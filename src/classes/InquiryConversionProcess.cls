/**********************************************************************************************************************
* Name               : InquiryService                                                                                 *
* Description        : Has the below functionality.                                                                   *
*                      - Convert Inquiry to Person/Business Account or Person/Business Contact.                       *
*                      - Copy all the activities of an Inquiry to the respective created Account.                     *
*                      - Create Account Team Members.                                                                 *
* Created Date       : 22/01/2017                                                                                     *
* Created By         : NSI                                                                                            *
* --------------------------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                                  *
* 1.0         Subhash     05/02/2017      Initial Draft.                                                            *
* 1.1      Vineet    12/02/2017    Added account team creation code.                                         *
* 1.2      Vineet    09/07/2017    Added check to not map the siebel ids of the task while cloning it,       * 
*                                           from inquiry on conversion.                                               *
* 1.3      Vineet    24/07/2017    #1590 : Added code change the owner of the account and the inquiry,       *
*                                           to the person taking action on the step if the associated inquiry         *
*                                           is assigned to a queue.                                                   *
* 1.4      Vineet    24/07/2017    Added bypass flag for not running the inquiry trigger,                    *
*                                           as we are only updating the status of the inquiry.                        *
**********************************************************************************************************************/
public without sharing class InquiryConversionProcess{
  @future 
  public static void convertInquiry(Set<Id> inquiryIdsList){ 
      String queryString;
      String userObjectKeyPrefix = Schema.SObjectType.User.getKeyPrefix();
      String businessContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('DAMAC Contact').getRecordTypeId();
      String businessAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
      String personAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        List<Account> newAccountUpsertList = new List<Account>();
        List<Account> newAccountInsertList = new List<Account>();
      List<account> newAccounts = new List<account>(); 
      List<Buyer__c> updateBuyersList = new List<Buyer__c>();  
      Set<Inquiry__c> inquiryToCreateContactsSet = new Set<Inquiry__c>(); 
      Set<Id> ownerIdsSet = new Set<Id>();
      Map<String,Inquiry_Conversion_Mapping__c> custSettMapping = new Map<String,Inquiry_Conversion_Mapping__c>();
        queryString = 'SELECT Name, Company__c, Inquiry_Status__c, Associated_Customer__c';
        /* Query all other fields to Map from Inquiry to Account and Activity. */
        custSettMapping = Inquiry_Conversion_Mapping__c.getall();
        for(String s : custSettMapping.keySet()){
            queryString += (queryString == '' ? '' : ',') + s;
        }
        queryString += ' ,(SELECT Id, Organisation_Name__c FROM Buyers__r) FROM Inquiry__c WHERE Id=: inquiryIdsList';
        List<Inquiry__c> inquiriesList = Database.query(queryString);
        for(Inquiry__c thisInquiry : inquiriesList) {    
            ownerIdsSet.add(thisInquiry.ownerId);
        }
    Map<Id,User> userListWithInquiriesMap = new Map<Id,User> ([SELECT Id, IsPortalEnabled FROM User WHERE Id =: ownerIdsSet AND IsPortalEnabled = true]);
        for(Inquiry__c thisInquiry : inquiriesList){     
          
          if(String.isBlank(thisInquiry.Associated_Customer__c)){
                Account newAccount = new Account();
                for(String s : custSettMapping.keySet()){
                if(thisInquiry.Buyers__r.size() > 0) {
                  system.debug('thisInquiry.Buyers__r[0].Organisation_Name__c: ' + thisInquiry.Buyers__r[0].Organisation_Name__c);
                    if(String.isBlank(thisInquiry.Buyers__r[0].Organisation_Name__c)){                
                        String accountField = custSettMapping.get(s).Person_Account_Field_Name__c;                      
                        if(String.isNotBlank(accountField)){
                            newAccount.put(accountField, thisInquiry.get(s));  
                        }
                        newAccount.put('RecordtypeId', personAccountRecordTypeId);
                    }else{
                        String accountField = custSettMapping.get(s).Business_Account_Field_Name__c;                      
                        if(String.isNotBlank(accountField)){
                            system.debug('#### Corporate Account Field = '+accountField+', Value = '+thisInquiry.get(s));
                            newAccount.put(accountField, thisInquiry.get(s));
                            newAccount.Name = thisInquiry.Buyers__r[0].Organisation_Name__c;
                        }
                        newAccount.put('RecordtypeId', businessAccountRecordTypeId);
                        inquiryToCreateContactsSet.add(thisInquiry);
                    }
                }
                }
                String accountOwner = newAccount.OwnerId;
                if(!accountOwner.startsWith(userObjectKeyPrefix) || userListWithInquiriesMap.containsKey(accountOwner)){
          newAccount.OwnerId = UserInfo.getUserId();  
        }
        
                if(String.isNotBlank(thisInquiry.Party_ID__c)){
                  newAccountUpsertList.add(newAccount);
                }else{
                  newAccountInsertList.add(newAccount);  
                }
                newAccounts.add(newAccount);
            }
      }



    if(!newAccountUpsertList.isEmpty()){
            try {
              upsert newAccountUpsertList Party_ID__c; 
            } catch(Exception  e){ 
          Log__c obj = new Log__c();
                  obj.Description__c  = e.getMessage ();
                  insert obj;
            }
      // Calling method to create account team. 
      createAccountTeam(newAccountUpsertList, ownerIdsSet);
    }
        
    if(!newAccountInsertList.isEmpty()){
      insert newAccountInsertList;
      //Calling method to create account team.
      createAccountTeam(newAccountInsertList, ownerIdsSet);
    }

        Map<Id, Id> inquiryAccountMap = new Map<Id, Id>();
        Set<Id> accountids = new Set<Id>();
        for(Account thisAccount : newAccounts){ 
            accountids.add(thisAccount.Id);
            inquiryAccountMap.put(thisAccount.Inquiry__c, thisAccount.Id);
        }
        
        /* Create contacts for Business Accounts. */
        if(inquiryToCreateContactsSet.size() > 0){
          List<contact> newContacts = new List<contact>();          
          for(Inquiry__c inquiry : inquiryToCreateContactsSet){
            contact newContact = new contact();
            newContact.recordTypeId = businessContactRecordTypeId;
            for(String s:custSettMapping.keySet()){ 
              String conFld = custSettMapping.get(s).Business_contact_Field_Name__c; 
              if(conFld != null){
                newContact.put(conFld, inquiry.get(s));
                newContact.put('AccountId',inquiryAccountMap.get(inquiry.Id));  
              }            
            }
            String contactOwner = newContact.OwnerId;
                if(!contactOwner.startsWith(userObjectKeyPrefix)){
              newContact.OwnerId = UserInfo.getUserId();  
            }
            newContacts.add(newContact);
          }
          upsert newContacts Party_ID__c;
        }
        system.debug('#### inquiryAccountMap = '+inquiryAccountMap);
        /* Update inquiry record. */
        for(Inquiry__c thisInquiry : inquiriesList){
          thisInquiry.Associated_Customer__c = inquiryAccountMap.get(thisInquiry.Id);
          thisInquiry.Inquiry_Status__c = 'Closed Won';
          thisInquiry.By_Pass_Validation__c = true;
          String inquiryOwner = thisInquiry.OwnerId;
          if(!inquiryOwner.startsWith(userObjectKeyPrefix)){
            thisInquiry.OwnerId = UserInfo.getUserId();  
          }
          for(Buyer__c thisBuyer : thisInquiry.Buyers__r){
            Buyer__c buyerRecord = new Buyer__c();
            buyerRecord.Id = thisBuyer.Id;
            buyerRecord.Account__c = inquiryAccountMap.get(thisInquiry.Id);
            updateBuyersList.add(buyerRecord);  
          }
        }
          
        /* Updating buyer's account info. */    
        if(!updateBuyersList.isEmpty()){
          update updateBuyersList;  
        }
        /* Clone activities. */
        if(!accountids.isEmpty() && !inquiryIdsList.isEmpty() && test.isrunningtest() ){
          cloneActivities(accountids, inquiryIdsList);        
        }
        /* Update inquiry status as closed won and update the account and inquiry owner to the, 
           property consultant taking the action on the step that triggers this conversion. */
    TriggerFactoryCls.setBYPASS_UPDATE_TRIGGER();
        update inquiriesList; 
        TriggerFactoryCls.resetBYPASS_UPDATE_TRIGGER(); 
    }
    
    /*********************************************************************************************
    * @Description : Method to create account teams.                                             *
    * @Params      : List<Account>, Set<Id>                                                      *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    @TestVisible
    private static void createAccountTeam(List<Account> convertedAccountList, Set<Id> ownerIdsSet){
        List<AccountTeamMember> insertAccountTeamRecords = new List<AccountTeamMember>();
        /* Calling method to get team members map. */
        Map<Id, Set<Id>> userTeamMembersMap = getTeamMembers(ownerIdsSet);
        for(Account thisAccount : convertedAccountList){
          if(userTeamMembersMap.containsKey(thisAccount.OwnerId)){
            for(Id thisTeamMember : userTeamMembersMap.get(thisAccount.OwnerId)){
              AccountTeamMember atRecord = new AccountTeamMember();
                atRecord.AccountId = thisAccount.Id;
                atRecord.AccountAccessLevel = 'Read';
                atRecord.ContactAccessLevel = 'Read';
                atRecord.CaseAccessLevel = 'None';
                atRecord.OpportunityAccessLevel = 'None';
                atRecord.UserId = thisTeamMember;
                insertAccountTeamRecords.add(atRecord);   
            }  
          }
        }   
        if(!insertAccountTeamRecords.isEmpty()){
            insert insertAccountTeamRecords; 
        }
    }
    
    /*********************************************************************************************
    * @Description : Method to get team members.                                                 *
    * @Params      : Set<Id>                                                                     *
    * @Return      : Map<Id, Set<Id>>                                                            *
    *********************************************************************************************/
    private static Map<Id, Set<Id>> getTeamMembers(Set<Id> ownerIdsSet){
      Map<Id, Set<Id>> userTeamMembersMap = new Map<Id, Set<Id>>();
      for(User thisUser : [SELECT Id, ManagerId, Manager_Role__c, Manager.Manager_Role__c, 
                    Manager.ManagerId, Manager.Manager.ManagerId 
                 FROM User 
                 WHERE Id IN: ownerIdsSet AND IsActive = true]){
            Set<Id> userManagerIdsSet = new Set<Id>{thisUser.Id};
            if(thisUser.ManagerId != null && String.isNotBlank(thisUser.Manager_Role__c) && 
               ((String.isNotBlank(thisUser.Manager_Role__c) && thisUser.Manager_Role__c.containsIgnoreCase('DOS')) || 
                (String.isNotBlank(thisUser.Manager.Manager_Role__c) && thisUser.Manager.Manager_Role__c.containsIgnoreCase('HOS')))){
                userManagerIdsSet.add(thisUser.ManagerId);
            }
            if(thisUser.Manager.ManagerId != null && String.isNotBlank(thisUser.Manager.Manager_Role__c) && 
               thisUser.Manager.Manager_Role__c.containsIgnoreCase('HOS')){ 
              userManagerIdsSet.add(thisUser.Manager.ManagerId);
            }
        userTeamMembersMap.put(thisUser.Id, new Set<Id>(userManagerIdsSet));   
      }  
      return userTeamMembersMap;
    }
       
    /*********************************************************************************************
    * @Description : Method to clone activities from task to account.                            *
    * @Params      : Set<Id>, Set<Id>                                                            *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public static void cloneActivities(Set<Id> parentIds, Set<Id> inquiryIdsList){
        /* Get all the updatable fields from task and store all updatable fields in the map. */
        Map<String,String> updatableTaskFields = new Map<String,String>();
        Map<String, Schema.SObjectField> mapTaskFields = Schema.SObjectType.Task.fields.getMap();
        for(String fieldName : mapTaskFields.keySet()) {
            if(mapTaskFields.get(fieldName).getDescribe().isUpdateable() && !fieldName.equalsIgnoreCase('Siebel_Row_ID__c')) {
                updatableTaskFields.put(fieldName , fieldName);
            }
        }   
        system.debug('#### updatable Task Fields = '+updatableTaskFields);
        String queryString;
        queryString = 'SELECT Id';
        for(String thisField : updatableTaskFields.keySet()){
            queryString += (queryString == '' ? '' : ',') + thisField;
        }
        queryString += ' FROM Task WHERE WhatId =: inquiryIdsList';
        List<task> tasksToClone = new List<task>();
        tasksToClone = Database.query(queryString);
        
        /* Map to hold key as inquiry Id and value as Account Id. */
        Map<Id, Id> mapInqAcc = new Map<Id, Id>();
        for(Account thisAccount : [SELECT Id, Inquiry__c FROM Account WHERE Id IN: parentIds]){
            mapInqAcc.put(thisAccount.Inquiry__c, thisAccount.Id);
        }
        
        /* Map to hold inquiry and its respective Tasks. */
        Map<Id, Set<Task>> mapInqTasks = new Map<Id, Set<Task>>();
        for(task thisTask : tasksToClone){
            if(mapInqTasks.containskey(thisTask.WhatId) ){
                mapInqTasks.get(thisTask.whatid).add(thisTask);
            }else{
                mapInqTasks.put(thisTask.whatId, new Set<Task>{thisTask});
            }   
        }
        
        /* populate same map with key as Account Id. */ 
        for(Id thisKey : mapInqTasks.keySet()){             
            mapInqTasks.put(mapInqAcc.get(thisKey), mapInqTasks.get(thisKey));
        }
        List<task> newTasksToInsert = new List<task>();
        for(String str : mapInqTasks.keySet()){
            if(str != null && str.subString(0,3) == '001'){
                for(task thisTask : mapInqTasks.get(str)){
          Task newTask = new Task();
          newTask = createTask (str, thisTask) ;
                    newTasksToInsert.add(newTask);
                }
            }
        }
        insert newTasksToInsert;
    }
    public static Task createTask (String str, Task objTask) {
        Task newTask = new Task();
        newTask = objTask.clone(false);   
        newTask.whatId = str;
        if(String.isNotBlank(newTask.Status) && newTask.Status.equalsIgnoreCase('Completed') && 
           String.isBlank(newTask.Activity_Outcome__c)){
               newTask.Activity_Outcome__c = 'NA';  
           }
          return newTask;
    }
}// End of class.