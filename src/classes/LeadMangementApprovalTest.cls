/***************************************************************************************************
* Description - The test class is written to cover code coverage of LeadMangementApproval 
        apex class
*
* Version   Date            Author            Description
* 1.0       28/03/18        Monali            Initial Draft
***************************************************************************************************/

@isTest
private class LeadMangementApprovalTest {

    static testMethod void testMethod1() {        
    Lead_Management__c leadObj = new Lead_Management__c();
        leadObj.Status__c = 'Submitted';
        insert leadObj;
        System.debug('======leadObj : ' + leadObj.Id);
    }
}