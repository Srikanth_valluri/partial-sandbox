@isTest
public class DiassociateInventoryController_TC {
     static testMethod void testMethod1() {
        Unit_Assignment__c us = new Unit_Assignment__c();
            us.Deal_Value__c = 15;
            us.Reason_For_Unit_Assignment__c = 'test';
            us.Start_Date__c = System.today();
            us.Proposal_Price__c = 14;
            us.Unit_Assignment_Name__c = 'test';
         insert us;
        Inventory__c i = new Inventory__c();
            i.Floor__c = 'test';
            i.Unit_Type__c = '20';
            i.Unit_Assignment__c = us.Id;
         insert i;
         
        us.Deal_Value__c = 16;
        us.Proposal_Price__c = 10;
        update us;
        
        Unit_Allocation__c allocate = new Unit_Allocation__c ();
        allocate.start_date__c = Date.Today ();
        allocate.End_Date__c = Date.Today().addDays (10);
        insert allocate;
        
        Inventory_User__c invUser = new Inventory_User__c();
        invUser.User__c = userinfo.getuserId();
        invUser.Inventory__c = i.id;
        invUser.Unit_allocation__c = allocate.id;
        invUser.Unit_Assignment__c = us.id;
        insert invUser;
    ApexPages.CurrentPage().getparameters().put('id', us.id);      
        
        Apexpages.StandardController sc = new Apexpages.StandardController(us);
        DiassociateInventoryController obj = new DiassociateInventoryController(sc); 
        obj.inventoryId = i.id;
        obj.diassociateInventory();     
    }
}