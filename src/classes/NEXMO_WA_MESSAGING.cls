//V2 - Charan added new logic to fix locale - 09072020
public class NEXMO_WA_MESSAGING{

    
    public id sfRecId;
    
    public sobject sfRec {get;set;}
    
    private id salesforceRecordId;
    
    public string whastAppMessage {get;set;}
    
    public boolean showPage {get;set;}
    
    private Nexmo_WA_Fields__mdt waFldMapping;
    
    public list<Nexmo_Whats_App_Message__c> msgHistory {get;set;}
    
    public string waMessage {get;set;}
    
    public boolean showUpload {get;set;}
    
    public List<SelectOption> templatesAvaliable {get;set;}
     
    public string selectedTemplate {get;set;} 
    
    public string templatePreview {get;set;}
    
    public List<SelectOption> mobileNumbersAvaliable {get;set;}
    
    public string selectedWhatsAppNumber {get;set;}
    
    public boolean showSessionButton {get;set;}

    public Boolean showWAPage { get; set; }
    
    public NEXMO_WA_MESSAGING(){    
        showPage = false; 
        try{     
            sfRecId = apexpages.currentpage().getparameters().get('id'); 
            showWAPage = false;
            try {
                List <NEXMO_WA_Owner_Assignment__c> assignments = new List <NEXMO_WA_Owner_Assignment__c> ();
                assignments = [SELECT Owner_Id__c FROM NEXMO_WA_Owner_Assignment__c WHERE Owner_Id__c =: Userinfo.getUserId ()];
                if (assignments.size () > 0) {
                    showWAPage = true;
                }
            } catch (Exception e) {
                
            }
            
            system.debug(sfRecId );
            getWhatsAppAPINumbers(sfRecId);
            getRecordName(sfRecId,selectedWhatsAppNumber);
            messageHistory();  
            populatetemplates();  
            getTemplatePreview();
        }catch(exception e){}
        if(checkWhatsEnabled()){
            showPage = true;    
        }
    }
    
    
    
    
    public void getWhatsAppAPINumbers(id recordId){ 
        string sObjName = recordId.getSObjectType().getDescribe().getName();        
        waFldMapping = new Nexmo_WA_Fields__mdt();
        waFldMapping = [select id,Nexmo_Whats_App_Number_API_Names__c,Object_Name__c from Nexmo_WA_Fields__mdt where Object_Name__c =:sObjName LIMIT 1];
        string mobileApi = waFldMapping.Nexmo_Whats_App_Number_API_Names__c;
        //Split API Names
        mobileNumbersAvaliable = new List<SelectOption>();     
        for(string s: mobileApi.split(';')){            
            mobileNumbersAvaliable.add(new SelectOption(s,s.replaceall('__c','').replaceall('_',' ')));            
        }
        system.debug('mobileNumbersAvaliable====='+mobileNumbersAvaliable);
    }
    
        
    public void getTemplatePreview(){
        system.debug('selectedTemplate=='+selectedTemplate);
        if((selectedTemplate == null || selectedTemplate == '') && showSessionButton == true){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Select a Template'));
        }else{
            map<string,string> templates = getTemplate(selectedTemplate);
            system.debug(templates+'---templates');
            templatePreview = templates.get('preview');   
            system.debug('templatePreview =='+templatePreview);
            map<integer,string> templateParms = new map<integer,string>();    
            templateParms = getTemplateParameters(sfRecId);
            if(templateParms.get(1) != null){
                templatePreview = templatePreview.replace('{{1}}',templateParms.get(1));
            }
            if(templateParms.get(2) != null){
                templatePreview = templatePreview.replace('{{2}}',templateParms.get(2));
            }
            if(templateParms.get(3) != null){
                templatePreview = templatePreview.replace('{{3}}',templateParms.get(3));
            }
            if(templateParms.get(4) != null){ // Added on Nov 3 2019
                templatePreview = templatePreview.replace('{{4}}',templateParms.get(4));
            }
        }
    }
    
    public boolean checkWhatsEnabled(){        
        user u = [select id,Enable_Nexmo_Whats_App__c from user where id=:userinfo.getuserid()];
        return u.Enable_Nexmo_Whats_App__c ;
    }
    
    public void populatetemplates(){
        list<Nexmo_Templates__mdt> allTemplates = new list<Nexmo_Templates__mdt>();
        allTemplates = [select id,DeveloperName,MasterLabel,Template_Preview__c, Excluded_Profiles__c FROM Nexmo_Templates__mdt];
        Profile prof = [Select id, name from Profile where id =: UserInfo.getProfileId()];
        templatesAvaliable  = new List<SelectOption>();    
        templatesAvaliable.add(new SelectOption('','-Select-'));
        for(Nexmo_Templates__mdt templateName:allTemplates){     
            if(templateName.Excluded_Profiles__c == null || (templateName.Excluded_Profiles__c != null && !templateName.Excluded_Profiles__c.contains(prof.Name)))       
                templatesAvaliable.add(new SelectOption(templateName.DeveloperName,templateName.MasterLabel));    
        }            
    }
        
    
    public map<string,string> getTemplate(string templateName){
        system.debug('>>>>>>>>>'+templateName);
        map<string,string> templatesMap = new map<string,string>();
        //try {
            Nexmo_Templates__mdt templates = new Nexmo_Templates__mdt();
            if (!Test.isRunningTest ()) {
                templates = [select id,developerName,Template_Preview__c,Fallback_Locale__c,Template_Namespace__c,Template_Parameters__c 
                            from Nexmo_Templates__mdt where DeveloperName=:templateName LIMIT 1];
            } else {
                templates.Template_Preview__c = 'Dear {{1}}';
                //templates.Fallback_Locale__c = 'en_gb';
                templates.Template_Namespace__c = '178ce8d5_8813_a776_5019_c736dd92d350';
                templates.Template_Parameters__c = '1';
                
            }
            templatesMap.put('preview',templates.Template_Preview__c );
            templatesMap.put('fallbacklocale',templates.Fallback_Locale__c);        
            templatesMap.put('namespace',templates.Template_Namespace__c);
            templatesMap.put('noOfParmsInTemplate',templates.Template_Parameters__c);
        //} catch (Exception e) {}
        
        return templatesMap;
    }
    

    /* Get the Authorization Header */    
    public static map<string,string> getCredentials(){
        
        map<string,string> credentialMap = new map<string,string>();
            try{  
                Nexmo_WA_Credentials__mdt  credentials = new Nexmo_WA_Credentials__mdt ();
                    
                credentials = [select id,API_Key__c,API_Secret__c,Whats_App_Number__c,Endpoint_URL__c,
                                Application_Id__c,Whats_App_Account_Namespace__c
                                FROM Nexmo_WA_Credentials__mdt LIMIT 1];
                
                system.debug(credentials.API_Secret__c);
                system.debug(credentials.API_Key__c);
                string forBase64 = credentials.API_Key__c+':'+credentials.API_Secret__c;
                blob blobforBase64 = blob.valueof(forBase64);
                string header = EncodingUtil.base64Encode(blobforBase64);            
                credentialMap.put('authHeader',header);
                credentialMap.put('whatsAppnumber',credentials.Whats_App_Number__c);
                credentialMap.put('endpoint',credentials.Endpoint_URL__c);                
                credentialMap.put('appid',credentials.Application_Id__c);
                system.debug(credentialMap);
                system.debug(credentialMap.size());
            }catch(Exception e){
                Apexpages.addmessages(e);
            }
        
        return credentialMap;
    }
    
    
    public string PrepareJWTToken(){
        string JWTAssertion='';
        map<string,string> credentials = getCredentials();
        string appId = credentials.get('appid');
        system.debug('APPLICATION ID=='+credentials.get('appid'));
        String JWTHeader = '{"alg": "RS256","typ": "JWT"}';
        JWTHeader = base64URLencode (Blob.valueOf(JWTHeader));
        Long rightNow = (dateTime.now().getTime()/1000);
        Long expireTime = datetime.now().addHours(24).getTime()/1000;
        String JWTClaim = '{"application_id": "'+appId+'",'
            +'"iat": "'+rightNow+'","jti": "'+rightNow +'0000087", "exp" : "'+expireTime+'"}';
        System.Debug (JWTClaim);
        JWTClaim = base64URLencode (Blob.valueOf(JWTClaim));
        String privateKey = '';
        
        Document doc = NEW Document ();
        doc = [ SELECT BODY FROM Document WHERE Name = 'JWT_Private_Key'];
        privateKey = (doc.Body).toString ();
        privateKey = privateKey .deleteWhiteSpace ();
        privateKey = privateKey .remove ('\n');
        
        
        String modifiedPrivateKey = prepareStringForBase64Decoding (privateKey);
        Blob privateKeyBlob = EncodingUtil.base64Decode (modifiedPrivateKey);
        Blob JWTSignature = Crypto.sign('RSA-SHA256', 
                                        Blob.valueof(JWTHeader +'.'+JWTClaim), 
                                        privateKeyBlob);
        System.Debug (base64URLencode (JWTSignature));
        JWTAssertion = JWTHeader +'.'+JWTClaim +'.'+base64URLencode (JWTSignature);
        System.Debug (JWTAssertion);
        return JWTAssertion;        
    }
    
    public static String prepareStringForBase64Decoding(String stringToPrepare) {
        String last4Bytes = stringToPrepare.substring(stringToPrepare.length() - 4, stringToPrepare.length());
        if (last4Bytes.endsWith('==')) {
            last4Bytes = last4Bytes.substring(0, 2) + '0K';
            stringToPrepare = stringToPrepare.substring(0, stringToPrepare.length() - 4) + last4Bytes;
        } else if (last4Bytes.endsWith('=')) {
            last4Bytes = last4Bytes.substring(0, 3) + 'N';
            stringToPrepare = stringToPrepare.substring(0, stringToPrepare.length() - 4) + last4Bytes;
        }
        return stringToPrepare;
    }
    
    public static String base64URLencode (Blob input){ 
        return EncodingUtil.base64Encode(input).replace('+', '-').replace('/', '_');
    }
    
    public string bodyTosend(boolean isWithMTM, string templateName){
        map<string,string> templatesMap = new map<string,string>();
        if(templateName != ''){
           templatesMap = getTemplate(templateName);
        }
        system.debug('templatesMap='+templatesMap);
        map<string,string> credentials = getCredentials(); 
        Decimal toNum = decimal.valueof(getRecordName(sfRecId,selectedWhatsAppNumber).substringafter(';').removestart('00')) ;  
        map<integer,string> templateParms = new map<integer,string>();    
        templateParms = getTemplateParameters(sfRecId);
        system.debug('templateParms =='+templateParms);
        //string bodyStr = '{   "from":{      "type":"whatsapp",      "number":"447418342136"   },   "to":{      "type":"whatsapp",      "number":"919652300133"   },   "message":{      "content":{         "type":"template",         "template":{            "name":"whatsapp:hsm:technology:nexmo:verify",            "parameters":[               {                  "default":"Nexmo Verification"               },               {                  "default":"64873"               },               {                  "default":"10"               }            ]         }      }   }}';
        //string bodyStr = '{   "from":{      "type":"whatsapp",      "number":"'+credentials.get('whatsAppnumber')+'"   },   "to":{      "type":"whatsapp",      "number":"'+toNum+'"   },   "message":{      "content":{         "type":"template",         "template":{            "name":"whatsapp:hsm:technology:nexmo:verify",            "parameters":[               {                  "default":"Nexmo Verification"               },               {                  "default":"64873"               },               {                  "default":"10"               }            ]         }      }   }}';
        String jsonString ;
        if(isWithMTM){
            NEXMO_OB_REQUEST_GEN obj = new NEXMO_OB_REQUEST_GEN ();
            NEXMO_OB_REQUEST_GEN.cls_from fromCls = new NEXMO_OB_REQUEST_GEN.cls_from ();
            fromCls.type = 'whatsapp';
            fromCls.WA_number = credentials.get('whatsAppnumber');
            obj.WA_from = fromCls;
            
            NEXMO_OB_REQUEST_GEN.cls_to toCls = new NEXMO_OB_REQUEST_GEN.cls_to ();
            toCls.type = 'whatsapp';
            toCls.WA_number = string.valueof(toNum);
            obj.to = toCls;
            
            NEXMO_OB_REQUEST_GEN.cls_message msgCls = new NEXMO_OB_REQUEST_GEN.cls_message ();
            NEXMO_OB_REQUEST_GEN.cls_content contentCls = new NEXMO_OB_REQUEST_GEN.cls_content ();
            contentCls.type = 'template';
            
            NEXMO_OB_REQUEST_GEN.cls_template templateCls = new NEXMO_OB_REQUEST_GEN.cls_template ();  
            templateName = templatesMap.get('namespace')+':'+templateName;
            system.debug('templateName ==='+templateName);
            //templateCls.name = '178ce8d5_8813_a776_5019_c736dd92d350:calls';
            templateCls.name = templateName;
            //templateCls.fallback_locale = templatesMap.get('fallbacklocale');
            
            string parmsCount = templatesMap.get('noOfParmsInTemplate');
            
            system.debug('###############'+templateCls);
            List <NEXMO_OB_REQUEST_GEN.cls_parameters> params = new List <NEXMO_OB_REQUEST_GEN.cls_parameters> ();
            if(parmsCount == '1' || parmsCount == '2' || parmsCount == '3' || parmsCount == '4'){                
                NEXMO_OB_REQUEST_GEN.cls_parameters param1 = new NEXMO_OB_REQUEST_GEN.cls_parameters ();            
                param1.WA_default = templateParms.get(1);
                params.add (param1);
            }
            
            if(parmsCount == '2' || parmsCount == '3' || parmsCount == '4'){
                NEXMO_OB_REQUEST_GEN.cls_parameters param2 = new NEXMO_OB_REQUEST_GEN.cls_parameters ();
                param2.WA_default = templateParms.get(2);
                params.add (param2);
            }
            
            if(parmsCount == '3' || parmsCount == '4'){
                NEXMO_OB_REQUEST_GEN.cls_parameters param3 = new NEXMO_OB_REQUEST_GEN.cls_parameters ();            
                param3.WA_default = templateParms.get(3);
                params.add (param3);
            }
            
            if( parmsCount == '4'){
                NEXMO_OB_REQUEST_GEN.cls_parameters param4 = new NEXMO_OB_REQUEST_GEN.cls_parameters ();            
                param4.WA_default = templateParms.get(4);
                params.add (param4);
            }
            
            //V2 - Charan added new logic to fix locale - 09072020
            NEXMO_OB_REQUEST_GEN.cls_whatsapp wapp = new NEXMO_OB_REQUEST_GEN.cls_whatsapp();
            wapp.policy = 'deterministic';
            wapp.locale = 'en-GB';
            
            templateCls.parameters = params;
            contentCls.template = templateCls;
            msgCls.content = contentCls;
            msgcls.whatsapp = wapp; //V2
            obj.message = msgCls;
            
            jsonString = JSON.serialize (obj);
            System.Debug (jsonString);
            jsonString = jsonString.replaceAll ('WA_', '');
            System.Debug (jsonString);
        }else{ // NO MTM
            NEXMO_OB_REQUEST_GEN_NO_MTM obj = new NEXMO_OB_REQUEST_GEN_NO_MTM ();
            NEXMO_OB_REQUEST_GEN_NO_MTM.cls_from fromCls = new NEXMO_OB_REQUEST_GEN_NO_MTM.cls_from ();
            fromCls.type = 'whatsapp';
            fromCls.WA_number =  credentials.get('whatsAppnumber');
            obj.WA_from = fromCls;
            
            NEXMO_OB_REQUEST_GEN_NO_MTM.cls_to toCls = new NEXMO_OB_REQUEST_GEN_NO_MTM.cls_to ();
            toCls.type = 'whatsapp';
            toCls.WA_number =  string.valueof(toNum);
            obj.WA_to = toCls;
            
            NEXMO_OB_REQUEST_GEN_NO_MTM.cls_content contentCls = new NEXMO_OB_REQUEST_GEN_NO_MTM.cls_content ();
            contentCls.type = 'text';
            contentCls.text = waMessage;
            
            NEXMO_OB_REQUEST_GEN_NO_MTM.cls_message msg = new NEXMO_OB_REQUEST_GEN_NO_MTM.cls_message ();
            msg.content = contentCls;
            obj.message = msg;
            
            jsonString = JSON.serialize (obj);
            System.Debug (jsonString);
            jsonString = jsonString.replaceAll ('WA_', '');
            System.Debug (jsonString);
        
        
        }
        return jsonString;
    }

    /* Send New Message to Nexmo Server */
    public void sendMessageWithMTM(){
        if(selectedTemplate == null || selectedTemplate=='' ){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Select a Template'));
        }else{
            system.debug(selectedTemplate);
            map<string,string> credentials = getCredentials();
            string JWTToken = PrepareJWTToken();        
            if(credentials.size() > 0){ 
                string bodyStr = bodyTosend(true,selectedTemplate);
                HttpRequest req = new HttpRequest();            
                req.setEndpoint(credentials.get('endpoint'));
                //req.setHeader('Authorization','Basic '+credentials.get('authHeader'));
                req.setHeader('Authorization','Bearer '+JWTToken);
                req.setBody(bodyStr);
                req.setHeader('Content-Type','application/json');
                req.setMethod('POST');        
                Http http = new Http();
                HTTPResponse res;
                if(!test.isRunningTest()){
                    res = http.send(req);
                    System.debug(req);
                    System.debug(res.getBody());
                    System.debug(res.getstatuscode());
                    if( res.getstatuscode() == 200 ||  res.getstatuscode() == 202 ){
                        NEXMO_MESSGAE_RESPONSE resp = NEXMO_MESSGAE_RESPONSE.parse(string.valueof(res.getBody()));
                        system.debug('message_uuid>>>>'+resp.message_uuid);
                        //createRequest(resp.message_uuid,waMessage,bodyStr,true);
                        templatePreview = templatePreview + label.Nexmo_New_Session_Message;
                        createRequest(resp.message_uuid,templatePreview,bodyStr,true);
                    }
                }else{
                    NEXMO_MESSGAE_RESPONSE resp = NEXMO_MESSGAE_RESPONSE.parse(string.valueof('{"message_uuid":"54af4397-0ea7-48c5-bc9a-d7551bfb2bfb"}'));                
                }
                waMessage = null;
            }  
        }  
    }
    
    public void sendMessageWithoutMTM(){    
        //string bodyStr = '{   "from":{      "type":"whatsapp",      "number":"447418342136"   },   "to":{      "type":"whatsapp",      "number":"919652300133"   },   "message":{      "content":{         "type":"text",         "text":"'+waMessage+'"      }   }}';
        map<string,string> credentials = getCredentials();
        string JWTToken = PrepareJWTToken();
        if(credentials.size() > 0){ 
            string bodyStr = bodyTosend(false,'');
            HttpRequest req = new HttpRequest();            
            req.setEndpoint(credentials.get('endpoint'));
            //req.setHeader('Authorization','Basic '+credentials.get('authHeader'));
            req.setHeader('Authorization','Bearer '+JWTToken);
            req.setBody(bodyStr);
            req.setHeader('Content-Type','application/json');
            req.setMethod('POST');        
            Http http = new Http();
            HTTPResponse res;
            if(!test.isRunningTest()){
                res = http.send(req);
                System.debug(req);
                System.debug(res.getBody());
                System.debug(res.getstatuscode());
                if( res.getstatuscode() == 200  ||  res.getstatuscode() == 202){
                    NEXMO_MESSGAE_RESPONSE resp = NEXMO_MESSGAE_RESPONSE.parse(string.valueof(res.getBody()));
                    system.debug('message_uuid>>>>'+resp.message_uuid);
                    createRequest(resp.message_uuid,waMessage,bodyStr,false);
                }
            }else{
                NEXMO_MESSGAE_RESPONSE resp = NEXMO_MESSGAE_RESPONSE.parse(string.valueof('{"message_uuid":"54af4397-0ea7-48c5-bc9a-d7551bfb2bfb"}'));                
                createRequest(resp.message_uuid,waMessage,'',false);
            }
            waMessage = null;
        }    
    }
    
    
    public void createRequest(string messageuuid,string messagetext,string reqBody,boolean iswithMTM){
        // Insert Parent
        Nexmo_Whats_App_Request__c request = new Nexmo_Whats_App_Request__c();
        request.Record_Id__c = sfrecid;
        request.Record_Name__c = getRecordName(sfrecid, selectedWhatsAppNumber).substringbefore(';');
        request.To_number__c = decimal.valueof(getRecordName(sfrecid,selectedWhatsAppNumber).substringafter(';').removeStart('00'));
        request.Unique_Key__c =  getRecordName(sfrecid,selectedWhatsAppNumber).substringafter(';').removestart('00')+userinfo.getuserid();
        request.ownerid = userinfo.getuserid();
        request.Last_Message_Sent__c = system.now();
        request.Last_Message__c = wamessage;
        if(iswithMTM){
            request.Last_MTM_Sent_at__c = system.now();            
        }
        User u = [SELECT Profile.Name From User WHERE Id =: UserInfo.getUserId ()];
        if (u.Profile.Name == 'Promoter Community Profile') {
            String sObjName = sfrecid.getSObjectType().getDescribe().getName();
    
            if (sObjName == 'Inquiry__c') {
                request.Inquiry__c = sfrecid;
            }
        }
        /* To Send Message to Bot
        List < Nexmo_Whats_App_Message__c > messages = new List < Nexmo_Whats_App_Message__c > ();
        ID userId = Userinfo.getUserId();
        messages = [SELECT ID FROM Nexmo_Whats_App_Message__c WHERE Nexmo_Whats_App_Request__r.OwnerId =: userId
            AND direction__c = 'Outbound'
            AND CreatedDate = TODAY AND Nexmo_Whats_App_Request__r.Record_Id__c =: sfRecId
        ];
        if (messages.size() == 0) {
            Nexmo_WA_Credentials__mdt credentials = new Nexmo_WA_Credentials__mdt();

            credentials = [select id, API_Key__c, API_Secret__c, Whats_App_Number__c, Endpoint_URL__c,
                Application_Id__c, Whats_App_Account_Namespace__c
                FROM Nexmo_WA_Credentials__mdt LIMIT 1
            ];

            sendMessageToBot(String.valueOf(request.To_number__c), credentials.Whats_App_Number__c, wamessage);
        }*/
        upsert request Unique_Key__c;
        // Insert Child
        Nexmo_Whats_App_Message__c message = new Nexmo_Whats_App_Message__c();
        message.Nexmo_Whats_App_Request__c = request.id;
        message.direction__c = 'Outbound';
        message.Nexmo_Message_uuid__c = messageuuid;
        message.Nexmo_Message_Body__c = messagetext;
        message.Nexmo_Message_Request_Body__c = reqBody;
        insert message;
    }
    
    /*    public void sendMessageToBot(String toNumber, String fromNumber, String message) {
        String reqBody = '{"from": { "type": "whatsapp", "number": "' + fromNumber + '" },"to": { "type": "whatsapp", "number": "' + toNumber + '" },' +
            '"message": {"content": {"type": "text","text": "' + message + '"}}}';

        System.Debug(reqBody);
        HTTPRequest req = new HTTPRequest();
        req.setEndpoint('https://damac-test.fstaging.com/api/support');
        req.setMethod('POST');
        req.setBody(reqBody);
        req.setHeader('Authorization', 'Bearer YWxpYWFkbWlu');
        req.setHeader('Content-Type', 'application/json');
        req.setTimeOut(120000);
        HTTP http = new HTTP();
        HTTPResponse res = new HTTPResponse();
        if (!Test.isRunningTest())
            res = http.send(req);
        System.Debug(res.getBody());
    }
*/

    public string getRecordName(id recordId,string APIName){
        string sObjName = recordId.getSObjectType().getDescribe().getName();        
        waFldMapping = new Nexmo_WA_Fields__mdt();
        waFldMapping = [select id,API_Name__c,Object_Name__c from Nexmo_WA_Fields__mdt where Object_Name__c =:sObjName LIMIT 1];
        if(APIName == null){
            APIName = waFldMapping.API_Name__c;
        }
        string soqlQuery;
        soqlQuery = 'SELECT Id,Name,'+APIName+' from '+sObjName+ ' WHERE ID=:recordId';
          
        SYSTEM.debug('soqlQuery'+soqlQuery);
        string valToreturn;
        sfRec = database.query(soqlQuery);
        if(sfRec.get(APIName) != null){
            valToreturn = string.valueof(sfRec.get('Name'))+';'+string.valueof(sfRec.get(APIName));
        }else{
            valToreturn = string.valueof(sfRec.get('Name'))+';'+string.valueof(0);
        }
        return valToreturn;
    }
    
    public map<integer,string> getTemplateParameters(id recordId){
        string sObjName = recordId.getSObjectType().getDescribe().getName();        
        waFldMapping = new Nexmo_WA_Fields__mdt();
        waFldMapping = [select id,API_Name__c,Object_Name__c,Parameter_1_API__c,Parameter_2_API__c,Parameter_3_API__c,Parameter_4_API__c from Nexmo_WA_Fields__mdt where Object_Name__c =:sObjName LIMIT 1];
        string paramFields = '';
        if(waFldMapping.Parameter_1_API__c != null){
            paramFields = paramFields+waFldMapping.Parameter_1_API__c+',';
        }
        if(waFldMapping.Parameter_2_API__c != null){
            paramFields = paramFields+waFldMapping.Parameter_2_API__c+',';
        }
        if(waFldMapping.Parameter_3_API__c != null){
            paramFields = paramFields+waFldMapping.Parameter_3_API__c+',';
        }
        if(waFldMapping.Parameter_4_API__c != null){
            paramFields = paramFields+waFldMapping.Parameter_4_API__c+',';
        }
        paramFields = paramFields.removeEnd(',');
        system.debug('paramFields=='+paramFields );

        string soqlQuery = 'SELECT Id, ';
        
        if (!paramFields.contains ('Name,')) {
            soqlQuery += 'Name,'+paramFields+' from '+sObjName+ ' WHERE ID=:recordId';
        } else {
            soqlQuery += paramFields+' from '+sObjName+ ' WHERE ID=:recordId';
        }
        
        
        
        SYSTEM.debug('soqlQuery'+soqlQuery);
        sfRec = database.query(soqlQuery);
        map<integer,string> toreturnMap = new map<integer,string>();
        
        
        if(waFldMapping.Parameter_1_API__c != null){
            toreturnMap.put(1,string.valueof(sfrec.get(waFldMapping.Parameter_1_API__c)));
        }
        if(waFldMapping.Parameter_2_API__c != null){
            toreturnMap.put(2,string.valueof(sfrec.get(waFldMapping.Parameter_2_API__c)));
        }
        if(waFldMapping.Parameter_3_API__c != null){
            toreturnMap.put(3,string.valueof(sfrec.get(waFldMapping.Parameter_3_API__c)));
        }
        if(waFldMapping.Parameter_4_API__c != null){
            toreturnMap.put(4,string.valueof(sfrec.get(waFldMapping.Parameter_4_API__c)));
        }
        
        return toreturnMap;
    }
    
    public void messageHistory(){
        showSessionButton = false;
        system.debug('selectedWhatsAppNumber>>>>'+selectedWhatsAppNumber);
        decimal num = 0;
        num = decimal.valueof(getRecordName(sfrecid,selectedWhatsAppNumber).substringafter(';'));        
        
        if(num != 0 && num !=null){
            msgHistory = new list<Nexmo_Whats_App_Message__c>();
            User userCheck = new User();
            userCheck = [SELECT FederationIdentifier FROM User WHERE ID =: UserInfo.getUserId()];
            if (userCheck.FederationIdentifier == 'venkatasubhash.k') {
                msgHistory = [select id, Message_Type__c, Direction__c, message_body_url__c, message_body_lng__c, message_body_lat__c, Location_Link__c, LastModifiedDate, Message_Submitted__c, Message_Delivered__c, Nexmo_Message_Body__c,
                              Message_Failed__c, Message_Read__c, Nexmo_Whats_App_Request__r.Is_in_customer_care_window__c, Nexmo_Whats_App_Request__r.To_number__c, 
                              Nexmo_Whats_App_Request__r.template_used__c,Nexmo_Whats_App_Request__r.Inquiry__r.name
                               from Nexmo_Whats_App_Message__c 
                              WHERE Nexmo_Whats_App_Request__r.To_number__c=:num //and Nexmo_Whats_App_Request__r.Record_Id__c=:sfrecid
                              ORDER BY createddate asc LIMIT 500];
            } else {
                msgHistory = [select id, Message_Type__c, Direction__c, message_body_url__c, message_body_lng__c, message_body_lat__c, Location_Link__c, LastModifiedDate, Message_Submitted__c, Message_Delivered__c, Nexmo_Message_Body__c,
                              Message_Failed__c, Message_Read__c, Nexmo_Whats_App_Request__r.Is_in_customer_care_window__c, 
                              Nexmo_Whats_App_Request__r.To_number__c, Nexmo_Whats_App_Request__r.template_used__c,Nexmo_Whats_App_Request__r.Inquiry__r.name
                               from Nexmo_Whats_App_Message__c 
                              WHERE Nexmo_Whats_App_Request__r.ownerid=:userinfo.getuserid() 
                              and Nexmo_Whats_App_Request__r.To_number__c=:num //and Nexmo_Whats_App_Request__r.Record_Id__c=:sfrecid
                              ORDER BY createddate asc LIMIT 500];
              
            }
            if( msgHistory.size() == 0 ){                
                showSessionButton = true;
            }else if( msgHistory.size()>0 ){
                for(Nexmo_Whats_App_Message__c w:msgHistory){
                    if( w.Nexmo_Whats_App_Request__r.Is_in_customer_care_window__c == TRUE ){
                        showSessionButton = FALSE; 
                        break;
                    }else if( w.Nexmo_Whats_App_Request__r.Is_in_customer_care_window__c == FALSE ){
                        showSessionButton = TRUE; 
                       // break;
                    } 
                }          
            }
            populatetemplates();
            if (showSessionButton) {
                showUpload = false;
            } else {
                showUpload = true;
            }

            User u = [SELECT Enable_Nexmo_WhatsApp_Upload__c from User WHERE id =: UserInfo.getUserID()];
            if (!u.Enable_Nexmo_WhatsApp_Upload__c)
                showUpload = false;
                
            
        }else{
            msgHistory = new list<Nexmo_Whats_App_Message__c>();
        }
        
    }
       
    
       
    

}