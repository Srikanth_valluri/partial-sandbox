/*
    version     Date        Author         Description
                3/7/2019    Arjun Khatri    1.Created SMS History of type OTP
*/

public without sharing class OTPService {
    /*
    @future (callout=true)
    public static void Sendtextmessage(list<String> lstPhoneNumbers){
        List<OTP__c> lstotp =  new List<OTP__c>();
        for(String PhoneNumber : lstPhoneNumbers){
            Id caseId = null;
            if(PhoneNumber.contains(':')){
               list<String> lstValues = PhoneNumber.split(':');
               PhoneNumber = lstValues[1];
               caseId = lstValues[0];
            }
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http http = new Http(); 
            String user= 'salesforce';
            String passwd= 'D@$al3sF0rc387!';
            req.setMethod('POST' ); // Method Type
            req.setEndpoint('http://api.smscountry.com/SMSCwebservice_bulk.aspx'); 
            string otpnumber = String.valueOf(Math.random()).substring(8,12);
            string otpMessage = 'Dear customer, your booking of property has been initiated to verify please use OTP:'+ otpnumber;// Server Url
            //req.setHeader('Content-Type', 'application/x-www-form-urlencoded'); // Content TypeAuthorization=' + authorization
            //req.setHeader('user', user);
            //req.setHeader('passwd', passwd);
            req.setBody('user='+ user + '&passwd=' + passwd +'&message=' + otpMessage + '&mobilenumber=' + PhoneNumber + '&sid=DAMAC GRP'); // Request Parameters
            try {
                system.debug('Request Body---'+req.getBody());
                system.debug('Request Header---'+req.getHeader('Authorization'));
                system.debug('Request Header---'+req.getHeader('Content-Type'));
                res = http.send(req);
                system.debug('Response---'+res.getBody());
                if(res.getBody() != null){
                    // Parse Response
                    if('SMS message(s) sent' == res.getBody()){
                         system.debug('Message Send Sucessfully');
                         OTP__c otp =  new OTP__c();
                         
                         otp.OTP_Number__c = otpnumber;
                         otp.Phone_Number__c = PhoneNumber;
                         otp.Case__c = caseId;
                         lstotp.add(otp);
                    }
                }
            } catch(Exception e) {
                System.debug('Error***** '+ e);
            }
        }
        if(!lstotp.isEmpty()){
            try{
                upsert lstotp Phone_Number__c;
            }catch(exception ex){
                System.debug('DML Exception*****: '+ ex);
            }
        }
    }
    */
    
    @future (callout=true)
    public static void Sendtextmessage(String PhoneNumber){
        Id otpRecTypeId =  Schema.SObjectType.SMS_History__c.getRecordTypeInfosByName().get('OTP History').getRecordTypeId();
        List<OTP__c> lstotp =  new List<OTP__c>();
        List<SMS_History__c>smsLst = new List<SMS_History__c>();
        Id caseId = null;
        system.debug('*****PhoneNumber*****'+PhoneNumber);
        if(PhoneNumber.contains(':')){
           list<String> lstValues = PhoneNumber.split(':');
           PhoneNumber = lstValues[1];
           caseId = lstValues[0];
        }
        system.debug('*****before removing PhoneNumber*****'+PhoneNumber);
        if(PhoneNumber.startsWith('00')) {
            PhoneNumber = PhoneNumber.removeStart('00');
        }else if(PhoneNumber.startsWith('0')) {
            PhoneNumber = PhoneNumber.removeStart('0');
        }
        system.debug('*****before removing PhoneNumber*****'+PhoneNumber);
        system.debug('*****caseId*****'+caseId);
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http(); 
        String user= Label.Damac_CRM_SMS_Service_User_name;
        String passwd= Label.Damac_CRM_SMS_Service_Password;
        //String strSID = Label.Damac_CRM_SMS_Service_Sender_Id;
        String strSID = SMSClass.getSenderName(user, PhoneNumber, false);

        req.setMethod('POST' ); // Method Type
        req.setEndpoint('http://api.smscountry.com/SMSCwebservice_bulk.aspx'); 
        string otpnumber = String.valueOf(Math.random()).substring(8,12);
        string otpMessage = 'Dear customer, your booking of property has been initiated to verify please use OTP:'+ otpnumber;// Server Url
        //req.setHeader('Content-Type', 'application/x-www-form-urlencoded'); // Content TypeAuthorization=' + authorization
        //req.setHeader('user', user);
        //req.setHeader('passwd', passwd);
        otpMessage = GenericUtility.encodeChar(otpMessage);
        req.setBody('user='+ user + '&passwd=' + passwd +'&message=' + otpMessage + '&mobilenumber=' 
        + PhoneNumber + '&sid=' + strSID + '&MTYPE=LNG&DR=Y'); // Request Parameters
        try {
            system.debug('Request Body---'+req.getBody());
            system.debug('Request Header---'+req.getHeader('Authorization'));
            system.debug('Request Header---'+req.getHeader('Content-Type'));
            res = http.send(req);
            system.debug('Response---'+res.getBody());
            if(res.getBody() != null){
                // Parse Response
                //if('SMS message(s) sent' == res.getBody()){
                SMS_History__c smsObj = new SMS_History__c();

                smsObj.Phone_Number__c = PhoneNumber;
                smsObj.Case__c = caseId;
                smsObj.RecordTypeId = otpRecTypeId;
                
                if(String.valueOf(res.getBody()).contains('OK:')){
                    system.debug('Message Send Sucessfully');
                    smsObj.Is_SMS_Sent__c = true;
                    smsObj.sms_Id__c = res.getBody().substringAfter('OK:');
                    OTP__c otp =  new OTP__c();
                    /*
                    lstotp = [Select Phone_Number__c from OTP__c where Phone_Number__c = :PhoneNumber Limit 1];
                    if(lstotp.size() > 0){
                    otp = lstotp[0];
                    }
                    */
                    otp.OTP_Number__c = otpnumber;
                    otp.Phone_Number__c = PhoneNumber;
                    otp.Case__c = caseId;
                    lstotp.add(otp);
                }
                else {
                    smsObj.Description__c = res.getBody();
                }
                
                System.debug('smsLst = ' + smsLst);
                if(!lstotp.isEmpty()){
                    try{
                        upsert lstotp Phone_Number__c;
                        smsObj.OTP__c = lstotp[0].Id;
                        smsLst.add(smsObj);
                    }catch(exception ex){
                        System.debug('DML Exception*****: '+ ex);
                    }
                }
                
                if(smsLst.size() > 0 ) {
                    insert smsLst;
                }
            }
        } catch(Exception e) {
            System.debug('Error***** '+ e);
        }
    }
}