@isTest
private class AP_DisplayPropertyRankingTest{
    @isTest
    static void testGetData() {
        Marketing_Documents__c createMarketingDocObj =new Marketing_Documents__c();
        createMarketingDocObj.Name='test';
        insert createMarketingDocObj;
        
        List<Inventory__c> createInventoryList = new List<Inventory__c>();
        for(Integer i=0; i<10; i++){
            Inventory__c createInventoryObj = new Inventory__c();
            createInventoryObj.status__c='Released';
            createInventoryObj.Property_Status__c='Ready';
            createInventoryObj.Unit_Type__c='Hotel';
            createInventoryObj.property_city__c='Dubai';
            createInventoryObj.Marketing_Name_Doc__c=createMarketingDocObj.id;
            createInventoryObj.property_name__c='DAMAC TOWER';
            createInventoryObj.Bedroom_Type__c='Single';
            createInventoryObj.Floor_Package_Name__c='Test';
            createInventoryList.add(createInventoryObj);
        }
        insert createInventoryList;       
                
        list<Property_Ranking__c> lstPropRank = new list<Property_Ranking__c>();
        for(Integer i=0; i<20; i++){
            Property_Ranking__c objPropCom = new Property_Ranking__c();
            objPropCom.Computed_Score__c = 2+i;
            objPropCom.Ignore_Calculation__c = true;
            objPropCom.Inventory__c = createInventoryList[0].id;
            objPropCom.Rank__c = 10-i;
            lstPropRank.add(objPropCom);
        }
        for(Integer i=0; i<20; i++){
            Property_Ranking__c objPropCom = new Property_Ranking__c();
            objPropCom.Computed_Score__c = 10+i;
            objPropCom.Ignore_Calculation__c = false;
            objPropCom.Inventory__c = createInventoryList[0].id;
            objPropCom.Rank__c = 10+i;
            lstPropRank.add(objPropCom);
        }
        insert lstPropRank;
        
        List<User> userList = [SELECT Id,ContactId, AccountId, Profile.Name,
                                    Account.Broker_Class__c,
                                    Account.recordTypeId, 
                                    Account.Country__c,
                                    Account.Agency_Corporate_Type__c,
                                    Account.Agency_Type__c
                                FROM User 
                                WHERE ID =:UserInfo.getUserId() 
                                LIMIT 1];   

        Announcement__c anncObj = new Announcement__c();
        anncObj.Start_Date__c = system.today().adddays(-3);
        anncObj.End_Date__c = system.today().adddays(3);
        anncObj.Title__c = 'Test';
        anncObj.Active__c = true;
        anncObj.Agency_Type__c = 'Corporate';
        anncObj.Type__c = 'New Launches';
        anncObj.Account__c = userList[0].AccountId;
        anncObj.Description__c = 'test123';
        insert anncObj;

        Announcement__c anncObjOffer = new Announcement__c();
        anncObjOffer.Start_Date__c = system.today().adddays(-3);
        anncObjOffer.End_Date__c = system.today().adddays(3);
        anncObjOffer.Title__c = 'Test';
        anncObjOffer.Active__c = true;
        anncObjOffer.Agency_Type__c = 'Corporate';
        anncObjOffer.Type__c = 'Offer';
        anncObjOffer.Account__c = userList[0].AccountId;
        anncObjOffer.Description__c = 'test123';
        insert anncObjOffer;

        Test.startTest();
            System.currentPageReference().getParameters().put('langCode', 'zh_CN');
            AP_AgentPortalNewLaunches controller;            
            AP_DisplayPropertyRanking obj = new AP_DisplayPropertyRanking(controller);    
            obj.selectedValue = 'Offer';
            AP_DisplayPropertyRanking.AnnouncementsWrapper objWrap = new AP_DisplayPropertyRanking.AnnouncementsWrapper();
            objWrap.image = 'test';    
            objWrap.dateToDisplay = system.today();
            objWrap.type = 'Offer';
            objWrap.name = 'test';
            PageReference pageRef = Page.AgentPortal; 
            Test.setCurrentPage(pageRef);    
            
            obj.strSelectedLanguage = null;
            obj.getDropdownItems();            
            obj.populateSelectedAnnouncements();
        Test.stopTest();
    }

    @isTest
    static void testGetData1() {
        
        

        Announcement__c anncObjOffer = new Announcement__c();
        anncObjOffer.Start_Date__c = system.today().adddays(-3);
        anncObjOffer.End_Date__c = system.today().adddays(3);
        anncObjOffer.Title__c = 'Test';
        anncObjOffer.Active__c = true;
        anncObjOffer.Agency_Type__c = 'Corporate';
        anncObjOffer.Type__c = 'Offer';
        Account objAccount = new Account();
        objAccount.Name = 'abc';
        insert objAccount;
        anncObjOffer.Account__c =objAccount.Id;
        anncObjOffer.Description__c = 'test123';
        insert anncObjOffer;

        Test.startTest();
            System.currentPageReference().getParameters().put('langCode', 'zh_CN');
            AP_AgentPortalNewLaunches controller;            
            AP_DisplayPropertyRanking obj = new AP_DisplayPropertyRanking(controller);    
            obj.selectedValue = 'Offer';
            AP_DisplayPropertyRanking.AnnouncementsWrapper objWrap = new AP_DisplayPropertyRanking.AnnouncementsWrapper();
            objWrap.image = 'test';    
            objWrap.dateToDisplay = system.today();
            objWrap.type = 'Offer';
            objWrap.name = 'test';
            PageReference pageRef = Page.AgentPortal; 
            Test.setCurrentPage(pageRef);    
            
            obj.strSelectedLanguage = null;
            obj.getDropdownItems();            
            obj.populateSelectedAnnouncements();
        Test.stopTest();
    }
}