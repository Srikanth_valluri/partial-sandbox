/************************************************************************************************
 * @Name              : DAMAC_Agents_UnitDocuments_Test
 * @Description       : Test Class for DAMAC_Agents_UnitDocuments 
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         27/03/2020       Created
***********************************************************************************************/

@isTest
public class DAMAC_Agents_UnitDocuments_Test {

    @isTest
    static void testAPI(){
        NSIBPM__Service_Request__c serviceRequest = InitializeSRDataTest.getSerReq('Agent Registration',false,null);
        serviceRequest.ID_Type__c = null;
        serviceRequest.Agency_Type__c = 'Individual';
        serviceRequest.Agency__c = null;
        insert serviceRequest;
        
        Account acc = InitialiseTestData.getCorporateAccount('TestAgency');
        insert acc ;

        Contact con = new Contact();
        con.LastName = 'Corporate Agency test';
        con.AccountId = acc.Id;
        insert con ;
        
        Amendment__c newAmd = new Amendment__c();
        newAmd.First_Name__c = 'Test';
        newAmd.Service_Request__c = serviceRequest.id;
        newAmd.Contact__c = con.Id;
        insert newAmd;
        
        Document_Template__c objDocTemplate1 = new Document_Template__c();
        objDocTemplate1.Code__c = 'PASSPORT_COPY';
        objDocTemplate1.In_Use__c = true;
        objDocTemplate1.Document_Name__c = 'Bank statement';
        objDocTemplate1.SR_Record_Type__c = 'Agent_Registration';
        objDocTemplate1.User_Doc__c = true;
        insert objDocTemplate1;
        
        Unit_Documents__c unitDoc = new Unit_Documents__c();
        unitDoc.Service_Request__c = serviceRequest.Id;
        unitDoc.Document_Name__c= 'Test Doc';
        unitDoc.Sys_Doc_ID__c = 'dfdsfs';
        unitDoc.CV_Id__c = 'testcvid'; 
        unitDoc.Amendment__c = newAmd.Id;
        unitDoc.Status__c = 'Uploaded';
        unitDoc.Document_Template__c =  objDocTemplate1.Id;
        INSERT unitDoc;
 
        
        NSIBPM__Document_Master__c docmaster = new NSIBPM__Document_Master__c();
        docmaster.Name = 'Test DM';
        docmaster.NSIBPM__Code__c = 'Bank Statement';
        docmaster.NSIBPM__Document_Type_Code__c = 'Doc Type';
        insert docmaster;
        
        NSIBPM__SR_Template_Docs__c stempDoc = new NSIBPM__SR_Template_Docs__c();
        stempDoc.NSIBPM__Optional__c = false;
        stempDoc.NSIBPM__Document_Master__c = docmaster.id;
        insert stempDoc;
        
        NSIBPM__SR_Doc__c SRDoc = new NSIBPM__SR_Doc__c();
        SRDoc.Name = 'Test Name';
        SRDoc.NSIBPM__Document_Master__c = docmaster.Id;
        SRDoc.NSIBPM__Service_Request__c = serviceRequest.Id;
        SRDoc.NSIBPM__Status__c = 'Approved';
        SRDoc.Amendment__c = newAmd.Id;
        SRDoc.Agreement_Name__c = 'test';
        SRDoc.NSIBPM__SR_Template_Doc__c = stempDoc.Id;
        insert SRDoc;

        Test.startTest();
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/unitdocuments/' + con.Id;
        request.httpMethod = 'GET';
       // request.addHeader('Content-Type', 'application/json'); 
       // request.requestBody = Blob.valueof(myJSON);
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Agents_UnitDocuments.doGet();
        Test.stopTest();
        
        
    }
}