/*-------------------------------------------------------------------------------------------------
Description: Test class for CallingListGenerator

    ============================================================================================================================
        Version | Date(DD-MM-YYYY) | Last Modified By | Comments
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 11-12-2017       | Lochana Rajput   | 1. Added test method to Calling List creation on update of Bounced Cheque reason
   =============================================================================================================================
*/
@isTest
private class CallingListGeneratorTest {

    @isTest static void test_callingListCreation() {
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Bounced Cheque SR').getRecordTypeId();
        List<Account> lstAccounts = new List<Account>();
        List<Case> lstCases = new List<Case>();
        
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        lstAccounts = TestDataFactory_CRM.createCustomers(1);
        insert lstAccounts;
        for(Account acc : lstAccounts) {
            lstCases.add(TestDataFactory_CRM.createCase(acc.Id, caseRecordTypeId));
        }
        insert lstCases;
        for(Case objCase : lstCases) {
            objCase.Bounced_Reason__c = 'Test reason';
        }
        Test.startTest();
        update lstCases;
        Test.stopTest();
        System.assertEquals(lstCases.size(), [SELECT count() FROM Calling_List__c]);
    }

    @isTest static void test_readyOffPlan() {
    List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Bounced Cheque SR').getRecordTypeId();
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );
        List<Account> lstAccounts = new List<Account>();
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<SR_Booking_Unit__c> lstSRBookingUnits = new List<SR_Booking_Unit__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        List<Case> lstCases = new List<Case>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        lstAccounts = TestDataFactory_CRM.createCustomers(1);
        insert lstAccounts;
        for(Account acc : lstAccounts) {
            lstCases.add(TestDataFactory_CRM.createCase(acc.Id, caseRecordTypeId));
        }
        insert lstCases;
        for(Case objCase : lstCases) {
            objCase.Bounced_Reason__c = 'Test reason';
        }
        Test.startTest();
        update lstCases;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 1);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, lstCases.size());
        lstBookingUnits[0].Registration_ID__c = '74712';
        insert lstBookingUnits;
        for(Integer i=0; i<lstCases.size(); i++) {
            lstSRBookingUnits.addAll(TestDataFactory_CRM.createSRBookingUnis(lstCases[i].Id,new Booking_Unit__c[] {lstBookingUnits[i]}));
        }
        insert lstSRBookingUnits;
        Test.stopTest();
        /*System.assertEquals(false, [SELECT Case__r.Is_EHO_Case__c FROM SR_Booking_Unit__c
                                    WHERE Booking_Unit__c =: lstBookingUnits[0].Id].Case__r.Is_EHO_Case__c);*/
    }
}