@RestResource(urlMapping='/api_SubmitBooking')
global class API_SubmitBooking {
    
    @HttpPost
    global static void doPost(SubmitBookingRequestDTO requestDTO) {
        String srRequestId = requestDTO.serviceRequestId;
        String srQuery = SOQLHelper.getObjectQuery('NSIBPM__Service_Request__c');
        srQuery += ' WHERE Id = :srRequestId';
        NSIBPM__Service_Request__c srRecord = Database.query(srQuery);
        
        ApexPages.StandardController controller = new ApexPages.StandardController(srRecord);
        
        DAMAC_UNIT_BOOKING unitBooking = new DAMAC_UNIT_BOOKING(controller);
        
        unitBooking.srRecord.Mode_of_Payment__c = requestDTO.modeOfPayment;
        unitBooking.srRecord.Token_Amount_AED__c = requestDTO.tokenAmountAED;
        unitBooking.srRecord.Agency__c = requestDTO.selectedAgencyId;
        unitBooking.srRecord.Agent_Name__c = getUserIdFromContactId(requestDTO.selectedAgentId);
        unitBooking.srRecord.RM_Email__c = requestDTO.rmEmail;
        unitBooking.rmEmailID = requestDTO.rmEmail;
        unitBooking.primaryBuyer = requestDTO.primaryBuyer;
        
        for (Selected_Units__c inv : unitBooking.interestedInv) {
            for (Selected_Units__c selectedUnit : requestDTO.unitsList) {
                if (inv.Id == selectedUnit.Id) {
                    inv.SPA_Type__c = selectedUnit.SPA_Type__c;
                    inv.Payment_Plan__c = selectedUnit.Payment_Plan__c;
                    inv.Selected_Campaign__c = selectedUnit.Selected_Campaign__c;
                    inv.Selected_Campaign_Name__c = selectedUnit.Selected_Campaign_Name__c;
                    
                    inv.Selected_Option__c = selectedUnit.Selected_Option__c;
                    inv.Selected_Option_Name__c = selectedUnit.Selected_Option_Name__c;
                    
                    inv.Selected_Promotion__c = selectedUnit.Selected_Promotion__c;
                    inv.Selected_Promotion_Name__c = selectedUnit.Selected_Promotion_Name__c;
                    
                    inv.Selected_Scheme__c = selectedUnit.Selected_Scheme__c;
                    inv.Selected_Scheme_Name__c = selectedUnit.Selected_Scheme_Name__c;
                }
            }
        }
        
        //unitBooking.interestedInv = requestDTO.unitsList;
        unitBooking.submitbooking();
        
        SubmitBookingResponseDTO responseDTO = new SubmitBookingResponseDTO();
        responseDTO.response = unitBooking.customPageMessage;
        responseDTO.isError = unitBooking.isError;
        responseDTO.isSuccess = unitBooking.isSuccess;
        
        RestResponse response = RestContext.response;
        response.addHeader('Content-Type', 'application/json');
        response.responseBody = Blob.valueOf(JSON.serialize(responseDTO));
    }
    
    private static String getUserIdFromContactId(String contactId) {
        String userId = null;
        
        for (User currentUser : [SELECT Id FROM User WHERE ContactId = :contactId]) {
            userId = currentUser.Id;            
        }
        
        return userId;
    }
    
    global class SubmitBookingRequestDTO {
        public String serviceRequestId {get; set;}
        public String modeOfPayment {get; set;}
        public Double tokenAmountAED {get; set;}
        public String selectedAgencyId {get; set;}
        public String selectedAgentId {get; set;}
        public String rmEmail {get; set;}
        public Buyer__c primaryBuyer {get; set;}
        public List<Selected_Units__c> unitsList {get;set;}
    }
    
    global class SubmitBookingResponseDTO {
        public Boolean isError {get; set;}
        public Boolean isSuccess {get; set;}
        public String response {get; set;}
    }
}