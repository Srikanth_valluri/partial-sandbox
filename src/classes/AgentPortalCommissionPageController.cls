/**************************************************************************************************
* Name               : AgentPortalCommissionPageController                                               
* Description        : An apex page controller for  AgentPortalCommission                                            
* Created Date       : Pratiksha Narvekar                                                                       
* Created By         : 05/Sep/2017                                                                
* Last Modified Date :                                                                             
* Last Modified By   :                                                                            
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR                    DATE              Description                                                              
* 1.0         Pratiksha Narvekar       05/Sep/2017        Initial Draft
* 1.1         Monali Nagpure           15/02/2015         Commented unwanted code to avoid view state issue                                                   
**************************************************************************************************/
public with sharing class AgentPortalCommissionPageController {
private Contact contactInfo;
private Id accountId;
// v1.1 Commented By Monali - 15/02/2018
//public List<Agent_Commission__c> agentCommissionList{set;get;} 
//public Decimal totalUnitPrice{set;get;}
//public Decimal totalCommissionAmount{set;get;}
// v1.1 Commented By Monali - 15/02/2018
public Integer fieldSetSize{set;get;}
public static String ImageSource{set;get;}

/**************************************************************************************************
    Method:         DamacCommissionController
    Description:    Constructor executing model of the class 
**************************************************************************************************/

	public AgentPortalCommissionPageController() {

		contactInfo = UtilityQueryManager.getContactInformation();
        //accountId = UtilityQueryManager.getAccountId();
        if(null != contactInfo)
          accountId = contactInfo.AccountID;
        //totalCommissionAmount = 0;
        //totalUnitPrice = 0;


		Date currentDate = System.now().date();
		Date firstDateOfCurrentMonth = Date.newInstance(currentDate.year(), currentDate.month(), 1);
        Date twoMonthsBackDate = firstDateOfCurrentMonth.addMonths(-2);

		//get all unpaid commissions
		//get all paid commissions for past 3 months



		//system.debug(agentCommissionList);
        List<Schema.FieldsetMember> lstMembers = SObjectType.Agent_Commission__c.FieldSets.Agent_Commission_List.getFields();
        system.debug(lstMembers.size());
        fieldSetSize = (lstMembers.size() == 0)?0:lstMembers.size()-1;
	}
    // v1.1 Commented By Monali - 15/02/2018
	/*public List<Agent_Commission__c> getCommissionsList(){
		agentCommissionList = new List<Agent_Commission__c>();
        //agentCommissionList = UtilityQueryManager.getCommission(accountId);
        agentCommissionList = AgentPortalUtilityQueryManager.getCommissionClone(accountId);
        totalCommissionAmount = UtilityQueryManager.totalCommissionAmount;
        totalUnitPrice = UtilityQueryManager.totalUnitPrice;
		return agentCommissionList;
	}*/
    // v1.1 Commented By Monali - 15/02/2018
}