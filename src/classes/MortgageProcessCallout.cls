/**
 * @File Name          : MortgageProcessCallout.cls
 * @Description        : 
 * @Author             : Dipika Rajput
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 6/20/2019, 6:29:18 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 * 
 *==============================================================================
 * 1.0  6/18/2019, 3:34:23 PM       Dipika Rajput           Initial Version
 * 1.1  1/10/2021                   Aishwarya Todkar        Added ME check      
**/
public class MortgageProcessCallout {   

    @testVisible private static final String INITIATE_MORTGAGE  = 'INITIATE_MORTGAGE';
    @testVisible private static final String UPDATE_MORTGAGE_STAUTS  = 'UPDATE_MORTGAGE_STAUTS';

    public static void sendbankDetails(Case mortgageCaseObj,String caseNumberStr,Booking_Unit__c selectedbookingUnit ){
        
        System.debug('insidemortgageCaseObj:::::::'+mortgageCaseObj);
        
        // getting credentials from setting.
        IpmsRestServices__c settings = IpmsRestServices__c.getInstance();
        
        // callout to IPMS for sending case details.
        HttpResponse res = new HttpResponse();
        HttpRequest req = new HttpRequest();
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');
        req.setHeader('Accept-Language', 'en-US');
        req.setTimeout(20000);// timeout in milliseconds
        
         /* If Mortgage initiation process which is initiated from
         mortgage component then call Initiatemortgae method.*/
         System.debug('mortgageCaseObj.Mortgage_Flag__c::'+mortgageCaseObj.Mortgage_Flag__c);
         System.debug('Label.Mortgage_Intitiation_Flag:'+Label.Mortgage_Intitiation_Flag);
        
        if(mortgageCaseObj.Mortgage_Flag__c.trim() == Label.Mortgage_Intitiation_Flag.trim()){
             System.debug('isnide when true:');
            req.setEndpoint(Label.Mortgage_Url+'/webservices/rest/XXDC_PROCESS_SERVICE_WS/process/'+Label.Mortgage_Initiation_Process_Name+'/');
        } 
        
        // Task Closure and Approval process and rejection.
        else if(mortgageCaseObj.Mortgage_Flag__c.trim() == Label.Mortgage_Task_Closure_Flag.trim() || 
                mortgageCaseObj.Mortgage_Flag__c.trim() == Label.Mortgage_Approval_Flag.trim() ||
                mortgageCaseObj.Mortgage_Flag__c.trim() == Label.Mortgage_Rejection_Flag.trim() || 
                mortgageCaseObj.Mortgage_Flag__c.trim().equalsIgnoreCase( 'ME' ) ) {
            req.setEndpoint(Label.Mortgage_Url+'/webservices/rest/XXDC_PROCESS_SERVICE_WS/process/'+Label.Update_Mortgage_Status_Process+'/');
        }
        
        
        req.setMethod('POST');
        req.setHeader(
            'Authorization' ,
            'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(settings.Username__c + ':' + settings.Password__c))
        );   
        
        String requestBody =
            '{' +
              '"PROCESS_Input": {' +
                '"RESTHeader": {' +
                  '"Responsibility": "ONT_ICP_SUPER_USER",' +
                  '"RespApplication": "ONT",' +
                  '"SecurityGroup": "STANDARD",' +
                  '"NLSLanguage": "AMERICAN"' +
                '},' +
                '"InputParameters": {' +
                  '"P_REQUEST_NUMBER": '
        ;

        // Request number is assigned with case number.
        if(( mortgageCaseObj.Mortgage_Flag__c.trim() == Label.Mortgage_Intitiation_Flag.trim() ) && String.isNotBlank(caseNumberStr)){
            requestBody += ('"' +  '2-'+ caseNumberStr + '"');
        }
        else if((mortgageCaseObj.Mortgage_Flag__c.trim() == Label.Mortgage_Task_Closure_Flag.trim() || 
                mortgageCaseObj.Mortgage_Flag__c.trim() == Label.Mortgage_Approval_Flag.trim() ||
                mortgageCaseObj.Mortgage_Flag__c.trim() == Label.Mortgage_Rejection_Flag.trim()) &&
                String.isNotBlank(mortgageCaseObj.caseNumber)){
                
                requestBody += ('"' +  '2-'+ mortgageCaseObj.caseNumber + '"');
        }

        requestBody +=
        ',"P_SOURCE_SYSTEM": "SFDC",' +
              '"P_REQUEST_NAME": ';

       // requestBody += ('"' + GET_INV_INFO_LIST + '"');
       // Request Name in case od initiation
        if(mortgageCaseObj.Mortgage_Flag__c.trim() == Label.Mortgage_Intitiation_Flag.trim()){
            requestBody += ('"' + INITIATE_MORTGAGE + '"');
        }
        
        // Request name in case of task clousre and approval and Rejection
        else if(mortgageCaseObj.Mortgage_Flag__c.trim() == Label.Mortgage_Task_Closure_Flag.trim() || 
                mortgageCaseObj.Mortgage_Flag__c.trim() == Label.Mortgage_Approval_Flag.trim() ||
                mortgageCaseObj.Mortgage_Flag__c.trim() == Label.Mortgage_Rejection_Flag.trim()){
                    requestBody += ('"' + UPDATE_MORTGAGE_STAUTS + '"');
        }
        
        

        requestBody += ',"P_REQUEST_MESSAGE": {';

        requestBody += '"P_REQUEST_MESSAGE_ITEM": [{';
            
        // Param Id is assigned with registration number.
        if( selectedbookingUnit != NULL && selectedbookingUnit.Registration_ID__c != NULL){
                requestBody += '"PARAM_ID":"'+String.valueOf(selectedbookingUnit.Registration_ID__c )+ '",';
        }
        else{
             requestBody += '"PARAM_ID":"'+String.valueOf(mortgageCaseObj.Registration_ID__c ) + '",' ;
        }
        //requestBody += 
            if(mortgageCaseObj.Mortgage_Flag__c.trim() == Label.Mortgage_Intitiation_Flag.trim()){
          
                    // Attribute1 Loan Offer Date Optional for initiation process
          requestBody +='"ATTRIBUTE1":"' + (String.isBlank(FmcUtils.formatAsIpmsDate(mortgageCaseObj.Loan_Offer_Date__c)) ? '' : FmcUtils.formatAsIpmsDate(mortgageCaseObj.Loan_Offer_Date__c) ) + '",' +
          
                     //ATTRIBUTE2 Loan Bank Name Required 
          '"ATTRIBUTE2":"' + (String.isBlank(mortgageCaseObj.Bank_Name_Picklist__c) ? '' : mortgageCaseObj.Bank_Name_Picklist__c) + '",' +
           
          //ATTRIBUTE3 Loan Bank Code Optional
          '"ATTRIBUTE3":"' + (String.isBlank(String.valueOf(mortgageCaseObj.Bank_Code__c ))? '' : String.valueOf(mortgageCaseObj.Bank_Code__c)) + '",' +
           
          //ATTRIBUTE4 Loan Amount Required 
          '"ATTRIBUTE4":"' + (String.isBlank(String.valueOf(mortgageCaseObj.Loan_Amount__c) ) ? '' : String.valueOf(mortgageCaseObj.Loan_Amount__c))+ '",' +

          //ATTRIBUTE5 Loan Agreement Date optional
          '"ATTRIBUTE5":"' + (String.isBlank(FmcUtils.formatAsIpmsDate(mortgageCaseObj.Loan_Agreement_Date__c)) ? '' : FmcUtils.formatAsIpmsDate(mortgageCaseObj.Loan_Agreement_Date__c)) + '",' +
          
          // ATTRIBUTE6 Passing value for  which is submit date of case.
          '"ATTRIBUTE6":"' + (String.isBlank(FmcUtils.formatAsIpmsDate(mortgageCaseObj.Submit_Date__c)) ? '' : FmcUtils.formatAsIpmsDate(mortgageCaseObj.Submit_Date__c)) + '",' +

          // ATTRIBUTE7 Noc Request Date
          '"ATTRIBUTE7":"' + (String.isBlank(FmcUtils.formatAsIpmsDate(mortgageCaseObj.NOC_Request_Date__c)) ? '' :FmcUtils.formatAsIpmsDate(mortgageCaseObj.NOC_Request_Date__c)) + '",' +

          // ATTRIBUTE8 NOC Received Date 
          '"ATTRIBUTE8":"' + (String.isBlank(FmcUtils.formatAsIpmsDate(mortgageCaseObj.NOC_Received_Date__c)) ? '' : FmcUtils.formatAsIpmsDate(mortgageCaseObj.NOC_Received_Date__c)) + '",'+
          
          // ATTRIBUTE9 which may differ like I in case of initiation  
          '"ATTRIBUTE9":"' + (
                           String.isBlank(mortgageCaseObj.Mortgage_Flag__c) ? '' : mortgageCaseObj.Mortgage_Flag__c) + '"' ;
          
        }
              else if(mortgageCaseObj.Mortgage_Flag__c.trim() == Label.Mortgage_Task_Closure_Flag.trim() || 
                  mortgageCaseObj.Mortgage_Flag__c.trim() == Label.Mortgage_Approval_Flag.trim() ||
                  mortgageCaseObj.Mortgage_Flag__c.trim() == Label.Mortgage_Rejection_Flag.trim()){
                   
                      // Attribute1 mortgage flag  in case of update process 
                   requestBody +='"ATTRIBUTE1":"' + (
                         String.isBlank(
                             mortgageCaseObj.Mortgage_Flag__c
                         ) ? '' : mortgageCaseObj.Mortgage_Flag__c) + '"' ;
        }
            

        requestBody += '}]}}}}';

        System.debug('requestBody:::::::::::::final:::'+requestBody);
        req.setbody(requestBody);
        System.debug('requestBody::::::::::::::'+requestBody);
        
        system.debug(' req: body'+ req.getBody());
        system.debug(' req: '+ req);
        Http httpInstance = new Http();
        res = httpInstance.send(req);
        system.debug('res  body: '+ res.getBody());
        system.debug('res  : '+ res  );
    
    }

   
}