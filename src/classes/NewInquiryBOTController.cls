/*
CH: Recordtype Mapping to Pre Inquiry change done on June 13 2018 by Subhash
*/

public class NewInquiryBOTController {

    public Inquiry__c inquiryObj        {set; get;}
    public Boolean saveRecordFlag       {get; set;}
    public Boolean fieldValidation      {get; set;}
    public Boolean isInquiryInsert      {get; set;}
    public String inquirySuccessMsg     {get; set;}
    public String inquiryOwner          {get; set;}
    public String inquiryCampaign       {get; set;}

    public NewInquiryBOTController() {
        inquirySuccessMsg = '';
        inquiryOwner = '';
        inquiryCampaign = '';
        fieldValidation = false;
        saveRecordFlag = false;
        Date todayDate = Date.today();
        inquiryObj = new Inquiry__c();
        inquiryObj.Preferred_Language__c = '';
        inquiryObj.Mobile_CountryCode__c = '';
    }

    public void saveInquiry() {
        String actualNumber = '';
        String trimmedNumber = '';
        if (true) {
            actualNumber = inquiryObj.Mobile_Phone_Encrypt__c;
            if (String.isNotBlank(actualNumber)) {
                integer first = 0;
                integer temp = 0;
                integer offset = 9;
                integer last = actualNumber.length();
                if(actualNumber.length() > 9) {
                    while((first+offset)<last){
                        /*system.debug(trimmedNumber);
                        system.debug('## ' + first + '## ' + offset + '## ' + last);
                        system.debug(actualNumber.substring(first, first+offset));
                        system.debug(Integer.valueOf(actualNumber.substring(first,first+offset)));*/
                        trimmedNumber += String.valueOf(Integer.valueOf(actualNumber.substring(first, first + offset)));
                        first+=offset;
                    }
                    trimmedNumber += String.valueOf(Integer.valueOf(actualNumber.substring(first, last)));
                    inquiryObj.Mobile_Phone_Encrypt__c = trimmedNumber;
                } else{
                    inquiryObj.Mobile_Phone_Encrypt__c = String.valueOf(Integer.valueOf(actualNumber));
                }
            }
            //system.debug(inquiryObj.Mobile_Phone_Encrypt__c);
            inquiryObj.Inquiry_Source__c = 'Digital';

            Id inquiryRecordTypeId =
                //Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(DAMAC_Constants.INQUIRY_RT).getRecordTypeId(); Change added on June 13 2018 by Subhash
                Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(DAMAC_Constants.INQUIRY_PreInq_RT).getRecordTypeId();
            if (inquiryRecordTypeId != null) {
                inquiryObj.RecordTypeId = inquiryRecordTypeId;
            }

            if (String.isNotBlank(inquiryCampaign)) {
                List<Campaign__c> campaignList = [SELECT Id,pcassignment__c FROM Campaign__c WHERE Name = :inquiryCampaign];
                if (!campaignList.isEmpty()) {
                    inquiryObj.Campaign__c = campaignList[0].Id;
                    if( campaignList[0].pcassignment__c ){
                        id recType = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(DAMAC_Constants.INQUIRY_RT).getRecordTypeId();
                        inquiryObj.RecordTypeId  = recType;
                    }
                }
            }

            try{
                system.debug('## inquiryObj BEFORE INSERT >>>>>  ' + inquiryObj);
                insert inquiryObj;
                system.debug('## inquiryObj INSERTED >>>>>  ' + inquiryObj);
                isInquiryInsert = true;
                if (inquiryObj.Id != null) {
                    List<Inquiry__c> inquiryList = [SELECT Id, Name FROM Inquiry__c WHERE Id = :inquiryObj.Id];
                    if (!inquiryList.isEmpty()) {
                        inquirySuccessMsg = '  Inquiry Saved with inquiry number: ' + inquiryList[0].Name;
                    }

                    // 0050Y000001SVjwQAG >  Syed Imran Farooq
                    if (String.isNotBlank(inquiryOwner)) {
                        inquiryObj.OwnerId = inquiryOwner;
                        inquiryObj.Assigned_To__c = inquiryOwner;
                        update inquiryObj;
                        system.debug('## inquiryObj UPDATED >>>>>  ' + inquiryObj);
                    }
                }

                inquiryObj = new Inquiry__c();
                inquiryOwner = '';
                inquiryCampaign = '';
                saveRecordFlag = true;
            } catch(Exception ex){
                system.debug('ex.getStackTraceString >>>> ' +  ex.getStackTraceString());
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error ,ex.getMessage()));
            }
        }
    }

}