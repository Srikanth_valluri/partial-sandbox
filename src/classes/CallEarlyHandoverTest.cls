/******************************************************************************
* Description - Test class developed for CallEarlyHandoverToGetHandoverDetails
*
* Version            Date            Author                    Description
* 1.0                04/02/2018           						Initial Draft
********************************************************************************/
@isTest
private class CallEarlyHandoverTest {
	 static CallEarlyHandoverToGetHandoverDetails.EarlyHandoverMQService1Response obj = new CallEarlyHandoverToGetHandoverDetails.EarlyHandoverMQService1Response();
        
    Static String resp ;
	  @isTest static void TestMethod1(){
       Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,EarlyHandoverMQService1.GetHandoverDetailsResponse_element>();
        EarlyHandoverMQService1.GetHandoverDetailsResponse_element response1 = new EarlyHandoverMQService1.GetHandoverDetailsResponse_element();
        response1.return_x ='{"data":[{"ATTRIBUTE3":null,"ATTRIBUTE10":"Y","ATTRIBUTE2":"0","ATTRIBUTE1":"76726",'+
        		'"ATTRIBUTE14":null,"ATTRIBUTE13":"N","ATTRIBUTE12":null,"ATTRIBUTE11":"Y","ATTRIBUTE9":"0",'+
        		'"ATTRIBUTE8":"Agreement executed by DAMAC","ATTRIBUTE7":"N","ATTRIBUTE6":null,"ATTRIBUTE5":null,'+
        		'"ATTRIBUTE4":"780.49","ATTRIBUTE16":null,"ATTRIBUTE15":null}],"message":"Successfully Fetched Data",'+
        		'"status":"S"}'; 
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
        obj = CallEarlyHandoverToGetHandoverDetails.CallEarlyHandoverMQService1GetHandoverDetails('Registrationid');
     // System.assert(resp != null);  
     Test.stopTest();
  }
	 
}