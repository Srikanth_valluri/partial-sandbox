@isTest(SeeAllData=true)
public class SendAppointmentsToMObileAppTest {
public  testmethod static void method()
    {
        
        Account a=new Account();
        a.name='test';
        insert a;
        Contact c=new Contact();
        c.AccountId=a.id;
        c.LastName='test';
        insert c;
        User u = new User(
     ProfileId = [SELECT Id FROM Profile WHERE Name = 'Customer Community Login User(Use this)'].Id,
     LastName = 'last',
     Email = 'puser000@amamama.com',
     Username = 'puser000@amamama.com' + System.currentTimeMillis(),
     CompanyName = 'TEST',
     Title = 'title',
     Alias = 'alias',
     TimeZoneSidKey = 'America/Los_Angeles',
     EmailEncodingKey = 'UTF-8',
     LanguageLocaleKey = 'en_US',
     LocaleSidKey = 'en_US',
    ContactID=c.id
);
        insert u;
      Calling_List__c cl=new Calling_List__c();
        cl.Account__c=a.id;
        cl.RecordTypeId=Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Appointment Scheduling').getRecordTypeId();
        insert cl;
     SendAppointmentsToMObileApp.SendAppointments(u.Username);
    }
}