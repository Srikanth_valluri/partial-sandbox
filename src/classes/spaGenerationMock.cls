@isTest
/*
* Revision History: 
* Version   Author          Date        Description.
* 1.1       Swapnil Gholap  21/11/2017  Initial Draft
*/

global class spaGenerationMock implements WebServiceMock {    
    
    global void doInvoke(
       Object stub,
       Object request,
       Map<String, Object> response,
       String endpoint,
       String soapAction,
       String requestName,
       String responseNS,
       String responseName,
       String responseType) {
                     
       spaGeneration.processSPAResponse_element   respElement = new spaGeneration.processSPAResponse_element  ();
       //respElement.return_x = '{"data":[{"PROC_STATUS":"E","PROC_MESSAGE":"[EX_ERROR]Process with Id=466228 and Request Id :42069149 Aborting...","ATTRIBUTE3":"SPA","ATTRIBUTE2":null,"ATTRIBUTE1":"https://sftest.deeprootsurface.com/docs/t/IPMS-005681-1513694208030.xlsx","PARAM_ID":"2-005644","ATTRIBUTE4":null}],"MESSAGE_ID":"2-1514964001837","message":"[WARNING] Check the PROC_MESSAGE attribute for Actual Error Message(s), Error Message Count = 1","status":"S"}';
       respElement.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"Aborting...","ATTRIBUTE3":"LAG","ATTRIBUTE2":null,"ATTRIBUTE1":"https://sftest.deeprootsurface.com/docs/t/IPMS-005681-1513694208030.xlsx","PARAM_ID":"2-005644","ATTRIBUTE4":null}],"MESSAGE_ID":"2-1514964001837","message":"[WARNING] Check the PROC_MESSAGE attribute for Actual Error Message(s), Error Message Count = 1","status":"S"}';
       response.put('response_x', respElement);                  
   }
   
}