/**************************************************************************************************
    * Name               : MergePartiesController                                                     *
    * Description        : Controller class to Merge Parties and their child to master Party          *
    * Created Date       : 26/01/2018                                                                 *
    * Created By         : Srikanth                                                                   *
    * ----------------------------------------------------------------------------------------------- *
    * VERSION     AUTHOR            DATE            COMMENTS                                          *
    * 1.0         Srikanth         26/01/2018      Initial Draft.                                     *
    **************************************************************************************************/

    global without sharing class MergePartiesController {

        /*********************************************************************************************
        * @Description : Remote Action                                                               *
        * @Params      : To execute the Batch class to merge child parties with master parties       *
        * @Return      : String                                                                        *
        *********************************************************************************************/ 
        @remoteAction
        global static String mergeAccounts (String masterPartyId, String mergeableParties) {
            try {
                String response;
                Account masterAccount = new Account ();
                List <String> mergeablePartiesList = new List <String> ();
                Boolean portalEnabledMaster = false;
                Boolean portalEnabledMerge = false;
                if (masterPartyId != '' && masterPartyId != null) {
                    if (mergeableParties != '' && mergeableParties != null) {
                        if (mergeableParties.contains (',')) {
                            mergeablePartiesList = mergeableParties.split (',');
                        } else {
                            mergeablePartiesList.add (mergeableParties);
                        }
                        masterAccount = Database.query (' SELECT Party_Id__c FROM Account WHERE Party_Id__c =:masterPartyId ');
                        
                        for (Account acc : [SELECT Party_Id__c, IsCustomerPortal FROM Account WHERE Party_Id__c =:masterPartyId]) {
                            portalEnabledMaster = acc.IsCustomerPortal;
                        }
                        for (Account acc : Database.query (' SELECT Party_Id__c, IsCustomerPortal FROM Account WHERE Party_Id__c IN:mergeablePartiesList ') ){
                            if (acc.IsCustomerPortal) {
                                portalEnabledMerge = acc.IsCustomerPortal;
                            }
                        }
                        system.debug('portalEnabledMaster : '+ portalEnabledMaster);
                        system.debug(' <<<<<<<<< portalEnabledMerge : '+ portalEnabledMerge);
                        if (!portalEnabledMaster && portalEnabledMerge) {
                            system.debug(' <<<<<<<<< in if : ');
                        
                            List<Account> lstAccToUpdate = new List<Account>();
                            for (Account acc : Database.query (' SELECT Party_Id__c, IsCustomerPortal FROM Account WHERE Party_Id__c IN:mergeablePartiesList ') ){
                                if (acc.IsCustomerPortal) {
                                    acc.IsCustomerPortal = false;
                                }
                                lstAccToUpdate.add(acc);
                            }                       
                            system.debug(' <<<<<<< lstAccToUpdate : '+lstAccToUpdate);
                            update lstAccToUpdate;                  
                        
                        
                            List<User> lstNewUser = new List<User>();
                            List<User> lstUpdateUser = new List<User>();
                            
                            //updateAccount(mergeablePartiesList);
                            system.debug(' <<<<<<<<<lstUpdateUser 2 '+lstUpdateUser);
                            
                            //Database.executeBatch (new UpdateMergePartiesAccount (masterAccount, mergeablePartiesList), 2);
                            Database.executeBatch (new MergePartiesBatchHelper (masterAccount, mergeablePartiesList), 2);
                            response = 'Success';
                        } else if(!portalEnabledMaster && !portalEnabledMerge ){
                        
                        
                            //response = Label.Merge_Portal_Enabled;
                            Database.executeBatch (new MergePartiesBatchHelper (masterAccount, mergeablePartiesList), 2);
                            response = 'Success';
                        } else if(portalEnabledMaster && portalEnabledMerge){
                        
                            List<Account> lstAccToUpdate = new List<Account>();
                            for (Account acc : Database.query (' SELECT Party_Id__c, IsCustomerPortal FROM Account WHERE Party_Id__c =:masterPartyId ') ){
                                if (acc.IsCustomerPortal) {
                                    acc.IsCustomerPortal = false;
                                }
                                lstAccToUpdate.add(acc);
                            }                       
                            system.debug(' <<<<<<< lstAccToUpdate : '+lstAccToUpdate);
                            update lstAccToUpdate;
                            
                            List<Account> lstAccToUpdate1 = new List<Account>();
                            for (Account acc : Database.query (' SELECT Party_Id__c, IsCustomerPortal FROM Account WHERE Party_Id__c IN:mergeablePartiesList ') ){
                                if (acc.IsCustomerPortal) {
                                    acc.IsCustomerPortal = false;
                                }
                                lstAccToUpdate1.add(acc);
                            }                       
                            system.debug(' <<<<<<< lstAccToUpdate1 : '+lstAccToUpdate1);
                            update lstAccToUpdate1;     
                        
                            Database.executeBatch (new MergePartiesBatchHelper (masterAccount, mergeablePartiesList), 2);
                            response = 'Success';
                        }else if (portalEnabledMaster && !portalEnabledMerge){
                            List<Account> lstAccToUpdate = new List<Account>();
                            for (Account acc : Database.query (' SELECT Party_Id__c, IsCustomerPortal FROM Account WHERE Party_Id__c =:masterPartyId ') ){
                                if (acc.IsCustomerPortal) {
                                    acc.IsCustomerPortal = false;
                                }
                                lstAccToUpdate.add(acc);
                            }                       
                            system.debug(' <<<<<<< lstAccToUpdate : '+lstAccToUpdate);
                            update lstAccToUpdate;      
                        
                        
                            Database.executeBatch (new MergePartiesBatchHelper (masterAccount, mergeablePartiesList), 2);
                            response = 'Success';
                        }else {
                            
                        }
                        
                    }
                    else {
                        response = 'Merge Accounts are Missing.';    
                    }
                        
                }
                else {
                    response = 'Master Account is Missing';
                }
                return response;
            }
            catch (Exception e) {
                return e.getMessage ();
            }
        
        }

        /*********************************************************************************************
        * @Description : Helper Method                                                               *
        * @Params      : To get all fields from the object                                           *
        * @Return      : String                                                                      *
        *********************************************************************************************/ 
        public static string getAllFields (String objectName) {
            string fields = '';
            sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
            if (objectType == null)
                return fields;
            
            Map <String, Schema.SObjectField> fieldMap = objectType.getDescribe ().fields.getMap ();
            for (Schema.SObjectField sfield : fieldMap.Values()) {
                if (sfield.getDescribe().isUpdateable ())
                    fields += sfield.getDescribe().getName ()+ ', ';
                
            }            
            return fields.removeEnd(', '); 
        }
        /*********************************************************************************************
        * @Description : Helper Method                                                               *
        * @Params      : To get all Accounts based on user search                                    *
        * @Return      : List of Accounts                                                                     *
        *********************************************************************************************/ 
        @remoteAction
        global static List <SObject> getLookupRecords (String objName, String searchableField, 
                                                            String pattern, String masterPartyId, String recordTypeName) {
            try {
                
                List <String> recordTypeIdsList = new List <String> ();
                if (recordTypeName != '' && recordTypeName != NULL) {
                    ID recordTypeId = [Select name From RecordType where sobjecttype = 'Account' and Name =: recordTypeName].Id;
                    recordTypeIdsList.add (recordTypeId);
                }
                else {
                    ID recordTypeId = [Select name From RecordType where sobjecttype = 'Account' and Name = 'Person Account'].Id;
                    ID businessRecordTypeId = [Select name From RecordType where sobjecttype = 'Account' and Name = 'Business Account'].Id;
                    recordTypeIdsList.add (recordTypeId);
                    recordTypeIdsList.add (businessRecordTypeId);
                }
                String query = '';
                List <SObject> objList = NEW List <SObject> ();
                
                if (String.isNotBlank (searchableField)) {
                    query = 'SELECT Id, Name, recordType.Name, '+searchableField+' FROM '+objName+' WHERE '+searchableField+' LIKE \'%'+pattern+'%\' ';
                    if (masterPartyId == '') {
                        masterPartyId = 'NULL';
                    } 
                    else {
                        masterPartyId = '\''+masterPartyId+'\'';
                    }
                    query += ' AND '+searchableField+' != null AND '+searchableField +' != '+masterPartyId
                            +' AND RecordTypeId IN :recordTypeIdsList ORDER BY Name DESC Limit 20';
                    System.Debug (query);
                    objList = Database.query (query);
                }
                return objList;
                
            } catch (Exception e) {
                return new List <SObject> ();
            }
        }
    }