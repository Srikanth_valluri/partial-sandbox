@isTest
public class NeedAnalysis_InventoriesControllerTest{
    testMethod static void propertiesSuggested(){
        
        Account acc = new Account ();
        acc.Name = 'test';
        acc.Agency_Type__c = 'Corporate';
        acc.Vendor_ID__c = '969696';
        acc.RecordTypeId = DamacUtility.getRecordTypeId('Account', 'Corporate Agency');
        insert acc;
        Contact con = new Contact();
        con.FirstName = 'Test';
        con.LastName= 'Test';
        con.Email = 'test@test.com';
        con.AccountId = acc.id;
        con.Agent_Representative__c = true;
        insert con;
        
        Inquiry__c inq = new Inquiry__c ();
        inq.Activity_Counter__c = 101;
        inq.Inquiry_Status__c = 'Active';
        inq.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        inq.Telesales_Executive__c = UserInfo.getUserId();
        inq.Promoter_Name__c = 'Test';
        inq.Last_Name__c = 'Test';
        inq.Our_Quote__c = 20000;
        inq.Price_Expectation__c = 100000;
        inq.Type_of_Property_Interested_in__c = 'Residential';
        inq.No_Of_Bed_Rooms_Availble_in_SF__c = '1';
        insert inq;
        
        Location__c loc = InitializeSRDataTest.createLocation('123', 'Building');
        loc.Property_ID__c = '123';
        insert loc;
        
        Property__c newProperty = new Property__c();
        newProperty.Property_ID__c = 1;
        newProperty.Property_Code__c = 'VIR';
        newProperty.Property_Name__c = 'VIRIDIS @ AKOYA OXYGEN';
        newProperty.District__c = 'AL YUFRAH 2';
        newProperty.AR_Transaction_Type__c  = 'INV VIR';
        newProperty.Penalty_Transaction_Type__c = 'DM-PENALTIES-VIR';
        newProperty.Brokerage_Distribution_Set__c = '11600';
        newProperty.Sales_Commission_Dist_Set__c = '11601';
        newProperty.Currency_Of_Sale__c = 'AED';
        newProperty.Signature_Col_Customer_Stmt__c = 'Front Line Investment Management Co. LLC';
        newProperty.EOI_Enabled__c = true;
        insert newProperty;
        
        Inventory__c invent = new Inventory__c();
        invent.CM_Price_Per_Sqft__c = 20;
        invent.Inventory_ID__c = '1';
        invent.Building_ID__c = '1';
        invent.Floor_ID__c = '1';
        invent.Marketing_Name__c = 'Damac Heights';
        invent.Address_Id__c = '1';
        invent.EOI__C = NULL;
        invent.Price__c = String.valueOf(20000);
        invent.Tagged_to_EOI__c = false;
        invent.Is_Assigned__c = false;
        invent.Property_ID__c = newProperty.Id;
        invent.Property__c = newProperty.Id;
        invent.Status__c = 'Released';
        invent.special_price__c = 100000;
        invent.CurrencyISOCode = 'AED';
        invent.Property_ID__c = '1234';
        invent.Floor_Package_ID__c = '';
        invent.building_location__c = loc.Id;
        invent.property_id__c = newProperty.Id;
        invent.Property_Type__c = 'Residential';
        invent.IPMS_Bedrooms__c = '1';
        insert invent;
        
        PageReference pageRef = Page.NeedAnalysis_Inventories;
        pageRef.getParameters().put('id', String.valueOf(inq.id));
        Test.setCurrentPage(pageRef);
        
        Test.startTest();
            NeedAnalysis_InventoriesController na_InvContrl = new NeedAnalysis_InventoriesController();
        Test.stopTest();
    }
}