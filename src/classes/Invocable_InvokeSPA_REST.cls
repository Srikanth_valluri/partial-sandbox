public class Invocable_InvokeSPA_REST {
    @InvocableMethod
    public static void UpdateBuyer(List<Id> stepId) {
        for (New_Step__c step :[SELECT Id, Service_Request__c FROM New_Step__c WHERE Id IN :stepId]) {
            EvaluateCustomCode(step);
        }
    }

    public static String EvaluateCustomCode(New_Step__c step) {
        String retStr = 'Success';
        List<Id> BookingIds = new List<Id> ();
        try {
            for (Booking_Unit__c BU : [SELECT Id, Booking__c FROM Booking_Unit__c WHERE Status__c != 'Removed' and Booking__r.Deal_SR__c = :step.Service_Request__c]) {
                bookingIds.add(BU.booking__c);
            }
            for (id i : bookingIds) {
                //IPMS_REST_SPA_getProcessID cls = new IPMS_REST_SPA_getProcessID();
                IPMS_REST_SPA_getProcessID.DataForRequest(i);
            }
        }
        catch(Exception ex) {
            retStr = '#### Exception at line number = ' + ex.getLineNumber() + ' , Exception Message = ' + ex.getMessage();
        }
        return retStr;
    }
}