public without sharing class GenerateSOAController {
    
    public static soaResponse getSOADocument(String regId){
        GenerateSOAService.COCDHttpSoap11Endpoint calloutObj = new GenerateSOAService.COCDHttpSoap11Endpoint();
        calloutObj.timeout_x = 120000;
        soaResponse resObj = new soaResponse();

        try{
            String response = calloutObj.GenCustomerStatement( '2-'+String.valueOf( Datetime.now().getTime()), 'DP_SOA', 'SFDC', regId, '', '', regId );
            system.debug('== Statement of Account Response =='+response );
            resObj = (soaResponse)JSON.deserialize(response, GenerateSOAController.soaResponse.class);
            system.debug('soa response === '+ resObj);
        } catch (Exception e ){
            resObj.status = 'Exception';
            resObj.url = '';
            resObj.PROC_MESSAGE = e.getMessage();

           // insert new Error_Log__c(Error_Details__c='error for regid '+ regId +' '+e.getMessage());
        }
        
        return resObj;
    }


    public class soaResponse{
        public String status {get;set;}
        public String url {get;set;}
        public String PROC_MESSAGE ;
        public String PROC_STATUS ;
    }
}