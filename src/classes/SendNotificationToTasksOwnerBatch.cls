public class SendNotificationToTasksOwnerBatch implements Database.Batchable<sObject>{
public list<Task_name_for_send_notification__mdt> tasknamemetadata = new list<Task_name_for_send_notification__mdt>();

   public Database.QueryLocator start(Database.BatchableContext BC){
       list<string> tasksList= new list<string>();
       // tasksList=
        string query='';
       for(Task_name_for_send_notification__mdt tn:[select Active__c,Email_Template_Name__c,Task_Name__c from Task_name_for_send_notification__mdt where Active__c=true]){
           tasksList.add(tn.Task_Name__c);
           tasknamemetadata.add(tn);
       }
       system.debug('tasknamemetadata::'+tasknamemetadata);
       if(tasknamemetadata.size()>0){
        query='Select ID,whatID,ActivityDate,Subject,Process_Name__c,Task_Due_Date__c,OwnerId from Task where ActivityDate=TODAY AND Subject=:tasksList';
        system.debug('query:::'+query);
                      //List<sObject> sobjList = Database.query(query);
                      //system.debug('sobjList:::'+sobjList);
       }
      return Database.getQueryLocator(query);
   }

   public void execute(Database.BatchableContext BC, List<task> scope){
       map<id,id> AssignMap= new map<id,id>();
       map<id,Calling_List__c> CallingListMap= new map<id,Calling_List__c>();
       map<id,list<Calling_List__c>> taskmap= new map<id,list<Calling_List__c>>();
       List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();  
       map<string,string> sendEmailMap = new map<string,string>();
       system.debug('scope:::'+scope.size());
    if(scope.size()>0){
        for(task ts:scope){
            // String myIdPrefix = String.valueOf(ts.whatID).substring(0,3);
            // system.debug('myIdPrefix:::'+myIdPrefix);
            string myIdPrefix=findObjectNameFromRecordIdPrefix(ts.whatID);
            // if(myIdPrefix=='a3A'){
            if(myIdPrefix=='Calling_List__c'){
                AssignMap.put(ts.id,ts.whatID);
                
            }     
            
        }
        system.debug('AssignMap:::'+AssignMap);
        system.debug('AssignMap Values:::'+AssignMap.values());
        if(AssignMap.values().size()>0){
            for(Calling_List__c cl:[select id,name from Calling_List__c where id=:AssignMap.values()]){
                CallingListMap.put(cl.id,cl);
            }
        }
       system.debug('CallingListMap::'+CallingListMap);
        if(CallingListMap.size()>0){
            for(task ts:scope){
                 if(taskmap.containskey(ts.OwnerId) && CallingListMap.containskey(ts.whatID)){
                taskmap.get(ts.OwnerId).add(CallingListMap.get(ts.whatID));
                }else{
                 taskmap.put(ts.OwnerId,new list<Calling_List__c>{CallingListMap.get(ts.whatID)});
                } 
            }
        }
         system.debug('relatedtoIDs:::'+taskmap.keyset().size()+'---'+taskmap);
        if(taskmap.keyset().size()>0){
            
            for(user us:[select id,email from user where id=:taskmap.keyset()]){
                if(!sendEmailMap.containskey(us.id)){
                    sendEmailMap.put(us.id,us.email);
                }
            }
        }
        system.debug('sendEmailMap::'+sendEmailMap);
        if(sendEmailMap.size()>0){
            string htmlBody='';
             string mainbody ='';
             string endstring='';
             mainbody='<div>'+'Dear  ' +'Team'+' , </div>\n \n';
             mainbody+= '<div> <h4>Call back is schedule for below calling list </h4></div>\n \n';
             endstring='<div>Thanks ,</div>\n \n';
             endstring+='SalesForce Team.';
           for(id ids:taskmap.keyset()){
               system.debug('ids::::'+ids);
              if(sendEmailMap.containskey(ids)){
            htmlBody = '<table border="1" style="border-collapse: collapse"><caption>Request Summary Data</caption><tr><th>Call Back Date</th><th>RelatedTo</th></tr>';

                  for(Calling_List__c tss:taskmap.get(ids)){
                            system.debug('test::'+tss);
                            DateTime yourDate = Datetime.now();
                            String dateOutput = yourDate.format('dd-MM-yyyy');
                     String ActivityDate = dateOutput; if(tss.id == null ){ActivityDate = '[Not Provided]';}
                     // String ActivityDate = string.valueof(tss.ActivityDate); if(tss.ActivityDate == null ){ActivityDate = '[Not Provided]';}
                    // String relatedTo = tss.whatID; if(tss.whatID == null){relatedTo = '[Not Provided]';} 
                    String relatedTo = tss.name; if(tss.name == null){relatedTo = '[Not Provided]';}    
                     htmlBody += '<tr><td>' + ActivityDate + '</td><td>' + relatedTo + '</td></tr>';
                    //htmlBody += '<tr><td>' + ActivityDate + '</td><td> <a href="/' + relatedTo + '">' + relatedTo + '</a>'+'</td></tr>';
                  }
                    htmlBody += '</table>';
                     string basebody='';
                   basebody= mainbody+'<div>'+htmlBody+'</div>\n\n'+endstring;
                    system.debug('Total Body::'+basebody);
                    
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                      List<String> lstToaddress = new List<String>();
                     // lstToaddress.add('dipika.rajput@eternussolutions.com');
                     // lstToaddress.add('aditya.kishor@damacgroup.com');
                       lstToaddress.add(sendEmailMap.get(ids));
                      mail.setToAddresses (lstToaddress);
                         mail.setSenderDisplayName('SalesForce Team');
                         mail.setSubject('Scheduled Calling List');
                           mail.setHtmlBody(basebody);
                          mails.add(mail);
                              lstToaddress.clear();
                         
                
                
              }
                     
          }
          if(mails.size()>0){
              try{
                                 Messaging.sendEmail(mails);
                                system.debug('send email success'+mails);
                                system.debug('send email size()'+mails.size());
                                
                              }
                              catch(Exception exp)
                              {
                                system.debug('exp in send email '+exp.getMessage());
                              }
          }
              
        }
    }
   }
   

   public void finish(Database.BatchableContext BC){
   }
    public static String findObjectNameFromRecordIdPrefix(String recordIdOrPrefix) {
            String objectName = '';
            try {            
            String myIdPrefix = String.valueOf(recordIdOrPrefix).substring(0,3);    //Get schema information            
            Map<String, Schema.SObjectType> gd =  Schema.getGlobalDescribe();       //Loop through all the sObject types returned by Schema        
            for(Schema.SObjectType stype : gd.values()){  
               Schema.DescribeSObjectResult r = stype.getDescribe();              
                String prefix = r.getKeyPrefix();              
               // System.debug('Prefix is ' + prefix);                        
                if(prefix!=null && prefix.equals(myIdPrefix)){           
                    objectName = r.getName();                     
                    //System.debug('Object Name! ' + objectName);      
                    break;             
                    }            
                    }        
                    }catch(Exception e){      
                        System.debug(e);         
                        
                    }       
                    return objectName;  
                    } 
}