public class Damac_RM_BookACar {
    
    public Sales_Tours__c trip { get; set; }
    public ID inqId { get; set; }
    public String msg { get; set; }
    public Inquiry__c inq;
    
    public Damac_RM_BookACar() {
        inqId = apexpages.currentpage().getparameters().get('id');
        inq = new Inquiry__c ();
        inq = [SELECT OwnerId, Mobile_Phone__c,Campaign__c FROM Inquiry__c WHERE Id =: inqId ];
        
        trip = new Sales_Tours__c ();
        trip.Inquiry__c = inqId;
        trip.Tour_Type__c = 'Book A Car';
        trip.OwnerId = inq.OwnerId;
        trip.Client_Contact_Number__c = inq.Mobile_Phone__c;
        trip.No_Of_Cars__c = 1;
        msg = '';
    }
    
    public void save () {
        msg = ''; 
          
        try {    
            if (trip.Check_In_Date__c >= DateTime.Now()) {          
                insert trip;
                List<Trip__c> newTripsList = new List<Trip__c>();
                if(trip.No_Of_Cars__c != null && trip.No_Of_Cars__c != 0){
                    Integer count = 1;
                    while (count <= trip.No_Of_Cars__c) {
                        Trip__c newTrip = new Trip__c ();
                        newtrip.Inquiry__c = inqId;
                        newtrip.Start_Time__c = trip.Check_In_Date__c;
                        newTrip.End_Time__c = trip.Check_Out_Date__c;
                        newtrip.Trip_Type__c = 'One way - Pick up';
                        newtrip.Visiting_Sales_Office__c = trip.Meeting_Location_Sales_office__c;
                        newtrip.Trip_Requester_Comments__c = trip.Comments__c;
                        newtrip.Pick_Up_Address__c = trip.Pickup_Location_Fleet__c;
                        newtrip.Drop_Address__c = trip.Drop_off_Location__c;
                        newTrip.Sales_tours__c = trip.Id;
                        newTrip.Trip_Date__c = Date.valueOf(newTrip.Start_Time__c);
    
                        String profileIds = Label.Profile_For_Trip_Owner_Change;
                        if (profileIds.contains (UserInfo.getProfileId())) {
                            newTrip.OwnerId = inq.OwnerId;
                        }
                        newTripsList.add(newTrip);
                        count++;
                    }
                    
                    if(!newTripsList.isEmpty()){
                        insert newTripsList;
                    }
                }
                msg = 'Your request for a car is initiated.';
            } else {
                msg = 'Start Date should be greater than current time.';
            }
        } catch(exception e){
            msg = e.getMessage();
        }     
    }

}