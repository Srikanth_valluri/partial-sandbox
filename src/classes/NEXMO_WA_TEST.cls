@istest
public class NEXMO_WA_TEST{
    @testSetup static void setup() {
        List<NEXMO_WA_Owner_Assignment__c > ownerAssignmentList = new List<NEXMO_WA_Owner_Assignment__c >();
        NEXMO_WA_Owner_Assignment__c ownerAssignment = new NEXMO_WA_Owner_Assignment__c();
        ownerAssignment.Order__c = 1;
        ownerAssignment.Is_Assigned__c = False;
        ownerAssignment.Owner_Id__c = UserInfo.getUserId();
        ownerAssignment.Name = 'Owner 1';
        ownerAssignmentList.add(ownerAssignment) ;
        NEXMO_WA_Owner_Assignment__c ownerAssignment1 = new NEXMO_WA_Owner_Assignment__c();
        ownerAssignment1.Order__c = 2;
        ownerAssignment1.Owner_Id__c = UserInfo.getUserId();
        ownerAssignment1.Name = 'Owner 2';
        ownerAssignment1.Is_Assigned__c = True;
        ownerAssignmentList.add(ownerAssignment1);
        insert ownerAssignmentList;
    }
    static testmethod void test_rest_class_wa_status(){
        Nexmo_Whats_App_Request__c req = new Nexmo_Whats_App_Request__c();
        req.Unique_Key__c = '1234567890';
        req.To_number__c = 1234567890;
        req.Last_MTM_Sent_at__c = system.now();
        insert req;
        
        Nexmo_Whats_App_Message__c msg = new Nexmo_Whats_App_Message__c();
        msg.Nexmo_Whats_App_Request__c = req.id;
        msg.Nexmo_Message_uuid__c = '537832e2-4b39-41c6-ae26-4e1af71bfd90';
        insert msg;
        
        test.starttest();
            // executed
            string reqstr ='{"message_uuid":"537832e2-4b39-41c6-ae26-4e1af71bfd90","to":{"number":"1234567890","type":"whatsapp"},"from":{"number":"447418342136","type":"whatsapp"},"timestamp":"2018-11-01T16:27:51.865Z","status":"read"}';
            NEXMO_RESPONSE_PARSER parsecls = NEXMO_RESPONSE_PARSER.parse(reqstr);
            string jsonstr = JSON.serialize(parsecls);
            RestRequest requ = new RestRequest(); 
            RestResponse res = new RestResponse();
            requ.requestURI = '/nexmohook/status';  
            requ.httpMethod = 'POST';
            requ.requestbody = blob.valueof(jsonstr);
            RestContext.request = requ;
            NEXMO_WA_REST_SERVICES.doPost();
        test.stoptest();
    
    }
    
    static testmethod void test_rest_class_wa_inbound(){    
        
        Nexmo_Whats_App_Request__c req = new Nexmo_Whats_App_Request__c();
        req.Unique_Key__c = '1234567890';
        req.To_number__c = 1234567890;
        req.Last_MTM_Sent_at__c = system.now();
        insert req;
        
        Nexmo_Whats_App_Message__c msg = new Nexmo_Whats_App_Message__c();
        msg.Nexmo_Whats_App_Request__c = req.id;        
        insert msg;
        
        test.starttest();
            string reqstr ='{"to":{"type_Z":"whatsapp","number_Z":"447418342136"},"timestamp":"2018-11-01T16:28:06.044Z","message_uuid":"4539de9f-3d4c-46e0-9a19-e4719f34c7ea","message":{"content":{"type":"text","text":null}},"from_Z":"1234567890","direction":"inbound"}';
            NEXMO_INB_RESPONSE_PARSER parsecls = NEXMO_INB_RESPONSE_PARSER.parse(reqstr);
            string jsonstr = JSON.serialize(parsecls);
            system.debug('&&&&'+jsonstr);
            RestRequest requ = new RestRequest(); 
            RestResponse res = new RestResponse();
            requ.requestURI = '/nexmohook/inbound';  
            requ.httpMethod = 'POST';
            requ.requestbody = blob.valueof(jsonstr);        
            RestContext.request = requ;
            NEXMO_WA_REST_SERVICES.doPost();
        test.stoptest();
    }
    
    static testmethod void test_rest_class_wa_inbound_NewCustomer(){
        test.starttest();
            // executed
            string reqstr ='{"to":{"type_Z":"whatsapp","number_Z":"447418342136"},"timestamp":"2018-11-01T16:28:06.044Z","message_uuid":"4539de9f-3d4c-46e0-9a19-e4719f34c7ea","message":{"content":{"type":"image","image":{ "url" : "www.test.com","caption":"Who"}}},"from":"1234567890","direction":"inbound"}';
            NEXMO_INB_RESPONSE_PARSER parsecls = NEXMO_INB_RESPONSE_PARSER.parse(reqstr);
            NEXMO_WA_REST_SERVICES.getCampaignIdForStandInquiry ('test');
            string jsonstr = JSON.serialize(parsecls);
            system.debug('&&&&'+jsonstr);
            RestRequest requ = new RestRequest(); 
            RestResponse res = new RestResponse();
            requ.requestURI = '/nexmohook/inbound';  
            requ.httpMethod = 'POST';
            requ.requestbody = blob.valueof(jsonstr);
            system.debug('@@@test'+requ.requestbody);
            RestContext.request = requ;
            NEXMO_WA_REST_SERVICES.doPost();
        test.stoptest();
    }
    static testmethod void test_rest_class_wa_inbound_NewCustomer2(){
        test.starttest();
            // executed
            string reqstr ='{"to":{"type_Z":"whatsapp","number_Z":"447418342136"},"timestamp":"2018-11-01T16:28:06.044Z","message_uuid":"4539de9f-3d4c-46e0-9a19-e4719f34c7ea","message":{"content":{"type":"audio","audio":{ "url" : "www.test.com","caption":"Who"}}},"from":"1234567890","direction":"inbound"}';
            NEXMO_INB_RESPONSE_PARSER parsecls = NEXMO_INB_RESPONSE_PARSER.parse(reqstr);
            NEXMO_WA_REST_SERVICES.getCampaignIdForStandInquiry ('test');
            string jsonstr = JSON.serialize(parsecls);
            system.debug('&&&&'+jsonstr);
            RestRequest requ = new RestRequest(); 
            RestResponse res = new RestResponse();
            requ.requestURI = '/nexmohook/inbound';  
            requ.httpMethod = 'POST';
            requ.requestbody = blob.valueof(jsonstr);
            system.debug('@@@test'+requ.requestbody);
            RestContext.request = requ;
            NEXMO_WA_REST_SERVICES.doPost();
        test.stoptest();
    }


}