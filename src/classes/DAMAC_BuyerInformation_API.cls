/***************************************************************************************************
 * @Name              : DAMAC_BuyerInformation_API
 * @Test Class Name   : DAMAC_BuyerInformation_API_Test
 * @Description       : Listing Buyer Details based on param, bookingId
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         09/07/2020       Created
****************************************************************************************************/
@RestResource(urlMapping = '/buyerInformation/*')
global class DAMAC_BuyerInformation_API {

    @HttpGet
    global static void getResults(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String bookingId = req.params.get('bookingId');
        System.debug('bookingId>>>' + bookingId);
        User currentUser = [SELECT ContactID FROM User WHERE ID = :Userinfo.getUserId()];
        Contact con = new Contact();
        /*  try {
                con = [SELECT Name FROM Contact WHERE ID =: currentUser.contactId];
            } catch (Exception e) {}*/
        String jsonBody = '';
        String primaryBuyerBody = '';
        String jointBuyerBody = '';
        String pbUnitDocumentJsonBody = '';
        String jbUnitDocumentJsonBody = '';
        try {
          if (req.requestURI == '/buyerInformation') {
            Map < ID,String > primaryBuyerInfoMap = new Map < ID,String > ();
            Map < ID,String > jointBuyerInfoMap = new Map < ID,String > ();
            Map < ID,String > unitDocumentsMap = new Map < ID,String > ();
            Map < ID,String > jointUnitDocumentsMap = new Map < ID,String > ();
    
            Map < String,List < Unit_Documents__c >> jointUnitDocumentsListMap = new Map < String,List < Unit_Documents__c >> ();
    
            List < Buyer__c > primaryBuyerList = new List < Buyer__c > ();
            List < Buyer__c > jointBuyerList = new List < Buyer__c > ();
            List < NSIBPM__Service_Request__c > srList = new List < NSIBPM__Service_Request__c > ();
            List < Unit_Documents__c > pbUnitDocumentsList;
            List < Unit_Documents__c > jbUnitDocumentsList;
    
            List < Id > jointBuyerId = new List < Id > ();
    
            Id srId;
            jsonBody = '{';
            String previewURL;
            String jbUnitDocumentBody = '';
            NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
            list<Attachment> srAttachments = new list<attachment>();
            if (bookingId != '' && bookingId != NULL) {
    
                primaryBuyerList = [SELECT Id, Buyer_Type__c, Title__c, First_Name__c, Last_Name__c, DOB__c, Passport_Expiry__c, Gender__c, Email__c, 
                                         Phone_Country_Code__c, Phone__c, Country__c, City__c, Nationality__c, EID_No__c, Damac_Agent_Action_Type__c, 
                                         Address_Line_1__c, Address_Line_2__c, Address_Line_3__c, Address_Line_4__c, Passport_Number__c, Place_of_Issue__c, 
                                         Deal_Status__c              
                                         FROM Buyer__c
                                         WHERE Primary_Buyer__c = true AND
                                         Booking__c = :bookingId LIMIT 1];
    
                jointBuyerList = [SELECT Id, Buyer_Type__c, Title__c, First_Name__c, Last_Name__c, DOB__c, Passport_Expiry__c, Gender__c, 
                                       Email__c, Phone_Country_Code__c, Phone__c, Country__c, City__c, Nationality__c, EID_No__c, Address_Line_1__c, 
                                       Address_Line_2__c, Address_Line_3__c, Address_Line_4__c, Passport_Number__c, Place_of_Issue__c, Deal_Status__c, 
                                       Damac_Agent_Action_Type__c 
                                       FROM Buyer__c
                                       WHERE Primary_Buyer__c = false
                                       AND
                                       Booking__c = :bookingId];
    
                srId = [SELECT Deal_SR__c FROM Booking__c WHERE Id = :bookingId].Deal_SR__c;
                sr = [Select id, Token_Attachment_Name__c, Reservation_Form_Attachment_Name__c 
                                                from NSIBPM__Service_Request__c 
                                                where id=: srId];
                srAttachments = [Select id, name, contentType, BodyLength  
                                              from Attachment 
                                              where parentId =: sr.Id
                                              and (name =: sr.Token_Attachment_Name__c or name =: sr.Reservation_Form_Attachment_Name__c)];
                String primaryBuyerFirstName = '%' + primaryBuyerList[0].First_Name__c + '%';              
                for (Buyer__c Buyer: jointBuyerList) {
                    jointBuyerId.add(Buyer.Id);
    
                }
                system.debug('jointBuyerId>>>' + jointBuyerId);
                pbUnitDocumentsList = [SELECT Id, Document_Template__c, Document_Template__r.Name, Sys_Doc_ID__c, CV_Id__c, Doc_Type__c, Buyer__c, 
                                            Document_Name__c, Service_Request__c, Preview_Download_Final__c, Status__c,              
                                            (SELECT Id, Name, ContentType, BodyLength FROM Attachments)
                                            FROM Unit_Documents__c
                                            WHERE Service_Request__c = : srId AND Buyer__c =: primaryBuyerList[0].Id];
                                         /*    WHERE (Service_Request__c = :srId OR Buyer__c=: primaryBuyerList[0].Id) AND
                                             (Document_Name__c = 'Reservation Copy' OR Document_Name__c = 'Token Copy' OR Document_Name__c != '')];*/
                /*List<Unit_Documents__c> ReservationAndTokenDocumentList = [SELECT Id, Document_Template__c, Document_Template__r.Name, Sys_Doc_ID__c, CV_Id__c, Doc_Type__c, Buyer__c, 
                                            Document_Name__c, Service_Request__c, Preview_Download_Final__c, Status__c,              
                                            (SELECT Id, Name, ContentType, BodyLength FROM Attachments),
                                            (SELECT id, contentDocument.FileType, ContentDocument.Title, ContentDocument.ContentSize, ContentDocumentId FROM ContentDocumentLinks)
                                            FROM Unit_Documents__c
                                            WHERE Service_Request__c = :sr.Id  AND 
                                            ( Document_Name__c = 'Reservation Copy'
                                              OR Document_Name__c = 'Token Copy')]; 
              
               pbUnitDocumentsList.addAll(ReservationAndTokenDocumentList);
               */

                                               
               jbUnitDocumentsList = [SELECT Id, Document_Template__c, Document_Template__r.Name, Sys_Doc_ID__c, CV_Id__c, Doc_Type__c, Buyer__c, 
                                            Document_Name__c, Service_Request__c, Preview_Download_Final__c, Status__c,                 
                                            (SELECT Id, Name, ContentType, BodyLength FROM Attachments)
                                            FROM Unit_Documents__c
                                            WHERE Service_Request__c = :srId
                                            AND Buyer__c IN: jointBuyerId 
                                           /* AND ((NOT Document_Name__c LIKE: primaryBuyerFirstName)
                                            AND (Document_Name__c != 'Reservation Copy') AND(Document_Name__c != 'Token Copy'))];*/
                                            AND ((Document_Name__c != 'Reservation Copy') AND (Document_Name__c != 'Token Copy'))];
                                            
                system.debug('jbUnitDocumentsList >>' + jbUnitDocumentsList);
                for (Unit_Documents__c doc: jbUnitDocumentsList) {
                    List < Unit_Documents__c > docList = new List < Unit_Documents__c > ();
                    if (jointUnitDocumentsListMap.containsKey(doc.Buyer__c)) {
                        docList = jointUnitDocumentsListMap.get(doc.Buyer__c);
                    }
                    docList.add(doc);
                    jointUnitDocumentsListMap.put(doc.Buyer__c, docList);
                }
            }
            system.debug('jointUnitDocumentsListMap: ' + jointUnitDocumentsListMap);
    
            // To get the primary buyer details based on the booking
            for (Buyer__c buyer: primaryBuyerList) {
              String body = '';
    
              //Id
              body += '"Buyer_Id": "' + buyer.Id + '"';
              //Buyer Type
              if (buyer.Buyer_Type__c != NULL) {
                body += ', ' + '"Buyer_Type__c": "' + buyer.Buyer_Type__c + '"';
              }
              else {
                body += ', ' + '"Buyer_Type__c": ' + null;
              }
    
              //Title
              if (buyer.Title__c != NULL) {
                body += ', ' + '"Title__c": "' + buyer.Title__c + '"';
              }
              else {
                body += ', ' + '"Title__c": ' + null;
              }
    
              //First_Name
              if (buyer.First_Name__c != NULL) {
                body += ', ' + '"First_Name__c": "' + buyer.First_Name__c + '"';
              }
              else {
                body += ', ' + '"First_Name__c": ' + null;
              }
              //Last_Name
              if (buyer.Last_Name__c != NULL) {
                body += ', ' + '"Last_Name__c": "' + buyer.Last_Name__c + '"';
              }
              else {
                body += ', ' + '"Last_Name__c": ' + null;
              }
              //Gender
              if (buyer.Gender__c != NULL) {
                body += ', ' + '"Gender__c": "' + buyer.Gender__c + '"';
              }
              else {
                body += ', ' + '"Gender__c": ' + null;
              }
              //Email
              if (buyer.Email__c != NULL) {
                body += ', ' + '"Email__c": "' + buyer.Email__c + '"';
              }
              else {
                body += ', ' + '"Email__c": ' + null;
              }
    
              //DOB
              if (buyer.DOB__c != NULL) {
                body += ', ' + '"DOB__c": "' + buyer.DOB__c + '"';
              }
              else {
                body += ', ' + '"DOB__c": ' + null;
              }
              //Deal_Status
              if (buyer.Deal_Status__c != NULL) {
                body += ', ' + '"Deal_Status__c": "' + buyer.Deal_Status__c + '"';
              }
              else {
                body += ', ' + '"Deal_Status__c": ' + null;
              }
              //Country_Code
              if (buyer.Phone_Country_Code__c != NULL) {
                body += ', ' + '"Phone_Country_Code__c": "' + buyer.Phone_Country_Code__c + '"';
              }
              else {
                body += ', ' + '"Phone_Country_Code__c": ' + null;
              }
              //Mobile_Number
              if (buyer.Phone__c != NULL) {
                body += ', ' + '"Phone__c": "' + buyer.Phone__c + '"';
              }
              else {
                body += ', ' + '"Phone__c": ' + null;
              }
              //Country
              if (buyer.Country__c != NULL) {
                body += ', ' + '"Country__c": "' + buyer.Country__c + '"';
              }
              else {
                body += ', ' + '"Country__c": ' + null;
              }
              //City
              if (buyer.City__c != NULL) {
                body += ', ' + '"City__c": "' + buyer.City__c + '"';
              }
              else {
                body += ', ' + '"City__c": ' + null;
              }
              //Nationality
              if (buyer.Nationality__c != NULL) {
                body += ', ' + '"Nationality__c": "' + buyer.Nationality__c + '"';
              }
              else {
                body += ', ' + '"Nationality__c": ' + null;
              }
              //EID_No
              if (buyer.EID_No__c != NULL) {
                body += ', ' + '"EID_No__c": "' + buyer.EID_No__c + '"';
              }
              else {
                body += ', ' + '"EID_No__c": ' + null;
              }
              //Address_Line_1
              if (buyer.Address_Line_1__c != NULL) {
                body += ', ' + '"Address_Line_1__c": "' + buyer.Address_Line_1__c + '"';
              }
              else {
                body += ', ' + '"Address_Line_1__c": ' + null;
              }
              //Address_Line_2
              if (buyer.Address_Line_2__c != NULL) {
                body += ', ' + '"Address_Line_2__c": "' + buyer.Address_Line_2__c + '"';
              }
              else {
                body += ', ' + '"Address_Line_2__c": ' + null;
              }
              //Address_Line_3
              if (buyer.Address_Line_3__c != NULL) {
                body += ', ' + '"Address_Line_3__c": "' + buyer.Address_Line_3__c + '"';
              }
              else {
                body += ', ' + '"Address_Line_3__c": ' + null;
              }
              //Address_Line_4
              if (buyer.Address_Line_4__c != NULL) {
                body += ', ' + '"Address_Line_4__c": "' + buyer.Address_Line_4__c + '"';
              }
              else {
                body += ', ' + '"Address_Line_4__c": ' + null;
              }
              //Passport_Number
              if (buyer.Passport_Number__c != NULL) {
                body += ', ' + '"Passport_Number__c": "' + buyer.Passport_Number__c + '"';
              }
              else {
                body += ', ' + '"Passport_Number__c": ' + null;
              }
              //Passport_Expiry__c                      
              if (buyer.Passport_Expiry__c != NULL) {
                body += ', ' + '"Passport_Expiry__c": "' + buyer.Passport_Expiry__c + '"';
              }
              else {
                body += ', ' + '"Passport_Expiry__c": ' + null;
              }
              //Place_of_Issue
              if (buyer.Place_of_Issue__c != NULL) {
                body += ', ' + '"Place_of_Issue__c": "' + buyer.Place_of_Issue__c + '"';
              }
              else {
                body += ', ' + '"Place_of_Issue__c": ' + null;
              }
    
              // body += '}'; 
    
              primaryBuyerInfoMap.put(buyer.Id, body);
              System.Debug('primaryBuyerInfoMap >>>' + primaryBuyerInfoMap.Values());
    
            }
    
            // To get the joint buyer details based on the booking
            for (Buyer__c buyer: jointBuyerList) {
    
              String body = '';
    
              //Id
              body += '"Buyer_Id": "' + buyer.Id + '"';
              //Buyer Type
              if (buyer.Buyer_Type__c != NULL) {
                body += ', ' + '"Buyer_Type__c": "' + buyer.Buyer_Type__c + '"';
              }
              else {
                body += ', ' + '"Buyer_Type__c": ' + null;
              }
    
              //Title
              if (buyer.Title__c != NULL) {
                body += ', ' + '"Title__c": "' + buyer.Title__c + '"';
              }
              else {
                body += ', ' + '"Title__c": ' + null;
              }
    
              //First_Name
              if (buyer.First_Name__c != NULL) {
                body += ', ' + '"First_Name__c": "' + buyer.First_Name__c + '"';
              }
              else {
                body += ', ' + '"First_Name__c": ' + null;
              }
              //Last_Name
              if (buyer.Last_Name__c != NULL) {
                body += ', ' + '"Last_Name__c": "' + buyer.Last_Name__c + '"';
              }
              else {
                body += ', ' + '"Last_Name__c": ' + null;
              }
              //Gender
              if (buyer.Gender__c != NULL) {
                body += ', ' + '"Gender__c": "' + buyer.Gender__c + '"';
              }
              else {
                body += ', ' + '"Gender__c": ' + null;
              }
              //Email
              if (buyer.Email__c != NULL) {
                body += ', ' + '"Email__c": "' + buyer.Email__c + '"';
              }
              else {
                body += ', ' + '"Email__c": ' + null;
              }
              //DOB
              if (buyer.DOB__c != NULL) {
                body += ', ' + '"DOB__c": "' + buyer.DOB__c + '"';
              }
              else {
                body += ', ' + '"DOB__c": ' + null;
              }
              //Deal_Status
              if (buyer.Deal_Status__c != NULL) {
                body += ', ' + '"Deal_Status__c": "' + buyer.Deal_Status__c + '"';
              }
              else {
                body += ', ' + '"Deal_Status__c": ' + null;
              }
              //Country_Code
              if (buyer.Phone_Country_Code__c != NULL) {
                body += ', ' + '"Phone_Country_Code__c": "' + buyer.Phone_Country_Code__c + '"';
              }
              else {
                body += ', ' + '"Phone_Country_Code__c": ' + null;
              }
              //Mobile_Number
              if (buyer.Phone__c != NULL) {
                body += ', ' + '"Phone__c": "' + buyer.Phone__c + '"';
              }
              else {
                body += ', ' + '"Phone__c": ' + null;
              }
              //Country
              if (buyer.Country__c != NULL) {
                body += ', ' + '"Country__c": "' + buyer.Country__c + '"';
              }
              else {
                body += ', ' + '"Country__c": ' + null;
              }
              //City
              if (buyer.City__c != NULL) {
                body += ', ' + '"City__c": "' + buyer.City__c + '"';
              }
              else {
                body += ', ' + '"City__c": ' + null;
              }
              //Nationality
              if (buyer.Nationality__c != NULL) {
                body += ', ' + '"Nationality__c": "' + buyer.Nationality__c + '"';
              }
              else {
                body += ', ' + '"Nationality__c": ' + null;
              }
              //EID_No
              if (buyer.EID_No__c != NULL) {
                body += ', ' + '"EID_No__c": "' + buyer.EID_No__c + '"';
              }
              else {
                body += ', ' + '"EID_No__c": ' + null;
              }
              //Address_Line_1
              if (buyer.Address_Line_1__c != NULL) {
                body += ', ' + '"Address_Line_1__c": "' + buyer.Address_Line_1__c + '"';
              }
              else {
                body += ', ' + '"Address_Line_1__c": ' + null;
              }
              //Address_Line_2
              if (buyer.Address_Line_2__c != NULL) {
                body += ', ' + '"Address_Line_2__c": "' + buyer.Address_Line_2__c + '"';
              }
              else {
                body += ', ' + '"Address_Line_2__c": ' + null;
              }
              //Address_Line_3
              if (buyer.Address_Line_3__c != NULL) {
                body += ', ' + '"Address_Line_3__c": "' + buyer.Address_Line_3__c + '"';
              }
              else {
                body += ', ' + '"Address_Line_3__c": ' + null;
              }
              //Address_Line_4
              if (buyer.Address_Line_4__c != NULL) {
                body += ', ' + '"Address_Line_4__c": "' + buyer.Address_Line_4__c + '"';
              }
              else {
                body += ', ' + '"Address_Line_4__c": ' + null;
              }
              //Passport_Number
              if (buyer.Passport_Number__c != NULL) {
                body += ', ' + '"Passport_Number__c": "' + buyer.Passport_Number__c + '"';
              }
              else {
                body += ', ' + '"Passport_Number__c": ' + null;
              }
              //Passport_Expiry__c                      
              if (buyer.Passport_Expiry__c != NULL) {
                body += ', ' + '"Passport_Expiry__c": "' + buyer.Passport_Expiry__c + '"';
              }
              else {
                body += ', ' + '"Passport_Expiry__c": ' + null;
              }
              //Place_of_Issue Damac_Agent_Action_Type__c
              if (buyer.Place_of_Issue__c != NULL) {
                body += ', ' + '"Place_of_Issue__c": "' + buyer.Place_of_Issue__c + '"';
              }
              else {
                body += ', ' + '"Place_of_Issue__c": ' + null;
              }
              //Damac_Agent_Action_Type__c
              if (buyer.Damac_Agent_Action_Type__c != NULL) {
                body += ', ' + '"Damac_Agent_Action_Type__c": "' + buyer.Damac_Agent_Action_Type__c + '"';
              }
              else {
                body += ', ' + '"Damac_Agent_Action_Type__c": ' + null;
              }
    
              if (jointUnitDocumentsListMap.containsKey(buyer.Id)) {
    
                body += ', ' + '"Unit_Documents": [';
                jbUnitDocumentBody = '';
    
                for (Unit_Documents__c unitDocs: jointUnitDocumentsListMap.get(buyer.Id)) {
    
                  // /services/data/v49.0/sobjects/Attachment/00P1w000003hP8i/body
    
                  if (unitDocs.Sys_Doc_ID__c != null && unitDocs.Sys_Doc_ID__c != '') {
    
                    previewURL = URL.getOrgDomainUrl().toExternalForm() + '/services/data/v49.0/sobjects/Attachment/' + unitDocs.Sys_Doc_ID__c + '/body';
    
                  }
    
                  jbUnitDocumentBody += '{';
                  //Document_Id
                  // if(unitDocs.Buyer__c != NULL) {    
                  jbUnitDocumentBody += '"Document_Id": "' + unitDocs.Id + '"';
                  /* }
                                else{                            
                                    jbUnitDocumentBody +=  '"Document_Id": ' + null;
                                }*/
    
                  //Document_Name
                  if (unitDocs.Document_Name__c != NULL) {
                    jbUnitDocumentBody += ', ' + '"Document_Name__c": "' + unitDocs.Document_Name__c + '"';
                  }
                  else {
                    jbUnitDocumentBody += ', ' + '"Document_Name__c": ' + null;
                  }
    
                  //Preview_Download_Final  
                  if (unitDocs.Preview_Download_Final__c != NULL) {
                    jbUnitDocumentBody += ', ' + '"Preview_Download_Final__c": "' + previewURL + '"';
                  }
                  else {
                    jbUnitDocumentBody += ', ' + '"Preview_Download_Final__c": ' + null;
                  }
    
                  //Status 
                  if (unitDocs.Status__c != NULL) {
                    jbUnitDocumentBody += ', ' + '"Status__c": "' + unitDocs.Status__c + '"';
                  }
                  else {
                    jbUnitDocumentBody += ', ' + '"Status__c": ' + null;
                  }
    
                  if (unitDocs.attachments.size() > 0) {
    
                    jbUnitDocumentBody += ', ' + '"File_Id": "' + unitDocs.attachments[0].Id + '"';
                    //File_Name 
                    if (unitDocs.attachments[0].Name != NULL) {
                      jbUnitDocumentBody += ', ' + '"File_Name": "' + unitDocs.attachments[0].Name + '"';
                    }
                    else {
                      jbUnitDocumentBody += ', ' + '"File_Name": ' + null;
                    }
                    //File_Type 
                    if (unitDocs.attachments[0].ContentType != NULL) {
                      jbUnitDocumentBody += ', ' + '"File_Type": "' + unitDocs.attachments[0].ContentType + '"';
                    }
                    else {
                      jbUnitDocumentBody += ', ' + '"File_Type": ' + null;
                    }
                    //File_size 
                    if (unitDocs.attachments[0].BodyLength != NULL) {
                      jbUnitDocumentBody += ', ' + '"File_Size": "' + unitDocs.attachments[0].BodyLength + '"';
                    }
                    else {
                      jbUnitDocumentBody += ', ' + '"File_Size": ' + null;
                    }
                  }
    
                  else {
    
                    jbUnitDocumentBody += ', ' + '"File_Id": ' + null +
                                          ', ' + '"File_Name": ' + null +
                                          ', ' + '"File_Type": ' + null +
                                          ', ' + '"File_Size": ' + null;
                  }
    
                  jbUnitDocumentBody += '},';
    
                }
    
                jbUnitDocumentBody = jbUnitDocumentBody.removeEnd(',');
                body += jbUnitDocumentBody + ']';
    
              }
    
              body += '}';
    
              jointBuyerInfoMap.put(buyer.Id, body);
              System.Debug('jointBuyerInfoMap >>>' + jointBuyerInfoMap.Values());
    
            }
    
            if (!primaryBuyerInfoMap.isEmpty()) {
              // adding Inquiry details to JSON
              for (String body: primaryBuyerInfoMap.values()) {
                if (primaryBuyerBody != '') {
                  primaryBuyerBody += ', {' + body;
                }
                else {
                  primaryBuyerBody += '"Primary_Buyer": {' + body;
                }
              }
    
            }
    
            //Adding Unit Documents to primary buyer
            for (Unit_Documents__c unitDocs: pbUnitDocumentsList) {
    
              String pbUnitDocumentBody = '';
    
              // /services/data/v49.0/sobjects/Attachment/00P1w000003hP8i/body
    
              if (unitDocs.Sys_Doc_ID__c != null && unitDocs.Sys_Doc_ID__c != '') {
    
                previewURL = URL.getOrgDomainUrl().toExternalForm() + '/services/data/v49.0/sobjects/Attachment/' + unitDocs.Sys_Doc_ID__c + '/body';
    
              }
    
              //Document_Id
    
              pbUnitDocumentBody += '"Document_Id": "' + unitDocs.Id + '"';
    
              //Document_Name
              if (unitDocs.Document_Name__c != NULL) {
                pbUnitDocumentBody += ', ' + '"Document_Name__c": "' + unitDocs.Document_Name__c + '"';
              }
              else {
                pbUnitDocumentBody += ', ' + '"Document_Name__c": ' + null;
              }
    
              //Preview_Download_Final  
              if (unitDocs.Preview_Download_Final__c != NULL) {
                pbUnitDocumentBody += ', ' + '"Preview_Download_Final__c": "' + previewURL + '"';
              }
              else {
                pbUnitDocumentBody += ', ' + '"Preview_Download_Final__c": ' + null;
              }
    
              //Status 
              if (unitDocs.Status__c != NULL) {
                pbUnitDocumentBody += ', ' + '"Status__c": "' + unitDocs.Status__c + '"';
              }
              else {
                pbUnitDocumentBody += ', ' + '"Status__c": ' + null;
              }
              if (unitDocs.attachments.size() > 0) {
                //File_Id
                pbUnitDocumentBody += ', ' + '"File_Id": "' + unitDocs.attachments[0].Id + '"';
                //File_Name 
                if (unitDocs.attachments[0].Name != NULL) {
                  pbUnitDocumentBody += ', ' + '"File_Name": "' + unitDocs.attachments[0].Name + '"';
                }
                else {
                  pbUnitDocumentBody += ', ' + '"File_Name": ' + null;
                }
                //File_Type 
                if (unitDocs.attachments[0].ContentType != NULL) {
                  pbUnitDocumentBody += ', ' + '"File_Type": "' + unitDocs.attachments[0].ContentType + '"';
                }
                else {
                  pbUnitDocumentBody += ', ' + '"File_Type": ' + null;
                }
                //File_size 
                if (unitDocs.attachments[0].BodyLength != NULL) {
                  pbUnitDocumentBody += ', ' + '"File_Size": "' + unitDocs.attachments[0].BodyLength + '"';
                }
                else {
                  pbUnitDocumentBody += ', ' + '"File_Size": ' + null;
                }
    
              }
                            
              else {
                  pbUnitDocumentBody += ', ' + '"File_Id": ' + null + 
                                      ', ' + '"File_Name": ' + null +
                                      ', '+ '"File_Type": ' + null +
                                      ', '+ '"File_Size": ' + null;
              }
    
              pbUnitDocumentBody += '}';
    
              unitDocumentsMap.put(unitDocs.Id, pbUnitDocumentBody);
    
            }
            //Adding Token and Reservation
            for(Attachment att : srAttachments){
              String pbUnitDocumentBody = '';
              string previewURL1 = URL.getOrgDomainUrl().toExternalForm() + '/services/data/v49.0/sobjects/Attachment/' + att.Id + '/body';
              string docName = '';
              if(att.Name == sr.Token_Attachment_Name__c)
                docName = 'Token Copy';
              else if(att.Name == sr.Reservation_Form_Attachment_Name__c)
                docName = 'Reservation Copy';
              else
                docName = 'Others';

              pbUnitDocumentBody += '"Document_Id": "' + att.Id + '"';
              pbUnitDocumentBody += ', ' + '"Document_Name__c": "' + docName + '"';
              pbUnitDocumentBody += ', ' + '"Preview_Download_Final__c": "' + previewURL1 + '"';
              pbUnitDocumentBody += ', ' + '"Status__c": "Uploaded"';
              pbUnitDocumentBody += ', ' + '"File_Id": "' + att.Id + '"';
              pbUnitDocumentBody += ', ' + '"File_Name": "' + docName + '"';
              pbUnitDocumentBody += ', ' + '"File_Type": "' + att.ContentType + '"';
              pbUnitDocumentBody += ', ' + '"File_Size": "' + att.BodyLength + '"}';              
              unitDocumentsMap.put(att.Id, pbUnitDocumentBody);
            }
            
            if (!unitDocumentsMap.isEmpty()) {
              for (String body: unitDocumentsMap.values()) {
                if (pbUnitDocumentJsonBody != '') {
                  pbUnitDocumentJsonBody += ', {' + body;
                }
                else {
                  pbUnitDocumentJsonBody += '"Unit_Documents": [{' + body;
                }
              }
            }
            else {
              pbUnitDocumentJsonBody += '"Unit_Documents": [{' + '}';
            }
    
            if (jsonBody == '{') {
              jsonBody += '"Data": {' + primaryBuyerBody + ' , ' + pbUnitDocumentJsonBody + ']}';
            }
            //jsonBody += '}';
            system.debug('jsonBody: ' + jsonBody);
            if (!jointBuyerInfoMap.isEmpty()) {
              // adding Inquiry details to JSON                    
              for (String body: jointBuyerInfoMap.values()) {
    
                if (jointBuyerBody != '') {
                  jointBuyerBody += ', {' + body;
                }
                else {
                  jointBuyerBody += ',' + '"joint_Buyer": [{' + body;
                }
    
              }
              if (jsonBody != '{') {
                jsonBody += jointBuyerBody + jbUnitDocumentJsonBody + ']';
              }
            }
            else {
              jsonBody += ',"joint_Buyer": []'; //',"joint_Buyer": [{' + '"Unit_Documents": [{}]  }]';
    
            }
            jsonBody += '}}';
            system.debug('jsonBody: ' + jsonBody);
    
          } // end of request URI checking                            
    
        } // end of try block
        catch(exception e) {
          //statusCode = 406;
          jsonBody = e.getMessage();
          jsonBody = jSON.serialize(jsonBody);
    
        } // end of catch block
        RestResponse response = RestContext.response;
        response.addHeader('Content-Type', 'application/json');
        response.responseBody = Blob.valueOf(jsonBody);
    }
}