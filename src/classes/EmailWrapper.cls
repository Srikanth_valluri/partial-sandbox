public class EmailWrapper {

    public class EmailDetails{
        Public String toAddress {get;set;}
        Public String toName {get;set;}
        Public String ccAddress {get;set;}
        Public String ccName {get;set;}
        Public String bccAddress {get;set;}
        Public String bccName {get;set;}
        Public String subject {get;set;}
        Public String substitutions {get;set;}
        Public String fromAddress {get;set;}
        Public String fromName {get;set;}
        Public String replyToAddress {get;set;}
        Public String replyToName {get;set;}
        Public String contentType {get;set;}
        Public String contentValue {get;set;}
        Public String templateId {get;set;}
        Public Blob attachmentContentType {get;set;}
        Public String attachmentName {get;set;}
        Public String accountId {get;set;}
        Public String fromId {get;set;}
    }
    
    public static EmailDetails parse(String json) 
    {
        return (EmailDetails) System.JSON.deserialize(json, EmailDetails.class);
    }
}