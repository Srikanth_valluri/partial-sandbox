/*-------------------------------------------------------------------------------------------------
Description: Test class for PromotorInquiryController
    ============================================================================================================================
        Version | Date(DD-MM-YYYY) | Last Modified By | Comments
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 29-03-2018       | Rajnish Kumar    | 1. Added test methods to test the functionality
 =============================================================================================================================
*/
@isTest
public class PromotorInquiryController_Test {
    
    @testsetup
      static void setup() {
        List<Campaign__c> lstCampaign = new List<Campaign__c>();
        for (Integer i=0;i<10;i++) {
            Campaign__c objCampaign = InitialiseTestData.getCampaignDetails();
            objCampaign.Campaign_Type_New__c = 'Stands';
            objCampaign.Campaign_Name__c = 'test';
            objCampaign.Marketing_Active__c= true;
            objCampaign.Credit_Control_Active__c= true;
            objCampaign.Sales_Admin_Active__c = true;
            
            lstCampaign.add(objCampaign);
        }
        insert lstCampaign;
           
    }
    @istest
      static void setup1() {
            Campaign__c objCampaign = [SELECT ID from Campaign__c limit 1];
            Inquiry__c inq = New Inquiry__c();
            inq.Meeting_Type__c = ' Scheduled Tour';
            inq.Tour_Date_Time__c = system.now();
            inq.Campaign__c = objCampaign.id;
            insert inq;
          

        
    }
   
  @istest
    private static void submitInquiry() {
        PromotorInquiryController controller = new PromotorInquiryController();
        Campaign__c objCampaign = [SELECT ID from Campaign__c limit 1];
        controller.selectedStand = objCampaign.Id;
        controller.userId = UserInfo.getUserId();
       PromotorInquiryController.queryInquiries();
     // controller.meetingOnStand = true;
       
        Test.startTest();
        controller.cancel();
        controller.selectStand();
        controller.submitInquiry();
        PromotorInquiryController.getFieldDependencies('Inquiry__c','Country__c','City_new__c');
        controller.updateCityList();
       
        Test.stopTest();
        //System.assertEquals(1, [select count() from Inquiry__c]);
    }
    
    
    
  @istest
    private static void submitInquiry1() {
        PromotorInquiryController controller = new PromotorInquiryController();
        Campaign__c objCampaign = [SELECT ID from Campaign__c limit 1];
        controller.selectedStand = objCampaign.Id;
        controller.userId = UserInfo.getUserId();
       PromotorInquiryController.queryInquiries();
     // controller.meetingOnStand = true;
       
        Test.startTest();
        controller.cancel();
        controller.selectStand();
        controller.submitInquiry();
        PromotorInquiryController.getFieldDependencies('Inquiry__c','Country__c','City_new__c');

        controller.updateCityList();
       
        Test.stopTest();
        //System.assertEquals(1, [select count() from Inquiry__c]);
    }
   
}