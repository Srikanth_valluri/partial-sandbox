/*********************************************************************************
*   Description :  Executes FMServiceChargesCalloutBatch class.                  *
*--------------------------------------------------------------------------------*
*   Revision History:                                                            *
*   Version     Author              Date            Description                  *
*   1.0         Aishwarya Todkar    03/03/2019      Initial Draft                *
*********************************************************************************/
global class FMServiceChargesCalloutBatchSched implements Schedulable {
    global void execute(SchedulableContext sc) {

         database.executebatch(new FMServiceChargesCalloutBatch(),1);
    }
}