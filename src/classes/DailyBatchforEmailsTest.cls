@istest
public class DailyBatchforEmailsTest {
    static testMethod void testMethod1(){
        Account a = new Account();
        a.Name = 'Test Account';  
        a.party_ID__c = '1039032';
        a.Email__c = 'test@test.com';
        a.ZBEmailStatus__c = 'Valid';
        insert a;
        
        Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = a.id;
        sr.RecordTypeId = RecType;
        insert sr;

        Booking__c  bk = new  Booking__c();
        bk.Account__c = a.id;
        bk.Deal_SR__c = sr.id;
        insert bk;
        
        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = bk.id;
        bu.Unit_Name__c = 'LSB/10/B1001'; 
        bu.Registration_Status__c = 'Agreement Cancellation Verified';
        bu.Registration_ID__c = '74655';
        insert bu;
        DateTime currentTime = System.Now();
        String strHR = currentTime.format('HH:mm');
        string minutes = strHR.substringAfter(':');
        strHR = strHR.replace(minutes , '00');
        Schedule_Email__c schEMail = new Schedule_Email__c();
        schEMail.Booking_Unit__c= bu.id;
        schEMail.From_Address__c = 'test@test.com';
        schEMail.Email_Template__c = 'EHO';
        schEMail.Frequency__c = 'Daily';
        schEMail.Scheduled_Time__c = strHR;
        schEMail.Last_Updated__c = Date.valueOf(system.now().addDays(-1));
        schEmail.SMS_Template__c = 'EHO';
        schEmail.Email_Engine__c = 'Salesforce';
        schEmail.Comminication_Type__c = 'Email Only';
        insert schEMail;
        
        DailyBatchforEmails dailyBatch = new DailyBatchforEmails();
        Database.executeBatch(dailyBatch);
    }
    static testMethod void testMethod2(){
        Account a = new Account();
        a.Name = 'Test Account';  
        a.party_ID__c = '1039032';
        a.ZBEmailStatus__c = 'Valid';
        insert a;
        
        Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = a.id;
        sr.RecordTypeId = RecType;
        insert sr;

        Booking__c  bk = new  Booking__c();
        bk.Account__c = a.id;
        bk.Deal_SR__c = sr.id;
        insert bk;
        
        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = bk.id;
        bu.Unit_Name__c = 'LSB/10/B1001'; 
        bu.Registration_Status__c = 'Agreement Cancellation Verified';
        bu.Registration_ID__c = '74655';
        insert bu;
        
        Schedule_Email__c schEMail = new Schedule_Email__c();
        schEMail.Booking_Unit__c= bu.id;
        schEMail.From_Address__c = 'test@test.com';
        schEMail.Email_Template__c = 'EHO';
        schEMail.Frequency__c = 'Daily';
        schEmail.SMS_Template__c = 'Unit_SOA';
        schEmail.Email_Engine__c = 'SendGrid';
        schEMail.Last_Updated__c = Date.valueOf(system.now().addDays(-1));
        schEmail.Comminication_Type__c = 'Email & SMS';
        insert schEMail;
        
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
        DailyBatchforEmails dailyBatch = new DailyBatchforEmails();
        Database.executeBatch(dailyBatch);
        test.stopTest();
    }
    
    static testMethod void testMethod3(){
        Account a = new Account();
        a.Name = 'Test Account';  
        a.party_ID__c = '1039032';
        a.Email__c = 'test@test.com';
        a.ZBEmailStatus__c = 'Valid';
        insert a;
        
        Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = a.id;
        sr.RecordTypeId = RecType;
        insert sr;

        Booking__c  bk = new  Booking__c();
        bk.Account__c = a.id;
        bk.Deal_SR__c = sr.id;
        insert bk;
        
        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = bk.id;
        bu.Unit_Name__c = 'LSB/10/B1001'; 
        bu.Registration_Status__c = 'Agreement Cancellation Verified';
        bu.Registration_ID__c = '74655';
        insert bu;
        DateTime currentTime = System.Now();
        String strHR = currentTime.format('HH:mm');
        string minutes = strHR.substringAfter(':');
        strHR = strHR.replace(minutes , '00');
        Schedule_Email__c schEMail = new Schedule_Email__c();
        schEMail.Booking_Unit__c= bu.id;
        schEMail.From_Address__c = 'test@test.com';
        schEMail.Email_Template__c = 'EHO';
        schEMail.Frequency__c = 'Daily';
        schEMail.Scheduled_Time__c = strHR;
        schEMail.Last_Updated__c = Date.valueOf(system.now().addDays(-1));
        schEmail.SMS_Template__c = 'EHO';
        schEmail.Email_Engine__c = 'SendGrid';
        schEmail.Comminication_Type__c = 'Email Only';
        insert schEMail;
        
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
        DailyBatchforEmails dailyBatch = new DailyBatchforEmails();
        Database.executeBatch(dailyBatch);
        test.stopTest();
    }
}