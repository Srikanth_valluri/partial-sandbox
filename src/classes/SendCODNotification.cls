global with sharing class SendCODNotification implements Schedulable{

  global static Id CODRecordTypeId = DamacUtility.getRecordTypeId('Case', 'Change of Details');
  global static Id COJBRecordTypeId = DamacUtility.getRecordTypeId('Case', 'Change of Joint Buyer');
  global static Id NameAndNationalityRecordTypeId = DamacUtility.getRecordTypeId('Case', 'Name Nationality Change');
  global static Id PassportDetailsUpdateRecordTypeId = DamacUtility.getRecordTypeId('Case', 'Passport Detail Update');

    
   global List<Case> lstCase;

    @InvocableMethod
    global static void PushCaseForNotification(List<Case> lstCase) {
        system.debug('lstCase : '+lstCase);
        SendCODNotification m = new SendCODNotification();
        m.lstCase = lstCase;
        String day = string.valueOf(system.now().day());
        String month = string.valueOf(system.now().month());
        String hour = string.valueOf(system.now().hour());
        String minute = string.valueOf(system.now().minute() + 1);
        String second = string.valueOf(system.now().second());
        String year = string.valueOf(system.now().year());
        String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
    
        String jobID = system.schedule('COD Notification Job '+lstCase[0].CaseNumber + system.now(), strSchedule, m);
    }


    global void execute(SchedulableContext ctx) {
        sendSMSToCustomer(lstCase);
    }

    
    global static void sendSMSToCustomer(List<Case> lstCase) {
        
        Set<String> codCase = new Set<String>();
        Set<String> nameAndNationalityCase = new Set<String>();
        Set<String> passportDetailsCase = new Set<String>();
        List<Case> codCaseRecordsSet = new List<Case>();
        List<Case> additionalCaseRecordSet = new List<Case>();
        List<Case> nncCaseRecordsSet = new List<Case>();
        List<Case> ppuCaseRecordsSet = new List<Case>();
        
        system.debug('CODRecordTypeId : ' + CODRecordTypeId);
        system.debug('lstCase : ' + lstCase);
        
        for( Case objCase : lstCase ){

            system.debug( objCase );
            if( objCase.RecordTypeId == CODRecordTypeId && objCase.Status == 'Closed' ){
                codCase.add(objCase.id);
                codCaseRecordsSet.add(objCase);
            }

            if( objCase.RecordTypeId == COJBRecordTypeId && objCase.Status == 'Closed' && objCase.Is_Primary_Customer_Updated__c ){
                additionalCaseRecordSet.add(objCase);
            }

            if( objCase.RecordTypeId == NameAndNationalityRecordTypeId && objCase.Status == 'Closed' ) {
                nameAndNationalityCase.add(objCase.id);
                nncCaseRecordsSet.add(objCase);
            }
            if( objCase.RecordTypeId == PassportDetailsUpdateRecordTypeId && objCase.Status == 'Closed' ){
                passportDetailsCase.add(objCase.id);
                ppuCaseRecordsSet.add(objCase);
            }
        }
        
        if( !codCase.isEmpty() ){
            sendSMSToCODCustomer( codCase );
            sendEmailToCODCustomer(codCaseRecordsSet);
        }
        if( !additionalCaseRecordSet.isEmpty() ){
            sendEmailOnAdditionalDetail( additionalCaseRecordSet );
        }
        if( !nameAndNationalityCase.isEmpty() ){
            sendSMSToNameAndNationalityCustomer( nameAndNationalityCase );
            sendEmailOnNNC(nncCaseRecordsSet);
        }
        if( !passportDetailsCase.isEmpty() ){
            sendSMSToPassportDetailsCustomer( passportDetailsCase );
            sendEmailOnpassportDetails(ppuCaseRecordsSet);
        }    
    }
  
    global static void sendEmailOnpassportDetails( List<Case> ppuCaseRecordsSet  ){
        Set<Id> accountIdSet = new Set<Id>();
        for( Case caseObj : ppuCaseRecordsSet  ){
            accountIdSet.add( caseObj.AccountId );
        }
        Map<Id, Account> AccountId_AccountRecordMap = getAccountDetails(accountIdSet);
        
        String strSubject = 'Passport Details Updated  - Service Request # '+ppuCaseRecordsSet[0].CaseNumber;

        
        String strBody = 'Dear '+ AccountId_AccountRecordMap.get(ppuCaseRecordsSet[0].AccountId).Name + ', <br /> Your request (Service Request #: ' + ppuCaseRecordsSet[0].CaseNumber;
        
        strBody += ')  for updating Passport details has been submitted and updated in our System as below: <br /><br />';
        
        strBody +=  'Passport No: ';
        strBody += ( AccountId_AccountRecordMap.get(ppuCaseRecordsSet[0].AccountId).IsPersonAccount ) ? AccountId_AccountRecordMap.get(ppuCaseRecordsSet[0].AccountId).Passport_Number__pc : AccountId_AccountRecordMap.get(ppuCaseRecordsSet[0].AccountId).CR_Number__c;
        
        strBody +=  ' <br /> Passport Expiry Date:  '; 
        strBody += ( AccountId_AccountRecordMap.get(ppuCaseRecordsSet[0].AccountId).IsPersonAccount ) ? String.valueOf(AccountId_AccountRecordMap.get(ppuCaseRecordsSet[0].AccountId).Passport_Expiry_Date__pc) : String.valueOf( AccountId_AccountRecordMap.get(ppuCaseRecordsSet[0].AccountId).CR_Registration_Expiry_Date__c) ;
        
        strBody +=  ' <br /> Passport Issue Place:';
        strBody += ( AccountId_AccountRecordMap.get(ppuCaseRecordsSet[0].AccountId).IsPersonAccount ) ? AccountId_AccountRecordMap.get(ppuCaseRecordsSet[0].AccountId).Passport_Issue_Place__pc : AccountId_AccountRecordMap.get(ppuCaseRecordsSet[0].AccountId).CR_Registration_Place__c ;
        
        strBody += ' <br /> Should you have any further queries or If you did not raise this request please contact At Your Service team (atyourservice@damacproperties.com or +9714 2375000) <br /><br />';
        strBody += 'Thank you, <br />';
        strBody += 'At Your Service Team <br /> DAMAC';

        sendMail( strSubject,strBody, AccountId_AccountRecordMap.get(ppuCaseRecordsSet[0].AccountId).IsPersonAccount ? AccountId_AccountRecordMap.get(ppuCaseRecordsSet[0].AccountId).Email__pc : AccountId_AccountRecordMap.get(ppuCaseRecordsSet[0].AccountId).Email__c );
      }    
  
    global static void sendEmailOnNNC( List<Case> nncCaseRecordsSet  ){
        Set<Id> accountIdSet = new Set<Id>();
        for( Case caseObj : nncCaseRecordsSet ){
            accountIdSet.add( caseObj.AccountId );
        }
        Map<Id, Account> AccountId_AccountRecordMap = getAccountDetails(accountIdSet);
        
        String strSubject = 'Name/ Nationality Updated  - Service Request # '+ nncCaseRecordsSet[0].CaseNumber;
        
        String strBody = 'Dear '+ AccountId_AccountRecordMap.get(nncCaseRecordsSet[0].AccountId).Name + ', <br /> Your request (Service Request #: ' + nncCaseRecordsSet[0].CaseNumber;
        
        strBody += ') for changing Name/ Nationality details has been submitted and updated in our System as below: <br /><br />';
        
        strBody += 'Name: '+AccountId_AccountRecordMap.get(nncCaseRecordsSet[0].AccountId).Name; 
        
        strBody +=  '<br /> Nationality: ' + AccountId_AccountRecordMap.get(nncCaseRecordsSet[0].AccountId).Nationality__pc;
        
        strBody +=  '<br /> Passport No: ';
        strBody += ( AccountId_AccountRecordMap.get(nncCaseRecordsSet[0].AccountId).IsPersonAccount ) ? AccountId_AccountRecordMap.get(nncCaseRecordsSet[0].AccountId).Passport_Number__pc : AccountId_AccountRecordMap.get(nncCaseRecordsSet[0].AccountId).CR_Number__c;
        
        strBody +=  '<br /> Passport Expiry Date:  '; 
        strBody += ( AccountId_AccountRecordMap.get(nncCaseRecordsSet[0].AccountId).IsPersonAccount ) ? String.valueOf(AccountId_AccountRecordMap.get(nncCaseRecordsSet[0].AccountId).Passport_Expiry_Date__pc) : String.valueOf(AccountId_AccountRecordMap.get(nncCaseRecordsSet[0].AccountId).CR_Registration_Expiry_Date__c );
        
        strBody +=  ' <br /> Passport Issue Place:';
        strBody += ( AccountId_AccountRecordMap.get(nncCaseRecordsSet[0].AccountId).IsPersonAccount ) ? AccountId_AccountRecordMap.get(nncCaseRecordsSet[0].AccountId).Passport_Issue_Place__pc : AccountId_AccountRecordMap.get(nncCaseRecordsSet[0].AccountId).CR_Registration_Place__c;
        
        strBody += ' <br /> Should you have any further queries or If you did not raise this request please contact At Your Service team (atyourservice@damacproperties.com or +9714 2375000) <br /><br />';
        strBody += 'Thank you, <br />';
        strBody += 'At Your Service Team <br /> DAMAC';

        sendMail( strSubject,strBody, AccountId_AccountRecordMap.get(nncCaseRecordsSet[0].AccountId).IsPersonAccount ? AccountId_AccountRecordMap.get(nncCaseRecordsSet[0].AccountId).Email__pc : AccountId_AccountRecordMap.get(nncCaseRecordsSet[0].AccountId).Email__c );
      }  
  
  @future(callout=true)
    global static void sendSMSToCODCustomer( Set<String> codCase  ){
    
    List<Case> lstCase = getCaseList(codCase);

    
    String strEmail = lstCase[0].Account.IsPersonAccount ? lstCase[0].Account.Email__pc : lstCase[0].Account.Email__c;
    String strMobile = lstCase[0].Account.IsPersonAccount ? lstCase[0].Account.Mobile_Phone_Encrypt__pc : lstCase[0].Account.Mobile__c;
    String strAdd = lstCase[0].Account.IsPersonAccount ? lstCase[0].Account.Address_Line_1__pc : lstCase[0].Account.Address_Line_1__c;
    
    
    String strBody = 'Dear '+ lstCase[0].Account.Name +', \n Your primary contact details is updated in our System as below (SR# '+lstCase[0].CaseNumber+'):\n\n';
    strBody = strBody + 'Email ID: ' + strEmail +'\n Contact Number: ' +strMobile  + '\n Address: ' + strAdd +'\n\n';
    strBody = strBody + 'Thank you,\n At Your Service Team \n DAMAC';
    
    if( lstCase[0].Account.Old_Mobile__c != null ) {
        sendSms(  strBody , string.valueOf( lstCase[0].Account.Old_Mobile__c ) , lstCase );
    }
    if( !string.isBlank(strMobile )){
        sendSms(  strBody , strMobile , lstCase );
    }
  }
  
  global static void sendEmailToCODCustomer( List<Case> codCaseSet  ){
    Set<Id> accountIdSet = new Set<Id>();
    for( Case caseObj : codCaseSet ){
        accountIdSet.add( caseObj.AccountId );
    }
    system.debug('accountIdSet === ' + accountIdSet);
    Map<Id, Account> AccountId_AccountRecordMap = getAccountDetails(accountIdSet);
    
    String strSubject = 'Contact Details Updated - Service Request #'+ codCaseSet[0].CaseNumber;

    system.debug('AccountId_AccountRecordMap === ' + AccountId_AccountRecordMap);
    String strBody = 'Dear '+ AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).Name + ', <br /> Your request (Service Request #: ' + codCaseSet[0].CaseNumber;
    
    strBody += ') for changing primary contact details has been submitted and updated in our System as below: <br /><br />';
    Boolean isPersonAccount = AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).IsPersonAccount;
    strBody += 'Email ID: '; 
    strBody += isPersonAccount ? AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).Email__pc : AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).Email__c;
    
    strBody +=  '<br /> Contact Number: '; 
    strBody +=  isPersonAccount ? AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).Mobile_Phone_Encrypt__pc : AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).Mobile__c;
    
    String strAddress = '';
    if( AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).IsPersonAccount ){
        strAddress = AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).Address_Line_1__pc + ', <br />';
        strAddress += String.isNotBlank( AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).Address_Line_2__pc ) ? AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).Address_Line_2__pc + ', <br />' : '';
        strAddress += String.isNotBlank( AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).Address_Line_3__pc ) ? AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).Address_Line_3__pc + ', <br />' : '';
        strAddress += String.isNotBlank( AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).Address_Line_4__pc ) ? AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).Address_Line_4__pc + ', <br />' : '';
    } else{
        strAddress = AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).Address_Line_1__c + ', <br />';
        strAddress += String.isNotBlank( AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).Address_Line_2__c ) ? AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).Address_Line_2__c + ', <br />' : '';
        strAddress += String.isNotBlank( AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).Address_Line_3__c ) ? AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).Address_Line_3__c + ', <br />' : '';
        strAddress += String.isNotBlank( AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).Address_Line_4__c ) ? AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).Address_Line_4__c + ', <br />' : '';
    }
    
    
    strBody += '<br /> Address: ' + strAddress;
    strBody += '<br /> Should you have any further queries or If you did not raise this request please contact At Your Service team (atyourservice@damacproperties.com or +9714 2375000) <br /><br />';
    strBody += 'Thank you, <br />';
    strBody += 'At Your Service Team <br /> DAMAC';

    if( AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).Old_Email__c != null ) {
        sendMail( strSubject,strBody, string.valueOf( AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).Old_Email__c ) );
    }   
    if( AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).Email__pc != null || AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).Email__c != null ) {
        sendMail( strSubject,strBody, AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).IsPersonAccount ? AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).Email__pc : AccountId_AccountRecordMap.get(codCaseSet[0].AccountId).Email__c );
    }
  }
    
  @future(callout=true)
    global static void sendSMSToNameAndNationalityCustomer( Set<String> nameAndNationalityCase  ){
    
    List<Case> lstCase = getCaseList(nameAndNationalityCase);

    
    String strEmail = lstCase[0].Account.IsPersonAccount ? lstCase[0].Account.Email__pc : lstCase[0].Account.Email__c;
    String strMobile = lstCase[0].Account.IsPersonAccount ? lstCase[0].Account.Mobile_Phone_Encrypt__pc : lstCase[0].Account.Mobile__c;
    String strAdd = lstCase[0].Account.IsPersonAccount ? lstCase[0].Account.Address_Line_1__pc : lstCase[0].Account.Address_Line_1__c;
    Date d =  lstCase[0].Passport_Issue_Date__c;
    String PassportIssueDate = ( d != null ) ? DateTime.newInstance(d.year(),d.month(),d.day()).format('dd-MMM-YYYY') : '';
    
    String strBody = 'Dear '+ lstCase[0].Account.Name +', \n Your Name Nationality details updated in our System as below (SR# '+ lstCase[0].CaseNumber+'): \n\n';
    strBody = strBody + 'Name: ' + lstCase[0].Account.Name + ' \n Nationality: ' +lstCase[0].Account.Nationality__pc + '\n Passport No: ' + lstCase[0].New_CR__c + ' \n Passport Expiry Date: ' + PassportIssueDate + '\n Passport Issue Place: ' + lstCase[0].Passport_Issue_Place__c  +' \n\n';
    strBody = strBody + 'Thank you, \n At Your Service Team \n DAMAC' ;

    sendSms(  strBody , strMobile , lstCase );
  }
  
  global static void sendEmailOnAdditionalDetail( List<Case> additionalCaseSet  ){
    Set<Id> accountIdSet = new Set<Id>();
    for( Case caseObj : additionalCaseSet ){
        accountIdSet.add( caseObj.AccountId );
    }
    Map<Id, Account> AccountId_AccountRecordMap = getAccountDetails(accountIdSet);
    
    String strSubject = 'Contact Details Updated  - Service Request # '+ additionalCaseSet[0].CaseNumber;
    
    String strBody = 'Dear '+ AccountId_AccountRecordMap.get(additionalCaseSet[0].AccountId).Name + ', <br /> Your request (Service Request #: ' + additionalCaseSet[0].CaseNumber;
    
    strBody += ') for adding/changing additional contact details has been submitted and updated in our System as below: <br /><br />';
    
    strBody += 'Additional Email ID: '; 
    strBody += ( AccountId_AccountRecordMap.get(additionalCaseSet[0].AccountId).IsPersonAccount ) ? AccountId_AccountRecordMap.get(additionalCaseSet[0].AccountId).Email_2__pc :AccountId_AccountRecordMap.get(additionalCaseSet[0].AccountId).Email_2__c + '<br />';
    
    strBody +=  ' \n Mobile Number 2: '; 
    strBody += ( AccountId_AccountRecordMap.get(additionalCaseSet[0].AccountId).IsPersonAccount ) ? AccountId_AccountRecordMap.get(additionalCaseSet[0].AccountId).Mobile_Phone_Encrypt_2__pc : AccountId_AccountRecordMap.get(additionalCaseSet[0].AccountId).Mobile_Phone_2__c;
    
    strBody +=  '<br /> Mobile Number 3: ';
    strBody += ( AccountId_AccountRecordMap.get(additionalCaseSet[0].AccountId).IsPersonAccount ) ? AccountId_AccountRecordMap.get(additionalCaseSet[0].AccountId).Mobile_Phone_Encrypt_3__pc : AccountId_AccountRecordMap.get(additionalCaseSet[0].AccountId).Mobile_Phone_3__c;
    
    strBody +=  ' <br /> Mobile Number 4: '; 
    strBody += ( AccountId_AccountRecordMap.get(additionalCaseSet[0].AccountId).IsPersonAccount ) ? AccountId_AccountRecordMap.get(additionalCaseSet[0].AccountId).Mobile_Phone_Encrypt_4__pc : AccountId_AccountRecordMap.get(additionalCaseSet[0].AccountId).Mobile_Phone_4__c;
    
    strBody +=  ' <br /> Mobile Number 5: ';
    strBody += ( AccountId_AccountRecordMap.get(additionalCaseSet[0].AccountId).IsPersonAccount ) ? AccountId_AccountRecordMap.get(additionalCaseSet[0].AccountId).Mobile_Phone_Encrypt_5__pc : AccountId_AccountRecordMap.get(additionalCaseSet[0].AccountId).Mobile_Phone_5__c;
    
    strBody += '<br /> Should you have any further queries or If you did not raise this request please contact At Your Service team (atyourservice@damacproperties.com or +9714 2375000) <br /><br />';
    strBody += 'Thank you, <br />';
    strBody += 'At Your Service Team <br /> DAMAC';

    sendMail( strSubject,strBody, AccountId_AccountRecordMap.get(additionalCaseSet[0].AccountId).IsPersonAccount ? AccountId_AccountRecordMap.get(additionalCaseSet[0].AccountId).Email__pc : AccountId_AccountRecordMap.get(additionalCaseSet[0].AccountId).Email__c );
  }

  @future(callout=true)
    global static void sendSMSToPassportDetailsCustomer( Set<String> passportDetailsCase  ){
    
    List<Case> lstCase = getCaseList(passportDetailsCase);

    
    String strEmail = lstCase[0].Account.IsPersonAccount ? lstCase[0].Account.Email__pc : lstCase[0].Account.Email__c;
    String strMobile = lstCase[0].Account.IsPersonAccount ? lstCase[0].Account.Mobile_Phone_Encrypt__pc : lstCase[0].Account.Mobile__c;
    String strAdd = lstCase[0].Account.IsPersonAccount ? lstCase[0].Account.Address_Line_1__pc : lstCase[0].Account.Address_Line_1__c;

    Date d =  lstCase[0].Passport_Issue_Date__c;
    String PassportIssueDate = ( d != null ) ? DateTime.newInstance(d.year(),d.month(),d.day()).format('dd-MMM-YYYY') : ''; 
    
    String strBody = 'Dear '+ lstCase[0].Account.Name +', \n Your Passport details has been updated in our System as below (SR# '+ lstCase[0].CaseNumber+'):\n\n';
    strBody = strBody + 'Passport No: ' + lstCase[0].New_CR__c + '\n Passport Expiry Date: ' + PassportIssueDate + '\n Passport Issue Place: ' + lstCase[0].Passport_Issue_Place__c  +'\n\n';
    strBody = strBody + 'Thank you,\n At Your Service Team \n DAMAC' ;

    sendSms(  strBody , strMobile , lstCase );
  }
  
  global static void sendSms( String strBody , String strMobile 
             , List<Case> lstCase ) {
    List<SMS_History__c> smsLst = new List<SMS_History__c>();
    String strUserName = Label.Damac_CRM_SMS_Service_User_name;//'damaccrm';
    String strPassword = Label.Damac_CRM_SMS_Service_Password; //'D@$al3sF0rc387!';
    //String strSID = Label.Damac_CRM_SMS_Service_Sender_Id;//'DAMAC GRP'; 
    String strSID = SMSClass.getSenderName(strUserName, strMobile, false);
            
    if(strMobile.startsWith('00')) {
        strMobile = strMobile.removeStart('00');
    }else if(strMobile.startsWith('0')) {
        strMobile = strMobile.removeStart('0');
    }
    HttpRequest req = new HttpRequest();
        req.setMethod('POST' ); // Method Type
        req.setEndpoint('http://api.smscountry.com/SMSCwebservice_bulk.aspx');
        req.setBody('user='+ strUserName + '&passwd=' + strPassword +'&message=' + GenericUtility.encodeChar(strBody) + '&mobilenumber=' 
        + strMobile + '&sid=' + strSID + '&MTYPE=LNG&DR=Y'); // Request Parameters
      
    Http http = new Http();
    try {
      HTTPResponse res = http.send( req );
      //HTTPResponse res;
        /*if(Test.isRunningTest()) {
           res = new HttpResponse();
        } else {
           res = http.send(req);
        }*/
      if( String.isNotBlank( res.getBody() ) ) {

        SMS_History__c smsObj = new SMS_History__c();
        smsObj.Message__c = strBody;
        smsObj.Phone_Number__c = strMobile;
        smsObj.Customer__c = lstCase[0].AccountId;
        //smsObj.Case = lstCase[0].Id;
        smsObj.Description__c = res.getBody();
        smsObj.Is_SMS_Sent__c = true;
        if(String.valueOf(res.getBody()).contains('OK:')){
            smsObj.sms_Id__c = res.getBody().substringAfter('OK:');
        }
        System.debug('smsObj::::if:'+smsObj);
        smsLst.add(smsObj);          
      }
    }
        catch( Exception e ) {
      SMS_History__c smsObj = new SMS_History__c();
      smsObj.Message__c = strBody;
      smsObj.Phone_Number__c = strMobile;
      smsObj.Customer__c = lstCase[0].AccountId;
      //smsObj.Case = lstCase[0].Id;
      smsObj.Description__c = e.getMessage();
      System.debug('smsObj::::else:'+smsObj);
      smsLst.add(smsObj);
        }
        if( !smsLst.isEmpty() ){
            try{
                upsert smsLst;
            }catch(exception ex){
                System.debug('DML Exception*****: '+ ex);
            }
        }
  }
  
  
    global static List<Case> getCaseList( set<String> setCaseIds ) {
        return new List<Case>( [ SELECT Id
                                        , Status,AccountId,Contact_Mobile__c,Contact_Email__c
                                        , Account.Party_ID__c,Account.Nationality__pc
                    , Account.Name
                    , Account.Old_Email__c
                    , Account.Old_Mobile__c
                    , Account.Email__pc
                    , Account.Email__c
                    , Account.Mobile_Phone_Encrypt__pc
                    , Account.Mobile__c
                    , Account.IsPersonAccount
                    , Account.Address_Line_1__pc
                    , Owner.Name
                                        , POA_Name__c
                                        , POA_Expiry_Date__c
                                        , POA_Relation_With_Owner__c
                                        , CaseNumber
                                        , POA_Issued_By__c
                    , Account.Address_Line_1__c
                    , Mortgage_Bank_Name__c
                    , Mortgage_Transaction_Type__c
                    , Mortgage_Value__c
                                        , Purpose_of_POA__c
                                        , POA_File_URL__c
                    , Booking_Unit__r.Registration_Id__c
                    , Passport_Issue_Place__c,Passport_Issue_Date__c,New_CR__c
                                      FROM Case 
                                     WHERE Id IN :setCaseIds ] );
    }
    
    global static Map<Id, Account> getAccountDetails( Set<Id> accountIdSet ){
        return new map<Id, Account>([ SELECT Id,
                                        IsPersonAccount, 
                                        Email__pc,
                                        Old_Email__c,
                                        Old_Mobile__c,
                                        Email__c, 
                                        Mobile_Phone_Encrypt__pc, 
                                        Mobile__c, 
                                        Address_Line_1__pc, 
                                        Address_Line_2__pc, 
                                        Address_Line_3__pc, 
                                        Address_Line_4__pc,
                                        Address_Line_1__c,
                                        Address_Line_2__c, 
                                        Address_Line_3__c, 
                                        Address_Line_4__c, 
                                        Name, 
                                        Email_2__pc, 
                                        Email_2__c, 
                                        Mobile_Phone_Encrypt_2__pc, 
                                        Mobile_Phone_2__c, 
                                        Mobile_Phone_Encrypt_3__pc, 
                                        Mobile_Phone_3__c, 
                                        Mobile_Phone_Encrypt_4__pc, 
                                        Mobile_Phone_4__c, 
                                        Mobile_Phone_Encrypt_5__pc, 
                                        Mobile_Phone_5__c ,
                                        Nationality__pc,
                                        Passport_Number__pc,
                                        CR_Number__c,
                                        Passport_Expiry_Date__pc,
                                        CR_Registration_Expiry_Date__c,
                                        Passport_Issue_Place__pc,
                                        CR_Registration_Place__c
                                    FROM Account WHERE ID IN: accountIdSet ] );
    }
    
    global static void sendMail( String strSubject ,String strBody, String email ){
        List<String> toAddresses = new List<String>();
        toAddresses.add( email );
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses( toAddresses );

        mail.setSubject( strSubject );
        mail.setHtmlBody( strBody );
        system.debug('mail' + mail);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}