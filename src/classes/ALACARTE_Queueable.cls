global class ALACARTE_Queueable implements Queueable, Database.AllowsCallouts {

    global set<id> bookingIds = new set<Id>();
    global ALACARTE_Queueable(set<id> bookingIds){
        this.bookingIds = bookingIds;
    }

    global void execute(QueueableContext qc){
        markAlacarteIPMS(bookingIds);
    }

    public static void markAlacarteIPMS(set<Id> bookingIds){
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://dxbhoebtstap.damacholding.home/webservices/rest/XXDC_REGISTRATION_UPD_PKG/upd_reg_spcl_case/');
        req.setMethod('POST');
        req.setHeader('ACCEPT','application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(120000);
        req.setHeader('Authorization', 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf('ORACLE_USER' + ':' + 'crp1user')));
        string reqXML = getRequestBody(bookingIds);
        req.setBody(reqXML);
        System.debug('>>>>>>>reqXML>>>>>>>'+reqXML);
        Http http = new Http();
        HTTPResponse res = http.send(req);
        log__c log = new log__c();
        log.Description__c = reqXML;
        log.Response__c = res.getBody();
        log.Status__c = res.getStatusCode() == 200 ? 'SUCCESS':'ERROR';
        log.Type__c = 'A LA CARTE';
        insert log;
    }

    public static string getRequestBody(set<Id> bookingIds){
        Registration Special_Case_Registration = new Registration();
        Special_Case_Registration.InputParameters  = new InputParams();
        Special_Case_Registration.InputParameters.P_INPUT_TBL = new InputTable();
        Special_Case_Registration.InputParameters.P_INPUT_TBL.INPUT_REC_TYPE = new list<InputRecordType>();
        list<Booking_unit__c> lstUnits = [Select id, Unit_name__c, Bedroom_Type__c, 
                                        Registration_ID__c, Interior_Color_Scheme__c, Exterior_Color_Scheme__c, 
                                        Pool_Option__c, Furniture_Color_Scheme__c, 
                                        (Select id, name, Option__c, Costing__c, Inventory__r.Property__r.Alacarte_Enabled__c from Inventory_Addons__r) 
                                        from Booking_Unit__c where Booking__c in: bookingIds];
        
        for(Booking_Unit__c bu : lstUnits){
            if(bu.Inventory__r.Property__r.Alacarte_Enabled__c && bu.Interior_Color_Scheme__c != null 
                    && bu.Exterior_Color_Scheme__c != null && bu.Pool_Option__c != null && bu.Furniture_Color_Scheme__c != null){
                InputRecordType irt = new InputRecordType();
                irt.P_REGISTRATION_ID = bu.Registration_ID__c; 
                irt.P_FILE_URL = '';
                irt.P_NO_OF_PAGES = '';
                irt.P_SPECIAL_CASE = '';
                irt.P_UNIT_CODE = bu.Unit_name__c;
                irt.X1_INTERIOR = bu.Interior_Color_Scheme__c;
                irt.X2_FACADE = bu.Exterior_Color_Scheme__c;
                irt.X3_BEDROOM_TYPE = bu.Bedroom_Type__c;
                irt.X4_FURNISHING = '';
                irt.X5_FURNISHING_TYPE = bu.Furniture_Color_Scheme__c;
                irt.X6_TERRACE = '';
                irt.X7_POOL = bu.Pool_Option__c;
                for(Inventory_AddOn__c iao: bu.Inventory_Addons__r){
                    if(iao.Name == 'Pool' && iao.Option__c == 'Splash Pool')
                        irt.X8_POOL_CHARGE = string.valueOf(iao.Costing__c);
                    if(iao.Name == 'Furniture' && iao.Option__c != 'Without Furniture')    
                        irt.X9_FURNISHING_CHARGE = string.valueOf(iao.Costing__c);
                }
                Special_Case_Registration.InputParameters.P_INPUT_TBL.INPUT_REC_TYPE.add(irt);
            }
        }
        return JSON.serialize(Special_Case_Registration);
    }


    public Registration Special_Case_Registration;

    public class Registration {
        public InputParams InputParameters;
    }
    
    public class InputParams {
        public InputTable P_INPUT_TBL;
    }
    
    public class InputTable {
        public list<InputRecordType> INPUT_REC_TYPE;
    }
    
    public class InputRecordType {
        public String P_REGISTRATION_ID;
        public String P_FILE_URL;
        public String P_NO_OF_PAGES;
        public String P_SPECIAL_CASE;
        public String P_UNIT_CODE;
        public String X1_INTERIOR;
        public String X2_FACADE;
        public String X3_BEDROOM_TYPE;
        public String X4_FURNISHING;
        public String X5_FURNISHING_TYPE;
        public String X6_TERRACE;
        public String X7_POOL;
        public string X8_POOL_CHARGE;
        public string X9_FURNISHING_CHARGE;
    }
}