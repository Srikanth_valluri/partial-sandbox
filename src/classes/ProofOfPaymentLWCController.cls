/**********************************************************************************************************************
Description: Apex controller for POP LWC component
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By   | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     | 27/07/2020        | Sayali Bhambure   | 1. Initial Draft
***********************************************************************************************************************/
public with sharing class ProofOfPaymentLWCController {
    public static final String SETTING_GET_AR = 'POP Fetch AR Receipts';
    public static final String SETTING_APPLY_AR = 'POP Approval API';
    public static final String GET_REQ_PREFIX = '{"GET_UNIDENTIFIED_RECEIPT_Input":{"RESTHeader":{"Responsibility":"ONT_ICP_SUPER_USER","RespApplication":"ONT","SecurityGroup":"STANDARD","NLSLanguage":"AMERICAN"},"InputParameters":';
    public static final String APPLY_REQ_PREFIX = '{ "APPLY_UNIDENTIFIED_RECEIPTS_Input":{ "RESTHeader": { "Responsibility":"ONT_ICP_SUPER_USER", "RespApplication":"ONT", "SecurityGroup":"STANDARD", "NLSLanguage":"AMERICAN" }, "InputParameters":';
    public static final String FETCHED_0_RECORDS = 'Successfully Fetched Records : 0';
    public static final String MSG_CONTACT_ADMIN = 'Some error occured, please contact Admin.';
    public static final String MSG_CREDS_NOT_CONFIGURED = 'Approval service credentials not configured in Credentials_Details custom setting. Please contact with Admin.';
    public static final String TASK_SUBJECT = 'Verify Proof of Payment Details in IPMS';
    public static final String USER_FINANCE = 'Finance';
    public static final String PROCESS_POP = 'POP';
    public static final String STATUS_REJECT = 'Reject';
    public static final String STATUS_REJECT_DUP = 'Reject-Duplicate';
    public static final String STATUS_CLOSED = 'Closed';

/**********************************************************************************************************************
Description : Method to return POP bank details
Parameter(s): Id
Return Type : List<Refernce>
**********************************************************************************************************************/
@AuraEnabled(cacheable=true)
public static List<Refernce> getReference(Id caseId) {
    List<POP_Bank_Details__c> popList = [Select Id 
        , Account_Number__c
        , Bank_Reference_number__c
        , Bank_Account_number__c
        , Beneficiary_Name__c
        , Beneficiary_Bank__c
        , Deposit_Amount__c 
        , Deposit_Date__c
        , Date_Range__c
        , Deposit_Range__c
        , Finance_Bank_Account_Number__c
    FROM POP_Bank_Details__c
    WHERE Case__c =: caseId];
    

     
    List<Refernce> refernceList = new  List<Refernce>();
    if(!popList.isEmpty()){
        for(POP_Bank_Details__c pop : popList){
            Refernce refObj = new Refernce();
            refObj.Id = pop.Id;
            refObj.refNum = pop.Bank_Reference_number__c;
            if( pop.Bank_Account_number__c != null ){
                refObj.BankAccountNumber = String.valueOf(pop.Bank_Account_number__c);
            }
            if( pop.Beneficiary_Name__c != null ){
                refObj.BankAccountNumber = refObj.BankAccountNumber +'-'+ pop.Beneficiary_Name__c;
            }
            if( pop.Beneficiary_Bank__c != null ){
                refObj.BankAccountNumber = refObj.BankAccountNumber +'-'+ pop.Beneficiary_Bank__c;
            }
            String accountNumber=pop.Account_Number__c;
            if( pop.Beneficiary_Bank__c != null && accountNumber!=null && accountNumber.length()>6){
                refObj.BankAccountNumber = refObj.BankAccountNumber +'-'+ accountNumber.subString(accountNumber.length()-6, accountNumber.length());
            }
            if( pop.Beneficiary_Bank__c != null && accountNumber!=null &&  accountNumber.length()<6 && accountNumber.length()>0){
                refObj.BankAccountNumber = refObj.BankAccountNumber +'-'+ accountNumber;
            }
            refObj.BankAccountNumber=pop.Finance_Bank_Account_Number__c;
            refObj.DepositAmount = pop.Deposit_Amount__c;
            refObj.DepositDate = pop.Deposit_Date__c;
            refObj.DateRange = pop.Date_Range__c;
            refObj.DepositRange = pop.Deposit_Range__c;
            refernceList.add(refObj);
        }
    }
    return refernceList;
}

/**********************************************************************************************************************
Description : Method to return POP bank details for picklst
Parameter(s): Id
Return Type : List<Refernce>
**********************************************************************************************************************/
@AuraEnabled(cacheable=true)
public static List<Refernce> getReferencepicklst(Id caseId) {
    List<SR_Booking_Unit__c> srBookings=[Select Id,Booking_Unit__c,Case__c,Booking_Unit__r.Inventory__c FROM SR_Booking_Unit__c Where Case__c =:caseId];
    List<POP_Bank_Details__c> popList= new List<POP_Bank_Details__c>();
    if(srBookings.size()>0){
        List<string> invLst=new List<String>();
        for(SR_Booking_Unit__c SR: srBookings){
            invLst.add(SR.Booking_Unit__r.Inventory__c);
        }
        List<Inventory__c> invRecordLst=[Select id,Building_Location__c from Inventory__c where Id IN :invLst];
        List<string> invBuLst=new List<String>();
        
        for(Inventory__c inv: invRecordLst){
            invBuLst.add(inv.Building_Location__c);
        }
       if(invBuLst.size() >0){
            popList= [Select Id 
                                                , Account_Number__c
                                                , Bank_Reference_number__c
                                                , Bank_Account_number__c
                                                , Beneficiary_Name__c
                                                , Beneficiary_Bank__c
                                                , Deposit_Amount__c 
                                                , Deposit_Date__c
                                                , Date_Range__c
                                                , Deposit_Range__c
                                                ,Finance_Bank_Account_Number__c
                                            FROM POP_Bank_Details__c
                                            WHERE Case__c =: caseId OR (ForCorporate__c=true AND ForFinance__c=true AND Building_Location__c=NULL) OR (Building_Location__c IN :invBuLst)];
           
    }
} else{
        popList = [Select Id 
        , Account_Number__c
        , Bank_Reference_number__c
        , Bank_Account_number__c
        , Beneficiary_Name__c
        , Beneficiary_Bank__c
        , Deposit_Amount__c 
        , Deposit_Date__c
        , Date_Range__c
        , Deposit_Range__c
        , Finance_Bank_Account_Number__c
    FROM POP_Bank_Details__c
    WHERE Case__c =: caseId OR (ForCorporate__c=true AND ForFinance__c=true AND Building_Location__c=NULL)];
    }

     
    List<Refernce> refernceList = new  List<Refernce>();
    if(!popList.isEmpty()){
        for(POP_Bank_Details__c pop : popList){
            Refernce refObj = new Refernce();
            refObj.Id = pop.Id;
            refObj.refNum = pop.Bank_Reference_number__c;
            if( pop.Bank_Account_number__c != null ){
                refObj.BankAccountNumber = String.valueOf(pop.Bank_Account_number__c);
            }
            if( pop.Beneficiary_Name__c != null ){
                refObj.BankAccountNumber = refObj.BankAccountNumber +'-'+ pop.Beneficiary_Name__c;
            }
            if( pop.Beneficiary_Bank__c != null ){
                refObj.BankAccountNumber = refObj.BankAccountNumber +'-'+ pop.Beneficiary_Bank__c;
            }
            String accountNumber=pop.Account_Number__c;
            if( pop.Beneficiary_Bank__c != null && accountNumber!=null && accountNumber.length()>6){
                refObj.BankAccountNumber = refObj.BankAccountNumber +'-'+ accountNumber.subString(accountNumber.length()-6, accountNumber.length());
            }
            if( pop.Beneficiary_Bank__c != null && accountNumber!=null &&  accountNumber.length()<6 && accountNumber.length()>0){
                refObj.BankAccountNumber = refObj.BankAccountNumber +'-'+ accountNumber;
            }
            refObj.BankAccountNumber=pop.Finance_Bank_Account_Number__c;
            refObj.DepositAmount = pop.Deposit_Amount__c;
            refObj.DepositDate = pop.Deposit_Date__c;
            refObj.DateRange = pop.Date_Range__c;
            refObj.DepositRange = pop.Deposit_Range__c;
            refernceList.add(refObj);
        }
    }
    return refernceList;
}


/**********************************************************************************************************************
Description : Method to return booking unit details
Parameter(s): Id
Return Type : List<buWrapper>
**********************************************************************************************************************/
@AuraEnabled(cacheable=true)
public static List<buWrapper> getBookingUnits(Id caseId) {
    List<SR_Booking_Unit__c> buList = [Select Id 
                                            , Registration_ID__c 
                                            , Amount_paid__c
                                            , Unit_Name__c
                                            , Booking_Unit__r.Registration_Status__c
                                        FROM SR_Booking_Unit__c
                                        WHERE Case__c =: caseId];
    List<buWrapper> buWrapperList = new  List<buWrapper>();
    if(!buList.isEmpty()){
        for(SR_Booking_Unit__c buObj : buList){
            buWrapper buWrapObj = new buWrapper();
            buWrapObj.Id =  buObj.Id;
            buWrapObj.regID = buObj.Registration_ID__c;
            buWrapObj.unitName = buObj.Unit_Name__c;
            buWrapObj.amount = buObj.Amount_paid__c;
            buWrapObj.regStatus = buObj.Booking_Unit__r.Registration_Status__c;
            buWrapperList.add(buWrapObj);
        }
    }
    return buWrapperList;
}

/**********************************************************************************************************************
Description : Method for Fetch AR callout
Parameter(s): String
Return Type : List<Receipts>
**********************************************************************************************************************/
@AuraEnabled(cacheable=true)
public static List<Receipts> getReceipts(String callOutReqParams) {

    List<Receipts> refernceListNew = new List<Receipts>();
    Credentials_Details__c creds = Credentials_Details__c.getInstance(SETTING_GET_AR);
    if(creds != null && String.isNotBlank(creds.Endpoint__c) 
        && String.isNotBlank(creds.Password__c) 
        && String.isNotBlank(creds.User_Name__c)) {
        String endpoint = creds.Endpoint__c;
        String username = creds.User_Name__c; 
        String password = creds.Password__c;

        String reqBody = GET_REQ_PREFIX + callOutReqParams + '}}';
        System.debug('reqBody-----'+reqBody);

        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http h = new Http();

        req.setEndpoint(endpoint);
        req.setMethod('POST');
        req.setTimeout(120000);

        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Accept', 'application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept-Language', 'en-US');
        req.setBody(reqBody);

        res = h.send(req);
        
        if( res != null && res.getBody() != null){
            System.debug( 'status = ' + res.getStatusCode( ) );
            System.debug( 'Response - -  ' + res );
            System.debug( 'Response body - -  ' + res.getBody( ) );

            JSON2ApexWrapper responseObj = ( JSON2ApexWrapper )System.JSON.deserialize( res.getBody(), JSON2ApexWrapper.class );

            if(responseObj.OutputParameters.X_RETURN_STATUS == 'S' 
                && responseObj.OutputParameters.X_RETURN_MESSAGE != FETCHED_0_RECORDS){
                for( X_RECEIPTS_DTL_TAB_ITEM objItem : responseObj.OutputParameters.X_RECEIPTS_DTL_TAB.X_RECEIPTS_DTL_TAB_ITEM ){
                    Receipts ref = new Receipts();
                    ref.isActive = false;
                    ref.DOC_SEQUENCE_VALUE = objItem.DOC_SEQUENCE_VALUE;
                    ref.BANK_REF_NUMBER = objItem.BANK_REF_NUMBER;
                    ref.RECEIPT_NUMBER = objItem.RECEIPT_NUMBER;
                    ref.REMARKS = objItem.REMARKS;
                    ref.RECEIPT_AMOUNT = objItem.RECEIPT_AMOUNT;
                    ref.BANK_ACCOUNT_NUM = objItem.BANK_ACCOUNT_NUM;
                    ref.RECEIPT_DATE = objItem.RECEIPT_DATE;
                    ref.CASH_RECEIPT_ID = objItem.CASH_RECEIPT_ID;
                    refernceListNew.add(ref);
                }       
            } 
            else if(responseObj.OutputParameters.X_RETURN_MESSAGE == FETCHED_0_RECORDS){
                Receipts ref = new Receipts();
                ref.REMARKS = '0 results';
                refernceListNew.add(ref);
            }
            else if(responseObj.OutputParameters.X_RETURN_STATUS != 'S'){
                Receipts ref = new Receipts();
                ref.REMARKS = 'Error';
                refernceListNew.add(ref);
            }
            
        }       
    }
    return refernceListNew;
    
}

/**********************************************************************************************************************
Description : Method to return added receipts
Parameter(s): String
Return Type : List<AddedReceipts>
**********************************************************************************************************************/
@AuraEnabled(cacheable=true)
public static List<AddedReceipts> getAddedReceipts(String rowFirst) {
    List<AddedReceipts> refernceList = (List<AddedReceipts>)JSON.deserialize(rowFirst,List<AddedReceipts>.class);
    return refernceList;
}

/**********************************************************************************************************************
Description : Method for apply receipt callout
Parameter(s): String, String, String
Return Type : String
**********************************************************************************************************************/
@AuraEnabled
public static String approvalCallout(String receiptDetails, String buDetails, String caseId) {

    List<Error_Log__c> listErrorLog = new List<Error_Log__c>();
    String response_msg = MSG_CONTACT_ADMIN;
    List<AddedReceipts> finalReceiptList = (List<AddedReceipts>)JSON.deserialize(receiptDetails,List<AddedReceipts>.class);
    List<buWrapper> finalBUtList = (List<buWrapper>)JSON.deserialize(buDetails,List<buWrapper>.class);
    try{   
        List<Case> caseList = [Select Id,CaseNumber from Case where Id=: caseId];   
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeFieldName('P_RECEIPT_REC');
        gen.writeStartObject();    
        gen.writeFieldName('P_RECEIPT_REC_ITEM');
        gen.writeStartArray();
        for(AddedReceipts objRep : finalReceiptList){
            gen.writeStartObject();
            gen.writeStringField('CASH_RECEIPT_ID', objRep.CASH_RECEIPT_ID);
            gen.writeStringField('DEPOSIT_DATE', objRep.DepositDate.split('T')[0]);
            gen.writeStringField('SR_NUMBER', caseList[0].CaseNumber);
            gen.writeEndObject();
        }
        gen.writeEndArray();
        gen.writeEndObject();
        gen.writeFieldName('P_SALESREG_REC');
        gen.writeStartObject();    
        gen.writeFieldName('P_SALESREG_REC_ITEM');
        gen.writeStartArray();
        for(buWrapper objBuWrap : finalBUtList){
            gen.writeStartObject();
            gen.writeStringField('REGISTRATION_ID', objBuWrap.regID);
            gen.writeNumberField('APPLY_AMOUNT', objBuWrap.amount);
            gen.writeEndObject();
        }
        gen.writeEndArray();
        gen.writeEndObject();
        gen.writeEndObject();
        String pretty = gen.getAsString();
        System.debug('pretty>>>>>'+pretty);
    
        String requestString = APPLY_REQ_PREFIX + pretty+ '}}';
        System.debug('requestString------'+requestString);
    
        Credentials_Details__c creds = Credentials_Details__c.getInstance(SETTING_APPLY_AR);
    
        if(creds != null && String.isNotBlank(creds.Endpoint__c) 
            && String.isNotBlank(creds.Password__c) 
            && String.isNotBlank(creds.User_Name__c)) {
            String endpoint = creds.Endpoint__c;
            String username = creds.User_Name__c; 
            String password = creds.Password__c;
    
            String reverseString = '{"APPLY_UNIDENTIFIED_RECEIPTS_Input":{"RESTHeader":{"Responsibility":"ONT_ICP_SUPER_USER","RespApplication":"ONT","SecurityGroup":"STANDARD","NLSLanguage":"AMERICAN"},"InputParameters":{"P_RECEIPT_REC":{"P_RECEIPT_REC_ITEM":[{"CASH_RECEIPT_ID":"3253778","DEPOSIT_DATE":"2019-07-23"}]},"P_SALESREG_REC":{"P_SALESREG_REC_ITEM":[{"REGISTRATION_ID":"111227","APPLY_AMOUNT":"547"}]}}}}';
            system.debug( 'reverseString : ' +  reverseString );
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http h = new Http();
    
            req.setEndpoint(endpoint);
            req.setMethod('POST');
            req.setTimeout(120000);
    
            Blob headerValue = Blob.valueOf(username + ':' + password);
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            req.setHeader('Authorization', authorizationHeader);
            req.setHeader('Accept', 'application/json');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Accept-Language', 'en-US');
            req.setBody(requestString);
             //if(!Test.isRunningTest()){        
                res = h.send(req);
            // }
               /* else{
                    
                   res =  '{ "OutputParameters" : { "@xmlns" : "http://xmlns.oracle.com/apps/ont/rest/XXDC_POP_RECEIPTS/apply_unidentified_receipts/", "@xmlns:xsi" : "http://www.w3.org/2001/XMLSchema-instance", "P_RECEPT_OUT_REC" : { "P_RECEPT_OUT_REC_ITEM" : [ { "X_CASH_RECEPT_ID" : "3308956", "X_URL" : "https://sftest.deeprootsurface.com/docs/e/65561599_3308956_RECEIPT.pdf", "X_STATUS" : "S", "X_ERROR_DESC" : "Receipt is applied successfully ..." } ] }, "X_RETURN_STATUS" : "S", "X_RETURN_MESSAGE" : "Applicatin is completed Successfully " } }';
                }*/
     
    
            if( res != null && res.getBody() != null) {
                System.debug( 'status = ' + res.getStatusCode( ) );
                System.debug( 'Response - -  ' + res );
                System.debug( 'Response body - -  ' + res.getBody( ) );
                Error_Log__c errorLog = new Error_Log__c();
                errorLog.Case__c = caseId;
                errorLog.Process_Name__c = 'POP Approval';
                errorLog.Service_Request_Body__c = requestString;
                errorLog.Service_Response_Body__c = res.getBody();
                listErrorLog.add(errorLog);
             
                JSON2ApexApprovalResponse responseObj = ( JSON2ApexApprovalResponse )System.JSON.deserialize( res.getBody(), JSON2ApexApprovalResponse.class );
                System.debug(responseObj.OutputParameters);
                System.debug(responseObj.OutputParameters.X_RETURN_STATUS);
                System.debug(responseObj.OutputParameters.X_RETURN_MESSAGE);
                if(responseObj != null && responseObj.OutputParameters.X_RETURN_STATUS == 'S' 
                && responseObj.OutputParameters.X_RETURN_MESSAGE.contains('completed Successfully')) {               
                    String wrapperStr = JSON.serialize(responseObj.OutputParameters.P_RECEPT_OUT_REC.P_RECEPT_OUT_REC_ITEM);
                    futureCallToOfficeURL(caseId,wrapperStr);
                    
                    response_msg = 'SUCCESS';
                }
                else if(responseObj.OutputParameters.X_RETURN_STATUS == 'E') {
                    //present error to user
                    System.debug('----'+responseObj.OutputParameters.X_RETURN_MESSAGE);
                    response_msg = responseObj.OutputParameters.X_RETURN_MESSAGE;
                    
                }
            }
        }else {
            response_msg = MSG_CREDS_NOT_CONFIGURED;
        }
      }
      catch(Exception ex) {
        Error_Log__c errorLog = new Error_Log__c();
        errorLog.Case__c = caseId;
        errorLog.Error_Details__c = ex.getMessage(); 
        listErrorLog.add(errorLog); 
        response_msg = ex.getMessage(); 
      }
      //insert error log
        if(!listErrorLog.isEmpty()) {
            insert listErrorLog;
        }
        return response_msg;
}

    @future(callout=true)
    public static void futureCallToOfficeURL(String caseId, String wrapperStr) {
        List<Task> objTask = new List<Task>();
        List<Case> myCase = new List<Case>();
        List<SR_Attachments__c> popReceipts = new List<SR_Attachments__c>();
        List<String> statuses = new List<String> { STATUS_REJECT , STATUS_REJECT_DUP , STATUS_CLOSED };
        List<Error_Log__c> listErrorLog = new List<Error_Log__c>();
        try{
            list<P_RECEPT_OUT_REC_ITEM> lstWrapperFromStr = (list<P_RECEPT_OUT_REC_ITEM>)JSON.deserialize(wrapperStr , list<P_RECEPT_OUT_REC_ITEM>.class);
            System.debug('lstWrapperFromStr'+ lstWrapperFromStr);
            if(String.isNotBlank(caseId)) {
                objTask = [SELECT Id
                                , WhatId
                                , Subject
                                , Assigned_User__c
                                , Process_Name__c
                                , Status 
                            FROM TASK 
                            WHERE WhatId = :caseId 
                            AND Subject = :TASK_SUBJECT 
                            AND Assigned_User__c = :USER_FINANCE 
                            AND Process_Name__c = :PROCESS_POP 
                            AND Status NOT IN :statuses 
                            ORDER BY CreatedDate DESC];
        
                myCase = [SELECT Id, Case_Summary__c,Rejection_Reason__c FROM Case WHERE Id = :caseId];
            }
              String officeURL = '';
              String receiptId = '';
             for(P_RECEPT_OUT_REC_ITEM outRec : lstWrapperFromStr) {
                        //send url to office 365 and store cash recep Id and 365url on task
                        System.debug('----'+outRec.X_CASH_RECEPT_ID);
                        System.debug('----'+outRec.X_URL);
                       if(!Test.isRunningTest()) {
                           if(outRec.X_URL != Null && outRec.X_CASH_RECEPT_ID != Null) {
                            String tempURl = UtillityForUpload.getOfficeURL(outRec.X_URL,'POPReceipt'+outRec.X_CASH_RECEPT_ID);
                            officeURL = tempURl+','+officeURL;
                            receiptId = outRec.X_CASH_RECEPT_ID + ',' +receiptId;
        
                            //store officeURL and cash receipt on financetask and close task
                            //myCase[0].Rejection_Reason__c = '';
                            myCase[0].Case_Summary__c = officeURL.removeEnd(',');
                            objTask[0].Cash_Receipt_Id__c = receiptId.removeEnd(',');
                        
                        //create SR attachment of receipt
                        if(String.isNotEmpty(tempURl)) {
                            SR_Attachments__c objAttach = new SR_Attachments__c ();
                            objAttach.Case__c = caseId;
                            objAttach.Attachment_URL__c = tempURl;                
                            objAttach.Name = 'POP receipt '+outRec.X_CASH_RECEPT_ID;
                            popReceipts.add(objAttach);
                        }
                       } 
                      }                    
               }
            //update doc urls on case
                if(!myCase.isEmpty()) {
                 if(!Test.isRunningTest()) {   update myCase;}
                }
                //close task
                objTask[0].Status = STATUS_CLOSED;
                if(!objTask.isEmpty()) { 
                 if(!Test.isRunningTest()) {   update objTask[0];}
                }
                //create sr attchment for receipts
                if(!popReceipts.isEmpty()) {
                    insert popReceipts;
                }
             }
             catch(Exception ex) {
                 Error_Log__c errorLog = new Error_Log__c();
                 errorLog.Case__c = caseId;
                 errorLog.Error_Details__c = ex.getMessage();
                 insert errorLog;
                 
             }
        }



/**********************************************************************************************************************
Description : Method to save bank account detail 
Parameter(s): List<AddedReceipts>
Return Type : Response
**********************************************************************************************************************/
    @AuraEnabled
    public static Response saveBankDetail(List<AddedReceipts> recipts) {
        system.debug('recipts '+recipts);
        Response response=new Response();
        response.isSuccess=true;
        List<POP_Bank_Details__c> popBankDetails=new List<POP_Bank_Details__c>();
        List<Integer> bankAccoundNumbers=new List<Integer>();
        for(AddedReceipts recipt:recipts){
            bankAccoundNumbers.add(Integer.valueOf(recipt.BankAccountNumber.substringBefore('-')));
        }
        List<POP_Bank_Details__c> Bankdetail=[Select id,Bank_Account_number__c, Beneficiary_Name__c, Beneficiary_Bank__c, Account_Number__c from POP_Bank_Details__c where Bank_Account_number__c IN : bankAccoundNumbers ];
        Map<Integer,POP_Bank_Details__c> popMap=new Map<Integer,POP_Bank_Details__c>();
        for(POP_Bank_Details__c popBank : Bankdetail){
            popMap.put(Integer.valueOf(popBank.Bank_Account_number__c) ,popBank);
            
        }
        
        for(AddedReceipts receipt : recipts){
            POP_Bank_Details__c popBankDetail=new POP_Bank_Details__c();
            popBankDetail.Id = receipt.Id;
            popBankDetail.Bank_Account_number__c = popMap.get(Integer.valueOf(receipt.BankAccountNumber.substringBefore('-'))).Bank_Account_number__c;
            popBankDetail.Beneficiary_Name__c= popMap.get(Integer.valueOf(receipt.BankAccountNumber.substringBefore('-'))).Beneficiary_Name__c;
            popBankDetail.Beneficiary_Bank__c =popMap.get(Integer.valueOf(receipt.BankAccountNumber.substringBefore('-'))).Beneficiary_Bank__c;
            popBankDetail.Account_Number__c = popMap.get(Integer.valueOf(receipt.BankAccountNumber.substringBefore('-'))).Account_Number__c ;
            popBankDetail.Bank_Reference_number__c=receipt.refNum;
            popBankDetail.Deposit_Amount__c =receipt.DepositAmount;
            popBankDetail.Deposit_Date__c = Date.valueOf(receipt.DepositDate) ;
            popBankDetail.Date_Range__c = receipt.DateRange;
            popBankDetail.Deposit_Range__c = receipt.DepositRange;
            popBankDetails.add(popBankDetail);
        }
        update popBankDetails;
        return response;
    }

public class Response {
    @AuraEnabled
    public Boolean isSuccess{ get; set; }
    @AuraEnabled
    public String errorMessage{ get; set; }

}

//wrapper for fetch AR response
public class JSON2ApexWrapper {
    public OutputParameters OutputParameters;
}

public class OutputParameters {
    public String xmlns;
    public String xmlnsxsi;
    public X_RECEIPTS_DTL_TAB X_RECEIPTS_DTL_TAB;
    public String X_RETURN_STATUS;
    public String X_RETURN_MESSAGE;
}

public class X_RECEIPTS_DTL_TAB {
    public List<X_RECEIPTS_DTL_TAB_ITEM> X_RECEIPTS_DTL_TAB_ITEM;
}

public class X_RECEIPTS_DTL_TAB_ITEM {
    public Integer DOC_SEQUENCE_VALUE;
    public String RECEIPT_NUMBER;
    public decimal RECEIPT_AMOUNT;
    public String RECEIPT_DATE;
    public String REMARKS;
    public String BANK_REF_NUMBER;
    public String BANK_ACCOUNT_NUM;
    public String CASH_RECEIPT_ID;
}

//wrapper for approval request
public class JSON2ApexApprovalReq {
    public APPLY_UNIDENTIFIED_RECEIPTS_Input APPLY_UNIDENTIFIED_RECEIPTS_Input;
}

public class P_SALESREG_REC {
    public List<P_SALESREG_REC_ITEM> P_SALESREG_REC_ITEM;
}

public class P_RECEIPT_REC {
    public List<P_RECEIPT_REC_ITEM> P_RECEIPT_REC_ITEM;
}

public class P_SALESREG_REC_ITEM {
    public String REGISTRATION_ID;
    public Decimal APPLY_AMOUNT;
}    

public class APPLY_UNIDENTIFIED_RECEIPTS_Input {
    public RESTHeader RESTHeader;
    public InputParameters InputParameters;
}

public class RESTHeader {
    public String Responsibility;
    public String RespApplication;
    public String SecurityGroup;
    public String NLSLanguage;
}

public class P_RECEIPT_REC_ITEM {
    public String CASH_RECEIPT_ID;
    public String DEPOSIT_DATE;
    
}

public class InputParameters {
    public P_RECEIPT_REC P_RECEIPT_REC;
    public P_SALESREG_REC P_SALESREG_REC;
}

//wrapper for approval response
public class JSON2ApexApprovalResponse { 
    public OutputParametersResp OutputParameters;
}

public class P_RECEPT_OUT_REC {
    public List<P_RECEPT_OUT_REC_ITEM> P_RECEPT_OUT_REC_ITEM;
}

public class P_RECEPT_OUT_REC_ITEM {
    public String X_CASH_RECEPT_ID;
    public String X_URL;
    public String X_STATUS;
    public String X_ERROR_DESC;
}

public class OutputParametersResp {
    public String appxmlns;
    public String appxmlnsxsi;
    public P_RECEPT_OUT_REC P_RECEPT_OUT_REC;
    public String X_RETURN_STATUS;
    public String X_RETURN_MESSAGE;
}

public class buWrapper {
    @AuraEnabled
    public String Id{ get; set; }
    @AuraEnabled
    public String regID{ get; set; }
    @AuraEnabled
    public String unitName{ get; set; }
     @AuraEnabled
    public Decimal amount{ get; set; }  
    @AuraEnabled
    public String regStatus{ get; set; }
}
public class AddedReceipts{
    @AuraEnabled
    public Id Id { get; set; }
    @AuraEnabled
    public Integer DOC_SEQUENCE_VALUE { get; set; }
    @AuraEnabled
    public String refNum{ get; set; }
    @AuraEnabled
    public String BankAccountNumber{ get; set; }
     @AuraEnabled
    public String DepositDate{ get; set; }
     @AuraEnabled
    public String DateRange{ get; set; }
    @AuraEnabled
    public Decimal DepositAmount{ get; set; }
    @AuraEnabled
    public String receiptNum{ get; set; }
    @AuraEnabled
    public String CASH_RECEIPT_ID{ get; set; }
     @AuraEnabled
    public String REMARKS{ get; set; }
    @AuraEnabled
    public String DepositRange{ get; set; }
   
}  
public class Refernce{
    @AuraEnabled
    public String Id{ get; set; }
    @AuraEnabled
    public String refNum{ get; set; }
    @AuraEnabled
    public String BankAccountNumber{ get; set; }
     @AuraEnabled
    public Date DepositDate{ get; set; }
     @AuraEnabled
    public String DateRange{ get; set; }
    @AuraEnabled
    public Decimal DepositAmount{ get; set; }
    @AuraEnabled
    public String DepositRange{ get; set; }
    //@AuraEnabled
    //public String uid{ get; set; }    
}   
public class Receipts{
    @AuraEnabled
    public Integer DOC_SEQUENCE_VALUE { get; set; }
    @AuraEnabled
    public String BANK_REF_NUMBER{ get; set; }
    @AuraEnabled
    public String RECEIPT_NUMBER{ get; set; }
     @AuraEnabled
    public String REMARKS{ get; set; }
     @AuraEnabled
    public decimal RECEIPT_AMOUNT{ get; set; }
    @AuraEnabled
    public String BANK_ACCOUNT_NUM{ get; set; }
    @AuraEnabled
    public String RECEIPT_DATE{ get; set; }
    @AuraEnabled
    public String CASH_RECEIPT_ID{ get; set; }
     @AuraEnabled
    public Boolean isActive{ get; set; }
} 
}