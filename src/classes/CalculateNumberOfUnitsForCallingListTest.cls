/*
* Description - Test class developed for CalculateNumberOfUnitsForCallingList
*
* Version            Date            Author            Description
* 1.0              07/12/2017        Arjun Khatri      Initial Draft
* 1.1              08/01/2017                          Fixes for Issue 694
*/
@isTest
private class CalculateNumberOfUnitsForCallingListTest {
   public static final String strBookingUnitActiveStatus = 'Agreement executed by DAMAC';
    public static Id collectioncallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Collections Calling List').RecordTypeId;
      
    public static testMethod void calculateNumberOfUnitsTest(){
         List<TriggerOnOffCustomSetting__c>settingLst = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst.add(newSetting);
        insert settingLst;
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;

        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( accountobj[0].Id, dealSR.Id, 1 );
        insert lstBookings;

        List<Booking_Unit__c> bookingUnitsObj = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        insert bookingUnitsObj;

        
        List<Calling_List__c> listCallingList = new List<Calling_List__c> ();
       Calling_List__c collectionCallInstManagement = new Calling_List__c(RecordTypeId = collectioncallingRecordTypeId,
                                                                         Call_Outcome__c = 'Recovery list',
                                                                         Account__c = accountobj[0].id,
                                                                         Customer_Flag__c = true,
                                                                         IsHideFromUI__c = false,
                                                                         Registration_ID__c = '1000',
                                                                         Party_ID__c = '1177923'    );
       listCallingList.add(collectionCallInstManagement);
       Calling_List__c collectionCallInstManagement2 = new Calling_List__c(RecordTypeId = collectioncallingRecordTypeId,
                                                                         Call_Outcome__c = 'Recovery list',
                                                                         Account__c = accountobj[0].id,
                                                                         Customer_Flag__c = true,
                                                                         IsHideFromUI__c = false,
                                                                         Registration_ID__c = '10300',
                                                                         Party_ID__c = '11723'  );
        listCallingList.add(collectionCallInstManagement2);
        insert listCallingList;
        Test.startTest();
    
            CalculateNumberOfUnitsForCallingList.calculateNumberOfUnits(listCallingList);
 
        Test.stopTest();        
    } 
}