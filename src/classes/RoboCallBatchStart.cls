/***********************************************************************************************
 * Ver       Date            Author                 Modification
 *==========================================================================================
 * 1.0      4/9/2019        Shashank Maind          Initial Version
 * 1.1      29/09/2019      Aishwarya Todkar        Added check for Rental Pool
 * 1.2      09/10/2019      Aishwarya Todkar        Segregated functionality of adding calls to 
                                                    Campaign and triggering Robo calls from two
                                                    different custom buttons.
**********************************************************************************************/
public with sharing class RoboCallBatchStart {

    private final Robocall_Campaign__c acct;
    
    public RoboCallBatchStart(ApexPages.StandardController stdController){
        System.debug('-->> Inside Batch: ' );
        this.acct = (Robocall_Campaign__c)stdController.getRecord();
        System.debug('-->> acct : ' + acct );
        
    }

/***********************************************************************************************
Description : Method to add calls on campaign; Executed from 'Add RoboCalls To This Campaign' 
                custom button.
************************************************************************************************/
    public void addCallsToCampaign() {

        //Get Campaign
        Robocall_Campaign__c objRobocall_Campaign = [SELECT 
                                                        Id
                                                        , Campaign_Id__c
                                                    FROM 
                                                        Robocall_Campaign__c
                                                    WHERE 
                                                        Id = :acct.Id];
        System.debug('-->> objRobocall_Campaign : ' + objRobocall_Campaign);
        if(objRobocall_Campaign.Campaign_Id__c != NULL) {
            System.debug('-->> Campaign_Id__c : ' + objRobocall_Campaign.Campaign_Id__c);
            System.debug('label Amount: ' + Label.Robo_Call_FM_Outstanding_Amount);
            
            //Get FM Outstanding amount and Date
            String FmOutstandingAmount_str = Label.Robo_Call_FM_Outstanding_Amount;
            if(!FmOutstandingAmount_str.endsWithIgnoreCase('.00')) {
                FmOutstandingAmount_str += '.00';
            }
            System.debug('FmOutstandingAmount_str :' + FmOutstandingAmount_str);

            Decimal FmOutstandingAmount =Decimal.valueOf(FmOutstandingAmount_str);
            System.debug('FmOutstandingAmount :' + FmOutstandingAmount);

            //Get Booking units
            String strQuery = 'SELECT Id, FM_Outstanding_Amount__c, FM_Outstanding_Amount_Formula__c ,Booking__r.Account__c,'
                            + ' Booking__r.Account__r.Mobile__c, Booking__r.Account__r.Mobile_Phone_Encrypt__pc, '
                            + ' Booking__r.Account__r.IsPersonAccount, FM_Outstanding_Date__c ,Unit_Name__c, Owner__r.Phone, '
                            + ' Property_Name__c, SC_From_RentalIncome__c,Unit_Active__c FROM Booking_Unit__c ' 
                            + ' WHERE FM_Outstanding_Amount_Formula__c >= '
                            +   FmOutstandingAmount 
                            + ' AND Unit_Active__c = \'Active\' AND SC_From_RentalIncome__c = false '
                            + ' AND Booking__c != null AND Booking__r.Account__c != null '
                            + ' AND (CM_Units__c = NULL OR CM_Units__c = \' \') AND ByPassFMReminder__c = False AND (Handover_Flag__c != \'N\' '
                            + ' OR Early_Handover__c = TRUE)'
                            + ' AND ((Booking__r.Account__r.Mobile__c != null AND Booking__r.Account__r.Mobile__c != \'0\') '
                            + ' OR ( Booking__r.Account__r.Mobile_Phone_Encrypt__pc != null AND  Booking__r.Account__r.Mobile_Phone_Encrypt__pc != \'0\'))'
                            + ' AND Rental_Pool__c = false ';
            System.debug('strQuery :' + strQuery);                
            List<Booking_Unit__c > listBU = database.query(strQuery);
            List<Added_Call__c> listAdded_Call = new List<Added_Call__c>();
            String strMobileNumber = '';

            if(listBU != null) {
                for(Booking_Unit__c objBooking_Unit: listBU) {
                    Added_Call__c objAdded_Call = new Added_Call__c();
                    if(objBooking_Unit.FM_Outstanding_Date__c == null) {
                        objBooking_Unit.FM_Outstanding_Date__c = Date.today();
                    }
                    
                    String inputDataStr = '';

                    if((objBooking_Unit.Booking__r.Account__r.Mobile_Phone_Encrypt__pc != NULL 
                        || objBooking_Unit.Booking__r.Account__r.Mobile__c != NULL) 
                    &&  objBooking_Unit.FM_Outstanding_Date__c != NULL) {
                        System.debug('-->> FM_Outstanding_Date__c:'+ objBooking_Unit.FM_Outstanding_Date__c);
                        String dayString = objBooking_Unit.FM_Outstanding_Date__c.format();
                        System.debug('-->> dayString:'+ dayString);

                        //Check If person account
                        if(objBooking_Unit.Booking__r.Account__r.IsPersonAccount == TRUE){
                            System.debug('-->> Mobile_Phone_Encrypt__pc: ' 
                                + objBooking_Unit.Booking__r.Account__r.Mobile_Phone_Encrypt__pc);
                            inputDataStr = objBooking_Unit.Booking__r.Account__r.Mobile_Phone_Encrypt__pc;
                            objAdded_Call.Phone__c = objBooking_Unit.Booking__r.Account__r.Mobile_Phone_Encrypt__pc;
                            strMobileNumber = objBooking_Unit.Booking__r.Account__r.Mobile_Phone_Encrypt__pc;
                        }
                        else {
                            inputDataStr = objBooking_Unit.Booking__r.Account__r.Mobile__c;
                            objAdded_Call.Phone__c =  objBooking_Unit.Booking__r.Account__r.Mobile__c;
                            strMobileNumber = objBooking_Unit.Booking__r.Account__r.Mobile__c;
                        }

                        inputDataStr += ',' +  objBooking_Unit.FM_Outstanding_Amount_Formula__c 
                                    + ',' + objBooking_Unit.Property_Name__c + ' '
                                    + objBooking_Unit.Unit_Name__c + ',' +  dayString + ';';
                        System.debug('-->> inputDataStr: ' + inputDataStr);

                        //Create Added Call Object Record
                        objAdded_Call.Account__c = objBooking_Unit.Booking__r.Account__c;
                        objAdded_Call.Booking_Unit__c = objBooking_Unit.Id;
                        objAdded_Call.Campaign_Robo__c = objRobocall_Campaign.Id;
                        objAdded_Call.Order_Id__c = objRobocall_Campaign.Campaign_Id__c;
                        objAdded_Call.Property_Name__c = objBooking_Unit.Property_Name__c != null ? objBooking_Unit.Property_Name__c : '';
                        objAdded_Call.Unit_Name__c = objBooking_Unit.Unit_Name__c != null ? objBooking_Unit.Unit_Name__c : '';
                        objAdded_Call.FM_Outstanding_Amount__c = objBooking_Unit.FM_Outstanding_Amount_Formula__c;
                        objAdded_Call.FM_Outstanding_Date__c = objBooking_Unit.FM_Outstanding_Date__c;
                        objAdded_Call.Input_String_For_Add_Call__c = inputDataStr;
                        listAdded_Call.add( objAdded_Call );
                    }
                }
            }
            if(!listAdded_Call.isEmpty()){
                insert listAdded_Call;
                ApexPages.addMessage(
                    new ApexPages.Message(
                        ApexPages.Severity.INFO,'Calls added Successfully.'
                    )
                );
            }
        } 
        else {
            ApexPages.addMessage(
                new ApexPages.Message(
                    ApexPages.Severity.ERROR
                    , 'Campaign Id Not Set. You cant Add on this record.'
                )
            );
        }
    }

/*************************************************************************************************
Description : Method to trigger calls to customers; Executed from 'Place Robo Call' custom button.
**************************************************************************************************/
    public void placeRoboCall() {
         
        if( acct != null) {

            //Get Campaign
            List<Robocall_Campaign__c> lstRobocall_Campaign = [SELECT 
                                                            Id
                                                            , Campaign_Id__c 
                                                        FROM 
                                                            Robocall_Campaign__c 
                                                        WHERE 
                                                            Id = :acct.Id
                                                        AND 
                                                            Campaign_Id__c != null];

            if( !lstRobocall_Campaign.isEmpty() && lstRobocall_Campaign.size() > 0 ) {
                String addCallString = '';
                String orderId = lstRobocall_Campaign[0].Campaign_Id__c;//'9503730';//

                if( String.isNotBlank( orderId )) {

                    //Get added calls related to Campaign
                    for(Added_Call__c addedCallObj : [SELECT
                                                        Id
                                                        , Phone__c
                                                        , Account__c
                                                        , Booking_Unit__c
                                                        , Campaign_Robo__c
                                                        , Order_Id__c
                                                        , Property_Name__c
                                                        , Unit_Name__c
                                                        , FM_Outstanding_Amount__c
                                                        , FM_Outstanding_Date__c
                                                        , Input_String_For_Add_Call__c
                                                    FROM
                                                        Added_Call__c
                                                    WHERE
                                                        Campaign_Robo__c =: acct.Id]) {

                        if( String.isNotBlank( addedCallObj.Input_String_For_Add_Call__c ) )
                            addCallString += addedCallObj.Input_String_For_Add_Call__c;
                    }//End For Loop

                    System.debug( 'addCallString == ' + addCallString);
                    if( String.isNotBlank( addCallString ) ) {
                        addCallString = addCallString.removeEnd(';');
                        try {

                            //Call API to trigger call
                            if( !Test.isRunningTest() )
                                RoboCallAPIs.addCalls( orderId, addCallString );
                                
                            ApexPages.addMessage(
                                new ApexPages.Message(
                                    ApexPages.Severity.INFO,'Robo call Placed Successfully.'
                                )
                            );
                        }
                        catch( Exception ex) {
                            ApexPages.addMessage(
                                new ApexPages.Message(
                                    ApexPages.Severity.ERROR,ex.getMessage()
                                )
                            );
                        }
                    }//End addCallString if
                }// End OrderId If
            }//End lstRobocall_Campaign if
        }//End acct if
    }//End placeRoboCall method
}