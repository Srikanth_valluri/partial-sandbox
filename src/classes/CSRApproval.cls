public without sharing class CSRApproval {
    
    public static map<ID,ID> mapManagerID ;
    public static map<ID,String> mapManagerRole;
    public static map<ID,String> mapManagerName;
    public static map<ID,Boolean> mapUserStatus;
    public static boolean fromPB = false;
    @InvocableMethod(label='getCase' )
    
    public static void getCase(List<Id> ids) {
       Case objCase =[Select id,accountId,Approving_User_Id__c,Approving_Authorities__c,Approving_User_Role__c,Approving_User_Name__c,OwnerId
       FROM Case
       Where Id =: ids[0] Limit 1 ];
       
       // create field history record
       
       List<GenericUtility.HistoryWrapper> lstFieldHistoryWrapper = new List<GenericUtility.HistoryWrapper>();
       
       List<Task> reviewTask = [SELECT Id,
                                       Owner.Name,
                                       OwnerId,
                                       CreatedDate,
                                       Description,
                                       Subject
                                FROM   Task
                                WHERE  WhatId =: objCase.Id
                                AND    Subject = 'Review the Case Summary SR'];
       
       if(reviewTask != null && reviewTask.size() > 0) {
            for(CaseSummaryRequest__c caseSummaryObj : [SELECT Id,
                                                               Manager_Approval_Assignment_Date__c,
                                                               Director_Approval_Assignment_Date__c,
                                                               HOD_Approval_Assignment_Date__c,
                                                               Committee_Approval_Assignment_Date__c
                                                        FROM   CaseSummaryRequest__c
                                                        WHERE  Case__c =: objCase.Id]) {
                //caseSummaryObj.Committee_Approval_Assignment_Date__c = system.now();
                GenericUtility.HistoryWrapper historyWrapInst= new GenericUtility.HistoryWrapper();
                historyWrapInst.accountId = objCase.accountId;
                historyWrapInst.appproverRole = 'Review Team';
                historyWrapInst.approverApprovalDate = System.now();
                historyWrapInst.approverAssignmentDate = reviewTask[0].CreatedDate;
                historyWrapInst.approverComments = reviewTask[0].Description;
                historyWrapInst.approverId = userinfo.getuserid();
                historyWrapInst.approverName = userinfo.getName();
                historyWrapInst.approverStatus = 'Approved';
                historyWrapInst.caseSummaryRequest = caseSummaryObj.Id;
                lstFieldHistoryWrapper.add(historyWrapInst);
            }
            GenericUtility.HistoryWrapper historyWrapInst= new GenericUtility.HistoryWrapper();
            historyWrapInst.accountId = objCase.accountId;
            historyWrapInst.appproverRole = 'Review Team';
            historyWrapInst.approverApprovalDate = System.now();
            historyWrapInst.approverAssignmentDate = reviewTask[0].CreatedDate;
            historyWrapInst.approverComments = reviewTask[0].Description;
            historyWrapInst.approverId = userinfo.getuserid();
            historyWrapInst.approverName = userinfo.getName();
            historyWrapInst.approverStatus = 'Approved';
            historyWrapInst.caseId = objCase.Id;
            lstFieldHistoryWrapper.add(historyWrapInst);
            
            GenericUtility genUtil = new GenericUtility();
            genUtil.createTrackingHistory(lstFieldHistoryWrapper);
        }
        
       update updateCase(objCase);
       fromPB = true;
    }
    
    
    public static Case updateCase(Case objCase){    
        
        String strCommitteeName = Label.CSR_Committee_Name_Id.subStringbefore('-');
        String strCommitteeId = Label.CSR_Committee_Name_Id.subStringafter('-');
        String strHODName = Label.CSR_HOD_Name_Id.subStringbefore('-');
        String strHODId = Label.CSR_HOD_Name_Id.subStringafter('-');
        
        
            
        List<CaseSummaryRequest__c> caseSummaryRequestList = [SELECT Id,
                                                                     Manager_Approval_Assignment_Date__c,
                                                                     Director_Approval_Assignment_Date__c,
                                                                     HOD_Approval_Assignment_Date__c,
                                                                     Committee_Approval_Assignment_Date__c
                                                              FROM   CaseSummaryRequest__c
                                                              WHERE  Case__c =: objCase.Id];
        
        System.debug('objCase=='+objCase);
        list<User> lstActiveUser = new list<User>();
        mapManagerID = new map<ID,ID>();
        mapManagerRole = new map<ID,String>();
        mapManagerName = new map<ID,String>();
        mapUserStatus = new map<ID,Boolean>();
        lstActiveUser = [Select Id,Name,isActive,Managerid,UserType,UserRole.Name,UserRoleID
                                   from User Where IsPortalEnabled = false AND isActive = true];
                                   //where isActive = true];

        System.debug('--lstActiveUser static-'+lstActiveUser.Size()+'----'+lstActiveUser);
        for(User objUser : lstActiveUser){
            if(objUser.managerid != null){
                mapManagerID.put(objUser.id,objUser.managerid);
            }
            if(objUser.UserRoleID != null){
                mapManagerRole.put(objUser.id,objUser.UserRole.Name);
                mapManagerName.put(objUser.id,objUser.Name);
                mapUserStatus.put(objUser.id, objUser.isActive);
            }

        }
        system.debug('--mapManagerID--'+mapManagerID);
        system.debug('--mapManagerRole--'+mapManagerRole);
        system.debug('--mapManagerName--'+mapManagerName);
        system.debug('--mapUserStatus--'+mapUserStatus);

        
        
        if(objCase.Approving_Authorities__c != null) {
            
            
            Boolean checkManager = true;
            List<String> lstAuth = new List<String>();
            lstAuth = objCase.Approving_Authorities__c.split(',');
            System.debug('objCase.Approving_Authorities__c'+ objCase.Approving_Authorities__c);
            System.debug('lstAuth'+ lstAuth);
            for (Integer i = 0; i < lstAuth.size()-1; i++) {
                System.debug('lstAuth[i]'+ lstAuth[i]);
                System.debug('lstAuth[i]'+ lstAuth[i+1]);
                 if (lstAuth[i] == objCase.Approving_User_Role__c && lstAuth[i+1] == 'Committee') {
                    objCase.Approving_User_Id__c = strCommitteeId;
                    objCase.Approving_User_Role__c = 'Committee';
                    objCase.Approving_User_Name__c = strCommitteeName;
                    checkManager = false;
                    for(CaseSummaryRequest__c caseSummaryObj : caseSummaryRequestList) {
                            caseSummaryObj.Committee_Approval_Assignment_Date__c = system.now();
                            caseSummaryObj.Committee_Approval_Date__c = Null;
                    }
                    break;
                }/*lse if ( lstAuth[i] == objCase.Approving_User_Role__c && lstAuth[i+1] == 'HOD'){
                    objCase.Approving_User_Id__c = strHODId;
                    objCase.Approving_User_Role__c = 'HOD';
                    objCase.Approving_User_Name__c = strHODName;
                    checkManager = false;
                    for(CaseSummaryRequest__c caseSummaryObj : caseSummaryRequestList) {
                            caseSummaryObj.HOD_Approval_Assignment_Date__c = system.now();
                    }
                    break;
                }*/
                else{
                    checkManager = true;  
                }
            }
        
            
            
            if (checkManager) {
                system.debug('if 2');
                Id idApprovingUserId;
                //Id idApprovingUserId = (String.isNotBlank(objCase.Approving_User_Id__c) && !objCase.Approving_User_Id__c.containsIgnoreCase(Label.CSR_Queue_label)  )?
                //objCase.Approving_User_Id__c : objCase.OwnerId;
                
                if( String.isNotBlank(objCase.Approving_User_Id__c) ) {
                    if(!objCase.Approving_User_Id__c.containsIgnoreCase(Label.CSR_Queue_label) && !fromPB ) {
                        idApprovingUserId = objCase.Approving_User_Id__c;
                    } else {
                        idApprovingUserId = objCase.OwnerId;
                    }
                }
                else {
                    idApprovingUserId = objCase.OwnerId;
                }
                
                idApprovingUserId = getApprovingUserId(idApprovingUserId);
                system.debug('mapManagerID '+ mapManagerID);
                system.debug('---mapManagerID ---  '+ mapManagerID.get(idApprovingUserId) );
                system.debug('---mapManagerID ---  '+ mapManagerRole.get(mapManagerID.get(idApprovingUserId)) );

                Id idManager = mapManagerID.containsKey(idApprovingUserId)? mapManagerID.get(idApprovingUserId ) : NULL;
                String strManagerRole = mapManagerRole.containsKey(idManager)? mapManagerRole.get(idManager) : '';
                
                
                if(strManagerRole.containsIgnoreCase('Committee')) {
                    objCase.Approving_User_Id__c = strCommitteeId;
                    objCase.Approving_User_Role__c = 'Committee';
                    objCase.Approving_User_Name__c = strCommitteeName;
                    for(CaseSummaryRequest__c caseSummaryObj : caseSummaryRequestList) {
                            caseSummaryObj.Committee_Approval_Assignment_Date__c = system.now();
                            caseSummaryObj.Committee_Approval_Date__c = Null;
                    }
                    
                }
                else{
                    strManagerRole = RoleUtility.getSalesforceRole(strManagerRole);
                    system.debug('strManagerRole  '+strManagerRole);
                    if(objCase.Approving_Authorities__c.containsIgnoreCase(strManagerRole)) {
                        system.debug('objCase === '+objCase );
                        objCase.Approving_User_Id__c = idManager;
                        objCase.Approving_User_Role__c = strManagerRole;
                        objCase.Approving_User_Name__c = mapManagerName.get(idManager);
                        if(strManagerRole.containsIgnoreCase('Manager')) {
                            for(CaseSummaryRequest__c caseSummaryObj : caseSummaryRequestList) {
                                caseSummaryObj.Manager_Approval_Assignment_Date__c = system.now();
                                caseSummaryObj.Manager_Approval_Date__c = Null;
                            }                           
                        }
                        else if(strManagerRole.containsIgnoreCase('Director')) {
                            for(CaseSummaryRequest__c caseSummaryObj : caseSummaryRequestList) {
                                caseSummaryObj.Director_Approval_Assignment_Date__c = system.now();
                                caseSummaryObj.Director_Approval_Date__c = Null;
                            }                           
                        }else if(strManagerRole.containsIgnoreCase('HOD')) {
                            for(CaseSummaryRequest__c caseSummaryObj : caseSummaryRequestList) {
                                caseSummaryObj.HOD_Approval_Assignment_Date__c = system.now();
                                caseSummaryObj.HOD_Approval_Date__c = Null;
                            }                           
                        }
            
                
                    }/*else if(objCase.Approving_Authorities__c.containsIgnoreCase('HOD')) {
                        objCase.Approving_User_Id__c = strHODId;
                        objCase.Approving_User_Role__c = 'HOD';
                        objCase.Approving_User_Name__c = strHODName;
                        for(CaseSummaryRequest__c caseSummaryObj : caseSummaryRequestList) {
                            caseSummaryObj.HOD_Approval_Assignment_Date__c = system.now();
                        }
                    }*/
                    
                }
            
            }
      
            
            
            
        }

        update caseSummaryRequestList;
        return objCase;

    }
    
    
    // Method to be called recursively to identify active manager
    private  static Id getApprovingUserId(Id idUser) {
        system.debug('*****idUser*****'+idUser);
        if(mapManagerID.containsKey(idUser)) {
            if(mapUserStatus.containsKey(mapManagerID.get(idUser)) && !mapUserStatus.get(mapManagerID.get(idUser))) {
                idUser = getApprovingUserId(mapManagerID.get(idUser));
            }
        }
        return idUser;
    }


}