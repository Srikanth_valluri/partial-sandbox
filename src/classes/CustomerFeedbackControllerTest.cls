@isTest
private class CustomerFeedbackControllerTest{
    
    @isTest
    static void itShouldHappy() {
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        Account acctIns = TestDataFactoryFM.createAccount();
        insert acctIns;
        
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
       
        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;
        
        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns.Inventory__c = invObj.id;
        insert buIns;
        
        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;
         
        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        fmUser.fm_role__c ='Property Manager';
        insert fmUser;

		FM_User__c fmUser1= TestDataFactoryFM.createFMUser(u,locObj);
        fmUser1.fm_role__c ='Property Director';
        insert fmUser1;

        recordType suggestionRT = [SELECT Id,
                      developername
                   FROM RecordType 
                   WHERE SObjectType = 'FM_Case__c' 
                   AND developername ='Suggestion'
                   LIMIT 1];
        system.debug('suggestionRT'+suggestionRT);

        FM_Case__c fmCaseObj=new FM_Case__c();        
        fmCaseObj.Description__c='test';
        fmCaseObj.Suggestion_Type__c = 'Common Area';
        fmCaseObj.Suggestion_Sub_Type__c = 'Reception'; 
        fmCaseObj.recordTypeId = suggestionRT.id;
        fmCaseObj.Booking_Unit__c = buIns.id;
        fmCaseObj.Status__c = 'Draft Request';
		fmCaseObj.Feedback_to_customer__c ='test';
		fmCaseObj.Request_Type__c = 'Suggestion';
		fmCaseObj.Submission_Datetime__c = system.today();
		//fmCaseObj.Customer_Experience__c = 'Satisfied';
        insert fmCaseObj;

        PageReference currentPage = Page.CustomerFeedbackPage;
        currentPage.getParameters().put('id', fmCaseObj.id);
        Test.setCurrentPage(currentPage);

        Test.startTest();
			ApexPages.StandardController sc = new ApexPages.StandardController(fmCaseObj);
            CustomerFeedbackController controller = new CustomerFeedbackController(sc);
			controller.isHappy = true;
            controller.submitFeedback();
        Test.stopTest();
    }

	@isTest
    static void itShouldSatisfied() {
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        Account acctIns = TestDataFactoryFM.createAccount();
        insert acctIns;
        
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
       
        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;
        
        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns.Inventory__c = invObj.id;
        insert buIns;
        
        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;
         
        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        fmUser.fm_role__c ='Property Manager';
        insert fmUser;

		FM_User__c fmUser1= TestDataFactoryFM.createFMUser(u,locObj);
        fmUser1.fm_role__c ='Property Director';
        insert fmUser1;

        recordType suggestionRT = [SELECT Id,
                      developername
                   FROM RecordType 
                   WHERE SObjectType = 'FM_Case__c' 
                   AND developername ='Suggestion'
                   LIMIT 1];
        system.debug('suggestionRT'+suggestionRT);

        FM_Case__c fmCaseObj=new FM_Case__c();        
        fmCaseObj.Description__c='test';
        fmCaseObj.Suggestion_Type__c = 'Common Area';
        fmCaseObj.Suggestion_Sub_Type__c = 'Reception'; 
        fmCaseObj.recordTypeId = suggestionRT.id;
        fmCaseObj.Booking_Unit__c = buIns.id;
        fmCaseObj.Status__c = 'Draft Request';
		fmCaseObj.Feedback_to_customer__c ='test';
		fmCaseObj.Request_Type__c = 'Suggestion';
		fmCaseObj.Submission_Datetime__c = system.today();
		//fmCaseObj.Customer_Experience__c = 'Satisfied';
        insert fmCaseObj;

        PageReference currentPage = Page.CustomerFeedbackPage;
        currentPage.getParameters().put('id', fmCaseObj.id);
        Test.setCurrentPage(currentPage);

        Test.startTest();
			ApexPages.StandardController sc = new ApexPages.StandardController(fmCaseObj);
            CustomerFeedbackController controller = new CustomerFeedbackController(sc);
			controller.isSatisfied = true;
            controller.submitFeedback();
        Test.stopTest();
    }

	@isTest
    static void itShouldNotSatisfied() {
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        Account acctIns = TestDataFactoryFM.createAccount();
        insert acctIns;
        
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
       
        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;
        
        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns.Inventory__c = invObj.id;
        insert buIns;
        
        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;
         
        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        fmUser.fm_role__c ='Property Manager';
        insert fmUser;

		FM_User__c fmUser1= TestDataFactoryFM.createFMUser(u,locObj);
        fmUser1.fm_role__c ='Property Director';
        insert fmUser1;

        recordType suggestionRT = [SELECT Id,
                      developername
                   FROM RecordType 
                   WHERE SObjectType = 'FM_Case__c' 
                   AND developername ='Suggestion'
                   LIMIT 1];
        system.debug('suggestionRT'+suggestionRT);

        FM_Case__c fmCaseObj=new FM_Case__c();        
        fmCaseObj.Description__c='test';
        fmCaseObj.Suggestion_Type__c = 'Common Area';
        fmCaseObj.Suggestion_Sub_Type__c = 'Reception'; 
        fmCaseObj.recordTypeId = suggestionRT.id;
        fmCaseObj.Booking_Unit__c = buIns.id;
        fmCaseObj.Status__c = 'Draft Request';
		fmCaseObj.Feedback_to_customer__c ='test';
		fmCaseObj.Request_Type__c = 'Suggestion';
		fmCaseObj.Submission_Datetime__c = system.today();
		//fmCaseObj.Customer_Experience__c = 'Satisfied';
        insert fmCaseObj;

        PageReference currentPage = Page.CustomerFeedbackPage;
        currentPage.getParameters().put('id', fmCaseObj.id);
        Test.setCurrentPage(currentPage);

        Test.startTest();
			ApexPages.StandardController sc = new ApexPages.StandardController(fmCaseObj);
            CustomerFeedbackController controller = new CustomerFeedbackController(sc);
			controller.isNotSatisfied = true;
            controller.submitFeedback();
        Test.stopTest();
    }
}