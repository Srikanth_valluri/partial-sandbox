@RestResource(urlMapping='/SaveJointBuyerDetails/*')
 Global class SaveJointBuyerDetails
 {
     @HtTPPost
    Global static Case SaveJointBuyersDetails(JoinBuyerWraper joinBuyerWraper)
    {
     Case cocdSR=new Case();
       try{
       
        cocdSR.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(joinBuyerWraper.RecordType).getRecordTypeId();
        Group g=[select Id,name from Group where Type = 'Queue' and name='Contact Center Queue'];
         cocdSR.AccountId =joinBuyerWraper.AccountID;
         cocdSR.Status=joinBuyerWraper.status;
         cocdSR.Origin=joinBuyerWraper.origin;
         cocdSR.Address__c = joinBuyerWraper.address1;
         
        cocdSR.Address_2__c =joinBuyerWraper.address2;
        cocdSR.Address_3__c =joinBuyerWraper.address3;
        cocdSR.Address_4__c =joinBuyerWraper.address4;
         cocdSR.City__c =joinBuyerWraper.city;
         cocdSR.State__c = joinBuyerWraper.state;
         cocdSR.Country__c = joinBuyerWraper.country;
         cocdSR.Postal_Code__c = joinBuyerWraper.postalCode;
         cocdSR.Contact_Email__c = joinBuyerWraper.email;
         cocdSR.Mobile_Country_Code__c =joinBuyerWraper.mobileCountryCode;
         cocdSR.Contact_Mobile__c =joinBuyerWraper.phone;
         cocdSR.Passport_File_URL__c =joinBuyerWraper.PassportFileUrl;
          cocdSR.Additional_Doc_File_URL__c =joinBuyerWraper.AdditionalDocFileUrl;
           cocdSR.OD_File_URL__c =joinBuyerWraper.UploadSignedChangeofDetails;
           cocdSR.OQOOD_Fee_Applicable__c = false;
            cocdSR.Type = 'Change of Joint Buyer Details';
            cocdSR.SR_Type__c = 'Change of Joint Buyer Details';
             cocdSR.id=joinBuyerWraper.salesforceId;
          cocdSR.Passport_Issue_Date__c = joinBuyerWraper.PassportIssuedDate;
          cocdSR.Passport_Issue_Place__c = joinBuyerWraper.PassportIssuedPlace;
          cocdSR.New_CR__c=joinBuyerWraper.passportNo;
          cocdSR.ownerid=g.id;
           upsert cocdSR; 
           }catch(Exception e)
        {
           system.debug('*****case Error '+e.getMessage());
          cocdSR.Payment_Terms_Error__c=e.getMessage();
          return cocdSR; 
        }
        
          Case caseList = [Select id,OwnerId, Status, Origin, Additional_Doc_File_URL__c from Case Where id = :cocdSR.id];
         if(cocdSR.Status=='Submitted')
         {
         Task objTask = new Task();
                try{
                objTask = TaskUtility.getTask((SObject)caseList, 'Verify COD Documents', 'CRE', 
                               'Complaint', system.today().addDays(1));
              //  objTask.Parent_Task_Id__c=complaintCaseObj.id;
                objTask.OwnerId = Label.DefaultCaseOwnerId;
                objTask.Priority = 'High';
                objTask.Status = 'In Progress';
               // objTask.currencyISOcode = caseList.currencyISOcode ;
                system.debug('--objTask--Promotions'+objTask);
                
                  insert objTask;
                  system.debug('>>>>>>>objTask'+objTask);
                  
                  
                  
                    if(String.isNotBlank(cocdSR.Passport_File_URL__c))
                {
                SR_Attachments__c srAttchmentObj = new SR_Attachments__c(Case__c = cocdSR.id,Account__c = cocdSR.AccountId,Type__c = extractType(cocdSR.Passport_File_URL__c),Name= 'Customer Passport '+ System.today(), Attachment_URL__c = cocdSR.Passport_File_URL__c);
                insert srAttchmentObj;
                }
                if(String.isNotBlank(cocdSR.Additional_Doc_File_URL__c))
                {
                SR_Attachments__c srAttchmentObj = new SR_Attachments__c(Case__c = cocdSR.id,Account__c = cocdSR.AccountId,Type__c = extractType(cocdSR.Additional_Doc_File_URL__c),Name= 'Additional Document '+ System.today(), Attachment_URL__c =  cocdSR.Additional_Doc_File_URL__c);
                insert srAttchmentObj;
                }
                if(String.isNotBlank(cocdSR.OD_File_URL__c))
                {
                SR_Attachments__c srAttchmentObj = new SR_Attachments__c(Case__c = cocdSR.id,Account__c = cocdSR.AccountId,Type__c = extractType(cocdSR.OD_File_URL__c),Name= 'Customer Signed CRF Form '+ System.today(), Attachment_URL__c = cocdSR.OD_File_URL__c);
                insert srAttchmentObj;
                }
                
                
                
                }  
                catch(exception e){
                  System.debug('task exception '+ e);
                } 
                }                     
       return cocdSR; 
    }
    
    public static String extractType( String strName ) {
        strName = strName.substring( strName.lastIndexOf('\\')+1 );
        return strName.substring( strName.lastIndexOf('.')+1 ) ;
    }
   global class JoinBuyerWraper 
   {
        public String RecordType;
        public String AccountID;
        public String status;
        public String origin;
        public String PassportFileUrl;
        public String AdditionalDocFileUrl;
        public String UploadSignedChangeofDetails;
        public String country;
        public String city;
        public String phone;
        public String state;
        public String postalCode;
        public String email;
        public Date PassportIssuedDate;  
        public String PassportIssuedPlace;
        public String passportNo;
        public String mobileCountryCode;
        public String address1;
        public String address2;
        public String address3;
        public String address4;
        
        public String salesforceId;
   } 
    
 
 }