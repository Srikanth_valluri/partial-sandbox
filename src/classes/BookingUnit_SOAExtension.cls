/************************************************************************************
Version     Author          Date        Description
1.1         Arjun Khatri    19/02/2019  1.Added flag for old soap, new soap and rest
                                          callout to generate SOA.
1.2                         27/02/2019  2.Added flag for old soap, new soap and rest
                                          callout to generate and email SOA.
************************************************************************************/

public with sharing class BookingUnit_SOAExtension {
    public list<SR_Attachments__c> lstCaseAttachment {get;set;}
    public String url;
    public blob urlHPBody;
    public String bookingUnitId;
    public Booking_Unit__c currentBookingUnit;
    public String errorMessage;
    public boolean bUStatus;
    public String custEmailid{get;set;}

    public BookingUnit_SOAExtension(ApexPages.standardController controller){
        bUStatus = true;
        errorMessage = 'Please try after sometime.';
       
        bookingUnitId = ApexPages.currentPage().getParameters().get('id');
        system.debug('bookingUnitId : : ' + bookingUnitId);
        currentBookingUnit =  getBookingUnit(bookingUnitId);
        
        if (currentBookingUnit.Booking__r.Account__r.IsPersonAccount == True) {
            custEmailid = currentBookingUnit.Booking__r.Account__r.Email__pc;
        } else {
            custEmailid = currentBookingUnit.Booking__r.Account__r.Email__c;
        }
        
    } 
 
    public pagereference DGMSOA(){  
        bookingUnitId = ApexPages.currentPage().getParameters().get('id');
        bookingUnitId = String.valueOf(bookingUnitId).substring(0, 15);
        System.Debug('=BookingUnit_SOAExtension*********DGMSOA====bookingUnitId : ' + bookingUnitId);
        currentBookingUnit =  getBookingUnit(bookingUnitId); 
        lstCaseAttachment = new list<SR_Attachments__c>();

        errorMessage = 'Please try after sometime.';
        List<Booking_Unit_Inactive_Status__c> bUnitStatusLst = Booking_Unit_Inactive_Status__c.getall().values();
        String status = currentBookingUnit.Registration_Status__c;
        for(Booking_Unit_Inactive_Status__c bUnitStatus : bUnitStatusLst) {
            if(status.equalsIgnoreCase(bUnitStatus.Status__c)){
                bUStatus = false;
            }
        }

        if(bUStatus) {
            GenerateDGMSOAController.soaResponse objSOPResponse = new GenerateDGMSOAController.soaResponse();
            
            try {
                    
                
                    objSOPResponse = GenerateDGMSOAController.getSOADocument(currentBookingUnit.Registration_ID__c);
                
                
               
            } catch(Exception e) {
                System.Debug('====Error : ' + e);
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,errorMessage));
                pagereference newpg = new PageReference ('/apex/BookingUnit_DGMSOA?id='+bookingUnitId);
                newpg.setRedirect(false);
                return newpg; 
            }

            System.Debug('===DGMSOA==GenerateDGMSOAController.soaResponse objSOPResponse : ' + objSOPResponse);

            if (objSOPResponse.url != 'null' && String.isNotBlank(objSOPResponse.url)  ){
                String url =  String.valueOf(objSOPResponse.url);
                if(!Test.isRunningTest())
                    urlHPBody = getBlob(url);
                System.Debug('===DGMSOA==GenerateDGMSOAController.soaResponse urlHPBody : ' + urlHPBody);
                SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
                objCaseAttachment.Name = 'FM SOA ' + currentBookingUnit.Name + ' '+ DateTime.now();
                objCaseAttachment.Attachment_URL__c = url;
                objCaseAttachment.Booking_Unit__c = currentBookingUnit.Id;
                lstCaseAttachment.add(objCaseAttachment);
                System.Debug('===DGMSOA==GenerateDGMSOAController.soaResponse urlHPBody : ' + lstCaseAttachment);
                try{
                    if (lstCaseAttachment != null && lstCaseAttachment.size()>0){
                        insert lstCaseAttachment;
                        System.debug('======Success==lstCaseAttachment : '+lstCaseAttachment[0].Id );
                    }
                } catch (Exception e) {
                    System.debug('========error : '+e );
                }

                String soaType = 'FM SOA.pdf';
                String userName = UserInfo.getUserName();
                User activeUser = [Select Email From User where Username = : userName limit 1];
                String userEmail = activeUser.Email;
                String custEmail;
                if (currentBookingUnit.Booking__r.Account__r.IsPersonAccount == True) {
                    custEmail = currentBookingUnit.Booking__r.Account__r.Email__pc;
                } else {
                    custEmail = currentBookingUnit.Booking__r.Account__r.Email__c;
                }
                SendEmail(urlHPBody, custEmail, currentBookingUnit.Booking__r.Account__r.Name, soaType, userEmail, currentBookingUnit.Unit_Name__c, currentBookingUnit.Booking__r.Account__c,url);

            }
        } else {
            errorMessage = 'Inactive Unit: Statement not available.';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,errorMessage));
        }
        pagereference newpg = new PageReference ('/'+bookingUnitId);
        newpg.setRedirect(false);
        return newpg;
    }

    public pagereference ShowDGMSOA(){  
        bookingUnitId = ApexPages.currentPage().getParameters().get('id');
        bookingUnitId = String.valueOf(bookingUnitId).substring(0, 15);
        System.Debug('=BookingUnit_SOAExtension*********DGMSOA====bookingUnitId : ' + bookingUnitId);
        currentBookingUnit =  getBookingUnit(bookingUnitId);  
        lstCaseAttachment = new list<SR_Attachments__c>();

        List<Booking_Unit_Inactive_Status__c> bUnitStatusLst = Booking_Unit_Inactive_Status__c.getall().values();
        String status = currentBookingUnit.Registration_Status__c;
        for(Booking_Unit_Inactive_Status__c bUnitStatus : bUnitStatusLst) {
            if(status.equalsIgnoreCase(bUnitStatus.Status__c)){
                bUStatus = false;
            }
        }

        if(bUStatus) {

            String soaUrl;
                    
            try {
                soaUrl = Test.isRunningTest()?'TestURl':FmIpmsRestCoffeeServices.getFMSoa( currentBookingUnit.Registration_ID__c );
            } catch(Exception e) {
                System.Debug('====Error : ' + e);
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,errorMessage));
                //pagereference newpg = new PageReference ('/apex/BookingUnit_DGMSOA?id='+bookingUnitId);
                //newpg.setRedirect(false);
                //return newpg; 
                pagereference newpg = new PageReference ('/apex/ShowSOAError?id='+bookingUnitId);
                newpg.setRedirect(true);
                return newpg;
            }

            //System.Debug('===ShowDGMSOA==GenerateDGMSOAController.soaResponse objSOPResponse : ' + objSOPResponse);

            if ( String.isNotBlank(soaUrl)) {
                System.debug('===ShowDGMSOA===Not balnk  : '+soaUrl );
                url = soaUrl;
                if(!Test.isRunningTest())
                    urlHPBody = getBlob(url);
                SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
                objCaseAttachment.Name = 'FM SOA ' + currentBookingUnit.Name + ' '+ DateTime.now();
                objCaseAttachment.Attachment_URL__c = url;
                objCaseAttachment.Booking_Unit__c = currentBookingUnit.Id;
                lstCaseAttachment.add(objCaseAttachment);
                
                try{
                    if (lstCaseAttachment != null && lstCaseAttachment.size()>0){
                        insert lstCaseAttachment;
                        System.debug('======Success==lstCaseAttachment : '+lstCaseAttachment[0].Id );
                    }
                } catch (Exception e) {
                    System.debug('========error : '+e );
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,errorMessage));
                    pagereference newpg = new PageReference ('/apex/ShowSOAError?id='+bookingUnitId);
                    newpg.setRedirect(true);
                    return newpg;
                }
            } else {
                System.debug('========objSOPResponse.url is null : ');
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,errorMessage));
                pagereference newpg = new PageReference ('/apex/ShowSOAError?id='+bookingUnitId);
                newpg.setRedirect(true);
                return newpg;
            }
            if (lstCaseAttachment == null && lstCaseAttachment.size() == 0){
                System.debug('===ShowDGMSOA===lstCaseAttachment null size 0 : ' );
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,errorMessage));
                pagereference newpg = new PageReference ('/apex/ShowSOAError?id='+bookingUnitId);
                newpg.setRedirect(true);
                return newpg;
            } else {
                System.debug('===ShowDGMSOA===lstCaseAttachment not size >0  : ' );
                pagereference newpg = new PageReference (lstCaseAttachment[0].Attachment_URL__c);
                newpg.setRedirect(true);
                return newpg;
            }
        } else {
            errorMessage = 'Inactive Unit: Statement not available.';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,errorMessage));
        }
        pagereference newpg = new PageReference ('/apex/ShowSOAError?id='+bookingUnitId);
        newpg.setRedirect(true);
        return newpg;
    }

    public pagereference PenaltySOA(){
        bookingUnitId = ApexPages.currentPage().getParameters().get('id');
        bookingUnitId = String.valueOf(bookingUnitId).substring(0, 15);
        System.Debug('==BookingUnit_SOAExtension*********PenaltySOA===bookingUnitId : ' + bookingUnitId);
        currentBookingUnit =  getBookingUnit(bookingUnitId);  
        lstCaseAttachment = new list<SR_Attachments__c>();
        
        List<Booking_Unit_Inactive_Status__c> bUnitStatusLst = Booking_Unit_Inactive_Status__c.getall().values();
        String status = currentBookingUnit.Registration_Status__c;
        for(Booking_Unit_Inactive_Status__c bUnitStatus : bUnitStatusLst) {
            if(status.equalsIgnoreCase(bUnitStatus.Status__c)){
                bUStatus = false;
            }
        }

        if(bUStatus) {

            PenaltyWaiverService.SopResponseObject objSOPResponse = new PenaltyWaiverService.SopResponseObject();
            try {
                objSOPResponse = PenaltyWaiverService.getSOPDocument(currentBookingUnit.Registration_ID__c);
            } catch(Exception e) {
                System.Debug('====Error : ' + e);
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,errorMessage));
                pagereference newpg = new PageReference ('/apex/BookingUnitPenaltySOA?id='+bookingUnitId);
                newpg.setRedirect(false);
                return newpg; 
            }

            System.Debug('=====PenaltyWaiverService.SopResponseObject objSOPResponse : ' + objSOPResponse);
            

            if (objSOPResponse.url != 'null' && String.isNotBlank(objSOPResponse.url)) {
                url = objSOPResponse.url;
                if(!Test.isRunningTest())
                    urlHPBody = getBlob(url);
                SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
                objCaseAttachment.Name = 'Penalty SOA ' + currentBookingUnit.Name + ' '+ DateTime.now();
                objCaseAttachment.Attachment_URL__c = url;
                objCaseAttachment.Booking_Unit__c = currentBookingUnit.Id;
                lstCaseAttachment.add(objCaseAttachment);   
                
                try{
                    if (lstCaseAttachment != null && lstCaseAttachment.size()>0){
                        insert lstCaseAttachment;
                        System.debug('======Success==lstCaseAttachment : '+lstCaseAttachment[0].Id );
                    }
                } catch (Exception e) {
                    System.debug('========error : '+e );
                }

                String soaType = 'Penalty SOA.pdf';
                String userName = UserInfo.getUserName();
                User activeUser = [Select Email From User where Username = : userName limit 1];
                String userEmail = activeUser.Email;
                String custEmail;
                if (currentBookingUnit.Booking__r.Account__r.IsPersonAccount == True) {
                    custEmail = currentBookingUnit.Booking__r.Account__r.Email__pc;
                } else {
                    custEmail = currentBookingUnit.Booking__r.Account__r.Email__c;
                }
                SendEmail(urlHPBody, custEmail, currentBookingUnit.Booking__r.Account__r.Name, soaType, userEmail, currentBookingUnit.Unit_Name__c, currentBookingUnit.Booking__r.Account__c,url);

            }
        } else {
            errorMessage = 'Inactive Unit: Statement not available.';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,errorMessage));
        }
        pagereference newpg = new PageReference ('/'+bookingUnitId);
        newpg.setRedirect(false);
        return newpg;
    }

    public pagereference ShowPenaltySOA(){
        bookingUnitId = ApexPages.currentPage().getParameters().get('id');
        bookingUnitId = String.valueOf(bookingUnitId).substring(0, 15);
        System.Debug('==BookingUnit_SOAExtension*********PenaltySOA===bookingUnitId : ' + bookingUnitId);
        currentBookingUnit =  getBookingUnit(bookingUnitId);  
        lstCaseAttachment = new list<SR_Attachments__c>();
        
        List<Booking_Unit_Inactive_Status__c> bUnitStatusLst = Booking_Unit_Inactive_Status__c.getall().values();
        String status = currentBookingUnit.Registration_Status__c;
        for(Booking_Unit_Inactive_Status__c bUnitStatus : bUnitStatusLst) {
            if(status.equalsIgnoreCase(bUnitStatus.Status__c)){
                bUStatus = false;
            }
        }

        if(bUStatus) {
            PenaltyWaiverService.SopResponseObject objSOPResponse = new PenaltyWaiverService.SopResponseObject();
            try {
                objSOPResponse = PenaltyWaiverService.getSOPDocument(currentBookingUnit.Registration_ID__c);
            } catch(Exception e) {
                System.Debug('====Error : ' + e);
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,errorMessage));
                /*pagereference newpg = new PageReference ('/apex/BookingUnitPenaltySOA?id='+bookingUnitId);
                newpg.setRedirect(false);
                return newpg; */
                pagereference newpg = new PageReference ('/apex/ShowSOAError?id='+bookingUnitId);
                newpg.setRedirect(true);
                return newpg;
            }

            System.Debug('=====PenaltyWaiverService.SopResponseObject objSOPResponse : ' + objSOPResponse);
            

            if (objSOPResponse.url != 'null' && String.isNotBlank(objSOPResponse.url)) {
                url = objSOPResponse.url;
                if(!Test.isRunningTest())
                    urlHPBody = getBlob(url);
                SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
                objCaseAttachment.Name = 'Penalty SOA ' + currentBookingUnit.Name + ' '+ DateTime.now();
                objCaseAttachment.Attachment_URL__c = url;
                objCaseAttachment.Booking_Unit__c = currentBookingUnit.Id;
                lstCaseAttachment.add(objCaseAttachment);   
                
                try{
                    if (lstCaseAttachment != null && lstCaseAttachment.size()>0){
                        insert lstCaseAttachment;
                        System.debug('======Success==lstCaseAttachment : '+lstCaseAttachment[0].Id );
                    }
                } catch (Exception e) {
                    System.debug('========error : '+e );
                }

                //String soaType = 'Penalty SOA.pdf';
                //SendEmail(urlHPBody, currentBookingUnit.Booking__r.Account__r.Email__c, currentBookingUnit.Booking__r.Account__r.Name, soaType);

            } else {
                System.debug('===in else url is null : ');
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,errorMessage));
                pagereference newpg = new PageReference ('/apex/ShowSOAError?id='+bookingUnitId);
                newpg.setRedirect(true);
                return newpg;
            }

            if (lstCaseAttachment == null && lstCaseAttachment.size() == 0){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,errorMessage));
                pagereference newpg = new PageReference ('/apex/ShowSOAError?id='+bookingUnitId);
                newpg.setRedirect(true);
                return newpg;
            } else {
                pagereference newpg = new PageReference (lstCaseAttachment[0].Attachment_URL__c);
                newpg.setRedirect(true);
                return newpg;
            }
        } else {
            errorMessage = 'Inactive Unit: Statement not available.';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,errorMessage));
        }
        pagereference newpg = new PageReference ('/apex/ShowSOAError?id='+bookingUnitId);
        newpg.setRedirect(true);
        return newpg;
        /*pagereference newpg = new PageReference ('/apex/ShowPenaltySOA?id='+bookingUnitId);
        newpg.setRedirect(true);
        return newpg;*/

    }

    public pagereference UnitSOA(){
        bookingUnitId = ApexPages.currentPage().getParameters().get('id');
        bookingUnitId = String.valueOf(bookingUnitId).substring(0, 15);
        System.Debug('=BookingUnit_SOAExtension********UnitSOA====bookingUnitId : ' + bookingUnitId);
        currentBookingUnit =  getBookingUnit(bookingUnitId);                                                 
        
        if(Label.SOA_Callout_Indicator_For_BU != NULL 
        && currentBookingUnit != NULL  
        && Label.SOA_Callout_Indicator_For_BU.equalsIgnoreCase('Y')) {
            pagereference newpg = unitSOA_OldSoap();
            
            //newpg.setRedirect(true);
            return newpg;
        }
        else if(Label.SOA_Callout_Indicator_For_BU.equalsIgnoreCase('N')) {
            //generateSOAByNewSoap();
            return null;
        }
        else if(Label.SOA_Callout_Indicator_For_BU != NULL 
        && currentBookingUnit != NULL  
        && Label.SOA_Callout_Indicator_For_BU.equalsIgnoreCase('R')) {
            pagereference newpg = generateSOAByRest();
            if( newpg != null) { 
                newpg.setRedirect(true);
                
                return newpg;
            }else {
                return null;
            }           
            
        }
        return null;
    }
    
    public pagereference unitSOA_OldSoap() {
        lstCaseAttachment = new list<SR_Attachments__c>();
        
        List<Booking_Unit_Inactive_Status__c> bUnitStatusLst = Booking_Unit_Inactive_Status__c.getall().values();
        String status = currentBookingUnit.Registration_Status__c;
        for(Booking_Unit_Inactive_Status__c bUnitStatus : bUnitStatusLst) {
            if(status.equalsIgnoreCase(bUnitStatus.Status__c)){
                bUStatus = false;
            }
        }

        if(bUStatus) {

            GenerateSOAController.soaResponse objSOAResponse = new GenerateSOAController.soaResponse();
            try {
                objSOAResponse = GenerateSOAController.getSOADocument(currentBookingUnit.Registration_ID__c);
            } catch(Exception e) {
                System.Debug('====Error : ' + e);
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,errorMessage));
                pagereference newpg = new PageReference ('/apex/BookingUnit_GenerateSOA?id='+bookingUnitId);
                newpg.setRedirect(false);
                return newpg; 
            }
            System.Debug('=====GenerateSOAController.soaResponse objSOAResponse : ' + objSOAResponse);

            if (objSOAResponse.url != 'null' && String.isNotBlank(objSOAResponse.url)) {
                url = objSOAResponse.url;
                
                //TODO  added to create record of soa genrator
                GenericUtility.createSOACreator(url,UserInfo.getUserId(),system.now(),currentBookingUnit.Registration_ID__c,'Generate and Email Unit SOA',NULL,currentBookingUnit.Id,NUll);


                if(!Test.isRunningTest())
                    urlHPBody = getBlob(url);
                SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
                objCaseAttachment.Name = 'Unit SOA ' + currentBookingUnit.Name + ' '+ DateTime.now();
                objCaseAttachment.Attachment_URL__c = url;
                objCaseAttachment.Booking_Unit__c = currentBookingUnit.Id;
                lstCaseAttachment.add(objCaseAttachment);
            
                try{
                    if (lstCaseAttachment != null && lstCaseAttachment.size()>0){
                        insert lstCaseAttachment;
                        System.debug('======Success==lstCaseAttachment : '+lstCaseAttachment[0].Id );
                    }
                } catch (Exception e) {
                    System.debug('========error : '+e );
                }
                String soaType = 'Unit SOA.pdf';
                String userName = UserInfo.getUserName();
                User activeUser = [Select Email From User where Username = : userName limit 1];
                String userEmail = activeUser.Email;
                String custEmail;
                if (currentBookingUnit.Booking__r.Account__r.IsPersonAccount == True) {
                    custEmail = currentBookingUnit.Booking__r.Account__r.Email__pc;
                } else {
                    custEmail = currentBookingUnit.Booking__r.Account__r.Email__c;
                }
                SendEmail(urlHPBody, custEmail, currentBookingUnit.Booking__r.Account__r.Name, soaType, userEmail, currentBookingUnit.Unit_Name__c, currentBookingUnit.Booking__r.Account__c,url);
            }
        } else {
            errorMessage = 'Inactive Unit: Statement not available.';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,errorMessage));
        }

        pagereference newpg = new PageReference ('/'+bookingUnitId);
        newpg.setRedirect(false);
        return newpg;
    }

    public pagereference ShowUnitSOA(){
        bookingUnitId = ApexPages.currentPage().getParameters().get('id');
        bookingUnitId = String.valueOf(bookingUnitId).substring(0, 15);
        System.Debug('=BookingUnit_SOAExtension********UnitSOA====bookingUnitId : ' + bookingUnitId);
        currentBookingUnit =  getBookingUnit(bookingUnitId);  
        
        if(Label.SOA_Callout_Indicator_For_BU != NULL 
        && currentBookingUnit != NULL  
        && Label.SOA_Callout_Indicator_For_BU.equalsIgnoreCase('Y')) {
            pagereference newpg = generateSOAByOldSoap();
            //newpg.setRedirect(true);
            return newpg;
        }
        else if(Label.SOA_Callout_Indicator_For_BU.equalsIgnoreCase('N')) {
            //generateSOAByNewSoap();
            return null;
        }
        else if(Label.SOA_Callout_Indicator_For_BU != NULL 
        && currentBookingUnit != NULL  
        && Label.SOA_Callout_Indicator_For_BU.equalsIgnoreCase('R')) {
            pagereference newpg = generateSOAByRest();

            if( newpg != null) { 
                newpg.setRedirect(true);
                return newpg;
            }else {
                return null;
            }
        }
        return null;
    }
    
    public pagereference generateSOAByRest() {
        System.debug('generateSOAByRest==');
        
        if(currentBookingUnit != null && currentBookingUnit.Registration_ID__c != Null) {
            System.debug('+++++++++Registration_ID__c != Null ++++++++++');
            String accId = currentBookingUnit.Account_Id__c != NULL ? String.valueOf(currentBookingUnit.Account_Id__c) : '';
            String strBuId = string.valueOf(currentBookingUnit.Id);
            String responseobj = GenerateDPSoaRest.generateDPSoa(string.valueOf(currentBookingUnit.Registration_ID__c)
                                            , accId
                                            , strBuId
                                            , '');
            System.debug('generateSOAByRest responseobj ==' + responseobj);
            //TODO  added to create record of soa genrator
                GenericUtility.createSOACreator(responseobj,UserInfo.getUserId(),system.now(),currentBookingUnit.Registration_ID__c,'Generate and Email unit SOA',NULL,currentBookingUnit.Id,NUll);

            if( responseobj.contains('Error :') ) {
                System.debug('responseobj Error : '+responseobj );
                ApexPages.addmessage(
                    new ApexPages.message(ApexPages.severity.ERROR,responseobj)
                );
                return null;
                //return ApexPages.CurrentPage();
            }
            else{
                Pagereference pg = new PageReference(responseobj);
                //pg.setRedirect(true);
                return pg;
            }
        }
        
        return null;
    }
    
    public pagereference generateSOAByNewSoap() { 
        System.debug('generateSOAByNewSoap==');
        return null;
    }
    
    public pagereference generateSOAByOldSoap() {
        System.debug('generateSOAByOldSoap==');
        lstCaseAttachment = new list<SR_Attachments__c>();
        
        List<Booking_Unit_Inactive_Status__c> bUnitStatusLst = Booking_Unit_Inactive_Status__c.getall().values();
        String status = currentBookingUnit.Registration_Status__c;
        for(Booking_Unit_Inactive_Status__c bUnitStatus : bUnitStatusLst) {
            if(status.equalsIgnoreCase(bUnitStatus.Status__c)){
                bUStatus = false;
            }
        }

        if(bUStatus) {

            GenerateSOAController.soaResponse objSOAResponse = new GenerateSOAController.soaResponse();
            try {
                objSOAResponse = GenerateSOAController.getSOADocument(currentBookingUnit.Registration_ID__c);
            } catch(Exception e) {
                System.Debug('====Error : ' + e);
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,errorMessage));
                /*pagereference newpg = new PageReference ('/apex/BookingUnit_GenerateSOA?id='+bookingUnitId);
                newpg.setRedirect(false);
                return newpg;*/ 
                pagereference newpg = new PageReference ('/apex/ShowSOAError?id='+bookingUnitId);
                newpg.setRedirect(true);
                return newpg;
            }
            System.Debug('=====GenerateSOAController.soaResponse objSOAResponse : ' + objSOAResponse);

            /*if (objSOAResponse.url != null  ) {*/
            if (objSOAResponse.url != 'null' && String.isNotBlank(objSOAResponse.url)) {
                url = objSOAResponse.url;
                if(!Test.isRunningTest())
                    urlHPBody = getBlob(url);
                SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
                objCaseAttachment.Name = 'Unit SOA ' + currentBookingUnit.Name + ' '+ DateTime.now();
                objCaseAttachment.Attachment_URL__c = url;
                objCaseAttachment.Booking_Unit__c = currentBookingUnit.Id;
                lstCaseAttachment.add(objCaseAttachment);
            
                try{
                    if (lstCaseAttachment != null && lstCaseAttachment.size()>0){
                        insert lstCaseAttachment;
                        System.debug('======Success==lstCaseAttachment : '+lstCaseAttachment[0].Id );
                    }
                } catch (Exception e) {
                    System.debug('========error : '+e ); 
                }
                //String soaType = 'Unit SOA.pdf';
                //SendEmail(urlHPBody, currentBookingUnit.Booking__r.Account__r.Email__c, currentBookingUnit.Booking__r.Account__r.Name, soaType);
            } else {
                System.debug('===in else url is null : ');
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,errorMessage));
                pagereference newpg = new PageReference ('/apex/ShowSOAError?id='+bookingUnitId);
                newpg.setRedirect(true);
                return newpg;
            }
            if (lstCaseAttachment == null && lstCaseAttachment.size() == 0){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,errorMessage));
                pagereference newpg = new PageReference ('/apex/ShowSOAError?id='+bookingUnitId);
                newpg.setRedirect(true);
                return newpg;
            } else {
                pagereference newpg = new PageReference (lstCaseAttachment[0].Attachment_URL__c);
                newpg.setRedirect(true);
                return newpg;
            }
        } else {
            errorMessage = 'Inactive Unit: Statement not available.';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,errorMessage));
        }

        pagereference newpg = new PageReference ('/apex/ShowSOAError?id='+bookingUnitId);
        newpg.setRedirect(true);
        return newpg;
        /*pagereference newpg = new PageReference ('/apex/ShowUnitSOA?id='+bookingUnitId);
        newpg.setRedirect(true);
        return newpg;*/
    }

    public static Blob getBlob(String url){
        PageReference pageRef = new PageReference(url);
        Blob ret = pageRef.getContentAsPDF();
        return ret;
    }

    public Booking_Unit__c getBookingUnit(String bookingUnitId){
        Booking_Unit__c currentBookingUnit = [ SELECT Id,
                                                      Name,
                                                      Account_Id__c,
                                                      Registration_ID__c,
                                                      Property_Name__c,
                                                      Unit_Name__c,
                                                      Requested_Price__c,
                                                      Selling_Price__c,
                                                      Registration_Status__c,
                                                      Booking__c,
                                                      Booking__r.Id,
                                                      Booking__r.Name,
                                                      Booking__r.Account__c,
                                                      Booking__r.Account__r.Email__c,
                                                      Booking__r.Account__r.Name,
                                                      Booking__r.Account__r.Email__pc,
                                                      Booking__r.Account__r.IsPersonAccount
                                                 FROM Booking_Unit__c
                                                WHERE Id = :bookingUnitId
                                                  ];
        return currentBookingUnit;
    }

    public pagereference CancelGeneration(){
        bookingUnitId = ApexPages.currentPage().getParameters().get('id');
        bookingUnitId = String.valueOf(bookingUnitId).substring(0, 15);
        System.Debug('=BookingUnit_SOAExtension********UnitSOA====bookingUnitId : ' + bookingUnitId);

        pagereference newpg = new PageReference ('/'+bookingUnitId);
        newpg.setRedirect(false);
        return newpg;
    }   
    
    public void SendEmail(blob file, String email, String accName, String soaType, String creMail, String buId, Id accId , string strUrl) {
        //New instance of a single email message
        system.debug('testing for Email');
        Messaging.SingleEmailMessage mail = 
        new Messaging.SingleEmailMessage();
        List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        String MyProflieName = PROFILE[0].Name;
        Boolean FM =MyProflieName.contains('FM');
       
        list<Messaging.SingleEmailMessage> mails = new  list<Messaging.SingleEmailMessage>(); 
        // The email template ID used for the email
        //mail.setTemplateId('00X30000001GLJj');
        //OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'no-replysf@damacgroup.com'];

        List<String> sendTo = new List<String>();
        if(email !='' || email != null) {
          sendTo.add(email);
        }
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'no-replysf@damacgroup.com'];
        if ( owea.size() > 0 ) {
            mail.setOrgWideEmailAddressId(owea.get(0).Id);
        }
        list<String> bccAddress = new list<String>();
        //bccAddress.add(creMail);
        /*
        * Updated bcc address on 10/02/2019
        */
        bccAddress.add(Label.SF_Copy_EMail_Address);
        //mail.setSubject('Statement of Account Generated.');
        mail.setToAddresses(sendTo);
        mail.setBccAddresses(bccAddress);
        mail.setUseSignature(false);
        //mail.setReplyTo('test123@acme.com');
        //mail.setSenderDisplayName('SalesForce.com');
        //mail.setWhatId(accId);
        //mail.setTargetObjectId(accId); 
        //mail.setSaveAsActivity(true);  
        
        string name = nameFormat(accName);
        
        String body = 'Dear ' + name + ',<br/>';
        /*body += '\n Please find attached Statement of Account. '; 
        body += '\n \n Thanks & Regards,';
        body += '\n SalesForce Team';*/
        if(soaType.equalsIgnoreCase('Unit SOA.pdf')) {
            mail.setSubject('Statement of Account for unit '+ buId);
            body += '<br/> As requested, please find attached the Statement of Account for unit '+ buId +'.<br/>';
        } else if(soaType.equalsIgnoreCase('Penalty SOA.pdf')) {
            mail.setSubject('Penalty Statement of Account for unit '+ buId);
            body += '<br/> As requested, please find attached the Penalty Statement of Account for unit '+ buId +'.<br/>';
        } else if(soaType.equalsIgnoreCase('FM SOA.pdf')) {
            mail.setSubject('Facilities management Statement of Account for unit '+ buId );
            body += '<br/> As requested, please find attached the Facilities management Statement of Account for unit '+ buId +'.<br/>';
        }
        if(FM==true){
        body += '<br/>Regards,';
        body += '<br/> LOAMS';
        body += '<br/> LUXURY OWNER ASSOCIATION MANAGEMENT SERVICES CO LLC';
        body += '<br/> P.O. Box: 2195, Dubai, United Arab Emirates';
        body += '<br/> Telephone: +971 4 70 49000';
        body += '<br/> E-mail: servicecharges@loams.ae';
        body += '<br/> ' +System.Label.DAMAC_LIVING;
        }
        else{
        body += '<br/>Regards,';
        body += '<br/>' + UserInfo.getName();
        body += '<br/> Officer-Client Relations';
        body += '<br/>DAMAC PROPERTIES Co. LLC.';
        body += '<br/> P.O. Box: 2195, Dubai, United Arab Emirates';
        body += '<br/> Telephone: +971 4 237 5000';
        body += '<br/> Fax: +971 4 373 1373';
        body += '<br/> E-mail: atyourservice@damacproperties.com';
        body += '<br/> http://www.damacproperties.com/';
         }

        mail.setHtmlBody(body);

        List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
        Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
        efa.setFileName(soaType);
        efa.setBody(file);
        fileAttachments.add(efa);
        mail.setFileAttachments(fileAttachments);
        mails.add(mail);
        try {
            Messaging.sendEmail(mails);
            System.debug('===========Mail sent====');
            
            EmailMessage emailMsg = new EmailMessage();

            emailMsg.ToAddress=(mails[0].getToAddresses())[0];
            emailMsg.FromAddress = UserInfo.getUserEmail();
            emailMsg.FromName = 'SOAs SR - Document';
            emailMsg.Subject=mails[0].getSubject();
            emailMsg.HtmlBody=mails[0].getHtmlBody();
            emailMsg.RelatedToId = accId; //Attach with the case
            emailMsg.MessageDate = system.now();
            emailMsg.Status = '3';
            emailMsg.Attachment_URL__c = strUrl;
            emailMsg.BccAddress = (mails[0].getBccAddresses())[0];
            insert emailMsg;
            
        } catch(Exception e) {
            System.debug('======Error in email sending=========' + e);
        }
    }
    
    public static String nameFormat ( String strName ) {
        system.debug( ' strName : '+ strName );
        strName = strName.toLowerCase();
        List<String> names = strName.split(' ');
        for (Integer i = 0; i < names.size(); i++) {
            names[i] = names[i].capitalize();
        }
        strName = String.join(names, ' ');
        system.debug(strName);
        return strName;
    }   
    
}