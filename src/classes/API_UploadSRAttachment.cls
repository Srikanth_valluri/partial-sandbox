@RestResource(urlMapping='/api_uploadSRAttachment/*')
global with sharing class API_UploadSRAttachment {
    
    @HttpPost
    global static void doPost(AttachmentRequestDTO attachmentReq) {
        /* test:  
/services/apexrest/api_uploadtokendeposit

{
    "attachmentReq": {
        "SRId":"a0M1w000000rET2",
        "Name":"newAtt",
        }
}

*/
        
        RestRequest request = RestContext.request;
        String requestType = request.requestURI.substring(request.requestURI.lastIndexOf('/' ) + 1);
        
        Integer statusCode = 200;
        
        NSIBPM__Service_Request__c srRecord = new NSIBPM__Service_Request__c();
        srRecord.Id = attachmentReq.SRId;
        
        if (requestType.equalsIgnoreCase('ReservationForm')) {
            srRecord.Reservation_Form_Attachment_Name__c = attachmentReq.Name;
        } else if (requestType.equalsIgnoreCase('TokenDeposit')) {
            srRecord.Token_Attachment_Name__c = attachmentReq.Name;
        }
        
        
        AttachmentResponseDTO responseDTO = new AttachmentResponseDTO();
        
        try {
            
            statusCode = 200;
            update srRecord;
            responseDTO.success = true;
        } catch (Exception e) {
            statusCode = 500;
            responseDTO.success = false;
            responseDTO.errors.add(e.getMessage());
        }
        
        RestResponse response = RestContext.response;
        response.statusCode = statusCode;
        response.addHeader('Content-Type', 'application/json');
        response.responseBody = Blob.valueOf(JSON.serialize(responseDTO));
    }
    
    global class AttachmentRequestDTO{
        public String SRId {get; set;}
        public String Name {get; set;}
        
    }
    
    global class AttachmentResponseDTO {
        public boolean success {get; set;}
        public List<String> errors {get; set;}
        
        global AttachmentResponseDTO() {
            success = false;
            errors = new List<String>();
        }
    }
}