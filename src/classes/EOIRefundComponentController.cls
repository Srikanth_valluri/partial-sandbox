/*
 * Description - Controller class for EOIRefundComponent visualforce component
 *
 * Version        Date            Author            Description
 * 1.0            04/03/18        Vivek Shinde      Initial Draft
 */
public without sharing class EOIRefundComponentController {
    
    public Case objCase { get; set; }
    public Case_Extension__c objServiceRequest {get; set;}
    public transient String eoiAttachmentName { get; set; }
    public transient String eoiAttachmentBody { get; set; }
    public transient String idAttachmentName { get; set; }
    public transient String idAttachmentBody { get; set; }
    public transient String wtfAttachmentName { get; set; }
    public transient String wtfAttachmentBody { get; set; }
    public transient String otherAttachmentName { get; set; }
    public transient String otherAttachmentBody { get; set; }
    public transient String strEOIDate { get; set; }
    
    private Account objAccount;
    private String strSRType;
    private String accountId;
    private String caseId;
    private List<SR_attachments__c> lstSRattachmentAll;
    
    private static final Id idEOIRT = Schema.SObjectType.Case.getRecordTypeInfosByName().get('EOI Refund').getRecordTypeId();
    private static final Id idEOIRTSR = Schema.SObjectType.Case_Extension__c.getRecordTypeInfosByName().get('EOI Refund').getRecordTypeId();
    
    public EOIRefundComponentController() {
        strSRType = ApexPages.currentPage().getParameters().get('SRType');
        accountId = ApexPages.currentPage().getParameters().get('AccountId');
        caseId = ApexPages.currentPage().getParameters().get('caseId');
        if(strSRType == 'EOIRefund') {
            init();
        }
    }
    
    // Method for initialization purpose
    private void init() {
        objCase = new Case();
        objAccount = new Account();
        objServiceRequest = new Case_Extension__c();
    }
    
    // Method to save case details on click of submit button
    public void saveCase() {
        try {
            system.debug('--objCase--'+objCase);
            system.debug('--idEOIRT--'+idEOIRT);
            objCase.RecordTypeId = idEOIRT;
            objCase.Origin = 'Walk-In';
            objCase.SR_Type__c = 'EOI Refund';
            objCase.AccountId = accountId;
            objCase.EOI_Date__c = String.isNotBlank(strEOIDate) ? Date.parse(strEOIDate) : null;
            upsert objCase;
            objServiceRequest.RecordTypeId = idEOIRTSR;
            objServiceRequest.Case__c = objCase.Id;
            upsert objServiceRequest;
            objCase = [Select Id, CaseNumber, Status, Customer_First_Name__c, Customer_Last_Name__c,Description,
                       Related_Unit_Name__c, Refund_Amount__c, Property_Consultant_Email__c,
                       EOI_Date__c, EOI_Reference_Number__c, Customer_Middle_Name__c, Receipt_Id__c
                       From Case Where Id =: objCase.Id];
           objServiceRequest = [Select Id,
                                       Beneficiary_Account_Name__c,
                                       Beneficiary_Account_Number__c,
                                       Beneficiary_Add__c,
                                       Beneficiary_Bank_Branch__c,
                                       Beneficiary_Bank_Name__c,
                                       Beneficiary_Branch_Country__c,
                                       Beneficiary_IBAN__c,
                                       Beneficiary_Name__c,
                                       IFSC__c,
                                       Payment_Mode__c,
                                       Property_Name_Short_Code__c,
                                       Receipt_Number__c,
                                       Sort_Code__c,
                                       SR_Number__c,
                                       Supp_Corr_Bank_Branch_add1__c,
                                       Supp_Corr_Bank_Name__c,
                                       Supp_Corr_Bank_Swift_Code__c,
                                       Swift_Code__c,
                                       CurrencyIsoCode
                                
                               From Case_Extension__c Where Id=:objServiceRequest.Id];
                               System.debug('objServiceRequest>>>>'+objServiceRequest);
        } catch(Exception excGen) {
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, excGen.getMessage()));
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, excGen.getStackTraceString()));
            if(excGen.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, excGen.getMessage().subStringAfter('FIELD_CUSTOM_VALIDATION_EXCEPTION,')));                
            }
            else {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, excGen.getMessage()));
            }
        }
    }
    
    // Method to save case details
    public Pagereference submitCase() {
        try {
            UploadMultipleDocController.data MultipleDocData = new UploadMultipleDocController.data();            
            List<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocRequest = formDocumentUploadRequest();
            
            if(lstMultipleDocRequest.Size()>0) {
                MultipleDocData = UploadMultipleDocController.getMultipleDocUrl(lstMultipleDocRequest);
                System.debug('--MultipleDocData--'+MultipleDocData);
                if(MultipleDocData != null && MultipleDocData.status == 'Exception') {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,MultipleDocData.message));                                         
                    errorLogger(MultipleDocData.message, objCase.Id, '');
                    return null;
                }                                   
                if(MultipleDocData != null && (MultipleDocData.Data == null || MultipleDocData.Data.Size() == 0)) {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,MultipleDocData.message)); 
                    errorLogger(MultipleDocData.message, objCase.Id, '');                                        
                    return null; 
                }
            }
            
            lstSRattachmentAll = new List<SR_Attachments__c>();
            if( String.isNotBlank( eoiAttachmentBody ) && String.isNotBlank( eoiAttachmentName ) ) {
                formAttachment( extractName( eoiAttachmentName ), objCase.Id , 'EOI Receipt'  );
            }
            eoiAttachmentBody = '';
            eoiAttachmentName = '';
            
            if( String.isNotBlank( idAttachmentBody ) && String.isNotBlank( idAttachmentName ) ) {
                formAttachment( extractName( idAttachmentName ), objCase.Id , 'ID'  );
            }
            idAttachmentBody = '';
            idAttachmentName = '';
            
            if( String.isNotBlank( wtfAttachmentBody ) && String.isNotBlank( wtfAttachmentName ) ) {
                formAttachment( extractName( wtfAttachmentName ), objCase.Id , 'Wire Transfer Form'  );
            }
            wtfAttachmentBody = '';
            wtfAttachmentName = '';
            
            if( String.isNotBlank( otherAttachmentBody ) && String.isNotBlank( otherAttachmentName ) ) {
                formAttachment( extractName( otherAttachmentName ), objCase.Id , 'Other'  );
            }
            otherAttachmentBody = '';
            otherAttachmentName = '';
            
            list<SR_attachments__c> lstSRattachmentValid = new list<SR_attachments__c>();
            if(lstSRattachmentAll.Size()>0 ){ //&& lstAttachment.Size()>0
                if(lstMultipleDocRequest.Size()>0 && MultipleDocData != null){
                    for(integer objInt=0; objInt<lstSRattachmentAll.Size(); objInt++ ) {
                        UploadMultipleDocController.MultipleDocResponse objData = MultipleDocData.Data[objInt];
                        if(objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url)) {
                           lstSRattachmentAll[objInt].Attachment_URL__c = objData.url;
                           lstSRattachmentValid.add(lstSRattachmentAll[objInt]);
                        }
                        else {
                           errorLogger('An exception has occurred while processing the request, document-'+objData.PARAM_ID+', PROC_STATUS-'+objData.PROC_STATUS+' URL-'+objData.url,objCase.Id,'');
                           ApexPages.addmessage(new ApexPages.message(
                           ApexPages.severity.Error,'An exception has occurred while processing the request, document-'+objData.PARAM_ID+', PROC_STATUS-'+objData.PROC_STATUS+' URL-'+objData.url));     
                        }
                    }                                   
                    if(lstSRattachmentValid.Size() > 0) {
                        insert lstSRattachmentValid;
                    }
                }
                else if(lstMultipleDocRequest.Size() > 0 && MultipleDocData == null){
                   ApexPages.addmessage(new ApexPages.message(
                   ApexPages.severity.Error,'An exception has occurred while processing the request of document upload, got null value, please contact the support team.'));                                        
                   return null;       
                }
            }
            
            objCase.Status = 'Submitted';
            objCase.Submit_For_Approval__c = true;
            objCase.AccountId = accountId;
            //objCase.Approving_Authorities__c = 'Manager,Director,HOD';
            string strAuth = SfRuleEngine.filterSfRuleEngine('Refund of EOI', objCase.Id);
            
            if( String.isNotBlank( strAuth )) {
                objCase.Approving_Authorities__c = strAuth;//'Manager,Director,HOD';
            }
            
            upsert objCase;
            //if(objServiceRequest.Payment_Mode__c == 'Transfer') {
                upsert objServiceRequest;
            //}
            
            
            return new Pagereference('/' + objCase.Id);
        } catch(Exception excGen) {
            if(excGen.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, excGen.getMessage().subStringAfter('FIELD_CUSTOM_VALIDATION_EXCEPTION,')));                
            }
            else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, excGen.getMessage()));
            }
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, excGen.getStackTraceString()));
        }
        return null;
    }
    
    // Method to form request for multiple document upload
    private List<UploadMultipleDocController.MultipleDocRequest> formDocumentUploadRequest() {
        List<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocRequest = new  List<UploadMultipleDocController.MultipleDocRequest>();
        Integer intIncrementor = 1;
        if( String.isNotBlank( eoiAttachmentBody ) && String.isNotBlank( eoiAttachmentName ) ) {
            
            UploadMultipleDocController.MultipleDocRequest objDocRequest = new UploadMultipleDocController.MultipleDocRequest();
            objDocRequest.category =  'Document';
            objDocRequest.entityName = 'Damac Service Requests'; 
            objDocRequest.fileDescription  =  'EOI Receipt';                        
            
            intIncrementor++;
            objDocRequest.fileId = objCase.CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+extractName(eoiAttachmentName).substringAfterLast('.');
            objDocRequest.fileName = objCase.CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+extractName(eoiAttachmentName).substringAfterLast('.');
            
            if(objCase != null){
                objDocRequest.registrationId =  objCase.CaseNumber;
            }
            objDocRequest.sourceFileName  = 'IPMS-'+objCase.CaseNumber+'-'+extractName( eoiAttachmentName );
            objDocRequest.sourceId  =  'IPMS-'+objCase.CaseNumber+'-'+extractName( eoiAttachmentName );                        
            if(String.isNotBlank(eoiAttachmentBody)){
                blob objBlob = extractBody(eoiAttachmentBody);
                if(objBlob != null){
                    objDocRequest.base64Binary =  EncodingUtil.base64Encode(objBlob);
                }
            }
            lstMultipleDocRequest.add(objDocRequest);                          
        }
        
        if( String.isNotBlank( idAttachmentBody ) && String.isNotBlank( idAttachmentName ) ) {
            
            UploadMultipleDocController.MultipleDocRequest objDocRequest = new UploadMultipleDocController.MultipleDocRequest();
            objDocRequest.category =  'Document';
            objDocRequest.entityName = 'Damac Service Requests'; 
            objDocRequest.fileDescription  =  'ID';                        
            
            intIncrementor++;
            objDocRequest.fileId = objCase.CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+extractName(idAttachmentName).substringAfterLast('.');
            objDocRequest.fileName = objCase.CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+extractName(idAttachmentName).substringAfterLast('.');
            
            if(objCase != null){
                objDocRequest.registrationId =  objCase.CaseNumber;
            }
            objDocRequest.sourceFileName  = 'IPMS-'+objCase.CaseNumber+'-'+extractName( idAttachmentName );
            objDocRequest.sourceId  =  'IPMS-'+objCase.CaseNumber+'-'+extractName( idAttachmentName );                        
            if(String.isNotBlank(idAttachmentBody)){
                blob objBlob = extractBody(idAttachmentBody);
                if(objBlob != null){
                    objDocRequest.base64Binary =  EncodingUtil.base64Encode(objBlob);
                }
            }
            lstMultipleDocRequest.add(objDocRequest);                          
        }
        
        if( String.isNotBlank( wtfAttachmentBody ) && String.isNotBlank( wtfAttachmentName ) ) {
            
            UploadMultipleDocController.MultipleDocRequest objDocRequest = new UploadMultipleDocController.MultipleDocRequest();
            objDocRequest.category =  'Document';
            objDocRequest.entityName = 'Damac Service Requests'; 
            objDocRequest.fileDescription  =  'Wire Transfer Form';
                                                  
            intIncrementor++;
            objDocRequest.fileId = objCase.CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+extractName(wtfAttachmentName).substringAfterLast('.');
            objDocRequest.fileName = objCase.CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+extractName(wtfAttachmentName).substringAfterLast('.');
            
            
            if(objCase != null){
                objDocRequest.registrationId =  objCase.CaseNumber;
            }
            objDocRequest.sourceFileName  = 'IPMS-'+objCase.CaseNumber+'-'+extractName( wtfAttachmentName );
            objDocRequest.sourceId  =  'IPMS-'+objCase.CaseNumber+'-'+extractName( wtfAttachmentName );
            if(String.isNotBlank(wtfAttachmentBody)){
                blob objBlob = extractBody(wtfAttachmentBody);
                if(objBlob != null){
                    objDocRequest.base64Binary =  EncodingUtil.base64Encode(objBlob);
                }
            }
            lstMultipleDocRequest.add(objDocRequest);                          
        }
        
        if( String.isNotBlank( otherAttachmentBody ) && String.isNotBlank( otherAttachmentName ) ) {
            
            UploadMultipleDocController.MultipleDocRequest objDocRequest = new UploadMultipleDocController.MultipleDocRequest();
            objDocRequest.category =  'Document';
            objDocRequest.entityName = 'Damac Service Requests'; 
            objDocRequest.fileDescription  =  'Power of Attorney';
                                                  
            intIncrementor++;
            objDocRequest.fileId = objCase.CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+extractName(otherAttachmentName).substringAfterLast('.');
            objDocRequest.fileName = objCase.CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+extractName(otherAttachmentName).substringAfterLast('.');
            
            
            if(objCase != null){
                objDocRequest.registrationId =  objCase.CaseNumber;
            }
            objDocRequest.sourceFileName  = 'IPMS-'+objCase.CaseNumber+'-'+extractName( otherAttachmentName );
            objDocRequest.sourceId  =  'IPMS-'+objCase.CaseNumber+'-'+extractName( otherAttachmentName );
            if(String.isNotBlank(otherAttachmentBody)){
                blob objBlob = extractBody(otherAttachmentBody);
                if(objBlob != null){
                    objDocRequest.base64Binary =  EncodingUtil.base64Encode(objBlob);
                }
            }
            lstMultipleDocRequest.add(objDocRequest);                          
        }
        return lstMultipleDocRequest;
    }
    
    // Method to form attachment object
    @testVisible
    private void formAttachment( String strfileName, Id parentId, String strDocType ) {
        /*Attachment objAttach = new Attachment() ;
        objAttach.Body = objBody ;
        objAttach.Name = strfileName ;
        //objAttach.ParentId = obj.id;
        lstAttachment.add( objAttach );*/
        
        SR_Attachments__c objAttach = new SR_Attachments__c ();
        objAttach.name = strfileName;
        objAttach.Case__c = objCase.Id;         
        objAttach.type__c = strDocType ;
        objAttach.Account__c = accountId;
        //obj.IsValid__c = (docType.equalsIgnoreCase('Wire Transfer Form') || docType.equalsIgnoreCase('Other')) ? true : false;
              
        lstSRattachmentAll.add(objAttach);
    }
    

    private String extractName( String strName ) {
        return strName.substring( strName.lastIndexOf('\\')+1 ) ;
    }
    private Blob extractBody( String strBody ) {
        //System.debug('--strBody ---'+strBody );
        strBody = EncodingUtil.base64Decode( strBody ).toString();
        return EncodingUtil.base64Decode( strBody.substring( strBody.lastIndexOf(',')+1 ) );
    }
    
    // Method for logging error
    @testVisible
    private void errorLogger(string strErrorMessage, string strCaseID, string strBookingUnitID) {
        Error_Log__c objError = new Error_Log__c();
        objError.Error_Details__c = strErrorMessage;
        if(String.isNotBlank(strCaseID) && strCaseID.startsWith('500')){
            objError.Case__c = strCaseID;
        }
        if(String.isNotBlank(strBookingUnitID)){
            objError.Booking_unit__c = strBookingUnitID;
        }
        insert objError;
    }
    
    public void clearValues() {
        if(objServiceRequest != Null && objServiceRequest.Payment_Mode__c == 'Credit Card') {
            objServiceRequest.Beneficiary_Account_Name__c = '';
            objServiceRequest.Beneficiary_Account_Number__c = '';
            objServiceRequest.Beneficiary_Add__c = '';
            objServiceRequest.Beneficiary_Bank_Branch__c = '';
            objServiceRequest.Beneficiary_Bank_Name__c = '';
            objServiceRequest.Beneficiary_Branch_Country__c = '';
            objServiceRequest.Beneficiary_IBAN__c = '';
            objServiceRequest.Beneficiary_Name__c = '';
            objServiceRequest.IFSC__c = '';
            objServiceRequest.Swift_Code__c = '';
            objServiceRequest.Supp_Corr_Bank_Branch_add1__c = '';
            objServiceRequest.Supp_Corr_Bank_Name__c = '';
            objServiceRequest.Supp_Corr_Bank_Swift_Code__c = '';
            objServiceRequest.Sort_Code__c = '';
      }
        
    }
}