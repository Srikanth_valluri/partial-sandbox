/*
 * Revision History:
 * Version   Author              Date         Description
 * 1.1       Niranjani Mishra    21/02/2018   Initial Draft
 */
@isTest
public class CRE_Cheque_Replacement_Controller_CCTest {
    static Account  objAcc;
    static Case objCase;
    static Booking__c objBooking;
    static Booking_Unit__c objBookingUnit;
    static NSIBPM__Service_Request__c objSR;
    static Booking_Unit_Active_Status__c objBUActive;

    static void testDataSetup() {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person',party_ID__C='123456');
        insert objAcc;

        objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;

        objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;

        objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'Test Unit';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC';
        insert objBookingUnit;

        objBUActive = new Booking_Unit_Active_Status__c ();
        objBUActive.name = 'Test Active Status';
        objBUActive.Status_Value__c = 'Agreement executed by DAMAC';
        insert objBUActive;
    }

    static testMethod void testChequeReplacementCaseCreation() {
        testDataSetup();

        PageReference pageRef = Page.DummyTitleDeedCREPortal;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('view', 'chequereplacement');
        //ApexPages.currentPage().getParameters().put('AccountID', objAcc.id);
        CRE_Cheque_Replacement_Controller_CC objCntlr = new CRE_Cheque_Replacement_Controller_CC();
        objCntlr.init();

        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id);
        ApexPages.currentPage().getParameters().put('SRType', 'ChequeReplacement');
        objCntlr = new CRE_Cheque_Replacement_Controller_CC();
        objCntlr.init();

        objCntlr.getName();
        objCntlr.bookingUnitDetails();

        objCntlr.submitCase();
        objCntlr.strSelectedProcess = 'Cheque Replacement';
        objCntlr.submitCase();

        objCntlr.strSelectedBookingUnit = objBookingUnit.Id;

        Test.startTest();
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        objCntlr.bookingUnitDetails();

        objCntlr.lstChequeDetailWrapper[0].blnIsReplaced = true;
        objCntlr.replaceCheques();

        objCntlr.lstChequeDetailWrapper[0].strSelectedReason = 'test reason';
        objCntlr.replaceCheques();
        objCntlr.submitCase();

        Case_Post_Dated_Cheque__c objPDC = new Case_Post_Dated_Cheque__c();
        objPDC.Case__c = objCntlr.objCase.id;
        objPDC.Receipt_No__c = '25524';
        objPDC.New_maturity_date__c = system.today();
        insert objPDC;

        ApexPages.currentPage().getParameters().put('caseID', objCntlr.objCase.Id);
        objCntlr.init();
        Test.stopTest();
    }

    static testMethod void testChequeHoldCaseCreation() {
        testDataSetup();

        PageReference pageRef = Page.DummyTitleDeedCREPortal;
        Test.setCurrentPage(pageRef);

        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id);
        ApexPages.currentPage().getParameters().put('SRType', 'ChequeReplacement');
        ApexPages.currentPage().getParameters().put('view', 'chequereplacement');
        CRE_Cheque_Replacement_Controller_CC objCntlr = new CRE_Cheque_Replacement_Controller_CC();
        objCntlr.init();

        objCntlr.getName();
        objCntlr.strSelectedProcess = 'Cheque Hold';
        objCntlr.strSelectedBookingUnit = objBookingUnit.Id;

        Test.startTest();
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        objCntlr.bookingUnitDetails();

        objCntlr.lstChequeDetailWrapper[0].blnIsReplaced = true;
        objCntlr.submitCase();
        objCntlr.lstChequeDetailWrapper[0].strSelectedDayHold = '1';
        objCntlr.submitCase();
        Test.stopTest();
    }

    static testMethod void testSecurityChequeCollectionCaseCreation() {
        testDataSetup();

        PageReference pageRef = Page.DummyTitleDeedCREPortal;
        Test.setCurrentPage(pageRef);

        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id);
        ApexPages.currentPage().getParameters().put('SRType', 'ChequeReplacement');
        ApexPages.currentPage().getParameters().put('view', 'chequereplacement');
        CRE_Cheque_Replacement_Controller_CC objCntlr = new CRE_Cheque_Replacement_Controller_CC();
        objCntlr.init();

        objCntlr.getName();
        objCntlr.strSelectedProcess = 'Security Cheque Collection';
        objCntlr.strSelectedBookingUnit = objBookingUnit.Id;

        Test.startTest();
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        objCntlr.bookingUnitDetails();

        objCntlr.submitCase();
        objCntlr.lstChequeDetailWrapper[0].blnIsReplaced = true;
        objCntlr.submitCase();
        Test.stopTest();
    }
}