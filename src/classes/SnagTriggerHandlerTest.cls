@isTest()
public class SnagTriggerHandlerTest{
    public static testMethod void rollupSnags(){
        Case objC = new Case();
        objC.Status = 'New';
        objC.Origin = 'Walk-In';
        insert objC;
        
        SNAGs__c objS = new SNAGs__c();
        objS.Case__c = objC.Id;
        objS.Priority__c = 'High';
        objS.Defect_Current_Status__c = 'Open';
        insert objS;
        
        objS.Defect_Current_Status__c = 'Rejected';
        update objS;
        
        objS.Defect_Current_Status__c = 'Closed';
        update objS;
        
        delete objS;
    }
}