/**********************************************************************************************************************
 * Description : Class to block owner reassignment to admin
 * --------------------------------------------------------------------------------------------------------------------
 * Version | Date(DD-MM-YYYY)  | Last Modified By  | Comments
 * --------------------------------------------------------------------------------------------------------------------
 * 1.0     | 24-09-2020        |Aishwarya Todkar   | Initial Draft
***********************************************************************************************************************/
public with sharing class OwnerReassignmentHandler {

    Static String adminUserId = Label.Admin_User_Id;    
/**********************************************************************************************************************
 * Description  : Method to block owner change to Admin for cases
 * Paramaeter   : Old Map, New Map
 * Return Type  : void
***********************************************************************************************************************/    
    public static void onBeforeUpdateCase( Map<Id, Case> oldMap, Map<Id, Case> newMap ) {
        
        for( Id caseId : newMap.keySet() ) {
            if( adminUserId.contains( String.valueOf( newMap.get( caseId ).ownerId ) )
            && !adminUserId.contains( String.valueOf( oldMap.get( caseId ).ownerId ) ) ) {
                newMap.get( caseId ).ownerId = oldMap.get( caseId ).ownerId;
            }
        }
    }

/**********************************************************************************************************************
 * Description  : Method to block owner change to Admin for calling lists
 * Paramaeter   : Old Map, New Map
 * Return Type  : void
***********************************************************************************************************************/    

    public static void onBeforeUpdateCL( Map<Id, Calling_List__c> oldMap, Map<Id, Calling_List__c> newMap ) {

        for( Id clId : newMap.keySet() ) {
            if( adminUserId.contains( String.valueOf( newMap.get( clId ).ownerId ) )
            && !adminUserId.contains( String.valueOf( oldMap.get( clId ).ownerId ) ) ) {
                newMap.get( clId ).ownerId = oldMap.get( clId ).ownerId;
            }
        }
    }
}