global without sharing class Damac_Schedule_Meeting {

    public Inquiry__c inq { get; set; }
    public Task meetingScheduledTask { get; set; }
    public String customMessage { get; set; }
    public String messageType { get; set; }   
 
    public Damac_Schedule_Meeting() {
        customMessage = '';
        messageType = '';        
        init();
    }
 
    public Damac_Schedule_Meeting (ApexPages.StandardController stdController) {
        customMessage = '';
        messageType = '';        
        init();
    }       

    public void init() {
        inq = new Inquiry__c();
        meetingScheduledTask = new Task ();
        meetingScheduledTask.Subject = 'Meeting at Office';
        meetingScheduledTask.Activity_Type_3__c = 'Meeting at Office';
        
    }
    
    public void createMeetingFollowup () {
        customMessage = '';
        messageType = '';
        try {
            meetingScheduledTask.WhatID = apexpages.currentpage().getparameters().get('id');
            if (inq.Meeting_Due_Date__c != NULL)
                meetingScheduledTask.ActivityDate = Date.ValueOf (inq.Meeting_Due_Date__c);
            if (meetingScheduledTask.ID != NULL)
                update meetingScheduledTask;    
            else
                insert meetingScheduledTask;
            inq.Id = apexpages.currentpage().getparameters().get('id');
            if (inq.Interested_Location__c != NULL) {
                String location = inq.Interested_Location__c;
                if (location.contains(', ')) {
                    location = location.removeStart('[').removeEnd(']').replaceAll(', ', ';');
                } else {
                    location = location.removeStart('[').removeEnd(']');
                }
                System.Debug(location);
                inq.Interested_Location__c = location;
            }
            if (inq.Interested_Development__c != NULL) {
                String location = inq.Interested_Development__c;
                if (location.contains(', ')) {
                    location = location.removeStart('[').removeEnd(']').replaceAll(', ', ';');
                } else {
                    location = location.removeStart('[').removeEnd(']');
                }
                inq.Interested_Development__c = location;
            }
            update inq;
            customMessage = 'Meeting Scheduled';
            messageType = 'success';
            init();
        }
        catch (Exception e) {
            messageType = 'error';
            customMessage = e.getMessage ();
        }
    }
    
    public List<SelectOption> getLocation(){
        
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Inquiry__c.Interested_Location__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        //ple.sort();
        for( Schema.PicklistEntry f : ple) {
              options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return options;
    }
    public List<SelectOption> getDevelopement(){
        
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Inquiry__c.Interested_Development__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        //ple.sort();
        for( Schema.PicklistEntry f : ple) {
              options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return options;
    }
}