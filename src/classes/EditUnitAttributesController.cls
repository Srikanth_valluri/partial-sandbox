/*********************************************************************************************************
* Name               : EditUnitAttributesController
* Test Class         : 
* Description        : Controller class for EditUnitAttributes VF Page
* Created Date       : 16/11/2020
* -----------------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst         16/11/2020      Initial Draft.
**********************************************************************************************************/
global With Sharing Class EditUnitAttributesController{
    PUBLIC List<String> propertyNames {get; set;}
    public string propertyId {get; set;}
    public boolean propertyExists {get; set;}
    public string propertyName {get; set;}
    Map<String, Property__c> propertyMap;
    Map<String, Inventory__c> invNameMap;
    public List<Inventory__c> inventoryList {get;set;}
    public string unitIds{get;set;}
    public string unitDetails {get; set;}
    public String[] unitIdList{get;set;}

    /*********************************************************************************************
    * @Description : Controller class.
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public EditUnitAttributesController() {
        propertyNames = new List<String>();
        propertyName = '';      
        propertyMap = new  Map<String, Property__c>();
        invNameMap = new  Map<String, Inventory__c>();
        inventoryList = new List<Inventory__c> ();
        unitDetails = JSON.serialize(invNameMap.values());
        
        for(Property__c property: [SELECT Id, Property_Name__c, Property_ID__c 
                                   FROM Property__c 
                                   WHERE Property_Id_is_Negative__c = FALSE
                                   AND Property_Name__c != '']){
            propertyMap.put(property.Id, property);
            propertyNames.add(property.Property_Name__c);
        }
        fetchUnitDetails();
    }
    
    /************************************************************************************************
    * @Description : method to fetch the Unit Details.
    * @Params      : void
    * @Return      : void
    ************************************************************************************************/
   public void fetchUnitDetails(){
        String projId = ApexPages.currentPage().getParameters().get('id');
        propertyId = projId;
        Property__c property =  propertyMap.get(projId);
        system.debug('projId: ' + projId);
        fetchPropertyDetails(property.Property_Name__c);
    }
    
    /************************************************************************************************
    * @Description : method to fetch the Unit Details.
    * @Params      : void
    * @Return      : void
    ************************************************************************************************/
    public void updateInventories(){
        String invDetails = ApexPages.currentPage().getParameters().get('invDetails');
        system.debug('invDetails: ' + invDetails);
       
        if(invDetails != null && invDetails != ''){
            String jsonString = invDetails.replace('Space Type', 'space_type');
            jsonString = jsonString.replace('Completed %', 'completed_per');
            jsonString = jsonString.replace('BCC Date', 'BCC_date');
            jsonString = jsonString.replace('Plot No.', 'plot_no');
            jsonString = jsonString.replace('Municipality No', 'municipality_no');
            jsonString = jsonString.replace('Con Status', 'con_status');
            jsonString = jsonString.replace('View Type', 'view_type');
            jsonString = jsonString.replace('Store Room', 'store_room');
            jsonString = jsonString.replace('DEWA No', 'DEWA_no');
            jsonString = jsonString.replace('Accessible Unit', 'accessible_unit');
            jsonString = jsonString.replace('Shell & Core', 'shell_core');
            system.debug('jsonString: ' + jsonString);
            List<invWrapperUnitAttributeJSON> wrapperList = invWrapperUnitAttributeJSON.parse(jsonString);
            system.debug('wrapperList: ' + wrapperList);
            List<Inventory__c> invToUpdate = new List<Inventory__c>();
            List<Inventory_Log__c> invLogList = new List<Inventory_Log__c>();
            for(invWrapperUnitAttributeJSON wrapper: wrapperList){
                system.debug('wrapper: ' + wrapper);
                Boolean updated = false;
                Inventory__c inv =  invNameMap.get(wrapper.unit);
                if(wrapper.Price != null && wrapper.Price != '' 
                        && inv.Special_Price__c != Decimal.valueOf(wrapper.Price)){
                    invLogList.add(InventoryProjectUtility.createInventoryLog(inv.Id, 'Special_Price__c', 
                                      String.valueOf(inv.Special_Price__c), wrapper.Price,  
                                      'Updated from Edit Unit Attributes', ''));
                    inv.Special_Price__c = Decimal.valueOf(wrapper.Price);
                }
                if(wrapper.Bedroom != null && wrapper.Bedroom != '' 
                        && inv.IPMS_Bedrooms__c != wrapper.Bedroom){
                    invLogList.add(InventoryProjectUtility.createInventoryLog(inv.Id, 'IPMS_Bedrooms__c', 
                                      String.valueOf(inv.IPMS_Bedrooms__c), wrapper.Bedroom,  
                                      'Updated from Edit Unit Attributes', ''));
                    inv.IPMS_Bedrooms__c = wrapper.Bedroom;
                }
                if(wrapper.space_type != null && wrapper.space_type != '' 
                        && inv.Space_Type_Lookup_Code__c != wrapper.space_type){
                    
                    invLogList.add(InventoryProjectUtility.createInventoryLog(inv.Id, 'Space_Type_Lookup_Code__c', 
                                      String.valueOf(inv.Space_Type_Lookup_Code__c), wrapper.space_type,  
                                      'Updated from Edit Unit Attributes', ''));
                    inv.Space_Type_Lookup_Code__c = wrapper.space_type;
                }
                
                if(wrapper.ACD != null && wrapper.ACD != '' 
                        && inv.ACD_Date__c != wrapper.ACD){
                    invLogList.add(InventoryProjectUtility.createInventoryLog(inv.Id, 'ACD_Date__c', 
                                      String.valueOf(inv.ACD_Date__c), wrapper.ACD,  
                                      'Updated from Edit Unit Attributes', ''));
                    inv.ACD_Date__c = wrapper.ACD;
                }
                if(wrapper.completed_per != null && wrapper.completed_per != '' 
                        && inv.Unit_Completion__c != Decimal.valueOf(wrapper.completed_per)){
                    invLogList.add(InventoryProjectUtility.createInventoryLog(inv.Id, 'Unit_Completion__c', 
                                      String.valueOf(inv.Unit_Completion__c), wrapper.completed_per,  
                                      'Updated from Edit Unit Attributes', ''));
                    inv.Unit_Completion__c = Decimal.valueOf(wrapper.completed_per);
                }
                system.debug('wrapper.BCC_date: ' + wrapper.BCC_date);
                system.debug('inv.Unit_BCC_Date__c: ' + inv.Unit_BCC_Date__c);
                if(wrapper.BCC_date != null && wrapper.BCC_date != '' 
                        && inv.Unit_BCC_Date__c != Date.valueOf(wrapper.BCC_date)){
                    invLogList.add(InventoryProjectUtility.createInventoryLog(inv.Id, 'Unit_BCC_Date__c', 
                                      String.valueOf(inv.Unit_BCC_Date__c), wrapper.BCC_date,  
                                      'Updated from Edit Unit Attributes', ''));
                    inv.Unit_BCC_Date__c =  Date.valueOf(wrapper.BCC_date);
                }
                
                system.debug('wrapper.Furnished: ' + wrapper.Furnished);
                if(wrapper.Furnished != null && wrapper.Furnished != '' 
                        && inv.Furnished__c != Boolean.valueOf(wrapper.Furnished)){
                    invLogList.add(InventoryProjectUtility.createInventoryLog(inv.Id, 'Furnished__c', 
                                      String.valueOf(inv.Furnished__c), wrapper.Furnished,  
                                      'Updated from Edit Unit Attributes', ''));
                    inv.Furnished__c = Boolean.valueOf(wrapper.Furnished);
                }
                
                if(wrapper.plot_no != null && wrapper.plot_no != '' 
                        && inv.Plot_Number__c != wrapper.plot_no){
                    invLogList.add(InventoryProjectUtility.createInventoryLog(inv.Id, 'Plot_Number__c', 
                                      String.valueOf(inv.Plot_Number__c), wrapper.plot_no,  
                                      'Updated from Edit Unit Attributes', ''));
                    inv.Plot_Number__c = wrapper.plot_no;
                }
                if(wrapper.municipality_no != null && wrapper.municipality_no != '' 
                        && inv.Municipality_Number__c != wrapper.municipality_no){
                    invLogList.add(InventoryProjectUtility.createInventoryLog(inv.Id, 'Municipality_Number__c', 
                                      String.valueOf(inv.Municipality_Number__c), wrapper.municipality_no,  
                                      'Updated from Edit Unit Attributes', ''));
                    inv.Municipality_Number__c = wrapper.municipality_no;    
                }
               
                if(wrapper.Parkings != null && wrapper.Parkings != '' 
                        && inv.Parking__c != wrapper.Parkings){
                    invLogList.add(InventoryProjectUtility.createInventoryLog(inv.Id, 'Parking__c', 
                                      String.valueOf(inv.Parking__c), wrapper.Parkings,  
                                      'Updated from Edit Unit Attributes', ''));
                    inv.Parking__c = wrapper.Parkings;
                }
                
                if(wrapper.view_type != null && wrapper.view_type != '' 
                        && inv.View_Type__c != wrapper.view_type){
                    invLogList.add(InventoryProjectUtility.createInventoryLog(inv.Id, 'View_Type__c', 
                                      String.valueOf(inv.View_Type__c), wrapper.view_type,  
                                      'Updated from Edit Unit Attributes', ''));
                    inv.View_Type__c = wrapper.view_type;
                }
                if(wrapper.store_room != null && wrapper.store_room != '' 
                        && inv.Store_Room__c != wrapper.store_room){
                    invLogList.add(InventoryProjectUtility.createInventoryLog(inv.Id, 'Store_Room__c', 
                                      String.valueOf(inv.Store_Room__c), wrapper.store_room,  
                                      'Updated from Edit Unit Attributes', ''));
                    inv.Store_Room__c = wrapper.store_room;
                }
                if(wrapper.DEWA_no != null && wrapper.DEWA_no != '' 
                        && inv.DEWA_Number__c != wrapper.DEWA_no){
                    invLogList.add(InventoryProjectUtility.createInventoryLog(inv.Id, 'DEWA_Number__c', 
                                      String.valueOf(inv.DEWA_Number__c), wrapper.DEWA_no,  
                                      'Updated from Edit Unit Attributes', ''));
                    inv.DEWA_Number__c = wrapper.DEWA_no;
                }
                if(wrapper.con_status != null && wrapper.con_status != '' 
                        && inv.Con_Status__c != wrapper.con_status){
                    invLogList.add(InventoryProjectUtility.createInventoryLog(inv.Id, 'Con_Status__c', 
                                      String.valueOf(inv.Con_Status__c), wrapper.con_status,  
                                      'Updated from Edit Unit Attributes', ''));
                    inv.Con_Status__c = wrapper.con_status;
                }
                
                 /*  if(wrapper.accessible_unit != null && wrapper.accessible_unit != '' 
                        && inv.Accessible_Unit__c != wrapper.accessible_unit){
                    invLogList.add(InventoryProjectUtility.createInventoryLog(inv.Id, 'Accessible_Unit__c', 
                                      String.valueOf(inv.Accessible_Unit__c), wrapper.accessible_unit,  
                                      'Updated from Edit Unit Attributes', ''));
                    inv.Accessible_Unit__c = wrapper.accessible_unit;
                }
             if(wrapper.shell_core != null && wrapper.shell_core != '' 
                        && inv.Shell_Core__c != wrapper.shell_core){
                    invLogList.add(InventoryProjectUtility.createInventoryLog(inv.Id, 'Shell_Core__c', 
                                      String.valueOf(inv.Shell_Core__c), wrapper.shell_core,  
                                      'Updated from Edit Unit Attributes', ''));
                    inv.Shell_Core__c = wrapper.shell_core;
                }*/
                
                invToUpdate.add(inv);
            }
            system.debug('invToUpdate: ' + invToUpdate);
            if(invToUpdate.size() > 0){
                update invToUpdate;
                if(invLogList.size() > 0){
                    insert invLogList;
                }
                fetchUnitDetails();
            }
        }
    }
    
      /************************************************************************************************
    * @Description : method to fetch the  Property Details.
    * @Params      : void
    * @Return      : void
    ************************************************************************************************/
  public void fetchPropertyDetails(String projName) {
      inventoryList = new List < Inventory__c > ();
      system.debug('projName: ' + projName);
      for (Inventory__c inv: [SELECT Id, Status__c, Unit_Location__c, Bedroom_Type_Code__c, ACD_Date__c,
                                Unit_Name__c, Building_Name__c, Property_Name_2__c,  Property__c,
                                Price__c, Selling_Price__c,Special_Price_2__c,
                                Area_Sqft__c, Special_Price__c, Unit_Completion__c,
                                Municipality_Number__c, Con_Status__c, DEWA_Number__c,
                                /*Accessible_Unit__c,*/IPMS_Bedrooms__c, Space_Type_Lookup_Code__c,
                                BccDate__c, Store_Room__c, Unit_BCC_Date__c,
                                Plot_Number__c, Parking__c, View_Type__c,Furnished__c
                                
                  FROM Inventory__c
                  WHERE Property__r.Property_Name__c =: projName
                  AND Unit_Name__c != '']) {
        invNameMap.put(inv.Unit_Name__c, inv);
      }
      system.debug('inventoryList: ' + invNameMap.values());
      unitDetails = JSON.serialize(invNameMap.values());
      system.debug('unitDetails: ' + unitDetails);
    }
   

}