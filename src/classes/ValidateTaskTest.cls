@isTest
public with sharing class ValidateTaskTest {
    @isTest
    static void testmethod1(){
         // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('AOPT').getRecordTypeId();
        
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase.Type = 'Change of Contact Details';
        objCase.IsPOA__c = false;
        objCase.OCR_verified__c = true;
        objCase.OQOOD_Fee_Applicable__c = false;
        objCase.Approval_Status__c = 'Approved' ;
        objCase.Status = 'Working';
        insert objCase;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        objSR.Agency__c = objAcc.Id ;
        insert objSR ;

        Id dealId = objSR.Deal_ID__c;
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id,1 );
        insert lstBookings ; 

        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings,1 );
        insert lstBookingUnits;
        
        Task objTask = new task();
        objTask.WhatId = objCase.id;
        objTask.subject = 'Follow up with customer';
        objTask.status = 'In progress';
        insert objTask;
        
        Test.startTest();
        objTask.status='Completed';
        try{
            update objTask;
            ValidateTask.validateFollowUpTask(objTask);
        }
        catch(Exception exceptionObject){
            System.assertEquals(
                exceptionObject.getMessage().contains('There is no notes for the day'),
                true,
                'This is the error added.'
            );
        }
        
        Test.stopTest();
        
    }
}