/*******************************************************************************************************
Description : Class to get shorten url from Rebrandly Service
--------------------------------------------------------------------------------------------------------
Version | Date(DD-MM-YYYY)  | Last Modified By  | Comments
--------------------------------------------------------------------------------------------------------
1.0     | 19-02-2020        |Aishwarya Todkar    | Initial Draft
********************************************************************************************************/
public with sharing class RebrandlyService {
    
    
    public static RebradnlyResponseWrapper getShortenUrl( String urlTobBeShort ) {

        Credentials_Details__c rebrandlyCs = Credentials_Details__c.getInstance( 'Rebrandly' );
        System.debug('rebrandlyCs==' + rebrandlyCs );
        
        String requestBody = '{' +
                                '"destination": "' + urlTobBeShort + '"' +
                                ', "domain": {' +
                                    '"fullName": "rebrand.ly"' +
                                '}' +
                            '}';
        if( rebrandlyCs != null ) {
            HttpRequest request = new HttpRequest();
            request.setBody( requestBody );
            request.setMethod( 'POST' );
            request.setHeader( 'apiKey', rebrandlyCs.Password__c );
            request.setHeader('Content-Type', 'application/json' );
            request.setEndpoint( rebrandlyCs.EndPoint__c );
            request.setHeader('Accept', 'application/json' );
            System.debug( 'Rebrandly request==' + request);
            System.debug( 'Rebrandly request Endpoint=' + request.getEndpoint());
            System.debug( 'Rebrandly request body ==' + request.getBody());
            HttpResponse response = new HttpResponse();

            response = new Http().send( request );
            System.debug( 'Rebrandly Response==' + response);
            System.debug( 'Rebrandly request apiKey header=' + response.getHeader('apikey'));
            System.debug( 'Rebrandly request apiKey header=' + response.getHeaderKeys());
            if( response.getBody() != null 
            && response.getStatusCode() == 200 ) {

                RebradnlyResponseWrapper objResWrap = RebradnlyResponseWrapper.parse( response.getBody() );
                // ( RebradnlyResponseWrapper )JSON.deserialize( response.getBody() , RebradnlyResponseWrapper.Class );
                // System.debug( 'Rebrandly deserialized Response == ' + objResWrap);
                return objResWrap;
            }
        }
        return null;
    }
}

/**
 RebrandlyService.getShortenUrl( 'https://www.youtube.com/channel/UCHK4HD0ltu1-I212icLPt3g' );
 */