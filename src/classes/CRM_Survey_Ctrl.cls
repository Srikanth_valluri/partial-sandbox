/*******************************************************************************************************
Description : Class to send survey email and sms
--------------------------------------------------------------------------------------------------------
Version | Date(DD-MM-YYYY)  | Last Modified By  | Comments
--------------------------------------------------------------------------------------------------------
1.0     | 10-12-2019        |Aishwarya Todkar    | Initial Draft

1.1     | 21-07-2020        |Aishwarya Todkar    | Marked duplicate on survey feedback
********************************************************************************************************/
public Class CRM_Survey_Ctrl {

    public String CxExperience      { get; set; }
    public String emailBody      { get; set; }
    public Integer selectedRating   { get; set; }
    String parentId;
    String creId;
    String accountId;
    String templateName;
    String surveyType;
    final String EMAIL_CASE_SURVEY = 'Email Case Survey';
    final String CALL_LOG_SURVEY = 'Call Log Survey';
    final String SOBJECT_SUFFIX = '__c';

    public CRM_Survey_Ctrl() {

        emailBody = '';

        parentId = ApexPages.currentPage().getParameters().get( 'id' );
        creId = ApexPages.currentPage().getParameters().get( 'creid' );
        accountId = ApexPages.currentPage().getParameters().get( 'AccountId' );
        templateName = ApexPages.currentPage().getParameters().get( 'Template' );
        String tempSurveyType = ApexPages.currentPage().getParameters().get( 'SurveyType' );
        
        if( String.isNotBlank( tempSurveyType ) ) {
            if( tempSurveyType.equalsIgnoreCase( 'EmailCaseSurvey' ) )
                surveyType = EMAIL_CASE_SURVEY;
            else if( tempSurveyType.equalsIgnoreCase( 'CallLogSurvey' ) )
                surveyType = CALL_LOG_SURVEY;
        }

        if( String.isNotBlank( templateName ) )
            emailBody = getSurveyEmailBody();
    }

/**************************************************************************************************************
Description : Method to get and display email body on UI
Parameters  : None
Return Type : String ( email body )
**************************************************************************************************************/ 
    public String getSurveyEmailBody() {

        String strEmailBody = '';
        System.debug('templateName = ' + templateName); //IMP

        //Get Email Template
        List<EmailTemplate> listEmailTemplate = new List<EmailTemplate>( [SELECT 
                                        Id,Name, Subject, Body, HtmlValue, TemplateType
                                    FROM 
                                        EmailTemplate 
                                    WHERE 
                                        Name =: templateName 
                                    OR
                                        DeveloperName =: templateName
                                    LIMIT 1
                                        ] );

        if( listEmailTemplate != null && listEmailTemplate.size() > 0 && String.isNotBlank( accountId ) ) {
            List<EmailMessage> lstEmails = new List<EmailMessage>( [ SELECT
                                                                    Id 
                                                                    , TextBody
                                                                FROM
                                                                    EmailMessage
                                                                WHERE
                                                                    Subject =: listEmailTemplate[0].Subject
                                                                AND
                                                                    RelatedToId =: accountId
                                                                Order By 
                                                                    CreatedDate DESC
                                                                LIMIT 1] );
            System.debug('lstEmails = ' + lstEmails);
            if( lstEmails!= null && lstEmails.size() > 0)
                strEmailBody = lstEmails[0].TextBody;

            System.debug( 'strEmailBody -- ' + strEmailBody); //IMP
        }
        return strEmailBody;
    }

/**************************************************************************************************************
Description : Method to capture survey response in sf
Parameters  : None
Return Type : PageReference
**************************************************************************************************************/ 
    public PageReference submitCrmSurvey() {
        System.debug('selectedRating -' + selectedRating ); //IMP
        System.debug('parentId -' + parentId ); //IMP
        System.debug('creId-' + creId); //IMP
        System.debug('CxExperience -' + CxExperience ); //IMP
        
        String sObjName = Id.valueOf( parentId ).getsobjecttype().getDescribe().getName();
        sObjName = sObjName.endsWith( SOBJECT_SUFFIX ) ? sObjName : sObjName + SOBJECT_SUFFIX;
        
        CRE_Survey_Feedback__c objSurveyRes = new CRE_Survey_Feedback__c();
        objSurveyRes.put( sObjName, parentId );
        objSurveyRes.CRE__c = creId;
        objSurveyRes.Customer_Experience__c = CxExperience;
        objSurveyRes.Rating__c = selectedRating;
        objSurveyRes.Account__c = accountId;
        objSurveyRes.CRM_Survey_Email__c = true;

        if( String.isNotBlank( surveyType ) )
            objSurveyRes.CRM_Survey_Type__c = surveyType;

        //Marking duplicate feedback
        String query = 'SELECT Id FROM CRE_Survey_Feedback__c WHERE CRM_Survey_Email__c = true AND ' + sObjName + ' = \'' + parentId + '\' ';
        if( String.isNotBlank( surveyType ) ) {
            query += ' AND CRM_Survey_Type__c = \'' + surveyType + '\'';
        }
        query += ' Limit 1';

        List<CRE_Survey_Feedback__c> listDuplicateSurveyRes = Database.query( query );
        if( listDuplicateSurveyRes != null && listDuplicateSurveyRes.size() > 0 )
            objSurveyRes.Duplicate_Feedback__c = true;
        insert objSurveyRes;

        System.debug( 'objSurveyRes = ' + objSurveyRes ); //IMP
        return null;
    }
}