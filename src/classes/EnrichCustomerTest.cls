@isTest
public without sharing class EnrichCustomerTest {

    @isTest
    static void positiveTest1(){
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = objAcc.Id);
        insert booking;

        
        Booking_unit__c objBU = new Booking_Unit__c (Booking__c = booking.Id,
                                                     Owner__c = objAcc.Id
                                                     ,Send_Bulk_Email__c =true
                                                     , Registration_Status_Code__c ='xx'
                                                     ,Tenant__c = objAcc.Id
                                                     ,Send_Tenant_Email__c = true
                                                     , DLDN_Notice__c ='afa'
                                                     );
        insert objBU; 
        
        ApexPages.currentPage().getParameters().put('Id',objBU.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objBU);
        EnrichCustomer enrchCus = new EnrichCustomer(sc);
        
        PageReference pageRef = Page.EnrichCustomer;
        pageRef.getParameters().put('id', String.valueOf(objBU.Id));
        Test.setCurrentPage(pageRef);
        Test.startTest();           
            enrchCus.accInst.Customer_Category__c = 'Elite';
            enrchCus.accInst.Subcategory__c = 'Test';
            enrchCus.accInst.Brief_Profile_Biography__c = 'Test'; 
            enrchCus.submitCase();
            enrchCus.onCancel();            
        Test.stopTest();
        
    }


    

}