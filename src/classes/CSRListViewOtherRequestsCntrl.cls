/****************************************************************************************
Description: CSRListViewOtherRequestsCntrl is a controller class for the page 
CSRListViewOtherRequests
----------------------------------------------------------------------------------------*
Version     Date        Author              Description                                 *
1.0        22/07/2020   Shruti Nikam        Initial Draft
*****************************************************************************************/

public class CSRListViewOtherRequestsCntrl {
    public String userid{get;set;}  
    
    /**************************************************************************************
    * Method Description: Method to retrive all pending cases of recordtype: 
                Case Summary - Client Relation and current logged in user
    * Input Parameters : fromDateFilter -- based on no of days filter applied on page
    * Return Type : List<Case> -- List of pending cases based on parameter filter
    ***************************************************************************************/
    public static List<Case> init(String fromDateFilter) {

        Id idOfCurrentLoggedInUser = Userinfo.getuserid();
        String caseStatus = 'Submitted';
        String recordType = 'Case Summary - Client Relation';
        List<Case> pendingCaseList = new List<Case>();
        if(fromDateFilter == 'ALL') {
            fromDateFilter = '';
        } else {
            fromDateFilter = 'AND CreatedDate = LAST_N_DAYS:'+fromDateFilter;
        }
        String queryString = 'SELECT Id, '+
                             'CaseNumber, '+
                             'CreatedDate, '+
                             'Approving_User_Role__c, '+
                             'Approving_User_Name__c, '+
                             'Account.Name,AccountId, '+
                             '(SELECT Id, '+
                                    'Name,Status__c, '+
                                    'Manager_Approval_Assignment_Date__c, '+
                                    'HOD_Approval_Assignment_Date__c, '+
                                    'Committee_Approval_Assignment_Date__c, '+
                                    'Director_Approval_Assignment_Date__c, '+
                                    'RequestType__c '+
                                    'FROM CaseSummaryRequests__r) '+
                             'FROM Case '+
                             'WHERE RecordType.Name = :recordType '+
                             'AND Approving_User_Id__c = : idOfCurrentLoggedInUser '+
                             'AND Status = :caseStatus '+
                              fromDateFilter +
                             'ORDER BY CreatedDate ASC ';

        pendingCaseList = Database.query(queryString);
        return pendingCaseList;
    }

    /**************************************************************************************
    * Method Description: Method to retrive all Case summary request for the current logged in user
    * Input Parameters : fromDateFilter -- based on no of days filter applied on page
    * Return Type : Map<String,List<CaseSummaryRequest__c>> -- Map of cases with Case summary request based on parameter filter
    ***************************************************************************************/
    public static Map<String,List<CaseSummaryRequest__c>> fetchCaseSummaryRequest(String fromDateFilter) {
        
        if(fromDateFilter == 'ALL') {
            fromDateFilter = '';
        } else {
            fromDateFilter = 'AND Case__r.CreatedDate = LAST_N_DAYS:'+fromDateFilter;
        }
        Id currentUserID = Userinfo.getuserid();
        List<CaseSummaryRequest__c> caseSummaryList = new List<CaseSummaryRequest__c>();
        String queryString = 'Select Id,'+
                                    'Name,'+
                                    'Manager_Approval_Assignment_Date__c,'+
                                    'HOD_Approval_Assignment_Date__c,'+
                                    'Committee_Approval_Assignment_Date__c,'+
                                    'Director_Approval_Assignment_Date__c,'+
                                    'RequestType__c,'+
                                    'Manager_Id__c,'+
                                    'Manager_Name__c,'+
                                    'Manager_Approval_Status__c,'+
                                    'HOD_Id__c,HOD_Name__c,'+
                                    'HOD_Approval_Status__c,'+
                                    'Director_Id__c,Director_Name__c,'+
                                    'Director_Approval_Status__c,'+
                                    'Committee_Id__c,Committee_Name__c,'+
                                    'Committee_Approval_Status__c,'+
                                    'Case__r.CaseNumber,'+
                                    'Case__r.Account.Name,'+
                                    'Case__r.AccountId,'+
                                    'Case__r.Status,'+
                                    'Case__c,'+
                                    'Case__r.Approving_User_Role__c,'+
                                    'Case__r.Approving_User_Name__c,'+
                                    'Case__r.CreatedDate,'+
                                    'Committee_Approval_Date__c,'+
                                    'Director_Approval_Date__c,'+
                                    'HOD_Approval_Date__c,'+
                                    'Manager_Approval_Date__c, '+
                                    'Status__c '+
                            'FROM    CaseSummaryRequest__c '+
                            'WHERE   (Manager_Id__c = : currentUserID '+
                            'OR      HOD_Id__c = : currentUserID '+
                            'OR      Director_Id__c = : currentUserID '+
                            'OR      Committee_Id__c = : currentUserID) '+
                            'AND    (Manager_Approval_Status__c != Null '+
                            'OR      HOD_Approval_Status__c != Null '+
                            'OR      Director_Approval_Status__c != Null '+
                            'OR      Committee_Approval_Status__c!= Null) '+
                            fromDateFilter +
                            ' ORDER   BY Case__r.CreatedDate ASC';
        
        caseSummaryList = Database.query(queryString);
        
        Map<String,List<CaseSummaryRequest__c>> caseSummaryMap = new Map<String,List<CaseSummaryRequest__c>>();
        for(CaseSummaryRequest__c objCaseSummary: caseSummaryList) {
            if(caseSummaryMap.containsKey(objCaseSummary.Case__r.CaseNumber) 
               && caseSummaryMap.get(objCaseSummary.Case__r.CaseNumber) != NULL
               && caseSummaryMap.get(objCaseSummary.Case__r.CaseNumber).size() > 0) {
                List<CaseSummaryRequest__c> caseSummaryLst = caseSummaryMap.get(objCaseSummary.Case__r.CaseNumber);
                caseSummaryLst.add(objCaseSummary);
                caseSummaryMap.put(objCaseSummary.Case__r.CaseNumber,caseSummaryLst);
               } else {
                   List<CaseSummaryRequest__c> caseSummaryLst = new List<CaseSummaryRequest__c>();
                   caseSummaryLst.add(objCaseSummary);                 
                   caseSummaryMap.put(objCaseSummary.Case__r.CaseNumber,caseSummaryLst);
               }
            
        }
        return caseSummaryMap;
        
                                                                             
    }    
    
    /**************************************************************************************
    * Method Description: Method to create wrapper list with all Case summary request for 
                            the current logged in user based on their statuses
    * Input Parameters : status -- based on status tile clicked on page
                fromDateFilter -- based on no of days filter applied on page
    * Return Type : List<CSRListViewWrapper> --List of wrapper class with Case summary request based on parameter filter
    ***************************************************************************************/
    
    public static List<CSRListViewWrapper> getStatuswiseCSRList(String status, String fromDateFilter) {
        List<CaseSummaryRequest__c> caseSummaryList = new List<CaseSummaryRequest__c>();
        Map<String,List<CaseSummaryRequest__c>> caseSummaryMap = new Map<String,List<CaseSummaryRequest__c>>();
        List<CSRListViewWrapper> wrapperList = new List<CSRListViewWrapper>();
        caseSummaryMap = fetchCaseSummaryRequest(fromDateFilter);
        for(String caseNumber : caseSummaryMap.keyset()) {
            CSRListViewWrapper wrapperObj = new CSRListViewWrapper();
            for(CaseSummaryRequest__c objCaseSummary:caseSummaryMap.get(caseNumber)) {
                if( (objCaseSummary.Manager_Id__c == Userinfo.getuserid() 
                   && objCaseSummary.Manager_Approval_Status__c == status ) || 
                   (objCaseSummary.HOD_Id__c == Userinfo.getuserid() 
                   && objCaseSummary.HOD_Approval_Status__c == status) || 
                    (objCaseSummary.Director_Id__c == Userinfo.getuserid() 
                   && objCaseSummary.Director_Approval_Status__c == status ) ||
                    (objCaseSummary.Committee_Id__c == Userinfo.getuserid() 
                   && objCaseSummary.Committee_Approval_Status__c == status )) {
                        wrapperObj.caseNumber = caseNumber;
                        wrapperObj.caseId = objCaseSummary.Case__c;
                        wrapperObj.caseAccountName = objCaseSummary.Case__r.Account.Name;
                        wrapperObj.caseAccountId = objCaseSummary.Case__r.AccountId;
                        wrapperObj.csrList = new List<CaseSummaryRequest__c>(caseSummaryMap.get(caseNumber));
                        if( objCaseSummary.Manager_Approval_Assignment_Date__c != null &&
                            objCaseSummary.Manager_Id__c == Userinfo.getuserid()){
                            wrapperObj.approvalAssignmentDate = objCaseSummary.Manager_Approval_Assignment_Date__c;
                          }else if( objCaseSummary.Director_Approval_Assignment_Date__c  != null &&
                            objCaseSummary.Director_Id__c == Userinfo.getuserid()){
                            wrapperObj.approvalAssignmentDate = objCaseSummary.Director_Approval_Assignment_Date__c;
                          }else if( objCaseSummary.HOD_Approval_Assignment_Date__c != null &&
                            objCaseSummary.HOD_Id__c == Userinfo.getuserid()){
                            wrapperObj.approvalAssignmentDate = objCaseSummary.HOD_Approval_Assignment_Date__c;
                          }else if( objCaseSummary.Committee_Approval_Assignment_Date__c != null &&
                            objCaseSummary.Committee_Id__c == Userinfo.getuserid()){
                            wrapperObj.approvalAssignmentDate = objCaseSummary.Committee_Approval_Assignment_Date__c;
                        }
                        wrapperObj.caseStatus = objCaseSummary.Case__r.Status;
                        if(objCaseSummary.Case__r.Status == 'Submitted' && objCaseSummary.Case__r.Approving_User_Name__c != null ) {
                            wrapperObj.pendingWith = objCaseSummary.Case__r.Approving_User_Name__c; 
                        }
                        if( objCaseSummary.Manager_Id__c == Userinfo.getuserid() &&
                        objCaseSummary.Manager_Approval_Date__c != null){
                            wrapperObj.approvalDate = objCaseSummary.Manager_Approval_Date__c;
                          }else if( objCaseSummary.Director_Id__c == Userinfo.getuserid() &&
                          objCaseSummary.Director_Approval_Date__c != null){
                            wrapperObj.approvalDate = objCaseSummary.Director_Approval_Date__c;
                          }else if( objCaseSummary.HOD_Id__c == Userinfo.getuserid() &&
                          objCaseSummary.HOD_Approval_Date__c != null){
                            wrapperObj.approvalDate = objCaseSummary.HOD_Approval_Date__c;
                          }else if( objCaseSummary.Committee_Id__c == Userinfo.getuserid() &&
                          objCaseSummary.Committee_Approval_Date__c != null){
                            wrapperObj.approvalDate = objCaseSummary.Committee_Approval_Date__c;
                        }
                        wrapperList.add(wrapperObj);
                        break; 
                }
            }
            
        }
        
        return wrapperList;
    }
    
    /**************************************************************************************
    * Method Description: Remote action method to return number of cases for the current logged in user based on their statuses
    * Input Parameters : status -- based on status tile
                fromDateFilter -- based on no of days filter applied on page
    * Return Type : Integer -- count of Cases with applied date filter and status
    ***************************************************************************************/
    @RemoteAction
    public static Integer getParameterBasedSRCount(String status, String fromDateFilter) {
        return getStatuswiseCSRList(status,fromDateFilter).size();
    }
    
    /**************************************************************************************
    * Method Description: Remote action method to return List of cases for the current logged in user based on their statuses
    * Input Parameters : status -- based on status tile
                fromDateFilter -- based on no of days filter applied on page
    * Return Type : List<CSRListViewWrapper> -- List of warpper class with Case summary request and applied date filter and status
    ***************************************************************************************/
    @RemoteAction
    public static List<CSRListViewWrapper> getParameterBasedSRList(String status, String fromDateFilter) {
        return getStatuswiseCSRList(status,fromDateFilter);
    }
    
    /**************************************************************************************
    * Method Description: Remote action method to return number of all cases for the current logged in user
    * Input Parameters : fromDateFilter -- based on no of days filter applied on page
    * Return Type : Integer -- count of all Cases with applied date filter and status
    ***************************************************************************************/
    @RemoteAction
    public static Integer getTotalSRCount(String fromDateFilter) {
        Integer totalCount = init(fromDateFilter).size() + fetchCaseSummaryRequest(fromDateFilter).keyset().size();
        return totalCount;
    }

    /**************************************************************************************
    * Wrapper Class Description: Wrapper Class to the table structure required on the page 
    ***************************************************************************************/
    public class CSRListViewWrapper  {
        String caseNumber;
        Id caseId; 
        Id caseAccountId;
        String caseAccountName;
        List<CaseSummaryRequest__c> csrList;
        DateTime approvalAssignmentDate; 
        DateTime approvalDate; 
        String caseStatus; 
        String pendingWith; 

        public CSRListViewWrapper() {
            this.approvalDate = null;
            this.pendingWith = '';
        }


    }
    
}