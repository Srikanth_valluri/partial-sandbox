@RestResource(urlMapping='/GetMyUnitBookings/*')
 Global class GetMyUnitBookings
 {
     @HtTPPost
    Global static list<Booking_Unit__c> GetMyUnitBooking(String AccountId)
    {
       list<Booking_Unit__c> bookingUnit = [ SELECT  Id
                                                       ,Name
                                                       ,Unit_Name__c
                                                       ,Handover_Flag__c
                                                       ,Early_Handover__c
                                                       ,Handover_Notice_Sent__c
                                                       ,Registration_ID__c
                                                       ,Inventory__r.Unit__c
                                                       ,Primary_Buyer_s_Email__c
                                                       ,Token_Amount__c
                                                       ,Selling_Price__c
                                                       ,CurrencyISOCode
                                                       ,Construction_RERA__c
                                                       ,Primary_Buyer_s_Name__c
                                                       ,Inventory__r.Property_Name_2__c
                                                       ,Inventory__r.IPMS_Bedrooms__c
                                                       ,Inventory__r.Selling_Price__c
                                                       ,Inventory__r.Price_Per_Sqft__c
                                                       ,Inventory__r.Property_Status__c
                                                       ,Inventory__r.Anticipated_Completion_Date__c
                                                       ,Inventory__r.Property__r.Name
                                                       ,Inventory__r.Property__c
                                                       ,Unit_Details__c
                                                       ,Booking__r.Deal_SR__r.Name
                                               FROM     Booking_Unit__c
                                               WHERE    Booking__r.Account__c =: AccountId
                                               AND      Unit_Name__c !=''];
          return bookingUnit;
                                               
    }
 }