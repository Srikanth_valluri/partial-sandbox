/***************************************************************************************************
* Description - This is the test class is for LeadManagementriggerController to cover code coverage
*
* Version   Date            Author            Description
* 1.0       29/03/18        Monali            Initial Draft
***************************************************************************************************/

@isTest
private class LeadManagementriggerHandlerTest {
    @testSetup
  static void createTestdata() {
    Id RecordTypeIdContact = 
      Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName()
        .get('Deal').getRecordTypeId();


    NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
    SR.recordtypeid=RecordTypeIdContact;
    SR.Eligible_to_Sell_in_Dubai__c = true;
    SR.Agency_Type__c = 'Individual';
    SR.ID_Type__c = 'Passport';
    // SR.Agency__c = a.id;
    SR.Agency_Email_2__c = 'test2@gmail.com';
    SR.Alternate_Agency_Email_2__c = 'test1@gmail.com';
    SR.Country_of_Sale__c = 'UAE;KSA;Lebanon';
    insert SR;


    Booking__c objBooking = new Booking__c();
    objBooking.Deal_SR__c = sr.Id;
    insert objBooking;
    List<Booking_Unit__c> lstBUs = new List<Booking_Unit__c>();
    for(Integer i=0; i<5; i++) {
      lstBUs.add(new Booking_Unit__c(Booking__c = objBooking.ID));
    }
    insert lstBUs;


    Id agenTeamRT = 
      Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Agent Team')
        .getRecordTypeId();


    Inquiry__c inquiry = InitialiseTestData.getInquiryDetails('Agent Team',455);
    inquiry.Inquiry_Source__c = 'Prospecting';
    insert inquiry;


    Buyer__c buyerObj = new Buyer__c();
    buyerObj.Inquiry__c = inquiry.Id;
    buyerObj.Booking__c = objBooking.ID;
    insert buyerObj;


    Inquiry__c newInquiry = InitialiseTestData.getInquiryDetails('Agent Team',855);
    newInquiry.Inquiry_Source__c = 'Customer Referral';
    insert newInquiry;
  }

  // Select Inquiry and tag on buyer
    static testMethod void testMethod1() {
      Buyer__c buyer = [Select Id, Inquiry__c From Buyer__c Limit 1];
      Inquiry__c newInquiry = [Select Id 
                                 From Inquiry__c 
                                Where Inquiry_Source__c = 'Customer Referral' Limit 1];

        list<Lead_Admin_Console__mdt> lstRules = [ SELECT Inquiry_Source__c,
                                            Action__c,
                                            Action_Code__c,
                                            Buyer_Details_Same__c,
                                            Campaign_Name__c,
                                            PC_Part_of_Active_RoadShow__c,
                                            Related_Campaign_on_Booking_Unit__c,
                                            Related_Inquiry_Owner_Same__c,
                                            Type__c
                                       FROM Lead_Admin_Console__mdt
                                      WHERE Action_Code__c = 'SEL_INQ'];

    Lead_Management__c leadObj = new Lead_Management__c();
        leadObj.Buyer__c = buyer.Id;
        leadObj.New_Inquiry__c = newInquiry.Id;
        leadObj.Old_Inquiry__c = buyer.Inquiry__c;
        leadObj.Status__c = 'Submitted';
        leadObj.Lead_Admin_Console_Id__c = lstRules[0].Id;
        leadObj.Reason_for_HOD_Update__c = 'Name Correction';
        leadObj.Update_campaign_on_inquiry__c = TRUE;
        insert leadObj;

        Lead_Management__c leadObj1 = new Lead_Management__c();
        leadObj1.Buyer__c = buyer.Id;
        leadObj1.New_Inquiry__c = newInquiry.Id;
        leadObj1.Old_Inquiry__c = buyer.Inquiry__c;
        leadObj1.Lead_Admin_Console_Id__c = lstRules[0].Id;
        leadObj1.Approval_Status__c = 'Approved By Managing Director';
        leadObj1.Status__c = 'Approved';
        leadObj1.Reason_for_HOD_Update__c = 'Name Correction';
        insert leadObj1;

        List<Lead_Management__c> lmList = new List<Lead_Management__c>();
        lmList.add(leadObj1);

        LeadManagementriggerHandler instance = new LeadManagementriggerHandler();
        instance.UpdateLeadManagement(lmList);

    }

}