public class NewStepApprovalEmailTemplateController {
    
    public Id StepId{get; set;}
    public Integer iListSize {get; set;}
    public String ManagerEmail{get; set;}
    public String ManagerType{get; set;}
    
    public NewStepApprovalEmailTemplateController(){
        iListSize = 0;
        ManagerEmail = '';
    }
    
    public New_Step__c step{
        get{
            if(StepId != null){
                if(step != null){
                    return step;
                }else{
                    for(New_Step__c stp :[SELECT Name, Comments__c, Step_No__c, Step_Status__c, Step_Type__c, Service_Request__c, Manager_Email__c,
                                          Service_Request__r.Total_Booking_Amount__c, Service_Request__r.OwnerId, Service_Request__r.Owner.Name,
                                          Service_Request__r.List_of_Units__c, Service_Request__r.NSIBPM__Record_Type_Name__c, HOS_Email__c,
                                          Service_Request__r.Name, Service_Request__r.Agency_Name__c, Service_Request__r.Agency_Type__c, 
                                          Service_Request__r.First_Name__c, Service_Request__r.Last_Name__c, Service_Request__r.Agency__r.Name
                                          FROM New_Step__c 
                                          WHERE Id =: StepId])
                    {
                        step = stp;
                    }
                    
                    system.debug(step);
                    
                    if(ManagerType == 'Manager' && step.Manager_Email__c != null){
                        ManagerEmail = step.Manager_Email__c;
                    }else if(ManagerType == 'HOS' && step.HOS_Email__c != null){
                        ManagerEmail = step.HOS_Email__c;
                    }else if(ManagerType == 'GM'){
                        for(User u : [SELECT Name, Profile.Name, UserRole.Name, Email FROM User WHERE UserRole.Name = 'General Manager' LIMIT 1]){
                            ManagerEmail = u.Email;
                        }
                    }
                    system.debug(ManagerEmail);
                    return step;
                    
                }
            }else{
                return new New_Step__c();
            }
        }
        set;
    }
    
    public User managerUser{
        get{
            if(ManagerEmail != null && ManagerEmail != ''){
                if(managerUser != null){
                    return managerUser;
                }else{
                    for(User u : [SELECT Id, Name, FirstName, LastName,LastModifiedDate FROM User WHERE Email =: ManagerEmail ORDER BY LastModifiedDate DESC LIMIT 1]){
                        managerUser = u;
                    }
                    system.debug(managerUser);
                    return managerUser;
                }
            }else{
                return new User();
            }
        }
        set;
    }
}