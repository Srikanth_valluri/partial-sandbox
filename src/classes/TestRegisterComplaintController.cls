@isTest
public class TestRegisterComplaintController {
    public static testmethod void testCreateFMCase(){
        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;
        
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
        
        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;
        
        PageReference myVfPage = Page.FMComplaintProcessPage;
        
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Service_Charge_Enquiry');
        
        RegisterComplaintController obj=new RegisterComplaintController();
        obj.objFMCase=fmCaseObj;
        obj.createRequestForComplaint();
    }
    
    public static testmethod void submitFMCase(){
        Id profileId = [select id from profile where name = 'System Administrator'].id;
        User u=TestDataFactoryFM.createUser(profileId);
        insert u;
        
        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;
       
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;
        
        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;
        
        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;
        
        FM_User__c fmUser=TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;
        
        PageReference myVfPage = Page.FMComplaintProcessPage;
        
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Service_Charge_Enquiry';
        insert fmCaseObj;
        
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Service_Charge_Enquiry');
        
        RegisterComplaintController obj=new RegisterComplaintController();
        obj.objFMCase.id=fmCaseObj.id;
        
        test.startTest();
        obj.submitRequestForComplaint();
        obj.returnBackToCasePage();
        test.stopTest();
    }
    
    public static testmethod void testUploadDocument(){
        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;
        
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
        
        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;
        
        PageReference myVfPage = Page.FMComplaintProcessPage;
        
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Service_Charge_Enquiry';
        insert fmCaseObj;
      
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Service_Charge_Enquiry');
        
        Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
        
        RegisterComplaintController obj=new RegisterComplaintController();
        obj.strDocumentBody='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        obj.strDocumentName='Test_Document_Work_Permit.htm';
        obj.createCaseShowUploadDoc();
        
        test.startTest();
        obj.uploadDocument();
        test.stopTest();
    }  

}