@isTest
public class FetchAOPTCasesForCallingListCntrlTest {
    @testSetup
    static void allTheDataForThisTestClass(){
       List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;        
        Id caseRecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('AOPT').RecordTypeId;
        Id caseBCRecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Bounced Cheque SR').RecordTypeId;
        Id bouncedChequeRTId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Bounced Cheque').RecordTypeId;
        Account accObj = new Account(Name = 'Miss. Madina Alieva');
        insert accObj;
        
        List<Case>caseLst = new List<Case>();
        caseLst.add( new Case(AccountId = accObj.id,RecordTypeId = caseRecordTypeId,Status = 'New'));
        caseLst.add( new Case(AccountId = accObj.id,RecordTypeId = caseRecordTypeId,Status = 'New'));
        caseLst.add( new Case(AccountId = accObj.id,RecordTypeId = caseRecordTypeId,Status = 'New'));
        insert caseLst;
        
        Calling_List__c callObj = new Calling_List__c(Account__c = accObj.Id,Registration_ID__c = '12851');
        insert callObj;
        
        Case bouncedChequeCaseObj = new Case(AccountId = accObj.Id, RecordTypeId = caseBCRecordTypeId, Status = 'New');
        insert bouncedChequeCaseObj;
        
        Calling_List__c bouncedCallObj = new Calling_List__c(Account__c = accObj.Id,Registration_ID__c = '84357',
                                             RecordTypeId =bouncedChequeRTId, Case__c = bouncedChequeCaseObj.Id);
        insert bouncedCallObj;
        
    }
    public static testMethod void fetchCasesTest(){
        Calling_List__c callObjInst = [SELECT Id,
                                              Account__c,
                                              Registration_ID__c
                                         FROM Calling_List__c
                                         LIMIT 1];
        System.debug('inside test class:callObjInst::'+callObjInst);
        System.assertNotEquals(null,callObjInst.Id);
        Test.startTest();
            PageReference aoptPage = Page.FetchAOPTCasesForCallingListPage;
            Test.setCurrentPage(aoptPage);
            aoptPage.getParameters().put('Id',String.valueOf(callObjInst.Id));
            ApexPages.StandardController sc = new ApexPages.StandardController(callObjInst);
            FetchAOPTCasesForCallingListController controllerObj = new FetchAOPTCasesForCallingListController(sc);
        Test.stopTest();
        //controllerObj.fetchCases();
    }
    public static testMethod void displayBouncedChequeCasesTest(){
        Id bouncedChequeRTId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Bounced Cheque').RecordTypeId;
        Calling_List__c callObjInst = [SELECT Id,
                                              Account__c,
                                              Registration_ID__c
                                         FROM Calling_List__c
                                         WHERE Registration_ID__c = '84357'
                                         LIMIT 1];
        System.debug('inside test class:callObjInst::'+callObjInst);
        System.assertNotEquals(null,callObjInst.Id);
         Test.startTest();
            PageReference aoptPage = Page.FetchAOPTCasesForCallingListPage;
            Test.setCurrentPage(aoptPage);
            aoptPage.getParameters().put('Id',String.valueOf(callObjInst.Id));
            ApexPages.StandardController sc = new ApexPages.StandardController(callObjInst);
            FetchAOPTCasesForCallingListController controllerObj = new FetchAOPTCasesForCallingListController(sc);
        Test.stopTest();
    }
}