public class Damac_FAServices {
    
    
    @Future (Callout = TRUE)
    public static void doCallout (ID srId) {
        IPMS_Integration_Settings__mdt data = new IPMS_Integration_Settings__mdt ();
        data = [SELECT Endpoint_URL__c, UserName__c, Password__c FROM IPMS_Integration_Settings__mdt WHERE DeveloperName = 'F_A_Document_Service' LIMIT 1];
        
        HTTPRequest req = new HTTPRequest ();
        req.setMethod ('POST');
        req.setHeader ('Content-Type', 'application/json');
        req.setEndpoint (data.Endpoint_URL__c);
        req.setTimeOut (120000);
        
        Blob headerValue = Blob.valueOf(data.UserName__c+ ':' + data.password__c);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);        
        req.setHeader('Authorization', authorizationHeader);
        
        HTTP http = new HTTP ();
        
        String reqBody = '';
        
        
        String innerJSON = '';
        
        for (Booking_Unit__c unit :[SELECT Registration_ID__c, No_of_Documents__c, Furniture__c, (Select Sys_doc_id__c FROM Unit_Documents__r)
                                         FROM Booking_Unit__c 
                                        WHERE Furniture__c = TRUE 
                                        AND Booking__r.Deal_sr__c =: srId])
        {
            String furniture = unit.Furniture__c ? 'Y' : 'N';
            String fileUrl = '';
            for (Unit_Documents__c udoc :unit.Unit_Documents__r) {
                fileUrl += udoc.Sys_doc_id__c+',';
            }
            fileUrl = fileUrl.removeEnd (',');
            
            String noOFPages = unit.No_of_Documents__c != null ? String.valueOf (unit.No_of_Documents__c) : '';
            
            innerJSON += '{"P_REGISTRATION_ID" : "'+unit.Registration_ID__c+'", "P_FILE_URL" : "'+fileURL+'", "P_NO_OF_PAGES" : "'+noOFPages+'", "P_SPECIAL_CASE" : "'+furniture+'", "P_UNIT_CODE" :""},';
            
                      
        }
        innerJSON = ' {"P_INPUT_TBL": {"INPUT_REC_TYPE": ['+innerJSON .removeEnd (',')+']}}';
        
        
        reqBody = '{"Special_Case_Registration": {"InputParameters":'+innerJSON +'}}';
        System.debug (reqBody);
        
        req.setBody (reqBody);
        
        HTTPResponse res = new HTTPResponse ();
        try {
            if (!Test.isRunningTest ())
                res = http.send (req);
            System.debug (res.getBody ());
            createLog(srId, reqBody, 'F & A Request');
            createLog(srId, res.getBody (), 'F & A Response');
            
        } catch (Exception e) {
            System.debug (e.getMessage ());
            createLog(srId, reqBody, 'F & A Request');
            createLog(srId, e.getMessage (), 'F & A Response');
        }
        
    }
    
    public static void createLog(string parentId, String errorMessage, String type){
        log__c log = new log__c();
        log.Service_Request__c = parentId;
        log.Description__c = errorMessage;
        log.Type__c = type;
        insert log;
    }
    
    
    
   
}