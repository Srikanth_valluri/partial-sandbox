/*-------------------------------------------------------------------------------------------------
Description:
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
v1.0     | 09-12-2018       | Lochana Rajput   | 1. Create task for FM Finance and push to IPMS
----------------------------------------------------------------------------------------------------------------------------
v1.0     | 17-01-2019       | Lochana Rajput   | 1. Create task for Property Finance and push to IPMS
=============================================================================================================================
*/
public with sharing class FundTransferFMFinanceTask {

	@InvocableMethod
	public static void createTaskForFMFinance(List<Task> lstTasksPB) {
		List<Task> lstTasks = new List<Task>();
		Map<Id, List<task>> mapCaseId_tasks = new Map<Id, List<task>>();
		set<Id> setTaskIds = new set<Id>();
		set<Id> setFMCaseIds = new set<Id>();
		system.debug('lstTasksPB*****'+lstTasksPB);
		system.debug('Label*****'+Label.FM_Fund_Transfer_Allocate_Funds_task_subject);
		String keyPrefix = FM_Case__c.sobjecttype.getDescribe().getKeyPrefix();
		for(Task objTask : lstTasksPB) {
			system.debug('setFMCaseIds*****'+objTask.Subject);
			system.debug('setFMCaseIds*****'+string.valueOf(objTask.WhatId).left(3).equalsIgnoreCase(keyPrefix));
			if(string.valueOf(objTask.WhatId).left(3).equalsIgnoreCase(keyPrefix)
			&& objTask.Subject == Label.FM_Fund_Transfer_Allocate_Funds_task_subject)
			setFMCaseIds.add(objTask.WhatId);
		}
		system.debug('setFMCaseIds*****'+setFMCaseIds);
		for(FM_Case__c objFMCase : [SELECT Id, Type_of_Fund_Transfer__c
									FROM FM_Case__c
									WHERE Id IN: setFMCaseIds
									AND Type_of_Fund_Transfer__c = 'To property payment']) {
			Task objTask = new Task();
			objTask.whatId = objFMCase.Id;
			objTask.Subject = Label.FM_Fund_Transfer_Allocate_Funds_task_subject;
			objTask.Status = 'Not Started';
			objTask.Priority = 'Normal';
			objTask.Process_Name__c = 'FM Fund Transfer';
			objTask.ActivityDate = Date.today().addDays(2);
			objTask.Assigned_User__c = 'Property Finance';
			lstTasks.add(objTask);
		}
		insert lstTasks;
		system.debug('lstTasks*****'+lstTasks);
		setTaskIds = new Map<Id, Task>(lstTasks).keySet();
		if(setTaskIds.size() > 0 && setFMCaseIds.size() > 0) {
			createTaskinIPMS(setTaskIds, setFMCaseIds);
		}

	}

	//Below method does task mapping for IPMS task, and pushes it to IPMS
	@future(callout=true)
	public static void createTaskinIPMS(Set<Id> setTaskIds,Set<Id> setFMCaseIds) {

		list<Error_Log__c> listErrors = new list<Error_Log__c>();
		map<Id,FM_Case__c> mapId_FmCase = new map<Id,FM_Case__c>();

		for(FM_Case__c obj : [SELECT Id,
									 Name,
									 Fund_Transfer_Amount__c,
									 Fund_Transfer_To_Unit__c,
									 Fund_Transfer_To_Unit__r.Registration_ID__c,
									 Type_of_Fund_Transfer__c,
									 Fund_Transfer_From_Unit__c,
									 Fund_Transfer_To_Unit__r.Unit_Name__c,
									 Fund_Transfer_From_Unit__r.Unit_Name__c,
									 Status__c,
									 CreatedDate,
									 Additional_Doc_File_URL__c,
									 Account__r.Party_Id__c,
									 Fund_Transfer_From_Unit__r.Registration_ID__c
							    FROM FM_Case__c
							   WHERE ID IN:setFMCaseIds]) {
			mapId_FmCase.put(obj.Id, obj);
		}
		system.debug('mapId_FmCase*****'+mapId_FmCase);
		map<Id,Task> mapId_Task = new map<Id,Task>(FM_Utility.fetchTaskDetails(setTaskIds));
		system.debug('mapId_Task*****'+mapId_Task);
		list<Task> lstTaskToUpdate = new list<Task>();
		GenerateIpmsBeans beanObj = new GenerateIpmsBeans();
		for(Task objT : mapId_Task.values()) {
			list<TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5> regTerms = new list<TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5>();
			//**********************
			// create HEADER bean
            TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objHeaderBean = new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
            objHeaderBean.PARAM_ID = mapId_FmCase.get(objT.WhatId).Name;
            objHeaderBean.ATTRIBUTE1 = 'HEADER';
            objHeaderBean.ATTRIBUTE2 = objT.Process_Name__c;
            objHeaderBean.ATTRIBUTE3 = mapId_FmCase.get(objT.WhatId).Status__c;
            objHeaderBean.ATTRIBUTE4 = objT.Owner.Name;
            objHeaderBean.ATTRIBUTE5 = mapId_FmCase.get(objT.WhatId).Account__r.Party_Id__c;
            objHeaderBean.ATTRIBUTE6 = mapId_FmCase.get(objT.WhatId).Fund_Transfer_From_Unit__r.Registration_ID__c;
            objHeaderBean.ATTRIBUTE7 = String.valueOf(mapId_FmCase.get(objT.WhatId).CreatedDate.format('dd-MMM-yyyy').toUpperCase()); // format this as DD-MON- YYYY
            objHeaderBean.ATTRIBUTE8 = '';
            objHeaderBean.ATTRIBUTE9 = '';
            beanObj.createTaskBean10_24(objHeaderBean,'',objT.WhatId,'','','','','','','','','','','','','');
            beanObj.createTaskBean25_39(objHeaderBean,'','','','','','','','','','','','','','','');
            regTerms.add(objHeaderBean);

			// create TASK bean
            TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objTaskBean = new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
            objTaskBean.PARAM_ID = mapId_FmCase.get(objT.WhatId).Name;
            objTaskBean.ATTRIBUTE1 = 'TASK';
            objTaskBean.ATTRIBUTE2 = objT.Subject;
            objTaskBean.ATTRIBUTE3 = objT.Status;
            System.debug('objT.Subject'+objT.Subject);
            System.debug('ipms_role===='+objT.IPMS_Role__c);
            System.debug('Assigned_user===='+objT.assigned_user__c);
            objTaskBean.ATTRIBUTE4 = objT.IPMS_Role__c;
            objTaskBean.ATTRIBUTE5 = '';
            objTaskBean.ATTRIBUTE6 = '';
            objTaskBean.ATTRIBUTE7 = String.valueOf(objT.CreatedDate.format('dd-MMM-yyyy').toUpperCase()); // format this as DD-MON- YYYY
            objTaskBean.ATTRIBUTE8 = objT.Id;
            Datetime dt = objT.ActivityDate;
            objTaskBean.ATTRIBUTE9 = String.valueOf(dt.format('dd-MMM-yyyy').toUpperCase());
			beanObj.createTaskBean10_24(objTaskBean
                                    , ''
                                    , mapId_FmCase.get(objT.WhatId).Account__r.Party_Id__c
                                    , mapId_FmCase.get(objT.WhatId).Fund_Transfer_From_Unit__r.Registration_ID__c
                                    , mapId_FmCase.get(objT.WhatId).Fund_Transfer_From_Unit__r.Unit_Name__c
                                    , mapId_FmCase.get(objT.WhatId).Fund_Transfer_To_Unit__r.Registration_ID__c
                                    , mapId_FmCase.get(objT.WhatId).Fund_Transfer_To_Unit__r.Unit_Name__c
                                    , string.valueOf(mapId_FmCase.get(objT.WhatId).Fund_Transfer_Amount__c)
                                    , mapId_FmCase.get(objT.WhatId).Type_of_Fund_Transfer__c
                                    , ''
                                    , ''
                                    , ''
                                    , ''
                                    , ''
                                    , ''
                                    , '');
			beanObj.createTaskBean25_39(objTaskBean,'','','','','','','','','','','','','','','');
            beanObj.createTaskBean41_50(objTaskBean,'','','',''
                                , mapId_FmCase.get(objT.WhatId).Additional_Doc_File_URL__c
                                ,'','','','','');
            System.debug('objTaskBean======'+objTaskBean);
            System.debug('beanObj======'+beanObj);
            System.debug('regTerms======'+regTerms);
			regTerms.add(objTaskBean);

			TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objUnitBean = new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
            objUnitBean.PARAM_ID = mapId_FmCase.get(objT.WhatId).Name;
            objUnitBean.ATTRIBUTE1 = 'UNITS';
            objUnitBean.ATTRIBUTE2 = objT.Process_Name__c;
            objUnitBean.ATTRIBUTE3 = '';
            objUnitBean.ATTRIBUTE4 = '';
            objUnitBean.ATTRIBUTE5 = '';
            objUnitBean.ATTRIBUTE6 = mapId_FmCase.get(objT.WhatId).Fund_Transfer_From_Unit__r.Registration_ID__c;
            objUnitBean.ATTRIBUTE7 = '';
            objUnitBean.ATTRIBUTE8 = '';
            objUnitBean.ATTRIBUTE9 = '';
            beanObj.createTaskBean10_24(objUnitBean,'','','','','','','','','','','','','','','');
            beanObj.createTaskBean25_39(objUnitBean,'','','','','','','','','','','','','','','');
            regTerms.add(objUnitBean);

			//Uploading task to IPMS
			FM_PushTaskToIPMS objClass = new FM_PushTaskToIPMS();
			String response = objClass.pushTaskToIpms(regTerms);
			system.debug('response*****'+response);

			if(response.equalsIgnoreCase('Success')){

				objT.Pushed_To_IPMS__c = true;
				lstTaskToUpdate.add(objT);

			}
			else {

				Error_Log__c objErr = FM_PushTaskToIPMS.createErrorLogRecord(mapId_FmCase.get(objT.WhatId).Account__c
										,mapId_FmCase.get(objT.WhatId).Fund_Transfer_From_Unit__c
										,objT.WhatId);
				objErr.Error_Details__c = response;
				objErr.Process_Name__c = objT.Process_Name__c;
				System.debug('objErr========='+objErr);
				listErrors.add(objErr);

			}
		}//for

		System.debug('listErrors======='+listErrors);
		insert listErrors;
		System.debug('lstTaskToUpdate-------'+lstTaskToUpdate);
		update lstTaskToUpdate;

	}

}