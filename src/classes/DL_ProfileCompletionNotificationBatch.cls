/**********************************************************************************************************************
Description: Batch to send push notification to 'DAMAC Living App Users' regarding profile completion.
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By   | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     | 02-11-2020        | Subin Antony (QBurst)   | Initial Draft
***********************************************************************************************************************/
global class DL_ProfileCompletionNotificationBatch implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {
    @testVisible
    private static final String push_notification_sandbox_baseurl = 'https://ptctest.damacgroup.com';
    @testVisible
    private static final String push_notification_prod_baseurl = 'https://ptc.damacgroup.com';
    @testVisible
    private static final String push_notification_endpoint = '/hellodamac/api/v1/notifications/trigger-push';
    @testVisible
    private static final String api_token = 'c13d9a4e2be67d853d7428f5a9a952d6';
    
    private static ID ownerUsrProfileID;
    static {
       ownerUsrProfileID = [SELECT Id FROM Profile WHERE Name = 'Customer Community Login User(Use this)' LIMIT 1].id; 
    }
    
    Map<ID, Account> partyWithCompletedProfile_Map;
    Map<ID, Account> partyWithIncompleteProfile_Map;
    
    Integer runningCount = 0;
    String query = 'SELECT  Id, Name, Status__c, Salutation, SLA__c, Party_ID__c, Party_Type__c, Customer_Type__c, ' + 
        'Organisation_Name__c, Industry, Designation__pc, Primary_Language__c, Date_of_Birth__pc, Date_Of_Birth__c, ' + 
        'Passport_Number__pc, Passport_Expiry_Date__pc, Passport_Number__c, Passport_Expiry_Date__c, ' + 
        'CR_Number__c, CR_Registration_Expiry_Date__c, Nationality__c, Nationality__pc, ' + 
        'Address_Line_1__pc, Address_Line_2__pc, Address_Line_3__pc, Address_Line_4__pc, City__pc, Country__pc, ' + 
        'Address_Line_1__c, Address_Line_2__c, Address_Line_3__c, Address_Line_4__c, City__c, Country__c, ' + 
        'PersonMailingStreet, PersonMailingPostalCode, PersonMailingCity, PersonMailingState, PersonMailingCountry, ' + 
        'PersonEmail, Phone, WA_Number__c, WhatsApp_Number__c, ' + 
        'Email__pc, Email_2__pc, Email_3__pc, Email_4__pc, Email_5__pc, ' + 
        'Email__c, Email_1__c, Email_2__c, Email_3__c, ' + 
        'Mobile_Country_Code__c, Mobile__c, Mobile_Country_Code__pc, Mobile_Phone_Encrypt__pc, ' + 
        'Mobile_Country_Code_2__c, Mobile_Phone_2__c, Mobile_Country_Code_2__pc, Mobile_Phone_Encrypt_2__pc, ' + 
        'Mobile_Country_Code_3__c, Mobile_Phone_3__c, Mobile_Country_Code_3__pc, Mobile_Phone_Encrypt_3__pc, ' + 
        'Mobile_Country_Code_4__c, Mobile_Phone_4__c, Mobile_Country_Code_4__pc, Mobile_Phone_Encrypt_4__pc, ' + 
        'Mobile_Country_Code_5__c, Mobile_Phone_5__c, Mobile_Country_Code_5__pc, Mobile_Phone_Encrypt_5__pc, ' + 
        '(SELECT id, Name, Attachment_URL__c FROM Case_Attachments__r WHERE Name LIKE \'%Customer Passport%\' ORDER BY CreatedDate DESC LIMIT 1) ' +
        'FROM Account WHERE Party_Type__c = \'PERSON\'';
    
    global DL_ProfileCompletionNotificationBatch() {
        query += ' AND Is_DAMAC_Living_App_user__c = true AND id IN (SELECT AccountId FROM User WHERE ProfileId = \'' + ownerUsrProfileID +'\' AND AccountId != null AND IsActive = TRUE)';
    }
    
    global DL_ProfileCompletionNotificationBatch(List<String> accountIds) { /* for selectively running batch */
        if(NULL != accountIds && accountIds.size() > 0) {
            String seperator = '';
            
            query += ' AND id IN (';
            for(String accId : accountIds) {
                query += seperator + '\'' + accid + '\'';
                seperator = ',';
            }
            query += ')';
        } 
    }
    
    global Database.QueryLocator start( Database.BatchableContext bc) {
        System.debug('PROF_COMPL_BATCH_QUERY: - - - - - - - - - - - - - - - - - - : ' + query ); // formed in constructor
        return Database.getQueryLocator(query);
    }
    
    global void execute( Database.BatchableContext bc, List<Account> customers) {
        partyWithCompletedProfile_Map = new Map<ID, Account>();
        partyWithIncompleteProfile_Map = new Map<ID, Account>();
        runningCount += (NULL != customers ? customers.size() : 0);
        
        System.debug('PROF_COMPL_BATCH_EXEC: - - - - - - - - - - - - - - - - - - : runningCount : ' + runningCount);
        
        for(Account customerAcc : customers) {
            Boolean profileCompleted = false;
            String passportDocURL = customerAcc.Case_Attachments__r.size() > 0 ? customerAcc.Case_Attachments__r[0].Attachment_URL__c : null; /* ??? */
            
            if(customerAcc.Party_Type__c == 'PERSON') {
                profileCompleted = 
                    (String.isNotBlank(customerAcc.Passport_Number__pc) && 
                     NULL != customerAcc.Passport_Expiry_Date__pc && 
                     String.isNotBlank(passportDocURL) && 
                     (String.isNotBlank(customerAcc.Address_Line_1__pc) || String.isNotBlank(customerAcc.Address_Line_2__pc) || 
                      String.isNotBlank(customerAcc.Address_Line_3__pc) || String.isNotBlank(customerAcc.Address_Line_4__pc)) && 
                     String.isNotBlank(customerAcc.City__pc) && 
                     String.isNotBlank(customerAcc.Country__pc) && 
                     String.isNotBlank(customerAcc.Mobile_Phone_Encrypt__pc) && 
                     String.isNotBlank(customerAcc.Email__pc) && 
                     (String.isNotBlank(customerAcc.Mobile_Phone_Encrypt_2__pc) || String.isNotBlank(customerAcc.Mobile_Phone_Encrypt_3__pc) || 
                      String.isNotBlank(customerAcc.Mobile_Phone_Encrypt_4__pc) || String.isNotBlank(customerAcc.Mobile_Phone_Encrypt_5__pc)) && 
                     (String.isNotBlank(customerAcc.Email_2__pc) || String.isNotBlank(customerAcc.Email_3__pc) || 
                      String.isNotBlank(customerAcc.Email_4__pc) || String.isNotBlank(customerAcc.Email_5__pc)) && 
                     String.isNotBlank(customerAcc.WhatsApp_Number__c) );
            }
            else {
                profileCompleted = true; /* no non-person accounts are queried. Even if it comes, notification should not go. */
                /* profileCompleted = 
                    (String.isNotBlank(customerAcc.Passport_Number__c) && 
                     NULL != customerAcc.Passport_Expiry_Date__c && 
                     String.isNotBlank(passportDocURL) && 
                     (String.isNotBlank(customerAcc.Address_Line_1__c) || String.isNotBlank(customerAcc.Address_Line_2__c) || 
                      String.isNotBlank(customerAcc.Address_Line_3__c) || String.isNotBlank(customerAcc.Address_Line_4__c)) && 
                     String.isNotBlank(customerAcc.City__c) && 
                     String.isNotBlank(customerAcc.Country__c) && 
                     String.isNotBlank(customerAcc.Mobile__c) && 
                     String.isNotBlank(customerAcc.Email__c) && 
                     (String.isNotBlank(customerAcc.Mobile_Phone_2__c) || String.isNotBlank(customerAcc.Mobile_Phone_3__c) || 
                      String.isNotBlank(customerAcc.Mobile_Phone_4__c) || String.isNotBlank(customerAcc.Mobile_Phone_5__c)) && 
                     (String.isNotBlank(customerAcc.Email_1__c) || String.isNotBlank(customerAcc.Email_2__c) || 
                      String.isNotBlank(customerAcc.Email_3__c)) && 
                     String.isNotBlank(customerAcc.WhatsApp_Number__c) ); */
            }
            
            if(profileCompleted) { /* Party_Type__c == 'PERSON' only */
                partyWithCompletedProfile_Map.put(customerAcc.id, customerAcc);
            } else { /* Party_Type__c == 'PERSON' only */
                partyWithIncompleteProfile_Map.put(customerAcc.id, customerAcc);
            }
            
        	System.debug('PROF_COMPL_BATCH_EXEC: - - - - - - - - - - - - - - - - - - : Acc : ' + customerAcc);
        }
        
        // Filter out owner accounts only
        // List<Account> ownerAccountsWithIncompleteProfile = filterOutOwnersAndTenants(partyWithIncompleteProfile_Map).owners;
        
        // Send push notification to complete profile for each 'ownerAccountsWithIncompleteProfile'
        System.debug('PROF_COMPL_BATCH_EXEC: - - - - - - - - - - - - - - - - - - : OWN_INCOMPL_LST : size : ' + partyWithIncompleteProfile_Map.values().size());
        pushProfileCompletionInAppNotificationForOwners(partyWithIncompleteProfile_Map.values());
    }
    
    global void finish( Database.BatchableContext BC ) {
        System.debug( '- - >> Finished Batch to send Push Notification for App users with incomplete profile data.' );
    }
    
    /*
    private static SegregatedCustomerWrap filterOutOwnersAndTenants(Map<ID, Account> customerMap) {
        SegregatedCustomerWrap filteredCustomers = new SegregatedCustomerWrap();
        List<Account> owners = new List<Account>();
        List<Account> tenants = new List<Account>();
        
        Set<ID> custAccIdSet = customerMap.keySet();
        List<User> lstUser = [SELECT id, Username, FullPhotoUrl, Profile.Name, Contact.AccountId 
                              FROM User WHERE Contact.AccountId IN :custAccIdSet];
        if(NULL != lstUser && !lstUser.isEmpty()) {
            for(User usr : lstUser) {
                String usrProfileName = usr.Profile.Name;
                if(String.isNotBlank(usrProfileName) && usrProfileName.equalsIgnoreCase('Tenant Community Login User')) {
                    tenants.add(customerMap.get(usr.Contact.AccountId));
                } else {
                    owners.add(customerMap.get(usr.Contact.AccountId));
                }
            }
        }
        
        filteredCustomers.owners = owners;
        filteredCustomers.tenants = tenants;
        return filteredCustomers;
    } */
    
    private static void pushProfileCompletionInAppNotificationForOwners(List<Account> accountsToNotify) {
        if(NULL != accountsToNotify && !accountsToNotify.isEmpty()) {
            String title = 'Update your contacts & get a free breakfast';
            String message = 'Please update your contact details in profile where we can contact you ' + 
                'and your complimentary breakfast at the Stage restaurant, Paramount hotel is ready!';
            
            for(Account acc : accountsToNotify) {
                if(NULL != acc.Party_ID__c) {
                    sendPushNotification(acc.id, message, title, Integer.valueOf(acc.Party_ID__c), 'profile-completion', null, null, null, null);
                }
            }
        }
    }

    public static void sendPushNotification(String accountId, String message, String title, Integer partyId, String screenType, String receiptId, Integer regId, String paymentPriority, String imageUrl) {
        String jsonBody = '   {  '  + 
        '       "email": "'+accountId+'",  '  + 
        '       "template_id": 0,  '  + 
        '       "message": "'+message+'",  '  + 
        '       "title": "'+title+'",  '  + 
        '       "payload": {  '  + 
        '           "account_id": "'+accountId+'",  '  + 
        '           "party_id": '+partyId+',  '  + 
        '           "screen": "'+screenType+'",  '  + 
        '           "receipt_id": "'+receiptId+'",  '  + 
        '           "reg_id": '+regId+',  '  + 
        '           "payment_priority": "'+paymentPriority+'"  '  + 
        '       },  '  + 
        '       "image_url": "'+imageUrl+'",  '  + 
        '       "source_application_id": 9,  '  + 
        '       "dest_application_id": 2  '  + 
        '  }  ' ; 

        Boolean isSandbox = [SELECT IsSandbox FROM Organization].IsSandbox;
        System.debug('IsSandbox in push notification is: ' + isSandbox);

        String endpoint = isSandbox ? push_notification_sandbox_baseurl : push_notification_prod_baseurl;
        endpoint += push_notification_endpoint;
        System.debug('endpoint: ' + endpoint);
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpoint);
        request.setMethod('POST');
        request.setBody(jsonBody);
        request.setHeader('API-TOKEN',api_token);
        request.setHeader('Content-Type', 'application/json');
        HttpResponse response;
        if(Test.isRunningTest()) {
            response = new HttpResponse();
            response.setStatus('OK');
            response.setStatusCode(200);
            response.setBody('{"meta_data":{"title":"OK","status_code":1,"message":"Successfully pushed message","developer_message":null}}');
        } else {
            response = http.send(request);
        }
        
        System.debug('requestBody:: ' + request.getBody());
        System.debug('response:: ' + response);
        System.debug('response body :: ' + response.getBody());
    }
    
    /*
    public class SegregatedCustomerWrap {
        public List<Account> owners;
        public List<Account> tenants;
    } */
}