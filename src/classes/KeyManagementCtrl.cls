/********************************************************************************************************************************
 * Description : key Management controller
 *===============================================================================================================================
 * Ver      Date-DD/MM/YYYY     Author              Modification
 *===============================================================================================================================
 * 1.0      06/05/2020          Aishwarya Todkar    Initial Draft
*********************************************************************************************************************************/

public without sharing class KeyManagementCtrl {
    
    public id keyOwner{ get; set; }
    public String selectedTeam { get; set; }
    public List<SelectOption> listTeams { get; set; }
    public String phone { get; set; }
    public String emailId { get; set; }
    public DateTime issueDateTime { get; set; }
    public String selectedIssuanceReason { get; set; }
    public List<SelectOption> listIssuanceReason { get; set; }
    public Date returnByDate { get; set; }
    public String returnRemark { get; set; }
    public String issuanceRemark { get; set; }
    public String personName { get; set; }
    public String ownerType { get; set; }
    public String ownerSearchKey { get; set; }
    public List<User> owners { get; set;}
    public List<Account> accounts { get; set;}
    public List<Key_Inventory__c> keyInventory { get; set; }
    public String addedKeys { get; set; }
    public String transactionType { get; set; }
    public List<SelectOption> listKeyInventoryDisplayFields { get; set; }
    public List<SelectOption> listKeyIssuanceDisplayFields { get; set; }
    public Set<String> setFixedFields;
    public String selectedDisplayFields { get; set; }
    public String selectedKeyIssuanceDisplayFields { get; set; }    
    public String selectedCategoryKey { get; set; }
    public List<SelectOption> listCategoryKey { get; set; }
    public String selectedCommonArea { get; set; }
    public List<SelectOption> listCommonArea { get; set; }
    public String selectedKeyArea { get; set; }
    public List<SelectOption> listKeyArea { get; set; }
    public String selectedIssueStatus { get; set; }
    public List<SelectOption> listIssueStatus { get; set; }
    public String ownerAddress { get; set; }
    public String ownerEmail { get; set; }
    public String ownerPhone { get; set; }
    public String selectedOwnerType { get; set; }
    public List<SelectOption> listOwnerType { get; set; }
    public String selectedStatus { get; set; }
    public List<SelectOption> listStatus { get; set; }
    public String selectedSubStatus { get; set; }
    public List<SelectOption> listSubStatus { get; set; }
    public String cardNumber { get; set; }
    public String selectedKeyType { get; set; }
    public List<SelectOption> listKeyType { get; set; }
    public Key_Inventory__c keyInvent { get; set; }
    
    public List<DisplayFieldsWrapper> listKeyInvWrapper { get; set;}
    public List<DisplayFieldsWrapper> listKeyIssWrapper { get; set;}
    public String strKeyInventDisplayFields {get; set; }
    public String strKeyIssuDisplayFields {get; set; }
    //public List<sObject> selectList {get; set; }
    
    public List<String> listSelectedFields { get; set;}
    public String keyInventoryQuery { get; set; }
    public String keyIssuanceQuery { get; set; }
    
    public KeyManagementCtrl() {
        ownerType = '';
        ownerSearchKey = '';
        owners = new List<User>();
        accounts = new List<Account>();
        keyInvent = new Key_Inventory__c();
        transactionType = 'Key Issuance';
        listKeyInventoryDisplayFields = new List<SelectOption>();
        listKeyIssuanceDisplayFields = new List<SelectOption>();
        listKeyInvWrapper = new List<DisplayFieldsWrapper>();
        listKeyIssWrapper = new List<DisplayFieldsWrapper>();
        listSelectedFields = new List<String>();
        strKeyInventDisplayFields = '';
        strKeyIssuDisplayFields = '';
        /*setFixedFields = new Set<String>{ 'Id'
                                        , 'Property_Name__c'
                                        , 'Unit_Name__c'
                                        , 'Key_Card_Number__c' };*/
        init();
    }

/********************************************************************************************************************************
* Method Name : init
* Description : method for initializations
* Return Type : void
* Parameter(s): None
********************************************************************************************************************************/
    public void init() {
        listTeams = new List<SelectOption>();
        Schema.DescribeFieldResult fieldTeam = Key_Issuance__c.Team__c.getDescribe();
        List<Schema.PicklistEntry> picklist = fieldTeam.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : picklist){
            listTeams.add( new SelectOption( pickListVal.getValue(),pickListVal.getLabel()) );
        }    
        
        listIssuanceReason = new List<SelectOption>();
        Schema.DescribeFieldResult fieldReason = Key_Issuance__c.Reason_For_Issuance__c.getDescribe();
        List<Schema.PicklistEntry> picklistEntry = fieldReason.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : picklistEntry){
            listIssuanceReason.add( new SelectOption( pickListVal.getValue(),pickListVal.getLabel()) );
        }
        
         listCategoryKey = new List<SelectOption>();
        Schema.DescribeFieldResult fieldInventory = Key_Inventory__c.Category_Of_Key__c.getDescribe();
        List<Schema.PicklistEntry> picklistInventory = fieldInventory.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : picklistInventory){
            listCategoryKey.add( new SelectOption( pickListVal.getValue(),pickListVal.getLabel()) );
        }
        
        listCommonArea = new List<SelectOption>();
        Schema.DescribeFieldResult fieldCommonArea = Key_Inventory__c.Common_Area_Type__c.getDescribe();
        List<Schema.PicklistEntry> picklistCommonArea = fieldCommonArea.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : picklistCommonArea){
            listCommonArea.add( new SelectOption( pickListVal.getValue(),pickListVal.getLabel()) );
        }
        
        listKeyArea = new List<SelectOption>();
        Schema.DescribeFieldResult fieldKeyArea = Key_Inventory__c.Key_Area__c.getDescribe();
        List<Schema.PicklistEntry> picklistKeyArea = fieldKeyArea.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : picklistKeyArea){
            listKeyArea.add( new SelectOption( pickListVal.getValue(),pickListVal.getLabel()) );
        }
        
        listIssueStatus = new List<SelectOption>();
        Schema.DescribeFieldResult fieldIssueStatus = Key_Inventory__c.Key_Issue_Status__c.getDescribe();
        List<Schema.PicklistEntry> picklistIssueStatus = fieldIssueStatus.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : picklistIssueStatus){
            listIssueStatus.add( new SelectOption( pickListVal.getValue(),pickListVal.getLabel()) );
        }
        
        listOwnerType = new List<SelectOption>();
        Schema.DescribeFieldResult fieldOwnerType = Key_Inventory__c.Key_Owner_Type__c.getDescribe();
        List<Schema.PicklistEntry> picklistOwnerType = fieldOwnerType.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : picklistOwnerType){
            listOwnerType.add( new SelectOption( pickListVal.getValue(),pickListVal.getLabel()) );
        }
        
        listStatus = new List<SelectOption>();
        Schema.DescribeFieldResult fieldKeyStatus = Key_Inventory__c.Key_Status__c.getDescribe();
        List<Schema.PicklistEntry> picklistKeyStatus = fieldKeyStatus.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : picklistKeyStatus){
            listStatus.add( new SelectOption( pickListVal.getValue(),pickListVal.getLabel()) );
        }
        
        listSubStatus = new List<SelectOption>();
        Schema.DescribeFieldResult fieldSubStatus = Key_Inventory__c.Key_Sub_Status__c.getDescribe();
        List<Schema.PicklistEntry> picklistSubStatus = fieldSubStatus.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : picklistSubStatus){
            listSubStatus.add( new SelectOption( pickListVal.getValue(),pickListVal.getLabel()) );
        }
        
        listKeyType = new List<SelectOption>();
        Schema.DescribeFieldResult fieldTypeKey = Key_Inventory__c.Type_Of_Key__c.getDescribe();
        List<Schema.PicklistEntry> picklistTypeKey = fieldTypeKey.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : picklistTypeKey){
            listKeyType.add( new SelectOption( pickListVal.getValue(),pickListVal.getLabel()) );
        }

        
    }

/********************************************************************************************************************************
* Method Name : searchOwners
* Description : method to search owners
* Return Type : void
* Parameter(s): None
********************************************************************************************************************************/    
    public void searchOwners() {
        owners.clear();
        accounts.clear();
        system.debug('ownerType--'+ownerType);
        if(ownerType == 'User'){
            string searchquery = 'select Id, Name from user where name  like \'%'+ownerSearchKey+'%\' ';
            owners = Database.query( searchquery );
            system.debug('owners--'+owners);
        }
        
        else if(ownerType == 'Account'){
            string searchquery = 'select Id, Name from Account where name like \'%'+ownerSearchKey+'%\' ';
            accounts = Database.query( searchquery );
            system.debug('accounts--'+accounts);
        } 
        
    }

/********************************************************************************************************************************
* Method Name : getKeyInventoryDisplayFields
* Description : method to get Key inventory fields to display
* Return Type : void
* Parameter(s): None
********************************************************************************************************************************/
    public List<SelectOption> getKeyInventoryDisplayFields(){
        setFixedFields = new Set<String>{ 'id'
            , 'property_name__c'
            , 'unit_name__c'
            , 'key_card_number__c' };
        System.debug('transactionType : ' + transactionType);
            
            Schema.SObjectType ObjectSchema = Schema.getGlobalDescribe().get( 'Key_Inventory__c' );
            Map<String, Schema.SObjectField> fieldMap = ObjectSchema.getDescribe().fields.getMap();
            List<String>listFields = new List<String>();
            listFields.addAll( fieldMap.keySet() );
            listFields.sort();
            
            for ( String fieldName: listFields ) {
                
                Schema.DescribeFieldResult fieldDescribe = fieldMap.get(fieldName).getDescribe();
                System.debug('Field : ' + fieldName);
                if( /*fieldDescribe.isUpdateable() && */ fieldDescribe.isAccessible() && !setFixedFields.contains( fieldName ) ) {
                    System.debug('Field Added');
                    
                    listKeyInventoryDisplayFields.add( 
                        new SelectOption( fieldName + ',' + fieldDescribe.getLabel(), fieldDescribe.getLabel() ) );
                }
            }             
        return listKeyInventoryDisplayFields;
    }
    
/********************************************************************************************************************************
* Method Name : getKeyInventoryDisplayFields
* Description : method to get Key inventory fields to display
* Return Type : List<SelectOption>
* Parameter(s): none
********************************************************************************************************************************/    
    public List<SelectOption> getKeyIssuanceDisplayFields(){
        setFixedFields = new Set<String>{ 'id'
            , 'property_name__c'
            , 'unit_name__c'
            , 'key_card_number__c'
            , 'team__c'
            , 'person_name__c'
            , 'reason_for_issuance__c'
            , 'issue_date_time__c'};
        System.debug('transactionType : ' + transactionType);

            
            Schema.SObjectType ObjectSchema = Schema.getGlobalDescribe().get( 'Key_Issuance__c' );
            Map<String, Schema.SObjectField> fieldMap = ObjectSchema.getDescribe().fields.getMap();
            List<String>listFields = new List<String>();
            listFields.addAll( fieldMap.keySet() );
            listFields.sort();
            
            for ( String fieldName: listFields ) {
                
                Schema.DescribeFieldResult fieldDescribe = fieldMap.get(fieldName).getDescribe();
                System.debug('Field : ' + fieldName);
                if( /*fieldDescribe.isUpdateable() && */ fieldDescribe.isAccessible() && !setFixedFields.contains( fieldName ) ) {
                    System.debug('Field Added');
                    listKeyIssuanceDisplayFields.add( new SelectOption( fieldName + ',' + fieldDescribe.getLabel(), fieldDescribe.getLabel()  ) );
                }
            }             
    
     return listKeyIssuanceDisplayFields;
    }

/********************************************************************************************************************************
* Method Name : executeQuery
* Description : method to get Keys
* Return Type : void
* Parameter(s): none
********************************************************************************************************************************/    
    public void executeQuery(){
        //selectList = new List<sObject>();
        keyInventory = new List<Key_Inventory__c>();
        listKeyInvWrapper = new List<DisplayFieldsWrapper>();
        listKeyIssWrapper = new List<DisplayFieldsWrapper>();
        string searchquery = '';
        listKeyIssWrapper.clear();
        listSelectedFields = new List<String>();
        System.debug('keyOwner : ' + keyOwner);
         
        listSelectedFields = keyInventoryQuery.split(',');
        
        if( transactionType.equalsIgnoreCase( 'Key Issuance' ) ) {
            searchquery = 'select Id, Property_Name__c, Unit_Name__c,Key_Card_Number__c ' + keyInventoryQuery 
                        + ' from Key_Inventory__c where ( Key_Owner__c = \'' 
                        + keyOwner + '\' OR Key_Owner_Account__c = \'' + keyOwner + '\')' 
                        + ' AND Key_Issue_Status__c != \'Issued\''
                        + ' LIMIT 9990';

            //if(get-type)
        }
        else if( transactionType.equalsIgnoreCase( 'Key Return' ) ) {
            searchquery = 'select Id, Property_Name__c, Unit_Name__c, Key_Card_Number__c, (select Id,Team__c,' 
                        + 'Person_Name__c, Reason_For_Issuance__c, Issue_Date_Time__c ' 
                        + keyIssuanceQuery + ' from Key_Issuances__r '+ 'order by CreatedDate desc Limit 1) ' 
                        + keyInventoryQuery + ' from Key_Inventory__c where ( Key_Owner__c = \'' 
                        + keyOwner + '\' OR Key_Owner_Account__c = \'' 
                        + keyOwner + '\') ' 
                        + 'AND ( Key_Issue_Status__c != \'With Key Owner\' OR Key_Issue_Status__c = \'Issued\')'
                        + ' LIMIT 5000';
            
            listKeyIssWrapper = ( List<DisplayFieldsWrapper> ) JSON.deserialize( strKeyIssuDisplayFields, List<DisplayFieldsWrapper>.Class);
        }
        else {
           searchquery = 'select Id, Property_Name__c, Unit_Name__c,Key_Card_Number__c,Category_Of_Key__c,Common_Area_Type__c,'
                         + 'Inventory__c,Inventory_Status__c,Key_Area__c,Key_Issue_Status__c,Key_Owner__c,Key_Owner_Account__c,'
                         + 'Key_Owner_Address__c,Key_Owner_Email__c,Key_Owner_Name__c,Key_Owner_Phone__c,Key_Owner_Type__c,Key_Status__c,'
                         + 'Key_Sub_Status__c,Type_Of_Key__c ' + keyInventoryQuery + '  from Key_Inventory__c where ( Key_Owner__c = \'' 
                         + keyOwner + '\' OR Key_Owner_Account__c = \'' + keyOwner + '\')' ;
        }

        //searchquery = searchquery + ' LIMIT 9990';
        System.debug('searchquery : ' + searchquery);

        try {
            keyInventory = Database.query( searchquery );
        }
        catch (Exception ex) {
            apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage() ) );
        }
        
         
        listKeyInvWrapper = ( List<DisplayFieldsWrapper> ) JSON.deserialize( strKeyInventDisplayFields, List<DisplayFieldsWrapper>.Class);
       
    }
    
/********************************************************************************************************************************
* Method Name : submitKeyIssuance
* Description : method to issue all selected keys
* Return Type : void
* Parameter(s): none
********************************************************************************************************************************/
    public void submitKeyIssuance(){
        
        submitKeyIssuanceFuture( addedKeys, selectedTeam, selectedIssuanceReason
                                , emailId, phone, returnByDate, issuanceRemark, personName);
        updatekeyInventory( addedKeys,'Issued');
        
    }
    
/********************************************************************************************************************************
* Method Name : submitKeyIssuance
* Description : method issue all selected keys in async
* Return Type : void
* Parameter(s): none
********************************************************************************************************************************/
    @future
    public static void submitKeyIssuanceFuture(String addedKeys, String selectedTeam
                                            , String selectedIssuanceReason, String emailId
                                            , String phone, Date returnByDate
                                            , String issuanceRemark, String personName) {
                                                
        List<String> listKeyInvIds = new List<String>();
        List<Key_Issuance__c> listKeyIssuance = new List<Key_Issuance__c>();
        addedKeys = addedKeys.removeEnd(',');
        system.debug(' submitKeyIssuance addedKeys==='+addedKeys);
        if(addedKeys != ''){
            listKeyInvIds = addedKeys.split(',');
        }
        
        Set<String> setKeyInvIds = new Set<String>();
        setKeyInvIds.addAll(listKeyInvIds);
        
        for(String invId : setKeyInvIds){
            Key_Issuance__c objKeyIssuance = new Key_Issuance__c();
            objKeyIssuance.Issue_Date_Time__c = System.now();
            objKeyIssuance.Team__c = selectedTeam;
            objKeyIssuance.Reason_For_Issuance__c = selectedIssuanceReason;
            objKeyIssuance.Email_Id__c = emailId;
            objKeyIssuance.Phone_Mobile__c = phone;
            objKeyIssuance.Return_By_Date__c = returnByDate;
            objKeyIssuance.Issuance_Remark__c = issuanceRemark;
            objKeyIssuance.Person_Name__c = personName;
            objKeyIssuance.Key_Inventory__c = invId;
            objKeyIssuance.Transaction_Type__c = 'Key Issuance';
            listKeyIssuance.add(objKeyIssuance);
        }
        
        if(!listKeyIssuance.isEmpty()){
            //insert listKeyIssuance;        
            Database.saveResult[] saveResult =  Database.Insert(listKeyIssuance,false);
            /*Integer successRecords = 0;
            Integer failureRecords = 0;
            for(Database.saveResult result:saveResult){
                if(result.isSuccess()){
                    successRecords ++;
                }else{
                    failureRecords ++;
                }
            }
            if(successRecords > 0){
                //apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Number of records processed ' +successRecords));
                
            }
            if(failureRecords > 0){
                //apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Number of records failed ' +failureRecords));
                
            }*/
        }
        System.debug('setKeyInvIds===' + setKeyInvIds);
        system.debug('listKeyIssuance==='+listKeyIssuance);
    }

/********************************************************************************************************************************
* Method Name : submitKeyIssuance
* Description : method to return all selected keys
* Return Type : void
* Parameter(s): none
********************************************************************************************************************************/    
    public void submitKeyReturn(){
        submitKeyReturnFuture(addedKeys, returnRemark);
        updatekeyInventory( addedKeys, 'With Key Owner');
    }
    
/********************************************************************************************************************************
* Method Name : submitKeyReturnFuture
* Description : method to return all selected keys in async
* Return Type : void
* Parameter(s): none
********************************************************************************************************************************/    
    @future
    public static void submitKeyReturnFuture( String addedKeys, String returnRemark) {
        List<String> listKeyInvIds = new List<String>();
        List<Key_Issuance__c> listKeyIssuance = new List<Key_Issuance__c>();
        addedKeys = addedKeys.removeEnd(',');
        system.debug('submitKeyReturn addedKeys==='+addedKeys);
        if(addedKeys != ''){
            listKeyInvIds = addedKeys.split(',');
        }
        
        Set<String> setKeyInvIds = new Set<String>();
        setKeyInvIds.addAll(listKeyInvIds);
        
        for(String invId : setKeyInvIds){
            Key_Issuance__c objKeyIssuance = new Key_Issuance__c();
            objKeyIssuance.Returned_Date_Time__c = System.now();
            objKeyIssuance.Return_Remark__c = returnRemark;
            objKeyIssuance.Key_Inventory__c = invId;
            objKeyIssuance.Transaction_Type__c = 'Key Return';
            listKeyIssuance.add(objKeyIssuance);
        }
        
        if(!listKeyIssuance.isEmpty()){
            //insert listKeyIssuance;
             Database.saveResult[] saveResult =  Database.Insert(listKeyIssuance,false);
            /*Integer successRecords = 0;
            Integer failureRecords = 0;
            for(Database.saveResult result:saveResult){
                if(result.isSuccess()){
                    successRecords ++;
                }else{
                    failureRecords ++;
                }
            }
            if(successRecords > 0){
                //apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Number of records processed ' +successRecords));
                
            }
            if(failureRecords > 0){
                //apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Number of records failed ' +failureRecords));
                
            }*/
        }
        
        System.debug('setKeyInvIds===' + setKeyInvIds);
        system.debug('listKeyIssuance==='+listKeyIssuance);
    }

    @future
    public static void updatekeyInventory( String addedKeys, String status) {
        
        if( String.isNotBlank( addedKeys ) ) {
            List<String> listKeyInvIds = new List<String>();
            List<Key_Inventory__c> listKeyInventory = new List<Key_Inventory__c>();
            addedKeys = addedKeys.removeEnd(',');
            system.debug('updateKeyInv addedKeys==='+addedKeys);
            if(addedKeys != ''){
                listKeyInvIds = addedKeys.split(',');
            }
            
            Set<String> setKeyInvIds = new Set<String>();
            setKeyInvIds.addAll(listKeyInvIds);

            for(String invId : setKeyInvIds){
                listKeyInventory.add( new Key_Inventory__c( Id = invId, Key_Issue_Status__c = status));
            }
            if( listKeyInventory.size() > 0 ) {
                update listKeyInventory;
            }
        }
    }
    public void submitInventoryChange(){
        /*List<String> listKeyInvIds = new List<String>();
        List<Key_Inventory__c> listKeyInventory = new List<Key_Inventory__c>();
        addedKeys = addedKeys.removeEnd(',');
        system.debug(' submitInventoryChange addedKeys==='+addedKeys);
        if(addedKeys != ''){
            listKeyInvIds = addedKeys.split(',');
        }
        
        Set<String> setKeyInvIds = new Set<String>();
        setKeyInvIds.addAll(listKeyInvIds);
        
        for(String invId : setKeyInvIds){
            Key_Inventory__c objInventory = new Key_Inventory__c();
            objInventory.Id = invId;
            if(String.isNotBlank(selectedKeyArea)){
                objInventory.Key_Area__c = selectedKeyArea;
            }
            if(String.isNotBlank(selectedCommonArea)){
                objInventory.Common_Area_Type__c = selectedCommonArea;
            }
            if(String.isNotBlank(selectedKeyType)){
                objInventory.Type_Of_Key__c = selectedKeyType;
            }
            if(String.isNotBlank(selectedCategoryKey)){
                objInventory.Category_Of_Key__c = selectedCategoryKey;
            }
            if(String.isNotBlank(cardNumber)){
                objInventory.Key_Card_Number__c = cardNumber;
            }
            if(String.isNotBlank(keyInvent.Key_Owner_Account__c)){
                objInventory.Key_Owner_Account__c = keyInvent.Key_Owner_Account__c;
            }
            if(String.isNotBlank(ownerEmail)){
                objInventory.Key_Owner_Email__c = ownerEmail;
            }
            if(String.isNotBlank(ownerPhone)){
                objInventory.Key_Owner_Phone__c = ownerPhone;
            }
            if(String.isNotBlank(ownerAddress)){
                objInventory.Key_Owner_Address__c = ownerAddress;
            }
            if(String.isNotBlank(ownerType)){
                objInventory.Key_Owner_Type__c = ownerType;
            }
            if(String.isNotBlank(selectedStatus)){
                objInventory.Key_Status__c = selectedStatus;
            }
            if(String.isNotBlank(selectedSubStatus)){
                objInventory.Key_Sub_Status__c = selectedSubStatus;
            }
            if(String.isNotBlank(selectedIssueStatus)){
                objInventory.Key_Issue_Status__c = selectedIssueStatus;
            }
            listKeyInventory.add( objInventory );
        }
        
        update listKeyInventory;*/
    }
/********************************************************************************************************************************
* Description : wrapper class of fields to display
********************************************************************************************************************************/     
    public Class DisplayFieldsWrapper {
        public String apiName { get; set; }
        public String label { get; set; }    
        
        Public DisplayFieldsWrapper() {
            apiName = '';
            label = '';
        }
    }
}