@isTest
public class SurveySubmissionAPITest {
	@TestSetup
    static void TestData() {
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(Agency_ID__c = '1234');
        insert sr;
        
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account(LastName = 'Test Account',Party_ID__c = '63062',RecordtypeId = rtId,Email__pc = 'test@mailinator.com');
        insert account;
        
        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];
        
        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Tenant Community Login User%' Limit 1];
        User user = new User(Username = System.now().millisecond() + 'test12345@test.com',ContactId = con.Id,ProfileId = portalProfile.Id,Alias='test123',Email='test12345@test.com',EmailEncodingKey = 'UTF-8',
                             LastName = 'Test Account',CommunityNickname = 'test12345',TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',LanguageLocaleKey = 'en_US');
        Database.insert(user);
        
        Booking__c booking = new Booking__c(Deal_SR__c = sr.Id,Account__c = account.Id);
        insert booking;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'test';
        locObj.Location_Type__c = 'Building';
        locObj.Location_ID__c = '12345';
        locObj.UAEProp__c = true;
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        invObj.Property_Country__c = 'UNITED ARAB EMIRATES';
        invObj.Marketing_Name_Doc__c = invObj.Id;
        invObj.Building_Location__c = locObj.Id;
        invObj.Property_Status__c = 'Ready'; 
        invObj.Unit_type__c = 'Villa';
        insert invObj;
        
        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id,Unit_Name__c = 'BD4/12/1204',Registration_ID__c = '3901',Handover_Flag__c = 'Y',Inventory__c = invObj.id,Dummy_Booking_Unit__c = true);
        insert bookingUnit;
        
        Survey_CRM__c amenitySurvey = new Survey_CRM__c(Name='App SR Completion Survey',Is_Active__c=TRUE);
        insert amenitySurvey;
        
        List<Survey_Question_CRM__c> questionList = new List<Survey_Question_CRM__c>();
        Survey_Question_CRM__c q1 = new Survey_Question_CRM__c(Name='Ease of booking amenities',OrderNumber__c=1,Type__c='Rating',Survey__c=amenitySurvey.Id);
        questionList.add(q1);
        Survey_Question_CRM__c q2 = new Survey_Question_CRM__c(Name='Overall app experience',OrderNumber__c=2,Type__c='Rating',Survey__c=amenitySurvey.Id);
        questionList.add(q2);
        Survey_Question_CRM__c q3 = new Survey_Question_CRM__c(Name='User friendliness of app',OrderNumber__c=3,Type__c='Rating',Survey__c=amenitySurvey.Id);
        questionList.add(q3);
        insert questionList;
    }
	
    @isTest
    static void testSRSurvey() {
        Booking_Unit__c objBU = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        Account objAcc = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        
        String bookingUnitId = String.valueOf(objBU.Id);
        String accountId = String.valueOf(objAcc.Id);
        
        FM_Case__c fmCase = new FM_Case__c(Request_Type_DeveloperName__c = 'Tenant_Renewal_T',
                                           RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Renewal').getRecordTypeId(),
                                           Request_Type__c = 'Tenant Renewal',
                                           Booking_Unit__c = bookingUnitId);
        insert fmCase;
        
        String fmCaseId = String.valueOf(fmCase.Id);
        
        String requestJson = 
            				'	{												' +
                			'		"booking_unit_id": "' + bookingUnitId + '",	' +
                			'		"account_id" : "' + accountId + '",			' +
                			'		"survey_type" : "sr",					' +
                			'		"fm_case_id" : "' + fmCaseId + '",			' +
                			'		"response_details": [						' +
                        	'		{											' +
                            '			"question_id": 1,						' +
                            '			"rating" : 5,							' +
                            '			"comments" : "Very good experience"		' +
                        	'		},											' +
                        	'		{											' +
                            '			"question_id": 2,						' +
                            '			"rating" : 4,							' +
                            '			"comments" : ""							' +
                        	'		},											' +
                        	'		{											' +
                            '			"question_id": 3,						' +
                            '			"rating" : 3,							' +
                            '			"comments" : ""							' +
                        	'		}											' +
                    		'		]											' +
        					'	}												' ;
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/SubmitSurvey';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(requestJson);
        RestContext.request = req;
        RestContext.response = res;
        
        SurveySubmissionAPI.submitSurveyResponse();
        
        Test.stopTest();
    }
    
    @isTest
    static void testProfileSurveyWithNoBookingUnitId() {
        Booking_Unit__c objBU = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        Account objAcc = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        
        String bookingUnitId = String.valueOf(objBU.Id);
        String accountId = String.valueOf(objAcc.Id);
        
        String requestJson = 
            				'	{												' +
                			'		"booking_unit_id": "",						' +
                			'		"account_id" : "' + accountId + '",			' +
                			'		"survey_type" : "profile",					' +
                			'		"fm_case_id" : "",							' +
                			'		"response_details": [						' +
                        	'		{											' +
                            '			"question_id": 1,						' +
                            '			"rating" : 3,							' +
                            '			"comments" : "Not good experience"		' +
                        	'		},											' +
                        	'		{											' +
                            '			"question_id": 2,						' +
                            '			"rating" : 2,							' +
                            '			"comments" : ""							' +
                        	'		},											' +
                        	'		{											' +
                            '			"question_id": 3,						' +
                            '			"rating" : 1,							' +
                            '			"comments" : ""							' +
                        	'		}											' +
                    		'		]											' +
        					'	}												' ;
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/SubmitSurvey';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(requestJson);
        RestContext.request = req;
        RestContext.response = res;
        
        SurveySubmissionAPI.submitSurveyResponse();
        
        Test.stopTest();
    }
    
    @isTest
    static void testWithNoJSON() {
        String jsonString = ''; 
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/SubmitSurvey';  
        req.requestBody = Blob.valueOf(jsonString);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        SurveySubmissionAPI.submitSurveyResponse();
        
        Test.stopTest();
    }
    
    @isTest
    static void testWithNullAccount() {
        Booking_Unit__c objBU = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        Account objAcc = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
		
		String bookingUnitId = String.valueOf(objBU.Id);
        String accountId = String.valueOf(objAcc.Id);
		
        String jsonString =  
            				'	{												' +
                			'		"booking_unit_id": "' + bookingUnitId + '",	' +
                			'		"survey_type" : "",							' +
                			'		"response_details": [						' +
                        	'		{											' +
                            '			"question_id": 1,						' +
                            '			"rating" : 5,							' +
                            '			"comments" : "Very good experience"		' +
                        	'		}											' +
                    		'		]											' +
        					'	}												' ;
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/SubmitSurvey';  
        req.requestBody = Blob.valueOf(jsonString);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        SurveySubmissionAPI.submitSurveyResponse();
        
        Test.stopTest();
    }
    
    @isTest
    static void testWithNoAccount() {
        Booking_Unit__c objBU = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        Account objAcc = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
		
		String bookingUnitId = String.valueOf(objBU.Id);
        String accountId = String.valueOf(objAcc.Id);
		
        String jsonString =  
            				'	{												' +
                			'		"booking_unit_id": "' + bookingUnitId + '",	' +
                			'		"account_id" : "",							' +
                			'		"survey_type" : "",							' +
                			'		"response_details": [						' +
                        	'		{											' +
                            '			"question_id": 1,						' +
                            '			"rating" : 5,							' +
                            '			"comments" : "Very good experience"		' +
                        	'		}											' +
                    		'		]											' +
        					'	}												' ;
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/SubmitSurvey';  
        req.requestBody = Blob.valueOf(jsonString);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        SurveySubmissionAPI.submitSurveyResponse();
        
        Test.stopTest();
    }
    
    @isTest
    static void testWithNullSurveyType() {
        Booking_Unit__c objBU = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        Account objAcc = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
		
		String bookingUnitId = String.valueOf(objBU.Id);
        String accountId = String.valueOf(objAcc.Id);
		
        String jsonString =  
            				'	{												' +
                			'		"booking_unit_id": "' + bookingUnitId + '",	' +
                			'		"account_id" : "' + accountId + '",			' +
                			'		"response_details": [						' +
                        	'		{											' +
                            '			"question_id": 1,						' +
                            '			"rating" : 5,							' +
                            '			"comments" : "Very good experience"		' +
                        	'		}											' +
                    		'		]											' +
        					'	}												' ;
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/SubmitSurvey';  
        req.requestBody = Blob.valueOf(jsonString);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        SurveySubmissionAPI.submitSurveyResponse();
        
        Test.stopTest();
    }
    
    @isTest
    static void testWithNoSurveyType() {
        Booking_Unit__c objBU = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        Account objAcc = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
		
		String bookingUnitId = String.valueOf(objBU.Id);
        String accountId = String.valueOf(objAcc.Id);
		
        String jsonString =  
            				'	{												' +
                			'		"booking_unit_id": "' + bookingUnitId + '",	' +
                			'		"account_id" : "' + accountId + '",			' +
                			'		"survey_type" : "",							' +
                			'		"response_details": [						' +
                        	'		{											' +
                            '			"question_id": 1,						' +
                            '			"rating" : 5,							' +
                            '			"comments" : "Very good experience"		' +
                        	'		}											' +
                    		'		]											' +
        					'	}												' ;
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/SubmitSurvey';  
        req.requestBody = Blob.valueOf(jsonString);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        SurveySubmissionAPI.submitSurveyResponse();
        
        Test.stopTest();
    }
    
    @isTest
    static void testWithInvalidSurveyType() {
        Booking_Unit__c objBU = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        Account objAcc = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
		
		String bookingUnitId = String.valueOf(objBU.Id);
        String accountId = String.valueOf(objAcc.Id);
		
        String jsonString =  
            				'	{												' +
                			'		"booking_unit_id": "' + bookingUnitId + '",	' +
                			'		"account_id" : "' + accountId + '",			' +
                			'		"survey_type" : "abc",						' +
                			'		"response_details": [						' +
                        	'		{											' +
                            '			"question_id": 1,						' +
                            '			"rating" : 5,							' +
                            '			"comments" : "Very good experience"		' +
                        	'		}											' +
                    		'		]											' +
        					'	}												' ;
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/SubmitSurvey';  
        req.requestBody = Blob.valueOf(jsonString);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        SurveySubmissionAPI.submitSurveyResponse();
        
        Test.stopTest();
    }
    
    @isTest
    static void testWithNoResponseDetails() {
        Booking_Unit__c objBU = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        Account objAcc = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
		
		String bookingUnitId = String.valueOf(objBU.Id);
        String accountId = String.valueOf(objAcc.Id);
		
        String jsonString =  
            				'	{												' +
                			'		"booking_unit_id": "' + bookingUnitId + '",	' +
                			'		"account_id" : "' + accountId + '",			' +
                			'		"survey_type" : "profile",					' +
                			'		"response_details": []						' +
        					'	}												' ;
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/SubmitSurvey';  
        req.requestBody = Blob.valueOf(jsonString);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        SurveySubmissionAPI.submitSurveyResponse();
        
        Test.stopTest();
    }
    
    @isTest
    static void apiMainMethodTestNullFmCase() {
        Booking_Unit__c objBU = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        Account objAcc = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
		
		String bookingUnitId = String.valueOf(objBU.Id);
        String accountId = String.valueOf(objAcc.Id);
        
        String jsonString =  
            				'	{												' +
                			'		"booking_unit_id": "' + bookingUnitId + '",	' +
                			'		"account_id" : "' + accountId + '",			' +
                			'		"survey_type" : "sr",						' +
                			'		"response_details": [						' +
                        	'		{											' +
                            '			"question_id": 1,						' +
                            '			"rating" : 5,							' +
                            '			"comments" : "Very good experience"		' +
                        	'		}											' +
                    		'		]											' +
        					'	}												' ;
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/SubmitSurvey';  
        req.requestBody = Blob.valueOf(jsonString);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        SurveySubmissionAPI.submitSurveyResponse();
        
        Test.stopTest();
    }
    
    @isTest
    static void apiMainMethodTestNoFmCase() {
        Booking_Unit__c objBU = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        Account objAcc = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
		
		String bookingUnitId = String.valueOf(objBU.Id);
        String accountId = String.valueOf(objAcc.Id);
        
        String jsonString =  
            				'	{												' +
                			'		"booking_unit_id": "' + bookingUnitId + '",	' +
                			'		"account_id" : "' + accountId + '",			' +
                			'		"survey_type" : "sr",						' +
            				'		"fm_case_id" : "",							' +
                			'		"response_details": [						' +
                        	'		{											' +
                            '			"question_id": 1,						' +
                            '			"rating" : 5,							' +
                            '			"comments" : "Very good experience"		' +
                        	'		}											' +
                    		'		]											' +
        					'	}												' ;
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/SubmitSurvey';  
        req.requestBody = Blob.valueOf(jsonString);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        SurveySubmissionAPI.submitSurveyResponse();
        
        Test.stopTest();
    }
}