public with sharing class FmIpmsRestCoffeeServices {

    @testVisible private static final String SF_REST_BASEURL = Label.FM_REST_IPMS_New_BaseUrl;

    @testVisible private static final String APEX_LOGIN = '/apex/login?client-id={0}';

    @testVisible private static final String UNIT_SOA = '/customer/DPSOA?registration_id={0}';
    
    @testVisible private static final String FM_SOA = '/customer/FMSOA?registration_id={0}';

    @testVisible private static final String UNIT_SOA_LANG = '/customer/DPSOA?registration_id={0}&language=Y';

    @testVisible private static final String BULK_SOA = '/customer/DPSOA?party_id={0}';

    @testVisible private static final String BULK_SOA_LANG = '/customer/DPSOA?party_id={0}&language=Y';

    @testVisible private static final String DP_INV_BY_REG_ID = '/customer/DPINV?registration_id={0}&language=Y';

    @testVisible private static final String DP_INV_BY_CUST_TRX_ID = '/customer/DPINV?cust_trx_id={0}&language=Y';

    @testVisible private static final String DP_RECEIPT_ID = '/customer/DPRECEIPT?receipt_id={0}&language=Y';

    public Static List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();

    //public static Boolean logErrors = true;
    public static Boolean updateCustomSetting = true;
    public static String accessToken;
    public static String languageCode;
    public static Map<String, String> urlLanguageCodeMap = new Map<String, String>();

    public static Map<String, String> languageCodeMap = new Map<String, String> {
        'ab' => 'Abkhazian',
        'aa' => 'Afar',
        'af' => 'Afrikaans',
        'ak' => 'Akan',
        'sq' => 'Albanian',
        'am' => 'Amharic',
        'ar' => 'Arabic',
        'an' => 'Aragonese',
        'hy' => 'Armenian',
        'as' => 'Assamese',
        'av' => 'Avaric',
        'ae' => 'Avestan',
        'ay' => 'Aymara',
        'az' => 'Azerbaijani',
        'bm' => 'Bambara',
        'ba' => 'Bashkir',
        'eu' => 'Basque',
        'be' => 'Belarusian',
        'bn' => 'Bengali',
        'bh' => 'Bihari',
        'bi' => 'Bislama',
        'bs' => 'Bosnian',
        'br' => 'Breton',
        'bg' => 'Bulgarian',
        'my' => 'Burmese',
        'ca' => 'Catalan',
        'ch' => 'Chamorro',
        'ce' => 'Chechen',
        'ny' => 'Chichewa',
        'zh' => 'Chinese',
        'cv' => 'Chuvash',
        'kw' => 'Cornish',
        'co' => 'Corsican',
        'cr' => 'Cree',
        'hr' => 'Croatian',
        'cs' => 'Czech',
        'da' => 'Danish',
        'dv' => 'Divehi',
        'nl' => 'Dutch',
        'dz' => 'Dzongkha',
        'en' => 'English',
        'eo' => 'Esperanto',
        'et' => 'Estonian',
        'ee' => 'Ewe',
        'fo' => 'Faroese',
        'fj' => 'Fijian',
        'fi' => 'Finnish',
        'fr' => 'French',
        'ff' => 'Fula',
        'gl' => 'Galician',
        'gd' => 'Gaelic',
        'gv' => 'Gaelic',
        'ka' => 'Georgian',
        'de' => 'German',
        'el' => 'Greek',
        'kl' => 'Greenlandic',
        'gn' => 'Guarani',
        'gu' => 'Gujarati',
        'ht' => 'Haitian Creole',
        'ha' => 'Hausa',
        'he' => 'Hebrew',
        'hz' => 'Herero',
        'hi' => 'Hindi',
        'ho' => 'Hiri Motu',
        'hu' => 'Hungarian',
        'is' => 'Icelandic',
        'io' => 'Ido',
        'ig' => 'Igbo',
        'id' => 'Indonesian',
        'in' => 'Indonesian',
        'ia' => 'Interlingua',
        'ie' => 'Interlingue',
        'iu' => 'Inuktitut',
        'ik' => 'Inupiak',
        'ga' => 'Irish',
        'it' => 'Italian',
        'ja' => 'Japanese',
        'jv' => 'Javanese',
        'kl' => 'Kalaallisut',
        'kn' => 'Kannada',
        'kr' => 'Kanuri',
        'ks' => 'Kashmiri',
        'kk' => 'Kazakh',
        'km' => 'Khmer',
        'ki' => 'Kikuyu',
        'rw' => 'Kinyarwanda',
        'rn' => 'Kirundi',
        'ky' => 'Kyrgyz',
        'kv' => 'Komi',
        'kg' => 'Kongo',
        'ko' => 'Korean',
        'ku' => 'Kurdish',
        'kj' => 'Kwanyama',
        'lo' => 'Lao',
        'la' => 'Latin',
        'lv' => 'Latvian',
        'li' => 'Limburgish',
        'ln' => 'Lingala',
        'lt' => 'Lithuanian',
        'lu' => 'Luga-Katanga',
        'lg' => 'Luganda,Ganda',
        'lb' => 'Luxembourgish',
        'gv' => 'Manx',
        'mk' => 'Macedonian',
        'mg' => 'Malagasy',
        'ms' => 'Malay',
        'ml' => 'Malayalam',
        'mt' => 'Maltese',
        'mi' => 'Maori',
        'mr' => 'Marathi',
        'mh' => 'Marshallese',
        'mo' => 'Moldavian',
        'mn' => 'Mongolian',
        'na' => 'Nauru',
        'nv' => 'Navajo',
        'ng' => 'Ndonga',
        'nd' => 'Northern Ndebele',
        'ne' => 'Nepali',
        'no' => 'Norwegian',
        'nb' => 'Norwegian bokmål',
        'nn' => 'Norwegian nynorsk',
        'ii' => 'Nuosu',
        'oc' => 'Occitan',
        'oj' => 'Ojibwe',
        'cu' => 'Old Church Slavonic',
        'or' => 'Oriya',
        'om' => 'Oromo (Afaan Oromo)',
        'os' => 'Ossetian',
        'pi' => 'Pāli',
        'ps' => 'Pashto',
        'fa' => 'Persian (Farsi)',
        'pl' => 'Polish',
        'pt' => 'Portuguese',
        'pa' => 'Punjabi',
        'qu' => 'Quechua',
        'rm' => 'Romansh',
        'ro' => 'Romanian',
        'ru' => 'Russian',
        'se' => 'Sami',
        'sm' => 'Samoan',
        'sg' => 'Sango',
        'sa' => 'Sanskrit',
        'sr' => 'Serbian',
        'sh' => 'Serbo-Croatian',
        'st' => 'Sesotho',
        'tn' => 'Setswana',
        'sn' => 'Shona',
        'ii' => 'Sichuan Yi',
        'sd' => 'Sindhi',
        'si' => 'Sinhalese',
        'ss' => 'Siswati',
        'sk' => 'Slovak',
        'sl' => 'Slovenian',
        'so' => 'Somali',
        'nr' => 'Southern Ndebele',
        'es' => 'Spanish',
        'su' => 'Sundanese',
        'sw' => 'Swahili (Kiswahili)',
        'ss' => 'Swati',
        'sv' => 'Swedish',
        'tl' => 'Tagalog',
        'ty' => 'Tahitian',
        'tg' => 'Tajik',
        'ta' => 'Tamil',
        'tt' => 'Tatar',
        'te' => 'Telugu',
        'th' => 'Thai',
        'bo' => 'Tibetan',
        'ti' => 'Tigrinya',
        'to' => 'Tonga',
        'ts' => 'Tsonga',
        'tr' => 'Turkish',
        'tk' => 'Turkmen',
        'tw' => 'Twi',
        'ug' => 'Uyghur',
        'uk' => 'Ukrainian',
        'ur' => 'Urdu',
        'uz' => 'Uzbek',
        've' => 'Venda',
        'vi' => 'Vietnamese',
        'vo' => 'Volapük',
        'wa' => 'Wallon',
        'cy' => 'Welsh',
        'wo' => 'Wolof',
        'fy' => 'Western Frisian',
        'xh' => 'Xhosa',
        'yi' => 'Yiddish',
        'ji' => 'Yiddish',
        'yo' => 'Yoruba',
        'za' => 'Zhuang',
        'zu' => 'Zulu'
    };

    //private static boolean performDMLonCustomSetting = false;
    //private static String newAccessToken;

    /* ------------- Updated 17th June '19 --------------- */
    public static String getAccessToken() {
        return IpmsRestServices__c.getInstance().BearerToken__c;
    }

    public static String getNewBearerToken() {
        IpmsRestServices__c ipmsRestServiceSettings = IpmsRestServices__c.getInstance();
        String clientId = ipmsRestServiceSettings.Client_Id__c;

        if (String.isNotBlank(clientId)) {
            HttpRequest req = new HttpRequest();
            String endpoint = String.format(APEX_LOGIN, new List<String>{clientId});
            req.setEndpoint(SF_REST_BASEURL + endpoint);
            req.setMethod('POST');
            req.setTimeout(120000);
            HttpResponse response = new Http().send(req);

            System.debug('JSON response: ' + response.getBody());
            Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            //newAccessToken = ;
            //performDMLonCustomSetting = true;
            //System.debug('New Token is: '+newAccessToken);


            return String.valueOf(m.get('token'));
        } else {
            System.debug('ClientId in Custom setting is blank');
            return null;
        }

    }
    
    /* ------------------------------------------------------------------- */

    public static String getFMSoa(String registrationId) {
        if (String.isBlank(registrationId)) {
            return registrationId;
        }
        String resultBody;

        HttpResponse res = callout(String.format(FM_SOA, new List<String>{ registrationId }));
        resultBody = res.getBody();


        String unitSoaUrl = fetchResponseUrl(resultBody);

        if (String.isBlank(unitSoaUrl)) {

            Error_Log__c errlog = new Error_Log__c(
                                    Process_Name__c = 'Unit SOA Generation',
                                    Account__c = CustomerCommunityUtils.customerAccountId,
                                    Error_Details__c = 'res:\n' + res.getBody()
            );

            lstErrorLog.add(errlog);
            
        
        }
        System.debug('UnitSoaUrl: ' +unitSoaUrl);
        return unitSoaUrl;
    }

    /* ------------------------------------------------------------------- */

    public static String getUnitSoa(String registrationId) {
        if (String.isBlank(registrationId)) {
            return registrationId;
        }
        String resultBody;

        HttpResponse res = callout(String.format(UNIT_SOA, new List<String>{ registrationId }));
        resultBody = res.getBody();


        String unitSoaUrl = fetchResponseUrl(resultBody);

        if (String.isBlank(unitSoaUrl)) {

            Error_Log__c errlog = new Error_Log__c(
                                    Process_Name__c = 'Unit SOA Generation',
                                    Account__c = CustomerCommunityUtils.customerAccountId,
                                    Error_Details__c = 'res:\n' + res.getBody()
            );

            lstErrorLog.add(errlog);
            //try {
            //    insert new Error_Log__c(
            //        Process_Name__c = 'Unit SOA Generation',
            //        Account__c = CustomerCommunityUtils.customerAccountId,
            //        Error_Details__c = 'res:\n' + res.getBody()
            //    );
            //} catch(Exception e) {
            //    System.debug(e.getMessage());
            //}
        
        }
        System.debug('UnitSoaUrl: ' +unitSoaUrl);
        return unitSoaUrl;
    }

    public static String getUnitSoaInLanguage(String registrationId) {
        if (String.isBlank(registrationId)) {
            return registrationId;
        }

        String resultBody;
        HttpResponse res = callout(String.format(UNIT_SOA_LANG, new List<String>{ registrationId }));
        resultBody = res.getBody();

        String unitSoaUrl = fetchResponseUrl(resultBody);

        if (String.isBlank(unitSoaUrl)) {

            Error_Log__c errlog = new Error_Log__c(
                                  Process_Name__c = 'Unit SOA Generation',
                                  Account__c = CustomerCommunityUtils.customerAccountId,
                                  Error_Details__c = 'res:\n' + res.getBody()
            );

            lstErrorLog.add(errlog);
            //try {
            //    insert new Error_Log__c(
            //        Process_Name__c = 'Unit SOA Generation',
            //        Account__c = CustomerCommunityUtils.customerAccountId,
            //        Error_Details__c = 'res:\n' + res.getBody()
            //    );
            //} catch(Exception e) {
            //    System.debug(e.getMessage());
            //}
        }
        System.debug('UnitSoaUrl: ' +unitSoaUrl);
        return unitSoaUrl;
    }

    //Below method returns 2 url of UnitSoaInLanguage.
    public static List<String> getAllUnitSoaInLanguageUrl(String registrationId) {
        system.debug('in method========');
       if (String.isBlank(registrationId)) {
            return null;
      }
        HttpResponse res = callout(String.format(UNIT_SOA_LANG, new List<String>{ registrationId }));
        system.debug('res========'+res);
        system.debug('getActionUrlList(res.getBody()========'+getActionUrlList(res.getBody()));
        return getActionUrlList(res.getBody());

    }

    public static String getBulkSoa(String partyId) {
        if (String.isBlank(partyId)) {
            return partyId;
        }
        String resultBody;

        HttpResponse res = callout(String.format(BULK_SOA, new List<String>{ partyId }));
        resultBody = res.getBody();
        System.debug('ResultBody: '+resultBody);

        String unitSoaUrl = fetchResponseUrl(resultBody);

        if (String.isBlank(unitSoaUrl)) {

            Error_Log__c errlog = new Error_Log__c(
                                Process_Name__c = 'Bulk SOA Generation',
                                Account__c = CustomerCommunityUtils.customerAccountId,
                                Error_Details__c = 'res:\n' + res.getBody()
            );

            lstErrorLog.add(errlog);

            //try {
            //    insert new Error_Log__c(
            //        Process_Name__c = 'Bulk SOA Generation',
            //        Account__c = CustomerCommunityUtils.customerAccountId,
            //        Error_Details__c = 'res:\n' + res.getBody()
            //    );
            //} catch(Exception e) {
            //    System.debug(e.getMessage());
            //}
        }
        System.debug('UnitSoaUrl: ' +unitSoaUrl);
        return unitSoaUrl;
    }

    public static String getBulkSoaInLanguage(String partyId) {
        if (String.isBlank(partyId)) {
            return partyId;
        }
        String resultBody;

        HttpResponse res = callout(String.format(BULK_SOA_LANG, new List<String>{ partyId }));
        resultBody = res.getBody();
        System.debug('resultBody: ' + resultBody);

        String unitSoaUrl = fetchResponseUrl(resultBody);

        if (String.isBlank(unitSoaUrl)) {

            Error_Log__c errlog = new Error_Log__c(
                                Process_Name__c = 'Bulk SOA Generation',
                                Account__c = CustomerCommunityUtils.customerAccountId,
                                Error_Details__c = 'res:\n' + res.getBody()
            );

            lstErrorLog.add(errlog);

            //try {
            //    insert new Error_Log__c(
            //        Process_Name__c = 'Bulk SOA Generation',
            //        Account__c = CustomerCommunityUtils.customerAccountId,
            //        Error_Details__c = 'res:\n' + res.getBody()
            //    );
            //} catch(Exception e) {
            //    System.debug(e.getMessage());
            //}
        }
        System.debug('UnitSoaUrl: ' +unitSoaUrl);
        return unitSoaUrl;
    }

    //Below method returns 2 url of BulkSoaInLanguage.
    public static List<String> getAllBulkSoaInLanguageUrl(String partyId) {
        if (String.isBlank(partyId)) {
            return null;
        }

        HttpResponse res = callout(String.format(BULK_SOA_LANG, new List<String>{ partyId }));
        return getActionUrlList(res.getBody());

    }

    //public static List<String> getInvoiceByRegIdInLang(String registrationId) {
    //    if(String.isBlank(registrationId)) {
    //        return null;
    //    }
    //    HttpResponse res = callout(String.format(DP_INV_BY_REG_ID, new List<String>{ registrationId }));
    //    return getActionUrlList(res.getBody());
    //    //return new List<String> {
    //    //    'https://ptctest.damacgroup.com/COFFEE/apex/view/XXDCARANA_DPIN_54925592_1.PDF',
    //    //    'https://ptctest.damacgroup.com/COFFEE/apex/view/XXDCARANA_DPIN_54925593_1.PDF'
    //    //};
    //}

    public static List<String> getInvoiceByCustTrxIdInLang(String customerTrxId) {

        if(String.isBlank(customerTrxId)) {
            return null;
        }
        system.debug('customerTrxId===='+customerTrxId);
        system.debug('DP_INV_BY_CUST_TRX_ID===='+DP_INV_BY_CUST_TRX_ID);
        HttpResponse res = callout(String.format(DP_INV_BY_CUST_TRX_ID, new List<String>{ customerTrxId }));
        system.debug('res===='+res);
        return getActionUrlList(res.getBody());

        //return new List<String> {
        //    'https://ptctest.damacgroup.com/COFFEE/apex/view/XXDCARANA_DPIN_54925592_1.PDF',
        //    'https://ptctest.damacgroup.com/COFFEE/apex/view/XXDCARANA_DPIN_54925593_1.PDF'
        //};
    }

    //Created on 4 july
    public static List<String> getAllReceiptUrl(String receiptId) {
        if(String.isBlank(receiptId)) {
            return null;
        }

        HttpResponse res = callout(String.format( DP_RECEIPT_ID , new List<String>{ receiptId }));
        return getActionUrlList(res.getBody());
    }

    public static HttpResponse callout(String relativeRequestUrl) {

        accessToken = getAccessToken();
        System.debug('accessToken is: '+accessToken);  // to get Access token from Custom setting;
        Boolean invalidTokenInCustomSetting = false;
        if (String.isBlank(accessToken)) {
            accessToken = getNewBearerToken();
            invalidTokenInCustomSetting = true;
        }
        HttpRequest req = new HttpRequest();
        req.setEndpoint(SF_REST_BASEURL + relativeRequestUrl);
        System.debug(req.getEndpoint());
        req.setMethod('GET');
        req.setHeader('Accept', 'application/json');  //new
        req.setHeader('Authorization','Bearer' + accessToken);
        req.setTimeout(120000);
        System.debug('req : '+req);
        HttpResponse response = new Http().send(req);

        System.debug('Status Code = ' + response.getStatusCode());
        System.debug('body = ' + response.getBody());

        //updated on 19th June
        Map<String, Object> responseMap = new Map<String, Object>();
        if(String.isNotBlank(response.getBody())) {
            responseMap = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
        }

        if (String.valueOf(responseMap.get('error')) == 'Unauthorized') {
            System.debug('Access Token was expired');
            accessToken = getNewBearerToken();
            invalidTokenInCustomSetting = true;

            System.debug('New Access Token: '+accessToken);

            req = new HttpRequest();
            req.setEndpoint(SF_REST_BASEURL + relativeRequestUrl);
            System.debug(req.getEndpoint());
            req.setMethod('GET');
            req.setHeader('Authorization','Bearer' + accessToken);
            req.setTimeout(120000);
            response = new Http().send(req);
            System.debug('Status Code = ' + response.getStatusCode());
            System.debug('body = ' + response.getBody());

        }


        if(invalidTokenInCustomSetting) {
            updateBearerToken();
        }


        return response;
        //return new Http().send(req);
    }

    public static void updateBearerToken() {

        System.debug('updateCustomSetting in FmIpms Class is: '+updateCustomSetting);

        if (updateCustomSetting) {
            System.debug('Perform DML on CS with newAccess Token - '+accessToken);
            IpmsRestServices__c ipmsCustomSettingwithNewToken = IpmsRestServices__c.getOrgDefaults();
            ipmsCustomSettingwithNewToken.BearerToken__c = accessToken;
            update ipmsCustomSettingwithNewToken;    

            if(lstErrorLog.size() > 0) {
                System.debug('lstErrorLog : ' + lstErrorLog);
                insert lstErrorLog;
            }         
        }

    }


    public static List<String> getActionUrlList(String resultBody) {
        system.debug('resultBody===='+resultBody);
        //Response response = (Response) Json.deserialize( resultBody, Response.class);
        //if (response == NULL) {
        //    return NULL;
        //}
        //if (response.complete == NULL
        //    || !response.complete
        //    || response.actions == NULL
        //    || response.actions.isEmpty()
        //) {
        //    return NULL;
        //}
        //String pdfUrl;
        //List<String> urlList = new List<String>();

        //Action[] responseAction = response.actions;

        //if(responseAction != null && responseAction.size() > 0) {
        //    System.debug('Action list size: '+responseAction.size());
        //    for( Integer i=0; i<responseAction.size(); i++ ) {
        //        pdfUrl = responseAction[i].url;
        //        System.debug('pdfUrl '+i+' : '+pdfUrl);
        //        urlList.add(pdfUrl);
        //    }
        //    System.debug('urlList is: '+urlList);
        //    return urlList;
        //}

        //Updated on 3rd July as per New Response in JSON
        Response response = (Response) Json.deserialize( resultBody, Response.class);
        system.debug('response===='+response);
        if (response == NULL) {
            return NULL;
        }
        if (response.complete == NULL
            || !response.complete
            || response.responseLines == NULL
            || response.responseLines.isEmpty()
        ) {
            return NULL;
        }
        String pdfUrl;
        List<String> urlList = new List<String>();

        responseLines[] responseLinelst = response.responseLines;
        if(responseLinelst != null && responseLinelst.size() > 0) {
            System.debug('ResponseLines list size: '+responseLinelst.size());
            for( Integer i=0; i<responseLinelst.size(); i++ ) {
                pdfUrl = responseLinelst[i].url;
                languageCode = responseLinelst[i].language.toLowerCase();
                System.debug('pdfUrl '+i+' : '+pdfUrl);
                System.debug('languageCode '+i+' : '+languageCode);
                if(!String.isBlank(pdfUrl) && pdfUrl != Null) {
                    urlList.add(pdfUrl);
                }
                if(!String.isBlank(languageCode) && languageCode != Null) {
                    urlLanguageCodeMap.put(pdfUrl , languageCode);
                }
            }
            System.debug('urlList is: ' + urlList);
            System.debug('urlLanguageCodeMap is: ' + urlLanguageCodeMap);
            return urlList;
        }

        return null;
    }



    @testVisible
    private static String fetchResponseUrl(String result) {
        //Response response = (Response) Json.deserialize(result, Response.class);

        //if (response == NULL) {
        //    return NULL;
        //}
        //if (response.complete == NULL
        //    || !response.complete
        //    || response.actions == NULL
        //    || response.actions.isEmpty()
        //) {
        //    return NULL;
        //}
        ///* ------ Updated on 18th June ------------*/
        //String pdfUrl;
        //String actionStr;

        //Action[] respAct = response.actions;
        //if(respAct != null && respAct.size() > 0) {
        //    pdfUrl = respAct[respAct.size() - 1].url;
        //    actionStr = respAct[respAct.size() - 1].action;
        //    System.debug('actionStr for Language Code is: '+actionStr);
        //    languageCode = actionStr.substringAfter('_').toLowerCase();
        //    System.debug('Language Code is: '+languageCode);
        //}
        //return pdfUrl;

        //updated on 3 July as per New response in JSON
        Response response = (Response) Json.deserialize(result, Response.class);

        if (response == NULL) {
            return NULL;
        }
        if (response.complete == NULL
            || !response.complete
            || response.responseLines == NULL
            || response.responseLines.isEmpty()
        ) {
            return NULL;
        }
        String pdfUrl;
        String actionStr;

        ResponseLines[] respLines = response.responseLines;
        if(respLines != null && respLines.size() > 0) {
            pdfUrl = respLines[respLines.size() - 1].url;
            languageCode = respLines[respLines.size() - 1].language.toLowerCase();
            System.debug('Language Code is: '+languageCode);
        }
        return pdfUrl;

       // return response.actions[0].url;
    }

    public class Response {
        public String           responseId;   //142215
        public String           responseTime; //Tue Jul 17 16:19:15 GST 2018
        public String           status;
        public String           responseMessage;
        //public Action[]         actions;
        public ResponseLines[]  responseLines;
        public Boolean          complete;
        public String           error;
        public String           message;
    }

    //public class Action {
    //    public String action;
    //    public String method;
    //    public String url;
    //}

    public class ResponseLines {
        public String documentName;
        public String language;
        public string url;
    }

}