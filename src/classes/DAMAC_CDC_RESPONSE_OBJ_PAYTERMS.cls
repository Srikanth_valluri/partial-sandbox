/*
Developed By: DAMAC IT Team
Usage:DAMAC_CDC_REST_UTILITY
*/
public class DAMAC_CDC_RESPONSE_OBJ_PAYTERMS{
    
   public list<PAYMENT_TERMS> PAYMENT_TERMS;

    
    public class PAYMENT_TERMS{
        public String INSTALLMENT;  //DP
        public String EVENT;    //Immediate
        public String EXPECTED_DATE;
        public String PERCENT;  //10
        public String PAYMENT_DATE; //02-JAN-2017
    }
    public static DAMAC_CDC_RESPONSE_OBJ_PAYTERMS parse(String json){
        return (DAMAC_CDC_RESPONSE_OBJ_PAYTERMS) System.JSON.deserialize(json, DAMAC_CDC_RESPONSE_OBJ_PAYTERMS.class);
    }
}