public class UpdatePaymentTerms {
    
    @InvocableMethod 
    public static void invokeApex(list<Case> lstCase) {
        
        /*Set<Id> setWhatId = new Set<Id>();
        for (Task objTask : lstTask){
            setWhatId.add(objTask.WhatId);
        }
        set<Id> BUIds = new set<Id>();
        list<case> lstCase = new list<case>();
        lstCase=[select id, CaseNumber, Booking_Unit__r.Id from case where id in:setWhatId];
        system.debug('lstCase'+lstCase);
        
        for(case objCase: lstCase){
            BUIds.add(objCase.Booking_Unit__r.Id);
        }
        system.debug('BUIds'+BUIds);

        String P_SR_NUMBER = lstCase[0].CaseNumber;*/
        String caseId = lstCase[0].Id;
        
        Case objCase = [Select Id, RecordTypeId, RecordType.DeveloperName From Case WHere Id =: caseId ];
        if (objCase.RecordType.DeveloperName == 'Early_Handover') {
            CreatePaymentTermsforEHO.EarlyHandoverPaymentPlanCreationResponce(caseId);
        } else if (objCase.RecordType.DeveloperName == 'Handover') {
            CreatePaymentTermsforESH.EarlySettlementPaymentPlanCreation(caseId);
        }
    }
}