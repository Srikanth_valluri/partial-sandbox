Public class Damac_TrackVehicle {
    Public List<locationDetails> locations { get; set; }
    public Decimal checkInLat { get; set; }
    public Decimal checkInLang { get; set; }
    
    public String checkInLocation { get; set; }
    public String checkOutLocation { get; set; }
    
    public Decimal checkOutLat { get; set; }
    public Decimal checkOutLang { get; set; }
    public Trip_Events__c tripEventLast { get; set; }
    public Trip_Events__c tripEventFirst { get; set; }
    
    public class locationDetails {
        public String lat {get; set; }
        public String lang {get; set; }
        public String angle {get; set; }
        public locationDetails() {
            lat = '';
            lang = '';
            angle = '';
        }
    }
    public Damac_TrackVehicle (ApexPages.standardController stdController) {
        locations = new List<locationDetails> ();
        checkOutLang = 0.0;
        checkOutLat = 0.0;
        checkInLat = 0.0;
        checkInLang = 0.0;
        checkInLocation = '';
        checkOutLocation = '';
        try {    
            Trip__c trip = (Trip__c)stdController.getRecord();
            List<Trip_Events__c> tripEventsFirst = new List<Trip_Events__c>();
            List<Trip_Events__c> tripEventsLast = new List<Trip_Events__c>();
            tripEventLast = new Trip_Events__c();
            tripEventFirst = new Trip_Events__c();
            tripEventsFirst = [SELECT Latitude__c, Longitude__c, Cars__c, Trip__r.Car__r.Vehicle_Id__c, Check_In_Reason__c, Time__c
                          From Trip_Events__c 
                          WHERE Trip__c =: trip.Id
                          AND Check_In_Reason__c = 'Customer Picked'
                          Order by CreatedDate asc Limit 1];
            tripEventsLast = [SELECT Latitude__c, Longitude__c, Cars__c, Trip__r.Car__r.Vehicle_Id__c, Check_In_Reason__c, Time__c
                          From Trip_Events__c Where Check_In_Reason__c = 'CheckOut'
                          AND Trip__c =: trip.Id
                          Order by CreatedDate desc Limit 1];
            if(tripEventsFirst.size() > 0) {
                for(Trip_Events__c tripEve : tripEventsFirst){
                    tripEventFirst = tripEve;
                }
            }
            if(tripEventsLast.size() > 0) {
                for(Trip_Events__c tripEve : tripEventsLast){
                    tripEventLast = tripEve;
                }
            }
            
            
            String vehicleId = tripEventLast.Trip__r.Car__r.Vehicle_Id__c;
            tripEventFirst.Time__c = tripEventFirst.Time__c.replace('T', ' ').split ('\\+')[0];
            String fromDate = tripEventfirst.Time__c+'';
            fromDate = fromDate.split('\\+')[0];
            fromDate = EncodingUtil.urlEncode(fromDate, 'UTF-8');
            fromDate = fromDate.replaceAll('%3A', ':');
            System.Debug(fromDate);
            
            
            tripEventLast.Time__c = tripEventLast.Time__c.replace('T', ' ').split ('\\+')[0];
            
            String toDate = tripEventLast.Time__c+'';
            toDate = toDate.split('\\+')[0];
            toDate = EncodingUtil.urlEncode(toDate, 'UTF-8');
            toDate = toDate.replaceAll('%3A', ':');
            
            HTTPResponse response = new HTTPResponse();
            if(!Test.isRunningTest()) {
                response = Damac_FleetRootService.getVehicleHistory(vehicleId, fromDate, toDate);
            } else {
                String body = '{"status": 200,"results": [{"Latitude": 25.22728,"Longitude": 55.2780516, "TimeStamp" : "2019-07-02T05:06:43+04:00"}], "message": ""}';
                response.setBody(body);
            }
            
            String responseBody = response.getBody();
            System.debug('::::::response::::::::::::'+responseBody);
            Damac_GetVehiclehistoryJSONParser obj =  Damac_GetVehiclehistoryJSONParser.parse(responseBody);
            
            DateTime PreviousProcessedTime = NULL;
            for (Damac_GetVehiclehistoryJSONParser.cls_results result : obj.results) {
                String timeStamp = result.TimeStamp;
                System.Debug(timeStamp);
                String DateVal = timeStamp.SubStringBefore('T');
                
                String TimeVal = timeStamp.SubStringAfter('T');
                timeVal = timeVal.split ('\\+')[0];
                
                DateTime processedTime = DateTime.NewInstance(Integer.valueOf(DateVal.split('-')[0]), Integer.valueOf(DateVal.split('-')[1]), Integer.valueOf(DateVal.split('-')[2]),
                                        Integer.valueOf(TimeVal.split(':')[0]), Integer.valueOf(TimeVal.split(':')[1]), Integer.valueOf(TimeVal.split(':')[2]));
                
                System.Debug (checkInLat);
                if (checkOutLat == 0.0) {
                    checkOutLocation = result.CurrentLocation;
                    checkOutLat = result.Latitude;
                    checkOutLang = result.Longitude;
                }
                if (checkInLat == 0.0) {
                    checkInLat = result.Latitude;
                    checkInLocation = result.CurrentLocation;
                    checkInLang = result.Longitude;
                }
                if (PreviousProcessedTime == NULL) {
                    PreviousProcessedTime = processedTime;
                }
                
                if (processedTime < PreviousProcessedTime) {
                    System.Debug('Check In');
                    checkInLocation = result.CurrentLocation;
                    checkInLat = result.Latitude;
                    checkInLang = result.Longitude;
                }
                if (processedTime > PreviousProcessedTime) {
                    System.Debug('Check Out');
                    checkOutLat = result.Latitude;
                    checkOutLocation = result.CurrentLocation;
                    checkOutLang = result.Longitude;
                }
                PreviousProcessedTime = processedTime;
                locationDetails locaDetails = new locationDetails();
                locaDetails.angle = String.valueOf(result.Angle);
                locaDetails.lat = String.valueOf(result.Latitude);
                locaDetails.lang = String.valueOf(result.Longitude);
                
                locations.add(locaDetails);
                
            }
            System.Debug(CheckInLat+'==='+checkInLang+'==='+checkOutLat+'==='+checkInLang);
        } catch (Exception e) {
            System.Debug(e.getMessage());
        }
    }
}