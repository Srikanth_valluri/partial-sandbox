@isTest(SeeAllData=true)
private class CC_createParentSRStep_Test{
  static testMethod void test_EvaluateCustomCode_UseCase1(){
  
  NSIBPM__Service_Request__c  sSR1 = new NSIBPM__Service_Request__c ();
  insert sSR1;
  NSIBPM__Service_Request__c  sSR2 = new NSIBPM__Service_Request__c ();
  sSR2.NSIBPM__Parent_SR__c = sSR1.Id;
  insert sSR2;
  NSIBPM__Step__c sStep = new NSIBPM__Step__c();
  sStep.NSIBPM__SR__c =sSR2.Id;
    CC_createParentSRStep obj01 = new CC_createParentSRStep();
    obj01.EvaluateCustomCode(sSR2,sStep);
  }
 static testMethod void test_EvaluateCustomCode_UseCase2(){
  
  NSIBPM__Service_Request__c  sSR1 = new NSIBPM__Service_Request__c ();
  insert sSR1;
  NSIBPM__Step__c sStep = new NSIBPM__Step__c();
  sStep.NSIBPM__SR__c =sSR1.Id;
    CC_createParentSRStep obj01 = new CC_createParentSRStep();
    obj01.EvaluateCustomCode(sSR1,sStep);
  } 
  
}