@isTest
private class CancelAmenityBooking_APITest {

    @TestSetup
    static void TestData() {
    
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Name = 'JNU';
        locObj.Building_Name__c = 'Janusia';
        insert locObj;

        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id,
                                                          Registration_ID__c = '3901',
                                                          Inventory__c = invObj.Id,
                                                          Unit_Name__c='JNU/SD168/XH2910B');
        insert bookingUnit;

        Property__c propObj = TestDataFactoryFM.createProperty();
        insert propObj;

        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;

        Resource__c objRes = new Resource__c();
        objRes.Chargeable_Fee_Slot__c = 10;
        //objRes.Non_Operational_Days__c = 'Sunday';
        objRes.Resource_Type__c = 'FM Amenity';
        objRes.No_of_advance_booking_days__c = 2;
        objRes.Resource_Type__c = 'FM Amenity';
        objRes.Deposit__c = 3;
        objRes.Chargeable_Fee_Slot__c = 5;
        objRes.No_of_deposit_due_days__c = 1;
        objRes.Contact_Person_Email__c = 'test@test.com';   //
        objRes.Contact_Person_Name__c = 'Test'; //
        objRes.Contact_Person_Phone__c = '1234567890';  //
        insert objRes;

        Resource_Sharing__c objRS= new Resource_Sharing__c();
        objRS.Building__c = locObj.Id;
        objRS.Resource__c = objRes.Id;
        insert objRS;

        Resource_Slot__c objResSlot1 = new Resource_Slot__c();
        objResSlot1.Resource__c = objRes.Id;
        objResSlot1.Start_Date__c = Date.Today();
        objResSlot1.End_Date__c = Date.Today().addDays(30);
        objResSlot1.Start_Time_p__c = '09:00';
        objResSlot1.End_Time_p__c = '09:30';
        insert objResSlot1;

        Resource_Slot__c objResSlot2 = new Resource_Slot__c();
        objResSlot2.Resource__c = objRes.Id;
        objResSlot2.Start_Date__c = Date.Today();
        objResSlot2.End_Date__c = Date.Today().addDays(30);
        objResSlot2.Start_Time_p__c = '10:00';
        objResSlot2.End_Time_p__c = '10:30';
        insert objResSlot2;

        Non_operational_days__c obj = new Non_operational_days__c();
        obj.Non_operational_date__c = Date.today().addDays(3);
        obj.Resource__c = objRes.Id;
        insert obj;

        String devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().
                                get('Amenity Booking').getRecordTypeId();
        FM_Case__c objFm = new FM_Case__c();
        objFm.RecordTypeId = devRecordTypeId;
        objFm.Origin__c='Portal';  //'App-Hello Damac';
        objFm.Amenity_Booking_Status__c = 'Booking Confirmed';
        objFm.Status__c = 'Closed';
        objFm.Booking_Start_Time_p__c = '09:00';
        objFm.Booking_End_Time_p__c = '09:30';
        objFm.Resource_Booking_Type__c = 'FM Amenity';
        objFm.Resource__c = objRes.Id;
        objFm.Resource_SlotID__c = objResSlot1.id;
        objFm.Booking_Date__c = Date.today().addDays(1);
        objFm.Booking_Unit__c = bookingUnit.Id;
        objFm.Account__c = account.Id;
        objFm.Contact_Email__c = 'test@test.com';
        objFM.Contact_Mobile__c = '123456789';
        objFM.Contact_person__c = 'Test';
        objFm.Initiated_by_tenant_owner__c = 'Owner';
        objFm.Admin__c = fmUser.FM_User__c;
        objFm.Tenant__c =  account.Id;
        objFm.Tenant_Email__c = account.Email__pc;

        insert objFm;

        
    }

    @isTest
    static void CancelBookingTest() {

        Account acc = [SELECT id FROM Account WHERE Party_ID__c = '63062'];
        FM_Case__c objFM = [SELECT id FROM FM_Case__c WHERE Account__c =:acc.id];

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/cancelAmenityBooking';  
        req.addParameter('fmCaseId',objFM.Id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        CancelAmenityBooking_API.CancelBooking();
        Test.stopTest();
    }

    //Negative Scenarios
    @isTest
    static void CancelBookingTestNoParam() {

        Account acc = [SELECT id FROM Account WHERE Party_ID__c = '63062'];
        FM_Case__c objFM = [SELECT id FROM FM_Case__c WHERE Account__c =:acc.id];

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/cancelAmenityBooking';  
        //req.addParameter('fmCaseId',objFM.Id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        CancelAmenityBooking_API.CancelBooking();
        Test.stopTest();
    }

    @isTest
    static void CancelBookingTestParamNull() {

        Account acc = [SELECT id FROM Account WHERE Party_ID__c = '63062'];
        FM_Case__c objFM = [SELECT id FROM FM_Case__c WHERE Account__c =:acc.id];

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/cancelAmenityBooking';  
        req.addParameter('fmCaseId','');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        CancelAmenityBooking_API.CancelBooking();
        Test.stopTest();
    }

    @isTest
    static void CancelBookingTestAlredyCancelled() {

        Account acc = [SELECT id FROM Account WHERE Party_ID__c = '63062'];
        FM_Case__c objFM = [SELECT id, Status__c, Amenity_Booking_Status__c FROM FM_Case__c WHERE Account__c =:acc.id];
        objFM.Status__c = 'Cancelled';
        objFM.Amenity_Booking_Status__c = 'Cancelled';
        update objFM;

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/cancelAmenityBooking';  
        req.addParameter('fmCaseId',objFM.id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        CancelAmenityBooking_API.CancelBooking();
        Test.stopTest();
    }

}