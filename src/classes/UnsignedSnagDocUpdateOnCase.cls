/****************************************************************************************
Description: UnsignedSnagDocUpdateOnCase is used in a scheduled action in process builder 
             to update signed doc status
----------------------------------------------------------------------------------------*
Version     Date            Author              Description                             
----------------------------------------------------------------------------------------*
1.0         29 Sep 2020     Shruti Nikam        Initial Draft 
*****************************************************************************************/
public with sharing class UnsignedSnagDocUpdateOnCase {
	@InvocableMethod
    public static void checkDocNotsigned(List<cuda_signnow__SignNowStatus__c> SignNowStatusList) {
        system.debug('###Entered');
        List<Case> caseList = new List<Case>(); 
        caseList = [Select id, Snag_Completion_Date__c from case where id =:SignNowStatusList[0].cuda_signnow__RelatedToCase__c limit 1];
        if(SignNowStatusList[0].cuda_signnow__Status__c == 'pending' &&
           caseList[0].Snag_Completion_Date__c == System.today()-3) {
            caseList[0].Snag_handover_doc_not_signed_72hrs__c = true;
            caseList[0].Status = 'Closed';
            update caseList;
        }
    }
}