public without sharing class RentalPoolAgreementCREController_CC extends RentalPoolAgreementCREController{
    public String caseId{get;set;}
    public RentalPoolAgreementCREController_CC() {
        super();
    }

    public override String getName() {
        system.debug('strAccountID*****#####'+strAccountID);
        system.debug('strCaseId********#####'+strCaseId);
        system.debug('strSRtypeURL*****#####'+strSRType);
        if(String.isNotBlank(strAccountID) && strSRType == 'RentalPool'){
            init();
        }
        return strAccountID;
    }
    public override PageReference submitSR() {
        saveAsDraft();
        Task taskObj = TaskUtility.getTask((SObject)objCase, Label.TaskGenerateRPDoc,
                    'CRE','Rental Pool Agreement', system.today().addDays(1));
        if(! String.valueOf(objCase.OwnerId).startsWith('005')) {//If owner is not User
            taskObj.OwnerId = Label.DefaultCaseOwnerId;
        }
        insert taskObj;
        objCase.status = 'Submitted';
        update objCase;
        //Create task
        caseId = objCase.id;
        //return new PageReference('/Customer');
        return null;
    }
    public override PageReference saveAsDraft() {
        Integer i=1;//Used to generate unique URL for documents
        Set<SR_Attachments__c> setSRAttachments_Valid = new Set<SR_Attachments__c>();
        List<DocumentWrap> lstSRAttachments = new List<DocumentWrap>();
        UploadMultipleDocController.data respObj = new UploadMultipleDocController.data();
        //Upload POA

        if(objCase.Id != null) {
            lstSRAttachments_Valid = [SELECT Id, Name, Case__c, View__c, Attachment__c, Type__c, Case__r.CaseNumber, IsValid__c
                                        FROM SR_Attachments__c WHERE Case__c =: objCase.Id];
        }
        List<UploadMultipleDocController.MultipleDocRequest> lstReq = new List<UploadMultipleDocController.MultipleDocRequest>();
        if(String.isNotBlank(strPOAAttachmentBody) && String.isNotBlank(strPOAAttachmentName) && lstAccount.size() > 0
            && !checkAttachmentType('Power Of Attorney') && String.isNotBlank(strSelectedUnit) &&  strSelectedUnit != 'None') {
            i++;
            //SR Attachement for POA
            SR_Attachments__c objSRAtt = new SR_Attachments__c();
            // objSRAtt.Name = extractName(strPOAAttachmentName);
            objSRAtt.Name = 'POA Document';
            objSRAtt.Type__c = 'Power Of Attorney';
            objSRAtt.Case__c = objCase.Id;
            objSRAtt.isValid__c = false;
            objSRAtt.IsRequired__c = true;
            lstSRAttachments.add(new DocumentWrap(extractName(strPOAAttachmentName), objSRAtt));
            UploadMultipleDocController.MultipleDocRequest reqObjPOA = new UploadMultipleDocController.MultipleDocRequest();
            reqObjPOA.category = 'Document';
            reqObjPOA.entityName = 'Damac Service Requests';
            if(String.isNotBlank(strPOAAttachmentBody)){
                blob objBlob = extractBody(strPOAAttachmentBody);
                if(objBlob != null){
                    reqObjPOA.base64Binary =  EncodingUtil.base64Encode(objBlob);
                }
            }
            reqObjPOA.fileDescription = 'Power Of Attorney';
            reqObjPOA.fileId = objCase.CaseNumber + '-'
                                            + String.valueOf(System.currentTimeMillis()+i) +'.'+ strPOAAttachmentName.substringAfterLast('.');
            reqObjPOA.fileName = objCase.CaseNumber + '-'
                                            + String.valueOf(System.currentTimeMillis()+i)  +'.'+ strPOAAttachmentName.substringAfterLast('.');
            reqObjPOA.registrationId = objCase.CaseNumber;
            reqObjPOA.sourceFileName = 'IPMS-'+lstAccount[0].party_ID__C+'-'+extractName(strPOAAttachmentName);
            reqObjPOA.sourceId = 'IPMS-'+lstAccount[0].party_ID__C+'-'+extractName(strPOAAttachmentName);
            lstReq.add(reqObjPOA);
            strPOAAttachmentBody = null;
            strPOAAttachmentName = null;
        }//POA
        if(String.isNotBlank(strCRFAttachmentBody) && String.isNotBlank(strCRFAttachmentName) && lstAccount.size() > 0
            && !checkAttachmentType('CRF Form') && String.isNotBlank(strSelectedUnit) &&  strSelectedUnit != 'None') {
            i++;
            //SR Attachement for POA
            SR_Attachments__c objSRAtt = new SR_Attachments__c();
            // objSRAtt.Name = extractName(strCRFAttachmentName);
            objSRAtt.Name = 'CRF Document';
            objSRAtt.Type__c = 'CRF Form';
            objSRAtt.Case__c = objCase.Id;
            objSRAtt.isValid__c = true;
            objSRAtt.IsRequired__c = true;
            lstSRAttachments.add(new DocumentWrap(extractName(strCRFAttachmentName), objSRAtt));
            UploadMultipleDocController.MultipleDocRequest reqObjCRF = new UploadMultipleDocController.MultipleDocRequest();
            reqObjCRF.category = 'Document';
            reqObjCRF.entityName = 'Damac Service Requests';
            if(String.isNotBlank(strCRFAttachmentBody)){
                blob objBlob = extractBody(strCRFAttachmentBody);
                if(objBlob != null){
                    reqObjCRF.base64Binary =  EncodingUtil.base64Encode(objBlob);
                }
            }
            reqObjCRF.fileDescription = 'CRF Form';
            reqObjCRF.fileId = objCase.CaseNumber + '-'
                                            + String.valueOf(System.currentTimeMillis()+i) +'.'+ strCRFAttachmentName.substringAfterLast('.');
            reqObjCRF.fileName = objCase.CaseNumber + '-'
                                            + String.valueOf(System.currentTimeMillis()+i)  +'.'+ strCRFAttachmentName.substringAfterLast('.');
            reqObjCRF.registrationId = objCase.CaseNumber;
            reqObjCRF.sourceFileName = 'IPMS-'+lstAccount[0].party_ID__C+'-'+extractName(strCRFAttachmentName);
            reqObjCRF.sourceId = 'IPMS-'+lstAccount[0].party_ID__C+'-'+extractName(strCRFAttachmentName);
            lstReq.add(reqObjCRF);
            strCRFAttachmentBody = null;
            strCRFAttachmentName = null;
        }//POA
        if(lstReq.size() > 0) {
            try{
                respObj= UploadMultipleDocController.getMultipleDocUrl(lstReq);
                if(respObj != NULL) {
                    if(respObj.status == 'Exception'){
                        ApexPages.addmessage(new ApexPages.message(
                            ApexPages.severity.Error,respObj.message));
                        createErrorLogRecord(strAccountID, strSelectedUnit, objCase.Id, respObj.message);
                        return null;
                    }
                }
                if(respObj.Data == null || respObj.Data.size()==0) {
                   ApexPages.addmessage(new ApexPages.message(
                   ApexPages.severity.Error,'Problems while getting response from document upload'));
                   createErrorLogRecord(strAccountID, strSelectedUnit, objCase.Id, 'Problems while getting response from document upload');
                   return null;
               }

               for(DocumentWrap att : lstSRAttachments) {
                for(UploadMultipleDocController.MultipleDocResponse objData : respObj.data) {
                    if('IPMS-'+lstAccount[0].party_ID__C+'-'+att.attName == objData.PARAM_ID){
                       if(objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url)){
                           att.SR_att.Attachment_URL__c = objData.url;
                           att.SR_att.Case__c = objCase.Id;
                           setSRAttachments_Valid.add(att.SR_att);
                       }
                       else{
                           ApexPages.addmessage(new ApexPages.message(
                           ApexPages.severity.Error,'Problems while getting response from document '+objData.PARAM_ID));
                           // errorLogger('Problems while getting response from document '+objData.PARAM_ID, '', '');
                           return null;
                       }
                   }
                }
            }//outer for
            //Saving details
            System.debug('====lstSRAttachments==' + lstSRAttachments);
            System.debug('====setSRAttachments_Valid==' + setSRAttachments_Valid);

            if(setSRAttachments_Valid.size() > 0) {
            
            List<SR_Attachments__c> lstAtt = new List<SR_Attachments__c>();
                lstAtt.addAll(setSRAttachments_Valid);
                for(SR_Attachments__c att : lstAtt) {
                    att.Case__c = objCase.Id;
                }
                upsert lstAtt;

                lstSRAttachments_Valid = [SELECT Id, Name, Case__c, View__c, Attachment__c, Type__c, Case__r.CaseNumber, IsValid__c
                                        FROM SR_Attachments__c WHERE Case__c =: objCase.Id];
                blnShowPOATable = checkAttachmentType('Power Of Attorney');
                blnShowCRFTable = checkAttachmentType('CRF Form');
                // blnShowOtherTable = checkAttachmentType('Other');
                System.debug('====blnShowPOATable==' + blnShowPOATable);
                System.debug('====blnShowPOATable==' + blnShowPOATable);
            }


            System.debug('===respObj==' + respObj);
            }
            catch(Exception excp) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, excp.getMessage() + excp.getLineNumber()));
            }
        }
        if(objCase.IsPOA__c) {
            if(String.isNotBlank(casePOAExpiryDate)) {
                objCase.POA_Expiry_Date__c = generateDateFromString(casePOAExpiryDate);
            }
        }
        else {
            objCase.POA_Expiry_Date__c = NULL;
            objCase.POA_Name__c = NULL;
            objCase.Purpose_of_POA__c = NULL;
            objCase.POA_Issued_By__c = NULL;
            casePOAExpiryDate = '';
        }
        if(String.isNotBlank(strUnitAvailableDate)) {
            objCase.Unit_Availability_Date__c = generateDateFromString(strUnitAvailableDate);
        }
        system.debug('strSelectedUnit---'+strSelectedUnit);
        objCase.Booking_unit__c = strSelectedUnit;
        objCase.Selected_Rental_Pool_Offer__c = strSelectedOffer;
         if(String.isNotBlank(objBU.Handover_Flag__c) && objBU.Handover_Flag__c == 'Y') {
            objCase.Handover__c = true;
        }
        try{
        upsert objCase;
        }
        Catch(Exception ex){
            system.debug('EXCEPTION----'+ex);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage() + ex.getLineNumber()));
        }
        objCase = [SELECT Id,
                        CaseNumber,
                        IsPOA__c,
                        POA_Name__c,
                        OwnerId,
                        Status,
                        Purpose_of_POA__c,
                        POA_Issued_By__c,
                        Mode_of_Dispatch_of_Agreement__c,
                        Unit_Availability_Date__c,
                        POA_Expiry_Date__c,
                        Booking_Unit__c,
                        (SELECT Id, Name, Case__c,
                                    View__c,
                                    Attachment__c,
                                    Type__c,
                                    Case__r.CaseNumber, IsValid__c
                        FROM SR_Attachments__r)
                FROM Case
                WHERE Id =: objCase.Id LIMIT 1];
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Case saved successfully: ' + objCase.CaseNumber));
        return null;
    }
    public override void proceedCase() {
        //Create Case
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Agreement').getRecordTypeId();
        objCase.AccountId = strAccountID;
        objCase.RecordTypeId = devRecordTypeId;
        objCase.Origin = 'Portal';
        objCase.Selected_Rental_Pool_Offer__c = strSelectedOffer;
        if(String.isNotBlank(objBU.Handover_Flag__c) && objBU.Handover_Flag__c == 'Y') {
            objCase.Handover__c = true;
        }

        upsert objCase;
        objCase = [SELECT Id,
                                CaseNumber,
                                IsPOA__c,
                                POA_Name__c,
                                OwnerId,
                                Status,
                                Purpose_of_POA__c,
                                POA_Issued_By__c,
                                Mode_of_Dispatch_of_Agreement__c,
                                Unit_Availability_Date__c,
                                POA_Expiry_Date__c,
                                Selected_Rental_Pool_Offer__c,
                                Booking_Unit__c,
                                (SELECT Id, Name, Case__c,
                                            View__c,
                                            Attachment__c,
                                            Type__c,
                                            Case__r.CaseNumber, IsValid__c
                                FROM SR_Attachments__r)
                    FROM Case
                    WHERE Id =: objCase.Id LIMIT 1];
        proceed = true;
        //saveAsDraft();
    }
}