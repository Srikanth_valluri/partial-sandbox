/**
 * @File Name          : ScheduleViolationProcessTaskCreatorTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 8/20/2019, 12:43:51 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/20/2019, 12:34:10 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
public with sharing class ScheduleViolationProcessTaskCreatorTest {
    public static testmethod void testSchedule(){
        
        FM_Case__c fmCaseObj = new FM_Case__c();
        fmCaseObj.Issue_Date__c = Date.today();
        fmCaseObj.Request_Type_DeveloperName__c = 'Violation_Notice';
        insert fmCaseObj;
        
        Task taskForAdmin = new Task();
        taskForAdmin.Assigned_User__c='FM Admin';
        taskForAdmin.Priority='Normal';
        taskForAdmin.WhatID = fmCaseObj.Id;
        taskForAdmin.status='Not Started';
        taskForAdmin.subject=Label.Select_Notice_Type_and_Issue_Violation_Notice_Task;
        taskForAdmin.Process_Name__c = 'Violation Notice';
        taskForAdmin.ActivityDate = System.today();
        insert taskForAdmin;

        Test.StartTest();
         ScheduleViolationProcessTaskCreator sh1 = new ScheduleViolationProcessTaskCreator(
            taskForAdmin,
            UserInfo.getUserId() 
         );      
         String sch = '0  00 1 3 * ?';
           system.schedule('Test', sch, sh1);
        Test.stopTest();
    }
}