/**************************************************************************************************
* Name         : BirthdayReminderNotificationBatch
* Test Class  : 
* Description: Batch class to send email notification related to Key Person Birthday
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR      DATE
* 1.0         QBurst      29/09/2020
**************************************************************************************************/
public class BirthdayReminderNotificationBatch implements Database.Batchable<sObject>{

    /*********************************************************************************************
    * @Description: Implementing the start method of batch interface, contains query.
    * @Params      : Database.BatchableContext
    * @Return      : Database.QueryLocator
    *********************************************************************************************/  
    public Database.QueryLocator start(Database.BatchableContext BC){
        Date after7days = System.today().addDays(7);
        return Database.getQueryLocator([SELECT Id, Name, Account_Owner__c,
                                             Account.OwnerId, Account.Owner.Email
                                         FROM Contact 
                                         WHERE Account.RecordType.Name = 'Corporate Agency'
                                         AND Account.Agency_Type__c = 'Corporate'
                                         AND Owner__c = true 
                                         AND Birthdate != NULL
                                         AND Birthdate =: after7days]); 
    }

    /*********************************************************************************************
    * @Description: Implementing the execute method of the batch interface, contains the criteria.
    * @Params      : Database.BatchableContext, List<sObject>
    * @Return: void
    *********************************************************************************************/
    public void execute(Database.BatchableContext BC, List<Contact> scope){
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Id templateId =  [select id, name from EmailTemplate where developername = 'Birthday_Reminder_Key_Person_BD_Team'].id; 
        Email_Notification_To_CC_List__c toCCList = Email_Notification_To_CC_List__c.getValues('Key Person Birthday Reminder');
        for(Contact con: scope){
            List<String> toAddresses = new List<String>();
            List<String> ccAddresses = new List<String>();
            List<String> bccAddresses = new List<String>();
            
            if(toCCList != null){
                if(toCCList.To_Addresses__c != null){
                    for(String email: toCCList.To_Addresses__c.split(',')){
                        toAddresses.add(email.trim());
                    }
                }
                if(toCCList.CC_Addresses__c != null){
                    for(String email: toCCList.CC_Addresses__c.split(',')){
                        ccAddresses.add(email.trim());
                    }
                }
                if(toCCList.BCC_Addresses__c != null){
                    for(String email: toCCList.BCC_Addresses__c.split(',')){
                        bccAddresses.add(email.trim());
                    }
                }
            }
            Messaging.SingleEmailMessage mail = Messaging.renderStoredEmailTemplate(templateId, con.Account.ownerId, con.Id);
            mail.setToAddresses(toAddresses);
            mail.setCCAddresses(ccAddresses);
            mail.setBCCAddresses(bccAddresses);
            mail.setTargetObjectId(con.Account.ownerId);
            mail.setSenderDisplayName('Damac Property ');
            mail.setReplyTo('noreply@Damacgroup.com');
            mail.setTemplateId(templateId);
            mail.setSaveAsActivity(false);
            mails.add(mail);  
            system.debug('mail: ' + mail);
        }
        Messaging.sendEmail(mails);
    }

    /*********************************************************************************************
    * @Description: Implementing the Finish method, to send an email after job completion.
    * @Params      : Database.BatchableContext
    * @Return: void
    *********************************************************************************************/
    public void finish(Database.BatchableContext BC){
    }
}// End of class.