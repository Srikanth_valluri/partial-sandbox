@isTest
private class RoboCallPickedUpNotifierTest {

    @isTest 
    private static void notifyCreTest() {
    
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                    OnOffCheck__c = true);
        insert newSetting1;
        
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, 
                       FirstName='Test FirstName1', 
                       LastName='Test LastName2'
                       );
        insert objAcc;
        Id rtId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Recovery Calling List').getRecordTypeId();
        Calling_List__c objCall = new Calling_List__c(RecordtypeId = rtId,
                                                      Account__c = objAcc.Id
                                                     );
        insert objCall;

        Robocall_Campaign__c objRoboCamp = new Robocall_Campaign__c(Name = 'test');
        insert objRoboCamp;

        Added_Call__c objAddCall = new Added_Call__c(Input_String_For_Add_Call__c = 'Test'
                                                     , Calling_List__c = objCall.Id
                                                     , Campaign_Robo__c = objRoboCamp.Id);
        insert objAddCall;

        /*FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', '');

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
            'https://api.sendgrid.com/v3/mail/send' => sendGridResponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));*/
        
        Test.startTest();

        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User usr = new User(Alias = 'he', Email='abc.123@test.com', 
                    EmailEncodingKey='UTF-8', LastName='Hi', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='abc.123@test.com');
        insert usr ;
        System.runAs(usr) {
            objCall.ownerId = usr.id;
            update objCall;

            List<Id> lstAddCallID = new List<Id>();
            lstAddCallID.add(objAddCall.Id);

            RoboCallPickedUpNotifier.notifyCre(lstAddCallID);
        }

        Test.stopTest();
    }
    
     @isTest 
    private static void replaceMergeFieldsTest() {
    
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                    OnOffCheck__c = true);
        insert newSetting1;
        
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, 
                       FirstName='Test FirstName1', 
                       LastName='Test LastName2'
                       );
        insert objAcc;
        Id rtId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Recovery Calling List').getRecordTypeId();
        Calling_List__c objCall = new Calling_List__c(RecordtypeId = rtId,
                                                      Account__c = objAcc.Id
                                                     );
        insert objCall;

        Robocall_Campaign__c objRoboCamp = new Robocall_Campaign__c(Name = 'test');
        insert objRoboCamp;

        Added_Call__c objAddCall = new Added_Call__c(Input_String_For_Add_Call__c = 'Test'
                                                     , Calling_List__c = objCall.Id
                                                     , Campaign_Robo__c = objRoboCamp.Id);
        insert objAddCall;
        
        Test.startTest();

        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User usr = new User(Alias = 'he', Email='abc.123@test.com', 
                    EmailEncodingKey='UTF-8', LastName='Hi', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='abc.123@test.com');
        insert usr ;
        System.runAs(usr) {
            objCall.ownerId = usr.id;
            update objCall;

            List<Id> lstAddCallID = new List<Id>();
            lstAddCallID.add(objAddCall.Id);
            Added_Call__c newObjAddCall = [SELECT Id,
                                                Calling_List__r.Name,
                                                Calling_List__r.Owner.Name,
                                                Calling_List__r.Account__r.Name,
                                                Calling_List__c
                                           FROM Added_Call__c 
                                                LIMIT 1];
            String strTest = '{!Calling_List__c.Name} {!Calling_List__c.OwnerFullName} {!Account.Name} {!Calling_List__c.Id}';
            //String strTest = '{!Account.Name}';
            RoboCallPickedUpNotifier.replaceMergeFields(strTest , newObjAddCall );
        }

        Test.stopTest();
    
    }


}