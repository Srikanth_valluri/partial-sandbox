global class DMLBatchIterator implements Database.Batchable<sobject>{
    
    global string action;
    global list<sobject> scope2process;
    global DMLBatchIterator(list<sObject> scope, string action){
        scope2process = scope;
        this.action = action;
    }
    
    global Iterable<sObject> start(Database.BatchableContext bc){
        return new CustomIterable(scope2process);
    }
    
    global void execute(Database.BatchableContext bc, list<sobject> scope){ 
        if(action.tolowerCase() == 'insert')
            insert scope;
        if(action.tolowerCase() == 'update')
            update scope;    
        if(action.tolowerCase() == 'delete')
            delete scope;    
    }
    
    global void finish(Database.BatchableContext bc){
    
    }

}