/**************************************************************************************************
* Name               : Invocable_CancelDealRejectionSteps_Test
* Description        : Test Class for Invocable_CancelDealRejectionSteps
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR          DATE            Comments
* 1.0                         16/06/2019      Created
**************************************************************************************************/
@isTest
public class Invocable_CancelDealRejectionSteps_Test {

    @testSetup 
    static void setupData() {
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;

        Id DealRT = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = acc.Id;
        sr.RecordTypeId = DealRT;
        sr.Agency__c = acc.id;
        sr.Agency_Type__c = 'Corporate';
        insert sr;

        List<New_Step__c> newStepsList = new List<New_Step__c>();
        New_Step__c nstp1 = new New_Step__c();
        nstp1.Service_Request__c = sr.id;
        nstp1.Step_No__c = 2;
        nstp1.Step_Status__c = 'Awaiting Token Deposit';
        nstp1.Step_Type__c = 'Token Payment';
        newStepsList.add(nstp1);

        New_Step__c nstp2 = new New_Step__c();
        nstp2.Service_Request__c = sr.id;
        nstp2.Step_No__c = 3;
        nstp2.Step_Status__c = 'CANCELLED';
        nstp2.Step_Type__c = 'PC Confirmation';
        newStepsList.add(nstp2);
        insert newStepsList;

        Booking__c booking = new Booking__c(Account__c = acc.Id, Deal_SR__c = sr.Id);
        insert booking;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id, Unit_Name__c = 'Test name 1',
                                                          Registration_ID__c = '1600', Registration_Status__c = 'Active Status',
                                                          Unit_Selling_Price_AED__c = 100);
        insert bookingUnit;
    }

    static Testmethod void Test_Invocable_CancelDealRejectionSteps(){
        test.startTest();
        New_Step__c nstp = [SELECT Id FROM New_Step__c WHERE Step_Type__c = 'PC Confirmation' LIMIT 1];
        Invocable_CancelDealRejectionSteps.CancelDealRejectionSteps(new List<Id> {nstp.Id});
        test.stopTest();
    }
}