@isTest
public class TestApprovalHelper {
	public static testmethod void insertFMCase() {
        Id profileId = [select id from profile where name = 'System Administrator'].id;
        User u= TestDataFactoryFM.createUser(profileId);
        insert u;
        
		Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;
       
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk =TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
      
        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;
        
        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns.Inventory__c = invObj.id;
        insert buIns;
        
        FM_User__c fmUser = TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;
        
        Id recTypeId=Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Change of Contact Details').getRecordTypeId();
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Change_of_Contact_Details';
        fmCaseObj.Booking_Unit__c=buIns.id;
        fmCaseObj.Email__c='a@gmail.com';
        fmCaseObj.Mobile_no__c='12121212';
        fmCaseObj.RecordTypeId=recTypeId;
        //fmCaseObj.Approving_Authorities__c='FM Admin';
        insert fmCaseObj;
        fmCaseObj.Approving_Authorities__c='FM Admin';
        fmCaseObj.Approval_Status__c='Pending';
        fmCaseObj.Submit_for_Approval__c=true;
        update fmCaseObj;
    }
    
    public static testmethod void testExtractApprovedCaseId(){
        Id profileId = [select id from profile where name = 'System Administrator'].id;
        User u= TestDataFactoryFM.createUser(profileId);
        insert u;
        
		Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;
       
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk =TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
      
        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;
        
        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns.Inventory__c = invObj.id;
        insert buIns;
        
        FM_User__c fmUser = TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;
        
        Id recTypeId=Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Change of Contact Details').getRecordTypeId();
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Change_of_Contact_Details';
        fmCaseObj.Booking_Unit__c=buIns.id;
        fmCaseObj.Email__c='a@gmail.com';
        fmCaseObj.Mobile_no__c='12121212';
        fmCaseObj.RecordTypeId=recTypeId;
        fmCaseObj.Approving_Authorities__c='FM Admin';
        insert fmCaseObj;
        fmCaseObj.Approving_Authorities__c='';
        fmCaseObj.Approval_Status__c='Pending';
        fmCaseObj.Submit_for_Approval__c=true;
        update fmCaseObj;
    }
}