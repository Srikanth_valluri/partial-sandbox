@isTest
private class ProceedCasecontrollerTest
{

    static  testmethod void testAutoRun(){
         Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Fund Transfer Active Units').getRecordTypeId();
         list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
         insert accountobj;
         Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
         insert caseobj;
         test.startTest();
         PageReference pg  = Page.ProceedCase;
         Test.setCurrentPage(pg);
         ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
         ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
         Pagereference newPg = controller.redirectToHomePage();
        // System.assertEquals(pg,controller.redirectToHomePage());
         test.stopTest();

    }
    static  testmethod void Assignment(){
         Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
         list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
          insert accountobj;
         Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
         caseobj.Seller__c = accountobj[0].Id;
         insert caseobj;
         test.startTest();
         PageReference pg  = Page.ProceedCase;
         Test.setCurrentPage(pg);
         ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
         ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        Pagereference newPg= controller.callCREPortalHomeForAssignment(accountobj[0].id,caseobj.id);
        test.stopTest();

        // System.assertEquals(pg,controller.callCREPortalHomeForAssignment(accountobj[0].id,caseobj.id));

    }
    static  testmethod void callCREPortalHomeForPenalty(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Penalty Waiver').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        //caseobj.RecordType.DeveloperName ='Penalty_Waiver';
        insert caseobj;
        test.startTest();
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        Pagereference newPg=controller.callCREPortalHomeForPenalty(accountobj[0].id,caseobj.id);
        test.stopTest();

      //  System.assertEquals(pg,controller.callCREPortalHomeForPenalty(accountobj[0].id,caseobj.id));

    }
    static  testmethod void AOPT(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('AOPT').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        //caseobj.RecordType.DeveloperName ='AOPT';
        insert caseobj;
        test.startTest();
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        system.debug(pg);
        system.debug(controller.callCREPortalHomeForAOPT(accountobj[0].id,caseobj.id, 'AOPT'));
        Pagereference newPg = controller.callCREPortalHomeForAOPT(accountobj[0].id,caseobj.id, 'AOPT');
        test.stopTest();

       // System.assertEquals(pg,newPg);
    }

    static  testmethod void parking()
    {
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Parking').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        //caseobj.RecordType.DeveloperName ='AOPT';
        insert caseobj;
        test.startTest();
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        //system.debug(pg);
        //system.debug(controller.callCREPortalHomeForAOPT(accountobj[0].id,caseobj.id));
        Pagereference newPg = controller.callCREPortalHomeForParking(accountobj[0].id,caseobj.id);
        controller.redirectToHomePage();
        test.stopTest();

       // System.assertEquals(pg,newPg);
    }

    static  testmethod void FundTransfer1(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Fund Transfer').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        //caseobj.RecordType.DeveloperName='Fund_Transfer';
        insert caseobj;
        test.startTest();
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        Pagereference newPg=controller.callCREFundTransfer(accountobj[0].id,caseobj.id);
        test.stopTest();

      //  System.assertEquals(pg,controller.callCREFundTransfer(accountobj[0].id,caseobj.id));
    }
    static  testmethod void test_titleDeed(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Title Deed').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        insert caseobj;
        test.startTest();
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        Pagereference newPg=controller.redirectToHomePage();
        test.stopTest();

      //  System.assertEquals(pg,controller.callCREFundTransfer(accountobj[0].id,caseobj.id));
    }
    static  testmethod void FundTransferActiveUnits(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Fund Transfer Active Units').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        //caseobj.RecordType.DeveloperName='Fund_Transfer_Active_Units';
        insert caseobj;
        test.startTest();
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        Pagereference newPg=controller.callCREFundTransferActiveUnits(accountobj[0].id,caseobj.id);
        test.stopTest();

       // System.assertEquals(pg,controller.callCREFundTransferActiveUnits(accountobj[0].id,caseobj.id));
    }
    static  testmethod void Refunds(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Customer Refund').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        //caseobj.RecordType.DeveloperName='Token_Refund';
        insert caseobj;
        test.startTest();
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
         Pagereference newPg=controller.callCREPortalHomeRefunds(accountobj[0].id,caseobj.id);
         test.stopTest();

      //  System.assertEquals(pg,controller.callCREPortalHomeRefunds(accountobj[0].id,caseobj.id));

    }
    static  testmethod void ProofOfPayment(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POP').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        //caseobj.RecordType.DeveloperName='Proof_of_Payment_SR';
        insert caseobj;
        test.startTest();
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        Pagereference newPg=controller.callCREPortalPOP(accountobj[0].id,caseobj.id);
       // System.assertEquals(pg,controller.callCREPortalPOP(accountobj[0].id,caseobj.id));
    }
    static  testmethod void COCD(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        //caseobj.RecordType.DeveloperName='Change_of_Details';
        insert caseobj;
        test.startTest();
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
         Pagereference newPg=controller.callCRECOD(accountobj[0].id,caseobj.id,caseobj.RecordType.DeveloperName,caseobj.Status);
         test.stopTest();


    }

    static  testmethod void Penalty_Waiver(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Penalty Waiver').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        System.assert(caseobj!=null);
    
        insert caseobj;
        test.startTest();
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        controller.redirectToHomePage();
        test.stopTest();
    
    }
    static  testmethod void AOPT1(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('AOPT').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        System.assert(caseobj!=null);
    
        insert caseobj;
        test.startTest();
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        controller.redirectToHomePage();
        test.stopTest();
    
    }
    static  testmethod void POP1(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POP').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        System.assert(caseobj!=null);
        insert caseobj;
        test.startTest();
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        controller.redirectToHomePage();
        test.stopTest();
    
    }
    static  testmethod void Token_Refund(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Token Refund').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        System.assert(caseobj!=null);
        insert caseobj;
        test.startTest();
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        controller.redirectToHomePage();
        test.stopTest();
    
    }
    static  testmethod void FundTransfer(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Fund Transfer').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        System.assert(caseobj!=null);
        insert caseobj;
        test.startTest();
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        controller.redirectToHomePage();
        test.stopTest();
    
    }
    static  testmethod void FundTransferActive1(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Fund Transfer Active Units').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        System.assert(caseobj!=null);
        insert caseobj;
        test.startTest();
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        controller.redirectToHomePage();
        test.stopTest();
    
    }
    static  testmethod void changeofDetail(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        System.assert(caseobj!=null);
        insert caseobj;
        test.startTest();
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        controller.redirectToHomePage();
        test.stopTest();
    }
    
    static  testmethod void plotHandover(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Plot NOC').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        System.assert(caseobj!=null);
        insert caseobj;
        test.startTest();
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        controller.redirectToHomePage();
        test.stopTest();
    }
    
    static  testmethod void ChangeofJointBuyer(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Joint Buyer').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        System.assert(caseobj!=null);
        insert caseobj;
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        controller.redirectToHomePage();
    }
    static  testmethod void NameNationalityChange(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Name Nationality Change').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        System.assert(caseobj!=null);
        insert caseobj;
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        controller.redirectToHomePage();
    }
    static  testmethod void PassportDetailUpdate(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Passport Detail Update').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        System.assert(caseobj!=null);
        insert caseobj;
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        controller.redirectToHomePage();
    }
    
    static  testmethod void ComplaintProcess(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Complaint').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        System.assert(caseobj!=null);
        insert caseobj;
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        controller.redirectToHomePage();
    }
    
    static  testmethod void ErrorHandling(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Complaint').getRecordTypeId();
    
        Case caseobj =TestDataFactory_CRM.createCase(null,caseRecordTypeId);
        System.assert(caseobj!=null);
        insert caseobj;
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        controller.redirectToHomePage();
    }
    
    static  testmethod void RPAgreementProcess(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Agreement').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        System.assert(caseobj!=null);
        insert caseobj;
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        controller.redirectToHomePage();
    }
    
    static  testmethod void RPAgreementEHOProcess(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Agreement Early Handover').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        System.assert(caseobj!=null);
        insert caseobj;
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        controller.redirectToHomePage();
    }
    
    static  testmethod void RPAssignmentProcess(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Assignment').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        caseobj.Seller__c = accountobj[0].id;
        System.assert(caseobj!=null);
        insert caseobj;
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        controller.redirectToHomePage();
    }
    
    static  testmethod void RPTerminationProcess(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        System.assert(caseobj!=null);
        insert caseobj;
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        controller.redirectToHomePage();
    }
    
    static  testmethod void PromotionsProcess(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Promotions').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        System.assert(caseobj!=null);
        insert caseobj;
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        controller.redirectToHomePage();
    }
    
    static  testmethod void EHOProcess(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        System.assert(caseobj!=null);
        insert caseobj;
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        controller.redirectToHomePage();
    }
    
    static  testmethod void LHOProcess(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Lease Handover').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        System.assert(caseobj!=null);
        insert caseobj;
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        controller.redirectToHomePage();
    }

    static  testmethod void Overdue_Rebate(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Overdue Rebate/Discount').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        System.assert(caseobj!=null);
        insert caseobj;
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        controller.redirectToHomePage();
    }
    
    /*static  testmethod void RebateOnAdvance(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rebate On Advance').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        System.assert(caseobj!=null);
       // insert caseobj;
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        controller.redirectToHomePage();
    }*/

    static  testmethod void RiyadhRotanaConversion(){
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Riyadh Rotana Conversion').getRecordTypeId();
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;
        Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
        System.assert(caseobj!=null);
        insert caseobj;
        PageReference pg  = Page.ProceedCase;
        Test.setCurrentPage(pg);
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        ProceedCasecontroller  controller = new ProceedCasecontroller(sc);
        controller.redirectToHomePage();
    }
}