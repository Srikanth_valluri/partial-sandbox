/*-------------------------------------------------------------------------------------------------
Description: Controller for LeadManagement page

    =======================================================================================================
        Version | Date(DD-MM-YYYY) | Last Modified By | Comments
    --------------------------------------------------------------------------------------------------------
        1.0     | 26-03-2018       | Lochana Rajput   | 1. Added logic to implement a rule as initial draft
    --------------------------------------------------------------------------------------------------------
        2.0     | 27-03-2018       | Lochana Rajput   | 1. Added logic to implement all rules
    --------------------------------------------------------------------------------------------------------
        3.0     | 09-04-2018       | Lochana Rajput   | 1. Added logic to save Lead Update reason
    --------------------------------------------------------------------------------------------------------
        3.1     | 10-04-2018       | Lochana Rajput   | 1. Added logic to apply same buyer details logic
                                                        2. Getting buyers for selected booking unit
    --------------------------------------------------------------------------------------------------------
        3.2     | 11-04-2018       | Lochana Rajput   | 1. Added logic to allow users to update Inquiry on Buyer
    --------------------------------------------------------------------------------------------------------
        3.3     | 14-05-2018       | Lochana Rajput   | 1. Removed 'Related Campaign On Booking Unit' filter
     --------------------------------------------------------------------------------------------------------
        4.0     | 20-05-2018       | Monali Nagpure   | 1. Added upload attachment functionality.   
        5.0     | 24-09-2020       | Srikanth Valluri | 1. Added Campaign update functionality.   
   =========================================================================================================
*/
public without sharing class LeadManagementController {
    public Lead_Management__c objLead{get;set;}
    public String selectedBuyer{get;set;}
    public String selectedInquiry{get;set;}
    public String selectedbookingUnit{get;set;}
    public Buyer__c selectedBuyerObj {get;set;}
    // public Boolean showNewInq {get;set;}
    public Boolean showBuyerDetails {get;set;}
    @testVisible private list<Lead_Admin_Console__mdt> lstRules;
    public String msg {get;set;}
    public Boolean showMessage {get;set;}
    public Boolean showBuyerPanel{get;set;}
    public String msgType {get;set;}
    public List<Buyer__c> buyerList {get;set;}
    public Lead_Management__c leadObj{get;set;}
    public String whereCondBU {get;set;}
    public List<Inquiry__c> lstInqSelected{get;set;}
    // Added by Monali v4.0 START
    public String otherAttachmentBody { get; set; }
    public String otherAttachmentName { get; set; }
    public String additionalAttachmentBody { get; set; }
    public String additionalAttachmentName { get; set; }
    public String ccfAttachmentBody { get; set; }
    public String ccfAttachmentName { get; set; }
    public Attachment attachmentObj ; 
    public String campaignId { get; set; } // 5.0
    // Added by Monali v4.0 END

    public LeadManagementController () {
        campaignId = ''; // 5.0
        showBuyerDetails = false;
        lstInqSelected = new List<Inquiry__c>();
        whereCondBU = ' (';
        buyerList = new List<Buyer__c>();
        showBuyerPanel = false;
        leadObj = new Lead_Management__c();
        //Getting Active status values for booking units
        List<Booking_Unit_Active_Status__c> csActiveValues = Booking_Unit_Active_Status__c.getall().values();
        for(Booking_Unit_Active_Status__c cs : csActiveValues) {
            whereCondBU += ' Registration_Status__c = \\\'' + cs.Status_Value__c + '\\\' OR ';
        }
        whereCondBU = whereCondBU.removeEnd('OR ');
        whereCondBU += ' )';
        // showNewInq = false;

        // Added by Monali v4.0 27/05/2018  START 
        otherAttachmentBody = '';
        otherAttachmentName = '';
        additionalAttachmentBody = '';
        additionalAttachmentName = '';
        ccfAttachmentBody = '';
        ccfAttachmentName = '';
        // Added by Monali v4.0 27/05/2018  END

    }

    public void getBuyers() {
        showMessage = false;
        buyerList = new List<Buyer__c>();
        selectedBuyerObj = new Buyer__c();
        showBuyerDetails = false;
        selectedInquiry = '';
        lstInqSelected = new List<Inquiry__c>();
        //leadObj = new Lead_Management__c();
        if(String.isNotBlank(selectedbookingUnit)) {
            //Get Booking Id
            List<Booking_unit__c> BU = new List<Booking_unit__c>();
            //Getting Active status values for booking units
            List<String> lstValues = new List<String>();
            List<Booking_Unit_Active_Status__c> csActiveValues = Booking_Unit_Active_Status__c.getall().values();
            for(Booking_Unit_Active_Status__c cs : csActiveValues) {
                lstValues.add(cs.Status_Value__c);
            }
            BU = [SELECT Id, 
                         Booking__c,
                         Registration_ID__c,
                         Unit_Name__c
                    FROM Booking_unit__c
                   WHERE Unit_Name__C =: selectedbookingUnit
                     AND Registration_Status__c IN: lstValues
                  LIMIT 1];

            if(BU.size() > 0) {
                buyerList = [SELECT Id, First_Name__c,Name,
                                    Last_Name__c, Inquiry__c, Inquiry__r.name, Inquiry__r.RecordType.Name 
                            FROM Buyer__c
                            WHERE Booking__c =: BU[0].Booking__c];
                if(buyerList.size() ==0) {
                    showMessage = true;
                    msg= 'No buyers associated with the selected booking unit';
                    msgType = 'error';

                } else{
                    leadObj.Booking_Unit__c = BU[0].Id;
                    leadObj.Booking_Unit_Name__c = BU[0].Unit_Name__c;
                    leadObj.Booking_Unit_Registration_Id__c = BU[0].Registration_ID__c;
                }
            }
            else {
                showMessage = true;
                msg= 'There is no active booking against the unit selected';
                msgType = 'error';
            }
        }
        else {
            showMessage = true;
            msg= 'Please select booking unit';
            msgType = 'error';
        }
    }

    public Pagereference getBuyerDetails() {
        showMessage = false;

        if(String.isBlank(selectedBuyer) || String.isBlank(selectedInquiry)) {
            buyerList = new List<Buyer__c>();
            selectedBuyerObj = new Buyer__c();
            showBuyerDetails = false;
            lstInqSelected = new List<Inquiry__c>();
            //leadObj = new Lead_Management__c();
            showMessage = true;
            msg= 'Please select buyer and inquiry';
            msgType = 'error';
            return NULL;
        } 

        try{
            //Get selected inquiry
            if(String.isNotBlank(selectedInquiry)) {
                lstInqSelected = [SELECT Id,Campaign__c,
                                      Campaign__r.Campaign_Name__c,
                                      Campaign__r.RecordTypeId,
                                      Campaign__r.Active__c,
                                      OwnerId,
                                      Campaign_Name__c,
                                      First_Name__c,
                                      Last_Name__c,
                                      Email__c,
                                      Inquiry_Created_Date__c,
                                      HOD__c,
                                      HOS__c,
                                      Owner.Name
                              FROM Inquiry__c
                              WHERE Name =: selectedInquiry LIMIT 1];
            }
            showMessage = false;
            //Get buyer details
            if(String.isNotBlank(selectedBuyer)) {
                selectedBuyerObj = [SELECT Id,Booking__c,Inquiry__c,
                                         First_name__c,
                                         Last_Name__c,
                                         Inquiry__r.Name,
                                         Inquiry__r.Inquiry_Source__c,
                                         Email__c
                                     FROM Buyer__c
                                     WHERE id =: selectedBuyer];
                if(selectedBuyerObj != NULL) {
                    showBuyerDetails = true;
                }
            }
        } catch(Exception excp) {
            showMessage = true;
            msg= 'Error getting the details: '+excp.getMessage()+ ': '+excp.getLineNumber();
            msgType = 'error';
        }
        return null;
    }

    public Pagereference save() {
        showMessage = false;
        String campaignName='', relatedCampaignOnBookingUnit='', inquirySrc;
        Boolean sameBuyerDetails,relatedInqOwnerSame,PCPartOfActiveRoadshow;
        sameBuyerDetails = false;
        relatedInqOwnerSame = false;
        PCPartOfActiveRoadshow = false;

        List<Inquiry__c> lstInq = new List<Inquiry__c>();
        lstRules = new list<Lead_Admin_Console__mdt>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        List<String> lstBUCampaignNames = new List<String>();
        System.debug('======selectedBuyerObj==========='+selectedBuyerObj);

        //Id roadshowCampaignRecordTypeId = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Roadshows').getRecordTypeId();
        if(selectedBuyerObj != NULL && String.isNotBlank(selectedBuyerObj.Inquiry__c)) {
        //Get Buyer related inquiry
            lstInq = [ SELECT Id,
                              Campaign__c,
                              Campaign__r.Campaign_Name__c,
                              Campaign__r.RecordTypeId,
                              Campaign__r.Active__c,
                              OwnerId,
                              First_Name__c,
                              Last_Name__c,
                              Email__c,
                              Inquiry_Source__c,
                              Inquiry_Created_Date__c,
                              HOD__c,
                              HOS__c,
                              Owner.Name
                         FROM Inquiry__c
                        WHERE Id =: selectedBuyerObj.Inquiry__c];
        }
        System.debug('======lstInq==========='+lstInq);
        System.debug('======lstInqSelected==========='+lstInqSelected);

        List<Attachment> lstAttachment = new List<Attachment>();
        leadObj.Buyer__c = selectedBuyerObj.Id;
        if(lstInqSelected.size() > 0 ){
            leadObj.New_Inquiry__c = lstInqSelected[0].Id!= null ? lstInqSelected[0].Id : null;
            leadObj.New_Inquiry_Campaign_Name__c = lstInqSelected[0].Campaign__r.Campaign_Name__c != '' ? lstInqSelected[0].Campaign__r.Campaign_Name__c : '';
            leadObj.New_Inquiry_Created_Date__c = lstInqSelected[0].Inquiry_Created_Date__c != null ? lstInqSelected[0].Inquiry_Created_Date__c : null;
            leadObj.New_Inquiry_HOD__c = lstInqSelected[0].HOD__c != null ? lstInqSelected[0].HOD__c : null ;
            leadObj.New_Inquiry_HOS__c = lstInqSelected[0].HOS__c != null ? lstInqSelected[0].HOS__c : null ; 
            leadObj.New_Inquiry_Name__c = (lstInqSelected[0].First_Name__c != null ? lstInqSelected[0].First_Name__c : '' ) + (lstInqSelected[0].Last_Name__c != null ? lstInqSelected[0].Last_Name__c : '' );
            leadObj.New_Inquiry_Owner__c = lstInqSelected[0].OwnerId != null ? lstInqSelected[0].OwnerId : null ;
        }
        
        if(lstInq.size() > 0 ){
            leadObj.Old_Inquiry__c = lstInq[0].Id != null ? lstInq[0].Id : null;
            leadObj.Old_Inquiry_Campaign_Name__c = lstInq[0].Campaign__r.Campaign_Name__c != '' ? lstInq[0].Campaign__r.Campaign_Name__c : '';
            leadObj.Old_Inquiry_Created_Date__c = lstInq[0].Inquiry_Created_Date__c != null ? lstInq[0].Inquiry_Created_Date__c : null;
            leadObj.Old_Inquiry_HOD__c = lstInq[0].HOD__c != null ? lstInq[0].HOD__c : null ;
            leadObj.Old_Inquiry_HOS__c = lstInq[0].HOS__c != null ? lstInq[0].HOS__c : null ;
            leadObj.Old_Inquiry_Name__c = (lstInq[0].First_Name__c != null ? lstInq[0].First_Name__c : '' ) + (lstInq[0].Last_Name__c != null ? lstInq[0].Last_Name__c : '' );
            leadObj.Old_Inquiry_Owner__c = lstInq[0].OwnerId != null ? lstInq[0].OwnerId : null ;
        }
        
        leadObj.Status__c = 'Submitted';    

        try {
            insert leadObj;
            System.debug('======leadObj==========='+leadObj);

            // Added by Monali v4.0 START
            if(otherAttachmentName != '' && otherAttachmentName != ''){
                attachmentObj = createAttachment(leadObj, otherAttachmentName, otherAttachmentBody);
                lstAttachment.add(attachmentObj); 
            }

            if(additionalAttachmentName != '' && additionalAttachmentName != ''){
                attachmentObj = createAttachment(leadObj, additionalAttachmentName, additionalAttachmentBody);
                lstAttachment.add(attachmentObj); 
            }

            if(ccfAttachmentName != '' && ccfAttachmentName != ''){
                attachmentObj = createAttachment(leadObj, ccfAttachmentName, ccfAttachmentBody);
                lstAttachment.add(attachmentObj); 
            }

            insert lstAttachment;

            otherAttachmentBody = '';
            otherAttachmentName = '';
            additionalAttachmentBody = '';
            additionalAttachmentName = '';
            ccfAttachmentBody = '';
            ccfAttachmentName = '';

            // Added by Monali v4.0 END

            leadObj = [SELECT Name,
                              Lead_Update_Reason__c,
                              Replace_inquiry_on_buyer__c,Status__c,
                              Same_Buyer_Details__c
                              FROM Lead_Management__c WHERE Id =: leadObj.Id];
            System.debug('======leadObj===Record========'+leadObj);

            msgType = 'success';
            msg = 'Request has been submitted for approval: ' + leadObj.Name;
            showMessage = true;
            leadObj = new Lead_Management__c();
        }
        catch(System.DmlException excp) {
            msg = excp.getMessage() + excp.getLineNumber();
            msgType = 'error';
            showMessage = true;
            System.debug('===DmlException======'+excp);
        }
        return null;
    }

    // Added by Monali v4.0 START
    public Attachment createAttachment(Lead_Management__c leadObj, String attachmentName, String attachmentBody){
        attachmentObj = new Attachment(parentid = leadObj.Id, Name = extractName(attachmentName), Body = extractBody(attachmentBody));
        return attachmentObj;
    }
    // Added by Monali v4.0 END

    public void clear() {
        buyerList = new List<Buyer__c>();
        selectedBuyerObj = new Buyer__c();
        showBuyerDetails = false;
        showMessage = false;
        lstInqSelected = new List<Inquiry__c>();
        leadObj = new Lead_Management__c();
        selectedbookingUnit = NULL;
    }

    public pageReference cancel() {
        pageReference pg = new pageReference('/home/home.jsp');
        return pg;
    }

    // Added by Monali v4.0 START

    @Testvisible
    private Blob extractBody( String strBody ) {
        strBody = EncodingUtil.base64Decode( strBody ).toString();
        return EncodingUtil.base64Decode( strBody.substring( strBody.lastIndexOf(',')+1 ) );
    }

    @Testvisible
    private String extractName( String strName ) {
        return strName.substring( strName.lastIndexOf('\\')+1 ) ;
    }
    // Added by Monali v4.0 END
    
    //5.0
    @RemoteAction
    public static List < Campaign__c > searchCampaign (String pSearchKey) {
        String searchString = '%' + pSearchKey +  '%';
        return [SELECT Name, Campaign_name__c FROM Campaign__c WHERE Name LIKE :searchString OR Campaign_Name__c LIKE :searchString LIMIT 10];
    }

    

}