@RestResource(urlMapping='/surveyQuestions/')
global class SurveyQuestionsService {

    @HttpPost
    global static void getsurveyQuestions(){
        RestResponse res = RestContext.response;
        RestRequest req = RestContext.request;
        System.debug('Body: ' + (req.requestBody != null ? req.requestBody.toString() : ''));
        System.debug('Params: ' + req.params);
        String surveyType=req.params.get('surveyType');
        try{
            List<SurveyQuestionWrapper> allQuestions = new List<SurveyQuestionWrapper>();
            List<Survey_CRM__c> surveyList = [Select Id,Name,Hide_Survey_Name__c,Survey_Header__c,Thank_You_Text__c,Description__c from Survey_CRM__c
                                              where Is_Active__c = True and Type__c ='Portal' and End_Date__c > :System.Today() and Start_Date__c < :System.Today()];
    
    
            //surveyTaken = new Survey_Taken_CRM__c();
    
            if(surveyList != null && surveyList.size()>0){
    
    
                Survey_CRM__c survey = surveyList[0];
                allQuestions=getQuestion(survey);
                res.responseBody = Blob.valueOf(JSON.serialize(allQuestions));
                res.statusCode = 200;
    
                // Retrieve all necessary information to be displayed on the page
                
            }
        } catch (Exception e) {
            res.responseBody = Blob.valueOf(e.getMessage());
            res.statusCode = 500;
        }
        
    }
    
    global static List<SurveyQuestionWrapper> getQuestion(Survey_CRM__c survey) {

        List<Survey_Question_CRM__c> allQuestionsObject = [Select s.Type__c, s.Id, s.Survey__c, s.Required__c, s.Question__c, s.Order_Number_Displayed__c,
                                                       s.OrderNumber__c, s.Name, s.Choices__c,Choice_for_Additional_Textbox__c,Survey_Question__c,Additional_Description__c
                                                       From Survey_Question_CRM__c s
                                                       WHERE s.Survey__c =: survey.Id ORDER BY s.OrderNumber__c];
        List<SurveyQuestionWrapper> allQuestions = new List<SurveyQuestionWrapper>();
        List<Survey_Question_CRM__c> parentQuestions = new List<Survey_Question_CRM__c>();
        Map<Id,List<Survey_Question_CRM__c>> mapParentIdToChildQuestions = new Map<Id,List<Survey_Question_CRM__c>>();
        for (Survey_Question_CRM__c q : allQuestionsObject){
            if(q.Survey_Question__c == null){
                parentQuestions.add(q);
            }else{
                if(!mapParentIdToChildQuestions.containsKey(q.Survey_Question__c)){
                    mapParentIdToChildQuestions.put(q.Survey_Question__c,new List<Survey_Question_CRM__c>());
                }
                mapParentIdToChildQuestions.get(q.Survey_Question__c).add(q);
            }
        }

        for (Survey_Question_CRM__c q : parentQuestions){

            List<SurveyQuestionWrapper> subQuestionWrapper = new List<SurveyQuestionWrapper>();
            List<Survey_Question_CRM__c> subQuestions = mapParentIdToChildQuestions.get(q.Id);
            if(subQuestions != null && subQuestions.size() >0){
                for (Survey_Question_CRM__c question : subQuestions){
                    subQuestionWrapper.add(new SurveyQuestionWrapper(question,NULL));
                }
            }

            SurveyQuestionWrapper theQ = new SurveyQuestionWrapper(q,subQuestionWrapper);
            allQuestions.add(theQ);
        }
        return allQuestions;
    }
}