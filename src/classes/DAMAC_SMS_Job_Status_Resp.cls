//
//Json Object Class for SMS Bulk Status Response Parsing
//

public class DAMAC_SMS_Job_Status_Resp{
    public String Status;   //OK
    public cls_Data Data;
    public class cls_Data {
        public Integer BulkId;  //3032707
        public String BulkStatus;   //Finished
        public String DateAdded;    //2017-11-06T10:30:14.36
        public String DateToStart;  //2017-11-06T10:31:00
        public cls_IndividualResponses[] IndividualResponses;
        public String Message;  //Hi This is a Test SMS 00
        public String ReferenceName;    //DAMAC
        public String SenderName;   //DAMAC
        public Integer TotalNumbers;    //1
        public Integer TotalProcessed;  //1
        public Integer TotalValid;  //1
    }
    class cls_IndividualResponses {
        public Integer countryId;   //203
        public String creditsUsed;  //0.055000
        public String dlrStatus;    //Delivered
        public Integer messageLength;   //1
        public String mobileNo; //
        public Integer msgId;   //527743259
    }
    public static DAMAC_SMS_Job_Status_Resp parse(String json){
        return (DAMAC_SMS_Job_Status_Resp) System.JSON.deserialize(json, DAMAC_SMS_Job_Status_Resp.class);
    }

    
}