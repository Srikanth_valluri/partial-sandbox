@isTest
private class RoboCallServiceTest {

	@isTest
	static void getBodyAsBlobTest() {

		FmHttpCalloutMock.Response processRequestsReponse = new FmHttpCalloutMock.Response(200, 'Success',  '   {  '  + 
		'     "status": "success",  '  + 
		'     "authaccount": "430403a7-acaf-478f-b284-c85b809380a4",  '  + 
		'     "order_id": "16713135",  '  + 
		'     "response": "{\n  \"dnd_count\": \"0\",\n  \"message\": \"call added successfully\",\n  \"sivr_call_ids\": [{\"call_id\":\"7e50d35d-e4f9-45f0-867d-b1d549daacc3\"}]\n}",  '  + 
		'     "headers": {  '  + 
		'       "content-type": "application/json",  '  + 
		'       "x-api-key": "NRMPGEB9aCakjQqBzvRyC1ediT4iVH768rxsDMlo",  '  + 
		'       "authorization": "430403a7-acaf-478f-b284-c85b809380a4"  '  + 
		'     }  '  + 
		'  }  ' );

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response>{
            Label.Robo_Call_URL => processRequestsReponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));

        Test.startTest();
        Map<String, String> mapKeyVal = new Map<String, String>{'keyword'=>'Test','file'=>'csv'};
        RoboCallService.getBodyAsBlob(mapKeyVal);

        blob blobData = Blob.valueOf('Test');
        RoboCallService.processRoboRequest(blobData, label.Robo_Call_URL, label.Robocall_Authorization);
        Test.stopTest();
	}

}