/********************************************************************************************************************************
 * Description : Verify mobile and email for particular account
 *===============================================================================================================================
 * Ver      Date-DD/MM/YYYY     Author              Modification
 *===============================================================================================================================
 * 2.0      31/08/2020          Komal Shitole       Intial Draft
*********************************************************************************************************************************/
public with sharing class ContactDetailsVerification {

    /********************************************************************************************************************************
    * Method Name : getAccountDetailsOnInit
    * Description : method to get details for all mobile and email field values from account
    * Return Type : List<fieldWrapper>
    * Parameter(s): Account Id
    ********************************************************************************************************************************/
        @AuraEnabled
        public static List<FieldWrapper> getAccountDetailsOnInit(String recordId) {
            Account account = new Account();
            GetPhoneAndEmailFieldAccount__mdt getPhoneAndEmailFieldAccountList;
            Account acc;
            List<FieldWrapper> fieldWrapperList = new List<FieldWrapper>();
            List<String> phoneFieldNameList = new List<String>();
            List<String> countryCodeFieldNameList = new List<String>();
            String accountId;
            if(recordId.startsWithIgnoreCase('001')) {
                acc = [Select Id,
                              RecordType.DeveloperName
                       FROM   Account
                       WHERE  ID = : recordId];
            }
            else {
                Calling_List__c objCalling = [Select Id,
                                                 Account__c
                                          FROM   Calling_List__c
                                          WHERE  Id = : recordId ];
                if(String.isNotBlank(objCalling.Account__c)) {
                    acc = [Select Id,
                                RecordType.DeveloperName
                        FROM   Account
                        WHERE  Id = : objCalling.Account__c];
                }
            }
            if(acc != null) {
                getPhoneAndEmailFieldAccountList=[SELECT Id,
                                                    Email_Field_Name__c,
                                                    Phone_Field_Name__c,
                                                    Account_Record_Types__c
                                            FROM GetPhoneAndEmailFieldAccount__mdt
                                            WHERE DeveloperName =: acc.RecordType.DeveloperName];
            }
            if(getPhoneAndEmailFieldAccountList != null){
                for(String strObj : getPhoneAndEmailFieldAccountList.Phone_Field_Name__c.split(',')) {
                    if(String.isNotBlank(strObj)) {
                        System.debug('strObj'+strObj);
                        phoneFieldNameList.add(strObj.split('-')[0].trim());
                        if(strObj.split('-')[1].trim() != 'blank') {
                            countryCodeFieldNameList.add(strObj.split('-')[1].trim());
                        }
                    }
                }
                String phoneField = String.join(phoneFieldNameList,',');
                String countryCodeField = String.join(countryCodeFieldNameList,',');
                if(String.isNotBlank(acc.Id)) {
                    accountId = acc.Id;
                }
                if(String.isNotBlank(accountId)) {
                    String query='SELECT Id,'
                    +phoneField+','
                    +countryCodeField+','
                    +getPhoneAndEmailFieldAccountList.Email_Field_Name__c+','
                    +'Name,'
                    +'Party_ID__c'+
                    ' FROM Account';
                    query+=' WHERE'+' Id =: accountId';
                    account = Database.query(query);
                    Map<String, Schema.SObjectField> mapOfFieldNameToLabel = getLabels();
                    FieldWrapper objField;
                    for(String str : getPhoneAndEmailFieldAccountList.Email_Field_Name__c.split(',')) {
                        objField = new FieldWrapper();
                        objField.fieldName = str;
                        objField.maskedValue = String.isNotBlank(String.valueOf(account.get(str)))?String.valueOf(account.get(str)).left(4)+'xxxxxx'+String.valueOf(account.get(str)).right(4):'';
                        objField.value = String.valueOf(account.get(str));
                        objField.isVerified = false;
                        objField.label = mapOfFieldNameToLabel.get(str).getDescribe().getLabel();
                        objField.isEmail = true;
                        fieldWrapperList.add(objField);
                    }
                    for(String str : getPhoneAndEmailFieldAccountList.Phone_Field_Name__c.split(',')) {
                        String countryCodeFieldName;
                        if(String.isNotBlank(str)) {
                            String phoneFieldName = str.split('-')[0].trim();
                            if(str.split('-')[1].trim() != 'blank') {
                                countryCodeFieldName = str.split('-')[1].trim();
                            }
                            objField = new FieldWrapper();
                            objField.fieldName = str;
                           
                            objField.isVerified = false;
                            objField.label = mapOfFieldNameToLabel.get(phoneFieldName).getDescribe().getLabel();
                            objField.isEmail= false;
                            if(countryCodeFieldName != null) {
                                objField.countryCode = String.valueOf(account.get(countryCodeFieldName));
                            }
                            system.debug('countryCodeFieldName>>>>>>'+countryCodeFieldName);
                            system.debug('phoneFieldName>>>>>>'+phoneFieldName);
                            if(countryCodeFieldName != null && phoneFieldName != null && String.isNotBlank(String.valueOf(account.get(countryCodeFieldName))) && String.isNotBlank(String.valueOf(account.get(phoneFieldName)))) {
                                objField.value = String.valueOf(account.get(countryCodeFieldName)).substringAfter(':').trim()+String.valueOf(account.get(phoneFieldName));
                            } else if( phoneFieldName != null){
                                objField.value = String.valueOf(account.get(phoneFieldName));
                            }
                            objField.maskedValue = String.isNotBlank(objField.value)?String.valueOf(objField.value).left(7)+'xxxxxx'+String.valueOf(objField.value).right(4):'';
                            fieldWrapperList.add(objField);
                    }
                }
            }
        }
            return fieldWrapperList;
        }
    /********************************************************************************************************************************
    * Method Name : getAccountdetails
    * Description : method to mark tick against the values which matches with inputs
    * Return Type : List<ContactDetailsVerification.fieldWrapper>
    * Parameter(s): Phone,Email,FieldList
    ********************************************************************************************************************************/
        @AuraEnabled
        public static List<FieldWrapper> getAccountdetails(String phone, String email, String accountRecordId){
            system.debug('phone>>'+phone);
            system.debug('email>>'+email);
            system.debug('accountRecordId>>>>'+accountRecordId);
            String phoneNumber;
            if(phone.contains('+')) {
                phoneNumber = phone.remove('+');
            }
            else {
                phoneNumber = phone;
            }
            //List<ContactDetailsVerification.FieldWrapper> fieldList = (List<ContactDetailsVerification.FieldWrapper>)JSON.deserialize(fieldListString, List<ContactDetailsVerification.FieldWrapper>.class);
            List<FieldWrapper> fieldWrapperList = new List<FieldWrapper>();
            List<FieldWrapper> fieldList;
            if(String.isNotBlank(accountRecordId)) {
                fieldList = getAccountDetailsOnInit(accountRecordId);
            }
            system.debug('fieldList>>>'+fieldList);
            for(FieldWrapper objField : fieldList) {
                if (objField.value != null) {
                    system.debug('objFieldValue>>>>>'+objField.value);
                    if(
                        (!String.isEmpty(phoneNumber) && objField.isEmail == false && objField.value.contains(phoneNumber)) ||
                        (!String.isEmpty(email) && objField.isEmail == true && objField.value == email)
                    ) {
                        objField.isVerified = true;
                        fieldWrapperList.add(objField);
                        system.debug('objField>>>>>'+objField);
                    }
                    else {
                        objField.isVerified = false;
                    }
                }
            }
            system.debug('fieldWrapperList'+fieldWrapperList);
            return fieldWrapperList;
        }
    /********************************************************************************************************************************
    * Method Name : getLabels
    * Description : method to get map of field name to label from Account
    * Return Type : Map<String, Schema.SObjectField>
    * Parameter(s): None
    ********************************************************************************************************************************/
        private static Map<String, Schema.SObjectField> getLabels() {
            Map<String,String> mapOfFieldNameToLabel = new Map<String,String>();
            Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            Schema.SObjectType accountSchema = schemaMap.get('Account');
            Map<String, Schema.SObjectField> fieldMap = accountSchema.getDescribe().fields.getMap();
            return fieldMap;
        }
    
        public class FieldWrapper  {
            @AuraEnabled
            public String fieldName;
            @AuraEnabled
            public String value;
            @AuraEnabled
            public String maskedValue;
            @AuraEnabled
            public Boolean isVerified;
            @AuraEnabled
            public string label;
            @AuraEnabled
            public Boolean isEmail;
            @AuraEnabled
            public String countryCode;
            @AuraEnabled
            public String countryCodeMobile;
        }
    }