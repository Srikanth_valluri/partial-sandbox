/******************************************************************************
* Description - Test class developed for EarlyHandoverMQService1
*
* Version            Date            Author                    Description
* 1.0                17/12/17        Naresh Kaneriya (Accely)   Initial Draft
********************************************************************************/
@isTest
public class EarlyHandoverMQService1Test{
    
    
    static EarlyHandoverMQService1.HandoverHttpSoap11Endpoint  obj = new EarlyHandoverMQService1.HandoverHttpSoap11Endpoint();
    Static String resp ;

  @isTest static void TestMethod1(){
     Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,EarlyHandoverMQService1.UpdatePDCInformationResponse_element>();
        EarlyHandoverMQService1.UpdatePDCInformationResponse_element response1 = new EarlyHandoverMQService1.UpdatePDCInformationResponse_element();
        response1.return_x ='S'; 
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
        resp = obj.UpdatePDCInformation('P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM','Registrationid','Name','PDCAmount','ChequeDate','ChequeNumber','PaymentPlan','Type_x'); 
      System.assert(resp != null);  
     Test.stopTest();
  }
  
  
  
   @isTest static void TestMethod2(){
     Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,EarlyHandoverMQService1.generatePCCResponse_element>();
        EarlyHandoverMQService1.generatePCCResponse_element response1 = new EarlyHandoverMQService1.generatePCCResponse_element();
        response1.return_x ='S'; 
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
        resp = obj.generatePCC('P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM','RegistrationId','StageId','BatchName');
      System.assert(resp != null);  
     Test.stopTest();
  }
  
  
  
  @isTest static void TestMethod3(){
      
      List<EarlyHandoverMQService3.APPSXXDC_AOPT_PKG_WSX1843128X6X5> regTerms = new  List<EarlyHandoverMQService3.APPSXXDC_AOPT_PKG_WSX1843128X6X5>();
     Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,EarlyHandoverMQService1.EarlyHandoverPaymentPlanCreationResponse_element>();
        EarlyHandoverMQService1.EarlyHandoverPaymentPlanCreationResponse_element response1 = new EarlyHandoverMQService1.EarlyHandoverPaymentPlanCreationResponse_element();
        response1.return_x ='S'; 
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
        resp = obj.EarlyHandoverPaymentPlanCreation('P_REGISTRATION_ID','P_SR_NUMBER','P_SR_TYPE',regTerms);
      System.assert(resp != null);  
     Test.stopTest();
  }


   @isTest static void TestMethod4(){
      
      List<EarlyHandoverMQService3.APPSXXDC_AOPT_PKG_WSX1843128X6X5> regTerms = new  List<EarlyHandoverMQService3.APPSXXDC_AOPT_PKG_WSX1843128X6X5>();
     Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,EarlyHandoverMQService1.GetHandoverDetailsResponse_element>();
        EarlyHandoverMQService1.GetHandoverDetailsResponse_element response1 = new EarlyHandoverMQService1.GetHandoverDetailsResponse_element();
        response1.return_x ='S'; 
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
        resp = obj.GetHandoverDetails('P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM','Registrationid');
      System.assert(resp != null);  
     Test.stopTest();
  }


    @isTest static void TestMethod5(){
      
      List<EarlyHandoverMQService3.APPSXXDC_AOPT_PKG_WSX1843128X6X5> regTerms = new  List<EarlyHandoverMQService3.APPSXXDC_AOPT_PKG_WSX1843128X6X5>();
     Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,EarlyHandoverMQService1.EHOCRFRequestGenerationResponse_element>();
        EarlyHandoverMQService1.EHOCRFRequestGenerationResponse_element response1 = new EarlyHandoverMQService1.EHOCRFRequestGenerationResponse_element();
        response1.return_x ='S'; 
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
        resp = obj.EHOCRFRequestGeneration('P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM','Registrationid','ProjectName','AptNumber','CustomerName','JointBuyerName','OtherPropertieswithDamac','SubjectOfRequest');
      System.assert(resp != null);  
     Test.stopTest();
  }
  
  
  
   @isTest static void TestMethod6(){
     Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,EarlyHandoverMQService1.LetterofDischargeResponse_element>();
        EarlyHandoverMQService1.LetterofDischargeResponse_element  response1 = new EarlyHandoverMQService1.LetterofDischargeResponse_element ();
        response1.return_x ='S'; 
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
        resp = obj.LetterofDischarge('P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM','RegistrationId','StageId','BatchName');
      System.assert(resp != null);  
     Test.stopTest();
  }
  
  
  
   @isTest static void TestMethod7(){
     Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,EarlyHandoverMQService1.GetFinalInvoiceResponse_element>();
        EarlyHandoverMQService1.GetFinalInvoiceResponse_element response1 = new EarlyHandoverMQService1.GetFinalInvoiceResponse_element();
        response1.return_x ='S'; 
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
        resp = obj.GetFinalInvoice('P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM','Registrationid');
      System.assert(resp != null);  
     Test.stopTest();
  }
  
  
  
  @isTest static void TestMethod8(){
      
      List<EarlyHandoverMQService2.APPSXXDC_PROCESS_SERX1794747X1X5> regTerms =  new List<EarlyHandoverMQService2.APPSXXDC_PROCESS_SERX1794747X1X5>();
     Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,EarlyHandoverMQService1.UPDATE_EARLY_HO_FLAGResponse_element>();
        EarlyHandoverMQService1.UPDATE_EARLY_HO_FLAGResponse_element response1 = new EarlyHandoverMQService1.UPDATE_EARLY_HO_FLAGResponse_element();
        response1.return_x ='S'; 
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
        resp = obj.UPDATE_EARLY_HO_FLAG('P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM',regTerms);
      System.assert(resp != null);  
     Test.stopTest();
  }


   @isTest static void TestMethod9(){
      
      List<EarlyHandoverMQService3.APPSXXDC_AOPT_PKG_WSX1843128X6X5> regTerms = new  List<EarlyHandoverMQService3.APPSXXDC_AOPT_PKG_WSX1843128X6X5>();
     Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,EarlyHandoverMQService1.UpdateBookingUnitResponse_element>();
        EarlyHandoverMQService1.UpdateBookingUnitResponse_element response1 = new EarlyHandoverMQService1.UpdateBookingUnitResponse_element();
        response1.return_x ='S'; 
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
        resp = obj.UpdateBookingUnit('P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM','Registrationid','HandoverFlag','PDCAmEarlyHandoverFlagount','RentalPoolFlag');
      System.assert(resp != null);  
     Test.stopTest();
  }


    @isTest static void TestMethod10(){
      
     
     Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,EarlyHandoverMQService1.getBuildingRERAPercentageResponse_element>();
        EarlyHandoverMQService1.getBuildingRERAPercentageResponse_element response1 = new EarlyHandoverMQService1.getBuildingRERAPercentageResponse_element();
        response1.return_x ='S'; 
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
        resp = obj.getBuildingRERAPercentage('P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM','BuildingId');
      System.assert(resp != null);  
     Test.stopTest(); 
  }
  
  
   @isTest static void TestMethod11(){
      
     
     Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,EarlyHandoverMQService1.GenerateKeyReleaseFormResponse_element>();
        EarlyHandoverMQService1.GenerateKeyReleaseFormResponse_element response1 = new EarlyHandoverMQService1.GenerateKeyReleaseFormResponse_element();
        response1.return_x ='S'; 
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
        resp = obj.GenerateKeyReleaseForm('P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM','RegistrationId','StageId','BatchName');
      System.assert(resp != null);  
     Test.stopTest(); 
  }


}