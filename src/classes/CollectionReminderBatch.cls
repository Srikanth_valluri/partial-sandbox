/************************************************************************************************
 * Description: Batch to send Dues email reminders                                              *
 ===============================================================================================*
 * Ver      Date            Author                  Modification                                *
 *==============================================================================================*
 * 1.0      31/7/2019       Arjun Khatri            Initial Draft                               *
*************************************************************************************************/
public class CollectionReminderBatch implements Database.Batchable<sObject>
                                                                , Database.AllowsCallouts
                                                                , Database.Stateful {
    private String query;
    List<Collection_Reminders__c> lstRemindersToUpdate;
    List<EmailMessage> lstEmails;
    Set<String> setArabicNationality = new Set<String> {'saudi','jordanian','omani', 'uae'
                                                        , 'kuwaiti', 'qatari', 'lebanese', 'libyan'
                                                        , 'Kazak', 'egyption', 'egyptianiraqi', 'iranian'};
    Set<String> setChinesenationality = new Set<String> {'chinese', 'hongkong', 'vietnam', 'singaporean'
                                                        , 'republic of china', 'taiwanese'};
    public Database.Querylocator start( Database.BatchableContext bc ) {
        query = 'SELECT Id, InvoiceDetails__c, Email_Schedule__c, Email_Sent__c, Reg_Id__c, InvoiceDetails__r.Customer_Name__c, '
                + ' InvoiceDetails__r.Accounts__r.Nationality__c, InvoiceDetails__r.Accounts__r.Nationality__pc, '
                + ' InvoiceDetails__r.Accounts__r.isPersonAccount, InvoiceDetails__r.Accounts__r.Email__pc, '
                + ' InvoiceDetails__r.Accounts__r.Email__c, InvoiceDetails__r.Inv_Due__c, InvoiceDetails__r.Due_Date__c, '
                + ' InvoiceDetails__r.Accounts__r.Name, InvoiceDetails__r.BookingUnits__r.Unit_Name__c,InvoiceDetails__r.Unit_Active__c '
                + ' FROM Collection_Reminders__c '
                + ' WHERE Email_Sent__c = false '
                + ' AND InvoiceDetails__c != null '
                + ' AND InvoiceDetails__r.Accounts__c != null '
                + ' AND (InvoiceDetails__r.Accounts__r.Email__pc != null OR InvoiceDetails__r.Accounts__r.Email__c != Null) '
                + ' AND InvoiceDetails__r.Unit_Active__c =\'Active\'  ' 
                + ' AND Email_Triggering_Date__c = Today';
        System.debug('Query = ' + query);
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext bc, List<Collection_Reminders__c> collectionReminders ) {
        lstEmails = new List<EmailMessage>();
        lstRemindersToUpdate = new List<Collection_Reminders__c >();
        System.debug('collectionReminders ==> ' + collectionReminders);
        for(Collection_Reminders__c objCLRem : collectionReminders) {
            string nationality = objCLRem.InvoiceDetails__r.Accounts__r.Nationality__c != NULL 
                                ? objCLRem.InvoiceDetails__r.Accounts__r.Nationality__c 
                                : objCLRem.InvoiceDetails__r.Accounts__r.Nationality__pc != NULL
                                ? objCLRem.InvoiceDetails__r.Accounts__r.Nationality__pc
                                : 'Indian';
            
            
            if(setArabicNationality.contains(nationality.toLowerCase())) {
                nationality = 'UAE';
            }  
            else if(setChinesenationality.contains(nationality.toLowerCase())) {
                nationality = 'Chinese';
            }

            System.debug('nationality !!==' + nationality);
            //Get email template based on nationality and Email Schedule
            EmailTemplate emailTemplateObj;
            if(string.isNotBlank(nationality) && string.isNotBlank(objCLRem.Email_Schedule__c)) {
                emailTemplateObj
                    = ProcessEmailTemplateSelector.fetchEmailTemplateRecord(
                        ProcessEmailTemplateSelector.getEmailTemplateName(
                            'Invoice Process - ' + objCLRem.Email_Schedule__c
                            , nationality
                        )
                    );
            }

            System.debug('emailTemplateObj == ' + emailTemplateObj);
            
            if(emailTemplateObj != null) {
                
                //Call method to send reminders through send grid
                sendReminders(emailTemplateObj, objCLRem);
            }
            
        }//END FOR LOOP
        
        system.debug('lstEmails = '+ lstEmails );
        system.debug('lstRemindersToUpdate= '+ lstRemindersToUpdate);
        if(lstEmails != null && lstEmails.size() > 0) {
            
            insert lstEmails;
        }

        if(lstRemindersToUpdate != null && lstRemindersToUpdate.size() > 0) {
            update lstRemindersToUpdate;
        }
    }

/****************************************************************************************************
Method Name     : sendReminders                                                                     *
Description     : Send reminders through send grid                                                  *
Date            : 31/7/2019                                                                         *
Parameter(s)    : EmailTemplate Instance and Collection_Reminders__c Instance                       *
Return          : void                                                                              *
*****************************************************************************************************/  
    public void sendReminders(EmailTemplate emailTemplateObj, Collection_Reminders__c objCLRem) {
        String toAddress = '', fromAddress,replyToAddress = '', contentType, contentValue,contentBody = '';
        String subject, strAccountId, strDPIId, bccAddress ='', strCCAddress;
        
        strAccountId = objCLRem.InvoiceDetails__r.Accounts__c;
        strDPIId = objCLRem.InvoiceDetails__c;

        // If Person Acount
        if(objCLRem.InvoiceDetails__r.Accounts__r.isPersonAccount
        && objCLRem.InvoiceDetails__r.Accounts__r.Email__pc != null) {
            toAddress = objCLRem.InvoiceDetails__r.Accounts__r.Email__pc;
        }

        //If business account
        else if(objCLRem.InvoiceDetails__r.Accounts__r.Email__c != null){
            toAddress = objCLRem.InvoiceDetails__r.Accounts__r.Email__c;
        }
        System.debug('toAddress = ' + toAddress);

        fromAddress =  Label.AT_Your_Service;
        bccAddress = Label.Sf_Copy_Bcc_Mail_Id;
        contentType = 'text/html';
        strCCAddress = '';
        if(toAddress != '' && emailTemplateObj != null) {
            
            if(emailTemplateObj.body != NULL){
                    contentBody =  emailTemplateObj.body;
            }
            if(emailTemplateObj.htmlValue != NULL){
                contentValue = emailTemplateObj.htmlValue;
            }
            if(string.isblank(contentValue)){
                contentValue='No HTML Body';
            }
            subject = emailTemplateObj.Subject != NULL ? emailTemplateObj.Subject : 'DAMAC Dues Reminder';
            List<Attachment> lstAttach = new List<Attachment>();
            lstAttach = getSoa(objCLRem.Reg_Id__c);

            String customerName, dueAmount, unitName;
            Date paymentDate;
            
            customerName = objCLRem.InvoiceDetails__r.Customer_Name__c != null ? objCLRem.InvoiceDetails__r.Customer_Name__c : objCLRem.InvoiceDetails__r.Accounts__r.Name;
            
            if(objCLRem.InvoiceDetails__r.Inv_Due__c != null)
                dueAmount = GenericUtility.doFormatting(Decimal.valueOf(objCLRem.InvoiceDetails__r.Inv_Due__c), 2, ',', '.');
            paymentDate = objCLRem.InvoiceDetails__r.Due_Date__c;
            
            if(objCLRem.InvoiceDetails__r.BookingUnits__c != null )
                UnitName = objCLRem.InvoiceDetails__r.BookingUnits__r.Unit_Name__c;
            
            if(customerName != null && dueAmount != null && paymentDate != null) {
                customerName = GenericUtility.getCamelCase(customerName);
                contentValue = contentValue.replace('{CustomerName}', customerName);
                contentValue = contentValue.replace('{DueAmount}', dueAmount);
                contentValue = contentValue.replace('{UnitName}', unitName);
                contentValue = contentValue.replace('{PaymentDate}', String.valueOf(paymentDate));
                
                if(contentValue.contains(']]>')) {
                    system.debug('Contains ]]>');
                    contentValue.replace(']]>', '');
                } 
                contentBody = contentBody.replace('{CustomerName}', customerName);
                contentBody = contentBody.replace('{DueAmount}', dueAmount);
                contentBody = contentBody.replace('{UnitName}', unitName);
                contentBody = contentBody.replace('{PaymentDate}', String.valueOf(paymentDate));
            
                // Callout to sendgrid to send an email
                SendGridEmailService.SendGridResponse objSendGridResponse =
                    SendGridEmailService.sendEmailService(
                        toAddress
                        , ''
                        , strCCAddress
                        , ''
                        , bccAddress
                        , ''
                        , subject
                        , ''
                        , fromAddress
                        , ''
                        , replyToAddress
                        , ''
                        , contentType
                        , contentValue
                        , ''
                        , lstAttach
                    );

                System.debug('objSendGridResponse == ' + objSendGridResponse);
                String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;
                if( responseStatus == 'Accepted' ) {
                    EmailMessage mail = new EmailMessage();
                    mail.Subject = subject;
                    mail.MessageDate = System.Today();
                    mail.Status = '3';//'Sent';
                    mail.RelatedToId = strAccountId;
                    mail.Account__c  = strAccountId;
                    mail.Type__c = 'DP Invoice';
                    mail.ToAddress = toAddress;
                    mail.FromAddress = fromAddress;
                    mail.TextBody = contentBody; //contentValue.replaceAll('\\<.*?\\>', '');
                    mail.Sent_By_Sendgrid__c = true;
                    mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                    mail.DP_Invoices__c = strDPIId;
                    mail.CcAddress = strCCAddress;
                    mail.BccAddress = bccAddress;
                    lstEmails.add(mail);
                    system.debug('Mail obj == ' + mail);

                    objCLRem.Email_Sent__c = true;
                    lstRemindersToUpdate.add(objCLRem);
                }//End Email Activity Creation
                
            }//End Payment details null check if
        }//End emailTemplateObj IF

        FmIpmsRestCoffeeServices.updateCustomSetting = true;
        FmIpmsRestCoffeeServices.updateBearerToken();
    }
    
/****************************************************************************************************
Method Name     : getSoa                                                                            *
Description     : To get Statement Of Account (SOA)                                                 *
Date            : 1/8/2019                                                                          *
Parameter(s)    : Registration Id                                                                   *
Return          : List of Attachments                                                               *
*****************************************************************************************************/
    public List<Attachment> getSoa(String regId) {
        FmIpmsRestCoffeeServices.updateCustomSetting = false;
        String responseUrl;
        List<Attachment> lstAttach = new List<Attachment>();

        try {
            if(String.isNotBlank(regId))
                responseUrl = FmIpmsRestCoffeeServices.getUnitSoa(regId);
                System.debug('responseUrl is: '+responseUrl);
        }
        catch(Exception e) {
            System.debug('FmIpmsRestCoffeeServices Exception : '+ e.getMessage());
        }

        
        if(String.isNotBlank(responseUrl)) {
            String accessToken = FmIpmsRestCoffeeServices.accessToken;
            HttpRequest req = new HttpRequest();
            req.setEndpoint(responseUrl);
            req.setHeader('Accept', 'application/json');
            req.setMethod('GET');
            req.setHeader('Authorization','Bearer' + accessToken);
            req.setTimeout(120000);
            HttpResponse response = new Http().send(req);
            System.debug('Status Code = ' + response.getStatusCode());
            System.debug('Pdf Body = ' + response.getBodyAsBlob());
            
            if(response.getStatusCode() == 200 && response.getBodyAsBlob() != null) {
                Attachment attach = new Attachment();
                attach.contentType = 'application/pdf';
                attach.name = 'Unit SOA '+regId+'.pdf';
                attach.body = response.getBodyAsBlob();
                lstAttach.add(attach);
            }
            /*
            PageReference pageRef = new PageReference(responseUrl);
            Attachment att = new Attachment();
            att.Body = pageRef.getContentAsPDF();
            att.Name = 'Unit SOA ' + regId + '.pdf';
            lstAttach.add(att);*/
        }
        return lstAttach; 
    }

    public void finish( Database.BatchableContext bc ) {
    }
}