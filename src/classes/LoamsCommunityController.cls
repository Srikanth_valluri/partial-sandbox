public without sharing class LoamsCommunityController {

    public static final String GATEWAY_NAME;
    static {
        String siteBaseUrl = Site.getBaseUrl();
        siteBaseUrl = String.isBlank(siteBaseUrl) ? '' : siteBaseUrl;
        GATEWAY_NAME = siteBaseUrl.contains('customers.damacproperties.com')
                        ? 'LOAMS Portal Customers Domain' : 'LOAMS Portal';
    }

    public String               view                {get; set;}
    public Id                   customerAccountId   {get; set;}
    public Boolean              isOwner             {get; set;}
    public LiveAgentSettings__c liveAgentSettings   {get; set;}
    public String               pageTitle           {get; set;}


    public LoamsCommunityController() {
        initializeVariables();
        fetchPageParams();
        view = String.isBlank(view) ? 'home' : view.toLowerCase();
        setPageTitle();
    }

    private void initializeVariables() {
        customerAccountId = CustomerCommunityUtils.customerAccountId;
        isOwner = FmcUtils.isOwner();
        liveAgentSettings = LiveAgentSettings__c.getInstance('FM Portal');
        if (liveAgentSettings == NULL) {
            liveAgentSettings = new LiveAgentSettings__c(
                Name                    = 'FM Portal',
                DeploymentId__c         = '',
                ButtonId__c             = '',
                DeploymentScriptUrl__c  = '',
                ChatInitUrl__c          = ''
            );
        }
    }

    private Map<String, String> fetchPageParams() {
        Map<String, String> params = ApexPages.currentPage().getParameters();
        view = params.get('view');
        return params;
    }

    public PageReference authenticateUser() {
        if ('Guest'.equalsIgnoreCase(UserInfo.getUserType())) {
            return Network.forwardToAuthPage(ApexPages.currentPage().getUrl());
        } else {
            return null;
        }
    }

    public String getFullPhotoUrl() {
        return CustomerCommunityUtils.getFullPhotoUrl();
    }

    private void setPageTitle() {
        pageTitle = 'Community Portal - ';
        String suffix = (new Map<String, String> {
            ''                          =>  'Dashboard',
            'home'                      =>  'Dashboard',
            'profile'                   =>  'My Profile',
            'unitdetails'               =>  'Unit Details',
            'casedetails'               =>  'Case Details',
            'invoices'                  =>  'View Outstanding Invoices',
            'generatereceipt'           =>  'View Receipts',
            'valueaddedservices'        =>  'Value Added Services',
            'violationnotices'          =>  'Violation Notices',
            'notices'                   =>  'Notices',
            'newsletters'               =>  'Newsletters',
            'factsheets'                =>  'Factsheets',
            'forms'                     =>  'Forms',
            'faq'                       =>  'FAQs',
            'photogallery'              =>  'Photo Gallery',
            'suggestions'               =>  'Suggestions',
            'contacts'                  =>  'Contacts',
            'leaseoutunit'              =>  'Rent out Unit',
            'requestleasedunit'         =>  'Lease a Unit',
            'requestleasedparking'      =>  'Leased Parking',
            'requestunitmodification'   =>  'Modify Unit',
            'bookamenity'               =>  'Book an Amenity Request',
            'referfriend'               =>  'Refer A Friend',
            'newappointment'            =>  'Schedule Appointment With Property Manager',
            'servicechargeenquiry'      =>  'Service Charge Enquiry',
            'proofofpayment'            =>  'Proof of Payment',
            'movein'                    =>  'Move In',
            'moveout'                   =>  'Move Out',
            'tenantrenewal'             =>  'Tenant Renewal',
            'helpdeskrequest'           =>  'Raise Helpdesk Request',
            'viewhelpdeskrequests'      =>  'View Helpdesk Requests',
            'servicecharge'             =>  'View Service Charge and Statement',
            'vacationnotice'            =>  'Vacation Notice',
            'cocd'                      =>  'Change of Details',
            'purchaseadditionalparking' =>  'Purchase Additional Parking',
            'tenantregistration'        =>  'Tenant Registration',
            'bookanamenity'             =>  'Book An Amenity',
            'requestaccesscard'         =>  'Request Access Card',
            'nocforfitoutalterations'   =>  'Request NOC for Fit Out/Alterations',
            'applyworkpermit'           =>  'Apply for Work Permits',
            'asbuiltdrawings'           =>  'Request for As-built drawings'
        }).get(view);

        if (String.isBlank(suffix)) {
            suffix = view;
        }
        pageTitle += suffix;
    }

    @RemoteAction
    public static List<NotificationService.Notification> getNotifications(){
        NotificationService.Notification request = new NotificationService.Notification();
        request.accountId = CustomerCommunityUtils.customerAccountId;
        return FmNotificationService.getNotifications(request);
    }

    @RemoteAction
    public static list<NotificationService.Notification> markRead(List<NotificationService.Notification> requestList){
        return FmNotificationService.markRead(requestList);
    }

    @RemoteAction
    public static List<Sobject> globalSearch(String searchString) {
        if (String.isBlank(searchString)) {
            return NULL;
        }
        searchString = '%' + searchString + '%';
        List<Sobject> results = new List<Sobject>();
        results.addAll( (List<Sobject>) [
            SELECT  Id, Name, RecordTypeId, RecordType.Name, Status__c
            FROM    FM_Case__c
            WHERE   Name LIKE :searchString
                AND (Account__c = :CustomerCommunityUtils.customerAccountId
                    OR CreatedById = :UserInfo.getUserId())
        ]);
        results.addAll( (List<Sobject>) [
            SELECT  Id, Name, Unit_Name__c
            FROM    Booking_Unit__c
            WHERE   Unit_Name__c LIKE :searchString
                AND (Handover_Flag__c = 'Y' OR Early_Handover__c = true)
                AND Registration_Status__c IN :Booking_Unit_Active_Status__c.getAll().keySet()
                AND (Booking__r.Account__c = :CustomerCommunityUtils.customerAccountId
                    OR Tenant__c = :CustomerCommunityUtils.customerAccountId)
        ]);
        return results;
    }
}