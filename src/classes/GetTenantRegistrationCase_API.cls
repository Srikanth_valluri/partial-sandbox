/**********************************************************************************************************************
Description: This API is used for getting the Tenant Registration Cases
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
2.0     |   04-01-2021      | Shubham Suryawanshi | Added few more query parameters in FM Case SOQL query
***********************************************************************************************************************/
@RestResource(urlMapping='/getTenantRegistrationCase/*')
global class GetTenantRegistrationCase_API {

    public static final String NO_FMCASES = 'Please enter valid access code';
    public static final String NO_BU_FOUND = 'The entered access code is not valid';
    public static final String SUBMITTED_CASE = 'Given case is already submitted';
    public static final String SUCCESS = 'Successful';
    public static List<String> invalidStatusForCaseReturning = new List<String>{'Submitted', 'Tenant Registered; Awaiting Move in confirmation' , 'Closed'};
    public static String responseMessage;
    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        //200 => 'OK',
        //400 => 'Bad Request',
        //401 => 'Unauthorized',
        //500 => 'Internal Server Error' 
        1 => 'Success',
        2 => 'Info',/*As per Azure bug : 1628*/
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };

    @HttpPost 
    global static FinalReturnWrapper getTenantRegCaseFromId() {

        FinalReturnWrapper retunResponse = new FinalReturnWrapper();
        cls_meta_data objMeta = new cls_meta_data();
        cls_data objData = new cls_data();
        //FM_Case__c objFMCase = new FM_Case__c();

        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);

        if(!r.params.containsKey('accessCode')) {

            objMeta.message = 'Missing parameter : accessCode';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
           //objMeta.is_success = false;

            retunResponse.meta_data = objMeta;
            return retunResponse;

        }
        /*Currently BU Checking is kept Optional - Coming Soon....*/ /*Commented block for BU check 1*/
        else if(!r.params.containsKey('bookingUnitId')) {
            objMeta.message = 'Missing parameter : bookingUnitId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
           //objMeta.is_success = false;
            retunResponse.meta_data = objMeta;
            return retunResponse;
        }
        else if(r.params.containsKey('accessCode') && String.isBlank(r.params.get('accessCode'))) {
            objMeta.message = 'Missing parameter value: accessCode';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
            //objMeta.is_success = false;

            retunResponse.meta_data = objMeta;
            return retunResponse;
        }
        /*Currently BU Checking is kept Optional - Coming Soon....*/ /*Commented block for BU check 1*/
        else if(r.params.containsKey('bookingUnitId') && String.isBlank(r.params.get('bookingUnitId'))) { /*Commented block for BU check 2*/
            objMeta.message = 'Missing parameter value: bookingUnitId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
            //objMeta.is_success = false;

            retunResponse.meta_data = objMeta;
            return retunResponse;
        }
        

        if(String.isNotBlank(r.params.get('accessCode'))) {
            
            List<FM_Case__c> lstFmCase = getFMCase(r.params.get('accessCode'));

            //Logic to check if case is -'Submitted','Tenant Registered; Awaiting Move in confirmation' , 'Closed'
            if( lstFmCase != null && !lstFmCase.isEmpty() && invalidStatusForCaseReturning.contains(lstFmCase[0].Status__c)) {
            	System.debug('lstFmCase[0].Status__c ::' + lstFmCase[0].Status__c);
                
                objMeta.message = SUBMITTED_CASE;
                objMeta.status_code = 2;
                objMeta.title = mapStatusCode.get(2);
                objMeta.developer_message = null;
                //objMeta.is_success = false;

                retunResponse.meta_data = objMeta;
                return retunResponse;
            }

            FmCaseWrapper objFMCase  = fetchTenantRegFmCase(lstFmCase);

            responseMessage = objFMCase != null ? SUCCESS : NO_FMCASES;
            /*Currently BU Checking is kept Optional - Coming Soon....*/ /*Commented block for BU check 1*/
            if(objFMCase != null) {
                System.debug('bookingUnitId:: '+r.params.get('bookingUnitId')); /*Commented block for BU check 3*/
                System.debug('objFMCase.booking_unit_id:: '+objFMCase.booking_unit_id);
                System.debug('bookingUnitId length:: '+r.params.get('bookingUnitId').length());

                if(r.params.get('bookingUnitId').length() >= 15) {
                    responseMessage = objFMCase.booking_unit_id.substring(0,15) == r.params.get('bookingUnitId').substring(0,15) ? SUCCESS : NO_BU_FOUND;
                }
                else{
                    responseMessage = NO_BU_FOUND;
                }

                //responseMessage = objFMCase.booking_unit_id.contains(r.params.get('bookingUnitId')) ? SUCCESS : NO_BU_FOUND;
            }

            System.debug('responseMessage:: ' + responseMessage);
            //objMeta.is_success = objMeta.status_code == 1 ? true : false;
            objMeta.message = responseMessage;
            objMeta.status_code = responseMessage == SUCCESS ? 1 : 3;
            objMeta.title = mapStatusCode.get(objMeta.status_code);
            objMeta.developer_message = null;

            objData.fm_case = responseMessage == SUCCESS ? objFMCase : null;

            retunResponse.data = objData;
            retunResponse.meta_data = objMeta;

            System.debug('retunResponse:: ' + retunResponse);
            
        }

        return retunResponse;

    }

    //public static FmCaseWrapper fetchTenantRegFmCase(String fmCaseId) {
      public static FmCaseWrapper fetchTenantRegFmCase(List<FM_Case__c> lstFmCase) {

        FmCaseWrapper objFmCaseWrapper = new FmCaseWrapper();

        //List<FM_Case__c> lstFmCase = getFMCase(fmCaseId);

        System.debug('lstFmCase:: ' + lstFmCase);  
        
        if(lstFmCase.size() > 0) {

            objFmCaseWrapper.id = lstFmCase[0].id;
            objFmCaseWrapper.fm_case_number = lstFmCase[0].Name;
            objFmCaseWrapper.booking_unit_id = lstFmCase[0].Booking_Unit__c;
            objFmCaseWrapper.first_name = lstFmCase[0].First_Name__c;
            objFmCaseWrapper.last_name = lstFmCase[0].Last_Name__c;
            objFmCaseWrapper.gender = lstFmCase[0].Gender__c;
            objFmCaseWrapper.date_of_birth = lstFmCase[0].Date_of_Birth__c != null? Datetime.newInstance(lstFmCase[0].Date_of_Birth__c.year(), lstFmCase[0].Date_of_Birth__c.month(), lstFmCase[0].Date_of_Birth__c.day()).format('yyyy-MM-dd') : '';

            objFmCaseWrapper.nationality = lstFmCase[0].Nationality__c;
            objFmCaseWrapper.passport_number = lstFmCase[0].Passport_Number__c;
            objFmCaseWrapper.passport_expiry_date = lstFmCase[0].Expiry_Date__c != null ? Datetime.newInstance(lstFmCase[0].Expiry_Date__c.year(), lstFmCase[0].Expiry_Date__c.month(), lstFmCase[0].Expiry_Date__c.day()).format('yyyy-MM-dd') : '';
            objFmCaseWrapper.emirates_id = lstFmCase[0].Emirates_Id__c;
            objFmCaseWrapper.mobile_code = lstFmCase[0].Mobile_Country_Code__c;
            objFmCaseWrapper.mobile_number = lstFmCase[0].Mobile_no__c;
            objFmCaseWrapper.email_address = lstFmCase[0].Tenant_Email__c;
            objFmCaseWrapper.no_of_adults = lstFmCase[0].No_of_Adults__c;
            objFmCaseWrapper.no_of_children = lstFmCase[0].No_of_Children__c;
            objFmCaseWrapper.lease_start_date = lstFmCase[0].Start_Date__c != null ? Datetime.newInstance(lstFmCase[0].Start_Date__c.year(), lstFmCase[0].Start_Date__c.month(), lstFmCase[0].Start_Date__c.day()).format('yyyy-MM-dd') : '';
            objFmCaseWrapper.lease_end_date = lstFmCase[0].End_Date__c != null ? Datetime.newInstance(lstFmCase[0].End_Date__c.year(), lstFmCase[0].End_Date__c.month(), lstFmCase[0].End_Date__c.day()).format('yyyy-MM-dd') : '';
            objFmCaseWrapper.ejari_number = lstFmCase[0].Ejari_Number__c;
            objFmCaseWrapper.move_in_date = lstFmCase[0].Move_in_date__c != null ? Datetime.newInstance(lstFmCase[0].Move_in_date__c.year(), lstFmCase[0].Move_in_date__c.month(), lstFmCase[0].Move_in_date__c.day()).format('yyyy-MM-dd') : '';
            objFmCaseWrapper.move_in_type = lstFmCase[0].Move_in_Type__c;
            objFmCaseWrapper.moving_company_name = lstFmCase[0].Company__c;
            objFmCaseWrapper.moving_contractor_name = lstFmCase[0].Contractor__c;
            objFmCaseWrapper.moving_contractor_phone = lstFmCase[0].Mobile_no_contractor__c;

            objFmCaseWrapper.is_person_with_spl_needs = lstFmCase[0].isPersonWithSpecialNeeds__c;
            objFmCaseWrapper.is_having_pets = lstFmCase[0].isHavingPets__c;


            List<cls_emergency_contact> lstEmer = new List<cls_emergency_contact>();
            if(lstFmCase[0].Emergency_Contact_Details__r.size() > 0) {

                for(FM_Additional_Detail__c objFmEmer : lstFmCase[0].Emergency_Contact_Details__r) {
                    cls_emergency_contact objEmer = new cls_emergency_contact();

                    objEmer.id = objFmEmer.id;
                    objEmer.emergency_full_name = objFmEmer.Name__c;
                    objEmer.emergency_relationship = objFmEmer.Relationship__c;
                    objEmer.emergency_email_address = objFmEmer.Email__c;
                    objEmer.emergency_mobile_number = objFmEmer.Mobile__c;

                    lstEmer.add(objEmer);
                }
            }

            List<cls_vehicle_details> lstVehicle = new List<cls_vehicle_details>();
            System.debug('lstFmCase---->'+lstFmCase[0]);
              System.debug('lstFmCase[0].Vehicle_Details__r.size()---->'+lstFmCase[0].Vehicle_Details__r.size());
            if(lstFmCase[0].Vehicle_Details__r.size() > 0) {
            
                for(FM_Additional_Detail__c objFmVeh : lstFmCase[0].Vehicle_Details__r) {
                    cls_vehicle_details objVehicle = new cls_vehicle_details();

                    objVehicle.id = objFmVeh.id;
                    objVehicle.vehicle_number = objFmVeh.Vehicle_Number__c;
                    objVehicle.vehicle_make = objFmVeh.Vehicle_Make_Model__c;

                    lstVehicle.add(objVehicle);
                }

               
            }


            List<cls_additional_members> lstResidents = new List<cls_additional_members>();
            if(lstFmCase[0].Resident_Details__r.size() > 0) {

                for(FM_Additional_Detail__c obj : lstFmCase[0].Resident_Details__r) {
                    cls_additional_members objResident = new cls_additional_members();

                    objResident.id = obj.id;
                    list<String> names = String.isNotBlank(obj.Name__c) ? obj.Name__c.split(' ') : new List<String>();
                    if(names.size() > 0) {
                        objResident.member_first_name = names[0];
                    }
                    if(names.size() == 2) {
                        objResident.member_last_name = names[1];
                    }

                    objResident.member_gender = obj.Gender__c;
                    objResident.member_date_of_birth = obj.Date_of_Birth__c != null ? Datetime.newInstance(obj.Date_of_Birth__c.year(), obj.Date_of_Birth__c.month(), obj.Date_of_Birth__c.day()).format('yyyy-MM-dd') : '';
                    objResident.member_nationality = obj.Nationality__c;
                    objResident.member_passport_number = obj.Passport_Number__c;
                    objResident.member_emirates_id = obj.Identification_Number__c;
                    objResident.member_mobile_number = obj.Mobile__c;
                    objResident.member_email_address = obj.Email__c;

                    lstResidents.add(objResident);

                }
                
            }

            
            list<cls_attachment> lstAttachments = new list<cls_attachment>();

            if(lstFmCase[0].Documents__r.size() > 0) {
                System.debug('lstFmCase[0].Documents__r'+lstFmCase[0].Documents__r);
                for(SR_Attachments__c obj : lstFmCase[0].Documents__r) {
                    cls_attachment objAttach = new cls_attachment();
                    objAttach.id = obj.id;
                    objAttach.name = obj.Name;
                    objAttach.file_extension = obj.Type__c;
                    objAttach.attachment_url = obj.Attachment_URL__c;

                    lstAttachments.add(objAttach);
                }
            }

            System.debug('lstEmer:: ' + lstEmer);
            System.debug('lstVehicle:: ' + lstVehicle);
            System.debug('lstAttachments:: ' + lstAttachments);
            System.debug('lstResidents:: ' + lstResidents);

            //objFmCaseWrapper.emergency_contact_details = objEmer;
            //objFmCaseWrapper.vehicle_details = objVehicle;
            objFmCaseWrapper.emergency_contact_details = lstEmer;
            objFmCaseWrapper.vehicle_details = lstVehicle;

            objFmCaseWrapper.attachments = lstAttachments;
            objFmCaseWrapper.additional_members = lstResidents;

        }
        else {
            objFmCaseWrapper = null;
        }         

        System.debug('objFmCaseWrapper: ' + objFmCaseWrapper);

        return objFmCaseWrapper;        
    }

    // This method is also getting called from - GetSRListForLoggedInUser_API class
    public static List<FM_Case__c> getFMCase(String fmCaseId) {

        List<FM_Case__c> lstFmCase = [SELECT id 
                                           , Name
                                           , Status__c
                                           , Tenant__c
                                           , Booking_Unit__c
                                           , First_Name__c
                                           , Last_Name__c
                                           , Gender__c
                                           , Date_of_Birth__c
                                           , Nationality__c
                                           , Passport_Number__c
                                           , Expiry_Date__c
                                           , Emirates_Id__c
                                           , Mobile_Country_Code__c
                                           , Mobile_no__c
                                           , Tenant_Email__c
                                           , No_of_Adults__c
                                           , No_of_Children__c
                                           , Start_Date__c
                                           , End_Date__c
                                           , Ejari_Number__c
                                           , Move_in_date__c
                                           , Move_in_Type__c
                                           , Company__c
                                           , Contractor__c
                                           , Mobile_no_contractor__c
                                           , Submission_Date__c
                                           , isHavingPets__c
                                           , isPersonWithSpecialNeeds__c
                                           , Expected_move_out_date__c
                                           , Move_in_move_out_type__c
                                           , Reason_for_Move_Out__c
                                           , Booking_Unit__r.Unit_Name__c
                                           , Type_of_Access_Card__c
                                           , Access_Card_Permission__c
                                           , Request_Type_DeveloperName__c
                                           , New_Access_Card_Fee__c
                                           , Access_Card_Replacement_Fee__c
                                           , Approval_Status__c
                                           , Unit_Name__c
                                           , Access_Card_Payment_Receipt_No__c
                                           , Access_Card_Payment_Receipt_URL__c
                                           , Type_of_Alteration__c
                                           , Type_of_NOC__c
                                           , Description__c
                                           , Person_To_Collect__c
                                           , Contact_person_contractor__c
                                           , Email_2__c
                                           , Mobile_Country_Code_2__c
                                           , FM_Manager_Email__c
                                           , Admin__c
                                           , Booking_Unit__r.Property_Name__c
                                           , Approving_Authorities__c
                                           , Booking_Unit__r.Property_City__c
                                           , FitOut_Alterations_Payment_Receipt_No__c
                                           , Request_Type__c
                                           , Noc_for_Fitout_Alterations_Fee__c
                                           , Suggestion_Type__c
                                           , Suggestion_Sub_Type__c

                                           , (SELECT id
                                                  , Name__c
                                                  , Relationship__c
                                                  , Email__c
                                                  , Mobile__c 
                                              FROM Emergency_Contact_Details__r 
                                              WHERE RecordType.Name = 'Emergency Contact') 
                                           , (SELECT id
                                                   , Vehicle_Number__c
                                                   , Vehicle_Make_Model__c 
                                                   , Vehicle_Sticker_No__c
                                                   , Parking_Slot_Number__c
                                              FROM Vehicle_Details__r 
                                              WHERE RecordType.Name = 'Vehicle' )
                                           , (SELECT id
                                                   , Name__c
                                                   , Nationality__c
                                                   , Gender__c
                                                   , Date_of_Birth__c
                                                   , Passport_Number__c
                                                   , Identification_Number__c
                                                   , Mobile__c
                                                   , Email__c
                                              FROM Resident_Details__r
                                              WHERE RecordType.Name = 'Resident')  
                                           , (SELECT id
                                                   , Attachment_url__c
                                                   , Name 
                                                   , Type__c
                                              FROM Documents__r) 

                                     FROM FM_Case__c WHERE 
                                     id =: fmCaseId];

        System.debug('lstFmCase:: ' + lstFmCase);
        return lstFmCase;
    }

    
    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_data {
        public FMCaseWrapper fm_case ;
    }

    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message;   
    }

    public class FMCaseWrapper {
        public String id;
        public String fm_case_number;
        public String booking_unit_id;
        public String first_name;
        public String last_name;
        public String gender;
        public String date_of_birth;
        public String no_of_adults;
        public String no_of_children;
        public boolean is_person_with_spl_needs;
        public boolean is_having_pets;

        public String nationality;
        public String passport_number;
        public String passport_expiry_date;
        public String emirates_id;
        public String mobile_code;
        public String mobile_number;
        public String email_address;
        
        public String lease_start_date;
        public String lease_end_date;
        public String ejari_number;

        public String move_in_date;
        public String move_in_type;
        public String moving_company_name;
        public String moving_contractor_name;
        public String moving_contractor_phone;

        public cls_emergency_contact[] emergency_contact_details;
        public cls_vehicle_details[] vehicle_details;

        public cls_attachment[] attachments;
        public cls_additional_members[] additional_members;


    }

    public class cls_emergency_contact {

        public String id;
        public String emergency_full_name;
        public String emergency_relationship;
        public String emergency_email_address;
        //public String emer_countryCode;
        public String emergency_mobile_number;

    }

    public class cls_vehicle_details {

       public String id;
       public String vehicle_number;
       public String vehicle_make;
    }

    public class cls_additional_members {
        public String id;
        public String member_first_name;
        public String member_last_name;
        public String member_gender;
        public String member_date_of_birth;
        public String member_nationality;
        public String member_passport_number;
        //public String member_passport_expiry_date;
        public String member_emirates_id;
        //public String member_country_code;
        public String member_mobile_number;
        public String member_email_address;
    }

    public class cls_attachment {

        public String id;
        public String name;
        public String attachment_url;
        public String file_extension;

    }
}