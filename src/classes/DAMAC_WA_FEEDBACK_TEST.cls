@istest
public class DAMAC_WA_FEEDBACK_TEST{

    static testmethod void testM1(){
        User PCuser = new User(
         ProfileId = [SELECT Id FROM Profile WHERE Name = 'Property Consultant'].Id,
         LastName = 'last',
         Email = 'puser000@amamama.com',
         Username = 'puser000@amamama.com' + System.currentTimeMillis(),
         CompanyName = 'TEST',
         Title = 'title',
         Alias = 'alias',
         TimeZoneSidKey = 'America/Los_Angeles',
         EmailEncodingKey = 'UTF-8',
         LanguageLocaleKey = 'en_US',
         LocaleSidKey = 'en_US'
        );
        insert PCuser;
        
         List<String> inqSalesOffice = new List<String>();

        Schema.DescribeFieldResult fieldResult = Inquiry__c.Sales_Office__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for( Schema.PicklistEntry f : ple){
            inqSalesOffice.add(f.getValue());
        }
        system.debug('====Test class===inqSalesOffice=======' +inqSalesOffice); 

        
        List<QueueSObject> QueueList = [SELECT Queue.Id, 
                                               Queue.Name, 
                                               Queue.Type 
                                          FROM QueueSObject 
                                         WHERE Queue.Type ='Queue' 
                                           AND Queue.Name IN :inqSalesOffice];
                                           
       

        
        Id devRecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        Inquiry__c inq = new Inquiry__c();
        inq.Mobile_Phone_Encrypt__c = '05789088';
        inq.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Preferred_Language__c = 'English';
        inq.Inquiry_Status__c = 'New' ;
        inq.First_Name__c = 'Test First name';
        inq.Last_Name__c = 'Test Last name';
        inq.Email__c = 'test@gmail.com';
        inq.Inquiry_Source__c = 'Customer Referral';
        inq.Mobile_Phone__c = '456789';
        inq.RecordTypeId = devRecordTypeId;
        inq.Assigned_PC__c = PCuser.Id;
        inq.Inquiry_Status__c = 'Meeting Scheduled';
        inq.Sales_Office__c = 'AKOYA';
        inq.Meeting_Due_Date__c = system.now();
        insert inq;


        
        Office_Meeting__c  officeMeeting = new Office_Meeting__c();
        officeMeeting.Inquiry__c = inq.id;
        officeMeeting.Outcome__c = 'Show';
        officeMeeting.Status__c = 'Not Started';
        officeMeeting.Check_Out_Date__c = system.now();
        for(QueueSObject queue : QueueList){
            // system.debug('*****queueName*****' + queue);
            String queueName = queue.Queue.Name;
            if(queueName.equalsIgnoreCase(inq.Sales_Office__c)) {
                 officeMeeting.OwnerId = queue.Queue.Id;
            }
        }
        insert officeMeeting;
        list<id> ofmids = new list<id>();
        ofmids.add(officeMeeting.id);
        
        id parentflowRtypeId = Schema.SObjectType.Whats_App_Flows__c.getRecordTypeInfosByName().get('Parent Flow').getRecordTypeId();
        Whats_App_Flows__c parent = new Whats_App_Flows__c();
        parent.name = 'Feedback';
        parent.Menu__c = '1';
        //parent.keyword__c = 'hi';
        parent.start_this_on_confusion__c = true;
        parent.confusion_menu__c = '1';
        parent.recordtypeid= parentflowRtypeId;
        parent.end_message__c= 'asdsaas';
        insert parent;

        
        DAMAC_WA_FEEDBACK.sendFeedbackRequest(ofmids);
    
    }

}