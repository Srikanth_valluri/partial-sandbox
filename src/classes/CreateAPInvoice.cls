/****************************************************************************************
Description: Class to make Invoice generation service request for Service Charge Reset
             and Waiver SR
----------------------------------------------------------------------------------------*
Version     Date        Author              Description                                 *
1.0         16/05/19    Rohit Jahagirdar    Initial Draft                               *
2.0         11/07/2020  Akshata Anvekar     Method to form Service charge reset request *
*****************************************************************************************/
public without sharing class CreateAPInvoice {
    private static final String HANDOVER = 'Handover';
    private static final String FUND_TRANSFER_ACTIVE_UNITS = 'Fund Transfer Active Units';
    private static final String FM_ACCOUNT_TASK_SUBJECT = 'Allocate Fund Trf frm Damac';
    public static List<APInvoiceRequestWrapper> apInvoiceRequestWrapper;    
  	
    public class APInvoiceRequestWrapper{
        String partyId;
        String caseNumber;
        String caseId;
        String processName;
        String waiverAmount;
        String subject;
        String caseOwnerId;
        String dtStartDate;
        String dtEndDate;
        String taskOwnerEmail;
        String caseOwnerEmail;
        String taskId;
        String taskOwnerName;
    }
    
    /* Invocable method to send completed tasks
     * for invoice generation request
     * Input Parameters : Task List
     */
    @InvocableMethod
    public static void sendTasks(List<Task> lstTask){
        //System.debug('====>inside invokable'+lstTask);
        List<Id> lstSCTaskId = new List<Id>();
        List<Id> lstFundTrfTaskId = new List<Id>();
        List<Id> lstTaskId = new List<Id>();
        List<Id> lstHandoverTaskId = new List<Id>();
        Boolean isServiceChargeWaiver = false;
        
        // Service Charge Waiver request section
        for(Task objTask : lstTask){
            if(String.isNotBlank(objTask.Subject) && objTask.Subject == System.Label.Validate_Service_Charge_Waiver){
                lstTaskId.add(objTask.Id);
            }   
        }
        
        if(!lstTaskId.isEmpty()){
            invokeAPInvoiceAPI(lstTaskId, System.Label.Service_Charge_Waiver);
        }
        
        // Service Charge Reset request section
        for(Task objTask : lstTask){
            if(String.isNotBlank(objTask.Subject) && objTask.Subject == System.Label.Validate_Service_Charge_Reset){
                lstSCTaskId.add(objTask.Id);
            }   
        }
        
        if(!lstSCTaskId.isEmpty()){
            invokeAPInvoiceAPI(lstSCTaskId, System.Label.Service_Charge_Reset);
        }

        // Excess Fund Transfer for Handover
        for(Task objTask : lstTask){
            if(String.isNotBlank(objTask.Subject) && objTask.Subject == System.Label.Validate_Excess_Fund_Transfer && 
                objTask.Process_Name__c == HANDOVER){
                lstHandoverTaskId.add(objTask.Id);
            }   
        }
        
        if(!lstHandoverTaskId.isEmpty()){
            invokeAPInvoiceAPI(lstHandoverTaskId, HANDOVER);
        }
        
        // Excess Fund Transfer for Active Units
        for(Task objTask : lstTask){
            if(String.isNotBlank(objTask.Subject) && 
               objTask.Subject == System.Label.Validate_Excess_Fund_Transfer && 
              objTask.Process_Name__c == FUND_TRANSFER_ACTIVE_UNITS){
                lstFundTrfTaskId.add(objTask.Id);
            }   
        }
        
        if(!lstFundTrfTaskId.isEmpty()){
            invokeAPInvoiceAPI(lstFundTrfTaskId, FUND_TRANSFER_ACTIVE_UNITS);
        }
        
    }
    
    /* Method to form Invoice generation service request 
     * for Waiver SR
     * Input Parameters : Task List
     * Return Type : APInvoiceRequestWrapper
     */
    public static List<APInvoiceRequestWrapper> getSCWaiverRequest(List<Id> lstTaskId){
        apInvoiceRequestWrapper = new List<APInvoiceRequestWrapper>();
        
        APInvoiceRequestWrapper invoiceWrapperObj = new APInvoiceRequestWrapper();
        String strCaseId;    
            
            for(Task taskObj : [SELECT Id,
                                       Subject,
                                       Process_Name__c,
                                       WhatId,
                                	   Owner.Name, 
                                       Owner.Email
                                FROM Task
                                WHERE Id IN :lstTaskId
                                AND What.Type = 'Case']){
                 invoiceWrapperObj.processName = taskObj.Process_Name__c;
                 invoiceWrapperObj.subject = taskObj.Subject;  
                 invoiceWrapperObj.taskId = taskObj.Id;
                 invoiceWrapperObj.taskOwnerEmail = taskObj.Owner.Email;
                 invoiceWrapperObj.taskOwnerName = taskObj.Owner.Name;            
                 strCaseId = taskObj.WhatId;
            }
            
            for(Case caseObj : [SELECT Id,
                                       OwnerId,
                                       CaseNumber,
                                       Waiver_Amount_AED__c, 
                                       Registration_ID__c,
                                       RecordTypeName__c,
                                       Excess_Amount__c,
                                Owner.Email
                                FROM Case
                                WHERE Id =: strCaseId]){
                invoiceWrapperObj.caseNumber = '2-'+caseObj.CaseNumber;
                if(caseObj.RecordTypeName__c == HANDOVER){
                    invoiceWrapperObj.waiverAmount = (caseObj.Excess_Amount__c != NULL)?
                    String.valueOf(caseObj.Excess_Amount__c).removeEnd('.000'): NULL;
                }else{
                    invoiceWrapperObj.waiverAmount = (caseObj.Waiver_Amount_AED__c != NULL)?
                    String.valueOf(caseObj.Waiver_Amount_AED__c).removeEnd('.000'): NULL;
                }
                //invoiceWrapperObj.waiverAmount = String.valueOf(caseObj.Waiver_Amount_AED__c).removeEnd('.000');
                invoiceWrapperObj.caseId = caseObj.Id;
                invoiceWrapperObj.caseOwnerId = caseObj.OwnerId;
                invoiceWrapperObj.partyId = caseObj.Registration_ID__c;
                invoiceWrapperObj.caseOwnerEmail = caseObj.Owner.Email;
            }
            
            /*for(Account acc : [SELECT Party_ID__c 
                               FROM Account 
                               WHERE Id in (SELECT AccountId 
                                            FROM Case 
                                            WHERE Id =: strCaseId)]){
                invoiceWrapperObj.partyId = acc.Party_ID__c;                                    
            }*/
            apInvoiceRequestWrapper.add(invoiceWrapperObj);
            
            return apInvoiceRequestWrapper;
    }
    
    /* Method to form invoice generation 
     * request for Service Charge Reset SR
     * Input Parameters : Task List
     * Return Type : APInvoiceRequestWrapper
     */
    public static List<APInvoiceRequestWrapper> getSCResetRequest(List<Id> lstTaskId){
    apInvoiceRequestWrapper = new List<APInvoiceRequestWrapper>();
    Map<Id, Task> mapCaseIdToTask = new Map<Id, Task>();
       
        for(Task objTask : [Select Id, WhatId, Owner.Name, Owner.Email, Process_Name__c From Task Where Id IN :lstTaskId ]){
        	mapCaseIdToTask.put(objTask.WhatId,objTask);	    
        }
        for(Case objCase : [Select Id
                                 , CaseNumber
                                 , Party_ID__c
                                 , OwnerId
                            	 , Owner.Email
                                 , (Select Reset_Start_Date__c
                                         , Reset_End_Date__c
                                         , Amount_paid__c
                                         , Registration_ID__c
                                         , Excess_Amount_Refund_Liability_AED__c
                                      From SR_Booking_Units__r
                                     )
                             From Case
                            Where Id In :mapCaseIdToTask.keySet()]){
          for(SR_Booking_Unit__c objSRBooking : objCase.SR_Booking_Units__r){
            APInvoiceRequestWrapper objRequestWrapper = new APInvoiceRequestWrapper();
            //if(!objCase.SR_Booking_Units__r.isEmpty()){
                
                objRequestWrapper.dtStartDate = (objSRBooking.Reset_Start_Date__c != NULL)?
                                                converDateToString(objSRBooking.Reset_Start_Date__c) : '';
                objRequestWrapper.dtEndDate = (objSRBooking.Reset_End_Date__c != NULL)? 
                                                converDateToString(objSRBooking.Reset_End_Date__c): '';

                if(mapCaseIdToTask.get(objCase.Id).Process_Name__c == FUND_TRANSFER_ACTIVE_UNITS){
                    objRequestWrapper.waiverAmount = (objSRBooking.Excess_Amount_Refund_Liability_AED__c != NULL)?
                    String.valueOf(objSRBooking.Excess_Amount_Refund_Liability_AED__c).removeEnd('.000'): NULL;
                }else{
                    objRequestWrapper.waiverAmount = (objSRBooking.Amount_paid__c != NULL)?
                    String.valueOf(objSRBooking.Amount_paid__c).removeEnd('.000'): NULL;
                }
                
                objRequestWrapper.partyId = (objSRBooking.Registration_ID__c != NULL)?
                                            objSRBooking.Registration_ID__c: '';
                objRequestWrapper.caseNumber = '2-'+ objCase.CaseNumber;
                objRequestWrapper.processName = mapCaseIdToTask.get(objCase.Id).Process_Name__c;
                objRequestWrapper.caseId = String.isNotBlank(objCase.Id) ? objCase.Id : '';
                objRequestWrapper.caseOwnerId = String.isNotBlank(objCase.OwnerId) ?
                                                objCase.OwnerId : '';
              	objRequestWrapper.taskOwnerEmail = mapCaseIdToTask.get(objCase.Id).Owner.Email;
                objRequestWrapper.caseOwnerEmail = objCase.Owner.Email;
                objRequestWrapper.taskId = mapCaseIdToTask.get(objCase.Id).Id;
                objRequestWrapper.taskOwnerName = mapCaseIdToTask.get(objCase.Id).Owner.Name;
                apInvoiceRequestWrapper.add(objRequestWrapper);
            //}
            }
            
        }
        return apInvoiceRequestWrapper;
    }

    
    /* Method to convert date into valid 
     * string format for request
     */
    public static String converDateToString(Date objDate){
        DateTime objDateTime = DateTime.newInstanceGMT(objDate.year(),objDate.month(),objDate.day());
        return String.valueOf(objDateTime.format('d-MMM-yyyy'));
    }

    /* Method to get Org wide email address
     * to set to From Adress in Email
     * Input Parameters : No parameters
     * Return Type : String 
     */
    public static String getOrgAddress(){
        String strAddress = System.Label.From_Address_for_Invoice_generation_Failed_Email;
        List<OrgWideEmailAddress> listOgrAddress = [select Id
                                                    	 , Address 
                                                      from OrgWideEmailAddress 
                                                     where Address = :strAddress 
                                                     Limit 1];
        if(!listOgrAddress.isEmpty()){
            return listOgrAddress[0].Address; 
        }
        return '';
    }
    
    /* Method make Invoice generation service request 
     * for Service Charge Reset and Waiver SR
     * Input Parameters : Tasks List, Boolean identifier for Process
     */
    @future(callout=true)
    public static void invokeAPInvoiceAPI(List<Id> lstTaskId, String processName){
    List<APInvoiceRequestWrapper> apInvoiceRequestWrapper; 
        if(!lstTaskId.isEmpty() && 
        (processName == System.Label.Service_Charge_Reset || processName == FUND_TRANSFER_ACTIVE_UNITS)){
            apInvoiceRequestWrapper = getSCResetRequest(lstTaskId);
        }else if(!lstTaskId.isEmpty() && processName == System.Label.Service_Charge_Waiver || processName == HANDOVER){
            apInvoiceRequestWrapper = getSCWaiverRequest(lstTaskId);
        }

        Credentials_Details__c creds = Credentials_Details__c.getInstance( 'AP Invoice API' );
        
        if( creds!=null && !apInvoiceRequestWrapper.isEmpty()){
            String userName = creds.user_Name__c;
            String password = creds.password__c;
            String endPoint = creds.endPoint__c;
            List<Error_Log__c> listErrorLog = new List<Error_Log__c>();	
            List<Case> caseListToClose = new List<Case>();
            List<Task> taskListToInsert = new List<Task>();
            List<EmailMessage> emailMessageListToInsert = new List<EmailMessage>();
			
            for(Integer i=0; i<apInvoiceRequestWrapper.size(); i++){
                if(apInvoiceRequestWrapper[i].dtStartDate == null && apInvoiceRequestWrapper[i].dtEndDate == null){
                	apInvoiceRequestWrapper[i].dtStartDate = '';
                    apInvoiceRequestWrapper[i].dtEndDate = '';
            	}
                
                String requestBody = '{'+
                                        '  \"PROCESS_Input\": {'+
                                        '   \r\n\"RESTHeader": {'+
                                        '      \r\n\"Responsibility": "ONT_ICP_SUPER_USER",'+
                                        '      \r\n\"RespApplication": "ONT",'+
                                        '      \r\n\"SecurityGroup": "STANDARD",'+
                                        '      \r\n\"NLSLanguage": "AMERICAN"'+
                                        '    },'+
                                        '    \r\n\"InputParameters": {'+
                                        '      \r\n\"P_REQUEST_NUMBER": "A2232",'+
                                        '      \r\n\"P_SOURCE_SYSTEM": "SFDC",'+
                                        '      \r\n\"P_REQUEST_NAME": "CREATE_AP_INVOICE", '+
                                        '    \r\n\"P_REQUEST_MESSAGE": {'+
                                        '    \r\n\"P_REQUEST_MESSAGE_ITEM": ['+
                                        '    {'+
                                        '      \r\n\"PARAM_ID": "'+apInvoiceRequestWrapper[i].partyId+'",'+
                                        '      \r\n\"ATTRIBUTE1": "'+apInvoiceRequestWrapper[i].caseNumber+'",'+
                                        '      \r\n\"ATTRIBUTE2": "'+apInvoiceRequestWrapper[i].processName+'",'+
                                        '      \r\n\"ATTRIBUTE3": "'+apInvoiceRequestWrapper[i].waiverAmount+'",'+
                                        '      \r\n\"ATTRIBUTE4": "'+apInvoiceRequestWrapper[i].dtStartDate+'",'+
                                        '      \r\n\"ATTRIBUTE5": "'+apInvoiceRequestWrapper[i].dtEndDate+'"'+
                                        '    }'+
                                        '   ]'+
                                        '    }'+
                                        '  }'+
                                        '}'+
                                        '}';
                String headerValue = 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(userName + ':' + password));            
                HttpRequest request = new HttpRequest();
                request.setMethod('POST');
                request.setHeader('Accept', 'application/json');
                request.setHeader('Content-Type', 'application/json');
                request.setHeader('Accept-Language', 'en-US');
                request.setHeader('Authorization', headerValue);
                request.setBody(requestBody);
                request.setEndPoint(endPoint);
                request.setTimeout(120000);
                Http http = new Http();
                HttpResponse response = new HttpResponse();
                
                try{
                    response = http.send(request);
                    if( response != null && response.getBody() != null){
                        APInvoiceResponseWrapper responseObj = ( APInvoiceResponseWrapper )System.JSON.deserialize( response.getBody(), APInvoiceResponseWrapper.class );
                        
                        
                        for( X_RESPONSE_MESSAGE_ITEM objItem : responseObj.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM ){
                            Error_Log__c errorLog = new Error_Log__c();
                            errorLog.Error_Details__c = objItem.PROC_MESSAGE;
                            errorLog.Case__c = apInvoiceRequestWrapper[i].caseId;
                            errorLog.Process_Name__c = apInvoiceRequestWrapper[i].processName;
                            errorLog.OwnerId = apInvoiceRequestWrapper[i].caseOwnerId;
                            errorLog.Service_Request_Body__c = requestBody;
                            errorLog.Service_Response_Body__c = response.getBody();
                            errorLog.Is_Error__c = objItem.PROC_STATUS == 'E'? true : false;
                            listErrorLog.add(errorLog);
                            
                            if(objItem.PROC_STATUS == 'S'){
                                if(String.isNotBlank(apInvoiceRequestWrapper[i].caseId)){
                                    Case caseObj = new Case();
                                    caseObj.Id = apInvoiceRequestWrapper[i].caseId;
                                    if(!apInvoiceRequestWrapper[i].processName.equalsIgnoreCase(HANDOVER) &&
                                      !apInvoiceRequestWrapper[i].processName.equalsIgnoreCase(FUND_TRANSFER_ACTIVE_UNITS)){
                                        caseObj.Status = 'Closed';
                                    }
                                    if(apInvoiceRequestWrapper[i].processName.equalsIgnoreCase(FUND_TRANSFER_ACTIVE_UNITS)){
                                    	Task objTask = new Task();
                                        objTask.Status = 'Not Started';
                                        System.debug('Fund transfr');
                                        objTask.Process_Name__c = apInvoiceRequestWrapper[i].processName;
                                        objTask.Receipt_Number__c = String.isNotBlank(apInvoiceRequestWrapper[i].caseOwnerId) 
                                        ? apInvoiceRequestWrapper[i].caseOwnerId
                                        : '';
                                        objTask.OwnerId = String.isNotBlank(apInvoiceRequestWrapper[i].caseOwnerId) 
                                        ? apInvoiceRequestWrapper[i].caseOwnerId
                                        : '';
                                        objTask.WhatId = apInvoiceRequestWrapper[i].caseId;
                                        objTask.Subject = FM_ACCOUNT_TASK_SUBJECT;    
                                        objTask.ActivityDate = system.today().addDays(2);
                                        objTask.Assigned_User__c = 'FM Account';
                                        objTask.Priority = 'High';    
                                        
                                        if(!taskListToInsert.contains(objTask)){
                                    		taskListToInsert.add(objTask);
                                		}
                                    }
                                    
                                    
                                    caseListToClose.add(caseObj);

                                    if(apInvoiceRequestWrapper[i].processName.equalsIgnoreCase(FUND_TRANSFER_ACTIVE_UNITS) ||
                                        apInvoiceRequestWrapper[i].processName.equalsIgnoreCase(HANDOVER) ){
                                        String strFromAddress = getOrgAddress(); 
                                        EmailMessage mail = sendEmailToTaskOwner( apInvoiceRequestWrapper[i].taskOwnerEmail
                                                    , apInvoiceRequestWrapper[i].caseOwnerEmail
                                                    , strFromAddress
                                                    , apInvoiceRequestWrapper[i].caseId
                                                    , apInvoiceRequestWrapper[i].taskOwnerName                    
                                                    , apInvoiceRequestWrapper[i].caseNumber
                                                    , requestBody
                                                    , response.getBody()
                                                    , apInvoiceRequestWrapper[i].processName
                                                    , objItem.PROC_STATUS);
                                        emailMessageListToInsert.add(mail);
                                    }
                                    
                                }
                                
                            }
                            if(objItem.PROC_STATUS == 'E'){
                                Task objTask = new Task();
                                objTask.Id = apInvoiceRequestWrapper[i].taskId;
                                objTask.Status = 'Not Started';
                                
                                if(!taskListToInsert.contains(objTask)){
                                    taskListToInsert.add(objTask);
                                }
                                	
                               String strFromAddress = getOrgAddress(); 
                               EmailMessage mail = sendEmailToTaskOwner( apInvoiceRequestWrapper[i].taskOwnerEmail
                                                    , apInvoiceRequestWrapper[i].caseOwnerEmail
                                                    , strFromAddress
                                                    , apInvoiceRequestWrapper[i].caseId
                                                    , apInvoiceRequestWrapper[i].taskOwnerName                    
                                                    , apInvoiceRequestWrapper[i].caseNumber
                                                    , requestBody
                                                    , response.getBody()
                                                    , apInvoiceRequestWrapper[i].processName
                                                    , objItem.PROC_STATUS);
                               emailMessageListToInsert.add(mail);
                            }
                        }
                        
                    }
                }catch(Exception e){
                    System.debug('Exception-->' + e);
                } 
            }
                              
       
            if(Case.SObjectType.getDescribe().isAccessible() && Schema.SObjectType.Case.fields.Id.isAccessible() &&
               Schema.SObjectType.Case.fields.Status.isAccessible() && caseListToClose!=null && !caseListToClose.isEmpty()){
                 update caseListToClose;
            }
            
            if(listErrorLog!=null && !listErrorLog.isEmpty()){
                 insert listErrorLog;
            }
                            
            if(taskListToInsert!=null && !taskListToInsert.isEmpty()){
                 upsert taskListToInsert;
            }
            
            if(emailMessageListToInsert!=null && !emailMessageListToInsert.isEmpty()){
            	insert emailMessageListToInsert;
            }
        }
    }
    
    /* Method to generate Email body
     * for Task Owner 
     * Input Parameters : OwnerId, CaseNumber, Request Body, Response Body
     * Return Type : String 
     */
    public static String getEmailBody(String ownerId, String caseNumber, String request, String response, String status){
        String strEmailBody;
        if(status == 'E'){
            strEmailBody = 'Dear '+ownerId+' ,'+
            				  '\r\n We could not create the invoice for Case '+caseNumber+' in IPMS due to the below error '+
            				  '\r\n'+
            				  '\r\n Request : '+request+' ' +
            				  '\r\n'+
            				  '\r\n Response : '+response+' '+
            				  '\r\n'+
            				  '\r\n Please check or contact your Salesforce Admin to resolve the issue. '+
            				  '\r\n'+
            				  '\r\n Thank you.';
        }else if(status == 'S'){
            strEmailBody = 'Dear '+ownerId+' ,'+
            				  '\r\n Invoice created successfully for Case '+caseNumber+' in IPMS '+
            				  '\r\n Thank you.';
        }
        
        return strEmailBody;
    }
    
    /* Method to send Email
     * to Task Owner and create Email Messages record
     * Input Parameters : EmailToAddress, EmailCCAddress, EmailFromAddress, Case Id, Owner Id, Case Number, REST Request Body, 
     *                    REST Response Body, Process Name
     * Return Type : EmailMessage
     */
    public static EmailMessage sendEmailToTaskOwner(String strToAddress, String strCcAddress,String strFromAddress, Id caseId, String ownerId, 
                                                    String caseNumber, String requestBody, String responseBody, String processName, String status){
        String strEmailBody;
        String strSubject;
        if(status == 'E' && (processName == HANDOVER || processName == FUND_TRANSFER_ACTIVE_UNITS)){
            strEmailBody = getEmailBody(ownerId, caseNumber, requestBody, responseBody, status);
            strSubject = 'Invoice Creation Failed!! Case '+caseNumber+' - '+processName;
        } else if(status == 'S' && (processName == HANDOVER || processName == FUND_TRANSFER_ACTIVE_UNITS)){
            strEmailBody = getEmailBody(ownerId, caseNumber, requestBody, responseBody, status);
            strSubject = 'Invoice Created!! Case '+caseNumber+' - '+processName;
        }                                               
        
        SendGridEmailService.SendGridResponse emailResponse = SendGridEmailService.sendEmailService(  strToAddress
                                                                               , ''
                                                                               , strCcAddress
                                                                               , ''
                                                                               , ''
                                                                               , ''
                                                                               , strSubject
                                                                               , ''
                                                                               , strFromAddress
                                                                               , ''
                                                                               , ''
                                                                               , ''
                                                                               , 'Text'
                                                                               , strEmailBody
                                                                               , ''
                                                                               , new List<Attachment>());
        
        EmailMessage mail = new EmailMessage();
        mail.Subject = strSubject;
        mail.MessageDate = System.Today();
        mail.Status = '3';//'Sent';
        mail.RelatedToId = caseId;
        //mail.Account__c  = accountId;
        mail.ToAddress = strToAddress;
        mail.FromAddress = strFromAddress;
        mail.TextBody = strEmailBody;//contentValue.replaceAll('\\<.*?\\>', '');
        mail.Sent_By_Sendgrid__c = true;
        mail.SentGrid_MessageId__c = emailResponse.messageId;
        //mail.Booking_Unit__c = objBU.Id;
        mail.CcAddress = strCcAddress;
        //system.debug('Mail obj == ' + mail);
        //System.debug('Email Response'+emailResponse);
        
        return mail;
    }
    
    /*
        Wrapper for AP Invoice REST Response
    */
    public class APInvoiceResponseWrapper{       
            public OutputParameters OutputParameters; 
            }
        
            public class OutputParameters {  
                public String xmlns;
                public String xmlns_xsi;
                public X_RESPONSE_MESSAGE X_RESPONSE_MESSAGE;
                public String X_RETURN_STATUS;
                public String X_RETURN_MESSAGE;
            }
            
        
            public class X_RESPONSE_MESSAGE_ITEM {
                public String PARAM_ID;
                public String PROC_STATUS;
                public String PROC_MESSAGE;
                public String ATTRIBUTE1;
                public String ATTRIBUTE2;
                public String ATTRIBUTE3;
                public String ATTRIBUTE4;
            }
        
            public class X_RESPONSE_MESSAGE {
                public List<X_RESPONSE_MESSAGE_ITEM> X_RESPONSE_MESSAGE_ITEM;
            }
}