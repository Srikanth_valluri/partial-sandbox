@isTest
public class Invocable_RegisterAgents_Test {

    @isTest static void ccAgentRegistered() {
        
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test456', email='xyz1@email.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz1@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
            User pwUser = new User(alias = 'test456', email='xyz1@email.com',
                emailencodingkey='UTF-8', lastname='Paula Wehbeh', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='pw@email.com',UserRoleId = userRoleObj.Id);
                insert pwUser;

            List<Account_SR_Field_Mapping__c> lstAccSRFldMapping = new List<Account_SR_Field_Mapping__c>();
            lstAccSRFldMapping.add(new Account_SR_Field_Mapping__c(name = '1',Account_Field__c = 'Firstname',SR_Field__c='First_Name__c',Is_Person_Account__c = true));
            lstAccSRFldMapping.add(new Account_SR_Field_Mapping__c(name = '2',Account_Field__c = 'lastname',SR_Field__c='Last_Name__c',Is_Person_Account__c = true));
            lstAccSRFldMapping.add(new Account_SR_Field_Mapping__c(name = '3',Account_Field__c = 'Name',Is_Common_to_All_RT__c = true,SR_Field__c='Agency_Name__c',Is_Person_Account__c = false));
            lstAccSRFldMapping.add(new Account_SR_Field_Mapping__c(name = '4',Account_Field__c = 'Agency_Type__c',SR_Field__c='Agency_Type__c',Is_Person_Account__c = false));
            
            insert lstAccSRFldMapping;
			NSIBPM__Service_Request__c currSR = new NSIBPM__Service_Request__c();
            Account newAcc ;
            List<Account> accList = new List<Account>();
            for(Integer i=0;i<10;i++) 
            {
			    newAcc = new Account();
                newAcc.name = 'TestCCReg' + i;
                accList.add(newAcc);

            }
            insert accList;
            Account accObj = accList[0];
            currSR.NSIBPM__Customer__c = accList[0].Id;
            currSR.Bank_Name__c = 'XYZ';
            currSR.Agency_Name__c = 'abc';
			currSR.Country__c = 'United Arab Emirates';
            currSR.Agency_Type__c = 'Corporate';
            insert currSR;
            Contact newCon;
            List<Contact>conList = new List<Contact>();
            for(Integer i=0;i<accList.Size();i++){
                newCon = new Contact();
                newCon.FirstName = 'FirstName' + i;
                newCon.LastName = 'LastName' + i;
                newCon.Email = 'TestCCReg' + i + '@gmail.com';
                newCon.Accountid = accList[i].Id;
                conList.add(newCon);
            }
            insert conList;
            Map<id,contact> mpContact = new Map<id,contact>();
            for(contact cnct : conlist){
                mpContact.put(cnct.id,cnct);
            }
            Id accRecordTypeId   = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

            Account agency1 = InitialiseTestData.getCorporateAccount('Agency125');
            Account agency2 = InitialiseTestData.getCorporateAccount('Agency126');

            insert agency1;
            insert agency2;

            Account acc = new Account();
            acc.lastname = 'ltest';
            acc.RecordTypeId = accRecordTypeId ;
            acc.Blacklisted__c = true ;
            acc.Reason_for_Blacklisting__c = 'Agency Blacklisting unit test';
            insert acc ;

            Contact newCont = new Contact();
            newCon.FirstName = 'Test';
            newCont.LastName = 'Agency125';
            newCont.AccountId = agency1.Id ;
            insert newCont ;


            NSIBPM__SR_Steps__c srStep= new NSIBPM__SR_Steps__c();
            insert srStep;

            List<NSIBPM__Service_Request__c> SRList = InitialiseTestData.createTestServiceRequestRecords(new List<NSIBPM__Service_Request__c>{
                new NSIBPM__Service_Request__c(recordTypeId = InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Registration')),
                    new NSIBPM__Service_Request__c(recordTypeId = InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Registration'))});


            NSIBPM__Service_Request__c serviceRequestObject = SRList[0] ;
            serviceRequestObject.NSIBPM__Customer__c = agency1.Id ;
            serviceRequestObject.Agency_Name__c = 'test1 User' ;
            serviceRequestObject.Last_Name__c = 'test User' ;
            update serviceRequestObject ;

            NSIBPM__Service_Request__c serviceRequestObject1 = SRList[1] ;
            serviceRequestObject.NSIBPM__Customer__c = agency2.Id ;
            serviceRequestObject.Agency_Name__c = 'test1 User' ;
            serviceRequestObject.Last_Name__c = 'test1 User' ;
            serviceRequestObject.Agency_Type__c = 'Corporate';
            serviceRequestObject.OwnerId = UserInfo.getUserId();
            update serviceRequestObject ;

            Amendment__c newAmd;
            List<Amendment__c> amdList = new List<Amendment__c>();
            for(integer i=0;i<conList.Size();i++){
                newAmd =  new Amendment__c();
                newAmd.First_Name__c = 'testfn'+i;
                newAmd.Last_Name__c = 'testfn'+i;
                newAmd.Account__c = conList[i].AccountId;
                newAmd.Contact__c = conList[i].Id;
                if(i==0){
                    newAmd.Owner__c = true;
                    newAmd.Shareholding__c =100;
                }
                newAmd.Authorised_Signatory__c = true;
                newAmd.Agent_Representative__c =true;
                newAmd.Portal_Administrator__c = true;
                newAmd.Email__c ='TestCCReg'+i+'@test.com';
                newAmd.ID_Expiry_Date__c = System.Today().addDays(100);
                newAmd.ID_Type__c = 'Visa';
                newAmd.Service_Request__c = SRList[0].Id ;
                amdList.add(newAmd);
            }
            insert amdList;
            Map<String, Contact> testconMap = new Map<String, Contact>();
            Map<String, Amendment__c> testamdMap = new Map<String, Amendment__c>();

            for (Amendment__c amd :  amdList) {
                testconMap.put(amd.id, mpContact.get(amd.Contact__c));
                testamdMap.put(amd.id,amd);
            }



            List<New_Step__c> createStepList = new List<New_Step__c>();
            createStepList.add(new New_Step__c(Service_Request__c = SRList[0].id, Step_No__c  = 1, Step_Type__c = 'Agent Executive Review', Step_Status__c = 'Awaiting Additional Info'));
            createStepList.add(new New_Step__c(Service_Request__c = SRList[0].id, Step_No__c = 2, Step_Type__c  = 'Agreement Signing', Step_Status__c = 'More Info Updated'));
            insert createStepList;

            User portalUser1  = InitialiseTestData.getPortalUser('portalCCUser@test.com',newCont.Id);
            insert portalUser1 ;

            List<Id> stepIdList = new List<Id>();
            stepIdList.add(createStepList[0].Id);
            stepIdList.add(createStepList[1].Id);
            
            Agency_Owner__c agencyOwner = new Agency_Owner__c();
            agencyOwner.Name = 'Real_Estate_UAE';
            agencyOwner.User_Name__c = 'User 456';
            insert agencyOwner;
            
            System.Test.startTest();

            
            Invocable_RegisterAgents.registerAgents(stepIdList);
            
            Invocable_RegisterAgents.checkForDuplicatePersonAccounts('Test', acc.id);
            Invocable_RegisterAgents.checkForDuplicatePersonAccounts('Test', null);
            Invocable_RegisterAgents.getAgencyOwnerId('United Arab Emirates', 'Real Estate', 'Incorp_India');
            Invocable_RegisterAgents.getAgencyOwnerId('China', 'Real Estate', 'Incorp_India');
            Invocable_RegisterAgents.getAgencyOwnerId('United Arab Emirates', 'Not Real Estate', 'Incorp_India');
            Invocable_RegisterAgents.getAgencyOwnerId('China', 'Not Real Estate', 'Incorp_India');
            
            System.Test.stopTest();
        }
    }
    
    @isTest static void ccAgentRegistered2() {
        
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test456', email='xyz1@email.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz1@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
            User pwUser = new User(alias = 'test456', email='xyz1@email.com',
                emailencodingkey='UTF-8', lastname='Paula Wehbeh', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='pw@email.com',UserRoleId = userRoleObj.Id);
                insert pwUser;

            List<Account_SR_Field_Mapping__c> lstAccSRFldMapping = new List<Account_SR_Field_Mapping__c>();
            lstAccSRFldMapping.add(new Account_SR_Field_Mapping__c(name = '1',Account_Field__c = 'Firstname',SR_Field__c='First_Name__c',Is_Person_Account__c = true));
            lstAccSRFldMapping.add(new Account_SR_Field_Mapping__c(name = '2',Account_Field__c = 'lastname',SR_Field__c='Last_Name__c',Is_Person_Account__c = true));
            lstAccSRFldMapping.add(new Account_SR_Field_Mapping__c(name = '3',Account_Field__c = 'Name',Is_Common_to_All_RT__c = true,SR_Field__c='Agency_Name__c',Is_Person_Account__c = false));
            lstAccSRFldMapping.add(new Account_SR_Field_Mapping__c(name = '4',Account_Field__c = 'Agency_Type__c',SR_Field__c='Agency_Type__c',Is_Person_Account__c = false));
            
            insert lstAccSRFldMapping;
			NSIBPM__Service_Request__c currSR = new NSIBPM__Service_Request__c();
            Account newAcc ;
            List<Account> accList = new List<Account>();
            for(Integer i=0;i<10;i++) 
            {
			    newAcc = new Account();
                newAcc.name = 'TestCCReg' + i;
                accList.add(newAcc);

            }
            insert accList;
            Account accObj = accList[0];
            currSR.NSIBPM__Customer__c = accList[0].Id;
            currSR.Bank_Name__c = 'XYZ';
            currSR.Agency_Name__c = 'abc';
			currSR.Country__c = 'United Arab Emirates';
            currSR.Agency_Type__c = 'Corporate';
            insert currSR;
            Contact newCon;
            List<Contact>conList = new List<Contact>();
            for(Integer i=0;i<accList.Size();i++){
                newCon = new Contact();
                newCon.FirstName = 'FirstName' + i;
                newCon.LastName = 'LastName' + i;
                newCon.Email = 'TestCCReg' + i + '@gmail.com';
                newCon.Accountid = accList[i].Id;
                conList.add(newCon);
            }
            insert conList;
            Map<id,contact> mpContact = new Map<id,contact>();
            for(contact cnct : conlist){
                mpContact.put(cnct.id,cnct);
            }
            Id accRecordTypeId   = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

            Account agency1 = InitialiseTestData.getCorporateAccount('Agency125');
            Account agency2 = InitialiseTestData.getCorporateAccount('Agency126');

            insert agency1;
            insert agency2;

            Account acc = new Account();
            acc.lastname = 'ltest';
            acc.RecordTypeId = accRecordTypeId ;
            acc.Blacklisted__c = true ;
            acc.Reason_for_Blacklisting__c = 'Agency Blacklisting unit test';
            insert acc ;

            Contact newCont = new Contact();
            newCon.FirstName = 'Test';
            newCont.LastName = 'Agency125';
            newCont.AccountId = agency1.Id ;
            insert newCont ;


            NSIBPM__SR_Steps__c srStep= new NSIBPM__SR_Steps__c();
            insert srStep;

            List<NSIBPM__Service_Request__c> SRList = InitialiseTestData.createTestServiceRequestRecords(new List<NSIBPM__Service_Request__c>{
                new NSIBPM__Service_Request__c(recordTypeId = InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Registration')),
                    new NSIBPM__Service_Request__c(recordTypeId = InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Registration'))});


            NSIBPM__Service_Request__c serviceRequestObject = SRList[0] ;
            serviceRequestObject.Last_Name__c = 'test User' ;
            serviceRequestObject.NSIBPM__Customer__c = agency2.Id;
            serviceRequestObject.Agency_Type__c = 'Individual';
            serviceRequestObject.OwnerId = UserInfo.getUserId();
            update serviceRequestObject ;

            Amendment__c newAmd;
            List<Amendment__c> amdList = new List<Amendment__c>();
            for(integer i=0;i<conList.Size();i++){
                newAmd =  new Amendment__c();
                newAmd.First_Name__c = 'testfn'+i;
                newAmd.Last_Name__c = 'testfn'+i;
                newAmd.Account__c = conList[i].AccountId;
                newAmd.Contact__c = conList[i].Id;
                if(i==0){
                    newAmd.Owner__c = true;
                    newAmd.Shareholding__c =100;
                }
                newAmd.Authorised_Signatory__c = true;
                newAmd.Agent_Representative__c =true;
                newAmd.Portal_Administrator__c = true;
                newAmd.Email__c ='TestCCReg'+i+'@test.com';
                newAmd.ID_Expiry_Date__c = System.Today().addDays(100);
                newAmd.ID_Type__c = 'Visa';
                newAmd.Service_Request__c = SRList[0].Id ;
                amdList.add(newAmd);
            }
            insert amdList;
            Map<String, Contact> testconMap = new Map<String, Contact>();
            Map<String, Amendment__c> testamdMap = new Map<String, Amendment__c>();

            for (Amendment__c amd :  amdList) {
                testconMap.put(amd.id, mpContact.get(amd.Contact__c));
                testamdMap.put(amd.id,amd);
            }



            List<New_Step__c> createStepList = new List<New_Step__c>();
            createStepList.add(new New_Step__c(Service_Request__c = SRList[0].id, Step_No__c  = 1, Step_Type__c = 'Agent Executive Review', Step_Status__c = 'Awaiting Additional Info'));
            createStepList.add(new New_Step__c(Service_Request__c = SRList[0].id, Step_No__c = 2, Step_Type__c  = 'Agreement Signing', Step_Status__c = 'More Info Updated'));
            insert createStepList;

            User portalUser1  = InitialiseTestData.getPortalUser('portalCCUser@test.com',newCont.Id);
            insert portalUser1 ;

            List<Id> stepIdList = new List<Id>();
            stepIdList.add(createStepList[0].Id);
            stepIdList.add(createStepList[1].Id);
            
            Agency_Owner__c agencyOwner = new Agency_Owner__c();
            agencyOwner.Name = 'Real_Estate_UAE';
            agencyOwner.User_Name__c = 'User 456';
            insert agencyOwner;
            
            System.Test.startTest();

            
            Invocable_RegisterAgents.registerAgents(stepIdList);
            
            Invocable_RegisterAgents.checkForDuplicatePersonAccounts('Test', acc.id);
            Invocable_RegisterAgents.checkForDuplicatePersonAccounts('Test', null);
            Invocable_RegisterAgents.getAgencyOwnerId('United Arab Emirates', 'Real Estate', 'Incorp_India');
            Invocable_RegisterAgents.getAgencyOwnerId('China', 'Real Estate', 'Incorp_India');
            Invocable_RegisterAgents.getAgencyOwnerId('United Arab Emirates', 'Not Real Estate', 'Incorp_India');
            Invocable_RegisterAgents.getAgencyOwnerId('China', 'Not Real Estate', 'Incorp_India');
            
            System.Test.stopTest();
        }
    }
    
    @isTest static void ccAgentRegistered3() {
        
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test456', email='xyz1@email.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz1@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
            User pwUser = new User(alias = 'test456', email='xyz1@email.com',
                emailencodingkey='UTF-8', lastname='Paula Wehbeh', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='pw@email.com',UserRoleId = userRoleObj.Id);
                insert pwUser;

            List<Account_SR_Field_Mapping__c> lstAccSRFldMapping = new List<Account_SR_Field_Mapping__c>();
            lstAccSRFldMapping.add(new Account_SR_Field_Mapping__c(name = '1',Account_Field__c = 'Firstname',SR_Field__c='First_Name__c',Is_Person_Account__c = true));
            lstAccSRFldMapping.add(new Account_SR_Field_Mapping__c(name = '2',Account_Field__c = 'lastname',SR_Field__c='Last_Name__c',Is_Person_Account__c = true));
            lstAccSRFldMapping.add(new Account_SR_Field_Mapping__c(name = '3',Account_Field__c = 'Name',Is_Common_to_All_RT__c = true,SR_Field__c='Agency_Name__c',Is_Person_Account__c = false));
            lstAccSRFldMapping.add(new Account_SR_Field_Mapping__c(name = '4',Account_Field__c = 'Agency_Type__c',SR_Field__c='Agency_Type__c',Is_Person_Account__c = false));
            
            insert lstAccSRFldMapping;
			NSIBPM__Service_Request__c currSR = new NSIBPM__Service_Request__c();
            Account newAcc ;
            List<Account> accList = new List<Account>();
            for(Integer i=0;i<10;i++) 
            {
			    newAcc = new Account();
                newAcc.name = 'TestCCReg' + i;
                accList.add(newAcc);

            }
            insert accList;
            Account accObj = accList[0];
            currSR.NSIBPM__Customer__c = accList[0].Id;
            currSR.Bank_Name__c = 'XYZ';
            currSR.Agency_Name__c = 'abc';
			currSR.Country__c = 'United Arab Emirates';
            currSR.Agency_Type__c = 'Corporate';
            insert currSR;
            Contact newCon;
            List<Contact>conList = new List<Contact>();
            for(Integer i=0;i<accList.Size();i++){
                newCon = new Contact();
                newCon.FirstName = 'FirstName' + i;
                newCon.LastName = 'LastName' + i;
                newCon.Email = 'TestCCReg' + i + '@gmail.com';
                newCon.Accountid = accList[i].Id;
                conList.add(newCon);
            }
            insert conList;
            Map<id,contact> mpContact = new Map<id,contact>();
            for(contact cnct : conlist){
                mpContact.put(cnct.id,cnct);
            }
            Id accRecordTypeId   = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

            Account agency1 = InitialiseTestData.getCorporateAccount('Agency125');
            Account agency2 = InitialiseTestData.getCorporateAccount('Agency126');

            insert agency1;
            insert agency2;

            Account acc = new Account();
            acc.lastname = 'ltest';
            acc.RecordTypeId = accRecordTypeId ;
            acc.Blacklisted__c = true ;
            acc.Reason_for_Blacklisting__c = 'Agency Blacklisting unit test';
            insert acc ;

            Contact newCont = new Contact();
            newCon.FirstName = 'Test';
            newCont.LastName = 'Agency125';
            newCont.AccountId = agency1.Id ;
            insert newCont ;


            NSIBPM__SR_Steps__c srStep= new NSIBPM__SR_Steps__c();
            insert srStep;

            List<NSIBPM__Service_Request__c> SRList = InitialiseTestData.createTestServiceRequestRecords(new List<NSIBPM__Service_Request__c>{
                new NSIBPM__Service_Request__c(recordTypeId = InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Registration')),
                    new NSIBPM__Service_Request__c(recordTypeId = InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Registration'))});


            NSIBPM__Service_Request__c serviceRequestObject = SRList[0] ;
            serviceRequestObject.Last_Name__c = 'test User' ;
            serviceRequestObject.Agency_Type__c = 'Individual';
            serviceRequestObject.OwnerId = UserInfo.getUserId();
            update serviceRequestObject ;

            Amendment__c newAmd;
            List<Amendment__c> amdList = new List<Amendment__c>();
            for(integer i=0;i<conList.Size();i++){
                newAmd =  new Amendment__c();
                newAmd.First_Name__c = 'testfn'+i;
                newAmd.Last_Name__c = 'testfn'+i;
                newAmd.Account__c = conList[i].AccountId;
                newAmd.Contact__c = conList[i].Id;
                if(i==0){
                    newAmd.Owner__c = true;
                    newAmd.Shareholding__c =100;
                }
                newAmd.Authorised_Signatory__c = true;
                newAmd.Agent_Representative__c =true;
                newAmd.Portal_Administrator__c = true;
                newAmd.Email__c ='TestCCReg'+i+'@test.com';
                newAmd.ID_Expiry_Date__c = System.Today().addDays(100);
                newAmd.ID_Type__c = 'Visa';
                newAmd.Service_Request__c = SRList[0].Id ;
                amdList.add(newAmd);
            }
            insert amdList;
            Map<String, Contact> testconMap = new Map<String, Contact>();
            Map<String, Amendment__c> testamdMap = new Map<String, Amendment__c>();

            for (Amendment__c amd :  amdList) {
                testconMap.put(amd.id, mpContact.get(amd.Contact__c));
                testamdMap.put(amd.id,amd);
            }



            List<New_Step__c> createStepList = new List<New_Step__c>();
            createStepList.add(new New_Step__c(Service_Request__c = SRList[0].id, Step_No__c  = 1, Step_Type__c = 'Agent Executive Review', Step_Status__c = 'Awaiting Additional Info'));
            createStepList.add(new New_Step__c(Service_Request__c = SRList[0].id, Step_No__c = 2, Step_Type__c  = 'Agreement Signing', Step_Status__c = 'More Info Updated'));
            insert createStepList;

            User portalUser1  = InitialiseTestData.getPortalUser('portalCCUser@test.com',newCont.Id);
            insert portalUser1 ;

            List<Id> stepIdList = new List<Id>();
            stepIdList.add(createStepList[0].Id);
            stepIdList.add(createStepList[1].Id);
            
            Agency_Owner__c agencyOwner = new Agency_Owner__c();
            agencyOwner.Name = 'Real_Estate_UAE';
            agencyOwner.User_Name__c = 'User 456';
            insert agencyOwner;
            
            System.Test.startTest();

            
            Invocable_RegisterAgents.registerAgents(stepIdList);
            
            Invocable_RegisterAgents.checkForDuplicatePersonAccounts('Test', acc.id);
            Invocable_RegisterAgents.checkForDuplicatePersonAccounts('Test', null);
            Invocable_RegisterAgents.getAgencyOwnerId('United Arab Emirates', 'Real Estate', 'Incorp_India');
            Invocable_RegisterAgents.getAgencyOwnerId('China', 'Real Estate', 'Incorp_India');
            Invocable_RegisterAgents.getAgencyOwnerId('United Arab Emirates', 'Not Real Estate', 'Incorp_India');
            Invocable_RegisterAgents.getAgencyOwnerId('China', 'Not Real Estate', 'Incorp_India');
            
            System.Test.stopTest();
        }
    }
    
    @isTest static void ccAgentRegistered4() {
        
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test456', email='xyz1@email.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz1@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
            User pwUser = new User(alias = 'test456', email='xyz1@email.com',
                emailencodingkey='UTF-8', lastname='Paula Wehbeh', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='pw@email.com',UserRoleId = userRoleObj.Id);
                insert pwUser;

            List<Account_SR_Field_Mapping__c> lstAccSRFldMapping = new List<Account_SR_Field_Mapping__c>();
            lstAccSRFldMapping.add(new Account_SR_Field_Mapping__c(name = '1',Account_Field__c = 'Firstname',SR_Field__c='First_Name__c',Is_Person_Account__c = true));
            lstAccSRFldMapping.add(new Account_SR_Field_Mapping__c(name = '2',Account_Field__c = 'lastname',SR_Field__c='Last_Name__c',Is_Person_Account__c = true));
            lstAccSRFldMapping.add(new Account_SR_Field_Mapping__c(name = '3',Account_Field__c = 'Name',Is_Common_to_All_RT__c = true,SR_Field__c='Agency_Name__c',Is_Person_Account__c = false));
            lstAccSRFldMapping.add(new Account_SR_Field_Mapping__c(name = '4',Account_Field__c = 'Agency_Type__c',SR_Field__c='Agency_Type__c',Is_Person_Account__c = false));
            
            insert lstAccSRFldMapping;
			NSIBPM__Service_Request__c currSR = new NSIBPM__Service_Request__c();
            Account newAcc ;
            List<Account> accList = new List<Account>();
            for(Integer i=0;i<10;i++) 
            {
			    newAcc = new Account();
                newAcc.name = 'TestCCReg' + i;
                accList.add(newAcc);

            }
            insert accList;
            Account accObj = accList[0];
            currSR.NSIBPM__Customer__c = accList[0].Id;
            currSR.Bank_Name__c = 'XYZ';
            currSR.Agency_Name__c = 'abc';
			currSR.Country__c = 'United Arab Emirates';
            currSR.Agency_Type__c = 'Corporate';
            insert currSR;
            Contact newCon;
            List<Contact>conList = new List<Contact>();
            for(Integer i=0;i<accList.Size();i++){
                newCon = new Contact();
                newCon.FirstName = 'FirstName' + i;
                newCon.LastName = 'LastName' + i;
                newCon.Email = 'TestCCReg' + i + '@gmail.com';
                newCon.Accountid = accList[i].Id;
                conList.add(newCon);
            }
            insert conList;
            Map<id,contact> mpContact = new Map<id,contact>();
            for(contact cnct : conlist){
                mpContact.put(cnct.id,cnct);
            }
            Id accRecordTypeId   = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

            Account agency1 = InitialiseTestData.getCorporateAccount('Agency125');
            Account agency2 = InitialiseTestData.getCorporateAccount('Agency126');

            insert agency1;
            insert agency2;

            Account acc = new Account();
            acc.lastname = 'ltest';
            acc.RecordTypeId = accRecordTypeId ;
            acc.Blacklisted__c = true ;
            acc.Reason_for_Blacklisting__c = 'Agency Blacklisting unit test';
            insert acc ;

            Contact newCont = new Contact();
            newCon.FirstName = 'Test';
            newCont.LastName = 'Agency125';
            newCont.AccountId = agency1.Id ;
            insert newCont ;


            NSIBPM__SR_Steps__c srStep= new NSIBPM__SR_Steps__c();
            insert srStep;

            List<NSIBPM__Service_Request__c> SRList = InitialiseTestData.createTestServiceRequestRecords(new List<NSIBPM__Service_Request__c>{
                new NSIBPM__Service_Request__c(recordTypeId = InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Registration')),
                    new NSIBPM__Service_Request__c(recordTypeId = InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Registration'))});


            NSIBPM__Service_Request__c serviceRequestObject = SRList[0] ;
            serviceRequestObject.Agency_Name__c = 'test1 User' ;
            serviceRequestObject.Last_Name__c = 'test User' ;
            update serviceRequestObject ;

            NSIBPM__Service_Request__c serviceRequestObject1 = SRList[1] ;
            serviceRequestObject.Agency_Name__c = 'test1 User' ;
            serviceRequestObject.Last_Name__c = 'test1 User' ;
            serviceRequestObject.Agency_Type__c = 'Corporate';
            serviceRequestObject.OwnerId = UserInfo.getUserId();
            update serviceRequestObject ;

            Amendment__c newAmd;
            List<Amendment__c> amdList = new List<Amendment__c>();
            for(integer i=0;i<conList.Size();i++){
                newAmd =  new Amendment__c();
                newAmd.First_Name__c = 'testfn'+i;
                newAmd.Last_Name__c = 'testfn'+i;
                newAmd.Account__c = conList[i].AccountId;
                newAmd.Contact__c = conList[i].Id;
                if(i==0){
                    newAmd.Owner__c = true;
                    newAmd.Shareholding__c =100;
                }
                newAmd.Authorised_Signatory__c = true;
                newAmd.Agent_Representative__c =true;
                newAmd.Portal_Administrator__c = true;
                newAmd.Email__c ='TestCCReg'+i+'@test.com';
                newAmd.ID_Expiry_Date__c = System.Today().addDays(100);
                newAmd.ID_Type__c = 'Visa';
                newAmd.Service_Request__c = SRList[0].Id ;
                amdList.add(newAmd);
            }
            insert amdList;
            Map<String, Contact> testconMap = new Map<String, Contact>();
            Map<String, Amendment__c> testamdMap = new Map<String, Amendment__c>();

            for (Amendment__c amd :  amdList) {
                testconMap.put(amd.id, mpContact.get(amd.Contact__c));
                testamdMap.put(amd.id,amd);
            }



            List<New_Step__c> createStepList = new List<New_Step__c>();
            createStepList.add(new New_Step__c(Service_Request__c = SRList[0].id, Step_No__c  = 1, Step_Type__c = 'Agent Executive Review', Step_Status__c = 'Awaiting Additional Info'));
            createStepList.add(new New_Step__c(Service_Request__c = SRList[0].id, Step_No__c = 2, Step_Type__c  = 'Agreement Signing', Step_Status__c = 'More Info Updated'));
            insert createStepList;

            User portalUser1  = InitialiseTestData.getPortalUser('portalCCUser@test.com',newCont.Id);
            insert portalUser1 ;

            List<Id> stepIdList = new List<Id>();
            stepIdList.add(createStepList[0].Id);
            stepIdList.add(createStepList[1].Id);
            
            Agency_Owner__c agencyOwner = new Agency_Owner__c();
            agencyOwner.Name = 'Real_Estate_UAE';
            agencyOwner.User_Name__c = 'User 456';
            insert agencyOwner;
            
            System.Test.startTest();

            
            Invocable_RegisterAgents.registerAgents(stepIdList);
            
            Invocable_RegisterAgents.checkForDuplicatePersonAccounts('Test', acc.id);
            Invocable_RegisterAgents.checkForDuplicatePersonAccounts('Test', null);
            Invocable_RegisterAgents.getAgencyOwnerId('United Arab Emirates', 'Real Estate', 'Incorp_India');
            Invocable_RegisterAgents.getAgencyOwnerId('China', 'Real Estate', 'Incorp_India');
            Invocable_RegisterAgents.getAgencyOwnerId('United Arab Emirates', 'Not Real Estate', 'Incorp_India');
            Invocable_RegisterAgents.getAgencyOwnerId('China', 'Not Real Estate', 'Incorp_India');
            
            System.Test.stopTest();
        }
    }
}