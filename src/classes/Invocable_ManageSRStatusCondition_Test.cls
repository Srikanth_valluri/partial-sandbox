@isTest
public class Invocable_ManageSRStatusCondition_Test {
    @testSetup static void setupData() {
        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Agent Registration',false,null);
        sr.New_Country_of_Sale__c = 'UAE';
        insert sr;
        
        New_Step__c stp = new New_Step__c(Service_Request__c = sr.Id, Step_No__c  = 1, Step_Type__c = 'Agent Executive Review', Step_Status__c = 'Awaiting Additional Info');
        insert stp;
        
        List<string> statuses = new list<string>{'UNDER_REVIEW','AWAITING_FFA_AA'};
            Map<string,NSIBPM__SR_Status__c> mpsrStatus =  InitializeSRDataTest.createSRStatus(statuses);
    }
    
    @isTest static void test_method_1() {
        
        List<Id> stepList = new List<Id>();
        for (New_Step__c newStep : [SELECT Id FROM New_Step__c]) {
            stepList.add(newStep.Id);
        }
        
        Test.startTest();
        
        Invocable_ManageSRStatusConditionBased.manageSRStatusConditionBased(stepList);
        
        Test.stopTest();
    }
}