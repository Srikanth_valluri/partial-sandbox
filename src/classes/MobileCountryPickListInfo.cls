/*
  Written by : CH V Gopinadh
  Test class : NationalityPickListInfo_Test
  Decription : This will send response as picklist values of Mobile Country on Inquiry
*/
@RestResource(urlMapping='/mobilecountrypicklist/*')
global class MobileCountryPickListInfo{
    @HttpGet 
    global static void getNewFundraiser(){
        RestResponse res = RestContext.response;
        RestRequest req = RestContext.request;
        Schema.DescribeFieldResult fieldResult = Inquiry__c.Mobile_CountryCode__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        List<String> pickListValuesList = new List<String>();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        } 
        //picklist picklist = new picklist();
        //picklist.picklitsValues = pickListValuesList;
        System.debug('pickListValuesListpickListValuesList'+pickListValuesList);
        res.responseBody = blob.valueof(JSON.serialize(pickListValuesList));
        res.statusCode = 200;
                
    }
    
}