@isTest
private class ConnectReadinessFlagUpdateWebServiceTest{
    
    
    @testSetup
    static void setup() {
      
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        Location__c locationNew = new Location__c();
        locationNew.Location_ID__c = '3456' ;
        locationNew.Location_Type__c = 'Unit' ;
        locationNew.Name = 'Test';
        insert locationNew;     
        
        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Property_Code__c = 'Test';
        objInventory.Unit__c = '1345';
        objInventory.Unit_Ok_to_Send_HO_EHO_Notice__c = false;
        objInventory.Building_Location__c = locationNew.Id;
        insert objInventory;
        
    }
    
    
    
    @isTest static void controllerTest1() {
        
        Inventory__c objInv = [Select Id From Inventory__c];

        Test.startTest();
        //As Per Best Practice it is important to instantiate the Rest Context
        String JsonMsg = 'Test';
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/FlagUpdate';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;

        //Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1));
        List<ConnectReadinessFlagUpdateWebService.UnitDetailsResponseWrapper>  lstWrap = ConnectReadinessFlagUpdateWebService.getUnitsDetails('Test');
        Test.stopTest();

    }
    
    @isTest static void controllerTest2() {
        
        Inventory__c objInv = [Select Id From Inventory__c];

        Test.startTest();
        //As Per Best Practice it is important to instantiate the Rest Context
        String JsonMsg = '{"arrayOfInventory": [ {"strUnitName" : "","strPropertyCode" : "Test","ID" : objInv.Id ,"blnRedinessFlag" : true}]}';
                
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/FlagUpdate';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        
        List<ConnectReadinessFlagUpdateWebService.UnitDetailsResponseWrapper> arrayOfInventory = new List<ConnectReadinessFlagUpdateWebService.UnitDetailsResponseWrapper>();
        arrayOfInventory.add( new ConnectReadinessFlagUpdateWebService.UnitDetailsResponseWrapper(objInv.Id ,'Test',true,''));
        
        //Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1));
        ConnectReadinessFlagUpdateWebService.stampReadinessFlagInSF(arrayOfInventory);
        Test.stopTest();

    }

    @isTest static void controllerTest3() {
        
        Inventory__c objInv = [Select Id From Inventory__c];

        Test.startTest();
        //As Per Best Practice it is important to instantiate the Rest Context
        String JsonMsg = 'xyz';
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/FlagUpdate';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;

        //Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1));
        List<ConnectReadinessFlagUpdateWebService.UnitDetailsResponseWrapper>  lstWrap = ConnectReadinessFlagUpdateWebService.getUnitsDetails('xyz');
        Test.stopTest();

    }

}