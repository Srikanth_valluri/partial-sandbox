/*
Description:  Rest Response Class for WHATSAPP CLIENT to read data from WHATSAPP
Developed By: DAMAC IT Team
*/


@RestResource(urlMapping='/waflows/*')
global without sharing class DAMAC_WA_REST_FLOWS{   

    private static WA_Credentials__mdt creds;
    @HttpPost
    global static void doPost(){    
            
        system.debug(RestContext.request);
        system.debug(RestContext.request.params);
        system.debug(RestContext.request.params.size());
        
        
        if (RestContext.request.params.size() > 0) {             
            map<string,string> respMap = new map<string,string>();
            respMap = RestContext.request.params;
            
            
            
            // Event Message Incoming in to Salesforce 
            if(respMap.containskey('event') && respMap.get('event') == 'message' && respMap.get('message[dir]') == 'i'){
                
                string cuid = respMap.get('contact[uid]'); // Sender Whats App Number
                system.debug('WHATS_APP_NUMBER=='+cuid);
                string whatsAppMessageId = respMap.get('message[uid]'); // WhatApp Message Unique Id                    
                string Message = respMap.get('message[body][text]'); // Message sent by the Customer
                boolean childFound = false; // Input from customer Expected and to update on the child
                boolean CancelFlowKeywordFound =false; // To know cancel the Flow
                
                Whats_App_Flows__c flowThread;
                boolean isconfused = false;
                id userflowRtypeId =  Schema.SObjectType.Whats_App_Flows__c.getRecordTypeInfosByName().get('User Flow').getRecordTypeId();
                
                //Check for Flow Definers
                list<Whats_App_Flows__c> keyWords = [select id,
                                                            Restart_Cancel_Keyword__c 
                                                            from Whats_App_Flows__c 
                                                            where Restart_Cancel_Keyword__c=:Message];
                if( keyWords.size() > 0){
                    CancelFlowKeywordFound = true;   
                }
                
                if(!CancelFlowKeywordFound ){
                    //Check for Active Flow
                    // Check for Active Flow on Parent first
                    list<Whats_App_Flows__c> toupdate = [select id,
                                                            Menu__c,
                                                            User_Input__c,
                                                            User_Selected_Flow__c,
                                                            Keyword__c,
                                                            Name 
                                                            from Whats_App_Flows__c 
                                                            where Active_Flow__c=:true 
                                                            and Waiting_For_Input__c=:true 
                                                            and Name=:cuid LIMIT 1];
                    
                    // If Nothing found on parent check on child                        
                    if(toupdate.size() == 0){
                        if(!test.isRunningTest()){
                            toupdate = [select id,
                                                Menu__c,
                                                User_Input__c,
                                                User_Selected_Flow__c,
                                                Keyword__c,Name 
                                                from Whats_App_Flows__c 
                                                where User_Selected_Flow__r.Active_Flow__c=:true 
                                                and Waiting_For_Input__c=:true 
                                                and User_Selected_Flow__r.Name=:cuid LIMIT 1];
                        }ELSE{
                             toupdate = [select id,
                                            Menu__c,
                                            User_Input__c,
                                            User_Selected_Flow__c,
                                            Keyword__c,Name 
                                            from Whats_App_Flows__c limit 1];
                        }
                        if(toupdate.size() > 0){
                            childFound = true; // Flag that Child is found
                        }
                    }
                    
                    // Nothing Found - Its a Fresh message find the Applicable Flow
                    if( toupdate.size() == 0 && !CancelFlowKeywordFound){// New Flow
                    list<Whats_App_Flows__c> Findflow = new list<Whats_App_Flows__c>();
                          Findflow = [select id,Menu__c,
                                                                    ( select id,Name,order__c from Child_Flows__r ) 
                                                                    from Whats_App_Flows__c where 
                                                                    Keyword__c=:message and recordtype.Name='Parent Flow' LIMIT 1];
                        //Check in Child Flows
                        if( Findflow.size() == 0  ){
                            
                                        Findflow = [select id,Menu__c,
                                                                    ( select id,Name,order__c from Child_Flows__r ) 
                                                                    from Whats_App_Flows__c where 
                                                                    Keyword__c=:message and recordtype.Name='Child Flow' and Active_flow__c=true LIMIT 1];
                           
                        }
                        
                        if( Findflow.size() >0  ){
                            //Create Flow for the Whats App Number
                            flowThread = new Whats_App_Flows__c ();
                            flowThread.Name = cuid;
                            flowThread.Flow_For__c =  cuid+'-'+whatsAppMessageId;
                            flowThread.User_Selected_Flow__c  = Findflow[0].id;
                            //flowThread.recordtypeid = '0121w00000012BX';
                            flowThread.recordtypeid = userflowRtypeId;
                            //flowThread.User_Input__c = Message;
                            flowThread.Waiting_For_Input__c= true;
                            //flowThread.keyword__c= Findflow[0].Child_Flows__r[0].id;
                            flowThread.Active_Flow__c = true;
                            upsert flowThread Flow_For__c; 
                                                           
                            
                            // Send First Message
                            doCallout(Findflow[0].menu__c,Findflow[0].id+'-'+flowThread.id,cuid);  
                        }else{ // No Flow Key word is found
                            Findflow = [select id,Menu__c,Confusion_Menu__c,END_Message__c,
                                                                    ( select id,Name,order__c from Child_Flows__r ) 
                                                                    from Whats_App_Flows__c where 
                                                                    Start_this_on_Confusion__c=true LIMIT 1];
                                //flowThread = [select id from Whats_App_Flows__c name=:cuid limit 1];
                                doCallout(Findflow[0].END_Message__c,string.valueof(system.now()),cuid);
                                //doCallout('Sorry i am not trained on this yet ?? ! try typing Hi  ??',string.valueof(system.now()),cuid);
                        } 
                                         
                    }else{ // Found an Existing Flow
                        
                        toupdate[0].User_Input__c = Message;
                        toupdate[0].Waiting_For_Input__c = false;
                        update toUpdate[0];
                        
                        list<Whats_App_Flows__c> NEXTASK = new list<Whats_App_Flows__c>();
                        if(toUpdate[0].keyword__c != null){ // Entered subflows
                            if(!test.isRunningTest()){
                              NEXTASK = [select id,Name,Menu__c,Query_Result__c,Query__c,Type__c,(select id,Name,query__c from Child_Flows__r) from Whats_App_Flows__c where id=:toUpdate[0].keyword__c] ;
                            }else{
                                NEXTASK = [select id,Name,Menu__c,Query_Result__c,Query__c,Type__c,(select id,Name,query__c from Child_Flows__r) from Whats_App_Flows__c LIMIT 1] ;
                            }
                        }else{
                            if(!test.isRunningTest()){
                              NEXTASK = [select id,Name,Menu__c,Query_Result__c,Query__c,Type__c,(select id,Name,query__c  from Child_Flows__r ) from Whats_App_Flows__c where Parent_Flow__c=:toUpdate[0].User_Selected_Flow__c and keyword__c=:message] ;
                            }else{
                                NEXTASK = [select id,Name,Menu__c,Query_Result__c,Query__c,Type__c,(select id,Name,query__c  from Child_Flows__r ) from Whats_App_Flows__c LIMIT 1] ;
                            }
                            if(NEXTASK.size() == 0){
                                system.debug('Entered confused input or Non Flow input');
                                isconfused = true;
                                toupdate[0].Waiting_For_Input__c = true;
                                update toupdate[0];
                                list<Whats_App_Flows__c> Findflow = [select id,Menu__c,Confusion_Menu__c,
                                                                    ( select id,Name,order__c from Child_Flows__r ) 
                                                                    from Whats_App_Flows__c where 
                                                                    Start_this_on_Confusion__c=true LIMIT 1];
                                flowThread = [select id from Whats_App_Flows__c  where Waiting_For_Input__c=true and name=:cuid limit 1];
                                doCallout(Findflow[0].Confusion_Menu__c,Findflow[0].id+'-'+flowThread.id+string.valueof(system.now()),cuid);                                    
                                                                    
                                
                            }
                        }
                        system.debug('is confused'+isconfused );
                        if(!isconfused){
                            if(nextask[0].Child_Flows__r.SIZE() >0 && NEXTASK[0].Child_Flows__r[0].Name != 'END' ){
                                Whats_App_Flows__c flowThreadChild = new Whats_App_Flows__c ();
                                flowThreadChild .Name = NEXTASK[0].Name; 
                                if(!childFound){                       
                                    flowThreadChild .User_Selected_Flow__c  = toUpdate[0].id;
                                }else{
                                    flowThreadChild .User_Selected_Flow__c  = toUpdate[0].User_Selected_Flow__c;
                                }
                                flowThreadChild .keyword__c = NEXTASK[0].Child_Flows__r[0].id;
                                //flowThreadChild .recordtypeid = '0121w00000012BX';                                 
                                flowThreadChild.recordtypeid = userflowRtypeId;
                                flowThreadChild.Waiting_For_Input__c = true;
                                flowThreadChild.Flow_For__c = toUpdate[0].id+'-'+toUpdate[0].User_Selected_Flow__c ;
                                flowThreadChild.query__c =   NEXTASK[0].Child_Flows__r[0].query__c;
                                upsert flowThreadChild Flow_For__c;
                                
                                doCallout(NEXTASK[0].Name+''+NEXTASK[0].Menu__c,toUpdate[0].id+'-'+flowThreadChild.id,cuid);  // Added Menu
                                
                               
                            }ELSE if(NEXTASK[0].Child_Flows__r[0].Name == 'END' ){
                                Whats_App_Flows__c flowThreadChild = new Whats_App_Flows__c ();
                                flowThreadChild .Name = NEXTASK[0].Name; 
                                if(!childFound){                       
                                    flowThreadChild .User_Selected_Flow__c  = toUpdate[0].id;
                                }else{
                                    flowThreadChild .User_Selected_Flow__c  = toUpdate[0].User_Selected_Flow__c;
                                }
                                flowThreadChild .keyword__c = NEXTASK[0].Child_Flows__r[0].id;
                                //flowThreadChild .recordtypeid = '0121w00000012BX';                                 
                                flowThreadChild.recordtypeid = userflowRtypeId;
                                flowThreadChild.Waiting_For_Input__c = false;
                                flowThreadChild.Flow_For__c = toUpdate[0].id+'-'+toUpdate[0].User_Selected_Flow__c;
                                flowThreadChild.query__c =   NEXTASK[0].Child_Flows__r[0].query__c;
                                
                                upsert flowThreadChild Flow_For__c;
                                
                                //Update top parent
                                //Whats_App_Flows__c updateTopParent = [select id,active_flow__c from Whats_App_Flows__c where id=:toUpdate[0].User_Selected_Flow__c LIMIT 1 ]    ;
                                Whats_App_Flows__c updateTopParent = [select id,Name,active_flow__c from Whats_App_Flows__c where name=:cuid LIMIT 1 ]    ;
                                updateTopParent.active_flow__c = FALSE;
                                updateTopParent.Name = updateTopParent.Name+'-END';
                                UPDATE updateTopParent;
                                system.debug('-->'+updateTopParent);
                                updateFlow(updateTopParent.id);
                                doCallout(NEXTASK[0].Name+''+NEXTASK[0].Menu__c,toUpdate[0].id+'-'+flowThreadChild .id,cuid); 
                                
                            
                            }
                        }//Confused If End
                    } 
                     
                }  else{
                    //Check for Active Flow
                    // Check for Active Flow on parent first
                    list<Whats_App_Flows__c> toupdate = [select id,Menu__c,User_Input__c,User_Selected_Flow__c,Keyword__c,Name from Whats_App_Flows__c where Active_Flow__c=:true and Waiting_For_Input__c=:true and Name=:cuid LIMIT 1];
                    
                    // If Nothing found on parent check on child
                    
                    if(toupdate.size() == 0){
                        toupdate = [select id,Menu__c,User_Input__c,User_Selected_Flow__c,Keyword__c,Name from Whats_App_Flows__c where User_Selected_Flow__r.Active_Flow__c=:true and Waiting_For_Input__c=:true and User_Selected_Flow__r.Name=:cuid LIMIT 1];
                        if(toupdate.size() > 0){
                            childFound = true;
                        }
                    }
                    
                    //Update top parent
                    Whats_App_Flows__c updateTopParent = [select id,active_flow__c from Whats_App_Flows__c where id=:toUpdate[0].User_Selected_Flow__c LIMIT 1 ]    ;
                    updateTopParent.active_flow__c = FALSE;
                    UPDATE updateTopParent;
                    
                   //Cancel Flow
                   doCallout('Cancelled'+' ??',string.valueof(system.now()),cuid);
                }         
                          
            }
            
        }   
    
    }
        
    @future(callout=true)
    public static void doCallout(string message,string uniqueId,string toNumber){
        user u = new user();
        u = [select id,Enable_WhatsApp__c,Enable_WhatsApp_Upload__c,WA_Account__c from user where id=:userinfo.getuserid() LIMIT 1];                
        creds = new WA_Credentials__mdt();
        //creds = [select Endpoint_URL__c,API_Key__c,WA_Account__c from WA_Credentials__mdt where WA_Account__c=:u.WA_Account__c LIMIT 1];
        creds = [select Endpoint_URL__c,API_Key__c,WA_Account__c from WA_Credentials__mdt where DeveloperName='OFFM_Feedback' LIMIT 1];
        
        
        string apiKey = creds.API_Key__c;
        string AccountPhoneNum = creds.WA_Account__c;        
        //string toMobNum = '919652300133'; 
        string toMobNum = toNumber;
        string UniqueIdCid = EncodingUtil.urlencode(uniqueId,'UTF-8');
        string encodedwaMessage = EncodingUtil.urlencode(message,'UTF-8');
                                    
        string endpoint = creds.Endpoint_URL__c+'token='+apiKey+'&uid='+AccountPhoneNum+'&to='+toMobNum+'&custom_uid='+UniqueIdCid+'&text='+encodedwaMessage;
        system.debug(endpoint);
        HttpRequest req = new HttpRequest();            
        req.setEndpoint(endpoint);
        req.setMethod('POST'); 
        req.setheader('Content-Type','application/x-www-form-urlencoded');
        Http http = new Http();
        HttpResponse response = new HttpResponse();
        if(!test.isrunningtest()){
            response = http.send(req);
        }
    }
    
    public static void updateFlow(string parentId){
        system.debug('--->PathId'+parentId);
        Whats_App_Flows__c flow = [select id,User_Input__c,Salesforce_Record_Id__c,value__c,(select id,Name,user_input__c from User_Selected_Flows__r) from Whats_App_Flows__c where id=:parentId];
        
        string flowpath = '';
        for(Whats_App_Flows__c waf:flow.User_Selected_Flows__r){
            system.debug(waf.user_input__c);
            if(waf.user_input__c != null){
                flowpath = flowpath + waf.user_input__c;
            }
        }
        //flow.User_Input__c = flowpath;
        system.debug('--->Path'+flowpath);
        //update flow;
        
        
        // Update Record 
        if(flow.Salesforce_Record_Id__c != null){
            
                                                                    
            office_meeting__c ofm = [select id from office_meeting__c where id=:flow.Salesforce_Record_Id__c];
            
            
            if(flow.user_input__c == '4'){
                ofm.How_do_you_rate_your_experience_today__c='Exceeded Expectation';
            }
            if(flow.user_input__c == '3'){
                ofm.How_do_you_rate_your_experience_today__c='Good';
            }
            if(flow.user_input__c == '2'){
                ofm.How_do_you_rate_your_experience_today__c='Average';
            }
            if(flow.user_input__c == '1'){
                ofm.How_do_you_rate_your_experience_today__c='Bad';
            }
            ofm.Why_were_you_not_satisfied_with_your_exp__c = flowpath;
            update ofm;
        }
        
    }
    
    
   
}