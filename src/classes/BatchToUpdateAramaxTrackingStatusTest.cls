@isTest
public class BatchToUpdateAramaxTrackingStatusTest {
    
    public class FirstFlightServiceMock implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req) {
            req.setMethod('POST');
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            String Json = '{ "AirwayBillTrackList": [{ "AirWayBillNo": "123456", "Destination": "ABU DHABI-UNITED ARAB EMIRATES", "ForwardingNumber": "", "Origin": "DUBAI-UNITED ARAB EMIRATES", "ShipmentProgress": 3, "ShipperReference": "AE5248222", "TrackingLogDetails": [{ "ActivityDate": "Saturday 02 November 2019", "ActivityTime": "12:21", "DeliveredTo": "", "Location": "DUBAI", "Remarks": "Location Information Received", "Status": "LC" }, { "ActivityDate": "Saturday 02 November 2019", "ActivityTime": "11:43", "DeliveredTo": "", "Location": "DUBAI", "Remarks": "Whatsapp To Customer", "Status": "SM" } ], "Weight": "1.000" }], "Code": 1, "Description": "Success" }';
            res.setBody(Json);
            res.setStatusCode(200);
            return res;
        }
    }
    
    public class AramexServiceMock implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req) {
            req.setMethod('POST');
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            String Json = '{ "Transaction": { "Reference1": null, "Reference2": null, "Reference3": null, "Reference4": null, "Reference5": null }, "Notifications": [], "HasErrors": false, "TrackingResults": [ { "Key": "44097879825", "Value": [ { "WaybillNumber": "44097879825", "UpdateCode": "SH005", "UpdateDescription": "Delivered", "UpdateDateTime": "/Date(1601282220000+0300)/", "UpdateLocation": "Dubai, United Arab Emirates", "Comments": "test ", "ProblemCode": "", "GrossWeight": "0.5", "ChargeableWeight": "0.5", "WeightUnit": "KG" }, { "WaybillNumber": "44097879825", "UpdateCode": "SH203", "UpdateDescription": "Record Created", "UpdateDateTime": "/Date(1599121080000+0300)/", "UpdateLocation": "Dubai, United Arab Emirates", "Comments": "", "ProblemCode": "", "GrossWeight": "0.5", "ChargeableWeight": "0.5", "WeightUnit": "KG" }, { "WaybillNumber": "44097879825", "UpdateCode": "SH278", "UpdateDescription": "Data received.", "UpdateDateTime": "/Date(1599121080000+0300)/", "UpdateLocation": "Dubai, United Arab Emirates", "Comments": " ", "ProblemCode": "", "GrossWeight": "0.5", "ChargeableWeight": "0.5", "WeightUnit": "KG" } ] } ], "NonExistingWaybills": [] }';
            res.setBody(Json);
            res.setStatusCode(200);
            return res;
        }
    }
    
	@isTest
    public static void testFirstFlightTrackingStatusBatch(){
        Account objAccount = new Account();
        objAccount.Name = 'TestAcc';
        objAccount.Party_ID__c = '3000';
        insert objAccount;
        
        Case objCase = new Case();
        objCase.AccountId = ObjAccount.Id;
        insert objCase;
        
        Credentials_Details__c objCred = new Credentials_Details__c();
        objCred.Name = 'First Flight Tracking';
        objCred.Endpoint__c = 'https://ontrack.firstflightme.com/FFCService.svc/Tracking';
        objCred.User_Name__c = '3000';
        objCred.Password__c = 'fftes';
        insert objCred;
        
        Courier_Delivery__c objFirstFlightCourier = new Courier_Delivery__c();
        objFirstFlightCourier.Courier_Service__c = 'First Flight';
        objFirstFlightCourier.Airway_Bill__c = '123456';
        objFirstFlightCourier.Country__c = 'AE';
        objFirstFlightCourier.Case__c = objCase.Id;
        objFirstFlightCourier.Account__c = objAccount.Id;
        insert objFirstFlightCourier;
        
        Courier_Delivery__c objAramexCourier = new Courier_Delivery__c();
        objAramexCourier.Courier_Service__c = 'First Flight';
        objAramexCourier.Airway_Bill__c = '123456';
        objFirstFlightCourier.Country__c = 'AE';
        objAramexCourier.Case__c = objCase.Id;
        objAramexCourier.Account__c = objAccount.Id;
        insert objAramexCourier;
        
        Test.setMock(HttpCalloutMock.class, new FirstFlightServiceMock());
        Test.startTest();
        BatchToUpdateAramaxTrackingStatus obj = new BatchToUpdateAramaxTrackingStatus();
        DataBase.executeBatch(obj);
        Test.stopTest();
    }
    
    @isTest
    public static void testAramexTrackingStatusBatch(){
        Account objAccount = new Account();
        objAccount.Name = 'TestAcc';
        objAccount.Party_ID__c = '3000';
        insert objAccount;
        
        Case objCase = new Case();
        objCase.AccountId = ObjAccount.Id;
        insert objCase;
        
        Credentials_Details__c objCredAramex = new Credentials_Details__c();
        objCredAramex.Name = 'Aramax Tracking';
        objCredAramex.Endpoint__c = 'https://ws.dev.aramex.net/ShippingAPI.V2/Tracking/Service_1_0.svc/json/TrackShipments';
        objCredAramex.User_Name__c = '3000';
        objCredAramex.Password__c = 'fftes';
        insert objCredAramex;
        
        Courier_Delivery__c objFirstFlightCourier = new Courier_Delivery__c();
        objFirstFlightCourier.Courier_Service__c = 'Aramex';
        objFirstFlightCourier.Airway_Bill__c = '44097879825';
        objFirstFlightCourier.Country__c = 'AE';
        objFirstFlightCourier.Case__c = objCase.Id;
        objFirstFlightCourier.Account__c = objAccount.Id;
        insert objFirstFlightCourier;
        
        Courier_Delivery__c objAramexCourier = new Courier_Delivery__c();
        objAramexCourier.Courier_Service__c = 'Aramex';
        objAramexCourier.Airway_Bill__c = '44097879825';
        objFirstFlightCourier.Country__c = 'AE';
        objAramexCourier.Case__c = objCase.Id;
        objAramexCourier.Account__c = objAccount.Id;
        insert objAramexCourier;
        
        Test.setMock(HttpCalloutMock.class, new AramexServiceMock());
        Test.startTest();
        BatchToUpdateAramaxTrackingStatus obj = new BatchToUpdateAramaxTrackingStatus();
        DataBase.executeBatch(obj);
        Test.stopTest();
    }
}