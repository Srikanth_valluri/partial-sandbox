// Created By :  Naresh Kaneriya

@isTest
public class AgentOnGoingBookingsControllerTest{


public static testMethod void Test_1(){
      Test.startTest();
        Id adminRoleId = [SELECT Id FROM UserRole WHERE DeveloperName = 'Chairman' LIMIT 1].Id;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'qw56', email='xqw@email.com',
                emailencodingkey='UTF-8', lastname='Userqw', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xqw@email.com',UserRoleId = adminRoleId);
        System.RunAs(adminUser){
            Account adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
            insert adminAccount;
            
            Contact adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
            insert adminContact;
            
            Contact agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
            insert agentContact;
            
            User portalUser = InitialiseTestData.getPortalUser('xyz@test.com', adminContact.Id, 'Admin');
            
            //User portalOnlyAgent = InitialiseTestData.getPortalUser('asba1@test.com', agentContact.Id, 'Agent');
          
        
        System.runAs(portalUser){
          NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
                SR.Agency_Type__c = 'Corporate';
                SR.Agency__c = null;
                SR.Trade_License_Number__c = 'test';
                insert SR;
          PageReference pageRef = Page.AgentOnGoingBookings;
          test.setCurrentPageReference(pageRef);
          pageRef.getParameters().put('sr',SR.Id);
          AgentOnGoingBookingsController  obj =  new AgentOnGoingBookingsController();
          Set<String> SetSrid =  new Set<String>();
          SetSrid.add('SetSrid');
          obj.lstSR = null;
          obj.listToDisplay = null;
          obj.SrStatus = null; 
          obj.SetSrid = SetSrid;
          obj.SendPayementLinkMail();  
        }
      
  
        }
  Test.stopTest();

}
public static testMethod void Test_2(){
      Test.startTest();
        Id adminRoleId = [SELECT Id FROM UserRole WHERE DeveloperName = 'Chairman' LIMIT 1].Id;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'qw56', email='xqw@email.com',
                emailencodingkey='UTF-8', lastname='Userqw', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xqw@email.com',UserRoleId = adminRoleId);
        System.RunAs(adminUser){
        Account adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
        insert adminAccount;
        
        Contact adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
        adminContact.Owner__c = false;
         adminContact.Portal_Administrator__c = false;
        insert adminContact;
        
        Contact agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
        insert agentContact;
        
        User portalUser = InitialiseTestData.getPortalUser('xyz@test.com', adminContact.Id, 'Admin');
       // User portalOnlyAgent = InitialiseTestData.getPortalUser('asba1@test.com', agentContact.Id, 'Agent');
       
     
     System.runAs(portalUser){
       NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
            SR.Agency_Type__c = 'Corporate';
            SR.Agency__c = null;
            SR.Trade_License_Number__c = 'test';
            insert SR;
      PageReference pageRef = Page.AgentOnGoingBookings;
      test.setCurrentPageReference(pageRef);
      pageRef.getParameters().put('sr',SR.Id);
      AgentOnGoingBookingsController  obj =  new AgentOnGoingBookingsController();
      Set<String> SetSrid =  new Set<String>();
      SetSrid.add('SetSrid');
      obj.lstSR = null;
      obj.listToDisplay = null;
      obj.SrStatus = null; 
      obj.SetSrid = SetSrid;
      obj.SendPayementLinkMail();  
     }
        }
  Test.stopTest();


}


}