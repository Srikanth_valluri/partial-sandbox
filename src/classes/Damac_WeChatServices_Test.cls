@isTest
public class Damac_WeChatServices_Test {
    static testMethod void Damac_WeChatServices () {
        ID recordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();      
        Inquiry__c inq = new Inquiry__c ();
        inq.Mobile_phone__c = '7894561230';
        inq.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Mobile_Phone_Encrypt__c = 'WERTYUIYRTYUIYRTYU';
        inq.Class__c = 'test';
        inq.First_Name__c = 'test';
        inq.Inquiry_Status__c = 'Active';
        inq.Inquiry_Source__c ='Agent Referral';  
        inq.RecordtypeId = recordTypeId;
        inq.Class__c = 'test';
        inq.Email__c = 'test@gmail.com';
        inq.Phone_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Nationality__c = 'Australian';
        inq.weChat_Id__c = 'WERTYUIYRTYUIYRTYU';
        insert inq;        
        
        Wechat_Message__c msg = new Wechat_Message__c ();
        msg.Last_Message_Sent__c = DateTime.Now ();
        msg.Outbound_Success__c = false;
        msg.Record_Id__c = inq.Id;

        msg.Wechat_ID__c = 'WERTYUIYRTYUIYRTYU';
        msg.Unique_Id__c = 'WERTYUIYRTYUIYRTYU'+userInfo.getUserId ();
        upsert msg Unique_Id__c ;
        
        Wechat_Deliverability__c del = new Wechat_Deliverability__c ();
        del.Wechat_Message__c = msg.id;
        del.Message_Source_Type__c = 'Inbound';
        insert del;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/wechatResponse';  
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf ('{"trigger" : "event", "_id" : "WERTYUIYRTYUIYRTYU", "messages": [{"source" : {"type" : "test"}}], "appUser" : {"conversationStarted" : true, "givenName" : "test", "surName" : "test", "email" : "test@test.com"}}');
        RestContext.request = req;
        RestContext.response = res; 
        
        Damac_WeChatServices obj = new Damac_WeChatServices ();
        Damac_WeChatServices.SMSPOSTResponse ();
        Damac_WeChatServices.sendInquiryCreationMsg (msg.Wechat_ID__c, inq.Id);
        Damac_WeChatServices.getUserDetails (inq.weChat_Id__c);
        Damac_WeChatServices.PostMessage (inq.id, inq.weChat_id__c, 'test', 'text', 'test');
        Damac_WeChatServices.prepareStringForBase64Decoding ('test==');
        
        Apexpages.currentpage().getparameters().put('id', inq.id);
        ApexPages.standardController std = new APexPages.StandardController (inq);
        Damac_CreateWeChatInquiryController sObj = new Damac_CreateWeChatInquiryController (std);
        sObj.save ();
        Damac_CreateWeChatInquiryController.getAllFields ('Inquiry__c');
    }
}