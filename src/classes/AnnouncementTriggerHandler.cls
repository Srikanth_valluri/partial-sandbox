/****************************************************************************************************************
* Name                  : AnnouncementTriggerHandler                                                            *
* Trigger               : AnnouncementTrigger                                                                   *
* Description           : This class will Notify all Agency Admin uses once Announcement record is created      *
* Created By            : NSI - Sivasankar K                                                                    *               
* Created Date          : 05/Feb/2017                                                                           *   
* ------------------------------------------------------------------------------------------------------------  *
* VERSION     AUTHOR                     DATE             Description                                           *                                        
* 1.0         NSI - Sivasankar K         05/Feb/2017      Initial development                                   *
*****************************************************************************************************************/
public with sharing class AnnouncementTriggerHandler implements TriggerFactoryInterface {

    /************************************************************************************************
    * @Description : this method will execute on after insert trigger only.                         *
    * @Params      : Map<Id, sObject>                                                               *
    * @Return      : void                                                                           *
    *************************************************************************************************/
    public void executeAfterInsertTrigger(Map<Id, sObject> newRecordsMap){
        try{
            notifyAgencyAdminswithanAnnouncement(newRecordsMap.values());
        }catch(Exception ex){
            Trigger.old[0].addError('#### Exception at line number = '+ex.getLineNumber()+' , Exception Message = '+ex.getMessage());
        }
    }

    /************************************************************************************************
    * @Description : Method to notify admins with announcement.                                     *
    * @Params      : Set<ID>                                                                        * 
    * @Return      : void                                                                           *
    ************************************************************************************************/
    @testvisible private void notifyAgencyAdminswithanAnnouncement(List<Announcement__c> newAnnouncements){
        
        List<Messaging.MassEmailMessage> massEmails = new List<Messaging.MassEmailMessage>();
        Map<String,Set<ID>> mapOfagencyAdminIDs = new  Map<String,Set<ID>>();
        Set<ID> agencyAdminIDs;
        List<ID> lstAgencyAdminIDs;
        String keyVal = '';
        
        List<EmailTemplate> emailTemp = new List<EmailTemplate>([SELECT Id,Name,Subject,body FROM EmailTemplate WHERE DeveloperName ='Announcement_Emails']);
        if(!emailTemp.isEmpty() && emailTemp.size() > 0 ){
            for(User us : [SELECT ID,Contact.Account.Recordtype.Name,Contact.Account.Agency_Tier__c 
            			   FROM User 
            			   WHERE IsActive = true AND 
            			   		 Profile.Name IN ('Customer Community - Admin','Customer Community - Agent + Admin','Customer Community - Agent + Admin + Auth','Customer Community - Auth + Admin') AND 
            			   		 Contact.Account.Recordtype.DeveloperName LIKE '%Agency%']){
                keyVal = ((String.isNotBlank(us.Contact.Account.Recordtype.Name) && us.Contact.Account.Recordtype.Name.containsIgnoreCase('Agency')) ? (us.Contact.Account.Recordtype.Name.containsIgnoreCase('Individual') ? 'INDIVIDUAL':'CORPORATE') : '');
                keyVal += (String.isNotBlank(us.Contact.Account.Agency_Tier__c) ? us.Contact.Account.Agency_Tier__c : '');
                keyVal = keyVal.toUpperCase();
                
                if(mapOfagencyAdminIDs.containsKey(keyVal)){
                    agencyAdminIDs = mapOfagencyAdminIDs.get(keyVal);
                    agencyAdminIDs.add(us.ID);
                    mapOfagencyAdminIDs.put(keyVal,agencyAdminIDs);
                }else{
                    agencyAdminIDs = new Set<ID>{us.ID};
                    mapOfagencyAdminIDs.put(keyVal,agencyAdminIDs);
                }
            }
            
            for(Announcement__c ann : newAnnouncements){
                 keyVal = ann.Agency_Type__c+''+ann.Agency_Tier__c;
                 keyVal = keyVal.toUpperCase();
                 if(String.isNotBlank(keyVal) && mapOfagencyAdminIDs.containsKey(keyVal) && mapOfagencyAdminIDs.get(keyVal).size() > 0){
                    lstAgencyAdminIDs = new List<ID>();
                    lstAgencyAdminIDs.addAll(mapOfagencyAdminIDs.get(keyVal));
                    Messaging.MassEmailMessage mail = new Messaging.MassEmailMessage();
                    mail.setTargetObjectIds(lstAgencyAdminIDs);
                    mail.setTemplateID(emailTemp[0].Id);
                    mail.setSaveAsActivity(false);
                    massEmails.add(mail);
                 }
            }
            if(!massEmails.isEmpty() && massEmails.size() > 0){
                Messaging.sendEmail(massEmails);
            }
        }
    }
    
    // TO BE Implemented
    public void executeBeforeInsertTrigger(List<sObject> newRecordsList){}
    public void executeBeforeUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){}
    public void executeBeforeInsertUpdateTrigger(List<sObject> newRecordsList, Map<Id,sObject> oldRecordsMap){}
    public void executeBeforeDeleteTrigger(Map<Id,sObject> oldRecordsMap){}
    public void executeAfterUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){}
    public void executeAfterInsertUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){}
    public void executeAfterDeleteTrigger(Map<Id,sObject> oldRecordsMap){}
}// End of class.