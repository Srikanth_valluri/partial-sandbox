/*
* Description - Test class developed for 'ProcessSelectionExtensionTest'
*
* Version            Date            Author            Description
* 1.0                29/01/18        Vivek Shinde      Initial Draft
*/
@isTest
private class ProcessSelectionExtensionTest {
    static testMethod void testMethod1() {
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
         
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        insert objCase;
        List<Case> lstCase = new List<Case>();
        lstCase.add(objCase);
        
        ProcessSelectionComponentController objProcessSelCntlr = new ProcessSelectionComponentController();
        List<Account> lstAccount = ProcessSelectionComponentController.searchMovie('Test FirstName');
        
        ApexPages.StandardSetController cases = new ApexPages.StandardSetController(lstCase);
        
        Test.startTest();
            Test.setCurrentPageReference(new PageReference('ProcessSelectionPage')); 
            System.currentPageReference().getParameters().put('CaseId', objCase.Id);
            System.currentPageReference().getParameters().put('SRType', 'AdditionalParking');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
    
            ProcessSelectionExtension obj = new ProcessSelectionExtension(cases);
    
            Test.setCurrentPageReference(new PageReference('ProcessSelectionPage')); 
            System.currentPageReference().getParameters().put('CaseId', objCase.Id);
            System.currentPageReference().getParameters().put('SRType', 'AssignmentRequest');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            obj = new ProcessSelectionExtension(cases);
    
            Test.setCurrentPageReference(new PageReference('ProcessSelectionPage')); 
            System.currentPageReference().getParameters().put('CaseId', objCase.Id);
            System.currentPageReference().getParameters().put('SRType', 'BouncedCheque');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            obj = new ProcessSelectionExtension(cases);
    
            Test.setCurrentPageReference(new PageReference('ProcessSelectionPage')); 
            System.currentPageReference().getParameters().put('CaseId', objCase.Id);
            System.currentPageReference().getParameters().put('SRType', 'FundTransfer');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            obj = new ProcessSelectionExtension(cases);
    
            Test.setCurrentPageReference(new PageReference('ProcessSelectionPage')); 
            System.currentPageReference().getParameters().put('CaseId', objCase.Id);
            System.currentPageReference().getParameters().put('SRType', 'FurniturePackage');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            obj = new ProcessSelectionExtension(cases);
    
            Test.setCurrentPageReference(new PageReference('ProcessSelectionPage')); 
            System.currentPageReference().getParameters().put('CaseId', objCase.Id);
            System.currentPageReference().getParameters().put('SRType', 'Mortgage');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            obj = new ProcessSelectionExtension(cases);
    
            Test.setCurrentPageReference(new PageReference('ProcessSelectionPage')); 
            System.currentPageReference().getParameters().put('CaseId', objCase.Id);
            System.currentPageReference().getParameters().put('SRType', 'PenaltyWaiver');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            obj = new ProcessSelectionExtension(cases);
    
            Test.setCurrentPageReference(new PageReference('ProcessSelectionPage')); 
            System.currentPageReference().getParameters().put('CaseId', objCase.Id);
            System.currentPageReference().getParameters().put('SRType', 'ProofOfPayment');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            obj = new ProcessSelectionExtension(cases);
    
            Test.setCurrentPageReference(new PageReference('ProcessSelectionPage')); 
            System.currentPageReference().getParameters().put('CaseId', objCase.Id);
            System.currentPageReference().getParameters().put('SRType', 'Refunds');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            obj = new ProcessSelectionExtension(cases);
    
            Test.setCurrentPageReference(new PageReference('ProcessSelectionPage')); 
            System.currentPageReference().getParameters().put('CaseId', objCase.Id);
            System.currentPageReference().getParameters().put('SRType', 'COCD');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            obj = new ProcessSelectionExtension(cases);
    
            Test.setCurrentPageReference(new PageReference('ProcessSelectionPage')); 
            System.currentPageReference().getParameters().put('CaseId', objCase.Id);
            System.currentPageReference().getParameters().put('SRType', 'COCD');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            obj = new ProcessSelectionExtension(cases);
    
            Test.setCurrentPageReference(new PageReference('ProcessSelectionPage')); 
            System.currentPageReference().getParameters().put('CaseId', objCase.Id);
            System.currentPageReference().getParameters().put('SRType', 'Early_Handover');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            obj = new ProcessSelectionExtension(cases);
    
            Test.setCurrentPageReference(new PageReference('ProcessSelectionPage')); 
            System.currentPageReference().getParameters().put('CaseId', objCase.Id);
            System.currentPageReference().getParameters().put('SRType', 'NOCVisa');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            obj = new ProcessSelectionExtension(cases);
    
            Test.setCurrentPageReference(new PageReference('ProcessSelectionPage')); 
            System.currentPageReference().getParameters().put('CaseId', objCase.Id);
            System.currentPageReference().getParameters().put('SRType', 'Complaint');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            obj = new ProcessSelectionExtension(cases);
    
            Test.setCurrentPageReference(new PageReference('ProcessSelectionPage')); 
            System.currentPageReference().getParameters().put('CaseId', objCase.Id);
            System.currentPageReference().getParameters().put('SRType', 'Home');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            obj = new ProcessSelectionExtension(cases);
    
            Test.setCurrentPageReference(new PageReference('ProcessSelectionPage')); 
            System.currentPageReference().getParameters().put('CaseId', objCase.Id);
            System.currentPageReference().getParameters().put('SRType', 'AOPT');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            obj = new ProcessSelectionExtension(cases);
    
            Test.setCurrentPageReference(new PageReference('ProcessSelectionPage')); 
            System.currentPageReference().getParameters().put('CaseId', objCase.Id);
            System.currentPageReference().getParameters().put('SRType', 'RentalPool');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            obj = new ProcessSelectionExtension(cases);
    
            Test.setCurrentPageReference(new PageReference('ProcessSelectionPage')); 
            System.currentPageReference().getParameters().put('CaseId', objCase.Id);
            System.currentPageReference().getParameters().put('SRType', 'TitleDeed');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            obj = new ProcessSelectionExtension(cases);
    
            Test.setCurrentPageReference(new PageReference('ProcessSelectionPage')); 
            System.currentPageReference().getParameters().put('CaseId', objCase.Id);
            System.currentPageReference().getParameters().put('SRType', 'Handover');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            obj = new ProcessSelectionExtension(cases);
    
            Test.setCurrentPageReference(new PageReference('ProcessSelectionPage')); 
            System.currentPageReference().getParameters().put('CaseId', objCase.Id);
            System.currentPageReference().getParameters().put('SRType', 'RentalPoolTermination');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            obj = new ProcessSelectionExtension(cases);
    
    
            Test.setCurrentPageReference(new PageReference('ProcessSelectionPage')); 
            System.currentPageReference().getParameters().put('CaseId', objCase.Id);
            System.currentPageReference().getParameters().put('SRType', 'RentalPoolAssignment');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            obj = new ProcessSelectionExtension(cases);
            
            Test.setCurrentPageReference(new PageReference('ProcessSelectionPage')); 
            System.currentPageReference().getParameters().put('CaseId', objCase.Id);
            System.currentPageReference().getParameters().put('SRType', 'RentalPoolAgreement');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            obj = new ProcessSelectionExtension(cases);
            
            Test.setCurrentPageReference(new PageReference('ProcessSelectionPage')); 
            System.currentPageReference().getParameters().put('CaseId', objCase.Id);
            System.currentPageReference().getParameters().put('SRType', 'RentalPoolEHO');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            obj = new ProcessSelectionExtension(cases);
    
            Test.setCurrentPageReference(new PageReference('ProcessSelectionPage')); 
            System.currentPageReference().getParameters().put('CaseId', objCase.Id);
            System.currentPageReference().getParameters().put('SRType', 'PlotHandover');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            obj = new ProcessSelectionExtension(cases);
            
            Test.setCurrentPageReference(new PageReference('ChequeReplacement')); 
            System.currentPageReference().getParameters().put('CaseId', objCase.Id);
            System.currentPageReference().getParameters().put('SRType', 'ChequeReplacement');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            obj = new ProcessSelectionExtension(cases);
        Test.stopTest();
    }
}