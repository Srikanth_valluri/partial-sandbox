public with sharing class Cancel_CODController {
	public Id caseId;
	public String errorMessage {get;set;}
	
	public Cancel_CODController( Id caseIdTemp ) {
		caseId = caseIdTemp ;
	}
	
    public Cancel_CODController(ApexPages.standardController controller) {
        PageReference currentPage = ApexPages.currentPage();
        if (currentPage != NULL) {
            caseId = currentPage.getParameters().get('id');
        }
    }
	
    public pagereference CancelCODCase(){
        if(Approval.isLocked(caseId)) {
            System.Debug('======Case is locked: ' + caseId);
            errorMessage = 'You cannot cancel a case that is submitted for approval. Please ask approver to either approve or reject the case.';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,errorMessage));
            return null;
        } else {
        	Case currentCase = [Select Id,
        							   Status
        						  From Case
        						 Where Id = :caseId];

        	System.Debug('======currentCase : ' + currentCase);
        	if(String.isNotBlank(currentCase.Status) &&
                ( currentCase.Status.equalsIgnoreCase('Cancelled')
                || currentCase.Status.equalsIgnoreCase('Closed')
                || currentCase.Status.equalsIgnoreCase('Rejected') )
        	) {
                System.Debug('======currentCase currentCase Status : ' + currentCase.Status);
                errorMessage = 'You cannot cancel a case that is ' + currentCase.Status + '.';
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,errorMessage));
                return null;
            } else if(String.isNotBlank(currentCase.Status))  {
        		System.Debug('======cancel the case : ' + currentCase);
        		currentCase.Status = 'Cancelled';

        		try{
    	    		update currentCase;
    	    		System.Debug('=update the case : ' + currentCase);
    			} catch(Exception e){
    				System.Debug('=Error in update the case : ' + e);
    				errorMessage = 'Please contact your system adminstrator ' + e.getMessage();
    				ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,errorMessage));
                	return null;
    			}
        	}
        }
    	pagereference newpg = new PageReference ('/'+caseId);
        newpg.setRedirect(false);
        return newpg;
    }
}