@isTest
public class CollectionMemoDocGenCtrlTest {
    private static testMethod void testDDP(){

        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rebate On Advance').getRecordTypeId();
        Case caseObj = new Case(Status='Draft Request', RecordTypeID=caseRecTypeId);
        insert caseObj;

        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c( name='Rebate Advance Payment Letter'
                                                        , Drawloop_Document_Package_Id__c = 'fjafjkaf'
                                                        , Delivery_Option_Id__c ='dasdasd' );
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController( caseObj );
        CollectionMemoDocGenCtrl ctrl = new CollectionMemoDocGenCtrl( stdCtrl );
        CollectionMemoDocGenCtrl.generateCollectionMemo();
        Test.stopTest();

    }

    private static testMethod void testBlankDDPId(){

        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rebate On Advance').getRecordTypeId();
        Case caseObj = new Case(Status='Draft Request', RecordTypeID=caseRecTypeId);
        insert caseObj;

        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c( name='Rebate Advance Payment Letter',Delivery_Option_Id__c ='dasdasd' );
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController( caseObj );
        CollectionMemoDocGenCtrl ctrl = new CollectionMemoDocGenCtrl( stdCtrl );
        CollectionMemoDocGenCtrl.generateCollectionMemo();
        Test.stopTest();

    }

    private static testMethod void testBlankDeliveryId(){

        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rebate On Advance').getRecordTypeId();
        Case caseObj = new Case(Status='Draft Request', RecordTypeID=caseRecTypeId);
        insert caseObj;

        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c(name='Rebate Advance Payment Letter', Drawloop_Document_Package_Id__c = 'fjafjkaf');
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController( caseObj );
        CollectionMemoDocGenCtrl ctrl = new CollectionMemoDocGenCtrl( stdCtrl );
        CollectionMemoDocGenCtrl.generateCollectionMemo();
        Test.stopTest();

    }

     private static testMethod void testBlankDDDP(){

        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rebate On Advance').getRecordTypeId();
        Case caseObj = new Case(Status='Draft Request', RecordTypeID=caseRecTypeId);
        insert caseObj;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController( caseObj );
        CollectionMemoDocGenCtrl ctrl = new CollectionMemoDocGenCtrl( stdCtrl );
        CollectionMemoDocGenCtrl.generateCollectionMemo();
        Test.stopTest();

    }
}