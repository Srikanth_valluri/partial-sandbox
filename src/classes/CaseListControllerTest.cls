@isTest
private class CaseListControllerTest {

    @isTest static void CaseListTest() {
        user communityUser = CommunityTestDataFactory.createPortalUser();
        User objUser = [select id,contact.accountId,contactID from user where id =: communityUser.id ];
        String RecTypeId= [select Id from RecordType where (Name='POP') and (SobjectType='Case')].Id;

        update new Account(Id = objUser.Contact.AccountId, AnnualRevenue = 1000);

        System.runAs(communityUser){
            Case objCase = TestDataFactory_CRM.createCase(objUser.contact.accountId,RecTypeId);
            objCase.SR_Type__c = 'proof of payment sr';
            objCase.contactID = objUser.contactId;
            insert objCase;
            Case cObj =  [SELECT Id, Status from CASE where Id = :objCase.Id LIMIT 1];
            System.debug(' cObj Creation = ' + cObj );
            System.debug(' cObj Status= ' + cObj.Status );
            cObj.Status = 'Submitted';
            update cObj;
            System.Test.StartTest();
                CustomerCommunityUtils.customerAccountId = objUser.Contact.AccountId;
                CaseListController obj = new CaseListController();
                obj.customerAccountId = CustomerCommunityUtils.customerAccountId;
                CaseListController.createCaseComment(objCase.id,'Test Comment');
                CaseListController.switchCaseView(objCase.id,'back');
                CaseListController.switchCaseView(objCase.id,'main');
                CaseListController.deleteCase(objCase.id);
            System.Test.StopTest();
        }

    }

}