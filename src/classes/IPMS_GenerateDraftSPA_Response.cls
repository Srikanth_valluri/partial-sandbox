/************************************************************************************************
 * @Name              : IPMS_GenerateDraftSPA_Response
 * @Test Class Name   : IPMS_REST_Generate_Draft_SPA_Test
 * @Description       : Response JSON deserializing class for Draft SPA
 * Modification Log
 * 1.0    QBurst    29/01/2020        Created Class
 ************************************************************************************************/

public class IPMS_GenerateDraftSPA_Response{
    public String responseId;   //6493
    public String responseTime; //Sun Jul 21 16:30:58 GMT+04:00 2019
    public String status;   //S
    public String responseMessage;  //Process Completed...
    public Integer elapsedTimeMs;   //685
    public cls_responseLines[] responseLines;
    public boolean complete;
    public class cls_responseLines {
        public String language;   //EN
        public String documentName; //SPA[AGSX]
        public String url; 
    }
    
    /****************************************************************************
    * @Description : execute method for Queueable Context                       
    * @Params      : String json - json response received foe webservie callout 
    *                               of Generate Draft SPA                       
    * @Return      : IPMS_GenerateDraftSPA_Response - deserialized response     
    ****************************************************************************/
    public static IPMS_GenerateDraftSPA_Response parse(String json){
        return (IPMS_GenerateDraftSPA_Response) System.JSON.deserialize(json, 
                                                    IPMS_GenerateDraftSPA_Response.class);
    }

   
}