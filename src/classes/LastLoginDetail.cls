public without sharing class LastLoginDetail{
    public datetime lastLogin {get;set;}
    public LastLoginDetail(ApexPages.StandardController stdCon){
        lastLogin = null;
        findLastLoginInfo(stdCon.getId());
    }
    
    public void findLastLoginInfo(Id accountId){
        list<Account> lstA = [Select Id
                                     , isPersonAccount
                                     , PersonContactId 
                              from Account 
                              where Id =: accountId
                              and isPersonAccount = true
                              and PersonContactId != null];
        system.debug('lstA*****'+lstA);
        if(!lstA.isEmpty()){
            list<User> lstUser = [SELECT LastLoginDate
                                         , ContactId 
                                  FROM User 
                                  WHERE ContactId =: lstA[0].PersonContactId];
            system.debug('lstUser*****'+lstUser);
            if(!lstUser.isEmpty()){
                lastLogin = lstUser[0].LastLoginDate;
            }
            system.debug('lastLogin*****'+lastLogin);
        }
    }
}