/*
Created Date: 15/06/2020
Schedule Class: Damac_CardExpiryNotificationSch_Acc, Damac_CardExpiryNotificationSch_Con
Test Class: Damac_CardExpiryNotificationBatchTest
VisualForce Component: CardExpiryNotification
Controller related to VF Component: CardExpiryNotificationController
Description: Retrieve the Account and its related Contacts records based on criteria whose expiry date is in next 30 days
and send each Account Id to VF component to the template data and send that to user.
*/
global class Damac_CardExpiryNotificationBatch_Acc implements Database.Batchable<sObject>, Database.stateful {
    
  
    List<String> customLabelList = new List<String> (); 
  
    String cardExpiry = Label.CardExpiryNotification;
    
       
    EmailTemplate et = [SELECT Id, Name FROM EmailTemplate WHERE Name = 'Damac_CardExpiryNotification' LIMIT 1];
    
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        if (cardExpiry.contains (',')) {
            customLabelList = cardExpiry.split (',');
        } else {
            customLabelList.add (cardExpiry);
        }    
        
        String contactStatus = 'Cancelled';
        
        String query = 'SELECT Expired_Cards__c, Name, Agency_Email__c, owner.Email, '
                        + '(SELECT Expired_Cards__c, Name FROM Contacts WHERE Status__c !=: contactStatus'
                        + ' AND User_Type__c IN: customLabelList AND Expired_Cards__c != null'
                        + ') FROM Account WHERE Expired_Cards__c != null';
        
        return Database.getQueryLocator(query);
    }
    
    //Calling the sendMailToUsers() method and passing the Account as parameter
    global void execute(Database.BatchableContext bc, List<Account> recList) {
        for(Account acc : recList) {
            sendMailToUsers(acc.Id, acc.Owner.Email);
            
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        
    }
    
    //Send the Account Id to VF email template and retrieve the content of email and sending to user
    public void sendMailToUsers(Id accId, String accEmail) {
        List<String> sendTo = new List<String>();
        if(accEmail != null) {
            sendTo.add(accEmail);            
        }
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        mail.setToAddresses(sendTo);
        mail.setSenderDisplayName(Userinfo.getname());
        mail.setTreatTargetObjectAsRecipient(false);
        mail.setTargetObjectId(userinfo.getUserId());
        mail.setWhatId(accId);
        mail.setSaveAsActivity(false);
        mail.setTemplateId(et.Id);   
        mails.add(mail);
        Messaging.sendEmail(mails);
    }    
}