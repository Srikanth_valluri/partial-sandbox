/**************************************************************************************************
* Name               : MergePartiesBatchHelper                                                    *
* Description        : Controller class to Merge Parties and their child to master Party          *
* Created Date       : 26/01/2018                                                                 *
* Created By         : Srikanth                                                                   *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR            DATE            COMMENTS                                          *
* 1.0         Srikanth         26/01/2018      Initial Draft.                                     *
**************************************************************************************************/
global without sharing class MergePartiesBatchHelper implements Database.Batchable <SObject>, Database.Stateful {
    
    global Account masterAccount;
    global List <String> mergeableParties;
    /*********************************************************************************************
    * @Description : Controller class.                                                           *
    * @Params      : master Party Id and related mergable account Ids                            *
    * @Return      : void                                                                        *
    *********************************************************************************************/ 
    global MergePartiesBatchHelper (Account masterParty, List <String> mergeablePartiesList) {
        masterAccount = masterParty;
        mergeableParties = mergeablePartiesList;
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC) {
        String query = ' SELECT Party_Id__c FROM Account WHERE Party_Id__c IN :mergeableParties';
        return Database.getQueryLocator (query);
    }
    
    global void execute (Database.BatchableContext BC, List <Account> scope){
        Database.MergeResult[] results = Database.merge(masterAccount, scope, false);
        String response = '';
        List <String> mergedAccountIds = new List <String> ();
        
        for (Database.MergeResult res : results) {
            if (res.isSuccess()) {
                response = 'Success';
                mergedAccountIds = res.getMergedRecordIds();
                system.debug('mergedAccountIds'+mergedAccountIds);
            }
            else {
                for(Database.Error err : res.getErrors()) 
                    response = err.getMessage();
            }
        }
        System.Debug (response);
        masterAccount = [ SELECT Party_ID__c,(Select Id,FirstName,LastName,Email From Contacts) FROM ACCOUNT WHERE ID =: masterAccount.ID];
        System.Debug (masterAccount.Party_ID__c);
        
        if (mergedAccountIds.size () > 0) {            
            List <Booking__c> bookingsToUpdate = new List <Booking__c> ();
            for (Booking__c booking :[ SELECT Account__c, Party_ID__c FROM Booking__c WHERE  ACCOUNT__C =: masterAccount.ID ]) {
                booking.Account__c = masterAccount.id;
                booking.Party_ID__c = masterAccount.Party_ID__c ;
                bookingsToUpdate.add (booking);
            }
            update bookingsToUpdate;
            system.enqueueJob(new MergePartyIdsUpdate (masterAccount.ID));            
        }
        if (response == 'Success') {
            List <Merge_History__c> mergedAccountsList = NEW List <Merge_History__c > ();
            if (scope.size () > 0) {
                for (Account key :scope) {
                    
                    Merge_History__c mergedAccounts = new Merge_History__c ();
                    mergedAccounts.Master_Account__c = masterAccount.id;
                    mergedAccounts.Master_Party_ID__c = masterAccount.Party_Id__c;
                    mergedAccounts.Merged_Party_Id__c = key.Party_Id__c;
                    mergedAccountsList.add (mergedAccounts);
                    
                }
            }
            
            insert mergedAccountsList;
            /*List<User> lstNewUser = new List<User>();
            Id profileId = [select id from profile where name = 'Customer Community Login User(Use this)'].id;


            List<User> lstUserMaster = [SELECT IsActive, IsPortalEnabled ,Name,UserRoleId,Username,Title,localesidkey 
                                       , Party_Id__c,Phone,ManagerId,EmployeeNumber
                                       , IPMS_Employee_ID__c,Address,email ,emailencodingkey ,languagelocalekey
                                       , ContactId ,timezonesidkey ,FirstName ,LastName ,ProfileId,Alias
                                    FROM User
                                   where Party_Id__c =: masterAccount.Party_ID__c  ];
            system.debug(' lstUserMaster 1 : ' + lstUserMaster);
                                     
            if( lstUserMaster.size() > 0 ) {
                lstUserMaster[0].Party_Id__c = '';
                lstUserMaster[0].IPMS_Employee_ID__c = '';
                system.debug(' lstUserMaster 2 : ' + lstUserMaster);
                update lstUserMaster[0];                
            }

            system.debug(' lstUserMaster 3 : ' + lstUserMaster);
            system.debug(' masterAccount :  '+ masterAccount);
            system.debug(' masterAccount.Contacts[0] :  '+ masterAccount.Contacts[0]);
            User objUserClone = new User();
            objUserClone.IsActive = true;
            //objUserClone.IsPortalEnabled = true;
            String aliasVal = String.valueOf(masterAccount.Contacts[0].Id);
            objUserClone.alias = aliasVal.substring(0, 5);
            objUserClone.Email = masterAccount.Contacts[0].Email != null ? masterAccount.Contacts[0].Email : 'test@test.com' ;
            objUserClone.emailencodingkey = 'UTF-8';
            objUserClone.timezonesidkey = 'Asia/Dubai';
            objUserClone.FirstName = masterAccount.Contacts[0].FirstName != '' ? masterAccount.Contacts[0].FirstName : 'Test First Name' ;
            objUserClone.LastName = masterAccount.Contacts[0].LastName != '' ? masterAccount.Contacts[0].LastName : 'Test Last Name' ;
            objUserClone.ProfileId = profileId;
            objUserClone.languagelocalekey = 'en_US';
            objUserClone.localesidkey = 'en_GB';
            objUserClone.Party_Id__c = masterAccount.Party_ID__c;
            objUserClone.IPMS_Employee_ID__c = masterAccount.Party_ID__c;
            objUserClone.ContactId = masterAccount.Contacts[0].Id;
            objUserClone.username = getUserName(masterAccount.Contacts[0]);
            
            system.debug(' objUserClone:  '+ objUserClone);
            
            
            lstNewUser.add(objUserClone);
            
            
            insert lstNewUser;
            */
            UpdateUserForMergeParties.UpdateUsers(masterAccount);            
            
        }
    }
    /*public static String randomWithLimit(Integer upperLimit) {
        Integer rand = Math.round(Math.random() * 1000);
        return String.valueOf(Math.mod(rand, 9999)).leftpad(4, '0');
    }
    
    public static string getUserName(Contact c) {
        String username = '';
        if (c.FirstName != NULL) {
            username += c.FirstName.replaceAll( '\\s+', '');
        }
        if (c.LastName != NULL) {
            username += c.LastName.replaceAll( '\\s+', '');
        }
        username += randomWithLimit(9999);

        username += '@damacagents.com';
        return username;
    }   */
    global void finish (Database.BatchableContext BC) {
    
    }
    
}