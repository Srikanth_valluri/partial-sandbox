/****************************************************************************************************
* Name               : AssociatePaymentPlanController
* Description        : Controller class for Associate Payment Plan VF Page
* Created Date       : 16/10/2020
* Created By         : QBurst
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst         16/10/2020      Initial Draft
****************************************************************************************************/

global with sharing class AssociatePaymentPlanController {
    public List<String> propertyNames {get; set;}
    public string propertyId {get; set;}
    public boolean propertyExists {get; set;}
    public string propertyName {get; set;}
    public Map<String, Property__c> propertyMap;
    public static string buildingDetails {get;set;}

    /*********************************************************************************************
    * @Description : Constructor method
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public AssociatePaymentPlanController() {
        propertyNames = new List<String>();
        propertyName = '';
        propertyExists = false;
        propertyId = ApexPages.currentPage().getParameters().get('Id');

        List<BuildingDetailsWrapper> wrapperList = new List<BuildingDetailsWrapper>();
        buildingDetails = JSON.serialize(wrapperList);
        propertyMap = new  Map<String, Property__c>();
        for(Property__c property: [SELECT Id, Property_Name__c, Property_ID__c 
                                   FROM Property__c 
                                   WHERE Property_Name__c != ''
                                   ORDER BY Property_Name__c ASC]){
            propertyMap.put(property.Property_Name__c, property);
            propertyNames.add(property.Property_Name__c);
        }
        if(propertyId != null && propertyId != ''){
            for(Property__c property: [SELECT Id, Property_Name__c, Property_ID__c 
                                   FROM Property__c 
                                   WHERE Property_Name__c != ''
                                   AND Id =: propertyId]){
                propertyName = property.Property_Name__c;
                fetchPropertyDetails(propertyId);
                propertyExists = true;
            }
        }
    }

    public void fetchBuildingDetails(){
        String projName = ApexPages.currentPage().getParameters().get('projectName');
        Property__c property =  propertyMap.get(projName);
        system.debug('property: ' + property);
        fetchPropertyDetails(property.Id);
    }

    public void fetchPropertyDetails(String propertyId){
         Map<Id, Location__c> locationMap = new Map<Id, Location__c>();
         Map<Id, Boolean> locationPayPlanMap = new Map<Id, Boolean>();
         Map<Id, Integer> locationFloorCountMap = new Map<Id, Integer>();
         Map<Id, List<Inventory__c>> locationInventoryMap = new Map<Id, List<Inventory__c>>();
         List<BuildingDetailsWrapper> wrapperList = new List<BuildingDetailsWrapper>();
         for(Location__c loc: [SELECT Id, Name, Building_Name__c 
                               FROM Location__c 
                               WHERE Location_Type__c = 'Building'
                               AND Property_Name__c =: propertyId]){
             locationMap.put(loc.Id, loc);
             locationPayPlanMap.put(loc.Id, false);
         }
         system.debug('locationMap: '+ locationMap);
         for(Payment_Plan_Association__c planAssoc : [SELECT Id, Payment_Plan__c, Location__c
                                                     FROM Payment_Plan_Association__c
                                                     WHERE Location__c IN: locationMap.keyset()]){
             locationPayPlanMap.put(planAssoc .Location__c, true);
         }
         for(Location__c loc: [SELECT Id, Name, Building_Name__c , Building_Number__c
                               FROM Location__c 
                               WHERE Location_Type__c = 'Floor'
                               AND Building_Number__c IN: locationMap.keyset()]){
             Integer count = 0;
             if(locationFloorCountMap.containsKey(loc.Building_Number__c)){
                 count = locationFloorCountMap.get(loc.Building_Number__c);
             }
             count++;
             locationFloorCountMap.put(loc.Building_Number__c, count);
         }
         system.debug('locationFloorCountMap: '+ locationFloorCountMap);
         for(Inventory__c inv: [SELECT Id, Status__c, Building_Location__c 
                                FROM Inventory__c 
                                WHERE Building_Location__c IN: locationMap.keyset()]){
             List<Inventory__c> invList = new List<Inventory__c>();
             if(locationInventoryMap.containsKey(inv.Building_Location__c)){
                 invList = locationInventoryMap.get(inv.Building_Location__c);
             }
             invList.add(inv);
             locationInventoryMap.put(inv.Building_Location__c, invList);    
         }
         system.debug('locationInventoryMap: '+ locationInventoryMap);
         for(Location__c loc: locationMap.values()){
             Integer units = 0;
             Integer inventory = 0;
             Integer available = 0;
             Integer sold = 0;
             Integer restricted = 0;
             Integer floors = 0;
             Boolean payPlan = false;
             if(locationPayPlanMap.containsKey(loc.Id)){
                 payPlan = locationPayPlanMap.get(loc.Id);
             }
             if(locationFloorCountMap.containsKey(loc.Id)){
                 floors = locationFloorCountMap.get(loc.Id);
             }
             if(locationInventoryMap.containsKey(loc.Id)){
                 for(Inventory__c inv: locationInventoryMap.get(loc.Id)){
                     units++;
                     if(inv.Status__c == 'AVAILABLE'){
                         available++;
                     } else if(inv.Status__c == 'Inventory'){
                         inventory++;
                     } else if(inv.Status__c == 'Sold'){
                         sold++;
                     } else if(inv.Status__c == 'Restricted'){
                         restricted++;
                     }
                 }
             }
             BuildingDetailsWrapper details = new BuildingDetailsWrapper(loc.Building_Name__c, loc.Id, payPlan, units, 
                                                                 inventory, available, sold, restricted, floors);
             system.debug('details'+details);
             
             wrapperList.add(details);
             system.debug('wrapperList'+wrapperList);
         }    
         buildingDetails = JSON.serialize(wrapperList);
         system.debug('buildingDetails: ' + buildingDetails);
    }

    public class BuildingDetailsWrapper{
        public string building;
        public string buildingId;
        public string plan;
        public Integer units;
        public Integer inventory;
        public Integer available;
        public Integer sold;
        public Integer restricted;
        public Integer floors;
        public BuildingDetailsWrapper(){
        }
        public BuildingDetailsWrapper(String buildingName, String bId,
                                        Boolean planPresent, Integer unitsCount, 
                                        Integer inventoryCount, Integer availableCount, 
                                        Integer soldCount, Integer restrictedCount,
                                        Integer floorsCount){
            building = buildingName;
            buildingId = bId;
            if(planPresent){
                plan = 'plan available';
            } else{
                plan = 'plan NOT available';
            }
            units = unitsCount;
            inventory = inventoryCount;
            available = availableCount;
            sold = soldCount;
            restricted = restrictedCount;
            floors = floorsCount;
            system.debug('res: ' + restricted);
        }
    }
}