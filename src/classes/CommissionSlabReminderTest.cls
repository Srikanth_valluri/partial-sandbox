/************************************************************************************************
 * @Name              : CommissionSlabReminderTest
 * @Description       : Its a unit test class for CommissionSlabReminder Scheduler and Batch
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log
 * 1.0         QBurst         17/11/2020       Created
***********************************************************************************************/

@IsTest
Public Class CommissionSlabReminderTest{
    
    public static testmethod void unittestmethod1(){
        Email_Notification_To_CC_List__c toCCList = new Email_Notification_To_CC_List__c();
        toCCList.Name = 'Commission Slab Update';

        toCCList.To_Addresses__c = 'test@test1.com';
        toCCList.cc_Addresses__c = 'test@test2.com';
        toCCList.bcc_Addresses__c = 'test@test3.com';
        
        insert toCCList;
        Id AgencyRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        

            Account A1 = new Account(Name = 'Test Account', Agency_Type__c = 'Corporate',Agent_Client_Base_Country__c ='Aruba;india;');
            insert A1;
            List<Contact> conList = new List<Contact>();
            Contact ownerCon = new Contact( AccountID = A1.id, FirstName = 'Test', LastName = 'User1', Owner__c = true,
                                     email = 'test-user@testuser123.com', Birthdate=system.today().addDays(7) );
            conList.add(ownerCon);
            Contact signatoryCon = new Contact( AccountID = A1.id, FirstName = 'Test', LastName = 'User2', Authorised_Signatory__c = true,
                                     email = 'test-user@testuser123.com' );
            conList.add(signatoryCon);
            Contact C1 = new Contact( AccountID = A1.id, FirstName = 'Test', LastName = 'User3', 
                                     email = 'test-user@testuser1234.com' );
            conList.add(c1);
            insert conList; 
            
            Agency_PC__c agencyPc = new Agency_PC__c();
            agencyPc.Agency__c = a1.Id;
            agencyPc.User__c = userinfo.getUserId();
            insert agencyPc;
            a1.recordTypeId = AgencyRecordTypeId;   
            a1.Agency_Short_Name__c = 'testShrName';
            a1.Party_ID__c = '769875';
            a1.Email__c = 'test@gmail.com';
            a1.Mobile__c = '7894561230';
            a1.Title__c = 'Ms.';
            a1.Passport_Number__c = '123654';
            update a1;
            
            New_Partnership_Program__c prg = new New_Partnership_Program__c();
            prg.Account__c = a1.Id;
            prg.Current_commission_slab__c = '10%';
            insert prg; 
            
            test.startTest();
            CommissionSlabReminderScheduler schObj = new CommissionSlabReminderScheduler();
            String sch = '0 0 23 * * ?'; 
            system.schedule('Test Scheduler', sch, schObj); 
            CommissionSlabReminderBatch reminderBatch = new CommissionSlabReminderBatch();
            Database.executeBatch(reminderBatch); 
            test.stopTest();
    }
}