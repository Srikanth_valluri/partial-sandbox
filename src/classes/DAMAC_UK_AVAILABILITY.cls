public class DAMAC_UK_AVAILABILITY{
    
    
    public transient List<inventory__c> ukInventory {get;set;}
    public transient list<string> idsselected{get;set;}
    public transient list<string> idsInvSelected{get;set;}
    public string selectedInventoryIdsForBooking {get;set;}

    public Account searchAcc        {get; set;}
    public Inquiry__c searchInq     {get; set;}
    public String emailSubject     {get; set;}
    public String emaiBody          {get; set;}

    public boolean ismobile{get;set;}
    public DAMAC_UK_AVAILABILITY(){
    
        List<inventory_user__c> userAssignedInventory = new List<inventory_user__c>();
        
        userAssignedInventory = [select id,inventory__c from inventory_user__c where user__c=:userinfo.getuserid() and inventory__r.status__c='Released'];
        set<id> userAssignedInventoryIds = new set<id>();
        for(inventory_user__c iu:userAssignedInventory ){
            userAssignedInventoryIds.add(iu.inventory__c);
        }
        ukInventory = new List<inventory__c>();
        
        ukInventory = [select id,Special_Price_Tax_Amount__c,Selling_Price_excl_VAT__c,UP_Label__c,FP_Label__c,PP_Label__c,Custom_Unit_Plan__c,Custom_Floor_Plan__c,Custom_Plot_Plan__c,Use_Custom_Plot_Plan__c,Use_Custom_Floor_Plan__c,Use_Custom_Unit_Plan__c,Area__c ,View_Type__c,Brand__c,Rera_Percentage__c,Parking__c,Floor_Package_Name__c ,Plot_Area__c ,Building_Name__c,Building_ID__c,Location_Code__c ,IPMS_Bedrooms__c ,Bedrooms__c ,Area_sft__c ,Price_Per_Sqft__c ,Property_Country__c ,Plot_Area_sft__c ,Status__c ,List_Price_calc__c ,Floor_Plan__c,Plot_Plan__c,Special_Price_calc__c ,Unit_Plan__c,name,Marketing_name__c,Building_Location__r.Name,Anticipated_Completion_Date__c,property_name__c,Area_Sqft_2__c,
                        Property_Status__c,Location_Type__c,Special_Price__c,Currency_of_Sale__c,Property_city__c,Unit_Type__c,Unit_Location__r.Name,Bedroom_Type__c,Marketing_Name_Doc__r.Marketing_Plan_Label__c,Marketing_Name_Doc__r.Marketing_Plan__c,Marketing_Name_Doc__r.Primary_Image__c, Property__r.Property_Plan__c,Property__r.Property_Plan_Label__c
                        from inventory__c 
                        where status__c='Released' 
                        and 
                        ( Is_Assigned__c=False OR ID IN:userAssignedInventoryIds ) 
                        and property_Country__c='UNITED KINGDOM'];
        
    
    }
    @remoteAction
    public static String createFormRequest(String selectedUnits) {
        try {
            List <String> invIds = New List <String> ();
            if (selectedUnits.contains (',')) {
                invIds = selectedUnits.split (',');
            } else {
                invIds.add (selectedUnits);
            }
            String inventoryIds = '';
            String unitCodes = '';
            Map<Id,String> invWithUnitName = new Map<Id,String>();
            for (Inventory__c inv : [SELECT ID,Unit_name__c FROM Inventory__c WHERE ID IN : invIds order BY CreatedDate DESC]) {
                inventoryIds += inv.Id+',';
                unitCodes += inv.Unit_name__c+',';
                invWithUnitName.put(inv.Id,inv.Unit_Name__c);
            }
            inventoryIds = inventoryIds.removeEnd (',');
            unitCodes = unitCodes.removeEnd (',');
            Map <String, List <Damac_PromotionsService.Promotions>> promotionResponse = new Map <String, List <Damac_PromotionsService.Promotions>> ();
            
            try {
                promotionResponse  = Damac_PromotionsService.promotionsRequest (unitCodes);
                system.debug ('promotionResponse'+promotionResponse);
            } catch (Exception e) {}
            
            Form_Request__c formRequest = new Form_request__c ();
            Id ukformId = Schema.SObjectType.Form_Request__c.getRecordTypeInfosByName().get('UK Form').getRecordTypeId();
            formRequest.RecordTypeId = ukformId ;
            formRequest.Selected_Inventory_Ids__c = inventoryIds ;
            insert formRequest;
            
            List <Form_request_Inventories__c> formInvs = new List <Form_request_Inventories__c> ();
            for (String invId :invIds) {
                if (invId != '') {
                    Form_request_Inventories__c formInv = new Form_request_Inventories__c ();
                    formInv.Inventory__c = invId;
                    formInv.Form_request__c = formRequest.Id;
                    formInvs.add (formInv);
                }
            }
            insert formInvs;
            List <Available_Options__c> availableOptionsList = new List <Available_Options__c> ();
            
            System.debug('Came here');
            for(Form_request_Inventories__c  eachFrInv : formInvs){
                for (String key :promotionResponse.keySet ()) {
                    for (Damac_PromotionsService.Promotions promotion : promotionResponse.get (key)) {
                        System.debug('invWithUnitName.get(eachFrInv.Inventory__c)'+invWithUnitName.get(eachFrInv.Inventory__c));
                        System.debug('promotion.unitCode'+promotion.unitCode);
                        if(invWithUnitName.get(eachFrInv.Inventory__c) != null && invWithUnitName.get(eachFrInv.Inventory__c) == promotion.unitCode){
                            Available_Options__c availableOptions = new Available_Options__c ();
                            availableOptions.Promotion_Option__c = promotion.promotion;
                            availableOptions.TemplateIdPN__c = promotion.promotionId;
                            availableOptions.Type_of_Option__c = 'Promotion';
                            availableOptions.Form_Request_Inventory__c = eachFrInv.Id;
                            availableOptionsList.add (availableOptions); 
                        }  
                    }
                }     
            }
            insert availableOptionsList;
            return formRequest.Id;
        } catch (Exception e) {
            return e.getMessage ()+' '+e.getLineNumber();
        }
    }

    @RemoteAction 
    public static List<Account> getAccounts(String pSearchKey) {
        String searchString = '%'
                            + pSearchKey 
                            + '%';
        List<Account> accountList = new List<Account>();
        accountList = [ SELECT Id, Name, Email__c
                          FROM Account 
                         WHERE OwnerId = :UserInfo.getUserId()
                           AND Name LIKE :searchString 
        ]; 
        System.debug('accountList:::: '+accountList);
        return accountList;
    }

    @RemoteAction 
    public static List<Inquiry__c> getInquiries(String pSearchKey) {
        String searchString = '%'
                            + pSearchKey 
                            + '%';
        List<Inquiry__c> inquiryList = new List<Inquiry__c>();
        inquiryList = [ SELECT Id, Name, First_name__c, Last_Name__c, Email__c 
                        FROM Inquiry__c 
                        WHERE OwnerId = :UserInfo.getUserId()
                        AND ( First_Name__c LIKE :searchString 
                            OR Last_Name__c LIKE :searchString 
                            OR Name LIKE :searchString 
                        )
        ]; 
        System.debug('inquiryList:::: '+inquiryList);
        return inquiryList;
    }

    public void sendemailfun(){
        Id offerid = Apexpages.currentPage().getParameters().get('salesofferrecordid');
        Id entityId = Apexpages.currentPage().getParameters().get('entityId');
        Id shareprojectdetailid = Apexpages.currentPage().getParameters().get('shareprojectdetailid');
        ID jobID = System.enqueueJob(new Damac_ShareProjectDetails (offerId, entityId, shareprojectdetailid));
        //sendemailmodalact(offerid,entityID,shareprojectdetailid);
        //System.debug('offerid@@' + offerid.getsobjecttype());
        //System.debug('entityID@@' + entityID.getsobjecttype());
    }

}