/******************************************************************************************************************************************
Description: Class to send reminders for Welcome Calling List
===========================================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By  | Comments
-------------------------------------------------------------------------------------------------------------------------------------------
1.0     | 18-01-2020        | Aishwarya Todkar  | 1. Initial draft
*******************************************************************************************************************************************/
public class WelcomeCLReminder {
    
    static Map<String, SendGrid_Email_Details__mdt> mapSendGridDetails;
    static Map<String,EmailTemplate> mapEmailTemplates;
    static Map<String, String> mapUserNametoEmail;
    static List<EmailMessage> lstEmails;
    static List<Calling_List__c> listWelcomeClToUpdate;
    static Set<String> setEmailIdsToExclude;

    @InvocableMethod(label='sendReminder' description='Send Welcome call outcome reminders')
    public static void sendReimder( List<Id> Ids ) {
        sendReimderAsync( Ids );
    }
    @future( callout = true )
    public static void sendReimderAsync( List<Id> Ids ) {
        
        //Sending 24 hrs, 48 hrs and 72 hrs reminders
        Set<Date> setReminderDates = new Set<Date>();
        setReminderDates.add( system.today() );
        setReminderDates.add( system.today().addDays( -1 ) );
        setReminderDates.add( system.today().addDays( -2 ) );

        //Set of welcome call outcomes
        Set<String> welcomCallOutcomes = new Set<String>{ 
            'Unreachable / Not answering / Switched off'
            , 'Invalid Phone Number'
            , 'Invalid Email'
            , 'Invalid Address'
        };

        //Set of Email ids to exclude
        setEmailIdsToExclude = new Set<String>();
        setEmailIdsToExclude.addAll( Label.Welcome_CL_Excluded_Mails.split( ',' ) );
                                                                    
        String sQry = 'SELECT';
        sQry += ' Id, OwnerId, PC_Email__c, Reminder_Date__c, Call_Count__c, Welcome_Call_Outcome__c, Booking_Unit__r.Unit_Name__c,'
            + ' Booking_Unit__r.DOS_Name__c, Booking_Unit__r.HOS_Name__c, Booking_Unit__r.HOD_Name__c, Owner.Email'
            + ' FROM Calling_List__c'
            + ' WHERE RecordType.Name = \'Welcome Calling List\''
            + ' AND ( Call_Count__c = null OR Call_Count__c < 3 ) '
            + ' AND Welcome_Call_Outcome__c != null AND Welcome_Call_Outcome__c IN : welcomCallOutcomes AND Booking_Unit__c != null' 
            + ' AND Reminder_Date__c != null AND Reminder_Date__c IN : setReminderDates ' 
            + ' AND ( PC_Email__c != null OR Booking_Unit__r.DOS_Name__c != null OR Booking_Unit__r.HOS_Name__c != null OR Booking_Unit__r.HOD_Name__c != null )';
            
        //To test the single record
        if( String.isNotBlank ( Label.Record_Id_To_Test ) 
            && Label.Record_Id_To_Test.toLowerCase().contains('on') 
            && String.isNotBlank( Label.Record_Id_To_Test.substringAfter('-') )
            && Label.Record_Id_To_Test.substringAfter('-').startsWith( 'a37') 
            && !Test.isRunningTest() ) {
                sQry = sQry + ' AND Id = \'' + Label.Record_Id_To_Test.substringAfter('-') + '\'  ';
        }
        else {
            sQry = sQry + ' AND Id IN : Ids'; 
        }

        List<Calling_List__c> listWelcomeCl = new List<Calling_List__c>();
        lstEmails = new List<EmailMessage>();
        listWelcomeClToUpdate = new List<Calling_List__c>();

        listWelcomeCl = Database.query( sQry );
        System.debug( 'listWelcomeCl==' + listWelcomeCl);
        
        //Call method to get email templates
        getConfigs();

        //Call method to get email Ids of users
        getUserEmailIds( listWelcomeCl );

        //Call method to send reminder        
        for( Calling_List__c objCl : listWelcomeCl ) {
            sendmail( objCl );
        }
        //Update call count on calling list
        if( !listWelcomeClToUpdate.isEmpty() ) {
            update listWelcomeClToUpdate;
        }

        //insert email message
        if( !lstEmails.isEmpty() && !Test.isRunningTest() ) {
            insert lstEmails;
        }
    }

    public static void getConfigs() {
        Set<String> setEmailTemplateNames = new Set<String>();
        mapSendGridDetails= new Map<String,SendGrid_Email_Details__mdt>();
        mapEmailTemplates = new Map<String,EmailTemplate>();

        //get configured sendrid details
        for(SendGrid_Email_Details__mdt mdt : [SELECT
                                                    MasterLabel
                                                    , From_Address__c
                                                    , From_Name__c
                                                    , To_Address__c
                                                    , To_Name__c
                                                    , CC_Address__c
                                                    , CC_Name__c
                                                    , Bcc_Address__c
                                                    , Bcc_Name__c
                                                    , Reply_To_Address__c
                                                    , Reply_To_Name__c
                                                    , Email_Template__c
                                                FROM
                                                    SendGrid_Email_Details__mdt
                                                WHERE
                                                    MasterLabel Like '%Welcome Calling List Reminder%'
                                                ] ) {
            if( mdt.MasterLabel.contains( '24' ) ) {
                mapSendGridDetails.put( '24', mdt );
                setEmailTemplateNames.add( mdt.Email_Template__c );
            }
            else if( mdt.MasterLabel.contains( '48' ) ) {
                mapSendGridDetails.put( '48', mdt );
                setEmailTemplateNames.add( mdt.Email_Template__c );
            }
            else if( mdt.MasterLabel.contains( '72' ) ) {
                mapSendGridDetails.put( '72', mdt );
                setEmailTemplateNames.add( mdt.Email_Template__c );
            }                                       
        }
        if( !setEmailTemplateNames.isEmpty() ) {
            //Get Email Template
            for( EmailTemplate templateObj : [SELECT 
                                                Id
                                                , Name  
                                                , Subject  
                                                , Body
                                                , HtmlValue
                                                , TemplateType
                                            FROM 
                                                EmailTemplate 
                                            WHERE 
                                                Name IN: setEmailTemplateNames
                                            ]) {
                if( mapSendGridDetails.get( '24' ).Email_Template__c.equalsIgnoreCase( templateObj.name ) ) {
                    mapEmailTemplates.put( '24', templateObj );
                }
                else if( mapSendGridDetails.get( '48' ).Email_Template__c.equalsIgnoreCase( templateObj.name ) ) {
                    mapEmailTemplates.put( '48', templateObj );
                }
                else if( mapSendGridDetails.get( '72' ).Email_Template__c.equalsIgnoreCase( templateObj.name ) ) {
                    mapEmailTemplates.put( '72', templateObj );
                }                                            
            }
        }
    }

/******************************************************************************************************************************************
Description : Method to get User EmailIds
Parameter(s): List of Calling lists
Return      : void
*******************************************************************************************************************************************/
    public static void getUserEmailIds( List<Calling_List__c> listWelcomeCl ) {
        
        Set<String> userNames = new Set<String>();
        for( Calling_List__c objCl : listWelcomeCl ) {
            
            if( String.isNotBlank ( objCl.Booking_Unit__r.DOS_Name__c ) ) {
                userNames.add( objCl.Booking_Unit__r.DOS_Name__c ); 
            }
            if( String.isNotBlank ( objCl.Booking_Unit__r.HOS_Name__c ) ) {
                userNames.add( objCl.Booking_Unit__r.HOS_Name__c );
            }
            if( String.isNotBlank ( objCl.Booking_Unit__r.HOD_Name__c ) ) {
                userNames.add( objCl.Booking_Unit__r.HOD_Name__c );
            }
        }

        if( !userNames.isEmpty() ) {
            mapUserNametoEmail = new Map<String,String>();
            for( User objUser : [ SELECT 
                                        Email
                                        , Name 
                                    FROM 
                                        User
                                    WHERE
                                        isActive = true
                                    AND
                                        Name IN : userNames
                                    AND 
                                        Email NOT IN : setEmailIdsToExclude ] ) {
                mapUserNametoEmail.put( objUser.Name, objUser.Email );                   
            }
        }
    }

/******************************************************************************************************************************************
Description : Method to send reminder emails
Parameter(s): Calling Lists instance
Return      : void
*******************************************************************************************************************************************/
    //@future( callout = true)
    public static void sendmail( Calling_List__c objCl ) {

        // Calling_List__c objCl = new Calling_List__c();
        // objCl = [ SELECT
        //                 Id
        //                 , Booking_Unit__r.DOS_Name__c
        //                 , Booking_Unit__r.HOS_Name__c
        //                 , Booking_Unit__r.HOD_Name__c
        //                 , PC_Email__c
        //                 , Reminder_Date__c
        //                 , Owner.Email
        //                 , Call_Count__c
        //             FROM
        //                 Calling_List__c
        //             WHERE
        //                 Id =: ClId ];
        //Set toAddress
        String toAddress = '';
        if( mapUserNametoEmail != null && !mapUserNametoEmail.isEmpty() ) {
            if( String.isNotBlank( objCl.Booking_Unit__r.DOS_Name__c ) 
            && mapUserNametoEmail.containsKey( objCl.Booking_Unit__r.DOS_Name__c ) ) {
                toAddress = mapUserNametoEmail.get( objCl.Booking_Unit__r.DOS_Name__c ) + ',';
            }

            if( String.isNotBlank( objCl.Booking_Unit__r.HOS_Name__c ) 
            && mapUserNametoEmail.containsKey( objCl.Booking_Unit__r.HOS_Name__c ) 
            && !toAddress.contains( mapUserNametoEmail.get( objCl.Booking_Unit__r.HOS_Name__c ) ) ) {
                toAddress += mapUserNametoEmail.get( objCl.Booking_Unit__r.HOS_Name__c ) + ',';
            }

            if( String.isNotBlank( objCl.Booking_Unit__r.HOD_Name__c ) 
            && mapUserNametoEmail.containsKey( objCl.Booking_Unit__r.HOD_Name__c ) 
            && !toAddress.contains( mapUserNametoEmail.get( objCl.Booking_Unit__r.HOD_Name__c ) ) ) {
                toAddress += mapUserNametoEmail.get( objCl.Booking_Unit__r.HOD_Name__c ) + ',';
            }

            if( String.isNotBlank( objCl.PC_Email__c ) 
            && !toAddress.contains( objCl.PC_Email__c ) 
            && !setEmailIdsToExclude.contains(objCl.PC_Email__c ) ) {
                toAddress += objCl.PC_Email__c;
            }
        }
        toAddress.removeEnd( ',' );

        EmailTemplate objEmailTemplate;
        SendGrid_Email_Details__mdt objSendGridMdt;
        String strKey = objCl.Reminder_Date__c == System.today()? '24'
                    : objCl.Reminder_Date__c == System.today().addDays( -1 ) ? '48'
                    : objCl.Reminder_Date__c == System.today().addDays( -2 ) ? '72' : '';
        
        if( String.isNotBlank( strKey ) ) {
            if( mapEmailTemplates != null && mapEmailTemplates.containskey( strKey ) ) {
                objEmailTemplate = mapEmailTemplates.get( strKey);
            }
            if( mapSendGridDetails != null && mapSendGridDetails.containsKey( strKey ) ) {
                objSendGridMdt = mapSendGridDetails.get( strKey );
            }
            if( objEmailTemplate != null && objSendGridMdt != null ) {

                GenericUtility.SendGridWrapper sgWrap = new GenericUtility.SendGridWrapper();
        
                sgWrap = GenericUtility.prepareSendGridEmail( objSendGridMdt );
                
                sgWrap.toAddress = String.isNotBlank( sgWrap.toAddress ) ? toAddress + ',' + sgWrap.toAddress : toAddress;

                //Added by Aishwarya T on 18/01/2021
                sgWrap.ccAddress = String.isNotBlank( sgWrap.ccAddress ) ? objCl.Owner.Email + ',' + sgWrap.ccAddress : objCl.Owner.Email;
                        
                sgWrap.contentValue = MergeFieldReplacer.replaceMergeFields( objEmailTemplate.HtmlValue, 'Calling List', objCl.Id ) ;
                
                sgWrap.contentBody = MergeFieldReplacer.replaceMergeFields( objEmailTemplate.Body, 'Calling List', objCl.Id );
                
                sgWrap.subject = MergeFieldReplacer.replaceMergeFields( objEmailTemplate.Subject, 'Calling List', objCl.Id );
                
                sgWrap.listAttachment = new  List<Attachment>();
                
                sgWrap.relatedToId = objCl.Id;

                sgWrap.clId = objCl.Id;

                sgWrap.processType = 'Welcome Calling List Reminder';

                //Call generic method to send email via sendGrid
                List<EmailMessage> lstEmailsTemp = GenericUtility.sendEmailsBySendGrid( sgWrap );
                if( !lstEmailsTemp.isEmpty() ) {
                    lstEmails.addAll( lstEmailsTemp );
                    objCl.Call_Count__c = objCl.Call_Count__c == null ? 1 : objCl.Call_Count__c +1;
                    listWelcomeClToUpdate.add( objCl );
                }
            }
        }
    }
}