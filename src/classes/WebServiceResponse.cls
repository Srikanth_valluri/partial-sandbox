/************************************************************************************************
 * @Name              : WebServiceResponse
 * @Test Class Name   : 
 * @Description       : Apex Class to send universal response
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         Charan Vuyyuru 14/07/2020       Created
 ***********************************************************************************************/
global class WebServiceResponse {  
    webservice static Integer STATUS_CODE_SUCCESS = 200;
    webservice static Integer STATUS_ALLOTMENT_ERROR = 300;
    webservice static Integer STATUS_CODE_UNAUTHORIZED = 403;
    webservice static Integer STATUS_CODE_SYSTEM_ERROR = 500;
    public static final Integer STATUS_CODE_NOT_FOUND = 400;
    public static final Integer STATUS_CODE_OLD_VERSION = 101;
    //--- Generic Web-Service Messages
    public static final String SUCCESS_MESSAGE = 'Success';
    public static final String MESSAGE_SUCCESS = 'Success';  
    public static final String GENERIC_ERROR = 'An error has occured. Please contact your system administrator.';
    public static final String MESSAGE_GENERIC_ERROR = 'An error has occured. Please contact your system administrator.';
    
    webservice Integer statusCode {get;set;}
    webservice String message {get;set;}
    webservice Boolean success {get;set;}
    global Object data {get;set;}
    
    global WebServiceResponse(){
        this.statusCode = 200; 
        this.message = 'SUCCESS';
        this.success = true;
    }
    
    global String serialize () {
        return JSON.serialize(this);
    }
}