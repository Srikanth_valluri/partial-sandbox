public Class CRM_SurveySMSSender {
    
    public static void sendSurveySMS( String smsContents, String phoneNumber, Id accountId) {
        
        //Credentials_Details__c creds = RebateOnAdvanceService.getCredentials('CRM Survey SMS');

        /*if( creds != null && String.isNotBlank( creds.Endpoint__c ) ) {
            String userName = creds.User_Name__c; //crp1user
            String password = creds.Password__c; //oracle_user
            String endPoint = creds.Endpoint__c;
            String strSID = creds.Grant_type__c;*/

        // String biltyLink = new BitlyService().Shorten( Label.CRM_Survey_Site_Url );
        // System.debug( 'biltyLink-' + biltyLink );
        // smsContents = smsContents.replace('{Bitly Link}', biltyLink );

        // System.debug('smsContents 2-'+ smsContents);
            String userName = Label.Damac_CRM_SMS_Service_User_name;
            String password = Label.Damac_CRM_SMS_Service_Password;
            //String strSID = Label.Damac_CRM_SMS_Service_Sender_Id;
            String strSID = SMSClass.getSenderName(userName, phoneNumber, false);
    
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http http = new Http();
            req.setMethod('POST' ); // Method Type
            req.setEndpoint( Label.SMSCountryAPIBaseURL ); 
            string msgCont = GenericUtility.encodeChar( smsContents );
            system.debug('msgCont = ' + msgCont);
            
            
            req.setBody('user='+ userName + '&passwd=' + password +'&message=' 
                        + msgCont + '&mobilenumber=' + phoneNumber 
                        + '&sid='+ strSID + '&MTYPE=LNG&DR=Y'); // Request Parameters
            res = http.send(req);
            system.debug('req Body---'+req.getBody());
            system.debug('Response---'+res);
            system.debug('Response body---'+res.getBody());
            
            if( res.getBody() != null ){
                    
                // Parse Response
                SMS_History__c smsObj = new SMS_History__c();
                smsObj.Message__c = smsContents;
                smsObj.Phone_Number__c = phoneNumber;
                smsObj.Customer__c = accountId;
                //smsObj.Booking_Unit__c = objBU.Id;
                smsObj.Description__c = res.getBody();

                if(String.valueOf(res.getBody()).contains('OK:')){
                    smsObj.Is_SMS_Sent__c = true;
                    smsObj.sms_Id__c = res.getBody().substringAfter('OK:');
                }

                insert smsObj;
                System.debug('smsObj::::else:'+smsObj);
            }
       // }//End creds if
    }
}