/***************************************************************************************************************
* Name               : DocumentHelperTest                                                                      *
* Description        : Test class for DocumentHelper.                                       *
* Created Date       : 17/02/2019                                                                              *
* Created By         : CL                                                                                      *
* -------------------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                           *
* 1.0         CL          17/02/2019      Initial Draft.                                                       *
****************************************************************************************************************/
@isTest
private class DocumentHelperTest
{
  @isTest
  static void itShould()
  {
    NSIBPM__SR_Template__c objTemplate1 = new NSIBPM__SR_Template__c();
    objTemplate1.Name = 'Agent_Registration';
    objTemplate1.NSIBPM__Active__c = true;
    objTemplate1.NSIBPM__Estimated_Hours__c = 10;
    objTemplate1.NSIBPM__Menu__c = 'fdfvdvfdv fgdvfvff';  
    objTemplate1.NSIBPM__Menutext__c = 'fdfvdvfdv fgdvfvff dfzdvdfzv';
    objTemplate1.NSIBPM__SR_RecordType_API_Name__c = 'Agent_Registration';
    insert   objTemplate1;

    SR_Process__c objSRProcess = new SR_Process__c();
    objSRProcess.New_Document_Process__c = true;
    objSRProcess.Name = 'System Administrator';
    insert objSRProcess;
    String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
    Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
    User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                      LocaleSidKey='en_US', ProfileId = p.Id,
                      TimeZoneSidKey='America/Los_Angeles',
                      UserName=uniqueUserName);
    insert u;
    System.runAs(u) {

      NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
      RecordType rt = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'NSIBPM__Service_Request__c' AND Name = 'Agent Registration'];
      System.debug('rt--------->' + rt);
      //Id recType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Agent Registration').getRecordTypeId();
      objSR.Nationality__c = 'China';
      objSR.Country_of_Sale__c = 'UAE';
      objSR.Primary_Language__c = 'English';
      objSR.Agency_Type__c = 'Individual';
      objSR.ORN_Number__c = '23122345';
      objSR.RecordTypeId = rt.Id;
      objSR.DP_ok__c = true;
      objSR.DP_Overdue__c = false;
      objSR.Is_Submitted__c = false;
      objSR.NSIBPM__SR_Template__c = objTemplate1.Id;
      
      System.debug('objSR--------->' + objSR);
      /* ----------------------------------------------------------------------------------- */
      Document_Template__c objSRDocument1 = new Document_Template__c();
      objSRDocument1.In_Use__c = true;
      objSRDocument1.Conditions_Filter__c = '1 AND 2';
      objSRDocument1.Document_Name__c = 'Bank statement';
      objSRDocument1.Optional__c = false;
      objSRDocument1.SR_Record_Type__c = 'Agent_Registration';
      objSRDocument1.Save_Before_Submit__c = true;
      insert objSRDocument1;

      Document_Conditions__c objCondition1 = new Document_Conditions__c();
      objCondition1.Field_API_Name__c = 'Nationality__c';
      objCondition1.Field_Type__c = 'Text';
      objCondition1.Field_Value__c = 'China';
      objCondition1.Object_Name__c = 'NSIBPM__Service_Request__c';
      objCondition1.Operator__c = '=';
      objCondition1.Sr_No__c = 1;
      objCondition1.Document_Template__c = objSRDocument1.Id;
      insert objCondition1;

      Document_Conditions__c objCondition2 = new Document_Conditions__c();
      objCondition2.Field_API_Name__c = 'ORN_Number__c';
      objCondition2.Field_Type__c = 'Decimal';
      objCondition2.Field_Value__c = '2312245';
      objCondition2.Object_Name__c = 'NSIBPM__Service_Request__c';
      objCondition2.Operator__c = '!=';
      objCondition2.Sr_No__c = 2;
      objCondition2.Document_Template__c = objSRDocument1.Id;
      insert objCondition2;
      System.debug('objSRDocument1--------->' + objSRDocument1);
      System.debug('objCondition1--------->' + objCondition1);
      System.debug('objCondition2--------->' + objCondition2);
      /* ----------------------------------------------------------------------------------- */
      /* ----------------------------------------------------------------------------------- */
      Document_Template__c objSRDocument2 = new Document_Template__c();
      objSRDocument2.In_Use__c = true;
      objSRDocument2.Conditions_Filter__c = '1 OR 2';
      objSRDocument2.Document_Name__c = 'Bank statement';
      objSRDocument2.Optional__c = false;
      objSRDocument2.SR_Record_Type__c = 'Agent_Registration';
      objSRDocument2.Save_Before_Submit__c = true;
      insert objSRDocument2;

      Document_Conditions__c objCondition3 = new Document_Conditions__c();
      objCondition3.Field_API_Name__c = 'ORN_Number__c';
      objCondition3.Field_Type__c = 'Decimal';
      objCondition3.Field_Value__c = '23122345';
      objCondition3.Object_Name__c = 'NSIBPM__Service_Request__c';
      objCondition3.Operator__c = '<';
      objCondition3.Sr_No__c = 1;
      objCondition3.Document_Template__c = objSRDocument2.Id;
      insert objCondition3;

      Document_Conditions__c objCondition4 = new Document_Conditions__c();
      objCondition4.Field_API_Name__c = 'ORN_Number__c';
      objCondition4.Field_Type__c = 'Decimal';
      objCondition4.Field_Value__c = '23122345';
      objCondition4.Object_Name__c = 'NSIBPM__Service_Request__c';
      objCondition4.Operator__c = '>';
      objCondition4.Sr_No__c = 2;
      objCondition4.Document_Template__c = objSRDocument2.Id;
      insert objCondition4;
      /* ----------------------------------------------------------------------------------- */
      /* ----------------------------------------------------------------------------------- */
      Document_Template__c objSRDocument3 = new Document_Template__c();
      objSRDocument3.In_Use__c = true;
      objSRDocument3.Conditions_Filter__c = '1';
      objSRDocument3.Document_Name__c = 'Bank statement';
      objSRDocument3.Optional__c = false;
      objSRDocument3.SR_Record_Type__c = 'Agent_Registration';
      objSRDocument3.Save_Before_Submit__c = true;
      insert objSRDocument3;

      Document_Conditions__c objCondition5 = new Document_Conditions__c();
      objCondition5.Field_API_Name__c = 'ORN_Number__c';
      objCondition5.Field_Type__c = 'Decimal';
      objCondition5.Field_Value__c = '23122345';
      objCondition5.Object_Name__c = 'NSIBPM__Service_Request__c';
      objCondition5.Operator__c = '<=';
      objCondition5.Sr_No__c = 1;
      objCondition5.Document_Template__c = objSRDocument3.Id;
      insert objCondition5;
      /* ----------------------------------------------------------------------------------- */
      /* ----------------------------------------------------------------------------------- */
      Document_Template__c objSRDocument4 = new Document_Template__c();
      objSRDocument4.In_Use__c = true;
      objSRDocument4.Conditions_Filter__c = '1 AND (2 OR 2)';
      objSRDocument4.Document_Name__c = 'Bank statement';
      objSRDocument4.Optional__c = false;
      objSRDocument4.SR_Record_Type__c = 'Agent_Registration';
      objSRDocument4.Save_Before_Submit__c = true;
      insert objSRDocument4;

      Document_Conditions__c objCondition6 = new Document_Conditions__c();
      objCondition6.Field_API_Name__c = 'ORN_Number__c';
      objCondition6.Field_Type__c = 'Decimal';
      objCondition6.Field_Value__c = '23122345';
      objCondition6.Object_Name__c = 'NSIBPM__Service_Request__c';
      objCondition6.Operator__c = '>=';
      objCondition6.Sr_No__c = 1;
      objCondition6.Document_Template__c = objSRDocument4.Id;
      insert objCondition6;

      Document_Conditions__c objCondition7 = new Document_Conditions__c();
      objCondition7.Field_API_Name__c = 'DP_ok__c';
      objCondition7.Field_Type__c = 'Boolean';
      objCondition7.Field_Value__c = 'TRUE';
      objCondition7.Object_Name__c = 'NSIBPM__Service_Request__c';
      objCondition7.Operator__c = '=';
      objCondition7.Sr_No__c = 2;
      objCondition7.Document_Template__c = objSRDocument4.Id;
      insert objCondition7;
      /* ----------------------------------------------------------------------------------- */
      /* ----------------------------------------------------------------------------------- */
      Document_Template__c objSRDocument5 = new Document_Template__c();
      objSRDocument5.In_Use__c = true;
      objSRDocument5.Conditions_Filter__c = '1 AND 2 OR 3 OR 4 AND 5 OR 6';
      objSRDocument5.Document_Name__c = 'Bank statement';
      objSRDocument5.Optional__c = false;
      objSRDocument5.SR_Record_Type__c = 'Agent_Registration';
      objSRDocument5.Save_Before_Submit__c = true;
      insert objSRDocument5;

      Document_Conditions__c objCondition8 = new Document_Conditions__c();
      objCondition8.Field_API_Name__c = 'Nationality__c';
      objCondition8.Field_Type__c = 'text';
      objCondition8.Field_Value__c = 'China';
      objCondition8.Object_Name__c = 'NSIBPM__Service_Request__c';
      objCondition8.Operator__c = 'LIKE';
      objCondition8.Sr_No__c = 1;
      objCondition8.Document_Template__c = objSRDocument5.Id;
      insert objCondition8;

      Document_Conditions__c objCondition9 = new Document_Conditions__c();
      objCondition9.Field_API_Name__c = 'DP_Overdue__c';
      objCondition9.Field_Type__c = 'Boolean';
      objCondition9.Field_Value__c = 'FALSE';
      objCondition9.Object_Name__c = 'NSIBPM__Service_Request__c';
      objCondition9.Operator__c = '=';
      objCondition9.Sr_No__c = 2;
      objCondition9.Document_Template__c = objSRDocument5.Id;
      insert objCondition9;

      Document_Conditions__c objCondition10 = new Document_Conditions__c();
      objCondition10.Field_API_Name__c = 'Nationality__c';
      objCondition10.Field_Type__c = 'text';
      objCondition10.Field_Value__c = 'China';
      objCondition10.Object_Name__c = 'NSIBPM__Service_Request__c';
      objCondition10.Operator__c = 'Ends With';
      objCondition10.Sr_No__c = 3;
      objCondition10.Document_Template__c = objSRDocument5.Id;
      insert objCondition10;

      Document_Conditions__c objCondition11 = new Document_Conditions__c();
      objCondition11.Field_API_Name__c = 'Nationality__c';
      objCondition11.Field_Type__c = 'text';
      objCondition11.Field_Value__c = 'China';
      objCondition11.Object_Name__c = 'NSIBPM__Service_Request__c';
      objCondition11.Operator__c = 'Starts With';
      objCondition11.Sr_No__c = 4;
      objCondition11.Document_Template__c = objSRDocument5.Id;
      insert objCondition11;

      Document_Conditions__c objCondition12 = new Document_Conditions__c();
      objCondition12.Field_API_Name__c = 'Nationality__c';
      objCondition12.Field_Type__c = 'text';
      objCondition12.Field_Value__c = 'China';
      objCondition12.Object_Name__c = 'NSIBPM__Service_Request__c';
      objCondition12.Operator__c = 'Contains';
      objCondition12.Sr_No__c = 5;
      objCondition12.Document_Template__c = objSRDocument5.Id;
      insert objCondition12;

      Document_Conditions__c objCondition13 = new Document_Conditions__c();
      objCondition13.Field_API_Name__c = 'Nationality__c';
      objCondition13.Field_Type__c = 'text';
      objCondition13.Field_Value__c = 'China';
      objCondition13.Object_Name__c = 'NSIBPM__Service_Request__c';
      objCondition13.Operator__c = '!=';
      objCondition13.Sr_No__c = 6;
      objCondition13.Document_Template__c = objSRDocument5.Id;
      insert objCondition13;
      /* ----------------------------------------------------------------------------------- */

      List<NSIBPM__Service_Request__c> listSR = new LIst<NSIBPM__Service_Request__c>();
      listSR.add(objSR);
      //DocumentHelper.generateDocumentsForSR(listSR);
      insert objSR;

      NSIBPM__Status__c objStepStatus = new NSIBPM__Status__c();
      objStepStatus.Name = 'Agent Executive Approved';
      objStepStatus.NSIBPM__Code__c = 'ext12';
      objStepStatus.NSIBPM__DEV_Id__c = '5345';
      objStepStatus.NSIBPM__Rejection__c = false;
      objStepStatus.NSIBPM__SR_Closed_Status__c = true;
      objStepStatus.NSIBPM__Type__c = 'Intermediate';
      insert objStepStatus;

      NSIBPM__Step_Template__c objTemplate = new NSIBPM__Step_Template__c();
      objTemplate.Name = 'Agent_Update';
      objTemplate.NSIBPM__Code__c = 'fd344';
      objTemplate.NSIBPM__DEV_Id__c = 'sr1';
      objTemplate.NSIBPM__Estimated_Hours__c = 10;
      objTemplate.NSIBPM__Step_RecordType_API_Name__c = 'fdfvdvfdv fgdvfvff';  
      objTemplate.NSIBPM__Summary__c = 'fdfvdvfdv fgdvfvff dfzdvdfzv';
      insert   objTemplate;

      System.debug('objSR--------->' + objSR.Record_Type_Name__c);
      New_Step__c objStep = new New_Step__c();
      objStep.Service_Request__c = objSR.Id;
      objStep.Step_No__c = 1;
      objStep.Is_Closed__c = false;
      objStep.Step_Status__c = 'Assigned';
      objStep.Step_Type__c = 'Document Follow Up';
      insert objStep;

      //DocumentHelper.getSRIdsFromSteps(new List<New_Step__c> {objStep});
      Test.startTest();
      
      Test.setMock(HttpCalloutMock.class, new RestServiceMock());
      DocumentHelper.login();
      DocumentHelper.generateLOIAgreements(objSR.Id);
      DocumentHelper.generateAggrementsButton(objSR.Id);
      DocumentHelper.fetchAmendmentsFromSR(new Set<Id>{objSR.Id});
      Test.stopTest();
    }
  }
}