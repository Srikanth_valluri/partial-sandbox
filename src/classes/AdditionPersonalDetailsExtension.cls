/***********************************************************************************************
 * @Name              : AdditionPersonalDetailsExtension
 * @Test Class Name   : AdditionPersonalDetailsExtensionTest
 * @Description       : Controller class for Addition Personal Details VF page.
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log
 * 1.0         ADM CRM         30/11/2017      Created
 * 2.0         Arjun Khatri    13/02/2019      Commented code to overcome the concurrent limit
 * 										       exception.
 * 3.0         Arjun Khatri    26/02/2019      Added methods maskMobilesOrgAccount(),
 * 											   maskMobiles().
***********************************************************************************************/

public with sharing class AdditionPersonalDetailsExtension {
	public String accId {get; set;}
	public Boolean communityUser {get; set;}
	public Decimal portfolioAED {get; set;}
	public String customerCategory {get; set;}
	public String goodStandingFalg{get; set;}
	public Account account {get; set;}
	public String isCommunityUser {get; set;}
	public Boolean isOrgAccount{get; set;}
	public Boolean isCollection {get; set;}
	public Set<Id> bookingIdList;
	public String noMobileEncrypt{get; set;}
	public String strPhotoURL{get; set;}
	public String userId{get; set;}
	public List<Contact> lstContacts{get; set;}
	// Added on 29/05/18 : To get Next payment Amount, Due date,Amount overdue
	public Date myNextPaymentDate {get; set;} // Payment_Terms_del__r.Payment_Date__c
	public String strTotalDue{get; set;}
	public String strCurrentValue{get; set;}
	public String strAmountOverdue{get; set;}
	public datetime lastLogin{get; set;}
	public transient WrapperBookingUnit bookingUnitWrapper{get; set;}
	public String maskedMobile{get; set;}
	public String maskedMobile1{get; set;}
	public String maskedMobile2{get; set;}
	public String maskedMobile3{get; set;}
	public String maskedMobile4{get; set;}
	public String maskedMobile5{get; set;}
	public String alternateMob{get; set;}
	Public Boolean mm {get; set;}
	Public Boolean isNotFAM {get; set;}
	Public String myProfileName {get; set;}
	public Boolean displayCallButton{get; set;} /* Added on 17/08/2020 : To display call button
								based on the Profiles and 'DELC on Account' value. */

	public AdditionPersonalDetailsExtension(ApexPages.StandardController controller){
		lastLogin = null;
		myNextPaymentDate = null;
		strCurrentValue = '0';
		strTotalDue = '0';
		strAmountOverdue = '';
		maskedMobile = '';
		maskedMobile1 = '';
		maskedMobile2 = '';
		maskedMobile3 = '';
		maskedMobile4 = '';
		maskedMobile5 = '';
		alternateMob = '';
		displayCallButton = true;

		List<Profile> profile = [SELECT Id, Name FROM profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
		myProfileName = profile[0].Name;

		if( Label.Label_CTI_Profile.contains(myProfileName)){
			mm = true;
		}else{
			mm = false;
		}

		bookingUnitWrapper = new WrapperBookingUnit();
		init();
	}


	public void init(){
		lstContacts = new List<Contact>();
		communityUser =false;
		portfolioAED = 0.0;
		customerCategory = '';
		goodStandingFalg = 'YES';
		isOrgAccount = true;
		bookingIdList = new Set<Id>();
		isCommunityUser = 'NO';
		noMobileEncrypt = 'N/A';
		accId = ApexPages.currentPage().getParameters().get('id').escapeHtml4();
		accId = String.valueOf(accId).substring(0, 15);
		// Added on 29/05/18 : To get Next payment Amount, Due date,Amount overdue
		Map<string, object> mapCurrentValue = new Map<string, object>();

		if ( String.isNotBlank(accId)) {
			lastLogin = LastLoginInfo(accId);
		}

		account = [SELECT Name,
						  Id,
						  CurrencyIsoCode,
						  Portfolio_value_in_AED__c,
						  Customer_Type__c,
						  Good_standing__c,
						  Phone,
						  PersonEmail,
						  Nationality__pc,
						  Party_Type__c,
						  Party_ID__c,
						  SLA__c,
						  Title__c,
						  Alternate_Country_Code__c,
						  alternate_Number__c,
						  Passport_Number__c,
						  Customer_Category__c,
						  Passport_Number__pc,
						  Organisation_Name__c,
						  CR_Number__c,
						  Mobile__c,
						  Email__c,
						  Email__pc,
						  Person_Business_Masked_Email__c,
						  PersonContactId,
						  PersonMailingStreet,
						  PersonMailingPostalCode,
						  PersonMailingCity,
						  PersonMailingState,
						  PersonMailingCountry,
						  Address_Line_1__pc,
						  Address_Line_2__pc,
						  Address_Line_3__pc,
						  Address_Line_4__pc,
						  City__pc,
						  Country__pc,
						  Address_Line_1__c,
						  Address_Line_2__c,
						  Address_Line_3__c,
						  Address_Line_4__c,
						  City__c,
						  Country__c,
						  Salutation,
						  IsCustomerPortal,
						  Nationality__c,
						  Mobile_Phone_Encrypt__c,
						  Mobile_Phone_2__c,
						  Mobile_Phone_3__c,
						  Mobile_Phone_4__c,
						  Mobile_Phone_5__c,
						  Mobile_Phone_Encrypt__pc,
						  Mobile_Phone_Encrypt_2__pc,
						  Mobile_Phone_Encrypt_3__pc,
						  Mobile_Phone_Encrypt_4__pc,
						  Mobile_Phone_Encrypt_5__pc,
						  Telephone__c,
						  Click_to_Dial_Mobile_Phone__c,
						  Click_to_Dial_Mobile_Phone_2__c,
						  Click_to_Dial_Mobile_Phone_3__c,
						  Click_to_Dial_Mobile_Phone_4__c,
						  Click_to_Dial_Mobile_Phone_5__c,
						  Click_To_Dial_Telephone__c,
						  Click_To_Dial_Mobile__c,
						  Customer_Masked_Mobile__c,
						  Corporate_Customer_Mobile__c,
						  FAM_CAUTION__c,Primary_CRE__c,Secondary_CRE__c,Tertiary_CRE__c,
						  Units_under_DELC__c,
						  (SELECT Id,
								  Name,
								  Account__c
							 FROM Bookings__r),
						  (SELECT Id,
								  Lastname,
								  FirstName,
								  AccountId,
								  Mobile_Phone__c,
								  Mobile_Phone_2__c,
								  Mobile_Phone_3__c,
								  Mobile_Phone_4__c,
								  Mobile_Phone_5__c
						  FROM Contacts)
					 FROM Account
					Where Id = :accId ];

		if(account.Party_Type__c == 'PERSON'){
			isOrgAccount = false;
		}

		isNotFAM = true;
		String currentUserId = UserInfo.getUserId();

		if(String.isNotBlank(account.FAM_CAUTION__c)
			&& Label.FAMFieldValue.containsIgnoreCase(account.FAM_CAUTION__c)){
				if(account.Primary_CRE__c == NULL &&
				   account.Secondary_CRE__c == NULL &&
				   account.Tertiary_CRE__c == NULL
				){
					isNotFAM = false;
				}else if(!((account.Primary_CRE__c != NULL &&
				         currentUserId.contains(account.Primary_CRE__c)) ||
						(account.Secondary_CRE__c != NULL && currentUserId.contains(account.Secondary_CRE__c)) ||
						(account.Tertiary_CRE__c != NULL && currentUserId.contains(account.Tertiary_CRE__c)))
				){
					isNotFAM = false;
				}
		}
		if(isOrgAccount) {
			maskMobilesOrgAccount();
		}
		else{
			maskMobiles();
		}

		if( account.Contacts.isEmpty() == false ) {
			lstContacts.addAll( account.Contacts );
		}

		if (account.Customer_Category__c != null){
			customerCategory = account.Customer_Category__c;
		}

		if(account.IsCustomerPortal){
			isCommunityUser = 'YES';
		}
		if (account.PersonContactId != null) {
			List<User> lstUser = [SELECT Id
									   , ContactId
									   , mediumPhotoUrl
									FROM User
								   WHERE ContactId = :account.PersonContactId];

			if (lstUser != null && lstUser.size() >0) {
				strPhotoURL = lstUser[0].mediumPhotoUrl;
				 userId = lstUser[0].Id;
			} else {
				strPhotoURL = '/profilephoto/005/M';
			}
		} else if (account.Contacts != null && account.Contacts.size() > 0) {
			List<User> lstUser = [SELECT Id
									   , ContactId
									   , mediumPhotoUrl
									FROM User
								   WHERE ContactId
									  IN :account.Contacts];

			if (lstUser != null && lstUser.size() >0) {
				strPhotoURL = lstUser[0].mediumPhotoUrl;
				 userId = lstUser[0].Id;
			} else {
				strPhotoURL = '/profilephoto/005/M';
			}
		} else {
			strPhotoURL = '/profilephoto/005/M';
		}

		for(Booking__c b : account.Bookings__r) {
			bookingIdList.add(b.Id);
		}

		set<Id> profId = new set<id>();
		for (Profile objProf : [SELECT Id
									 , Name
								  FROM Profile
								 WHERE Name = 'Collection - CRE'
									OR Name = 'Collection - Director'
									OR Name = 'Collection - Manager'
								    OR Name = 'Collection - CRE1']) {
			profId.add(objProf.Id);
		}
		if (profId.contains(UserInfo.getProfileId())) {
			isCollection = true;
		} else {
			isCollection = false;
		}

		setDisplayCallNowValue();
	}

	public void maskMobilesOrgAccount() {
		if(String.isNotBlank(account.Mobile__c)) {
			String tempMob = String.valueOf(account.Mobile__c);
			if(tempMob.length() >= 9){
				maskedMobile = tempMob.Substring(0, 3) +
					'xxxx'+tempMob.Substring(tempMob.length()-3,tempMob.length());
			}

		}
		if(String.isNotBlank(account.alternate_Number__c ) ){
			String tempalternatemob = String.valueOf(account.alternate_Number__c);
			if(tempalternatemob.length() >= 9){
				alternateMob  = tempalternatemob.Substring(0, 3) +
					'xxxx'+tempalternatemob.Substring(tempalternatemob.length()-3,tempalternatemob.length());
			}
		}
		if(account.Contacts.size() > 0){
			if( account.Contacts[0].Mobile_Phone__c != NULL) {
				String tempMob = String.valueOf(account.Contacts[0].Mobile_Phone__c );
				if(tempMob.length() >= 9){
					maskedMobile1 = tempMob.Substring(0, 3) +
						'xxxx'+tempMob.Substring(tempMob.length()-3,tempMob.length());
				}
			}
			if(account.Contacts[0].Mobile_Phone_2__c != NULL) {
				String tempMob = String.valueOf(account.Contacts[0].Mobile_Phone_2__c );
				if(tempMob.length() >= 9){
					maskedMobile2 = tempMob.Substring(0, 3) +
						'xxxx'+tempMob.Substring(tempMob.length()-3,tempMob.length());
				}
			}
			if(account.Contacts[0].Mobile_Phone_3__c != NULL) {
				String tempMob = String.valueOf(account.Contacts[0].Mobile_Phone_3__c );
				if(tempMob.length() >= 9){
					maskedMobile3 = tempMob.Substring(0, 3) +
					 	'xxxx'+tempMob.Substring(tempMob.length()-3,tempMob.length());
				}
			}
			if(account.Contacts[0].Mobile_Phone_4__c != NULL) {
				String tempMob = String.valueOf(account.Contacts[0].Mobile_Phone_4__c );
				if(tempMob.length() >= 9){
					maskedMobile4 = tempMob.Substring(0, 3) +
						'xxxx'+tempMob.Substring(tempMob.length()-3,tempMob.length());
				}
			}
			if(account.Contacts[0].Mobile_Phone_5__c != NULL) {
				String tempMob = String.valueOf(account.Contacts[0].Mobile_Phone_5__c );
				if(tempMob.length() >= 9){
					maskedMobile5 = tempMob.Substring(0, 3) +
						'xxxx'+tempMob.Substring(tempMob.length()-3,tempMob.length());
				}
			}
		}
	}

	public void maskMobiles() {
		if(String.isNotBlank(account.Mobile_Phone_Encrypt__pc)) {
			String tempMob = String.valueOf(account.Mobile_Phone_Encrypt__pc);
			if(tempMob.length() >= 9){
				maskedMobile = tempMob.Substring(0, 3) +
					'xxxx'+tempMob.Substring(tempMob.length()-3,tempMob.length());
			}
		}

		if(account.Mobile_Phone_Encrypt_2__pc != NULL) {
			String tempMob = String.valueOf(account.Mobile_Phone_Encrypt_2__pc );
			if(tempMob.length() >= 9){
				maskedMobile1 = tempMob.Substring(0, 3) +
				    'xxxx'+tempMob.Substring(tempMob.length()-3,tempMob.length());
			}
		}
		if(account.Mobile_Phone_Encrypt_3__pc != NULL) {
			String tempMob = String.valueOf(account.Mobile_Phone_Encrypt_3__pc );
			if(tempMob.length() >= 9){
				maskedMobile2 = tempMob.Substring(0, 3) +
					'xxxx'+tempMob.Substring(tempMob.length()-3,tempMob.length());
			}
		}
		if(account.Mobile_Phone_Encrypt_4__pc != NULL) {
			String tempMob = String.valueOf(account.Mobile_Phone_Encrypt_4__pc );
			if(tempMob.length() >= 9){
				maskedMobile3 = tempMob.Substring(0, 3) +
					'xxxx'+tempMob.Substring(tempMob.length()-3,tempMob.length());
			}
		}
		if(account.Mobile_Phone_Encrypt_5__pc != NULL) {
			String tempMob = String.valueOf(account.Mobile_Phone_Encrypt_5__pc );
			if(tempMob.length() >= 9){
				maskedMobile4 = tempMob.Substring(0, 3) +
				 	'xxxx'+tempMob.Substring(tempMob.length()-3,tempMob.length());
			}
		}
		if(account.Telephone__c != NULL) {
			String tempMob = String.valueOf(account.Telephone__c );
			if(tempMob.length() >= 9){
				maskedMobile5 = tempMob.Substring(0, 3) +
					'xxxx'+tempMob.Substring(tempMob.length()-3,tempMob.length());
			}
		}

		//TODO addeded for alternate mob
		if(account.alternate_Number__c != NULL &&
			String.valueOf(account.alternate_Number__c).startsWith('00') ){
			String tempalternatemob = String.valueOf(account.alternate_Number__c);
			if(tempalternatemob.length() >= 9){
				alternateMob  = tempalternatemob.Substring(0, 3) +
					'xxxx'+tempalternatemob.Substring(tempalternatemob.length()-3,tempalternatemob.length());
			}
		}
	}

	public static DateTime lastLoginInfo( Id accountId ) {
		DateTime lastLogin = null;
		list<Account> lstA = [SELECT Id
								   , isPersonAccount
								   , PersonContactId
							    FROM Account
							   WHERE Id =: accountId
							     AND isPersonAccount = true
								 AND PersonContactId != null
							   LIMIT 1];
		if(!lstA.isEmpty()){
			list<User> lstUser = [SELECT LastLoginDate
									   , ContactId
								    FROM User
								   WHERE ContactId =: lstA[0].PersonContactId LIMIT 1];
			if(!lstUser.isEmpty()){
				lastLogin = lstUser[0].LastLoginDate;
			}
		}
		return lastLogin;
	}

	@RemoteAction
	public static ConnectApi.Photo setPhotoFromVersion(String fileId, String userId) {
		ConnectApi.Photo photo;
		List<ContentVersion> lstVersion = [ SELECT  Id
													, ContentDocumentId
													, VersionNumber
													, VersionData
													, FileType
													, Title
											FROM    ContentVersion
											WHERE   Id = :fileId];
		if (lstVersion.size() > 0) {
			photo = ConnectApi.UserProfiles.setPhoto(
				Network.getNetworkId(),
				userId,
				new ConnectApi.BinaryInput(
					lstVersion[0].VersionData,
					'image/' + lstVersion[0].FileType.toLowerCase(),
					lstVersion[0].Title
				)
			);
			Database.delete(lstVersion[0].ContentDocumentId);
		}
		return photo;
	}

	/************************************************************************
	* @Description : Method to set the displayCallButton to true/false on the
	*                basis of user profiles and DELC on Account field value.
	* @Params      : None
	* @Return      : None
	*************************************************************************/
	public void setDisplayCallNowValue(){
		Set<Id> profileIdSet = new Set<Id>();
		Set<String> allowedProfileList = new Set<String>();

		for(Allowed_Profiles_for_Call_Email__mdt obj
			:[SELECT Profile_Name__c
				FROM Allowed_Profiles_for_Call_Email__mdt
			 ]
		){
			allowedProfileList.add(obj.Profile_Name__c);
		}

		for (Profile objProf : [Select Id, Name From Profile
								Where Name IN :allowedProfileList
								OR Name LIKE '%FM%'
								OR Name LIKE '%Property%']) {
			ProfileIdSet.add(objProf.Id);
		}

		if (ProfileIdSet != null &&
			!ProfileIdSet.contains(UserInfo.getProfileId()) &&
			account.Units_under_DELC__c
		   ) {
			displayCallButton = false;
		}
	}
}