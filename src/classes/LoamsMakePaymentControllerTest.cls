@isTest
private class LoamsMakePaymentControllerTest {

    @isTest
    static void testPositive() {

        PaymentGateway__c gateway = new PaymentGateway__c(
            Name = LoamsCommunityController.GATEWAY_NAME,
            Url__c = 'https://www.payment-gateway.com',
            MerchantId__c = 'merchant_id',
            AccessCode__c = 'access_code',
            EncryptionKey__c = 'encryption_key'
        );
        insert gateway;

        User portalUser = CommunityTestDataFactory.createPortalUser();
        Id portalAccountId = [SELECT Id, Contact.AccountId FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;

        Location__c location = new Location__c(
            Name = 'Test Location',
            Location_ID__c = 'LOC'
        );
        insert location;

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(portalAccountId, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            unit.Inventory__c = inventory.Id;
        }
        insert lstBookingUnit;

        Test.startTest();
            System.assertEquals(NULL, LoamsMakePaymentController.lookupBuilding(NULL));
            System.assertEquals(NULL, LoamsMakePaymentController.lookupUnit(NULL, NULL));
            System.assert(!LoamsMakePaymentController.lookupBuilding(location.Name).isEmpty());
            System.assert(LoamsMakePaymentController.lookupUnit(lstBookingUnit[0].Unit_Name__c, location.Id).isEmpty());

            Test.setCurrentPage(Page.GuestMakePayment);
            ApexPages.currentPage().getParameters().put('retUrl', '/');
            LoamsMakePaymentController controller = new LoamsMakePaymentController();
            controller.getPhoneCountryCodes();
            controller.getCountryList();

            controller.payer.firstName      = 'First';
            controller.payer.lastName       = 'Last';
            controller.payer.phone          = '0000000000';
            controller.payer.phoneCode      = '+0';
            controller.payer.email          = 'e@mail.com';
            controller.payer.country        = 'Country';
            controller.payer.state          = 'State';
            controller.payer.city           = 'City';
            controller.payer.zip            = '000';
            controller.payer.address        = 'Address';
            controller.payer.project        = location.Id;
            controller.payer.unit           = lstBookingUnit[0].Id;
            controller.payer.amount         = 1;

            PageReference paymentPage = controller.initiateTransaction();
            List<SelectOption> lstPaymentTypes = controller.getPaymentTypes();
            List<SelectOption> lstOtherPaymentTypes = controller.getOtherPaymentTypes();

        Test.stopTest();

        System.assertNotEquals(NULL, paymentPage);
    }

    @isTest
    static void testNegative() {
        Test.startTest();

            Test.setCurrentPage(Page.GuestMakePayment);
            ApexPages.currentPage().getParameters().put('retUrl', '/');
            LoamsMakePaymentController controller = new LoamsMakePaymentController();

            System.assertEquals(NULL, controller.initiateTransaction());
            controller.payer.firstName      = 'First';
            System.assertEquals(NULL, controller.initiateTransaction());
            controller.payer.lastName       = 'Last';
            System.assertEquals(NULL, controller.initiateTransaction());
            controller.payer.phone          = '0000000000';
            System.assertEquals(NULL, controller.initiateTransaction());
            controller.payer.phoneCode      = '+0';
            System.assertEquals(NULL, controller.initiateTransaction());
            controller.payer.email          = 'e@mail.com';
            System.assertEquals(NULL, controller.initiateTransaction());
            controller.payer.country        = 'Country';
            System.assertEquals(NULL, controller.initiateTransaction());
            controller.payer.state          = 'State';
            System.assertEquals(NULL, controller.initiateTransaction());
            controller.payer.city           = 'City';
            System.assertEquals(NULL, controller.initiateTransaction());
            controller.payer.zip            = '000';
            System.assertEquals(NULL, controller.initiateTransaction());
            controller.payer.address        = 'Address';
            System.assertEquals(NULL, controller.initiateTransaction());
            controller.payer.project        = 'Project with a loooooooong name';
            System.assertEquals(NULL, controller.initiateTransaction());
            controller.payer.unit           = 'Unit with a looooooooooong name';
            System.assertEquals(NULL, controller.initiateTransaction());
            controller.payer.amount         = 0;
            System.assertEquals(NULL, controller.initiateTransaction());
            controller.payer.amount         = 1;
            System.assertEquals(NULL, controller.initiateTransaction());
            controller.payer.project        = 'Project';
            System.assertEquals(NULL, controller.initiateTransaction());
            controller.payer.unit           = 'Unit';
            System.assertEquals(NULL, controller.initiateTransaction());

        Test.stopTest();
    }

}