global with sharing class DAMAC_Unit_PreBooking{
    public DAMAC_Unit_PreBooking(DAMAC_UK_AVAILABILITY controller) {

    }

    public static Boolean isPortalUser {get {        
            User userObj = [SELECT IsPortalEnabled  FROM user WHERE id=:userinfo.getuserid() LIMIT 1];
            return userObj.IsPortalEnabled;
        } 
        set;}
    public DAMAC_Unit_PreBooking(DAMAC_Project_Avaliability_Clone controller) {}
    public DAMAC_Unit_PreBooking(DAMAC_Project_Avaliability controller) {}    
    public DAMAC_Unit_PreBooking(AP_ProjectCompareController controller) {}    
    //public DAMAC_Unit_PreBooking(DAMAC_Project_Avaliability_Clone controller) {} 
    public DAMAC_Unit_PreBooking(AP_UnitSearchController controller) {}
    public DAMAC_Unit_PreBooking(AP_BookingLandingController controller) {} 
    public String selectedInvIds { get; set; }
    public static string preBookId { get; set; }
    public DAMAC_Unit_PreBooking() { 

    } // empty constructor
    
    @RemoteAction
    global static string SendPreBookId(String selectedInvIds, string eoiID,boolean isUkBooking, String deRecordId) {
        if(selectedInvIds != ''){
            
            list<String> idHolder = selectedInvIds.split(',');
            System.debug(idHolder.size() +'>>>>>>>>>>>>>>>');
            set<string> tempDeDupeHolder = new set<string>();
            tempDeDupeHolder.addall(idHolder);
            list<inventory__c> preBookInventory = new list<inventory__c>();
            preBookInventory = [select building_location__c from Inventory__c where Id IN:tempDeDupeHolder];
            if(preBookInventory.size()>0){
                //try{
                //V1.2 Store The unit on creation of SR
                String unitOfBooking = '';

                list<inventory__c> allinv = [SELECT Id, Property__c, Property__r.Name, Building_Location__c,View_Type__c , 
                                             Unit_Area_sft__c , Brand__c ,Bedroom_Type__c,CM_Price_Per_Sqft__c,Location_Code__c,
                                             IPMS_Bedrooms__c, Special_Price_2__c, Marketing_Name__c, Unit_Name__c,
                                             Building_Name__c, Property_Name__c, Floor__c, Area_Sqft_2__c,
                                             Building_Location__r.Building_Name__c, Building_Location__r.Name, 
                                             Bedrooms__c, Unit__c, Unit_Type__c, Property_Status__c,
                                             Area__c, Selling_Price__c, Special_Price__c, Price_Per_Sqft__c,
                                             MD_Price__c, List_Price__c, Property_Country__c, Status__c, 
                                             Floor_Package_ID__c,CurrencyIsoCode, Floor_Package_Type__c, List_Price_calc__c, Special_Price_calc__c 
                                             FROM Inventory__c where status__c='Released' and ID IN:tempDeDupeHolder AND EOI__c =: eoiID];
                
                
                UtilityWrapperManager availableInventoriesList = new UtilityWrapperManager();
                availableInventoriesList.ibwList = new List<UtilityWrapperManager.InventoryBuyerWrapper>();
                List<UtilityWrapperManager.InventoryBuyerWrapper> ibwList = new List<UtilityWrapperManager.InventoryBuyerWrapper>();
                
                String unitCodes = '';
                for(inventory__c thisinventory :allinv){
                    unitOfBooking += thisinventory.Location_Code__c;
                    unitOfBooking += ',';
                    
                    unitCodes += thisinventory.Unit_Name__c+',';
                    
                    ibwList.add(new UtilityWrapperManager.InventoryBuyerWrapper(true, false, false, 40000, 0.0, null,
                                                                                '', '', '', 0, thisInventory,
                                                                                new Booking_Unit__c(),
                                                                                new Map<String, Payment_Plan__c>(),
                                                                                new List<Buyer__c>{new Buyer__c(Primary_Buyer__c = true)}));
                }
                if (Test.isRunningTest ()) {
                    availableInventoriesList.ibwList = ibwList;
                    system.debug('availableInventoriesList==='+availableInventoriesList);
                    List<DH_ResponseWrapper> droolResponseList = createAvailableOptions(availableInventoriesList);
                }
                unitCodes = unitCodes.removeEnd (',');
                Map <String, List <Damac_PromotionsService.Promotions>> promotionResponse = new Map <String, List <Damac_PromotionsService.Promotions>> ();
                
                try {
                    promotionResponse  = Damac_PromotionsService.promotionsRequest (unitCodes);
                    system.debug (promotionResponse);
                } catch (Exception e) {}
                
                NSIBPM__Service_Request__c newPreBooking = new NSIBPM__Service_Request__c();
                list<Selected_Units__c> iinvLst = new list<Selected_Units__c>();
                newPreBooking.recordtypeid= Schema.Sobjecttype.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
                
                if(isPortalUser) {
                    
                    Id portalDealTeamQueueId = [SELECT Id FROM Group where Name = 'Portal Deal Team' and Type = 'Queue' LIMIT 1].Id;
                    User userObj = [SELECT Id,ContactId, Contact.AccountId  FROM user WHERE id=:userinfo.getuserid() LIMIT 1];
                    system.debug('userObj----------------------------'+userObj);
                    newPreBooking.Agency__c = userObj.Contact.AccountId;
                    newPreBooking.NSIBPM__Contact__c = userObj.ContactId;
                    newPreBooking.Agent_Name__c = userObj.Id;
                    newPreBooking.Deal_Type__c = 'Agent Portal Deal';
                    newPreBooking.Created_By_AgentPortal__c = true;
                    newPreBooking.OwnerId = portalDealTeamQueueId;
                    //newPreBooking.Manager_For_Agent_Portal__c = damacUserObj.ManagerId;
                
                }
                newPreBooking.booking_wizard_level__c ='';
                newPreBooking.Booking_from_New_Wizard__c = true;
                unitOfBooking = unitOfBooking.removeEnd(',');
                newPreBooking.List_of_Units__c = unitOfBooking;
                if(isUkBooking){
                    newPreBooking.Is_UK_Deal__c = true;
                }
                system.debug('deRecordId: ' + deRecordId);
                if(deRecordId != null && deRecordId != ''){
                    for(Deal_Exception_Request__c der: [SELECT Id FROM Deal_Exception_Request__c WHERE Id =: deRecordId]){
                        newPreBooking.Deal_Exception_Request__c = der.Id;
                    }
                }
                System.Debug (eoiid);
                if (eoiId != '')
                    newPreBooking.EOI_Process__c = EOIID;
                insert newPreBooking;
                
                for(inventory__c i:preBookInventory){
                    Selected_Units__c iinv = new Selected_Units__c();
                    iinv.inventory__c = i.id;
                    iinv.Service_Request__c = newPreBooking.id;
                    iinvLst.add(iinv);
                }
                insert iinvLst;
                Map <String, Id> selectedUnitIdsMap = new map <String, Id> ();                    
                
                for (Selected_Units__c unit :[ SELECT inventory__r.Unit_Location__r.Name FROM Selected_Units__c WHERE ID IN :iinvLst]) {
                    selectedUnitIdsMap.put (unit.inventory__r.Unit_Location__r.Name, unit.Id);
                }
                
                List <Available_Options__c> availableOptionsList = new List <Available_Options__c> ();
                
                
                for (String key :promotionResponse.keySet ()) {
                    for (Damac_PromotionsService.Promotions promotion : promotionResponse.get (key)) {
                        Available_Options__c availableOptions = new Available_Options__c ();
                        availableOptions.Promotion_Option__c = promotion.promotion;
                        availableOptions.Selected_Units__c = selectedUnitIdsMap.get (promotion.unitCode);
                        availableOptions.TemplateIdPN__c = promotion.promotionId;
                        availableOptions.Type_of_Option__c = 'Promotion';
                        availableOptionsList.add (availableOptions);   
                        
                    }
                    
                }
                insert availableOptionsList;
                
                preBookId = newPreBooking.id;
                return preBookId; 
                
            }else{
                return 'Error';
            }
        }        
        return 'Error';
    }
    public static List <DH_ResponseWrapper> createAvailableOptions (UtilityWrapperManager availableInventoriesList) {
        
        Set<Id> InventoryId =  new Set<Id>();
        List<Campaign_Inventory__c> CamList = new List<Campaign_Inventory__c>();  
        List<DH_RequestWrapper.cls_demo_project1_InOutObject> unitsList=new  List<DH_RequestWrapper.cls_demo_project1_InOutObject>();
        system.debug('...availableInventoriesList...'+availableInventoriesList);
        if(availableInventoriesList.ibwList!=null&&availableInventoriesList.ibwList.size()>0){
            
            for(UtilityWrapperManager.InventoryBuyerWrapper thisInventory:availableInventoriesList.ibwList){
                if(thisInventory.isSelected&&thisInventory.selectedInventory.Id!=null){
                    InventoryId.add(thisInventory.selectedInventory.Id);
                }
            }
            
            if(!InventoryId.isEmpty()){
                for(Campaign_Inventory__c Cam : [select Id , Inventory__c , Campaign__r.Campaign_Name__c  from Campaign_Inventory__c where  Inventory__c =: InventoryId ]){
                    CamList.add(Cam);
                }
            }
            
            for(UtilityWrapperManager.InventoryBuyerWrapper thisInventory:availableInventoriesList.ibwList){
                
                if(thisInventory.isSelected&&thisInventory.selectedInventory.Id!=null){
                    if(String.isBlank(thisInventory.selectedInventory.Floor_Package_Type__c)){
                        system.debug('#### selected inventory createRequestData= '+thisInventory.selectedInventory);
                        system.debug('#### selected inventory bookingUnit createRequestData= '+thisInventory.bookingUnit);
                        DH_RequestWrapper.cls_demo_project1_InOutObject reqEle=new  DH_RequestWrapper.cls_demo_project1_InOutObject();
                        reqEle.productType = thisInventory.selectedInventory.CurrencyIsoCode ;
                        reqEle.dateofBooking=system.now().format('YYYY-MM-dd');
                        reqEle.projectName=!string.isBlank(thisInventory.selectedInventory.Property_Name__c)?thisInventory.selectedInventory.Property_Name__c:'null';
                        reqEle.buildingName=thisInventory.selectedInventory.Building_Location__r.Name!=null?thisInventory.selectedInventory.Building_Location__r.Name:'null';
                        reqEle.bedroomType=!string.isBlank(thisInventory.selectedInventory.Bedroom_Type__c)?thisInventory.selectedInventory.Bedroom_Type__c:'null';
                        reqEle.numberofBedrooms=!string.isBlank(thisInventory.selectedInventory.IPMS_Bedrooms__c)?(thisInventory.selectedInventory.IPMS_Bedrooms__c.equalsIgnoreCase('Studio')?'1':thisInventory.selectedInventory.IPMS_Bedrooms__c):'null';
                        reqEle.cunstructionStatus=!string.isBlank(thisInventory.selectedInventory.Property_Status__c)?thisInventory.selectedInventory.Property_Status__c:'null';
                        //reqEle.area=Integer.valueof(thisInventory.selectedInventory.Area_Sqft_2__c) ;
                        reqEle.area=thisInventory.selectedInventory.Area_Sqft_2__c!=null?Integer.valueof(thisInventory.selectedInventory.Area_Sqft_2__c):0;
                        // reqEle.price=Integer.valueof(thisInventory.selectedInventory.Special_Price__c) ;
                        reqEle.price=thisInventory.selectedInventory.Special_Price__c!=null?Integer.valueof(thisInventory.selectedInventory.Special_Price__c):0;
                        
                        // reqEle.agent=   String.valueof(availableInventoriesList.dealRecord.Agent_Name__r.Name) != null ?  availableInventoriesList.dealRecord.Agent_Name__r.Name : 'null' ;             
                        reqEle.agent='null';
                        reqEle.pcId=UserInfo.getName();
                        reqEle.unitIdCN=!string.isBlank(thisInventory.selectedInventory.Unit__c)?thisInventory.selectedInventory.Unit__c:'null';
                        reqEle.marketingProject=!string.isBlank(thisInventory.selectedInventory.Marketing_Name__c)?thisInventory.selectedInventory.Marketing_Name__c:'null';
                        reqEle.views=!string.isBlank(thisInventory.selectedInventory.View_Type__c)?thisInventory.selectedInventory.View_Type__c:'null';
                        reqEle.floor=!string.isBlank(thisInventory.selectedInventory.Floor__c)?thisInventory.selectedInventory.Floor__c:'null';
                        reqEle.category=!string.isBlank(thisInventory.selectedInventory.Unit_Type__c)?thisInventory.selectedInventory.Unit_Type__c:'null';
                        reqEle.subCategory=!string.isBlank(thisInventory.selectedInventory.Brand__c)?thisInventory.selectedInventory.Brand__c:'null';
                        reqEle.inventoryThreshold=0;
                        reqEle.customerthresholdValue=0;
                        //reqEle.region=!string.isBlank(thisInventory.jointBuyerList[0].Country__c)?thisInventory.jointBuyerList[0].Country__c:'null';
                        // reqEle.residence=!string.isBlank(thisInventory.jointBuyerList[0].City__c)?thisInventory.jointBuyerList[0].City__c:'null';
                        
                        if(!thisInventory.jointBuyerList.isEmpty()){
                            for(Buyer__c buyer :thisInventory.jointBuyerList){
                                if(buyer.Primary_Buyer__c == true){
                                    reqEle.region=!string.isBlank(buyer.Country__c)?buyer.Country__c:'null';
                                    reqEle.residence=!string.isBlank(buyer.City__c)?buyer.City__c:'null';
                                } 
                                
                                else{
                                    reqEle.region='null';
                                    reqEle.residence='null';  
                                }
                            }
                        }
                        else{
                            reqEle.region='null';
                            reqEle.residence='null';
                        }
                        if(!CamList.isEmpty()){
                            for(Campaign_Inventory__c C : CamList){
                                if(C.Inventory__c == thisInventory.selectedInventory.Id){
                                    reqEle.campaignNameInput=!string.isBlank(C.Campaign__r.Campaign_Name__c)? C.Campaign__r.Campaign_Name__c:'null';
                                    break;
                                }
                            }
                        }
                        else{
                            reqEle.campaignNameInput= 'null';
                        }
                        reqEle.noofUnits=0;
                        reqEle.totalArea=0;   
                        reqEle.totalDealValue=0;
                        reqEle.noofUnitsInput=0;
                        // add new Parameter
                        reqEle.schemeId=6;
                        reqEle.noofunitsinputPN=2;
                        reqEle.totalAreaPN=0;
                        reqEle.totalDealValuePN=0;
                        reqEle.promoIdPN=4;
                        unitsList.add(reqEle);
                    }
                    
                    /*else{     
                        
                        // Send all paramter as blank when inventory have Floor_Package_Type__c value
                        
                        DH_RequestWrapper.cls_demo_project1_InOutObject reqEle=new  DH_RequestWrapper.cls_demo_project1_InOutObject();
                        reqEle.productType = 'null' ;
                        reqEle.dateofBooking=system.now().format('YYYY-MM-dd');
                        reqEle.projectName='null';
                        reqEle.buildingName='null';
                        reqEle.bedroomType='null';
                        reqEle.numberofBedrooms='null';
                        reqEle.cunstructionStatus='null';
                        reqEle.area= 0;
                        reqEle.price=0;
                        reqEle.agent='null';
                        reqEle.pcId='null';
                        reqEle.unitIdCN=!string.isBlank(thisInventory.selectedInventory.Unit__c)?thisInventory.selectedInventory.Unit__c:'null';
                        reqEle.marketingProject='null';
                        reqEle.views='null';
                        reqEle.floor='null';
                        reqEle.category='null';
                        reqEle.subCategory='null';
                        reqEle.inventoryThreshold=0;
                        reqEle.customerthresholdValue=0;
                        reqEle.region='null';
                        reqEle.residence='null';
                        reqEle.campaignNameInput= 'null';
                        reqEle.noofUnits=0;
                        reqEle.totalArea=0;
                        reqEle.totalDealValue=0;
                        reqEle.noofUnitsInput=0;
                        reqEle.schemeId=6;
                        reqEle.noofunitsinputPN=2;
                        reqEle.totalAreaPN=0;
                        reqEle.totalDealValuePN=0;
                        reqEle.promoIdPN=4;
                        unitsList.add(reqEle);
                        
                    }*/
                }
            }
        }
        DH_RequestWrapper.cls_object inout=new  DH_RequestWrapper.cls_object();
        
        inout.demo_project1_InOutObject=unitsList;
        
        DH_RequestWrapper.cls_insert inst=new  DH_RequestWrapper.cls_insert();
        
        inst.object_1=inout;
        
        inst.out_identifier='InOutObject';
        DH_RequestWrapper.cls_commands cmnd=new  DH_RequestWrapper.cls_commands();
        cmnd.insert_1=inst;
        List<DH_RequestWrapper.cls_commands> cmndList=new  List<DH_RequestWrapper.cls_commands>();
        cmndList.add(cmnd);
        DH_RequestWrapper rw1=new  DH_RequestWrapper();
        rw1.commands=cmndList;
        system.debug(rw1);
        string body=JSON.serialize(rw1);
        system.debug(body);
        List<DH_ResponseWrapper> respWrapList=new  List<DH_ResponseWrapper>();
        if(!string.isBlank(body)){
            body=body.contains('demo_project1_InOutObject') ? body.replace('demo_project1_InOutObject','demo.project1.InOutObject') : body;
            body=body.contains('object_1') ? body.replace('object_1','object') : body;
            body=body.contains('insert_1') ? body.replace('insert_1','insert'): body;
            system.debug('Request body for Drool***'+body);
            respWrapList=DH_DroolCalloutService.getResponse(body);
        }
        system.debug('Response body for Drool::::'+respWrapList);
        if(respWrapList!=null){
            return respWrapList; 
        }
        else
            return null;
        
    }
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    @RemoteAction
    global static string createpaymentPlanHeader(String selectedInvIds) {
        
        return 'Error';
    }
    
    
}