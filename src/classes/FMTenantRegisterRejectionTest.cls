/*-------------------------------------------------------------------------------------------------
Description: Test class for class FMTenantRegisterRejection
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
v1.0     | 29-07-2019       | Lochana Rajput   | Added test method to test Tenant Registration rfejction functionality
=============================================================================================================================
*/
@isTest
private class FMTenantRegisterRejectionTest {

    @isTest
    public static void testTRApprovalRejection() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        NSIBPM__Service_Request__c objServReq = TestDataFactory_CRM.createServiceRequest();
        insert objServReq ;

        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objServReq.Id,1);
        insert lstBooking ;

        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus ;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking ,3);
        for( Booking_Unit__c objUnit : lstBookingUnit  ) {
            objUnit.Resident__c = objAcc.Id ;
            objUnit.Handover_Flag__c = 'Y' ;
        }
        lstBookingUnit[2].Owner__c = objAcc.Id ;
        insert lstBookingUnit ;

        list<FM_Case__c> lstFMCases = new list<FM_Case__c>();

        Id recTypeId=Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Registration').getRecordTypeId();
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Tenant_Registration';
        fmCaseObj.Booking_Unit__c=lstBookingUnit[0].id;
        fmCaseObj.Email__c='a@gmail.com';
        fmCaseObj.Mobile_no__c='12121212';
        fmCaseObj.RecordTypeId=recTypeId;
        fmCaseObj.Status__c='Submitted';
        fmCaseObj.Origin__c='Portal';
        fmCaseObj.Approval_Status__c = 'Pending';
        fmCaseObj.Submit_for_Approval__c = true;
        fmCaseObj.Approving_Authorities__c='Property Manager__FM Manager,Master Community Property Manager';
        fmCaseObj.Account__c=objAcc.id;
        fmCaseObj.Current_Approver__c = 'FM Manager__';
        fmCaseObj.Tenant_Email__c = 'test@gmail.com';
        insert fmCaseObj;

        Approval.ProcessSubmitRequest req1 =new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval- FM Manager.');
        req1.setObjectId(fmCaseObj.id);
        req1.setNextApproverIds(new Id[] {UserInfo.getUserId()});

        // Submit the approval request for the cli
        Approval.ProcessResult result = Approval.process(req1);

        // Verify the result
        System.assert(result.isSuccess());

        System.assertEquals(
            'Pending', result.getInstanceStatus(),
            'Instance Status'+result.getInstanceStatus());


        // Approve the submitted request
        // First, get the ID of the newly created item
        List<Id> newWorkItemIds = result.getNewWorkitemIds();

        // Instantiate the new ProcessWorkitemRequest object and populate it
        Approval.ProcessWorkitemRequest req2 =
            new Approval.ProcessWorkitemRequest();
        req2.setComments('manager');
        req2.setAction('Reject');
        req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});

        // Use the ID from the newly created item to specify the item to be worked
        req2.setWorkitemId(newWorkItemIds.get(0));

        // Submit the request for approval
        Approval.ProcessResult result2 =  Approval.process(req2);
        system.debug('mazi query'+[select Id, Status from ProcessInstance where TargetObjectId = :fmCaseObj.id]);
        fm_case__c objCase = [SELECT Id, Approval_Status__c,Description__c,Manager_Comments__c,
                                            recordType.DeveloperName,Outstanding_service_charges__c,
                                            Admin__c,Request_Type_DeveloperName__c,
                                            Tenant_Email__c,Name,
                                            (SELECT Id, IsPending, ProcessInstanceId
                                                  , StepStatus
                                                  , Comments FROM ProcessSteps
                                            WHERE (StepStatus ='Started' OR StepStatus ='Rejected')
                                            ORDER BY  SystemModstamp DESC)
                                    FROM FM_Case__c where id = : fmCaseObj.id];
        system.debug('--------->objCase '+objCase);
        // Verify the results
        system.debug('--------->'+result2);
        System.assert(result2.isSuccess(), 'Result Status:'+result2.isSuccess());

        //System.assertEquals('Approved', result2.getInstanceStatus(),      'Instance Status'+result2.getInstanceStatus());

        //Approval.ProcessWorkitemRequest prWkItem = new Approval.ProcessWorkitemRequest();
        Approval.ProcessSubmitRequest objApprovalSubmitRequest = new Approval.ProcessSubmitRequest();
        objApprovalSubmitRequest.setComments( 'manager' );
        //prWkItem.setAction('Reject');
        objApprovalSubmitRequest.setObjectId( fmCaseObj.id );
        objApprovalSubmitRequest.setProcessDefinitionNameOrId( 'FM_Approval_Process' );
        objApprovalSubmitRequest.setSkipEntryCriteria( true );
        objApprovalSubmitRequest.setNextApproverIds( new Id[] { UserInfo.getUserId() } );
        Approval.ProcessResult[] lstApprovalResult = Approval.process(new List<Approval.ProcessSubmitRequest>{objApprovalSubmitRequest});
        system.debug('result'+lstApprovalResult);
        ProcessInstance pi = [Select ID, Status, TargetObject.Name,
                (SELECT Id, ActorId, ProcessInstanceId FROM Workitems),
                (SELECT Id, StepStatus, Comments FROM Steps) From ProcessInstance
                Where TargetObjectID =:fmCaseObj.id limit 1];
        system.debug('pi'+pi);
        lstFMCases.add(fmCaseObj);
        //FmcTenantRegistrationAccountBatch.recallApprovals(new List<FM_case__c>{fmCaseObj});

        Test.startTest();
            FMTenantRegisterRejection controller = new FMTenantRegisterRejection();
            FMTenantRegisterRejection.createVerifyTask(lstFMCases);
        Test.stopTest();

    }


}