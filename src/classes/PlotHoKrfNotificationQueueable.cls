/**********************************************************************************************
Description: Queueable class to send Plot Ho KRF Notification
----------------------------------------------------------------------------------------------
Version Date(DD-MM-YYYY)    Author              Version History
-----------------------------------------------------------------------------------------------
1.0     04-11-2019          Aishwarya Todkar    Initial Draft
***********************************************************************************************/
Public Class PlotHoKrfNotificationQueueable implements Queueable,Database.AllowsCallouts {
    
    Id caseId;
    public PlotHoKrfNotificationQueueable( Id caseRecId ){
        System.debug( 'caseRecId -==' + caseRecId );
        caseId = caseRecId;
    }   

    public void execute(QueueableContext qc){
        System.debug( 'caseId== ' + caseId );
        if( caseId != null ) {
            Case objCase = [SELECT
                                Id
                                , Booking_Unit__r.Booking__r.Account__c
                                , Booking_Unit__r.Booking__r.Account__r.Name
                                , Booking_Unit__r.Unit_Name__c
                                , Owner.Name
                                , Owner.Email
                                , (SELECT
                                        Id
                                        , Attachment_URL__c
                                        , Name
                                    FROM
                                        SR_Attachments__r
                                    WHERE
                                        Attachment_URL__c != null)
                            FROM
                                Case
                            WHERE
                                Booking_Unit__c != null
                            AND
                                Booking_Unit__r.Booking__c != null
                            AND
                                Booking_Unit__r.Booking__r.Account__c !=null
                            AND 
                                id =: caseId 
                           ];
            //Get Sendgrid Email details from custom metadat
            List<SendGrid_Email_Details__mdt> listPlotHoCs 
                = new List<SendGrid_Email_Details__mdt>( [SELECT
                                                            From_Address__c
                                                            , From_Name__c
                                                            , To_Address__c
                                                            , To_Name__c
                                                            , CC_Address__c
                                                            , CC_Name__c
                                                            , Bcc_Address__c
                                                            , Bcc_Name__c
                                                            , Reply_To_Address__c
                                                            , Reply_To_Name__c
                                                            , Email_Template__c
                                                        FROM
                                                            SendGrid_Email_Details__mdt
                                                        WHERE
                                                            MasterLabel = 'Plot HO KRF Email Notification'
                                                        LIMIT 1
                                                        ] );
                                                        
            if( listPlotHoCs != null && listPlotHoCs.size() > 0) {
                SendGrid_Email_Details__mdt objPlotHoCs = listPlotHoCs[0];
                List<EmailTemplate> listPlotHoEmailTemplate;
                
                if( String.isNotBlank(objPlotHoCs.Email_Template__c) ) {
                    
                    //Get Email Template
                    listPlotHoEmailTemplate = new List<EmailTemplate>( [SELECT 
                                                    ID, Subject, Body, HtmlValue, TemplateType
                                                FROM 
                                                    EmailTemplate 
                                                WHERE 
                                                    Name =: objPlotHoCs.Email_Template__c limit 1]
                                                    );
                    system.debug('listPlotHoEmailTemplate  === ' + listPlotHoEmailTemplate );
                    //Prepare SendGrid Email Details
                    if( listPlotHoEmailTemplate != null && listPlotHoEmailTemplate.size() > 0 ) {
                        EmailTemplate objPlotHoEmailTemplate = listPlotHoEmailTemplate[0];

                        String toAddress = String.isNotBlank( objPlotHoCs.To_Address__c ) ? objPlotHoCs.To_Address__c : '';
                        String toName = String.isNotBlank( objPlotHoCs.To_Name__c ) ? objPlotHoCs.To_Name__c : '';
                        String ccAddress = String.isNotBlank( objPlotHoCs.CC_Address__c ) ? objPlotHoCs.CC_Address__c : '';
                        String ccName = String.isNotBlank( objPlotHoCs.CC_Name__c ) ? objPlotHoCs.CC_Name__c : '';
                        String bccAddress = String.isNotBlank( objPlotHoCs.Bcc_Address__c ) ? objPlotHoCs.Bcc_Address__c : '';
                        String bccName = String.isNotBlank( objPlotHoCs.Bcc_Name__c ) ? objPlotHoCs.Bcc_Name__c : '';
                        String fromAddress = String.isNotBlank( objPlotHoCs.From_Address__c ) ? objPlotHoCs.From_Address__c : '';
                        String fromName = String.isNotBlank( objPlotHoCs.From_Name__c ) ? objPlotHoCs.From_Name__c : '';
                        String replyToAddress = String.isNotBlank( objPlotHoCs.Reply_To_Address__c ) ? objPlotHoCs.Reply_To_Address__c : '';
                        String replyToName = String.isNotBlank( objPlotHoCs.Reply_To_Name__c ) ? objPlotHoCs.Reply_To_Name__c : '';
                        String contentType = 'text/html';
                        String contentValue = replaceMergeFields( objPlotHoEmailTemplate.HtmlValue, objCase );
                        String contentBody = replaceMergeFields( objPlotHoEmailTemplate.Body, objCase );
                        String subject = replaceMergeFields( objPlotHoEmailTemplate.Subject, objCase );
                        ccAddress = ccAddress + ',' + objCase.Owner.Email;
                        List<Attachment> lstAttach;
                        
                        system.debug('toAddress === ' + toAddress);
                        system.debug('fromAddress === ' + fromAddress);
                        system.debug('contentValue === ' + contentValue);
                        system.debug('ccAddress === ' + ccAddress);

                        //Call method to send an email via SendGrid
                        List<EmailMessage> lstEmails = new List<EmailMessage>(); 
                        if(toAddress != '') {
                                  
                            SendGridEmailService.SendGridResponse objSendGridResponse = 
                                SendGridEmailService.sendEmailService(
                                                            toAddress, ''
                                                            , ccAddress, ''
                                                            , bccAddress, ''
                                                            , subject, ''
                                                            , fromAddress, ''
                                                            , replyToAddress, ''
                                                            , contentType
                                                            , contentValue, ''
                                                            , new List<Attachment>{} );
                            
                            system.debug('objSendGridResponse === ' + objSendGridResponse);
                            
                            String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;
                            System.debug('responseStatus== ' + responseStatus);

                            //Create reportable activity 
                            if (responseStatus == 'Accepted') {
                                Id accountId =  objCase.Booking_Unit__c != null 
                                                && objCase.Booking_Unit__r.Booking__c != null 
                                                && objCase.Booking_Unit__r.Booking__r.Account__c != null
                                                ? objCase.Booking_Unit__r.Booking__r.Account__c
                                                : null;

                                EmailMessage mail = new EmailMessage();
                                mail.Subject = subject;
                                mail.MessageDate = System.Today();
                                mail.Status = '3';//'Sent';
                                mail.relatedToId = accountId;
                                mail.Account__c  = accountId;
                                mail.Type__c = 'PLOT HO KRF Notification';
                                mail.ToAddress = toAddress;
                                mail.FromAddress = fromAddress;
                                mail.TextBody = contentBody;//contentValue.replaceAll('\\<.*?\\>', '');
                                mail.Sent_By_Sendgrid__c = true;
                                mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                                mail.Booking_Unit__c = objCase.Booking_Unit__c;
                                mail.CcAddress = ccAddress;
                                mail.BccAddress = bccAddress;
                                lstEmails.add(mail);
                                system.debug('Mail obj == ' + mail);
                            }
                        }
                        System.debug('lstEmails=== ' + lstEmails);
                        if(lstEmails.size() > 0) {
                            if(!Test.isRunningTest()) {
                                insert lstEmails;
                            }
                        }
                    } // END listPlotHoEmailTemplate if
                } // END Email_Template__c if
            } // End listPlotHoCs if
        } // End case if
    }

/*********************************************************************************
 * Method Name : replaceMergeFields
 * Description : Replace merge fields from email contents
 * Return Type : String
 * Parameter(s): Contents, Case
**********************************************************************************/
    public static string replaceMergeFields( String strContents, Case objCase) {
        if( String.isNotBlank( strContents ) ) {
            if( strContents.contains( '{Account name}' ) ) {
                if( String.isNotBlank( objCase.Booking_Unit__r.Booking__r.Account__r.name )) {
                    strContents = strContents.replace( '{Account name}', objCase.Booking_Unit__r.Booking__r.Account__r.name );
                }
                else {
                    strContents = strContents.replace( '{Account name}', '' );
                }
            }

            if( strContents.contains( '{unit name}' ) ) {
                if( String.isNotBlank( objCase.Booking_Unit__r.Unit_Name__c )) {
                    strContents = strContents.replace( '{unit name}', objCase.Booking_Unit__r.Unit_Name__c );
                }
                else {
                    strContents = strContents.replace( '{unit name}', '' );
                }
            }

            if( strContents.contains( '{Name of HO case owner}' ) ) {
                if( String.isNotBlank( objCase.Owner.Name )) {
                    strContents = strContents.replace( '{Name of HO case owner}', objCase.Owner.Name );
                }
                else {
                    strContents = strContents.replace( '{Name of HO case owner}', '' );
                }
            }

            if( strContents.contains( '{Documents}' ) ) {
                
                if( objCase.SR_Attachments__r.size() > 0 ) {
                    String documents = getDocUrls( objCase.SR_Attachments__r );
                    if( String.isNotBlank( documents ) ) {
                        strContents = strContents.replace( '{Documents}', documents );
                    }
                    else {
                        strContents = strContents.replace( '{Documents}', 'Not Available!' );
                    }    
                }
                else {
                    strContents = strContents.replace( '{Documents}', 'Not Available!' );
                }
            }
        }
        return strContents;
    }

/*********************************************************************************
 * Method Name : getDocUrls
 * Description : To get Document URLs
 * Return Type : String
 * Parameter(s): List of SR_Attachments__c
**********************************************************************************/
    public static String getDocUrls( List<SR_Attachments__c> lstSRAttachment) {
        String docUrl = '';
        if( lstSRAttachment != null && lstSRAttachment.size() > 0 ) {
            docUrl = '<ul>';
            for( SR_Attachments__c objSR : lstSRAttachment ) {
                if( objSR.Attachment_URL__c != null ) {
                    docUrl += '<li><a href=\'' + objSR.Attachment_URL__c + '\'>' + objSR.Name + '</a></li>';
                }
            }
            docUrl += '</ul>'; 
        }
        return docUrl;
    }
}