@isTest
Private class DAMAC_Inventory_External_Test {

    @isTest
    static void test_method_one() {
        Location__c loc = InitializeSRDataTest.createLocation('123','Building');
        loc.Property_ID__c = '123';
        insert loc;    
        
        Property__c newProperty = new Property__c();
        newProperty.Property_ID__c  = 1;
        newProperty.Property_Code__c    = 'VIR' ;
        newProperty.Property_Name__c    = 'VIRIDIS @ AKOYA OXYGEN' ;
        newProperty.District__c = 'AL YUFRAH 2' ;
        newProperty.AR_Transaction_Type__c  = 'INV VIR' ;
        newProperty.Penalty_Transaction_Type__c = 'DM-PENALTIES-VIR' ;
        newProperty.Brokerage_Distribution_Set__c   = '11600' ;
        newProperty.Sales_Commission_Dist_Set__c    = '11601' ;
        newProperty.Currency_Of_Sale__c = 'AED' ;
        newProperty.Signature_Col_Customer_Stmt__c  = 'Front Line Investment Management Co. LLC' ;
        newProperty.EOI_Enabled__c = true;
        insert newProperty;   
        
        Marketing_Documents__c mDoc = new Marketing_Documents__c();
        mDoc.Marketing_Name__c = 'Test MarketingDoc';
        insert mDoc;
        
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;
        
        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Property__c = objProperty.Id;
        lstInv[0].Marketing_Name_Doc__c = mDoc.id;
        lstInv[0].Unit_Location__c = loc.id;
        lstInv[0].building_location__c = loc.id;
        lstInv[0].property_id__c = newProperty.id;
        
        insert lstInv; 
        
        SalesOffer_Record__c salesObj = new SalesOffer_Record__c();
        salesObj.Selected_Inventory_Ids__c = String.valueOf(lstInv[0].id);
        salesObj.Type__c = 'DownLoad Availaibility';
        insert salesObj;
        
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/queryInventory/salesOffer/link'; 
        req.params.put ('id', salesObj.id);
        req.httpMethod = 'GET';        
        RestContext.request = req;
        RestContext.response= res;
        DAMAC_Inventory_External.getInventory();        
        
        req.requestURI = '/queryInventory/salesOffer/retrieve'; 
        req.params.put ('id', salesObj.id);
        req.httpMethod = 'GET';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External.getInventory();
        
        req.requestURI = '/queryInventory/searchInventory';
        req.params.put ('value', 'test');
        req.httpMethod = 'GET';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External.getInventory();
        
        req.requestURI = '/queryInventory/projects';
        req.params.put ('value', 'test');
        req.httpMethod = 'GET';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External.getInventory();
        
        req.requestURI = '/queryInventory';
        req.params.put ('type', 'Popular');
        req.httpMethod = 'GET';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External.getInventory();
        
        req.requestURI = '/queryInventory';
        req.params.put ('type', 'General');
        req.httpMethod = 'GET';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External.getInventory();
        
        req.requestURI = '/queryInventory';
        req.params.put ('type', 'New');
        req.httpMethod = 'GET';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External.getInventory();
        
        req.requestURI = '/queryInventory/unit';
        req.params.put ('id', lstInv[0].id);
        req.httpMethod = 'GET';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External.getInventory();
        
        req.requestURI = '/queryInventory/offers';
        req.params.put ('unitid', lstInv[0].id);
        req.httpMethod = 'GET';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External.getInventory();
        
        req.requestURI = '/queryInventory/offers/v2';
        req.params.put ('unitid', lstInv[0].id);
        req.httpMethod = 'GET';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External.getInventory();
        
        req.requestURI = '/queryInventory/terms';
        req.params.put ('id', lstInv[0].id);
        req.httpMethod = 'GET';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External.getInventory();
        
        req.requestURI = '/queryInventory/projCoordinates';
        req.params.put ('id', lstInv[0].id);
        req.httpMethod = 'GET';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External.getInventory();
        
        req.requestURI = '/queryInventory/proj/brochures';
        req.params.put ('id', lstInv[0].id);
        req.httpMethod = 'GET';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External.getInventory();
        
        req.requestURI = '/queryInventory/proj/gallery';
        req.params.put ('id', lstInv[0].id);
        req.httpMethod = 'GET';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External.getInventory();
        
        req.requestURI = '/queryInventory/proj/360degree';
        req.params.put ('id', lstInv[0].id);
        req.httpMethod = 'GET';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External.getInventory();
        
        req.requestURI = '/queryInventory/proj';
        req.params.put ('id', lstInv[0].id);
        req.httpMethod = 'GET';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External.getInventory();
        
        req.requestURI = '/queryInventory/proj/details';
        req.params.put ('id', lstInv[0].id);
        req.httpMethod = 'GET';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External.getInventory();
        
        List<Currency_Rate__c> currencyconversionlst = new List<Currency_Rate__c>();
        
        Currency_Rate__c currency1 = new Currency_Rate__c();
        currency1.Conversion_Date__c = System.today();
        currency1.Conversion_Rate__c = 0.12;
        currency1.From_Currency__c = 'AED';
        currency1.To_Currency__c = 'INR';
        currency1.IPMS_Row_ID__c= '12345';
        currencyconversionlst .add(currency1);
        
        Currency_Rate__c currency3 = new Currency_Rate__c();
        currency3.Conversion_Date__c = System.today();
        currency3.Conversion_Rate__c = 0.12;
        currency3.From_Currency__c = 'AED';
        currency3.To_Currency__c = 'EUR';
        currency3.IPMS_Row_ID__c= '12348';
        currencyconversionlst .add(currency3);
        
        Currency_Rate__c currency2 = new Currency_Rate__c();
        currency2.Conversion_Date__c = System.today();
        currency2.Conversion_Rate__c = 0.12;
        currency2.From_Currency__c = 'EUR';
        currency2.To_Currency__c = 'AED';
        currency2.IPMS_Row_ID__c= '12346';
        currencyconversionlst .add(currency2);
        
        insert currencyconversionlst ;
        
        req.requestURI = '/queryInventory/currencyRates';
        req.params.put ('id', lstInv[0].id);
        req.httpMethod = 'GET';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External.getInventory();
        
        req.requestURI = '/queryInventory/currencycodes';
        req.params.put ('id', lstInv[0].id);
        req.httpMethod = 'GET';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External.getInventory();
        
        
        req.requestURI = '/queryInventory/salesOffer/Create';
        req.requestBody = Blob.valueOf('{"invIds" : "'+lstInv[0].id+'" }');
        req.httpMethod = 'POST';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External.doPost();
        
        
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        
        
        Inventory_App_Layouts__c obj = new Inventory_App_Layouts__c();
        obj.Active__c = true;
        obj.Order__c = 1;
        obj.Search_Field__c = '(MAX)Price_Per_Sqft__c';
        obj.Search_Label__c = 'Max Price/sft';
        obj.Search_Section__c = 'Other Details';
        obj.Value_Type__c = 'decimal';
        insert obj;
        
        req.requestURI = '/InventoryService';
        
        req.httpMethod = 'GET';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_InventoryService.getInventory();
        
        
        req.requestURI = '/InventoryService/applyFilters';
        req.requestBody = Blob.valueOf('{"(MIN)Special_Price__c" : "1"}');
        req.httpMethod = 'POST';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_InventoryService.getResults();
        
        Test.stopTest();
        
        
            
    }
}