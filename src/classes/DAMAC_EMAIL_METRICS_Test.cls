@isTest
public class DAMAC_EMAIL_METRICS_Test{
    static testMethod void testParse() {
        String json = '[{\"email\":\"srikanth.v@bigworks.co\",\"event\":\"processed\",\"sg_event_id\":\"L6LRScnlQ0KYKIgxcwMpHA\",\"sg_message_id\":\"L4Nm8tbbSeyH1rfiEvvxGg.filter0038p3iad2-12212-5B2BF2E6-38.0\",\"smtp-id\":\"<L4Nm8tbbSeyH1rfiEvvxGg@ismtpd0003p1lon1.sendgrid.net>\",\"timestamp\":1529606887,\"uniqueId\":\"a4z9E000000AblKQAS\"},'+
        '{\"email\":\"srikanth.v@bigworks.co\",\"event\":\"delivered\",\"ip\":\"167.89.100.223\",\"response\":\"250 2.0.0 OK 1529606887 d1-v6si5207553qve.70 - gsmtp\",\"sg_event_id\":\"zrBP5-ZiQ7uI4-iA_hYbWg\",\"sg_message_id\":\"L4Nm8tbbSeyH1rfiEvvxGg.filter0038p3iad2-12212-5B2BF2E6-38.0\",\"smtp-id\":\"<L4Nm8tbbSeyH1rfiEvvxGg@ismtpd0003p1lon1.sendgrid.net>\",\"timestamp\":1529606887,\"tls\":1,\"uniqueId\":\"a4z9E000000AblKQAS\"}]';
        List<SendGridResponseParser> obj = SendGridResponseParser.parse(json);
        
        String errorJson = '{\"message\":\"test\", \"field\":\"test\",\"help\":\"test\"}';
        SendGridErrorHandler objError = SendGridErrorHandler.parse (errorJSON);
        
        
        
    }
    static testMethod void testEventParse() {
        testEvent ('Processed');
        testEvent ('delivered');
        testEvent ('deferred');
        testEvent ('dropped');
        testEvent ('Processed');
        testEvent ('bounce');
        testEvent ('open');
        testEvent ('click');
        testEvent ('group_resubscribe');
        testEvent ('group_unsubscribe');
        testEvent ('spamreport');
        testEvent ('unsubscribe');
        
    }
    static void testEvent (String eventType) {
        Email_request__c reqMetrics = new Email_request__c ();
        reqMetrics.Template_For__c = 'Sales Offer Inquiry';
        insert reqMetrics;
        
        Email_Metrics__c metrics = new Email_Metrics__c ();
        metrics.Email_request__c = reqMetrics.iD;
        Damac_Constants.skip_EmailMetricsTrigger = TRUE;
        insert metrics;
        
        String body = '[{"email":"srikanth.v@bigworks.co","timestamp":1529917578,"ip":"64.233.173.36","uniqueId":"'+metrics.ID+'","sg_event_id":"22XVSTLWR2yIJheQFwwvcw","sg_message_id":"haPEXfVsTwCT6gijMvZ9gg.filter0048p3mdw1-3035-5B308C63-31.0","useragent":"Mozilla/5.0 (Windows NT 5.1; rv:11.0) Gecko Firefox/11.0 (via ggpht.com GoogleImageProxy)","event":"'+eventType+'"}]';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'http://testing.com';  
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(body);
        Blob body1 = req.requestBody;
        String bodyString = body1.toString(); 
        RestContext.request = req;
        RestContext.response = res;
        
        DAMAC_EMAIL_METRICS.doPost ();
    }
}