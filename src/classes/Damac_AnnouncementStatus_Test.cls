@isTest
public class Damac_AnnouncementStatus_Test {
public static User portalUser;
    public static testMethod void method1() {
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole1');
        insert userRoleObj;        
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 't446', email='xyz441@email.com',
            emailencodingkey='UTF-8', lastname='User 45446', languagelocalekey='en_US',
            localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
            ,timezonesidkey='America/Los_Angeles', username='xyz144@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
            Account testAccount = new Account();
                testAccount.Name='Test Account' ;
                testAccount.Country_of_Incorporation_New__c  = 'Afghanistan';
                DAMAC_Constants.skip_AccountTrigger  = true;
            insert testAccount;
            Contact cont = new Contact();
                cont.FirstName ='Test';
                cont.LastName ='Test';
                cont.accountid =testAccount.id;
                cont.Announcement_Status__c  = 'Read';
            insert cont;
            Announcement__c acc = New Announcement__c ();
                acc.Countries_Include__c  = 'Afghanistan';
                acc.Start_Date__c  = system.today();
                acc.End_Date__c  = system.today();
                acc.Active__c  = true;
                acc.Description__c  = 'test';
            insert acc;
            portalUser = InitialiseTestData.getPortalUser('test99@test.com', cont.id, 'Admin');
            System.runAs(portalUser){
                Damac_AnnouncementStatus obj = new Damac_AnnouncementStatus();
                DataBase.executeBatch(obj);
            }
        }
    }
}