public without sharing class PromotionsUtility 
{

    //submit record for approval
    public static void submitRecordForApproval(Case objCase)
    {
        try
        {
            system.debug('objCase.OwnerId '+objCase.OwnerId);
            User objUser = new User();
            String strCaseOwner = String.valueOf(objCase.OwnerId);
            system.debug('strCaseOwner : '+strCaseOwner);
            system.debug('strCaseOwner.startsWith : '+strCaseOwner.startsWith('00G'));
            system.debug('Label.DefaultCaseOwnerId : '+Label.DefaultCaseOwnerId);
            if(strCaseOwner.startsWith('00G')){
                objUser = [Select Manager_Role__c, Id From User where Id =: Label.DefaultCaseOwnerId];
            } else{
                objUser = [Select Manager_Role__c, Id FROM User where Id =: objCase.OwnerId];
            }
            system.debug('objUser '+objUser);
            if(objUser != null && String.isNotBlank(objUser.Manager_Role__c))
            {
                system.debug('objUser.Manager_Role__c '+objUser.Manager_Role__c);
                objCase.Approving_Authorities__c = objUser.Manager_Role__c;
                objCase.Submit_for_Approval__c = true;
                objCase.Status = 'Manager Approval';
                update objCase;

                Task objTask = new Task();
                objTask.ActivityDate = System.today() +1;
                objTask.Assigned_User__c = 'CRE Manager';
                objTask.CurrencyIsoCode = 'AED';
                objTask.OwnerId = objUser.Id;
                objTask.Priority = 'High';
                objTask.Process_Name__c = 'Promotion Package';
                objTask.Status = 'Not Started';
                objTask.Subject = 'Manager Approval - Pending';
                objTask.WhatId = objCase.Id;

                insert objTask;
            }
        }
        catch(Exception exp)
        {
            system.debug('exception occured '+exp.getMessage());
            system.debug('exception occured '+exp.getStackTraceString());
            errorLoggerNew('Error Promotion Submit Record Approval: '+exp.getMessage()+' - '+exp.getStackTraceString(),objCase.Id,'');
        }
    }

    //submit record for approval
    @InvocableMethod
    public static void submitRecordForApprovalProtal(List<Task> lstSR)
    {   
        system.debug('submitRecordForApprovalProtal called');
        Case objCase = new Case();
        try
        {   
            if(lstSR != null && lstSR.size() > 0)
            {
              Task objTask = lstSR[0];
              system.debug('objTask '+objTask );
              
              objCase = [ Select 
                          Id
                          ,OwnerId
                          ,Booking_unit__c,Total_Amount__c
                          ,Submit_for_Approval__c 
                          FROM
                          Case
                          Where Id =: objTask.WhatId
                        ];
              if(objTask.Subject == 'Verify Case Details' && objTask.Status.equalsIgnoreCase('Completed'))
              {
                    submitRecordForApproval(objCase);
                }
            }
        }
        catch(Exception exp)
        {
            system.debug('exception occured '+exp.getMessage());
            system.debug('exception occured '+exp.getStackTraceString());
            errorLoggerNew('Error Promotion Submit Record Approval Portal: '+exp.getMessage()+' - '+exp.getStackTraceString(),objCase.Id,'');
        }
    }

    // method used to insert record in Error Log Object
    public static void errorLoggerNew(string strErrorMessage, string strCaseID,string strBookingUnitID)
    {
        Error_Log__c objError = new Error_Log__c();
        objError.Error_Details__c = strErrorMessage;
        objError.Process_Name__c = 'Promotions';
        if(String.isNotBlank(strCaseID) && strCaseID.startsWith('500')){
            objError.Case__c = strCaseID;
        }
        if(String.isNotBlank(strBookingUnitID)){
            objError.Booking_unit__c = strBookingUnitID;
        }
        insert objError;
    }
}