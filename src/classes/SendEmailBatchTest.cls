@istest
public class SendEmailBatchTest {
    @isTest(SeeAllData=false)
    static void testMethod1(){
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles',
         UserName=uniqueUserName);

        Account a = new Account();
        //a.Name = 'Test Account';  
        a.FirstName='Test FirstName';
        a.LastName='Test LastName';
        a.party_ID__c = '10';
        a.Email__c = 'test@test.com';
        a.Email__pc = 'test1@test.com';
        a.RecordTypeId  = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        a.ZBEmailStatus__c = 'Valid';      
        a.PersonEmail = 'new@test.com';
        a.Email_2__pc = 'new@1.com'; 
        a.Email_3__pc = 'new@r.com';
        a.Email_4__pc = 'new@3.com';
        a.Email_5__pc = 'new@4.com';
        insert a;
        
        System.runAs(u) {
            Id FolderId = [SELECT DeveloperName,Id FROM Folder WHERE DeveloperName = 'CRE_Processes'].Id;
            EmailTemplate etCS = new EmailTemplate ();
            etCS.Name = 'EHO';
            etCS.TemplateType = 'text';
            etCS.developerName = 'HO1';
            etCS.FolderId = FolderId;
            etCS.Subject = 'Early Handover Offer: For Unit {!Booking_Unit__c.Unit_Name__c}';
            etCS.Body = 'Dear {!Booking_Unit__c.Customer_Name__c},   {!Booking_Unit__c.Unit_Type__c}, unit no {!Booking_Unit__c.Unit_Name__c} Accordingly, {!Booking_Unit__c.AWB_No__c}, for reference.';
            insert etCS;
            
            Attachment objAttach = new Attachment();
            objAttach.Name = 'Test File';
            objAttach.Body = Blob.valueOf('Unit Test Attachment Body');
            objAttach.ParentId = etCS.Id;
            insert objAttach;
        }
        
        Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = a.id;
        sr.RecordTypeId = RecType;
        insert sr;

        Booking__c  bk = new  Booking__c();
        bk.Account__c = a.id;
        bk.Deal_SR__c = sr.id;
        insert bk;
        
        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = bk.id;
        bu.Unit_Name__c = 'LSB/10/B1001'; 
        bu.Registration_Status__c = 'Agreement Cancellation Verified';
        bu.Registration_ID__c = '74655';
        insert bu;
        string buID = String.valueOf(bu.id);
        List<String> BUIds = new List<String>();
        BUIds.add(buID);
        
        Test.startTest();         
        SendEmailBatch dailyBatch = new SendEmailBatch(BUIds,'EHO_SMS','priorityservice@damacgroup.com','Email & SMS','None', 
            'Salesforce', 'HO1', 'service@damacgroup.com', 'FM', 'Yes');
        Database.executeBatch(dailyBatch);
        Test.stopTest();
    }
   @isTest(SeeAllData=false)
     static void testMethod2(){       
        
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles',
         UserName=uniqueUserName);
        
        Account a = new Account();
        //a.Name = 'Test Account';  
        a.FirstName='Test FirstName';
        a.LastName='Test LastName';
        a.party_ID__c = '10';
        a.Email__c = 'test@test.com';
        a.Email__pc = 'test1@test.com';
        a.RecordTypeId  = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        a.ZBEmailStatus__c = 'Valid';      
        a.PersonEmail = 'new@test.com';
        a.Email_2__pc = 'new@1.com'; 
        a.Email_3__pc = 'new@r.com';
        a.Email_4__pc = 'new@3.com';
        a.Email_5__pc = 'new@4.com';
        insert a;
                
        Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = a.id;
        sr.RecordTypeId = RecType;
        insert sr;

        Booking__c  bk = new  Booking__c();
        bk.Account__c = a.id;
        bk.Deal_SR__c = sr.id;
        insert bk;
        
        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = bk.id;
        bu.Unit_Name__c = 'LSB/10/B1001'; 
        bu.Registration_Status__c = 'Agreement Cancellation Verified';
        bu.Registration_ID__c = '74655';
        insert bu;
        string buID = String.valueOf(bu.id);
        List<String> BUIds = new List<String>();
        BUIds.add(buID);

        booking_unit__c objBU = [Select Booking__r.Account__r.PersonEmail from booking_unit__c where id=: bu.id];
        system.debug('objBU'+objBU.Booking__r.Account__r.PersonEmail);
        
        test.startTest();  
        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());      
        SendEmailBatch dailyBatch = new SendEmailBatch(BUIds,'EHO','priorityservice@damacgroup.com','Email & SMS','Yes', 
            'SendGrid', 'EHO', 'service@damacgroup.com', 'FM Quarterly Notice', 'Yes');
        Database.executeBatch(dailyBatch);
        test.stopTest();
    }

    @isTest(SeeAllData=false)
    static void testMethod3(){
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles',
         UserName=uniqueUserName);

        Account a = new Account();
        //a.Name = 'Test Account';  
        a.FirstName='Test FirstName';
        a.LastName='Test LastName';
        a.party_ID__c = '10';
        a.Email__c = 'test@test.com';
        a.Email__pc = 'test1@test.com';
        a.RecordTypeId  = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        a.ZBEmailStatus__c = 'Valid';      
        a.PersonEmail = 'new@test.com';
        a.Email_2__pc = 'new@1.com'; 
        a.Email_3__pc = 'new@r.com';
        a.Email_4__pc = 'new@3.com';
        a.Email_5__pc = 'new@4.com';
        insert a;
        
        System.runAs(u) {
            Id FolderId = [SELECT DeveloperName,Id FROM Folder WHERE DeveloperName = 'CRE_Processes'].Id;
            EmailTemplate etCS = new EmailTemplate ();
            etCS.Name = 'EHO';
            etCS.TemplateType = 'text';
            etCS.developerName = 'HO1';
            etCS.FolderId = FolderId;
            etCS.Subject = 'Early Handover Offer: For Unit {!Booking_Unit__c.Unit_Name__c}';
            etCS.Body = 'Dear {!Booking_Unit__c.Customer_Name__c},   {!Booking_Unit__c.Unit_Type__c}, unit no {!Booking_Unit__c.Unit_Name__c} Accordingly, {!Booking_Unit__c.AWB_No__c}, for reference.';
            insert etCS;
            
            Attachment objAttach = new Attachment();
            objAttach.Name = 'Test File';
            objAttach.Body = Blob.valueOf('Unit Test Attachment Body');
            objAttach.ParentId = etCS.Id;
            insert objAttach;
        }
        
        Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = a.id;
        sr.RecordTypeId = RecType;
        insert sr;

        Booking__c  bk = new  Booking__c();
        bk.Account__c = a.id;
        bk.Deal_SR__c = sr.id;
        insert bk;
        
        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = bk.id;
        bu.Unit_Name__c = 'LSB/10/B1001'; 
        bu.Registration_Status__c = 'Agreement Cancellation Verified';
        bu.Registration_ID__c = '74655';
        insert bu;
        string buID = String.valueOf(bu.id);
        List<String> BUIds = new List<String>();
        BUIds.add(buID);
        
        Test.startTest();         
        SendEmailBatch dailyBatch = new SendEmailBatch(BUIds,'EHO_SMS','priorityservice@damacgroup.com','Email & SMS','Yes', 
            'Salesforce', 'HO1', 'service@damacgroup.com', 'FM', 'Yes');
        Database.executeBatch(dailyBatch);
        Test.stopTest();
    }

    @isTest
    static void testPho(){
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles',
         UserName=uniqueUserName);

        Account a = new Account();
        //a.Name = 'Test Account';  
        a.FirstName='Test FirstName';
        a.LastName='Test LastName';
        a.party_ID__c = '10';
        a.Email__c = 'test@test.com';
        a.Email__pc = 'test1@test.com';
        a.RecordTypeId  = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        a.ZBEmailStatus__c = 'Valid';      
        a.PersonEmail = 'new@test.com';
        a.Email_2__pc = 'new@1.com'; 
        a.Email_3__pc = 'new@r.com';
        a.Email_4__pc = 'new@3.com';
        a.Email_5__pc = 'new@4.com';
        insert a;
        
        System.runAs(u) {
            /*Id FolderId = [SELECT DeveloperName,Id FROM Folder WHERE DeveloperName = 'CRE_Processes'].Id;
            EmailTemplate etCS = new EmailTemplate ();
            etCS.Name = 'Chinese PreHo Notice Email Template';
            etCS.TemplateType = 'text';
            etCS.developerName = 'Chinese_PreHo_Notice_Email_Template';
            etCS.FolderId = FolderId;
            etCS.Subject = 'Early Handover Offer: For Unit {!Booking_Unit__c.Unit_Name__c}';
            etCS.Body = 'Dear {!Booking_Unit__c.Customer_Name__c},   {!Booking_Unit__c.Unit_Type__c}, unit no {!Booking_Unit__c.Unit_Name__c} Accordingly, {!Booking_Unit__c.AWB_No__c}, for reference.';
            insert etCS;
            
            EmailTemplate etCS2 = new EmailTemplate ();
            etCS2.Name = 'Arabic PreHo Notice Email Template';
            etCS2.TemplateType = 'text';
            etCS2.developerName = 'Arabic_PreHo_Notice_Email_Template';
            etCS2.FolderId = FolderId;
            etCS2.Subject = 'Early Handover Offer: For Unit {!Booking_Unit__c.Unit_Name__c}';
            etCS2.Body = 'Dear {!Booking_Unit__c.Customer_Name__c},   {!Booking_Unit__c.Unit_Type__c}, unit no {!Booking_Unit__c.Unit_Name__c} Accordingly, {!Booking_Unit__c.AWB_No__c}, for reference.';
            insert etCS2;

            Attachment objAttach = new Attachment();
            objAttach.Name = 'Test File';
            objAttach.Body = Blob.valueOf('Unit Test Attachment Body');
            objAttach.ParentId = etCS.Id;
            insert objAttach;


            Attachment objAttach2 = new Attachment();
            objAttach2.Name = 'Test File';
            objAttach2.Body = Blob.valueOf('Unit Test Attachment Body');
            objAttach2.ParentId = etCS2.Id;
            insert objAttach2;*/
        }
        
        Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = a.id;
        sr.RecordTypeId = RecType;
        insert sr;

        Booking__c  bk = new  Booking__c();
        bk.Account__c = a.id;
        bk.Deal_SR__c = sr.id;
        insert bk;
        
        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = bk.id;
        bu.Unit_Name__c = 'LSB/10/B1001'; 
        bu.Registration_Status__c = 'Agreement Cancellation Verified';
        bu.Registration_ID__c = '74655';
        bu.URLs_of_Document__c = '<li> ' + '<a href="https://ptctest.damacgroup.com/COFFEE/apex/document/view/d768f8ec110b0207ba7a209f7975fbb1" target="_blank">' 
                                            + 'Statement Of Account' + '</a>' + '</li>' ;
        insert bu;
        string buID = String.valueOf(bu.id);
        List<String> BUIds = new List<String>();
        BUIds.add(buID);
        
         insert new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
        OnOffCheck__c = true);
        
         insert new IpmsRestServices__c(
                   SetupOwnerId = UserInfo.getOrganizationId(),
                   BaseUrl__c = 'http://83.111.194.181:8045/webservices/rest',
                   Username__c = 'oracle_user',
                   Password__c = 'crp1user',
                   Client_Id__c = '8MABLQM-KJ8I8-1XA58-WWCM1S1',
                   BearerToken__c = 'eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWyiwuVrJSKs5IBKK8bCUdpczEEiUrQ1MzAwtTMwsDIx2l1IoCqIC5oRFIoLQ4tSgvMTcVqM_C19HJJ9BX19vLwtNC1zDC0dRCNzzc2dcw2FCpFgBXRb-1XQAAAA.6Ym224Vwr9AniBeq6gL8OM9u4vGnUB_vbEUVjWojg14',
                   Timeout__c = 120000
                   );
                   
        FmHttpCalloutMock.Response bulkSoaMockResponse = new FmHttpCalloutMock.Response(200, 'Success', '   {  '  +
         '       "responseId": "3072019-10305298",  '  +
         '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         '       "status": "S",  '  +
         '       "responseMessage": "Process Completed",  '  +
         '       "elapsedTimeMs": 8497,  '  +
         '       "responseLines": [  '  +
         '           {  '  +
         '               "documentName": "DPSOA",  '  +
         '               "language": "EN",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
         '           },  '  +
         '           {  '  +
         '               "documentName": "DPSOA_AR",  '  +
         '               "language": "AR",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
         '           }  '  +
         '       ],  '  +
         '       "complete": true  '  +
         '  }  ');

        String BULK_SOA_str = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.BULK_SOA, new List<String> {'63062'});
        String blobStr = 'Test Blob Response';
        Test.startTest();
        
        FmHttpCalloutMock.Response blobResponse = new FmHttpCalloutMock.Response(200, 'Success', blobStr);
        Test.setMock( HttpCalloutMock.class, new SendGridResponseMock());      
        SendEmailBatch dailyBatch = new SendEmailBatch(BUIds,'Arabic_PreHo_Notice_Email_Template','priorityservice@damacgroup.com','Email Only','None', 
            'SendGrid', 'None', 'service@damacgroup.com', 'Statement Of Account', 'Yes');
        Database.executeBatch(dailyBatch);
        Test.stopTest();
    }
}