/*---------------------------------------------------------------------------------------------------------------------
Description: Controller of Complaint Case creation
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By      | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     | 08-11-2020        | Aishwarya Todkar      | removed New Complaint Request task
=======================================================================================================================*/
public without sharing class ComplaintController_CC extends ComplaintController {

    public Boolean blnFlag;

    public ComplaintController_CC() {
        super(false);
        if (CustomerCommunityUtils.isCurrentView('complaint')) {
            super();
        }
    }

    public String strCaseID { get; set; }

    public override void init(){
        System.debug('ComplaintController initselectedAcc======= '+selectedAcc);
        System.debug('ComplaintController init strSRType======= '+strSRType);
        complaintCaseObj = new Case(AccountId=selectedAcc
                                  ,Type=ComplaintProcessUtility.COMPLAINT_CASE_TYPE
                                  ,RecordTypeId=ComplaintProcessUtility.getRecordTypeId()
                                  ,Status=ComplaintProcessUtility.COMPLAINT_INITIAL_STATUS
                                  ,Origin='Portal');

        accountObj = new Account();
        bookingUnitList = new List<Booking_Unit__c>();
        isError = true;
        selectedUnit = '';
        bookingUnitDetailsFromIPMS = new UnitDetailsService.BookinUnitDetailsWrapper();
        //accountsList = new List<Account>();
        //accountsList =  [SELECT Name, Id, IsPersonAccount FROM Account];

        lstCategories = new list<SelectOption>();
            lstCategories.add(new selectOption('Unit Details', 'Unit Details'));
            lstCategories.add(new selectOption('Flags', 'Flags'));
            lstCategories.add(new selectOption('Unit Status', 'Unit Status'));
            lstCategories.add(new selectOption('Open SRs', 'Open SRs'));
        strSelectedCategory = 'Unit Details';
    }

    public override PageReference complaintSubmitCase(){
    system.debug('>>>Case'+complaintCaseObj);
      complaintCaseObj.Status=ComplaintProcessUtility.COMPLAINT_SUBMIT_STATUS;
      complaintCaseObj.Origin = 'Portal';
      // Boolean blnFlag = saveComplaintCase();
      saveComplaintCase();
      if(!blnFlag) {
          return null;
      }
      strCaseID = complaintCaseObj.Id;
      System.debug('strCaseID : '+strCaseID);
      // saveComplaintCase();
      System.debug('Case Status : '+complaintCaseObj.Status);
      /*if(complaintCaseObj.Status == 'Submitted') {
          PageReference pageRef = new PageReference('/customer');
          pageRef.setRedirect(true);
          return pageRef;
      }else {
          return NULL;
      }*/
      return null;
    }


    public override PageReference saveComplaintCase(){
        blnFlag = true;
        Integer i=1;
        if( String.isBlank( complaintCaseObj.AccountId ) ) {
            complaintCaseObj.AccountId = selectedAcc;
        }
        if( String.isBlank( complaintCaseObj.Booking_Unit__c ) ) {
            complaintCaseObj.Booking_Unit__c = selectedBookingUnitId;
        }
        try{
            String crfFileName = '';
            String attachment1 = '';
            String attachment2 = '';
            List<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocReq =
                                new List<UploadMultipleDocController.MultipleDocRequest>();
            if(String.isNotBlank( crfAttachmentBody ) && String.isNotBlank( crfAttachmentName )
                        && String.isBlank( complaintCaseObj.CRF_File_URL__c ) ) {
                UploadMultipleDocController.MultipleDocRequest objMultipleDocRequest =
                                        new UploadMultipleDocController.MultipleDocRequest();
                objMultipleDocRequest.category = 'Document';
                objMultipleDocRequest.entityName = 'Damac Service Requests';
                objMultipleDocRequest.base64Binary = EncodingUtil.base64Encode(extractBody(crfAttachmentBody));
                objMultipleDocRequest.fileDescription = extractType(crfAttachmentName);
          /*objMultipleDocRequest.fileId = 'IPMS-'+bookingUnitDetails.Registration_ID__c+'-'+extractName(crfAttachmentName);
          objMultipleDocRequest.fileName = 'CRFfile';*/

                objMultipleDocRequest.fileId = caseNumber+'-'+String.valueOf(System.currentTimeMillis() + i)+'.'+extractType(crfAttachmentName);
                objMultipleDocRequest.fileName = caseNumber+'-'+String.valueOf(System.currentTimeMillis() + i)+'.'+extractType(crfAttachmentName);
                i++;

          //caseNumber+'-'+String.valueOf(System.currentTimeMillis() + i)+'.'+extractType(crfAttachmentName);

                objMultipleDocRequest.registrationId = caseNumber;
                objMultipleDocRequest.sourceId = 'IPMS-'+caseNumber+'-'+extractName(crfAttachmentName);
                objMultipleDocRequest.sourceFileName = 'IPMS-'+caseNumber+'-'+extractName(crfAttachmentName);

                crfFileName = objMultipleDocRequest.sourceFileName;
                lstMultipleDocReq.add(objMultipleDocRequest);
            }

            if(String.isNotBlank( attachment1Body ) && String.isNotBlank( attachment1Name )
                        && String.isBlank( complaintCaseObj.Additional_Doc_File_URL__c ) ) {
                UploadMultipleDocController.MultipleDocRequest objMultipleDocRequest =
                                        new UploadMultipleDocController.MultipleDocRequest();
                objMultipleDocRequest.category = 'Document';
                objMultipleDocRequest.entityName = 'Damac Service Requests';
                objMultipleDocRequest.base64Binary = EncodingUtil.base64Encode(extractBody(attachment1Body));
                objMultipleDocRequest.fileDescription = extractType(attachment1Name);
          /*objMultipleDocRequest.fileId = 'IPMS-'+bookingUnitDetails.Registration_ID__c+'-'+extractName(attachment1Name);
          objMultipleDocRequest.fileName = 'Attachment1';*/

                objMultipleDocRequest.fileId = caseNumber+'-'+String.valueOf(System.currentTimeMillis() + i)+'.'+extractType(attachment1Name);
                objMultipleDocRequest.fileName = caseNumber+'-'+String.valueOf(System.currentTimeMillis() + i)+'.'+extractType(attachment1Name);
                i++;

                objMultipleDocRequest.registrationId = caseNumber;
                objMultipleDocRequest.sourceId = 'IPMS-'+caseNumber+'-'+extractName(attachment1Name);
                objMultipleDocRequest.sourceFileName = 'IPMS-'+caseNumber+'-'+extractName(attachment1Name);
                attachment1 = objMultipleDocRequest.sourceFileName;
                lstMultipleDocReq.add(objMultipleDocRequest);
            }

            if(String.isNotBlank( attachment2Body ) && String.isNotBlank( attachment2Name )
                            && String.isBlank( complaintCaseObj.OD_File_URL__c ) ) {
                UploadMultipleDocController.MultipleDocRequest objMultipleDocRequest =
                new UploadMultipleDocController.MultipleDocRequest();
                objMultipleDocRequest.category = 'Document';
                objMultipleDocRequest.entityName = 'Damac Service Requests';
                objMultipleDocRequest.base64Binary = EncodingUtil.base64Encode(extractBody(attachment2Body));
                objMultipleDocRequest.fileDescription = extractType(attachment2Name);
          /*objMultipleDocRequest.fileId = 'IPMS-'+bookingUnitDetails.Registration_ID__c+'-'+extractName(attachment2Name);
          objMultipleDocRequest.fileName = 'Attachment2';*/

                objMultipleDocRequest.fileId = caseNumber+'-'+String.valueOf(System.currentTimeMillis() + i)+'.'+extractType(attachment2Name);
                objMultipleDocRequest.fileName = caseNumber+'-'+String.valueOf(System.currentTimeMillis() + i)+'.'+extractType(attachment2Name);
                i++;

                objMultipleDocRequest.registrationId = caseNumber;
                objMultipleDocRequest.sourceId = 'IPMS-'+caseNumber+'-'+extractName(attachment2Name);
                objMultipleDocRequest.sourceFileName = 'IPMS-'+caseNumber+'-'+extractName(attachment2Name);
                attachment2 = objMultipleDocRequest.sourceFileName;
                lstMultipleDocReq.add(objMultipleDocRequest);
            }

            UploadMultipleDocController.data objResponseData = new UploadMultipleDocController.data();
            objResponseData = UploadMultipleDocController.getMultipleDocUrl(lstMultipleDocReq);
            system.debug('objResponseData DOC*****'+objResponseData);
            if(objResponseData != null && objResponseData.status == 'S'
                    && objResponseData.Data != null && objResponseData.Data.size() > 0) {
            system.debug('>>>Case1'+complaintCaseObj);
            for(UploadMultipleDocController.MultipleDocResponse objData : objResponseData.Data) {
                if(String.isNotBlank(objData.url) && String.isNotBlank( crfFileName )
                            && (objData.PARAM_ID.containsIgnoreCase(crfFileName))) {
                    complaintCaseObj.CRF_File_URL__c  = objData.url;
                }

                if(String.isNotBlank(objData.url) && String.isNotBlank( attachment1 )
                                    && (objData.PARAM_ID.containsIgnoreCase(attachment1))) {
                    complaintCaseObj.Additional_Doc_File_URL__c  = objData.url;
                }

                if(String.isNotBlank(objData.url) && String.isNotBlank( attachment2 )
                                    && (objData.PARAM_ID.containsIgnoreCase(attachment2))) {
                    complaintCaseObj.OD_File_URL__c  = objData.url;
                }
            }
            complaintCaseObj.Origin = 'Portal';

          // Changes done on 03/07/2018 to populate Account Email on Case
            accountObj = [SELECT Party_Id__c, IsPersonAccount, Email__pc, Email__c FROM Account WHERE Id =: selectedAcc];
            complaintCaseObj.Account_Email__c = accountObj.IsPersonAccount ? accountObj.Email__pc : accountObj.Email__c ;

            upsert complaintCaseObj;
            // Case caseList = [Select id,OwnerId, Status, Origin, Additional_Doc_File_URL__c from Case Where id = :complaintCaseObj.id];
            // system.debug('caseList>> '+caseList);
            // Task objTask = new Task();
            // try {
            //     // objTask = TaskUtility.getTask((SObject)caseList, 'New Complaint Request', 'CRE',
            //     //                'Complaint', system.today().addDays(1));

            //     // objTask.OwnerId = Label.DefaultCaseOwnerId;
            //     // objTask.Priority = 'High';
            //     // objTask.Status = 'In Progress';
            //     // objTask.currencyISOcode = caseList.currencyISOcode ;
            //     // system.debug('--objTask--Promotions'+objTask);

            //     //insert objTask;
            //       system.debug('>>>>>>>objTask'+objTask);
            // }
            // catch(exception e){
            //   System.debug('task exception '+ e);
            // }
           // return null;
          if( !crfAttachmentUploaded && String.isNotBlank( crfAttachmentBody ) && String.isNotBlank( crfAttachmentName ) ){
            uploadAttachment( complaintCaseObj.Id, extractType(crfAttachmentName), ComplaintProcessUtility.COMPLAINT_CRF_NAME, complaintCaseObj.CRF_File_URL__c );
            crfAttachmentBody = null;
            crfAttachmentName = null;

          }

          if( !attachment1Uploaded && String.isNotBlank( attachment1Body ) && String.isNotBlank( attachment1Name ) ){
            uploadAttachment( complaintCaseObj.Id, extractType(attachment1Name), ComplaintProcessUtility.COMPLAINT_ATT1_NAME, complaintCaseObj.Additional_Doc_File_URL__c );
            attachment1Body = null;
            attachment1Name = null;
          }

          if( !attachment2Uploaded && String.isNotBlank( attachment2Body ) && String.isNotBlank( attachment2Name ) ){
            uploadAttachment( complaintCaseObj.Id, extractType(attachment2Name), ComplaintProcessUtility.COMPLAINT_ATT2_NAME, complaintCaseObj.OD_File_URL__c );
            attachment2Body = null;
            attachment2Name = null;
          }
        }else{
          complaintCaseObj.Origin = 'Portal';

          // Changes done on 03/07/2018 to populate Account Email on Case
          accountObj = [SELECT Party_Id__c, IsPersonAccount, Email__pc, Email__c FROM Account WHERE Id =: selectedAcc];
          complaintCaseObj.Account_Email__c = accountObj.IsPersonAccount ? accountObj.Email__pc : accountObj.Email__c ;

          upsert complaintCaseObj;
          // Case caseList = [Select id,OwnerId, Status, Origin, Additional_Doc_File_URL__c from Case Where id = :complaintCaseObj.id];
          // system.debug('caseList>> '+caseList);
          // Task objTask = new Task();
          // try{
          // objTask = TaskUtility.getTask((SObject)caseList, 'New Complaint Request', 'CRE',
          //                'Complaint', system.today().addDays(1));

          // objTask.OwnerId = Label.DefaultCaseOwnerId;
          // objTask.Priority = 'High';
          // objTask.Status = 'In Progress';
          // objTask.currencyISOcode = caseList.currencyISOcode ;
          // system.debug('--objTask--Promotions'+objTask);

          //   insert objTask;
          //   system.debug('>>>>>>>objTask'+objTask);
          // }
          // catch(exception e){
          //   System.debug('task exception '+ e);
          // }
        }
      } catch(Exception excGen) {
          blnFlag = false;
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, excGen.getMessage()));
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, excGen.getStackTraceString()));
      }
      //return blnFlag;
      return null;
    }
}