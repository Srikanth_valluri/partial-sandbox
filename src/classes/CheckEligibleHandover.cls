public class CheckEligibleHandover {

     public static CheckEligibleHandover.EligibleHandover getEligibleHandover(String RegistrationId, String processName, String subProcessName, String modeOfRequest,
         String noOfMajorSnagsInApartment, String accessPresent,String utilitiesAvailable,String percAptsSnagged,String EHOCase,
         String DaysToEarliestViewing ) {
         
         EligibleForHandoverUpdated.EligibleForHandoverNoticeRuleHttpSoap11Endpoint calloutObj = 
            new EligibleForHandoverUpdated.EligibleForHandoverNoticeRuleHttpSoap11Endpoint ();
        calloutObj.timeout_x = 120000;
        EligibleHandover resObj = new EligibleHandover ();
        
         try{
            String response = calloutObj.EligibleForHandoverNotice(RegistrationId, processName, subProcessName, modeOfRequest, noOfMajorSnagsInApartment,
                accessPresent, utilitiesAvailable, percAptsSnagged, EHOCase, DaysToEarliestViewing, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''
               );
            resObj = (EligibleHandover)JSON.deserialize(response, CheckEligibleHandover.EligibleHandover.class);
            system.debug('keyhandover response === '+ resObj);
            return resObj;
       } catch (Exception e){
         
       }
      return null;   
     }
     
     public class EligibleHandover {
        public String allowed {get;set;}
        public String message {get;set;}
        public String mortgageNOCfromBank {get;set;}
        public String ifPoaTakingHandoverColatePoaPassportResidence {get;set;}
        public String corporateValidTradeLicence {get;set;}
        public String corporateArticleMemorandumOfAssociation {get;set;}
        public String corporateBoardResolution {get;set;}
        public String corporatePoa {get;set;}
        public String signedForm {get;set;}
        public String clearAndValidPassportCopyOfOwner {get;set;}
        public String clearAndValidPassportCopyOfJointOwner {get;set;}
        public String visaOrEntryStampWithUid {get;set;}
        public String copyofValidEmiratesId {get;set;}
        public String copyofValidGccId {get;set;}
        public String handoverChecklistAndLod {get;set;}
        public String keyReleaseForm {get;set;}
        public String checkOriginalSpaAndtakeCopyOfFirstFourPagesOfSpa {get;set;}
        public String areaVariationAddendum {get;set;}
        public String tempOne {get;set;}
        public String tempTwo {get;set;}
        public String tempThree {get;set;}
        public String handoverNoticeAllowed {get;set;}
        public String approvalQueueOne {get;set;}
        public String approvalQueueTwo {get;set;}
        public String approvalQueueThree {get;set;}
        public String eligibleforRentalPool {get;set;}
    }

}