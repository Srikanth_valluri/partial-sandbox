@isTest
public class DLDAgreementHandler_Test {
    public static testMethod void method1() {
        Account acc = new Account ();
            acc.LastName = 'test';
            acc.Vendor_ID__c = '123456';
            acc.Agency_Type__c = 'Corporate';
            acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        insert acc;
        
        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);        
            sr.Eligible_to_Sell_in_Dubai__c = true;
            sr.Agency_Type__c = 'Individual';
            sr.ID_Type__c = 'Passport';
            sr.Token_Amount_AED__c = 40000;
            sr.RecordTypeId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Change Agent').getRecordTypeId();
            sr.Booking_Wizard_Level__c = null;
            sr.Agency_Email_2__c = 'test2@gmail.com';
            sr.Alternate_Agency_Email_2__c = 'test1@gmail.com';
            sr.Country_of_Sale__c = 'UAE';
            sr.Mode_of_Payment__c = 'Online_Payment';
            sr.agency__c = acc.id;
            sr.Token_Attachment_Name__c = 'Unit Test Attachment';
        insert sr;
        
        Attachment attach=new Attachment();     
            attach.Name='Unit Test Attachment';
            Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
            attach.body = bodyBlob;
            attach.parentId = sr.id;
        insert attach;
        
        List<Booking__c> lstbk = new List<Booking__c>();
            lstbk.add(InitializeSRDataTest.createBooking(sr.id));
            lstbk.add(InitializeSRDataTest.createBooking(sr.id));
            lstbk[0].Deal_SR__c = sr.id;
        insert lstbk;
        
        buyer__c b = new buyer__c();
            b.Buyer_Type__c =  'Individual';
            b.Address_Line_1__c =  'Ad1';
            b.Country__c =  'United Arab Emirates';
            b.City__c = 'Dubai' ;       
            b.Account__c = acc.id;
            b.dob__c = system.today().addyears(-30);
            b.Email__c = 'test@test.com';
            b.First_Name__c = 'firstname' ;
            b.Last_Name__c =  'lastname';
            b.Nationality__c = 'Indian' ;
            b.Passport_Expiry__c = system.today().addyears(20) ;
            b.Passport_Number__c = 'J0565556' ;
            b.Phone__c = '569098767' ;
            b.Phone_Country_Code__c = 'United Arab Emirates: 00971' ;
            b.Place_of_Issue__c =  'India';
            b.Title__c = 'Mr';
            b.booking__c = lstbk[0].id;
            b.Primary_Buyer__c =true;
        insert b;
        
        Location__c loc = InitializeSRDataTest.createLocation('123','Building');
            loc.Property_ID__c = '123';
        insert loc;
        
        Property__c newProperty = new Property__c();
            newProperty.Property_ID__c  = 1;
            newProperty.Property_Code__c    = 'VIR' ;
            newProperty.Property_Name__c    = 'VIRIDIS @ AKOYA OXYGEN' ;
            newProperty.District__c = 'AL YUFRAH 2' ;
            newProperty.AR_Transaction_Type__c  = 'INV VIR' ;
            newProperty.Penalty_Transaction_Type__c = 'DM-PENALTIES-VIR' ;
            newProperty.Brokerage_Distribution_Set__c   = '11600' ;
            newProperty.Sales_Commission_Dist_Set__c    = '11601' ;
            newProperty.Currency_Of_Sale__c = 'AED' ;
            newProperty.Signature_Col_Customer_Stmt__c  = 'Front Line Investment Management Co. LLC' ;
            newProperty.EOI_Enabled__c = true;
        insert newProperty;   
        
        Inventory__c invent = new Inventory__c();
            invent.CM_Price_Per_Sqft__c = 20;
            invent.Inventory_ID__c = '1';
            invent.Building_ID__c = '1';
            invent.Floor_ID__c = '1';
            invent.Marketing_Name__c = 'Damac Heights';
            invent.Address_Id__c = '1' ;
            invent.EOI__C = NULL;
            invent.Tagged_to_EOI__c = false;
            invent.Is_Assigned__c = false;
            invent.Property_ID__c = newProperty.ID;
            invent.Property__c = newProperty.Id;
            invent.Status__c = 'Released';
            invent.special_price__c = 1000000;
            invent.CurrencyISOCode = 'AED';
            invent.Property_ID__c = '1234'; 
            invent.Floor_Package_ID__c = ''; 
            invent.building_location__c = loc.id;
            invent.property_id__c = newProperty.id;
        insert invent;
        
        Booking_Unit__c bu1 = new Booking_Unit__c();
            bu1.Booking__c = lstbk[0].id;
            bu1.Payment_Method__c = 'Cash';
            bu1.Primary_Buyer_s_Email__c = 'test@damac.com';
            bu1.Primary_Buyer_s_Name__c = 'testNSI';
            bu1.Primary_Buyer_s_Nationality__c = 'Russia';
            bu1.Inventory__c = invent.id;
            bu1.Registration_ID__c = '1234';      
        insert bu1;
        
        Payment_Plan__c pp = new Payment_Plan__c();        
            pp.Effective_From__c = system.today();
            pp.Effective_To__c = system.today(); 
            pp.Building_Location__c =  loc.id;  
            pp.term_id__c = '1234'; 
            pp.Booking_Unit__c = bu1.id;  
        insert pp;                     
        Payment_Terms__c pt = new Payment_Terms__c();
            pt.Payment_Plan__c = pp.id;        
            pt.Percent_Value__c = '5';       
        insert pt;
        
        DLD_Agreement__c agreement = New DLD_Agreement__c ();
            agreement.Service_Request__c = sr.id;
            agreement.Booking_Unit__c = bu1.id;
            agreement.Inventory__c = invent.id;
        insert agreement;
        agreement.Agreement_Status__c = 'Reference Id Generated';
        update agreement;
        Test.startTest();        
        DLDAgreementHandler.generateAgreements(sr.id);
        Test.StopTest();
    }
}