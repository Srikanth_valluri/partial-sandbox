public with sharing class DTENController {
    public  Map<String, Object> responseMap;
    public  String pdfresponse {get; set;}
    public void  fetchPDFResponse() {
        responseMap = new  Map<String, Object>();
        HTTPRequest req = new HTTPRequest();
        String url = 'https://ptc.damacgroup.com/COFFEE/public/UK_AVAILABILITY';
        req.setEndPoint(url);
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/pdf');
        HTTP h = new HTTP();
        HTTPResponse resp = h.send(req);
        system.debug('>>>resp>>'+resp.getBody());
        responseMap = (Map<String, Object>)JSON.deserializeUntyped(resp.getBody());
            
        pdfresponse=(String)responseMap.get('responseMessage');
        system.debug('>>pdfresponse>>>'+pdfresponse);

        
    }
}