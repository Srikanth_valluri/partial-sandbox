public class EscalateCLBatch implements Database.Batchable<sObject>,Database.AllowsCallouts{
    map<String, Id> mapRecordTypeId;
    Map<String,Id> mapOfNameToId;
    Map<Id,User> mapOfIdToUser;
    
    public EscalateCLBatch(){
        
        mapRecordTypeId = new map<String, Id>();
        mapOfIdToUser = new Map<Id,User>();
        mapOfNameToId = new Map<String,Id>();
        
        for(RecordType recTypeInst : [select Id, DeveloperName from RecordType
                                      where SobjectType = 'Calling_List__c']) {
                                          mapRecordTypeId.put(recTypeInst.DeveloperName, recTypeInst.Id);
                                      }
        system.debug('mapRecordTypeId==='+mapRecordTypeId);
        
         List<User> lstActiveUser = [Select Id,
                                    Name,
                                    isActive,                                   
                                    UserType,Team_Leader_Name__c,
                                    UserRole.Name,
                                    UserRoleID,
                                    FirstName,
                                    LastName,Email,
                                    ManagerId,
                                    Manager.FirstName,
                                    Manager.ManagerId,
                                    Manager.LastName
                                    from User Where IsPortalEnabled = false AND isActive = true];
          
        for(User thisUser:lstActiveUser) {
            mapOfIdToUser.put(thisUser.Id,thisUser);
            mapOfNameToId.put(thisUser.FirstName+' '+thisUser.LastName,thisUser.Id);
        }
        
        
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){
    List<String> lstRecordTypeString = Label.CLRecordtypes.split(',');
    List<Id> lstRecordTypeIds = new List<Id>();
    for(String thisRecordType : lstRecordTypeString) {
        lstRecordTypeIds.add(mapRecordTypeId.get(thisRecordType));
    }
    System.debug('lstRecordTypeIds-->'+lstRecordTypeIds);
    List<String> lstRecordType = new List<String> {'Collections Calling List','Lease Handover Calling List','Handover Calling List','Early Handover Calling List'};
        return Database.getQueryLocator([Select Id,Name,Account__r.Name,
                                         CRE_Manager__c,
                                         OwnerId,                               
                                         Current_Escl_with__c,
                                         RTP_Escl_Date__c,
                                         Director_Escl_Date__c,
                                         SVP_Escl_Date__c,  
                                         DirectorNextEsclDate__c,
                                         managerNextEsclDate__c,
                                         CreNextEsclDate__c,
                                         Director_Outcome__c,
                                         TL_Manager_Outcome__c,
                                         Call_Outcome__c,
                                         Current_Escl_User_Role__c,Under_Management_Review__c,
                                         Calling_List_Type__c,Promise_To_Pay_Date__c,PTP_Date_Mgr__c
                                         FROM Calling_List__c
                                         WHERE ( RecordTypeId IN:lstRecordTypeIds
                                            AND ForQlik__c = true
                                            AND IsHideFromUI__c= false
                                            AND OwnerId != NULL
                                            AND Calling_List_Type__c != 'DP Calling'
                                            AND (CreNextEsclDate__c = today OR DirectorNextEsclDate__c = today OR managerNextEsclDate__c = today)
                                            AND ((Call_Outcome__c = Null OR Call_Outcome__c = 'Refuse to Pay')OR
                                                (Director_Outcome__c = Null OR Director_Outcome__c = 'Refuse to Pay')OR
                                                (TL_Manager_Outcome__c = Null OR TL_Manager_Outcome__c = 'Refuse to Pay')))
                                            OR( Promise_To_Pay_Date__c = Today AND Call_Outcome__c = 'Promise to Pay' AND RecordType.Name IN: lstRecordType 
                                                AND  Under_Management_Review__c != true AND ForQlik__c = true AND IsHideFromUI__c= false )
                                            OR( PTP_Date_Mgr__c = Today AND Call_Outcome__c = 'Promise to Pay' AND RecordType.Name IN: lstRecordType 
                                                AND ForQlik__c = true AND IsHideFromUI__c= false AND Under_Management_Review__c != true  )
                                        ]); 
    }
    
    public void execute(Database.BatchableContext BC, List<Calling_List__c> scope){        
       System.debug('mapOfIdToUser'+mapOfIdToUser);
       list<Calling_List__c> lstCL = new list<Calling_List__c>();
        for(Calling_List__c thisCalling : scope) {
            system.debug('scope'+scope);
            System.debug('thisCalling.OwnerId'+thisCalling.OwnerId);
            try {
                if(!( mapOfIdToUser.get(thisCalling.Current_Escl_with__c) != NULL && mapOfIdToUser.get(thisCalling.Current_Escl_with__c).Id != NULL &&
                        String.valueOf(mapOfIdToUser.get(thisCalling.Current_Escl_with__c).Id).containsIgnoreCase(Label.Ajay_sID))){
                    
                    if( thisCalling.CreNextEsclDate__c != NULL && 
                        thisCalling.CreNextEsclDate__c == System.today()&& 
                        (thisCalling.Call_Outcome__c == Null || thisCalling.Call_Outcome__c == 'Refuse to Pay')&&
                        thisCalling.Current_Escl_with__c == Null && 
                        mapOfIdToUser.get(thisCalling.OwnerId).ManagerId != NULL ) {
                         System.debug('in 1 if');
                        thisCalling.Current_Escl_with__c = mapOfIdToUser.get(thisCalling.OwnerId).ManagerId;
                        thisCalling.RTP_Escl_Date__c = System.Now();
                        thisCalling.Escalated_Mgr__c = mapOfIdToUser.get(thisCalling.OwnerId).Manager.FirstName+' '+mapOfIdToUser.get(thisCalling.OwnerId).Manager.LastName;
                        thisCalling.Current_Escl_User_Role__c = 'Manager';
                        
                    }else if( thisCalling.managerNextEsclDate__c != NULL && 
                        thisCalling.managerNextEsclDate__c == System.today()&& thisCalling.Current_Escl_with__c != NULL &&
                        (thisCalling.TL_Manager_Outcome__c == Null || thisCalling.TL_Manager_Outcome__c == 'Refuse to Pay') &&
                        mapOfIdToUser.get(thisCalling.OwnerId).ManagerId != Null &&
                        mapOfIdToUser.get(thisCalling.Current_Escl_with__c).ManagerId != NULL &&
                        mapOfIdToUser.get(thisCalling.OwnerId).ManagerId == thisCalling.Current_Escl_with__c) {
                         System.debug('in 2 if');
                        thisCalling.Director_Escl_Date__c = System.Now();
                        thisCalling.Escalated_Director__c = mapOfIdToUser.get(thisCalling.Current_Escl_with__c).Manager.FirstName+' '+mapOfIdToUser.get(thisCalling.Current_Escl_with__c).Manager.LastName;
                        thisCalling.Current_Escl_with__c = mapOfIdToUser.get(thisCalling.Current_Escl_with__c).ManagerId;
                        thisCalling.Current_Escl_User_Role__c = 'Director';
                        
                    }else if(  thisCalling.DirectorNextEsclDate__c != NULL &&
                        thisCalling.DirectorNextEsclDate__c == System.today()&& thisCalling.Current_Escl_with__c != NULL &&
                        (thisCalling.Director_Outcome__c == Null || thisCalling.Director_Outcome__c == 'Refuse to Pay') &&
                        mapOfIdToUser.get(thisCalling.OwnerId).Manager.ManagerId != Null &&
                        mapOfIdToUser.get(thisCalling.Current_Escl_with__c).ManagerId != Null && 
                        mapOfIdToUser.get(thisCalling.OwnerId).Manager.ManagerId == thisCalling.Current_Escl_with__c ) {
                        System.debug('in 3 if');
                        thisCalling.SVP_Escl_Date__c =System.Now();
                        thisCalling.Escalated_SVP__c = mapOfIdToUser.get(thisCalling.Current_Escl_with__c).Manager.FirstName+' '+mapOfIdToUser.get(thisCalling.Current_Escl_with__c).Manager.LastName;
                        thisCalling.Current_Escl_with__c = mapOfIdToUser.get(thisCalling.Current_Escl_with__c).ManagerId;
                    }
                }
                System.debug('thisCalling.Promise_To_Pay_Date__c'+thisCalling.Promise_To_Pay_Date__c );
                System.debug('thisCalling.Call_Outcome__c'+thisCalling.Call_Outcome__c);
                if(thisCalling.Promise_To_Pay_Date__c == System.Today() &&
                    thisCalling.Call_Outcome__c == 'Promise to Pay'){
                    Id TLId = mapOfNameToId.get(mapOfIdToUser.get(thisCalling.OwnerId).Team_Leader_Name__c);
                    if(TLId != NULL){
                        sendEmail( mapOfIdToUser.get(TLId),thisCalling );
                    }else{
                        thisCalling.Under_Management_Review__c = true;
                        sendEmail( mapOfIdToUser.get(mapOfIdToUser.get(thisCalling.OwnerId).ManagerId),thisCalling );
                    }
                }   
                
                if(Date.ValueOf(thisCalling.PTP_Date_Mgr__c) == System.Today() &&
                    thisCalling.Call_Outcome__c == 'Promise to Pay'){
                    Id mangerId = mapOfIdToUser.get(thisCalling.OwnerId).ManagerId;
                    if(mangerId != NULL){
                        sendEmail( mapOfIdToUser.get(mangerId),thisCalling );
                        thisCalling.Under_Management_Review__c = true;
                        
                    }
                        
                }
            }catch (Exception e) {
                 System.debug('Exception'+e);
                // Handle this exception here
            }
        }
        update scope;
            
        
    }
    public void finish(Database.BatchableContext BC){
    }
    
    public static void sendEmail( User ObjUser,Calling_List__c objCL ) {
        List<Attachment> lstAttach = new List<Attachment>();
        List<EmailMessage> lstEmails = new List<EmailMessage>();
        

        EmailTemplate emailTemplateObj = [SELECT Id,Name
                                                , Subject
                                                , Body
                                                , HtmlValue
                                                , TemplateType
                                                , BrandTemplateId
                                                FROM EmailTemplate 
                                                WHERE Name = 'PTP_mail_mgr_TL' LIMIT 1 ];
                                                
        // TODO Added to have attachment related to email template

        Attachment[] attachmentLst = [ SELECT Id,ContentType,Name, Body  From Attachment where parentId = : emailTemplateObj.Id];
        lstAttach.addAll(  attachmentLst );

        String toAddress = '', fromAddress,replyToAddress = '', contentType, contentValue,contentBody = '';
        String subject, strAccountId, strDPIId, bccAddress ='', strCCAddress;
        
        //strAccountId = objCase.AccountId;

        
        toAddress = (ObjUser != NULL && String.isNotBlank(ObjUser.Email))? ObjUser.Email:
                     '';
        
        System.debug('toAddress = ' + toAddress);
        // need to verify 
        fromAddress = 'noreply@damacgroup.com';
        bccAddress = Label.Sf_Copy_Bcc_Mail_Id;
        contentType = 'text/html';
        strCCAddress = '';
        system.debug(' emailTemplateObj : '+ emailTemplateObj );
        if( toAddress != '' && emailTemplateObj != null ) {
            
            if(emailTemplateObj.body != NULL){
                contentBody =  emailTemplateObj.body;
            }
            if(emailTemplateObj.htmlValue != NULL){
                contentValue = emailTemplateObj.htmlValue;
            }
            if(string.isblank(contentValue)){
                contentValue='No HTML Body';
            }
            subject = emailTemplateObj.Subject != NULL ? emailTemplateObj.Subject : 'Social Media Escalation';
           //Message__c,CaseNumber,FirstName LastName Name,Account__r.Name
            
            contentValue = String.isNotBlank(objCL.Name)?contentValue.replace('{CLname}', objCL.Name): contentValue.replace('{CLname}', '');
            contentValue = String.isNotBlank(objCL.Account__r.Name)?contentValue.replace('{CXName}', objCL.Account__r.Name):contentValue.replace('{CXName}', '');
            
               
            contentBody = String.isNotBlank(objCL.Name)?contentBody.replace('{CLname}', objCL.Name):contentBody.replace('{CLname}', '');
            contentBody = String.isNotBlank(objCL.Account__r.Name)?contentBody.replace('{CXName}', objCL.Account__r.Name):contentBody.replace('{CXName}', '');
            
            
                // Callout to sendgrid to send an email
                SendGridEmailService.SendGridResponse objSendGridResponse =
                    SendGridEmailService.sendEmailService(
                        toAddress
                        , ''
                        , strCCAddress
                        , ''
                        , bccAddress
                        , ''
                        , subject
                        , ''
                        , fromAddress
                        , ''
                        , replyToAddress
                        , ''
                        , contentType
                        , contentValue
                        , ''
                        , lstAttach
                    );

                System.debug('objSendGridResponse == ' + objSendGridResponse);
                //Create Email Activity
                String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;
                if (responseStatus == 'Accepted') {
                }//End Email Activity Creation
                
        }//End Payment details null check if
    }//End emailTemplateObj IF
}