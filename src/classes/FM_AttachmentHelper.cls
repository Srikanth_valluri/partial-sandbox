/*-------------------------------------------------------------------------------------------------
Description: Attachement trigger helper
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 23-01-2019       | Lochana Rajput   | 1. Added logic to handle FM NOC attachments created from drawloop
----------------------------------------------------------------------------------------------------------------------------
1.1     | 28-01-2019       | Lochana Rajput   | 1. Added logic to replace FM NOC attachments
----------------------------------------------------------------------------------------------------------------------------
1.2     | 09-04-2019       | Lochan Karle     | 1. Added logic to handle FM Notice & BR attachments created from drawloop
                                                   related to the Booking Units
=============================================================================================================================
*/
public without sharing class FM_AttachmentHelper {
    
    public static void createFM_SR_Attachment(List<Attachment> newlstAttachment) {
        String keyPrefixFMCase = FM_Case__c.sobjecttype.getDescribe().getKeyPrefix();
        String keyPrefixBU = Booking_unit__c.sobjecttype.getDescribe().getKeyPrefix();
        Set<Id> setAttachmentIds = new Set<Id>();
        Set<Id> setRecordIds = new Set<Id>();
        system.debug('keyPrefixFMCase*****'+keyPrefixFMCase);
        system.debug('keyPrefixBU*****'+keyPrefixBU);
        system.debug('newlstAttachment*****'+newlstAttachment[0].ParentId);
        //Need to add check for NOC Document
        for(Attachment att : newlstAttachment) {
            system.debug('att.name'+att.name);
            system.debug('ala if madhe'+label.Drawloop_Document_Generation_FM_cases.contains(att.name));
            if((string.valueOf(att.ParentId).left(3).equalsIgnoreCase(keyPrefixFMCase) || string.valueOf(att.ParentId).left(3).equalsIgnoreCase(keyPrefixBU))
                && label.Drawloop_Document_Generation_FM_cases.contains(att.name)) {
                setRecordIds.add(att.ParentId);
                setAttachmentIds.add(att.Id);
            }
        }//for
        system.debug('setRecordIds*****'+setRecordIds);
        system.debug('setAttachmentIds*****'+setAttachmentIds);
        if(setRecordIds.size() > 0) {
            uploadDocumentCentralRepository(setAttachmentIds,setRecordIds);
        }
    }

    @future(callout=true)
    public static void uploadDocumentCentralRepository(Set<Id> setAttachmentIds, Set<Id> setRecordIds) {
        String keyPrefixFMCase = FM_Case__c.sobjecttype.getDescribe().getKeyPrefix();
        String keyPrefixBU = Booking_unit__c.sobjecttype.getDescribe().getKeyPrefix();
        List<FM_Case__c> lstCases = new List<FM_Case__c>();
        Map<Id,SR_Attachments__c> FMCaseSRAttachmentMap = new Map<Id,SR_Attachments__c>();
        list<SR_Attachments__c> lstSRAttachments = new list<SR_Attachments__c>();
        list<SR_Attachments__c> lstSRAttachmentsCase = new list<SR_Attachments__c>();
        list<SR_Attachments__c> lstSRAttachmentsBU = new list<SR_Attachments__c>();
        string srAttachmentName = Label.Drawloop_Document_Generation_FM_cases;
        List<String> lstSrAttachmentName = srAttachmentName.split(',');
        System.debug('lstSrAttachmentName'+lstSrAttachmentName);
        Set<Id> setCaseIds = new set<Id>();
        Set<Id> setBUIds = new set<Id>();
        for(Id objId: setRecordIds){
            if(String.valueOf(objId).startsWith(keyPrefixFMCase)) {
                setCaseIds.add(objId);
            } else if(String.valueOf(objId).startsWith(keyPrefixBU)) {
                setBUIds.add(objId);
            }           
        }
        System.debug('setCaseIds'+setCaseIds);
        System.debug('setBUIds'+setBUIds);

        if(setCaseIds.size() > 0){
            lstSRAttachmentsCase = [SELECT Id, FM_Case__c, name, Booking_Unit__c
                                    FROM SR_Attachments__c
                                    WHERE FM_Case__c IN : setCaseIds
                                    AND name IN: lstSrAttachmentName];
        } else if(setBUIds.size() > 0) {
            lstSRAttachmentsBU = [SELECT Id, FM_Case__c, name, Booking_Unit__c
                                    FROM SR_Attachments__c
                                    WHERE booking_unit__c IN : setBUIds
                                    AND name IN: lstSrAttachmentName];
        }
        if(!lstSRAttachmentsCase.isEmpty()) {
            lstSRAttachments.addAll(lstSRAttachmentsCase);
        } else if(!lstSRAttachmentsBU.isEmpty()){
            lstSRAttachments.addAll(lstSRAttachmentsBU);
        }

        system.debug('lstSRAttachments===='+lstSRAttachments);

        for (SR_Attachments__c objSR: lstSRAttachments) {
            if(label.Drawloop_Document_Generation_FM_cases.contains(objSR.name)){
                system.debug('yaaayy naav match zal====');
                if(String.isNotBlank(objSR.FM_Case__c)){
                    FMCaseSRAttachmentMap.put(objSR.FM_Case__c,objSR);
                } else if(String.isNotBlank(objSR.Booking_Unit__c)){
                    FMCaseSRAttachmentMap.put(objSR.Booking_Unit__c,objSR);
                }                    
            }
        }
        system.debug('FMCaseSRAttachmentMap===='+FMCaseSRAttachmentMap);
        List<UploadMultipleDocController.MultipleDocRequest> lstReq = new List<UploadMultipleDocController.MultipleDocRequest>();
        //Get attachment body
        List<Attachment> lstAttachments = new List<Attachment>();
        lstAttachments = [SELECT Id,Body,Parent.Name,ParentId,Name
                            FROM Attachment
                            WHERE Id IN : setAttachmentIds
                        ORDER BY CreatedDate DESC LIMIT 1];
        system.debug('lstAttachments*****'+lstAttachments);
        system.debug('setAttachmentIds*****'+setAttachmentIds);

        List<Booking_Unit__c> lstBookingUnitIds = new List<Booking_Unit__c>();
        lstBookingUnitIds = [SELECT Id ,Property_Name_Inventory__c,Unit_Name__c,Value__c,Registration_Id__c,
                                                            Customer_Name__c,Key_Release_Date__c,FM_Email__c,Loams_Email__c
                                FROM Booking_Unit__c 
                                WHERE Id IN: setBUIds];
        Map<Id,string> BURegIdMap = new Map<Id,string>();
        for(Booking_Unit__c objBU: lstBookingUnitIds){
            BURegIdMap.put(objBU.Id,objBU.Registration_Id__c);
        }
        system.debug('BURegIdMap*****'+BURegIdMap);

        Map<Attachment,string> attachmentRegIdMap = new Map<Attachment,string>();
        
        for(Attachment att : lstAttachments) {
            if(String.valueOf(att.ParentId).startsWith(keyPrefixFMCase)) {
                attachmentRegIdMap.put(att,att.Parent.name);
            } else if(String.valueOf(att.ParentId).startsWith(keyPrefixBU)){
                attachmentRegIdMap.put(att,BURegIdMap.get(att.ParentId));
            }
        }
        system.debug('attachmentRegIdMap*****'+attachmentRegIdMap);
        Integer counter=1;
        List<DocWrapper> lstDocWrapper = new List<DocWrapper>();
        // List<SR_Attachments__c> lstSRAttachments = new List<SR_Attachments__c>();
        // for(FM_Case__c objFMCase : [SELECT Name,Account__r.Party_ID__c,
        //                          (SELECT Name FROM Attachments
        //                          WHERE ID IN: setAttachmentIds)
        //                          FROM FM_Case__c where ID IN: setRecordIds]) {
            // for(Attachment att : objFMCase.Attachments) {
            for(Attachment att : attachmentRegIdMap.keyset()) {
                system.debug('att.ParentId===='+att.ParentId);
                SR_Attachments__c objAtt = new SR_Attachments__c();
                if(FMCaseSRAttachmentMap.containsKey(att.ParentId)) {
                    system.debug('att.ParentId====ala if madhe');
                    system.debug('existing srAttach'+FMCaseSRAttachmentMap.get(att.ParentId));
                    objAtt = FMCaseSRAttachmentMap.get(att.ParentId);
                } else {
                    system.debug('att.ParentId====ala else madhe');
                    objAtt.Name = att.Name;
                    objAtt.Type__c = att.Name.substringBeforeLast('.');
                    if(String.valueOf(att.ParentId).startsWith(keyPrefixFMCase)) {
                        objAtt.FM_Case__c = att.ParentId;
                    } else if(String.valueOf(att.ParentId).startsWith(keyPrefixBU)){
                        objAtt.Booking_Unit__c = att.ParentId;
                    }                        
                }
                system.debug('objAtt====ala'+objAtt);
                // lstSRAttachments.add(objAtt);
                String strFileExtension = '';
                if(att.Name.lastIndexOf('.') != -1 ) {
                    strFileExtension = att.Name.substring( att.Name.lastIndexOf('.')+1 , att.Name.length() );
                }
                String strTime = String.valueOf(System.currentTimeMillis());
                lstDocWrapper.add(new DocWrapper(att.Parent.Name,counter,strTime,objAtt));
                UploadMultipleDocController.MultipleDocRequest reqObj = new UploadMultipleDocController.MultipleDocRequest();
                reqObj.category = 'Document';
                reqObj.entityName = 'Damac Service Requests';
                reqObj.base64Binary =  EncodingUtil.Base64Encode(att.Body);
                reqObj.fileDescription = att.Name;
                // reqObj.fileId = objFMCase.Name + '-' + strTime + '-' + counter + '-' + strFileExtension ;
                reqObj.fileId = 'IPMS-' + att.Parent.Name + '-' + strTime + counter+'-'+strFileExtension;
                reqObj.fileName = reqObj.sourceId = reqObj.sourceFileName = reqObj.fileId;
                /*if(String.valueOf(att.ParentId).startsWith(keyPrefixFMCase)) {
                    reqObj.registrationId = att.Parent.Name;
                } else if(String.valueOf(att.ParentId).startsWith(keyPrefixBU)){
                    reqObj.registrationId = att.Parent.Name;
                }*/
                reqObj.registrationId = attachmentRegIdMap.get(att);
                system.debug('maza reqObj========='+reqObj);
                lstReq.add(reqObj);
                counter++;
            }//for
        // }//for

        //Sending document upload request
        if(lstReq.size() > 0) {
            UploadMultipleDocController.data respObj = new UploadMultipleDocController.data();
            respObj= UploadMultipleDocController.getMultipleDocUrl(lstReq);
            System.debug('==respObj======'+respObj);
            if(respObj != NULL) {
                if(respObj.status != 'Exception'){
                    if(respObj.Data != null && respObj.Data.size() >0){
                        Set<SR_Attachments__c> setSRAttachments_Valid = new Set<SR_Attachments__c>();
                    for(DocWrapper objWrapper : lstDocWrapper) {

                        for(UploadMultipleDocController.MultipleDocResponse objData : respObj.data) {
                            System.debug('===objData.PARAM_ID==' + objData.PARAM_ID);
                            System.debug('=objWrapper.caseNumber====' + objWrapper.caseNumber);
                            System.debug('=objWrapper.strTime====' + objWrapper.strTime);
                            String strFileExtension = '';
                            if(objWrapper.objSR.Name.lastIndexOf('.') != -1 ) {
                                strFileExtension = objWrapper.objSR.Name.substring( objWrapper.objSR.Name.lastIndexOf('.')+1 , objWrapper.objSR.Name.length() );
                            }
                            if('IPMS-'+objWrapper.caseNumber+'-'+objWrapper.strTime+objWrapper.counter + '-' + strFileExtension
                                == objData.PARAM_ID){
                                if(objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url)){
                                    objWrapper.objSR.Attachment_URL__c = objData.url;
                                    setSRAttachments_Valid.add(objWrapper.objSR);
                                }
                                else{
                                    System.debug('======'+objData.PARAM_ID);
                                }
                            }
                        }
                    }//outer for
                    List<SR_Attachments__c> lstAtt = new List<SR_Attachments__c>();
                    lstAtt.addAll(setSRAttachments_Valid);
                    upsert lstAtt;
                    System.debug('==INSERT ===='+lstAtt);
                    }
                }
            }
        }//if
        list<attachment> lstAttachmentToDelete = new list<attachment>();
        lstAttachmentToDelete = [SELECT id, Parent.Name, name
                                    FROM Attachment
                                    WHERE parentId IN : setRecordIds
                                    AND name IN: lstSrAttachmentName
                                    ORDER BY CreatedDate DESC];
        system.debug('lstAttachmentToDelete====='+lstAttachmentToDelete);
        if(lstAttachmentToDelete.size() > 1) {
            system.debug('lstAttachmentToDelete[1]'+lstAttachmentToDelete[1]);
            delete lstAttachmentToDelete[1];
        }

    }//uploadDocumentCentralRepository

    public class DocWrapper {
        Integer counter;
        String strTime;
        String caseNumber;
        SR_Attachments__c objSR;
        public DocWrapper(String caseNumber, Integer counter,String strTime, SR_Attachments__c objSR) {
            this.caseNumber = caseNumber;
            this.counter = counter;
            this.strTime = strTime;
            this.objSR = objSR;
        }
    }
}