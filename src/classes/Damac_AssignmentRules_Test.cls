@isTest
public class Damac_AssignmentRules_Test{

    public static testmethod void testMethodRules (){
        
        user u=[select name from user where id=:userinfo.getUserId()];
        system.runAs(u){
            Group grp = new Group();
            grp.name = 'Test Group1';
            grp.Type = 'Regular'; 
            Insert grp; 
            system.debug('222'+grp.id);
            //Create Group Member
            GroupMember grpMem1 = new GroupMember();
            grpMem1.UserOrGroupId = UserInfo.getUserId();
            grpMem1.GroupId = grp.Id;
            Insert grpMem1;
            
            LIST<Inquiry_Assignment_Rules__c> rules = new LIST<Inquiry_Assignment_Rules__c> ();
            Inquiry_Assignment_Rules__c ua = new Inquiry_Assignment_Rules__c ();
            ua.Is_Campaign_Active__c = 'NO';
            ua.Active__C = true;
            ua.Execute_on__c = 'Create';
            ua.ownerid = grp.Id;
            ua.Tenure__c = '>1 week';
            rules.add(ua);  
            Inquiry_Assignment_Rules__c rule1 = new Inquiry_Assignment_Rules__c ();
            rule1.Tenure__c = '<1 week';
            rule1.Active__c = true;
            rule1.Execute_on__c = 'Create';
            rule1.Queue_Assignment__c =true;
            rule1.Fields_to_Apply_Rule__c = 'Class__c';
            rule1.Class__c = 'Priority Digital';
            rules.add(rule1);
            insert rules;
        }
        
        Damac_AssignmentRules obj = new Damac_AssignmentRules ();
        obj.getRuleOptions ();
        obj.selectedCriteria = 'Create';
        obj.displayRules ();
    }
}