/*
Name:DealExceptionRequest_SearchController
Description: Controller to Provide the released Units of a User and Un assigned Inventory
Author: DAMAC IT Team
*/

public class DealExceptionRequest_SearchController{
        public transient list<string> idsselected{get;set;}
        public transient list<string> idsInvSelected{get;set;}
        public transient String idString{get;set;}
        public transient list<string> propertyNameFilter{get;set;}
        public transient list<string> bedRoomFilter{get;set;}
        public transient list<string> inventoryRecsId{get;set;}
        public list<Marketing_Documents__c> marketingNames {get;set;}
        public transient list<inventory__c> filteredinventory {get;set;}
        public transient set<string> marketingNamesFiltered{get;set;}
        public String untFtrsToFilterStr{get;set;}
        @testvisible private list<Inventory_User__c> otherUserinventory;
        @testvisible private set<id> otherInventoryIds;
        @testvisible private string status='Released';
        public string invs{get;set;}
        public boolean ismobile{get;set;}
        public List<String> invStatuses {get;set;}
        //UA and EOI CR
        public string selectedInventoryIdsForBooking {get;set;}
        public string selectedProductProjectname{get; set;}
        public Map<string, set<String>> mpProperties{get; set;}
        public Map<string, List<Inventory__c>> mpAvailable{get; set;}
        public Map<string, Map<string, List<Inventory__c>>> mpFinal{get; set;}
        public Map<string, Location__c> mpBuildings {get; set;}
        public Map<String, List<Payment_Plan__c>> mpPaymentPlans{get; set;}
        public Map<Id, List<Payment_Terms__c>> mpPaymentTerms{get; set;}
        public string planKeyStr{get; set;}
        public boolean render{get;set;}
        public integer offsetval {get;set;}
        public integer recNum {get;set;}
        public integer limits {get;set;}
        public integer limitVal {get;set;}
        public String searchText {get;set;}
        public String searchTextVal {get;set;}
        public String sortOrderVal {get;set;}
        public String sortColVal {get;set;}
        public Integer totalRecs {get;set;}
        public Integer startIndex {get;set;}
        public Integer endIndex {get;set;}
        public Id currentUser {get;set;}
        public string selInventories {get;set;}
        public Boolean hasUnit {get;set;}
        Set<String> projID = new Set<String>();
        Set<String> marktID = new Set<String>();
        
        public Map<String, Map<String, List <String>>> unitIdResponceMap {get;set;}
        public List<String> selectedIdsList {get;set;}
        public List<String> untFtrsToFilterList;
        public List<Inventory_User__c> thisUserInventory;

        public DealExceptionRequest_SearchController(){
            hasUnit = false;
            offsetval = 0;
            recNum = 0;
            limits =10;
            currentUser = userinfo.getuserid();
            invStatuses = new List<String>();
            String customLabelValue = System.Label.Deal_Exception_Inventory_Statuses;
            for(String invStatus: customLabelValue.split(',')){
                invStatuses.add(invStatus.trim());
            }
            pageload();
            
        }

        public void isS1(){
            ismobile = false;
        }

        /****************************************************************************************
        Method:         getRecCount
        Description:    Get record count of avaliable inventory 
        **************************************************************************************************/
        public Integer getRecCount(String pageLoadParam){
            if(pageLoadParam == 'pageLoadCall'){
               offsetval = 0;
               limits = 10;
            }
            untFtrsToFilterStr = ' ';
            List<inventory__c> inventoryRecs = new List<inventory__c>();
            startIndex = (recNum != 0 ? offsetval +1 : offsetval);
            endIndex = (limits < recNum ? offsetval + limits : offsetval + recNum);
            String returnString = createQueryString(idsInvSelected, idsselected, untFtrsToFilterStr,
                                                    propertyNameFilter, bedRoomFilter, searchTextVal);

            String queryStr = 'select id,Marketing_Name_Doc__r.id,property_name__c from inventory__c ';
            queryStr += returnString;
            //queryStr += ' Marketing_name__c != NULL AND ';
            queryStr += '  ID NOT IN: otherInventoryIds AND ';
            queryStr += ' status__c IN: invStatuses  LIMIT 10000';
            inventoryRecs = database.query(queryStr);
            inventoryRecsId = new List<String>();
            for(inventory__c invObj : inventoryRecs){
                inventoryRecsId.add(String.valueOf(invObj.id));
            }
            idString = string.join(inventoryRecsId,',');
            system.debug('>>>>idString>>>>>'+idString);
            return inventoryRecs.size();
        }

        /****************************************************************************************
        Method:         filterData
        Description:    filter avaliablity as per user applied filters
        **************************************************************************************************/
        public PageReference filterData() {
            offsetval = 0;
            recNum = 0;
            limits =10;
            searchTextVal = '';
            isS1();
            system.debug('>>>idsselectedFilter>>>>'+idsselected);                                                           
            string queryString = 'select id,Special_Price_Tax_Amount__c,Selling_Price_excl_VAT__c ,Unit_Assignment__r.UA_Type__c,'
                                + 'Tagged_To_Unit_Assignment__c,UP_Label__c,FP_Label__c,PP_Label__c,Custom_Unit_Plan__c,'
                                + 'Custom_Floor_Plan__c,Custom_Plot_Plan__c,Use_Custom_Plot_Plan__c,Use_Custom_Floor_Plan__c,'
                                + 'Use_Custom_Unit_Plan__c,Area__c ,View_Type__c,Brand__c,Rera_Percentage__c,Parking__c,'
                                + 'Floor_Package_Name__c ,Plot_Area__c ,Building_Name__c,Building_ID__c,Location_Code__c ,'
                                + 'IPMS_Bedrooms__c ,Bedrooms__c ,Area_sft__c ,Price_Per_Sqft__c ,Property_Country__c ,'
                                + 'Plot_Area_sft__c ,Status__c ,List_Price_calc__c ,Floor_Plan__c,Plot_Plan__c,'
                                + 'Special_Price_calc__c ,Unit_Plan__c,name,Marketing_name__c,Building_Location__r.Name,'
                                + 'Anticipated_Completion_Date__c,property_name__c,Area_Sqft_2__c,';
                  queryString += 'Property_Status__c,Location_Type__c,Special_Price__c,Currency_of_Sale__c,Property_city__c,'
                                  + 'Unit_Type__c,Unit_Location__r.Name,Bedroom_Type__c,Marketing_Name_Doc__r.Marketing_Plan_Label__c,'
                                  + 'Marketing_Name_Doc__r.Marketing_Plan__c,Marketing_Name_Doc__r.Primary_Image__c, '
                                  + 'Property__r.Property_Plan__c,Property__r.Property_Plan_Label__c ';
                   queryString += 'from inventory__c ';
            String returnString = createQueryString(idsInvSelected, idsselected, untFtrsToFilterStr,
                                                    propertyNameFilter, bedRoomFilter, searchTextVal);
            queryString += returnString;
            system.debug('>>>queryStringFilter>>>>'+queryString);
            //queryString += ' Marketing_name__c != NULL AND '
            queryString += ' ID NOT IN: otherInventoryIds AND  ';
            queryString += ' status__c IN: invStatuses  '+ 'ORDER BY Special_Price__c LIMIT :limits OFFSET: offsetval';// LIMIT 10000';
            filteredinventory = database.query(querystring);
            System.debug('@@@queryfilteredinventory>>>>>'+ filteredinventory.size());
            marketingNamesFiltered = new set<string>();
            recNum = filteredinventory.size();
            // Get Record Count
            totalRecs = getRecCount('noPageLoadCall');
            return null;
        }
        set<string> unitss = new set<string>();
        set<string> propertyNames = new set<string>();
        set<string> bedrooms= new set<string>();
        set<string> district =new set<String>();
        Map<String,id> productname_idmap = new Map<String,Id>();

        /****************************************************************************************
        Method     :    dataPagination
        Description:    filter avaliablity as per user applied filters
        *****************************************************************************************/
        public void dataPagination(){
            string queryString  = 'Select id,Special_Price_Tax_Amount__c,Selling_Price_excl_VAT__c,UP_Label__c,FP_Label__c,PP_Label__c,'
                                + 'Custom_Unit_Plan__c,Custom_Floor_Plan__c,Custom_Plot_Plan__c,Use_Custom_Plot_Plan__c,'
                                + 'Use_Custom_Floor_Plan__c,Use_Custom_Unit_Plan__c,Area__c ,View_Type__c,Brand__c,Rera_Percentage__c,'
                                + 'Parking__c,Floor_Package_Name__c ,Plot_Area__c ,Building_Name__c,Building_ID__c,Location_Code__c ,'
                                + 'IPMS_Bedrooms__c ,Bedrooms__c ,Area_sft__c ,Price_Per_Sqft__c ,Property_Country__c ,Plot_Area_sft__c ,'
                                + 'Status__c ,List_Price_calc__c ,Floor_Plan__c,Plot_Plan__c,Special_Price_calc__c ,Unit_Plan__c,name,'
                                + 'Marketing_name__c,Building_Location__r.Name,Anticipated_Completion_Date__c,property_name__c,Area_Sqft_2__c,'
                                + 'Property_Status__c,Location_Type__c,Special_Price__c,Currency_of_Sale__c,Property_city__c,'
                                + 'Unit_Type__c,Unit_Location__r.Name,Bedroom_Type__c,Marketing_Name_Doc__r.Marketing_Plan_Label__c,'
                                + 'Marketing_Name_Doc__r.Marketing_Plan__c,Marketing_Name_Doc__r.Primary_Image__c, '
                                + 'Property__r.Property_Plan__c,Property__r.Property_Plan_Label__c '
                                + 'from inventory__c ';

            String returnString = createQueryString(idsInvSelected, idsselected, untFtrsToFilterStr, propertyNameFilter, bedRoomFilter, searchTextVal);
            queryString += returnString;
            
            String sortParam;
            // Sorting====
            if(sortColVal == 'none' || sortColVal == NULL){
                queryString += ' Marketing_Name__c != NULL AND ID NOT IN: otherInventoryIds '
                            + '  AND status__c=:status LIMIT :limits OFFSET: offsetval'; 
            }
            else{
                if(sortColVal == 'Product'){
                    sortParam = 'Marketing_Name__c '+sortOrderVal+'';
                }
                else if(sortColVal == 'Project'){
                    sortParam = 'Property_Name__c '+sortOrderVal+'';
                }
                else if(sortColVal == 'Units'){
                    sortParam = 'Unit_Location__r.Name '+sortOrderVal+'';
                }
                else if(sortColVal == 'Bedroom_Type'){
                    sortParam = 'Bedroom_Type__c '+sortOrderVal+'';
                }
                else if(sortColVal == 'Bedrooms'){
                    sortParam = 'IPMS_Bedrooms__c '+sortOrderVal+'';
                }
                queryString += ' Marketing_Name__c != NULL AND ID NOT IN: otherInventoryIds AND status__c=:status AND IS_aSSIGNED__C = FALSE ORDER BY '
                                +sortParam+' LIMIT :limits OFFSET: offsetval';
            }
            filteredinventory = database.query(querystring);
            recNum = filteredinventory.size();
            totalRecs = getRecCount('noPageLoadCall');
            System.debug('@@@ data pagination called @@@');
        }

        /****************************************************************************************
        Method:         pageload
        Description:    On Page load show avaliable inventory 
        **************************************************************************************************/
        public void pageload(){
            isS1();
            mpProperties = new Map<String,set<String>>();
            mpAvailable = new Map<String, list<Inventory__c>>();
            mpFinal = new Map<string, Map<string, List<Inventory__c>>>();
            mpBuildings = new Map<String,Location__c>();
            mpPaymentTerms = new Map<Id, List<Payment_Terms__c>>();
            mpPaymentPlans = new Map<String, List<Payment_Plan__c>>();
            planKeyStr = '';
            render = true;
            otherUserinventory = new list<Inventory_User__c>();
            thisUserInventory = new list<Inventory_User__c>();

            thisUserInventory = [select id,inventory__c,Campaign__r.name,Campaign__c 
                                 from inventory_user__c 
                                 where user__c=:userinfo.getuserid()  and inventory__r.status__c =: status 
                                         and inventory__r.Tagged_To_Unit_Assignment__c = false];
                                         //and inventory__r.Tagged_To_Unit_Assignment__c = false
            otherUserinventory = [select id,inventory__c from inventory_user__c 
                                    where User__c!=:userinfo.getuserid() and inventory__r.Tagged_To_Unit_Assignment__c = false 
                                            and inventory__r.status__c =: status];

            set<id> thisPcinvIds = new set<id>();
            if(thisUserInventory.size() >0){
                for(inventory_user__c thisPcInv:thisUserInventory){
                    thisPcinvIds.add(thisPcInv.inventory__c);
                }
            }
            otherInventoryIds = new set<id>();
            for(inventory_user__c invUs:otherUserinventory){
                if(thisPcinvIds.size()>0){
                    if(!thisPcinvIds.contains(invUs.inventory__c)){
                        otherInventoryIds.add(invUs.inventory__c);
                    }
                } else{
                    otherInventoryIds.add(invUs.inventory__c);
                }
            }
            
            idsselected = new list<string>();
            idsInvSelected = new List<String>();
            propertyNameFilter  = new list<string>();
            filteredinventory = new list<inventory__c>();
            marketingNames = new list<Marketing_Documents__c>();
            bedRoomFilter =  new list<string>();
            string queryString = '';

            queryString =   'select id,Selling_Price_excl_VAT__c,Special_Price_Tax_Amount__c,UP_Label__c,FP_Label__c,'
                        + 'PP_Label__c,Custom_Unit_Plan__c,Custom_Floor_Plan__c,Custom_Plot_Plan__c,Use_Custom_Plot_Plan__c,'
                        + 'Use_Custom_Floor_Plan__c,Use_Custom_Unit_Plan__c,Area__c ,View_Type__c,Brand__c,Rera_Percentage__c,'
                        + 'Parking__c,Floor_Package_Name__c ,Plot_Area__c ,Building_Name__c,Building_ID__c,Location_Code__c ,'
                        + 'IPMS_Bedrooms__c ,Bedrooms__c ,Property_Country__c ,Area_sft__c ,Price_Per_Sqft__c ,Plot_Area_sft__c ,'
                        + 'Status__c ,Floor_Plan__c,List_Price_calc__c ,Plot_Plan__c,Unit_Plan__c,Special_Price_calc__c ,name,'
                        + 'Marketing_name__c,Building_Location__r.Name,Anticipated_Completion_Date__c,property_name__c,Area_Sqft_2__c,District__c,';
            queryString +=  'Property_Status__c,Location_Type__c,Special_Price__c,Currency_of_Sale__c,Property_city__c,Unit_Type__c,'
                        + 'Unit_Location__r.Name,Bedroom_Type__c,Marketing_Name_Doc__r.Marketing_Plan_Label__c,'
                        + 'Marketing_Name_Doc__r.Marketing_Plan__c,Marketing_Name_Doc__r.Primary_Image__c, '
                        + 'Property__r.Property_Plan__c,Property__r.Property_Plan_Label__c'; 
            queryString +=   ' from inventory__c where Marketing_Name__c != NULL AND ';
            
            queryString += ' status__c=:status AND IS_aSSIGNED__C = FALSE  and ID NOT IN: otherInventoryIds LIMIT :limits OFFSET: offsetval';
            
            filteredinventory =  database.query(queryString);
            recNum = filteredinventory.size();

            list<inventory__c> invS = new list<inventory__c>();
            invS  = [select id,Floor_Package_Name__c,Selling_Price_excl_VAT__c ,property_city__c,Bedroom_Type__c,
                        Property_Status__c,Unit_Type__c,Unit__c,property_name__c,Anticipated_Completion_Date__c,
                        View_Type__c,District__c,Marketing_Name_Doc__r.name,Marketing_Name_Doc__c 
                    from inventory__c where status__c =: status  ORDER BY ACD_Date__c asc LIMIT 10000 ];

            if(invS.size() > 0){
                for(inventory__c inv:invs){
                    if(inv.Marketing_Name_Doc__c != null){
                        productname_idmap.put(inv.Marketing_Name_Doc__r.name,inv.Marketing_Name_Doc__c);  
                    }
                    if(inv.Unit__c != null){
                        unitss.add(inv.Unit__c.toUpperCase());
                    }
                    if(inv.property_name__c != null){
                        propertyNames.add(inv.property_name__c.toUpperCase());
                    }
                    
                    if(inv.Bedroom_Type__c !=null){
                        bedrooms.add(inv.Bedroom_Type__c );
                    }  
                }
            }
            totalRecs = getRecCount('pageLoadCall');
        }


/* ************************************************** POPULATE FILTERS **********************************************************************/        
        // MARKETING NAME
        public List<SelectOption> getItems() {
            List<SelectOption> options = new List<SelectOption>();
            List<String> productnamelst = new List<String>();
            for(String productname : productname_idmap.keyset()){
                productnamelst.add(productname);
            }
            productnamelst.sort();
            for(String name: productnamelst){
                options.add(new SelectOption(productname_idmap.get(name),name));
            }
            return options;
        }
        
        //Units unitss 
        public List<SelectOption> getUnits() {
            system.debug('Inside Unit Type');
            List<SelectOption> options = new List<SelectOption>();
            for(string u:unitss){
                //system.debug('Unit Type==>>>'+u);
                options.add(new SelectOption(u,u));
            }
            return options;
        }

        //PROPERTY NAMES
        public List<SelectOption> getPropertyNames() {
            List<SelectOption> options = new List<SelectOption>();
            List<String> propertynamessort = new List<String>();
            propertynamessort.addall(propertyNames);
            propertynamessort.sort();
            for(string u:propertynamessort){
                options.add(new SelectOption(u,u));
            }
            return options;
        }

        // Bed rooms
        public List<SelectOption> getBedRooms() {
            List<SelectOption> options = new List<SelectOption>();
            List<String> bedroomsort = new List<String>();
            bedroomsort.addall(bedrooms);
            bedroomsort.sort();
            for(string u:bedroomsort){
                options.add(new SelectOption(u,u));
            }
            return options;
        }

       /***********************************************************************************************************************/      
       public void nextbtn(){
            offsetval += limits ;
            dataPagination();
        }

        public void prvbtn(){
            if(offsetval >= limits){
                offsetval -= limits ;
            } else{
                offsetval -= offsetval ;
            }
            dataPagination();
        }
        
        public void changeRecCountFunMethod(){
            limits = integer.valueof((Apexpages.currentPage().getParameters().get('limitVal')));
            dataPagination();
        }

        public void changeSortOrder(){
            sortOrderVal = Apexpages.currentPage().getParameters().get('sortOrder');
            sortColVal = Apexpages.currentPage().getParameters().get('sortCol');
            dataPagination();
        }

       public void searchRecs(){
            offsetval = 0;
            searchTextVal = Apexpages.currentPage().getParameters().get('searchText');
            dataPagination();
        }

        public boolean getprv(){
            if(offsetval == 0){
                return true;
            } else{
                return false;
            }
        }

        public boolean getnxt(){
            if(recNum < limits){
                return true;
            } else{
                return false;
            }
        }

    // JS Remoting action called when searching for a Units name
    @RemoteAction
    public static List<inventory__c> searchUnits (String searchTerm) {
        list<Inventory_User__c> otherUserinvUnit = new list<Inventory_User__c>();
        list<Inventory_User__c> thisUserInvUnit = new list<Inventory_User__c>();
        List<String> invStatuses = new List<String>();
        String customLabelValue = System.Label.Deal_Exception_Inventory_Statuses;
        for(String invStatus: customLabelValue.split(',')){
            invStatuses.add(invStatus.trim());
        }
        thisUserInvUnit = [select id,inventory__c from inventory_user__c where user__c=:userinfo.getuserid() 
                            and inventory__r.Tagged_To_Unit_Assignment__c = false and inventory__r.status__c IN: invStatuses];
        otherUserinvUnit = [select id,inventory__c from inventory_user__c where User__c !=: userinfo.getuserid() 
                            and inventory__r.Tagged_To_Unit_Assignment__c = false and inventory__r.status__c IN: invStatuses];

        set<id> thisPcinvIdsUnit = new set<id>();
        if(thisUserInvUnit.size() >0){
            for(inventory_user__c thisPcInv:thisUserInvUnit){
                thisPcinvIdsUnit.add(thisPcInv.inventory__c);
            }
        }

        Set<id> otherInventoryIdsUnit = new set<id>();
        for(inventory_user__c invUs: otherUserinvUnit){
            if(thisPcinvIdsUnit.size()>0){
                if(!thisPcinvIdsUnit.contains(invUs.inventory__c)){
                    otherInventoryIdsUnit.add(invUs.inventory__c);
                }
            } else{
                otherInventoryIdsUnit.add(invUs.inventory__c);
            }
        }
        system.debug('invStatuses: ' + invStatuses);
        system.debug('otherInventoryIdsUnit: ' + otherInventoryIdsUnit);
        system.debug('searchTerm: ' + searchTerm);
        String unitQuery ='Select Id,Unit__c From inventory__c where Unit__c like \'%' 
                            + String.escapeSingleQuotes(searchTerm) 
                            + '%\' AND Status__c IN: invStatuses  '
                            // + ' AND ID NOT IN: otherInventoryIdsUnit ' 
                             + 'LIMIT 20';
        List<inventory__c> unitsFtr = Database.query(unitQuery);
        system.debug('unitQuery: ' + unitQuery);
        system.debug('unitsFtr: ' + unitsFtr);
        return unitsFtr;
    }

    @remoteAction 
    public Static string checkSelectedUnits (String selectedInvIds, String EOIID) {
        String response = '';
        if (eoiId != '' && eoiId != NULL) {
            list<String> idHolder = selectedInvIds.split(',');
            List <String> eoiInventories = new list<String>();
            for (Inventory__c inv: [select building_location__c, Unit__c from Inventory__c where EOI__c =: EOIID]) {
                eoiInventories.add (inv.ID);
            }
            System.Debug (idHolder);
            System.Debug (eoiInventories);
            for (String val :idHolder) {
                if (val.contains (',')) {
                    val = val.replace (',', '');
                }
                if (val != '') {
                    if (!eoiInventories.contains (val)) {
                        response = 'Please select Inventories related to EOI ';
                        break;
                    }
                }
            }
        }
        return response;
        
        
    }

    public String createQueryString(List<string> idsInvSelected, List<string> idsselected, string untFtrsToFilterStr,
                                    List<string> propertyNameFilter, List<string> bedRoomFilter, String searchTextVal){
        String queryString;
        system.debug('>>>idsselectedFiltercreateQueryString>>>>'+idsselected);
        
        untFtrsToFilterList = new List<String>();
        if(idsInvSelected != null || idsselected !=null || untFtrsToFilterStr != '' || propertyNameFilter !=null ||  bedRoomFilter != null ){
                if(String.isNotBlank(untFtrsToFilterStr)){
                    untFtrsToFilterList = untFtrsToFilterStr.split(',');
                }
                queryString = 'where ';
                if(idsInvSelected != null && idsInvSelected.size() > 0 ){
                    queryString += 'ID IN: idsInvSelected AND';
                }
                if(idsselected != null && idsselected.size() > 0 ){
                    queryString += 'Marketing_Name_Doc__r.id IN: idsselected AND';
                }
               
                if(propertyNameFilter != null && propertyNameFilter.size()>0){
                    queryString += ' property_name__c IN: propertyNameFilter AND';
                }
               
                if(bedRoomFilter!= null && bedRoomFilter.size()>0){
                     queryString += ' Bedroom_Type__c IN: bedRoomFilter AND';
                }
                 //Unit Features
                if(untFtrsToFilterList != null && untFtrsToFilterList.size()>0){
                    queryString += ' Unit__c IN: untFtrsToFilterList AND';
                }

            }
            
          //  queryString += ' Status__c = \'Released\' AND ';
            
            if(searchTextVal  != '' && searchTextVal != null){
                queryString += ' (Property_Name__c LIKE \'%' +searchTextVal
                     +'%\' OR Unit_Location__r.Name LIKE \'%'+searchTextVal
                     +'%\' OR IPMS_Bedrooms__c LIKE \'%'+searchTextVal+'%\') AND ';
             }
        return queryString;
    }

    /**
     * v1.1 Craig  - Share Project Details
     * Method to Search Inquiry
     */
    @RemoteAction 
    public static List<Inquiry__c> getInquiries(String pSearchKey) {
        String searchString = '%'
                            + pSearchKey 
                            + '%';
        List<Inquiry__c> inquiryList = new List<Inquiry__c>();
        inquiryList = [ SELECT Id, Name, First_name__c, Last_Name__c, Email__c 
                          FROM Inquiry__c 
                         WHERE OwnerId = :UserInfo.getUserId()
                           AND ( First_Name__c LIKE :searchString 
                                OR Last_Name__c LIKE :searchString 
                                OR Name LIKE :searchString 
                               )
        ]; 
        System.debug('inquiryList:::: '+inquiryList);
        return inquiryList;
    }

    /**
     * v1.1 Craig  - Share Project Details
     * Method to Search Account
     */
    @RemoteAction 
    public static List<Account> getAccounts(String pSearchKey) {
        String searchString = '%'
                            + pSearchKey 
                            + '%';
        List<Account> accountList = new List<Account>();
        accountList = [ SELECT Id, Name, Email__c
                          FROM Account 
                         WHERE OwnerId = :UserInfo.getUserId()
                           AND Name LIKE :searchString 
        ]; 
        System.debug('accountList:::: '+accountList);
        return accountList;
    }
}