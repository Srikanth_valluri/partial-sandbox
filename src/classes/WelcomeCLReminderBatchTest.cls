@isTest
private class WelcomeCLReminderBatchTest {
    @TestSetup
    static void createTestData(){
        Id welcomeClRecTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Welcome Calling List').getRecordTypeId();
        
        List<TriggerOnOffCustomSetting__c>settingLst = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                    OnOffCheck__c = true);
                                                                                    insert newSetting1;
        // create Account
        Account objAcc = new Account(Name = 'test', Party_Id__c = '43434');
        insert objAcc;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        objBookingUnit.DOS_Name__c = 'Admin CRM';
        objBookingUnit.HOS_Name__c = 'Admin CRM';
        objBookingUnit.HOD_Name__c = 'Admin CRM';
        insert objBookingUnit;

        Calling_List__c objCL = new Calling_List__c();
        objCL.Booking_Unit__c = objBookingUnit.Id;
        objCL.PC_Email__c = 'test@tst.com';
        objCL.Welcome_Call_Outcome__c = 'Invalid Email';
        objCL.Reminder_Date__c = System.today().addDays( -1 );
        objCL.RecordTypeId = welcomeClRecTypeId;
        insert objCL;
    }

    @isTest
    private static void testTestCase() {
        // Test data setup

        // Actual test
        Test.startTest();
        Database.executeBatch( new WelcomeCLReminderBatch() );
        Test.stopTest();

        // Asserts
    }
}