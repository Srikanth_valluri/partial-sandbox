@isTest
public class SR_OwnerPassortRESTresourceTest {
    static testMethod void  test1(){
        Id personAcc_rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account acc1 = new Account( LastName = 'Test Account1',
                                       Party_ID__c = '63066662',
                                       RecordtypeId = personAcc_rtId,
                                       Email__pc = 'test1@mailinator.com');
        insert acc1;
        Account acc2 = new Account( LastName = 'Test Account2',
                                       Party_ID__c = '63066663',
                                       RecordtypeId = personAcc_rtId,
                                       Email__pc = 'test2@mailinator.com');
        insert acc2;
        
        Id passDetailChgCase_rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Passport Detail Update').getRecordTypeId();
        Case testPassDetChgCase = new Case();
        testPassDetChgCase.recordTypeId = passDetailChgCase_rtId;
        testPassDetChgCase.AccountId = acc2.id;
        testPassDetChgCase.type = 'Passport Detail Update SR';
        testPassDetChgCase.SR_Type__c = 'Passport Detail Update SR';
        testPassDetChgCase.Origin = 'Portal';
        testPassDetChgCase.status = 'Draft Request';
        testPassDetChgCase.New_CR__c = 'PL0098RT98765';
        testPassDetChgCase.Passport_Issue_Date__c = Date.today();
        testPassDetChgCase.Passport_Issue_Place__c = 'document_issue_place';
        testPassDetChgCase.Passport_File_URL__c = 'passport_file_url';
        testPassDetChgCase.Additional_Doc_File_URL__c = 'additional_doc_file_url';
        insert testPassDetChgCase;
        
        Case srCase = [SELECT id, caseNumber, AccountId FROM Case WHERE AccountId = :acc2.id LIMIT 1];
        
        String baseUrl = URL.getOrgDomainUrl().toExternalForm(); /* URL.getSalesforceBaseUrl() */
        
        Test.startTest();
        
        RestRequest request = new RestRequest();
        request.requestUri = baseUrl + '/services/apexrest/srCase/owner/passport';
        request.httpMethod = 'GET';
        
        RestResponse response = new RestResponse();
		
        RestContext.request = request;
        RestContext.response = response;
        
        SR_OwnerPassortRESTresource.doGet(); /* 1 */
        
        request.addParameter('case_number', 'invalid');
        SR_OwnerPassortRESTresource.doGet(); /* 2 */
        
        request.addParameter('account_id', String.valueOf(srCase.id));
        SR_OwnerPassortRESTresource.doGet(); /* 3 */
        
        request.addParameter('account_id', String.valueOf(acc2.id));
        SR_OwnerPassortRESTresource.doGet(); /* 4 */
        
        request.addParameter('case_number', String.valueOf(srCase.caseNumber));
        SR_OwnerPassortRESTresource.doGet(); /* 5 */
        
        SR_OwnerPassortRESTresource.doDeleteDraft(); /* 6 */
        
        Test.stopTest();
    }
    
    static testMethod void  test2(){
        Id personAcc_rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account acc1 = new Account( LastName = 'Test Account1',
                                       Party_ID__c = '63066662',
                                       RecordtypeId = personAcc_rtId,
                                       Email__pc = 'test1@mailinator.com');
        insert acc1;
        Account acc2 = new Account( LastName = 'Test Account2',
                                       Party_ID__c = '63066663',
                                       RecordtypeId = personAcc_rtId,
                                       Email__pc = 'test2@mailinator.com');
        acc2.Primary_Language__c = 'Arabic';
        insert acc2;
        
        Id passDetailChgCase_rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Passport Detail Update').getRecordTypeId();
        Case testPassDetChgCase = new Case();
        testPassDetChgCase.recordTypeId = passDetailChgCase_rtId;
        testPassDetChgCase.AccountId = acc2.id;
        testPassDetChgCase.type = 'Passport Detail Update SR';
        testPassDetChgCase.SR_Type__c = 'Passport Detail Update SR';
        testPassDetChgCase.Origin = 'Portal';
        testPassDetChgCase.status = 'Draft Request';
        testPassDetChgCase.New_CR__c = 'PL0098RT98765';
        testPassDetChgCase.Passport_Issue_Date__c = Date.today();
        testPassDetChgCase.Passport_Issue_Place__c = 'document_issue_place';
        testPassDetChgCase.Passport_File_URL__c = 'passport_file_url';
        testPassDetChgCase.Additional_Doc_File_URL__c = 'additional_doc_file_url';
        insert testPassDetChgCase;
        
        
        Map<String, Object> requestMap_post = new Map<String, Object>();
        requestMap_post.put('is_draft', true);
        requestMap_post.put('account_id', String.valueOf(acc1.id));
        requestMap_post.put('draft_sr_id', '');
        requestMap_post.put('document_number', 'TESTpass98765');
        requestMap_post.put('document_expiry_date_string', '2029-12-30');
        requestMap_post.put('document_issue_place', 'Dubai');
        requestMap_post.put('passport_file_url', 'https://testcdn/docs/6758765465');
        requestMap_post.put('additional_doc_file_url', 'https://testcdn/docs/6758765465');
        
        String reqJSON = JSON.serialize(requestMap_post);
        String baseUrl = URL.getOrgDomainUrl().toExternalForm(); /* URL.getSalesforceBaseUrl() */
        
        Test.startTest();
        
        RestRequest request = new RestRequest();
        request.requestUri = baseUrl + '/services/apexrest/srCase/owner/passport';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(reqJSON);
        
        RestResponse response = new RestResponse();
		
        RestContext.request = request;
        RestContext.response = response;
        
        callPostMethod(requestMap_post); /* 1 */
        
        requestMap_post.put('account_id', '');
        callPostMethod(requestMap_post); /* 2 */
        
        requestMap_post.put('account_id', String.valueOf(acc1.id));
        requestMap_post.put('document_number', '');
        callPostMethod(requestMap_post); /* 3 */
        
        requestMap_post.put('document_number', 'TEST987654321');
        requestMap_post.put('document_expiry_date_string', '12/31/2029');
        callPostMethod(requestMap_post); /* 4 */
        
        requestMap_post.put('document_expiry_date_string', '2029-12-30');
        requestMap_post.put('account_id', String.valueOf(testPassDetChgCase.id));
        callPostMethod(requestMap_post); /* 5 */
        
        requestMap_post.put('document_expiry_date_string', '2029-12-30');
        requestMap_post.put('account_id', String.valueOf(acc1.id));
        callPostMethod(requestMap_post); /* 6 */
        
        requestMap_post.put('account_id', String.valueOf(acc2.id));
        callPostMethod(requestMap_post); /* 7 */
        
        requestMap_post.put('is_draft', false);
        requestMap_post.put('draft_sr_id', String.valueOf(testPassDetChgCase.id));
        requestMap_post.put('passport_file_url', '');
        callPostMethod(requestMap_post); /* 8 */
        
        requestMap_post.put('passport_file_url', 'https://testcdn/docs/6758765465');
        callPostMethod(requestMap_post); /* 9 */
        
        Test.stopTest();
    }
    
    private static void callPostMethod(Map<String, Object> requestMap_post){
        SR_OwnerPassortRESTresource.doPost(
        (Boolean)requestMap_post.get('is_draft'), 
        (String)requestMap_post.get('account_id'), 
        (String)requestMap_post.get('draft_sr_id'), 
        (String)requestMap_post.get('document_number'), 
        (String)requestMap_post.get('document_expiry_date_string'), 
        (String)requestMap_post.get('document_issue_place'), 
        (String)requestMap_post.get('passport_file_url'), 
        (String)requestMap_post.get('additional_doc_file_url')
        );
    }
}