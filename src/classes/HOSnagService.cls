public class HOSnagService {

    public locationWrapper getLocation(String bookingUnitName, String URL) {
        SnagWSDL wsdlObj = new SnagWSDL();
        SnagWSDL.SnagrwebHttpSoap11Endpoint snagObj = new SnagWSDL.SnagrwebHttpSoap11Endpoint();
        String responseStr = snagObj.LocationExtRef(bookingUnitName, URL);
        System.debug('==responseStr===' + responseStr); 
        String resp = responseStr.substringAfter('[').substringBeforeLast(']');
        locationWrapper locWrap;
        if (!String.isBlank(resp)) {
            locWrap = (locationWrapper)JSON.deserialize(resp, locationWrapper.class);
            System.debug('==locWrap===' + locWrap);
        }
        return locWrap;
    }
    
     public List<SnagRResponseWrapper> getDefects(String locationId, String URL) {
        SnagWSDL wsdlObj = new SnagWSDL();
        SnagWSDL.SnagrwebHttpSoap11Endpoint snagObj = new SnagWSDL.SnagrwebHttpSoap11Endpoint();
        String responseStr = snagObj.defects(locationId, URL);
        System.debug('==responseStr===' + responseStr);
        List<SnagRResponseWrapper> lstSnagR = new List<SnagRResponseWrapper>();
        lstSnagR = (List<SnagRResponseWrapper>)JSON.deserialize(responseStr, List<SnagRResponseWrapper>.class);
        System.debug('==lstSnagR.size===' + lstSnagR.size());
        //System.debug('==lstSnagR===' + lstSnagR[0]);
        return lstSnagR;
    }

     public class locationWrapper {
         public String LocID;
         public String DrwgID;
         public String Xcoord;
         public String Ycoord;
         public String Location;
         public String Datestamp;
         public String Lat;
         public String Lng;
         public String LocGUID;
         public String DateSync;
         public String ExtRef;
         public String Closed;
         public String RFDI;
         public String LocationTypeID;
     }
     
     public class SnagRResponseWrapper {
        public String ID;
        public String GUID;
        public String DrawingID;
        public String DrawingTitle;
        public String Status;
        public String CurrentStatus;
        public String UserDescription;
        public String LocationID;
        public String Location;
        public String LocationExtRef;
        public String LocationGUID;
        public String XCoord;
        public String YCoord;
        public String PriorityID;
        public String Priority;
        public String DueBy;
        public String DaysLeft;
        public String GroupID;
        public String GroupName;
        public String GroupInitials;
        public String DateClosed;
        public String BigPhoto;
        public String Lattitude;
        public String Longitude;
        public String CreatedByID;
        public String CreatedBy;
        public String CreatedDate;
        public String SDCategoryID;
        public String SDCategory;
        public String SDCode;
        public String SDType;
        public String SDID;
        public String SDDescription;
        public String DateSync;
        public String MPGUID;
        public String FCGUID;
        public String FutureDefectGUID;
        public String PreviousDefectGUID;
        public String ProjectTitle;
        public String ContractNo;
        public String Role;
        public String UGUserID;
        public String HistoryCount;
        public String LastChanged;
        public String PhotoExists;
    }
}