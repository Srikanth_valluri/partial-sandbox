/* * * * * * * * * * * * * *
*  Class Name:   AdditionalParkingTaskHandlerTest
*  Purpose:      Unit test class for AdditionalParkingTaskHandler Controller
*  Author:       Hardik Mehta - ESPL
*  Company:      ESPL
*  Created Date: 29-Dec-2017
*  Updated Date: 29-Dec-2017
*  Type:         Test Class
* * * * * * * * * * * * */
@isTest
public with sharing class AdditionalParkingTaskHandlerTest 
{

  public static final String strBookingUnitActiveStatus = 'Agreement executed by DAMAC';

  /* * * * * * * * * * * * *
  *  Method Name:  createTaskForArchival_test
  *  Purpose:      This method is used to unit test createTaskForArchival method
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 29-Dec-2017
  * * * * * * * * * * * * */
  static testMethod void createTaskForArchival_test()
  {
    List<Booking__c> bookingList = new List<Booking__c>();
    List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
    List<SR_Booking_Unit__c> srBookingUnitList = new List<SR_Booking_Unit__c>();

    //insert Booking Unit Active Status custom setting record
    insertBookingUnitActiveStatus();

    //insert sample data for Account
    Account objAccount = TestDataFactory_CRM.createPersonAccount();
    insert objAccount;

    Id parkingRecordTypeID = getRecordTypeIdForParking();

    Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
    String orgId = userInfo.getOrganizationId();
    String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
    Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
    String uniqueName = orgId + dateString + randomInt;
    User tuser = new User(  firstname = 'testFName',
                            lastName = 'testLName',
                            email = uniqueName + '@test' + orgId + '.org',
                            Username = uniqueName + '@test' + orgId + '.org',
                            EmailEncodingKey = 'ISO-8859-1',
                            Alias = uniqueName.substring(18, 23),
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            LanguageLocaleKey = 'en_US',
                            ProfileId = p.Id
                         );
    //insert tuser;
    System.runAs(tuser) 
    {
      Task objArchivalTask = new Task();
      Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , parkingRecordTypeID);
    
      objCase.Status = 'Submitted';
      objCase.POA_Expiry_Date__c = System.today();
      objCase.Approval_Status__c = 'Approved';
      objCase.Status = 'Submitted';
      objCase.IsPOA__c = true;
      objCase.Parking_Details_JSON__c = '[{"strInstallment":"1- INSTALLMENT","strDate":"14/12/2017","strAmount":"3333.33"},{"strInstallment":"2- INSTALLMENT","strDate":"15/12/2017","strAmount":"3333.33"},{"strInstallment":"3- INSTALLMENT","strDate":"16/12/2017","strAmount":"3333.33"}]';
      objCase.Pending_Amount__c = 500;
      objCase.Parking_Type_Of_Payment__c = 'Full Payment';
      objCase.Parking_Total_Payment_Terms__c = 3;
      objCase.Parking_Payment_Mode__c = 'Cash Payment';
      insert objCase;

      objCase = [Select OwnerId FROM Case WHERE Id =:objCase.Id];
      
      objArchivalTask.ActivityDate = System.today() +1;
      objArchivalTask.Assigned_User__c = 'CDC';
      objArchivalTask.CurrencyIsoCode = 'AED';
      objArchivalTask.OwnerId = objCase.OwnerId;
      objArchivalTask.Priority = 'High';
      objArchivalTask.Process_Name__c = 'Parking';
      objArchivalTask.Status = 'Not Started';
      objArchivalTask.Subject = 'Archival Task For CRE';
      objArchivalTask.WhatId = objCase.Id;
      insert objArchivalTask;

      //create Deal SR record
      NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
      insert objDealSR;

      //create Booking record for above created Deal and Account
      bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
      insert bookingList;

      //create Property data
      Property__c objProperty = new Property__c();
      objProperty.Name = 'Test Project';
      objProperty.Property_Name__c = 'Test Property';
      objProperty.Property_ID__c = 3431;
      objProperty.CurrencyIsoCode = 'AED';
      insert objProperty;

      //create data for Inventory
      Inventory__c objInventory = new Inventory__c();
      objInventory.Property__c = objProperty.Id;
      objInventory.Unit__c = '1345';
      insert objInventory;

      //create Booking Units record for above created Bookings
      bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
      for(Booking_Unit__c objBookingUnit : bookingUnitList)
      {
        objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
        objBookingUnit.Inventory__c = objInventory.Id;
        objBookingUnit.Registration_ID__c = '74712';
        objBookingUnit.Re_Assigned__c = false;
      }
      insert bookingUnitList;

      srBookingUnitList = TestDataFactory_CRM.createSRBookingUnis(objCase.Id , bookingUnitList);
      insert srBookingUnitList;

      Test.startTest();

        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );
        AdditionalParkingTaskHandler.createTaskForArchival(objArchivalTask);
      Test.stopTest();
    }
  }

  /* * * * * * * * * * * * *
  *  Method Name:  getRecordTypeIdForParking
  *  Purpose:      This method is used to get record type ID for Parking
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 29-Dec-2017
  * * * * * * * * * * * * */
  private static Id getRecordTypeIdForParking()
  {
    Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
    Id parkingRecordTypeID = caseRecordTypes.get('Parking').getRecordTypeId();
    return parkingRecordTypeID;
  }

  /* * * * * * * * * * * * *
  *  Method Name:  insertBookingUnitActiveStatus
  *  Purpose:      This method is used to insert custom setting record for Booking Unit Active Status.
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 29-Dec-2017
  * * * * * * * * * * * * */
  private static void insertBookingUnitActiveStatus()
  {
    List<Booking_Unit_Active_Status__c> buActiveStatusList = new List<Booking_Unit_Active_Status__c>();

    Booking_Unit_Active_Status__c objBUActiveStatus = new Booking_Unit_Active_Status__c();
    objBUActiveStatus.Name = strBookingUnitActiveStatus;
    objBUActiveStatus.Status_Value__c = strBookingUnitActiveStatus;
    buActiveStatusList.add(objBUActiveStatus);

    insert buActiveStatusList;
  }
}