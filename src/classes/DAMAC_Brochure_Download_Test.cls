/*****************************************************************************************************************
* Name              : DAMAC_Brochure_Download_Test
* Description       : Test Class for DAMAC_Brochure_Download   
* -------------------------------------------------------------------------------------------------------------
* VERSION       AUTHOR          DATE            COMMENTS
* 1.0            QBurst      09/03/2020          Created
*****************************************************************************************************************/

@isTest
public class DAMAC_Brochure_Download_Test {

  @isTest
    static void testSuccess(){
        Email_Request__c req = new Email_Request__c ();
        insert req;

        Email_Request_Mappings__c emailReq = new Email_Request_Mappings__c();
        emailReq.Name = 'Download Brochure';
        emailReq.Email_Request_Id__c = req.Id;
        insert emailReq;

        Campaign__c thisCampaign = InitialiseTestData.createCampaign();
        insert thisCampaign;

        List<Website_Subscriber_Field_Mappings__c> mappingList 
                        = new List<Website_Subscriber_Field_Mappings__c>();        
        Website_Subscriber_Field_Mappings__c mapping1 = new Website_Subscriber_Field_Mappings__c();
        mapping1.Name = 'r1_title';
        mapping1.Field_API__c = 'R1_Title__c';
        mapping1.Field_Type__c = 'Text';
        mappingList.add(mapping1);
        Website_Subscriber_Field_Mappings__c mapping2 = new Website_Subscriber_Field_Mappings__c();
        mapping2.Name = 'campaign_id';
        mapping2.Field_API__c = 'Campaign_Id__c';
        mapping2.Field_Type__c = 'Text';
        mapping2.Lookup_Field_API__c = 'Marketing_Campaign__c';
        mapping2.Lookup_Object_API__c = 'Campaign__c';
        mappingList.add(mapping2);
        Website_Subscriber_Field_Mappings__c mapping3 = new Website_Subscriber_Field_Mappings__c();
        mapping3.Name = 'Opted Out Date';
        mapping3.Field_Type__c = 'DateTime';
        mapping3.Field_API__c = 'Opted_Out_Date__c';
        mappingList.add(mapping3);
        insert mappingList;

        String myJSON = '{ '
                        + ' "Opt In Details": "",'
                        + ' "Email Type": "HTML",'
                        + ' "Email Status" : "Opted In",'
                        + ' "Last Modified": "2008-02-01 01:29:00",'
                        + ' "Opted Out Date": "",'
                        + ' "CRM Lead Source": "",'
                        + ' "Opt Out Details": "",'
                        + ' "Opted Out Date": "2008-02-02 01:29:00",'
                        + ' "Opt In Date": "2008-02-01 01:29:00",'
                        + ' "CREATED_FROM": "",'
                        + ' "header_img_path": "test image path 1",'
                        + ' "Name": "",'
                        + ' "Phone": "",'
                        + ' "r1_link" : "test r1 link",'
                        + ' "r1_title": "Download Brochure",'
                        + ' "r2_link": "",'
                        + ' "r2_title": "",'
                        + ' "r3_link": "",'
                        + ' "r3_title": "",'
                        + ' "source_type": "project",'
                        + ' "Stand": "",'
                        + ' "STO": "0",'
                        + ' "zero_bounce_status" : "Valid",'
                        + ' "email": "varun.ullattil@gmail.com",'
                        + ' "campaign_id" : "' + thisCampaign.Id + '",'
                        + ' "page_title" : "Expect the exceptional",'
                        + ' "ia_brochure" : "",'
                        + ' "utm_campaign" : "",'
                        + ' "utm_medium" : "",'
                        + ' "utm_source" : ""'
                        + ' }';
        
        Test.startTest();
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/brochuredownload';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json'); 
        request.requestBody = Blob.valueof(myJSON);
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Brochure_Download.createWebsiteSubscriber();
        Test.stopTest();
    }
    
    @isTest
    static void testEmailMissing(){
        String myJSON = '{"wrongparam": "testemailfortestclass@test.com"}';
        Test.startTest();
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/brochuredownload';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json'); 
        request.requestBody = Blob.valueof(myJSON);
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Brochure_Download.createWebsiteSubscriber();
        Test.stopTest();
    }
}