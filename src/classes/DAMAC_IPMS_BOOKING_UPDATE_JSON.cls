/**************************************************************************************************
 * @Name              : DAMAC_IPMS_BOOKING_UPDATE_JSON
 * @Test Class Name   : DAMAC_IPMS_PARTY_CREATION_TEST
 * @Description       : JSON Class for DAMAC_IPMS_BOOKING_UPDATE_JSON
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0                        14/08/2019       Created
**************************************************************************************************/
public class DAMAC_IPMS_BOOKING_UPDATE_JSON{

    public String responseId;
    public String responseTime;
    public String requestName;
    public String status;
    public String responseMessage;
    public Integer elapsedTimeMs;
    public Boolean complete;

    public static DAMAC_IPMS_BOOKING_UPDATE_JSON parse(String json) {
        return (DAMAC_IPMS_BOOKING_UPDATE_JSON) System.JSON.deserialize(json, DAMAC_IPMS_BOOKING_UPDATE_JSON.class);
    }
}