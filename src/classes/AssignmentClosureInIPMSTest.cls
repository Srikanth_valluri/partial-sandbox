@isTest (SeeAllData = false)
private class AssignmentClosureInIPMSTest{
    
    private static testMethod void AssignmentClosure(){
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Mobile_Phone_Encrypt__c = '9674858963';
        objAcc.Mobile_Country_Code__c = 'India: 0091';
        objAcc.Mobile_Encrypt__c = '9674858963';
        insert objAcc ;
        system.debug('objAcc=='+objAcc);

        Id recTypeAssignment = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(null, recTypeAssignment);
        objCase.Status = 'Closed';
        insert objCase;
        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        AssignmentClosureInIPMS.closeCaseInIPMS(new list<Id>{objCase.Id});
        
        Error_Log__c objE = AssignmentClosureInIPMS.createErrorLogRecord(objAcc.Id, objCase.Booking_Unit__c, objCase.Id);
        AssignmentClosureInIPMS.insertErrorLog(new list<Error_Log__c>{objE});
        test.StopTest();
    }
    
    private static testMethod void RPAssignmentClosure(){
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Mobile_Phone_Encrypt__c = '9674858963';
        objAcc.Mobile_Country_Code__c = 'India: 0091';
        objAcc.Mobile_Encrypt__c = '9674858963';
        insert objAcc ;
        system.debug('objAcc=='+objAcc);

        Id recTypeAssignment = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Assignment').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(null, recTypeAssignment);
        objCase.Status = 'Closed';
        insert objCase;
        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        AssignmentClosureInIPMS.closeCaseInIPMS(new list<Id>{objCase.Id});
        test.StopTest();
    }
}