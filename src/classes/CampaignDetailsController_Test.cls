@isTest
public class CampaignDetailsController_Test {
	static testmethod void m1(){    
     	Id RSRecordTypeId = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Roadshows').getRecordTypeId();
        Campaign__c camp = new Campaign__c();
        camp.RecordTypeId = RSRecordTypeId;
        camp.Campaign_Name__c = 'Test Campaign';
        camp.Campaign_Description__c = 'Test Campaign';
        camp.start_date__c = System.today();
        camp.end_date__c = System.Today().addDays(30);
        camp.Marketing_start_date__c = System.today();
        camp.Marketing_end_date__c = System.Today().addDays(30);
        insert camp;
        
        Id agenTeamRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Agent Team').getRecordTypeId();
        Inquiry__c inqObj= new Inquiry__c(Campaign__c = camp.Id, Is_Meeting_Scheduled__c= true, Meeting_Due_Date__c = Date.today (), 
                                          RecordTypeId=agenTeamRT,
                                          Inquiry_Source__c='Agent Referral',
                                          Mobile_Phone_Encrypt__c='456123',
                                          Mobile_CountryCode__c='American Samoa: 001684',
                                          Mobile_Phone__c='1234',Email__c='mk@gmail.com',
                                          First_Name__c='Test',Last_Name__c='Last',
                                          CR_Number__c='0987',ORN_Number__c='7842',
                                          Agency_Type__c='Corporate',Organisation_Name__c = 'Oliver',
                                          isDuplicate__c=false);
        insert inqObj; 
        
        CampaignDetailsController.getDetails(inqObj.id);
    }
}