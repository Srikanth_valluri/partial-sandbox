/*********************************************************************************************************
* Name               : selectUnitToCommunicateController
* Test Class         : 
* Description        : Controller class for selectUnitToCommunicate VF Page
* Created Date       : 09/11/2020
* -----------------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst         19/11/2020      Initial Draft.
**********************************************************************************************************/
global With Sharing class selectUnitToCommunicateController {
    public String unitId {get; set;}
    public string invDetails {get;set;}
    public Inventory__c inv {get;set;}
    public string mailBody {get;set;}
     /*********************************************************************************************
    * @Description : Constructor method
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public selectUnitToCommunicateController() {
        unitId = ApexPages.currentPage().getParameters().get('Id');
        if(unitId != null && unitId != ''){
           	GetUnitDetails(unitId);
           }
    }
    public void GetUnitDetails(string invId){
        string status,name,comments,building,pName,unitType ='';
        inv = [SELECT Unit_Name__c,Status__c,Unit_Type__c,Area__c,Property_Name_2__c,Property__c,
                            Comments__c,Building_Name__c FROM Inventory__c WHERE id =:invId];
        invDetails = JSON.serialize(inv);
        system.debug('invDetails'+inv.Comments__c);
       if(inv.Status__c == null || inv.Status__c ==''){
            status= '-';
        }else{
            status = inv.Status__c;
        }
       if(inv.Unit_Name__c == null || inv.Unit_Name__c ==''){
            name= '-';
        }else{
            name = inv.Unit_Name__c;
        }
       if(inv.Unit_Type__c == null || inv.Unit_Type__c ==''){
            unitType= '-';
        }else{
            unitType = inv.Unit_Type__c;
        }
        if(inv.Property_Name_2__c == null || inv.Property_Name_2__c ==''){
            pName= '-';
        }else{
            pName = inv.Property_Name_2__c;
        }
      if(inv.Building_Name__c == null || inv.Building_Name__c ==''){
            building = '-';
        }else{
            building = inv.Building_Name__c;
        }
        if(inv.Comments__c == null || inv.Comments__c ==''){
            comments = '-';
        }else{
            comments = inv.Comments__c;
        }
        mailBody = 'Hi,<br/><br/>Please find the below list of units.<br/><br/>All prices in this communication are valid only for today and subject to change without notice,<br/><br/>';
        mailbody += '<table border="1"  cellspacing="0" width="100%"><thead><tr><th>Unit</th><th>Type</th><th>Area</th><th>Status</th><th>Property</th><th>Building</th>';
 		mailbody += '<th>Remarks</th></tr></thead><tbody><tr style="text-align: center;width:20px;"><td>'+name +'</td><td>'+unitType+'</td><td>'+inv.Area__c+'</td>';
    	mailbody += '<td>'+status+'</td><td>'+pName+'</td>';
        mailbody += '<td>'+building+'</td><td>'+comments+'</td></tr></tbody></table><br/><br/>';
        mailBody +='Regards,<br/><br/>This is a system generated mail,do not reply on this.';
    }
    public void SendMail(){
        String toaddr = ApexPages.currentPage().getParameters().get('toAddress');
        String ccAddress = ApexPages.currentPage().getParameters().get('ccAddress');
        String bccAddress = ApexPages.currentPage().getParameters().get('bccAddress');
        String htmlBody = ApexPages.currentPage().getParameters().get('mailBody');
        String mailSub = ApexPages.currentPage().getParameters().get('mailSubject');
        system.debug('toaddr'+toaddr);
        system.debug('mailBody'+mailBody);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = toAddr.split(',');
         if(ccAddress !='' && ccAddress !=null ){
            String[] ccAddresses = ccAddress.split(',');
            mail.setCcAddresses(ccAddresses);
        }         
        if(bccAddress !='' && bccAddress !=null ){
            String[] bccAddresses = bccAddress.split(',');           
            mail.setBccAddresses(bccAddresses);            
        }
        mail.setToAddresses(toAddresses);
        system.debug('toaddr'+toAddresses);
        mail.setSubject(mailSub);
        mail.setPlainTextBody(htmlBody);
	    mail.setHtmlBody(htmlBody);
        system.debug('mailSub'+mailSub);
        system.debug('mailBody'+htmlBody);
        try{
            // Send the email you have created.
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        catch (Exception e){
       system.debug(e);
        }

    }

}