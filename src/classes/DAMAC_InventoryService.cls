/*

    Class Name     : DAMAC_InventoryService
    Test class Name: DAMAC_Inventory_External_Test
    Description    : To send filtered Invnetories to Inventory APP
*/

@RestResource(urlMapping='/InventoryService/*')
global class DAMAC_InventoryService{

    @HttpPOST
    global static void getResults(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String reqBody = req.requestBody.toString();
        System.debug (reqBody);
        // URL to send the Inventories based on the filters received from the inventory app and send the filtered Inventories
        if(req.requestURI == '/InventoryService/applyFilters'){ 
            Map<String, Object> jsonResponse = (Map<String, Object>) JSON.deserializeUntyped(reqBody);

            System.debug(jsonResponse);
            String status = 'Released';
            String query = 'SELECT '
                        +'Unit_Plan_SF_Link__c,Floor_Plan_SF_Link__c, Plot_Plan_SF_Link__c,'
                        +'Marketing_Name_Doc__c, Marketing_Name_Doc__r.Name, Property_Name__c, Unit_Id__c, '
                        +'Unit_Name__c, Bedroom_Type__c, Bedrooms__c,IPMS_Bedrooms__c, Area_sft__c,'
                        +'Unit_Type__c, View_Type__c, Property_Status__c, Price_Per_Sqft__c,'
                        +'ACD_Date__c, Plot_Area__c, Floor_Plan__c, Unit_Plan__c, Plot_Plan__c,'
                        +'Sales_Offer_Additional_Charges__c, Unit_Catagory__c, convertCurrency(Total_Price_inc_VAT__c), '
                        +'Rera_Percentage__c, Special_Price__c, Special_Price_Tax_Amount__c, Marketing_Plan__c,Property_country__c,Property_City__c '
                        +'from inventory__c '
                        +'WHERE Tagged_To_Unit_Assignment__c = FALSE AND Marketing_name__c != NULL '
                        +'AND Status__c =: status AND Is_Assigned__c = false AND ';
            
            Map<String, List<String>> valuesMap = new Map<String, List<String>>();
            for (String key:jsonResponse.keySet()){
                List<String> values = new List<String>();
                if (String.valueOf(jsonResponse.get(key)).contains(',')) {
                    for (String val :String.valueOf(jsonResponse.get(key)).split(',')) {
                        values.add('\''+val.removeStart('(').removeEnd(')').trim()+'\'');
                    }
                }
                else
                    values.add('\''+String.valueOf(jsonResponse.get(key)).trim()+'\'');

                valuesMap.put(key, values);
            }
            for (String key:jsonResponse.keySet()){
                
                if (key.startsWith ('(MAX)')) {
                    Decimal maxPrice = Decimal.valueOf(String.valueOf(jsonResponse.get(key)));
                    query += ' '+key.removeSTART('(MAX)') +' <= '+maxPrice+' AND ';
                }
                else if (key.startsWith ('(MIN)')) {
                    Decimal minPrice = Decimal.valueOf(String.valueOf(jsonResponse.get(key)));
                    query += ' '+key.removeSTART('(MIN)') +' >= '+ minPrice+' AND ';
                } else {
                    List<String> valuesList = new List<String>();
                    if (valuesMap.containsKey(key))
                        valuesList = valuesMap.get(key) ;

                    query += ' '+key +' IN '+valuesMap.get(key)+' AND ';
                }           
            }
            query = query.removeEND('AND ');
            query = query.removeEND('WHERE ');
            query = query+' Order BY Unit_Released_Date_from_IPMS__c DESC, Marketing_Name__c, '
                   +' Unit_Name__c, District__c, '
                   +' Property_City__c, Price__c, Area_sft__c DESC LIMIT 50000';
            System.Debug (query);
            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(Database.query(query))); 
        }
        else {
            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf('{"Error" : "URL Not found"}');    
        }
    }

    // Method to send the Inventory layout from inventory app layouts object to display it in the app
    @HttpGET
    global static void getInventory(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        Map <String, List<Inventory_App_Layouts__c>> layoutsMap 
                = new Map<String, List<Inventory_App_Layouts__c>>();
        for (Inventory_App_Layouts__c layout:[SELECT Order__c, Search_Field__c, 
                                            Search_Label__c, Search_Section__c,
                                            Value_Type__c 
                                            FROM Inventory_App_Layouts__c 
                                            WHERE Active__c = TRUE])
        {
            if(layoutsMap.containsKey(layout.Search_Section__c))
                layoutsMap.get(layout.Search_Section__c).add(layout);
            else
                layoutsMap.put(layout.Search_Section__c, new List<Inventory_App_Layouts__c>{layout});
        }
        list<inventory__c> invS = new list<inventory__c>();
        invS  = [SELECT Floor_Package_Name__c,Selling_Price_excl_VAT__c,
            Unit_Plan_SF_Link__c,Floor_Plan_SF_Link__c, Plot_Plan_SF_Link__c,
            Property_city__c, Bedroom_Type__c,IPMS_Bedrooms__c,Property_Status__c,
            Unit_Type__c,Unit__c, Property_name__c, Property__c,
            Anticipated_Completion_Date__c,View_Type__c,
            District__c,Marketing_Name_Doc__r.Name,
            Marketing_Name_Doc__c 
            FROM Inventory__c 
            WHERE status__c='Released' 
            AND Is_Assigned__c = FALSE
            Order BY Unit_Released_Date_from_IPMS__c DESC, Marketing_Name__c, 
            Unit_Name__c, District__c, 
            Property_City__c, Price__c, Area_sft__c DESC
            LIMIT 10000 ];
            
        Map<String,id> productName_idmap = new Map<String,Id>();
        Map <string, ID> propertyNames = new Map<string, ID>();
        Set<string> propStatuses = new Set<string>();
        Set<string> unitTypes = new Set<string>();
        Set<string> unitss = new Set<string>();
        Set<string> acd = new Set<string>();
        Set<string> bedrooms= new Set<string>();
        Set<string> viewTypes = new Set<string>();
        Set<string> pkgNames = new Set<string>();
        Set<string> prjctFtrs = new Set<string>();
        Set<string> district = new Set<String>();
        Set<string> cities = new Set<String>();

        if(invS.size() > 0){
            for (Inventory__c inv:invs){
                if(inv.Marketing_Name_Doc__c != null){
                    productName_idmap.put(inv.Marketing_Name_Doc__r.name, inv.Marketing_Name_Doc__c);  
                }
                if(inv.property_name__c != null){
                    propertyNames.put(inv.property_name__c.toUpperCase(), inv.Property__c);
                }
                if(inv.Property_Status__c != null){
                    propStatuses.add(inv.Property_Status__c.toUpperCase());
                }
                if(inv.Unit_Type__c != null){
                    unitTypes.add(inv.Unit_Type__c.toUpperCase());
                }
                if(inv.Unit__c != null){
                    unitss.add(inv.Unit__c.toUpperCase());
                }
                
                if(inv.Anticipated_Completion_Date__c !=null){
                    acd.add(inv.Anticipated_Completion_Date__c.toUpperCase());
                }
                if(inv.IPMS_Bedrooms__c!=null){
                    //bedrooms.add(inv.Bedroom_Type__c.toUpperCase() );
                    bedrooms.add(inv.IPMS_Bedrooms__c.toUpperCase() );
                }  
                if(inv.Floor_Package_Name__c != null){
                    pkgNames.add(inv.Floor_Package_Name__c.toUpperCase());
                }
                if(inv.View_Type__c != null){
                    viewTypes.add(inv.View_Type__c.toUpperCase());
                }
                if(inv.District__c != null){
                    district.add(inv.District__c.toUpperCase());
                }
                if(inv.property_city__c != null){
                    cities.add(inv.property_city__c.toUpperCase());
                }
            }
        }
        List<String> productnamelst = new List<String>();
        for (String productname : productname_idmap.keyset()){
            productnamelst.add(productname);
        }
        productnamelst.sort();

        List<String> propertynamessort = new List<String>();
        
        for (String productname : propertyNames.keyset()){
            propertynamessort.add(productName);
        }
        propertynamessort.sort();

        List<String> unittypesort = new List<String>();
        unittypesort.addall(unitTypes);
        unittypesort.sort(); 

        List<String> bedroomsort = new List<String>();
        bedroomsort.addall(bedrooms);
        bedroomsort.sort();

        List<String> viewtypesort = new List<String>();
        viewtypesort.addall(viewTypes);
        viewtypesort.sort();

        List<String> districtlst = new List<String>();
        districtlst.addAll(district);
        districtlst.sort();

        List<String> pkgNamesLst = new List<String>();
        pkgNamesLst.addAll(pkgNames);
        pkgNamesLst.sort();

        List<String> acdlst = new List<String>();
        acdlst.addAll(acd);
        acdlst.sort();

        List<String> unitlst = new List<String>();
        unitlst.addAll(unitss);
        unitlst.sort();

        List<String> propStatuseslst = new List<String>();
        propStatuseslst.addAll(propStatuses);
        propStatuseslst.sort();
        
        List<String> citiesLst = new List<String>();
        citiesLst.addAll(cities);
        citiesLst.sort();
        
        System.Debug(districtLst);
        

        Map<String, List<String>> optionsMap = new Map<String, List<String>>();
        optionsMap.put(('Marketing_Name_Doc__c').toLowerCase(), productnamelst);
        optionsMap.put(('property_name__c').toLowerCase(), propertynamessort);
        optionsMap.put(('Property_Status__c').toLowerCase(), propStatuseslst);
        optionsMap.put(('Unit_Type__c').toLowerCase(), unittypesort);
        optionsMap.put(('Unit__c').toLowerCase(), unitlst);
        optionsMap.put(('property_name__c').toLowerCase(), propertynamessort);
        optionsMap.put(('Anticipated_Completion_Date__c').toLowerCase(), acdlst);
        optionsMap.put(('IPMS_Bedrooms__c').toLowerCase(), bedroomsort);
        optionsMap.put(('Floor_Package_Name__c').toLowerCase(), pkgNamesLst);
        optionsMap.put(('View_Type__c').toLowerCase(), viewtypesort);
        optionsMap.put(('District__c').toLowerCase(), districtlst);
        optionsMap.put(('Property_city__c').toLowerCase(), citiesLst);

        String jsonString = '{';
        for (String key:layoutsMap.keySet()){
            jsonString += '"'+key+'" : [';
            for (Inventory_App_Layouts__c obj:layoutsMap.get(key)){
                jsonString += '{';
                    jsonString += '"Label" : "'+obj.Search_Label__c+'",'
                                + '"API" : "'+obj.Search_Field__c+'",'
                                + '"Type" : "'+obj.Value_Type__c+'",'
                                + '"Order" : "'+obj.Order__c+'",'
                                + '"Values" : [';
                                if (optionsMap.containsKey(obj.Search_Field__c.toLowerCase())) {
                                    List<String> valuesList = optionsMap.get(obj.Search_Field__c.toLowerCase());
                                    for (String option :valuesList){
                                        
                                        String value = '';
                                        if (obj.Search_Field__c == 'Marketing_Name_Doc__c') {
                                            if (productname_idMap.containsKey (option))
                                                value = productname_idmap.get(option);
                                        }
                                        if (obj.Search_Field__c.toLowerCase() == 'property_name__c') {
                                            if (propertyNames.containsKey (option)) {
                                                value = propertyNames.get(option);
                                            }
                                        }
                                        if (obj.Search_Field__c != 'Marketing_Name_Doc__c' && obj.Search_Field__c.toLowerCase() != 'property_name__c')
                                            value = option.toUppercase();
                                       
                                        jsonString += '{"Label" : "'+option.toUpperCase()+'",'
                                                    + '"Value" : "'+value+'"},';
                                    
                                    }
                                } 
                                jsonString = jsonString.removeEnd(',');
                                jsonString += ']';
                jsonString += '},';
            }
            jsonString = jsonString.removeEnd(',');
            jsonString += '],';
        }
        jsonString = jsonString.removeEnd(',');
        jsonString += '}';
        System.Debug(jsonString);
        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.responseBody = Blob.valueOf(jsonString); 
    }



}