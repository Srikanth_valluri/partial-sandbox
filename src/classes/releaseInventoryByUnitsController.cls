/****************************************************************************************************
* Name               : releaseInventoryByUnitsController
* Description        : Controller class for releaseInventoryByUnits VF Page
* Created Date       : 22/10/2020
* Created By         : QBurst
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst         22/10/2020      Initial Draft
****************************************************************************************************/
    
    global with sharing class releaseInventoryByUnitsController {
    public List<String> propertyNames {get; set;}
    Map<String, Property__c> propertyMap;
    public static string projectDetails {get;set;}
    public static string invDetails {get;set;}
    public string projName {get; set;}
    public string selectedStatus {get; set;}
    public List<String> restrictReasons {get; set;}
    public string resReasons {get;set;}
    public string invId {get; set;}
    public map<Id, String> invCommentMap {get; set;}
    public Map<String, projectDetailsWrapper> projectWrapperMap{get; set;}
    public String invCommentMapJSON  {get; set;}
    //public String invCommentMapJSON {get{return JSON.serialize(invCommentMap);}}

    /*********************************************************************************************
    * @Description : Constructor method
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public releaseInventoryByUnitsController() {
        propertyNames = new List<String>();
        propertyMap = new  Map<String, Property__c>();
        invCommentMap = new map<Id, String>(); 
        List<Inventory__c> invList = new List<Inventory__c>();
        invDetails = JSON.serialize(invList);
        invCommentMapJSON = JSON.serialize(invCommentMap);
        for(Property__c property: [SELECT Id, Property_Name__c, Property_ID__c, District__c 
                                   FROM Property__c 
                                   WHERE Property_Name__c != ''
                                   ORDER BY Property_Name__c ASC]){
            
            propertyNames.add(property.Property_Name__c);
            propertyMap.put(property.Property_Name__c, property);
        }
    }

    /************************************************************************************************
    * @Description : method to fetch the Buildng Details.                                           *
    * @Params      : void                                                                           *
    * @Return      : void                                                                           *
    ************************************************************************************************/
    public void fetchBuildingDetails(){
        String projName = ApexPages.currentPage().getParameters().get('projectName');
        Property__c propertyObj =  propertyMap.get(projName);
        projectWrapperMap = new Map<String, projectDetailsWrapper>();
        restrictReasons = new List<String> ();
        projectDetails = '';        
        system.debug('property: ' + propertyObj);
        for(Property__c property: [SELECT Id, Property_Name__c, Property_ID__c, District__c 
                                   FROM Property__c 
                                   WHERE Property_Name__c =: propertyObj.Property_Name__c
                                   ORDER BY Property_Name__c ASC]){
            projectDetailsWrapper wrapper = new projectDetailsWrapper();
            wrapper.projectName = property.Property_Name__c;
            wrapper.projectId = property.Id;
            wrapper.type = 'TOWER';
            wrapper.city = property.District__c;
            projectWrapperMap.put(property.Property_Name__c, wrapper);
        }
        system.debug('projectWrapperMap'+projectWrapperMap);
        List<Inventory_Restrict_Codes__c> restrictReasonCS = Inventory_Restrict_Codes__c.getall().values();
        for(Inventory_Restrict_Codes__c reason: restrictReasonCS){
            restrictReasons.add( reason.Name);
        }
        resReasons = JSON.serialize(restrictReasons);
        fetchProjectDetails();
    }

    public void fetchProjectDetails(){
         Map<String, Location__c> locationMap = new Map<String, Location__c>();
         Map<String, Integer> locationFloorCountMap = new Map<String, Integer>();
         Map<String, Integer> locationBuildingCountMap = new Map<String, Integer>();
         Map<String, List<Inventory__c>> locationInventoryMap = new Map<String, List<Inventory__c>>();
         invCommentMap = new map<Id, String>();
        invId = '';
        projName = '';
        selectedStatus = '';
         List<projectDetailsWrapper> wrapperList = new List<projectDetailsWrapper>();
         for(Location__c loc: [SELECT Id, Name, Building_Name__c , Property_Name__c, Property_Name__r.Property_Name__c
                               FROM Location__c 
                               WHERE Location_Type__c = 'Building'
                               AND Property_Name__r.Property_Name__c IN: projectWrapperMap.keyset()]){
             Integer count = 0;
             if(locationBuildingCountMap.containsKey(loc.Property_Name__r.Property_Name__c)){
                 count = locationBuildingCountMap.get(loc.Property_Name__r.Property_Name__c);
             }
             count++;
             locationBuildingCountMap.put(loc.Property_Name__r.Property_Name__c, count);
         }
         for(Location__c loc: [SELECT Id, Name, Building_Name__c , Building_Number__c,
                                     Building_Number__r.Property_Name__c,
                                     Building_Number__r.Property_Name__r.Property_Name__c
                               FROM Location__c 
                               WHERE Location_Type__c = 'Floor'
                               AND Building_Number__r.Property_Name__r.Property_Name__c IN: projectWrapperMap.keyset()]){
             Integer count = 0;
             if(locationFloorCountMap.containsKey(loc.Building_Number__r.Property_Name__r.Property_Name__c)){
                 count = locationFloorCountMap.get(loc.Building_Number__r.Property_Name__r.Property_Name__c);
             }
             count++;
             locationFloorCountMap.put(loc.Building_Number__r.Property_Name__r.Property_Name__c, count);
         }
         system.debug('locationFloorCountMap: '+ locationFloorCountMap);
         for(Inventory__c inv: [SELECT Id, Status__c, Building_Location__c,
                                     Building_Location__r.Property_Name__r.Property_Name__c
                                FROM Inventory__c 
                                WHERE Building_Location__r.Property_Name__r.Property_Name__c IN: projectWrapperMap.keyset()
                                ORDER BY Building_Location__r.Property_Name__r.Property_Name__c DESC
                                ]){
             List<Inventory__c> invList = new List<Inventory__c>();
             if(locationInventoryMap.containsKey(inv.Building_Location__r.Property_Name__r.Property_Name__c)){
                 invList = locationInventoryMap.get(inv.Building_Location__r.Property_Name__r.Property_Name__c);
             }
             invList.add(inv);
             locationInventoryMap.put(inv.Building_Location__r.Property_Name__r.Property_Name__c, invList);    
         }
         system.debug('locationInventoryMap: '+ locationInventoryMap);
        List<Inventory__c> invList = new List<Inventory__c>();
        for(Inventory__c inv: [SELECT Id, Status__c, Building_Location__c, Unit_Name__c,
                               Building_Location__r.Property_Name__r.Property_Name__c,
                                   Restriction_Category__c, IPMS_Bedrooms__c,
                                   Selling_Price__c, ACD_Date__c
                               FROM Inventory__c 
                               WHERE Unit_Name__c != null AND Building_Location__r.Property_Name__r.Property_Name__c IN: projectWrapperMap.keyset() AND
                               (Status__c = 'Inventory' OR Status__c = 'Restricted' OR Status__c = 'Released')
                               ORDER BY Building_Location__r.Property_Name__r.Property_Name__c DESC]){
                
                  invList.add(inv);
                  invCommentMap.put(inv.Id, '');
        }
        invDetails = JSON.serialize(invList);
         for(String projectName: projectWrapperMap.keyset()){
             system.debug('projectName'+projectName);
             system.debug('projectWrapperMap.get(projectName)');
             projectDetailsWrapper wrapper = projectWrapperMap.get(projectName);
             Integer units = 0;
             Integer inventory = 0;
             Integer available = 0;
             Integer sold = 0;
             Integer restricted = 0;
             Integer floors = 0;
             Integer buildings = 0;
             Boolean payPlan = false;
             if(locationBuildingCountMap.containsKey(projectName)){
                 buildings = locationBuildingCountMap.get(projectName);
             }
             if(locationFloorCountMap.containsKey(projectName)){
                 floors = locationFloorCountMap.get(projectName);
             }
             if(locationInventoryMap.containsKey(projectName)){
                 system.debug('projectName'+projectName);              
                 for(Inventory__c inv: locationInventoryMap.get(projectName)){
                     units++;
                     if(inv.Status__c == 'AVAILABLE' || inv.Status__c == 'Released'){
                         available++;
                     } else if(inv.Status__c == 'Inventory'){
                         inventory++;
                     } else if(inv.Status__c == 'Sold'){
                         sold++;
                     } else if(inv.Status__c == 'Restricted'){
                         restricted++;
                     }
                 }
             }
             wrapper.buildings = buildings;
             wrapper.floors = floors;
             wrapper.units = units;
             wrapper.available = available;
             wrapper.inventory = inventory;
             wrapper.sold = sold;
             wrapper.restricted = restricted;
             system.debug('wrapper'+wrapper);
             wrapperList.add(wrapper);
         } 
         invCommentMapJSON = JSON.serialize(invCommentMap);   
         projectDetails = JSON.serialize(wrapperList);
         system.debug('projectDetails: ' + projectDetails);
    }

    public List<SelectOption> getRestrictReasonOptions() {
        List<Inventory_Restrict_Codes__c> restrictReasons = Inventory_Restrict_Codes__c.getall().values();
        List<SelectOption> restrictReasonOptions = new List<SelectOption>();
        for(Inventory_Restrict_Codes__c reason: restrictReasons){
            restrictReasonOptions.add(new SelectOption(reason.Name, reason.Name));
        }
        system.debug('restrictReasonOptions: ' + restrictReasonOptions);
        return restrictReasonOptions;
    }

    public void updateComments(){
        String invId = ApexPages.currentPage().getParameters().get('unitInvId');
        String comments = ApexPages.currentPage().getParameters().get('invComments');
        invCommentMap.put(invId, comments);
        system.debug('invCommentMap: ' + invCommentMap);
        invCommentMapJSON = JSON.serialize(invCommentMap);  
    }
    
    
    public void fetchInventoryDetails(){
        String pjName = ApexPages.currentPage().getParameters().get('projName');
        String slStatus = ApexPages.currentPage().getParameters().get('status');
        invCommentMap = new map<Id, String>();        
        system.debug('selectedStatus: ' + slStatus);
        List<Inventory__c> invList = new List<Inventory__c>();
        if(pjName != null && pjName != ''){
            projName = pjName;
        }        
		system.debug('projectName: ' + projName);
        if(slStatus != null && slStatus != ''){
            selectedStatus = slStatus;
            if(selectedStatus.trim() == 'Available'){
                for(Inventory__c inv: [SELECT Id, Status__c, Building_Location__c,
                                     Bedrooms__c,  ACD_Date__c, Selling_Price__c,
                                     Restriction_Remarks__c, Restriction_Category__c,
                                     Comments__c, Unit_Name__c,IPMS_Bedrooms__c,
                                     Building_Location__r.Property_Name__r.Property_Name__c
                                FROM Inventory__c 
                                WHERE Unit_Name__c != null AND Building_Location__r.Property_Name__r.Property_Name__c =: projName
                                AND (Status__c =: selectedStatus OR Status__c ='Released')]){
                 invList.add(inv);
                 invCommentMap.put(inv.Id, '');
             }
          } else if(selectedStatus.trim() == 'None'){
                system.debug('invList'+projectWrapperMap);
                system.debug('in else');
                 for(Inventory__c inv: [SELECT Id, Status__c, Building_Location__c,
                                     Bedrooms__c,  ACD_Date__c, Selling_Price__c,
                                     Restriction_Remarks__c, Restriction_Category__c,
                                     Comments__c, Unit_Name__c,IPMS_Bedrooms__c,
                               Building_Location__r.Property_Name__r.Property_Name__c
                               FROM Inventory__c 
                               WHERE Unit_Name__c != null AND Building_Location__r.Property_Name__r.Property_Name__c IN: projectWrapperMap.keyset() AND
                               (Status__c = 'Inventory' OR Status__c = 'Restricted' OR Status__c = 'Released')
                               ORDER BY Building_Location__r.Property_Name__r.Property_Name__c DESC]){
                invList.add(inv); 
                invCommentMap.put(inv.Id, '');  
            }
        } else{
            for(Inventory__c inv: [SELECT Id, Status__c, Building_Location__c,
                                     Bedrooms__c,  ACD_Date__c, Selling_Price__c,
                                     Restriction_Remarks__c, Restriction_Category__c,
                                     Comments__c, Unit_Name__c,IPMS_Bedrooms__c,
                                     Building_Location__r.Property_Name__r.Property_Name__c
                                FROM Inventory__c 
                                WHERE Unit_Name__c != null AND Building_Location__r.Property_Name__r.Property_Name__c =: projName
                                AND Status__c =: selectedStatus]){
                invList.add(inv);
                invCommentMap.put(inv.Id, ''); 
            }
        }
        }
        else{
            system.debug('Inventory else');
            for(Inventory__c inv: [SELECT Id, Status__c, Building_Location__c,
                                     Bedrooms__c,  ACD_Date__c, Selling_Price__c,
                                     Restriction_Remarks__c, Restriction_Category__c,
                                     Comments__c, Unit_Name__c,IPMS_Bedrooms__c,
                               Building_Location__r.Property_Name__r.Property_Name__c
                               FROM Inventory__c 
                               WHERE Unit_Name__c != null AND (Status__c = 'Inventory' OR Status__c = 'Restricted' OR Status__c = 'Released')
                                   AND Building_Location__r.Property_Name__r.Property_Name__c  =: projName                                
                               ORDER BY Building_Location__r.Property_Name__r.Property_Name__c DESC]){
                invList.add(inv); 
                invCommentMap.put(inv.Id, '');
                 system.debug('Inventory '+invList);
            }
        }
        invDetails = JSON.serialize(invList);
        system.debug('invDetails: ' + invDetails);
    }

    public void updateInventory(){
        String updDetails = ApexPages.currentPage().getParameters().get('updateDetails');
        system.debug('updateDetails: ' + updDetails);
        List<InvUpdateWrapper> deserializeResults = (List<InvUpdateWrapper>)JSON.deserialize(updDetails, List<InvUpdateWrapper>.class);
        system.debug('deserializeResults: ' + deserializeResults);
        List<Inventory__c> invList = new List<Inventory__c>();
        Map<Id, Inventory__c> invMap = new Map<Id, Inventory__c>();
        for(InvUpdateWrapper wrapper: deserializeResults){
            invMap.put(wrapper.unitId, null);
        }
        for(Inventory__c inv: [SELECT Id, Status__c, Restriction_Remarks__c, Restriction_Category__c,Comments__c
                                FROM Inventory__c WHERE Id IN: invMap.keyset()]){  
            invMap.put(inv.Id, inv);  
        }
        
        for(InvUpdateWrapper wrapper: deserializeResults){
            Inventory__c inv = invMap.get(wrapper.unitId);
            String status = wrapper.status;
           system.debug('reason'+wrapper.restricted_reason);
            if(status == 'released'){
                status = 'Released';
            } else if(status == 'restricted'){
                system.debug('status: ' + status);
                status = 'Restricted';
                if(wrapper.restricted_reason != 'None'){
                    inv.Restriction_Category__c = wrapper.restricted_reason;
                    inv.Restriction_Remarks__c = invCommentMap.get(inv.Id);
                }
            } else if(status == 'inventory'){
                status = 'Inventory';
            }
            inv.Status__c = status;
            system.debug('inv.Status__c : ' + inv.Status__c );
            String comment = inv.Comments__c;
            if(comment != null && comment != ''){
                comment += '\n';
            }
            comment += invCommentMap.get(inv.Id);
            inv.Comments__c = comment; 
            invList.add(inv);
        }
        system.debug('invList: ' + invList);
        if(invList.size() > 0){
            update invList;
        }        
        fetchInventoryDetails();
        fetchProjectDetails();
    }

    public class InvUpdateWrapper{
        public String unitId;
        public string status;
        public string restricted_reason;
        public invUpdateWrapper(){}
    }

     public class projectDetailsWrapper{
        public string projectName;
        public string type;
        public string projectId;
        public string city;
        public Integer buildings;
        public Integer floors;
        public Integer units;
        public Integer inventory;
        public Integer available;
        public Integer sold;
        public Attachment attach;
        public Integer restricted;
        public projectDetailsWrapper(){}
        public projectDetailsWrapper(String proName, String proId, String cityName,
                                Integer buildingCount, Integer floorsCount, Integer unitsCount, 
                                Integer inventoryCount, Integer availableCount, 
                                Integer soldCount, Integer restrictedCount){
            projectName = proName;
            projectId = proId;
            city = cityName;
            buildings = buildingCount;
            floors = floorsCount;
            units = unitsCount;
            inventory = inventoryCount;
            available = availableCount;
            sold = soldCount;
            restricted = restrictedCount;
        }
    }
}