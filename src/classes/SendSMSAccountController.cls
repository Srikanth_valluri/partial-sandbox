public virtual without sharing class SendSMSAccountController {
    
    public Set<String> nsPhones                 {Set;get;}
    public Set<String> sPhones                  {set;get;}
    public List<selectoption> soptions          {set;get;}
    public List<selectoption> nsoptions         {set;get;}
    public List<string> selected                {set;get;}
    public List<string> removed                 {set;get;}
    public String msgContent                    {get;set;}
    public String accountId                     {get;set;}
    public String selectedTemplateId            {get;set;}
    public String templateBody                  {get;set;}
    public Boolean isComplete                   {get;set;}
    public List<String>selectedLabelsLst        {get;set;}
    public Boolean isFMSMS                      {get;set;}

    Public Map<String,String> mapMaskedNoUnmasked {get;set;}
    public SendSMSAccountController(){

    }
    
    public SendSMSAccountController(ApexPages.StandardController controller){

    }
    
    public Pagereference init() {
        mapMaskedNoUnmasked = new Map<String,String>();
        Pagereference pg;
        isComplete = false;
        accountId = ApexPages.currentPage().getParameters().get('Id');
        accountId = String.valueOf(accountId).substring(0, 15);
        selectedLabelsLst = new List<String>();
        nsPhones = new set<String>();
        List<String> countryCodestr = new List<String>();  
        List<String>phoneNumberLst = new List<String>();
        Map<String,String>phoneValueToPhoneLabelMap = new Map<String,String>();
        List<Account> accountList = [SELECT Id,                          
                          Mobile_Phone_Encrypt__pc,
                          Mobile_Phone_Encrypt_2__pc,
                          Mobile_Phone_Encrypt_3__pc,
                          Mobile_Phone_Encrypt_4__pc,
                          Mobile_Phone_Encrypt_5__pc,                          
                          Mobile__c,
                          Mobile_Country_Code__pc,
                          Mobile_Country_Code_2__pc,
                          Mobile_Country_Code_3__pc,
                          Mobile_Country_Code_4__pc,
                          Mobile_Country_Code_5__pc,FAM_CAUTION__c,bypass_FAM__c,
                          isPersonAccount,Primary_CRE__c,Secondary_CRE__c,Tertiary_CRE__c,
                          (SELECT Id, 
                                  Lastname, 
                                  FirstName, 
                                  AccountId,
                                  Mobile_Phone__c,
                                  Mobile_Phone_2__c,
                                  Mobile_Phone_3__c,
                                  Mobile_Phone_4__c,
                                  Mobile_Phone_5__c,
                                  Mobile_Country_Code__c,
                                  Mobile_Country_Code_2__c,
                                  Mobile_Country_Code_3__c,
                                  Mobile_Country_Code_4__c,
                                  Mobile_Country_Code_5__c
                          FROM Contacts) 
                       FROM Account
                      WHERE Id = :accountId 
                       ];
                       
            String CurrentUserId = UserInfo.getUserId();            
            
            System.debug('objAccount'+accountList[0]);
            if( accountList[0] != NULL && accountList[0].bypass_FAM__c == False && String.isNotBlank(accountList[0].FAM_CAUTION__c) 
                   && Label.FAMFieldValue.containsIgnoreCase(accountList[0].FAM_CAUTION__c) && Label.FAM_Check.ContainsIgnoreCase('Y') ){
                System.debug('Outer If');
                if(accountList[0].Primary_CRE__c == NULL && accountList[0].Secondary_CRE__c == NULL && accountList[0].Tertiary_CRE__c == NULL){
                    System.debug('Iner If');
                    pg = new PageReference(Label.CSRBaseURL+'/apex/NotAuthorize');
                }else if( !((accountList[0].Primary_CRE__c != NULL && CurrentUserId.contains(accountList[0].Primary_CRE__c)) ||
                    (accountList[0].Secondary_CRE__c != NULL && CurrentUserId.contains(accountList[0].Secondary_CRE__c)) ||
                    (accountList[0].Tertiary_CRE__c != NULL && CurrentUserId.contains(accountList[0].Tertiary_CRE__c))) ){
                    System.debug('Iner else If');
                    pg = new PageReference( Label.CSRBaseURL+'/apex/NotAuthorize');
                    
                }
            }
          for(Account objAccount : accountList){
          
            if (objAccount.isPersonAccount == true) {
            system.debug('in if');
                /*if (objAccount.Mobile__c != null){
                    phoneNumberLst.add(objAccount.Mobile__c );
                }*/
                
                if (objAccount.Mobile_Phone_Encrypt__pc != null){
                    phoneNumberLst.add(maskNo(objAccount.Mobile_Phone_Encrypt__pc ));
                    mapMaskedNoUnmasked.put(maskNo(objAccount.Mobile_Phone_Encrypt__pc ),objAccount.Mobile_Phone_Encrypt__pc);
                    
                }
                
                if (objAccount.Mobile_Phone_Encrypt_2__pc != null){
                    phoneNumberLst.add(maskNo(objAccount.Mobile_Phone_Encrypt_2__pc ));
                    mapMaskedNoUnmasked.put(maskNo(objAccount.Mobile_Phone_Encrypt_2__pc ),objAccount.Mobile_Phone_Encrypt_2__pc);
                }
                
                if (objAccount.Mobile_Phone_Encrypt_3__pc != null){
                    phoneNumberLst.add(maskNo(objAccount.Mobile_Phone_Encrypt_3__pc ));
                    mapMaskedNoUnmasked.put(maskNo(objAccount.Mobile_Phone_Encrypt_3__pc ),objAccount.Mobile_Phone_Encrypt_3__pc);
                }
                
                if (objAccount.Mobile_Phone_Encrypt_4__pc != null){
                    phoneNumberLst.add(maskNo(objAccount.Mobile_Phone_Encrypt_4__pc ));
                    mapMaskedNoUnmasked.put(maskNo(objAccount.Mobile_Phone_Encrypt_4__pc ),objAccount.Mobile_Phone_Encrypt_4__pc);
                }
                
                if (objAccount.Mobile_Phone_Encrypt_5__pc != null){
                    phoneNumberLst.add(maskNo(objAccount.Mobile_Phone_Encrypt_5__pc ));
                    mapMaskedNoUnmasked.put(maskNo(objAccount.Mobile_Phone_Encrypt_5__pc ),objAccount.Mobile_Phone_Encrypt_5__pc);
                }
            } 
            else {
            system.debug('in else');
                if(objAccount.Contacts.size() > 0) {
                    if (objAccount.Contacts[0].Mobile_Phone__c != null){
                        phoneNumberLst.add(maskNo(objAccount.Contacts[0].Mobile_Phone__c));
                        mapMaskedNoUnmasked.put(maskNo(objAccount.Contacts[0].Mobile_Phone__c ),objAccount.Contacts[0].Mobile_Phone__c);
                    }
                    
                    if (objAccount.Contacts[0].Mobile_Phone_2__c != null){
                        phoneNumberLst.add(maskNo(objAccount.Contacts[0].Mobile_Phone_2__c));
                        mapMaskedNoUnmasked.put(maskNo(objAccount.Contacts[0].Mobile_Phone_2__c ),objAccount.Contacts[0].Mobile_Phone_2__c);
                    }
                    
                    if (objAccount.Contacts[0].Mobile_Phone_3__c != null){
                        phoneNumberLst.add(maskNo(objAccount.Contacts[0].Mobile_Phone_3__c));
                        mapMaskedNoUnmasked.put(maskNo(objAccount.Contacts[0].Mobile_Phone_3__c ),objAccount.Contacts[0].Mobile_Phone_3__c);
                    }
                    
                    if (objAccount.Contacts[0].Mobile_Phone_4__c != null){
                        phoneNumberLst.add(maskNo(objAccount.Contacts[0].Mobile_Phone_4__c));
                        mapMaskedNoUnmasked.put(maskNo(objAccount.Contacts[0].Mobile_Phone_4__c ),objAccount.Contacts[0].Mobile_Phone_4__c);
                    }
                    
                    if (objAccount.Contacts[0].Mobile_Phone_5__c != null){
                        phoneNumberLst.add(maskNo(objAccount.Contacts[0].Mobile_Phone_5__c));
                        mapMaskedNoUnmasked.put(maskNo(objAccount.Contacts[0].Mobile_Phone_5__c ),objAccount.Contacts[0].Mobile_Phone_5__c);
                    }
                }               
            }
          } 
          if(phoneNumberLst != null && !phoneNumberLst.isEmpty()){
              for(String phoneNum : phoneNumberLst){
                if(phoneNum != null){
                     nsPhones.add(phoneNum);
                 }
              } 
          } 
          sPhones = new Set<string>();
          soptions = new List<selectoption>();
          nsoptions = new List<selectoption>();
          selected = new List<string>();
          removed = new List<string>();
          fetchPhones();
        return pg;
    }
    
    public void fetchPhones(){
      nsoptions.clear();
      system.debug('!!!!!nsoptions'+nsoptions);
        soptions.clear();
        if(sPhones.size() == 0){
            SelectOption p=new SelectOption('none','--None--');
            soptions.add(p);
        }else{
            for(String firstSelectOpt : sPhones){
              if(firstSelectOpt != null){
                   SelectOption firstOpt = new SelectOption(firstSelectOpt,firstSelectOpt);
                   soptions.add(firstOpt);
                   System.debug('soptions:::::'+soptions);
              }
            }
        }
         if(nsPhones.size() == 0){
           SelectOption firstNotSelectOpt = new SelectOption('none','--None--');
            nsoptions.add(firstNotSelectOpt);
            system.debug('@@@@@@nsoptions'+nsoptions);
        }
        else{
              for(String selectOpt : nsPhones){
                    if(selectOpt != null && !selectOpt.contains('null')){
                        if (selectOpt != null) {
                              SelectOption notSelectOptions = new SelectOption(selectOpt,selectOpt);
                              nsoptions.add(notSelectOptions);
                              system.debug('#######nsoptions'+nsoptions);
                          }
                    }
              }
        }
    }
    public void addElements(){
      List<String>selectedValLst = new  List<String>();
        for(String selectedInst : selected){
            if(selectedInst != null){
                selectedValLst.add(selectedInst);
            }
        }
        sPhones.addAll(selectedValLst);
        nsPhones.removeAll(selected);
        fetchPhones();
    }
    public void removeElements(){
       List<string>removedValToLabelLst = new List<string>();
        for(String removedInst : removed){
            if(removedInst != null){
                removedValToLabelLst.add(removedInst);
            }
        }
        sPhones.removeAll(removed);
        nsPhones.addAll(removedValToLabelLst);
        fetchPhones();
    }
    public List<SelectOption> getMyPersonalTemplateOptions(){
      List<SelectOption> options = new List<SelectOption>();
      if(options.size() == 0)
        options.add(new SelectOption('--None--','--None--'));
      for(EmailTemplate emailObj :[SELECT Id,
                                          Name,
                                          Body
                                     FROM EmailTemplate
                                    WHERE Folder.Name = 'SMS Templates'
                                     AND TemplateType = 'Visualforce']){
         
        System.debug('emailObj:::::'+emailObj);
        options.add(new SelectOption(emailObj.Id,emailObj.Name));
      }
      System.debug('options::::::::*'+options);
      return options;
    }
    public void showContent(){
        if( selectedTemplateId == '--None--' || selectedTemplateId == '' ) {
            templatebody = '';
            return ;
        }
        EmailTemplate selectedTempLate = [SELECT Subject, 
                                                 Body, 
                                                 HtmlValue, 
                                                 Markup, 
                                                 TemplateType 
                                            FROM EmailTemplate
                                           WHERE Id = :selectedTemplateId];
       templatebody = String.valueOf((selectedTempLate.body));
       if(!Test.isRunningTest())
       Messaging.reserveSingleEmailCapacity(1);
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[]{'invalid@emailaddr.es'};
       mail.setToAddresses(toAddresses);
       mail.setUseSignature(false);
       mail.setSaveAsActivity(false);
       mail.setSenderDisplayName('DAMAC');
    
       mail.setTargetObjectId(UserInfo.getUserId());
       mail.setWhatId(accountId);
       mail.setTemplateId(selectedTempLate.Id);
       Savepoint sp = Database.setSavepoint();
       if(!Test.isRunningTest()){
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
       }
       Database.rollback(sp);
       templatebody = mail.getPlainTextBody();
       String mailHtmlBody = mail.getHTMLBody();
       String mailSubject = mail.getSubject();
    }
    
    public void callSMSService(){
        
          List<String> listStrings = new List<String>(sPhones);
          List<String> toSendStrings = new List<String>();
          for(string str : listStrings){
              toSendStrings.add(mapMaskedNoUnmasked.get(str));
          }
          if ( toSendStrings.isEmpty() == true ) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please choose atleast 1 mobile number'));
          }
          else if( String.isBlank( templateBody ) )  {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please enter some text'));
                return ;
          }
          else{
                //system.assert(false, 'Message Successfully Sent !!! '+toSendStrings);
                SendSMSAccountService.Sendtextmessage(toSendStrings,templateBody,accountId,isFMSMS,'');
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.confirm, 'Message Successfully Sent !!! '+toSendStrings));
                isComplete = true;
         }
   }
   
   Public String maskNo (String tempMob){
       return tempMob.Substring(0, 3) + 'xxxx'+tempMob.Substring(tempMob.length()-3,tempMob.length());
   }
 }