/**************************************************************************************************
* Name               : TasksToInquiryMappingScheduler                                             *
* Description        : Run the class to map the tasks to related Inquiry created from 3Cx integ   *
* Created Date       : 04/04/2017                                                                 *
* Created By         : NSI                                                                        *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR            DATE            COMMENTS                                          *
* 1.0         Sivasankar        12/01/2017      Initial Draft                                     *
**************************************************************************************************/
global class TasksToInquiryMappingScheduler implements Schedulable{
    global void execute(SchedulableContext SC) { 
        try{ 
            List<Task> tasksToMapInquiry = new List<Task>([SELECT Id, Status, User_Ext_No__c, Campaign_Number__c, Calling_Number__c, WhatID 
                                                           FROM Task 
                                                           WHERE CreatedBy.UserName LIKE '%CTI%' AND 
                                                                 User_Ext_No__c != null AND 
                                                                 Calling_Number__c != null AND 
                                                                 WhatId = null and createddate=today // Added on Aug 1 2018 =  createddate=:today
                                                           LIMIT : LIMITS.getLimitQueryRows()]); 
        
            if(!tasksToMapInquiry.isEmpty()){
                TasksToInquiryMapping.mapTaskToInquiry(tasksToMapInquiry, true);
            }
        }catch(Exception ex){ 
            system.debug('Exception at line number = '+ex.getLineNumber()+', Exception message = '+ex.getMessage()); 
        }
    }
}//End of class.