public with sharing class MyReceiptsController {
    //public static List<ReceiptDetailsWrapper> lstWrapper              {get;set;}
    private static final Map<String, String> RECEIPT_ATTRIBUTE_MAP = new Map<String, String> {
        'ATTRIBUTE1' => 'DocumentNumber',
        'ATTRIBUTE2' => 'ReceiptType',
        'ATTRIBUTE3' => 'PartyId',
        'ATTRIBUTE4' => 'PartyName',
        'ATTRIBUTE5' => 'CurrencyCode',
        'ATTRIBUTE6' => 'EnteredAmount',
        'ATTRIBUTE7' => 'UnAppliedAmount',
        'ATTRIBUTE8' => 'AppliedAmount',
        'ATTRIBUTE9' => 'FunctionalAmount',
        'ATTRIBUTE10' => 'ReceiptNumber',
        'ATTRIBUTE11' => 'ReferenceNumber',
        'ATTRIBUTE12' => 'ReceiptDate',
        'ATTRIBUTE13' => 'ValidatedYN',
        'ATTRIBUTE14' => 'RegistrationID',
        'ATTRIBUTE15' => 'MiscPaymentSource',
        'ATTRIBUTE16' => 'PaymentMethod',
        'ATTRIBUTE17' => 'Comments',
        'ATTRIBUTE18' => 'URL',
        'ATTRIBUTE19' => 'URLGeneratedOn'
    };

    @RemoteAction
    public static Object fetchMyReceipts() {
        String partyId = CustomerCommunityUtils.getPartyId();
        System.debug('partyId = ' + partyId);
        String strReceipts = ReceiptDetailsController.getReceiptDetails(partyId);

        if (String.isBlank(strReceipts)) {
            return null;
        }
        Map<String, Object> jsonReceipts = (Map<String, Object>) JSON.deserializeUntyped(strReceipts);

        Object objReceipts = jsonReceipts.get('data');
        if (objReceipts == NULL) return NULL;

        List<Object> lstReceipt = (List<Object>) objReceipts;

        for (Object receipt : lstReceipt) {
            Map<String, Object> receiptMap = (Map<String, Object>) receipt;
            for (String attribute : RECEIPT_ATTRIBUTE_MAP.keySet()) {
                receiptMap.put(RECEIPT_ATTRIBUTE_MAP.get(attribute), String.valueOf(receiptMap.get(attribute)));
            }
        }

       //return GenerateURL(lstReceipt);

        return lstReceipt;
    }

    @RemoteAction
    public static List<string> GenerateURL(String docNo) {
        //List<GenerateReceiptProcessService.APPSXXDC_PROCESS_SERX1794747X1X5> lstRegTerms =
        //    new List<GenerateReceiptProcessService.APPSXXDC_PROCESS_SERX1794747X1X5>();
        //map<String,Object> MapStr_Object = (map<String,Object>)receipt;
        /*GenerateReceiptProcessService.APPSXXDC_PROCESS_SERX1794747X1X5 regTerms = new GenerateReceiptProcessService.APPSXXDC_PROCESS_SERX1794747X1X5();
        regTerms.ATTRIBUTE1 = '';
        regTerms.ATTRIBUTE10 = '';
        regTerms.ATTRIBUTE11 = '';
        regTerms.ATTRIBUTE12 = '';
        regTerms.ATTRIBUTE13 = '';
        regTerms.ATTRIBUTE14 = '';
        regTerms.ATTRIBUTE15 = '';
        regTerms.ATTRIBUTE16 = '';
        regTerms.ATTRIBUTE17 = '';
        regTerms.ATTRIBUTE18 = '';
        regTerms.ATTRIBUTE19 = '';
        regTerms.ATTRIBUTE2 = '';
        regTerms.ATTRIBUTE20 = '';
        regTerms.ATTRIBUTE21 = '';
        regTerms.ATTRIBUTE22 = '';
        regTerms.ATTRIBUTE23 = '';
        regTerms.ATTRIBUTE24 = '';
        regTerms.ATTRIBUTE25 = '';
        regTerms.ATTRIBUTE26 = '';
        regTerms.ATTRIBUTE27 = '';
        regTerms.ATTRIBUTE28 = '';
        regTerms.ATTRIBUTE29 = '';
        regTerms.ATTRIBUTE3 = docNo;
        regTerms.ATTRIBUTE30 = '';
        regTerms.ATTRIBUTE31 = '';
        regTerms.ATTRIBUTE32 = '';
        regTerms.ATTRIBUTE33 = '';
        regTerms.ATTRIBUTE34 = '';
        regTerms.ATTRIBUTE35 = '';
        regTerms.ATTRIBUTE36 = '';
        regTerms.ATTRIBUTE37 = '';
        regTerms.ATTRIBUTE38 = '';
        regTerms.ATTRIBUTE39 = '';
        regTerms.ATTRIBUTE4 = '';
        regTerms.ATTRIBUTE41 = '';
        regTerms.ATTRIBUTE42 = '';
        regTerms.ATTRIBUTE43 = '';
        regTerms.ATTRIBUTE44 = '';
        regTerms.ATTRIBUTE45 = '';
        regTerms.ATTRIBUTE46 = '';
        regTerms.ATTRIBUTE47 = '';
        regTerms.ATTRIBUTE48 = '';
        regTerms.ATTRIBUTE49 = '';
        regTerms.ATTRIBUTE5 = '';
        regTerms.ATTRIBUTE50 = '';
        regTerms.ATTRIBUTE6 = '';
        regTerms.ATTRIBUTE7 = '';
        regTerms.ATTRIBUTE8 = '';
        regTerms.ATTRIBUTE9 = '';
        regTerms.PARAM_ID = docNo;
        lstRegTerms.add(regTerms);*/
        System.debug('DocNumber(ReceiptID) : ' + docNo);
        //List<GenerateReceiptService.ReceiptResponse> lstGenerateReceipt = new List<GenerateReceiptService.ReceiptResponse>();
        //lstGenerateReceipt = GenerateReceiptService.getReceiptURL(lstRegTerms);
        //system.debug('lstGenerateReceipt-----'+lstGenerateReceipt);
        //String url='';
        //if(lstGenerateReceipt != null
        //    && !lstGenerateReceipt.isEmpty()
        //    && lstGenerateReceipt[0].status != 'Exception'
        //) {
        //    url= lstGenerateReceipt[0].url;
        //}else{
        //    url = 'No URL Found.';
        //}
        //return url;

        //New Code 17/07/19
        List<String> lstReceiptUrl = new List<String>();
        try {
            List<String> lstReceiptUrl_Temp = new List<String>();
            lstReceiptUrl_Temp = FmIpmsRestCoffeeServices.getAllReceiptUrl(docNo);
            for(String objUrl: lstReceiptUrl_Temp){
                System.debug('-->> objUrl : ' + objUrl);
                String strLangCode = FmIpmsRestCoffeeServices.urlLanguageCodeMap.get(objUrl);
                System.debug('-->> strLangCode : ' + strLangCode);
                String strLangName = FmIpmsRestCoffeeServices.languageCodeMap.get(strLangCode);
                System.debug('-->> strLangName : ' + strLangName);
                lstReceiptUrl.add(objUrl + '-'+ strLangName);
            }
        } catch(Exception e) {
            System.debug(e.getMessage());
        }
        System.debug('lstReceiptUrl = ' + lstReceiptUrl);
        //
        if(lstReceiptUrl ==  null || lstReceiptUrl.isEmpty() ) {
            return null;
        }
        return lstReceiptUrl;
    }

    //public class ReceiptDetailsWrapper {
    //    public String ATTRIBUTE1 {get;set;}
    //    public String ATTRIBUTE2 {get;set;}
    //    public String ATTRIBUTE3 {get;set;}
    //    public String ATTRIBUTE4 {get;set;}
    //    public String ATTRIBUTE5 {get;set;}
    //    public String ATTRIBUTE6 {get;set;}
    //    public String ATTRIBUTE7 {get;set;}
    //    public String ATTRIBUTE8 {get;set;}
    //    public String ATTRIBUTE9 {get;set;}
    //    public String ATTRIBUTE10 {get;set;}
    //    public String ATTRIBUTE11 {get;set;}
    //    public String ATTRIBUTE12 {get;set;}
    //    public String ATTRIBUTE13 {get;set;}
    //    public String ATTRIBUTE14 {get;set;}
    //    public String ATTRIBUTE15 {get;set;}
    //    public String ATTRIBUTE16 {get;set;}
    //    public String ATTRIBUTE17 {get;set;}
    //    public String ATTRIBUTE18 {get;set;}
    //    public String ATTRIBUTE19 {get;set;}
    //    public String ATTRIBUTE20 {get;set;}
    //    public String url {get;set;}

    //    public ReceiptDetailsWrapper(){

    //    }
    //}
}