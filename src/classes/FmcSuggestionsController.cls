public without sharing class FmcSuggestionsController {

    public  List<SelectOption>      units               {get; set;}
    public  FM_Case__c              sr                  {get; set;}
    private Map<Id, Location__c>    mapUnitToBuilding   {get; set;}


    private Map<Id, Map<String, Id>>    mapBuildingToRolesToUserId;

    private static final String SR_SUBMIT_SUCCESS = 'Your Suggestion has been submitted successfully';
    private static final String SR_DRAFT_SUCCESS = 'Your Suggestion has been saved successfully';
    private static final String GENERIC_SAVE_ERROR = 'There was an error while processing your request. Please try again later.';

    private Id suggestionRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName()
                                        .get('Suggestion').getRecordTypeId();

    private Map<String, Id> mapFmUserRoleToId;
    private Map<Id, String> mapUserIdToEmail;

    public FmcSuggestionsController() {
        if (FmcUtils.isCurrentView('suggestionscomplaints')) {
        system.debug('ala method madhe====');
            initializeVariables();
        }
    }

    public void initializeVariables() {
        mapUnitToBuilding = new Map<Id, Location__c>();
        units = new List<SelectOption>();
        Set<Id> setBuildingId = new Set<Id>();
        for (Booking_Unit__c unit : FmcUtils.queryUnitsForAccount(
            CustomerCommunityUtils.customerAccountId,
            'Id, Name, Unit_Name__c, Inventory__c, Inventory__r.Building_Location__c, '
                + 'Inventory__r.Building_Location__r.Id, Inventory__r.Building_Location__r.Name, '
                + 'Inventory__r.Building_Location__r.Building_Name__c, '
                + 'Inventory__r.Building_Location__r.Loams_Email__c'
        )) {
            units.add(new SelectOption(unit.Id, unit.Unit_Name__c));
            if (unit.Inventory__c != NULL && unit.Inventory__r.Building_Location__c != NULL) {
                setBuildingId.add(unit.Inventory__r.Building_Location__c);
                mapUnitToBuilding.put(unit.Id, unit.Inventory__r.Building_Location__r);
            }
        }
        mapBuildingToRolesToUserId = FM_Utility.getFmUsersByLocationAndRole(setBuildingId);

        String caseId = ApexPages.currentPage().getParameters().get('id');
        if (String.isBlank(caseId)) {
            caseId = NULL;
        }

        List<FM_Case__c> draftSr = [
            SELECT      Id
                        , Name
                        , Status__c
                        , Booking_Unit__c
                        , Request_Type__c
                        , Suggestion_Type__c
                        , Suggestion_Sub_Type__c
                        , Description__c
            FROM        FM_Case__c
            WHERE       CreatedById = :UserInfo.getUserId()
                    AND RecordTypeId = :suggestionRecordTypeId
                    AND Origin__c = 'Portal'
                    AND Id = :caseId
                    AND Status__c = 'Draft Request'
            ORDER BY    Status__c
            LIMIT       1
        ];
        system.debug('draftSr===='+draftSr);
        if (draftSr.isEmpty()) {
            sr = new FM_Case__c(
                Origin__c = 'Portal',
                RecordTypeId = suggestionRecordTypeId,
                Request_Type__c = 'Suggestion',
                Request_Type_DeveloperName__c = 'Suggestion',
                Account__c = CustomerCommunityUtils.customerAccountId
            );
        } else {
            sr = draftSr[0];
        }
    }

    public void saveAsDraft() {
        system.debug('sr===='+sr);
        sr.Status__c = 'Draft Request';
        sr.Origin__c = 'Portal';
        sr.RecordTypeId = suggestionRecordTypeId;
        sr.Request_Type__c = 'Suggestion';
        sr.Request_Type_DeveloperName__c = 'Suggestion';
        sr.Account__c = CustomerCommunityUtils.customerAccountId;
        try {
            upsert sr;
        } catch(Exception excp) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, GENERIC_SAVE_ERROR));
            return;
        }
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, SR_DRAFT_SUCCESS));
    }

    public void submitSr() {
        system.debug('mapUnitToBuilding==='+mapUnitToBuilding);
        Location__c building = mapUnitToBuilding.get(sr.Booking_Unit__c);
        system.debug('building==='+building);
        sr.Submitted__c = true;
        sr.Status__c = 'Submitted';
        sr.Origin__c = 'Portal';
        sr.RecordTypeId = suggestionRecordTypeId;
        sr.Request_Type__c = 'Suggestion';
        sr.Request_Type_DeveloperName__c = 'Suggestion';
        sr.Account__c = CustomerCommunityUtils.customerAccountId;
        sr.Admin__c = Label.FM_Collection_User_Id;
        sr.Email_2__c = Label.Customer_Happiness_Centre_email;
        sr.Email_3__c = Label.FM_Collection_User_Email;
        if (building != NULL) {
            sr.Email__c = building.Loams_Email__c;
            sr.Property_Manager__c = mapBuildingToRolesToUserId.get(building.Id).get('Property Manager');
            sr.Property_Director__c = mapBuildingToRolesToUserId.get(building.Id).get('Property Director');
            for (User user : [
                SELECT  Id
                        , Email
                FROM    User
                WHERE   Id = :sr.Property_Manager__c
                    OR  Id = :sr.Property_Director__c
            ]) {
                if (user.Id == sr.Property_Manager__c) {
                    sr.Property_Manager_Email__c = user.Email;
                } else if (user.Id == sr.Property_Director__c) {
                    sr.Property_Director_Email__c = user.Email;
                }
            }
        }

        Datetime today = DateTime.newInstance(Date.today(), Time.newInstance(0, 0, 0, 0));
        String dayOfWeek = today.format('EEEE');
        if (dayOfWeek == 'Sunday' || dayOfWeek == 'Monday'){
            sr.Suggestion_Escalation_Date__c = system.today().addDays(3);
        } else if (dayOfWeek == 'Tuesday' || dayOfWeek == 'Wednesday' || dayOfWeek == 'Thurday' ||
            dayOfWeek == 'Friday'){
            sr.Suggestion_Escalation_Date__c = system.today().addDays(5);
        } else if (dayOfWeek == 'Saturday'){
            sr.Suggestion_Escalation_Date__c = system.today().addDays(4);
        }

        try {
            upsert sr;
        } catch(Exception excp) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, GENERIC_SAVE_ERROR));
            return;
        }
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, SR_SUBMIT_SUCCESS));
    }
}