@isTest
public class DrawloopAttachments_Test {

    public static testmethod void PostMethod(){
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        
        NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
        SR.Agency_Name__c = 'Test Account';
        SR.NSIBPM__Customer__c  = a.Id;
        SR.NSIBPM__Email__c = 'test@nsigulf.com';
        SR.NSIBPM__Send_SMS_to_Mobile__c= '12123';
        SR.COUNTRY_OF_SALE__C = 'UAE;Lebanon;Jordan';
        insert SR;
        
        Unit_Documents__c objUnitDocument = new Unit_Documents__c();
        objUnitDocument.Document_Name__c = 'test';
        objUnitDocument.Document_Description__c = 'test wedcf d';
        objUnitDocument.Document_type__c = 'Registration';
        objUnitDocument.Optional__c = false;
        objUnitDocument.Status__c = 'Pending Upload';
        objUnitDocument.User_Doc__c = false;
        objUnitDocument.DDP_ids__c = 'test-test';
        objUnitDocument.Service_Request__c = SR.id;
        insert objUnitDocument;


        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/drawloopAttachment/*';  
        req.httpMethod = 'POST';
        //Create JSON
        req.requestBody = Blob.valueof(objUnitDocument.id+'####Testing');
        RestContext.request = req;
        RestContext.response= res;
        // Test starts here
        Test.startTest();
        // method calling    
            DrawloopAttachments.doPost(); 
        Test.stopTest();
    }
}