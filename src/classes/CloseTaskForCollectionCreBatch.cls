/**
 * Batch call to close Inbound Tasks for Collection CRE's
 */
global class CloseTaskForCollectionCreBatch implements Database.Batchable<sObject> {
    global final String taskQuery;
    global final Case collectionsInboundDummyCase;

    public static final String collectionsInboundDummyCaseSubject =
        System.Label.Collections_Inbound_Dummy_Case_Subject;
    public static final String nonSrCaseRecordTypeName = 'Non_SR_case';
    public static final String collectionQueueName = 'Collection_Queue';
    public static final String accountName = 'TEST BOOKING';
    public static final String collectionCreRoleName = 'Collection - CRE';
    public static final String collectionManagerRoleName = 'Collection - Manager';
    public static final String inboundTaskSubject = '\'%Inbound:%\'';
    public static final String outboundTaskSubject = '\'%Outbound:%\'';

    global CloseTaskForCollectionCreBatch() {
        this.collectionsInboundDummyCase =
            getCollectionsInboundDummyCase(collectionsInboundDummyCaseSubject);
        this.taskQuery = createInboundTaskForCollectionCreQuery();
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(this.taskQuery);
    }

    global void execute(Database.BatchableContext BC, List<sObject> tasks) {
        System.debug('============== tasks : ' + tasks);
        for (Task task : (List<Task>) tasks) {
            task.WhatId = this.collectionsInboundDummyCase.Id;
            task.Status = 'Closed';
        }
        update tasks;
    }

    global void finish(Database.BatchableContext BC) {
    }

    public Case getCollectionsInboundDummyCase(String collectionsInboundDummyCaseSubject) {
        List<Case> collectionsInboundDummyCases =
            [
                SELECT
                    Id
                    , Subject
                    , RecordTypeId
                FROM
                    Case
                WHERE
                    Subject =: collectionsInboundDummyCaseSubject
            ];

        if (collectionsInboundDummyCases.size() <= 0) {
            RecordType nonSrRecordType =
                [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName =: nonSrCaseRecordTypeName];
            Group collectionQueue =
                [SELECT Id, Name, DeveloperName FROM Group WHERE DeveloperName =: collectionQueueName];
            List<Account> accounts = [SELECT Id, Name FROM Account WHERE Name =: accountName];

            Case collectionsInboundDummyCase = new Case(
                Subject = collectionsInboundDummyCaseSubject,
                RecordTypeId = nonSrRecordType.Id,
                OwnerId = collectionQueue.Id,
                Status = 'Closed'
            );
            if (accounts.size() != 0) {
                collectionsInboundDummyCase.AccountId = accounts[0].Id;
            }
            insert collectionsInboundDummyCase;
            return collectionsInboundDummyCase;
        }

        return collectionsInboundDummyCases[0];
    }

    public String createInboundTaskForCollectionCreQuery() {
        String createInboundTaskForCollectionCreQuery = '';
        List<UserRole> collectionUserRoles =
            [SELECT Id FROM UserRole WHERE (Name =: collectionCreRoleName OR Name =: collectionManagerRoleName)];

        createInboundTaskForCollectionCreQuery +=
            'SELECT' +
                ' Id' +
                ' ,IsClosed' +
                ' ,Process_Name__c' +
                ' ,Subject' +
                ' ,WhatId' +
                ' ,WhoId' +
                ' ,Owner.UserRoleId' +
            ' FROM' +
                ' Task' +
            ' WHERE' +
                ' (Subject LIKE ' + inboundTaskSubject +
                ' OR Subject LIKE ' + outboundTaskSubject + ')' +
                ' AND (Owner.UserRoleId = \'' + collectionUserRoles[0].Id + '\'' +
                ' OR Owner.UserRoleId = \'' + collectionUserRoles[1].Id + '\')' +
                ' AND IsClosed = false';

        System.debug('======== createInboundTaskForCollectionCreQuery : ' + createInboundTaskForCollectionCreQuery);
        return createInboundTaskForCollectionCreQuery;
    }
}