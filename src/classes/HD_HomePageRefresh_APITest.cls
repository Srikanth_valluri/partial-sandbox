@isTest
public class HD_HomePageRefresh_APITest {
    @isTest(seeAllData = true)
    static void  testHomePageRefresh(){
        User owner1 = [SELECT id, name, profileId, profile.name, Contact.accountId 
                       FROM User WHERE profile.name = 'Customer Community Login User(Use this)' 
                       AND IsActive = TRUE AND Contact.accountId != NULL LIMIT 1];
        User tenant1 = [SELECT id, name, profileId, profile.name, Contact.accountId 
                       FROM User WHERE profile.name = 'Tenant Community Login User' 
                       AND IsActive = TRUE AND Contact.accountId != NULL LIMIT 1];
        
        String ownerAcc1_ID, tenentAcc1_ID;
        if(NULL != owner1 && NULL != owner1.Contact && NULL != owner1.Contact.accountId){
            ownerAcc1_ID = String.valueOf(owner1.Contact.accountId);
        }
        if(NULL != tenant1 && NULL != tenant1.Contact && NULL != tenant1.Contact.accountId){
            tenentAcc1_ID = String.valueOf(tenant1.Contact.accountId);
        }
        
        Id personAcc_rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account acc1 = new Account( LastName = 'Test Account1',
                                       Party_ID__c = '6555553062',
                                       RecordtypeId = personAcc_rtId,
                                       Email__pc = 'test1@mailinator.com');
        insert acc1;
        
        String baseUrl = URL.getOrgDomainUrl().toExternalForm(); /* URL.getSalesforceBaseUrl() */
        
        Test.startTest();
        
        RestRequest request = new RestRequest();
        request.requestUri = baseUrl + '/services/apexrest/hd/home';
        request.httpMethod = 'GET';
        
        RestResponse response = new RestResponse();
		
        RestContext.request = request;
        RestContext.response = response;
        
        HD_HomePageRefresh_API.doGet(); /* 1 */
        
        request.addParameter('account_id', String.valueOf(owner1.id));
        HD_HomePageRefresh_API.doGet(); /* 2 */
        
        request.addParameter('account_id', String.valueOf(acc1.id));
        HD_HomePageRefresh_API.doGet(); /* 3 */
        
        request.addParameter('account_id', String.valueOf(ownerAcc1_ID));
        HD_HomePageRefresh_API.doGet(); /* 4 */
    }
}