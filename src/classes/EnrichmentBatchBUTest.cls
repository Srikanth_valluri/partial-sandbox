@isTest
public class EnrichmentBatchBUTest {
    @isTest
    static void test(){
        List<Account> accountList = new List<Account>();
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        objAccount.Nationality__c = 'UAE';
        objAccount.Email__pc = 'abc@z.com';
        objAccount.Party_ID__c = '12345';
        objAccount.Active_Customer__c = 'Active';      
        insert objAccount ;
        accountList.add(objAccount);
        
        

        NSIBPM__Service_Request__c  objSR = TestDataFactory_CRM.createServiceRequest();
        
        insert objSR;

        NSIBPM__Status__c   objstatus = new NSIBPM__Status__c();
        objstatus.NSIBPM__Code__c = 'CANCELLED';
        insert objstatus;

        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        objBooking.Account__c = objAccount.Id; 
        insert objBooking;
        
        Property__c objProperty = new Property__c();
        objProperty.Property_ID__c = 12345;
        objProperty.Property_Name__c = Label.EnrichmentBatchBUPropName;
        insert objProperty;
        
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        insert objInventory;
        
        List<Booking_Unit__c> lstBU = new List<Booking_Unit__c>();
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Registration_ID__c = '3901';
        objBU.Registration_Status_Code__c = 'AB';
        objBU.Booking__c = objBooking.Id;
        objBU.Inventory__c = objInventory.Id;
        insert objBU;
        lstBU.add(objBU);
                       
        Credentials_Details__c objCredDetail = new Credentials_Details__c();
        objCredDetail.Name = 'Enrichment';
        objCredDetail.Endpoint__c = 'https://damaccustenrich.servicebus.windows.net/customer-enrichment-hub-prod/messages';
        objCredDetail.Password__c = 'test';
        objCredDetail.User_Name__c = 'test';
        insert objCredDetail;
        
        Test.startTest();
            EnrichmentBatchBU objEnrichment = new EnrichmentBatchBU();
            DataBase.executeBatch(objEnrichment,1);            
        Test.stopTest();
    }
}