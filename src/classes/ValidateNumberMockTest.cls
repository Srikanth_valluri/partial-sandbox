@isTest
global class ValidateNumberMockTest implements HttpCalloutMock{
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"status": "valid",'+
                    '" status_message": "Success",'+
                    '"lookup_outcome": 0,'+
                    '"lookup_outcome_message": "Success",'+
                    '"request_id": "373a2cc9-9d62-4109-a33c-886b7974eb0c",'+
                    '"international_format_number": "917015774814",'+
                    '"national_format_number": "070157 74814",'+
                   '"country_code": "IN",'+
                    '"country_code_iso3": "IND",'+
                   ' "country_name": "India",'+
                    '"country_prefix": "91",'+
                   ' "request_price": "0.03000000",'+
                   ' "remaining_balance": "16.24",'+
                   ' "current_carrier": {'+
                    '    "network_code": "405858",'+
                     '   "name": "Reliance Jio Infocomm Limited - Gujarat",'+
                      '  "country": "IN",'+
                      '  "network_type": "mobile"'+
                   ' },'+
                    '"original_carrier": {'+
                     '   "network_code": "405859",'+
                        '"name": "Reliance Jio Infocomm Limited - Haryana",'+
                       ' "country": "IN",'+
                       ' "network_type": "mobile"'+
                   ' },'+
                   ' "valid_number": "valid",'+
                   ' "reachable": "reachable",'+
                    '"ported": "not_ported",'+
                   ' "roaming": {"status": "not_roaming"}}'
                   );
        res.setStatusCode(200);
        return res;
     }  
}