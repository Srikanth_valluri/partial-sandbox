/*
Developed By: DAMAC IT Team
Usage:DAMAC_WAYBEO_REST_REQUEST
*/
public class DAMAC_WAYBEO_REST_REQUEST{

    public String wrapup;    
    public String callid;    
    public String customer_clid ; 
    public String is_missed;
    public string called_no ;
    public string inquiry_id;
    
    public string extension;
    public string direction;
    public string campaignNumber;
    public string answered_time;
    public string completed_time;
    public string recording_url;
    //public string assignTo;
    
    public static DAMAC_WAYBEO_REST_REQUEST parse(String json){
        return (DAMAC_WAYBEO_REST_REQUEST) System.JSON.deserialize(json, DAMAC_WAYBEO_REST_REQUEST.class);
    }
}