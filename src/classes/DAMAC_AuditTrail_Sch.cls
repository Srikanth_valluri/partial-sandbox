/*
Description: Schedulable class for the DAMAC_AuditTrail to 
Developed By: DAMAC IT
*/
global class DAMAC_AuditTrail_Sch implements Schedulable { 
    global void execute(SchedulableContext SC) { 
    DAMAC_AuditTrail bcs = new DAMAC_AuditTrail();     
    Database.executeBatch(bcs,200); 
  }
 
}