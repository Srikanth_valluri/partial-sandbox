public class DAMAC_ContactLines_REQ{
    public String subRequestName;   //UPDATE_CUSTOMER_CONTACT
    public String propertyLocation; //DP
    public String partyId;  //2676094
    public cls_contactLines[] contactLines;
    public class cls_contactLines {
        public String primaryFlag;  //Y
        public String contactType;  //PHONE_MOBILE
        public String countryCode;  //91
        public String areaCode; //80
        public String phoneNumber;  //556656
        public String email;
        public String url;
    }
    
}