@RestResource (URLMapping='/getInvalidEmails/*')
global class Damac_CheckInvalidEmail {
    
    @HttpGET
    global static void doGet () {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        List <String> response = New List <String> ();
        for (Inquiry_Checks__c rec :[SELECT Email__c FROM Inquiry_Checks__c WHERE Email__c != NULL]) {
            response.add (rec.Email__c);
        }
        res.responseBody = Blob.valueof(JSON.serialize(response));
        res.statusCode = 200;
        
    }

}