/**
 * @File Name          : ReraPercentUpdTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 1/22/2020, 2:38:19 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/22/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
public Class ReraPercentUpdTest {
    
    @isTest
    public static void testReraPercentUpdate() {
        
        
        Property__c  propObj =  TestDataFactory_CRM.createProperty();
        
        propObj.Property_Code__c = 'BuilingCode';
        insert propObj;
        insert new Location__c( Location_ID__c = 'test',Location_Code__c = 'BuilingCode'
        ,Property_Name__c = propObj.Id
        );

        

        Test.startTest();
        ReraPercentUpd o = new ReraPercentUpd(); 
        ReraPercentUpd.ReraPercentUpd('BuilingCode'
                        , 'Percent'
                        , 'ProjNum'
                        , String.valueOf( system.today())
                        , String.valueOf( system.today())
                        , 'LastRERAPerc'
                        , 'PlannedPerc'
                        , String.valueOf( system.today()) );
        Test.stopTest();
    }
}