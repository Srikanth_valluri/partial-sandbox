/************************************************************************************************
 * @Name              : DAMAC_DealExceptionRequestAPI
 * @Test Class Name   : DAMAC_DealExceptionRequestAPI_Test
 * @Description       : RestResource Class for fetching Deal Exception Requests submitted for Management Approval
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         24/06/2020       Created
 * 1.1         Srikanth V     12/07/2020       Added the details of approver Email, Name
***********************************************************************************************/

@RestResource(urlMapping='/dealexceptionrequest/*')
global without sharing class DAMAC_DealExceptionRequestAPI{   
    
    global static set<string> status2consider = new set<string>{'Awaiting Agents Sales Management Review', 'Awaiting Management Approval', 'Awaiting MIS HOD Approval'};
    //global static set<string> status2consider = new set<string>{'Awaiting Agents Sales Management Review'};
    global static String approval_queue = 'Management Queue';

    @HttpGet
    global static GetResponseWrapper doGet(){
        RestRequest request = RestContext.request;
        GetResponseWrapper resp = new GetResponseWrapper();
        RestResponse response = RestContext.response;
        response.addHeader('Content-Type', 'application/json');
        String derId = '';
        derId = request.requestURI.substringAfter('dealexceptionrequest/');
        String mngQueueId = '';
        Map<Id, User> mngQueueUsersMap = new Map<Id, User>();
        try{
            mngQueueId = [SELECT Id FROM Group WHERE type='Queue' AND Name =: approval_queue LIMIT 1].Id;
            system.debug('mngQueueId: ' + mngQueueId);
            for(GroupMember grpMember: [SELECT UserOrGroupId FROM GroupMember WHERE GroupId =: mngQueueId]){
                mngQueueUsersMap.put(grpMember.UserOrGroupId, null);
            }
            system.debug('mngQueueUsersMap: ' + mngQueueUsersMap);
            for(User usr: [SELECT Id, Name, Email FROM User WHERE Id IN: mngQueueUsersMap.keyset()]){
                mngQueueUsersMap.put(usr.Id, usr);
            }
            system.debug('mngQueueUsersMap: ' + mngQueueUsersMap);
            detailWrapper details = new detailWrapper();
            details.approverUsers = mngQueueUsersMap.values();
            List<checkListWrapper> chkListItemsList = new List<checkListWrapper>();
            checkListWrapper chkList = new checkListWrapper();
            chkList.slNo = 1;
            chkList.value = '2% DLD Waiver';
            chkList.isTextArea = false;
            chkListItemsList.add(chkList);
            List<Checklist_Items__c>  checkListItemsList = new List<Checklist_Items__c>();
            details.checkListItemsList = new List<Checklist_Items__c>();
            for(Checklist_Items__c chk: [SELECT Value__c, Priority__c, Is_Text_Area__c 
                                         FROM Checklist_Items__c 
                                         WHERE Type__c = 'Deal Exception Request'
                                         ORDER BY Priority__c ASC]){
              //  checkListItemsList.add(chk);
                details.checkListItemsList.add(chk);
            }
            if(derId != null && derId != ''){
                system.debug('derId: ' + derId);
                Deal_Exception_Request__c deRecord = getderRecord(derId);
                if(deRecord != null){
                    resp.statusCode = '200';
                    resp.statusMessage = 'Fetched Deal Exception Requests Successfully';
                    List<String> planIdList = new List<String>();
                    Map<String, Payment_Plan__c> planMap = new Map<String, Payment_Plan__c>();
                    resp.derList = new List<Deal_Exception_Request__c>();
                    for(SObjectField f: Deal_Exception_Request__c.sobjecttype.getdescribe().fields.getmap().values()) {
                        try{
                            if(String.valueOf(deRecord.get(f)) != null && String.valueOf(deRecord.get(f)) != '' ){
                            } else{
                                deRecord.put(f, null); 
                            }
                        } catch(exception e){
                             system.debug('exception: ' + e.getMessage());
                        }
                    }
                    resp.derList.add(deRecord); 
                   // details.deUnitWrapper = new List<deUnitsWrapper>();//Map<String, deUnitsWrapper>();
                    details.deUnitWrapper = new List<Map<String, deUnitsWrapper>>();
                    List<String> processInstanceIds = new List<String>();
                    Deal_Exception_Request__c derDetails = getderRecordDetails(derId);
                    for (Deal_Exception_Request__c derApproval : [SELECT Id, (SELECT ID FROM ProcessInstances  ORDER BY CreatedDate DESC) 
                                                                  FROM Deal_Exception_Request__c WHERE ID  =: derId]){
                        for(ProcessInstance pi :derApproval.ProcessInstances) {
                            processInstanceIds.add(pi.Id);
                        }
                    }
                    Set <ID> userIds = new Set <ID> ();
                    List <processInstance> processInstances = new List <ProcessInstance> ();
                    processInstances = [SELECT TargetObjectId, LastActorId, LastActor.Name, CompletedDate, Status, 
                                        (SELECT Id, ActorId, Actor.Name, Actor.Email, OriginalActor.Name, Comments, OriginalActor.Type,
                                         StepStatus, IsPending, ProcessInstanceId, SystemModstamp
                                         FROM StepsAndWorkitems 
                                         WHERE StepStatus != 'Started' 
                                         ORDER BY ID DESC, CreatedDate DESC), 
                                        (SELECT Id, StepStatus, Comments FROM Steps ORDER BY CreatedDate DESC LIMIT 1 )
                                        FROM ProcessInstance 
                                        WHERE Id IN :processInstanceIds ORDER BY CreatedDate DESC];
                    
                    //1.1 begin
                    map<id, string> mpUserEmails = new map<id, string>(); 
                    map<id, User> mpUsers = new map<id, User>();
                    for (ProcessInstance pi :processInstances){
                        if (pi.StepsAndWorkitems.size() > 0) {
                            set<id> approverUserIds = new set<Id>();
                            set<Id> queueIds = new set<Id>();
                            set<Id> otherUserIds = new set<Id>();
                            for(ProcessInstanceHistory pih :pi.StepsAndWorkitems){
                                userIds.add (pih.ActorId);
                                if(pih.OriginalActor.Type == 'Queue')
                                    queueIds.add(pih.OriginalActorId);
                                else
                                    otherUserIds.add(pih.OriginalActorId);
                            }    

                            for(GroupMember gm :[Select Id, UserOrGroupId from GroupMember where Group.type='Queue' and GroupId in: QueueIds]){
                                approverUserIds.add(gm.UserOrGroupId);
                            }
    
                            mpUsers = new map<id, User>([Select id, email from user where id in: approverUserIds or id in: userIds or Id in: otherUserIds]);
                            for(GroupMember gm :[Select Id, UserOrGroupId, groupId from GroupMember where Group.type='Queue' and GroupId in: QueueIds]){
                                if(mpUsers.containskey(gm.UserOrGroupId))
                                    mpUserEmails.put(gm.groupId, mpUserEmails.containsKey(gm.groupId) ? mpUserEmails.get(gm.groupId)+','+mpUsers.get(gm.UserOrGroupId).Email : mpUsers.get(gm.UserOrGroupId).Email);
                            }
                        }
                    }
                    
                    for (User U : [SELECT Name, Email FROM User WHERE Id In :UserIds]) {
                        details.approverUsers.add (u);
                    }
                    //1.1 End
                    
                    //details.checkListItemsList = checkListItemsList;
                    details.approvalDetails = new List<approvalWrapper>();
                    
                    for (ProcessInstance pi : processInstances) {
                        system.debug('Approval Process: ' + pi);                        
                        if (pi.StepsAndWorkitems.size() > 0) {
                            integer i = 1;
                            for(ProcessInstanceHistory pih :pi.StepsAndWorkitems) {
                                approvalWrapper wrapper = new approvalWrapper();
                                wrapper.status = pih.StepStatus;
                                wrapper.sequenceNo = i;
                                wrapper.comments = pih.Comments;
                                wrapper.requestDate = pih.SystemModstamp;
                                wrapper.actualApprover = pih.Actor.Name;
                                wrapper.originalApprover = pih.OriginalActor.Name;
                                if(pih.StepStatus == 'Approved' || pih.StepStatus == 'Reassigned') {
                                    if (mpUsers.containsKey (pih.ActorId)) {
                                        wrapper.approverEmail = mpUsers.get(pih.ActorId).Email;
                                    } else {
                                        wrapper.approverEmail = '';
                                    }
                                }
                                else {
                                    if(pih.actorId == null)
                                        wrapper.approverEmail = (pih.OriginalActor.Type == 'Queue' ? mpUserEmails.get(pih.OriginalActorId): mpUsers.get(pih.OriginalActorId).email);
                                    else {
                                        if (mpusers.containsKey (pih.actorId)) {
                                            wrapper.approverEmail = mpUsers.get(pih.actorId).email;
                                        } else {
                                            wrapper.approverEmail = '';
                                        }
                                    }
                                }
                                system.debug('Approval History: ' + pih);
                                
                                details.approvalDetails.add(wrapper);
                                i++;
                            }
                        }
                    }
                    Map <ID, Sales_Margins__c> salesMargins = new Map <ID, Sales_Margins__c> ();
                    salesMargins = getSalesMargin (derId);
                    
                    for(Deal_Exception_Unit__c deUnit: derDetails.Deal_Exception_Units__r){
                        
                        deUnitsWrapper unitWrapper = new deUnitsWrapper();
                        if (deUnit.Sales_Margins__c != null) {
                            unitWrapper.salesMargin = salesMargins.get (deUnit.Sales_Margins__c);
                        }
                        unitWrapper.deUnit = deUnit;
                        if(deUnit.Payment_Plan__c != null){
                            planIdList.add(deUnit.Payment_Plan__c );
                        }
                        if(deUnit.Proposed_Payment_Plan__c != null){
                            planIdList.add(deUnit.Proposed_Payment_Plan__c );
                        }
                        Map<String, deUnitsWrapper> newMap = new Map<String, deUnitsWrapper>();
                        newMap.put('UnitName', unitWrapper);
                        details.deUnitWrapper.add(newMap);//put(deUnit.Name, unitWrapper);   
                    }
                    system.debug('planIdList: ' + planIdList);
                    String planQuery = 'SELECT ' + '(SELECT  ' + getAllFields ('Payment_Terms__c') 
                                        + ' FROM Payment_Terms__r ORDER BY Sequence_Number__C ASC, Installment__c ASC),'
                                + getAllFields ('Payment_Plan__c') + ' FROM Payment_Plan__c WHERE Id IN: planIdList' ;
                    system.debug('planQuery: ' + planQuery);
                    for(Payment_Plan__c plan: Database.query (planQuery)){
                        planMap.put(plan.Id, plan);
                    }
                    system.debug('planMap: ' + planMap);
                  //  system.debug('details.deUnitWrapper.values(): ' + details.deUnitWrapper.values());
                   // for(deUnitsWrapper unitWrapper: details.deUnitWrapper/*.values()*/){
                    for(Map<String, deUnitsWrapper> newMap: details.deUnitWrapper){
                        deUnitsWrapper unitWrapper = newMap.get('UnitName');
                        if(unitWrapper.deUnit.Payment_Plan__c != null 
                                && planMap.containsKey(unitWrapper.deUnit.Payment_Plan__c) 
                                && planMap.get(unitWrapper.deUnit.Payment_Plan__c) != null){
                            unitWrapper.paymentPlan = planMap.get(unitWrapper.deUnit.Payment_Plan__c);
                        }
                        if(unitWrapper.deUnit.Proposed_Payment_Plan__c != null 
                                && planMap.containsKey(unitWrapper.deUnit.Proposed_Payment_Plan__c) 
                                && planMap.get(unitWrapper.deUnit.Proposed_Payment_Plan__c) != null){
                            unitWrapper.proposedPaymentPlan = planMap.get(unitWrapper.deUnit.Proposed_Payment_Plan__c);
                        }
                    }
                    resp.details = details;
                    
                } else{
                    resp.statusCode = '400';
                    resp.statusMessage = 'Unable to find Deal Exception Request for the given Id';
                }
            } 
            else{
                String derQuery = 'SELECT Agency__r.name, Agent__r.Name, inquiry__r.name, Account__r.name,'// Status__c, '
                            + 'CreatedBy.Name, RM__r.Name, DOS__r.Name, HOS__r.Name, HOD__r.Name, HOD__r.email,'
                            + ' (SELECT ID FROM ProcessInstances  ORDER BY CreatedDate DESC), '
                            + getAllFields ('Deal_Exception_Request__c') 
                            + ' FROM Deal_Exception_Request__c WHERE Status__c in: status2consider ORDER BY LastModifiedDate DESC';
                system.debug('derQuery: ' + derQuery);
                List<Deal_Exception_Request__c> derList = Database.query (derQuery);

                Map<String, String> processInstanceIDMap = new Map<String, String>();
                Map<Id, Deal_Exception_Request__c> derMap = new Map<Id, Deal_Exception_Request__c>();
                Map<String, List<approvalWrapper>> approvalMap = new Map<String, List<approvalWrapper>>();
                for(Deal_Exception_Request__c der: derList){
                    for(ProcessInstance pi :der.ProcessInstances) {
                        processInstanceIDMap.put(pi.Id, der.Id);
                    }
                    /*
                    for(SObjectField f: Deal_Exception_Request__c.sobjecttype.getdescribe().fields.getmap().values()) {
                        try{
                            if(String.valueOf(der.get(f)) != null && String.valueOf(der.get(f)) != '' ){
                            
                            }
                            else{
                                der.put(f, null); 
                            }
                        }
                        catch(exception e){
                            system.debug('exception: ' + e.getMessage());
                        }
                    }
                    system.debug('der: ' + der);
                    */
                }
                Set <ID> userIds = new Set <ID> ();
                List <processInstance> processInstances = new List <ProcessInstance> ();
                processInstances = [SELECT Id, TargetObjectId, LastActorId, LastActor.Name, CompletedDate, Status, 
                                    (SELECT Id, ActorId, Actor.Name, OriginalActor.Type, OriginalActor.Name, Comments, 
                                     StepStatus, IsPending, ProcessInstanceId, SystemModstamp
                                     FROM StepsAndWorkitems 
                                     WHERE StepStatus != 'Started' 
                                     ORDER BY Id DESC, CreatedDate DESC), 
                                    (SELECT Id, StepStatus, Comments FROM Steps ORDER BY CreatedDate DESC LIMIT 1 )
                                    FROM ProcessInstance 
                                    WHERE Id IN : processInstanceIDMap.keyset() ORDER BY CreatedDate DESC];
                
                //1.1 begin
                map<id, string> mpUserEmails = new map<id, string>(); 
                map<id, User> mpUsers = new map<id, User>();
                set<Id> queueIds = new set<Id>();
                set<Id> otherUserIds = new set<Id>();
                set<id> approverUserIds = new set<Id>();
                for (ProcessInstance pi :processInstances){
                    if (pi.StepsAndWorkitems.size() > 0){                        
                        for(ProcessInstanceHistory pih :pi.StepsAndWorkitems) {
                            userIds.add(pih.ActorId);
                            if(pih.OriginalActor.Type == 'Queue')
                                queueIds.add(pih.OriginalActorId);
                            else
                                otherUserIds.add(pih.OriginalActorId);
                        }
                    }
                } 

                for(GroupMember gm :[Select Id, UserOrGroupId from GroupMember where Group.type='Queue' and GroupId in: QueueIds]){
                    approverUserIds.add(gm.UserOrGroupId);
                }
                
                mpUsers = new map<id, User>([Select id, email from user where id in: approverUserIds or id in: userIds or id in: otherUserIds]);
                for(GroupMember gm :[Select Id, UserOrGroupId, groupId from GroupMember where Group.type='Queue' and GroupId in: QueueIds]){
                    if(mpUsers.containskey(gm.UserOrGroupId))
                        mpUserEmails.put(gm.groupId, mpUserEmails.containsKey(gm.groupId) ? mpUserEmails.get(gm.groupId)+','+mpUsers.get(gm.UserOrGroupId).Email : mpUsers.get(gm.UserOrGroupId).Email);
                }

                for (User U : [SELECT Name, Email FROM User WHERE Id In :UserIds]) {
                    details.approverUsers.add (u);
                }
                
                //1.1 End
                for (ProcessInstance pi : processInstances) {
                    system.debug('Approval Process: ' + pi);
                    List<approvalWrapper> approvalList = new  List<approvalWrapper>();
                    String deRecordId =  processInstanceIDMap.get(pi.Id); 
                    if(approvalMap.containsKey(deRecordId)){
                        approvalList = approvalMap.get(deRecordId);
                    }                  
                    if (pi.StepsAndWorkitems.size() > 0) {
                        integer i = 1;
                        for(ProcessInstanceHistory pih :pi.StepsAndWorkitems) {
                            approvalWrapper wrapper = new approvalWrapper();
                            wrapper.status = pih.StepStatus;
                            wrapper.comments = pih.Comments;
                            wrapper.sequenceNo = i;
                            wrapper.requestDate = pih.SystemModstamp;
                            wrapper.actualApprover = pih.Actor.Name;
                            wrapper.originalApprover = pih.OriginalActor.Name;
                            system.debug (mpusers);
                            system.debug (pih.actorId);
                            System.debug (pih);
                            
                            if(pih.StepStatus == 'Approved') {
                                if (mpUsers.containsKey (pih.actorId)) {
                                    wrapper.approverEmail = mpUsers.get(pih.ActorId).Email;
                                } else {
                                    wrapper.approverEmail = '';
                                }
                            } 
                            else {
                                if(pih.actorId == null)
                                    wrapper.approverEmail = (pih.OriginalActor.Type == 'Queue' ? mpUserEmails.get(pih.OriginalActorId): mpUsers.get(pih.OriginalActorId).email);
                                
                                else {
                                    if (mpUsers.containsKey (pih.actorId)) {
                                        wrapper.approverEmail = mpUsers.get(pih.actorId).email;
                                    } else {
                                        wrapper.approverEmail = '';
                                    }
                                }    
                            }     
                            system.debug('Approval History: ' + pih);

                            approvalList.add(wrapper);
                            i++;
                        }
                        approvalMap.put(deRecordId, approvalList);
                    }
                }
                List<derListWrapper> derListWrapper = new List<derListWrapper>();
                for(Deal_Exception_Request__c der: derList){
                    derListWrapper wrapper = new derListWrapper();
                    wrapper.der = der;
                    wrapper.approvalDetails = approvalMap.get(der.Id);
                    derListWrapper.add(wrapper);
                }
                resp.derListWrapper = derListWrapper;
                if( derList.size() > 0){
                    resp.statusCode = '200';
                    resp.statusMessage = 'Fetched Deal Exception Requests Successfully';
                } else{
                    resp.statusCode = '400';
                    resp.statusMessage = 'No Deal Exception Requests Awaiting Management Approval';
                }
            }
        } catch( exception e){
            resp.statusCode = '500';
            resp.statusMessage = 'Internal Error while fetching Requests - ' + e.getMessage()+e.getLineNumber();
        }
        return resp;
    }
    
    @HttpPost
    global static PostResponseWrapper doPost(){
        RestRequest request = RestContext.request;
        PostResponseWrapper resp = new PostResponseWrapper();
        RestResponse response = RestContext.response;
        response.addHeader('Content-Type', 'application/json');
        String jsonBody = request.requestBody.toString();
        system.debug('jsonBody: ' + jsonBody);
        PostRequestWrapper reqst = (PostRequestWrapper) JSON.deserialize(jsonBody, PostRequestWrapper.class);   
        try{
            resp.deRecord = getderRecord(reqst.requestId);
            system.debug('resp.deRecord: ' + resp.deRecord);
            system.debug('reqst.checkListItems: ' + reqst.checkListItems);
            if(resp.deRecord != null){
                String action = '';
                system.debug('Request comments: ' + reqst.comments);
                system.debug('Request action: ' + reqst.action);
                if(status2Consider.contains(resp.deRecord.Status__c)){
                    if(reqst.action.toLowerCase() == 'approve'){
                        action = 'Approved';
                    }
                    if(reqst.action.toLowerCase() == 'reject'){
                        action = 'Rejected';
                    }
                    system.debug('action: ' + action);
                    if(action == 'Approved' || action == 'Rejected'){
                        try{
                            if(action == 'Approved' && reqst.checkListItems != null){
                                for(Checklist_Item__c items: reqst.checkListItems){
                                    items.Deal_Exception_Request__c = reqst.requestId;
                                    items.Name = String.valueOf(Integer.valueOf(items.Priority__c));
                                    if(items.Checklist_Value__c == 'Yes'){
                                        items.Validated__c = true;
                                    }
                                }
                                if(reqst.checkListItems.size() > 0){
                                    insert reqst.checkListItems;
                                }
                            }
                            Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
                            if(action == 'Approved'){
                                req.setAction('Approve');
                            } else{
                                req.setAction('Reject');
                            }
                            req.setComments(reqst.comments);
                            ProcessInstanceWorkitem pItem = [SELECT Id FROM ProcessInstanceWorkitem 
                                                             WHERE ProcessInstance.TargetObjectId =: resp.deRecord.id];
                            req.setWorkitemId(pItem.Id);
                            Approval.ProcessResult result = Approval.process(req);
                            system.debug('result: ' + result);
                            String comments = '';
                            if(resp.deRecord.Management_Comments__c != null && resp.deRecord.Management_Comments__c != ''){
                                comments = resp.deRecord.Management_Comments__c + '\n\n';
                            }
                            comments += reqst.comments;
                            system.debug('comments: ' + comments);
                            Deal_Exception_Request__c der = new Deal_Exception_Request__c();
                            der.Id = resp.deRecord.Id;
                            der.Management_Comments__c = comments;
                            update der;
                            resp.deRecord = getderRecord(reqst.requestId);
                            resp.statusCode = '200';
                            resp.statusMessage = action + ' Deal Exception Request ' + resp.deRecord.Name + ' Successfully';
                        }  catch( exception e){
                            resp.statusCode = '500';
                            resp.statusMessage = 'Internal Error while Approving/Rejecting Request - ' + e.getMessage();
                        }
                    } else{
                        resp.statusCode = '400';
                        resp.statusMessage = 'Wrong value for Action. Provide Approve/Reject';
                    }
                } else{
                    resp.statusCode = '400';
                    resp.statusMessage = 'Deal Exception Request ' + resp.deRecord.Name 
                            + ' is not available for Approval. [Status: ' + resp.deRecord.Status__c + ']';
                }
            } else{
                resp.statusCode = '404';
                resp.statusMessage = 'Unable to find Deal Exception Request for the given Id';
            }
        } catch( exception e){
            resp.statusCode = '500';
            resp.statusMessage = 'Internal Error while fetching Requests - ' + e.getMessage();
        }
        return resp;
    }
    
    public static Map <ID, Sales_Margins__c> getSalesMargin (String derRecordId) {
        Set <ID> marginIds = new Set <ID> ();
        for (Deal_Exception_Unit__c unit : [SELECT Sales_Margins__c FROM Deal_Exception_Unit__c WHERE Deal_Exception_Request__c =: derRecordId]) {
            marginIds.add (unit.Sales_Margins__c);
        }
    
        Map <ID, Sales_Margins__c> salesMargins = new Map <ID, Sales_Margins__c> ();
        for (Sales_Margins__c margin : Database.Query ('SELECT '+getAllFields ('Sales_Margins__c')+' FROM Sales_Margins__c WHERE ID IN: marginIds')) {
            salesMargins.put (margin.Id, margin);
        }
        return salesMargins;
    }
    
    public static Deal_Exception_Request__c getderRecord(String derRecordId){
        String query = 'SELECT Agency__r.name, Agent__r.Name, inquiry__r.name, Account__r.name, '
                      + 'CreatedBy.Name, RM__r.Name, DOS__r.Name, HOS__r.Name, HOD__r.Name, HOD__r.Email, '
                      
                      + getAllFields ('Deal_Exception_Request__c') + ' FROM Deal_Exception_Request__c WHERE id=: derRecordId';
        system.debug('query: ' + query);
        Deal_Exception_Request__c derRecord = Database.query (query);
        return derRecord;
    }

    public static Deal_Exception_Request__c getderRecordDetails(String derRecordId){
        String query = 'SELECT Agency__r.name, Agent__r.Name, inquiry__r.name, Account__r.name, '
                      + 'CreatedBy.Name, RM__r.Name, DOS__r.Name, HOS__r.Name, HOD__r.Name, HOD__r.Email,'
                      + '(SELECT  ' + getAllFields ('Deal_Exception_Unit__c') 
                      + ', Inventory__r.Unit_Name__c, Inventory__r.Conn_Project_Code__c, Inventory__r.Building_Name__c, Inventory__r.Unit_Location__c, inventory__r.Area__c '
                     // + ', Proposed_Payment_Plan__r.Name, Payment_Plan__r.Name '
                      + ' FROM Deal_Exception_Units__r), '
                      + getAllFields ('Deal_Exception_Request__c') + ' FROM Deal_Exception_Request__c WHERE id=: derRecordId';
        system.debug('query: ' + query);
        Deal_Exception_Request__c derRecord = Database.query (query);
        return derRecord;
    }

    public static string getAllFields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        if (objectType == null) {
            return fields;
        }
        Map <String, Schema.SObjectField> fieldMap = objectType.getDescribe ().fields.getMap ();
        for (Schema.SObjectField sfield : fieldMap.Values()) {
            fields += sfield.getDescribe().getName ()+ ', ';
        }
        return fields.removeEnd(', ');
    }

    global class GetResponseWrapper{
        public String StatusCode;
        public String StatusMessage;
        public String requestType;
        List<Deal_Exception_Request__c> derList;
        List<derListWrapper> derListWrapper;
        detailWrapper details;
        global GetResponseWrapper(){
            requestType = 'Deal Exception';
        }
    }
    
    global class derListWrapper{
        public List<approvalWrapper> approvalDetails;
        public Deal_Exception_Request__c der;
    }

    global class detailWrapper{
        List<approvalWrapper> approvalDetails;
        List<User> approverUsers;
        //Map<String, deUnitsWrapper> deUnitWrapper;
       // List<deUnitsWrapper> deUnitWrapper;
        List<Map<String, deUnitsWrapper>> deUnitWrapper;
      //  List<checkListWrapper> checkListItems;
        List<Checklist_Items__c> checkListItemsList;
    }
    
    global class checkListWrapper{
        public Integer slNo;
        public String value;
        public Boolean isTextArea;
    }

    global class approvalWrapper{
        public DateTime requestDate;
        public integer sequenceNo;
        public String status;
        public String originalApprover;
        public String actualApprover;
        public String comments;
        public string approverEmail;
    }

    global class deUnitsWrapper{
        Deal_Exception_Unit__c deUnit;
        Sales_Margins__c salesMargin;
        Payment_Plan__c paymentPlan;
        Payment_Plan__c proposedPaymentPlan;
        global deUnitsWrapper(){
            deUnit = new Deal_Exception_Unit__c();
            salesMargin = new Sales_Margins__c ();
            paymentPlan = new Payment_Plan__c();
            proposedPaymentPlan = new  Payment_Plan__c();
        }
    }

    global class PostRequestWrapper{
        public String requestId;
        public String action;
        public String comments;
        List<Checklist_Item__c> checkListItems;
    }

    global class PostResponseWrapper{
        public String StatusCode;
        public String StatusMessage;
        public String requestType;
        Deal_Exception_Request__c deRecord;
        
        global PostResponseWrapper(){
            requestType = 'Deal Exception';
        }
    }
}