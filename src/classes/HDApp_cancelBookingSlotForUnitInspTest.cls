@isTest
public class HDApp_cancelBookingSlotForUnitInspTest {
     @isTest
    static void testCancelAppointment(){
    
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/cancelBookingForUnitInsp'; 
        req.httpMethod = 'POST';
        req.addParameter('callingListId', '');
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
            HDApp_cancelBookingSlotForUnitInsp_API.cancelBookingSlotForUnit();
        Test.stopTest();
    }
    
    @isTest
    static void testCancelAppointment2(){
    
    TriggerOnOffCustomSetting__c objTrigger = new TriggerOnOffCustomSetting__c();
    objTrigger.Name = 'CallingListTrigger';
    objTrigger.OnOffCheck__c = true;
    insert objTrigger ;
    
    Calling_List__c objCallingList = new Calling_List__c();
    objCallingList.Appointment_Status__c = 'Cancelled';
    insert objCallingList;
    
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/cancelBookingForUnitInsp';  
        req.httpMethod = 'POST';
        req.addParameter('callingListId', objCallingList.Id);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
            HDApp_cancelBookingSlotForUnitInsp_API.cancelBookingSlotForUnit();
        Test.stopTest();
    }
    
    
    
    @isTest
    static void testCancelAppointment3(){
    
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/cancelBookingForUnitInsp';  
        req.httpMethod = 'POST';
        req.addParameter('callingListId', 'test');
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
            HDApp_cancelBookingSlotForUnitInsp_API.cancelBookingSlotForUnit();
        Test.stopTest();
    }
    
    
    @isTest
    static void testCancelAppointment4(){
    
    TriggerOnOffCustomSetting__c objTrigger = new TriggerOnOffCustomSetting__c();
    objTrigger.Name = 'CallingListTrigger';
    objTrigger.OnOffCheck__c = true;
    insert objTrigger ;
    
    Calling_List__c objCallingList = new Calling_List__c();
    objCallingList.Appointment_Status__c = 'Confirmed';
    insert objCallingList;
    
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/cancelBookingForUnitInsp';  
        req.httpMethod = 'POST';
        req.addParameter('callingListId', objCallingList.Id);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
            HDApp_cancelBookingSlotForUnitInsp_API.cancelBookingSlotForUnit();
        Test.stopTest();
    }
}