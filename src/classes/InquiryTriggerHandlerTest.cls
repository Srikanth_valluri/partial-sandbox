/*
* Name : Pavithra Gajendra
* Date : 02/08/2017
* Purpose : Test class for Inquiry Trigger handler 
* Company : NSI Gulf
* 
*/
@isTest(seeAllData=false)
private class InquiryTriggerHandlerTest {
    
    
    //Commented code from line 73-241 for lead prioratization deployment
    @isTest static void updateExistingInquiryTest()
    {
        Test.startTest ();
            
            
            String digitalRecordType=DamacUtility.getRecordTypeId('Campaign__c', 'Digital');
            Id agenTeamRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
            Campaign__c campaignObject = new Campaign__c( Lead_Prioritization__c = 'Priority Digital', RecordTypeId=digitalRecordType,Campaign_Type_New__c='Social',Campaign_Category_New__c='Facebook',Campaign_Sub_Category_1__c='Organic',End_Date__c=date.parse('11/10/18'), Marketing_End_Date__c=date.parse('11/10/18'), Marketing_Start_Date__c=date.parse('11/10/16'), Start_Date__c=date.parse('11/10/16'));
            insert campaignObject;
            Lead_Prioritization_Configuration__c leadPriorityObject=new Lead_Prioritization_Configuration__c(Campaign_Type__c='Social',Campaign_Category__c='Facebook',Campaign_Sub_Category_1__c='Organic',Class__c='A-Class',isActive__c = true);
            insert leadPriorityObject;
            Inquiry__c inqObj= new Inquiry__c(Is_Meeting_Scheduled__c= true, Meeting_Due_Date__c = Date.today (), Campaign__c=campaignObject.Id,RecordTypeId=agenTeamRT,Pre_InquiryId__c='123456',Mobile_Phone_Encrypt__c='456123',Mobile_CountryCode__c='American Samoa: 001684',Mobile_Phone__c='1234',Email__c='mk@gmail.com',First_Name__c='Test',Last_Name__c='Last',CR_Number__c='0987',/*ORN_Number__c='7842',Agency_Type__c='Corporate',isDuplicate__c=false,*/Organisation_Name__c = 'Oliver');
            insert inqObj;
            
            PCInquiryCounter__c pcObj = new PCInquiryCounter__c();
            pcObj.Name=inqObj.id;
            pcObj.All_Inquiry_Count__c=4;
            pcObj.New_Inquiry_Count__c=2;
            insert pcObj;
            
            Map<Id, List<User>> userPreferredLanguageMap = new Map<Id, List<User>> ();
            ID userId = Userinfo.getUserId ();
            User u = [SELECT Name from User WHERE ID =:userId ];
            userPreferredLanguageMap.put (campaignObject.id, new List<USer> {u});
            
            InquiryTriggerHandler obj = new InquiryTriggerHandler ();
            obj.getUserWithLoad (userPreferredLanguageMap);
        Test.stopTest ();
    }
}