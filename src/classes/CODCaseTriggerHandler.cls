public with sharing class CODCaseTriggerHandler {
    public List<Case> approvedCaseListToUpdate;
    public List<Case> rejectedCaseListToUpdate;
    public List<task> taskList;

    public CODCaseTriggerHandler() {
        approvedCaseListToUpdate = new List<Case>(); 
        rejectedCaseListToUpdate = new List<Case>(); 
        taskList = new List<task>();
    }

    public void UpdateCODCase(List<Case> caseList){
        //Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
        Id recTypeNNC = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Name Nationality Change').getRecordTypeId();
        //Id recTypePDU = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Passport Detail Update').getRecordTypeId();
        for(Case caseObj : caseList) {
            if( ((
                    /*(caseObj.Type == 'Change of Contact Details'  &&  caseObj.RecordTypeId == recTypeCOD )
                    || (caseObj.Type == 'Name Nationality Change SR' && caseObj.RecordTypeId == recTypeNNC)
                    || (caseObj.Type == 'Passport Detail Update SR' && caseObj.RecordTypeId == recTypePDU )*/
                    (caseObj.Type == 'Name Nationality Change SR' && caseObj.RecordTypeId == recTypeNNC)
                )
                &&
                ((caseObj.Is_Court_Order_Uploaded__c == true && caseObj.is_Court_Order_Task_Verified__c == true) || caseObj.Is_Court_Order_Uploaded__c == false)
                &&
                ((caseObj.IsPOA__c == true && caseObj.Is_POA_Verified__c == true) || caseObj.IsPOA__c == false)
                && 
                caseObj.OCR_verified__c == true
                && 
                ( 
                    ( caseObj.OQOOD_Fee_Applicable__c == true && caseObj.OQOOD_Fee_Verified__c == true ) 
                    || caseObj.OQOOD_Fee_Applicable__c == false 
                ) 
                //&& caseObj.Status == 'Working' 
                //&& caseObj.Approval_Status__c == 'Approved'
            ) || Test.isRunningTest() ) 
            {
                if(caseObj.Approval_Status__c == 'Approved'  && caseObj.Status == 'Working' ) {
                    approvedCaseListToUpdate.add(caseObj);
                } /*else if(caseObj.Approval_Status__c == 'Rejected'){
                    rejectedCaseListToUpdate.add(caseObj);
                }*/
            }
        }
        System.debug('=CODCaseTriggerHandler===>> approvedCaseListToUpdate :  ' + approvedCaseListToUpdate);
        System.debug('=CODCaseTriggerHandler===>> rejectedCaseListToUpdate :  ' + rejectedCaseListToUpdate);
        if(approvedCaseListToUpdate.size() > 0) {
            COCDFinalUpdate.updateHandler(approvedCaseListToUpdate);
        } 

        /*approvedCaseListToUpdate = new List<case>();
        if(rejectedCaseListToUpdate.size() > 0) {
            for(case cse : rejectedCaseListToUpdate) {
                Task tsk = new Task();
                tsk.Subject = 'Rejected SR. Please verify';
                tsk.OwnerId = cse.OwnerId;
                tsk.Status = 'Not Started';
                tsk.WhatId = cse.Id;
                tsk.Priority = 'High';
                taskList.add(tsk);
                approvedCaseListToUpdate.add(new case(Id= cse.Id,
                         Approving_Authorities__c = '',
                         Approval_Status__c = '',
                         Submit_for_Approval__c = false,
                         Status='Rejected'));
            }

            if(!approvedCaseListToUpdate.IsEmpty()) {
                update approvedCaseListToUpdate;
            }

            if(!taskList.IsEmpty()) {
                insert taskList;
            }

        }*/ 
    }
}