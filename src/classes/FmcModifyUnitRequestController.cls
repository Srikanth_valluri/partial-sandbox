public without sharing class FmcModifyUnitRequestController {

    public  List<SelectOption>          units                   {get; set;}
    public  FM_Request__c               req                     {get; set;}

    private static final String SR_SUBMIT_SUCCESS = 'Thank you for submitting the details. You will be contacted shortly by us!';
    private static final String GENERIC_SAVE_ERROR = 'There was an error while processing your request. Please try again later.';
    private static final String SELECT_UNIT_MESSAGE = 'Please select a unit.';

    private Id modifyUnitRecordTypeId = Schema.SObjectType.FM_Request__c.getRecordTypeInfosByName()
                                        .get('Modify Unit').getRecordTypeId();
    private Map<Id, Booking_Unit__c>    mapUnits                {get; set;}
    private Id                          customerAccountId   =   CustomerCommunityUtils.customerAccountId;
    private Account customerAccount;

    public FmcModifyUnitRequestController() {
        if (FmcUtils.isCurrentView('RequestUnitModification')) {
            initializeVariables();
        }
    }

    public void initializeVariables() {
        units = new List<SelectOption>();
        mapUnits = new Map<Id, Booking_Unit__c>();
        customerAccountId = CustomerCommunityUtils.customerAccountId;
        customerAccount = [
            SELECT  Id, Name, Party_Type__c, IsPersonAccount, Email__pc, Email__c
                    , Mobile_Phone_Encrypt__pc, Mobile__c, Mobile_Country_Code__c, Mobile_Country_Code__pc
            FROM    Account
            WHERE   Id = :customerAccountId];
        for (Booking_Unit__c unit : FmcUtils.queryUnitsForAccount(
            customerAccountId,
            'Id, Name, Unit_Name__c, Booking__r.Account__c, Inventory__c, Inventory__r.Building_Location__c, '
                + 'Inventory__r.Building_Location__r.Id, Inventory__r.Building_Location__r.Name, '
                + 'Inventory__r.Building_Location__r.Building_Name__c'
        )) {
            units.add(new SelectOption(unit.Id, unit.Unit_Name__c));
            mapUnits.put(unit.Id, unit);
        }

        req = new FM_Request__c(
            Source__c = 'LOAMS Portal',
            RecordTypeId = modifyUnitRecordTypeId,
            Request_Type__c = 'Modify Unit',
            Account__c = customerAccountId,
            Email__c = customerAccount.IsPersonAccount ? customerAccount.Email__pc : customerAccount.Email__c,
            Mobile_Country_Code__c = customerAccount.IsPersonAccount ? customerAccount.Mobile_Country_Code__pc : customerAccount.Mobile_Country_Code__c,
            Mobile_no__c = customerAccount.IsPersonAccount ? customerAccount.Mobile_Phone_Encrypt__pc : customerAccount.Mobile__c
        );
    }

    public void submitRequest() {

        Booking_Unit__c unit = mapUnits.get(req.Booking_Unit__c);

        if (unit == NULL) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, SELECT_UNIT_MESSAGE));
            return;
        }

        Map<String, Id> mapFmUsersByRole =  FM_Utility.getFmUsersByLocationAndRole(
            new Set<Id> {unit.Inventory__r.Building_Location__c}
        ).get(unit.Inventory__r.Building_Location__c);
        System.debug('mapFmUsersByRole = ' + mapFmUsersByRole);
        if (mapFmUsersByRole == NULL || mapFmUsersByRole.isEmpty()) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, GENERIC_SAVE_ERROR));
            return;
        }

        Map<Id, User> userDetails = new Map<Id, User>([
            SELECT  Id, Email
            FROM    User
            WHERE   Id IN :mapFmUsersByRole.values()
        ]);

        User fmManager          = userDetails.get(mapFmUsersByRole.get('FM Manager'));
        User propertyManager    = userDetails.get(mapFmUsersByRole.get('Property Manager'));

        if (fmManager == NULL && propertyManager == NULL) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, GENERIC_SAVE_ERROR));
            return;
        }

        req.Status__c = 'Submitted';
        req.Source__c = 'LOAMS Portal';
        req.RecordTypeId = modifyUnitRecordTypeId;
        req.Request_Type__c = 'Modify Unit';
        req.Account__c = customerAccountId;
        req.Email__c = customerAccount.IsPersonAccount ? customerAccount.Email__pc : customerAccount.Email__c;
        req.Mobile_Country_Code__c = customerAccount.IsPersonAccount ? customerAccount.Mobile_Country_Code__pc : customerAccount.Mobile_Country_Code__c;
        req.Mobile_no__c = customerAccount.IsPersonAccount ? customerAccount.Mobile_Phone_Encrypt__pc : customerAccount.Mobile__c;
        req.FM_Manager_Email__c         = fmManager == NULL         ? NULL : fmManager.Email;
        req.Property_Manager_Email__c   = propertyManager == NULL   ? NULL : propertyManager.Email;

        try {
            upsert req;
        } catch(Exception excp) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, GENERIC_SAVE_ERROR));
            return;
        }
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, SR_SUBMIT_SUCCESS));
    }

}