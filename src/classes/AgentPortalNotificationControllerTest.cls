/**************************************************************************************************
* Name               : AgentPortalNotificationController
* Description        :                                               
* Created Date       : Naresh(Accely)                                                                       
* Created By         : 6/09/2017                                                                 
* Last Modified Date :                                                                            
* Last Modified By   :                                                                            
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR                    DATE                                                                    
* 1.1         Naresh kaneriya      6/09/2017                                                             
**************************************************************************************************/
@isTest(SeeAllData=false)
public class AgentPortalNotificationControllerTest{
 


public static testMethod void PortalNotificationTest(){

ApexPages.currentPage().getParameters().put('nid','nid');
ApexPages.currentPage().getParameters().put('notificationId','01p9E0000001crl');

AgentPortalNotificationController  obj = new AgentPortalNotificationController ();
obj.getNotification();
obj.viewNotificationDetail();
obj.viewCurrentNotificationDetail();
obj.readNotification('01p9E0000001crl');


}


}