/**
 * Description: Test class for ParkingOfferAcceptanceLetterController
 */
@isTest
private class ParkingOfferAcceptanceLetterTest {

    /*
     * Description : Test method to check whether the batch is called or not which generates the Drawloop document.
     */
    @isTest static void testOfferAcceptanceLetter() {

        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        Id parkingRecordTypeID = getRecordTypeIdForParking();

        // Creation of Case
        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , parkingRecordTypeID);
        insert objCase;

        objCase = [Select OwnerId FROM Case WHERE Id =:objCase.Id];

        // Creation of Task
        Task objTask = new Task();
        objTask.subject = 'Generate Parking Offer & Acceptance Letter';
        objTask.whatId = objCase.Id;
        objTask.status = 'In Progress';
        insert objTask;

        PageReference redirectionUrl;
        Test.startTest();
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(objCase);
            ParkingOfferAcceptanceLetterController objController = new ParkingOfferAcceptanceLetterController(standardControllerInstance);
            Test.setCurrentPageReference(new PageReference('Page.ParkingOfferAcceptanceLetter'));
            System.currentPageReference().getParameters().put('Id', objCase.Id);
            redirectionUrl = objController.generateOfferAcceptanceLetter();
        Test.stopTest();

        // Check whether redirected to case detail page
        System.assert(redirectionUrl != null);
        System.assertEquals(redirectionUrl.getURL(), '/'+objCase.Id);
    }

    /*
     * Description : Test method to check whether the error message is shown or not.
     */
    @isTest static void testOfferAcceptanceLetterWithoutTask() {

        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        Id parkingRecordTypeID = getRecordTypeIdForParking();

        // Creation of Case
        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , parkingRecordTypeID);
        insert objCase;

        objCase = [Select OwnerId FROM Case WHERE Id =:objCase.Id];

        PageReference redirectionUrl;
        Test.startTest();
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(objCase);
            ParkingOfferAcceptanceLetterController objController = new ParkingOfferAcceptanceLetterController(standardControllerInstance);
            Test.setCurrentPageReference(new PageReference('Page.ParkingOfferAcceptanceLetter'));
            System.currentPageReference().getParameters().put('Id', objCase.Id);
            redirectionUrl = objController.generateOfferAcceptanceLetter();
        Test.stopTest();

        // Check whether error message is generated or not
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        System.assertEquals(1, pageMessages.size());
    }

    /**
     * Method to get the "Parking" record type
     */
    private static Id getRecordTypeIdForParking() {
      Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
      Id parkingRecordTypeID = caseRecordTypes.get('Parking').getRecordTypeId();
      return parkingRecordTypeID;
    }
}