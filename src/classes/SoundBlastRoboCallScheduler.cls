/*****************************************************************************************
 * Description - Scheduler of SoundBlastRoboCallApiBatch Class
 *----------------------------------------------------------------------------------------
 * Version        Date          Author              Description
 * 1.0            18/05/2020    Aishwarya Todkar    Initial Draft
 *****************************************************************************************/
global class SoundBlastRoboCallScheduler implements Schedulable {
    public void execute(SchedulableContext sc) {
        Database.executebatch(new SoundBlastRoboCallApiBatch( Label.Robo_Call_Campaign_Id), 1);
    }
}