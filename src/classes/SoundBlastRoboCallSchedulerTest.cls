@isTest
private class SoundBlastRoboCallSchedulerTest {

    static testMethod void testScheduler() {
      Test.startTest();
        String jobName = 'demo job';
        SoundBlastRoboCallScheduler ascsObject = new SoundBlastRoboCallScheduler ();
      String sch = '0 0 23 * * ?'; 
      system.schedule(jobName+System.Now(), sch, ascsObject); 
    Test.stopTest();
    }
}