@isTest
private class CreateCilControllerTest {
    @testSetup static void setupData() {
        AP_Permission_Setup__c aps = new AP_Permission_Setup__c();
        aps.SetupOwnerId = [select Id from Profile where name='Customer Community - Agent'].Id;
        aps.Book_Unit__c = true;
        aps.Create_A_CIL__c = true;
        aps.Create_EOI__c = true;
        aps.Edit_SR__c = true;
        aps.Support_Ticket__c = true;
        aps.Agent_Filter__c = true;
        aps.Enter_Buyer__c = true;
        insert aps;
        
        AP_Permission_Setup__c aps1 = new AP_Permission_Setup__c();
        aps1.SetupOwnerId = UserInfo.getOrganizationID();
        aps1.Book_Unit__c = true;
        aps1.Create_A_CIL__c = true;
        aps1.Create_EOI__c = true;
        aps1.Edit_SR__c = true;
        aps1.Support_Ticket__c = true;
        aps1.Agent_Filter__c = true;
        aps1.Enter_Buyer__c = true;
        insert aps1;

        Location__c loc = InitializeSRDataTest.createLocation('123','Building');
        loc.Property_ID__c = '123';
        insert loc; 

        Payment_Plan__c pp = new Payment_Plan__c();        
        pp.Effective_From__c = system.today();
        pp.Effective_To__c = system.today(); 
        pp.Building_Location__c =  loc.id;  
        pp.term_id__c = '1234';   
        insert pp;   

        Property__c newProperty = new Property__c();
        newProperty.Property_ID__c  = 1;
        newProperty.Property_Code__c    = 'VIR' ;
        newProperty.Property_Name__c    = 'VIRIDIS @ AKOYA OXYGEN' ;
        newProperty.District__c = 'AL YUFRAH 2' ;
        newProperty.AR_Transaction_Type__c  = 'INV VIR' ;
        newProperty.Penalty_Transaction_Type__c = 'DM-PENALTIES-VIR' ;
        newProperty.Brokerage_Distribution_Set__c   = '11600' ;
        newProperty.Sales_Commission_Dist_Set__c    = '11601' ;
        newProperty.Currency_Of_Sale__c = 'AED' ;
        newProperty.Signature_Col_Customer_Stmt__c  = 'Front Line Investment Management Co. LLC' ;
        newProperty.EOI_Enabled__c = true;
        insert newProperty; 

        Inventory__c invent = new Inventory__c();
        invent.CM_Price_Per_Sqft__c = 20;
        invent.Inventory_ID__c = '1';
        invent.Building_ID__c = '1';
        invent.Floor_ID__c = '1';
        invent.Marketing_Name__c = 'Damac Heights';
        invent.Address_Id__c = '1' ;
        invent.EOI__C = NULL;
        invent.Tagged_to_EOI__c = false;
        invent.Is_Assigned__c = false;
        invent.Property_ID__c = newProperty.ID;
        invent.Property__c = newProperty.Id;
        invent.Status__c = 'Released';
        invent.special_price__c = 1000000;
        invent.CurrencyISOCode = 'AED';
        invent.Property_ID__c = '1234'; 
        invent.Floor_Package_ID__c = ''; 
        invent.building_location__c = loc.id;
        invent.property_id__c = newProperty.id;
        invent.Unit__c = 'Test Unit';
        invent.Bedroom_Type__c = 'STD';
        invent.Property_Name__c = newProperty.Property_Name__c;
        
        insert invent;

        List<NSIBPM__Status__c> createStatus = new List<NSIBPM__Status__c>();
        createStatus = InitialiseTestData.createStatusRecords(
            new List<NSIBPM__Status__c>{
                new NSIBPM__Status__c(NSIBPM__Code__c = 'AWAITING_ADDITIONAL_INFO', Name = 'AWAITING_ADDITIONAL_INFO'),
                    new NSIBPM__Status__c(NSIBPM__Code__c = 'MORE_INFO_UPDATED', Name = 'MORE_INFO_UPDATED'),
                    new NSIBPM__Status__c(NSIBPM__Code__c = 'AWAITING_FFA_AA', Name = 'AWAITING_FFA_AA'),
                    new NSIBPM__Status__c(NSIBPM__Code__c = 'FFA_AA_UPLOADED', Name = 'FFA_AA_UPLOADED')
                }
            );

        NSIBPM__SR_Steps__c srStep= new NSIBPM__SR_Steps__c();
        insert srStep;

        List<NSIBPM__SR_Template__c> SRTemplateList =  
            InitialiseTestData.createTestTemplateRecords(
                new List<NSIBPM__SR_Template__c>{
                    new NSIBPM__SR_Template__c()
                }
            );

        Account acc = new Account ();
        acc.Name = 'test';
        acc.Agency_Type__c = 'Corporate';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        //acc.Temp_Vendor_Id__c = 'sdcdcvsdfvdfsvfv';
        insert acc;  

        EOI_Process__c eoObj = new EOI_Process__c();
        eoObj.No_of_Units__c = 'Single Unit – Token AED 40,000';
        eoObj.Agency__c =   acc.id;
        eoObj.Mode_of_Token_Payment__c = 'Cash';
        eoObj.Property__c = newProperty.id;
        insert eoObj;

        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);        
        sr.Eligible_to_Sell_in_Dubai__c = true;
        sr.Agency_Type__c = 'Individual';
        sr.ID_Type__c = 'Passport';
        sr.Token_Amount_AED__c = 40000;
        sr.RecordTypeId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Agent Update').getRecordTypeId();
        sr.Booking_Wizard_Level__c = null;
        sr.Agency_Email_2__c = 'test2@gmail.com';
        sr.Alternate_Agency_Email_2__c = 'test1@gmail.com';
        sr.Country_of_Sale__c = 'UAE';
        sr.Mode_of_Payment__c = 'Online_Payment';
        sr.agency__c = acc.id;
        insert sr;

        List<NSIBPM__Step__c> createStepList = InitialiseTestData.createTestStepRecords(
            new List<NSIBPM__Step__c>{
                new NSIBPM__Step__c(NSIBPM__SR__c = SR.id, 
                                    NSIBPM__Status__c = createStatus[0].Id, 
                                    NSIBPM__SR_Step__c = srStep.id
                    ),
                new NSIBPM__Step__c(NSIBPM__SR__c = SR.id, 
                                    NSIBPM__Status__c = createStatus[1].Id, 
                                    NSIBPM__SR_Step__c = srStep.id
                )
            }
        );

        NSIBPM__Document_Master__c Dm = new NSIBPM__Document_Master__c();
        Dm.Name = 'Test DM';
        Dm.NSIBPM__Code__c = 'Bank Statement';
        Dm.NSIBPM__Document_Type_Code__c = 'Doc Type';
        insert Dm;

        //List<NSIBPM__SR_Doc__c> SRDoc_List = new List<NSIBPM__SR_Doc__c>();
        NSIBPM__SR_Doc__c srDoc = new NSIBPM__SR_Doc__c();
        srDoc = new NSIBPM__SR_Doc__c();
        srDoc.Name = 'Test SR DOC';
        srDoc.NSIBPM__Is_Not_Required__c = true;
        srDoc.NSIBPM__Status__c = 'Re-upload';
        srDoc.NSIBPM__Document_Master__c = Dm.Id;
        srDoc.NSIBPM__Service_Request__c = SR.id;

        /*Contact con = new Contact();
        con.LastName = 'testlnme';
        con.AccountId = acc.Id ;
        con.Portal_Administrator__c=true;
        insert con;*/

        List<Booking__c> lstBookings = new List<Booking__c>();
        lstBookings.add(InitializeSRDataTest.createBooking(SR.id));
        //lstBookings.add(InitializeSRDataTest.createBooking(SRList[0].id));
        lstBookings[0].Deal_SR__c = SR.id;
        lstBookings[0].Inquiry_Account_Id__c = acc.id;       
        insert lstBookings;

        Campaign__c camp = new Campaign__c();
        camp.End_Date__c = system.today().addmonths(10);
        camp.Marketing_End_Date__c = system.today().addmonths(10);
        camp.Marketing_Start_Date__c = system.today().addmonths(-10);
        camp.Start_Date__c =  system.today().addmonths(-10);
        insert camp;

        Inquiry__c inq = new Inquiry__c ();
        inq.Activity_Counter__c =101;
        inq.Inquiry_Status__c='Active';
        inq.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        inq.campaign__c = camp.id;
        inq.ownerId = userinfo.getUserId();
        insert inq;

        List<Booking_Unit__c> BUList = new List<Booking_Unit__c>();
        Booking_Unit__c bu1 = new Booking_Unit__c();
        bu1.Booking__c = lstBookings[0].id;
        bu1.Payment_Method__c = 'Cash';
        bu1.Primary_Buyer_s_Email__c = 'test@damac.com';
        bu1.Primary_Buyer_s_Name__c = 'testNSI';
        bu1.Primary_Buyer_s_Nationality__c = 'Russia';
        bu1.Inventory__c = invent.id;
        bu1.Registration_ID__c = '1234'; 
        bu1.Payment_Plan_Id__c = pp.id;     
        bu1.Requested_Price_AED__c = 500;     
        BUList.add(bu1);
        insert BUList;

        buyer__c b = new buyer__c();
        b.Buyer_Type__c =  'Individual';
        b.Address_Line_1__c =  'Ad1';
        b.Country__c =  'United Arab Emirates';
        b.City__c = 'Dubai' ;       
        b.Account__c = acc.id;
        b.dob__c = system.today().addyears(-30);
        b.Email__c = 'test@test.com';
        b.First_Name__c = 'firstname' ;
        b.Last_Name__c =  'lastname';
        b.Nationality__c = 'Indian' ;
        b.Passport_Expiry__c = system.today().addyears(20) ;
        b.Passport_Number__c = 'J0565556' ;
        b.Phone__c = '569098767' ;
        b.Phone_Country_Code__c = 'United Arab Emirates: 00971' ;
        b.Place_of_Issue__c =  'India';
        b.Title__c = 'Mr';
        b.booking__c = lstBookings[0].id;
        b.Primary_Buyer__c =true;
        b.Unique_Key__c = lstBookings[0].id+''+acc.id;
              
        insert b;

        Agent_Commission__c agentCommission = InitialiseTestData.createAgentCommission(acc.Id,System.now().Date(),System.now().Date());
        agentCommission.Booking_Unit__c = BUList[0].Id;
        insert agentCommission;

        /*Unit_Documents__c unitDoc = new Unit_Documents__c();
        unitDoc.Booking_Unit__c = BUList[0].Id;
        insert unitDoc;*/

        proof_of_payment__c objPOP = new proof_of_payment__c();
        objPOP.deal_sr__c = SR.id;
        objPOP.booking_unit__c = BUList[0].id;
        objPOP.payment_amount__c = 1432332;
        objPOP.payment_mode__c = 'Cash';
        objPOP.payment_remarks__c = 'AED';
        objPOP.currency__c = 'AED';
        objPOP.payment_date__c = system.today();
        insert objPOP;

        Receipt__c receipt = new Receipt__c();
        receipt.Amount__c = 100;
        receipt.Booking_Unit__c = BUList[0].id;
        insert receipt;

        Payment_Plan__c paymentPlanPrent = new Payment_Plan__c();
        paymentPlanPrent.TERM_ID__c='4546';
        paymentPlanPrent.Booking_Unit__c = BUList[0].id;
        insert paymentPlanPrent;

        Payment_Plan__c paymentPlan = new Payment_Plan__c();
        paymentPlan.Parent_Payment_Plan__c = paymentPlanPrent.id;
        paymentPlan.Booking_Unit__c = BUList[0].id;
        paymentPlan.Status__c = 'Active';
        insert paymentPlan;

        Payment_Terms__c pymTerms = new Payment_Terms__c();
        pymTerms.Payment_Plan__c = paymentPlan.id;
        insert pymTerms;
    }

    static testMethod void testMethod1() {

        profile profId = [SELECT Id FROM Profile WHERE Name='Standard User']; // query on Profile to fetch ProfileId
        String orgId = UserInfo.getOrganizationId();
        Profile p = [select Id from Profile where name='Customer Community - Admin'];
        //Contact con = [SELECT Id,Owner__c,Portal_Administrator__c, AccountId FROM Contact LIMIT 1];
        Property__c newProperty = [SELECT Id  FROM Property__c LIMIT 1];
        List<Booking_Unit__c> BUList = [SELECT Id From Booking_Unit__c];
        Inquiry__c inq = [SELECT Id From Inquiry__c Limit 1];
        Inventory__c invent = [SELECT Id From Inventory__c Limit 1];

         //user 
        /*User portalUser = new User(alias = 'test456', email='testusr1@test.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.id, country='United Arab Emirates',IsActive =true,
                ContactId = con.Id,timezonesidkey='America/Los_Angeles', username='portalUser1Test@test.com');
        insert portalUser;*/

        //Test.startTest();
        
        Id adminRoleId = [SELECT Id FROM UserRole WHERE DeveloperName = 'Chairman' LIMIT 1].Id;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'qw56', email='xqw@email.com',
                emailencodingkey='UTF-8', lastname='Userqw', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xqw@email.com',UserRoleId = adminRoleId);
        System.RunAs(adminUser){
            
            createCurrencyValues(2);
            createPageFlowData();
            Account adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
            insert adminAccount;
            
            Contact adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
            insert adminContact;
            Contact adminContactNew = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
            adminContactNew.Owner__c = false;
            adminContactNew.Agent_Representative__c= false;
            adminContactNew.Portal_Administrator__c= false;
            Test.startTest();
            insert adminContactNew;
            
            Contact agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
            insert agentContact;
            
            User portalUser = InitialiseTestData.getPortalUser('xyz@test.com', adminContact.Id, 'Admin');
            User portalUserNew = InitialiseTestData.getPortalUser('t11t@test.com', adminContactNew.Id, 'Admin');
            Test.setMock(HttpCalloutMock.class, new MockHttpDamacOfferServiceResponse ());
            
            System.runAs(portalUserNew){
        
        
            AP_AgentPortalNewLaunches portalLaunchesController = new AP_AgentPortalNewLaunches();
            CreateCilController controller = new CreateCilController(portalLaunchesController);
            CreateCilController.getNoOfUnits();
            controller.getFilterList();
            controller.getMobileCode();
            controller.getCityList();
            controller.getPropertyNames();
            controller.idsSelected = new list<string>{newProperty.Id};
            controller.bRoom = new list<string>{'1'};
            controller.viewProperties();
            controller.getBookinUnitNames();
            Test.stopTest();
            controller.getUnitNames();
            controller.getInqNames();
            controller.getCilInqueries();
            controller.getBedRooms();
            controller.getLessOrMore();
            controller.getUnits();
            controller.getCities();
            controller.getItems();
            controller.getToCurrencyValues();
            controller.strSelectedLanguage = 'zh_CN';
            controller.getFilterList();
            /*
            String searchString = 'Test';
            CreateCilController.searchUnits(searchString);
            controller.filterName = 'Buy';
            controller.untFtrsToFilterStr = 'Test Unit';
            controller.filterData();

            controller.filterName = 'Find a Property';
            controller.untFtrsToFilterStr = 'Test Unit';
            controller.bRoom = new List<String>();
            controller.bRoom.add('STD');
            controller.filterData();

            controller.filterName = 'Create a CIL';
            controller.firstname = 'Test firstname';
            controller.lastName = 'Test lastName';
            controller.email = 'Test@email.com';
            controller.countryCode = '91';
            controller.mobileNumber = '121232325';
            controller.cilUrl();

            controller.filterName = 'Raise an Expression of Interest';
            controller.propertyNameId = newProperty.Id;
            controller.cilUrl();

            controller.filterName = 'Upload Proof of Payment';
            controller.bookingUnitNameId = BUList[0].Id;
            controller.cilUrl();

            controller.filterName = 'Update Company details';
            controller.cilUrl();

            controller.filterName = 'Book a Fly in';
            controller.inquiryId = inq.Id;
            controller.cityVAl = 'Test';
            controller.aedValFlyin = 'Test';
            controller.cilUrl();

            CreateCilController.SendPreBookId(invent.Id);
            */

        }
        }
        
    }

    static testMethod void testMethod2() {

        profile profId = [SELECT Id FROM Profile WHERE Name='Standard User']; // query on Profile to fetch ProfileId
        String orgId = UserInfo.getOrganizationId();
        Profile p = [select Id from Profile where name='Customer Community - Admin'];
        //Contact con = [SELECT Id,Owner__c,Portal_Administrator__c, AccountId FROM Contact LIMIT 1];
        Property__c newProperty = [SELECT Id  FROM Property__c LIMIT 1];
        List<Booking_Unit__c> BUList = [SELECT Id From Booking_Unit__c];
        Inquiry__c inq = [SELECT Id From Inquiry__c Limit 1];
        Inventory__c invent = [SELECT Id From Inventory__c Limit 1];

         //user 
        /*User portalUser = new User(alias = 'test456', email='testusr1@test.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.id, country='United Arab Emirates',IsActive =true,
                ContactId = con.Id,timezonesidkey='America/Los_Angeles', username='portalUser1Test@test.com');
        insert portalUser;*/

        //Test.startTest();
        
        Id adminRoleId = [SELECT Id FROM UserRole WHERE DeveloperName = 'Chairman' LIMIT 1].Id;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'qw56', email='xqw@email.com',
                emailencodingkey='UTF-8', lastname='Userqw', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xqw@email.com',UserRoleId = adminRoleId);
        System.RunAs(adminUser){
            
            createCurrencyValues(2);
            createPageFlowData();
            Account adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
            insert adminAccount;
            
            Contact adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
            insert adminContact;
            Contact adminContactNew = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
            adminContactNew.Owner__c = false;
            adminContactNew.Agent_Representative__c= false;
            adminContactNew.Portal_Administrator__c= false;
            Test.startTest();
            insert adminContactNew;
            
            Contact agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
            insert agentContact;
            
            User portalUser = InitialiseTestData.getPortalUser('xyz@test.com', adminContact.Id, 'Admin');
            User portalUserNew = InitialiseTestData.getPortalUser('t11t@test.com', adminContactNew.Id, 'Admin');
            Test.setMock(HttpCalloutMock.class, new MockHttpDamacOfferServiceResponse ());
            
            System.runAs(portalUserNew){
        
        
            AP_AgentPortalNewLaunches portalLaunchesController = new AP_AgentPortalNewLaunches();
            CreateCilController controller = new CreateCilController(portalLaunchesController);
            CreateCilController.searchInventories('T'); 
            String searchString = 'Test';
            CreateCilController.searchUnits(searchString);
            controller.filterName = 'Buy';
            controller.untFtrsToFilterStr = 'Test Unit';
            controller.filterData();
            
            
            controller.filterName = 'Book';
            controller.untFtrsToFilterStr = 'Test Unit';
            controller.filterData();

            controller.filterName = 'Find a Property';
            controller.untFtrsToFilterStr = 'Test Unit';
            controller.bRoom = new List<String>();
            controller.bRoom.add('STD');
            controller.filterData();

            controller.filterName = 'Create a CIL';
            controller.firstname = 'Test firstname';
            controller.lastName = 'Test lastName';
            controller.email = 'Test@email.com';
            controller.countryCode = '91';
            controller.mobileNumber = '121232325';
            controller.cilUrl();

            controller.filterName = 'Raise an Expression of Interest';
            controller.propertyNameId = newProperty.Id;
            controller.cilUrl();

            controller.filterName = 'Upload Proof of Payment';
            controller.bookingUnitNameId = BUList[0].Id;
            controller.cilUrl();

            controller.filterName = 'Update Company details';
            controller.cilUrl();

            controller.filterName = 'Book a Fly in';
            controller.inquiryId = inq.Id;
            controller.cityVAl = 'Test';
            controller.aedValFlyin = 'Test';
            controller.cilUrl();

            CreateCilController.SendPreBookId(invent.Id);

        }
        }
        
    }

    private static void createBookingUnit(Integer noOfRecords) {
        List<Booking_Unit__c> bookingUnits = new List<Booking_Unit__c>();
        for(Integer i=0; i< noOfRecords; i++ ) {
            Booking_Unit__c bookingUnit = new Booking_Unit__c(
                
            );
            bookingUnits.add(bookingUnit);
        }
        insert bookingUnits;
    }
    
    private static void createCurrencyValues(Integer noOfRecords) {
        List<To_Currency_Values__c> currencyValues = new List<To_Currency_Values__c>();
        for(Integer i=0; i< noOfRecords; i++ ) {
            To_Currency_Values__c currencyValue = new To_Currency_Values__c(
                Name = 'Test1'+i,
                Label__c = 'Test'
            );
            currencyValues.add(currencyValue);
        }
        insert currencyValues;
    }
    
    private static void createPageFlowData() {
        Page_Flow__c PFobj = new Page_Flow__c(
            Name = 'VAT Agent Update',
            Master_Object__c = 'NSIBPM__Service_Request__c',
            Master_Page__c = 'Process_Flow'
        );
        insert PFobj; 
    }
}