@isTest(SeeAlldata=false)
public class AssignTSATeamInquiriesBatchTest {
    public static testmethod void testMethod1(){
           /*
            List<Group> g1 = [select Id from Group where  Type = 'Queue' AND NAME = 'Telesales Team'];
            Profile p = [SELECT Id FROM Profile WHERE Name='Property Consultant'];
            User u = new User(Alias = 'standt', Email='sxyzh@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testingcabc', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='abcvgf@testorg.com');
            TS_Assignment_Settings__c teleSalesCustomSetting = new TS_Assignment_Settings__c();
            teleSalesCustomSetting.Name = 'test';
            teleSalesCustomSetting.Owner_Id__c = '0050Y000001T9MmQAK';
            teleSalesCustomSetting.Telesales_Executive__c = 'Test';
            teleSalesCustomSetting.Round_Robin_Id__c= 2;
             teleSalesCustomSetting.Type__c = 'Stands';
             insert teleSalesCustomSetting ;
            TS_Assignment_Settings__c teleSalesCustomSettingNew  = new TS_Assignment_Settings__c();
            teleSalesCustomSettingNew.Name = 'test1';
             teleSalesCustomSettingNew.Owner_Id__c = String.valueOf(u.id);
            teleSalesCustomSettingNew.Telesales_Executive__c = 'Test';
            teleSalesCustomSettingNew.Type__c = 'Roadshows';
            teleSalesCustomSettingNew.Round_Robin_Id__c = 2;
            insert teleSalesCustomSettingNew ;
            //system.debug('>>g1>>>'+g1);
            */
            /*Group g1 = new Group(Name='Telesales Team', type='Queue');
            insert g1;
            QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Inquiry__c');
            insert q1;
             system.debug('>>g1[0].id>>>'+g1.id);
             System.runAs(new User(Id = UserInfo.getUserId()))
                {*/
            /*        String digitalRecordType=DamacUtility.getRecordTypeId('Campaign__c',
                                                               'Digital');
                    Campaign__c campaignObject = new Campaign__c(RecordTypeId=digitalRecordType,Campaign_Type_New__c='Social',Campaign_Category_New__c='Facebook',Campaign_Sub_Category_1__c='Organic',End_Date__c=date.parse('11/10/18'), Marketing_End_Date__c=date.parse('11/10/18'), Marketing_Start_Date__c=date.parse('11/10/16'), Start_Date__c=date.parse('11/10/16'));
                    insert campaignObject;
                    Id preInq = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
                    Inquiry__c inqObj= new Inquiry__c(RecordTypeId=preInq,Campaign__c=campaignObject.Id,
                                                    Mobile_CountryCode__c='American Samoa: 001684',
                                                    Mobile_Phone__c='1234',Mobile_Phone_Encrypt__c = '123456789',
                                                    Email__c='mk@gmail.com',
                                                    First_Name__c='Test',Last_Name__c='Last',
                                                    Inquiry_Status__c = 'Unreachable - Not responding',
                                                    Last_Activity_Date__c = System.now(),
                                                    OwnerId = g1[0].id
                                                    );
                    insert inqObj;
                    Inquiry__c inqObjNew= new Inquiry__c(RecordTypeId=preInq,Campaign__c=campaignObject.Id,
                                                    Mobile_CountryCode__c='American Samoa: 001684',
                                                    Mobile_Phone__c='1234',Mobile_Phone_Encrypt__c = '123456789123',
                                                    Email__c='mk@gmail123.com',
                                                    First_Name__c='Test',Last_Name__c='Last',
                                                    Inquiry_Status__c = 'Unreachable - Not responding',
                                                    Last_Activity_Date__c = System.now().Date().addDays(-10),
                                                    OwnerId = g1[0].id
                                                    );
                    insert inqObjNew;
                    inqObj.OwnerId = g1[0].id;
                    update inqObj;
                    system.debug('>>inqObj.Owner.Name>>>'+inqObj.Owner.Name);
                    inqObjNew.OwnerId = g1[0].id;
                    update inqObjNew;
                    system.debug('>>inqObjNew.OwnerName>>>'+inqObjNew.Owner.Name);
               // }
             
                
                List<Inquiry__c> inqList = [Select id,OwnerID,Owner.Name from Inquiry__c];
                system.debug('>>inqList>>>'+inqList[0].Owner.Name);
            Test.setMock(HttpCalloutMock.class, new ValidateEmailMockTest());
            
            ScheduleTSATeamInquiriesAssignment sh1 = new ScheduleTSATeamInquiriesAssignment();
            String sch = '0 0 23 * * ?';  
            system.schedule('Test TSA', sch, sh1);
            */
            Test.startTest();
            AssignTSATeamInquiriesBatch tSABatch = new AssignTSATeamInquiriesBatch();
            DataBase.executeBatch(tSABatch);
            
            ReAssignTelesalesTeamInquiry assignTSA = new ReAssignTelesalesTeamInquiry();
            assignTSA.assignTSAToInquiry(null, null, 0);
        assignTSA.updateInquiryOwner (null);
        String CRON_EXP = '0 0 0 3 9 ? 2022';
        
        String jobId = System.schedule('testBasicScheduledApex',
                                       CRON_EXP ,new ScheduleTSATeamInquiriesAssignment ());
            Test.stopTest();
        }
    }