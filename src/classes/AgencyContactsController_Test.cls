@isTest
public with sharing class AgencyContactsController_Test {

    static testmethod void testAgencyContacts() {

        // Test Data
        User pcUser = InitialiseTestData.getPropertyConsultantUsers('userABC@test.com');
        insert pcUser;

        Account acc = InitialiseTestData.getCorporateAccount('TestAgency');
        insert acc ;

        List<Contact> conList = new List<Contact>();
        for (Integer i = 0; i < 5; i++) {
            Contact con = new Contact();
            con.LastName = 'Corporate Agency ' + i;
            con.AccountId = acc.Id;
            con.OwnerId = pcUser.Id;
            conList.add(con);
        }
        insert conList ;

        Task taskObj = new Task();
        taskObj.WhatId = acc.Id;
        taskObj.ActivityDate = System.Today();
        taskObj.OwnerId = pcUser.Id;
        taskObj.Status = 'Completed';
        taskObj.Priority = 'Normal';
        taskObj.Activity_Type__c = 'Calls';
        taskObj.Activity_Sub_Type__c = '';
        taskObj.Start_Date__c = System.Today();
        taskObj.End_Date__c = System.Today();
        taskObj.Description = 'Hello';
        taskObj.Agency_Contacts_Ids__c = conList[0].Id;
        taskObj.Agency_Contacts_Involved__c = conList[0].LastName;
        taskObj.Activity_Outcome__c='Email Delivered';
        insert taskObj;

        AgencyContactsController agencyContacts;

        // Test the functionality in user context
        System.runAs(pcUser) {
            System.Test.startTest();
            ApexPages.StandardController controller = new ApexPages.StandardController(acc);
            PageReference agencyActiviesPage = Page.AgencyContacts;
            Test.setCurrentPage(agencyActiviesPage);
            agencyActiviesPage.getParameters().put('accId', acc.Id);
            agencyActiviesPage.getParameters().put('taskId', taskObj.Id);
            agencyContacts = new AgencyContactsController(controller);
            agencyContacts.contactsWrapperList[0].isSelected = true;
            agencyContacts.createActivityContactRecords();
            System.Test.stopTest();
        }
    }

    static testmethod void testAgencyContacts1() {

        // Test Data
        User pcUser = InitialiseTestData.getPropertyConsultantUsers('userABC@test.com');
        insert pcUser;

        Account acc = InitialiseTestData.getCorporateAccount('TestAgency');
        insert acc ;

        List<Contact> conList = new List<Contact>();
        for (Integer i = 0; i < 5; i++) {
            Contact con = new Contact();
            con.LastName = 'Corporate Agency ' + i;
            con.AccountId = acc.Id;
            con.OwnerId = pcUser.Id;
            conList.add(con);
        }
        insert conList ;

        Task taskObj = new Task();
        taskObj.WhatId = acc.Id;
        taskObj.ActivityDate = System.Today();
        taskObj.OwnerId = pcUser.Id;
        taskObj.Status = 'Completed';
        taskObj.Priority = 'Normal';
        taskObj.Activity_Type__c = 'Calls';
        taskObj.Activity_Sub_Type__c = '';
        taskObj.Start_Date__c = System.Today();
        taskObj.End_Date__c = System.Today();
        taskObj.Description = 'Hello';
        taskObj.Activity_Outcome__c='Email Delivered';
        insert taskObj;

        AgencyContactsController agencyContacts;

        // Test the functionality in user context
        System.runAs(pcUser) {
            System.Test.startTest();
            ApexPages.StandardController controller = new ApexPages.StandardController(acc);
            PageReference agencyActiviesPage = Page.AgencyContacts;
            Test.setCurrentPage(agencyActiviesPage);
            agencyActiviesPage.getParameters().put('accId', acc.Id);
            agencyActiviesPage.getParameters().put('taskId', taskObj.Id);
            agencyContacts = new AgencyContactsController(controller); 
            agencyContacts.contactsWrapperList[0].isSelected = true;
            agencyContacts.createActivityContactRecords();
            System.Test.stopTest();
        }
    }

}