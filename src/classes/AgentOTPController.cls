/***********************************************************************************************************************************
* Name               : AgentOTPController*
* Description        : Controller class for OTP                                 *
* Created Date       : 05/02/2017                                                                                      *
* Created By         : Naresh (Accely)                                                                                             *
* ---------------------------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR               DATE            COMMENTS                                                               *
* 1.0         Naresh- Accely       14/08/2017      Initial Draft.                                                         *
*
**************************************************************************************************************************************/

public without sharing class AgentOTPController{
    public static String PhoneNumber {get ; set ;}
    public static String CountryCode {get ; set ;}
    //public static string otp {get; set;}
    public static boolean OTPFlag {get; set;}
    public static Boolean showScreen5 {get; set;}

// Added By Lovel Constructor
public AgentOTPController(){
  OTPFlag =  true;
  showScreen5 = false;
  system.debug('1OTPFlag'+OTPFlag);
  
}
public void resendOTP(){
  OTPFlag =  true;
}


public  void showScreen5(){
  showScreen5 =  true;
}

   /******************************************************************************************************************
    * @Description : Method to Display Primay Buyer Mobile Number On 4th Screen of Booking Property
    * @Params      : void
    * @Return      : void                                                                           
     *******************************************************************************************************************/

public static boolean verifyotp(String PhoneNumber1,string otp){
    List<otp__c> lstotp =  new List<otp__c>();
  	system.debug('PhoneNumber :'+PhoneNumber1);
  	system.debug('otp :'+otp);
  	//string PhoneNumber1 =  CountryCode+PhoneNumber;
    lstotp =  [Select Phone_Number__c, OTP_Number__c from otp__c where  Phone_Number__c =:PhoneNumber1 and  OTP_Number__c =:otp];
    if(lstotp.size() > 0){
        system.debug('OTP Verified');
      	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'OTP Vefified')); 
      	return  true;
    }
    else{
          system.debug('OTP Not Verified');
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'OTP Not Verified')); 
      return  false;
    }


 }
 public static void  GenrateOTP(String PhonenNumber1){
      OTPFlag = false;
      system.debug('OTPFlag1'+OTPFlag);
      OTPService.Sendtextmessage(PhonenNumber1);
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'OTP Send'));      
 }
}