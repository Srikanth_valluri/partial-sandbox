@RestResource(urlMapping='/getFmCasePicklistFields/*')
global  class GetFMCasePicklistFields_API {
    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        //200 => 'OK',
        //400 => 'Bad Request',
        //401 => 'Unauthorized',
        //500 => 'Internal Server Error' 
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Technical error'
    };
    
    @HttpPost 
    global static FinalReturnWrapper getPickListValues() {
        
        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);
        
        cls_meta_data objMeta = new cls_meta_data();
        
        List<PickListValuesWrapper> pickListValuesWrapperList = new List<PickListValuesWrapper>();
        PickListValuesWrapper nationality = getselectOptions('FM_Case__c','Nationality__c');
        pickListValuesWrapperList.add(nationality);
        System.debug('nationality'+nationality);
        PickListValuesWrapper moveInType = getselectOptions('FM_Case__c','Move_In_Type__c');
        System.debug('moveInType'+moveInType);
        pickListValuesWrapperList.add(moveInType);
        PickListValuesWrapper gender = getselectOptions('FM_Case__c','Gender__c');
        System.debug('gender'+gender);
        pickListValuesWrapperList.add(gender);
        PickListValuesWrapper mobileCountry = getselectOptions('FM_Case__c','Mobile_Country_Code__c');
        System.debug('mobileCountry'+mobileCountry);
        pickListValuesWrapperList.add(mobileCountry);
        PickListValuesWrapper noOfAdults = getselectOptions('FM_Case__c','No_of_Adults__c');
        System.debug('noOfAdults'+noOfAdults);
        pickListValuesWrapperList.add(noOfAdults);
        PickListValuesWrapper noOfChildrens = getselectOptions('FM_Case__c','No_of_Children__c');
        System.debug('noOfChildrens'+noOfChildrens);
        pickListValuesWrapperList.add(noOfChildrens);        
        //PickListValuesWrapper relationship = getselectOptions('FM_Additional_Detail__c','Relationship1__c');
        //System.debug('relationship'+relationship);
        //pickListValuesWrapperList.add(relationship);
        List<CountryISOWrapper> lstCountryISO = getCountryselectOptions('FM_Case__c','Country_ISO__c');
        System.debug('lstCountryISO'+lstCountryISO);

        
        System.debug('pickListValuesWrapperList'+pickListValuesWrapperList);
        
        objMeta.message = pickListValuesWrapperList.size() > 0 ? 'Successful' : 'No picklist Values';
        objMeta.status_code = 1;
        objMeta.title = mapStatusCode.get(1);
        objMeta.developer_message = null;
        
        cls_data objData = new cls_data();
        objData.lookup_list = pickListValuesWrapperList;
        objData.lookup_countries = lstCountryISO;
        
        FinalReturnWrapper objReturn = new FinalReturnWrapper();
        objReturn.data = objData;
        objReturn.meta_data = objMeta;
        System.debug('objReturn: ' + objReturn);
        
        return objReturn;
    }
    public static PickListValuesWrapper getselectOptions(String sObjectName, string fieldName) {
        system.debug('objObject --->' + sObjectName);
        system.debug('fieldName --->' + fieldName);       
        List < String > allOpts = new list < String > ();
        Schema.sObjectType objType = Schema.getGlobalDescribe().get(sObjectName);      
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();       
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();       
        list < Schema.PicklistEntry > values = fieldMap.get(fieldName).getDescribe().getPickListValues();       
        
        PickListValuesWrapper picklistValuesWrapperObj = new PickListValuesWrapper();
        List<CountryISOWrapper> lstCountryISO = new List<CountryISOWrapper>();

        for (Schema.PicklistEntry a: values) {
            if(sObjectName == 'FM_Case__c' && fieldName == 'Country_ISO__c') { //FOR formatiing picklist_countries
                String picVal = a.getValue();
                List<String> lstStr = picVal.split('-');

                CountryISOWrapper objCountryISO = new CountryISOWrapper();
                objCountryISO.country_name = lstStr.size() > 0 ? lstStr[0] : '';
                objCountryISO.nationality = lstStr.size() > 1 ? lstStr[1] : '';
                objCountryISO.iso_code = lstStr.size() > 2 ? lstStr[2] : '';
                objCountryISO.mobile_country_code = lstStr.size() > 3 ? lstStr[0]+': '+lstStr[3] : '';

                lstCountryISO.add(objCountryISO);
            }
            else {
                allOpts.add(a.getValue());            
            }
            
        }
        
        picklistValuesWrapperObj.lookup_type = fieldName.remove('__c').remove('1').toLowerCase();
        picklistValuesWrapperObj.lookup_value = allOpts;  
        //picklistValuesWrapperObj.picklist_countries = lstCountryISO;

        System.debug('picklistValues'+picklistValuesWrapperObj);
        return picklistValuesWrapperObj;
    }

    public static List<CountryISOWrapper> getCountryselectOptions(String sObjectName, string fieldName) {
        system.debug('objObject --->' + sObjectName);
        system.debug('fieldName --->' + fieldName);       
        List < String > allOpts = new list < String > ();
        Schema.sObjectType objType = Schema.getGlobalDescribe().get(sObjectName);      
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();       
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();       
        list < Schema.PicklistEntry > values = fieldMap.get(fieldName).getDescribe().getPickListValues();       
        
        //PickListValuesWrapper picklistValuesWrapperObj = new PickListValuesWrapper();
        List<CountryISOWrapper> lstCountryISO = new List<CountryISOWrapper>();

        for (Schema.PicklistEntry a: values) {
            if(sObjectName == 'FM_Case__c' && fieldName == 'Country_ISO__c') { //FOR formatiing picklist_countries
                String picVal = a.getValue();
                List<String> lstStr = picVal.split('-');

                CountryISOWrapper objCountryISO = new CountryISOWrapper();
                objCountryISO.country_name = lstStr.size() > 0 ? lstStr[0] : '';
                objCountryISO.nationality = lstStr.size() > 1 ? lstStr[1] : '';
                objCountryISO.iso_code = lstStr.size() > 2 ? lstStr[2] : '';
                objCountryISO.mobile_country_code = lstStr.size() > 3 ? lstStr[0]+': '+lstStr[3] : '';

                lstCountryISO.add(objCountryISO);
            }
            
        }
        
        

        System.debug('lstCountryISO'+lstCountryISO);
        return lstCountryISO;
    }
    
    public class PickListValuesWrapper {
        public String lookup_type;
        public List<String> lookup_value;
        //public List<CountryISOWrapper> picklist_countries;
    }

    public class CountryISOWrapper {
        public String country_name;
        public String nationality;
        public String iso_code;
        public String mobile_country_code;
    }
    
    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_data {
        public PickListValuesWrapper[] lookup_list;
        public CountryISOWrapper[] lookup_countries;
    }

    public class cls_meta_data {
        public String message; 
        public Integer status_code;
        public String title; 
        public String developer_message; 
    }
    
}