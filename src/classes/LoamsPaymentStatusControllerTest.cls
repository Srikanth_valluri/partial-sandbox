@isTest
private class LoamsPaymentStatusControllerTest {

    @isTest
    static void positiveTest() {

        PaymentGateway__c gateway = new PaymentGateway__c(
            Name = LoamsCommunityController.GATEWAY_NAME,
            Url__c = 'https://www.payment-gateway.com',
            MerchantId__c = 'merchant_id',
            AccessCode__c = 'access_code',
            EncryptionKey__c = 'encryption_key'
        );
        insert gateway;

        Test.setCurrentPage(Page.CommunityPaymentStatus);


        String mockResponseJson = '{"responseId": "260645","responseTime": "Sun Oct 14 13:13:50 GST 2018","requestName": "CREATE_RECEIPT","extRequestName": "RECEIPT-12345","responseMessage": "CREATE_RECEIPT Completed, Generate the Receipt to View the Receipt File","actions": [{"action": "GENERATE_RECEIPT","method": "GET","url": "https://ptctest.damacgroup.com/DCOFFEE/report/RECEIPT/IPMS/NO_KEY/RECEIPT_ID/2137546"}],"complete": true}';

        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = 'https://test.damacgroup.com/test.pdf';
        response.actions.add(action);

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(new Map<String, FmHttpCalloutMock.Response> {

            FmIpmsRestServices.SF_REST_BASEURL + FmIpmsRestServices.CREATE_RECEIPT
                =>
            new FmHttpCalloutMock.Response(200, 'S', mockResponseJson)
            ,
            'https://ptctest.damacgroup.com/DCOFFEE/report/RECEIPT/IPMS/NO_KEY/RECEIPT_ID/2137546'
                =>
            new FmHttpCalloutMock.Response(200, 'S', JSON.serialize(response))
            ,
            FmIpmsRestServices.SF_REST_BASEURL
            + FmIpmsRestServices.UNIT_SOA_BY_REGISTRATIONID.removeEndIgnoreCase('{0}')
                =>
            new FmHttpCalloutMock.Response(200, 'S', JSON.serialize(response))
            ,
            'https://test.damacgroup.com/test.pdf'
                =>
            new FmHttpCalloutMock.Response(200, 'S', EncodingUtil.base64Encode(Blob.valueOf('test')))
        }));

        Account account = TestDataFactoryFM.createAccount();
        account.Email__pc = 'test@test.com';
        insert account;
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(account);
        insert sr;
        Booking__c booking = TestDataFactoryFM.createBooking(account, sr);
        insert booking;
        Booking_unit__c bookingUnit = TestDataFactoryFM.createBookingUnit(account, booking);
        insert bookingUnit;

        FM_Receipt__c fmReceipt = new FM_Receipt__c();
        fmReceipt.Booking_Unit__c = bookingUnit.Id;
        fmReceipt.Amount__c = 1;
        fmReceipt.CustomerEmail__c = 'test@test.com';
        insert fmReceipt;
        fmReceipt = [SELECT Id, Name FROM FM_Receipt__c WHERE Id = :fmReceipt.Id];

        Test.startTest();

            LoamsPaymentStatusController controller = new LoamsPaymentStatusController();
            controller.updateReceipt();

        Test.stopTest();
    }

    @isTest
    static void guestPaymentPositiveTest() {

        PaymentGateway__c gateway = new PaymentGateway__c(
            Name = LoamsCommunityController.GATEWAY_NAME,
            Url__c = 'https://www.payment-gateway.com',
            MerchantId__c = 'merchant_id',
            AccessCode__c = 'access_code',
            EncryptionKey__c = 'encryption_key'
        );
        insert gateway;

        Test.setCurrentPage(Page.CommunityPaymentStatus);


        String mockResponseJson = '{"responseId": "260645","responseTime": "Sun Oct 14 13:13:50 GST 2018","requestName": "CREATE_RECEIPT","extRequestName": "RECEIPT-12345","responseMessage": "CREATE_RECEIPT Completed, Generate the Receipt to View the Receipt File","actions": [{"action": "GENERATE_RECEIPT","method": "GET","url": "https://ptctest.damacgroup.com/DCOFFEE/report/RECEIPT/IPMS/NO_KEY/RECEIPT_ID/2137546"}],"complete": true}';

        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = 'https://test.damacgroup.com/test.pdf';
        response.actions.add(action);

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(new Map<String, FmHttpCalloutMock.Response> {

            FmIpmsRestServices.SF_REST_BASEURL + FmIpmsRestServices.CREATE_RECEIPT
                =>
            new FmHttpCalloutMock.Response(200, 'S', mockResponseJson)
            ,
            'https://ptctest.damacgroup.com/DCOFFEE/report/RECEIPT/IPMS/NO_KEY/RECEIPT_ID/2137546'
                =>
            new FmHttpCalloutMock.Response(200, 'S', JSON.serialize(response))
            ,
            FmIpmsRestServices.SF_REST_BASEURL
            + FmIpmsRestServices.UNIT_SOA_BY_REGISTRATIONID.removeEndIgnoreCase('{0}')
                =>
            new FmHttpCalloutMock.Response(200, 'S', JSON.serialize(response))
            ,
            'https://test.damacgroup.com/test.pdf'
                =>
            new FmHttpCalloutMock.Response(200, 'S', EncodingUtil.base64Encode(Blob.valueOf('test')))
        }));

        Account account = TestDataFactoryFM.createAccount();
        account.Email__pc = 'test@test.com';
        insert account;
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(account);
        insert sr;
        Booking__c booking = TestDataFactoryFM.createBooking(account, sr);
        insert booking;
        Booking_unit__c bookingUnit = TestDataFactoryFM.createBookingUnit(account, booking);
        insert bookingUnit;

        FM_Receipt__c fmReceipt = new FM_Receipt__c();
        fmReceipt.Booking_Unit__c = bookingUnit.Id;
        fmReceipt.Amount__c = 1;
        // fmReceipt.Discount_Applied__c = Label.AdvanceServiceChargePayment;
        fmReceipt.CustomerEmail__c = 'test@test.com';
        fmReceipt.Payment_Type__c = LoamsMakePaymentController.FM_GUEST_PAYMENT;
        fmReceipt.Guest_Payment_Type__c = 'Service Charges';
        fmReceipt.Order_Status__c = 'Success';
        insert fmReceipt;
        fmReceipt = [SELECT Id, Name FROM FM_Receipt__c WHERE Id = :fmReceipt.Id];

        Test.startTest();

            ApexPages.currentPage().getParameters().put('orderNo', fmReceipt.Name);
            ApexPages.currentPage().getParameters().put('order_status', 'Success');
            ApexPages.currentPage().getParameters().put('merchant_param1', 'FM Guest Payment');
            LoamsPaymentStatusController controller = new LoamsPaymentStatusController();
            controller.updateReceipt();

        Test.stopTest();
    }

    @isTest
    static void guestPaymentOthersPositiveTest() {

        PaymentGateway__c gateway = new PaymentGateway__c(
            Name = LoamsCommunityController.GATEWAY_NAME,
            Url__c = 'https://www.payment-gateway.com',
            MerchantId__c = 'merchant_id',
            AccessCode__c = 'access_code',
            EncryptionKey__c = 'encryption_key'
        );
        insert gateway;

        Test.setCurrentPage(Page.CommunityPaymentStatus);


        String mockResponseJson = '{"responseId": "260645","responseTime": "Sun Oct 14 13:13:50 GST 2018","requestName": "CREATE_RECEIPT","extRequestName": "RECEIPT-12345","responseMessage": "CREATE_RECEIPT Completed, Generate the Receipt to View the Receipt File","actions": [{"action": "GENERATE_RECEIPT","method": "GET","url": "https://ptctest.damacgroup.com/DCOFFEE/report/RECEIPT/IPMS/NO_KEY/RECEIPT_ID/2137546"}],"complete": true}';

        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = 'https://test.damacgroup.com/test.pdf';
        response.actions.add(action);

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(new Map<String, FmHttpCalloutMock.Response> {

            FmIpmsRestServices.SF_REST_BASEURL + FmIpmsRestServices.CREATE_RECEIPT
                =>
            new FmHttpCalloutMock.Response(200, 'S', mockResponseJson)
            ,
            'https://ptctest.damacgroup.com/DCOFFEE/report/RECEIPT/IPMS/NO_KEY/RECEIPT_ID/2137546'
                =>
            new FmHttpCalloutMock.Response(200, 'S', JSON.serialize(response))
            ,
            FmIpmsRestServices.SF_REST_BASEURL
            + FmIpmsRestServices.UNIT_SOA_BY_REGISTRATIONID.removeEndIgnoreCase('{0}')
                =>
            new FmHttpCalloutMock.Response(200, 'S', JSON.serialize(response))
            ,
            'https://test.damacgroup.com/test.pdf'
                =>
            new FmHttpCalloutMock.Response(200, 'S', EncodingUtil.base64Encode(Blob.valueOf('test')))
        }));

        Account account = TestDataFactoryFM.createAccount();
        account.Email__pc = 'test@test.com';
        insert account;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(account);
        insert sr;

        Booking__c booking = TestDataFactoryFM.createBooking(account, sr);
        insert booking;

        Booking_unit__c bookingUnit = TestDataFactoryFM.createBookingUnit(account, booking);
        insert bookingUnit;

        FM_Receipt__c fmReceipt = new FM_Receipt__c();
        fmReceipt.Booking_Unit__c = bookingUnit.Id;
        fmReceipt.Amount__c = 1;
        // fmReceipt.Discount_Applied__c = Label.AdvanceServiceChargePayment;
        fmReceipt.CustomerEmail__c = 'test@test.com';
        fmReceipt.Payment_Type__c = LoamsMakePaymentController.FM_GUEST_PAYMENT;
        fmReceipt.Guest_Payment_Type__c = 'Others';
        fmReceipt.Order_Status__c = 'Success';
        insert fmReceipt;
        fmReceipt = [SELECT Id, Name FROM FM_Receipt__c WHERE Id = :fmReceipt.Id];

        Test.startTest();

            ApexPages.currentPage().getParameters().put('orderNo', fmReceipt.Name);
            ApexPages.currentPage().getParameters().put('order_status', 'Success');
            ApexPages.currentPage().getParameters().put('merchant_param1', 'FM Guest Payment');
            LoamsPaymentStatusController controller = new LoamsPaymentStatusController();
            controller.updateReceipt();

        Test.stopTest();
    }

    @isTest
    static void guestPaymentOthersNegativeTest() {

        PaymentGateway__c gateway = new PaymentGateway__c(
            Name = LoamsCommunityController.GATEWAY_NAME,
            Url__c = 'https://www.payment-gateway.com',
            MerchantId__c = 'merchant_id',
            AccessCode__c = 'access_code',
            EncryptionKey__c = 'encryption_key'
        );
        insert gateway;

        Test.setCurrentPage(Page.CommunityPaymentStatus);


        String mockResponseJson = '{"responseId": "260645","responseTime": "Sun Oct 14 13:13:50 GST 2018","requestName": "CREATE_RECEIPT","extRequestName": "RECEIPT-12345","responseMessage": "CREATE_RECEIPT Completed, Generate the Receipt to View the Receipt File","actions": [{"action": "GENERATE_RECEIPT","method": "GET","url": "https://ptctest.damacgroup.com/DCOFFEE/report/RECEIPT/IPMS/NO_KEY/RECEIPT_ID/2137546"}],"complete": true}';

        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = 'https://test.damacgroup.com/test.pdf';
        response.actions.add(action);

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(new Map<String, FmHttpCalloutMock.Response> {

            FmIpmsRestServices.SF_REST_BASEURL + FmIpmsRestServices.CREATE_RECEIPT
                =>
            new FmHttpCalloutMock.Response(200, 'S', mockResponseJson)
            ,
            'https://ptctest.damacgroup.com/DCOFFEE/report/RECEIPT/IPMS/NO_KEY/RECEIPT_ID/2137546'
                =>
            new FmHttpCalloutMock.Response(200, 'S', JSON.serialize(response))
            ,
            FmIpmsRestServices.SF_REST_BASEURL
            + FmIpmsRestServices.UNIT_SOA_BY_REGISTRATIONID.removeEndIgnoreCase('{0}')
                =>
            new FmHttpCalloutMock.Response(200, 'S', JSON.serialize(response))
            ,
            'https://test.damacgroup.com/test.pdf'
                =>
            new FmHttpCalloutMock.Response(200, 'S', EncodingUtil.base64Encode(Blob.valueOf('test')))
        }));

        Account account = TestDataFactoryFM.createAccount();
        account.Email__pc = 'test@test.com';
        insert account;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(account);
        insert sr;

        Booking__c booking = TestDataFactoryFM.createBooking(account, sr);
        insert booking;

        Booking_unit__c bookingUnit = TestDataFactoryFM.createBookingUnit(account, booking);
        insert bookingUnit;

        FM_Receipt__c fmReceipt = new FM_Receipt__c();
        fmReceipt.Booking_Unit__c = bookingUnit.Id;
        fmReceipt.Amount__c = 1;
        // fmReceipt.Discount_Applied__c = Label.AdvanceServiceChargePayment;
        fmReceipt.CustomerEmail__c = 'test@test.com';
        fmReceipt.Payment_Type__c = LoamsMakePaymentController.FM_GUEST_PAYMENT;
        fmReceipt.Guest_Payment_Type__c = 'Others';
        fmReceipt.Order_Status__c = 'Failure';
        insert fmReceipt;
        fmReceipt = [SELECT Id, Name FROM FM_Receipt__c WHERE Id = :fmReceipt.Id];

        Test.startTest();

            ApexPages.currentPage().getParameters().put('orderNo', fmReceipt.Name);
            ApexPages.currentPage().getParameters().put('order_status', 'Failure');
            ApexPages.currentPage().getParameters().put('merchant_param1', 'Payment');
            LoamsPaymentStatusController controller = new LoamsPaymentStatusController();
            controller.updateReceipt();

        Test.stopTest();
    }

    @isTest
    static void negativeTest() {
        Test.startTest();

            Test.setCurrentPage(Page.CommunityPaymentStatus);
            LoamsPaymentStatusController controller = new LoamsPaymentStatusController();

        Test.stopTest();
    }

}