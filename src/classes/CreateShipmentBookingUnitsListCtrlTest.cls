@isTest
public without sharing Class CreateShipmentBookingUnitsListCtrlTest {
    @isTest
    public static void createRequestTestCaseOne() {
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Address_Line_1__c = 'XYZ';
        acc.Country__c = 'Canada';
        acc.City__c = 'Barcelona';
        acc.State__c = 'lmn';
        acc.Zip_Postal_Code__c = '111';
        insert acc;
        
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;
        
        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        insert objInventory;
        
        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;
        
        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(acc.Id,objDealSR.Id,1);
        insert bookingList;
        
        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,3);
        insert bookingUnitList;
        
        Case objCase = new Case();
        objCase.AccountId = acc.Id;
        insert objCase;
        
        Courier_Delivery__c objAramex = new Courier_Delivery__c();
        objAramex.Courier_Service__c = 'First Flight'; 
        objAramex.Case__c = objCase.Id;
        objAramex.Courier_Status__c = 'Success';
        objAramex.Account__c = acc.Id;
        
        insert objAramex;
        
        Courier_Item__c objCourier = new Courier_Item__c();
        objCourier.Courier_Delivery__c = objAramex.Id;
        objCourier.Case__c = objCase.Id;
       
        objCourier.Booking_Unit__c = bookingUnitList[0].Id;
       
        insert objCourier;
        System.debug('objCourier>>'+objCourier);
        
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(bookingUnitList);
        stdSetController.setSelected(bookingUnitList);
        CreateShipmentBookingUnitsListCtrl lstController = new CreateShipmentBookingUnitsListCtrl(stdSetController);
        Test.startTest();
            lstController.createRequest();
        Test.stopTest();
    }
    
     @isTest
     public static void createRequestTestCase2() {
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Address_Line_1__c = 'XYZ';
        acc.Country__c = 'Canada';
        acc.City__c = 'Barcelona';
        acc.State__c = 'lmn';
        acc.Zip_Postal_Code__c = '111';
        insert acc;
        
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;
        
        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        insert objInventory;
        
        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;
        
        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(acc.Id,objDealSR.Id,1);
        insert bookingList;
        
        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,3);
        insert bookingUnitList;
        
        Case objCase = new Case();
        objCase.AccountId = acc.Id;
        insert objCase;
        
        Courier_Delivery__c objAramex = new Courier_Delivery__c();
        objAramex.Courier_Service__c = 'First Flight'; 
        objAramex.Case__c = objCase.Id;
        objAramex.Courier_Status__c = 'Success';
        objAramex.Account__c = acc.Id;
        
        insert objAramex;
        
        Courier_Item__c objCourier = new Courier_Item__c();
        objCourier.Courier_Delivery__c = objAramex.Id;
        objCourier.Case__c = objCase.Id;
       
        //objCourier.Booking_Unit__c = bookingUnitList[0].Id;
       
        insert objCourier;
        System.debug('objCourier>>'+objCourier);
        
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(bookingUnitList);
        stdSetController.setSelected(bookingUnitList);
        CreateShipmentBookingUnitsListCtrl lstController = new CreateShipmentBookingUnitsListCtrl(stdSetController);
        Test.startTest();
            lstController.createRequest();
        Test.stopTest();
    }
}