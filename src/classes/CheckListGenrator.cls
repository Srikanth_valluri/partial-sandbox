/***********************************************************************************************************************
 * @Name              : CheckListGenrator
 * @Test Class Name   : CheckListGenratorTest
 * @Description       : Class to generate handover checklist document based on certain parameters.
 * ---------------------------------------------------------------------------------------------------------------------
 * VERSION  |AUTHOR                 |DATE           |Modifications
 * ---------------------------------------------------------------------------------------------------------------------
 * 1.0      |Aishwarya Todkar       |16-08-2020     |Initial Draft
 * 
 * 1.1      |Aishwarya Todkar       |14-10-2020     |Added DEFAULT_CHECKLIST_WITH_AV
************************************************************************************************************************/
public  without sharing class CheckListGenrator{

    public Static String drawloopDocName = '';
    public Static Boolean isEsign = false;
    public Static String CHECKLIST_OP_1 = 'CHECKLIST_OP_1';
    public Static String CHECKLIST_OP_2 = 'CHECKLIST_OP_2';
    public Static String CHECKLIST_OP_3 = 'CHECKLIST_OP_3';
    public Static String CHECKLIST_OP_4 = 'CHECKLIST_OP_4';
    public Static String CHECKLIST_OP_5 = 'CHECKLIST_OP_5';
    public Static String CHECKLIST_OP_6 = 'CHECKLIST_OP_6';
    public Static String CHECKLIST_OP_7 = 'CHECKLIST_OP_7';
    public Static String CHECKLIST_OP_8 = 'CHECKLIST_OP_8';
    public Static String CHECKLIST_OP_9 = 'CHECKLIST_OP_9';
    public Static String CHECKLIST_OP_10 = 'CHECKLIST_OP_10';
    public Static String CHECKLIST_OP_11 = 'CHECKLIST_OP_11';
    public Static String CHECKLIST_OP_1_WITH_AV = 'CHECKLIST_OP_1_WITH_AV';
    public Static String CHECKLIST_OP_2_WITH_AV = 'CHECKLIST_OP_2_WITH_AV';
    public Static String CHECKLIST_OP_3_WITH_AV = 'CHECKLIST_OP_3_WITH_AV';
    public Static String CHECKLIST_OP_4_WITH_AV = 'CHECKLIST_OP_4_WITH_AV';
    public Static String CHECKLIST_OP_5_WITH_AV = 'CHECKLIST_OP_5_WITH_AV';
    public Static String CHECKLIST_OP_6_WITH_AV = 'CHECKLIST_OP_6_WITH_AV';
    public Static String CHECKLIST_OP_7_WITH_AV = 'CHECKLIST_OP_7_WITH_AV';
    public Static String CHECKLIST_OP_8_WITH_AV = 'CHECKLIST_OP_8_WITH_AV';
    public Static String CHECKLIST_OP_9_WITH_AV = 'CHECKLIST_OP_9_WITH_AV';
    public Static String CHECKLIST_OP_10_WITH_AV = 'CHECKLIST_OP_10_WITH_AV';
    public Static String CHECKLIST_OP_11_WITH_AV = 'CHECKLIST_OP_11_WITH_AV';
    public Static String DEFAULT_CHECKLIST = 'DEFAULT_CHECKLIST';
    public Static String DEFAULT_CHECKLIST_WITH_AV = 'DEFAULT_CHECKLIST_WITH_AV';
    public Static String AKOYA = 'Akoya';
    public Static String VILLA = 'Villa';
    public Static String DIFC = 'DIFC';
    public Static String ABU_DHABI = 'Abu Dhabi';
    public Static String PLOT = 'PLOT';
    public Static String UPPER_CREST = 'UPPER CREST';
    public Static String SERVICED = 'Serviced';
    public Static String BDW = 'BDW';
    public Static String BDM = 'BDM';
    public Static String LEBANON = 'Lebanon';
    public Static String QATAR = 'Qatar';
    public Static String JORDAN = 'Jordan';
    public Static String SAUDI = 'Saudi';
    public Static String DEA = 'DEA';
    public Static String DEB = 'DEB';
    public Static String UAE1 = 'United Arab Emirates';
    public Static String UAE2 = 'UAE';

    /***********************************************************************************************************************
    * @Description : Method to generateHandover Checklist with E-signature placeholder.
    * @Params      : String unitType
                     String Property_Name
                     String Property_Country
                     String Building_Name
                     String District
                     String PropertyCity
                     String Permitted_Use
                     Id BUId
                     String Origin
                     Id CaseId
                     Boolean isAV
    * @Return      : None
    ***********************************************************************************************************************/
    public Static void genrateVirtualHOCheckList(  String Unit_Type
                                                , String Property_Name
                                                , String Property_Country
                                                , String Building_Name
                                                , String District
                                                , String PropertyCity
                                                , String Permitted_Use
                                                , Id BUId
                                                , String Origin
                                                , Id CaseId
                                                , Boolean isAV
                                                ) {
        isEsign = true;
        genrateHOCheckList( Unit_Type
                            , Property_Name
                            , Property_Country
                            , Building_Name
                            , District
                            , PropertyCity
                            , Permitted_Use
                            , BUId
                            , Origin
                            , CaseId
                            , isAV );
        isEsign = false;
    }

    /***********************************************************************************************************************
    * @Description : Method to generateHandover Checklist based on certain parameters.
    * @Params      : String unitType
                     String Property_Name
                     String Property_Country
                     String Building_Name
                     String District
                     String PropertyCity
                     String Permitted_Use
                     Id BUId
                     String Origin
                     Id CaseId
                     Boolean isAV
    * @Return      : None
    ***********************************************************************************************************************/
    public Static void genrateHOCheckList( String Unit_Type
                                        , String Property_Name
                                        , String Property_Country
                                        , String Building_Name
                                        , String District
                                        , String PropertyCity
                                        , String Permitted_Use
                                        , Id BUId
                                        , String Origin
                                        , Id CaseId
                                        , Boolean isAV
                                        ) {

            if( String.isNotBlank( Property_Country ) 
            && ( Property_Country.equalsIgnoreCase( UAE1 ) || Property_Country.equalsIgnoreCase( UAE2 ) ) ) {
                if( String.isNotBlank( Property_Name )
                && String.isNotBlank( Unit_Type )
                && Property_Name.containsIgnoreCase( AKOYA )
                && Unit_Type.equalsIgnoreCase( VILLA ) 
                && !isAV ) {
                    drawloopDocName = CHECKLIST_OP_1;
                } 
                else if( String.isNotBlank( Property_Name )
                && String.isNotBlank( Unit_Type ) 
                && Property_Name.containsIgnoreCase( AKOYA ) 
                && Unit_Type.equalsIgnoreCase( VILLA )
                && isAV ) {
                    drawloopDocName = CHECKLIST_OP_1_WITH_AV;
                } 
                else if( String.isNotBlank( Property_Name ) 
                && String.isNotBlank( Unit_Type )
                && Property_Name.containsIgnoreCase( AKOYA )
                && !Unit_Type.equalsIgnoreCase( VILLA )
                && !isAV ) {
                    drawloopDocName = CHECKLIST_OP_2;
                }
                else if( String.isNotBlank( Property_Name )
                && String.isNotBlank( Unit_Type )
                && Property_Name.containsIgnoreCase( AKOYA )
                && !Unit_Type.equalsIgnoreCase( VILLA )
                && isAV ) {
                    drawloopDocName = CHECKLIST_OP_2_WITH_AV;
                } 
                else if( String.isNotBlank( District ) 
                && District.equalsIgnoreCase( DIFC ) 
                && !isAV ) {
                    drawloopDocName = CHECKLIST_OP_3;
                } 
                else if( String.isNotBlank( District ) 
                && District.equalsIgnoreCase( DIFC )
                && isAV ) {
                    drawloopDocName = CHECKLIST_OP_3_WITH_AV;
                }
                else if( String.isNotBlank( PropertyCity) 
                && PropertyCity.containsIgnoreCase( ABU_DHABI ) 
                && !isAV ) {
                    drawloopDocName = CHECKLIST_OP_4;
                }
                else if( String.isNotBlank( PropertyCity)
                && PropertyCity.containsIgnoreCase( ABU_DHABI ) 
                && isAV ) {
                    drawloopDocName = CHECKLIST_OP_4_WITH_AV;
                } 
                else if( String.isNotBlank( Unit_Type ) 
                && Unit_Type.equalsIgnoreCase( PLOT ) 
                && !isAV ) {
                    drawloopDocName = CHECKLIST_OP_5;
                } 
                else if( String.isNotBlank( Unit_Type ) 
                && Unit_Type.equalsIgnoreCase( PLOT ) 
                && isAV ) {
                    drawloopDocName = CHECKLIST_OP_5_WITH_AV;
                } 
                else if( String.isNotBlank( Property_Name ) 
                && Property_Name.containsIgnoreCase( UPPER_CREST ) 
                && !isAV ) {
                    drawloopDocName = CHECKLIST_OP_6 ;
                } 
                else if( String.isNotBlank( Property_Name ) 
                && Property_Name.containsIgnoreCase( UPPER_CREST ) 
                && isAV ) {
                    drawloopDocName = CHECKLIST_OP_6_WITH_AV;
                } 
                else if( String.isNotBlank( Permitted_Use ) 
                && Permitted_Use.containsIgnoreCase( SERVICED ) 
                && !isAV ) {
                    drawloopDocName = CHECKLIST_OP_7;
                } 
                else if( String.isNotBlank( Permitted_Use ) 
                && Permitted_Use.containsIgnoreCase( SERVICED ) 
                && isAV ) {
                    drawloopDocName = CHECKLIST_OP_7_WITH_AV;
                }
                else if( Building_Name.equalsIgnoreCase( BDW ) ) {
                    //TBD
                }
                else if( Building_Name.equalsIgnoreCase( BDM ) ) {
                    //TBD
                }
                else{
                    drawloopDocName = isAV ?  DEFAULT_CHECKLIST_WITH_AV : DEFAULT_CHECKLIST;
                }
            }
            else if( String.isNotBlank( Property_Country ) && Property_Country.equalsIgnoreCase( LEBANON ) && !isAV ) {
                    drawloopDocName = CHECKLIST_OP_8;
            } 
            else if( String.isNotBlank( Property_Country ) && Property_Country.equalsIgnoreCase( LEBANON ) && isAV ) {
                    drawloopDocName = CHECKLIST_OP_8_WITH_AV;
            } 
            else if( String.isNotBlank( Property_Country ) && Property_Country.equalsIgnoreCase( QATAR ) && !isAV ) {
                    drawloopDocName = CHECKLIST_OP_9;
            } 
            else if( String.isNotBlank( Property_Country ) && Property_Country.equalsIgnoreCase( QATAR ) && isAV ) {
                    drawloopDocName = CHECKLIST_OP_9_WITH_AV;
            } 
            else if( String.isNotBlank( Property_Country ) && Property_Country.equalsIgnoreCase( JORDAN ) && !isAV ) {
                    drawloopDocName = CHECKLIST_OP_10;
            } 
            else if( String.isNotBlank( Property_Country ) && Property_Country.equalsIgnoreCase( JORDAN ) && isAV ) {
                    drawloopDocName = CHECKLIST_OP_10_WITH_AV;
            } 
            else if( String.isNotBlank( Property_Country ) && Property_Country.containsIgnoreCase( SAUDI )
            && ( Building_Name.equalsIgnoreCase( DEA ) || Building_Name.equalsIgnoreCase( DEB ) )
            && !isAV ) {
                drawloopDocName = CHECKLIST_OP_11;
            } 
            else if( String.isNotBlank( Property_Country )
            && Property_Country.containsIgnoreCase( SAUDI )
            && (String.isNotBlank( Building_Name )
            && ( Building_Name.equalsIgnoreCase( DEA ) || Building_Name.equalsIgnoreCase( DEB ) ) )
            && isAV ) {
                drawloopDocName = CHECKLIST_OP_11_WITH_AV;
            }
            else{
                drawloopDocName = isAV ?  DEFAULT_CHECKLIST_WITH_AV : DEFAULT_CHECKLIST;
            }

            // Added for documents with E-sign
            if( !String.isBlank( drawloopDocName ) && isEsign ) {
                drawloopDocName = drawloopDocName + '_E-SIGN';
            }
            //IMP debug to verify the Drawloop Name Generated.
            System.debug( 'drawloopDocName ==='+drawloopDocName );
            
            Riyadh_Rotana_Drawloop_Doc_Mapping__c drawloopCS
                = Riyadh_Rotana_Drawloop_Doc_Mapping__c.getInstance( drawloopDocName );
                
            //IMP debug to verify the Custom Setting Name.
            System.debug( 'Custom Setting Name based ob Drawloop name ==='+drawloopCS );

            List<Booking_Unit__c> listBu = new List<Booking_Unit__c>();
            listBu = [ SELECT
                            Id
                            , HO_EHO_PHO_CaseId__c
                            , Booking__c
                        FROM
                            Booking_Unit__c
                        WHERE
                            ID =: BUId
                        LIMIT 1 ];

            if( drawloopCS != Null
            && String.isNotBlank( drawloopCS.Delivery_Option_Id__c )
            && String.isNotBlank( drawloopCS.Drawloop_Document_Package_Id__c ) ) {
                if( listBu != null && !listBu.isEmpty() ) {
                    operateRecipients( listBu[0].Booking__c, drawloopCS.Drawloop_Document_Package_Id__c );
                }
                if( !Test.isRunningTest() ) {
                    Database.executeBatch( new GenerateBUDrawloopDocumentBatch( BUId 
                                                                            , drawloopCS.Drawloop_Document_Package_Id__c,
                                                                              drawloopCS.Delivery_Option_Id__c ) );
                }
            }

            if( Origin =='Case' && CaseId != NULL && listBu != null  && !listBu.isEmpty() ) {
                listBu[0].HO_EHO_PHO_CaseId__c = CaseId ;
                update listBu;
            }
    }

    /***********************************************************************************************************************
    * @Description : Method to fetch the related buyers to the booking unit.
    * @Params      : Id bookingUnit
                     String ddpId
    * @Return      : None
    ***********************************************************************************************************************/
    public static void operateRecipients( Id bookingId, String ddpId ) {
        List<Loop__dsRecipient__c> listRecipients_PB = [ SELECT
                                                                Id
                                                                , Loop__Address__c
                                                                , Loop__StaticName__c
                                                                , Loop__IntegrationOption__c
                                                            FROM
                                                                Loop__dsRecipient__c
                                                            WHERE
                                                                Loop__IntegrationOption__r.Loop__DDP__c =: ddpId
                                                            AND
                                                                Loop__dsRoleName__c = 'Primary Buyer'
                                                            LIMIT 1
                                                        ];

        List<Loop__dsRecipient__c> listRecipients_JB_ToDelete = [ SELECT
                                                                Id
                                                                , Loop__Address__c
                                                                , Loop__StaticName__c
                                                            FROM
                                                                Loop__dsRecipient__c
                                                            WHERE
                                                                Loop__IntegrationOption__r.Loop__DDP__c =: ddpId
                                                            AND
                                                                Loop__dsRoleName__c Like 'Joint Buyer%'
                                                        ];
        
        if( listRecipients_JB_ToDelete != null && !listRecipients_JB_ToDelete.isEmpty() ) {
            delete listRecipients_JB_ToDelete;
        }
        List<Loop__dsRecipient__c> listRecipients_JB_toCreate = new List<Loop__dsRecipient__c>();
        Integer i = 1;
        for( Buyer__c byr : [ SELECT Id
                                    , Booking__c
                                    , First_Name__c
                                    , Last_Name__c
                                    , Account__r.Name
                                    , Account__r.Person_Business_Email__c
                                    , Primary_Buyer__c
                                FROM
                                    Buyer__c
                                WHERE
                                    Booking__c =: bookingId
                                AND
                                    Account__c != null
                                AND
                                    Account__r.Person_Business_Email__c != null
                                ORDER BY
                                    Account__r.Name ASC ] ) {
                                        
                        System.debug( 'byr'+byr );

            if( listRecipients_PB != null && !listRecipients_PB.isEmpty() ) {
                if( byr.Primary_Buyer__c ) {

                    //Updating Primary Buyer recipient
                    listRecipients_PB[0].Loop__StaticName__c = byr.Account__r.Name;
                    listRecipients_PB[0].Loop__Address__c = byr.Account__r.Person_Business_Email__c;
                }
                else{

                    //Creating Joint Buyer Recipients
                    Loop__dsRecipient__c obj_JB_Rec = new Loop__dsRecipient__c();
                    obj_JB_Rec.Loop__StaticName__c = byr.Account__r.Name;
                    obj_JB_Rec.Loop__Address__c = byr.Account__r.Person_Business_Email__c;
                    obj_JB_Rec.Loop__dsRoleName__c = 'Joint Buyer ' + i;
                    obj_JB_Rec.Loop__dsRoutingOrder__c = 1;
                    obj_JB_Rec.Loop__dsStaticRecipient__c = 'Static Name-Email';
                    obj_JB_Rec.Loop__IntegrationOption__c = listRecipients_PB[0].Loop__IntegrationOption__c;
                    listRecipients_JB_toCreate.add( obj_JB_Rec );
                    i++;
                }
            }
        }

        if( listRecipients_PB != null && !listRecipients_PB.isEmpty() ) {
            update listRecipients_PB;
        }

        if( !listRecipients_JB_toCreate.isEmpty() ) {
            insert listRecipients_JB_toCreate;
        }
    }
}