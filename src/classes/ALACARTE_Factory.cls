@RestResource(urlMapping='/DAMACAlaCarteFactory/*')
global class ALACARTE_Factory {
    @HttpPost
    global static void doPost(){
    
        RestContextHandler handler = new RestContextHandler(true);
        RestRequest req = handler.restRequest;
        
        try{
            System.debug('>>>>>requestURI>>>>>>>'+req.requestURI);
            String action = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
            System.debug('>>>>>action>>>>>>>'+action);

            Map<String, string> params = handler.getRequestParams();
            System.debug('>>>>>params>>>>>>>'+params); 
            
            if(action.toLowerCase() == 'inventories'){
                string condition = 'where Marketing_Name__c = \'A LA CARTE\'';
                string mainQuery = 'Select id, '+string.join(readFieldset('ALACARTE_API', 'Inventory__c'),',')+' from Inventory__c '+condition;
                System.debug('>>>>>>mainQuery>>>>>>>>>'+mainQuery);
                list<Inventory__c> inventories = database.query(mainQuery);
                
                handler.response.data = inventories;
                handler.response.message = '';
                handler.response.statusCode = WebServiceResponse.STATUS_CODE_SUCCESS;
            }

            if(action.toLowerCase() == 'statusupdate'){
                string inventoryId = DAMAC_Utility.trimData(params.get('inventoryId'));
                list<Inventory__c> lstInv = [Select id from Inventory__c where Unit_Name__c =: inventoryId];
                string status = DAMAC_Utility.trimData(params.get('status'));
                if(!lstInv.isEmptY()){
                    Inventory__c inv = lstInv[0];
                    inv.Status__c = status;
                    update inv; 

                    handler.response.data = '';
                    handler.response.message = 'Inventory Status updated successfully.';
                    handler.response.statusCode = WebServiceResponse.STATUS_CODE_SUCCESS;
                }
                else{
                    handler.response.data = '';
                    handler.response.message = 'Unit not found - '+inventoryId;
                    handler.response.statusCode = WebServiceResponse.STATUS_ALLOTMENT_ERROR;
                }
            }

            if(action.toLowerCase() == 'attributes'){
                string mainQuery = 'Select id, '+string.join(readFieldset('ALACARTE_API', 'Inventory_Attribute__c'),',')+' from Inventory_Attribute__c';
                System.debug('>>>>>>mainQuery>>>>>>>>>'+mainQuery);
                list<Inventory_Attribute__c> inventories = database.query(mainQuery);
                
                string inventoryId = DAMAC_Utility.trimData(params.get('inventoryId'));
                
                handler.response.data = inventories;
                handler.response.message = '';
                handler.response.statusCode = WebServiceResponse.STATUS_CODE_SUCCESS;
            }
            
            if(action.toLowerCase() == 'inventoryassignment'){
                
                string rmEmail = params.containsKey('rmEmail') ? (params.get('rmEmail') != '' ? DAMAC_Utility.trimData(params.get('rmEmail')) :'') : '';
                string inventoryId = DAMAC_Utility.trimData(params.get('inventoryId'));
                list<Inventory__c> lst = [Select id from Inventory__c where Unit_Name__c =: inventoryId];
                Inventory_User__c iUser = new Inventory_User__c();
                if(!lst.isEmpty()){
                    if(rmEmail != ''){
                        List<User> lstUsers = [Select id from User where email =: rmEmail limit 1];
                        if(!lstUsers.isEmpty()){
                            iUser.Inventory__c = lst[0].id;
                            iUser.User__c = lstUsers[0].Id;
                            iUser.Unique_Key__c = iUser.Inventory__c+'###'+iUser.User__c;
                            Database.upsert(iUser , Inventory_User__c.Unique_Key__c, false); 
                            
                            handler.response.data = '';
                            handler.response.message = 'Inventory Assigned to provided PC successfully.';
                            handler.response.statusCode = WebServiceResponse.STATUS_CODE_SUCCESS;
                        }
                        else{
                            handler.response.data = '';
                            handler.response.message = 'User not found with provided details - '+rmEmail;
                            handler.response.statusCode = WebServiceResponse.STATUS_ALLOTMENT_ERROR;
                        }
                    }
                    else{
                        iUser.Inventory__c = lst[0].id;
                        iUser.User__c = System.label.ALACARTE_DefaultPC;
                        iUser.Unique_Key__c = iUser.Inventory__c+'###'+iUser.User__c;
                        Database.upsert(iUser , Inventory_User__c.Unique_Key__c, false);
                        
                        handler.response.data = '';
                        handler.response.message = 'Inventory assigned to default User.';
                        handler.response.statusCode = WebServiceResponse.STATUS_CODE_SUCCESS;
                    }
                }
                else{
                    handler.response.data = '';
                    handler.response.message = 'Unit not found - '+inventoryId;
                    handler.response.statusCode = WebServiceResponse.STATUS_ALLOTMENT_ERROR;
                }
            }
            
            handler.finalize();
                
        }
        catch (Exception exc) {
            system.debug('>>>>>>>>>>ERROR>>>>>>>>>>>' +exc.getmessage()+'>>>>>>LineNO>>>>>'+exc.getlinenumber()+'>>>'+exc.getStackTraceString());
            handler.response.success = false;
            handler.response.statusCode = WebServiceResponse.STATUS_CODE_SYSTEM_ERROR;
            handler.response.message = exc.getMessage()+'-'+exc.getLineNumber();
            handler.finalize(exc);
        }
    }
    
    //readFieldSet
    public static list<string> readFieldSet(String fieldSetName, String ObjectName){
        list<string> FieldApiNames = new list<string>();
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
       
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
        for(Schema.FieldSetMember f : fieldSetObj.getFields()) {
            FieldApiNames.add(f.getFieldPath());
        }
        return FieldApiNames;
    }


    
}