/*********************************************************************************************************
* Name               : AgentCapsuleFormController_Remote
* Test Class         : AgentCapsuleFormController_Test
* Description        : Remote Extension class for AgentCapsuleForm VF Page
* Created Date       : 28/09/2020
* ----------------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst         28/10/2020      Initial Draft.
**********************************************************************************************************/
public With Sharing Class AgentCapsuleFormController_Remote{

    public AgentCapsuleFormController_Remote(ApexPages.StandardController controller) {   }
    public AgentCapsuleFormController_Remote(AgentCapsuleFormController controller) {}    

        @RemoteAction 
    public static List<Account> getAccounts(String pSearchKey) {
        String searchString = '%' + pSearchKey  + '%';
        List<Account> accountList = [SELECT Id, Name 
                                    FROM Account 
                                    WHERE Name LIKE :searchString 
                                    and RecordType.name = 'Corporate Agency'
                                    LIMIT 5]; 
        System.debug('accountList:::: ' + accountList);
        return accountList;
    }
}