/*-------------------------------------------------------------------------------------------------
Description: Test class for SMSClass

============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 26-11-2018       | Lochana Rajput   | 1. Test class for invocable method
=============================================================================================================================
*/
@isTest
private class SMSClassTest {

	@isTest static void test_method_one() {
		SMS_History__c smsObj = new SMS_History__c();
	   	smsObj.Phone_Number__c = '1234567890';
	   	smsObj.Message__c = 'Test';
	   	insert smsObj;
	   	List<SMS_History__c> lstSMSHist = new List<SMS_History__c>();
	   	lstSMSHist.add(smsObj);
	   	SMSClass moveOutObj = new SMSClass();
	   	SMSClass.sendSMS(lstSMSHist);
	}
}