/*-------------------------------------------------------------------------------------------------
Description: Test class for SendSMSQueueable

============================================================================================================================
    Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
    1.0     | 31-07-2017       | Lochana Rajput   | 1. Initial draft
=============================================================================================================================
*/
@isTest
private class SendSMSQueueableTest {

	@isTest static void test_SMSSend() {
		SMS_History__c obj = new SMS_History__c();
		obj.Message__c ='Test message';
		obj.Phone_Number__c = '8989898989';
		obj.Name = 'Test SMS';
		insert obj;

		Test.setMock(HttpCalloutMock.class, new MockHttpAgentOTPController());
		Test.startTest();
		System.enqueueJob(new SendSMSQueueable(obj.ID));
		Test.stopTest();
	}

	@isTest static void test_FMCaserejectionComments() {
		Id recTypeId=Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Move In').getRecordTypeId();
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Task_Id__c=123;
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Change_of_Contact_Details';
        fmCaseObj.Email__c='a@gmail.com';
        fmCaseObj.Mobile_no__c='12121212';
        fmCaseObj.RecordTypeId=recTypeId;
		fmCaseObj.Submitted__c = true;
        insert fmCaseObj;
		Test.startTest();
		fmCaseObj.Approving_Authorities__c='FM Admin';
        fmCaseObj.Approval_Status__c='Rejected';
        fmCaseObj.Submit_for_Approval__c=true;
		update fmCaseObj;
        Test.stopTest();

	}

}