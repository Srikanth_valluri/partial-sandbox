@isTest
global  Class DLP_DateHttpMock implements HttpCalloutMock {
    
    Integer reqNumber;
    
    public DLP_DateHttpMock( Integer reqNumber) {
        this.reqNumber = reqNumber;
    }

    global HTTPResponse respond(HTTPRequest req){
        
        HttpResponse res = new HttpResponse();
        String responseBody;

        if( reqNumber == 1 ) {
            responseBody = '{'+
                                '"responseId": "45470",' +
                                '"responseTime": "Sun Nov 24 08:54:32 GMT+04:00 2019",' +
                                '"requestName": "Set DLP Date",' + 
                                '"status": "S",' + 
                                '"responseMessage":"DLP Set for TEST/10/101",' + 
                                '"elapsedTimeMs": 0,' +
                                '"complete": true ' + 
                            '}';
        }
        else if( reqNumber ==2 ) {
            responseBody = '{'+
                                '"responseId": "-45470",' +
                                '"responseTime": "Sun Nov 24 08:54:32 GMT+04:00 2019",' +
                                '"requestName": "Set DLP Date",' + 
                                '"status": "E",' + 
                                '"responseMessage":"Value cannot be null.\r\nParameter name: No destinatio n ' + 
                                    'for a conversion operation <Fsi.Concept.Contracts.Entities.ServiceModel.Locat ionDto> ' + 
                                    '- <Fsi.Concept.Setup.Contracts.Entities.IFAREALO>",' + 
                                '"elapsedTimeMs": 0,' +
                                '"complete": true ' + 
                            '}';
        }
        res.setBody( responseBody );
        return res;
    }
}