public class MergeFields
{
    public static Map<String, String> parse(map<String, String> mapIdContent)
    {
        //System.debug('!!!!!!!!!!!objectName'+objectName); Set<Id> ids, String content
       // System.debug('!!!!!!!!!!!content'+content);
        Set<String> allApis = new Set<String>();
        String objectAPI = '';
        Set<Id> ids = new set<Id>();
        system.debug('!!!!!!!!!!mapIdContent'+mapIdContent.keyset());
        for(String objKey : mapIdContent.keyset())
        {
            Id objId;
            if (objKey.contains('-')) {
                objId = objKey.substringBefore('-');
            } else {
                objId = objKey;
            }
            ids.add(objId);            
            objectAPI = getObjectAPI(objId);
            system.debug('!!!!!objectAPI'+objectAPI );
            system.debug('!!!!!mapIdContent'+mapIdContent);
            system.debug('!!!!!mapIdContent.get(objKey)'+mapIdContent.get(objKey) );
            if (mapIdContent != null && mapIdContent.containsKey(objKey) 
                && mapIdContent.get(objKey) != null) {
                allApis.addAll(getFields(mapIdContent.get(objKey), objectAPI));
            }
            //break;
        }
        system.debug('!!!ids'+ids);
        system.debug('!!!!!!!!!allApis'+allApis);
        String objectFields = '';
        
        for(String field : allApis)
        {
            objectFields += field + ', ';
        }
        
        system.debug('!!!!!!!!!objectFields '+objectFields );
        if(objectFields != '')
        {
            Map<String, String> objectIdToNewContent = new Map<String, String>();
            objectFields = objectFields.substring(0, objectFields.length() - 2);
            String queryStr = 'select ' + objectFields + ' from ' + objectAPI + ' where Id in :ids';
            system.debug('!!!queryStr '+queryStr);
            
            Map<Id, Map<String, String>> mapobjIdTemp = new Map<Id, Map<String, String>>();
            for(sObject obj : Database.query(queryStr))
            {
                Map<String, String> tempMap = new Map<String, String>();
                for(String field : allApis)
                {
                    tempMap.put(field, String.valueOf(getFieldValue(obj, field)));
                }
                system.debug('!!!!!tempMap'+tempMap);
                mapobjIdTemp.put(obj.Id, tempMap);
                system.debug('!!!!!!!!mapobjIdTemp'+mapobjIdTemp);                
            }
            
            for(String objKey : mapIdContent.keyset()) {
                system.debug('!!!!inside second for');
                String objId;
                if (objKey.contains('-')) {
                    objId = objKey.substringBefore('-');
                } else {
                    objId = objKey;
                }
                system.debug('!!!!mapobjIdTemp.get(objId)'+mapobjIdTemp.get(objId));
                system.debug('!!!!mapIdContent.get(objId)'+mapIdContent.get(objId));
                objectIdToNewContent.put(objKey , match(mapIdContent.get(objKey), mapobjIdTemp.get(objId), objectAPI));
                system.debug('!!!!!!!objectIdToNewContent'+objectIdToNewContent);
            }
            
            return objectIdToNewContent;
        }
        
        Map<String, String> objectIdToContent = new Map<String, String>();
        for(String objId : mapIdContent.keyset())
        {
            objectIdToContent.put(objId, mapIdContent.get(objId));
        }
        system.debug('!!!!!!!!!!objectIdToContent'+objectIdToContent);
        return objectIdToContent;
    }

    public static Object getFieldValue(SObject entity, String fieldName)
    {
        if(entity == null)
        {
            return null;
        }

        if(fieldName.contains('.'))
        {
            return getFieldValue(entity.getSObject(fieldName.substringBefore('.')), fieldName.substringAfter('.'));
        }
        else
        {
            return entity.get(fieldName);
        }
    }

    private static Set<String> getFields(String content, String objectAPI)
    {
        Set<String> fieldApis = new Set<String>();
        Matcher m = Pattern.compile('(\\{!\\s*' + objectAPI + '\\.([A-Za-z\\._0-9]+)\\s*\\})').matcher(content);
        Set<String> matchFields = new Set<String>();
        while(m.find())
        {
            String merge_field = m.group(2);
            fieldApis.add(merge_field);
        }

        return fieldApis;
    }

    private static String match(String content, Map<String, String> obj, String objectAPI)
    {
        Matcher m = Pattern.compile('(\\{!\\s*' + objectAPI + '\\.([A-Za-z\\._0-9]+)\\s*\\})').matcher(content);
        Set<String> matchFields = new Set<String>();
        while(m.find())
        {
            String merge_field = (String)m.group(0);
            String field = (String)m.group(2);

            String value;
            try
            {
                value = (String)obj.get(field);
            }
            catch(Exception e)
            {
                value = '';
            }
            if(value != null)
            {
                content = content.replace(merge_field, value);
            }
            else
            {
                content = content.replace(merge_field, '');
            }
        }

        return content;
    }

    private static String getObjectAPI(Id recordId)
    {
        /*String keyCode  = recordId.subString(0,3);
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        for(Schema.SObjectType objectInstance : gd.values())
        {
            if(objectInstance.getDescribe().getKeyPrefix() == keyCode)
            {
                return objectInstance.getDescribe().getName();
            }
        }
        return null;*/
        String objectAPI = recordId.getSObjectType().getDescribe().getName();
        return objectAPI;
    }
}