@isTest
private class FullCalendarControllerTest {
	
	static Account  objAcc;
    static Booking__c objBooking;
    static Booking_Unit__c objBookingUnit;
    static NSIBPM__Service_Request__c objSR;
	
	static testMethod void getCalendarEventsTest() {
	    
	    Id userId = UserInfo.getUserId();
	    Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
	    
        objAcc = new Account(Primary_Language__c = 'Arabic'
                            , RecordTypeId = personAccRTId
                            , FirstName='Test FirstName'
                            , LastName='Test LastName'
                            , Email__pc = 'test@t.com'
                            , Email__c = 'test@t.com'
                            , Type='Person'
                            , party_ID__C='123456'
                            , Primary_CRE__c = userId
                            , Secondary_CRE__c = userId
                            , Tertiary_CRE__c = userId);
        insert objAcc;
        
        objSR = new NSIBPM__Service_Request__c();
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;

        objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;

        objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC';
        insert objBookingUnit;
        
        Location__c loc = new Location__c();
        loc.Location_ID__c = '22656';
        loc.Name = 'JNU';
        insert loc;
        
        Appointment__c objApp = new Appointment__c();
        objApp = new Appointment__c();
        objApp.Appointment_Date__c = date.newinstance( 2019, 02, 12 );
        objApp.Appointment_End_Date__c = date.newinstance( 2019, 02, 21 );
        objApp.Building__c = loc.id;
        objApp.Processes__c = 'Handover';
        objApp.Sub_Processes__c = 'Key Handover';
        objApp.Group_Name__c = 'Handover Queue';
        objApp.Slots__c = '12:00 - 13:00';
        insert objApp;

        Appointment__c objApp1 = new Appointment__c();
        objApp1.Appointment_Date__c = date.newinstance( 2019, 02, 12 );
        objApp1.Appointment_End_Date__c = date.newinstance( 2019, 02, 21 );
        objApp1.Building__c = loc.id;
        objApp1.Processes__c = 'Handover';
        objApp1.Sub_Processes__c = 'Key Handover';
        objApp1.Group_Name__c = 'Handover Queue';
        objApp1.Slots__c = '17:00 - 18:00';
        insert objApp1;

        Appointment__c objApp2 = new Appointment__c();
        objApp2.Appointment_Date__c = date.newinstance( 2019, 02, 12 );
        objApp2.Appointment_End_Date__c = date.newinstance( 2019, 02, 21 );
        objApp2.Building__c = loc.id;
        objApp2.Processes__c = 'Handover';
        objApp2.Sub_Processes__c = 'Key Handover';
        objApp2.Group_Name__c = 'Handover Queue';
        objApp2.Slots__c = '10:00 - 11:00';
        insert objApp2;
        
        String slctdDate = '22-02-2019';
        String accountId = objAcc.id;
        String unitName = objBookingUnit.id;
        String processName = '01-Handover';
        String subProcessName = 'Key Handover';
        String source = 'Portal';
        
        String queryStr = ' SELECT Id'
                        +', Name'
                        +', Appointment_Date__c'
                        +', Building__c'
                        +', Process_Name__c'
                        +', Slots__c'
                        +', Sub_Process_Name__c'
                        +', Time_Slot_Sub__c'
                        +' FROM Appointment__c ';
            
        FullCalendarController objFullCalendarController = new FullCalendarController();
        
        Test.startTest();
            FullCalendarController.CalendarEvent objCalendarEvent = new FullCalendarController.CalendarEvent();
            FullCalendarController.getCalendarEvents(queryStr);
        Test.stopTest();
	}
}