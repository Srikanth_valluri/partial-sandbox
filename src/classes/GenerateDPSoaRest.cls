/********************************************************************************************************************************
* Description - REST callout to generate SOA for DP Calling list                                                                *
*-------------------------------------------------------------------------------------------------------------------------------*
* Version   Date            Author              Description                                                                     *
*-------------------------------------------------------------------------------------------------------------------------------*
* 1.0       12/02/2019      Arjun Khatri        Initial Draft                                                                   * 
* 1.2       07/10/2020      Aishwarya Todkar    1. Generated SOA from COFFEE service                                            *
*********************************************************************************************************************************/
global Class GenerateDPSoaRest {
   
    public static String responseMsg;
    //List of error logs
    Static List<Error_Log__c>  listErrorLogs ;
    
    public static String generateDPSoa(String regId, String accountId, String buId, String dpClId) {
        listErrorLogs  = new List<Error_Log__c>();
        
        try {
            //if( !Test.isRunningTest() ) {
                responseMsg = FmIpmsRestCoffeeServices.getUnitSoa( regId );
                //responseMsg = '<ul><li> <a href="https://test" target="_blank">Statement Of Account</a></li></ul>';
            //}
        }
        catch( Exception e ) {
            responseMsg = 'Error : ' + e.getMessage();
            //Create Error Log
            createErrorLog( responseMsg, accountId, buId, dpClId );
        }
        /*
        listErrorLogs  = new List<Error_Log__c>();
        String responseMsg = '';
        endurl = Label.DP_Soa_Rest_Endpoint + regId;
        system.debug('generateDPSoa endurl === ' + endurl);
        if(String.isNotBlank(endurl)) {  
            HttpRequest req = new HttpRequest();
            req.setEndpoint(endurl);
            req.setMethod('GET');
            req.setHeader('ACCEPT','application/json');
            req.setHeader('Content-Type', 'application/json');
            req.setTimeout(120000);
            System.debug('generateDPSoa request === ' + req);
            Http http = new Http();
            HTTPResponse res;
            res = http.send(req);
            system.debug('generateDPSoa response === ' + res);
            system.debug('generateDPSoa response body === ' + res.getBody());
            
            
            if(res.getStatusCode() == 200) {
                DPSoaResponseWrapper soaResWrapObj = new DPSoaResponseWrapper();
                soaResWrapObj = (DPSoaResponseWrapper)JSON.deserialize(
                res.getBody(), DPSoaResponseWrapper.class);
                system.debug('generateDPSoa soaResWrapObj === ' + soaResWrapObj);
                if(soaResWrapObj.actions != NULL && soaResWrapObj.actions.size() > 0) {
                    responseMsg = soaResWrapObj.Actions[0].url;
                }
                else {
                    responseMsg = 'Error : actions does not recieved.';
                    //Create Error Log
                    createErrorLog(responseMsg, accountId, buId, dpClId);
                }
            }
            else {
                responseMsg = 'Error : ' + res.getBody();
                 //Create Error Log
                createErrorLog(responseMsg, accountId, buId, dpClId);
            }
        }
        else {
            responseMsg = 'Error : Blank endpoint URL.';
            createErrorLog(responseMsg, accountId, buId, dpClId);
        }*/
        
        if(!listErrorLogs.isEmpty() && listErrorLogs.size() > 0) {
            insert listErrorLogs;
        }
        return responseMsg;
    }
    
    public static void createErrorLog(String responseMsg,String accountId, String buId, String dpClId) {
        Error_Log__c  objError = new Error_Log__c ();
        objError.Error_Details__c  = responseMsg;
        if(String.isNotBlank(accountId)){
            objError.Account__c = Id.valueof(accountId);
        }
        if(String.isNotBlank(buId)){
            objError.Booking_unit__c = Id.valueof(buId);
        }
        if(String.isNotBlank(dpClId)){
            objError.Calling_List__c = Id.valueof(dpClId);
        }
        listErrorLogs.add(objError);
    }
    
    
    /*public Class DPSoaResponseWrapper {
        String responseId, responseTime;
        List<ActionsWrapper> actions;
        public DPSoaResponseWrapper() {
            responseId = '';
            responseTime = '';
            actions =  new List<ActionsWrapper>();
        }
    }
    
    public Class ActionsWrapper {
        String action, method,url;
    }*/
    
    
/*********************************************************************************
 * Method Name : prepareDPSOA
 * Description : Preparing the request body for the DP SOA request.
 * Return Type : String
 * Parameter(s): List of reg Ids.
**********************************************************************************/    
    /*public static string prepareDPSOA(List<id> rids){
    
        String reqno = '123456'+ GetFormattedDateTime(system.now());
        string body  =  '';
        body +=  preparesoapHeader(body); 
        body += '<soapenv:Body>';
        body += '<proc:InputParameters>';
        //body += '<!--Optional:-->';
        body += '<proc:P_REQUEST_NUMBER>'+reqno+'</proc:P_REQUEST_NUMBER>';
        //body += '<!--Optional:-->';
        body += '<proc:P_SOURCE_SYSTEM>SFDC</proc:P_SOURCE_SYSTEM>';
        //body += '<!--Optional:-->';
        body += '<proc:P_REQUEST_NAME>DP_SOA</proc:P_REQUEST_NAME>';
        //body += '<!--Optional:-->';
        body += '<proc:P_REQUEST_MESSAGE>';
        //body += '<!--Zero or more repetitions:-->';
        for(Id recRegId: rids){
            body += '<proc:P_REQUEST_MESSAGE_ITEM>';
            //body += '<!--Optional:SFDC ID-->';
            body += '<proc:PARAM_ID>SFDC-'+recRegId+'</proc:PARAM_ID>';
            //body += '<!--Optional:REG ID-->';
            body += '<proc:ATTRIBUTE3>'+recRegId+'</proc:ATTRIBUTE3>'; 
            body += '</proc:P_REQUEST_MESSAGE_ITEM>';
        }
        body += '</proc:P_REQUEST_MESSAGE>';
        body += '</proc:InputParameters>';
        body += '</soapenv:Body>';
        body += '</soapenv:Envelope>';
        
        return body.trim();
    }
*/
/*********************************************************************************
 * Method Name : preparesoapHeader
 * Description : Preparing the general header for the http request.
 * Return Type : String
 * Parameter(s): List of reg Ids.
**********************************************************************************/
    /*public static string preparesoapHeader(string body){
    
        body += '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xxdc="http://xmlns.oracle.com/apps/ont/soaprovider/plsql/xxdc_process_service_ws/" xmlns:proc="http://xmlns.oracle.com/apps/ont/soaprovider/plsql/xxdc_process_service_ws/process/">';
        body += '<soapenv:Header>';
        body += '<xxdc:SOAHeader>';
        //body += '<!--Optional:-->';
        body += '<xxdc:Responsibility>ONT_ICP_SUPER_USER</xxdc:Responsibility>';
        //body += '<!--Optional:-->';
        body += '<xxdc:RespApplication>ONT</xxdc:RespApplication>';
        //body += '<!--Optional:-->';
        body += '<xxdc:SecurityGroup>standard</xxdc:SecurityGroup>';
        //body += '<!--Optional:-->';
        body += '<xxdc:NLSLanguage>american</xxdc:NLSLanguage>';
        //body += '<!--Optional:-->';
        body += '<xxdc:Org_Id/>';
        body += '</xxdc:SOAHeader>';
        body += '<wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">';
        body += '<wsse:UsernameToken>';
        body += '<wsse:Username>'+usrname+'</wsse:Username>';
        body += '<wsse:Password>'+pwd+'</wsse:Password>';
        body += '</wsse:UsernameToken>';
        body += '</wsse:Security>';
        body += '</soapenv:Header>';
        return body;
    }*/
   
/*********************************************************************************
 * Method Name : invokeSOAGeneration
 * Description : Callout for the SOA generation.
 * Return Type : String
 * Parameter(s): Body as String
**********************************************************************************/
    /*webservice static void invokeSOAGeneration(List<id> regids){

        HTTPRequest req  =  new HTTPRequest();
        req.setMethod('POST');

        String reqXML  =  prepareDPSOA(regids);
        reqXML  =  reqXML.replaceAll('null', '');
        reqXML  =  reqXML.trim();
        req.setbody(reqXML);
        req.setEndpoint(endurl);
        System.debug('>>>>>>>>reqXML>>>>>>>>'+reqXML);
        req.setHeader('Content-Type','text/xml');
        req.setTimeout(120000);
        HTTP http  =  new HTTP();
        if(!Test.isrunningTest()){
            try{
                HTTPResponse res  =  http.send(req);

                System.debug('>>>>>Response>>>>>>'+res.getbody());

            }
            catch(Exception ex) {
                System.debug('Callout error: '+ ex);
                Log__c objLog  =  new Log__c();
                objLog.Description__c ='Ids=='+regids+'-Line No===>' 
                 +  ex.getLineNumber()+'---Message==>'+ex.getMessage();
                objLog.Type__c  =  'Webservice Callout For SOA Generation';
                insert objLog; 
            } 
        } 
    }*/

/*********************************************************************************
 * Method Name : GetFormattedDateTime
 * Description : Method to Get current time in a string.
 * Return Type : String
 * Parameter(s): DateTime
**********************************************************************************/
    //Method to Get current time in a string
    /*public static string GetFormattedDateTime(DateTime dt){
        String yyyy = string.valueof(dt.year());
        String mm = string.valueof(dt.month());
        String dd = string.valueof(dt.day());
        String hh = string.valueof(dt.hour());
        String mi = string.valueof(dt.minute());
        String ss = string.valueof(dt.second());
        String ms = string.valueof(dt.millisecond());
        
        String formatdate = yyyy + mm + dd + hh + mi + ss + ms;
        return formatdate;
    }*/
}