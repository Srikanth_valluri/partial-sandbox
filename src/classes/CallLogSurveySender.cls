/****************************************************************************************************************
Description : Class to send survey email and sms
-----------------------------------------------------------------------------------------------------------------
Version | Date(DD-MM-YYYY)  | Last Modified By  | Comments
-----------------------------------------------------------------------------------------------------------------
1.0     | 24-02-2020        |Aishwarya Todkar   | Initial Draft

1.1     | 12-04-2020        |Aishwarya Todkar   | Removed executing batch

1.2     | 21-07-2020        |Aishwarya Todkar   | Sent survey asynchornously

1.3     | 17-09-2020        |Aishwarya Todkar   | Added empty and blank check
*****************************************************************************************************************/

public with sharing class CallLogSurveySender {

    static String templateName;

/*****************************************************************************************************************
 * Description  : Method to execute batch to send survey email and SMS
 * Parameter(s) : List of Ids
 * Return       : void
 * Called from process builder 'Call Log Survey'
 *****************************************************************************************************************/
    @InvocableMethod
    public static void sendSurvey( List<Id> listIds) {
        if( listIds != NULL && !listIds.isEmpty() ) {
            //Database.executeBatch( new CallLogSurveyBatch( callLogIdSet ), 1 );
            sendSurveyAsync( listIds );
        }//End listIds if
    }//End Method

/*****************************************************************************************************************
 * Description  : Method send survey emails asynchornously
 * Parameter(s) : List call log id
 * Return       : void
 *****************************************************************************************************************/
    @future(callout = true)
    public static void sendSurveyAsync( List<Id> listIds ) {
        List<EmailMessage> lstEmails = new List<EmailMessage>();
        for( Call_Log__c objLog : [ SELECT 
                                        Id 
                                        , Account__c 
                                        , Account__r.Name 
                                        , Account__r.Person_Business_Email__c 
                                        , Account__r.Mobile_Person_Business__c 
                                        , Called_Number__c 
                                        , Calling_Number__c  
                                        , Call_Recording_URL__c  
                                        , Call_Type__c 
                                        , OwnerId
                                        , Owner.Name 
                                    FROM 
                                        Call_Log__c 
                                    WHERE 
                                        Id IN : listIds ] ) {
            System.debug('objLog--' + objLog);
            //Sending survey emails
            List<EmailMessage> tempListEmails = sendSurveyEmail( objLog );
            
            if( tempListEmails != null && !tempListEmails.isEmpty() )
                lstEmails.addAll( tempListEmails );

            //Sending survey sms
            sendSurveySMS( objLog );

        } //End callLogList loop

        if( lstEmails != null && !lstEmails.isEmpty() ) {
            if( !Test.isRunningTest() ) {
                insert lstEmails;
            }
        }
    }
/*****************************************************************************************************************
 * Description : Method send survey emails
 * Parameter(s) : Call Log Record
 * Return : List<EmailMessage>
 *****************************************************************************************************************/
    public static List<EmailMessage> sendSurveyEmail( Call_Log__c objLog ) {
        String metaDataRec =  'Call Log Survey';
        System.debug('metaDataRec=='+ metaDataRec);

        List<SendGrid_Email_Details__mdt> listSendGridDetails = CRM_SurveyEmailsSender.getSendGridDetails( metaDataRec );
        if( listSendGridDetails != null &&  !listSendGridDetails.isEmpty() ) {
            
            SendGrid_Email_Details__mdt objSendGridCs = listSendGridDetails[0];
            System.debug( 'objSendGridCs:::  ' + objSendGridCs);
            //Get Email Template
            List<EmailTemplate> listEmailTemplate = new List<EmailTemplate>( [SELECT 
                                                                                    Id
                                                                                    , Name
                                                                                    , Subject
                                                                                    , Body
                                                                                    , HtmlValue
                                                                                    , TemplateType
                                                                                    , DeveloperName
                                                                                FROM 
                                                                                    EmailTemplate 
                                                                                WHERE 
                                                                                    Name =: objSendGridCs.Email_Template__c 
                                                                                LIMIT 1 
                                                                            ] );
            system.debug('listEmailTemplate  === ' + listEmailTemplate );
    
            if( listEmailTemplate != null && !listEmailTemplate.isEmpty() ) {

                EmailTemplate emailTemplateObj = listEmailTemplate[0];
                
                templateName =  emailTemplateObj.DeveloperName;

                String toAddress = objLog.Account__r.Person_Business_Email__c;

                String toName = String.isNotBlank( objSendGridCs.To_Name__c ) ? objSendGridCs.To_Name__c : '';

                String ccAddress = String.isNotBlank( objSendGridCs.CC_Address__c ) ? objSendGridCs.CC_Address__c : '';

                String ccName = String.isNotBlank( objSendGridCs.CC_Name__c ) ? objSendGridCs.CC_Name__c : '';

                String bccAddress = String.isNotBlank( objSendGridCs.Bcc_Address__c ) ? objSendGridCs.Bcc_Address__c : '';

                String bccName = String.isNotBlank( objSendGridCs.Bcc_Name__c ) ? objSendGridCs.Bcc_Name__c : '';

                String fromAddress = String.isNotBlank( objSendGridCs.From_Address__c ) ? objSendGridCs.From_Address__c : '';

                String fromName = String.isNotBlank( objSendGridCs.From_Name__c ) ? objSendGridCs.From_Name__c : '';

                String replyToAddress = String.isNotBlank( objSendGridCs.Reply_To_Address__c ) ? objSendGridCs.Reply_To_Address__c : '';

                String replyToName = String.isNotBlank( objSendGridCs.Reply_To_Name__c ) ? objSendGridCs.Reply_To_Name__c : '';

                String contentType = 'text/html';
                
                String subject = emailTemplateObj.Subject;

                String contentValue = CRM_SurveyEmailsSender.replaceMergeFields( 
                                                        emailTemplateObj.HtmlValue      
                                                        , objLog.Id                     // Record Id
                                                        , objLog.OwnerId                // CRE Id
                                                        , objLog.Account__c             // Account Id
                                                        , objLog.Owner.Name             // CRE Name
                                                        , objLog.Account__r.Name        // Customer Name
                                                        , templateName                  // Template Name
                                                        );

                String contentBody = CRM_SurveyEmailsSender.replaceMergeFields( 
                                                        emailTemplateObj.Body   
                                                        , objLog.Id                     // Record Id
                                                        , objLog.OwnerId                // CRE Id
                                                        , objLog.Account__c             // Account Id
                                                        , objLog.Owner.Name             // CRE Name
                                                        , objLog.Account__r.Name        // Customer Name
                                                        , templateName                  // Template Name
                                                        );
                
                List<EmailMessage> lstEmails;
                //Call method to send an email via SendGrid
                if(String.isNotBlank(toAddress)) {
                            
                    lstEmails = CRM_SurveyEmailsSender.sendEmailsBySendGrid( toAddress, toName
                                                                            , ccAddress, ccName
                                                                            , bccAddress, bccName
                                                                            , subject, ''
                                                                            , fromAddress, fromName
                                                                            , replyToAddress, replyToName
                                                                            , contentType, contentValue
                                                                            , '', contentBody
                                                                            , null, objLog.Account__c );
                    return lstEmails;
                }
            }                                                             
        }// End listSendGridDetails if
        return null;
    }

/*****************************************************************************************************************
 * Description : Method send survey SMS
 * Parameter(s) : Call Log Record
 * Return : None
 *****************************************************************************************************************/
    public static void sendSurveySMS( Call_Log__c objLog ) {
        String siteUrl = CRM_SurveyEmailsSender.replaceMergeFields( 
            Label.CRM_Survey_Site_Url   // Site Url
            , objLog.Id                 // Record Id
            , objLog.OwnerId            // CRE Id
            , objLog.Account__c         // Account Id
            , objLog.Owner.Name         // CRE Name
            , objLog.Account__r.Name    // Customer Name
            , templateName              // Template Name
        );
        
        
        RebradnlyResponseWrapper objResWrap = RebrandlyService.getShortenUrl( siteUrl );                                                        
        if( objResWrap != null ) {
            String shortUrl = objResWrap.shortUrl;                                                             
            String smsContents = Label.Call_Log_Survey_SMS;
            String phoneNumber = objLog.Account__r.Mobile_Person_Business__c;
            if( String.isNotBlank( phoneNumber ) ) {
                if( phoneNumber.startsWith( '00' ) ) {
                    phoneNumber = phoneNumber.removeStart( '00' );
                }
                else if( phoneNumber.startsWith( '0' ) ) {
                    phoneNumber = phoneNumber.removeStart( '0' );
                }
                if(String.isNotBlank(shortUrl)) {
                    smsContents = smsContents.replace('{Survey Link}', shortUrl);
                    CRM_SurveySMSSender.sendSurveySMS( smsContents, phoneNumber, objLog.Account__c );
                }
            }
        }
    }
}