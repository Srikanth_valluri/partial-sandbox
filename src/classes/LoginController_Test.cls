/**************************************************************************************************
* Name               : LoginController_Test                                                
* Description        : An apex page controller for LoginController                                          
* Created Date       : 29/03/2018                                                                       
* Created By         : Rajnish                                                                 
* Last Modified Date :                                                                            
* Last Modified By   :                                                                            
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR                    DATE                                                                    
* 1.0         Rajnish                29/03/2018                                                           
**************************************************************************************************/
@isTest
public class LoginController_Test {

    static void init(){
        cookie Name = new Cookie('username', 'test', null, 365, false);
        //cookie PasswordCokies = new Cookie('password', 'test', null, 365, false);
        ApexPages.currentPage().setCookies(new Cookie[] {Name});
        
        
    }
    
    @isTest static void loginGuestUser() {
        
        Test.StartTest();
        init();
        
        LoginController damacLogin = new LoginController();
        damacLogin.rememberMe = true;
        damacLogin.username = 'test@damac.com';
        damacLogin.password = 'salesforce1';
        damacLogin.login();
        
        PageReference pg = new PageReference('/Damac_Home');
        //system.assertEquals(damacLogin.redirectToHome(),pg);
        damacLogin.redirectToHome();
        
        Test.stopTest();
        
    }
}