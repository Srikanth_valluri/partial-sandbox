/*========================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By      | Comments
--------------------------------------------------------------------------------------------------------
1.0     |   16-11-2020    | Anand Venkitakrishnan  | New version of GetAmenities API with additional accountId param
1.1     |   14-12-2020    | Anand Venkitakrishnan  | Fixed issues related to user tenancy checks
1.2     |   17-12-2020    | Shubham Suryawanshi    | Added the condition-check for byPassing service charge overdue check for unit with Allow_Amenity_Booking__c flag marked as true
1.3     |   20-12-2020    | Shubham Suryawanshi    | Updated the IF check for residingOwner boolean flag
1.4     |   14-01-2021    | Shubham Suryawanshi    | Updated the IF check for isTenant boolean flag & ruled out the Tenant profile level check
--------------------------------------------------------------------------------------------------------
*******************************************************************************************************/   

@RestResource(urlMapping='/getAmenitiesList/*')
global class GetAmenitiesByUnitAndAccount_API{
  
    public static final string NO_AMENITY_FOR_BUILDING = 'No amenities available for the selected building';
    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong',
        7 => 'Tenancy contract expired'
    };
    
    @HttpPost
    global static FinalReturnWrapper GetAmenitiesForUnit() {
    
        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);
    
        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        cls_data objData = new cls_data();
        cls_meta_data objMeta = new cls_meta_data();
        List<cls_get_amenity_list> listAmenities = new List<cls_get_amenity_list>();
    
        if(!r.params.containsKey('bookingUnitId')) {

            objMeta.message = 'Missing parameter : bookingUnitId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
           //objMeta.is_success = false;

            returnResponse.meta_data = objMeta;
            return returnResponse;

        }
        else if(r.params.containsKey('bookingUnitId') && String.isBlank(r.params.get('bookingUnitId'))) {
            objMeta.message = 'Missing parameter value: bookingUnitId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;

            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
    else if(!r.params.containsKey('accountId')) {
            objMeta.message = 'Missing parameter : accountId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
      
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(r.params.containsKey('accountId') && String.isBlank(r.params.get('accountId'))) {
            objMeta.message = 'Missing parameter value: accountId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
      
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        
        String accountId = r.params.get('accountId');
        
        List<User> usr = [SELECT id
                               , Name 
                               , Username
                               , Profile.Name 
                               , Contact.AccountId 
                         FROM User
                         WHERE Contact.AccountId =: accountId];
        System.debug('usr:'+usr);
    
        if(usr == null || usr.isEmpty()) {
            objMeta.message = 'No account present for given accountId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
      
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        
        //Commented this check as was not accurate due to profiles assigned to user. - 14/01/2021
        // Boolean isTenant = usr[0].profile.Name.contains('Tenant Community') ? true : false;
        // System.debug('isTenant:'+isTenant);
        
        if(String.isNotBlank(r.params.get('bookingUnitId'))) {
            System.debug('bookingUnitId:'+r.params.get('bookingUnitId'));
      
            List<Booking_Unit__c> lstBU = [SELECT id 
                                                , Unit_Name__c
                                                , Registration_Id__c 
                                                , Booking__r.Account__c
                                                , Booking__r.Account__r.Party_ID__c
                                                , Property_Name__c
                                                , ByPassFMReminder__c
                                                , SC_From_RentalIncome__c
                                                , CM_Units__c
                                                , Lease_Expiry_Date__c
                                                , Owner__c
                                                , Resident__c
                                                , Tenant__c
                                                , Allow_Amenity_Booking__c
                                                , Resident_Type__c
                                           FROM Booking_Unit__c
                                           WHERE id =: r.params.get('bookingUnitId')];
            if(lstBU.size() > 0) {
                Boolean residingTenant = false;
                Boolean validTenant = false;
                Boolean residingOwner = false;

                Boolean isTenant = lstBU[0].Tenant__c != null ? true : false;
                System.debug('isTenant:'+isTenant);
                
                if(isTenant) {
                    //Check if tenant is residing in the unit to allow/block amenity booking
                    if(lstBU[0].Tenant__c == accountId && lstBU[0].Tenant__c == lstBU[0].Resident__c) {
                        residingTenant = true;
                    }
                    
                    if(residingTenant) {
                        //Check tenancy contract expiry
                        Boolean isContractExpired = false;
                        
                        //1.1
                        if(lstBU[0].Lease_Expiry_Date__c != NULL) {
                            isContractExpired = lstBU[0].Lease_Expiry_Date__c >= System.today() ? false : true;
                        }
                        
                        if(isContractExpired) {
                            objMeta.message = 'You won\'t be able to book the amenity as your tenancy contract expired. Please submit a tenancy renewal service request.';
                            objMeta.status_code = 7;
                        }
                        else {
                            validTenant = true;
                        }
                    }
                    else {
                        objMeta.message = 'You cannot book an amenity for this unit as it has a different tenant';
                        objMeta.status_code = 2;
                    }
                }
                else {
                    //Check if current account is owner
                    if(lstBU[0].Booking__r.Account__c == accountId) {
                        residingOwner = true;
                    }
                    /*Below is the previous logic for - residingOwner flag (Ruled out)*/
                    //Check if owner is residing in the unit to allow/block amenity booking
                    //if(lstBU[0].Owner__c == accountId && lstBU[0].Owner__c == lstBU[0].Resident__c) {
                    //    residingOwner = true;
                    //}
                    
                    if(!residingOwner) {
                        //objMeta.message = 'You cannot book an amenity for this unit as it has a tenant';
                        objMeta.message = 'You cannot book an amenity as the owner to this unit is different';
                        objMeta.status_code = 2;
                    }
                }
                
                if(validTenant || residingOwner) {
                    //Check service fee overdue
                    Boolean isDuePending = isServiceFeeDuePending(lstBU[0]);
                    if(!isDuePending) {
                        //fetch all amenities for given BU
                        List<Resource__c> lstRes = GetAllAmenitiesForBU(lstBU[0], false, '');
                        
                        List<String> lstFootballRes = label.FootballAmenityForURLRedirect.split(',');
                        System.debug('lstFootballRes : ' + lstFootballRes);
                        
                        Boolean isSandbox = [SELECT IsSandbox FROM Organization].IsSandbox;
                        System.debug('IsSandbox is: '+isSandbox);
                        
                        for(Resource__c objRes : lstRes) {
                            cls_get_amenity_list objAmenity = new cls_get_amenity_list();
                            
                            System.debug('objRes.Name : ' + objRes.Name);
                            if(lstFootballRes.contains(objRes.Name)) {
                                objAmenity.is_booking_enabled = false;
                                objAmenity.booking_reason_not_allowed = 'Booking for selected Amenity is not handled by Damac';
                                if(isSandbox) {
                                    objAmenity.partner_site_url = 'https://google.com/';
                                }
                                else {
                                    objAmenity.partner_site_url = 'https://invictusacademy.skedda.com/';
                                    //https://invictusacademy.skedda.com/
                                }
                            }
                            else {
                                objAmenity.is_booking_enabled = true; 
                            }
                            
                            objAmenity.id = objRes.id;
                            objAmenity.amenity_name = objRes.Name;
                            objAmenity.location_latitude = String.isNotBlank(objRes.Location_Latitude__c) ? Decimal.valueOf(objRes.Location_Latitude__c) : null;
                            objAmenity.location_longitude = String.isNotBlank(objRes.Location_Longitude__c) ? Decimal.valueOf(objRes.Location_Longitude__c) : null;
                            listAmenities.add(objAmenity);
                        }
                        
                        if(lstRes.size() > 0) {
                            objMeta.message = 'Successful';
                            objMeta.status_code = 1;
                        }
                        else {
                            //objMeta.message = 'No Amenities available for given bookingUnitId';
                            objMeta.message = NO_AMENITY_FOR_BUILDING;/*as per bug : 1978*/
                            objMeta.status_code = 2;
                        }
                        
                    }
                    else {
                        //Due pending
                        objMeta.message = 'Service fee overdue';
                        objMeta.status_code = 2;
                    }
                }
            }
            else {
                objMeta.message = 'No Booking Unit found for the given bookingUnitId';
                objMeta.status_code = 3;
            }
        }
    
        objMeta.title = objMeta.message == NO_AMENITY_FOR_BUILDING ? 'Sorry!' : mapStatusCode.get(objMeta.status_code);/*As per Bug : 1978*/
        objMeta.developer_message = null;
    
        objData.amenities = listAmenities;
    
        returnResponse.meta_data = objMeta;
        returnResponse.data = objData;
    
        System.debug('returnResponse:: ' + returnResponse);
        
        return returnResponse;
    }
  
    public static boolean isServiceFeeDuePending(Booking_Unit__c objBU) {
    
        /*Below IF check is added for bypassing service charge overdue check for units with Allow_Amenity_Booking__c marked as TRUE - as discussed with Charith*/
        System.debug('objBU.Allow_Amenity_Booking__c : ' + objBU.Allow_Amenity_Booking__c);
        if(objBU.Allow_Amenity_Booking__c) {
            return false;
        }    

        Boolean isDuePresent;
        FmIpmsRestServices.DueInvoicesResult dues = FmIpmsRestServices.getDueInvoices(objBU.Registration_Id__c, objBU.Booking__r.Account__r.Party_ID__c, objBU.Property_Name__c);
        System.debug('-->> Invoice dues.....'+ dues.dueAmountExceptCurrentQuarter);
        
        //Added the CM_Units, ByPassFMReminder__c, SC_From_RentalIncome__c check - Shubham 11/03/2020
        System.debug('due in label :'+label.ServiceChargeDueLimitInAmenityBooking);
    
        Decimal dueLimit = Decimal.valueOf(label.ServiceChargeDueLimitInAmenityBooking);
        System.debug('dueLimit: ' + dueLimit);
    
        //null == dues ||- The CM_Units, ByPassFMReminder__c, SC_From_RentalIncome__c check isremoved as per discussed with Shinu- Shubham 01/10/2020 
        if( dues.dueAmountExceptCurrentQuarter == null || dues.dueAmountExceptCurrentQuarter < dueLimit ) {
            isDuePresent = false;
        }
        else {
            isDuePresent = true;
        }
    
        System.debug('isDuePresent :'+isDuePresent);
        return isDuePresent;
    }
  
    public static List<Resource__c> GetAllAmenitiesForBU(Booking_Unit__c objBU, boolean fetchSignleAmenity, String resourceId) {
        
        System.debug('fetchSignleAmenity:: ' + fetchSignleAmenity);
    
        List<Resource__c> lstAmenities = new List<Resource__c>();
        if(objBU != null) {
            //Get building Id
      
            if(String.isNotBlank(objBU.Unit_Name__c)) {
                String buildingName = objBU.Unit_Name__c.split('/')[0];
                List<Location__c> lstBuildings = new List<Location__c>();
                lstBuildings = [SELECT  Id
                                        , Name
                                        , Property_Name__c
                                        , ( SELECT  FM_User__c
                                                    , FM_User__r.Email
                                                    , FM_Role__c
                                                    , FM_User__r.Name
                                            FROM FM_Users__r
                                            WHERE FM_Role__c = 'FM Admin'
                                            OR FM_Role__c = 'Master Community Admin'
                                            LIMIT 1)
                                FROM Location__c
                                WHERE Name =: buildingName
                                AND RecordType.DeveloperName = 'Building'
                                LIMIT 1];
                System.debug('===lstBuildings.size()==='+lstBuildings.size());
                if(lstBuildings.size() >0) {
                    lstAmenities =  getAvailableResources(lstBuildings[0].Property_Name__c, lstBuildings[0].Id, fetchSignleAmenity, resourceId);
                }
            }
        }

        return lstAmenities;
    }

    public Static List<Resource__c> getAvailableResources (String propertyId, String buildingId, boolean isSingleResource, String resourceId) {
    
        List<Resource__c> lstResources = new List<Resource__c>();
        
        //Prepare dynamic query
        System.debug('propertyId: '+propertyId);
        System.debug('buildingId: '+buildingId);
        System.debug('isSingleResource: '+isSingleResource);
        System.debug('resourceId: '+resourceId);
    
        String query = 'SELECT Name,ID,Contact_Person_Name__c,Contact_Person_Email__c,';
        query += 'Amenity_Admin__c,Amenity_Manager__c,Description__c,No_of_advance_closing_days__c,';
        query += 'Public_Amenity_Name__c, Chargeable_Fee_Slot_for_Guest__c,Deposit_for_Guest__c,';
        query += 'Contact_Person_Phone__c,No_of_advance_booking_days__c,Non_Operational_Days__c, Location_Latitude__c, Location_Longitude__c,';
        query += 'Chargeable_Fee_Slot__c,Deposit__c,Max_no_of_people_hosts_guests__c,No_of_deposit_due_days__c,';
        query += '(SELECT Non_operational_date__c FROM Non_operational_day__r)';
        query += ' FROM Resource__c ';
        query += ' WHERE Resource_Type__c = \'FM Amenity\'';
        //query += ' AND ((Chargeable_Fee_Slot__c = null OR Chargeable_Fee_Slot__c = 0.0 ) AND (Deposit__c = null OR Deposit__c = 0.0))';    //to filter out paid amenities  (Ruled out on 27 Jan'21 to include paid amenities as well)
        //query += ' AND ID IN (SELECT Resource__c from Resource_Slot__c)';
        if(!isSingleResource) {
            query += ' AND  ID IN  (SELECT Resource__c FROM Resource_Sharing__c';
            query += ' WHERE (Property__c =: propertyId AND Property__c != NULL)';
            query += ' OR (Building__c =: buildingId AND Building__c != NULL))';
        }
        else {
            query += ' AND Id =: resourceId';
        }
        System.debug('===query===='+query);
        lstResources = Database.query(query);
    
        System.debug('lstResources: '+lstResources);
    
        return lstResources;
    }
  
  global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }
  
    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message;   
    }
  
    public class cls_data {
        public cls_get_amenity_list[] amenities;
    }
  
    public class cls_get_amenity_list {
        public String id;
        public String amenity_name;
        public boolean is_booking_enabled;
        public String booking_reason_not_allowed;
        public String partner_site_url;
        public Decimal location_latitude;
        public Decimal location_longitude;
    }
}