/*
* Description - Test class developed for PenaltyWaiverUtility
*
* Version            Date            Author            Description
* 1.0              20/11/2017      Ashish Agarwal     Initial Draft
*/

@isTest
public class PenaltyWaiverUtilityTest { 
    
    static testMethod void testRelatedBookingUnits() {
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        //Insert Options
         List<Option__c> lstOptions = TestDataFactory_CRM.createOptions( lstBookingUnits );
         insert lstOptions ;
        
        //Insert Customer Setting Records
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveUnitCustomSetting( new list<String>{ 'Agreement executed by DAMAC' } );
        insert lstActiveStatus ;
        
        //Insert Cases for the units
        list<Case> lstCases = new list<Case>();
        Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
            objCase.Booking_Unit__c = objUnit.Id;
            lstCases.add( objCase );
        }
        insert lstCases ;
        
        list<SR_Attachments__c> lstAttachment = new list<SR_Attachments__c>();
        for( Case objCase : lstCases ) {
            lstAttachment.add( TestDataFactory_CRM.createCaseDocument( objCase.Id, PenaltyWaiverUtility.ATTACH_TYPE_FULL_CRF ) );
        }
        insert lstAttachment ;
        
        Map<Id, Booking_Unit__c> mapAllRelatedBookingUnits;
        test.startTest();
        mapAllRelatedBookingUnits = PenaltyWaiverUtility.getAllRelatedBookingUnits(objAcc.Id);
        test.stopTest();
        //System.assertEquals(25, mapAllRelatedBookingUnits.size()); 
    }
    
    static testMethod void testValidRecordTypeId() {
        Id recordTypeID;
        test.startTest();
        recordTypeID = PenaltyWaiverUtility.getRecordTypeId('Business_Account', 'Account');
        test.stopTest();
        
        List<RecordType> lstRecordTypes = [SELECT Id, Name, DeveloperName, SobjectType  
                                            FROM RecordType
                                            WHERE DeveloperName = :'Business_Account'];
        //System.assertEquals(recordTypeID, lstRecordTypes[0].Id); 
    }
    
    static testMethod void testInvalidRecordTypeId() {
        Id recordTypeID;
        test.startTest();
        recordTypeID = PenaltyWaiverUtility.getRecordTypeId('abc', 'Account');
        test.stopTest();
        
        List<RecordType> lstRecordTypes = [SELECT Id, Name, DeveloperName, SobjectType  
                                            FROM RecordType
                                            WHERE DeveloperName = :'abc'];
        //System.assertEquals(0, lstRecordTypes.size()); 
    }
    
    static testMethod void testFetchAttachmentFromCases() {
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        //Insert Options
         List<Option__c> lstOptions = TestDataFactory_CRM.createOptions( lstBookingUnits );
         insert lstOptions ;
        
        //Insert Customer Setting Records
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveUnitCustomSetting( new list<String>{ 'Agreement executed by DAMAC' } );
        insert lstActiveStatus ;
        
        //Insert Cases for the units
        list<Case> lstCases = new list<Case>();
        Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
            objCase.Booking_Unit__c = objUnit.Id;
            lstCases.add( objCase );
        }
        insert lstCases ;
        
        list<SR_Attachments__c> lstAttachment = new list<SR_Attachments__c>();
        for( Case objCase : lstCases ) {
            lstAttachment.add( TestDataFactory_CRM.createCaseDocument( objCase.Id, PenaltyWaiverUtility.ATTACH_TYPE_FULL_CRF ) );
        }
        insert lstAttachment ;
        
        test.startTest();
        map<Id,SR_Attachments__c> mapIdVsSRAttachments = PenaltyWaiverUtility.fetchCaseAttachments(lstCases[0].Id);
        test.stopTest();
    }
    
    static testMethod void testGetTasks() {
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        //Insert Options
         List<Option__c> lstOptions = TestDataFactory_CRM.createOptions( lstBookingUnits );
         insert lstOptions ;
        
        //Insert Customer Setting Records
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveUnitCustomSetting( new list<String>{ 'Agreement executed by DAMAC' } );
        insert lstActiveStatus ;
        
        //Insert Cases for the units
        list<Case> lstCases = new list<Case>();
        Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
            objCase.Booking_Unit__c = objUnit.Id;
            lstCases.add( objCase );
        }
        insert lstCases ;
        
        set<Id> setCaseId = new set<Id>();
        list<Task> lstTasks;
        for(Case caseObj : lstCases) {
            setCaseId.add(caseObj.Id);
        }
        test.startTest();
        lstTasks = PenaltyWaiverUtility.getTasks(setCaseId);
        test.stopTest();
        //System.assert(lstTasks.size() > 0);
    }
    
    static testMethod void testGetCaseMap() {
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        //Insert Options
         List<Option__c> lstOptions = TestDataFactory_CRM.createOptions( lstBookingUnits );
         insert lstOptions ;
        
        //Insert Customer Setting Records
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveUnitCustomSetting( new list<String>{ 'Agreement executed by DAMAC' } );
        insert lstActiveStatus ;
        
        //Insert Cases for the units
        list<Case> lstCases = new list<Case>();
        Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
            objCase.Booking_Unit__c = objUnit.Id;
            lstCases.add( objCase );
        }
        insert lstCases ;
        
        set<Id> setCaseId = new set<Id>();
        map<Id, Case> mapIdVsCase;
        for(Case caseObj : lstCases) {
            setCaseId.add(caseObj.Id);
        }
        test.startTest();
        mapIdVsCase = PenaltyWaiverUtility.getCaseMap(setCaseId);
        test.stopTest();
        //System.assert(mapIdVsCase.size() > 0);
    }
    
    static testMethod void testRelatedCasesOfBookingUnit() {
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        //Insert Options
         List<Option__c> lstOptions = TestDataFactory_CRM.createOptions( lstBookingUnits );
         insert lstOptions ;
        
        //Insert Customer Setting Records
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveUnitCustomSetting( new list<String>{ 'Agreement executed by DAMAC' } );
        insert lstActiveStatus ;
        
        //Insert Cases for the units
        list<Case> lstCases = new list<Case>();
        Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
            objCase.Booking_Unit__c = objUnit.Id;
            lstCases.add( objCase );
        }
        insert lstCases ;
        
        list<Case> lstAllRelatedCase;
        test.startTest();
        lstAllRelatedCase = PenaltyWaiverUtility.getAllRelatedCases(lstBookingUnits[0].Id);
        test.stopTest();
        //System.assert(lstAllRelatedCase.size() > 0);
    }
    
    static testMethod void testRelatedJunctions() {
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        //Insert Options
         List<Option__c> lstOptions = TestDataFactory_CRM.createOptions( lstBookingUnits );
         insert lstOptions ;
        
        //Insert Customer Setting Records
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveUnitCustomSetting( new list<String>{ 'Agreement executed by DAMAC' } );
        insert lstActiveStatus ;
        
        //Insert Cases for the units
        list<Case> lstCases = new list<Case>();
        Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
            objCase.Booking_Unit__c = objUnit.Id;
            lstCases.add( objCase );
        }
        insert lstCases ;
        
        list<SR_Booking_Unit__c> lstAllRelatedJunctions;
        test.startTest();
        lstAllRelatedJunctions = PenaltyWaiverUtility.getAllRelatedJunctions(lstBookingUnits[0].Id);
        test.stopTest();
        //System.assert(lstAllRelatedJunctions.size() > 0);
    }
    
    static testMethod void testPWCasesRelatedToAccount() {
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        //Insert Options
         List<Option__c> lstOptions = TestDataFactory_CRM.createOptions( lstBookingUnits );
         insert lstOptions ;
        
        //Insert Customer Setting Records
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveUnitCustomSetting( new list<String>{ 'Agreement executed by DAMAC' } );
        insert lstActiveStatus ;
        
        //Insert Cases for the units
        list<Case> lstCases = new list<Case>();
        Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
            objCase.Booking_Unit__c = objUnit.Id;
            objCase.Approval_Status__c = 'Approved';
            objCase.Status = 'Closed';
            objCase.IPMS_Updated__c = true;
            lstCases.add( objCase );
        }
        insert lstCases ;
        
        list<Case> lstAllRelatedCases;
        test.startTest();
        lstAllRelatedCases = PenaltyWaiverUtility.getAllPWCasesFromAccount(objAcc.Id);
        test.stopTest();
        //System.assert(lstAllRelatedCases.size() > 0);
    }
    
    static testMethod void testGetAllJointBuyer() {
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        List<Buyer__c> lstBuyers = TestDataFactory_CRM.createBuyer( lstBookings[0].Id, 5, objAcc.Id );//new List<Buyer__c>();
        /*for(Integer i = 0; i <= 5; i++) {
            Buyer__c objBuyer = new Buyer__c(First_Name__c = 'Test', Booking__c = lstBookings[0].Id);
            lstBuyers.add(objBuyer);
        }*/
        insert lstBuyers;
        
        list<Buyer__c> lstAllBuyers;
        test.startTest();
        lstAllBuyers = PenaltyWaiverUtility.getAllJointBuyer(lstBookings[0].Id);
        test.stopTest();
        //System.assertEquals(lstAllBuyers.size(), 5);
    }
    
    static testMethod void testFetchingHelpDocUrl() {
        
        /*Document objDoc = new Document();
        objDoc.DeveloperName = 'Penalty_Waiver_Help';
        objDoc.Name = 'Penalty_Waiver_Help';
        insert objDoc ;
        */
        test.startTest();
        	PenaltyWaiverUtility.fetchHelpDocURL( 'Penalty_Waiver_Help' );
        test.stopTest();
        //System.assertEquals(recordTypeID, lstRecordTypes[0].Id); 
    }
}