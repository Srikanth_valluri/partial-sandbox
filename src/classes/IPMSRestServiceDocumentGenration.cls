public with sharing class IPMSRestServiceDocumentGenration {
    @testVisible private static final String LUSAIL_SPA = '/customer/LUSAIL_SPA?registration_id={0}';
     @testVisible private static final String PUBLIC_SITE_URL    = String.isNotBlank(System.Label.OfficePreviewUrl)?System.Label.OfficePreviewUrl:'';
    
    
    public class ResponseLines {
        public String documentName;
        public String language;
        public String url;
    }
    
    public static ResponseWrapper parse(String json) {
        return (ResponseWrapper) System.JSON.deserialize(json, ResponseWrapper.class);
    }
    

    public class ResponseWrapper {
        public String responseId;
        public String responseTime;
        public String status;
        public String responseMessage;
        public Integer elapsedTimeMs;
        public List<ResponseLines> responseLines;
        public Boolean complete;
        //public List<Error_Log__c>  Error_log_lst;
        
        /*public ResponseWrapper( String responseId, String responseTime, String status, String responseMessage,
                                Integer elapsedTimeMs, List<ResponseLines> responseLines, Boolean complete  ){
            this.responseId = responseId;
            this.responseTime = responseTime;
            this.status = status;
            this.responseMessage = responseMessage;
            this.elapsedTimeMs = elapsedTimeMs;
            this.responseLines = responseLines;
            this.complete = complete;
        }*/
        /*public ResponseWrapper( List<Error_Log__c> errLst ){
            this.Error_log_lst = errLst;
        }*/

        public ResponseWrapper(){}
    }
    
    
    
 
    public static string genrateLUSAIL_SPA_Doc(Booking_Unit__c objBu){
        system.debug(' In method  genrateLUSAIL_SPA_Doc');
        String LUSAIL_SPAUrl = ''; 
        FmIpmsRestCoffeeServices.updateCustomSetting = false;

        if(objBu!= null && String.isNotBlank(objBu.Registration_ID__c) ){
            system.debug('if');
            //ResponseWrapper resultObj;
            //R-66352_P-1000215_D-docname_L-Primary Buyer's Name Party_Id__c,Primary_Buyer_s_Name__c
            String fileName = 'R-'+ objBu.Registration_ID__c +
                             '_P-'+objBu.Party_Id__c +'_D-LUSAIL_SPA' + '_L-'+objBu.Primary_Buyer_s_Name__c;
            ResponseWrapper ResponseWrapperObj;
            
            HttpResponse res = FmIpmsRestCoffeeServices.callout(String.format(LUSAIL_SPA, new List<String>{ objBu.Registration_ID__c }));
            if(res.getStatusCode() == 200 || res.getStatusCode() == 201){
                ResponseWrapperObj = new ResponseWrapper();
                if( res.getBody() != null ){
                    ResponseWrapperObj = parse(res.getBody());
                    LUSAIL_SPAUrl =(ResponseWrapperObj != null)? fetchResponseUrl(ResponseWrapperObj):'';
                    System.debug('LUSAIL_SPAUrl--' +LUSAIL_SPAUrl)  ;

                    /*if(String.isNotBlank(LUSAIL_SPAUrl)){
                            //Http callout

                        String accessToken = FmIpmsRestCoffeeServices.getNewBearerToken();
                        System.debug('AccessToken is' + accessToken);

                        HttpRequest req = new HttpRequest();
                        req.setEndpoint(LUSAIL_SPAUrl);
                        req.setHeader('Accept', 'application/json');
                        req.setMethod('GET');
                        req.setHeader('Authorization','Bearer' + accessToken);
                        req.setTimeout(120000);
                        HttpResponse response = new Http().send(req);
                        System.debug('Status Code = ' + response.getStatusCode());
                        System.debug('Pdf Body = ' + response.getBodyAsBlob());

                        ApexPages.Message pageMessage;

                        if(response.getStatusCode() == 200) {
                            system.debug('attachment  before insert ');
                            Blob pdfBlob = response.getBodyAsBlob();
                            Attachment attach = new Attachment();
                            attach.contentType = 'application/pdf';
                            attach.name = 'LUSAIL_SPA.pdf';
                            attach.parentId = objBu.Id;
                            attach.body = pdfBlob;
                            insert attach;
                            system.debug('attachment insert ');
                            
            
                            /*Blob  pdfBlob;
                            pdfBlob = EncodingUtil.base64Decode(EncodingUtil.base64Encode(response.getBodyAsBlob()))  ;
                            
                            
                            Office365RestService.ResponseWrapper responseWrap = new Office365RestService.ResponseWrapper();
                            responseWrap = Office365RestService.uploadToOffice365Result(attach.body, fileName, '', '', '', '');
                            if( responseWrap != null  && String.isNotBlank(responseWrap.id)){
                            LUSAIL_SPAUrl = PUBLIC_SITE_URL + responseWrap.id;
                            }*/
                        
                    

                    }
            
                
           }else if (res.getStatusCode() != 200 && res.getStatusCode() != 201) {
                List<Error_Log__c> Error_log_lst = new List<Error_Log__c>();
                Error_Log__c errlog = new Error_Log__c(
                                        Process_Name__c = 'LUSAIL_SPA_Doc Generation',
                                        Account__c = CustomerCommunityUtils.customerAccountId,
                                        Error_Details__c = 'res:\n' + res.getBody()
                );
    
                Error_log_lst.add(errlog);
                insert Error_log_lst;
                
            }
            return LUSAIL_SPAUrl;
        }else{
            return null;
        }

    }
            
    

    



    private static String fetchResponseUrl(ResponseWrapper ResponseWrapperObj) {
        if(ResponseWrapperObj != null){
            List<ResponseLines> ResponseLinesLst = new List<ResponseLines>();
            ResponseLinesLst= ResponseWrapperObj.responseLines;
            if (ResponseLinesLst.size() > 0) {
                return ResponseLinesLst[0].url;
            }
            else{
                return null;
            }
        }else{
            return null;
        }
       
    }

}

/**System.debug('.....................'+ 
IPMSRestServiceDocumentGenration.genrateLUSAIL_SPA_Doc('62235'));

System.debug('.....................'+ 
FmIpmsRestCoffeeServices.getNewBearerToken());
 */