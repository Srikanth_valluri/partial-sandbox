/*********************************************************************************************************
* Name               : Damac_SRTokenController
* Test Class         : Damac_SRTokenController_Test
* Description        : To display token copy Attachments, buyer, unit info, payment plan details
* Created Date       : 06/05/2020
* Created By         : QBurst
* -----------------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst         06/05/2020         Created
*********************************************************************************************************/

Public with sharing class Damac_SRTokenController {
    public NSIBPM__Service_Request__c srDetails { get; set; }
    public List <Attachment> attachments { get; set; }
    public List <Unit_Documents__c> bookingDocs { get; set; }
    public List <Unit_Documents__c> unitDocs { get; set; }
    public String attachmentURL { get; set; }
    public Attachment selectedAttachment { get; set; }
    public List <Buyer__c> primaryBuyer { get; set; }
    public New_Step__c stepDetails { get; set; }
    public String stepId {get; set;}
    public Deal_Team__c dealTeam { get; set; }
    public List <Booking_Unit__c> bookingUnits { get; set; }
    public Map <Id,  List<Payment_Plan__c>> paymentPlansMap { get; set; }
    public Map<String, Step_Status_Transition__c> stepStatusTransitionMap;
    public Unit_documents__c tokenUDoc { get; set; }
    public Unit_documents__c reservationUDoc { get; set; }
    public List <ExitUnit> exitUnits { get; set; }
    public ID srId;


    public class ExitUnit {
        public Boolean checked {get; set; }
        public Booking_Unit__c bookingUnit { get; set; }
        public ExitUnit () {
            checked = false;
            bookingUnit = new Booking_Unit__c ();
        }
    }
    // Init method to load the records when page loads based on srId
    public void init () { 
        exitUnits = new List <ExitUnit> ();
        system.debug('Inside init Method');
        srId = apexpages.currentpage().getparameters().get('id');
        stepStatusTransitionMap = new Map<String, Step_Status_Transition__c>();
        attachmentURL = '';
        stepId = '';
        selectedAttachment = new Attachment ();
        srDetails = new NSIBPM__Service_Request__c ();
        srDetails = [SELECT Deal_Type__c, Name, NSIBPM__Internal_Status_Name__c, Mode_of_Payment__c, Token_Amount_AED__c,
                             Reservation_Form_Attachment_Name__c, Token_Attachment_Name__c
                     FROM NSIBPM__Service_Request__c WHERE Id =: srId];
        system.debug('srDetails: '+ srDetails);
        loadTokenRecord();
        getBuyers ();
        fetchStepDetails ();
        getDealTeam ();
        getBookingUnits ();
        getPaymentPlans();
        loadForms ();
        loadBookingDocs ();
        getUnitDocs ();
        upsertViewCount (srId, srId, 'apex/DAMAC_SRAttachmens?id=' + srId, false, '', 'DAMAC_SRAttachments');
    }
    
    public void updateSR () {

        update srDetails;
    }
    
    public void checkExitUnits () {
        exitUnits = new List <ExitUnit> ();
        List <String> StatusToExclude = new List <String> ();
        StatusToExclude = Label.Booking_Units_To_Exclude_From_Exit.split (',');
        
        for (Booking_Unit__c unit : [SELECT Name, Unit_Location__c FROM Booking_Unit__c 
                WHERE Booking__r.Deal_SR__c =: srDetails.Id 
                AND Recovery_type__c != 'Exit'
                AND Registration_Status__c IN: StatusToExclude]) {
            ExitUnit u = new ExitUnit ();
            u.checked = false;
            u.bookingUnit = unit;
            exitUnits.add (u);
        }
    }
    
    public void updateExitUnits () {
        List <Booking_Unit__c> bookingUnitsToUpdate = new List <Booking_unit__c> ();
        for (ExitUnit u : exitUnits) {
            if (u.checked) {
                u.bookingUnit.Recovery_type__c = 'Exit';
                bookingUnitsToUpdate.add (u.bookingUnit);
            }
        }
        if (bookingUnitsToUpdate.size () > 0) {
            update bookingUnitsToUpdate;
        }
    }

    // To load the token on SR token page when it is loaded by finance team
    public void loadTokenRecord () {
        system.debug('Inside loadTokenRecord Method');
        tokenUDoc = new Unit_Documents__c ();
        srDetails = new NSIBPM__Service_Request__c ();
        srId = apexpages.currentpage().getparameters().get('id');
        
        srDetails = [SELECT Deal_Type__c, Name, NSIBPM__Internal_Status_Name__c, Mode_of_Payment__c, Token_Amount_AED__c,
                     Reservation_Form_Attachment_Name__c, Token_Attachment_Name__c
                     FROM NSIBPM__Service_Request__c WHERE Id =: srId];
        Attachment tokenForm = new Attachment ();
        try {
            tokenForm = [SELECT Id, Name, ParentId, createdDate, createdBy.Name FROM Attachment 
                         WHERE Name =: srDetails.Token_Attachment_Name__c
                         AND ParentId =: srId order By createdDate DESC LIMIT 1];
            system.debug('tokenForm: ' + tokenForm);
        } catch (Exception e) {}
        if (tokenForm.Id != null) {
            List <Unit_documents__c> tokenDocs = new List <Unit_documents__c> ();
            tokenDocs = [SELECT Id, Name, CV_Id__c, Service_Request__c, Document_Name__c, 
                             Document_Description__c, Sys_Doc_ID__c, Document_type__c
                         FROM Unit_documents__c WHERE Service_Request__c =: srId AND Document_type__c = 'Booking Docs'
                             AND Document_Name__c = 'Token Copy'];
            if (tokenDocs.size () == 0){
                system.debug('No Token Doc exists');
                tokenUDoc.Service_Request__c = srId;
                tokenUDoc.Document_Name__c = 'Token Copy';
                tokenUDoc.Document_Description__c = tokenForm.Name;
                tokenUDoc.Sys_Doc_ID__c = tokenForm.Id;
                tokenUDoc.Status__c = 'Uploaded';
                tokenUDoc.Document_type__c = 'Booking Docs';
                tokenUDoc.Booking_Doc_CreatedBy__c = tokenForm.createdBy.Name;
                tokenUDoc.Booking_Doc_CreatedDate__c = tokenForm.CreatedDate;
                insert tokenUDoc;
            } else {
                system.debug('Token Doc exists: ' + tokenDocs[0]);
                tokenUDoc = tokenDocs[0];
                tokenUDoc.Booking_Doc_CreatedBy__c = tokenForm.createdBy.Name;
                tokenUDoc.Booking_Doc_CreatedDate__c = tokenForm.CreatedDate;
            }
        }
    }

    // To load booking unit document records
    public void loadBookingDocs () {
        system.debug('Inside loadBookingDocs Method');
        attachments = new List <Attachment> ();
        bookingDocs = new List <Unit_Documents__c> ();
        
        List <ID> tokenResIds = new List <ID >();
        if (tokenUDoc != null)
            tokenResIds.add (tokenUDoc.Sys_Doc_ID__c);
        if (reservationUDoc != null)
            tokenResIds.add (reservationUDoc.Sys_Doc_ID__c);
        
        try {
            bookingDocs = [SELECT Id, Document_Name__c, createdBy.Name, Created_Date_Time__c, Sys_Doc_ID__c, CV_id__c,
                               Booking_Doc_CreatedBy__c, Booking_Doc_CreatedDate__c, IS_Invalid__c, Document_type__c
                           FROM Unit_Documents__c
                           WHERE Service_Request__c =: srId
                               AND (Sys_Doc_ID__c != NULL OR CV_id__c != NULL)
                               AND Document_type__c = 'Booking Docs'
                               AND Status__c = 'Uploaded' Order By Booking_Doc_CreatedDate__c, createdDate DESC];
        } catch (Exception e) {}
        
    }

    public PageReference approveRejectStep(){
        system.debug('Inside approveRejectStep Method');
        String unitIds = apexpages.currentpage().getparameters().get('unitIds');
        List <ID> unitsToInvalid = new List <ID> ();
        for (Step_Status_Transition__c transition : [SELECT Id, From_Step_Status__c, To_Step_Status__c, Step_Type__c, SR_External_Status__c, 
                                                             SR_Internal_Status__c, Is_Closed__c
                                                     FROM Step_Status_Transition__c 
                                                     WHERE Step_Type__c = 'Token Payment']) {
            stepStatusTransitionMap.put(transition.To_Step_Status__c, transition);
        }
        Step_Status_Transition__c transition = new Step_Status_Transition__c();
        NSIBPM__Service_Request__c serviceRequest = new NSIBPM__Service_Request__c();
        if(stepDetails != null){
            serviceRequest.Id = stepDetails.Service_Request__c;
            system.debug('unitIds: ' + unitIds);
            if(unitIds != null && unitIds != ''){
                if (unitIds.contains(';')) {
                    unitsToInvalid = unitIds.split(';');
                } else {
                    unitsToInvalid.add (unitIds);
                }
                List <Unit_Documents__c> invalidDocs = new List <Unit_Documents__c> ();
                List <Unit_Documents__c> clonedInvalidDocs = new List <Unit_Documents__c> ();
                for (Unit_Documents__c docs: [SELECT Id, Service_Request__c, Buyer__c, Booking__c, Booking_Unit__c,
                                                         Status__c, Document_Name__c, Document_Template__c, Document_type__c, Is_Invalid__c
                                              FROM Unit_Documents__c WHERE Id IN: unitsToInvalid])  {
                    docs.Is_Invalid__c = true;
                    invalidDocs.add (docs);
                    Unit_Documents__c invalidDoc = new Unit_Documents__c ();
                    invalidDoc.Service_Request__c = docs.Service_Request__c;
                    invalidDoc.Buyer__c = docs.Buyer__c;
                    invalidDoc.Booking__c = docs.Booking__c;
                    invalidDoc.Booking_Unit__c = docs.Booking_Unit__c;
                    invalidDoc.Document_Name__c = docs.Document_Name__c;
                    invalidDoc.Document_Template__c = docs.Document_Template__c;
                    invalidDoc.Document_type__c = docs.Document_type__c;
                    invalidDoc.Is_Invalid__c = false;
                    invalidDoc.Id = NULL;
                    invalidDoc.Status__c = 'Pending Upload';
                    clonedInvalidDocs.add (invalidDoc); 
                    system.debug('Step Rejected');
                    
                    
                    transition = stepStatusTransitionMap.get('Token Document Rejected'); 
                            
                }
                system.debug('invalidDocs: ' + invalidDocs);
                system.debug('clonedInvalidDocs: ' + clonedInvalidDocs);
                update invalidDocs;
                insert clonedInvalidDocs;
            }else{
                system.debug('Step to be closed successfully');
                transition = stepStatusTransitionMap.get('Token Deposit Paid');
                serviceRequest.Token_Deposit_Paid_Date_Time__c = system.now();
            }
            stepDetails.Step_Status__c = transition.To_Step_Status__c;
            stepDetails.Is_Closed__c = transition.Is_Closed__c;
            if(String.isNotBlank(transition.SR_Internal_Status__c)) {
                serviceRequest.Internal_Status__c = transition.SR_Internal_Status__c;
            }
            if(String.isNotBlank(transition.SR_External_Status__c)) {
                serviceRequest.External_Status__c = transition.SR_External_Status__c;
            }
            system.debug('stepDetails: ' + stepDetails);
            system.debug('serviceRequest: ' + serviceRequest);
            update stepDetails;
            update serviceRequest;
        }
        PageReference srPage = new PageReference('/' + serviceRequest.Id);
        return srPage;
    }

    // To take the token, reservation forms and create Unit documents under SR
    public void loadForms () {
        system.debug('Inside loadForms Method');
        tokenUDoc = new Unit_Documents__c ();
        reservationUDoc = new Unit_Documents__c ();
        Attachment tokenForm = new Attachment ();
        try {
            
            tokenForm = [SELECT Id, Name, ParentId, createdDate, createdBy.Name FROM Attachment 
                         WHERE Name =: srDetails.Token_Attachment_Name__c
                             AND ParentId =: srId order By createdDate DESC LIMIT 1];
        } catch (Exception e) {}
        Attachment reservationForm = new Attachment ();
        try {
            reservationForm = [SELECT Id, Name, ParentId, createdDate, createdBy.Name FROM Attachment 
                               WHERE Name =: srDetails.Reservation_Form_Attachment_Name__c
                                   AND ParentId =: srId order By createdDate DESC LIMIT 1];
        } catch (Exception e) {}
        if (tokenForm.Id != null) {
            List <Unit_documents__c> tokenDocs = new List <Unit_documents__c> ();
            tokenDocs = [SELECT Id, Name, CV_Id__c, Service_Request__c, Document_Name__c, 
                                 Document_Description__c, Sys_Doc_ID__c, Document_type__c, 
                                 Booking_Doc_CreatedBy__c, Booking_Doc_CreatedDate__c
                         FROM Unit_documents__c WHERE Service_Request__c =: srId AND Document_type__c = 'Booking Docs'
                             AND Document_Name__c = 'Token Copy'
                             Order by createddate DESC];
            if (tokenDocs.size () == 0){
                
                tokenUDoc.Service_Request__c = srId;
                tokenUDoc.Document_Name__c = 'Token Copy';
                tokenUDoc.Document_Description__c = tokenForm.Name;
                tokenUDoc.Sys_Doc_ID__c = tokenForm.ID;
                tokenUDoc.Document_type__c = 'Booking Docs';
                tokenUDoc.Booking_Doc_CreatedBy__c = tokenForm.createdBy.Name;
                tokenUDoc.Booking_Doc_CreatedDate__c = tokenForm.CreatedDate;
                insert tokenUDoc;    
            } else {
                tokenUDoc = tokenDocs[0];
                tokenUDoc.Booking_Doc_CreatedBy__c = tokenForm.createdBy.Name;
                tokenUDoc.Booking_Doc_CreatedDate__c = tokenForm.CreatedDate;
            }
        }
        if (reservationForm.Id != null) {
            List <Unit_documents__c> reservationDocs = new List <Unit_documents__c> ();
            reservationDocs = [SELECT Id, Name, CV_Id__c, Service_Request__c, Document_Name__c, 
                                       Document_Description__c, Sys_Doc_ID__c, Document_type__c,
                                       Booking_Doc_CreatedBy__c, Booking_Doc_CreatedDate__c
                               FROM Unit_documents__c WHERE Service_Request__c =: srId AND Document_type__c = 'Booking Docs'
                                   AND Document_Name__c = 'Reservation Copy'
                                   Order by createddate DESC];
            if (reservationDocs.size () == 0){
                reservationUDoc.Service_Request__c = srId;
                reservationUDoc.Document_Name__c = 'Reservation Copy';
                reservationUDoc.Document_Description__c = reservationForm.Name;
                reservationUDoc.Sys_Doc_ID__c = reservationForm.Id;
                reservationUDoc.Document_type__c = 'Booking Docs';
                reservationUDoc.Status__c = 'Uploaded';
                reservationUDoc.Booking_Doc_CreatedBy__c = reservationForm.createdBy.Name;
                reservationUDoc.Booking_Doc_CreatedDate__c = reservationForm.CreatedDate;
                insert reservationUDoc;    
            } else {
                reservationUDoc = reservationDocs[0];
                if (reservationUDoc.Document_Description__c == null)
                    reservationUDoc.Document_Description__c = reservationForm.Name+'.pdf';
                reservationUDoc.Booking_Doc_CreatedBy__c = reservationForm.createdBy.Name;
                reservationUDoc.Booking_Doc_CreatedDate__c = reservationForm.CreatedDate;
            }
        }
        
        if (reservationForm.Id == null) {
            List <Unit_documents__c> reservationDocs = new List <Unit_documents__c> ();
            reservationDocs = [SELECT Id, createdBy.Name, CreatedDate, Name, CV_Id__c, Service_Request__c, Document_Name__c, 
                                       Document_Description__c, Sys_Doc_ID__c, Document_type__c,
                                       Booking_Doc_CreatedBy__c, Booking_Doc_CreatedDate__c
                                FROM Unit_documents__c WHERE Service_Request__c =: srId 
                                AND Document_Name__c = 'Reservation Copy' 
                                AND Document_type__c = 'Booking Docs'
                                Order by createddate DESC
                                LIMIT 1];
                               
            if (reservationDocs.size () > 0){
                reservationUDoc = reservationDocs[0];
                if (reservationUDoc.Document_Description__c == null)
                    reservationUDoc.Document_Description__c = reservationUDoc.Name+'.pdf';
                reservationUDoc.Booking_Doc_CreatedBy__c = reservationUDoc.createdBy.Name;
                reservationUDoc.Booking_Doc_CreatedDate__c = reservationUDoc.CreatedDate;   
            } 
        }
        system.debug (reservationUDoc);
    }

    // To update the Content version id of token/reservation form in unit document records
    @remoteAction
    public static void updateDocCVID (String unitDocId, String fileId) {
        Unit_Documents__c doc = new Unit_Documents__c ();
        doc.Id = unitDocId;
        doc.CV_Id__c = fileId;
        doc.Status__c = 'Uploaded';
        update doc;
        ContentVersion version = [SELECT ContentDocumentId FROM ContentVersion WHERE id =: fileId ];
        ContentDocumentLink cd = New ContentDocumentLink(
            LinkedEntityId = unitDocId, 
            ContentDocumentId = version.ContentDocumentId, 
            shareType = 'V');
        insert cd;
        
    }

    // To load the unit document records
    public void getUnitDocs () {
        system.debug('Inside getUnitDocs Method');
        unitDocs = new List <Unit_Documents__c> ();
        try {
            unitDocs = [SELECT Document_Name__c, createdBy.Name, Created_Date_Time__c, Sys_Doc_ID__c, CV_id__c, IS_Invalid__c
                        FROM Unit_Documents__c
                        WHERE Service_Request__c =: srId AND Document_Template__c != NULL 
                            AND (Sys_Doc_ID__c != NULL OR CV_id__c != NULL)
                            AND Document_type__c != 'Booking Docs'
                            AND Status__c = 'Uploaded' order By Created_Date_Time__c DESC];
        } catch (Exception e) {}
    }

    // To get the booking units associated to the SR
    public void getBookingUnits () {
        system.debug('Inside getBookingUnits Method');
        bookingUnits = new List <Booking_unit__c> ();
        try {
            bookingUnits = [SELECT Id, Booking_Unit_Type__c, Inventory__r.District__c, 
                                Inventory__r.IPMS_Bedrooms__c, Inventory__r.Check_List_Type__c,
                                Inventory__r.Unit_Name__c, Inventory__r.Unit_Type__c,
                                Inventory__r.Area_sft__c, Requested_Price_AED__c 
                            FROM Booking_unit__c 
                            WHERE Booking__r.Deal_SR__c =: srId];
        } catch (Exception e) {}
    }

    // To get the Payment Plans associated to the SR
    public void getPaymentPlans () {
        system.debug('Inside getBookingUnits Method');
        paymentPlansMap = new Map <Id, List<Payment_Plan__c>> ();
        try {
            for(Payment_Plan__c plan: [SELECT Id, Name, Booking_Unit__c, Effective_From__c, Effective_To__c,
                                                (SELECT Id, Name, Installment__c, Description__c, Description_Arabic__c, Milestone_Event__c,
                                                        Milestone_Event_Arabic__c, Payment_Date__c,Payment_Amount__c, Payment_Plan__c, 
                                                        Percent_Value__c, Sequence_Number__c 
                                                 FROM Payment_Terms__r ORDER BY Sequence_Number__c ASC)
                                       FROM Payment_Plan__c 
                                       WHERE Booking_Unit__r.Booking__r.Deal_SR__c =: srId]){
                List<Payment_Plan__c> planList = new List<Payment_Plan__c>();
                if(paymentPlansMap.containsKey(plan.Booking_Unit__c)){
                    planList = paymentPlansMap.get(plan.Booking_Unit__c);
                }
                planList.add(plan);
                paymentPlansMap.put(plan.Booking_Unit__c, planList);
            }
        } catch (Exception e) {}
    }    
    // To display the deal team
    public void getDealTeam () {
        system.debug('Inside getDealTeam Method');
        dealTeam = new Deal_Team__c ();
        try {
            dealTeam = [SELECT Associated_PC__c, Associated_PC__r.Name, Associated_PC__r.SmallPhotoUrl,
                            Associated_HOS__c, Associated_HOS__r.Name, Associated_HOS__r.SmallPhotoUrl,
                            Associated_HOD__c, Associated_HOD__r.Name, Associated_HOD__r.SmallPhotoUrl
                        FROM Deal_Team__c 
                        WHERE Associated_Deal__c =: srId
                            AND PC_Team__c = 'PC Team'];
        } catch (Exception e) {}
    }

    // To get the checklist details and displaying in order wise based on custom meta data stored
    public void fetchStepDetails () {
        system.debug('Inside fetchStepDetails Method');
        stepDetails = new New_Step__c ();
        try {
            stepDetails = [SELECT Id, Name, Is_Closed__c, Service_Request__c,comments__c 
                           FROM New_Step__c 
                           WHERE Step_Status__c = 'Awaiting Token Deposit'
                               AND Step_Type__c = 'Token Payment'
                               AND Service_Request__c =: srId];
            stepId = stepDetails.Id;
        } catch (Exception e) {}
    }

    // To get primary buyer information
    public void getBuyers () {
        system.debug('Inside getBuyers Method');
        primaryBuyer = new List <Buyer__c> ();
        try {
            primaryBuyer = [SELECT Primary_buyer__c, Title__c, First_Name__c, Last_Name__c, 
                                Email__c, Phone__c, Nationality__c, Passport_Number__c, City__c, Country__c, 
                                Address_Line_1__c, Address_Line_2__c, Address_Line_3__c, Address_Line_4__c
                            FROM Buyer__c WHERE Booking__r.Deal_SR__c =: srId AND Primary_buyer__c = TRUE ];
        } catch (Exception e) {}
    }

    // To get the content version id when it is uploaded from page
    @remoteAction
    public static ID getRelatedParentId (id docId, Id srId) {
        ContentVersion version = [SELECT ContentDocumentId FROM ContentVersion WHERE id =: docId ];
        ContentDocumentLink cd = New ContentDocumentLink(LinkedEntityId = srId, ContentDocumentId = version.ContentDocumentId, shareType = 'V');
        insert cd;
        return version.Id;
    }

    // to update the page view count when the document is previewed
    public void previewAttachment () {
        String attachmentId = apexpages.currentpage().getparameters().get('attachmentId');
        String fileId = apexpages.currentpage().getparameters().get('fileId');
        String source = apexpages.currentpage().getparameters().get('source');
        try {
            selectedAttachment = [SELECT Name, ParentId, CreatedBy.Name, CreatedDate FROM Attachment WHERE ID =: attachmentId ];
            attachmentURL = 'servlet/servlet.FileDownload?file='+attachmentId;
        } catch (Exception e) {
            attachmentURL = attachmentId;            
        }
        upsertViewCount (srDetails.Id, attachmentId, attachmentURL, true, fileId, source);
    }

    // To get the file id from page view record when it is already loaded
    @remoteAction
    public static String getFileId (String srId, String attachmentId) {
        try {
            String fileId = '';
            Page_View__c view = [SELECT file_id__c FROM Page_View__c WHERE Is_File_View__c = TRUE AND attachment_id__c =: attachmentId 
                                 AND User__c =: UserInfo.getUserId() LIMIT 1];
            if (view.file_id__c != null) {
                fileId = view.file_id__c;
                String attachmentURL = 'servlet/servlet.FileDownload?file='+attachmentId;
                upsertViewCount (srId, attachmentId, attachmentURL, true, fileId, 'DAMAC_SRAttachments');
            }
            return fileId;
        } catch (Exception e) {
            return '';
        }
    }

    // To track page view/attachment/file views
    public static void upsertViewCount (ID srId, String type, String URL, Boolean isFileView, String fileId, String source) {
        String ip;
        string sessionInfo='';
        if(!Test.isRunningTest()){
            Map <String, String> currentSessionAttributes = Auth.SessionManagement.getCurrentSession();
            ip = currentSessionAttributes.get('SourceIp');
            for(string s:currentSessionAttributes.keyset()){
                sessionInfo = sessionInfo+'\n'+s+'-'+currentSessionAttributes.get(s);
            }
        }
        String objectName = 'NSIBPM__Service_Request__c';
        NSIBPM__Service_Request__c srDetails = [SELECT Name, NSIBPM__Internal_Status_Name__c 
                                                FROM NSIBPM__Service_Request__c 
                                                WHERE ID =: srId];
        list<Page_View__c> pv = new list<Page_View__c>();
        String key = userinfo.getuserid()+'-'+source+'-'+type+'-'+system.today().day()+'/'+system.today().Month()+'/'+system.today().year();
        pv = [SELECT View_Count__c,Unique_Key__c from Page_View__c where Unique_Key__c=:key limit 1];
        if(pv.size() == 0 ){
            Page_View__c p = new Page_View__c();
            p.Unique_Key__c = key;
            p.User__c = userinfo.getuserid();
            p.Last_Open_Date_time__c = system.now();
            p.Page_URL__c = string.valueof(system.URL.getOrgDomainUrl().gethost())+'/'+URL;
            p.View_Count__c = 1;    
            p.Log_Date__c = system.today();
            p.IP_Address__c = ip ;
            p.Session_Info__c = sessionInfo;
            p.source__c = source;
            p.record_id__c = srId;
            try {
                if (p.record_id__c != NULL) {
                    ID recordId = p.record_id__c;
                    String sObjectType = recordId.getSobjectType().getDescribe().getName();
                    Sobject acc = Database.query('SELECT RecordTypeId FROM '+sObjectType+' WHERE ID =: recordId');
                    String recordTypeId = ''+acc.get('RecordTypeId');
                    p.record_type_id__c = recordTypeId;
                    p.record_type_name__c = Schema.getGlobalDescribe().get(sObjectType).getDescribe().getRecordTypeInfosById().get(recordTypeId).getName();
                }
            } catch (Exception e) {}
            p.Is_File_View__c = isFileView;
            if (isFileView) {
                p.attachment_id__c = type;
                p.file_id__c = fileId;
            }
            p.record_name__c = srDetails.Name;
            P.Object_Name__c = objectName;
            pv.add(p);  
        }else{
            pv[0].Last_Open_Date_time__c = system.now();
            pv[0].View_Count__c = pv[0].View_Count__c + 1;
            pv[0].IP_Address__c = ip ;
            pv[0].Session_Info__c = sessionInfo;
        }
        upsert pv Unique_Key__c;
    }
    @remoteAction
    public static String contentPublicUrl (Id contentVersionId) {
        return Damac_SRAttachmentView.contentPublicUrl (contentVersionId);
    
    }
    
    @remoteAction
    public static String getDocumentName ( Id attachmentId, String docName) {
        Attachment att = new Attachment ();
        att = [SELECT name FROM Attachment WHERE Id =: attachmentId];
        String fileName = att.name;
        String fileType = filename.substring(filename.lastIndexOf('.') + 1);
        return docName+'.'+fileType;
    }
}