@isTest
public without sharing class UnsignedSnagDocUpdateOnCaseTest {
	@isTest
    static void checkDocNotsignedTest(){
	     Set<Id> recTypeIdSet=new Set<Id>();
        for(RecordType rec :[SELECT DeveloperName,Id,Name
                FROM RecordType
                WHERE SobjectType = 'Case'
                And (Name='Handover'
                OR Name='Early Handover'
                )  ]){
        recTypeIdSet.add(rec.Id);
        }
        Id HOCase=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc =  new Account(RecordTypeId = personAccRTId,
                    FirstName='Test FirstName1',
                    LastName='Test LastName2',
                    Type='Person',Email__pc = 'test@test.com'

                    );
        insert objAcc ;
        
        Id dealrecid=Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        
        NSIBPM__Service_Request__c Deal = new NSIBPM__Service_Request__c();
        Deal.RecordTypeId=dealrecid;
        insert Deal;
        
        Booking__c B=new Booking__c();
        B.Booking_Channel__c='Web';
        B.Deal_SR__c=Deal.Id;
        B.Account__c = objAcc.Id; 
        insert B;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Unit_Name__c='HWT/PT124/XL2265B';
        objBU.Registration_ID__c='71609';
        objBU.Registration_Status_Code__c='LE';
        objBU.Party_Id__c='79658';
        objBU.Booking__c=B.Id;
        insert objBU;

        TriggerOnOffCustomSetting__c objCustomSetting = new TriggerOnOffCustomSetting__c();
        objCustomSetting.Name = 'CallingListTrigger';
        objCustomSetting.OnOffCheck__c = false;
        insert objCustomSetting;

        Id callingListRecTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Appointment Scheduling').getRecordTypeId();

        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Account__c = objAcc.Id;
        objCallingList.Appointment_Start_DateTime__c = System.now();
        objCallingList.Appointment_End_DateTime__c = System.now()+1;
        objCallingList.Booking_Unit__c = objBU.Id;
        objCallingList.Virtual_Viewing__c = true;
        objCallingList.RecordTypeId = callingListRecTypeId;
        insert objCallingList;

        Case sCase = new Case();
        sCase.RecordtypeId=HOCase;//'0122500000018qf';
        sCase.Booking_Unit__c= objBU.Id;
        sCase.AccountId=objAcc.Id;
        sCase.Snag_Completion_Date__c = System.today()-3;
        insert sCase;
        
        String DOC_NAME = 'Snag Rectification Form';
	
	  cuda_signnow__SignNowStatus__c objStatus = new cuda_signnow__SignNowStatus__c();
		objStatus.cuda_signnow__RelatedToCase__c = sCase.Id;
		objStatus.cuda_signnow__Document_Id__c = '1233';
		objStatus.cuda_signnow__SN_Documet_Id__c = '1233';
		objStatus.cuda_signnow__Record_Id__c = sCase.Id;
		objStatus.cuda_signnow__Record_Type__c = 'Case';
		//objStatus.cuda_signnow__Sent_To__c = strInviteSentTo;
		objStatus.cuda_signnow__Status__c = 'Pending';
		objStatus.Name = DOC_NAME;
		insert objStatus;
		List<cuda_signnow__SignNowStatus__c> lstSnag = new List<cuda_signnow__SignNowStatus__c>();
		lstSnag.add(objStatus);
		 Test.startTest();
            //As Per Best Practice it is important to instantiate the Rest Context

            UnsignedSnagDocUpdateOnCase.checkDocNotsigned(lstSnag);

        Test.stopTest();
	}
}