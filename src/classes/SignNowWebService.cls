/****************************************************************************************
Description: Class to make calllout request for different SignNow Services.
----------------------------------------------------------------------------------------*
Version     Date        Author              Description                                 *
1.0        2/09/2020    Akshata Anvekar    Initial Draft                                *
*****************************************************************************************/
public class SignNowWebService {
    /* Method to fetch metadata record
     * for SignNow API credentials
     * Input Parameters : No parameters
     * Return Type : SignNow_API__mdt 
     */
    public SignNow_API__mdt getSignNowCreds(){
        List<SignNow_API__mdt> lstMetadata = [Select Basic_Token__c
                                                    , Callback_URL__c
                                                    , Password__c
                                                    , Username__c
                                                 From SignNow_API__mdt
                                             Where DeveloperName = 'Credentials'];
        if(lstMetadata != null && !lstMetadata.isEmpty()){
        	return lstMetadata[0];    
        }
       	return null; 
    }
    
    /* Method to fetch access token
     * from SignNow using basic token
     * Input Parameters : No parameters
     * Return Type : string 
     */
    public string fetchAccessToken() {
        SignNow_API__mdt objCreds = getSignNowCreds();
        
        String userName = objCreds.Username__c;
        String password = objCreds.Password__c;
        String headerValue = 'Basic ' + objCreds.Basic_Token__c;
        JSONGenerator gen = JSON.createGenerator(true);
		gen.writeStartObject();
		gen.writeStringField('username', userName);
		gen.writeStringField('password', password);
		gen.writeStringField('grant_type', 'password');
		gen.writeEndObject();
        system.debug(gen.getAsString());
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        req.setEndpoint('https://api.signnow.com/oauth2/token');
        req.setHeader('Authorization', headerValue);
        req.setHeader('content-Type','application/json');           
        req.setMethod('POST');
        req.setBody(gen.getAsString());
        req.setTimeout(120000);
        system.debug(req);
        Http http = new Http();
        res=http.send(req);
        JsonToApexWrapper objResponse = new JsonToApexWrapper();
        objResponse = (JsonToApexWrapper)System.JSON.deserialize(res.getBody(), JsonToApexWrapper.class);
        System.debug( 'objResponse.access_token- -  ' + objResponse.access_token );
        return objResponse.access_token;
    }  
	
     /* Method to fetch access token
     * from SignNow using basic token
     * Input Parameters : Attachment, access token
     * Return Type : string 
     */
    public String uploadDocumentToSignNow(Attachment objFile, String strAccessToken){
        Blob bodyEncoded = makeBlobWithFile('file', objFile.Body, objFile.Name, '');
        
        String headerValue = 'Bearer ' + strAccessToken;
        String boundary = '------------------------700252463407842306668015';

        HttpRequest req = new HttpRequest( );
        req.setMethod('POST');
        req.setBodyAsBlob( bodyEncoded );
        req.setHeader('Content-Type','multipart/form-data; boundary='+boundary);
        req.setHeader( 'Authorization',headerValue );
        req.setEndpoint( 'https://api.signnow.com/document/fieldextract' );
        req.setTimeout(120000);
        HTTPResponse response = new Http().send(req );   
        System.debug( 'status = ' + response.getStatusCode( ) );
        System.debug( 'Response - -  ' + response );
        System.debug( 'Response body - -  ' + response.getBody( ) );
        JsonToApexWrapper objResponse = new JsonToApexWrapper();
        objResponse = (JsonToApexWrapper)System.JSON.deserialize(response.getBody(), JsonToApexWrapper.class);
        System.debug( 'objResponse document id- -  ' + objResponse.id );
        return objResponse.id;
    }
    
    /* Method to create encoaded blob from file
     * Input Parameters : form data key, attachment body, filename, other form data parameters
     * Return Type : Blob 
     */
    public static Blob makeBlobWithFile(String key ,Blob attachBody,string filename,string otherParamsEncoded){
        String boundary = '------------------------700252463407842306668015';
        String header = '--' + boundary + '\r\n' +
                        + 'Content-Type: application/octet-stream\r\n'+
                        + 'Content-Disposition: form-data; name="'+key+'";filename="' + filename +'"';        
        
        String headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header + '\r\n\r\n'));
        while(headerEncoded.endsWith('=')){
            header += ' ';
            headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
        }
        String footer = '--' + boundary + '--';     
        String bodyEncoded = EncodingUtil.base64Encode(attachBody);       
        Blob formBlob = null;    
        String last4Bytes = bodyEncoded .substring(bodyEncoded.length()-4,bodyEncoded.length());
        
        if(last4Bytes.endsWith('==')) {
            last4Bytes = last4Bytes.substring(0,2) + '0K';
            bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
             formBlob = EncodingUtil.base64Decode(otherParamsEncoded+headerEncoded+bodyEncoded+footerEncoded);
        
        } else if(last4Bytes.endsWith('=')) {
            last4Bytes = last4Bytes.substring(0,3) + 'N';
            bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
             footer = '\n' + footer;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
             formBlob = EncodingUtil.base64Decode(otherParamsEncoded+headerEncoded+bodyEncoded+footerEncoded);
        } else {
             footer = '\r\n' + footer;
             String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
             formBlob = EncodingUtil.base64Decode(otherParamsEncoded+headerEncoded+bodyEncoded+footerEncoded);
        }
        
        return formBlob; // returning the form data as a blob 
        
    }
    
    /* Method to send signature invite
     * Input Parameters : SignNow document Id, invite request body, access token
     * Return Type : JsonToApexWrapper 
     */
    public JsonToApexWrapper sendSignatureInvite(String strDocId, String strRequestBody, String strAccessToken){
    	String headerValue = 'Bearer ' + strAccessToken;
        HttpRequest req = new HttpRequest();
        HttpResponse response = new HttpResponse();
        req.setEndpoint('https://api.signnow.com/document/'+strDocId+'/invite');
        req.setHeader('Authorization', headerValue);
        req.setHeader('content-Type','application/json');           
        req.setMethod('POST');
        req.setBody(strRequestBody);
        req.setTimeout(120000);
        system.debug(req);
        Http http = new Http();
        response=http.send(req);
        System.debug('---->Response'+response.getBody());
        JsonToApexWrapper objResponse = new JsonToApexWrapper();
        objResponse = (JsonToApexWrapper)System.JSON.deserialize(response.getBody(), JsonToApexWrapper.class);
        System.debug( 'objResponse document id- -  ' + objResponse.status );
        return objResponse;
    }
    
    /* Method to subscribe document complete
     * event in SignNow
     * Input Parameters : SignNow document Id, access token
     * Return Type : Void 
     */
    public void subscribeEvent(String strDocId, String strAccessToken) {
		SignNow_API__mdt objCreds = getSignNowCreds();
        String headerValue = 'Bearer ' + strAccessToken;
        String callbackUrl = objCreds.Callback_URL__c;
        String eventType = 'document.complete';
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        
        req.setEndpoint('https://api.signnow.com/api/v2/events');
        req.setHeader('Authorization', headerValue);
        req.setHeader('content-Type','application/json');           
        req.setMethod('POST');
		JSONGenerator gen = JSON.createGenerator(true);
		gen.writeStartObject();
		gen.writeStringField('event', eventType);
		gen.writeStringField('entity_id', strDocId);
		gen.writeStringField('action', 'callback');
		gen.writeFieldName('attributes');
		gen.writeStartObject();
		gen.writeStringField('callback', callbackUrl);
		gen.writeBooleanField('docid_queryparam', true);
		gen.writeEndObject();
		gen.writeEndObject();
		String strRequestBody = gen.getAsString();
		system.debug(strRequestBody);
        req.setBody(strRequestBody);
        req.setTimeout(120000);
        system.debug(req);
        Http http = new Http();
        res=http.send(req);	
        System.debug('---->Event Response'+res.getBody());
    }
    
    /* Method to download sent document
     * copy from SignNow
     * Input Parameters : SignNow document Id, access token
     * Return Type : Blob 
     */
    public Blob downloadSentDocument(String strDocId, String strAccessToken) {
        String headerValue = 'Bearer ' + strAccessToken;
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        req.setEndpoint('https://api.signnow.com/document/'+strDocId+'/download?with_history=1');
        req.setHeader('Authorization', headerValue);         
        req.setMethod('GET');
        req.setTimeout(120000);
        system.debug(req);
        Http http = new Http();
        res=http.send(req);   
        System.debug('---->Response'+res.getBody());
        return res.getBodyAsBlob();
	}
    
    /* Method to download signed document
     * copy from SignNow
     * Input Parameters : cuda_signnow__SignNowStatus__c
     * Return Type : Blob 
     */
    Public Blob getSignedDocument(cuda_signnow__SignNowStatus__c signNowObj){
        	String headerValue = 'Bearer ' + fetchAccessToken();
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            req.setEndpoint('https://api.signnow.com/document/'+signNowObj.cuda_signnow__Document_Id__c+'/download/collapsed');
            req.setHeader('Authorization', headerValue);
            req.setMethod('GET');
            req.setTimeout(120000);
            Http http = new Http();
            res=http.send(req);
            System.debug('---->Response'+res);
            if(res.getStatusCode()==200){ 
                return res.getBodyAsBlob();
            }
        	return null;
    }
    
    public class JsonToApexWrapper{
        public string expires_in;
        public string token_type;
        public string access_token;
        public string refresh_token;
        public string scope;
        public string last_login;
        public string id;
        public string status;
    }
}