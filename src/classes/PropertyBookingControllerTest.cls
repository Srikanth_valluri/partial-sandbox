/**************************************************************************************************
* Name               : PropertyBookingControllerTest                                              *
* Description        : Test class for PropertyBookingController.                                  *
* Created Date       : 20/04/2017                                                                 *
* Created By         : NSI                                                                        *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR                 DATE            COMMENTS                                     *
* 1.0         NSI - Vineet           20/04/2017      Initial Draft.      
==================================================================================================*
* 1.1         Accely- Pratiksha      14/07/2017      changes in code to increase code coverage -- *
*                                                    Current code coverage -26%                   *
**************************************************************************************************/
@isTest
private class PropertyBookingControllerTest {

    private static testMethod void setup1() {
        PropertyBookingController obj = new PropertyBookingController ();
        obj.selectedCampaignId = '';
        obj.errorMessage = '';
        obj.errorPrefix = '';
        obj.mode = '';
        obj.showScreen1 = false;
        obj.showScreen2 = false;
        obj.showScreen3 = false;
        obj.showScreen4 = false;
        obj.isError = false;
        obj.isSuccess = false;
        obj.isSubmitted = false;
        obj.inventoryIdsList  = null;
        obj.finalWrap = null;
        obj.availableInventoriesList = null;
        obj.selectUnits ();
        obj.getPackageRelatedInventories (null);
        obj.createPortfolio ();
        PropertyBookingController.validateSelectedUnits ('', null);
        obj.selectPaymentPlan ();
        obj.selectPaymentMethod ();
        obj.submitBooking ();
        obj.saveBooking ();
        obj.submitApprovalProcess ('', '');
        obj.clearIds ();
        obj.getCampaignRelatedBookingUnit (null);
        obj.createDealTeam (null, null);
        obj.createInquiry (null);
        obj.createAssociatedCampaign (null);
        obj.createBookingRecords (null, null);
        obj.updateBookingRecords (null, null);
        obj.updateBuyerRecords (null, null);
        obj.createBuyerRecords (null, null,null);
        obj.getPhoneCodeValueMap ();
        obj.getAssociatedCampaign (null);
        obj.getInquiryRelatedCampaign (null);
        obj.createBookingUnitRecords (null, null);
        obj.createPaymentPlans (null, null, null);
        obj.createPaymentTerms (null, null);
        obj.closeErrorBox ();
        PropertyBookingController.validateBuyerFields (null, false, false);
        obj.prePopulateData ('');
        obj.setWizardLevel (null);
        obj.checkInventoryAvailable (null);
        obj.getInventoryDetails (null);
        obj.getPaymentPlanDetails (null);
        obj.getSelectedPaymentPlanDetails (null);
        obj.getBookingDetails (null);
        PropertyBookingController.validateEmailAddress ('');
        PropertyBookingController.validatePhone ('');
        PropertyBookingController.validateDateOfBirth (null);
        PropertyBookingController.getBuyerFields ();
        obj.getRoadshowCampaign ();
        obj.getInquiryRelatedPromoter (null);
        
        
    }

/*********************************************************************************/

}