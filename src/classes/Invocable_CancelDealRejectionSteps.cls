/**************************************************************************************************
* Name               : Invocable_CancelDealRejectionSteps
* Description        : Invocable Class to Update Registration Status
* Test Class         : Invocable_CancelDealRejectionSteps_Test
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR               DATE             Comments
* 1.0                            16/06/2019         Created
* 1.1         QBurst             02/04/2020         SOAP to REST change in Webservice call
**************************************************************************************************/
public class Invocable_CancelDealRejectionSteps  {
    @InvocableMethod
    public static void CancelDealRejectionSteps(List<Id> stepId) {
        for(New_Step__c step : [SELECT Id, Service_Request__c FROM New_Step__c WHERE Id IN : stepId]) {
            evaluateCustomCode(step);
        }
    }

    public static String evaluateCustomCode(New_Step__c step) {
        String retStr = 'Success';
        List<Id> bookingIds = new List<Id>();
        try{
            NSIBPM__Service_Request__c objSR = SRUtility.getSRDetails(step.Service_Request__c);
            //Get all steps of SR which are Open and set status to Cancelled
            List<New_Step__c> getAllsteps = new List<New_Step__c>();
            getAllsteps = [SELECT Id, Step_Status__c FROM New_Step__c 
                           WHERE Service_Request__c = : objSR.id AND Id != : step.id 
                           AND Is_Closed__c = false AND Step_Status__c != 'Sales Audit Rejected' 
                           LIMIT 50000];
            if(getAllsteps.size() > 0){
                for(New_Step__c stp : getAllsteps){
                    stp.Step_Status__c = 'Cancelled';
                    stp.Is_Closed__c = True;
                }
                update getAllsteps;
            }     
            for(Booking__c bookings : [SELECT Id FROM Booking__c WHERE Deal_SR__c = :step.Service_Request__c]){
                bookingIds.add(bookings.id);
            }
            if(bookingIds.size() > 0) {
                system.debug('#### invoking CancelDealRejectionSteps');
                //system.enqueueJob(new AsyncReceiptWebservice (bookingIds, 'StatusUpdate'));  
                DAMAC_IPMS_PARTY_CREATION.statusUpdate(bookingIds, '', step.Id); // 1.1
            }
            return retStr;
        } catch(exception ex) {
            return ex.getMessage();
        }
    }
}