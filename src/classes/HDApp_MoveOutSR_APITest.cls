@isTest
private class HDApp_MoveOutSR_APITest {

    @isTest
    static void apiMainMethodTestDraft() {

        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];

        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Customer Community Login User(Use this)%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              //ContactId = contact1.Id,
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
        Database.insert(user1);

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'Test Unit Name',
                                      Registration_ID__c = '3901');
        insert bookingUnit;

        String moveOutDate = Datetime.newInstance(Date.today().addDays(10).year(), Date.today().addDays(10).month(), Date.today().addDays(10).day()).format('yyyy-MM-dd');

        String jsonString =  '   {  '  + 
        '       "action": "draft",  '  + 
        '       "id": "",  '  + 
        '       "booking_unit_id": "'+bookingUnit.Id+'",  '  + 
        '       "account_id" : "'+account.Id+'",  '  + 
        '       "expected_move_out_date": "'+moveOutDate+'",  '  + 
        '       "move_out_type": "Using moving company",     '  + 
        '       "moving_company_name" : "ABC",  '  + 
        '       "moving_contractor_name" : "ABC contractor",  '  + 
        '       "moving_contractor_phone" : "021321",  '  + 
        '       "reason_to_move_out" : "test"  '  + 
        '  }  ' ; 

        

        Test.startTest();

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/InsertMoveOutCase';  
            req.requestBody = Blob.valueOf(jsonString);
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;

            HDApp_MoveOutSR_API.apiMainMethod();
        Test.stopTest();
    }


    @isTest
    static void apiMainMethodTestSubmit() {

        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];

        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Tenant Community Login User%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              //ContactId = contact1.Id,
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
        Database.insert(user1);

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1202',
                                      Registration_ID__c = '3901');
        insert bookingUnit;

        FM_Case__c objFMCase = new FM_Case__c();
        objFMCase.Request_Type__c = 'Move Out';
        objFMCase.Origin__c = 'Portal';
        objFMCase.Tenant__c = account.Id;  
        objFMCase.Booking_Unit__c = bookingUnit.id;
        objFMCase.isHelloDamacAppCase__c = true;
        insert objFMCase;

        SR_Attachments__c srAttach1 = new SR_Attachments__c();
        srAttach1.Name = 'Trade license of moving company';
        srAttach1.Attachment_URL__c = 'http://damacproperties.force.com/Documents/apex/GetFile?id=01NUEFKAW4NVD35H4Z7VEKEMYYSTVGULRT';
        srAttach1.Type__c = 'JPG';
        srAttach1.FM_Case__c = objFMCase.Id;
        insert srAttach1;

        SR_Attachments__c srAttach2 = new SR_Attachments__c();
        srAttach2.Name = 'Third Party Insurance';
        srAttach2.Attachment_URL__c = 'http://damacproperties.force.com/Documents/apex/GetFile?id=01NUEFKAW4NVD35H4Z7VEKEMYYSTVGULRT';
        srAttach2.Type__c = 'JPG';
        srAttach2.FM_Case__c = objFMCase.Id;
        insert srAttach2;

        SR_Attachments__c srAttach3 = new SR_Attachments__c();
        srAttach3.Name = 'NOC from landlord';
        srAttach3.Attachment_URL__c = 'http://damacproperties.force.com/Documents/apex/GetFile?id=01NUEFKAW4NVD35H4Z7VEKEMYYSTVGULRT';
        srAttach3.Type__c = 'JPG';
        srAttach3.FM_Case__c = objFMCase.Id;
        insert srAttach3;

        String moveOutDate = Datetime.newInstance(Date.today().addDays(10).year(), Date.today().addDays(10).month(), Date.today().addDays(10).day()).format('yyyy-MM-dd');

        String jsonString =  '   {  '  + 
        '       "action": "submit",  '  + 
        '       "id": "'+objFMCase.id+'",  '  + 
        '       "booking_unit_id": "'+bookingUnit.Id+'",  '  + 
        '       "account_id" : "'+account.Id+'",  '  + 
        '       "expected_move_out_date": "'+moveOutDate+'",  '  + 
        '       "move_out_type": "Using moving company",     '  + 
        '       "moving_company_name" : "ABC",  '  + 
        '       "moving_contractor_name" : "ABC contractor",  '  + 
        '       "moving_contractor_phone" : "021321",  '  + 
        '       "reason_to_move_out" : "test"  '  + 
        '  }  ' ; 

        

        Test.startTest();

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/InsertMoveOutCase';  
            req.requestBody = Blob.valueOf(jsonString);
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;

            HDApp_MoveOutSR_API.apiMainMethod();
        Test.stopTest();

    }

    @isTest
    static void apiMainMethodTestNoJSON() {

        String jsonString = ''; 
        
        Test.startTest();

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/InsertMoveOutCase';  
            req.requestBody = Blob.valueOf(jsonString);
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;

            HDApp_MoveOutSR_API.apiMainMethod();
        Test.stopTest();
    }

    @isTest
    static void apiMainMethodTestNoAction() {

        String jsonString =  '   {  '  + 
        '       "action": "",  '  + 
        '       "id": "",  '  + 
        '       "booking_unit_id": "",  '  + 
        '       "account_id" : "",  '  + 
        '       "expected_move_out_date": "",  '  + 
        '       "move_out_type": "Using moving company",     '  + 
        '       "moving_company_name" : "ABC",  '  + 
        '       "moving_contractor_name" : "ABC contractor",  '  + 
        '       "moving_contractor_phone" : "021321",  '  + 
        '       "reason_to_move_out" : "test"  '  + 
        '  }  ' ; 

        Test.startTest();

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/InsertMoveOutCase';  
            req.requestBody = Blob.valueOf(jsonString);
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;

            HDApp_MoveOutSR_API.apiMainMethod();
        Test.stopTest();
    }

    @isTest
    static void apiMainMethodTestNoAccount() {

        String jsonString =  '   {  '  + 
        '       "action": "draft",  '  + 
        '       "id": "",  '  + 
        '       "booking_unit_id": "1234",  '  + 
        '       "account_id" : "",  '  + 
        '       "expected_move_out_date": "",  '  + 
        '       "move_out_type": "Using moving company",     '  + 
        '       "moving_company_name" : "ABC",  '  + 
        '       "moving_contractor_name" : "ABC contractor",  '  + 
        '       "moving_contractor_phone" : "021321",  '  + 
        '       "reason_to_move_out" : "test"  '  + 
        '  }  ' ; 

        Test.startTest();

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/InsertMoveOutCase';  
            req.requestBody = Blob.valueOf(jsonString);
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;

            HDApp_MoveOutSR_API.apiMainMethod();
        Test.stopTest();
    }
}