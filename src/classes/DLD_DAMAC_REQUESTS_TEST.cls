@isTest
public class DLD_DAMAC_REQUESTS_TEST {
    public static testMethod void method1 () {
        DLD_Agreement__c  agreement = New DLD_Agreement__c ();
            agreement.Property_Id__c = 120;
            agreement.Property_Price__c = '1200';
            agreement.Procedure_Id__c = 120;
            agreement.Contract_Date__c = system.now();
            agreement.English_Terms__c = 'test';
            agreement.Arabic_Terms__c = 'test';
            agreement.Parking_Price__c = 20;
            agreement.Investor_Registration_fees__c = 120;
            agreement.Discount__c = 12;
            agreement.Developer_Registration_Fees__c = 12;
        insert agreement;
        DLD_Damac_Property_Mapping__c mapping = new DLD_Damac_Property_Mapping__c ();
        mapping.DLD_User_Name__c = 'test';
        mapping.Password__c = 'test';
        mapping.DLD_Property_Id__c = 120;
        insert mapping;
        
        NSIBPM__Service_Request__c SR1 = new NSIBPM__Service_Request__c();
        SR1.Agency_Name__c = 'Test Account';
        SR1.NSIBPM__Email__c = 'test@nsigulf.com';
        SR1.NSIBPM__Send_SMS_to_Mobile__c= '12123';
        SR1.COUNTRY_OF_SALE__C = 'UAE;Lebanon;Jordan';
        insert SR1;        
        
        unit_documents__c unitDocs = New unit_documents__c();
            unitDocs.DLD_Agreement__c = agreement.id;
            unitDocs.DLD_SF_File_Id__c = 'test';
            unitDocs.Service_Request__c = SR1.id;
        insert unitDocs;
        
        ContentVersion cv = new ContentVersion();
            cv.title='Test title';
            cv.VersionData=blob.valueof('New Bitmap Image.bmp');
            cv.PathOnClient ='/abc.txt';
            cv.origin  ='H';
            Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body'); 
            cv.VersionData=bodyBlob; 
        insert cv;
        
        Agreement_Buyers__c agreementBuyers = New Agreement_Buyers__c ();
            agreementBuyers.DLD_Agreement__c = agreement.id;
            agreementBuyers.Emirates_Id__c = '1';
            agreementBuyers.Buyer_type__c =true;
            agreementBuyers.Residency_Issue_Place_Id__c = '11';
            agreementBuyers.Passport_Issue_Place_En__c = 'test';
            agreementBuyers.Residency_Country_Id__c = '11';
            agreementBuyers.Person_Type__c = '1';
            
            agreementBuyers.Passport_Issue_Place_Ar__c = '11';          
            agreementBuyers.Passport_Type_Id__c = '11';
            agreementBuyers.M_Uae_Id_Expiry_Date__c = Date.today ();
            agreementBuyers.M_Residency_Start_Date__c = Date.today ();
            agreementBuyers.M_Residency_Expiry_Date__c = Date.today ();
            agreementBuyers.M_Passport_Expiry_Date__c = Date.today ();

            agreementBuyers.M_Id_Issue_Date__c = Date.today ();
            agreementBuyers.M_Birth_Date__c = Date.today ();

            agreementBuyers.Country_Id__c = '12';
        insert agreementBuyers;
        
        DLD_Payment_Plans__c payPlans = New DLD_Payment_Plans__c ();
            payPlans.DLD_Agreement__c = agreement.id;
        insert payPlans;
        
        Log__c l = new Log__c();
            l.Description__c = 'RESPONSE ===';
            l.dld_agreement__c = agreement.Id;
            
            l.Type__c='test';
            insert l;
        
        DLD_DAMAC_REQUESTS obj = New DLD_DAMAC_REQUESTS();
        
        Apexpages.currentpage().getparameters().put('Id',agreement.id);
        apexpages.currentpage().getparameters().put('rowNumber',String.valueOf(1));
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
            obj.uploadedDocuments ();
            obj.AgreementId = agreement.id;
            obj.pageLoad();
            obj.cancelProcedure ();
            obj.updateNOCStatus ();
            obj.addPaymentPlan();
            obj.UpdateDataToSF();
            DLD_DAMAC_REQUESTS.getPropertyDetails('test');
            obj.deletePaymentPlan();
            DLD_DAMAC_REQUESTS.convertDateToEpoch(Date.today());
            
            obj.UnlockRequest();
            obj.dismissAllValidations();
        Test.startTest();

            obj.prepareDataForRequest1();
            DLD_DAMAC_REQUESTS.updateUnitDocFileId(unitDocs.id,cv.id);
            obj.checkStatusByRefid ();
            DLD_DAMAC_RESP_OBJ1.OqoodResponse  obj1 = New DLD_DAMAC_RESP_OBJ1.OqoodResponse  ();
            List<DLD_DAMAC_RESP_OBJ1.ValidationErrors> errList = New List<DLD_DAMAC_RESP_OBJ1.ValidationErrors>();
            DLD_DAMAC_RESP_OBJ1.ValidationErrors errObj = New DLD_DAMAC_RESP_OBJ1.ValidationErrors();
                errObj.ErrorNumber = 400;
                errObj.ErrorMessage ='test';
                errObj.ErrorMessageAr = 'test';
            errList.add(errObj);
                obj1.IsSuccess = false;
                obj1.ValidationErrors = errList;
            DLD_DAMAC_REQUESTS.createValidationList(obj1,agreement);
            DLD_DAMAC_REQUESTS.multipartType('','','','');
            DLD_DAMAC_RESP_OBJ dataObj = new DLD_DAMAC_RESP_OBJ();  
            dataObj.data = '{"status": "success","OqoodResponse": {"issuccess": true,"Documents": [{"docName": "testDoc" , "isRequired" : false}],"OutputDocuments": [{"Identity": 1,"url": "test.com"}],'
                                    +'"PartiesList": [{"Address": "testAddr", "ParticipantNumber" :"1", "Documents" : [{"docName" : "docName", "isRequired" : false}]}], "ValidationErrors" : [{"ErrorNumber" : 200, "ErrorMessage" : "test", "ErrorMessageAr" : "test"}],'
                                    +' "RequestDetailsObj": {"ArabicTerms": "test"}}}';
                                    
            
            DLD_DAMAC_RESP_OBJ1 respCls = DLD_DAMAC_RESP_OBJ1.parse(dataObj.data);
            DLD_DAMAC_REQUESTS.saveReqData('OQOOD_STATUS_CHECK',agreement.Id,'test','test','test',true,respCls);
            try{
                DLD_NOC_DOC_RESP.parse('{"data" : "test"}');
            }catch(Exception e) {}
            try{
                DLD_NOC_DOC_RESP0.parse('{"data" : "test"}');
            }catch(Exception e){}
            try{
                DLD_DAMAC_ERROR_RESP.parse('{"data" : "test"}');
            }
            catch(Exception e) {}
            
            obj.updateDocumentUploadStatus (unitDocs.id, false, null);
            obj.uploadoqooddoc ();
            obj.uploadparty ();
            obj.QueryParticipant ();
            obj.cloneProcedure ();
            DLD_DAMAC_REQUESTS.checkUAEID ('1234');
            
            DLD_DAMAC_REQUESTS.sendRequestToDLD (agreement.Id, 'test', 'test', 'https://tes.com');
            String jsonVal = '{"requestRefID" : "1234"}';
            DLD_DAMAC_REF_ID_RESP.parse (jsonVal);
            
            String json2Val = '{"RequestType" : 1}';
            DLD_DAMAC_REQ_OBJ.parse (json2Val );
            DLD_DAMAC_RESP_OBJ.parse ('{"data" : "test"}');
            DLD_STATUS_RESP.parse ('{"status" : "test"}');
            
            Location__c loc=new Location__c();
              loc.Location_ID__c='123';
              insert loc;
              
            Inventory__c inv = new Inventory__c();
              inv.Unit_Location__c=loc.id;
              insert inv;
              
              NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
              SR.Delivery_mode__c='Email';
              SR.Deal_ID__c='1001';
              insert SR;
              
              Booking__c bk= new Booking__c();
              bk.Deal_SR__c=SR.id;
              bk.Booking_channel__c='Office';
              insert bk;
              
              Booking_Unit__c BU = new Booking_Unit__c ();
              BU.Booking__c=bk.id;
              BU.Inventory__c=inv.id;
              insert BU;
              
              Damac_DLD_PaymentPlans.retrievePayPlan (bu.id);
              String xmlBody = '<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/"><env:Header/><env:Body><OutputParameters xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://xmlns.oracle.com/apps/ont/soaprovider/plsql/xxdc_process_service_ws/retrieve/"><X_RESPONSE_MESSAGE><X_RESPONSE_MESSAGE_ITEM><ATTRIBUTE1>98224</ATTRIBUTE1><ATTRIBUTE2>120089</ATTRIBUTE2><ATTRIBUTE3>864381</ATTRIBUTE3><ATTRIBUTE4>DP</ATTRIBUTE4><ATTRIBUTE5>24</ATTRIBUTE5><ATTRIBUTE6>17-JAN-2018</ATTRIBUTE6><ATTRIBUTE7 xsi:nil="true"/><ATTRIBUTE8>Immediate</ATTRIBUTE8><ATTRIBUTE9 xsi:nil="true"/><ATTRIBUTE10>DEPOSIT</ATTRIBUTE10><ATTRIBUTE11 xsi:nil="true"/><ATTRIBUTE12>فورا</ATTRIBUTE12><ATTRIBUTE13>1132930</ATTRIBUTE13><ATTRIBUTE14 xsi:nil="true"/><ATTRIBUTE15 xsi:nil="true"/><ATTRIBUTE16 xsi:nil="true"/><ATTRIBUTE17 xsi:nil="true"/><ATTRIBUTE18 xsi:nil="true"/><ATTRIBUTE19 xsi:nil="true"/><ATTRIBUTE20 xsi:nil="true"/><ATTRIBUTE21 xsi:nil="true"/><ATTRIBUTE22 xsi:nil="true"/><ATTRIBUTE23 xsi:nil="true"/><ATTRIBUTE24 xsi:nil="true"/><ATTRIBUTE25 xsi:nil="true"/><ATTRIBUTE26 xsi:nil="true"/><ATTRIBUTE27 xsi:nil="true"/><ATTRIBUTE28 xsi:nil="true"/><ATTRIBUTE29 xsi:nil="true"/><ATTRIBUTE30 xsi:nil="true"/><ATTRIBUTE31 xsi:nil="true"/><ATTRIBUTE32 xsi:nil="true"/><ATTRIBUTE33 xsi:nil="true"/><ATTRIBUTE34 xsi:nil="true"/><ATTRIBUTE35 xsi:nil="true"/><ATTRIBUTE36 xsi:nil="true"/><ATTRIBUTE37 xsi:nil="true"/><ATTRIBUTE38 xsi:nil="true"/><ATTRIBUTE39 xsi:nil="true"/><ATTRIBUTE40 xsi:nil="true"/></X_RESPONSE_MESSAGE_ITEM><X_RESPONSE_MESSAGE_ITEM><ATTRIBUTE1>98224</ATTRIBUTE1><ATTRIBUTE2>120089</ATTRIBUTE2><ATTRIBUTE3>864382</ATTRIBUTE3><ATTRIBUTE4>I001</ATTRIBUTE4><ATTRIBUTE5>0</ATTRIBUTE5><ATTRIBUTE6 xsi:nil="true"/><ATTRIBUTE7 xsi:nil="true"/><ATTRIBUTE8 xsi:nil="true"/><ATTRIBUTE9 xsi:nil="true"/><ATTRIBUTE10>1ST INSTALMENT</ATTRIBUTE10><ATTRIBUTE11 xsi:nil="true"/><ATTRIBUTE12 xsi:nil="true"/><ATTRIBUTE13>1132931</ATTRIBUTE13><ATTRIBUTE14 xsi:nil="true"/><ATTRIBUTE15 xsi:nil="true"/><ATTRIBUTE16 xsi:nil="true"/><ATTRIBUTE17 xsi:nil="true"/><ATTRIBUTE18 xsi:nil="true"/><ATTRIBUTE19 xsi:nil="true"/><ATTRIBUTE20 xsi:nil="true"/><ATTRIBUTE21 xsi:nil="true"/><ATTRIBUTE22 xsi:nil="true"/><ATTRIBUTE23 xsi:nil="true"/><ATTRIBUTE24 xsi:nil="true"/><ATTRIBUTE25 xsi:nil="true"/><ATTRIBUTE26 xsi:nil="true"/><ATTRIBUTE27 xsi:nil="true"/><ATTRIBUTE28 xsi:nil="true"/><ATTRIBUTE29 xsi:nil="true"/><ATTRIBUTE30 xsi:nil="true"/><ATTRIBUTE31 xsi:nil="true"/><ATTRIBUTE32 xsi:nil="true"/><ATTRIBUTE33 xsi:nil="true"/><ATTRIBUTE34 xsi:nil="true"/><ATTRIBUTE35 xsi:nil="true"/><ATTRIBUTE36 xsi:nil="true"/><ATTRIBUTE37 xsi:nil="true"/><ATTRIBUTE38 xsi:nil="true"/><ATTRIBUTE39 xsi:nil="true"/><ATTRIBUTE40 xsi:nil="true"/></X_RESPONSE_MESSAGE_ITEM><X_RESPONSE_MESSAGE_ITEM><ATTRIBUTE1>98224</ATTRIBUTE1><ATTRIBUTE2>120089</ATTRIBUTE2><ATTRIBUTE3>864383</ATTRIBUTE3><ATTRIBUTE4>I002</ATTRIBUTE4><ATTRIBUTE5>10</ATTRIBUTE5><ATTRIBUTE6>12-JAN-2019</ATTRIBUTE6><ATTRIBUTE7 xsi:nil="true"/><ATTRIBUTE8>Within 360 days of Sale Date</ATTRIBUTE8><ATTRIBUTE9 xsi:nil="true"/><ATTRIBUTE10>2ND INSTALMENT</ATTRIBUTE10><ATTRIBUTE11>360</ATTRIBUTE11><ATTRIBUTE12>خلال 360 يوما من تاريخ البيع</ATTRIBUTE12><ATTRIBUTE13>1132932</ATTRIBUTE13><ATTRIBUTE14 xsi:nil="true"/><ATTRIBUTE15 xsi:nil="true"/><ATTRIBUTE16 xsi:nil="true"/><ATTRIBUTE17 xsi:nil="true"/><ATTRIBUTE18 xsi:nil="true"/><ATTRIBUTE19 xsi:nil="true"/><ATTRIBUTE20 xsi:nil="true"/><ATTRIBUTE21 xsi:nil="true"/><ATTRIBUTE22 xsi:nil="true"/><ATTRIBUTE23 xsi:nil="true"/><ATTRIBUTE24 xsi:nil="true"/><ATTRIBUTE25 xsi:nil="true"/><ATTRIBUTE26 xsi:nil="true"/><ATTRIBUTE27 xsi:nil="true"/><ATTRIBUTE28 xsi:nil="true"/><ATTRIBUTE29 xsi:nil="true"/><ATTRIBUTE30 xsi:nil="true"/><ATTRIBUTE31 xsi:nil="true"/><ATTRIBUTE32 xsi:nil="true"/><ATTRIBUTE33 xsi:nil="true"/><ATTRIBUTE34 xsi:nil="true"/><ATTRIBUTE35 xsi:nil="true"/><ATTRIBUTE36 xsi:nil="true"/><ATTRIBUTE37 xsi:nil="true"/><ATTRIBUTE38 xsi:nil="true"/><ATTRIBUTE39 xsi:nil="true"/><ATTRIBUTE40 xsi:nil="true"/></X_RESPONSE_MESSAGE_ITEM><X_RESPONSE_MESSAGE_ITEM><ATTRIBUTE1>98224</ATTRIBUTE1><ATTRIBUTE2>120089</ATTRIBUTE2><ATTRIBUTE3>864384</ATTRIBUTE3><ATTRIBUTE4>I003</ATTRIBUTE4><ATTRIBUTE5>20</ATTRIBUTE5><ATTRIBUTE6>12-MAY-2019</ATTRIBUTE6><ATTRIBUTE7 xsi:nil="true"/><ATTRIBUTE8>Within 480 Days of Sale Date</ATTRIBUTE8><ATTRIBUTE9 xsi:nil="true"/><ATTRIBUTE10>3RD INSTALMENT</ATTRIBUTE10><ATTRIBUTE11>480</ATTRIBUTE11><ATTRIBUTE12>خلال 480 يوما من تاريخ البيع</ATTRIBUTE12><ATTRIBUTE13>1132933</ATTRIBUTE13><ATTRIBUTE14 xsi:nil="true"/><ATTRIBUTE15 xsi:nil="true"/><ATTRIBUTE16 xsi:nil="true"/><ATTRIBUTE17 xsi:nil="true"/><ATTRIBUTE18 xsi:nil="true"/><ATTRIBUTE19 xsi:nil="true"/><ATTRIBUTE20 xsi:nil="true"/><ATTRIBUTE21 xsi:nil="true"/><ATTRIBUTE22 xsi:nil="true"/><ATTRIBUTE23 xsi:nil="true"/><ATTRIBUTE24 xsi:nil="true"/><ATTRIBUTE25 xsi:nil="true"/><ATTRIBUTE26 xsi:nil="true"/><ATTRIBUTE27 xsi:nil="true"/><ATTRIBUTE28 xsi:nil="true"/><ATTRIBUTE29 xsi:nil="true"/><ATTRIBUTE30 xsi:nil="true"/><ATTRIBUTE31 xsi:nil="true"/><ATTRIBUTE32 xsi:nil="true"/><ATTRIBUTE33 xsi:nil="true"/><ATTRIBUTE34 xsi:nil="true"/><ATTRIBUTE35 xsi:nil="true"/><ATTRIBUTE36 xsi:nil="true"/><ATTRIBUTE37 xsi:nil="true"/><ATTRIBUTE38 xsi:nil="true"/><ATTRIBUTE39 xsi:nil="true"/><ATTRIBUTE40 xsi:nil="true"/></X_RESPONSE_MESSAGE_ITEM><X_RESPONSE_MESSAGE_ITEM><ATTRIBUTE1>98224</ATTRIBUTE1><ATTRIBUTE2>120089</ATTRIBUTE2><ATTRIBUTE3>864385</ATTRIBUTE3><ATTRIBUTE4>I004</ATTRIBUTE4><ATTRIBUTE5>20</ATTRIBUTE5><ATTRIBUTE6>11-JUL-2019</ATTRIBUTE6><ATTRIBUTE7 xsi:nil="true"/><ATTRIBUTE8>Within 540 days of Sale Date</ATTRIBUTE8><ATTRIBUTE9 xsi:nil="true"/><ATTRIBUTE10>4TH INSTALMENT</ATTRIBUTE10><ATTRIBUTE11>540</ATTRIBUTE11><ATTRIBUTE12>خلال 540 يوما من تاريخ البيع</ATTRIBUTE12><ATTRIBUTE13>1132934</ATTRIBUTE13><ATTRIBUTE14 xsi:nil="true"/><ATTRIBUTE15 xsi:nil="true"/><ATTRIBUTE16 xsi:nil="true"/><ATTRIBUTE17 xsi:nil="true"/><ATTRIBUTE18 xsi:nil="true"/><ATTRIBUTE19 xsi:nil="true"/><ATTRIBUTE20 xsi:nil="true"/><ATTRIBUTE21 xsi:nil="true"/><ATTRIBUTE22 xsi:nil="true"/><ATTRIBUTE23 xsi:nil="true"/><ATTRIBUTE24 xsi:nil="true"/><ATTRIBUTE25 xsi:nil="true"/><ATTRIBUTE26 xsi:nil="true"/><ATTRIBUTE27 xsi:nil="true"/><ATTRIBUTE28 xsi:nil="true"/><ATTRIBUTE29 xsi:nil="true"/><ATTRIBUTE30 xsi:nil="true"/><ATTRIBUTE31 xsi:nil="true"/><ATTRIBUTE32 xsi:nil="true"/><ATTRIBUTE33 xsi:nil="true"/><ATTRIBUTE34 xsi:nil="true"/><ATTRIBUTE35 xsi:nil="true"/><ATTRIBUTE36 xsi:nil="true"/><ATTRIBUTE37 xsi:nil="true"/><ATTRIBUTE38 xsi:nil="true"/><ATTRIBUTE39 xsi:nil="true"/><ATTRIBUTE40 xsi:nil="true"/></X_RESPONSE_MESSAGE_ITEM><X_RESPONSE_MESSAGE_ITEM><ATTRIBUTE1>98224</ATTRIBUTE1><ATTRIBUTE2>120089</ATTRIBUTE2><ATTRIBUTE3>864386</ATTRIBUTE3><ATTRIBUTE4>I005</ATTRIBUTE4><ATTRIBUTE5>30</ATTRIBUTE5><ATTRIBUTE6>07-JAN-2020</ATTRIBUTE6><ATTRIBUTE7 xsi:nil="true"/><ATTRIBUTE8>Within 720 days of Sale Date</ATTRIBUTE8><ATTRIBUTE9 xsi:nil="true"/><ATTRIBUTE10>5TH INSTALMENT</ATTRIBUTE10><ATTRIBUTE11>720</ATTRIBUTE11><ATTRIBUTE12>خلال 720 يوما من تاريخ البيع</ATTRIBUTE12><ATTRIBUTE13>1132935</ATTRIBUTE13><ATTRIBUTE14 xsi:nil="true"/><ATTRIBUTE15 xsi:nil="true"/><ATTRIBUTE16 xsi:nil="true"/><ATTRIBUTE17 xsi:nil="true"/><ATTRIBUTE18 xsi:nil="true"/><ATTRIBUTE19 xsi:nil="true"/><ATTRIBUTE20 xsi:nil="true"/><ATTRIBUTE21 xsi:nil="true"/><ATTRIBUTE22 xsi:nil="true"/><ATTRIBUTE23 xsi:nil="true"/><ATTRIBUTE24 xsi:nil="true"/><ATTRIBUTE25 xsi:nil="true"/><ATTRIBUTE26 xsi:nil="true"/><ATTRIBUTE27 xsi:nil="true"/><ATTRIBUTE28 xsi:nil="true"/><ATTRIBUTE29 xsi:nil="true"/><ATTRIBUTE30 xsi:nil="true"/><ATTRIBUTE31 xsi:nil="true"/><ATTRIBUTE32 xsi:nil="true"/><ATTRIBUTE33 xsi:nil="true"/><ATTRIBUTE34 xsi:nil="true"/><ATTRIBUTE35 xsi:nil="true"/><ATTRIBUTE36 xsi:nil="true"/><ATTRIBUTE37 xsi:nil="true"/><ATTRIBUTE38 xsi:nil="true"/><ATTRIBUTE39 xsi:nil="true"/><ATTRIBUTE40 xsi:nil="true"/></X_RESPONSE_MESSAGE_ITEM></X_RESPONSE_MESSAGE><X_RETURN_STATUS>S</X_RETURN_STATUS><X_RETURN_MESSAGE>[6] Reg Term Records Fetched for Reg Id =98224</X_RETURN_MESSAGE></OutputParameters></env:Body></env:Envelope>';
              Damac_DLD_PaymentPlans.parsePayPlanResponse (xmlBody);
              apexpages.currentpage().getparameters().put ('docID', unitDocs.ID);
              obj.reUpload ();
              
              DAMAC_VALIDATE_EID obj2 = new DAMAC_VALIDATE_EID();
              DAMAC_VALIDATE_EID.checkEID ('test');
              DAMAC_VALIDATE_EID_RESP obj3 = new DAMAC_VALIDATE_EID_RESP ();
              
        Test.stopTest();
    }
    
   
}