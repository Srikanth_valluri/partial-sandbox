public class CREPortalHomeExtension {

    public String srType                                {get;set;}
    public Boolean createAdditionalParking              {get;set;}
    public Boolean createAssignmentRequest              {get;set;}
    public Boolean createAOPT                           {get;set;}
    public Boolean createBouncedCheque                  {get;set;}
    public Boolean createFundTransfer                   {get;set;}
    public Boolean createFundTransferActiveUnits        {get;set;}
    public Boolean createFurniturePackage               {get;set;}
    public Boolean createMortgage                       {get;set;}
    public Boolean createPenaltyWaiver                  {get;set;}
    public Boolean createProofOfPayment                 {get;set;}
    public Boolean createRefunds                        {get;set;}
    public Boolean createCOCD                           {get;set;}
    public Boolean createEarlyHandover                  {get;set;}
    public Boolean createNOCforVisa                     {get;set;}
    public Boolean createComplaint                      {get;set;}
    public Boolean createRentalPool                     {get;set;}
    public Boolean createTitleDeed                      {get;set;}
    public Boolean createPlotHandover                   {get;set;}

    public Boolean createRentalPoolTermination          {get;set;}
    public Boolean createRentalPoolAssignment           {get;set;}

    public Boolean home                                 {get;set;}
    public List<Account> accountsList                   {get;set;}
    public list<SelectOption> listAccountOptions        {get;set;}
    public Id selectedAccountId                         {get;set;}
    public Id selectedCaseId                            {get;set;}
    public Boolean isAccountSelected                    {get;set;}
    public Boolean createHandover                       {get;set;}
    public Map<Id,Account> mapIdAccount                 {get;set;}
    public Account account                              {get;set;}
    public string strAccountId                          {get;set;}
    public Account objAccount                           {get;set;}// account object to display details for AOPT process
    public Boolean isOrgAccount                      {get;set;}
    public String selectedAccountName                   {get; set;}
    public Boolean isFieldDisabled                      {get; set;}

      // Instance fields
    public String searchTerm {get; set;}
    public String selectedAccount {get; set;}

    public list<SelectOption> listCRESRs                {get;set;}
    public Id selectedCRE                               {get;set;}

    // JS Remoting action called when searching for a movie name
    @RemoteAction
    public static List<Account> searchMovie(String searchTerm) {
        System.debug('Account Name is: '+searchTerm );
        List<Account> lstAccount = Database.query('Select Id, Name from Account where name like \'%' + String.escapeSingleQuotes(searchTerm) + '%\'');
        return lstAccount;
    }
    /*public CREPortalHomeController(DashboardController dashboardController){
        init();
    }
    */

    public CREPortalHomeExtension(ApexPages.StandardSetController cases){
        init();
    }
/*
  public AccountRenewal(ApexPages.StandardController controller)
    {
        // extension constructor logic
    }
*/
    public void init() {
        selectedAccountName = '';
        srType = ApexPages.currentPage().getParameters().get('SRType');
        system.debug('srType == '+srType );
        strAccountId = ApexPages.currentPage().getParameters().get('AccountId');
        system.debug('strAccountId'+strAccountId);
        selectedCaseId = ApexPages.currentPage().getParameters().get('CaseId');

        populateCreateSRBoolean();
        isOrgAccount = true;
        accountsList = new List<Account>();
        mapIdAccount= new Map<Id,Account>();
        account = new Account();

        if (strAccountId != null && strAccountId != '') {
            selectedAccountId = strAccountId;
            //isAccountSelected = true;
            selectAccount();
            if (account != null) {
                selectedAccountName = account.Name;
            }
        } else {
            isAccountSelected = false;
        }

        if (strAccountId != null && strAccountId != '' && srType != null && srType != '' && selectedAccountName != null) {
            isFieldDisabled = true;
        } else {
            isFieldDisabled = false;
        }

        listAccountOptions = new list <SelectOption>();
        listAccountOptions.add(new SelectOption('', '--NONE--'));
        List<Account> listAccount = [SELECT Name, Id, Organisation_Name__c,Salutation
                                        ,IsPersonAccount,PersonEmail,Phone,Nationality__pc,Party_Type__c,SLA__c
                                     FROM Account
                                     ORDER BY Name
                                     LIMIT 250
                                    ];
        listAccount.addAll([SELECT Name, Id, Organisation_Name__c,Salutation
                                        ,IsPersonAccount,PersonEmail,Phone,Nationality__pc,Party_Type__c,SLA__c
                                     FROM Account WHERE Name = 'Shashank Maind']);
        for (Account account: listAccount) {
            listAccountOptions.add(new SelectOption(account.Id, account.Name));
            mapIdAccount.put(account.Id,account);
        }
        // fetch account details for AOPT process
        getCustomerPortfolio();
    }

    public void populateCreateSRBoolean(){

        createAdditionalParking = false;
        createAssignmentRequest = false;
        createNOCforVisa = false;
        createAOPT = false;
        createBouncedCheque = false;
        createFundTransfer = false;
        createFundTransferActiveUnits = false;
        createFurniturePackage = false;
        createMortgage = false;
        createPenaltyWaiver = false;
        createProofOfPayment = false;
        createRefunds = false;
        createCOCD = false;
        createEarlyHandover = false;
        createComplaint = false;
        createRentalPool = false;
        createTitleDeed = false;
        createHandover = false;
        createRentalPoolTermination = false;
        createRentalPoolAssignment = false;
        createPlotHandover = false;
        home = false;

        system.debug('srType$$$$$'+srType);

        listCRESRs = new list<SelectOption>();
        listCRESRs.add(new selectOption('', '- None -'));
        listCRESRs.add(new selectoption('AdditionalParking','ADDITIONAL PARKING'));
        listCRESRs.add(new selectoption('AssignmentRequest','ASSIGNMENT REQUEST'));
        listCRESRs.add(new selectoption('BouncedCheque','BOUNCED CHEQUE'));
        listCRESRs.add(new selectoption('AOPT','AOPT'));
        listCRESRs.add(new selectoption('FundTransfer','FUND TRANSFER REJECTED UNITS'));
        listCRESRs.add(new selectoption('FundTransferActiveUnits','FUND TRANSFER ACTIVE UNITS'));
        listCRESRs.add(new selectoption('FurniturePackage','FURNITURE PACKAGE'));
        listCRESRs.add(new selectoption('Mortgage','MORTGAGE'));
        listCRESRs.add(new selectoption('PenaltyWaiver','PENALTY WAIVER'));
        listCRESRs.add(new selectoption('ProofOfPayment','PROOF OF PAYMENT'));
        listCRESRs.add(new selectoption('Refunds','REFUNDS'));
        listCRESRs.add(new selectoption('COCD','COD'));
        listCRESRs.add(new selectoption('Early_Handover','EARLY HANDOVER'));
        listCRESRs.add(new selectoption('NOCVisa','NOC FOR VISA'));
        listCRESRs.add(new selectoption('Complaint','COMPLAINT'));
        listCRESRs.add(new selectoption('RentalPool','RENTAL POOL'));
        //listCRESRs.add(new selectoption('Home','Home'));
        listCRESRs.add(new selectoption('TitleDeed','TITLE DEED'));
        listCRESRs.add(new selectoption('Handover','HANDOVER'));
        listCRESRs.add(new selectoption('RentalPoolTermination','RP TERMINATION'));
        listCRESRs.add(new selectoption('RentalPoolAssignment','RP ASSIGNMENT'));
        //listCRESRs.add(new selectoption('PlotHandover','PlotHandover'));

        if(srType != null && srType != ''){
            if(srType.equals('AdditionalParking')){
                createAdditionalParking = true;
            } else if(srType.equals('AssignmentRequest')){
                createAssignmentRequest = true;
            } else if(srType.equals('AOPT')){
                createAOPT = true;
            } else if(srType.equals('BouncedCheque')){
                createBouncedCheque = true;
            } else if(srType.equals('FundTransfer') || srType.equals('FundTransferActiveUnits')){
                createFundTransfer = true;
            } /*else if(srType.equals('FundTransferActiveUnits')){
                createFundTransferActiveUnits = true;
            }*/else if(srType.equals('FurniturePackage')){
                createFurniturePackage = true;
            } else if(srType.equals('Mortgage')){
                createMortgage = true;
            } else if(srType.equals('PenaltyWaiver')){
                system.debug('Inside penalty waiver...');
                createPenaltyWaiver = true;
            } else if(srType.equals('ProofOfPayment')){
                createProofOfPayment = true;
            } else if(srType.equals('Refunds')){
                createRefunds = true;
            } else if(srType.equals('COCD')){
                createCOCD = true;
            } else if(srType.equals('Early_Handover')){
                createEarlyHandover = true;
            } else if(srType.equals('NOCVisa')){
                createNOCforVisa = true;
            } else if(srType.equals('Complaint')){
                createComplaint = true;
            } else if(srType.equals('Home')){
                home = true;
            } else if(srType.equals('RentalPool')){
                createRentalPool = true;
            } else if(srType.equals('TitleDeed')){
                createTitleDeed = true;
            } else if(srType.equals('Handover')){
                createHandover = true;
            } else if(srType.equals('RentalPoolTermination')){
                createRentalPoolTermination = true;
            } else if(srType.equals('RentalPoolAssignment')){
                createRentalPoolAssignment = true;
            } else if(srType.equals('PlotHandover')){
                createPlotHandover = true;
            }
        } else {
            home = true;
        }
        listCRESRs.sort();
    }

    public void selectAccount(){
        if(selectedAccountId != null){
            isAccountSelected = true;
            isOrgAccount = true;
            //account = mapIdAccount.get(selectedAccountId);
            account = [ SELECT Id
                             , Title__c
                             , Name
                             , Party_ID__c
                             , Nationality__c
                             , Passport_Number__c
                             , IsPersonAccount
                             , PersonMobilePhone
                             , Salutation
                             , Phone
                             , PersonEmail
                             , Nationality__pc
                             , SLA__c
                             , Organisation_Name__c
                             , CR_Number__c
                             , Mobile__c
                             , Email__c
                             , Email__pc
                             , PersonMailingStreet
                             , PersonMailingPostalCode
                             , PersonMailingCity
                             , PersonMailingState
                             , PersonMailingCountry
                             , Address_Line_1__c
                             , Address_Line_2__c
                             , Address_Line_3__c
                             , Address_Line_4__c
                             , Party_Type__c,
                             Mobile_Phone_Encrypt__pc,
                             Address_Line_1__pc,
                             Address_Line_2__pc,
                             Address_Line_3__pc,
                             Address_Line_4__pc,
                             City__c,
                             Country__c,
                             City__pc,
                             Country__pc
                             ,Passport_Number__pc
                             //Joint Buyer info pending
                          FROM Account
                         WHERE Id = :selectedAccountId ];
             if(account.Party_Type__c == 'PERSON') {
                 isOrgAccount = false;
             }
        } else {
            isAccountSelected = false;
        }
    }

  // method used to retrieve customer portfolio details
  public void getCustomerPortfolio()
  {
  if(strAccountId != null){
        objAccount = [ SELECT Id,Name,Title__c,Organisation_Name__c
                      ,PersonTitle,PersonMobilePhone
                      ,PersonEmail,FirstName,LastName
                      ,MiddleName,Party_ID__c,Nationality__pc
                      ,Party_Type__c,Passport_Number__c,CR_Number__c
                      FROM Account WHERE ID = : strAccountId ];
      }
  }

}