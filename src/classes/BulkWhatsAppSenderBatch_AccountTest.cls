@isTest
public class BulkWhatsAppSenderBatch_AccountTest {
    @isTest
    static void testBulkWhatsappSenderBatch(){
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        objAccount.Send_Bulk_Email__c = true;
		objAccount.Mobile_Phone_Encrypt__pc = '0012345';
        objAccount.Mobile__c = '0012345678';
        insert objAccount;
        
        Test.startTest();
        Database.executeBatch(new BulkWhatsAppSenderBatch_Account(), 200);
        Test.stopTest();
    }
    
    @isTest
    static void testBulkWhatsappSenderBatch1(){
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        objAccount.Send_Bulk_Email__c = true;
		objAccount.Mobile_Phone_Encrypt__pc = '12345';
        objAccount.Mobile__c = '12345678';
        insert objAccount;
        
        Test.startTest();
        BulkWhatsAppSenderBatch_Account objBulkWhatsapp = new BulkWhatsAppSenderBatch_Account();
		objBulkWhatsapp.createRequest('test', 'test', 'test', true, objAccount);       
        Test.stopTest();
    }
}