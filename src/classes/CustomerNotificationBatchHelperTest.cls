/******************************************************************************
* Description - Test class developed for CustomerNotificationBatchHelper
*
* Version            Date            Author                    Description
* 1.0                11/12/17        Naresh Kaneriya (Accely)   Initial Draft
********************************************************************************/
@isTest
private class CustomerNotificationBatchHelperTest{
  
    public static testmethod void Test_makeQueryString(){
        Test.startTest();
        CustomerNotificationBatchHelper.makeQueryString();
        Test.stopTest();
    }

   // Test Method: sendSMSToCustomer
    public static testmethod void Test_sendSMSToCustomerTry(){
        
        string strmobilenumber = '9956785214';
        string strmessagebody = 'Message sent';
        Calling_List__c objcall = new Calling_List__c();
        Test.startTest(); 
        Test.setMock(HttpCalloutMock.class, new MockHttpAgentOTPController()); 
        //CustomerNotificationBatchHelper.sendSMSToCustomer(strmobilenumber, strmessagebody, objcall);
        CustomerNotificationBatchHelper.sendSMSToCustomer( new SMS_History__c() );
        System.assert((CustomerNotificationBatchHelper.sendSMSToCustomer( new SMS_History__c() )) != null);
        Test.stopTest();
    } 
    
     // Test Method: sendSMSToCustomer
    public static testmethod void Test_sendSMSToCustomerCatch(){
        
        string strmobilenumber = '9956785214';
        string strmessagebody = 'Message sent';
        Calling_List__c objcall = new Calling_List__c(); 
        Test.startTest(); 
        //CustomerNotificationBatchHelper.sendSMSToCustomer(strmobilenumber, strmessagebody, objcall);
        CustomerNotificationBatchHelper.sendSMSToCustomer( new SMS_History__c() );
        System.assert((CustomerNotificationBatchHelper.sendSMSToCustomer( new SMS_History__c() )) != null);
        Test.stopTest();
    }
    
    // Test Method: getMapDaysEmailTemplate
    public static testmethod void Test_getMapDaysEmailTemplate(){
        Test.startTest();
        
        list<Collection_Customer_Notification_Setting__mdt> lstsetting = new list<Collection_Customer_Notification_Setting__mdt>();
        lstsetting = [SELECT  Email_Template_API_Name__c,Number_of_Days__c FROM Collection_Customer_Notification_Setting__mdt limit 1];
        CustomerNotificationBatch.CustomerNotificationInfoWrapper obj =  new CustomerNotificationBatch.CustomerNotificationInfoWrapper();
        CustomerNotificationBatchHelper.getMapDaysEmailTemplate(lstsetting, obj);
        Test.stopTest(); 
    }
    
        // Test Method: getEmailTemplateMap
    public static testmethod void Test_getEmailTemplateMap(){
        Test.startTest();
        
         EmailTemplate ET = new EmailTemplate();
         ET.isActive = true;
         ET.Name = 'name';
         ET.DeveloperName = 'unique_name_addSomethingSpecialHere';
         ET.TemplateType = 'text';
         ET.FolderId = UserInfo.getUserId();
         insert ET ;
         
         System.assertEquals(ET.DeveloperName,'unique_name_addSomethingSpecialHere');
         map<Id, EmailTemplate> mapemailtemplate = new map<Id,EmailTemplate>();
         mapemailtemplate.put(ET.Id ,ET);
         CustomerNotificationBatch.CustomerNotificationInfoWrapper objinfowrap = new CustomerNotificationBatch.CustomerNotificationInfoWrapper();
         CustomerNotificationBatchHelper.getEmailTemplateMap(mapemailtemplate, objinfowrap);
         Test.stopTest();
    }
    
}