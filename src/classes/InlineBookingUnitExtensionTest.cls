/***********************************************************************************************
 * @Name              : InlineBookingUnitExtensionTest
 * @Description       : Test class developed for 'InlineBookingUnitExtension'
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log
 * 1.0        20/11/17        Lochan            Initial Draft
 * 1.1        19/08/2017      Neha Dave         Added test methods for restricting certain users
 *                                              sending emails to customers.
***********************************************************************************************/
@isTest
public class InlineBookingUnitExtensionTest {

    public static String CALL_NOW_BUTTON_SET_TO_FALSE= 'The value should be true when the'
		+' Dispute Flag has values D,E,L,C & BC and Profiles must contain FM/Property or if it is a'
        +' Collection Profile';

    static testMethod void unitSOAtestMethod() {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='12345');
        insert objAcc;

        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        Booking__c objBooking = new Booking__c(Account__c=objAcc.Id, Deal_SR__c=dealSR.Id);
        insert objBooking;

        Booking_Unit__c BUObj = new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='Test name',
        Registration_ID__c = '92061', Registration_Status__c = 'Active Status', Unit_Selling_Price_AED__c = 100);
        insert BUObj;

        Test.startTest();

        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );    
        PageReference pageRef = Page.BookingUnitPage;
        pageRef.getParameters().put('id', String.valueOf(objAcc.Id));
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController controller = new ApexPages.standardController(objAcc);
        InlineBookingUnitExtension obj = new InlineBookingUnitExtension(controller);
        
        PageReference pageRef1 = Page.BookingUnitPage;
        pageRef1.getParameters().put('bookingUnitId', String.valueOf(BUObj.Id));
        Test.setCurrentPage(pageRef1);
        
        obj.buId = BUObj.Id;
        obj.GenerateSOA();   
        Test.stopTest();
    }  
	
	static testMethod void unitSOAtestMethod2() {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='12345');
        insert objAcc;

        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        Booking__c objBooking = new Booking__c(Account__c=objAcc.Id, Deal_SR__c=dealSR.Id);
        insert objBooking;

        Booking_Unit__c BUObj = new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='Test name',
        Registration_ID__c = '92061', Registration_Status__c = 'Active Status', Unit_Selling_Price_AED__c = 100);
        insert BUObj;

        Test.startTest();

        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(2) );    
        PageReference pageRef = Page.BookingUnitPage;
        pageRef.getParameters().put('id', String.valueOf(objAcc.Id));
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController controller = new ApexPages.standardController(objAcc);
        InlineBookingUnitExtension obj = new InlineBookingUnitExtension(controller);
        
        PageReference pageRef1 = Page.BookingUnitPage;
        pageRef1.getParameters().put('bookingUnitId', String.valueOf(BUObj.Id));
        Test.setCurrentPage(pageRef1);
        
        obj.buId = BUObj.Id;
        obj.GenerateSOA();   
        Test.stopTest();
    }  
	
	static testMethod void unitSOAtestMethod1() {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='12345');
        insert objAcc;

        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        Booking__c objBooking = new Booking__c(Account__c=objAcc.Id, Deal_SR__c=dealSR.Id);
        insert objBooking;

        Booking_Unit__c BUObj = new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='Test name',
        Registration_ID__c = '92061', Registration_Status__c = 'Active Status', Unit_Selling_Price_AED__c = 100);
        insert BUObj;
		
		Booking_Unit_Inactive_Status__c inactiveStatusList = new Booking_Unit_Inactive_Status__c(Name='Active Status', Status__c = 'Active Status');
		insert inactiveStatusList;

        Test.startTest();

        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );    
        PageReference pageRef = Page.BookingUnitPage;
        pageRef.getParameters().put('id', String.valueOf(objAcc.Id));
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController controller = new ApexPages.standardController(objAcc);
        InlineBookingUnitExtension obj = new InlineBookingUnitExtension(controller);
        
        PageReference pageRef1 = Page.BookingUnitPage;
        pageRef1.getParameters().put('bookingUnitId', String.valueOf(BUObj.Id));
        Test.setCurrentPage(pageRef1);
        
        obj.buId = BUObj.Id;
        obj.GenerateSOA();   
        Test.stopTest();
    }  
	
	
	static testMethod void redirect() {
		Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='12345');
        insert objAcc;

        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        Booking__c objBooking = new Booking__c(Account__c=objAcc.Id, Deal_SR__c=dealSR.Id);
        insert objBooking;

        Booking_Unit__c BUObj = new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='Test name',
        Registration_ID__c = '92061', Registration_Status__c = 'Active Status', Unit_Selling_Price_AED__c = 100);
        insert BUObj;

        Test.startTest();

        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );    
        PageReference pageRef = Page.BookingUnitPage;
        pageRef.getParameters().put('id', String.valueOf(objAcc.Id));
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController controller = new ApexPages.standardController(objAcc);
        InlineBookingUnitExtension obj = new InlineBookingUnitExtension(controller);
		pagereference newpg = obj.redirect();
		Test.stopTest();
	}
	
	

    static testMethod void penaltySOAtestMethod() {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='12345');
        insert objAcc;

        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        Booking__c objBooking = new Booking__c(Account__c=objAcc.Id, Deal_SR__c=dealSR.Id);
        insert objBooking;

        Booking_Unit__c BUObj = new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='Test name',
        Registration_ID__c = '92061', Registration_Status__c = 'Active Status', Unit_Selling_Price_AED__c = 100);
        insert BUObj;

        Test.startTest();

        SOAPCalloutServiceMock.returnToMe = new Map<String, GenerateSOPService.GeneratePenaltyStatmentResponse_element>();
        GenerateSOPService.GeneratePenaltyStatmentResponse_element response1 = new GenerateSOPService.GeneratePenaltyStatmentResponse_element();
        response1.return_x = '{"Status":"S","PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=338396 and Request Id :40185767 ...","Message":"Process Completed Returning 1 Response Message(s)...","PARAM_ID":"78152","REQUEST_ID":"40185767","STAGE_ID":"338396","URL":"https://sftest.deeprootsurface.com/docs/e/40185767_78152_PENALTY.pdf"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
    
        PageReference pageRef = Page.BookingUnitPage;
        pageRef.getParameters().put('id', String.valueOf(objAcc.Id));
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController controller = new ApexPages.standardController(objAcc);
        InlineBookingUnitExtension obj = new InlineBookingUnitExtension(controller);
        
        PageReference pageRef1 = Page.BookingUnitPage;
        pageRef1.getParameters().put('bookingUnitId', String.valueOf(BUObj.Id));
        Test.setCurrentPage(pageRef1);
        
        obj.buId = BUObj.Id;
        obj.PenaltySOA();   
        Test.stopTest();
    } 
	
	static testMethod void penaltySOAtestMethod2() {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='12345');
        insert objAcc;

        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        Booking__c objBooking = new Booking__c(Account__c=objAcc.Id, Deal_SR__c=dealSR.Id);
        insert objBooking;

        Booking_Unit__c BUObj = new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='Test name',
        Registration_ID__c = '92061', Registration_Status__c = 'Active Status', Unit_Selling_Price_AED__c = 100);
        insert BUObj;

        Test.startTest();

        SOAPCalloutServiceMock.returnToMe = new Map<String, GenerateSOPService.GeneratePenaltyStatmentResponse_element>();
        GenerateSOPService.GeneratePenaltyStatmentResponse_element response1 = new GenerateSOPService.GeneratePenaltyStatmentResponse_element();
        response1.return_x = '{"Status":"E","PROC_STATUS":"E","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=338396 and Request Id :40185767 ...","Message":"Process Completed Returning 1 Response Message(s)...","PARAM_ID":"78152","REQUEST_ID":"40185767","STAGE_ID":"338396","URL":""}';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
    
        PageReference pageRef = Page.BookingUnitPage;
        pageRef.getParameters().put('id', String.valueOf(objAcc.Id));
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController controller = new ApexPages.standardController(objAcc);
        InlineBookingUnitExtension obj = new InlineBookingUnitExtension(controller);
        
        PageReference pageRef1 = Page.BookingUnitPage;
        pageRef1.getParameters().put('bookingUnitId', String.valueOf(BUObj.Id));
        Test.setCurrentPage(pageRef1);
        
        obj.buId = BUObj.Id;
        obj.PenaltySOA();   
        Test.stopTest();
    } 

static testMethod void penaltySOAtestMethod1() {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='12345');
        insert objAcc;

        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        Booking__c objBooking = new Booking__c(Account__c=objAcc.Id, Deal_SR__c=dealSR.Id);
        insert objBooking;

        Booking_Unit__c BUObj = new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='Test name',
        Registration_ID__c = '92061', Registration_Status__c = 'Active Status', Unit_Selling_Price_AED__c = 100);
        insert BUObj;
		
		Booking_Unit_Inactive_Status__c inactiveStatusList = new Booking_Unit_Inactive_Status__c(Name='Active Status', Status__c = 'Active Status');
		insert inactiveStatusList;

        Test.startTest();

        SOAPCalloutServiceMock.returnToMe = new Map<String, GenerateSOPService.GeneratePenaltyStatmentResponse_element>();
        GenerateSOPService.GeneratePenaltyStatmentResponse_element response1 = new GenerateSOPService.GeneratePenaltyStatmentResponse_element();
        response1.return_x = '{"Status":"S","PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=338396 and Request Id :40185767 ...","Message":"Process Completed Returning 1 Response Message(s)...","PARAM_ID":"78152","REQUEST_ID":"40185767","STAGE_ID":"338396","URL":"https://sftest.deeprootsurface.com/docs/e/40185767_78152_PENALTY.pdf"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
    
        PageReference pageRef = Page.BookingUnitPage;
        pageRef.getParameters().put('id', String.valueOf(objAcc.Id));
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController controller = new ApexPages.standardController(objAcc);
        InlineBookingUnitExtension obj = new InlineBookingUnitExtension(controller);
        
        PageReference pageRef1 = Page.BookingUnitPage;
        pageRef1.getParameters().put('bookingUnitId', String.valueOf(BUObj.Id));
        Test.setCurrentPage(pageRef1);
        
        obj.buId = BUObj.Id;
        obj.PenaltySOA();   
        Test.stopTest();
    } 	

    static testMethod void DGMSOAtestMethod() {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='12345');
        insert objAcc;

        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        Booking__c objBooking = new Booking__c(Account__c=objAcc.Id, Deal_SR__c=dealSR.Id);
        insert objBooking;

        Booking_Unit__c BUObj = new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='Test name',
        Registration_ID__c = '92061', Registration_Status__c = 'Active Status', Unit_Selling_Price_AED__c = 100);
        insert BUObj;
		
		Booking_Unit_Inactive_Status__c inactiveStatusList = new Booking_Unit_Inactive_Status__c(Name='Active Status', Status__c = 'Active Status');
		insert inactiveStatusList;

        Test.startTest();
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );    
        PageReference pageRef = Page.BookingUnitPage;
        pageRef.getParameters().put('id', String.valueOf(objAcc.Id));
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController controller = new ApexPages.standardController(objAcc);
        InlineBookingUnitExtension obj = new InlineBookingUnitExtension(controller);
        
        PageReference pageRef1 = Page.BookingUnitPage;
        pageRef1.getParameters().put('bookingUnitId', String.valueOf(BUObj.Id));
        Test.setCurrentPage(pageRef1);
        
        obj.buId = BUObj.Id;
        obj.DGMSOA();   

        //InlineBookingUnitExtension.BUInfo objBUInfo = new InlineBookingUnitExtension.BUInfo();
        Test.stopTest();
    }  
	
	static testMethod void DGMSOAtestMethod2() {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='12345');
        insert objAcc;

        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        Booking__c objBooking = new Booking__c(Account__c=objAcc.Id, Deal_SR__c=dealSR.Id);
        insert objBooking;

        Booking_Unit__c BUObj = new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='Test name',
        Registration_ID__c = '92061', Registration_Status__c = 'Active Status', Unit_Selling_Price_AED__c = 100);
        insert BUObj;
		
		Booking_Unit_Inactive_Status__c inactiveStatusList = new Booking_Unit_Inactive_Status__c(Name='Active Status', Status__c = 'Active Status');
		insert inactiveStatusList;

        Test.startTest();
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(2) );    
        PageReference pageRef = Page.BookingUnitPage;
        pageRef.getParameters().put('id', String.valueOf(objAcc.Id));
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController controller = new ApexPages.standardController(objAcc);
        InlineBookingUnitExtension obj = new InlineBookingUnitExtension(controller);
        
        PageReference pageRef1 = Page.BookingUnitPage;
        pageRef1.getParameters().put('bookingUnitId', String.valueOf(BUObj.Id));
        Test.setCurrentPage(pageRef1);
        
        obj.buId = BUObj.Id;
        obj.DGMSOA();   

        //InlineBookingUnitExtension.BUInfo objBUInfo = new InlineBookingUnitExtension.BUInfo();
        Test.stopTest();
    }  
	
	static testMethod void DGMSOAtestMethod1() {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='12345');
        insert objAcc;

        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        Booking__c objBooking = new Booking__c(Account__c=objAcc.Id, Deal_SR__c=dealSR.Id);
        insert objBooking;

        Booking_Unit__c BUObj = new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='Test name',
        Registration_ID__c = '92061', Registration_Status__c = 'Active Status', Unit_Selling_Price_AED__c = 100);
        insert BUObj;
		

        Test.startTest();
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );    
        PageReference pageRef = Page.BookingUnitPage;
        pageRef.getParameters().put('id', String.valueOf(objAcc.Id));
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController controller = new ApexPages.standardController(objAcc);
        InlineBookingUnitExtension obj = new InlineBookingUnitExtension(controller);
        
        PageReference pageRef1 = Page.BookingUnitPage;
        pageRef1.getParameters().put('bookingUnitId', String.valueOf(BUObj.Id));
        Test.setCurrentPage(pageRef1);
        
        obj.buId = BUObj.Id;
        obj.DGMSOA();   

        //InlineBookingUnitExtension.BUInfo objBUInfo = new InlineBookingUnitExtension.BUInfo();
        Test.stopTest();
    }  

    @isTest
    public static void collectionCREProfileTest(){
        UserRole userRole = [Select Id
								  , Name
							   FROM UserRole
							  WHERE Name = 'Collection - CRE'
							  LIMIT 1];
		User user = new User();
		user.ProfileID = [SELECT Id
							FROM Profile
						   WHERE Name='Collection - CRE'].id;
		user.EmailEncodingKey = 'ISO-8859-1';
		user.LanguageLocaleKey = 'en_US';
		user.TimeZoneSidKey = 'America/New_York';
		user.LocaleSidKey = 'en_US';
        user.FirstName = 'first';
		user.LastName = 'last';
		user.Username = 'test126097@appirio.com';
		user.CommunityNickname = 'testUser123';
		user.Alias = 't1';
		user.Email = 'no@email.com';
		user.IsActive = true;
		user.userRoleId = userRole.Id;
		insert user;

		System.runAs(user) {

			Id personAccRTId =
				Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
			Account objAcc = new Account(RecordTypeId = personAccRTId
										, FirstName = 'Test FirstName'
										, LastName = 'Test LastName'
										, Units_under_DELC__c = true);
			insert objAcc;

			//Insert Service Request
			NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
			objSR.Agency__c = objAcc.Id ;
			insert objSR ;

			Id dealId = objSR.Deal_ID__c;
			//Insert Bookings
			List<Booking__c> lstBookings =
				TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id,1);
			insert lstBookings ;

			//Insert Booking Units
			List<Booking_Unit__c> lstBookingUnits =
				TestDataFactory_CRM.createBookingUnits(lstBookings,1);
			for(Booking_Unit__c  buObj :lstBookingUnits){
				buObj.Dispute_Flag__c = 'BC';
			}

			insert lstBookingUnits;

			PageReference pageRef = Page.Account_SendEmail;
			pageRef.getParameters().put('id', String.valueOf(objAcc.Id));
			Test.setCurrentPage(pageRef);
			ApexPages.standardController controller = new ApexPages.standardController(objAcc);
			InlineBookingUnitExtension obj = new InlineBookingUnitExtension(controller);
            obj.redirect();
			System.AssertEquals(true, obj.sendEmail, CALL_NOW_BUTTON_SET_TO_FALSE);
		}
    }
    
    /************************************************************************
	* @Description : Test method to verify if the sendEmail is set to
	*                true when the user profile belongs to Collections and
	*                DELC on Account field value is False.
	* @Params      : None
	* @Return      : None
	*************************************************************************/
	@isTest
	public static void unitsUnderDELCFalseTest(){
		UserRole userRole = [Select Id
								  , Name
							   FROM UserRole
							  WHERE Name = 'Collection - Manager'
							  LIMIT 1];
		User user = new User();
		user.ProfileID = [SELECT Id
							FROM Profile
						   WHERE Name='Collection - Manager'].id;
		user.EmailEncodingKey = 'ISO-8859-1';
		user.LanguageLocaleKey = 'en_US';
		user.TimeZoneSidKey = 'America/New_York';
		user.LocaleSidKey = 'en_US';
		user.FirstName = 'first';
		user.LastName = 'last';
		user.Username = 'test126097@appirio.com';
		user.CommunityNickname = 'testUser123';
		user.Alias = 't1';
		user.Email = 'no@email.com';
		user.IsActive = true;
		user.userRoleId = userRole.Id;
		insert user;

		System.runAs(user) {
			Id personAccRTId =
				Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
				Account objAcc = new Account(RecordTypeId = personAccRTId
											, FirstName = 'Test FirstName'
											, LastName = 'Test LastName'
											, Units_under_DELC__c = false);
			insert objAcc;

			//Insert Service Request
			NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
			objSR.Agency__c = objAcc.Id ;
			insert objSR ;

			Id dealId = objSR.Deal_ID__c;
			//Insert Bookings
			List<Booking__c> lstBookings =
				TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id,1);
			insert lstBookings ;

			//Insert Booking Units
			List<Booking_Unit__c> lstBookingUnits =
				TestDataFactory_CRM.createBookingUnits(lstBookings,1);
			insert lstBookingUnits;

			PageReference pageRef = Page.Account_SendEmail;
			pageRef.getParameters().put('id', String.valueOf(objAcc.Id));
			Test.setCurrentPage(pageRef);
			ApexPages.standardController controller = new ApexPages.standardController(objAcc);
			InlineBookingUnitExtension obj = new InlineBookingUnitExtension(controller);
            obj.redirect();
			System.AssertEquals(true, obj.sendEmail, CALL_NOW_BUTTON_SET_TO_FALSE);
		}
	}

	/************************************************************************
	* @Description : Test method to verify if the sendEmail is set to
	*                true when the user profile is not allowed and
	*                DELC on Account field value is true.
	* @Params      : None
	* @Return      : None
	*************************************************************************/
	@isTest
	public static void otherProfilesTest(){
		User user = new User();
		user.ProfileID = [SELECT Id
							FROM Profile
						   WHERE Name='Ameyo CTI'].id;
		user.EmailEncodingKey = 'ISO-8859-1';
		user.LanguageLocaleKey = 'en_US';
		user.TimeZoneSidKey = 'America/New_York';
		user.LocaleSidKey = 'en_US';
		user.FirstName = 'first';
		user.LastName = 'last';
		user.Username = 'test126097@appirio.com';
		user.CommunityNickname = 'testUser123';
		user.Alias = 't1';
		user.Email = 'no@email.com';
		user.IsActive = true;
		insert user;

		System.runAs(user) {

			Id personAccRTId =
				Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
				Account objAcc = new Account(RecordTypeId = personAccRTId
											, FirstName = 'Test FirstName'
											, LastName = 'Test LastName'
											, Units_under_DELC__c = true);
			insert objAcc;

			//Insert Service Request
			NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
			objSR.Agency__c = objAcc.Id ;
			insert objSR ;

			Id dealId = objSR.Deal_ID__c;
			//Insert Bookings
			List<Booking__c> lstBookings =
				TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id,1);
			insert lstBookings ;

			//Insert Booking Units
			List<Booking_Unit__c> lstBookingUnits =
				TestDataFactory_CRM.createBookingUnits(lstBookings,1);
			insert lstBookingUnits;

			PageReference pageRef = Page.Account_SendEmail;
			pageRef.getParameters().put('id', String.valueOf(objAcc.Id));
			Test.setCurrentPage(pageRef);
			ApexPages.standardController controller = new ApexPages.standardController(objAcc);
			InlineBookingUnitExtension obj = new InlineBookingUnitExtension(controller);
            obj.redirect();
			System.AssertEquals(false, obj.sendEmail, CALL_NOW_BUTTON_SET_TO_FALSE);
		}
	}

	/************************************************************************
	* @Description : Test method to verify if the sendEmail is set to
	*                true when the user profile contains FM and
	*                DELC on Account field value is False.
	* @Params      : None
	* @Return      : None
	*************************************************************************/
	@isTest
	public static void containsFMTest(){
		User user = new User();
		user.ProfileID = [SELECT Id
							FROM Profile
						   WHERE Name='FM Collections'].id;
		user.EmailEncodingKey = 'ISO-8859-1';
		user.LanguageLocaleKey = 'en_US';
		user.TimeZoneSidKey = 'America/New_York';
		user.LocaleSidKey = 'en_US';
		user.FirstName = 'first';
		user.LastName = 'last';
		user.Username = 'test126097@appirio.com';
		user.CommunityNickname = 'testUser123';
		user.Alias = 't1';
		user.Email = 'no@email.com';
		user.IsActive = true;
		insert user;

		System.runAs(user) {

			Id personAccRTId =
				Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
				Account objAcc = new Account(RecordTypeId = personAccRTId
											, FirstName = 'Test FirstName'
											, LastName = 'Test LastName'
											, Units_under_DELC__c = true);
			insert objAcc;

			//Insert Service Request
			NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
			objSR.Agency__c = objAcc.Id ;
			insert objSR ;

			Id dealId = objSR.Deal_ID__c;
			//Insert Bookings
			List<Booking__c> lstBookings =
				TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id,1);
			insert lstBookings ;

			//Insert Booking Units
			List<Booking_Unit__c> lstBookingUnits =
				TestDataFactory_CRM.createBookingUnits(lstBookings,1);
			insert lstBookingUnits;

			PageReference pageRef = Page.Account_SendEmail;
			pageRef.getParameters().put('id', String.valueOf(objAcc.Id));
			Test.setCurrentPage(pageRef);
			ApexPages.standardController controller = new ApexPages.standardController(objAcc);
			InlineBookingUnitExtension obj = new InlineBookingUnitExtension(controller);
            obj.redirect();
			System.AssertEquals(true, obj.sendEmail, CALL_NOW_BUTTON_SET_TO_FALSE);
		}
	}

}