/**************************************************************************************************
* Name               : UploadSRDocsController                                                     *
* Description        : Controller class to Upload SR documents as Files, display the exising file *
* Created Date       : 26/01/2018                                                                 *
* Created By         : Srikanth                                                                   *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR            DATE            COMMENTS                                          *
* 1.0         Srikanth         26/01/2018      Initial Draft.                                     *
* 1.1         QBurst          30/04/2020       Added Awaiting URRF Step change on upload          *
* 1.2         QBurst          06/05/2020       Added Token Document Re upload                     *
**************************************************************************************************/

public class UploadSRDocsController {

    public static Boolean isPortalUser {get;set;}
   // public List <NSIBPM__SR_Doc__c> srDocs { get; set; }
     public List <Unit_Documents__c > srDocs { get; set; }
    public NSIBPM__Service_Request__c srDoc { get; set; }
    //public List <NSIBPM__SR_Doc__c> completedDocs { get; set; }
    public List <Unit_Documents__c> completedDocs { get; set; }
    public Integer pendingDocumentsCount { get; set; }
    public Integer uploadedDocsCount { get; set; } 
    public static Boolean midOfficeApproval {get;set;} // 1.1
    public static Boolean tokenApproval {get;set;} // 1.2
    
    /*********************************************************************************************
* @Description : Controller class.                                                           *
* @Params      : void                                                                        *
* @Return      : void                                                                        *
*********************************************************************************************/ 
    public UploadSRDocsController () {
        isPortalUser = [SELECT IsPortalEnabled  FROM user WHERE id=:userinfo.getuserid() LIMIT 1].IsPortalEnabled;
        midOfficeApproval = false; // 1.1
        tokenApproval = false; // 1.2
        srDocs = new List <Unit_Documents__c> ();
        completedDocs = new List <Unit_Documents__c> ();
        uploadedDocsCount = 0;
        pendingDocumentsCount = 0;
        Id recordId = apexpages.currentPage().getParameters().get ('id');
        // 1.1 starts
        srDoc = [ SELECT Name,Step_2_1_Mid_Office_Approval_Status__c, Step_2_0_Token_Payment_Status__c 
                  FROM NSIBPM__Service_Request__c WHERE ID =: recordId ];
        if(srDoc.Step_2_1_Mid_Office_Approval_Status__c == 'Mid Office Rejected'){
            midOfficeApproval = true;
        } // 1.1 ends
        // 1.2 starts
        if(srDoc.Step_2_0_Token_Payment_Status__c == 'Token Document Rejected'){
            tokenApproval = true;
        } // 1.2 ends
        srDocs = [SELECT Name, Service_Request__r.Name, 
                  Document_Type__c, Status__c, Document_Name__c,
                  Sys_Doc_ID__c , Is_Required__c
                  FROM Unit_Documents__c 
                  WHERE Service_Request__c =:recordId AND Status__c = 'Pending Upload' AND Document_name__c != 'Reservation Copy'];
        
        completedDocs = [ SELECT Document_Name__c, Document_Description__c, 
                         Is_Required__c, Document_Type__c, Preview_Download_Final__c,
                         Sys_Doc_ID__c, CreatedBy.Name
                         FROM Unit_Documents__c 
                         WHERE Service_Request__c =:recordId AND Status__c = 'Uploaded'];
        
        uploadedDocsCount = completedDocs.size ();
        pendingDocumentsCount = srDocs.size ();
    }

    /*********************************************************************************************
    * @Description : Method to Update the File ID in SR Document.                                                           *
    * @Params      : Content Version Id, SR Document Id                                                                        *
    * @Return      : SR Document Id                                                                        *
    *********************************************************************************************/
    @RemoteAction
    public Static String uploadFile(String sfFileId, String srDocId )
    {
        
        try {
            ContentVersion version = [SELECT ContentDocumentId FROM ContentVersion WHERE id =: sfFileId ];
            //NSIBPM__SR_Doc__c srDoc = [SELECT NSIBPM__Doc_ID__c FROM NSIBPM__SR_Doc__c WHERE ID =: srDocId AND NSIBPM__Doc_ID__c = NULL LIMIT 1];
            Unit_Documents__c srDoc = [SELECT Sys_Doc_ID__c FROM Unit_Documents__c WHERE ID =: srDocId AND Sys_Doc_ID__c = NULL LIMIT 1];
            srDoc.Sys_Doc_ID__c = sfFileId;
            srDoc.Status__c = 'Uploaded';
            update srDoc;
            try {
                
                ContentDocumentLink cd = New ContentDocumentLink(
                    LinkedEntityId = srDoc.id, 
                    ContentDocumentId = version.ContentDocumentId, 
                    shareType = 'V');
                insert cd;
                
            } catch (Exception e) {
                system.debug('e---------------------'+e.getMessage());
            }
            return srDoc.id;
        }
        catch (Exception e) {
            return '';
        }        
    }
    
    @remoteAction 
    public static String updateSR (ID srDocId) {
        try {
            // 1.1 starts 
            if(midOfficeApproval== null || Test.isRunningTest()) {
                midOfficeApproval = false;
                NSIBPM__Service_Request__c srDoc2 = [ SELECT Name,Step_2_1_Mid_Office_Approval_Status__c 
                                                      FROM NSIBPM__Service_Request__c WHERE ID =: srDocId ];
                if(srDoc2.Step_2_1_Mid_Office_Approval_Status__c == 'Mid Office Rejected'){
                    midOfficeApproval = true;
                }
            }
            // 1.1 ends 1.2 starts
            if(tokenApproval== null || Test.isRunningTest()) {
                tokenApproval = false;
                NSIBPM__Service_Request__c srDoc3 = [ SELECT Name,Step_2_0_Token_Payment_Status__c 
                                                      FROM NSIBPM__Service_Request__c WHERE ID =: srDocId ];
                if(srDoc3.Step_2_0_Token_Payment_Status__c == 'Token Document Rejected'){
                    tokenApproval = true;
                }
            }
            //1.2 starts
            system.debug('tokenApproval: ' + tokenApproval);
            system.debug('midOfficeApproval: ' + midOfficeApproval);
            NSIBPM__Service_Request__c srRecord= new NSIBPM__Service_Request__c ();
            srRecord = [SELECT IS_uk_deal__c, form_request__c FROM NSIBPM__Service_Request__c 
                    WHERE Id =: srDocId];
                    
            if(midOfficeApproval){ // 1.1
                Boolean pendingDocs = false;
                if (srRecord.is_Uk_Deal__c == true) {
                    for (Unit_Documents__c doc : [SELECT ID FROM Unit_documents__c WHERE Status__c = 'Pending Upload' 
                                                   AND Document_name__c = 'Reservation Copy' AND Service_request__c =: srRecord.Id]) {
                        pendingDocs = true;
                    }
                }
                if (pendingDocs == false) {
                    New_Step__c step = [SELECT Id, Name 
                                        FROM New_Step__c 
                                        WHERE Step_Type__c = 'Revised URRF Upload'
                                            AND Is_Closed__c = FALSE
                                            AND Step_Status__c = 'Awaiting Revised URRF'
                                            AND Service_Request__c=: srDocId];
                    system.debug('step: ' + step);
                    step.Step_Status__c = 'Revised URRF Uploaded';
                    step.Is_Closed__c = true;   
                    Update step;
                    return 'Success'; 
                } else {
                   
                    return Damac_RegenerateURRF.generateURRF (srRecord.form_Request__c, srRecord.id);
                }
            } else if(tokenApproval){ // 1.2
                New_Step__c step = [SELECT Id, Name 
                                    FROM New_Step__c 
                                    WHERE Step_Type__c = 'Revised Token Document Upload'
                                        AND Is_Closed__c = FALSE
                                        AND Step_Status__c = 'Awaiting Token Document Upload'
                                        AND Service_Request__c=: srDocId];
                system.debug('step: ' + step);
                step.Step_Status__c = 'Token Document Uploaded';
                step.Is_Closed__c = true;   
                Update step;
                return 'Success'; 
            } else{ // 1.1 ends
                
                
                
                Id srStatusId = [select id,name from NSIBPM__SR_Status__c where Name = 'Awaiting Manager Approval'].id;
                NSIBPM__Service_Request__c req = new NSIBPM__Service_Request__c ();
                req = [SELECT NSIBPM__Internal_SR_Status__c , Internal_Status__c , External_Status__c , Required_Docs_Uploaded__c , IS_uk_deal__c FROM NSIBPM__Service_Request__c 
                        WHERE Id =: srDocId];
                        
                req.id = srDocId;
                
                if (!req.IS_uk_deal__c) {
                    req.NSIBPM__Internal_SR_Status__c = srStatusId;                    
                    req.Internal_Status__c = 'AWAITING_MANAGER_APPROVAL';
                    req.External_Status__c = 'AWAITING_MANAGER_APPROVAL';
                } else {
                    Id srMidOfficeStatusId = [select id,name from NSIBPM__SR_Status__c where Name = 'Awaiting Mid Office Approval'].id;
                    req.NSIBPM__Internal_SR_Status__c = srMidOfficeStatusId ;       
                }
                req.Required_Docs_Uploaded__c = TRUE;
                Update req;
                return 'Success';
            }
        } catch (Exception e) {
            System.debug (e.getMessage () +' - '+e.getLineNumber());
            return 'Error';
        }
        
    }
    @RemoteAction 
    public static String getPassportDetails(String attachmentBody, String fileName, String dealId, String requestType){
        
        String sUrl = system.Label.Tibits_URL+requestType;
        String boundary = '----------------------------741e90d31eff';
        String header = '--'+boundary+'\nContent-Disposition: form-data; name="file"; filename="'+fileName+'";\nContent-Type: application/octet-stream';
        String footer = '--'+boundary+'--';              
        String headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
        String bodyEncoded = '';
        while(headerEncoded.endsWith('=')){
            header+=' ';
            headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
        }
        system.debug (attachmentBody);
        if(attachmentBody.containsIgnoreCase('base64,')){
            bodyEncoded = attachmentBody.subStringAfter('base64,');  
        }else if(attachmentBody.containsIgnoreCase('data:image/png;base64,')){
            bodyEncoded = attachmentBody.subStringAfter('data:image/png;base64,');    
        }else if(attachmentBody.containsIgnoreCase('data:image/jpeg;base64,')){
            bodyEncoded = attachmentBody.subStringAfter('data:image/jpeg;base64,');    
        }else if(attachmentBody.containsIgnoreCase('data:image/jpg;base64,')){
            bodyEncoded = attachmentBody.subStringAfter('data:image/jpg;base64,');    
        }
        system.debug (bodyEncoded);
        Blob bodyBlob = null;
        String last4Bytes = bodyEncoded.substring(bodyEncoded.length()-4, bodyEncoded.length());
        if(last4Bytes.endsWith('==')) {
            last4Bytes = last4Bytes.substring(0,2) + '0K';
            bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);
        }else if(last4Bytes.endsWith('=')) {
            last4Bytes = last4Bytes.substring(0,3) + 'N';
            bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
            footer = '\n' + footer;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);              
        }else {
            footer = '\r\n' + footer;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);  
        }
        System.Debug ('bodyBlob----------------------'+bodyBlob);
        HttpRequest req = new HttpRequest();
        req.setHeader('Content-Type','multipart/form-data; boundary='+boundary);
        req.setMethod('POST');
        req.setEndpoint(sUrl);
        req.setBodyAsBlob(bodyBlob);
        req.setTimeout(120000);
        Http http = new Http();
        HTTPResponse res = new HTTPResponse ();
        
        if(!Test.isrunningtest()) {
            //res = http.send(req);
            System.debug (res.getStatusCode ());
            System.debug (res.getBody ());
        }
        else{
            res = new HttpResponse();
            res.setStatusCode(200);
            res.setBody(json.serialize(new wrapperTbitsResponse()));
        }
        
        wrapperTbitsResponse obj = new wrapperTbitsResponse();
        String result;
        if(res != null && res.getStatusCode() != null && res.getStatusCode() == 200) { 
            obj = (wrapperTbitsResponse)Json.deserialize(res.getBody(), wrapperTbitsResponse.class);
            obj.errorCode = res.getStatusCode();
            obj.errorDescription = new cls_errorDescription(res.getStatus());
            system.debug('#### response after deserializing = '+obj); 
            if (Test.isRunningTest ()) {
                bodyEncoded = 'test';
                cls_fields fields = new cls_fields ();
                fields.valid = true;
                fields.text = 'test';
                fields.name = 'PassportNumber.jpg';
                cls_fields[] fieldsVal = new cls_fields[1];
                fieldsVal[0] = fields;
                UploadSRDocsController.wrapperTbitsResponse objVal = new UploadSRDocsController.wrapperTbitsResponse ();
                objVal.fields = fieldsVal;
                obj.fields = fieldsVal;
            }
            if (obj != null && obj.fields != null 
                && obj.fields.size() > 0 
                && String.isNotBlank(dealId) && String.isNotBlank(bodyEncoded)){ 
                    for(cls_fields thisResponse : obj.fields){
                        if(thisResponse.name != null && 
                           (thisResponse.name.equalsIgnoreCase('PassportNumber') || 
                            thisResponse.name.equalsIgnoreCase('NationalId'))){
                                String extension = filename.contains('.') ? filename.subStringAfter('.') : '.jpg';
                                String documentType;
                                if(String.isNotBlank(requestType) && requestType.containsIgnoreCase('Passport')){
                                    documentType = 'Passport';  
                                }else if(String.isNotBlank(requestType) && requestType.containsIgnoreCase('Id')){
                                    documentType = 'National Id';  
                                }
                                if(String.isNotBlank(thisResponse.text) && String.isNotBlank(documentType)){
                                    result = 'success';
                                    break;
                                }
                            }  
                    }
                }
        } else {
            obj.errorCode = res.getStatusCode();
            obj.errorDescription = new cls_errorDescription(res.getStatus()); 
            //result = 'fail';
            result = 'success';
        }
        return result;
    }
    
    public class cls_errorDescription { 
        public String message;
        
        public cls_errorDescription(String message){
            this.message = message;    
        }
    }
    
    public class cls_fields {
        public boolean valid;
        public String text; 
        public String name; 
    }
    public class cls_ReferenceID { }
    public class wrapperTbitsResponse{
        public String result; 
        public Integer errorCode; 
        public cls_errorDescription errorDescription;
        public cls_ReferenceID ReferenceID;
        public cls_fields[] fields;
        
        public wrapperTbitsResponse(){
            result = '';
            errorCode = null;
            errorDescription = new cls_errorDescription('');
            fields= new list<cls_fields>();
            ReferenceID = new cls_ReferenceID();
        }
    }
}