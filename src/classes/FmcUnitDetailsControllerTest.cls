@isTest
private class FmcUnitDetailsControllerTest {

    @isTest
    static void testController() {
        User portalUser = CommunityTestDataFactory.createPortalUser();
        Id portalAccountId = [SELECT Id, Contact.AccountId FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;

        System.debug('portalAccountId = ' + portalAccountId);

        Location__c location = new Location__c(
            Name = 'Test Location',
            Location_ID__c = 'LOC',
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0
        );
        insert location;

        insert new List<SR_Attachments__c> {
            new SR_Attachments__c(
                Building__c = location.Id,
                Document_Type__c = 'Form'
            )
        };

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(portalAccountId, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Unit_Name__c = 'Unit Name';
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            unit.Inventory__c = inventory.Id;
        }
        insert lstBookingUnit;

        insert TestDataFactory_CRM.createActiveFT_CS();

        CustomerCommunityUtils.customerAccountId = portalAccountId;


        PageReference currentPage = Page.CommunityPortal;
        currentPage.getParameters().put('id', lstBookingUnit[0].Id);
        currentPage.getParameters().put('view', 'UnitDetails');
        Test.setCurrentPage(currentPage);

        Test.startTest();
            FmcUnitDetailsController controller = new FmcUnitDetailsController();

        Test.stopTest();
    }

    @isTest
    static void testControllerFromTenant() {
        Account ownerAccount = new Account(
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId(),
            FirstName='Test FirstName',
            LastName='Test LastName',
            Type='Person',
            Party_ID__C='65432'
        );
        insert ownerAccount;

        User portalUser = CommunityTestDataFactory.createPortalUser();
        Id portalAccountId = [SELECT Id, Contact.AccountId FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;

        System.debug('portalAccountId = ' + portalAccountId);

        Location__c location = new Location__c(
            Name = 'Test Location',
            Location_ID__c = 'LOC',
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0
        );
        insert location;

        insert new List<SR_Attachments__c> {
            new SR_Attachments__c(
                Building__c = location.Id,
                Document_Type__c = 'Form'
            )
        };

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(ownerAccount.Id, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Unit_Name__c = 'Unit Name';
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            unit.Inventory__c = inventory.Id;
        }
        insert lstBookingUnit;

        insert TestDataFactory_CRM.createActiveFT_CS();

        CustomerCommunityUtils.customerAccountId = portalAccountId;


        PageReference currentPage = Page.CommunityPortal;
        currentPage.getParameters().put('id', lstBookingUnit[0].Id);
        currentPage.getParameters().put('view', 'UnitDetails');
        Test.setCurrentPage(currentPage);

        Test.startTest();
            FmcUnitDetailsController controller = new FmcUnitDetailsController();

        Test.stopTest();
    }

    @isTest
    private static void testUnitDetailsFromIpms() {
        PageReference currentPage = Page.CommunityPortal;
        currentPage.getParameters().put('view', 'UnitDetails');
        Test.setCurrentPage(currentPage);

        Test.startTest();
            FmcUnitDetailsController.getUnitDetailsFromIPMS('12345');
        Test.stopTest();
    }

    @isTest
    private static void testUnitSoaNullRegId() {
        PageReference currentPage = Page.CommunityPortal;
        currentPage.getParameters().put('view', 'UnitDetails');
        Test.setCurrentPage(currentPage);

        Test.startTest();
            System.assertEquals(NULL, FmcUnitDetailsController.generateUnitSoa(NULL));
        Test.stopTest();
    }

    @isTest
    private static void testUnitSoaInvalidRegId() {
        FmIpmsRestServices.Response mockResponse = new FmIpmsRestServices.Response();
        mockResponse.complete = true;
        mockResponse.actions = new List<FmIpmsRestServices.Action>{ new FmIpmsRestServices.Action() };
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(
            503, 'error', JSON.serialize(mockResponse), NULL
        ));

        PageReference currentPage = Page.CommunityPortal;
        currentPage.getParameters().put('view', 'UnitDetails');
        Test.setCurrentPage(currentPage);

        Test.startTest();
            try {
                FmcUnitDetailsController.generateUnitSoa('INVALID');
            } catch(Exception e) {
                System.assert(e instanceOf LoamsCommunityException);
            }
        Test.stopTest();
    }

    @isTest
    private static void testUnitSoaValidRegId() {
        FmIpmsRestServices.Response mockResponse = new FmIpmsRestServices.Response();
        mockResponse.complete = true;
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = 'https://www.damacproperties.com';
        mockResponse.actions = new List<FmIpmsRestServices.Action>{ action };
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(
            200, 'success', JSON.serialize(mockResponse), NULL
        ));

        PageReference currentPage = Page.CommunityPortal;
        currentPage.getParameters().put('view', 'UnitDetails');
        Test.setCurrentPage(currentPage);

        Test.startTest();
            System.assert(String.isNotBlank(FmcUnitDetailsController.generateUnitSoa('1234567')));
        Test.stopTest();
    }

    @isTest
    private static void testLocationSoaNullProjectCode() {
        PageReference currentPage = Page.CommunityPortal;
        currentPage.getParameters().put('view', 'UnitDetails');
        Test.setCurrentPage(currentPage);

        Test.startTest();
            System.assertEquals(NULL, FmcUnitDetailsController.generateLocationSoa('12345', NULL));
        Test.stopTest();
    }

    @isTest
    private static void testLocationSoaInvalidProjectCode() {
        FmIpmsRestServices.Response mockResponse = new FmIpmsRestServices.Response();
        mockResponse.complete = true;
        mockResponse.actions = new List<FmIpmsRestServices.Action>{ new FmIpmsRestServices.Action() };
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(
            503, 'error', JSON.serialize(mockResponse), NULL
        ));

        PageReference currentPage = Page.CommunityPortal;
        currentPage.getParameters().put('view', 'UnitDetails');
        Test.setCurrentPage(currentPage);

        Test.startTest();
            try {
                FmcUnitDetailsController.generateLocationSoa('12345', 'INVALID');
            } catch(Exception e) {
                System.assert(e instanceOf LoamsCommunityException);
            }
        Test.stopTest();
    }

    @isTest
    private static void testLocationSoaValidProjectCode() {
        FmIpmsRestServices.Response mockResponse = new FmIpmsRestServices.Response();
        mockResponse.complete = true;
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = 'https://www.damacproperties.com';
        mockResponse.actions = new List<FmIpmsRestServices.Action>{ action };
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(
            200, 'success', JSON.serialize(mockResponse), NULL
        ));

        PageReference currentPage = Page.CommunityPortal;
        currentPage.getParameters().put('view', 'UnitDetails');
        Test.setCurrentPage(currentPage);

        Test.startTest();
            FmcUnitDetailsController.generateLocationSoa('12345', 'LOC');
        Test.stopTest();
    }

    @isTest
    private static void testBulkSoaNullPartyId() {
        PageReference currentPage = Page.CommunityPortal;
        currentPage.getParameters().put('view', 'UnitDetails');
        Test.setCurrentPage(currentPage);

        Test.startTest();
            System.assertEquals(NULL, FmcUnitDetailsController.generateBulkSoa(NULL));
        Test.stopTest();
    }

    @isTest
    private static void testBulkSoaInvalidPartyId() {
        FmIpmsRestServices.Response mockResponse = new FmIpmsRestServices.Response();
        mockResponse.complete = true;
        mockResponse.actions = new List<FmIpmsRestServices.Action>{ new FmIpmsRestServices.Action() };
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(
            503, 'error', JSON.serialize(mockResponse), NULL
        ));

        Test.startTest();
            try {
                FmcUnitDetailsController.generateBulkSoa('INVALID');
            } catch(Exception e) {
                System.assert(e instanceOf LoamsCommunityException);
            }
        Test.stopTest();
    }

    @isTest
    private static void testBulkSoaValidPartyId() {
        FmIpmsRestServices.Response mockResponse = new FmIpmsRestServices.Response();
        mockResponse.complete = true;
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = 'https://www.damacproperties.com';
        mockResponse.actions = new List<FmIpmsRestServices.Action>{ action };
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(
            200, 'success', JSON.serialize(mockResponse), NULL
        ));

        Test.startTest();
            FmcUnitDetailsController.generateBulkSoa('12345');
        Test.stopTest();
    }

    @isTest
    private static void testPaymentInvalidParameters() {
        Test.startTest();
            FmcUnitDetailsController.doPay(NULL, 0, 'https://www.damacproperties.com');
        Test.stopTest();
    }

    @isTest
    private static void testPaymentWithoutGatewayDetails() {
        User portalUser = CommunityTestDataFactory.createPortalUser();
        Id portalAccountId = [SELECT Id, Contact.AccountId FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;

        System.debug('portalAccountId = ' + portalAccountId);

        Location__c location = new Location__c(
            Name = 'Test Location',
            Location_ID__c = 'LOC',
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0
        );
        insert location;

        insert new List<SR_Attachments__c> {
            new SR_Attachments__c(
                Building__c = location.Id,
                Document_Type__c = 'Form'
            )
        };

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(portalAccountId, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Unit_Name__c = 'Unit Name';
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            unit.Inventory__c = inventory.Id;
            unit.Online_Payment_Party__c  = 'Third Party';
        }
        insert lstBookingUnit;

        insert TestDataFactory_CRM.createActiveFT_CS();

        insert new NSIBPM__Document_Master__c(
            NSIBPM__Code__c = 'Third party consent'
        );

        CustomerCommunityUtils.customerAccountId = portalAccountId;

        Test.startTest();
            System.runAs(portalUser) {
                FmcUnitDetailsController.doPay(lstBookingUnit[0].Id, 1, 'https://www.damacproperties.com');
            }
        Test.stopTest();
    }

    @isTest
    private static void testPaymentWithValidParametersAndGateway() {
        User portalUser = CommunityTestDataFactory.createPortalUser();
        Id portalAccountId = [SELECT Id, Contact.AccountId FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;

        System.debug('portalAccountId = ' + portalAccountId);

        Location__c location = new Location__c(
            Name = 'Test Location',
            Location_ID__c = 'LOC',
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0
        );
        insert location;

        insert new List<SR_Attachments__c> {
            new SR_Attachments__c(
                Building__c = location.Id,
                Document_Type__c = 'Form'
            )
        };

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(portalAccountId, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Unit_Name__c = 'Unit Name';
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            unit.Inventory__c = inventory.Id;
            unit.Online_Payment_Party__c  = 'Third Party';
        }
        insert lstBookingUnit;

        insert TestDataFactory_CRM.createActiveFT_CS();

        insert new NSIBPM__Document_Master__c(
            NSIBPM__Code__c = 'Third party consent'
        );

        PaymentGateway__c gateway = new PaymentGateway__c(
            Name = LoamsCommunityController.GATEWAY_NAME,
            Url__c = 'https://www.payment-gateway.com',
            MerchantId__c = 'merchant_id',
            AccessCode__c = 'access_code',
            EncryptionKey__c = 'encryption_key'
        );
        insert gateway;

        CustomerCommunityUtils.customerAccountId = portalAccountId;

        Test.startTest();
            System.runAs(portalUser) {
                FmcUnitDetailsController.doPay(lstBookingUnit[0].Id, 1, 'https://www.damacproperties.com');
            }
        Test.stopTest();
    }

}