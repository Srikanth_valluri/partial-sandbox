@isTest
public class UMA_DocGenPageCtrlTest {
    private static testMethod void testDDP(){

        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Lease Handover').getRecordTypeId();
        Case caseObj = new Case(Status='Draft Request', RecordTypeID=caseRecTypeId);
        insert caseObj;

        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c( name='LHO - UMA',Drawloop_Document_Package_Id__c = 'fjafjkaf',Delivery_Option_Id__c ='dasdasd' );
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController( caseObj );
        UMA_DocGenPageCtrl ctrl = new UMA_DocGenPageCtrl( stdCtrl );
        UMA_DocGenPageCtrl.generateUMA();
        Test.stopTest();

    }

    private static testMethod void testBlankDDPId(){

        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Lease Handover').getRecordTypeId();
        Case caseObj = new Case(Status='Draft Request', RecordTypeID=caseRecTypeId);
        insert caseObj;

        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c( name='LHO - UMA',Delivery_Option_Id__c ='dasdasd' );
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController( caseObj );
        UMA_DocGenPageCtrl ctrl = new UMA_DocGenPageCtrl( stdCtrl );
        UMA_DocGenPageCtrl.generateUMA();
        Test.stopTest();

    }

    private static testMethod void testBlankDeliveryId(){

        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Lease Handover').getRecordTypeId();
        Case caseObj = new Case(Status='Draft Request', RecordTypeID=caseRecTypeId);
        insert caseObj;

        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c(name='LHO - UMA', Drawloop_Document_Package_Id__c = 'fjafjkaf');
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController( caseObj );
        UMA_DocGenPageCtrl ctrl = new UMA_DocGenPageCtrl( stdCtrl );
        UMA_DocGenPageCtrl.generateUMA();
        Test.stopTest();

    }

     private static testMethod void testBlankDDDP(){

        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Lease Handover').getRecordTypeId();
        Case caseObj = new Case(Status='Draft Request', RecordTypeID=caseRecTypeId);
        insert caseObj;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController( caseObj );
        UMA_DocGenPageCtrl ctrl = new UMA_DocGenPageCtrl( stdCtrl );
        UMA_DocGenPageCtrl.generateUMA();
        Test.stopTest();

    }
}