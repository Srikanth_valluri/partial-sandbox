/**************************************************************************************************
* Name               : IPMS_PartyId_Creation_Response
* Test Class         : DAMAC_UNIT_BOOKING_TEST
* Description        : JSON Class for Praty Id Creation Response from IPMS
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR          DATE            Comments
* 1.0                         16/06/2019      Created
**************************************************************************************************/
public class IPMS_PartyId_Creation_Response{

    public class ResponseLines {
        public String bookingLocation;
        public String partyId;
        public String extCustomerNumber;
        public String status;
        public String startTime;
        public String endTime;
        public List<LineStatus> lineStatus;
    }

    public String responseId;
    public String responseTime;
    public String status;
    public String responseMessage;
    public Integer elapsedTimeMs;
    public List<ResponseLines> responseLines;
    public Boolean complete;

    public class LineStatus {
        public String status;
        public String message;
        public String entity;
        public String entityId;
    }

    public static IPMS_PartyId_Creation_Response parse(String json) {
        return (IPMS_PartyId_Creation_Response) System.JSON.deserialize(json, IPMS_PartyId_Creation_Response.class);
    }
}