@isTest
private class ResetDateComponentControllerTest {

    @testSetup
    static void createSetupDate() {
        insert TestDataFactory_CRM.createPersonAccount();
    }

    @isTest static void test_getActiveSourceUnits() {
        List<Fund_Transfer_Unit_Status__c> lstRejectedStatus = new List<Fund_Transfer_Unit_Status__c>();
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        
        ResetDateComponentController controller = new ResetDateComponentController();
        controller.accountId = objAcc.Id;
        //controller.strSRType = 'SERVICE_CHARGE_RESET';
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 3);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 3);
        insert lstBookingUnits;
        SOAPCalloutServiceMock.returnToMe = new Map<String, unitDetailsController.getUnitDetailValuesResponse_element>();
        unitDetailsController.getUnitDetailValuesResponse_element response = new unitDetailsController.getUnitDetailValuesResponse_element();
        response.return_x = '["{\"ATTRIBUTE1\":\"78152\",\"ATTRIBUTE2\":\"\",\"ATTRIBUTE3\":\"LE\",\"ATTRIBUTE4\":\"N\",\"ATTRIBUTE5\":\"0\",\"ATTRIBUTE6\":\"\",\"ATTRIBUTE7\":\"\",\"ATTRIBUTE8\":\"253760\",\"ATTRIBUTE9\":\"253760\",\"ATTRIBUTE10\":\"0\",\"ATTRIBUTE11\":\"2525\",\"ATTRIBUTE12\":\"N\",\"ATTRIBUTE13\":\"Y\",\"ATTRIBUTE14\":\"N\",\"ATTRIBUTE15\":\"\",\"ATTRIBUTE16\":\"N\",\"ATTRIBUTE17\":\"N\",\"ATTRIBUTE18\":\"123745600\",\"ATTRIBUTE19\":\"0\",\"ATTRIBUTE20\":\"6.81\",\"ATTRIBUTE21\":\"1\",\"ATTRIBUTE22\":\"Y\",\"ATTRIBUTE23\":\"1523\",\"ATTRIBUTE24\":\"0\",\"ATTRIBUTE25\":\"1523\",\"ATTRIBUTE26\":\"PLATINUM\",\"ATTRIBUTE27\":\"N\",\"ATTRIBUTE28\":\"N\",\"ATTRIBUTE29\":\"OFF-PLAN\",\"ATTRIBUTE30\":\"N\",\"ATTRIBUTE31\":\"N\",\"ATTRIBUTE32\":\"\",\"ATTRIBUTE33\":\"440\",\"ATTRIBUTE34\":\"N\",\"ATTRIBUTE35\":\"51794\",\"ATTRIBUTE36\":\"\",\"ATTRIBUTE37\":\"\",\"ATTRIBUTE38\":\"\",\"ATTRIBUTE39\":\"27-MAR-2016\",\"ATTRIBUTE40\":\"AYKCB/40/4011\",\"ATTRIBUTE41\":\"253760\",\"ATTRIBUTE42\":\"0\",\"ATTRIBUTE43\":\"2525\",\"ATTRIBUTE44\":\"0\",\"ATTRIBUTE45\":\"N\",\"ATTRIBUTE46\":\"N\",\"ATTRIBUTE47\":\"N\",\"ATTRIBUTE48\":\"N\",\"ATTRIBUTE49\":\"Y\",\"ATTRIBUTE50\":\"N\",\"ATTRIBUTE51\":\"Agreement executed by DAMAC\",\"ATTRIBUTE52\":\"00\",\"ATTRIBUTE53\":\"6.81\",\"ATTRIBUTE54\":\"17-AUG-2020\",\"ATTRIBUTE55\":\"31-OCT-2021\",\"ATTRIBUTE56\":\"0\",\"ATTRIBUTE57\":\"0\",\"ATTRIBUTE58\":\"51794\",\"ATTRIBUTE59\":\"20\",\"ATTRIBUTE60\":\".2\",\"ATTRIBUTE61\":\"0\",\"ATTRIBUTE62\":\"0\",\"ATTRIBUTE63\":\"\",\"ATTRIBUTE64\":\"\",\"ATTRIBUTE65\":\"N\",\"ATTRIBUTE66\":\"N\",\"ATTRIBUTE67\":\"N\",\"ATTRIBUTE68\":\"0\",\"ATTRIBUTE69\":\"N\",\"ATTRIBUTE70\":\"\",\"ATTRIBUTE71\":\"\",\"ATTRIBUTE72\":\"1\",\"ATTRIBUTE73\":\"PARK_BAY\",\"ATTRIBUTE74\":\"\",\"ATTRIBUTE75\":\"0\",\"ATTRIBUTE76\":\"0\",\"ATTRIBUTE77\":\"\",\"ATTRIBUTE78\":\"1015040\",\"ATTRIBUTE79\":\"\",\"ATTRIBUTE80\":\"\",\"ATTRIBUTE81\":\"\",\"ATTRIBUTE82\":\"SA \u2013 Execution Pending awaiting Deposit\",\"ATTRIBUTE83\":\"Damac Canal One Property Development LLC\",\"ATTRIBUTE84\":\"N\",\"ATTRIBUTE85\":\"1600\",\"ATTRIBUTE86\":\"0\",\"ATTRIBUTE87\":\"20\",\"ATTRIBUTE88\":\"253760\",\"ATTRIBUTE89\":\"1444\",\"ATTRIBUTE90\":\"AYKCB\",\"ATTRIBUTE91\":\"AYKON CITY TOWER - B\",\"ATTRIBUTE92\":\"\",\"ATTRIBUTE93\":\"\",\"ATTRIBUTE94\":\"1074105\",\"ATTRIBUTE95\":\"N...\",\"ATTRIBUTE96\":\"793\",\"ATTRIBUTE97\":\"Normal\",\"ATTRIBUTE98\":\"27-MAR-2016\",\"ATTRIBUTE99\":\"APARTMENT\",\"ATTRIBUTE100\":\"\",\"ATTRIBUTE101\":\"Y\",\"ATTRIBUTE102\":\"1268800\",\"ATTRIBUTE103\":\"Y\",\"ATTRIBUTE104\":\"Y\",\"ATTRIBUTE105\":\"0\",\"ATTRIBUTE106\":\"27-MAR-2016\",\"ATTRIBUTE107\":\"N\",\"ATTRIBUTE108\":\"AYKON CITY\",\"ATTRIBUTE109\":\"DUBAI\",\"ATTRIBUTE110\":\"One Bedroom\",\"ATTRIBUTE111\":\"HOTEL APARTMENTS\",\"ATTRIBUTE112\":\"\",\"ATTRIBUTE113\":\"\",\"ATTRIBUTE114\":\"\",\"ATTRIBUTE115\":\"\",\"ATTRIBUTE116\":\"\",\"ATTRIBUTE117\":\"\",\"ATTRIBUTE118\":\"\",\"ATTRIBUTE119\":\"\",\"ATTRIBUTE120\":\"\"}"]';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(3) );
        PageReference pageRef = Page.ResetDateProcesspage;
        Test.setCurrentPage(pageRef);
        
        //ApexPages.currentPage().getParameters().put('AccountId', objAcc.Id);
        controller.lstSelectedUnits.add(lstBookingUnits[0].Id);
        controller.strCRFAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strCRFAttachmentName = 'C:/fakepath/Document_2.txt';
        
        controller.strOtherAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strOtherAttachmentName  = 'C:/fakepath/Document_3.txt';

        controller.strHandoverAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strHandoverAttachmentName  = 'C:/fakepath/Document_4.txt';
        
        controller.strLoamsSoaAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strLoamsSoaAttachmentName  = 'C:/fakepath/Document_4.txt';
        
        controller.strDpSoaAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strDpSoaAttachmentName  = 'C:/fakepath/Document_4.txt';
        
        controller.selectedOtherDocType  = 'Test';

        Test.startTest();
        controller.init();
        controller.getBUs();
        controller.clearInfo();
        controller.getUnitDetails();
        controller.saveAsDraft();
        controller.submitSRReset();
        Test.stopTest();
    }
 
    @isTest static void test_GoToInit1() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        
        Case objCase = new Case();
        //objCase.status = 'Draft Request';
        objCase.status  = 'Submitted';
        insert objCase;
        
        
        //controller.strSRType = 'SERVICE_CHARGE_RESET';
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 3);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 3);
        insert lstBookingUnits;
        
        SR_Booking_unit__C objSr = new SR_Booking_unit__C();
        objSr.Case__c = objCase.id;
        //objSr.Reset_Start_Date__c = system.today();
        objSr.Reset_End_Date__c = system.today().addDays(6);
        objSr.Service_Charge_Reset_Comments__c = 'Test';
        objSr.Service_Charge_Reset_Reason__c = 'Gesture of goodwill.';
        objSr.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objSr;
        
        
        ResetDateComponentController controller = new ResetDateComponentController();
        controller.accountId = objAcc.Id;
        controller.caseId = objCase.Id;
        controller.strCRFAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strCRFAttachmentName = 'C:/fakepath/Document_2.txt';
        
        //controller.strOtherAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        //controller.strOtherAttachmentName  = 'C:/fakepath/Document_3.txt';

        controller.strHandoverAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strHandoverAttachmentName  = 'C:/fakepath/Document_4.txt';
        
        //controller.strLoamsSoaAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        //controller.strLoamsSoaAttachmentName  = 'C:/fakepath/Document_4.txt';
        
        //controller.strDpSoaAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        //controller.strDpSoaAttachmentName  = 'C:/fakepath/Document_5.txt';
        
        controller.selectedOtherDocType  = 'Test';

        Test.startTest();
        controller.init();
        controller.saveAsDraft();
        controller.removeHandoverAttachment();
        controller.removeLoamsSoaAttachment();
        controller.removeDpSoaAttachment();
        controller.submitSRReset();
        Test.stopTest();
    }

  	@isTest static void test_GoToInit2() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        
        Case objCase = new Case();
        objCase.status = 'Draft Request';
        //objCase.status  = 'Submitted';
        insert objCase;
        
        
        //controller.strSRType = 'SERVICE_CHARGE_RESET';
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 3);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 3);
        insert lstBookingUnits;
        
        SR_Booking_unit__C objSr = new SR_Booking_unit__C();
        objSr.Case__c = objCase.id;
        objSr.Reset_Start_Date__c = system.today();
        objSr.Reset_End_Date__c = system.today().addDays(6);
        objSr.Service_Charge_Reset_Comments__c = 'Test';
        objSr.Service_Charge_Reset_Reason__c = 'Gesture of goodwill.';
        objSr.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objSr;
        
        ResetDateComponentController controller = new ResetDateComponentController();
        controller.accountId = objAcc.Id;
        controller.caseId = objCase.Id;
        controller.strCRFAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strCRFAttachmentName = 'C:/fakepath/Document_2.txt';
        
        //controller.strOtherAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        //controller.strOtherAttachmentName  = 'C:/fakepath/Document_3.txt';

        //controller.strHandoverAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        //controller.strHandoverAttachmentName  = 'C:/fakepath/Document_4.txt';
        
        controller.strLoamsSoaAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strLoamsSoaAttachmentName  = 'C:/fakepath/Document_4.txt';
        
        controller.strDpSoaAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strDpSoaAttachmentName  = 'C:/fakepath/Document_5.txt';
        
        controller.selectedOtherDocType  = 'Test';

        Test.startTest();
        controller.init();
        controller.saveAsDraft();
        controller.submitSRReset();
        Test.stopTest();
    }
    
    @isTest static void test_GoToInit3() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        
        Case objCase = new Case();
        //objCase.status = 'Draft Request';
        objCase.status  = 'Submitted';
        insert objCase;
        
        
        //controller.strSRType = 'SERVICE_CHARGE_RESET';
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 3);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 3);
        insert lstBookingUnits;
        
        SR_Booking_unit__C objSr = new SR_Booking_unit__C();
        objSr.Case__c = objCase.id;
        objSr.Reset_Start_Date__c = system.today();
        objSr.Reset_End_Date__c = system.today().addDays(6);
        objSr.Service_Charge_Reset_Comments__c = 'Test';
        objSr.Service_Charge_Reset_Reason__c = 'Gesture of goodwill.';
        objSr.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objSr;
        
        ResetDateComponentController controller = new ResetDateComponentController();
        controller.accountId = objAcc.Id;
        controller.caseId = objCase.Id;
        controller.strCRFAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strCRFAttachmentName = 'C:/fakepath/Document_2.txt';
        
        controller.strOtherAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strOtherAttachmentName  = 'C:/fakepath/Document_3.txt';

        controller.strHandoverAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strHandoverAttachmentName  = 'C:/fakepath/Document_4.txt';
        
        //controller.strLoamsSoaAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        //controller.strLoamsSoaAttachmentName  = 'C:/fakepath/Document_4.txt';
        
        //controller.strDpSoaAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        //controller.strDpSoaAttachmentName  = 'C:/fakepath/Document_5.txt';
        
        controller.selectedOtherDocType  = 'Test';

        Test.startTest();
            controller.init();
            controller.saveAsDraft();
            controller.submitSRReset();
        Test.stopTest();
    }


    /*@isTest static void test_getSelectedUnitDetailsFromIPMSTarget() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        SOAPCalloutServiceMock.returnToMe = new Map<String, unitDetailsController.getUnitDetailValuesResponse_element>();
        unitDetailsController.getUnitDetailValuesResponse_element response = new unitDetailsController.getUnitDetailValuesResponse_element();
        response.return_x = '["{\"ATTRIBUTE1\":\"78152\",\"ATTRIBUTE2\":\"\",\"ATTRIBUTE3\":\"LE\",\"ATTRIBUTE4\":\"N\",\"ATTRIBUTE5\":\"0\",\"ATTRIBUTE6\":\"\",\"ATTRIBUTE7\":\"\",\"ATTRIBUTE8\":\"253760\",\"ATTRIBUTE9\":\"253760\",\"ATTRIBUTE10\":\"0\",\"ATTRIBUTE11\":\"2525\",\"ATTRIBUTE12\":\"N\",\"ATTRIBUTE13\":\"Y\",\"ATTRIBUTE14\":\"N\",\"ATTRIBUTE15\":\"\",\"ATTRIBUTE16\":\"N\",\"ATTRIBUTE17\":\"N\",\"ATTRIBUTE18\":\"123745600\",\"ATTRIBUTE19\":\"0\",\"ATTRIBUTE20\":\"6.81\",\"ATTRIBUTE21\":\"1\",\"ATTRIBUTE22\":\"Y\",\"ATTRIBUTE23\":\"1523\",\"ATTRIBUTE24\":\"0\",\"ATTRIBUTE25\":\"1523\",\"ATTRIBUTE26\":\"PLATINUM\",\"ATTRIBUTE27\":\"N\",\"ATTRIBUTE28\":\"N\",\"ATTRIBUTE29\":\"OFF-PLAN\",\"ATTRIBUTE30\":\"N\",\"ATTRIBUTE31\":\"N\",\"ATTRIBUTE32\":\"\",\"ATTRIBUTE33\":\"440\",\"ATTRIBUTE34\":\"N\",\"ATTRIBUTE35\":\"51794\",\"ATTRIBUTE36\":\"\",\"ATTRIBUTE37\":\"\",\"ATTRIBUTE38\":\"\",\"ATTRIBUTE39\":\"27-MAR-2016\",\"ATTRIBUTE40\":\"AYKCB/40/4011\",\"ATTRIBUTE41\":\"253760\",\"ATTRIBUTE42\":\"0\",\"ATTRIBUTE43\":\"2525\",\"ATTRIBUTE44\":\"0\",\"ATTRIBUTE45\":\"N\",\"ATTRIBUTE46\":\"N\",\"ATTRIBUTE47\":\"N\",\"ATTRIBUTE48\":\"N\",\"ATTRIBUTE49\":\"Y\",\"ATTRIBUTE50\":\"N\",\"ATTRIBUTE51\":\"Agreement executed by DAMAC\",\"ATTRIBUTE52\":\"00\",\"ATTRIBUTE53\":\"6.81\",\"ATTRIBUTE54\":\"17-AUG-2020\",\"ATTRIBUTE55\":\"31-OCT-2021\",\"ATTRIBUTE56\":\"0\",\"ATTRIBUTE57\":\"0\",\"ATTRIBUTE58\":\"51794\",\"ATTRIBUTE59\":\"20\",\"ATTRIBUTE60\":\".2\",\"ATTRIBUTE61\":\"0\",\"ATTRIBUTE62\":\"0\",\"ATTRIBUTE63\":\"\",\"ATTRIBUTE64\":\"\",\"ATTRIBUTE65\":\"N\",\"ATTRIBUTE66\":\"N\",\"ATTRIBUTE67\":\"N\",\"ATTRIBUTE68\":\"0\",\"ATTRIBUTE69\":\"N\",\"ATTRIBUTE70\":\"\",\"ATTRIBUTE71\":\"\",\"ATTRIBUTE72\":\"1\",\"ATTRIBUTE73\":\"PARK_BAY\",\"ATTRIBUTE74\":\"\",\"ATTRIBUTE75\":\"0\",\"ATTRIBUTE76\":\"0\",\"ATTRIBUTE77\":\"\",\"ATTRIBUTE78\":\"1015040\",\"ATTRIBUTE79\":\"\",\"ATTRIBUTE80\":\"\",\"ATTRIBUTE81\":\"\",\"ATTRIBUTE82\":\"SA \u2013 Execution Pending awaiting Deposit\",\"ATTRIBUTE83\":\"Damac Canal One Property Development LLC\",\"ATTRIBUTE84\":\"N\",\"ATTRIBUTE85\":\"1600\",\"ATTRIBUTE86\":\"0\",\"ATTRIBUTE87\":\"20\",\"ATTRIBUTE88\":\"253760\",\"ATTRIBUTE89\":\"1444\",\"ATTRIBUTE90\":\"AYKCB\",\"ATTRIBUTE91\":\"AYKON CITY TOWER - B\",\"ATTRIBUTE92\":\"\",\"ATTRIBUTE93\":\"\",\"ATTRIBUTE94\":\"1074105\",\"ATTRIBUTE95\":\"N...\",\"ATTRIBUTE96\":\"793\",\"ATTRIBUTE97\":\"Normal\",\"ATTRIBUTE98\":\"27-MAR-2016\",\"ATTRIBUTE99\":\"APARTMENT\",\"ATTRIBUTE100\":\"\",\"ATTRIBUTE101\":\"Y\",\"ATTRIBUTE102\":\"1268800\",\"ATTRIBUTE103\":\"Y\",\"ATTRIBUTE104\":\"Y\",\"ATTRIBUTE105\":\"0\",\"ATTRIBUTE106\":\"27-MAR-2016\",\"ATTRIBUTE107\":\"N\",\"ATTRIBUTE108\":\"AYKON CITY\",\"ATTRIBUTE109\":\"DUBAI\",\"ATTRIBUTE110\":\"One Bedroom\",\"ATTRIBUTE111\":\"HOTEL APARTMENTS\",\"ATTRIBUTE112\":\"\",\"ATTRIBUTE113\":\"\",\"ATTRIBUTE114\":\"\",\"ATTRIBUTE115\":\"\",\"ATTRIBUTE116\":\"\",\"ATTRIBUTE117\":\"\",\"ATTRIBUTE118\":\"\",\"ATTRIBUTE119\":\"\",\"ATTRIBUTE120\":\"\"}"]';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        
        ResetDateComponentController controller = new ResetDateComponentController();
        controller.accountId = objAcc.Id;
        //controller.strSRType = 'SERVICE_CHARGE_RESET';
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 3);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 3);
        insert lstBookingUnits;
        
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Charge Reset').getRecordTypeId();
        // List<Case> lstOpenCasesBU = new List<Case>();
        insert new Case(Status='Open', Booking_Unit__c = lstBookingUnits[1].Id, RecordTypeID=caseRecordTypeId);
        insert new Case(Status='Closed', Booking_Unit__c = lstBookingUnits[0].Id, RecordTypeID=caseRecordTypeId);
        insert new Case(Status='Closed', Booking_Unit__c = lstBookingUnits[1].Id, RecordTypeID=caseRecordTypeId);
        Booking_Unit__c obj = [select Id, Booking__c, Unit_Name__c, Booking__r.Account__r.Name from Booking_Unit__c where id=: lstBookingUnits[0].Id];
        controller.lstSelectedUnits.add(lstBookingUnits[0].Id);
        

        list<Buyer__c> lstBuyer = new list<Buyer__c>();
        lstBuyer = TestDataFactory_CRM.createBuyer(obj.Booking__c,3, objAcc.Id);
        insert lstBuyer;

        controller.strCRFAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strCRFAttachmentName = 'C:/fakepath/Document_2.txt';
        
        controller.strOtherAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strOtherAttachmentName  = 'C:/fakepath/Document_3.txt';

        controller.strHandoverAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strHandoverAttachmentName  = 'C:/fakepath/Document_4.txt';
        
        controller.strLoamsSoaAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strLoamsSoaAttachmentName  = 'C:/fakepath/Document_4.txt';
        
        controller.selectedOtherDocType  = 'Test';

        Test.startTest();
        controller.getBUs();
        controller.getUnitDetails();
        Test.stopTest();
    }*/
    
    @isTest static void test_SaveFunctionality() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        

        ResetDateComponentController controller = new ResetDateComponentController();
        controller.accountId = objAcc.Id;
        //controller.strSRType = 'SERVICE_CHARGE_RESET';
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 3);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 3);
        insert lstBookingUnits;
        controller.lstSelectedUnits.add(lstBookingUnits[0].Id);

        controller.strCRFAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strCRFAttachmentName = 'C:/fakepath/Document_2.txt';
        
        controller.strOtherAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strOtherAttachmentName  = 'C:/fakepath/Document_3.txt';

        controller.strHandoverAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strHandoverAttachmentName  = 'C:/fakepath/Document_4.txt';
        
        controller.strLoamsSoaAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strLoamsSoaAttachmentName  = 'C:/fakepath/Document_4.txt';
        
        controller.selectedOtherDocType  = 'Test';
        
        Test.startTest();
        
        // Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );
        controller.getBUs();
        controller.getUnitDetails();
        controller.saveAsDraft();
        Test.stopTest();
    }
    
    @isTest static void test_SubmitFunctionality() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        
        
        // Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        
        ResetDateComponentController controller = new ResetDateComponentController();
        controller.accountId = objAcc.Id;
        //controller.strSRType = 'SERVICE_CHARGE_RESET';
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 3);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 3);
        insert lstBookingUnits;
        
        
        controller.lstSelectedUnits.add(lstBookingUnits[0].Id);
        
        controller.strCRFAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strCRFAttachmentName = 'C:/fakepath/Document_2.txt';
        
        controller.strOtherAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strOtherAttachmentName  = 'C:/fakepath/Document_3.txt';

        controller.strHandoverAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strHandoverAttachmentName  = 'C:/fakepath/Document_4.txt';
        
        controller.strLoamsSoaAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strLoamsSoaAttachmentName  = 'C:/fakepath/Document_4.txt';
        
        controller.selectedOtherDocType  = 'Test';
        Test.startTest();
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );
        controller.getBUs();
        controller.getUnitDetails();
        controller.submitSRReset();
        Test.stopTest();

    }
    
    @isTest static void test_removeAttachment() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        
        ResetDateComponentController controller = new ResetDateComponentController();
        controller.accountId = objAcc.Id;
        //controller.strSRType = 'SERVICE_CHARGE_RESET';
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 3);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 3);
        insert lstBookingUnits;
        
        
        
        controller.lstSelectedUnits.add(lstBookingUnits[0].Id);
        

        controller.strCRFAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strCRFAttachmentName = 'C:/fakepath/Document_2.txt';
        
        controller.strOtherAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strOtherAttachmentName  = 'C:/fakepath/Document_3.txt';

        controller.strHandoverAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strHandoverAttachmentName  = 'C:/fakepath/Document_4.txt';
        
        controller.strLoamsSoaAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strLoamsSoaAttachmentName  = 'C:/fakepath/Document_4.txt';
        
        controller.selectedOtherDocType  = 'Test';

        SR_Attachments__c objAtt1 = new SR_Attachments__c();
        objAtt1.Type__c = 'Other';
        objAtt1.Case__c = controller.caseObj.Id;
        insert objAtt1;
        
        SR_Attachments__c objAtt2 = new SR_Attachments__c();
        objAtt2.Type__c = 'Customer Request Form';
        objAtt2.Case__c = controller.caseObj.Id;
        insert objAtt2;

        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
        Test.startTest();
        controller.getBUs();
        controller.getTargetUnits();
        controller.removeOtherAttachment();
        controller.removeCrfAttachment();
        Test.stopTest();
    }
    
    @isTest static void test_SubmitDpSoaFunctionality() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        
        
        // Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        
        ResetDateComponentController controller = new ResetDateComponentController();
        controller.accountId = objAcc.Id;
        //controller.strSRType = 'SERVICE_CHARGE_RESET';
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 3);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 3);
        insert lstBookingUnits;
        
        
        controller.lstSelectedUnits.add(lstBookingUnits[0].Id);
        
        controller.strCRFAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strCRFAttachmentName = 'C:/fakepath/Document_2.txt';
        
        controller.strOtherAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strOtherAttachmentName  = 'C:/fakepath/Document_3.txt';

        controller.strHandoverAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strHandoverAttachmentName  = 'C:/fakepath/Document_4.txt';
        
        controller.strLoamsSoaAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strLoamsSoaAttachmentName  = 'C:/fakepath/Document_4.txt';
        
        controller.strDpSoaAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strDpSoaAttachmentName  = 'C:/fakepath/Document_4.txt';
        
        controller.selectedOtherDocType  = 'Test';
        Test.startTest();
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );
        //controller.init();
        controller.getBUs();
        controller.getUnitDetails();
        controller.saveAsDraft();
        controller.submitSRReset();
        Test.stopTest();

    }
    
    @isTest static void test_GoToInit4() {
        
        Account objAcc = new Account();
        objAcc.Name = 'Test';
        Insert objAcc;
        
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        
        //Account objAcc = [SELECT Id from Account LIMIT 1];
        
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        
        /*Case objCase = new Case();
        objCase.status = 'Draft Request';
        //objCase.status  = 'In Progress';
        insert objCase;*/
        
        
        //controller.strSRType = 'SERVICE_CHARGE_RESET';
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 3);
        insert lstBookings;
        
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 3);
        insert lstBookingUnits;
        
        SR_Booking_unit__C objSr = new SR_Booking_unit__C();
        //objSr.Case__c = objCase.id;
        objSr.Reset_Start_Date__c = system.today();
        objSr.Reset_End_Date__c = system.today().addDays(6);
        objSr.Service_Charge_Reset_Comments__c = 'Test';
        objSr.Service_Charge_Reset_Reason__c = 'Gesture of goodwill.';
        objSr.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objSr;
        
        ResetDateComponentController controller = new ResetDateComponentController();
        controller.accountId = objAcc.Id;
        //controller.caseId = objCase.Id;
        controller.strCRFAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strCRFAttachmentName = 'C:/fakepath/Document_2.txt';
       
        
        controller.strLoamsSoaAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strLoamsSoaAttachmentName  = 'C:/fakepath/Document_4.txt';
        
        controller.strDpSoaAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strDpSoaAttachmentName  = 'C:/fakepath/Document_5.txt';
        
        controller.selectedOtherDocType  = 'Test';

        Test.startTest();
        //controller.init();
        //controller.saveAsDraft();
        //controller.submitSRReset();
        controller.getTargetUnits();
        controller.getUnitDetails() ;
        Test.stopTest();
    }
    
     @isTest static void test_GoToInit5() {
        
        Account objAcc = new Account();
        objAcc.Name = 'Test';
        Insert objAcc;
        
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        
               
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        
        /*Case objCase = new Case();
        objCase.status = 'Draft Request';
        insert objCase;*/
         
         Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Charge Reset').getRecordTypeId();
        System.debug('recTypeCOD :'+ recTypeCOD);
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        //objCase.Booking_Unit__c=lstBookingUnits[0].Id;
        objCase.status = 'Draft Request';
        insert objCase;
        
        
        //controller.strSRType = 'SERVICE_CHARGE_RESET';
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 3);
        insert lstBookings;
        
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 3);
        insert lstBookingUnits;
        
        SR_Booking_unit__C objSr = new SR_Booking_unit__C();
        objSr.Case__c = objCase.id;
        objSr.Reset_Start_Date__c = system.today();
        objSr.Reset_End_Date__c = system.today().addDays(6);
        objSr.Service_Charge_Reset_Comments__c = 'Test';
        objSr.Service_Charge_Reset_Reason__c = 'Gesture of goodwill.';
        objSr.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objSr;
        
        ResetDateComponentController controller = new ResetDateComponentController();
        controller.accountId = objAcc.Id;
        controller.caseId = objCase.Id;
        controller.strCRFAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strCRFAttachmentName = 'C:/fakepath/Document_2.txt';
       
        
        controller.strLoamsSoaAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strLoamsSoaAttachmentName  = 'C:/fakepath/Document_4.txt';
        
        controller.strDpSoaAttachmentBody  = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        controller.strDpSoaAttachmentName  = 'C:/fakepath/Document_5.txt';
        
        controller.selectedOtherDocType  = 'Test';

        Test.startTest();
        //controller.init();
        //controller.saveAsDraft();
        //controller.submitSRReset();
        controller.getTargetUnits();
        controller.getUnitDetails() ;
        controller.checkExistingSRExists();
        Test.stopTest();
    }
    
}