public with sharing class BulkSOAExtensionService {

    public BulkSOAExtensionService() {
    }
    
    public static CustomerDetails getUnitInformation(String strEndpoint) {
        Http objHttp = new Http();
        HttpRequest objRequest = new HttpRequest();
        objRequest.setEndpoint( strEndpoint );
        objRequest.setMethod('GET');
        objRequest.setTimeout(120000);
        try {
            HttpResponse objResponse = objHttp.send( objRequest );
            system.debug('--objResponse--'+objResponse.getBody());
            if( objResponse.getStatusCode() == 200 ) {
                CustomerDetails objDeserialized = (CustomerDetails)JSON.deserialize(objResponse.getBody(), BulkSOAExtensionService.CustomerDetails.class);
                system.debug('== Object =='+objDeserialized);
                return objDeserialized ;
            }
        }
        catch( Exception e ) {
            system.debug('== Exception occured while making a HTTP callout for unit details == '+e.getMessage() );
        }
        return null;
    }
    
    public static TransactionDetails getTransactionDetails( String strLineURL ) {
        Http objHttp = new Http();
        HttpRequest objRequest = new HttpRequest();
        objRequest.setEndpoint( strLineURL );
        objRequest.setMethod('GET');
        objRequest.setTimeout(120000);
        try {
            HttpResponse objResponse = objHttp.send( objRequest );
            system.debug('--objResponse--'+objResponse.getBody());
            if( objResponse.getStatusCode() == 200 ) {
                TransactionDetails objDeserialized = (TransactionDetails)JSON.deserialize(objResponse.getBody(), BulkSOAExtensionService.TransactionDetails.class);
                system.debug('== Object =='+objDeserialized);
                return objDeserialized ;
            }
        }
        catch( Exception e ) {
            system.debug('== Exception occured while making a HTTP callout for unit details == '+e.getMessage() );
        }
        return null;
    }
    
    public static PaymentDetails getPaymentDetails(String strRegId) {
        PaymentDetails objPaymentDetails = new PaymentDetails();
        List<PaymentTerm> lstTerms = new List<PaymentTerm>();
        objPaymentDetails.decTotalPercentage = 0;
        objPaymentDetails.decTotalAmount = 0;
        
        List<Payment_Plan__c> lstPlan = [Select Id, Status__c,
            (Select Id, Installment__c, Milestone_Event__c, Payment_Date__c, Percent_Value__c,
            Payment_Plan__r.Booking_Unit__r.Requested_Price__c 
            From Payment_Terms__r Where Percent_Value__c != null Order By Installment__c) 
            From Payment_Plan__c 
            Where Booking_Unit__c != null AND Booking_Unit__r.Registration_Id__c =: strRegId AND 
            (Status__c = 'Active' OR Status__c = null) AND Booking_Unit__r.Requested_Price__c != null limit 1];
        
        if(lstPlan != null && !lstPlan.isEmpty() && lstPlan[0].Payment_Terms__r != null && !lstPlan[0].Payment_Terms__r.isEmpty()) {
            for(Payment_Terms__c objTerm: lstPlan[0].Payment_Terms__r) {
                system.debug('--objTerm--'+objTerm);
                PaymentTerm objPT = new PaymentTerm();
                objPT.strMilestone = objTerm.Milestone_Event__c;
                objPT.dtEventDate = objTerm.Payment_Date__c;
                objPT.strInstallment = objTerm.Installment__c;
                objPT.strPercent = objTerm.Percent_Value__c;
                Decimal decPercentage = Decimal.valueOf(objTerm.Percent_Value__c);
                objPT.decAmountDue = (objTerm.Payment_Plan__r.Booking_Unit__r.Requested_Price__c * decPercentage)/100;
                lstTerms.add(objPT);
                
                objPaymentDetails.decTotalPercentage += decPercentage;
                objPaymentDetails.decTotalAmount += objPT.decAmountDue; 
            }
        }    
        
        objPaymentDetails.lstTerms = lstTerms;
        return objPaymentDetails;
    }
    
    public class CustomerDetails {
        public ProcessAttributes processAttributes { get; set; }
        public list<UnitDetails> units { get; set; }
        
        public CustomerDetails() {}
    }
    
    public class ProcessAttributes {
        public String responseId;
        public String responseTime ;
        public String responseMessage ;
        public String nextAction ;
        public String recordCount ;
        public String totalPages;
        public String currentPage;
        public String next {get; set;}
        public String previous {get; set;}
        
        public ProcessAttributes() {}
    }
    
    public class UnitDetails {
        public String registrationId { get; set; }
        public String unitNumber { get; set; }
        public String unitStatus { get; set; }
        public String unitPrice { get; set; }
        public String registrationDate { get; set; }
        public String propertyName { get; set; }
        public String debitTotal { get; set; }
        public String creditTotal { get; set; }
        public String balance { get; set; }
        public String vatCharged { get; set; }
        public String vatPaid { get; set; }
        public String installmentDue { get; set; }
        public String curency { get; set; }
        public String lineURL { get; set; }
        public String sellerName { get; set; }
        public BankDetails bankDetail { get; set; }
        public List<PDCDetails> customerpdcdetails { get; set; }
        
        public UnitDetails() {
            registrationId = '';
            unitNumber = '';
            unitStatus = '';
            unitPrice = '';
            registrationDate = '';
            propertyName = '';
            debitTotal = '';
            creditTotal = '';
            balance = '';
            vatCharged = '';
            vatPaid = '';
            installmentDue = '';
            curency = '';
            lineURL = '';
            sellerName = '';
            bankDetail = new BankDetails();
        }
    }
    
    public class BankDetails {
        public String id { get; set; }
        public String beneficiaryName { get; set; }
        public String beneficiaryBank { get; set; }
        public String bankBranch { get; set; }
        public String bankAccountName { get; set; }
        public String beneficiaryNumber { get; set; }
        public String eftSwiftCode { get; set; }
        public String iban { get; set; }
        
        public BankDetails() {
            id = '';
            beneficiaryName = '';
            beneficiaryBank = '';
            bankBranch = '';
            bankAccountName = '';
            beneficiaryNumber = '';
            eftSwiftCode = '';
            iban = '';
        }
    }
    
    public class PDCDetails {
        public String checkNumber { get; set; }
        public String pdcDate { get; set; }
        public String amount { get; set; }
    }
    
    public class TransactionDetails {
        public ProcessAttributes processAttributes { get; set; }
        public list<TransacDetails> custlines { get; set; }
        
        public TransactionDetails() {}
    }
    
    public class TransacDetails {
        public String id { get; set; }
        public String partyId { get; set; }
        public String registrationId { get; set; }
        public String registrationDate { get; set; }
        public String statusFlag { get; set; }
        public String propertyId { get; set; }
        public String propertyName { get; set; }
        public String buildingId { get; set; }
        public String building { get; set; }
        public String unitId { get; set; }
        public String unitNumber { get; set; }
        public String orgId { get; set; }
        public String docType { get; set; }
        public String docDate { get; set; }
        public String dueDate { get; set; }
        public String description { get; set; }
        //public String currency { get; set; }
        public String debit { get; set; }
        public String credit { get; set; }
        public String balance { get; set; }
        public String vatCharged { get; set; }
        public String vatPaid { get; set; }
        
        public TransacDetails() {
             id = '';
             partyId = '';
             registrationId = '';
             registrationDate = '';
             statusFlag = '';
             propertyId = '';
             propertyName = '';
             buildingId = '';
             building = '';
             unitId = '';
             unitNumber = '';
             orgId = '';
             docType = '';
             docDate = '';
             dueDate = '';
             description = '';
            // currency = '';
             debit = '';
             credit = '';
             balance = '';
             vatCharged = '';
             vatPaid = '';
        }
    }
    
    public Class PaymentDetails {
        public Decimal decTotalPercentage {get; set;}
        public Decimal decTotalAmount {get; set;}
        public List<PaymentTerm> lstTerms {get; set;}
    }
    
    public Class PaymentTerm {
        public String strMilestone {get; set;}
        public Date dtEventDate {get; set;}
        public String strInstallment {get; set;}
        public String strPercent {get; set;}
        public Decimal decAmountDue {get; set;}
    }
}