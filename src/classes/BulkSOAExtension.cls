public with sharing class BulkSOAExtension {
    
    public BulkSOAExtensionService.CustomerDetails objResponse { get; set; }
    public String strLineURL { get; set; }
    public BulkSOAExtensionService.UnitDetails objSelectedUnit { get; set; }
    public BulkSOAExtensionService.TransactionDetails objTransactionResponse { get; set; }
    public String ObjectId;
    private Account objAcc;
    private Calling_List__c objCalling;
    public BulkSOAExtension(ApexPages.StandardController controller) {
        System.debug('controller.getRecord()::::'+controller.getRecord().Id);
         ObjectId = String.valueOf(controller.getRecord().Id);
        if(ObjectId.startsWith('001')){
          objAcc = (Account)controller.getRecord();
          system.debug('== Extension Method Called ==');
          system.debug('== Account Record =='+ objAcc);
          system.debug('== Party Id =='+objAcc.Party_Id__c);
        }
        else if(ObjectId.startsWith(Label.Calling_List_Prefix)){
          objCalling =  (Calling_List__c)controller.getRecord();
          system.debug('==objCalling  Extension Method Called ==');
          system.debug('==objCalling objCalling Record =='+ objCalling);
          system.debug('=objCalling= Party Id =='+objCalling.Party_Id__c);
        }
    }

    public void getUnitInformation() {
      String strEndpoint;
        if( objAcc != null && String.isNotBlank( objAcc.Party_Id__c ) ) {
            //String strEndpoint = 'http://94.200.40.200:8080/DCOFFEE/customer/statement/'+'760002';
            strEndpoint = Label.Bulk_SOA_Endpoint + objAcc.Party_Id__c;
            objResponse = BulkSOAExtensionService.getUnitInformation( strEndpoint ) ; //objAcc.Party_Id__c

            //TODO 
            GenericUtility.createSOACreator('Bulk SOA',UserInfo.getUserId(),system.now(),'','Bulk SOA',ObjectId ,NULL,NULL);


        }
        else if(objCalling != null && String.isNotBlank( objCalling.Party_Id__c )){
          strEndpoint = Label.Bulk_SOA_Endpoint + objCalling.Party_Id__c;
            objResponse = BulkSOAExtensionService.getUnitInformation( strEndpoint ) ;

            //TODO 
            GenericUtility.createSOACreator('Bulk SOA',UserInfo.getUserId(),system.now(),'','Bulk SOA',NULL,NULL,ObjectId );

        }
    }
    
    public void getTransactionInformation() {
        if(String.isNotBlank( strLineURL )) {
            objTransactionResponse = BulkSOAExtensionService.getTransactionDetails( strLineURL ) ;
        }
    }
    
    public PageReference getTransactionDetails() {
        system.debug('== Line URL =='+strLineURL);
        if( String.isNotBlank( strLineURL ) ) {
            for( BulkSOAExtensionService.UnitDetails objUnitDetails : objResponse.units ) {
                if( strLineURL.equalsIgnoreCase( objUnitDetails.lineURL ) ) {
                    objSelectedUnit = objUnitDetails ;
                    break ;
                }
            }
            if(ObjectId.startsWith('001')){
              system.debug('== objSelectedUnit =='+objSelectedUnit);
              PageReference pg = Page.BulkSOA ;
              pg.getParameters().put('Id', objAcc.Id );
              return pg ;
            }
            else If(ObjectId.startsWith(Label.Calling_List_Prefix)){
              system.debug('== objSelectedUnit =='+objSelectedUnit);
              PageReference pg = Page.BulkSOAForCallingList ;
              pg.getParameters().put('Id', objCalling.Id );
              return pg ;
            }
        }
        return null;
    }
    
    public void fetchPreviousUnits() {
        system.debug('--objResponse.processAttributes.previous--'+objResponse.processAttributes.previous);
        objResponse = BulkSOAExtensionService.getUnitInformation(objResponse.processAttributes.previous);
    }
    
    public void fetchNextUnits() {
        system.debug('--objResponse.processAttributes.next--'+objResponse.processAttributes.next);
        objResponse = BulkSOAExtensionService.getUnitInformation(objResponse.processAttributes.next);
    }
    
    public void fetchPreviousTransactions() {
        system.debug('--objTransactionResponse.processAttributes.previous--'+objTransactionResponse.processAttributes.previous);
        objTransactionResponse = BulkSOAExtensionService.getTransactionDetails(objTransactionResponse.processAttributes.previous);
    }
    
    public void fetchNextTransactions() {
        system.debug('--objTransactionResponse.processAttributes.next--'+objTransactionResponse.processAttributes.next);
        objTransactionResponse = BulkSOAExtensionService.getTransactionDetails(objTransactionResponse.processAttributes.next);
    }
}