public class TargetTriggerHandler implements TriggerFactoryInterface{
    
    public void executeBeforeInsertTrigger(List<sObject> newRecordsList){
        for(Target__c thisTarget : (List<Target__c>)newRecordsList){
            thisTarget.Uniq__c = String.valueOf(thisTarget.User__c).substring(0,15) + '-' + thisTarget.Month__c + '-' + thisTarget.Year__c;
            thisTarget.Month_Key__c = thisTarget.Month__c + '-' + thisTarget.Year__c;
        }
    }
    public void executeBeforeUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){
        for(Target__c newTarget : (List<Target__c>)newRecordsMap.values()){
            Target__c oldTarget = (Target__c)oldRecordsMap.get(newTarget.Id);
            if(newTarget != null && oldTarget != null && newTarget.User__c != oldTarget.User__c){
                newTarget.Uniq__c = String.valueOf(newTarget.User__c).substring(0,15) + '-' + newTarget.Month__c + '-' + newTarget.Year__c;
            }
        }
    }
    public void executeAfterInsertUpdateTrigger(Map<Id, sObject> MapNewRecords, Map<Id, sObject> MapOldRecords){}
    public void executeAfterUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){
        /*
        set<string> monthKeys = new set<string>();
        for(Target__c thisTarget : (List<Target__c>)newRecordsMap.values()){
            if(thisTarget.DOS_User__c != null)
                monthKeys.add(thisTarget.Month_Key__c);
        }
        
        if(!monthKeys.isEmpty())
            rollUpTargets(monthKeys);
        */
    }
    public void executeBeforeInsertUpdateTrigger(List<sObject> newRecordsList, Map<Id,sObject> oldRecordsMap){}
    public void executeBeforeDeleteTrigger(Map<Id,sObject> oldRecordsMap){}
    public void executeAfterDeleteTrigger(Map<Id,sObject> oldRecordsMap){}
    
    public void executeAfterInsertTrigger(Map<Id,sObject> newRecordsMap){
        Set<integer> months = new Set<integer>();
        Set<integer> years = new Set<integer>();
        Map<Id, sObject> dealTeamRecordsMap = new Map<Id, sObject>();
        List<Deal_Team__c> dealTeamList = new List<Deal_Team__c>();
        Map<String, Integer> monthMap = new Map<String, Integer>{'January'=>1, 'February'=>2, 'March'=>3, 'April'=>4, 'May'=>5, 'June'=>6, 
            'July'=>7, 'August'=>8, 'September'=>9, 'October'=>10, 'November'=>11, 'December'=>12};
                
        try{
            set<string> monthKeys = new set<string>();
            for(Target__c thisTarget : (List<Target__c>)newRecordsMap.values()){
                months.add(monthMap.get(thisTarget.Month__c));
                years.add(Integer.valueOf(thisTarget.Year__c));
                if(thisTarget.DOS_User__c != null)
                    monthKeys.add(thisTarget.Month_Key__c);
            }
            
            //if(!monthKeys.isEmpty())
            //    rollUpTargets(monthKeys);
            
            if(!months.isEmpty() && !years.isEmpty()){
                for(Deal_Team__c thisDealTeam : [SELECT Id, Name, Associated_Deal__c, Deal_Registration_Date__c FROM Deal_Team__c 
                                                 WHERE CALENDAR_MONTH(Deal_Registration_Date__c) IN : months
                                                 AND CALENDAR_YEAR(Deal_Registration_Date__c) IN : years
                                                 AND (PC_Target__c = null OR DOS_Target__c = null OR HOS_Target__c = null OR HOD_Target__c = null)])
                {
                    dealTeamRecordsMap.put(thisDealTeam.Id, thisDealTeam);
                }    
                
                if(!dealTeamRecordsMap.isEmpty()){
                    DealTeamTrgHandler dtHandler = new DealTeamTrgHandler();
                    dealTeamList = dtHandler.addTarget(dealTeamRecordsMap, new Map<id, sObject>());            
                    if(!dealTeamList.isEmpty()){
                        TriggerFactoryCls.setBYPASS_UPDATE_TRIGGER();
                        update dealTeamList;
                        TriggerFactoryCls.resetBYPASS_UPDATE_TRIGGER();
                    }
                }
            }
            
        }
        catch(Exception ex){
            system.debug('#### Exception at line number = '+ex.getLineNumber()+' , Exception message = '+ex.getMessage());
        }
        
    } 
    /*
    public void rollUpTargets(set<string> keys){
        list<AggregateResult> lstDOSResult = [Select sum(Target__c) tot, DOS_User__c dos, Month_key__c key
                                                from Target__c 
                                                where RecordType.Name = 'Staff' and month_Key__c in: keys
                                                and DOS_User__c != null
                                                group by DOS_User__c, Month_key__c];
        Map<string, decimal> mpTotals = new map<string, decimal>();
        for(AggregateResult ar : lstDOSResult){
            if(string.valueOf(ar.get('dos')) != null){
                string userkey = string.valueOf(ar.get('dos')).substring(0,15)+'-'+string.valueOf(ar.get('key'));
                mpTotals.put(userkey, (decimal) ar.get('tot'));
            }
        }                                        
                                                
        list<AggregateResult> lstHOSResult = [Select sum(Target__c) tot, HOS_User__c hos, Month_key__c key
                                                from Target__c 
                                                where RecordType.Name = 'Staff' and month_Key__c in: keys
                                                and HOS_User__c != null
                                                group by HOS_User__c, Month_key__c];
        for(AggregateResult ar : lstHOSResult){
            if(string.valueOf(ar.get('hos')) != null){
                string userkey = string.valueOf(ar.get('hos')).substring(0,15)+'-'+string.valueOf(ar.get('key'));
                mpTotals.put(userkey, (decimal) ar.get('tot'));
            }
        }                                        
                                                
        list<AggregateResult> lstHODResult = [Select sum(Target__c) tot, HOD_User__c hod, Month_key__c key
                                                from Target__c 
                                                where RecordType.Name = 'Staff' and month_Key__c in: keys 
                                                and HOD_User__c != null
                                                group by HOD_User__c, Month_key__c];
        for(AggregateResult ar : lstHODResult){
            if(string.valueOf(ar.get('hod')) != null){
                string userkey = string.valueOf(ar.get('hod')).substring(0,15)+'-'+string.valueOf(ar.get('key'));
                mpTotals.put(userkey, (decimal) ar.get('tot'));
            }
        }
        
        map<string, Target__c> mpTargetsExisting = new map<string, Target__c>();
        list<Target__c> existing = [Select id, Uniq__c from Target__c where Uniq__c =: mpTotals.keyset()];
        for(Target__c tar: existing){
            mpTargetsExisting.put(tar.Uniq__c, tar);
        }
        System.debug('>>>>mpTargetsExisting>>>>>>'+mpTargetsExisting);
        
        list<Target__c> lst2Update = new list<Target__c>();
        for(string userKey : mpTotals.Keyset()){
            System.debug('>>>>userKey>>>>>>'+userKey);
            Target__c tar = mpTargetsExisting.containsKey(userKey) ? mpTargetsExisting.get(userKey) : new Target__c();
            tar.Uniq__c = userKey;
            list<string> splits = tar.Uniq__c.split('-');
            tar.Month__c = splits[1];
            tar.Year__c = splits[2];
            tar.User__c = splits[0];
            tar.recordTypeId = Schema.SObjectType.Target__c.getRecordTypeInfosByName().get('Staff').getRecordTypeId();
            tar.Target__c = mpTotals.get(tar.Uniq__c);
            lst2Update.add(tar);
        }
        
        if(!lst2Update.isEmpty())
            upsert lst2Update;
    }
    */
}