/*

Controller for VF page on the Assignment Rule which shows the count of Inquiries from the Rule
*/
global class DAMAC_RULE_UTILITY{
    
    public transient integer resultrows{get;set;}
    public String query { get; set; }
    public DAMAC_RULE_UTILITY(ApexPages.StandardController controller) {
        
    }
    
    webservice static String retrySMS (String recordId, String objectAPI) {
        String query = '';
        string ruleId = recordId;
        String fields = getAllfields('Inquiry_Assignment_Rules__c');
        string queryCondition = 'SELECT RecordType.Name, '+fields+' FROM Inquiry_Assignment_Rules__c '
            +' WHERE id= \''+ruleId+'\' LIMIT 1';
        
        Inquiry_Assignment_Rules__c assignmentRule;
        assignmentRule = Database.query (queryCondition);
        
        List <Inquiry_Assignment_Rules__c> childRules = new List <Inquiry_Assignment_Rules__c> ();
        childRules = Database.query ('SELECT '+fields+' FROM Inquiry_Assignment_Rules__c '
                                     +' WHERE Active__c = True AND Parent_Reassign_Rule__c = \''+ruleId+'\' Order by Order__c Asc');
        
        String userLogic = assignmentRule.filter_logic__c;
        if (assignmentRule.Rule_for_HOD_Filter_Logic__c != NULL) {
            if (userLogic == NULL)
                userLogic = '';
            userLogic = assignmentRule.Rule_for_HOD_Filter_Logic__c+' '+userLogic;
        }
        
        if (assignmentRule.recordType.Name == 'Retry SMS') {
            query = 'SELECT Id FROM '+assignmentRule.Object_Api__c +' WHERE ';
        }    
        
        Map <String, Inquiry_Assignment_Rules__c> rulesMap = new Map <String, Inquiry_Assignment_Rules__c> ();
        for (Inquiry_Assignment_Rules__c con: childRules){
            rulesMap.put (String.valueOf(con.order__c), con);
        }
        
        if (String.isNotBlank (userLogic)) { 
            query += +' ( '+ formatQuery (userLogic, rulesMap)+' ) ';
            
            if(assignmentRule.query_Limit__c != null){
                query += ' LIMIT '+assignmentRule.query_Limit__c;
            }
        }
        system.debug(query);
        Database.executeBatch (new Damac_RetrySMSBatch(query, objectAPI, assignmentRule.SMS_Message__c), 1);
        return 'Job started.';
    }
    
    
    
    public void onload(){ 
        query = '';
        string ruleId = apexpages.currentpage().getparameters().get('id'); 
        String fields = getAllfields('Inquiry_Assignment_Rules__c');
        //String executeOn = 'Reassign';
        string queryCondition = 'SELECT RecordType.Name, '+fields+' FROM Inquiry_Assignment_Rules__c '
            //+' WHERE Execute_on__c =:executeOn ' 
            +' WHERE id= \''+ruleId+'\' LIMIT 1';
        
        
        system.debug('rulequey'+queryCondition);
        Inquiry_Assignment_Rules__c assignmentRule;
        assignmentRule = Database.query (queryCondition);
        // Childs and Query
        
        List <Inquiry_Assignment_Rules__c> childRules = new List <Inquiry_Assignment_Rules__c> ();
        childRules = Database.query ('SELECT '+fields+' FROM Inquiry_Assignment_Rules__c '
                                     +' WHERE Active__c = True AND Parent_Reassign_Rule__c = \''+ruleId+'\' Order by Order__c Asc');
        
        String userLogic = assignmentRule.filter_logic__c;
        if (assignmentRule.Rule_for_HOD_Filter_Logic__c != NULL) {
            if (userLogic == NULL)
                userLogic = '';
            userLogic = assignmentRule.Rule_for_HOD_Filter_Logic__c+' '+userLogic;
        }
        
        if(assignmentRule.execute_on__c == 'Dialing Priority'){
            String recordTypeName = assignmentRule.Inquiry_Record_Type__c;    
            system.debug('recordTypeName'+recordTypeName );
            query = 'SELECT id FROM Dialing_list__c WHERE ';
        }else if(assignmentRule.execute_on__c == 'Reassign'){ 
            String recordTypeName = assignmentRule.Inquiry_Record_Type__c;
            system.debug('recordTypeName'+recordTypeName );
            query = 'SELECT id FROM Inquiry__c WHERE RecordType.Name =\''+recordTypeName+'\' AND ';
        } else if (assignmentRule.recordType.Name == 'Retry SMS') {
            query = 'SELECT Id FROM '+assignmentRule.Object_Api__c +' WHERE ';
        }    
        
        String output;
        Map <String, Inquiry_Assignment_Rules__c> rulesMap = new Map <String, Inquiry_Assignment_Rules__c> ();
        for (Inquiry_Assignment_Rules__c con: childRules){
            rulesMap.put (String.valueOf(con.order__c), con);
        }
        
        if (String.isNotBlank (userLogic)) { 
            query += +' ( '+ formatQuery (userLogic, rulesMap)+' ) ';
            system.debug('>>>FINAL QUERY'+query);
            if(assignmentRule.query_Limit__c != null){
                query += ' LIMIT '+assignmentRule.query_Limit__c;
            }
            resultrows= RecordCount(query);
            system.debug('resultrows'+resultrows);
        }
        
    }
    public static String formatQuery (String queryLogic, Map <String, Inquiry_Assignment_Rules__c> rules) {
        String query = '';
        for (String key : queryLogic.split (' ')) {
            if (key.isNumeric ()) {
                System.Debug (key );
                query += +' '+rules.get (key).Query_Condition__c+' ';
            } else
                query += key;
        }
        System.debug (query);
        return query;
    }
    
    public static integer RecordCount (String soqlQuery) { 
        integer inquiriesCount = 0; 
        String UserId = UserInfo.getUserId();
        Send_SMS__c settings = Send_SMS__c.getInstance (UserInfo.getUserID ());
        String SerViceURL = settings.Salesforce_Base_URL__c;
        
        partnerSoapSforceCom.Soap partner = new partnerSoapSforceCom.Soap();
        partnerSoapSforceCom.SessionHeader_element header=new partnerSoapSforceCom.SessionHeader_element();
        header.sessionId = userInfo.getSessionID ();
        
        partnerSoapSforceCom.LoginResult result = new partnerSoapSforceCom.LoginResult ();
        partner.SessionHeader = header;
        partner.endpoint_x = SerViceURL+'/services/Soap/u/33.0';
        partner.timeout_x = 120000;
        
        try{
            partnerSoapSforceCom.QueryResult qr = partner.Query (soqlQuery);
            inquiriesCount = qr.size;
            system.debug (inquiriesCount);
            
            
        } catch(Exception e ) {
            system.debug(e.getMessage ());
        }
        if (Test.isRunningTest ()){
            inquiriesCount = 1;
        }
        return inquiriesCount;
        
    }
    
    
    public static string getAllfields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        if (objectType == null) {
            return fields;
        }
        for (string f :objectType.getDescribe ().fields.getMap ().keySet ())
            fields += f+ ', ';
        return fields.removeEnd(', '); 
    }
    
    
    
    
}