public class Be_My_Colleague {
	public Inquiry__c newInquiry {get; set;}
	public Boolean savedLead {get;set;}
	public Be_My_Colleague() {
		newInquiry = new Inquiry__c();
		savedLead = false;
	}
	public void save(){
		try{
			Id inquiryRecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
			List<Campaign__c> lstCampaign = [Select Id, Campaign_Name__c From Campaign__c Where Campaign_Name__c = 'Be My Neighbour' LIMIT 1];
			newInquiry.Mobile_Phone_Encrypt__c = newInquiry.Mobile_Phone__c;
			newInquiry.Mobile_Phone__c = null;
			newInquiry.RecordTypeId = inquiryRecordTypeId;
			newInquiry.Primary_Contacts__c = 'Mobile Phone';
			newInquiry.Preferred_Language__c = 'English';
			newInquiry.Inquiry_Source__c = Label.Be_my_Colleague_Inquiry_Source;
			newInquiry.Referred_Customer__c = CustomerCommunityUtils.customerAccountId;
			if(!lstCampaign.isEmpty()){
				newInquiry.Campaign__c = lstCampaign[0].Id;
			}
			system.debug('>>>>>>newInquiry : '+newInquiry);
			savedLead = false;
			if(newInquiry != null){
				system.debug('>>>>>>Inserting newInquiry : '+newInquiry);
				insert newInquiry;
				system.debug('>>>>>>Inserted');
				savedLead = true;
				newInquiry = new Inquiry__c();
			}
		}catch(Exception e){
            System.debug('Exception: ' + e.getMessage() + e.getStackTraceString() );
            savedLead = false;
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Some error occured while saving response'));
        }
	}
}