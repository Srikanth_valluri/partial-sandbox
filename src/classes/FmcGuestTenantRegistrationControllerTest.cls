@isTest
private class FmcGuestTenantRegistrationControllerTest {

    @isTest
    static void testController() {
        insert new Location__c(Name = 'LOC', Location_ID__c = 'LOC');
        System.assertEquals(NULL, FmcGuestTenantRegistrationController.lookupBuilding(NULL));
        System.assert(FmcGuestTenantRegistrationController.lookupBuilding('test').isEmpty());
        System.assertEquals(NULL, FmcGuestTenantRegistrationController.lookupUnit('test', NULL));
        System.assert(FmcGuestTenantRegistrationController.lookupUnit('test', 'INVALID_LOC').isEmpty());
    }

    @isTest static void checkDraftCaseTest() {
        Account customerAccount = TestDataFactoryFM.createAccount();
        insert customerAccount;

        NSIBPM__Service_Request__c objSr = TestDataFactoryFM.createServiceRequest(customerAccount);
        insert objSr;

        Booking__c  objBooking = TestDataFactoryFM.createBooking(customerAccount, objSr);
        insert objBooking;

        Booking_Unit__c objBookingUnit = TestDataFactoryFM.createBookingUnit(customerAccount, objBooking);
        insert objBookingUnit;

        FM_Case__c objFmCase = new FM_Case__c();
        objFmCase.Booking_Unit__c = objBookingUnit.id;
        objFmCase.Origin__c = 'Portal - Guest';
        objFmCase.Request_Type__c = 'Tenant Registration';
        objFmCase.Status__c = 'In Progress';
        insert objFmCase;

        Test.startTest();
            FmcGuestTenantRegistrationController.checkDraftCase(objBookingUnit.id);
        Test.stopTest();
    }

    @isTest static void checkDraftCaseTest2() {
        Account customerAccount = TestDataFactoryFM.createAccount();
        insert customerAccount;

        NSIBPM__Service_Request__c objSr = TestDataFactoryFM.createServiceRequest(customerAccount);
        insert objSr;

        Booking__c  objBooking = TestDataFactoryFM.createBooking(customerAccount, objSr);
        insert objBooking;

        Booking_Unit__c objBookingUnit = TestDataFactoryFM.createBookingUnit(customerAccount, objBooking);
        insert objBookingUnit;

        FM_Case__c objFmCase = new FM_Case__c();
        objFmCase.Booking_Unit__c = objBookingUnit.id;
        objFmCase.Origin__c = 'Portal - Guest';
        objFmCase.Request_Type__c = 'Tenant Registration';
        objFmCase.Status__c = 'In Progress';
        insert objFmCase;

        Test.startTest();
            FmcGuestTenantRegistrationController.checkDraftCase('');
        Test.stopTest();
    }
}