/*--------------------------------------------------------------------------------------------------
Description: Controller for PromotorRegistrationPage

====================================================================================================
 Version | Date(DD-MM-YYYY) | Last Modified By | Comments                                           
----------------------------------------------------------------------------------------------------
 1.0     | 18-02-2018       | Lochana Rajput   | 1. Initial draft                                   
----------------------------------------------------------------------------------------------------
 2.0     | 19-02-2018       | Lochana Rajput   | 1. Added methods to save Inquiry details           
----------------------------------------------------------------------------------------------------
 3.0     | 26-02-2018       | Lochana Rajput   | 1. Added logic to assign owner based Meeting Type
----------------------------------------------------------------------------------------------------
 4.0     | 05-03-2018       | Craig Lobo       | 1. Commented the Sharing Logic.
----------------------------------------------------------------------------------------------------
 5.0     | 15-03-2018       | Lochana Rajput   | 1. Added logic to retain selected stand
----------------------------------------------------------------------------------------------------
 6.0     | 20-03-2018       | Lochana Rajput   | 1. Added logic to assign Inquiry creation date if meeting
                                                    type is direct tour
   =================================================================================================
*/

public without sharing class PromotorRegistrationController {

    public Inquiry__c objInquiry                    {get; set;}
    public String userId                            {get; set;}
    public String msgType                           {get; set;}
    public String requiredCls                       {get; set;}
    public String msg                               {get; set;}
    public String selectedStand                     {get; set;}
    public Boolean showMessage                      {get; set;}
    public Boolean showInqPanel                      {get; set;}
    public List<SelectOption> stands                {get; set;}
    public InquiryShareWithManager inqShareObj;
    public string selectedStandStr{get;set;}
    map<String,String> mapStandVal_Label;
    
    @RemoteAction
    public static List<Inquiry__c> queryInquiries() {
         return [SELECT Id, First_Name__c, Last_Name__c, Mobile_CountryCode__c, Mobile_Phone_Encrypt__c, Email__c,Country__c from Inquiry__c ];
    }
    public PromotorRegistrationController() {
        showInqPanel = false;
        mapStandVal_Label = new map<String,String>();
        showMessage = false;
        objInquiry = new Inquiry__c();
        stands = new List<SelectOption>();
        System.debug('==getUserId=='+UserInfo.getUserId());
        //stands.add(new SelectOption('None', 'None'));
        for(Campaign__c campaign : [SELECT Name
                                         , Id
                                         , Campaign_Name__c
                                      FROM Campaign__c
                                     WHERE Active__c = true
                                       AND Campaign_Type_New__c = 'Stands'
                                       AND Campaign_Name__c != NULL
        ] ) {
            stands.add(new SelectOption(campaign.Id, campaign.Campaign_Name__c));
            mapStandVal_Label.put(campaign.Id, campaign.Campaign_Name__c);
        }
        requiredCls = stands.size() > 1 ? 'requiredField' : '';
    }

    public pageReference cancel() {
        pageReference pg = new pageReference('/home/home.jsp');
        return pg;
    }
    public pageReference selectStand() {
        if(mapStandVal_Label.containsKey(selectedStand)) {
            showInqPanel = true;
            selectedStandStr = mapStandVal_Label.get(selectedStand);
        }
        return null;
    }
    public pageReference submitInquiry() {
        showMessage = false;
        System.debug('==objInquiry==' + objInquiry);
        System.debug('==PC userId==' + userId);

        // Get the list of Promoter Profile Names
        List<String> profileNames = Label.PromoterCommunityProfile.split(',');
        User currentUser = [ SELECT Id
                                  , Profile.Name
                                  , Name
                                  , ContactId
                               FROM User
                              WHERE Id = :UserInfo.getUserId()
        ];

        objInquiry.Created_By_Profile__c = currentUser.Profile.Name;

        // Populate Promoter Name if User Profiel is mentioned in the Promoter Label
        if(profileNames != null 
            && !profileNames.isEmpty()
            && profileNames.contains(currentUser.Profile.Name)
        ) {
            objInquiry.Promoter_Name__c = currentUser.Name;
        }

        // Populate Campaign
        if(selectedStand != 'None' && String.isNotBlank(selectedStand)) {
            objInquiry.Campaign__c = selectedStand;
        }

        // Assign the Inquiry to the Current User and Inquiry RT as Pre-Inquiry 
        if(objInquiry.Meeting_Type__c == 'Scheduled Tour' 
            || String.isBlank(objInquiry.Meeting_Type__c)
        ) {
            objInquiry.RecordTypeId = 
                Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
            objInquiry.OwnerId = UserInfo.getUserId();
        }
        if(objInquiry.Meeting_Type__c == 'Direct Tour' ) {
                objInquiry.Tour_Date_Time__c = System.now();
        }

        if(String.isNotBlank(userId)) {

            // Assign Owner to PC
            objInquiry.Assigned_PC__c = userId;
            if(objInquiry.Meeting_Type__c == 'Direct Tour' 
                || objInquiry.Meeting_Type__c == 'Meeting On Stand'
            ) {
                objInquiry.OwnerId = userId;
                objInquiry.recordTYpeId = 
                    Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
            }
        }

        System.debug('==objInquiry==: ' + objInquiry.Tour_Date_Time__c);
        showMessage = true;
        if(String.isNotBlank(String.valueOf(objInquiry.Tour_Date_Time__c))
            && (objInquiry.Tour_Date_Time__c > Date.today().addDays(3)
            || objInquiry.Tour_Date_Time__c < Date.today())
            && objInquiry.Meeting_Type__c == 'Scheduled Tour') {
            msgType = 'error';
            msg='Please select Tour Date/Time within 3 days from today';
            return null;
        }
        try {
            // DML Inquiry
            insert objInquiry;
            List<Inquiry__c> inqRecLst = [ SELECT Id
                                                , Name
                                                , OwnerId
                                                , CreatedById 
                                            FROM Inquiry__c 
                                            WHERE Id = :objInquiry.Id
            ];

            // v5.0 05-03-2018 Craig - Commented the Sharing Logic.
            // Create a Sharing record for the Inquiry Creator (only for Internal Users) 
            /*if(profileNames.contains(currentUser.Profile.Name)
                && profileNames != null 
                && inqRecLst != null
                && !profileNames.isEmpty()
                && !inqRecLst.isEmpty()
            ) {
                // Pass the Inquiry in a list to be shared with its Managers 
                inqShareObj = new InquiryShareWithManager();
                inqShareObj.InquiryShare(inqRecLst);
                //Inquiry__share objInquiryShare = new Inquiry__share();
                //objInquiryShare.ParentId = objInquiry.Id;
                //objInquiryShare.UserOrGroupId = UserInfo.getUserId();
                //objInquiryShare.RowCause = Schema.Inquiry__Share.RowCause.Promoter__c;
                //objInquiryShare.AccessLevel = 'Read';
                // DML Inquiry Share
                //insert objInquiryShare; 
            } */
            // v5.0 05-03-2018 Craig

            msg = ' Inquiry ' + inqRecLst[0].Name + ' has been created successfully ';
            objInquiry = new Inquiry__c();
            //selectedStand = '';
            userId = null;
            msgType = 'success';
            ApexPages.addmessage(new ApexPages.message(
                ApexPages.severity.Info,
                'Inquiry has been created successfully'
            ));
        } catch(System.DmlException excp) {
            msgType = 'error';
            System.debug('===='+excp.getDmlFieldNames(0));
            System.debug('==***=='+excp);
            msg = excp.getMessage() 
                + ': '
                + excp.getLineNumber();
            ApexPages.addmessage(new ApexPages.message(
                ApexPages.severity.Error, 
                excp.getMessage() + ': ' + excp.getLineNumber()
            ));
        }
        return null;
    }
}