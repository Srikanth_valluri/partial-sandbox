/***************************************************************************************************************
Description: Schedulable class for RoasteringpiBatch.
________________________________________________________________________________________________________________
Version Date(DD-MM-YYYY)    Author              Description
----------------------------------------------------------------------------------------------------------------
1.0     16-07-2020          Komal Shitole       Initial Draft
****************************************************************************************************************/
public class RoasteringApiBatchScheduler Implements Schedulable  {
     public void execute(SchedulableContext SC){
        System.debug('inside scheduler');
        RoasteringApiBatch batchInst = new RoasteringApiBatch();
        Database.executeBatch(batchInst,1);
    }
}