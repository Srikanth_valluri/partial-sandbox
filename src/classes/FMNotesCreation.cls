public without sharing class FMNotesCreation{//AKISHOR

    public static void createFMNoteBasedOnCEComments( List<Calling_List__c> lstCallingList ,Map<Id, Calling_List__c> mapOldCallList ) {
        
        Id fmCallingListRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('FM Collections').RecordTypeId;
        
        system.debug('in  createNoteBasedOnCEComments : '+lstCallingList);

        List<FM_Notes__c> lstFMNote = new List<FM_Notes__c>();
        if( lstCallingList.size() > 0 ) {
            for( Calling_List__c objCallingList : lstCallingList ) {
                if( objCallingList.IsHideFromUI__c == false 
                 && objCallingList.CE_Comments__c != '' 
                 && (mapOldCallList.get(objCallingList.Id).CE_Comments__c != objCallingList.CE_Comments__c )
                 && (objCallingList.RecordTypeId == fmCallingListRecordTypeId) ) {
                
                        FM_Notes__c fmnotes = new FM_Notes__c();
                        fmnotes.FMOutcome__c=objCallingList.Call_Outcome__c;
                        fmnotes.FM_Remarks__c=objCallingList.CE_Comments__c;
                        fmnotes.PTP_Date__c=objCallingList.PTP_Date__c;
                        //fmnotes.Result__c=objCallingList.CallOutcome__c;  
                        fmnotes.Type__c='Notes';
                        fmnotes.FMCallingList__c = objCallingList.Id;
                        fmnotes.Account__c = objCallingList.Account__c;
                        
                        lstFMNote.add(fmnotes);
                                                  
                }
            }
            try{
                if( lstFMNote != null && lstFMNote.size() > 0 ) {
                    insert lstFMNote;
                }
            } catch( Exception e ) {
                System.debug('there is something problem:while insertting Note :'+e.getMessage());
            }
        }
    }
    
}