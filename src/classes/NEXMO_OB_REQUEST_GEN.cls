public class NEXMO_OB_REQUEST_GEN{
    public cls_from WA_from;
    public cls_to to;
    public cls_message message;
    public class cls_from {
        public String type; //whatsapp
        public String WA_number;   //
    }
    public class cls_to {
        public String type; //whatsapp
        public String WA_number;   //
    }
    public class cls_message {
        public cls_content content;
        public cls_whatsapp whatsapp;
    }
    
    public class cls_whatsapp{
        public string policy;
        public string locale;
    }
    public class cls_content {
        public String type; //template
        public cls_template template;
    }
    public class cls_template {
        public String name; //whatsapp:hsm:technology:nexmo:verify
        //public string fallback_locale;
        public cls_parameters[] parameters;
    }
    public class cls_parameters {
        public String WA_default;  //Nexmo Verification
    }
    public static NEXMO_OB_REQUEST_GEN parse(String json){
        return (NEXMO_OB_REQUEST_GEN) System.JSON.deserialize(json, NEXMO_OB_REQUEST_GEN.class);
    }
}