public without sharing class FmcGuestTenantRegistrationController {

    @RemoteAction
    public static List<Location__c> lookupBuilding(String name) {
        if (String.isBlank(name)) {
            return NULL;
        }
        name = '%' + String.escapeSingleQuotes(name) + '%';
        return [
            SELECT  Id
                    , Name
                    , Building_Name__c
                    , Location_Code__c
            FROM    Location__c
            WHERE   (Building_Name__c LIKE :name OR  Name LIKE :name)
                AND (NOT Building_Name__c  LIKE '%STORAGE%')
                AND (Location_Type__c = 'Building' OR Location_Type__c = NULL)
            LIMIT   200
        ];
    }

    @RemoteAction
    public static List<Booking_Unit__c> lookupUnit(String name, String locationId) {
        if (String.isBlank(name) || String.isBlank(locationId)) {
            return NULL;
        }
        name = '%' + String.escapeSingleQuotes(name) + '%';
        Set<String> setActiveStatuses = Booking_Unit_Active_Status__c.getAll().keySet();
        return [
            SELECT  Id
                    , Unit_Name__c
            FROM    Booking_Unit__c
            WHERE   Inventory__r.Building_Location__c = :locationId
                    AND Unit_Name__c LIKE :name
                    AND ((Registration_Status__c IN :setActiveStatuses
                            AND (Property_Status__c = 'Ready' OR Inventory__r.Property_Status__c = 'Ready')
                        ) OR Dummy_Booking_Unit__c = TRUE)
            LIMIT   200
        ];
    }

    @RemoteAction
    public static String checkDraftCase(String unitId) {
        if(String.isBlank(unitId)) {
            return NULL;
        }
        FM_Case__c objFmCase =  [ SELECT Id
                                        , Booking_Unit__c
                                        , Origin__c
                                        , Request_Type__c
                                        , Status__c
                                  FROM FM_Case__c
                                  WHERE Booking_Unit__c = :unitId
                                  AND Origin__c = 'Portal - Guest'
                                  AND Request_Type__c = 'Tenant Registration'
                                  AND Status__c IN ('Draft Request' , 'In Progress' , 'New' , 'Awaiting Correction')
                                  ORDER BY CreatedDate DESC
                                  LIMIT 1 ];
        if(objFmCase != NULL){
            return objFmCase.id;
        }else {
            return NULL;
        }
    }
}