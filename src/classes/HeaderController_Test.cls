/**
    @Who  : Rajnish Kumar
    @When : 29 March 2018
    @What : This class is test class of Header Controller.
*/

@isTest
public class HeaderController_Test {
    public static testMethod void testUnit() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        
        User u = new User(Alias = 'standt', Email='sync@google.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='sarduser@testorg.com',
            Stand_Id__c= 'a126E000000a7Vx');
 
            insert u;
            Campaign__c camp = new Campaign__c();
        //camp.RecordTypeId = RSRecordTypeId;
        camp.Campaign_Name__c='Test Campaign';
        //camp.Active__c = false;
        camp.Campaign_Type_New__c = 'Stands';
        camp.start_date__c = System.today();
        camp.end_date__c = System.Today().addDays(30);
        camp.Marketing_start_date__c = System.today();
        camp.Marketing_end_date__c = System.Today().addDays(30);
        camp.Language__c = 'English';
        insert camp;
    
         HeaderController objHeader = new HeaderController();
         objHeader.forwardToStartPage();
         objHeader.selectStand();
     
   }
    
}