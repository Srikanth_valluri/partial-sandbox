@isTest
public class CC_Update_sObject_Text {

    public static testmethod void UnitTest()
    {
        CC_Update_sObject obj=new CC_Update_sObject();
        List<NSIBPM__Action__c> actionList  =new List<NSIBPM__Action__c>();
        NSIBPM__Action__c action=new NSIBPM__Action__c();
        action.NSIBPM__Action_Type__c='NSIBPM__Service_Request__c';
        action.NSIBPM__Field_Name__c='All_Joint_Buyer_Same__c';
        action.NSIBPM__Field_Type__c='BOOLEAN';
        action.NSIBPM__Value_or_Field__c='Value';
        action.NSIBPM__Field_Value__c='true';
        actionList.add(action);
        
        action=new NSIBPM__Action__c();
        action.NSIBPM__Action_Type__c='NSIBPM__Service_Request__c';
        action.NSIBPM__Field_Name__c='Amount__c';
        action.NSIBPM__Field_Type__c='DOUBLE';
        action.NSIBPM__Value_or_Field__c='Value';
        action.NSIBPM__Field_Value__c='1';
        actionList.add(action);
        
        
        action=new NSIBPM__Action__c();
        action.NSIBPM__Action_Type__c='NSIBPM__Service_Request__c';
        action.NSIBPM__Field_Name__c='Broker_Card_Expiry_Date__c';
        action.NSIBPM__Field_Type__c='DATE';
        action.NSIBPM__Value_or_Field__c='Value';
        action.NSIBPM__Field_Value__c='2017/01/01';
        actionList.add(action);
        
       //NSIBPM__Step__c 
       //
        action=new NSIBPM__Action__c();
        action.NSIBPM__Action_Type__c='NSIBPM__Step__c';
        action.NSIBPM__Field_Name__c='NSIBPM__Step_No__c';
        action.NSIBPM__Field_Type__c='DOUBLE';
        action.NSIBPM__Value_or_Field__c='Value';
        action.NSIBPM__Field_Value__c='1';
        actionList.add(action);
        
        
        action=new NSIBPM__Action__c();
        action.NSIBPM__Action_Type__c='NSIBPM__Step__c';
        action.NSIBPM__Field_Name__c='NSIBPM__Closed_Date__c';
        action.NSIBPM__Field_Type__c='DATE';
        action.NSIBPM__Value_or_Field__c='Value';
        action.NSIBPM__Field_Value__c='2017/01/01';
        actionList.add(action);
        
        
        action=new NSIBPM__Action__c();
        action.NSIBPM__Action_Type__c='NSIBPM__Step__c';
        action.NSIBPM__Field_Name__c='NSIBPM__Summary__c';
        action.NSIBPM__Field_Type__c='TEXT';
        action.NSIBPM__Value_or_Field__c='Value';
        action.NSIBPM__Field_Value__c='2017/01/01';
        actionList.add(action);
        
        
        action=new NSIBPM__Action__c();
        action.NSIBPM__Action_Type__c='SR Field';
        action.NSIBPM__Field_Name__c='NSIBPM__Summary__c';
        action.NSIBPM__Field_Type__c='TEXT';
        action.NSIBPM__Value_or_Field__c='Value';
        action.NSIBPM__Field_Value__c='2017/01/01';
        actionList.add(action);
        
        
        action=new NSIBPM__Action__c();
        action.NSIBPM__Action_Type__c='Account';
        action.NSIBPM__Field_Name__c='NSIBPM__Summary__c';
        action.NSIBPM__Field_Type__c='TEXT';
        action.NSIBPM__Value_or_Field__c='Value';
        action.NSIBPM__Field_Value__c='2017/01/01';
        actionList.add(action);
        insert actionList; 
            	List<NSIBPM__SR_Template__c> SRTemplateList =  InitialiseTestData.createTestTemplateRecords(new List<NSIBPM__SR_Template__c>{new NSIBPM__SR_Template__c()});
        Id RecordTypeIdAGENT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        Id RecordTypeIdContact = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        Account a = new Account();
        a.recordtypeid=RecordTypeIdAGENT;
        a.Name = 'Test Account';
        a.Agency_Short_Name__c = 'testShrName';
        insert a;
        
        Account a2 = new Account();
        a2.recordtypeid=RecordTypeIdAGENT;
        a2.Name = 'Test Account';
        a2.Agency_Short_Name__c = 'testShrName';
        insert a2;
    	
        NSIBPM__Service_Request__c sr = InitializeSRDataTest.getSerReq('Agent Registration',true,null);
        sr.agency__c=a.ID;
        sr.recordtypeid=RecordTypeIdContact;
        sr.NSIBPM__SR_Template__c = SRTemplateList[0].Id;
        sr.ID_Type__c = 'Passport';
		insert sr;
		
        NSIBPM__Step__c stp = InitializeSRDataTest.createStep(sr.id,null,null);
        
        insert stp;
        /*
        List<NSIBPM__Service_Request__c> requestList=TestDataFactory.createTestServiceRequestRecords(new List<NSIBPM__Service_Request__c>{new NSIBPM__Service_Request__c()});
        testDataFactory.createTestStepRecords(new List<NSIBPM__Step__c>{new NSIBPM__Step__c()});
       
        NSIBPM__Step__c step=new NSIBPM__Step__c();
        step.NSIBPM__SR__c=requestList[0].id;
        insert step;
        */
        
        NSIBPM__Service_Request__c aa;
       // a.Address_Change__c
      //a.Agency_Name__c
     //a.RERA_Expiry_Change__c
        //obj.UpdatesObject(action,step);
        //obj.UpdatesObject(action2,step);
        //obj.UpdatesObject(action3,step);
        for(NSIBPM__Action__c act:actionList)
        {
            obj.UpdatesObject(act,stp);    
        }
    }
}