public without sharing class HO_DocGeneration {
  
  public static HO_DocGeneration.HO_DocResponse GetHOPack(String reqName, String regId) {
        HO_DocGeneration.HO_DocResponse objDocWrapper ;
        HODocuments1.HOInvoicePackHttpSoap11Endpoint objHOPack = new HODocuments1.HOInvoicePackHttpSoap11Endpoint();
        objHOPack.timeout_x = 120000 ;
        list<HODocuments2.APPSXXDC_PROCESS_SERX1794747X1X5> lstReg = new list<HODocuments2.APPSXXDC_PROCESS_SERX1794747X1X5>();
        HODocuments2.APPSXXDC_PROCESS_SERX1794747X1X5 reg = new HODocuments2.APPSXXDC_PROCESS_SERX1794747X1X5();
        reg.ATTRIBUTE1 = '';
        reg.ATTRIBUTE10 = '';
        reg.ATTRIBUTE11 = '';
        reg.ATTRIBUTE12 = '';
        reg.ATTRIBUTE13 = '';
        reg.ATTRIBUTE14 = '';
        reg.ATTRIBUTE15 = '';
        reg.ATTRIBUTE16 = '';
        reg.ATTRIBUTE17 = '';
        reg.ATTRIBUTE18 = '';
        reg.ATTRIBUTE19 = '';
        reg.ATTRIBUTE2 = '';
        reg.ATTRIBUTE20 = '';
        reg.ATTRIBUTE21 = '';
        reg.ATTRIBUTE22 = '';
        reg.ATTRIBUTE23 = '';
        reg.ATTRIBUTE24 = '';
        reg.ATTRIBUTE25 = '';
        reg.ATTRIBUTE26 = '';
        reg.ATTRIBUTE27 = '';
        reg.ATTRIBUTE28 = '';
        reg.ATTRIBUTE29 = '';
        reg.ATTRIBUTE3 = '';
        reg.ATTRIBUTE30 = '';
        reg.ATTRIBUTE31 = '';
        reg.ATTRIBUTE32 = '';
        reg.ATTRIBUTE33 = '';
        reg.ATTRIBUTE34 = '';
        reg.ATTRIBUTE35 = '';
        reg.ATTRIBUTE36 = '';
        reg.ATTRIBUTE37 = '';
        reg.ATTRIBUTE38 = '';
        reg.ATTRIBUTE39 = '';
        reg.ATTRIBUTE4 = '';
        reg.ATTRIBUTE41 = '';
        reg.ATTRIBUTE42 = '';
        reg.ATTRIBUTE43 = '';
        reg.ATTRIBUTE44 = '';
        reg.ATTRIBUTE45 = '';
        reg.ATTRIBUTE46 = '';
        reg.ATTRIBUTE47 = '';
        reg.ATTRIBUTE48 = '';
        reg.ATTRIBUTE49 = '';
        reg.ATTRIBUTE5 = '';
        reg.ATTRIBUTE50 = '';
        reg.ATTRIBUTE6 = '';
        reg.ATTRIBUTE7 = '';
        reg.ATTRIBUTE8 = '';
        reg.ATTRIBUTE9 = '';
        reg.PARAM_ID = regId;
        lstReg.add(reg);
        system.debug('lstReg'+lstReg);
        string strHand;
        objDocWrapper = new HO_DocGeneration.HO_DocResponse();
        try {
            strHand = objHOPack.HandOverPack('2-'+String.valueOf(System.currentTimeMillis()), reqName,'SFDC',lstReg);
            system.debug('strHand '+strHand );
            JSON2Apex objHoPackResponse = new JSON2Apex ();
            objHoPackResponse = (HO_DocGeneration.JSON2Apex)JSON.deserialize(strHand, HO_DocGeneration.JSON2Apex.class);
            String strResponse = strHand.substringAfter('[').substringBeforeLast(']');
            Map<String, object> mapHOPack = (Map<String, object>)JSON.deserializeUntyped( strResponse.replace('\\','').removeStart('"').removeEnd('"') );
                    system.debug('mapHOPack'+mapHOPack );                    
                    objDocWrapper.P_PARAM_ID= String.valueOf( mapHOPack.get('PARAM_ID') );
                    objDocWrapper.P_PROC_STATUS= String.valueOf( mapHOPack.get('PROC_STATUS') );
                    objDocWrapper.P_PROC_MESSAGE= String.valueOf( mapHOPack.get('PROC_MESSAGE') );
                    objDocWrapper.URL= String.valueOf( mapHOPack.get('ATTRIBUTE1') );
                    //objDetailsWrapper.P_ATTRIBUTE2= String.valueOf( objUnitDetails.get('ATTRIBUTE2') );
                    //objDetailsWrapper.P_ATTRIBUTE3= String.valueOf( objUnitDetails.get('ATTRIBUTE3') );
            system.debug(objDocWrapper);
        } catch (Exception e){
            objDocWrapper.P_PROC_MESSAGE = strHand;
            objDocWrapper.URL = '';
        }
        return objDocWrapper ;
    }
  
  public static HO_DocGeneration.HO_DocResponse GetHOInvoice(String reqName, String regId, String strStartDate, String strEndDate) {
        HO_DocGeneration.HO_DocResponse objDocWrapper ;
        HODocuments1.HOInvoicePackHttpSoap11Endpoint objHOInvoice = new HODocuments1.HOInvoicePackHttpSoap11Endpoint();
        objHOInvoice.timeout_x = 120000 ;
        list<HODocuments2.APPSXXDC_PROCESS_SERX1794747X1X5> lstReg = new list<HODocuments2.APPSXXDC_PROCESS_SERX1794747X1X5>();
        HODocuments2.APPSXXDC_PROCESS_SERX1794747X1X5 reg = new HODocuments2.APPSXXDC_PROCESS_SERX1794747X1X5();
        reg.ATTRIBUTE1 = strStartDate;
        reg.ATTRIBUTE10 = '';
        reg.ATTRIBUTE11 = '';
        reg.ATTRIBUTE12 = '';
        reg.ATTRIBUTE13 = '';
        reg.ATTRIBUTE14 = '';
        reg.ATTRIBUTE15 = '';
        reg.ATTRIBUTE16 = '';
        reg.ATTRIBUTE17 = '';
        reg.ATTRIBUTE18 = '';
        reg.ATTRIBUTE19 = '';
        reg.ATTRIBUTE2 = strEndDate;
        reg.ATTRIBUTE20 = '';
        reg.ATTRIBUTE21 = '';
        reg.ATTRIBUTE22 = '';
        reg.ATTRIBUTE23 = '';
        reg.ATTRIBUTE24 = '';
        reg.ATTRIBUTE25 = '';
        reg.ATTRIBUTE26 = '';
        reg.ATTRIBUTE27 = '';
        reg.ATTRIBUTE28 = '';
        reg.ATTRIBUTE29 = '';
        reg.ATTRIBUTE3 = '';
        reg.ATTRIBUTE30 = '';
        reg.ATTRIBUTE31 = '';
        reg.ATTRIBUTE32 = '';
        reg.ATTRIBUTE33 = '';
        reg.ATTRIBUTE34 = '';
        reg.ATTRIBUTE35 = '';
        reg.ATTRIBUTE36 = '';
        reg.ATTRIBUTE37 = '';
        reg.ATTRIBUTE38 = '';
        reg.ATTRIBUTE39 = '';
        reg.ATTRIBUTE4 = '';
        reg.ATTRIBUTE41 = '';
        reg.ATTRIBUTE42 = '';
        reg.ATTRIBUTE43 = '';
        reg.ATTRIBUTE44 = '';
        reg.ATTRIBUTE45 = '';
        reg.ATTRIBUTE46 = '';
        reg.ATTRIBUTE47 = '';
        reg.ATTRIBUTE48 = '';
        reg.ATTRIBUTE49 = '';
        reg.ATTRIBUTE5 = '';
        reg.ATTRIBUTE50 = '';
        reg.ATTRIBUTE6 = '';
        reg.ATTRIBUTE7 = '';
        reg.ATTRIBUTE8 = '';
        reg.ATTRIBUTE9 = '';
        reg.PARAM_ID = regId;
        lstReg.add(reg);
        system.debug('lstReg'+lstReg);
        string strHand;
        objDocWrapper = new HO_DocGeneration.HO_DocResponse();
        try {
            strHand = objHOInvoice.HandOverInvoice('2-'+String.valueOf(System.currentTimeMillis()), reqName,'SFDC',lstReg);
            system.debug('strHand '+strHand );
            JSON2Apex objHoInvoiceResponse = new JSON2Apex ();
            objHoInvoiceResponse = (HO_DocGeneration.JSON2Apex)JSON.deserialize(strHand, HO_DocGeneration.JSON2Apex.class);
            String strResponse = strHand.substringAfter('[').substringBeforeLast(']');
            Map<String, object> mapHOInvoice = (Map<String, object>)JSON.deserializeUntyped( strResponse.replace('\\','').removeStart('"').removeEnd('"') );
                    system.debug('mapHOInvoice'+mapHOInvoice );                    
                    objDocWrapper.P_PARAM_ID= String.valueOf( mapHOInvoice.get('PARAM_ID') );
                    objDocWrapper.P_PROC_STATUS= String.valueOf( mapHOInvoice.get('PROC_STATUS') );
                    objDocWrapper.P_PROC_MESSAGE= String.valueOf( mapHOInvoice.get('PROC_MESSAGE') );
                    objDocWrapper.URL= String.valueOf( mapHOInvoice.get('ATTRIBUTE1') );
                    //objDetailsWrapper.P_ATTRIBUTE2= String.valueOf( objUnitDetails.get('ATTRIBUTE2') );
                    //objDetailsWrapper.P_ATTRIBUTE3= String.valueOf( objUnitDetails.get('ATTRIBUTE3') );
            system.debug(objDocWrapper);
        } catch (Exception e){
            objDocWrapper.P_PROC_MESSAGE = strHand;
            objDocWrapper.URL = '';
        }
        return objDocWrapper ;
    }
    
   public class JSON2Apex {
        public List<HO_DocResponse> data;
        public String message;
        public String status;
    }
    
  public class HO_DocResponse {
        public String P_PARAM_ID{get;set;}
        public String P_PROC_STATUS{get;set;}
        public String P_PROC_MESSAGE{get;set;}
        public String URL{get;set;}
    }
    
}