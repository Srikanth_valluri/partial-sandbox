/**
 * Logic class for Call Log Webservice
 */
public class CallLogLogic {

    /**
     * Method to process request and send the response
     */
    public static void processCallLogRequest() {
        CallLogResponseBody response = new CallLogResponseBody();

        // Get request body
        String requestBody = RestContext.request.requestBody.toString();
        System.debug('=========== requestBody : ' + requestBody);

        try {
            // Parse request body
            CallLogRequestBody request = (CallLogRequestBody) JSON.deserializeStrict(
                requestBody,
                CallLogRequestBody.Class
            );

            User cre = getCreForExtension(request.agentExtension);
            system.debug('cre----'+cre);
            if (cre == null) {
                response.errorMessage = 'No CRE User found for the given extension';
                RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
                Restcontext.response.statusCode = 400;
                return;
            }
            DateTime startTime = CtiTntegrationUtility.getDateTimeForUserTimeZone(request.startTime);
            DateTime endTime = CtiTntegrationUtility.getDateTimeForUserTimeZone(request.endTime);
            if (startTime == null || endTime == null ) {
                response.errorMessage = 'Please enter valid date format (DD/MM/YYYY HH:MM:SS)';
                RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
                Restcontext.response.statusCode = 400;
                return;
            }
            Call_Log__c callLog = createCallLog(cre, request, startTime, endTime);
        } catch (Exception ex) {
            response.errorMessage = ex.getMessage();
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
            Restcontext.response.statusCode = 400;
            System.debug('============= response : ' + JSON.serialize(response));
            return;
        }

        response.status = 'Call Log Created';
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
        Restcontext.response.statusCode = 200;
    }

    /**
     * Method to create call log record
     */
    public static Call_Log__c createCallLog(User cre, CallLogRequestBody callLogRequest,DateTime startTime, DateTime endTime) {
        system.debug('into call log method');
        Call_Log__c callLog = new Call_Log__c();
        callLog.CRE__c = cre.Id;
        callLog.Calling_Number__c = callLogRequest.callingNumber;
        callLog.Called_Number__c = callLogRequest.calledNumber;
        callLog.Call_Type__c = callLogRequest.callType;
        //callLog.Start_Time__c = DateTime.parse(callLogRequest.startTime);
        //callLog.End_Time__c = DateTime.parse(callLogRequest.endTime);

        /*DateTime startTime = getDateTimeForUserTimeZone(callLogRequest.startTime);
        DateTime endTime = getDateTimeForUserTimeZone(callLogRequest.endTime);
        if(startTime == null || endTime == null ) {
            return callLog;
        }*/
        callLog.Start_Time__c = startTime;
        callLog.End_Time__c = endTime;
        callLog.Termination_Party__c = callLogRequest.terminatingParty;
        callLog.Call_Recording_URL__c = callLogRequest.callRecordingUrl;
        
        // TODO
        
        if( String.isNotBlank( callLogRequest.recordId ) ) {
            Id objId = callLogRequest.recordId;
            String sObjName = objId.getsobjecttype().getDescribe().getName();
            if ( String.isNotBlank(sObjName) && sObjName.contains('Account')) {
                callLog.Account__c =  objId;
            } else if( String.isNotBlank(sObjName) && sObjName.contains('Calling') ){
                CallLog.Calling_List__c = objId;
                Calling_List__c objCL = [SELECT Account__c from Calling_List__c WHERE id =: objId LIMIT 1];
                if(objCL != NULL && objCL.Account__c != NULL)
                callLog.Account__c = objCL.Account__c;
            }
        } else {
            Account account = getAccountForCallingNumber(callLogRequest.callingNumber);
            if( account != null ){
                callLog.Account__c = account.Id;
            }
            // Associated Calling List with Call Log
            if (System.Label.Bln_Populate_Calling_List_On_Call_Log.equalsIgnoreCase('true')) {
                Calling_List__c objCallingList = getCallingListRecord(callLogRequest.callingNumber, cre.Id);
                if (objCallingList != null) {
                    CallLog.Calling_List__c = objCallingList.Id;         
                }
            }
        }
        

        
        // TODO end
        insert callLog;
        return callLog;
    }

    /**
     * Method to CRE User for the extension
     */
    public static User getCreForExtension(String extension) {
        List<User> creUsers =
            [
                SELECT
                    Id,
                    Extension
                FROM
                    User
                WHERE
                    Extension =: extension
            ];
        if (creUsers.size() > 0) {
            return creUsers[0];
        } else {
            return null;
        }
    }

    /**
    * Method to get datetime for passed string in user timezone.
    * @param: dateTimeString: Date time value in string format
    * @return: DateTime
    **/
    /*public static DateTime getDateTimeForUserTimeZone(String dateTimeString) {

        DateTime newDateTime;
        if(String.isBlank(dateTimeString)) {
            return newDateTime;
        }
        list<String> spiltStrList = dateTimeString.split(' ');
        if(spiltStrList.size() >= 2) {
            list<String> dateComponentsList = spiltStrList[0].split('/');
            list<String> timeComponentsList = spiltStrList[1].split(':');

            if(dateComponentsList.size() == 3 && timeComponentsList.size() == 3) {
                newDateTime = DateTime.newInstance(Integer.valueOf(dateComponentsList[2]),
                                                   Integer.valueOf(dateComponentsList[0]),
                                                   Integer.valueOf(dateComponentsList[1]),
                                                   Integer.valueOf(timeComponentsList[0]),
                                                   Integer.valueOf(timeComponentsList[1]),
                                                   Integer.valueOf(timeComponentsList[2]));
            }

            return newDateTime;
        }
        return newDateTime;
    }*/

    /**
     * Method get account for the calling number
     */
    public static Account getAccountForCallingNumber(String callingNumber) {
        List<Account> accounts = CtiTntegrationUtility.getRegisteredAccount(callingNumber);
        if (accounts.size() > 0) {
            return accounts[0];
        } else {
            return null;
        }
    }

    /**
     * Method to get Calling list for Call Log
     */
    public static Calling_List__c getCallingListRecord(String callingNumber, Id ownerId) {
        if (String.isNotBlank(callingNumber) && ownerId != null) {
            Map <String,Schema.RecordTypeInfo> mapCallingListRecordTypes = Calling_List__c.sObjectType.getDescribe().getRecordTypeInfosByName();
            List<Calling_List_Record_Types__c> lstCallingListRecordTypes = Calling_List_Record_Types__c.getall().values();
            Set<Id> setCallingListIds = new Set<Id>();
            List<Calling_List__c> lstCallingList = new List<Calling_List__c>();
            if (lstCallingListRecordTypes != null && lstCallingListRecordTypes.size() > 0) {
                for (Calling_List_Record_Types__c objCallingListRecordTypeName : lstCallingListRecordTypes) {
                    if (mapCallingListRecordTypes.containsKey(objCallingListRecordTypeName.Name)) {
                        setCallingListIds.add(mapCallingListRecordTypes.get(objCallingListRecordTypeName.Name).getRecordTypeId());                       
                    }
                }
            }
            
            if (setCallingListIds.size() > 0) {
                lstCallingList = [SELECT Id FROM Calling_List__c WHERE RecordTypeId In: setCallingListIds
                    AND IsHideFromUI__c = false AND Mobile_Phone__c =: callingNumber AND OwnerId =: ownerId AND Customer_Flag__c = true ORDER BY LastModifiedDate DESC LIMIT 1];
          //LastModifiedDate = Today ORDER BY LastModifiedDate DESC LIMIT 1];
            }

            if (lstCallingList.size() > 0) {
                return lstCallingList[0];
            } else {
                return null;
            }
        } else {
            return null;
        }

    }
    /**
     * Wrapper class for CallLogWebService request body
     */
    public class CallLogRequestBody {
        public String callingNumber {get; set;}
        public String calledNumber {get; set;}
        public String agentExtension {get; set;}
        public String callType {get; set;}
        public String startTime {get; set;}
        public String endTime {get; set;}
        public String terminatingParty {get; set;}
        public String terminationReason {get; set;}
        public String callRecordingUrl {get; set;}
        public String recordId {get; set;}

        public CallLogRequestBody() {
            this.callingNumber = '';
            this.calledNumber = '';
            this.agentExtension = '';
            this.callType = '';
            this.startTime = '';
            this.endTime = '';
            this.terminatingParty = '';
            this.terminationReason = '';
            this.callRecordingUrl = '';
            this.recordId = '';
        }
    }

    /**
     * Wrapper class for CallLogWebService response body
     */
    public class CallLogResponseBody {
        public String status;
        public String errorCode;
        public String errorMessage;

        public CallLogResponseBody() {
            this.status = '';
            this.errorCode = '';
            this.errorMessage = '';
        }
    }
}