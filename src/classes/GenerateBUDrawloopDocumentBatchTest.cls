@isTest
public class GenerateBUDrawloopDocumentBatchTest{
    private static testMethod void testOne(){
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
          objBookingUnit.Okay_to_release_keys__c = true;
          objBookingUnit.Keys_Released__c = true;
        }
        insert bookingUnitList;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        GenerateBUDrawloopDocumentBatch objClass = new GenerateBUDrawloopDocumentBatch(bookingUnitList[0].Id
                                                                                   , System.Label.Client_Acknowledgment_Form_DDP_Template_Id
                                                                                   , System.Label.Client_Acknowledgment_Form_DDP_Delivery_Id );
        Database.Executebatch(objClass);
        Test.stopTest();
    }
}