@isTest
public class AgentacademycontrollerTest {
    
    @isTest static void m1() {
        
        Subject__c sub = new Subject__c();
        sub.name = 'DAMAC Communities';
        sub.Duration__c = '2h';
        insert sub;
        
        Training__c train = new Training__c();
        train.Name__c = 'dubai';
        train.Date_of_Training__c = date.NewInstance(System.today().addYears(1).year(), 1,1);
        train.From_Time_Slot__c = '16:30';
        train.To_Time_Slot__c = '17:30';
        train.Subject__c = sub.id;
        train.Total_Slots__c = 10;
        insert train;
        
        test.starttest();
        Agentacademycontroller agn = new Agentacademycontroller();
        agn.selDate = train.Date_of_Training__c+'';
        agn.fetchSubjects();
        agn.selSubject = sub.Id;
        agn.fetchTimeSlots();
        try{
            agn.createAttendee();
        }catch(Exception e){
            
        }
        agn.attendee.First_name__c = 'test 1';
        agn.attendee.last_name__c = 'test 2';
        agn.attendee.Mobile_country_code__c = 'United Arab Emirates: 00971';
        agn.attendee.Mobile_number__c = '123456789';
        agn.attendee.Email__c = 'c@k.com';
        
        try{
            agn.createAttendee();
        }catch(Exception e){
            
        }
        agn.selTraining = train.id;
        
        agn.addAttendee ();
        agn.bookSession ();
        agn.fetchAvailableSlots ();
        Apexpages.currentpage().getParameters().put('index', '0');
        agn.deleteAttendee();
        
        try{
            agn.createAttendee();
        }catch(Exception e){
            
        }
        test.stoptest();
        
    }

}