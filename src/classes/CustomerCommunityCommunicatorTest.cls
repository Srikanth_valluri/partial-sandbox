@isTest
public class CustomerCommunityCommunicatorTest{

    private static final String INVITATION_TEMPLATE_NAME = 'Send Invitation Email';

    @testSetup
    static void setup() {
        insert TestUtility.createEmailTemplate(INVITATION_TEMPLATE_NAME, 'Hello');
        // inserting later to avoid setup-non-setup objects error
    }

    static testMethod void TestBulk(){
        TestUtility utility=new TestUtility();
        insert TestUtility.createCommunityLanguageSetting('en_US', INVITATION_TEMPLATE_NAME);
        List<Account> accList = utility.accInsrt(10);
        list<id> accId = new list<id>();
        for (Account acc1 : accList) {
            accId.add(acc1.id);
        }
          ApexPages.StandardController sc = new ApexPages.StandardController(accList[0]);
          ApexPages.StandardSetController setObj = new ApexPages.StandardSetController(accList);
          setObj.setSelected(accList);
        CustomerCommunityCommunicator CustomerCommunityInstance  = new CustomerCommunityCommunicator(sc);
        CustomerCommunitySendInvites CustomerCommunityObj = new CustomerCommunitySendInvites(setObj);
        
        //CustomerCommunityCommunicator.sendInvitationEmail(accId);
        CustomerCommunityInstance.getInvites();
        CustomerCommunityObj.sendInvitationEmail();
        list<Customer_Community_Language_Settings__c > mailList1=[
            Select language__c
                 , Community_Invitation_Email__c
            from Customer_Community_Language_Settings__c
            where Language__c = :UserInfo.getLanguage()
            LIMIT 1
        ];
        system.assertEquals(1,mailList1.size());
   }

}