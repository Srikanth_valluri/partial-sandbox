/*-------------------------------------------------------------------------------------------------
Description: TEst class for CallingListBookingUnitsController

    ============================================================================================================================
        Version | Date(DD-MM-YYYY) | Last Modified By | Comments
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 12-12-2017       | Lochana Rajput   | To test booking unit details for related CallingList.Case
   =============================================================================================================================
*/
@isTest
private class CallingListBookingUnitsControllerTest {

    @testSetup
    static void createSetupDate() {
        Id callingListRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Bounced Cheque').getRecordTypeId();
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Bounced Cheque SR').getRecordTypeId();
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<SR_Booking_Unit__c> lstSRBookingUnits = new List<SR_Booking_Unit__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRecordTypeId);
        insert objCase;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 5);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 10);
        lstBookingUnits[0].Registration_ID__c = '92061';
        lstBookingUnits[0].Handover_Flag__C='N';
        lstBookingUnits[0].Early_Handover__c=true;
        insert lstBookingUnits;
        lstSRBookingUnits = TestDataFactory_CRM.createSRBookingUnis(objCase.Id,lstBookingUnits);
        insert lstSRBookingUnits;
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        Test.startTest();
        List<Calling_List__c> lstCallingList = TestDataFactory_CRM.createCallingList('Bounced_Cheque',1,lstBookingUnits[0]);
        lstCallingList[0].Case__c=objCase.Id;
        insert lstCallingList[0];
    }
    @isTest static void test_BookingUnits() {
        Calling_List__c objCallingList = [SELECT Id FROM Calling_List__c LIMIT 1];
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        Test.startTest();
        PageReference pageRef = Page.CallingListBookingUnits;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(objCallingList);
        System.currentPageReference().getParameters().put('id', objCallingList.Id);
        CallingListBookingUnitsController controller = new CallingListBookingUnitsController(sc);
        Test.stopTest();
        System.assertEquals(true, controller.lstBookingWrapper.size() > 0);
    }

    @isTest static void test_generateSOA() {
        Calling_List__c objCallingList = [SELECT Id FROM Calling_List__c LIMIT 1];
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );
        Test.startTest();
        PageReference pageRef = Page.CallingListBookingUnits;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(objCallingList);
        System.currentPageReference().getParameters().put('id', objCallingList.Id);
        CallingListBookingUnitsController controller = new CallingListBookingUnitsController(sc);
        controller.regId='92061';
        controller.generateSOA();
        Test.stopTest();
        System.assertEquals('https://sftest.deeprootsurface.com/docs/e/40187735_92061_SOA.pdf', controller.soaURL);
    }

}