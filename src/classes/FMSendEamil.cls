public with sharing class FMSendEamil{//AKISHOR
    public String accId                                        {get;set;}
    public List<Account> accountList                           {get;set;}
    public List<Booking__c> bookingList                        {get;set;}
    public List<Booking_Unit__c> bookingUnitList               {get;set;}
    //public List<BUInfo> BUInfoList                             {get;set;}
    public String bookingUnitId                                {get;set;}
    public String Email                                        {get;set;}
    public String buId                                         {get;set;}
    public Boolean isRedirected                                {get;set;}
    public list<SR_Attachments__c> lstSRAttachment;
    public String url;
    public blob urlHPBody;
    public Booking_Unit__c currentBookingUnit;
    public Set<Id> bookingIdSet;
    public boolean blnFMUser {get;set;}

    public FMSendEamil(ApexPages.StandardController controller) {
        init();

    }

    public void init(){
        isRedirected = false;
        url = null;
        urlHPBody = null;
        accountList = new List<Account>();
        bookingList = new List<Booking__c>();
        bookingUnitList = new List<Booking_Unit__c>();
        //BUInfoList = new List<BUInfo>();
        lstSRAttachment = new list<SR_Attachments__c>();
        currentBookingUnit = new Booking_Unit__c();
        bookingIdSet = new Set<Id>();
        blnFMUser = false;

        /*accId = ApexPages.currentPage().getParameters().get('id');
        System.debug('before accId= ' + accId);
        accId = String.valueOf(accId).substring(0, 15);
        System.debug('after accId= ' + accId);*/

  }
    /*public void SendEmail(blob file, String email, String accName, String soaType) {
        //New instance of a single email message
        Messaging.SingleEmailMessage mail =
                    new Messaging.SingleEmailMessage();

        // The email template ID used for the email
        //mail.setTemplateId('00X30000001GLJj');
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'no-replysf@damacgroup.com'];

        List<String> sendTo = new List<String>();
        if(email !='' || email != null) {
          sendTo.add(email);
        }
        if ( owea.size() > 0 ) {
            mail.setOrgWideEmailAddressId(owea.get(0).Id);
        }

        mail.setSubject('Statement of Account Generated.');
        mail.setToAddresses(sendTo);
        mail.setBccSender(false);
        mail.setUseSignature(false);
        //mail.setReplyTo('test123@acme.com');
        mail.setSenderDisplayName('DAMAC');
        mail.setSaveAsActivity(false);

        String body = 'Dear ' + accName;
        body += '\n Please find attached Statement of Account. ';
        body += '\n \n Thanks & Regards,';
        body += '\n SalesForce Team';
        mail.setHtmlBody(body);

        List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
        Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
        efa.setFileName(soaType);
        efa.setBody(file);
        fileAttachments.add(efa);
        mail.setFileAttachments(fileAttachments);

        try {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            System.debug('===========Mail sent====');
        } catch(Exception e) {
            System.debug('======Error in email sending=========' + e);
        }
    }*/

    public pagereference redirect() {
        accId = ApexPages.currentPage().getParameters().get('id');
        System.debug('==In redirect method====accId=========' + accId);
        String personEmail;String NewAcId;
        pagereference newpg;//String AcId='0010Y00000MaKkw';
        isRedirected = true;
        for (Abandoned_Calls__c FMCalls: [Select Id, Email__c,Account__c From Abandoned_Calls__c Where Id =: accId]){
            if (FMCalls.Email__c == null) {
              //  newpg = new PageReference (Label.Email_send_URL+'/_ui/core/email/author/EmailAuthor?p2_lkid='+accId+'&rtype=003&p3_lkid='+accId+'&retURL=' + accId );
                newpg = new PageReference (Label.Email_send_URL+'/_ui/core/email/author/EmailAuthor?p2_lkid='+FMCalls.Account__c+'&rtype=003&p3_lkid='+FMCalls.Account__c+'&p5='+''+'&retURL=' + accId);
                System.debug('newpg::::'+newpg);

            } else if (FMCalls.Email__c != null) {
                //newpg = new PageReference (Label.Email_send_URL+'/_ui/core/email/author/EmailAuthor?p2_lkid='+accId+'&rtype=003&p3_lkid='+accId+'&retURL='+accId);
               newpg = new PageReference (Label.Email_send_URL+'/_ui/core/email/author/EmailAuthor?p2_lkid='+FMCalls.Account__c+'&rtype=003&p3_lkid='+FMCalls.Account__c+'&p5='+''+'&retURL=' + accId);
            System.debug('newpg:::222:'+newpg);
            }
        }

        newpg.setRedirect(false);
        return newpg;
    }
}