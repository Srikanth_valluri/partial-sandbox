/**************************************************************************************************
* Name               : SendEmailToPromotorForInquiriesScheduler                                   *
* Description        : Scheduler class for SendEmailToPromotorForInquiries  batch class.          *
* Created Date       : 24/07/2018                                                                 *                                                                       *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR            DATE            COMMENTS                                          *
* 1.0         Tejas Naik      24/07/2018      Initial Draft.                                    *
**************************************************************************************************/
public class SendEmailToPromotorForInquiriesScheduler implements Schedulable {
  public void execute(SchedulableContext SC) {
    SendEmailToPromotorForInquiries  schduleobj = new SendEmailToPromotorForInquiries(System.now());
    Database.executeBatch(schduleobj); 
  }
}// End of class.