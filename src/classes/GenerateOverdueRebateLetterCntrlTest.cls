/**
 * Description: Test class for GenerateOverdueRebateLetterCntrl 
 */
@isTest
private class GenerateOverdueRebateLetterCntrlTest {

    /*
     * Description : Test method to check whether the batch is called or not which generates the Drawloop document.
     */
    @isTest static void testMethod1() {
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
            objBookingUnit.Unit_Name__c = 'ABZ/12/20';
          objBookingUnit.Okay_to_release_keys__c = true;
          objBookingUnit.Keys_Released__c = true;
        }
        insert bookingUnitList;

        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c(Name='Overdue-Basswood and Albizia', Delivery_Option_Id__c='dsandkjnd', Document_Name__c='test', Drawloop_Document_Package_Id__c='dcnccmc');
        Case objCase = TestDataFactory_CRM.createCase( objAccount.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Overdue Rebate/Discount').getRecordTypeId() );
        objCase.OwnerId = UserInfo.getUserId();
        objCase.Booking_Unit__c = bookingUnitList[0].Id;
        insert objCase ;
        
        Task objTask = TaskUtility.getTask((SObject)objCase , 'Generate Offer Letter and share with Customer', 'CRE',
                                'Overdue Amount Rebate', system.today().addDays(1));
        insert objTask;
        
        PageReference redirectionUrl;
        Test.startTest();
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(objCase);
            GenerateOverdueRebateLetterCntrl objController = new GenerateOverdueRebateLetterCntrl(standardControllerInstance);
            Test.setCurrentPageReference(new PageReference('Page.GenerateOverdueRebateLetter'));
            System.currentPageReference().getParameters().put('Id', objCase.Id);
            objController.callDrawloop();
            objController.returnToCase();
        Test.stopTest();


    }
    
    @isTest static void testMethod2() {
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
     
        insert bookingUnitList;

        Case objCase = TestDataFactory_CRM.createCase( objAccount.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Overdue Rebate/Discount').getRecordTypeId() );
        objCase.OwnerId = UserInfo.getUserId();
        
        insert objCase ;
        
        Task objTask = TaskUtility.getTask((SObject)objCase , 'Generate Offer Letter and share with Customer', 'CRE',
                                'Overdue Amount Rebate', system.today().addDays(1));
        //insert objTask;

        PageReference redirectionUrl;
        Test.startTest();
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(objCase);
            GenerateOverdueRebateLetterCntrl objController = new GenerateOverdueRebateLetterCntrl(standardControllerInstance);
            Test.setCurrentPageReference(new PageReference('Page.GenerateOverdueRebateLetter'));
            System.currentPageReference().getParameters().put('Id', objCase.Id);
            objController.executeBatch();
        Test.stopTest();

    }

    @isTest static void testMethod3() {
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
          objBookingUnit.Is_Client_Acknowledgment_Form_Generated__c  = true;
        }
        insert bookingUnitList;

        Case objCase = TestDataFactory_CRM.createCase( objAccount.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId() );
        objCase.OwnerId = UserInfo.getUserId();
        
        insert objCase ;
        
        Task objTask = TaskUtility.getTask((SObject)objCase , 'Generate Offer Letter and share with Customer', 'CRE',
                                'Overdue Amount Rebate', system.today().addDays(1));
        insert objTask;

        PageReference redirectionUrl;
        Test.startTest();
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(objCase);
            GenerateOverdueRebateLetterCntrl objController = new GenerateOverdueRebateLetterCntrl(standardControllerInstance);
            Test.setCurrentPageReference(new PageReference('Page.GenerateOverdueRebateLetter'));
            System.currentPageReference().getParameters().put('Id', objCase.Id);
            objController.executeBatch();
        Test.stopTest();

    }

}