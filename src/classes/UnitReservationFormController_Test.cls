/************************************************************************************************
 * @Name              : UnitReservationFormController_Test
 * @Description       : Test Class for UnitReservationFormController
 * Modification Log
 * 1.0    QBurst    29/01/2020        Created Class
 ************************************************************************************************/


@isTest
private class UnitReservationFormController_Test {

    @isTest static void UnitReservationPDFTest() {
        Inventory__c inventoryObj = InitialiseTestData.getInventoryDetails('INV-001','123','ACZ11',123,123);
        inventoryObj.Project_Category__c = 'VILLA';
        inventoryObj.IPMS_Bedrooms__c = 'Studio';
        insert inventoryObj;
        
        Inventory__c invenobj = InitialiseTestData.getInventoryDetails('INV-002','123','ACZ11',123,123);
        invenobj.Property_Type__c = 'Villa';
        invenobj.IPMS_Bedrooms__c = 'Plot';
        insert invenobj;
        
        Test.startTest();
        PageReference pageRef = Page.UnitReservationFormPDF;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('recId', String.valueOf(inventoryObj.Id));
        ApexPages.StandardController inventoryStd = new ApexPages.StandardController(inventoryObj);         
        UnitReservationFormController inventory = new UnitReservationFormController(inventoryStd);
        
        pageRef.getParameters().put('recId', String.valueOf(invenObj.Id));
        ApexPages.StandardController inventoryStd2 = new ApexPages.StandardController(invenObj);         
        UnitReservationFormController inventory2 = new UnitReservationFormController(inventoryStd2);
        
        
        inventoryObj.Project_Category__c = 'MID-RISE';
        inventoryObj.IPMS_Bedrooms__c = '1';
        update inventoryObj;
        UnitReservationFormController inventory3 = new UnitReservationFormController(inventoryStd);

        invenobj.Property_Type__c = 'Serviced Apartments';
        invenobj.IPMS_Bedrooms__c = '2 BR';
        update invenobj;
        UnitReservationFormController inventory4 = new UnitReservationFormController(inventoryStd2);

        Test.stopTest();
    }
}