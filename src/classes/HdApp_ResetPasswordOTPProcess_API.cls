/**********************************************************************************************************************
Description: This API is used for sending OTP to customer before resetting password from Damac living app & also 
             verfying the OTP entere.
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   12-01-2020      | Shubham Suryawanshi | Created initial draft
***********************************************************************************************************************/
@RestResource(urlMapping='/resetPasswordOTP/*')
global class HdApp_ResetPasswordOTPProcess_API {

    /*Do not move in constant class*/
    public static final Integer otpTimeOut= Integer.valueof(label.Timeout_duration_for_Reset_Password_OTP_verification);
    public static List<EmailMessage> lstEmailMsg = new List<EmailMessage>();
    /***********************************************************************************************
    Method Name : sendOTPtoCustomer
    Description :This method is used for sending OTP to user
    Parameter(s): None
    Return Type : FinalReturnWrapper (Wrapper)
    ************************************************************************************************/
    @HttpGet
    global static FinalReturnWrapper sendOTPtoCustomer() {

        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        HDApp_Utility.cls_meta_data objMeta = new HDApp_Utility.cls_meta_data();
        cls_data objData = new cls_data();
        
        RestRequest r = RestContext.request;
        System.debug('Request params:' + r.params);
        if(!r.requestURI.equalsIgnoreCase('/resetPasswordOTP/sendOTP')) {
            objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.commonErrorMsg,HDApp_Constants.INVALID_RESET_SEND_OTP_ENDPOINT,6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        if(!r.params.containsKey('username')) {
            objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.commonErrorMsg,HDApp_Constants.NO_USERNAME_ATTR, 8);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(r.params.containsKey('username') && String.isBlank(r.params.get('username'))) {
            objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.NO_USERNAME_PASSED,HDApp_Constants.NO_USERNAME_PASSED,8);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        System.debug('username: ' + r.params.get('username'));
        List<User> userList = HDApp_Utility.getUserDetailsFromUsername(r.params.get('username'));
        
        if( userList.isEmpty() || userList.size() < 1 ) {
            objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.INVALID_USERNAME,HDApp_Constants.INVALID_USERNAME,8);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        //Main logic
        Boolean isSandbox = [SELECT IsSandbox FROM Organization].IsSandbox;
        System.debug('IsSandbox is: '+isSandbox);
        String otpCode = String.valueOf((Math.random() * 10000).intValue()).leftPad(4, '0');
        System.debug('otpCode: ' + otpCode);

        try{
            // Logic for checking if user is blocked due to multiple OTP requests
            Boolean isBlockedForOTPRequest = isUserBlockedForOTPReq(userList[0].Account);
            if(isBlockedForOTPRequest) {
                 //Updating Account when not making callouts;
                update userList[0].Account;
                returnResponse.meta_data = HDApp_Utility.ReturnMetaResponse(String.format(HDApp_Constants.TOO_MANY_OTP_REQ_ATTEMPTS, new List<String> {Label.HDApp_Locking_Time_After_OTP_Requests_Exceeds}), '', 12);
                return returnResponse;
            }
            String returnedGuid = SendOTPviaEmailandSMS(otpCode, userList[0].Account);
            System.debug('returnedGuid:: ' + returnedGuid);

            //Updating Account after callouts;
            update userList[0].Account;
            //Masking of emailId & mobile Number
            String EmailAddress = userList[0].Account.IsPersonAccount ? userList[0].Account.Email__pc : userList[0].Account.Email__c;
            String maskedEmailId = EmailAddress.replaceAll('(^[^@]{2}|(?!^)\\G)[^@]', '$1x');

            String mobileNumber = userList[0].Account.IsPersonAccount ? userList[0].Account.Mobile_Phone_Encrypt__pc : userList[0].Account.Mobile_Phone_Encrypt__c;
            String maskedMobileNo = 'xxxxxxx' + mobileNumber.right(3);
            //Masking end
            List<String> emailAndMobile = new List<String> {maskedEmailId, maskedMobileNo};
            String success_msg = String.format(HDApp_Constants.SUCCESSFULLY_OTP_SENT, emailAndMobile);
            String dev_msg = isSandbox ? 'otpcode is : '+otpCode +'. ' : null;
            objMeta = HDApp_Utility.ReturnMetaResponse(success_msg, dev_msg, 1);
            objData.guid = returnedGuid;
        }
        catch(exception ex) {
            objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg, ex.getMessage(), 6);
            returnResponse.meta_data = objMeta;
        }
        returnResponse.meta_data = objMeta;
        returnResponse.data = objData;
        System.debug('returnResponse: ' + returnResponse);
        return returnResponse;
    }

    /***********************************************************************************************
    Method Name : SendOTPviaEmailandSMS
    Description :This method is used for sending OTP to user via Email & SMS
    Parameter(s): String otpCode, Account account
    Return Type : String
    ************************************************************************************************/
    public static String SendOTPviaEmailandSMS(String otpCode, Account account) {
        String returnGUID = '';
        String otpEmailResoponse = sendOTP_Email( otpCode, account ); 
        String smsOtpResponse = sendOTP_SMS( otpCode, account);

        if(lstEmailMsg.size() > 0) {
            insert lstEmailMsg;//To avoid uncommitted work pending issue;
        }
        if(otpEmailResoponse == 'success' || smsOtpResponse == 'success') {
            //Insert OTP with GUID
            String generatedGUID = HDApp_Utility.getGUID();
            returnGUID = insertOTP(otpCode, generatedGUID, account);
        }
        return returnGUID;
    }

    /***********************************************************************************************
    Method Name : sendOTP_Email
    Description :This method is used for sending OTP to user via Email
    Parameter(s): String otpCode, Account account
    Return Type : String
    ************************************************************************************************/
    public static String sendOTP_Email( String otpCode, Account account ) {
        List<OrgWideEmailAddress> orgWideAddressHelloDamac = [
            SELECT  Id
                    , Address
                    , DisplayName
            FROM    OrgWideEmailAddress
            WHERE   Id = :Label.CommunityPortalOrgWideAddressId
        ];

        String emailId = account.IsPersonAccount ? account.Email__pc : account.Email__c;
        String emailSubject = HDApp_Constants.RESET_PASSWORD_OTP_EMAIL_SUBJECT;

        // String emailBody = 'Hello ' + account.name + ',';
        // emailBody += '\n\n' + otpCode + ' is the code for verifying your account';
        String emailBody = HDApp_Constants.RESET_PWD_EMAIL_BODY.replace('{accountName}', account.name);
        emailBody = emailBody.replace('{otp}', otpCode);

        SendGridEmailService.SendGridResponse response = SendGridEmailService.sendEmailService(
            emailId, account.name, '', '', '', '', emailSubject, '',
            orgWideAddressHelloDamac[0].Address, orgWideAddressHelloDamac[0].DisplayName, '', '', 'text/plain',
            emailBody, '', new List<Attachment>()
        );

        String sendGridRes = Test.isRunningTest() ? 'Accepted' : response.ResponseStatus;

        if(sendGridRes == 'Accepted') {
            EmailMessage mail = new EmailMessage();
            mail.Subject = emailSubject;
            mail.MessageDate = System.Today();
            mail.Status = '3';
            mail.RelatedToId = account.Id;//Put FM case id
            mail.ToAddress = emailId;
            mail.FromAddress = orgWideAddressHelloDamac[0].Address;
            mail.TextBody = emailBody;
            mail.CcAddress = '';
            mail.BccAddress = '';
            mail.Sent_By_Sendgrid__c = true;
            mail.SentGrid_MessageId__c = response.messageId;
            //mail.Booking_Unit__c = account.Booking_Unit__c;
            mail.Account__c = account.Id;
            //insert mail;
            lstEmailMsg.add(mail);
            return 'success';
        }
        else {
            Error_Log__c objError = new Error_Log__c();
            objError.Account__c = account.id;
            objError.Error_Details__c = 'Reset Password Email OTP Verification request failed from SendGrid';
            objError.Process_Name__c = 'Generic Email';
            insert objError;
            return 'Reset Password Email OTP Verification request failed from SendGrid service callout';
        }
        //Email end
    }

    /***********************************************************************************************
    Method Name : sendOTP_SMS
    Description :This method is used for sending OTP to user via SMS
    Parameter(s): String otpCode, Account account
    Return Type : String
    ************************************************************************************************/
    public static String sendOTP_SMS( String otpCode, Account account ) {
        String returnMsg = '';
        String phoneNumber = account.IsPersonAccount ? account.Mobile_Phone_Encrypt__pc : account.Mobile_Phone_Encrypt__c;
        System.debug('*****before removing PhoneNumber*****'+phoneNumber);
        String smsBody = HDApp_Constants.RESET_PWD_SMS_BODY.replace('{otp}', otpCode);
        //Calling SMS service (DAMAC CRM)
        String smsServiceResponse = sendSMSService(phoneNumber, smsBody);
        //POst SMS service :
        if(smsServiceResponse != null){
            SMS_History__c verificationSms = new SMS_History__c();
            verificationSms.Customer__c = account.Id;
            verificationSms.Message__c = HDApp_Constants.RESET_PWD_SMS_BODY.replace('{otp}', otpCode);
            verificationSms.Phone_Number__c = phoneNumber;
            verificationSms.Name = 'OTP for reset password Damac Living App';
            verificationSms.Description__c = smsServiceResponse;
            verificationSms.Is_SMS_Sent__c = smsServiceResponse.contains('OK:') ? true : false;
            insert verificationSms;
            returnMsg = 'success';
        }
        else {
            returnMsg = 'SMS service failed to send message';
        }
        return returnMsg;
        //Earlier Logic
        // SMS_History__c verificationSms = new SMS_History__c();
        // verificationSms.Customer__c = account.Id;
        // //verificationSms.Message__c = otpCode + ' is the code for verifying your mobile number.';
        // verificationSms.Message__c = HDApp_Constants.RESET_PWD_SMS_BODY.replace('{otp}', otpCode);
        // verificationSms.Phone_Number__c = account.IsPersonAccount ? account.Mobile_Phone_Encrypt__pc : account.Mobile_Phone_Encrypt__c;
        // verificationSms.Name = 'OTP for reset password Damac Living App';
        // if(Test.isRunningTest()) {}
        // else {
        //     //System.debug('verificationSms :'+verificationSms);
        //     try {
        //         System.debug('verificationSms: ' + verificationSms);
        //         insert verificationSms;
        //         returnMsg = 'success';
        //     }
        //     catch(exception e) {
        //         returnMsg = e.getMessage();
        //     }
        // }
        // return returnMsg;
        // SMS end
    }

    /***********************************************************************************************
    Method Name : sendSMSService
    Description : This method contains the callout method for sending sms via. Damac CRM Account 
    Parameter(s): String phoneNumber, String smsBody
    Return Type : String
    ************************************************************************************************/
    public static String sendSMSService(String phoneNumber, String smsBody) {
    
        if(phoneNumber.startsWith('00')) {
            phoneNumber = phoneNumber.removeStart('00');
        }else if(phoneNumber.startsWith('0')) {
            phoneNumber = phoneNumber.removeStart('0');
        }
        system.debug('*****before removing phoneNumber*****'+phoneNumber);
        //
        String responseMsg = '';
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http(); 
        String user= Label.Damac_CRM_SMS_Service_User_name;
        String passwd= Label.Damac_CRM_SMS_Service_Password;
        String strSID = SMSClass.getSenderName(user, phoneNumber, false);
        String smsMessage = GenericUtility.encodeChar(smsBody); 
    
        req.setMethod('POST' ); // Method Type
        req.setEndpoint('https://api.smscountry.com/SMSCwebservice_bulk.aspx');
        //verificationSms.Message__c = HDApp_Constants.RESET_PWD_SMS_BODY.replace('{otp}', otpCode);
        req.setBody('user='+ user + '&passwd=' + passwd +'&message=' + smsMessage + '&mobilenumber=' 
                + phoneNumber + '&sid=' + strSID + '&MTYPE=LNG&DR=Y'); // Request Parameters
        try {
            system.debug('Request Body---'+req.getBody());
            system.debug('Request Header---'+req.getHeader('Authorization'));
            system.debug('Request Header---'+req.getHeader('Content-Type'));
            res =  !Test.isRunningTest() ? http.send(req) : new HttpResponse();
            system.debug('Response---'+res.getBody());
            responseMsg = !Test.isRunningTest() ? res.getBody() : 'OK: 1234';
        }
        catch(Exception e) {
            responseMsg = e.getMessage();
        }
        return responseMsg;
    }
    
    
    /***********************************************************************************************
    Method Name : insertOTP
    Description :This method is used for creating OTP__c record
    Parameter(s): String otpCode, String guid, Account account
    Return Type : String
    ************************************************************************************************/
    public static String insertOTP(String otpCode, String guid, Account account) {
        //Logic to expire existing OTPs
        List<OTP__c> existingOTPs = new List<OTP__c>();
        for(OTP__c objOTP : [SELECT id
                                  , isExpired__c
                            FROM OTP__c
                            WHERE Account__c =: account.id
                            AND FM_Process_Name__c =: HDApp_Constants.RESET_PASSWORD_PROCESS
                            AND isExpired__c = false ]) {

            objOTP.isExpired__c = true;
            existingOTPs.add(objOTP);
        }                             
        System.debug('existingOTPs after marking true: ' + existingOTPs);
        if(existingOTPs.size() > 0) {
            update existingOTPs;
        }
        
        OTP__c objOTP = new OTP__c();
        objOTP.GUID__c = guid;
        objOTP.FM_SMS_OTP__c = otpCode;
        objOTP.FM_Email_OTP__c = otpCode;
        objOTP.FM_Process_Name__c = HDApp_Constants.RESET_PASSWORD_PROCESS;
        objOTP.Account__c = account.id;
        objOTP.OTP_Number__c = otpCode;

        System.debug('objOTP: ' + objOTP);
        insert objOTP;

        return guid;
    }

    /***********************************************************************************************
    Method Name : verifyUserOTP
    Description :This method is used for sending OTP to user
    Parameter(s): None
    Return Type : FinalReturnWrapper (Wrapper)
    ************************************************************************************************/
    @HttpPost    
    global static FinalReturnWrapper verifyUserOTP() {

        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        HDApp_Utility.cls_meta_data objMeta = new HDApp_Utility.cls_meta_data();
        cls_data objData = new cls_data();
        
        RestRequest r = RestContext.request;
        System.debug('Request params:' + r.params);
        if(!r.requestURI.equalsIgnoreCase('/resetPasswordOTP/verifyotp')) {
            objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.commonErrorMsg, HDApp_Constants.INVALID_RESET_VERIFY_OTP_ENDPOINT, 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        if(!r.params.containsKey('username')) {
            objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.commonErrorMsg,HDApp_Constants.NO_USERNAME_ATTR, 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(r.params.containsKey('username') && String.isBlank(r.params.get('username'))) {
            objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.commonErrorMsg,HDApp_Constants.NO_USERNAME_PASSED,6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        //Checking user's status
        List<User> userList = HDApp_Utility.getUserDetailsFromUsername(r.params.get('username'));
        if( userList.isEmpty() || userList.size() < 1 ) {
            objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.INVALID_USERNAME,HDApp_Constants.INVALID_USERNAME,8);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        // Logic for checking if user is blocked due to multiple OTP requests
        Boolean isBlockedForOTPRequest = isUserBlockedForOTPReq(userList[0].Account);
        if(isBlockedForOTPRequest) {
            //update userList[0].Account;
            returnResponse.meta_data = HDApp_Utility.ReturnMetaResponse(String.format(HDApp_Constants.TOO_MANY_OTP_REQ_ATTEMPTS, new List<String> {Label.HDApp_Locking_Time_After_OTP_Requests_Exceeds}), '', 12);
            return returnResponse;
        }
        //Checking user's status - END
        if(!r.params.containsKey('guid')) {
            objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.commonErrorMsg, HDApp_Constants.NO_GUID_ATTR, 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(r.params.containsKey('guid') && String.isBlank(r.params.get('guid'))) {
            objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.REQUEST_NEW_OTP, HDApp_Constants.NO_GUID_PASSED, 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        if(!r.params.containsKey('otp')) {
            objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.commonErrorMsg, HDApp_Constants.NO_OTP_ATTR, 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(r.params.containsKey('otp') && String.isBlank(r.params.get('otp'))) {
            objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.NO_OTP_PASSED, HDApp_Constants.NO_OTP_PASSED, 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        
        System.debug('otp: '+r.params.get('otp'));
        System.debug('username: ' + r.params.get('username'));
        //This is done above
        // List<User> userList = HDApp_Utility.getUserDetailsFromUsername(r.params.get('username'));
        
        // if( userList.isEmpty() || userList.size() < 1 ) {
        //     objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,HDApp_Constants.INVALID_USERNAME,6);
        //     returnResponse.meta_data = objMeta;
        //     return returnResponse;
        // }
        List<OTP__c> otpRecordList = getOTPfromGUID(r.params.get('guid'), userList[0].Account.id, false, false);
        if( otpRecordList.isEmpty() || otpRecordList.size() < 1 ) {
            objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,HDApp_Constants.INVALID_GUID,6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        Boolean isOTPValid = checkOTPValidity(otpRecordList[0], otpTimeOut);
        if(!isOTPValid) {
            objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.OTP_VERIFICATION_TIMEOUT, HDApp_Constants.OTP_VERIFICATION_TIMEOUT, 9);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        //check OTP
        if( otpRecordList[0].OTP_Number__c == r.params.get('otp') ) {
            //success
            otpRecordList[0].isVerified__c = true;
            update otpRecordList;   //Expirying verified OTP after its usage;
            objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.OTP_VERIFICATION_SUCCESS,'', 1);
            objData.guid = otpRecordList[0].GUID__c;
        }
        else {
            objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.WRONG_OTP, HDApp_Constants.WRONG_OTP, 2);
        }
        returnResponse.meta_data = objMeta;
        returnResponse.data = objData;
        System.debug('returnResponse in verifyOTP : ' + returnResponse );
        return returnResponse;
    }

    /***********************************************************************************************
    Method Name : getOTPfromGUID
    Description : This method is used for fetching OTP record grom guid
    Parameter(s): String inputGuid, String accountId
    Return Type : List<OTP__c>
    ************************************************************************************************/
    public static List<OTP__c> getOTPfromGUID(String inputGuid, String accountId, boolean isVerified, boolean isExpired) {
        List<OTP__c> otpRecord = [SELECT id
                                       , FM_Email_OTP__c
                                       , FM_Process_Name__c
                                       , FM_SMS_OTP__c
                                       , OTP_Number__c
                                       , GUID__c    
                                       , isExpired__c
                                       , isVerified__c
                                       , CreatedDate
                                  FROM OTP__c
                                  WHERE GUID__c =:inputGuid
                                  AND Account__c =: accountId
                                  AND FM_Process_Name__c =: HDApp_Constants.RESET_PASSWORD_PROCESS
                                  AND isExpired__c =: isExpired
                                  AND isVerified__c =: isVerified
                                  ORDER BY CreatedDate DESC
                                  LIMIT 1];
        System.debug('otpRecord: ' + otpRecord);
        return otpRecord;
    }

    /***********************************************************************************************
    Method Name : checkOTPValidity
    Description : This method is used for verifying if OTP is valid or not
    Parameter(s): OTP__c objOTP, Integer otpTimeOut
    Return Type : boolean
    ************************************************************************************************/
    public static boolean checkOTPValidity(OTP__c objOTP, Integer otpTimeOut) {
        //If OTP createdDate is more than past 5 mins, than its expired
        // check 5 minutes validity
         Boolean isValid = false;
         /*Minutes check*/
            System.debug('otpTimeOut:: ' + otpTimeOut);
            Datetime timeValid = objOTP.CreatedDate.addMinutes(otpTimeOut);
            System.debug('timeValid: ' + timeValid);
            System.debug('now: ' + Datetime.now());
            isValid = Datetime.now() <= timeValid ? true : false;
            System.debug('isValid :: ' + isValid);
         /*Minutes check end*/
         return isValid;
    }

    /***********************************************************************************************
    Method Name : isUserBlockedForOTPReq
    Description : This method is used for verifying if user has attempted excess OTP requestes
    Parameter(s): Account 
    Return Type : boolean
    ************************************************************************************************/
    public static boolean isUserBlockedForOTPReq(Account account) {
        System.debug('account: ' + account);

        if(!account.Is_Blocked_for_Change_Password_OTP__c) {
            System.debug('time now: ' + datetime.now());
            DateTime past5Mins = datetime.now().addMinutes(-5);
            System.debug('past5Mins : ' + past5Mins);

            List<OTP__c> invalidOTPs = new List<OTP__c>();
            for(OTP__c objOTP : [SELECT id
                                      , FM_Email_OTP__c
                                      , FM_Process_Name__c
                                      , FM_SMS_OTP__c
                                      , OTP_Number__c
                                      , GUID__c    
                                      , isExpired__c
                                      , isVerified__c
                                      , CreatedDate
                                 FROM OTP__c
                                 WHERE Account__c =: account.Id
                                 AND FM_Process_Name__c =: HDApp_Constants.RESET_PASSWORD_PROCESS
                                 AND isExpired__c = true
                                 AND CreatedDate >: past5Mins
                                 ORDER BY CreatedDate ASC]) {
                System.debug('objOTP: ' + objOTP);                                     
                if(objOTP.isVerified__c) {
                    invalidOTPs.clear();
                }
                else {
                    invalidOTPs.add(objOTP);
                }
            }
            System.debug( 'invalidOTPs.size(): '+ invalidOTPs.size() + 'invalidOTPs: ' + invalidOTPs);
            if(invalidOTPs.size() > 3) {   //3 - because, in first 2 transactions the  unxpired OTPs a re present
                account.Is_Blocked_for_Change_Password_OTP__c = true;
                account.Last_Change_Password_OTP_Time__c = Datetime.now();
                return true;  //blocked
            }
            else {
                return false; //un-blocked
            }
        }

        if(account.Is_Blocked_for_Change_Password_OTP__c) {
            if(account.Is_Blocked_for_Change_Password_OTP__c && account.Last_Change_Password_OTP_Time__c != null && account.Last_Change_Password_OTP_Time__c.addMinutes(Integer.valueOf(Label.HDApp_Locking_Time_After_OTP_Requests_Exceeds)) < Datetime.now()) {
                //release lock
                account.Is_Blocked_for_Change_Password_OTP__c = false;
                account.First_Change_Password_OTP_Time__c = null;
                account.Last_Change_Password_OTP_Time__c = null;
                account.Change_Password_OTP_Attempts__c = 1; //as this transaction will be successfull so considering it 1;
                //update account;
                return false;// Not-blocked
            }
            else {
                return true;// blocked
            }
        }
        else if(account.First_Change_Password_OTP_Time__c != null && account.First_Change_Password_OTP_Time__c.addMinutes(Integer.ValueOf(Label.HDApp_Active_Time_for_Change_Password_OTP_Requests)) > Datetime.now() && account.Change_Password_OTP_Attempts__c != null && account.Change_Password_OTP_Attempts__c > Integer.valueOf(Label.HDApp_Max_Passwd_Reset_OTP_Requests) ) {
            account.Is_Blocked_for_Change_Password_OTP__c = true;
            account.Last_Change_Password_OTP_Time__c = Datetime.now(); 
            //update account;
            return true;// blocked
        }
        else {
            //fetch last non-expired & non-verified OTP for its timestamp;
            List<OTP__c> lastOTP =   [SELECT id
                                            , FM_Email_OTP__c
                                            , FM_Process_Name__c
                                            , FM_SMS_OTP__c
                                            , OTP_Number__c
                                            , GUID__c    
                                            , isExpired__c
                                            , isVerified__c
                                            , CreatedDate
                                    FROM OTP__c
                                    WHERE Account__c =: account.Id
                                    AND FM_Process_Name__c =: HDApp_Constants.RESET_PASSWORD_PROCESS
                                    AND isExpired__c = false
                                    AND isVerified__c = false
                                    ORDER BY CreatedDate DESC
                                    LIMIT 1];
            if(lastOTP.size() > 0 && lastOTP[0].createdDate.addMinutes(Integer.valueOf(Label.HDApp_Active_Time_for_Change_Password_OTP_Requests)) > Datetime.now()) {
                // if( account.First_Change_Password_OTP_Time__c != null && account.First_Change_Password_OTP_Time__c != lastOTP[0].CreatedDate ) {
                //     account.First_Change_Password_OTP_Time__c = lastOTP[0].CreatedDate;
                //     account.Last_Change_Password_OTP_Time__c = null;
                // }
                // else {
                //     account.First_Change_Password_OTP_Time__c = Datetime.now();
                //     account.Last_Change_Password_OTP_Time__c = null;
                // }
                account.First_Change_Password_OTP_Time__c = lastOTP[0].CreatedDate;
                account.Last_Change_Password_OTP_Time__c = null;
            }                                 
            else {
                account.First_Change_Password_OTP_Time__c = Datetime.now();
                account.Last_Change_Password_OTP_Time__c = null;
            }
            if( account.Change_Password_OTP_Attempts__c == null ) {
                account.Change_Password_OTP_Attempts__c = 0;
            }
            account.Change_Password_OTP_Attempts__c += 1;
            //update account;
            return false; //Not-blocked
        }
    }

    /***********************************************************************************************
    Description : This is return response wrapper class declaration
    ************************************************************************************************/    
    global class FinalReturnWrapper {
        public HDApp_Utility.cls_meta_data meta_data;
        public cls_data data;
    }

    public class cls_data {
        public String guid; 
    }
}