global class Damac_RetrySMSBatchSchedule implements Schedulable{
    global void execute(SchedulableContext ctx) {
        Database.executeBatch(new Damac_RetrySMSBatch ('', 'Stand_inquiry__c', ''), 10);
    }

}