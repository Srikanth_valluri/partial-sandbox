/****************************************************************************************************
* Name               : AssociatePaymentPlanFloorController
* Description        : Controller class for Associate Payment Plan VF Page
* Created Date       : 16/10/2020
* Created By         : QBurst
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst         16/10/2020      Initial Draft
****************************************************************************************************/

    global with sharing class AssociatePaymentPlanFloorController {
    public String buildingId {get; set;}
    public Location__c building {get; set;}
    public static string floorDetails {get;set;}
    /*********************************************************************************************
    * @Description : Constructor method
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public AssociatePaymentPlanFloorController() {
        buildingId = ApexPages.currentPage().getParameters().get('Id');
        List<FloorDetailsWrapper> wrapperList = new List<FloorDetailsWrapper>();
        floorDetails = JSON.serialize(wrapperList);
        if(buildingId != null && buildingId != ''){
            building = [SELECT Id, Name, Building_Name__c, Location_ID__c,
                                Property_Name__r.Property_Name__c, Property_Name__c
                               FROM Location__c 
                               WHERE Location_Type__c = 'Building'
                               AND Id =: buildingId];    
            fetchFloorDetails();
        }
    }

    public void fetchFloorDetails(){
         Map<Id, Location__c> locationMap = new Map<Id, Location__c>();
         Map<Id, Boolean> locationPayPlanMap = new Map<Id, Boolean>();
         Map<Id, List<Inventory__c>> locationInventoryMap = new Map<Id, List<Inventory__c>>();
         List<FloorDetailsWrapper> wrapperList = new List<FloorDetailsWrapper>();
         for(Location__c loc: [SELECT Id, Name, Floor_Name__c, Floor_Number__c 
                               FROM Location__c 
                               WHERE Location_Type__c = 'Floor'
                               AND Building_Number__c =: buildingId
                               ORDER BY Floor_Number__c ASC]){
             locationMap.put(loc.Id, loc);
             locationPayPlanMap.put(loc.Id, false);
         }
         system.debug('locationMap: '+ locationMap);
          for(Payment_Plan_Association__c planAssoc : [SELECT Id, Payment_Plan__c, Location__c
                                                     FROM Payment_Plan_Association__c
                                                     WHERE Location__c IN: locationMap.keyset()]){
             locationPayPlanMap.put(planAssoc.Location__c , true);
         }
         
         for(Inventory__c inv: [SELECT Id, Status__c, Floor_Location__c 
                                FROM Inventory__c 
                                WHERE Floor_Location__c IN: locationMap.keyset()]){
             List<Inventory__c> invList = new List<Inventory__c>();
             if(locationInventoryMap.containsKey(inv.Floor_Location__c)){
                 invList = locationInventoryMap.get(inv.Floor_Location__c);
             }
             invList.add(inv);
             locationInventoryMap.put(inv.Floor_Location__c, invList);
         }
         system.debug('locationInventoryMap: '+ locationInventoryMap);
         for(Location__c loc: locationMap.values()){
             Integer units = 0;
             Integer inventory = 0;
             Integer available = 0;
             Integer sold = 0;
             Integer restricted = 0;
             Boolean payPlan = false;
             if(locationPayPlanMap.containsKey(loc.Id)){
                 payPlan = locationPayPlanMap.get(loc.Id);
             }
             if(locationInventoryMap.containsKey(loc.Id)){
                 for(Inventory__c inv: locationInventoryMap.get(loc.Id)){
                     units++;
                     if(inv.Status__c == 'AVAILABLE'){
                         available++;
                     } else if(inv.Status__c == 'Inventory'){
                         inventory++;
                     } else if(inv.Status__c == 'Sold'){
                         sold++;
                     } else if(inv.Status__c == 'Restricted'){
                         restricted++;
                     }
                 }
             }
             FloorDetailsWrapper details = new FloorDetailsWrapper(loc.Floor_Name__c, loc.Id, payPlan, units, 
                                                                 inventory, available, sold, restricted);
             wrapperList.add(details);
         }    
         floorDetails = JSON.serialize(wrapperList);
         system.debug('floorDetails: ' + floorDetails);
    }

    public class FloorDetailsWrapper{
        public string floor;
        public string floorId;
        public string plan;
        public Integer units;
        public Integer inventory;
        public Integer available;
        public Integer sold;
        public Integer restricted;
        public FloorDetailsWrapper(){
        }
        public FloorDetailsWrapper(String floorName, String fId,
                                Boolean planPresent, Integer unitsCount, 
                                Integer inventoryCount, Integer availableCount, 
                                Integer soldCount, Integer restrictedCount){
            floor = floorName;
            floorId = fId;
            if(planPresent){
                plan = 'plan available';
            } else{
                plan = 'plan NOT available';
            }
            units = unitsCount;
            inventory = inventoryCount;
            available = availableCount;
            sold = soldCount;
            restricted = restrictedCount;
        }
    }
}