/****************************************************************************************************
* Name          : TransferRelatedRecordsTest                                                        *
* Description   : Class to test the TransferRelatedRecords class functionality                      *
* Created Date  : 17-07-2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE        COMMENTS                                                    *
* 1.0   Craig Lobo          17-07-2018  Initial Draft.                                              *
****************************************************************************************************/
@isTest
public class TransferRelatedRecordsTest {

    @isTest
    public static void testMethod1() {
        Id perInqRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();

        Inquiry__c inqObj= new Inquiry__c(
            First_Name__c = 'Test', 
            Last_Name__c = 'Last', 
            Mobile_Phone_Encrypt__c = '456123', 
            Mobile_CountryCode__c = 'American Samoa: 001684', 
            Mobile_Phone__c = '1234',
            Email__c = 'mk@gmail.com',
            Agency_Type__c = 'Individual',
            RecordTypeId = perInqRT,
            Pre_InquiryId__c = '12345',
            Telesales_Executive__c = userinfo.getuserid(),
            OwnerId = userinfo.getuserid()
        );
        insert inqObj;
        inqObj.Pre_InquiryId__c = inqObj.id;
        update inqObj;
        Inquiry__c inqObjNew= new Inquiry__c(
            First_Name__c = 'Test', 
            Last_Name__c = 'Last', 
            Mobile_Phone_Encrypt__c = '456123', 
            Mobile_CountryCode__c = 'American Samoa: 001684', 
            Mobile_Phone__c = '1234',
            Email__c = 'mk@gmail.com',
            Agency_Type__c = 'Individual',
            RecordTypeId = perInqRT,
            Pre_InquiryId__c = inqObj.id,
            Telesales_Executive__c = userinfo.getuserid(),
            OwnerId = userinfo.getuserid()
        );
        insert inqObjNew;
        Map<Id, Inquiry__c> newInquiriesMap = new  Map<Id, Inquiry__c>();
        newInquiriesMap.put(inqObj.id,inqObj);
        Travel_Details__c td = new Travel_Details__c();
        td.Inquiry__c = inqObj.Id;
        insert td;

        inqObj.Inquiry_Status__c = 'Meeting Scheduled';
        inqObj.Tickets_Confirmed__c = true;
        inqObj.Assigned_PC__c = UserInfo.getUserId();

        System.Test.startTest();
     //   update inqObj;
        try{
            TransferRelatedRecords tranClass= new TransferRelatedRecords();
        } catch (exception e){}
        try{
            TransferRelatedRecords.moveTravelDetailRecords(newInquiriesMap);
        } catch (exception e){}
        try{
            TransferRelatedRecords.TransferActivitiestoInq(inqObj.id);
        } catch (exception e){}
        System.Test.stopTest();

    }

}