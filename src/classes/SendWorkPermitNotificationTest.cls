/**
 * @File Name          : SendWorkPermitNotificationTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 10/1/2019, 3:51:10 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/1/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
private  class SendWorkPermitNotificationTest {
    
    @isTest static void isShould() {
        Account objAcc = new Account();
        objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        Location__c objLoc = new Location__c();
        objLoc = TestDataFactoryFM.createLocation();
        insert objLoc;
        Violation_Rule__c objVR = new Violation_Rule__c();
        objVR.Building__c = objLoc.Id;
        objVR.Category__c  ='General Violations';
        objVR.Payble_Fine__c = 100;
        objVR.Remedial_Period__c = '7 days';
        objVR.Violation_Of_Rule__c = 'Abusing personnel working in the community';
        insert objVR;
        FM_User__c objFMUser = new FM_User__c(FM_User__c=UserInfo.getUserId(),
                    FM_Role__c = 'Property Manager',
                    Building__c = objLoc.Id);
        insert objFMUser;
        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAcc);
        insert objSR;
        Booking__c objBooking = TestDataFactoryFM.createBooking(objAcc, objSR);
        insert objBooking;
        Booking_unit__c objBU = TestDataFactoryFM.createBookingUnit(objAcc, objBooking);
        insert objBU;

        FM_Case__c fmObj = new FM_Case__c();
        Id RecordTypeIdFMCase = DamacUtility.getRecordTypeId(
            'FM_Case__c', 'Work Permit'
        );
        fmObj.recordtypeid = RecordTypeIdFMCase;
        fmObj.Property_Director_Email__c = 'a@a.com';
        fmObj.Select_Notice_Type_Task_Created_Date__c = date.today().adddays(-5); 
        fmObj.Is_Violation_Case_Escalation_Sent__c = false;
        fmObj.Status__c = 'Submitted';
        insert fmObj;

        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', ProfileId = p.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testdmacorg.com');

        System.runAs(u) {  
            fmObj.Status__c = 'Closed'; 
            update fmObj;         
        } 
        List<Id>fmcaseId = new List<Id>();
        fmcaseId.add(fmObj.Id);
        test.startTest();
            Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
            SendWorkPermitNotification.initiateNotification(fmCaseId);
        test.stopTest();
        
    }
}