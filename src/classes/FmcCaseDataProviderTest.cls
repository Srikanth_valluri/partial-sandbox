@isTest
private class FmcCaseDataProviderTest {

    @isTest
    static void testProvidedData() {
        Account account = new Account(Name = 'Test Account');
        insert account;

        CustomerCommunityUtils.customerAccountId = account.Id;

        DataDisplayConfig config = new DataDisplayConfig();
        config.strategy = 'class';
        config.recordLimit = '1000';
        config.objectName = 'FM_Case__c';
        config.fieldset = '';
        config.fieldList = new List<Map<String,String>> {
            new Map<String, String> {
                'field' => 'Name',
                'title' => 'SR Number',
                'type' => 'link',
                'href' => '?view=CaseDetails&id={!Id}',
                'referencedFields' => 'Id'
            },
            new Map<String, String> {
                'field' => 'Submission_Datetime__c',
                'title' => 'SR Raised Date'
            },
            new Map<String, String> {
                'field'=> 'Request_Type__c',
                'title'=> 'SR Type'
            },
            new Map<String, String> {
                'field' => 'Booking_Unit__r.Unit_Name__c',
                'title' => 'Unit Name'
            },
            new Map<String, String> {
                'field'=> 'Status__c',
                'title'=> 'SR status'
            },
            new Map<String, String> {
                'field' => 'Closure_Datetime__c',
                'title' => 'Due Date'
            }
        };

        config.displayAs = 'table';
        config.dataProviderClass = 'FmcCaseDataProvider';
        config.dataList = new List<Map<String, Object>>();
        config.configJSON = 'FmcCaseDataProvider';

        Test.startTest();
            new FmcCaseDataProvider().getData(config);
        Test.stopTest();
    }

}