@isTest
public class PDCReceiptGeneratorTest {
    public class PDCResponseGenerator implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            req.setMethod('POST');
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            //res.setBody('{"PDC_Receipt": {"InputParameters":{"P_REG_ID":"110037","P_RECEIPT_NUMBER":"","P_RECEIPT_METHOD_ID":"107702","P_RECEIPT_DATE":"2020-09-15","P_RECEIPT_CLASSIFICATION":"Early Handover","P_PAYMENT_MODE":"Cash","P_PARTY_ID":"2606120","P_PARAMETER3":"","P_PARAMETER2":"","P_PARAMETER1":"","P_ORG_ID":"81","P_MATURITY_DATE":"2020-09-15","P_CURRENCY_CODE":"AED","P_COMMENTS":"Test reason","P_CASH_RECEIPT_ID":"","P_AMOUNT":""}}}');
            String Json = '{ "OutputParameters" : {"@xmlns" : "http://xmlns.oracle.com/apps/ont/rest/XXDC_PDC_AR_RECEIPTS_PKG/create_receipt_pdc_ar/", "@xmlns:xsi" : "http://www.w3.org/2001/XMLSchema-instance","X_RECEIPT_ID" : "12233","X_RECEIPT_NO" : "12233", "X_RETURN_STATUS" : "E","X_ERROR_MSG" : "Either maturity date is null or Receipt Date is null or Cheque Hold Reason is Null" } }';
            res.setBody(Json);
            res.setStatusCode(200);
            return res;
        }
    }
    
    @isTest
    static void testGenrateReceipt(){
        
        Account objAccount = new Account();
        objAccount.Name = 'test';
        objAccount.Party_ID__c = '12345';
        insert objAccount;
        
        Case objCase = new Case();
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();
        objCase.AccountId = objAccount.Id;
        insert objCase;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        objBooking.Account__c = objAccount.Id;
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Registration_ID__c = 'test';
        objBU.Booking__c = objBooking.Id;
        insert objBU;
        
        TriggerOnOffCustomSetting__c objTrigger = new TriggerOnOffCustomSetting__c();
        objTrigger.Name = 'CallingListTrigger';
        objTrigger.OnOffCheck__c = true;
        insert objTrigger;
        
        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Unit_Name__c = 'test';       
        objCallingList.Account__c = objAccount.Id;
        objCallingList.Booking_Unit__c  = objBU.Id;
        insert objCallingList;
        
      
        Case_Post_Dated_Cheque__c objCasePost = new Case_Post_Dated_Cheque__c();
        objCasePost.Receipt_No__c =  '191';
        objCasePost.Receipt_Date__c = System.today();
        objCasePost.New_maturity_date__c = System.today(); 
        objCasePost.New_Amount__c = 100;
        objCasePost.CurrencyIsoCode =  'AED';
        objCasePost.Reason__c = 'testReason';
        objCasePost.IPMS_Cash_Receipt_Id__c = '';
        objCasePost.Receipt_Classification__c = 'AOPT';
        objCasePost.Mode_of_Payment__c = 'CDM-Cash Deposit';
        objCasePost.New_Drawer__c = ' testDrawer';
        objCasePost.New_Bank__c = 'testBank';
        objCasePost.Account__c = objAccount.Id;
        objCasePost.Calling_List__c = objCallingList.Id;
        objCasePost.Case__c = objCase.id;
        objCasePost.Booking_Unit__c = objBU.Id;
        objCasePost.Calling_List__c = objCallingList.Id;
        objCasePost.OwnerId = UserInfo.getUserId();
        insert objCasePost;
        
        Credentials_Details__c objCred = new Credentials_Details__c();
        objCred.Name = 'PDCReceiptGenerator';
        objCred.Endpoint__c = 'http://83.111.194.181:8033/webservices/rest/XXDC_PDC_AR_RECEIPTS_PKG/create_receipt_pdc_ar/';
        objCred.User_Name__c = 'oracle_user';
        objCred.Password__c = 'crp1user';
        insert objCred;
        
        List<PDCReceiptGenerator.generateVariables> listGenerateVariables = new List<PDCReceiptGenerator.generateVariables>();
        PDCReceiptGenerator.generateVariables objGenerateVariables = new PDCReceiptGenerator.generateVariables();
        objGenerateVariables.ObjName = 'test';
        listGenerateVariables.add(objGenerateVariables);
        
        Test.setMock(HttpCalloutMock.class, new PDCResponseGenerator ());
        Test.startTest();
        //PDCReceiptGenerator.sendEmailToOwner('raveena.jain@eternussolutions.com','noreply@damacgroup.com',
        // 'There are one or more receipt creation failure -  please see below deatils : \n\n' );
        PDCReceiptGenerator.callingGenerateReceipt(listGenerateVariables);
        PDCReceiptGenerator.generatePDCReceipt(new List<Id>{objCasePost.Id});
        
        Test.stopTest();
    }
}