/***************************************************************************************************
* Name               : AsyncWebserviceForBuyer *
* Description        : Controller class Send Joint Buyer creation, Buyer deletion requests to IPMS  *
* Created By         : Srikanth                 
* 1.1  27-01-2020    : QBurst     Added Condition for suppressing Natinality                        *
****************************************************************************************************/

public class AsyncWebserviceForBuyer {
    
    /*********************************************************************************************
* @Description : JSON creation Method                                                        *
* @Params      : SR Name, Buyer record, List of Booking Units, request type, Boolean value   *
* @Return      : String                                                                      *
*********************************************************************************************/
    public static String createJSON (List <Booking_Unit__c> units, String srName, Buyer__c buyer, String subReqName, Boolean Async) {
        
        List<BuyerJSONCreation.registrationLines> registrationLines = new List<BuyerJSONCreation.registrationLines>();
        List<BuyerJSONCreation.jointCustomerLines> jointCustomerLines = new List<BuyerJSONCreation.jointCustomerLines>();
        List<BuyerJSONCreation.customerContactLines> customerContactLines = new List<BuyerJSONCreation.customerContactLines>();
        List<BuyerJSONCreation.customerAddressLines> customerAddressLines = new List<BuyerJSONCreation.customerAddressLines>();
        List<BuyerJSONCreation.customerArabicLines> customerArabicLines = new List<BuyerJSONCreation.customerArabicLines>();
        
        BuyerJSONCreation obj = new BuyerJSONCreation();
        obj.sourceSystem = 'SFDC';
        obj.extRequestNumber = srName;
        
        BuyerJSONCreation.customerContactLines customerContactLine = new BuyerJSONCreation.customerContactLines();
        customerContactLine.partyId = buyer.Party_ID__c;
        customerContactLine.homeCountryCode = '';
        customerContactLine.homeAreaCode = '';
        customerContactLine.home_number = '';
        customerContactLine.workCountryCode = '';
        customerContactLine.workAreaCode = '';
        customerContactLine.workNumber = '';
        
        String countryCode = '971';
        if(buyer.Phone_Country_Code__c != null ){
            countryCode = buyer.Phone_Country_Code__c.split (':')[1];
            countryCode = countryCode.deleteWhitespace();
            countryCode = countryCode.removeStart('00');
        }

        customerContactLine.mobileCountryCode = countryCode;
        customerContactLine.mobileAreaCode = '';
        customerContactLine.mobileNumber = buyer.Phone__c;
        customerContactLine.faxCountryCode = '';
        customerContactLine.faxAreaCode = '';
        customerContactLine.faxNumber = '';
        customerContactLine.emailAddress = buyer.Email__c;
        customerContactLine.url = '';
        customerContactLine.mobileCountryCodeAr = '';
        customerContactLine.mobileAreaCodeAr = '';
        customerContactLine.mobileNumberAr = '';
        customerContactLine.phoneCountryCodeAr = '';
        customerContactLine.phoneAreaCodeAr = '';
        customerContactLine.phoneNumberAr = '';
        customerContactLine.phoneExtentionAr = '';
        customerContactLine.faxCountryCodeAr = '';
        customerContactLine.faxAreaCodeAr = '';
        customerContactLine.faxNumberAr = '';
        customerContactLine.faxExtensionAr = '';
        customerContactLines.add(customerContactLine);
        
        BuyerJSONCreation.customerAddressLines addressLine = new BuyerJSONCreation.customerAddressLines();
        addressLine.partyId = buyer.Party_ID__c;
        addressLine.address1 = buyer.Address_Line_1__c;
        addressLine.address2 = buyer.Address_Line_2__c;
        addressLine.address3 = buyer.Address_Line_3__c;
        addressLine.address4 = buyer.Address_Line_4__c;
        addressLine.postalCode = '';
        addressLine.state = '';
        addressLine.country = buyer.Country__c;
        addressLine.city = buyer.City__c;
        customerAddressLines.add(addressLine);
        
        BuyerJSONCreation.customerArabicLines arabicLines = new BuyerJSONCreation.customerArabicLines();
        arabicLines.partyId = buyer.Party_ID__c;
        arabicLines.titleAr = buyer.Title_Arabic__c;
        arabicLines.firstNameAr = buyer.First_Name_Arabic__c;
        arabicLines.middleNameAr = buyer.Middle_Name_Arabic__c;
        arabicLines.lastNameAr = buyer.Last_Name_Arabic__c;
        if(buyer.Buyer_Type__c !='Corporate'){//1.1
         arabicLines.nationalityAr = buyer.Nationality_Arabic__c;
        }
        arabicLines.crNumberAr = buyer.CR_Number__c;
        arabicLines.crRegDateAr = formatDate (buyer.CR_Registration_Expiry_Date_New__c);
        arabicLines.crRegPlaceAr = buyer.CR_Registration_Place__c;
        arabicLines.emailAddressAr = buyer.Email__c;
        arabicLines.passportIssueDateAr = formatDate (buyer.Passport_Expiry__c);
        arabicLines.passportIssuePlaceAr = buyer.Place_of_Issue_Arabic__c;
        arabicLines.address1Ar = buyer.Address_Line_1_Arabic__c;
        arabicLines.address2Ar = buyer.Address_Line_2_Arabic__c;
        arabicLines.address3Ar = buyer.Address_Line_3_Arabic__c;
        arabicLines.address4Ar = buyer.Address_Line_4_Arabic__c;
        arabicLines.cityAr = buyer.City_Arabic__c;
        arabicLines.countryAr = buyer.Country_Arabic__c;
        arabicLines.postalCodeAr = '';
        customerArabicLines.add(arabicLines);
        
        BuyerJSONCreation.jointCustomerLines customerLine = new BuyerJSONCreation.jointCustomerLines();
        customerLine.subRequestName = subReqName;
        customerLine.extCustomerNumber = buyer.Buyer_ID__c;
        customerLine.partyId = buyer.Party_ID__c;
        if (buyer.buyer_Type__c == 'Individual')
            customerLine.partyType = 'Person';
        
        if (buyer.buyer_Type__c == 'Corporate')
            customerLine.partyType = 'Organization';
        
        customerLine.personTitle = buyer.Title__c;
        if (buyer.Buyer_Type__c == 'Corporate') {
            customerLine.organizationName = buyer.Organisation_Name__c != NULL ? buyer.Organisation_Name__c : '';
        } else {
            customerLine.organizationName = '';
        }
        customerLine.personFirstName = buyer.First_Name__c;
        customerLine.personMiddleName = buyer.Middle_Name__c;
        customerLine.personLastName = buyer.Last_Name__c;
        if(buyer.Buyer_Type__c !='Corporate'){//1.1
         customerLine.nationality = buyer.Nationality__c;
        }
        customerLine.passportNumber = buyer.Passport_Number__c;
        customerLine.ppIssueDate = '';
        customerLine.ppIssuePlace = buyer.Place_of_Issue__c;
        customerLine.ppExpDate = formatDate(buyer.Passport_Expiry__c);
        customerLine.gender = buyer.gender__c;
        customerLine.birthPlace = '';
        customerLine.birthDate = formatDate (buyer.DOB__c);
        
        customerLine.eidNumber = buyer.EID_No__c;
        customerLine.eidExpDate = formatDate (buyer.EID_Expiry_Date__c);
        
        customerLine.crNumber = buyer.CR_Number__c;
        customerLine.crRegPlace = buyer.CR_Registration_Place__c;
        customerLine.crRegDate = formatDate (buyer.CR_Registration_Expiry_Date_New__c);
        
        customerLine.customerContactLines = customerContactLines;
        customerLine.customerAddressLines = customerAddressLines;
        customerLine.customerArabicLines = customerArabicLines;
        jointCustomerLines.add(customerLine);
        
        for (Booking_Unit__c bu: units) {
            BuyerJSONCreation.registrationLines registrationLine = new BuyerJSONCreation.registrationLines();
            registrationLine.registrationId = bu.Registration_ID__c;           
            registrationLine.jointCustomerLines = jointCustomerLines;
            registrationLines.add(registrationLine);
        }
        obj.registrationLines = registrationLines;
        String reqBody = JSON.serializePretty(obj);
        system.debug('reqBody---'+reqBody);
        String responseStatus = '';
        
        if (Async) {
            
            id jobId = System.enqueueJob(new BuyerWebserviceQueuable (reqBody, buyer.Id, subReqName));
            buyer.job_id__c = jobId;
            Update buyer;
            
        }
        else
            responseStatus = sendToIPMS (reqBody, buyer.Id, subReqName);
        
        return responseStatus;
    }
    
    public static String SendToIPMS (String reqBody, String recId, String subReqName) {
        String response = '';
        try{
            String EndPointURL = '';
            try {
                IPMS_Integration_Settings__mdt ipms_Rest = [SELECT Endpoint_URL__c,
                                                            Password__c,  
                                                            Username__c 
                                                            FROM IPMS_Integration_Settings__mdt 
                                                            WHERE DeveloperName = 'IPMS_Buyer_REST' LIMIT 1];
                EndPointURL = ipms_Rest.Endpoint_URL__c;
            }
            catch (Exception e) {}          
            if (Test.isRunningTest ()) {
                EndPointURL = 'www.test.com';
            }            
            
            
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint (EndPointURL);
            req.setMethod ('POST');
            req.setheader ('Content-Type', 'application/json');
            req.setbody (reqBody);
            req.setTimeOut (120000);
            System.Debug (EndPointURL);
            System.Debug ('---Request Body---'+reqBody);
            HttpResponse res = new HttpResponse();
            if (!Test.isRunningTest())
                res = h.send(req);
            System.Debug (res.getBody ());
            if (Test.isRunningTest()) {
                res.setStatusCode(201);
                res.setBody('{"responseId":"77752","responseTime":"Mon Mar 05 13:44:37 GST 2018","requestName":"REGISTRATION","extRequestName":"SR-216260","responseLines":[{"id":"77752","lineItems":[{"id":"10933","processName":"REGISTRATION_LINE","processStatus":"S","startTime":"05-Mar-2018 13:44:26","endTime":"05-Mar-2018 13:48:52","attributes":[{"name":"REGISTRATION_ID","value":"98945"}],"lineItems":[{"id":"10934","processName":"CREATE_JOINT_BUYER","processStatus":"S","startTime":"05-Mar-2018 13:44:26","endTime":"05-Mar-2018 13:48:52","attributes":[{"name":"REGISTRATION_ID","value":"98945"}],"lineItems":[{"id":"10935","processName":"CUSTOMER_ADDRESS","processStatus":"E","processMessage":"[CCP]CREATE_CUSTOMER_EBS [Status=E][x_return_msg=[C_C_E]Invalid Customer Type :Individual]","startTime":"05-Mar-2018 13:44:26","attributes":[{"name":"REGISTRATION_ID","value":"98945"}]},{"id":"10936","processName":"CUSTOMER_ARABIC","processStatus":"E","processMessage":"[CCP]CREATE_CUSTOMER_EBS [Status=E][x_return_msg=[C_C_E]Invalid Customer Type :Individual]","startTime":"05-Mar-2018 13:44:26","attributes":[{"name":"REGISTRATION_ID","value":"98945"}]},{"id":"10937","processName":"CUSTOMER_CONTACT","processStatus":"E","processMessage":"[CCP]CREATE_CUSTOMER_EBS [Status=E][x_return_msg=[C_C_E]Invalid Customer Type :Individual]","startTime":"05-Mar-2018 13:44:26","attributes":[{"name":"REGISTRATION_ID","value":"98945"}]}]}]},{"id":"10938","processName":"REGISTRATION_LINE","processStatus":"S","startTime":"05-Mar-2018 13:44:26","endTime":"05-Mar-2018 13:48:53","attributes":[{"name":"REGISTRATION_ID","value":"98946"}],"lineItems":[{"id":"10939","processName":"CREATE_JOINT_BUYER","processStatus":"S","startTime":"05-Mar-2018 13:44:26","endTime":"05-Mar-2018 13:48:53","attributes":[{"name":"REGISTRATION_ID","value":"98946"}],"lineItems":[{"id":"10940","processName":"CUSTOMER_ADDRESS","processStatus":"E","processMessage":"[CCP]CREATE_CUSTOMER_EBS [Status=E][x_return_msg=[C_C_E]Invalid Customer Type :Individual]","startTime":"05-Mar-2018 13:44:26","attributes":[{"name":"REGISTRATION_ID","value":"98946"}]},{"id":"10941","processName":"CUSTOMER_ARABIC","processStatus":"E","processMessage":"[CCP]CREATE_CUSTOMER_EBS [Status=E][x_return_msg=[C_C_E]Invalid Customer Type :Individual]","startTime":"05-Mar-2018 13:44:26","attributes":[{"name":"REGISTRATION_ID","value":"98946"}]},{"id":"10942","processName":"CUSTOMER_CONTACT","processStatus":"E","processMessage":"[CCP]CREATE_CUSTOMER_EBS [Status=E][x_return_msg=[C_C_E]Invalid Customer Type :Individual]","startTime":"05-Mar-2018 13:44:26","attributes":[{"name":"REGISTRATION_ID","value":"98946"}]}]}]}]}],"complete":true}');
            }
            if(res.getStatusCode() == 201 ) {
                String returnMsg = parseResponse (res.getBody (), recId, subReqName);
                if (returnMsg == 'Success') {
                    createLog (recId, '', 'Success'); 
                    return 'Success';
                }
                else {
                    createLog (recId, res.getBody(), 'Error');
                    return 'Error';     
                }
            } else {
                createLog (recId, res.getBody(), 'Error');
                response = 'ERROR';
            }
        } catch(exception e){
            System.Debug ('-----'+e.getMessage ());
            createLog (recId, String.valueof (e.getmessage()), 'Error');
            response = 'ERROR';
        }
        
        return response;
    }
    
    public static String parseResponse (String responseBody, Id BuyerId, String subReqName) {
        String returnMsg = '';
        try {
            Buyer__c buyer = new Buyer__c ();
            buyer = [ SELECT Party_ID__c FROM Buyer__c WHERE ID =: buyerId];
            Boolean updateBuyer = false;
            ParseBuyerResponse response = ParseBuyerResponse.parse (responseBody);
            
            for (ParseBuyerResponse.cls_responseLines responseLines :response.responseLines) {
                for (ParseBuyerResponse.cls_lineItems lineItem : responseLines.lineItems) {
                    for (ParseBuyerResponse.cls_lineItems item : lineItem.lineItems) {
                        system.debug (item.processName+'===='+subReqName);
                        system.debug (item.processStatus);
                        if (item.processName == subReqName) {
                            if (item.processStatus == 'S') {
                                returnMsg = 'Success';
                                system.debug (item.attributes);
                                for (ParseBuyerResponse.cls_attributes attribute : item.attributes) {
                                   
                                    if (buyer.Party_ID__c == NULL) {
                                        if (attribute.Name == 'PARTY_ID') {
                                            buyer.Party_ID__c = attribute.value;
                                            updateBuyer = true;
                                            break;                                            
                                        } 
                                    }
                                    System.Debug (attribute.Name+'__ UPdate __'+updateBuyer );                                
                                    
                                }
                            } else {
                                returnMsg =  '';
                                break;
                            }
                        }
                    }
                }
                
            }
            if (updateBuyer) {
                update buyer;
                System.Debug ('*** Update Buyer ***'+buyer.ID);
            }
            return returnMsg;
        }
        catch (Exception e) {
            return '';
        }
    }
    public static void createLog (string parentId, String ErrorMessage, String type) {
        log__c log = new log__c ();
        log.Buyer__c = parentId;
        log.Type__c = type;
        log.Description__c = ErrorMessage;
        insert log;
    }
    public static String formatDate (Date dateVal) {
        if (dateVal == NULL) return '';
        
        String dd, mm, mon = '';
        dd = String.valueof(dateVal.day());
        mm = String.valueof(dateVal.month());
        
        if (mm == '1')
            mon = 'JAN';
        else if (mm == '2')
            mon = 'FEB';
        else if (mm == '3')
            mon = 'MAR';
        else if (mm == '4')
            mon = 'APR';
        else if (mm == '5')
            mon = 'MAY';
        else if (mm == '6')
            mon = 'JUN';
        else if (mm == '7')
            mon = 'JUL';
        else if (mm == '8')
            mon = 'AUG';
        else if (mm == '9')
            mon = 'SEP';
        else if (mm == '10')
            mon = 'OCT';
        else if (mm == '11')
            mon = 'NOV';
        else if (mm == '12')
            mon = 'DEC';
        
        return dd+'-'+mon+'-'+dateVal.year();
        
        
    }
    
}