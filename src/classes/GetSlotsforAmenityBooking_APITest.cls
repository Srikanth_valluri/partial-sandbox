@isTest 
private class GetSlotsforAmenityBooking_APITest {

    @TestSetup
    static void TestData() {
    
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'Janusia';
        insert locObj;

        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id,
                                                          Registration_ID__c = '3901',
                                                          Inventory__c = invObj.Id,
                                                          Unit_Name__c='JNU/SD168/XH2910B');
        insert bookingUnit;

        Booking_Unit__c bookingUnit2 = new Booking_Unit__c(Booking__c = booking.Id,
                                                          Registration_ID__c = '3902',
                                                          Inventory__c = invObj.Id,
                                                          Unit_Name__c='JNU/SD168/XH2910C');
        insert bookingUnit2;

        Property__c propObj = TestDataFactoryFM.createProperty();
        insert propObj;

        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;

        Resource__c objRes = new Resource__c();
        objRes.Name = 'Cricket Ground';
        objRes.Public_Amenity_Name__c = 'Cricket Ground';
        //objRes.Chargeable_Fee_Slot__c = 10;
        //objRes.Non_Operational_Days__c = 'Sunday';
        //objRes.Resource_Type__c = 'FM Amenity';
        objRes.No_of_advance_booking_days__c = 10;
        objRes.Resource_Type__c = 'FM Amenity';
        //objRes.Deposit__c = 3;
        ///objRes.Chargeable_Fee_Slot__c = 5;
        //objRes.No_of_deposit_due_days__c = 1;
        objRes.Contact_Person_Email__c = 'test@test.com';   //
        objRes.Contact_Person_Name__c = 'Test'; //
        objRes.Contact_Person_Phone__c = '1234567890';  //
        insert objRes;

        Resource_Sharing__c objRS= new Resource_Sharing__c();
        objRS.Building__c = locObj.Id;
        objRS.Resource__c = objRes.Id;
        insert objRS;

        Resource_Slot__c objResSlot1 = new Resource_Slot__c();
        objResSlot1.Resource__c = objRes.Id;
        objResSlot1.Start_Date__c = Date.Today().addDays(-2);
        objResSlot1.End_Date__c = Date.Today().addDays(30);
        objResSlot1.Start_Time_p__c = '09:00';
        objResSlot1.End_Time_p__c = '09:30';
        insert objResSlot1;

        Resource_Slot__c objResSlot2 = new Resource_Slot__c();
        objResSlot2.Resource__c = objRes.Id;
        objResSlot2.Start_Date__c = Date.Today().addDays(-2);
        objResSlot2.End_Date__c = Date.Today().addDays(30);
        objResSlot2.Start_Time_p__c = '09:00';
        objResSlot2.End_Time_p__c = '09:30';
        insert objResSlot2;

        Non_operational_days__c obj = new Non_operational_days__c();
        obj.Non_operational_date__c = Date.today().addDays(3);
        obj.Resource__c = objRes.Id;
        insert obj;

        String devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().
                                get('Amenity Booking').getRecordTypeId();
        
        Resource__c res = [SELECT id FROM Resource__c WHERE Name = 'Cricket Ground'];                        

        FM_Case__c objFm = new FM_Case__c();
        objFm.RecordTypeId = devRecordTypeId;
        objFm.Origin__c='Portal';  //'App-Hello Damac';
        objFm.Amenity_Booking_Status__c = 'Booking Confirmed';
        objFm.Status__c = 'Closed';
        objFm.Booking_Start_Time_p__c = '09:00';
        objFm.Booking_End_Time_p__c = '09:30';
        objFm.Resource_Booking_Type__c = 'FM Amenity';
        objFm.Resource__c = res.Id;
        objFm.Resource_SlotID__c = objResSlot1.id;
        objFm.Booking_Date__c = Date.today().addDays(1);
        objFm.Booking_Unit__c = bookingUnit.Id;
        objFm.Account__c = account.Id;
        objFm.Contact_Email__c = 'test@test.com';
        objFM.Contact_Mobile__c = '123456789';
        objFM.Contact_person__c = 'Test';
        objFm.Initiated_by_tenant_owner__c = 'Owner';
        objFm.Admin__c = fmUser.FM_User__c;
        objFm.Tenant__c =  account.Id;
        objFm.Tenant_Email__c = account.Email__pc;

        insert objFm;

        
    }


    @isTest
    static void getAmenitySlotsTest1() {


        String dateDayAfterTomorrow = Datetime.newInstance(Date.today().addDays(2).year(), Date.today().addDays(2).month(), Date.today().addDays(2).day()).format('yyyy-MM-dd');
        Resource__c objRes = [SELECT id FROM Resource__c WHERE Resource_Type__c = 'FM Amenity'];
        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenityBookingSlots';  
        req.addParameter('bookingDate', dateDayAfterTomorrow);
        req.addParameter('amenityId',objRes.Id);
        req.addParameter('bookingUnitId',objBU.Id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Non_operational_days__c objNP = [SELECT id 
                                              , Non_operational_date__c
                                              , Resource__c
                                         FROM Non_operational_days__c
                                         WHERE Resource__c =: objRes.Id];

        objNP.Non_operational_date__c = Date.today().addDays(1);
        update objNP;

        Test.startTest();
        GetSlotsforAmenityBooking_API.getAmenitySlots();
        Test.stopTest();
    }

    @isTest
    static void getAmenitySlotsTest2() {


        String dateTomorrow = Datetime.newInstance(Date.today().addDays(1).year(), Date.today().addDays(1).month(), Date.today().addDays(1).day()).format('yyyy-MM-dd');
        Resource__c objRes = [SELECT id FROM Resource__c WHERE Resource_Type__c = 'FM Amenity'];
        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        //FM_Case__c objFm = [SELECT id
        //                         , Amenity_Booking_Status__c
        //                         , Status__c
        //                         , Booking_Start_Time_p__c
        //                         , Booking_End_Time_p__c
        //                         , Resource_Booking_Type__c
        //                         , Resource__c
        //                         , Resource_SlotID__c
        //                         , Booking_Date__c
        //                         , Booking_Unit__c
        //                         , Account__c
        //                    FROM FM_Case__c
        //                    WHERE Booking_Unit__c =:objBU.id];


        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenityBookingSlots';  
        req.addParameter('bookingDate', dateTomorrow);
        req.addParameter('amenityId',objRes.Id);
        req.addParameter('bookingUnitId',objBU.Id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        GetSlotsforAmenityBooking_API.getAmenitySlots();
        Test.stopTest();
    }

    @isTest
    static void getAmenitySlotsTest3() {


        String dateTomorrow = Datetime.newInstance(Date.today().addDays(1).year(), Date.today().addDays(1).month(), Date.today().addDays(1).day()).format('yyyy-MM-dd');
        Resource__c objRes = [SELECT id FROM Resource__c WHERE Resource_Type__c = 'FM Amenity'];
        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3902'];
        //FM_Case__c objFm = [SELECT id
        //                         , Amenity_Booking_Status__c
        //                         , Status__c
        //                         , Booking_Start_Time_p__c
        //                         , Booking_End_Time_p__c
        //                         , Resource_Booking_Type__c
        //                         , Resource__c
        //                         , Resource_SlotID__c
        //                         , Booking_Date__c
        //                         , Booking_Unit__c
        //                         , Account__c
        //                    FROM FM_Case__c
        //                    WHERE Booking_Unit__c =:objBU.id];


        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenityBookingSlots';  
        req.addParameter('bookingDate', dateTomorrow);
        req.addParameter('amenityId',objRes.Id);
        req.addParameter('bookingUnitId',objBU.Id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        GetSlotsforAmenityBooking_API.getAmenitySlots();
        Test.stopTest();
    }

    //Negative scenarios
    @isTest
    static void getAmenitySlotsTestNegative1() {


        String dateTomorrow = Datetime.newInstance(Date.today().addDays(1).year(), Date.today().addDays(1).month(), Date.today().addDays(1).day()).format('yyyy-MM-dd');
        Resource__c objRes = [SELECT id FROM Resource__c WHERE Resource_Type__c = 'FM Amenity'];
        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenityBookingSlots';  
        //req.addParameter('bookingDate', dateTomorrow);
        req.addParameter('amenityId',objRes.Id);
        req.addParameter('bookingUnitId',objBU.Id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;


        Test.startTest();
        GetSlotsforAmenityBooking_API.getAmenitySlots();
        Test.stopTest();
    }

    @isTest
    static void getAmenitySlotsTestNegative2() {


        String dateTomorrow = Datetime.newInstance(Date.today().addDays(1).year(), Date.today().addDays(1).month(), Date.today().addDays(1).day()).format('yyyy-MM-dd');
        Resource__c objRes = [SELECT id FROM Resource__c WHERE Resource_Type__c = 'FM Amenity'];
        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenityBookingSlots';  
        req.addParameter('bookingDate', dateTomorrow);
        //req.addParameter('amenityId',objRes.Id);
        req.addParameter('bookingUnitId',objBU.Id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        //Non_operational_days__c objNP = [SELECT id 
        //                                      , Non_operational_date__c
        //                                      , Resource__c
        //                                 FROM Non_operational_days__c
        //                                 WHERE Resource__c =: objRes.Id];

        Test.startTest();
        GetSlotsforAmenityBooking_API.getAmenitySlots();
        Test.stopTest();
    }

    @isTest
    static void getAmenitySlotsTestNegative3() {


        String dateTomorrow = Datetime.newInstance(Date.today().addDays(1).year(), Date.today().addDays(1).month(), Date.today().addDays(1).day()).format('yyyy-MM-dd');
        Resource__c objRes = [SELECT id FROM Resource__c WHERE Resource_Type__c = 'FM Amenity'];
        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenityBookingSlots';  
        req.addParameter('bookingDate', dateTomorrow);
        req.addParameter('amenityId',objRes.Id);
        //req.addParameter('bookingUnitId','');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        //Non_operational_days__c objNP = [SELECT id 
        //                                      , Non_operational_date__c
        //                                      , Resource__c
        //                                 FROM Non_operational_days__c
        //                                 WHERE Resource__c =: objRes.Id];

        Test.startTest();
        GetSlotsforAmenityBooking_API.getAmenitySlots();
        Test.stopTest();
    }

    //Negative scenarios 2
    @isTest
    static void getAmenitySlotsTestNegative4() {


        String dateTomorrow = Datetime.newInstance(Date.today().addDays(1).year(), Date.today().addDays(1).month(), Date.today().addDays(1).day()).format('yyyy-MM-dd');
        Resource__c objRes = [SELECT id FROM Resource__c WHERE Resource_Type__c = 'FM Amenity'];
        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenityBookingSlots';  
        req.addParameter('bookingDate', '');
        req.addParameter('amenityId',objRes.Id);
        req.addParameter('bookingUnitId',objBU.Id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        GetSlotsforAmenityBooking_API.getAmenitySlots();
        Test.stopTest();
    }

    @isTest
    static void getAmenitySlotsTestNegative5() {


        String dateTomorrow = Datetime.newInstance(Date.today().addDays(1).year(), Date.today().addDays(1).month(), Date.today().addDays(1).day()).format('yyyy-MM-dd');
        Resource__c objRes = [SELECT id FROM Resource__c WHERE Resource_Type__c = 'FM Amenity'];
        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenityBookingSlots';  
        req.addParameter('bookingDate', dateTomorrow);
        req.addParameter('amenityId','');
        req.addParameter('bookingUnitId',objBU.Id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        GetSlotsforAmenityBooking_API.getAmenitySlots();
        Test.stopTest();
    }

    @isTest
    static void getAmenitySlotsTestNegative6() {


        String dateTomorrow = Datetime.newInstance(Date.today().addDays(1).year(), Date.today().addDays(1).month(), Date.today().addDays(1).day()).format('yyyy-MM-dd');
        Resource__c objRes = [SELECT id FROM Resource__c WHERE Resource_Type__c = 'FM Amenity'];
        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenityBookingSlots';  
        req.addParameter('bookingDate', dateTomorrow);
        req.addParameter('amenityId',objRes.Id);
        req.addParameter('bookingUnitId','');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        GetSlotsforAmenityBooking_API.getAmenitySlots();
        Test.stopTest();
    }

    @isTest
    static void getAmenitySlotsTestNegative7() {


        String dateTomorrow = Datetime.newInstance(Date.today().addDays(1).year(), Date.today().addDays(1).month(), Date.today().addDays(1).day()).format('yyyy-MM-dd');
        Resource__c objRes = [SELECT id FROM Resource__c WHERE Resource_Type__c = 'FM Amenity'];
        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenityBookingSlots';  
        req.addParameter('bookingDate', dateTomorrow);
        req.addParameter('amenityId',objRes.Id);
        req.addParameter('bookingUnitId',objBU.Id);
        req.addParameter('isModify','true');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        GetSlotsforAmenityBooking_API.getAmenitySlots();
        Test.stopTest();
    }

    @isTest
    static void getAmenitySlotsTestNegative8() {


        String dateTomorrow = Datetime.newInstance(Date.today().addDays(1).year(), Date.today().addDays(1).month(), Date.today().addDays(1).day()).format('yyyy-MM-dd');
        Resource__c objRes = [SELECT id FROM Resource__c WHERE Resource_Type__c = 'FM Amenity'];
        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenityBookingSlots';  
        req.addParameter('bookingDate', dateTomorrow);
        req.addParameter('amenityId',objRes.Id);
        req.addParameter('bookingUnitId',objBU.id);
        req.addParameter('isModify','true');
        req.addParameter('fmCaseId','');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        GetSlotsforAmenityBooking_API.getAmenitySlots();
        Test.stopTest();
    }

    @isTest
    static void getAmenitySlotsTestNegative9() {


        String dateTomorrow = Datetime.newInstance(Date.today().addDays(1).year(), Date.today().addDays(1).month(), Date.today().addDays(1).day()).format('yyyy-MM-dd');
        Resource__c objRes = [SELECT id FROM Resource__c WHERE Resource_Type__c = 'FM Amenity'];
        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenityBookingSlots';  
        req.addParameter('bookingDate', dateTomorrow);
        req.addParameter('amenityId',objRes.id);
        req.addParameter('bookingUnitId','1234');
        //req.addParameter('isModify','true');
        //req.addParameter('fmCaseId','');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        GetSlotsforAmenityBooking_API.getAmenitySlots();
        Test.stopTest();
    }

    @isTest
    static void getAmenitySlotsTestNegative10() {


        String dateTomorrow = Datetime.newInstance(Date.today().addDays(1).year(), Date.today().addDays(1).month(), Date.today().addDays(1).day()).format('yyyy-MM-dd');
        Resource__c objRes = [SELECT id FROM Resource__c WHERE Resource_Type__c = 'FM Amenity'];
        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenityBookingSlots';  
        req.addParameter('bookingDate', dateTomorrow);
        req.addParameter('amenityId','1234');
        req.addParameter('bookingUnitId',objBU.id);
        //req.addParameter('isModify','true');
        //req.addParameter('fmCaseId','');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        GetSlotsforAmenityBooking_API.getAmenitySlots();
        Test.stopTest();
    }

    
}