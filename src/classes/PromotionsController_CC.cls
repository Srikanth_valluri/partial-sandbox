public without sharing class PromotionsController_CC extends PromotionsController{
    public PromotionsController_CC() {
        super(false);
        if (CustomerCommunityUtils.isCurrentView('promotions')) {
            super();
        }
    }
    public override Pagereference createSR()
    {
        system.debug('totalPrice=='+totalPrice);
        system.debug('objUnit.Id=='+objUnit.Id);
        String strStatus;

        try
        {
            objCase.Booking_Unit__c = objUnit.Id;
            objCase.Pending_Amount__c = totalPrice;
            objCase.Total_Amount__c = totalPrice;

            Id promotionRecordTypeID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Promotions').getRecordTypeId();
            if(String.isNotBlank(strCaseStatusSubmitted) && strCaseStatusSubmitted.equalsIgnoreCase('Submitted'))
            {
                objCase.Status = 'Submitted';
            }
            else
            {
                objCase.Status = 'New';
            }
            objCase.RecordTypeID = promotionRecordTypeID;
            objCase.Subject = 'Promotions SR';
            objCase.Origin = 'Portal';
            objCase.SR_Type__c = 'Promotions';

            upsert objCase;

            List<Promotion_Package_Allocation__c> lstPromotionAllocation = new List<Promotion_Package_Allocation__c>();
            Set<Id> promotionPackageSetForDelete = new Set<Id>();
            Set<Id> bookingUnitSetForDelete = new Set<Id>();

            if(lstPromotions != null && lstPromotions.size() > 0)
            {
                Map<Id,Promotion_Package_Allocation__c> mapPromotionAllocation = new Map<Id,Promotion_Package_Allocation__c>();
                // get all promotion allocated to Booking Unit previously
                for(Promotion_Package_Allocation__c objAllocate : [  SELECT Id,Promotion_Package__c
                                                                     FROM
                                                                     Promotion_Package_Allocation__c
                                                                     WHERE
                                                                     Case__c =: objCase.Id
                                                                     AND Booking_Unit__c =: objUnit.Id])
                {
                    mapPromotionAllocation.put(objAllocate.Promotion_Package__c,objAllocate);
                }

                for(PromotionsWrapper objPromotion : lstPromotions)
                {
                    system.debug('mapPromotionAllocation contains '+!mapPromotionAllocation.containsKey(objPromotion.objPromotionOffer.Promotion_Package__c));
                    system.debug('objPromotion.blnIsSelected '+objPromotion.blnIsSelected);
                    if(objPromotion.blnIsSelected && !mapPromotionAllocation.containsKey(objPromotion.objPromotionOffer.Promotion_Package__c))
                    {
                        Promotion_Package_Allocation__c objPromotionAllocate = new Promotion_Package_Allocation__c();
                        objPromotionAllocate.Booking_Unit__c = objPromotion.objPromotionOffer.Booking_Unit__c;
                        objPromotionAllocate.Promotion_Package__c = objPromotion.objPromotionOffer.Promotion_Package__c;
                        objPromotionAllocate.Case__c = objCase.Id;

                        lstPromotionAllocation.add(objPromotionAllocate);
                    }
                    else if(!objPromotion.blnIsSelected && mapPromotionAllocation.containsKey(objPromotion.objPromotionOffer.Promotion_Package__c))
                    {
                        promotionPackageSetForDelete.add(objPromotion.objPromotionOffer.Promotion_Package__c);
                        bookingUnitSetForDelete.add(objPromotion.objPromotionOffer.Booking_Unit__c);
                    }
                }

                system.debug('lstPromotionAllocation '+lstPromotionAllocation);
                insert lstPromotionAllocation;

                List<Promotion_Package_Allocation__c> lstPromotionAllocationToDelete = getPromotionAllocation(
                                                                                                       promotionPackageSetForDelete
                                                                                                       ,bookingUnitSetForDelete
                                                                                                       ,objCase.Id);
                system.debug('lstPromotionAllocationToDelete '+lstPromotionAllocationToDelete);
                if(lstPromotionAllocationToDelete != null && lstPromotionAllocationToDelete.size() > 0)
                {
                    delete lstPromotionAllocationToDelete;
                }
            }
            else
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error : Please select atleast one Promotion.'));
                return null;
            }

            // map data to case object fields.
            if(objCase.Id != null)
            {
              strCaseID = objCase.Id;
              Case objInsertedCase = [SELECT Id,CaseNumber,OwnerId,RecordType.name,Status FROM Case WHERE id =: strCaseID LIMIT 1];

              strStatus = 'SR record created successfully.'+' '+'SR No:'+objInsertedCase.CaseNumber;
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Success : '+strStatus));
              system.debug('>>>>>>>>strCaseStatusSubmitted'+strCaseStatusSubmitted);
              if(String.isNotBlank(strCaseStatusSubmitted) && strCaseStatusSubmitted.equalsIgnoreCase('Submitted'))
              {
                // if SR is submitted create first task and submit for approval
                //PromotionsUtility.submitRecordForApproval(objInsertedCase);

                User objUser = new User();
                String strCaseOwner = String.valueOf(objInsertedCase.OwnerId);
                system.debug('strCaseOwner : '+strCaseOwner);
                system.debug('>>strCaseOwner.startsWith : ');
                system.debug('strCaseOwner.startsWith : '+strCaseOwner.startsWith('00G'));

                system.debug('Label.DefaultCaseOwnerId : '+Label.DefaultCaseOwnerId);
                if(strCaseOwner.startsWith('00G')){
                    objUser = [Select Manager_Role__c, Id From User where Id =: Label.DefaultCaseOwnerId];
                    if(objUser != null && String.isNotBlank(objUser.Manager_Role__c)){
                        objInsertedCase.Approving_Authorities__c = objUser.Manager_Role__c;
                        update objInsertedCase;
                    }
                }

                Task objTask = new Task();
                objTask = TaskUtility.getTask((SObject)objInsertedCase, 'Verify Case Details', 'CRE',
                               'Promotion Package', system.today().addDays(1));
                objTask.OwnerId = Label.DefaultCaseOwnerId;
                system.debug('--objTask--Promotions'+objTask);
                insert objTask;
                system.debug('>>>>>>>objTask'+objTask);
                return null;
              }
              else
              {
              	return null;
                //system.debug('save as draft clicked');
                //system.debug('strAccParamId '+strAccParamId);

                //Pagereference pg = Page.PromotionsProcessRequestPage;
                //pg.getParameters().put('AccountId',strAccParamId);
                //pg.getParameters().put('caseID',objCase.Id);
                //pg.getParameters().put('SRType','Promotions');
                //pg.setRedirect(true);
                //return pg;
              }
            }
            else
            {
                strStatus += 'SR record not created. <br/>';
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error : '+strStatus));
                return null;
            }
        }
        catch(Exception exp)
        {
            ApexPages.addmessage(new ApexPages.message(
                                      ApexPages.severity.Error,'Error - Submitting SR : '+exp.getMessage()));
        }
        return null;
    }
}