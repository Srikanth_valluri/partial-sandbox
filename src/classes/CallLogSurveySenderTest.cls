@isTest
public with sharing class CallLogSurveySenderTest {
    
    @isTest
    public static void testSendSurvey() {
        Account objAcc = new Account( Name = 'Test'
                                , Mobile__c = '0009122323232'
                                , Email__c = 'test.232@t.com');
        insert objAcc;
        Call_Log__c objCall = new Call_Log__c( Account__c = objAcc.Id, Calling_Number__c='009829392');
        insert objCall;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RebrandlyServiceHttpMock() );
        CallLogSurveySender.sendSurvey( new List<Id>{objCall.Id} );
        Test.stopTest();
    }
}