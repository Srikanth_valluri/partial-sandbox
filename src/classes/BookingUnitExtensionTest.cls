/*
* Description - Test class developed for 'BookingUnitExtension'
*
* Version            Date            Author            Description
* 1.0                26/11/17        Lochana            Initial Draft
* 1.1                30/11/17        Monali             Updated for Payment details
*/
@isTest
private class BookingUnitExtensionTest 
{

    static testMethod void testMethod1() 
    {   
        //insert sample data for Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        
        Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id aoptRecordTypeID = caseRecordTypes.get('AOPT').getRecordTypeId();

        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , aoptRecordTypeID);
        objCase.Status = 'Submitted';
        insert objCase;

        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        Account objAcc = [SELECT Id from Account LIMIT 1];
        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        
        List<Booking__c> lstBookings = new List<Booking__c>();
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 1);
        insert lstBookings;
        
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 1);
        insert lstBookingUnits;
        
        list<Buyer__c> lstBuyer = new list<Buyer__c>();
        lstBuyer = TestDataFactory_CRM.createBuyer(lstBookingUnits[0].Booking__c,5, objAcc.Id);
        insert lstBuyer;
        
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT());
        PageReference pageRef = Page.BookingUnit_PDCInformation;
        pageRef.getParameters().put('id', String.valueOf(lstBookingUnits[0].Id));
        pageRef.getParameters().put('AccountId', String.valueOf(objAcc.Id));
        pageRef.getParameters().put('SRType', 'FundTransferActiveUnits');
        
        Test.setCurrentPage(pageRef);
        Test.startTest();
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAssignment() );
        ApexPages.StandardController sc = new ApexPages.StandardController(lstBookingUnits[0]);
        BookingUnitExtension controller = new BookingUnitExtension(sc);
        controller.init();
        Test.stopTest();
    }

    static testMethod void testMethod2() 
    {   
        //insert sample data for Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        
        Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id aoptRecordTypeID = caseRecordTypes.get('AOPT').getRecordTypeId();

        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , aoptRecordTypeID);
        objCase.Status = 'Submitted';
        insert objCase;

        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        Account objAcc = [SELECT Id from Account LIMIT 1];
        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        
        List<Booking__c> lstBookings = new List<Booking__c>();
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 1);
        insert lstBookings;
        
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 1);
        insert lstBookingUnits;
        
        list<Buyer__c> lstBuyer = new list<Buyer__c>();
        lstBuyer = TestDataFactory_CRM.createBuyer(lstBookingUnits[0].Booking__c,5, objAcc.Id);
        insert lstBuyer;
        
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT());
        PageReference pageRef = Page.BookingUnit_PDCInformation;
        pageRef.getParameters().put('id', String.valueOf(lstBookingUnits[0].Id));
        pageRef.getParameters().put('AccountId', String.valueOf(objAcc.Id));
        pageRef.getParameters().put('SRType', 'FundTransferActiveUnits');
        
        Test.setCurrentPage(pageRef);
        Test.startTest();
        /*Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1) );*/

        SOAPCalloutServiceMock.returnToMe = new Map<String, AssignmentProcessWSDL.getPDCDetailsResponse_element>();
        AssignmentProcessWSDL.getPDCDetailsResponse_element response1 = new AssignmentProcessWSDL.getPDCDetailsResponse_element();
        response1.return_x = '{"data":[{"ATTRIBUTE3":"Kanchan Mahajan","ATTRIBUTE10":"10000","ATTRIBUTE2":"176374","ATTRIBUTE1":"57020","ATTRIBUTE9":"0","ATTRIBUTE8":null,"ATTRIBUTE7":null,"ATTRIBUTE6":"05-JUN-2014","ATTRIBUTE5":"Receipt","ATTRIBUTE4":"Cash"}],"message":"[45] Customer Statement Fetched for Reg Id =57020","status":"S"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );

        ApexPages.StandardController sc = new ApexPages.StandardController(lstBookingUnits[0]);
        BookingUnitExtension controller = new BookingUnitExtension(sc);
        controller.ParsePDCDetails();
        BookingUnitExtension.pdcDetailsWrapper objpdcDetailsWrapper = new BookingUnitExtension.pdcDetailsWrapper();
        objpdcDetailsWrapper.ATTRIBUTE1='';
        objpdcDetailsWrapper.ATTRIBUTE2='';
        objpdcDetailsWrapper.ATTRIBUTE3='';
        objpdcDetailsWrapper.ATTRIBUTE4='';
        objpdcDetailsWrapper.ATTRIBUTE5='';
        objpdcDetailsWrapper.ATTRIBUTE6='';
        objpdcDetailsWrapper.ATTRIBUTE7='';
        objpdcDetailsWrapper.ATTRIBUTE8='';

        SOAPCalloutServiceMock.returnToMe = new Map<String, AssignmentProcessWSDL.getPDCDetailsResponse_element>();
        AssignmentProcessWSDL.getPDCDetailsResponse_element response2 = new AssignmentProcessWSDL.getPDCDetailsResponse_element();
        response2.return_x = '{"data":[{"ATTRIBUTE3":"Kanchan Mahajan","ATTRIBUTE10":"10000","ATTRIBUTE2":"176374","ATTRIBUTE1":"57020","ATTRIBUTE9":"0","ATTRIBUTE8":null,"ATTRIBUTE7":null,"ATTRIBUTE6":"05-JUN-2014","ATTRIBUTE5":"Receipt","ATTRIBUTE4":"Cash"}],"message":"[45] Customer Statement Fetched for Reg Id =57020","status":"E"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response2);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        controller.ParsePDCDetails();

        Test.stopTest();
    }

    static testMethod void testMethod3() 
    {   
        //insert sample data for Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        
        Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id aoptRecordTypeID = caseRecordTypes.get('AOPT').getRecordTypeId();

        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , aoptRecordTypeID);
        objCase.Status = 'Submitted';
        insert objCase;

        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        Account objAcc = [SELECT Id from Account LIMIT 1];
        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        
        List<Booking__c> lstBookings = new List<Booking__c>();
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 1);
        insert lstBookings;
        
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 1);
        insert lstBookingUnits;
        
        list<Buyer__c> lstBuyer = new list<Buyer__c>();
        lstBuyer = TestDataFactory_CRM.createBuyer(lstBookingUnits[0].Booking__c,5, objAcc.Id);
        insert lstBuyer;
        
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT());
        PageReference pageRef = Page.BookingUnit_PDCInformation;
        pageRef.getParameters().put('id', String.valueOf(lstBookingUnits[0].Id));
        pageRef.getParameters().put('AccountId', String.valueOf(objAcc.Id));
        pageRef.getParameters().put('SRType', 'FundTransferActiveUnits');
        
        Test.setCurrentPage(pageRef);
        Test.startTest();
        /*Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1) );*/

        SOAPCalloutServiceMock.returnToMe = new Map<String, PaymentMade1.customerPaymentMadeResponse_element>();
        PaymentMade1.customerPaymentMadeResponse_element response1 = new PaymentMade1.customerPaymentMadeResponse_element();
        response1.return_x = '{"data":[{"ATTRIBUTE3":"Kanchan Mahajan","ATTRIBUTE10":"10000","ATTRIBUTE2":"176374","ATTRIBUTE1":"57020","ATTRIBUTE9":"0","ATTRIBUTE8":null,"ATTRIBUTE7":null,"ATTRIBUTE6":"05-JUN-2014","ATTRIBUTE5":"Receipt","ATTRIBUTE4":"Cash"}],"message":"[45] Customer Statement Fetched for Reg Id =57020","status":"S"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() ); 

        ApexPages.StandardController sc = new ApexPages.StandardController(lstBookingUnits[0]);
        BookingUnitExtension controller = new BookingUnitExtension(sc);
        controller.ParseCustomerPayment();
        BookingUnitExtension.PaymentWrapper objPaymentWrapper = new BookingUnitExtension.PaymentWrapper();
        objPaymentWrapper.ATTRIBUTE1='';
        objPaymentWrapper.ATTRIBUTE2='';
        objPaymentWrapper.ATTRIBUTE3='';
        objPaymentWrapper.ATTRIBUTE4='';
        objPaymentWrapper.ATTRIBUTE5='';
        objPaymentWrapper.ATTRIBUTE6='';
        objPaymentWrapper.ATTRIBUTE7='';
        objPaymentWrapper.ATTRIBUTE8='';
        objPaymentWrapper.ATTRIBUTE9='';
        objPaymentWrapper.ATTRIBUTE10='';

        SOAPCalloutServiceMock.returnToMe = new Map<String, PaymentMade1.customerPaymentMadeResponse_element>();
        PaymentMade1.customerPaymentMadeResponse_element response2 = new PaymentMade1.customerPaymentMadeResponse_element();
        response2.return_x = '{"data":[{"ATTRIBUTE3":"Kanchan Mahajan","ATTRIBUTE10":"10000","ATTRIBUTE2":"176374","ATTRIBUTE1":"57020","ATTRIBUTE9":"0","ATTRIBUTE8":null,"ATTRIBUTE7":null,"ATTRIBUTE6":"05-JUN-2014","ATTRIBUTE5":"Receipt","ATTRIBUTE4":"Cash"}],"message":"[45] Customer Statement Fetched for Reg Id =57020","status":"E"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response2);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        controller.ParseCustomerPayment(); 

        Test.stopTest();
    }

}