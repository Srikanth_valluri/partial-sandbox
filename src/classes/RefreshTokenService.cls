public without sharing class RefreshTokenService {
    
    public static AgingBucketResponse getAgingbucket(String regId){
        CollectionUpdateService.CallingListHttpSoap11Endpoint calloutObj = new CollectionUpdateService.CallingListHttpSoap11Endpoint();
        calloutObj.timeout_x = 120000;
        AgingBucketResponse resObj = new AgingBucketResponse();

        try{
            String response = calloutObj.getCollectionCallingListIPMS( '2-'+String.valueOf( Datetime.now().getTime()),regId, '', '', '', '', '', '', '' );
            system.debug('== aging bucket =='+response );
            if(!String.isBlank(response)) {
            	resObj = (AgingBucketResponse)JSON.deserialize(response, RefreshTokenService.AgingBucketResponse.class);
            }
            system.debug('resObj response === '+ resObj);
        } catch ( Exception e ){
            system.debug('e '+ e);
            resObj.ATTRIBUTE13 = 'Exception : ' + e.getMessage() ;
      }
        
        return resObj;
    }


    public class AgingBucketResponse{
        public String ATTRIBUTE2 		{get;set;}
        public String ATTRIBUTE5 		{get;set;}
		public String ATTRIBUTE6 		{get;set;}
		public String ATTRIBUTE13 		{get;set;}
		public String ATTRIBUTE14 		{get;set;}
		public String ATTRIBUTE15 		{get;set;}
		public String ATTRIBUTE16 		{get;set;}
		public String ATTRIBUTE17 		{get;set;}
		public String ATTRIBUTE18 		{get;set;}
		public String ATTRIBUTE19 		{get;set;}
		public String ATTRIBUTE20 		{get;set;}
		public String ATTRIBUTE21 		{get;set;}	
		public String ATTRIBUTE22 		{get;set;}
		public String ATTRIBUTE23		{get;set;}
    }
}