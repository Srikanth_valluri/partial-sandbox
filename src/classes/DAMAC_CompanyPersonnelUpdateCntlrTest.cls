@isTest
private class DAMAC_CompanyPersonnelUpdateCntlrTest {

     @isTest static void test_method_one() {
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test456', email='xyz1@email.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz1@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
        
        Page_Flow__c pageFlow1 = InitialiseTestData.createPageFlow();
        insert pageFlow1 ;

        Account agency1 = InitialiseTestData.getCorporateAccount('Agency129');
        insert agency1;

        Page__c page1 = InitialiseTestData.createPageForPageFlow('Process_Flow',pageFlow1.Id,1);
        insert page1;

        List<NSIBPM__SR_Template__c> SRTemplateList =  InitialiseTestData.createTestTemplateRecords(new List<NSIBPM__SR_Template__c>{new NSIBPM__SR_Template__c()});
        List<NSIBPM__SR_Template__c> SRTemplateListNew = new List<NSIBPM__SR_Template__c>();
        for(NSIBPM__SR_Template__c tempObj :SRTemplateList){
            tempObj.NSIBPM__SR_RecordType_API_Name__c = 'Agent_Update';
            SRTemplateListNew.add(tempObj);
        }
        update SRTemplateListNew;
        List<NSIBPM__Service_Request__c> SRList  = InitialiseTestData.createTestServiceRequestRecords(new List<NSIBPM__Service_Request__c>{
                    new NSIBPM__Service_Request__c(recordTypeId = InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Update'),
                                                    NSIBPM__SR_Template__c = SRTemplateList[0].Id)});

        NSIBPM__Service_Request__c serviceRequest = SRList[0];
        serviceRequest.NSIBPM__Customer__c = agency1.id;

        
        upsert serviceRequest;

        List<NSIBPM__Status__c> createStatus = new List<NSIBPM__Status__c>();
        createStatus = InitialiseTestData.createStatusRecords(
                new List<NSIBPM__Status__c>{
                    new NSIBPM__Status__c(NSIBPM__Code__c = 'AWAITING_ADDITIONAL_INFO', Name = 'AWAITING_ADDITIONAL_INFO'),
                    new NSIBPM__Status__c(NSIBPM__Code__c = 'MORE_INFO_UPDATED', Name = 'MORE_INFO_UPDATED'),
                    new NSIBPM__Status__c(NSIBPM__Code__c = 'AWAITING_FFA_AA', Name = 'AWAITING_FFA_AA'),
                    new NSIBPM__Status__c(NSIBPM__Code__c = 'FFA_AA_UPLOADED', Name = 'FFA_AA_UPLOADED')});

        NSIBPM__SR_Steps__c srStep= new NSIBPM__SR_Steps__c();
        insert srStep;



        Contact newCont = new Contact();
        newCont.LastName = 'Agency129';
        newCont.AccountId = agency1.Id ;

        insert newCont ;

        User portalUser  = InitialiseTestData.getPortalUser('portalIdDamac1@test.com',newCont.Id);
        insert portalUser ;


        List<NSIBPM__Step__c> createStepList = InitialiseTestData.createTestStepRecords(
            new List<NSIBPM__Step__c>{
                new NSIBPM__Step__c(NSIBPM__SR__c = SRList[0].id, NSIBPM__Status__c = createStatus[0].Id, NSIBPM__SR_Step__c = srStep.id),
                new NSIBPM__Step__c(NSIBPM__SR__c = SRList[0].id, NSIBPM__Status__c = createStatus[1].Id, NSIBPM__SR_Step__c = srStep.id)});

        List<Amendment__c> amdList = new List<Amendment__c>();
       for(integer i=0;i<5;i++){
           Amendment__c newAmd =  new Amendment__c();
           newAmd.First_Name__c = 'For -> ' + i;
           if(i==0){
               newAmd.Owner__c = true;
               newAmd.Shareholding__c =100;
           }

           newAmd.Authorised_Signatory__c = true;
           newAmd.Agent_Representative__c =true;
           newAmd.Portal_Administrator__c = true;
           newAmd.Email__c ='test'+i+'@test.com';
           newAmd.ID_Type__c = 'Visa';
           amdList.add(newAmd);
       }
       insert amdList;



        List<NSIBPM__Document_Master__c> DM_List = InitialiseTestData.createSRDocMasterRecords(5);
        NSIBPM__SR_Template_Docs__c SrTemplateDoc = new NSIBPM__SR_Template_Docs__c();
        SrTemplateDoc.NSIBPM__SR_Template__c = SRTemplateList[0].Id;
        SrTemplateDoc.NSIBPM__Document_Master__c = DM_List[0].Id;
        SrTemplateDoc.NSIBPM__Added_through_Code__c = true;
        SrTemplateDoc.NSIBPM__On_Submit__c = true;
        insert SrTemplateDoc;

        Amendment__c newAmd1 =  new Amendment__c();
        newAmd1.First_Name__c = 'test 1';
        newAmd1.Authorised_Signatory__c = true;
        newAmd1.Agent_Representative__c =true;
        newAmd1.Portal_Administrator__c = true;
        newAmd1.Email__c ='test34@test.com';
        newAmd1.ID_Type__c = 'Visa';
        newAmd1.Mark_For_Deletion__c = false;
        newAmd1.ID_Number__c = '12345';

        insert newAmd1 ;

        Amendment__c newAmd2 =  new Amendment__c();
        newAmd2.First_Name__c = 'test 2';
        newAmd2.Last_Name__c = 'lastname';
        newAmd2.Authorised_Signatory__c = true;
        newAmd2.Agent_Representative__c =true;
        newAmd2.Portal_Administrator__c = true;
        newAmd2.Email__c ='test34@test.com';
        newAmd2.ID_Type__c = 'Visa';
        newAmd2.Mark_For_Deletion__c = true;
        newAmd2.ID_Number__c = '12345';
        insert newAmd2;

        Contact newCon =  new Contact();
        newCon.FirstName = 'test 2';
        newCon.LastName = 'lastname';
        newCon.ID_Number__c = '12345';
        newCon.AccountId = agency1.Id;
        insert newCon ;

        Amendment__c newAmd6 =  new Amendment__c();
        newAmd6.First_Name__c = 'test 2';
        newAmd6.Last_Name__c = 'lastname';
        newAmd6.Authorised_Signatory__c = true;
        newAmd6.Agent_Representative__c =true;
        newAmd6.Portal_Administrator__c = true;
        newAmd6.Email__c ='test34@test.com';
        newAmd6.ID_Type__c = 'Visa';
        newAmd6.Contact__c = newCont.Id;
        newAmd6.Mark_For_Deletion__c = true;
        newAmd6.ID_Number__c = '12345';
        newAmd6.Account__c = agency1.Id;
        newAmd6.ID_Issue_Date__c = System.Today();
        newAmd6.ID_Expiry_Date__c = System.Today().addDays(100);
        newAmd6.Designation__c = 'CEo';
        insert newAmd6;

        Amendment__c newAmd3 =  new Amendment__c();
        newAmd3.First_Name__c = 'test 2';
        newAmd3.Last_Name__c = 'lastname';
        newAmd3.Authorised_Signatory__c = true;
        newAmd3.Agent_Representative__c =true;
        newAmd3.Portal_Administrator__c = true;
        newAmd3.Email__c ='test34@test.com';
        newAmd3.ID_Type__c = 'Visa';
        newAmd3.Contact__c = newCont.Id;
        newAmd3.Mark_For_Deletion__c = true;
        newAmd3.ID_Number__c = '12345';
        newAmd3.Account__c = agency1.Id;
        insert newAmd3;

        Amendment__c newAmd7 =  new Amendment__c();

        Amendment__c newAmd5 =  new Amendment__c();
        newAmd5.Owner__c = true;
        newAmd5.Agent_Representative__c = true;
        newAmd5.Broker_Card_Number__c = null;
        newAmd5.Broker_Card_Expiry_Date__c = NULL;

        Amendment__c newAmd4 =  new Amendment__c();
        newAmd4.First_Name__c = 'test 2';
        newAmd4.Last_Name__c = 'lastname';
        newAmd4.ID_Issue_Date__c = Date.newInstance(2016, 12, 9);
        newAmd4.ID_Expiry_Date__c = Date.newInstance(2222, 12, 9);
        newAmd4.Owner__c = true;
        newAmd4.Shareholding__c = 100;
        newAmd4.Designation__c = 'CEO';
        newAmd4.Authorised_Signatory__c = true;
        newAmd4.Portal_Administrator__c = true;
        newAmd4.Email__c ='test34@test.com';
        newAmd4.ID_Type__c = 'Passport';
        newAmd4.Contact__c = newCont.Id;
        newAmd4.Mark_For_Deletion__c = false;
        newAmd4.ID_Number__c = '12345';
        newAmd4.Account__c = agency1.Id;
        insert newAmd4;

        String docName;
        for(Integer i = 0; i < 99; i++) {
            docName = docName + 'test';
        }

        Section__c sec = new Section__c();
        sec.page__c = page1.id;
        insert sec;

        Section_Detail__c d = new Section_Detail__c();
        d.section__c = sec.id;
        d.Navigation_Directions__c = 'Forward';
        insert d;
        
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.Process_Flow'));
        System.currentPageReference().getParameters().put('Type', 'Agent_Registration');
        System.currentPageReference().getParameters().put('FlowId', String.valueOf(pageFlow1.Id));
        System.currentPageReference().getParameters().put('PageId', String.valueOf(page1.Id));
        System.currentPageReference().getParameters().put('Id', serviceRequest.Id);
        system.debug('serviceRequest.NSIBPM__Customer__c >>>>>>  ' + serviceRequest.NSIBPM__Customer__c);
        ApexPages.StandardController sc = new ApexPages.StandardController(serviceRequest);
        DAMAC_CompanyPersonnelUpdateCntlr updatePage = new DAMAC_CompanyPersonnelUpdateCntlr(sc);

        updatePage.currAmdId = newAmd1.Id ;
        updatePage.currAmnd = newAmd1;
        updatePage.getAllAmendments();
        updatePage.createAmd();
        updatePage.editAmd();
        updatePage.cancel();
        updatePage.init();
        //updatePage.createDocuments(amdList[0]);
        //updatePage.deleteSRDocs(amdList[0],'Upsert');
        //updatePage.deleteSRDocs(amdList[0],'Edit');
        updatePage.validateAmendment(amdList[0]);
        //updatePage.createSRDoc('Test Doc',SrTemplateDoc,amdList[0].Id);
        updatePage.disableContact(newCont.Id,amdList[0]);
        updatePage.validateAmendment(amdList[0]);
        updatePage.deleteAmd();
        
        //updatePage.strNavigatePageId = page1.id;
        //updatePage.goTopage();
        //Boolean testDuplicateContacts = updatePage.duplicateContactCheck(null);
        //updatePage.EditSectionDetails();
        //updatePage.strActionId = d.id;
        //updatePage.DynamicButtonAction();
        //updatePage.getDyncPgMainPBBottom();
        //updatePage.getDyncPgMainPBTop();
        //updatePage.getDyncPgMainPBSection();
        //updatePage.getDyncPgMainPB();
        Test.stopTest();
        }
    }
    
    
    
    @isTest static void test_method_one_parttwo() {
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test456', email='xyz1@email.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz1@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
        
        Page_Flow__c pageFlow1 = InitialiseTestData.createPageFlow();
        insert pageFlow1 ;

        Account agency1 = InitialiseTestData.getCorporateAccount('Agency129');
        insert agency1;

        Page__c page1 = InitialiseTestData.createPageForPageFlow('Process_Flow',pageFlow1.Id,1);
        insert page1;

        List<NSIBPM__SR_Template__c> SRTemplateList =  InitialiseTestData.createTestTemplateRecords(new List<NSIBPM__SR_Template__c>{new NSIBPM__SR_Template__c()});
        List<NSIBPM__SR_Template__c> SRTemplateListNew = new List<NSIBPM__SR_Template__c>();
        for(NSIBPM__SR_Template__c tempObj :SRTemplateList){
            tempObj.NSIBPM__SR_RecordType_API_Name__c = 'Agent_Update';
            SRTemplateListNew.add(tempObj);
        }
        update SRTemplateListNew;
        List<NSIBPM__Service_Request__c> SRList  = InitialiseTestData.createTestServiceRequestRecords(new List<NSIBPM__Service_Request__c>{
                    new NSIBPM__Service_Request__c(recordTypeId = InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Update'),
                                                    NSIBPM__SR_Template__c = SRTemplateList[0].Id)});

        NSIBPM__Service_Request__c serviceRequest = SRList[0];
        serviceRequest.NSIBPM__Customer__c = agency1.id;

        
        upsert serviceRequest;

        List<NSIBPM__Status__c> createStatus = new List<NSIBPM__Status__c>();
        createStatus = InitialiseTestData.createStatusRecords(
                new List<NSIBPM__Status__c>{
                    new NSIBPM__Status__c(NSIBPM__Code__c = 'AWAITING_ADDITIONAL_INFO', Name = 'AWAITING_ADDITIONAL_INFO'),
                    new NSIBPM__Status__c(NSIBPM__Code__c = 'MORE_INFO_UPDATED', Name = 'MORE_INFO_UPDATED'),
                    new NSIBPM__Status__c(NSIBPM__Code__c = 'AWAITING_FFA_AA', Name = 'AWAITING_FFA_AA'),
                    new NSIBPM__Status__c(NSIBPM__Code__c = 'FFA_AA_UPLOADED', Name = 'FFA_AA_UPLOADED')});

        NSIBPM__SR_Steps__c srStep= new NSIBPM__SR_Steps__c();
        insert srStep;



        Contact newCont = new Contact();
        newCont.LastName = 'Agency129';
        newCont.AccountId = agency1.Id ;

        insert newCont ;

        User portalUser  = InitialiseTestData.getPortalUser('portalIdDamac1@test.com',newCont.Id);
        insert portalUser ;


        List<NSIBPM__Step__c> createStepList = InitialiseTestData.createTestStepRecords(
            new List<NSIBPM__Step__c>{
                new NSIBPM__Step__c(NSIBPM__SR__c = SRList[0].id, NSIBPM__Status__c = createStatus[0].Id, NSIBPM__SR_Step__c = srStep.id),
                new NSIBPM__Step__c(NSIBPM__SR__c = SRList[0].id, NSIBPM__Status__c = createStatus[1].Id, NSIBPM__SR_Step__c = srStep.id)});

        List<Amendment__c> amdList = new List<Amendment__c>();
       for(integer i=0;i<5;i++){
           Amendment__c newAmd =  new Amendment__c();
           newAmd.First_Name__c = 'For -> ' + i;
           if(i==0){
               newAmd.Owner__c = true;
               newAmd.Shareholding__c =100;
           }

           newAmd.Authorised_Signatory__c = true;
           newAmd.Agent_Representative__c =true;
           newAmd.Portal_Administrator__c = true;
           newAmd.Email__c ='test'+i+'@test.com';
           newAmd.ID_Type__c = 'Visa';
           amdList.add(newAmd);
       }
       insert amdList;



        List<NSIBPM__Document_Master__c> DM_List = InitialiseTestData.createSRDocMasterRecords(5);
        NSIBPM__SR_Template_Docs__c SrTemplateDoc = new NSIBPM__SR_Template_Docs__c();
        SrTemplateDoc.NSIBPM__SR_Template__c = SRTemplateList[0].Id;
        SrTemplateDoc.NSIBPM__Document_Master__c = DM_List[0].Id;
        SrTemplateDoc.NSIBPM__Added_through_Code__c = true;
        SrTemplateDoc.NSIBPM__On_Submit__c = true;
        insert SrTemplateDoc;

        Amendment__c newAmd1 =  new Amendment__c();
        newAmd1.First_Name__c = 'test 1';
        newAmd1.Authorised_Signatory__c = true;
        newAmd1.Agent_Representative__c =true;
        newAmd1.Portal_Administrator__c = true;
        newAmd1.Email__c ='test34@test.com';
        newAmd1.ID_Type__c = 'Visa';
        newAmd1.Mark_For_Deletion__c = false;
        newAmd1.ID_Number__c = '12345';

        insert newAmd1 ;

        Amendment__c newAmd2 =  new Amendment__c();
        newAmd2.First_Name__c = 'test 2';
        newAmd2.Last_Name__c = 'lastname';
        newAmd2.Authorised_Signatory__c = true;
        newAmd2.Agent_Representative__c =true;
        newAmd2.Portal_Administrator__c = true;
        newAmd2.Email__c ='test34@test.com';
        newAmd2.ID_Type__c = 'Visa';
        newAmd2.Mark_For_Deletion__c = true;
        newAmd2.ID_Number__c = '12345';
        insert newAmd2;

        Contact newCon =  new Contact();
        newCon.FirstName = 'test 2';
        newCon.LastName = 'lastname';
        newCon.ID_Number__c = '12345';
        newCon.AccountId = agency1.Id;
        insert newCon ;

        Amendment__c newAmd6 =  new Amendment__c();
        newAmd6.First_Name__c = 'test 2';
        newAmd6.Last_Name__c = 'lastname';
        newAmd6.Authorised_Signatory__c = true;
        newAmd6.Agent_Representative__c =true;
        newAmd6.Portal_Administrator__c = true;
        newAmd6.Email__c ='test34@test.com';
        newAmd6.ID_Type__c = 'Visa';
        newAmd6.Contact__c = newCont.Id;
        newAmd6.Mark_For_Deletion__c = true;
        newAmd6.ID_Number__c = '12345';
        newAmd6.Account__c = agency1.Id;
        newAmd6.ID_Issue_Date__c = System.Today();
        newAmd6.ID_Expiry_Date__c = System.Today().addDays(100);
        newAmd6.Designation__c = 'CEo';
        insert newAmd6;

        Amendment__c newAmd3 =  new Amendment__c();
        newAmd3.First_Name__c = 'test 2';
        newAmd3.Last_Name__c = 'lastname';
        newAmd3.Authorised_Signatory__c = true;
        newAmd3.Agent_Representative__c =true;
        newAmd3.Portal_Administrator__c = true;
        newAmd3.Email__c ='test34@test.com';
        newAmd3.ID_Type__c = 'Visa';
        newAmd3.Contact__c = newCont.Id;
        newAmd3.Mark_For_Deletion__c = true;
        newAmd3.ID_Number__c = '12345';
        newAmd3.Account__c = agency1.Id;
        insert newAmd3;

        Amendment__c newAmd7 =  new Amendment__c();

        Amendment__c newAmd5 =  new Amendment__c();
        newAmd5.Owner__c = true;
        newAmd5.Agent_Representative__c = true;
        newAmd5.Broker_Card_Number__c = null;
        newAmd5.Broker_Card_Expiry_Date__c = NULL;

        Amendment__c newAmd4 =  new Amendment__c();
        newAmd4.First_Name__c = 'test 2';
        newAmd4.Last_Name__c = 'lastname';
        newAmd4.ID_Issue_Date__c = Date.newInstance(2016, 12, 9);
        newAmd4.ID_Expiry_Date__c = Date.newInstance(2222, 12, 9);
        newAmd4.Owner__c = true;
        newAmd4.Shareholding__c = 100;
        newAmd4.Designation__c = 'CEO';
        newAmd4.Authorised_Signatory__c = true;
        newAmd4.Portal_Administrator__c = true;
        newAmd4.Email__c ='test34@test.com';
        newAmd4.ID_Type__c = 'Passport';
        newAmd4.Contact__c = newCont.Id;
        newAmd4.Mark_For_Deletion__c = false;
        newAmd4.ID_Number__c = '12345';
        newAmd4.Account__c = agency1.Id;
        insert newAmd4;

        String docName;
        for(Integer i = 0; i < 99; i++) {
            docName = docName + 'test';
        }

        Section__c sec = new Section__c();
        sec.page__c = page1.id;
        insert sec;

        Section_Detail__c d = new Section_Detail__c();
        d.section__c = sec.id;
        d.Navigation_Directions__c = 'Forward';
        insert d;
        
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.Process_Flow'));
        System.currentPageReference().getParameters().put('Type', 'Agent_Registration');
        System.currentPageReference().getParameters().put('FlowId', String.valueOf(pageFlow1.Id));
        System.currentPageReference().getParameters().put('PageId', String.valueOf(page1.Id));
        System.currentPageReference().getParameters().put('Id', serviceRequest.Id);
        system.debug('serviceRequest.NSIBPM__Customer__c >>>>>>  ' + serviceRequest.NSIBPM__Customer__c);
        ApexPages.StandardController sc = new ApexPages.StandardController(serviceRequest);
        DAMAC_CompanyPersonnelUpdateCntlr updatePage = new DAMAC_CompanyPersonnelUpdateCntlr(sc);

        updatePage.currAmdId = newAmd1.Id ;
        updatePage.currAmnd = newAmd1;
        //updatePage.getAllAmendments();
        //updatePage.createAmd();
        //updatePage.editAmd();
        //updatePage.cancel();
        //updatePage.init();
        //updatePage.createDocuments(amdList[0]);
        //updatePage.deleteSRDocs(amdList[0],'Upsert');
        //updatePage.deleteSRDocs(amdList[0],'Edit');
        //updatePage.validateAmendment(amdList[0]);
        //updatePage.createSRDoc('Test Doc',SrTemplateDoc,amdList[0].Id);
        //updatePage.disableContact(newCont.Id,amdList[0]);
        //updatePage.validateAmendment(amdList[0]);
        //updatePage.deleteAmd();
        
        updatePage.strNavigatePageId = page1.id;
        updatePage.goTopage();
        Boolean testDuplicateContacts = updatePage.duplicateContactCheck(null);
        updatePage.EditSectionDetails();
        updatePage.strActionId = d.id;
        updatePage.DynamicButtonAction();
        updatePage.getDyncPgMainPBBottom();
        updatePage.getDyncPgMainPBTop();
        updatePage.getDyncPgMainPBSection();
        updatePage.getDyncPgMainPB();
        Test.stopTest();
        }
    }


    @isTest static void test_method_two() {
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test456', email='xyz2@email.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz2@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
       
       Page_Flow__c pageFlow1 = InitialiseTestData.createPageFlow();
       insert pageFlow1 ;

       Account agency1 = InitialiseTestData.getCorporateAccount('Agency129');
       insert agency1;

       Page__c page1 = InitialiseTestData.createPageForPageFlow('Process_Flow',pageFlow1.Id,1);
       insert page1;

       List<NSIBPM__SR_Template__c> SRTemplateList =  InitialiseTestData.createTestTemplateRecords(new List<NSIBPM__SR_Template__c>{new NSIBPM__SR_Template__c()});

       List<NSIBPM__Service_Request__c> SRList  = InitialiseTestData.createTestServiceRequestRecords(new List<NSIBPM__Service_Request__c>{
                   new NSIBPM__Service_Request__c(recordTypeId = InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Update'),
                                                   NSIBPM__SR_Template__c = SRTemplateList[0].Id)});

       NSIBPM__Service_Request__c serviceRequest = SRList[0];
       serviceRequest.NSIBPM__Customer__c = agency1.id;
       serviceRequest.SR_Type__c  = 'Company SR';
       upsert serviceRequest;

       List<NSIBPM__Status__c> createStatus = new List<NSIBPM__Status__c>();
       createStatus = InitialiseTestData.createStatusRecords(
               new List<NSIBPM__Status__c>{
                   new NSIBPM__Status__c(NSIBPM__Code__c = 'AWAITING_ADDITIONAL_INFO', Name = 'AWAITING_ADDITIONAL_INFO'),
                   new NSIBPM__Status__c(NSIBPM__Code__c = 'MORE_INFO_UPDATED', Name = 'MORE_INFO_UPDATED'),
                   new NSIBPM__Status__c(NSIBPM__Code__c = 'AWAITING_FFA_AA', Name = 'AWAITING_FFA_AA'),
                   new NSIBPM__Status__c(NSIBPM__Code__c = 'FFA_AA_UPLOADED', Name = 'FFA_AA_UPLOADED')});

       NSIBPM__SR_Steps__c srStep= new NSIBPM__SR_Steps__c();
       insert srStep;

       Contact newCont = new Contact();
       newCont.LastName = 'Agency129';
       newCont.AccountId = agency1.Id ;

       insert newCont ;

       User portalUser  = InitialiseTestData.getPortalUser('portalIdDamac1@test.com',newCont.Id);
       insert portalUser ;

       List<NSIBPM__Step__c> createStepList = InitialiseTestData.createTestStepRecords(
           new List<NSIBPM__Step__c>{
               new NSIBPM__Step__c(NSIBPM__SR__c = SRList[0].id, NSIBPM__Status__c = createStatus[0].Id, NSIBPM__SR_Step__c = srStep.id),
               new NSIBPM__Step__c(NSIBPM__SR__c = SRList[0].id, NSIBPM__Status__c = createStatus[1].Id, NSIBPM__SR_Step__c = srStep.id)});

       List<Amendment__c> amdList = new List<Amendment__c>();
      for(integer i=0;i<5;i++){
          Amendment__c newAmd =  new Amendment__c();
          newAmd.First_Name__c = 'For -> ' + i;
          if(i==0){
              newAmd.Owner__c = true;
              newAmd.Shareholding__c =100;
          }

          newAmd.Authorised_Signatory__c = true;
          newAmd.Agent_Representative__c =true;
          newAmd.Portal_Administrator__c = true;
          newAmd.Email__c ='test'+i+'@test.com';
          newAmd.ID_Type__c = 'Visa';
          amdList.add(newAmd);
      }
      insert amdList;

       List<NSIBPM__Document_Master__c> DM_List = InitialiseTestData.createSRDocMasterRecords(5);
       NSIBPM__SR_Template_Docs__c SrTemplateDoc = new NSIBPM__SR_Template_Docs__c();
       SrTemplateDoc.NSIBPM__SR_Template__c = SRTemplateList[0].Id;
       SrTemplateDoc.NSIBPM__Document_Master__c = DM_List[0].Id;
       SrTemplateDoc.NSIBPM__Added_through_Code__c = true;
       SrTemplateDoc.NSIBPM__On_Submit__c = true;
       insert SrTemplateDoc;

       Amendment__c newAmd1 =  new Amendment__c();
       newAmd1.First_Name__c = 'test 1';
       newAmd1.Authorised_Signatory__c = true;
       newAmd1.Agent_Representative__c =true;
       newAmd1.Portal_Administrator__c = true;
       newAmd1.Email__c ='test34@test.com';
       newAmd1.ID_Type__c = 'Visa';
       newAmd1.Mark_For_Deletion__c = false;
       newAmd1.ID_Number__c = '12345';

       insert newAmd1 ;

       Amendment__c newAmd2 =  new Amendment__c();
       newAmd2.First_Name__c = 'test 2';
       newAmd2.Last_Name__c = 'lastname';
       newAmd2.Authorised_Signatory__c = true;
       newAmd2.Agent_Representative__c =true;
       newAmd2.Portal_Administrator__c = true;
       newAmd2.Email__c ='test34@test.com';
       newAmd2.ID_Type__c = 'Visa';
       newAmd2.Mark_For_Deletion__c = true;
       newAmd2.ID_Number__c = '12345';
       insert newAmd2;

       Contact newCon =  new Contact();
       newCon.FirstName = 'test 2';
       newCon.LastName = 'lastname';
       newCon.ID_Number__c = '12345';
       newCon.AccountId = agency1.Id;
       insert newCon ;

       Amendment__c newAmd6 =  new Amendment__c();
       newAmd6.First_Name__c = 'test 2';
       newAmd6.Last_Name__c = 'lastname';
       newAmd6.Authorised_Signatory__c = true;
       newAmd6.Agent_Representative__c =true;
       newAmd6.Portal_Administrator__c = true;
       newAmd6.Email__c ='test34@test.com';
       newAmd6.ID_Type__c = 'Visa';
       newAmd6.Contact__c = newCont.Id;
       newAmd6.Mark_For_Deletion__c = true;
       newAmd6.ID_Number__c = '12345';
       newAmd6.Account__c = agency1.Id;
       newAmd6.ID_Issue_Date__c = System.Today();
       newAmd6.ID_Expiry_Date__c = System.Today().addDays(100);
       newAmd6.Designation__c = 'CEo';
       insert newAmd6;

       Amendment__c newAmd3 =  new Amendment__c();
       newAmd3.First_Name__c = 'test 2';
       newAmd3.Last_Name__c = 'lastname';
       newAmd3.Authorised_Signatory__c = true;
       newAmd3.Agent_Representative__c =true;
       newAmd3.Portal_Administrator__c = true;
       newAmd3.Email__c ='test34@test.com';
       newAmd3.ID_Type__c = 'Visa';
       newAmd3.Contact__c = newCont.Id;
       newAmd3.Mark_For_Deletion__c = true;
       newAmd3.ID_Number__c = '12345';
       newAmd3.Account__c = agency1.Id;
       insert newAmd3;

       Amendment__c newAmd7 =  new Amendment__c();

       Amendment__c newAmd5 =  new Amendment__c();
       newAmd5.Owner__c = true;
       newAmd5.Agent_Representative__c = true;
       newAmd5.Broker_Card_Number__c = null;
       newAmd5.Broker_Card_Expiry_Date__c = NULL;

       Amendment__c newAmd4 =  new Amendment__c();
       newAmd4.First_Name__c = 'test 2';
       newAmd4.Last_Name__c = 'lastname';
       newAmd4.ID_Issue_Date__c = Date.newInstance(2016, 12, 9);
       newAmd4.ID_Expiry_Date__c = Date.newInstance(2222, 12, 9);
       newAmd4.Owner__c = true;
       newAmd4.Shareholding__c = 100;
       newAmd4.Designation__c = 'CEO';
       newAmd4.Authorised_Signatory__c = true;
       newAmd4.Portal_Administrator__c = true;
       newAmd4.Email__c ='test34@test.com';
       newAmd4.ID_Type__c = 'Passport';
       newAmd4.Contact__c = newCont.Id;
       newAmd4.Mark_For_Deletion__c = false;
       newAmd4.ID_Number__c = '12345';
       newAmd4.Account__c = agency1.Id;
       insert newAmd4;

       String docName;
       for(Integer i = 0; i < 99; i++) {
           docName = docName + 'test';
       }

       Section__c sec = new Section__c();
       sec.page__c = page1.id;
       insert sec;

       Section_Detail__c d = new Section_Detail__c();
       d.section__c = sec.id;
       d.Navigation_Directions__c = 'Forward';
       insert d;
       Account_SR_Field_Mapping__c srfiM = new Account_SR_Field_Mapping__c ();
       srfiM.Account_Field__c = 'Bank_Account_Number__c';
       srfiM.Is_Common_to_All_RT__c = true;
       srfiM.Is_Person_Account__c = false;
       srfiM.RecordType_Name__c = 'Agent_Update';
       srfiM.SR_Field__c= 'Account_Number__c';
       srfiM.Name= 'Account_Number__c';
       insert srfiM;
       Agent_Site__c asite = new Agent_Site__c();
       asite.Tax_Registration_Number__c = '211233122133';
       asite.Registration_Certificate_Date__c = date.today();
       asite.Agency__c = agency1.Id;
       asite.Name  = 'UAE';
       insert asite;
       Test.startTest(); 
       Test.setCurrentPageReference(new PageReference('Page.Process_Flow'));
       System.currentPageReference().getParameters().put('Type', 'Agent_Registration');
       System.currentPageReference().getParameters().put('FlowId', String.valueOf(pageFlow1.Id));
       System.currentPageReference().getParameters().put('PageId', String.valueOf(page1.Id));
       System.currentPageReference().getParameters().put('Id', serviceRequest.Id);
       system.debug('serviceRequest.NSIBPM__Customer__c >>>>>>  ' + serviceRequest.NSIBPM__Customer__c);
       ApexPages.StandardController sc = new ApexPages.StandardController(serviceRequest);
       DAMAC_CompanyPersonnelUpdateCntlr updatePage = new DAMAC_CompanyPersonnelUpdateCntlr(sc);

       updatePage.currAmdId = newAmd1.Id ;
       updatePage.currAmnd = newAmd1;

       updatePage.strNavigatePageId = page1.id;

       Boolean testDuplicateContacts = updatePage.duplicateContactCheck(null);
        updatePage.init();
        updatePage.prePopulateServiceRequest(serviceRequest,agency1.Id,'Agent_Update');
       updatePage.strActionId = d.id;
       updatePage.currAmnd = newAmd2;
       updatePage.save();
       updatePage.currAmnd = newAmd1;
       updatePage.save();
       updatePage.currAmnd = newAmd3;
       updatePage.save();
       updatePage.currAmnd = newAmd4;
       updatePage.save();
       updatePage.currAmnd = newAmd6;
       updatePage.save();
       updatePage.objFinSR = serviceRequest;
       updatePage.currAmnd = newAmd4;
       updatePage.save();
       updatePage.currAmnd = newAmd7;
       updatePage.save();
       updatePage.objFinSR.City_Of_Incorporation_New__c = 'Dubai';
       updatePage.objFinSR.Agency_Corporate_Type__c = 'Real Estate';
       updatePage.currAmnd = newAmd5;
       updatePage.save();
        updatePage.listValidation();
        Amendment__c newAmd12 =  new Amendment__c();
        newAmd12.First_Name__c = 'test 1';
        newAmd12.Authorised_Signatory__c = true;
        newAmd12.Agent_Representative__c =true;
        newAmd12.Portal_Administrator__c = true;
        newAmd12.Email__c ='test34@test.com';
        newAmd12.ID_Type__c = 'Visa';
        newAmd12.Mark_For_Deletion__c = false;
        newAmd12.ID_Number__c = '12345';
        updatePage.amendment_List.add(newAmd12);
        updatePage.upsertAmendment();
        updatePage.amndListSize = 1;
        updatePage.issueDate1 = '';
        updatePage.idExpDate1 = '';                    
    updatePage.broExpDate1 = '';                   
    updatePage.issueDate2 = '';                     
    updatePage.idExpDate2 = '';                    
    updatePage.broExpDate2 = '';   
    updatePage.issueDate3= '';
    updatePage.idExpDate3 = ''; 
    updatePage.broExpDate3= '';              
    updatePage.issueDate4 = '';                   
    updatePage.idExpDate4 = '';                   
   updatePage.broExpDate4 = '';         
    updatePage.issueDate5 = '';     
    updatePage.idExpDate5 = '';  
    updatePage.broExpDate5= ''; 
    updatePage.isSubmitted = false;
    updatePage.hideCompanyPersonnal = false;                                       
      
       Test.stopTest();
    }
   }

}