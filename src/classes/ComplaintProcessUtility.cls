public with sharing class ComplaintProcessUtility {
  public static final String COMPLAINT_CASE_TYPE = 'Complaints SR';
  public static final String COMPLAINT_RECTYPE_NAME = 'Complaint';
  public static final String COMPLAINT_INITIAL_STATUS = 'Draft Request';
  public static final String COMPLAINT_SUBMIT_STATUS = 'Submitted';
  public static final String COMPLAINT_EXCEPTION_MESSAGE = 'An exception has occurred while processing the request, please retry';
  public static final String COMPLAINT_NO_URL_MESSAGE = 'No URL found from IPMS';
  public static final String COMPLAINT_PORTAL_ORIGIN = 'Walk-In';
  public static String COMPLAINT_CRF_NAME = 'Customer Signed CRF Form '+ System.today();
  public static String COMPLAINT_ATT1_NAME = 'Customer Attachment 1 '+ System.today();
  public static String COMPLAINT_ATT2_NAME = 'Customer Attachment 2 '+ System.today();
  public static String PROCESS_NAME = 'Complaints';

    public static String getRecordTypeId(){
        return Schema.SObjectType.Case.getRecordTypeInfosByName().get(COMPLAINT_RECTYPE_NAME).getRecordTypeId();
    }

    public static String sendSMSNotification(String msgContent, String phoneNumber){
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http(); 

        String user = Label.Damac_CRM_SMS_Service_User_name;//'damaccrm';
        String passwd = Label.Damac_CRM_SMS_Service_Password;//'D@$al3sF0rc387!';
        //String strSID = Label.Damac_CRM_SMS_Service_Sender_Id;//'DAMAC GRP'; 
        String strSID = SMSClass.getSenderName(user, phoneNumber, false);

        req.setMethod('POST' ); // Method Type
        req.setEndpoint('http://api.smscountry.com/SMSCwebservice_bulk.aspx'); 
        String tempMsgContent = GenericUtility.encodeChar(msgContent);

        if(PhoneNumber.startsWith('00')) {
            PhoneNumber = PhoneNumber.removeStart('00');
        }
        else if(PhoneNumber.startsWith('0')) {
            PhoneNumber = PhoneNumber.removeStart('0');
        }               
        req.setBody('user='+ user + '&passwd=' + passwd +'&message=' + tempMsgContent + '&mobilenumber=' 
                    + phoneNumber + '&sid=' 
                    + strSID + '&MTYPE=LNG&DR=Y'); // Request Parameters
        try {
          system.debug('Request Body---'+req.getBody());
          system.debug('Request Header---'+req.getHeader('Authorization'));
          system.debug('Request Header---'+req.getHeader('Content-Type'));
          res = http.send(req);
          system.debug('Response---'+res.getBody());
          /*if(res.getBody() != null){
            // Parse Response
            if('SMS message(s) sent' != res.getBody()){
              logError(caseId, res.getBody());
            }
          }*/
            if(res.getBody() != null){
                SMS_History__c smsObj = new SMS_History__c();
                smsObj.Message__c = MsgContent;
                smsObj.Phone_Number__c = PhoneNumber;
                //smsObj.Customer__c = callObj.Account__c;
                //smsObj.Calling_List__c = callObj.Id;
                smsObj.Description__c = res.getBody();
                    
                if(res.getBody().contains('OK:')) {
                    smsObj.Is_SMS_Sent__c = true;
                    smsObj.sms_Id__c = res.getBody().substringAfter('OK:');
                }
                
                insert smsObj;
            }
            
            return res.getBody(); 
        } catch(Exception e) {
            System.debug('Error***** '+ e);
          return e.getMessage(); 
        }
    }
}