@isTest
public with sharing class Damac_InquiryAssignment_Test {

    static testmethod void testInquiryUserAssignment1 () {
         LIST<Inquiry_Assignment_Rules__c> assignmentrules = new list<Inquiry_Assignment_Rules__c> ();
        Inquiry_Assignment_Rules__c rule = new Inquiry_Assignment_Rules__c(  Active__c=true,Fields_to_Apply_Rule__c='Class__c');
        assignmentrules.add(rule);
        insert assignmentrules;
       
        LIST<Inquiry__C> inquiries = new List<Inquiry__C> ();
        Inquiry__c inq = new Inquiry__c (Last_Name__c='test1',First_Name__c='test',
                                        Preferred_Language__c='English',Inquiry_Source__c='Chat',
                                        Primary_Contacts__c='Mobile Phone',Mobile_Phone__c='3134345456567',
                                        Mobile_CountryCode__c='  India: 0091',Inquiry_Assignment_Rules__c = rule.Id,
                                        Mobile_Phone_Encrypt__c = 'test');
        inquiries.add(inq);
        Insert inquiries;
        DAMAC_InquiryAssignment InqAssignment = new DAMAC_InquiryAssignment ();
        InqAssignment.getTopInquires();
        ApexPages.currentPage().getParameters().put('queueID',userinfo.getuserid());
        ApexPages.currentPage().getParameters().put('skipLimits','true');
        InqAssignment.executeAssignment();
        InqAssignment.getUserRelatedInquires();
        InqAssignment.checkjobStatus();
        inqAssignment.executeReAssignmentLogic ();
        DAMAC_InquiryAssignment.getAllFields ('Inquiry__c');
        
    }
    
}