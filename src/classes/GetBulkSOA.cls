public class GetBulkSOA {

    Account accnt;
    public list<Account> acntid;
    public static Map<String, Object> output;

     public GetBulkSOA(ApexPages.StandardController controller) {
        accnt= (Account)controller.getrecord();
        system.debug('rec id'+accnt);
        accnt= [ Select id,name, party_id__c from Account where id= :accnt.id limit 1 ];
         }

    public pagereference getURL() {
    
        wrappercls resObj = new wrappercls();
    
        String strUrl = '';
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        String ipmsUrl=System.Label.BulkSOAUrl;
        //String finalUrl=ipmsUrl+'&PARTY_ID='+partyid;
        String finalUrl=ipmsUrl+accnt.party_id__c;
        //System.debug('input: '+finalUrl);
        request.setEndpoint(finalUrl);
        request.setMethod('GET');
        request.setTimeout(120000);
        
        system.debug('request : ' + request);
        
        HttpResponse response = http.send(request);
        System.debug('response: '+response.getbody());
        resObj =  (wrappercls)json.deserialize(response.getBody(), GetBulkSOA.wrappercls.class);
        //output = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
        //system.debug( 'resObj : ' + resObj );
        system.debug( 'response: ' + response);
        //system.debug( resObj.actions[0].url  );

        strUrl = resObj.actions[0].url;  
        //system.debug( ' strUrl :  ' + strUrl);
             
        Pagereference pg = new PageReference(strUrl);
                pg.setRedirect(true);
                return pg;
       
    }
  
    public class wrappercls {
        public boolean complete{get;set;}
        public Integer responseid{get;set;}
        public String responsetime{get;set;}
        public List<actions> actions{get;set;}
    }

    public class actions {
        public String action{get;set;}
        public String method{get;set;}
        public String url{get;set;}
    }
}