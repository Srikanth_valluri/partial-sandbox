@isTest
public with sharing class RebrandlyServiceTest {
    
    @isTest
    public static void testGetShortenUrl() {
        insert new Credentials_Details__c( Name = 'Rebrandly', Password__c = 'test', EndPoint__c = 'https://tt231dgsg.com');

        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new RebrandlyServiceHttpMock() );
            RebrandlyService.getShortenUrl( 'https://dd231dgsg.com' );
        Test.stopTest();
    }
}