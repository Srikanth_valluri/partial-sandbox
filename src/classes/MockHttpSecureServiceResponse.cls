@isTest
global class MockHttpSecureServiceResponse implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals('https://www.damacproperties.com/en/webservices/offers.json', req.getEndpoint());
        //System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{ "meta": { "code": 200, "requestId": "5aad6de8db04f536533c14e7" }, "response": { "groups": [ { "items": [ { "venue": { "id": "4e59c22c18388cd5cba8bbd9", "name": "Axis Bank", "contact": {"phone": "+97142130000","formattedPhone": "+971 4 213 0000"},"location": { "address": "Aundh Branch, Near Gaikwad Petrol Pump", "lat": 18.55769295435833, "lng": 73.80429301456995, "labeledLatLngs": [ { "label": "display", "lat": 18.55769295435833, "lng": 73.80429301456995 } ], "distance": 968, "cc": "IN", "city": "Pune", "state": "Mahārāshtra", "country": "India", "formattedAddress": [ "Aundh Branch, Near Gaikwad Petrol Pump", "Pune", "Mahārāshtra", "India" ] } } } ] } ] } }');
        res.setStatusCode(200);
        return res;
    }
}