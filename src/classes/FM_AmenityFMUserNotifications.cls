/*-------------------------------------------------------------------------------------------------
Description: To send email to FM users of building when booking is confirmed/cancelled/rejected
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 21-11-2017       | Lochana Rajput   | 1. Get email addresses and send email
=============================================================================================================================
*/
public without sharing class FM_AmenityFMUserNotifications {
	//Invoked from 'FM Amenity Booking' process builder
	@InvocableMethod
	public static void sendNotificationEmails(List<FM_Case__c> lstFMCases) {
		//Get unit names to identify building
		List<String> lstBuildingNames = new List<String>();
		Map<String, String> FMCase_Building = new Map<String, String>();
		for(FM_Case__c obj : lstFMCases) {
			System.debug('==obj.==' + obj.Amenity_Booking_Status__c + '===='+obj.Unit_Name__c);
			if(String.isNotBlank(obj.Unit_Name__c) &&
			(obj.Amenity_Booking_Status__c =='Booking Confirmed' || obj.Amenity_Booking_Status__c == 'Cancelled'
				|| obj.Amenity_Booking_Status__c == 'Rejected')) {
				lstBuildingNames.add(obj.Unit_Name__c.substringBefore('/'));
				FMCase_Building.put(obj.Id, obj.Unit_Name__c.substringBefore('/'));
			}
		}//for
		//Get FM users for the buildings
		Map<String, List<String>> mapbuilding_FMUserIds = new Map<String, List<String>>();
		System.debug('==FMCase_Building==' + FMCase_Building);
		System.debug('==AmenityNotificationRoles==' + Label.AmenityNotificationRoles.split(','));
		//Get FM users with roles mentioned in custom label
		for(Location__c objLoc : [SELECT Name,
										(SELECT FM_User__r.Email
										FROM FM_Users__r
										WHERE FM_Role__c IN: Label.AmenityNotificationRoles.split(',')
										AND FM_User__r.isActive = true)
								FROM Location__c
								WHERE Name IN : FMCase_Building.values()]) {
			if(objLoc.FM_Users__r.size() > 0) {
               for(FM_User__c obj: objLoc.FM_Users__r) {
				   if(mapbuilding_FMUserIds.containsKey(objLoc.Name)) {
					   mapbuilding_FMUserIds.get(objLoc.Name).add(obj.FM_User__r.Email);
				   }
				   else {
					   mapbuilding_FMUserIds.put(objLoc.Name, new String[]{obj.FM_User__r.Email});
				   }
			   }
		   }//if
		}//Location for
		// Send email to FM users
		System.debug('==mapbuilding_FMUserIds==' + mapbuilding_FMUserIds);
		List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
		List<EmailTemplate> lstTemplates = new List<EmailTemplate>();
		Map<String, Id> mapStatus_Id = new Map<String, Id>();
		for(EmailTemplate et : [SELECT Id,DeveloperName FROM EmailTemplate
						WHERE DeveloperName IN: Label.FM_AmenityComonTemplateNames.split(',')]) {
			if(et.DeveloperName == 'FM_Amenity_Rejected_Common_Template') {
				mapStatus_Id.put('Rejected',et.id);
			}
			else if(et.DeveloperName == 'FM_Amenity_Cancelled_Common_Template') {
				mapStatus_Id.put('Cancelled',et.id);
			}
			else if(et.DeveloperName == 'FM_Amenity_Confirmation_Common_Template') {
				mapStatus_Id.put('Booking Confirmed',et.id);
			}
		}
		Contact objCon = [SELECT Id FROm Contact LIMIT 1];
		for(FM_Case__c objFMCase : lstFMCases) {

			if(String.isNotBlank(objFMCase.Unit_Name__c)
					&& mapbuilding_FMUserIds.containsKey(objFMCase.Unit_Name__c.substringBefore('/'))) {
				Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
				System.debug('===Email Ids===='+
					mapbuilding_FMUserIds.get(objFMCase.Unit_Name__c.substringBefore('/')));
				email.setToAddresses(mapbuilding_FMUserIds.get(objFMCase.Unit_Name__c.substringBefore('/')));
				if(objFMCase.Amenity_Booking_Status__c == 'Booking Confirmed') {
					if(mapStatus_Id.containsKey(objFMCase.Amenity_Booking_Status__c)) {
						email.setTemplateId(mapStatus_Id.get(objFMCase.Amenity_Booking_Status__c));
					}
				}
				else if(objFMCase.Amenity_Booking_Status__c == 'Cancelled') {
					if(mapStatus_Id.containsKey(objFMCase.Amenity_Booking_Status__c)) {
						email.setTemplateId(mapStatus_Id.get(objFMCase.Amenity_Booking_Status__c));
					}
				}
				else if(objFMCase.Amenity_Booking_Status__c == 'Rejected') {
					if(mapStatus_Id.containsKey(objFMCase.Amenity_Booking_Status__c)) {
						email.setTemplateId(mapStatus_Id.get(objFMCase.Amenity_Booking_Status__c));
					}
				}
				email.setWhatId(objFMCase.Id);
				email.setTargetObjectId(objCon.Id);
				email.setTreatTargetObjectAsRecipient(false);
				email.setSaveAsActivity(false);
				emailList.add(email);
			}
		}//for
		//Send email
		 Messaging.SendEmailResult[] emailResults= Messaging.sendEmail(emailList);
		 List<Error_Log__c> lstErrorLogs = new List<Error_Log__c>();
		 for(Messaging.SendEmailResult er : emailResults) {
			 for(Messaging.SendEmailError error : er.getErrors()) {
				 lstErrorLogs.add(new Error_Log__c(Process_Name__c = 'Amenity Booking',
				 Error_Details__c = error.getMessage()));
			 }
		 }//for
	}//method
}//class