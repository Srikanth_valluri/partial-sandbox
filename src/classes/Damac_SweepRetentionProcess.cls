global class Damac_SweepRetentionProcess implements Database.Batchable<sObject> {

    global Database.QueryLocator start(Database.BatchableContext BC){
        
        String query = 'SELECT Name, Pre_InquiryId__c, Is_Retention__c, Telesales_Executive__c, Sweep_Inquiry__c FROM Inquiry__c WHERE Pre_InquiryId__c != NULL AND Is_Retention__c = TRUE AND Owner_Team__c = \'Retention Team\'';
        
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List <Inquiry__c> scope){
        Set <ID> preInqIds = new Set <ID> ();
        Set <ID> inqIds = new Set <ID> ();
        List <Inquiry__c> inqToUpdate = new List <Inquiry__c> ();
        for (Inquiry__c inq :scope) {
            inqIds.add (inq.Id);
            inq.Telesales_Executive__c = NULL;
            inq.Sweep_Inquiry__c = true;
            inq.Is_Retention__c = false;
            inqToUpdate.add (inq);
            
            if (inq.Pre_InquiryId__c != NULL) {
                preInqIds.add(inq.Pre_InquiryId__c);
            }
        }
        Update inqToUpdate;
       
        if (preInqIds.size () > 0) {
            ID defaultOwnerId = [SELECT Id from Group where Name = 'Nurturing for Sweep Queue' and Type = 'Queue'].ID;
            List <Inquiry__c> preInqToUpdate = new List <Inquiry__c> ();
            
            for (Inquiry__c inq : [SELECT OwnerId, Telesales_Executive__c FROM Inquiry__c WHERE ID IN :preInqIds]) {
                inq.OwnerId = defaultOwnerId;
                preInqToUpdate.add (inq);
            }
            update preInqToUpdate;            
            
            Database.delete([SELECT UserOrGroupId
                            FROM Inquiry__Share
                            WHERE ParentId IN :preInqIds
                            ], false);
                            
            Database.delete([SELECT UserOrGroupId
                            FROM Inquiry__Share
                            WHERE ParentId IN :InqIds
                            ], false);
                
        }
    }
    
    global void finish(Database.BatchableContext BC){
    }
}