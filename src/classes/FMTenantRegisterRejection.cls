/*-------------------------------------------------------------------------------------------------
Description: Class used in process builder 'FM Case : Tenant Registration'
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
v1.0     | 24-01-2019       | Lochana Rajput   | 1. Added logic to create verify task for FM admin
=============================================================================================================================
*/
public without sharing class FMTenantRegisterRejection {
    @InvocableMethod
    public static void createVerifyTask(List<FM_Case__c> lstFMCases) {
        Set<String> setProcessInstanceId = new Set<String>();
        set<String> setFMCaseIds = new set<String>();
        for( FM_Case__c objFMCase : lstFMCases ) {
         setFMCaseIds.add( objFMCase.Id );
        }
        System.debug('==setFMCaseIds===='+setFMCaseIds);
        Map<String, String> sObjectMap = new Map<String, String>();
        List<FM_Case__c> lstCasesToUpdate = new List<FM_Case__c>();
        Map<Id, String> mapComments = new Map<Id, String>();
        for(FM_Case__c objFMCase :[SELECT Id, Approval_Status__c,Description__c,Manager_Comments__c,
                                            recordType.DeveloperName,Outstanding_service_charges__c,
                                            Admin__c,Request_Type_DeveloperName__c,
                                            Tenant_Email__c,Name,
                                            (SELECT Id, IsPending, ProcessInstanceId
                                                  , StepStatus
                                                  , Comments FROM ProcessSteps
                                            WHERE (StepStatus ='Started' OR StepStatus ='Rejected')
                                            ORDER BY  SystemModstamp DESC)
                                    FROM FM_Case__c where id IN : setFMCaseIds]) {
            setProcessInstanceId = new Set<String>();
            System.debug('==objFMCase===='+objFMCase);
            System.debug('==ProcessSteps===='+objFMCase.ProcessSteps);
            // for(ProcessInstanceStep step : objFMCase.ProcessSteps) {
            for(Integer i=0; i<objFMCase.ProcessSteps.size(); i++) {
                if(objFMCase.ProcessSteps[i].StepStatus =='Rejected') {
                    setProcessInstanceId.add(objFMCase.ProcessSteps[i].ProcessInstanceId);
                    mapComments.put(objFMCase.Id, objFMCase.ProcessSteps[i].Comments);
                }
                else if(objFMCase.ProcessSteps[i].StepStatus =='Started' && objFMCase.ProcessSteps[i].Comments != NULL
                        && objFMCase.ProcessSteps[i].Comments.containsIgnoreCase('manager')
                        && setProcessInstanceId.contains(objFMCase.ProcessSteps[i].ProcessInstanceId)) {
                    lstCasesToUpdate.add(objFMCase);
                    break;
                }
            }//for
        }//for
        System.debug('==lstCasesToUpdate===='+lstCasesToUpdate);
        //Check if approval rejected by any manager
        List<Task> lstTasks = new List<Task>();
        List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();
        // Contact objCon = [SELECT Id from Contact LIMIT 1];
        List<EmailTemplate> lstTemplate = [SELECT ID
                                            FROM EmailTemplate
                                            WHERE DeveloperName ='FM_Case_Rejection_Template_with_comments'];
        for(FM_Case__c objFMCase : lstCasesToUpdate) {
            //Populate approving authorities
            objFMCase.Approving_Authorities__c = FMTenantRegistrationController.populateApprovingAuthotities(objFMCase.Outstanding_service_charges__c
                ,objFMCase.Request_Type_DeveloperName__c);
            objFMCase.Approval_Status__c = 'Pending';
            objFMCase.Submit_for_Approval__c = false;
            objFMCase.Recall_Approvals__c = true;//Recalls approval process,batch to run every 10 minutes
            //Email for rejection
            if(objFMCase.Tenant_Email__c != NULL && lstTemplate.size() > 0) {
                String emailBody = 'Hello,<br/><br/>' + objFMCase.Name + ' has been rejected.<br/><br/>' +
                                'Comments:<br/> ' + mapComments.get(objFMCase.Id) + '<br/><br/>Thanks';
                Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
                // emailToSend.toAddresses = wrapperObj.toAddresses;
                // emailToSend.optOutPolicy = wrapperObj.optOutPolicy;
                emailToSend.subject = 'FM Case Rejected : ' + objFMCase.Name;
                emailToSend.setHtmlBody(emailBody);
                // Messaging.SingleEmailMessage emailToSend =
                // Messaging.renderStoredEmailTemplate(lstTemplate[0].ID, objCon.Id, objFMCase.Id);
                // emailToSend.setTemplateId(lstTemplate[0].ID);
                // emailToSend.setTargetObjectId(objCon.Id);
                // emailToSend.setTreatTargetObjectAsRecipient(false);
                emailToSend.saveAsActivity = true;
                emailToSend.setWhatId(objFMCase.Id);
                emailToSend.setToAddresses(new String[]{objFMCase.Tenant_Email__c});
                lstMsgsToSend.add(emailToSend);
            }
            //Create task to verify customer
            Task objTask = new Task();
            objTask.whatId = objFMCase.Id;
            objTask.Subject = Label.Subject_For_Admin_Task;
            objTask.Status = 'Not Started';
            objTask.Priority = 'Normal';
            objTask.Process_Name__c = 'Tenant Registration';
            objTask.ActivityDate = Date.today().addDays(2);
            objTask.Assigned_User__c = 'FM Admin';
            if(String.isNotBlank(objFMCase.Admin__c)) {
                objTask.OwnerId = objFMCase.Admin__c;
            }
            lstTasks.add(objTask);
        }//for
        Messaging.sendEmail(lstMsgsToSend);
        insert lstTasks;
        update lstCasesToUpdate;
    }
}