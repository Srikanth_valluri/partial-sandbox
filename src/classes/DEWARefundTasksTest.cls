@isTest
public class DEWARefundTasksTest {
     @isTest
    public static void testMethod1(){
        Account objA = TestDataFactoryFM.createAccount();
        insert objA;
        
        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objA);
        insert objSR;
        
        Booking__c objBooking = TestDataFactoryFM.createBooking(objA,objSR);
        insert objBooking;

        Location__c locObj=TestDataFactoryFM.createLocation();
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;
        
        Booking_Unit__c objBU = TestDataFactoryFM.createBookingUnit(objA,objBooking);
        objBU.Inventory__c=invObj.id;
        objBU.Tenant__c = objA.Id; 
        insert objBU;
        
        Case objCase = new Case();
        objCase.Account = objA;
        objCase.Status = 'Submitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Type = 'Offer & Acceptance Letter';  
        objCase.Account_Email__c = 'test@gmail.com';
        objCase.Refund_Amount__c = 1400;
        objCase.Old_Bills_Unpaid__c = 'Yes';
        objCase.Dispatch_Status__c = 'Yes';
        insert objCase;
                
        Task objTask = new Task();
        objTask.Subject = System.Label.Inform_Customer_And_Provide_Next_Month_DEWA_Bill;
        objTask.WhatId = objCase.Id;
        objTask.OwnerId = userinfo.getUserId();
        objTask.Status = 'Completed';
        objTask.Priority = 'Normal';
        insert objTask;
        List<Id> lstTask = new List<Id>();
        lstTask.add(objTask.Id);
        Test.startTest();
         DEWARefundTasks.updateCreateTask(lstTask);
        Test.stopTest();
    }
    
         @isTest
    public static void testMethod2(){
        Account objA = TestDataFactoryFM.createAccount();
        insert objA;
        
        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objA);
        insert objSR;
        
        Booking__c objBooking = TestDataFactoryFM.createBooking(objA,objSR);
        insert objBooking;

        Location__c locObj=TestDataFactoryFM.createLocation();
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;
        
        Booking_Unit__c objBU = TestDataFactoryFM.createBookingUnit(objA,objBooking);
        objBU.Inventory__c=invObj.id;
        objBU.Tenant__c = objA.Id; 
        insert objBU;
        
        Case objCase = new Case();
        objCase.Account = objA;
        objCase.Status = 'Submitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Type = 'Offer & Acceptance Letter';  
        objCase.Account_Email__c = 'test@gmail.com';
        objCase.Refund_Amount__c = 1400;
        objCase.Old_Bills_Unpaid__c = 'Yes';
        objCase.Dispatch_Status__c = 'Yes';
        insert objCase;
                
        
        Task objTask1 = new Task();
        objTask1.Subject = System.Label.Validate_Request_and_Calculations;
        objTask1.WhatId = objCase.Id;
        objTask1.OwnerId = userinfo.getUserId();
        objTask1.Status = 'Rejected';
        objTask1.Priority = 'Normal';
        objTask1.Description = 'Test Cooments';
        insert objTask1;
        List<Id> lstTask = new List<Id>();
        lstTask.add(objTask1.Id);
        Test.startTest();
         DEWARefundTasks.updateCreateTask(lstTask);
        Test.stopTest();
    }
    
    
             @isTest
    public static void testMethod3(){
        Account objA = TestDataFactoryFM.createAccount();
        insert objA;
        
        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objA);
        insert objSR;
        
        Booking__c objBooking = TestDataFactoryFM.createBooking(objA,objSR);
        insert objBooking;

        Location__c locObj=TestDataFactoryFM.createLocation();
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;
        
        Booking_Unit__c objBU = TestDataFactoryFM.createBookingUnit(objA,objBooking);
        objBU.Inventory__c=invObj.id;
        objBU.Tenant__c = objA.Id; 
        insert objBU;
        
        Case objCase = new Case();
        objCase.Account = objA;
        objCase.Status = 'Submitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Type = 'Offer & Acceptance Letter';  
        objCase.Account_Email__c = 'test@gmail.com';
        objCase.Refund_Amount__c = 1400;
        objCase.Old_Bills_Unpaid__c = 'Yes';
        objCase.Dispatch_Status__c = 'Yes';
        insert objCase;
                
        
        Task objTask1 = new Task();
        objTask1.Subject = System.Label.Calculate_Verify_Refund_Amount;
        objTask1.WhatId = objCase.Id;
        objTask1.OwnerId = userinfo.getUserId();
        objTask1.Status = 'Completed';
        objTask1.Priority = 'Normal';
        insert objTask1;
        List<Id> lstTask = new List<Id>();
        lstTask.add(objTask1.Id);
        Test.startTest();
         DEWARefundTasks.updateCreateTask(lstTask);
        Test.stopTest();
    }
    
          @isTest
    public static void testMethod4(){
        Account objA = TestDataFactoryFM.createAccount();
        insert objA;
        
        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objA);
        insert objSR;
        
        Booking__c objBooking = TestDataFactoryFM.createBooking(objA,objSR);
        insert objBooking;

        Location__c locObj=TestDataFactoryFM.createLocation();
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;
        
        Booking_Unit__c objBU = TestDataFactoryFM.createBookingUnit(objA,objBooking);
        objBU.Inventory__c=invObj.id;
        objBU.Tenant__c = objA.Id; 
        insert objBU;
        
        Case objCase = new Case();
        objCase.Account = objA;
        objCase.Status = 'Submitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Type = 'Offer & Acceptance Letter';  
        objCase.Account_Email__c = 'test@gmail.com';
        objCase.Refund_Amount__c = 1400;
        objCase.Old_Bills_Unpaid__c = 'Yes';
        objCase.Dispatch_Status__c = 'Yes';
        insert objCase;
                
        
        Task objTask1 = new Task();
        objTask1.Subject = System.Label.Validate_Request_and_Calculations;
        objTask1.WhatId = objCase.Id;
        objTask1.OwnerId = userinfo.getUserId();
        objTask1.Status = 'Completed';
        objTask1.Priority = 'Normal';
        objTask1.Description = 'Test Cooments';
        insert objTask1;
        List<Id> lstTask = new List<Id>();
        lstTask.add(objTask1.Id);
        Test.startTest();
         DEWARefundTasks.updateCreateTask(lstTask);
        Test.stopTest();
    }
}