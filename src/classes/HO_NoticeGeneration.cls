public without sharing class HO_NoticeGeneration {

     public static HO_NoticeGeneration.HO_NoticeResponse GetHONotice(String projectName, String reqName, String regId) {
        HO_NoticeGeneration.HO_NoticeResponse objNoticeWrapper ;
        HoNotice1.HONoticeHttpSoap11Endpoint objHONotice = new HoNotice1.HONoticeHttpSoap11Endpoint ();
        objHONotice.timeout_x = 120000 ;
        list<HoNotice2.APPSXXDC_PROCESS_SERX1794747X1X5> lstReg = new list<HoNotice2.APPSXXDC_PROCESS_SERX1794747X1X5>();
        HoNotice2.APPSXXDC_PROCESS_SERX1794747X1X5 reg = new HoNotice2.APPSXXDC_PROCESS_SERX1794747X1X5();
        reg.ATTRIBUTE1 = projectName;
        //reg.ATTRIBUTE1 = 'XXDCHONOTICE1';
        reg.ATTRIBUTE10 = '';
        reg.ATTRIBUTE11 = '';
        reg.ATTRIBUTE12 = '';
        reg.ATTRIBUTE13 = '';
        reg.ATTRIBUTE14 = '';
        reg.ATTRIBUTE15 = '';
        reg.ATTRIBUTE16 = '';
        reg.ATTRIBUTE17 = '';
        reg.ATTRIBUTE18 = '';
        reg.ATTRIBUTE19 = '';
        reg.ATTRIBUTE2 = '';
        reg.ATTRIBUTE20 = '';
        reg.ATTRIBUTE21 = '';
        reg.ATTRIBUTE22 = '';
        reg.ATTRIBUTE23 = '';
        reg.ATTRIBUTE24 = '';
        reg.ATTRIBUTE25 = '';
        reg.ATTRIBUTE26 = '';
        reg.ATTRIBUTE27 = '';
        reg.ATTRIBUTE28 = '';
        reg.ATTRIBUTE29 = '';
        reg.ATTRIBUTE3 = '';
        reg.ATTRIBUTE30 = '';
        reg.ATTRIBUTE31 = '';
        reg.ATTRIBUTE32 = '';
        reg.ATTRIBUTE33 = '';
        reg.ATTRIBUTE34 = '';
        reg.ATTRIBUTE35 = '';
        reg.ATTRIBUTE36 = '';
        reg.ATTRIBUTE37 = '';
        reg.ATTRIBUTE38 = '';
        reg.ATTRIBUTE39 = '';
        reg.ATTRIBUTE4 = '';
        reg.ATTRIBUTE41 = '';
        reg.ATTRIBUTE42 = '';
        reg.ATTRIBUTE43 = '';
        reg.ATTRIBUTE44 = '';
        reg.ATTRIBUTE45 = '';
        reg.ATTRIBUTE46 = '';
        reg.ATTRIBUTE47 = '';
        reg.ATTRIBUTE48 = '';
        reg.ATTRIBUTE49 = '';
        reg.ATTRIBUTE5 = '';
        reg.ATTRIBUTE50 = '';
        reg.ATTRIBUTE6 = '';
        reg.ATTRIBUTE7 = '';
        reg.ATTRIBUTE8 = '';
        reg.ATTRIBUTE9 = '';
        reg.PARAM_ID = regId;
        lstReg.add(reg);
        system.debug('lstReg'+lstReg);
        string strHand;
        objNoticeWrapper = new HO_NoticeGeneration.HO_NoticeResponse();
        try {
            strHand = objHONotice.HandOverNotice('2-'+String.valueOf(System.currentTimeMillis()), reqName,'SFDC',lstReg);
            system.debug('strHand '+strHand );
            JSON2Apex objHoNoticeResponse = new JSON2Apex ();
            objHoNoticeResponse = (HO_NoticeGeneration.JSON2Apex)JSON.deserialize(strHand, HO_NoticeGeneration.JSON2Apex.class);
            String strResponse = strHand.substringAfter('[').substringBeforeLast(']');
            Map<String, object> mapHONotice = (Map<String, object>)JSON.deserializeUntyped( strResponse.replace('\\','').removeStart('"').removeEnd('"') );
                    system.debug('mapHONotice'+mapHONotice );                    
                    objNoticeWrapper.P_PARAM_ID= String.valueOf( mapHONotice.get('PARAM_ID') );
                    objNoticeWrapper.P_PROC_STATUS= String.valueOf( mapHONotice.get('PROC_STATUS') );
                    objNoticeWrapper.P_PROC_MESSAGE= String.valueOf( mapHONotice.get('PROC_MESSAGE') );
                    objNoticeWrapper.URL= String.valueOf( mapHONotice.get('ATTRIBUTE1') );
            system.debug(objNoticeWrapper);
        } catch (Exception e){
            objNoticeWrapper.P_PROC_MESSAGE = strHand;
            objNoticeWrapper.URL = '';
        }
        system.debug('!!!!!!!objNoticeWrapper'+objNoticeWrapper);
        return objNoticeWrapper;
    }
    
    public class JSON2Apex {
        public List<HO_NoticeResponse> data;
        public String message;
        public String status;
    }
    
    public class HO_NoticeResponse {
        public String P_PARAM_ID{get;set;}
        public String P_PROC_STATUS{get;set;}
        public String P_PROC_MESSAGE{get;set;}
        public String URL{get;set;}
    }
}