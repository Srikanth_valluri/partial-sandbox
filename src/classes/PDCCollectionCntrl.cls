/********************************************************************************************************************************
* Description : Class to generate the PDC receipt
*===============================================================================================================================
* Ver      Date-DD/MM/YYYY     Author              Modification
*===============================================================================================================================
* 1.0      21/09/2020        Arjun Khatri          Intial Draft
*********************************************************************************************************************************/
public without sharing Class PDCCollectionCntrl {
    
    @AuraEnabled(cacheable=true)
    public static List<ParentDetails> getParentDetails(String CLId,String caseId) {
        
        
        List<ParentDetails> lstParentDetails = new List<ParentDetails>();
        
        if( String.isNotBlank( CLId ) && CLId.startsWith('a37') ) {
            for( Calling_List__c objCL :  [
                SELECT  Id
                , Account__c
                , Account__r.Name
                , Name
                , Case__c
                , Booking_Unit__c
                , Booking_Unit__r.Unit_Name__c                  
                FROM Calling_List__c
                WHERE Id = :CLId
            ] ){
                ParentDetails objParentDetails = new ParentDetails();
                //objParentDetails.strCaseNumber = ''; 
                if(objCL.Case__c != null)                           
                objParentDetails.strCaseNumber = objCL.Case__c;
                else
                objParentDetails.strCaseNumber = '';   
                System.debug('objParentDetails.strCaseNumber'+objParentDetails.strCaseNumber);
                objParentDetails.strCLName = objCL.Name;
                objParentDetails.strAccName = objCL.Account__r.Name;
                objParentDetails.strAccId = objCL.Account__c;
                objParentDetails.strBUName = objCL.Booking_Unit__r.Unit_Name__c;
                objParentDetails.strBUId = objCL.Booking_Unit__c;
                lstParentDetails.add(objParentDetails);
            }
        }
        if( String.isNotBlank( caseId ) && caseId.startsWith('500') ) {
            for( Case objCase :  [
                SELECT  Id
                , AccountId
                , Account.Name
                , CaseNumber
                , Booking_Unit__c
                , Booking_Unit__r.Unit_Name__c                  
                FROM Case
                WHERE Id = :caseId
            ] ){
                ParentDetails objParentDetails = new ParentDetails();
                objParentDetails.strCaseNumber = objCase.CaseNumber;
                objParentDetails.strCLName = '';
                objParentDetails.strAccName = objCase.Account.Name;
                objParentDetails.strAccId = objCase.AccountId;
                objParentDetails.strBUName = objCase.Booking_Unit__r.Unit_Name__c;
                objParentDetails.strBUId = objCase.Booking_Unit__c;
                lstParentDetails.add(objParentDetails);
            }
        }
        
        return lstParentDetails;
    }
    
    @AuraEnabled(cacheable=true)
    public static List<Case_Post_Dated_Cheque__c> getPDCDetails(String CLId,String caseId) {
        List<Case_Post_Dated_Cheque__c> lstPDC = new List<Case_Post_Dated_Cheque__c>();
        
        if( String.isNotBlank(CLId) || String.isNotBlank(caseId) ) {
            String strQuery = 'SELECT Id,Receipt_No__c,Receipt_Date__c,New_maturity_date__c,New_Amount__c,Reason__c,IPMS_Cash_Receipt_Id__c,';
            strQuery += 'Receipt_Classification__c,Mode_of_Payment__c,Account__c,Account__r.Name,Calling_List__c,Booking_Unit__c,';
            strQuery += 'Booking_Unit__r.Unit_Name__c,New_Drawer__c,New_Bank__c FROM Case_Post_Dated_Cheque__c ';
            if( String.isNotBlank( CLId ) && CLId.startsWith('a37') ) {
                strQuery += 'WHERE Calling_List__c = :CLId';
            }
            if( String.isNotBlank( caseId ) && caseId.startsWith('500') ) {
                strQuery += 'WHERE Case__c = :caseId';
            }
            lstPDC = Database.query(strQuery);
        }
        /*return [
SELECT  Id
, Receipt_No__c 
, Receipt_Date__c
, New_maturity_date__c
, New_Amount__c
, Reason__c
, IPMS_Cash_Receipt_Id__c
, Receipt_Classification__c 
, Mode_of_Payment__c 
, Account__c
, Account__r.Name
, Calling_List__c
, Booking_Unit__c
, Booking_Unit__r.Unit_Name__c
, New_Drawer__c
, New_Bank__c 
FROM Case_Post_Dated_Cheque__c
WHERE Calling_List__c = :CLId
];*/
        return lstPDC;      
    }
    
    @AuraEnabled
    public static void callPDCReceiptService(String CLId,String caseId) {
        if( String.isNotBlank(CLId) || String.isNotBlank(caseId) ) {
            /*if( String.isNotBlank(CLId) && CLId.startsWith('a37') ) {
                Map<Id, Case_Post_Dated_Cheque__c> mapCasePDC = new Map<Id, Case_Post_Dated_Cheque__c>([SELECT Id FROM Case_Post_Dated_Cheque__c WHERE Calling_List__c = :CLId]);
                if(!mapCasePDC.isEmpty()){
                    PDCReceiptGenerator.futureGeneratePDC(new List<Id>(mapCasePDC.keySet()));
                }
            }else*/
            if( String.isNotBlank(caseId) && caseId.startsWith('500') ) {
                Map<Id, Case_Post_Dated_Cheque__c> mapCasePDC = new Map<Id, Case_Post_Dated_Cheque__c>([SELECT Id FROM Case_Post_Dated_Cheque__c WHERE Case__c = :caseId]);
                if(!mapCasePDC.isEmpty()){
                    PDCReceiptGenerator.futureGeneratePDC(new List<Id>(mapCasePDC.keySet()));
                }
            }
        }
    }
    
    @future(callout=true)
    public static void futureGeneratePDC( String CLId,String caseId ) {
        
    }
    
    @AuraEnabled
    public static String createCase(Id CLId) {
        List<Calling_List__c> callingListToUpdate = new  List<Calling_List__c>();
        
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Post Dated Cheque').getRecordTypeId();
        List<Case> objCaseList = new List<Case>();
        Case objCase = new Case();
        objCase.RecordTypeId = recTypeId;
        objCaseList.add(objCase);
        Insert objCaseList;
        
        if(objCase != null){
            List<Calling_List__c>  lstCallingList = [SELECT Id
                                                     ,Case__c
                                                     FROM Calling_List__c
                                                     WHERE Id =: CLId
                                                     LIMIT 1
                                                    ];
            
            if(!lstCallingList.isEmpty()){
                for(Calling_List__c objCL : lstCallingList){
                    objCL.Case__c = objCase.Id;
                    callingListToUpdate.add(objCL);
                }
            }
            
            if(!callingListToUpdate.isEmpty()){
                Update callingListToUpdate;
                
                List<Case_Post_Dated_Cheque__c> callingListPDC = 
                    new List<Case_Post_Dated_Cheque__c>([SELECT Id
                                                         	  , Case__c
                                                           FROM Case_Post_Dated_Cheque__c
                                                          WHERE ID =:CLId]);
                for(Case_Post_Dated_Cheque__c pdcObj :callingListPDC){
                    pdcObj.Case__c = objCase.Id;
                }
                update callingListPDC;
                
                List<Task> objTaskList = new List<Task>();
                Task objTask = new Task();
                objTask.Subject = 'Finance Verification';
                objTask.Parent_Task_Id__c = CLId;
                objTask.WhatId = objCase.Id;
                objTask.assigned_user__c = 'Finance';
                objTask.ActivityDate = System.today().addDays(1);
                objTaskList.add(objTask);
                Insert objTaskList;
            }
        }
        return objCase.Id;
    }
    
    public class ParentDetails{
        @AuraEnabled
        public String strCaseNumber {get;set;}
        @AuraEnabled
        public String strCLName     {get;set;} 
        @AuraEnabled
        public String strAccName    {get;set;}
        @AuraEnabled
        public String strAccId     {get;set;}
        @AuraEnabled
        public String strBUName     {get;set;}
        @AuraEnabled
        public String strBUId     {get;set;}
        // public ParentDetails( String strCaseNumber 
        //                     , String strCLName 
        //                     , String strAccName 
        //                     , String strAccId
        //                     , String strBUName
        //                     , String strBUId ) {
        //     this.strCaseNumber  = strCaseNumber;
        //     this.strCLName      = strCLName;
        //     this.strAccName     = strAccName;
        //     this.strAccName     = strAccId;
        //     this.strBUName      = strBUName;
        //     this.strAccName     = strBUId;
        // }
    }
    
}