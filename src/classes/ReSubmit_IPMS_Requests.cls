/**************************************************************************************************
* Name               : ReSubmit_IPMS_Requests
* Description        : Adhoc Webservice Calls from the Buttons on Detail Pages
                        
                        -POP SR
                        -POP Attachment                 
* Created Date       : July/2017                                                                 
* Created By         : PwC ME - Venkata Subhash K                                                     
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR                         DATE                                                              
* 1.0         PwC ME - Venkata Subhash K     03/07/2017                                                        
**************************************************************************************************/

global class ReSubmit_IPMS_Requests{

    webservice static string Retry_POP_SR(Id IdToSubmit){
        string reqStatus = 'Success';
        try{
            list<id> popSrId = new list<id>();
            popSrId.add(IdToSubmit);
            IPMS_Requests.sendPoPSR(popSrId);
        }catch(Exception e){
            reqStatus = e.getMessage(); 
        }
        return reqStatus;
    
    }

}