global class DealExceptionRequest_Remote{
    public DealExceptionRequest_Remote(ApexPages.StandardController controller) {   
    }
    public DealExceptionRequest_Remote(DealExceptionRequest_SearchController controller) {}    
    public static string preBookId { get; set; }
    public static Boolean isPortalUser {
        get {        
            User userObj = [SELECT IsPortalEnabled  FROM user WHERE id=:userinfo.getuserid() LIMIT 1];
            return userObj.IsPortalEnabled;
        } 
        set;
    }
    
    /*
    Query the Individual Agnecy Details and return results on the Page
    */
    @RemoteAction 
    global static List<Account> getAgencyDetails(String searchKey){
        List<Account> agencyList = new List<Account>();
        List<Id> accountIdsList = new List<Id>();
        for(Agency_PC__c thisAgency : [SELECT Id, user__c, Agency__c
                                       FROM Agency_PC__c 
                                       WHERE user__c =: UserInfo.getUserId() 
                                       AND  (Agency__r.RecordTypeId =: DamacUtility.getRecordTypeId('Account', 'Individual Agency'))]){
            accountIdsList.add(thisAgency.Agency__c);   
        }
        system.debug('#### accountIdsList = '+accountIdsList);
        for(Account thisAccount : [SELECT Id, Name, RecordTypeId, RecordType.Name,
                                       (SELECT Id, Name, Org_ID_formula__c, Org_ID__c FROM Agent_Sites__r  WHERE End_Date__c = NULL) 
                                   FROM Account 
                                   WHERE Name LIKE: '%' + searchKey + '%' 
                                   AND (Id IN: accountIdsList OR (RecordTypeId =: DamacUtility.getRecordTypeId('Account', 'Corporate Agency') 
                                           AND  Blacklisted__c = false AND  Terminated__c = false))]){
            agencyList.add(thisAccount);    
        }
        return agencyList;
    }
    
    /*
    Query the Corporate Agnecy Details and return results on the Page   
    */
    @RemoteAction 
    global static List<Contact> getCorporateAgents(String selectedCorporateAgency){
        List<Contact> agentsList = new List<Contact>();
        try {
            agentsList = [select id,Name from contact where AccountId=:selectedCorporateAgency];
        } Catch (Exception e) {}
        return agentsList;
    }  
    
    /*
    Query the Inquiry details owned by the User Logged in and return the results on the Page
    */
    @RemoteAction 
    global static List<Inquiry__c> getInquiryDetails(String searchKey){
        String key = '%' + searchKey + '%';
        List<Inquiry__c> inquiryList = new List<Inquiry__c>();
        inquiryList = [SELECT id,Name,First_name__c,Last_Name__c,Email__c,Mobile_Phone__c,Passport_Number__c 
                       FROM inquiry__c 
                       WHERE ( First_Name__c LIKE:key OR Last_Name__c LIKE:key OR Name LIKE:key ) 
                               AND  ( Inquiry_Status__c='Active' OR Inquiry_Status__c='New' OR Inquiry_Status__c='Qualified' 
                                           OR Inquiry_Status__c = 'Meeting Scheduled' OR Inquiry_Status__c ='Negotiation' 
                                           OR Inquiry_Status__c = 'Negotiation In-Progress' OR Inquiry_Status__c='Contact Made')
                               AND (recordtype.Name='Inquiry' OR recordtype.Name='Inquiry Decrypted') 
                               AND ownerid=: userinfo.getuserid() 
                               //AND Deal_Exception_Request__c = null
                           LIMIT 10];  
        return inquiryList;
    }
    
    /*
    Query the Account Details owned by the Logged in User and return results on to the Page
    */
    @RemoteAction 
    global static List<Account> getAccountDetails(String searchKey){
        system.debug(searchkey+'####');
        List<Account> AcconuntList = new List<Account>();
            AcconuntList = [SELECT Id, Name, FirstName, LastName 
                            FROM Account 
                            WHERE ownerid =: userinfo.getuserid()  
                                    AND  (FirstName LIKE: searchKey OR LastName LIKE: searchKey OR Name LIKE: '%' + searchkey + '%' ) 
                                    AND Deal_Exception_Request__c = null
                                    AND (recordtype.Name='Person Account'  OR recordtype.Name='Business Account')
                            LIMIT 10];
            System.debug('indiviual acoiunt -----' + AcconuntList);
            return AcconuntList;      
        
    } 


     @RemoteAction
    global static string createBulkDealRequest() {

        Deal_Exception_Request__c dealExpReq = new Deal_Exception_Request__c();
        dealExpReq.Status__c = 'Draft';
        Map<String, User> userNameMap = new Map<String, User>();
        User currentUser;
        for(User usr: [SELECT Id, Name,  DOS_Name__c, HOS_Name__c, HOD_Name__c,Sales_Office__c FROM User WHERE Id =: UserInfo.getUserId()]){
            currentUser = usr;
            userNameMap.put(usr.DOS_Name__c, null);
            userNameMap.put(usr.HOS_Name__c, null);
            userNameMap.put(usr.HOD_Name__c, null);
        }
        dealExpReq.RM__c = currentUser.Id;
        dealExpReq.Sales_Office__c = currentUser.Sales_Office__c;
        for(User usr: [SELECT Id, Name FROM User WHERE Name IN: userNameMap.keyset() AND IsActive = TRUE]){
            userNameMap.put(usr.Name, usr);
        }
        if(userNameMap.containsKey(currentUser.DOS_Name__c) && userNameMap.get(currentUser.DOS_Name__c) != null){
            dealExpReq.DOS__c = userNameMap.get(currentUser.DOS_Name__c).Id;
        }
        if(userNameMap.containsKey(currentUser.HOS_Name__c) && userNameMap.get(currentUser.HOS_Name__c) != null){
            dealExpReq.HOS__c = userNameMap.get(currentUser.HOS_Name__c).Id;
        }
        if(userNameMap.containsKey(currentUser.HOD_Name__c) && userNameMap.get(currentUser.HOD_Name__c) != null){
            dealExpReq.HOD__c = userNameMap.get(currentUser.HOD_Name__c).Id;
            
        }
        dealExpReq.recordTypeId = Schema.SObjectType.Deal_Exception_Request__c.getRecordTypeInfosByName().get('Bulk Deal').getRecordTypeId();
        insert dealExpReq;
        String dealExpId = '';
        dealExpId = dealExpReq.id;
        return dealExpId; 
    }
    
        
     @RemoteAction
    global static string createDealExpRequest(String selectedInvIds) {
        if(selectedInvIds != ''){
            list<String> idHolder = selectedInvIds.split(',');
            System.debug(idHolder.size() +'>>>>>>>>>>>>>>>');
            set<string> tempDeDupeHolder = new set<string>();
            tempDeDupeHolder.addall(idHolder);
            list<inventory__c> preBookInventory = new list<inventory__c>();
            preBookInventory = [select building_location__c from Inventory__c where Id IN: tempDeDupeHolder];
            if(preBookInventory.size()>0){
                //try{
                //V1.2 Store The unit on creation of SR
                String unitOfBooking = '';

                list<inventory__c> allinv = [SELECT Id, Property__c, Property__r.Name, Building_Location__c,View_Type__c ,
                                                 Unit_Area_sft__c , Brand__c ,Bedroom_Type__c,CM_Price_Per_Sqft__c,Location_Code__c,
                                                 IPMS_Bedrooms__c, Special_Price_2__c, Marketing_Name__c, Unit_Name__c,
                                                 Building_Name__c, Property_Name__c, Floor__c, Area_Sqft_2__c, ACD_Date__c,
                                                 Building_Location__r.Building_Name__c, Building_Location__r.Name, 
                                                 Bedrooms__c, Unit__c, Unit_Type__c, Property_Status__c,
                                                 Final_Total_Area__c, Current_AC_Area_SFT__c,
                                                 Area__c, Selling_Price__c, Special_Price__c, Price_Per_Sqft__c,
                                                 MD_Price__c, List_Price__c, Property_Country__c, Status__c, 
                                                 Floor_Package_ID__c,CurrencyIsoCode, Floor_Package_Type__c, 
                                                 List_Price_calc__c, Special_Price_calc__c, Sales_Margins__c
                                             FROM Inventory__c where ID IN: tempDeDupeHolder];
                
                
                UtilityWrapperManager availableInventoriesList = new UtilityWrapperManager();
                availableInventoriesList.ibwList = new List<UtilityWrapperManager.InventoryBuyerWrapper>();
                List<UtilityWrapperManager.InventoryBuyerWrapper> ibwList = new List<UtilityWrapperManager.InventoryBuyerWrapper>();
                map<string,string> buildingIdMap = new map<string,string>();
                String unitCodes = '';
                for(inventory__c thisinventory : allinv){
                    buildingIdMap.put(thisinventory.building_location__c, '');
                    unitOfBooking += thisinventory.Location_Code__c;
                    unitOfBooking += ',';
                    unitCodes += thisinventory.Unit_Name__c+',';
                   ibwList.add(new UtilityWrapperManager.InventoryBuyerWrapper(true, false, false, 40000,0.0, null, '', '', '', 0,
                                                                                thisInventory, new Booking_Unit__c(),
                                                                                new Map<String, Payment_Plan__c>(),
                                                                                new List<Buyer__c>{new Buyer__c(Primary_Buyer__c = true)}));
                }
                if (Test.isRunningTest ()) {
                    availableInventoriesList.ibwList = ibwList;
                    system.debug('availableInventoriesList==='+availableInventoriesList);
                }
                unitCodes = unitCodes.removeEnd (',');
                for(payment_plan__c payPlan: [SELECT Id, building_location__c, Name, 
                                                    (SELECT Name FROM payment_terms__r)
                                                FROM payment_plan__c
                                                WHERE Building_location__c IN: buildingIdMap.keyset()
                                                AND Selected_Units__c = NULL
                                                AND Effective_From__c <= TODAY
                                                AND Effective_To_calculated__c >= TODAY
                                                AND parent_payment_plan__c = null]){
                    buildingIdMap.put(payPlan.building_location__c, payPlan.Id);
                }
                
                Deal_Exception_Request__c dealExpReq = new Deal_Exception_Request__c();
                list<Deal_Exception_Unit__c> deUnitsLst = new list<Deal_Exception_Unit__c>();
                unitOfBooking = unitOfBooking.removeEnd(',');
                dealExpReq.List_of_Units__c = unitOfBooking;
                dealExpReq.Status__c = 'Draft';
                
                Map<String, User> userNameMap = new Map<String, User>();
                User currentUser;
                for(User usr: [SELECT Id, Name,  DOS_Name__c, HOS_Name__c, HOD_Name__c,Sales_Office__c FROM User WHERE Id =: UserInfo.getUserId()]){
                    currentUser = usr;
                    userNameMap.put(usr.DOS_Name__c, null);
                    userNameMap.put(usr.HOS_Name__c, null);
                    userNameMap.put(usr.HOD_Name__c, null);
                }
                dealExpReq.RM__c = currentUser.Id;
                dealExpReq.Sales_Office__c = currentUser.Sales_Office__c;
                for(User usr: [SELECT Id, Name FROM User WHERE Name IN: userNameMap.keyset() AND IsActive = TRUE]){
                    userNameMap.put(usr.Name, usr);
                }
                if(userNameMap.containsKey(currentUser.DOS_Name__c) && userNameMap.get(currentUser.DOS_Name__c) != null){
                    dealExpReq.DOS__c = userNameMap.get(currentUser.DOS_Name__c).Id;
                }
                if(userNameMap.containsKey(currentUser.HOS_Name__c) && userNameMap.get(currentUser.HOS_Name__c) != null){
                    dealExpReq.HOS__c = userNameMap.get(currentUser.HOS_Name__c).Id;
                }
                if(userNameMap.containsKey(currentUser.HOD_Name__c) && userNameMap.get(currentUser.HOD_Name__c) != null){
                    dealExpReq.HOD__c = userNameMap.get(currentUser.HOD_Name__c).Id;
                   
                }
                
                insert dealExpReq;
                Unit_Documents__c unitDoc = new  Unit_Documents__c();
                unitDoc.Document_Name__c = 'Passport Copy'; 
                unitDoc.Deal_Exception_Request__c = dealExpReq.Id;
                unitDoc.Status__c = 'Pending Upload';
                insert unitDoc;  
                for(inventory__c inv: allinv){
                    Deal_Exception_Unit__c deUnits = new Deal_Exception_Unit__c();
                    deUnits.inventory__c = inv.id;
                    if(inv.List_Price__c != null){
                        deUnits.List_Price__c = inv.List_Price__c;
                        if(inv.Status__c == 'Released'){
                            deUnits.Requested_Price__c = inv.List_Price__c;
                        }
                    }
                    deUnits.Unit_Name__c = inv.Unit_Name__c;
                    if(inv.Sales_Margins__c != null){
                        deUnits.Sales_Margins__c = inv.Sales_Margins__c;
                    }
                    deUnits.Area_SFT__c = inv.Final_Total_Area__c;
                    deUnits.Inventory_Status__c = inv.Status__c;
                    deUnits.A_C_Area__c = inv.Current_AC_Area_SFT__c;
                    deUnits.ACD__c = inv.ACD_Date__c;
                    deUnits.Project_Name__c = inv.Marketing_Name__c;
                    if(buildingIdMap.containsKey(inv.building_location__c) && buildingIdMap.get(inv.building_location__c) != ''){
                        deUnits.Payment_Plan__c = buildingIdMap.get(inv.building_location__c);
                    }
                   
                    deUnits.Deal_Exception_Request__c = dealExpReq.id;
                    deUnitsLst.add(deUnits);
                }
                insert deUnitsLst;
                
                Map <String, Id> deUnitIdsMap = new map <String, Id> ();                    
                for (Deal_Exception_Unit__c unit :[ SELECT inventory__r.Unit_Location__r.Name FROM Deal_Exception_Unit__c WHERE ID IN :deUnitsLst]) {
                    deUnitIdsMap.put (unit.inventory__r.Unit_Location__r.Name, unit.Id);
                }
                String dealExpId = '';
                dealExpId = dealExpReq.id;
                return dealExpId; 
            } else{
                return 'Error';
            }
        }        
        return 'Error';
    }



    @RemoteAction
    global static string createFurnitureRequest(String selectedInvIds) {
        if(selectedInvIds != ''){
            list<String> idHolder = selectedInvIds.split(',');
            System.debug(idHolder.size() +'>>>>>>>>>>>>>>>');
            set<string> tempDeDupeHolder = new set<string>();
            tempDeDupeHolder.addall(idHolder);
            list<inventory__c> preBookInventory = new list<inventory__c>();
            preBookInventory = [select building_location__c from Inventory__c where Id IN: tempDeDupeHolder];
            if(preBookInventory.size()>0){
                //try{
                //V1.2 Store The unit on creation of SR
                String unitOfBooking = '';

                list<inventory__c> allinv = [SELECT Id, Property__c, Property__r.Name, Building_Location__c,View_Type__c ,
                                                 Unit_Area_sft__c , Brand__c ,Bedroom_Type__c,CM_Price_Per_Sqft__c,Location_Code__c,
                                                 IPMS_Bedrooms__c, Special_Price_2__c, Marketing_Name__c, Unit_Name__c,
                                                 Building_Name__c, Property_Name__c, Floor__c, Area_Sqft_2__c, ACD_Date__c,
                                                 Building_Location__r.Building_Name__c, Building_Location__r.Name, 
                                                 Bedrooms__c, Unit__c, Unit_Type__c, Property_Status__c,
                                                 Final_Total_Area__c, Current_AC_Area_SFT__c,
                                                 Area__c, Selling_Price__c, Special_Price__c, Price_Per_Sqft__c,
                                                 MD_Price__c, List_Price__c, Property_Country__c, Status__c, 
                                                 Floor_Package_ID__c,CurrencyIsoCode, Floor_Package_Type__c, 
                                                 List_Price_calc__c, Special_Price_calc__c, Sales_Margins__c
                                             FROM Inventory__c where ID IN: tempDeDupeHolder];
                
                
                UtilityWrapperManager availableInventoriesList = new UtilityWrapperManager();
                availableInventoriesList.ibwList = new List<UtilityWrapperManager.InventoryBuyerWrapper>();
                List<UtilityWrapperManager.InventoryBuyerWrapper> ibwList = new List<UtilityWrapperManager.InventoryBuyerWrapper>();
                map<string,string> buildingIdMap = new map<string,string>();
                String unitCodes = '';
                for(inventory__c thisinventory : allinv){
                    buildingIdMap.put(thisinventory.building_location__c, '');
                    unitOfBooking += thisinventory.Location_Code__c;
                    unitOfBooking += ',';
                    unitCodes += thisinventory.Unit_Name__c+',';
                   ibwList.add(new UtilityWrapperManager.InventoryBuyerWrapper(true, false, false, 40000,0.0, null, '', '', '', 0,
                                                                                thisInventory, new Booking_Unit__c(),
                                                                                new Map<String, Payment_Plan__c>(),
                                                                                new List<Buyer__c>{new Buyer__c(Primary_Buyer__c = true)}));
                }
                if (Test.isRunningTest ()) {
                    availableInventoriesList.ibwList = ibwList;
                    system.debug('availableInventoriesList==='+availableInventoriesList);
                }
                unitCodes = unitCodes.removeEnd (',');
                for(payment_plan__c payPlan: [SELECT Id, building_location__c, Name, 
                                                    (SELECT Name FROM payment_terms__r)
                                                FROM payment_plan__c
                                                WHERE Building_location__c IN: buildingIdMap.keyset()
                                                AND Selected_Units__c = NULL
                                                AND Effective_From__c <= TODAY
                                                AND Effective_To_calculated__c >= TODAY
                                                AND parent_payment_plan__c = null]){
                    buildingIdMap.put(payPlan.building_location__c, payPlan.Id);
                }
                
                Deal_Exception_Request__c dealExpReq = new Deal_Exception_Request__c();
                list<Deal_Exception_Unit__c> deUnitsLst = new list<Deal_Exception_Unit__c>();
                unitOfBooking = unitOfBooking.removeEnd(',');
                dealExpReq.List_of_Units__c = unitOfBooking;
                dealExpReq.Status__c = 'Draft';
                
                Map<String, User> userNameMap = new Map<String, User>();
                User currentUser;
                for(User usr: [SELECT Id, Name,  DOS_Name__c, HOS_Name__c, HOD_Name__c,Sales_Office__c FROM User WHERE Id =: UserInfo.getUserId()]){
                    currentUser = usr;
                    userNameMap.put(usr.DOS_Name__c, null);
                    userNameMap.put(usr.HOS_Name__c, null);
                    userNameMap.put(usr.HOD_Name__c, null);
                }
                dealExpReq.RM__c = currentUser.Id;
                dealExpReq.Sales_Office__c = currentUser.Sales_Office__c;
                for(User usr: [SELECT Id, Name FROM User WHERE Name IN: userNameMap.keyset() AND IsActive = TRUE]){
                    userNameMap.put(usr.Name, usr);
                }
                if(userNameMap.containsKey(currentUser.DOS_Name__c) && userNameMap.get(currentUser.DOS_Name__c) != null){
                    dealExpReq.DOS__c = userNameMap.get(currentUser.DOS_Name__c).Id;
                }
                if(userNameMap.containsKey(currentUser.HOS_Name__c) && userNameMap.get(currentUser.HOS_Name__c) != null){
                    dealExpReq.HOS__c = userNameMap.get(currentUser.HOS_Name__c).Id;
                }
                if(userNameMap.containsKey(currentUser.HOD_Name__c) && userNameMap.get(currentUser.HOD_Name__c) != null){
                    dealExpReq.HOD__c = userNameMap.get(currentUser.HOD_Name__c).Id;
                    
                }
                
                dealExpReq.recordTypeId = Schema.SObjectType.Deal_Exception_Request__c.getRecordTypeInfosByName().get('Request Furniture').getRecordTypeId();
                dealExpReq.FRR__c = true;
                dealExpReq.Request_Date__c = DateTime.Now();
                insert dealExpReq;
                Unit_Documents__c unitDoc = new  Unit_Documents__c();
                unitDoc.Document_Name__c = 'Passport Copy'; 
                unitDoc.Deal_Exception_Request__c = dealExpReq.Id;
                unitDoc.Status__c = 'Pending Upload';
                insert unitDoc;  
                Unit_Documents__c unitDocPay = new  Unit_Documents__c();
                unitDocPay.Document_Name__c = 'Proof of payment'; 
                unitDocPay.Deal_Exception_Request__c = dealExpReq.Id;
                unitDocPay.Status__c = 'Pending Upload';
                insert unitDocPay;  
                for(inventory__c inv: allinv){
                    Deal_Exception_Unit__c deUnits = new Deal_Exception_Unit__c();
                    deUnits.inventory__c = inv.id;
                    if(inv.List_Price__c != null){
                        deUnits.List_Price__c = inv.List_Price__c;
                        if(inv.Status__c == 'Released'){
                            deUnits.Requested_Price__c = inv.List_Price__c;
                        }
                    }
                    deUnits.Unit_Name__c = inv.Unit_Name__c;
                    if(inv.Sales_Margins__c != null){
                        deUnits.Sales_Margins__c = inv.Sales_Margins__c;
                    }
                    deUnits.Area_SFT__c = inv.Final_Total_Area__c;
                    deUnits.Inventory_Status__c = inv.Status__c;
                    deUnits.A_C_Area__c = inv.Current_AC_Area_SFT__c;
                    deUnits.ACD__c = inv.ACD_Date__c;
                    deUnits.Project_Name__c = inv.Marketing_Name__c;
                    if(buildingIdMap.containsKey(inv.building_location__c) && buildingIdMap.get(inv.building_location__c) != ''){
                        deUnits.Payment_Plan__c = buildingIdMap.get(inv.building_location__c);
                    }
                   
                    deUnits.Deal_Exception_Request__c = dealExpReq.id;
                    deUnitsLst.add(deUnits);
                }
                insert deUnitsLst;
                Map <String, Id> deUnitIdsMap = new map <String, Id> ();                    
                for (Deal_Exception_Unit__c unit :[ SELECT inventory__r.Unit_Location__r.Name FROM Deal_Exception_Unit__c WHERE ID IN :deUnitsLst]) {
                    deUnitIdsMap.put (unit.inventory__r.Unit_Location__r.Name, unit.Id);
                }
                String dealExpId = '';
                dealExpId = dealExpReq.id;
                return dealExpId; 
            } else{
                return 'Error';
            }
        }        
        return 'Error';
    }


     public static Map<String, Payment_Plan__c> getPaymentPlanRemote (String selectedPaymentPlan){
        Map<String, Payment_Plan__c> paymentPlanTermsListMap = new Map<String,Payment_Plan__c>();
        for(Payment_Plan__c thisPaymentPlan :  [SELECT Id, Building_ID__c, Name, Building_Location__c, Effective_From__c,
                                                Effective_To_calculated__c, TERM_ID__c,
                                                (SELECT Id, Description__c, Milestone_Event__c, Line_ID__c,
                                                 Milestone_Event_Arabic__c, Percent_Value__c, Payment_Date__c,
                                                 Installment__c, Modified_Percent_Value__c
                                                 FROM Payment_Terms__r ORDER  BY Installment__c)
                                                FROM Payment_Plan__c
                                                WHERE Id =: selectedPaymentPlan]){
            if(thisPaymentPlan.Id == selectedPaymentPlan && !thisPaymentPlan.Payment_Terms__r.isEmpty()){
                paymentPlanTermsListMap.put(thisPaymentPlan.Id, thisPaymentPlan);
            }
        }
        system.debug('#### paymentPlanTermsListMap = '+paymentPlanTermsListMap);
        return paymentPlanTermsListMap;
    }    
    
    @RemoteAction    
    public static String createProposedPlan(String paymntPlanId, String derUnitId){
        System.Debug ('paymentPlanId: ' + paymntPlanId );
        System.Debug ('derUnitId: ' + derUnitId );
        Map<String, Payment_Plan__c> paymentPlanMapRemote = getPaymentPlanRemote(paymntPlanId);
        Payment_plan__c plan = paymentPlanMapRemote.get(paymntPlanId);
        System.Debug ('paymentPlanMapRemote: ' + paymentPlanMapRemote );
        System.Debug ('plan: ' + plan );
        Payment_plan__c proposedPlan = plan.clone(false, false, false, false);
        proposedPlan.TERM_ID__c = null;
        insert proposedPlan;
        List<Payment_Terms__c> newPaymentTerms = new List<Payment_Terms__c>();
        List<Payment_Terms__c> tempPaymentTerms = new List<Payment_Terms__c> (paymentPlanMapRemote.get(paymntPlanId).Payment_Terms__r);
        for(Payment_Terms__c thisPaymentTerm : tempPaymentTerms){
            Payment_Terms__c proposedTerm = thisPaymentTerm.clone(false, false, false, false);
            proposedTerm.TERM_ID__c = null;
            proposedTerm.Line_ID__c = null;
            
            proposedTerm.Payment_plan__c = proposedPlan.Id;
            newPaymentTerms.add(proposedTerm);
        }
        insert newPaymentTerms;
        Deal_Exception_Unit__c updUnit = new Deal_Exception_Unit__c();
        updUnit.Id = derUnitId;
        updUnit.proposed_payment_plan__c = proposedPlan.Id;
        update updUnit;
        return proposedPlan.Id;
    }
    
    /*********************************************************************************************
    * @Description : Query the Booking Unit details
    * @Params      : 1. String searchKey - Registration Id of the Booking Unit
    * @Return      : void
    *********************************************************************************************/
    @RemoteAction 
    public static List<SObject> getBookingUnits(String key){
        String regId = key + '%';
        List<SObject> recordList = new List<SObject>();
        List<String> statusList = new List<String>();
        for(String toAddress: Label.DER_Booking_Unit_Registration_Status.split(',')){
            statusList.add(toAddress.trim());
        } 
        system.debug('statusList: ' + statusList);
        string query = 'SELECT Id, Registration_ID__c, Name, Unit_Name__c, Registration_Status__c FROM Booking_Unit__c'  
                    + ' WHERE Registration_ID__c LIKE:  regId' 
                    + ' AND Registration_Status__c IN: statusList'
                    + ' AND Booking__r.Deal_SR__r.OwnerId = \'' +  userinfo.getuserid() + '\' LIMIT 10';
        system.debug('query: ' + query);
        recordList = Database.query(query);
        system.debug('recordList: ' + recordList);
        return recordList;
    }  
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    @RemoteAction
    global static string createpaymentPlanHeader(String selectedInvIds) {
        return 'Error';
    }
}