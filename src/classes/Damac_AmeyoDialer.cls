global class Damac_AmeyoDialer {
    
    public Damac_AmeyoDialer (Apexpages.standardController stdController) {
    }
    public void dialAmeyo() {
        Id inqId = apexpages.currentpage().getparameters().get('id');
        invokeAmeyo(inqId);
    }
    
    webservice static void invokeAmeyo(id inquiryId){
        Inquiry__c inq = [SELECT Encrypted_Mobile__c, Encrypted_Mobile_2__c, createdDate FROM Inquiry__c WHERE ID =: inquiryId LIMIT 1];
        
        Ameyo_Credentials__c credentials = Ameyo_Credentials__c.getInstance(UserInfo.getUserID());
        
        string formattedDate = inq.CreatedDate.format('YYYY-MM-dd');
                         
        String endPoint = '';
        //if(System.label.Ameyo_Switcher == 'true')
            endPoint = credentials.Endpoint__c
                        +'?command='+credentials.Command__c+'&data={%22userId%22:%22'+UserInfo.getUserName()+'%22,%22campaignId%22:%22'
                            +credentials.CampaignId__c+'%22,%22phone%22:%22'+inq.Encrypted_Mobile__c+'%22,%22searchable%22:%22'+inq.Id
                            +'%22,%22shouldAddCustomer%22:%22false%22,%22additionalParams%22:'
                            +'{%22phone1%22:%22'+inq.Encrypted_Mobile__c+'%22,%22phone2%22:%22'+inq.Encrypted_Mobile_2__c+'%22,%22id%22:%22'+inq.Id+'%22,%22inquiry_created_date%22:%22'+formattedDate+'%22}} ';
        /*
        else
            endpoint = credentials.Endpoint__c
                        +'?command='+credentials.Command__c+'&data={%22userId%22:%22'+UserInfo.getUserName()+'%22,%22campaignId%22:%22'
                            +credentials.CampaignId__c+'%22,%22phone%22:%22'+inq.Encrypted_Mobile__c+'%22,%22searchable%22:%22'+inq.Id
                            +'%22,%22shouldAddCustomer%22:%22false%22,%22additionalParams%22:'
                            +'{%22phone1%22:%22'+inq.Encrypted_Mobile__c+'%22,%22id%22:%22'+inq.Id+'%22}} ';
        */
        /*
        String endPoint = credentials.Endpoint__c
                        +'?command='+credentials.Command__c+'&data= ';
        string params = '{';
                params += '"userId":"'+UserInfo.getUserName()+'",';
                params += '"campaignId":"'+credentials.CampaignId__c+'",';
                params += '"phone":"'+inq.Encrypted_Mobile__c+'",';
                params += '"searchable%22":"'+inq.Id+'",';
                params += '"inquiry_created_date":"'+formattedDate+'",';
                params += '"shouldAddCustomer":"false",';
                params += '"additionalParams":{';
                    params += '"phone1":"'+inq.Encrypted_Mobile__c+'",';
                    params += '"id":"'+inq.Id+'"';
                params += '}';
            params += '}';                
        
        System.debug('>>>>Before>>>>>>'+params);
        params = EncodingUtil.urlEncode(params, 'UTF-8');
        System.debug('>>>>After>>>>>>'+params);
        endPoint += params;
        */
        
        system.debug('endpoint='+endPoint);
        
        HTTPRequest req = new HTTPRequest();
        req.setMethod('POST');
        req.setEndpoint(endpoint);
        
        req.setHeader('hash-key', credentials.hash_key__c);
        req.setHeader('host', credentials.host__c);
        req.setHeader('policy-name', credentials.policy_name__c);
        req.setHeader('requesting-host', credentials.requesting_host__c);
        req.setHeader('content-Type', 'application/json');        
        req.setTimeOut(120000);
        
        
        HTTP http = new HTTP();
        HTTPResponse res = new HTTPResponse();
        if (!Test.isRunningTest())
            res = http.send(req);
        
        System.Debug(res.getStatusCode());
        System.Debug(res.getBody());
        
        Ameyo_Log__c log = new Ameyo_Log__c();
        log.CrtObjectId__c = inq.Id+'-'+DateTime.Now().getTime();
        log.srcPhone__c = inq.Encrypted_Mobile__c;
        log.Click_to_Dial_Response__c = res.getBody();
        insert log;
                 
    }
}