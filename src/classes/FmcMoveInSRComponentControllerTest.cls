@istest
public class FmcMoveInSRComponentControllerTest {
    static  Id                          portalAccountId;
    static  User                        portalUser          = new User();
    static  Account                     a                   = new Account();
    static  Account                     a1                  = new Account();
    static  Booking__c                  bk                  = new  Booking__c();
    static  NSIBPM__Service_Request__c  sr                  = new NSIBPM__Service_Request__c();
    static  Booking_Unit__c             bu                  = new Booking_Unit__c();
    static  Booking_Unit__c             bu1                 = new Booking_Unit__c();
    static  Location__c                 locObj              = new Location__c();
    static  FM_Case__c                  fmObj               = new FM_Case__c();
    static  FM_Case__c                  fmObjRes            = new FM_Case__c();
    static  FM_Case__c                  fmObjVeh            = new FM_Case__c();
    static  FM_Case__c                  fmObjHousehold      = new FM_Case__c();
    static  FM_User__c                  userObj             = new FM_User__c();
    static  FM_Additional_Detail__c     addDetails          = new FM_Additional_Detail__c();
    static  FM_Additional_Detail__c     addDetailsRes       = new FM_Additional_Detail__c();
    static  FM_Additional_Detail__c     addDetailsVeh       = new FM_Additional_Detail__c();
    static  FM_Additional_Detail__c     addDetailsHouseHold = new FM_Additional_Detail__c();
    static  SR_Attachments__c           srObj               = new SR_Attachments__c();
    static  Task                        taskObj             = new Task();

    static void SetUp(){

        portalUser = CommunityTestDataFactory.createPortalUser();
        portalAccountId = [ SELECT Id
                                   , Contact.AccountId
                            FROM User
                            WHERE Id = :portalUser.Id ][0].Contact.AccountId;

        /*a.Name = 'Test Account';
        a.party_ID__C = '1039032';
        insert a;*/

        Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName()
                        .get('Deal').getRecordTypeId();
        sr.NSIBPM__Customer__c = portalAccountId;
        sr.RecordTypeId = RecType;
        insert sr;

        bk.Account__c = portalAccountId;
        bk.Deal_SR__c = sr.id;
        insert bk;

        bu.Booking__c = bk.id;
        bu.Unit_Name__c = 'LSB/10/B1001';
        bu.Owner__c = a.id;
        bu.Property_City__c = 'Dubai';
        bu.Handover_Flag__c = 'Y';
        bu.Early_Handover__c = true;
        insert bu;
        bu1.Booking__c = bk.id;
        bu1.Unit_Name__c = 'LSB/10/B1001';
        bu1.Owner__c = a1.id;
        bu1.Property_City__c = 'Dubai';
        bu1.Handover_Flag__c = 'Y';
        bu1.Early_Handover__c = true;
        insert bu1;

        locObj.Name  = 'LSB';
        locObj.Location_ID__c = '83488';
        insert locObj;

        userObj.Building__c = locObj.id;
        userObj.FM_Role__c = 'FM Admin';
        insert userObj;

        Id RecordTypeIdFMCase = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get(
                                    'Move In').getRecordTypeId();
        fmObj.Booking_Unit__c = bu.id;
        fmObj.Expected_move_out_date__c = Date.Today();
        fmObj.Move_in_date__c = Date.Today();
        fmObj.Tenant__c = portalAccountId;
        fmObj.Account__c = portalAccountId;
        fmObj.Access_card_required__c = 'Yes';
        fmObj.Actual_move_in_date__c = system.today();
        fmObj.recordtypeid = RecordTypeIdFMCase;
        insert fmObj;

        fmObjRes.Booking_Unit__c = bu.id;
        fmObjRes.Expected_move_out_date__c = Date.Today();
        fmObjRes.Move_in_date__c = Date.Today();
        fmObjRes.Tenant__c = portalAccountId;
        fmObjRes.Account__c = portalAccountId;
        fmObjRes.Access_card_required__c = 'Yes';
        fmObjRes.Actual_move_in_date__c = system.today();
        fmObjRes.recordtypeid = RecordTypeIdFMCase;
        insert fmObjRes;

        fmObjVeh.Booking_Unit__c = bu.id;
        fmObjVeh.Expected_move_out_date__c = Date.Today();
        fmObjVeh.Move_in_date__c = Date.Today();
        fmObjVeh.Tenant__c = portalAccountId;
        fmObjVeh.Account__c = portalAccountId;
        fmObjVeh.Access_card_required__c = 'Yes';
        fmObjVeh.Actual_move_in_date__c = system.today();
        fmObjVeh.recordtypeid = RecordTypeIdFMCase;
        insert fmObjVeh;

        fmObjHouseHold.Booking_Unit__c = bu.id;
        fmObjHouseHold.Expected_move_out_date__c = Date.Today();
        fmObjHouseHold.Move_in_date__c = Date.Today();
        fmObjHouseHold.Tenant__c = portalAccountId;
        fmObjHouseHold.Account__c = portalAccountId;
        fmObjHouseHold.Access_card_required__c = 'Yes';
        fmObjHouseHold.Actual_move_in_date__c = system.today();
        fmObjHouseHold.recordtypeid = RecordTypeIdFMCase;
        insert fmObjHouseHold;

        srObj.Name = 'Test';
        srObj.FM_Case__c = fmObj.id;
        insert srObj;

        addDetails.Resident_Account__c = portalAccountId;
        addDetails.Household_Account__c = portalAccountId;
        addDetails.Pet_Account__c = portalAccountId;
        addDetails.Vehicle_Account__c = portalAccountId;
        addDetails.Emergency_Contact_Account__c = portalAccountId;
        addDetails.Emergency_Contact_Case__c = fmObj.id;

        addDetails.Resident_Case__c = fmObjRes.id;
        insert addDetails;

        Id RecordTypeIdaddDetailsRes = Schema.SObjectType.FM_Additional_Detail__c
                                        .getRecordTypeInfosByName().get('Resident').getRecordTypeId();
        addDetailsRes.RecordTypeId  =  RecordTypeIdaddDetailsRes;
        addDetailsRes.Resident_Account__c = portalAccountId;
        addDetailsRes.Household_Account__c = portalAccountId;
        addDetailsRes.Pet_Account__c = portalAccountId;
        addDetailsRes.Vehicle_Account__c = portalAccountId;
        addDetailsRes.Resident_Case__c = fmObjRes.id;
        insert addDetailsRes;

        Id RecordTypeIdaddDetailsVeh = Schema.SObjectType.FM_Additional_Detail__c
                                        .getRecordTypeInfosByName().get('Vehicle').getRecordTypeId();
        addDetailsVeh.RecordTypeId  =  RecordTypeIdaddDetailsVeh;
        addDetailsVeh.Resident_Account__c = portalAccountId;
        addDetailsVeh.Household_Account__c = portalAccountId;
        addDetailsVeh.Pet_Account__c = portalAccountId;
        addDetailsVeh.Vehicle_Case__c = fmObjVeh.id;
        addDetailsVeh.Vehicle_Account__c = portalAccountId;
        insert addDetailsVeh;
        Id RecordTypeIdaddDetailsHH = Schema.SObjectType.FM_Additional_Detail__c
                                        .getRecordTypeInfosByName().get('Household').getRecordTypeId();
        addDetailsHouseHold.RecordTypeId  =  RecordTypeIdaddDetailsHH;
        addDetailsHouseHold.Resident_Account__c = portalAccountId;
        addDetailsHouseHold.Household_Account__c = portalAccountId;
        addDetailsHouseHold.Pet_Account__c = portalAccountId;
        addDetailsHouseHold.Vehicle_Account__c = portalAccountId;
        addDetailsHouseHold.Household_Case__c = fmObjHousehold.id;
        insert addDetailsHouseHold;

        taskObj.Description = 'Test'; //string
        taskObj.Status ='Completed';
        taskObj.Subject = Label.Move_in_date_task;
        taskObj.WhatId = fmObj.id; //record id
        insert taskObj;
    }

    static testMethod void testMethod1() {
        SetUp();
        System.debug('portalAccountId = ' + portalAccountId);
        CustomerCommunityUtils.customerAccountId = portalAccountId;
        PageReference currentPage = Page.CommunityPortal;
        Test.setCurrentPage(currentPage);
        ApexPages.currentPage().getParameters().put('view', 'MoveIn');
        ApexPages.currentPage().getParameters().put('AccountId',portalAccountId);
        ApexPages.currentPage().getParameters().put('SRType','Move_in_Request');
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));
        ApexPages.currentPage().getParameters().put('UnitId',String.valueOf(bu.id));
        Test.startTest();
        //System.runAs(portalUser) {
        FmcMoveInSRComponentController moveInObj = new FmcMoveInSRComponentController();
        moveInObj.strDate = String.valueOf('12/12/2018');
        //moveInObj.getUnitDetails();
        moveInObj.submitSr();
        //}
        Test.stopTest();
    }

    static testMethod void testMethodSaveAsDraft() {
        SetUp();

        System.debug('portalAccountId = ' + portalAccountId);
        CustomerCommunityUtils.customerAccountId = portalAccountId;

        PageReference myVfPage = Page.CommunityPortal;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('view', 'MoveIn');
        ApexPages.currentPage().getParameters().put('UnitId', bu.id);
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));
        Test.startTest();
        //System.runAs(portalUser) {
        FmcMoveInSRComponentController moveInObj = new FmcMoveInSRComponentController();
        Id RecordTypeIdaddDetails = Schema.SObjectType.FM_Additional_Detail__c
                                    .getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
        addDetails.RecordTypeId  =  RecordTypeIdaddDetails;
        moveInObj.getUnitDetails();
        moveInObj.insertCase();
        moveInObj.saveSr();
        //}
        Test.stopTest();
    }

    static testMethod void testMethodSaveAsDraftRes() {
        SetUp();
        System.debug('portalAccountId = ' + portalAccountId);
        CustomerCommunityUtils.customerAccountId = portalAccountId;

        PageReference myVfPage = Page.CommunityPortal;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('view', 'MoveIn');
        ApexPages.currentPage().getParameters().put('UnitId', bu.id);
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObjRes.id));
        Test.startTest();
        //System.runAs(portalUser) {
        FmcMoveInSRComponentController moveInObj = new FmcMoveInSRComponentController();
        moveInObj.getUnitDetails();
        moveInObj.insertCase();
        moveInObj.saveAsDraft();
        //}
        Test.stopTest();
    }

    static testMethod void testMethodSaveAsDraftVeh() {
        SetUp();
        System.debug('portalAccountId = ' + portalAccountId);
        CustomerCommunityUtils.customerAccountId = portalAccountId;

        PageReference myVfPage = Page.CommunityPortal;


        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('view', 'MoveIn');
        ApexPages.currentPage().getParameters().put('UnitId', bu.id);
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObjVeh.id));
        Test.startTest();
        //System.runAs(portalUser) {
        FmcMoveInSRComponentController moveInObj = new FmcMoveInSRComponentController();
        moveInObj.getUnitDetails();
        moveInObj.insertCase();
        moveInObj.saveAsDraft();
        //}
        Test.stopTest();
    }

    static testMethod void testMethodSaveAsDraftHouseHold() {
        SetUp();
        System.debug('portalAccountId = ' + portalAccountId);
        CustomerCommunityUtils.customerAccountId = portalAccountId;

        PageReference myVfPage = Page.CommunityPortal;
        myVfPage.getParameters().put('view', 'MoveIn');

        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObjHouseHold.id));
        ApexPages.currentPage().getParameters().put('UnitId', bu.id);

        Test.startTest();
        //System.runAs(portalUser) {
        FmcMoveInSRComponentController moveInObj = new FmcMoveInSRComponentController();
        moveInObj.getUnitDetails();
        moveInObj.insertCase();
        moveInObj.saveAsDraft();
        //}
        Test.stopTest();
    }

    static testMethod void testMethodaddDetailsRes() {
        SetUp();
        System.debug('portalAccountId = ' + portalAccountId);
        CustomerCommunityUtils.customerAccountId = portalAccountId;

        PageReference myVfPage = Page.CommunityPortal;
        ApexPages.currentPage().getParameters().put('view', 'MoveIn');

        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));

        Test.startTest();
        //System.runAs(portalUser) {
        FmcMoveInSRComponentController moveInObj = new FmcMoveInSRComponentController();
        moveInObj.detailType = 'Resident';
        moveInObj.getUnitDetails();
        //moveInObj.addDetails();
        moveInObj.indexVar = 0;
        moveInObj.removeDetails();
        //}
        Test.stopTest();
    }

    static testMethod void testMethodaddDetailshouseHold() {
        SetUp();

        PageReference myVfPage = Page.MoveInRequestPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('view', 'MoveIn');
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));
        ApexPages.currentPage().getParameters().put('UnitId', bu.id);

        CustomerCommunityUtils.customerAccountId = portalAccountId;

        Test.startTest();
        //System.runAs(portalUser) {
        FmcMoveInSRComponentController moveInObj = new FmcMoveInSRComponentController();
        moveInObj.detailType = 'Household';
        moveInObj.getUnitDetails();
        //moveInObj.addDetails();
        moveInObj.indexVar = 0;
        moveInObj.removeDetails();
        //}
        Test.stopTest();
    }

    static testMethod void testMethodaddDetailsVehicle() {
        SetUp();
        System.debug('portalAccountId = ' + portalAccountId);
        CustomerCommunityUtils.customerAccountId = portalAccountId;

        PageReference myVfPage = Page.CommunityPortal;
        myVfPage.getParameters().put('view', 'MoveIn');

        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));
        ApexPages.currentPage().getParameters().put('UnitId', bu.id);

        Test.startTest();
        //System.runAs(portalUser) {
        FmcMoveInSRComponentController moveInObj = new FmcMoveInSRComponentController();
        moveInObj.detailType = 'Vehicle';
        moveInObj.getUnitDetails();
        moveInObj.addDetails();
        moveInObj.indexVar = 0;
        moveInObj.removeDetails();
        //}
        Test.stopTest();
    }

    static testMethod void testMethodaddDetailsPet() {
        SetUp();
        System.debug('portalAccountId = ' + portalAccountId);
        CustomerCommunityUtils.customerAccountId = portalAccountId;

        PageReference myVfPage = Page.CommunityPortal;
        myVfPage.getParameters().put('view', 'MoveIn');

        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));
        ApexPages.currentPage().getParameters().put('UnitId', bu.id);

        Test.startTest();
        //System.runAs(portalUser) {
        FmcMoveInSRComponentController moveInObj = new FmcMoveInSRComponentController();
        moveInObj.detailType = 'Pet Details';
        moveInObj.getUnitDetails();
        moveInObj.addDetails();
        moveInObj.indexVar = 0;
        moveInObj.removeDetails();
        //}
        Test.stopTest();
    }

    static testMethod void testMethodaddDetailsEC() {
        SetUp();
        System.debug('portalAccountId = ' + portalAccountId);
        CustomerCommunityUtils.customerAccountId = portalAccountId;

        PageReference myVfPage = Page.CommunityPortal;
        myVfPage.getParameters().put('view', 'MoveIn');

        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));
        ApexPages.currentPage().getParameters().put('UnitId', bu.id);

        Test.startTest();
        //System.runAs(portalUser) {
        FmcMoveInSRComponentController moveInObj = new FmcMoveInSRComponentController();
        moveInObj.detailType = 'Emergency Contact';
        moveInObj.getUnitDetails();
        moveInObj.addDetails();
        moveInObj.indexVar = 0;
        moveInObj.removeDetails();
        //}
        Test.stopTest();
    }

    static testMethod void testMethoddeleteDoc() {
        SetUp();
        System.debug('portalAccountId = ' + portalAccountId);
        CustomerCommunityUtils.customerAccountId = portalAccountId;

        PageReference myVfPage = Page.CommunityPortal;
        myVfPage.getParameters().put('view', 'MoveIn');

        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));
        ApexPages.currentPage().getParameters().put('UnitId', bu.id);

        Test.startTest();
        //System.runAs(portalUser) {
        FmcMoveInSRComponentController moveInObj = new FmcMoveInSRComponentController();
        moveInObj.isTenant = true;
        moveInObj.docIdToDelete = srObj.id;
        moveInObj.getUnitDetails();
        moveInObj.deleteDocument();
        //}
        Test.stopTest();
    }

    static testMethod void testWOCaseId() {
        SetUp();
        System.debug('portalAccountId = ' + portalAccountId);
        CustomerCommunityUtils.customerAccountId = portalAccountId;

        PageReference myVfPage = Page.CommunityPortal;
        myVfPage.getParameters().put('view', 'MoveIn');
        myVfPage.getParameters().put('id', fmObj.id);

        Test.setCurrentPage(myVfPage);

        Test.startTest();
        //System.runAs(portalUser) {
        FmcMoveInSRComponentController moveInObj = new FmcMoveInSRComponentController();
        //moveInObj.FMCaseID = 'a4025000000UPKP';
        moveInObj.FMCaseID = fmObj.id;
        moveInObj.getUnitDetails();
        moveInObj.goToTerms();
        //}
        Test.stopTest();
    }

    static testMethod void testUserMaster() {
        SetUp();

        FM_User__c objFmUser = new FM_User__c();
        objFmUser.Building__c = locObj.id;
        objFmUser.FM_Role__c = 'Master Community Property Manager';
        insert objFmUser;

        System.debug('portalAccountId = ' + portalAccountId);
        CustomerCommunityUtils.customerAccountId = portalAccountId;

        PageReference myVfPage = Page.CommunityPortal;
        myVfPage.getParameters().put('view', 'MoveIn');
        myVfPage.getParameters().put('id', fmObj.id);

        Test.setCurrentPage(myVfPage);

        Test.startTest();
        //System.runAs(portalUser) {
        FmcMoveInSRComponentController moveInObj = new FmcMoveInSRComponentController();
        //moveInObj.FMCaseID = 'a4025000000UPKP';
        moveInObj.FMCaseID = fmObj.id;
        moveInObj.getUnitDetails();
        moveInObj.goToTerms();
        //}
        Test.stopTest();
    }

    static testMethod void testAsGuestUser() {
        SetUp();
        fmObj.Status__c = 'New';
        fmObj.Origin__c = 'Portal - Guest';
        update fmObj;

        User guestUser = [SELECT Id FROM User WHERE Name = :FmcUtils.GUEST_USER_NAME];

        System.debug('portalAccountId = ' + portalAccountId);
        CustomerCommunityUtils.customerAccountId = portalAccountId;

        PageReference myVfPage = Page.CommunityPortal;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('view', 'MoveIn');
        ApexPages.currentPage().getParameters().put('UnitId', bu.id);
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));

        Test.startTest();

            System.runAs(guestUser) {
                FmcMoveInSRComponentController moveInObj = new FmcMoveInSRComponentController();
                Id RecordTypeIdaddDetails = Schema.SObjectType.FM_Additional_Detail__c
                                            .getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
                addDetails.RecordTypeId  =  RecordTypeIdaddDetails;
                moveInObj.getUnitDetails();
                moveInObj.insertCase();
                moveInObj.saveSr();
            }

        Test.stopTest();
    }

    static testMethod void testWoCaseIdAsGuestUser() {
        SetUp();
        fmObj.Status__c = 'New';
        fmObj.Origin__c = 'Portal - Guest';
        update fmObj;

        User guestUser = [SELECT Id FROM User WHERE Name = :FmcUtils.GUEST_USER_NAME];

        System.debug('portalAccountId = ' + portalAccountId);
        CustomerCommunityUtils.customerAccountId = portalAccountId;

        PageReference myVfPage = Page.CommunityPortal;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('view', 'MoveIn');
        ApexPages.currentPage().getParameters().put('UnitId', bu.id);
        //ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));

        Test.startTest();

            System.runAs(guestUser) {
                FmcMoveInSRComponentController moveInObj = new FmcMoveInSRComponentController();
                Id RecordTypeIdaddDetails = Schema.SObjectType.FM_Additional_Detail__c
                                            .getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
                addDetails.RecordTypeId  =  RecordTypeIdaddDetails;
                moveInObj.getUnitDetails();
                moveInObj.insertCase();
                moveInObj.saveSr();
            }

        Test.stopTest();
    }

    static testMethod void testSubmittedCaseAsGuestUser() {
        SetUp();
        fmObj.Status__c = 'Submitted';
        fmObj.Origin__c = 'Portal - Guest';
        update fmObj;

        User guestUser = [SELECT Id FROM User WHERE Name = :FmcUtils.GUEST_USER_NAME];

        System.debug('portalAccountId = ' + portalAccountId);
        CustomerCommunityUtils.customerAccountId = portalAccountId;

        PageReference myVfPage = Page.CommunityPortal;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('view', 'MoveIn');
        ApexPages.currentPage().getParameters().put('UnitId', bu.id);
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));

        Test.startTest();

            System.runAs(guestUser) {
                FmcMoveInSRComponentController moveInObj = new FmcMoveInSRComponentController();
                Id RecordTypeIdaddDetails = Schema.SObjectType.FM_Additional_Detail__c
                                            .getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
                addDetails.RecordTypeId  =  RecordTypeIdaddDetails;
                moveInObj.getUnitDetails();
                moveInObj.insertCase();
                moveInObj.saveSr();
            }

        Test.stopTest();
    }

    /*static testMethod void testSubmittedCaseWoCaseIdAsGuestUser() {
        SetUp();
        fmObj.Status__c = 'Submitted';
        fmObj.Origin__c = 'Portal - Guest';
        update fmObj;

        User guestUser = [SELECT Id FROM User WHERE Name = :FmcUtils.GUEST_USER_NAME];

        System.debug('portalAccountId = ' + portalAccountId);
        CustomerCommunityUtils.customerAccountId = portalAccountId;

        PageReference myVfPage = Page.CommunityPortal;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('view', 'MoveIn');
        ApexPages.currentPage().getParameters().put('UnitId', bu.id);
        //ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));

        Test.startTest();

            System.runAs(guestUser) {
                FmcMoveInSRComponentController moveInObj = new FmcMoveInSRComponentController();
                Id RecordTypeIdaddDetails = Schema.SObjectType.FM_Additional_Detail__c
                                            .getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
                addDetails.RecordTypeId  =  RecordTypeIdaddDetails;
                moveInObj.getUnitDetails();
                moveInObj.insertCase();
                moveInObj.saveSr();
            }

        Test.stopTest();
    }*/

    static testMethod void testWoCaseIdUnitIdAsGuestUser() {
        SetUp();
        fmObj.Origin__c = 'Portal - Guest';
        fmObj.Status__c = 'New';
        update fmObj;

        User guestUser = [SELECT Id FROM User WHERE Name = :FmcUtils.GUEST_USER_NAME];

        System.debug('portalAccountId = ' + portalAccountId);
        CustomerCommunityUtils.customerAccountId = portalAccountId;

        PageReference myVfPage = Page.CommunityPortal;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('view', 'MoveIn');
        ApexPages.currentPage().getParameters().put('UnitId', bu.id);
        //ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));

        Test.startTest();

            System.runAs(guestUser) {
                FmcMoveInSRComponentController moveInObj = new FmcMoveInSRComponentController();
                Id RecordTypeIdaddDetails = Schema.SObjectType.FM_Additional_Detail__c
                                            .getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
                addDetails.RecordTypeId  =  RecordTypeIdaddDetails;
                moveInObj.getUnitDetails();
                moveInObj.insertCase();
                moveInObj.saveSr();
            }

        Test.stopTest();
    }

    static testMethod void testInvalidUnitIdAsGuestUser() {
        SetUp();
        fmObj.Origin__c = 'Portal - Guest';
        update fmObj;

        User guestUser = [SELECT Id FROM User WHERE Name = :FmcUtils.GUEST_USER_NAME];

        System.debug('portalAccountId = ' + portalAccountId);
        CustomerCommunityUtils.customerAccountId = portalAccountId;

        PageReference myVfPage = Page.CommunityPortal;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('view', 'MoveIn');
        ApexPages.currentPage().getParameters().put('UnitId', fmObj.id);
        //ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));

        Test.startTest();

            System.runAs(guestUser) {
                FmcMoveInSRComponentController moveInObj = new FmcMoveInSRComponentController();
                Id RecordTypeIdaddDetails = Schema.SObjectType.FM_Additional_Detail__c
                                            .getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
                addDetails.RecordTypeId  =  RecordTypeIdaddDetails;
                moveInObj.getUnitDetails();
                moveInObj.insertCase();
                moveInObj.saveSr();
            }

        Test.stopTest();
    }

    static testMethod void testBlankUnitIdAsGuestUser() {
        SetUp();
        fmObj.Origin__c = 'Portal - Guest';
        update fmObj;

        User guestUser = [SELECT Id FROM User WHERE Name = :FmcUtils.GUEST_USER_NAME];

        System.debug('portalAccountId = ' + portalAccountId);
        CustomerCommunityUtils.customerAccountId = portalAccountId;

        PageReference myVfPage = Page.CommunityPortal;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('view', 'MoveIn');
        ApexPages.currentPage().getParameters().put('UnitId', '');
        //ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));

        Test.startTest();

            System.runAs(guestUser) {
                FmcMoveInSRComponentController moveInObj = new FmcMoveInSRComponentController();
                Id RecordTypeIdaddDetails = Schema.SObjectType.FM_Additional_Detail__c
                                            .getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
                addDetails.RecordTypeId  =  RecordTypeIdaddDetails;
                moveInObj.getUnitDetails();
                moveInObj.insertCase();
                moveInObj.saveSr();
            }

        Test.stopTest();
    }

    static testMethod void testUnitIdAsGuestUser() {
        SetUp();
        fmObj.Origin__c = 'Portal - Guest';
        fmObj.Status__c = 'New';
        update fmObj;

        User guestUser = [SELECT Id FROM User WHERE Name = :FmcUtils.GUEST_USER_NAME];

        System.debug('portalAccountId = ' + portalAccountId);
        CustomerCommunityUtils.customerAccountId = portalAccountId;

        PageReference myVfPage = Page.CommunityPortal;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('view', 'MoveIn');
        ApexPages.currentPage().getParameters().put('UnitId', fmObj.id);
        //ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));

        Test.startTest();

            System.runAs(guestUser) {
                FmcMoveInSRComponentController moveInObj = new FmcMoveInSRComponentController();
                Id RecordTypeIdaddDetails = Schema.SObjectType.FM_Additional_Detail__c
                                            .getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
                addDetails.RecordTypeId  =  RecordTypeIdaddDetails;
                moveInObj.getUnitDetails();
                moveInObj.insertCase();
                moveInObj.saveSr();
            }

        Test.stopTest();
    }
}