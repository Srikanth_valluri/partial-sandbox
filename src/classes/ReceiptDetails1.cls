//Generated by wsdl2apex

public class ReceiptDetails1 {
    public class GenerateReceipt_element {
        public String P_REQUEST_NUMBER;
        public String P_REQUEST_NAME;
        public String P_SOURCE_SYSTEM;
        public ReceiptDetails2.APPSXXDC_PROCESS_SERX1794747X1X5[] regTerms;
        private String[] P_REQUEST_NUMBER_type_info = new String[]{'P_REQUEST_NUMBER','http://action.com',null,'0','1','true'};
        private String[] P_REQUEST_NAME_type_info = new String[]{'P_REQUEST_NAME','http://action.com',null,'0','1','true'};
        private String[] P_SOURCE_SYSTEM_type_info = new String[]{'P_SOURCE_SYSTEM','http://action.com',null,'0','1','true'};
        private String[] regTerms_type_info = new String[]{'regTerms','http://action.com',null,'0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://action.com','true','true'};
        private String[] field_order_type_info = new String[]{'P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM','regTerms'};
    }
    public class GetReceiptDetailsResponse_element {
        public String return_x;
        private String[] return_x_type_info = new String[]{'return','http://action.com',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://action.com','true','true'};
        private String[] field_order_type_info = new String[]{'return_x'};
    }
    public class GetReceiptDetails_element {
        public String P_REQUEST_NUMBER;
        public String P_REQUEST_NAME;
        public String P_SOURCE_SYSTEM;
        public ReceiptDetails3.APPSXXDC_PROCESS_SERX1794747X2X4 regTerms;
        private String[] P_REQUEST_NUMBER_type_info = new String[]{'P_REQUEST_NUMBER','http://action.com',null,'0','1','true'};
        private String[] P_REQUEST_NAME_type_info = new String[]{'P_REQUEST_NAME','http://action.com',null,'0','1','true'};
        private String[] P_SOURCE_SYSTEM_type_info = new String[]{'P_SOURCE_SYSTEM','http://action.com',null,'0','1','true'};
        private String[] regTerms_type_info = new String[]{'regTerms','http://action.com',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://action.com','true','true'};
        private String[] field_order_type_info = new String[]{'P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM','regTerms'};
    }
    public class GenerateReceipt1Response_element {
        public String return_x;
        private String[] return_x_type_info = new String[]{'return','http://action.com',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://action.com','true','true'};
        private String[] field_order_type_info = new String[]{'return_x'};
    }
    public class GenerateReceipt1_element {
        public String P_REQUEST_NAME;
        public String P_SOURCE_SYSTEM;
        public String ParamId;
        public String Attr1;
        public String Attr2;
        public String Attr3;
        private String[] P_REQUEST_NAME_type_info = new String[]{'P_REQUEST_NAME','http://action.com',null,'0','1','true'};
        private String[] P_SOURCE_SYSTEM_type_info = new String[]{'P_SOURCE_SYSTEM','http://action.com',null,'0','1','true'};
        private String[] ParamId_type_info = new String[]{'ParamId','http://action.com',null,'0','1','true'};
        private String[] Attr1_type_info = new String[]{'Attr1','http://action.com',null,'0','1','true'};
        private String[] Attr2_type_info = new String[]{'Attr2','http://action.com',null,'0','1','true'};
        private String[] Attr3_type_info = new String[]{'Attr3','http://action.com',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://action.com','true','true'};
        private String[] field_order_type_info = new String[]{'P_REQUEST_NAME','P_SOURCE_SYSTEM','ParamId','Attr1','Attr2','Attr3'};
    }
    public class GenerateReceiptResponse_element {
        public String return_x;
        private String[] return_x_type_info = new String[]{'return','http://action.com',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://action.com','true','true'};
        private String[] field_order_type_info = new String[]{'return_x'};
    }
    public class ReceiptIPMSHttpSoap11Endpoint {
        public String endpoint_x = Label.WSDLBaseUrl+':8080/CRM_SR_NEW/services/ReceiptIPMS.ReceiptIPMSHttpSoap11Endpoint/';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://action.com', 'ReceiptDetails1', 'http://process.xxdc_process_service_ws.plsql.soaprovider.ont.apps.xmlns.oracle.com/xsd', 'ReceiptDetails2', 'http://retrieve.xxdc_process_service_ws.plsql.soaprovider.ont.apps.xmlns.oracle.com/xsd', 'ReceiptDetails3'};
        public String GenerateReceipt1(String P_REQUEST_NAME,String P_SOURCE_SYSTEM,String ParamId,String Attr1,String Attr2,String Attr3) {
            ReceiptDetails1.GenerateReceipt1_element request_x = new ReceiptDetails1.GenerateReceipt1_element();
            request_x.P_REQUEST_NAME = P_REQUEST_NAME;
            request_x.P_SOURCE_SYSTEM = P_SOURCE_SYSTEM;
            request_x.ParamId = ParamId;
            request_x.Attr1 = Attr1;
            request_x.Attr2 = Attr2;
            request_x.Attr3 = Attr3;
            ReceiptDetails1.GenerateReceipt1Response_element response_x;
            Map<String, ReceiptDetails1.GenerateReceipt1Response_element> response_map_x = new Map<String, ReceiptDetails1.GenerateReceipt1Response_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'urn:GenerateReceipt1',
              'http://action.com',
              'GenerateReceipt1',
              'http://action.com',
              'GenerateReceipt1Response',
              'ReceiptDetails1.GenerateReceipt1Response_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.return_x;
        }
        public String GetReceiptDetails(String P_REQUEST_NUMBER,String P_REQUEST_NAME,String P_SOURCE_SYSTEM,ReceiptDetails3.APPSXXDC_PROCESS_SERX1794747X2X4 regTerms) {
            ReceiptDetails1.GetReceiptDetails_element request_x = new ReceiptDetails1.GetReceiptDetails_element();
            request_x.P_REQUEST_NUMBER = P_REQUEST_NUMBER;
            request_x.P_REQUEST_NAME = P_REQUEST_NAME;
            request_x.P_SOURCE_SYSTEM = P_SOURCE_SYSTEM;
            request_x.regTerms = regTerms;
            ReceiptDetails1.GetReceiptDetailsResponse_element response_x;
            Map<String, ReceiptDetails1.GetReceiptDetailsResponse_element> response_map_x = new Map<String, ReceiptDetails1.GetReceiptDetailsResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'urn:GetReceiptDetails',
              'http://action.com',
              'GetReceiptDetails',
              'http://action.com',
              'GetReceiptDetailsResponse',
              'ReceiptDetails1.GetReceiptDetailsResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.return_x;
        }
        public String GenerateReceipt(String P_REQUEST_NUMBER,String P_REQUEST_NAME,String P_SOURCE_SYSTEM,ReceiptDetails2.APPSXXDC_PROCESS_SERX1794747X1X5[] regTerms) {
            ReceiptDetails1.GenerateReceipt_element request_x = new ReceiptDetails1.GenerateReceipt_element();
            request_x.P_REQUEST_NUMBER = P_REQUEST_NUMBER;
            request_x.P_REQUEST_NAME = P_REQUEST_NAME;
            request_x.P_SOURCE_SYSTEM = P_SOURCE_SYSTEM;
            request_x.regTerms = regTerms;
            ReceiptDetails1.GenerateReceiptResponse_element response_x;
            Map<String, ReceiptDetails1.GenerateReceiptResponse_element> response_map_x = new Map<String, ReceiptDetails1.GenerateReceiptResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'urn:GenerateReceipt',
              'http://action.com',
              'GenerateReceipt',
              'http://action.com',
              'GenerateReceiptResponse',
              'ReceiptDetails1.GenerateReceiptResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.return_x;
        }
    }
}