@isTest
public class HDApp_DewaRegistration_APITest {
    @isTest
    static void testDewaRegistration(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/dewaRegistrationForBookingUnit'; 
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        HDApp_DewaRegistration_API.dewaRegistrationProcess();
        Test.stopTest();
    }
    
    @isTest
    static void testDewaRegistration2(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/dewaRegistrationForBookingUnit'; 
        req.httpMethod = 'POST';
        req.addParameter('bookingUnitId', '');
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        HDApp_DewaRegistration_API.dewaRegistrationProcess();
        Test.stopTest();
    }
    
    @isTest
    static void testDewaRegistration3(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/dewaRegistrationForBookingUnit'; 
        req.httpMethod = 'POST';
        req.addParameter('bookingUnitId', 'test');
        req.addParameter('accountId', '');
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        HDApp_DewaRegistration_API.dewaRegistrationProcess();
        Test.stopTest();
    }
    
     @isTest
    static void testDewaRegistration4(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/dewaRegistrationForBookingUnit'; 
        req.httpMethod = 'POST';
        req.addParameter('bookingUnitId', 'test');
        req.addParameter('accountId', 'test');
        req.addParameter('dewaNumber', '');
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        HDApp_DewaRegistration_API.dewaRegistrationProcess();
        Test.stopTest();
    }
    
    @isTest
    static void testDewaRegistration5(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/dewaRegistrationForBookingUnit'; 
        req.httpMethod = 'POST';
        req.addParameter('bookingUnitId', 'test');
        req.addParameter('accountId', 'test');
        req.addParameter('dewaNumber', '12345');
        req.addParameter('documentName', '');
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        HDApp_DewaRegistration_API.dewaRegistrationProcess();
        Test.stopTest();
    }
    
    @isTest
    static void testDewaRegistration6(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/dewaRegistrationForBookingUnit'; 
        req.httpMethod = 'POST';
        req.addParameter('bookingUnitId', 'test');
        req.addParameter('accountId', 'test');
        req.addParameter('dewaNumber', '12345');
        req.addParameter('documentName', 'test');
        req.addParameter('fileName', '');
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        HDApp_DewaRegistration_API.dewaRegistrationProcess();
        Test.stopTest();
    }
    
    @isTest
    static void testDewaRegistration7(){
        
        Account objAccount = new Account();
        objAccount.Name = 'test';
        insert objAccount;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        objBooking.Account__c = objAccount.Id; 
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        //objBU.DLP_End_Date__c = Date.today().addDays(2);
        objBU.Registration_ID__c = '3901';
        objBU.Unit_Name__c = 'test';
        objBU.Booking__c = objBooking.Id;
        //objBU.DEWA_Utility_Number__c = '12345';
        insert objBU;
        
        SR_Attachments__c objSRAttachment = new SR_Attachments__c();
        objSRAttachment.Attachment_URL__c = 'test@salesforce.com';
        objSRAttachment.Booking_Unit__c = objBU.Id;
        objSRAttachment.Attachment__c = 'test';
        insert objSRAttachment;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/dewaRegistrationForBookingUnit'; 
        req.httpMethod = 'POST';
        req.addParameter('bookingUnitId', objBU.Id);
        req.addParameter('dewaNumber', '12345');
        req.addParameter('accountId', objAccount.Id);
        req.addParameter('documentName', 'test');
        req.addParameter('fileName', 'test.pdf');
        req.requestBody = Blob.valueof('test.pdf');
        RestContext.request = req;
        RestContext.response = res;

        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
        
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        //Test.setMock(HttpCalloutMock.class, new Office365RestService());
        Test.startTest();
        HDApp_DewaRegistration_API.dewaRegistrationProcess();
        Test.stopTest();
    }
}