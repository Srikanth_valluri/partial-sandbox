@isTest
public class Invocable_ResetSalesAudit_Test {
    
    @testSetup 
    static void setupData() {
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        Id DealRT = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c parentSR = new NSIBPM__Service_Request__c();
        parentSR.NSIBPM__Customer__c = acc.Id;
        parentSR.RecordTypeId = DealRT;
        parentSR.Agency__c = acc.id;
        parentSR.Agency_Type__c = 'Corporate';
        insert parentSR;
        
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = acc.Id;
        sr.RecordTypeId = DealRT;
        sr.Agency__c = acc.id;
        sr.Agency_Type__c = 'Corporate';
        sr.NSIBPM__Parent_SR__c = parentSR.Id;
        insert sr;
        
        New_Step__c nstp1 = new New_Step__c();
        nstp1.Service_Request__c = sr.id;
        nstp1.Step_No__c = 2;
        nstp1.Step_Status__c = 'Awaiting Token Deposit';
        nstp1.Step_Type__c = 'Token Payment';
        insert nstp1;
        
        Booking__c booking = new Booking__c(Account__c = acc.Id, Deal_SR__c = parentSR.Id);
        insert booking;
    }
    
    @isTest
    static void test_ResetSalesAudit() {   
        New_Step__c nstp = [select id From New_Step__c];
        Invocable_ResetSalesAudit.ResetSalesAudit(new List<Id> {nstp.Id});
    }
    
    
}