public class DLD_STATUS_RESP {
    public String procedureKey;
    public String status;
    
    
    public static DLD_STATUS_RESP parse(String json) {
        return (DLD_STATUS_RESP) System.JSON.deserialize(json, DLD_STATUS_RESP.class);
    }
}