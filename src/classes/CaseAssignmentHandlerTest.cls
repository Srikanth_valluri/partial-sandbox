@isTest
private without sharing class CaseAssignmentHandlerTest {

    private static void setMock() {
        SOAPCalloutServiceMock.returnToMe = new Map < String, TaskCreationWSDL.SRDataToIPMSMultipleResponse_element > ();
        TaskCreationWSDL.SRDataToIPMSMultipleResponse_element response_x = new TaskCreationWSDL.SRDataToIPMSMultipleResponse_element();
        response_x.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"SR Header Data Created for SR # :2-005057","PARAM_ID":"2-005057"},{"PROC_STATUS":"S","PROC_MESSAGE":"Created SR Task Data for SR # :2-005057 Task :Verify POA Documents","PARAM_ID":"2-005057"},{"PROC_STATUS":"E","PROC_MESSAGE":"Error: Param Id Not Passed...","PARAM_ID":"NULL"}],"message":"[WARNING] Check the PROC_MESSAGE attribute for Actual Error Message(s), Error Message Count = 1","status":"S"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response_x);
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
    }

    static testMethod void cocdwoUser() {
        setMock();

        Test.startTest();
        CaseAssignmentHandler CAH = new CaseAssignmentHandler();
        User communityUser = CommunityTestDataFactory.CreatePortalUser();
        communityUser = [SELECT Id, Name, ContactId, Contact.AccountId
                         FROM User
                         WHERE Id =: CommunityUser.Id];
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();

        system.runAs(communityUser) {
            Case objCase = TestDataFactory_CRM.createCase(communityUser.Contact.AccountId, recTypeCOD);
            objCase.Status = 'New';
            objCase.Type = 'Change of Contact Details';
            objCase.Origin = 'Portal';
            objCase.IsPOA__c = true;
            objCase.POA_Task_Created__c = false;
            objCase.OQOOD_Verification_Task_Created__c = false;
            insert objCase;

            List < Case > updatedObjCase = [SELECT ID, Status
                                            FROM CASE
                                            WHERE ID =: objCase.ID AND RecordTypeId != NULL];
            updatedObjCase[0].Status = 'Submitted';
            update updatedObjCase;
        }
        Test.stopTest();
    }

    static testMethod void cocdPrimaryCRE() {
        setMock();

        Test.startTest();
        CaseAssignmentHandler CAH = new CaseAssignmentHandler();
        String currentUserId = UserInfo.getUserId();
        User communityUser = CommunityTestDataFactory.CreatePortalUser();
        communityUser = [SELECT Id, Name, ContactId, Contact.AccountId
                         FROM User
                         WHERE Id =: CommunityUser.Id];
        List<Account> lstAcc = [Select Id, Primary_CRE__c, Secondary_CRE__c, Tertiary_CRE__c
                                From Account
                                Where Id =: communityUser.Contact.AccountId];
        lstAcc[0].Primary_CRE__c = currentUserId;
        update lstAcc;
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();

        system.runAs(communityUser) {
            Case objCase = TestDataFactory_CRM.createCase(communityUser.Contact.AccountId, recTypeCOD);
            objCase.Status = 'New';
            objCase.Type = 'Change of Contact Details';
            objCase.Origin = 'Portal';
            objCase.IsPOA__c = true;
            objCase.POA_Task_Created__c = false;
            objCase.OQOOD_Verification_Task_Created__c = false;
            insert objCase;

            List < Case > updatedObjCase = [SELECT ID, Status
                                            FROM CASE
                                            WHERE ID =: objCase.ID AND RecordTypeId != NULL];
            updatedObjCase[0].Status = 'Submitted';
            update updatedObjCase;
        }
        Test.stopTest();
    }
    static testMethod void cocdSecondaryCRE() {
        setMock();

        Test.startTest();
        CaseAssignmentHandler CAH = new CaseAssignmentHandler();
        String currentUserId = UserInfo.getUserId();
        User communityUser = CommunityTestDataFactory.CreatePortalUser();
        communityUser = [SELECT Id, Name, ContactId, Contact.AccountId
                         FROM User
                         WHERE Id =: CommunityUser.Id];
        List<Account> lstAcc = [Select Id, Primary_CRE__c, Secondary_CRE__c, Tertiary_CRE__c
                                From Account
                                Where Id =: communityUser.Contact.AccountId];
        lstAcc[0].Secondary_CRE__c = currentUserId;
        update lstAcc;
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();

        system.runAs(communityUser) {
            Case objCase = TestDataFactory_CRM.createCase(communityUser.Contact.AccountId, recTypeCOD);
            objCase.Status = 'New';
            objCase.Type = 'Change of Contact Details';
            objCase.Origin = 'Portal';
            objCase.IsPOA__c = true;
            objCase.POA_Task_Created__c = false;
            objCase.OQOOD_Verification_Task_Created__c = false;
            insert objCase;

            List < Case > updatedObjCase = [SELECT ID, Status
                                            FROM CASE
                                            WHERE ID =: objCase.ID AND RecordTypeId != NULL];
            updatedObjCase[0].Status = 'Submitted';
            update updatedObjCase;

        }
        Test.stopTest();
    }
    static testMethod void cocdTertiaryCRE() {
        setMock();

        Test.startTest();
        CaseAssignmentHandler CAH = new CaseAssignmentHandler();
        String currentUserId = UserInfo.getUserId();
        User communityUser = CommunityTestDataFactory.CreatePortalUser();
        communityUser = [SELECT Id, Name, ContactId, Contact.AccountId
                         FROM User
                         WHERE Id =: CommunityUser.Id];
        List<Account> lstAcc = [Select Id, Primary_CRE__c, Secondary_CRE__c, Tertiary_CRE__c
                                From Account
                                Where Id =: communityUser.Contact.AccountId];
        lstAcc[0].Tertiary_CRE__c = currentUserId;
        update lstAcc;
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();

        system.runAs(communityUser) {
            Case objCase = TestDataFactory_CRM.createCase(communityUser.Contact.AccountId, recTypeCOD);
            objCase.Status = 'New';
            objCase.Type = 'Change of Contact Details';
            objCase.Origin = 'Portal';
            objCase.IsPOA__c = true;
            objCase.POA_Task_Created__c = false;
            objCase.OQOOD_Verification_Task_Created__c = false;
            insert objCase;

            List < Case > updatedObjCase = [SELECT ID, Status
                                            FROM CASE
                                            WHERE ID =: objCase.ID AND RecordTypeId != NULL];
            updatedObjCase[0].Status = 'Submitted';
            update updatedObjCase;
        }
        Test.stopTest();
    }

    // POP test methods

    static testMethod void popWoUser() {
        setMock();

        Test.startTest();
        CaseAssignmentHandler CAH = new CaseAssignmentHandler();
        User communityUser = CommunityTestDataFactory.CreatePortalUser();
        communityUser = [SELECT Id, Name, ContactId, Contact.AccountId
                         FROM User
                         WHERE Id =: CommunityUser.Id];
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POP').getRecordTypeId();

        system.runAs(communityUser) {
            Case objCase = TestDataFactory_CRM.createCase(communityUser.Contact.AccountId, recTypeCOD);
            objCase.Status = 'New';
            objCase.Type = 'Change of Contact Details';
            objCase.Origin = 'Portal';
            objCase.IsPOA__c = true;
            objCase.POA_Task_Created__c = false;
            objCase.OQOOD_Verification_Task_Created__c = false;
            insert objCase;

            List < Case > updatedObjCase = [SELECT ID, Status
                                            FROM CASE
                                            WHERE ID =: objCase.ID AND RecordTypeId != NULL];
            updatedObjCase[0].Status = 'Submitted';
            update updatedObjCase;
        }
        Test.stopTest();
    }

    static testMethod void popPrimaryCRE() {
        setMock();

        Test.startTest();
        CaseAssignmentHandler CAH = new CaseAssignmentHandler();
        String currentUserId = UserInfo.getUserId();
        User communityUser = CommunityTestDataFactory.CreatePortalUser();
        communityUser = [SELECT Id, Name, ContactId, Contact.AccountId
                         FROM User
                         WHERE Id =: CommunityUser.Id];
        List<Account> lstAcc = [Select Id, Primary_CRE__c, Secondary_CRE__c, Tertiary_CRE__c
                                From Account
                                Where Id =: communityUser.Contact.AccountId];
        lstAcc[0].Primary_CRE__c = currentUserId;
        update lstAcc;
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POP').getRecordTypeId();

        system.runAs(communityUser) {
            Case objCase = TestDataFactory_CRM.createCase(communityUser.Contact.AccountId, recTypeCOD);
            objCase.Status = 'New';
            objCase.Type = 'Change of Contact Details';
            objCase.Origin = 'Portal';
            objCase.IsPOA__c = true;
            objCase.POA_Task_Created__c = false;
            objCase.OQOOD_Verification_Task_Created__c = false;
            insert objCase;

            List < Case > updatedObjCase = [SELECT ID, Status
                                            FROM CASE
                                            WHERE ID =: objCase.ID AND RecordTypeId != NULL];
            updatedObjCase[0].Status = 'Submitted';
            update updatedObjCase;
        }
        Test.stopTest();
    }
    static testMethod void popSecondaryCRE() {
        setMock();

        Test.startTest();
        CaseAssignmentHandler CAH = new CaseAssignmentHandler();
        String currentUserId = UserInfo.getUserId();
        User communityUser = CommunityTestDataFactory.CreatePortalUser();
        communityUser = [SELECT Id, Name, ContactId, Contact.AccountId
                         FROM User
                         WHERE Id =: CommunityUser.Id];
        List<Account> lstAcc = [Select Id, Primary_CRE__c, Secondary_CRE__c, Tertiary_CRE__c
                                From Account
                                Where Id =: communityUser.Contact.AccountId];
        lstAcc[0].Secondary_CRE__c = currentUserId;
        update lstAcc;
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POP').getRecordTypeId();

        system.runAs(communityUser) {
            Case objCase = TestDataFactory_CRM.createCase(communityUser.Contact.AccountId, recTypeCOD);
            objCase.Status = 'New';
            objCase.Type = 'Change of Contact Details';
            objCase.Origin = 'Portal';
            objCase.IsPOA__c = true;
            objCase.POA_Task_Created__c = false;
            objCase.OQOOD_Verification_Task_Created__c = false;
            insert objCase;

            List < Case > updatedObjCase = [SELECT ID, Status
                                            FROM CASE
                                            WHERE ID =: objCase.ID AND RecordTypeId != NULL];
            updatedObjCase[0].Status = 'Submitted';
            update updatedObjCase;

        }
        Test.stopTest();
    }
    static testMethod void popTertiaryCRE() {
        setMock();

        Test.startTest();
        CaseAssignmentHandler CAH = new CaseAssignmentHandler();
        String currentUserId = UserInfo.getUserId();
        User communityUser = CommunityTestDataFactory.CreatePortalUser();
        communityUser = [SELECT Id, Name, ContactId, Contact.AccountId
                         FROM User
                         WHERE Id =: CommunityUser.Id];
        List<Account> lstAcc = [Select Id, Primary_CRE__c, Secondary_CRE__c, Tertiary_CRE__c
                                From Account
                                Where Id =: communityUser.Contact.AccountId];
        lstAcc[0].Tertiary_CRE__c = currentUserId;
        update lstAcc;
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POP').getRecordTypeId();

        system.runAs(communityUser) {
            Case objCase = TestDataFactory_CRM.createCase(communityUser.Contact.AccountId, recTypeCOD);
            objCase.Status = 'New';
            objCase.Type = 'Change of Contact Details';
            objCase.Origin = 'Portal';
            objCase.IsPOA__c = true;
            objCase.POA_Task_Created__c = false;
            objCase.OQOOD_Verification_Task_Created__c = false;
            insert objCase;

            List < Case > updatedObjCase = [SELECT ID, Status
                                            FROM CASE
                                            WHERE ID =: objCase.ID AND RecordTypeId != NULL];
            updatedObjCase[0].Status = 'Submitted';
            update updatedObjCase;
        }
        Test.stopTest();
    }

    // Parking test methods

    static testMethod void parkingWoUser() {
        setMock();

        Test.startTest();
        CaseAssignmentHandler CAH = new CaseAssignmentHandler();
        User communityUser = CommunityTestDataFactory.CreatePortalUser();
        communityUser = [SELECT Id, Name, ContactId, Contact.AccountId
                         FROM User
                         WHERE Id =: CommunityUser.Id];
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Parking').getRecordTypeId();

        system.runAs(communityUser) {
            Case objCase = TestDataFactory_CRM.createCase(communityUser.Contact.AccountId, recTypeCOD);
            objCase.Status = 'New';
            objCase.Type = 'Change of Contact Details';
            objCase.Origin = 'Portal';
            objCase.IsPOA__c = true;
            objCase.POA_Task_Created__c = false;
            objCase.OQOOD_Verification_Task_Created__c = false;
            insert objCase;

            List < Case > updatedObjCase = [SELECT ID, Status
                                            FROM CASE
                                            WHERE ID =: objCase.ID AND RecordTypeId != NULL];
            updatedObjCase[0].Status = 'Submitted';
            update updatedObjCase;
        }
        Test.stopTest();
    }

    static testMethod void parkingPrimaryCRE() {
        setMock();

        Test.startTest();
        CaseAssignmentHandler CAH = new CaseAssignmentHandler();
        String currentUserId = UserInfo.getUserId();
        User communityUser = CommunityTestDataFactory.CreatePortalUser();
        communityUser = [SELECT Id, Name, ContactId, Contact.AccountId
                         FROM User
                         WHERE Id =: CommunityUser.Id];
        List<Account> lstAcc = [Select Id, Primary_CRE__c, Secondary_CRE__c, Tertiary_CRE__c
                                From Account
                                Where Id =: communityUser.Contact.AccountId];
        lstAcc[0].Primary_CRE__c = currentUserId;
        update lstAcc;
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Parking').getRecordTypeId();

        system.runAs(communityUser) {
            Case objCase = TestDataFactory_CRM.createCase(communityUser.Contact.AccountId, recTypeCOD);
            objCase.Status = 'New';
            objCase.Type = 'Change of Contact Details';
            objCase.Origin = 'Portal';
            objCase.IsPOA__c = true;
            objCase.POA_Task_Created__c = false;
            objCase.OQOOD_Verification_Task_Created__c = false;
            insert objCase;

            List < Case > updatedObjCase = [SELECT ID, Status
                                            FROM CASE
                                            WHERE ID =: objCase.ID AND RecordTypeId != NULL];
            updatedObjCase[0].Status = 'Submitted';
            update updatedObjCase;
        }
        Test.stopTest();
    }
    static testMethod void parkingSecondaryCRE() {
        setMock();

        Test.startTest();
        CaseAssignmentHandler CAH = new CaseAssignmentHandler();
        String currentUserId = UserInfo.getUserId();
        User communityUser = CommunityTestDataFactory.CreatePortalUser();
        communityUser = [SELECT Id, Name, ContactId, Contact.AccountId
                         FROM User
                         WHERE Id =: CommunityUser.Id];
        List<Account> lstAcc = [Select Id, Primary_CRE__c, Secondary_CRE__c, Tertiary_CRE__c
                                From Account
                                Where Id =: communityUser.Contact.AccountId];
        lstAcc[0].Secondary_CRE__c = currentUserId;
        update lstAcc;
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Parking').getRecordTypeId();

        system.runAs(communityUser) {
            Case objCase = TestDataFactory_CRM.createCase(communityUser.Contact.AccountId, recTypeCOD);
            objCase.Status = 'New';
            objCase.Type = 'Change of Contact Details';
            objCase.Origin = 'Portal';
            objCase.IsPOA__c = true;
            objCase.POA_Task_Created__c = false;
            objCase.OQOOD_Verification_Task_Created__c = false;
            insert objCase;

            List < Case > updatedObjCase = [SELECT ID, Status
                                            FROM CASE
                                            WHERE ID =: objCase.ID AND RecordTypeId != NULL];
            updatedObjCase[0].Status = 'Submitted';
            update updatedObjCase;

        }
        Test.stopTest();
    }
    static testMethod void parkingTertiaryCRE() {
        setMock();

        Test.startTest();
        CaseAssignmentHandler CAH = new CaseAssignmentHandler();
        String currentUserId = UserInfo.getUserId();
        User communityUser = CommunityTestDataFactory.CreatePortalUser();
        communityUser = [SELECT Id, Name, ContactId, Contact.AccountId
                         FROM User
                         WHERE Id =: CommunityUser.Id];
        List<Account> lstAcc = [Select Id, Primary_CRE__c, Secondary_CRE__c, Tertiary_CRE__c
                                From Account
                                Where Id =: communityUser.Contact.AccountId];
        lstAcc[0].Tertiary_CRE__c = currentUserId;
        update lstAcc;
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Parking').getRecordTypeId();

        system.runAs(communityUser) {
            Case objCase = TestDataFactory_CRM.createCase(communityUser.Contact.AccountId, recTypeCOD);
            objCase.Status = 'New';
            objCase.Type = 'Change of Contact Details';
            objCase.Origin = 'Portal';
            objCase.IsPOA__c = true;
            objCase.POA_Task_Created__c = false;
            objCase.OQOOD_Verification_Task_Created__c = false;
            insert objCase;

            List < Case > updatedObjCase = [SELECT ID, Status
                                            FROM CASE
                                            WHERE ID =: objCase.ID AND RecordTypeId != NULL];
            updatedObjCase[0].Status = 'Submitted';
            update updatedObjCase;
        }
        Test.stopTest();
    }
}