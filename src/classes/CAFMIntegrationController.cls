global virtual without sharing class CAFMIntegrationController {
    
    webservice static void getAllBuildings(){
        String sessionId;
        sessionId = getSessionIDfromCAFM();
        System.debug('SessionId--->'+sessionId);
        schemasDatacontractOrg200407FsiCon_c3.ArrayOfBuildingDto allBuildingsResult =
            new schemasDatacontractOrg200407FsiCon_c3.ArrayOfBuildingDto();
        wwwFsiCoUkServicesEvolution0409_c3.PropertyServiceV31  obj = new  wwwFsiCoUkServicesEvolution0409_c3.PropertyServiceV31();
        allBuildingsResult = obj.GetAllBuildings(sessionId,0);
        //System.debug('===allBuildingsResult===>'+allBuildingsResult.BuildingDto);
        //System.debug('===allBuildingsResult===>'+allBuildingsResult.BuildingDto.size());
        for(schemasDatacontractOrg200407FsiCon_c3.BuildingDto objBuilding : allBuildingsResult.BuildingDto) {
            System.debug('objBuilding>>>>'+objBuilding);
            System.debug('objBuilding>>>>'+objBuilding.BuildingId);
            //System.debug('Limits.getHeapSize()>>>>>>>>>>'+Limits.getHeapSize());
        }
        
    }
    //Parameters Inpersonate Id and SiteId
    webservice static void getBuildingsBySiteId(){
        String sessionId;
        sessionId = getSessionIDfromCAFM();
        System.debug('SessionId--->'+sessionId);
        schemasDatacontractOrg200407FsiCon_c3.ArrayOfBuildingDto allBuildingBySiteIdResult =
            new schemasDatacontractOrg200407FsiCon_c3.ArrayOfBuildingDto();
        wwwFsiCoUkServicesEvolution0409_c3.PropertyServiceV31  obj = new  wwwFsiCoUkServicesEvolution0409_c3.PropertyServiceV31();
        allBuildingBySiteIdResult = obj.GetBuildingsBySiteId(sessionId,0,115);
        System.debug('===allBuildingBySiteIdResult===>'+allBuildingBySiteIdResult);
        System.debug('===allBuildingsResult===>'+allBuildingBySiteIdResult.BuildingDto);
        System.debug('===allBuildingsResult===>'+allBuildingBySiteIdResult.BuildingDto.size());
        for(schemasDatacontractOrg200407FsiCon_c3.BuildingDto objBuilding : allBuildingBySiteIdResult.BuildingDto) {
            System.debug('objBuilding>>>>'+objBuilding);
        }
    }
    
    webservice static void getAllLocations(){
        String sessionId;
        sessionId = getSessionIDfromCAFM();
        System.debug('SessionId--->'+sessionId);
        schemasDatacontractOrg200407FsiCon_c3.ArrayOfLocationDto allLocationsResult =
            new schemasDatacontractOrg200407FsiCon_c3.ArrayOfLocationDto();
        wwwFsiCoUkServicesEvolution0409_c3.PropertyServiceV31  obj = new  wwwFsiCoUkServicesEvolution0409_c3.PropertyServiceV31();
        allLocationsResult = obj.GetAllLocations(sessionId,Null);
        System.debug('===allLocationsResult===>'+allLocationsResult);
    }
    
    webservice static void getLocationsByBuildingId(Long impersonateAccountId,Long buildingId){
        String sessionId;
        sessionId = getSessionIDfromCAFM();
        System.debug('SessionId--->'+sessionId);
        schemasDatacontractOrg200407FsiCon_c3.ArrayOfLocationDto allLocationsByBuildingIdResult =
            new schemasDatacontractOrg200407FsiCon_c3.ArrayOfLocationDto();
        wwwFsiCoUkServicesEvolution0409_c3.PropertyServiceV31  obj = new  wwwFsiCoUkServicesEvolution0409_c3.PropertyServiceV31();
        allLocationsByBuildingIdResult = obj.GetLocationsByBuildingId(sessionId,impersonateAccountId,buildingId);
        System.debug('===allLocationsByBuildingIdResult===>'+allLocationsByBuildingIdResult);
    }
    
    /*webservice static void getAllContracts(Long impersonateAccountId){
        String sessionId;
        sessionId = getSessionIDfromCAFM();
        System.debug('SessionId--->'+sessionId);
        schemasDatacontractOrg200407FsiCon_c2.ArrayOfContractDtoV3 getAllContractsResult =
            new schemasDatacontractOrg200407FsiCon_c2.ArrayOfContractDtoV3();
        wwwFsiCoUkServicesEvolution0409_c2.ContractServiceV31  obj = new  wwwFsiCoUkServicesEvolution0409_c2.ContractServiceV31();
        getAllContractsResult = obj.GetAllContracts(sessionId,impersonateAccountId);
        System.debug('===getAllContractsResult===>'+getAllContractsResult);
    }*/
        
    @TestVisible
    private static String getSessionIDfromCAFM() {
      try{
          wwwFsiCoUkServicesEvolution0409.SecurityService1 obj = new wwwFsiCoUkServicesEvolution0409.SecurityService1();
          obj.timeout_x = 120000;
          schemasDatacontractOrg200407FsiPlat.AuthenticationDto result = obj.Authenticate(Label.CAFM_Username,Label.CAFM_Password);
          system.debug('>>result>>>'+result);
          if(result != NULL) {
              if(result.OperationResult == 'Succeeded') {
                  System.debug('===result.SessionId=='+result);
                  return result.SessionId;
              }
          }
      }
      catch(System.Exception excp) {
          System.debug('==excp==SESSION ID='+excp);
          insert new Error_Log__c(Error_Details__c =excp.getMessage() + ': '+excp.getLineNumber(),
                                  // FM_Case__c = objFMCase.Id,
                                  Process_Name__c = 'Helpdesk' );
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,excp.getMessage() + excp.getLineNumber()));
      }
     return NULL;
  }

}