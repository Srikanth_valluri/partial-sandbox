@isTest
public without sharing Class CaseExtensionControllerTest {
     @isTest
    static void testAccountDetail(){
        Account objAccount = new Account();
        objAccount.Name = 'test';
        insert objAccount;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        objBooking.Account__c = objAccount.Id;
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Booking__c = objBooking.Id;
        insert objBU;
        
        
     Id recordTypeId =
          [SELECT Id FROM RecordType
            WHERE DeveloperName = 'Customer_Refund' AND sObjectType = 'Case'].Id;
        


        Case objCase = new Case();
        objCase.AccountId = objCase.Id;
        objCase.RecordTypeId = recordTypeId;
        insert objCase;
        System.debug('objCase>>>>>>>>>>>>'+objCase.RecordTypeId);
        System.debug('objCase>>>'+objCase.RecordType.DeveloperName);
        
        Id recordTypeCaseExtId =
          [SELECT Id FROM RecordType
            WHERE DeveloperName = 'Customer_Refund' AND sObjectType = 'Case_Extension__c'].Id;
        Case_Extension__c objCaseExt = new Case_Extension__c();
        objCaseExt.Case__c = objCase.id;
        objCaseExt.RecordTypeId = recordTypeCaseExtId;
        insert objCaseExt;
        
        //CaseExtensionController objCaseExt = new CaseExtensionController();
        CaseExtensionController.getCaseExtensionId(objCase.Id);
        
    }
}