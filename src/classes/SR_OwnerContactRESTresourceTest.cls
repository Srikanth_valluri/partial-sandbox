@isTest
public class SR_OwnerContactRESTresourceTest {
    static testMethod void  test1(){
        Id personAcc_rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account acc1 = new Account( LastName = 'Test Account1',
                                       Party_ID__c = '63066662',
                                       RecordtypeId = personAcc_rtId,
                                       Email__pc = 'test1@mailinator.com');
        insert acc1;
        Account acc2 = new Account( LastName = 'Test Account2',
                                       Party_ID__c = '63066663',
                                       RecordtypeId = personAcc_rtId,
                                       Email__pc = 'test2@mailinator.com');
        insert acc2;
        
        Id contctDetailChgCase_rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
        Case testConDetChgCase = new Case();
        testConDetChgCase.recordTypeId = contctDetailChgCase_rtId;
        testConDetChgCase.AccountId = acc2.id;
        testConDetChgCase.type = 'Change of Contact Details';
        testConDetChgCase.SR_Type__c = 'Change of Contact Details';
        testConDetChgCase.Origin = 'Portal';
        testConDetChgCase.status = 'Draft Request';
        testConDetChgCase.Mobile_Country_Code__c = 'United Arab Emirates: 00971';
        testConDetChgCase.Contact_Mobile__c = '9988776655';
        testConDetChgCase.Contact_Email__c = 'name@test.com';
        testConDetChgCase.Country__c = 'United Arab Emirates';
        testConDetChgCase.Address__c = 'Test address';
        testConDetChgCase.City__c = 'Dubai';
        testConDetChgCase.State__c = 'State';
        testConDetChgCase.Postal_Code__c = '665544';
        testConDetChgCase.CRF_File_URL__c = 'cocd_file_url';
        testConDetChgCase.Additional_Doc_File_URL__c = 'additional_doc_file_url';
        insert testConDetChgCase;
        
        Case srCase = [SELECT id, caseNumber, AccountId FROM Case WHERE AccountId = :acc2.id LIMIT 1];
        
        String baseUrl = URL.getOrgDomainUrl().toExternalForm(); /* URL.getSalesforceBaseUrl() */
        
        Test.startTest();
        
        RestRequest request = new RestRequest();
        request.requestUri = baseUrl + '/services/apexrest/srCase/owner/contact';
        request.httpMethod = 'GET';
        
        RestResponse response = new RestResponse();
		
        RestContext.request = request;
        RestContext.response = response;
        
        SR_OwnerContactRESTresource.doGet(); /* 1 */
        
        request.addParameter('case_number', 'invalid');
        SR_OwnerContactRESTresource.doGet(); /* 2 */
        
        request.addParameter('account_id', String.valueOf(srCase.id));
        SR_OwnerContactRESTresource.doGet(); /* 3 */
        
        request.addParameter('account_id', String.valueOf(acc2.id));
        SR_OwnerContactRESTresource.doGet(); /* 4 */
        
        request.addParameter('case_number', String.valueOf(srCase.caseNumber));
        SR_OwnerContactRESTresource.doGet(); /* 5 */
        
        SR_OwnerContactRESTresource.doDeleteDraft(); /* 6 */
        
        Test.stopTest();
    }
    
    static testMethod void  test2(){
        Id personAcc_rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account acc1 = new Account( LastName = 'Test Account1',
                                       Party_ID__c = '63066662',
                                       RecordtypeId = personAcc_rtId,
                                       Email__pc = 'test1@mailinator.com');
        insert acc1;
        Account acc2 = new Account( LastName = 'Test Account2',
                                       Party_ID__c = '63066663',
                                       RecordtypeId = personAcc_rtId,
                                       Email__pc = 'test2@mailinator.com');
        acc2.Primary_Language__c = 'Arabic';
        insert acc2;
        
        Id contctDetailChgCase_rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
        Case testConDetChgCase = new Case();
        testConDetChgCase.recordTypeId = contctDetailChgCase_rtId;
        testConDetChgCase.AccountId = acc2.id;
        testConDetChgCase.type = 'Change of Contact Details';
        testConDetChgCase.SR_Type__c = 'Change of Contact Details';
        testConDetChgCase.Origin = 'Portal';
        testConDetChgCase.status = 'Draft Request';
        testConDetChgCase.Mobile_Country_Code__c = 'United Arab Emirates: 00971';
        testConDetChgCase.Contact_Mobile__c = '9988776655';
        testConDetChgCase.Contact_Email__c = 'name@test.com';
        testConDetChgCase.Country__c = 'United Arab Emirates';
        testConDetChgCase.Address__c = 'Test address';
        testConDetChgCase.City__c = 'Dubai';
        testConDetChgCase.State__c = 'State';
        testConDetChgCase.Postal_Code__c = '665544';
        testConDetChgCase.CRF_File_URL__c = 'cocd_file_url';
        testConDetChgCase.Additional_Doc_File_URL__c = 'additional_doc_file_url';
        insert testConDetChgCase;
        
        Map<String, Object> requestMap_post = new Map<String, Object>();
        requestMap_post.put('is_draft', true);
        requestMap_post.put('account_id', '');
        requestMap_post.put('draft_sr_id', ''); 
        requestMap_post.put('country', ''); 
        requestMap_post.put('state', ''); 
        requestMap_post.put('city', ''); 
        requestMap_post.put('address', ''); 
        requestMap_post.put('postal_code', ''); 
        requestMap_post.put('primary_email_id', ''); 
        requestMap_post.put('country_code', ''); 
        requestMap_post.put('primary_mobile_number', ''); 
        requestMap_post.put('passport_file_url', ''); 
        requestMap_post.put('cocd_form_url', 'https://testcdn/docs/6758765465');
        requestMap_post.put('additional_doc_file_url', 'https://testcdn/docs/6758765465');
        
        String reqJSON = JSON.serialize(requestMap_post);
        String baseUrl = URL.getOrgDomainUrl().toExternalForm(); /* URL.getSalesforceBaseUrl() */
        
        Test.startTest();
        
        RestRequest request = new RestRequest();
        request.requestUri = baseUrl + '/services/apexrest/srCase/owner/passport';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(reqJSON);
        
        RestResponse response = new RestResponse();
		
        RestContext.request = request;
        RestContext.response = response;
        
        callPostMethod(requestMap_post); /* 1 empty AccountId*/
        
        requestMap_post.put('account_id', String.valueOf(acc1.id));
        callPostMethod(requestMap_post); /* 2 required fields empty */
        
        requestMap_post.put('country', 'United Arab Emirates'); 
        requestMap_post.put('state', 'Dubai'); 
        requestMap_post.put('city', 'Dubai'); 
        requestMap_post.put('address', 'Test address, Test street'); 
        requestMap_post.put('postal_code', '665544'); 
        requestMap_post.put('primary_email_id', 'test22@abc.com'); 
        requestMap_post.put('country_code', 'United Arab Emirates: 00971'); 
        requestMap_post.put('primary_mobile_number', '9988776655'); 
        requestMap_post.put('account_id', String.valueOf(testConDetChgCase.id));
        callPostMethod(requestMap_post); /* 3 invalid Account */
        
        requestMap_post.put('account_id', String.valueOf(acc1.id));
        callPostMethod(requestMap_post); /* 4 valid case: save as draft */
        
        requestMap_post.put('account_id', String.valueOf(acc1.id));
        callPostMethod(requestMap_post); /* 4i invalid case: save as draft : duplicate */
        
        requestMap_post.put('is_draft', false); // testConDetChgCase
        requestMap_post.put('account_id', String.valueOf(acc2.id));
        requestMap_post.put('cocd_form_url', '');
        callPostMethod(requestMap_post); /* 5 invalid submit case : cocd_form_url absent */
        
        requestMap_post.put('draft_sr_id', '');
        requestMap_post.put('cocd_form_url', 'https://testcdn/docs/6758765465');
        callPostMethod(requestMap_post); /* 6 invalid submit case : draft_sr_id absent */
        
        requestMap_post.put('draft_sr_id', String.valueOf(acc1.id));
        callPostMethod(requestMap_post); /* 7 invalid submit case : draft_sr_id invalid */
        
        requestMap_post.put('draft_sr_id', String.valueOf(testConDetChgCase.id));
        requestMap_post.put('is_draft', true);
        callPostMethod(requestMap_post); /* 8 invalid save as draft : duplicate */
        
        requestMap_post.put('is_draft', false);
        callPostMethod(requestMap_post); /* 9 valid submit case */
        
        Test.stopTest();
    }
    
    private static void callPostMethod(Map<String, Object> requestMap_post){
        SR_OwnerContactRESTresource.doPost(
        (Boolean)requestMap_post.get('is_draft'), 
        (String)requestMap_post.get('account_id'), 
        (String)requestMap_post.get('draft_sr_id'), 
        (String)requestMap_post.get('country'), 
        (String)requestMap_post.get('state'), 
        (String)requestMap_post.get('city'), 
        (String)requestMap_post.get('address'), 
        (String)requestMap_post.get('postal_code'), 
        (String)requestMap_post.get('primary_email_id'), 
        (String)requestMap_post.get('country_code'), 
        (String)requestMap_post.get('primary_mobile_number'), 
        (String)requestMap_post.get('passport_file_url'), 
        (String)requestMap_post.get('cocd_form_url')
        );
    }
}