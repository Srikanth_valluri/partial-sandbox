public class InvRestrictionReqTriggerHandler {
    
    
    public static void afterUpdateHandler(list<Inventory_Restriction_Request__c> newList, map<id, Inventory_Restriction_Request__c> oldMap){
        List<Inventory_Restriction_Request__c> approvedReqList = new List<Inventory_Restriction_Request__c>();
         List<Inventory_Restriction_Request__c> restrictedReqList = new List<Inventory_Restriction_Request__c>();
        system.debug(oldMap);
        for(Inventory_Restriction_Request__c invResReq: newList){
            system.debug(invResReq);
            system.debug(oldMap.get(invResReq.Id).Status__c );
            if(invResReq.Status__c != oldMap.get(invResReq.Id).Status__c 
                    && invResReq.Status__c == 'Approved'
                    && oldMap.get(invResReq.Id).Status__c == 'Submitted'){
                approvedReqList.add(invResReq);
            } 
             if(invResReq.Status__c != oldMap.get(invResReq.Id).Status__c 
                    && invResReq.Status__c == 'Completed'
                    && oldMap.get(invResReq.Id).Status__c == 'Approved'){
                restrictedReqList.add(invResReq);
            } 
        }
        system.debug('approvedReqList: ' + approvedReqList);
        system.debug('restrictedReqList: ' + restrictedReqList);
        if(approvedReqList.size() > 0){
            notifySalesAmdinsReqApproval(approvedReqList);
        } 
        if(restrictedReqList.size() > 0){
            notifyInventoryRestriction(restrictedReqList);
        }    
    }

    public static void beforeUpdateHandler(list<Inventory_Restriction_Request__c> newList, map<id, Inventory_Restriction_Request__c> oldMap){
        List<Inventory_Restriction_Request__c> reqList = new List<Inventory_Restriction_Request__c>();
       
        for(Inventory_Restriction_Request__c invResReq: newList){
            system.debug(invResReq);
             system.debug(oldMap.get(invResReq.Id).Functional_User__c+'++'+invResReq.Functional_User__c);
             system.debug(oldMap.get(invResReq.Id).Restrict_Reason__c+'++'+invResReq.Restrict_Reason__c);
            system.debug(oldMap.get(invResReq.Id).Function__c+'++'+invResReq.Function__c);
            if((invResReq.Function__c != oldMap.get(invResReq.Id).Function__c 
                    || invResReq.Functional_User__c != oldMap.get(invResReq.Id).Functional_User__c
                    || invResReq.Restrict_Reason__c != oldMap.get(invResReq.Id).Restrict_Reason__c)
                || invResReq.Approval_User__c == null){
                reqList.add(invResReq);
            }
        }
        if(reqList.size() > 0){
            updateApprovalMatrix(reqList);
        }   
    }
    
    public static void notifySalesAmdinsReqApproval(List<Inventory_Restriction_Request__c> reqList){
        String queue = 'Sales_Admin_Queue';
        List<String> toAddresses = new List<String>();
        for(User salesAdminUser: [SELECT Email FROM User WHERE Id IN (
                                        SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName =: queue)]){
            toAddresses.add(salesAdminUser.Email);
        }
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        for(Inventory_Restriction_Request__c invResReq: reqList){
            Messaging.SingleEmailMessage mail = new  Messaging.SingleEmailMessage();
            String body = 'Hi,<br/><br/>';
            
            body += '<br/>Inventory Restriction Request #' + invResReq.Name + ' has been Approved.';
            
            
            body += '<br/><br/> Link to the Request: ' + System.URL.getSalesforceBaseUrl().toExternalForm() 
                        + '/apex/InventoryRestrictionRequest?id=' + invResReq.Id;
            String emailSubject = 'Inventory Restriction Request (' + invResReq.Name + ') Approved';
            
            mail.setToAddresses(toAddresses);
            mail.setSenderDisplayName('Damac Property ');
            mail.setReplyTo('noreply@Damacgroup.com');
            mail.setSubject(emailSubject);
            mail.setSaveAsActivity(false);
            System.debug('...body...' + body);
            mail.setHtmlBody(body);
            mails.add(mail);  
            system.debug('mail: ' + mail);
        }
        Messaging.sendEmail(mails);
    }

    public static void notifyInventoryRestriction(List<Inventory_Restriction_Request__c> reqList){
        String queue = 'MIS_Team';
        List<String> toAddresses = new List<String>();
        List<String> toAddressesReq = new List<String>();
        for(User salesAdminUser: [SELECT Email FROM User WHERE Id IN (
                                        SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName =: queue)]){
            toAddresses.add(salesAdminUser.Email);
        }
        Map<Id, String> toEmailUserMap  = new Map<Id, String>();
        Map <Id, List<Inventory_Restriction_Unit__c>> reqUnitsMap = new  Map <Id, List<Inventory_Restriction_Unit__c>>();
        for(Inventory_Restriction_Request__c invResReq: reqList){
            reqUnitsMap.put(invResReq.Id, null);
            if(invResReq.Functional_User__c != null){
                toEmailUserMap.put(invResReq.Functional_User__c, null);
            }
            if(invResReq.Approval_User__c != null){
                toEmailUserMap.put(invResReq.Approval_User__c, null);
            }
            if(invResReq.Approval_User_2__c != null){
                toEmailUserMap.put(invResReq.Approval_User_2__c, null);
            }
            if(invResReq.Approval_User_3__c != null){
                toEmailUserMap.put(invResReq.Approval_User_3__c, null);
            }
           
        }
        for(User usr: [SELECT Id, Email FROM User WHERE Id IN: toEmailUserMap.keyset()]){
            toEmailUserMap.put(usr.Id, usr.Email);
        }
        for(Inventory_Restriction_Unit__c unit: [SELECT Id, Inventory__c, Inventory__r.Name,
                                                        Inventory__r.Unit_Location__r.Name,  Inventory_Restriction_Request__c,
                                                        Inventory__r.Marketing_Name__c, Inventory__r.Property_Name__c
                                                 FROM Inventory_Restriction_Unit__c
                                                 WHERE Inventory_Restriction_Request__c IN: reqUnitsMap.keyset()]){
        
             List<Inventory_Restriction_Unit__c> unitList = new List<Inventory_Restriction_Unit__c>();
             if(reqUnitsMap.containsKey(unit.Inventory_Restriction_Request__c) && reqUnitsMap.get(unit.Inventory_Restriction_Request__c) != null){
                 unitList = reqUnitsMap.get(unit.Inventory_Restriction_Request__c);
             }
             unitList.add(unit);
             reqUnitsMap.put(unit.Inventory_Restriction_Request__c, unitList);
        }
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        for(Inventory_Restriction_Request__c invResReq: reqList){
            toAddressesReq = toAddresses;
            if(invResReq.Functional_User__c != null && toEmailUserMap.containsKey(invResReq.Functional_User__c)
                    && toEmailUserMap.get(invResReq.Functional_User__c) != null){
                toAddressesReq.add(toEmailUserMap.get(invResReq.Functional_User__c));
            }
            if(invResReq.Approval_User__c != null && toEmailUserMap.containsKey(invResReq.Approval_User__c)
                    && toEmailUserMap.get(invResReq.Approval_User__c) != null){
                toAddressesReq.add(toEmailUserMap.get(invResReq.Approval_User__c));
            }
            if(invResReq.Approval_User_2__c != null && toEmailUserMap.containsKey(invResReq.Approval_User_2__c)
                    && toEmailUserMap.get(invResReq.Approval_User_2__c) != null){
                toAddressesReq.add(toEmailUserMap.get(invResReq.Approval_User_2__c));
            }
            if(invResReq.Approval_User_3__c != null && toEmailUserMap.containsKey(invResReq.Approval_User_3__c)
                    && toEmailUserMap.get(invResReq.Approval_User_3__c) != null){
                toAddressesReq.add(toEmailUserMap.get(invResReq.Approval_User_3__c));
            }
            Messaging.SingleEmailMessage mail = new  Messaging.SingleEmailMessage();
            String body = 'Hi,<br/><br/>';
            
            body += '<br/>Inventory Restriction Request #' + invResReq.Name + ' Processing has been completed.';
            
            body += '<br/><br/> The Following Inventories have been Restricted:<br/>';
            for(Inventory_Restriction_Unit__c unit: reqUnitsMap.get(invResReq.Id)){
                body += unit.Inventory__r.Unit_Location__r.Name + ' ( '
                        + unit.Inventory__r.Marketing_Name__c + ' - ' + unit.Inventory__r.Property_Name__c + ' ).<br/>';
                   
            }
            body += '<br/><br/> Link to the Request: ' + System.URL.getSalesforceBaseUrl().toExternalForm() 
                        + '/apex/InventoryRestrictionRequest?id=' + invResReq.Id;
            String emailSubject = 'Inventory Restriction Request (' + invResReq.Name + ') Processed';
            
            mail.setToAddresses(toAddressesReq);
            mail.setSenderDisplayName('Damac Property ');
            mail.setReplyTo('noreply@Damacgroup.com');
            mail.setSubject(emailSubject);
            mail.setSaveAsActivity(false);
            System.debug('...body...' + body);
            mail.setHtmlBody(body);
            mails.add(mail);  
            system.debug('mail: ' + mail);
        }
        Messaging.sendEmail(mails);
    }    

    public static void updateApprovalMatrix(List<Inventory_Restriction_Request__c> reqList){
        List<Id> userIdList = new List<Id>();
        List<Inventory_Restriction_Functional_Matrix__c> matrixList = new List<Inventory_Restriction_Functional_Matrix__c>();
        List<String> restrictReasonList = new List<String>();
        List<String> restrictFunctionList = new List<String>();
        for(Inventory_Restriction_Request__c req: reqList){
            if(req.Functional_User__c != null){
                userIdList.add(req.Functional_User__c);
            }
            if(req.Restrict_Reason__c != null){
                restrictReasonList.add(req.Restrict_Reason__c);
            }
            if(req.Function__c != null){
                restrictFunctionList.add(req.Function__c);
            }
            req.Inventory_Restriction_Functional_Matrix__c = null;
            req.Approval_User__c = null;
            req.Approval_User_2__c = null;
            req.Approval_User_3__c = null;
        }
        for(Inventory_Restriction_Functional_Matrix__c apprMatrix: 
                    [SELECT Id, Defined_Approval_User__c, Defined_Approval_User_2__c, Defined_Approval_User_3__c,
                            Function__c, Functional_User__c , Restrict_Reason__c,
                            Functional_User_2__c, Functional_User_3__c
                     FROM Inventory_Restriction_Functional_Matrix__c
                     WHERE (Functional_User__c IN: userIdList 
                                 OR Functional_User_2__c IN: userIdList 
                                 OR Functional_User_3__c IN: userIdList)
                             AND Restrict_Reason__c IN: restrictReasonList]){
            matrixList.add(apprMatrix);
        }
        system.debug('matrixList: ' + matrixList);
        for(Inventory_Restriction_Request__c req: reqList){
            for(Inventory_Restriction_Functional_Matrix__c apprMatrix: matrixList){
                if(req.Functional_User__c != null
                        && (apprMatrix.Functional_User__c != null && req.Functional_User__c == apprMatrix.Functional_User__c
                            || (apprMatrix.Functional_User_2__c != null && req.Functional_User__c == apprMatrix.Functional_User_2__c)
                            || (apprMatrix.Functional_User_3__c != null && req.Functional_User__c == apprMatrix.Functional_User_3__c))
                        && apprMatrix.Function__c.contains(req.Function__c)
                        && req.Restrict_Reason__c == apprMatrix.Restrict_Reason__c
                        ){
                
                    req.Inventory_Restriction_Functional_Matrix__c = apprMatrix.Id;
                    req.Approval_User__c = apprMatrix.Defined_Approval_User__c;
                    req.Approval_User_2__c = apprMatrix.Defined_Approval_User_2__c;
                    req.Approval_User_3__c = apprMatrix.Defined_Approval_User_3__c;
                }
            }
        }
    }
}