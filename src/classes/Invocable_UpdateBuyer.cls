/**************************************************************************************************
* Name               : Invocable_UpdateBuyer
* Test Class         : Invocable_UpdateBuyer_Test
* Description        : This is the custom code class for sending updating buyer details to IPMS         
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR          DATE            Comments
* 1.0                         16/06/2019      Created
* 1.1         QBurst          08/04/2020      SOAP to REST Changes
* 1.2         QBurst          17/04/2020      Added Buyer Type check
**************************************************************************************************/
public class Invocable_UpdateBuyer {
    @InvocableMethod
    public static void updateBuyer(List<Id> stepId) {
        for (New_Step__c step :[SELECT Id, Service_Request__c FROM New_Step__c WHERE Id IN :stepId]) {
            evaluateCustomCode(step);
        }
    }

    public static String evaluateCustomCode(New_Step__c step) {
        String retStr = 'Success';
        List<Id> bookingIds = new List<Id> ();
        try {
            for (Booking__c bookings :[SELECT Id FROM Booking__c WHERE Deal_SR__c = :step.Service_Request__c]) {
                bookingIds.add(bookings.id);
            }
            if (bookingIds.size() > 0) {
                Boolean primary = false;
                Boolean jointBuyer = false;
                List<Buyer__c> buyerlist = [SELECT Id, Primary_Buyer__c, Status__c, Change_Count__c,buyer_type__c 
                                            FROM Buyer__c WHERE Is_3rd_Party__c = false AND Booking__c IN : bookingIds];
                for (Buyer__c buyer : buyerlist) {
                    if (buyer.Primary_Buyer__c) {
                        if (buyer.Change_Count__c > 0) {
                            primary = true;
                        }
                    } else if (buyer.Status__c == 'New' || buyer.Status__c == 'Updated' || buyer.Status__c == 'Removed') {
                        jointBuyer = true;
                    }
                    // 1.2 starts
                    Boolean isPerson = true;
                    if(buyer.buyer_type__c == 'Corporate'){
                        isPerson = false;
                    }
                    // 1.2 ends
                    if (primary) {
                       // system.enqueueJob(new AsyncReceiptWebservice(BookingIds, 'Party Update'));
                        DAMAC_IPMS_PARTY_CREATION.CreateParty('UPDATE_CUSTOMER', isPerson, true, buyer.id, true); 
                    }
                    if (jointBuyer) {
                        //system.enqueueJob(new AsyncReceiptWebservice(BookingIds, 'Joint Buyer'));
                        DAMAC_IPMS_PARTY_CREATION.CreateParty('UPDATE_CUSTOMER', isPerson, false, buyer.id, false); 
                    }
                }  
            }
        } catch(Exception e) {
            retStr = 'Error :' + e.getMessage() + '';
            Log__c objLog = new Log__c();
            objLog.Description__c = 'Ids==' + BookingIds + '-Line No===>' + e.getLineNumber() + '---Message==>' + e.getMessage();
            objLog.Type__c = 'Error for Update Buyer custom code';
            insert objLog;
        }
        return retStr;
    }
}