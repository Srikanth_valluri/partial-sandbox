public without sharing class HDApp_cancelAppointmentDetailsHandler {
    
    public static List<Calling_List__c> cancelAppointment(String callingListId){
        
        System.debug('callingListId--->'+ callingListId);
        List<Calling_List__c> callingList = new List<Calling_List__c>();
        List<Calling_List__c> callingListToUpdate = new List<Calling_List__c>();
        
        if(String.isNotBlank(callingListId)){
            callingList = [SELECT id
                                , name
                                , Appointment_Status__c 
                          FROM Calling_List__c  
                          WHERE Id =: callingListId 
                          AND Appointment_Status__c != 'Cancelled'
                         ];
            
            System.debug('calling list'+callingList);
            
            if(callingList !=null && callingList.size()>0){
                
                //Update status on Calling List record
                System.debug('callingList'+callingList);
                for(Calling_List__c callingListobj: callingList){
                    callingListobj.Appointment_Status__c = 'Cancelled';
                    callingListToUpdate.add(callingListobj);
                }
                
                update callingListToUpdate;
                System.debug('Updated callingList----->'+callingListToUpdate);
                
                //Update status on Event record
                List<Event> eventList = new List<Event>();
                List<Event> eventListToUpdate = new List<Event>();
                eventList = [Select Id
                            , status__c 
                            FROM Event 
                            WHERE whatId =: callingListToUpdate[0].Id];
                if(eventList!=null && eventList.size()>0){
                    System.debug('---objEvent inside---');
                    for(Event eventObj: eventList){
                        eventObj.Status__c = 'Cancelled'; 
                        eventListToUpdate.add(eventObj);  
                    }
                    
                    update eventListToUpdate;
                    System.debug('Updated eventList----->'+eventListToUpdate);
                }
                return callingListToUpdate;
            }
        }
        return callingListToUpdate;
    }
    
}