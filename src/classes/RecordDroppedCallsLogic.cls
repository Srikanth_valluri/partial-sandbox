/**
 * Logic class for Record Dropped Calls WebService
 */
public class RecordDroppedCallsLogic {

    /**
     * Method to process request and send the response
     */
    public static void processRecordDroppedCallsRequest() {
        RecordDroppedCallsResponse response = new RecordDroppedCallsResponse();

        // Get request body
        String requestBody = RestContext.request.requestBody.toString();
        System.debug('=========== requestBody : ' + requestBody);

        try {
            // Parse request body
            RecordDroppedCallsRequestBody request =
                (RecordDroppedCallsRequestBody) JSON.deserializeStrict(
                    requestBody,
                    RecordDroppedCallsRequestBody.Class
                );
            System.debug('===== request : ' + request);

            User cre = getCreForExtension(request.creExtension);
/*
            if (cre == null) {
                response.errorMessage = 'No CRE User found for the given extension';
                RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
                Restcontext.response.statusCode = 400;
                return;
            }
*/
            Account account = getAccountForCallingNumber(request.callingNumber);
/*
            if (account == null) {
                response.errorMessage = 'No Account found for the given calling number';
                RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
                Restcontext.response.statusCode = 400;
                return;
            }
*/
            DateTime callDate = CtiTntegrationUtility.getDateTimeForUserTimeZone(request.dateAndTime);
            if (callDate == null) {
                response.errorMessage = 'Please enter valid date format (MM/DD/YYYY HH:MM:SS)';
                RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
                Restcontext.response.statusCode = 400;
                return;
            } else {
                 createDropCallRequest(account, cre, request,callDate);
            }
        } catch (Exception ex) {
            response.errorMessage = ex.getMessage();
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
            Restcontext.response.statusCode = 400;
            System.debug('============= response : ' + JSON.serialize(response));
            return;
        }

        response.status = 'Call Drop Logged';
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
        Restcontext.response.statusCode = 200;
    }

    /**
     * Create call back request
     */
    public static Calling_List__c createDropCallRequest(
            Account account,
            User cre,
            RecordDroppedCallsRequestBody request,
            DateTime callDate
    ) {
        Calling_List__c dropCallInst = new Calling_List__c();
        dropCallInst.RecordTypeId =
            Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Dropped Calls - Call Back').getRecordTypeId();
        dropCallInst.Calling_List_Type__c = 'Dropped calls - Call Back';
        dropCallInst.Call_Date__c = callDate;
        //dropCallList.Call_Date__c = Date.parse(request.dateAndTime);
        dropCallInst.Called_Number__c = request.calledNumber;
        dropCallInst.Calling_Number__c = request.callingNumber;
        dropCallInst.Country_of_calling_number__c =
            CtiTntegrationUtility.getCallingCountryFromCallingNumber(request.callingNumber);
        if (String.isNotEmpty(request.creExtension)) {
            dropCallInst.CRE_Extension__c = Integer.ValueOf(request.creExtension);
        }

        // Set account
        if (account != null) {
            dropCallInst.Account__c = account.Id;
            dropCallInst.Customer_Name__c = account.Name;
            dropCallInst.Customer_Category__c = account.Customer_Type__c;
            dropCallInst.Party_ID__c = account.Party_ID__c;
            if (account.Record_Type_Name__c == 'Person Account') {
                dropCallInst.Mobile_Phone__c = account.Mobile_Phone_Encrypt__pc;
                dropCallInst.Mobile_Phone_2__c = account.Mobile_Phone_Encrypt_2__pc;
                dropCallInst.Mobile_Phone_3__c = account.Mobile_Phone_Encrypt_3__pc;
                dropCallInst.Mobile_Phone_4__c = account.Mobile_Phone_Encrypt_4__pc;
                dropCallInst.Mobile_Phone_5__c = account.Mobile_Phone_Encrypt_5__pc;
            }
        }

/*
        // Set Owner
        if (cre != null) {
            dropCallInst.OwnerId = cre.Id;
        }
        if (account != null && account.Primary_CRE__c != null) {
            dropCallInst.OwnerId = account.Primary_CRE__c;
        }
*/

        dropCallInst = setCallingListOwner(
            dropCallInst,
            account,
            request.creExtension,
            cre
        );

        insert dropCallInst;
        return dropCallInst;
    }

    /**
     * Method to set owner of calling list record
     */
    public static Calling_List__c setCallingListOwner(
            Calling_List__c callingListInst,
            Account account,
            String extension,
            User cre
    ) {

        // Check if cre present
        if (cre != null) {
            callingListInst.OwnerId = cre.Id;

        // Check if call was for collection
        } else if (Collection_Payment_Pool__c.getInstance(extension) != null) {
            System.debug('>>>>>>>>>>> Collections Extension : ' + extension);

            // Get cre  assigned with lowest number of 'Dropped Calls - Call Back'
            Id collectionCreId = getUserIdWithLowestWorkLoad(
                getCreWorkloadMap(
                    getQueueUsers('Collection_Queue'),
                    'Dropped Calls - Call Back'
                )
            );

            // If failed to find cre assign the record to collection queue
            if (collectionCreId == null) {
                Group collectionQueue =
                    [SELECT Id, Name FROM Group WHERE Type = 'Queue' AND DeveloperName = 'Collection_Queue'];
                if (collectionQueue != null) {
                    callingListInst.OwnerId = collectionQueue.Id;
                }
            } else {
                callingListInst.OwnerId = collectionCreId;
            }

        // Check if registered customer and if Primary/Secondary/Tertiary Cre avaliable
        } else if (account != null) {
            if (String.isNotBlank(account.Primary_CRE__c)) {
                callingListInst.OwnerId = account.Primary_CRE__c;
            } else if (String.isNotBlank(account.Secondary_CRE__c)) {
                callingListInst.OwnerId = account.Secondary_CRE__c;
            } else if (String.isNotBlank(account.Tertiary_CRE__c)) {
                callingListInst.OwnerId = account.Tertiary_CRE__c;
            }
        }

        // If no owner found from above logic set owner as Contact Centre Queue
        if (String.isBlank(callingListInst.OwnerId)) {
            Group contactCentreQueue =
                [SELECT Id, Name FROM Group WHERE Type = 'Queue' AND DeveloperName = 'Contact_Center_Queue'];
            callingListInst.OwnerId = contactCentreQueue.Id;
        }
        System.debug('>>>>>>>>>> callingListInst.OwnerId : ' + callingListInst.OwnerId);

        return callingListInst;
    }

    /**
     * Method get account for the calling number
     */
    public static Account getAccountForCallingNumber(String callingNumber) {
/*
        List<Account> accounts =
            [
                SELECT
                    Id,
                    Phone,
                    Primary_CRE__c
                FROM
                    Account
                WHERE
                    Mobile_Phone_Encrypt__pc =: callingNumber
                    OR Mobile_Phone_Encrypt_2__pc =: callingNumber
                    OR Mobile_Phone_Encrypt_3__pc =: callingNumber
                    OR Mobile_Phone_Encrypt_4__pc =: callingNumber
                    OR Mobile_Phone_Encrypt_5__pc =: callingNumber
            ];
*/
        List<Account> accounts = CtiTntegrationUtility.getRegisteredAccount(callingNumber);
        if (accounts.size() > 0) {
            return accounts[0];
        } else {
            return null;
        }
    }

    /**
     * Method to CRE User for the extension
     */
    public static User getCreForExtension(String extension) {
        List<User> creUsers =
            [
                SELECT
                    Id,
                    Extension
                FROM
                    User
                WHERE
                    Extension =: extension
            ];
        if (creUsers.size() > 0) {
            return creUsers[0];
        } else {
            return null;
        }
    }

    /**
     * Method to fetch user ids of a queue
     */
    public static Set<Id> getQueueUsers(String queueName) {
        Set<Id> queueUserIds = new Set<Id>();
        String userType = Schema.SObjectType.User.getKeyPrefix();

        List<GroupMember> groupMembers =
            [
                SELECT
                    UserOrGroupId,
                    Group.Type,
                    GroupId
                FROM
                    GroupMember
                WHERE
                    Group.Type = 'Queue' AND
                    Group.DeveloperName =: queueName
            ];

        System.debug('======== groupMembers : ' + groupMembers);
        for (GroupMember groupMember : groupMembers) {
            if (((String) groupMember.UserOrGroupId).startsWith(userType)) {
                queueUserIds.add(groupMember.UserOrGroupId);
            }
        }
        System.debug('======== queueUserIds : ' + queueUserIds);
        return queueUserIds;
    }

    /**
     * Method to get cre workload for unclosed calling list records
     */
    public static Map<Id, Integer> getCreWorkloadMap(Set<Id> userIds, String callingListRecordType) {
        Map<Id, Integer> userIdToWorkLoad = new Map<Id, Integer>();
        List<Calling_List__c> droppedCallCallingLists =
            [
                SELECT
                    Id,
                    Name,
                    OwnerId
                FROM
                    Calling_List__c
                WHERE
                    RecordType.Name =: callingListRecordType AND
                    Calling_List_Status__c  != 'Closed' AND
                    OwnerId IN :userIds
            ];
        System.debug('======== droppedCallCallingLists : ' + droppedCallCallingLists);
        System.debug('======== droppedCallCallingLists Size : ' + droppedCallCallingLists.size());

        for (Id userId : userIds) {
            userIdToWorkLoad.put(userId, 0);
        }
        for (Calling_List__c callingList : droppedCallCallingLists) {
            if (userIdToWorkLoad.containsKey(callingList.OwnerId)) {
                Integer count = userIdToWorkLoad.get(callingList.OwnerId);
                userIdToWorkLoad.put(callingList.OwnerId, ++count);
            }
        }

        System.debug('======= userIdToWorkLoad : ' + userIdToWorkLoad);
        return userIdToWorkLoad;
    }

    /**
     * Method to get a user id of a cre with lowest workload
     */
    public static Id getUserIdWithLowestWorkLoad(Map<Id, Integer> userIdToWorkLoad) {
        Id creIdWithLowestWorkLoad = null;
        for (Id creId : userIdToWorkLoad.keySet()) {
            if (creIdWithLowestWorkLoad == null) {
                creIdWithLowestWorkLoad = creId;
            } else if (userIdToWorkLoad.get(creIdWithLowestWorkLoad) > userIdToWorkLoad.get(creId)) {
                creIdWithLowestWorkLoad = creId;
            }
        }
        System.debug('======= creIdWithLowestWorkLoad : ' + creIdWithLowestWorkLoad);
        return creIdWithLowestWorkLoad;
    }

    /**
    * Method to get datetime for passed string in user timezone.
    * @param: dateTimeString: Date time value in string format
    * @return: DateTime
    **/
    /*public static DateTime getDateTimeForUserTimeZone(String dateTimeString) {

        DateTime newDateTime;
        if(String.isBlank(dateTimeString)) {
            return newDateTime;
        }
        list<String> spiltStrList = dateTimeString.split(' ');
        if(spiltStrList.size() >= 2) {
            list<String> dateComponentsList = spiltStrList[0].split('/');
            list<String> timeComponentsList = spiltStrList[1].split(':');

            System.debug('=========== dateComponentsList : ' + dateComponentsList);
            System.debug('=========== timeComponentsList : ' + timeComponentsList);
            if(dateComponentsList.size() == 3 && timeComponentsList.size() == 3) {
                newDateTime = DateTime.newInstance(Integer.valueOf(dateComponentsList[2]),
                                                   Integer.valueOf(dateComponentsList[0]),
                                                   Integer.valueOf(dateComponentsList[1]),
                                                   Integer.valueOf(timeComponentsList[0]),
                                                   Integer.valueOf(timeComponentsList[1]),
                                                   Integer.valueOf(timeComponentsList[2]));
            }

            System.debug('=========== newDateTime : ' + newDateTime);
            return newDateTime;
        }
        System.debug('=========== newDateTime : ' + newDateTime);
        return newDateTime;
    }*/

    /**
     * Wrapper class for RecordDroppedCallsWebService request body
     */
    public class RecordDroppedCallsRequestBody {
        public String dateAndTime {get; set;}
        public String calledNumber {get; set;}
        public String callingNumber {get; set;}
        public String creExtension {get; set;}

        public RecordDroppedCallsRequestBody() {
            this.dateAndTime = '';
            this.calledNumber = '';
            this.callingNumber = '';
            this.creExtension = '';
        }
    }

    /**
     * Wrapper class for RecordDroppedCallsWebservice response body
     */
    public class RecordDroppedCallsResponse {
        public String status;
        public String errorCode;
        public String errorMessage;

        public RecordDroppedCallsResponse() {
            this.status = '';
            this.errorCode = '';
            this.errorMessage = '';
        }
    }
}