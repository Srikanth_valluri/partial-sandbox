@istest
public class DAMAC_WA_REST_SERVICES_TEST{



    static testmethod void DAMAC_WA_REST_SERVICES_M(){
        
        Whats_App_Message__c parent = new Whats_App_Message__c();
        parent.Unique_Id__c  = '001234';
        parent.Last_Message_Sent__c = 'TEST';
        parent.Outbound_Success__c  = true;
        parent.Unique_Id__c  = 'abc'+string.valueof(system.now());
        parent.Number__c = '001234';
        insert parent;
        
        Whats_App_Deliverability__c child = new Whats_App_Deliverability__c ();
        child.Whats_App_Message__c = parent.id;
        child.messagebodytext__c = 'Test';
        child.messagedir__c  ='o';
        child.messagecuid__c = '123';
        insert child;
        
        test.starttest();
        // executed
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        // pass the req and resp objects to the method     
        req.requestURI = 'https://ap1.salesforce.com/services/apexrest/whatsapphook';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        req.params.put('event','ack'); 
        req.params.put('muid',''); 
        req.params.put('cuid','123'); 
        req.params.put('ack',''); 
        req.params.put('token',''); 
        DAMAC_WA_REST_SERVICES.doPost();
        test.stoptest();
    
    }
    
    

    static testmethod void DAMAC_WA_REST_SERVICES_M1(){
        
        Whats_App_Message__c parent = new Whats_App_Message__c();
        parent.Unique_Id__c  = '001234';
        parent.Last_Message_Sent__c = 'TEST';
        parent.Outbound_Success__c  = true;
        //parent.Unique_Id__c  = 'abc'+string.valueof(system.now());
        parent.Number__c = '001234';
        insert parent;
        
        Whats_App_Deliverability__c child = new Whats_App_Deliverability__c ();
        child.Whats_App_Message__c = parent.id;
        child.messagebodytext__c = 'Test';
        child.messagedir__c  ='o';
        child.messagecuid__c = '123';
        insert child;
        
        test.starttest();
        // executed
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        // pass the req and resp objects to the method     
        req.requestURI = 'https://ap1.salesforce.com/services/apexrest/whatsapphook';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        req.params.put('event','message'); 
        req.params.put('contact[uid]','001234'); 
        req.params.put('message[dir]','i'); 
        req.params.put('message[uid]','123'); 
        req.params.put('message[dtm]',''); 
        req.params.put('contact[name]',''); 
        req.params.put('token','');
        req.params.put('uid','');
        req.params.put('message[type]','');
        req.params.put('message[cuid]','abc');
        
        req.params.put('message[ack]','');
        req.params.put('contact[type]','');
        DAMAC_WA_REST_SERVICES.doPost();
        test.stoptest();
    }


}