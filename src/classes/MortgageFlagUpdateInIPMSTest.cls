/***********************************************************************
* Description - Test class developed for MortgageFlagUpdateInIPMS
*
* Version            Date            Author        Description
* 1.0                01/03/18        Arjun         Initial Draft
**********************************************************************/
@isTest
private class MortgageFlagUpdateInIPMSTest
{
  @isTest static void testupdateFlagInIPMS() {
    Account accObj = TestDataFactory_CRM.createPersonAccount();
    insert accObj;

        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( accObj.Id, dealSR.Id, 1 );
        insert lstBookings;

        List<Booking_Unit__c> bookingUnitsObj = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        insert bookingUnitsObj;
        
        List<Case> lstCase = createCase('Mortgage',1,accObj.Id );
            insert lstCase ;
        
        
        Test.startTest();
    
        Test.setMock( WebServiceMock.class, new MortgageWebServiceCalloutMock());
    
        MortgageFlagUpdateInIPMS.updateMortgageFlag(lstCase);
        MortgageFlagUpdateInIPMS.Data objR = new MortgageFlagUpdateInIPMS.Data();
        objR.message = 'test';
        objR.status  = 'Success';     
       Test.stopTest();
            
  }

  public static List<Case> createCase( String recordTypeDevName, Integer counter ,Id accId  ) {

        Id recodTypeIdCase = [
            SELECT Id
            FROM RecordType
            WHERE SObjectType=:'Case'
            AND DeveloperName=:recordTypeDevName
            AND IsActive = TRUE LIMIT 1
        ].Id;

        List<Case> lstCase = new List<Case>();
        for( Integer i=0; i<counter; i++ ) {
            lstCase.add(new Case( AccountId = accId, Credit_Note_Amount__c=5000, RecordTypeId = recodTypeIdCase ) );
        }
        return lstCase;
    }
  
  @isTest static void recoveryFlagUpdatewithException() {
         Account accObj = TestDataFactory_CRM.createPersonAccount();
    insert accObj;

        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( accObj.Id, dealSR.Id, 1 );
        insert lstBookings;

        List<Booking_Unit__c> bookingUnitsObj = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        insert bookingUnitsObj;
        
        List<Case> lstCase = createCase('Mortgage',1,accObj.Id );
            insert lstCase ;
            MortgageFlagUpdateInIPMS.updateMortgageFlag(lstCase);
  } 
 
}