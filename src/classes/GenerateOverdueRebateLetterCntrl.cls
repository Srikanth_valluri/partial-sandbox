/**********************************************************************************************
Version     Date            Author              Description
-----------------------------------------------------------------------------------------------
1.1         2/1/2020        Aishwarya Todkar    Added check for Basswood(BWD) and Albizia(ABZ) 
                                                units as requested by Aditya
***********************************************************************************************/

public with sharing class GenerateOverdueRebateLetterCntrl {
    public String caseId;
    public Case objC;
    public GenerateOverdueRebateLetterCntrl( ApexPages.StandardController stdCon ) {
        caseId = stdCon.getId();
        objC = [ Select Id
                      , OwnerId
                      , Booking_Unit__c 
                      , Booking_Unit__r.Registration_ID__c
                      , Registration_ID__c
                      , Booking_Unit__r.Unit_Name__c
                      ,(Select 
                            Id 
                        from 
                            Tasks 
                        Where 
                            Subject = 'Confirm payment received by DAMAC as per offer letter' Limit 1)
                      , RecordType.DeveloperName
                      , (SELECT 
                            Id
                            , Name  
                        FROM 
                            SR_Attachments__r 
                        WHERE Name = 'Upload Signed Overdue Rebate Letter' LIMIT 1)
                   FROM Case 
                  WHERE Id =: caseId ];
    }
    
    public void callDrawloop(){
        executeBatch();
    }
    
    public pageReference returnToCase(){
        PageReference Pg = new PageReference('/'+caseId);
        return pg;
    }
    
    public void executeBatch(){
        if( objC.Recordtype.DeveloperName == 'Overdue_Rebate_Discount' ) {
            //if(String.isNotBlank(objC.Booking_Unit__r.Registration_ID__c)){
                List<String> lstClosedStatus = new List<String>{'Completed', 'Closed'};
                List<String> lstSubject = new List<String>{'Generate Offer Letter and share with Customer'};
                List<Task> lstTask = [SELECT Status, OwnerId, Id FROM Task WHERE Subject In: lstSubject AND Status Not In: lstClosedStatus AND WhatId =: objC.Id];

                if ( !lstTask.isEmpty() ) {
                    String ddpId,deliveryId;

                    if( String.isNotBlank( objC.Booking_Unit__r.Unit_Name__c) 
                    && ( objC.Booking_Unit__r.Unit_Name__c.toLowerCase().startsWith('bsw') 
                    || objC.Booking_Unit__r.Unit_Name__c.toLowerCase().startsWith('abz') ) ) {
                        Riyadh_Rotana_Drawloop_Doc_Mapping__c ddpCsObj = Riyadh_Rotana_Drawloop_Doc_Mapping__c.getInstance('Overdue-Basswood and Albizia');    
                        System.debug('ddpCsObj == ' + ddpCsObj);
                        if( ddpCsObj != null ) {
                            deliveryId = ddpCsObj.Delivery_Option_Id__c;
                            ddpId = ddpCsObj.Drawloop_Document_Package_Id__c;
                        }
                        
                    }
                    else {
                        ddpId = System.Label.Overdue_Rebate_Letter_DDP;
                        deliveryId = System.Label.Overdue_Rebate_Letter_Template;
                    }
                    System.debug('ddpId == ' + ddpId);
                    System.debug('deliveryId == ' + deliveryId);
                    if( String.isNotBlank( ddpId ) && String.isNotBlank( deliveryId ) ) {

                        GenerateDrawloopDocumentBatch objInstance =
                            new GenerateDrawloopDocumentBatch(caseId
                                , ddpId
                                , deliveryId );
                        Id batchId;
                        if (!Test.isRunningTest()) {
                            batchId = Database.ExecuteBatch(objInstance);
                        }
                        if(String.valueOf(batchId) != '000000000000000'){
                            
                             SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
                             if( objC.SR_Attachments__r.size() > 0 ) {
                                objCaseAttachment.Id = objC.SR_Attachments__r[0].Id;
                             }
                             objCaseAttachment.Case__c = caseId ;
                             objCaseAttachment.Name = 'Upload Signed Overdue Rebate Letter';
                             objCaseAttachment.Booking_Unit__c = objC.Booking_Unit__c;
                             upsert objCaseAttachment;
                             
                             Task objTask = TaskUtility.getTask((SObject)objC , 'Confirm payment received by DAMAC as per offer letter', 'CRE',
                                'Overdue Amount Rebate', system.today().addDays(1));
                             if( objC.Tasks.size() > 0 ) {
                                objTask.Id = objC.Tasks[0].Id;
                             }
                             upsert objTask;
                             
                            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Your request for Overdue Rebate Letter was successfully submitted. Please check the documents section for the document in a while.');
                            ApexPages.addMessage(myMsg);
                        }else{
                            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Your request for for Overdue Rebate Letter could not be completed. Please try again later.');
                            ApexPages.addMessage(myMsg);
                        }
                    }
                }else {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'You are not authorized , because there is no activity (Generate Offer Letter and share with Customer) to generate Overdue Amount Rebate Letter'));               
                }
            /*}else{
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Regid for the new unit has not been generated yet. Please try again later.');
                ApexPages.addMessage(myMsg);
            }*/
        }else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Overdue Amount Rebate Letter cannot be generated on this Service Request.');
            ApexPages.addMessage(myMsg);
        }
    }
}