/************************************************************************************************
 * @Name              : DAMACServicesFactory
 * @Test Class Name   :
 * @Description       : Base Class to handle services
 * Modification Log
 * VERSION     AUTHOR           DATE            Update Log
 * 1.0         Srikanth Valluri 28/07/2020       Created
 ***********************************************************************************************/
@RestResource(urlMapping = '/DAMACFactory/*')
global with sharing class DAMACServicesFactory{
    @HttpGET
    global static void doGET(){

        RestContextHandler handler = new RestContextHandler(true);
        RestRequest req = handler.restRequest;

        try{
            System.debug('>>>>>requestURI>>>>>>>' + req.requestURI);
            String action = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
            System.debug('>>>>>action>>>>>>>' + action);
            System.debug(req.Headers);
            map<string, string> mpHeaders = req.headers;
            if(mpHeaders.containsKey('serviceId')){
                if(Damac_PhoneEncrypt.decryptPhoneNumber(mpHeaders.get('serviceId')) == 'Hello DAMAC'){
                    String projectCode = req.params.get('projectCode');
                    String buildingCode = req.params.get('buildingCode');

                    //Search Inventory
                    if (action.toLowerCase() == 'searchinventory')
                        handler = DAMACFactoryUtils.handleInventorySearch(projectCode, buildingCode);
                }
                else{
                    handler.response.success = false;
                    handler.response.statusCode = WebServiceResponse.STATUS_CODE_SYSTEM_ERROR;
                    handler.response.message = 'Service key found is not matching.';
                }
            }
            else{
                handler.response.success = false;
                handler.response.statusCode = WebServiceResponse.STATUS_CODE_SYSTEM_ERROR;
                handler.response.message = 'No Service key found.';
            }
            handler.finalize();
        } catch (Exception exc){
            system.debug('>>>>>>>>>>ERROR>>>>>>>>>>>' + exc.getmessage()+'>>>>>>LineNO>>>>>' + exc.getlinenumber());
            handler.response.success = false;
            handler.response.statusCode = WebServiceResponse.STATUS_CODE_SYSTEM_ERROR;
            handler.response.message = exc.getMessage()+'-' + exc.getLineNumber();
            handler.finalize(exc);
        }
    }
}