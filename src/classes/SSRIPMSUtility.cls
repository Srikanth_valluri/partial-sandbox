public class SSRIPMSUtility {

    @invocableMethod
    public static void verifyTaskBeforePushToIPMS(List<Task> lstTask) {
        List<Task> lstTaskToPush = new List<Task>();
        Set<Id> setTasksIds = new Set<Id>();
        if (!lstTask.isEmpty()) {
            for (Task objTask : lstTask) {
                if (objTask.subject.equalsIgnoreCase('Update Fund Transfer Details in IPMS') &&
                    objTask.Assigned_User__c.equalsIgnoreCase('Finance')  &&
                    objTask.Process_Name__c.equalsIgnoreCase('Sales Service Request - Fund Transfer') &&
                    objTask.WhatId != null
                ) {
                    lstTaskToPush.add(objTask);
                    setTasksIds.add(objTask.Id);
                }
            }
            
            if (!lstTaskToPush.isEmpty()) {
                createTaskInIPMS(setTasksIds);
                //pushTaskToIPMS(lstTaskToPush);
            }
        }
    }

    @future(callout = true)
    public static void createTaskInIPMS(Set<Id> setTaskIds) {
        List<Task> lstTask = [SELECT Id, Subject, WhatId,Assigned_User__c, Pushed_to_IPMS__c,Task_Error_Details__c,Status,CreatedDate,ActivityDate,Description FROM Task WHERE ID IN: setTaskIds];
        if (!lstTask.isEmpty()) {
            pushTaskToIPMS(lstTask);
        }
    }

    public static void pushTaskToIPMS(List<Task> lstTask) {
        if (!lstTask.isEmpty()) {
            List<Task> lstTaskToUpdate = new List<Task>();
            Map<Id, Sales_Service_Request__c> mapSSR = new Map<Id, Sales_Service_Request__c>();
            Set<Id> lstSSRIds = new Set<Id>();
            List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();

            for (Task objTask : lstTask) {
                if (objTask.WhatId != null) {
                    lstSSRIds.add(objTask.WhatId);
                }
            }

            if (!lstSSRIds.isEmpty()) {
                mapSSR = getSSRMap(lstSSRIds);
                for (Task objTask : lstTask) {
                    if (objTask.subject.equalsIgnoreCase('Update Fund Transfer Details in IPMS')) {
                        Sales_Service_Request__c objCase = mapSSR.get(objTask.WhatId);
                        List<TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5> lstObjBeans = new List<TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5>();

                        TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objHeaderBean = new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();

                        objHeaderBean = setCommonBeanFields(objHeaderBean);
                        objHeaderBean = setHeaderBean(objHeaderBean, objCase);
                        lstObjBeans.add(objHeaderBean);
                        
                        TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objTaskBean =
                            new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
                        objTaskBean = setCommonBeanFields(objTaskBean);
                        objTaskBean = setTaskBean(objTaskBean, objCase, objTask);
                        objTaskBean.ATTRIBUTE4 = 'FINANCEEXECUTIVE';
                        //objTaskBean.ATTRIBUTE16 = 'Finance';
                        objTaskBean.ATTRIBUTE16 = String.isNotBlank(objCase.Approving_Users__c) ? objCase.Approving_Users__c : '';
                        lstObjBeans.add(objTaskBean);
                        if(objCase.Booking_Units__r != null && !objCase.Booking_Units__r.isEmpty()) {
                            for(SR_Booking_Unit__c objSRBU : objCase.Booking_Units__r) {
                                if(objSRBU.Booking_Unit__r.Registration_Id__c != null) {
                                    //strRegIds += objSRBU.Booking_Unit__r.Registration_Id__c + ',';
                                    TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objUnitBean = new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
                                    objUnitBean = setCommonBeanFields(objUnitBean);
                                    objUnitBean = setUnitBean(objUnitBean, objCase, objSRBU.Booking_Unit__r.Registration_Id__c);
                                    objUnitBean.ATTRIBUTE7 = 'From Unit';
                                    lstObjBeans.add(objUnitBean);
                                }
                            }
                        }
                        if(objCase.Fund_Transfer_Units__r != null && !objCase.Fund_Transfer_Units__r.isEmpty()) {
                            for(Fund_Transfer_Unit__c objFTU : objCase.Fund_Transfer_Units__r) {
                                if(objFTU.New_Unit__r.Registration_Id__c != null) {
                                    TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objUnitBean = new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
                                    objUnitBean = setCommonBeanFields(objUnitBean);
                                    objUnitBean = setUnitBean(objUnitBean, objCase, objFTU.New_Unit__r.Registration_Id__c);
                                    objUnitBean.ATTRIBUTE6 = objFTU.New_Unit__r.Registration_Id__c;
                                    objUnitBean.ATTRIBUTE7 = 'To Unit';
                                    objUnitBean.ATTRIBUTE8 = objFTU.Allocated_Amount__c != null ? String.valueOf(objFTU.Allocated_Amount__c) : '';
                                    //objUnitBean.ATTRIBUTE9 = objFTU.Allocated_Amount__c != null ? String.valueOf(objFTU.Allocated_Amount__c) : '';
                                    lstObjBeans.add(objUnitBean);
                                }
                            }
                            //strRegIds = strRegIds.removeEnd(',');
                        }
                        TaskCreationWSDL.TaskHttpSoap11Endpoint objClass = new TaskCreationWSDL.TaskHttpSoap11Endpoint();
                        objClass.timeout_x = 120000;
                        String strResponse = objClass.SRDataToIPMSMultiple('2-' + String.valueOf(System.currentTimeMillis()),
                            'CREATE_SR', 'SFDC', lstObjBeans);

                        system.debug('strResponse*****'+strResponse);
                        TaskCreationResponse objRes = (TaskCreationResponse)JSON.deserialize(strResponse, TaskCreationResponse.class);
                        system.debug('objRes*****'+objRes);
                        if(objRes != null && String.isNotBlank(objRes.status) && objRes.status.equalsIgnoreCase('S')) {
                            objTask.Pushed_to_IPMS__c = true;
                        }else {
                            objTask.Pushed_to_IPMS__c = false;
                            objTask.Task_Error_Details__c = objRes != null && String.isNotBlank(objRes.message) ?
                                (objRes.message.length() > 255 ? objRes.message.substring(0, 254) : objRes.message) : '';
                            if(objRes != null && String.isNotBlank(objRes.message)) {
                                lstErrorLog.add(new Error_Log__c(Case__c = objCase.Id, Error_Details__c = objRes.message));
                            }
                        }
                        lstTaskToUpdate.add(objTask);
                    }
                }
                if (!lstTaskToUpdate.isEmpty()) {
                    update lstTaskToUpdate;
                }
            }
        }
    }

    public static TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 setHeaderBean(TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objHeaderBean,Sales_Service_Request__c objSSR) {
 
        objHeaderBean.PARAM_ID = '2-' + objSSR.Name;
        system.debug('param id*****'+objSSR.Name);
        objHeaderBean.ATTRIBUTE1 = 'HEADER';
        objHeaderBean.ATTRIBUTE2 = objSSR.RecordType.Name;
        objHeaderBean.ATTRIBUTE3 = objSSR.Status__c;
        system.debug('objSSR.Status*****'+objSSR.Status__c);
        objHeaderBean.ATTRIBUTE4 = objSSR.Owner.Name; //CRM
        objHeaderBean.ATTRIBUTE5 = String.isNotBlank(objSSR.Account__r.Party_ID__c) ? objSSR.Account__r.Party_ID__c : '-1';
        system.debug('Party ID****'+objSSR.Account__r.Party_ID__c);
        objHeaderBean.ATTRIBUTE7 = String.valueOf(objSSR.CreatedDate.format('dd-MMM-yyyy').toUpperCase()); // format this as DD-MON- YYYY
        system.debug('SR creation date*****'+String.valueOf(objSSR.CreatedDate.format('dd-MMM-yyyy').toUpperCase()));
        objHeaderBean.ATTRIBUTE11 = objSSR.Id;

        return objHeaderBean;
    }

    //Method to set common fields on IPMS task object
    public static TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 setCommonBeanFields(TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objBean) {
        objBean.PARAM_ID = '';
        objBean.ATTRIBUTE1 = '';
        objBean.ATTRIBUTE2 = '';
        objBean.ATTRIBUTE3 = '';
        objBean.ATTRIBUTE4 = '';
        objBean.ATTRIBUTE5 = '';
        objBean.ATTRIBUTE6 = '';
        objBean.ATTRIBUTE7 = '';
        objBean.ATTRIBUTE8 = '';
        objBean.ATTRIBUTE9 = '';
        objBean.ATTRIBUTE10 = '';
        objBean.ATTRIBUTE11 = '';
        objBean.ATTRIBUTE12 = '';
        objBean.ATTRIBUTE13 = '';
        objBean.ATTRIBUTE14 = '';
        objBean.ATTRIBUTE15 = '';
        objBean.ATTRIBUTE16 = '';
        objBean.ATTRIBUTE17 = '';
        objBean.ATTRIBUTE18 = '';
        objBean.ATTRIBUTE19 = '';
        objBean.ATTRIBUTE20 = '';
        objBean.ATTRIBUTE21 = '';
        objBean.ATTRIBUTE22 = '';
        objBean.ATTRIBUTE23 = '';
        objBean.ATTRIBUTE24 = '';
        objBean.ATTRIBUTE25 = '';
        objBean.ATTRIBUTE26 = '';
        objBean.ATTRIBUTE27 = '';
        objBean.ATTRIBUTE28 = '';
        objBean.ATTRIBUTE29 = '';
        objBean.ATTRIBUTE30 = '';
        objBean.ATTRIBUTE31 = '';
        objBean.ATTRIBUTE32 = '';
        objBean.ATTRIBUTE33 = '';
        objBean.ATTRIBUTE34 = '';
        objBean.ATTRIBUTE35 = '';
        objBean.ATTRIBUTE36 = '';
        objBean.ATTRIBUTE37 = '';
        objBean.ATTRIBUTE38 = '';
        objBean.ATTRIBUTE39 = '';
        //objHeaderBean.ATTRIBUTE40 = '';
        objBean.ATTRIBUTE41 = '';
        objBean.ATTRIBUTE42 = '';
        objBean.ATTRIBUTE43 = '';
        objBean.ATTRIBUTE44 = '';
        objBean.ATTRIBUTE45 = '';
        objBean.ATTRIBUTE46 = '';
        objBean.ATTRIBUTE47 = '';
        objBean.ATTRIBUTE48 = '';
        objBean.ATTRIBUTE49 = '';
        objBean.ATTRIBUTE50 = '';

        return objBean;
    }

    public static TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 setTaskBean(TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objTaskBean, Sales_Service_Request__c objSSR, Task objTask) {

        objTaskBean.PARAM_ID = '2-' + objSSR.Name;
        objTaskBean.ATTRIBUTE1 = 'TASK';
        objTaskBean.ATTRIBUTE2 = objTask.Subject;
        system.debug('subject*****'+objTask.Subject);
        objTaskBean.ATTRIBUTE3 = objTask.Status;
        system.debug('subject*****'+objTask.Status);
        //objTaskBean.ATTRIBUTE4 = 'FINANCEMANAGER';
        objTaskBean.ATTRIBUTE7 = String.valueOf(objTask.CreatedDate.format('dd-MMM-yyyy').toUpperCase()); // format this as DD-MON- YYYY
        system.debug('created date*****'+String.valueOf(objTask.CreatedDate.format('dd-MMM-yyyy').toUpperCase()));
        objTaskBean.ATTRIBUTE8 = objTask.Id;
        system.debug('task id*****'+objTask.Id);
        Datetime dt = objTask.ActivityDate;
        objTaskBean.ATTRIBUTE9 = dt != null ? String.valueOf(dt.format('dd-MMM-yyyy').toUpperCase()) : '';
        objTaskBean.ATTRIBUTE49 = String.isNotBlank(objTask.Description) ? objTask.Description : '';
        //system.debug('due date*****'+String.valueOf(dt.format('dd-MMM-yyyy').toUpperCase()));

        return objTaskBean;
    }

    public static TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 setUnitBean(TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objUnitBean,
    Sales_Service_Request__c objSSR, String strRegId) {

        objUnitBean.PARAM_ID = '2-' + objSSR.Name;
        objUnitBean.ATTRIBUTE1 = 'UNITS';
        objUnitBean.ATTRIBUTE2 = objSSR.RecordType.Name;
        objUnitBean.ATTRIBUTE6 = strRegId;
        system.debug('reg Id*****'+strRegId);
        return objUnitBean;
    }

    //Method to get case details
    public static Map<Id, Sales_Service_Request__c> getSSRMap(Set<Id> setSSRId) {
        return new Map<Id, Sales_Service_Request__c>([Select Id, Name, Approving_Users__c, RecordType.DeveloperName, RecordType.Name, POA_Name__c, POA_Expiry_Date__c,Purpose_of_POA__c, POA_Issued_By__c, Account__r.Party_ID__c, CreatedDate,Status__c,OwnerId,Owner.Name,(Select Id, Booking_Unit__c, Booking_Unit__r.Registration_Id__c,Booking_Unit__r.Unit_Selling_Price_AED__c From Booking_Units__r Where Booking_Unit__c != null),(Select Id, New_Unit__c, New_Unit__r.Registration_Id__c, Allocated_Amount__c From Fund_Transfer_Units__r Where New_Unit__c != null),
        (Select Id, Attachment_URL__c From Documents__r Where Booking_Unit__c != null)
        From Sales_Service_Request__c Where Id IN: setSSRId]);
    }

    public Class TaskCreationResponse {
        public string message;
        public string status;

        public TaskCreationResponse(){
        }
    }
}