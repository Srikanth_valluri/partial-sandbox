/**************************************************************************************************
* Name               : DamacSingleAnnouncementControllerTest
* Description        : Test Class for DamacSingleAnnouncementController
* Created Date       : 30/09/2017                                                                     
* Created By         : pratiksha narvekar                                                           
* Last Modified Date :                                                                            
* Last Modified By   :                                                                            
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR                    DATE                                                                    
* 1.0         pratiksha narvekar        30/09/2017                                                               
**************************************************************************************************/
@isTest(seeAllData=false)
public class DamacSingleAnnouncementControllerTest{


public static testMethod void SingleAnnouncementTest(){

 DamacSingleAnnouncementController  obj =  new DamacSingleAnnouncementController();
 Notification__c objNot = new Notification__c();
 objNot.Title__c = 'test';
 objNot.Description__c = 'test';
 insert objNot;
 ApexPages.currentPage().getParameters().put('Id',objNot.ID);
 obj.getSingleNotification(); 
 
   }

}