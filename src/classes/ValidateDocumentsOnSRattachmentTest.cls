@isTest
/*
* Revision History: 
* Version   Author          Date        Description.
* 1.1       Swapnil Gholap  20/11/2017  Initial Draft
*/

public class ValidateDocumentsOnSRattachmentTest {
     static testMethod void Test1(){
        test.StartTest();
        
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person',party_ID__C='123456');
        insert objAcc;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'Test Unit';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        objBookingUnit.HOS_Name__c = 'Test HOS'; 
                       
        insert objBookingUnit;
        
        
        list<ID> lstCaseID = new list<ID>();
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Customer Refund').getRecordTypeId();
        Case objCase1 = new Case();        
        objCase1.Status = 'New';        
        objCase1.RecordTypeID = devRecordTypeId;
        objCase1.Approving_Authorities__c  = 'VP';
        objCase1.AccountID = objAcc.id;
        objCase1.Roles_from_Rule_Engine__c = 'VP';
        insert objCase1;                   
        
        list<SR_Attachments__c> lstSRAttachments = new list<SR_Attachments__c>();
        SR_Attachments__c objSRatt = new SR_Attachments__c ();
        objSRatt.name = 'Test NOC';
        objSRatt.Case__c = objCase1.Id;         
        objSRatt.type__c = 'NOC';            
        
        lstSRAttachments.add(objSRatt);
        insert lstSRAttachments;
        
        ValidateDocumentsOnSRattachment.ValidateDocument(lstSRAttachments);
          
        lstSRAttachments[0].isValid__c = true;
        update lstSRAttachments;
                
        ValidateDocumentsOnSRattachment.ValidateDocument(lstSRAttachments);
 
        test.StopTest();
     }
     
     static testMethod void Test2(){
        test.StartTest();
        
        UserRole role1 = new UserRole();
        role1.Name = 'HOD';
        role1.DeveloperName =  'HOD';
        insert role1; 
            
        User objMGR= new User();
        objMGR.FirstName = 'User199';
        objMGR.LastName = 'Test199';
        objMGR.ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' limit 1].Id;       
        objMGR.Email = 'Manager1237@amamama.com';
        objMGR.Username = 'Manager1237@amamama.com';
        objMGR.CompanyName = 'TEST22';
        objMGR.Title = 'title2';
        objMGR.Alias = 'ali12';
        objMGR.TimeZoneSidKey = 'Asia/Kolkata';
        objMGR.EmailEncodingKey = 'UTF-8';
        objMGR.LanguageLocaleKey = 'en_US';
        objMGR.LocaleSidKey = 'en_US';
        objMGR.IsActive = true;
        objMGR.UserRoleId = role1.Id;              
        insert objMGR;
        
        User objUser = new User();
        objUser.FirstName = 'User299';
        objUser.LastName = 'Test299';
        objUser.ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' limit 1].Id;       
        objUser.Email = 'Manager123@amamama.com';
        objUser.Username = 'Manager123@amamama.com';
        objUser.CompanyName = 'TEST22';
        objUser.Title = 'title2';
        objUser.Alias = 'ali12';
        objUser.TimeZoneSidKey = 'Asia/Kolkata';
        objUser.EmailEncodingKey = 'UTF-8';
        objUser.LanguageLocaleKey = 'en_US';
        objUser.LocaleSidKey = 'en_US';
        objUser.IsActive = true;    
        objUser.ManagerID = objMGR.id;       
        insert objUser;
        
        System.runAs(objUser){     
        
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person',party_ID__C='123456');
        insert objAcc;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'Test Unit';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true; 
        objBookingUnit.HOS_Name__c = 'Test HOS';                    
        insert objBookingUnit;
        
        
        list<ID> lstCaseID = new list<ID>();
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Token Refund').getRecordTypeId();
        Case objCase1 = new Case();        
        objCase1.Status = 'New';        
        objCase1.RecordTypeID = devRecordTypeId;
        objCase1.Approving_Authorities__c  = 'VP';
        objCase1.AccountID = objAcc.id;
        insert objCase1;                   
        
        list<SR_Attachments__c> lstSRAttachments = new list<SR_Attachments__c>();
        SR_Attachments__c objSRatt = new SR_Attachments__c ();
        objSRatt.name = 'Test NOC';
        objSRatt.Case__c = objCase1.Id;         
        objSRatt.type__c = 'NOC';            
        
        lstSRAttachments.add(objSRatt);
        insert lstSRAttachments;
        
       
            ValidateDocumentsOnSRattachment.ValidateDocument(lstSRAttachments);
            
            objCase1.Active_Unit_Ids__c = objBookingUnit.id;
            update objCase1;
            
            lstSRAttachments[0].isValid__c = true;
            update lstSRAttachments;
            
            ValidateDocumentsOnSRattachment.ValidateDocument(lstSRAttachments);
                    
                        
            
            objBookingUnit.HOS_Name__c = 'User199 Test199';                        
            update objBookingUnit; 
            
            
        }
        test.StopTest();
     }
     
     static testMethod void Test2_D(){
        test.StartTest();
        
        UserRole role1 = new UserRole();
        role1.Name = 'HOD';
        role1.DeveloperName =  'HOD';
        insert role1; 
            
        User objMGR= new User();
        objMGR.FirstName = 'User199';
        objMGR.LastName = 'Test199';
        objMGR.ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' limit 1].Id;       
        objMGR.Email = 'Manager1237@amamama.com';
        objMGR.Username = 'Manager1237@amamama.com';
        objMGR.CompanyName = 'TEST22';
        objMGR.Title = 'title2';
        objMGR.Alias = 'ali12';
        objMGR.TimeZoneSidKey = 'Asia/Kolkata';
        objMGR.EmailEncodingKey = 'UTF-8';
        objMGR.LanguageLocaleKey = 'en_US';
        objMGR.LocaleSidKey = 'en_US';
        objMGR.IsActive = true;
        objMGR.UserRoleId = role1.Id;              
        insert objMGR;
        
        User objUser = new User();
        objUser.FirstName = 'User299';
        objUser.LastName = 'Test299';
        objUser.ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' limit 1].Id;       
        objUser.Email = 'Manager123@amamama.com';
        objUser.Username = 'Manager123@amamama.com';
        objUser.CompanyName = 'TEST22';
        objUser.Title = 'title2';
        objUser.Alias = 'ali12';
        objUser.TimeZoneSidKey = 'Asia/Kolkata';
        objUser.EmailEncodingKey = 'UTF-8';
        objUser.LanguageLocaleKey = 'en_US';
        objUser.LocaleSidKey = 'en_US';
        objUser.IsActive = true;    
        objUser.ManagerID = objMGR.id;       
        insert objUser;
        
        System.runAs(objUser){     
        
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person',party_ID__C='123456');
        insert objAcc;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
              
        
        Booking_Unit__c objBookingUnit1 = new Booking_Unit__c();
        objBookingUnit1.Registration_ID__c  = '123456';
        objBookingUnit1.Unit_Name__c  = 'Test Unit1';
        objBookingUnit1.Booking__c  = objBooking.id;
        objBookingUnit1.Mortgage__c = true; 
        objBookingUnit1.Manager_Name__c = 'Test Manager1';   
        objBookingUnit1.HOS_name__c = '';                 
        insert objBookingUnit1;
        
        
        list<ID> lstCaseID = new list<ID>();
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Token Refund').getRecordTypeId();
        Case objCase1 = new Case();        
        objCase1.Status = 'New';        
        objCase1.RecordTypeID = devRecordTypeId;
        objCase1.Approving_Authorities__c  = 'VP';
        objCase1.AccountID = objAcc.id;
        insert objCase1;                   
        
        list<SR_Attachments__c> lstSRAttachments = new list<SR_Attachments__c>();
        SR_Attachments__c objSRatt = new SR_Attachments__c ();
        objSRatt.name = 'Test NOC';
        objSRatt.Case__c = objCase1.Id;         
        objSRatt.type__c = 'NOC';            
        
        lstSRAttachments.add(objSRatt);
        insert lstSRAttachments;
        
       objBookingUnit1.HOS_name__c = '';  
       update objBookingUnit1;
                  
            
            objCase1.Active_Unit_Ids__c = objBookingUnit1.id;
            update objCase1;
            
            lstSRAttachments[0].isValid__c = true;
            update lstSRAttachments;
            
            ValidateDocumentsOnSRattachment.ValidateDocument(lstSRAttachments);                                                        
            
        }
        test.StopTest();
     }
     
     static testMethod void Test2A(){
        test.StartTest();
        
         UserRole role1 = new UserRole();
        role1.Name = 'HOD';
        role1.DeveloperName =  'HOD';
        insert role1; 
            
        User objMGR= new User();
        objMGR.FirstName = 'User199';
        objMGR.LastName = 'Test199';
        objMGR.ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' limit 1].Id;       
        objMGR.Email = 'Manager1237@amamama.com';
        objMGR.Username = 'Manager1237@amamama.com';
        objMGR.CompanyName = 'TEST22';
        objMGR.Title = 'title2';
        objMGR.Alias = 'ali12';
        objMGR.TimeZoneSidKey = 'Asia/Kolkata';
        objMGR.EmailEncodingKey = 'UTF-8';
        objMGR.LanguageLocaleKey = 'en_US';
        objMGR.LocaleSidKey = 'en_US';
        objMGR.IsActive = true;
        objMGR.UserRoleId = role1.Id;              
        insert objMGR;
        
        User objUser = new User();
        objUser.FirstName = 'User299';
        objUser.LastName = 'Test299';
        objUser.ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' limit 1].Id;       
        objUser.Email = 'Manager123@amamama.com';
        objUser.Username = 'Manager123@amamama.com';
        objUser.CompanyName = 'TEST22';
        objUser.Title = 'title2';
        objUser.Alias = 'ali12';
        objUser.TimeZoneSidKey = 'Asia/Kolkata';
        objUser.EmailEncodingKey = 'UTF-8';
        objUser.LanguageLocaleKey = 'en_US';
        objUser.LocaleSidKey = 'en_US';
        objUser.IsActive = true;    
        objUser.ManagerID = objMGR.id;       
        insert objUser;
        
        System.runAs(objUser){     
        
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person',party_ID__C='123456');
        insert objAcc;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'Test Unit';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true; 
        objBookingUnit.HOS_Name__c = 'Test HOS';                    
        insert objBookingUnit;
        
        
        list<ID> lstCaseID = new list<ID>();
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Token Refund').getRecordTypeId();
        Case objCase1 = new Case();        
        objCase1.Status = 'New';        
        objCase1.RecordTypeID = devRecordTypeId;
        objCase1.Approving_Authorities__c  = 'VP';
        objCase1.AccountID = objAcc.id;
        insert objCase1;                   
        
        list<SR_Attachments__c> lstSRAttachments = new list<SR_Attachments__c>();
        SR_Attachments__c objSRatt = new SR_Attachments__c ();
        objSRatt.name = 'Test NOC';
        objSRatt.Case__c = objCase1.Id;         
        objSRatt.type__c = 'NOC';            
        
        lstSRAttachments.add(objSRatt);
        insert lstSRAttachments;
        
       
            ValidateDocumentsOnSRattachment.ValidateDocument(lstSRAttachments);
            
            objCase1.Active_Unit_Ids__c = objBookingUnit.id;
            update objCase1;
            
            lstSRAttachments[0].isValid__c = true;
            update lstSRAttachments;
            
            ValidateDocumentsOnSRattachment.ValidateDocument(lstSRAttachments);
                    
           
            
            objBookingUnit.Manager_Name__c = 'User299 Test299';
            objBookingUnit.HOS_Name__c = '';                        
            update objBookingUnit; 
            
            
        }
        test.StopTest();
     }
     
      static testMethod void Test2B(){
        test.StartTest();
        
         UserRole role1 = new UserRole();
        role1.Name = 'HOD';
        role1.DeveloperName =  'HOD';
        insert role1; 
            
        User objMGR= new User();
        objMGR.FirstName = 'User199';
        objMGR.LastName = 'Test199';
        objMGR.ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' limit 1].Id;       
        objMGR.Email = 'Manager1237@amamama.com';
        objMGR.Username = 'Manager1237@amamama.com';
        objMGR.CompanyName = 'TEST22';
        objMGR.Title = 'title2';
        objMGR.Alias = 'ali12';
        objMGR.TimeZoneSidKey = 'Asia/Kolkata';
        objMGR.EmailEncodingKey = 'UTF-8';
        objMGR.LanguageLocaleKey = 'en_US';
        objMGR.LocaleSidKey = 'en_US';
        objMGR.IsActive = true;
        objMGR.UserRoleId = role1.Id;              
        insert objMGR;
        
        User objUser = new User();
        objUser.FirstName = 'User299';
        objUser.LastName = 'Test299';
        objUser.ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' limit 1].Id;       
        objUser.Email = 'Manager123@amamama.com';
        objUser.Username = 'Manager123@amamama.com';
        objUser.CompanyName = 'TEST22';
        objUser.Title = 'title2';
        objUser.Alias = 'ali12';
        objUser.TimeZoneSidKey = 'Asia/Kolkata';
        objUser.EmailEncodingKey = 'UTF-8';
        objUser.LanguageLocaleKey = 'en_US';
        objUser.LocaleSidKey = 'en_US';
        objUser.IsActive = true;    
        objUser.ManagerID = objMGR.id;       
        insert objUser;
        
        System.runAs(objUser){     
        
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person',party_ID__C='123456');
        insert objAcc;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'Test Unit';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true; 
        //objBookingUnit.HOS_Name__c = 'Test HOS';                    
        insert objBookingUnit;
        
        
        list<ID> lstCaseID = new list<ID>();
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Token Refund').getRecordTypeId();
        Case objCase1 = new Case();        
        objCase1.Status = 'New';        
        objCase1.RecordTypeID = devRecordTypeId;
        objCase1.Approving_Authorities__c  = 'VP';
        objCase1.AccountID = objAcc.id;
        insert objCase1;                   
        
        list<SR_Attachments__c> lstSRAttachments = new list<SR_Attachments__c>();
        SR_Attachments__c objSRatt = new SR_Attachments__c ();
        objSRatt.name = 'Test NOC';
        objSRatt.Case__c = objCase1.Id;         
        objSRatt.type__c = 'NOC';            
        
        lstSRAttachments.add(objSRatt);
        insert lstSRAttachments;
        
       
            ValidateDocumentsOnSRattachment.ValidateDocument(lstSRAttachments);
            
            objCase1.Active_Unit_Ids__c = objBookingUnit.id;
            update objCase1;
            
            lstSRAttachments[0].isValid__c = true;
            update lstSRAttachments;                             
           
           objBookingUnit.Manager_Name__c = '';
            objBookingUnit.HOS_Name__c = '';                        
            update objBookingUnit; 
            
            ValidateDocumentsOnSRattachment.ValidateDocument(lstSRAttachments); 
            
        }
        test.StopTest();
     }
     
     static testMethod void Test3(){
        // to cover Hardik's AOPT code
        
        
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person',party_ID__C='123456');
        insert objAcc;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'Test Unit';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true; 
        objBookingUnit.HOS_Name__c = 'Test HOS';                    
        insert objBookingUnit;
        
        
        list<ID> lstCaseID = new list<ID>();
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Token Refund').getRecordTypeId();
        Case objCase1 = new Case();        
        objCase1.Status = 'Submitted';        
        objCase1.RecordTypeID = devRecordTypeId;
        objCase1.Approving_Authorities__c  = 'VP';
        objCase1.AccountID = objAcc.id;
        objCase1.Offer_Acceptance_Letter_Generated__c = true;
        objCase1.O_A_Signed_Copy_Uploaded__c = true;
        objCase1.Approval_Status__c = 'approved';
        insert objCase1;                   
        
        list<SR_Attachments__c> lstSRAttachments = new list<SR_Attachments__c>();
        SR_Attachments__c objSRatt = new SR_Attachments__c ();
        objSRatt.name = 'Test NOC';
        objSRatt.Case__c = objCase1.Id;
        objSRatt.isValid__c  = true;         
        objSRatt.type__c = 'NOC';            
        
        lstSRAttachments.add(objSRatt);
        insert lstSRAttachments;
        
        test.StartTest();
      
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );
        ValidateDocumentsOnSRattachment.ValidateDocument(lstSRAttachments);        
      
        test.StopTest();
     }
}