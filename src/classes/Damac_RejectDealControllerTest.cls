@isTest
private class Damac_RejectDealControllerTest {

    @isTest
    private static void testName() {
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        
        Id DealRT = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = a.Id;
        sr.RecordTypeId = DealRT;
        sr.Agency__c = a.id;
        sr.Agency_Type__c = 'Corporate';
        insert sr;
        
        New_Step__c nstp = new New_Step__c();
        nstp.Service_Request__c = sr.id;
        nstp.Step_No__c = 2;
        nstp.Step_Status__c = 'Awaiting Token Deposit';
        nstp.Step_Type__c = 'Token Payment';
        insert nstp;
/*
        NSIBPM__SR_Template__c SRTemplate = new NSIBPM__SR_Template__c();
        srtemplate.NSIBPM__SR_RecordType_API_Name__c = 'Deal';
        insert SRTemplate;

        Id RecType1 = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = a.id;
        sr.RecordTypeId = RecType1;
        sr.Agency__c = a.id;
        sr.NSIBPM__SR_Template__c = SRTemplate.id;
        sr.Agency_Type__c = 'Corporate';
        insert sr;

        NSIBPM__Step_Template__c sttempl = new NSIBPM__Step_Template__c();
        sttempl.NSIBPM__Step_RecordType_API_Name__c = 'Deal';
        sttempl.NSIBPM__Code__c = 'CANCELLED';
        insert sttempl;

        NSIBPM__Status__c stpSt = new NSIBPM__Status__c();
        stpSt.NSIBPM__Code__c = 'CANCELLED';
        stpSt.NSIBPM__Type__c = 'Start';
        insert stpst;

        NSIBPM__Step__c stp = new NSIBPM__Step__c();
        stp.NSIBPM__SR__c = sr.id;
        stp.NSIBPM__Status__c = stpst.id;
        stp.NSIBPM__Step_Template__c = sttempl.id;
        insert stp;

        NSIBPM__Step_Template__c sttempl1 = new NSIBPM__Step_Template__c();
        sttempl1.NSIBPM__Step_RecordType_API_Name__c = 'Deal';
        sttempl1.NSIBPM__Code__c = 'DEAL';
        insert sttempl1;

        NSIBPM__Status__c stpSt1 = new NSIBPM__Status__c();
        stpSt1.NSIBPM__Code__c = 'REJECTED';
        stpSt1.NSIBPM__Type__c = 'END';
        insert stpst1;

        NSIBPM__Step__c stp1 = new NSIBPM__Step__c();
        stp1.NSIBPM__SR__c = sr.id;
        stp1.NSIBPM__Status__c = stpst1.id;
        stp1.NSIBPM__Step_Template__c = sttempl1.id;
        insert stp1;
        
        NSIBPM__SR_Status__c status = new NSIBPM__SR_Status__c();
        status.NSIBPM__Code__c = 'REJECTED';
        insert status;  
        
        List<string> statuses = new list<string>{'fromcode','tocode'};
        Map<string,NSIBPM__Status__c> stepStatuses = InitializeSRDataTest.createStepStatus(statuses);
        
        
        NSIBPM__Transition__c trsition = new NSIBPM__Transition__c(NSIBPM__From__c = stepStatuses.values()[0].ID, NSIBPM__To__c = stepStatuses.values()[1].id,NSIBPM__Transition_Code__c = 'REJECTED');
        insert trsition;
        */
        
        test.starttest();
        Apexpages.currentpage().getparameters().put('id', sr.id);
        Apexpages.currentpage().getparameters().put('stepId', nstp.id);
        ApexPages.StandardController stdController = new ApexPages.StandardController(sr);
        Damac_RejectDealController obj = new Damac_RejectDealController(stdController);
        
        sr.NSIBPM__Rejection_Reason__c = 'Test class comments';
        sr.Reject_Status__c = 'DP Not paid';
        obj.srRecord = sr;
        obj.save();
        test.stoptest();
    }
}