@RestResource(urlMapping='/getRequestedPicklist/*')
global class HDAppGetGenericPicklistValues_API {

    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };

    @HttpPost
    global static FinalReturnWrapper  GetPicklistFields() {
    
        RestRequest req = RestContext.request;
        Blob body = req.requestBody;
        String jsonString = body.toString();
        System.debug('jsonString'+jsonString);
        System.debug('Request params:' + req.params);

        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        cls_meta_data objMeta = new cls_meta_data();
        cls_data objData = new cls_data();
        List<PickListValuesWrapper> pickListValuesWrapperList = new List<PickListValuesWrapper>();

        if(body==null) {
            objMeta.message = 'No JSON body found';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;

            returnResponse.meta_data = objMeta;
            return returnResponse;

        }

        RequestJSONWrapper objReqWrap;
        if(String.isNotBlank(jsonString)) {
            objReqWrap = (RequestJSONWrapper)JSON.deserialize(jsonString,RequestJSONWrapper.class);
        }
        System.debug('objReqWrap:: ' + objReqWrap);

        if(objReqWrap.requested_picklist_fields != null && objReqWrap.requested_picklist_fields.size() > 0) {

            for(String fieldName : objReqWrap.requested_picklist_fields){

                if(fieldName == 'preferred_language') {

                    GetFMCasePicklistFields_API.PickListValuesWrapper ObjPreferred_language = GetFMCasePicklistFields_API.getselectOptions('Account','Primary_Language__c');
                    
                    PickListValuesWrapper preferred_language = (PickListValuesWrapper)JSON.deserialize(JSON.serialize(ObjPreferred_language), PickListValuesWrapper.class);
                    
                    //New
                    List<ValueWrapper> valWrapLst = new list<ValueWrapper>();
                    for(String objStr : ObjPreferred_language.lookup_value) {
                        ValueWrapper objVW = new ValueWrapper();
                        objVW.code = objStr;
                        objVW.description = objStr;
                        valWrapLst.add(objVW);
                    }

                    //preferred_language.lookup_values = ObjPreferred_language.lookup_value;
                    preferred_language.lookup_values = valWrapLst;
                    preferred_language.lookup_type = 'preferred_language';
                    pickListValuesWrapperList.add(preferred_language);
                    System.debug('preferred_language'+preferred_language);
                }

                if(fieldName == 'other_payment_types') {
                    List<HD_App_Other_Payment_Types__mdt> lstPaymentMdt = [SELECT id
                                                                                , MasterLabel
                                                                                , Description__c 
                                                                           FROM HD_App_Other_Payment_Types__mdt];
                    System.debug('lstPaymentMdt:: ' + lstPaymentMdt);

                   List<ValueWrapper> lstPaymentWrap = new List<ValueWrapper>();
                   List<String> lstPaymentValues = new List<String>();
                    if(lstPaymentMdt.size() > 0) {
                        for(HD_App_Other_Payment_Types__mdt objMdt : lstPaymentMdt) {
                            ValueWrapper objPaymnt = new ValueWrapper();
                            objPaymnt.code = objMdt.MasterLabel;
                            objPaymnt.description = objMdt.Description__c;
                            lstPaymentWrap.add(objPaymnt);             
                        }
                    }
                    
                    PickListValuesWrapper otherPayments =  new PickListValuesWrapper();
                    System.debug('lstPaymentWrap:: ' + lstPaymentWrap);
                    otherPayments.lookup_type = 'other_payment_types';
                    otherPayments.lookup_values = lstPaymentWrap;
                    pickListValuesWrapperList.add(otherPayments);

                }
            }
        }

        System.debug('pickListValuesWrapperList'+pickListValuesWrapperList);
        
        objMeta.message = pickListValuesWrapperList.size() > 0 ? 'Successful' : 'No picklist Values';
        objMeta.status_code = 1;
        objMeta.title = mapStatusCode.get(1);
        objMeta.developer_message = null;
        
        objData.lookup_list = pickListValuesWrapperList;

        returnResponse.data = objData;
        returnResponse.meta_data = objMeta;
        System.debug('returnResponse: ' + returnResponse);
        
        return returnResponse;
    }

    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_meta_data {
        public Integer status_code;
        public String message;
        public String title;
        public String developer_message;   
    }

    public class cls_data {
        public PickListValuesWrapper[] lookup_list;
        //public String lookup_type;
        //public List<PaymentTypeWrapper> lookup_values;
    }
    
     public class PickListValuesWrapper {
        public String lookup_type;
        public List<ValueWrapper> lookup_values;
    }

    public class ValueWrapper {
        public String code;
        public String description;
    }

    public class RequestJSONWrapper {
        public String[] requested_picklist_fields;
    }
}