/****************************************************************************************************
* Name          : UpdateBookingUnitToIPMS                                                           *
* Description   : Class to Update the Booking Unit Data to IPMS                                     *
* Created Date  : 23/04/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR                DATE          COMMENTS                                                *
* 1.0   Craig Lobo            23/04/2018    Initial Draft.                                          *
* 2.0   Craig Lobo            05-06-2018    Updated the logic to fetch field values for the         *
*                                           Lead Management record                                  *
* 2.1   Sriaknth V            24-09-2020    To send the campaign name to IPMS
****************************************************************************************************/
global with sharing class UpdateBookingUnitToIPMS {

    global static String endURL;
    global static String serviceUserName;
    global static String servicePassword;
    //global static Map<String, String> bookingUnitLMMap = new Map<String, String>();

    public UpdateBookingUnitToIPMS() {
        
    }

    /**
     * Callout to Update the Booking Unit Data
     */
    @future(callout=true)
    webservice static void updateBookingUnitsInIPMS(List<Id> pLeadManagementIdList) {

        getIPMSsettings('IPMS_webservice');
        HTTPRequest req = new HTTPRequest();
        req.setMethod('POST');

        String reqXML = createXMLRequest(pLeadManagementIdList);
        System.debug('***reqXML***:   '+reqXML);
        
        reqXML = reqXML.replaceAll('null', '');
        reqXML = reqXML.trim();
        req.setbody(reqXML);
        req.setEndpoint(endURL);
        System.debug('>>>>>>>>reqXML>>>>>>>>' + reqXML);
        req.setHeader('Content-Type', 'text/xml');
        req.setTimeout(120000);
        HTTP http = new HTTP();
        if(!Test.isrunningTest()) {
            try {
                HTTPResponse res = http.send(req);
                System.debug('>>>>>Response>>>>>>'+res.getbody());
                //parseResponseStatus(res.getBody());
            } catch(Exception ex) {
                System.debug('Callout error: '+ ex);
                Log__c objLog = new Log__c(
                    Description__c  = 'UPDATE_LEAD_INFO--Ids=='
                                    + pLeadManagementIdList
                                    + '-Line No===>'
                                    + ex.getLineNumber()
                                    + '---Message==>'
                                    + ex.getMessage(),
                    Type__c = ' Webservice Callout to Update the Booking Unit (From LM Console)'
                );

                // DML
                insert objLog;
            }
        }
    }

    /**
     * Get the IPMS integration setting
     */ 
    global static void getIPMSsettings(String serviceName){
        System.debug('***SSS***'+serviceName);
        IPMS_Integration_Settings__mdt ipms = [SELECT Id, Endpoint_URL__c, Password__c, Username__c 
                                                 FROM IPMS_Integration_Settings__mdt 
                                                WHERE DeveloperName = :serviceName
        ];
        endURL = ipms.Endpoint_URL__c;
        serviceUserName = ipms.Username__c;
        servicePassword = PasswordCryptoGraphy.DecryptPassword(ipms.Password__c);
        system.debug('***WS_URL***'+endURL);
    }

    /**
     * Method to prepare the request for the Web Service
     */  
    global static String createXMLRequest(List<Id> pLeadManagementIdList) {

        String requestbody = '';
        List<Lead_Management__c> approvedLMList = new List<Lead_Management__c>();

        String leadManagementQuery  = ' SELECT Id, Name, Campaign_Name__c, Booking_Unit_Registration_Id__c, '
                                    + ' Booking_Unit__c, Correct_HOD__c, Correct_Promoter_Name__c,'
                                    + ' Correct_TSA_Name__c, Correct_TSA_Manager__c, '
                                    + ' Promoter_Name__c, HOD_Name__c, Registration_Id__c, '
                                    + ' TSA_Name__c, TSA_Manager_Name__c '
                                    + ' FROM Lead_Management__c '
                                    + ' WHERE Id IN :pLeadManagementIdList ';
        System.debug('>>>>leadManagementQuery>>>>  :   '+leadManagementQuery);
        approvedLMList = Database.query(leadManagementQuery);
        System.debug('>>>Lead_Management__c LIST >>>   :   '+approvedLMList);

        if (!approvedLMList.isEmpty()) {
            String reqNumber = '';
            reqNumber   = 'UPDATE_LEAD_INFO-'
                        + approvedLMList[0].Name
                        + '-'
                        + IPMS_WSHelper.GetFormattedDateTime(System.now());
            System.debug('REQ NO===>>>>>'+reqNumber);

            requestbody = IPMS_WSHelper.sSoaHeader('IPMS_webservice', serviceUserName, servicePassword);
            requestbody += '<soapenv:Body>'
                        + '<proc:InputParameters>'
                        + '<proc:P_REQUEST_NUMBER>'
                        + reqNumber
                        + '</proc:P_REQUEST_NUMBER>'
                        + '<proc:P_SOURCE_SYSTEM>SFDC</proc:P_SOURCE_SYSTEM>'
                        + '<proc:P_REQUEST_NAME>UPDATE_REGISTRATION</proc:P_REQUEST_NAME>'
                        + '<proc:P_REQUEST_MESSAGE>';

            for (Lead_Management__c lmObj : approvedLMList) {
                //bookingUnitLMMap.put(lmObj.Booking_Unit__c, lmObj.Id);
                requestbody += '<proc:P_REQUEST_MESSAGE_ITEM>'

                                // BU Id
                            + '<proc:PARAM_ID>' 
                            + lmObj.Booking_Unit__c 
                            + '</proc:PARAM_ID>'
                            
                                // Registration ID
                            + '<proc:ATTRIBUTE1>' 
                            + lmObj.Registration_Id__c 
                            + '</proc:ATTRIBUTE1>'
                            
                                // EntityName
                            + '<proc:ATTRIBUTE2>UPDATE_LEAD_INFO</proc:ATTRIBUTE2>';

                        // Promoter Name
                    if (lmObj.Promoter_Name__c && String.isNotBlank(lmObj.Correct_Promoter_Name__c)) {
                        System.debug('Promoter_Name__c>>> : ' + lmObj.Correct_Promoter_Name__c);
                        requestbody += '<proc:ATTRIBUTE19>' 
                                    + lmObj.Correct_Promoter_Name__c  
                                    + '</proc:ATTRIBUTE19>';
                    }

                        // HOD Name
                    
                    //2.1
                    if (lmObj.Campaign_Name__c != null && String.isNotBlank(lmObj.Campaign_Name__c)) {
                        System.debug('Campaign Name>>> : ' + lmObj.Campaign_Name__c);
                        requestbody += '<proc:ATTRIBUTE20>' 
                                    + lmObj.Campaign_Name__c
                                    + '</proc:ATTRIBUTE20>';
                    } else if (lmObj.HOD_Name__c && String.isNotBlank(lmObj.Correct_HOD__c)) {
                        System.debug('HOD_Name__c>>> : ' + lmObj.Correct_HOD__c);
                        requestbody += '<proc:ATTRIBUTE20>' 
                                    + lmObj.Correct_HOD__c
                                    + '</proc:ATTRIBUTE20>';
                    }

                        // TeleSalesExecutive
                    if (lmObj.TSA_Name__c && String.isNotBlank(lmObj.Correct_TSA_Name__c)) {
                        System.debug('TSA_Name__c>>> : ' + lmObj.Correct_TSA_Name__c);
                        requestbody += '<proc:ATTRIBUTE21>' 
                                    + lmObj.Correct_TSA_Name__c
                                    + '</proc:ATTRIBUTE21>';
                    }

                        // TeleSalesManager
                    if (lmObj.TSA_Manager_Name__c && String.isNotBlank(lmObj.Correct_TSA_Manager__c)) {
                        System.debug('TSA_Manager_Name__c>>> : ' + lmObj.Correct_TSA_Manager__c);
                        requestbody += '<proc:ATTRIBUTE22>' 
                                    + lmObj.Correct_TSA_Manager__c
                                    + '</proc:ATTRIBUTE22>';
                    }
                requestbody += '</proc:P_REQUEST_MESSAGE_ITEM>';
            } // Lead Management Loop

            requestbody += '</proc:P_REQUEST_MESSAGE>'
                        + '</proc:InputParameters>'
                        + '</soapenv:Body>'
                        + '</soapenv:Envelope>';

        } // List check

        System.debug('requestbody>>> : ' + requestbody);
        return requestbody.trim();

    }
}