@isTest
public class HDApp_WPContractorSR_APITest {
	@TestSetup
    static void TestData() {
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(Agency_ID__c = '1234');
        insert sr;
        
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;
        
        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];
        
        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Customer Community Login User(Use this)%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
        Database.insert(user1);
        
        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'test';
        locObj.Location_Type__c = 'Building';
        locObj.Location_ID__c = '12345';
        locObj.UAEProp__c = true;
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        invObj.Property_Country__c = 'UNITED ARAB EMIRATES';
        invObj.Marketing_Name_Doc__c = invObj.Id;
        invObj.Building_Location__c = locObj.Id;
        invObj.Property_Status__c = 'Ready'; 
        invObj.Unit_type__c = 'Villa';
        insert invObj;
        
        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1204',
                                      Registration_ID__c = '3901',
                                      Handover_Flag__c = 'N',
                                      Inventory__c = invObj.id,
                                      Dummy_Booking_Unit__c = true);
        insert bookingUnit;
        
        Id rtPopId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Work Permit').getRecordTypeId();
        FM_Case__c objWPFmCaseContractor = new FM_Case__c();
        objWPFmCaseContractor.Origin__c = 'Portal';
        objWPFmCaseContractor.isHelloDamacAppCase__c = true;
        objWPFmCaseContractor.Account__c = account.Id;
        objWPFmCaseContractor.RecordtypeId = rtPopId;
        objWPFmCaseContractor.Status__c = 'Draft Request';
        objWPFmCaseContractor.Request_Type__c = 'Work Permit';
        objWPFmCaseContractor.Booking_Unit__c = bookingUnit.Id;
        objWPFmCaseContractor.Work_Permit_Type__c = 'Work Permit - Including Fit Out';
        objWPFmCaseContractor.Person_To_Collect__c = 'Contractor';
        objWPFmCaseContractor.Contact_person_contractor__c = 'Test Contractor';
        objWPFmCaseContractor.Email_2__c = 'testctr@test.com';
        insert objWPFmCaseContractor;
        
        SR_Attachments__c srAttach = new SR_Attachments__c();
        srAttach.Name = 'Test';
        srAttach.Attachment_URL__c = 'http://damacproperties.force.com/Documents/apex/GetFile?id=01NUEFKAW4NVD35H4Z7VEKEMYYSTVGULRT';
        srAttach.FM_Case__c = objWPFmCaseContractor.Id;
        insert srAttach;
        
        FM_Case__c objWPFmCaseConsultant = objWPFmCaseContractor.clone();
        objWPFmCaseConsultant.Person_To_Collect__c = 'Consultant';
        objWPFmCaseConsultant.Contact_person__c = 'Test Consultant';
        objWPFmCaseConsultant.Email__c = 'testcst@test.com';
        insert objWPFmCaseConsultant;
    }
    
    @isTest
    static void testGetSRDetails() {
        Test.startTest();
		
        List<FM_Case__c> listFMCase = [SELECT Id,Name FROM FM_Case__c WHERE Person_To_Collect__c='Contractor' LIMIT 1];
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/contractorSR/getDetails';
        req.addParameter('fmCaseId',listFMCase[0].Id);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WPContractorSR_API.getWorkPermitSRDetails();
        
        Test.stopTest();
    }
    
    @isTest
    static void testGetSRDetailsWithFMCaseIdNull() {
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/contractorSR/getDetails';
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WPContractorSR_API.getWorkPermitSRDetails();
        
        Test.stopTest();
    }
    
    @isTest
    static void testGetSRDetailsWithInvalidFMCaseId() {
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/contractorSR/getDetails';
        req.addParameter('fmCaseId','abcd');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WPContractorSR_API.getWorkPermitSRDetails();
        
        Test.stopTest();
    }
    
    @isTest
    static void testSaveSRDetailsForContractor() {
        Account account = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c bookingUnit = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        
        Test.startTest();
        
        List<FM_Case__c> listFMCase = [SELECT Id,Name FROM FM_Case__c WHERE Person_To_Collect__c='Contractor' LIMIT 1];
        
        String jsonString = 
            '   {																			'  +
            '       "id": "' + listFMCase[0].Id + '",										'  +
            '       "person_to_collect": "Contractor",										'  +
            '       "contact_person_type": "Cleaning Contractor",							'  +
            '       "contact_person_office_telephone": "123456789",							'  +
            '       "contact_person_name": "Test Contractor",								'  +
            '       "contact_person_mobile_number": "111111111",							'  +
            '       "contact_person_mobile_country_code": "United Arab Emirates: 00971",	'  +
            '       "contact_person_email": "testctr@test.com",								'  +
            '       "contact_person_company_name": "Test Company",							'  +
            '       "booking_unit_id": "' + bookingUnit.Id + '",							'  +
            '       "account_id": "' + account.Id + '",										'  +
            '       "employees_details": [													'  +
            '           {																	'  +
            '               "employee_name": "Emp 1",										'  +
            '               "employee_id": "",												'  +
            '               "mobile_number": "1111111111",									'  +
            '               "emirates_id": "1234",											'  +
            '               "company_name": "Test company"									'  +
            '           },																	'  +
            '           {																	'  +
            '               "employee_name": "Emp 2",										'  +
            '               "employee_id": "",												'  +
            '               "mobile_number": "2222222222",									'  +
            '               "emirates_id": "5678",											'  +
            '               "company_name": "Test company"									'  +
            '           }																	'  +
            '       ]																		'  +
            '   }	';
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/contractorSR/saveDetails';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonString);
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WPContractorSR_API.saveWorkPermitSRDetails();
        
        Test.stopTest();
    }
    
    @isTest
    static void testSaveSRDetailsForConsultant() {
        Account account = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c bookingUnit = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        
        Test.startTest();
        
        List<FM_Case__c> listFMCase = [SELECT Id,Name FROM FM_Case__c WHERE Person_To_Collect__c='Consultant' LIMIT 1];
        
        String jsonString = 
            '   {																			'  +
            '       "id": "' + listFMCase[0].Id + '",										'  +
            '       "person_to_collect": "Consultant",										'  +
            '       "contact_person_type": "Cleaning Contractor",							'  +
            '       "contact_person_office_telephone": "123456789",							'  +
            '       "contact_person_name": "Test Consultant",								'  +
            '       "contact_person_mobile_number": "111111111",							'  +
            '       "contact_person_mobile_country_code": "United Arab Emirates: 00971",	'  +
            '       "contact_person_email": "testctr@test.com",								'  +
            '       "contact_person_company_name": "Test Company",							'  +
            '       "booking_unit_id": "' + bookingUnit.Id + '",							'  +
            '       "account_id": "' + account.Id + '",										'  +
            '       "employees_details": [													'  +
            '           {																	'  +
            '               "employee_name": "Emp 1",										'  +
            '               "employee_id": "",												'  +
            '               "mobile_number": "1111111111",									'  +
            '               "emirates_id": "1234",											'  +
            '               "company_name": "Test company"									'  +
            '           },																	'  +
            '           {																	'  +
            '               "employee_name": "Emp 2",										'  +
            '               "employee_id": "",												'  +
            '               "mobile_number": "2222222222",									'  +
            '               "emirates_id": "5678",											'  +
            '               "company_name": "Test company"									'  +
            '           }																	'  +
            '       ]																		'  +
            '   }	';
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/contractorSR/saveDetails';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonString);
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WPContractorSR_API.saveWorkPermitSRDetails();
        
        Test.stopTest();
    }
    
    @isTest
    static void testSaveSRDetailsWithFMCaseIdNull() {
        Test.startTest();
        
        String jsonString = 
            '   {																			'  +
            '       "id": "",																'  +
            '       "person_to_collect": "Contractor"										'  +
            '   }	';
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/contractorSR/saveDetails';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonString);
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WPContractorSR_API.saveWorkPermitSRDetails();
        
        Test.stopTest();
    }
    
    @isTest
    static void testSaveSRWithBlankPersonToCollect() {
        Test.startTest();
        
        List<FM_Case__c> listFMCase = [SELECT Id,Name FROM FM_Case__c WHERE Person_To_Collect__c='Contractor' LIMIT 1];
        
        String jsonString = 
            '   {																			'  +
            '       "id": "' + listFMCase[0].Id + '",										'  +
            '       "person_to_collect": ""													'  +
            '   }	';
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/contractorSR/saveDetails';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonString);
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WPContractorSR_API.saveWorkPermitSRDetails();
        
        Test.stopTest();
    }
    
    @isTest
    static void testSaveSRDetailsWithInvalidJSON() {
        String jsonString = 
            '   {	'  +
            '   }	'; 
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/contractorSR/saveDetails';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonString);
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WPContractorSR_API.saveWorkPermitSRDetails();
        
        Test.stopTest();
    }
}