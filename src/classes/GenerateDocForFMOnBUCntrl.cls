public without sharing class GenerateDocForFMOnBUCntrl {

    public String buId                                  {get;set;}
    public List<Booking_Unit__c> lstBu                  {get;set;}
    public blob urlHPBody;
    public list<SR_Attachments__c> lstCaseAttachment    {get;set;}
    public String custEmailid                           {get;set;}
    
    public GenerateDocForFMOnBUCntrl(ApexPages.StandardController controller) {
        lstBu = new List<Booking_Unit__c>();
        buId = ApexPages.currentPage().getParameters().get('id');
        
        System.debug('buId:::::::'+buId);
        lstBu = [ SELECT Id ,Party_Id__c
                       , Unit_Name__c,Name
                       , Registration_ID__c
                       , Booking__r.Account__r.IsPersonAccount
                       , Booking__r.Account__r.Email__pc
                       , Booking__r.Account__r.Email__c
                       , Booking__r.Account__r.Name
                       , Booking__r.Account__c
                    FROM Booking_Unit__c
                   WHERE Id = :buId LIMIT 1 ];
                   
        if (lstBu[0].Booking__r.Account__r.IsPersonAccount == True) {
            custEmailid = lstBu[0].Booking__r.Account__r.Email__pc;
        } else {
            custEmailid = lstBu[0].Booking__r.Account__r.Email__c;
        }
                   
    }
    
    public PageReference generateFMSOA(){
        System.debug('lstBu:::::::'+lstBu);
        if( String.isNotBlank( lstBu[0].Registration_ID__c ) ) {
            PageReference  pg = GenerateDocForFMCLCntrl.fetchUnitFMSOA( lstBu[0].Registration_ID__c , null,  lstBu[0].Id , null );
            if(pg != Null){
                //TODO  added to create record of soa genrator
                GenericUtility.createSOACreator(String.valueOf(pg) ,UserInfo.getUserId(),system.now(),lstBu[0].Registration_ID__c,'Bulk service charge SOA',NULL,lstBu[0].Id,NULL);
            }
            return pg ;
        }else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please enter registration id for Generating SOA'));
            return null;
        }
    }// End of generateFMSOA
    
    public PageReference generateFMPartySOA(){
        
        if( String.isNotBlank( lstBu[0].Party_ID__c ) ) {
           PageReference  pg =GenerateDocForFMCLCntrl.fetchPartyWiseFMSOA( lstBu[0].Party_ID__c , null , lstBu[0].Id , null );
           if(pg != Null){
                //TODO  added to create record of soa genrator
                GenericUtility.createSOACreator(String.valueOf(pg),UserInfo.getUserId(),system.now(),lstBu[0].Registration_ID__c,'BULK service charge SOA',NULL,lstBu[0].Id,NULL);
            }
            return pg ;
                
        }else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please provide party id for Generating SOA'));
            return null;
        }

    }// End of generateFMPartySOA
    
    
    public pagereference generateAndEmailFMUnitSOA(){
        System.debug('lstBu:::::::'+lstBu);
        if( String.isNotBlank( lstBu[0].Registration_ID__c ) ) {
            //return GenerateDocForFMCLCntrl.fetchUnitFMSOA( lstBu[0].Registration_ID__c , null,  lstBu[0].Id , null );
            String soaUrl;
            try {
                soaUrl = FmIpmsRestServices.getUnitSoaByRegistrationId( lstBu[0].Registration_ID__c );
            } catch(Exception excp) {
                errorLogger( excp.getMessage()+' at ' +String.valueOf( excp.getLineNumber() ) , null ,lstBu[0].Id ,null  );
            }
            
            if (String.isBlank(soaUrl)) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error : Unit SOA generation failed. Please try again'));
                return null;                
            }else {
                if(!Test.isRunningTest())
                    urlHPBody = getBlob(soaUrl);
                /*SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
                objCaseAttachment.Name = 'FM SOA ' + lstBu[0].Name + ' '+ DateTime.now();
                objCaseAttachment.Attachment_URL__c = soaUrl;
                objCaseAttachment.Booking_Unit__c = lstBu[0].Id;
                lstCaseAttachment.add(objCaseAttachment);
                
                try{
                    if (lstCaseAttachment != null && lstCaseAttachment.size()>0){
                        insert lstCaseAttachment;
                        System.debug('======Success==lstCaseAttachment : '+lstCaseAttachment[0].Id );
                    }
                } catch (Exception e) {
                    System.debug('========error : '+e );
                }*/

                String soaType = 'FM Unit SOA.pdf';
                String userName = UserInfo.getUserName();
                User activeUser = [Select Email From User where Username = : userName limit 1];
                String userEmail = activeUser.Email;
                String custEmail;
                if (lstBu[0].Booking__r.Account__r.IsPersonAccount == True) { 
                    custEmail = lstBu[0].Booking__r.Account__r.Email__pc;
                } else {
                    custEmail = lstBu[0].Booking__r.Account__r.Email__c;
                }
                SendEmail(urlHPBody, custEmail, lstBu[0].Booking__r.Account__r.Name, soaType, userEmail, lstBu[0].Unit_Name__c, lstBu[0].Booking__r.Account__c,soaUrl);

            }
        }else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please enter registration id for Generating SOA'));
            return null;
        }
        pagereference newpg = new PageReference ('/'+lstBu[0].Id);
        newpg.setRedirect(false);
        return newpg;
    }

    public pagereference generateAndEmailFMPartySOA(){
        System.debug('lstBu:::::::'+lstBu);
   
        if( String.isNotBlank( lstBu[0].Party_ID__c ) ) {
            String soaUrl;
            try {
                soaUrl = FmIpmsRestServices.getBulkSoaUrlForPartyId( lstBu[0].Party_ID__c );
                System.debug('soaUrl:::::::'+soaUrl);
            } catch(Exception excp) {
                errorLogger( excp.getMessage()+' at ' +String.valueOf( excp.getLineNumber() ) , null ,lstBu[0].Id ,null  );
            }
            System.debug('soaUrl::::::1 :'+soaUrl); 
            if (String.isBlank(soaUrl)) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error : Bulk SOA generation failed. Please try again'));
                return null;                
            }else {
                if(!Test.isRunningTest()){
                    urlHPBody = getBlob(soaUrl);
                }
                else {
                    urlHPBody = blob.valueOf('Test');
                }

                System.debug('urlHPBody:::::::'+urlHPBody);
                String soaType = 'Party SOA.pdf';
                String userName = UserInfo.getUserName();
                User activeUser = [Select Email From User where Username = : userName limit 1];
                String userEmail = activeUser.Email;
                String custEmail;
                if (lstBu[0].Booking__r.Account__r.IsPersonAccount == True) { 
                    custEmail = lstBu[0].Booking__r.Account__r.Email__pc;
                } else {
                    custEmail = lstBu[0].Booking__r.Account__r.Email__c;
                }
                
                System.debug('userEmail:::::::'+userEmail);
                System.debug('custEmail:::::::'+custEmail);
                
                SendEmail(urlHPBody, custEmail, lstBu[0].Booking__r.Account__r.Name, soaType, userEmail, lstBu[0].Unit_Name__c, lstBu[0].Booking__r.Account__c,soaUrl);

            }
        }else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please enter party id for Generating SOA'));
            return null;
        }
        pagereference newpg = new PageReference ('/'+lstBu[0].Id);
        newpg.setRedirect(false);
        return newpg;
    }
    
    
    public pagereference CancelGeneration(){
    
        System.Debug('=BookingUnit_SOAExtension********buId : ' + buId);

        pagereference newpg = new PageReference ('/'+buId);
        newpg.setRedirect(false);
        return newpg;
    }       
    
    public static void errorLogger(string strErrorMessage,Id clId, Id buId , Id accId ){
        Error_Log__c objError = new Error_Log__c();
        objError.Error_Details__c = strErrorMessage;
        objError.Calling_List__c = clId;
        objError.Booking_Unit__c = buId;
        objError.Account__c = accId;
        insert objError;
    } 
    
    public void SendEmail(blob file, String email, String accName, String soaType, String creMail, String buId, Id accId , string strUrl) {
        //New instance of a single email message
        Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();
                    
        list<Messaging.SingleEmailMessage> mails = new  list<Messaging.SingleEmailMessage>();

        List<String> sendTo = new List<String>();
        if(email !='' || email != null) {
          sendTo.add(email);
        }
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'no-replysf@damacgroup.com'];
        if ( owea.size() > 0 ) {
            mail.setOrgWideEmailAddressId(owea.get(0).Id);
        }
        list<String> bccAddress = new list<String>();
        //bccAddress.add(creMail);
        /*
        * Updated bcc address on 10/02/2019
        */
        bccAddress.add(Label.SF_Copy_EMail_Address);
        //mail.setSubject('Statement of Account Generated.');
        mail.setToAddresses(sendTo);
        mail.setBccAddresses(bccAddress);
        mail.setUseSignature(false);
        //mail.setReplyTo('test123@acme.com');
        //mail.setSenderDisplayName('SalesForce.com');
        //mail.setWhatId(accId);
        //mail.setTargetObjectId(accId); 
        //mail.setSaveAsActivity(true);  
        
        string name = nameFormat(accName);
        
        String body = 'Dear ' + name + ',<br/>';
        /*body += '\n Please find attached Statement of Account. '; 
        body += '\n \n Thanks & Regards,';
        body += '\n SalesForce Team';*/
        if(soaType.equalsIgnoreCase('FM Unit SOA.pdf')) {
            mail.setSubject('Service Charge Statement of Account for unit '+ buId);
            body += '<br/> As requested, please find attached Service Charge Statement of Account for unit '+ buId +'.<br/>';
        } else if(soaType.equalsIgnoreCase('Party SOA.pdf')) {
            mail.setSubject('Bulk Service charge Statement of Account ');
            body += '<br/> As requested, please find attached the Bulk Service charge Statement of Account.<br/>';
        }
        body += '<br/>Regards,';
        body += '<br/>' + UserInfo.getName();
        body += '<br/> Officer-Client Relations';
        body += '<br/>DAMAC PROPERTIES Co. LLC.';
        body += '<br/> P.O. Box: 2195, Dubai, United Arab Emirates';
        body += '<br/> Telephone: +971 4 237 5000';
        body += '<br/> Fax: +971 4 373 1373';
        body += '<br/> E-mail: atyourservice@damacproperties.com';
        body += '<br/> http://www.damacproperties.com/';


        mail.setHtmlBody(body);

        List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
        Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
        efa.setFileName(soaType);
        efa.setBody(file);
        fileAttachments.add(efa);
        mail.setFileAttachments(fileAttachments);
        mails.add(mail);
        try {
            Messaging.sendEmail(mails);
            System.debug('===========Mail sent====');
            
            EmailMessage emailMsg = new EmailMessage();

            emailMsg.ToAddress=(mails[0].getToAddresses())[0];
            emailMsg.FromAddress = UserInfo.getUserEmail();
            emailMsg.FromName = 'SOAs SR - Document';
            emailMsg.Subject=mails[0].getSubject();
            emailMsg.HtmlBody=mails[0].getHtmlBody();
            emailMsg.Account__c = accId; //Attach with the account
            emailMsg.MessageDate = system.now();
            emailMsg.Status = '3';
            emailMsg.Attachment_URL__c = strUrl;
            emailMsg.BccAddress = (mails[0].getBccAddresses())[0];
            insert emailMsg;
            
        } catch(Exception e) {
            System.debug('======Error in email sending=========' + e);
        }
    }
    
    public static Blob getBlob(String url){
        PageReference pageRef = new PageReference(url);
        Blob ret = pageRef.getContentAsPDF();
        return ret;
    }
    
    public static String nameFormat ( String strName ) {
        system.debug( ' strName : '+ strName );
        strName = strName.toLowerCase();
        List<String> names = strName.split(' ');
        for (Integer i = 0; i < names.size(); i++) {
            names[i] = names[i].capitalize();
        }
        strName = String.join(names, ' ');
        system.debug(strName);
        return strName;
    }
    
}