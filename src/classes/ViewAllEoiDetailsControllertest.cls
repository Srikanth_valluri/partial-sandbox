@isTest
public Class ViewAllEoiDetailsControllertest{
    
    public static testmethod void method_one(){
        Inquiry__c inqObj= new Inquiry__c(
          Mobile_Phone_Encrypt__c='456123',
          Mobile_CountryCode__c='American Samoa: 001684',
          Mobile_Phone__c='1234',
          Email__c='vieweoi@gmail.com',
          First_Name__c='Test',
          Last_Name__c='Last',
          Meeting_Type__c = 'Scheduled Tour'
          );
            
        insert inqObj;
        
         Account acc = InitialiseTestData.getCorporateAccount('Test Agency11');
         insert acc ;
        
        
        EOI_Process__c eoiobj = new EOI_Process__c();
        eoiobj.Inquiry__c = inqObj.id;
        eoiobj.Agency__c = acc.id;
        eoiobj.Mode_of_Token_Payment__c = 'Cash';
        eoiobj.No_of_Units__c = '1 Unit';
        insert eoiobj;
    
        Test.startTest();
            ViewAllEoiDetailsController con = new ViewAllEoiDetailsController();
            
        Test.stopTest();
    }
}