@isTest

public class MortgageIPMSCalloutTest {
     static Account  objAcc;
     static Case objCase;
     static NSIBPM__Service_Request__c objSR;
     
     static testMethod void initialMethod(){
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person',party_ID__C='123456');
        insert objAcc;
        
        objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
        
        objCase = new Case();
        objCase.AccountID = objAcc.id ;        
        objCase.POA_Expiry_Date__c = System.today();
        objCase.IsPOA__c = true;
        objCase.status = 'New';       
        insert objCase;
        
     }
     
       static testMethod void Test1(){
        test.StartTest();
        initialMethod();
        
        List<Case> lstCase = new List<Case>();
        lstCase.add(objCase);
                
        MortgageIPMSCallout.updateCaseInIPMS(lstCase);
         
        test.StopTest();      
      }
                                 
       
}