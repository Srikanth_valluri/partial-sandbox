public class UploadInventoryController {
    public blob csvDataBlob{get; set;}
    public string csvDataStr;
    public string selProperty{get; set;}
    public list<SelectOption> propertyOptions{get; set;}
    public list<Location__c> buildings{get; set;}
    public list<Location__c> floors{get; set;}
    public list<Location__c> units{get; set;}
    public list<Inventory__c> inventories{get; set;}
    public UploadInventoryController() {
        buildings = new list<Location__c>();
        floors = new list<Location__c>();
        units = new list<Location__c>();
        inventories = new list<Inventory__c>();
        propertyOptions = new list<Selectoption>();
        propertyOptions.add(new SelectOption('', '--Select Property--'));
        for(Property__c prop: [Select id, name from Property__c])
            propertyOptions.add(new SelectOption(prop.Id, prop.Name));
    }

    public void upload(){
        Savepoint sp = Database.setSavepoint();
        
        try{
            csvDataStr = csvDataBlob.toString();
            readCSVFile();
            Apexpages.addmessage(new Apexpages.Message(ApexPages.Severity.info, 'Uploaded Successfully.'));
        }
        catch(Exception e){
            Apexpages.addmessage(new Apexpages.Message(ApexPages.Severity.fatal, e.getMessage()+'-'+e.getLineNumber()));
            Database.RollBack(sp);
        }
    }

    public void readCSVFile(){
        list<string> csvLines = csvDataStr.split('\n');
        list<string> csvFieldNames = csvLines[0].split(',');
        //Insert Building Locations
        map<string, Location__c> mpBuildings = new map<string, Location__c>();
        for(integer i = 1; i < csvLines.size(); i++){
            string csvLine = csvLines[i];
            list<string> lineData = csvLine.split(',');
            if(lineData.size() > 0){
                if(lineData[2] == 'Building'){
                    Location__c build = new Location__c();
                    build.name = lineData[0];
                    build.Location_ID__c = lineData[1];
                    build.Location_Code__c = lineData[0];
                    build.RecordTypeId = Schema.getGlobalDescribe().get('Location__c').getDescribe().getRecordTypeInfosByName().get('Building').getRecordTypeId();
                    build.Property_Name__c = selProperty;
                    build.Building_Name__c = lineData[3];
                    build.Seller_name__c = lineData[4];
                    build.Master_developer__c = lineData[5];
                    build.Developer_Number__c = lineData[6];
                    mpBuildings.put(build.Location_ID__c, build);
                }
            }
        }

        if(!mpBuildings.isEmpty()){
            insert mpBuildings.values();
            buildings = mpBuildings.values();
        }
        //Insert Floor Locations
        map<string, Location__c> mpFloors = new map<string, Location__c>();
        for(integer i = 1; i < csvLines.size(); i++){
            string csvLine = csvLines[i];
            list<string> lineData = csvLine.split(',');
            if(lineData.size() > 0){
                if(lineData[2] == 'Floor'){
                    Location__c floor = new Location__c();
                    floor.Location_ID__c = lineData[1];
                    floor.Location_Code__c = lineData[0];
                    floor.name = lineData[0];
                    floor.RecordTypeId = Schema.getGlobalDescribe().get('Location__c').getDescribe().getRecordTypeInfosByName().get('Floor').getRecordTypeId();
                    floor.Property_Name__c = selProperty;
                    floor.Building_Number__c = mpBuildings.get(lineData[8]).Id;
                    floor.Floor_Name__c = lineData[7];
                    mpFloors.put(floor.Location_ID__c, floor);
                }
            }
        }

        if(!mpFloors.isEmpty()){
            insert mpFloors.values();
            floors = mpFloors.values();
        }

        //Insert Floor Locations
        map<string, Location__c> mpUnits = new map<string, Location__c>();
        map<string, Inventory__c> mpInventories = new map<string, Inventory__c>();
        for(integer i = 1; i < csvLines.size(); i++){
            string csvLine = csvLines[i];
            list<string> lineData = csvLine.split(',');
            if(lineData.size() > 0){
                if(lineData[2] == 'Unit'){
                    Location__c unit = new Location__c();
                    unit.Location_ID__c = lineData[1];
                    unit.Location_Code__c = lineData[0];
                    unit.name = lineData[0];
                    unit.RecordTypeId = Schema.getGlobalDescribe().get('Location__c').getDescribe().getRecordTypeInfosByName().get('Unit').getRecordTypeId();
                    unit.Property_Name__c = selProperty;
                    unit.Building_Number__c = mpBuildings.get(lineData[16]).Id;
                    unit.Floor_Number__c = mpFloors.get(lineData[15]).Id;
                    unit.Unit_Name__c = lineData[9];
                    mpUnits.put(unit.Location_ID__c, unit);

                    Inventory__c invRec = new Inventory__c();
                    invRec.Building_Location__c = mpBuildings.get(lineData[16]).Id;
                    invRec.Floor_Location__c = mpFloors.get(lineData[15]).Id;
                    invRec.Unit__c = unit.Location_ID__c;
                    invRec.Property__c = selProperty;
                    invRec.Unit_type__c = lineData[18];
                    invRec.View_type__c = lineData[19];
                    invRec.Status__c = lineData[20];
                    invRec.Conn_project_Code__c = lineData[21];
                    invRec.Bedroom_Type__c = lineData[22];
                    invRec.Space_Type_Lookup_Code__c = lineData[23];
                    mpInventories.put(invRec.Unit__c, invRec);
                }
            }
        }

        if(!mpUnits.isEmpty()){
            insert mpUnits.values();
            units = mpUnits.values();
        }
        if(!mpInventories.isEmpty()){
            for(string locationId : mpInventories.keyset()){
                Inventory__c inv = mpInventories.get(locationId);
                inv.Unit_Location__c = mpUnits.get(inv.Unit__c).Id;
                inv.Unit__c = mpUnits.get(inv.Unit__c).Location_Code__c;
            }
            insert mpInventories.values();
            inventories = mpInventories.values();
        }
    }
}