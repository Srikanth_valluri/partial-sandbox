/*
 * Description: Test class for FetchCaseDetailsController
 */

@isTest
private class FetchCaseDetailsControllerTest {

    @isTest
    private static void testGetCaseDetails() {

        // Creation of case
        Case objCase = new Case();
        insert objCase;
        List<Case> lstCaseRetrieved;

        Test.startTest();
            FetchCaseDetailsController objController = new FetchCaseDetailsController();
            objController.whatId = objCase.Id;
            objController.getCaseDetails();
        Test.stopTest();
    }
}