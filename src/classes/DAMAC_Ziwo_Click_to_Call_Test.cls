@isTest
public class DAMAC_Ziwo_Click_to_Call_Test {
    Static testMethod void testMethod1() {  
        Ziwo_settings__c settings = new Ziwo_settings__c ();
        settings.Endpoint__c = 'WWW.DamacGroup.com';
        settings.user_name__c = 'DAMAC';
        settings.Last_logged_In_Time__c  = DateTime.Now().addDays(-2);
        settings.password__c = 'DAMAC';
        settings.API_key__c = 'test';
        insert settings;
        
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'puser000@test.com',
            Username = 'puser000@test.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            Ziwo_DID__c = 'test'
        );
        insert u;
        
        
        id recTypeid = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        Inquiry__c inq = new Inquiry__c ();
        inq.Is_Meeting_scheduled__c = false;
        inq.RecordTypeId = recTypeid;
        inq.ownerId = Userinfo.getUserId();
        insert inq;
        
        Waybeo_Logs__c  log = new Waybeo_Logs__c ();
        log.Ziwo_recordingFile__c = 'test.com';
        log.Ziwo_callID__c = 'test'+DateTime.Now().getTime();
        insert log;
        
        System.debug (inq.Id);
        System.debug (inq.ownerId);
        
        String taskId = DAMAC_Ziwo_Click_to_Call.callZiwo (inq.id, '');
        taskId = taskId.split ('####')[0];
        Task t = [SELECT ID FROM Task WHERE ID =: taskId];
        t.Next_activity_Date__c = DateTime.Now().addDays(2);
        
        ApexPages.standardController contorller = new ApexPages.standardController (inq);
        DAMAC_Ziwo_Click_to_Call obj = new DAMAC_Ziwo_Click_to_Call(contorller);
        obj.taskRecord = t;
        obj.updateTask ();
        
        apexPages.currentPage().getParameters().Put ('id', log.Id);
        Damac_ZiwoCallRecording obj1 = new Damac_ZiwoCallRecording (contorller );
        obj1.init ();
        
        Call_Log__c objCallLog = new Call_Log__c();
        objCallLog.Call_Recording_URL__c = '34534dfsgabds';
        objCallLog.Calling_Number__c = '342354234';
        objCallLog.call_ID__c = '123';
        insert objCallLog;
        apexPages.currentPage().getParameters().Put ('id', objCallLog.Id);
        obj1.initCallLog ();
        obj.getPickListValuesOfActivityOutcomes();
        obj.getPickListValuesOfActivityType3 ();

    }
}