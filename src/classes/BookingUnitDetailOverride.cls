public without sharing class BookingUnitDetailOverride{
    public string recordId;
    
    public BookingUnitDetailOverride(ApexPages.StandardController sc){
        recordId = sc.getId();
        system.debug('*****recordId*****'+recordId);
    }
    
    public pageReference redirectToDetail(){
        PageReference pgRef;
        list<String> lstProfiles = new list<String>();
         if(Test.isRunningTest()){
            lstProfiles.add('System Administrator');
        }else{
            lstProfiles.addAll(Label.FM_Profile_Names.split(','));
        }
        list<User> lstUser = [Select Id
                                   , ProfileId
                                   , Profile.Name 
                              From User 
                              Where Id =: UserInfo.getUserId()
                              and Profile.Name IN : lstProfiles];
        // If current user is not an FM user redirect to Account detail page
        system.debug('*****lstUser*****'+lstUser);
        if(lstUser == null || lstUser.isEmpty()){
            pgRef = new Pagereference('/'+recordId);
            pgRef.getParameters().put('nooverride', '1');
            return pgRef;
        }else{
            list<Booking_Unit__c> lstBU = [Select Id
                                                , Unit_Name__c 
                                           From Booking_Unit__c 
                                           Where Id =: recordId];
            system.debug('*****lstBU*****'+lstBU);
            if(lstBU != null && !lstBU.isEmpty() && String.isNotBlank(lstBU[0].Unit_Name__c)){
                list<String> lstStr = lstBU[0].Unit_Name__c.split('/');
                boolean blnAllowAccess = false;
                for(Location__c objL : [Select Id
                                             , Name
                                             , (Select Id
                                                     , FM_User__c
                                                     , Building__c
                                                From FM_Users__r
                                                Where FM_User__c =: UserInfo.getUserId()) 
                                        From Location__c 
                                        Where Name =: lstStr[0]]){
                    system.debug('*****objL.FM_Users__r*****'+objL.FM_Users__r);
                    if(!objL.FM_Users__r.isEmpty()){
                        blnAllowAccess = true;
                    }
                }
                system.debug('*****blnAllowAccess*****'+blnAllowAccess);
                if(blnAllowAccess){
                    pgRef = new Pagereference('/'+recordId);
                    pgRef.getParameters().put('nooverride', '1');
                    return pgRef;
                }else{
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'You do not have access to this Booking Unit record. Please contact your Administrator');
                    ApexPages.addMessage(myMsg);
                    return null;
                }
            }
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'You do not have access to this Booking Unit record. Please contact your Administrator');
            ApexPages.addMessage(myMsg);
            return null;
        }
    }
}