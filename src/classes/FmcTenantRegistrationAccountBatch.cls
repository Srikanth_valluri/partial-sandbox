/**
 * @File Name          : FmcTenantRegistrationAccountBatch.cls
 * @Description        :
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              :
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 10/7/2019, 12:43:21 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    10/7/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
global class FmcTenantRegistrationAccountBatch implements Database.Batchable<sObject>, Schedulable {

    String query = 'SELECT Id'

                        + ', Tenant__c'
                        + ', First_Name__c'
                        + ', Last_Name__c'
                        + ', Mobile_Country_Code__c'
                        + ', Mobile_no__c'
                        + ', Tenant_Email__c'
                        + ', Gender__c'
                        + ', Nationality__c'
                        + ', Passport_Number__c'
                        + ', Status__c'
                        + ', Submitted__c'
                        + ', Property_Manager_Email__c'
                        + ', FM_Manager_Email__c'
                        + ', Recall_Approvals__c'
                + ' FROM  FM_Case__c'
                + ' WHERE RecordType.Name = \'Tenant Registration\''
                        + ' AND (Create_Tenant__c = TRUE OR Update_Tenant__c = TRUE OR Recall_Approvals__c = TRUE)'
                        + ' AND Status__c NOT IN (\'New\', \'In Progress\', \'Draft Request\', \'Closed\', \'Rejected\', \'Cancelled\')'
                + ' ORDER BY CreatedDate DESC';

    global FmcTenantRegistrationAccountBatch() {

    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute( Database.BatchableContext BC, List<sObject> scope ) {
        List<FM_Case__c> lstSr = (List<FM_Case__c>) scope;
        System.debug('lstSr = ' + Json.serialize(scope));

        list<FM_Case__c> lstFMCases = new list<FM_Case__c>();
        list<FM_Case__c> lstRecallingFMCases = new list<FM_Case__c>();
        for( FM_Case__c objFMCase : lstSr ) {
            if( objFMCase.Recall_Approvals__c ) {
                lstRecallingFMCases.add( objFMCase );
            }
            else {
                lstFMCases.add( objFMCase );
            }
        }

        List<Account> lstTenantAccount = createTenantAccounts( lstFMCases );
        if(lstTenantAccount != NULL && !lstTenantAccount.isEmpty()){
            Database.upsert(lstTenantAccount, false);
        }

        lstFMCases = updateTenantOnSrAndSubmitForApproval(lstFMCases, lstTenantAccount);

        updateTenantOnAdditionalDetails(lstFMCases);

        if( !lstRecallingFMCases.isEmpty() ) {
            recallApprovals( lstRecallingFMCases );
        }

    }

    global void finish(Database.BatchableContext BC) {

    }

    private static List<Account> createTenantAccounts(List<FM_Case__c> lstRegistrationSr) {
        //create a map that will hold the values of the list
        map<id,account> accmap = new map<id,account>();
        list<account> accLstToInsert = new list<account>();

        Id personAccountRecordTypeId =
                Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

        List<Account> lstTenantAccount = new List<Account>();
        for (FM_Case__c sr : lstRegistrationSr) {

            String strCountryCode = String.isNotBlank(sr.Mobile_Country_Code__c) && sr.Mobile_Country_Code__c.split(':').size() >=1 ?
                                    sr.Mobile_Country_Code__c.split(':')[1] : String.isNotBlank(sr.Mobile_Country_Code__c) ? sr.Mobile_Country_Code__c : '';
            strCountryCode = strCountryCode.trim();
            System.debug('sr.Mobile_no__c in batch - createTenantAccounts: '+sr.Mobile_no__c);

            Account accObj = new Account(
                 Id = sr.Tenant__c,
                RecordTypeId = personAccountRecordTypeId,
                FirstName = sr.First_Name__c,
                LastName = sr.Last_Name__c,
                PersonMobilePhone = strCountryCode + Long.valueOf( sr.Mobile_no__c.remove('-').remove(' ').remove('+').removeStart('0').removeStart('0') ),
                Mobile_Country_Code__pc = sr.Mobile_Country_Code__c,
                Mobile_Country_Code__c = sr.Mobile_Country_Code__c,
                Mobile_Phone_Encrypt__pc = strCountryCode + Long.valueOf( sr.Mobile_no__c.remove('-').remove(' ').remove('+').removeStart('0').removeStart('0') ),
                Mobile_Phone_Encrypt__c = sr.Mobile_no__c,
                PersonEmail = sr.Tenant_Email__c,
                Email__pc = sr.Tenant_Email__c,
                Gender__pc = sr.Gender__c,
                Nationality__pc = sr.Nationality__c,
                Passport_Number__pc = sr.Passport_Number__c,
                Tenant__c = 'Yes',
                OwnerId = UserInfo.getUserId()
            );
            if(accObj.Id != NULL){
                accmap.put(accObj.Id, accObj);
            }
            else{
                lstTenantAccount.add(accObj);
            }

        }
        lstTenantAccount.addAll(accmap.values());

        //System.debug('lstTenantAccount = ' + Json.serialize(lstTenantAccount));
        return lstTenantAccount;
    }

    private static List<FM_Case__c> updateTenantOnSrAndSubmitForApproval(
        List<FM_Case__c> lstSr, List<Account> lstTenantAccount
    ) {
        for(Integer i = 0; i < lstSr.size(); i++) {
            if (lstSr[i].Tenant__c == NULL) {
                lstSr[i].Create_Tenant__c = false;
                if (lstSr[i].Submitted__c && 'Submitted'.equalsIgnoreCase(lstSr[i].Status__c)) {
                    lstSr[i].Submit_for_Approval__c = true;
                }
                lstSr[i].Tenant__c = lstTenantAccount[i].Id;
            } else {
                lstSr[i].Update_Tenant__c = false;
            }
        }
        Database.update(lstSr, false);
        return lstSr;
    }

    private static void updateTenantOnAdditionalDetails(List<FM_Case__c> lstSr) {
        Map<Id, FM_Case__c> srDetails = new Map<Id, FM_Case__c>(lstSr);

        List<FM_Additional_Detail__c> lstAdditionalDetails = new List<FM_Additional_Detail__c>();
        for (FM_Additional_Detail__c detail : [
                SELECT  Id
                        , Resident_Case__c
                        , Emergency_Contact_Case__c
                        , Vehicle_Case__c
                FROM    FM_Additional_Detail__c
                WHERE   Vehicle_Case__c IN :lstSr
                        OR Resident_Case__c IN :lstSr
                        OR Emergency_Contact_Case__c IN :lstSr
        ]) {
            if (detail.Vehicle_Case__c != NULL) {
                detail.Vehicle_Account__c = srDetails.get(detail.Vehicle_Case__c).Tenant__c;
            } else if (detail.Resident_Case__c != NULL) {
                detail.Resident_Account__c = srDetails.get(detail.Resident_Case__c).Tenant__c;
            } else if (detail.Emergency_Contact_Case__c != NULL) {
                detail.Emergency_Contact_Account__c = srDetails.get(detail.Emergency_Contact_Case__c).Tenant__c;
            }
            lstAdditionalDetails.add(detail);
        }
        Database.update(lstAdditionalDetails, false);
    }

    public static void recallApprovals( list<FM_Case__c> lstFMCases ) {
        list<Error_Log__c> lstErrors = new list<Error_Log__c>();
        for( FM_Case__c objFMCase : lstFMCases ) {
            list<ProcessInstanceWorkitem> lstPendingWorkItems = [ SELECT Id
                                                                       , ProcessInstanceId
                                                                    FROM ProcessInstanceWorkitem
                                                                   WHERE ProcessInstance.TargetObjectId = :objFMCase.Id
                                                                     AND ProcessInstance.Status = 'Pending' ];
            system.debug( '==lstPendingWorkItems==' +lstPendingWorkItems );
            if( !lstPendingWorkItems.isEmpty() ) {
                List<ProcessInstanceStep> lstProcessInsStep = [ SELECT Comments
                                                         FROM ProcessInstanceStep
                                                        WHERE ProcessInstanceId = :lstPendingWorkItems[0].ProcessInstanceId];
                system.debug( '==lstProcessInsStep==' +lstProcessInsStep );
                if (!lstProcessInsStep.isEmpty()) {
                    for (ProcessInstanceStep processInsStep : lstProcessInsStep) {
                        if(processInsStep.Comments != NULL){
                            if (processInsStep.Comments.split('-').size()>1 && processInsStep.Comments.split('-')[1] == 'FM Manager' ){
                                objFMCase.Property_Manager_Email__c = '';
                            } else {
                                objFMCase.FM_Manager_Email__c = '';
                            }
                        }
                    }
                }
                Approval.ProcessWorkitemRequest objWorkItem = new Approval.ProcessWorkitemRequest();
                objWorkItem.setAction( 'Removed' );
                objWorkItem.setWorkItemId( lstPendingWorkItems[0].Id );
                try {
                    Approval.ProcessResult result = Approval.process( objWorkItem );
                    objFMCase.Recall_Approvals__c = false ;
                }
                catch( Exception e ) {
                    lstErrors.add( FM_Utility.createErrorLog( NULL, NULL, objFMCase.Id, 'Tenant Registration', e.getMessage() ) );
                }
            }
        }
        //update lstFMCases;
        Database.SaveResult[] srFmList = Database.Update(lstFMCases, false);
        for (Database.SaveResult sr : srFmList) {
            if (!sr.isSuccess()) {
                for(Database.Error errMsg : sr.getErrors() ) {
                    lstErrors.add( FM_Utility.createErrorLog( NULL, NULL, NULL, 'Tenant Registration Batch', errMsg.getMessage() ) );
                } 
            }
        }

        insert lstErrors;
    }   

    global void execute(SchedulableContext sc) {
        FmcTenantRegistrationAccountBatch tenantAccountCreationBatch = new FmcTenantRegistrationAccountBatch();
        Database.executeBatch(tenantAccountCreationBatch, 5);
    }

}