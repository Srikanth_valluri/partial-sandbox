@isTest
private class HDApp_createFMReceipt_APITest {

    @isTest 
    private static void createFmReceiptTest1(){
        
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];

        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Customer Community Login User(Use this)%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              //ContactId = contact1.Id,
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
        Database.insert(user1);

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'Test Unit Name',
                                      Registration_ID__c = '3901');
        insert bookingUnit;

        String jsonString =   '   {  '  + 
        '       "payment_details" : {  '  + 
        '           "is_guest_payment" : false,  '  + 
        '           "account_id" : "'+account.Id+'",  '  + 
        '           "reg_id" : "3901",  '  + 
        '           "payment_type" : "Service Charges",  '  + 
        '           "other_payment_type" : "",  '  + 
        '           "amount" : 10,  '  + 
        '           "bank_acknowledgement_id" : "123",  '  + 
        '           "bank_receipt_number" : "034121423847",  '  + 
        '           "bank_reference_number" : "249529",  '  + 
        '           "billing_address" : "dubai , deira",  '  + 
        '           "billing_city" : "dubai",  '  + 
        '           "billing_country" : "United Arab Emirates",  '  + 
        '           "billing_email" : "test@test.com",  '  + 
        '           "billing_name" : "test test",  '  + 
        '           "billing_phone" : "2132124",  '  + 
        '           "billing_state" : "dubai",  '  + 
        '           "billing_zip" : "000001",  '  + 
        '           "card_holder_name" : "Test",  '  + 
        '           "card_name" : "Test",  '  + 
        '           "payment_currency" : "AED",  '  + 
        '           "eci_value" : "05",  '  + 
        '           "order_number" : "FM-RECEIPT-058901",  '  + 
        '           "order_status" : "Success",  '  + 
        '           "payment_mode" : "Credit card",  '  + 
        '           "ipms_receipt_url" : "https://ptctest.damacgroup.com/COFFEE/apex/document/view/8c4b07e5084c9d9f894734593dc9b568",  '  + 
        '           "ipms_receipt_number" : "12354",  '  + 
        '           "status_code" : "00",  '  + 
        '           "status_message" : "Approved",  '  + 
        '           "tracking_id" : "109016213169",  '  + 
        '           "failure_message" : "",  '  + 
        '           "bank_qsi_number" : "30000017771",  '  + 
        '           "merchant_amount" : 10  '  + 
        '       },  '  + 
        '       "paid_invoice_details" :[  '  + 
        '           {  '  + 
        '               "invoice_number" : "7870423",  '  + 
        '               "call_type" : "Master Community Levy",  '  + 
        '               "creation_date" : "2020-03-03",  '  + 
        '               "due_date" : "2020-03-31",  '  + 
        '               "party_id" : "63062",  '  + 
        '               "party_name" : "Test Test",  '  + 
        '               "due_remaining" : 12.00,  '  + 
        '               "project_name" : "Test",  '  + 
        '               "registration_id" : "3901",  '  + 
        '               "trx_number" : "242",  '  + 
        '               "trx_type" : "INV",  '  + 
        '               "trx_id" : "2124",  '  + 
        '               "unit_name" : "BD4/10/1001"  '  + 
        '           },  '  + 
        '           {  '  + 
        '               "invoice_number" : "7870424",  '  + 
        '               "call_type" : "Master Community Levy",  '  + 
        '               "creation_date" : "2020-03-03",  '  + 
        '               "due_date" : "2020-03-31",  '  + 
        '               "party_id" : "63062",  '  + 
        '               "party_name" : "Test Test",  '  + 
        '               "due_remaining" : 25.00,  '  + 
        '               "project_name" : "Test",  '  + 
        '               "registration_id" : "3901",  '  + 
        '               "trx_number" : "242",  '  + 
        '               "trx_type" : "INV",  '  + 
        '               "trx_id" : "2124",  '  + 
        '               "unit_name" : "BD4/10/1001"  '  + 
        '           }  '  + 
        '       ]  '  + 
        '  }  '; 

        Test.startTest();
        
            Credentials_Details__c settings = new Credentials_Details__c();
            settings.Name = 'office365RestServices';
            settings.User_Name__c = 'Some Value';
            settings.Password__c = 'Some Value';
            settings.Resource__c = 'Some Value';
            settings.grant_type__c = 'Some Value';
            insert settings;
            //Set mock response
            Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
            
            /*String blobStr = 'Test Blob Response';
            FmHttpCalloutMock.Response blobResponse = new FmHttpCalloutMock.Response(200, 'Success', blobStr);
            
            Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
                'https://ptctest.damacgroup.com/COFFEE/apex/document/view/8c4b07e5084c9d9f894734593dc9b568' => blobResponse
            };

            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));*/

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/createFmReceipt';  
            req.requestBody = Blob.valueOf(jsonString);
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;

            HDApp_createFMReceipt_API.createFmReceipt();
        Test.stopTest();
    }

    @isTest 
    private static void createFmReceiptTestNoAccId() {

        String jsonString =  '   {  '  + 
        '       "payment_details" : {  '  + 
        '           "is_guest_payment" : false,  '  + 
        '           "account_id" : "",  '  + 
        '           "reg_id" : "3901",  '  + 
        '           "payment_type" : "Service Charges",  '  + 
        '           "other_payment_type" : "",  '  + 
        '           "amount" : 10,  '  + 
        '           "bank_acknowledgement_id" : "123",  '  + 
        '           "bank_receipt_number" : "034121423847",  '  + 
        '           "bank_reference_number" : "249529",  '  + 
        '           "billing_address" : "dubai , deira",  '  + 
        '           "billing_city" : "dubai",  '  + 
        '           "billing_country" : "United Arab Emirates",  '  + 
        '           "billing_email" : "test@test.com",  '  + 
        '           "billing_name" : "test test",  '  + 
        '           "billing_phone" : "2132124",  '  + 
        '           "billing_state" : "dubai",  '  + 
        '           "billing_zip" : "000001",  '  + 
        '           "card_holder_name" : "Test",  '  + 
        '           "card_name" : "Test",  '  + 
        '           "payment_currency" : "AED",  '  + 
        '           "eci_value" : "05",  '  + 
        '           "order_number" : "FM-RECEIPT-058901",  '  + 
        '           "order_status" : "Success",  '  + 
        '           "payment_mode" : "Credit card",  '  + 
        '           "ipms_receipt_url" : "test@payment.com",  '  + 
        '           "ipms_receipt_number" : "12354",  '  + 
        '           "status_code" : "00",  '  + 
        '           "status_message" : "Approved",  '  + 
        '           "tracking_id" : "109016213169",  '  + 
        '           "failure_message" : "",  '  + 
        '           "bank_qsi_number" : "30000017771",  '  + 
        '           "merchant_amount" : 10  '  + 
        '       },  '  + 
        '       "paid_invoice_details" :[  '  + 
        '           {  '  + 
        '               "invoice_number" : "7870423",  '  + 
        '               "call_type" : "Master Community Levy",  '  + 
        '               "creation_date" : "2020-03-03",  '  + 
        '               "due_date" : "2020-03-31",  '  + 
        '               "party_id" : "63062",  '  + 
        '               "party_name" : "Test Test",  '  + 
        '               "due_remaining" : 12.00,  '  + 
        '               "project_name" : "Test",  '  + 
        '               "registration_id" : "3901",  '  + 
        '               "trx_number" : "242",  '  + 
        '               "trx_type" : "INV",  '  + 
        '               "trx_id" : "2124",  '  + 
        '               "unit_name" : "BD4/10/1001"  '  + 
        '           },  '  + 
        '           {  '  + 
        '               "invoice_number" : "7870424",  '  + 
        '               "call_type" : "Master Community Levy",  '  + 
        '               "creation_date" : "2020-03-03",  '  + 
        '               "due_date" : "2020-03-31",  '  + 
        '               "party_id" : "63062",  '  + 
        '               "party_name" : "Test Test",  '  + 
        '               "due_remaining" : 25.00,  '  + 
        '               "project_name" : "Test",  '  + 
        '               "registration_id" : "3901",  '  + 
        '               "trx_number" : "242",  '  + 
        '               "trx_type" : "INV",  '  + 
        '               "trx_id" : "2124",  '  + 
        '               "unit_name" : "BD4/10/1001"  '  + 
        '           }  '  + 
        '       ]  '  + 
        '  }  ';

        Test.startTest();

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/createFmReceipt';  
            req.requestBody = Blob.valueOf(jsonString);
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;

            HDApp_createFMReceipt_API.createFmReceipt();
        Test.stopTest();
    }

    @isTest 
    private static void createFmReceiptTestNoAccParam() {

        String jsonString =  '   {  '  + 
        '       "payment_details" : {  '  + 
        '           "is_guest_payment" : false,  '  + 
        '           "reg_id" : "3901",  '  + 
        '           "payment_type" : "Service Charges",  '  + 
        '           "other_payment_type" : "",  '  + 
        '           "amount" : 10,  '  + 
        '           "bank_acknowledgement_id" : "123",  '  + 
        '           "bank_receipt_number" : "034121423847",  '  + 
        '           "bank_reference_number" : "249529",  '  + 
        '           "billing_address" : "dubai , deira",  '  + 
        '           "billing_city" : "dubai",  '  + 
        '           "billing_country" : "United Arab Emirates",  '  + 
        '           "billing_email" : "test@test.com",  '  + 
        '           "billing_name" : "test test",  '  + 
        '           "billing_phone" : "2132124",  '  + 
        '           "billing_state" : "dubai",  '  + 
        '           "billing_zip" : "000001",  '  + 
        '           "card_holder_name" : "Test",  '  + 
        '           "card_name" : "Test",  '  + 
        '           "payment_currency" : "AED",  '  + 
        '           "eci_value" : "05",  '  + 
        '           "order_number" : "FM-RECEIPT-058901",  '  + 
        '           "order_status" : "Success",  '  + 
        '           "payment_mode" : "Credit card",  '  + 
        '           "ipms_receipt_url" : "test@payment.com",  '  + 
        '           "ipms_receipt_number" : "12354",  '  + 
        '           "status_code" : "00",  '  + 
        '           "status_message" : "Approved",  '  + 
        '           "tracking_id" : "109016213169",  '  + 
        '           "failure_message" : "",  '  + 
        '           "bank_qsi_number" : "30000017771",  '  + 
        '           "merchant_amount" : 10  '  + 
        '       },  '  + 
        '       "paid_invoice_details" :[  '  + 
        '           {  '  + 
        '               "invoice_number" : "7870423",  '  + 
        '               "call_type" : "Master Community Levy",  '  + 
        '               "creation_date" : "2020-03-03",  '  + 
        '               "due_date" : "2020-03-31",  '  + 
        '               "party_id" : "63062",  '  + 
        '               "party_name" : "Test Test",  '  + 
        '               "due_remaining" : 12.00,  '  + 
        '               "project_name" : "Test",  '  + 
        '               "registration_id" : "3901",  '  + 
        '               "trx_number" : "242",  '  + 
        '               "trx_type" : "INV",  '  + 
        '               "trx_id" : "2124",  '  + 
        '               "unit_name" : "BD4/10/1001"  '  + 
        '           },  '  + 
        '           {  '  + 
        '               "invoice_number" : "7870424",  '  + 
        '               "call_type" : "Master Community Levy",  '  + 
        '               "creation_date" : "2020-03-03",  '  + 
        '               "due_date" : "2020-03-31",  '  + 
        '               "party_id" : "63062",  '  + 
        '               "party_name" : "Test Test",  '  + 
        '               "due_remaining" : 25.00,  '  + 
        '               "project_name" : "Test",  '  + 
        '               "registration_id" : "3901",  '  + 
        '               "trx_number" : "242",  '  + 
        '               "trx_type" : "INV",  '  + 
        '               "trx_id" : "2124",  '  + 
        '               "unit_name" : "BD4/10/1001"  '  + 
        '           }  '  + 
        '       ]  '  + 
        '  }  '; 

        Test.startTest();

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/createFmReceipt';  
            req.requestBody = Blob.valueOf(jsonString);
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;

            HDApp_createFMReceipt_API.createFmReceipt();
        Test.stopTest();
    }

    @isTest 
    private static void createFmReceiptTestNoRegParam() {

        String jsonString =  '   {  '  + 
        '       "payment_details" : {  '  + 
        '           "is_guest_payment" : true,  '  + 
        '           "payment_type" : "Service Charges",  '  + 
        '           "other_payment_type" : "",  '  + 
        '           "amount" : 10,  '  + 
        '           "bank_acknowledgement_id" : "123",  '  + 
        '           "bank_receipt_number" : "034121423847",  '  + 
        '           "bank_reference_number" : "249529",  '  + 
        '           "billing_address" : "dubai , deira",  '  + 
        '           "billing_city" : "dubai",  '  + 
        '           "billing_country" : "United Arab Emirates",  '  + 
        '           "billing_email" : "test@test.com",  '  + 
        '           "billing_name" : "test test",  '  + 
        '           "billing_phone" : "2132124",  '  + 
        '           "billing_state" : "dubai",  '  + 
        '           "billing_zip" : "000001",  '  + 
        '           "card_holder_name" : "Test",  '  + 
        '           "card_name" : "Test",  '  + 
        '           "payment_currency" : "AED",  '  + 
        '           "eci_value" : "05",  '  + 
        '           "order_number" : "FM-RECEIPT-058901",  '  + 
        '           "order_status" : "Success",  '  + 
        '           "payment_mode" : "Credit card",  '  + 
        '           "ipms_receipt_url" : "test@payment.com",  '  + 
        '           "ipms_receipt_number" : "12354",  '  + 
        '           "status_code" : "00",  '  + 
        '           "status_message" : "Approved",  '  + 
        '           "tracking_id" : "109016213169",  '  + 
        '           "failure_message" : "",  '  + 
        '           "bank_qsi_number" : "30000017771",  '  + 
        '           "merchant_amount" : 10  '  + 
        '       },  '  + 
        '       "paid_invoice_details" :[  '  + 
        '           {  '  + 
        '               "invoice_number" : "7870423",  '  + 
        '               "call_type" : "Master Community Levy",  '  + 
        '               "creation_date" : "2020-03-03",  '  + 
        '               "due_date" : "2020-03-31",  '  + 
        '               "party_id" : "63062",  '  + 
        '               "party_name" : "Test Test",  '  + 
        '               "due_remaining" : 12.00,  '  + 
        '               "project_name" : "Test",  '  + 
        '               "registration_id" : "3901",  '  + 
        '               "trx_number" : "242",  '  + 
        '               "trx_type" : "INV",  '  + 
        '               "trx_id" : "2124",  '  + 
        '               "unit_name" : "BD4/10/1001"  '  + 
        '           },  '  + 
        '           {  '  + 
        '               "invoice_number" : "7870424",  '  + 
        '               "call_type" : "Master Community Levy",  '  + 
        '               "creation_date" : "2020-03-03",  '  + 
        '               "due_date" : "2020-03-31",  '  + 
        '               "party_id" : "63062",  '  + 
        '               "party_name" : "Test Test",  '  + 
        '               "due_remaining" : 25.00,  '  + 
        '               "project_name" : "Test",  '  + 
        '               "registration_id" : "3901",  '  + 
        '               "trx_number" : "242",  '  + 
        '               "trx_type" : "INV",  '  + 
        '               "trx_id" : "2124",  '  + 
        '               "unit_name" : "BD4/10/1001"  '  + 
        '           }  '  + 
        '       ]  '  + 
        '  }  '; 

        Test.startTest();

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/createFmReceipt';  
            req.requestBody = Blob.valueOf(jsonString);
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;

            HDApp_createFMReceipt_API.createFmReceipt();
        Test.stopTest();
    }

    @isTest 
    private static void createFmReceiptTestNoRegParamVal() {

        String jsonString =  '   {  '  + 
        '       "payment_details" : {  '  + 
        '           "is_guest_payment" : true,  '  + 
        '           "reg_id" : "",  '  + 
        '           "payment_type" : "Service Charges",  '  + 
        '           "other_payment_type" : "",  '  + 
        '           "amount" : 10,  '  + 
        '           "bank_acknowledgement_id" : "123",  '  + 
        '           "bank_receipt_number" : "034121423847",  '  + 
        '           "bank_reference_number" : "249529",  '  + 
        '           "billing_address" : "dubai , deira",  '  + 
        '           "billing_city" : "dubai",  '  + 
        '           "billing_country" : "United Arab Emirates",  '  + 
        '           "billing_email" : "test@test.com",  '  + 
        '           "billing_name" : "test test",  '  + 
        '           "billing_phone" : "2132124",  '  + 
        '           "billing_state" : "dubai",  '  + 
        '           "billing_zip" : "000001",  '  + 
        '           "card_holder_name" : "Test",  '  + 
        '           "card_name" : "Test",  '  + 
        '           "payment_currency" : "AED",  '  + 
        '           "eci_value" : "05",  '  + 
        '           "order_number" : "FM-RECEIPT-058901",  '  + 
        '           "order_status" : "Success",  '  + 
        '           "payment_mode" : "Credit card",  '  + 
        '           "ipms_receipt_url" : "test@payment.com",  '  + 
        '           "ipms_receipt_number" : "12354",  '  + 
        '           "status_code" : "00",  '  + 
        '           "status_message" : "Approved",  '  + 
        '           "tracking_id" : "109016213169",  '  + 
        '           "failure_message" : "",  '  + 
        '           "bank_qsi_number" : "30000017771",  '  + 
        '           "merchant_amount" : 10  '  + 
        '       },  '  + 
        '       "paid_invoice_details" :[  '  + 
        '           {  '  + 
        '               "invoice_number" : "7870423",  '  + 
        '               "call_type" : "Master Community Levy",  '  + 
        '               "creation_date" : "2020-03-03",  '  + 
        '               "due_date" : "2020-03-31",  '  + 
        '               "party_id" : "63062",  '  + 
        '               "party_name" : "Test Test",  '  + 
        '               "due_remaining" : 12.00,  '  + 
        '               "project_name" : "Test",  '  + 
        '               "registration_id" : "3901",  '  + 
        '               "trx_number" : "242",  '  + 
        '               "trx_type" : "INV",  '  + 
        '               "trx_id" : "2124",  '  + 
        '               "unit_name" : "BD4/10/1001"  '  + 
        '           },  '  + 
        '           {  '  + 
        '               "invoice_number" : "7870424",  '  + 
        '               "call_type" : "Master Community Levy",  '  + 
        '               "creation_date" : "2020-03-03",  '  + 
        '               "due_date" : "2020-03-31",  '  + 
        '               "party_id" : "63062",  '  + 
        '               "party_name" : "Test Test",  '  + 
        '               "due_remaining" : 25.00,  '  + 
        '               "project_name" : "Test",  '  + 
        '               "registration_id" : "3901",  '  + 
        '               "trx_number" : "242",  '  + 
        '               "trx_type" : "INV",  '  + 
        '               "trx_id" : "2124",  '  + 
        '               "unit_name" : "BD4/10/1001"  '  + 
        '           }  '  + 
        '       ]  '  + 
        '  }  '; 

        Test.startTest();

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/createFmReceipt';  
            req.requestBody = Blob.valueOf(jsonString);
            req.httpMethod = 'POST';
            RestContext.request = req;
            RestContext.response = res;

            HDApp_createFMReceipt_API.createFmReceipt();
        Test.stopTest();
    }

}