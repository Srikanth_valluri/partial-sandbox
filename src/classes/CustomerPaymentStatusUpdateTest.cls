@isTest(seeAllData=false)
public class CustomerPaymentStatusUpdateTest {
    static testMethod void myUnitTest2() {
        PageReference pageRef = Page.PaymentStatusUpdate;
        Test.setCurrentPageReference(pageRef);
        Receipt__c R = new Receipt__c();
        R.Amount__c=100;
        R.Receipt_Type__c='Card';
        R.Transaction_Date__c=system.now();
        Id rt=Schema.SObjectType.Receipt__c.getRecordTypeInfosByName().get('Card').getRecordTypeId();
        R.RecordTypeId=rt;
        insert R;
        Test.starttest();
        Receipt__c newR =[select id,name,Payment_Gateway_Ref_No__c,Amount__c,Card_Amount__c from Receipt__c where Receipt_Type__c= 'Card' limit 1];
        String message = newR.name+'|1001312000011127|AED|1000.00|SUCCESS|5450545351321|CC|VISA|00000|No Error|01|Enrolled|07|120405|ACCEPT|No Fraud|YES|350.50|USD||||||';
            String keyval= Label.Encryption_Key;
            Blob cryptoKey = EncodingUtil.base64Decode(keyval);
            Blob iv = Blob.valueof('0123456789abcdef');
            Blob data = Blob.valueOf(message);
            Blob encryptedData = Crypto.encrypt('AES256', cryptoKey,iv, data);
            system.debug('EEEE=>'+encryptedData) ;
            String encryptedMessage  = EncodingUtil.base64Encode(encryptedData);
            System.debug('EEEESSSS=>'+encryptedMessage);

        Test.setMock(HttpCalloutMock.class, new IpmsReceiptMock(newR.Id));

        ApexPages.CurrentPage().getparameters().put('responseParameter',encryptedMessage);
        CustomerPaymentStatusUpdate controllerCls = new CustomerPaymentStatusUpdate();
        controllerCls.updateReceiptMethod();
        controllerCls.checkReceiptPaymentStatus();
        Test.stoptest();
    }

     static testMethod void myUnitTest1() {
        PageReference pageRef = Page.PaymentStatusUpdate;
        Test.setCurrentPageReference(pageRef);
        Receipt__c R = new Receipt__c();
        R.Amount__c=100;
        R.Receipt_Type__c='Card';
        R.Transaction_Date__c=system.now();
        Id rt=Schema.SObjectType.Receipt__c.getRecordTypeInfosByName().get('Card').getRecordTypeId();
        R.RecordTypeId=rt;
        insert R;
        PaymentGateway__c cstmObj = new PaymentGateway__c();
        cstmObj.Name = 'Customer Portal';
        cstmObj.AccessCode__c = 'AVRN02FD76AW13NRWA';
        cstmObj.EncryptionKey__c = '793AA04215256A5D8F6DBDE595FAA526';
        cstmObj.MerchantId__c = '43560';
        cstmObj.Url__c = 'https://secure.ccavenue.ae/transaction/transaction.do ';
        insert cstmObj;

        Test.starttest();
        list<PaymentGateway__c> lstCustmSettng = new list<PaymentGateway__c>();
        lstCustmSettng = [SELECT AccessCode__c, EncryptionKey__c, MerchantId__c,Url__c 
                          FROM PaymentGateway__c
                          WHERE Name = 'Customer Portal'];
        system.debug('>>>>lstCustmSettng>>>>'+lstCustmSettng);
        Receipt__c newR =[select id,name,Payment_Gateway_Ref_No__c,Amount__c,Card_Amount__c from Receipt__c where Receipt_Type__c= 'Card' limit 1];
        System.debug('newR>>>>' + newR);
        System.debug('newR.Name>>>>' + newR.Name);
        String message = 'tid=RECP-0683&merchant_id=43560&order_id=RECP-0683&amount=657.60&currency=AED&redirect_url=https://crmuatpro-servicecloudtrial-155c0807bf-1580afc5db1.cs83.force.com/Customer/CustomerPaymentStatusUpdate&cancel_url=https://crmuatpro-servicecloudtrial-155c0807bf-1580afc5db1.cs83.force.com/Customer/CustomerPaymentStatusUpdate&billing_name=MOHAMED KHALIFA&billing_address=New Cairo, Cairo City, First Settlement,&billing_city=New Cairo&billing_state=NA&billing_zip=0&billing_country=EG';
            String keyval= lstCustmSettng[0].EncryptionKey__c;
            Blob cryptoKey = Blob.valueOf(keyval);
            Blob hash = Crypto.generateDigest('MD5', cryptoKey );
            Blob data = Blob.valueOf(message);
            Blob encryptedData = Crypto.encryptWithManagedIV('AES128', hash , data);
            system.debug('EEEE=>'+encryptedData) ;
            String encryptedMessage = EncodingUtil.convertToHex(encryptedData );
            System.debug('EEEESSSS=>'+encryptedMessage);
            String orderNo = newR.Name;
        Test.setMock(HttpCalloutMock.class, new IpmsReceiptMock(newR.Id));

        ApexPages.CurrentPage().getparameters().put('encResp',encryptedMessage);
        ApexPages.CurrentPage().getparameters().put('orderNo',orderNo);
        CustomerPaymentStatusUpdate controllerCls = new CustomerPaymentStatusUpdate();
        //controllerCls.paramMap = '74&tracking_id=107005237866&bank_ref_no=null&order_status=Failure&failure_message=&payment_mode=Credit Card&card_name=MasterCard&status_code=null&status_message=Unspecified Failure&currency=AED&amount=657.6&billing_name=MOHAMED KHALIFA&billing_address=New Cairo, Cairo City, First Settlement&billing_city=New Cairo&billing_state=NA&billing_zip=0&billing_country=Afghanistan&billing_tel=0000000&billing_email=kanu@mailinator.com&delivery_name=&delivery_address=&delivery_city=&delivery_state=&delivery_zip=&delivery_country=&delivery_tel=&merchant_param1=&merchant_param2=&merchant_param3=&merchant_param4=&merchant_param5=&vault=N&offer_type=null&offer_code=null&discount_value=0.0&mer_amount=657.6&eci_value=05&card_holder_name=kanu&bank_receipt_no=null&merchant_param6=512345234';
        controllerCls.updateReceipt();
        controllerCls.checkReceiptPaymentStatus();
        Test.stoptest();
    }

    public class IpmsReceiptMock implements HttpCalloutMock {

        public String receiptId;

        public IpmsReceiptMock(String pReceiptId) {
            receiptId = pReceiptId;
        }

        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse response = new HttpResponse();
            response.setBody(
                '<parent>' +
                    '<child1>' +
                        '<child2>' +
                            '<child3>' +
                                '<child4>' +
                                    '<PROC_STATUS>' +
                                        'S' +
                                    '</PROC_STATUS>'+
                                    '<PROC_MESSAGE>' +
                                        'SUCCESS' +
                                    '</PROC_MESSAGE>'+
                                    '<PARAM_ID>' +
                                        receiptId +
                                    '</PARAM_ID>'+
                                    '<ATTRIBUTE1>' +
                                        receiptId +
                                    '</ATTRIBUTE1>'+
                                '</child4>'+
                            '</child3>'+
                        '</child2>'+
                    '</child1>'+
                '</parent>'
            );
            return response;
        }
    }
}