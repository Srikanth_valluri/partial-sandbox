@isTest
/*
*RevisionHistory:
*VersionAuthorDateDescription.
*1.1SwapnilGholap20/11/2017InitialDraft
*/
public class WrapperBookingUnitTest{

    static testMethod void Test1(){
        WrapperBookingUnit obj  =  new WrapperBookingUnit();
        obj.decAreaVariation_SqFt = 123;
        obj.strTypeVariation = 'Test';
        obj.decOtherCharges = 123;
        obj.decPaidPercent = 123;
        obj.decPDCcoveragePercent = 123;
        obj.intNoOfTimesCusDefaulted = 123;
        obj.blnRecoveryFlag = true;
        obj.blnPriorAOPT = true;
        obj.decTotalOutstanding = 123;
        obj.decDefaultAmt = 123;
        obj.decPenaltyOverdues = 123;
        obj.decPenaltyWaivers = 123;
        obj.decConstructionStatusPercent = 123;        
        obj.intNumber = 123;
        //UnitDetailsService.BookinUnitDetailsWrapperobjIPMSDetailsWrapper
        obj.strUnit = 'Test';
        obj.strProject = 'Test';
        obj.strProjectCity = 'Test';
        obj.strBedroomType = 'Test';
        obj.strUnitType = 'Test';
        obj.strPermittedUse = 'Test';
        obj.strReady_OffPlan = 'Test';
        obj.strRentalPool = 'Test';
        obj.strDispute = 'Test';
        obj.blnDispute = true;
        obj.blnEnforcement = true;
        obj.blnLitigation = true;
        obj.blnMortage = true;
        obj.blnCourtCase = true;
        obj.blnFTLsent = true;
        obj.blnEHO = true;
        obj.strHO = 'Test';
        obj.strHandoverFlag = 'Test';
        obj.blnRentalPool = true;
        obj.blnOQOODReg = true;
        obj.strPCC = 'Test';
        obj.blnPCC = true;
        obj.blnHO = true;
        obj.strOQOODregistered = 'Test';
        obj.blnOQOODregistered = true;
        obj.strAgreementStatus = 'Test';
        obj.strUnderTermination = 'Test';
        obj.intArea = 123;
        obj.decPrice = 123;
        obj.intNoOfBookingDay = 123;
        obj.intNoOfDefaulted = 123;
        obj.intPenaltyAmount = 123;
        obj.str_URC_SPA = 'Test';
        obj.bln_URC_SPA = true;
        obj.strDefault = 'Test';
        obj.intOverdue = 123;
        obj.intRERAstatus = 123;
        obj.decRERAstatus = 123;
        obj.strDealStatus = 'Test';
        obj.strIDavailable = 'Test';
        obj.intBCC = 123;
        obj.intPrevWaivers = 123;
        obj.intTotalWaiver = 123;
        obj.strPC_AgentName = 'Test';
        obj.intConversationStatus = 123;
        obj.strDocsStatus = 'Test';
        obj.intPricePerSqFt = 123;
        obj.blnDOCOK = true;
        obj.blnDPOK = true;
        obj.decPaidPercentage = 123;
        obj.decDefaultPercentage = 123;
        obj.intTotalPenaltyCustomer = 123;
        obj.intTotalPenaltyUnit = 123;
        obj.strOriginalACD = 'Test';
        obj.strCurrentACD = 'Test';
        //Booking_Unit__cobjBookingUnit
        obj.strRegID = 'Test';
        obj.strConstructionStatus = 'Test';
        obj.Penalty_OverDues = 123;
        obj.intPenaltyOverdue = 123;
        obj.decPenaltyWaiver = 123;
        obj.decDuesServiceCharges = 123;
        obj.decLatePaymentFees = 123;
        obj.strPromotionScheme = 'Test';
        obj.strInvoicesRaised = 'Test';
        obj.decAmountPaid = 123;
        obj.strDealTeam = 'Test';
        obj.strBookingType = 'Test';
        obj.strPaymentPlan = 'Test';
        obj.strStatementofAccount = 'Test';
        obj.strUnitPlan = 'Test';
        obj.strFloorPlan = 'Test';
        obj.strFJOPDArea = 'Test';
        obj.decBuiltUpPrice = 123;
        obj.decPlotPrice = 123;
        obj.dateBookingDate = System.today();
        obj.dateAgreementDate = System.today();
        obj.blnRegistrationFeePaid = true;
        obj.strPRCStatus = 'Test';
        obj.strPenalties = 'Test';
        obj.strPriorAOPT = 'Test';
        obj.strAssociatedPC = 'Test';
        obj.strAssociatedDOS = 'Test';
        obj.strAssociatedHOS = 'Test';
        obj.strPaymentPlanId = 'Test';
        obj.strCurrentPaymentPlanType = 'Test';
        
    }
}