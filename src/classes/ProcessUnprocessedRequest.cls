public with sharing class ProcessUnprocessedRequest implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful {
    
    public final list<String> lstSupportedObject = new list<String>{ 'Case' };
    public list<SObject> lstSObjectToBeUpdated ;
    public Integer intIndex ;
    public boolean killSwitch ;
    
    public ProcessUnprocessedRequest( Integer intStartIndex, Boolean blnChainBatch ) {
        intIndex = intStartIndex ;
        killSwitch = blnChainBatch ;
        lstSObjectToBeUpdated = new list<SObject>();
    }
    
    public ProcessUnprocessedRequest() {
        intIndex = 0 ;
        killSwitch = true ;
        lstSObjectToBeUpdated = new list<SObject>();
    }
    
    public Database.Querylocator start( Database.BatchableContext bc ) {
        system.debug('== ProcessUnprocessedRequest == Start Method Called ==');
        system.debug('== ProcessUnprocessedRequest == intIndex ==' +intIndex );
        system.debug('== ProcessUnprocessedRequest == killSwitch ==' +killSwitch );
        
        if( intIndex < lstSupportedObject.size() ) {
            return Database.getQueryLocator( makeQueryString( lstSupportedObject[ intIndex ] ) );
        }
        return null ;
    }
     
    public void execute( Database.BatchableContext bc, list<SObject> lstSObject ) {
        ID jobID = System.enqueueJob(new ProcessUnprocessedRequestQueable(lstSObject));
    }
     
    public void finish( Database.BatchableContext bc ) {
        
        
        intIndex++ ;
        
        if( intIndex < lstSupportedObject.size() ) {
            system.debug('== ProcessUnprocessedRequest == Inside batch chaining ==');
            Database.executeBatch( new ProcessUnprocessedRequest( intIndex, true ), 200 );
        }
        else {
            system.debug('== ProcessUnprocessedRequest == Next Batch in 5 mins ==');
            String jobName = 'ProcessUnprocessedRequests -- ' + Datetime.now();
            intIndex = 0 ;
            if( killSwitch && !Test.isRunningTest() ) {
                system.scheduleBatch( this, jobName, 5, 200 );
            }
        }
    }
    
    public String makeQueryString( String strObjectName ) {
        return 'SELECT Id, Is_Batch_Processing_Pending__c, Status, LOA_Submitted__c FROM ' + strObjectName + ' WHERE Is_Batch_Processing_Pending__c = TRUE';
    }
}