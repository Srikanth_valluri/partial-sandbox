public with sharing class TaskTriggerHandlerMoveIn_Out {


    public static void executeBeforeUpdateTrigger(List<Task> scope) {
        String fmCasePrefix = DamacUtility.getObjectPrefix('FM_Case__c');
        Set<Id> fmCaseIDs = new Set<Id>();
        List<Task> lstFMCaseTasks = new List<Task>();
        for(Task thisTask : scope){
            if(String.isNotBlank(thisTask.Status)
                && thisTask.WHATID != NULL
                && (thisTask.Status == 'Completed' || thisTask.Status == 'Closed') 
                && String.valueOf(thisTask.WHATID).startsWith(fmCasePrefix)) {
                fmCaseIDs.add(thisTask.WHATID);
                lstFMCaseTasks.add(thisTask);
            }
        }
        if(lstFMCaseTasks.size() >0 && fmCaseIDs.size() >0) {
            validationRuleOnTaskClosure(fmCaseIDs, lstFMCaseTasks);
        }
    }

    public static void validationRuleOnTaskClosure (Set<Id> fmCaseIDs,List<Task> lstFMCaseTasks){
        Set<Id> moveInCases = new Set<Id>();
        Set<Id> moveOutCases = new Set<Id>();
        for(FM_Case__c obj :  [SELECT Id, Actual_move_in_date__c, RecordType.DeveloperName,
                                        Actual_move_out_date__c
                    FROM FM_Case__c
                    WHERE Id IN: fmCaseIDs
                    AND (Actual_move_in_date__c = NULL OR Actual_move_out_date__c = NULL)]) {
            if( ( obj.RecordType.DeveloperName == 'Move_In' || obj.RecordType.DeveloperName == 'Tenant_Registration' ) &&
                  obj.Actual_move_in_date__c == NULL) {
                moveInCases.add(obj.Id);
            }
            else if(obj.RecordType.DeveloperName == 'Move_Out' && obj.Actual_move_out_date__c == NULL) {
                moveOutCases.add(obj.Id);
            }
        }
        for(Task obj : lstFMCaseTasks) {
            if(moveInCases.contains(obj.WhatId) && obj.Subject == Label.Move_in_date_task
                && !Test.isRunningTest()) {
                obj.addError('Please update actual move-in date on FM Case before closing the task');
            }
            else if(moveOutCases.contains(obj.WhatId) && obj.Subject == Label.Update_actual_move_out_date
                && !Test.isRunningTest()) {
                obj.addError('Please update actual move-out date on FM Case before closing the task');
            }
        }
     }

     public static void checkEmailPhoneOnCallingList(List<Task> lstTask) {

        for(Task objTask : lstTask) {
            if(objTask.Status == 'Completed' && objTask.Subject == 'Incorrect Contact Details' && objTask.Process_Name__c == 'Welcome Call' && objTask.WhatId != Null ) {

                String objName = objTask.WhatId.getSObjectType().getDescribe().getName();
                System.debug('objName:: '+objName);

                if(objName != 'Calling_List__c') {
                    continue;
                }

                List<Calling_List__c> lstCallinglst = [SELECT Id
                                                            , RecordtypeId
                                                            , RecordType.Name
                                                            , Email_1__c
                                                            , Phone_1__c 
                                                       FROM Calling_List__c 
                                                       Where Id =: objTask.WhatId];

               
                if(lstCallinglst.size() > 0) {
                    String recTypeName = Schema.getGlobalDescribe().get('Calling_List__c').getDescribe().getRecordTypeInfosById().get(lstCallinglst[0].RecordtypeId).getName();
                    System.debug('recTypeName :: '+recTypeName);

                    if(recTypeName == 'COD for Sales' && String.isBlank(lstCallinglst[0].Email_1__c) && String.isBlank(lstCallinglst[0].Phone_1__c) ) {
                        objTask.addError('Please update Phone/Email on calling list before closing the task!!');
                    }
                }//End of If(callingLst size)
            }
        }//End of For(objTask);
     }
}