@isTest
public class API_CommunityPortalTest {
    @testSetup static void setup () {
        Account adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
        adminAccount.Agency_Type__c = 'Individual';
        adminAccount.Party_ID__c = '1234';
        insert adminAccount;
        
        Contact adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
        adminContact.Owner__c = true;
        adminContact.Agent_Representative__c= true;
        adminContact.Portal_Administrator__c= true;
        adminContact.FirstName = 'test1';
        adminContact.LastName = 'test2';
        insert adminContact;
        
        User portalUser = InitialiseTestData.getPortalUser('agentEmail@damacgroup.com.fullcopy', adminContact.Id, 'Admin');
        insert portalUser;

    }
    
    @isTest static void login() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/login';  
        req.addParameter('username', 'agentEmail@damacgroup.com.fullcopy');
        req.addParameter ('password', 'test@123');
        req.addParameter ('domain', 'test.salesforce.com');
        req.httpMethod = 'Post';
        RestContext.request = req;
        RestContext.response = res;

        FmIpmsRestServices.DueInvoicesResponse invoicesResponse = new FmIpmsRestServices.DueInvoicesResponse();
        invoicesResponse.OutputParameters = new FmIpmsRestServices.OutputParameters();
        invoicesResponse.OutputParameters.X_RETURN_STATUS = 'S';
        invoicesResponse.OutputParameters.X_RETURN_MESSAGE = 'Total Records Fetched...1';
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE = new FmIpmsRestServices.ResponseMessage();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM = new List<Map<String, String>>();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM.add(
            new Map<String, String> {
                'ATTRIBUTE1' => 'registrationId',
                'ATTRIBUTE2' => 'unitName',
                'ATTRIBUTE3' => 'projectName',
                'ATTRIBUTE4' => 'customerId',
                'ATTRIBUTE5' => 'orgId',
                'ATTRIBUTE6' => 'partyId',
                'ATTRIBUTE7' => 'partyName',
                'ATTRIBUTE8' => 'trxNumber',
                'ATTRIBUTE9' => 'creationDate',
                'ATTRIBUTE10' => 'callType',
                'ATTRIBUTE11' => '50',
                'ATTRIBUTE12' => '100',
                'ATTRIBUTE13' => '50',
                'ATTRIBUTE14' => FmcUtils.formatAsIpmsDate(Date.today()),
                'ATTRIBUTE15' => 'trxType',
                'ATTRIBUTE16' => '0'
            }
        );
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(invoicesResponse)));
        
        API_CommunityPortalLogin.login();
    
    }
    
    @isTest static void login2() {

        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Name = 'JNU';
        locObj.Building_Name__c = 'Janusia';
        insert locObj;

        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id,
                                                          Registration_ID__c = '3901',
                                                          Inventory__c = invObj.Id,
                                                          Unit_Name__c='JNU/SD168/XH2910B',
                                                          Resident__c = account.id,
                                                          Registration_Status__c = 'Agreement executed by DAMAC',
                                                          Handover_Flag__c = 'Y',
                                                          Dummy_Booking_Unit__c = true );
        insert bookingUnit;

        FM_Case__c fmCase = new FM_Case__c(Request_Type_DeveloperName__c = 'Move_in_Request',
                                           RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Move In').getRecordTypeId(),
                                           Request_Type__c = 'Move In',
                                           Origin__c = 'Portal',
                                           Booking_Unit__c = bookingUnit.Id,
                                           Status__c = 'Draft Request');
        insert fmCase;

        Property__c propObj = TestDataFactoryFM.createProperty();
        insert propObj;

        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;

        Resource__c objRes = new Resource__c();
        objRes.Chargeable_Fee_Slot__c = 10;
        objRes.Non_Operational_Days__c = 'Sunday';
        objRes.Resource_Type__c = 'FM Amenity';
        objRes.No_of_advance_booking_days__c = 2;
        objRes.Resource_Type__c = 'FM Amenity';
        objRes.Deposit__c = 3;
        objRes.Chargeable_Fee_Slot__c = 5;
        objRes.No_of_deposit_due_days__c = 1;
        objRes.Contact_Person_Email__c = 'test@test.com';   //
        objRes.Contact_Person_Name__c = 'Test'; //
        objRes.Contact_Person_Phone__c = '1234567890';  //
        insert objRes;

        Resource_Sharing__c objRS= new Resource_Sharing__c();
        objRS.Building__c = locObj.Id;
        objRS.Resource__c = objRes.Id;
        insert objRS;

        Resource_Slot__c objResSlot1 = new Resource_Slot__c();
        objResSlot1.Resource__c = objRes.Id;
        objResSlot1.Start_Date__c = Date.Today();
        objResSlot1.End_Date__c = Date.Today().addDays(30);
        objResSlot1.Start_Time_p__c = '09:00';
        objResSlot1.End_Time_p__c = '09:30';
        insert objResSlot1;

        Non_operational_days__c obj = new Non_operational_days__c();
        obj.Non_operational_date__c = Date.today();
        obj.Resource__c = objRes.Id;
        insert obj;

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/login'; 
        req.addParameter('username', '1234');
        req.addParameter ('password', 'test@123');
        req.addParameter ('domain', 'test.salesforce.com');
        req.httpMethod = 'Post';
        RestContext.request = req;
        RestContext.response = res;

        FmIpmsRestServices.DueInvoicesResponse invoicesResponse = new FmIpmsRestServices.DueInvoicesResponse();
        invoicesResponse.OutputParameters = new FmIpmsRestServices.OutputParameters();
        invoicesResponse.OutputParameters.X_RETURN_STATUS = 'S';
        invoicesResponse.OutputParameters.X_RETURN_MESSAGE = 'Total Records Fetched...1';
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE = new FmIpmsRestServices.ResponseMessage();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM = new List<Map<String, String>>();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM.add(
            new Map<String, String> {
                'ATTRIBUTE1' => 'registrationId',
                'ATTRIBUTE2' => 'unitName',
                'ATTRIBUTE3' => 'projectName',
                'ATTRIBUTE4' => 'customerId',
                'ATTRIBUTE5' => 'orgId',
                'ATTRIBUTE6' => 'partyId',
                'ATTRIBUTE7' => 'partyName',
                'ATTRIBUTE8' => 'trxNumber',
                'ATTRIBUTE9' => 'creationDate',
                'ATTRIBUTE10' => 'callType',
                'ATTRIBUTE11' => '50',
                'ATTRIBUTE12' => '100',
                'ATTRIBUTE13' => '50',
                'ATTRIBUTE14' => FmcUtils.formatAsIpmsDate(Date.today()),
                'ATTRIBUTE15' => 'trxType',
                'ATTRIBUTE16' => '0'
            }
        );
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(invoicesResponse)));
        
        API_CommunityPortalLogin.login(); 

        List<Booking_Unit__c> lstBU = [SELECT id,
                                              Booking__c,
                                              Registration_ID__c,
                                              Inventory__c,
                                              Unit_Name__c,
                                              Resident__c,
                                              Registration_Status__c,
                                              Handover_Flag__c
                                      FROM Booking_Unit__c
                                      WHERE Registration_ID__c = '3901'];
        API_CommunityPortalLogin.getBUtoPropertyMapForAmenity(lstBU);
    
    }

    @isTest static void login3() {

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/login'; 
        req.addParameter('username', '&&&<<');
        req.addParameter ('password', 'test@123');
        req.addParameter ('domain', 'test.salesforce.com');
        req.httpMethod = 'Post';
        RestContext.request = req;
        RestContext.response = res;

        FmIpmsRestServices.DueInvoicesResponse invoicesResponse = new FmIpmsRestServices.DueInvoicesResponse();
        invoicesResponse.OutputParameters = new FmIpmsRestServices.OutputParameters();
        invoicesResponse.OutputParameters.X_RETURN_STATUS = 'S';
        invoicesResponse.OutputParameters.X_RETURN_MESSAGE = 'Total Records Fetched...1';
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE = new FmIpmsRestServices.ResponseMessage();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM = new List<Map<String, String>>();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM.add(
            new Map<String, String> {
                'ATTRIBUTE1' => 'registrationId',
                'ATTRIBUTE2' => 'unitName',
                'ATTRIBUTE3' => 'projectName',
                'ATTRIBUTE4' => 'customerId',
                'ATTRIBUTE5' => 'orgId',
                'ATTRIBUTE6' => 'partyId',
                'ATTRIBUTE7' => 'partyName',
                'ATTRIBUTE8' => 'trxNumber',
                'ATTRIBUTE9' => 'creationDate',
                'ATTRIBUTE10' => 'callType',
                'ATTRIBUTE11' => '50',
                'ATTRIBUTE12' => '100',
                'ATTRIBUTE13' => '50',
                'ATTRIBUTE14' => FmcUtils.formatAsIpmsDate(Date.today()),
                'ATTRIBUTE15' => 'trxType',
                'ATTRIBUTE16' => '0'
            }
        );
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(invoicesResponse)));
        
        API_CommunityPortalLogin.login(); 
    } 

    @isTest static void login4() {

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/login'; 
        //req.addParameter('username', '&&&<<');
        req.addParameter ('password', 'test@123');
        req.addParameter ('domain', 'test.salesforce.com');
        req.httpMethod = 'Post';
        RestContext.request = req;
        RestContext.response = res;

        FmIpmsRestServices.DueInvoicesResponse invoicesResponse = new FmIpmsRestServices.DueInvoicesResponse();
        invoicesResponse.OutputParameters = new FmIpmsRestServices.OutputParameters();
        invoicesResponse.OutputParameters.X_RETURN_STATUS = 'S';
        invoicesResponse.OutputParameters.X_RETURN_MESSAGE = 'Total Records Fetched...1';
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE = new FmIpmsRestServices.ResponseMessage();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM = new List<Map<String, String>>();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM.add(
            new Map<String, String> {
                'ATTRIBUTE1' => 'registrationId',
                'ATTRIBUTE2' => 'unitName',
                'ATTRIBUTE3' => 'projectName',
                'ATTRIBUTE4' => 'customerId',
                'ATTRIBUTE5' => 'orgId',
                'ATTRIBUTE6' => 'partyId',
                'ATTRIBUTE7' => 'partyName',
                'ATTRIBUTE8' => 'trxNumber',
                'ATTRIBUTE9' => 'creationDate',
                'ATTRIBUTE10' => 'callType',
                'ATTRIBUTE11' => '50',
                'ATTRIBUTE12' => '100',
                'ATTRIBUTE13' => '50',
                'ATTRIBUTE14' => FmcUtils.formatAsIpmsDate(Date.today()),
                'ATTRIBUTE15' => 'trxType',
                'ATTRIBUTE16' => '0'
            }
        );
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(invoicesResponse)));
        
        API_CommunityPortalLogin.login(); 
    } 
    
     @isTest static void login5() {

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;
         
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR; 
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        objBooking.Account__c = account.id;
        insert objBooking;
         
         Location__c lstLocations = new Location__c();
        lstLocations.Building_Name__c = 'test';
        lstLocations.Location_Type__c = 'Building';
        lstLocations.Location_ID__c = '12345';
        insert lstLocations;
        
        Inventory__c objInventory = new     Inventory__c();
        objInventory.Building_Location__c = lstLocations.Id;
        //objInventory.Building_Location__c = lstLocations.Location_ID__c;
        objInventory.Building_ID__c = '12345';
        objInventory.Property_Status__c = 'Ready';
        insert objInventory;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Unit_Name__c ='test';
        objBU.Property_Name__c = 'test';
        objBU.Dummy_Booking_Unit__c = True;
        objBU.Booking__c = objBooking.Id;
        objBU.Registration_Status__c = 'Agreement Cancellation Verified';
        objBU.Inventory__c = objInventory.Id;
        insert objBU;
        
        FM_Case__c objFM = new FM_Case__c();
        objFM.Origin__c = 'Portal - Guest';
        objFM.Request_Type__c = 'Tenant Registration';
        objFM.Status__c = 'Draft Request';
        objFM.Booking_Unit__c = objBU.Id;
        insert objFM;
        
               
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/login';  
        req.addParameter('username', '&&&<<');
        req.addParameter ('password', 'test@123');
        req.addParameter ('domain', '');
        req.httpMethod = 'Post';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
         API_CommunityPortalLogin.getCompletedHOSteps(objBU.Id, account.id);
        Test.stopTest();
        
    
    }
    
     @isTest static void login6() {
     
         insert new IpmsRestServices__c(
                   SetupOwnerId = UserInfo.getOrganizationId(),
                   BaseUrl__c = 'http://83.111.194.181:8045/webservices/rest',
                   Username__c = 'oracle_user',
                   Password__c = 'crp1user',
                   Client_Id__c = '8MABLQM-KJ8I8-1XA58-WWCM1S1',
                   BearerToken__c = 'eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWyiwuVrJSKs5IBKK8bCUdpczEEiUrQ1MzAwtTMwsDIx2l1IoCqIC5oRFIoLQ4tSgvMTcVqM_C19HJJ9BX19vLwtNC1zDC0dRCNzzc2dcw2FCpFgBXRb-1XQAAAA.6Ym224Vwr9AniBeq6gL8OM9u4vGnUB_vbEUVjWojg14',
                   Timeout__c = 120000
                   );
         
         NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Name = 'JNU';
        locObj.Building_Name__c = 'Janusia';
        insert locObj;

        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id,
                                                          Registration_ID__c = '3901',
                                                          Inventory__c = invObj.Id,
                                                          Unit_Name__c='JNU/SD168/XH2910B',
                                                          Resident__c = account.id,
                                                          Registration_Status__c = 'Agreement executed by DAMAC',
                                                          Handover_Flag__c = 'Y',
                                                          Dummy_Booking_Unit__c = true,
                                                          Property_Country__c = 'United Arab Emirates' );
        insert bookingUnit;

        FM_Case__c fmCase = new FM_Case__c(Request_Type_DeveloperName__c = 'Move_in_Request',
                                           RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Move In').getRecordTypeId(),
                                           Request_Type__c = 'Move In',
                                           Origin__c = 'Portal',
                                           Booking_Unit__c = bookingUnit.Id,
                                           Status__c = 'Draft Request');
        insert fmCase;

        Property__c propObj = TestDataFactoryFM.createProperty();
        insert propObj;

        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;

        Resource__c objRes = new Resource__c();
        objRes.Chargeable_Fee_Slot__c = 10;
        objRes.Non_Operational_Days__c = 'Sunday';
        objRes.Resource_Type__c = 'FM Amenity';
        objRes.No_of_advance_booking_days__c = 2;
        objRes.Resource_Type__c = 'FM Amenity';
        objRes.Deposit__c = 3;
        objRes.Chargeable_Fee_Slot__c = 5;
        objRes.No_of_deposit_due_days__c = 1;
        objRes.Contact_Person_Email__c = 'test@test.com';   //
        objRes.Contact_Person_Name__c = 'Test'; //
        objRes.Contact_Person_Phone__c = '1234567890';  //
        insert objRes;

        Resource_Sharing__c objRS= new Resource_Sharing__c();
        objRS.Building__c = locObj.Id;
        objRS.Resource__c = objRes.Id;
        insert objRS;

        Resource_Slot__c objResSlot1 = new Resource_Slot__c();
        objResSlot1.Resource__c = objRes.Id;
        objResSlot1.Start_Date__c = Date.Today();
        objResSlot1.End_Date__c = Date.Today().addDays(30);
        objResSlot1.Start_Time_p__c = '09:00';
        objResSlot1.End_Time_p__c = '09:30';
        insert objResSlot1;

        Non_operational_days__c obj = new Non_operational_days__c();
        obj.Non_operational_date__c = Date.today();
        obj.Resource__c = objRes.Id;
        insert obj;

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/login'; 
        req.addParameter('username', '1234');
        req.addParameter ('password', 'test@123');
        req.addParameter ('domain', 'test.salesforce.com');
        req.httpMethod = 'Post';
        RestContext.request = req;
        RestContext.response = res;

        FmIpmsRestServices.DueInvoicesResponse invoicesResponse = new FmIpmsRestServices.DueInvoicesResponse();
        invoicesResponse.OutputParameters = new FmIpmsRestServices.OutputParameters();
        invoicesResponse.OutputParameters.X_RETURN_STATUS = 'S';
        invoicesResponse.OutputParameters.X_RETURN_MESSAGE = 'Total Records Fetched...1';
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE = new FmIpmsRestServices.ResponseMessage();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM = new List<Map<String, String>>();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM.add(
            new Map<String, String> {
                'ATTRIBUTE1' => 'registrationId',
                'ATTRIBUTE2' => 'unitName',
                'ATTRIBUTE3' => 'projectName',
                'ATTRIBUTE4' => 'customerId',
                'ATTRIBUTE5' => 'orgId',
                'ATTRIBUTE6' => 'partyId',
                'ATTRIBUTE7' => 'partyName',
                'ATTRIBUTE8' => 'trxNumber',
                'ATTRIBUTE9' => 'creationDate',
                'ATTRIBUTE10' => 'callType',
                'ATTRIBUTE11' => '50',
                'ATTRIBUTE12' => '100',
                'ATTRIBUTE13' => '50',
                'ATTRIBUTE14' => FmcUtils.formatAsIpmsDate(Date.today()),
                'ATTRIBUTE15' => 'trxType',
                'ATTRIBUTE16' => '0'
            }
        );
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(invoicesResponse)));
        
        API_CommunityPortalLogin.login(); 

        List<Booking_Unit__c> lstBU = [SELECT id,
                                              Booking__c,
                                              Registration_ID__c,
                                              Inventory__c,
                                              Unit_Name__c,
                                              Resident__c,
                                              Registration_Status__c,
                                              Handover_Flag__c
                                      FROM Booking_Unit__c
                                      WHERE Registration_ID__c = '3901'];
         
         Test.startTest();
         API_CommunityPortalLogin.GetUnitsDetailsFromAccountId(account.Id);
         Test.stopTest();
    
    }
    
    @isTest static void resetPassword() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/resetpasswordportal';  
        req.addParameter('username', 'agentEmail@damacgroup.com.fullcopy');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        API_CommunityPortalResetPassword.resetPassword(); 
    
    }
    
    @isTest static void resetPassword2() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/resetpasswordportal';  
        req.addParameter('username', '1234');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        API_CommunityPortalResetPassword.resetPassword(); 
    
    }
    
    @isTest static void resetPasswordException () {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/resetpasswordPortal';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        API_CommunityPortalResetPassword.resetPassword(); 
    
    }
    
    @isTest static void changePassword() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/changepasswordPortal';  
        //req.addParameter('username', 'agentEmail@damacgroup.com.fullcopy');
        //req.addParameter('newpassword', 'damac@testUAT');
        
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        API_CommunityPortalChangePassword.changepassword(); 
    
    }
    
    @isTest static void changePassword2() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/changepasswordPortal';  
        req.addParameter('accountId', 'test');
        //req.addParameter('newpassword', 'damac@testUAT');
        
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        API_CommunityPortalChangePassword.changepassword(); 
    
    }

    @isTest static void changePassword3() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/changepasswordPortal';  
        req.addParameter('newpassword', 'test');
        req.addParameter('accountId', '');
        
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        API_CommunityPortalChangePassword.changepassword(); 
    
    }

    @isTest static void changePassword4() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/changepasswordPortal';  
        req.addParameter('accountId', 'test');
        req.addParameter('newpassword', '');
        
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        API_CommunityPortalChangePassword.changepassword(); 
    }

    @isTest static void changePassword5() {
        
                NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com',
                                       Salutation = 'MR',
                                       Nationality__c = 'Indian',
                                       Nationality__pc = 'Indian',
                                       Passport_Number__pc = '1234',
                                       Passport_Number__c = '1234',
                                       Passport_Expiry_Date__c = Date.today(),
                                       Passport_Expiry_Date__pc = Date.today(),
                                       PersonEmail = 'test@test.com',
                                       Email__c = 'test@test.com',
                                       Mobile_Phone_Encrypt__pc = '0091123456789',
                                       Mobile__c = '0091123456789',
                                       PersonMailingStreet = 'test',
                                       PersonMailingPostalCode = '12345',
                                       PersonMailingCity = 'test',
                                       PersonMailingState = 'test',
                                       PersonMailingCountry = 'test',
                                       Address_Line_1__pc = 'test',
                                       Address_Line_2__pc = 'test',
                                       Address_Line_3__pc = 'test',
                                       Address_Line_4__pc = 'test',
                                       Address_Line_1__c = 'test',
                                       Address_Line_2__c = 'test',
                                       Address_Line_3__c = 'test',
                                       Address_Line_4__c = 'test',
                                       City__pc = 'test',
                                       City__c = 'Batna',
                                       Country__pc = 'India',
                                       Country__c = 'India',
                                       Mobile_Phone_Encrypt_2__pc = '0091123456789',
                                       Mobile_Phone_Encrypt_3__pc = '0091123456789',
                                       Mobile_Phone_Encrypt_4__pc = '0091123456789',
                                       Mobile_Phone_Encrypt_5__pc = '0091123456789',
                                       Email_2__pc = 'test@test.com',
                                       Email_3__pc = 'test@test.com',
                                       Email_4__pc = 'test@test.com',
                                       Email_5__pc = 'test@test.com',
                                       Status__c = 'Active',
                                       Email_1__c = 'test@test.com',
                                       Email_2__c = 'test@test.com',
                                       Email_3__c = 'test@test.com',
                                       Primary_Language__c = 'English',
                                       Date_of_Birth__pc = Date.today().addDays(-10),
                                       Date_Of_Birth__c = Date.today().addDays(-10),
                                       Mobile_Country_Code__pc = 'India: 0091',
                                       Mobile_Country_Code__c = 'India: 0091',
                                       Mobile_Country_Code_2__c = 'India: 0091',
                                       Mobile_Country_Code_3__c = 'India: 0091',
                                       Mobile_Country_Code_4__c = 'India: 0091',
                                       Mobile_Country_Code_5__c = 'India: 0091',
                                       Mobile_Phone_2__c = '0091123456789',
                                       Mobile_Phone_3__c = '0091123456789',
                                       Mobile_Phone_4__c = '0091123456789',
                                       Mobile_Phone_5__c = '0091123456789',
                                       Mobile_Country_Code_2__pc = 'India: 0091',
                                       Mobile_Country_Code_3__pc = 'India: 0091',
                                       Mobile_Country_Code_4__pc = 'India: 0091',
                                       Mobile_Country_Code_5__pc = 'India: 0091',
                                       CR_Number__c = '123456',
                                       CR_Registration_Expiry_Date__c = Date.Today().addDays(10),
                                       Mobile_Phone_Encrypt__c = '12356987' );
        insert account;
        
        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];

        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Customer Community Login User(Use this)%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              //ContactId = contact1.Id,
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
        Database.insert(user1);
        
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/changepasswordPortal';  
        req.addParameter('accountId', account.Id);
        req.addParameter('newpassword', 'test1234');
        
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        API_CommunityPortalChangePassword.changepassword(); 
    }

    
    @isTest static void changePasswordException() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/changepasswordPortal';  
        
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        API_CommunityPortalChangePassword.changepassword(); 
    
    }
}