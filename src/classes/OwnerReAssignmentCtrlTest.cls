@isTest
public class OwnerReAssignmentCtrlTest {
    @isTest
    static void testShowOwners(){
        Profile objProfile = [
            SELECT Id 
            FROM Profile 
            WHERE Name='Standard User'
        ];
        User objUser = new User();
        objUser.alias = 'test';
        objUser.username = 'test1234@testorg.com';
        objUser.email = 'test1234@testorg.com';
        objUser.firstName = 'test1';
        objUser.lastName = 'test2';
        objUser.localeSidKey = 'cs';
        objUser.languageLocaleKey = 'en_US';
        objUser.emailEncodingKey = 'Big5';
        objUser.timeZoneSidKey = 'Africa/Cairo';
        objUser.currencyIsoCode = '';
        objUser.profileId = objProfile.Id;
        insert objUser;
        System.runAs( objUser ) {
            
            System.debug( 'Current User: ' + UserInfo.getUserName() );
            System.debug( 'Current Profile: ' + UserInfo.getProfileId() ); 
        }
        
        Test.startTest();
            OwnerReAssignmentCtrl objOwnerReAssignmentCtrl = new OwnerReAssignmentCtrl();
            objOwnerReAssignmentCtrl.ownerType = 'User';
            objOwnerReAssignmentCtrl.ownerSearchVal = 'test';
            objOwnerReAssignmentCtrl.searchOwners();
        Test.stopTest();
        
    }
    
    @isTest
    static void testShowQueue(){
        Group objGroup = new Group( Name='test' );
        insert objGroup;
        
        Test.startTest();
            OwnerReAssignmentCtrl objOwnerReAssignmentCtrl = new OwnerReAssignmentCtrl();
            objOwnerReAssignmentCtrl.ownerType = 'Queue';
            objOwnerReAssignmentCtrl.ownerSearchVal = 'test';
            objOwnerReAssignmentCtrl.searchOwners();
        Test.stopTest();
    }
    
    @isTest
    static void testShowPortalUser(){
        Profile objProfile = [
            SELECT Id 
            FROM Profile 
            WHERE Name = 'Standard User'
        ];
        User objUser = new User();
        objUser.alias = 'test';
        objUser.username = 'test1234@testorg.com';
        objUser.email = 'test1234@testorg.com';
        objUser.firstName = 'test1';
        objUser.lastName = 'test2';
        objUser.localeSidKey = 'cs';
        objUser.languageLocaleKey = 'en_US';
        objUser.emailEncodingKey = 'Big5';
        objUser.timeZoneSidKey = 'Africa/Cairo';
        objUser.currencyIsoCode = '';
        objUser.profileId = objProfile.Id;
        insert objUser;        
        System.runAs( objUser ) {
            
            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId()); 
        }
        
        Test.startTest();
            OwnerReAssignmentCtrl objOwnerReAssignmentCtrl = new OwnerReAssignmentCtrl();
            objOwnerReAssignmentCtrl.ownerType = 'Portal User';
            objOwnerReAssignmentCtrl.ownerSearchVal = 'test';
            objOwnerReAssignmentCtrl.searchOwners();
        Test.stopTest();
    }
    
    @isTest
    static void testreAssignOwner(){
        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Email__c = 'test@gmail.com';
        
        TriggerOnOffCustomSetting__c setting = new TriggerOnOffCustomSetting__c();
        setting.Name = 'CallingListTrigger';
        setting.OnOffCheck__c = true;
        insert setting;
               
        Profile objProfile = [
            SELECT Id 
            FROM Profile 
            WHERE Name='Standard User'
        ];
        User objUser = new User();
        objUser.alias = 'test';
        objUser.username = 'test1234@testorg.com';
        objUser.email = 'test1234@testorg.com';
        objUser.firstName = 'test1';
        objUser.lastName = 'test2';
        objUser.localeSidKey = 'cs';
        objUser.languageLocaleKey = 'en_US';
        objUser.emailEncodingKey = 'Big5';
        objUser.timeZoneSidKey = 'Africa/Cairo';
        objUser.currencyIsoCode = '';
        objUser.profileId = objProfile.Id;
        insert objUser;  
        
        System.runAs( objUser ){
            insert objCallingList;
        }
        
        Test.startTest();
            OwnerReAssignmentCtrl objOwnerReAssignmentCtrl = new OwnerReAssignmentCtrl();
         	objOwnerReAssignmentCtrl.query = 'SELECT Id, Email__c FROM Calling_List__c';
        	objOwnerReAssignmentCtrl.strDisplayFields = '[{"apiName":"Email__c","label":"Email"}]';
            objOwnerReAssignmentCtrl.executeQuery();
            objOwnerReAssignmentCtrl.clIdsToReAssign = objCallingList.Id;
            objOwnerReAssignmentCtrl.changeOwnerToId = objUser.Id;
            objOwnerReAssignmentCtrl.reAssignOwner();
        Test.stopTest();
        
        List<Calling_List__c> callingList = [
            SELECT ownerId 
            FROM Calling_List__c 
            WHERE Id = :objCallingList.Id
        ];
        
    }
}