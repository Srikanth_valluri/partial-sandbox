/************************************************************************************************
 * @Name              : CreateTeamsMeetingController
 * @Test Class Name   : CreateTeamsMeetingController_Test
 * @Description       : Controller Class for CreateTeamsMeeting VF Page
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         26/03/2020       Created
***********************************************************************************************/

Public Class CreateTeamsMeetingController{
    public string inqId {get;set;}
    public String emailList {get; set;}
    public Inquiry__c inquiry {get; set;}
    public String message {get; set;}
    public String subject {get; set;}
    public String messageBody {get; set;}
    public Boolean success;
    public Boolean isUpdate {get; set;}
    public String eventId;
    public String meetingId;
    public String taskSFId{get; set;}
    public String endPoint{get; set;}
    public String token{get; set;}
    public String body{get; set;}
    public String buttonLabel {get; set;}
    public String pageTitle {get; set;}
    public Task tsk;
    public String idFromPage{get; set;}
    public string startTimeString;
    public String meetingLink {get; set;}
    public Boolean updateSuccess{get; set;}
    public String updateResponse{get; set;}

    /************************************************************************************************
    * @Description : Constructor method 
    * @Params      : None
    * @Return      : None
    ************************************************************************************************/
    public CreateTeamsMeetingController(){
        emailList = '';
        message = '';
        success = false;
        isUpdate = false;
        updateSuccess = false;
        updateResponse = '';
        eventId = '';
        buttonLabel = '';
        pageTitle = '';
        messageBody = '';
        idFromPage = Apexpages.currentPage().getParameters().get('Id');
        system.debug('Object: ' + getSobjectNameById(idFromPage));
        if(getSobjectNameById(idFromPage) == 'Task'){
            taskSFId = idFromPage;
            buttonLabel = 'Update Meeting';
            pageTitle = 'Update Teams Meeting';
            if(taskSFId != null && taskSFId != ''){
                isUpdate = true;
                token = DAMAC_MSTeamsIntegration.fetchAccessToken();
                tsk = [SELECT Id, Subject, Type, Call_Start_Time__c, Activity_Type_3__c, 
                                    Call_End_Time__c, Description, OwnerId, WhatId 
                              FROM Task WHERE Id =: taskSFId];
                if(tsk.WhatId != null && getSobjectNameById(tsk.WhatId) == 'Inquiry__c'){
                    system.debug('Object Type: ' + getSobjectNameById(tsk.WhatId));
                    inqId = tsk.WhatId;
                }
                if(tsk.Subject.contains('Online Meeting:')){
                    subject = tsk.Subject.subStringAfter('Online Meeting:');
                } else{
                    subject = tsk.Subject;
                }
                system.debug('tsk.Description: ' + tsk.Description);
                if(tsk.Description != null){
                     eventId = tsk.Description.substringAfter('Calendar Event Id:');
                     messageBody = tsk.Description.substringAfter('Meeting Email Message:').substringBefore('Calendar Event Id:');
                     messageBody = messageBody.replaceAll('<br/>', '\r');
                     meetingLink = tsk.Description.substringAfter('Meeting Link: ').substringBefore('Meeting Id:');
                     meetingLink = meetingLink.replaceAll('<br/>', '\r');
                     emailList = tsk.Description.substringAfter('Attendees:').substringBefore('Meeting Email Message:').trim();
                     meetingId = tsk.Description.substringAfter('Meeting Id:').substringBefore('Attendees:').trim();
                }
                endPoint = 'https://graph.microsoft.com/v1.0/me/calendar/events/' + eventId;
            } else{
                message = 'Event Id is missing';
            }
        } else if(getSobjectNameById(idFromPage) == 'Inquiry__c'){
            inqId = idFromPage;
            buttonLabel = 'Create Meeting';
            pageTitle = 'Create Teams Meeting';
            subject = 'DAMAC: Meeting with Property Advisor';
            
        } else{
            message = 'Wrong Id';
        }
        system.debug('inqId: ' + inqId);
        system.debug('eventId: ' + eventId);
        system.debug('isUpdate: ' + isUpdate);
        if(inqId != null && inqId != ''){
            inquiry = [SELECT Id, Name, Owner.Email, TSA_TL_of_Owner__c, Email__c, First_Name__c, Last_Name__c, Notes__c,
                           Online_Meeting_Start_Date_Time__c, Online_Meeting_End_Date_Time__c
                       FROM Inquiry__c WHERE Id =: inqId];
            if(emailList == ''){
                emailList += inquiry.email__c;
            }
            if(inquiry.Owner.Email != null && !emailList.contains(inquiry.Owner.Email)){
                emailList += ',' + inquiry.Owner.Email;
            }
            if(!emailList.contains(UserInfo.getUserEmail())){
                emailList += ',' + UserInfo.getUserEmail();
            }
            if(inquiry.TSA_TL_of_Owner__c != null){
                for(User tl: [SELECT Id, Name, Email FROM User WHERE Id =: inquiry.TSA_TL_of_Owner__c]) {
                    if(!emailList.contains(tl.Email)){
                        emailList += ',' + tl.Email;
                    }
                }
            }
            if(isUpdate) {
                inquiry.Online_Meeting_Start_Date_Time__c = tsk.Call_Start_Time__c;
                inquiry.Online_Meeting_End_Date_Time__c = tsk.Call_End_Time__c;
            } else{
                inquiry.Online_Meeting_Start_Date_Time__c = System.now().addHours(3);
                inquiry.Online_Meeting_End_Date_Time__c = inquiry.Online_Meeting_Start_Date_Time__c.addMinutes(30);
            }
            String timeZone = UserInfo.getTimezone().getDisplayName().subStringBefore(' ');
            startTimeString = inquiry.Online_Meeting_Start_Date_Time__c.format('dd/MM/YYYY HH:mm') + ' ' + timeZone;
            if(messageBody == ''){
                messageBody = 'Dear ' + inquiry.First_Name__c + ',\n\n'
                      + 'Thank you for your interest in DAMAC Properties.'
                      + ' We have scheduled a meeting with our Relationship Manager on ' 
                      + inquiry.Online_Meeting_Start_Date_Time__c.format('dd/MM/YYYY HH:mm') + ' ' + timeZone
                      + ', to help you find your dream home.';
            }
        } else{
            message = 'Inquiry Id is missing';
        }
    }

    /************************************************************************************************
    * @Description : Updates the endDateTime and message body when the startDateTime is changes
    * @Params      : None
    * @Return      : None
    ************************************************************************************************/
    public void updateEndDateTime(){
          system.debug('inquiry.Online_Meeting_Start_Date_Time__c: ' + inquiry.Online_Meeting_Start_Date_Time__c);
          inquiry.Online_Meeting_End_Date_Time__c = inquiry.Online_Meeting_Start_Date_Time__c.addMinutes(30);
          String timeZone = UserInfo.getTimezone().getDisplayName().subStringBefore(' ');
          messageBody  = messageBody.replace(startTimeString, inquiry.Online_Meeting_Start_Date_Time__c.format('dd/MM/YYYY HH:mm') + ' ' + timeZone);
          startTimeString = inquiry.Online_Meeting_Start_Date_Time__c.format('dd/MM/YYYY HH:mm')  + ' ' + timeZone;
          system.debug('inquiry.Online_Meeting_End_Date_Time__c: ' + inquiry.Online_Meeting_End_Date_Time__c);
    }

    /************************************************************************************************
    * @Description : Invokes createTeamsMeeting() in DAMAC_MSTeamsIntegration with the required
    *                    inputs from the VF Page
    * @Params      : None
    * @Return      : None 
    ************************************************************************************************/
    public void createMeeting(){
        if(inquiry.Online_Meeting_Start_Date_Time__c != null && inquiry.Online_Meeting_Start_Date_Time__c > DateTime.Now()){
            if(isUpdate){
                updateMeeting(inquiry.Online_Meeting_Start_Date_Time__c, inquiry.Online_Meeting_End_Date_Time__c, 
                                                                            emailList, subject, messageBody,  inquiry);
            } else{
                Task tsk = new Task();
                system.debug('messageBody: ' + messageBody);
                messageBody = messageBody.replaceAll('\r\n', '<br/>');
                messageBody = messageBody.replaceAll('\n', '<br/>');
                messageBody = messageBody.replaceAll('\r', '<br/>');
                tsk = DAMAC_MSTeamsIntegration.createTeamsMeeting(inquiry.Online_Meeting_Start_Date_Time__c, inquiry.Online_Meeting_End_Date_Time__c,
                                                                     emailList, subject, messageBody,  inquiry);  
                if(tsk != null){
                    try{
                        insert tsk;
                        message = 'Meeting Created Successfully'; 
                        success = true;
                    } catch(exception e){
                        message = 'Error while creating SF Task';    
                    }
                } else{
                    message = 'Error while creating Teams Meeting';
                    success = true;
                }
            }
        } else if(inquiry.Online_Meeting_Start_Date_Time__c == null){
            message = 'Start Time missing';
        } else {
            message = 'Start Time already expired';
        }
        system.debug('message: ' + message);
        if(!isUpdate || message != ''){
            if(success){
                ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.CONFIRM, message));
            }else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, message));
            }
        }
    }

    /************************************************************************************************
    * @Description : Updates the Salesforce Calendar Event based on Graph API Update Response
    * @Params      : None
    * @Return      : None
    ************************************************************************************************/   
    public void updateEvent(){
        system.debug('updateSuccess: ' + updateSuccess);
        system.debug('updateResponse: ' + updateResponse);
        if(updateSuccess){
            message = 'Meeting Updated Successfully';
            ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.CONFIRM, message));
            Task meetingTask = new Task();
            meetingTask.id = taskSFId;
            meetingTask.Subject = 'Online Meeting: ' + subject;
            meetingTask.Call_End_Time__c = inquiry.Online_Meeting_End_Date_Time__c;
            meetingTask.Call_Start_Time__c = inquiry.Online_Meeting_Start_Date_Time__c;
            
            String description = subject + '\n\nMeeting Link: ' + meetingLink
                                   + '\n\nMeeting Id:' + meetingId 
                                   + '\n\nAttendees:' + emailList
                                   + '\n\nMeeting Email Message:' + messageBody
                                   + '\n\nCalendar Event Id:' + eventId; 
            meetingTask.Description = description;
            update meetingTask;
        } else{
            system.debug('Error thrown: ' + updateResponse);
            message = 'Meeting Updation Failed';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, message));
        }
    }

    /************************************************************************************************
    * @Description : Creates body for sending Update Request to Graph API
    * @Params      : 1. DateTime startTime - Updated startTime of Calendar Event
                     2. DateTime endTime - Updated endTime of Calendar Event
                     3. String emailList - Updated List of Calendar Event Attendess
                     4. String subject - Updated subject of Calendar Event
                     5. String messageBody - Updated Message content for Calendar Event Invite
                     6. Inquiry__c inq - Related Inquiry record
    * @Return      : None
    ************************************************************************************************/   
    public void updateMeeting(DateTime startTime, DateTime endTime, String emailList, String subject, String messageBody, Inquiry__c inq){
        System.debug('updateMeeting method()');
        String msg = '';
        String stTime = startTime.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
        String enTime = endTime.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
        String name = '';
        if(inq.First_Name__c != null && inq.First_Name__c != ''){
            name = inq.First_Name__c;
        }
        if(inq.Last_Name__c != null && inq.Last_Name__c != ''){
            if(name != ''){
                name += ' ';
            }
            name += inq.Last_Name__c;
        }
        String attendees = '';
        for(String emailAdr: emailList.split(',')){
            if(attendees != ''){
                attendees += ' , ';
            }
            attendees += '{"emailAddress": {"address":"' + emailAdr.trim() + '","name": ""},"type": "required"}';
        }
        body = '{"subject": "' + subject + '", "body": { '
              + '"contentType": "HTML", "content": "' + messageBody + '<br/><br/> Meeting Link: ' + meetingLink + '<br/><br/>" },'
              + '"start": {"dateTime": "' + stTime + '","timeZone": "Greenwich Standard Time"},'
              + '"end": {"dateTime": "' + enTime + '","timeZone": "Greenwich Standard Time"},'
              + '"attendees": [' + attendees + ']}';  
    }

    /************************************************************************************************
    * @Description : Identifies the SObjectType of the input Id
    * @Params      : Id inputId - Id for which SOjectType is to be returned
    * @Return      : String - Name of the SObjectType of Input Id
    ************************************************************************************************/
    public static String getSobjectNameById(Id inputId) {
        Schema.SObjectType sobjectType = inputId.getSObjectType();
        return sobjectType.getDescribe().getName();
    } 
    }