/*-------------------------------------------------------------------------------------------------
Description: Controller class for FMPenaltyWaiver Component
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
v1.0     | 13-02-2019       | Lochana Rajput   | 1. Added logic to generate CRF Document
=============================================================================================================================
*/
public virtual without sharing class FMPenaltyWaiverController {

    public Boolean pollerBool{get;set;}
    public List<Attachment> lstCRFAttachments;
    public String attachmentURL{get;set;}
    private String batchId;
    public String strHelpPageId { get; set; }
    public String strAccountId { get; set; }
    public String strSelectedUnit { get; set; }
    public String strCaseId { get; set; }
    public String strSRType { get; set; }
    public Integer indexOfDocToRemove { get; set; }

    public transient String strDocumentBody { get; set; }
    public transient String strDocumentName { get; set; }
    public transient String deleteAttRecId { get; set; }

    public Booking_Unit__c objUnit { get; set; }

    public FM_Case__c objFMCase { get; set; }

    public list<FM_Additional_Detail__c> lstPendingInvoices { get; set; }
    public map< String, FM_Documents__mdt > mapMetaDocs { get; set; }
    public map<String, SR_Attachments__c> mapUploadedDocs { get; set; }
    public list<SR_Attachments__c> lsCustomDocs{get;set;}

    public FMPenaltyWaiverController() {
        lstCRFAttachments = new List<Attachment>();
        objFMCase = new FM_Case__c();
        indexOfDocToRemove = 1;
        lstPendingInvoices = new list<FM_Additional_Detail__c>();
        strCaseId = ApexPages.currentPage().getParameters().get('Id');
        lsCustomDocs = new List<SR_Attachments__c>();
        if( String.isBlank( strCaseId ) ) {
            strAccountId = ApexPages.currentPage().getParameters().get('AccountId');
            strSelectedUnit = ApexPages.currentPage().getParameters().get('UnitId');
            strSRType = ApexPages.currentPage().getParameters().get('SRType');
        }
        else {
            objFMCase = FM_Utility.getCaseDetails( strCaseId ) ;
            strAccountId = objFMCase.Account__c ;
            strSelectedUnit = objFMCase.Booking_Unit__c ;
            strSRType = objFMCase.Request_Type_DeveloperName__c ;
        }

        if( String.isNotBlank( strSelectedUnit ) ) {
            init();
        }
    }

    public void init() {
        objUnit = FM_Utility.getUnitDetails( strSelectedUnit ) ;
        initializeFMCase();
        processDocuments();

        if( objUnit != NULL && String.isBlank(strCaseId)) {
            try {
                FmIpmsRestServices.DueInvoicesResult objResult =
                                        FmIpmsRestServices.getAllInvoices( objUnit.Registration_Id__c,
                                                                           '','');
                                                                           //objUnit.Inventory__r.Property_Code__c );
                system.debug('==objResult=='+objResult);
                if( objResult != NULL ) {
                    objFMCase.Outstanding_service_charges__c = String.isNotBlank( objResult.totalDueAmount ) ? objResult.totalDueAmount : '0';
                    calculatePendingDueInvoices( objResult );
                }
            }
            catch( Exception e ) {
                 system.debug('==Exception=='+e.getMessage() + ' ' + e.getLineNumber());
                 Error_Log__c objError = FM_Utility.createErrorLog( strAccountId,
                                                                   strSelectedUnit,
                                                                   objFMCase.Id,
                                                                   'Penalty Waiver',
                                                                   e.getMessage() + ' ' + e.getLineNumber() );
                //insert objError ;
            }
        }else {
           lstPendingInvoices = getAdditionalDetails();
        }
    }

    public PageReference savePenaltyWaiverCase() {
        if( objFMCase != NULL ) {
            objFMCase.Status__c = 'In Progress';
            try {
                Decimal totalAmountToBeWaived = 0;
                if(!lstPendingInvoices.isEmpty()){
                   for(FM_Additional_Detail__c insAdditionalDetail : lstPendingInvoices){
                        if(insAdditionalDetail.Percent_to_be_waived__c != NULL){
                            system.debug('!!!!!insAdditionalDetail.Charged_Amount__c'+insAdditionalDetail.Charged_Amount__c);
                            insAdditionalDetail.Amount_to_be_Waived__c = insAdditionalDetail.Charged_Amount__c * (insAdditionalDetail.Percent_to_be_waived__c/100);
                            insAdditionalDetail.Amount_to_be_Waived__c = insAdditionalDetail.Amount_to_be_Waived__c.setScale(2);
                            totalAmountToBeWaived = totalAmountToBeWaived + insAdditionalDetail.Amount_to_be_Waived__c;
                        }
                    }
                }
                objFMCase.Amount_to_be_Waived__c = totalAmountToBeWaived.setScale(2) ;
                upsert objFMCase ;
                if(objFMCase.id != Null){
                    if(!lstPendingInvoices.isEmpty()){
                        for(FM_Additional_Detail__c insAdditionalDetail : lstPendingInvoices){
                            insAdditionalDetail.Penalty_Case__c = objFMCase.id;
                        }
                    }
                    upsert lstPendingInvoices;
                }
                return new PageReference('/' + objFMCase.Id);
            }
            catch( Exception e ) {
                Error_Log__c objError = FM_Utility.createErrorLog( strAccountId,
                                                                   strSelectedUnit,
                                                                   objFMCase.Id,
                                                                   'Penalty Waiver',
                                                                   e.getMessage() + ' ' + e.getLineNumber() );
                insert objError ;
            }
        }

        return NULL ;
    }

    public PageReference submitPenaltyWaiverCase() {
        if( objFMCase != NULL ) {
            objFMCase.Status__c = 'Submitted';
            if( String.isNotBlank( objFMCase.Unit_Name__c ) ) {
                list<FM_User__c> lstUsers = FM_Utility.getApprovingUsers( new set<String> { objFMCase.Unit_Name__c.split('/')[0] } );
                for( FM_User__c objUser : lstUsers ) {
                    if( String.isNotBlank( objUser.FM_Role__c ) ) {
                        if( objUser.FM_Role__c.containsIgnoreCase( 'Manager') ) {
                            objFMCase.FM_Manager_Email__c = objUser.FM_User__r.Email;
                        }
                        else if( objUser.FM_Role__c.containsIgnoreCase( 'Admin') &&
                                 objFMCase.Admin__c == NULL ) {
                            objFMCase.Admin__c = objUser.FM_User__c;
                        }
                    }
                }
            }
            objFMCase.Approving_Authorities__c = '';
            try {
                Boolean isPreviousYearWaiver = false ;
                Boolean isCurrentYearWaiver = false ;

                if( !lstPendingInvoices.isEmpty() ){
                   objFMCase.Amount_to_be_Waived__c = 0 ;
                   for( FM_Additional_Detail__c insAdditionalDetail : lstPendingInvoices ){
                        if( insAdditionalDetail.Percent_to_be_waived__c != NULL ){
                            system.debug('@@@@insAdditionalDetail.Charged_Amount__c'+insAdditionalDetail.Charged_Amount__c);
                            insAdditionalDetail.Approved_Percent_to_be_waived__c = insAdditionalDetail.Percent_to_be_waived__c ;
                            insAdditionalDetail.Amount_to_be_Waived__c = ( insAdditionalDetail.Charged_Amount__c * ( insAdditionalDetail.Percent_to_be_waived__c/100 ) ).setScale(2);
                            objFMCase.Amount_to_be_Waived__c += insAdditionalDetail.Amount_to_be_Waived__c;
                            if( String.isNotBlank( insAdditionalDetail.Penalty_Date__c ) ) {
                                Date penaltyDate = getDateValueOfString( insAdditionalDetail.Penalty_Date__c );
                                if( penaltyDate.monthsBetween( system.today() ) > 12 && !isPreviousYearWaiver ) {
                                    isPreviousYearWaiver = true ;
                                }
                                else if( penaltyDate.monthsBetween( system.today() ) <= 12 ) {
                                    isCurrentYearWaiver = true ;
                                }
                            }
                        }
                    }
                }
                if( objFMCase.Amount_to_be_Waived__c < 200 ) {
                    objFMCase.Approving_Authorities__c = Label.Penalty_Waiver_FM_Collection_Approving_User;
                }
                else {
                    if( isPreviousYearWaiver ) {
                        objFMCase.Approving_Authorities__c = Label.Penalty_Waiver_SVP_Approving_User;
                    }
                    else if( isCurrentYearWaiver ) {
                        objFMCase.Approving_Authorities__c = Label.Penalty_Waiver_VP_Approving_User;
                    }
                }
                QueueSObject QueueID = [SELECT Queue.Id, Queue.Name, Queue.Type 
                                        FROM QueueSObject 
                                        WHERE Queue.Type ='Queue' AND Queue.Name =: System.Label.Penalty_Waiver_Owners Limit 1];
                system.debug('mazi queue======'+QueueID);
                if(QueueID != NULL) {
                    objFMCase.OwnerId = QueueID.Queue.Id;
                }
                upsert objFMCase ;
                system.debug('mazi objFMCase updated======'+objFMCase);
                if(objFMCase.id != Null){
                    if(!lstPendingInvoices.isEmpty()){
                        for(FM_Additional_Detail__c insAdditionalDetail : lstPendingInvoices){
                            insAdditionalDetail.Penalty_Case__c = objFMCase.id;
                        }
                    }
                    upsert lstPendingInvoices;
                }
                PageReference objPage = new PageReference('/' + objFMCase.Id );
                return objPage ;
            }
            catch( Exception e ) {
                Error_Log__c objError = FM_Utility.createErrorLog( strAccountId,
                                                                   strSelectedUnit,
                                                                   objFMCase.Id,
                                                                   'Penalty Waiver',
                                                                   e.getMessage() + ' ' + e.getLineNumber() );
                insert objError ;
            }
        }
        return NULL ;
    }

    public void insertCase() {
        try {
            if( objFMCase.Id == NULL ) {
                insert objFMCase ;
                objFMCase = FM_Utility.getCaseDetails( objFMCase.Id );
            }

        }
        catch( Exception e ) {
            Error_Log__c objError = FM_Utility.createErrorLog( strAccountId,
                                                               strSelectedUnit,
                                                               objFMCase.Id,
                                                               'Penalty Waiver',
                                                               e.getMessage() );
            insert objError ;
        }
    }

    public void uploadDocument() {
        if( objFMCase != NULL && objFMCase.Id != NULL ) {
            UploadMultipleDocController.data objResponse = new UploadMultipleDocController.data();
            list<UploadMultipleDocController.MultipleDocRequest> lstWrapper = new list<UploadMultipleDocController.MultipleDocRequest>();
            if( String.isNotBlank( strDocumentName ) && String.isNotBlank( strDocumentBody ) ) {
                lstWrapper.add( PenaltyWaiverHelper.makeWrapperObject( EncodingUtil.Base64Encode( RentalPoolTerminationHelper.extractBody( strDocumentBody ) ) ,
                                                                       strDocumentName ,
                                                                       strDocumentName ,
                                                                       objUnit.Registration_Id__c, objFMCase.Id, '1' ) );

            }
            if( !lstWrapper.isEmpty() ) {
                objResponse = PenaltyWaiverService.uploadDocumentsOnCentralRepo( lstWrapper );
                if( objResponse != NULL && objResponse.data != NULL ) {
                    insertCustomAttachment( objFMCase.Id, objResponse );
                }
            }
        }
    }

    @testVisible
    protected void insertCustomAttachment( Id fmCaseId, UploadMultipleDocController.data objResponse ) {

        strDocumentName = strDocumentName.substring( 0, strDocumentName.lastIndexOf('.') );

        SR_Attachments__c objCustAttach ;
        for( UploadMultipleDocController.MultipleDocResponse objFile : objResponse.data ) {
            objCustAttach = new SR_Attachments__c();
            objCustAttach.FM_Account__c = strAccountId ;
            objCustAttach.Attachment_URL__c = objFile.url;
            objCustAttach.Booking_Unit__c = strSelectedUnit ;
            objCustAttach.FM_Case__c = fmCaseId ;
            objCustAttach.Name = strDocumentName ;
            //objCustAttach = setDocumentValidity(objCustAttach);
            strDocumentName = '';
            strDocumentBody = '';
        }

        insert objCustAttach ;
        processDocuments();
    }

    public void processDocuments() {
        system.debug('==mapUploadedDocs=='+mapUploadedDocs);
        system.debug('==mapMetaDocs=='+mapMetaDocs);
        mapUploadedDocs = new map<String, SR_Attachments__c>() ;
        mapMetaDocs = new map< String, FM_Documents__mdt >();
         system.debug('==list of docs=='+FM_Utility.getDocumentsList( objFMCase.Id ));
        if( objFMCase != NULL && objFMCase.Id != NULL ) {
            for( SR_Attachments__c objAttch : FM_Utility.getDocumentsList( objFMCase.Id ) ) {
                mapUploadedDocs.put(objAttch.Name, objAttch );
                if( lsCustomDocs.size() > 1){
                    lsCustomDocs.remove(lsCustomDocs.size() -1);
                }else{
                    lsCustomDocs.clear();
                }
            }
        }

        for( FM_Documents__mdt objDoc : FM_Utility.getDocumentsList( strSRType, objUnit.Property_City__c ) ) {
            if( !mapUploadedDocs.containsKey( objDoc.MasterLabel ) ) {
                mapMetaDocs.put( objDoc.MasterLabel , objDoc );
            }
        }

        mapUploadedDocs = mapUploadedDocs.isEmpty() ? NULL : mapUploadedDocs ;
        mapMetaDocs = mapMetaDocs.isEmpty() &&  lsCustomDocs.isEmpty() ? NULL : mapMetaDocs ;
        system.debug('==mapUploadedDocs=='+mapUploadedDocs);
        system.debug('==mapMetaDocs=='+mapMetaDocs);
    }

    public void deleteAttachment() {
        if( String.isNotBlank( deleteAttRecId ) ) {
            for( SR_Attachments__c objAttach  : mapUploadedDocs.values() ) {
                if( objAttach.Id == deleteAttRecId ) {
                    delete objAttach ;
                }
            }
            processDocuments();
        }
    }

    @testVisible
    protected void calculatePendingDueInvoices( FmIpmsRestServices.DueInvoicesResult objResult ) {
        if( objResult != NULL && objResult.lstDueInvoice != NULL &&
            !objResult.lstDueInvoice.isEmpty() ) {
            lstPendingInvoices = new list<FM_Additional_Detail__c>();
            for( FmIpmsRestServices.DueInvoice objDueInvoice : objResult.lstDueInvoice ) {
                if( String.isNotBlank( objDueInvoice.callType ) && objDueInvoice.callType.equalsIgnoreCase( 'Interest' ) ) {
                    FM_Additional_Detail__c objAddPenaltyDetails =
                        new FM_Additional_Detail__c( RecordTypeId = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByDeveloperName().get('Penalty_Waiver').getRecordTypeId(),
                                                     Penalty_Date__c = objDueInvoice.creationDate,
                                                     Charged_Amount__c = objDueInvoice.chargeAmount,
                                                     Paid_Amount__c = objDueInvoice.paidAmount,
                                                     Due_Remaining__c = objDueInvoice.dueRemaining,
                                                     Transaction_Reference__c = objDueInvoice.trxNumber,
                                                     Amount_to_be_waived__c = 0 );
                    objFMCase.Total_Paid_Amount__c += objDueInvoice.paidAmount != NULL ? objDueInvoice.paidAmount : 0 ;
                    objFMCase.Total_Penalty_Amount__c += objDueInvoice.chargeAmount != NULL ? objDueInvoice.chargeAmount : 0;
                    objFMCase.Total_Penalty_Due__c += objDueInvoice.dueRemaining != NULL ? objDueInvoice.dueRemaining : 0;
                    lstPendingInvoices.add( objAddPenaltyDetails );
                }
            }
        }
    }

    @testVisible
    protected void initializeFMCase() {
        objFMCase = objFMCase == NULL ? new FM_Case__c() : objFMCase ;
        objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByDeveloperName().get('Penalty_Waiver').getRecordTypeId();
        objFMCase.Account__c = strAccountId ;
        objFMCase.Booking_Unit__c = strSelectedUnit ;
        objFMCase.Status__c = 'New';
        objFMCase = setCaseOrigin(objFMCase);
        objFMCase.Request_Type__c = 'Penalty Waiver';
        objFMCase.Request_Type_DeveloperName__c = strSRType ;
        objFMCase.Amount_to_be_Waived__c = objFMCase.id != NULL ? objFMCase.Amount_to_be_Waived__c : 0;
        objFMCase.Total_Paid_Amount__c = objFMCase.id != NULL ? objFMCase.Total_Paid_Amount__c : 0;
        objFMCase.Total_Penalty_Amount__c = objFMCase.id != NULL ? objFMCase.Total_Penalty_Amount__c : 0;
        objFMCase.Total_Penalty_Due__c = objFMCase.id != NULL ? objFMCase.Total_Penalty_Due__c : 0;
    }

    protected virtual FM_Case__c setCaseOrigin(FM_Case__c fmCase) {
        fmCase.Origin__c = 'Walk-In';
        return fmCase;
    }

    protected List<FM_Additional_Detail__c> getAdditionalDetails(){
        List<FM_Additional_Detail__c> lstAdditionalDetails =  new List<FM_Additional_Detail__c>();
        if(!String.isBlank(strCaseId)){
            lstAdditionalDetails = FM_Utility.getRelatedObjectDetails(strCaseId,'Penalty_Waiver');
            System.debug('lstAdditionalDetails======='+lstAdditionalDetails);
        }
        return lstAdditionalDetails;
    }

    public void addCustomDocuments() {
        mapUploadedDocs = mapUploadedDocs == NULL ? new map<String, SR_Attachments__c>() : mapUploadedDocs ;
        if(lsCustomDocs != NULL) {
            SR_Attachments__c attachObj = new SR_Attachments__c();
            attachObj.Name ='Additional Document';
            if( !mapUploadedDocs.containsKey( attachObj.Name) ) {
                lsCustomDocs.add(attachObj);
            }
        }
        mapUploadedDocs = mapUploadedDocs.isEmpty() ? NULL : mapUploadedDocs ;
    }

    public void removeCustomDocuments() {
        if(lsCustomDocs != NULL) {
            if( lsCustomDocs.size() > 1 ) {
                lsCustomDocs.remove(lsCustomDocs.size()-1);
            }else{
                lsCustomDocs.clear();
            }
        }
    }

    public Date getDateValueOfString( String strDate ) {
        list<String> lstDateComponents = strDate.split('-');
        if( lstDateComponents.size() > 2 ) {
            Date penaltyDate = Date.newInstance( Integer.valueOf(lstDateComponents[2]),
                                                 getNumberEquivalentOfMonth( lstDateComponents[1] ) ,
                                                 Integer.valueOf(lstDateComponents[0]) );
            return penaltyDate ;
        }
        return NULL ;
    }

    @testVisible
    protected Integer getNumberEquivalentOfMonth( String strMonthName ) {
        switch on strMonthName {
            when 'JAN' {
                return 1 ;
            }
            when 'FEB' {
                return 2 ;
            }
            when 'MAR' {
                return 3 ;
            }
            when 'APR' {
                return 4 ;
            }
            when 'MAY' {
                return 5 ;
            }
            when 'JUN' {
                return 6 ;
            }
            when 'JUL' {
                return 7 ;
            }
            when 'AUG' {
                return 8 ;
            }
            when 'SEP' {
                return 9 ;
            }
            when 'OCT' {
                return 10 ;
            }
            when 'NOV' {
                return 11 ;
            }
            when 'DEC' {
                return 12 ;
            }
            when else {
                return 0;
            }
        }
    }

    public void generateNDA() {
        System.debug('objFMCase--------'+objFMCase.Id);
        if( objFMCase.Id != NULL ) {
            lstCRFAttachments = [SELECT id,
                                     name,
                                     parentId
                                 FROM Attachment
                                 WHERE parentId =: objFMCase.id
                                 AND name =: System.Label.CRF_Document
                                 ORDER By createddate desc
                                 LIMIT 1];
            system.debug('lstCRFAttachments'+lstCRFAttachments);
            if(!lstCRFAttachments.isEmpty()) {
                String idDocument = lstCRFAttachments[0].Id;
                attachmentURL = '/servlet/servlet.FileDownload?file=' + idDocument;
            }
            if(String.isBlank(attachmentURL)) {
                FM_GenerateDrawloopDocumentBatch objInstance = new FM_GenerateDrawloopDocumentBatch(objFMCase.Id
                                 , System.Label.CRF_Doc_DDP_Template_Id
                                 , System.Label.CRF_Doc_Template_Delivery_Id);
                batchId = Database.ExecuteBatch(objInstance);
                System.debug('==batchId===='+batchId);
                pollerBool = true;
            }//if
        }//if
    }//generateNDA

    public void checkDrawloopBatchStatus() {
        AsyncApexJob job = [SELECT Id, Status FROM AsyncApexJob WHERE Id =: batchId];
        System.debug('objFMCase--------'+objFMCase.Id);
        System.debug('job--------'+job.Status);
        System.debug('pollerBool--------'+pollerBool);
        if(job.Status == 'Completed') {
            lstCRFAttachments = [SELECT id, parentId
                                 FROM Attachment
                                 WHERE parentId =: objFMCase.id
                                 AND name =: System.Label.CRF_Document
                                 ORDER By createddate desc
                                 LIMIT 1];
             System.debug('lstCRFAttachments--------'+lstCRFAttachments);
            if(lstCRFAttachments.size() > 0) {
                pollerBool = false;
                system.debug('objAttach==='+lstCRFAttachments[0]);
                String idDocument = lstCRFAttachments[0].Id;
                attachmentURL = '/servlet/servlet.FileDownload?file=' + idDocument;
            }
        }
        else {
            pollerBool = true;
        }
    }
}