/*
Description: Batch Class to Create the Audit Trail records.
Developed by -DAMAC IT
*/
global class DAMAC_AuditTrail implements Database.Batchable<sObject> {

    // The batch job starts
    global Database.Querylocator start(Database.BatchableContext bc){
        String query;
        if(!test.isrunningtest()){  
            query = 'SELECT Id,Action,CreatedBy.Name,createdbyid,CreatedDate,Display,Section,DelegateUser FROM SetupAuditTrail where createddate'+LABEL.AuditTrailQueryInterval;
        }else{
            query = 'SELECT Id,Action,CreatedBy.Name,createdbyid,CreatedDate,Display,Section,DelegateUser FROM SetupAuditTrail LIMIT 10';
        }
        System.debug('>>>> start ' + query);
        return Database.getQuerylocator(query);
    } 
  
  // The batch job executes and operates on one batch of records
  global void execute(Database.BatchableContext bc, List<sObject> scope){
    System.debug('>>>> execute ' + scope.size());
    list<AuditTrail__c> allAudit = new list<AuditTrail__c>();
    for(sobject obj:scope){
        string auditTrailid = obj.id;
        auditTrailid = auditTrailid.substring(0,15);
        AuditTrail__c audit = new AuditTrail__c();
        audit.AuditTrail_Id__c = auditTrailid;   
        audit.Action__c = string.valueof(obj.get('Action'));
        //audit.Created_by_Name__c = string.valueof(obj.get('CreatedBy.Name'));
        audit.CreatedbyId__c = string.valueof(obj.get('createdbyid'));
        audit.Display__c = string.valueof(obj.get('Display'));
        audit.Section__c = string.valueof(obj.get('Section'));
        audit.Delegate_User__c = string.valueof(obj.get('DelegateUser'));
        allAudit.add(audit);
    }    
    upsert allAudit AuditTrail_Id__c;
  }
  
  // The batch job finishes
  global void finish(Database.BatchableContext bc){
    //AsyncApexJob job = [SELECT Id, Status FROM AsyncApexJob WHERE Id = :bc.getJobId()]; 
    //System.debug('>>>> finish ' + job.Status);
  }
}