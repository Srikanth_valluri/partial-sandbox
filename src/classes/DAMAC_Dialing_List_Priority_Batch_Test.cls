@IsTest
public class DAMAC_Dialing_List_Priority_Batch_Test {

    static testmethod void method1(){
        dialing_list__c dl= new dialing_list__c(); 
        insert dl;
        
        list<Inquiry_Assignment_Rules__c> inqAssign = new list<Inquiry_Assignment_Rules__c> ();
        Inquiry_Assignment_Rules__c inqAssign1 = new Inquiry_Assignment_Rules__c();
        inqAssign1.Active__c = True;
        inqAssign1.Execute_on__c = 'Dialing Priority';
        inqAssign1.Filter_Logic__c = ' 1 ';
        inqAssign1.Inquiry_Record_Type__c = 'Inquiry';
        inqAssign.add(inqAssign1);
        
        Inquiry_Assignment_Rules__c inqAssign2 = new Inquiry_Assignment_Rules__c();
        inqAssign2.Active__c = True;
        inqAssign2.Execute_on__c = 'Dialing Priority';
        inqAssign2.Filter_Logic__c = ' 1 ';
        inqAssign2.Inquiry_Record_Type__c = 'Inquiry';
        inqAssign.add(inqAssign2);
        
        insert inqAssign;
        
        Inquiry_Assignment_Rules__c inqAssign3 = new Inquiry_Assignment_Rules__c();
        inqAssign3.Active__c = True;
        inqAssign3.Execute_on__c = 'Dialing Priority';
        inqAssign3.Parent_Reassign_Rule__c = inqAssign2.Id;
        inqAssign3.Order__c = 1;
        inqAssign3.Parameter__c = 'id';
        inqAssign3.Condition__c = '=';
        inqAssign3.Value__c = dl.id;
        insert inqAssign3;
        
        Inquiry_User_Assignment_Rules__c userInq = new Inquiry_User_Assignment_Rules__c ();
        userInq.Weekly__c=2;
        userInq.SetupOwnerID = UserInfo.getUserID ();
        userInq.Reassignment_Limit__c = 500;
        userInq.Net_Direct_Sales_Rank__c = null;
        insert userInq;
       

        test.startTest();
       
        ExecuteReassignmentBatch.executeBatch(inqAssign2.id,'Dialing Priority');
        test.stopTest();
    }
    
    /*
    static testmethod void method2(){
        list<Inquiry_Assignment_Rules__c> inqAssign = new list<Inquiry_Assignment_Rules__c> ();
        Inquiry_Assignment_Rules__c inqAssign1 = new Inquiry_Assignment_Rules__c();
        inqAssign1.Active__c = True;
        inqAssign1.Execute_on__c = 'Dialing Priority';
        inqAssign1.Filter_Logic__c = '1';
        inqAssign1.Inquiry_Record_Type__c = 'Inquiry';
        inqAssign1.Skip_Powerline__c =true;
        inqAssign.add(inqAssign1);
         insert inqAssign;
        
        Inquiry_Assignment_Rules__c inqAssign2 = new Inquiry_Assignment_Rules__c();
        inqAssign2.Active__c = True;
        inqAssign2.Execute_on__c = 'Dialing Priority';
        inqAssign2.Parent_Reassign_Rule__c = inqAssign1.Id;
        inqAssign2.Order__c = 1;
        inqAssign2.Parameter__c = 'Assigned_Date_Age__c';
        inqAssign2.Condition__c = '>';
        inqAssign2.Value__c = '1';
        
        insert inqAssign2;
        
        Inquiry_User_Assignment_Rules__c userInq = new Inquiry_User_Assignment_Rules__c ();
        userInq.Weekly__c=2;
        userInq.SetupOwnerID = UserInfo.getUserID ();
        userInq.Reassignment_Limit__c = 500;
        userInq.Net_Direct_Sales_Rank__c = null;
        insert userInq;
       

        test.startTest();
        
        DAMAC_Dialing_List_Priority_Batch inqBatch = new DAMAC_Dialing_List_Priority_Batch(new List <ID> ());
        database.executeBatch(inqBatch);
        
        //inqBatch.executeBatch(BC,new list<Inquiry__c> {inq});
        test.stopTest();
    }
    
    
    static testmethod void method3(){
        list<Inquiry_Assignment_Rules__c> inqAssign = new list<Inquiry_Assignment_Rules__c> ();
        Inquiry_Assignment_Rules__c inqAssign1 = new Inquiry_Assignment_Rules__c();
        inqAssign1.Active__c = True;
        inqAssign1.Execute_on__c = 'Dialing Priority';
        inqAssign1.Filter_Logic__c = '1';
        inqAssign1.Inquiry_Record_Type__c = 'Inquiry';
        inqAssign.add(inqAssign1);
        insert inqAssign;
        
        Inquiry_Assignment_Rules__c inqAssign2 = new Inquiry_Assignment_Rules__c();
        inqAssign2.Active__c = True;
        inqAssign2.Execute_on__c = 'Dialing Priority';
        inqAssign2.Parent_Reassign_Rule__c = inqAssign1.Id;
        inqAssign2.Order__c = 1;
        inqAssign2.Parameter__c = 'Assigned_Date_Age__c';
        inqAssign2.Condition__c = '>';
        inqAssign2.Value__c = '1';
        insert inqAssign2;
        
        Inquiry_User_Assignment_Rules__c userInq = new Inquiry_User_Assignment_Rules__c ();
        userInq.Weekly__c=2;
        userInq.SetupOwnerID = UserInfo.getUserID ();
        userInq.Reassignment_Limit__c = 500;
        userInq.Net_Direct_Sales_Rank__c = null;
        insert userInq;
       

        test.startTest();
        DAMAC_Dialing_List_Priority_Batch inqBatch = new DAMAC_Dialing_List_Priority_Batch(new List <ID> ());
        database.executeBatch(inqBatch);
       
        test.stopTest();
    }
   
    
    
     static testmethod void method5(){
        list<Inquiry_Assignment_Rules__c> inqAssign = new list<Inquiry_Assignment_Rules__c> ();
        Inquiry_Assignment_Rules__c inqAssign1 = new Inquiry_Assignment_Rules__c();
        inqAssign1.Active__c = True;
        inqAssign1.Execute_on__c = 'Dialing Priority';
        inqAssign1.Filter_Logic__c = '1';
        inqAssign1.Inquiry_Record_Type__c = 'Inquiry';
        inqAssign.add(inqAssign1);
        
        Inquiry_Assignment_Rules__c inqAssign2 = new Inquiry_Assignment_Rules__c();
        inqAssign2.Active__c = True;
        inqAssign2.Execute_on__c = 'Dialing Priority';
        inqAssign2.Filter_Logic__c = '1';
        inqAssign2.Inquiry_Record_Type__c = 'Inquiry';
        inqAssign.add(inqAssign2);
        
        insert inqAssign;
        
        Inquiry_Assignment_Rules__c inqAssign3 = new Inquiry_Assignment_Rules__c();
        inqAssign3.Active__c = True;
        inqAssign3.Execute_on__c = 'Dialing Priority';
        inqAssign3.Parent_Reassign_Rule__c = inqAssign1.Id;
        inqAssign3.Order__c = 1;
        inqAssign3.Parameter__c = 'Assigned_Date_Age__c';
        inqAssign3.Condition__c = '>';
        inqAssign3.Value__c = '1';
        insert inqAssign3;
        
        Inquiry_User_Assignment_Rules__c userInq = new Inquiry_User_Assignment_Rules__c ();
        userInq.Weekly__c=2;
        userInq.SetupOwnerID = UserInfo.getUserID ();
        userInq.Reassignment_Limit__c = 500;
        userInq.Net_Direct_Sales_Rank__c = null;
        insert userInq;
       

            test.starttest();
                system.debug(inqAssign[0].id);
                ApexPages.StandardController sc = new ApexPages.StandardController(inqAssign[0]);            
                DAMAC_RULE_UTILITY cls = new DAMAC_RULE_UTILITY(sc);
                PageReference pageRef = Page.DAMAC_RULE_UTILITY;
                pageRef.getParameters().put('id', String.valueOf(inqAssign[0].Id));
                Test.setCurrentPage(pageRef);
                cls.onload();
            test.stoptest();        
       
    }
    
     */   

}