@isTest
private class CreateSRCallingListControllerTest {
  @testSetup
      static void allTheDataForThisTestClass(){
         Id accRecordTypeId = Schema.SobjectType.Account.RecordTypeInfosByName.get('Person Account').RecordTypeId;
         Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
         Account accObj = new Account(FirstName = 'Madina',
                         LastName = 'Alieva',
                         RecordTypeId = accRecordTypeId);
        insert accObj;
        
        Calling_List__c callObj = new Calling_List__c(Account__c = accObj.Id,Registration_ID__c = '12851');
        insert callObj;
            // insert Case
        Case objCase = TestDataFactory_CRM.createCase( accObj.Id, recTypeCOD );
        insert objCase;
      }
      static testMethod void srCallingTest(){
           // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
      
        Calling_List__c callObjInst = [SELECT Id,
                                              Account__c,
                                              Registration_ID__c
                                         FROM Calling_List__c
                                         LIMIT 1];
        System.debug('inside test class:callObjInst::'+callObjInst);
        System.assertNotEquals(null,callObjInst.Id);
        Test.startTest();
            PageReference pageRef = Page.CreateSRForCallingListPage;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('Id',String.valueOf(callObjInst.Id));
            System.currentPageReference().getParameters().put('SRType', 'AssignmentRequest');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(callObjInst);
            
            CreateSRCallingListController controllerObj = new CreateSRCallingListController(sc);
          controllerObj.searchTerm = 'Test FirstName';
          List<Account> lstAccount = CreateSRCallingListController.searchMovie(controllerObj.searchTerm);
          
          Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('Id',String.valueOf(callObjInst.Id));
            System.currentPageReference().getParameters().put('SRType', 'AdditionalParking');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            controllerObj = new CreateSRCallingListController(sc);
            
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('Id',String.valueOf(callObjInst.Id));
            System.currentPageReference().getParameters().put('SRType', 'BouncedCheque');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            controllerObj = new CreateSRCallingListController(sc);
            
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('Id',String.valueOf(callObjInst.Id));
            System.currentPageReference().getParameters().put('SRType', 'FundTransfer');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            controllerObj = new CreateSRCallingListController(sc);
            
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('Id',String.valueOf(callObjInst.Id));
            System.currentPageReference().getParameters().put('SRType', 'FurniturePackage');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            controllerObj = new CreateSRCallingListController(sc);
            
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('Id',String.valueOf(callObjInst.Id));
            System.currentPageReference().getParameters().put('SRType', 'Mortgage');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            controllerObj = new CreateSRCallingListController(sc);
            
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('Id',String.valueOf(callObjInst.Id));
            System.currentPageReference().getParameters().put('SRType', 'PenaltyWaiver');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            controllerObj = new CreateSRCallingListController(sc);
            
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('Id',String.valueOf(callObjInst.Id));
            System.currentPageReference().getParameters().put('SRType', 'ProofOfPayment');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            controllerObj = new CreateSRCallingListController(sc);
            
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('Id',String.valueOf(callObjInst.Id));
            System.currentPageReference().getParameters().put('SRType', 'Refunds');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            controllerObj = new CreateSRCallingListController(sc);
            
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('Id',String.valueOf(callObjInst.Id));
            System.currentPageReference().getParameters().put('SRType', 'COCD');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            controllerObj = new CreateSRCallingListController(sc);
            
             Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('Id',String.valueOf(callObjInst.Id));
            System.currentPageReference().getParameters().put('SRType', 'EarlyHandover');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            controllerObj = new CreateSRCallingListController(sc);
            
             Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('Id',String.valueOf(callObjInst.Id));
            System.currentPageReference().getParameters().put('SRType', 'NOCVisa');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            controllerObj = new CreateSRCallingListController(sc);
            
             Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('Id',String.valueOf(callObjInst.Id));
            System.currentPageReference().getParameters().put('SRType', 'Complaint');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            controllerObj = new CreateSRCallingListController(sc);
            
             Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('Id',String.valueOf(callObjInst.Id));
            System.currentPageReference().getParameters().put('SRType', 'Home');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            controllerObj = new CreateSRCallingListController(sc);
            
             Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('Id',String.valueOf(callObjInst.Id));
            System.currentPageReference().getParameters().put('SRType', 'RentalPool');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            controllerObj = new CreateSRCallingListController(sc);
            
            
             Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('Id',String.valueOf(callObjInst.Id));
            System.currentPageReference().getParameters().put('SRType', 'TitleDeed');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            controllerObj = new CreateSRCallingListController(sc);
            
             Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('Id',String.valueOf(callObjInst.Id));
            System.currentPageReference().getParameters().put('SRType', 'Handover');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            controllerObj = new CreateSRCallingListController(sc);
            
             /*Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('Id',String.valueOf(callObjInst.Id));
            System.currentPageReference().getParameters().put('SRType', 'RentalPoolTermination');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            controllerObj = new CreateSRCallingListController(sc);
            
             Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('Id',String.valueOf(callObjInst.Id));
            System.currentPageReference().getParameters().put('SRType', 'RentalPoolAssignment');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            controllerObj = new CreateSRCallingListController(sc);
            
             Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('Id',String.valueOf(callObjInst.Id));
            System.currentPageReference().getParameters().put('SRType', 'PlotHandover');
            System.currentPageReference().getParameters().put('AccountId', objAcc.Id);
            controllerObj = new CreateSRCallingListController(sc);*/
            
             
            
        Test.stopTest();
        //controllerObj.fetchCases();
    }
  
}