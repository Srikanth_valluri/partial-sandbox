/** 
 * Description: This class throws error if the Complaint Process related Tasks are closed without 
 * filling certain fields on Case like "CRE Analysis", "Manager Analysis", "Complaint Outcome"
 */
public class ComplaintTaskTriggerHandler {

    /**
     * This method performs validations on Task object by checking the field values on Case object
     */
    public static void performValidationOnCase(List<Task> lstTasks) {
        List<Id> lstCaseIds = new List<Id>();
        Map<Id, List<Task>> mapTaskCaseForCRE = new Map<Id, List<Task>>();
        Map<Id, List<Task>> mapTaskCaseForManager = new Map<Id, List<Task>>();
        List<Task> lstTasksForCREError = new List<Task>();
        List<Task> lstTasksForManagerError = new List<Task>();

        // Iterate over the tasks and check if there are any tasks which are Closed or Completed for Complaint process
        for (Task objTask : lstTasks) {
            if (String.isNotBlank(objTask.Status) && String.isNotBlank(objTask.Process_Name__c)  && String.isNotBlank(objTask.WhatId) && (objTask.Status == 'Completed' || objTask.Status == 'Closed') && objTask.Process_Name__c == 'Complaint' && objTask.WhatId != null&& String.valueOf(objTask.WhatId).startsWith('500')) {

                // Extract all the tasks which are assigned to CRE
                if (String.isNotBlank(objTask.Subject) && String.isNotBlank(objTask.Assigned_User__c) && objTask.Subject == 'New Complaint Request' && objTask.Assigned_User__c == 'CRE') {
                    lstCaseIds.add(objTask.WhatId);
                    if (mapTaskCaseForCRE.containsKey(objTask.WhatId)) {
                        list<Task> lstTempTask = mapTaskCaseForCRE.get(objTask.WhatId);
                        lstTempTask.add(objTask);
                        mapTaskCaseForCRE.put(objTask.WhatId, lstTempTask);
                    } else {
                        mapTaskCaseForCRE.put(objTask.WhatId, new List<Task> {objTask});
                    }
                }

                // Extract all the tasks which are assigned to CRE Manager
                else if (String.isNotBlank(objTask.Subject) && String.isNotBlank(objTask.Assigned_User__c) && objTask.Subject == 'Complaint case not closed after 24 hours' && objTask.Assigned_User__c == 'CRE Manager') {
                    lstCaseIds.add(objTask.WhatId);
                    if (mapTaskCaseForManager.containsKey(objTask.WhatId)) {
                        list<Task> lstTempTask = mapTaskCaseForManager.get(objTask.WhatId);
                        lstTempTask.add(objTask);
                        mapTaskCaseForManager.put(objTask.WhatId, lstTempTask);
                    } else {
                        mapTaskCaseForManager.put(objTask.WhatId, new List<Task> {objTask});
                    }
                }
            }
        }

        // Fetch the related Case records
        List<Case> lstCase = [SELECT Id, CRE_Analysis__c, Manager_Analysis__c, Complaint_Outcome__c  FROM Case WHERE Id In: lstCaseIds];

        if (lstCase != null && !lstCase.isEmpty()) {
            for (Case objCase : lstCase) {
                if ((objCase.Complaint_Outcome__c == null || objCase.CRE_Analysis__c == null) && mapTaskCaseForCRE.containsKey(objCase.Id)) {
                    lstTasksForCREError.addAll(mapTaskCaseForCRE.get(objCase.Id));
                }
                else if ((objCase.Complaint_Outcome__c == null || objCase.Manager_Analysis__c == null) && mapTaskCaseForManager.containsKey(objCase.Id)) {
                    lstTasksForManagerError.addAll(mapTaskCaseForManager.get(objCase.Id));
                }
            }
        }

        // Throw error for Tasks which are assigned to CRE
        if (lstTasksForCREError != null && !lstTasksForCREError.isEmpty()) {
            for (Task objTask : lstTasksForCREError) {
                objTask.addError('Please enter CRE Analysis and Complaint Outcome field values on related case record');
            }
        }

        // Throw error for Tasks which are assigned to Manager CRE
        if (lstTasksForManagerError != null && !lstTasksForManagerError.isEmpty()) {
            for (Task objTask : lstTasksForManagerError) {
                objTask.addError('Please enter Manager Analysis and Complaint Outcome field values on related case record');
            }
        }
    }
}