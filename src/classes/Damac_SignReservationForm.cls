Public with sharing class Damac_SignReservationForm implements Queueable, Database.allowsCallouts {
    
    SET<ID> bookingUnitIds = new Set <ID> ();
    public Damac_SignReservationForm (Set <ID> unitIds) {
        bookingUnitIds = unitIds;
    }
    
    public void execute(QueueableContext context) {
        Signnow_Credentials__c credentials = Signnow_Credentials__c.getInstance ();
        String accessToken = Damac_SignNowAPI.authenticate(credentials);
        geneateAndUploadReservationForm (accessToken);
        
    }
    
    public void geneateAndUploadReservationForm (String accessToken) {
        Signnow_Credentials__c credentials = Signnow_Credentials__c.getInstance (); 
        List <ID> bookingIds = new List <ID> ();
        Map <ID, Buyer__c> buyerDetails = new Map <ID, Buyer__c> ();
        List <Booking_Unit__c> bookingUnits = new List <Booking_Unit__c> ();
        bookingUnits = [SELECT Inventory__r.Unit_Name__c, Booking__c, Booking__r.Deal_SR__c FROM Booking_Unit__c WHERE ID IN: bookingUnitIds];
        for (Booking_Unit__c unit : bookingUnits)
        {
            if (unit.Booking__c != null)
                bookingIds.add (unit.Booking__c);
        }
        
        if (bookingIds.size () > 0) {
            for (Buyer__c buyer : [SELECT Booking__c, Email__c, Primary_Buyer__c FROM Buyer__c WHERE Booking__c IN : bookingIds AND Primary_Buyer__c = TRUE])
            {
                buyerDetails.put (buyer.Booking__c, buyer);
            }
        }
        LIst <Agent_Sign_Positions__mdt> signerPositions = [SELECT MasterLabel, Document_Name__c, Page_Number__c,Signer__c, Type__c,
                                                          X_Position__c, Y_Position__c, Width__c, Height__c,Required__c
                                                   FROM Agent_Sign_Positions__mdt
                                                   WHERE Signer__c = '1' 
                                                   AND Document_Name__c = 'ReservationForm'
                                               ORDER BY Page_Number__c];
        List <ESign_Details__c> eSignRecords = new List <ESign_Details__c> ();
        for (Booking_Unit__c unit : bookingUnits)
        {
            PageReference pdf = page.UnitReservationFormPDF;
            pdf.getParameters().put( 'id', unit.Id );
            Attachment att = new Attachment ();
            att.Body = pdf.getContentAsPDF();
            att.Name = 'Reservation Form -'+unit.Inventory__r.Unit_Name__c+'.pdf';
            
            String documentId = Damac_SignNowAPI.uploadAttachment(credentials, att, accessToken, null, signerPositions);
            List <String> documentIds = new List <String> ();
            documentIds.add (documentId);
            
            Map <String, HttpResponse> res = new Map <String, HttpResponse> ();
            Buyer__c primaryBuyer = buyerDetails.get (unit.Booking__c);
            
            res = Damac_SignNowAPI.sendInvitieToSign (credentials, documentIds, accessToken, 
                                                      primaryBuyer.Email__c, 
                                                      '', '', att.Name);

            System.debug (res.get (documentId).getBody());
            ESign_Details__c sDoc = new ESign_Details__c ();
            sDoc.Document_Id__c = documentId;
            sDoc.To_Email__c = primaryBuyer.Email__c;
            sDoc.Service_Request__c = unit.Booking__r.Deal_SR__c;
            sDoc.Status__c = 'Email Sent';
            eSignRecords.add (sDoc);
            
        }
        if (eSignRecords.size () > 0) {
            Database.insert(eSignRecords, false);
        }        
    }
    
}