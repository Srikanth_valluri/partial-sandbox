/****************************************************************************************************
* Name          : RefreshUserTargetsBatchTest                                                       *
* Description   : Test class for RefreshUserTargetsBatch                                            *
* Created Date  : 12/04/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                *
* 1.0         Craig Lobo    12/04/2018      Initial Draft.                                          *
****************************************************************************************************/
@isTest 
public class RefreshUserTargetsBatchTest {

    @isTest static void testMethod1 (){

        Profile profileObj = [SELECT Id FROM Profile WHERE Name = 'Property Consultant' LIMIT 1]; 
        User userObj = new User(
            Alias = 'stand', 
            Email = 'user@testorg.com', 
            EmailEncodingKey = 'UTF-8', 
            LastName = 'CreateActionPlanBatchTest', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey='en_US', 
            ProfileId = profileObj.Id, 
            IsActive = true,
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = 'usersfd@testorg.com'
        );
        insert userObj;

        Target__c targetObjMonth = new Target__c(
            Target__c = 999,
            User__c = userObj.Id,
            Month__c =  Datetime.now().format('MMMMM'),
            Year__c = String.valueOf(System.today().year())
        );
        insert targetObjMonth;

        Test.startTest();
        ScheduleRefreshUserTargetsBatch schedulerInstance = new ScheduleRefreshUserTargetsBatch();
        String cronStr = '0 2 0 1 1/1 ? *'; 
        system.schedule('Test RefreshUserTargetsBatch', cronStr, schedulerInstance);
        RefreshUserTargetsBatch userTargetsBatch = new RefreshUserTargetsBatch();
        ID batchProcessId = Database.executeBatch(userTargetsBatch, 100);
        Test.stopTest();

    }

}