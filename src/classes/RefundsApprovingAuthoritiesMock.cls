@isTest
/*
* Revision History: 
* Version   Author          Date        Description.
* 1.1       Swapnil Gholap  21/11/2017  Initial Draft
*/
global class RefundsApprovingAuthoritiesMock implements WebServiceMock {
    global void doInvoke(
       Object stub,
       Object request,
       Map<String, Object> response,
       String endpoint,
       String soapAction,
       String requestName,
       String responseNS,
       String responseName,
       String responseType) {
                            
       RefundsRule.TokenRefundsTransfersResponse_element respElement = new RefundsRule.TokenRefundsTransfersResponse_element();
       respElement.return_x = '{"allowed":"Yes","message":null,"recommendingAuthorityOne":"Manager","recommendingAuthorityTwo":"Manager","recommendingAuthorityThree":"Manager","recommendingAuthorityFour":"Manager","approvingAuthorityOne":"Manager","approvingAuthorityTwo":"Manager","approvingAuthorityThree":"Manager","percToBeRefundedTransferred":null,"deductionFeePsf":null,"deductionFeeFlat":null,"deductionFeePercentage":null}';
       response.put('response_x', respElement);
       
   }
   
   global class RefundsApprovingAuthoritiesMock1 implements WebServiceMock{
       
       global void doInvoke(
       Object stub,
       Object request,
       Map<String, Object> response,
       String endpoint,
       String soapAction,
       String requestName,
       String responseNS,
       String responseName,
       String responseType) {
                            
       RefundsRule.TokenRefundsTransfersResponse_element respElement = new RefundsRule.TokenRefundsTransfersResponse_element();
       respElement.return_x = '{"allowed":"No","message":null,"recommendingAuthorityOne":"Manager","recommendingAuthorityTwo":"Manager","recommendingAuthorityThree":"Manager","recommendingAuthorityFour":"Manager","approvingAuthorityOne":"Manager","approvingAuthorityTwo":"Manager","approvingAuthorityThree":"Manager","percToBeRefundedTransferred":null,"deductionFeePsf":null,"deductionFeeFlat":null,"deductionFeePercentage":null}';
       response.put('response_x', respElement);
       
       }
   
   }
}