public with sharing class accountDetailRetrieval {
    public accountDetailRetrieval() {

    }

    @AuraEnabled(cacheable=true)
    public static string  getAccountDetails(String accountId)
    {
        list<Account> accList = [SELECT Id
        , Title__c
        , Corporate_Customer_Mobile__c
        , Customer_Masked_Mobile__c
        , Customer_Email__c
        , Name
        , Party_ID__c
        , Nationality__c
        , Passport_Number__c
        , IsPersonAccount
        , PersonMobilePhone
        , Salutation
        , Phone
        , PersonEmail
        , Nationality__pc
        , SLA__c
        , Organisation_Name__c
        , CR_Number__c
        , Mobile__c
        , Email__c
        , Email__pc
        , PersonMailingStreet
        , PersonMailingPostalCode
        , PersonMailingCity
        , PersonMailingState
        , PersonMailingCountry
        , Address_Line_1__c
        , Address_Line_2__c
        , Address_Line_3__c
        , Address_Line_4__c
        , Party_Type__c,
        Mobile_Phone_Encrypt__pc,
        Address_Line_1__pc,
        Address_Line_2__pc,
        Address_Line_3__pc,
        Address_Line_4__pc,
        City__c,
        Country__c,
        City__pc,
        Country__pc
        ,Passport_Number__pc
       
        
     FROM Account
    WHERE Id = :accountId ];
        return JSON.serialize(accList);
                        
    }

    @AuraEnabled(cacheable=true)
    public static list<Booking_Unit__c>  getAllBUs( String accountId )
    {   
        list<Booking_Unit__c> buList= [SELECT Id,Name
                                    ,Unit_Name__c
                                    ,Registration_ID__c
                                    ,Registration_Status__c
                                    ,Inventory__r.Unit__c
                                    ,Token_Amount__c
                                    ,Selling_Price__c
                                    ,Requested_Price__c
                                    ,Requested_Price_AED__c
                                    ,Bank_Account_Name__c
                                    ,Beneficiary_Bank__c
                                    ,Beneficiary_Name__c
                                    ,Beneficiary_Number__c
                                    ,Bank_Account_Id__c
                                    ,Virtual_Account_Number__c
                                    FROM Booking_Unit__c
                                    WHERE Booking__r.Account__c = :accountId];
        return bulist;
    }
    @AuraEnabled(cacheable=true)
    public static String  getBUDetails(String accountId,String caseId)
    {   
        //system.assert(false, ' accountId : ' + accountId + ' caseId : ' + caseId );
        String strResult = '';
        if( String.isNotBlank( caseId ) ) {
            // SR booking unit and POP details
            List<Case> lstCase = [ SELECT Id
                                        , Total_Token_Amount__c
                                        , Total_Dues__c
                                        , Cheque_Amount__c
                                        , Third_Party__c
                                        , Third_Party_Is_Company__c
                                        , IS_EHO_Bounced_Cheque__c
                                        , Cheque_Bank_Name__c
                                        , Cheque_Number__c
                                        , Bounced_Cheque_Date_of_Expiry__c
                                        , CurrencyIsoCode
                                        , Payment_Mode__c
                                        , Customer_Selected_Bank_for_Mortgage__c
                                        , Status
                                        , Rejection_Reason__c                                        
                                        , (SELECT Id
                                                , Amount_paid__c
                                                , Booking_Unit__c
                                                , Case__c
                                                , Registration_ID__c
                                                , Unit_Name__c
                                                , Booking_Unit__r.Registration_Status__c
                                            FROM SR_Booking_Units__r 
                                            WHERE Case__c =: caseId)
                                        , (SELECT Id
                                                , Bank_Reference_number__c
                                                , Bank_Account_number__c
                                                , Beneficiary_Name__c
                                                , Beneficiary_Bank__c
                                                , Deposit_Amount__c 
                                                , Deposit_Date__c
                                             FROM POP_Bank_Details__r
                                            WHERE Case__c =: caseId)
                                        FROM Case
                                       WHERE Id=: caseId LIMIT 1 ];
            
            strResult = JSON.serialize(lstCase);
        } else {
            if( String.isNotBlank( accountId ) ) {
                system.debug( ' <<<< accountId else : ' + accountId );
                list<Booking_Unit__c> buList= [SELECT Id,Name
                                            ,Unit_Name__c
                                            ,Registration_ID__c
                                            ,Registration_Status__c
                                            ,Inventory__r.Unit__c
                                            ,Token_Amount__c
                                            ,Selling_Price__c
                                            ,Requested_Price__c
                                            ,Requested_Price_AED__c
                                            ,Bank_Account_Name__c
                                            ,Bank_Account_Id__c
                                            ,Beneficiary_Bank__c
                                            ,Beneficiary_Name__c
                                            ,Beneficiary_Number__c
                                            ,Booking__r.Account__c
                                            FROM Booking_Unit__c
                                            WHERE Booking__r.Account__c = :accountId];
                system.debug( ' <<<< buList else : ' +buList );
                strResult = JSON.serialize(buList);
                system.debug( ' <<<< strResult : ' +strResult );
            }
        }

        return strResult;
                        
    }
    @AuraEnabled(cacheable=true)
    public static list<Case>  getBCCheque(String accId) {
        return [SELECT Id 
                     , Cheque_Number__c
                     , Cheque_Amount__c 
                     , Cheque_Bank_Name__c
                     , Bounced_Cheque_Date_of_Expiry__c
                  FROM Case 
                 WHERE AccountId =:accId 
                   AND RecordType.Name = 'Bounced Cheque SR' ];
    }

    @AuraEnabled(cacheable=true)
    public static list<Case>  getExistingSR(String accId) {
        return [SELECT Id 
                     , CaseNumber
                     , CreatedDate 
                     , Status
                     , Owner.Name
                     , Payment_Mode__c
                     , Total_Dues__c
                  FROM Case 
                 WHERE AccountId =:accId 
                   AND RecordType.Name = 'POP' ];
    }

    // To get FinancePagePicklist value
    @AuraEnabled(cacheable=true)
    public static list<String>  getFinancePagePicklist(){
        list<String> statusOptionsResult=new list<String>();
        List<POP_Bank_Details__c>  popBankDetailsLst  = [
            SELECT Id
            , Account_Number__c
            , Bank_Reference_number__c
            , Bank_Account_number__c
            , Beneficiary_Name__c
            , Beneficiary_Bank__c
            , Deposit_Amount__c 
            , Deposit_Date__c
            , ForFinance__c
         FROM POP_Bank_Details__c
        WHERE ForFinance__c = true
        ];  


System.debug('popBankDetailsLst>>>>'+popBankDetailsLst);
if(popBankDetailsLst != Null && popBankDetailsLst.size() > 0) {
for (POP_Bank_Details__c objPop : popBankDetailsLst) {                
statusOptionsResult.add(String.valueOf(objPop.Bank_Account_number__c+'/'+objPop.Beneficiary_Name__c+'/'+objPop.Account_Number__c));                                       
}   
}
    return statusOptionsResult;
    }

}