/********************************************************************************************************************************************
 * Description: Generic class to replace the merge fields
 * ==========================================================================================================================================
 * Version  | Date(DD-MM-YYYY) | Last Modified By   | Comments
 * ------------------------------------------------------------------------------------------------------------------------------------------
 * 1.0      | 15-10-2020       | Aishwarya Todkar   | 1. Initial Draft
 * 1.1      | 28-01-2021       | Aishwarya Todkar   | 1. Added today date
 * ******************************************************************************************************************************************/
public class MergeFieldReplacer {

    /*****************************************************************************************************************************************
    Description: Method to replcae merge fields
    Return Type: String
    Parameters : Email Body, Process name, record Id
    ******************************************************************************************************************************************/
        public static String replaceMergeFields( String strContents, String processName, Id recordId ) {
                
            if( String.isNotBlank( strContents )
            && String.isNotBlank( processName )
            && recordId != null ) {
                
                Set<String> setApiNames = new Set<String>();
                List<Merge_Field_Mapping__mdt> listMergeFldMdt = new List<Merge_Field_Mapping__mdt>();
                String objectName = recordId.getSObjectType().getDescribe().getName();
                
                if( strContents.contains( '{!Today}' ) ) {
                    strContents = strContents.replace( '{!Today}', String.valueOf( System.today() ) );
                }

                if( String.isNotBlank( objectName ) ) {
        
                    //get configured merge field mappings
                    for( Merge_Field_Mapping__mdt mdt : [ SELECT
                                                            Field_API_Name__c
                                                            , Merge_Field__c
                                                            , Process_Name__c
                                                            , Priority_Order__c
                                                        FROM
                                                            Merge_Field_Mapping__mdt
                                                        WHERE
                                                            Process_Name__c =: processName
                                                        ORDER BY 
                                                            Priority_Order__c ASC ] ) {
                        setApiNames.add( mdt.Field_API_Name__c );
                        listMergeFldMdt.add( mdt );
                    }
                    
                    System.debug('listMergeFldMdt-->'+ listMergeFldMdt );
                    
                    if( !setApiNames.isEmpty() && !listMergeFldMdt.isEmpty() ) {
                        
                        //Build query
                        String strQuery = 'SELECT ';
                        for( String apiName : setApiNames ) {
                            strQuery += apiName + ' ,';
                        }
                        strQuery = strQuery.removeEnd( ',');
                        strQuery = strQuery 
                                + ' FROM ' + objectName
                                + ' WHERE Id=\'' + recordId + '\'';
                        System.debug('strQuery--> ' + strQuery ); //IMP
                        
                        try {
                            List<sObject> listQueryResult = new List<sObject>();
                            listQueryResult = Database.query( strQuery );
                            System.debug( 'listQueryResult--> ' + listQueryResult); //IMP
        
                            //Replacing merge fields
                            if( !listQueryResult.isEmpty() ) {
                                
                                for( Merge_Field_Mapping__mdt mdt : listMergeFldMdt) {
                                    if( strContents.contains( mdt.Merge_Field__c ) ) {
                                        
                                        //call method to get string value
                                        String strVal = getStringValue( mdt.Field_API_Name__c, listQueryResult[0]);
                                        
                                        if( String.isNotBlank( strVal ) ) {
                                            if( mdt.Merge_Field__c.equalsIgnoreCase( '{!Customer_Name__c}' )) {
                                                strVal = GenericUtility.getCamelCase( strVal );
                                            }
                                            strContents = strContents.replace( mdt.Merge_Field__c , strVal );
                                        }
                                        else {
                                            strContents = strContents.replace( mdt.Merge_Field__c, '');
                                        }
                                    } 
                                }
                                return strContents;
                            }
                        } //End try
                        catch (DmlException e) {
                            System.debug('The following exception has occurred: ' + e.getMessage());
                        }
                    }// End setApiName if
                }// End objectName if
            }//End main if
            return null;  
        }// End replaceMergeFields method
     
    /******************************************************************************************************************************************
    Method Description  : Method to return String value from record
    Parameters          : Api Name, sObject Instance
    Return Type         : String
    ******************************************************************************************************************************************/
        public static String getStringValue( String field_API_Name, sObject obj ) {
            
            if( field_API_Name.contains('.') ) {
                String strAfter = field_API_Name.subStringAfter('.');
                String relatedObject1 = field_API_Name.subStringBefore('.');
                if( strAfter.contains('.') ) {
                    
                    String relatedObject2 = strAfter.subStringBefore('.');
                    strAfter = strAfter.subStringAfter('.');
    
                    if( strAfter.contains('.') ) {
    
                        // 3 level parenting
                        String relatedObject3 = strAfter.subStringBefore('.');
                        String fieldOfRelatedObj = strAfter.subStringAfter('.');
                        System.debug('Parenting 3---');
                        System.debug('related expression--' + relatedObject1 + '.' + relatedObject2 + '.' + relatedObject3 + '.' + fieldOfRelatedObj );
                        if( obj.getSobject( relatedObject1 ) != null 
                        && obj.getSobject( relatedObject1 ).getSobject( relatedObject2 ) != null
                        && obj.getSobject( relatedObject1 ).getSobject( relatedObject2 ).getSobject( relatedObject3 ) != null
                        && obj.getSobject( relatedObject1 ).getSobject( relatedObject2 ).getSobject( relatedObject3 ).get( fieldOfRelatedObj ) != null ) {
                            return String.valueOf( 
                                    obj.getSobject( relatedObject1 ).getSobject( relatedObject2 ).getSobject( relatedObject3 ).get( fieldOfRelatedObj ) 
                                );
                        } else {
                            return null;
                        }
                    }
                    else {
    
                        // 2 level parenting
                        String fieldOfRelatedObj = strAfter;
                        System.debug('Parenting 2---');
                        System.debug('related expression--' + relatedObject1 + '.' + relatedObject2 + '.' + fieldOfRelatedObj );
                        
                        if(obj.getSobject( relatedObject1 ) != null 
                        && obj.getSobject( relatedObject1 ).getSobject( relatedObject2 ) != null
                        && obj.getSobject( relatedObject1 ).getSobject( relatedObject2 ).get( fieldOfRelatedObj ) != null) {
                            return String.valueOf( obj.getSobject( relatedObject1 ).getSobject( relatedObject2 ).get( fieldOfRelatedObj ) );
                        } else {
                            return null;
                        }
                    }
                }
                else {
    
                    // 1 level parenting
                    System.debug('Parenting 1---');
                    System.debug('related expression--' + relatedObject1 + '.' + strAfter );
                    
                    if(obj.getSobject( relatedObject1 ) != null && obj.getSobject( relatedObject1 ).get( strAfter ) != null) {
                        return String.valueOf( obj.getSobject( relatedObject1 ).get( strAfter ) );
                    } else {
                        return null;
                    }
                }
            }
            else {
            
                if( obj.get( field_API_Name ) != null ) {
                    return String.valueOf( obj.get( field_API_Name ) );
                } else {
                    return null;
                }
            }
        }
    }