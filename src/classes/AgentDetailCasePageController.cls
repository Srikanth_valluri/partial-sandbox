public class AgentDetailCasePageController {
public case__c objCase {get;set;}    
    public AgentDetailCasePageController(){
        String caseID = apexpages.currentpage().getparameters().get('id');
        objCase = new case__C();
        if(String.isNotBlank(caseID )){
            objCase = [Select id,name,Booking_Number__c,No_of_Nights__c,
            Date_Time__c,Case_Description__c,Case_Subject__c,
            Project_Name__c,Unit_Number__c,Remarks__c,Drop_Off_Location__c,Inquiry_Number__c,
            Pick_Up_Location__c,Sales_Office__c
            from Case__c where id =: caseID];
        }
    }   
}