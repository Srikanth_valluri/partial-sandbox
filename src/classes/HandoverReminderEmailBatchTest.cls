/******************************************************************************
* Description - Test class developed for HandoverReminderEmailBatch
*
* Version            Date            Author                    Description
* 1.0                24/04/18        Lochan Karle            Initial Draft
********************************************************************************/
@isTest
private class HandoverReminderEmailBatchTest {
    
    static testMethod void testOne() {
         
        TriggerOnOffCustomSetting__c objOff = new TriggerOnOffCustomSetting__c ();
        objOff.Name = 'CallingListTrigger';
        objOff.OnOffCheck__c = true;
        insert objOff;
        
        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        Acc.Email__pc = 'abc@z.com';
        insert Acc;
        System.assert(Acc != null);
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);        
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Location__c loc = new Location__c();
        loc.Location_ID__c = '22656';
        loc.Name = 'JNU';
        loc.EHO_Notification_1_Email_Template__c = 'EHO';
        loc.EHO_Notification_2_Email_Template__c = 'EHO';
        loc.Notification_1_Email_Template__c = 'NHO';
        loc.Notification_2_Email_Template__c = 'NHO';
        insert loc;
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'JNU/WE/12';  
        BU.Registration_Status__c  = 'Active';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;
        BU.Approval_Status__c = 'HO';  
        insert BU;
        System.assert(BU != null);    
        
        Booking_Unit__c BU1 =  new Booking_Unit__c();
        BU1.Unit_Name__c = 'Test Units 123';  
        BU1.Registration_Status__c  = 'Active';
        BU1.Registration_ID__c   = '5678';  
        BU1.Unit_Selling_Price_AED__c  = 100;  
        BU1.Booking__c = Booking.Id;
        BU1.Approval_Status__c = 'EHO';  
        insert BU1;
 
        List<String> lstBUID= new List<String>();
        lstBUID.add(BU.Id);
        lstBUID.add(BU1.Id);
        
        String data = 'AVghfdihroegioesjrioglokstihrieuythrhjgurygugtufhvgr';
        ContentVersion cv = new ContentVersion();
        cv.ContentLocation = 'S';
        cv.PathOnClient = BU.Unit_Name__c + ' Handover Pack.zip';
        cv.Title = BU.Unit_Name__c + ' Handover Pack.zip';
        cv.VersionData = EncodingUtil.base64Decode(data);
        insert cv;
        
        ContentVersion cvNew = new ContentVersion();
        cvNew.ContentLocation = 'S';
        cvNew.PathOnClient = BU1.Unit_Name__c + ' EHO Offer.zip';
        cvNew.Title = BU1.Unit_Name__c + ' EHO Offer.zip';
        cvNew.VersionData = EncodingUtil.base64Decode(data);
        insert cvNew;
        
        ContentVersion cv1 = [select ContentDocumentId from ContentVersion where Id =: cv.Id];
        
        ContentVersion cv2 = [select ContentDocumentId from ContentVersion where Id =: cvNew.Id];
        
        ContentDocumentLink cl = new ContentDocumentLink(LinkedEntityId = BU.Id, ContentDocumentId = cv1.ContentDocumentId, ShareType = 'I');
        insert cl;
        
        ContentDocumentLink cl2 = new ContentDocumentLink(LinkedEntityId = BU1.Id, ContentDocumentId = cv2.ContentDocumentId, ShareType = 'I');
        insert cl2;
        test.startTest();
            Database.executeBatch( new HandoverReminderEmailBatch(lstBUID) );    
        test.stopTest();        
    }
}