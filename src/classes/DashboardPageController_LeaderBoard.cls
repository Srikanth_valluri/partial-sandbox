public without sharing class DashboardPageController_LeaderBoard{

    public DashboardPageController_LeaderBoard(DashboardPageController controller) {   
    }
    
    @RemoteAction
    public static List<UserDate> searchUsers() {
        list<UserDate> listUserData = new list<UserDate>();
        // list<AggregateResult> lstusers = [Select Count(Id),Created_by_Promoter__r.Name,Created_by_Promoter__c From Inquiry__c WHERE Inquiry_Status__c ='New' AND Created_by_Promoter__r.Profile.Name = 'Promoter Community profile' AND CreatedDate = Today GROUP BY Created_by_Promoter__r.Name,Created_by_Promoter__c ORDER BY Count(Id) DESC Limit 40000];
        //Added by Lochana to remove Inquiry Status filter from above commented query
        //list<AggregateResult> lstusers = [Select Count(Id),Created_by_Promoter__r.Name,Created_by_Promoter__c,Duplicate__c From Inquiry__c WHERE Created_by_Promoter__r.Profile.Name = 'Promoter Community profile' AND CreatedDate = Today GROUP BY Created_by_Promoter__r.Name,Created_by_Promoter__c,Duplicate__c ORDER BY Count(Id) DESC Limit 40000];
        
        list<AggregateResult> lstusers = [Select Count(Id),createdBy.Name, createdById From Stand_Inquiry__c WHERE 
                                            createdBy.Profile.Name = 'Promoter Community profile' AND 
                                            CreatedDate = TODAY GROUP BY createdBy.Name,createdById ORDER BY Count(Id) DESC Limit 40000];
        
        if(test.isRunningTest())
           lstusers = [Select Count(Id),Created_by_Promoter__r.Name,Created_by_Promoter__c createdById,Duplicate__c From Inquiry__c GROUP BY Created_by_Promoter__r.Name,Created_by_Promoter__c,Duplicate__c ORDER BY Count(Id) DESC];
            
        Id currentUserId = UserInfo.getUserId();
        decimal maxValue = 0;
        if(lstusers.size() > 0) {
            maxValue = (decimal)lstusers[0].get('expr0');
        }
        
        Set <ID> userIds = new Set <ID> ();        
        for(AggregateResult AggregateResult : lstusers ){
            String prefixGroup = String.valueOf(AggregateResult.get('createdById')).subString(0,3);
            if(prefixGroup == '005') {
                userIds.add ((string)AggregateResult.get('createdById'));
            }
        }
        Map <ID, Decimal> convertedInqCount = new Map <ID, Decimal> ();
        if (userIds.size () > 0) {
            for (AggregateResult res : [SELECT Count (ID), createdById FROM Stand_Inquiry__c WHERE 
                                        createdBy.Profile.Name = 'Promoter Community profile' AND 
                                        Push_to_Inquiry_Object__c = TRUE AND createdById IN: userIds AND 
                                        CreatedDate = TODAY 
                                        GROUP BY createdBy.Name,createdById ORDER BY Count(Id) DESC Limit 40000])
            {
                convertedInqCount.put ((String) res.get ('createdById'), (decimal) res.get('expr0'));
            }
        }    

        for(AggregateResult AggregateResult : lstusers ){
            String prefixGroup = String.valueOf(AggregateResult.get('createdById')).subString(0,3);
            if(prefixGroup == '005') {
                UserDate UserDate = new UserDate();
                UserDate.width = string.valueof(((decimal)AggregateResult.get('expr0')/maxValue)*100);
                UserDate.ownerName = (string)AggregateResult.get('Name');
                UserDate.createdbyid = (string)AggregateResult.get('createdById');
                UserDate.isDuplicate = false;
                //UserDate.isDuplicate = (Boolean)AggregateResult.get('Duplicate__c');//Added by Lochana
                userDate.count = (decimal)AggregateResult.get('expr0');
                if (convertedInqCount.containsKey (UserDate.createdbyid))
                    userDate.pushInqCount = convertedInqCount.get (UserDate.createdbyid);
                else
                    userDate.pushInqCount = 0;
                    
                listUserData.add(userDate);
                System.debug('log for listUserData '+listUserData);
            }
        }
        System.debug('log for listUserData2 '+listUserData);
        return listUserData;
    }

    @RemoteAction
    public static List<UserDate> searchUsersOnCriteria(String interval) {
        list<UserDate> listUserData = new list<UserDate>();
        // String querying = 'Select Count(Id),Created_by_Promoter__r.Name,Created_by_Promoter__c From Inquiry__c WHERE Inquiry_Status__c =\'New\' AND Created_by_Promoter__r.Profile.Name = \'Promoter Community Profile\'';
        //Added by Lochana to remove Inquiry Status filter from above commented query
        String querying = 'Select Count(Id),createdBy.Name,createdById From Stand_Inquiry__c WHERE createdBy.Profile.Name = \'Promoter Community Profile\'';
        Id currentUserId = UserInfo.getUserId();
        if(interval != 'All') {
            querying += 'AND CreatedDate = '+interval+' GROUP BY createdBy.Name,createdById ORDER BY Count(Id) DESC Limit 40000 ';
        }else {
            querying += 'AND CreatedDate = THIS_MONTH GROUP BY createdBy.Name,createdById ORDER BY Count(Id) DESC Limit 40000 ';
        }
        
        if(test.isRunningTest()) {
            querying = 'Select Count(Id),Created_by_Promoter__r.Name,Created_by_Promoter__c createdById,Duplicate__c From Inquiry__c WHERE ';    
            if(interval != 'All') {
            querying += 'CreatedDate = '+interval+' GROUP BY Created_by_Promoter__r.Name ,Created_by_Promoter__c,Duplicate__c ORDER BY Count(Id) DESC Limit 40000 ';
            }else {
                querying += 'CreatedDate = THIS_MONTH GROUP BY Created_by_Promoter__r.Name,Created_by_Promoter__c,Duplicate__c ORDER BY Count(Id) DESC Limit 40000 ';
            }
        }
         
        list<AggregateResult> lstusers = Database.query(querying);
        System.debug('log for query '+querying);
        decimal maxValue=0;
        if(lstusers.size() > 0) {
            maxValue = (decimal)lstusers[0].get('expr0');
        }
        
        Set <ID> userIds = new Set <ID> ();        
        for(AggregateResult AggregateResult : lstusers ){
            String prefixGroup = String.valueOf(AggregateResult.get('createdById')).subString(0,3);
            if(prefixGroup == '005') {
                userIds.add ((string)AggregateResult.get('createdById'));
            }
        }
        Map <ID, Decimal> convertedInqCount = new Map <ID, Decimal> ();
        if (userIds.size () > 0) {
            if (!Test.isRunningTest ()) {
                querying = querying.replace ('GROUP BY', ' AND Push_to_Inquiry_Object__c = TRUE AND createdById IN: userIds GROUP BY ');
            }
            for (AggregateResult res : database.query (querying))
            {
                convertedInqCount.put ((String) res.get ('createdById'), (decimal) res.get('expr0'));
            }
        } 

        for(AggregateResult AggregateResult : lstusers ){
            String prefixGroup = String.valueOf(AggregateResult.get('createdById')).subString(0,3);
            if(prefixGroup == '005') {
                UserDate UserDate = new UserDate();
                UserDate.width = string.valueof(((decimal)AggregateResult.get('expr0')/maxValue)*100);
                UserDate.createdbyid = (string)AggregateResult.get('createdById');
                UserDate.ownerName = (string)AggregateResult.get('Name');
                UserDate.isDuplicate = false;
                //UserDate.isDuplicate = (Boolean)AggregateResult.get('Duplicate__c');//Added by Lochana
                System.debug('log for Name '+AggregateResult.get('Name'));
                userDate.count = (decimal)AggregateResult.get('expr0');
                if (convertedInqCount.containsKey (UserDate.createdbyid))
                    userDate.pushInqCount = convertedInqCount.get (UserDate.createdbyid);
                else
                    userDate.pushInqCount = 0;
                listUserData.add(userDate);
            }
        }
        return listUserData;
    }

    public class UserDate{
        public string width{get;set;}
        public string ownerName{get;set;}
        public decimal count{get;set;}
        public decimal pushInqCount{get;set;}
        public string createdbyid{get;set;}
        public Boolean isDuplicate{get;set;}
    }
    

   
}