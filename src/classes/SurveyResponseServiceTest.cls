/*
* Test class for SurveyQuestionsService class
*/
@isTest
public class SurveyResponseServiceTest{
    /*
    *   When no account is found
    */
    @isTest
    public static void SurveyResponseServiceTest_NoAccount(){
        CreRoutingTest.createCREUsers('creTest@primaryUser.com', '7000');
        

        RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.requestUri = '/surveyQuestions';
        req.requestBody = Blob.valueOf('Portal');
        RestContext.request = req;
        RestContext.response = new RestResponse();
        Survey_CRM__c sCRM=new Survey_CRM__c();
        sCRM.Is_Active__c=true;
        sCRM.Type__c='Portal';
        sCRM.End_Date__c=Date.Today().addDays(10);
        sCRM.Start_Date__c=Date.Today().addDays(-10);
        insert sCRM;
        
        List<Survey_Question_CRM__c> questionList = new List<Survey_Question_CRM__c>();
        
        Survey_Question_CRM__c sQuestionRating1=new Survey_Question_CRM__c();
        sQuestionRating1.Name='Recommend DAMAC properties';
        sQuestionRating1.Question__c='How likely are you to recommend Damac Properties to a friend or colleague?';
        sQuestionRating1.Type__c='Rating';
        sQuestionRating1.Survey__c=sCRM.Id;
        sQuestionRating1.Required__c=false;
        sQuestionRating1.Order_Number_Displayed__c='1';
        sQuestionRating1.OrderNumber__c=1;
        sQuestionRating1.Choices__c='0\n1\n2\n3\n4\n5\n6\n7\n8\n9\n10';
        questionList.add(sQuestionRating1);     
        
        Survey_Question_CRM__c sQuestionRating=new Survey_Question_CRM__c();
        sQuestionRating.Name='Recommend DAMAC properties';
        sQuestionRating.Question__c='How likely are you to recommend Damac Properties to a friend or colleague?';
        sQuestionRating.Type__c='Picklist';
        sQuestionRating.Survey__c=sCRM.Id;
        sQuestionRating.Required__c=false;
        sQuestionRating.Order_Number_Displayed__c='2';
        sQuestionRating.OrderNumber__c=2;
        sQuestionRating.Choices__c='0\n1\n2\n3\n4\n5\n6\n7\n8\n9\n10';
        sQuestionRating.Choice_for_Additional_Textbox__c='0;1;2;3;4;5;6;7;8;';
        sQuestionRating.Additional_Description__c='0 - Not Likely at All\n10 - Extremely Likely';
        //insert sQuestionRating;
        questionList.add(sQuestionRating);
        
          
        //questionList.add(sQuestionRating1);
        
       /* Survey_Question_CRM__c sQuestionFreeText=new Survey_Question_CRM__c();
        sQuestionFreeText.Name='Please share any other comments you have';
        sQuestionFreeText.Question__c='Please share any other comments you have';
        sQuestionFreeText.Type__c='Free Text';
        sQuestionFreeText.Survey__c=sCRM.Id;
        sQuestionFreeText.Required__c=true;
        sQuestionFreeText.Order_Number_Displayed__c='3';
        sQuestionFreeText.OrderNumber__c=3;
        
        questionList.add(sQuestionFreeText);*/
        
        Survey_Question_CRM__c sQuestionFreeText1=new Survey_Question_CRM__c();
        sQuestionFreeText1.Name='Please share any other comments you have';
        sQuestionFreeText1.Question__c='Please share any other comments you have';
        sQuestionFreeText1.Type__c='Free Text';
        sQuestionFreeText1.Survey__c=sCRM.Id;
        sQuestionFreeText1.Required__c=false;
        sQuestionFreeText1.Order_Number_Displayed__c='4';
        sQuestionFreeText1.OrderNumber__c=4;        
        questionList.add(sQuestionFreeText1);
        
         Survey_Question_CRM__c sQuestioncheckBox=new Survey_Question_CRM__c();
        sQuestioncheckBox.Name='Please share any other comments you have';
        sQuestioncheckBox.Question__c='Please share any other comments you have';
        sQuestioncheckBox.Type__c='Multi-Select--Vertical';
        sQuestioncheckBox.Survey__c=sCRM.Id;
        sQuestioncheckBox.Required__c=false;
        sQuestioncheckBox.Order_Number_Displayed__c='5';
        sQuestioncheckBox.OrderNumber__c=5; 
        sQuestioncheckBox.Choices__c='Happy\nSatisfied\ntrue';      
        questionList.add(sQuestioncheckBox);
        
        Survey_Question_CRM__c sQuestionRadio=new Survey_Question_CRM__c();
        sQuestionRadio.Name='Please share any other comments you have';
        sQuestionRadio.Question__c='Please share any other comments you have';
        sQuestionRadio.Type__c='Single Select--Vertical';
        sQuestionRadio.Survey__c=sCRM.Id;
        sQuestionRadio.Required__c=false;
        sQuestionRadio.Order_Number_Displayed__c='6';
        sQuestionRadio.OrderNumber__c=6;
        sQuestionRadio.Choices__c='Happy\nSatisfied';       
        questionList.add(sQuestionRadio);
        
        Survey_Question_CRM__c sQuestion=new Survey_Question_CRM__c();
        sQuestion.Name='Share Feedback';
        sQuestion.Question__c='From your experience, please share your feedback by marking appropriate boxes below';
        sQuestion.Type__c='Section';
        sQuestion.Survey__c=sCRM.Id;
        sQuestion.Required__c=true;
        sQuestion.Order_Number_Displayed__c='7';
        sQuestion.OrderNumber__c=7;
        //sQuestion.Choices__c='Once;Twice;3 - 5 Times;More Than 5 Times;NA';
        questionList.add(sQuestion);
        
        Survey_Question_CRM__c sQuestionSelectRow=new Survey_Question_CRM__c();
        sQuestionSelectRow.Name='Times DAMAC Contacted';
        sQuestionSelectRow.Question__c='How many times did you contact / visit our customer service department before your problem was resolved?';
        sQuestionSelectRow.Type__c='Single Select--Horizontal';
        sQuestionSelectRow.Survey__c=sCRM.Id;
        sQuestionSelectRow.Required__c=false;
        sQuestionSelectRow.Order_Number_Displayed__c='3';
        sQuestionSelectRow.OrderNumber__c=3;
        sQuestionSelectRow.Choices__c='Once;Twice;3 - 5 Times;More Than 5 Times;NA';
        sQuestionSelectRow.Survey_Question__c=sQuestion.Id;
        questionList.add(sQuestionSelectRow);
        
        /*Survey_Question_CRM__c sQuestion1=new Survey_Question_CRM__c();
        sQuestion1.Name='Times DAMAC Contacted';
        sQuestion1.Question__c='How many times did you contact / visit our customer service department before your problem was resolved?';
        sQuestion1.Type__c='Single Select--Horizontal';
        sQuestion1.Survey__c=sCRM.Id;
        sQuestion1.Required__c=true;
        sQuestion1.Order_Number_Displayed__c='8';
        sQuestion1.OrderNumber__c=8;
        sQuestion1.Choices__c='Once;Twice;3 - 5 Times;More Than 5 Times;NA';
        sQuestion1.Survey_Question__c=sQuestion.Id;
        questionList.add(sQuestion1);*/
        
        insert questionList;
        
        
        
        
        Test.startTest();
        //SurveyQuestionsService.getsurveyQuestions();
        List<SurveyQuestionWrapper> questioInput=SurveyQuestionsService.getQuestion(sCRM);
        SurveyResponseService.submitResults(questioInput);
        List<SurveyQuestionWrapper> newList=new List<SurveyQuestionWrapper> ();
        for(SurveyQuestionWrapper inp: questioInput){
            if(inp.renderSelectCheckboxes){
                inp.selectedOptions.add('0');
                inp.selectedOptions.add('1');
                inp.selectedOptions.add('');
            }else{
            inp.selectedOption='0';
            }
            newList.add(inp);
        }
        SurveyResponseService.submitResults(newList);
        Test.stopTest();
        //String saveSurveyLogicResponse = RestContext.response.responseBody.toString();
        
   }
   
   @isTest
public static void SurveyResponseServiceTest_FreeText(){
Survey_CRM__c sCRM=new Survey_CRM__c();
        sCRM.Is_Active__c=true;
        sCRM.Type__c='Portal';
        sCRM.End_Date__c=Date.Today().addDays(10);
        sCRM.Start_Date__c=Date.Today().addDays(-10);
        insert sCRM;
    Survey_Question_CRM__c sQuestion1=new Survey_Question_CRM__c();
        sQuestion1.Name='Times DAMAC Contacted';
        sQuestion1.Question__c='How many times did you contact / visit our customer service department before your problem was resolved?';
        sQuestion1.Type__c='Free Text';
        sQuestion1.Survey__c=sCRM.Id;
        sQuestion1.Required__c=true;
        sQuestion1.Order_Number_Displayed__c='1';
        sQuestion1.OrderNumber__c=1;
        sQuestion1.Choices__c='Once;Twice;3 - 5 Times;More Than 5 Times;NA';
        //sQuestion1.Survey_Question__c=sQuestion.Id;
    insert sQuestion1;
    Test.startTest();
    List<SurveyQuestionWrapper> questioInput=SurveyQuestionsService.getQuestion(sCRM);
        system.debug('list.....'+questioInput);
        SurveyResponseService.submitResults(questioInput);
        Test.stopTest();
}
@isTest
public static void SurveyResponseServiceTest_PickList(){
Survey_CRM__c sCRM=new Survey_CRM__c();
        sCRM.Is_Active__c=true;
        sCRM.Type__c='Portal';
        sCRM.End_Date__c=Date.Today().addDays(10);
        sCRM.Start_Date__c=Date.Today().addDays(-10);
        insert sCRM;
    Survey_Question_CRM__c sQuestion1=new Survey_Question_CRM__c();
        sQuestion1.Name='Times DAMAC Contacted';
        sQuestion1.Question__c='How many times did you contact / visit our customer service department before your problem was resolved?';
        sQuestion1.Type__c='Picklist';
        sQuestion1.Survey__c=sCRM.Id;
        sQuestion1.Required__c=true;
        sQuestion1.Order_Number_Displayed__c='1';
        sQuestion1.OrderNumber__c=1;
        sQuestion1.Choices__c='Once;Twice;3 - 5 Times;More Than 5 Times;NA';
        //sQuestion1.Survey_Question__c=sQuestion.Id;
    insert sQuestion1;
    Test.startTest();
    List<SurveyQuestionWrapper> questioInput=SurveyQuestionsService.getQuestion(sCRM);
        system.debug('list.....'+questioInput);
        SurveyResponseService.submitResults(questioInput);
        Test.stopTest();
}
@isTest
public static void SurveyResponseServiceTest_Rating(){

Survey_CRM__c sCRM=new Survey_CRM__c();
        sCRM.Is_Active__c=true;
        sCRM.Type__c='Portal';
        sCRM.End_Date__c=Date.Today().addDays(10);
        sCRM.Start_Date__c=Date.Today().addDays(-10);
        insert sCRM;
    Survey_Question_CRM__c sQuestion1=new Survey_Question_CRM__c();
        sQuestion1.Name='Times DAMAC Contacted';
        sQuestion1.Question__c='How many times did you contact / visit our customer service department before your problem was resolved?';
        sQuestion1.Type__c='Rating';
        sQuestion1.Survey__c=sCRM.Id;
        sQuestion1.Required__c=true;
        sQuestion1.Order_Number_Displayed__c='1';
        sQuestion1.OrderNumber__c=1;
        sQuestion1.Choices__c='Once;Twice;3 - 5 Times;More Than 5 Times;NA';
        //sQuestion1.Survey_Question__c=sQuestion.Id;
    insert sQuestion1;
    Test.startTest();
    List<SurveyQuestionWrapper> questioInput=SurveyQuestionsService.getQuestion(sCRM);
        system.debug('list.....'+questioInput);
        SurveyResponseService.submitResults(questioInput);
        Test.stopTest();
}
@isTest
public static void SurveyResponseServiceTest_MSV(){

Survey_CRM__c sCRM=new Survey_CRM__c();
        sCRM.Is_Active__c=true;
        sCRM.Type__c='Portal';
        sCRM.End_Date__c=Date.Today().addDays(10);
        sCRM.Start_Date__c=Date.Today().addDays(-10);
        insert sCRM;
    Survey_Question_CRM__c sQuestion1=new Survey_Question_CRM__c();
        sQuestion1.Name='Times DAMAC Contacted';
        sQuestion1.Question__c='How many times did you contact / visit our customer service department before your problem was resolved?';
        sQuestion1.Type__c='Multi-Select--Vertical';
        sQuestion1.Survey__c=sCRM.Id;
        sQuestion1.Required__c=true;
        sQuestion1.Order_Number_Displayed__c='1';
        sQuestion1.OrderNumber__c=1;
        sQuestion1.Choices__c='Once;Twice;3 - 5 Times;More Than 5 Times;NA';
        //sQuestion1.Survey_Question__c=sQuestion.Id;
    insert sQuestion1;
    Test.startTest();
    List<SurveyQuestionWrapper> questioInput=SurveyQuestionsService.getQuestion(sCRM);
        system.debug('list.....'+questioInput);
        SurveyResponseService.submitResults(questioInput);
        Test.stopTest();
}
@isTest
public static void SurveyResponseServiceTest_SSV(){

Survey_CRM__c sCRM=new Survey_CRM__c();
        sCRM.Is_Active__c=true;
        sCRM.Type__c='Portal';
        sCRM.End_Date__c=Date.Today().addDays(10);
        sCRM.Start_Date__c=Date.Today().addDays(-10);
        insert sCRM;
    Survey_Question_CRM__c sQuestion1=new Survey_Question_CRM__c();
        sQuestion1.Name='Times DAMAC Contacted';
        sQuestion1.Question__c='How many times did you contact / visit our customer service department before your problem was resolved?';
        sQuestion1.Type__c='Single Select--Vertical';
        sQuestion1.Survey__c=sCRM.Id;
        sQuestion1.Required__c=true;
        sQuestion1.Order_Number_Displayed__c='1';
        sQuestion1.OrderNumber__c=1;
        sQuestion1.Choices__c='Once;Twice;3 - 5 Times;More Than 5 Times;NA';
        //sQuestion1.Survey_Question__c=sQuestion.Id;
    insert sQuestion1;
    Test.startTest();
    List<SurveyQuestionWrapper> questioInput=SurveyQuestionsService.getQuestion(sCRM);
        system.debug('list.....'+questioInput);
        SurveyResponseService.submitResults(questioInput);
        Test.stopTest();
}

@isTest
public static void SurveyResponseServiceTest_SSH(){

Survey_CRM__c sCRM=new Survey_CRM__c();
        sCRM.Is_Active__c=true;
        sCRM.Type__c='Portal';
        sCRM.End_Date__c=Date.Today().addDays(10);
        sCRM.Start_Date__c=Date.Today().addDays(-10);
        insert sCRM;
    Survey_Question_CRM__c sQuestion1=new Survey_Question_CRM__c();
        sQuestion1.Name='Times DAMAC Contacted';
        sQuestion1.Question__c='How many times did you contact / visit our customer service department before your problem was resolved?';
        sQuestion1.Type__c='Single Select--Horizontal';
        sQuestion1.Survey__c=sCRM.Id;
        sQuestion1.Required__c=true;
        sQuestion1.Order_Number_Displayed__c='1';
        sQuestion1.OrderNumber__c=1;
        sQuestion1.Choices__c='Once;Twice;3 - 5 Times;More Than 5 Times;NA';
        //sQuestion1.Survey_Question__c=sQuestion.Id;
    insert sQuestion1;
    Test.startTest();
    List<SurveyQuestionWrapper> questioInput=SurveyQuestionsService.getQuestion(sCRM);
        system.debug('list.....'+questioInput);
        SurveyResponseService.submitResults(questioInput);
        Test.stopTest();
}
}