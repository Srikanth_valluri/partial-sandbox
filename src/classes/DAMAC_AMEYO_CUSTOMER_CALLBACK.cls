public class DAMAC_AMEYO_CUSTOMER_CALLBACK {

    // Invocable method to get the customer id using process builder
    @InvocableMethod()
    Public static void setCustomerManagerCallBack (list<id> inquiryIds){
        setCustomerCallbackAsync (inquiryIds);
    }
    
    // Async transaction to get the customer id and sending the callback info
    @Future(Callout = TRUE)
    Public static void setCustomerCallbackAsync (list<id> inquiryIds){
           
        Ameyo_Credentials__c credentials = Ameyo_Credentials__c.getInstance(UserInfo.getUserID());
       
        // For Callback API
        DAMAC_AMEYO_CALLBACK_JSON callBackObj = new DAMAC_AMEYO_CALLBACK_JSON();
        
        callBackObj.campaignId = credentials.Callback_campaign_id__c != NULL ? Integer.valueOf(credentials.Callback_campaign_id__c) : NULL;
        callBackObj.sessionId = DAMAC_AMEYO_CUSTOMER.checkSession ();
        callBackObj.isSelfCallBack = 'false';
        callBackObj.callBackHandlerType = 'voice.campaign.callback.handler';
        callBackObj.userId = 'apiuser';    
        
        DAMAC_AMEYO_CALLBACK_JSON.cls_callBackProperties callBackProp = new DAMAC_AMEYO_CALLBACK_JSON.cls_callBackProperties();
        for (Inquiry__c inq:[SELECT Full_Name__c, Owner.Name, Is_Owner_Queue__c,Ameyo_Callback_Id__c, //Owner_Ameyo_lead_id__c 
                            Ameyo_Customer_Id__c, Call_back_Date_time__c,
                            Ameyo_Campaign_Id__c, Encrypted_Mobile__c FROM Inquiry__c WHERE id in :inquiryIds]){
            
            // Callback API
            if (inq.Call_back_Date_time__c != NULL){
                datetime dt = inq.Call_back_Date_time__c.addMinutes(integer.valueof(credentials.callback_in_minutes__c));
                string  st=dt.format('dd-MM-yyyy HH:mm:ss');
                callBackObj.callBackTime = st;
            }
            callBackProp.customerId = inq.Ameyo_Customer_Id__c;
            callBackProp.phone = inq.Encrypted_Mobile__c;
        }
        
        callBackObj.callBackProperties = callBackProp;
        
        String callbackReqBody = JSON.serialize (callBackObj);
        System.Debug('::: Callback :::'+callBackReqBody);
        
            
        // HTTP request to send the callback information.
        HTTPRequest callBackReq = new HTTPRequest ();
        callBackReq.setEndpoint (credentials.Endpoint__c+'?command=addCallback');
        callBackReq.setHeader ('Content-Type', 'application/x-www-form-urlencoded');
        callBackReq.setHeader ('hash-key', credentials.hash_key__c);
        callBackReq.setHeader ('Host', credentials.host__c);
        callBackReq.setHeader ('policy-name', credentials.policy_name__c);
        callBackReq.setHeader ('requesting-host', credentials.requesting_host__c);
        callBackReq.setTimeout (120000);
        callBackReq.setMethod ('POST');
        System.debug ('Request Body::::'+callbackReqBody);
        callBackReq.setBody ('data='+EncodingUtil.URLEncode(callbackReqBody, 'UTF-8'));
        HTTP http = new HTTP ();
        HTTPResponse callbackRes = new HTTPResponse ();
        if (!Test.isRunningTest ()){
            callbackRes = http.send (callBackReq);
        }else {
            callbackRes.setStatusCode (200);
            callbackRes.setBody ('{"beanResponse":{"id":"d462-5dbfbbdb-cm-23599","campaignId":12,"callBackTime":"2019-11-27 09:59:51.617","selfCallback":false,"callBackProperties":{"customerId":"8645996","phone":"dcecc9c3a4c0f068e3bacc75b450d45e"},"userId":"apiuser","phone":"dcecc9c3a4c0f068e3bacc75b450d45e","customerId":8645996}}');
        }
        
        System.Debug ('::::: Call Back Response::::'+callbackRes.getStatusCode()+'::::'+callbackRes.getBody());
        List <Inquiry__c> inqToUpdate = new List <Inquiry__c> ();
        for (Inquiry__c inq :[SELECT Ameyo_Customer_Id__c FROM Inquiry__c WHERE ID IN :inquiryIds]) {
            if (callBackRes.getStatusCode () == 200){ 
                DAMAC_AMEYO_CALLBACK_RESPONSE respcls = DAMAC_AMEYO_CALLBACK_RESPONSE.PARSE(callbackRes.getBody());     
                inq.Ameyo_Callback_Id__c = respcls.BeanResponse.id;
                inqToUpdate.add (inq);
            }
        }
        if (InqToUpdate.size () > 0) {
            DAMAC_Constants.skip_InquiryTrigger = true;
            Update inqToUpdate;
        }
        
        
    }
    
}