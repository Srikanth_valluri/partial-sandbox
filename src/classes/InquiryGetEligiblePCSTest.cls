@IsTest
public class InquiryGetEligiblePCSTest {
    static testmethod void method01() {
        
        Inquiry_User_Assignment_Rules__c ua1 = new Inquiry_User_Assignment_Rules__c ();
        ua1.Daily__c = 2;
        ua1.Monthly__c =3;
        ua1.Weekly__c =5;
        insert ua1;
        
        Campaign__c camp = new Campaign__c ();
        camp.Start_Date__c = system.today().adddays(-3);
        camp.End_Date__c =  system.today().adddays(3);
        camp.Marketing_Start_Date__c =  system.today().adddays(-3);
        camp.Marketing_End_Date__c =  system.today().adddays(3);
        camp.PCAssignment__c = true;
        insert camp;
        
        Inquiry_Assignment_Rules__c ua = new Inquiry_Assignment_Rules__c ();
        ua.Priority__c = 1;
        ua.Active__C = true;
        ua.Inquiry_Record_Type__c = 'Inquiry';
        ua.Fields_to_Apply_Rule__c = 'RecordTypeId';
        ua.Execute_on__c = 'Create';
        insert ua;
        
        Assigned_PC__c assign = new Assigned_PC__c ();
        assign.User__c = userinfo.getUserId();
        assign.Campaign__c = camp.Id;
        insert assign;
        
        Id inquiryRecId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        LIST<Inquiry__c> enquirylist = new LIST<Inquiry__c> ();
        Inquiry__c inquiry = new Inquiry__c();
        inquiry.First_Name__c = 'test';
        inquiry.RecordTypeId = inquiryRecId;
        inquiry.Last_Name__c = 's';
        inquiry.Primary_Contacts__c = 'Mobile Phone';
        inquiry.Preferred_Language__c = 'French';
        inquiry.Inquiry_Source__c = 'Chat';
        inquiry.Mobile_Phone__c = '1234567890';
        inquiry.Mobile_CountryCode__c = 'India: 0091';
        inquiry.Email__c = 'test@test.com';
        inquiry.Mobile_Phone_Encrypt__c = '8734637856';
        inquiry.Class__c = 'Priority Digital';
        enquirylist.add(inquiry);
        insert enquirylist;
        
        InquiryGetEligiblePCS pcs = new InquiryGetEligiblePCS();
        //pcs.getEligiblePCSfromQueue(enquirylist);
        pcs.getEligiblsPCS(enquirylist);
       
    }
    static testmethod void method02 () {
        
        Inquiry_User_Assignment_Rules__c ua1 = new Inquiry_User_Assignment_Rules__c ();
        ua1.Daily__c = 2;
        ua1.Monthly__c =3;
        ua1.Weekly__c =5;
        insert ua1;
        
        Campaign__c camp = new Campaign__c ();
        camp.Start_Date__c = system.today().adddays(-3);
        camp.End_Date__c =  system.today().adddays(3);
        camp.Marketing_Start_Date__c =  system.today().adddays(-3);
        camp.Marketing_End_Date__c =  system.today().adddays(3);
        camp.PCAssignment__c = true;
        camp.Marketing_Active__c = true;
        camp.Credit_Control_Active__c = true;
        camp.Sales_Admin_Active__c = true;
        insert camp;
        
        Inquiry_Assignment_Rules__c ua = new Inquiry_Assignment_Rules__c ();
        ua.Priority__c = 1;
        ua.Active__C = true;
        ua.Inquiry_Record_Type__c = 'Inquiry';
        ua.Fields_to_Apply_Rule__c = 'RecordTypeId';
        ua.is_Campaign_Active__c = 'Yes';
        ua.Execute_on__c = 'Create';
        insert ua;
        
        Assigned_PC__c assign = new Assigned_PC__c ();
        assign.User__c = userinfo.getUserId();
        assign.Campaign__c = camp.Id;
        insert assign;
        
        Id inquiryRecId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        LIST<Inquiry__c> enquirylist = new LIST<Inquiry__c> ();
        Inquiry__c inquiry = new Inquiry__c();
        inquiry.First_Name__c = 'test';
        inquiry.RecordTypeId = inquiryRecId;
        inquiry.Last_Name__c = 's';
        inquiry.Primary_Contacts__c = 'Mobile Phone';
        inquiry.Preferred_Language__c = 'French';
        inquiry.Inquiry_Source__c = 'Chat';
        inquiry.Mobile_Phone__c = '1234567890';
        inquiry.Mobile_CountryCode__c = 'India: 0091';
        inquiry.Email__c = 'test@test.com';
        inquiry.Mobile_Phone_Encrypt__c = '8734637856';
        inquiry.Class__c = 'Priority Digital';
        inquiry.campaign__c = camp.id;
        enquirylist.add(inquiry);
        insert enquirylist;
        
        InquiryGetEligiblePCS pcs = new InquiryGetEligiblePCS();
        //pcs.getEligiblePCSfromQueue(enquirylist);
        pcs.getEligiblsPCS(enquirylist);
       
    }
    static testmethod void method03 () {
        
        Inquiry_User_Assignment_Rules__c ua1 = new Inquiry_User_Assignment_Rules__c ();
        ua1.Daily__c = 2;
        ua1.Monthly__c =3;
        ua1.Weekly__c =5;
        insert ua1;
        
        Campaign__c camp = new Campaign__c ();
        camp.Start_Date__c = system.today().adddays(-3);
        camp.End_Date__c =  system.today().adddays(-2);
        camp.Marketing_Start_Date__c =  system.today().adddays(-3);
        camp.Marketing_End_Date__c =  system.today().adddays(-2);
        camp.PCAssignment__c = true;
        insert camp;
        
        Inquiry_Assignment_Rules__c ua = new Inquiry_Assignment_Rules__c ();
        ua.Priority__c = 1;
        ua.Active__C = true;
        ua.Inquiry_Record_Type__c = 'Inquiry';
        ua.Fields_to_Apply_Rule__c = 'RecordTypeId';
        ua.is_Campaign_Active__c = 'No';
        ua.Execute_on__c = 'Create';
        insert ua;
        
        Assigned_PC__c assign = new Assigned_PC__c ();
        assign.User__c = userinfo.getUserId();
        assign.Campaign__c = camp.Id;
        insert assign;
        
        Id inquiryRecId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        LIST<Inquiry__c> enquirylist = new LIST<Inquiry__c> ();
        Inquiry__c inquiry = new Inquiry__c();
        inquiry.First_Name__c = 'test';
        inquiry.RecordTypeId = inquiryRecId;
        inquiry.Last_Name__c = 's';
        inquiry.Primary_Contacts__c = 'Mobile Phone';
        inquiry.Preferred_Language__c = 'French';
        inquiry.Inquiry_Source__c = 'Chat';
        inquiry.Mobile_Phone__c = '1234567890';
        inquiry.Mobile_CountryCode__c = 'India: 0091';
        inquiry.Email__c = 'test@test.com';
        inquiry.Mobile_Phone_Encrypt__c = '8734637856';
        inquiry.Class__c = 'Priority Digital';
        inquiry.campaign__c = camp.id;
        enquirylist.add(inquiry);
        insert enquirylist;
        
        InquiryGetEligiblePCS pcs = new InquiryGetEligiblePCS();
        //pcs.getEligiblePCSfromQueue(enquirylist);
        pcs.getEligiblsPCS(enquirylist);
       
    }
}