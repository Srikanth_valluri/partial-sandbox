@isTest
public class DAMAC_SMS_Request_ObjTest {
    public static testMethod void method1() {
        String json=        '{  '+
        '   "SenderName":"DAMAC",'+
        '   "ScheduledDate":"",'+
        '   "ReturnIndividualResponse":false,'+
        '   "RemoveDuplicates":false,'+
        '   "ReferenceName":"",'+
        '   "MobileNumbers":[  '+
        '      "97100000000"'+
        '   ],'+
        '   "Message":"Test SMS From VF",'+
        '   "AsynchronousSubmission":true'+
        '}'+
        '';
        DAMAC_SMS_REQUEST_OBJ obj = DAMAC_SMS_REQUEST_OBJ.parse(json);
        System.assert(obj != null);
    }
}