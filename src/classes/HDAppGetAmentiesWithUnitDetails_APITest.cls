@isTest
public class HDAppGetAmentiesWithUnitDetails_APITest {
    @isTest
    static void testMainResponseCreator(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getUnitDetailsWithAmenities';  
       	//req.addParameter('regId', '');
        //Blob requestBody = Blob.valueOf('requestString');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
            HDAppGetAmentiesWithUnitDetails_API.MainResponseCreator();
        Test.stopTest();
    }
    
     @isTest
    static void testMainResponseCreator2(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getUnitDetailsWithAmenities';  
       	req.addParameter('regId', '');
        //Blob requestBody = Blob.valueOf('requestString');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
            HDAppGetAmentiesWithUnitDetails_API.MainResponseCreator();
        Test.stopTest();
    }
    
    @isTest
    static void testMainResponseCreator3(){
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        insert objBooking;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'Janusia';
        insert locObj;

        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        invObj.Marketing_Name_Doc__c = invObj.Id;
        insert invObj;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.DLP_End_Date__c = Date.today().addDays(2);
        objBU.Registration_ID__c = '3901';
        objBU.Booking__c = objBooking.Id;
        objBu.Inventory__c = invObj.Id;
        objBu.Agreement_Date__c = Date.newInstance(2020, 01, 30);
        objBu.Registration_DateTime__c = DateTime.newInstance(2020, 03, 30); // Registration_Date__c is formula field
        objBu.PCC_Date__c = Date.newInstance(2020, 06, 30);
        objBu.Eligible_for_Handover_Notice_Date__c = Date.newInstance(2020, 07, 10);
        objBu.Handover_Notice_Sent_Date__c = Date.newInstance(2020, 07, 10);
        objBu.Key_Release_Date__c = Date.newInstance(2020, 08, 10);
        objBu.Early_Handover_Date__c = Date.newInstance(2020, 09, 10);
        objBu.Handover_Date__c = Date.newInstance(2020, 09, 10);
        objBu.Lease_Handover_Date__c = Date.newInstance(2020, 09, 10);
        insert objBU;
        
		Marketing_Documents__c objMarketing = new Marketing_Documents__c();
		insert objMarketing;
        
        Property__c objProperty = new Property__c();
        objProperty.Property_ID__c = 125.0;
        insert objProperty;
        
        Project_Information__c objProject = new Project_Information__c();
        objProject.Project_Features__c = 'Beach;Cafes';
        objProject.Marketing_Documents__c = objMarketing.Id;
        objProject.Property__c = objProperty.Id;
        insert objProject;
        
        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;
        
        List<FM_Case__c> fmCases = new List<FM_Case__c>();
        
        FM_Case__c fmCaseObj1=new FM_Case__c();
        fmCaseObj1.Issue_Date__c=Date.today();
        fmCaseObj1.Move_in_date__c=Date.today().addMonths(3);
        fmCaseObj1.Contact_person__c='test1';
        fmCaseObj1.Description__c='test1';
        fmCaseObj1.Contact_person_contractor__c='test1';
        fmCaseObj1.Nationality__c ='Pakistani';
        fmCaseObj1.Booking_Unit__c = objBU.Id ;
        fmCaseObj1.Tenant__c = acctIns.Id ;
        fmCaseObj1.Account__c = acctIns.Id ;
        fmCaseObj1.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Registration').getRecordTypeId();
        fmCaseObj1.Request_Type__c = 'Tenant Registration';
        fmCaseObj1.Status__c = 'Tenant Registered; Awaiting Move in confirmation';
        fmCases.add(fmCaseObj1);
        
        FM_Case__c fmCaseObj2=new FM_Case__c();
        fmCaseObj2.Issue_Date__c=Date.today();
        fmCaseObj2.Move_in_date__c=Date.today().addMonths(4);
        fmCaseObj2.Contact_person__c='test2';
        fmCaseObj2.Description__c='test2';
        fmCaseObj2.Contact_person_contractor__c='test2';
        fmCaseObj2.Nationality__c ='Pakistani';
        fmCaseObj2.Booking_Unit__c = objBU.Id ;
        fmCaseObj2.Tenant__c = acctIns.Id ;
        fmCaseObj2.Account__c = acctIns.Id ;
        fmCaseObj2.Request_Type__c = 'Move In';
        fmCaseObj2.Status__c = 'Closed';
        fmCases.add(fmCaseObj2);
        
        FM_Case__c fmCaseObj3=new FM_Case__c();
        fmCaseObj3.Issue_Date__c=Date.today();
        fmCaseObj3.Expected_move_out_date__c=Date.today().addMonths(5);
        fmCaseObj3.Contact_person__c='test3';
        fmCaseObj3.Description__c='test3';
        fmCaseObj3.Contact_person_contractor__c='test3';
        fmCaseObj3.Nationality__c ='Pakistani';
        fmCaseObj3.Booking_Unit__c = objBU.Id ;
        fmCaseObj3.Tenant__c = acctIns.Id ;
        fmCaseObj3.Account__c = acctIns.Id ;
        fmCaseObj3.Request_Type__c = 'Move Out';
        fmCaseObj3.Status__c = 'Closed';
        fmCases.add(fmCaseObj3);
        
        insert fmCases;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getUnitDetailsWithAmenities';  
       	req.addParameter('regId', objBU.Registration_ID__c);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
            HDAppGetAmentiesWithUnitDetails_API.MainResponseCreator();
        Test.stopTest();
    }
}