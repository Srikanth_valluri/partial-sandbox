/**********************************************************************************************************************
Description: This API is used for creating Access Card FM Case & performting other required operations raised from 
Damac Living App. 
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   02-11-2020      | Shubham Suryawanshi | Created initial draft
1.1     |   29-11-2020      | Shubham Suryawanshi | Bug fixes & updated the error message for exception case
***********************************************************************************************************************/
@RestResource(urlMapping='/accessCardSROps/*')
global class HDApp_AccessCardSR_API {

    public static String errorMsg;
    public static Integer statusCode;
    public static String unitRegId;
    public static String partyId;
    public static Decimal vatRate = Decimal.valueOf(label.Access_Card_VAT_Percent);
    public static final String commonErrorMsg = 'This may be because of technical error that we\'re working to get fixed. Please try again later.';

    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };

    public static Map<Integer, String> mapIdToCardname = new Map<Integer, String>{
        1 => 'Parking',
        2 => 'Building',
        3 => 'Amenities',
        4 => 'Entrance',
        5 => 'Facility'
    };


    /**********************************************************************************************************************
    Description : Method to create Draft access card SR
    Parameter(s):  NA
    Return Type : FinalReturnWrapper 
    Remark : Earlier this method was @HttpPost, updated to @HttpPut on 03/11/2020
    **********************************************************************************************************************/
    @HttpPut
    global static FinalReturnWrapper createDraftAccessCardSR() {

        RestRequest req = RestContext.request;
        Blob body = req.requestBody;
        String jsonString = body.toString();
        System.debug('jsonString'+jsonString);
        System.debug('Request params:' + req.params);

        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        FmCaseWrapper objFmWrap = new FmCaseWrapper(); 
        cls_meta_data objMeta = new cls_meta_data();
        cls_data objData = new cls_data();

        if(String.isBlank(jsonString)) {
            objMeta = ReturnMetaResponse('No JSON body found in request','No JSON body found in request', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        System.debug('jsonString:: ' + jsonString);
        objFmWrap = (FmCaseWrapper)JSON.deserialize(jsonString, FmCaseWrapper.class);
        System.debug('objFmWrap: ' + objFmWrap);

        if(objFmWrap.action == null || (!objFmWrap.action.equalsIgnoreCase('draft') )) {
            objMeta = ReturnMetaResponse('Internal processing error','Invalid parameter value in : action', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;

        }

        if(objFmWrap != null) {
            System.debug('action in InsertTenantRegCase: ' + objFmWrap.action);

            FM_Case__c objFM = ProcessFMCase(objFmWrap, objFmWrap.action);
            System.debug('objFM: ' + objFm);

            List<FM_Case__c> accessCardCase = objFM != null ? GetTenantRegistrationCase_API.getFMCase(objFM.id) : null;
            objData = accessCardCase != null && !accessCardCase.isEmpty() ? clsDataCreator(accessCardCase[0]) : null;

            System.debug('errorMsg:' + errorMsg);
            System.debug('statusCode:' + statusCode);
            objMeta = errorMsg == 'Successful'? ReturnMetaResponse(errorMsg,errorMsg, statusCode) : ReturnMetaResponse(commonErrorMsg, errorMsg, statusCode);
        }
        else {
            objMeta = String.isNotBlank(errorMsg) && statusCode != 1 ? ReturnMetaResponse(errorMsg, errorMsg, statusCode) : ReturnMetaResponse('No FM Case found/Created', '', 2);
        }

        returnResponse.data = objData;
        returnResponse.meta_data = objMeta;

        return returnResponse;
    }

    public Static FM_Case__c ProcessFMCase( FmCaseWrapper objFmCaseWrap, String action ) {

        Integer noOfCards = 0;  //to store total number of cards being issued;
        Booking_Unit__c unit = new Booking_Unit__c();
        if(String.isNotBlank(objFmCaseWrap.booking_unit_id)) {
            unit = getUnitDetails(objFmCaseWrap.booking_unit_id);
        }
        else {
            errorMsg = 'Missing parameter value : booking_unit_id in JSON request body';
            statusCode = 3;
            return null;
        }

        if(unit == null) {

            errorMsg = 'No booking unit found for given booking_unit_id';
            statusCode = 3;
            return null;
        }

        if(String.isBlank(objFmCaseWrap.account_id)) {
            errorMsg = 'No account_id found in request';
            statusCode = 3;
            return null;
        }

        Account objAcc = HDApp_MoveOutSR_API.getCurrentAccountDetails(objFmCaseWrap.account_id);
        System.debug('objAcc:: ' + objAcc);

        System.debug('objFmCaseWrap:: ' + objFmCaseWrap);
        FM_Case__c objCase = new FM_Case__c();

        if(String.isNotBlank(objFmCaseWrap.id)) {       //When case is already created
            objCase.id = objFmCaseWrap.id;
        }

        //Populating common entities
        Boolean isTenant = unit.Tenant__c == objFmCaseWrap.account_id ? true : false;
        objCase.isHelloDamacAppCase__c = true;
        objCase.Origin__c='Portal';
        objCase.Status__c='Draft Request';
        Id devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get(
                                'Request For Access Card').getRecordTypeId();
        objCase.RecordTypeId = devRecordTypeId;//Set record type
        
        //objCase.Initiated_by_tenant_owner__c = isTenant ? 'tenant' : 'owner';
        objCase.Request_Type_DeveloperName__c = isTenant ? 'Request_for_Access_cards_T' : 'Request_for_Access_cards';
        objCase.Tenant_email__c = objAcc.Email__pc;
        objCase.Tenant__c = objAcc.Id;
        objCase.Booking_Unit__c  = unit.id;
        objCase.Request_Type__c = 'Request For Access Card';
        //objCase.Building_Name__c = objLoc != null ? objLoc.Name : null;        
        objCase.Account__c = unit.Booking__r.Account__c;

        //For getting FM Admin to stamp on Admin__c fild of FM Case
        Location__c objLoc = new Location__c();
        if(String.isNotBlank(unit.Unit_Name__c)) {
            String buildingName = unit.Unit_Name__c.split('/')[0];
            List<Location__c> lstBuildings = new List<Location__c>();
            lstBuildings = [SELECT Id
                                  , Name
                                  , Property_Name__c
                                  , Community_Name__c
                                  , Access_Card_Replacement_Fee__c
                                  , New_Access_Card_Fee__c
                                  , (SELECT FM_User__c
                                            , FM_User__r.Email
                                            , FM_Role__c
                                            , FM_User__r.Name
                                     FROM FM_Users__r)
                            FROM Location__c
                            WHERE Name = :buildingName
                            AND RecordType.DeveloperName = 'Building'
                            LIMIT 1 ];
            if(lstBuildings.size() > 0) {
                objLoc = lstBuildings[0];
                if(objLoc.FM_Users__r.size() > 0) {
                    for(FM_User__c obj :objLoc.FM_Users__r) {
                        if(obj.FM_Role__c == 'FM Admin'
                                    || obj.FM_Role__c == 'Master Community Admin') {
                            objCase.Admin__c = obj.FM_User__c;
                        }
                    }//for
                }
            }
        }

        if(action.equalsIgnoreCase('draft')) {
            objCase.Status__c = 'Draft Request';
        }

        //Populating Access-Card SR specific fields
        if(objFmCaseWrap.selected_access_card_details != null && objFmCaseWrap.selected_access_card_details.size() > 0) {
            for(AccessCardCountDetailsWrap obj : objFmCaseWrap.selected_access_card_details){
                //objCase.Access_Card_Permission__c = mapIdToCardname.containskey(obj.card_id) ? mapIdToCardname.get(obj.card_id) + ';' : '';
                if(mapIdToCardname.containskey(obj.card_id)) {
                    objCase.Access_Card_Permission__c += mapIdToCardname.get(obj.card_id) + '; ';
                }
                else {
                    errorMsg = 'Invalid card_id passed in request';
                    statusCode = 3;
                    return null;    
                }
                noOfCards += obj.number_of_cards;
                /*IMPORTANT*/
                //Need to add logic for storing card count against each card_id;
            }
        }


        System.debug('objFmCaseWrap.type_of_request: ' + objFmCaseWrap.type_of_request);
        if(String.isNotBlank(objFmCaseWrap.type_of_request) && objFmCaseWrap.type_of_request.containsIgnoreCase('New') ) {
            objCase.Type_of_Access_Card__c = 'New Request';
            objCase.New_Access_Card_Fee__c = getAccessCardsAmountwithVAT(getTotalAmountForAccessCards(objCase.Type_of_Access_Card__c, noOfCards),vatRate);
        }
        else if(String.isNotBlank(objFmCaseWrap.type_of_request) && objFmCaseWrap.type_of_request.containsIgnoreCase('Replace') ) {
            objCase.Type_of_Access_Card__c = 'Replacement';
            objCase.Access_Card_Replacement_Fee__c = getAccessCardsAmountwithVAT(getTotalAmountForAccessCards(objCase.Type_of_Access_Card__c, noOfCards),vatRate);
        }


        System.debug('objCase.Access_Card_Permission__c: ' + objCase.Access_Card_Permission__c);
        if(String.isNotBlank(objCase.Access_Card_Permission__c)) {
            objCase.Access_Card_Permission__c = objCase.Access_Card_Permission__c.removeEnd('; ').remove('null').trim();
            System.debug('objCase.Access_Card_Permission__c after removal of ; : ' + objCase.Access_Card_Permission__c);
        }

        try {
            System.debug('objCase before insert: ' + objCase);
            upsert objCase;
            errorMsg = 'Successful';
            statusCode = 1;
            System.debug('objCase after insert: ' + objCase);
        }
        catch(Exception e) {
            System.debug('Exception: ' + e.getMessage());
            errorMsg = e.getMessage();
            statusCode = 6;
        }

        return objCase;

    }

    public static cls_data clsDataCreator(FM_Case__c objFM) {
        System.debug('objFM in clsDataCreator : ' + objFM);
        if(objFM == null) {
            return null;
        }
        cls_data dataObj = new cls_data();

        dataObj.booking_unit_id = String.isNotBlank(objFM.Booking_Unit__c) ? objFM.Booking_Unit__c : null;
        dataObj.booking_unit_name = String.isNotBlank(objFM.Unit_Name__c) ? objFM.Unit_Name__c : null;
        dataObj.sr_id = objFM.id;
        dataObj.sr_number = objFM.Name;
        dataObj.sr_type = 'Access card';
        dataObj.sr_status = objFM.Status__c;
        dataObj.sr_submission_date = objFM.Submission_Date__c != null ? Datetime.newInstance(objFM.Submission_Date__c.year(), objFM.Submission_Date__c.month(), objFM.Submission_Date__c.day()).format('yyyy-MM-dd') : '';
        dataObj.sr_approval_status = objFM.Approval_Status__c != null ? objFM.Approval_Status__c : 'Pending';
        if(String.isNotBlank(objFM.Type_of_Access_Card__c) && objFM.Type_of_Access_Card__c == 'New Request') {
            dataObj.amount = objFM.New_Access_Card_Fee__c != null ? objFM.New_Access_Card_Fee__c : null;
        }
        else if(String.isNotBlank(objFM.Type_of_Access_Card__c) && objFM.Type_of_Access_Card__c == 'Replacement') {
            dataObj.amount = objFM.Access_Card_Replacement_Fee__c != null ? objFM.Access_Card_Replacement_Fee__c : null;
        }
        dataObj.card_type = objFM.Access_Card_Permission__c != null ? objFM.Access_Card_Permission__c.replaceAll(';',', ') : null;
        list<cls_attachment> lstAttachments = new list<cls_attachment>();

        if(objFM.Documents__r.size() > 0) {
            System.debug('objFM.Documents__r'+objFM.Documents__r);
            for(SR_Attachments__c obj : objFM.Documents__r) {
                cls_attachment objAttach = new cls_attachment();
                objAttach.id = obj.id;
                objAttach.name = obj.Name;
                objAttach.file_extension = obj.Type__c;
                objAttach.attachment_url = obj.Attachment_URL__c;

                lstAttachments.add(objAttach);
            }
        }
        dataObj.attachments = lstAttachments;

        System.debug('dataObj:: ' + dataObj);
        return dataObj;

    }

    /**********************************************************************************************************************
    Description : Method to get the amount with calculation details for access cards being requested
    Parameter(s):  NA
    Return Type : FinalReturnWrapper 
    **********************************************************************************************************************/
    @HttpPost
    global static AmountDetailsReturnWrapper getAccessCardAmountDetails() {

        RestRequest req = RestContext.request;
        Blob body = req.requestBody;
        String jsonString = body.toString();
        System.debug('jsonString'+jsonString);
        //System.debug('Request params:' + req.params);

        AmountDetailsReturnWrapper returnResponse = new AmountDetailsReturnWrapper();
        cls_get_amount_details_input_wrap objInputWrap = new cls_get_amount_details_input_wrap(); 
        cls_meta_data objMeta = new cls_meta_data();
        cls_amount_data objData = new cls_amount_data();

        if(String.isBlank(jsonString)) {
            objMeta = ReturnMetaResponse('No JSON body found in request','No JSON body found in request', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        System.debug('jsonString:: ' + jsonString);
        objInputWrap = (cls_get_amount_details_input_wrap)JSON.deserialize(jsonString, cls_get_amount_details_input_wrap.class);
        System.debug('objInputWrap: ' + objInputWrap);

        if(String.isBlank(objInputWrap.booking_unit_id)) {
            objMeta = ReturnMetaResponse('Internal processing error','Missing parameter value: booking_unit_id', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        if(String.isBlank(objInputWrap.type_of_request)) {
            objMeta = ReturnMetaResponse('Internal processing error','Missing parameter value: type_of_request', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        Integer noOfCards = 0;
        List<cls_card_fee_details> lstCardFeeDetails = new List<cls_card_fee_details>();
        if(objInputWrap.selected_access_card_details != null && objInputWrap.selected_access_card_details.size() > 0) {
            for(AccessCardCountDetailsWrap obj : objInputWrap.selected_access_card_details){
                cls_card_fee_details objFeeReturn = new cls_card_fee_details();

                if(mapIdToCardname.containskey(obj.card_id)) {
                    objFeeReturn.card_type = mapIdToCardname.get(obj.card_id);
                    objFeeReturn.card_id = obj.card_id;
                    objFeeReturn.number_of_cards = obj.number_of_cards;
                    noOfCards += obj.number_of_cards;
                    if(objInputWrap.type_of_request.containsIgnoreCase('new')) {
                        objFeeReturn.fee_amount = ( Decimal.valueOf(obj.number_of_cards) * Decimal.valueOf(Label.New_Access_Card_Fees) );
                        System.debug('objFeeReturn.fee_amount: ' + objFeeReturn.fee_amount);
                    }
                    else if(objInputWrap.type_of_request.containsIgnoreCase('replace')) {
                        objFeeReturn.fee_amount = ( Decimal.valueOf(obj.number_of_cards) * Decimal.valueOf(Label.Access_Card_Replacement_Fees) );
                        System.debug('objFeeReturn.fee_amount: ' + objFeeReturn.fee_amount);
                    }
                }
                else {
                    objMeta = ReturnMetaResponse('Internal processing error','Invalid card_id passed in request', 3);
                    returnResponse.meta_data = objMeta;
                    return returnResponse;  
                }
                lstCardFeeDetails.add(objFeeReturn);
            }

            System.debug('noOfCards: ' + noOfCards);
            //Calculation for Sub-total & Total amount including VAT
            Decimal subTotal = getTotalAmountForAccessCards(objInputWrap.type_of_request, noOfCards);
            vatRate = Decimal.valueOf(label.Access_Card_VAT_Percent);
            Decimal total = getAccessCardsAmountwithVAT(subtotal,vatRate);

            cls_total_amount objTotal = new cls_total_amount();
            objTotal.subtotal = subtotal;
            objTotal.vat = vatRate;
            objTotal.total = total.setscale(2);

            objData.card_fee_details = lstCardFeeDetails;
            objData.total_amount = objTotal;
            objMeta = ReturnMetaResponse('Successful','', 1); 

            returnResponse.data = objData;
            returnResponse.meta_data = objMeta;

            System.debug('returnResponse: ' + returnResponse);
            return returnResponse;
        }
        else {
            objMeta = ReturnMetaResponse('Card quantity not passed','Card quantity not passed in request', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;  
        }

    }

    public static Decimal getTotalAmountForAccessCards(String reqType, Integer totalCards) {

        Decimal totalAmt;
        if(reqType.containsIgnoreCase('new')) {
            totalAmt = ( Decimal.valueOf(totalCards) * Decimal.valueOf(Label.New_Access_Card_Fees) );
            System.debug('totalAmt new: ' + totalAmt);
        }
        else if(reqType.containsIgnoreCase('replace')) {
            totalAmt = ( Decimal.valueOf(totalCards) * Decimal.valueOf(Label.Access_Card_Replacement_Fees) );
            System.debug('totalAmt replace: ' + totalAmt);
        }
        return totalAmt;
    }

    public static Decimal getAccessCardsAmountwithVAT(Decimal subTotal, Decimal vat) {
        System.debug('subTotal in getAccessCardsAmountwithVAT: ' + subTotal);
        Decimal total = ( (subtotal * vat)/100 ) + subtotal;

        System.debug('total:: ' + total);
        return total;
    }
    
    /**********************************************************************************************************************
    Description : Method to submit the created access card SR
    Parameter(s):  NA
    Return Type : FinalReturnWrapper 
    **********************************************************************************************************************/
    @httpPatch
    global static FinalReturnWrapper submitAccessCardSR() {

        RestRequest req = RestContext.request;
        Blob body = req.requestBody;
        String jsonString = body.toString();
        System.debug('jsonString'+jsonString);
        //System.debug('Request params:' + req.params);

        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        AccessCardSubmitSRWrap objSubmitWrap = new AccessCardSubmitSRWrap(); 
        cls_meta_data objMeta = new cls_meta_data();
        cls_data objData = new cls_data();

        if(String.isBlank(jsonString)) {
            objMeta = ReturnMetaResponse('No JSON body found in request','No JSON body found in request', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        System.debug('jsonString:: ' + jsonString);
        objSubmitWrap = (AccessCardSubmitSRWrap)JSON.deserialize(jsonString, AccessCardSubmitSRWrap.class);
        System.debug('objSubmitWrap: ' + objSubmitWrap);

        if(objSubmitWrap.action == null || (!objSubmitWrap.action.equalsIgnoreCase('submit') )) {
            objMeta = ReturnMetaResponse('Internal processing error','Invalid parameter value in : action', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        if(String.isBlank(objSubmitWrap.id)) {
            objMeta = ReturnMetaResponse('Internal processing error','Missing parameter value: id', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        List<FM_Case__c> accessCardCase = GetTenantRegistrationCase_API.getFMCase(objSubmitWrap.id);
        if( accessCardCase.isEmpty() || accessCardCase == null) {
            objMeta = ReturnMetaResponse('No case found','No any FM case found for given fmCaseId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;  
        }
        //Verify if NOC for Landlord doc is uploaded if current Account is Tenant
        Boolean isTenant = accessCardCase[0].Request_Type_DeveloperName__c == 'Request_for_Access_cards_T' ? true : false;
        if(isTenant && !HDApp_MoveOutSR_API.verifyIsNocDocUploaded(accessCardCase[0].id) ) {
            objMeta = ReturnMetaResponse('Mandatory document upload pending','', 2);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        //Populating access-card submit operation specific details
        accessCardCase[0].Approval_Status__c='Pending';
        accessCardCase[0].Submit_for_Approval__c = false;
        accessCardCase[0].Submitted__c = true;
        accessCardCase[0].Status__c='Submitted';
        accessCardCase[0].Access_Card_Payment_Receipt_No__c = objSubmitWrap.receipt_number != null ? objSubmitWrap.receipt_number : null;
        accessCardCase[0].Access_Card_Payment_Receipt_URL__c = objSubmitWrap.receipt_url != null ? objSubmitWrap.receipt_url : null;

        //Uploading doc to O365 & creating its corresponding Sr_Attachment record
        UploadMultipleDocController.data objResponse = new UploadMultipleDocController.data();
        list<UploadMultipleDocController.MultipleDocRequest> lstWrapper = new list<UploadMultipleDocController.MultipleDocRequest>();
        if(String.isNotBlank(objSubmitWrap.receipt_url)) {
            HttpRequest request = new HttpRequest();
            request.setEndpoint(objSubmitWrap.receipt_url);
            request.setHeader('Accept', 'application/json');
            request.setMethod('GET');
            //request.setHeader('Authorization','Bearer' + accessToken);
            request.setTimeout(120000);

            HttpResponse response = new HttpResponse();
            if(!Test.isRunningTest()) {
                response = new Http().send(request);
            }
            else {
                response = new HttpResponse();
            }
            
            System.debug('Status Code = ' + response.getStatusCode());
            System.debug('Pdf Body = ' + response.getBodyAsBlob());

            if( response.getStatusCode() == 200 && response.getBodyAsBlob() != null ) {
                String requestString = EncodingUtil.Base64Encode(response.getBodyAsBlob());
                if(string.isNotBlank( requestString ) ) {
                    lstWrapper.add( FM_Utility.makeWrapperObject(  requestString,
                                                                   'Access Card payment receipt' ,
                                                                   'Access card Pay receipt.pdf' ,
                                                                   accessCardCase[0].Name, '1' ) );
                }
                System.debug('== lstWrapper =='+lstWrapper);
            }
        }
        //Doc upload & SR Attchment creation logic - ENDS
        Booking_Unit__c objBU = getUnitDetails(accessCardCase[0].Booking_Unit__c);
        //Stamping outstanding service amount for given unit
        if(objBU != null) {

            /*Commented due to - Read timed out issue - Need to confirm from Shinu*/

            //try {
            //    //objFMCase.Account__c = objBU.Booking__r.Account__c;
            //    FmIpmsRestServices.DueInvoicesResult objResponse =
            //                    FmIpmsRestServices.getDueInvoices( objBU.Registration_Id__c,'',
            //                    objBU.Inventory__r.Property_Code__c);
            //    System.debug('>>>---objResponse--- : '+objResponse);
            //    if(objResponse != NULL) {
            //        accessCardCase[0].Outstanding_service_charges__c =
            //                !String.isBlank( objResponse.totalDueAmount )
            //                ? objResponse.totalDueAmount : '0';
            //        //allowInitiation = Decimal.valueof(objFMCase.Outstanding_service_charges__c) <= minimumOutstandingCharge ? true : false;
            //    }
            //}
            //catch(Exception e) {
            //    system.debug( '--Exception while fetching oustanding balance--'+e.getMessage());
            //    objMeta = ReturnMetaResponse('Internal operation failure',e.getMessage(), 6);
            //    returnResponse.meta_data = objMeta;
            //    return returnResponse;
            //}
        }
        
        //Inserting SR Attachment
        if( !lstWrapper.isEmpty() ) {
            objResponse = PenaltyWaiverService.uploadDocumentsOnCentralRepo( lstWrapper, 'FM' );
            system.debug('== objResponse document upload =='+objResponse);
            system.debug('== objResponse.data =='+objResponse.data);
            if( objResponse != NULL && objResponse.data != NULL ) {
                InsertAttachment_API.AttachmentWrapper objSRAttach = InsertAttachment_API.insertCustomAttachment( accessCardCase[0], 'Access Card payment receipt', objResponse, '', 'Access card Pay receipt.pdf');
                System.debug('objSRAttach:: ' + objSRAttach);
            }

        }

        //For stamping approving authorities
        list<FM_Approver__mdt> lstApprovers = new list<FM_Approver__mdt>();
        System.debug('--- objBU.Property_City__c --- : '+ objBU.Property_City__c);
        System.debug('--- accessCardCase[0].Request_Type_DeveloperName__c --- : '
                                        +accessCardCase[0].Request_Type_DeveloperName__c);
        if(objBU != NULL && accessCardCase[0].Request_Type_DeveloperName__c != NULL) {
            lstApprovers = FM_Utility.fetchApprovers(accessCardCase[0].Request_Type_DeveloperName__c,
                                                    objBU.Property_City__c);
        }
        System.debug('>>>---lstApprovers--- : '+lstApprovers);
        String approvingRoles = '';
        for(FM_Approver__mdt mdt : lstApprovers) {
            approvingRoles += mdt.Role__c + ',';
        }
        approvingRoles = approvingRoles.removeEnd(',');
        accessCardCase[0].Approving_Authorities__c = approvingRoles;
        System.debug('>>>---accessCardCase[0].Request_Type__c--- : '+accessCardCase[0].Request_Type__c);
        update accessCardCase;

        //Fetch FM case post update DML 
        List<FM_Case__c> lstSubmittedFmCase = GetTenantRegistrationCase_API.getFMCase(accessCardCase[0].id);
        objData = clsDataCreator(lstSubmittedFmCase[0]);
        objMeta = ReturnMetaResponse('Successful','', 1);

        returnResponse.data = objData;
        returnResponse.meta_data = objMeta;

        return returnResponse; 

    }


    public static Booking_Unit__c getUnitDetails(String buId) {

        List<Booking_Unit__c> lstUnit = [ SELECT Id
                                               , Unit_Name__c
                                               , Booking__r.Account__c
                                               , Booking__r.Account__r.Party_Id__c
                                               , Registration_Id__c
                                               , Property_City__c
                                               , Resident__c
                                               , Tenant__c
                                               , Inventory__r.Property_Code__c
                                         FROM Booking_Unit__c
                                         WHERE Id = :buId
                                         LIMIT 1 ];

        return lstUnit != null && lstUnit.size() > 0 ? lstunit[0] : null;                            
    }

    public static cls_meta_data ReturnMetaResponse(String message, String devMessage, Integer statusCode) {
        cls_meta_data retMeta = new cls_meta_data();
        retMeta.message = message;
        retMeta.status_code = statusCode;
        retMeta.title = mapStatusCode.get(statusCode);
        retMeta.developer_message = devMessage;
        return retMeta;
    }


    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message; 
    }

    public class cls_data {
        public String booking_unit_id;
        public String booking_unit_name;
        public String sr_id;
        public String sr_number;
        public String sr_type;
        public String sr_status;
        public String sr_submission_date;
        public String sr_approval_status;
        public Decimal amount;
        public String card_type;
        public cls_attachment[] attachments;

    }

    public class FmCaseWrapper {
        public String action;
        public String id;
        public String booking_unit_id;
        public String account_id;
        public String type_of_request;
        public AccessCardCountDetailsWrap[] selected_access_card_details;
        
    }

    public class AccessCardSubmitSRWrap {
        public String action;
        public String id;
        public String receipt_number;
        public String receipt_url;
    }

    public class AccessCardCountDetailsWrap {
        public Integer card_id;
        public Integer number_of_cards;
    }

    public class cls_attachment {
        public String id;
        public String name;
        public String attachment_url;
        public String file_extension;
    }

    /*****************************************/
    /* For getAccessCardAmountDetails Method */
    /*****************************************/
    global class AmountDetailsReturnWrapper {
        public cls_amount_data data;
        public cls_meta_data meta_data;
    }

    public class cls_get_amount_details_input_wrap {
        public String booking_unit_id;
        public String type_of_request;
        public AccessCardCountDetailsWrap[] selected_access_card_details;
    }

    public class cls_amount_data{
        public cls_card_fee_details[] card_fee_details;
        public cls_total_amount total_amount;
    }

    public class cls_card_fee_details {
        public String card_type;
        public Integer card_id;
        public Integer number_of_cards;
        public Decimal fee_amount;
    }

    public class cls_total_amount {
        public Decimal subtotal;
        public Decimal vat;
        public Decimal total;
    }
}


/*
INPUT JSON :

DRAFT -

{
    "action": "draft",
    "id": "",
    "booking_unit_id": "a0x0Y000002UIDF",
    "account_id" : "001aajbkjbsdf",
    "type_of_request" : "New card",
    "selected_access_card_details" : [
        {
            "card_id " : 1,
            "number_of_cards" : 1
        },
        {
            "card_id" : 2,
            "number_of_cards" : 1
        }
    ],
}

SUBMIT -

{
    "action": "submit",
    "id": "ax01asdscc55",
    "receipt_number" : "TRNX001254546",
    "receipt_url" : "https://abc.com"
}


*/


//label.Access_Card_Replacement_Fees
//label.New_Access_Card_Fees