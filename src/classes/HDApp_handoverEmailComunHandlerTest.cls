@isTest
public class HDApp_handoverEmailComunHandlerTest {
    @isTest
    static void test_1() {
        Id personAcc_rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAccount_1 = new Account( LastName = 'Test 1', Party_ID__c = '33307771');
        insert objAccount_1;
            
        Case objCase_1 = new Case();
        insert objCase_1;
        
        NSIBPM__Service_Request__c objSR_1 = new NSIBPM__Service_Request__c();
        insert objSR_1;
        
        Booking__c objBooking_1 = new Booking__c();
        objBooking_1.Deal_SR__c = objSR_1.Id;
        objBooking_1.Account__c = objAccount_1.Id;
        insert objBooking_1;
        
        Booking_Unit__c objBU_1 = new Booking_Unit__c();
        objBU_1.DLP_End_Date__c = Date.today().addDays(3);
        objBU_1.Registration_ID__c = '3901';
        objBU_1.Unit_Name__c = 'test1/test';
        objBU_1.Snag_Received__c = true;
        objBU_1.Title_Deed_Uploaded__c = true;
        objBU_1.DP_PCC_Issued__c = 'final';
        objBU_1.Booking__c = objBooking_1.Id;
        objBU_1.Case__c = objCase_1.Id;
        insert objBU_1;
        
        Location__c objLocation_1 = new Location__c();
        objLocation_1.Name = 'test1';
        objLocation_1.Location_ID__c = '12345001';
        insert objLocation_1;
        
        SR_Attachments__c objSRAttachment = new SR_Attachments__c();
        objSRAttachment.Account__c = objAccount_1.Id;
        objSRAttachment.Booking_Unit__c = objBU_1.Id;
        Insert objSRAttachment;
        
        TriggerOnOffCustomSetting__c objTrigger = new TriggerOnOffCustomSetting__c();
        objTrigger.Name = 'CallingListTrigger';
        objTrigger.OnOffCheck__c = true;
        insert objTrigger;
        
        Appointment__c objAppointment_1 = new Appointment__c();
        objAppointment_1.Slots__c = '09:00 - 09:30';
        objAppointment_1.Building__c = objLocation_1.Id;
        insert objAppointment_1;
        
        Id appointSched_rtId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Appointment Scheduling').RecordTypeId;
        List<Calling_List__c> lstCallingList = new List<Calling_List__c>();
        
        Calling_List__c objCallingList_1 = new Calling_List__c();
        objCallingList_1.RecordTypeId = appointSched_rtId;
        objCallingList_1.Unit_Name__c = 'test1/test';
        objCallingList_1.Service_Type__c = 'Handover';
        objCallingList_1.Sub_Purpose__c = 'Unit Viewing';
        objCallingList_1.Appointment_Status__c = 'Confirmed';
        objCallingList_1.Appointment_Date__c = Date.today().addDays(3);
        objCallingList_1.Appointment_Slot__c = '09:00 - 09:30';
        objCallingList_1.Appointment__c = objAppointment_1.Id;
        objCallingList_1.Account__c = objAccount_1.Id;
        objCallingList_1.Booking_Unit__c  = objBU_1.Id;
        lstCallingList.add(objCallingList_1);
        
        Calling_List__c objCallingList_2 = new Calling_List__c();
        objCallingList_2.RecordTypeId = appointSched_rtId;
        objCallingList_2.Unit_Name__c = 'test1/test';
        objCallingList_2.Service_Type__c = 'Handover';
        objCallingList_2.Sub_Purpose__c = 'Unit Viewing';
        objCallingList_2.Appointment_Status__c = 'Rejected';
        objCallingList_2.Appointment_Date__c = Date.today().addDays(3);
        objCallingList_2.Appointment_Slot__c = '09:00 - 09:30';
        objCallingList_2.Appointment_Rejection_Reason__c = 'test_reason';
        objCallingList_2.Appointment__c = objAppointment_1.Id;
        objCallingList_2.Account__c = objAccount_1.id; // objAccount_2.Id;
        objCallingList_2.Booking_Unit__c  = objBU_1.Id;
        lstCallingList.add(objCallingList_2);
        
        Calling_List__c objCallingList_3 = new Calling_List__c();
        objCallingList_3.RecordTypeId = appointSched_rtId;
        objCallingList_3.Unit_Name__c = 'test1/test';
        objCallingList_3.Service_Type__c = 'Handover';
        objCallingList_3.Sub_Purpose__c = 'Documentation';
        objCallingList_3.Appointment_Status__c = 'Confirmed';
        objCallingList_3.Appointment_Date__c = Date.today().addDays(3);
        objCallingList_3.Appointment_Slot__c = '09:00 - 09:30';
        objCallingList_3.Appointment_Rejection_Reason__c = 'test_reason';
        objCallingList_3.Appointment__c = objAppointment_1.Id;
        objCallingList_3.Account__c = objAccount_1.id; // objAccount_3.Id;
        objCallingList_3.Booking_Unit__c  = objBU_1.Id;
        lstCallingList.add(objCallingList_3);
        
        Calling_List__c objCallingList_4 = new Calling_List__c();
        objCallingList_4.RecordTypeId = appointSched_rtId;
        objCallingList_4.Unit_Name__c = 'test1/test';
        objCallingList_4.Service_Type__c = 'Handover';
        objCallingList_4.Sub_Purpose__c = 'Documentation';
        objCallingList_4.Appointment_Status__c = 'Rejected';
        objCallingList_4.Appointment_Date__c = Date.today().addDays(3);
        objCallingList_4.Appointment_Slot__c = '09:00 - 09:30';
        objCallingList_4.Appointment_Rejection_Reason__c = 'test_reason';
        objCallingList_4.Appointment__c = objAppointment_1.Id;
        objCallingList_4.Account__c = objAccount_1.id; // objAccount_4.Id;
        objCallingList_4.Booking_Unit__c  = objBU_1.Id;
        lstCallingList.add(objCallingList_4);
        
        Calling_List__c objCallingList_5 = new Calling_List__c();
        objCallingList_5.RecordTypeId = appointSched_rtId;
        objCallingList_5.Unit_Name__c = 'test1/test';
        objCallingList_5.Service_Type__c = 'Handover';
        objCallingList_5.Sub_Purpose__c = 'Documentation';
        objCallingList_5.Appointment_Status__c = 'Cancelled';
        objCallingList_5.Appointment_Date__c = Date.today().addDays(3);
        objCallingList_5.Appointment_Slot__c = '09:00 - 09:30';
        objCallingList_5.Appointment_Rejection_Reason__c = 'test_reason';
        objCallingList_5.Appointment__c = objAppointment_1.Id;
        objCallingList_5.Account__c = objAccount_1.id; // objAccount_5.Id;
        objCallingList_5.Booking_Unit__c  = objBU_1.Id;
        lstCallingList.add(objCallingList_5);
        
        Calling_List__c objCallingList_6 = new Calling_List__c();
        objCallingList_6.RecordTypeId = appointSched_rtId;
        objCallingList_6.Unit_Name__c = 'test1/test';
        objCallingList_6.Service_Type__c = 'Handover';
        objCallingList_6.Sub_Purpose__c = 'Key Handover';
        objCallingList_6.Appointment_Status__c = 'Confirmed';
        objCallingList_6.Appointment_Date__c = Date.today().addDays(3);
        objCallingList_6.Appointment_Slot__c = '09:00 - 09:30';
        objCallingList_6.Appointment_Rejection_Reason__c = 'test_reason';
        objCallingList_6.Appointment__c = objAppointment_1.Id;
        objCallingList_6.Account__c = objAccount_1.id; // objAccount_6.Id;
        objCallingList_6.Booking_Unit__c  = objBU_1.Id;
        lstCallingList.add(objCallingList_6);
        
        Calling_List__c objCallingList_7 = new Calling_List__c();
        objCallingList_7.RecordTypeId = appointSched_rtId;
        objCallingList_7.Unit_Name__c = 'test1/test';
        objCallingList_7.Service_Type__c = 'Handover';
        objCallingList_7.Sub_Purpose__c = 'Key Handover';
        objCallingList_7.Appointment_Status__c = 'Rejected';
        objCallingList_7.Appointment_Date__c = Date.today().addDays(3);
        objCallingList_7.Appointment_Slot__c = '09:00 - 09:30';
        objCallingList_7.Appointment_Rejection_Reason__c = 'test_reason';
        objCallingList_7.Appointment__c = objAppointment_1.Id;
        objCallingList_7.Account__c = objAccount_1.id; // objAccount_7.Id;
        objCallingList_7.Booking_Unit__c  = objBU_1.Id;
        lstCallingList.add(objCallingList_7);
        
        insert lstCallingList;
        
        Test.startTest();
        HDApp_handoverEmailCommunicationHandler.sendHandoverEmail(lstCallingList);
        Test.stopTest();
    }
}