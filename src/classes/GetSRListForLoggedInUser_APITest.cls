@isTest
public class GetSRListForLoggedInUser_APITest {
    @isTest
    static void testMoveInCases() {


        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];

        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Customer Community Login User(Use this)%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              //ContactId = contact1.Id,
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
        Database.insert(user1);

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1204',
                                      Registration_ID__c = '3901');
        insert bookingUnit;

        List<FM_Case__c> lstFMCases = new List<FM_Case__c>();
        //Move-In Case
        Id rtMoveInId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Move In').getRecordTypeId();
        FM_Case__c objMoveIn = new FM_Case__c();
        objMoveIn.Origin__c = 'Portal';
        objMoveIn.isHelloDamacAppCase__c = true;
        objMoveIn.Tenant__c = account.Id;
        objMoveIn.Account__c = account.id;
        objMoveIn.RecordtypeId = rtMoveInId;
        objMoveIn.Status__c = 'Submitted';
        lstFMCases.add(objMoveIn);

        //Move-Out Case
        Id rtMoveOutId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Move Out').getRecordTypeId();
        FM_Case__c objMoveOut = new FM_Case__c();
        objMoveOut.Origin__c = 'Portal';
        objMoveOut.isHelloDamacAppCase__c = true;
        objMoveOut.Tenant__c = account.Id;
        objMoveOut.Account__c = account.id;
        objMoveOut.RecordtypeId = rtMoveOutId;
        objMoveOut.Status__c = 'Submitted';
        lstFMCases.add(objMoveOut);

        //COCD
        List<Case> lstCases = new List<Case>();
        Case objCOCD = new Case();
        objCOCD.AccountId = account.Id;
        objCOCD.SR_Type__c = 'Change of Contact Details';
        objCOCD.Origin = 'Portal';
        objCOCD.isHelloDamacAppCase__c = true;
        objCOCD.Status = 'Submitted';
        lstCases.add(objCOCD);

        //Passport
        Case objPassport = new Case();
        objPassport.AccountId = account.Id;
        objPassport.SR_Type__c = 'Passport Detail Update';
        objPassport.Origin = 'Portal';
        objPassport.isHelloDamacAppCase__c = true;
        objPassport.Status = 'Submitted';
        lstCases.add(objPassport);


        Test.startTest();
        
        insert lstCases;
        insert lstFMCases;

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getMoveInSR';  
        req.addParameter('accountId',account.id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        GetSRListForLoggedInUser_API.getSRList();

        Test.stopTest();


        
    }


    //Profine name for Tenant - Tenant Community Login User

    @isTest
    static void testMoveInCasesNegative1() {

        Test.startTest();

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getMoveInSR';  
        req.addParameter('accountId','');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        GetSRListForLoggedInUser_API.getSRList();
        
        Test.stopTest();
        
    }

    @isTest
    static void testMoveInCasesNegative2() {

        Test.startTest();

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getMoveInSR';  
        //req.addParameter('accountId','');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        GetSRListForLoggedInUser_API.getSRList();
        
        Test.stopTest();
        
    }

    @isTest
    static void testMoveInCasesNegative3() {

        Test.startTest();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getMoveInSR';  
        req.addParameter('accountId',account.Id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        GetSRListForLoggedInUser_API.getSRList();
        
        Test.stopTest();
        
    }

    /*********************************************************************************/
    /*********************************************************************************/

    @isTest
    static void testMoveInCasesFromId(){

        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;

        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'Janusia';
        insert locObj;

        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id,
                                                          Registration_ID__c = '3901',
                                                          Inventory__c = invObj.Id,
                                                          Unit_Name__c='JNU/SD168/XH2910B');
        insert bookingUnit;
        
        FM_Case__c fmCase = new FM_Case__c(Request_Type_DeveloperName__c = 'Move_in_Request',
                                           RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Move In').getRecordTypeId(),
                                           Request_Type__c = 'Move In',
                                           Status__c = 'Submitted',
                                           Account__c = account.Id,
                                           Origin__c = 'Portal',
                                           Booking_Unit__c = bookingUnit.Id);
        insert fmCase;
        

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getMoveInSR';  
        req.addParameter('recordType','Move In');
        req.addParameter('fmCaseId',fmCase.Id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        GetSRListForLoggedInUser_API.getMoveInCaseFromCaseId();
    }
    
    @isTest
    static void testMoveInCasesFromId1(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getMoveInSR';  
        req.addParameter('recordType','');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        GetSRListForLoggedInUser_API.getMoveInCaseFromCaseId();
    }
    
    @isTest
    static void testMoveInCasesFromIdBlankRecord(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getMoveInSR';
        req.addParameter('fmCaseId','test');
        req.addParameter('recordType','');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        GetSRListForLoggedInUser_API.getMoveInCaseFromCaseId();
    }
    
    @isTest    
    static void testMoveInCasesFromIdBlankFM(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getMoveInSR';  
        req.addParameter('fmCaseId','');
        req.addParameter('recordType','test');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        GetSRListForLoggedInUser_API.getMoveInCaseFromCaseId();
    }
    
    
    @isTest
    static void testMoveInCasesFromIdNotBlank(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getMoveInSR';  
        req.addParameter('recordType','test');
        req.addParameter('fmCaseId','Move In');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        GetSRListForLoggedInUser_API.getMoveInCaseFromCaseId();
    }
    
    @isTest
    static void testMoveInCasesFromIdNotBlankRecordFm(){
        
        FM_Case__c objFM = new FM_Case__c();
        insert objFM;
        
        FM_Additional_Detail__c objFMAddition = new FM_Additional_Detail__c();
        objFMAddition.Emergency_Contact_Case__c = objFM.Id;
        objFMAddition.Vehicle_Case__c = objFM.Id;
        objFMAddition.Resident_Case__c = objFM.Id;
        insert objFMAddition;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getMoveInSR';  
        req.addParameter('recordType', objFM.Id);
        req.addParameter('fmCaseId', objFM.Id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        GetSRListForLoggedInUser_API.getMoveInCaseFromCaseId();
        
        GetTenantRegistrationCase_API.getFMCase(objFM.Id);
    }
    
    @isTest
    static void testgetMoveInCaseFromCaseIdNoRecType(){
        
        FM_Case__c objFM = new FM_Case__c();
        insert objFM;
        
        FM_Additional_Detail__c objFMAddition = new FM_Additional_Detail__c();
        objFMAddition.Emergency_Contact_Case__c = objFM.Id;
        objFMAddition.Vehicle_Case__c = objFM.Id;
        objFMAddition.Resident_Case__c = objFM.Id;
        insert objFMAddition;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getMoveInSR';  
        //req.addParameter('recordType', objFM.Id);
        req.addParameter('fmCaseId', objFM.Id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        GetSRListForLoggedInUser_API.getMoveInCaseFromCaseId();
        
        GetTenantRegistrationCase_API.getFMCase(objFM.Id);
    }
    
    @isTest
  static void tenancyRenewalTest() {
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(Agency_ID__c = '1234');
        insert sr;
        
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];

        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Tenant Community Login User%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              //ContactId = contact1.Id,
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
        Database.insert(user1);
        
        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;
    
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'test';
        locObj.Location_Type__c = 'Building';
        locObj.Location_ID__c = '12345';
        locObj.UAEProp__c = true;
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        invObj.Property_Country__c = 'UNITED ARAB EMIRATES';
        invObj.Marketing_Name_Doc__c = invObj.Id;
        invObj.Building_Location__c = locObj.Id;
        invObj.Property_Status__c = 'Ready'; 
        invObj.Unit_type__c = 'Villa';
        insert invObj;
    
        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1204',
                                      Registration_ID__c = '3901',
                                      Handover_Flag__c = 'Y',
                                      Inventory__c = invObj.id,
                                      Dummy_Booking_Unit__c = true);
        insert bookingUnit;
        
        Id rtMoveInId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Renewal').getRecordTypeId();
        FM_Case__c objTenRenewal = new FM_Case__c();
        objTenRenewal.Origin__c = 'Portal';
        objTenRenewal.isHelloDamacAppCase__c = true;
        objTenRenewal.Tenant__c = account.Id;
        objTenRenewal.RecordtypeId = rtMoveInId;
        objTenRenewal.Status__c = 'In Progress';
        objTenRenewal.Request_Type__c = 'Tenant Renewal';
        objTenRenewal.Booking_Unit__c = bookingUnit.id;
        
        Test.startTest();
    
        insert objTenRenewal;
    
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getMoveInSR';  
        req.addParameter('accountId',account.id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        GetSRListForLoggedInUser_API.getSRList();
        
        Test.stopTest();
  }
}