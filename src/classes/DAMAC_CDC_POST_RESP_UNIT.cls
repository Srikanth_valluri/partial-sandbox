/*
Developed By: DAMAC IT Team
Usage:DAMAC_CDC_REST_UTILITY
*/
public class DAMAC_CDC_POST_RESP_UNIT{

    public String REGID;    //Hello World
    public String STATUS;    //verified
    public String MESSAGE;  //
    
    public static DAMAC_CDC_POST_RESP_UNIT parse(String json){
        return (DAMAC_CDC_POST_RESP_UNIT) System.JSON.deserialize(json, DAMAC_CDC_POST_RESP_UNIT.class);
    }
}