public class GenericDataRendererController {

    //public String configJSON {set;get;}



    @RemoteAction
    public static DataDisplayConfig getData( DataDisplayConfig config) {

        //if configJSON is not blank load it from config document
        //set this blank in subsequent calls to save on queries
        if( String.isBlank(config.configJSON) == false ){
            config = DataRendererUtils.extractConfig(config);
        }

        return DataRendererUtils.loadData(config);
        //return null;
    }

    public Boolean renderedSection {set;get;}

    {
        renderedSection = false;
    }

    public PageReference methodOne(){
        renderedSection = !renderedSection;
        return null;
    }

}