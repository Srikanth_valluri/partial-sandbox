global class CRMPendingTaskEmailBatch implements Database.Batchable<Sobject>{//AKISHOR: for sending reminder email for open tasks
    Map<String,List<Task>> listOfTask=new Map<String,List<Task>>();//email vs task list
    Set<Id> CaseId= new Set<Id>();
    global Database.QueryLocator Start(Database.BatchableContext bc)
    {

        return Database.getQueryLocator([SELECT ID,createdDate,What.id,Owner.Email,OwnerId,Owner.Name,
        Status,ActivityDate,Subject from Task WHERE Status = 'Not Started' 
        AND Owner.IsActive = True  AND ActivityDate!=NULL AND ActivityDate <=:system.today() 
        AND Assigned_User__c='CRE' AND Owner.Email !=NULL]);
    }

    global void Execute(Database.BatchableContext bc, List<Task>scope)
    {
        set<Id> setCaseId = new set<Id>();
        map<Id,String> mapCaseIdCaseNumber = new map<Id,String>();
        for(Task task:scope)
        {
            if(!listOfTask.containsKey(task.Owner.Email))
            {
                listOfTask.put(task.Owner.Email, new List<Task>());
            }
            listOfTask.get(task.Owner.Email).add(task);
            if( task.What.Id != null ) { 
                setCaseId.add(task.What.Id);
            }
        }

        for( Case objCase : [Select Id , CaseNumber From Case where Id IN: setCaseId] ) {
            if( objCase != null ){
                mapCaseIdCaseNumber.put(objCase.Id,objCase.CaseNumber);
            }
        }
        
        List<Messaging.SingleEmailMessage> pendingTaskEmailAlert = new List<Messaging.SingleEmailMessage>();

        for(String mail :listOfTask.keySet())
        {
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'no-replysf@damacgroup.com'];
            String[] toAddresses = new String[] {mail};
            email.setToAddresses(toAddresses);
            email.setSubject('Pending Tasks as of Today');
            String userName='Team';//listOfTask.get(mail)[0].Owner.Name;
            String htmlBody= '';
            //String CaseNum='';

            htmlBody = '<table width="100%" border="0" cellspacing="0" cellpadding="10" align="center" bgcolor="#F7F7F7">'+
                +'<tr>'+
                +'<td style="font-size: 14px; font-weight: normal; font-family:Calibri;line-height: 20px; color: #333;"><br />'+
                +'<br />'+
                +'Dear, '+username+',</td>'+
                +'</tr>'+
                +'<tr>'+
                +'<td style="font-size: 14px; font-weight: normal; font-family:Calibri; line-height: 18px; color: #333;">Below tasks are pending for your action.</td>'+
                +'</tr>'+
                +'</table>';

            htmlBody +=  '<table border="1" style="border-collapse: collapse"><tr><th>Case#</th><th>Task Description</th><th>Created On</th><th>Due On</th><th>Task Status</th><th>Owner</th></tr>';

            for(Task task:listOfTask.get(mail))
            {
                String dueDate = task.ActivityDate.format();
                String Subject =task.Subject;
                DateTime dt=task.CreatedDate;
                String what=task.What.Id;
                String status=task.Status;String TaskOwner=task.Owner.Name;
                //String Casenum = [Select CaseNumber from Case where Id =: what LIMIT 1].CaseNumber;
                htmlBody +='<tr><td>' + mapCaseIdCaseNumber.get(task.What.Id) + '</td><td>' + Subject +'</td><td>'+ dt + '</td><td>'+ dueDate +'</td><td>'+ status + '</td><td>'+ TaskOwner+ '</td></tr>';
            }
            htmlBody += '</table><br>';
            email.setHtmlBody(htmlBody);
              if (owea.size() > 0 ) {
                    email.setOrgWideEmailAddressId(owea.get(0).Id);
                }
            pendingTaskEmailAlert.add(email);
        }
        Messaging.sendEmail(pendingTaskEmailAlert);

    }
    global void Finish(Database.BatchableContext bc)
    {

    }

}