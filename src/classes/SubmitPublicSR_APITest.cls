@isTest
public class SubmitPublicSR_APITest {
    //@isTest
    //static void testSubmitSRAmenity(){
    //    RestRequest req = new RestRequest();
    //    RestResponse res = new RestResponse();
    //    req.requestURI = '/submitPublicSr';  
    //    req.addParameter('recordType', 'Amenity Booking');
    //    Blob requestBody = Blob.valueOf('requestString');
    //    req.httpMethod = 'POST';
    //    RestContext.request = req;
    //    RestContext.response = res;
        
    //    Test.startTest();
    //        SubmitPublicSR_API.SubmitSR();
    //    Test.stopTest();
    //}

    @isTest
    static void testSubmitSRTenant(){
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'Test Unit Name',
                                      Registration_ID__c = '3901');
        insert bookingUnit;

        String requestJson =  '   {  '  + 
        '    "action": "draft",  '  + 
        '    "id": "",  '  + 
        '    "booking_unit_id": "'+bookingUnit.id+'",  '  + 
        '    "first_name": "Testing",  '  + 
        '    "last_name": "TR",  '  + 
        '    "gender": "",  '  + 
        '    "date_of_birth": "",  '  + 
        '    "no_of_adults": "0",  '  + 
        '    "no_of_children": "0",  '  + 
        '    "is_person_with_spl_needs": true,  '  + 
        '    "is_having_pets": true,  '  + 
        '    "nationality": "UAE",  '  + 
        '    "passport_number": "1234",  '  + 
        '    "passport_expiry_date": "",  '  + 
        '    "emirates_id": "11222",  '  + 
        '    "mobile_code": "India: 0091",  '  + 
        '    "mobile_number": "8888857410",  '  + 
        '    "email_address": "shubham.suryawanshi@eternussolutions.com",  '  + 
        '    "lease_start_date": "2020-07-15",  '  + 
        '    "lease_end_date": "2020-08-15",  '  + 
        '    "ejari_number": "2020",  '  + 
        '    "move_in_date": "2020-08-17",  '  + 
        '    "move_in_type": "Self move-in",  '  + 
        '    "moving_company_name": "",  '  + 
        '    "moving_contractor_name": "",  '  + 
        '    "moving_contractor_phone": "",  '  + 
        '    "vehicle_details": [  '  + 
        '        {  '  + 
        '           "id": "",  '  + 
        '           "vehicle_number": "1234",  '  + 
        '           "vehicle_make": "Honda"  '  + 
        '        }  '  + 
        '    ],  '  + 
        '    "emergency_contact_details": [  '  + 
        '       {  '  + 
        '           "id": "",  '  + 
        '           "emergency_full_name" : "Test",  '  + 
        '           "emergency_relationship" : "Shubham",  '  + 
        '           "emergency_email_address" : "no@no.com",  '  + 
        '           "emergency_mobile_number" : "231324"  '  + 
        '       }  '  + 
        '      '  + 
        '    ],  '  + 
        '    "additional_members":[  '  + 
        '       {  '  + 
        '           "id": "",  '  + 
        '           "member_first_name" : "Test",  '  + 
        '           "member_last_name" : "Test",  '  + 
        '           "member_gender" : "Male",  '  + 
        '           "member_date_of_birth" : "",  '  + 
        '           "member_nationality" : "",  '  + 
        '           "member_passport_number" : "",  '  + 
        '           "member_emirates_id" : "",  '  + 
        '           "member_mobile_number" : "",  '  + 
        '           "member_email_address" : ""  '  + 
        '       }  '  + 
        '        '  + 
        '    ],  '  + 
        '    "otp_code_details": {'  + 
        '       "mobile_otp": "2417",  '  + 
        '       "email_otp": "9548",  '  + 
        '       "guid": "b09e81ef-cf42-13f0-2a3d-e5a0384e4cb5"  '  + 
        '     }  '  + 
        '   }  '  + 
        '     '  + 
        '    ' ; 


        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/submitPublicSr';  
        req.addParameter('recordType', 'Tenant Registration');
        req.requestBody = Blob.valueOf(requestJson);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
            SubmitPublicSR_API.SubmitSR();
        Test.stopTest();
    }
    
    //@isTest
    //static void testSubmitSRBlank(){
    //    RestRequest req = new RestRequest();
    //    RestResponse res = new RestResponse();
    //    req.requestURI = '/submitPublicSr';  
    //    req.addParameter('recordType', '');
    //    req.httpMethod = 'POST';
    //    RestContext.request = req;
    //    RestContext.response = res;
        
    //    Test.startTest();
    //        SubmitPublicSR_API.SubmitSR();
    //    Test.stopTest();
    //}
    
    //@isTest
    //static void testSubmitSR(){
    //    RestRequest req = new RestRequest();
    //    RestResponse res = new RestResponse();
    //    req.requestURI = '/submitPublicSr';  
    //    //req.addParameter('recordType', '');
    //    req.httpMethod = 'POST';
    //    RestContext.request = req;
    //    RestContext.response = res;
        
    //    Test.startTest();
    //        SubmitPublicSR_API.SubmitSR();
    //    Test.stopTest();
    //}
}