@isTest
private class AP_EOIProcessControllerTest
{
	static testmethod void testEOIProcess(){
        Id accountRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.RecordTypeId = accountRTId;
        acc.Agency_Short_Name__c = 'testShrName';
        insert acc;
            
        Id salesEventsRt = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Sales Events').getRecordTypeId();
        List<Campaign__c> campaignList = TestDataFactory.createCampaignRecords(new List<Campaign__c>{new Campaign__c(RecordTypeId=salesEventsRt), new Campaign__c(RecordTypeId=salesEventsRt),
            new Campaign__c(RecordTypeId=salesEventsRt), new Campaign__c(RecordTypeId=salesEventsRt), new Campaign__c(RecordTypeId=salesEventsRt)});
        TestDataFactory.createInquiryRecords(new List<Inquiry__c> {new Inquiry__c(Campaign__c=campaignList[0].Id, Inquiry_Source__c='', OwnerId = UserInfo.getUserId())});
        
        Property__c propObjnew = new Property__c();
        propObjnew.Name = System.Label.Default_EOI_Property;
        propObjnew.Property_ID__c   = 455546;
        propObjnew.EOI_Enabled__c = true;
        insert propObjnew;

		EOI_Process__c eoObj = new EOI_Process__c();
		eoObj.No_of_Units__c = 'Single Unit – Token AED 40,000';
		eoObj.Agency__c = acc.id;
		eoObj.Mode_of_Token_Payment__c = 'Cash';
		eoObj.Property__c = propObjnew.id ;
		insert eoObj;

		Campaign__c camp = new Campaign__c();
        camp.End_Date__c = system.today().addmonths(10);
        camp.Marketing_End_Date__c = system.today().addmonths(10);
        camp.Marketing_Start_Date__c = system.today().addmonths(-10);
        camp.Start_Date__c =  system.today().addmonths(-10);
        insert camp;

		Inquiry__c inq = new Inquiry__c ();
        inq.Activity_Counter__c =101;
        inq.Inquiry_Status__c='Active';
        inq.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        inq.campaign__c = camp.id;
        insert inq;

		Attachment objAttachment = new Attachment();
        objAttachment.Name = 'Test Document';
        objAttachment.Body = blob.valueOf('test');
        objAttachment.ContentType='image/png';
        objAttachment.ParentId = eoObj.id;
        
        
        
        ApexPages.currentPage().getParameters().put('inquiryId',inq.id);
        AP_EOIProcessController objController = new AP_EOIProcessController();    
		AP_EOIProcessController.insertEOI(propObjnew.id, '1 Unit','Cheque',2000,'test',inq.id);
		AP_EOIProcessController.doUploadAttachment(eoObj.id,'test','test',objAttachment.id);
        AP_EOIProcessController.getUnits();
        AP_EOIProcessController.getPaymentMode();
        objController.submitDetails();
        
        list<EOI_Process__c> insertedEOI = new list<EOI_Process__c>();
        insertedEOI = [Select id,Mode_of_Token_Payment__c From EOI_Process__c Where No_of_Units__c = '1 Unit'];
        //system.assertEquals(insertedEOI[0].Mode_of_Token_Payment__c, 'Cheque');
    }
    /*
    static testmethod void testModeOfPayment(){
        Id accountRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.RecordTypeId = accountRTId;
        acc.Agency_Short_Name__c = 'testShrName';
        insert acc;
            
        Id salesEventsRt = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Sales Events').getRecordTypeId();
        List<Campaign__c> campaignList = TestDataFactory.createCampaignRecords(new List<Campaign__c>{new Campaign__c(RecordTypeId=salesEventsRt), new Campaign__c(RecordTypeId=salesEventsRt),
            new Campaign__c(RecordTypeId=salesEventsRt), new Campaign__c(RecordTypeId=salesEventsRt), new Campaign__c(RecordTypeId=salesEventsRt)});
        TestDataFactory.createInquiryRecords(new List<Inquiry__c> {new Inquiry__c(Campaign__c=campaignList[0].Id, Inquiry_Source__c='', OwnerId = UserInfo.getUserId())});
        
        Property__c propObjnew = new Property__c();
        propObjnew.Name = 'REVA RESIDENCES';
        propObjnew.Property_ID__c   = 455546;
        propObjnew.EOI_Enabled__c = true;
        insert propObjnew;

        
        EOIProcessController.InquiryDetails jsonobject_In = new EOIProcessController.InquiryDetails();
        jsonobject_In.noUnits = '1 Unit';
        jsonobject_In.property= propObjnew.id;
        jsonobject_In.mode = 'Cheque';
        jsonobject_In.token = '2';
        jsonobject_In.comm = 'Test comments';
        String inJSON = JSON.serialize(jsonobject_In);
        
        EOIProcessController.EOIDetails jsonobject_EOI = new EOIProcessController.EOIDetails();
        jsonobject_EOI.fname = 'Fname';
        jsonobject_EOI.lname= 'Lname';
        //jsonobject_EOI.passportNo = 'PN2102';
        //jsonobject_EOI.pLang = 'English';
        jsonobject_EOI.mCCode = 'India: 0091';
        jsonobject_EOI.mNo = '5088486348';
        jsonobject_EOI.email = 'abdihs@gmail.com';
        //jsonobject_EOI.comm = 'Test comments';   
        String EOIJSON = JSON.serialize(jsonobject_EOI);     
        
        EOIProcessController.InquiryDetails test = new EOIProcessController.InquiryDetails();
        EOIProcessController.EOIDetails test1 = new EOIProcessController.EOIDetails();  
        EOIProcessController objController = new EOIProcessController();        
        EOIProcessController.Save(EOIJSON,inJSON);   
        List<SelectOption> selOptsNoOfUnits=EOIProcessController.getNoOfUnits();         
        List<SelectOption> selOpts=EOIProcessController.getModeofpayment();
        system.debug('mode======='+selOpts);
        //system.assertEquals(selOpts.size(), 4);
    }*/
}