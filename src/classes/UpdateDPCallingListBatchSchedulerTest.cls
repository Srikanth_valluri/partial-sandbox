/*
* Description - Test class developed for ScheduleUpdateCustomerFlagonCallingList
*/
@isTest
public class UpdateDPCallingListBatchSchedulerTest {
      static testMethod void testExecute() {
        Test.startTest();
            UpdateDPCallingListBatchScheduler objScheduler = new UpdateDPCallingListBatchScheduler();
            String schCron = '0 30 8 1/1 * ? *';
            String jobId = System.Schedule('Reccord Updated',schCron,objScheduler);
        Test.stopTest();
        CronTrigger cronTrigger = [
            SELECT Id
                 , CronExpression
                 , TimesTriggered
                 , NextFireTime
            FROM CronTrigger
           WHERE id = :jobId
        ];
        System.assertEquals( schCron,  cronTrigger.CronExpression  , 'Cron expression should match' );
        System.assertEquals( 0, cronTrigger.TimesTriggered  , 'Time to Triggered batch should be 0' );
        List<AsyncapexJob> apexJobs = [SELECT Id, 
                                              ApexClassID ,
                                              JobType , 
                                              Status 
                                         FROM AsyncapexJob
                                        WHERE JobType = 'BatchApex'];
        System.assertEquals( false , apexJobs.isEmpty()  , 'Expected at least one Async apex Job' );
        System.assertEquals( 'BatchApex', apexJobs[0].JobType  , 'Job Type should match ' );
    }
}