/***************************************************************************************************
 * Description - Riyadh Rotana Conversions Process page controller.
 *--------------------------------------------------------------------------------------------------
 * Version      Date            Author                  Description
 * 1.0          11/09/19        Aishwarya Todkar        Initial Draft
 * 1.1          15/10/19        Aishwarya Todkar        Added method 'setBuilding_and_Agreement'
 * 2.0          5/12/19         Aishwarya Todkar        Captured owner interest from UI
 ***************************************************************************************************/
public class RRC_SR_Creation_Controller {
    
    @TestVisible Id accountId, caseId;
    @TestVisible List<Account> lstAccount;
    public Id caseRecTypeId;
    @TestVisible Case caseObj;
    public String caseStatus;
    public Booking_Unit__c selectedUnit;
    public string selectedUnitId                { get; set; }
    public List<SelectOption> lstSourceUnits    { get; set; }
    public string selectedBuilding              { get; set; }
    public List<SelectOption> lstBuildings      { get; set; }
    public string selectedSigned                { get; set; }
    public List<SelectOption> lstSigned         { get; set; }
    public String ownerInterestInHotel          { get; set; }
    public List<SelectOption> lstInterest       { get; set; }
    public Boolean showUnitDetails              { get; set; }
    public Boolean isGotoInit                   { get; set; }
    public Boolean isSRSubmitted                { get; set; }
    public Boolean isSRDrafted                  { get; set; }
    public Map<Id,Booking_Unit__c> mapIdToBU    { get; set; }
    

    public RRC_SR_Creation_Controller() {
        accountId =  ApexPages.currentPage().getParameters().get('AccountId');
        caseId = ApexPages.currentPage().getParameters().get('caseId');
        caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Riyadh Rotana Conversion').getRecordTypeId();
        lstAccount = new List<Account>();
        selectedBuilding = '';
        selectedSigned = '';
        caseStatus = '';
        isGotoInit = false;
        isSRDrafted = false;
        isSRSubmitted = false;
        ownerInterestInHotel = '';
        showUnitDetails = true;
        caseObj = new Case();
        mapIdToBU = new Map<Id, Booking_Unit__c>();
        init();
    }

/***********************************************************************************************
Method Name : init
Description : initializations
Parameter(s): None
Return Type : void
************************************************************************************************/
    public void init() { 
        lstAccount = [Select Id, party_ID__C From Account where Id =: accountId];
        lstBuildings = new List<SelectOption> {//new SelectOption('None', '--None--'),
                                             new SelectOption('Fendi', 'Fendi')
                                            , new SelectOption('Paramount', 'Paramount')};

        lstSigned = new List<SelectOption> {//new SelectOption('None', '--None--'),
                                             new SelectOption('SPA', 'SPA')
                                            , new SelectOption('AGS', 'AGS')};
        lstInterest = new List<SelectOption> {//new SelectOption('None', '--None--'),
                                             new SelectOption('Interested', 'Interested')
                                            , new SelectOption('Not Interested', 'Not Interested')};         
        if(caseId != null) {
            //Go to initiation logic
            System.debug('===Go to initiation==');
            showUnitDetails = true;
            isSRDrafted = true;
            caseObj = [SELECT 
                            Id
                            , Booking_Unit__c
                            , Building__c
                            //, Owner_Interest_In_Hotel__c
                            , Signed_SPA_AGS__c
                            , Booking_Unit__r.Unit_Name__c
                            , CaseNumber
                            , Status
                            , OwnerId
                        From 
                            Case 
                        WHERE
                            Id =: caseId
                        ];
            if(caseObj.Status == 'Submitted' || caseObj.Status == 'Closed') {
                isSRSubmitted = true;
                getTargetUnits();
            }
            else if(caseObj.Status == 'In Progress') {
                getTargetUnits();
            }
            else if(caseObj.Status == 'Draft Request'){
                isSRDrafted = true;
                getTargetUnits();
            }
        }
        else {
            getUnits();
        }
    }

/***********************************************************************************************
Method Name : getUnits
Description : Gets all active units of Customer
Parameter(s): None
Return Type : void
************************************************************************************************/
    public void getUnits() {
        List<Booking_Unit__c> lstBookingUnit = new List<Booking_Unit__c>();
        
        mapIdToBU = new Map<Id, Booking_Unit__c>();

        //Getting Active status values for booking units
        List<String> lstActiveStatus = new List<String>();
        for(Booking_Unit_Active_Status__c cs : Booking_Unit_Active_Status__c.getall().values()) {
            lstActiveStatus.add(cs.Status_Value__c);
        }

        if(accountId != null) {
            lstBookingUnit =[SELECT 
                                Id
                                , Registration_ID__c
                                , Name
                                , Unit_Name__c
                                , Inventory__r.Property_Name__c
                                , Rotana_Agreement_Type__c
                                , Riyadh_Building__c
                            FROM 
                                Booking_Unit__c
                            WHERE
                                Booking__r.Account__c != null
                            AND
                                Booking__r.Account__c =: accountId
                            AND 
                                Unit_Name__c != NULL
                            AND
                                Registration_Status__c IN : lstActiveStatus
                            AND 
                                (
                                    Unit_Name__c LIKE 'DEA%'
                                 OR Unit_Name__c LIKE 'DEB%'
                                )
                            ];
             system.debug('--lstBookingUnit ----------'+lstBookingUnit);
            if(! lstBookingUnit.isEmpty()) {
                system.debug('--lstBookingUnit inn if-----------'+lstBookingUnit);
                lstSourceUnits = new List<SelectOption>();
                for(Booking_Unit__c objBU : lstBookingUnit) {
                    lstSourceUnits.add(new SelectOption(objBU.Id, objBU.Unit_Name__c));
                    mapIdToBU.put( objBU.Id, objBU);
                }
                if( lstBookingUnit.size() == 1) {
                system.debug('--single unit-----------'+lstBookingUnit);
                     selectedUnitId = lstBookingUnit[0].Id;
                     setBuilding_and_Agreement();
                         
                }
            }
            else {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'No booking units to display!'));
                showUnitDetails = false;
                lstSourceUnits = new List<SelectOption>();
            }
        }
    }// End getUnits method

/***********************************************************************************************
Method Name : getUnitDetails
Description : Gets unit details of selected unit.
Parameter(s): None
Return Type : PageReference
************************************************************************************************/
    public PageReference getUnitDetails() {
        system.debug('selectedUnitId==' + selectedUnitId);
        selectedUnit = new Booking_Unit__c();
        if(selectedUnitId != null) {

            List<Case> lstExistingSR = checkExistingSR();
            if(lstExistingSR != null ) {
                showUnitDetails = false;
                string err = 'An open Service Request already exists for the chosen Booking Unit:' 
                        +'<a href="/'+ lstExistingSR[0].Id + '">' + lstExistingSR[0].CaseNumber + '</a>';
                System.debug('====SR exists====');
                
                ApexPages.addmessage(
                    new ApexPages.message(
                        ApexPages.severity.Error,err )
                );
                return null;
            } // End sr exist if
            else {
                //showUnitDetails = true;
                //Creating SR
                caseObj.accountId = accountId;
                caseObj.recordTypeId = caseRecTypeId;
                caseObj.status = caseStatus;
                caseObj.Booking_Unit__c =  selectedUnitId;
                caseObj.Owner_Interested_In_Hotel__c = ownerInterestInHotel;

                //Updating Unit
                //selectedUnit.Id = selectedUnitId;
                //selectedUnit.Building__c = selectedBuilding;
                //selectedUnit.Owner_Interested_In_Hotel__c = ownerInterestInHotel;
                //selectedUnit.Signed_SPA_AGS__c = selectedSigned;

                try {
                    //update selectedUnit;
                    upsert caseObj;
                }
                catch(Exception ex) {
                    showUnitDetails = false;
                    ApexPages.addmessage(new ApexPages.message(
                                ApexPages.severity.Error,ex.getMessage()));
                    return null;
                }
                system.debug('caseObj == ' + caseObj);
            } // End sr exist else
        }
        else {
            return null;
        }
        return new PageReference('/' + caseObj.ID);
    }

/***********************************************************************************************
Method Name : getTargetUnits
Description : Resuming drafted SR
Parameter(s): None
Return Type : void
************************************************************************************************/
public void getTargetUnits() {
        system.debug('==getTargetUnits()===');
        lstSourceUnits = new List<SelectOption>();
        isGotoInit = true;
        showUnitDetails = true;
        
        if(caseObj != null) {
            selectedUnitId = caseObj.Booking_Unit__c;
            system.debug(' selectedUnitId='+ selectedUnitId);

            lstSourceUnits.add(new SelectOption( caseObj.Booking_Unit__c, caseObj.Booking_Unit__r.Unit_Name__c) );
            selectedBuilding = caseObj.Building__c;
            selectedSigned = caseObj.Signed_SPA_AGS__c;
        }
    }

/***********************************************************************************************
Method Name : saveAsDraft
Description : Saves the SR
Parameter(s): None
Return Type : PageReference
************************************************************************************************/
    public PageReference saveAsDraft() {
        caseStatus = 'Draft Request';
        return getUnitDetails();
    }

/***********************************************************************************************
Method Name : submitSR
Description : Submits the SR
Parameter(s): None
Return Type : PageReference
************************************************************************************************/   
    public PageReference submitSR() {
        caseStatus = 'Submitted';
        return getUnitDetails();
    }

/***********************************************************************************************
Method Name : checkExistingSR
Description : Check whether SR exists or not against selected unit
Parameter(s): None
Return Type : List Of SR
************************************************************************************************/
    public List<Case> checkExistingSR(){ 
        system.debug('checkExistingSR selectedUnitId== ' +selectedUnitId);
        
        List<Case> lstExistingSR =  
                        [SELECT Id
                            , CaseNumber
                            , Booking_unit_Name__c
                            , Booking_Unit__r.Registration_ID__c
                        FROM 
                            Case
                        WHERE 
                            Booking_Unit__c =: selectedUnitId
                        AND 
                            RecordType.DeveloperName = 'Riyadh_Rotana_Conversion'
                        AND 
                            Status = 'Submitted' //Not In : new Set<String>{'Closed', 'Rejected', 'Cancelled'}
                        ];
        if(lstExistingSR.size() > 0) {
            return lstExistingSR;
        }
        return null;
    }

/***********************************************************************************************
Method Name : setBuilding_and_Agreement
Description : Sets Building and Agreement Type
Parameter(s): None
Return Type : void
************************************************************************************************/
    public void setBuilding_and_Agreement() {
        System.debug('getAgreementType selected unit = ' + selectedUnitId);
        System.debug('mapIdToBU = ' + mapIdToBU);
        if( String.isNotBlank( selectedUnitId ) 
        && mapIdToBU.size() > 0 
        && mapIdToBU.containsKey( selectedUnitId ) ) {
            
            showUnitDetails = true;
            //Set Agreement Type
            if( mapIdToBU.get( selectedUnitId ).Rotana_Agreement_Type__c == null ) {
                selectedSigned = 'SPA';
            }
            else {
                selectedSigned = mapIdToBU.get( selectedUnitId ).Rotana_Agreement_Type__c;
            }
            System.debug('selectedSigned = ' + selectedSigned); 

            //Set Building
            if( mapIdToBU.get( selectedUnitId ).Unit_Name__c != null) {
                selectedBuilding = mapIdToBU.get( selectedUnitId ).Riyadh_Building__c;
                System.debug('selectedBuilding = ' + selectedBuilding);
            }
        }
    }
}