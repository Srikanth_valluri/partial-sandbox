/**************************************************************************************************
 * @Name              : InventoryUpdateIPMSQueueable
 * @Test Class Name   : InventoryHelperTest 
 * @Description       : Queueable class to update Inventory Details in IPMS
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst          25/08/2020       Created
**************************************************************************************************/
public class InventoryUpdateIPMSQueueable implements Queueable,Database.AllowsCallouts{
    
    public list<Inventory__c> invList {get;set;}
    public Map<Id, String> invOldStatusMap {get; set;}
    
    public InventoryUpdateIPMSQueueable(List<Inventory__c> invList, Map<Id, String> invOldStatusMap){
        this.invList = invList;
        this.invOldStatusMap = invOldStatusMap;
    }
    
    public void execute(QueueableContext context) {
        List<log__c> logList = new List<log__c>();
        for(Inventory__c inv: invList){
            String jsonBody = createJSONBody(inv,invOldStatusMap.get(inv.Id));
            system.debug('Jsonbody: ' + jsonBody);
            if(jsonBody != ''){
                logList.addAll(inventoryStatusIPMSRequest(jsonBody, inv.Id));
            }
        }
        if(logList.size() > 0){
            insert logList;
        }
    }
    
    public List<log__c> inventoryStatusIPMSRequest(String jsonBody, String invId){
        List<log__c> logList = new List<log__c>();
        IPMS_Integration_Settings__mdt settings = [SELECT Client_ID__c, Endpoint_URL__c 
                                                   FROM IPMS_Integration_Settings__mdt 
                                                   WHERE DeveloperName = 'UK_Party' LIMIT 1];
        HttpResponse res = new HTTPResponse ();
        String reqName = 'UPDATE_UNIT_STATUS';
        try {
            string token = DAMAC_IPMS_PARTY_CREATION.getIPMSAccessToken();
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(settings.Endpoint_URL__c + '/inventory/update/unit');
            req.setMethod('POST');
            req.setHeader('Authorization', 'Bearer ' + token);
            req.setHeader('Content-Type', 'application/json');
            req.setTimeout(120000);
            req.setBody(jsonBody);
            system.debug('jsonBody: ' + jsonBody);
            system.debug('Request: ' + req);
            
            if (!Test.isRunningTest()) {
                res = h.send(req);
            } else {
                res.setBody ('{"status" : "true"}');
            }
            system.debug('res: ' + res);
            system.debug('response body: ' + res.getBody());
            log__c logReq = createLog(invId, jsonBody, reqName + ' REQUEST');
            logList.add(logReq);
            log__c logResponse = createLog(invId, res.getBody(), reqName + ' RESPONSE');
            logList.add(logResponse);
        } catch (Exception e) {
            log__c logException = createLog(invId, e.getmessage(), reqName + ' RESPONSE');
            logList.add(logException);
            system.debug('Exception While Updating Inventory Status in IPMS: ' + e);
            
        }  
        return logList;                                               
    }

    
    public string createJSONBody(Inventory__c inv, String oldStatus){
        String body = '';
        InventoryUpdateJSON jsonClass = new InventoryUpdateJSON();
        jsonClass.operation = 'UPDATE_UNIT_STATUS';
        jsonClass.unitId = inv.Unit_ID__c;     //'31931';
        jsonClass.currentStatus =  '';
        jsonClass.newStatus = '';
        if(Inventory_Status_IPMS_Code__c.getValues(inv.Status__c) != null){
            jsonClass.newStatus = Inventory_Status_IPMS_Code__c.getValues(inv.Status__c).Status_Code__c;
        }
        if(Inventory_Status_IPMS_Code__c.getValues(oldStatus) != null){
            jsonClass.currentStatus  = Inventory_Status_IPMS_Code__c.getValues(oldStatus).Status_Code__c;
        }
        jsonClass.restrictCode = inv.Restriction_Code__c;   //'FDC';
        jsonClass.remarks = inv.Restriction_Remarks__c; // 'Unit is approved by Somebody';
        jsonClass.updatedBy =  Label.IPMS_Inventory_Default_User; //'SASANKA.RATH'; //userInfo.getUserName(); //
        body = JSON.serialize(jsonClass);
        return body;
    }
    
    public static log__c createLog(string invId, String message, String type){
        log__c log = new log__c();
        log.Inventory__c = invId;
        log.Description__c = message;
        log.Type__c = type;
        return log;
    }
    
    public class InventoryUpdateJSON{
        public String operation; 
        public String unitId; 
        public String currentStatus; 
        public String newStatus; 
        public String restrictCode;
        public String remarks; 
        public String updatedBy; 

    }
}