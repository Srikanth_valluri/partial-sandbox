/* * * * * * * * * * * * * *
*  Class Name:   TaskUtilityTest
*  Purpose:      Unit test class for TaskUtility Controller
*  Author:       Hardik Mehta - ESPL
*  Company:      ESPL
*  Created Date: 22-Nov-2017
*  Updated Date: 22-Nov-2017
*  Type:         Test Class
* * * * * * * * * * * * */
@isTest
private class TaskUtilityTest 
{ 

  /* * * * * * * * * * * * *
  *  Method Name:  getTask_test
  *  Purpose:      This method is used to unit test functionality in getTask
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 22-Nov-2017
  * * * * * * * * * * * * */
  static testMethod void getTask_test()
  { 
    //insert sample data for Account
    Account objAccount = TestDataFactory_CRM.createPersonAccount();
    insert objAccount;
    
    Id aoptRecordTypeID = getRecordTypeIdForAOPT();

    Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , aoptRecordTypeID);
    objCase.Status = 'Submitted';
    insert objCase;
    
    objCase = [Select Id, OwnerId From Case Where Id =: objCase.Id];

    Test.startTest();
    
    TaskUtility.getTask((SObject)objCase, 'Generate Offer & Acceptance Letter', 'CRE', 
                            'AOPT', system.today().addDays(1));
    Test.stopTest();
  }

  /* * * * * * * * * * * * *
  *  Method Name:  getRecordTypeIdForAOPT
  *  Purpose:      This method is used to get record type ID for AOPT
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 22-Nov-2017
  * * * * * * * * * * * * */
  private static Id getRecordTypeIdForAOPT()
  {
    Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
    Id aoptRecordTypeID = caseRecordTypes.get('AOPT').getRecordTypeId();
    return aoptRecordTypeID;
  }
  
}