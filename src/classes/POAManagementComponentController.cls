/*
 * Description - Controller class for POAManagementComponent visualforce component
 *
 * Version        Date            Author            Description
 * 1.0            21/02/18        Vivek Shinde      Initial Draft
 */
public without sharing class POAManagementComponentController {
    public Boolean blnIsShowPOATable { get; set; }
    public List<Case> lstCase { get; set; }
    public POA__c objPOA { get; set; }
    public String crfAttachmentName { get; set; }
    public String crfAttachmentBody { get; set; }
    public String poaAttachmentName { get; set; }
    public String poaAttachmentBody { get; set; }
    public String otherAttachmentName { get; set; }
    public String otherAttachmentBody { get; set; }
    public Boolean blnIsShowPOACreationPanel { get; set; }
    public Boolean blnIsShowUnitSelectionPanel { get; set; }
    public List<String> lstSelectedUnits { get; set; }
    public List<SelectOption> lstUnits { get; set; }
    public POA_Unit__c objPOAUnit { get; set; }
    public String strPOAToEdit { get; set; }
    public List<String> lstSelectedRights { get; set; }
    public List<SelectOption> lstRights { get; set; }
    //public List<SelectOption> lstCustomerLevelRights { get; set; }
    //public List<SelectOption> lstUnitLevelRights { get; set; }
    public List<SelectOption> lstRightType { get; set; }
    public String strSelectedRightType { get; set; }
    public String strPOAValidTillDate {get; set;}
    
    public List<POAWrapper> lstPOAWrapper {get; set;}
    
    private List<String> lstActiveStatus;
    private String accountId;
    private String strSRType;
    private String caseId;
    public Case objCase {get; set;}
    private List<SR_attachments__c> lstSRattachmentAll;
    private Account objAccount;
    
    private static final Id idPOART = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POA').getRecordTypeId();
    
    public POAManagementComponentController() {
        strSRType = ApexPages.currentPage().getParameters().get('SRType');
        accountId = ApexPages.currentPage().getParameters().get('AccountId');
        caseId = ApexPages.currentPage().getParameters().get('caseId');
        if(String.isNotBlank(accountId) && strSRType == 'POA') {
            init();
        }
    }
    
    private void init() {
        //lstPOA = new List<POA__c>();
        lstPOAWrapper = new List<POAWrapper>();
        blnIsShowPOATable = true;
        //lstPOA.add(new POA__c(First_Name__c = 'FN', Last_Name__c = 'LN', POA_Verification_Status__c = 'POA Verification Pending'));
        lstSRattachmentAll = new list<SR_attachments__c>();
        blnIsShowPOACreationPanel = false;
        
        /*lstRights = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = POA_Unit__c.POA_Services__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            lstRights.add(new SelectOption(pickListVal.getLabel(), pickListVal.getLabel()));
        }*/
        
        //Query POA related to account
        /*if(String.isNotBlank(accountId)) {
            lstPOA = [Select Id, First_Name__c, Last_Name__c, Email_Address__c, Mobile_Number__c,
                      POA_Verification_Status__c From POA__c Where Related_to_Account__c =: accountId];
        }*/
        
        objAccount = [Select Id, Party_Id__c From Account Where Id =: accountId];
        objCase = new Case();
        objPOA = new POA__c();
        populatePOAList();
        populateUnits();
        populatePOARights();
    }
    
    // Method to populate POA list
    private void populatePOAList() {
        lstPOAWrapper = new List<POAWrapper>();
        if(String.isNotBlank(accountId)) {
            /*lstPOA = [Select Id, First_Name__c, Last_Name__c, Email_Address__c, Mobile_Number__c,
                      POA_Verification_Status__c, POA_Valid_Till__c, POA_Issued_By__c, POA_Number__c, POA_Nationality__c
                      From POA__c Where Related_to_Account__c =: accountId Order By CreatedDate];*/
                      
            /*for(POA_Related_Account__c objPRA: [Select Id, POA__c, Related_to_Account__c, POA__r.First_Name__c, POA__r.Last_Name__c, 
                POA__r.Email_Address__c, POA__r.Mobile_Number__c, POA__r.POA_Verification_Status__c, POA__r.POA_Valid_Till__c,
                POA__r.POA_Issued_By__c, POA__r.POA_Number__c, POA__r.POA_Nationality__c, POA__r.Passport_Number__c
                From POA_Related_Account__c Where Related_to_Account__c =: accountId Order By CreatedDate]) {
                
                lstPOA.add(new POA__c(
                    Id = objPRA.POA__c,
                    First_Name__c = objPRA.POA__r.First_Name__c,
                    Last_Name__c = objPRA.POA__r.Last_Name__c,
                    Email_Address__c = objPRA.POA__r.Email_Address__c,
                    Mobile_Number__c = objPRA.POA__r.Mobile_Number__c,
                    Passport_Number__c = objPRA.POA__r.Passport_Number__c,
                    POA_Verification_Status__c = objPRA.POA__r.POA_Verification_Status__c,
                    POA_Valid_Till__c = objPRA.POA__r.POA_Valid_Till__c,
                    POA_Issued_By__c = objPRA.POA__r.POA_Issued_By__c,
                    POA_Number__c = objPRA.POA__r.POA_Number__c,
                    POA_Nationality__c = objPRA.POA__r.POA_Nationality__c
                ));
            }*/
            
            for(Case objCase: [Select Id, Customer_First_Name__c, Customer_Last_Name__c, Email__c, Mobile_Phone_2__c, 
                               POA_Valid_Till__c, POA_Related_Account__c, POA_Related_Account__r.POA_Verification_Status__c
                               From Case Where AccountId =: accountId And RecordTypeId =: idPOART]) {
            	POAWrapper objPW = new POAWrapper();
                objPW.strPRAId = objCase.POA_Related_Account__c;
                objPW.strFirstName = objCase.Customer_First_Name__c;
                objPW.strLastName = objCase.Customer_Last_Name__c;
                objPW.strEmail = objCase.Email__c;
                objPW.strMobile = objCase.Mobile_Phone_2__c != null ? String.valueOf(objCase.Mobile_Phone_2__c) : '';
                objPW.dtValidTill = objCase.POA_Valid_Till__c;
                objPW.strVerificationStatus = objCase.POA_Related_Account__c != null ? objCase.POA_Related_Account__r.POA_Verification_Status__c : 'POA Verification Pending';
                lstPOAWrapper.add(objPW);                  
          	}
        }
    }
    // Method to populate list of selectoption of units
    private void populateUnits() {
        lstSelectedUnits = new List<String>();
        //Getting Active status values for booking units
        List<Booking_Unit_Active_Status__c> csActiveValues = Booking_Unit_Active_Status__c.getall().values();
        lstActiveStatus = new List<String>();
        for(Booking_Unit_Active_Status__c cs : csActiveValues) {
            lstActiveStatus.add(cs.Status_Value__c);
        }
        
        lstUnits = new List<SelectOption>();
        for(Booking_Unit__c objBU: [Select Id, Unit_Name__c, Registration_Status__c From Booking_Unit__c
                                    Where (Booking__r.Account__c = : accountId
                                    AND Registration_ID__c != NULL 
                                    AND Unit_Name__c != NULL)]) {
                                    //AND Registration_Status__c IN : lstActiveStatus)]) {
            lstUnits.add(new SelectOption(objBU.Id, objBU.Unit_Name__c));
            //blnIsShowPOACreationPanel = true;
            //mapBU.put(objBU.Id, objBU.Unit_Name__c);
        }
    }
    
    // Method to query POA rights from custom metadata type
    private void populatePOARights() {
        lstRightType = new List<SelectOption>();
        lstRightType.add(new SelectOption('', '--None--'));
        lstRightType.add(new SelectOption('Customer', 'Customer'));
        lstRightType.add(new SelectOption('Unit', 'Unit'));
        
        lstRights = new List<SelectOption>();
        //lstCustomerLevelRights = new List<SelectOption>();
        //lstUnitLevelRights = new List<SelectOption>();
        for(POA_Right__mdt objRight : [SELECT MasterLabel, QualifiedApiName, Applicable_Level__c, POA_Right_Full_Name__c 
                                        FROM POA_Right__mdt]) {
            lstRights.add(new SelectOption(objRight.POA_Right_Full_Name__c, objRight.POA_Right_Full_Name__c));
            /*if(String.isNotBlank(objRight.Applicable_Level__c) && objRight.Applicable_Level__c.equalsIgnoreCase('Customer')) {
                lstRights.add(new SelectOption(objRight.Applicable_Level__c, objRight.Applicable_Level__c));
            }
            else if(String.isNotBlank(objRight.Applicable_Level__c) && objRight.Applicable_Level__c.equalsIgnoreCase('Unit')) {
                lstUnitLevelRights.add(new SelectOption(objRight.Applicable_Level__c, objRight.Applicable_Level__c));
            }*/
        }
    }
    
    /*public void registerPOA() {
        //blnIsShowPOATable = false;
        blnIsShowPOACreationPanel = true;
        objPOA = new POA__c();
    }*/
    
    public void registerPOA() {
        blnIsShowPOATable = false;
        blnIsShowPOACreationPanel = true;
        objPOA = new POA__c();
        objCase = new Case();
    }
    
    // Method to save Case and POA details
    public Pagereference savePOADetails() {
        try {
            if(String.isNotEmpty(strSelectedRightType) && strSelectedRightType.equalsIgnoreCase('Unit') && lstSelectedRights.isEmpty()) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please select some rights for the selected units'));
                return null;
            }
            
            UploadMultipleDocController.data MultipleDocData = new UploadMultipleDocController.data();            
            List<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocRequest = formDocumentUploadRequest();
            
            if(lstMultipleDocRequest.Size()>0) {
                MultipleDocData = UploadMultipleDocController.getMultipleDocUrl(lstMultipleDocRequest);
                System.debug('--MultipleDocData--'+MultipleDocData);
                if(MultipleDocData != null && MultipleDocData.status == 'Exception') {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,MultipleDocData.message));                                         
                    errorLogger(MultipleDocData.message, objCase.Id, '');
                    return null;
                }                                   
                if(MultipleDocData != null && (MultipleDocData.Data == null || MultipleDocData.Data.Size() == 0)) {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,MultipleDocData.message)); 
                    errorLogger(MultipleDocData.message, objCase.Id, '');                                        
                    return null; 
                }
            }
            
            if( String.isNotBlank( crfAttachmentBody ) && String.isNotBlank( crfAttachmentName ) ) {
                formAttachment( extractName( crfAttachmentName ), extractBody( crfAttachmentBody ), objCase.Id , 'CRF Form'  );
            }
            crfAttachmentBody = '';
            crfAttachmentName = '';
            
            if( String.isNotBlank( poaAttachmentBody ) && String.isNotBlank( poaAttachmentName ) ) {
                formAttachment( extractName( poaAttachmentName ), extractBody( poaAttachmentBody ), objCase.Id , 'POA'  );
            }
            poaAttachmentBody = '';
            poaAttachmentName = '';
            
            if( String.isNotBlank( otherAttachmentBody ) && String.isNotBlank( otherAttachmentName ) ) {
                formAttachment( extractName( otherAttachmentName ), extractBody( otherAttachmentBody ), objCase.Id , 'Other'  );
            }
            otherAttachmentBody = '';
            otherAttachmentName = '';
            
            list<SR_attachments__c> lstSRattachmentValid = new list<SR_attachments__c>();
            if(lstSRattachmentAll.Size()>0 ){ //&& lstAttachment.Size()>0
                if(lstMultipleDocRequest.Size()>0 && MultipleDocData != null){
                    for(integer objInt=0; objInt<lstSRattachmentAll.Size(); objInt++ ) {
                        UploadMultipleDocController.MultipleDocResponse objData = MultipleDocData.Data[objInt];
                        if(objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url)) {
                           lstSRattachmentAll[objInt].Attachment_URL__c = objData.url;
                           lstSRattachmentValid.add(lstSRattachmentAll[objInt]);
                        }
                        else {
                           errorLogger('An exception has occurred while processing the request, document-'+objData.PARAM_ID+', PROC_STATUS-'+objData.PROC_STATUS+' URL-'+objData.url,objCase.Id,'');
                           ApexPages.addmessage(new ApexPages.message(
                           ApexPages.severity.Error,'An exception has occurred while processing the request, document-'+objData.PARAM_ID+', PROC_STATUS-'+objData.PROC_STATUS+' URL-'+objData.url));     
                        }
                    }                                   
                    if(lstSRattachmentValid.Size() > 0) {
                        insert lstSRattachmentValid;
                    }
                }
                else if(lstMultipleDocRequest.Size() > 0 && MultipleDocData == null){
                   ApexPages.addmessage(new ApexPages.message(
                   ApexPages.severity.Error,'An exception has occurred while processing the request of document upload, got null value, please contact the support team.'));                                        
                   return null;       
                }
            }
            
            //objPOA.Related_to_Account__c = accountId;
            /*objPOA.POA_Verification_Status__c = 'POA Verification Pending';
            objPOA.POA_Valid_Till__c = Date.parse(strPOAValidTillDate);
            objPOA.Right_Type__c = strSelectedRightType;
            upsert objPOA;*/
            
            //objCase.POA__c = objPOA.Id;
            objCase.Status = 'Submitted';
            objCase.Right_Type__c = strSelectedRightType;
            upsert objCase;
            system.debug('--objCase--'+objCase.Id);
            
            /*if(objPOA.Id != null) {
                delete [Select Id From POA_Unit__c Where POA_Related_Account__r.Related_to_Account__c =: accountId];
            }*/
            
            //delete [Select Id From SR_Booking_Unit__c where Case__c =: objCase.Id];
            
            //List<SR_Booking_Unit__c> lstSRBU = new List<SR_Booking_Unit__c>();
            List<Task> lstTask = new List<Task>();
            
            if(!lstSelectedUnits.isEmpty() && strSelectedRightType.equalsIgnoreCase('Unit')) {
                /*String strSelectedRights = '';
                system.debug('--lstSelectedRights--'+lstSelectedRights);
                for(String strSelectedRight: lstSelectedRights) {
                    strSelectedRights += strSelectedRight + ';';
                }
                strSelectedRights = strSelectedRights.removeEnd(';');
                
                system.debug('--strSelectedRights--'+strSelectedRights);*/
                
                /*List<POA_Unit__c> lstPU = new List<POA_Unit__c>();
                for(String strBUId: lstSelectedUnits) {
                    POA_Unit__c objPU = new POA_Unit__c();
                    objPU.Booking_Unit__c = strBUId;
                    objPU.POA__c = objPOA.Id;
                    objPU.POA_Services__c = strSelectedRights;
                    lstPU.add(objPU);
                }
                insert lstPU;*/
                
                lstTask = populateTasks(lstSelectedUnits, lstSelectedRights);
            }
            else if(strSelectedRightType.equalsIgnoreCase('Customer')) {
                List<String> lstBUIds = new List<String>();
                for(SelectOption objOption: lstUnits) {
                    lstBUIds.add(objOption.getValue());
                }
                lstTask = populateTasks(lstBUIds, lstSelectedRights);
            }
            
            insert lstTask;
            
            Pagereference objPageRef = new Pagereference('/' + objCase.Id);
            objPageRef.setRedirect(true);
            return objPageRef;
        } catch(Exception excGen) {
            system.debug('--excGen.getMessage()--'+excGen.getMessage());
            system.debug('--excGen.getStackTraceString()--'+excGen.getStackTraceString());
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, excGen.getMessage()));
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, excGen.getStackTraceString()));
        }
        return null;
    }
    
    // Method to form list of SR Booking Units
    /*private List<SR_Booking_Unit__c> populateSRBU(List<String> lstBUId, List<String> lstSelectedRights) {
        List<SR_Booking_Unit__c> lstSRBU = new List<SR_Booking_Unit__c>();
        for(String strBUId: lstBUId) {
            for(String strSelectedRight: lstSelectedRights) {
                SR_Booking_Unit__c objSRBU = new SR_Booking_Unit__c();
                objSRBU.Case__c = objCase.Id;
                objSRBU.Booking_Unit__c = strBUId;
                objSRBU.POA_Right__c = strSelectedRight;
                lstSRBU.add(objSRBU);
            }
        }
        return lstSRBU;
    }*/
    
    // Method to form list of Tasks
    private List<Task> populateTasks(List<String> lstBUId, List<String> lstSelectedRights) {
        List<Task> lstTask = new List<Task>();
        for(String strBUId: lstBUId) {
            for(String strSelectedRight: lstSelectedRights) {
                /*SR_Booking_Unit__c objSRBU = new SR_Booking_Unit__c();
                objSRBU.Case__c = objCase.Id;
                objSRBU.Booking_Unit__c = strBUId;
                objSRBU.POA_Right__c = strSelectedRight;
                lstSRBU.add(objSRBU);*/
                
                Task objTask = new Task();
                if(!Test.isRunningTest()) {
                    objTask =  TaskUtility.getTask((SObject)objCase, 'Verify POA Right', 'Legal', 
                                   'POA', system.today().addDays(1));
                }
                else {
                    objTask.WhatId = objCase.Id;
                    objTask.Subject = 'Verify POA Right';
                    objTask.Assigned_User__c = 'Legal';
                    objTask.Status = 'Not Started';
                }
                objTask.Booking_Unit__c = strBUId;
                objTask.POA_Right__c = strSelectedRight;
                lstTask.add(objTask);
            }
        }
        return lstTask;
    }
    
    // Method to form request for multiple document upload
    private List<UploadMultipleDocController.MultipleDocRequest> formDocumentUploadRequest() {
        List<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocRequest = new  List<UploadMultipleDocController.MultipleDocRequest>();
        Integer intIncrementor = 1;
        //System.debug('--crfAttachmentBody ---'+crfAttachmentBody +'---crfAttachmentName --'+crfAttachmentName );
        if( String.isNotBlank( crfAttachmentBody ) && String.isNotBlank( crfAttachmentName ) ) {
            
            UploadMultipleDocController.MultipleDocRequest objDocRequest = new UploadMultipleDocController.MultipleDocRequest();
            objDocRequest.category =  'Document';
            objDocRequest.entityName = 'Damac Service Requests'; 
            objDocRequest.fileDescription  =  'CRF Form';                        
            
            intIncrementor++;
            objDocRequest.fileId = objCase.CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+extractName(crfAttachmentName).substringAfterLast('.');
            objDocRequest.fileName = objCase.CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+extractName(crfAttachmentName).substringAfterLast('.');
            
            System.debug('---System.currentTimeMillis()--'+System.currentTimeMillis());
            System.debug('---intIncrementor--'+intIncrementor);
            System.debug('---objDocRequest.fileName--'+objDocRequest.fileName);
            System.debug('---extractName crfAttachmentName .--'+extractName(crfAttachmentName).substringAfterLast('.'));
            
            if(objCase != null){
                objDocRequest.registrationId =  objCase.CaseNumber;
            }
            objDocRequest.sourceFileName  = 'IPMS-'+objAccount.party_ID__C+'-'+extractName( crfAttachmentName );
            objDocRequest.sourceId  =  'IPMS-'+objAccount.party_ID__C+'-'+extractName( crfAttachmentName );                        
            if(String.isNotBlank(crfAttachmentBody)){
                blob objBlob = extractBody(crfAttachmentBody);
                if(objBlob != null){
                    objDocRequest.base64Binary =  EncodingUtil.base64Encode(objBlob);
                }
            }
            lstMultipleDocRequest.add(objDocRequest);                          
            
        }
        
        if( String.isNotBlank( PoaAttachmentBody ) && String.isNotBlank( PoaAttachmentName ) ) {
            
            UploadMultipleDocController.MultipleDocRequest objDocRequest = new UploadMultipleDocController.MultipleDocRequest();
            objDocRequest.category =  'Document';
            objDocRequest.entityName = 'Damac Service Requests'; 
            objDocRequest.fileDescription  =  'Power of Attorney';
                                                  
            intIncrementor++;
            objDocRequest.fileId = objCase.CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+extractName(PoaAttachmentName).substringAfterLast('.');
            objDocRequest.fileName = objCase.CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+extractName(PoaAttachmentName).substringAfterLast('.');
            
            
            if(objCase != null){
                objDocRequest.registrationId =  objCase.CaseNumber;
            }
            objDocRequest.sourceFileName  = 'IPMS-'+objAccount.party_ID__C+'-'+extractName( PoaAttachmentName );
            objDocRequest.sourceId  =  'IPMS-'+objAccount.party_ID__C+'-'+extractName( PoaAttachmentName );
            if(String.isNotBlank(PoaAttachmentBody)){
                blob objBlob = extractBody(PoaAttachmentBody);
                if(objBlob != null){
                    objDocRequest.base64Binary =  EncodingUtil.base64Encode(objBlob);
                }
            }
            lstMultipleDocRequest.add(objDocRequest);                          
            
        }
        
        if( String.isNotBlank( otherAttachmentBody ) && String.isNotBlank( otherAttachmentName ) ) {
            
            UploadMultipleDocController.MultipleDocRequest objDocRequest = new UploadMultipleDocController.MultipleDocRequest();
            objDocRequest.category =  'Document';
            objDocRequest.entityName = 'Damac Service Requests'; 
            objDocRequest.fileDescription  =  'Power of Attorney';
                                                  
            intIncrementor++;
            objDocRequest.fileId = objCase.CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+extractName(otherAttachmentName).substringAfterLast('.');
            objDocRequest.fileName = objCase.CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+extractName(otherAttachmentName).substringAfterLast('.');
            
            
            if(objCase != null){
                objDocRequest.registrationId =  objCase.CaseNumber;
            }
            objDocRequest.sourceFileName  = 'IPMS-'+objAccount.party_ID__C+'-'+extractName( otherAttachmentName );
            objDocRequest.sourceId  =  'IPMS-'+objAccount.party_ID__C+'-'+extractName( otherAttachmentName );
            if(String.isNotBlank(otherAttachmentBody)){
                blob objBlob = extractBody(otherAttachmentBody);
                if(objBlob != null){
                    objDocRequest.base64Binary =  EncodingUtil.base64Encode(objBlob);
                }
            }
            lstMultipleDocRequest.add(objDocRequest);                          
            
        }
        return lstMultipleDocRequest;
    }
    
    // Method for logging error
    private void errorLogger(string strErrorMessage, string strCaseID, string strBookingUnitID) {
        Error_Log__c objError = new Error_Log__c();
        objError.Error_Details__c = strErrorMessage;
        if(String.isNotBlank(strCaseID) && strCaseID.startsWith('500')){
            objError.Case__c = strCaseID;
        }
        if(String.isNotBlank(strBookingUnitID)){
            objError.Booking_unit__c = strBookingUnitID;
        }
        insert objError;
    }
    
    // Method to form attachment object
    public void formAttachment( String strfileName, Blob objBody, Id parentId, String strDocType ) {
        /*Attachment objAttach = new Attachment() ;
        objAttach.Body = objBody ;
        objAttach.Name = strfileName ;
        //objAttach.ParentId = obj.id;
        lstAttachment.add( objAttach );*/
        
        SR_Attachments__c objAttach = new SR_Attachments__c ();
        objAttach.name = strfileName;
        objAttach.Case__c = objCase.Id;         
        objAttach.type__c = strDocType ;
        objAttach.Account__c = accountId;
        //obj.IsValid__c = (docType.equalsIgnoreCase('Wire Transfer Form') || docType.equalsIgnoreCase('Other')) ? true : false;
              
        lstSRattachmentAll.add(objAttach);
    }
    
    private String extractName( String strName ) {
        return strName.substring( strName.lastIndexOf('\\')+1 ) ;
    }
    private Blob extractBody( String strBody ) {
        //System.debug('--strBody ---'+strBody );
        strBody = EncodingUtil.base64Decode( strBody ).toString();
        return EncodingUtil.base64Decode( strBody.substring( strBody.lastIndexOf(',')+1 ) );
    }
    
    public void showUnitSelectionPanel() {
        try {
            blnIsShowPOATable = false;
            blnIsShowPOACreationPanel = true;
            blnIsShowUnitSelectionPanel = true;
            
            if(objCase.Id == null) {
                objCase.AccountId = accountId;
                objCase.RecordTypeId = idPOART;
                objCase.Origin = 'Walk-In';
                objCase.SR_Type__c = 'POA';
                objCase.POA_Valid_Till__c = Date.parse(strPOAValidTillDate);
                upsert objCase;
                
                objCase = [Select Id, AccountId, CaseNumber, OwnerId, Customer_First_Name__c, Customer_Last_Name__c, Email__c, Mobile_Phone_2__c, 
                           POA_Valid_Till__c, POA_Related_Account__c, POA_Related_Account__r.POA_Verification_Status__c, 
                           New_CR__c, POA_Issued_By_Picklist__c, POA_Number__c, Nationality__c
                           From Case Where Id =: objCase.Id];
                system.debug('--objCase next--'+objCase);
            }
            
            // Duplicate match for existing POA
            /*List<POA__c> lstPOA = [Select Id, First_Name__c, Last_Name__c, Email_Address__c, Mobile_Number__c, Passport_Number__c,
                      POA_Verification_Status__c, POA_Valid_Till__c, POA_Issued_By__c, POA_Number__c, POA_Nationality__c
                      From POA__c Where (Passport_Number__c =: objPOA.Passport_Number__c And 
                      POA_Nationality__c =: objPOA.POA_Nationality__c) Or Email_Address__c =: objPOA.Email_Address__c Order By CreatedDate];
            if(lstPOA != null && !lstPOA.isEmpty()) {
                objPOA.Id = lstPOA[0].Id;
            }*/
            //lstUnits = new List<SelectOption>();
            //Map<Id, String> mapBU = new Map<Id, String>();
            //lstUnits.add(new SelectOption('abc', 'ABC'));
            
            /*for(Booking_Unit__c objBU: [Select Id, Unit_Name__c, Registration_Status__c From Booking_Unit__c
                                        Where (Booking__r.Account__c = : accountId
                                        AND Registration_ID__c != NULL 
                                        AND Unit_Name__c != NULL
                                        AND Registration_Status__c IN : lstActiveStatus)]) {
                //lstUnits.add(new SelectOption(objBU.Id, objBU.Unit_Name__c));
                mapBU.put(objBU.Id, objBU.Unit_Name__c);
            }*/
            
            Set<Id> setBUId = new Set<Id>();
            //Query booking units which are part of existing POA
            /*for(POA_Unit__c objPU: [Select Id, Booking_Unit__c From POA_Unit__c
                Where Booking_Unit__c IN: mapBU.keySet()]) {
                setBUId.add(objPU.Booking_Unit__c);
            }
            
            for(Id idBU: mapBU.keySet()) {
                if(!setBUId.contains(idBU)) {
                    lstUnits.add(new SelectOption(idBU, mapBU.get(idBU)));
                }
            }*/
            
            system.debug('--objPOA--'+objPOA);
            /*if(objPOA.Id != null) {
                String strServices = '';
                lstSelectedUnits = new List<String>();
                for(POA_Unit__c objPU: [Select Id, Booking_Unit__c, Booking_Unit__r.Unit_Name__c, POA_Services__c 
                                        From POA_Unit__c Where POA__c =: objPOA.Id]) {
                    lstSelectedUnits.add(objPU.Booking_Unit__c);
                    lstUnits.add(new SelectOption(objPU.Booking_Unit__c, objPU.Booking_Unit__r.Unit_Name__c));
                    strServices = objPU.POA_Services__c;
                }
                
                system.debug('--strServices--'+strServices);
                if(String.isNotBlank(strServices)) {
                    lstSelectedRights = new List<String>();
                    
                    for(String strService: strServices.split(';')) {
                        lstSelectedRights.add(strService);
                    }
                    system.debug('--lstSelectedRights--'+lstSelectedRights);
                }
            }*/
        } catch(Exception excGen) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, excGen.getMessage()));
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, excGen.getStackTraceString()));
        }
    }
    
    // Method to show unit selection panel and initialize the required variables
    public void editPOA() {
        /*List<POA__c> lstPOA = [Select Id, First_Name__c, Last_Name__c, Email_Address__c, Mobile_Number__c, Right_Type__c,
                               POA_Verification_Status__c, POA_Valid_Till__c, Passport_Number__c,
                               POA_Issued_By__c, POA_Number__c, POA_Nationality__c From POA__c Where Id =: strPOAToEdit];
        if(lstPOA != null && !lstPOA.isEmpty()) {
            objPOA = lstPOA[0];
            strPOAValidTillDate = objPOA.POA_Valid_Till__c != null ? objPOA.POA_Valid_Till__c.format() : '';
            strSelectedRightType = objPOA.Right_Type__c;
            
            Set<String> setRights = new Set<String>();
            Set<String> setBUId = new Set<String>();
            for(POA_Unit__c objPU: [Select Id, Booking_Unit__c, Booking_Unit__r.Unit_Name__c, POA_Right__c 
                                    From POA_Unit__c Where POA_Related_Account__r.Related_to_Account__c =: accountId]) {
                if(String.isNotBlank(objPU.POA_Right__c))
                    setRights.add(objPU.POA_Right__c);
                if(String.isNotBlank(strSelectedRightType) && strSelectedRightType.equalsIgnoreCase('Unit'))
                    setBUId.add(objPU.Booking_Unit__c);
            }
            lstSelectedRights = new List<String>();
            lstSelectedUnits = new List<String>();
            lstSelectedRights.addAll(setRights);
            lstSelectedUnits.addAll(setBUId);
            blnIsShowPOATable = false;
            blnIsShowPOACreationPanel = true;
        }*/
        //showUnitSelectionPanel();
    }
    
    // Method to delete POA and related units
    public void deletePOA() {
        List<POA_Related_Account__c> lstPOA1 = [Select Id, POA_Verification_Status__c 
                                                From POA_Related_Account__c Where Id =: strPOAToEdit];
        if(lstPOA1 != null && !lstPOA1.isEmpty()) {
            lstPOA1[0].POA_Verification_Status__c = 'POA Terminated';
            update lstPOA1;
            populatePOAList();
                      
            // Also delete POA Account
        }
    }
    
    // Method to return to the main page
    public Pagereference cancelPOA() {
        Pagereference objPageRef = new Pagereference('/' + accountId);
        objPageRef.setRedirect(true);
        return objPageRef;
    }
    
    public class POAWrapper {
        public String strPRAId {get; set;}
        public String strFirstName {get; set;}
        public String strLastName {get; set;}
        public String strEmail {get; set;}
        public String strMobile {get; set;}
        public Date dtValidTill {get; set;}
        public String strVerificationStatus {get; set;}
    }
}