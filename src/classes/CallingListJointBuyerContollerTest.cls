@isTest
private class CallingListJointBuyerContollerTest {
    static testMethod void test_Method1() {
        //insert sample data for Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        Account objAcc = [SELECT Id from Account LIMIT 1];
        
        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        
        List<Booking__c> lstBookings = new List<Booking__c>();
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 1);
        insert lstBookings;
        
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 1);
        insert lstBookingUnits;
        
        list<Buyer__c> lstBuyer = new list<Buyer__c>();
        lstBuyer = TestDataFactory_CRM.createBuyer(lstBookingUnits[0].Booking__c,5, objAcc.Id);
        insert lstBuyer;
        
        Id RecordTypeIdCollection = [
            SELECT Id
            FROM RecordType
            WHERE SObjectType='Calling_List__c' 
            AND DeveloperName='Recovery_Calling_List'
            AND IsActive = TRUE LIMIT 1
        ].Id;
        
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;

        Calling_List__c  objCallingList = new Calling_List__c();
		objCallingList.IsHideFalse_Relationship__c = objAcc.Id;
		objCallingList.Registration_ID__c = lstBookingUnits[0].Registration_ID__c ;
		objCallingList.Inv_Due__c = 0;
		objCallingList.DM_Due_Amount__c = 0;
		objCallingList.RecordTypeId = RecordTypeIdCollection;
        insert objCallingList;
        
        Test.startTest();
            PageReference PageInst = Page.CallingListJointBuyer;
            Test.setCurrentPage(PageInst);
            String callingListId = PageInst.getParameters().put('Id',String.valueOf(objCallingList.Id));
            ApexPages.StandardController sc = new ApexPages.StandardController(objCallingList);
            CallingListJointBuyerContoller controller = new CallingListJointBuyerContoller(sc);
            controller.init();    
        Test.stopTest();
    }
}