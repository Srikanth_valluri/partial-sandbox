/****************************************************************************************
Description : Controller of VF page 'RebateServicePage' that gets and sends receipt Ids *
              to/from IPMS on closure of Finance Task of Rebate SR.                     *
----------------------------------------------------------------------------------------*
Version     Date        Author              Description                                 *
                                                                                        *
1.0         28/8/2019   Aishwarya Todkar    Initial Drafet                              *
1.1         9/10/2019   Aishwarya Todkar    Fixed issue of authorization                *
1.2         17/03/2020  Aishwarya Todkar    Updated Finance Task Name                   *
*****************************************************************************************/

public Class RebateOnAdvanceService {


    public static List<SelectOption> lstReceiptIds { get; set; }

    public  List<ReceiptIdWrapper> lstReceiptIdwapper { get; set; }

    public static List<String> selectedReceiptIds { get; set; }

    public Boolean fromDetailPage = false;

    public Boolean showPanel { get; set; }

    public Id caseId;

    public String registrationId;
    
    public Case objCase;
    
    public Task objTask;

    public RebateOnAdvanceService (ApexPages.StandardController stdController) {
        this.objCase = (Case)stdController.getRecord();
        caseId = objCase.id;
        showPanel = true;
        if( caseId != null ) {
            fromDetailPage = true;
            List<Case> lstCase = [SELECT 
                                        Id
                                        , Registration_Id__c 
                                    FROM 
                                        Case 
                                    WHERE 
                                        Id =: caseId
                                    AND 
                                        Registration_Id__c != null];
            
            List<Task> lstTask = [SELECT
                                        Id
                                        , Status
                                        , Update_IPMS__c
                                    FROM
                                        Task
                                    WHERE
                                        WhatId =: caseId
                                    AND
                                        Subject != null
                                    AND 
                                        Subject Like  '%Check advance paid and apply%' 
                                    LIMIT 1];
            System.debug('lstTask  == ' + lstTask);
            if( lstTask == null || lstTask.size() == 0 ) {
                showPanel = false;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Task not found.'));
            }
            else if(lstTask != null && lstTask.size() > 0 && (lstTask[0].Status == 'Completed' || lstTask[0].Status == 'Closed')) {
                showPanel = false;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Task already ' + lstTask[0].Status));
            }
            else {
                objTask = new Task();
                objTask = lstTask[0];

                if(lstCase != null && lstCase.size() > 0 ) {
                
                    if(lstCase[0].Registration_Id__c == null) {
                        showPanel = false;
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Registration Id can not be blank.'));
                    }
                    else {
                        registrationId = lstCase[0].Registration_Id__c;
                        getReceipts( caseId, registrationId);
                    }
                }
            }
        }
    }
    
    public RebateOnAdvanceService() {}

/**************************************************************************************
Method Name : callGetReceipts
Date        : 28/8/2019
Parameters  : List of Registration Id
Return type : void
***************************************************************************************/
    /*@InvocableMethod
    public Static void callGetReceipts( List<Id> lstTaskId ) {

        fromDetailPage = false;

        lstReceiptIds = new List<SelectOption>();
        System.debug('lstTaskId -=' + lstTaskId);
        if( lstTaskId != null && lstTaskId.size() > 0 ) {
            Set<Id> whatIdSet = new Set<Id>();

            for(Task objTask : [SELECT 
                                    WhatId 
                                FROM 
                                    Task 
                                WHERE Id IN : lstTaskId]) {
                whatIdSet.add(objTask.WhatId);
            }

            if( whatIdSet != null && whatIdSet.size() > 0 ) {
                for( Case objCase : [SELECT 
                                    Id
                                    , Registration_Id__c 
                                FROM 
                                    Case 
                                WHERE 
                                    Id IN : whatIdSet
                                AND 
                                    Registration_Id__c != null] ) {
                    getReceipts( objCase.Id, objCase.Registration_Id__c );
                }
            }
        }
    }*/

/**************************************************************************************
Method Name : getReceipts
Date        : 28/8/2019
Parameters  : Registration Id
Return type : void
***************************************************************************************/
    //@future(callout = true) //commented as it will be executing from vf page only for now
    public void getReceipts( Id caseId, String regId) {
        System.debug('caseId = ' + caseId);
        System.debug('regId = ' + regId);
        Credentials_Details__c creds = getCredentials('Rebate Get Receipt');
        
        lstReceiptIdwapper = new List<ReceiptIdWrapper>();

        Set<Integer> receiptIdSet = new Set<Integer>();

        if( String.isNotBlank(regId) && creds != null ) {

            String userName = creds.User_Name__c; //crp1user
            String password = creds.Password__c; //oracle_user
            String endPoint = creds.Endpoint__c;
            //'https://dxbhoebtstap.damacholding.home:4488/webservices/rest/Rebate/xxdc_open_receipt_proc/';

            String requestBody = '{' +
                                    '"XXDC_OPEN_RECEIPT_PROC_Input":' +
                                    '{' +
                                        '"RESTHeader":' +
                                        '{' +
                                            '"Responsibility":"ONT_ICP_SUPER_USER"' +
                                            ', "RespApplication":"ONT"' +
                                            ', "SecurityGroup":"STANDARD"' +
                                            ', "NLSLanguage":"AMERICAN"' +
                                        '}' +
                                    ', "InputParameters":' +
                                        '{ ' +
                                            '"P_REGISTRATION_ID":"' + regId + '"' +
                                        '}' +
                                    '}' +
                                '}';
            
            String headerValue = 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(userName + ':' + password));
            
            HttpRequest request = new HttpRequest();

            request.setMethod('POST');

            request.setHeader('Accept', 'application/json');

            request.setHeader('Content-Type', 'application/json');

            request.setHeader('Accept-Language', 'en-US');

            request.setHeader('Authorization', headerValue);

            request.setBody(requestBody);

            request.setEndPoint(endPoint);

            System.debug('request = ' + request);
            System.debug('requestBody = ' + request.getBody());

            Http http = new Http();
            HttpResponse response = new HttpResponse();

            try {
                /*ReceiptIdWrapper wrapObj1 = new ReceiptIdWrapper();
                wrapObj1.isSelected = false;
                wrapObj1.receiptId = 882343;
                wrapObj1.receiptAmount = '-1277.9';
                wrapObj1.receiptNumber = 'Unionpay/ 6222***8059/ 012345';
                wrapObj1.receiptDate =  '2019-03-12';
                lstReceiptIdwapper.add(wrapObj1);

                ReceiptIdWrapper wrapObj2 = new ReceiptIdWrapper();
                wrapObj2.isSelected = false;
                wrapObj2.receiptId = 32323;
                wrapObj2.receiptAmount = '-6224.9';
                wrapObj2.receiptNumber = 'Unionpay/ 6222***8059/ 012346';
                wrapObj2.receiptDate =  '2019-03-12';
                lstReceiptIdwapper.add(wrapObj2);
                
                ReceiptIdWrapper wrapObj3 = new ReceiptIdWrapper();
                wrapObj3.isSelected = false;
                wrapObj3.receiptId = 973524;
                wrapObj3.receiptAmount = '4454.6';
                wrapObj3.receiptNumber = 'Unionpay/ 6222***8059/ 012347';
                wrapObj3.receiptDate =  '2019-03-12';
                lstReceiptIdwapper.add(wrapObj3);
                
                ReceiptIdWrapper wrapObj4 = new ReceiptIdWrapper();
                wrapObj4.isSelected = false;
                wrapObj4.receiptId = 35454;
                wrapObj4.receiptAmount = '62322.9';
                wrapObj4.receiptNumber = 'Unionpay/ 6222***8059/ 012348';
                wrapObj4.receiptDate =  '2019-03-12';
                lstReceiptIdwapper.add(wrapObj4);*/

                response = http.send(request);

                System.debug('response body ==' + response.getBody());
                System.debug('response status code ==' + response.getStatusCode());
                System.debug('response status ==' + response.getStatus());

                if( response != null && response.getBody() != null && response.getStatusCode() == 200) {

                    CashReceiptResponse responseObj = (CashReceiptResponse) System.JSON.deserialize(
                                                        response.getBody(), CashReceiptResponse.class);
                    System.debug('deserialized response : ' + responseObj);
                       String receiptIds = '';
                    //fill receipt id set
                    if(responseObj != null 
                    && responseObj.OutputParameters != null 
                    && responseObj.OutputParameters.L_OPEN_REC_TBL != null
                    && responseObj.OutputParameters.L_OPEN_REC_TBL.L_OPEN_REC_TBL_ITEM != null) {
                        
                        for(cls_L_OPEN_REC_TBL_ITEM L_OPEN_REC_TBL_ITEM : responseObj.OutputParameters.L_OPEN_REC_TBL.L_OPEN_REC_TBL_ITEM) {
                            
                            if(L_OPEN_REC_TBL_ITEM.CASH_RECEIPT_ID != null) {
                                receiptIds += L_OPEN_REC_TBL_ITEM.CASH_RECEIPT_ID;
                                //receiptIdSet.add(Integer.valueOf(L_OPEN_REC_TBL_ITEM.CASH_RECEIPT_ID));
                                /*lstReceiptIds.add(new SelectOption(L_OPEN_REC_TBL_ITEM.CASH_RECEIPT_ID
                                                                ,L_OPEN_REC_TBL_ITEM.CASH_RECEIPT_ID));*/

                                ReceiptIdWrapper wrapObj = new ReceiptIdWrapper();
                                wrapObj.isSelected = false;
                                wrapObj.receiptId = Integer.valueOf(L_OPEN_REC_TBL_ITEM.CASH_RECEIPT_ID);
                                wrapObj.receiptAmount = String.isNotBlank(L_OPEN_REC_TBL_ITEM.RECEIPT_AMOUNT) ? L_OPEN_REC_TBL_ITEM.RECEIPT_AMOUNT : '';
                                wrapObj.receiptNumber = String.isNotBlank(L_OPEN_REC_TBL_ITEM.RECEIPT_NUMBER) ? L_OPEN_REC_TBL_ITEM.RECEIPT_NUMBER : ''; 
                                wrapObj.receiptDate = String.isNotBlank(L_OPEN_REC_TBL_ITEM.RECEIPT_DATE) 
                                                        ? String.valueOf(Date.valueOf(L_OPEN_REC_TBL_ITEM.RECEIPT_DATE)) : '';
                                lstReceiptIdwapper.add(wrapObj);
                                System.debug('lstReceiptIdwapper==' + lstReceiptIdwapper);
                            }
                        }//End for

                        /*if( String.isNotBlank(receiptIds) ) {
                            receiptIds = receiptIds.removeEnd(',');
                            System.debug('RebateOnAdvance_Queueable enqueued ==');
                            if(!Test.isRunningTest() && !fromDetailPage){
                                
                                //system.enqueueJob(new RebateOnAdvance_Queueable(regId, receiptIds, caseId));
                                //createAdvanceCM( regId, receiptIdSet, caseId );
                            }
                        }*/
                    }// End deserialized responseObj if
                    else {
                        //Add error log record
                        if(responseObj != null && responseObj.OutputParameters != null && responseObj.OutputParameters.X_ERRBUF != null) {
                            
                            showPanel = false;
                            if(fromDetailPage) {
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, responseObj.OutputParameters.X_ERRBUF));
                            }
                            else {
                                Error_log__c objErr =  GenericUtility.createErrorLog(
                                                            responseObj.OutputParameters.X_ERRBUF
                                                            , ''
                                                            , ''
                                                            , ''
                                                            , caseId);
                                if(objErr != null) {
                                    insert objErr;
                                    System.debug('objErr 1 = ' + objErr);
                                }                            
                            }   
                        }
                    }
                }//End response if
                else {

                    showPanel = false;
                    if(fromDetailPage) {
                        if(response == null) 
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Could Not Found Responsne.'));
                        else if(response.getStatusCode() != 200 )
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, response.getBody()));
                    }
                    else {
                        // create error log record
                        Error_log__c objErr =  GenericUtility.createErrorLog('Could Not Found Responsne.', '', '', '', caseId);
                        if(objErr != null) {
                            insert objErr;
                        }
                        System.debug('objErr 2 = ' + objErr);
                    }
                }
                
            }//End try
            catch( Exception e ) {

                showPanel = false;
                System.debug('getReceipts Exception = ' + e.getmessage());
                if(fromDetailPage) {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, e.getmessage()));
                }
                else {
                    Error_log__c objErr =  GenericUtility.createErrorLog(e.getmessage(), '', '', '', caseId);
                    if(objErr != null) {
                        insert objErr;
                    }
                    System.debug('objErr 3 = ' + objErr); 
                }
                //system.enqueueJob(new RebateOnAdvance_Queueable(regId, '3206718,3206719', caseId)); //for testing
            }
        }//End regId if
    }

/**************************************************************************************
Method Name : goBack
Date        : 01/9/2019
Parameters  : None
Return type : PageReference
***************************************************************************************/
    public PageReference goBack() {
            return new PageReference('/' + caseId);
    }

/**************************************************************************************
Method Name : doValidation
Date        : 01/9/2019
Parameters  : None
Return type : PageReference
***************************************************************************************/
    public PageReference doValidation() {
    
        Set<integer> receiptIdSet = new Set<Integer>();
        System.debug('lstReceiptIdwapper =' + lstReceiptIdwapper);
        System.debug('registrationId =' + registrationId);
        System.debug('caseId =' + caseId);

        if(lstReceiptIdwapper == null || lstReceiptIdwapper.size() == 0) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please Select Receipt Id.'));
            return null;
        }
        else {
            for(ReceiptIdWrapper wrapObj : lstReceiptIdwapper) {
                if( wrapObj.isSelected )
                    receiptIdSet.add( wrapObj.receiptId);
            }
        }
        if(String.isBlank(registrationId)) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Registration Id can not be blank.'));
            return null;
        }

        if(caseId == null) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Case Id can not be blank.'));
            return null;
        }

        System.debug('receiptIdSet = ' + receiptIdSet);

        if( receiptIdSet.size() > 0 ) {
            createAdvanceCM(registrationId, receiptIdSet, caseId);
        }
        else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Warning, 'Please Select at least one Receipt Id.'));
        }    
            
        return null;
    }

/**************************************************************************************
Method Name : createAdvanceCM
Date        : 01/9/2019
Parameters  : Registration Id, Set of ReceiptIds and Case Id
Return type : PageReference
***************************************************************************************/
   Public PageReference createAdvanceCM(String regId, Set<Integer> receiptIdSet, Id caseId) {

        System.debug(' ==== createAdvanceCM ====');
        
        Credentials_Details__c creds = RebateOnAdvanceService.getCredentials('Rebate Create Advance CM');

        if( String.isNotBlank(regId) && receiptIdSet != null && receiptIdSet.size() > 0 && creds != null ) {
            
            String userName = creds.User_Name__c; //crp1user
            String password = creds.Password__c; //oracle_user
            String endPoint = creds.Endpoint__c;
            //'https://dxbhoebtstap.damacholding.home:4488/webservices/rest/Rebate/create_advance_cm/'

            String cashReceiptBody = '';
            for( Integer CASH_RECEIPT_ID : receiptIdSet ) {
                cashReceiptBody += '{ "CASH_RECEIPT_ID":' + CASH_RECEIPT_ID + '},';
            }

            cashReceiptBody = cashReceiptBody.removeEnd(',');

            System.debug('cashReceiptBody = ' + cashReceiptBody);

            String requestBody = '{' +
                                    '"CREATE_ADVANCE_CM_Input":' +
                                        '{' +
                                            '"RESTHeader":' +
                                                '{' +
                                                    '"Responsibility":"ONT_ICP_SUPER_USER",' +
                                                    '"RespApplication":"ONT", ' +
                                                    '"SecurityGroup":"STANDARD",' +
                                                    '"NLSLanguage":"AMERICAN"' +
                                                '},' +
                                            '"InputParameters":' +
                                                '{' +
                                                    '"P_REGISTRATION_ID":"' + regId + '",' +
                                                    '"L_OPEN_REC_TBL":' +
                                                        '{' +
                                                            '"L_OPEN_REC_TBL_ITEM":' +
                                                                '[' +
                                                                    cashReceiptBody +
                                                                ']' +
                                                        '}' +
                                                '}' +
                                        ' }' +
                                    '}';
            String headerValue = 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(userName + ':' + password));
            HttpRequest request = new HttpRequest();
            
            request.setMethod('POST');

            request.setHeader('Accept','application/json');

            request.setHeader('Content-Type','application/json');

            request.setHeader('Accept-Language','en-US');

            request.setHeader('Authorization',headerValue);

            request.setBody(requestBody);

            request.setEndPoint(endPoint);

            System.debug('request = ' + request);
            System.debug('requestBody = ' + request.getBody());

            Http http = new Http();
            HttpResponse response = new HttpResponse();

            try {
                response = http.send(request);

                System.debug('response body ==' + response.getBody());
                System.debug('response status code ==' + response.getStatusCode());
                System.debug('response status ==' + response.getStatus());
                if( response != null && response.getBody() != null) {
                    
                    //Parse the response
                    RebateOnAdvanceService.CashReceiptResponse responseObj 
                        = (RebateOnAdvanceService.CashReceiptResponse)System.JSON.deserialize(
                                                        response.getBody(), RebateOnAdvanceService.CashReceiptResponse.class);
                    System.debug('deserialized response 2: ' + responseObj);

                    //if respose is successful then close the SR
                    if( responseObj != null && responseObj.OutputParameters != null 
                    && responseObj.OutputParameters.X_ERRCODE != null
                    && responseObj.OutputParameters.X_ERRCODE == 'S') {
                        
                        System.debug('Updating case');
                        if(String.isNotBlank(String.valueOf(caseId)))
                            Update new Case(Id = caseId, status = 'Closed');
                            Update new task(Id = objTask.Id, Status = 'Closed', Update_IPMS__c = true );
                            return new PageReference('/' + caseId);
                    }
                    //else log error
                    else if(responseObj != null && responseObj.OutputParameters != null 
                    && responseObj.OutputParameters.X_ERRCODE != null
                    && responseObj.OutputParameters.X_ERRCODE == 'E'
                    && responseObj.OutputParameters.X_ERRBUF != null) {
                        
                        showPanel = false;
                        System.debug('Error 1');
                        if(fromDetailPage) {
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, responseObj.OutputParameters.X_ERRBUF));
                            return null;
                        }
                        else {
                            Error_log__c objErr =  GenericUtility.createErrorLog(responseObj.OutputParameters.X_ERRBUF, '', '', '', caseId);
                            if(objErr != null) {
                                insert objErr;
                            }
                        }
                    }

                }//End response null if
                else {

                    showPanel = false;
                    System.debug('Error 2');
                    // create error log record
                    if(fromDetailPage) {
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Could Not Found Responsne.'));
                        return null;
                    }
                    else {
                        Error_log__c objErr =  GenericUtility.createErrorLog('Could Not Found Responsne.', '', '', '', caseId);
                        if(objErr != null) {
                            insert objErr;
                        }
                    }
                }
            }
            catch( Exception e ) {
                System.debug('Error 3 getReceipts Exception = ' + e.getmessage());
                system.debug('fromDetailPage='+ fromDetailPage);
                
                showPanel = false;
                // create error log record
                if(fromDetailPage) {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, e.getmessage()));
                    return null;
                }
                else {
                    Error_log__c objErr =  GenericUtility.createErrorLog(e.getmessage(), '', '', '', caseId);
                    if(objErr != null) {
                        insert objErr;
                    }
                }
            }
        }//End regId if
        
        return null;
    }//createAdvanceCM End

/**************************************************************************************
Method Name : getCredentials
Date        : 28/8/2019
Parameters  : Name
Return type : Credentials_Details__c
***************************************************************************************/
    Public Static Credentials_Details__c getCredentials(String credName) {
        if(String.isNotBlank(credName)) {

            List<Credentials_Details__c> lstCreds = [ SELECT
                                                            Id
                                                            , Name
                                                            , User_Name__c
                                                            , Password__c
                                                            , Endpoint__c
                                                        FROM
                                                            Credentials_Details__c
                                                        WHERE
                                                            Name =: credName];
            if( lstCreds != null && lstCreds.size() > 0 ) {
                return lstCreds[0];
            }
        }
        return null;
    }
    
/**************************************************************************************
Name        : ReceiptIdWrapper
Date        : 02/09/2019
***************************************************************************************/
    public class ReceiptIdWrapper {
        Public Boolean isSelected {get; set; }
        public Integer receiptId {get; set; }
        Public String receiptNumber { get; set; }
        Public String receiptAmount { get; set; }
        Public String receiptDate { get; set; }   
    }

/**************************************** RESPONSE WRAPPERS START  ***************************************************/
    public Class CashReceiptResponse {

        public OutputParameters_cls OutputParameters;
    }
    public class OutputParameters_cls {
        public String xmlns;
        public String xmlns_xsi;
        public cls_L_OPEN_REC_TBL L_OPEN_REC_TBL;
        public String X_ERRBUF;
        public String X_ERRCODE;
    }

    public class cls_L_OPEN_REC_TBL_ITEM  {
        public String REGISTRATION_ID;
        public String CASH_RECEIPT_ID;
        public String RECEIPT_NUMBER;
        public String RECEIPT_DATE;
        public String RECEIPT_AMOUNT;
    }

    public class cls_L_OPEN_REC_TBL {
        public List<cls_L_OPEN_REC_TBL_ITEM> L_OPEN_REC_TBL_ITEM;
    }
/**************************************** RESPONSE WRAPPERS END ***************************************************/
}