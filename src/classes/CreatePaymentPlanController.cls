/****************************************************************************************************
* Name               : CreatePaymentPlanController                                                  *
* Description        : Controller class Create PaymentPan and Create PaymentPlanTerms               *
* Created Date       : 08/10/2020                                                                   *
* Created By         : QBurst                                                                       *
* -----------------------------------------------------------------------------------------------   *
* VERSION     AUTHOR            DATE            COMMENTS                                            *
* 1.0         QBurst         09/02/2018      Initial Draft                                          *
****************************************************************************************************/
global with sharing class CreatePaymentPlanController {
    
    public string description {get;set;}
    public string planDesc {get;set;}
    public string planType {get;set;}
    public string isRounding {get;set;}
    public date effectiveFrom {get;set;}
    public date effectiveTo {get;set;}
    public Decimal totalPercent {get; set;}
    public string percentage {get; set;}
    public string count {get; set;}
    public Integer lineCount {get; set;}
    public Integer discount {get;set;}
    public static Integer termLines {get;set;}
    public string milestoneType {get;set;}
    public Payment_Plan__c plan {get;set;}
    public Payment_Terms__c planTerm {get;set;}
   // public static List<Payment_Terms__c> termsList {get;set;}
    public List<Payment_Terms__c> termsList {get;set;}
    public static string paymentTerms {get;set;}
    public static string translatedTexts {get;set;}
    public string planNames {get;set;}
    /*********************************************************************************************
    * @Description : Controller class.                                                           *
    * @Params      : void                                                                        *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public CreatePaymentPlanController() {
        system.debug('construtr');
        plan = new Payment_Plan__c();  
        paymentTerms = 'test';
        List<string> Names = new List<string>();
        termsList = new List < Payment_Terms__c > ();
        planNames = '';
        planDesc = '';
        planType = '';
        effectiveFrom = system.today();
        effectiveTo = system.today();
        totalPercent = 100;
        milestoneType = 'Immediate';
        lineCount = 5;
        List<Payment_Plan__c> paymentPlanList = new List<Payment_Plan__c>();
        paymentPlanList = [SELECT id, Name,Payment_Term_Description__c
                                     FROM Payment_Plan__c
                                    /* WHERE Parent_Payment_Plan__c = NULL AND Active__c = true
                                     AND Effective_To_calculated__c >= TODAY*/ 
                                     ORDER BY CreatedDate DESC
                                     LIMIT 20000]; 
        for(Payment_Plan__c plan :paymentPlanList){
            Names.add(plan.Payment_Term_Description__c);
        }
        planNames = JSON.serialize(Names);
        system.debug('planNames'+planNames);
    }
    
    /************************************************************************************************
    * @Description : method to Insert the  PaymentTerms                                             *
    * @Params      : void                                                                           *
    * @Return      : PageRefrence                                                                           *
    ************************************************************************************************/
    public PageReference insertPaymentDetails(){
        String planId='';
        List<Payment_Plan__c> planList= new List<Payment_Plan__c>();
        List<Payment_Terms__c> planTermsList= new List<Payment_Terms__c>();
        system.debug('Inserting Plan and Terms');
        system.debug('lineCount: ' + lineCount);
        system.debug('termsList: ' + termsList);
        plan.Effective_From__c = effectiveFrom;
        plan.Effective_To__c = effectiveTo;
        plan.Type__c = planType;
        plan.Payment_Term_Description__c = planDesc;
        plan.Discount_Percent__c = discount;
        plan.Rounding_Applicable__c = isRounding;
        planList.add(plan);
        Database.SaveResult[] srList = Database.insert(planList, false);
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                System.debug('Successfully inserted PaymentPlan. ID: ' + sr.getId());
                planId = sr.getId();
            }
            else{
                System.debug('Error Occured');
            }
        }
        List<string> textToTranslate = new List<string>();
        if(planId != ''){
            for(Payment_Terms__c terms : termsList){
                planTerm = new Payment_Terms__c();
                planTerm.Description__c = terms.Description__c;
                planTerm.Milestone_Event__c = terms.Milestone_Event__c;
                planTerm.Milestone_Event_Arabic__c = terms.Milestone_Event_Arabic__c;
                planTerm.Installment__c = terms.Installment__c;
                planTerm.Percent_Value__c = terms.Percent_Value__c;
                planTerm.Payment_Plan__c = planId;
                planTermsList.add(planTerm);
            }
            insert planTermsList;
        }
        system.debug(plan);
        system.debug(planTermsList);
        PageReference pr = new PageReference('/apex/PaymentPlanDetails');
        pr.setRedirect(true);
        return pr;
    }
    
    
    public void showReviewPage() {
        system.debug('termsList: ' + termsList);
    }
   
    /************************************************************************************************
    * @Description : method to create the  PaymentTerms                                             *
    * @Params      : void                                                                           *
    * @Return      : void                                                                           *
    ************************************************************************************************/
    
   public void createPaymentTerms() {
   // CreatePaymentPlanController.getTranslatedText('milestoneType');
    system.debug('entered');
    String effFrom = ApexPages.currentPage().getParameters().get('startDate');
    String effTo = ApexPages.currentPage().getParameters().get('endDate');
    count = ApexPages.currentPage().getParameters().get('countLines');
    String totPercent = ApexPages.currentPage().getParameters().get('percentage');
    planDesc = ApexPages.currentPage().getParameters().get('desc');
    planType = ApexPages.currentPage().getParameters().get('planType');
    milestoneType = ApexPages.currentPage().getParameters().get('milestoneType');
    isRounding = ApexPages.currentPage().getParameters().get('rounding');
    string discountPerc = ApexPages.currentPage().getParameters().get('discount');
    if(discountPerc != ''){
        discount = Integer.valueOf(discountPerc);   
    }    
    if(effFrom != null && effFrom != ''){
    effectiveFrom = Date.parse(effFrom);
    }
    if(effTo != null && effTo != ''){
    effectiveTo = Date.parse(effTo);
    }
    if(totPercent != null && totPercent != ''){
    totalPercent = Decimal.ValueOf(totPercent);
    }        
    lineCount = Integer.ValueOf(count);
    system.debug('effFrom: '+ effFrom);
    system.debug('count: '+ count);
    system.debug('totPercent: '+ totPercent);
    system.debug('lineCount: '+ lineCount);
    system.debug('effTo: '+ effTo);
    system.debug('planDesc: '+ planDesc);
    system.debug('planType: '+ planType);
    system.debug('milestoneType: '+ milestoneType);
    system.debug('Discount: '+ discountPerc);
    system.debug('isRounding: '+ isRounding);
    
    Integer installmentCount=1;
    Decimal adjustment = 0;
    Decimal percentage = (totalPercent / lineCount).setScale(2, System.RoundingMode.DOWN);
    adjustment = totalPercent - (percentage * lineCount);
    system.debug('percentage: '+ percentage);
    system.debug('lineCount: '+ lineCount);
    getTranslatedText(milestoneType);
    system.debug('ArabicText: '+ translatedTexts);
    for (Integer i = 0; i < lineCount; i++) {
        planTerm = new Payment_Terms__c();
        if (i == 0) {
            planTerm.Description__c = 'DEPOSIT';
            planTerm.Installment__c = 'DP';
            planTerm.Percent_Value__c = String.valueOf(percentage + adjustment);
        }
        else {
            planTerm.Percent_Value__c = String.valueOf(percentage);
            if(i == 1){
               planTerm.Description__c = '1st INSTALLMENT'; 
            }
            else{
                
                installmentCount=installmentCount+1;
                if(i == 2){
                     planTerm.Description__c = installmentCount+' nd' +' INSTALLMENT';
                }
                else if(i == 3){
                     planTerm.Description__c = installmentCount+' rd' +' INSTALLMENT';
                }
                else{
                     planTerm.Description__c = installmentCount+' th' +' INSTALLMENT';
                }
               
            }  
            planTerm.Installment__c = 'I00' + (i);
            if((i) / 10 > 0){
                planTerm.Installment__c = 'I0' + (i);
            }            
        }        
        planTerm.Milestone_Event__c = milestoneType;
        if(translatedTexts != null){
        planTerm.Milestone_Event_Arabic__c = translatedTexts;
        }
        planTerm.Start_Date__c = effectiveFrom;
        planTerm.End_Date__c = effectiveTo;
        system.debug('planTerm: '+ planTerm);
        termsList.add(planTerm);
        }
      // translatedTexts.clear();
       system.debug('termsList: ' + termsList);
       paymentTerms=JSON.serialize(termsList);
       system.debug('paymentTerms: ' + paymentTerms);
   }
    
   /************************************************************************************************
    * @Description : method to update the  PaymentTerms list                                        *
    * @Params      : void                                                                           *
    * @Return      : void                                                                           *
    ************************************************************************************************/ 
   public void UpdatePaymentTerms() {
        system.debug('UpdatePaymentTerms termsList: ' + termsList);
        translatedTexts = '';       
        String effFrom = ApexPages.currentPage().getParameters().get('startDate');
        String effTo = ApexPages.currentPage().getParameters().get('endDate');
        String totPercent = ApexPages.currentPage().getParameters().get('percent');     
        String mileStoneType = ApexPages.currentPage().getParameters().get('mileStone');
        String event = ApexPages.currentPage().getParameters().get('event');        
        String paymentDate = ApexPages.currentPage().getParameters().get('paymentDate');       
        String eventdate = ApexPages.currentPage().getParameters().get('eventdate');        
        String paymentInDays = ApexPages.currentPage().getParameters().get('paymentInDays');
        String description = ApexPages.currentPage().getParameters().get('description');
        system.debug('paymentInDays :'+paymentInDays);
        system.debug('percent :'+totPercent);
        system.debug('mileStone :'+mileStoneType);
        system.debug('event :'+event);
        system.debug('paymentDate :'+paymentDate);
        system.debug('eventdate :'+eventdate);
       system.debug('effFrom :'+effFrom);
       system.debug('description :'+description);
       
       // system.debug('Desc :'+desc);
       List<Payment_Terms__c> UpdatedList = new List<Payment_Terms__c>();
       if(effFrom != null && effFrom != ''){
            effectiveFrom = Date.parse(effFrom);
           system.debug('effectiveFrom'+effectiveFrom);
        }
        if(effTo != null && effTo != ''){
            effectiveTo = Date.parse(effTo);
        }
      for(Payment_Terms__c terms :termsList){
           if(description == terms.Description__c)
           {
               if(mileStoneType == 'On Completion of'){
                   terms.Milestone_Event__c = 'On Completion of '+event;                   
               }
               else if(mileStoneType == 'On or Before'){
                    terms.Milestone_Event__c = 'On or Before '+paymentDate;
                    terms.Payment_Date__c = Date.parse(paymentDate);
               }
               else if(mileStoneType == 'Within'){
                    terms.Milestone_Event__c ='Within '+paymentInDays+' days of '+eventDate;
                    terms.Payment_Date__c = Date.parse(paymentDate);
               }
               else if(mileStoneType == 'Months Anniversary'){
                    terms.Milestone_Event__c ='Months Anniversary';
               }else{
                    terms.Milestone_Event__c ='Immediate';
               }
               string milestoneArabic = '';
               if(mileStoneType == 'Within'){
                   if(eventDate == 'SaleDate'){
                       milestoneArabic ='Within X days of Sale Date';
                   }if(eventDate == 'AgreementDate'){
                       milestoneArabic ='Within X days of Agreement Date'; 
                   } 
                        getTranslatedText(milestoneArabic); 
                        terms.Milestone_Event_Arabic__c = translatedTexts.replace('X',paymentInDays);
                        system.debug(translatedTexts.replace('X',paymentInDays));
                        system.debug('terms.Milestone_Event_Arabic__c'+terms.Milestone_Event_Arabic__c);
                   
                 
               }
               else {
                getTranslatedText(terms.Milestone_Event__c);
                terms.Milestone_Event_Arabic__c = translatedTexts;
               }
               terms.Percent_Value__c=totPercent;
               terms.Start_Date__c=effectiveFrom;
               terms.End_Date__c=effectiveTo;
              
           }
           system.debug('term :'+terms);
          UpdatedList.add(terms);
       } 
            
        system.debug('UpdatedList' +UpdatedList);
        paymentTerms=JSON.serialize(UpdatedList);
    }
    
    
    public void UpdatePaymentTermsList(){
        system.debug('termsList: ' + termsList);
        String effFrom = ApexPages.currentPage().getParameters().get('startDate');
        String effTo = ApexPages.currentPage().getParameters().get('endDate');
        String totPercent = ApexPages.currentPage().getParameters().get('percent');     
        String mileStoneType = ApexPages.currentPage().getParameters().get('mileStone');
        String event = ApexPages.currentPage().getParameters().get('event');        
        String paymentDate = ApexPages.currentPage().getParameters().get('paymentDate');       
        String eventdate = ApexPages.currentPage().getParameters().get('eventdate');        
        String paymentInDays = ApexPages.currentPage().getParameters().get('paymentInDays');
        String description = ApexPages.currentPage().getParameters().get('description');
        system.debug('paymentInDays :'+paymentInDays);
        system.debug('percent :'+totPercent);
        system.debug('mileStone :'+mileStoneType);
        system.debug('event :'+event);
        system.debug('paymentDate :'+paymentDate);
        system.debug('eventdate :'+eventdate);
        
        if(effFrom != null && effFrom != ''){
            effectiveFrom = Date.parse(effFrom);
           system.debug('effectiveFrom'+effectiveFrom);
        }
        if(effTo != null && effTo != ''){
            effectiveTo = Date.parse(effTo);
        }
        Integer installmentCount=1;
        planTerm = new Payment_Terms__c();
        if(termsList.size() == 0){
            planTerm.Description__c = 'DEPOSIT';
            planTerm.Installment__c = 'DP';
        }
        else{
            if(termsList.size() == 1){
           
            planTerm.Description__c = installmentCount+' st ' +' INSTALLMENT';
        }
        else if(termsList.size() == 2)
        {   installmentCount++;
            planTerm.Description__c = installmentCount+' nd' +' INSTALLMENT';
        }else if(termsList.size() == 3){
            installmentCount=3;
            planTerm.Description__c = installmentCount+' rd' +' INSTALLMENT';
        }
        else{
            installmentCount = termsList.size();
            planTerm.Description__c = installmentCount+' th' +' INSTALLMENT';
        }
        planTerm.Installment__c = 'I00' + (termsList.size());
        if((termsList.size()-1) / 10 > 0){
                planTerm.Installment__c = 'I0' + (termsList.size()-1);
            }
        }
        planTerm.Percent_Value__c = String.valueOf(totPercent);
        planTerm.Start_Date__c=effectiveFrom;
        planTerm.End_Date__c=effectiveTo;
        
        if(mileStoneType == 'On Completion of'){
                   planTerm.Milestone_Event__c = 'On Completion of '+event;
               }
               else if(mileStoneType == 'On or Before'){
                    planTerm.Milestone_Event__c = 'On or Before ';
                    planTerm.Payment_Date__c = Date.parse(paymentDate);
               }
               else if(mileStoneType == 'Within'){
                    planTerm.Milestone_Event__c = 'Within ' +'X'+' Days of '+eventdate ;
                    planTerm.Payment_Date__c = Date.parse(paymentDate);
               }else if(mileStoneType == 'Immediate'){
                    planTerm.Milestone_Event__c = 'Immediate';
                   
               }
        string eventArabic = planTerm.Milestone_Event__c;
        getTranslatedText(eventArabic);
        if(translatedTexts != null){
            planTerm.Milestone_Event_Arabic__c = translatedTexts;
        }
        System.debug('planTerm : '+planTerm);
        termsList.add(planTerm);
        System.debug('termsList : '+termsList);
        paymentTerms=JSON.serialize(termsList);
    }
    
    /************************************************************************************************
    * @Description : method to Delete the PaymentTerms  from list                                            *
    * @Params      : void                                                                           *
    * @Return      : void                                                                           *
    ************************************************************************************************/ 
    public void DeletePaymentTerms(){
        system.debug('DeletePaymentTerms: ' + termsList);
        String description = ApexPages.currentPage().getParameters().get('description');
        system.debug('installment: ' + description);
        List < Payment_Terms__c > UpdatedList = new List < Payment_Terms__c > ();
        List < Payment_Terms__c > UpdatedListNew = new List < Payment_Terms__c > ();
        Payment_Terms__c terms;
        Integer pos=0;
        for(integer i=0;i<=termsList.size();i++)
            {
                if(termsList.get(i).Description__c == description)
                {
                    pos = i;
                    break;
                }
                
            }
        System.debug('Pos'+pos);
        Integer j=0;
        for( Payment_Terms__c term : termsList)
        {
            if(j==pos){
                break;
                
            }else{
                UpdatedList.add(term);
            }
            j++;
            
        }
            System.debug('UpdatedList'+UpdatedList);
            for (integer i = pos; i < termsList.size()-1; i++)
            {
                System.debug('termList + i'+i);
                terms = new Payment_Terms__c();
                terms.Description__c = termsList.get(i).Description__c;
                terms.Installment__c = termsList.get(i).Installment__c;
                terms.Milestone_Event__c = termsList.get(i+1).Milestone_Event__c;
                terms.Start_Date__c = termsList.get(i+1).Start_Date__c;
                terms.Percent_Value__c = termsList.get(i+1).Percent_Value__c;
                string milestoneArabic = terms.Milestone_Event__c;
                getTranslatedText(milestoneArabic);
                terms.Milestone_Event_Arabic__c = translatedTexts;
                system.debug('terms: '+terms);
                //termsList.get(i + 1).Description__c = termsList.get(i).Description__c;
               // termsList.get(i + 1).Installment__c = termsList.get(i).Installment__c;
                 UpdatedList.add(terms);
            }
        for (Payment_Terms__c p: UpdatedList) {
            system.debug(p);
        }
        system.debug('UpdatedList: ' + UpdatedList);
        termsList = UpdatedList;
        paymentTerms = JSON.serialize(UpdatedList);
    }
    
   //@future(callout=false)
   public static void getTranslatedText(String texts) { 
       system.debug('getTranslatedText->texts'+texts);
       translatedTexts = '';
       Map<String, Milestone_Arabic__c> arabicmap = Milestone_Arabic__c.getAll();
       system.debug('arabicmap'+arabicmap);
       List<Milestone_Arabic__c> mcs = Milestone_Arabic__c.getall().values();
       system.debug('mcs'+mcs);
       if(texts == 'Immediate'){
         Milestone_Arabic__c myCS1 = Milestone_Arabic__c.getValues(texts);
         translatedTexts = myCS1.Arabic__c;
       }else if(texts == 'On Completion'){
         Milestone_Arabic__c myCS1 = Milestone_Arabic__c.getValues(texts);
         translatedTexts = myCS1.Arabic__c;
       }else if(texts == 'On or Before'){
           system.debug('on or bfr'+texts);
         Milestone_Arabic__c myCS1 = Milestone_Arabic__c.getValues(texts);
         translatedTexts = myCS1.Arabic__c;
       }else if(texts == 'Within X days of Sale Date'){
         Milestone_Arabic__c myCS1 = Milestone_Arabic__c.getValues(texts);
         translatedTexts = myCS1.Arabic__c;
       }else if(texts == 'Within X days of Agreement Date'){
         Milestone_Arabic__c myCS1 = Milestone_Arabic__c.getValues(texts);
         translatedTexts = myCS1.Arabic__c;
       }
      // Milestone_Arabic__c myCS1 = Milestone_Arabic__c.getValues('texts');
      // String myCCVal = myCS1.Arabic__c;
       system.debug('myCCVal'+translatedTexts);
       /*   
        HTTPResponse resp = null; 
        translatedTexts = new List<String>();
        if (texts != '') { 
            string test = texts;
            string newtest = test.replace(' ','%20');
            system.debug(newtest);            
            HttpRequest req = new HttpRequest();
            Http http = new Http(); 
            string url = 'https://www.googleapis.com/language/translate/v2?key='
                    + 'AIzaSyDRGGayaFVzowZx5SR60n931-xu13Yp-is'
                    + '&source=en&target=ar&q=' + newtest;
 
            req.setMethod('GET');
            req.setEndPoint(url);
            req.setTimeout(30000);
            resp = http.send(req);
            system.debug('response'+resp.getBody());
            if (resp != null) {
                String jsonResults = ''; 
                jsonResults = resp.getBody(); 
                JSONParser parser = JSON.createParser(jsonResults);while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) 
                && (parser.getText() == 'translatedText')) {
                    parser.nextToken();
                    translatedTexts.add(parser.getText());
                }
            } 
         }
        } else{
            System.debug(LoggingLevel.WARN, 'The text intended to translate is empty');
        }
    //return translatedTexts;
        */
    }
}