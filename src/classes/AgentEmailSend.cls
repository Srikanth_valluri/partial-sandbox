public class AgentEmailSend{
   
    // Method to send Email
    public static void SendMail(Set<ID> SrIds){
    try{
        List<string> SendTo=new  List<string>();
        List<Booking_Unit__c> Bulist=new  List<Booking_Unit__c>();
        List<Messaging.SingleEmailMessage> mails=new  List<Messaging.SingleEmailMessage>();
        
        Bulist=[SELECT id,Primary_Buyer_s_Email__c,Token_Amount__c, Primary_Buyer_s_Name__c, Unit_Details__c,NI_Payment_URL__c , Booking__r.Deal_SR__c FROM Booking_Unit__c WHERE Booking__r.Deal_SR__c = : SrIds ];
        if(!Bulist.isEmpty()){
            for(Booking_Unit__c bu:Bulist){
            	System.debug('...inside mail class...');
                if(string.isNotBlank(bu.Primary_Buyer_s_Email__c))
                SendTo.add(bu.Primary_Buyer_s_Email__c);
                System.debug('...SendTo...'+SendTo);
               // SendTo.add('pratiksha.n@accely.com');
                Messaging.SingleEmailMessage mail=new  Messaging.SingleEmailMessage();
                mail.setToAddresses(SendTo);
                mail.setSenderDisplayName('Damac Property ');
                mail.setReplyTo('noreply@Damacgroup.com');
                mail.setSubject('Payment pending for -'+bu.Unit_Details__c);
                string body='Dear ' +bu.Primary_Buyer_s_Name__c+'<br/><br/>';
                body+='Congratulations on choosing '+bu.Unit_Details__c+'  <br/><br/>';
                body+= ' Please proceed to make a token payment of AED - '+bu.Token_Amount__c+'<br/> using the <a href="'+bu.NI_Payment_URL__c +'">payment link </a> to complete your booking process.<br/><br/> Regards ,<br/> Damac Sales ';
                mail.setHtmlBody(body);
                mails.add(mail);  
            }
        }
        Messaging.sendEmail(mails);
        
    }
      
    catch(Exception Ex){
        System.debug('Error Message in AgentEmailSend---- '+Ex.getMessage());
        System.debug('Error Message @Line Number---- '+Ex.getLineNumber());
    }
        
    }
}