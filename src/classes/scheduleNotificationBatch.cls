global class scheduleNotificationBatch  implements Schedulable {
   global void execute(SchedulableContext SC) {
      NotificationBatch  notifyobj = new NotificationBatch(); 
      database.executebatch(notifyobj);
   }
}