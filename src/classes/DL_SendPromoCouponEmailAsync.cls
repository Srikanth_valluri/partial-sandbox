/*
Class Name : DL_SendPromoCouponEmailAsync
Description : To send email notification asyncronously to account owner on profile completion

========================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
--------------------------------------------------------------------------------------------------------
1.0     |   12-11-2020      | Subin Antony        | Initial Draft
1.0     |   27-01-2021      | Subin Antony        | Modified PTC_BASE_URL constants (Azure Bug 3004)
*******************************************************************************************************/
global class DL_SendPromoCouponEmailAsync implements Queueable, Database.AllowsCallouts {
    public static final String PTC_BASE_URL_STG = 'https://ptctest.damacgroup.com/hellodamac';
    public static final String PTC_BASE_URL_PRD = 'https://ptc.damacgroup.com/hellodamac-prod';
    public static final String PTC_PROMO_END_PNT = '/api/v1/customers/';
    public static final String PTC_API_TOKEN_STG = 'c13d9a4e2be67d853d7428f5a9a952d6';
    public static final String PTC_API_TOKEN_PRD = 'c13d9a4e2be67d853d7428f5a9a952d6';
    public static Boolean isSandbox;
    static {
        isSandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }
    
    private String accountId;
    private String contactEmail;
    
    global DL_SendPromoCouponEmailAsync(String accountId, String contactEmail) {
        this.accountId = accountId;
        this.contactEmail = contactEmail;
    }
    
    public void execute(QueueableContext context) {
        sendPromoCouponEmailAndUpdateAccount(accountId, contactEmail);
    }
    
    public static void sendPromoCouponEmailAndUpdateAccount(String accountId, String accountEmail) {
        // Check if EmailMessage(Activity) for promo coupon email sent is existing for this Account !!!!! IMPORTANT !!!!
        List<EmailMessage> promoCaptureEmailLogs = [SELECT id, subject, status, RelatedToId, Account__c FROM EmailMessage 
                                                    WHERE Account__c = :accountId 
                                                    AND subject = 'EMAILED PROMOTION COUPON FOR MOBILE APP PROFILE COMPLETION'];
        if(NULL != promoCaptureEmailLogs && promoCaptureEmailLogs.size() > 0) {
            System.debug('!!!!!! ----------------------------------------------: PROMO EMAIL FOR PROFILE COMPLETION ALREADY SENT ------!!!!!');
            return;
        }
        
        System.debug('!!!!!! ----------------------------------------------: BEFORE FETCHING PROMOCODE FROM PTC ------!!!!!');
        HttpRequest getPromoCouponCodeRequest = new HttpRequest();
        String baseUrl = IsSandbox ? PTC_BASE_URL_STG : PTC_BASE_URL_PRD;
        String apiToken = IsSandbox ? PTC_API_TOKEN_STG : PTC_API_TOKEN_PRD;
        getPromoCouponCodeRequest.setEndpoint(baseUrl + PTC_PROMO_END_PNT + accountId + '/offers/1');
        getPromoCouponCodeRequest.setMethod('POST');
        getPromoCouponCodeRequest.setheader('Content-Type','application/json');
        getPromoCouponCodeRequest.setHeader('API-TOKEN', apiToken);
        getPromoCouponCodeRequest.setbody('{}');
        getPromoCouponCodeRequest.setTimeout(120000);
        
        HttpResponse getPromoCouponCodeResponse;
        try{
            if(!Test.isRunningTest()) {
                getPromoCouponCodeResponse = new Http().send(getPromoCouponCodeRequest);
            }
            else {
                // for test excecution context
                getPromoCouponCodeResponse = new HttpResponse();
                getPromoCouponCodeResponse.setStatus('OK');
                getPromoCouponCodeResponse.setStatusCode(200);
                getPromoCouponCodeResponse.setBody('{"data":{"redeem_code":"BREAKFAST#333"},"meta_data":{"title":"Successful","message":"Success","status_code":1,"developer_message":null}}');
            }
                
            system.debug('PTC_CALLOUT_Response : ............................................ ' + getPromoCouponCodeResponse);
            system.debug('PTC_CALLOUT_ResponseStatus : ............................................ ' + getPromoCouponCodeResponse.getStatusCode());
            system.debug('PTC_CALLOUT_ResponseBody : ............................................ ' + getPromoCouponCodeResponse.getBody());
        }
        catch(Exception ex){
            system.debug('ERROR WHILE GENERATING PROMO COUPON CODE FOR PROFILE COMPLETION');
            system.debug(ex.getMessage());
            system.debug(ex.getStackTraceString());
            return;
        }
        
        if(getPromoCouponCodeResponse.getStatusCode() == 200 && 
        NULL != getPromoCouponCodeResponse.getBody() && 
        !(getPromoCouponCodeResponse.getBody().equals('')) ){
            String promoCode = '';
            ResponseWrapper promoResponseWrapper;
            try {
                promoResponseWrapper = (ResponseWrapper)JSON.deserialize(getPromoCouponCodeResponse.getBody(), ResponseWrapper.class);
                if(NULL != promoResponseWrapper && NULL != promoResponseWrapper.data && 
                String.isNotBlank(promoResponseWrapper.data.redeem_code)) {
                    promoCode = promoResponseWrapper.data.redeem_code;
                }
                else {
                	system.debug('ERROR IN GENERATED PROMO COUPON CODE RESPONSE FROM MIDDLEWARE: ' + getPromoCouponCodeResponse.getBody());
                    return;
                }
            }
            catch(Exception ex) {
                system.debug('ERROR WHILE PARSING PROMO COUPON CODE FROM MIDDLEWARE RESPOSE: ' + getPromoCouponCodeResponse.getBody());
                system.debug(ex.getMessage());
                system.debug(ex.getStackTraceString());
                return;
            }
            
            /* Sending promo email for profile data completion */
            OrgWideEmailAddress orgWideAddressHelloDamac;
            EmailTemplate promoEmailTemplate;
            try{
                orgWideAddressHelloDamac = [SELECT  id, Address, DisplayName 
                                            FROM OrgWideEmailAddress 
                                            WHERE id = :Label.CommunityPortalOrgWideAddressId LIMIT 1];
                promoEmailTemplate = [SELECT id, name, developerName, isActive, subject, body, HtmlValue 
                                      FROM EmailTemplate WHERE developerName = 'Promotion_coupon_Email_for_DL' LIMIT 1];
            }
            catch(Exception ex){
                system.debug(ex.getMessage());
                system.debug(ex.getStackTraceString());
                return;
            }
            
            System.debug('!!!!!! ----------------------------------------------: IN METHOD TO SEND PROMO EMAIL FOR PROFILE COMPLETION ------!!!!!');
            
            String fromAddress = orgWideAddressHelloDamac.Address;
            String fromName = orgWideAddressHelloDamac.DisplayName;
            
            String msgTemplate;
            if(NULL != promoEmailTemplate && String.isNotBlank(promoEmailTemplate.HtmlValue)) {
                msgTemplate = promoEmailTemplate.HtmlValue;
            } else {
                msgTemplate = 'Thank you for downloading the DAMAC Living app! <br /> ' + 
                    '{PROMO_MESSAGE}! <br /> Your complimentary breakfast at the Stage restaurant is ready! <br /> ' +
                    'Your voucher code : {PROMO_CODE} <br /> ' + 
                    'OTHER REWARDS YOU CAN EXPECT: BUY 1 GET 1 BRUNCH OFFERS on Friday or Saturday at 20% OFF ACROSS DINING & SPA <br /> ' + 
                    'To avail this offer, present your voucher code at the venue <br /> ' + 
                    '*Terms & conditions apply. <br /> Booking in advance is recommended.  <br /> ' + 
                    'Offer not valid in conjunction with any other discount or promotion.  <br /> ' + 
                    'Breakfast offer is for one-time consumption only. <br />  <br /> DAMACPROPERTIES.COM';
            }
            msgTemplate = msgTemplate.replace('{PROMO_MESSAGE}', 'Thank you for completing your user profile in DAMAC Living app');
            String mergedMessge = msgTemplate.replace('{PROMO_CODE}', promoCode);
            
            String toAddress = accountEmail; /* comma seperated list of email addresses */
            String ccAddress = ''; /* comma seperated list of email addresses */
            String bccAddress = ''; /* comma seperated list of email addresses */
            String toName, ccName, bccName, replyToName; /* not required */
            String contentType = 'text/html'; /* 'text/plain' */
            
            String subject = 'Greetings from DAMAC: Complementary coupon for completing your user profile in DAMAC Living App';
            String contentValue = mergedMessge; /* mergedMessageContent */
            
            String replyToAddress = ''; /* not required */
            String sendGridTemplateId = ''; /* not required */
            String substitutions = ''; /* not required */
            List<Attachment> attachments = new List<Attachment>();
            
            SendGridEmailService.SendGridResponse sendEmailResponse;
            try{
                if(Test.isRunningTest()) {
                    sendEmailResponse = new SendGridEmailService.SendGridResponse();
                    sendEmailResponse.ResponseStatus = 'Accepted';
                    sendEmailResponse.messageId = 'TEST_VSaubDIpKIFqeAw';
                } 
                else {
                    sendEmailResponse = SendGridEmailService.sendEmailService(
                        toAddress, toName, ccAddress, ccName, bccAddress, bccName, 
                        subject, substitutions, fromAddress, fromName, replyToAddress, replyToName, 
                        contentType, contentValue, sendGridTemplateId, attachments);
                }
                
                system.debug('!!!!!!-------------------: SEND_GRID Response : ' + sendEmailResponse);
            }
            catch(Exception ex){
                system.debug(ex.getMessage());
                system.debug(ex.getStackTraceString());
                system.debug('ERROR: Failed in SendGrid service to send email.');
                return;
            }
            
            /* Updating account with promocode not required. Alternatively capture email in 'EmailMessage' activity under the Account concerned.
             * Create activity log for email sent event: insert EmailMessage */
            String promoCaptureSubject = 'EMAILED PROMOTION COUPON FOR MOBILE APP PROFILE COMPLETION';
            createEmailSentLog(accountId, fromAddress, toAddress, ccAddress, bccAddress, 
                               promoCaptureSubject, contentValue, sendEmailResponse.messageId);
        } /* if(getPromoCouponCodeResponse.getStatusCode() == 200 && ...) */
    } /* sendPromoCouponEmailAndUpdateAccount(accountId, accountEmail) */
    
    private static void createEmailSentLog (String accountId, String fromAddress, String toAddress, 
    String ccAddress, String bccAddress, String subject, String messageBody, String responsMsgId){
        EmailMessage emailLog = new EmailMessage();
        emailLog.Subject = subject;
        emailLog.MessageDate = System.Now();
        emailLog.Status = '3';
        emailLog.RelatedToId = accountId; /* no Case here */
        emailLog.ToAddress = toAddress;
        emailLog.FromAddress = fromAddress;
        emailLog.TextBody = messageBody;
        emailLog.CcAddress = ccAddress;
        emailLog.BccAddress = bccAddress;
        emailLog.Sent_By_Sendgrid__c = true;
        emailLog.SentGrid_MessageId__c = responsMsgId;
        emailLog.Booking_Unit__c = null;
        emailLog.Account__c = accountId;
        
        try{
            insert emailLog;
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
            system.debug(ex.getStackTraceString());
        }
    }

    /* Wrapper classes for fetching callout reponse from PTC middleware */
    public class ResponseWrapper {
        public cls_meta_data meta_data;
        public cls_data data;
    }

    public class cls_meta_data {
        public Integer status_code;
        public String message;
        public String title;
        public String developer_message;   
    }
    
    public class cls_data {
        public String redeem_code;
    }
}