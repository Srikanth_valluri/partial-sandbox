@isTest 
private class PaymentAPIController_TC {

    static testMethod void PaymentAPIControllerMethod() {
       // NSIBPM__Service_Request__c sr = InitializeSRDataTest.getSerReq('Deal',true,null);
        List<NSIBPM__Service_Request__c> sr= TestDataFactory.createTestServiceRequestRecords(new List<NSIBPM__Service_Request__c>{new NSIBPM__Service_Request__c()});
        //insert sr;
        
        Booking__c book = new booking__c();
        book.Deal_SR__c = sr[0].id;
        book.Booking_Channel__c = 'Web';
        insert book;
        
         Inquiry__c inquiryRecord = new Inquiry__c();
        inquiryRecord.By_Pass_Validation__c = true;
        inquiryRecord.Party_ID__c = '12345';
        inquiryRecord.Title__c = 'MR.';
        inquiryRecord.Title_Arabic__c ='MR.';
        inquiryRecord.First_Name__c = 'Test';
        inquiryRecord.First_Name_Arabic__c ='Test';
        inquiryRecord.Last_Name__c ='Test';
        inquiryRecord.Last_Name_Arabic__c = 'Test';
        insert inquiryRecord;
        
         buyer__c b = new buyer__c();
        b.Buyer_Type__c =  'Individual';
        b.Address_Line_1__c =  'Ad1';
        b.Country__c =  'United Arab Emirates';
        b.City__c = 'Dubai' ;
        b.Inquiry__c = inquiryRecord.Id ;
        b.Date_of_Birth__c = string.valueof(system.today().addyears(-30)) ;
        b.Email__c = 'test@test.com';
        b.First_Name__c = 'firstname' ;
        b.Last_Name__c =  'lastname';
        b.Nationality__c = 'Indian' ;
        b.Passport_Expiry_Date__c = string.valueof(system.today().addyears(20)) ;
        b.Passport_Number__c = 'J0565556' ;
        b.Phone__c = '569098767' ;
        b.Phone_Country_Code__c = 'United Arab Emirates: 00971' ;
        b.Place_of_Issue__c =  'India';
        b.Title__c = 'Mr';
        b.booking__c = book.id;
        b.Primary_Buyer__c = true;
        insert b;
        PageReference pageRef = Page.CC_Payments;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',book.Deal_SR__c);
        PaymentAPIController Payment = new PaymentAPIController ();
        payment.termsAndConditions = true;
        Payment.generatePayment();
        PageReference pp = Payment.cancel();
    }

    static testMethod void PaymentAPIControllerMethod1() {

         Inquiry__c inquiryRecord = new Inquiry__c();
        inquiryRecord.By_Pass_Validation__c = true;
        inquiryRecord.Party_ID__c = '12345';
        inquiryRecord.Title__c = 'MR.';
        inquiryRecord.Title_Arabic__c ='MR.';
        inquiryRecord.First_Name__c = 'Test';
        inquiryRecord.First_Name_Arabic__c ='Test';
        inquiryRecord.Last_Name__c ='Test';
        inquiryRecord.Last_Name_Arabic__c = 'Test';
        insert inquiryRecord;

        Travel_Details__c td = new Travel_Details__c();
        td.Inquiry__c = inquiryRecord.Id;
        insert td;

        PageReference pageRef = Page.CC_Payments;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('detailsId', td.Id);
        PaymentAPIController Payment = new PaymentAPIController ();
        payment.termsAndConditions = true;
        Payment.generatePayment();
        PageReference pp = Payment.cancel();
    }
}