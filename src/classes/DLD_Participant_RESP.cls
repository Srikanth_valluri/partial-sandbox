public class DLD_Participant_RESP {

    public class ParticipantDocuments {
        public String docName;
        public String docType;
        public List<Docs> docs;
        public Boolean isRequired;
        public Integer maxPages;
        public Integer maxSize;
    }

    public class PersonDetails {
        public String dob;
        public String emiratesId;
        public String familyName;
        public String middleName;
        public String nationality;
        public String passport;
        public String uaeIdNumber;
    }

    public AuthorityDetails authorityDetails;
    public String creatorMsp;
    public String docType;
    public String email;
    public String ownerMSP;
    public List<ParticipantDocuments> participantDocuments;
    public Integer participantID;
    public String participantNameAr;
    public String participantNameEn;
    public String participantNumber;
    public String participantStatus;
    public Integer participantType;
    public String partkey;
    public PersonDetails personDetails;
    public String phone;

    public class AuthorityDetails {
        public String activity;
        public String issuingAuthority;
        public String licenseExpiryDate;
        public String licenseIssueDate;
        public String licenseNumber;
    }

    public class Docs {
        public String docId;
        public String hash;
    }

    
    public static DLD_Participant_RESP parse(String json) {
        return (DLD_Participant_RESP) System.JSON.deserialize(json, DLD_Participant_RESP.class);
    }
}