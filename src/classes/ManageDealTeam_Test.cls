/*
* Test Class for ManageDealTeam.
*/
@isTest
private class ManageDealTeam_Test {
    @testSetup static void setupData() {
        try{
            list<string> rolenames = new List<string>{'PC','HOS','DOS'};
                Map<string,UserRole> mpRoles = InitializeSRDataTest.createRoles(rolenames);
            
            set<string> stProfileNames = new set<string>{'Property Consultant','Director of Sales','Head of Sales'};
                Map<string,profile> mpProfile = InitializeSRDataTest.getprofiles(stProfileNames);
            
            Map<id,integer> mpprofileidUsercount = new Map<Id,integer>();
            Map<Id,id> mpProfileIDRoleID = new Map<Id,Id>();
            for(profile p : mpprofile.values()){
                mpprofileidUsercount.put(p.id,2);
                if(p.name == 'Property Consultant')
                    mpProfileIDRoleID.put(p.id,mpRoles.get('PC').id);
                if(p.name == 'Director of Sales')
                    mpProfileIDRoleID.put(p.id,mpRoles.get('DOS').id);
                if(p.name == 'Head of Sales')
                    mpProfileIDRoleID.put(p.id,mpRoles.get('HOS').id);
            }
            List<user> lstUsers = InitializeSRDataTest.createInternalUser(mpprofileidUsercount);
            
            for(user u : lstUsers){
                system.debug('--Username>'+u.username);
                u.UserRoleId = mpProfileIDRoleID.get(u.profileid);
            }
            insert lstUsers;
        }
        catch(exception ex){
            system.debug('---> exception '+ex.getmessage());
        }
        
    } 
    
    //change agent
    @isTest static void test_method_0() {
        Test.startTest();
        set<string> stProfileNames = new set<string>{'Property Consultant','Director of Sales','Head of Sales'};
            Map<string,profile> mpProfile = InitializeSRDataTest.getprofiles(stProfileNames);
        
        Map<id,integer> mpprofileidUsercount = new Map<Id,integer>();
        Map<Id,id> mpProfileIDRoleID = new Map<Id,Id>();
        for(profile p : mpprofile.values()){
            mpprofileidUsercount.put(p.id,2);
        }
        Map<string,List<User>> mpUsers = new Map<string,List<User>>();
        for(user u : [select id,name,userrole.name,CompanyName from user WHERE ProfileId IN: mpprofileidUsercount.keySet()]){
            if(mpUsers.containskey(u.userrole.name)){
                List<User> lstu = mpUsers.get(u.userrole.name);
                lstu.add(u);
                mpUsers.put(u.userrole.name,lstu);
            }
            else{
                List<User> lstu = new List<User>();
                lstu.add(u);
                mpUsers.put(u.userrole.name,lstu);
            }
        }
        //pratiksha
        List<NSIBPM__SR_Template__c> SRTemplateList =  InitialiseTestData.createTestTemplateRecords(new List<NSIBPM__SR_Template__c>{new NSIBPM__SR_Template__c()});
        
        Id RecordTypeIdAGENT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        Account a = new Account();
        a.recordtypeid=RecordTypeIdAGENT;
        a.Name = 'Test Account';
        a.Agency_Short_Name__c = 'testShrName';
        insert a;  
        
        Id RecordTypeIdContact = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        
        NSIBPM__Service_Request__c sr =  InitializeSRDataTest.getSerReq('Deal',false,null);
        sr.recordtypeid=RecordTypeIdContact;
        sr.NSIBPM__SR_Template__c = SRTemplateList[0].Id;
        sr.Agency__c = a.id;
        sr.ID_Type__c = 'Passport';
        insert sr;
        
        List<NSIBPM__Service_Request__c> lstSR = new List<NSIBPM__Service_Request__c>();
        lstSR.add(InitializeSRDataTest.getSerReq('Deal',true,null));
        lstSR.add(InitializeSRDataTest.getSerReq('Deal',true,null));
        lstSR.add(InitializeSRDataTest.getSerReq('Deal',true,null));
        
        lstSR[0].Agent_Name__c = mpUsers.get('PC')[0].id;
        lstSR[0].Agency__c = a.id;
        lstSR[0].Agency_Type__c = 'Corporate';
        
        lstSR[1].Select_User_1__c = mpUsers.get('PC')[0].id;
        lstSR[1].Agency__c = a.id;
        lstSR[1].Select_User_2__c = mpUsers.get('HOS')[1].id;
        lstSR[1].Change_Type__c = 'HOS';
        
        
        lstSR[2].Select_User_1__c = mpUsers.get('PC')[0].id;
        lstSR[2].Agency__c = a.id;
        lstSR[2].Select_User_2__c = mpUsers.get('PC')[1].id;
        lstSR[2].Change_Type__c = 'Change';
        
        insert lstSR;
        
        Deal_Team__c dt = InitializeSRDataTest.createDealTeam(lstSR[2].NSIBPM__Parent_SR__c,mpUsers.get('DOS')[0].id,mpUsers.get('HOS')[0].id,mpUsers.get('PC')[0].id);
        insert dt;
        
        List<NSIBPM__Step__c> lststeps = new List<NSIBPM__Step__c>();
        lststeps.add(InitializeSRDataTest.createStep(sr.id,null,null));
        lststeps.add(InitializeSRDataTest.createStep(sr.id,null,null));
        lststeps.add(InitializeSRDataTest.createStep(sr.id,null,null));
        
        insert lststeps;
        
        ManageDealTeam objmdt = new ManageDealTeam();
        objmdt.EvaluateCustomCode(lstSR[0],lststeps[0]);
        objmdt.EvaluateCustomCode(lstSR[1],lststeps[1]);
        objmdt.EvaluateCustomCode(lstSR[2],lststeps[2]);
        objmdt.EvaluateCustomCode(null,null);
        Test.stopTest();
    }
    
    //change agent
    @isTest static void test_method_1() {
        Test.startTest();
        {
            set<string> stProfileNames = new set<string>{'Property Consultant','Director of Sales','Head of Sales'};
                Map<string,profile> mpProfile = InitializeSRDataTest.getprofiles(stProfileNames);
            
            Map<id,integer> mpprofileidUsercount = new Map<Id,integer>();
            Map<Id,id> mpProfileIDRoleID = new Map<Id,Id>();
            for(profile p : mpprofile.values()){
                mpprofileidUsercount.put(p.id,2);
            }
            
            List<NSIBPM__SR_Template__c> SRTemplateList =  InitialiseTestData.createTestTemplateRecords(new List<NSIBPM__SR_Template__c>{new NSIBPM__SR_Template__c()});
            
            Id RecordTypeIdAGENT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
            Account a = new Account();
            a.recordtypeid=RecordTypeIdAGENT;
            a.Name = 'Test Account';
            a.Agency_Short_Name__c = 'testShrName';
            insert a;  
            
            Id RecordTypeIdContact = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
            
            NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
            SR.recordtypeid=RecordTypeIdContact;
            SR.NSIBPM__SR_Template__c = SRTemplateList[0].Id;
            SR.Agency__c = a.id;
            SR.ID_Type__c = 'Passport';
            SR.country_of_sale__c = 'UAE';
            insert SR;
            
            List<User> lstUsers = [select id,name from user WHERE ProfileId IN: mpprofileidUsercount.keySet()];
            
            NSIBPM__Service_Request__c SR2 = InitializeSRDataTest.getSerReq('Deal',true,null);
            insert SR2;
            ManageDealTeam.ChangeAgent(SR2);
            
            SR.Agent_Name__c = lstUsers[0].id;
            SR.Agency_Type__c = 'Corporate';
            update SR;
            ManageDealTeam.ChangeAgent(SR);
            
            SR.Agent_Name__c = lstUsers[0].id;
            SR.Agency_Type__c = 'Corporate';
            update SR2;
            ManageDealTeam.ChangeAgent(SR2);
        }
        Test.stopTest();
    }
    
}