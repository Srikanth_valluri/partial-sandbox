public class IPMSJSONCreation {
    
    public String sourceSystem;
    public String extRequestNumber;
    public List <blockLines> blockLines;
    
    public class blockLines {
        public String subRequestName;
        public String unitId;
        public String pcId;
        public String managerId;
        public String agentId;
        public String reasonText;
    }
    
    public static IPMSJSONCreation parse(String json) {
        return (IPMSJSONCreation) System.JSON.deserialize(json, IPMSJSONCreation.class);
    }
    
    
}