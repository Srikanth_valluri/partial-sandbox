@isTest
public class GenerateDrawloopDocumentTest{
    static testMethod void TestNegative(){
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        
        Case objCase = new Case();
        objCase.Status = 'New';
        objCase.Origin = 'Web';
        objCase.NOC_Processing_Pending__c = true;
        objCase.RecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Assignment').RecordTypeId;
        objCase.Payment_Verified__c = true;
        objCase.Document_Verified__c = true;
        objCase.Pending_Amount__c = 10;
        insert objCase;
        
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        GenerateDrawloopDocument controller = new GenerateDrawloopDocument(sc);
        controller.callDrawloop();
        controller.returnToCase();
        Test.StopTest();
   }
    
    static testMethod void TestNegative1(){
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest( );
        insert objSR ;
        
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1);
        insert lstBookings;
        
        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        Inventory__c objInv = TestDataFactory_CRM.createInventory(objProp.Id);
        objInv.Property_Country__c = 'UAE';
        objInv.Property_City__c = 'Dubai';
        objInv.Property_Status__c = 'Ready';
        objInv.Bedroom_Type__c = ' 1 BR';
        insert objInv;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        lstBookingUnits[0].Inventory__c = objInv.Id;
        lstBookingUnits[0].Area__c = 35;
        lstBookingUnits[0].Handover_Flag__c = 'Y';
        lstBookingUnits[0].Registration_ID__c = null;
        insert lstBookingUnits;
        
        Case objCase = new Case();
        objCase.Status = 'New';
        objCase.Origin = 'Web';
        objCase.NOC_Processing_Pending__c = true;
        objCase.New_Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.RecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Assignment').RecordTypeId;
        objCase.Payment_Verified__c = true;
        objCase.Document_Verified__c = true;
        objCase.Pending_Amount__c = 0;
        insert objCase;
        
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        GenerateDrawloopDocument controller = new GenerateDrawloopDocument(sc);
        controller.callDrawloop();
        controller.returnToCase();
        Test.StopTest();
   }
    
    static testMethod void TestPositive(){
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest( );
        insert objSR ;
        
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1);
        insert lstBookings;
        
        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        Inventory__c objInv = TestDataFactory_CRM.createInventory(objProp.Id);
        objInv.Property_Country__c = 'UAE';
        objInv.Property_City__c = 'Dubai';
        objInv.Property_Status__c = 'Ready';
        objInv.Bedroom_Type__c = ' 1 BR';
        insert objInv;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        lstBookingUnits[0].Inventory__c = objInv.Id;
        lstBookingUnits[0].Area__c = 35;
        lstBookingUnits[0].Handover_Flag__c = 'Y';
        insert lstBookingUnits;
        
        Case objCase = new Case();
        objCase.Status = 'New';
        objCase.Origin = 'Web';
        objCase.NOC_Processing_Pending__c = true;
        objCase.New_Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.RecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Assignment').RecordTypeId;
        objCase.Payment_Verified__c = true;
        objCase.Document_Verified__c = true;
        objCase.Pending_Amount__c = 0;
        insert objCase;
        
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        GenerateDrawloopDocument controller = new GenerateDrawloopDocument(sc);
        controller.callDrawloop();
        controller.returnToCase();
        Test.StopTest();
   }

    static testMethod void TestPiazza1(){
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest( );
        insert objSR ;
        
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1);
        insert lstBookings;
        
        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        Inventory__c objInv = TestDataFactory_CRM.createInventory(objProp.Id);
        objInv.Property_Country__c = 'UAE';
        objInv.Property_City__c = 'Dubai';
        objInv.Property_Status__c = 'Ready';
        objInv.Bedroom_Type__c = ' 1 BR';
        insert objInv;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        lstBookingUnits[0].Inventory__c = objInv.Id;
        lstBookingUnits[0].Area__c = 35;
        lstBookingUnits[0].Handover_Flag__c = 'Y';
        lstBookingUnits[0].Property_Country__c = 'Qatar';
        insert lstBookingUnits;
        
        Case objCase = new Case();
        objCase.Status = 'New';
        objCase.Origin = 'Web';
        objCase.NOC_Processing_Pending__c = true;
        objCase.New_Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.RecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Assignment').RecordTypeId;
        objCase.Payment_Verified__c = true;
        objCase.Document_Verified__c = true;
        objCase.Pending_Amount__c = 0;
        insert objCase;
        
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        GenerateDrawloopDocument controller = new GenerateDrawloopDocument(sc);
        controller.callDrawloop();
        controller.returnToCase();
        Test.StopTest();
    }
    static testMethod void TestPiazza2(){
        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c(name = 'Assignment NOC For Piazza');
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest( );
        insert objSR ;
        
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1);
        insert lstBookings;
        
        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        Inventory__c objInv = TestDataFactory_CRM.createInventory(objProp.Id);
        objInv.Property_Country__c = 'UAE';
        objInv.Property_City__c = 'Dubai';
        objInv.Property_Status__c = 'Ready';
        objInv.Bedroom_Type__c = ' 1 BR';
        insert objInv;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        lstBookingUnits[0].Inventory__c = objInv.Id;
        lstBookingUnits[0].Area__c = 35;
        lstBookingUnits[0].Handover_Flag__c = 'Y';
        lstBookingUnits[0].Property_Country__c = 'Qatar';
        insert lstBookingUnits;
        
        Case objCase = new Case();
        objCase.Status = 'New';
        objCase.Origin = 'Web';
        objCase.NOC_Processing_Pending__c = true;
        objCase.New_Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.RecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Assignment').RecordTypeId;
        objCase.Payment_Verified__c = true;
        objCase.Document_Verified__c = true;
        objCase.Pending_Amount__c = 0;
        insert objCase;
        
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        GenerateDrawloopDocument controller = new GenerateDrawloopDocument(sc);
        controller.callDrawloop();
        controller.returnToCase();
        Test.StopTest();
    }
    static testMethod void TestPiazza3(){
        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c(name = 'Assignment NOC For Piazza', Drawloop_Document_Package_Id__c='3r3e');
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest( );
        insert objSR ;
        
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1);
        insert lstBookings;
        
        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        Inventory__c objInv = TestDataFactory_CRM.createInventory(objProp.Id);
        objInv.Property_Country__c = 'UAE';
        objInv.Property_City__c = 'Dubai';
        objInv.Property_Status__c = 'Ready';
        objInv.Bedroom_Type__c = ' 1 BR';
        insert objInv;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        lstBookingUnits[0].Inventory__c = objInv.Id;
        lstBookingUnits[0].Area__c = 35;
        lstBookingUnits[0].Handover_Flag__c = 'Y';
        lstBookingUnits[0].Property_Country__c = 'Qatar';
        insert lstBookingUnits;
        
        Case objCase = new Case();
        objCase.Status = 'New';
        objCase.Origin = 'Web';
        objCase.NOC_Processing_Pending__c = true;
        objCase.New_Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.RecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Assignment').RecordTypeId;
        objCase.Payment_Verified__c = true;
        objCase.Document_Verified__c = true;
        objCase.Pending_Amount__c = 0;
        insert objCase;
        
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        GenerateDrawloopDocument controller = new GenerateDrawloopDocument(sc);
        controller.callDrawloop();
        controller.returnToCase();
        Test.StopTest();
    }

    static testMethod void TestPiazza4(){
        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c(name = 'Assignment NOC For Piazza', Drawloop_Document_Package_Id__c='3r3e',Delivery_Option_Id__c='8898');
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest( );
        insert objSR ;
        
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1);
        insert lstBookings;
        
        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        Inventory__c objInv = TestDataFactory_CRM.createInventory(objProp.Id);
        objInv.Property_Country__c = 'UAE';
        objInv.Property_City__c = 'Dubai';
        objInv.Property_Status__c = 'Ready';
        objInv.Bedroom_Type__c = ' 1 BR';
        insert objInv;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        lstBookingUnits[0].Inventory__c = objInv.Id;
        lstBookingUnits[0].Area__c = 35;
        lstBookingUnits[0].Handover_Flag__c = 'Y';
        lstBookingUnits[0].Property_Country__c = 'Qatar';
        insert lstBookingUnits;
        
        Case objCase = new Case();
        objCase.Status = 'New';
        objCase.Origin = 'Web';
        objCase.NOC_Processing_Pending__c = true;
        objCase.New_Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.RecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Assignment').RecordTypeId;
        objCase.Payment_Verified__c = true;
        objCase.Document_Verified__c = true;
        objCase.Pending_Amount__c = 0;
        insert objCase;
        
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        GenerateDrawloopDocument controller = new GenerateDrawloopDocument(sc);
        controller.callDrawloop();
        controller.returnToCase();
        Test.StopTest();
    }
}