@isTest
private class BookingUnitDataProviderTest {
    @isTest
    static void testGetData() {
        User communityUser = CommunityTestDataFactory.CreatePortalUser();
        communityUser = [SELECT Id, AccountId FROM User WHERE Id = :communityUser.Id];
        NSIBPM__Service_Request__c dealSr = TestDataFactory_CRM.createServiceRequest();
        insert dealSr;
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(communityUser.AccountId, dealSr.Id, 1);
        insert lstBooking;
        List<Booking_Unit__c> lstUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        insert lstUnit;

        DataDisplayConfig config = new DataDisplayConfig();
        config.fieldList = new List<Map<String, String>> {
            new Map<String, String> {
                'field'=> 'Name',
                'title'=> 'Name',
                'type'=> 'link',
                'onClick'=> 'ccUtils.switchView("UnitDetail", "{!Id}")',
                DataRendererUtils.REFERENCED_FIELDS => 'Id'
            }
        };
        config.detailFieldList = new List<Map<String, String>> {
            new Map<String, String> {
                'field'=> 'Name',
                'title'=> 'Name',
                DataRendererUtils.REFERENCED_FIELDS => 'LastModifiedById'
            }
        };
        config.objectName = 'Booking_Unit__c';
        System.runAs(communityUser) {
            new BookingUnitDataProvider().getData(config);
        }
    }
}