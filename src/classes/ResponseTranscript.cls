@RestResource(urlMapping='/ResponseTranscript/*')
global with sharing class ResponseTranscript{

    @HttpPost 
    global static TranscriptDataFormat saveTranscriptLog(String CALL_ID, String MEETING_ID, String TRANSCRIPT) {
    
        System.debug('-->>TRANSCRIPT: '+ TRANSCRIPT);
        System.debug('-->>MEETING_ID: '+ MEETING_ID);
        System.debug('-->>CALL_ID: '+ CALL_ID);
        
        List<Call_Log__c> listCallLog = [SELECT Transcript_URL__c, Meeting_Id__c FROM Call_Log__c WHERE Id = :CALL_ID];
        //String strTranscript = (String)System.JSON.deserialize(TRANSCRIPT, String.class);
        //System.debug('-->>strTranscript : '+ strTranscript);
        Blob fileBody = Blob.valueOf(TRANSCRIPT);
        System.debug('-->>fileBody: '+ fileBody );
        
        if(!listCallLog.isEmpty()){
            for(Call_Log__c objCallLog: listCallLog){
                objCallLog.Meeting_Id__c  = MEETING_ID;
                List < UploadMultipleDocController.MultipleDocRequest > lstMultipleDocReq = new List < UploadMultipleDocController.MultipleDocRequest > ();
                UploadMultipleDocController.data respObj = new UploadMultipleDocController.data();
                List < Error_Log__c > errorLogList = new List < Error_Log__c > ();
                String errorMessage = '';
                 
                UploadMultipleDocController.MultipleDocRequest reqObjCRF = new UploadMultipleDocController.MultipleDocRequest();
                reqObjCRF.base64Binary = EncodingUtil.base64Encode(fileBody);
                reqObjCRF.category = 'Document';
                reqObjCRF.entityName = 'Damac Service Requests';
                reqObjCRF.fileDescription = 'Call Transcript of Wav file';
                reqObjCRF.fileId = objCallLog.Id + String.valueOf(System.currentTimeMillis())+'.'+'txt';
                reqObjCRF.fileName = objCallLog.Id + String.valueOf(System.currentTimeMillis())+'.'+'txt';
                
                reqObjCRF.registrationId = objCallLog.Id;
                reqObjCRF.sourceFileName = 'IPMS-' + objCallLog.Id + '-' + 'CallTranscript'+'.'+'txt';
                reqObjCRF.sourceId = 'IPMS-' + objCallLog.Id + '-' + 'CallTranscript';
                lstMultipleDocReq.add(reqObjCRF);
                
               /* if (lstMultipleDocReq.size() > 0) {
                    respObj = UploadMultipleDocController.getMultipleDocUrl(lstMultipleDocReq);
                    System.debug('===respObj==' + respObj);
                }*/
                
                if( !Test.isRunningTest() ) { 
                    respObj = UploadMultipleDocController.getMultipleDocUrl(lstMultipleDocReq);
                    System.debug('===respObj==' + respObj);
                }else {
                    
                    List<UploadMultipleDocController.MultipleDocResponse> data = new List<UploadMultipleDocController.MultipleDocResponse>();
                    UploadMultipleDocController.MultipleDocResponse objMultipleDocResponse = new UploadMultipleDocController.MultipleDocResponse();
                    objMultipleDocResponse.PROC_STATUS = 'S';
                    objMultipleDocResponse.url = 'www.google.com';
                    data.add(objMultipleDocResponse);
                    
                    respObj.Data = data;
                }                
                
                if (respObj != NULL && lstMultipleDocReq.size() > 0) {
                        if (respObj.status == 'Exception') {
                            errorMessage = respObj.message;
                            Error_Log__c objErr = new Error_Log__c();
                            errorMessage = respObj.message;
                            objErr.Error_Details__c = errorMessage;
                            objErr.Call_Log__c = objCallLog.Id;
                            errorLogList.add(objErr);
                        }
                        if(respObj.data !=null)
                        for (UploadMultipleDocController.MultipleDocResponse objData: respObj.data) {
                            if (objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url)) {
                                objCallLog.Transcript_URL__c = objData.url;
                                system.debug('objData.url : '+objData.url);
                            } else {
                                //errorMessage = 'Problems while getting response from document ' + objData.PARAM_ID;
                                Error_Log__c objErr = new Error_Log__c();
                                errorMessage = respObj.message;
                                objErr.Error_Details__c = 'Problems while getting response from document ';
                                objErr.Call_Log__c = objCallLog.Id;
                                errorLogList.add(objErr);
                            }
                        }
                }
                
                

                 if (errorLogList != null && errorLogList.size() > 0) {
                    insert errorLogList;
                    System.debug('===errorLogList==' + errorLogList);
                 }
                    
            }
            
            try{
               Database.update(listCallLog);
               
    
            } catch(Exception e){
                
            }
        }
        
        TranscriptDataFormat objTranscriptDataFormat = new TranscriptDataFormat(CALL_ID, MEETING_ID);
        
       
        
        return objTranscriptDataFormat;
    }
    
    global with sharing class TranscriptDataFormat{
        String callID;
        String meetingId;
        //String transcriptData;
        
        public TranscriptDataFormat(String callID, String meetingId){
            this.callID = callID;
            this.meetingId = meetingId ;
           // this.transcriptData = transcriptData;
        }
    }
}