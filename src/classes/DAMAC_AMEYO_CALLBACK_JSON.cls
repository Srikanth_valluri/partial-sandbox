public class DAMAC_AMEYO_CALLBACK_JSON{
    
    public String sessionId;    //d721-5da761d7-ses-apiuser-0KhZoZQW-1049
    public Integer campaignId;  //1
    //public integer queueId;

    
    public String callBackTime; //010-12-2029 1:07:26
    public String isSelfCallBack;   //false
    public String userId;   //ameyo_sup
    public String callBackHandlerType;  //voice.campaign.callback.handler
    public cls_callBackProperties callBackProperties;
    public class cls_callBackProperties {
        public String customerId;   //4148793
        public String phone;    //9582323281
    }
    public static DAMAC_AMEYO_CALLBACK_JSON parse(String json){
        return (DAMAC_AMEYO_CALLBACK_JSON) System.JSON.deserialize(json, DAMAC_AMEYO_CALLBACK_JSON.class);
    }
}