public without sharing class AP_AddNewActivityController {
    public String BASE_URL = Site.getBaseSecureUrl();
    public Inquiry__c objInquiry {get;set;}
    public NSIBPM__Service_Request__c objSR{get;set;}
    public Task objTask{get;set;}
    public string currentInquiryId {get;set;}
    public string pageName {get;set;}
    public String customStatus {get;set;}
    public String activityType3 {get;set;}
    public String meetingOutcome {get;set;}
    public String subject {get;set;}
    public String SRID;
    public List<SelectOption> outcomeLst {get;set;}
    public STring strSelectedLanguage {get;set;}
   /* @TestVisible
    public List<SelectOption> OutcomeLst {
        get {
            if (OutcomeLst == null) {
                OutcomeLst = getOutcomeLst();
            }
            return OutcomeLst;
        }
        private set;
    }*/
    @TestVisible
    public List<SelectOption> activityTypeLst {
        get {
            if (activityTypeLst == null) {
                activityTypeLst = getactivityTypeLst();
            }
            return activityTypeLst;
        }
        private set;
    }
  
  public AP_AddNewActivityController(){
    objSR = new NSIBPM__Service_Request__c();
        currentInquiryId = apexpages.currentPage().getParameters().get('inquiryId');
        pageName = apexpages.currentPage().getParameters().get('pageName');
        SRID = apexpages.currentPage().getParameters().get('Id');
        strSelectedLanguage = apexpages.currentPage().getParameters().get('langCode');
        if(SRID!= null){
          objSR = [SELECT id, (SELECT id 
                                      FROM Bookings__r)
                          FROM NSIBPM__Service_Request__c
                          WHERE id =: SRID];
          system.debug('objCurrentSR--'+objSR);
      }
        objInquiry = new Inquiry__c();
        if(String.isNotBlank(currentInquiryId))
        {
          objInquiry = [Select id,Name,Last_Name__c,First_Name__c,Mobile_Phone_Encrypt__c
                    ,Passport_Expiry_Date__c,Passport_Number__c,Passport_Place_of_Issue__c
                    ,Preferred_Language__c,Budget__c,Financing__c,Inquiry_Source__c,CreatedBy.Name
                    ,CreatedDate,LastModifiedBy.Name,Email__c,Country__c,
                    (SELECT id, Buyer_Type__c,Name,First_Name__c,Last_Name__c 
                    FROM Buyers__r)
              FROM Inquiry__c
                        WHERE Id =: currentInquiryId];
          system.debug('objInquiry----'+objInquiry);
          /*lstTask = new list<Task>();
          lstTask = [Select id,subject,Whatid,ActivityDate,Status
                ,CreatedDate,Activity_Outcome__c,Type 
                ,Activity_Type_3__c
                ,Task_Due_Date__c,CreatedBy.Name
                from Task where whatId = :currentInquiryId ]; 
      system.debug('lstTaskSize----'+lstTask.size());
      system.debug('lstTask----'+lstTask);    */         
        }
  }

  /*@TestVisible 
    private List<SelectOption> getOutcomeLst() {
        List<SelectOption> optionList = new List<SelectOption>();
        
        try {
            Map<String, Schema.Sobjectfield> srFields = Schema.SObjectType.Task.fields.getMap();
            Schema.DescribeFieldResult describe = srFields.get('Meeting_Outcome__c').getDescribe();
            System.debug('==describe=='+describe);
            List<Schema.PicklistEntry> options = describe.getPicklistValues();
            System.debug('==options=='+options);
          optionList.add(new selectOption('', 'Outcome'));
            for (Schema.PicklistEntry option : options) {
                optionList.add(new selectOption(option.getValue(), option.getLabel()));
            }           
                      
        }
        catch(Exception ex) {
            System.debug('Unable to get field details');
            //throw ex;
        }
        return optionList;
    }*/
    public void activityOutcome(){
        system.debug('???????activityType3???????'+activityType3);
        outcomeLst = new List<SelectOption>();
        if(activityType3 == 'Call - Outbound'){
            outcomeLst.add(new selectOption('Meeting Scheduled', 'Meeting Scheduled'));
            outcomeLst.add(new selectOption('Interested, but more follow up needed', 'Interested, but more follow up needed'));
            outcomeLst.add(new selectOption('Not interested', 'Not interested'));
        }
        if(activityType3 == 'Call-Inbound'){
            outcomeLst.add(new selectOption('Meeting Scheduled', 'Meeting Scheduled'));
            outcomeLst.add(new selectOption('Interested, but more follow up needed', 'Interested, but more follow up needed'));
            outcomeLst.add(new selectOption('Not interested', 'Not interested'));
            outcomeLst.add(new selectOption('Requested Call back', 'Requested Call back'));
            outcomeLst.add(new selectOption('Unreachable or phone switched off', 'Unreachable or phone switched off'));
            outcomeLst.add(new selectOption('Did Not Pick Up Call', 'Did Not Pick Up Call'));
        }
        if(activityType3 == 'SMS-Inbound'){
            outcomeLst.add(new selectOption('Meeting Scheduled', 'Meeting Scheduled'));
            outcomeLst.add(new selectOption('Interested, but more follow up needed', 'Interested, but more follow up needed'));
            outcomeLst.add(new selectOption('Not interested', 'Not interested'));
           
        }
        if(activityType3 == 'SMS-Outbound'){
            outcomeLst.add(new selectOption('SMS Delivered', 'SMS Delivered'));
            outcomeLst.add(new selectOption('SMS Not Delivered', 'SMS Not Delivered'));           
        }
        if(activityType3 == 'Meeting at Office'){
            outcomeLst.add(new selectOption('Show - Options given and considering', 'Show - Options given and considering'));
            outcomeLst.add(new selectOption('Show - Further meeting scheduled', 'Show - Further meeting scheduled'));  
            outcomeLst.add(new selectOption('Show - Interested, but more follow up needed', 'Show - Interested, but more follow up needed'));
            outcomeLst.add(new selectOption('Show - Not interested', 'Show - Not interested'));  
            outcomeLst.add(new selectOption('No Show - Meeting re-scheduled', 'No Show - Meeting re-scheduled'));
            outcomeLst.add(new selectOption('No Show - Follow up needed', 'No Show - Follow up needed'));  
            outcomeLst.add(new selectOption('No Show - Not Interested', 'No Show - Not Interested'));
            outcomeLst.add(new selectOption('No Show - Unreachable or phone switched off', 'No Show - Unreachable or phone switched off'));           
        }
        if(activityType3 == 'Meeting Outside Office'){
            outcomeLst.add(new selectOption('Show - Options given and considering', 'Show - Options given and considering'));
            outcomeLst.add(new selectOption('Show - Further meeting scheduled', 'Show - Further meeting scheduled'));  
            outcomeLst.add(new selectOption('Show - Interested, but more follow up needed', 'Show - Interested, but more follow up needed'));
            outcomeLst.add(new selectOption('Show - Not interested', 'Show - Not interested'));  
            outcomeLst.add(new selectOption('No Show - Meeting re-scheduled', 'No Show - Meeting re-scheduled'));
            outcomeLst.add(new selectOption('No Show - Follow up needed', 'No Show - Follow up needed'));  
            outcomeLst.add(new selectOption('No Show - Not Interested', 'No Show - Not Interested'));
            outcomeLst.add(new selectOption('No Show - Unreachable or phone switched off', 'No Show - Unreachable or phone switched off'));           
        }
        if(activityType3 == 'Meeting-Site Visit'){
            outcomeLst.add(new selectOption('Show - Options given and considering', 'Show - Options given and considering'));
            outcomeLst.add(new selectOption('Show - Further meeting scheduled', 'Show - Further meeting scheduled'));  
            outcomeLst.add(new selectOption('Show - Interested, but more follow up needed', 'Show - Interested, but more follow up needed'));
            outcomeLst.add(new selectOption('Show - Not interested', 'Show - Not interested'));  
            outcomeLst.add(new selectOption('No Show - Meeting re-scheduled', 'No Show - Meeting re-scheduled'));
            outcomeLst.add(new selectOption('No Show - Follow up needed', 'No Show - Follow up needed'));  
            outcomeLst.add(new selectOption('No Show - Not Interested', 'No Show - Not Interested'));
            outcomeLst.add(new selectOption('No Show - Unreachable or phone switched off', 'No Show - Unreachable or phone switched off'));           
        }
        if(activityType3 == 'Email Delivered'){
            outcomeLst.add(new selectOption('Meeting Scheduled', 'Meeting Scheduled'));
            outcomeLst.add(new selectOption('Interested, but more follow up needed', 'Interested, but more follow up needed'));
            outcomeLst.add(new selectOption('Not interested', 'Not interested'));
           
        }
        if(activityType3 == 'Email-Outbound'){
            outcomeLst.add(new selectOption('Email Delivered', 'Email Delivered'));
            outcomeLst.add(new selectOption('Email Bounced', 'Email Bounced'));           
        }
        //return null;
    }

    @TestVisible
    private List<SelectOption> getactivityTypeLst() {
        List<SelectOption> optionList = new List<SelectOption>();
        
        try {
            Map<String, Schema.Sobjectfield> srFields = Schema.SObjectType.Task.fields.getMap();
            Schema.DescribeFieldResult describe = srFields.get('Activity_Type_3__c').getDescribe();
            System.debug('==describe=='+describe);
            List<Schema.PicklistEntry> options = describe.getPicklistValues();
            System.debug('==options=='+options);
          optionList.add(new selectOption('', 'Type of Activity'));
            for (Schema.PicklistEntry option : options) {
                optionList.add(new selectOption(option.getValue(), option.getLabel()));
            }           
                      
        }
        catch(Exception ex) {
            System.debug('Unable to get field details');
            //throw ex;
        }
        return optionList;
    }

  public PageReference addNew(){
    PageReference pg ;
        system.debug('>>>currentInquiryId>>'+currentInquiryId);
    objTask = new Task();
    objTask.OwnerId = UserInfo.getUserId();
    objTask.Subject =subject;
        //objTask.isInquiry__c =true; 
    //objTask.Task_Due_Date__c='';
    objTask.Activity_Type_3__c=activityType3;
    objTask.Activity_Outcome__c=meetingOutcome;
    objTask.WhatId=currentInquiryId;
    objTask.Status = 'Completed';
    objTask.Priority = 'Normal';
    objTask.ActivityDate = System.today();
    try{
      insert objTask;
      system.debug('>>objTask'+objTask);
      //objTask.OwnerId = UserInfo.getUserId();
      //update objTask;
      pg=closeTask();
      return pg;
    }
    catch(Exception e){
      system.debug('>>Exception'+e);
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, e.getMessage()));
    }
    return null;
  }

  public PageReference closeTask(){
    PageReference pg ;
    system.debug('>>pageName'+pageName);
    if(pageName=='AP_CILDetailPage'){
            pg = Page.AP_CILDetailPage;
      //pg = new PageReference(Label.AP_CommunityRedirectURL+ '/' +pageName);
            pg.getParameters().put('inquiryId',currentInquiryId);
            if(String.isNotBlank(strSelectedLanguage)) {
                pg.getParameters().put('langCode',strSelectedLanguage);
            }
    }else{
      pg = new PageReference(BASE_URL+ '/' +pageName+'?langCode='+strSelectedLanguage);
            
    }
    return pg;
  }
}