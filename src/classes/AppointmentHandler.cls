/****************************************************************************************************
* Name          : AppointmentHandler                                                                *
* Description   : Logic to show error or reparent assignment slot on cre change                     *
* Created Date  : 05-02-2019                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE        COMMENTS                                                    *
* 1.0                                   Initial Draft.                                              *
****************************************************************************************************/

public with sharing class AppointmentHandler {
    
     public static void onCREUpdate(map<Id, Calling_List__c> mapNewAppointment, 
        map<Id, Calling_List__c> mapOldAppointment) {
         
         set<Id> setAssignedCRE = new set<Id>();
         list<String> lstProcessName = new list<String>();
         list<String> lstSubProcessName = new list<String>();
         set<Date> setAppointmentDate = new set<Date>();
         set<String> setAppointmentSlot = new set<String>();
         list<String> lstAppointmentDay = new list<String>();
         list<Calling_List__c> lstUpdateCallList = new list<Calling_List__c>();
         Id AppointmentRTId = 
            Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Appointment Scheduling').getRecordTypeId();
         
         for (Calling_List__c objCall : mapNewAppointment.values()) {
             if (objCall.RecordTypeId == AppointmentRTId){
                 Calling_List__c oldCallingList = mapOldAppointment.get(objCall.Id);
                 
                 if (objCall.Assigned_CRE__c != oldCallingList.Assigned_CRE__c){
                     system.debug('!!!!cre updated');
                     setAssignedCRE.add(objCall.Assigned_CRE__c);
                     lstProcessName.add(objCall.Service_Type__c);
                     lstSubProcessName.add(objCall.Sub_Purpose__c);
                     setAppointmentDate.add(objCall.Appointment_Date__c);
                     Datetime dt = DateTime.newInstance(objCall.Appointment_Date__c, Time.newInstance(0, 0, 0, 0));                     
                     string dayofDate = dt.format('EEEE');
                     lstAppointmentDay.add(dayofDate);
                     String startDate, endDate;
                     if (objCall.Appointment_Start_DateTime__c != null){
                         startDate = objCall.Appointment_Start_DateTime__c.format('HH:mm');
                     }
                     if (objCall.Appointment_End_DateTime__c != null){
                         endDate = objCall.Appointment_End_DateTime__c.format('HH:mm');
                     }
                     setAppointmentSlot.add(startDate + ' - '+ endDate);
                     lstUpdateCallList.add(objCall);
                 }
             }
         }// end of for
         map<id, list<Appointment__c>> mapCREAppointment = new map<Id, list<Appointment__c>>();
         if (setAssignedCRE != null && !setAssignedCRE.isEmpty()){
             system.debug('!!!!!setProcessName'+lstProcessName);
             //system.debug('!!!!!setSubProcessName'+setSubProcessName);
             String processNames = String.join(lstProcessName, ',');
             String subProcessNames = String.join(lstSubProcessName, ',');
             String weekdays = String.join(lstAppointmentDay, ',');
             system.debug('!!!!!subProcessNames'+subProcessNames);
             system.debug('!!!!!setAppointmentDate'+setAppointmentDate);
             system.debug('!!!!!setAppointmentDay'+lstAppointmentDay);
             system.debug('!!!!!setAppointmentSlot'+setAppointmentSlot);
             for (Appointment__c objApp: [Select Id
                                            , Name
                                            , Assigned_CRE__c
                                            , Processes__c
                                            , Sub_Processes__c
                                            , Appointment_Date__c
                                            , Appointment_End_Date__c
                                            , Day_of_Week__c
                                            , Slots__c
                                            , (Select Id, Name, Appointment_Date__c 
                                                From Calling_List__r
                                                Where Appointment_Date__c IN: setAppointmentDate
                                                And (Appointment_Status__c != 'Rejected' And
                                                Appointment_Status__c != 'Cancelled'))
                                          From Appointment__c
                                          Where Assigned_CRE__c IN: setAssignedCRE
                                          And Processes__c INCLUDES (: processNames)
                                          And Sub_Processes__c INCLUDES (: subProcessNames)
                                          And Appointment_Date__c <=: setAppointmentDate
                                          And Appointment_End_Date__c >=: setAppointmentDate
                                          And ( Day_of_Week__c INCLUDES (: weekdays)
                                          Or Day_of_Week__c = null)
                                          And Slots__c IN: setAppointmentSlot
                                         ]){
                 system.debug('!!!!!!inside query');
                 if (mapCREAppointment.containsKey(objApp.Assigned_CRE__c)) {
                    list<Appointment__c> lstAppointment = mapCREAppointment.get(objApp.Assigned_CRE__c);
                    lstAppointment.add(objApp);
                    mapCREAppointment.put(objApp.Assigned_CRE__c, lstAppointment);
                 } else {
                    mapCREAppointment.put(objApp.Assigned_CRE__c, new list <Appointment__c> {objApp});
                 }
             }
             system.debug('!!!!!!!mapCREAppointment'+mapCREAppointment);
         }
         
         for (Calling_List__c objCallList : lstUpdateCallList){             
             String startDate, endDate;
             if (objCallList.Appointment_Start_DateTime__c != null){
                startDate = objCallList.Appointment_Start_DateTime__c.format('HH:mm');
             }
             if (objCallList.Appointment_End_DateTime__c != null){
                endDate = objCallList.Appointment_End_DateTime__c.format('HH:mm');
             }
             String strSlot = startDate + ' - '+ endDate;
             Datetime dt = DateTime.newInstance(objCallList.Appointment_Date__c, Time.newInstance(0, 0, 0, 0));                     
             string dayofDate = dt.format('EEEE');
             if (mapCREAppointment != null && 
                mapCREAppointment.containsKey(objCallList.Assigned_CRE__c)){
	             for (Appointment__c objAppointment : mapCREAppointment.get(objCallList.Assigned_CRE__c)){
	                 if (objAppointment.Processes__c != null 
	                    && objAppointment.Processes__c.contains(objCallList.Service_Type__c) 
	                    && objAppointment.Sub_Processes__c != null
	                    && objAppointment.Sub_Processes__c.contains(objCallList.Sub_Purpose__c)
	                    && objAppointment.Slots__c != null
	                    && objAppointment.Slots__c == strSlot
	                    && objAppointment.Appointment_Date__c <= objCallList.Appointment_Date__c
	                    && objAppointment.Appointment_End_Date__c >= objCallList.Appointment_Date__c
	                    && ( objAppointment.Day_of_Week__c == null || (objAppointment.Day_of_Week__c != null 
	                    && objAppointment.Day_of_Week__c.contains(dayofDate)))
	                  ){
	                      system.debug('!!!!objAppointment.Calling_List__r'+objAppointment.Calling_List__r);
	                     if (objAppointment.Calling_List__r != null 
	                       && !objAppointment.Calling_List__r.isEmpty()){
	                         objCallList.addError('Appointment for this CRE is already booked, please choose another cre');
	                     } else {
	                         objCallList.Appointment__c = objAppointment.id;
	                     }
	                 }
	            }
            }
         }         
     }
}