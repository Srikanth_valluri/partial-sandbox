public Class InventoryProjectUtility{
    public InventoryProjectUtility(){}
    
    public static void insertInventoryLog(Id invId, String fieldAPIName, String oldValue, 
                                                            String newValue, String details, String remarks){
        Inventory_Log__c invLog = createInventoryLog(invId, fieldAPIName, oldValue,
                                                                 newValue, details, remarks);
        insert invLog;
    }
    
    public static Inventory_Log__c createInventoryLog(Id invId, String fieldAPIName, String oldValue, 
                                                    String newValue, String details, String remarks){
        return createLog(invId, '', fieldAPIName, oldValue, newValue, details, remarks, 'Inventory');
    }
    
    public static Inventory_Log__c createBookingUnitLog(Id invId, Id buId, String fieldAPIName, String oldValue, 
                                                    String newValue, String details, String remarks){
        return createLog(invId, String.valueOf(buId), fieldAPIName, oldValue, newValue, details, remarks, 'Booking Unit');
    }
    
    public static Inventory_Log__c createLog(Id invId, String buId,  String fieldAPIName, String oldValue, 
                                                 String newValue, String details, String remarks, String logObject){
        system.debug('invId'+invId);  
        String fieldLabel = '';
        String type='Inventory__c';
        fieldLabel = Schema.getGlobalDescribe().get('Inventory__c').getDescribe().fields.getMap().get(fieldAPIName).getDescribe().getLabel();
        
        Inventory_Log__c invLog = new Inventory_Log__c();
        invLog.Inventory__c = invId;
        if(buId != ''){
            invLog.Booking_Unit__c = buId;
        }
        invLog.User__c = UserInfo.getUserId();
        invLog.Field_API_Name__c = fieldAPIName;
        invLog.Field_Name__c = fieldLabel;
        invLog.Updated_Date__c = system.now();
        invLog.Old_Value__c = oldValue;
        invLog.New_Value__c = newValue;
        invLog.Update_Details__c = details;
        invLog.Log_Object__c = logObject;
        system.debug('invLog: ' + invLog);
        return invLog;
    }
    
    @future
    public static void insertListInventoryLog(List<Id> invIdList, String fieldAPIName, String oldValue, 
                                                             String newValue, String details, String remarks){
        insertListLog(invIdList, fieldAPIName, oldValue, newValue, details, remarks, 'Inventory');
    } 
    
    
    public static void insertListLog(List<Id> invIdList, String fieldAPIName, String oldValue, 
                                                             String newValue, String details, String remarks, String logObject){
        String fieldLabel = '';
        String type='Inventory__c';
        fieldLabel = Schema.getGlobalDescribe().get('Inventory__c').getDescribe().fields.getMap().get(fieldAPIName).getDescribe().getLabel();
        List<Inventory_Log__c> logList = new List<Inventory_Log__c>();
        for(Id invId: invIdList) {
            Inventory_Log__c invLog = new Inventory_Log__c();
            invLog.Inventory__c = invId;
            invLog.User__c = UserInfo.getUserId();
            invLog.Field_API_Name__c = fieldAPIName;
            invLog.Field_Name__c = fieldLabel;
            invLog.Updated_Date__c = system.now();
            invLog.Old_Value__c = oldValue;
            invLog.New_Value__c = newValue;
            invLog.Update_Details__c = details;
            invLog.Update_Details__c = remarks;
            invLog.Log_Object__c = logObject;
            
            logList.add(invLog);
        }
        if(logList.size() > 0){
            insert logList;
        }
    }
    
 
    
}