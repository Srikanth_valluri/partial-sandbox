public class AP_FormRequestController{
    public List<Form_Request__c> formRequestsList { get; set; }
    public AP_FormRequestController(){
        formRequestsList = new List<Form_Request__c>();
        User currentUser = [SELECT Contact.AccountId FROM User WHERE ID =: userInfo.getUserId()];
        formRequestsList = [SELECT Name,Buyer_Type__c, Inquiry__r.Name, Status__c, CreatedDate, Purchaser_s_Name__c, Passport_Number__c ,
                                    Email__c, Nationality__c, (SELECT Unit_Name__c FROM Form_Request_Inventory__r)
                                    FROM Form_Request__c WHERE RecordType.Name != 'UK Form' AND CreatedById =: Userinfo.getUserId () 
                                    Order By Name DESC];
                                    
                                    /*AND 
                                    ((Inquiry__c != null AND Inquiry__r.Name != null) OR 
                                    (Inquiry__c = null AND Corporate__c =: currentUser.Contact.AccountId))];*/
    }
    @remoteAction
    public static String  previewSignedForm (ID formRequestId) {
        try {
            ContentDocumentLink link = [SELECT contentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: formRequestId];
            ContentVersion version = [SELECT versionData FROM contentVersion WHERE ContentDocumentId =: link.ContentDocumentId];

            Attachment att = new Attachment ();
            att.Body = version.versionData;
            att.ParentId = formRequestId;
            att.Name = 'Reservation Copy.pdf';
            insert att;
            return './servlet/servlet.FileDownload?file='+att.Id;
            
        } catch (Exception e) {
            return 'Failed to generate the preview link';
        }
    }
    
}