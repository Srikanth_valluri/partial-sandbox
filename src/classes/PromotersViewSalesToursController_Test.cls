/*-------------------------------------------------------------------------------------------------
Description: Test class for PromotersViewSalesToursController

    ============================================================================================================================
        Version | Date(DD-MM-YYYY) | Last Modified By | Comments
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 29-03-2018       | Rajnish Kumar  | 1. Initial draft
   =============================================================================================================================
*/
@isTest
public  class PromotersViewSalesToursController_Test{
    @isTest
    public static void createTestData() {
        Account ac = new Account(name ='Grazitti') ;
        insert ac; 
       
        Contact con = new Contact(LastName ='testCon',AccountId = ac.Id);
        insert con; 
        /*User promoter = new User(
         ProfileId = [SELECT Id FROM Profile WHERE Name like '%Promoter%'][0].Id,
         LastName = 'last',
         Email = 'puser000@amamama.com',
         Username = 'puser000@amamama.com' + System.currentTimeMillis(),
         CompanyName = 'TEST',
         Title = 'title',
         Alias = 'alias',
         contactId = con.id,
         TimeZoneSidKey = 'America/Los_Angeles',
         EmailEncodingKey = 'UTF-8',
         LanguageLocaleKey = 'en_US',
         LocaleSidKey = 'en_US'
         
        );*/
        
        List<Inquiry__c> lstInquiry = new List<Inquiry__c>();
        List<Sales_Tours__c> lstSalesTours = new List<Sales_Tours__c>();
       
        Campaign__c objCampaign = InitialiseTestData.getCampaignDetails();
        objCampaign.Campaign_Type_New__c = 'Stands';
        objCampaign.Campaign_Name__c = 'test';
        objCampaign.Marketing_Active__c= true;
        objCampaign.Credit_Control_Active__c= true;
        objCampaign.Sales_Admin_Active__c = true;
        insert   objCampaign ;

        //insert promoter;
        //System.debug('====promoter===='+promoter);
        // System.runAs(promoter){
       
            Inquiry__c inquiryRec = InitialiseTestData.getInquiryDetails('Inquiry', 1);
            inquiryRec.Campaign__c = objCampaign.id;
            insert inquiryRec;
            
            /*for(Inquiry__c inq : lstInquiry) {
                for (Integer i=0;i<2;i++) {
                    Sales_Tours__c salesTour = new Sales_Tours__c(Inquiry__c=inq.ID,
                                            Tour_Outcome__c = 'Show - Not interested',
                                            Comments__c='Test');
                    lstSalesTours.add(salesTour);
                }
            }*/
           // insert lstSalesTours;
            PromotersViewSalesToursController controller = new PromotersViewSalesToursController();
            ApexPages.StandardSetController test = controller.setCon;
            controller.search();
            list<Sales_Tours__c> salesTours = new list<Sales_Tours__c>();
            salesTours=controller.getlstSalesTours();
            System.debug('====salesTours===='+salesTours);
        // }
        
    }
}