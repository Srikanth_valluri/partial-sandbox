@isTest
private class UnitBuyersDataProviderTest {
    @isTest
    static void testGetData() {
        DataDisplayConfig config = new DataDisplayConfig();
        config.recordId = NULL;
        config.fieldList = new List<Map<String, String>> {
            new Map<String, String> {
                'field'=> 'Id',
                'title'=> 'Id'
            }
        };
        config.detailFieldList = new List<Map<String, String>> {
            new Map<String, String> {
                'field'=> 'Name',
                'title'=> 'Name'
            }
        };
        config.objectName = 'Buyer__c';
        new UnitBuyersDataProvider().getData(config);
    }
}