@isTest
private class CREEmailEsclationTest{//AKISHOR

  @isTest static void test_batch() {
    List<Case> lstCases = new List<Case>();
    //Set<Id> caseid= new Set<Id>(); //0050Y000000PluO
     Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
    User u = new User(Alias = 'standt', Email='teststdusertest@testorg.com', 
    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
    LocaleSidKey='en_US', ProfileId = p.Id, 
    TimeZoneSidKey='America/Los_Angeles', UserName='teststdusertest@testorg.com'); 
    insert u;
    for(Integer i=0;i<10;i++) {
        lstCases.add(new Case(Status='Submitted',Escalation_Date__c=Date.today(),
            OwnerId=u.id));
      }
      insert lstCases;
    
   List<Task> lstTask = new List <Task>();
    for(Case cs : lstCases){
        lstTask.add(new Task(Subject='New Complaint Request',Assigned_User__c='CRE',Status='Not Started',
        ActivityDate=Date.today(),WhatId=cs.id,OwnerId=u.id));
    }
    insert lstTask;
    User userObj = [SELECT Email,IsActive FROM user where id =: u.id];
    system.debug('owner == ' + userObj);
    userObj.isActive = true;
    userObj.Email = 'test@tgmail.com';
    update userObj;
    system.debug( 'lstTask  : ' + [SELECT ID,createdDate,What.id,Owner.Email,OwnerId,Owner.Name,
  Status,ActivityDate,Subject, Assigned_User__c,Owner.IsActive from Task] );
  System.debug('Owner.Email = ' + lstTask[0].Owner.Email);
  System.debug('Owner.IsActive = ' + lstTask[0].Owner.IsActive);
      Test.startTest();
      CRMPendingTaskEmailBatch batchObj = new CRMPendingTaskEmailBatch();
      Database.executeBatch(batchObj);
      Test.stopTest();
  }

}