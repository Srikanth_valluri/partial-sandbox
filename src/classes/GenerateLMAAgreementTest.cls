/**
 * Description: Test class for GenerateLMAAgreement
 */
@isTest
private class GenerateLMAAgreementTest {

    /*
     * Description : Test method to check whether document is generated through Drawloop or not
     * where Booking Unit has Reg Id and RGS Percent.
     */
    @isTest static void testCallDrawloop() {

        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c>  lstBookings = TestDataFactory_CRM.createBookingForAccount(objAccount.Id, objSR.Id, 1);
        insert lstBookings ;
        
        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;

        Inventory__c  objInv = TestDataFactory_CRM.createInventory(objProp.Id);
        objInv.Property_Country__c = 'UAE';
        objInv.Property_City__c = 'Dubai';
        objInv.Property_Status__c = 'Ready';
        objInv.Bedroom_Type__c = ' 1 BR';
        insert objInv;

        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        lstBookingUnits[0].Inventory__c = objInv.Id;
        lstBookingUnits[0].Area__c = 35;
        lstBookingUnits[0].Handover_Flag__c = 'Y';
        lstBookingUnits[0].Registration_ID__c = '12345';
        lstBookingUnits[0].RGS_Percent__c = '50';
        insert lstBookingUnits;
        Id handoverRecordTypeId = getRecordTypeIdForHandover();

        // Creation of Case
        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , handoverRecordTypeId);
        objCase.Handover__c = true;
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objCase;

        objCase = [Select OwnerId,Registration_ID__c,Handover__c,Booking_Unit__c FROM Case WHERE Id =:objCase.Id];

        PageReference redirectionUrl;
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new RestServiceMock());
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(objCase);
            GenerateLMAAgreement objController = new GenerateLMAAgreement(standardControllerInstance);
            Test.setCurrentPageReference(new PageReference('Page.GenerateLMAAgreement'));
            System.currentPageReference().getParameters().put('Id', objCase.Id);
            redirectionUrl = objController.returnToCase();
            objController.callDrawloop();
        Test.stopTest();
    }

    /*
     * Description : Test method to check whether document is generated through Drawloop or not
     * where Booking Unit does not have Reg Id.
     */
    @isTest static void testCallDrawloopWithoutRegId() {

        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c>  lstBookings = TestDataFactory_CRM.createBookingForAccount(objAccount.Id, objSR.Id, 1);
        insert lstBookings ;
        
        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;

        Inventory__c  objInv = TestDataFactory_CRM.createInventory(objProp.Id);
        objInv.Property_Country__c = 'UAE';
        objInv.Property_City__c = 'Dubai';
        objInv.Property_Status__c = 'Ready';
        objInv.Bedroom_Type__c = ' 1 BR';
        insert objInv;

        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        lstBookingUnits[0].Inventory__c = objInv.Id;
        lstBookingUnits[0].Area__c = 35;
        lstBookingUnits[0].Handover_Flag__c = 'Y';
        lstBookingUnits[0].RGS_Percent__c = '50';
        lstBookingUnits[0].Registration_ID__c = null;
        insert lstBookingUnits;
        Id handoverRecordTypeId = getRecordTypeIdForHandover();

        // Creation of Case
        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , handoverRecordTypeId);
        objCase.Handover__c = true;
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objCase;

        objCase = [Select OwnerId,Registration_ID__c,Handover__c,Booking_Unit__c FROM Case WHERE Id =:objCase.Id];

        PageReference redirectionUrl;
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new RestServiceMock());
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(objCase);
            GenerateLMAAgreement objController = new GenerateLMAAgreement(standardControllerInstance);
            Test.setCurrentPageReference(new PageReference('Page.GenerateLMAAgreement'));
            System.currentPageReference().getParameters().put('Id', objCase.Id);
            objController.callDrawloop();
        Test.stopTest();
    }

    /*
     * Description : Test method to check whether document is generated through Drawloop or not
     * where Booking Unit does not have RGS Percent.
     */
    @isTest static void testCallDrawloopWithoutRGSPercent() {

        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c>  lstBookings = TestDataFactory_CRM.createBookingForAccount(objAccount.Id, objSR.Id, 1);
        insert lstBookings ;
        
        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;

        Inventory__c  objInv = TestDataFactory_CRM.createInventory(objProp.Id);
        objInv.Property_Country__c = 'UAE';
        objInv.Property_City__c = 'Dubai';
        objInv.Property_Status__c = 'Ready';
        objInv.Bedroom_Type__c = ' 1 BR';
        insert objInv;

        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        lstBookingUnits[0].Inventory__c = objInv.Id;
        lstBookingUnits[0].Area__c = 35;
        lstBookingUnits[0].Handover_Flag__c = 'Y';
        lstBookingUnits[0].Registration_ID__c = '12345';
        insert lstBookingUnits;
        Id handoverRecordTypeId = getRecordTypeIdForHandover();

        // Creation of Case
        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , handoverRecordTypeId);
        objCase.Handover__c = true;
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objCase;

        objCase = [Select OwnerId,Registration_ID__c,Handover__c,Booking_Unit__c FROM Case WHERE Id =:objCase.Id];

        PageReference redirectionUrl;
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new RestServiceMock());
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(objCase);
            GenerateLMAAgreement objController = new GenerateLMAAgreement(standardControllerInstance);
            Test.setCurrentPageReference(new PageReference('Page.GenerateLMAAgreement'));
            System.currentPageReference().getParameters().put('Id', objCase.Id);
            objController.callDrawloop();
        Test.stopTest();
    }

    /*
     * Description : Test method to check whether document is generated through Drawloop or not
     * where Case does not have handover flag set to true.
     */
    @isTest static void testCallDrawloopWithoutHO() {

        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c>  lstBookings = TestDataFactory_CRM.createBookingForAccount(objAccount.Id, objSR.Id, 1);
        insert lstBookings ;
        
        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;

        Inventory__c  objInv = TestDataFactory_CRM.createInventory(objProp.Id);
        objInv.Property_Country__c = 'UAE';
        objInv.Property_City__c = 'Dubai';
        objInv.Property_Status__c = 'Ready';
        objInv.Bedroom_Type__c = ' 1 BR';
        insert objInv;

        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        lstBookingUnits[0].Inventory__c = objInv.Id;
        lstBookingUnits[0].Area__c = 35;
        lstBookingUnits[0].Handover_Flag__c = 'Y';
        lstBookingUnits[0].Registration_ID__c = '12345';
        insert lstBookingUnits;
        Id handoverRecordTypeId = getRecordTypeIdForHandover();

        // Creation of Case
        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , handoverRecordTypeId);
        objCase.Handover__c = false;
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objCase;

        objCase = [Select OwnerId,Registration_ID__c,Handover__c,Booking_Unit__c FROM Case WHERE Id =:objCase.Id];

        PageReference redirectionUrl;
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new RestServiceMock());
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(objCase);
            GenerateLMAAgreement objController = new GenerateLMAAgreement(standardControllerInstance);
            Test.setCurrentPageReference(new PageReference('Page.GenerateLMAAgreement'));
            System.currentPageReference().getParameters().put('Id', objCase.Id);
            objController.callDrawloop();
        Test.stopTest();
    }

    /**
     * Method to get the "Handover" record type
     */
    private static Id getRecordTypeIdForHandover() {
      Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
      Id handoverRecordTypeId = caseRecordTypes.get('Handover').getRecordTypeId();
      return handoverRecordTypeId;
    }
}