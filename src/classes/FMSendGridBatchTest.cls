/*-------------------------------------------------------------------------------------------------
Description: Test class to test the functionality of FMSendGridBatch and SendGridIterable classes
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 27-02-2019       | Lochana Rajput   | 1. Added cases to test the functionality of FMSendGridBatch and 
                                                SendGridIterable classes
=============================================================================================================================
*/
@isTest
private class FMSendGridBatchTest {
    static testMethod void testMethod1(){
        list<Attachment> lstAttach = new list<Attachment>();
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        objAcc.Country__c = 'United Arab Emirates';
        insert objAcc ;

        NSIBPM__Service_Request__c objServReq = TestDataFactory_CRM.createServiceRequest();
        insert objServReq ;

        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objServReq.Id,1);
        insert lstBooking ;
        
        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking ,3);
        for( Booking_Unit__c objUnit : lstBookingUnit  ) {
            objUnit.Resident__c = objAcc.Id ;
            objUnit.Handover_Flag__c = 'Y' ;
        }
        lstBookingUnit[2].Owner__c = objAcc.Id ;
        insert lstBookingUnit ;

        Attachment objAttach = new Attachment();
        objAttach.Body = Blob.valueof('Test Mail');
        objAttach.Name = 'Test Email';
        objAttach.ParentId = objAcc.Id;
        insert objAttach;
        
        lstAttach.add(objAttach);

        list<SendGridIterable.sendGridEmail> lstClass = new list<SendGridIterable.sendGridEmail>();
        SendGridIterable.sendGridEmail objClass = new SendGridIterable.sendGridEmail();
        objClass.toAddress='a@mailinator.com';
        objClass.toName='test';
        objClass.ccAddress='ad@j.com';
        objClass.ccName='test';
        objClass.bccAddress='ad@jd.com';
        objClass.bccName='test';
        objClass.subject='test';
        objClass.substitutions='test';
        objClass.fromAddress='test@k.com';
        objClass.fromName='test';
        objClass.replyToAddress='ad@j.com';
        objClass.replyToName='test';
        objClass.contentType='plain/text';
        objClass.contentValue='test';
        objClass.templateId='test';
        objClass.lstAttach = lstAttach;
        objClass.relatedId = objAcc.id;
        objClass.buId= lstBookingUnit[0].id;
        lstClass.add(objClass);
        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
        test.startTest();  
        FMSendGridBatch objBatchClass = new FMSendGridBatch(lstClass);
        Database.Executebatch(objBatchClass);
        
        Test.stopTest();
        
        
    }
}