public with sharing class DataCategoryUtil {

    /**
     * Method to fetch the Data Categories related to Knowledge Article Version
     * 
     * @return describeCategoryStructureResult List of the categories
     */
    public static List<DescribeDataCategoryGroupStructureResult> 
        getDescribeDataCategoryGroupStructureResults(
        ) {
        
        List<DescribeDataCategoryGroupStructureResult> describeCategoryStructureResult;

        //Call to the describeDataCategoryGroups to get the list of category groups associated
        List<String> objType = new List<String>();
        objType.add('KnowledgeArticleVersion');//objType.add(Label.KnowledgeArticle); - replaced for footer test class
        system.debug('>>>>>objType>>>'+objType);
        List<DescribeDataCategoryGroupResult> describeCategoryResult = Schema.describeDataCategoryGroups(objType);
        system.debug('>>>>>describeCategoryResult>>>'+describeCategoryResult);
        //Creating a list of pair objects to use as a parameter for the describe call
        List<DataCategoryGroupSobjectTypePair> dataCategoryTypePairs = 
            new List<DataCategoryGroupSobjectTypePair>();
        for(DescribeDataCategoryGroupResult singleResult : describeCategoryResult) {
            DataCategoryGroupSobjectTypePair dataCategoryTypePair = 
                new DataCategoryGroupSobjectTypePair();
            dataCategoryTypePair.setSobject(singleResult.getSobject());
            dataCategoryTypePair.setDataCategoryGroupName(singleResult.getName());
            dataCategoryTypePairs.add(dataCategoryTypePair);
        }
        describeCategoryStructureResult = Schema.describeDataCategoryGroupStructures(
            dataCategoryTypePairs, 
            false
        );
        for(DescribeDataCategoryGroupStructureResult singleResult : 
            describeCategoryStructureResult
        ) {
            singleResult.getName();
            DataCategory [] toplevelCategories = singleResult.getTopCategories();
            List<DataCategory> allCategories = getAllCategories(toplevelCategories);
            for(DataCategory category : allCategories) {
                category.getName();
            }
        }
        system.debug('>>>>>describeCategoryStructureResult>>>'+describeCategoryStructureResult);
        return describeCategoryStructureResult;
    }

    /**
     * Method to fetch all the child categories
     * 
     * @param categories List of Top Level Categories
     *
     * @return allCattegories List of All Categories
     */
    private static  DataCategory[] getAllCategories(DataCategory [] categories) {
        if(categories.isEmpty()) {
             return new DataCategory[]{};
        }else {
            DataCategory [] categoriesClone = categories.clone();
            DataCategory category = categoriesClone[0];
            DataCategory[] allCategories = new DataCategory[]{category};
            categoriesClone.remove(0);
            categoriesClone.addAll(category.getChildCategories());
            allCategories.addAll(getAllCategories(categoriesClone));
            return allCategories;
        }
    }

    /**
     * Method to get the Categories in the wrapper format
     *
     * @return List<RootObject> list of Data Category in wrapper format
     */
    public static List<RootObject> getDataCategoriesList() {
        return (
            List<RootObject>)JSON.deserialize(
            JSON.serialize(
                getDescribeDataCategoryGroupStructureResults()
            ), 
            List<RootObject>.class
        );
    }

    /**
     * Wrapper to get all the Child Categories 
     */
    public class ChildCategory {
        //Get the Sub Child Categories related to Child Categories 
        public List<ChildCategory> childCategories { get; set; }
        public String label { get; set; }
        public String name { get; set; }
        public ChildCategory(List<ChildCategory> pchildCategories, String plabel,String pname) {
            childCategories = pchildCategories;
            label = plabel;
            name = pname;
        }
    }

    /**
     * Wrapper to get all the Top Categories 
     */
    public class TopCategory {
        public List<ChildCategory> childCategories { get; set; }
        public String label { get; set; }
        public String name { get; set; }
        public TopCategory(List<ChildCategory> pChildCategories, String plabel,String pname) {
            childCategories = pChildCategories;
            label = plabel;
            name = pname;
        }
    }

    /**
     * Wrapper to get all the Root Categories 
     */
    public class RootObject {
        public String description { get; set; }
        public String label { get; set; }
        public String name { get; set; }
        
        public List<TopCategory> topCategories { get; set; }
        public RootObject(
            String pdescription, 
            List<TopCategory> ptopCategories, 
            String plabel,
            String pname
        ) {
            description = pdescription;
            topCategories = ptopCategories;
            label = plabel;
            name = pname;
        }
    }
}