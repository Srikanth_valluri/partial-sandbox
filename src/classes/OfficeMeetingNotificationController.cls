public class OfficeMeetingNotificationController{
    public Id officeMeetngId {get;set;}
    public Office_Meeting__c getMeeting() {
        Office_Meeting__c meeting ;
        meeting  = [
                    SELECT Recommended_RM__c,Recommended_RM__r.Name, Inquiry__c,Inquiry__r.Project__c,Inquiry__r.Name,Inquiry__r.Type_of_Property_Interested_in__c,
                            Inquiry__r.Property__c,Inquiry__r.Area_In_sq_ft__c,Inquiry__r.No_Of_Bed_Rooms_Availble_in_SF__c,Inquiry__r.Property__r.name,
                            Inquiry__r.Budget__c,Inquiry__r.Completed__c,Inquiry__r.Joint_Income_To_Be_Considered__c,Inquiry__r.Financing__c 
                     FROM Office_Meeting__c  
                     WHERE id =: officeMeetngId 
                     ];
    return meeting ;
    }
    
}