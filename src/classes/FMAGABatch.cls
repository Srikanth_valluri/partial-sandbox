/*-------------------------------------------------------------------------------------------------
Description: Batch to create tasks for different AGA meetings
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 29-12-2018       | Lochana Rajput   | 1. Added logic to create FM Cases
2.0     | 30-12-2018       | Lochana Rajput   | 1. Added logic to create FM Cases and tasks
=============================================================================================================================
*/
public class FMAGABatch implements Database.Batchable<sObject> {

	public Database.QueryLocator start(Database.BatchableContext BC) {
		Date today = Date.today();
		List<String> lstMeetingNames = new List<String>();
		lstMeetingNames = Label.AGAMeetingTypelabels.split(',');
		String query = 'SELECT Id,Property__c, ';
		query += ' Meeting_Date__c,Meeting_Time__c,Meeting_Venue__c,Type_of_Meeting__c';
		query += ' FROM AGA_Schedule__c ';
		query += ' WHERE Property__c != NULL';
		query += ' AND Task_Creation_Date__c =: today';
		query += ' AND Type_of_Meeting__c IN : lstMeetingNames';
		system.debug('batch records'+Database.Query(query));
		return Database.getQueryLocator(query);
	}

   	public void execute(Database.BatchableContext BC, List<AGA_Schedule__c> scope) {
		List<FM_Case__c> lstFMCases = new List<FM_Case__c>();
		Set<ID> AGAIds = new Set<ID>();
		Set<String> meetingTypes = new Set<String>();
        List<Task> lstTasks = new List<Task>();
		Set<Id> propertyIds = new Set<Id>();
		Map<Id, Id> mapPropertyId_FMUserId = new Map<Id, Id>();
		Map<Id, String> mapPropertyId_FMUserRole = new Map<Id, String>();
		//Get existing cases for schedules; when meeting date updated

		Id devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('AGA Process').getRecordTypeId();
		for(AGA_Schedule__c obj : scope) {
			AGAIds.add(obj.ID);
			propertyIds.add(obj.Property__c);

		}
		System.debug('====AGAIds==='+AGAIds);
		Map<Id, List<FM_Case__c>> mapProperty_FMCases = new Map<Id, List<FM_Case__c>>();
		for(AGA_Schedule__c obj : [SELECT Id, Property__c,Type_of_Meeting__c
									FROM AGA_Schedule__c
									WHERE Id NOT IN (SELECT AGA_Schedule__c FROM FM_Case__c
													WHERE RecordType.Name ='AGA Process'
													AND AGA_Schedule__c IN : AGAIds)
									AND ID IN: AGAIds]) {
			// lstFMCases.add(new FM_Case__c(RecordTypeId=devRecordTypeId,
			// 	Property__c= obj.Property__c,
			// 	Meeting_Type__c = obj.Type_of_Meeting__c,
            //     AGA_Schedule__c = obj.Id,
            //     Status__c = 'Submitted'
			// 	));
				meetingTypes.add(obj.Type_of_Meeting__c);
				if(mapProperty_FMCases.containsKey(obj.Property__c)) {
					mapProperty_FMCases.get(obj.Property__c).add(new FM_Case__c(RecordTypeId=devRecordTypeId,
					Property__c= obj.Property__c,
					Meeting_Type__c = obj.Type_of_Meeting__c,
	                AGA_Schedule__c = obj.Id,
	                Status__c = 'Submitted'
					));
				}
				else {
					mapProperty_FMCases.put(obj.Property__c, new FM_Case__c[] {new FM_Case__c(RecordTypeId=devRecordTypeId,
						Property__c= obj.Property__c,
						Meeting_Type__c = obj.Type_of_Meeting__c,
		                AGA_Schedule__c = obj.Id,
		                Status__c = 'Submitted'
					)});
				}
		}

		Map<String, Decimal> mapMeetingType_EscalationDays = new Map<String, Decimal>();
		for(FM_Process_Escalation_Matrix__mdt objMDT : [SELECT Escalate_After_in_hours__c,
                                   Role__c,
                                   MasterLabel
                              FROM FM_Process_Escalation_Matrix__mdt
                              WHERE MasterLabel IN: meetingTypes]) {
			mapMeetingType_EscalationDays.put(objMDT.MasterLabel, objMDT.Escalate_After_in_hours__c);
		}//for
		System.debug('====mapMeetingType_EscalationDays=====' + mapMeetingType_EscalationDays);
		Map<String, String> mapPropertyId_PropertyDirectorEmail = new Map<String, String>();
		for(Location__c obj : [SELECT Id,Property_Name__c,
								(SELECT FM_User__c,FM_User__r.Email,FM_Role__c from FM_Users__r
								WHERE (FM_Role__c like '%Property Manager%'
									OR FM_Role__c = 'Property Director'))
							   FROM Location__c
							   WHERE Recordtype.Name = 'Building'
							   AND Property_Name__c IN: propertyIds]) {
			for(FM_User__c objUser : obj.FM_Users__r) {
				if(objUser.FM_Role__c.containsIgnoreCase('Property Manager')) {
	 			   mapPropertyId_FMUserId.put(obj.Property_Name__c, objUser.FM_User__c);
	 			   mapPropertyId_FMUserRole.put(obj.Property_Name__c, objUser.FM_Role__c);
	 		   }
			   if(objUser.FM_Role__c.containsIgnoreCase('Property Director')) {
	 			   mapPropertyId_PropertyDirectorEmail.put(obj.Property_Name__c, objUser.FM_User__r.Email);
	 		   }
			}//for
	   	}
		for(Id propertyId : propertyIds) {
			if(mapProperty_FMCases.containsKey(propertyId)) {
				for(FM_Case__c obj : mapProperty_FMCases.get(propertyId)) {
					if(mapPropertyId_PropertyDirectorEmail.containsKey(propertyId)) {
						obj.Property_Director_Email__c = mapPropertyId_PropertyDirectorEmail.get(propertyId);
					}
					System.debug('====obj.Meeting_Type__c=====' + obj.Meeting_Type__c);
					if(mapMeetingType_EscalationDays.containsKey(obj.Meeting_Type__c)) {
						obj.Escalation_Date__c = Date.Today().addDays((Integer)mapMeetingType_EscalationDays.get(obj.Meeting_Type__c));
					}
				}//for
				lstFMCases.addAll(mapProperty_FMCases.get(propertyId));
			}

		}//

	   	insert lstFMCases;
        System.debug('====CASES==='+lstFMCases);
        for(FM_Case__c obj: lstFMCases) {
			//Task to send notice
            Task noticeTaskObj = new Task();
			noticeTaskObj.whatId = obj.Id;
			noticeTaskObj.Subject = Label.FM_SendNoticeTaskLabel;
			noticeTaskObj.Status = 'Not Started';
			noticeTaskObj.Priority = 'Normal';
			noticeTaskObj.Process_Name__c = 'AGA Process';

			noticeTaskObj.ActivityDate = Date.today().addDays(5);

			//Task to book venue for meeting
            Task objTask = new Task();
			objTask.whatId = obj.Id;
			objTask.Subject = Label.FM_BookVenueTaskLabel;
			objTask.Status = 'Not Started';
			objTask.Priority = 'Normal';
			objTask.Process_Name__c = 'AGA Process';

			objTask.ActivityDate = Date.today().addDays(5);
			if(mapPropertyId_FMUserRole.containsKey(obj.Property__c)) {
				objTask.Assigned_User__c = mapPropertyId_FMUserRole.get(obj.Property__c);
				noticeTaskObj.Assigned_User__c = mapPropertyId_FMUserRole.get(obj.Property__c);
			}
			else if(mapPropertyId_FMUserId.containsKey(obj.Property__c)) {
				objTask.OwnerID = mapPropertyId_FMUserId.get(obj.Property__c);
				noticeTaskObj.OwnerID = mapPropertyId_FMUserId.get(obj.Property__c);
			}
            lstTasks.add(objTask);
			lstTasks.add(noticeTaskObj);
        }
        insert lstTasks;
	}

	public void finish(Database.BatchableContext BC) {

	}

}