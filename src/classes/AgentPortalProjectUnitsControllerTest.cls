// Created By :  Naresh Kaneriya
@isTest
public class AgentPortalProjectUnitsControllerTest {
     public static Contact adminContact;
    public static User portalUser;
    public static Account adminAccount;
    public static User portalOnlyAgent;
    
    static void init(){

        adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
        insert adminAccount;
        
        adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
        insert adminContact;
        
        Contact agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
        insert agentContact;
        
        portalUser = InitialiseTestData.getPortalUser('test@test.com', adminContact.Id, 'Admin');
        portalOnlyAgent = InitialiseTestData.getPortalUser('test1@test.com', agentContact.Id, 'Agent');
        
        System.runAs(portalUser){
            Property__c property = InitialiseTestData.insertProperties();
            InitialiseTestData.createInventoryUser(property);
            ApexPages.currentPage().getParameters().put('Id',property.ID);
        }
    }
    
    @isTest static void showProjects(){
        Test.startTest();
        init();
        System.runAs(portalUser){
            AgentPortalProjectUnitsController  damacProjectController = new AgentPortalProjectUnitsController();
        }
        Test.stopTest();
    }
    
    @isTest static void filterInventories(){
        Test.startTest();
        init();
        
        System.runAs(portalUser){
            ApexPages.currentPage().getParameters().put('locationNamesSelected','\'Dubai\'');
            ApexPages.currentPage().getParameters().put('projectTypeSelected','\'Residential\'');
            ApexPages.currentPage().getParameters().put('BedroomsSelected','\'1\'');
            ApexPages.currentPage().getParameters().put('Location','\'1\'');
            ApexPages.currentPage().getParameters().put('sfdc.tabName','\'1\'');
            ApexPages.currentPage().getParameters().put('MinPrice','12333');
            ApexPages.currentPage().getParameters().put('MaxPrice','343223');
            ApexPages.currentPage().getParameters().put('UnitBedrooms','1');
            ApexPages.currentPage().getParameters().put('Bedrooms','1');
             ApexPages.currentPage().getParameters().put('Type','Residential');
            
            
            AgentPortalProjectUnitsController damacProjectController = new AgentPortalProjectUnitsController();
            damacProjectController.filterInventories();
            
        }
        
        Test.stopTest();
    }
    
    @isTest static void projectQueryParam(){
        Test.startTest();
        init();
        System.runAs(portalUser){
         ApexPages.currentPage().getParameters().put('Bedrooms','1');
             ApexPages.currentPage().getParameters().put('UnitType','Residential');
            ApexPages.currentPage().getParameters().put('floorPkgName','test.pkg');
              ApexPages.currentPage().getParameters().put('FloorPackageType','Package');
            ApexPages.currentPage().getParameters().put('VillaType','High-rise');
             
             ApexPages.currentPage().getParameters().put('Location','Dubai');
              ApexPages.currentPage().getParameters().put('MasterDeveloper','Damac Hills');
              ApexPages.currentPage().getParameters().put('ACDDate','Q2 2018');
               ApexPages.currentPage().getParameters().put('BedroomType','1BR');
              ApexPages.currentPage().getParameters().put('UnitVillaType','High-rise');
            ApexPages.currentPage().getParameters().put('propertyStatus','Ready');
            ApexPages.currentPage().getParameters().put('floorPkgType','Package');

             Apexpages.currentPage().getParameters().put('MarketingName','DAMAC HILLS - ARTESIA');
            Apexpages.currentPage().getParameters().put('District','Dubai Land');
           
           AgentPortalProjectUnitsController damacProjectController2 = new AgentPortalProjectUnitsController();
        }
        Test.stopTest();
    }
}