/**************************************************************************************************
* Name               : AP_AgentPortalNewLaunches
* Description        : An apex page controller for AgentPortalOffice
* Created Date       : Pratiksha Narvekar
* Created By         : 31/07/2017
* Last Modified Date :
* Last Modified By   : 
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR                    DATE
* 1.0         Pratiksha Narvekar        31/07/2017
**************************************************************************************************/

public without sharing class AP_AgentPortalNewLaunches{

    /**************************************************************************************************
            Variables used in the class
    **************************************************************************************************/
    public List<String> projectType                                     {set;get;} // show type filter in VF 
    public String isEligibleForVAT                                      {get;set;}
    public String showCommission                                        {get;set;}
    public list<String> bedroomsforDashboard                             {get;set;}                
    public list<String> bedrooms                                        {set;get;}
    public List<Inventory__c> inventories                               {set;get;}
    public String inventoryPrefix                                       {set;get;}
    public Inventory_Area_range__c inventoryAreaRange                   {set;get;}
    public NSIBPM__Service_Request__c vatSR                             {set;get;}
    public List<Inventory_Price__c> priceRangeValues                    {set;get;}
    public String areaSqftMin                                           {set;get;}
    public String areaSqftMax                                           {set;get;}
    public String priceMin                                              {set;get;}
    public String priceMax                                              {set;get;}
    public Decimal priceMinRange                                        {set;get;}
    public Decimal priceMaxRange                                        {set;get;}
    public Inventory_Price_Configuration__c inventoryPriceConfiguration {set;get;}
    public String propertyId                                            {set;get;}
    public String userId                                                {set;get;}
    private String inventoryIdQuery                                     {set;get;}
    private Set<Id> InventoryIDs;
    private String generalInventoryQuery;
    public String bedroomsSelectedFromURL                               {set;get;}
    public String typeSelectedFromURL                                   {set;get;}
    public String locationSelectedFromURL                               {set;get;}
    public String propertyName                                          {set;get;}
    public string tabName                                               {set;get;}
    public List<String> floorPackageType                                {set;get;}
    public String floorPackageTypeSelected                              {set;get;}
    public String floorPackageNameSelected                              {set;get;}
    public List<String> floorPackageName                                {set;get;}
    public List<String> villaType                                       {set;get;}
    public List<String> propertyStatus                                  {set;get;}
    public String villaTypeSelected                                     {set;get;}
    public String propertyStatusSelected                                {set;get;}
    public String DisplayCommission                                     {set;get;}
    private String MarketingName;
    private String District;
    public String vatRegDate                                            {get; set;}
    public String vatRegNumber                                          {get; set;}
    public String vatBusinessType                                       {get; set;}
    public String sCountries                                            {get; set;}
    public Boolean isDubai                                              {get; set;}
    public String vatPageID                                             {get; set;}
    public String accId                                                 {get; set;}
    public transient Attachment att;
    public Boolean userAccess                                           {get; set;}
    public String strSelectedLanguage                                   {get; set;}
    public List<Selectoption> listLanguages                             {set; get;}


    //public map<String,AgentPortalCommissionAccess__C> AgentCommission = AgentPortalCommissionAccess__C.getall();
    /**************************************************************************************************
    Method:         DamacProjectController
    Description:    Constructor executing model of the class
    **************************************************************************************************/
    public AP_AgentPortalNewLaunches() {
        user objUser = [Select Id,contactid,contact.account.name from User where id=:userinfo.getuserid()];
        string name = objUser.contact.account.name;
        system.debug('name'+name);
        if(String.isNotBlank(name)){
            if(name.equals(Label.POP_Account_access)) {
                userAccess = true;
            } else {
                userAccess = false;
            }
        }
        if( apexpages.currentPage().getParameters().get('langCode') != null){
                strSelectedLanguage = apexpages.currentPage().getParameters().get('langCode');
        }
        vatPageID = '';
        accId = '';
        showCommission = 'none';
        userId = UserInfo.getUserId();
        List<User> listUser = new List<User>();
        listUser = [select id,contactid from user where id=: userId Limit 1];
        List<Contact> listConn = new List<Contact>();
        listConn = [ SELECT ID,
                            Agency_City_Of_Incorporation__c,
                            Account.Agency_Type__c,
                            AccountId,
                            Eligible_For_VAT__c,
                            Owner__c,
                            Portal_Administrator__c
                       FROM Contact
                      WHERE Id= :listUser[0].ContactId
                      LIMIT 1];
        List<NSIBPM__SR_Template__c> srTemplateList = new List<NSIBPM__SR_Template__c>();
        srTemplateList = [SELECT Id 
                            FROM NSIBPM__SR_Template__c 
                           WHERE NSIBPM__SR_RecordType_API_Name__c = 'VAT_Update'
                           LIMIT 1
        ];
        Contact currentContact = new Contact();
        for(Contact objCon : listConn){
            currentContact = objCon;
            accId = objCon.AccountId;
            if(objCon.Owner__c == true || objCon.Portal_Administrator__c == true ){
                showCommission = 'block';
            }
            else{
                showCommission = 'none';
            }
            if (objCon.Eligible_For_VAT__c) {
                isEligibleForVAT = 'block';
                vatSR = new NSIBPM__Service_Request__c();
                vatSR.Agency_Type__c = objCon.Account.Agency_Type__c;
                vatSR.Agency__c = objCon.AccountId;
                vatSR.NSIBPM__Customer__c = objCon.AccountId;
                vatSR.Mode_of_Payment__c = '';
                if (!srTemplateList.isEmpty() && srTemplateList != null) {
                    vatSR.NSIBPM__SR_Template__c = srTemplateList[0].Id; 
                }
                vatRegDate = '';
                vatRegNumber = '';
                vatBusinessType = '';
            } else {
                isEligibleForVAT = 'none';
            }
            if (objCon.Agency_City_Of_Incorporation__c == 'Dubai') {
                isDubai = true;
            } else {
                isDubai = false;
            }
        }
     /*   listLanguages = new List<SelectOption>();
        for(AP_Languages__mdt objLanguage : [SELECT DeveloperName,
                                                    MasterLabel
                                            FROM AP_Languages__mdt])
        {
            if(objLanguage.MasterLabel != 'English') {
                listLanguages.add(new selectOption(objLanguage.DeveloperName,objLanguage.MasterLabel));
            }
        }
        listLanguages.add(0,new selectOption('en_US','  English'));*/
        vatPageID = [SELECT Id FROM Page_Flow__c WHERE Name=: 'VAT Agent Update' LIMIT 1].Id;

        projectType = AgentPortalUtilityQueryManager.getAllProjectTypes();
        bedroomsfordashboard = AgentPortalUtilityQueryManager.getAllBedRooms();
        floorPackageType = new List<String>();
        villaType = new List<String>();
        propertyStatus = new List<String>();
        String ProfileID = userInfo.getProfileID();
        
        //DisplayCommission = AgentCommission.containsKey(ProfileID) ? 'Block':'None';
        /*for(AgentPortalCommissionAccess__C objA : AgentCommission){
            if(objA.Name == ProfileID){
                DisplayCommission = 'block';
            }
            else{
                DisplayCommission = 'none';
            }
        }*/
        floorPackageType = DamacUtility.getPicklistValue(Inventory__c.SobjectType,'Floor_Package_Type__c');
        getVillaType();
        getPropertyStatus();

        if(ApexPages.currentPage().getParameters().containsKey('floorPkgName') && null != ApexPages.currentPage().getParameters().get('floorPkgName'))
            floorPackageNameSelected = ApexPages.currentPage().getParameters().get('floorPkgName');
        else
            floorPackageNameSelected = ' ';


        inventories = new List<Inventory__c>();
        inventoryPrefix = DamacUtility.getObjectPrefix('Inventory__c');

        if(ApexPages.currentPage().getParameters().containsKey('Location') && ApexPages.currentPage().getParameters().get('Location') != ''
            && ApexPages.currentPage().getParameters().get('Location') != 'All'){
            locationSelectedFromURL = String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('Location'));
        }

         if(ApexPages.currentPage().getParameters().containsKey('sfdc.tabName')){
            tabName = String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('sfdc.tabName'));
        }

        inventoryAreaRange = Inventory_Area_range__c.getValues(DAMAC_Constants.INVENTORY_AREA_RANGE);
        //priceRangeValues = AgentPortalUtilityQueryManager.getPriceRange();
        //inventoryPriceConfiguration = Inventory_Price_Configuration__c.getValues(DAMAC_Constants.INVENTORY_STEP);

        inventoryIdQuery ='SELECT Inventory__c from Inventory_User__c where Inventory__r.Status__c = \'RELEASED\' AND Inventory__r.Unit_Location__c != null'+
                                     ' AND Inventory__r.Address__c != null AND End_Date__c >= TODAY AND User__c=\''+UserInfo.getUserId()+'\'';

        generalInventoryQuery = 'SELECT Id from Inventory__c where Status__c=\'RELEASED\' AND Unit_Location__c != null'+
                                ' AND Address__c != null AND Is_Assigned__c = false';

        InventoryIDs = AgentPortalUtilityQueryManager.getInventoryIDs(inventoryIdQuery);



        //get all inventories from the general inventory pool
        Set<ID> generalInventoryId = AgentPortalUtilityQueryManager.getAllGeneralInventories(generalInventoryQuery );
        InventoryIDs.addAll(generalInventoryId);

        propertyId = null;
        if(ApexPages.currentPage().getParameters().containsKey('Id')){
            propertyId = ApexPages.currentPage().getParameters().get('Id');
        }

        MarketingName = '';
        District = '';

        if(ApexPages.currentPage().getParameters().containsKey('District') &&
             ApexPages.currentPage().getParameters().get('District') != '' &&
             null != ApexPages.currentPage().getParameters().get('District')
                ){
            District = ApexPages.currentPage().getParameters().get('District');
        }

         if(ApexPages.currentPage().getParameters().containsKey('MarketingName') &&
             ApexPages.currentPage().getParameters().get('MarketingName') != '' &&
             null != ApexPages.currentPage().getParameters().get('MarketingName')
                ){
            MarketingName =  ApexPages.currentPage().getParameters().get('MarketingName');
         }

        Map<String,Decimal> minMaxPriceMap = AgentPortalUtilityQueryManager.getMinMaxPrice(propertyId);

        if(ApexPages.currentPage().getParameters().containsKey('MinPrice') && null != ApexPages.currentPage().getParameters().get('MinPrice')){
            priceMin = ApexPages.currentPage().getParameters().get('MinPrice');
        }

        if(ApexPages.currentPage().getParameters().containsKey('MaxPrice') && null != ApexPages.currentPage().getParameters().get('MaxPrice')){
            priceMax = ApexPages.currentPage().getParameters().get('MaxPrice');
        }

        if( null !=  minMaxPriceMap){
            priceMinRange = minMaxPriceMap.get('min');
            priceMaxRange =  minMaxPriceMap.get('max');
            priceMin = (priceMin ==  null)?String.valueOf(priceMinRange):priceMin;
            priceMax= (priceMax == null)?String.valueOf(priceMaxRange):priceMax;
        }


        floorPackageName = new List<String>();
        floorPackageName = AgentPortalUtilityQueryManager.getAllFloorPackageName(InventoryIDs,propertyId);
        getAllInventories();

    }

    public List<SelectOption> getCountries() {
        List<SelectOption> options = new List<SelectOption>();
        /*Schema.DescribeFieldResult fieldResult =NSIBPM__Service_Request__c.Tax_Registration_Country__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple) {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        } */
        options.add(new SelectOption('United Arab Emirates','United Arab Emirates'));
        System.debug('options AFTER>>>>>>>> ' + options); 
        return options;
    }

    public void setCountries(String sCountries) {
        this.sCountries= sCountries;
    }

    public void submitVatSR() {
        
        System.debug('vatSR;>>>>>>>> ' + vatSR);
        System.debug('vatBusinessType;>>>>>>>> ' + vatBusinessType);

        
        if (vatSR != null && vatRegDate != '' 
            && vatRegNumber != '' && sCountries != '') {
            vatSR.UAE_Tax_Registration_Number__c = vatRegNumber;
            vatSR.VAT_Registration_Certificate_Date__c = Date.valueOf(vatRegDate);
            vatSR.Tax_Registration_Country__c = vatBusinessType;
            vatSR.RecordTypeId = 
                Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Agent Update').getRecordTypeId();
            insert vatSR;
            for (NSIBPM__SR_Status__c srStatus : [SELECT Id 
                                                    FROM NSIBPM__SR_Status__c 
                                                    WHERE Name = 'Submitted']
            ) {
                vatSR.NSIBPM__Internal_SR_Status__c = srStatus.Id;
                vatSR.NSIBPM__External_SR_Status__c = srStatus.Id;
                vatSR.NSIBPM__Submitted_Date__c = system.today();
                vatSR.NSIBPM__Submitted_DateTime__c = system.now();
            }
            //uploadDoc();
            update vatSR;
            
            System.debug('vatSR AFTER>>>>>>>> ' + vatSR);
            System.debug('vatSRID AFTER>>>>>>>> ' + vatSR.Id);
        }
    }


     Public Attachment getatt()
    {
        att= new Attachment();
        return att;
    }
   
    public void uploadDoc()
    {
        ID contactId = [Select contactid from User where id =: Userinfo.getUserid()].contactId;
        ID AccID  = [Select AccountID from Contact where id =: contactId].AccountId;
        //String accid = System.currentPagereference().getParameters().get('id');
        //PageReference pageRef = System.currentPagereference();
        //System.Debug('Current Page'+pageRef);
        System.Debug('Attachment Account Id'+AccID);
        Attachment a = new Attachment(parentId = AccID, name=att.name, body = att.body,contentType=att.contentType);
         
         /* insert the attachment */
         insert a; 
         submitVatSR();
        //return (pageRef);
    } 



    public void getAllInventories(){

        if(null != propertyId){
            String condition= 'Id,Property_Name_2__c,Property__r.Property_Name__c FROM Inventory__c WHERE Unit_Type__c != null AND Unit_Location__c != null '+
                        ' AND Id IN :InventoryIDs AND Property__r.Id = \''+propertyId+'\''+
                ' AND Status__c = \'Released\' ';


            if(ApexPages.currentPage().getParameters().containsKey('UnitBedrooms')){

                if(ApexPages.currentPage().getParameters().get('UnitBedrooms') != ''
                    && ApexPages.currentPage().getParameters().get('UnitBedrooms') != 'All')
                    bedroomsSelectedFromURL = ApexPages.currentPage().getParameters().get('UnitBedrooms');

            }else if(ApexPages.currentPage().getParameters().containsKey('Bedrooms')){
                if(ApexPages.currentPage().getParameters().get('Bedrooms') != ''
                    && ApexPages.currentPage().getParameters().get('Bedrooms') != 'All')
                    bedroomsSelectedFromURL = ApexPages.currentPage().getParameters().get('Bedrooms');
            }

            if(null != bedroomsSelectedFromURL && bedroomsSelectedFromURL != ''){
                List<String> bedroomsSelectedLists = bedroomsSelectedFromURL.split(',');

                if(null != bedroomsSelectedLists){
                  condition+= ' AND IPMS_Bedrooms__c IN (\''+String.join(bedroomsSelectedLists,'\',\'')+'\')';
                 }
            }

            if(ApexPages.currentPage().getParameters().containsKey('UnitType')){
                if(ApexPages.currentPage().getParameters().get('Type') != ''
                && ApexPages.currentPage().getParameters().get('Type') != 'All')
               typeSelectedFromURL= ApexPages.currentPage().getParameters().get('UnitType');

            }else if(ApexPages.currentPage().getParameters().containsKey('Type')){
                if(ApexPages.currentPage().getParameters().get('Type') != ''
                && ApexPages.currentPage().getParameters().get('Type') != 'All')
                typeSelectedFromURL= ApexPages.currentPage().getParameters().get('Type');
            }

            if(null != typeSelectedFromURL && typeSelectedFromURL != ''){
                List<String> typeSelectedLists= typeSelectedFromURL.split(',');

                if(null != typeSelectedLists){
                   condition+= ' AND Unit_Type__c IN (\''+String.join(typeSelectedLists,'\',\'')+'\')';
                }
            }

            if(String.isNotEmpty(priceMin)){
                condition += ' AND ( List_Price_calc__c >=' + priceMin +' OR Special_Price_calc__c >= ' +priceMin + ') ';
            }

            if(String.isNotEmpty(priceMax)){
                condition += ' AND ( List_Price_calc__c <='+ priceMax +' OR Special_Price_calc__c <= ' +priceMax+ ') ';
            }

             if(String.isNotEmpty(floorPackageNameSelected.trim())){
                condition += ' AND Floor_Package_Name__c =\''+floorPackageNameSelected.trim()+'\'';
            }

          if(ApexPages.currentPage().getParameters().containsKey('floorPkgType') &&
            null != ApexPages.currentPage().getParameters().get('floorPkgType') &&
            '' != ApexPages.currentPage().getParameters().get('floorPkgType') &&
            'All' != ApexPages.currentPage().getParameters().get('floorPkgType')){
                floorPackageTypeSelected= ApexPages.currentPage().getParameters().get('floorPkgType');
                List<String> floorPackageTypeLst= floorPackageTypeSelected.split(',');

                if(null != floorPackageTypeLst){
                   condition+= ' AND Floor_Package_Type__c IN (\''+String.join(floorPackageTypeLst,'\',\'')+'\')';
                }
          }

            if(ApexPages.currentPage().getParameters().containsKey('villaType') &&
            null != ApexPages.currentPage().getParameters().get('villaType') &&
            '' != ApexPages.currentPage().getParameters().get('villaType') &&
            'All' != ApexPages.currentPage().getParameters().get('villaType')){
                villaTypeSelected= ApexPages.currentPage().getParameters().get('villaType');
                List<String> villaTypeLst= villaTypeSelected.split(',');

                if(null != villaTypeLst){
                   condition+= ' AND  Project_Category__c IN (\''+String.join(villaTypeLst,'\',\'')+'\')';
                }
              }

              if(ApexPages.currentPage().getParameters().containsKey('propertyStatus') &&
                null != ApexPages.currentPage().getParameters().get('propertyStatus') &&
                '' != ApexPages.currentPage().getParameters().get('propertyStatus') &&
                'All' != ApexPages.currentPage().getParameters().get('propertyStatus')){
                    propertyStatusSelected= ApexPages.currentPage().getParameters().get('propertyStatus');
                    List<String> propertyStatusLst= propertyStatusSelected.split(',');

                    if(null != propertyStatusLst){
                       condition+= ' AND Property_Status__c IN (\''+String.join(propertyStatusLst,'\',\'')+'\')';
                    }
              }

              if(String.isNotEmpty(MarketingName) && null != MarketingName){
                condition += ' AND Marketing_Name__c =\''+MarketingName+'\'';
              }

              if(String.isNotEmpty(District) && null != District){
                condition += ' AND Property__r.District__c =\''+District+'\'';
              }


            condition += ' LIMIT 10000';

            inventories = AgentPortalUtilityQueryManager.getInventoryList(condition,InventoryIDs);

            if(null != inventories && inventories.size()>0){
                propertyName = inventories[0].Property__r.Property_Name__c;
            }
        }
    }

    public void filterInventories(){

        if(null != propertyId){
             String condition = 'Id FROM Inventory__c WHERE Unit_Type__c != null AND Unit_Location__c != null AND Id IN :InventoryIDs AND Property__r.Id = \''+propertyId+'\''+
                    ' AND Status__c = \'Released\'';

             String BedroomsSelected = ApexPages.currentPage().getParameters().get('BedroomsSelected');
             String TypeSelected = ApexPages.currentPage().getParameters().get('TypeSelected');

             //BedroomsSelected Selected is blank or All then dont include it in query
            if(null != BedroomsSelected && '\'\'' != BedroomsSelected && BedroomsSelected != '\'All\'' ){
                condition += ' AND IPMS_Bedrooms__c IN ('+BedroomsSelected+')';
            }

            if(null != TypeSelected && '\'\'' != TypeSelected && TypeSelected != '\'All\''){
                condition += ' AND Unit_Type__c IN '+
                                '('+TypeSelected+')';
            }

            /*if(String.isNotEmpty(areaSqftMin) && String.isNotEmpty(areaSqftMax)){
                condition += ' AND Area_Sqft_2__c>='+areaSqftMin+' AND Area_Sqft_2__c<='+areaSqftMax;
            }*/

            if(String.isNotEmpty(priceMin)){
                condition += ' AND ( List_Price_calc__c >=' + priceMin +' OR Special_Price_calc__c >= ' +priceMin + ') ';
            }

            if(String.isNotEmpty(priceMax)){
                condition += ' AND ( List_Price_calc__c <='+ priceMax +' OR Special_Price_calc__c <= ' +priceMax+ ') ';
            }

            if(String.isNotEmpty(floorPackageNameSelected)){
                condition += ' AND Floor_Package_Name__c =\''+floorPackageNameSelected+'\'';
            }

            if(String.isNotEmpty(floorPackageTypeSelected) && null != floorPackageTypeSelected && '\'\'' != floorPackageTypeSelected && floorPackageTypeSelected != '\'All\'')
                condition += 'AND Floor_Package_Type__c IN '+
                                '('+floorPackageTypeSelected+')';

             if(String.isNotEmpty(villaTypeSelected) && null != villaTypeSelected && '\'\'' != villaTypeSelected && villaTypeSelected != '\'All\'')
                condition += 'AND Project_Category__c IN '+
                                '('+villaTypeSelected+')';

             if(String.isNotEmpty(propertyStatusSelected) && null != propertyStatusSelected && '\'\'' != propertyStatusSelected && propertyStatusSelected != '\'All\'')
                condition += 'AND Property_Status__c IN '+
                                '('+propertyStatusSelected+')';

             if(String.isNotEmpty(MarketingName) && null != MarketingName){
                condition += ' AND Marketing_Name__c =\''+MarketingName+'\'';
              }

             if(String.isNotEmpty(District) && null != District){
                condition += ' AND Property__r.District__c =\''+District+'\'';
              }


            inventories = AgentPortalUtilityQueryManager.getInventoryList(condition+' LIMIT 10000',InventoryIDs);

        }

    }

    /*********************************************************************************************
    * @Description : Get all villas and property status                                         *
    * @Params      : void                                                                       *
    * @Return      : void                                                                       *
    *********************************************************************************************/
    public void getVillaType(){

        for(AggregateResult thisInventory:[SELECT Project_Category__c FROM Inventory__c where Project_Category__c != NULL
                                        AND Status__c = 'Released' GROUP BY Project_Category__c ]){
            villaType.add((String)thisInventory.get('Project_Category__c'));
        }
    }

    public void getPropertyStatus(){
        for(AggregateResult thisInventory:[SELECT Property_Status__c FROM Inventory__c where Property_Status__c != null
                                        AND Status__c = 'Released' GROUP BY Property_Status__c]){
            propertyStatus.add((String)thisInventory.get('Property_Status__c'));
        }
    }
    /*
   public static List<Inventory__c> getNewLaunches(){

      List<Inventory__c> inventory = new List<Inventory__c>();
      inventory =[select Id,
                         Selling_Price__c,
                         Price_Per_Sqft__c,
                         List_Price__c,
                         Marketing_Name_Doc__c,
                         Marketing_Name_Doc__r.Marketing_Name__c,
                         Marketing_Name_Doc__r.Logo_URL__c
                  FROM Inventory__c
                  WHERE Price__c !=''
                  AND List_Price__c != null
                  AND Price_Per_Sqft__c != null
                  AND Property_Status__c ='Ready'
                  Order by LastModifiedDate
                  Limit 10];
     for(Inventory__c objInt : inventory){
        System.debug('...test url..'+objInt.Marketing_Name_Doc__r.Logo_URL__c);
     }
     List<Marketing_Documents__c> objM = new List<Marketing_Documents__c> ();
     objM =[select id,Marketing_Name__c,Logo_URL__c from Marketing_Documents__c limit 10];
      System.debug('...objM...'+objM);
      return inventory;

    }
    */
    public  List<NewPopularSearches> getPopularSearchesStudio(){
        List<NewPopularSearches> listPopularSearches = new List<NewPopularSearches>();
        List<Inventory__c> Listinventory = new List<Inventory__c>();
        Listinventory =[select Id,
                               Unit_Type__c,
                               Marketing_Name_Doc__r.Logo_URL__c,
                               Address__r.City__c
                       FROM Inventory__c
                       WHERE Property_Status__c ='Ready'
                       AND Unit_Type__c ='Studio' Limit 1 ];
         for(Inventory__c objInt : Listinventory){
            NewPopularSearches objNewPopularSearches = new NewPopularSearches();
            objNewPopularSearches.StudioURL = objInt.Marketing_Name_Doc__r.Logo_URL__c;
            listPopularSearches.add(objNewPopularSearches);
         }
        return listPopularSearches;
    }
        public  List<NewPopularSearches> getPopularSearchesDubaiReadyApt(){
        List<NewPopularSearches> listPopularSearches = new List<NewPopularSearches>();
        List<Inventory__c> Listinventory = new List<Inventory__c>();
        Listinventory =[select Id,
                           Unit_Type__c,
                           Marketing_Name_Doc__r.Logo_URL__c,
                           Address__r.City__c
                   FROM Inventory__c
                   WHERE Property_Status__c ='Ready'
                   AND Unit_Type__c Like '%Apartments%'
                   AND Property_City__c ='Dubai'
                   Limit 1];
         for(Inventory__c objInt : Listinventory){
            NewPopularSearches objNewPopularSearches = new NewPopularSearches();
            objNewPopularSearches.StudioURL = objInt.Marketing_Name_Doc__r.Logo_URL__c;
            listPopularSearches.add(objNewPopularSearches);
         }
        return listPopularSearches;
    }
        public  List<NewPopularSearches> getPopularSearchesDubaiReadyVillas(){
        List<NewPopularSearches> listPopularSearches = new List<NewPopularSearches>();
        List<Inventory__c> Listinventory = new List<Inventory__c>();
        Listinventory =[select Id,
                           Unit_Type__c,
                           Marketing_Name_Doc__r.Logo_URL__c,
                           Address__r.City__c
                   FROM Inventory__c
                   WHERE Property_Status__c ='Ready'
                   AND Unit_Type__c = 'Villas'
                   AND Property_City__c ='Dubai'
                   Limit 1
                  // AND Unit_Type__c ='Studio' Limit 1
                   ];
         for(Inventory__c objInt : Listinventory){
            NewPopularSearches objNewPopularSearches = new NewPopularSearches();
            objNewPopularSearches.StudioURL = objInt.Marketing_Name_Doc__r.Logo_URL__c;
            listPopularSearches.add(objNewPopularSearches);
         }
        return listPopularSearches;
    }
        public  List<NewPopularSearches> getPopularSearchesBusinessBay(){
        List<NewPopularSearches> listPopularSearches = new List<NewPopularSearches>();
        List<Inventory__c> Listinventory = new List<Inventory__c>();
        Listinventory =[select Id,
                           Unit_Type__c,
                           Marketing_Name_Doc__r.Logo_URL__c,
                           Address__r.City__c
                   FROM Inventory__c
                   WHERE Property_Status__c ='Ready'
                   AND Bedroom_Type__c = '1 BR'
                   AND Master_Developer_EN__c Like '%Business Bay%'
                   Limit 1
                  // AND Unit_Type__c ='Studio' Limit 1
                   ];
         for(Inventory__c objInt : Listinventory){
            NewPopularSearches objNewPopularSearches = new NewPopularSearches();
            objNewPopularSearches.StudioURL = objInt.Marketing_Name_Doc__r.Logo_URL__c;
            listPopularSearches.add(objNewPopularSearches);
         }
        return listPopularSearches;
    }
        public  List<NewPopularSearches> getPopularSearchesTwoBHKApt(){
        List<NewPopularSearches> listPopularSearches = new List<NewPopularSearches>();
        List<Inventory__c> Listinventory = new List<Inventory__c>();
        Listinventory =[select Id,
                           Unit_Type__c,
                           Marketing_Name_Doc__r.Logo_URL__c,
                           Address__r.City__c
                   FROM Inventory__c
                   WHERE Property_Status__c ='Ready'
                   AND Bedroom_Type__c = '2 BR'
                   AND  Unit_Type__c ='Apartments'
                   Limit 1
                  // AND Unit_Type__c ='Studio' Limit 1
                   ];
         for(Inventory__c objInt : Listinventory){
            NewPopularSearches objNewPopularSearches = new NewPopularSearches();
            objNewPopularSearches.StudioURL = objInt.Marketing_Name_Doc__r.Logo_URL__c;
            listPopularSearches.add(objNewPopularSearches);
         }
        return listPopularSearches;
    }
        public  List<NewPopularSearches> getPopularSearchesOneMillionVillas(){
        List<NewPopularSearches> listPopularSearches = new List<NewPopularSearches>();
        List<Inventory__c> Listinventory = new List<Inventory__c>();
        Listinventory =[select Id,
                           Unit_Type__c,
                           Marketing_Name_Doc__r.Logo_URL__c,
                           Address__r.City__c
                   FROM Inventory__c
                   WHERE Property_Status__c ='Ready'
                   AND Unit_Type__c = 'Hotel Apartments'
                   AND Property_City__c ='Dubai'
                   Limit 1
                  // AND Unit_Type__c ='Studio' Limit 1
                   ];
         for(Inventory__c objInt : Listinventory){
            NewPopularSearches objNewPopularSearches = new NewPopularSearches();
            objNewPopularSearches.StudioURL = objInt.Marketing_Name_Doc__r.Logo_URL__c;
            listPopularSearches.add(objNewPopularSearches);
         }
        return listPopularSearches;
    }

/*
     public static List<NewLaunchesInventory> getNewLaunchesClone(){
      List<NewLaunchesInventory> onjlist = new List<NewLaunchesInventory>();
      List<Inventory__c> inventory = new List<Inventory__c>();
      inventory =[select Id,
                         Selling_Price__c,
                         Price_Per_Sqft__c,
                         List_Price__c,
                         Marketing_Name_Doc__c,
                         Marketing_Name_Doc__r.Marketing_Name__c,
                         Marketing_Name_Doc__r.Logo_URL__c
                  FROM Inventory__c
                  WHERE Price__c !=''
                  AND List_Price__c != null
                  AND Price_Per_Sqft__c != null
                  AND Property_Status__c ='Ready'
                  Order by LastModifiedDate
                  Limit 10];
     for(Inventory__c objInt : inventory){
         NewLaunchesInventory objNewLaunch = new NewLaunchesInventory();
         objNewLaunch.IntID = objInt.Id;
         objNewLaunch.SellingPrice = objInt.Selling_Price__c;
         objNewLaunch.PPSqt = objInt.Price_Per_Sqft__c;
         objNewLaunch.ListPrice = objInt.List_Price__c;
         objNewLaunch.MarketingName = objInt.Marketing_Name_Doc__r.Marketing_Name__c;
         objNewLaunch.URL = objInt.Marketing_Name_Doc__r.Logo_URL__c;
         onjlist.add(objNewLaunch);
     }
      return onjlist;

    }
*/
    public List<NewLaunches__mdt> getNewLaunches() {
        List<NewLaunches__mdt> newLaunches = new List<NewLaunches__mdt>();
        newLaunches =
            [
                SELECT
                    Pic__c,
                    Title__c,
                    PDF_Url__c,
                    Description__c
                FROM
                    NewLaunches__mdt
            ];
        return newLaunches;
    }

    public List<Agents_Offers_For_You__mdt> getAgentsOfferForYou() {
        List<Agents_Offers_For_You__mdt> agentOffers = new List<Agents_Offers_For_You__mdt>();
        agentOffers =
            [
                SELECT
                    Pic__c,
                    Title__c,
                    PDF_Url__c,
                    Description__c
                FROM
                    Agents_Offers_For_You__mdt
            ];
        return agentOffers;
    }

/*
    public class NewLaunchesInventory{
        public Decimal SellingPrice{get;set;}
        public ID IntID{get;set;}
        public Decimal PPSqt{get;set;}
        public Decimal ListPrice{get;set;}
        public string MarketingName{get;set;}
        public string URL{get;set;}
    }
*/

    public class NewPopularSearches{
        public string StudioURL{get;set;}
        public string DubaiReadyApartment{get;set;}
        public string ReadyVillas{get;set;}
        public string BusinessBay{get;set;}
        public string Apartment{get;set;}
        public string OneMillionVillas{get;set;}
        public string HotelRooms{get;set;}
        public string HotelApartments{get;set;}
    }

  
}