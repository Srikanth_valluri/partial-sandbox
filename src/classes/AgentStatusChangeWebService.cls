/**
 * Agent Status Change web-service
 */
@RestResource(urlMapping='/agentStatus/*')
global class AgentStatusChangeWebService {

    /**
     * Method to process post request
     */
    @HttpPost
    global static void doPost() {
        AgentStatusChangeLogic.processAgentStatusChangeRequest();
    }
}