/****************************************************************************************************************
* Name                  : AgentToPCMappingController                                                            *
* Visualforce Page      : AgentToPCMapping                                                                      *
* Test Class            : AgentToPCMappingController_Test                                                       *
* Description           : This class map the agents to PCs                                                      *
* Created By            : NSI - Sivasankar K                                                                    *
* Created Date          : 06/Feb/2017                                                                           *
* Last Modified Date    :                                                                                       *
* Last Modified By      :                                                                                       *
* ------------------------------------------------------------------------------------------------------------  *
* ChangeHistroy     VERSION     AUTHOR                     DATE             Description                         *
* CH00              1.0         NSI - Sivasankar K         06/Feb/2017      Initial developmen                  *
* CH01              2.0                                                                                         *
* CH02              3.0                                                                                         *
*****************************************************************************************************************/
    public without sharing class AgentToPCMappingController {

    public Integer noOfRecToDisplay     {get; set;} // Number of records to be displayed in page.
    public Integer total                {get; set;} // total records
    public List<User> allPCUsers        {get; set;} //Users to display in table
    public String selUserName           {get; set;} // Selected User Name
    public String selSM                 {get; set;} // Selected Sales Manager
    public String selHOS                {get; set;} // Selected Head of Sales
    public String selDOS                {get; set;} // Selected Director of Sales
    public String msgType               {get; set;}
    public String isDisplay             {get; set;}
    public String message               {get; set;}
    public String strAgencyContactID    {get; set;}


    public String agencyID {
        get{
            if(agencyID == null || agencyID == '')
                agencyID = ApexPages.currentPage().getParameters().get('id');
            return agencyID;
        }
        set;
    }

    public Set<ID> existingPCs {get;set;}
    public String existingPCIDS {get;set;}

    @testVisible private Set<ID> userIDs = new Set<ID>();
    @testVisible private String conditionsString = '';
    @testVisible private integer totalRecs = 0;
    @testVisible private integer offsetVal = 0;
    @testVisible private integer LimitSize = 5;

    public AgentToPCMappingController(ApexPages.StandardController controller){
        existingPCIDS = '';
        if (message == '' || message == null) {
            isDisplay = 'hide';
        }
        msgType = '';
        message = '';
        strAgencyContactID = '';
        strAgencyContactID = ApexPages.currentPage().getParameters().get('conId');
        existingPCUsers();
        total = totalRecs = getTotalRecords();
        allPCUsers = getallPCUsers();
        selUserName = selSM = selHOS = selDOS = null;
    }

    public void searchUsers() {
        messageBlock('hide', '', '');
        allPCUsers.clear();
        conditionsString = '';
        userIDs = new Set<ID>();

        if(String.isNotBlank(selSM)){
            userIDs.add(selSM);
        }
        if(String.isNotBlank(selHOS)){
            userIDs.add(selHOS);
        }
        if(String.isNotBlank(selDOS)){
            userIDs.add(selDOS);
        }
        /*conditionsString = ((String.isNotBlank(selSM)) ? 'Name LIKE \'%'+selSM+'%\' AND Profile.Name =\''+System.Label.SM_Profile+'\'':'');
        conditionsString = ((String.isNotBlank(selHOS)) ? 'Name LIKE \'%'+selHOS+'%\' AND Profile.Name =\''+System.Label.HOS_Profile+'\'':conditionsString);
        conditionsString = ((String.isNotBlank(selDOS)) ? 'Name LIKE \'%'+selDOS+'%\' AND Profile.Name =\''+System.Label.DOS_Profile+'\'':conditionsString);
        conditionsString = ((String.isNotBlank(selSM) && String.isNotBlank(selHOS) && String.isNotBlank(selDOS)) ? '(Name LIKE \'%'+selSM+'%\' OR Name LIKE \'%'+selHOS+'%\' OR Name LIKE \'%'+selDOS+'%\') AND Profile.Name IN(\''+System.Label.SM_Profile+'\',\''+System.Label.DOS_Profile+'\',\''+System.Label.HOS_Profile+'\')':conditionsString);
        conditionsString = ((String.isNotBlank(selSM) && String.isNotBlank(selHOS)) ? '(Name LIKE \'%'+selSM+'%\' OR Name LIKE \'%'+selHOS+'%\') AND Profile.Name IN(\''+System.Label.SM_Profile+'\',\''+System.Label.HOS_Profile+'\')':conditionsString);
        conditionsString = ((String.isNotBlank(selHOS) && String.isNotBlank(selDOS)) ? '(Name LIKE \'%'+selHOS+'%\' OR Name LIKE \'%'+selDOS+'%\') AND Profile.Name IN(\''+System.Label.DOS_Profile+'\',\''+System.Label.HOS_Profile+'\')':conditionsString);
        conditionsString = ((String.isNotBlank(selSM) && String.isNotBlank(selDOS)) ? '(Name LIKE \'%'+selSM+'%\' OR Name LIKE \'%'+selDOS+'%\') AND Profile.Name IN(\''+System.Label.SM_Profile+'\',\''+System.Label.DOS_Profile+'\')':conditionsString);

        System.debug('conditionsString ===>'+conditionsString);

        if(String.isNotBlank(conditionsString))
            for(User us: Database.query('SELECT ID FROM USER WHERE IsActive = True AND '+conditionsString)){
                userIDs.add(us.id);
            }

        */
        conditionsString = ((String.isNotBlank(selUserName) ? 'ID =\''+selUserName+'\'' : ''));
        System.debug('userIDs == Search ====> '+userIDs);
        if(!userIDs.isEmpty() && userIDs.size() > 0){
            conditionsString += (String.isNotBlank(conditionsString) ? ' AND ' :'')+'(ManagerId IN: userIDs OR Manager.ManagerId IN: userIDs OR Manager.Manager.ManagerId IN: userIDs OR Manager.Manager.Manager.ManagerId IN: userIDs)';
        }
        System.debug('conditionsString == Search ====> '+conditionsString);

        limitsize = noOfRecToDisplay;
        total = totalRecs = getTotalRecords();
        allPCUsers = getallPCUsers();
    }

    /************************************************************************************************
    * @Description : this method will update the page with new page size                            *
    * @Params      :                                                                              *
    * @Return      : void                                                                           *
    *************************************************************************************************/
    public void updatePage() {
        allPCUsers.clear();
        limitsize = noOfRecToDisplay;
        allPCUsers = getallPCUsers();
    }

    /************************************************************************************************
    * @Description : this method will update the page with new page size                            *
    * @Params      :                                                                                *
    * @Return      : void                                                                           *
    *************************************************************************************************/
    public void addPCsToAgency() {
        messageBlock('hide', '', '');

        // *****BLOCK FOR ADD PC BUTTON ON CONTACT LAYOUT (ONLY FOR CORPORTE CONTACTS)*****
        if (String.isNotBlank(strAgencyContactID)) {
            User userPCRec;
            Integer selectedPCs = 0;
            for(User pcUserObj :  allPCUsers) {
                if (pcUserObj.IsActive) {
                    selectedPCs ++;
                    userPCRec = pcUserObj;
                }
            }
            if (userPCRec != null && selectedPCs == 1) {
                Contact contactRecord = new Contact();
                contactRecord.Id = strAgencyContactID;
                contactRecord.OwnerId = userPCRec.Id;
                try {
                    System.debug('contactRecord='+contactRecord);
                    update contactRecord;
                    System.debug('contactRecord UPDATED==='+contactRecord);
                    Contact updatedContact = new Contact();
                    updatedContact = [ SELECT Id
                                            , OwnerId
                                            , Owner.Name
                                            , Owner.Email
                                            , AccountId
                                         FROM Contact
                                        WHERE Id = :strAgencyContactID];
                    List<Agency_PC__c> conAgencyPCList = new List<Agency_PC__c>();
                    conAgencyPCList = [SELECT Id
                                            , Name
                                            , Agency__c
                                            , User__c
                                            , Contact__c
                                         FROM Agency_PC__c
                                        WHERE Contact__c = :contactRecord.Id];
                    if (conAgencyPCList.isEmpty()) {
                        Agency_PC__c newAgencyPC = new Agency_PC__c();
                        newAgencyPC.Name = updatedContact.Owner.Name;
                        newAgencyPC.User__c = updatedContact.OwnerId;
                        newAgencyPC.PC_Email__c = updatedContact.Owner.Email;
                        newAgencyPC.Contact__c = updatedContact.Id;
                        newAgencyPC.Agency__c = updatedContact.AccountId;
                        insert newAgencyPC;
                        System.debug('newAgencyPC Created==='+newAgencyPC);
                    }
                    messageBlock('show', 'success', ' PC updated sucessfully. ');
                } Catch(Exception ex){
                    messageBlock('show', 'error', ex.getMessage());
                }
            } else {
                // *****Selected PC more that 1 for Corporate Contact*****
                messageBlock('show', 'error', ' Please select only one PC. ');
            }
            //reinitialize the values.
            reinitializePage();
        } else if (String.isBlank(strAgencyContactID)) {
            // *****BLOCK FOR ADD PC BUTTON ON ACCOUNT LAYOUT (ONLY FOR INDIVIDUAL ACCOUNT)*****
            List<Agency_PC__c> agPCs = new List<Agency_PC__c>();
            try{
                // All users selected on page
                Contact personAccContact = [SELECT Id FROM Contact WHERE AccountID = :agencyID];
                for(User pc :  allPCUsers){
                    if(pc.IsActive) {
                        Agency_PC__c agencyPC = new Agency_PC__c();
                        agencyPC.Agency__c = agencyID;
                        agencyPC.User__c = pc.id;
                        agencyPC.Name = pc.Name;
                        if (personAccContact != null) {
                            agencyPC.Contact__c = personAccContact.Id;
                        }
                        agPCs.add(agencyPC);
                    }
                }
                if(!agPCs.isEmpty() && agPCs.size() > 0) {
                    insert agPCs;
                    messageBlock('show', 'success', agPCs.size() + ' PCs added sucessfully.');
                }
                //reinitialize the values.
                reinitializePage();
            } Catch(Exception ex) {
                messageBlock('show', 'error', ex.getMessage());
            }
        }
    }

    /************************************************************************************************
    * @Description : Method to reinitialize the page and values                                     *
    * @Params      :                                                                                *
    * @Return      : void                                                                           *
    *************************************************************************************************/
    public void reinitializePage() {
        selUserName = selSM = selDOS = selHOS = '';
        existingPCIDS = '';
        existingPCUsers();//call existing users
        limitsize = noOfRecToDisplay;
        total = totalRecs = getTotalRecords();
        allPCUsers = getallPCUsers();
    }

    /************************************************************************************************
    * @Description : Method to Display the success/Error messages                                   *
                     depending on their visiblity, Value and Type                                   *
    * @Params      : pDisplay > show/hide, pType > success/error, pMessage > custom message         *
    * @Return      : void                                                                           *
    *************************************************************************************************/
    public void messageBlock(String pDisplay, String pType, String pMessage) {
        isDisplay = pDisplay;
        msgType = pType;
        message = pMessage;
    }

    /************************************************************************************************
    * @Description : Get all existing PC Users of the Agency                                        *
    * @Params      :                                                                              *
    * @Return      : void                                                                           *
    *************************************************************************************************/
    public void existingPCUsers(){
        existingPCs = new Set<ID>();
        existingPCIDS = '';
        //if (String.isBlank(strAgencyContactID)) {
            for(Agency_PC__c aPC : new List<Agency_PC__c>([SELECT User__c FROM Agency_PC__c WHERE Agency__c =: agencyID])){
                existingPCs.add(aPC.User__c);
                existingPCIDS +='\\\''+aPC.User__c+'\\\',';
            }
        /*} else {
            for(Agency_PC__c aPC : new List<Agency_PC__c>([SELECT User__c FROM Agency_PC__c
                                                            WHERE Agency__c =: agencyID
                                                              AND Id NOT IN ])){
                existingPCs.add(aPC.User__c);
                existingPCIDS +='\\\''+aPC.User__c+'\\\',';
            }
        }*/
        System.debug('existingPCIDS=== '+existingPCIDS);
        if(String.isNotBlank(existingPCIDS)){
            existingPCIDS = existingPCIDS.subStringBeforeLast(',');
            existingPCIDS = '('+existingPCIDS+')';
        }
        System.debug('existingPCIDS=== '+existingPCIDS);
    }

    /************************************************************************************************
    * @Description : Get the Total records to be displayed for the search conditions                *
    * @Params      :                                                                              *
    * @Return      : void                                                                           *
    *************************************************************************************************/
    @testVisible private Integer getTotalRecords(){
        System.debug('conditionsString == getRecords ====> '+conditionsString);
        List<User> totalUsers = new List<User>();
        try{
            System.debug('existingPCs=== '+existingPCs);
            /*totalUsers = Database.query('SELECT Id FROM User WHERE ID NOT IN: existingPCs AND IsActive = True AND Profile.Name=\'Property Consultant\''+(String.isNotBlank(conditionsString) ? ' AND '+conditionsString : ''));*/
            String userQuery = '';
            if (String.isBlank(strAgencyContactID)) {
                userQuery = ' SELECT ID,Name,IsActive,UserRole.Name FROM USER '
                          + ' WHERE ID NOT IN: existingPCs AND IsActive = True '
                          + ' AND Profile.Name=\'Property Consultant\''
                          + (String.isNotBlank(conditionsString) ? ' AND ' + conditionsString:'')
                          + ' LIMIT:limitsize OFFSET:offsetVal';
            } else {
                userQuery = ' SELECT ID,Name,IsActive,UserRole.Name FROM USER '
                          + ' WHERE IsActive = True AND Profile.Name=\'Property Consultant\''
                          + (String.isNotBlank(conditionsString) ? ' AND ' + conditionsString:'')
                          + ' LIMIT:limitsize OFFSET:offsetVal';
            }
            totalUsers = Database.query(userQuery);
        }Catch(Exception ex){
            messageBlock('show', 'error', ex.getMessage());
        }
        return totalUsers.size();
    }

    /************************************************************************************************
    * @Description : this method will get all the PC users based on search condition                *
    * @Params      :                                                                              *
    * @Return      : void                                                                           *
    *************************************************************************************************/
    @testVisible private List<User> getallPCUsers(){
        List<User> pcUsers = new List<User>();
        try{

            System.debug('conditionsString == getAllUsers ====> '+conditionsString);
            /*for(User us : Database.query('SELECT ID,Name,IsActive,UserRole.Name FROM USER WHERE ID NOT IN: existingPCs AND IsActive = True AND Profile.Name=\'Property Consultant\''+(String.isNotBlank(conditionsString) ? ' AND '+conditionsString:'')+' LIMIT:limitsize OFFSET:offsetVal')){*/
            String userQuery = '';
            if (String.isBlank(strAgencyContactID)) {
                userQuery = ' SELECT ID,Name,IsActive,UserRole.Name FROM USER '
                          + ' WHERE ID NOT IN: existingPCs AND IsActive = True '
                          + ' AND Profile.Name=\'Property Consultant\''
                          + (String.isNotBlank(conditionsString) ? ' AND ' + conditionsString:'')
                          + ' LIMIT:limitsize OFFSET:offsetVal';
            } else {
                userQuery = ' SELECT ID,Name,IsActive,UserRole.Name FROM USER '
                          + ' WHERE IsActive = True AND Profile.Name=\'Property Consultant\''
                          + (String.isNotBlank(conditionsString) ? ' AND ' + conditionsString:'')
                          + ' LIMIT:limitsize OFFSET:offsetVal';
            }
            for(User us : Database.query(userQuery)) {
                System.debug('us===> '+us.Id);
                us.IsActive = false;
                pcUsers.add(us);
            }
        }Catch(Exception ex){
            messageBlock('show', 'error', ex.getMessage());
        }
        return pcUsers;
    }

    /************************************************************************************************
    * @Description : this method will set hte offsetVal to display first page records               *
    * @Params      :                                                                              *
    * @Return      : void                                                                           *
    *************************************************************************************************/
    public void Firstbtn() {
        offsetVal = 0;
        allPCUsers = getallPCUsers();
    }

    /************************************************************************************************
    * @Description : this method will set hte offsetVal to display previous page records            *
    * @Params      :                                                                              *
    * @Return      : void                                                                           *
    *************************************************************************************************/
    public void prvbtn() {
        offsetVal = offsetVal-limitsize;
        offsetVal = (offsetVal < 0 ? 0:offsetVal);
        allPCUsers = getallPCUsers();
    }

    /************************************************************************************************
    * @Description : this method will set hte offsetVal to display next page records                *
    * @Params      :                                                                              *
    * @Return      : void                                                                           *
    *************************************************************************************************/
    public void Nxtbtn() {
        offsetVal = offsetVal+limitsize;
        allPCUsers = getallPCUsers();
    }

    /************************************************************************************************
    * @Description : this method will set hte offsetVal to display last page records                *
    * @Params      :                                                                              *
    * @Return      : void                                                                           *
    *************************************************************************************************/
    public void lstbtn() {
        offsetVal= totalrecs - math.mod(totalRecs,LimitSize);
        allPCUsers = getallPCUsers();
    }

    /************************************************************************************************
    * @Description : this method will get checks that hasmore records in current search             *
    * @Params      :                                                                              *
    * @Return      : void                                                                           *
    *************************************************************************************************/
    public Boolean gethasNext() {
        if((offsetVal + LimitSize) > totalRecs)
            return true;
        else
            return false;

    }

    /************************************************************************************************
    * @Description : this method will get checks that previous records in current search            *
    * @Params      :                                                                              *
    * @Return      : Boolean                                                                        *
    *************************************************************************************************/
    public Boolean gethasPrevious() {
        if(offsetVal == 0)
          return true;
        else
          return false;
    }
}