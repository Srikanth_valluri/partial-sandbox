global class NoAttachmentsInPopEmailBatchOnCaseSched implements Schedulable {
    global void execute(SchedulableContext sc) {
        database.executebatch(new NoAttachmentsInPopEmailBatchOnCase(),1);
    }
}