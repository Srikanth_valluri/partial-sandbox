/**********************************************************************************************************************
Description: This API is used for getting the details of all Appointments booked by any portal account user.
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   27-11-2020      | Subin Antony		  | Created initial draft
***********************************************************************************************************************/

@RestResource(urlMapping='/appointment/listAll')
global class DL_GetAllAppointmentsBooked_API {
    public static Map<Integer, String> statusCodeMap;
    static {
        statusCodeMap = new Map<Integer, String>{
            0 => 'Empty List',
            1 => 'Success',
            2 => 'Failure',
            3 => 'Bad Request',
            6 => 'Something went wrong',
            7 => 'Info'
        };
    }
	
    @HttpGet
    global static FinalReturnWrapper fetchAllAppointmentsBooked() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        RestContext.response.addHeader('Content-Type', 'application/json');
        String errorMsg, exceptionMsg, successMsg;
        
        String account_id = req.params.containskey('account_id') ? req.params.get('account_id') : '';
        if(String.isBlank(account_id)){
            errorMsg = 'Please provide account_id.';
            return getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
        }
        
        String bu_id = req.params.containskey('bu_id') ? req.params.get('bu_id') : '';
        if(String.isBlank(bu_id)){
            errorMsg = 'Please select a booking unit for booking appointment.';
            return getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
        }
        
        Account accountRecord;
        try{
            accountRecord = [SELECT Id, Name, isPersonAccount, Email__pc, Email__c, 
                             Primary_CRE__c, Secondary_CRE__c, Tertiary_CRE__c, Primary_Language__c 
                             FROM Account WHERE Id = :account_id];
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
        	exceptionMsg = ex.getMessage();
            accountRecord = NULL;
        }
        if(NULL == accountRecord){
            errorMsg = 'The account_id provided does not match any records.';
            return getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
        }
        
        Set<String> processNameSet = new Set<String>();
        List<Active_Process_on_Portal_Meta__mdt> lstActivePortalProcess = [SELECT id, MasterLabel, CRE_MasterLabel__c, DeveloperName, 
                                                 Appointment_Hr__c, Unique_code_number__c, (SELECT id, MasterLabel, DeveloperName, 
                                                 Unique_code_number__c FROM Sub_Processes_for_Portal__r) 
                                                 FROM Active_Process_on_Portal_Meta__mdt 
                                                 WHERE  CRE_MasterLabel__c != NULL 
                                                 AND CRE_MasterLabel__c NOT IN ('Handover','Rotana','Recovery') 
                                                 ORDER BY MasterLabel];
        if(NULL != lstActivePortalProcess && lstActivePortalProcess.size() > 0) {
            for(Active_Process_on_Portal_Meta__mdt portalAppointmentProcess : lstActivePortalProcess) {
                processNameSet.add(portalAppointmentProcess.CRE_MasterLabel__c);
            }
        }
        
        // query the list of booked appointments(calling list)
        Id appointSchedRecTypID = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Appointment Scheduling').getRecordTypeId();
        List<Calling_List__c> bookedAppointments;
        try{
            if(processNameSet.size() > 0) {
                bookedAppointments = [SELECT id, name, Appointment_Status__c, Service_Type__c, Sub_Purpose__c, Remarks__c, 
                                  Account__c, Assigned_CRE__c, Assigned_CRE__r.name, Appointment_Date__c, 
                                  Appointment_Slot__c, Appointment_Start_DateTime__c, Appointment_End_DateTime__c, 
                                  Appointment__c, Appointment__r.name, Appointment__r.Slots__c, 
                                  Booking_Unit__c, Booking_Unit__r.Unit_Name__c, Booking_Unit__r.Registration_ID__c 
                                  FROM Calling_List__c WHERE recordTypeID = :appointSchedRecTypID 
                                  AND Account__c = :account_id AND Booking_Unit__c = :bu_id 
                                  AND Service_Type__c IN :processNameSet ORDER BY CreatedDate DESC];
            }
            else {
                bookedAppointments = [SELECT id, name, Appointment_Status__c, Service_Type__c, Sub_Purpose__c, Remarks__c, 
                                  Account__c, Assigned_CRE__c, Assigned_CRE__r.name, Appointment_Date__c, 
                                  Appointment_Slot__c, Appointment_Start_DateTime__c, Appointment_End_DateTime__c, 
                                  Appointment__c, Appointment__r.name, Appointment__r.Slots__c, 
                                  Booking_Unit__c, Booking_Unit__r.Unit_Name__c, Booking_Unit__r.Registration_ID__c 
                                  FROM Calling_List__c WHERE recordTypeID = :appointSchedRecTypID 
                                  AND Account__c = :account_id AND Booking_Unit__c = :bu_id ORDER BY CreatedDate DESC];
            }
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
        	exceptionMsg = ex.getMessage();
            bookedAppointments = NULL;
        }
        if(NULL == bookedAppointments || bookedAppointments.size() < 1) {
            errorMsg = 'Query returned no or empty list of appointment records.';
            FinalReturnWrapper responseWrapper = getErrorResponse(1, statusCodeMap.get(1), errorMsg, exceptionMsg); /* special case, even if empty result, sending success response */
            
            cls_data responseData = new cls_data();
            responseData.past_bookings = new List<AppointmentDetailsWrap>(); /* empty result */
            responseData.upcoming_bookings = new List<AppointmentDetailsWrap>(); /* empty result */
            responseWrapper.data = responseData;
            
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
        	return responseWrapper;
        }
        else {
            AppointmentDetailsWrap[] previousAppointments = new List<AppointmentDetailsWrap>();
            AppointmentDetailsWrap[] upcommingAppointments = new List<AppointmentDetailsWrap>();
            for(Calling_List__c appointmentCL : bookedAppointments) {
                if(NULL != appointmentCL.Appointment_Status__c && 
                appointmentCL.Appointment_Status__c.equalsIgnoreCase('Cancelled')) {
                    continue; /* not including cancelled appointments */
                }
                
                AppointmentDetailsWrap appointmentDetails = new AppointmentDetailsWrap();
                
                appointmentDetails.appointment_id = appointmentCL.id;
                appointmentDetails.appointment_name = appointmentCL.name; /* appointmentCL.Appointment__r.name; */
                appointmentDetails.appointment_status = (NULL != appointmentCL.Appointment_Status__c && 
                                                         appointmentCL.Appointment_Status__c.equalsIgnoreCase('Confirmed')) ? 
                    'Booked' : appointmentCL.Appointment_Status__c;
                appointmentDetails.assigned_cre_name = appointmentCL.Assigned_CRE__r.name;
                appointmentDetails.purpose = appointmentCL.Service_Type__c;
                appointmentDetails.sub_purpose = appointmentCL.Sub_Purpose__c;
                appointmentDetails.remarks = appointmentCL.Remarks__c;
            	DateTime appointmentDateTime = (NULL != appointmentCL.Appointment_Start_DateTime__c) ? 
                    appointmentCL.Appointment_Start_DateTime__c : 
                	DateTime.newInstance(appointmentCL.Appointment_Date__c, Time.newInstance(9, 0, 0, 0));
                appointmentDetails.appointment_date = (NULL != appointmentDateTime) ? appointmentDateTime.format('yyyy-MM-dd') : NULL;
                appointmentDetails.appointment_slot = formatSlotString(appointmentCL.Appointment__r.Slots__c);
                appointmentDetails.booking_unit_name = appointmentCL.Booking_Unit__r.Unit_Name__c;
                appointmentDetails.booking_unit_id = appointmentCL.Booking_Unit__c;
                
                if(NULL != appointmentDateTime && appointmentDateTime > DateTime.NOW()) {
                    upcommingAppointments.add(appointmentDetails);
                } else {
                    previousAppointments.add(appointmentDetails);
                }
            }
            
            FinalReturnWrapper responseWrapper = new FinalReturnWrapper();
        	cls_meta_data responseMetaData = new cls_meta_data();
            responseMetaData.status_code = 1;
            responseMetaData.title = statusCodeMap.get(1);
            responseMetaData.message = 'Successfully fetched booked appointments for the given portal user account.';
            responseWrapper.meta_data = responseMetaData;
            
            cls_data responseData = new cls_data();
            responseData.past_bookings = previousAppointments;
            responseData.upcoming_bookings = upcommingAppointments;
            responseWrapper.data = responseData;
            
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
        	return responseWrapper;
        }
    }
    
    private static FinalReturnWrapper getErrorResponse(Integer statusCode, String title, 
    String responseMessage, String devMessage) {
        FinalReturnWrapper responseWrapper = new FinalReturnWrapper();
        cls_meta_data responseMetaData = new cls_meta_data();
        responseMetaData.status_code = statusCode;
        responseMetaData.title = title;
        responseMetaData.message = responseMessage;
        responseMetaData.developer_message = devMessage;
        responseWrapper.meta_data = responseMetaData;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
        return responseWrapper;
    }
    
    public static String formatSlotString(String slotString_24H) {
        String formattedString = slotString_24H;
        try {
            String[] times = slotString_24H.split('-');
            String startTimeString = times[0].trim();
            String endTimeString = times[1].trim();
            
            String[] startTimeParts = startTimeString.split(':');
            Integer startHour = Integer.valueOf(startTimeParts[0].trim());
            Integer startMinute = Integer.valueOf(startTimeParts[1].trim());
            String startTimeFormatted = (startHour > 12 ? (startHour - 12) : startHour) + ':' + 
                (startMinute < 10 ? ('0'+startMinute) : (''+startMinute))  + (startHour >= 12 ? 'PM' : 'AM');
            
            String[] endTimeParts = endTimeString.split(':');
            Integer endHour = Integer.valueOf(endTimeParts[0].trim());
            Integer endMinute = Integer.valueOf(endTimeParts[1].trim());
            String endTimeFormatted = (endHour > 12 ? (endHour - 12) : endHour) + ':' + 
                (endMinute < 10 ? ('0'+endMinute) : (''+endMinute)) + (endHour >= 12 ? 'PM' : 'AM');
            
            formattedString = startTimeFormatted + ' - ' + endTimeFormatted;
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
        }
        return formattedString;
    }
    
    global class FinalReturnWrapper {
        public cls_meta_data meta_data;
        public cls_data data;
    }
    
    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message; 
    }
    
    public class cls_data {
        public appointmentDetailsWrap[] past_bookings;
        public appointmentDetailsWrap[] upcoming_bookings;
    }
    
    public class AppointmentDetailsWrap {
        public String appointment_id;
        public String appointment_name;
        public String appointment_status;
        public String assigned_cre_name;
        public String purpose;
        public String sub_purpose;
        public String remarks;
        public String appointment_date;
        public String appointment_slot;
        public String booking_unit_name;
        public String booking_unit_id;
    }
}