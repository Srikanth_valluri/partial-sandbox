/*------------------------------------------------------------------------------
Description: Controller class for FMAccessCardComponent

================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
--------------------------------------------------------------------------------
v1.0     | 04-10-2018       | Lochana Rajput   | 1. Initial draft
------------------------------------------------------------------------------------
v2.0     | 24-12-2018       | Lochana Rajput   | 1. Added role for master community
================================================================================
*/
public without sharing virtual class FMAccessCardComponentController {
    public FM_Case__c objFMCase{get;set;}
    public Integer noOfRequDocs{get;set;}
    public Account objAcc;
    public String strDocBody{get;set;}
    public String strDocName{get;set;}
    public list<SR_Attachments__c> uploadedDocuments{get;set;}
    public Booking_Unit__c objBU{get;set;}
    public String strAccountId{get;set;}
    public String strSelectedUnit{get;set;}
    public String strSRType{get;set;}
    public Location__c objLocation{get;set;}
    public String strFMCaseId{get;set;}
    public Boolean isEditable{get;set;}
    public Boolean showDocPanel{get;set;}
    public List<String> selectedValuesOfAccessCard{get;set;}
    public Boolean allowInitiation{get;set;}
    public list<FM_Documents__mdt> lstDocuments { get; set; }
    public SelectOption[] valueListOfAccessCard { get; set; }
    public String docIdToDelete{get;set;}
    public Set<String> uploadedDocLst;
    private map<String, FM_Documents__mdt> mapProcessDocuments ;


    protected FMAccessCardComponentController(Boolean shouldCall) {}

    public FMAccessCardComponentController() {
        noOfRequDocs=0;
        uploadedDocLst = new Set<String>();
        uploadedDocuments = new List<SR_Attachments__c>();
        objFMCase = new FM_Case__c();
        selectedValuesOfAccessCard = new List<String>();
        objBU = new Booking_Unit__c();
        objAcc = new Account();
        objLocation = new Location__c();
        showDocPanel = false;
        lstDocuments = new List<FM_Documents__mdt>();

        Decimal minimumOutstandingCharge=0;
        isEditable = allowInitiation=true;
        strFMCaseId = ApexPages.currentPage().getParameters().get('Id');
        if(String.isBlank(strFMCaseId)){
            strAccountId = ApexPages.currentPage().getParameters().get('AccountId');
            strSelectedUnit = ApexPages.currentPage().getParameters().get('UnitId');
            strSRType = ApexPages.currentPage().getParameters().get('SRType');
            objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().
                                    get('Request For Access Card').getRecordTypeId();
        }
        else {//Get existing case details
            getCaseDetails();
        }
        //get account details
        if(String.isNotBlank(strAccountId)) {
            List<Account> lstAcc = new List<Account>();
            lstAcc = [SELECT Id, Party_ID__c,Name,Mobile_Phone__pc,Email__pc,IsPersonAccount
                    FROM Account
                    WHERE Id =: strAccountId
                    AND IsPersonAccount = TRUE];
            if(lstAcc.size() >0) {
                objAcc = lstAcc[0];
                objFMCase.Tenant_email__c = objAcc.Email__pc;
                objFMCase.Tenant__c = objAcc.Id;
            }
        }
        //Case initiation
        Id devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().
                            get('Request For Access Card').getRecordTypeId();
        objFMCase.Request_Type_DeveloperName__c = strSRType;
        System.debug('===strSRType==='+strAccountId);
        objFMCase.RecordTypeId = devRecordTypeId;
        objFMCase.Origin__c='Walk-In';
        objFMCase.Tenant__c = strAccountId ;
        objFMCase.Booking_Unit__c = strSelectedUnit ;
        objFMCase.Request_Type__c = 'Request For Access Card';
        System.debug('>>>>--objFMCase.Request_Type_DeveloperName__c---:'+objFMCase.Request_Type_DeveloperName__c);
        FM_process__mdt fmProcessMetadata=[select Minimum_Outstanding_Charge__c
                                        from FM_process__mdt
                                        where DeveloperName =:objFMCase.Request_Type_DeveloperName__c];
        if(fmProcessMetadata != NULL){
            minimumOutstandingCharge = fmProcessMetadata.Minimum_Outstanding_Charge__c != NULL ? fmProcessMetadata.Minimum_Outstanding_Charge__c : 0;
        }
        if(String.isNotBlank(strSelectedUnit)) {
            List<Booking_Unit__c> lstBU = new List<Booking_Unit__c>();
            lstBU = [SELECT Id,Registration_ID__c,Inventory__r.Property_Code__c,
                            Booking__r.Account__c,Property_City__c ,
                            Booking__r.Account__r.Name,
                            Unit_Name__c,Property_Name__c
                    FROM Booking_Unit__c
                    WHERE ID =: strSelectedUnit];

            if(lstBU.size() > 0) {
                objBU = lstBU[0];
                // objFMCase.Account__c = objBU.Booking__r.Account__c;
                if(String.isNotBlank(lstBU[0].Unit_Name__c)) {
                    String buildingName = lstBU[0].Unit_Name__c.split('/')[0];
                    List<Location__c> lstBuildings = new List<Location__c>();
                    lstBuildings = [SELECT Id,Name,Property_Name__c,Community_Name__c,
                                        Access_Card_Replacement_Fee__c,New_Access_Card_Fee__c,
                                        (SELECT FM_User__c,FM_User__r.Email,FM_Role__c,FM_User__r.Name
                                        FROM FM_Users__r)
                                    FROM Location__c
                                    WHERE Name =: buildingName
                                    AND RecordType.DeveloperName = 'Building' LIMIT 1];
                    if(lstBuildings.size() > 0) {
                        objLocation = lstBuildings[0];
                        if(objLocation.FM_Users__r.size() > 0) {
                            for(FM_User__c obj :objLocation.FM_Users__r) {
                                if(obj.FM_Role__c == 'FM Admin' || obj.FM_Role__c == 'Master Community Admin') {
                                    objFMCase.Admin__c = obj.FM_User__c;
                                }
                            }//for
                        }
                    }
                }
            }
            if( objBU != NULL ) {
                try {
                    objFMCase.Account__c = objBU.Booking__r.Account__c;
                    FmIpmsRestServices.DueInvoicesResult objResponse =FmIpmsRestServices.getDueInvoices( objBU.Registration_Id__c,'',
                    objBU.Inventory__r.Property_Code__c);
                  system.debug('== objResult =='+objResponse);
                    if(objResponse != NULL) {
                        objFMCase.Outstanding_service_charges__c = !String.isBlank( objResponse.totalDueAmount ) ? objResponse.totalDueAmount : '0';
                        allowInitiation = Decimal.valueof(objFMCase.Outstanding_service_charges__c) <= minimumOutstandingCharge
                                            ? true : false;
                    }
                }
                catch(Exception e) {
                    system.debug( '----Exception while fetching oustanding balance----'+e.getMessage() );
                }
            }
        }
        getfieldValues();
        getDocuments();//Get documents to upload from metadata
        if(String.isNotBlank(strFMCaseId)){
            showDocPanel = lstDocuments.size() > 0 ? TRUE : FALSE;
        }
    }//constructor

    public void getCaseDetails() {
        objFMCase = [SELECT Id,Request_Type_DeveloperName__c,Tenant__c,
                            Type_of_Access_Card__c,
                            Name,
                            Outstanding_service_charges__c,
                            Access_Card_Permission__c,
                            Booking_Unit__c,
                            (SELECT Type__c,Id,Name, View__c,Attachment_URL__c
                            FROM Documents__r)
                    FROM FM_Case__c
                    WHERE Id =: strFMCaseId];
        uploadedDocuments.addAll(objFMCase.Documents__r);
        getUploadedDocuments();
        if(String.isNotBlank(objFMCase.Access_Card_Permission__c)) {
            selectedValuesOfAccessCard.addAll(objFMCase.Access_Card_Permission__c.split(';'));
        }
        strAccountId = objFMCase.Tenant__c;
        strSelectedUnit = objFMCase.Booking_Unit__c;
        strSRType = objFMCase.Request_Type_DeveloperName__c;
    }

    private void getUploadedDocuments() {
        uploadedDocLst = new Set<String>();
        for(SR_Attachments__c obj : uploadedDocuments) {
            uploadedDocLst.add(obj.Name);
        }
    }
    public void createCaseShowUploadDoc(){
        // createMethod();
        if(objFMCase != NULL) {
            System.debug('>>>---objFMCase.Request_Type__c--- : '+objFMCase.Request_Type__c);
            upsert objFMCase;
        }
        System.debug('objFMCase-----'+objFMCase);
        System.debug('objFMCase.id------'+objFMCase.id);
        if( objFMCase != NULL && objFMCase.Id != NULL ) {
            System.debug('--get docs--');
            getDocuments();
        }
        strFMCaseId=objFMCase.id;
        System.debug('strFMCaseId----'+strFMCaseId);
        System.debug('--lstDocuments.size()-- :'+lstDocuments.size());
        showDocPanel = lstDocuments.size() > 0 ? TRUE : FALSE;
        System.debug('--showDocPanel-- :'+showDocPanel);
    }

    public void getDocuments() {
        lstDocuments = new List<FM_Documents__mdt>();
        mapProcessDocuments = new map<String,FM_Documents__mdt>();
        System.debug('uploadedDocLst======' + uploadedDocLst);
        System.debug('objBU++++++' + objBU);
        if(objBU != NULL) {
            for (FM_Documents__mdt objDocMeta : FM_Utility.getDocumentsList( strSRType, objBU.Property_City__c)) {
	            System.debug('objDocMeta = ' + objDocMeta);
	            mapProcessDocuments.put(objDocMeta.Document_Name__c, objDocMeta);
	            if(!uploadedDocLst.contains(objDocMeta.Document_Name__c)) {
	                    lstDocuments.add(objDocMeta);
	            }
	            if(objDocMeta.Mandatory__c) {
	                noOfRequDocs++;
	            }
	        }
	        System.debug('lstDocuments++++++' + lstDocuments);
        }
        // showDocPanel = lstDocuments.size() > 0 ? TRUE : FALSE;
    }

    public PageReference uploadDocument() {
        try{
            String fileType = Apexpages.currentPage().getParameters().get('fileName');
            System.debug('filetype :: '+fileType);
            System.debug('==strDocBody==='+strDocBody);
            System.debug('==strDocName==='+strDocName);
            if(String.isNotBlank(strDocBody) && String.isNotBlank(strDocName)) {
                System.debug('==strDocName==='+strDocName);
                List<UploadMultipleDocController.MultipleDocRequest> lstReq = new List<UploadMultipleDocController.MultipleDocRequest>();
                List<SR_Attachments__c> lstAttachments = new List<SR_Attachments__c>();
                SR_Attachments__c objAtt = new SR_Attachments__c();
                objAtt.Name = strDocName;
                objAtt.Type__c = fileType;
                // objAtt.Type__c = fileNames.size() ==2 ? fileNames[1]: '';
                objAtt.FM_Case__c = objFMCase.Id;
                objAtt.Booking_Unit__c = objBU.Id;
                lstAttachments.add(objAtt);
                System.debug('==lstAttachments==='+lstAttachments);
                UploadMultipleDocController.MultipleDocRequest reqObj = new UploadMultipleDocController.MultipleDocRequest();
                reqObj.category = 'Document';
                reqObj.entityName = 'Damac Service Requests';

                blob objBlob = FM_Utility.extractBody(strDocBody);
                if(objBlob != null){
                    reqObj.base64Binary =  EncodingUtil.base64Encode(objBlob);
                }
                reqObj.fileDescription = strDocName;
                reqObj.fileId = objFMCase.Name + '-'+ String.valueOf(System.currentTimeMillis()) +'.'
                + strDocName.substringAfterLast('.');
                reqObj.fileName = objFMCase.Name + '-'
                            + String.valueOf(System.currentTimeMillis())  +'.'+ strDocName.substringAfterLast('.')+'.'+ fileType;
                reqObj.registrationId = objFMCase.Name;
                reqObj.sourceFileName = 'IPMS-'+objAcc.party_ID__C+'-'+(strDocName);
                reqObj.sourceId = 'IPMS-'+objAcc.party_ID__C+'-'+(strDocName);
                lstReq.add(reqObj);
                System.debug('==strDocName=== :'+strDocName);
                System.debug('==lstReq.size()==='+lstReq.size());
                if(lstReq.size() > 0) {
                    UploadMultipleDocController.data respObj = new UploadMultipleDocController.data();
                    respObj= UploadMultipleDocController.getMultipleDocUrl(lstReq);
                    System.debug('===respObj====' + respObj);
                    if(respObj != NULL && lstReq.size() > 0) {
                        if(respObj.status == 'Exception'){
                            ApexPages.addmessage(new ApexPages.message(
                                ApexPages.severity.Error,respObj.message));
                            return null;
                       }
                       System.debug('===lstAttachment==' + lstAttachments);
                       if(respObj.Data == null || respObj.Data.size()==0){
                           ApexPages.addmessage(new ApexPages.message(
                           ApexPages.severity.Error,'Problems while getting response from document upload'));
                           return null;
                       }
                       System.debug('===lstAttachment==' + lstAttachments);
                       Set<SR_Attachments__c> setSRAttachments_Valid = new Set<SR_Attachments__c>();
                        for(SR_Attachments__c att : lstAttachments) {
                            System.debug('===att==' + att);
                            System.debug('===respObj.data==' + respObj.data);
                            for(UploadMultipleDocController.MultipleDocResponse objData : respObj.data) {
                                System.debug('===objData==' + objData);
                                System.debug('===objData.PARAM_ID==' + objData.PARAM_ID);
                                System.debug('===lstAttachments==' + 'IPMS-'+objAcc.party_ID__C+'-'+(strDocName));
                                if('IPMS-'+objAcc.party_ID__c+'-'+(strDocName) == objData.PARAM_ID){
                                   if(objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url)){
                                       att.Attachment_URL__c = objData.url;
                                       att.FM_Case__c = objFMCase.Id;
                                       setSRAttachments_Valid.add(att);
                                   }
                                   else{
                                       ApexPages.addmessage(new ApexPages.message(
                                       ApexPages.severity.Error,'Problems while getting response from document '+objData.PARAM_ID));
                                       // errorLogger('Problems while getting response from document '+objData.PARAM_ID, '', '');
                                       return null;
                                   }
                               }
                            }
                        }//outer for
                        List<SR_Attachments__c> lstAtt = new List<SR_Attachments__c>();

                        lstAtt.addAll(setSRAttachments_Valid);
                        for(SR_Attachments__c att : lstAtt) {
                            att.FM_Case__c = objFMCase.Id;
                        }
                        upsert lstAtt;
                        uploadedDocuments = [SELECT Type__c,Name, View__c,Attachment_URL__c
                                            from SR_Attachments__c
                                            WHERE FM_Case__c =:objFMCase.Id];
                        System.debug('===uploadedDocuments=='+uploadedDocuments);
                        for(Integer i=0; i<lstDocuments.size(); i++) {
                            if(lstDocuments[i].Document_Name__c == strDocName) {
                                lstDocuments.remove(i);
                                break;
                            }
                        }
                        showDocPanel = lstDocuments.size() > 0 ? TRUE : FALSE;
                        // lstDocuments.remove(lstDocuments.indexOf(fileNames[0]));//removing uploaded doc placeholder
                }//res
            }//req
            }
        }//try
        catch(Exception excp) {
            System.debug('excp==='+excp);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                excp.getMessage() + excp.getLineNumber()));
        }
        strDocBody = '';
        return null;
    }

    public void deleteDocument() {
        try{
            //docIdToDelete contains Document id to delete and its name resp separated by ','
            List<String> tempLst = docIdToDelete.split(',');
            System.debug('tempLst===' + tempLst);
            delete [SELECT Id from SR_Attachments__c where Id=:tempLst[0]];
            uploadedDocuments = new List<SR_Attachments__c>();
            uploadedDocuments = [SELECT Type__c,Name, View__c,Attachment_URL__c
                                FROM SR_Attachments__c
                                WHERE FM_Case__c =:objFMCase.Id];
            // getUploadedDocuments();
            if(mapProcessDocuments.containsKey(tempLst[1])) {
                lstDocuments.add(mapProcessDocuments.get(tempLst[1]));
            }
            showDocPanel = lstDocuments.size() > 0 ? TRUE : FALSE;
            // getDocumentsList();
        }
        catch(System.Exception excp) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                excp.getMessage() + excp.getLineNumber()));
        }
    }
    public void draftSR() {
        if(selectedValuesOfAccessCard != NULL && !selectedValuesOfAccessCard.isEmpty()) {
            objFMCase.Access_Card_Permission__c=String.join(selectedValuesOfAccessCard ,';');
        }
        if(objFMCase.Type_of_Access_Card__c == 'New Request') {
            objFMCase.New_Access_Card_Fee__c = objLocation.New_Access_Card_Fee__c;
        }
        else if(objFMCase.Type_of_Access_Card__c == 'Replacement') {
            objFMCase.Access_Card_Replacement_Fee__c = objLocation.Access_Card_Replacement_Fee__c;
        }
    }
    public virtual PageReference saveAsDraft() {
        draftSR();
        objFMCase.Status__c='Draft Request';
        upsert objFMCase;
        return new PageReference('/' + objFMCase.Id);
    }
    public virtual PageReference submitSR() {
        draftSR();
        System.debug('==objFMCase==' + objFMCase);
        objFMCase.Approval_Status__c='Pending';
        objFMCase.Submit_for_Approval__c = true;
        objFMCase.Submitted__c = true;
        objFMCase.Status__c='Submitted';
        //insert objFMCase;
        list<FM_Approver__mdt> lstApprovers = FM_Utility.fetchApprovers(objFMCase.Request_Type_DeveloperName__c,
        objBU.Property_City__c);
        System.debug('==lstApprovers==' + lstApprovers);
        String approvingRoles = '';
        for(FM_Approver__mdt mdt : lstApprovers) {
            approvingRoles += mdt.Role__c + ',';
        }
        approvingRoles = approvingRoles.removeEnd(',');
        objFMCase.Approving_Authorities__c = approvingRoles;
        upsert objFMCase;
        return new PageReference('/' + objFMCase.Id);
    }
    public void getfieldValues(){
        valueListOfAccessCard = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = FM_Case__c.Access_Card_Permission__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        System.debug('ple===='+ple);
       for( Schema.PicklistEntry f : ple)
       {
           System.debug('f-------'+f);
           System.debug('f.get'+f.getLabeL());
           valueListOfAccessCard.add(new SelectOption(f.getLabel(),f.getLabel()));
       }

    }
}