/****************************************************************************************************
* Name          : ApproveSalesToursController                                                       *
* Description   : ApproveSalesTours Page Controller                                                 *
* Created Date  : 22/04/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE            COMMENTS                                                *
* 1.0   Craig Lobo          22/04/2018      Initial Draft.                                          *
****************************************************************************************************/
public with sharing class ApproveSalesToursController {


    public String SALES_TOUR_QUERY = ' SELECT Id, Name, Inquiry_PC__c, Inquiry_Name__c, Is_Approved__c, '
                                   + ' Inquiry__c, Tour_Outcome__c, Budget__c, Comments__c, Check_In_Date__c, '
                                   + ' Check_Out_Date__c, Approval_Status__c, Inquiry__r.Assigned_PC__c '
                                   + ' FROM Sales_Tours__c WHERE Status__c = \'Submitted\' '
                                   + ' AND Is_Approved__c = false ';

    public List<Sales_Tours__c> salesToursList                                          {get; set;}
    public String returnId                                                              {get; set;}
    public String display                                                               {get; set;}
    public String myMsg                                                                 {get; set;}
    public String selAgencyPC                                                           {get; set;}

    /** 
     * Constructor
     */
    public ApproveSalesToursController(ApexPages.StandardController controller) {
        salesToursList = new List<Sales_Tours__c>();
        display = '';
        init();
    }

    /** 
     * Initial method to display the records on the page (Also called form refersh button)
     */
    public void init() {
        
        salesToursList.clear();
        salesToursList = DataBase.query(SALES_TOUR_QUERY);
        if (!salesToursList.isEmpty()) {
            returnId = salesToursList[0].Id;
            returnId = returnId.substring(0, 3);
        }
        System.debug('returnId >> : '+ returnId);
        System.debug('salesToursList >> : '+ salesToursList);
    }

    /**
     * method to Search the Sales Tours via PC Name
     */
    public void searchToursByPC() {
        salesToursList.clear();
        System.debug('selAgencyPC >> : '+ selAgencyPC);
        String searchQuery  = SALES_TOUR_QUERY
                            + ' AND Inquiry__r.Assigned_PC__c = :selAgencyPC ';
        salesToursList = DataBase.query(searchQuery);
        System.debug('searchToursByPC >> : '+ salesToursList);
    }

    /**
     * Method to update the Sales Tour records as Approved
     */
    public void approveSalesTours() {
        List<Sales_Tours__c> salesToursUpdateList = new List<Sales_Tours__c>();
        for (Sales_Tours__c tourObj : salesToursList) {
            if (tourObj.Is_Approved__c) {
                salesToursUpdateList.add(tourObj);
            } 
        }

        System.debug('salesToursUpdateList >> : '+ salesToursUpdateList);
        System.debug('salesToursUpdateList SIZE>> : '+ salesToursUpdateList.size());
        if (salesToursUpdateList.isEmpty()) {
            display = 'true';
            myMsg = ' Please select at least one Sales Tour to be Approved. ';
        } else {
            try {
                update salesToursUpdateList;
                display = 'false';
                myMsg = ' Sales Tours Approved Successfully. ';
                init();
            } catch(Exception e) {
                display = 'true';
                myMsg = e.getMessage();
            }
        }

    }

}