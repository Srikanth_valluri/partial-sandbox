@isTest
public with sharing class BitlyServiceTest {

    public class MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
       public HTTPResponse respond(HTTPRequest req) {
            
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"example":"test"}');
            res.setStatusCode(200);
            return res;
        }
    }
    @IsTest
    static void methodName0(){
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.startTest();
        BitlyService obj = new BitlyService();
        String result = obj.shorten( 'www.google.com' );
        Test.stopTest();
        
    }

   
}