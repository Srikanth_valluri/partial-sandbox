/**************************************************************************************************
* Name               : AgentPortalMyCustomersController
* Description        : An apex page controller for AgentPortalMyCustomers
* Created Date       : Pratiksha Narvekar                                                                       
* Created By         : 05-08-2017                                                               
* Last Modified Date :                                                                             
* Last Modified By   :                                                                            
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR                        DATE                                                                    
* 1.0         Pratiksha Narvekar        05/08/2017                                                               
* 1.1         Craig Lobo                08/11/2017                                                               
**************************************************************************************************/
public without sharing class AgentPortalMyCustomers{

    public PageReference loadCILData() {
        return null;
    } 

    public Boolean isEditMode {set;get;} 
    public string customListViewId{set;get;}
    public string CILPrefix{set;get;}
    public List<Inquiry__c> CILLists{set;get;}
    public decimal CilListSize{get;set;}
    public Id inquiryRecordTypeId {set;get;}
    public String tabName{set;get;}
    public List<CustomerDetails> listCustomerDetails{get;set;}
    public Integer LeadCount{get;set;}


    public AgentPortalMyCustomers() {
        isEditMode = false;
        CILLists = new List<Inquiry__c>();
        inquiryRecordTypeId = 
            Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(DAMAC_Constants.CIL_RT).getRecordTypeId();
        if(ApexPages.currentPage().getParameters().containsKey('sfdc.tabName')){
        tabName = ApexPages.currentPage().getParameters().get('sfdc.tabName');
        }
    }


/**************************************************************************************************
    Method:         loadCILData
    Description:    - show the custom view for inquiries
**************************************************************************************************/
    public List<CustomerDetails> getloadCILData(){

        Contact loginContact = AgentPortalUtilityQueryManager.getContactInformation();
        System.debug('...loginContact ...'+loginContact);

        listCustomerDetails = new List<CustomerDetails>();
        
        if(null != loginContact && 
            ( loginContact.Authorised_Signatory__c || loginContact.Agent_Representative__c 
            || loginContact.Owner__c || loginContact.Portal_Administrator__c)
        ) {
            system.debug('***loadCILData entered - Auth /Owner');
            Set<Id> userIds = AgentPortalUtilityQueryManager.getAllContact(loginContact.AccountID);      
            system.debug('***loadCILData entered - userIds'+userIds);
            string condition = 'AND Agent_Name__c IN :userIds';
            System.debug('...condition...'+condition+'...inquiryRecordTypeId...'+inquiryRecordTypeId+'...userIds...'+userIds);
            if(!Test.isRunningTest()) {
                String query = ' SELECT Id, First_Name__c, Agent_Name__r.Name,Agent_Name__c, '
                             + ' Last_Name__c,Email__c,Mobile_CountryCode__c,Mobile_Phone_5__c, '
                             + ' Passport_Number__c, Preferred_Language__c, Inquiry_Owner__c, '
                             + ' Name, Comments__c, CreatedDate, Mobile_Phone__c '
                             + ' FROM Inquiry__c '
                             + ' WHERE CreatedById = \'' 
                             + UserInfo.getUserId() 
                             + '\''
                             + ' AND RecordTypeId = \''
                             + inquiryRecordTypeId 
                             + '\''
                             + ' ORDER BY CreatedDate DESC ';
                System.debug('...query...'+query);
                CILLists =  Database.query(query);
            } else{
                CILLists = [Select Id, First_Name__c, Agent_Name__r.Name, Agent_Name__c, 
                            Last_Name__c,Email__c, Mobile_Phone__c, Comments__c, Name,
                            Mobile_CountryCode__c, Mobile_Phone_5__c, CreatedDate, 
                            Passport_Number__c,Preferred_Language__c, Inquiry_Owner__c
                            FROM Inquiry__c];
            }
            //AgentPortalUtilityQueryManager.getCILs(condition,inquiryRecordTypeId,userIds);
            
        } else if(null != loginContact) {
            String condition = ' AND CreatedById = \''
                             + UserInfo.getUserId()
                             + '\'';
            CILLists = AgentPortalUtilityQueryManager.getCILs(condition, inquiryRecordTypeId, null);
        }

        populateWrapper(CILLists, loginContact);
        CILPrefix = DamacUtility.getObjectPrefix('Inquiry__c');
        System.debug('ACILPrefix----' + CILPrefix);
        if(!listCustomerDetails.isEmpty()){
            CilListSize =listCustomerDetails.size();
            System.debug('ACilListSize----' + CilListSize);
        }

        LeadCount= listCustomerDetails.size();
        System.debug('LeadCount----' + LeadCount);
        System.debug('RETURN listCustomerDetails----' + listCustomerDetails);
        return listCustomerDetails;
    }

    public void populateWrapper(List<Inquiry__c> pCILLists, Contact ploginContact) {
        for(Inquiry__c objInq : pCILLists ) {
            if(Test.isRunningTest()) {
                objInq.Agent_Name__c = ploginContact.Id;
            }
            CustomerDetails objCustomerDetails = new CustomerDetails();
            objCustomerDetails.FirstName = objInq.First_Name__c;
            objCustomerDetails.LastName = objInq.Last_Name__c;
            objCustomerDetails.OwnerID= objInq.Inquiry_Owner__c;
            objCustomerDetails.InquiryNo= objInq.Name;
            objCustomerDetails.CreatedDate= String.valueof(objInq.CreatedDate); 
            objCustomerDetails.Comments= objInq.Comments__c;
            objCustomerDetails.AgentName = objInq.Agent_Name__r.Name;
            objCustomerDetails.Email= objInq.Email__c;
            objCustomerDetails.CountryCode= objInq.Mobile_CountryCode__c;
            objCustomerDetails.MobileNo = String.valueOf(objInq.Mobile_Phone__c);
            objCustomerDetails.Passport= objInq.Passport_Number__c;
            objCustomerDetails.PreferredLanguage= objInq.Preferred_Language__c;
            listCustomerDetails.add(objCustomerDetails);
        }
    }

    public void editCIL() { 
        isEditMode = true;
    }

    public void saveCIL() { 
        isEditMode = false;
    }

    public void cancelCIL() { 
        isEditMode = false;
    }

    public class CustomerDetails {
        public string FirstName         {get;set;}
        public string LastName          {get;set;}
        public string Email             {get;set;}
        public string ContactInfo       {get;set;}
        public string CountryCode       {get;set;}
        public string MobileNo          {get;set;}
        public string Passport          {get; set;}
        public string OwnerID           {get;set;}
        public string InquiryNo         {get;set;}
        public string AgentName         {get;set;} 
        public string CreatedDate       {get;set;}
        public string Comments          {get;set;}
        public string PreferredLanguage {get;set;}
        public boolean DPOK             {get;set;}
        public boolean DOCOK            {get;set;}
    }
}