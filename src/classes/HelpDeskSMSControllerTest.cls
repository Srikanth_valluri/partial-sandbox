/**
 * @File Name          : HelpDeskSMSControllerTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 1/30/2020, 5:20:31 PM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    1/20/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
public class HelpDeskSMSControllerTest {
    
    @isTest static void test_callTheSMSServiceFoClosedStatus() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', ProfileId = p.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testdmacorg.com');
            
        List<FM_Case__c> lstFMCases = new List<FM_Case__c>();
        List<Id> caseIdList = new List<Id>();
        System.runAs(u) {
        Contact con = new Contact(LastName = 'TEst');
        insert con;
        
        // Start 
            Property__c objProperty = new Property__c();
            objProperty.Name = 'Test Project';
            objProperty.Property_Name__c = 'Test Property';
            objProperty.Property_ID__c = 3431;
            objProperty.CurrencyIsoCode = 'AED';
            insert objProperty;
    
            Account objAcc = new Account();
            objAcc = TestDataFactory_CRM.createPersonAccount();
            objAcc.Email__pc = 'test@gmail.com';
            objAcc.Mobile_Phone_Encrypt__pc = '00919637884261';
            
            insert objAcc;
            
            NSIBPM__Service_Request__c objServReq = TestDataFactory_CRM.createServiceRequest();
            insert objServReq ;
        
            List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objServReq.Id,1);
            insert lstBooking ;
            
            List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
            insert lstActiveStatus ;
    
            List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking ,3);
            for( Booking_Unit__c objUnit : lstBookingUnit  ) {
                objUnit.Resident__c = objAcc.Id ;
                objUnit.Handover_Flag__c = 'Y' ;
                objUnit.Unit_Name__c = 'DSR/79/7903';
            }
            lstBookingUnit[2].Owner__c = objAcc.Id ;
            insert lstBookingUnit ;
    
            Location__c objLoc = new Location__c();
            objLoc = TestDataFactoryFM.createLocation();
            Id recTypeIdLoc=Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
            objLoc.RecordTypeId = recTypeIdLoc;
            objLoc.Name = 'DSR';
            objLoc.Property_Name__c = objProperty.id;
            objLoc.Loams_Email__c = 'test@test.com';
            insert objLoc;
        // End
        
        Id HELPDESK_RECORD_TYPE_ID = DamacUtility.getRecordTypeId('FM_Case__c', 'Helpdesk');
        FM_Case__c obj1 = new FM_Case__c();
        obj1.Email__c = 'tenant@gmail.com';
        obj1.Tenant_Email__c = 'owner@gmail.com';
        obj1.recordtypeid = HELPDESK_RECORD_TYPE_ID;
        obj1.Booking_Unit__c=lstBookingUnit[0].id;
        obj1.Status__c = 'Closed';
        obj1.Account__c = objAcc.Id;
        lstFMCases.add(obj1);        
              
        insert lstFMCases;
            for(fm_case__c obj: lstFMCases){
                caseIdList.add(obj.id);
            }
        HelpDeskSMSController.callTheSMSService(caseIdList);
        }
    }
     @isTest static void test_callTheSMSServiceForAssignedCAFM() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', ProfileId = p.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testdmacorg.com');
            
        List<FM_Case__c> lstFMCases = new List<FM_Case__c>();
        List<Id> caseIdList = new List<Id>();
        System.runAs(u) {
        Contact con = new Contact(LastName = 'TEst');
        insert con;
        
        // Start 
            Property__c objProperty = new Property__c();
            objProperty.Name = 'Test Project';
            objProperty.Property_Name__c = 'Test Property';
            objProperty.Property_ID__c = 3431;
            objProperty.CurrencyIsoCode = 'AED';
            insert objProperty;
    
            Account objAcc = new Account();
            objAcc = TestDataFactory_CRM.createPersonAccount();
            objAcc.Email__pc = 'test@gmail.com';
            objAcc.Mobile_Phone_Encrypt__pc = '00919637884261';
            
            insert objAcc;
            
            NSIBPM__Service_Request__c objServReq = TestDataFactory_CRM.createServiceRequest();
            insert objServReq ;
        
            List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objServReq.Id,1);
            insert lstBooking ;
            
            List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
            insert lstActiveStatus ;
    
            List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking ,3);
            for( Booking_Unit__c objUnit : lstBookingUnit  ) {
                objUnit.Resident__c = objAcc.Id ;
                objUnit.Handover_Flag__c = 'Y' ;
                objUnit.Unit_Name__c = 'DSR/79/7903';
            }
            lstBookingUnit[2].Owner__c = objAcc.Id ;
            insert lstBookingUnit ;
    
            Location__c objLoc = new Location__c();
            objLoc = TestDataFactoryFM.createLocation();
            Id recTypeIdLoc=Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
            objLoc.RecordTypeId = recTypeIdLoc;
            objLoc.Name = 'DSR';
            objLoc.Property_Name__c = objProperty.id;
            objLoc.Loams_Email__c = 'test@test.com';
            insert objLoc;
        // End
        
        Id HELPDESK_RECORD_TYPE_ID = DamacUtility.getRecordTypeId('FM_Case__c', 'Helpdesk');
        FM_Case__c obj1 = new FM_Case__c();
        obj1.Email__c = 'tenant@gmail.com';
        obj1.Tenant_Email__c = 'owner@gmail.com';
        obj1.recordtypeid = HELPDESK_RECORD_TYPE_ID;
        obj1.Booking_Unit__c=lstBookingUnit[0].id;
        obj1.CAFM_Task_State__c = 'Assigned';
        obj1.Account__c = objAcc.Id;
        lstFMCases.add(obj1);        
              
        insert lstFMCases;
            for(fm_case__c obj: lstFMCases){
                caseIdList.add(obj.id);
            }
        HelpDeskSMSController.callTheSMSService(caseIdList);
        }
    }

    @isTest static void test_callTheSMSServiceForSubmitted() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', ProfileId = p.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testdmacorg.com');
            
        List<FM_Case__c> lstFMCases = new List<FM_Case__c>();
        List<Id> caseIdList = new List<Id>();
        System.runAs(u) {
        Contact con = new Contact(LastName = 'TEst');
        insert con;
        
        // Start 
            Property__c objProperty = new Property__c();
            objProperty.Name = 'Test Project';
            objProperty.Property_Name__c = 'Test Property';
            objProperty.Property_ID__c = 3431;
            objProperty.CurrencyIsoCode = 'AED';
            insert objProperty;
    
            Account objAcc = new Account();
            objAcc = TestDataFactory_CRM.createPersonAccount();
            objAcc.Email__pc = 'test@gmail.com';
            objAcc.Mobile_Phone_Encrypt__pc = '00919637884261';
            
            insert objAcc;
            
            NSIBPM__Service_Request__c objServReq = TestDataFactory_CRM.createServiceRequest();
            insert objServReq ;
        
            List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objServReq.Id,1);
            insert lstBooking ;
            
            List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
            insert lstActiveStatus ;
    
            List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking ,3);
            for( Booking_Unit__c objUnit : lstBookingUnit  ) {
                objUnit.Resident__c = objAcc.Id ;
                objUnit.Handover_Flag__c = 'Y' ;
                objUnit.Unit_Name__c = 'DSR/79/7903';
            }
            lstBookingUnit[2].Owner__c = objAcc.Id ;
            insert lstBookingUnit ;
    
            Location__c objLoc = new Location__c();
            objLoc = TestDataFactoryFM.createLocation();
            Id recTypeIdLoc=Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
            objLoc.RecordTypeId = recTypeIdLoc;
            objLoc.Name = 'DSR';
            objLoc.Property_Name__c = objProperty.id;
            objLoc.Loams_Email__c = 'test@test.com';
            insert objLoc;
        // End
        
        Id HELPDESK_RECORD_TYPE_ID = DamacUtility.getRecordTypeId('FM_Case__c', 'Helpdesk');
        FM_Case__c obj1 = new FM_Case__c();
        obj1.Email__c = 'tenant@gmail.com';
        obj1.Tenant_Email__c = 'owner@gmail.com';
        obj1.recordtypeid = HELPDESK_RECORD_TYPE_ID;
        obj1.Booking_Unit__c=lstBookingUnit[0].id;
        obj1.Status__c = 'Submitted';
        obj1.CAFM_Task_State__c = '';
        obj1.Account__c = objAcc.Id;
        lstFMCases.add(obj1);        
              
        insert lstFMCases;
            for(fm_case__c obj: lstFMCases){
                caseIdList.add(obj.id);
            }
        HelpDeskSMSController.callTheSMSService(caseIdList);
        }
    }
}