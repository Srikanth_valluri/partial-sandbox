public without sharing class LeaseHandoverCREProcess {
    public id accountId {get;set;}
    public Booking_Unit__c objBookingUnit {get;set;}
    public Case objCase {get;set;}
    public UnitDetailsService.BookinUnitDetailsWrapper objBookinUnitDetailsWrapper {get; set;}
    public string strSRType {get;set;}
    public string strCaseID {get; set;}
    public string strLeaseYear {get; set;}
    public string errorMessage{get;set;}
    public string strSelectedUnit {get;set;}
    public string strSelectedCategory {get;set;}  
    public string strSelectedMonth {get;set;}
    public string strSelectedOption {get;set;}
    public string strSelectedReason {get;set;}
    public String CurrencyCode {get;set;}
    public string strPayPlanId {get;set;}
    public string strPaymentPlan {get;set;}
    public string SOAurl {get;set;}
    public string errorMsg {get;set;}
    public string errorMsg1 {get;set;}
    public string errorMsg2 {get;set;}
    public string caseId {get;set;}
    public string spaType {get;set;}
    public String docURL {get;set;}
    public transient string fileAttachmentBody {get;set;}
    public string fileAttachmentName {get;set;}
    public boolean showCurrentSection {get;set;}
    public boolean showDocuments {get;set;}
    public boolean isValid{get;set;}
    public boolean displayPopup {get;set;}
    public boolean blnReadOnly {get;set;}
    public boolean isBatchRunning {get;set;}
    public boolean displayOptions {get; set;}
    public boolean displayURL {get;set;}
    public boolean blnDocGenerated {get;set;}
    public boolean isHotel {get;set;}
    public list<selectOption> lstUnits;
    public list<selectoption> lstOptions {get;set;}
    public list<selectoption> lstMonths {get;set;}
    public list<selectoption> lstReasons {get;set;}
    public list<Case> lstExistingCase {get;set;}
    public list<Buyer__c> lstJointBuyers {get;set;}
    public list<SelectOption> lstCategories {get;set;}
    public list<SelectOption> lstRGSOptions {get;set;}
    public list<SR_Attachments__c> listInsertedDocs {get; set;}
    
    private string bookingId;
    private string caseNumber;
    private boolean isDraft;
    private list<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocReq;
    private list<SR_Attachments__c> lstSrAttachment; 
    
    public LeaseHandoverCREProcess (){
        objCase = new Case();
        
        lstCategories = new list<SelectOption>();
        lstCategories.add(new selectOption('Unit Details', 'Unit Details'));
        lstCategories.add(new selectOption('Flags', 'Flags'));
        lstCategories.add(new selectOption('Unit Status', 'Unit Status'));
        lstCategories.add(new selectOption('Joint Buyers', 'Joint Buyers'));
        lstCategories.add(new selectOption('Open SRs', 'Open SRs'));
        
        lstOptions = new list<selectoption>();
        lstOptions.add(new SelectOption('true','Yes'));
        lstOptions.add(new SelectOption('false','No'));
        
        lstRGSOptions = new list<SelectOption>();
        lstRGSOptions.add(new SelectOption('Option 1 – Opted out of RGS','Option 1 – Opted out of RGS'));
        lstRGSOptions.add(new SelectOption('Option 2 – In RGS','Option 2 – In RGS'));
        
        lstMonths = new list<SelectOption>();
        lstMonths.add(new SelectOption('None', 'None'));
        lstMonths.add(new SelectOption('One (1)', 'One (1)'));
        lstMonths.add(new SelectOption('Two (2)', 'Two (2)'));
        lstMonths.add(new SelectOption('Three (3)', 'Three (3)'));
        lstMonths.add(new SelectOption('Four (4)', 'Four (4)'));
        lstMonths.add(new SelectOption('Five (5)', 'Five (5)'));
        lstMonths.add(new SelectOption('Six (6)', 'Six (6)'));
        lstMonths.add(new SelectOption('Seven (7)', 'Seven (7)'));
        lstMonths.add(new SelectOption('Eight (8)', 'Eight (8)'));
        lstMonths.add(new SelectOption('Nine (9)', 'Nine (9)'));
        lstMonths.add(new SelectOption('Ten (10)', 'Ten (10)'));
        lstMonths.add(new SelectOption('Eleven (11)', 'Eleven (11)'));
        lstMonths.add(new SelectOption('Twelve (12)', 'Twelve (12)'));
        
        lstReasons = new list<SelectOption>();
        lstReasons.add(new SelectOption('None', 'None'));
        lstReasons.add(new SelectOption('from the Anticipated Completion Date of the Unit as specified in the SPA'
        , 'from the Anticipated Completion Date of the Unit as specified in the SPA'));
        lstReasons.add(new SelectOption('from Completion Date of the Unit as determined by the Handover Notice as per the terms of the SPA'
        , 'from Completion Date of the Unit as determined by the Handover Notice as per the terms of the SPA'));
        
        system.debug('****strSelectedMonth'+strSelectedMonth);
        //strSelectedMonth = 'Five (5)';
        strLeaseYear = '3';
        strSelectedOption = 'Option 1 – Opted out of RGS';
        showCurrentSection = false;
        showDocuments = false;
        displayOptions = false;
        blnDocGenerated = false;
        isHotel = false;
    }//End Constructor
    
    public virtual string getName() {
        if(String.isNotBlank(accountId) && String.isNotBlank(strSRType) 
            && strSRType == 'Lease_Handover'){
            if (!string.isBlank(strCaseID)) {
                gotoInitiation();
            }
        }
        return null;
    }
    
    public list <SelectOption> getlstUnits(){
        lstUnits = new list<selectOption>();
        lstUnits.add(new selectOption('', '--None--'));
        
        set<String> setActiveStatus = new set<String>(); 
        for(Booking_Unit_Active_Status__c unitStatusInst: [select Id, Status_Value__c
                                                               from Booking_Unit_Active_Status__c
                                                               where NOT (Name LIKE '%FTL%' 
                                                               OR Name LIKE '%Assignment%' 
                                                               OR Name LIKE '%Direct Release%')]) {
            if(String.isNotBlank(unitStatusInst.Status_Value__c)){
                setActiveStatus.add(unitStatusInst.Status_Value__c);
            }
        }
        for(Booking_Unit__c objBU : [Select Id,
                                            Name,
                                            Unit_Details__c,
                                            Unit_Name__c,
                                            Booking__c,                                           
                                            Booking__r.Account__c,
                                            Registration_Status__c,
                                            Eligible_For_Early_Handover__c,
                                            Early_Handover__c,
                                            Handover_Flag__c,
                                            Lease_Handover__c 
                                     From Booking_Unit__c
                                     Where Booking__r.Account__c =: accountId
                                     and Registration_Status__c IN: setActiveStatus                                     
                                    /* and Early_Handover__c = False
                                     and Handover_Flag__c != 'Y'*/
                                     and Lease_Handover__c = False
                                     Order By Unit_Details__c
                                     ]){
            if (objBU.Unit_Name__c != null){
                lstUnits.add(new selectOption(objBU.Id, objBU.Unit_Name__c));
            }
        }//End for
        return lstUnits;
    }//end getlstUnits
    
    public void autoPopulateBUDetails(){
        checkExistingSRExists();
        if (lstExistingCase.size() >0) {
            errorMsg1 = 'Error : An open Service Request already exists for the chosen Booking Unit. You will be unable to proceed';
        }
        if(!string.isBlank(strSelectedUnit) && lstExistingCase.size() == 0){ 
            showCurrentSection = true;   
            //errorMsg = 'Please add Lease Start and End Date';        
            bookingUnitInfo(strSelectedUnit); 
            CurrencyCode = objBookingUnit.CurrencyIsoCode;  
            
            if( objBookingUnit != null
            && /*(String.isNotBlank( objBookingUnit.Permitted_Use__c ) 
                && objBookingUnit.Permitted_Use__c.equalsIgnoreCase( 'Hotel' ) ) 
            || */(objBookingUnit.Inventory__c != null
                && String.isNotBlank( objBookingUnit.Inventory__r.Space_Type_Lookup_Code__c ) 
                && objBookingUnit.Inventory__r.Space_Type_Lookup_Code__c.equalsIgnoreCase( 'Hotel' ) ) ) {
                    isHotel = true;
                    showCurrentSection = false;
                    System.debug('It is Hotel'+isHotel);
            }
            else {
                isHotel = false;
                showCurrentSection = true;
                System.debug('It is not  Hotel');
            }
            bookingId = objBookingUnit.Booking__c;
            if (objBookingUnit.Payment_Plans__r.size() > 0) {
                strPaymentPlan = objBookingUnit.Payment_Plans__r[0].Name;
                strPayPlanId = objBookingUnit.Payment_Plans__r[0].Id;
            }
            if (objBookingUnit.SPA_Type__c != null){
                spaType = objBookingUnit.SPA_Type__c;
            }
            if (objBookingUnit.RGS_Percent__c != null 
                && decimal.valueOf(objBookingUnit.RGS_Percent__c) > 0){
                displayOptions = true;
            }
            lstJointBuyers = new list<Buyer__c>();
            for (Buyer__c objBuyer : [SELECT Id
                                              , Name
                                              , First_Name__c
                                              , Last_Name__c
                                              , Account__c
                                              , Buyer_ID__c
                                              , Account__r.Party_Id__c
                                           FROM Buyer__c
                                          WHERE Account__c != null
                                            AND Booking__c = :bookingId
                                            AND Primary_Buyer__c = false]) {
                  lstJointBuyers.add(objBuyer);                           
              }
              try {
                  objBookinUnitDetailsWrapper =
                    UnitDetailsService.getBookingUnitDetails(
                        objBookingUnit.Registration_ID__c
                    );                
              } catch (Exception ex) {
                  system.debug('Exception in Unit Details Service: ' + ex.getMessage());
              } 
              createCase();
         } else {
             objBookingUnit = new Booking_Unit__c();         
             objBookinUnitDetailsWrapper = new UnitDetailsService.BookinUnitDetailsWrapper();             
         }         
         System.debug(' Hotel--->'+isHotel);
    }// end autoPopulateBUDetails
    
    public list<Case> checkExistingSRExists(){
        set<String> setAllowedSRTypes = new set<String>();
        setAllowedSRTypes.add('Parking');
        setAllowedSRTypes.add('Complaint');
        setAllowedSRTypes.add('Promotion_Package');
        setAllowedSRTypes.add('POP');
        setAllowedSRTypes.add('Customer_Refund');
        setAllowedSRTypes.add('Token_Refund');
        setAllowedSRTypes.add('Utility_Registration_SR');
        setAllowedSRTypes.add('Title_Deed');
        
        lstExistingCase = new list<Case>(); 
        map<Id,Case> mapId_Case = new map<Id,Case>([Select c.Id
                                                         , c.Booking_Unit__c
                                                         , c.AccountId
                                                         , c.CaseNumber
                                                         , c.RecordType.DeveloperName
                                                         , c.RecordType.Name
                                                    From Case c
                                                    where c.Booking_Unit__c =: strSelectedUnit
                                                    and c.Status != 'Closed'
                                                    and c.Status != 'Rejected'
                                                    and c.Status != 'Cancelled'                                                                                                       
                                                    and c.RecordType.DeveloperName 
                                                        NOT IN : setAllowedSRTypes
                                                    and c.Id !=: strCaseID]);
        if(mapId_Case != null && !mapId_Case.isEmpty()){
            lstExistingCase.addAll(mapId_Case.values());
        }
        
        for(SR_Booking_Unit__c objSBU : [Select s.Id
                                              , s.Case__c
                                              , s.Case__r.Status
                                              , s.Case__r.CaseNumber
                                              , s.Case__r.RecordType.DeveloperName
                                              , s.Case__r.RecordType.Name
                                              , s.Booking_Unit__c 
                                         From SR_Booking_Unit__c s
                                         where s.Booking_Unit__c =:strSelectedUnit
                                         and s.Case__r.Status != 'Closed'
                                         and s.Case__r.Status != 'Rejected'
                                         and s.Case__r.Status != 'Cancelled'                                                                             
                                         and (s.Case__r.RecordType.DeveloperName = 'AOPT' 
                                            or s.Case__r.RecordType.DeveloperName = 'Handover'
                                            or s.Case__r.RecordType.DeveloperName = 'Fund_Transfer' 
                                            or s.Case__r.RecordType.DeveloperName 
                                                = 'Fund_Transfer_Active_Units')
                                         and s.Case__c !=: strCaseID]){
            if(!mapId_Case.containsKey(objSBU.Case__c)){
                Case objCase = objSBU.Case__r;
                lstExistingCase.add(objCase);
                mapId_Case.put(objSBU.Case__c, objSBU.Case__r);
            }
        }
        
        for (Account objAccount : [Select Id, 
                                        (Select RecordTypeId,
                                                RecordType.Name,
                                                RecordType.DeveloperName,
                                                CaseNumber,
                                                Status
                                        From Cases where (RecordType.DeveloperName 
                                            = 'Change_of_Details' or
                                            RecordType.DeveloperName = 'Change_of_Joint_Buyer' or
                                            RecordType.DeveloperName = 'Name_Nationality_Change' or
                                            RecordType.DeveloperName = 'Passport_Detail_Update')and
                                            Status != 'Closed'
                                            and Status != 'Rejected'
                                            and Status != 'Cancelled'                                                                                   
                                            )
                                   From Account
                                   Where Id=:accountId]){
            lstExistingCase.addAll(objAccount.Cases);           
        }
        System.debug( 'lstExistingCase LHO ==' + lstExistingCase);
        return lstExistingCase;
    }// end of checkExistingSRExists
    
     public void bookingUnitInfo(String BuId) {
        if (!string.isBlank(BuId)) {
            objBookingUnit = [select ID,
                                Bedroom_Type__c,                                  
                                Registration_DateTime__c,
                                Handover_Flag__c,
                                CurrencyIsoCode,
                                Permitted_Use__c,
                                Unit_Details__c,                        
                                Unit_Selling_Price__c,
                                Mortgage__c,
                                Under_Assignment__c,
                                Agreement_Date__c,
                                HOS_Name__c,
                                Manager_Name__c,
                                Property_Consultant__c,
                                Booking__c,
                                Property_Name__c,
                                Property_City__c,
                                RGS_Option__c,
                                Building_ID__c,
                                Registration_Status__c,                                                                
                                Inventory__r.Building_Location__c,
                                Inventory__r.Property__r.Name,
                                Inventory__r.Property__r.Property_City__c,
                                Inventory__r.Property__r.Property_Code__c,
                                Inventory__r.Property_City__c,
                                Inventory__r.Project_Category__c,
                                Inventory__r.Space_Type_Lookup_Code__c,
                                Booking__r.Account__r.Nationality__pc,
                                Booking__r.Account__r.Nationality__c,
                                Booking__r.Account__r.Party_Type__c,
                                Booking__r.Account__r.Country__c,
                                Booking__r.Account__r.IsPersonAccount,
                                Booking__r.Account__r.Country__pc,
                                Inventory__r.Unit_Plan__c,
                                Inventory__r.Floor_Plan__c,
                                Registration_ID__c,
                                Title_Deed__c,
                                Requested_Price__c,
                                Anticipated_Completion_Date__c,
                                Handover_Notice_Sent__c,
                                PCC_Release__c,
                                Rental_Pool__c,
                                Booking_Type__c,
                                Unit_Name__c,
                                Unit_Type__c,
                                SPA_Type__c,
                                RGS_Percent__c,
                                (Select Id, Name From Payment_Plans__r)
                            from Booking_Unit__c
                            where ID =: BuId];                
        }
    }// end of method bookingUnitInfo
    
     public void SOAtobeGenerated() {
        if (objBookingUnit != null && objBookingUnit.Registration_ID__c != null) {
            GenerateSOAController.soaResponse objSOA 
                = GenerateSOAController.getSOADocument(objBookingUnit.Registration_ID__c);            
            SOAurl = objSOA.url;
            system.debug('!!!!!objSOA'+SOAurl);
        }
    }//end SOAtobeGenerated
    
    public void GenerateDocuments() {
        showDocuments = true;
        errorMsg = '';
        createCase();
    }// end of method GenerateDocuments
    
    public void createCase() {
        Id caseRecordTypeId
            = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Lease Handover').getRecordTypeId();
        system.debug('!!!!caseId'+strCaseID);
        system.debug('!!!!objBookingUnit'+objBookingUnit.Id);
        if (String.isBlank(strCaseID)){
            strCaseID = caseId;
        }
        Case lhoCase = new Case(Id = strCaseID);
        lhoCase.RecordTypeID = caseRecordTypeId;
        lhoCase.Booking_Unit__c = objBookingUnit.id;
        lhoCase.AccountId = accountId;
        lhoCase.Status = 'New';
        lhoCase.Origin = 'Walk-In';
        /*if (!string.isBlank(strSelectedMonth)){
            lhoCase.No_of_Months_for_Lease__c = strSelectedMonth;
        }*/
        /*if (!string.isBlank(strSelectedReason)){
                lhoCase.Point_of_Reference__c = strSelectedReason;
        }*/
        
        if (displayOptions == true){
            system.debug('!!!strSelectedOption'+strSelectedOption);
	        if (!string.isBlank(strSelectedOption)){
	            lhoCase.RGS_Option__c = strSelectedOption;
	        }
	        if (!string.isBlank(strLeaseYear)){
	            lhoCase.Years_in_Lease__c = strLeaseYear;
	        }
        }
        /*system.debug('!!!!!!!!objCase.Lease_Commencement_Date__c'+objCase.Lease_Commencement_Date__c);
        if (objCase.Lease_Commencement_Date__c != null) {
            lhoCase.Lease_Commencement_Date__c = objCase.Lease_Commencement_Date__c;
        }
        if (objCase.Lease_End_Date__c != null) {
            lhoCase.Lease_End_Date__c = objCase.Lease_End_Date__c;
        }*/
        if (objBookingUnit.Handover_Flag__c != 'Y' && objBookingUnit.RGS_Percent__c == null) {
            lhoCase.Call_Outcome__c = 'Lease Handover without Rental Guarantee';
        } else if (objBookingUnit.Handover_Flag__c != 'Y' 
            && objBookingUnit.RGS_Percent__c != null) {
            lhoCase.Call_Outcome__c = 'Lease Handover with Rental Guarantee';
        } else if (objBookingUnit.Handover_Flag__c == 'Y' 
            && objBookingUnit.RGS_Percent__c == null) {
            lhoCase.Call_Outcome__c = 'Normal Handed over but without Rental Guarantee';
        } else if (objBookingUnit.Handover_Flag__c == 'Y' 
            && objBookingUnit.RGS_Percent__c != null) {
            lhoCase.Call_Outcome__c = 'Normal Handed over but with Rental Guarantee';
        }
        upsert lhoCase;
        caseId = lhoCase.Id;
        caseNumber = lhoCase.CaseNumber;
    }// end of method createCase
    
    public PageReference saveDraft() {
        isDraft = true;
        system.debug('!!!!!!!caseId'+caseId);
        if (caseId == null && !string.isBlank(strCaseID)) {
            caseId = strCaseID;
        }
        saveSubmit();
        Pagereference pgRef;
         if (caseId != null ) {
            pgRef = new Pagereference('/'+caseId );
         } 
         else {
            pgRef = new Pagereference('/500');
         }
         system.debug('== returning page ref from createSR()');
         return pgRef;
    }
    
    public PageReference saveSubmit() {
        system.debug('!!!!saveSubmit'+caseId );
        system.debug('!!!!strSelectedUnit'+strSelectedUnit);
        system.debug('!!!!!!!caseId'+caseId);
        set<String> setInActiveStatus = new set<String>(); 
        for(Status_Not_Allowed_for_LHO__c unitStatusInst: Status_Not_Allowed_for_LHO__c.getall().values()) {
            if(String.isNotBlank(unitStatusInst.Registration_Status__c)){
                setInActiveStatus.add(unitStatusInst.Registration_Status__c);
            }
        }
        if (caseId == null && !string.isBlank(strCaseID)) {
            caseId = strCaseID;
        }
        if (!string.isBlank(caseId ) && !string.isBlank(strSelectedUnit)) {            
            Case updateCase = new Case(Id = caseId);
            if (isDraft == true) {
                updateCase.Status = 'Draft Request';
            } else {
                if (setInActiveStatus.contains(objBookingUnit.Registration_Status__c)){
                    errorMsg2 = 'The Registration Status on Unit is '+objBookingUnit.Registration_Status__c + '. You can not proceed.';
                    return null;
                }
                updateCase.Status = 'Submitted';
                updateCase.Early_Handover_Status__c = 'Submitted';
                updateCase.Roles_from_Rule_Engine__c = 'Manager, Director';                
            }
            if (!string.isBlank(fileAttachmentName) && !string.isBlank(fileAttachmentBody)) {
                UploadDocument();
            }
            update updateCase;
        }
        if (displayOptions == true){
	        if (!string.isBlank(strSelectedOption)) {
	            Booking_Unit__c objUnit = new Booking_Unit__c(Id = objBookingUnit.id);
	            objUnit.RGS_Option__c = strSelectedOption; 
	            update objUnit;
	        }
        }
         Pagereference pgRef;
         if (caseId != null){
            pgRef = new Pagereference('/'+caseId);
         } else {
            pgRef = new Pagereference('/500');
         }
         system.debug('== returning page ref from createSR()');
         return pgRef;
    }
    
    public void UploadDocument() {
        list<SR_Attachments__c> lstAttachment = new list<SR_Attachments__c>();
        lstSrAttachment = new list<SR_Attachments__c>(); 
        lstMultipleDocReq = new List<UploadMultipleDocController.MultipleDocRequest>();       
        UploadMultipleDocController.data respObj = new UploadMultipleDocController.data();
        SR_Attachments__c objSRAttCRF = new SR_Attachments__c();
        objSRAttCRF.Type__c = 'Passport';
        objSRAttCRF.Name = extractName('Passport');
        objSRAttCRF.Case__c = caseId ;
        objSRAttCRF.isValid__c = isValid;
        lstSrAttachment.add(objSRAttCRF);
        
        UploadMultipleDocController.MultipleDocRequest reqObj 
            = new UploadMultipleDocController.MultipleDocRequest();
        reqObj.category = 'Document';
        reqObj.entityName = 'Damac Service Requests';
        if(String.isNotBlank(fileAttachmentBody )){
            blob objBlob = extractBody(fileAttachmentBody);
            if(objBlob != null){
                reqObj.base64Binary =  EncodingUtil.base64Encode(objBlob);
            }
        }
        reqObj.fileDescription = 'CRF Form';
        reqObj.fileId = 'IPMS-'+objBookingUnit.Registration_ID__c+'-'+extractName('Passport');
        reqObj.fileName = 'IPMS-'+objBookingUnit.Registration_ID__c+'-'+extractName('Passport');
        reqObj.registrationId = caseNumber;
        reqObj.sourceFileName = 'IPMS-'+objBookingUnit.Registration_ID__c+'-'+extractName('Passport');
        reqObj.sourceId = 'IPMS-'+objBookingUnit.Registration_ID__c+'-'+extractName('Passport');
        lstMultipleDocReq.add(reqObj);
        
        if(lstMultipleDocReq.size() > 0) {
            respObj= UploadMultipleDocController.getMultipleDocUrl(lstMultipleDocReq);
            System.debug('===respObj==' + respObj);
        }
        
         if(respObj != NULL && lstMultipleDocReq.size() > 0) {
             if(respObj.status == 'Exception'){                           
                 Error_Log__c objErr = createErrorLogRecord(accountId ,strSelectedUnit, caseId );
                 objErr.Error_Details__c = errorMessage;
                 insertErrorLog(objErr);
            }
            if(respObj.Data == null || respObj.Data.size()==0){                           
                Error_Log__c objErr = createErrorLogRecord(accountId ,strSelectedUnit, caseId );
                objErr.Error_Details__c = respObj.message;
                insertErrorLog(objErr);                           
            }
            if (lstSrAttachment != null && lstSrAttachment.size() > 0){                
                for(SR_Attachments__c att : lstSrAttachment) {
                    if (respObj.Data != null ) {
                        for (UploadMultipleDocController.MultipleDocResponse objData : respObj.data) {
                            if(objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url)){
                                att.Attachment_URL__c = objData.url;
                                att.Case__c = caseId ;
                                lstAttachment.add(att);
                            } else {
                                Error_Log__c objErr = 
                                    createErrorLogRecord(accountId ,strSelectedUnit, caseId );
                                objErr.Error_Details__c = respObj.message;
                                insertErrorLog(objErr);
                            }                        
                        }
                    }
                }
            }
        }
        if (lstAttachment != null && lstAttachment.size()>0){                 
            upsert lstAttachment;
        }
    }
    
    private String extractName( String strName ) {
      return strName.substring( strName.lastIndexOf('\\')+1 ) ;
    }
    
    public Blob extractBody( String strBody ) {
        return Blob.valueOf(strBody.substring( strBody.lastIndexOf(',')+1 ) );
    }
    
    public Error_Log__c createErrorLogRecord(Id accId, Id bookingUnitId, Id caseId){
        Error_Log__c objErr = new Error_Log__c();
        objErr.Account__c = accId;
        objErr.Booking_Unit__c = bookingUnitId;
        objErr.Case__c = caseId;
        objErr.Process_Name__c = 'Lease Handover';
        return objErr;
    }
    
     public void insertErrorLog(Error_Log__c objErr){
        try{
            insert objErr;
        }catch(Exception ex){
            errorMessage = 'Error : '+ ex.getMessage();
            system.debug('Error Log ex*****'+ex);
        }
    }
    
    public pageReference ViewAreaVariation() {
        system.debug('!!!!!!!!!!!!ViewCRF');
        CallHandoverMQServices.HandoverMQServicesResponse objLOD;
        objLOD 
            = CallHandoverMQServices.CallHandoverMQServiceLetterofDischarge(objBookingUnit.Registration_ID__c);
        system.debug('!!!!!!!objLOD'+objLOD);
        if (!string.isBlank(objLOD.url)){
            PageReference CRFPage = new PageReference (objLOD.url);
            system.debug('CRFPage'+CRFPage);
            CRFPage.setRedirect(true);
            return CRFPage;
        } else {
            displayPopup = true;
            return null;
        }
    }
    
    public void closePopup() {
        displayPopup = false;
        isBatchRunning = false;
        displayURL = false;
    }

    public PageReference gotoInitiation() {
        system.debug('!!!!gotoInitiation');
        Case objExtCase = [Select Id,
                                Booking_Unit__c,
                                Booking_Unit__r.Registration_ID__c,
                                Booking_Unit__r.SPA_Type__c,
                                Booking_Unit__r.RGS_Percent__c,
                                Booking_Unit__r.Permitted_Use__c,
                                POA_Issued_By__c,
                                POA_Name__c,
                                Purpose_of_POA__c,
                                POA_Expiry_Date__c,
                                Lease_Commencement_Date__c,
                                Lease_End_Date__c,
                                Security_Cheque_Amount__c,
                                Monthly_Rent__c,
                                Handover_Milestone_Date__c,
                                Completed_Milestones_Percent__c,
                                Status,
                                caseNumber,                                
                                CurrencyIsoCode,
                                NewPaymentTermJSON__c,
                                RGS_Option__c,
                                No_of_Months_for_Lease__c,
                                Point_of_Reference__c,
                                Years_in_Lease__c,
                                Booking_Unit__r.Inventory__r.Space_Type_Lookup_Code__c
                         From Case
                         Where Id = :strCaseID
                         limit 1
                        ];
            if (objExtCase != null) {
                strSelectedUnit = objExtCase.Booking_Unit__c;
                objBookinUnitDetailsWrapper =
                UnitDetailsService.getBookingUnitDetails(
                    objExtCase.Booking_Unit__r.Registration_ID__c
                );
                
                if (objExtCase.Status == 'Submitted' || objExtCase.Status == 'Closed' 
                    || objExtCase.Status == 'Rejected' || objExtCase.Status == 'Cancelled'){
                    blnReadOnly = true;
                    showCurrentSection = true;
                    showDocuments = true;
                } else if (objExtCase.Status == 'New') {
                    showCurrentSection = true;
                } else if (objExtCase.Status == 'Draft Request') {
                    showCurrentSection = true;
                    showDocuments = true;
                }
                
                if( objExtCase.Booking_Unit__c != null
                && (String.isNotBlank( objExtCase.Booking_Unit__r.Permitted_Use__c ) 
                    && objExtCase.Booking_Unit__r.Permitted_Use__c.equalsIgnoreCase( 'Hotel' ) ) 
                || (objExtCase.Booking_Unit__r.Inventory__c != null
                    && String.isNotBlank( objExtCase.Booking_Unit__r.Inventory__r.Space_Type_Lookup_Code__c ) 
                    && objExtCase.Booking_Unit__r.Inventory__r.Space_Type_Lookup_Code__c.equalsIgnoreCase( 'Hotel' ) ) ) {
                        isHotel = true;
                        showCurrentSection = false;
                }
                else {
                    isHotel = false;
                    showCurrentSection = true;
                }
                
                /*if (objExtCase.No_of_Months_for_Lease__c != null){
                    system.debug('!!!!!objExtCase.No_of_Months_for_Lease__c'+objExtCase.No_of_Months_for_Lease__c);
                    strSelectedMonth = objExtCase.No_of_Months_for_Lease__c;                    
                }*/
                if (objExtCase.RGS_Option__c != null){
                    strSelectedOption = objExtCase.RGS_Option__c;
                    displayOptions = true;
                }
                if (objExtCase.Years_in_Lease__c != null){
                    strLeaseYear = objExtCase.Years_in_Lease__c;
                }
                
                /*if (objExtCase.Point_of_Reference__c != null){
                    strSelectedReason = objExtCase.Point_of_Reference__c;
                }*/
                /*objCase.Lease_Commencement_Date__c = objExtCase.Lease_Commencement_Date__c;
                objCase.Lease_End_Date__c = objExtCase.Lease_End_Date__c;*/
                objBookingUnit = new Booking_Unit__c(Id = objExtCase.Booking_Unit__c); 
                spaType = objExtCase.Booking_Unit__r.SPA_Type__c;
                objBookingUnit.RGS_Percent__c = objExtCase.Booking_Unit__r.RGS_Percent__c;
                CurrencyCode = objExtCase.CurrencyIsoCode;
                fetchInsertedDocuments(objExtCase.Id);
                system.debug('!!!!!!!strSelectedMonth'+strSelectedMonth);
            }
        return null;
    }
    
    public void fetchInsertedDocuments(Id caseId) {
        listInsertedDocs = new list<SR_Attachments__c>();
        
         if (caseId != null)  {
             for (
                    SR_Attachments__c objSRAttachment :
                        [Select isValid__c,
                                Name,
                                Id,
                                Case__c,
                                Attachment__c,
                                View__c,
                                Attachment_URL__c,
                                Type__c
                         From SR_Attachments__c
                         Where Case__c =: caseId 
                         And Attachment_URL__c != null
                         And Type__c = 'Passport'
                        ]
            ) {
                listInsertedDocs.add(objSRAttachment);
            }
        }
    }
    
    public pageReference DocumentURL() {
        
        for (Attachment objAttachment : [Select Id
                                            , Name 
                                         From Attachment 
                                         Where Name LIKE 'Cover Letter%'
                                         and ParentId =: caseId limit 1]){
            system.debug('!!!!objAttachment'+objAttachment);
            docURL = URL.getSalesforceBaseUrl().toExternalForm()+ '/servlet/servlet.FileDownload?file='+ objAttachment.Id;  
            system.debug('!!!docURL'+docURL);                          
        }
        if (!string.isBlank(docURL)){
            blnDocGenerated  = true;           
        }
        return null;
    }
   /* public void callDrawloop(){
        if (objBookingUnit != null && objBookingUnit.RGS_Percent__c == null) {
            executeBatch(System.Label.LMA_existing_non_resident_DDP, 
                System.Label.LMA_existing_non_resident_Template, 'Agreement');
        } else if (objBookingUnit != null && objBookingUnit.RGS_Percent__c != null) {
            executeBatch(System.Label.LMA_with_RGS_DDP, 
                System.Label.LMA_with_RGS_Template, 'RGSAgreement');
        }
    }*/
    
    /*public void callRGSAgreement() {
        executeBatch(System.Label.LMA_with_RGS_DDP, System.Label.LMA_with_RGS_Template, 'RGSAgreement');
    }*/
    
    /*public void callRequest(){
        executeBatch(System.Label.LMA_Request_DDP, System.Label.LMA_Request_Template, 'Request');
    }*/
    
    /*public void executeBatch(Id DDPId, String templateId, String docName){
        if (caseId == null) {
            caseId = strCaseID;
        }
        GenerateDrawloopDocumentBatch objInstance = new GenerateDrawloopDocumentBatch(caseId
                                                                                      , DDPId
                                                                                      , templateId);
        Id batchId = Database.ExecuteBatch(objInstance);
        if(String.valueOf(batchId) != '000000000000000'){
            isBatchRunning = true;
            SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
            objCaseAttachment.Case__c = caseId ;
            if (docName == 'Request') {
                objCaseAttachment.Name = 'Signed LMA Request';
            } else if (docName == 'Agreement') {
                objCaseAttachment.Name = 'Signed Agreement - Non resident';
            } else if (docName == 'RGSAgreement') {
                objCaseAttachment.Name = 'Signed Agreement with RGS';
            }
            objCaseAttachment.Booking_Unit__c = objBookingUnit.Id;            
            insert objCaseAttachment;
            
            if (docName == 'Agreement' || docName == 'RGSAgreement') {
                Case updateCase = new Case(Id = caseId);
                updateCase.Agreement_Executed__c = true;
                update updateCase;
            } else if (docName == 'Request') {
                Case updateCase = new Case(Id = caseId);
                updateCase.Agreement_Signed__c = true;
                update updateCase;
            }
        }
    }*/
    
    public void callRequest(){
        executeBatch(System.Label.LHO_DDP_Id, System.Label.LHO_Cover_Letter_Template_Id);
    }
    
    public void executeBatch(Id DDPId, String templateId) {
        if (caseId == null) {
            caseId = strCaseID;
        }
        GenerateDrawloopDocumentBatch objInstance = new GenerateDrawloopDocumentBatch(caseId
                                                                                      , DDPId
                                                                                      , templateId);
        Id batchId = Database.ExecuteBatch(objInstance);
        if(String.valueOf(batchId) != '000000000000000'){
            isBatchRunning = true;
            displayURL = true;            
        }
    }
}