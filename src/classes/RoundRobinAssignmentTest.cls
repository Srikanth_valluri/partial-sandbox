@isTest
private class RoundRobinAssignmentTest {

    @isTest
    private static void testChangeOwner() {
        
       insert new Skip_Task_Trigger_for_Informatica__c(
            SetupOwnerId = UserInfo.getOrganizationId(), 
            Enable__c = true
        ); 
        Profile p2 = [SELECT Id FROM Profile WHERE Name='System Administrator' Limit 1];
        //ID adminRoleId = [ Select id from userRole where name = 'Director'].id;
        //UserRole userRoleObj = new UserRole(Name = 'Director', DeveloperName = 'MyCustomRole');
        //insert userRoleObj;
        User u2 = new User(Alias = 'standt2', Email='stand2@testorg.com', isActive = true,
                           EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p2.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='adm@testorg123.com');
        insert u2;
        
        User u3 = new User(Alias = 'standt2', Email='stand2@testorg.com', isActive = true,
                           EmailEncodingKey='UTF-8', LastName='Testing34', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p2.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='adm@testorg123.com');
        //insert u3;
        
        // Test data setup
        Round_Robin_Owner_Assignment__c r1 = new Round_Robin_Owner_Assignment__c( 
                                                    name = 'test1'
                                                    , Owner_Id__c = String.valueOf(userInfo.getUserId())
                                                    , Is_Current_Owner__c = true
                                                    , Order__c = 1
                                                    , Process_Name__c = 'POP' );
        Round_Robin_Owner_Assignment__c r2 = new Round_Robin_Owner_Assignment__c( 
                                                    name = 'test2'
                                                    , Owner_Id__c = String.valueOf(u2.Id)
                                                    , Is_Current_Owner__c = false
                                                    , Order__c = 2
                                                    , Process_Name__c = 'POP' );
        insert new List<Round_Robin_Owner_Assignment__c>{ r1, r2 };
        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POP').getRecordTypeId();

        Case objCase = new Case();
        objCase.recordTypeId = caseRecTypeId;
        insert objCase;

        /*Task t1 = new Task();
        t1.WhatId = objCase.Id;
        t1.Process_name__c = 'POP';
        t1.Assigned_User__c = 'Finance';*/
        List<Task> taskListToInsert = new List<Task>();
        //for(Integer i=0; i<=4;i++) {
            Task objTask = new Task();
            objTask.Subject = 'Verify Proof of Payment Details in IPMS';
            objTask.Assigned_User__c = 'Finance';
            objTask.ActivityDate = Date.today()+2;
            //objTask.OwnerId = objCase.OwnerId;
            objTask.Priority = 'Normal';
            objTask.CurrencyIsoCode = objCase.CurrencyIsoCode;
            objTask.Status = 'Not Started';
            objTask.WhatId = objCase.Id;
            objTask.Process_Name__c = 'POP';
            taskListToInsert.add(objTask);
        //}
        
        // Actual test
        Test.startTest();
           //RoundRobinAssignment.changeOwner(taskListToInsert);
           insert taskListToInsert;
        Test.stopTest();

        // Asserts
    }

}