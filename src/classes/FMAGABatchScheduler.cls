/*-------------------------------------------------------------------------------------------------
Description: Scheduler to schedule batch FMAGABatch everyday morning 8:00AM

============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 03-01-2019       | Lochana Rajput   | 1. Added scheduler logic
=============================================================================================================================
*/
public with sharing class FMAGABatchScheduler implements Schedulable{
	public static String cronExp = '0 0 17 ? * * *';

	public static String jobName = 'FMAGABatch';
	public void scheduleMyJob() {
		FMAGABatchScheduler batchObj = new FMAGABatchScheduler();
		System.schedule(jobName, cronExp, batchObj);
	}
	public void execute(SchedulableContext sc) {
		FMAGABatch batchObj = new FMAGABatch();
		Database.executeBatch(batchObj, 200);
	}
}