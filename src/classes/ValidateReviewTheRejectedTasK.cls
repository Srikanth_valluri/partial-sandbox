//SkipValidationForUser__c .getAll();
public with sharing class ValidateReviewTheRejectedTasK {
    public static void methodValidateReviewTheRejectedTask(Task taskObj){
        Id profileId= userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        system.debug('ProfileName : '+profileName);
        system.debug('taskObj : '+taskObj);
        system.debug('taskObj.whatId :'+taskObj.whatId);
        Case Objcase=[ SELECT id,OwnerId
                                ,(SELECT id
                                    ,Booking_Unit__c 
                                    From SR_Booking_Units__r
                                )
                            From Case
                            WHERE id =: taskObj.whatId
                            AND status != 'Closed'
                            AND status != 'Completed'
                            AND status != 'Cancelled'
                            LIMIT 1];

            System.debug('Objcase == ' +  Objcase);
            
            User objUser  = [SELECT Id,Division,ManagerId,Manager_Profile__c,DelegatedApproverId FROM User where Id =: Objcase.OwnerId Limit 1];
            System.debug('objUser == ' +  objUser);
            
            Boolean SkipValidation = false;
            
            if( objUser!= NULL && String.isNotBlank(objUser.Division) && objUser.Division == 'MidOffice'){
                SkipValidation = true;
            }
            
        if(profileName == 'Collection - CRE' && taskObj != NULL && taskObj.whatId != NULL  && !SkipValidation){
            
            
            Set<Id> setBuId =new Set<Id>();
            if( Objcase != NULL  && Objcase.SR_Booking_Units__r != NULL && Objcase.SR_Booking_Units__r.size()>0 ){
                for (SR_Booking_Unit__c objSRBooking : Objcase.SR_Booking_Units__r) {
                    setBuId.add(objSRBooking.Booking_Unit__c);
                }
                System.debug('setBuId == ' +  setBuId);
            }

            Set<Id> recTypeIdSet=new Set<Id>();
            List<String> lstRecordType = new List<String> {'Collections Calling List','Lease Handover Calling List','Handover Calling List','Early Handover Calling List'};
            for(RecordType rec :[SELECT DeveloperName,Id,Name 
                                FROM RecordType 
                                WHERE SobjectType = 'Calling_List__c' 
                                And Name IN:lstRecordType ]){
                recTypeIdSet.add(rec.Id);
            }
            System.debug('recTypeIdSet == ' +  recTypeIdSet);
            List< Calling_List__c> lstCallingList = new List< Calling_List__c>();
            if( setBuId != NULL  && recTypeIdSet != NULL  && setBuId.size() > 0  && recTypeIdSet.size() > 0 ){
                lstCallingList = [Select Id
                                        ,CE_Comments__c
                                        ,Booking_Unit__c
                                From 
                                    Calling_List__c
                                WHERE 
                                    RecordTypeId IN :recTypeIdSet 
                                    AND IsHideFromUI__c = false AND Account__c != null 
                                    AND Booking_Unit__c IN: setBuId 
                                    AND OwnerId =: UserInfo.getUserId()
                                    AND (Calling_List_Type__c = 'Collection Calling' OR
                                          Calling_List_Type__c = 'Advance List' )
                                    ORDER BY CreatedDate DESC];

                System.debug('lstCallingList == ' +  lstCallingList);
            }
            
            
            

            if(lstCallingList == NULL || lstCallingList.size() == 0 ){
                taskObj.addError('Please call/email and update the notes cl no.  to proceed for task closer');
            }


            Map<Id,Calling_List__c> mapBUIdCallingList = new Map<Id,Calling_List__c>(); 
            // fill the map 
            if( lstCallingList != NULL && lstCallingList.size() > 0 ){
                for( Calling_List__c objCallingList : lstCallingList) {
                    if( objCallingList != null && objCallingList.Booking_Unit__c != null ) {
                         mapBUIdCallingList.put( objCallingList.Booking_Unit__c,objCallingList);
                    }
                    
                }
            }

            System.debug('mapBUIdCallingList == ' +  mapBUIdCallingList);


            for (Id key : mapBUIdCallingList.keySet()) {
                system.debug('in map iterator');
                Calling_List__c objCallingList  = mapBUIdCallingList.get(key);
                system.debug('objCallingList :'+ objCallingList);
                system.debug('objCallingList.CE_Comments__c :'+ objCallingList.CE_Comments__c);
                if(objCallingList.CE_Comments__c == NULL || String.isBlank(objCallingList.CE_Comments__c))
                    taskObj.addError('Please call/email and update the notes cl no.  to proceed for task closer');    
            }
            
        }
    }
}