@isTest
public class MergePartiesController_TC {
    
    public static String COMMUNITY_USER_PROFILE = 'Customer Community Login User(Use this)';

    static testMethod void testMethod1() {
        NSIBPM__SR_Status__c srStatus = new NSIBPM__SR_Status__c ();
        srStatus.Name = 'Rejected';
        srStatus.NSIBPM__Code__c = '1234';
        insert srStatus;
        
        NSIBPM__SR_Template__c srTemplate = new NSIBPM__SR_Template__c ();
        srTemplate.Name = 'test';
        srTemplate.NSIBPM__SR_RecordType_API_Name__c = 'test';
        insert srTemplate;
        
        NSIBPM__Service_Request__c serviceRequest = new NSIBPM__Service_Request__c ();
        serviceRequest.Booking_No__c = '123';
        serviceRequest.NSIBPM__Internal_SR_Status__c = srStatus.ID;
        insert serviceRequest;
        
        Booking__c bookingRecord = new Booking__c ();
        bookingRecord.Booking_SR__c = serviceRequest.ID;
        bookingRecord.Deal_SR__c = serviceRequest.ID;
        insert bookingRecord;
        
        Account masterAccount = New Account ();
        masterAccount.LastName = 'test';
        masterAccount.RecordtypeID = '0120Y000000DjCf';
        masterAccount.Party_ID__c = '657540';
        insert masterAccount;
        
        system.debug('masterAccount'+masterAccount);
        
        Account mergeAccount = New Account ();
        mergeAccount.LastName = 'test';
        mergeAccount.RecordtypeID = '0120Y000000DjCf';
        mergeAccount.Party_ID__c = '97851040';
        insert mergeAccount;
        system.debug('mergeAccount'+mergeAccount);
        Account mergeAccount2 = New Account ();
        mergeAccount2.LastName = 'test';
        mergeAccount2.RecordtypeID = '0120Y000000DjCf';
        mergeAccount2.Party_ID__c = '9731045';
        insert mergeAccount2;
        system.debug('mergeAccount2'+mergeAccount2);
        string mergeAccountsList = mergeAccount.Party_ID__c+','+mergeAccount2.Party_ID__c;
        
        system.debug('mergeAccountsList '+mergeAccountsList);
        Buyer__c buyer = new Buyer__c ();
        buyer.IPMS_Registration_ID__c = '24154';
        buyer.Date_of_Birth__c = String.valueOf ( system.today ().addYears (-22) );
        buyer.Passport_Expiry_Date__c = String.valueOf (system.today ().addYears (22));
        buyer.Place_of_Issue__c = 'Test';       
        buyer.Buyer_Type__c ='Individual';
        buyer.Title__c = 'MISS.';
        buyer.Nationality__c = 'Indian';
        buyer.First_Name__c = 'test1';
        buyer.Account__c = masterAccount.ID;
        buyer.Last_Name__c = 'test2';
        buyer.Email__c = 'test@gmail.com';
        buyer.Passport_Number__c = '123456788';
        buyer.DOB__c = system.today ().addYears (-22);
        buyer.Passport_Expiry__c = system.today ().addDays (365) ;
        buyer.Phone_Country_Code__c = 'India: 0091';
        buyer.Phone__c = '8977365305';
        buyer.Country__c = 'India';
        buyer.City__c = 'Hyderabad';            
        buyer.Booking__c = bookingRecord .id;
        buyer.status__c ='new';
        buyer.Address_Line_1__c = 'testing';     
        //buyer.CR_Registration_Expiry_Date_New__c = system.today ().addDays (1265); 
        insert buyer;
        
        Booking__c booking = new Booking__c ();
        booking.Booking_SR__c = serviceRequest.ID;
        booking.Deal_SR__c = serviceRequest.ID;
        booking.Account__c = mergeAccount2.ID;
        booking.Party_ID__c = mergeAccount2.Party_ID__c;
        insert booking;
        MergePartiesController.mergeAccounts(masterAccount.Party_ID__c,mergeAccountsList);
        MergePartiesController.getAllFields('Account');
        MergePartiesController.getLookupRecords('Account','test','test','test','Person Account');
        MergePartiesController.getLookupRecords('Account','test','test','test','');
        MergePartiesController.getLookupRecords('Account','test','test','','Person Account');
        
    }
    static testMethod void testMethod2() {
        NSIBPM__SR_Status__c srStatus = new NSIBPM__SR_Status__c ();
        srStatus.Name = 'Rejected';
        srStatus.NSIBPM__Code__c = '1234';
        insert srStatus;
        NSIBPM__SR_Template__c srTemplate = new NSIBPM__SR_Template__c ();
        srTemplate.Name = 'test';
        srTemplate.NSIBPM__SR_RecordType_API_Name__c = 'test';
        insert srTemplate;
        NSIBPM__Service_Request__c serviceRequest = new NSIBPM__Service_Request__c ();
        serviceRequest.Booking_No__c = '123';
        serviceRequest.NSIBPM__Internal_SR_Status__c = srStatus.ID;
        insert serviceRequest;
        
        Booking__c bookingRecord = new Booking__c ();
        bookingRecord.Booking_SR__c = serviceRequest.ID;
        bookingRecord.Deal_SR__c = serviceRequest.ID;
        insert bookingRecord;
        Account masterAccount = New Account ();
        masterAccount.LastName = 'test';
        masterAccount.RecordtypeID = '0120Y000000DjCf';
        masterAccount.Party_ID__c = '';
        insert masterAccount;
        
        Account mergeAccount = New Account ();
        mergeAccount.LastName = 'test';
        mergeAccount.RecordtypeID = '0120Y000000DjCf';
        mergeAccount.Party_ID__c = '12345';
        insert mergeAccount;
        
        Buyer__c buyer = new Buyer__c ();
        buyer.IPMS_Registration_ID__c = '24154';
        buyer.Date_of_Birth__c = String.valueOf ( system.today ().addYears (-22) );
        buyer.Passport_Expiry_Date__c = String.valueOf (system.today ().addYears (22));
        buyer.Place_of_Issue__c = 'Test';
        buyer.Buyer_Type__c ='Individual';
        buyer.Title__c = 'MISS.';
        buyer.Nationality__c = 'Indian';
        buyer.First_Name__c = 'test1';
        buyer.Account__c = masterAccount.ID;
        buyer.Last_Name__c = 'test2';
        buyer.Email__c = 'test@gmail.com';
        buyer.Passport_Number__c = '123456788';
        buyer.DOB__c = system.today ().addYears (-22);
        buyer.Passport_Expiry__c = system.today ().addDays (365) ;
        buyer.Phone_Country_Code__c = 'India: 0091';
        buyer.Phone__c = '8977365305';
        buyer.Country__c = 'India';
        buyer.City__c = 'Hyderabad';            
        buyer.Booking__c = bookingRecord .id;
        buyer.status__c ='new';
        buyer.Address_Line_1__c = 'testing';
        //buyer.CR_Registration_Expiry_Date_New__c = system.today ().addDays (1265); 
        insert buyer;
        
        
        Account mergeAccount2 = New Account ();
        mergeAccount2.LastName = 'test';
        mergeAccount2.RecordtypeID = '0120Y000000DjCf';
        mergeAccount2.Party_ID__c = '123456';
        insert mergeAccount2;
        string mergeAccountsList = mergeAccount.Party_ID__c+','+mergeAccount2.Party_ID__c;
        Booking__c booking = new Booking__c ();
        booking.Booking_SR__c = serviceRequest.ID;
        booking.Deal_SR__c = serviceRequest.ID;
        booking.Account__c = mergeAccount2.ID;
        booking.Party_ID__c = mergeAccount2.Party_ID__c;
        insert booking;
        
        MergePartiesController.mergeAccounts(masterAccount.Party_ID__c,mergeAccountsList);
        MergePartiesController.getAllFields('');
        MergePartiesController.getLookupRecords('Account','test','test','test','Person Account');
        MergePartiesController.getLookupRecords('Account','test','test','test','');
        MergePartiesController.getLookupRecords('Account','test','test','','Person Account');
        
    }

    public static account createPersonAccount(){
        Id personAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account Accnt = new Account(RecordTypeID = personAccountId,
                                    FirstName = 'Test FName',
                                    LastName = 'Test LName',
                                    PersonMailingStreet = 'test@yahoo.com',
                                    PersonMailingPostalCode = '12345',
                                    PersonMailingCity = 'SFO',
                                    PersonHomePhone = '1234567',
                                    PersonMobilePhone = '12345678',
                                    PersonEmail = 'testPersonmail@mailinator.com',
                                    Mobile__c = '9988559966',
                                    Party_ID__c = '12345');
        return Accnt;
    }   
    
    static testMethod void testMethod3() {
        //User communityUser = CommunityTestDataFactory.createPortalUser();
        
        Id profileId = [select id from profile where name = :COMMUNITY_USER_PROFILE].id;

        Account objAccount = CreatePersonAccount();
        insert objAccount;

        Account mergeAccount = New Account ();
        mergeAccount.LastName = 'test';
        mergeAccount.RecordtypeID = '0120Y000000DjCf';
        mergeAccount.Party_ID__c = '123456';
        insert mergeAccount;        
        
        Account masterAccount = [Select Party_ID__c,PersonContactId From Account Where Id = :objAccount.Id];
            Account mergeAccount2 = [Select Party_ID__c,PersonContactId From Account Where Id = :mergeAccount.Id];
            
        system.debug('objAccount-------------'+masterAccount);
        system.debug('objAccount-------------'+masterAccount.PersonContactId);
        
        User user = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = profileId, country='United States',IsActive =true,
                ContactId = masterAccount.PersonContactId,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');

        insert user;
        
        NSIBPM__SR_Status__c srStatus = new NSIBPM__SR_Status__c ();
            srStatus.Name = 'Rejected';
            srStatus.NSIBPM__Code__c = '1234';
            insert srStatus;
            NSIBPM__SR_Template__c srTemplate = new NSIBPM__SR_Template__c ();
            srTemplate.Name = 'test';
            srTemplate.NSIBPM__SR_RecordType_API_Name__c = 'test';
            insert srTemplate;
            NSIBPM__Service_Request__c serviceRequest = new NSIBPM__Service_Request__c ();
            serviceRequest.Booking_No__c = '123';
            serviceRequest.NSIBPM__Internal_SR_Status__c = srStatus.ID;
            insert serviceRequest;
            
            Booking__c bookingRecord = new Booking__c ();
            bookingRecord.Booking_SR__c = serviceRequest.ID;
            bookingRecord.Deal_SR__c = serviceRequest.ID;
            insert bookingRecord;

            

            Buyer__c buyer = new Buyer__c ();
            buyer.IPMS_Registration_ID__c = '24154';
            buyer.Date_of_Birth__c = String.valueOf ( system.today ().addYears (-22) );
            buyer.Passport_Expiry_Date__c = String.valueOf (system.today ().addYears (22));
            buyer.Place_of_Issue__c = 'Test';
            buyer.Buyer_Type__c ='Individual';
            buyer.Title__c = 'MISS.';
            buyer.Nationality__c = 'Indian';
            buyer.First_Name__c = 'test1';
            buyer.Account__c = masterAccount.ID;
            buyer.Last_Name__c = 'test2';
            buyer.Email__c = 'test@gmail.com';
            buyer.Passport_Number__c = '123456788';
            buyer.DOB__c = system.today ().addYears (-22);
            buyer.Passport_Expiry__c = system.today ().addDays (365) ;
            buyer.Phone_Country_Code__c = 'India: 0091';
            buyer.Phone__c = '8977365305';
            buyer.Country__c = 'India';
            buyer.City__c = 'Hyderabad';            
            buyer.Booking__c = bookingRecord .id;
            buyer.status__c ='new';
            buyer.Address_Line_1__c = 'testing';
            //buyer.CR_Registration_Expiry_Date_New__c = system.today ().addDays (1265); 
            insert buyer;
            
            

            
            string mergeAccountsList = mergeAccount2.Party_ID__c;
            Booking__c booking = new Booking__c ();
            booking.Booking_SR__c = serviceRequest.ID;
            booking.Deal_SR__c = serviceRequest.ID;
            booking.Account__c = mergeAccount2.ID;
            booking.Party_ID__c = mergeAccount2.Party_ID__c;
            insert booking;
        
        System.runAs(user) {
        
            MergePartiesController.mergeAccounts(masterAccount.Party_ID__c,mergeAccountsList);
            MergePartiesController.getAllFields('');
            MergePartiesController.getLookupRecords('Account','test','test','test','Person Account');
            MergePartiesController.getLookupRecords('Account','test','test','test','');
            MergePartiesController.getLookupRecords('Account','test','test','','Person Account');
        }
    }
    
    static testMethod void testMethod4() {
        //User communityUser = CommunityTestDataFactory.createPortalUser();
        
        Id profileId = [select id from profile where name = :COMMUNITY_USER_PROFILE].id;

        Account objAccount = CreatePersonAccount();
        insert objAccount;

        Account mergeAccount = New Account ();
        mergeAccount.LastName = 'test';
        mergeAccount.RecordtypeID = '0120Y000000DjCf';
        mergeAccount.Party_ID__c = '123456';
        insert mergeAccount;    
        
        Account masterAccount = [Select Party_ID__c,PersonContactId From Account Where Id = :objAccount.Id];
        Account mergeAccount2 = [Select Party_ID__c,PersonContactId From Account Where Id = :mergeAccount.Id];
        
        system.debug('objAccount-------------'+mergeAccount2);
        system.debug('objAccount-------------'+mergeAccount2.PersonContactId);
        
        User user = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = profileId, country='United States',IsActive =true,
                ContactId = mergeAccount2.PersonContactId,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');

        insert user;
        

        
        NSIBPM__SR_Status__c srStatus = new NSIBPM__SR_Status__c ();
        srStatus.Name = 'Rejected';
        srStatus.NSIBPM__Code__c = '1234';
        insert srStatus;
        NSIBPM__SR_Template__c srTemplate = new NSIBPM__SR_Template__c ();
        srTemplate.Name = 'test';
        srTemplate.NSIBPM__SR_RecordType_API_Name__c = 'test';
        insert srTemplate;
        NSIBPM__Service_Request__c serviceRequest = new NSIBPM__Service_Request__c ();
        serviceRequest.Booking_No__c = '123';
        serviceRequest.NSIBPM__Internal_SR_Status__c = srStatus.ID;
        insert serviceRequest;
        
        Booking__c bookingRecord = new Booking__c ();
        bookingRecord.Booking_SR__c = serviceRequest.ID;
        bookingRecord.Deal_SR__c = serviceRequest.ID;
        insert bookingRecord;

        

        Buyer__c buyer = new Buyer__c ();
        buyer.IPMS_Registration_ID__c = '24154';
        buyer.Date_of_Birth__c = String.valueOf ( system.today ().addYears (-22) );
        buyer.Passport_Expiry_Date__c = String.valueOf (system.today ().addYears (22));
        buyer.Place_of_Issue__c = 'Test';
        buyer.Buyer_Type__c ='Individual';
        buyer.Title__c = 'MISS.';
        buyer.Nationality__c = 'Indian';
        buyer.First_Name__c = 'test1';
        buyer.Account__c = masterAccount.ID;
        buyer.Last_Name__c = 'test2';
        buyer.Email__c = 'test@gmail.com';
        buyer.Passport_Number__c = '123456788';
        buyer.DOB__c = system.today ().addYears (-22);
        buyer.Passport_Expiry__c = system.today ().addDays (365) ;
        buyer.Phone_Country_Code__c = 'India: 0091';
        buyer.Phone__c = '8977365305';
        buyer.Country__c = 'India';
        buyer.City__c = 'Hyderabad';            
        buyer.Booking__c = bookingRecord .id;
        buyer.status__c ='new';
        buyer.Address_Line_1__c = 'testing';
        //buyer.CR_Registration_Expiry_Date_New__c = system.today ().addDays (1265); 
        insert buyer;
        
        

        
        string mergeAccountsList = mergeAccount2.Party_ID__c;
        Booking__c booking = new Booking__c ();
        booking.Booking_SR__c = serviceRequest.ID;
        booking.Deal_SR__c = serviceRequest.ID;
        booking.Account__c = mergeAccount2.ID;
        booking.Party_ID__c = mergeAccount2.Party_ID__c;
        insert booking;
        
        System.runAs(user) {
            
            MergePartiesController.mergeAccounts(masterAccount.Party_ID__c,mergeAccountsList);
            MergePartiesController.getAllFields('');
            MergePartiesController.getLookupRecords('Account','test','test','test','Person Account');
            MergePartiesController.getLookupRecords('Account','test','test','test','');
            MergePartiesController.getLookupRecords('Account','test','test','','Person Account');
        }
    }
    
    static testMethod void testMethod5() {
        //User communityUser = CommunityTestDataFactory.createPortalUser();
        
        Id profileId = [select id from profile where name = :COMMUNITY_USER_PROFILE].id;

        Account objAccount = CreatePersonAccount();
        insert objAccount;

        Account mergeAccount = New Account ();
        mergeAccount.LastName = 'test';
        mergeAccount.RecordtypeID = '0120Y000000DjCf';
        mergeAccount.Party_ID__c = '123456';
        insert mergeAccount;        
        
        Account mergeAccount2 = [Select Party_ID__c,PersonContactId From Account Where Id = :mergeAccount.Id];
        Account masterAccount = [Select Party_ID__c,PersonContactId From Account Where Id = :objAccount.Id];
        system.debug('objAccount-------------'+mergeAccount2);
        system.debug('objAccount-------------'+mergeAccount2.PersonContactId);
        system.debug('objAccount-------------'+masterAccount);
        system.debug('objAccount-------------'+masterAccount.PersonContactId);

        
        User user1 = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = profileId, country='United States',IsActive =true,
                ContactId = mergeAccount2.PersonContactId,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');

        insert user1;
        
        User user = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = profileId, country='United States',IsActive =true,
                ContactId = masterAccount.PersonContactId,
                timezonesidkey='America/Los_Angeles', username='tester@noemail1.com');

        insert user;
    
        
        NSIBPM__SR_Status__c srStatus = new NSIBPM__SR_Status__c ();
        srStatus.Name = 'Rejected';
        srStatus.NSIBPM__Code__c = '1234';
        insert srStatus;
        NSIBPM__SR_Template__c srTemplate = new NSIBPM__SR_Template__c ();
        srTemplate.Name = 'test';
        srTemplate.NSIBPM__SR_RecordType_API_Name__c = 'test';
        insert srTemplate;
        NSIBPM__Service_Request__c serviceRequest = new NSIBPM__Service_Request__c ();
        serviceRequest.Booking_No__c = '123';
        serviceRequest.NSIBPM__Internal_SR_Status__c = srStatus.ID;
        insert serviceRequest;
        
        Booking__c bookingRecord = new Booking__c ();
        bookingRecord.Booking_SR__c = serviceRequest.ID;
        bookingRecord.Deal_SR__c = serviceRequest.ID;
        insert bookingRecord;

        

        Buyer__c buyer = new Buyer__c ();
        buyer.IPMS_Registration_ID__c = '24154';
        buyer.Date_of_Birth__c = String.valueOf ( system.today ().addYears (-22) );
        buyer.Passport_Expiry_Date__c = String.valueOf (system.today ().addYears (22));
        buyer.Place_of_Issue__c = 'Test';
        buyer.Buyer_Type__c ='Individual';
        buyer.Title__c = 'MISS.';
        buyer.Nationality__c = 'Indian';
        buyer.First_Name__c = 'test1';
        buyer.Account__c = masterAccount.ID;
        buyer.Last_Name__c = 'test2';
        buyer.Email__c = 'test@gmail.com';
        buyer.Passport_Number__c = '123456788';
        buyer.DOB__c = system.today ().addYears (-22);
        buyer.Passport_Expiry__c = system.today ().addDays (365) ;
        buyer.Phone_Country_Code__c = 'India: 0091';
        buyer.Phone__c = '8977365305';
        buyer.Country__c = 'India';
        buyer.City__c = 'Hyderabad';            
        buyer.Booking__c = bookingRecord .id;
        buyer.status__c ='new';
        buyer.Address_Line_1__c = 'testing';
        //buyer.CR_Registration_Expiry_Date_New__c = system.today ().addDays (1265); 
        insert buyer;
        
        

        
        string mergeAccountsList = mergeAccount2.Party_ID__c;
        Booking__c booking = new Booking__c ();
        booking.Booking_SR__c = serviceRequest.ID;
        booking.Deal_SR__c = serviceRequest.ID;
        booking.Account__c = mergeAccount2.ID;
        booking.Party_ID__c = mergeAccount2.Party_ID__c;
        insert booking;

        System.runAs(user) {
            MergePartiesController.mergeAccounts(masterAccount.Party_ID__c,mergeAccountsList);
            MergePartiesController.getAllFields('');
            MergePartiesController.getLookupRecords('Account','test','test','test','Person Account');
            MergePartiesController.getLookupRecords('Account','test','test','test','');
            MergePartiesController.getLookupRecords('Account','test','test','','Person Account');
        }
    }   
}