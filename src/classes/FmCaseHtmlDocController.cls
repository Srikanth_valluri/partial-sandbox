/**
 * @File Name          : FmCaseHtmlDocController.cls 
 * @Description        : Called from FinalNoticeHTMLDocument and FirstNoticeHTMLDocument page
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 10/16/2019, 4:36:02 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/10/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public without sharing class FmCaseHtmlDocController { 
    public fm_case__c srObj                                             {get; set;}
    public String todaysDate                                               {get; set;}
    public FmCaseHtmlDocController() {
        
        todaysDate = System.Today().format(); 
        srObj = new fm_case__c();
        String pJSONString = Apexpages.currentPage().getParameters().get('srJSON');
        System.debug('In pdf page :'+pJSONString);
        fm_case__c deserializedSR = 
            (fm_case__c)JSON.deserialize(pJSONString, fm_case__c.class);
        system.debug('---deserializedSR>>>>>>  ' + deserializedSR); 
        srObj = deserializedSR;
    }
}