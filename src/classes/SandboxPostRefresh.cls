global without sharing class SandboxPostRefresh implements SandboxPostCopy, Database.Batchable<SObject> {
    
    global void runApexClass(SandboxContext context) {
        
        //For Accounts masking
        string strQry = 'select Id,Email__c,Email__pc,Email_2__c,Email_2__pc,Email_3__c,Email_3__pc,Email_4__pc,Email_5__pc,PersonEmail,Mobile__c,Mobile_Phone_Encrypt__pc from Account';
        SandboxPostRefresh objSandboxPostRefresh = new SandboxPostRefresh();
        objSandboxPostRefresh.sQry = strQry;
        Database.executeBatch( objSandboxPostRefresh );

        //for Case
        strQry = 'select Id,SuppliedEmail,Email__c  from Case';
        objSandboxPostRefresh = new SandboxPostRefresh();
        objSandboxPostRefresh.sQry = strQry;
        Database.executeBatch( objSandboxPostRefresh );

        //Inquiry
        strQry = 'select Id,Email__c,Email_2__c,Email_3__c,Email_4__c,Email_5__c,Mobile_Phone__c,Mobile_Phone_2__c,Mobile_Phone_3__c,Mobile_Phone_4__c,Mobile_Phone_5__c from Inquiry__c';
        objSandboxPostRefresh = new SandboxPostRefresh();
        objSandboxPostRefresh.sQry = strQry;
        Database.executeBatch( objSandboxPostRefresh );

        //FM Case
        strQry = 'select Id,Email__c,Contact_Email__c,Account_Email__c,Email_2__c,Email_3__c,Email_4__c,Email_5__c from FM_Case__c';
        objSandboxPostRefresh = new SandboxPostRefresh();
        objSandboxPostRefresh.sQry = strQry;
        Database.executeBatch( objSandboxPostRefresh );
    }

    public string sQry = '';
    public List<Id> lstRecordIds = new list<Id>();
    
    public SandboxPostRefresh(string str, List<Id> lstIds) {
        lstRecordIds = lstIds;
        sQry = str;
    }

    public SandboxPostRefresh() {
    }

    public Database.Querylocator start( Database.BatchableContext bc ) {
        return sQry != '' ? Database.getQueryLocator(sQry) : null;
    }

    public void execute( Database.BatchableContext bc, list<sObject> scope ) {
        if(scope[0].getSObjectType().getDescribe().getName() == 'Account'){
            //Email__c,Email__pc,Email_2__c,Email_2__pc,Email_3__c,Email_3__pc,Email_4__pc,Email_5__pc,PersonEmail,Mobile__c,Mobile_Phone_Encrypt__pc
            for(Account obj : (list<Account>)scope){
                obj.Email__c = getMaskedEmail(obj.Email__c);
                obj.Email__pc = getMaskedEmail(obj.Email__pc);
                obj.Email_2__c = getMaskedEmail(obj.Email_2__c);
                obj.Email_2__pc = getMaskedEmail(obj.Email_2__pc);
                obj.Email_3__c = getMaskedEmail(obj.Email_3__c);
                obj.Email_3__pc = getMaskedEmail(obj.Email_3__pc);
                obj.Email_4__pc = getMaskedEmail(obj.Email_4__pc);
                obj.Email_5__pc = getMaskedEmail(obj.Email_5__pc);
                obj.PersonEmail = getMaskedEmail(obj.PersonEmail);
                obj.Mobile__c = getMaskedPhone(obj.Mobile__c);
                obj.Mobile_Phone_Encrypt__pc = getMaskedPhone(obj.Mobile_Phone_Encrypt__pc);
            }
            update scope;
        }else if(scope[0].getSObjectType().getDescribe().getName() == 'Case'){
            //SuppliedEmail,Email__c
            for(Case obj : (list<Case>)scope){
                obj.SuppliedEmail = getMaskedEmail(obj.SuppliedEmail);
                obj.Email__c = getMaskedEmail(obj.Email__c);
            }
            update scope;
        }else if(scope[0].getSObjectType().getDescribe().getName() == 'Inquiry__c'){
            //Email__c,Email_2__c,Email_3__c,Email_4__c,Email_5__c,Mobile_Phone__c,Mobile_Phone_2__c,Mobile_Phone_3__c,Mobile_Phone_4__c,Mobile_Phone_5__c
            for(Inquiry__c obj : (list<Inquiry__c>)scope){
                obj.Email__c = getMaskedEmail(obj.Email__c);
                obj.Email_2__c = getMaskedEmail(obj.Email_2__c);
                obj.Email_3__c = getMaskedEmail(obj.Email_3__c);
                obj.Email_4__c = getMaskedEmail(obj.Email_4__c);
                obj.Email_5__c = getMaskedEmail(obj.Email_5__c);
                obj.Mobile_Phone__c = getMaskedPhone(obj.Mobile_Phone__c);
                obj.Mobile_Phone_2__c = getMaskedPhone(obj.Mobile_Phone_2__c);
                obj.Mobile_Phone_3__c = getMaskedPhone(obj.Mobile_Phone_3__c);
                obj.Mobile_Phone_4__c = getMaskedPhone(obj.Mobile_Phone_4__c);
                obj.Mobile_Phone_5__c = getMaskedPhone(obj.Mobile_Phone_5__c);
            }
            update scope;
        }else if(scope[0].getSObjectType().getDescribe().getName() == 'FM_Case__c'){
            //Email__c,Contact_Email__c,Account_Email__c,Email_2__c,Email_3__c,Email_4__c,Email_5__c
            for(FM_Case__c obj : (list<FM_Case__c>)scope){
                obj.Email__c = getMaskedEmail(obj.Email__c);
                obj.Contact_Email__c = getMaskedEmail(obj.Contact_Email__c);
                obj.Account_Email__c = getMaskedEmail(obj.Account_Email__c);
                obj.Email_2__c = getMaskedEmail(obj.Email_2__c);
                obj.Email_3__c = getMaskedEmail(obj.Email_3__c);
                obj.Email_4__c = getMaskedEmail(obj.Email_4__c);
                obj.Email_5__c = getMaskedEmail(obj.Email_5__c);
            }
            update scope;
        }
    }
    
    public void finish( Database.BatchableContext bc ) {
    }

    public static string getMaskedEmail(string email){
        return email != null ? (Math.random()+''+Math.random()+'@mask.random') : email;
    }
    public static string getMaskedPhone(string phone){
        return phone != null ? ('00971'+Math.random()) : phone;
    }
}