/*
 * Description : This class is used to update Calling List Remark and outcome 
 * Revision History:
 *
 *  Version          Author              Date           Description
 *  1.0                                 7/11/2017       Initial Draft
 *                                                      
 */
global class UpdateCallingListOutcome implements Database.Batchable<sObject>{
    String query;
    global Database.QueryLocator start(Database.BatchableContext BC){
        // Id callingListRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Collections Calling List').RecordTypeId;
         Set<Id> recTypeIdSet=new Set<Id>();
        for(RecordType rec :[SELECT DeveloperName,Id,Name 
                               FROM RecordType 
                               WHERE SobjectType = 'Calling_List__c' 
                                 And Name='Collections Calling List' ]){
                               /*And (Name='Collections Calling List' 
                               OR Name='DP Calling List' 
                               OR Name= 'Aging Calling List') ]){*/
            recTypeIdSet.add(rec.Id);
        }
        //query = 'SELECT Id,IsHideFromUI__c FROM Calling_List__c WHERE RecordTypeId IN :recTypeIdSet';
        String Actual_OutstandingLabel = Label.Actual_OutStanding_Of_Calling_List;
        query = 'SELECT Id,Actual_Outstanding__c,Call_Outcome__c,Remarks__c FROM Calling_List__c WHERE Actual_Outstanding__c ='+Actual_OutstandingLabel+' AND RecordTypeId IN :recTypeIdSet';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Calling_List__c> callingList){
        List<Calling_List__c>updateCallingList = new List<Calling_List__c>();
        for(Calling_List__c callingInst : callingList){
            callingInst.Call_Outcome__c = 'Paid';
            callingInst.Remarks__c = Label.Remark_of_Calling_List;
            updateCallingList.add(callingInst);
        }
        update updateCallingList;
        System.debug('updateCallingList:'+updateCallingList);
    }
    global void finish(Database.BatchableContext BC){
    }
}