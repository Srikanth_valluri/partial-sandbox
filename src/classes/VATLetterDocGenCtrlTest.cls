@isTest
public Class VATLetterDocGenCtrlTest {

    @isTest static void testgenerateVATletter() {

        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Property
        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;

        // Create Inventory
        Inventory__c objInv = TestDataFactory_CRM.createInventory( objProp.Id );
        objInv.Building_Code__c = 'PLOT';
        insert objInv;

        //Configure DDP details
        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c(Name = 'VAT Letter'
            , Drawloop_Document_Package_Id__c = '432424'
            , Delivery_Option_Id__c = '43424' );

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
          objBookingUnit.Okay_to_release_keys__c = true;
          objBookingUnit.Keys_Released__c = true;
            objBookingUnit.Inventory__c = objInv.Id;
        }

        insert bookingUnitList;

        PageReference redirectionUrl;
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());      
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController( bookingUnitList[0] );
            VATLetterDocGenCtrl objController = new VATLetterDocGenCtrl( standardControllerInstance );
            Test.setCurrentPageReference( new PageReference( 'Page.VATLetterDocGen' ));
            System.currentPageReference().getParameters().put('Id', bookingUnitList[0].Id);
            objController.generateVATletter();   
        Test.stopTest();
    }

    @isTest static void testPlotHoKrf_noDDPdetails() {

        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Property
        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;

        // Create Inventory
        Inventory__c objInv = TestDataFactory_CRM.createInventory( objProp.Id );
        objInv.Building_Code__c = 'PLOT';
        insert objInv;

        //Configure DDP details
        /*insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c(Name = 'Plot Handover KRF'
            , Drawloop_Document_Package_Id__c = '432424'
            , Delivery_Option_Id__c = '43424' );*/
        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
          objBookingUnit.Okay_to_release_keys__c = true;
          objBookingUnit.Keys_Released__c = true;
            objBookingUnit.Inventory__c = objInv.Id;
        }
        insert bookingUnitList;

        PageReference redirectionUrl;
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());      
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController( bookingUnitList[0] );
            VATLetterDocGenCtrl objController = new VATLetterDocGenCtrl( standardControllerInstance );
            Test.setCurrentPageReference( new PageReference( 'Page.VATLetterDocGen' ));
            System.currentPageReference().getParameters().put('Id', bookingUnitList[0].Id);
            objController.generateVATletter();   
        Test.stopTest();
    }

    @isTest static void testPlotHoKrf_noTemplateId() {

        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Property
        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;

        // Create Inventory
        Inventory__c objInv = TestDataFactory_CRM.createInventory( objProp.Id );
        objInv.Building_Code__c = 'PLOT';
        insert objInv;

        //Configure DDP details
        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c(Name = 'VAT Letter'
            //, Drawloop_Document_Package_Id__c = '432424'
            , Delivery_Option_Id__c = '43424' );
        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
          objBookingUnit.Okay_to_release_keys__c = true;
          objBookingUnit.Keys_Released__c = true;
            objBookingUnit.Inventory__c = objInv.Id;
        }
        insert bookingUnitList;

        PageReference redirectionUrl;
        Test.startTest();
             Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());      
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController( bookingUnitList[0] );
            VATLetterDocGenCtrl objController = new VATLetterDocGenCtrl( standardControllerInstance );
            Test.setCurrentPageReference( new PageReference( 'Page.VATLetterDocGen' ));
            System.currentPageReference().getParameters().put('Id', bookingUnitList[0].Id);
            objController.generateVATletter();   
        Test.stopTest();
    }

    @isTest static void testPlotHoKrf_noDeliveryId() {

        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Property
        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;

        // Create Inventory
        Inventory__c objInv = TestDataFactory_CRM.createInventory( objProp.Id );
        objInv.Building_Code__c = 'PLOT';
        insert objInv;

        //Configure DDP details
        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c(Name = 'VAT Letter'
            , Drawloop_Document_Package_Id__c = '432424');
            //, Delivery_Option_Id__c = '43424' 
        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
          objBookingUnit.Okay_to_release_keys__c = true;
          objBookingUnit.Keys_Released__c = true;
            objBookingUnit.Inventory__c = objInv.Id;
        }
        insert bookingUnitList;

        PageReference redirectionUrl;
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());      
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController( bookingUnitList[0] );
            VATLetterDocGenCtrl objController = new VATLetterDocGenCtrl( standardControllerInstance );
            Test.setCurrentPageReference( new PageReference( 'Page.VATLetterDocGen' ));
            System.currentPageReference().getParameters().put('Id', bookingUnitList[0].Id);
            objController.generateVATletter();   
        Test.stopTest();
    }
}