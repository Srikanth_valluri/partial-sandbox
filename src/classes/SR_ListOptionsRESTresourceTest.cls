@isTest
public class SR_ListOptionsRESTresourceTest {
    @isTest(seeAllData = true)
    static void  test1(){
        User owner1 = [SELECT id, name, profileId, profile.name, Contact.accountId 
                       FROM User WHERE profile.name = 'Customer Community Login User(Use this)' 
                       AND IsActive = TRUE AND Contact.accountId != NULL LIMIT 1];
        User tenant1 = [SELECT id, name, profileId, profile.name, Contact.accountId 
                       FROM User WHERE profile.name = 'Tenant Community Login User' 
                       AND IsActive = TRUE AND Contact.accountId != NULL LIMIT 1];
        
        String ownerAcc1_ID, tenentAcc1_ID;
        if(NULL != owner1 && NULL != owner1.Contact && NULL != owner1.Contact.accountId){
            ownerAcc1_ID = String.valueOf(owner1.Contact.accountId);
        }
        if(NULL != tenant1 && NULL != tenant1.Contact && NULL != tenant1.Contact.accountId){
            tenentAcc1_ID = String.valueOf(tenant1.Contact.accountId);
        }
        
        Id contctDetailChgCase_rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
        Case testConDetChgCase = new Case();
        testConDetChgCase.recordTypeId = contctDetailChgCase_rtId;
        testConDetChgCase.AccountId = ownerAcc1_ID;
        testConDetChgCase.type = 'Change of Contact Details';
        testConDetChgCase.SR_Type__c = 'Change of Contact Details';
        testConDetChgCase.Origin = 'Portal';
        testConDetChgCase.status = 'Draft Request';
        testConDetChgCase.Mobile_Country_Code__c = 'United Arab Emirates: 00971';
        testConDetChgCase.Contact_Mobile__c = '9988776655';
        testConDetChgCase.Contact_Email__c = 'name@test.com';
        testConDetChgCase.Country__c = 'United Arab Emirates';
        testConDetChgCase.Address__c = 'Test address';
        testConDetChgCase.City__c = 'Dubai';
        testConDetChgCase.State__c = 'State';
        testConDetChgCase.Postal_Code__c = '665544';
        testConDetChgCase.CRF_File_URL__c = 'cocd_file_url';
        testConDetChgCase.Additional_Doc_File_URL__c = 'additional_doc_file_url';
        insert testConDetChgCase;
        
        Id passportDetailChgCase_rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Passport Detail Update').getRecordTypeId();
        Case testPassDetChgCase = new Case();
        testPassDetChgCase.recordTypeId = passportDetailChgCase_rtId;
        testPassDetChgCase.AccountId = ownerAcc1_ID;
        testPassDetChgCase.type = 'Passport Detail Update SR';
        testPassDetChgCase.SR_Type__c = 'Passport Detail Update SR';
        testPassDetChgCase.Origin = 'Portal';
        testPassDetChgCase.status = 'Draft Request';
        testPassDetChgCase.Mobile_Country_Code__c = 'United Arab Emirates: 00971';
        testPassDetChgCase.Contact_Mobile__c = '9988776655';
        testPassDetChgCase.Contact_Email__c = 'name@test.com';
        testPassDetChgCase.Country__c = 'United Arab Emirates';
        testPassDetChgCase.Address__c = 'Test address';
        testPassDetChgCase.City__c = 'Dubai';
        testPassDetChgCase.State__c = 'State';
        testPassDetChgCase.Postal_Code__c = '665544';
        testPassDetChgCase.CRF_File_URL__c = 'cocd_file_url';
        testPassDetChgCase.Additional_Doc_File_URL__c = 'additional_doc_file_url';
        insert testPassDetChgCase;
        
        String baseUrl = URL.getOrgDomainUrl().toExternalForm(); /* URL.getSalesforceBaseUrl() */
        
        Test.startTest();
        
        RestRequest request = new RestRequest();
        request.requestUri = baseUrl + '/services/apexrest/srCase/options';
        request.httpMethod = 'GET';
        
        RestResponse response = new RestResponse();
		
        RestContext.request = request;
        RestContext.response = response;
        
        SR_ListOptionsRESTresource.doGet(); /* 1 */
        
        request.addParameter('account_id', ownerAcc1_ID);
        SR_ListOptionsRESTresource.doGet(); /* 2 */
        
        request.addParameter('account_id', tenentAcc1_ID);
        SR_ListOptionsRESTresource.doGet(); /* 3 */
        
        request.addParameter('account_id', String.valueOf(testPassDetChgCase.id));
        SR_ListOptionsRESTresource.doGet(); /* 4 */
        
        Test.stopTest();
    }
}