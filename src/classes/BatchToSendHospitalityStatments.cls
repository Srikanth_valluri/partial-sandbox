global class BatchToSendHospitalityStatments implements Database.Batchable<sObject>, Database.AllowsCallouts{
    Map<Integer, String> mapOfMonthNumToMonth = new Map<Integer, String>{1 => 'M1',2 => 'M2',3 => 'M3',4 => 'M4',5 => 'M5',6 => 'M6',7 => 'M7',8 => 'M8',9 => 'M9',10 => 'M10',11 => 'M11',12 => 'M12'};
    Map<Integer, String> mapOfMonthNumToQuarter = new Map<Integer, String>{4 => 'Q1',7 => 'Q2',10=>'Q3',1=>'Q4'};
    
    list<string> lstDynamicConstants = new list<string>{
        'Hospitality_Doc_Unit_Status', 'Hospitality_Permitted_Uses'
    };

    map<string, Constant_Values__mdt> mapContants = getConstants(lstDynamicConstants);

    list<string> lstRegStatus = (mapContants.get('Hospitality_Doc_Unit_Status') != null && mapContants.get('Hospitality_Doc_Unit_Status').values__c != null) ? mapContants.get('Hospitality_Doc_Unit_Status').values__c.split(',') : new list<string>() ;
    
    list<string> lstPermittedUses = (mapContants.get('Hospitality_Permitted_Uses') != null && mapContants.get('Hospitality_Permitted_Uses').values__c != null) ? mapContants.get('Hospitality_Permitted_Uses').values__c.split(',') : new list<string>() ;
    
    public string sQry = '';
    
    global Database.QueryLocator start(Database.BatchableContext BC) {        
        if(sQry == ''){
            sQry  = 'SELECT Id, Unit_Name__c,Booking__r.Account__c,Booking__r.Account__r.Email__pc,Booking__r.Account__r.Name,Registration_Status__c,Permitted_Use_New__c';
            sQry += ' FROM Booking_Unit__c';
            sQry += ' WHERE  Registration_Status__c IN : lstRegStatus';
            sQry += ' AND Permitted_Use_New__c IN : lstPermittedUses';
        }
        System.debug('sQry>>>>>>>>>>'+sQry);
        return Database.getQueryLocator(sQry);
                                       
    }

    global void execute(Database.BatchableContext BC, List<Booking_Unit__c> lstBookingUnit) {
        system.debug('lstBookingUnit>>>>>>'+lstBookingUnit);
        List<EmailMessage> emailMessageList = new List<EmailMessage>();
        String currentQuarter;
        String currentMonth;
        Integer month;
        if(Test.isRunningTest()) {
            month = 1;
        }
        else {month = Date.Today().Month();}
        
        Integer currentYearForQuarter;
        Integer currentYearForMonth;
        if(month == 1) {
            currentMonth = mapOfMonthNumToMonth.get(month);
            currentQuarter = mapOfMonthNumToQuarter.get(month);
            currentYearForMonth = Date.Today().Year();
            currentYearForQuarter = Date.Today().Year() -1;
        }
        if(month == 4 || month == 7 || month == 10) {
            currentMonth = mapOfMonthNumToMonth.get(month);
            currentQuarter = mapOfMonthNumToQuarter.get(month);
            currentYearForMonth = Date.Today().Year();
            currentYearForQuarter = Date.Today().Year();
        }
        else if(month == 2 || month == 3 || month == 5 || month == 6 || month ==  8|| month == 9 || month == 11 || month == 12) {
            currentMonth = mapOfMonthNumToMonth.get(month);
            currentYearForMonth = Date.Today().Year();
        }
        for(Booking_Unit__c objBookingUnit: lstBookingUnit){
            System.debug('currentQuarter>>>'+currentQuarter);
            String unitName = (objBookingUnit.Unit_Name__c).replace('/','_');
          if(currentQuarter != Null) {
            String fileName = unitName+'_'+currentQuarter+'_'+String.valueOf(currentYearForQuarter)+'.pdf'; 
            List<Attachment> attchamentList =  GetFileIdOffice365AndSendEmailToCustomer.getFileAndReturnAttachment(fileName,objBookingUnit.Id);
             if(attchamentList != Null && attchamentList.size() > 0) {
              emailMessageList.addAll(GetFileIdOffice365AndSendEmailToCustomer.sendEmailToCustomer(objBookingUnit,attchamentList,'Hospitality_Template','',currentQuarter,String.valueOf(currentYearForQuarter)));
           }
          } 
          if(currentMonth != Null) {
            String fileName = unitName+'_'+currentMonth+'_'+String.valueOf(currentYearForMonth)+'.pdf'; 
            List<Attachment> attchamentList =  GetFileIdOffice365AndSendEmailToCustomer.getFileAndReturnAttachment(fileName,objBookingUnit.Id);
            if(attchamentList != Null && attchamentList.size() > 0) {
             emailMessageList.addAll(GetFileIdOffice365AndSendEmailToCustomer.sendEmailToCustomer(objBookingUnit,attchamentList,'Hospitality_Template',currentMonth,'',String.valueOf(currentYearForMonth)));
           }
          }
        }
        if(emailMessageList != Null && emailMessageList.size() > 0) {
                  insert emailMessageList;                  
        }
    
    }
    
    global void finish(Database.BatchableContext BC) {
    // finish code
    }

    public map<string, Constant_Values__mdt> getConstants(list<string> lstNames){
        map<string, Constant_Values__mdt> mapCVs = new map<string, Constant_Values__mdt>();
        for(Constant_Values__mdt objCS : [select DeveloperName,values__c from Constant_Values__mdt where DeveloperName IN : lstNames]){
            mapCVs.put(objCS.DeveloperName, objCS);
        }
        return mapCVs;
    }

}