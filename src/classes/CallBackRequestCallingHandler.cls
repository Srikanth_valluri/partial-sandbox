public without sharing class CallBackRequestCallingHandler {

    public static void onBeforeInsert( List<Calling_List__c> newCallingLst ) {
        updateCallBackRequestOwnerOnCreation(newCallingLst);
        DroppedCallsCallBackCallingHandler.onBeforeInsert(newCallingLst);
    }

    //Method to assign owner of  Collection inbound call on the basis of related call back request Calling list - Added on 06/06/18
    public static void updateCallBackRequestOwnerOnCreation( List<Calling_List__c> lstCallingList ) {
        Map<String,List<Call_Log__c>> mapCalledNumnerToObjCallingLog  = new Map<String,List<Call_Log__c>>();
        Set<String> setCalled_Number = new Set<String>();
        list<Error_Log__c> listErrorLog = new list<Error_Log__c>();
        Set<String> setCallingNumber = new Set<String>();
        
        List<Called_Number_List__c> objCalled_NumberList = Called_Number_List__c.getall().values();
        
        if( objCalled_NumberList != null && objCalled_NumberList.size() > 0 ) {
            for(Called_Number_List__c objCalled_Number : objCalled_NumberList){
                setCalled_Number.add(objCalled_Number.Name);
            }
            system.debug('setCalled_Number : ' + setCalled_Number);
            if (lstCallingList != null && lstCallingList.size() > 0) {
                Id devRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Call Back Request').getRecordTypeId();
                system.debug('devRecordTypeId : ' + devRecordTypeId);
                if( setCalled_Number != null && setCalled_Number.size() > 0 ) {
                    for( Calling_List__c objCalling_List : lstCallingList) {
                        if( objCalling_List.RecordTypeId == devRecordTypeId 
                         && objCalling_List.Called_Number__c != null && objCalling_List.Calling_Number__c != null) {
                            if( setCalled_Number.contains(objCalling_List.Called_Number__c) ) {
                                setCallingNumber.add( objCalling_List.Calling_Number__c );
                            }
                        }
                    }
                    system.debug('setCallingNumber : ' + setCallingNumber);
                    if( setCallingNumber != null && setCallingNumber.size() > 0 ) {
                        for( Call_Log__c objCall_Log : [SELECT Id ,OwnerId,CRE__c,Called_Number__c,Calling_Number__c
                                                                  FROM Call_Log__c 
                                                                 WHERE Calling_Number__c IN: setCallingNumber
                                                                   AND CreatedDate = LAST_N_DAYS:2 
                                                                   //AND CreatedDate = TODAY
                                                                   AND ( CRE_Profile_Id__c = 'Collection - Director' 
                                                                        OR CRE_Profile_Id__c = 'Collection - CRE'
                                                                        OR CRE_Profile_Id__c = 'Collection - Manager' )]) {
                            if( string.valueOf(objCall_Log.CRE__c).startsWith('005') ) {
                                if( mapCalledNumnerToObjCallingLog.containsKey(objCall_Log.Calling_Number__c) && mapCalledNumnerToObjCallingLog.get( objCall_Log.Calling_Number__c ) != null ) {
                                    List<Call_Log__c> lstCL = mapCalledNumnerToObjCallingLog.get( objCall_Log.Calling_Number__c );
                                    lstCL.add(objCall_Log);
                                    mapCalledNumnerToObjCallingLog.put( objCall_Log.Calling_Number__c,lstCL );
                                } else {
                                    mapCalledNumnerToObjCallingLog.put( objCall_Log.Calling_Number__c,new List<Call_Log__c>{objCall_Log});
                                }
                            }
                        }
                        system.debug('mapCalledNumnerToObjCallingLog : ' + mapCalledNumnerToObjCallingLog);
                        Id collectionQueueId = EmailMessageTriggerHandler.getGroupIdFromDeveloperName( 'Collection_Queue','Queue' );
                        system.debug('collectionQueueId : ' + collectionQueueId);
                        for( Calling_List__c objCalling_List : lstCallingList) {
                            if( objCalling_List.RecordTypeId == devRecordTypeId 
                             && objCalling_List.Called_Number__c != null && objCalling_List.Calling_Number__c != null) {
                                if( setCalled_Number.contains(objCalling_List.Called_Number__c) ) {
                                    if( mapCalledNumnerToObjCallingLog.containsKey(objCalling_List.Calling_Number__c) ) {
                                        objCalling_List.OwnerId =  mapCalledNumnerToObjCallingLog.get(objCalling_List.Calling_Number__c)[0].CRE__c;
                                    }else {
                                        objCalling_List.OwnerId =  collectionQueueId;
                                    }
                                }
                            }
                            system.debug('objCalling_List.OwnerId : ' + objCalling_List.OwnerId);
                        }
                        
                    } else {
                        Error_Log__c objErr = new Error_Log__c();
                        objErr.Calling_List__c = lstCallingList[0].Id;
                        objErr.Error_Details__c = 'New Calling list dont have the Calling Number'; 
                        listErrorLog.add(objErr);
                        insert listErrorLog;
                    }
                }
            }
        } else {
            Error_Log__c objErr = new Error_Log__c();
            objErr.Calling_List__c = lstCallingList[0].Id;
            objErr.Error_Details__c = 'Please add value in Called Number List Custom setting'; 
            listErrorLog.add(objErr);
            insert listErrorLog;
        }
    }
}