/***********************************************************************************************************************
* Name               : DealHandlerScheduler_Test                                                                       *
* Description        : Test class for DealHandlerScheduler, DealHandlerBatch.                                          *
* Created Date       : 05/02/2017                                                                                      *
* Created By         : NSI                                                                                             *
* ---------------------------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR            DATE            COMMENTS                                                               *
***********************************************************************************************************************/
@isTest 
public class DealHandlerScheduler_Test {

    public static testmethod void UnitTest(){
        DealHandlerScheduler handlerSch=new DealHandlerScheduler();
        String sch = '0 0 0 * * ?';
        system.schedule('Test check', sch, handlerSch);
        system.schedule('Test check2', sch, handlerSch);
    }
    
    
    public static testmethod void UnitTest2(){
      //pratiksha
      List<NSIBPM__SR_Template__c> SRTemplateList =  InitialiseTestData.createTestTemplateRecords(new List<NSIBPM__SR_Template__c>{new NSIBPM__SR_Template__c()});
      
      Id RecordTypeIdAGENT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
      Account a = new Account();
      a.recordtypeid=RecordTypeIdAGENT;
      a.Name = 'Test Account';
      a.Agency_Short_Name__c = 'testShrName';
      insert a;  
      
      Id RecordTypeIdContact = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        
        List<NSIBPM__Service_Request__c> requestList = 
            TestDataFactory.createTestServiceRequestRecords(
                new List<NSIBPM__Service_Request__c>{
                        new NSIBPM__Service_Request__c(recordTypeId = DamacUtility.getRecordTypeId('NSIBPM__Service_Request__c', 'Deal'))});
        requestList[0].Token_Deposit_Paid_Date_Time__c = null;  
        requestList[0].recordtypeid=RecordTypeIdContact;
        requestList[0].NSIBPM__SR_Template__c = SRTemplateList[0].Id;
        requestList[0].Agency__c = a.id;
        requestList[0].ID_Type__c = 'Passport'; 
        requestList[0].Token_Deposit_Due_Date_Time__c = system.now() - 1;    
        requestList[0].Internal_Status__c ='Submitted';                          
        update requestList;
        
        List<NSIBPM__SR_Status__c> srstatusList = 
            TestDataFactory.createSrStatusRecords(
                new List<NSIBPM__SR_Status__c>{
                    new NSIBPM__SR_Status__c(Name = 'REJECTED_DUE_TO_TIME_OUT', NSIBPM__Code__c = 'REJECTED_DUE_TO_TIME_OUT')});
        
        List<NSIBPM__Status__c> stepstatusList = 
            TestDataFactory.createStatusRecords(
                new List<NSIBPM__Status__c>{
                    new NSIBPM__Status__c(Name = 'REJECTED_DUE_TO_TIME_OUT', NSIBPM__Code__c = 'REJECTED_DUE_TO_TIME_OUT')});
         
        List<NSIBPM__Step__c> stepList = 
            TestDataFactory.createTestStepRecords(
                new List<NSIBPM__Step__c>{
                    new NSIBPM__Step__c(NSIBPM__SR__c = requestList[0].Id)});
                                    
        Inventory__c inv=new Inventory__c();
        inv.Start_Date__c=system.today()-10;
        inv.End_Date__c=system.today()+3;
        insert inv;
        
        List<Booking__c> bookingList= 
            testDataFactory.createBookingRecords(
                new List<Booking__c>{
                    new Booking__c(Deal_SR__c=requestList[0].id,Unique_Key__c='P123423 - '+requestList[0].id)});
        bookingList[0].Unique_Key__c='P123423 - '+requestList[0].id;
        update bookingList;
        
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Approve.');
        req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        req.setObjectId(requestList[0].Id);

        //Submit the approval request
        Approval.ProcessResult result = Approval.process(req);
        
        List<Booking_Unit__c> BookingUnitList= TestDataFactory.createBookingUnitRecords(new List<Booking_Unit__c>{new Booking_Unit__c(Booking__c=bookingList[0].id,Inventory__c=inv.id)});
        DealHandlerBatch dhbObject = new DealHandlerBatch();
        Database.executeBatch(dhbObject);
    }
}// End of test class.