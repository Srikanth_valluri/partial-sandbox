/******************************************************************************************************************************************
Description: Generic Utility class
===========================================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By  | Comments
--------------------------------------------------------------------------------------------------------------------------------------------
1.0     | 04-07-2019        | Arjun Khatri      | 1. Initial Draft

1.1     | 15-07-2019        | Arjun Khatri      | 1. Added method 'convertStringToDate'

1.2     | 18-11-2019        | Aishwarya Todkar  | 1. Added method 'getMonthNumber'

1.3     | 12-07-2020        | Shruti Nikam      | 1. Added wrapper and createTrackingHistory method 

1.4     | 20-10-2020        | Aishwarya Todkar  | 1. Added methods 'prepareSendGridEmail', 'sendEmailsBySendGrid' 
                                                    and wrapper 'SendGgridWrapper'
******************************************************************************************************************************************/
public class GenericUtility {
    
    public static String encodeChar(String msgCont ) {
        Map<String,String> mapEncodedChar = 
                        new Map<String, String>{'&'=>'%26'
                                                , '+'=>'%2B'
                                                , '%>'=>'%25'
                                                , '#'=>'%23'
                                                , '='=>'%3D'
                                                , '^'=>'%5E'
                                                , '~'=>'%7E'};
        System.debug('mapEncodedChar=='+mapEncodedChar);
        for(String char1 : mapEncodedChar.keySet()) {
            if(String.isNotBlank(msgCont) && msgCont.contains(char1)){
                System.debug('contains :' + char1);
                msgCont = msgCont.replace(char1,mapEncodedChar.get(char1));
            }
        }
        
        return msgCont;
    }
    
/******************************************************************************************************************************************
 * Method Name : getCamelCase
 * Description : Change case of string into Camel case.
 * Return Type : String
 * Parameter(s): Customer Name
*******************************************************************************************************************************************/
    public static String getCamelCase(String strName) {
        
        strName = strName.toLowerCase();    
        List<String> names = strName.split(' ');
        for (Integer i = 0; i < names.size(); i++)
            names[i] = names[i].capitalize();
        strName = String.join(names, ' ');

        System.debug('getCamelCase strName == ' + strName);
        return strName;
    }

/******************************************************************************************************************************************
 * Method Name : convertStringToDate
 * Description : Returns Date
 * Return Type : Date
 * Parameter(s): Day, Month, Year
*******************************************************************************************************************************************/    
    public static Date convertStringToDate(Integer intDay, Integer intMonth, Integer intYear) {
        return date.newInstance(intYear, intMonth, intDay);
    }

/******************************************************************************************************************************************
 * Method Name : doFormatting
 * Description : Adds commas to string amount
 * Return Type : String
 * Parameter(s): Value, seperators, No of decimal places
*******************************************************************************************************************************************/    
    public static String doFormatting(Decimal val, integer dec, String tSep, String dSep) {
        String s, tmp;
        Integer i = 4 + dec;
     
        // If the number of decimals is zero (0)... prevents the first 1000s seperator from being set at the 4th.
        if(dec==0){
            i--;
        }
     
        s = val.setScale(dec).toPlainString().replace(tSep, dSep);
        while(s.length() > i) {
            tmp = s.substring(0, s.length() - i) + tSep + s.substring(s.length() - i);
            s = tmp;
            i += 4;
        }
     
        // If the number is negative and has a number non-decimal digits divisible by 3, 
        //it prevents putting a comma before the 1st digit (ex -300,000.00  comes out -,300,000.00)
        if (s.substring(0,1) == '-') {
            if (s.substring(1,2) == tSep) {
                s = '-' + s.substring(2);
            }
        }
     
        return s;
    }
/******************************************************************************************************************************************
 * Method Name : removeLeadingZeros
 * Description : Removes leading zeros from phone number
 * Return Type : List of String
 * Parameter(s): List of String
*******************************************************************************************************************************************/    
    public static List<String> removeLeadingZeros(List<String> phoneNumbers) {
        List<String> newphoneNumbers = new List<String>();
        if(phoneNumbers != null) {
            for(String phoneNumber : phoneNumbers) {
                if(phoneNumber .startsWith('00')) {
                    phoneNumber = PhoneNumber.removeStart('00');
                }
                else if(PhoneNumber.startsWith('0')) {
                    phoneNumber = PhoneNumber.removeStart('0');
                }
                
                newphoneNumbers.add(phoneNumber);
            }
        }
        
        return newphoneNumbers;
    }

/******************************************************************************************************************************************
 * Method Name : createErrorLog
 * Description : Creates Error log
 * Return Type : Error_Log__c 
 * Parameter(s): Error Message, Account Id, Booking unit Id, Calling List Id, Case Id
*******************************************************************************************************************************************/    
    public static Error_Log__c createErrorLog(String responseMsg,String accountId, String buId, String ClId, String CaseId) {
        Error_Log__c  objError = new Error_Log__c ();
        objError.Error_Details__c  = responseMsg;
        if(String.isNotBlank(accountId)){
            objError.Account__c = Id.valueof(accountId);
        }
        if(String.isNotBlank(buId)){
            objError.Booking_unit__c = Id.valueof(buId);
        }
        if(String.isNotBlank(ClId)){
            objError.Calling_List__c = Id.valueof(ClId);
        }
        if(String.isNotBlank(CaseId)){
            objError.Case__c = Id.valueof(CaseId);
        }
        return objError;
    }

/******************************************************************************************************************************************
 * Method Name : getMonthNumber
 * Description : To get month number from month name
 * Return Type : Integer 
 * Parameter(s): String
*******************************************************************************************************************************************/
    public static Integer getMonthNumber( String monthName) {
        Map<String,Integer> mapOfMonth = new Map<String,Integer> {'jan' => 1,
                                                                    'feb' => 2,
                                                                    'mar' => 3, 
                                                                    'apr' => 4, 
                                                                    'may' => 5, 
                                                                    'jun' => 6,
                                                                    'jul' => 7,
                                                                    'aug' => 8,
                                                                    'sep' => 9,
                                                                    'oct' => 10,
                                                                    'nov' => 11,
                                                                    'dec' => 12 };
        Integer monthNumber = ( String.isNotBlank( monthName )
        && mapOfMonth.containsKey( monthName.toLowerCase() ) )
        ? mapOfMonth.get( monthName.toLowerCase() )
        : null;

        return monthNumber;
    }
/******************************************************************************************************************************************
 * Method Name : createSOACreator
 * Description : To Soa creater record
 * Return Type : void
 * Parameter(s): String URL,Id userID,Datetime timeOfCreation,String regId
*******************************************************************************************************************************************/   
    
    Public static void createSOACreator(String URL,Id userID,Datetime timeOfCreation,String regId,String Source, Id accountId, Id bookingUnitId, Id  callingListId){
        if( String.isNotBlank( URL )
            && userID != NULL
            && timeOfCreation != NULL){
            
                User_Calls__c obj = new User_Calls__c ();
                obj.SOAUrl__c = URL;
                obj.CRE__c = userID;
                obj.SOAtimeOfCreation__c = timeOfCreation;
                obj.regId__c = regId;
                obj.Source_of_SOA_Generation__c = Source;
                obj.Account__c = accountId;
                obj.Booking_Unit__c = bookingUnitId;
                obj.Calling_List__c = callingListId;
                insert obj;
            }
    
    }

/******************************************************************************************************************************************
 * Method Name : getMonthName
 * Description : To get month name from month number
 * Return Type : String 
 * Parameter(s): Integer
*******************************************************************************************************************************************/
    public static String getMonthName( Integer monthNumber) {
        Map<Integer, String> mapOfMonth = new Map<Integer, String> {1 => 'Jan',
                                                                        2 => 'Feb',
                                                                        3 => 'Mar', 
                                                                        4 => 'Apr', 
                                                                        5 => 'May', 
                                                                        6 => 'Jun',
                                                                        7 => 'Jul',
                                                                        8 => 'Aug',
                                                                        9 => 'Sep',
                                                                        10 =>'Oct',
                                                                        11 => 'Nov',
                                                                        12 => 'Dec'};
        String monthName = ( monthNumber != null
        && mapOfMonth.containsKey( monthNumber ) )
        ? mapOfMonth.get( monthNumber )
        : null;

        return monthName;
    }

/******************************************************************************************************************************************
Description : Method to prepare SendGridWrapper with some common parameters
Parameter(s): SendGrid_Email_Details__mdt
Return      : SendGridWrapper
******************************************************************************************************************************************/
    public static SendGridWrapper prepareSendGridEmail(  SendGrid_Email_Details__mdt objSendGridCs ) {

        SendGridWrapper sgWrap = new SendGridWrapper();

        sgWrap.toAddress = String.isNotBlank( objSendGridCs.To_Address__c ) ? objSendGridCs.To_Address__c : '';
                    
        sgWrap.toName = String.isNotBlank( objSendGridCs.To_Name__c ) ? objSendGridCs.To_Name__c : '';
        
        sgWrap.ccAddress = String.isNotBlank( objSendGridCs.CC_Address__c ) ? objSendGridCs.CC_Address__c : '';
        
        sgWrap.ccName = String.isNotBlank( objSendGridCs.CC_Name__c ) ? objSendGridCs.CC_Name__c : '';
        
        sgWrap.bccAddress = String.isNotBlank( objSendGridCs.Bcc_Address__c ) ? objSendGridCs.Bcc_Address__c : '';
        
        sgWrap.bccName = String.isNotBlank( objSendGridCs.Bcc_Name__c ) ? objSendGridCs.Bcc_Name__c : '';
        
        sgWrap.fromAddress = String.isNotBlank( objSendGridCs.From_Address__c ) ? objSendGridCs.From_Address__c : '';
        
        sgWrap.fromName = String.isNotBlank( objSendGridCs.From_Name__c ) ? objSendGridCs.From_Name__c : '';
        
        sgWrap.replyToAddress = String.isNotBlank( objSendGridCs.Reply_To_Address__c ) ? objSendGridCs.Reply_To_Address__c : '';
        
        sgWrap.replyToName = String.isNotBlank( objSendGridCs.Reply_To_Name__c ) ? objSendGridCs.Reply_To_Name__c : '';
        
        sgWrap.ContentType = 'text/html';

        return sgWrap;
    }

/******************************************************************************************************************************************
Description : Method to send email via SendGrid
Parameter(s): SendGridwrapper
Return      : List<EmailMessage>
******************************************************************************************************************************************/
    public static List<EmailMessage> sendEmailsBySendGrid( SendGridWrapper objWrap ) {
    
        SendGridEmailService.SendGridResponse objSendGridResponse = SendGridEmailService.sendEmailService( 
                                                                objWrap.toAddress, objWrap.toName
                                                                , objWrap.ccAddress, objWrap.ccName
                                                                , objWrap.bccAddress, objWrap.bccName
                                                                , objWrap.subject, objWrap.substitutions
                                                                , objWrap.fromAddress, objWrap.fromName
                                                                , objWrap.replyToAddress, objWrap.replyToName
                                                                , objWrap.contentType,objWrap.contentValue
                                                                , objWrap.templateId, objWrap.listAttachment
                                                                );
    
        system.debug('objSendGridResponse === ' + objSendGridResponse);
            
        String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;
        List<EmailMessage> lstEmails = new List<EmailMessage>(); 
            
        //Create reportable activity 
        if( responseStatus == 'Accepted' ) {
            EmailMessage mail = new EmailMessage();
            mail.Subject = objWrap.subject;
            mail.MessageDate = System.Today();
            mail.Status = '3';//'Sent';
            if( objWrap.relatedToId != null ){
                mail.RelatedToId = objWrap.relatedToId;
            }
            if( objWrap.AccountId != null ){
                mail.Account__c  = objWrap.AccountId;
            }
            if( objWrap.buId != null ) {
                mail.Booking_Unit__c = objWrap.buId;
            }
            if( objWrap.caseId != null ) {
                mail.ParentId = objWrap.caseId;
            }
            if( objWrap.clId != null ) {
                mail.Calling_List__c = objWrap.clId;
            }
            if( String.isNotBlank( objWrap.processType ) ) {
                mail.Type__c = objWrap.processType;
            }
            mail.ToAddress = objWrap.toAddress;
            mail.FromAddress = objWrap.fromAddress;
            mail.TextBody = objWrap.contentBody;//contentValue.replaceAll('\\<.*?\\>', '');
            mail.HtmlBody = objWrap.contentValue;
            mail.Sent_By_Sendgrid__c = true;
            mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
            mail.CcAddress = objWrap.ccAddress;
            mail.BccAddress = objWrap.bccAddress;
            lstEmails.add(mail);
        }
        return lstEmails;
    }

/******************************************************************************************************************************************
 * Method Name : createTrackingHistory
 * Description : To create field history for case approvals
 * Return Type : void  
 * Parameter(s): HistoryWrapper
*******************************************************************************************************************************************/
    public void createTrackingHistory(List<HistoryWrapper> historyWrapperList) {
        List<Field_History__c> FieldHistoryList = new List<Field_History__c>();
        for(HistoryWrapper historyWrapperObj : historyWrapperList) {
            Field_History__c FieldHistoryObj = new Field_History__c(); 
            FieldHistoryObj.AccountId__c = historyWrapperObj.accountId;
            FieldHistoryObj.Appprover_Role__c = historyWrapperObj.appproverRole;
            FieldHistoryObj.Approver_Approval_Date__c = historyWrapperObj.approverApprovalDate;
            FieldHistoryObj.Approver_Assignment_Date__c = historyWrapperObj.approverAssignmentDate;
            FieldHistoryObj.Approver_Comments__c = historyWrapperObj.approverComments;
            FieldHistoryObj.Approver_Id__c = historyWrapperObj.approverId;
            FieldHistoryObj.Approver_Name__c = historyWrapperObj.approverName;
            FieldHistoryObj.Approver_Status__c = historyWrapperObj.approverStatus;
            FieldHistoryObj.CaseSummaryRequest__c = historyWrapperObj.caseSummaryRequest;
            FieldHistoryObj.Case__c = historyWrapperObj.caseId;
            FieldHistoryObj.Date_Time__c = historyWrapperObj.dateTimeOfChange;
            FieldHistoryObj.User__c = historyWrapperObj.user;
            FieldHistoryList.add(FieldHistoryObj);
        }
        insert FieldHistoryList;
    }
/******************************************************************************************************************************************
 * Method Name : getApprovingAuthorities
 * Description : Fetch the approving authorities
 * Return Type : String
 * Parameter(s): 
*******************************************************************************************************************************************/
    public String getApprovingAuthorities(Decimal parentCaseAmt, Decimal caseAmt, String parentAuth, String caseAuth){
        String authorities='';
        System.debug('parentCaseAmt::' +parentCaseAmt);
        System.debug('caseAmt::'+caseAmt);
        System.debug('parentAuth::' +parentAuth);
        System.debug('caseAuth:::' +caseAuth);
        if(parentCaseAmt != null && caseAmt != null && !String.isBlank(parentAuth) && !String.isBlank(caseAuth)){
            parentAuth = parentAuth.toUpperCase();
            caseAuth = caseAuth.toUpperCase();
            if(parentCaseAmt == caseAmt && parentAuth.equals(caseAuth)){
                authorities = 'AUTO APPROVED';
            }else if(parentCaseAmt == caseAmt && !parentAuth.equals(caseAuth)){
                if(parentAuth.length() < caseAuth.length() && caseAuth.contains(parentAuth)){
                    authorities = caseAuth.substringAfter(parentAuth);
                    authorities = authorities.removeStart(',');
                }else if(parentAuth.length() > caseAuth.length() && parentAuth.contains(caseAuth)){
                    authorities = parentAuth.substringAfter(caseAuth);
                    authorities = authorities.removeStart(',');
                }
            }else if(parentCaseAmt < caseAmt && parentAuth.equals(caseAuth)){
                authorities = 'AUTO APPROVED';
            }else if(parentCaseAmt < caseAmt && !parentAuth.equals(caseAuth)){
                if(parentAuth.length() < caseAuth.length() && caseAuth.contains(parentAuth)){
                    authorities = caseAuth.substringAfter(parentAuth);
                    authorities = authorities.removeStart(',');
                }else if(parentAuth.length() > caseAuth.length() && parentAuth.contains(caseAuth)){
                    authorities = parentAuth.substringAfter(caseAuth);
                    authorities = authorities.removeStart(',');
                }
            }
        }
        return authorities;
    }

/******************************************************************************************************************************************
 * Class Name : HistoryWrapper
 * Description : Wrapper class create field history data set for case approvals
*******************************************************************************************************************************************/
    public class HistoryWrapper {
        public string objectName;
        public string ParentId;
        public Id accountId;
        public String appproverRole;
        public DateTime approverApprovalDate;
        public DateTime approverAssignmentDate; 
        public String approverComments;
        public String approverId; 
        public String approverName;
        public String approverStatus;
        public Id caseSummaryRequest; 
        public Id caseId;
        public Id bookingUnitId;
        public DateTime dateTimeOfChange; 
        public Id user;
        
        public HistoryWrapper() {
            ParentId = '';
            objectName = '';
            accountId = null;
            appproverRole = '';
            approverApprovalDate = null;
            approverAssignmentDate = null;
            approverComments = '';
            approverId = '';
            approverName = '';
            approverStatus = '';
            caseSummaryRequest = null;
            caseId = null;
            bookingUnitId = null;
            dateTimeOfChange = system.Now();
            user = Userinfo.getuserid();
        }
    }

/******************************************************************************************************************************************
 * Class Name   : SendGridWrapper
 * Description  : Wrapper class of sendGrid Email details
******************************************************************************************************************************************/

    public class SendGridWrapper {
        public String toAddress; 
        public String toName;
        public String ccAddress; 
        public String ccName;
        public String bccAddress;
        public String bccName;
        public String subject;
        public String substitutions;
        public String fromAddress;
        public String fromName;
        public String replyToAddress;
        public String replyToName;
        public String contentType;
        public String contentValue;
        public String templateId;
        public List<Attachment> listAttachment;
        public String contentBody;
        public Id relatedToId;
        public Id AccountId;
        public Id buId;
        public Id clId;
        public Id caseId;
        public String processType;

        public SendGridWrapper() {
            toAddress = toName = ccAddress = ccName = bccAddress = bccName = subject = substitutions = fromAddress = '';
            fromName = replyToAddress = replyToName = contentType = contentValue = templateId = contentBody = '';
            listAttachment = new List<Attachment>();
        }
    }
}