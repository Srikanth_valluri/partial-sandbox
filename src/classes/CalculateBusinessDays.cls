/**
 * Ver       Date            Author                 Modification
 * 1.0    9/23/2019         Arsh Dave              Initial Version
**/
public with sharing class CalculateBusinessDays {

    /* Method Checks is the date WeekEnd or Not. */
    public static Boolean IsWeekendDay(Date startDate) {

        Date startOfWeek   = startDate.toStartOfWeek();
        Integer dayOfWeek  = startOfWeek.daysBetween(startDate);// startDate.day() - startOfWeek.day();

        Boolean result = false;
        /* Excludes Friday and Saturday. Can replace this code with a custom setting where each year can update the dates of fixed holidays.*/
        result = dayOfWeek == 5 || dayOfWeek == 4 ? true : false; 
        return result;
    }

    /* Method Checks the number of days to be added or subtracted from a particular date and peforms the action */
    public static Date addBusinessDays(Date StartDate, Integer intBusinessDaysToAdd ) {
        Date finalDate = StartDate;
        Integer intdirection = intBusinessDaysToAdd < 0 ? -1 : 1;
        while(intBusinessDaysToAdd != 0) {
            finalDate = finalDate.AddDays(intdirection);
            if (!IsWeekendDay(finalDate)) {
                intBusinessDaysToAdd -= intdirection;
            }
        }
        return finalDate;
    }
}