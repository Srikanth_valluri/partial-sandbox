@RestResource(urlMapping='/API_Search_User/*')
global class API_Search_User{

    @HttpGET
    global static void searchRMorAgency() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        

         // To search for the RM user based on the email address.
        if(req.requestURI == '/API_Search_User/searchRM') {
            String emailId = req.params.get('email');
            String error = '';
            try {
                User userRMObj = [SELECT Id FROM user WHERE Email =:emailId 
                                            AND IsActive = TRUE 
                                            AND ( Profile.Name LIKE '%Property Consultant%' OR Profile.Name LIKE '%Agent Executive Team%' )
                                            LIMIT 1];
                RestContext.response.addHeader('Content-Type', 'application/json');
                RestContext.response.responseBody = Blob.valueOf('{"userId" : "'+userRMObj.id+'"}');
            } catch (Exception e) {
                error = Label.Validate_RM_Email;
                RestContext.response.addHeader('Content-Type', 'application/json');
                RestContext.response.responseBody = Blob.valueOf('{"userId" : "", "Error" : "'+error+'"}');
            }
            
        }
        
        // To search for the agent based on the email address provided.
        if(req.requestURI == '/API_Search_User/searchAgent') {
            String emailId = req.params.get('email');
           
            Contact contact = new Contact ();
            for (Contact con :[SELECT Email, AccountId FROM Contact WHERE Email =: emailId]) {
                contact = con;
            }
            Boolean isActive = false;
            for (User u :[ SELECT Name, IsActive FROM User WHERE contactId =: contact.Id AND IsActive = TRUE LIMIT 1]) {
                if (!u.isActive) {
                    isActive = false;
                } else 
                    isActive = true;
            }
            if (isActive) {
                RestContext.response.addHeader('Content-Type', 'application/json');
                RestContext.response.responseBody = Blob.valueOf('{"agentId" : "'+contact.id+'", "agencyId" : "'+contact.accountId+'"}'); 
                
            }
            else {
                
                RestContext.response.addHeader('Content-Type', 'application/json');
                RestContext.response.responseBody = Blob.valueOf('{"agentId" : "", "agencyId" : "", "Error" : "There is no agency or agent found with this Email."}'); 
            }
        }
    }
    
    
    

}