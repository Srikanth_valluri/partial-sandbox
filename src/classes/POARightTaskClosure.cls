/*
 * Description - Class used for creating POA units records when task to verify POA rights is closed
 *
 * Version        Date            Author            Description
 * 1.0            25/02/18        Vivek Shinde      Initial Draft
 * 1.1            01/03/18        Vivek Shinde      Changes for creating portal user
 */
public without sharing class POARightTaskClosure {
    
    // Method called from process builder to create POA Units records
    @InvocableMethod
    public static void createPOAUnits(List<Task> lstTask) {
        Set<Id> setCaseId = new Set<Id>();
        Set<Id> setTaskId = new Set<Id>();
        Id idPOAAccountRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('POA').getRecordTypeId();
        
        for(Task objTask: lstTask) {
            if(String.valueOf(objTask.WhatId).startsWith('500') && String.isNotBlank(objTask.Subject) &&
                objTask.Assigned_User__c != null && (objTask.Status == 'Closed' || objTask.Status == 'Completed')) {
                if(objTask.Subject.equals('Verify POA Right')) {
                    setTaskId.add(objTask.Id);
                    setCaseId.add(objTask.WhatId);
                }
            }
        }
        system.debug('--setCaseId--'+setCaseId);
        Map<Id, Case> mapCase = getCaseMap(setCaseId);
        Map<Id, List<Task>> mapCaseToTask = new Map<Id, List<Task>>();
        //Set<Id> setPOAId = new Set<Id>();
        //Set<Id> setAccountId = new Set<Id>();
        Map<Id, Case> mapCaseToUpdate = new Map<Id, Case>();
        
        for(Task objTask: lstTask) {
            if(!mapCaseToTask.containsKey(objTask.WhatId)) 
                mapCaseToTask.put(objTask.WhatId, new List<Task>{objTask});
            else
                mapCaseToTask.get(objTask.WhatId).add(objTask);
                
            //setPOAId.add(mapCase.get(objTask.WhatId).POA__c);
            //setAccountId.add(mapCase.get(objTask.WhatId).AccountId);
            
            if(!mapCaseToUpdate.containsKey(objTask.WhatId)) {
                Case objCase = mapCase.get(objTask.WhatId);
                objCase.Status = 'Closed';
                mapCaseToUpdate.put(objCase.Id, objCase);
            }
        }
        
        List<String> lstEmail = new List<String>();
        Map<Id, Account> mapCaseToPOAAccount = new Map<Id, Account>();
        
        for(Case objCase: mapCase.values()) {
            if(String.isNotBlank(objCase.Email__c)) {
                lstEmail.add(objCase.Email__c);
            }
            mapCaseToPOAAccount.put(objCase.Id, new Account());
        }

        Set<Id> setExistingAccount = new Set<Id>();
        List<Account> lstAcc = [Select Id, Email__pc From Account Where Email__pc IN: lstEmail];
        for(Case objCase: mapCase.values()) {
            for(Account objAcc: lstAcc) {
                if(objCase.Email__c == objAcc.Email__pc) {
                    mapCaseToPOAAccount.put(objCase.Id, objAcc);
                    setExistingAccount.add(objAcc.Id);
                    break;
                }
            }
        }
        
        Map<Id, Account> mapCaseToNewPOAAccount = new Map<Id, Account>();
        for(Id idCase: mapCaseToPOAAccount.keySet()) {
            if(mapCaseToPOAAccount.get(idCase).Id == null) {
                Account objPOAAccount = new Account();
                objPOAAccount.RecordTypeId = idPOAAccountRT;
                objPOAAccount.FirstName = mapCase.get(idCase).Customer_First_Name__c;
                objPOAAccount.LastName = mapCase.get(idCase).Customer_Last_Name__c;
                objPOAAccount.Email__pc = mapCase.get(idCase).Email__c;
                objPOAAccount.Mobile_Phone_Encrypt__pc = mapCase.get(idCase).Mobile_Phone_2__c;
                objPOAAccount.Passport_Number__pc = mapCase.get(idCase).New_CR__c;
                objPOAAccount.Nationality__pc = mapCase.get(idCase).Nationality__c;
                mapCaseToNewPOAAccount.put(mapCase.get(idCase).Id, objPOAAccount);
            }
        }
        
        insert mapCaseToNewPOAAccount.values();
        Set<Id> setCommunityAccId = new Set<Id>();
        for(Id idCase: mapCaseToNewPOAAccount.keySet()) {
            setCommunityAccId.add(mapCaseToNewPOAAccount.get(idCase).Id);
            mapCaseToPOAAccount.put(idCase, mapCaseToNewPOAAccount.get(idCase));
        }
        
        Map<Id, POA_Related_Account__c> mapCaseToPRA = new Map<Id, POA_Related_Account__c>();
        for(Case objCase: mapCase.values()) {
            POA_Related_Account__c objPRA = new POA_Related_Account__c();
            objPRA.Related_To_Account__c = objCase.AccountId;
            objPRA.POA_Valid_Till__c = objCase.POA_Valid_Till__c;
            objPRA.POA_Issued_By__c = objCase.POA_Issued_By_Picklist__c;
            objPRA.POA_Verification_Status__c = 'POA Verified by Legal';
            objPRA.Right_Type__c = objCase.Right_Type__c;
            objPRA.POA_Account__c = mapCaseToPOAAccount.get(objCase.Id).Id;
            mapCaseToPRA.put(objCase.Id, objPRA);
        }
        
        insert mapCaseToPRA.values();
        
        for(Id idCase: mapCaseToPRA.keySet()) {
            mapCaseToUpdate.get(idCase).POA_Related_Account__c = mapCaseToPRA.get(idCase).Id;
        }
        
        List<SR_Attachments__c> lstAttachment = new List<SR_Attachments__c>();
        for(Id idCase: mapCaseToPRA.keySet()) {
            if(mapCase.get(idCase).SR_Attachments__r != null && !mapCase.get(idCase).SR_Attachments__r.isEmpty()) {
                SR_Attachments__c objAttach = new SR_Attachments__c(
                    Name = mapCase.get(idCase).SR_Attachments__r[0].Name,
                    Type__c = mapCase.get(idCase).SR_Attachments__r[0].Type__c,
                    Attachment_URL__c = mapCase.get(idCase).SR_Attachments__r[0].Attachment_URL__c,
                    POA_Related_Account__c = mapCaseToPRA.get(idCase).Id);
                lstAttachment.add(objAttach);
            }
        }
        insert lstAttachment;
        
        List<POA_Unit__c> lstPUToInsert = new List<POA_Unit__c>();
        for(Id idCase: mapCaseToTask.keySet()) {
            for(Task objTask: mapCaseToTask.get(idCase)) {
                lstPUToInsert.add(new POA_Unit__c(
                    Booking_Unit__c = objTask.Booking_Unit__c,
                    POA_Right__c = objTask.POA_Right__c,
                    POA_Related_Account__c = mapCaseToPRA.get(mapCase.get(idCase).Id).Id));
            }
        }
        insert lstPUToInsert;
        
        update mapCaseToUpdate.values();
        
        Profile objPF = [Select id,name from Profile where name =: Label.Customer_Community_Profile_Name limit 1]; //'Customer Community Login User(Use this)'
        
        List<User> lstCommunityUser = new List<User>();
        for(Account objAcc: [Select Id, Email__pc, FirstName, LastName, PersonContactId From Account Where Id IN: setCommunityAccId]) {
            User objUser = new User(
                ContactId = objAcc.PersonContactId, 
                username = objAcc.Email__pc, 
                firstname = objAcc.FirstName,
                lastname = objAcc.LastName, 
                email = objAcc.Email__pc,
                communityNickname = objAcc.Email__pc,
                alias = string.valueof(objAcc.FirstName.substring(0,1) + objAcc.LastName.substring(0,3)), 
                profileid = objPF.Id, 
                emailencodingkey = 'UTF-8',
                languagelocalekey = 'en_US', 
                localesidkey = 'en_US', 
                timezonesidkey = 'Asia/Dubai');
            //Database.DMLOptions dlo = new Database.DMLOptions();
            //dlo.EmailHeader.triggerUserEmail= true;
            lstCommunityUser.add(objUser);
        } 
        
        insert lstCommunityUser;
        
        /*Map<Id, POA_Related_Account__c> mapPOAToRelatedAccount = new Map<Id, POA_Related_Account__c>();
        for(POA_Related_Account__c objPRA: [Select Id, POA__c, Related_to_Account__c,
            (Select Id, Name, Attachment_URL__c, Type__c From Documents__r Where Type__c = 'POA')
            From POA_Related_Account__c Where POA__c IN: setPOAId And Related_to_Account__c IN: setAccountId]) {
            mapPOAToRelatedAccount.put(objPRA.POA__c, objPRA);
        }
        
        Map<Id, SR_Attachments__c> mapPOAToAttachment = new Map<Id, SR_Attachments__c>();
        for(Id idCase: mapCaseToTask.keySet()) {
            if(mapCase.get(idCase).SR_Attachments__r != null && !mapCase.get(idCase).SR_Attachments__r.isEmpty()) {
                mapPOAToAttachment.put(mapCase.get(idCase).POA__c, new SR_Attachments__c(
                    Name = mapCase.get(idCase).SR_Attachments__r[0].Name,
                    Type__c = mapCase.get(idCase).SR_Attachments__r[0].Type__c,
                    Attachment_URL__c = mapCase.get(idCase).SR_Attachments__r[0].Attachment_URL__c));
            }
            if(!mapPOAToRelatedAccount.containsKey(mapCase.get(idCase).POA__c)) {
                mapPOAToRelatedAccount.put(mapCase.get(idCase).POA__c, 
                    new POA_Related_Account__c(POA__c = mapCase.get(idCase).POA__c, Related_to_Account__c = mapCase.get(idCase).AccountId));
            }
            else {
                if(mapPOAToRelatedAccount.get(mapCase.get(idCase).POA__c).Documents__r != null && !mapPOAToRelatedAccount.get(mapCase.get(idCase).POA__c).Documents__r.isEmpty()) {
                    mapPOAToAttachment.get(mapCase.get(idCase).POA__c).Id = mapPOAToRelatedAccount.get(mapCase.get(idCase).POA__c).Documents__r[0].Id;
                }
            }
        }
        
        upsert mapPOAToRelatedAccount.values();
        
        for(Id idPOA: mapPOAToAttachment.keySet()) {
            mapPOAToAttachment.get(idPOA).POA_Related_Account__c = mapPOAToRelatedAccount.get(idPOA).Id;
        }
        
        upsert mapPOAToAttachment.values();
        
        Map<Id, POA__c> mapPOA = getPOAMap(setPOAId);
        
        Map<Id, Account> mapPOAToAccount = new Map<Id, Account>();
        List<Account> lstAccountForCommunity = new List<Account>();
        
        for(Id idPOA: setPOAId) {
            Account objAcc = new Account();
            objAcc.Id = mapPOA.get(idPOA).POA_Account__c;
            objAcc.RecordTypeId = idPOAAccountRT;
            objAcc.FirstName = mapPOA.get(idPOA).First_Name__c;
            objAcc.LastName = mapPOA.get(idPOA).Last_Name__c;
            objAcc.Email__pc = mapPOA.get(idPOA).Email_Address__c;
            objAcc.Mobile_Phone_Encrypt__pc = mapPOA.get(idPOA).Mobile_Number__c;
            objAcc.Passport_Number__pc = mapPOA.get(idPOA).Passport_Number__c;
            objAcc.Nationality__pc = mapPOA.get(idPOA).POA_Nationality__c;
            
            if(objAcc.Id == null)
                lstAccountForCommunity.add(objAcc);
            mapPOAToAccount.put(idPOA, objAcc);
        }
        
        insert lstAccountForCommunity;
        Set<Id> setCommunityAccId = new Set<Id>();
        for(Account objAcc: lstAccountForCommunity) {
            setCommunityAccId.add(objAcc.Id);
        }
        
        Profile objPF = [Select id,name from Profile where name =: Label.Customer_Community_Profile_Name limit 1]; //'Customer Community Login User(Use this)'
        
        List<User> lstCommunityUser = new List<User>();
        for(Account objAcc: [Select Id, Email__pc, FirstName, LastName, PersonContactId From Account Where Id IN: setCommunityAccId]) {
            User objUser = new User(
                ContactId = objAcc.PersonContactId, 
                username = objAcc.Email__pc, 
                firstname = objAcc.FirstName,
                lastname = objAcc.LastName, 
                email = objAcc.Email__pc,
                communityNickname = objAcc.Email__pc,
                alias = string.valueof(objAcc.FirstName.substring(0,1) + objAcc.LastName.substring(0,3)), 
                profileid = objPF.Id, 
                emailencodingkey = 'UTF-8',
                languagelocalekey = 'en_US', 
                localesidkey = 'en_US', 
                timezonesidkey = 'Asia/Dubai');
            //Database.DMLOptions dlo = new Database.DMLOptions();
            //dlo.EmailHeader.triggerUserEmail= true;
            lstCommunityUser.add(objUser);
        } 
        
        insert lstCommunityUser;
        //Database.saveresult sr = Database.insert(u2,dlo);
        
        update mapCaseToUpdate.values();
        upsert mapPOAToAccount.values();
        
        List<POA_Unit__c> lstPUToInsert = new List<POA_Unit__c>();
        List<POA__c> lstPOAToUpdate = new List<POA__c>();
        for(Id idCase: mapCaseToTask.keySet()) {
            POA__c objPOA = mapPOA.get(mapCase.get(idCase).POA__c);
            objPOA.POA_Account__c = mapPOAToAccount.get(objPOA.Id).Id;
            objPOA.POA_Verification_Status__c = 'POA Verified by Legal';
            lstPOAToUpdate.add(objPOA);
            for(Task objTask: mapCaseToTask.get(idCase)) {
                lstPUToInsert.add(new POA_Unit__c(
                    Booking_Unit__c = objTask.Booking_Unit__c,
                    POA__c = mapCase.get(objTask.WhatId).POA__c,
                    POA_Right__c = objTask.POA_Right__c,
                    POA_Related_Account__c = mapPOAToRelatedAccount.get(mapCase.get(objTask.WhatId).POA__c).Id));
            }
        }
        
        update lstPOAToUpdate;
        insert lstPUToInsert;
        */
        
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /*Map<Id, Case> mapCase = getCaseMap(setCaseId);
        List<POA_Unit__c> lstPU = new List<POA_Unit__c>();
        Set<Id> setPOAId = new Set<Id>();
        Map<Id, Case> mapCaseToUpdate = new Map<Id, Case>();
        
        for(Task objTask: getTaskDetails(setTaskId)) {
            POA_Unit__c objPU = new POA_Unit__c();
            objPU.Booking_Unit__c = objTask.Booking_Unit__c;
            objPU.POA__c = mapCase.get(objTask.WhatId).POA__c;
            objPU.POA_Right__c = objTask.POA_Right__c;
            //objPU.POA_Services__c = strSelectedRights;
            lstPU.add(objPU);
            
            setPOAId.add(mapCase.get(objTask.WhatId).POA__c);
            
            if(!mapCaseToUpdate.containsKey(objTask.WhatId)) {
                Case objCase = mapCase.get(objTask.WhatId);
                objCase.Status = 'Closed';
                mapCaseToUpdate.put(objCase.Id, objCase);
            }
        }
        
        Map<Id, POA__c> mapPOA = getPOAMap(setPOAId);
        Set<Id> setPOAToUpdate = new Set<Id>();
        Map<Id, POA__c> mapPOAToUpdate = new Map<Id, POA__c>();
        
        for(Task objTask: getTaskDetails(setTaskId)) {
            if(!mapPOAToUpdate.containsKey(mapCase.get(objTask.WhatId).POA__c)) {
                POA__c objPOA = mapPOA.get(mapCase.get(objTask.WhatId).POA__c);
                objPOA.Related_to_Account__c = mapCase.get(objTask.WhatId).AccountId;
                mapPOAToUpdate.put(objPOA.Id, objPOA);
            }
            //if(mapPOA.get(mapCase.get(objTask.WhatId).POA__c).POA_Account__c == null) {
                setPOAToUpdate.add(mapCase.get(objTask.WhatId).POA__c);
            //}
        }
        
        Map<Id, Account> mapPOAToAccount = new Map<Id, Account>();
        Id idPOAAccountRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('POA').getRecordTypeId();
        
        for(Id idPOA: setPOAToUpdate) {
            Account objAcc = new Account();
            objAcc.Id = mapPOA.get(idPOA).POA_Account__c;
            objAcc.RecordTypeId = idPOAAccountRT;
            objAcc.FirstName = mapPOA.get(idPOA).First_Name__c;
            objAcc.LastName = mapPOA.get(idPOA).Last_Name__c;
            objAcc.Email__pc = mapPOA.get(idPOA).Email_Address__c;
            objAcc.Mobile_Phone_Encrypt__pc = mapPOA.get(idPOA).Mobile_Number__c;
            objAcc.Passport_Number__pc = mapPOA.get(idPOA).Passport_Number__c;
            mapPOAToAccount.put(idPOA, objAcc);
        }
        
        update mapCaseToUpdate.values();
        upsert mapPOAToAccount.values();
        
        for(Id idPOA: mapPOAToUpdate.keySet()) {
            if(mapPOAToAccount.containsKey(idPOA)) 
                mapPOAToUpdate.get(idPOA).POA_Account__c = mapPOAToAccount.get(idPOA).Id;
                mapPOAToUpdate.get(idPOA).POA_Verification_Status__c = 'POA Verified by Legal';
        }
        update mapPOAToUpdate.values();
        insert lstPU;*/
    }
    
    // Method to query task details
    /*private static List<Task> getTaskDetails(Set<Id> setTaskId) {
        return new List<Task>( [ SELECT WhoId, WhatId, Type, Status, OwnerId, Id, Subject, 
            Assigned_User__c, ActivityDate, Owner.Name, Booking_Unit__c, POA_Right__c
            FROM Task
            WHERE Id IN : setTaskId ] );
    }*/
    
    // Method to get case details
    public static Map<Id, Case> getCaseMap(Set<Id> setCaseId) {
        return new Map<Id, Case>([Select Id, CaseNumber, RecordType.DeveloperName, POA__c, AccountId,
            Customer_First_Name__c, Customer_Last_Name__c, Email__c, Mobile_Phone_2__c, 
            POA_Valid_Till__c, POA_Related_Account__c, Nationality__c,
            New_CR__c, POA_Issued_By_Picklist__c, POA_Number__c, Right_Type__c,
            (Select Id, Name, Attachment_URL__c, Type__c From SR_Attachments__r Where Type__c = 'POA') From Case
            Where Id IN: setCaseId]);
    }
    
    // Method to query POA details
    /*public static Map<Id, POA__c> getPOAMap(Set<Id> setPOAId) {
        return new Map<Id, POA__c>([Select Id, POA_Account__c, Related_to_Account__c, 
            Email_Address__c, First_Name__c, Last_Name__c, Mobile_Number__c, 
            Passport_Number__c, POA_Verification_Status__c, POA_Nationality__c
            From POA__c
            Where Id IN: setPOAId]);
    }*/
}