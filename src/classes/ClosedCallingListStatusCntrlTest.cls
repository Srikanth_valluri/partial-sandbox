@isTest
private class ClosedCallingListStatusCntrlTest {
    
    @testSetup 
   static void testData(){
        //Id walkInRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Walk In Calling List').RecordTypeId;
        TriggerOnOffCustomSetting__c obj1 = new TriggerOnOffCustomSetting__c();
        obj1.Name = 'CallingListTrigger';
        obj1.OnOffCheck__c = true;
        insert obj1;
        List<Calling_List__c>callingLst = new List<Calling_List__c>();
        Account accObj1 = new Account(Name = 'Miss. Madina Alieva');
        insert accObj1;
        Account accObj2 = new Account(Name = 'Miss. Madina Alieva Test');
        insert accObj2;
        Calling_List__c callObj1 = new Calling_List__c(Account__c = accObj1.Id );
        callingLst.add(callObj1);
        Calling_List__c callObj2 = new Calling_List__c(Account__c = accObj2.Id);
        callingLst.add(callObj2);
        insert callingLst;
    }
     static testMethod void changeStatus(){
        List<Calling_List__c> callObjInstLst = [SELECT Id,
                                                        Account__c
                                                  FROM Calling_List__c
                                         LIMIT 2];
        System.debug('inside test class:callObjInst::'+callObjInstLst);
        Test.startTest();
            PageReference serviceDatePage = Page.ClosedCallingListStatus;
            Test.setCurrentPage(serviceDatePage);
             ApexPages.StandardController sc = new ApexPages.StandardController(callObjInstLst[0]);
            ClosedCallingListStatusCntrl controllerObj = new ClosedCallingListStatusCntrl(sc);
           controllerObj.updateStatus();
        Test.stopTest();
       
    }
}