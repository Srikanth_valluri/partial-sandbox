@isTest
private class PopulateSOAForDPInvoiceBatchTest{
    @isTest
    static void itShould1(){
        insert new IpmsRestServices__c(
                SetupOwnerId = UserInfo.getOrganizationId(),
                BaseUrl__c = 'http://83.111.194.181:8045/webservices/rest',
                Username__c = 'oracle_user',
                Password__c = 'crp1user',
                Client_Id__c = '8MABLQM-KJ8I8-1XA58-WWCM1S1',
                BearerToken__c = 'eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWyiwuVrJSKs5IBKK8bCUdpczEEiUrQ1MzAwtTMwsDIx2l1IoCqIC5oRFIoLQ4tSgvMTcVqM_C19HJJ9BX19vLwtNC1zDC0dRCNzzc2dcw2FCpFgBXRb-1XQAAAA.6Ym224Vwr9AniBeq6gL8OM9u4vGnUB_vbEUVjWojg14',
                Timeout__c = 120000
                );

        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        insert bookingUnitList;
        
        DP_Invoices__c obj=new DP_Invoices__c();
        obj.Accounts__c = objAccount.id;
        obj.BookingUnits__c = bookingUnitList[0].id;
        insert obj;
		 String jsonStr =  '   {  '  +
         '       "responseId": "3072019-10305298",  '  +
         '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         '       "status": "S",  '  +
         '       "responseMessage": "Process Completed",  '  +
         '       "elapsedTimeMs": 8497,  '  +
         '       "responseLines": [  '  +
         '           {  '  +
         '               "documentName": "DPSOA",  '  +
         '               "language": "EN",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
         '           },  '  +
         '           {  '  +
         '               "documentName": "DPSOA_AR",  '  +
         '               "language": "AR",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
         '           }  '  +
         '       ],  '  +
         '       "complete": true  '  +
         '  }  ' ;

        Test.startTest();
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1));
            Test.setMock(HttpCalloutMock.class, new DPSoaHttpMock(1));
            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', jsonStr));
           // PopulateTaxInvoiceForDpInvoiceBatch objClass = new PopulateTaxInvoiceForDpInvoiceBatch();
            
            PopulateSOAForDPInvoiceBatch objClass = new PopulateSOAForDPInvoiceBatch();
        	objClass.calloutToGetBlob(
                new List<String>{'https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a'},
                obj
            );
            Database.Executebatch(objClass);
            
            map<DP_Invoices__c,List<Blob>> dpToBlob = new  map<DP_Invoices__c,List<Blob>>();
            list<blob> blobLst = new List<blob>();
            String str= 'test';
            Blob blobStr = Blob.valueOf(str);
            blobLst.add(blobStr);
            dpToBlob.put(obj,blobLst);
            SendContentToCentralRepository.getPublicUrlsOfSOAForDpInvoice(dpToBlob);
            
            
        Test.stopTest();

    }
     @isTest
    static void itShould(){
        insert new IpmsRestServices__c(
                SetupOwnerId = UserInfo.getOrganizationId(),
                BaseUrl__c = 'http://83.111.194.181:8045/webservices/rest',
                Username__c = 'oracle_user',
                Password__c = 'crp1user',
                Client_Id__c = '8MABLQM-KJ8I8-1XA58-WWCM1S1',
                BearerToken__c = 'eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWyiwuVrJSKs5IBKK8bCUdpczEEiUrQ1MzAwtTMwsDIx2l1IoCqIC5oRFIoLQ4tSgvMTcVqM_C19HJJ9BX19vLwtNC1zDC0dRCNzzc2dcw2FCpFgBXRb-1XQAAAA.6Ym224Vwr9AniBeq6gL8OM9u4vGnUB_vbEUVjWojg14',
                Timeout__c = 120000
                );

        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        insert bookingUnitList;
        
        DP_Invoices__c obj=new DP_Invoices__c();
        obj.Accounts__c = objAccount.id;
        obj.BookingUnits__c = bookingUnitList[0].id;
        insert obj;

        Test.startTest();
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1));
            Test.setMock(HttpCalloutMock.class, new DPSoaHttpMock(1));
            PopulateSOAForDPInvoiceBatch objClass = new PopulateSOAForDPInvoiceBatch();
            
            Database.Executebatch(objClass);
        Test.stopTest();

    }
}