public with sharing class UpdateJBBuyerOnCRMAllinOneBatch implements Database.Batchable<sObject> {
    /********************************************************************************************* 
    * @Description : .           *
    * @Params      : Database.BatchableContext                                                   *
    * @Return      : Database.QueryLocator                                                       *
    *********************************************************************************************/  
    public Database.QueryLocator start(Database.BatchableContext BC){
       
        String query = 'SELECT Id, JB_Name__c, JB_Party_Id__c, '+
                        'JB_Name_2__c, JB2_Party_Id__c ,'+
                        'JB3_Name__c, JB3_Party_Id__c ,'+
                        'Registration_Id__c, Unit__c, Unit__r.Booking__c '+
                       'FROM CRMAllinOne__c '+
                       'WHERE Registration_Id__c != NULL AND '+
                                'Unit__c != NULL AND '+
                               'JB_Name__c = NULL AND '+
                               'JB_Name_2__c = NULL AND '+
                               'JB3_Name__c = NULL ';
        system.debug('query : '+query);
        system.debug('Database.getQueryLocator(query) : '+ Database.getQueryLocator(query));
                                
      return Database.getQueryLocator(query);
    }
   
    /*********************************************************************************************
    * @Description :  *
    * @Params      : Database.BatchableContext, List<sObject>                                    *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void execute(Database.BatchableContext BC, List<SObject> CRMAllinOneLst){
        if( CRMAllinOneLst != NULL && CRMAllinOneLst.size()> 0 ){

            Set<Id> BookingIdSet = new Set<Id>();//camel case
            for(CRMAllinOne__c CRMObj : (List<CRMAllinOne__c>)CRMAllinOneLst){
                if(CRMObj != null && CRMObj.Unit__c!= null && CRMObj.Unit__r.Booking__c != NULL) // add null checks
                BookingIdSet.add(CRMObj.Unit__r.Booking__c);
            }
            List<Buyer__c> buyerLst;
            // BookingIdSet set null check 
            if(BookingIdSet != null){
                 buyerLst = [SELECT Booking__c, Id
                                            , Primary_Buyer__c
                                            , First_Name__c
                                            , Last_Name__c 
                                            , Party_ID__c
                                        FROM Buyer__c 
                                        WHERE Booking__c IN: BookingIdSet 
                                        AND Primary_Buyer__c = FALSE
                                    ];
                
            }
            

            Map<Id,List<Buyer__c>> MapBookingIdBuyers = new Map<Id,List<Buyer__c>>(); 
            // list null check
            if( buyerLst != NULL && buyerLst.size() > 0 ){
                for( Buyer__c objBuyer : buyerLst) {
                    if( objBuyer != null && objBuyer.Booking__c != null ) {
                        if( MapBookingIdBuyers.containsKey( objBuyer.Booking__c ) 
                            && MapBookingIdBuyers.get( objBuyer.Booking__c ) != null ) {
                            List<Buyer__c> lstJointBuyers = MapBookingIdBuyers.get( objBuyer.Booking__c );
                            lstJointBuyers.add(objBuyer);
                            MapBookingIdBuyers.put( objBuyer.Booking__c,lstJointBuyers );
                        } else {
                            MapBookingIdBuyers.put( objBuyer.Booking__c,new List<Buyer__c>{objBuyer});
                        }
                    }
                }
            }
            
            //JB_Name__c, JB_Party_Id__c   Party_ID__c
            //JB_Name_2__c, JB2_Party_Id__c 
            //JB3_Name__c, JB3_Party_Id__c
            List<CRMAllinOne__c> lstCRMAllinOne = new List<CRMAllinOne__c>();
            for(CRMAllinOne__c CRMObj : (List<CRMAllinOne__c>)CRMAllinOneLst){
                if(CRMObj.Unit__r.Booking__c != NULL && MapBookingIdBuyers.containsKey(CRMObj.Unit__r.Booking__c) ){
                    // booking contains check
                    List<Buyer__c> lstBuyer = MapBookingIdBuyers.get(CRMObj.Unit__r.Booking__c); // change list name
                    if(lstBuyer != NULL && lstBuyer.size() == 1 ){
                        CRMObj.JB_Name__c =lstBuyer[0].First_Name__c + lstBuyer[0].Last_Name__c;
                        CRMObj.JB_Party_Id__c =lstBuyer[0].Party_ID__c;
                        lstCRMAllinOne.add(CRMObj);
                    }
                    if(lstBuyer != NULL && lstBuyer.size() == 2){ // add equals to 
                        CRMObj.JB_Name__c =lstBuyer[0].First_Name__c + lstBuyer[0].Last_Name__c;
                        CRMObj.JB_Party_Id__c =lstBuyer[0].Party_ID__c;
                        CRMObj.JB_Name__c =lstBuyer[1].First_Name__c + lstBuyer[1].Last_Name__c;
                        CRMObj.JB_Party_Id__c =lstBuyer[1].Party_ID__c;
                        lstCRMAllinOne.add(CRMObj);
                    }
                    if(lstBuyer != NULL && lstBuyer.size() == 3){
                        CRMObj.JB_Name__c =lstBuyer[0].First_Name__c + lstBuyer[0].Last_Name__c;
                        CRMObj.JB_Party_Id__c =lstBuyer[0].Party_ID__c;
                        CRMObj.JB_Name__c =lstBuyer[1].First_Name__c + lstBuyer[1].Last_Name__c;
                        CRMObj.JB_Party_Id__c =lstBuyer[1].Party_ID__c;
                        CRMObj.JB_Name__c =lstBuyer[2].First_Name__c + lstBuyer[2].Last_Name__c;
                        CRMObj.JB_Party_Id__c =lstBuyer[2].Party_ID__c;
                        lstCRMAllinOne.add(CRMObj);
                    }
                }
            }
            if(lstCRMAllinOne != null && lstCRMAllinOne.size() > 0 )
            update lstCRMAllinOne;
        }
    }
    
    /*********************************************************************************************
    * @Description :            *
    * @Params      : Database.BatchableContext                                                   *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void finish(Database.BatchableContext BC){
    }
}