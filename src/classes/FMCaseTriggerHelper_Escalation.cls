public without sharing class FMCaseTriggerHelper_Escalation {

    private static final set<Integer> setDaysNotToConsider = new set<Integer>{ 5, 6 };

    public static void beforeInsert_updateFMCaseForEscation(List<FM_Case__c> newlstFMCases) {
        Map<String,List<FM_Case__c>> mapBuildingCodeFMCases=new Map<String,List<FM_Case__c>>();
        map<String,list<FM_Case__c>> mapPOPCases = new map<String,list<FM_Case__c>>();

        System.debug('newlstFMCases in method is: ' + newlstFMCases);
        for(FM_Case__c insFMCase:newlstFMCases) {
            SYstem.debug('instance FM Case: ' + insFMCase);
            System.debug('insFMCase.Status__c: '+insFMCase.Status__c);
            System.debug('insFMCase.Request_Type_DeveloperName__c: ' + insFMCase.Request_Type_DeveloperName__c);
            if( insFMCase.Status__c == 'Submitted' ) {
                System.debug('insFMCase.Request_Type_DeveloperName__c: ' + insFMCase.Request_Type_DeveloperName__c);
                if( String.isNotBlank(insFMCase.Unit_Name__c) ) {
                    if(mapBuildingCodeFMCases.containsKey(insFMCase.Unit_Name__c.split('/')[0])) {
                        mapBuildingCodeFMCases.get(insFMCase.Unit_Name__c.split('/')[0]).add(insFMCase);
                    }
                    else {
                        mapBuildingCodeFMCases.put(insFMCase.Unit_Name__c.split('/')[0],
                            new List<FM_Case__c>{insFMCase});
                    }
                }

                else if( String.isNotBlank( insFMCase.Request_Type_DeveloperName__c ) &&
                         insFMCase.Request_Type_DeveloperName__c.equalsIgnoreCase( 'Proof_of_Payment' ) ) {


                    System.debug('-->1 mapPOPCases : ' + mapPOPCases);
                    if( mapPOPCases.containsKey( insFMCase.Request_Type_DeveloperName__c ) ) {
                        System.debug('--- > Inside IF <---');
                        mapPOPCases.get( insFMCase.Request_Type_DeveloperName__c ).add( insFMCase );
                    }
                    else {
                        mapPOPCases.put( insFMCase.Request_Type_DeveloperName__c, new list<FM_Case__c>{ insFMCase } );
                        System.debug('-->2 mapPOPCases : ' + mapPOPCases);
                    }
                }
            }//if
        }//for

        System.debug('--> 3 Out of If-Else mapPOPCases: ' + mapPOPCases);
        if( !mapPOPCases.isEmpty() ) {
            calculatePOPEscalation( mapPOPCases );
        }

        if( mapBuildingCodeFMCases.size() > 0 ) {
            updateFMCases( mapBuildingCodeFMCases );
        }

    }

    public static void updateFMCaseForEscation(List<FM_Case__c> newlstFMCases, map<Id,FM_Case__c> oldmapFMCases) {

        Map<String,List<FM_Case__c>> mapBuildingCodeFMCases=new Map<String,List<FM_Case__c>>();
        map<String,list<FM_Case__c>> mapPOPCases = new map<String,list<FM_Case__c>>();
        System.debug('newlstFMCases = ' + newlstFMCases);
        System.debug('oldmapFMCases = ' + oldmapFMCases);
        for( FM_Case__c insFMCase:newlstFMCases ) {
            if( insFMCase.Status__c != oldmapFMCases.get(insFMCase.Id).Status__c && insFMCase.Status__c == 'Submitted' ) {
                if( String.isNotBlank(insFMCase.Unit_Name__c) ) {
                    if(mapBuildingCodeFMCases.containsKey(insFMCase.Unit_Name__c)) {
                        mapBuildingCodeFMCases.get(insFMCase.Unit_Name__c.split('/')[0]).add(insFMCase);
                    }
                    else {
                        mapBuildingCodeFMCases.put(insFMCase.Unit_Name__c.split('/')[0],
                            new List<FM_Case__c>{insFMCase});
                    }
                }//if
                else if( String.isNotBlank( insFMCase.Request_Type_DeveloperName__c ) &&
                         insFMCase.Request_Type_DeveloperName__c.equalsIgnoreCase( 'Proof_of_Payment' ) ) {

                    if( mapPOPCases.containsKey( insFMCase.Request_Type_DeveloperName__c ) ) {
                        mapPOPCases.get( insFMCase.Request_Type_DeveloperName__c ).add( insFMCase );
                    }
                    else {
                        mapPOPCases.put( insFMCase.Request_Type_DeveloperName__c, new list<FM_Case__c>{ insFMCase } );
                    }
                }
            }
        }//for

        if( !mapPOPCases.isEmpty() ) {
            calculatePOPEscalation( mapPOPCases );
        }

        if( mapBuildingCodeFMCases.size() > 0 ) {
            updateFMCases( mapBuildingCodeFMCases );
        }
    }//


    private static void updateFMCases(Map<String,List<FM_Case__c>> mapBuildingCodeFMCases ) {
        List<FM_Case__c> lstFMCaseToUpdate=new List<FM_Case__c>();
        Datetime dt = System.Now();
        String dayOfWeek=dt.format('EEEE');
        System.debug('===dayOfWeek==='+dayOfWeek);
        List<FM_User__c> lstFMUser=new List<FM_User__c>();
        lstFMUser=[select fm_user__r.Email,FM_Role__c,
                            Building__r.Name
                    from FM_User__c
                    where (FM_Role__c='FM Director'
                    OR FM_Role__c = 'Property Director')
                    AND FM_User__r.isActive = TRUE
                    AND  Building__r.Name IN :mapBuildingCodeFMCases.keySet()];

        List<FM_Process_Escalation_Matrix__mdt>
        lstCaseEscMetrics=[select Escalate_After_in_hours__c,
                                    Role__c,
                                    FM_Case_Day_of_Week__r.MasterLabel
                            from FM_Process_Escalation_Matrix__mdt
                            WHERE FM_Case_Day_of_Week__r.MasterLabel =: dayOfWeek];

        Integer daysToAdd=0;
        System.debug('===daysToAdd==='+daysToAdd);
        for(FM_Process_Escalation_Matrix__mdt ins:lstCaseEscMetrics) {
            System.debug('===ins==='+ins);
            if(ins.FM_Case_Day_of_Week__r.MasterLabel == dayOfWeek && ins.Role__c=='FM Director'){
                daysToAdd = (Integer)ins.Escalate_After_in_hours__c;
                break;
            }
        }
        System.debug('===daysToAdd==='+daysToAdd);

        for(FM_User__c insFMUSer:lstFMUser) {
            if(mapBuildingCodeFMCases.containsKey(insFMUser.Building__r.Name)) {
                for(FM_Case__c recordToUpdate : mapBuildingCodeFMCases.get(insFMUser.Building__r.Name)) {
                    if(insFMUSer.FM_Role__c == 'FM Director') {
                        recordToUpdate.FM_Director_Email__c = insFMUSer.fm_user__r.Email;
                    }
                    else if(insFMUSer.FM_Role__c == 'Property Director') {
                        recordToUpdate.Property_Director_Email__c = insFMUSer.fm_user__r.Email;
                    }


                    recordToUpdate.Escalation_Date__c = Date.Today().addDays(daysToAdd);
                    // lstFMCaseToUpdate.add(recordToUpdate);
                }
            }
        }
    }

    private static void calculatePOPEscalation( map<String,list<FM_Case__c>> mapRecordDeveloperNameCases ) {
        if( mapRecordDeveloperNameCases != NULL && !mapRecordDeveloperNameCases.isEmpty() ) {
            System.debug('mapRecordDeveloperNameCases.keySet() :' + mapRecordDeveloperNameCases.keySet());
            list<FM_Process_Escalation_Matrix__mdt> lstEscaltionMatrix = [ SELECT Escalate_After_in_hours__c
                                                                                , FM_Process__r.DeveloperName
                                                                             FROM FM_Process_Escalation_Matrix__mdt
                                                                            WHERE FM_Process__r.DeveloperName = :mapRecordDeveloperNameCases.keySet()
                                                                         ORDER BY DeveloperName ];
            //Debugs 11 july
            System.debug('--> lstEscaltionMatrix mdt: ' + lstEscaltionMatrix);
            System.debug('-->> FM_Process__r.DeveloperName from lstEscaltionMatrix: '+ lstEscaltionMatrix[0].FM_Process__r.DeveloperName);
            map<String,FM_Process_Escalation_Matrix__mdt> mapProcessEscaltion ;
            if( lstEscaltionMatrix != NULL && !lstEscaltionMatrix.isEmpty() ) {
                mapProcessEscaltion = new map<String,FM_Process_Escalation_Matrix__mdt>();
                for( FM_Process_Escalation_Matrix__mdt objMeta : lstEscaltionMatrix ) {
                    if( !mapProcessEscaltion.containsKey( objMeta.FM_Process__r.DeveloperName ) ) {
                        System.debug('objMeta.FM_Process__r.DeveloperName' + objMeta.FM_Process__r.DeveloperName);
                        mapProcessEscaltion.put( objMeta.FM_Process__r.DeveloperName, objMeta );
                        //Debugs 11 july
                        System.debug('Map of FM_processDeveloperName, mdtObj :' + mapProcessEscaltion);
                    }
                }
            }

            if( mapProcessEscaltion != NULL && !mapProcessEscaltion.isEmpty() ) {
                System.debug('mapRecordDeveloperNameCases = ' + mapRecordDeveloperNameCases);
                for( String strDeveloperName : mapRecordDeveloperNameCases.keySet() ) {
                    System.debug('mapProcessEscaltion = ' + JSON.serializePretty(mapProcessEscaltion));
                    System.debug('strDeveloperName = ' + strDeveloperName);
                    System.debug('mapProcessEscaltion.get( strDeveloperName ) = ' + mapProcessEscaltion.get( strDeveloperName ));
                    if( mapProcessEscaltion.containsKey( strDeveloperName ) && mapProcessEscaltion.get( strDeveloperName ) != NULL &&
                        mapProcessEscaltion.get( strDeveloperName ).Escalate_After_in_hours__c != NULL ) {
                        for( FM_Case__c objFMCase : mapRecordDeveloperNameCases.get( strDeveloperName ) ) {
                            //Debugs 11 july
                            System.debug('Fm Director EMail from label: ' + Label.FM_POP_Escalation_Email);
                            System.debug('Escalation Date: '+ Date.Today().addDays( Integer.valueOf ( mapProcessEscaltion.get( strDeveloperName ).Escalate_After_in_hours__c ) ));
                            //
                            objFMCase.FM_Director_Email__c = Label.FM_POP_Escalation_Email ;
                            objFMCase.Escalation_Date__c = Date.Today().addDays( Integer.valueOf ( mapProcessEscaltion.get( strDeveloperName ).Escalate_After_in_hours__c ) );
                            Integer intDayOfWeek = 0 ;
                            do {
                                intDayOfWeek = Math.mod( date.newInstance(1900, 07, 01).daysBetween( objFMCase.Escalation_Date__c ), 7 );
                                System.debug('intDaysOfWeek' + intDayOfWeek);
                                if(  setDaysNotToConsider.contains( intDayOfWeek )  ) {
                                    objFMCase.Escalation_Date__c = objFMCase.Escalation_Date__c.addDays( 1 );
                                }
                            } while( setDaysNotToConsider.contains( intDayOfWeek ) );
                        }
                    }
                }
            }
        }
    }
}