public class fmcRequestForWorkPermitControllerNew extends RequestForWorkPermitControllerNew{
    public  List<Booking_Unit__c>      objUnitList                      {get; set;}
    public  map<Id,Booking_Unit__c>    mapUnitsOwned                    {get; set;}
    public  map<Id,Booking_Unit__c>    mapMyUnits                       {get; set;}
    public  List<Id>                   objUnitOwner                     {get; set;}
    public  List<Id>                   objUnitRes                       {get; set;}
    public  Boolean                    hasOutstandingServiceCharges     {get; set;}
    @TestVisible
    public  String                     strUnitId                        {get; set;}
    public  Integer                    attchmentIndex                   {get; set;}

    public  Integer                     attachCounter = 0;

    public String                       view                            {get; set;}
    public Boolean                      proceedToSR                     {get; set;}
    //public String                       callJS                          {get; set;}
    public Boolean                      renderBool                      {get; set;}
    public Boolean                      showOTPBlock                    {get; set;}

    //public void doNothing() {
    //    proceedToSR = true;
    //}

    public fmcRequestForWorkPermitControllerNew() {
        super(false);
        //proceedToSR = false;
        //showOTPBlock = true;
        //proceedToSR = true;
        showOTPBlock = false;

        System.debug('renderBool: '+renderBool);
        if (FmcUtils.isCurrentView('ApplyWorkPermit')) {
            //super();
            contractorConsultantSuccessMessage += '<br/> An email with a link for uploading the below'
                                                +' required documents is send to the email id provided'
                                                +' above for contractor/ consultant. Please inform the'
                                                +' contractor/ consultant about the same. <br/> Please'
                                                +' note that the request will be submitted only once all'
                                                +' mandatory details and documents are uploaded.';
            hasOutstandingServiceCharges = false;
            instanceList = new List<FM_Case__c>();
            valueListOfDrawings = new List<SelectOption>();
            valuesOfDrawings = new List<String>();
            isEdittable = true;
            valueList = new SelectOption[0];
            values = new String[0];
            isMandatory = false;
            minimumOutstandingCharge = 0;
            //strAccountId = CustomerCommunityUtils.customerAccountId;
            strFMCaseId = ApexPages.currentPage().getParameters().get('Id');
            strSRType = 'Apply_for_Work_Permits';
            objFMCase = new FM_Case__c();
            if(String.isBlank(strFMCaseId)) {
                strAccountId = CustomerCommunityUtils.customerAccountId;
                strSelectedUnit = ApexPages.currentPage().getParameters().get('UnitId');
            }else {
                objFMCase = FM_Utility.getCaseDetails( strFMCaseId );
                strAccountId = objFMCase.Account__c ;
                strSelectedUnit = objFMCase.Booking_Unit__c;
                objFMCase = FM_Utility.getCaseDetails( strFMCaseId );
                fetchUnits();
                getfieldValues();
                init();
            }
            system.debug('== strSelectedUnit =='+strSelectedUnit );
            strSRType = 'Apply_for_Work_Permits';
            objFMCase.Request_Type_DeveloperName__c = 'Apply_for_Work_Permits' ;
            objFMCase.Request_Type__c = 'Work Permit';
            objFMCase.Outstanding_service_charges__c = '0';
            objUnit = new Booking_Unit__c();
            system.debug('== strAccountId =='+strAccountId );
            fetchUnits();
            if (String.isNotBlank(strSelectedUnit)) {
                //check the OTP verified field on FM Case - (If true then showOTPBlock =false);
                if(objFMCase.IsOTPVerified__c == false) {
                    showOTPBlock = true;
                }
                else {
                    showOTPBlock = false;
                    proceedToSr = true;
                }

            }
            urlForTermsAndCondition = openTermsAndConditionWindow();
        }
    }

    public void EnableSRProcess() {
        System.debug('Making proceedToSR : TRUE');
        System.debug('hasOutstandingServiceCharges = ' + hasOutstandingServiceCharges);
        proceedToSR = true;
        showOTPBlock = false;
    }

    public override void createMethod(){
        if(objFMCase != NULL) {
            objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName()
                                        .get('Work Permit').getRecordTypeId();
            objFMCase.Permit_To_Work_For__c = String.join(valuesOfDrawings ,';');
            objFMCase.Origin__c = 'Portal';
            objFMCase.Account__c = strAccountId ;
            objFMCase.Booking_Unit__c = strSelectedUnit ;
            objFMCase.Request_Type_DeveloperName__c = strSRType ;
            objFMCase.status__c ='Draft Request';
            //Added on 25/11/19
            System.debug('While saving proceedToSr : '+proceedToSR+' showOTPBlock is: '+showOTPBlock);
            if(proceedToSR == true && showOTPBlock == false) {
                objFMCase.IsOTPVerified__c = true;
            }

            System.debug('objFMCase=== : '+objFMCase);
            if(!String.isBlank(objFMCase.Email__c) || !String.isBlank(objFMCase.Email_2__c)) {
                objFMCase.Contact_Email__c = !String.isBlank(objFMCase.Email__c)
                                                ? objFMCase.Email__c : objFMCase.Email_2__c ;
            }
            objFMCase.Security_Deposit_Cheque_Amount__c = !String.isBlank(
                    securityChequeDepositAmount) ? Decimal.valueOf(securityChequeDepositAmount) : 0;
            objFMCase.Garbage_disposal_Amount__c = !String.isBlank(
                                garbageDisposalAmount) ? Decimal.valueOf(garbageDisposalAmount) : 0;
            objFMCase.Temporary_power_water_connection_Amount__c = String.isNotBlank(
                        temporaryPowerConnAmount) ? Decimal.valueOf(temporaryPowerConnAmount) : 0;
            upsert objFMCase;
          System.debug('>>>>objFMCase.id - :'+ objFMCase.id);
        }

    }

    public void fetchUnits(){
        Set<String> activeStatusSet= Booking_Unit_Active_Status__c.getall().keyset();
        mapUnitsOwned = new map<Id,Booking_Unit__c>();
        objUnitOwner = new List<Id>();
        objUnitRes = new List<Id>();
        mapMyUnits = new map<Id,Booking_Unit__c>();
        lstUnits = new list<SelectOption>();
        lstUnits.add(new selectOption('None','--None--'));
        objUnitList = [SELECT Id
                           , Owner__c
                           , Booking__r.Account__c
                           , Booking__r.Account__r.Name
                           , Owner__r.IsPersonAccount
                           , Tenant__r.IsPersonAccount
                           , Owner__r.Name
                           , Tenant__c
                           , Tenant__r.Name
                           , Resident__c
                           , Resident__r.Name
                           , Unit_Name__c
                           , Registration_Id__c
                           , Property_Name__c
                           , Property_City__c
                       FROM Booking_Unit__c
                       WHERE ( Resident__c = :strAccountId
                              OR Booking__r.Account__c = :strAccountId
                              OR Tenant__c = :strAccountId )
                       AND (
                            ( (Handover_Flag__c = 'Y' OR Early_Handover__c = true )
                               AND Registration_Status__c IN :activeStatusSet)
                            OR Dummy_Booking_Unit__c = true
                       )
        ];
        System.debug('objUnitList---- : ' +objUnitList);
        if(!objUnitList.isEmpty() && objUnitList != NULL) {
            for( Booking_Unit__c objBU : objUnitList ) {
                if( objBU.Owner__c == strAccountId ) {
                    objUnitOwner.add(objBU.Id);
                    mapUnitsOwned.put(objBU.Id,objBU);
                }
                if( ( objBU.Tenant__c == strAccountId || objBU.Resident__c == strAccountId )
                            && objBU.Owner__c != strAccountId ) {
                    objUnitRes.add(objBU.Id);
                    mapMyUnits.put(objBU.Id,objBU);
                }
                lstUnits.add(new selectOption(objBU.Id,objBU.Unit_Name__c));
                System.debug('lstUnits--- : '+lstUnits);
            }
        }else {
            ApexPages.addmessage(new ApexPages.message(
                        ApexPages.severity.Error,'No Booking Units available'));
        }
        system.debug('==lstUnits=='+lstUnits);
    }

    /* This Method is used to create Instance of Contractor Information */
    public override void createContractorInformation(){
         ContractorInfoList = new List<Contractor_Information__c>();
        String NumberOfEmployees=Apexpages.currentPage().getParameters().get('noOfEmp');
        System.debug('Inside create Contract Info Method NumberOfEmployees----'+NumberOfEmployees);
        Integer NumberOfEmp = Integer.valueOf(NumberOfEmployees);
        if(NumberOfEmployees != NULL && NumberOfEmp > 0 && NumberOfEmployees !='--None--' ){
            for(Integer count=0; count<NumberOfEmp; count++){
                Contractor_Information__c contractorObj = new Contractor_Information__c();
                System.debug('contractorObj:::::::'+contractorObj);
                ContractorInfoList.add(contractorObj);
            }
        }
        System.debug('------ContractorInfoList----'+ContractorInfoList.size());
        //return ContractorInfoList;
    }

    public override void init() {
        //initializeFMCase();
        System.debug('objFMCase.Submitted__c======'+objFMCase.Submitted__c);

        valueListOfDrawings.clear();
        valueListOfDrawings = new List<SelectOption>();
        valuesOfDrawings.clear();
        valuesOfDrawings = new List<String>();

        if(objFMCase.Submitted__c==true && objFMCase.Approval_Status__c != 'Rejected') {
            isEdittable=false;
        }
        System.debug('isEdittable======'+isEdittable);
        System.debug('strSelectedUnit==== :'+strSelectedUnit);
        objUnit = getUnitDetails(strSelectedUnit) ;

        //List<Location__c> objList=new List<Location__c>();
        if(objUnit != NULL && objUnit.Inventory__r.Building_Location__c != NULL ) {
            objList=[SELECT Temporary_power_water_connection_Amount__c
                            , Property_name__r.Name
                            , Security_Deposit_Cheque_Amount__c
                            , Garbage_disposal_Amount__c
                     FROM Location__c
                     WHERE id=:objUnit.Inventory__r.Building_Location__c
            ];
        }

        System.debug('objList--- : '+objList);
        if(objList != NULL && !objList.isEmpty())  {
            securityChequeDepositAmount = objList[0].Security_Deposit_Cheque_Amount__c != NULL
                                ? String.valueof(objList[0].Security_Deposit_Cheque_Amount__c) : '';
            garbageDisposalAmount = objList[0].Garbage_disposal_Amount__c!= NULL
                                    ? String.valueof(objList[0].Garbage_disposal_Amount__c) : '';
            temporaryPowerConnAmount = objList[0].Temporary_power_water_connection_Amount__c!= NULL
                    ? String.valueof(objList[0].Temporary_power_water_connection_Amount__c) : '';

        }
        System.debug('objUnit-->>> : '+objUnit);
        System.debug('strSRType-->>> : '+objUnit);
        if( String.isNotBlank( strSRType ) && objUnit != NULL ) {
            processDocuments();
        }
        System.debug('-----objFMCase.id-----'+objFMCase.id);
        System.debug('fmc lstDocument'+lstDocuments);
        getfieldValues();
        System.debug('valueListOfDrawings----> '+ valueListOfDrawings);
        system.debug('>>>>strAccountId : '+strAccountId);
        system.debug('>>>>strSelectedUnit : '+strSelectedUnit);

        instanceList = FM_Utility.getExistingFMCase( 'NOC_For_FitOut', strAccountId, strSelectedUnit );
         System.debug('>>>>>instanceList===='+instanceList);



        if(!instanceList.isEmpty()){
            objFMCase.Account__c = instanceList[0].Account__c;
            objFMCase.Booking_Unit__c = instanceList[0].Booking_unit__c;
            objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName()
                                        .get('Work Permit').getRecordTypeId();
            objFMCase.Request_Type_DeveloperName__c = strSRType ;
            objFMCase.Request_Type__c = 'Work Permit';
            objFMCase.Description__c = instanceList[0].Description__c;
            objFMCase.Person_To_Collect__c = instanceList[0].Person_To_Collect__c;
            objFMCase.Contact_person__c = instanceList[0].Contact_person__c;
            objFMCase.Contact_person_contractor__c = instanceList[0].Contact_person_contractor__c;
            objFMCase.Mobile_no__c = instanceList[0].Mobile_no__c;
            objFMCase.Mobile_no_contractor__c = instanceList[0].Mobile_no_contractor__c;
            objFMCase.Contact_Email__c = instanceList[0].Contact_Email__c;
            objFMCase.Office_tel__c = instanceList[0].Office_tel__c;
            objFMCase.Office_tel_contractor__c = instanceList[0].Office_tel_contractor__c;
            objFMCase.Mobile_Country_Code__c = instanceList[0].Mobile_Country_Code__c;
            objFMCase.Mobile_Country_Code_2__c = instanceList[0].Mobile_Country_Code_2__c;
            objFMCase.Mobile_Country_Code_3__c = instanceList[0].Mobile_Country_Code_3__c;
            objFMCase.Mobile_Country_Code_4__c = instanceList[0].Mobile_Country_Code_4__c;
            objFMCase.Parent_case__c = instanceList[0].Id;
            objFMCase.Email__c = instanceList[0].Email__c;
            objFMCase.Email_2__c = instanceList[0].Email_2__c;
            //objFMCase.Company_Name__c = instanceList[0].Company_Name__c;



            if(!String.isEmpty(instanceList[0].Permit_To_Work_For__c)){
                String[] fetchedValues=instanceList[0].Permit_To_Work_For__c.split(';');
                valuesOfDrawings=fetchedValues;
            }
            objUnit = getUnitDetails(strSelectedUnit) ;
            if( String.isNotBlank( strSRType ) && objUnit != NULL ) {
                processDocuments();
            }
            getfieldValues();
        }
        if( objUnit != NULL ) {
            try {
                FmIpmsRestServices.DueInvoicesResult objResponse = FmIpmsRestServices.getDueInvoices(
                                objUnit.Registration_Id__c,'',objUnit.Inventory__r.Property_Code__c );
                system.debug('== objResult =='+objResponse);
                System.debug('objResponse.totalDueAmount=== : '+objResponse.totalDueAmount);
                if( objResponse != NULL ) {
                    objFMCase.Outstanding_service_charges__c = String.isNotBlank(
                                    objResponse.totalDueAmount ) ? objResponse.totalDueAmount : '0';
                    System.debug('Outstanding_service_charges__c======= : '+ objFMCase.Outstanding_service_charges__c);
                }
            }
            catch( Exception e ) {
                system.debug( '----Exception while fetching oustanding balance----'+e.getMessage() );
            }
        }
        objFMCase.Request_Type_DeveloperName__c = 'Apply_for_Work_Permits' ;
        FM_process__mdt fmProcessMetadata=[ SELECT Minimum_Outstanding_Charge__c
                                            FROM FM_process__mdt
                                            WHERE DeveloperName = :objFMCase.Request_Type_DeveloperName__c];
        System.debug('Minimum_Outstanding_Charge__c=======:'+fmProcessMetadata.Minimum_Outstanding_Charge__c);
        if(fmProcessMetadata != NULL){
            minimumOutstandingCharge = fmProcessMetadata.Minimum_Outstanding_Charge__c != NULL
                                              ? fmProcessMetadata.Minimum_Outstanding_Charge__c : 0;
            System.debug('minimumOutstandingCharge=== : '+minimumOutstandingCharge);
        }
        system.debug( '==objFMCase.Outstanding_service_charges__c=='+objFMCase.Outstanding_service_charges__c );
        System.debug('objFMCase===='+objFMCase);
        if(objFMCase != NULL && String.isNotBlank(objFMCase.Outstanding_service_charges__c)
                && Decimal.valueOf(objFMCase.Outstanding_service_charges__c) > minimumOutstandingCharge) {
          hasOutstandingServiceCharges = true;
          System.debug('hasOutstandingServiceCharges====:'+hasOutstandingServiceCharges);
          ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.WARNING,
                        'Request for NOC can be submitted only on clearance of Service Charges'));
        }
    }

    public pageReference getUnitDetailsNew() {
      system.debug('strSelectedUnit : '+strSelectedUnit);
        if(strSelectedUnit != null) {
            showOTPBlock = true;
        }
        objFMCase.Description__c = '';
        objFMCase.Person_To_Collect__c = '';
        valueListOfDrawings.clear();
        valueListOfDrawings = new List<SelectOption>();
        valuesOfDrawings.clear();
        valuesOfDrawings=new List<String>();
        system.debug('strSelectedUnit : '+strSelectedUnit);
        if (String.isBlank(strSelectedUnit)) {
            return NULL;
        }
        init();
        return null;
    }

    public override PageReference submitRequestForWorkPermit() {
        if( objFMCase != NULL ) {
            System.debug('objFMCase--------'+objFMCase);
            if(objFMCase.id==null){
                objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName()
                                            .get('Work Permit').getRecordTypeId();
                objFMCase.Account__c = strAccountId ;
                objFMCase.Booking_Unit__c = strSelectedUnit ;
                objFMCase.Request_Type_DeveloperName__c = strSRType ;
                objFMCase.Origin__c = 'Portal';
                insert objFMCase;

                // Insert Contractor Information Records - 14/08/19
                if(ContractorInfoList != null && !ContractorInfoList.isEmpty()) {
                    for(Contractor_Information__c contractorObj : ContractorInfoList){
                        contractorObj.FM_Case__c = objFMCase.Id;
                    }
                    insert ContractorInfoList;
                }

            }
            else{
                 // Insert Contractor Information Records
                if(ContractorInfoList != null && !ContractorInfoList.isEmpty()) {
                    for(Contractor_Information__c contractorObj : ContractorInfoList){
                        contractorObj.FM_Case__c = objFMCase.Id;
                    }
                    insert ContractorInfoList;
                }

            }
            //
            if(!String.isBlank(objFMCase.Email__c) || !String.isBlank(objFMCase.Email_2__c)){
                objFMCase.Contact_Email__c = !String.isBlank(objFMCase.Email__c)
                                                ? objFMCase.Email__c : objFMCase.Email_2__c ;
            }
            objFMCase.Security_Deposit_Cheque_Amount__c = !String.isBlank(
                    securityChequeDepositAmount) ? Decimal.valueOf(securityChequeDepositAmount) : 0;
            objFMCase.Garbage_disposal_Amount__c = !String.isBlank(garbageDisposalAmount)
                                                    ? Decimal.valueOf(garbageDisposalAmount) : 0;
            objFMCase.Temporary_power_water_connection_Amount__c = !String.isBlank(
                            temporaryPowerConnAmount) ? Decimal.valueOf(temporaryPowerConnAmount) : 0;
            objFMCase.Permit_To_Work_For__c=String.join(valuesOfDrawings ,';');
            objFMCase.Origin__c = 'Portal';
            objFMCase.Status__c='Submitted';
            objFMCase.Submitted__c=true;

            System.debug('While submitting proceedToSr : '+proceedToSR+' showOTPBlock is: '+showOTPBlock);
            if(proceedToSR == true && showOTPBlock == false) {
                objFMCase.IsOTPVerified__c = true;
            }

            System.debug('objFMCase.submitted__c====='+objFMCase.Submitted__c);
            if(objFMCase.Submitted__c==true){
                System.debug('objFMCase.Unit_Name__c-----'+objUnit.Unit_Name__c);
                if( String.isNotBlank( objUnit.Unit_Name__c ) ) {
                    list<FM_User__c> lstUsers = FM_Utility.getApprovingUsers( new set<String> {
                                                            objUnit.Unit_Name__c.split('/')[0] } );
                    System.debug('lstUsers ======'+lstUsers);
                    for( FM_User__c objUser : lstUsers ) {
                        if( String.isNotBlank( objUser.FM_Role__c ) ) {
                            if( objUser.FM_Role__c.containsIgnoreCase( 'Manager') ) {
                                objFMCase.FM_Manager_Email__c = objUser.FM_User__r.Email;
                            }
                            else if( objUser.FM_Role__c.containsIgnoreCase( 'Admin')
                                        && objFMCase.Admin__c == NULL ) {
                                objFMCase.Admin__c = objUser.FM_User__c;
                            }
                        }
                    }
                }
                System.debug('objFMCase.submitted__c=====1'+objFMCase.Submitted__c);
                String strApprovingUsers='';
                System.debug('objFMCase.Request_Type_DeveloperName__c===='
                                    +objFMCase.Request_Type_DeveloperName__c);
                System.debug('objUnit.Property_City__c'+objUnit.Property_City__c);
                List<FM_Approver__mdt> listApproverUser = FM_Utility.fetchApprovers(
                                objFMCase.Request_Type_DeveloperName__c,objUnit.Property_City__c);
                System.debug('listApproverUser====='+listApproverUser);
                if(!listApproverUser.isEmpty()) {
                    System.debug('----1----');
                    for(FM_Approver__mdt fmApproverInstance:listApproverUser) {
                        System.debug('----2----');
                        strApprovingUsers=strApprovingUsers+fmApproverInstance.Role__c+',';
                    }
                    System.debug('strApprovingUsers==='+strApprovingUsers);
                    strApprovingUsers=strApprovingUsers.removeEnd(',');
                    objFMCase.Approving_Authorities__c=strApprovingUsers;
                    objFMCase.Approval_Status__c='Pending';
                    //objFMCase.Submit_for_Approval__c=true;
                    update objFMCase;
                }
            }
            PageReference objPage = new PageReference('/' + objFMCase.Id );
            if (objPage != NULL) {
                String unitId = objPage.getUrl().substringAfterLast('/');
                objPage = new PageReference(ApexPages.currentPage().getUrl());
                objPage.getParameters().clear();
                objPage.getParameters().put('view', 'CaseDetails');
                objPage.getParameters().put('id', unitId);
                objPage.setRedirect(true);
            }
            return objPage ;
        }
        return NULL ;
    }

    public void notifyContractorConsultantNew() {
        try{
          System.debug('-->> Inside notifyContractorConsultantNew: ');
          System.debug('-->> objFMCase.Id: ' + objFMCase.Id);
          if( objFMCase.Id == NULL  ) {
            System.debug('-->> Inside If: ');
            createMethod();
          }
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, contractorConsultantSuccessMessage));
          FM_NotifyGuestUserController objNotification = new FM_NotifyGuestUserController( new ApexPages.StandardController( objFMCase ) );
          objNotification.createSMSHistoryRecord();
          PageReference objPage = createRequestForWorkPermit();
          System.debug('-->> objPage: ' + objPage );
        }
         catch( Exception e ) {
            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, e.getMessage() ) );
        }
    }

    public override PageReference createRequestForWorkPermit() {
        if( objFMCase != NULL ) {
            System.debug('objFMCase--------'+objFMCase);
            createMethod();
            PageReference objPage = new PageReference('/' + objFMCase.Id );
            if (objPage != NULL) {
                String unitId = objPage.getUrl().substringAfterLast('/');
                objPage = new PageReference(ApexPages.currentPage().getUrl());
                objPage.getParameters().clear();
                objPage.getParameters().put('view', 'CaseDetails');
                objPage.getParameters().put('id', unitId);
                objPage.setRedirect(true);
            }
            return objPage ;
        }
        return NULL ;
    }

    public void addDocument() {
        attachCounter++;
        String attName = 'Attachment '+ attachCounter;
        lstDocuments.add(new FM_Documents__mdt(Document_Name__c=attName,
        Mandatory__c=false));
        // showDocPanel = lstDocuments.size() > 0 ? TRUE : FALSE;
    }

    public void removeDocument() {
        System.debug('===attchmentIndex======' + attchmentIndex);
        if(attchmentIndex != NULL) {
            lstDocuments.remove(attchmentIndex);
        }
    }

}