@isTest
public class CaseTempExeCallPrepCntrlTest {
  @IsTest
    static void positiveTestCase1(){
        Id userId = UserInfo.getUserId();

        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

        Account objAcc = new Account(Primary_Language__c = 'Arabic'
                            , RecordTypeId = personAccRTId
                            , FirstName='Test FirstName'
                            , LastName='Test LastName'
                            , Email__pc = 'test@t.com'
                            , Email__c = 'test@t.com'
                            , Type='Person'
                            , party_ID__C='123456'
                            , Primary_CRE__c = userId
                            , Secondary_CRE__c = userId
                            , Tertiary_CRE__c = userId);
        insert objAcc;
        
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
        OnOffCheck__c = true);

        settingLst2.add(newSetting1);
        insert settingLst2;

       
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;

        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;

        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC';
        objBookingUnit.Registration_Status_Code__c = 'ZX';
        insert objBookingUnit;
        
        Calling_List__c objCalling = new Calling_List__c();
        objCalling.RecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
        objCalling.Account_Email__c = 'test@t.com';
        objCalling.Account__c = objAcc.Id;
        objCalling.Booking_Unit__c = objBookingUnit.Id; 
        objCalling.RTP_Escl_Date__c = system.now(); 
        
        insert objCalling;
        
        Calling_List__c objCalling1 = new Calling_List__c();
        objCalling1.RecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
        objCalling1.Account_Email__c = 'test@t.com';
        objCalling1.Account__c = objAcc.Id;
        objCalling1.Booking_Unit__c = objBookingUnit.Id; 
        objCalling1.RTP_Escl_Date__c = system.now(); 
        
        insert objCalling1;
        
        /*Case objCase = new Case();
        //objCase.Case_Description__c = 'Testing Case Creation from Page' ;
        //objCase.Date_Time__c = null ;
        //objCase.CaseNumber = '123' ;
        objCase.Status = 'Closed';
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rebate On Advance').getRecordTypeId();
        objCase.AccountId = objAcc.Id;
        insert objCase;*/
        
        
        CaseTempExeCallPrepCntrl caseTempExeCallPrepObj;
        ApexPages.StandardController controller = new ApexPages.StandardController(objCalling);
        PageReference caseTempExeCallPrep = Page.CaseTempExeCallPrep;
        Test.setCurrentPage(caseTempExeCallPrep);
        ApexPages.currentPage().getParameters().put('id', objCalling.Id);
        caseTempExeCallPrepObj = new CaseTempExeCallPrepCntrl(controller);
        caseTempExeCallPrepObj.getBookingUnitDetails();
        caseTempExeCallPrepObj.getCallingListDetails();
        caseTempExeCallPrepObj.saveCallingList();
        caseTempExeCallPrepObj.getCaseDetails();
       
    }
    
    @IsTest
    static void positiveTestCase2(){
        Id userId = UserInfo.getUserId();

        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

        Account objAcc = new Account(Primary_Language__c = 'Arabic'
                            , RecordTypeId = personAccRTId
                            , FirstName='Test FirstName'
                            , LastName='Test LastName'
                            , Email__pc = 'test@t.com'
                            , Email__c = 'test@t.com'
                            , Type='Person'
                            , party_ID__C='123456'
                            , Primary_CRE__c = userId
                            , Secondary_CRE__c = userId
                            , Tertiary_CRE__c = userId);
        insert objAcc;
        
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
        OnOffCheck__c = true);

        settingLst2.add(newSetting1);
        insert settingLst2;

        Calling_List__c objCalling = new Calling_List__c();
        objCalling.RecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
        objCalling.Account_Email__c = 'test@t.com';
        objCalling.Account__c = objAcc.Id;
        
        insert objCalling;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;

        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;

        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '29898';
        objBookingUnit.Party_Id__c = '760002';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC';
        objBookingUnit.Registration_Status_Code__c = 'ZX';
        insert objBookingUnit;
        
        Booking_Unit__c objBookingUnit1 = new Booking_Unit__c();
        objBookingUnit1.Registration_ID__c  = '74123';
        objBookingUnit1.Party_Id__c = '760002';
        objBookingUnit1.Unit_Name__c  = 'JNU/ABC/2341';
        objBookingUnit1.Booking__c  = objBooking.id;
        objBookingUnit1.Mortgage__c = true;
        objBookingUnit1.Registration_Status__c = 'Agreement executed by DAMAC';
        objBookingUnit1.Registration_Status_Code__c = 'ZX';
        insert objBookingUnit1;
        
        Location__c objLoc = InitializeSRDataTest.createLocation('123','Building');
        insert objLoc;   
        
        Payment_Plan__c pp = new Payment_Plan__c();
        pp.Booking_Unit__c = objBookingUnit.id;
        pp.Effective_From__c = system.today().adddays(-4);
        pp.Effective_To__c = system.today().adddays(7); 
        pp.Building_Location__c =  objLoc.id;
        pp.Status__c = 'Active';
        insert pp;
        
        Payment_Terms__c pt = new Payment_Terms__c();
        pt.Payment_Plan__c = pp.id;
        pt.Booking_Unit__c =objBookingUnit.id;
        pt.Percent_Value__c = '5';
        pt.Modified_Percent_Value__c = '2'; 
        insert pt;
        
        
        CaseTempExeCallPrepCntrl caseTempExeCallPrepObj;
        ApexPages.StandardController controller = new ApexPages.StandardController(objCalling);
        PageReference caseTempExeCallPrep = Page.CaseTempExeCallPrep;
        Test.setCurrentPage(caseTempExeCallPrep);
        ApexPages.currentPage().getParameters().put('id', objCalling.Id);
        caseTempExeCallPrepObj = new CaseTempExeCallPrepCntrl(controller);
        CaseTempExeCallPrepCntrl.getPaymentPlan(objBookingUnit.Id);
        caseTempExeCallPrepObj.getBookingUnitDetails();
        /*Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
         String jsonStr =   '   {  '  +
         '       "responseId": "3072019-10305298",  '  +
         '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         '       "status": "S",  '  +
         '       "responseMessage": "Process Completed",  '  +
         '       "elapsedTimeMs": 8497,  '  +
         '       "responseLines": [  '  +
         '           {  '  +
         '               "documentName": "DPSOA",  '  +
         '               "language": "EN",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
         '           },  '  +
         '           {  '  +
         '               "documentName": "DPSOA_AR",  '  +
         '               "language": "AR",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
         '           }  '  +
         '       ],  '  +
         '       "complete": true  '  +
         '  }  ' ;
        
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', jsonStr));
        //caseTempExeCallPrepObj.unitRegId = objBookingUnit.Registration_ID__c;
        //caseTempExeCallPrepObj.getUnitSOAUrl();*/
    }
    
    @IsTest
    static void positiveTestCase3(){
        Profile p2 = [SELECT Id FROM Profile WHERE Name='System Administrator' Limit 1];
        //ID adminRoleId = [ Select id from userRole where name = 'Director'].id;
        //UserRole userRoleObj = new UserRole(Name = 'Director', DeveloperName = 'MyCustomRole');
        //insert userRoleObj;
        User u2 = new User(Alias = 'standt2', Email='stand2@testorg.com', isActive = true,
                           EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p2.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='adm@testorg123.com');
        insert u2;
                Id userId = UserInfo.getUserId();

        


        System.runAs(u2) {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
                    Account objAcc = new Account(Primary_Language__c = 'Arabic'
                            , RecordTypeId = personAccRTId
                            , FirstName='Test FirstName'
                            , LastName='Test LastName'
                            , Email__pc = 'test@t.com'
                            , Email__c = 'test@t.com'
                            , Type='Person'
                            , party_ID__C='123456'
                            , Primary_CRE__c = userId
                            , Secondary_CRE__c = userId
                            , Tertiary_CRE__c = userId);
        insert objAcc;
        
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
        OnOffCheck__c = true);

        settingLst2.add(newSetting1);
        insert settingLst2;
        
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;
        
        Property__c objProperty1 = new Property__c();
        objProperty1.Name = 'Test Project1';
        objProperty1.Property_Name__c = 'Test Property1';
        objProperty1.Property_ID__c = 3432;
        objProperty1.CurrencyIsoCode = 'AED';
        insert objProperty1;

        Calling_List__c objCalling = new Calling_List__c();
        objCalling.RecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
        objCalling.Account_Email__c = 'test@t.com';
        objCalling.Account__c = objAcc.Id;
        objCalling.RTP_Escl_Date__c = system.now(); 
        objCalling.Calling_List_Type__c = 'Collection Calling';
        objCalling.IsHideFromUI__c =false;
        objCalling.Property__c = objProperty.Id;
        //objCalling.Owner = u2.Id;
        
        insert objCalling;
        
        Calling_List__c objCalling1 = new Calling_List__c();
        objCalling1.RecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
        objCalling1.Account_Email__c = 'test@t.com';
        objCalling1.Account__c = objAcc.Id;
        objCalling1.RTP_Escl_Date__c = system.now();
        objCalling.IsHideFromUI__c =false;
        objCalling.Property__c = objProperty1.Id;
        
        insert objCalling1;
            CaseTempExeCallPrepCntrl caseTempExeCallPrepObj;
            ApexPages.StandardController controller = new ApexPages.StandardController(objCalling);
            PageReference caseTempExeCallPrep = Page.CaseTempExeCallPrep;
            Test.setCurrentPage(caseTempExeCallPrep);
            ApexPages.currentPage().getParameters().put('id', objCalling.Id);
            caseTempExeCallPrepObj = new CaseTempExeCallPrepCntrl(controller);
            caseTempExeCallPrepObj.selectedValue = 'Test Project,Test Project1';
            caseTempExeCallPrepObj.getOutcomeDetails();
            caseTempExeCallPrepObj.getAllUnitsRelatedToCustomer();
            caseTempExeCallPrepObj.selectedCalilingList();
            caseTempExeCallPrepObj.callingListBasedOnPropertyNames();
            caseTempExeCallPrepObj.propertyDetailsView();
            }
    }
    
    @IsTest
    static void positiveTestCase4(){
        Profile p2 = [SELECT Id FROM Profile WHERE Name='System Administrator' Limit 1];
        //ID adminRoleId = [ Select id from userRole where name = 'Director'].id;
        //UserRole userRoleObj = new UserRole(Name = 'Director', DeveloperName = 'MyCustomRole');
        //insert userRoleObj;
        User u2 = new User(Alias = 'standt2', Email='stand2@testorg.com', isActive = true,
                           EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p2.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='adm@testorg123.com');
        insert u2;
                Id userId = UserInfo.getUserId();

        


        System.runAs(u2) {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
                    Account objAcc = new Account(Primary_Language__c = 'Arabic'
                            , RecordTypeId = personAccRTId
                            , FirstName='Test FirstName'
                            , LastName='Test LastName'
                            , Email__pc = 'test@t.com'
                            , Email__c = 'test@t.com'
                            , Type='Person'
                            , party_ID__C='123456'
                            , Primary_CRE__c = userId
                            , Secondary_CRE__c = userId
                            , Tertiary_CRE__c = userId);
        insert objAcc;
        
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
        OnOffCheck__c = true);

        settingLst2.add(newSetting1);
        insert settingLst2;
        
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;
        
        Property__c objProperty1 = new Property__c();
        objProperty1.Name = 'Test Project1';
        objProperty1.Property_Name__c = 'Test Property1';
        objProperty1.Property_ID__c = 3432;
        objProperty1.CurrencyIsoCode = 'AED';
        insert objProperty1;

        Calling_List__c objCalling = new Calling_List__c();
        objCalling.RecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
        objCalling.Account_Email__c = 'test@t.com';
        objCalling.Account__c = objAcc.Id;
        objCalling.RTP_Escl_Date__c = system.now(); 
        objCalling.Calling_List_Type__c = 'Collection Calling';
        objCalling.IsHideFromUI__c =false;
        objCalling.Property__c = objProperty.Id;
        //objCalling.Owner = u2.Id;
        
        insert objCalling;
        
        Calling_List__c objCalling1 = new Calling_List__c();
        objCalling1.RecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
        objCalling1.Account_Email__c = 'test@t.com';
        objCalling1.Account__c = objAcc.Id;
        objCalling1.RTP_Escl_Date__c = system.now();
        objCalling.IsHideFromUI__c =false;
        objCalling.Property__c = objProperty1.Id;
        
        insert objCalling1;
            CaseTempExeCallPrepCntrl caseTempExeCallPrepObj;
            ApexPages.StandardController controller = new ApexPages.StandardController(objCalling);
            PageReference caseTempExeCallPrep = Page.CaseTempExeCallPrep;
            Test.setCurrentPage(caseTempExeCallPrep);
            ApexPages.currentPage().getParameters().put('id', objCalling.Id);
            caseTempExeCallPrepObj = new CaseTempExeCallPrepCntrl(controller);
            //caseTempExeCallPrepObj.selectedValue = 'Test Project,Test Project1';
            caseTempExeCallPrepObj.getOutcomeDetails();
            caseTempExeCallPrepObj.getAllUnitsRelatedToCustomer();
            caseTempExeCallPrepObj.saveCallingLists();
            
        }
    }
}