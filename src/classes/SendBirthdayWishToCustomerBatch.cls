/*-------------------------------------------------------------------------------------------------
Description: Batch to send the birthday wishes to customers 
============================================================================================================================
    Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
    1.0     | 16-03-2020      | Dipika Rajput   | Initial Draft
----------------------------------------------------------------------------------------------
  
=============================================================================================================================
*/
public class SendBirthdayWishToCustomerBatch implements Database.Batchable<sObject>,Database.AllowsCallouts{

public Database.QueryLocator start(Database.BatchableContext BC) {

    // collect the batches of account records or objects to be passed to execute
        Date tody = system.today();
        String query = 'SELECT Id, Name,Date_Of_Birth__c,Date_of_Birth__pc, Person_Business_Email__c FROM Account WHERE'
                      +' '+'('+'DAY_IN_MONTH(Date_Of_Birth__c) = ' + 
                      tody.day() +  ' OR DAY_IN_MONTH(Date_of_Birth__pc) = ' + tody.day() +')'
                      +' '+'AND'+' '+'('+'CALENDAR_MONTH(Date_Of_Birth__c) = ' + 
                      tody.month() +  ' OR CALENDAR_MONTH(Date_of_Birth__pc) = ' + tody.month() +')'+' '+
                      +'AND'+'('+'Date_Of_Birth__c != NULL'+' '+
                      +'OR'+' '+'Date_of_Birth__pc != NULL'+')'+' '+
                      +'AND Person_Business_Email__c != NULL'+' '+
                      +'AND Active_Customer__c = \'Active\' ';
                    
                      /* system.debug('query:::'+query);
                      List<sObject> sobjList = Database.query(query);
                      system.debug('sobjList:::'+sobjList); */
    
        return Database.getQueryLocator(query);
}

public void execute(Database.BatchableContext BC, List<Account> accList) {
    System.debug('accList---'+accList);
    String toAddress = '',strCCAddress = '', fromAddress = '',replyToAddress = '', contentType, contentValue,contentBody = '';String subject, strAccountId, strDPIId, bccAddress ='';
   

   // list<string> sendEmailList = new List<string>();
   // list<Account> accountList = new List<Account>();
    list<EmailMessage> lstEmailMsg = new List<EmailMessage>();
    List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();

   /* if(accList != NULL && !sendEmailList.isEmpty()){
        for(Account accObj : (List<Account>)accList){

            // Check if Email address is present for all the accounts
            //if(!sendEmailList.contains(accObj.Person_Business_Email__c)){
                sendEmailList.add(accObj.Person_Business_Email__c);
                accountList.add(accObj);
           // }
        }
    }   */    
    if(accList.size()>0){
        List<OrgWideEmailAddress> orgWideAddressHelloDamac = [SELECT  Id
                                                                    ,Address
                                                                    ,DisplayName
                                                               FROM  OrgWideEmailAddress
                                                               WHERE DisplayName = :Label.DAMAC_no_replysf_damacgroup_com ];
        // String toAddress ='dipika.rajput@eternussolutions.com';

        List<EmailTemplate> emailTemplateObj = [SELECT 
                                                ID, 
                                                Subject, 
                                                Body, 
                                                name,
                                                HtmlValue, 
                                                TemplateType,
                                                BrandTemplateId
                                            FROM  EmailTemplate 
                                            WHERE name =: Label.BirthdayWishTemplateForCustomer limit 1];
       
       // System.debug('emailTemplateObj = ' + emailTemplateObj);    
        Subject = emailTemplateObj[0].subject != null ? emailTemplateObj[0].subject : 'Birthday wish email';
        System.debug('accList[0].Person_Business_Email__c:::'+accList[0].Person_Business_Email__c);
        toAddress = String.valueOf(accList[0].Person_Business_Email__c);
        fromAddress = Label.AT_Your_Service;
        contentType = 'text/html';
        if(emailTemplateObj[0].body != NULL){
            contentBody = emailTemplateObj[0].body;
        }
        if(emailTemplateObj[0].htmlValue != NULL){
            contentValue = emailTemplateObj[0].htmlValue;
        } 
        //System.debug('contentValue:'+contentValue);
         //System.debug('contentBody:'+contentBody);
        if(accList[0].Name != NULL){
            contentValue = contentValue.replace('{!CustomerName}', GenericUtility.getCamelCase(accList[0].Name));
            contentBody = contentBody.replace('{!CustomerName}', GenericUtility.getCamelCase(accList[0].Name));
        }
        SendGridEmailService.SendGridResponse objSendGridResponse =   
                        SendGridEmailService.sendEmailService(
                            toAddress,
                            '', 
                            strCCAddress, 
                            '', 
                            bccAddress, 
                            '',
                            subject,
                            '',
                            fromAddress,
                            'DAMAC', 
                            orgWideAddressHelloDamac[0].Address, 
                            '', 
                            contentType, 
                            contentValue, 
                            '',
                            new List<Attachment>{}
                        );
                                                    
                        String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;                            
                        if(responseStatus == 'Accepted') {
                            strAccountId='';
                            strAccountId=accList[0].id;
                           System.debug('SendGrid response is accepted');
                            EmailMessage mail = new EmailMessage();
                            mail.Subject = subject;
                            mail.MessageDate = System.Today();
                            mail.Status = '3';//'Sent';
                            mail.RelatedToId = strAccountId;
                            mail.Account__c  = strAccountId;
                            mail.Type__c = 'FM Invoice';
                            mail.ToAddress = toAddress;
                            mail.FromAddress = fromAddress;
                            mail.TextBody = contentBody; //contentValue.replaceAll('\\<.*?\\>', '');
                            mail.Sent_By_Sendgrid__c = true;
                            mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                            mail.DP_Invoices__c = strDPIId;
                            mail.CcAddress = strCCAddress;
                            mail.BccAddress = bccAddress;

                           lstEmailMsg.add(mail);

                        } else {
                            Error_Log__c objError = new Error_Log__c();
                            objError.Account__c = accList[0].id;
                            objError.Error_Details__c = 'Unable to send Email to customer ';
                            objError.Process_Name__c = 'Happy Birthday';
                            lstErrorLog.add(objError);
                        }
    }
    System.debug('lstEmailMsg: ' + lstEmailMsg);
    if(lstEmailMsg.size() > 0) {
        if(!Test.isRunningTest()) {
            insert lstEmailMsg;
        }
    }

    System.debug('lstErrorLog: ' + lstErrorLog);
    if(lstErrorLog.size() > 0) {
        insert lstErrorLog;
    }
} 

public void finish(Database.BatchableContext BC) {
// execute any post-processing operations
}
}