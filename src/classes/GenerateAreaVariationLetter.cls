public without sharing class GenerateAreaVariationLetter{
    public String LODurl;
    Public String strPageID;
    case objCase;
    public GenerateAreaVariationLetter(ApexPages.standardController controller){
       
    }//End constructor
    
    public pagereference init(){
        strPageID = ApexPages.currentPage().getParameters().get('id');
        objCase = [select Id, CaseNumber, AccountId, Booking_Unit__c
                       , Booking_Unit__r.Registration_ID__c, Recordtype.DeveloperName
                       , Total_Major_Snags__c, Total_Snags_Open__c , Total_Snags_Closed__c
                       , Document_Verified__c, ParentId, Parent.CaseNumber, Parent.Document_Verified__c
                       from case where id =:strPageID];
        system.debug('objCase**********'+objCase);
        CallHandoverMQServices.HandoverMQServicesResponse objLOD;
        objLOD= CallHandoverMQServices.CallHandoverMQServiceLetterofDischarge(objCase.Booking_Unit__r.Registration_ID__c);
        //objLOD= CallHandoverMQServices.CallHandoverMQServiceLoD(objCase.Booking_Unit__r.Registration_ID__c);
        return processData(objLOD);
    }//End init
    
    public pagereference processData(CallHandoverMQServices.HandoverMQServicesResponse objLOD){
        LODurl = objLOD.url;               
        Id attachmentId = fetchAreaVariationDocument();
        list<SR_Attachments__c> lstCaseAttachment = new list<SR_Attachments__c>();
        list<Error_Log__c> lstErrorLog = new list<Error_Log__c>();
        system.debug('LODurl**********'+LODurl);
        if (LODurl == null) {
            
            system.debug('Starting Error Creation******');
            Error_Log__c objErr = new Error_Log__c();
            objErr.Account__c = objCase.AccountId;
            objErr.Booking_Unit__c = objCase.Booking_Unit__c;
            objErr.Case__c = objCase.id;
            objErr.Error_Details__c = objLOD.P_PROC_MESSAGE;
            if(objCase.RecordType.DeveloperName.equalsIgnoreCase('Early_Handover')){
                objErr.Process_Name__c = 'Early Handover';
            }else if(objCase.RecordType.DeveloperName.equalsIgnoreCase('Handover')){
                objErr.Process_Name__c = 'Handover';
            }
            lstErrorLog.add(objErr);
            system.debug('objLOD.P_PROC_MESSAGE******'+objLOD.P_PROC_MESSAGE);
            String strMsg = (objLOD.P_PROC_MESSAGE != null && objLOD.P_PROC_MESSAGE.contains('No change in Price, Area Variation Letter is not Required'))? 'No change in Price, Area Variation Letter is not Required' : 'Error : Area Variation could not be generated. Please try again';
            if (attachmentId != null) {
                Database.delete(attachmentId, true);
            }
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, strMsg);
            ApexPages.addMessage(myMsg);
            return null;
        } else {
             SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
             objCaseAttachment.Case__c = objCase.id;
             objCaseAttachment.Id = attachmentId;
             objCaseAttachment.Name = 'Area Variation Addendum '+ +system.now();
             objCaseAttachment.Attachment_URL__c = LODurl;
             objCaseAttachment.Booking_Unit__c = objCase.Booking_Unit__c;
             lstCaseAttachment.add(objCaseAttachment);
             /*
             SR_Attachments__c objCaseAttachment1 = new SR_Attachments__c();
             objCaseAttachment1.Case__c = objCase.id;
             objCaseAttachment1.Name = 'Signed Area Variation Addendum'+ +system.now();
             objCaseAttachment1.Booking_Unit__c = objCase.Booking_Unit__c;
             lstCaseAttachment.add(objCaseAttachment1);
             */
        }
        system.debug('lstCaseAttachment '+lstCaseAttachment );
        if (lstCaseAttachment != null && lstCaseAttachment.size()>0){
            upsert lstCaseAttachment;
        }
        if (lstErrorLog != null && lstErrorLog.size()>0){
            insert lstErrorLog;
        }
        pagereference newpg = new Pagereference(URL.getSalesforceBaseUrl().toExternalForm()+'/'+strPageID);
        newpg.setRedirect(true);
        return newpg;
    }
    
    public Id fetchAreaVariationDocument(){
        list<SR_Attachments__c> lstSR = [Select Id, Name, Case__c 
                                        from SR_Attachments__c 
                                        where Case__c =:strPageID
                                        and Name Like 'Area Variation Addendum%' limit 1];
        if(lstSR != null && !lstSR.isEmpty()){
            return lstSR[0].Id;
        }else{
            return null;
        }
    }
}