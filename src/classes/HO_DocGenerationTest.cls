/******************************************************************************
* Description - Test class developed for HO_DocGeneration
*
* Version            Date            Author                    Description
* 1.0                04/02/2018           						Initial Draft
********************************************************************************/
@isTest
private class HO_DocGenerationTest {
	 static HO_DocGeneration.HO_DocResponse obj = new HO_DocGeneration.HO_DocResponse();
   	Static String resp ;
	  @isTest static void testMethod1(){
	       Test.startTest();
	        SOAPCalloutServiceMock.returnToMe = new Map<String, HODocuments1.HandOverPackResponse_element>();
	        HODocuments1.HandOverPackResponse_element response1 = new  HODocuments1.HandOverPackResponse_element();
	        response1.return_x ='{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":null,"ATTRIBUTE1":'+
	        '"https://sftest.deeprootsurface.com/docs/e/AR_HO_PACK.pdf","PARAM_ID":"76726"}],'+
	        '"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}'; 
	        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
	        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
	        obj = HO_DocGeneration.GetHOPack('reqName','Registrationid');
	     // System.assert(resp != null);  
	     Test.stopTest();
	  }
	   @isTest static void testMethod2(){
	       Test.startTest();
	        SOAPCalloutServiceMock.returnToMe = new Map<String, HODocuments1.HandOverPackResponse_element>();
	        HODocuments1.HandOverPackResponse_element response1 = new  HODocuments1.HandOverPackResponse_element();
	        response1.return_x ='{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"File (Type,Size,Name) '+
	        ': PDF-13700-XDCINVSTMT_VAT_42834745_1.PDF","ATTRIBUTE1":"http://dxbhoebtstap.damacholding.'+
	        'home:8033/temp/42834745_INV.pdf","PARAM_ID":"76726"}],"message":"Process Completed Returning 1'+
	        ' Response Message(s)...","status":"S"}'; 
	        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
	        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
	        obj = HO_DocGeneration.GetHOInvoice('reqName','Registrationid','1/2/2018','12/12/2018');
	     // System.assert(resp != null);  
	     Test.stopTest();
	  }
}