public without sharing class CustomerCommunityUtils {

    private static final Integer SURVEY_CHECK_PERIOD_IN_DAYS = 3 * 30; // 90 days OR 3 months
    private static final Set<String> USER_RELATED_PARENT_FIELDS = new Set<String>{
        'Contact.AccountId', 'Account.Party_ID__c', 'Contact.Account.Party_ID__c',
        'Contact.Account.IsPersonAccount', 'Contact.Account.Survey_skipped_date__c',
        'Contact.Account.Passport_Expiry_Date__c', 'Contact.Account.Passport_Expiry_Date__pc'
    };

    public static Id customerAccountId;
    public static Id customerContactId;
    public static Boolean needsToTakeSurvey;

    public static User user;

    private static Boolean isPortalEnabled;

    private static Integer surveyCheckPeriod;
    private static Boolean hasNotTakenSurveyRecently;
    private static Boolean hasNotSkippedSurveyRecently;
    private static Boolean isCreatedRecently;

    static {
        Map<String, SObjectField> mapUserFields = Schema.SObjectType.User.fields.getMap();
        isPortalEnabled = mapUserFields.keySet().contains('isportalenabled');

        String userFields = '';
        for (SobjectField fieldToken : mapUserFields.values()) {
            Schema.DescribeFieldResult field = fieldToken.getDescribe();
            if (field.isAccessible()) {
                userFields += field.getName() + ',';
            }
        }
        for (String relatedParentField : USER_RELATED_PARENT_FIELDS) {
            userFields += relatedParentField + ',';
        }
        userFields = userFields.removeEndIgnoreCase(',');

        user = Database.query(' SELECT ' + userFields +' FROM User WHERE Id = \'' + UserInfo.getUserId() + '\'');

        customerAccountId = getCustomerAccountId();
        customerContactId = getCustomerContactId();

        surveyCheckPeriod = fetchSurveyCheckPeriod();

        hasNotTakenSurveyRecently = hasUserNotTakenSurveyRecently();
        hasNotSkippedSurveyRecently = hasUserNotSkippedSurveyRecently();
        isCreatedRecently = isUserCreatedRecently();

        needsToTakeSurvey = doesUserNeedToTakeSurvey();
    }

    public static Boolean isCurrentView(String view) {
        String currentView = ApexPages.currentPage().getParameters().get('view');
        if (String.isBlank(currentView)) {
            currentView = '';
        }
        return view.equalsIgnoreCase(currentView);
    }

    public static Boolean isPortalUser() {
        return isPortalEnabled
            && user != NULL
            && user.IsPortalEnabled;
    }

    public static Boolean isPortalUser(Id userId) {
        List<User> lstUser = new List<User>();
        if (isPortalEnabled) {
            lstUser = [ SELECT  Id, IsPortalEnabled
                        FROM    User
                        WHERE   Id = :userId];
        }
        return !lstUser.isEmpty() && lstUser[0].IsPortalEnabled;
    }

    public static Id getCustomerAccountId() {
        return (
            ((user == NULL || user.ContactId == NULL))
            ? ('Guest'.equalsIgnoreCase(UserInfo.getUserType()) || Test.isRunningTest()
                ? NULL : ('NULL'.equalsIgnoreCase(Label.Portal_User_debug_for_Admin) ? NULL : Label.Portal_User_debug_for_Admin)
            )
            : user.Contact.AccountId
        );
    }

    public static Id getCustomerContactId() {
        return user == NULL ? NULL : user.ContactId;
    }

    public static String getPartyId() {
        String partyId;
        partyId = (user == NULL
            || user.ContactId == NULL
            || user.Contact.AccountId == NULL
        ) ? NULL
        : user.Contact.Account.Party_ID__c;
        return partyId;
    }

    public static Date getPassportExpiryDate() {
        return (user == NULL || user.ContactId == NULL || user.Contact.AccountId == NULL)
            ? NULL : ((user.Contact.Account.IsPersonAccount) ? user.Contact.Account.Passport_Expiry_Date__pc : user.Contact.Account.Passport_Expiry_Date__c);
    }

    public static Boolean doesUserNeedToTakeSurvey() {
        return hasNotTakenSurveyRecently && hasNotSkippedSurveyRecently && isCreatedRecently;
    }

    private static Integer fetchSurveyCheckPeriod() {
        CustomerCommunitySettings__c customerCommunitySettings = CustomerCommunitySettings__c.getInstance();
        return (customerCommunitySettings != NULL
                && customerCommunitySettings.LastSurveyCheckPeriodInDays__c != NULL)
                    ? Integer.valueOf(customerCommunitySettings.LastSurveyCheckPeriodInDays__c)
                    : SURVEY_CHECK_PERIOD_IN_DAYS;
    }

    private static Boolean hasUserNotTakenSurveyRecently() {
        return [   SELECT  Id
                    FROM    Survey_Taken_CRM__c
                    WHERE   Customer_taking_Survey__c = :customerAccountId
                        AND CreatedDate
                                >
                            :(System.today() - surveyCheckPeriod)
                    LIMIT   1].isEmpty();
    }

    private static Boolean hasUserNotSkippedSurveyRecently() {
        return user.Contact.Account.Survey_skipped_Date__c <= (Date.today() - surveyCheckPeriod);
    }

    private static Boolean isUserCreatedRecently() {
        return user.CreatedDate.date().daysBetween(Date.today()) > surveyCheckPeriod;
    }

    public static String getFullPhotoUrl() {
        return user == NULL ? NULL : user.FullPhotoUrl;
    }

    /*
        Function(s) below this can and should be moved to a separate CommunityUtils class
     */

    /**
     * converts decimal to word format
     * @param  amount Decimal amount
     * @return        amount in short words
     */
    public static String getAmountInShortWords(Decimal amount) {
        return getAmountInShortWords((Double)amount);
    }

    /**
     * converts double to word format
     * @param  amount Double amount
     * @return        amount in short words
     */
    public static String getAmountInShortWords(Double amount) {
        String strAmount = '0';
        List<String> lstDenomination = new List<String> {
            '', 'K', 'Million', 'Billion', 'Trillion', 'Quadrillion', 'Quintillion', 'Sixtillion', 'Septillion'
        };
        Integer denominationIndex = 0;
        Double remainder = amount;
        while(amount >= 1) {
            if (amount <= 1000) {
                strAmount = '' + (((Decimal)amount).setScale(2));
                if (strAmount.contains('.')) {
                    while (strAmount.contains('.') && strAmount.endsWith('0')) {
                        strAmount = strAmount.removeEnd('0');
                        strAmount = strAmount.removeEnd('.');
                    }
                }
                if (strAmount.endsWith('.00')) {
                    strAmount = strAmount.removeEnd('.00');
                } else if (strAmount.endsWith('.0')) {
                    strAmount = strAmount.removeEnd('.0');
                }
                strAmount += ' ' + lstDenomination[denominationIndex];
            }
            denominationIndex++;
            amount /= 1000;
            remainder = Math.mod((Integer) amount, 1000);
        }
        return strAmount;
    }

    /**
     * parses the IPMS Date strings
     * @param  dateString Date String in IPMS format
     * @return            Parsed Date
     */
    public static Date parseIpmsDateString(String dateString) {
        if (String.isBlank(dateString) || dateString.countMatches('-') != 2) {
            return NULL;
        }
        List<String> lstDateParts = dateString.split('-');

        Map<String, Integer> mapMonthToNumber = new Map<String, Integer> {
            'JAN' => 1, 'FEB' => 2, 'MAR' => 3, 'APR' => 4, 'MAY' => 5, 'JUN' => 6,
            'JUL' => 7, 'AUG' => 8, 'SEP' => 9, 'OCT' => 10, 'NOV' => 11, 'DEC' => 12
        };

        try {
            Integer day     = Integer.valueOf(lstDateParts[0]);
            Integer month   = Integer.valueOf(mapMonthToNumber.get(lstDateParts[1]));
            Integer year    = Integer.valueOf(lstDateParts[2]);
            return Date.newInstance(year, month, day);
        } catch(TypeException typeExcp) {
            return NULL;
        }

    }
}