/**************************************************************************************************
* Name               : UpdateContactDetailsController
* Test Class         : UpdateContactDetailsController_Test
* Created By         : DAMAC IT
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR               DATE             Comments
* 1.0                                               Created
**************************************************************************************************/
public class UpdateContactDetailsController {
    public Buyer__c con{get; set;}
    public string email{get; set;}
    public string phone{get; set;}
    public UpdateContactDetailsController() {
        con = new Buyer__c();
    }

    public void saveDetails(){
        try{
            string recordId = Apexpages.currentPage().getParameters().get('id');
            Buyer__c buy = new Buyer__c(id = recordId);
            if(email != null && email != '')
                buy.Email__c = email;
            if(phone != null && phone != '')
                buy.Phone__c = phone;
            if(con.Phone_Country_Code__c != '')
                buy.Phone_Country_Code__c = con.Phone_Country_Code__c;
            update buy;
            con = new Buyer__c();
            email = '';
            phone = ''; 
            Apexpages.addMessage(new Apexpages.message(ApexPages.Severity.INFO, 'Record updated successfully.'));
        }
        catch(Exception e){
            Apexpages.addMessage(new Apexpages.message(ApexPages.Severity.fatal, e.getMessage()+'-'+e.getLineNumber()));
        }
    }
}