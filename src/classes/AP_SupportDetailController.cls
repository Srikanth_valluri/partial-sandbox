/*
    Functionality : To create a new case/To display the cases 
    Created by logged in user/To display the case details along with the case comments
*/

global class AP_SupportDetailController {

    public Map<id,user> userWithPhotoUrl                                                {get; set;}
    public List <String> supportSubjectList                                             {get; set;}
    public List <CaseComment> caseComments                                              {get; set;}
    public List <Case> existingCases                                                    {get; set;}
    public String errMessage                                                            {get; set;}
    public String themeType                                                             {get; set;}
    public String caseSub                                                               {get; set;}
    public Boolean isError                                                              {get; set;}
    public CaseComment newCaseComment                                                   {get; set;}
    public Case newCase                                                                 {get; set;}
    public Map <String, String> parametersMap = new Map <String, String> ();
    

    public AP_SupportDetailController () {
        //userWithPhotoUrl = new Map<id,user>();
        newCase = new Case ();
        caseSub = '';
        newCaseComment = new CaseComment ();
        caseComments = new List <CaseComment> ();
        existingCases = new List <Case> ();
        isError = false;
        errMessage = '';
        themeType ='slds-theme_info';
        supportSubjectList = new List <String> ();
        Set <id> userIdSet = new Set <id>();
        parametersMap = Apexpages.currentpage().getparameters();
        system.debug('parametersMap>>>>>  ' + parametersMap);
        // To display the case detail and comment
        if (parametersMap.containsKey ('id')) {
            ID caseId = parametersMap.get ('id');
            newCase = Database.Query (
                    + ' SELECT '
                    + getAllFields ('Case')
                    + ' FROM Case WHERE ID =: caseId'
            );
            system.debug('newCase>>>>>  ' + newCase);
            system.debug('caseId>>>>>  ' + caseId);
            caseComments = [ SELECT id,CreatedDate, CreatedbyId,CommentBody 
                               FROM CaseComment WHERE parentId = :caseId 
                           ORDER BY CreatedDate Desc ];
            system.debug('caseComments>>>>>  ' + caseComments);
            for(CaseComment commentsObj : caseComments){
                userIdSet.add(commentsObj.CreatedbyId);
            }
             userWithPhotoUrl = new Map<id,user>([SELECT FullPhotoUrl,
                                FirstName,LastName,Name,
                                ContactId,
                                Account.OwnerId,
                                Account.Secondary_Owner__c,
                                Account.Owner.Name,
                                Account.Secondary_Owner__r.Name
                           FROM User
                          WHERE Id IN :userIdSet]);
        System.debug('userWithPhotoUrl>>>>> --' + userWithPhotoUrl);
        /*for(CaseComment commentsObj : caseComments){
            userIdSet.add(commentsObj.CreatedbyId);
        }*/
        } else {
            // To get the subject values from the custom settings
            for (Case_Subject_Values__c val :[SELECT Name FROM Case_Subject_Values__c WHERE Name != NULL])
                supportSubjectList.add (val.name);
        }
    }
    
    /**
     *  Utility method to get all fields realted to the object name provided
     */
    public static string getAllFields (String objectName) {
        String fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        if (objectType == null)  {
            return fields;
        } 
        Map <String, Schema.SObjectField> fieldMap = objectType.getDescribe ().fields.getMap ();
        for (Schema.SObjectField sfield : fieldMap.Values()) {            
            fields += sfield.getDescribe().getName ()+ ', ';            
        }
        return fields.removeEnd(', '); 
    }
    
    /**
     *  To get the cases Created by logged in user 
     */
    public void DisplayMyCases () {
        ID userId = UserInfo.getUserID ();
        existingCases = Database.Query (
                      + 'SELECT '
                      + getAllFields ('Case')
                      + ' FROM Case WHERE CreatedbyId = :userId '
                      + ' Order By CreatedDate Desc LIMIT 200 '
        );
    }

    /** 
     * To create the new case from the portal and 
     * assigning it to Agent Case queue and agent case record type.
     */
    public void createNewCase () {
        isError = false;
        errMessage = '';
        themeType ='slds-theme_info';
        try {
            //newCase.Case_Type__c = 'Portal';
            System.debug('newCase>>>>> --' + newCase);
            System.debug('caseSub>>>>> --' + caseSub);
            System.debug('newCase.Subject>>>>> --' + newCase.Subject);
            if (String.isBlank(newCase.Subject) && String.isNotBlank(caseSub)) {
                newCase.Subject = caseSub;
            }
            newCase.Origin = 'Agent Portal';
            newCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Agent Case').getRecordTypeId();
            newCase.OwnerId = [SELECT Id FROM Group WHERE Type = 'Queue' AND NAME = 'Agent Cases'].Id;
            try {
                User u = new User ();
                u = [SELECT ContactId, Contact.AccountId FROM User WHERE Id =: userInfo.getUserId () ];
                //newCase.AccountId = u.Contact.AccountId;
                newCase.ContactId = u.ContactId;
                newCase.Agency__c = u.Contact.AccountId;
            } catch (Exception e) {
            }
            insert newCase;
            errMessage = 'Case created successfully.';
            themeType = 'slds-theme_success';
        } catch (Exception e) {
            //errMessage = e.getMessage ();
            //isError = true;
            //themeType = 'slds-theme_error';
        }
    }

    //To create new Case Comment
    public void saveCaseComment () {
        isError = false;
        errMessage = '';
        themeType ='slds-theme_info';
        try {
            newCaseComment.ParentId = newCase.Id;
            insert newCaseComment;
            newCaseComment = new CaseComment ();
            errMessage = 'Case comment added successfully.';
            themeType = 'slds-theme_success';
        } catch (Exception e) {
            //errMessage = e.getMessage ();
            //isError = true;
            //themeType = 'slds-theme_error';
        }
    }

    @RemoteAction
    global static String getContentDocURL (Id currentVersionId) {
        ContentDistribution newDistribution = new ContentDistribution(
            ContentVersionId = currentVersionId, 
            Name = 'External Link',
            PreferencesNotifyOnVisit = false
        );
        insert newDistribution;
        //return null;
        
        ContentDistribution distributionURL = [SELECT Id, DistributionPublicUrl, 
                                                      ContentDownloadUrl, ContentDocumentId
                                                 FROM ContentDistribution 
                                                WHERE Id =: newDistribution.ID
                                             ORDER BY LastModifiedDate DESC];
        System.Debug (distributionURL.ContentDownloadUrl);
        return distributionURL.ContentDownloadUrl;
    }
}