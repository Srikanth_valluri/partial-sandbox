@RestResource(urlMapping='/SendAppointmentsServiceTypesToMobileApp/*')
 Global class SendAppointmentsServiceTypesToMobileApp
 {
 
 public static final String HANDOVER_EARLY = '02-Handover - Early';
    public static final String HANDOVER_NORMAL = '01-Handover';
    public static final String PROPERTY_RESALE = '02-Property Resale';
    public static final String BILLING_MATTERS = '03-Statement/Billing Matters';
    public static final String PROJECT_UPDATES = '04-Project Updates';
    public static final String COCD = ' 05-Change of Details';
    public static final String ADDITIONAL_PARKING = '06-Additional Parking';
    public static final String REJECTED_UNITS = '07-Rejected Units';
    public static final String RENTAL_POOL = '08-Rental Pool';
    public static final String TITLE_DEED = '09-Title Deed';
    public static final String FUND_TRANSFER = '10-Fund Transfer';
    public static final String CHEQUE_COLLECTION = '12-Cheque Collection';
    public static final String PAYMENTS = '13-Payments';
    public static final String ENQUIRIES = 'Enquiries';
    public static final String MORTGAGE = 'Mortgage';
    public static final String VISA_NOC ='11-Visa NOC';
     @HtTPGet
    Global static list<String> SendAppointmentsServiceTypesToMobile()
    {
    
       List<Active_Process_on_Portal_Meta__mdt> lstActiveProcess = new List<Active_Process_on_Portal_Meta__mdt>();
        lstActiveProcess = [Select id,MasterLabel,DeveloperName from Active_Process_on_Portal_Meta__mdt];
        List<String>lstSRs = new List<String>();
        lstSRs.add('--None--');
        for(Active_Process_on_Portal_Meta__mdt obj : lstActiveProcess){
            if(obj.MasterLabel == BILLING_MATTERS || obj.MasterLabel == RENTAL_POOL
                || obj.MasterLabel == PROPERTY_RESALE || obj.MasterLabel == HANDOVER_NORMAL
                || obj.MasterLabel == COCD || obj.MasterLabel == VISA_NOC
                || obj.MasterLabel == ADDITIONAL_PARKING || obj.MasterLabel == PROJECT_UPDATES 
                || obj.MasterLabel == REJECTED_UNITS || obj.MasterLabel == TITLE_DEED
                || obj.MasterLabel ==FUND_TRANSFER){
                lstSRs.add(obj.MasterLabel);
            }else{
                lstSRs.add(obj.MasterLabel);
            }
        }
        
        return lstSRs;
    }
}