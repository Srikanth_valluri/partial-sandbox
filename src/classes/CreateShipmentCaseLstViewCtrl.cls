public class CreateShipmentCaseLstViewCtrl{
    
    public String selectedCS{get;set;}
    public List<SelectOption> couServices {get;set;}
    ApexPages.StandardSetController setCon;
    public Map<Id,List<Case>> accCaseMap{get;set;}
    public Map<Id,List<Case>> accCaseMapWithSC{get;set;}
    public List<Id> accIdsWithSC{get;set;}
    public List<Id> accIds{get;set;}
    public Id jobId{get;set;}
    
    public CreateShipmentCaseLstViewCtrl(ApexPages.StandardSetController controller)
    {
        selectedCS = '';
        //jobId = new id();;
        couServices = new List<SelectOption>();
        couServices.add(new SelectOption('First Flight', 'First Flight'));
        couServices.add(new SelectOption('Aramex', 'Aramex Regular Account'));
        couServices.add(new SelectOption('Aramex CDS', 'Aramex CDS'));
        setCon = controller;
        accCaseMap = new Map<Id,List<Case>>();
        accIds = new List<Id>();
        accCaseMapWithSC = new Map<Id,List<Case>>();
        accIdsWithSC = new List<Id>();
        List<Case> caseIds = new List<Case>();
        caseIds = (Case[])setCon.getSelected();
        List<Case> caseList = new List<Case>([SELECT Id,CaseNumber,Booking_Unit__c,Booking_Unit__r.Unit_Name__c,AccountId,RecordTypeName__c,Account.Name,Courier_Request_Already_Created__c  FROM Case WHERE Id IN :caseIds]);
        System.debug('Case List==> ' +caseList);
        for(Case caseInst : caseList) {
            if(caseInst.Courier_Request_Already_Created__c == false) {
                if(accCaseMap.containsKey(caseInst.AccountId)) {
                    List<Case> cases = new List<Case>();
                    cases = accCaseMap.get(caseInst.AccountId);
                    cases.add(caseInst);
                    accCaseMap.put(caseInst.AccountId,cases);
                
                } else {
                    List<Case> cases = new List<Case>();
                    cases.add(caseInst);
                    accCaseMap.put(caseInst.AccountId,cases);
                    accIds.add(caseInst.AccountId);
                }
            } else {
                if(accCaseMapWithSC.containsKey(caseInst.AccountId)) {
                    List<Case> cases = new List<Case>();
                    cases = accCaseMapWithSC.get(caseInst.AccountId);
                    cases.add(caseInst);
                    accCaseMapWithSC.put(caseInst.AccountId,cases);
                
                } else {
                    List<Case> cases = new List<Case>();
                    cases.add(caseInst);
                    accCaseMapWithSC.put(caseInst.AccountId,cases);
                    accIdsWithSC.add(caseInst.AccountId);
                }
            }
            system.debug('accCaseMap-->'+accCaseMap);
            system.debug('accIds-->'+accIds);
            system.debug('accCaseMapWithSC-->'+accCaseMapWithSC);
            system.debug('accIdsWithSC-->'+accIdsWithSC);
        }
        
    }

    public void createRequest()
    {
        CreateShipmentRequestBatch batchInst = new CreateShipmentRequestBatch(accCaseMap,selectedCS);
        jobId = Database.executeBatch(batchInst);
        
    }
}