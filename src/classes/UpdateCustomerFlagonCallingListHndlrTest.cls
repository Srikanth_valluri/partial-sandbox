@isTest
private class UpdateCustomerFlagonCallingListHndlrTest {
    
    public static Id welcomeCallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Welcome Calling List').RecordTypeId;
    public static  List<Calling_List__c>lstCallingLists;
    
    public static  Id RecordTypeIdCollection = [
                                        SELECT Id
                                        FROM RecordType
                                        WHERE SObjectType='Calling_List__c' 
                                        AND DeveloperName='Collections_Calling_List'
                                        AND IsActive = TRUE LIMIT 1
                                     ].Id;
    public static Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
    
    static testMethod void test_UpdateCustomerFlagonCallingListHandler () {
         UpdateCustomerFlagonCallingListBatch batchCls = new UpdateCustomerFlagonCallingListBatch ();
        
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
          
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',OnOffCheck__c = true));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForCollectionCL',OnOffCheck__c = true));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForFMCL',OnOffCheck__c = true));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForHOCL',OnOffCheck__c = true));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForEHOCL',OnOffCheck__c = true));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForEliteCL',OnOffCheck__c = true));
        
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        List<Calling_List__c> lstCallingLists = createCallingList( RecordTypeIdCollection , 5 ,objAcc.Id ,false);
        insert lstCallingLists;
       
        Test.startTest();
            Database.executeBatch(batchCls);
        Test.stopTest();
    }

    /*
     @ Description : To create calling list reocord
     @ Return      : list of calling list to be created
    */
    public static List<Calling_List__c> createCallingList( Id RecordTypeIdCollection, Integer counter , Id acctId,Boolean flag ) {
        List<Calling_List__c> lstCallingLists = new List<Calling_List__c>();
        for( Integer i=0; i<counter; i++ ) {
            lstCallingLists.add(new Calling_List__c(IsHideFromUI__c = false, Account__c = acctId  , Customer_Flag__c  = flag ,RecordTypeId = RecordTypeIdCollection ) );
            //lstCallingLists.add(new Calling_List__c(IsHideFromUI__c = false, Account__c = acctId  , Customer_Flag__c  = true ,RecordTypeId = RecordTypeIdCollection ) );
        
        }
        return lstCallingLists;
    }
    
    public static testMethod void updateMetadataOnSubordinateCallingsTest(){

        Map<String,ID> profiles = new Map<String,ID>();

        List<Profile> ps = [select id, name from Profile where name = 
         'Standard User'];

        for(Profile p : ps){
         profiles.put(p.name, p.id);
        }

        // Create the users to be used in this test.  

        // First make a new user.  


        User standard = new User(alias = 'standt', 
        email='standarduser@testorg.com', 
        emailencodingkey='UTF-8', 
        lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', 
        profileid = profiles.get('Standard User'), 
        timezonesidkey='America/Los_Angeles', 
        username='standarduser@testorg00231.com');

        insert standard;
      
        system.runas(standard){
            UpdateCustomerFlagonCallingListHandler.strRun_Subordinate_Logic = 'Y';
            List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            
            settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',OnOffCheck__c = true));
            settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForCollectionCL',OnOffCheck__c = true));
            settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForFMCL',OnOffCheck__c = true));
            
            insert settingLst2;
            
             Account objAcc = new Account(RecordTypeId = personAccRTId, 
                                         FirstName='Test FirstName1', 
                                         LastName='Test LastName2', 
                                         Type='Person', 
                                         party_ID__C='12345'
                                         );
            insert objAcc ;
            List<Calling_List__c>callingLst = new List<Calling_List__c>();
            Calling_List__c calling1 = new Calling_List__c(RecordTypeId = RecordTypeIdCollection,
                                                     Call_Outcome__c = 'Recovery list',
                                                     Account__c = objAcc.Id,
                                                     Customer_Flag__c = true,
                                                     Registration_ID__c = '122851',
                                                     Party_ID__c = '1177915');
            callingLst.add(calling1);
            Calling_List__c calling2 = new Calling_List__c(RecordTypeId = RecordTypeIdCollection,
                                                     Call_Outcome__c = 'Recovery list',
                                                     Account__c = objAcc.Id,
                                                     Registration_ID__c = '121851',
                                                     Customer_Flag__c = false,
                                                     Party_ID__c = '1177916');
            callingLst.add(calling2);
            Calling_List__c calling3 = new Calling_List__c(RecordTypeId = RecordTypeIdCollection,
                                                     Call_Outcome__c = 'Recovery list',
                                                     Account__c = objAcc.Id,
                                                     Registration_ID__c = '1212851',
                                                     Customer_Flag__c = false,
                                                     Party_ID__c = '1177917');
             callingLst.add(calling3);  
             insert callingLst;         
             UpdateCustomerFlagonCallingListHandler.updateMetadataOnSubordinateCallings(callingLst);
        }
    }
    
    
    static testMethod void test_updateFlagonCallingList () {
        
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
          
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',OnOffCheck__c = true));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForCollectionCL',OnOffCheck__c = true));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'RunCustomerFlagForFMCL',OnOffCheck__c = true));
        
        insert settingLst2;
        
        UpdateCustomerFlagonCallingListBatch batchCls = new UpdateCustomerFlagonCallingListBatch ();
        
        Id RecordTypeIdCollection = [
            SELECT Id
            FROM RecordType
            WHERE SObjectType='Calling_List__c' 
            AND DeveloperName='Collections_Calling_List'
            AND IsActive = TRUE LIMIT 1
        ].Id;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        

        
        List<Calling_List__c> lstCallingLists = createCallingList1( RecordTypeIdCollection , 5 ,objAcc.Id );
        insert lstCallingLists;
        
        Test.startTest();
            UpdateCustomerFlagonCallingListHandler.updateFlagonCallingList(lstCallingLists);
        Test.stopTest();
    }

    /*
     @ Description : To create calling list reocord
     @ Return      : list of calling list to be created
    */
    public static List<Calling_List__c> createCallingList1( Id RecordTypeIdCollection, Integer counter , Id acctId ) {

        List<Calling_List__c> lstCallingLists = new List<Calling_List__c>();
        for( Integer i=0; i<counter; i++ ) {
            lstCallingLists.add(new Calling_List__c(IsHideFromUI__c = false, Account__c = acctId  , Customer_Flag__c  = false ,RecordTypeId = RecordTypeIdCollection ) );
        }
        return lstCallingLists;
    }    
}