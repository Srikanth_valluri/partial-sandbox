/**************************************************************************************************
* Name               : DH_ResponseWrapper *
* Description        : This Wrapper Class  used To set Response Comes From  Drool Engine                     *
* Created Date       :                                                               *
* Created By         : UnKnow                                                                        *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR            DATE          COMMENTS                                              *

  1.1         Naresh(Accely)    20/06/2017    Add  New Parameter i.e schemeId
                                                                                            *
**************************************************************************************************/


public class DH_ResponseWrapper {
  
    public String buildingName;  //Buil2
    public String cunstructionStatus;  //Completed
    public String waiverCN;  //Charge1
    public string option6Id;
    public String waiverIdPN;  //1
    public string noofUnits;  //1
    public string discountPercentCN;  //15
    public string inLeuSN;
    public string productType;
    public string discountPercentPN;  //10
    public String unitIdCN;  //1
    public String option2Name;  //Get Truck
    public string option5Id;
    public string priceChangePN;  //0
    public String inLeuCN;  //10000
    public string priceChangePSF;  //0
    public string option4Id;
    public string inventoryThreshold;  //8
    public string unitIdPN;
    public string waiverSN;
    public string unitIdPP;
    public string option3Price;  //0
    public String waiverIdCN;  //1
    public string numberofBedrooms;  //2
    public string templateIdSN;
    public string discountPSF_SN;  //0
    public String agent;  //Agn1
    public String inLeuPN;  //20000
    public String category;  //Villa
    public string price;  //35000
    public String pcId;  //1
    public string floor;  //40
    public string paymentAtMilestone;
    public string option2Price;  //20000
    public string totalDealValue;  //35000
    public String projectName;  //Project2
    public string promoNameSN;
    public string option6Name;
    public String region;  //India
    public string option6Price;  //0
    public string option3Name;
    public string option1Price;  //10000
    public String templateIdCN;  //2
    public string discountPSF_CN;  //15000
    public string customerthresholdValue;  //10
    public String giftPN;  //Maruti
    public String option1Name;  //Get Car
    public string waiveridSN;
    public String templateIdPN;  //1
    public string discountPSF_PN;  //10000
    public String waiverPN;  //Change1
    public string discountPercentSN;  //0
    public String giftCN;  //Maruti
    public string customerId;
    public string area;  //15
    public string option5Price;  //0
    public String option1Id;  //1
    public string totalArea;  //15
    public String option2Id;  //2
    public string option3Id;
    public string discountFlatPN;  //2000
    public string discountFlatCN;  //2000
    public String subCategory;  //null
    public String promoNamePN;  //Rule 21
    public string giftSN;
    public string bedroomType;  //2
    public String residence;  //Mumbai
    public string option4Price;  //0
    public String views;  //Sea
    public string priceChangePer;  //0
    public string option4Name;
    public string inventoryId;
    public string facing;
    public string option5Name;
    public string dateofBooking;  //1495929600000
    public string discountFlatSN;  //0
    public String marketingProject;  //Mar1
    public string campId;
    public string campName;
    public string schemeIdIn;
    public string customerThresholdOld;
    public string noofUnitsInput;
    public string noofunitsinputPN;
    public string promoIdPN;
    public string totalDealValuePN;
    public string schemeId;
    public string customerThresOutPN;
    public string inventoryThresholdOld;
    public string totalAreaPN;
    public string inventoryThreshOutPN;
    public string promoIdOutputPN;
    public string templateIdOP;
    public string  templateIdOP1;
    public string  templateIdOP2;
    public string  templateIdOP3;
    public string  templateIdOP4;
    public string  templateIdOP5;
    public string  testVar{get;set;}
    // Add New Parameter
    public string discountPercentOP1;  
    public string discountFlatOP1; 
    public string discountPSF_OP1; 
    
    public String discountPercentOP2 ;
    public String discountFlatOP2;
    public String discountPSF_OP2;
    
    public String discountPercentOP3;
    public String discountFlatOP3;
    public String discountPSF_OP3;
    
    
    public String discountPercentOP4;
    public String discountFlatOP4;
    public String discountPSF_OP4;

    public String discountPercentOP5;
    public String discountFlatOP5;
    public String discountPSF_OP5;

    public String discountPercentOP6;
    public String discountFlatOP6;
    public String discountPSF_OP6;
   
}