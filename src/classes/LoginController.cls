global with sharing class LoginController {
    //global String username {get; set;}
    // global String password {get; set;}


    public string username{set;get;}
    public string password{set;get;}
    public boolean rememberMe{set;get;}
    
    
    public LoginController() {
        rememberMe = false;
       
        if (ApexPages.currentPage().getCookies().get('username') != NULL)
            username = ApexPages.currentPage().getCookies().get('username').getValue();
        if (ApexPages.currentPage().getCookies().get('password') != NULL) {
            string codedpassword = ApexPages.currentPage().getCookies().get('password').getValue();
            system.debug('codedpassword ::' + codedpassword );
            blob blobcodedpassword =  EncodingUtil.base64Decode(codedpassword);
            password = blobcodedpassword.toString();
            system.debug('password::' + password);
        }

        // registrationPageID = UtilityQueryManager.getProcessFlowId(LABEL.Agent_Portal_Registration_Page_Flow_Name);
    }


    public PageReference redirectToHome(){
        if(UserInfo.getUserType() != 'Guest'){
            return new PageReference('/PromoterDashboardPage');
        }

        return null;
    }
 
    global PageReference login() {
        blob blobPassword = blob.valueof(password);
        string codedPassword = EncodingUtil.base64Encode(blobPassword);
        if (rememberMe == true) {
            cookie Name = new Cookie('username', Username, null, 365, false);
            cookie PasswordCokies = new Cookie('password', codedPassword, null, 365, false);
            ApexPages.currentPage().setCookies(new Cookie[] {Name, PasswordCokies});        }
            
        return Site.login(username,password,'/PromoterDashboardPage');
    }

  // global PageReference login() {
  //       String startUrl = System.currentPageReference().getParameters().get('Damac_Login');
  //       return Site.login(username, password, startUrl );
  //   }
   
}