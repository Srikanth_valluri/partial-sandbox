public without sharing class HandoverController_CC {

    public String               parentCaseId            {get; set;}

    public static Map<String, Integer> APPOINTMENT_NUMBER = new Map<String, Integer> {
        'Documentation' => 1
        ,'Unit Viewing' => 2
        ,'Key Handover' => 3
    };

    @testVisible
    private static final String HO_TEMPLATE_NAME                = 'NHO';
    private static final String HANDOVER_PARENT_RT              = 'Handover Parent';
    private static final String HANDOVER_RT                     = 'Handover';
    private static final String UTILITY_REGISTRATION            = 'Utility Registration';
    private static final String ENDS_WITH_UTILITY_REGISTRATION  = '%Utility Registration';
    private static final String SIGNED_KEY_RELEASE_FORM         = '% Signed Key Release Form %';
    private static final String LETTER_OF_DISCHARGE             = '% Letter of Discharge %';
    private static final String SIGNED_LETTER_OF_DISCHARGE      = '% Signed Letter of Discharge %';
    private static final String DEWA                            = 'DEWA';
    private static final String EMPOWER                         = 'Empower';
    private static final String HANDOVER_CALLING_LIST_RT        = 'Handover Calling List';
    private static final String APPOINTMENT_SCHEDULING_RT       = 'Appointment Scheduling';
    private static final String EARLY_HANDOVER_RT               = 'Early Handover';

    private static final String PASSPORT                        = '%Passport%';
    private static final String EMIRATES_ID                     = '%Emirates Id%';
    private static final String EMPOWER_DOC                     = '%Empower%';

    public HandoverController_CC() {
        if (CustomerCommunityUtils.isCurrentView('handover')) {
            fetchPageParams();
        }
    }

    private void fetchPageParams() {
        parentCaseId = ApexPages.currentPage().getParameters().get('id');
    }

    @RemoteAction
    public static Map<Id, Handover> fetchHandovers() {
        if (CustomerCommunityUtils.customerAccountId == NULL) {
            return new Map<Id, Handover>();
        }

        Map<Id, Handover> mapLocationIdToHandover = new Map<Id, Handover>();

        for (Case sr : [ SELECT Id
                                , CaseNumber
                                , ParentId
                                , AccountId
                                , RecordTypeId
                                , RecordType.Id
                                , RecordType.Name
                                , Booking_Unit__c
                                , Booking_Unit__r.Id
                                , Booking_Unit__r.Name
                                , Booking_Unit__r.Unit_Name__c
                                , Booking_Unit__r.DEWA_Utility_Number__c
                                , Booking_Unit__r.DEWA_Registration_Date__c
                                , Booking_Unit__r.Empower_Utility_Number__c
                                , Booking_Unit__r.Empower_Registration_Date__c
                                , Booking_Unit__r.Inventory__c
                                , Booking_Unit__r.Inventory__r.Building_Location__c
                                , Booking_Unit__r.Inventory__r.Building_Location__r.Id
                                , Booking_Unit__r.Inventory__r.Building_Location__r.Name
                                , Booking_Unit__r.Inventory__r.Building_Location__r.Building_Name__c
                                , SNAGs_Completed__c
                                , ( SELECT  Id
                                            , Name
                                            , Case__c
                                            , Attachment_URL__c
                                            , Booking_Unit__c
                                            , IsValid__c
                                    FROM SR_Attachments__r
                                    WHERE (  ((Case__r.ParentId = NULL OR Case__r.RecordType.Name = 'Handover Parent' )
                                                OR (Case__r.ParentId != NULL OR Case__r.RecordType.Name = 'Handover')
                                                OR (Case__r.ParentId != NULL OR Case__r.RecordType.Name = 'Early Handover' ) )
                                                AND (Name LIKE :PASSPORT OR Name LIKE :EMPOWER_DOC OR Name LIKE :EMIRATES_ID OR Name LIKE :ENDS_WITH_UTILITY_REGISTRATION)
                                          )
                                  )
                                , ( SELECT  Id
                                            , Case__c
                                            , RecordTypeId
                                            , RecordType.Name
                                            , Account__c
                                            , Account__r.Name
                                            , Booking_Unit__c
                                            , Booking_Unit_Name__c
                                            , Documentation_Appointment_Date__c
                                            , Documentation_Appointment_Start_Time__c
                                            , Documentation_Appointment_End_Time__c
                                            , Unit_Viewing_Date__c
                                            , Viewing_Start_Time__c
                                            , Viewing_End_Time__c
                                            , Key_Handover_Appointment_Date__c
                                            , Key_Handover_Appointment_Start_Time__c
                                            , Key_Handover_Appointment_Stop_Time__c
                                            , Appointment_Status__c
                                    FROM Calling_List__r
                                    WHERE RecordType.Name = :HANDOVER_CALLING_LIST_RT
                                    AND Calling_List__c = NULL)
                         FROM Case
                         WHERE RecordType.Name IN (:HANDOVER_PARENT_RT, :HANDOVER_RT, :EARLY_HANDOVER_RT)
                         AND AccountId = :CustomerCommunityUtils.customerAccountId
                         ORDER BY RecordType.Name DESC
                                 , ParentId NULLS FIRST
                                 , Id
                         LIMIT 1000
        ]) {
            System.debug('>>>>>>sr ' + sr);
            System.debug('sr.RecordType.Name = ' + sr.RecordType.Name);
            System.debug('sr.Booking_Unit__c = ' + sr.Booking_Unit__c);
            System.debug('sr.Booking_Unit__r.Inventory__c = ' + sr.Booking_Unit__r.Inventory__c);
            System.debug('sr.Booking_Unit__r.Inventory__r.Building_Location__c = '
                + sr.Booking_Unit__r.Inventory__r.Building_Location__c);
            System.debug('sr.ParentId = '+sr.ParentId);
            if (sr.Booking_Unit__c != NULL
                && sr.Booking_Unit__r.Inventory__c != NULL
                && sr.Booking_Unit__r.Inventory__r.Building_Location__c != NULL
            ) {
                Handover handover = mapLocationIdToHandover.get(
                                        sr.Booking_Unit__r.Inventory__r.Building_Location__c);
                if (handover == NULL) {
                    handover = new Handover();
                    handover.location = sr.Booking_Unit__r.Inventory__r.Building_Location__r;
                    handover.unitSrs = new List<Case>();
                    handover.sr = new Case();
                }
                System.debug('handover = '+handover);
                System.debug('handover.sr = '+handover.sr);
                if (('Handover Parent'.equalsIgnoreCase(sr.RecordType.Name)
                          || 'Early Handover'.equalsIgnoreCase(sr.RecordType.Name))
                          && sr.ParentId == NULL) {
                    handover.sr = sr;
                    system.debug('>>>handover.sr'+handover.sr);
                    if ('Early Handover'.equalsIgnoreCase(sr.RecordType.Name)) {
                        sr.ParentId = sr.Id;
                        handover.unitSrs.add(sr);
                    }
                } else if (sr.ParentId != NULL && handover.sr.Id == sr.ParentId) {
                    handover.unitSrs.add(sr);
                    system.debug('>>>handover.unitSrs'+handover.unitSrs);
                }
                System.debug('handover.unitSrs.size = ' + handover.unitSrs.size());
                mapLocationIdToHandover.put(sr.Booking_Unit__r.Inventory__r.Building_Location__c,
                                                                                          handover);
                /*Handover handover = new Handover();
                handover.location = sr.Booking_Unit__r.Inventory__r.Building_Location__r;
                handover.sr = sr;
                handover.unitSrs = sr.Cases;
                mapLocationIdToHandover.put(sr.Booking_Unit__r.Inventory__r.Building_Location__c, handover);*/
            }
        }
         System.debug('>>>>mapLocationIdToHandover ' + mapLocationIdToHandover);
        return mapLocationIdToHandover;
    }

    public static String blank(string value) {
         return value == null ? '' : value;
    }

    @RemoteAction
    public static HandoverPack fetchHandoverEmail(Id caseId) {
        System.debug('caseId = ' + caseId);
        List<Case> lstCase = [ SELECT Id
                                    , AccountId
                                    , Booking_Unit__c
                                    , Booking_Unit__r.Id
                                    , Booking_Unit__r.Name
                                    , Booking_Unit__r.Unit_Name__c
                                    , Booking_Unit__r.DEWA_Utility_Number__c
                                    , Booking_Unit__r.DEWA_Registration_Date__c
                                    , Booking_Unit__r.Empower_Utility_Number__c
                                    , Booking_Unit__r.Empower_Registration_Date__c
                                    , Booking_Unit__r.Inventory__c
                                    , Booking_Unit__r.Inventory__r.Building_Location__c
                                    , Booking_Unit__r.Inventory__r.Building_Location__r.Id
                                    , Booking_Unit__r.Inventory__r.Building_Location__r.Name
                                    , Booking_Unit__r.Inventory__r.Building_Location__r.Building_Name__c
                                    , Booking_Unit__r.Inventory__r.Building_Location__r.HO_Email_Template__c
                               FROM Case
                               WHERE Id = :caseId ];
        System.debug('lstCase = ' + lstCase);
        if (lstCase.isEmpty()) {
            return NULL;
        }
        Case sr = lstCase[0];
         system.debug('sr : '+sr);
        system.debug('sr  Booking Unit : '+sr.Booking_Unit__c);
        system.debug('sr Booking Inventory : '+sr.Booking_Unit__r.Inventory__c);
        system.debug('sr Booking Inventroy Building Location : '
                                +sr.Booking_Unit__r.Inventory__r.Building_Location__c);
        System.debug('Template Name : '
                        +sr.Booking_Unit__r.Inventory__r.Building_Location__r.HO_Email_Template__c);
        String HO_TemplateName = blank(
                sr.Booking_Unit__r.Inventory__r.Building_Location__r.HO_Email_Template__c);

        List<EmailTemplate> templates = [ SELECT Id
                                                 , Subject
                                                 , Name
                                                 , DeveloperName
                                          FROM EmailTemplate
                                          WHERE DeveloperName = : HO_TemplateName ];
        System.debug('templates = ' + templates);
        if (templates.isEmpty()) {
            return NULL;
        }
        EmailTemplate template = templates[0];
        template.Subject = template.Subject == NULL ? '' : template.Subject.replaceAll('\\{!(.*?)\\}','%');
        System.debug('template :'+template.Subject);

        List<Task> lstTask = new List<Task>();
        System.debug('sr.AccountId = ' + sr.AccountId);
        lstTask = [ SELECT Id
                          , AccountId
                    FROM Task
                    WHERE AccountId = :sr.AccountId ];
        System.debug('lstTask = ' + lstTask);

        String queryBUSubject = '%'+sr.Booking_Unit__r.Unit_Name__c+'%';
        System.debug('queryBUSubject : ' + queryBUSubject);

        List<EmailMessage> lstEmailMessage = [
            SELECT  Id
                    , FromName
                    , FromAddress
                    , ToAddress
                    , CcAddress
                    , BccAddress
                    , Subject
                    , HtmlBody
                    , TextBody
                    , MessageDate
                    , CreatedDate
            FROM    EmailMessage
            WHERE (ActivityId IN :lstTask
                    AND Subject LIKE :template.Subject
                    AND Subject LIKE :queryBUSubject)
            ORDER BY CreatedDate DESC
            LIMIT   1
        ];
        System.debug('lstEmailMessage = ' + lstEmailMessage);
        if (lstEmailMessage.isEmpty()) {
            return NULL;
        }
         System.debug('lstEmailMessage[0].HtmlBody : '+lstEmailMessage[0].HtmlBody);
        HandoverPack handoverPack = new HandoverPack();
        handoverPack.email = lstEmailMessage[0];
        handoverPack.pack = getHandoverPackForUnit(sr.Booking_Unit__c);
        //handoverPack.emailHTMLBody = lstEmailMessage[0].HtmlBody;
        return handoverPack;
    }

    @RemoteAction
    public static SR_Attachments__c uploadDocument(
        Id srAttachmentId, String fileName, String fileBody, List<Integer> lstRegistrationDate, String utilityNumber
    ) {
        CaseAttachmentController caseAttachmentUploader = new CaseAttachmentController(
            new ApexPages.StandardController(new SR_Attachments__c(Id = srAttachmentId))
        );
        caseAttachmentUploader.fileName = fileName;
        caseAttachmentUploader.fileBody = EncodingUtil.base64Decode(fileBody);
        caseAttachmentUploader.selectedAwesomeness = false;
        caseAttachmentUploader.damacSigned = false;
        caseAttachmentUploader.needCorrection = false;
        caseAttachmentUploader.saveCaseAttachmentURL();
        if (caseAttachmentUploader.objSRAttach.Name.containsIgnoreCase(UTILITY_REGISTRATION)) {
            Booking_Unit__c unit = new Booking_Unit__c(
                Id = caseAttachmentUploader.objSRAttach.Case__r.Booking_Unit__c
            );
            Date registrationDate = Date.newInstance(lstRegistrationDate[0], lstRegistrationDate[1]
                                            , lstRegistrationDate[2]);
            if (caseAttachmentUploader.objSRAttach.Name.containsIgnoreCase(DEWA)) {
                unit.DEWA_Registration_Date__c = registrationDate;
                unit.DEWA_Utility_Number__c = utilityNumber;
            } else if (caseAttachmentUploader.objSRAttach.Name.containsIgnoreCase(EMPOWER)) {
                unit.Empower_Registration_Date__c = registrationDate;
                unit.Empower_Utility_Number__c = utilityNumber;
            }
            update unit;
        }
        return caseAttachmentUploader.objSRAttach;
    }

    @RemoteAction
    public static List<Calling_List__c> fetchCallingLists(Id unitSrId) {
        if (String.isBlank(unitSrId)) {
            return NULL;
        }

        Map<Integer, Calling_List__c> mapCallingList = new Map<Integer, Calling_List__c>{
            0   =>  new Calling_List__c(),
            1   =>  new Calling_List__c(Sub_Purpose__c = 'Documentation'),
            2   =>  new Calling_List__c(Sub_Purpose__c = 'Unit Viewing'),
            3   =>  new Calling_List__c(Sub_Purpose__c = 'Key Handover')
        };

        List<Calling_List__c> lstCallingList = [ SELECT  Id
                                                         , Case__c
                                                         , Calling_List__c
                                                         , RecordTypeId
                                                         , RecordType.Name
                                                         , Account__c
                                                         , Account__r.Name
                                                         , Booking_Unit__c
                                                         , Booking_Unit_Name__c
                                                         , Documentation_Appointment_Date__c
                                                         , Documentation_Appointment_Start_Time__c
                                                         , Documentation_Appointment_End_Time__c
                                                         , Unit_Viewing_Date__c
                                                         , Viewing_Start_Time__c
                                                         , Viewing_End_Time__c
                                                         , Key_Handover_Appointment_Date__c
                                                         , Key_Handover_Appointment_Start_Time__c
                                                         , Key_Handover_Appointment_Stop_Time__c
                                                         , Remarks__c
                                                         , Service_Type__c
                                                         , Sub_Purpose__c
                                                         , Appointment_Date__c
                                                         , Appointment_Slot__c
                                                         , Appointment_Status__c
                                                 FROM Calling_List__c
                                                 WHERE (Case__c = :unitSrId
                                                        AND RecordType.Name = :HANDOVER_CALLING_LIST_RT)
                                                 OR  (Calling_List__r.Case__c = :unitSrId
                                                      AND RecordType.Name = :APPOINTMENT_SCHEDULING_RT)];
        if (lstCallingList.isEmpty()) {
            return new List<Calling_List__c>();
        }
        for (Calling_List__c callList : lstCallingList) {
            if (callList.Calling_List__c == NULL) {
                mapCallingList.put(0, callList);
            } else if (APPOINTMENT_NUMBER.keySet().contains(callList.Sub_Purpose__c)) {
                mapCallingList.put(APPOINTMENT_NUMBER.get(callList.Sub_Purpose__c), callList);
            }
        }

        return mapCallingList.values();
    }

    @RemoteAction
    public static Decimal fetchUnitDues(Id caseId) {
        return null;
    }

    @RemoteAction
    public static ContentDocument getHandoverPackForUnit(Id unitId) {
        System.debug('unitId = ' + unitId);
        if (unitId == NULL) return NULL;

        System.debug([ SELECT Id
                             , ShareType
                             , Visibility
                             , LinkedEntityId
                             , ContentDocumentId
                             , ContentDocument.ParentId
                             , ContentDocument.ArchivedById
                             , ContentDocument.ArchivedDate
                             , ContentDocument.ContentModifiedDate
                             , ContentDocument.Id
                             , ContentDocument.CreatedDate
                             , ContentDocument.CreatedById
                             , ContentDocument.Description
                             , ContentDocument.FileExtension
                             , ContentDocument.FileType
                             , ContentDocument.IsArchived
                             , ContentDocument.IsDeleted
                             , ContentDocument.LastModifiedById
                             , ContentDocument.LastModifiedDate
                             , ContentDocument.LastReferencedDate
                             , ContentDocument.LastViewedDate
                             , ContentDocument.LatestPublishedVersionId
                             , ContentDocument.OwnerId
                             , ContentDocument.PublishStatus
                             , ContentDocument.ContentSize
                             , ContentDocument.SystemModstamp
                             , ContentDocument.Title
                       FROM ContentDocumentLink
                       WHERE LinkedEntityId = :unitId ]);

        List<ContentDocumentLink> lstContentDocumentLink = [ SELECT  Id
                                                                    , ShareType
                                                                    , Visibility
                                                                    , LinkedEntityId
                                                                    , ContentDocumentId
                                                                    , ContentDocument.SystemModstamp
                                                                    , ContentDocument.PublishStatus
                                                                    , ContentDocument.ContentSize
                                                                    , ContentDocument.FileExtension
                                                                    , ContentDocument.FileType
                                                                    , ContentDocument.LatestPublishedVersionId
                                                                    , ContentDocument.OwnerId
                                                                    , ContentDocument.Description
                                                                    , ContentDocument.Id
                                                                    , ContentDocument.ParentId
                                                                    , ContentDocument.Title
                                                             FROM ContentDocumentLink
                                                             WHERE LinkedEntityId = :unitId
                                                             AND ContentDocument.Title LIKE '%Handover Pack.zip'
                                                             LIMIT 1 ];
        System.debug('lstContentDocumentLink = ' + lstContentDocumentLink);
        if (lstContentDocumentLink.isEmpty()) {
            return NULL;
        }
        return lstContentDocumentLink[0].ContentDocument;
    }

    @RemoteAction
    public static String getNiPaymentData(Id unitId, Decimal amount, String sourceUrl) {
        return UnitDetailController.doPay(unitId, amount, sourceUrl, 0, 0);
    }

    @RemoteAction
    public static String downloadUnitSnags(String bookingUnitId, String bookingUnitName) {
        System.debug('--- bookingUnitId --- : '+bookingUnitId);
        if (String.isBlank(bookingUnitId)) {
            return NULL;
        }
        //String projectName = bookingUnitName.substringBefore('/');
        //System.debug('--- projectName --- : '+projectName);
        /*list<Case> lstSnagReport = [SELECT Id
                                          , Snags_Reported__c
                                          , (SELECT Id
                                                  , Name
                                                  , Attachment_URL__c
                                             FROM SR_Attachments__r
                                             WHERE Name = 'Snag Report'
                                             AND Attachment_URL__c != ''
                                             ORDER BY CreatedDate DESC LIMIT 1)
                                          , Booking_Unit__c
                                          , Booking_Unit__r.Unit_Name__c
                                          , SNAGs_Completed__c
                                    FROM Case
                                    WHERE (RecordType.DeveloperName = 'Handover'
                                        OR RecordType.DeveloperName = 'Early_Handover')
                                    AND AccountId =:CustomerCommunityUtils.customerAccountId
                                    AND Booking_Unit__c = :bookingUnitId ];
                                    //ORDER BY CreatedDate DESC LIMIT 1];
        System.debug('--- lstSnagReport --- : '+lstSnagReport);
        UnitSnags unitSnags = new UnitSnags();

        if(!lstSnagReport.isEmpty()){
          for(Case objCase : lstSnagReport) {
              System.debug('--- objCase.SR_Attachments__r --- : '+objCase.SR_Attachments__r);
              System.debug('--- objCase.Booking_Unit__r.Unit_Name__c --- : '+objCase.Booking_Unit__r.Unit_Name__c);
              if(objCase.Booking_Unit__c != NULL && objCase.Booking_Unit__r.Unit_Name__c == bookingUnitName) {
                  System.debug('If Booking Unit');
                  if(!objCase.SR_Attachments__r.isEmpty()) {
                      System.debug('Add Attachment URL');
                      unitSnags.SnagReportUrl = objCase.SR_Attachments__r[0].Attachment_URL__c;
                  }
              }
          }
        }
        System.debug('---unitSnags.SnagReportUrl --- : '+unitSnags.SnagReportUrl );

        list<Id> lstCaseId = new list<Id>();
        for(Case objCase : lstSnagReport) {
          lstCaseId.add(objCase.id);
        }
        System.debug('--- lstCaseId --- : '+lstCaseId);

        unitSnags.lstSnag = new list<SNAGs__c>();

        list<SNAGs__c> lstSnags = new list<SNAGs__c>();
        if(!lstSnagReport.isEmpty()) {
            lstSnags = [SELECT id
                               , Name
                               , SDCategory__c
                               , SDType__c
                               , Priority__c
                               , Status__c
                               , SD_Description__c
                               , User_Description__c
                               , Created_Date__c
                               , Booking_Unit__c
                               , Booking_Unit__r.Unit_Name__c
                               , Defect_Current_Status__c
                               , Image__c
                        FROM SNAGs__c
                        WHERE Case__c IN :lstCaseId
            ];
        }
        System.debug('--- lstSnags --- : '+lstSnags);
        System.debug('--- lstSnags Size --- : '+lstSnags.size());

        if (!lstSnags.isEmpty()) {
            unitSnags.lstSnag.addAll(lstSnags);
        } else {
            // ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'No SNAGs for this unit');
            // ApexPages.addMessage(myMsg);
            // return null;
            unitSnags.msg = 'No Snags for this Unit';
        } */

        //Updated by-Shashank
        List<Booking_Unit__c> lstBU = [SELECT id
                                    ,Inventory__r.Property__r.SnagValue__c
                                    ,SnagValue__c
                                    ,Unit_Name__c
                              FROM Booking_Unit__c
                              WHERE id = :bookingUnitId
                              AND Unit_Name__c =: bookingUnitName];

        String snagUrl;

        if(lstBU != null && !lstBU.isEmpty()) {
            String snagValue = lstBU[0].SnagValue__c;

            System.debug('snagValue :' + snagValue);
            System.debug('Label : Snag_Onsite_Base_URL :'+Label.Snag_Onsite_Base_URL);
            System.debug('Unit Name: ' + lstBU[0].Unit_Name__c);
            System.debug('Label.Snag_Onsite_URL_Key : ' + Label.Snag_Onsite_URL_Key);

            snagUrl = Label.Snag_Onsite_Base_URL+'/'+snagValue+'/api/OnsiteApi/GetSRFPDFByRef?ExtRef='+lstBU[0].Unit_Name__c+'&key='+Label.Snag_Onsite_URL_Key;
        }

        System.debug('snagUrl :'+snagUrl);

        return snagUrl;
        //return unitSnags;
    }

    @RemoteAction
    public static UnitSnags getUnitSnags(String bookingUnitId, String bookingUnitName) {
        System.debug('--- bookingUnitId --- : '+bookingUnitId);
        if (String.isBlank(bookingUnitId)) {
            return NULL;
        }
        /*list<Case> lstSnagReport = [SELECT Id
                                          , Snags_Reported__c
                                          , (SELECT Id
                                                  , Name
                                                  , Attachment_URL__c
                                             FROM SR_Attachments__r
                                             WHERE Name = 'Snag Report'
                                             AND Attachment_URL__c != ''
                                             ORDER BY CreatedDate DESC LIMIT 1)
                                          , Booking_Unit__c
                                          , Booking_Unit__r.Unit_Name__c
                                          , SNAGs_Completed__c
                                    FROM Case
                                    WHERE (RecordType.DeveloperName = 'Handover'
                                        OR RecordType.DeveloperName = 'Early_Handover')
                                    AND AccountId =:CustomerCommunityUtils.customerAccountId
                                    AND Booking_Unit__c = :bookingUnitId ];
                                    //ORDER BY CreatedDate DESC LIMIT 1];
        System.debug('--- lstSnagReport --- : '+lstSnagReport);
        UnitSnags unitSnags = new UnitSnags();

        if(!lstSnagReport.isEmpty()){
          for(Case objCase : lstSnagReport) {
              System.debug('--- objCase.SR_Attachments__r --- : '+objCase.SR_Attachments__r);
              System.debug('--- objCase.Booking_Unit__r.Unit_Name__c --- : '+objCase.Booking_Unit__r.Unit_Name__c);
              if(objCase.Booking_Unit__c != NULL && objCase.Booking_Unit__r.Unit_Name__c == bookingUnitName) {
                  System.debug('If Booking Unit');
                  if(!objCase.SR_Attachments__r.isEmpty()) {
                      System.debug('Add Attachment URL');
                      unitSnags.SnagReportUrl = objCase.SR_Attachments__r[0].Attachment_URL__c;
                  }
              }
          }
        }
        System.debug('---unitSnags.SnagReportUrl --- : '+unitSnags.SnagReportUrl );

        list<Id> lstCaseId = new list<Id>();
        for(Case objCase : lstSnagReport) {
          lstCaseId.add(objCase.id);
        }
        System.debug('--- lstCaseId --- : '+lstCaseId);*/
        UnitSnags unitSnags = new UnitSnags();
        unitSnags.lstSnag = new list<SNAGs__c>();

        //Added the below for getting new snags from  onsite & then updating - 13/10/19 - Shubham
        List<Booking_Unit__c> lstBU = [SELECT id
                                             ,Inventory__c
                                             ,Unit_Name__c
                                             ,Booking__r.Account__r.IsPersonAccount
                                             ,Booking__r.Account__r.Email__pc
                                             ,Booking__r.Account__r.Email__c
                                             ,Booking__r.Account__r.Mobile_Phone_Encrypt__pc
                                             ,Booking__r.Account__r.Name
                                             ,Booking__r.Account__r.Id
                                             ,Inventory__r.Building_Location__c
                                             ,Inventory__r.Building_Location__r.Id
                                             ,Inventory__r.Building_Location__r.Name
                                             ,Inventory__r.Building_Location__r.Is_New_Snagger__c
                                             ,Inventory__r.Building_Location__r.Building_Name__c
                                             ,
                                             ( SELECT id
                                                     ,Expected_date_of_completion_of_Snags__c
                                                     ,SNAGs_Completed__c
                                                     ,Owner.Email
                                                     ,Snags_Reported_at_first__c
                                                     ,Total_Snags_Reported_by_Customer__c
                                                     ,Total_Snags_Open__c
                                                     ,Total_Snags_Closed__c
                                                     ,Total_Snags_Fixed__c
                                                FROM Cases__r
                                                WHERE Booking_Unit__c =: bookingUnitId
                                             )
                                       FROM Booking_Unit__c
                                       WHERE id=:bookingUnitId];
        System.debug('lstBU : - '+lstBU);
        UpdateSnagInfoonCase.getSnagInfo(lstBU);
        //

        list<SNAGs__c> lstSnags = new list<SNAGs__c>();
        /*if(!lstSnagReport.isEmpty()) {*/
            lstSnags = [SELECT id
                               , Name
                               , SDCategory__c
                               , SDType__c
                               , Priority__c
                               , Status__c
                               , SD_Description__c
                               , User_Description__c
                               , Created_Date__c
                               , Booking_Unit__c
                               , Booking_Unit__r.Unit_Name__c
                               , Defect_Current_Status__c
                               , Image__c
                               , URL__c
                        FROM SNAGs__c
                        //WHERE Case__c IN :lstCaseId
                        WHERE Booking_Unit__c =:bookingUnitId
            ];
        //}
        System.debug('--- lstSnags --- : '+lstSnags);
        System.debug('--- lstSnags Size --- : '+lstSnags.size());

        if (!lstSnags.isEmpty()) {
            //New changes for displaying Snag Status on 'View Snags/defects' on handover  -Shashank
            for(SNAGs__c objSnag : lstSnags) {
                if(!objSnag.Defect_Current_Status__c.equalsIgnoreCase('Attended') && !objSnag.Defect_Current_Status__c.equalsIgnoreCase('Closed')) {

                    objSnag.Defect_Current_Status__c = 'Open';
                }
            }

            unitSnags.lstSnag.addAll(lstSnags);
        } else {
             //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'No SNAGs for this unit');
             //ApexPages.addMessage(myMsg);
             //return null;
            unitSnags.msg = 'No Snags for this Unit';
        }

        return unitSnags;
    }

    public class Handover {
        public Location__c  location    {get; set;}
        public Case         sr          {get; set;}
        public List<Case>   unitSrs     {get; set;}
    }

    public class HandoverPack {
        public EmailMessage         email               {get; set;}
        public ContentDocument      pack                {get; set;}
        public String               emailHTMLBody       {get;set;}
    }

    public class UnitSnags {
        public String                           SnagReportUrl   {get; set;}
        public String                           msg             {get; set;}
        public String                           url             {get; set;}
        public List<SNAGs__c>                   lstSnag         {get; set;}
    }
}