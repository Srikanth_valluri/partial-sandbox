/*
Class Name : CSR_SendNotificationToApprover
Description : To send push notification asyncronously to CSR Case Approver
Test Class : CSR_SendNotificationToApproverTest

========================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
--------------------------------------------------------------------------------------------------------
1.0     |   11-11-2020      | Subin Antony        | Initial Draft
*******************************************************************************************************/
global class CSR_SendNotificationToApprover {
    @testVisible
    private static final String push_notification_sandbox_baseurl = 'https://stgservices.damacgroup.com';
    @testVisible
    private static final String push_notification_prod_baseurl = 'https://services.damacgroup.com';
    @testVisible
    private static final String push_notification_endpoint = '/api/PushNotification/PushMessage';
    @testVisible
    private static final String api_token_stg = 'AD9A501EC7611D124681D5DA0833F540';
    @testVisible
    private static final String api_token_prd = 'AD9A501EC7611D124681D5DA0833F540';
    @testVisible
    private static final String access_token_stg = 'FE42DF6EBCC6902B26B45D62B567AD19';
    @testVisible
    private static final String access_token_prd = '89BBB3F7241282C534719C2C875B66BD';
    
    @testVisible
    private static Boolean isSandbox;
    static {
        isSandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }
    
    global class CaseDataWrap {
        @InvocableVariable(label='Approver Email' required=true)
        public String approverEmail;
        @InvocableVariable(label='Case ID' required=true)
        public String caseId;
        @InvocableVariable(label='Case Number' required=true)
        public String caseNumber;
    }
    
    @InvocableMethod(label='Send push notification to approver')
    public static void sendPushNotification(List<CaseDataWrap> caseDetailList) {
        System.debug('.............................ENTERED sendPushNotificationToApprover INVOCABLE_METHOD: recList: ' + caseDetailList);
        Map<String, String> approverEmailMapById = new Map<String, String>();
        Map<String, List<Case>> csrCaseListMapByEmail = new Map<String, List<Case>>();
        
        Set<String> approverEmailSet = new Set<String>();
        for(CaseDataWrap caseDetails : caseDetailList) {
            approverEmailSet.add(caseDetails.approverEmail);
        }
        System.debug('............................. ApproverEmailSet: ' + approverEmailSet);
        
        List<User> approverList;
        try {
            approverList = [SELECT id, Name, email, profileId, profile.name FROM User WHERE email = :approverEmailSet];
        }
        catch(Exception ex) {
            system.debug(ex.getMessage() + ' : ' + ex.getStackTraceString());
            approverList = NULL;
        }
        System.debug('............................. ApproverUsrList: ' + approverList);
        
        if(NULL != approverList && approverList.size() > 0) {
            for(User approver : approverList) {
                approverEmailMapById.put(approver.id, approver.email);
            }
        	System.debug('............................. approverEmailMapById: ' + approverEmailMapById);
            
            List<Case> csrCases;
            try {
                csrCases = [SELECT id, caseNumber, recordType.name, recordType.developerName, status, 
                            Approving_User_Role__c, Approving_User_Name__c, Approving_User_Id__c, 
                            Account.Name, AccountId, CreatedDate, CreatedBy.name 
                            FROM Case WHERE recordType.name = 'Case Summary - Client Relation' 
                            AND status = 'Submitted' 
                            AND Approving_User_Id__c IN :approverEmailMapById.keySet() ORDER BY CreatedDate ASC];
            }
            catch(Exception ex){
                system.debug(ex.getMessage() + ' : ' + ex.getStackTraceString());
                csrCases = NULL;
            }
        	System.debug('............................. csrCases: ' + csrCases);
            
            if(NULL != csrCases && csrCases.size() > 0) {
                System.debug('............................. csrCasesSize: ' + csrCases.size());
                for(Case csrCase : csrCases) {
                    String approverEmail = approverEmailMapById.get(csrCase.Approving_User_Id__c);
                    List<Case> approverCaseList;
                    if(csrCaseListMapByEmail.containsKey(approverEmail)) {
                        approverCaseList = csrCaseListMapByEmail.get(approverEmail);
                    }
                    else {
                        approverCaseList = new List<Case>();
                        
                    }
                    approverCaseList.add(csrCase);
                    csrCaseListMapByEmail.put(approverEmail, approverCaseList);
                }
                System.debug('............................. csrCaseListMapByEmail: ' + csrCaseListMapByEmail);
            }
        } /* if(NULL != approverList && approverList.size() > 0) */
        
        for(CaseDataWrap caseDetails : caseDetailList) {
            Integer pendingCaseCount = 0;
            List<Case> pendingCSR = csrCaseListMapByEmail.get(caseDetails.approverEmail);
            if(NULL != pendingCSR && pendingCSR.size() > 0) {
                pendingCaseCount = pendingCSR.size();
            }
            System.debug('##################################### : caseID: ' + caseDetails.caseId);
            System.debug('##################################### : caseID: ' + caseDetails.caseId);
            System.debug('##################################### : caseID: ' + caseDetails.caseId);
            pushNotificationServiceCallout(caseDetails.caseId, caseDetails.caseNumber, caseDetails.approverEmail, pendingCaseCount);
        }
    }
    
    @future(callout = true)
    public static void pushNotificationServiceCallout(String caseId, String caseNumber, String ownerEmail, Integer pendingCount) {
        String jsonBody = '   {  '  + 
        '       "email": "'+ownerEmail+'",  '  + 
        '       "message": "Case No: '+caseNumber+' has been assigned to you for your action",  '  + 
        '       "title": "CRM/Collections Case Assigned",  '  + 
        '       "payload": {  '  + 
        '           "email": "'+ownerEmail+'",  '  + 
        '           "detail_type": "crm-collections",  '  + 
        '           "unique_identifier": "'+caseId+'",  '  + 
        '       },  '  + 
        '       "total_pending_count": ' + pendingCount + ',  '  + 
        '       "source_application_id": 9,  '  + 
        '       "dest_application_id": 1  '  + 
        '  }  ' ; 
        
        String accessToken = isSandbox ? access_token_stg : access_token_prd;
        String apiToken = isSandbox ? api_token_stg : api_token_prd;
        String endpoint = isSandbox ? push_notification_sandbox_baseurl : push_notification_prod_baseurl;
        endpoint += push_notification_endpoint;
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpoint);
        request.setMethod('POST');
        request.setBody(jsonBody);
        request.setHeader('Access-Token', accessToken);
        request.setHeader('API-Token', apiToken);
        request.setHeader('Content-Type', 'application/json');
        HttpResponse response;
        if(Test.isRunningTest()) {
            response = new HttpResponse();
            response.setStatus('OK');
            response.setStatusCode(200);
            response.setBody('{"meta_data":{"title":"OK","status_code":1,"message":"Successfully pushed message","developer_message":null}}');
        } else {
            response = http.send(request);
        }
        
        System.debug('requestBody:: ' + request.getBody());
        System.debug('response:: ' + response);
        System.debug('response body :: ' + response.getBody());
    }
}