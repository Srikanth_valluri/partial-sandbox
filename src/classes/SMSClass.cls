/*
Modification History :
v1.1    Ravi    2/Nov/2020      Added the logic to get the SMS Sender Names from Metadata
*/
public without sharing class SMSClass {

    public static map<string, map<string, string[]>> mapSenderNames = new map<string, map<string, string[]>>();

	@InvocableMethod
    public static void sendSMS(List<SMS_History__c> lstSMSHist) {
        System.debug('===InvocableMethod===='+lstSMSHist.size());
        System.debug('===lstSMSHist===='+lstSMSHist);
        // if(FMCaseTriggerHelper.fromTrigger == false)

        set<Id> setSMSHistoryIds = new set<Id>();
        for( SMS_History__c objSMS : lstSMSHist ) {
         setSMSHistoryIds.add( objSMS.Id );
        }

         list<SMS_History__c> lstSMSHistory = [ SELECT Id
                                                  FROM SMS_History__c
                                                 WHERE Id IN :setSMSHistoryIds
                                                   AND FM_Case__r.RecordType.DeveloperName IN ( 'Tenant_Registration', 'Tenant_Renewal' )
                                                   AND (NOT Name LIKE 'Portal Tenant Registration Verification%')
                                                   AND Is_SMS_Sent__c = false ];
         if( lstSMSHistory != NULL && !lstSMSHistory.isEmpty() ) {
            /*list<SMS_History__c> lstSMSHistoryToUpdate = new list<SMS_History__c>();
            for( SMS_History__c objSMS : lstSMSHistory ) {
                if( objSMS.Is_SMS_Sent__c == false && String.isNotBlank( objSMS.Message__c ) ) {
                    system.debug('== objSMS =='+objSMS);
                    lstSMSHistoryToUpdate.add( CustomerNotificationBatchHelper.sendSMSToCustomer( objSMS ) ) ;
                }
            }
            update lstSMSHistoryToUpdate ;*/

         }
         else {
          System.enqueueJob(new SendSMSQueueable(lstSMSHist[0].ID));
         }
    }

    public static map<string, map<string, string[]>> getSMSSenderNames(){
        if(mapSenderNames.isEmpty()){
            for(SMS_Sender_Name__mdt obj : [select Country_Code__c,Promotion_Sender_Display_Name__c, Transaction_Sender_Display_Name__c ,SMS_Account_Name__c 
                from SMS_Sender_Name__mdt where Is_Active__c = true ])
            {
                map<string, string[]> mapCountrySenders = (mapSenderNames.containskey(obj.SMS_Account_Name__c) ? mapSenderNames.get(obj.SMS_Account_Name__c) : new map<string, string[]>() );
                mapCountrySenders.put(obj.Country_Code__c, new string[]{obj.Transaction_Sender_Display_Name__c, obj.Promotion_Sender_Display_Name__c});
                mapSenderNames.put(obj.SMS_Account_Name__c, mapCountrySenders);
            }
        }
        return mapSenderNames;
    }

    public static string getSenderName(string smsActName, string phoneNumber, Boolean isPrmotioanl){
        if(mapSenderNames.isEmpty())
            getSMSSenderNames();
        
        if(mapSenderNames.containskey(smsActName) && phoneNumber != null){
            phoneNumber = phoneNumber.trim();
            if(phoneNumber.startsWithIgnoreCase('00'))
                phoneNumber = phoneNumber.removeStart('00');
            if(phoneNumber.startsWithIgnoreCase('+'))
                phoneNumber = phoneNumber.removeStart('+');
            if(phoneNumber.startsWithIgnoreCase('0'))
                phoneNumber = phoneNumber.removeStart('0');
            
            for(string countryCode : mapSenderNames.get(smsActName).keySet()){
                if(phoneNumber.startsWithIgnoreCase(countryCode)){
                    return (isPrmotioanl ? mapSenderNames.get(smsActName).get(countryCode)[1] : mapSenderNames.get(smsActName).get(countryCode)[0] );
                }
            }

            if(mapSenderNames.get(smsActName).get('All') != null){
                return (isPrmotioanl ? mapSenderNames.get(smsActName).get('All')[1] : mapSenderNames.get(smsActName).get('All')[0] );
            }
        }
        return smsActName;//dafault Outcome
    }

}