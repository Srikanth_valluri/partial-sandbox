@isTest
public class AssociatePaymentPlanControllerTest {
    @testSetup
    public static void InsertData(){
        Property__c p = new Property__c();
        p.Property_Name__c = 'Test';
        p.Property_ID__c = 123456;
        insert p;
        Location__c locBuild = new Location__c();
        locBuild.Name = 'Test1';
        locBuild.Location_Code__c = 'Test1';
        locBuild.Building_Name__c = 'Test1';
        locBuild.Location_Type__c = 'Building';
        locBuild.Location_ID__c = '1345';
        locBuild.Property_Name__c = p.Id;
        insert locBuild;
        Location__c loc = new Location__c();
        loc.Name = 'AS';
        loc.Location_Type__c = 'Floor';
        loc.Location_Code__c = 'AS';
        loc.Building_Name__c = 'AS1';
        loc.Building_Number__c = locBuild.Id;
        loc.Location_ID__c = '133';
        insert loc;
        Location__c locUnit = new Location__c();
        locUnit.Name = 'AS1';
        locUnit.Location_Code__c = 'AS1';
        locUnit.Building_Name__c = 'AS2';
        locUnit.Location_Type__c = 'Floor';
        locUnit.Floor_Number__c = loc.Id;
        locUnit.Location_ID__c = '134';
        locUnit.RecordTypeId = '0120Y000000DniDQAS';
        insert locUnit;
        
        Payment_Plan__c plan = new Payment_Plan__c();
        plan.Building_ID__c = '123';
        plan.Payment_Term_Description__c = 'Test Desc';
        plan.Effective_From__c = System.today();
        plan.Effective_To__c = System.today()+1;
        plan.Building_Location__c = locBuild.Id;
        insert plan;
        Payment_Terms__c terms= new Payment_Terms__c();
        terms.Installment__c='DP';
        terms.Description__c='Test Desc';
        terms.Milestone_Event__c='Imeediate';
        terms.Percent_Value__c='10.00';
        terms.Payment_Plan__c=plan.Id;
        insert terms;
        Inventory__c inv = new Inventory__c();
        inv.Status__c = 'Inventory';
        inv.Unit_Location__c = locUnit.id;
        inv.Plot_Area__c = '100.10';
        inv.Terrace_Area__c = '100';
        inv.Balcony_Area__c = '100';
        inv.Gross_Area__c = '1';
        inv.Garage_Area__c = 10;
        inv.Price__c = '100';
        inv.Floor_Location__c = loc.Id;
        inv.Building_Location__c = locBuild.id;
        insert inv;
        
        
    }
    
    @isTest
    public static void TestMethod1(){
        Property__c p = [SELECT id ,Property_Name__c FROM Property__c WHERE Property_Name__c=: 'Test'];
        Test.startTest();
        PageReference pageRef = Page.associatePaymentPlan;
      	Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('projectName',p.Property_Name__c);
        AssociatePaymentPlanController controller = new AssociatePaymentPlanController();
        controller.fetchBuildingDetails();
        Test.stopTest();
        
    }

}