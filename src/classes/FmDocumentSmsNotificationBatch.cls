/**
 * @File Name          : FmDocumentSmsNotificationBatch.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 7/25/2019, 4:38:01 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    7/25/2019, 4:35:35 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
global class FmDocumentSmsNotificationBatch implements Database.Batchable<String>, Database.AllowsCallouts {



    String query;

    Map<String, Set<Id>> mapRecipientPhoneToSetBatchId;
    Map<Id, FM_Document_Upload_Batch__c> mapDocBatchWithDocument;
    Map<String, Id> mapRecipientAddressToAccountId;

    global FmDocumentSmsNotificationBatch(
        Map<String, Set<Id>> mapRecipientPhoneToSetBatchId,
        Map<Id, FM_Document_Upload_Batch__c> mapDocBatchWithDocument,
        Map<String, Id> mapRecipientAddressToAccountId
    ) {
        this.mapRecipientPhoneToSetBatchId = mapRecipientPhoneToSetBatchId;
        this.mapDocBatchWithDocument = mapDocBatchWithDocument;
        this.mapRecipientAddressToAccountId = mapRecipientAddressToAccountId;
        //System.debug('mapRecipientPhoneToSetBatchId = ' + mapRecipientPhoneToSetBatchId);
        //System.debug('mapRecipientAddressToAccountId = ' + mapRecipientAddressToAccountId);
    }

    global Iterable<String> start(Database.BatchableContext BC) {
        return new List<String>(mapRecipientPhoneToSetBatchId.keySet());
    }

    global void execute(Database.BatchableContext BC, List<String> scope) {
        List<SMS_History__c> lstSms = new List<SMS_History__c>();
        List<Error_Log__c> lstErrors = new List<Error_Log__c>();

        for (String recipientPhone : scope) {
            //System.debug('recipientPhone = ' + recipientPhone);

            for (Id batchId : mapRecipientPhoneToSetBatchId.get(recipientPhone)) {
                FM_Document_Upload_Batch__c docBatch = mapDocBatchWithDocument.get(batchId);
                //System.debug('docBatch.FM_Recipient__c = ' + docBatch.FM_Recipient__c);
                //System.debug('docBatch.Document_Level__c = ' + docBatch.Document_Level__c);
                if(docBatch.Notification_SMS_Message__c != NULL){
                    SMS_History__c sms = new SMS_History__c(
                        Customer__c = mapRecipientAddressToAccountId.get(recipientPhone),
                        Name = 'Document Upload notification for ' + docBatch.Name,
                        Phone_Number__c = recipientPhone,
                        Message__c = docBatch.Notification_SMS_Message__c
                    );
               

                    sms = sendSms(sms);
                    if (sms.Is_SMS_Sent__c != true) {
                        lstErrors.add(new Error_Log__c(
                            Account__c = sms.Customer__c,
                            Error_Details__c = 'Error in sending SMS to Salesforce \n\n' + sms.Description__c,
                            Process_Name__c = 'Document SMS'
                        ));
                    }

                    lstSms.add(sms);
                }
            }

        }

        List<Database.SaveResult> lstSmsSaveResult = Database.insert(lstSms, false);
        Integer counter = 0;
        for (Database.SaveResult smsSaveResult : lstSmsSaveResult) {
            if (!smsSaveResult.isSuccess()) {
                String error = '';
                for (Database.Error dbError : smsSaveResult.getErrors()) {
                    error += dbError.getMessage() + '\n';
                }
                error = error.removeEnd('\n');

                lstErrors.add(new Error_Log__c(
                    Account__c = lstSms[counter].Customer__c,
                    Error_Details__c = 'Error in sending SMS to Salesforce \n\n' + error,
                    Process_Name__c = 'Document SMS'
                ));
            }
            counter++;
        }
        Database.insert(lstErrors, false);
    }

    global void finish(Database.BatchableContext BC) {

    }

    private static SMS_History__c sendSms(SMS_History__c sms) {
        //System.debug('sms = ' + sms);

        String userName = Label.FM_SMS_service_username;
        String password = Label.FM_SMS_service_password;
        String strSID = SMSClass.getSenderName(userName, sms.Phone_Number__c, false);

        HttpRequest req = new HttpRequest();
        req.setMethod('POST' );
        req.setEndpoint(Label.SMSCountryAPIBaseURL);
        req.setBody(String.format(
            'user={0}&passwd={1}&message={2}&mobilenumber={3}&sid={4}&MTYPE=LNG&DR=Y',
            new List<String> {
                userName, password, sms.Message__c, sms.Phone_Number__c, strSID
            }
        ));

        try {
            HTTPResponse res = new Http().send(req);
            if (String.isNotBlank(res.getBody())) {
                if(String.valueOf(res.getBody()).contains('OK:')){
                    sms.Description__c = res.getBody();
                    sms.Is_SMS_Sent__c = true;
                    sms.sms_Id__c = res.getBody().substringAfter('OK:');
                }
                else {
                    sms.Description__c = res.getBody();
                    sms.Is_SMS_Sent__c = false;
                }
                
            }
        }
        catch(Exception e) {
            sms.Description__c = e.getMessage();
        }

        //System.debug('sms = ' + sms);
        return sms;
    }

}