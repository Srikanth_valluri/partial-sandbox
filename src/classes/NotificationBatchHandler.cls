public with sharing class NotificationBatchHandler {
  

  public NotificationBatchHandler(){
    System.debug('==NotificationBatchHandler==' );
  }

  public static void ProcessRecords(List<NSIBPM__Service_Request__c> listSR){
    System.debug('==ProcessRecords==' + listSR);
    Map<String,List<String>> Groupname_emailidlst =getQueueUserMap();
    List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>(); 
     
    for(NSIBPM__Service_Request__c SR : listSR){
            
          if((SR.NSIBPM__Internal_Status_Name__c == 'Under Agent Executive Review' && SR.Under_Agent_Review_1day_formula__c == Date.Today()) || Test.isRunningTest()){
              String mailbody = 'Hello,<br/><br/>';
              mailbody += '   The '+SR.name+' is in Under Agent Executive Review status from one day.<br/><br/>';
              mailbody += '<br/>Thank You.';
              Messaging.SingleEmailMessage mail =sendEmail(SR,Groupname_emailidlst.get('Agent_Manager_Queue'),mailbody);
              mailList.add(mail);
          }
          if(SR.NSIBPM__Internal_Status_Name__c == 'Under Agent Executive Review' && SR.Under_Agent_Review_2dayformula__c == Date.Today()){
              String mailbody = 'Hello,<br/><br/>';
              mailbody += '  The '+SR.name+' is in Under Agent Executive Review status from two days.<br/><br/>';
              mailbody += '<br/>Thank You.';
              Messaging.SingleEmailMessage mail =sendEmail(SR,Groupname_emailidlst.get('Agent_HOD_Queue'),mailbody);
              mailList.add(mail);
          }
          if(SR.NSIBPM__Internal_Status_Name__c == 'Under Agent Manager Review' && SR.Under_Manager_Review_1_day_formula__c == Date.Today()){
              String mailbody = 'Hello,<br/><br/>';
              mailbody += '    The '+SR.name+' is in Under Under Agent Manager Review status from one day.<br/><br/>';
              mailbody += '<br/>Thank You.';
              Messaging.SingleEmailMessage mail =sendEmail(SR,Groupname_emailidlst.get('Agent_HOD_Queue'),mailbody);
              mailList.add(mail);
          }
    }
    if(mailList.size() > 0){
      System.debug('Mail list' + mailList);
      System.debug('Mail list @@' + JSON.serialize(mailList));  
      Messaging.sendEmail(mailList);
    }
  }
  
  public static Map<String,List<String>> getQueueUserMap(){
      Map<String,List<String>> groupname_UserEmailLst = new Map<String,List<String>>();
      
      Map<Id,List<Id>> groupid_UserIds = new Map<Id,List<ID>>();
      Set<ID> userids = new Set<ID>();
      
      Map<Id,Group> groupMap = new Map<id,Group>([SELECT DeveloperName,
                                                         Name,
                                                         Id 
                                                  FROM Group 
                                                  WHERE Type = 'Queue' 
                                                  AND (DeveloperName ='Agent_HOD_Queue' 
                                                  OR DeveloperName = 'Agent_Manager_Queue')]);
      
      
      for(GroupMember grpmemberobj:[SELECT UserOrGroupId,
                                           groupid 
                                    FROM GroupMember 
                                    WHERE GroupId in :groupMap.keySet()])
      {   
          String objname = grpmemberobj.UserOrGroupId.getsobjecttype().getDescribe().getName();
          if(objname.equalsIgnoreCase('User')){ 
              if(groupid_UserIds.containsKey(grpmemberobj.groupid )){
                  groupid_UserIds.get(grpmemberobj.groupid).add(grpmemberobj.UserOrGroupId);
              }else{
                  groupid_UserIds.put(grpmemberobj.groupid,new List<Id>{grpmemberobj.UserOrGroupId});
              }
              userids.add(grpmemberobj.UserOrGroupId);
          }
      }//end of for
      
      Map<Id,User> usermap =new Map<Id,User> ([Select Email
                                               FROM User
                                               WHERE id in :userids
                                               AND IsActive =true ]);
                                               
      for(Id userid : usermap.keySet()){
          System.debug(' All user ids @@@@@ ' + userid);
      }                                                                              
      for(Id groupid : groupid_UserIds.keySet()){
          List<Id> userid = groupid_UserIds.get(groupid);
          for(Id objid : userid){
              String groupname = groupMap.get(groupid).DeveloperName;
              if(groupname_UserEmailLst.containsKey(groupname)){
                 System.debug('@@ Group name @@' + groupname);
                 System.debug('@@ userid @@' + objid);
                 if(usermap.containsKey(objid)){ 
                     groupname_UserEmailLst.get(groupname).add(usermap.get(objid).email);
                 }     
              }else{
                 if(usermap.containsKey(objid)){ 
                     groupname_UserEmailLst.put(groupname,new List<String>{usermap.get(objid).email}); 
                 }    
              }
          }
      
      }
      
      return groupname_UserEmailLst;                                                                 
  
  }
  
  public static Messaging.SingleEmailMessage sendEmail(NSIBPM__Service_Request__c SR,List<String> toAddress,String MailBody){
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      mail.setToAddresses(toAddress);
      mail.setSubject('Notification for SR');
      mail.setHtmlBody(MailBody);
      return mail;
  }

      
}