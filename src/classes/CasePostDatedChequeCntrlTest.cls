@isTest
public class CasePostDatedChequeCntrlTest {
    @isTest
    static void testPDCId(){
        
        Account objAccount = new Account();
        objAccount.Name = 'test';
        objAccount.Party_ID__c = '12345';
        insert objAccount;
        
        Case objCase = new Case();
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();
        objCase.AccountId = objAccount.Id;
        insert objCase;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        objBooking.Account__c = objAccount.Id;
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Registration_ID__c = 'test';
        objBU.Booking__c = objBooking.Id;
        insert objBU;
        
        TriggerOnOffCustomSetting__c objTrigger = new TriggerOnOffCustomSetting__c();
        objTrigger.Name = 'CallingListTrigger';
        objTrigger.OnOffCheck__c = true;
        insert objTrigger;
        
        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Unit_Name__c = 'test';       
        objCallingList.Account__c = objAccount.Id;
        objCallingList.Booking_Unit__c  = objBU.Id;
        insert objCallingList;
        
      
        Case_Post_Dated_Cheque__c objCasePost = new Case_Post_Dated_Cheque__c();
        objCasePost.Receipt_No__c =  '191';
        objCasePost.Receipt_Date__c = System.today();
        objCasePost.New_maturity_date__c = System.today(); 
        objCasePost.New_Amount__c = 100;
        objCasePost.CurrencyIsoCode =  'AED';
        objCasePost.Reason__c = 'testReason';
        objCasePost.IPMS_Cash_Receipt_Id__c = '';
        objCasePost.Receipt_Classification__c = 'AOPT';
        objCasePost.Mode_of_Payment__c = 'CDM-Cash Deposit';
        objCasePost.New_Drawer__c = ' testDrawer';
        objCasePost.New_Bank__c = 'testBank';
        objCasePost.Account__c = objAccount.Id;
        objCasePost.Calling_List__c = objCallingList.Id;
        objCasePost.Case__c = objCase.id;
        objCasePost.Booking_Unit__c = objBU.Id;
        objCasePost.Calling_List__c = objCallingList.Id;
        objCasePost.OwnerId = UserInfo.getUserId();
        insert objCasePost;
        
        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(objCasePost);
            CasePostDatedChequeCntrl objCasePDC = new CasePostDatedChequeCntrl(sc);
            objCasePDC.init();
            objCasePDC.back();
        Test.stopTest();
    }
}