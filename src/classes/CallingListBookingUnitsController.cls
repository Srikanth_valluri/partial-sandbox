/*-------------------------------------------------------------------------------------------------
Description: Controller for CallingListBookingUnits inline page
    ============================================================================================================================
        Version | Date(DD-MM-YYYY) | Last Modified By | Comments
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 12-12-2017       | Lochana Rajput   | Added logic to show booking unit details related to CallingList.Case
   =============================================================================================================================
*/
public without sharing class CallingListBookingUnitsController {
    String callingListId;
    Public String regId {get;set;}
    Public String soaUrl {get;set;}
    public Boolean isLightningMode{get;set;}
    // public List<SR_Booking_Unit__C> lstSRBookingUnits {get;set;}
    public List<BookingUnitWrapper> lstBookingWrapper {get;set;}
    //Constructor
    public CallingListBookingUnitsController(ApexPages.StandardController stdController) {
        callingListId = ApexPages.currentPage().getParameters().get('id');
        lstBookingWrapper = new List<BookingUnitWrapper>();
        if(String.isNotBlank(callingListId)) {
            getBookingUnits();
        }
    }

    public void getBookingUnits() {
        List<Calling_List__c> lstCallingList = new List<Calling_List__c>();
        lstCallingList = [SELECT Case__c FROM Calling_List__c
                                            WHERE Id =: callingListId
                                            AND Case__c != NULL];
        if(lstCallingList.size() > 0) {
            try{
                for(SR_Booking_Unit__c objSRBU : [SELECT Booking_Unit__r.Registration_ID__c,
                                                    Booking_Unit__r.Unit_Name__c,Booking_Unit__r.Handover_Flag__c,Booking_Unit__r.Early_Handover__c,
                                                    Booking_Unit__r.Name
                                                FROM SR_Booking_Unit__c
                                                WHERE Case__c =: lstCallingList[0].Case__c
                                                AND Booking_Unit__c != NULL
                                                AND Booking_Unit__r.Registration_ID__c != NULL]) {
                    String strResponse = assignmentEndpoints.fetchAssignmentDues(
                    new Booking_Unit__c(Registration_ID__c =  objSRBU.Booking_Unit__r.Registration_ID__c));
                    System.debug('====RESPONSE====' + strResponse);
                    Map<String,Object> apexmap = (Map<String, Object>) JSON.deserializeUntyped(strResponse);
                    system.debug('===Balance as per SOA===' + apexmap.get('Balance as per SOA'));
                    if(apexmap.containsKey('Balance as per SOA') && apexmap.get('Balance as per SOA') != NULL
                        && apexmap.get('Balance as per SOA') != '') {
                        lstBookingWrapper.add(new BookingUnitWrapper(objSRBU, Decimal.valueOf(String.valueOf(apexmap.get('Balance as per SOA')))));
                    }
                    else {
                        lstBookingWrapper.add(new BookingUnitWrapper(objSRBU, 0.00));
                    }
                }
            }
            catch(System.Exception excp) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,excp.getMessage() + excp.getLineNumber()));
            }
        }//if


    }
    public void generateSOA() {
        System.debug('==regId====' + regId);
        try {
            GenerateSOAController.soaResponse strResponse = GenerateSOAController.getSOADocument(regId);
            system.debug('SOA generated status '+strResponse.status);
            system.debug('SOA generated url'+strResponse.url);
            if(String.isBlank(strResponse.url) ||
               strResponse.url == null || strResponse.status == 'Exception' ) {
                   ApexPages.addmessage(new ApexPages.message(
                   ApexPages.severity.Error,'An exception has occurred while processing the request, got response null,  Status-'+strResponse.status));
                   // errorLogger('n exception has occurred while processing the request, got response null, Status-'+strResponse.status,objCase.Id,selectedUnitToShow);
               }else {
                   soaUrl = strResponse.url;
               }
        }
        catch(System.Exception excp) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,excp.getMessage() + excp.getLineNumber()));
        }

    }

    public class BookingUnitWrapper {
        public SR_Booking_Unit__c objBU {get;set;}
        public Decimal balanceAsPerSOA {get;set;}
        //
        // public BookingUnitWrapper() {
        //  objBU = new SR_Booking_Unit__c();
        //  overdueAmount = 0.0;
        // }
        public BookingUnitWrapper(SR_Booking_Unit__c objBU, Decimal balanceAsPerSOA) {
            this.objBU = objBU;
            this.balanceAsPerSOA = balanceAsPerSOA;
        }
    }

}