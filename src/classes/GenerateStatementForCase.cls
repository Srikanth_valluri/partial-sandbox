global class GenerateStatementForCase{
    
    WebService static string generateStatementOfAccountforCase(String Regid ){
        String strDocUrl = null;
        strDocUrl = GenerateSOAController.getSOADocument(Regid).url;
        return strDocUrl;
    }
    
     WebService static string generateStatementOfPenaltytforCase(String Regid){
        String strDocUrl = null;
        PenaltyWaiverService.SopResponseObject sopresponseObject = new PenaltyWaiverService.SopResponseObject();
        sopresponseObject  = PenaltyWaiverService.getSOPDocument(Regid);
        if(sopresponseObject.Status.equalsIgnoreCase('S')){
            strDocUrl=sopresponseObject.url;
        }else{
            strDocUrl='Error';
        }
        return strDocUrl;
    }
    
    webservice static void cancelWaiverRequest( Id caseId ) {
        /*list<Case> lstCase = [SELECT Id
                                   , CaseNumber
                                   , Status
                                   , RecordType.Name
                                   , IPMS_Updated__c
                                   , Actual_Approving_Percent__c
                                   , Additional_Details__c
                                   , Approved_Amount__c
                                   , Amount_to_be_waived__c
                                   , Approval_Status__c
                                   , OwnerId
                                FROM Case
                               WHERE Id = :caseId ];
        lstCase[0].Status = 'Cancelled';
        update lstCase[0] ;
        if( lstCase[0].IPMS_Updated__c == true ) {
            Task objTask = TaskUtility.getTask( ( SObject )lstCase[0], 'Penalty waiver request cancelled : Rollback approved amount', 'Finance', 
                                                 lstCase[0].RecordType.Name, system.today().addDays(1) );
            insert objTask ;
        }*/
        
        for( Case objCase : [SELECT Id
                                   , CaseNumber
                                   , Status
                                   , RecordType.Name
                                   , IPMS_Updated__c
                                   , Actual_Approving_Percent__c
                                   , Additional_Details__c
                                   , Approved_Amount__c
                                   , Amount_to_be_waived__c
                                   , Approval_Status__c
                                   , OwnerId
                                FROM Case
                               WHERE Id = :caseId ] ) {
            if( objCase.IPMS_Updated__c == false ) {
                objCase.Status = 'Cancelled';    
                update objCase ;
            }
        }
    }

}