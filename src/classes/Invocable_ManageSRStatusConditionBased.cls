global without sharing class Invocable_ManageSRStatusConditionBased  {
    @InvocableMethod
    public static void manageSRStatusConditionBased(List<Id> stepId) {
        List<NSIBPM__Service_Request__c> serviceRequestList = new List<NSIBPM__Service_Request__c>();
        List<New_Step__c> stepList = new List<New_Step__c>([SELECT Id, Service_Request__c FROM New_Step__c WHERE Id IN :stepId]);

        serviceRequestList = EvaluateCustomCode(stepList);

        update serviceRequestList;
    }

    public static List<NSIBPM__Service_Request__c> EvaluateCustomCode(List<New_Step__c> stepList){
        List<NSIBPM__Service_Request__c> retList = new List<NSIBPM__Service_Request__c>();

        Set<String> stStatusCodes = new Set<String>{'UNDER_REVIEW','AWAITING_FFA_AA'};
        Map<String, Id> mpstatus = new Map<String, Id>();
        for(NSIBPM__SR_Status__c srStatus : [SELECT id, Name, NSIBPM__Code__c FROM NSIBPM__SR_Status__c WHERE NSIBPM__Code__c IN : stStatusCodes]){
            mpstatus.put(srStatus.NSIBPM__Code__c,srStatus.id);
        }


        for (New_Step__c step : stepList) {
            NSIBPM__Service_Request__c objSR = SRUtility.getSRDetails(step.Service_Request__c);
            updateSR(objSR, mpstatus);

            retList.add(objSR);
        }

        return retList;
    }

    public static void updateSR(NSIBPM__Service_Request__c objSR, Map<String, Id> mpstatus){
        if (mpstatus != null && !mpstatus.isempty()) {
            if (objSR.New_Country_of_Sale__c != null || objSR.New_Regeneration_Sites__c !=null) {
                objSR.NSIBPM__External_SR_Status__c = mpstatus.get('AWAITING_FFA_AA');
                objSR.NSIBPM__Internal_SR_Status__c = mpstatus.get('AWAITING_FFA_AA');
            } else {
                objSR.NSIBPM__External_SR_Status__c = mpstatus.get('UNDER_REVIEW');
                objSR.NSIBPM__Internal_SR_Status__c = mpstatus.get('UNDER_REVIEW');
            }
        }
    }
}