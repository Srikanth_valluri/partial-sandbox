@istest

public class DAMAC_SEND_WA_MESSAGE_TEST{


    static testmethod void DAMAC_SEND_WA_MESSAGE_M(){
    
    Account a = new account();
    a.Name = 'Test';
    a.phone = '00123456123';
    insert a;
    system.debug(a.id);
    
    
    
    Whats_App_Message__c parent = new Whats_App_Message__c();
    parent.Unique_Id__c  = '001234';
    parent.Last_Message_Sent__c = 'TEST';
    parent.Outbound_Success__c  = true;
    parent.Unique_Id__c  = 'abc'+string.valueof(system.now());
    insert parent;
    
    Whats_App_Deliverability__c child = new Whats_App_Deliverability__c ();
    child.Whats_App_Message__c = parent.id;
    child.messagebodytext__c = 'Test';
    child.messagedir__c  ='o';
    
    string base64Data = 'Test';
    base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');   
    ContentVersion cv = new ContentVersion();
    cv.ContentLocation = 'S';
    cv.VersionData = EncodingUtil.base64Decode(base64Data);
    cv.Title = 'Test';
    cv.PathOnClient = 'Test';        
    insert cv;    
    
    
    PageReference pageRef = Page.wamessage;    
    Test.setCurrentPage(pageRef); 
    ApexPages.currentPage().getParameters().put('id', a.id);
     
    
    DAMAC_SEND_WA_MESSAGE controller = new DAMAC_SEND_WA_MESSAGE();
    controller.sfrecid = a.id;
    controller.wamessage = 'Test';
    
    controller.sfRec= a;
    controller.sendWAMessage();
    controller.createDeliveryRec(a,userinfo.getuserid(),'Test','Test','Test',true);
    DAMAC_WA_REMOTE.getAttachmentURL(cv.id);
    
    
    }

}