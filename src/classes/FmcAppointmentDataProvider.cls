public without sharing class FmcAppointmentDataProvider implements DataProvider {

    public DataDisplayConfig getData(DataDisplayConfig config) {
        List<FM_Case__c> lstSr = new List<FM_Case__c>();
        if (config.fieldList != NULL) {
            String fields = '';
            for (Map<String,String> fieldMap : config.fieldList) {
                String fieldName = fieldMap.get(DataRendererUtils.FIELD_NAME);
                if (String.isNotBlank(fieldName) && !fields.containsIgnoreCase(fieldName + ',')) {
                    fields += fieldName + ',';
                }
            }

            fields = fields.removeEnd(',');

            String customerAccountId = CustomerCommunityUtils.customerAccountId;


            System.debug( 'FmcCaseDataProvider query = \n' +
                ' SELECT ' + fields + ' FROM FM_Case__c '
                + (String.isBlank(config.filter) ? ' WHERE ' : config.filter + ' AND ')
                + ' RecordType.Name = \'Appointment\' AND '
                + ' (Account__c = ' + (customerAccountId == NULL ? ' NULL ' : '\'' + customerAccountId + '\'')
                + ' OR CreatedById = \'' + UserInfo.getUserId() + '\')'
                + ' ORDER BY CreatedDate DESC'
                + (String.isBlank(config.recordLimit) ? '' : ' LIMIT ' + config.recordLimit)
            );

            lstSr = Database.query(
                ' SELECT ' + fields + ' FROM FM_Case__c '
                + (String.isBlank(config.filter) ? ' WHERE ' : config.filter + ' AND ')
                + ' RecordType.Name = \'Appointment\' AND '
                + ' (Account__c = \'' + customerAccountId + '\'' + ' OR CreatedById = \'' + UserInfo.getUserId() + '\')'
                + ' ORDER BY CreatedDate DESC'
                + (String.isBlank(config.recordLimit) ? '' : ' LIMIT ' + config.recordLimit)
            );
        }
        config.dataList = DataRendererUtils.wrapData(config, lstSr);

        return config;
    }

}