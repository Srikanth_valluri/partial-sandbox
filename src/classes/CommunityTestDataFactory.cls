@istest
public class CommunityTestDataFactory {
    public static String COMMUNITY_USER_PROFILE = 'Customer Community Login User(Use this)';
    public static String COMMUNITY_TENANT_USER_PROFILE = 'Tenant Community Login User';

    public static user createPortalUser(){
        User user;
        try{
            Id profileId = [SELECT id FROM profile WHERE name = :COMMUNITY_USER_PROFILE].id;

            Account objAccount = createPersonAccount();
            insert objAccount;
            Account acc = [SELECT PersonContactId, Party_ID__c FROM Account WHERE Id = :objAccount.Id];
            //System.debug('objAccount-------------'+acc);
            //System.debug('objAccount.PersonContactId-------------'+acc.PersonContactId);
            //System.debug('objAccount.Party_ID__c = ' + acc.Party_ID__c);

            user = new User(alias = 'test123', email='test123@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = profileId, country='United States',IsActive =true,
                    ContactId = acc.PersonContactId,
                    timezonesidkey='America/Los_Angeles', username='tester@noemail.com',IPMS_Employee_ID__c='132',Sales_Office__c='AKOYA',ManagerId = UserInfo.getUserId());

            insert user;
        }catch(Exception e){
            System.debug('Portal User Creation Error = ' + e.getMessage());
        }
        return user;
    }

    public static account createPersonAccount(){
        Id personAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account Accnt = new Account(RecordTypeID = personAccountId,
                                    FirstName = 'Test FName',
                                    LastName = 'Test LName',
                                    PersonMailingStreet = 'test@yahoo.com',
                                    PersonMailingPostalCode = '12345',
                                    PersonMailingCity = 'SFO',
                                    PersonHomePhone = '1234567',
                                    PersonMobilePhone = '12345678',
                                    PersonEmail = 'testPersonmail@mailinator.com',
                                    Mobile__c = '9988559966',
                                    Party_ID__c = '12345');
        return Accnt;
    }

}