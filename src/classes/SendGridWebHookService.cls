/****************************************************************************************************
* Name          : SendGridWebHookService                                                            *
* Description   : Class to get Event Information from SendGrid                                      *
* Created Date  : 19-12-2018                                                                        *
* Created By    : ESPL                                                                              *
* Documentation : https://sendgrid.com/docs/API_Reference/Event_Webhook/event.html                  *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE        COMMENTS                                                    *
* 1.0                                 Initial Draft.                                                *
****************************************************************************************************/

@RestResource(urlMapping='/webhook')
global class SendGridWebHookService{
    
    @HttpPost
    global static String doPost() {
        //list<Error_Log__c> lstErrorLog = new list<Error_Log__c>();
        RestRequest req = RestContext.request;
        if (req != null && req.requestBody != null){
            List<eventWrapper> lstEvents = (List<eventWrapper>)JSON.deserialize(req.requestBody.toString().replace('smtp-id','smtp_id'), List<eventWrapper>.class);
            system.debug('!!!!!!!inside'+lstEvents);     
            map<String, eventWrapper> mapEmailEvent = new map<String,eventWrapper>();
            set<String> setMessageId = new set<String>();
            set<Id> setAccountId = new set<id>();
            list<EmailMessage> lstEmail = new list<EmailMessage>();
            list<Account> lstAccount = new list<Account>();
            
            for (eventWrapper objEvent : lstEvents){
                if (!String.isBlank(objEvent.email)){
                    mapEmailEvent.put(objEvent.email, objEvent);
                }
                if (!String.isBlank(objEvent.sg_message_id)){
                    setMessageId.add(objEvent.sg_message_id.substringBefore('.'));
                }
            }// end of for lstEvents
            
            set<String> setAddress = new set<String>();
            if (mapEmailEvent != null && !mapEmailEvent.keySet().isEmpty() ) {
                setAddress = getNewToAdd(mapEmailEvent.keySet());
            }            
            
            if (setAddress != null && !setAddress.isEmpty() 
                && !setMessageId.isEmpty()){
                for (EmailMessage objActivity : [Select Id
                                                    , Subject
                                                    , ToAddress
                                                    , SendGrid_Status__c
                                                    , Bounce_Reason__c
                                                    , Clicks_Count__c
                                                    , Opens_Count__c
                                                    , SentGrid_MessageId__c
                                                    , Sent_By_Sendgrid__c
                                                    , RelatedToId
                                                 From EmailMessage
                                                 Where ToAddress LIKE : setAddress 
                                                 And SentGrid_MessageId__c IN: setMessageId
                                                 And Sent_By_Sendgrid__c = true]){
                    String SGStatus = mapEmailEvent.get(objActivity.ToAddress).event;
                    if (!String.isBlank(SGStatus) ){
                        objActivity.SendGrid_Status__c = SGStatus;
                    }
                    if (SGStatus == 'bounce') {
                        objActivity.Bounce_Reason__c = mapEmailEvent.get(objActivity.ToAddress).reason;
                        String relatedId = objActivity.RelatedToId;
                        if (relatedId.startsWith('001')){
                            setAccountId.add(objActivity.RelatedToId);
                        }
                    }
                    if (SGStatus == 'open') {
                        if (objActivity.Opens_Count__c == null) {
                            objActivity.Opens_Count__c = 1;
                        } else {
                            objActivity.Opens_Count__c = objActivity.Opens_Count__c + 1;
                        }
                    }
                    if (SGStatus == 'click') {
                        if (objActivity.Clicks_Count__c == null) {
                            objActivity.Clicks_Count__c = 1;
                        } else {
                            objActivity.Clicks_Count__c = objActivity.Clicks_Count__c + 1;
                        }
                    }
                    lstEmail.add(objActivity);                      
                }// end of for
            }
            
            if (!lstEmail.isEmpty()){
                update lstEmail;
            }    
            
            if (!setAccountId.isEmpty()){
                for (Account objAccount : [Select Id
                                                , ZBEmailStatus__c
                                           From Account
                                           Where Id IN: setAccountId]){
                    objAccount.ZBEmailStatus__c = 'Invalid';
                    lstAccount.add(objAccount);     
                }
            }
            
            if (!lstAccount.isEmpty()){
                update lstAccount;
            }
               
            return '200';
        } else {
            return '400';
        }
    }
    
    public static Set<String> getNewToAdd( Set<String> mapkeyset  ) {
        Set<String> setNewToAddress = new Set<String>();
        if( !mapkeyset.isEmpty() && mapkeyset.size() > 0 ) {
            String strNewToAdd = '';
            for( String strToAdd : mapkeyset ) {
                if( string.isNotBlank( strToAdd ) ) {
                    strNewToAdd = '%'+strToAdd+'%';
                    setNewToAddress.add(strNewToAdd);
                }
            }
        }
        return setNewToAddress;
    }
    
    global class eventWrapper {
        public String email;
        public Integer timestamp;
        public String smtp_id;
        public String event;
        public String category;
        public String sg_event_id;
        public String sg_message_id;
        public String response;
        public String attempt;
        public String useragent;
        public String ip;
        public String url;
        public String reason;
        public String status;
        public Integer asm_group_id;  
        public String sendsubject;       
    }
    
    /*public static List<eventWrapper> parse(String json) {
        return (List<eventWrapper>) System.JSON.deserialize(json, List<eventWrapper>.class);
    }*/
    
}