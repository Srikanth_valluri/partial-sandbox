@isTest
private class CallLogTranscriptTest{
    
    
    @testSetup
    static void setup() {
    
    }
    
    @isTest static void triggerTestBulk() {
        List<Call_Log__c> listCallLog = new List<Call_Log__c>();
        for(Integer i=0; i<100; i++){
            Call_Log__c objCallLog = new Call_Log__c();
            objCallLog.Call_Recording_URL__c = '34534dfsgabds';
            objCallLog.Calling_Number__c = '342354234';
            //objCallLog.Currency = 'AED UAE Dirham';
            listCallLog.add(objCallLog);
        }
        System.Test.startTest();
        if(!listCallLog.isEmpty()){
            insert listCallLog;
        }
        if (System.Test.isRunningTest()) Test.setMock(HttpCalloutMock.class, new MockCallLogTranscript());
        System.Test.stopTest();
    }
    
    @isTest static void controllerTest() {
        
        Call_Log__c objCallLog = new Call_Log__c();
        objCallLog.Call_Recording_URL__c = '34534dfsgabds';
        objCallLog.Calling_Number__c = '342354234';
        //objCallLog.Currency = 'AED UAE Dirham';
       
        insert objCallLog;
        
        ApexPages.currentPage().getParameters().put('id', objCallLog.Id);
        
        System.Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(objCallLog);
        CallLogPageController objCallLogPageController = new CallLogPageController(sc);
        if (System.Test.isRunningTest()) Test.setMock(HttpCalloutMock.class, new MockCallLogTranscript());
        CallLogPageController.sendToPhpUtility();
        System.Test.stopTest();
    }
    
    @isTest static void controllerTest11() {
        
        Call_Log__c objCallLog = new Call_Log__c();
        objCallLog.Call_Recording_URL__c = '34534dfsgabds';
        objCallLog.Calling_Number__c = '342354234';
        //objCallLog.Currency = 'AED UAE Dirham';
       
        insert objCallLog;
        
        //ApexPages.currentPage().getParameters().put('id', objCallLog.Id);
        
        System.Test.startTest();
        //ApexPages.StandardController sc = new ApexPages.StandardController(objCallLog);
        CallLogTranscriptHelper objCallLogTranscriptHelper = new CallLogTranscriptHelper();
        //if (System.Test.isRunningTest()) Test.setMock(HttpCalloutMock.class, new MockCallLogTranscript());
        CallLogTranscriptHelper.sendToPhpUtility(new List<Call_Log__c>{objCallLog});
        CallLogTranscriptHelper.sendData(new set<Id>{objCallLog.Id});
        System.Test.stopTest();
    }   
}