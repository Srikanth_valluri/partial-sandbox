public class FM_EmailMessageBatchScheduler implements Schedulable {

    public static String cronExp = '0 0 8 * * ?';

    public static String jobName = 'FM_EmailMessageBatch';
    public void scheduleMyJob() {
      FM_EmailMessageBatchScheduler batchObj = new FM_EmailMessageBatchScheduler();
          System.schedule(jobName, cronExp, batchObj);
    }
    public void execute(SchedulableContext sc) {
        FM_EmailMessageBatch batchObj = new FM_EmailMessageBatch();
        Database.executeBatch(batchObj, 200);
    }
}