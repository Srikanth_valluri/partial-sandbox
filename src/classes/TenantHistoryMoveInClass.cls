public with sharing class TenantHistoryMoveInClass {
	@InvocableMethod
	public static void UpdateTenantHistory(List<FM_Case__c> lstFMCases) {
		// Map<Id, String> mapFMCase_Ids = new Map<Id, String>();
		Set<ID> setBUIds = new Set<ID>();
		Set<ID> setAccountIds = new Set<ID>();
		Set<ID> setTenantIds = new Set<ID>();
		List<Tenant_History__c> lstTenantHistoryUpdate = new List<Tenant_History__c>();
		Map<Id, SObject> sObjectMap = new Map<Id, SObject>();
		for(FM_Case__c obj: lstFMCases) {
			// mapFMCase_Ids.put(obj.Booking_Unit__c, obj.Account__c+obj.Tenant__c);
			setAccountIds.add(obj.Account__c);
			setTenantIds.add(obj.Tenant__c);
			setBUIds.add(obj.Booking_Unit__c);
		}
		//Get Tenant History
		List<Tenant_History__c> lstTenantHistory = new List<Tenant_History__c>();
		Map<ID, Tenant_History__c> mapBU_TenantHistory = new Map<ID, Tenant_History__c>();
		lstTenantHistory = [SELECT Booking_Unit__c,
									Id,
									Lease_Start_Date__c,
									Lease_End_Date__c,
									Tenant__c,
									Owner__c
							FROM Tenant_History__c
							WHERE Booking_Unit__c IN: setBUIds
							AND Owner__c IN : setAccountIds
							AND Tenant__c IN : setTenantIds
							// AND Is_Active__c = True
							ORDER BY CreatedDate DESC];
		for(Tenant_History__c obj : lstTenantHistory) {
			if(! mapBU_TenantHistory.containsKey(obj.Booking_Unit__c)) {
				mapBU_TenantHistory.PUT(obj.Booking_Unit__c, obj);
			}
		}
		for(FM_Case__c obj: lstFMCases) {
			if(mapBU_TenantHistory.containsKey(obj.Booking_Unit__c)) {
				// get Tenant History
				Tenant_History__c objTH = mapBU_TenantHistory.get(obj.Booking_Unit__c);
				if(objTH != NULL && objTH.Owner__c == obj.Account__c
					&& objTH.Tenant__c == obj.Tenant__c
					&& obj.Actual_move_in_date__c >= objTH.Lease_Start_Date__c
					&& obj.Actual_move_in_date__c <= objTH.Lease_End_Date__c) {
						objTH.Move_in_date__c = obj.Actual_move_in_date__c;
						objTH.Is_Active__c = true;
						lstTenantHistoryUpdate.add(objTH);
					}
			}
		}//for
		Update lstTenantHistoryUpdate;
	}//method
}//Class