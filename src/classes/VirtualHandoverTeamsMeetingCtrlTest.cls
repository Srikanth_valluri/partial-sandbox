/*-------------------------------------------------------------------------------------------------
Description: Test class for VirtualHandoverTeamsMeetingController

    ============================================================================================================================
        Version | Date(DD-MM-YYYY) | Last Modified By | Comments
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 12-08-2020       | Akshata Anavekar    | 1. Initial draft
   =============================================================================================================================
*/
@isTest
public class VirtualHandoverTeamsMeetingCtrlTest {
    private class VirtualHandoverMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            
            HTTPResponse res = new HTTPResponse();
            res.setBody('{"token_type": "Bearer","scope": "Calendars.Read","expires_in": 3599,"ext_expires_in": 3599,'
                                + '"access_token": " accesstoken","refresh_token": "refreshtoken"}');
            res.setStatusCode(200);
            return res;
            
        }
    }
    
    @isTest static void testcreateMeeting() {
    	API_Integration_Value__c objApiInt = new API_Integration_Value__c();
        objApiInt.name = 'Virtual Handover Graph API';
        objApiInt.Endpoint_URL__c = 'https://login.microsoftonline.com/';
        objApiInt.Tenant_Id__c = '57e43429-428a-4973-9720-e6907ae94def';
        objApiInt.Client_Id__c = 'b8f6c4e9-d493-4a2f-96f3-7d4ea68c1540';
        objApiInt.Client_Secret__c = 'f18Ocz6Phrng4OyW_iC_.R0z.e85a38qy4';
        objApiInt.Scope__c = 'Offline_access%20Calendars.Read%20Calendars.ReadWrite%20OnlineMeetings.Read%20OnlineMeetings.ReadWrite%20user.Read';
        objApiInt.Refresh_Token__c = 'OAQABAAAAAAAGV_bv21oQQ4ROqh0_1-tA4P4IswFvPEVC1Y6u-NplxxTXklGFdsBBIGMYyccIgg-66bzebSXv-4NR1YEglPxSeUQPojaZyFC8kQDkUdOlXVm9oz9oRxWt1YpJFRqpKnOF1zzK9ZZkaVdn2YpOXgtuNePZZdIOhGD-aHk60havFh8RyTVjIAOU0xMjzRqOrZiz0PQOL0DQmX4PMeyNOe_S6iOlI29kk-OaUvBPwPld1Ux9M_Yva-bzNw7Fs8gGRRE_-babB8oILd8VqWPfF_tQ0CQwoIclK2xOj9OGQpEGfN514fLWw3J9PLG1gL9wb0mnLkky3oVj6powQ-WpwDYLL_6AkM0Ovs4omhjdhzeLYmR_Iblw0uhoV8-Ekpjep10wRQElKoYaMwKrhQ6bntEIXqFJ1IgedeBC3soXuCLFaKqCgP_nHb5NZo5ePOiO16kjSAsV2dnBKkjmHDC52mtm1qlFYP-nVFT2JkTHyNCeLMqNbytTJmtt51vTzzgU3eq2VPx1CPUdjw9vxQsbtbt5c49OkaGw-jszWvlTufife1Gmoy1vdkaN-Ol64dyCscfTF2zhbP4p2BEa2pIutsA8h4rV5ov79y-tj2xpPCacsAB2oz-la_QHa_n5aWP3dIuyXhVQ1_uitj8TWNjR2Nx1Vk2-Lz6AoJneGTHWkYQU1zvLh0cOuiDn1imO3pIiEPmtedqNqYu_bI6xsUnaMov5buNbmMjp5AttQuf9q_gRowJwAhPkWxZuLhdIXDA-bx5QaLFVXYRAH-8ym-giz9-vbYm7rmWm-A45oGY-STHpFPzT-8jiE31GspES6mCbQZ_926_iLm1M7gcK_MdsAkMpq5Y1LnsXO-cz0BGltcMnmXDYcrw5JvWlNF-khfSc_gvnl_0SbLZs-JsJii_10OIl0hLkQsaURecr0YZHDwREHsNcId3kqKW1uTA6PKaoyGStIuUBuXwhGREWpTIeK-TpUpUKqnWDF_i_pzJ2wH5AdGvddGgEy0ghyeaMAo8kCWkM4MEy8C-6v6dXiHMvEVdV0RsQkCbzvnrlB_9MV8MSGUxrNKGmm6ot2_7EMRnG4Ff8Hzn95QJgjLtXIaLMVOOsyeTbdoo0G9h_lcfoA0QPwSAA';
        objApiInt.Meeting_EndPoint_URL__c = 'https://graph.microsoft.com/v1.0/me/onlineMeetings';
        objApiInt.Event_Creation_URL__c = 'https://graph.microsoft.com/v1.0/me/calendar/events';
        objApiInt.Redirect_URI__c = 'https://damacholding--crmuatpro.my.salesforce.com/services/apexrest/DLDTrusteeMeeting/';
        objApiInt.Username__c = 'DLDTrustee.Meeting@damacproperties.com';
        insert objApiInt;
        
        API_Integration_Value__c objApiInt1 = new API_Integration_Value__c();
        objApiInt1.name = 'Virtual Handover Graph API';
        objApiInt1.Endpoint_URL__c = 'https://login.microsoftonline.com/';
        objApiInt1.Tenant_Id__c = '57e43429-428a-4973-9720-e6907ae94def';
        objApiInt1.Client_Id__c = 'b8f6c4e9-d493-4a2f-96f3-7d4ea68c1540';
        objApiInt1.Client_Secret__c = 'f18Ocz6Phrng4OyW_iC_.R0z.e85a38qy4';
        objApiInt1.Scope__c = 'Offline_access%20Calendars.Read%20Calendars.ReadWrite%20OnlineMeetings.Read%20OnlineMeetings.ReadWrite%20user.Read';
        objApiInt1.Refresh_Token__c = 'OAQABAAAAAAAGV_bv21oQQ4ROqh0_1-tA4P4IswFvPEVC1Y6u-NplxxTXklGFdsBBIGMYyccIgg-66bzebSXv-4NR1YEglPxSeUQPojaZyFC8kQDkUdOlXVm9oz9oRxWt1YpJFRqpKnOF1zzK9ZZkaVdn2YpOXgtuNePZZdIOhGD-aHk60havFh8RyTVjIAOU0xMjzRqOrZiz0PQOL0DQmX4PMeyNOe_S6iOlI29kk-OaUvBPwPld1Ux9M_Yva-bzNw7Fs8gGRRE_-babB8oILd8VqWPfF_tQ0CQwoIclK2xOj9OGQpEGfN514fLWw3J9PLG1gL9wb0mnLkky3oVj6powQ-WpwDYLL_6AkM0Ovs4omhjdhzeLYmR_Iblw0uhoV8-Ekpjep10wRQElKoYaMwKrhQ6bntEIXqFJ1IgedeBC3soXuCLFaKqCgP_nHb5NZo5ePOiO16kjSAsV2dnBKkjmHDC52mtm1qlFYP-nVFT2JkTHyNCeLMqNbytTJmtt51vTzzgU3eq2VPx1CPUdjw9vxQsbtbt5c49OkaGw-jszWvlTufife1Gmoy1vdkaN-Ol64dyCscfTF2zhbP4p2BEa2pIutsA8h4rV5ov79y-tj2xpPCacsAB2oz-la_QHa_n5aWP3dIuyXhVQ1_uitj8TWNjR2Nx1Vk2-Lz6AoJneGTHWkYQU1zvLh0cOuiDn1imO3pIiEPmtedqNqYu_bI6xsUnaMov5buNbmMjp5AttQuf9q_gRowJwAhPkWxZuLhdIXDA-bx5QaLFVXYRAH-8ym-giz9-vbYm7rmWm-A45oGY-STHpFPzT-8jiE31GspES6mCbQZ_926_iLm1M7gcK_MdsAkMpq5Y1LnsXO-cz0BGltcMnmXDYcrw5JvWlNF-khfSc_gvnl_0SbLZs-JsJii_10OIl0hLkQsaURecr0YZHDwREHsNcId3kqKW1uTA6PKaoyGStIuUBuXwhGREWpTIeK-TpUpUKqnWDF_i_pzJ2wH5AdGvddGgEy0ghyeaMAo8kCWkM4MEy8C-6v6dXiHMvEVdV0RsQkCbzvnrlB_9MV8MSGUxrNKGmm6ot2_7EMRnG4Ff8Hzn95QJgjLtXIaLMVOOsyeTbdoo0G9h_lcfoA0QPwSAA';
        objApiInt1.Meeting_EndPoint_URL__c = 'https://graph.microsoft.com/v1.0/me/onlineMeetings';
        objApiInt1.Event_Creation_URL__c = 'https://graph.microsoft.com/v1.0/me/calendar/events';
        objApiInt1.Redirect_URI__c = 'https://damacholding--crmuatpro.my.salesforce.com/services/apexrest/virtualhandover/';
        objApiInt1.Username__c = 'virtual.handover@damacproperties.com';
        insert objApiInt1;
        
        TriggerOnOffCustomSetting__c objCustomSetting = new TriggerOnOffCustomSetting__c();
        objCustomSetting.Name = 'CallingListTrigger';
        objCustomSetting.OnOffCheck__c = false;
        insert objCustomSetting;
        
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
    	insert objAccount;
        
        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Account__c = objAccount.Id;
        objCallingList.Appointment_Start_DateTime__c = System.now();
        objCallingList.Appointment_End_DateTime__c = System.now()+1;
        insert objCallingList;
        
        Test.setMock(HttpCalloutMock.class, new VirtualHandoverMock());
        Test.startTest();
            ApexPages.StandardController objStdController = new ApexPages.StandardController(objCallingList);
            VirtualHandoverTeamsMeetingController objController = new VirtualHandoverTeamsMeetingController(objStdController);
            objController.createMeeting();
        Test.stopTest();
    }
    
    @isTest static void testUpdateMeeting() {
    	API_Integration_Value__c objApiInt = new API_Integration_Value__c();
        objApiInt.name = 'Virtual Handover Graph API';
        objApiInt.Endpoint_URL__c = 'https://login.microsoftonline.com/';
        objApiInt.Tenant_Id__c = '57e43429-428a-4973-9720-e6907ae94def';
        objApiInt.Client_Id__c = 'b8f6c4e9-d493-4a2f-96f3-7d4ea68c1540';
        objApiInt.Client_Secret__c = 'f18Ocz6Phrng4OyW_iC_.R0z.e85a38qy4';
        objApiInt.Scope__c = 'Offline_access%20Calendars.Read%20Calendars.ReadWrite%20OnlineMeetings.Read%20OnlineMeetings.ReadWrite%20user.Read';
        objApiInt.Refresh_Token__c = 'OAQABAAAAAAAGV_bv21oQQ4ROqh0_1-tA4P4IswFvPEVC1Y6u-NplxxTXklGFdsBBIGMYyccIgg-66bzebSXv-4NR1YEglPxSeUQPojaZyFC8kQDkUdOlXVm9oz9oRxWt1YpJFRqpKnOF1zzK9ZZkaVdn2YpOXgtuNePZZdIOhGD-aHk60havFh8RyTVjIAOU0xMjzRqOrZiz0PQOL0DQmX4PMeyNOe_S6iOlI29kk-OaUvBPwPld1Ux9M_Yva-bzNw7Fs8gGRRE_-babB8oILd8VqWPfF_tQ0CQwoIclK2xOj9OGQpEGfN514fLWw3J9PLG1gL9wb0mnLkky3oVj6powQ-WpwDYLL_6AkM0Ovs4omhjdhzeLYmR_Iblw0uhoV8-Ekpjep10wRQElKoYaMwKrhQ6bntEIXqFJ1IgedeBC3soXuCLFaKqCgP_nHb5NZo5ePOiO16kjSAsV2dnBKkjmHDC52mtm1qlFYP-nVFT2JkTHyNCeLMqNbytTJmtt51vTzzgU3eq2VPx1CPUdjw9vxQsbtbt5c49OkaGw-jszWvlTufife1Gmoy1vdkaN-Ol64dyCscfTF2zhbP4p2BEa2pIutsA8h4rV5ov79y-tj2xpPCacsAB2oz-la_QHa_n5aWP3dIuyXhVQ1_uitj8TWNjR2Nx1Vk2-Lz6AoJneGTHWkYQU1zvLh0cOuiDn1imO3pIiEPmtedqNqYu_bI6xsUnaMov5buNbmMjp5AttQuf9q_gRowJwAhPkWxZuLhdIXDA-bx5QaLFVXYRAH-8ym-giz9-vbYm7rmWm-A45oGY-STHpFPzT-8jiE31GspES6mCbQZ_926_iLm1M7gcK_MdsAkMpq5Y1LnsXO-cz0BGltcMnmXDYcrw5JvWlNF-khfSc_gvnl_0SbLZs-JsJii_10OIl0hLkQsaURecr0YZHDwREHsNcId3kqKW1uTA6PKaoyGStIuUBuXwhGREWpTIeK-TpUpUKqnWDF_i_pzJ2wH5AdGvddGgEy0ghyeaMAo8kCWkM4MEy8C-6v6dXiHMvEVdV0RsQkCbzvnrlB_9MV8MSGUxrNKGmm6ot2_7EMRnG4Ff8Hzn95QJgjLtXIaLMVOOsyeTbdoo0G9h_lcfoA0QPwSAA';
        objApiInt.Meeting_EndPoint_URL__c = 'https://graph.microsoft.com/v1.0/me/onlineMeetings';
        objApiInt.Event_Creation_URL__c = 'https://graph.microsoft.com/v1.0/me/calendar/events';
        objApiInt.Redirect_URI__c = 'https://damacholding--crmuatpro.my.salesforce.com/services/apexrest/DLDTrusteeMeeting/';
        objApiInt.Username__c = 'DLDTrustee.Meeting@damacproperties.com';
        insert objApiInt;
        
        API_Integration_Value__c objApiInt1 = new API_Integration_Value__c();
        objApiInt1.name = 'Virtual Handover Graph API';
        objApiInt1.Endpoint_URL__c = 'https://login.microsoftonline.com/';
        objApiInt1.Tenant_Id__c = '57e43429-428a-4973-9720-e6907ae94def';
        objApiInt1.Client_Id__c = 'b8f6c4e9-d493-4a2f-96f3-7d4ea68c1540';
        objApiInt1.Client_Secret__c = 'f18Ocz6Phrng4OyW_iC_.R0z.e85a38qy4';
        objApiInt1.Scope__c = 'Offline_access%20Calendars.Read%20Calendars.ReadWrite%20OnlineMeetings.Read%20OnlineMeetings.ReadWrite%20user.Read';
        objApiInt1.Refresh_Token__c = 'OAQABAAAAAAAGV_bv21oQQ4ROqh0_1-tA4P4IswFvPEVC1Y6u-NplxxTXklGFdsBBIGMYyccIgg-66bzebSXv-4NR1YEglPxSeUQPojaZyFC8kQDkUdOlXVm9oz9oRxWt1YpJFRqpKnOF1zzK9ZZkaVdn2YpOXgtuNePZZdIOhGD-aHk60havFh8RyTVjIAOU0xMjzRqOrZiz0PQOL0DQmX4PMeyNOe_S6iOlI29kk-OaUvBPwPld1Ux9M_Yva-bzNw7Fs8gGRRE_-babB8oILd8VqWPfF_tQ0CQwoIclK2xOj9OGQpEGfN514fLWw3J9PLG1gL9wb0mnLkky3oVj6powQ-WpwDYLL_6AkM0Ovs4omhjdhzeLYmR_Iblw0uhoV8-Ekpjep10wRQElKoYaMwKrhQ6bntEIXqFJ1IgedeBC3soXuCLFaKqCgP_nHb5NZo5ePOiO16kjSAsV2dnBKkjmHDC52mtm1qlFYP-nVFT2JkTHyNCeLMqNbytTJmtt51vTzzgU3eq2VPx1CPUdjw9vxQsbtbt5c49OkaGw-jszWvlTufife1Gmoy1vdkaN-Ol64dyCscfTF2zhbP4p2BEa2pIutsA8h4rV5ov79y-tj2xpPCacsAB2oz-la_QHa_n5aWP3dIuyXhVQ1_uitj8TWNjR2Nx1Vk2-Lz6AoJneGTHWkYQU1zvLh0cOuiDn1imO3pIiEPmtedqNqYu_bI6xsUnaMov5buNbmMjp5AttQuf9q_gRowJwAhPkWxZuLhdIXDA-bx5QaLFVXYRAH-8ym-giz9-vbYm7rmWm-A45oGY-STHpFPzT-8jiE31GspES6mCbQZ_926_iLm1M7gcK_MdsAkMpq5Y1LnsXO-cz0BGltcMnmXDYcrw5JvWlNF-khfSc_gvnl_0SbLZs-JsJii_10OIl0hLkQsaURecr0YZHDwREHsNcId3kqKW1uTA6PKaoyGStIuUBuXwhGREWpTIeK-TpUpUKqnWDF_i_pzJ2wH5AdGvddGgEy0ghyeaMAo8kCWkM4MEy8C-6v6dXiHMvEVdV0RsQkCbzvnrlB_9MV8MSGUxrNKGmm6ot2_7EMRnG4Ff8Hzn95QJgjLtXIaLMVOOsyeTbdoo0G9h_lcfoA0QPwSAA';
        objApiInt1.Meeting_EndPoint_URL__c = 'https://graph.microsoft.com/v1.0/me/onlineMeetings';
        objApiInt1.Event_Creation_URL__c = 'https://graph.microsoft.com/v1.0/me/calendar/events';
        objApiInt1.Redirect_URI__c = 'https://damacholding--crmuatpro.my.salesforce.com/services/apexrest/virtualhandover/';
        objApiInt1.Username__c = 'virtual.handover@damacproperties.com';
        insert objApiInt1;
        
        TriggerOnOffCustomSetting__c objCustomSetting = new TriggerOnOffCustomSetting__c();
        objCustomSetting.Name = 'CallingListTrigger';
        objCustomSetting.OnOffCheck__c = false;
        insert objCustomSetting;
        
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
    	insert objAccount;
        
        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Account__c = objAccount.Id;
        objCallingList.Appointment_Start_DateTime__c = System.now();
        objCallingList.Appointment_End_DateTime__c = System.now()+1;
        objCallingList.Remarks_Vivek__c = 'v-rohit@damacgroup.com';
        insert objCallingList;
        
        Test.setMock(HttpCalloutMock.class, new VirtualHandoverMock());
        Test.startTest();
            ApexPages.StandardController objStdController = new ApexPages.StandardController(objCallingList);
            VirtualHandoverTeamsMeetingController objController = new VirtualHandoverTeamsMeetingController(objStdController);
            objController.isUpdate = true;    
            objController.updateMeeting(System.now(),System.now(),'testEmail','testSubject','testBody',objCallingList);
        	objController.updateSuccess = true;
        	objController.updateEvent();
        Test.stopTest();
    }
}