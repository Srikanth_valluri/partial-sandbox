/**
 * @File Name          : CreateTaskForComplaint.cls
 * @Description        : Called from "Close FM Case on Task Close v1.1" process 
                            builder on closure of task verify all details
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 11/28/2019, 12:54:59 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    11/28/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public class CreateTaskForComplaint {
    
    @InvocableMethod(label='' description='')
    public static void onTaskClosureForComplaint (List<Id> lstIDs) {
        System.debug('lstIDs--------'+lstIDs);
        List<FM_Case__c> lstFmCase = new List<FM_Case__c>();
        lstFmCase = [
             SELECT  Id, Unit_Name__c, Booking_Unit__c, Booking_Unit__r.Inventory__c, OwnerId, CurrencyIsoCode,
                        Booking_Unit__r.Inventory__r.Building_Location__c, Request_Type__c, Submitted__c
                FROM    FM_Case__c
                WHERE   Id IN :lstIDs
                 AND Request_Type__c = : Label.Service_Charge_Enquiry
        ];
        System.debug('lstFmCase = ' + lstFmCase);

        List<Task> lstTask = new List<Task>();
        for (FM_Case__c fmCase : lstFmCase) {

            Task taskInstance = new Task();
            taskInstance.Priority = 'Normal';
            taskInstance.WhatId = fmCase.Id;
            taskInstance.Status = 'Not Started';
            taskInstance.Process_Name__c = fmCase.Request_Type__c;
            taskInstance.Subject = Label.Subject_is_Inform_Customer_for_Task;
            taskInstance.ActivityDate = Date.today();
            taskInstance.OwnerId = fmCase.ownerId;
            taskInstance.CurrencyIsoCode = fmCase.CurrencyIsoCode;
            taskInstance.Type = 'Portal';
              lstTask.add(taskInstance);
         }

        System.debug('lstTask--------'+lstTask);
        if(!lstTask.isEmpty()){
             insert lstTask;
        }
       
    }
}