/**
 * Craig - Added Status condition in Case query (NOT - New, Draft Request, Cancelled, In progress) - by Kanu (04-10-2019)
 */


public without sharing class SRDataProvider implements DataProvider {

    public DataDisplayConfig getData(DataDisplayConfig config) {
        Set<String> setField = Schema.SobjectType.Case.fields.getMap().keySet();
        List<Case> lstSr = new List<Case>();
        if (config.fieldList != NULL) {
            String fields = '';
            for (Map<String,String> fieldMap : config.fieldList) {
                String fieldName = fieldMap.get(DataRendererUtils.FIELD_NAME);
                if (String.isNotBlank(fieldName)
                    && (setField.contains(fieldName.toLowerCase()) || fieldName.contains('.'))
                    && !fields.containsIgnoreCase(fieldName + ',')
                ) {
                    fields += fieldName + ',';
                }
            }

            for (Map<String,String> fieldMap : config.detailFieldList) {
                String fieldName = fieldMap.get(DataRendererUtils.FIELD_NAME);
                if (String.isNotBlank(fieldName)
                    && (setField.contains(fieldName.toLowerCase()) || fieldName.contains('.'))
                    && !fields.containsIgnoreCase(fieldName + ',')
                ) {
                    fields += fieldName + ',';
                }
            }

            fields = fields.removeEnd(',');

            String customerContactId = CustomerCommunityUtils.customerContactId;
            String customerAccountId = CustomerCommunityUtils.customerAccountId;
            Id casePenaltyWaiverRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Penalty Waiver').getRecordTypeId();
            Id caseEmailRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
            Id caseNonSRCaseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Non SR case').getRecordTypeId();
            Id caseParentHandover = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover Parent').getRecordTypeId();

            System.debug( 'SRDataProvider query = \n' +
                ' SELECT ' + fields + ' FROM Case '
                + (String.isBlank(config.filter) ? ' WHERE ' : config.filter + ' AND ')
                + ' RecordTypeId != \'' + casePenaltyWaiverRTId + '\'' + ' AND '
                + ' (AccountId = ' + (customerAccountId == NULL ? ' NULL ' : '\'' + customerContactId + '\'')
                + ' OR CreatedById = \'' + UserInfo.getUserId() + '\')'
                + ' AND Status != \'New\' '
                + ' AND Status != \'Draft Request\' '
                + ' AND Status != \'Cancelled\' '
                + ' AND Status != \'In Progress\' '
                + (String.isBlank(config.recordLimit) ? '' : ' LIMIT ' + config.recordLimit)
            );

            lstSr = Database.query(
                ' SELECT ' + fields + ' FROM Case '
                + (String.isBlank(config.filter) ? ' WHERE ' : config.filter + ' AND ')
                + ' RecordTypeId != \'' + casePenaltyWaiverRTId + '\'' + ' AND '
                + ' RecordTypeId != \'' + caseEmailRTId + '\'' + ' AND '
                + ' RecordTypeId != \'' + caseNonSRCaseRTId + '\'' + ' AND '
                + ' RecordTypeId != \'' + caseParentHandover + '\'' + ' AND '
                + ' (AccountId = \'' + customerAccountId + '\'' + ' OR CreatedById = \'' + UserInfo.getUserId() + '\')'
                + ' AND Status != \'New\' '
                + ' AND Status != \'Draft Request\' '
                + ' AND Status != \'Cancelled\' '
                + ' AND Status != \'In Progress\' '
                + (String.isBlank(config.recordLimit) ? '' : ' LIMIT ' + config.recordLimit)
            );
        }
        config.dataList = DataRendererUtils.wrapData(config, lstSr);
        System.debug( 'SRDataProvider LIST>>> ' +config.dataList );
        return config;
    }
}