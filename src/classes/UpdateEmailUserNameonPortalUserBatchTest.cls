@isTest
private class UpdateEmailUserNameonPortalUserBatchTest {

    static testMethod void test_UpdateCustomerFlagonCallingListBatch () {

        UpdateEmailUserNameonPortalUserBatch batchCls = new UpdateEmailUserNameonPortalUserBatch ();

        Id profileId = [select id from profile where name = 'Customer Community Login User(Use this)'].id;

        Account objAccount = CommunityTestDataFactory.CreatePersonAccount();
        objAccount.Email__pc = 'test@user.com';
        objAccount.PersonEmail = '';
        insert objAccount;
        
        Account acc = [Select Party_ID__c,PersonContactId From Account Where Id = :objAccount.Id];
         
        User user = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = profileId, country='United States',IsActive =true,
                ContactId = acc.PersonContactId,Party_Id__c=acc.Party_ID__c,
                timezonesidkey='America/Los_Angeles', username='tester@noemail1.com');

        insert user;
        

    
        System.runAs(user) {
            Test.startTest();
                Database.executeBatch(batchCls);
            Test.stopTest();
        }       

        

    }
    
    static testMethod void test_UpdateCustomerFlagonCallingListBatch2 () {

        UpdateEmailUserNameonPortalUserBatch batchCls = new UpdateEmailUserNameonPortalUserBatch ();

        Id profileId = [select id from profile where name = 'Customer Community Login User(Use this)'].id;

        Account objAccount = CommunityTestDataFactory.CreatePersonAccount();
        objAccount.Email__pc = 'test@user.com';
        objAccount.PersonEmail = 'test11@user.com';
        insert objAccount;
        
        Account acc = [Select Party_ID__c,PersonContactId From Account Where Id = :objAccount.Id];
         
        User user = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = profileId, country='United States',IsActive =true,
                ContactId = acc.PersonContactId,Party_Id__c=acc.Party_ID__c,
                timezonesidkey='America/Los_Angeles', username='tester@noemail1.com');

        insert user;
        
        System.runAs(user) {
            Test.startTest();
                Database.executeBatch(batchCls);
            Test.stopTest();
        }
    }
    
    static testMethod void test_UpdateEmailOnPortalCxBatch () {

        UpdateEmailOnPortalCxBatch batchCls = new UpdateEmailOnPortalCxBatch ();

        Id profileId = [select id from profile where name = 'Customer Community Login User(Use this)'].id;

        Account objAccount = CommunityTestDataFactory.CreatePersonAccount();
        objAccount.Email__pc = 'test@user.com';
        objAccount.PersonEmail = 'test11@user.com';
        insert objAccount;
        
        Account acc = [Select Party_ID__c,PersonContactId From Account Where Id = :objAccount.Id];
         
        User user = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = profileId, country='United States',IsActive =true,
                ContactId = acc.PersonContactId,Party_Id__c=acc.Party_ID__c,
                timezonesidkey='America/Los_Angeles', username='tester@noemail1.com');

        insert user;
        
        System.runAs(user) {
            Test.startTest();
                Database.executeBatch(batchCls);
            Test.stopTest();
        }
    }
    
    
}