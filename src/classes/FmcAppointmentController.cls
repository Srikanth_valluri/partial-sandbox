public without sharing class FmcAppointmentController {

    public  FM_Case__c          sr              {get; set;}
    public  List<SelectOption>  units           {get; set;}

    private static final String SR_SUBMIT_SUCCESS = 'Your Appointment has been booked successfully';
    private static final String GENERIC_SAVE_ERROR = 'There was an error while booking your appointment. Please try again later.';

    public FmcAppointmentController() {
        if (FmcUtils.isCurrentView('NewAppointment')) {
            initializeVariables();
        }
    }

    private void initializeVariables() {
        sr = new FM_Case__c(
            Status__c = 'New',
            Origin__c = 'Portal',
            RecordTypeId = getAppointmentRecordTypeId(),
            Request_Type__c = 'Appointment',
            Account__c = CustomerCommunityUtils.customerAccountId
        );
        units = getUnitOptions();
    }

    private Id getAppointmentRecordTypeId() {
        return Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName()
                                        .get('Appointment').getRecordTypeId();
    }

    private List<SelectOption> getUnitOptions() {
        List<SelectOption> lstUnitOptions = new List<SelectOption>();
        for (Booking_Unit__c unit : FmcUtils.queryUnitsForAccount(
                CustomerCommunityUtils.customerAccountId,
                'Id, Name, Unit_Name__c, Inventory__c, Inventory__r.Building_Location__c, '
                + 'Inventory__r.Building_Location__r.Name, Inventory__r.Building_Location__r.Building_Name__c'
        )) {
            String unitLabel = '';
            unitLabel += (String.isBlank(unit.Inventory__c) || String.isBlank(unit.Inventory__r.Building_Location__c) ? ''
                : (String.isBlank(unit.Inventory__r.Building_Location__r.Building_Name__c)
                    ? unit.Inventory__r.Building_Location__r.Name : unit.Inventory__r.Building_Location__r.Building_Name__c
                )) + ' - ';
            lstUnitOptions.add(new SelectOption(
                unit.Id, unitLabel + (String.isBlank(unit.Unit_Name__c) ? '' : unit.Unit_Name__c)
            ));
        }
        return lstUnitOptions;
    }

    public void scheduleAppointment() {
        sr.Submitted__c = true;
        sr.Status__c = 'Submitted';
        sr.Appointment_Status__c = 'Submitted';
        sr.Origin__c = 'Portal';
        sr.RecordTypeId = getAppointmentRecordTypeId();
        sr.Request_Type__c = 'Appointment';
        sr.Request_Type_DeveloperName__c = 'Appointment';
        sr.Account__c = CustomerCommunityUtils.customerAccountId;

        try {
            upsert sr;
        } catch(Exception excp) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, GENERIC_SAVE_ERROR));
            return;
        }
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, SR_SUBMIT_SUCCESS));
    }

}