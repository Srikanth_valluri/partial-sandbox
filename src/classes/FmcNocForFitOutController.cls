public without sharing class FmcNocForFitOutController extends NOCForFitOutController {

    public  List<SelectOption>  lstUnit                         {get; set;}
    public  Boolean             hasOutstandingServiceCharges    {get; set;}
    public  String              attchmentName                   {get; set;}

    public  Integer             attachCounter = 0;

    public FmcNocForFitOutController() {
        super(false);
        if (FmcUtils.isCurrentView('NOCforFitOutAlterations')) {
            strAccountId = CustomerCommunityUtils.customerAccountId;
            strCaseId = ApexPages.currentPage().getParameters().get('id');
            strSRType = 'NOC_for_Fit_out_Alterations';
            strSelectedUnit = '';
            hasOutstandingServiceCharges = false;
            contractorConsultantSuccessMessage += '<br/><br/>An email with a link for uploading the'
                                                +' below required documents is send to the email id'
                                                +' provided above for contractor/ consultant. Please'
                                                +' inform the contractor/ consultant about the same.'
                                                +' <br/><br/>Please note that the request will be'
                                                +' submitted only once all mandatory details and'
                                                +' documents are uploaded.';

            if (String.isBlank(strCaseId)) {
                List<FM_Case__c> lstFmCase = [ SELECT Id
                                                      , Name
                                                      , NOC_Fees__c
                                               FROM FM_Case__c
                                               WHERE CreatedById = :UserInfo.getUserId()
                                               AND RecordType.Name = 'NOC For FitOut'
                                               AND Origin__c = 'Portal'
                                               AND Status__c IN ('', 'New', 'Draft Request', 'In Progress')
                                               ORDER BY Status__c
                                               LIMIT 1
                                             ];
                if (lstFmCase.isEmpty()) {
                    lstUnit = getUnitOptions();
                } else {
                    strCaseId = lstFmCase[0].Id;
                    objFMCase = FM_Utility.getCaseDetails(strCaseId);
                    strSelectedUnit = objFMCase.Booking_Unit__c;
                    init();
                    lstUnit = getUnitOptions();
                    //lstUnit = new List<SelectOption> {
                    //    new SelectOption(strSelectedUnit, objUnit.Unit_Name__c, true)
                    //};
                }
            } else {
                objFMCase = FM_Utility.getCaseDetails(strCaseId);
                strSelectedUnit = objFMCase.Booking_Unit__c;
                init();
                lstUnit = new List<SelectOption> {
                    new SelectOption(strSelectedUnit, objUnit.Unit_Name__c, true)
                };
            }
        }
    }

    public override Boolean validateOutstandingServiceCharges() {
        hasOutstandingServiceCharges = !super.validateOutstandingServiceCharges();
        System.debug('hasOutstandingServiceCharges = ' + hasOutstandingServiceCharges);
        if (hasOutstandingServiceCharges) {
            ApexPages.addMessage(new ApexPages.message(
                    ApexPages.Severity.WARNING
                    , 'Request for NOC can be submitted only on clearance of Service Charges'
            ));
        }
        return !hasOutstandingServiceCharges;
    }

    protected override SR_Attachments__c setDocumentValidity(SR_Attachments__c doc) {
        doc.isValid__c = false;
        return doc;
    }

    protected override FM_Case__c setCaseOrigin(FM_Case__c fmCase) {
        fmCase.Origin__c = 'Portal';
        return fmCase;
    }

    public PageReference saveAsDraft() {
        createNOCForFitOutCase();
        String nextSteps = 'You can click on "Submit Request" button to submit your request.';
        ApexPages.addMessage(new ApexPages.message(
            ApexPages.Severity.INFO, 'Your request has been saved successfully.' + ' ' + nextSteps
        ));
        return NULL;
    }

    public PageReference submitRequest() {
        if (!validateOutstandingServiceCharges()) {
            return NULL;
        }
        PageReference nextPage = submitNOCForFitOutCase();
        if (nextPage != NULL) {
            String unitId = nextPage.getUrl().substringAfterLast('/');
            nextPage = new PageReference(ApexPages.currentPage().getUrl());
            nextPage.getParameters().clear();
            nextPage.getParameters().put('view', 'CaseDetails');
            nextPage.getParameters().put('id', unitId);
            nextPage.setRedirect(true);
        }
        System.debug('nextPage = ' + nextPage);
        return nextPage;
    }

    public void selectUnit() {
        System.debug('strSelectedUnit = ' + strSelectedUnit);
        if (String.isNotBlank(strSelectedUnit)) {
            init();
        }
    }

    private static List<SelectOption> getUnitOptions() {
        List<SelectOption> lstOptions = new List<SelectOption>{new SelectOption('', 'Select', true)};
        for (Booking_Unit__c unit : FmcUtils.queryUnitsForAccount(
                    CustomerCommunityUtils.customerAccountId, 'Id, Name, Unit_Name__c'
        )) {
            lstOptions.add(new SelectOption(unit.Id, unit.Unit_Name__c));
        }
        return lstOptions;
    }

    public void addDocument() {
        attachCounter++;
        String attName = '_Attachment '+ attachCounter;
        // lstDocuments.add(new FM_Documents__mdt(Document_Name__c=attName, Mandatory__c=false));
        // showDocPanel = lstDocuments.size() > 0 ? TRUE : FALSE;
        System.debug('--- attName --- : '+attName);
        mapMetaDocs.put(attName,new FM_Documents__mdt(Document_Name__c=attName, Mandatory__c=false));
        mapDocSize = mapMetaDocs.size();
    }

    public void removeDocument() {
        System.debug('===attchmentName======' + attchmentName);
        if(attchmentName != NULL) {
            // lstDocuments.remove(attchmentIndex);
            mapMetaDocs.remove(attchmentName);
        }
        mapDocSize = mapMetaDocs.size();
    }
}