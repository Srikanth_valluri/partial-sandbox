global class GetJOPDAreaonBUScheduler implements Schedulable {

	public GetJOPDAreaonBUScheduler() {}

	public GetJOPDAreaonBUScheduler(String jobName) {
		System.schedule(jobName, '0 5 0 * * ? *', new GetJOPDAreaonBUScheduler());
	}
	public void execute(SchedulableContext sc) {
		Database.executebatch(new GetJOPDAreaonBU(), 99);
	}
}