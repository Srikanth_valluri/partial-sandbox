global class GenerateDrawloopDocumentBatch implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful{
    
    private static final Id assignmentRTId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Assignment').RecordTypeId;
    private String query;
    String caseId;
    String strDDPTemplateId;
    String strTemplateDeliveryId;
    public String SERVER_URL;
    public String SESSION_ID;
    
    
    public GenerateDrawloopDocumentBatch(String tempString, String strDDPTemplateId, String strTemplateDeliveryId){
        caseId = tempString ;
        this.strDDPTemplateId = strDDPTemplateId;
        this.strTemplateDeliveryId = strTemplateDeliveryId;
    }
    
    public Database.Querylocator start( Database.BatchableContext bc ) {
        login();
        query = 'SELECT Id, Status, RecordTypeId, OwnerId, Approval_Status__c, CreatedDate, Manager_Id__c, '+
                    'Seller__c, CaseNumber, Booking_Unit__c '+
                'FROM Case Where '+
                'Id = :caseId';
        return Database.getQueryLocator(query);
    }
    
    public void execute( Database.BatchableContext bc, list<Case> lstCases ) {
        system.debug('SESSION_ID*********'+SESSION_ID);
        
        if(SESSION_ID != null && SESSION_ID != ''){
            system.debug('SESSION ID received*********');
            Loop.loopMessage lm = new Loop.loopMessage();
            lm.sessionId = SESSION_ID;
            system.debug('== Case List Size ==' +lstCases.size() );
            for( Case objCase : lstCases ){
                system.debug('== In for Case : '+objCase);
                Map<string, string> variables = new map<String,String>(); // MAIN RECORD ID – SAME OBJECT AS THE DDP RECORD TYPE SPECIFIES’ // DDP ID
                variables.put('deploy', strTemplateDeliveryId);
                lm.requests.add(new Loop.loopMessage.loopMessageRequest(
                                objCase.Id, 
                                strDDPTemplateId, 
                                variables)); 
            } // end of for loop
            String response = lm.sendAllRequests();
            system.debug('== response ==' +response );
        }
    }
    
    public void finish( Database.BatchableContext bc ) {
        
    }
    
    //Method used for login purpose to get the session id
    public void login(){
        Admin_Login_for_Drawloop__c mc = Admin_Login_for_Drawloop__c.getOrgDefaults();
        Http h = new Http(); 
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://' + mc.Domain__c + '.salesforce.com/services/Soap/u/28.0');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        request.setHeader('SOAPAction', '""');
        request.setBody('<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Header/><Body><login xmlns="urn:partner.soap.sforce.com"><username>' + mc.Username__c + '</username><password>' + mc.Password__c + '</password></login></Body></Envelope>');
        
        HttpResponse response = h.send(request);
        system.debug('----Response Body-----'+response.getBody());
        
        Dom.XmlNode resultElmt = response.getBodyDocument().getRootElement()
        .getChildElement('Body','http://schemas.xmlsoap.org/soap/envelope/')
        .getChildElement('loginResponse','urn:partner.soap.sforce.com')
        .getChildElement('result','urn:partner.soap.sforce.com');
        
        SERVER_URL = resultElmt.getChildElement('serverUrl','urn:partner.soap.sforce.com').getText().split('/services')[0];
        SESSION_ID = resultElmt.getChildElement('sessionId','urn:partner.soap.sforce.com').getText();
        
        system.debug('--SERVER_URL---'+SERVER_URL);
        system.debug('--SESSION_ID---'+SESSION_ID);
        if(Test.isRunningTest()){
            SESSION_ID = 'none';
        }
    }  
}