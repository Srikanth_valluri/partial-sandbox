/**************************************************************************************************
* Name         : InventoryFilterValuesBatch
* Test Class  : 
* Description: Batch class to update InventoryFilterValues
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR      DATE
* 1.0         QBurst      27/11/2020
**************************************************************************************************/
public class InventoryFilterValuesBatch implements Database.Batchable<sObject>, Database.Stateful{

    public Map<String, String> filterAPIMap;
    public Map<String, Boolean> filterCaseMap;
    
    /*********************************************************************************************
    * @Description: Implementing the start method of batch interface, contains query.
    * @Params      : Databasevar.BatchableContext
    * @Return      : Database.QueryLocator
    *********************************************************************************************/  
    public Database.QueryLocator start(Database.BatchableContext BC){
        Map<String, List<String>>  filterValuesMap = new Map<String, List<String>>();
        filterAPIMap = new Map<String, String>();
        filterCaseMap = new Map<String, Boolean>();
        Map<String, String> relatedFieldAPIMap = new Map<String, String>();
        List<Inventory_Filters__c> filterCSList = Inventory_Filters__c.getall().values();
        for(Inventory_Filters__c filterCS: filterCSList){
            filterValuesMap.put(filterCS.Name, null);
            if(filterCS.Field_API__c != null && filterCS.Field_API__c != ''){
                filterAPIMap.put(filterCS.Name, filterCS.Field_API__c);
                filterCaseMap.put(filterCS.Name, filterCS.Case_Sensitive__c);
                if(filterCS.Related_Field_API__c != null && filterCS.Related_Field_API__c != ''){
                    relatedFieldAPIMap.put(filterCS.Name, filterCS.Related_Field_API__c);
                }
            }
        }
        for(Inventory_Filter__c filter: [SELECT Id, Name, Filter_Values__c FROM Inventory_Filter__c]){ 
            if(filter.Name != null && filter.Name != '' 
                    && filter.Filter_Values__c != null && filter.Filter_Values__c != ''){
                for(String val: filter.Filter_Values__c.split(';')){
                    if(val != null && val != ''){
                        if(filterValuesMap.containsKey(filter.Name)){
                            List<String> filterValues = new List<String>();
                            if(filterValuesMap.containsKey(filter.Name)){
                                if(filterValuesMap.get(filter.Name) != null){
                                    filterValues = filterValuesMap.get(filter.Name);
                                }
                                if(!filterValues.contains(val)){
                                    filterValues.add(val);
                                }
                            } 
                            filterValuesMap.put(filter.Name, filterValues);
                        } 
                    }
                }
            }
        }
        String query = 'SELECT Id ';
        
        
        if(filterAPIMap.size() > 0){
            for(String key: filterAPIMap.keyset()){
                String api = filterAPIMap.get(key);
                query += ', ' + api;
            }
            query += ' FROM inventory__c';
            query += '  WHERE ';
            List<String> queryCondition = new List<String>();
            for(String key: filterAPIMap.keyset()){
                String api = filterAPIMap.get(key);
                List<String> filterValuesList = new List<String>();
                if(filterValuesMap.containsKey(key) && filterValuesMap.get(key) != null){
                   for(String val: filterValuesMap.get(key)){
                       String formatVal = '\'' + String.escapeSingleQuotes(val) + '\'';
                       filterValuesList.add(formatVal);
                   }
                }
                String condition = ' ( ' + api + ' != NULL ';
                condition += ' AND ' + api + ' != \'\'';
                if(relatedFieldAPIMap.containsKey(key) 
                        && relatedFieldAPIMap.get(key) != null
                        && relatedFieldAPIMap.get(key) != ''){
                    condition += ' AND ' + relatedFieldAPIMap.get(key) + ' !=  NULL ';
                }
                if(filterValuesList.size() > 0){
                    condition += ' AND ' + api + ' NOT IN (';
                    condition += String.join(filterValuesList, ',');
                    condition += ') ';
                }
                condition += ') ';
                queryCondition.add(condition);
            }
            query += string.join(queryCondition,' OR ');
            //query += 'LIMIT 10';
        }
        system.debug('query: ' + query);
        return Database.getQueryLocator(query); 
    
    }

    /*********************************************************************************************
    * @Description: Implementing the execute method of the batch interface, contains the criteria.
    * @Params      : Database.BatchableContext, List<sObject>
    * @Return: void
    *********************************************************************************************/
    public void execute(Database.BatchableContext BC, List<Inventory__c> scope){
        system.debug('scope: ' +  scope);
        Map<String, List<String>>  filterValuesMap = new Map<String, List<String>>();
        Map<String, Inventory_Filter__c> filterNameMap = new Map<String, Inventory_Filter__c>();
        for(String key: filterAPIMap.keyset()){
            filterValuesMap.put(key, null);
        }
        List<Inventory_Filter__c> filterList = [SELECT Id, Name, Filter_Values__c FROM Inventory_Filter__c];
        for(Inventory_Filter__c filter: [SELECT Id, Name, Filter_Values__c FROM Inventory_Filter__c]){ 
            if(filter.Name != null && filter.Name != '' 
                    && filter.Filter_Values__c != null && filter.Filter_Values__c != ''){
                filterNameMap.put(filter.Name, filter);
                for(String val: filter.Filter_Values__c.split(';')){
                    if(val != null && val != ''){
                        if(filterValuesMap.containsKey(filter.Name)){
                            List<String> filterValues = new List<String>();
                            if(filterValuesMap.containsKey(filter.Name)){
                                if(filterValuesMap.get(filter.Name) != null){
                                    filterValues = filterValuesMap.get(filter.Name);
                                }
                                filterValues.add(val);
                            } 
                            filterValuesMap.put(filter.Name, filterValues);
                        } 
                    }
                }
            }
        }
        for(Inventory__c inv: scope){
            for(String key: filterAPIMap.keyset()){
                String fieldAPI = filterAPIMap.get(key);
                String  fieldVal;
                system.debug('key: ' + key);
                system.debug('fieldAPI: ' + fieldAPI);
                if(fieldAPI.contains('.')){
                    List<String> items = fieldAPI.split('\\.');
                    
                    system.debug('items: ' + items);
                    system.debug('inv.getSObject(items[0]): ' + inv.getSObject(items[0]));
                    if(inv.getSObject(items[0]) != null && inv.getSObject(items[0]).get(items[1]) != null){
                        fieldVal = String.valueOf(inv.getSObject(items[0]).get(items[1]));
                    }
                } else{
                    if(inv.get(fieldAPI) != null){
                        fieldVal = String.valueOf(inv.get(fieldAPI));
                    }
                }
                if(fieldVal != null && fieldVal != ''){
                    List<String> valuesList = new List<String>();
                    if(filterValuesMap.containsKey(key) && filterValuesMap.get(key) != null){
                        valuesList = filterValuesMap.get(key);
                    }
                    system.debug('key: ' + key + ', value: ' + fieldVal);
                    if(filterCaseMap.get(key)){
                        fieldVal = fieldVal.toUpperCase();
                    } 
                    if(!valuesList.contains(fieldVal)){
                        valuesList.add(fieldVal);
                        filterValuesMap.put(key, valuesList);
                    }    
                }
            }
        }
        system.debug('filterValuesMap: ' + filterValuesMap);
        List<Inventory_Filter__c> filtersToUpsert = new List<Inventory_Filter__c>();
        for(String key: filterValuesMap.keyset()){
            if(filterValuesMap.containsKey(key) &&  filterValuesMap.get(key) != null 
                    &&  filterValuesMap.get(key).size() > 0){
                Inventory_Filter__c filter = new Inventory_Filter__c();
                if(filterNameMap.containsKey(key)){
                    filter = filterNameMap.get(key);
                } else{
                    filter.Name = key;
                }
                filter.Filter_Values__c = String.join(filterValuesMap.get(key), ';');
                filtersToUpsert.add(filter);
            }
        }
        if(filtersToUpsert.size() > 0){
            system.debug('filtersToUpsert: ' + filtersToUpsert);
            upsert filtersToUpsert;    
        }
    }

    /*********************************************************************************************
    * @Description: Implementing the Finish method, to send an email after job completion.
    * @Params      : Database.BatchableContext
    * @Return: void
    *********************************************************************************************/
    public void finish(Database.BatchableContext BC){
    }
}// End of class.