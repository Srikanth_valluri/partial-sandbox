@IsTest
private class PreInquiryGetEligiblePCSTest {
    static testmethod void method01() {
        
        Inquiry_User_Assignment_Rules__c ua1 = new Inquiry_User_Assignment_Rules__c ();
        ua1.Daily__c = 2;
        ua1.Monthly__c =3;
        ua1.Weekly__c =5;
        insert ua1;
        
        Campaign__c camp = new Campaign__c ();
        camp.Start_Date__c = system.today().adddays(-3);
        camp.End_Date__c =  system.today().adddays(3);
        camp.Marketing_Start_Date__c =  system.today().adddays(-3);
        camp.Marketing_End_Date__c =  system.today().adddays(3);
        camp.PCAssignment__c = true;
       // camp.Active__c = true;
        insert camp;
        
       
        
        
        Assigned_PC__c assign = new Assigned_PC__c ();
        assign.User__c = userinfo.getUserId();
        assign.Campaign__c = camp.Id;
        insert assign;
        
        LIST<Inquiry__c> enquirylist = new LIST<Inquiry__c> ();
        Inquiry__c inquiry = new Inquiry__c();
        inquiry.First_Name__c = 'test';
        inquiry.Last_Name__c = 's';
        inquiry.Primary_Contacts__c = 'Mobile Phone';
        inquiry.Preferred_Language__c = 'French';
        inquiry.Inquiry_Source__c = 'Chat';
        inquiry.Mobile_Phone__c = '1234567890';
        inquiry.Mobile_CountryCode__c = 'India: 0091';
        inquiry.Email__c = 'test@test.com';
        inquiry.Mobile_Phone_Encrypt__c = '8734637856';
        inquiry.Class__c = 'Priority Digital';
        inquiry.Campaign__c = camp.Id;
        enquirylist.add(inquiry);
        insert enquirylist;
        
        user u=[select name from user where id=:userinfo.getUserId()];
        system.runAs(u){
        Group grp = new Group();
        grp.name = 'Test Group1';
        grp.Type = 'Regular'; 
        Insert grp; 
 system.debug('222'+grp.id);
        //Create Group Member
        GroupMember grpMem1 = new GroupMember();
        grpMem1.UserOrGroupId = UserInfo.getUserId();
        grpMem1.GroupId = grp.Id;
        Insert grpMem1;
            
        LIST<Inquiry_Assignment_Rules__c> rules = new LIST<Inquiry_Assignment_Rules__c> ();
             Inquiry_Assignment_Rules__c ua = new Inquiry_Assignment_Rules__c ();
        ua.Is_Campaign_Active__c = 'NO';
        ua.Active__C = true;
        ua.Execute_on__c = 'Create';
        ua.ownerid = grp.Id;
            ua.Tenure__c = '>1 week';
          rules.add(ua);  
        Inquiry_Assignment_Rules__c rule1 = new Inquiry_Assignment_Rules__c ();
            rule1.Tenure__c = '<1 week';
        rule1.Active__c = true;
        rule1.Execute_on__c = 'Create';
        rule1.Queue_Assignment__c =true;
        rule1.Fields_to_Apply_Rule__c = 'Class__c';
        rule1.Class__c = 'Priority Digital';
        rules.add(rule1);
        insert rules;
        test.startTest();
        PreInquiryGetEligiblePCS Inquiry1 = new PreInquiryGetEligiblePCS ();
        Inquiry1.getEligiblsPCS(enquirylist, 'Create');
        PreInquiryGetEligiblePCS.getAllfields('');
         PreInquiryGetEligiblePCS.getAllfields('Account');
        Inquiry1.getEligiblePCSfromQueue(enquirylist, 'Create', null);
        test.stopTest();
        }
    }
    static testmethod void method02(){
          
        Inquiry_User_Assignment_Rules__c ua1 = new Inquiry_User_Assignment_Rules__c ();
        ua1.Daily__c = 2;
        ua1.Monthly__c =3;
        ua1.Weekly__c =5;
        insert ua1;
        
        Campaign__c camp = new Campaign__c ();
        camp.Start_Date__c = system.today().adddays(-3);
        camp.End_Date__c =  system.today().adddays(3);
        camp.Marketing_Start_Date__c =  system.today().adddays(-3);
        camp.Marketing_End_Date__c =  system.today().adddays(3);
        camp.PCAssignment__c = true;
       // camp.Active__c = true;
        insert camp;
        
        Inquiry_Assignment_Rules__c ua = new Inquiry_Assignment_Rules__c ();
        ua.Is_Campaign_Active__c = 'NO';
        ua.Execute_on__c = 'Create';
        ua.Active__C = true;
        ua.Tenure__c = '<1 week';
        ua.Fields_to_Apply_Rule__c = 'Mobile_CountryCode__c';
        //ua.name = 'test';
        insert ua;
        
        
        Assigned_PC__c assign = new Assigned_PC__c ();
        assign.User__c = userinfo.getUserId();
        assign.Campaign__c = camp.Id;
        insert assign;
        
        LIST<Inquiry__c> enquirylist = new LIST<Inquiry__c> ();
        Inquiry__c inquiry = new Inquiry__c();
        inquiry.First_Name__c = 'test';
        inquiry.Last_Name__c = 's';
        inquiry.Primary_Contacts__c = 'Mobile Phone';
        inquiry.Preferred_Language__c = 'French';
        inquiry.Inquiry_Source__c = 'Chat';
        inquiry.Mobile_Phone__c = '1234567890';
        inquiry.Mobile_CountryCode__c = 'India: 0091';
        inquiry.Email__c = 'test@test.com';
        inquiry.Mobile_Phone_Encrypt__c = '8734637856';
        inquiry.Class__c = 'Priority Digital';
       // inquiry.Campaign__c = camp.Id;
        enquirylist.add(inquiry);
        insert enquirylist;
        
        LIST<Inquiry_Assignment_Rules__c> rules = new LIST<Inquiry_Assignment_Rules__c> ();
        Inquiry_Assignment_Rules__c rule1 = new Inquiry_Assignment_Rules__c ();
        rule1.Active__c = true;
        rule1.Queue_Assignment__c =true;
        rule1.Tenure__c = '>1 week';
        rule1.Fields_to_Apply_Rule__c = 'Class__c;RecordTypeId;Mobile_CountryCode__c';
        rule1.Class__c = 'Priority Digital';
        rules.add(rule1);
        insert rules;
        test.startTest();
        PreInquiryGetEligiblePCS Inquiry1 = new PreInquiryGetEligiblePCS ();
        Inquiry1.getEligiblsPCS(enquirylist, 'Create');
        PreInquiryGetEligiblePCS.getAllfields('');
         PreInquiryGetEligiblePCS.getAllfields('Account');
        Inquiry1.getEligiblePCSfromQueue(enquirylist, 'Create', null);
        test.stoptest();
       
    }
    static testmethod void method03(){
          
        Inquiry_User_Assignment_Rules__c ua1 = new Inquiry_User_Assignment_Rules__c ();
        ua1.Daily__c = 2;
        ua1.Monthly__c =3;
        ua1.Weekly__c =5;
        insert ua1;
        
        Campaign__c camp = new Campaign__c ();
        camp.Start_Date__c = system.today().adddays(-3);
        camp.End_Date__c =  system.today().adddays(3);
        camp.Marketing_Start_Date__c =  system.today().adddays(-3);
        camp.Marketing_End_Date__c =  system.today().adddays(3);
        camp.PCAssignment__c = true;
       // camp.Active__c = true;
        insert camp;
        
        Inquiry_Assignment_Rules__c ua = new Inquiry_Assignment_Rules__c ();
        ua.Is_Campaign_Active__c = 'NO';
        ua.Active__C = true;
        ua.Execute_on__c = 'Create';
        ua.Tenure__c = '<1 week';
        ua.Fields_to_Apply_Rule__c = 'Mobile_CountryCode__c';
        //ua.name = 'test';
        insert ua;
        
        
        Assigned_PC__c assign = new Assigned_PC__c ();
        assign.User__c = userinfo.getUserId();
        assign.Campaign__c = camp.Id;
        insert assign;
        
        LIST<Inquiry__c> enquirylist = new LIST<Inquiry__c> ();
        Inquiry__c inquiry = new Inquiry__c();
        inquiry.First_Name__c = 'test';
        inquiry.Last_Name__c = 's';
        inquiry.Primary_Contacts__c = 'Mobile Phone';
        inquiry.Preferred_Language__c = 'French';
        inquiry.Inquiry_Source__c = 'Chat';
        inquiry.Mobile_Phone__c = '1234567890';
        inquiry.Mobile_CountryCode__c = 'India: 0091';
        inquiry.Email__c = 'test@test.com';
        inquiry.Mobile_Phone_Encrypt__c = '8734637856';
        inquiry.Class__c = 'Priority Digital';
       // inquiry.Campaign__c = camp.Id;
        enquirylist.add(inquiry);
        insert enquirylist;
        
        user u=[select name from user where id=:userinfo.getUserId()];
        system.runAs(u){
        Group grp = new Group();
        grp.name = 'Test Group1';
        grp.Type = 'Regular'; 
        Insert grp; 
 system.debug('222'+grp.id);
        //Create Group Member
        GroupMember grpMem1 = new GroupMember();
        grpMem1.UserOrGroupId = UserInfo.getUserId();
        grpMem1.GroupId = grp.Id;
        Insert grpMem1;
        
        LIST<Inquiry_Assignment_Rules__c> rules = new LIST<Inquiry_Assignment_Rules__c> ();
        Inquiry_Assignment_Rules__c rule1 = new Inquiry_Assignment_Rules__c ();
            rule1.OwnerId = grp.id;
            rule1.Tenure__c = '>1 week';
        rule1.Active__c = false;
        rule1.Queue_Assignment__c =true;
        rule1.Execute_on__c = 'Create';
        rule1.Fields_to_Apply_Rule__c = 'Class__c;RecordTypeId;Mobile_CountryCode__c';
        rule1.Class__c = 'Priority Digital';
        rules.add(rule1);
        insert rules;
        test.startTest();
        PreInquiryGetEligiblePCS Inquiry1 = new PreInquiryGetEligiblePCS ();
        Inquiry1.getEligiblsPCS(enquirylist, 'Create');
        PreInquiryGetEligiblePCS.getAllfields('');
         PreInquiryGetEligiblePCS.getAllfields('Account');
        Inquiry1.getEligiblePCSfromQueue(enquirylist, 'Create', null);
        test.stoptest();
        }
       
    }
    static testmethod void method10(){
          
        Inquiry_User_Assignment_Rules__c ua1 = new Inquiry_User_Assignment_Rules__c ();
        ua1.Daily__c = 2;
        ua1.Monthly__c =3;
        ua1.Weekly__c =5;
        insert ua1;
        
        Campaign__c camp = new Campaign__c ();
        camp.Start_Date__c = system.today().adddays(-3);
        camp.End_Date__c =  system.today().adddays(3);
        camp.Marketing_Start_Date__c =  system.today().adddays(-3);
        camp.Marketing_End_Date__c =  system.today().adddays(3);
        camp.PCAssignment__c = true;
       // camp.Active__c = true;
        insert camp;
        
        Inquiry_Assignment_Rules__c ua = new Inquiry_Assignment_Rules__c ();
        ua.Is_Campaign_Active__c = 'NO';
        ua.Execute_on__c = 'Create';
        ua.Active__C = true;
        ua.Fields_to_Apply_Rule__c = 'Class__c';
        ua.Class__c = 'Priority Digital';
        ua.Assign_to_Created_by__c = true;
        //ua.name = 'test';
        insert ua;
        
        
        Assigned_PC__c assign = new Assigned_PC__c ();
        assign.User__c = userinfo.getUserId();
        assign.Campaign__c = camp.Id;
        insert assign;
        
        LIST<Inquiry__c> enquirylist = new LIST<Inquiry__c> ();
        Inquiry__c inquiry = new Inquiry__c();
        inquiry.First_Name__c = 'test';
        inquiry.Last_Name__c = 's';
        inquiry.Primary_Contacts__c = 'Mobile Phone';
        inquiry.Preferred_Language__c = 'French';
        inquiry.Inquiry_Source__c = 'Chat';
        inquiry.Mobile_Phone__c = '1234567890';
        inquiry.Mobile_CountryCode__c = 'India: 0091';
        inquiry.Email__c = 'test@test.com';
        inquiry.Mobile_Phone_Encrypt__c = '8734637856';
        inquiry.Class__c = 'Priority Digital';
       // inquiry.Campaign__c = camp.Id;
        enquirylist.add(inquiry);
        insert enquirylist;
        user u=[select name from user where id=:userinfo.getUserId()];
        system.runAs(u){
        Group grp = new Group();
        grp.name = 'Test Group1';
        grp.Type = 'Regular'; 
        Insert grp; 
 system.debug('222'+grp.id);
        //Create Group Member
        GroupMember grpMem1 = new GroupMember();
        grpMem1.UserOrGroupId = UserInfo.getUserId();
        grpMem1.GroupId = grp.Id;
        Insert grpMem1;
        
        LIST<Inquiry_Assignment_Rules__c> rules = new LIST<Inquiry_Assignment_Rules__c> ();
        Inquiry_Assignment_Rules__c rule1 = new Inquiry_Assignment_Rules__c ();
        rule1.Active__c = false;
        rule1.OwnerId = grp.Id;
        rule1.Queue_Assignment__c =true;
        rule1.Execute_on__c = 'Create';
        rule1.Fields_to_Apply_Rule__c = 'Class__c;RecordTypeId;Mobile_CountryCode__c';
        rule1.Class__c = 'Priority Digital';
        rule1.Assign_to_Created_by__c = true;
        rules.add(rule1);
        insert rules;
        test.startTest();
        PreInquiryGetEligiblePCS Inquiry1 = new PreInquiryGetEligiblePCS ();
        Inquiry1.getEligiblsPCS(enquirylist, 'Create');
        PreInquiryGetEligiblePCS.getAllfields('');
         PreInquiryGetEligiblePCS.getAllfields('Account');
        Inquiry1.getEligiblePCSfromQueue(enquirylist, 'Create', null);
            Inquiry1.userAssignment(enquirylist,'Create');
            test.stopTest();
        }
       
    }
    
    static testmethod void method0(){
          
        Inquiry_User_Assignment_Rules__c ua1 = new Inquiry_User_Assignment_Rules__c ();
        ua1.Daily__c = 2;
        ua1.Monthly__c =3;
        ua1.Weekly__c =5;
        insert ua1;
        
        
        Inquiry_Assignment_Rules__c ua = new Inquiry_Assignment_Rules__c ();
        ua.Is_Campaign_Active__c = 'NO';
        ua.Execute_on__c = 'Create';
        ua.Assign_to_Created_by__c =true;
        ua.Active__C = true;

        insert ua;
        
        
        
        LIST<Inquiry__c> enquirylist = new LIST<Inquiry__c> ();
        Inquiry__c inquiry = new Inquiry__c();
        inquiry.First_Name__c = 'test';
        inquiry.Last_Name__c = 's';
        inquiry.Primary_Contacts__c = 'Mobile Phone';
        inquiry.Preferred_Language__c = 'French';
        inquiry.Inquiry_Source__c = 'Chat';
        inquiry.Mobile_Phone__c = '1234567890';
        inquiry.Mobile_CountryCode__c = 'India: 0091';
        inquiry.Email__c = 'test@test.com';
        inquiry.Mobile_Phone_Encrypt__c = '8734637856';
        inquiry.Class__c = 'Priority Digital';

        enquirylist.add(inquiry);
        insert enquirylist;
        user u=[select name from user where id=:userinfo.getUserId()];
        system.runAs(u){
        Group grp = new Group();
        grp.name = 'Test Group1';
        grp.Type = 'Regular'; 
        Insert grp; 
 system.debug('222'+grp.id);
        //Create Group Member
        GroupMember grpMem1 = new GroupMember();
        grpMem1.UserOrGroupId = UserInfo.getUserId();
        grpMem1.GroupId = grp.Id;
        Insert grpMem1;
        
        LIST<Inquiry_Assignment_Rules__c> rules = new LIST<Inquiry_Assignment_Rules__c> ();
        Inquiry_Assignment_Rules__c rule1 = new Inquiry_Assignment_Rules__c ();
        rule1.Active__c = false;
        rule1.OwnerId = grp.Id;
        rule1.Assign_to_Created_by__c =true;
        rule1.Execute_on__c = 'Create';
        rule1.Fields_to_Apply_Rule__c = 'Class__c;RecordTypeId;Mobile_CountryCode__c';
        rule1.Class__c = 'Priority Digital';
        rules.add(rule1);
        insert rules;
        test.startTest();
        PreInquiryGetEligiblePCS Inquiry1 = new PreInquiryGetEligiblePCS ();
        Inquiry1.getEligiblsPCS(enquirylist, 'Create');
        PreInquiryGetEligiblePCS.getAllfields('');
         PreInquiryGetEligiblePCS.getAllfields('Account');
        Inquiry1.getEligiblePCSfromQueue(enquirylist, 'Create', null);
            test.stopTest();
        }
       
    }
    static testmethod void method4(){
          
        Inquiry_User_Assignment_Rules__c ua1 = new Inquiry_User_Assignment_Rules__c ();
        ua1.Daily__c = 2;
        ua1.Monthly__c =3;
        ua1.Weekly__c =5;
        insert ua1;
        
        Campaign__c camp = new Campaign__c ();
        camp.Start_Date__c = system.today().adddays(-3);
        camp.End_Date__c =  system.today().adddays(3);
        camp.Marketing_Start_Date__c =  system.today().adddays(-3);
        camp.Marketing_End_Date__c =  system.today().adddays(3);
        camp.PCAssignment__c = true;
       // camp.Active__c = true;
        insert camp;
        
        Inquiry_Assignment_Rules__c ua = new Inquiry_Assignment_Rules__c ();
        ua.Is_Campaign_Active__c = 'NO';
        ua.Execute_on__c = 'Create';
        ua.Active__C = true;
        //ua.name = 'test';
        insert ua;
        
        
        Assigned_PC__c assign = new Assigned_PC__c ();
        assign.User__c = userinfo.getUserId();
        assign.Campaign__c = camp.Id;
        insert assign;
        
        LIST<Inquiry__c> enquirylist = new LIST<Inquiry__c> ();
        Inquiry__c inquiry = new Inquiry__c();
        inquiry.First_Name__c = 'test';
        inquiry.Last_Name__c = 's';
        inquiry.Primary_Contacts__c = 'Mobile Phone';
        inquiry.Preferred_Language__c = 'French';
        inquiry.Inquiry_Source__c = 'Chat';
        inquiry.Mobile_Phone__c = '1234567890';
        inquiry.Mobile_CountryCode__c = 'India: 0091';
        inquiry.Email__c = 'test@test.com';
        inquiry.Mobile_Phone_Encrypt__c = '8734637856';
        inquiry.Class__c = 'Priority Digital';
       // inquiry.Campaign__c = camp.Id;
        enquirylist.add(inquiry);
        insert enquirylist;
        user u=[select name from user where id=:userinfo.getUserId()];
        system.runAs(u){
        Group grp = new Group();
        grp.name = 'Test Group1';
        grp.Type = 'Regular'; 
        Insert grp; 
 system.debug('222'+grp.id);
        //Create Group Member
        GroupMember grpMem1 = new GroupMember();
        grpMem1.UserOrGroupId = UserInfo.getUserId();
        grpMem1.GroupId = grp.Id;
        Insert grpMem1;
        
        LIST<Inquiry_Assignment_Rules__c> rules = new LIST<Inquiry_Assignment_Rules__c> ();
        Inquiry_Assignment_Rules__c rule1 = new Inquiry_Assignment_Rules__c ();
        rule1.Active__c = true;
        rule1.OwnerId = grp.Id;
        rule1.Queue_Assignment__c =true;
        rule1.Execute_on__c = 'Create';
        rule1.Fields_to_Apply_Rule__c = 'RecordTypeId';
        rule1.Class__c = 'Priority Digital';
            //rule1.Tenure__c = '';
        rules.add(rule1);
        insert rules;
        test.startTest();
        PreInquiryGetEligiblePCS Inquiry1 = new PreInquiryGetEligiblePCS ();
        Inquiry1.queueAssignment(enquirylist, 'Create');
        Inquiry1.getEligiblePCSfromQueue(enquirylist,'Create',rules);
            test.stopTest();
        }
       
    }
  
}