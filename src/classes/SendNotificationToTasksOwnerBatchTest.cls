@isTest
public class SendNotificationToTasksOwnerBatchTest{

     static Account  objAcc;
    static Case objCase;
    static Booking__c objBooking;
    static Booking_Unit__c objBookingUnit;
    static NSIBPM__Service_Request__c objSR;
    static Calling_List__c objCalling;
    
     static testMethod void initialMethod(){
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person',party_ID__C='123456');
        insert objAcc;
        
        objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = 'Mosk1422';
        objBookingUnit.Unit_Name__c  = 'Test Unit';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
     //   objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        insert objBookingUnit;
        
     }
    static testMethod void Test1(){
        
        initialMethod();
        
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
        OnOffCheck__c = true);
        
        settingLst2.add(newSetting1);
        insert settingLst2;
        
         ID RecordTypeID = 
         Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Appointment Scheduling').getRecordTypeId();
                            
        objCalling = new Calling_list__c();                    
        objCalling.Appointment_Date__c =  System.today();
        objCalling.Account__c =  objAcc.id; 
        objCalling.RecordTypeID  =  RecordTypeID ; 
        objCalling.Customer_Name__c = objAcc.Name;               
        objCalling.Appointment_Status__c = 'Requested';
        insert objCalling;
          
     /*  List<Calling_List__c> lstCalling = TestDataFactory_CRM.createCallingList( 'Appointment_Scheduling', 1 , objBookingUnit );
      insert lstCalling ; */
        Event objEvent = new Event(); 
        
        objEvent.Type = 'Meeting';                                                               
        objEvent.WhatId = objCalling.id;
        objEvent.StartDateTime = System.Now() ;
        objEvent.EndDateTime =  System.Now().addHours(1) ;
        objEvent.Subject = 'Appointment Scheduled with Customer:';
        objEvent.Description= 'Appointment Scheduled with Customer:';
        objEvent.status__c = 'Requested';       
       // insert objEvent ;
                                    
        task tsk = new task();
        tsk.Process_Name__c='Violation Notice';
        tsk.ActivityDate=system.today();
        tsk.whatid = objCalling.id;
        // tsk.whatid = lstCalling[0].id;
        //Overdue Amount Rebate
        //tsk.subject = 'Email: Case Summary Client Relation';
        // tsk.subject = 'Overdue Amount Rebate';
        tsk.subject = 'Upload Incident Report';
        
        tsk.Calling_Number__c = '971557030756';
        insert tsk;
         Test.startTest();
        SendNotificationToTasksOwnerBatch obj = new SendNotificationToTasksOwnerBatch();
            DataBase.executeBatch(obj); 
             Test.stopTest();
       
     }  
}