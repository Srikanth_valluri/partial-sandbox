@isTest
private class MortgageTaskClosureProcessTest
{
	     @isTest static void testUpdateMortgageFlag() {
            insert new Apex_Class_ON_OFF_Setting__c(
                Name = 'MortgageTaskClosureProcess',
            	ON_OFF__c = True
            );
        
   			Account accObj = TestDataFactory_CRM.createPersonAccount();
            insert accObj;
			
             NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
            insert dealSR;
    
            List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( accObj.Id, dealSR.Id, 1 );
            insert lstBookings;
    
            List<Booking_Unit__c> bookingUnitsObj = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
            bookingUnitsObj[0].Registration_ID__c = '5156487646';
            insert bookingUnitsObj;
            
            List<Case> caseLst= createCase('Mortgage',1,accObj.Id, bookingUnitsObj[0].id);
                insert caseLst;
            
            List<Task> taskList = new List<Task>();
            Task taskObj2 = new Task();
            taskObj2.WhatId = caseLst[0].Id;
            taskObj2.Priority = 'High';
            taskObj2.ActivityDate = System.Today();
            taskObj2.Status = 'Completed';
            taskObj2.Subject = System.label.Mortgage_15;
            taskObj2.Assigned_User__c = 'CDC';
            taskObj2.Process_Name__c = 'Mortgage';
            taskObj2.Task_Due_Date__c = System.Today();
            taskList.add(taskObj2);
            insert taskList;
            Test.startTest();
            Test.setMock(WebServiceMock.class, new MortgageWebServiceCalloutMock());
            
            
            
                Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForMortgage());
             Test.stopTest();
                MortgageTaskClosureProcess.calloutToSendMortgageMFlagOnTaskClosure(taskList);
            
  }
  
public static List<Case> createCase( String recordTypeDevName, Integer counter ,Id accId, Id bookingUnitId  ) {

        Id recodTypeIdCase = [
            SELECT Id
            FROM RecordType
            WHERE SObjectType=:'Case'
            AND DeveloperName=:recordTypeDevName
            AND IsActive = TRUE LIMIT 1
        ].Id;
        List<Case> lstCase = new List<Case>();
        for( Integer i=0; i<counter; i++ ) {
            lstCase.add(new Case( 
                Mortgage_Flag__c = Label.Mortgage_Task_Closure_Flag.trim(),
                NOC_Received_Date__c =System.today(),
                NOC_Request_Date__c = System.today(),
                Submit_Date__c = System.today(),
                Loan_Amount__c = 200,
                Bank_Code__c = '2532523',
                Loan_Offer_Date__c = System.today(),
                Bank_Name__c = 'test',
                Mortgage_Bank_Name__c ='test bank',
                Mortgage_Amount__c = 10, 
                AccountId = accId, 
                Booking_Unit__c = bookingUnitId,
                Credit_Note_Amount__c=5000, 
                Type = 'Mortgage',
                status = 'New',
                Mortgage_Status__c = 'Set Mortgage Flag',
                RecordTypeId = recodTypeIdCase ) );
        }
        return lstCase;
    }  

}