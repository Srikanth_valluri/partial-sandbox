/**************************************************************************************************
* Name               : ManageTeamController                                                       *
* Description        : This is a controller class for Manage Team page.                           *
* Created Date       : 22/01/2017                                                                 *
* Created By         : NSI - Vineet                                                               *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR            DATE                                                              *
* 1.0         NSI - Vineet      22/01/2017                                                        *
**************************************************************************************************/
public class ManageTeamController extends DamacUtility{
    public ManageTeamController () {}
    
}// End of class.