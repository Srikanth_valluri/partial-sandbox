@isTest
private class FM_EmailMessageBatchTest{
    @testSetup static void setup(){
        Case objCase = new Case();
        objCase.Origin = 'Email';
        objCase.RecordtypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('FM Email').getRecordTypeId();
        objCase.Status = 'Open';
        objCase.Property_Director__c = UserInfo.getUserId();
        objCase.Case_Reopen_Date__c = date.today().adddays(-6);
        objCase.Escalation_Date__c = date.today().adddays(-3);
        insert objCase;
        
        Case objCase1 = new Case();
        objCase1.Origin = 'Email';
        objCase1.RecordtypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('FM Email').getRecordTypeId();
        objCase1.Status = 'Open';
        objCase1.Property_Director__c = UserInfo.getUserId();
        objCase.Case_Reopen_Date__c = date.today().adddays(-7);
        objCase1.Escalation_Date__c = date.today().adddays(-4);
        insert objCase1;
    }
    
    @isTest static void testMethod1(){
        Database.ExecuteBatch(new FM_EmailMessageBatch());
    }
}