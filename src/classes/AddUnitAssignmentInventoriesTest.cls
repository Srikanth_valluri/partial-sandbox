@isTest (seeAllData=false)
public without sharing class AddUnitAssignmentInventoriesTest {

    @isTest static void addInventoriesTest(){ 
        Marketing_Documents__c mktObj = new Marketing_Documents__c();
        mktObj.Marketing_Name__c = 'OCEANSCAPE';
        insert mktObj;

        Property__c propertyObj = new Property__c();
        propertyObj.Name = 'Property';
        propertyObj.Property_ID__c = 12;
        insert propertyObj;

        Inventory__c inventory;
        Unit_Assignment__c uAObj = new Unit_Assignment__c();
        uAObj.Start_Date__c = Date.parse('11/12/17');
        uAObj.End_Date__c =  Date.parse('11/12/18');
        uAObj.Unit_Assignment_Name__c = 'UA-Test';
        uAObj.Reason_For_Unit_Assignment__c = 'Reason of UA';
        insert uAObj;

        List<Inventory__c> inventoriesList = new  List<Inventory__c>();
        for(Integer i = 0; i <= 40; i++) {
            inventory = InitialiseTestData.getInventoryDetails('345'+i,'123'+i,'234'+i,908+i,765+i); 
            inventory.Tagged_To_Unit_Assignment__c = false;
            inventory.Is_Assigned__c = false;
            inventory.Status__c = 'Released';
            inventory.Marketing_Name_Doc__c = mktObj.Id;
            inventory.Unit_Type__c = 'HOTEL APARTMENTS';
            inventory.Floor__c = 'Floor';
            inventory.Unit__c = 'Unit';
            inventory.Property__c = propertyObj.Id;
            inventoriesList.add(inventory);
        }
        insert inventoriesList ;

        List<User> userList = new  List<User>();
        Profile p = [SELECT Id FROM Profile WHERE Name = 'Property Consultant']; 
        for(Integer i = 0; i <= 40; i++ ) {
            User u = new User(Alias = 'standt', Email='lewisCharles'+i+'@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='shawnmicke'+i+'@test.com');
            userList.add(u);
        }
        insert userList;
        ApexPages.StandardController sc = new ApexPages.standardController(uAObj);
        AddUnitAssignmentInventoriesController addUAObj = new AddUnitAssignmentInventoriesController(sc);

        AddUnitAssignmentInventoriesController.InventoryWrapper wrapper = 
                    new AddUnitAssignmentInventoriesController.InventoryWrapper(inventoriesList[0].id, inventory, true);
        addUAobj.addInventoriesToUnitAssignment();
        addUAobj.inventoryWrapperList[0].isSelected = true ;
        addUAobj.addInventoriesToUnitAssignment();
        addUAobj.displayComponent();
        addUAobj.searchValue = 'Project';
        addUAobj.displayComponent();
        addUAobj.packageValue = 'Property';
        addUAobj.searchInventories();
        addUAobj.searchValue = 'Floor';
        addUAobj.displayComponent();
        addUAobj.floorValue = 'Floor';
        addUAobj.searchInventories();
        addUAobj.searchValue = 'Unit';
        addUAobj.displayComponent();
        addUAobj.unitValue = 'Unit';
        addUAobj.searchInventories();
        addUAobj.clearInventories();
        addUAobj.updatePage();
        addUAobj.startId = addUAobj.inventoryWrapperList[9].recordId;
        addUAobj.prevStartId = addUAobj.inventoryWrapperList[0].recordId;
        addUAobj.prvbtn();
        addUAobj.Nxtbtn();
        addUAobj.LastBtn();
        addUAobj.updatePage();
    }

}