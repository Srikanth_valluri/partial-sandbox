@isTest
private class Damac_WhatsappConsoleHelper_Test{
    static testMethod void getTestResults(){
        
        Inquiry__c testInquiry = new Inquiry__c();
        insert testInquiry;
        Damac_WhatsappConsoleHelper cls = new Damac_WhatsappConsoleHelper();
        cls.inqId = testInquiry.Id;
        cls.init();       
        Damac_WhatsappConsoleHelper.searchInquiryName('IQ-487');
        Nexmo_Whats_App_Request__c testNexmo_Whats_App_Request = new Nexmo_Whats_App_Request__c();
        testNexmo_Whats_App_Request.Last_Message_Sent__c = System.today();
        //testNexmo_Whats_App_Request.Read_At__c = ;
        testNexmo_Whats_App_Request.Customer_Name__c = 'Test';
        testNexmo_Whats_App_Request.Last_Message__c = '';
        testNexmo_Whats_App_Request.Latest_Inbound_Message_Received_on__c  = System.today();
        testNexmo_Whats_App_Request.Inquiry__c = testInquiry.Id;
        insert testNexmo_Whats_App_Request;
        Damac_WhatsappConsoleHelper.updateReadMsg(testNexmo_Whats_App_Request.Id);
    }
}