/**********************************************************************************************************************
Description: Batch to create Pre-legal task for bounced cheque
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Author            | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |                   |                   | 1.Initial Draft

1.1     | 19-08-2020        | Aishwarya Todkar  | 1.Added logic to not to create task if exist

1.2     | 19-08-2020        | Aishwarya Todkar  | 1.Updated 'Pending with legal' status on case

1.3     | 04-11-2020        | Aishwarya Todkar  | 1.Created Finance task and removed CRE task.
***********************************************************************************************************************/
public class AssignBCtoLegalBatch implements Database.Batchable<sObject> {
    
    String assignedUser = 'Finance';
    String priority = 'High';
    String taskStatus = 'Not Started';
    String subject = 'Handover cheque copy to legal'; 
    String caseStatus = 'Closed';
    
    public Database.QueryLocator start(Database.BatchableContext bc){
        List<String> lstRecordType = new List<String> {'Bounced Cheque SR'};
        Date dateToCheck = System.today().addDays( -3 );
        return Database.getQueryLocator([ SELECT 
                                                Id
                                                , OwnerId
                                                , SR_with_Legal__c
                                                , ( SELECT 
                                                        Id
                                                        , OwnerId 
                                                    FROM 
                                                        Calling_List__r )
                                            FROM 
                                                Case 
                                            WHERE 
                                                RecordType.Name IN: lstRecordType
                                            AND 
                                                Status !=: caseStatus
                                            AND 
                                                Day_Only(CreatedDate) =: dateToCheck//LAST_N_DAYS:3
                                        ] ); 
    }
    
    public void execute(Database.BatchableContext bc, List<Case> listCases){        
        
        List<Task> lstTask = new List<Task>();
        List<Calling_List__c> lstCl = new List<Calling_List__c>();
        Set<Id> setCaseId = new Set<Id>();
        for( Task objTask : [SELECT
                                WhatId
                            FROM
                                Task
                            WHERE
                                Subject =: subject
                            AND
                                WhatId In : listCases]) {
            setCaseId.add( objTask.WhatId );
        }
        for( Case thisCase : listCases ) {
            
            if( setCaseId.size() == 0 || ( setCaseId.size() > 0 && !setCaseId.contains( thisCase.Id ) ) ) {
                //thisCase.OwnerId = Label.UserIdLegal;
                thisCase.SR_with_Legal__c = true;
                thisCase.status = 'Pending With Legal';
                // if( thisCase.Calling_List__r != null && thisCase.Calling_List__r.size() > 0 ) {
                //     thisCase.Calling_List__r[0].OwnerId = Label.UserIdLegal;
                //     lstCl.add( thisCase.Calling_List__r[0] );
                // }
                Task objTask = new Task();
                objTask.Subject = subject;
                objTask.Assigned_User__c = assignedUser;
                //objTask.CurrencyIsoCode = 'UAE Dirham';
                objTask.ActivityDate = Date.today() + 1;
                objTask.OwnerId = Label.Bounced_Cheque_Finance_Task_Owner_Id;
                objTask.Priority = priority;
                objTask.Process_Name__c = 'Bounced Cheque';
                objTask.Status = taskStatus;
                objTask.WhatId = thisCase.Id;
                lstTask.add(objTask);
            }
        }

        if(listCases != null && !listCases.isEmpty()
        && lstTask != null && !listCases.isEmpty()
        /*&&  lstCL != null && !listCases.isEmpty() */) {
            
            //update lstCL;
            insert lstTask;
            update listCases;
        }
    }

    public void finish(Database.BatchableContext BC){
        
    }
}