/** 
 * Description: Test class for "AttachmentTriggerHandler" class
 1.1         QBurst        21/01/2020  Method for testing Update of Sys Id in Unit Documents on Attachment Insert 
 */

@isTest
private class AttachmentTriggerHandlerTest {

    private static testmethod void testexecuteBeforeInsertTrigger() {
        
        // Creation of Test Data
        Location__c objLocation =new Location__c();
        objLocation.Location_ID__c='123';
        insert objLocation;
        
        Inventory__c objInventory = new Inventory__c();
        objInventory.Unit_Location__c=objLocation.id;
        insert objInventory;
        
        String dealrecordtypeid = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        
        NSIBPM__Service_Request__c objServiceRequest = new NSIBPM__Service_Request__c();
        objServiceRequest.Delivery_mode__c='Email';
        objServiceRequest.Deal_ID__c='1001';
        objServiceRequest.recordtypeid = dealrecordtypeid;
        insert objServiceRequest;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objServiceRequest.id;
        objBooking.Booking_channel__c='Office';
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c ();
        objBookingUnit.Booking__c = objBooking.id;
        objBookingUnit.Inventory__c = objInventory.id; 
        objBookingUnit.Registration_ID__c = '56789';   
        insert objBookingUnit;
        
        proof_of_payment__c objPOP = new proof_of_payment__c();
        objPOP.deal_sr__c = objServiceRequest.id;
        objPOP.booking_unit__c = objBookingUnit.id;
        objPOP.payment_amount__c = 1432332;
        objPOP.payment_mode__c = 'Cash';
        objPOP.payment_remarks__c = 'AED';
        objPOP.currency__c = 'AED';
        objPOP.payment_date__c = system.today();
        insert objPOP;
        
        Attachment objAttachment = new Attachment();      
        objAttachment.Name = 'Test';
        Blob bodyBlob = Blob.valueOf('Unit Test Attachment');
        objAttachment.body = bodyBlob;
        objAttachment.parentId = objPOP.Id;
        objAttachment.ContentType = 'DOC';
        insert objAttachment;
        
        AttachmentTriggerHandler objAttachmentTriggerHandler = new AttachmentTriggerHandler();
        Test.startTest();
            objAttachmentTriggerHandler.executeBeforeInsertTrigger(new List<Attachment>{objAttachment});
        Test.stopTest();
    }

    private static testmethod void testexecuteBeforeUpdateTrigger() {
        
        // Creation of Test Data
        Location__c objLocation =new Location__c();
        objLocation.Location_ID__c='123';
        insert objLocation;
        
        Inventory__c objInventory = new Inventory__c();
        objInventory.Unit_Location__c=objLocation.id;
        insert objInventory;
        
        String dealrecordtypeid = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        
        NSIBPM__Service_Request__c objServiceRequest = new NSIBPM__Service_Request__c();
        objServiceRequest.Delivery_mode__c='Email';
        objServiceRequest.Deal_ID__c='1001';
        objServiceRequest.recordtypeid = dealrecordtypeid;
        insert objServiceRequest;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objServiceRequest.id;
        objBooking.Booking_channel__c='Office';
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c ();
        objBookingUnit.Booking__c = objBooking.id;
        objBookingUnit.Inventory__c = objInventory.id; 
        objBookingUnit.Registration_ID__c = '56789';   
        insert objBookingUnit;
        
        proof_of_payment__c objPOP = new proof_of_payment__c();
        objPOP.deal_sr__c = objServiceRequest.id;
        objPOP.booking_unit__c = objBookingUnit.id;
        objPOP.payment_amount__c = 1432332;
        objPOP.payment_mode__c = 'Cash';
        objPOP.payment_remarks__c = 'AED';
        objPOP.currency__c = 'AED';
        objPOP.payment_date__c = system.today();
        insert objPOP;
        
        Attachment objAttachment = new Attachment();      
        objAttachment.Name = 'Test';
        Blob bodyBlob = Blob.valueOf('Unit Test Attachment');
        objAttachment.body = bodyBlob;
        objAttachment.parentId = objPOP.Id;
        objAttachment.ContentType = 'DOC';
        insert objAttachment;
        
        AttachmentTriggerHandler objAttachmentTriggerHandler = new AttachmentTriggerHandler();
        Map<Id, Attachment> newMapAttachment = new Map<Id, Attachment>{objAttachment.Id => objAttachment};
        Test.startTest();
            objAttachmentTriggerHandler.executeBeforeUpdateTrigger(newMapAttachment, null);
            objAttachmentTriggerHandler.executeAfterUpdateTrigger(newMapAttachment, null);
            objAttachmentTriggerHandler.executeAfterInsertUpdateTrigger(newMapAttachment, null);
            objAttachmentTriggerHandler.executeBeforeInsertUpdateTrigger(new List<Attachment>{objAttachment}, null);
            objAttachmentTriggerHandler.executeBeforeDeleteTrigger(newMapAttachment);
        Test.stopTest();
    }

    private static testmethod void testHoVAT() {
        
        // Creation of Test Data
        Location__c objLocation =new Location__c();
        objLocation.Location_ID__c='123';
        insert objLocation;
        
        Inventory__c objInventory = new Inventory__c();
        objInventory.Unit_Location__c=objLocation.id;
        insert objInventory;
        
        String dealrecordtypeid = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        
        NSIBPM__Service_Request__c objServiceRequest = new NSIBPM__Service_Request__c();
        objServiceRequest.Delivery_mode__c='Email';
        objServiceRequest.Deal_ID__c='1001';
        objServiceRequest.recordtypeid = dealrecordtypeid;
        insert objServiceRequest;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objServiceRequest.id;
        objBooking.Booking_channel__c='Office';
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c ();
        objBookingUnit.Booking__c = objBooking.id;
        objBookingUnit.Inventory__c = objInventory.id; 
        objBookingUnit.Registration_ID__c = '56789';   
        insert objBookingUnit;
       
        Attachment objAttachment = new Attachment();      
        objAttachment.Name = 'Handed Over Cover Letter';
        Blob bodyBlob = Blob.valueOf('Unit Test Attachment');
        objAttachment.body = bodyBlob;
        objAttachment.parentId = objBookingUnit.Id;
        objAttachment.ContentType = 'DOC';
       
        Test.startTest();
            insert objAttachment;
        Test.stopTest();
    }

    private static testmethod void testexecuteAfterInsertTrigger() {
        
        // Creation of Test Data
        Announcement_Request__c ar = new Announcement_Request__c();
        insert ar;
        
        Attachment objAttachment = new Attachment();      
        objAttachment.Name = 'Test';
        Blob bodyBlob = Blob.valueOf('Unit Test Attachment');
        objAttachment.body = bodyBlob;
        objAttachment.parentId = ar.Id;
        objAttachment.ContentType = 'DOC';
        insert objAttachment;

        AttachmentTriggerHandler objAttachmentTriggerHandler = new AttachmentTriggerHandler();
        Map<Id, Attachment> newMapAttachment = new Map<Id, Attachment>{objAttachment.Id => objAttachment};
        Test.startTest();
            objAttachmentTriggerHandler.executeAfterInsertTrigger(newMapAttachment);
        Test.stopTest();
    }
    
     /****************1.1 starts ***************************/
    @isTest
    public static void testUnitDocAttachInsert() {
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;
        Unit_Documents__c unitDoc = new Unit_Documents__c();
        unitDoc.Service_Request__c = sr.Id;
        insert unitDoc;
        
        Test.startTest();
        Attachment att = new Attachment ( Name = 'Unit Doc Attachment', ParentId = unitDoc.Id, Body = blob.valueOf( 'TEST') );
        insert att;
        Test.stopTest();
    } 
    /******************* 1.1 ends **********************************/   
    
}