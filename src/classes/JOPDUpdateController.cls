public class JOPDUpdateController extends InventoryDesignChangeUtility{
	public List<Design_Change_Request__c> pendingRequests{get; set;}
    public List<Design_Change_Request__c> completedRequests{get; set;}
    public String recordId { get; set; }
    public Design_Change_Request__c dcr{get; set;}
    public list<LineData> csvLines{get; set;}
    public string loadMsg{get; set;}

    private static Final ID recordTypeId = Schema.SObjectType.Design_Change_Request__c.getRecordTypeInfosByName()
        .get('JOPD').getRecordTypeId();
    public JOPDUpdateController() {
        prepareData();
    }
    public void prepareData() {
        recordId = '';
        dcr = new Design_Change_Request__c();
        csvLines = new list<LineData>();
        
        string mainQuery = 'Select Day__c, Property__r.Name, '
            +String.join(readFieldset('DesignChangeFields', 'Design_Change_Request__c'),',')
            +' FROM	Design_Change_Request__c ';
        List<String> compStatus = new List<String> {'Approved','Rejected'};
            pendingRequests = new List<Design_Change_Request__c>();
        pendingRequests = database.query(mainQuery+' WHERE RecordTypeId =: recordTypeId AND status__c not in: compStatus order by createdDate desc');
        
        completedRequests = new List<Design_Change_Request__c>();
        completedRequests = database.query(mainQuery+' WHERE RecordTypeId =: recordTypeId AND status__c in: compStatus order by createdDate desc');
    }
    public void deleteRequest(){
        delete [Select id from Design_change_request__c where id=: recordId];
        prepareData(); 
    }
    public void uploadFile(){

        String fileString =  system.ApexPages.currentPage().getParameters().get('fileBlob');
        String fileName =  system.ApexPages.currentPage().getParameters().get('fileName');
        blob contentFile = EncodingUtil.base64Decode(fileString);

        string DataAsString = blobToString(contentFile,'ISO-8859-1');
        DataAsString = DataAsString.replaceAll('(\r\n|\r)','\n');
        list<string> Lines = DataAsString.split('\n');
        system.debug('fileLines Size: ' + Lines.size());

        for(integer i = 1; i< lines.size(); i++){
            String line = lines[i].trim();
            list<string> vals = line.split(',');
            LineData d = new lineData();
            d.floor = vals[0];
            d.unit_no = vals[1];
            d.merge_unit_no = vals[2];
            d.bedroom = vals[3];
            d.suite_area_sqm = vals[4];
            d.suite_area_sqft = vals[5];
            d.balcony_area_sqm = vals[6];
            d.balcony_area_sqft = vals[7];
            d.terrace_area_sqm = vals[8];
            d.terrace_area_sqft = vals[9];
            d.common_area_sqm = vals[10];
            d.common_area_sqft = vals[11];
            d.sellable_area_sqm = vals[12];
            d.sellable_area_sqft = vals[13];
            d.View_Type = vals[14];
            d.space_type = vals[15];
            csvLines.add(d);
        }

        dcr = new Design_Change_Request__c(); 
        dcr.recordTypeId = recordTypeId;
        dcr.Status__c = 'Draft';
        dcr.File_Name__c = fileName;
        dcr.No_Of_lines__c = csvLines.size();
        insert dcr;

        Attachment att = new Attachment();
        att.parentId = dcr.Id;
        att.Body = contentFile;
        att.name = fileName;
        insert att;

        dcr.Attachment_View_URL__c = '/servlet/servlet.FileDownload?file='+att.Id;
        update dcr;
        loadMsg = 'File Uploaded Successfully'; 
    }

    public void submitRequest(){
        loadMsg = '';
        dcr.Status__c = 'Submitted';
        update dcr;

        Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
        approvalRequest.setComments('DCR Submitted for approval');
        approvalRequest.setObjectId(dcr.Id);
        Approval.ProcessResult approvalResult = Approval.process(approvalRequest);

        loadMsg = 'SUCCESS';
    }
}