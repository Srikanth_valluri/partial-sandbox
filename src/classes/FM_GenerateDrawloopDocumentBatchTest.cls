@isTest
private class FM_GenerateDrawloopDocumentBatchTest
{
	private static testMethod void testOne(){
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;

        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        NSIBPM__Service_Request__c objServReq = TestDataFactory_CRM.createServiceRequest();
        insert objServReq ;

        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objServReq.Id,1);
        insert lstBooking ;

        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus ;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking ,3);
        for( Booking_Unit__c objUnit : lstBookingUnit  ) {
            objUnit.Resident__c = objAcc.Id ;
            objUnit.Handover_Flag__c = 'Y' ;
        }
        lstBookingUnit[2].Owner__c = objAcc.Id ;
        insert lstBookingUnit ;

        list<FM_Case__c> lstFMCases = new list<FM_Case__c>();

        Id recTypeId=Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Fund Transfer').getRecordTypeId();
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Tenant_Registration';
        fmCaseObj.Booking_Unit__c=lstBookingUnit[0].id;
        fmCaseObj.Email__c='a@gmail.com';
        fmCaseObj.Mobile_no__c='12121212';
        fmCaseObj.RecordTypeId=recTypeId;
        fmCaseObj.Status__c='Submitted';
        fmCaseObj.Origin__c='Portal';
        fmCaseObj.Approval_Status__c = 'Pending';
        fmCaseObj.Submit_for_Approval__c = true;
        fmCaseObj.Account__c=objAcc.id;
        fmCaseObj.Current_Approver__c = 'FM Manager__';
        fmCaseObj.Tenant_Email__c = 'test@gmail.com';
        insert fmCaseObj;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());

        FM_GenerateDrawloopDocumentBatch objClass = new FM_GenerateDrawloopDocumentBatch(fmCaseObj.Id
                                                                                   , System.Label.CRF_Doc_DDP_Template_Id
                                                                                   , System.Label.CRF_Doc_Template_Delivery_Id);
        Database.Executebatch(objClass);
        Test.stopTest();
    }
    /*
    private static testMethod void testBU(){
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;

        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        NSIBPM__Service_Request__c objServReq = TestDataFactory_CRM.createServiceRequest();
        insert objServReq ;

        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objServReq.Id,1);
        insert lstBooking ;

        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus ;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking ,3);
        for( Booking_Unit__c objUnit : lstBookingUnit  ) {
            objUnit.Resident__c = objAcc.Id ;
            objUnit.Handover_Flag__c = 'Y' ;
        }
        lstBookingUnit[2].Owner__c = objAcc.Id ;
        insert lstBookingUnit ;

        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());

        FM_GenerateDrawloopDocumentBatch objClass = new FM_GenerateDrawloopDocumentBatch(lstBookingUnit[0].Id
                                                                                   , System.Label.Notice_BR_TemplateId
                                                                                   , System.Label.Notice_BR_DDPId);
        Database.Executebatch(objClass);
        Test.stopTest();
    }*/
}