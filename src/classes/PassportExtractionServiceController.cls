public with sharing class PassportExtractionServiceController {
    public PassportExtractionServiceController(){

    }
    public static data getPassportServiceDetails(String filename, String passportFile) {
    	PassportExtractionService.PassportServiceHttpSoap11Endpoint calloutObj = 
                new PassportExtractionService.PassportServiceHttpSoap11Endpoint();
        calloutObj.timeout_x = 120000;
        data objdata = new data();
        string strJsonResponse;

        PassportExtractionUtility.Base64Binary b = new PassportExtractionUtility.Base64Binary();
        b.base64Binary = passportFile;

        try{
        	strJsonResponse = calloutObj.PassportServiceBase64(filename,b);
        	System.debug('===PassportServiceDetails Response========' + strJsonResponse);
        	objdata = (data)JSON.deserialize(strJsonResponse, data.class);

            system.debug(' response === '+ objdata);
    	}catch(Exception e ){
    		system.debug(' Error in PassportServiceDetails  === '+ e);
            system.debug(' In Catch Error in PassportServiceDetails  === '+ e.getMessage());
            objdata.error = e.getMessage();
    	}
        system.debug(' objdata objdata : === '+ objdata);
    	return objdata;
    }

    public class data {
        public List<PassportDetails> fields{get;set;}
        public String message {get;set;}
        public String status {get;set;}
        public String result {get;set;}
        public String error {get;set;}
        //public String fields {get;set;}
    }

    public class PassportDetails{
        public String text {get;set;}
        public String name {get;set;}
        
    }
}