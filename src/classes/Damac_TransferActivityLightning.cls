global class Damac_TransferActivityLightning {
    
    @AuraEnabled
    public static String transferActivities(String inquiryId){
        String message = '';
        String usrId = UserInfo.getUserId();
        Boolean hasAccess = false;
        List<String> userIdList = new List<String>();
        for(String userId: Label.Transfer_Activity_User_Ids.split(',')){
            userIdList.add(userId.trim());
            if(usrId.contains(userId.trim()) || test.isRunningTest()){
                hasAccess = true;    
            }
        }
        
        if(hasAccess){
            try{
                TransferRelatedRecords.TransferActivitiestoInq(inquiryId);
                message = 'Process Completed Successfully';
            } catch (exception e){
                message = 'Error while Processing: ' + e;
            }
        } else{
            message = 'You don\'t have access to perform this action.';
        }
        return message;
    }
    
}