/************************************************************************************
* Description - Rest service to update DLP Expiry Date                              *
*                                                                                   *
* Version   Date            Author              Description                         *
* 1.0       28/11/2019      Aishwarya Todkar    Initial Draft.                      *   
*************************************************************************************/
public class DLP_ExpiryDateUpdateServiceQueueable implements Queueable,Database.AllowsCallouts {

    String locationId; String dlpDate; Id buId;
    public DLP_ExpiryDateUpdateServiceQueueable( String StrLocationId, String StrdlpDate, Id idbuId ){
        locationId = StrLocationId;
        dlpDate = StrdlpDate;
        buId = idbuId;
    }
    
     public void execute(QueueableContext qc) {

       if( String.isNotBlank( locationId ) && String.isNotBlank( dlpDate ) ) {
           String accessToken = Test.isRunningTest() ? 'testToken' : FmIpmsRestCoffeeServices.getNewBearerToken();
            if( String.isNotBlank( accessToken ) ) {
               
                Credentials_Details__c creds = RebateOnAdvanceService.getCredentials('DLP Expiry Date Update Service');
                System.debug( 'creds = ' + creds);
                if( creds != null && String.isNotBlank( creds.Endpoint__c ) ) {
                    
                    String requestBody = '{' +
                                            '"cafmUnitCode":"' + locationId + '"' +
                                            ',"dlpDate":"' + dlpDate + 
                                        '"}';
                    String userName = creds.User_Name__c; //crp1user
                    String password = creds.Password__c; //oracle_user
                    String endPoint = creds.Endpoint__c;
                    String headerValue = 'Basic ' + EncodingUtil.base64Encode( Blob.valueOf( userName + ':' + password ) );
                    HttpRequest request = new HttpRequest();
                    request.setMethod( 'POST' );
                    request.setHeader( 'Accept','application/json');
                    request.setHeader( 'Content-Type','application/json' );
                    request.setBody( requestBody );
                    request.setEndPoint( endPoint );
                    request.setHeader('Authorization' ,'Bearer' + accessToken);//headerValue );
                    System.debug('request = ' + request);
                    System.debug('requestBody = ' + request.getBody());

                    HttpResponse response = new HttpResponse();
                    
                    try {
                        response = new Http().send( request );

                        System.debug('response body ==' + response.getBody());
                        System.debug('response status code ==' + response.getStatusCode());
                        System.debug('response status ==' + response.getStatus());

                        if( response != null && response.getBody() != null) {
                            DlpResponseWrapper dlpWrapObj = new DlpResponseWrapper();
                            dlpWrapObj = ( DlpResponseWrapper ) JSON.deserialize( response.getBody(), DlpResponseWrapper.class );
                            System.debug( 'dlpWrapObj = ' + dlpWrapObj );
                            String resMsg = dlpWrapObj .Status + ': ' + dlpWrapObj.responseMessage;
                            insert GenericUtility.createErrorLog(resMsg  , null, buId, null, null);
                        }
                    }
                    catch( Exception e ) {
                        System.debug('exception--' + e.getMessage()) ;
                        insert GenericUtility.createErrorLog( e.getMessage(), null, buId, null, null);
                    }
               }//End Credentials settings if
           }// End accessToken if
        }//End locationId & dlpDate if
    }// updateDLPExpiryDate method

    class DlpResponseWrapper {
        String responseId;
        String responseTime;
        String requestName;
        String status;
        String responseMessage;
        Integer elapsedTimeMs;
        Boolean complete;
    }
}