/* * * * * * * * * * * * * *
*  Class Name:   AdditionalParkingWSDLService
*  Purpose:      Service class for calling parking webservices
*  Author:       Hardik Mehta - ESPL
*  Company:      ESPL
*  Created Date: 7-Dev-2017
*  Updated Date: 22-Dec-2017
*  Type:         Class
* * * * * * * * * * * * */
global with sharing class AdditionalParkingWSDLService 
{

    public static AdditionalParkingDetailWrapper.AdditionalParkingDetails getParkingInformation(Decimal decBuildingID )
    {   
        AdditionalParkingDetailWrapper.AdditionalParkingDetails objAdditionalParkingDetailWrapper = 
        new AdditionalParkingDetailWrapper.AdditionalParkingDetails();
        system.debug('decBuildingID '+decBuildingID);
        try
        {
            AdditionalParkingWSDL.ParkingHttpSoap11Endpoint objHttpsoap = new AdditionalParkingWSDL.ParkingHttpSoap11Endpoint();
            objHttpsoap.timeout_x = 120000;
            String strResponse = objHttpsoap.getParking(decBuildingID,'');
            system.debug('getParking strResponse '+strResponse);
            objAdditionalParkingDetailWrapper = AdditionalParkingDetailWrapper.parse(strResponse);
            system.debug('objAdditionalParkingDetailWrapper '+objAdditionalParkingDetailWrapper);

            if(String.isNotBlank(objAdditionalParkingDetailWrapper.status) && objAdditionalParkingDetailWrapper.status.equalsIgnoreCase('e'))
            {
                errorLogger('Error getParking response '+objAdditionalParkingDetailWrapper.message);

            }
        }
        catch(Exception exp)
        {
            errorLogger('Error getParking '+exp.getMessage()+' - '+exp.getStackTraceString());
            objAdditionalParkingDetailWrapper.customErrorMsg = 'Error Fetching Parking Information FROM IPMS.';
        }

        return objAdditionalParkingDetailWrapper;
    }

    public static String createParkingPaymentTerms(parkingPaymentTermsXxdcParkingInvW.APPSXXDC_PARKING_INVX1844909X3X2[] parkingTerms)
    {   
        String strResponse;
        try
        {
            AdditionalParkingWSDL.ParkingHttpSoap11Endpoint objHttpsoap = new AdditionalParkingWSDL.ParkingHttpSoap11Endpoint();
            objHttpsoap.timeout_x = 120000;
            strResponse = objHttpsoap.createParking(parkingTerms);
            system.debug('createParkingPaymentTerms strResponse '+strResponse);
        }
        catch(Exception exp)
        {
            system.debug('exp createParkingPaymentTerms'+exp.getMessage());
            errorLogger('Error createParkingPaymentTerms '+exp.getMessage()+' - '+exp.getStackTraceString());
        }

        return strResponse;
    }

    public static String generateParkingRequestOffer(String strLetterType,AOPTDocumentGenerationXsd.DocGenDTO objDocGenDTO)
    {
        AOPTDocumentGeneration.SFDCDocumentGenerationHttpSoap11Endpoint objGenerateLetter = new AOPTDocumentGeneration.SFDCDocumentGenerationHttpSoap11Endpoint();
        objGenerateLetter.timeout_x = 120000;
        String strResponse = objGenerateLetter.DocGeneration( strLetterType , objDocGenDTO );
        return strResponse;
    }

    //method used to update parking status in IPMS
    public static String updateParkingStatusInIPMS(Decimal decUnitID, Decimal decDetailID,String strParkingBayNo,Decimal decPrice,String strSRNumber, String strStatus)
    {
        String strResponse;
        try
        {
            AdditionalParkingWSDL.ParkingHttpSoap11Endpoint objHttpsoap = new AdditionalParkingWSDL.ParkingHttpSoap11Endpoint();
            objHttpsoap.timeout_x = 120000;
            strResponse = objHttpsoap.updateParking(decUnitID, decDetailID,strParkingBayNo,decPrice,strStatus,strSRNumber);
            system.debug('updateParking strResponse '+strResponse);
        }
        catch(Exception exp)
        {
            system.debug('exp updateParking'+exp.getMessage());
            errorLogger('Error updateParking '+exp.getMessage()+' - '+exp.getStackTraceString());
        }

        return strResponse;
    }

    public static String postParkingFeeToIPMS(String strReqno,processXxdcProcessServiceWsPlsqlSoParkin.APPSXXDC_PROCESS_SERX1794747X1X5[] regTerms)
    {
        String strResponse;
        try
        {
            AdditionalParkingFee.ParkingFeeHttpSoap11Endpoint objHttpsoap = new AdditionalParkingFee.ParkingFeeHttpSoap11Endpoint();
            objHttpsoap.timeout_x = 120000;
            strResponse = objHttpsoap.ParkingAmount(strReqno, 'PARKING_FEE','SFDC',regTerms);
            system.debug('postParkingFeeToIPMS strResponse '+strResponse);
        }
        catch(Exception exp)
        {
            system.debug('exp updateParking'+exp.getMessage());
            errorLogger('Error updateParking '+exp.getMessage()+' - '+exp.getStackTraceString());
        }
        return strResponse;
    }

    private static void errorLogger(string strErrorMessage)
    {
        Error_Log__c objError = new Error_Log__c();
        objError.Error_Details__c = strErrorMessage;
        objError.Process_Name__c = 'Additional Parking';
        insert objError;
    }

    public static void errorLoggerNew(string strErrorMessage, string strCaseID,string strBookingUnitID)
    {
        Error_Log__c objError = new Error_Log__c();
        objError.Error_Details__c = strErrorMessage;
        objError.Process_Name__c = 'AOPT';
        if(String.isNotBlank(strCaseID) && strCaseID.startsWith('500')){
            objError.Case__c = strCaseID;
        }
        if(String.isNotBlank(strBookingUnitID)){
            objError.Booking_unit__c = strBookingUnitID;
        }
        insert objError;
    }

    @InvocableMethod
    public static void createTasksForParking(List<Case> lstSR)
    {   
        system.debug('createTasksForParking for method called');
        Case objCase = new Case();
        if(lstSR != null && lstSR.size() > 0)
        {
            objCase = lstSR.get(0);
            objCase = [Select Id,OwnerId,Pending_Amount__c from Case where Id =: objCase.Id];
            if(objCase != null)
            {   
                //call method to create task for CRE to verify payment
                if(objCase.Pending_Amount__c != null && objCase.Pending_Amount__c > 0)
                {
                    createTaskForPaymentVerification(objCase);
                }
                else
                {
                    //submitRecordForApproval(objCase);
                }
            }
            else
            {
                errorLoggerNew('Error createTasksForParking: objcase is null ',objCase.Id,'');
            }
        }
        else
        {
            system.debug('case list is null or empty');
            errorLoggerNew('Error createTasksForParking: lstSR is null or empty ',objCase.Id,'');
        }
    }

    //method used to create task for Finance to verify payment
    private static void createTaskForPaymentVerification(Case objCase)
    {  
        try
        {   List<Task> taskList = new List<Task>();
            taskList = [Select Id FROM Task WHERE Subject = 'Verify Payment' and Status != 'Completed' and WhatId =:objCase.Id];
            if(taskList.isEmpty())
            {
                Task objTask = new Task();
                objTask.ActivityDate = System.today() +1;
                objTask.Assigned_User__c = 'Finance';
                objTask.CurrencyIsoCode = 'AED';
                objTask.OwnerId = objCase.OwnerId;
                objTask.Priority = 'High';
                objTask.Process_Name__c = 'Parking';
                objTask.Status = 'Not Started';
                objTask.Subject = 'Verify Payment';
                objTask.WhatId = objCase.Id;

                insert objTask;
                AdditionalParkingTaskHandler.createTaskOnIPMS(new Set<Id>{objTask.Id},new Set<Id>{objCase.Id});
                system.debug('task created for verify payment for Finance ');
            }
            else
            {
                system.debug('task is already created with same subject');
            }
    
        }
        catch(Exception exp)
        {
            system.debug('exception occured '+exp.getMessage());
            system.debug('exception occured '+exp.getStackTraceString());
            errorLoggerNew('Error createTaskForPaymentVerification '+exp.getMessage()+' - '+exp.getStackTraceString(),objCase.Id,'');
        }
    }

    
}