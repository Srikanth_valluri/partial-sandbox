/*
 * Description - Test class for EOIRefundComponentController
 *
 * Version        Date            Author            Description
 * 1.0            05/03/18        Vivek Shinde      Initial Draft
 */
@isTest
private class EOIRefundComponentControllerTest {

    static testMethod void submitCaseTest() {
        
        PageReference pageRef = Page.EOIRefundProcessPage;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('SRType', 'EOIRefund'); 
        
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
        
        EOIRefundComponentController objCntlr = new  EOIRefundComponentController();
        objCntlr.eoiAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objCntlr.eoiAttachmentName = 'C/fakepath/Document_1.txt';
        objCntlr.idAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objCntlr.idAttachmentName = 'C:/fakepath/Document_2.txt';
        objCntlr.wtfAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objCntlr.wtfAttachmentName = 'C:/fakepath/Document_3.txt';  
        objCntlr.OtherAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objCntlr.OtherAttachmentName = 'C:/fakepath/Document_3.txt'; 
        objCntlr.saveCase();
        
        Test.StartTest();
             //Set mock response
            Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
            objCntlr.submitCase();
        Test.StopTest();
    }
    
    @isTest
    static void testFormAttachment(){
        Account objAccount = new Account();
        objAccount.Name = 'test';
        insert objAccount;
        
        Case objCase = new Case();
        objCase.AccountId = objAccount.Id;
        insert objCase;
        
        SR_Attachments__c objSRAttachment = new SR_Attachments__c ();
        objSRAttachment.Account__c = objAccount.Id;
        insert objSRAttachment;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        objBooking.Account__c = objAccount.Id;
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Booking__c = objBooking.Id;
        objBU.Case__c = objCase.Id;
        insert objBU;
        
        SR_Attachments__c objSRAttach = new SR_Attachments__c();
        objSRAttach.Type__c = 'test';
        objSRAttach.Name = 'test';
        objSRAttach.Case__c = objCase.Id;
        objSRAttach.Account__c = objAccount.Id;
        objSRAttach.Booking_Unit__c = objBU.Id;
        insert objSRAttach;
        
        Test.startTest();
            EOIRefundComponentController objEOIfFundCtrl = new  EOIRefundComponentController();
            objEOIfFundCtrl.errorLogger('test', objCase.Id, objBU.Id);
        Test.stopTest();
        
    }
}