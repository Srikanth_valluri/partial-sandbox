public class PushNotificationToMobileApp{
   public static boolean run = true;
   @Future(callout=true)
   public static void PushNotificationSend(String caseNo,String createdBy,String status){
        list<FCM_Token__c> f = [Select id
                                     , Fcm_Token__c
                                     , Account__c 
                                From FCM_Token__c 
                                Where createdbyid=:createdBy
                                and Fcm_Token__c != null];
        if(f != null && !f.isEmpty() ){
            NotificationWrapper wrap=new NotificationWrapper();
            system.debug('****caseNo*****'+caseNo);
            system.debug('****f*****'+f);
             system.debug('****f*****'+f[0].Fcm_Token__c);
            wrap.to = f[0].Fcm_Token__c;
            NotificationWrapper.NotificationMessage mes=new NotificationWrapper.NotificationMessage();
            if(status=='Draft Request')
            mes.body='Your request has been saved in draft. SR# is '+caseNo;
            if(status=='Cancelled')
            mes.body='Your request has been cancelled. SR# is '+caseNo;
            if(status=='Submitted')
            mes.body='Your request has been created. SR# is '+caseNo;
             if(status!='Submitted' && status!='Cancelled' && status!='Draft Request')
            mes.body='Your request has been updated. SR# is '+caseNo;
            
            wrap.notification=mes;
            String jsonBody = json.serialize(wrap);
            HttpRequest req = new HttpRequest(); 
            // Callout extended by /fcm/send 
            system.debug('****jsonBody*****'+jsonBody);
            req.setEndpoint('https://fcm.googleapis.com/fcm/send');
            req.setMethod('POST'); 
            // Authentication 
            req.setHeader('Authorization', 'key=AIzaSyDa3l1yzn2goeI0AXNkMlvy2ffc-bB9v5M'); 
            req.setHeader('Content-Type', 'application/json'); 
            req.setBody(jsonBody); 
            System.debug('jsonMaterials'+req.getBody());
            // send request
            Http http = new Http();
            HTTPResponse res = http.send(req);
            system.debug('****res *****'+res );
            system.debug('****res *****'+res.getbody());
          
        }
   }
}