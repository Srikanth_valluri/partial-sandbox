/************************************************************************************************
 
 * @Test Class Name  : Damac_UserAssignmentControllerTest
 * @For Class Name    : Damac_UserAssignmentController

***********************************************************************************************/

@isTest
public class Damac_UserAssignmentControllerTest{
    testMethod static void userAssignment(){
        Profile prof = [select id from profile where name LIKE '%Property Consultant%' LIMIT 1];
        User user = new User();
        user.firstName = 'Sri';
        user.lastName = 'Test';
        user.profileId = prof.id;
        user.username = 'sri.V@test.co';
        user.email = 'sri@test.com';
        user.Alias = 'sri';       
        user.LocaleSidKey = 'bn_IN'; 
        user.EmailEncodingKey = 'UTF-8';
        user.LanguageLocaleKey = 'en_US';
        user.TimeZoneSidKey = 'Pacific/Kiritimati';
        insert user;
        
        User_Assignment__c firstUserAssignment = new User_Assignment__c();
        firstUserAssignment.Shift_Day__c = Date.today();
        firstUserAssignment.User_Assigned__c = user.id;
        firstUserAssignment.Rank__c = 1;
        insert firstUserAssignment ;
        
        PageReference pageRef = Page.Damac_UserAssignment;
        Test.setCurrentPage(pageRef);
        Test.startTest();
            Damac_UserAssignmentController userAssignContr = new Damac_UserAssignmentController();
            userAssignContr.userAssignmet = firstUserAssignment ;
            userAssignContr.retriveUserAssignedRecords();
            Damac_UserAssignmentController.getUserDetails('sri');
            pageRef.getParameters().put('userToEdit', firstUserAssignment.id);
            userAssignContr.editUserAssign();
            pageRef.getParameters().put('userAssignId', firstUserAssignment.id);
            userAssignContr.saveUserAssign();
            pageRef.getParameters().put('userToDelete', firstUserAssignment.id);
            userAssignContr.deleteUserAssign();
            userAssignContr.newUserAssign();           
        Test.stopTest();
    
    }
}