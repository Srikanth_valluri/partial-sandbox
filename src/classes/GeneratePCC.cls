public without sharing class GeneratePCC{
    string strPageID;
    public GeneratePCC(ApexPages.standardController controller){
        
    }//End constructor
    
    public pagereference init(){
        strPageID = ApexPages.currentPage().getParameters().get('id');
        case objCase = [select CaseNumber, AccountId, Booking_Unit__c, Booking_Unit__r.Registration_ID__c,
                        Recordtype.Name 
                        from case where id =:strPageID];
        CallHandoverMQServices.HandoverMQServicesResponse objPCC;
        objPCC= CallHandoverMQServices.CallHandoverMQServiceName(objCase.Booking_Unit__r.Registration_ID__c, strPageID);
        String PCCurl = objPCC.URL;        
        
        list<SR_Attachments__c> lstCaseAttachment = new list<SR_Attachments__c>();
        list<Error_Log__c> lstErrorLog = new list<Error_Log__c>();
        Id attachmentId = fetchPCCDocument();
        if (PCCurl == null || PCCurl == '') {
            SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
            objCaseAttachment.Id = attachmentId;
            objCaseAttachment.Case__c = objCase.id;
            objCaseAttachment.Name = 'PCC ' + system.now();
            //objCaseAttachment.Attachment_URL__c = PCCurl;
            objCaseAttachment.Booking_Unit__c = objCase.Booking_Unit__c;
            lstCaseAttachment.add(objCaseAttachment);
            
            Error_Log__c objErr = new Error_Log__c();
            objErr.Account__c = objCase.AccountId;
            objErr.Booking_Unit__c = objCase.Booking_Unit__c;
            objErr.Case__c = objCase.id;
            objErr.Error_Details__c = objPCC.P_PROC_MESSAGE;
            if (objCase.Recordtype.Name == 'Early_Handover') {
                objErr.Process_Name__c = 'Early Handover';
            } else if (objCase.Recordtype.Name == 'Handover') {
                objErr.Process_Name__c = 'Handover';
            }
            lstErrorLog.add(objErr);
        } else {
             SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
             objCaseAttachment.Case__c = objCase.id;
             objCaseAttachment.Name = 'PCC ' + system.now();
             objCaseAttachment.Id = attachmentId;
             objCaseAttachment.Attachment_URL__c = PCCurl;
             objCaseAttachment.Booking_Unit__c = objCase.Booking_Unit__c;
             lstCaseAttachment.add(objCaseAttachment);
        }
            system.debug('lstCaseAttachment '+lstCaseAttachment );
            if (lstCaseAttachment != null && lstCaseAttachment.size()>0){
                upsert lstCaseAttachment;
            }
            
            if (lstErrorLog != null && lstErrorLog.size()>0){
                insert lstErrorLog;
            }
            /*list<Case> lstCase = new list<Case>();        
            for (Case objCase1: [Select Id, CaseNumber, AccountId, Booking_Unit__c, Early_Handover_Status__c 
                                From Case Where Id =:strPageID]) {
                objCase1.Early_Handover_Status__c = 'PCC Generated';
                lstCase.add(objCase1);
            }
            update lstCase;*/
            list<Task> lstTask = new list<Task>();
            for (Task objTask: [Select Id, Status, Subject, Process_Name__c 
                                From Task 
                                Where Status != 'Completed'
                                And Process_Name__c  = 'Early Handover'
                                And Subject = 'Generate PCC documents and Release it to customer'
                                And WhatId =: strPageID]) {
                objTask.Status = 'Completed';
                lstTask.add(objTask);                                
            }
            if (lstTask != null && lstTask.size() > 0) {
                update lstTask;
            }            
            
            pagereference newpg = new Pagereference(URL.getSalesforceBaseUrl().toExternalForm()+'/'+strPageID);
            newpg.setRedirect(true);
            return newpg;
        
    }//End init
    
    public Id fetchPCCDocument(){
        list<SR_Attachments__c> lstSR = [Select Id, Name, Case__c 
                                        from SR_Attachments__c 
                                        where Case__c =:strPageID
                                        and Name Like 'PCC%' limit 1];
        if(lstSR != null && !lstSR.isEmpty()){
            return lstSR[0].Id;
        }else{
            return null;
        }
    }
}