@IsTest
public class InquiryOwnerReshufflingBatch_Test {

    static testmethod void method1(){
        Inquiry_Country_Code_Categories__c code = new Inquiry_Country_Code_Categories__c();
        code.Team_Name__c = 'Rami Tabbara';
        code.Name = 'India:91';
        insert code;
        
        Id reassignParentRecordTypeId = Schema.SObjectType.Inquiry_Assignment_Rules__c.getRecordTypeInfosByDeveloperName().get('Reassign_Parent').getRecordTypeId();
        
        list<Inquiry_Assignment_Rules__c> inqAssign = new list<Inquiry_Assignment_Rules__c> ();
        Inquiry_Assignment_Rules__c inqAssign1 = new Inquiry_Assignment_Rules__c();
        inqAssign1.Active__c = True;
        inqAssign1.Execute_on__c = 'Reassign';
        inqAssign1.Filter_Logic__c = '1';
        inqAssign1.RecordTypeId = reassignParentRecordTypeId;
        inqAssign1.Rule_for_HOD__c = 'Rami Tabbara';
        inqAssign1.Inquiry_Record_Type__c = 'Inquiry';
        inqAssign.add(inqAssign1);
        
        Inquiry_Assignment_Rules__c inqAssign2 = new Inquiry_Assignment_Rules__c();
        inqAssign2.Active__c = True;
        inqAssign2.Execute_on__c = 'Reassign';
        inqAssign2.Filter_Logic__c = '1';
        inqAssign2.Inquiry_Record_Type__c = 'Inquiry';
        inqAssign.add(inqAssign2);
        
        insert inqAssign;
        
        Inquiry_Assignment_Rules__c inqAssign3 = new Inquiry_Assignment_Rules__c();
        inqAssign3.Active__c = True;
        inqAssign3.Execute_on__c = 'Reassign';
        inqAssign3.Parent_Reassign_Rule__c = inqAssign1.Id;
        inqAssign3.Order__c = 1;
        inqAssign3.Parameter__c = 'Assigned_Date_Age__c';
        inqAssign3.Condition__c = '>';
        inqAssign3.Value__c = '1';
        insert inqAssign3;
        
        Inquiry_User_Assignment_Rules__c userInq = new Inquiry_User_Assignment_Rules__c ();
        userInq.Weekly__c=2;
        userInq.SetupOwnerID = UserInfo.getUserID ();
        userInq.Reassignment_Limit__c = 500;
        userInq.Net_Direct_Sales_Rank__c = null;
        insert userInq;
        id recTypeid = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        list<Inquiry__c> inqs = new list<Inquiry__c> ();
        Inquiry__c inq = new Inquiry__c ();
        inq.Is_Meeting_scheduled__c = false;
        inq.RecordTypeId = recTypeid;
        inqs.add(inq);
        
         insert inqs;

        test.startTest();
        list<user> u=[select id from user where id !=:userinfo.getuserid() and isactive = true limit 2];
        inq.ownerid = u[0].id;
        update inq;
        inq.ownerid = u[1].id;
        update inq;
        ExecuteReassignmentBatch.executeBatch(inqAssign2.id,'Reassign');
        test.stopTest();
    }
    static testmethod void method2(){
        list<Inquiry_Assignment_Rules__c> inqAssign = new list<Inquiry_Assignment_Rules__c> ();
        Inquiry_Assignment_Rules__c inqAssign1 = new Inquiry_Assignment_Rules__c();
        inqAssign1.Active__c = True;
        inqAssign1.Execute_on__c = 'Reassign';
        inqAssign1.Filter_Logic__c = '1';
        inqAssign1.Inquiry_Record_Type__c = 'Inquiry';
        inqAssign1.Skip_Powerline__c =true;
        inqAssign.add(inqAssign1);
         insert inqAssign;
        
        Inquiry_Assignment_Rules__c inqAssign2 = new Inquiry_Assignment_Rules__c();
        inqAssign2.Active__c = True;
        inqAssign2.Execute_on__c = 'Reassign';
        inqAssign2.Parent_Reassign_Rule__c = inqAssign1.Id;
        inqAssign2.Order__c = 1;
        inqAssign2.Parameter__c = 'Assigned_Date_Age__c';
        inqAssign2.Condition__c = '>';
        inqAssign2.Value__c = '1';
        
        insert inqAssign2;
        
        Inquiry_User_Assignment_Rules__c userInq = new Inquiry_User_Assignment_Rules__c ();
        userInq.Weekly__c=2;
        userInq.SetupOwnerID = UserInfo.getUserID ();
        userInq.Reassignment_Limit__c = 500;
        userInq.Net_Direct_Sales_Rank__c = null;
        insert userInq;
        id recTypeid = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        list<Inquiry__c> inqs = new list<Inquiry__c> ();
        Inquiry__c inq = new Inquiry__c ();
        inq.Is_Meeting_scheduled__c = false;
        inq.RecordTypeId = recTypeid;
        inqs.add(inq);
        
        Inquiry__c inq1 = new Inquiry__c ();
        inq1.Is_Meeting_scheduled__c = false;
        inq1.RecordTypeId = recTypeid;
        inqs.add(inq1);
       
         insert inqs;

        test.startTest();
        list<user> u=[select id from user where id !=:userinfo.getuserid() and isactive = true limit 2];
        inq.ownerid = u[0].id;
        update inq;
        inq.ownerid = u[1].id;
        update inq;
        InquiryOwnerReshufflingBatch inqBatch = new InquiryOwnerReshufflingBatch (new List <ID> ());
        database.executeBatch(inqBatch);
        
        //inqBatch.executeBatch(BC,new list<Inquiry__c> {inq});
        test.stopTest();
    }
    static testmethod void method3(){
        list<Inquiry_Assignment_Rules__c> inqAssign = new list<Inquiry_Assignment_Rules__c> ();
        Inquiry_Assignment_Rules__c inqAssign1 = new Inquiry_Assignment_Rules__c();
        inqAssign1.Active__c = True;
        inqAssign1.Execute_on__c = 'Reassign';
        inqAssign1.Filter_Logic__c = '1';
        inqAssign1.Inquiry_Record_Type__c = 'Inquiry';
        inqAssign.add(inqAssign1);
        insert inqAssign;
        
        Inquiry_Assignment_Rules__c inqAssign2 = new Inquiry_Assignment_Rules__c();
        inqAssign2.Active__c = True;
        inqAssign2.Execute_on__c = 'Reassign';
        inqAssign2.Parent_Reassign_Rule__c = inqAssign1.Id;
        inqAssign2.Order__c = 1;
        inqAssign2.Parameter__c = 'Assigned_Date_Age__c';
        inqAssign2.Condition__c = '>';
        inqAssign2.Value__c = '1';
        insert inqAssign2;
        
        Inquiry_User_Assignment_Rules__c userInq = new Inquiry_User_Assignment_Rules__c ();
        userInq.Weekly__c=2;
        userInq.SetupOwnerID = UserInfo.getUserID ();
        userInq.Reassignment_Limit__c = 500;
        userInq.Net_Direct_Sales_Rank__c = null;
        insert userInq;
        id recTypeid = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        list<Inquiry__c> inqs = new list<Inquiry__c> ();
        Inquiry__c inq = new Inquiry__c ();
        inq.Is_Meeting_scheduled__c = false;
        inq.RecordTypeId = recTypeid;
        inqs.add(inq);
        
         insert inqs;

        test.startTest();
        InquiryOwnerReshufflingBatch inqBatch = new InquiryOwnerReshufflingBatch (new List <ID> ());
        database.executeBatch(inqBatch);
       
        test.stopTest();
    }
    static testmethod void method4(){
        list<Inquiry_Assignment_Rules__c> inqAssign = new list<Inquiry_Assignment_Rules__c> ();
        Inquiry_Assignment_Rules__c inqAssign1 = new Inquiry_Assignment_Rules__c();
        inqAssign1.Active__c = True;
        inqAssign1.Execute_on__c = 'Reassign';
        inqAssign1.Filter_Logic__c = '1';
        inqAssign1.Inquiry_Record_Type__c = 'Inquiry';
        inqAssign.add(inqAssign1);
        
        Inquiry_Assignment_Rules__c inqAssign2 = new Inquiry_Assignment_Rules__c();
        inqAssign2.Active__c = True;
        inqAssign2.Execute_on__c = 'Reassign';
        inqAssign2.Filter_Logic__c = '1';
        inqAssign2.Inquiry_Record_Type__c = 'Inquiry';
        inqAssign.add(inqAssign2);
        
        insert inqAssign;
        
        Inquiry_Assignment_Rules__c inqAssign3 = new Inquiry_Assignment_Rules__c();
        inqAssign3.Active__c = True;
        inqAssign3.Execute_on__c = 'Reassign';
        inqAssign3.Parent_Reassign_Rule__c = inqAssign1.Id;
        inqAssign3.Order__c = 1;
        inqAssign3.Parameter__c = 'Assigned_Date_Age__c';
        inqAssign3.Condition__c = '>';
        inqAssign3.Value__c = '1';
        insert inqAssign3;
        
        Inquiry_User_Assignment_Rules__c userInq = new Inquiry_User_Assignment_Rules__c ();
        userInq.Weekly__c=2;
        userInq.SetupOwnerID = UserInfo.getUserID ();
        userInq.Reassignment_Limit__c = 500;
        userInq.Net_Direct_Sales_Rank__c = null;
        insert userInq;
        id recTypeid = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        list<Inquiry__c> inqs = new list<Inquiry__c> ();
        Inquiry__c inq = new Inquiry__c ();
        inq.Is_Meeting_scheduled__c = false;
        inq.RecordTypeId = recTypeid;
        inqs.add(inq);
        
         insert inqs;

        test.startTest();
        list<user> u=[select id from user where id !=:userinfo.getuserid() and isactive = true limit 2];
        inq.ownerid = u[0].id;
        update inq;
        inq.ownerid = u[1].id;
        update inq;
        Map<Inquiry__c, List <User>> inqUsers = new map<Inquiry__c, List <User>> ();
        List <User> user1 = new list<user> ();
        user u1 = [select id from user where id=:userinfo.getuserid()];
        user1.add(u1);
        inqUsers.put(inq,user1);
        InquiryReassignment reAssign = new InquiryReassignment();
        try{
        InquiryReassignment.checkUser(inq, new list<user> {}, 1);
        }catch (exception e){
            try {
        InquiryReassignment.assignOwner(inqUsers); 
            }catch(exception excep){}
        }
        test.stopTest();
    }
    
    
    
     static testmethod void method5(){
        list<Inquiry_Assignment_Rules__c> inqAssign = new list<Inquiry_Assignment_Rules__c> ();
        Inquiry_Assignment_Rules__c inqAssign1 = new Inquiry_Assignment_Rules__c();
        inqAssign1.Active__c = True;
        inqAssign1.object_API__c = 'Inquiry__c';
        inqAssign1.Execute_on__c = 'Reassign';
        inqAssign1.Filter_Logic__c = '1';
        inqAssign1.Inquiry_Record_Type__c = 'Inquiry';
        inqAssign.add(inqAssign1);
        
        Inquiry_Assignment_Rules__c inqAssign2 = new Inquiry_Assignment_Rules__c();
        inqAssign2.Active__c = True;
        inqAssign2.Execute_on__c = 'Reassign';
        inqAssign2.Filter_Logic__c = '1';
        inqAssign2.Inquiry_Record_Type__c = 'Inquiry';
        inqAssign.add(inqAssign2);
        
        insert inqAssign;
        
        Inquiry_Assignment_Rules__c inqAssign3 = new Inquiry_Assignment_Rules__c();
        inqAssign3.Active__c = True;
        inqAssign3.Execute_on__c = 'Reassign';
        inqAssign3.Parent_Reassign_Rule__c = inqAssign1.Id;
        inqAssign3.Order__c = 1;
        inqAssign3.Parameter__c = 'Assigned_Date_Age__c';
        inqAssign3.Condition__c = '>';
        inqAssign3.Value__c = '1';
        insert inqAssign3;
        
        Inquiry_User_Assignment_Rules__c userInq = new Inquiry_User_Assignment_Rules__c ();
        userInq.Weekly__c=2;
        userInq.SetupOwnerID = UserInfo.getUserID ();
        userInq.Reassignment_Limit__c = 500;
        userInq.Net_Direct_Sales_Rank__c = null;
        insert userInq;
        id recTypeid = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        list<Inquiry__c> inqs = new list<Inquiry__c> ();
        Inquiry__c inq = new Inquiry__c ();
        inq.Is_Meeting_scheduled__c = false;
        inq.RecordTypeId = recTypeid;
        inqs.add(inq);
        
        insert inqs;

            test.starttest();
                system.debug(inqAssign[0].id);
                ApexPages.StandardController sc = new ApexPages.StandardController(inqAssign[0]);            
                DAMAC_RULE_UTILITY cls = new DAMAC_RULE_UTILITY(sc);
                PageReference pageRef = Page.DAMAC_RULE_UTILITY;
                pageRef.getParameters().put('id', String.valueOf(inqAssign[0].Id));
                Test.setCurrentPage(pageRef);
                cls.onload();
                
                DAMAC_RULE_UTILITY.retrySMS (inqAssign[0].Id, 'Inquiry__c');
            test.stoptest();        
       
    }
    
        

}