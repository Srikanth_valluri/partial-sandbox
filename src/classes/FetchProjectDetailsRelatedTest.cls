@isTest
public class FetchProjectDetailsRelatedTest{
  static testMethod void  testGetRestService(){
     Property__c objProject =new Property__c();
     objProject.name='DAMAC HILLS - ROCHESTER';
     objProject.Property_ID__c= 3212;
     objProject.Property_Code__c='RR';
     objProject.RERA_Project_Number__c='1410';
     objProject.Latitude__c = 55.25206210;
     objProject.Longitude__c = 25.02125400;
      insert   objProject;  
       
     Marketing_Documents__c mDoc = new Marketing_Documents__c();
     mDoc.Marketing_Name__c = 'Test MarketingDoc';
     insert mDoc;
         
     Inventory__c objInventory =new Inventory__c();
     objInventory.Marketing_Name__c= 'BEVERLY HILLS BOUTIQUE VILLAS';
     objInventory.Marketing_Name_Doc__c = mDoc.id;
     objInventory.Property__c = objProject.Id;
     objInventory.Status__c = 'Released';
     insert  objInventory; 
     
       Test.startTest();
     
       //As Per Best Practice it is important to instantiate the Rest Context 
       
       RestRequest req = new RestRequest(); 
       req.params.put('projectId', string.valueOf(objProject.Property_ID__c));
       RestResponse res = new RestResponse();
         
       req.requestURI = '/services/apexrest/fetchPropertyDetails';  //Request URL
       req.httpMethod = 'GET';//HTTP Request Type
       RestContext.request = req;
       RestContext.response= res;
       List<FetchProjectDetailsRelatedToIdWebService.ResponseResult> response = FetchProjectDetailsRelatedToIdWebService.getProjectsDetails();
     Test.stopTest();
  }
}