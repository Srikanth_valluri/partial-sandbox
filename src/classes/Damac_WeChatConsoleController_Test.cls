@isTest 
public class Damac_WeChatConsoleController_Test {
    public static testMethod void method1 () {
        ID recordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();      
        Inquiry__c inq = new Inquiry__c ();
            inq.Mobile_phone__c = '7894561230';
            inq.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
            inq.Mobile_Phone_Encrypt__c = 'WERTYUIYRTYUIYRTYU';
            inq.Class__c = 'test';
            inq.First_Name__c = 'test';
            inq.Inquiry_Status__c = 'Active';
            inq.Inquiry_Source__c ='Agent Referral';  
            inq.RecordtypeId = recordTypeId;
            inq.Class__c = 'test';
            inq.Email__c = 'test@gmail.com';
            inq.Phone_CountryCode__c = 'United Arab Emirates: 00971';
            inq.Nationality__c = 'Australian';
            inq.weChat_Id__c = 'WERTYUIYRTYUIYRTYU';
            inq.Property_MS__c = 'ADMIRAL BAY;AKOYA OXYGEN';
        insert inq; 
        Damac_WeChatConsoleController obj = New Damac_WeChatConsoleController ();
        obj.checkCurrentInquiry ();
        obj.updateCurrentInquiry ();
        obj.createRelatedActivity();
        obj.insertRelatedActivity();
        obj.cancelInsertActivity();
    }
}