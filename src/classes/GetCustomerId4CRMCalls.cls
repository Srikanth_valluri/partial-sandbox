@RestResource(urlMapping='/GetCustomerId4CRMCalls/*')
global with sharing class GetCustomerId4CRMCalls{
    
//*******************AKISHOR: Created On: 17/Jan/2021 to get account id for opening profile  ******************//

 @HttpPatch
    global static void GetCustomerId4CRMCalls(String IVROption, String CallingNumber) {                                                
           
    System.debug('-->>ivr: '+IVROption+CallingNumber);  
    List<Account> accounts = new List<Account>();
    if(IVROption!= '' && (IVROption== '2' || IVROption == '3') )
     {          
        accounts =
            [
                SELECT
                    Id,
                    Name,
                    Mobile_pc_CRM_Encrypt__c,
                    Mobile_c_CRM_Encrypt__c,
                    POA_CRM_Encrypt__c
                    //Mobile_Phone_Encrypt_4__pc,
                    //Mobile_Phone_Encrypt_5__pc                    
                FROM
                    Account
                WHERE
                    Mobile_pc_CRM_Encrypt__c=: CallingNumber
                    OR Mobile_c_CRM_Encrypt__c=: CallingNumber
                    OR POA_CRM_Encrypt__c=: CallingNumber
                    //OR Mobile_Phone_Encrypt_4__pc =: CallingNumber
                    //OR Mobile_Phone_Encrypt_5__pc =: CallingNumber
            ];
      }                              
          
            if(CallingNumber!='')
             {
                ResponseWrapper objResponseWrapper = new ResponseWrapper();
                try{ 
                  if(IVROption== '2' || IVROption == '3'){
                    objResponseWrapper.RecordId= accounts[0].Id;}
                    else {
                    objResponseWrapper.RecordId= 'No Customer Found';
                    }
                    objResponseWrapper.status = 'Success';
                    objResponseWrapper.statusCode = '200';
                    objResponseWrapper.errorMessage='Details Sent to Ameyo';
                    RestContext.response.responseBody = Blob.valueOf(JSON.serialize(objResponseWrapper));
                    Restcontext.response.statusCode = 200;   
                    return;              
                } catch( Exception ex ) {
                    system.debug( ex.getMessage() );
                    objResponseWrapper.errorMessage = ex.getMessage();
                    objResponseWrapper.statusCode = '400';
                    RestContext.response.responseBody = Blob.valueOf(JSON.serialize(objResponseWrapper));
                    Restcontext.response.statusCode = 400;
                    System.debug('============= response : ' + JSON.serialize(objResponseWrapper));
                    return;                 
                }   
            }
         
 }
    public class ResponseWrapper {
        public String status;
        public String RecordId;
        public String statusCode;
        public String errorMessage;
       
        public ResponseWrapper() {
            this.status = '';
            this.RecordId='';
            this.statusCode = '';
            this.errorMessage = '';
        }
       }
}