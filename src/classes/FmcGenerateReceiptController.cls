public without sharing class FmcGenerateReceiptController {

    private static final Map<String, String> RECEIPT_ATTRIBUTE_MAP = new Map<String, String> {
        'cashReceiptId' => 'DocumentNumber',
        'receiptType' => 'ReceiptType',
        'partyId' => 'PartyId',
        'customerName' => 'PartyName',
        'currencyCode' => 'CurrencyCode',
        'enteredAmount' => 'EnteredAmount',
        'unAppliedAmount' => 'UnAppliedAmount',
        'appliedAmount' => 'AppliedAmount',
        'functionalAmount' => 'FunctionalAmount',
        'receiptNumber' => 'ReceiptNumber',
        'receiptReference' => 'ReferenceNumber',
        'receiptDate' => 'ReceiptDate',
        'orgId' => 'ValidatedYN',
        'businessGroup' => 'RegistrationID',
        'paymentMethod' => 'MiscPaymentSource',
        'comment' => 'Comments',
        'url' => 'URL'
    };

    @RemoteAction
    public static Object fetchMyReceipts() {
        String partyId = CustomerCommunityUtils.getPartyId();
        System.debug('partyId = ' + partyId);
        String strReceipts = FmIpmsRestServices.getReceiptsForPartyId(partyId);
        system.debug('\n>>strReceipts : '+strReceipts);

        if (String.isBlank(strReceipts) || strReceipts == null) {
            System.debug('-----Here------');
            return null;
        }
        Map<String, Object> jsonReceipts = (Map<String, Object>) JSON.deserializeUntyped(String.ValueOf(strReceipts));

        Object objReceipts = jsonReceipts.get('responseLines');
        if (objReceipts == NULL) return NULL;

        List<Object> lstReceipt = (List<Object>) objReceipts;

        for (Object receipt : lstReceipt) {
            Map<String, Object> receiptMap = (Map<String, Object>) receipt;
            for (String attribute : RECEIPT_ATTRIBUTE_MAP.keySet()) {
                receiptMap.put(RECEIPT_ATTRIBUTE_MAP.get(attribute), String.valueOf(receiptMap.get(attribute)));
            }
        }

       //return GenerateURL(lstReceipt);

        return lstReceipt;
    }

    @RemoteAction
    public static string GenerateURL(String docNo) {
        return FmIpmsRestServices.generateDuplicateReceipt(docNo);
    }
}