@isTest
private class FM_ProofOfPaymentControllerTest {

    static testMethod void testMethodOne() {
        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;

        Location__c location = new Location__c(
            Name = 'Test Location',
            Building_Name__c = 'Test Building',
            Location_ID__c = 'LOC',
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0
        );
        insert location;

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;

        Booking_Unit__c buIns1=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns1.Unit_Name__c = 'Unit Name';
        buIns1.Registration_Status__c = lstActiveStatus[0].Name;
        buIns1.Handover_Flag__c = 'Y';
        buIns1.Early_Handover__c = true;
        buIns1.Inventory__c = inventory.Id;
        insert buIns1;

        SR_Attachments__c objAttach = new SR_Attachments__c();
        objAttach.Name = 'Test';
        objAttach.Booking_Unit__c = buIns1.Id ;
        objAttach.Account__c = acctIns.Id;
        insert objAttach;

        //Booking_Unit__c buIns2=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        //insert buIns2;

        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Nationality__c ='Pakistani';

        Test.setCurrentPage( Page.ProofOfPaymentProcessPage );

        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );

        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Proof_of_Payment');

        test.startTest();
        FM_ProofOfPaymentController objCont = new FM_ProofOfPaymentController();
        objCont.insertCase();

        objCont.deleteAttRecId = objAttach.Id ;
        objCont.deleteAttachment();

        objCont.lstUnitsWrap[0].isChecked = true ;
        objCont.savePopCase();
        objCont.submitPopCase();

        /*objCont.strDocumentBody ='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        objCont.strDocumentName='POP_Document.pdf';
        objCont.uploadDocument();*/
        test.stopTest();
    }

    static testMethod void testMethodTwo() {
        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;

        Location__c location = new Location__c(
            Name = 'Test Location',
            Building_Name__c = 'Test Building',
            Location_ID__c = 'LOC',
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0
        );
        insert location;

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;

        Booking_Unit__c buIns1=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns1.Unit_Name__c = 'Unit Name';
        buIns1.Registration_Status__c = lstActiveStatus[0].Name;
        buIns1.Handover_Flag__c = 'Y';
        buIns1.Early_Handover__c = true;
        buIns1.Inventory__c = inventory.Id;
        insert buIns1;

        //Booking_Unit__c buIns2=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        //insert buIns2;

        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Nationality__c ='Pakistani';
        insert fmCaseObj ;

        SR_Attachments__c objAttach = new SR_Attachments__c();
        objAttach.Name = 'Test';
        objAttach.Booking_Unit__c = buIns1.Id ;
        objAttach.Account__c = acctIns.Id;
        objAttach.FM_Case__c = fmCaseObj.Id ;
        insert objAttach;

        SR_Booking_Unit__c objUnit = new SR_Booking_Unit__c();
        objUnit.Booking_Unit__c = buIns1.Id ;
        objUnit.FM_Case__c = fmCaseObj.Id ;
        insert objUnit ;

        Test.setCurrentPage( Page.ProofOfPaymentProcessPage );

        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );

        ApexPages.currentPage().getParameters().put('Id',fmCaseObj.Id);

        test.startTest();
        FM_ProofOfPaymentController objCont = new FM_ProofOfPaymentController();
        objCont.insertCase();

        objCont.savePopCase();
        objCont.submitPopCase();

        /*objCont.strDocumentBody ='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        objCont.strDocumentName='POP_Document.pdf';
        objCont.uploadDocument();*/
        test.stopTest();
    }

    static testMethod void testMethodThree() {
        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;

        Location__c location = new Location__c(
            Name = 'Test Location',
            Building_Name__c = 'Test Building',
            Location_ID__c = 'LOC',
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0
        );
        insert location;

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;

        Booking_Unit__c buIns1=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns1.Unit_Name__c = 'Unit Name';
        buIns1.Registration_Status__c = lstActiveStatus[0].Name;
        buIns1.Handover_Flag__c = 'Y';
        buIns1.Early_Handover__c = true;
        buIns1.Inventory__c = inventory.Id;
        insert buIns1;

        //Booking_Unit__c buIns2=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        //insert buIns2;

        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Nationality__c ='Pakistani';
        insert fmCaseObj ;

        SR_Attachments__c objAttach = new SR_Attachments__c();
        objAttach.Name = 'Test';
        objAttach.Booking_Unit__c = buIns1.Id ;
        objAttach.Account__c = acctIns.Id;
        objAttach.FM_Case__c = fmCaseObj.Id ;
        insert objAttach;

        SR_Booking_Unit__c objUnit = new SR_Booking_Unit__c();
        objUnit.Booking_Unit__c = buIns1.Id ;
        objUnit.FM_Case__c = fmCaseObj.Id ;
        insert objUnit ;

        Test.setCurrentPage( Page.ProofOfPaymentProcessPage );

        Test.setMock( WebServiceMock.class, new MoveOutMockClass() );

        ApexPages.currentPage().getParameters().put('Id',fmCaseObj.Id);

        test.startTest();
        FM_ProofOfPaymentController objCont = new FM_ProofOfPaymentController();

        objCont.objFMCase = FM_Utility.getCaseDetails( fmCaseObj.Id );
        objCont.processUnitsWrapper();
        objCont.processDocuments();
        //objCont.insertCase();
        objCont.strDocumentBody ='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        objCont.strDocumentName='POP_Document.pdf';
        objCont.uploadDocument();
        test.stopTest();
    }

}