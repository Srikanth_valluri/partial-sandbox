@isTest
public with sharing class ComplaintEscalationNotifierTest{
    @TestSetup
    static void createTestData() {

        Case objCase = new Case();
                insert objCase;

        Task objTask = new Task();
        objTask.Subject = 'Resolve Complaint';
        objTask.whatId = objCase.Id;
        objTask.ActivityDate = System.today();
        objTask.Status = 'Not Started';
        objTask.Priority = 'Low';
        insert objTask;
    }
    
    @IsTest
    static void test1(){
        
        Task objTask = [SELECT Id from Task LIMIT 1];
        ComplaintEscalationNotifier.DataWrapper objWrap = new ComplaintEscalationNotifier.DataWrapper();
        objWrap.Ids = objTask.Id;
        objWrap.Eamil48Hour = true;
        Test.startTest();
        ComplaintEscalationNotifier.notifyOwner( new List<ComplaintEscalationNotifier.DataWrapper>{ objWrap});
        Test.stopTest();
    }
}