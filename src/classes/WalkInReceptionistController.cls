public class WalkInReceptionistController {
    public String inqNumber {
        get;
        set;
    }
    public String errorMessage {
        get;
        set;
    }
    public List < Office_meeting__c > meetings {
        get;
        set;
    }
    public Inquiry__c inq {
        get;
        set;
    }
    public Boolean inqSearch {
        get;
        set;
    }
    public Boolean showCheckIn {
        get;
        set;
    }
    public Boolean showInqDetails {
        get;
        set;
    }
    public Boolean showCheckOut {
        get;
        set;
    }
    public Boolean showFeedback {
        get;
        set;
    }
    public Boolean surveyCompleted {
        get;
        set;
    }

    public walkInReceptionistController() {
        inqNumber = '';
        inqSearch = true;
        showCheckIn = false;
        showInqDetails = false;
        showCheckOut = false;
        showFeedback = false;
        errorMessage = '';
        surveyCompleted = false;
        meetings = new List < Office_Meeting__c > ();
        inq = new Inquiry__c();
    }

    @RemoteAction
    public static List < Inquiry__c > searchInquiryName(String pSearchKey) {
        String searchString = '%' +
            pSearchKey +
            '%';
        return [SELECT Full_Name__c, Name, Pre_Inquiry_Number__c FROM Inquiry__c
            WHERE RecordType.Name = 'Inquiry'
            AND Is_Meeting_Confirmed__c = TRUE
            AND Sales_Office__c = 'PARK-TOWER'
            AND (First_Name__c LIKE: searchString 
                    OR Last_Name__c LIKE: searchString 
                    OR Name LIKE: searchString 
                    OR (Pre_Inquiry_Number__c LIKE: searchString 
                        AND Pre_Inquiry_Number__c != NULL
                    )
                )
                
            ORDER By Name DESC
            LIMIT 10
        ];
    }

    public void searchInquiry() {
        errorMessage = '';
        inq = new Inquiry__c();
        if (inqNumber != '' && inqNumber != NULL) {
            try {
                inqSearch = false;
                inqNumber = inqNumber.trim();
                inq = [SELECT Full_Name__c, Name, Purpose_Of_Buying__c, Budget__c, Project_Status__c,
                    Type_of_Property_Interested_in__c, Preferred_Language__c,
                    Nationality__c, Location__c, No_Of_Bed_Rooms_Availble_in_SF__c,
                    Purchased_with_DAMAC_before__c, Would_you_like_assistance_on_mortgage__c, Accompanied_by__c
                    FROM Inquiry__c
                    WHERE
                    ID =: inqNumber
                ];
                /*(Name =: inqNumber OR Pre_Inquiry_Number__c =: inqNumber OR First_Name__c =: inqNumber)
                AND RecordType.Name = 'Inquiry' AND Is_Meeting_Confirmed__c = TRUE LIMIT 1];*/
                meetings = [SELECT
                    Recommended_Rm__r.name, Inquiry__r.Name, Inquiry__r.Full_Name__c,
                    Outcome__c, Sales_Office__c, Check_In_Date__c, Check_Out_Date__c,
                    How_do_you_rate_your_experience_today__c, Why_were_you_not_satisfied_with_your_exp__c
                    FROM Office_meeting__c
                    WHERE Inquiry__c =: inq.id
                    Order BY Meeting_Due_Date__c DESC LIMIT 1
                ];
                if (meetings.size() == 0) {
                    inqSearch = true;
                    inqNumber = '';
                    errorMessage = 'There are no Meetings for this Inquiry.';
                } else {
                    for (Office_Meeting__c meeting: meetings) {
                        meeting.OutCome__c = NULL;
                        meeting.How_do_you_rate_your_experience_today__c = NULL;
                        meeting.Why_were_you_not_satisfied_with_your_exp__c = NULL;
                    }
                }

                showCheckIn = true;
            } catch (Exception e) {
                inqSearch = true;
                inqNumber = '';
                errorMessage = 'There is no inquiry/Meeting with the search term.';
            }
        } else {
            inqNumber = '';
            errorMessage = 'Please choose the inquiry.';
        }
    }

    public void updateCheckIn() {
        errorMessage = '';
        String outCome = '';
        for (Office_Meeting__c meeting: meetings) {
            meeting.Check_In_Date__c = DateTime.Now();
            
            if (meeting.outCome__c == NULL) {
                errorMessage = 'Please fill Meeting Outcome';
            }
            meeting.How_do_you_rate_your_experience_today__c = NULL;
            meeting.Why_were_you_not_satisfied_with_your_exp__c = null;
            outCome = meeting.Outcome__c;
        }
        if (errorMessage == '') {
            if (outCome == 'Show') {
                showCheckIn = false;
                showInqDetails = true;
            } else {
                showCheckIn = false;
                showInqDetails = false;
                showCheckOut = true;
            }


            update meetings;
        }
    }

    public void updateInqDetails() {
        errorMessage = '';
        if (showInqDetails == true) {
            String validate = validateInqFields(inq);
            if (validate == '') {
                update inq;
                showCheckIn = false;
                showInqDetails = false;
                showCheckOut = true;

            } else {
                showCheckIn = false;
                showInqDetails = true;
                showCheckOut = false;
                errorMessage = validate;
            }
        }
    }
    public void UpdateCheckOut() {
        if (showCheckOut == true) {
            String outCome = '';
            for (Office_Meeting__c meeting: meetings) {
                meeting.Check_Out_Date__c = DateTime.Now();
                outCome = meeting.Outcome__c;
            }
            if (Test.isRunningTest()) {
                errorMessage = '';
                outCome = 'Show';
            }
            if (errorMessage == '') {
                if (outCome == 'Show') {
                    showCheckIn = false;
                    showInqDetails = false;
                    showCheckOut = false;
                    showFeedBack = true;
                } else {
                    showCheckIn = false;
                    showInqDetails = false;
                    showCheckOut = false;
                    showFeedBack = false;
                    surveyCompleted = true;
                }
                update meetings;
            }
        }
    }
    public void updateSurvey() {
        String outcome = '';
        errorMessage = '';
        for (Office_meeting__c meeting: meetings) {
            if (meeting.outcome__c != 'Show') {
                meeting.How_do_you_rate_your_experience_today__c = NULL;
                meeting.Why_were_you_not_satisfied_with_your_exp__c = NULL;
            } else {
                outcome = 'Show';
                if (meeting.How_do_you_rate_your_experience_today__c != 'Bad')
                    meeting.Why_were_you_not_satisfied_with_your_exp__c = NULL;
            }
        }
        for (Office_meeting__c meeting: meetings) {

            if (meeting.How_do_you_rate_your_experience_today__c == NULL) {
                errorMessage = 'Please rate your Experience today.';
            }
            if (meeting.How_do_you_rate_your_experience_today__c == 'Bad') {
                if (meeting.Why_were_you_not_satisfied_with_your_exp__c == NULL) {
                    errorMessage = 'Please specify why you were not satisfied.';
                }
            }
        }
        if (errorMessage == '') {
            update meetings;
            showCheckIn = false;
            showInqDetails = false;
            showCheckOut = false;
            showFeedBack = false;
            surveyCompleted = true;
        }
    }

    public String validateInqFields(Inquiry__c inq) {
        String field = 'Please fill ';
        Map < String, String > fieldsToValidate = new Map < String, String > ();
        fieldsToValidate.put('Purpose_Of_Buying__c', 'Purpose Of Buying');
        fieldsToValidate.put('Accompanied_by__c', 'Accompanied By');
        fieldsToValidate.put('Budget__c', 'Budget (AED)');
        fieldsToValidate.put('Project_Status__c', 'Desired Project Status');
        fieldsToValidate.put('Type_of_Property_Interested_in__c', 'Property Type');
        fieldsToValidate.put('Preferred_Language__c', 'Preferred Language');
        fieldsToValidate.put('Nationality__c', 'Nationality');
        fieldsToValidate.put('Location__c', 'Location');
        fieldsToValidate.put('No_Of_Bed_Rooms_Availble_in_SF__c', 'No.of Bed Rooms(Available in SF)');
        fieldsToValidate.put('Purchased_with_DAMAC_before__c', 'Have you purchased with DAMAC before?');
        fieldsToValidate.put('Would_you_like_assistance_on_mortgage__c', 'Would you like assistance on mortgage');
        for (String key: fieldsToValidate.keySet()) {
            if (inq.get(key) == NULL) {
                field += fieldsToValidate.get(key) + ', ';

            }
        }
        if (field == 'Please fill ') {
            field = '';
        }
        field = field.removeEnd(', ');
        return field;

    }
}