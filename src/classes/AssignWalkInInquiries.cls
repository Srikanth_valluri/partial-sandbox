/****************************************************************************************************
* Name          : AssignWalkInInquiries                                                             *
* Description   : Class to Assing the Inquiries with Source Walk-In to certain Pc's                 *
* Created Date  : 13/05/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER     AUTHOR            DATE            COMMENTS                                                *
* 1.0     Craig Lobo        13/05/2018      Initial Draft.                                          *
* 1.1     Craig Lobo        17/05/2018      Updated the code to Date field insted of text fields.   *
* 1.2     Craig Lobo        20/05/2018      Added functionality to temporarily Skip a RM.           *
****************************************************************************************************/
public with sharing class AssignWalkInInquiries {

    public String WALKIN_QUERY  = ' SELECT Id, Name, Active__c, User__c, Order__c, RM_Office__c, '
                                + ' Week_Start_Day__c, Week_End_Day__c, User__r.Name, '
                                + ' Attended_Inquiries__c, Previous_RM__c, Skip_RM__c, '
                                + ' Available_On_Friday__c, Available_On_Saturday__c, '
                                + ' RM_Blacklisted__c, RM_On_Leave__c, User__r.isActive '
                                + ' FROM Walk_In_Schedule__c '
                                + ' WHERE User__r.isActive = true ';
                                //+ ' AND Week_Start_Day__c <= TODAY '
                                //+ ' AND Week_End_Day__c >= TODAY ';

    /**
     * Constructor
     */
    public AssignWalkInInquiries() {
        
    }

    /**
     * Method to assign Inquiry Owner to the Avaliable PCs
     */
    public void assignWalkInInquiriesToRM(List<Inquiry__c> pInquiryNewList) {

        System.debug('assignWalkInInquiriesToRM >>>' + pInquiryNewList);
        System.debug('UserInfo.getUserId() >>>' + UserInfo.getUserId());
        User currentUser = [SELECT Id, Sales_Office__c, Profile.Name 
                              FROM User 
                             WHERE Id = :UserInfo.getUserId() 
                             LIMIT 1
        ];

        // Logic to run only in current User Profile is Receptionist
        if (String.isNotBlank(currentUser.Sales_Office__c)
            && currentUser.Profile.Name == 'Receptionist'
        ) {

            String userSalesOffice = currentUser.Sales_Office__c;
            System.debug('userSalesOffice >>>' + userSalesOffice);
            Decimal lastOrder = 0;
            Decimal startIndex = 0;
            Decimal lastIndex = 0;
            Walk_In_Schedule__c walkinInstance; 
            Map<Decimal, Walk_In_Schedule__c> scheduleOrderMap = new Map<Decimal, Walk_In_Schedule__c>();
            String scheduleQuery = WALKIN_QUERY
                                 + ' AND RM_Office__c = \'' + userSalesOffice + '\''
                                 + ' ORDER BY Previous_RM__c ';

            System.debug(' scheduleQuery >>>' +  scheduleQuery);
            System.debug(' DataBase.Query(WALKIN_QUERY) >>>' +  DataBase.Query(scheduleQuery));


            // Populate Schedule Map to assign to Inquiries
            for (Walk_In_Schedule__c instance : DataBase.Query(scheduleQuery)) {
                if (instance.Active__c) {
                    if (instance.Previous_RM__c) {
                        startIndex = instance.Order__c;
                        walkinInstance = instance;
                        System.debug(' instance.Order__c; >>>' +  instance.Order__c);
                    }
                    if (instance.Order__c != null) {
                        scheduleOrderMap.put(instance.Order__c, instance);
                        if (lastOrder < instance.Order__c ) {
                            lastOrder = instance.Order__c;
                        }
                    }
                }
                instance.Previous_RM__c = false;
            }

            if (!scheduleOrderMap.isEmpty()) {
                System.debug('lastOrder >>>' + lastOrder);
                System.debug('scheduleOrderMap >>>' + scheduleOrderMap);
                System.debug('scheduleOrderMap Size >>>' + scheduleOrderMap.size());
                String dayName = DateTime.now().format('E');
                //String dayName = DateTime.now().addDays(3).format('E');
                Decimal dayNumber = Decimal.valueOf(System.today().day());
                String dayType = '';
                startIndex++;

                // Iterate over the New Inquiries to Assign Owner
                for (Inquiry__c newInq : pInquiryNewList) {
                    if (newInq.Inquiry_Source__c == 'Walk in' ) {
                        System.debug('BEFORE FOR startIndex >>>' + startIndex);
                        if (startIndex > lastOrder) {
                            startIndex = 1;
                        }

                        // Iterate over the Walkin records to assign Owner
                        for (Decimal counter = startIndex; counter <= lastOrder; counter++ ) {
                            System.debug('INSIDE FOR BEFORE IF counter >>>' + counter);
                            if (scheduleOrderMap.containsKey(counter)) {
                                Walk_In_Schedule__c wsInstance = scheduleOrderMap.get(counter);
                                if (wsInstance.User__c != null 
                                    && !wsInstance.RM_On_Leave__c 
                                    && !wsInstance.RM_Blacklisted__c
                                    && !wsInstance.Skip_RM__c 
                                ) {
                                    if ( (dayName != 'Fri' && dayName != 'Sat') // Sunday to Thursday
                                        || (dayName == 'Fri' && wsInstance.Available_On_Friday__c) // Only Friday
                                        || (dayName == 'Sat' && wsInstance.Available_On_Saturday__c) // Only Saturdat
                                    ) {
                                        System.debug('ASSIGNMEMT >>>' + dayName);
                                        System.debug('On_Friday__c >>>' + wsInstance.Available_On_Friday__c);
                                        System.debug('On_Saturday__c >>>' + wsInstance.Available_On_Saturday__c);
                                        newInq.OwnerId = scheduleOrderMap.get(counter).User__c;
                                        if (scheduleOrderMap.get(counter).Attended_Inquiries__c == null) {
                                            scheduleOrderMap.get(counter).Attended_Inquiries__c = 1;
                                        } else {
                                            scheduleOrderMap.get(counter).Attended_Inquiries__c 
                                                = scheduleOrderMap.get(counter).Attended_Inquiries__c + 1;
                                        }
                                        lastIndex = counter;
                                        break;
                                    }
                                }
                            } else if (counter > lastOrder) {
                                System.debug('ESLE Reset Counter >>>' + counter);
                                counter = 0;
                            }
                            System.debug('--------------------------------->>>');
                        }
                    } // Inquiry Source Check
                }  // Inquiry List Loop

                System.debug('lastIndex >>>' + lastIndex);
                System.debug('scheduleOrderMap 1>>>' + scheduleOrderMap);
                if (scheduleOrderMap.containsKey(lastIndex)) {
                    scheduleOrderMap.get(lastIndex).Previous_RM__c = true;
                }
                System.debug('scheduleOrderMap 2>>>' + scheduleOrderMap);
                if (walkinInstance != null && walkinInstance.Order__c != null
                    && !scheduleOrderMap.containsKey(walkinInstance.Order__c)) {
                    scheduleOrderMap.put(walkinInstance.Order__c, walkinInstance);
                }
                System.debug('scheduleOrderMap 3>>>' + scheduleOrderMap);

                // DML
                update scheduleOrderMap.values();
                System.debug('pInquiryNewList 3>>>' + pInquiryNewList); 
            } // ScheduleOrderMap Check*/
        }
    }

}