/*
* Description - Test class developed for 'AdditionalParkingFee'
*
* Version            Date            Author            Description
* 1.0                01/02/2018        Hardik            Initial Draft
*/
@isTest
public with sharing class AdditionalParkingFeeTest 
{
    static testMethod void ParkingAmountTest() 
    {
        AdditionalParkingFee.ParkingAmountResponse_element objParkingAmtResponse = new AdditionalParkingFee.ParkingAmountResponse_element();
        AdditionalParkingFee.ParkingAmount_element objParkingAmtElement = new AdditionalParkingFee.ParkingAmount_element();

        Test.startTest();
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );
            AdditionalParkingFee.ParkingFeeHttpSoap11Endpoint objReq = new AdditionalParkingFee.ParkingFeeHttpSoap11Endpoint();
            
            processXxdcProcessServiceWsPlsqlSoParkin.APPSXXDC_PROCESS_SERX1794747X1X5[] regTerms = new processXxdcProcessServiceWsPlsqlSoParkin.APPSXXDC_PROCESS_SERX1794747X1X5[1];
          processXxdcProcessServiceWsPlsqlSoParkin.APPSXXDC_PROCESS_SERX1794747X1X5 objRegTerm = new processXxdcProcessServiceWsPlsqlSoParkin.APPSXXDC_PROCESS_SERX1794747X1X5();
           objRegTerm.PARAM_ID = '2-123456';
           objRegTerm.ATTRIBUTE1 = '100';
           objRegTerm.ATTRIBUTE10 = '';
           objRegTerm.ATTRIBUTE11 = '';
           objRegTerm.ATTRIBUTE12 = '';
           objRegTerm.ATTRIBUTE13 = '';
           objRegTerm.ATTRIBUTE14 = '';
           objRegTerm.ATTRIBUTE15 = '';
           objRegTerm.ATTRIBUTE16 = '';
           objRegTerm.ATTRIBUTE17 = '';
           objRegTerm.ATTRIBUTE18 = '';
           objRegTerm.ATTRIBUTE19 = '';
           objRegTerm.ATTRIBUTE2 = '';
           objRegTerm.ATTRIBUTE20 = '';
           objRegTerm.ATTRIBUTE21 = '';
           objRegTerm.ATTRIBUTE22 = '';
           objRegTerm.ATTRIBUTE23 = '';
           objRegTerm.ATTRIBUTE24 = '';
           objRegTerm.ATTRIBUTE25 = '';
           objRegTerm.ATTRIBUTE26 = '';
           objRegTerm.ATTRIBUTE27 = '';
           objRegTerm.ATTRIBUTE28 = '';
           objRegTerm.ATTRIBUTE29 = '';
           objRegTerm.ATTRIBUTE3 = '';
           objRegTerm.ATTRIBUTE30 = '';
           objRegTerm.ATTRIBUTE31 = '';
           objRegTerm.ATTRIBUTE32 = '';
           objRegTerm.ATTRIBUTE33 = '';
           objRegTerm.ATTRIBUTE34 = '';
           objRegTerm.ATTRIBUTE35 = '';
           objRegTerm.ATTRIBUTE36 = '';
           objRegTerm.ATTRIBUTE37 = '';
           objRegTerm.ATTRIBUTE38 = '';
           objRegTerm.ATTRIBUTE39 = '';
           objRegTerm.ATTRIBUTE4 = '';
           objRegTerm.ATTRIBUTE41 = '';
           objRegTerm.ATTRIBUTE42 = '';
           objRegTerm.ATTRIBUTE43 = '';
           objRegTerm.ATTRIBUTE44 = '';
           objRegTerm.ATTRIBUTE45 = '';
           objRegTerm.ATTRIBUTE46 = '';
           objRegTerm.ATTRIBUTE47 = '';
           objRegTerm.ATTRIBUTE48 = '';
           objRegTerm.ATTRIBUTE49 = '';
           objRegTerm.ATTRIBUTE5 = '';
           objRegTerm.ATTRIBUTE50 = '';
           objRegTerm.ATTRIBUTE6 = '';
           objRegTerm.ATTRIBUTE7 = '';
           objRegTerm.ATTRIBUTE8 = '';
           objRegTerm.ATTRIBUTE9 = '';
           regTerms[0] = objRegTerm;
            objReq.ParkingAmount('123456','PARKING_FEE','SFDC',regTerms) ;

            //AsyncAdditionalParkingFee objAsyncReq = new AsyncAdditionalParkingFee();
        Test.stopTest();
    }
}