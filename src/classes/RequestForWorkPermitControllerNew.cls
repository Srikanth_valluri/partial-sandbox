/**
 * @File Name          : RequestForWorkPermitControllerNew.cls
 * @Description        :
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              :
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 8/14/2019, 5:39:25 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    8/7/2019, 2:34:24 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
global virtual without sharing class RequestForWorkPermitControllerNew {
    public list<SelectOption> lstUnits { get; set; }

    //public map<String, list<FM_Additional_Detail__c>> mapAdditionalDetails { get; set; }

    // Added on 8/7/2019
    public        List<Contractor_Information__c>   ContractorInfoList          {get;set;}

    public              Booking_Unit__c             objUnit                     {get;set;}
    public              Boolean                     isEdittable                 {get;set;}
    public              Boolean                     isMandatory                 {get;set;}
    public              FM_Case__c                  objFMCase                   {get;set;}
    public              string                      strFMCaseId                 {get;set;}
    public              String                      strSRType                   {get;set;}
    public              String                      strSelectedUnit             {get;set;}
    public              String[]                    valuesOfDrawings            {get;set;}
    public              SelectOption[]              valueListOfDrawings         {get;set;}
    @testVisible
    public              String                      strAccountId ;
    public              String[]                    values                      {get;set;}
    public              SelectOption[]              valueList                   {get;set;}
    public              List<FM_Case__c>            instanceList;
    public              String                      urlForTermsAndCondition     {get;set;}
    //public string feeAsBuiltDrawing{get; set;}
    public              string                      savedOrSubmitted            {get;set;}
    public              decimal                     minimumOutstandingCharge    {get;set;}
    public transient    String                      strDocumentBody             {get;set;}
    public transient    String                      strDocumentName             {get;set;}
    public transient    String                      deleteAttRecId              {get;set;}
    public              string                      securityChequeDepositAmount {get;set;}
    public              string                      garbageDisposalAmount       {get;set;}
    public              string                      temporaryPowerConnAmount    {get;set;}
    public              List<Location__c>           objList                     {get;set;}
    public              list<FM_Documents__mdt>     lstDocuments                {get;set;}
    public              list<SR_Attachments__c>     lstRecentAttachment         {get;set;}
    public              list<SR_Attachments__c>     lstUploadedDocs             {get;set;}

    @testVisible
    public              map<String, FM_Documents__mdt> mapProcessDocuments ;
    @testVisible
    public              map<String, SR_Attachments__c> mapUploadedDocs ;

    protected String contractorConsultantSuccessMessage = 'Consultant/Contractor Notified Successfully ';

    protected RequestForWorkPermitControllerNew(Boolean shouldCall) {}

    public RequestForWorkPermitControllerNew() {
        // Added on 8/7/2019


        objList=new List<Location__c>();
        instanceList=new List<FM_Case__c>();
        valueListOfDrawings=new List<SelectOption>();
        valuesOfDrawings=new List<String>();
        isEdittable=true;
        valueList = new SelectOption[0];
        values = new String[0];
        isMandatory=false;
        minimumOutstandingCharge = 0;
        urlForTermsAndCondition= openTermsAndConditionWindow();
        strFMCaseId = ApexPages.currentPage().getParameters().get('Id');
        if(String.isBlank( strFMCaseId )){
            strAccountId = ApexPages.currentPage().getParameters().get('AccountId');
            strSelectedUnit = ApexPages.currentPage().getParameters().get('UnitId');
            strSRType = ApexPages.currentPage().getParameters().get('SRType');
        }

        objFMCase=new FM_Case__c();
        System.debug('strAccountId---------'+strAccountId+'---strSelectedUnit--'+strSelectedUnit);

            instanceList = FM_Utility.getExistingFMCase( 'NOC_For_FitOut', strAccountId, strSelectedUnit );
            system.debug('instanceList---'+instanceList);



            system.debug('strFMCaseId---'+strFMCaseId);

            if((String.isNotBlank(strSelectedUnit)||
                String.isNotBlank(strFMCaseId)) &&
                instanceList.isEmpty()){
                init();

                // Create Contract Information records associated with fm case
                //System.debug('inside when strfmcase id not  null objFMCase.Number_of_Employees__c:---------'+objFMCase.Number_of_Employees__c);
               /* createCotractorInformation(
                     Integer.valueOf(objFMCase.Number_of_Employees__c)
                );*/
            }
            // Create Contract Information records associated with fm case
                //System.debug('~~~~~~~~~~objFMCase.Number_of_Employees__c:---------'+objFMCase.Number_of_Employees__c);


            if(!instanceList.isEmpty()){
                objFMCase.Account__c=instanceList[0].Account__c;
                objFMCase.status__c ='Draft Request';
                objFMcase.origin__c ='Walk-In';
                objFMCase.Booking_Unit__c=instanceList[0].Booking_unit__c;
                objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Work Permit').getRecordTypeId();
                objFMCase.Request_Type_DeveloperName__c = strSRType ;
                objFMCase.Request_Type__c = 'Work Permit';
                objFMCase.Description__c=instanceList[0].Description__c;
                objFMCase.Person_To_Collect__c=instanceList[0].Person_To_Collect__c;
                //objFMCase.Contact_person__c=instanceList[0].Contact_person__c;
                objFMCase.Contact_person_contractor__c=instanceList[0].Contact_person_contractor__c;
               // objFMCase.Mobile_no__c=instanceList[0].Mobile_no__c;
                objFMCase.Mobile_no_contractor__c=instanceList[0].Mobile_no_contractor__c;
                objFMCase.Contact_Email__c=instanceList[0].Contact_Email__c;
                //objFMCase.Office_tel__c=instanceList[0].Office_tel__c;
                objFMCase.Office_tel_contractor__c=instanceList[0].Office_tel_contractor__c;
                //objFMCase.Mobile_Country_Code__c=instanceList[0].Mobile_Country_Code__c;
                //objFMCase.Mobile_Country_Code_2__c=instanceList[0].Mobile_Country_Code_2__c;
                objFMCase.Mobile_Country_Code_3__c=instanceList[0].Mobile_Country_Code_3__c;
               // objFMCase.Mobile_Country_Code_4__c=instanceList[0].Mobile_Country_Code_4__c;
                objFMCase.Contractor_Type__c=instanceList[0].Contractor_Type__c;
                objFMCase.Parent_case__c = instanceList[0].Id;
                //objFMCase.Email__c = instanceList[0].Email__c;
                objFMCase.Email_2__c = instanceList[0].Email_2__c;
                objFMCase.Company_Name__c = instanceList[0].Company_Name__c;
                objFMCase.Number_of_Employees__c = instanceList[0].Number_of_Employees__c;
                System.debug('!!!!!!!!!!~objFMCase.Number_of_Employees__c:---------'+objFMCase.Number_of_Employees__c);



                if(!String.isEmpty(instanceList[0].Permit_To_Work_For__c)){
                    String[] fetchedValues=instanceList[0].Permit_To_Work_For__c.split(';');
                    valuesOfDrawings=fetchedValues;
                }
                objUnit = getUnitDetails(strSelectedUnit) ;
                if( String.isNotBlank( strSRType ) && objUnit != NULL ) {
                    processDocuments();
                }
                getfieldValues();
            }

            if(objUnit != NULL && objUnit.Inventory__r.Building_Location__c != NULL) {
                objList=[select Temporary_power_water_connection_Amount__c
                          , Property_name__r.Name
                          , Security_Deposit_Cheque_Amount__c
                          , Garbage_disposal_Amount__c
                       from Location__c
                       where id=:objUnit.Inventory__r.Building_Location__c];
            }

            if(!objList.isEmpty()) {

                securityChequeDepositAmount = objList[0].Security_Deposit_Cheque_Amount__c != NULL ? String.valueof(objList[0].Security_Deposit_Cheque_Amount__c) : '';
                garbageDisposalAmount = objList[0].Garbage_disposal_Amount__c!= NULL ? String.valueof(objList[0].Garbage_disposal_Amount__c) : '';
                temporaryPowerConnAmount = objList[0].Temporary_power_water_connection_Amount__c!= NULL ? String.valueof(objList[0].Temporary_power_water_connection_Amount__c) : '';

            }

            if( objUnit != NULL ) {
                try {
                    System.debug('objUnit.Registration_Id__c::'+objUnit.Registration_Id__c+'--objUnit.Inventory__r.Property_Code__c:'+objUnit.Inventory__r.Property_Code__c);
                    FmIpmsRestServices.DueInvoicesResult objResponse =FmIpmsRestServices.getDueInvoices( objUnit.Registration_Id__c,'',objUnit.Inventory__r.Property_Code__c );
                  system.debug('== objResult =='+objResponse);
                  if( objResponse != NULL ) {
                    objFMCase.Outstanding_service_charges__c = !String.isBlank( objResponse.totalDueAmount ) ? objResponse.totalDueAmount : '0';
                  }
                }
                catch( Exception e ) {
                    system.debug( '----Exception while fetching oustanding balance----'+e.getMessage() );
                }
            }

            System.debug('--- objFMCase Request_Type_DeveloperName__c --- : '+ objFMCase.Request_Type_DeveloperName__c);
            if(objFMCase != NULL && objFMCase.Request_Type_DeveloperName__c != NULL) {
                FM_process__mdt fmProcessMetadata=[select Minimum_Outstanding_Charge__c
                                                    from FM_process__mdt
                                                    where DeveloperName =:objFMCase.Request_Type_DeveloperName__c];
                if(fmProcessMetadata != NULL){
                  minimumOutstandingCharge = fmProcessMetadata.Minimum_Outstanding_Charge__c != NULL ? fmProcessMetadata.Minimum_Outstanding_Charge__c : 0;
              }
            }
    }

    /* This Method is used to create Instance of Contractor Information */
    public virtual void createContractorInformation(){
         ContractorInfoList = new List<Contractor_Information__c>();
        String NumberOfEmployees=Apexpages.currentPage().getParameters().get('noOfEmp');
        System.debug('Inside create Contract Info Method NumberOfEmployees----'+NumberOfEmployees);
        Integer NumberOfEmp = Integer.valueOf(NumberOfEmployees);
        if(NumberOfEmployees != NULL && NumberOfEmp > 0 && NumberOfEmployees !='--None--'){
            for(Integer count=0; count<NumberOfEmp; count++){
                Contractor_Information__c contractorObj = new Contractor_Information__c();
                System.debug('contractorObj:::::::'+contractorObj);
                ContractorInfoList.add(contractorObj);
            }
        }
        System.debug('------ContractorInfoList----'+ContractorInfoList.size());
        //return ContractorInfoList;
    }

    public virtual void init() {
        initializeFMCase();
        if(objFMCase.Submitted__c==true && objFMCase.Approval_Status__c != 'Rejected'){
            isEdittable=false;
        }
        if(strFMCaseId == null){
            //Initialize the Additional details map.
            //mapAdditionalDetails = new map<String, list<FM_Additional_Detail__c>>();
            //FM_Additional_Detail__c objDetail = new FM_Additional_Detail__c();
            //objDetail.RecordTypeId = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
            //objDetail.Emergency_Contact_Case__c = objFMCase.Id ;
            //mapAdditionalDetails.put( 'Emergency Contact' , new list<FM_Additional_Detail__c> { objDetail } ) ;
        }else{

            strSelectedUnit = objFMCase.Booking_Unit__c;

        }
        objUnit = getUnitDetails(strSelectedUnit);

        if( String.isNotBlank( strSRType ) && objUnit != NULL ) {
            processDocuments();
        }

        getfieldValues();

    }

    public void getfieldValues(){
        Schema.DescribeFieldResult fieldResult = FM_Case__c.Permit_To_Work_For__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

       for( Schema.PicklistEntry f : ple)
       {

           valueListOfDrawings.add(new SelectOption(f.getLabel(),f.getLabel()));
       }
    }

    public static Booking_Unit__c getUnitDetails( String strUnitId ) {

        if (String.isBlank(strUnitId)) {
            return NULL;
        }
        return  FM_Utility.getUnitDetails(strUnitId);
    }

    protected virtual FM_Case__c setCaseOrigin(FM_Case__c fmCase) {
        fmCase.Origin__c='Walk-In';
        return fmCase;
    }

    private void initializeFMCase() {
        if(strFMCaseId == null){
            objFMCase = new FM_Case__c();
            objFMCase = setCaseOrigin(objFMCase);
            objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Work Permit').getRecordTypeId();
            objFMCase.Account__c = strAccountId ;
            objFMCase.Booking_Unit__c = strSelectedUnit ;
            objFMCase.Request_Type_DeveloperName__c = strSRType ;
            objFMCase.Request_Type__c = 'Work Permit';
            objFMCase.status__c = 'Draft Request';
        }else{

            objFMCase = FM_Utility.getCaseDetails(strFMCaseId);
            if(!String.isEmpty(objFMCase.Permit_To_Work_For__c)){
                String[] fetchedValuesAfterSaveAsDraft=objFMCase.Permit_To_Work_For__c.split(';');
                valuesOfDrawings=fetchedValuesAfterSaveAsDraft;
            }

            strAccountId = objFMCase.Account__c ;
            strSelectedUnit = objFMCase.Booking_Unit__c ;
            strSRType = objFMCase.Request_Type_DeveloperName__c ;

        }
    }



    public void notifyContractorConsultant() {
    try{

          if( objFMCase.Id == NULL  ) {

            createMethod();
          }
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, contractorConsultantSuccessMessage));
          FM_NotifyGuestUserController objNotification = new FM_NotifyGuestUserController( new ApexPages.StandardController( objFMCase ) );
          objNotification.createSMSHistoryRecord();
        }
     catch( Exception e ) {
            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, e.getMessage() ) );
        }
    }

    public virtual PageReference createRequestForWorkPermit() {
        if( objFMCase != NULL ) {

            createMethod();
            List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
            String guestUserProfileName='';
            if(!PROFILE.isEmpty()){
                guestUserProfileName= PROFILE[0].Name;
            }
            if(guestUserProfileName=='Facilities Management Site Profile'){
                PageReference pgRfSaved=Page.ThankYouPageSiteRedirect;
                pgRfSaved.setRedirect(true);
                savedOrSubmitted='saved';
                pgRfSaved.getParameters().put('savedOrSubmitted',savedOrSubmitted);
                return pgRfSaved;
            }else{
                PageReference objPage = new PageReference('/' + objFMCase.Id );
                return objPage ;
            }
        }
        return NULL ;
    }

    public void createCaseShowUploadDoc(){
        createMethod();

        if( objFMCase != NULL && objFMCase.Id != NULL ) {
            processDocuments();
        }

        strFMCaseId=objFMCase.id;

    }
    public virtual void createMethod(){
            //objFMCase.Origin__c='Walk-In';
            //objFMCase.status__c='New';
            objFMCase.Permit_To_Work_For__c=String.join(valuesOfDrawings ,';');
            if(!String.isBlank(objFMCase.Email__c) || !String.isBlank(objFMCase.Email_2__c)){
                objFMCase.Contact_Email__c = !String.isBlank(objFMCase.Email__c) ? objFMCase.Email__c : objFMCase.Email_2__c ;
            }
            objFMCase.Security_Deposit_Cheque_Amount__c = !String.isBlank(securityChequeDepositAmount) ? Decimal.valueOf(securityChequeDepositAmount) : 0;
            objFMCase.Garbage_disposal_Amount__c = !String.isBlank(garbageDisposalAmount) ? Decimal.valueOf(garbageDisposalAmount) : 0;
            objFMCase.Temporary_power_water_connection_Amount__c = !String.isBlank(temporaryPowerConnAmount) ? Decimal.valueOf(temporaryPowerConnAmount) : 0;
            upsert objFMCase ;
    }

    public virtual PageReference submitRequestForWorkPermit() {
        if( objFMCase != NULL ) {

            if(objFMCase.id==null){
                insert objFMCase;

                // Insert Contractor Information Records
                if(ContractorInfoList != NULL && !ContractorInfoList.isEmpty()){
                    for(Contractor_Information__c contractorObj : ContractorInfoList){
                        contractorObj.FM_Case__c = objFMCase.Id;
                    }
                    insert ContractorInfoList;
                }
            }
            else{
                 // Insert Contractor Information Records
                 if(ContractorInfoList != NULL && !ContractorInfoList.isEmpty()){
                    for(Contractor_Information__c contractorObj : ContractorInfoList){
                        contractorObj.FM_Case__c = objFMCase.Id;
                    }
                    insert ContractorInfoList;
                 }
            }

            if(!String.isBlank(objFMCase.Email__c) || !String.isBlank(objFMCase.Email_2__c)){
                objFMCase.Contact_Email__c = !String.isBlank(objFMCase.Email__c) ? objFMCase.Email__c : objFMCase.Email_2__c ;
            }

            objFMCase.Security_Deposit_Cheque_Amount__c = !String.isBlank(securityChequeDepositAmount) ? Decimal.valueOf(securityChequeDepositAmount) : 0;
            objFMCase.Garbage_disposal_Amount__c = !String.isBlank(garbageDisposalAmount) ? Decimal.valueOf(garbageDisposalAmount) : 0;
            objFMCase.Temporary_power_water_connection_Amount__c = !String.isBlank(temporaryPowerConnAmount) ? Decimal.valueOf(temporaryPowerConnAmount) : 0;
            objFMCase.Permit_To_Work_For__c=String.join(valuesOfDrawings ,';');
            objFMCase.Status__c='Submitted';
            objFMCase.Submitted__c=true;


            if(objFMCase.Submitted__c==true){

                 if( String.isNotBlank( objUnit.Unit_Name__c ) ) {
                    list<FM_User__c> lstUsers = FM_Utility.getApprovingUsers( new set<String> { objUnit.Unit_Name__c.split('/')[0] } );

                    for( FM_User__c objUser : lstUsers ) {
                        if( String.isNotBlank( objUser.FM_Role__c ) ) {
                          if( objUser.FM_Role__c == 'FM Manager') {
                            objFMCase.FM_Manager_Email__c = objUser.FM_User__r.Email;
                          }
                          else if( objUser.FM_Role__c =='FM Admin' &&
                               objFMCase.Admin__c == NULL ) {
                            objFMCase.Admin__c = objUser.FM_User__c;
                          }else if(objuser.FM_Role__c == 'Property Manager'){
                              objFMCase.Property_Manager_Name__c = objUser.FM_User__r.Name;
                              objFMCase.Property_Manager_Email__c = objUser.FM_User__r.Email;
                          }else if(objuser.FM_Role__c == 'MC Security Manager'){
                              objFMCase.Email_5__c = objUser.FM_User__r.Email;
                          }
                        }
                    }
                  }


                String strApprovingUsers='';

                List<FM_Approver__mdt> listApproverUser=FM_Utility.fetchApprovers(objFMCase.Request_Type_DeveloperName__c,objUnit.Property_City__c);

                if(!listApproverUser.isEmpty()){
                    System.debug('----1----');
                    for(FM_Approver__mdt fmApproverInstance:listApproverUser){
                        System.debug('----2----');
                        strApprovingUsers=strApprovingUsers+fmApproverInstance.Role__c+',';
                    }

                    strApprovingUsers=strApprovingUsers.removeEnd(',');
                    objFMCase.Approving_Authorities__c=strApprovingUsers;
                    objFMCase.Approval_Status__c='Pending';
                    //objFMCase.Submit_for_Approval__c=true;
                    update objFMCase;
                }

            }

            List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
            String guestUserProfileName='';
            if(!PROFILE.isEmpty()){
                guestUserProfileName= PROFILE[0].Name;
            }
            if(guestUserProfileName=='Facilities Management Site Profile'){
                PageReference pgRf=Page.ThankYouPageSiteRedirect;
                pgRf.setRedirect(true);
                savedOrSubmitted='submitted';
                pgRf.getParameters().put('savedOrSubmitted',savedOrSubmitted);
                return pgRf;
            }else{
                PageReference objPage = new PageReference('/' + objFMCase.Id );
                return objPage ;
            }

        }

        return NULL ;
    }


    public String openTermsAndConditionWindow(){
        urlForTermsAndCondition = PenaltyWaiverUtility.fetchHelpDocURL('Work_Permit_Guidelines');
        urlForTermsAndCondition = !String.isEmpty(urlForTermsAndCondition) ? urlForTermsAndCondition : '' ;
        return urlForTermsAndCondition;
    }

    @testVisible
    public void processDocuments() {
        if(String.isBlank(strSRType)){
            strSRType=ApexPages.currentPage().getParameters().get('SRType');
        }
        lstUploadedDocs = NULL ;
        lstDocuments = NULL ;
        Set<FM_Documents__mdt> listSpecificDocs=new Set<FM_Documents__mdt>();
        // List<String> lstOfString=new List<String>();
        if( objFMCase != NULL && objFMCase.Id != NULL ) {
            mapUploadedDocs = new map<String, SR_Attachments__c>();
            for( SR_Attachments__c objAttach : fetchUploadedDocs() ) {
                mapUploadedDocs.put( objAttach.Name, objAttach );
            }
            if( mapUploadedDocs != NULL && !mapUploadedDocs.isEMpty() ) {
                lstUploadedDocs = new list<SR_Attachments__c>();
                lstUploadedDocs.addAll( mapUploadedDocs.values() );
            }
        }
        mapProcessDocuments = new map< String, FM_Documents__mdt >();
        lstDocuments = new list<FM_Documents__mdt>();
        system.debug('maze objDocMeta bahercha'+FM_Utility.getDocumentsList( strSRType, objUnit.Property_City__c ));
        for( FM_Documents__mdt objDocMeta : FM_Utility.getDocumentsList( strSRType, objUnit.Property_City__c ) ) {
            system.debug('maze objDocMeta'+objDocMeta);
            mapProcessDocuments.put( objDocMeta.DeveloperName, objDocMeta );

            if( mapUploadedDocs != NULL && !mapUploadedDocs.containsKey( objDocMeta.MasterLabel ) ) {
                system.debug('maze objDocMeta aatla'+objDocMeta);
                lstDocuments.add( objDocMeta );
            }
        }
        for(FM_Documents__mdt ins:lstDocuments){
            if(!mapUploadedDocs.keyset().contains(ins.Document_Name__c)) {
                // if(objFMCase.Work_Permit_Type__c == 'Work Permit - Including Fit Out'
                //     && ins.DeveloperName == 'NOC_WP'){
                //     ins.Mandatory__c = True;
                // }
                if(ins.Mandatory__c==true){
                    isMandatory=true;
                    // lstOfString.add('true');
                }
                if(objFMCase.Work_Permit_Type__c != NULL && ins.NOC_Type__c.contains(objFMCase.Work_Permit_Type__c)) {
                    listSpecificDocs.add(ins);
                }
            }//if
        }//for
        // if(lstOfString.size()>0){
        //     isMandatory=true;
        // }
        lstDocuments.clear();
        lstDocuments.addAll(listSpecificDocs);
        system.debug('maze docs'+lstDocuments);
    }

    public void deleteAttachment() {
        if( String.isNotBlank( deleteAttRecId ) ) {
           delete new SR_Attachments__c( Id = deleteAttRecId );
           processDocuments();
        }
    }

    @testVisible
    private void insertCustomAttachment( Id fmCaseId, UploadMultipleDocController.data objResponse ) {
        list<SR_Attachments__c> lstCustomAttachments = new list<SR_Attachments__c>();

        strDocumentName = strDocumentName.substring( 0, strDocumentName.lastIndexOf('.') );

        FM_Documents__mdt objDocMeta = mapProcessDocuments != null && mapProcessDocuments.containsKey( strDocumentName ) ?
                                       mapProcessDocuments.get( strDocumentName ) :
                                       NULL ;

        for( UploadMultipleDocController.MultipleDocResponse objFile : objResponse.data ) {
            SR_Attachments__c objCustAttach = new SR_Attachments__c();
            objCustAttach.Account__c = strAccountId ;
            objCustAttach.Attachment_URL__c = objFile.url;
            objCustAttach.Booking_Unit__c = strSelectedUnit ;
            objCustAttach.FM_Case__c = fmCaseId ;
            objCustAttach.Name = objDocMeta != NULL ? objDocMeta.MasterLabel : '' ;
            objCustAttach.Type__c = objDocMeta != NULL ? objDocMeta.Type__c : '' ;
            objCustAttach.isValid__c = true ;
            lstCustomAttachments.add( objCustAttach );
            strDocumentName = '';
            strDocumentBody = '';
        }

        if( !lstCustomAttachments.isEmpty() ) {
            insert lstCustomAttachments ;
            processDocuments() ;
        }

    }

    public list<SR_Attachments__c> fetchUploadedDocs() {
        System.debug('>>> objFMCase.Id : '+ objFMCase.Id);
        if( String.isNotBlank( objFMCase.Id ) ) {
            return [ SELECT Id
                          , Type__c
                          , isValid__c
                          , Attachment_URL__c
                          , Name
                       FROM SR_Attachments__c
                      WHERE FM_Case__c =: objFMCase.Id ];
        }
        return new list<SR_Attachments__c>();
    }

    public void uploadDocument() {

        if( objFMCase != NULL && objFMCase.Id != NULL ) {
            System.debug('=========='+objFMCase != NULL && objFMCase.Id != NULL);
            UploadMultipleDocController.data objResponse = new UploadMultipleDocController.data();
            list<UploadMultipleDocController.MultipleDocRequest> lstWrapper = new list<UploadMultipleDocController.MultipleDocRequest>();
            if( String.isNotBlank( strDocumentName ) && String.isNotBlank( strDocumentBody ) ) {
                lstWrapper.add( PenaltyWaiverHelper.makeWrapperObject( EncodingUtil.Base64Encode( RentalPoolTerminationHelper.extractBody( strDocumentBody ) ) ,
                                                                       strDocumentName ,
                                                                       strDocumentName ,
                                                                       objUnit.Registration_Id__c, objFMCase.Id, '1' ) );

            }
            if( !lstWrapper.isEmpty() ) {
                objResponse = PenaltyWaiverService.uploadDocumentsOnCentralRepo( lstWrapper );
                system.debug('== objResponse document upload =='+objResponse);
                if( objResponse != NULL && objResponse.data != NULL ) {
                    insertCustomAttachment( objFMCase.Id, objResponse );
                }
            }
        }
    }

}