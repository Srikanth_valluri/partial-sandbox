/**
 * @File Name          : MortgageTaskClosureProcess.cls
 * @Description        : Invoked from  "Case : CRM Mortgage Task Close Process" builder.
 * @Author             : Dipika Rajput
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 6/20/2019, 6:20:49 PM
 * @Modification Log   : 
 *============================================================================================================
 * Ver         Date                     Author                    Modification
 *============================================================================================================
 * 1.0    6/19/2019     Dipika Rajput       Initial Version
 * 1.1    6/19/2019     Aishwarya Todkar    Capture Mortgage Equity flag
*=============================================================================================================*/
public class MortgageTaskClosureProcess { 
    
    /* This method is used to callout in IPMS to send Mortgage M flag once (Verify details in System, 
    *   Stamp Lien Letter Handover original Lien Letter to Mortgage Adviser.)closure of task.
    */
    @InvocableMethod
    public static void calloutToSendMortgageMFlagOnTaskClosure(List<Task> closedTaskLst) {
        
        // Invoke Apex_Class_ON_OFF_Setting and check wether that class is on/off.
        Boolean apexOnOffSetting = getApexSettingTocheck('MortgageTaskClosureProcess');

        if(apexOnOffSetting){
            System.debug('closedTaskLst:::::::::'+closedTaskLst);
            // Set to store all the cases.
            Set<Id> setCasesIds = new Set<Id>();
        
        // Fetch cases ferom the task list.
        for( Task objTask : closedTaskLst ) {
            if( objTask.WhatId != null && 
                String.valueOf( objTask.WhatId ).startsWith('500') && 
                String.isNotBlank( objTask.Subject ) &&
                objTask.Subject.equalsIgnoreCase(Label.Mortgage_15) &&
                objTask.Assigned_User__c != null && objTask.Assigned_User__c.equalsIgnoreCase('CDC') &&
                ( objTask.Status == 'Closed' || objTask.Status == 'Completed') ) {
                setCasesIds.add( objTask.WhatId );
            }
        }
        System.debug('~~~~~~~~setCasesIds:::::::::'+setCasesIds);
            calloutInfutureToSendMortgageFlag(setCasesIds);
        }
    }
    
    /* Call this Future method to avoid unhandled work pednding issue*/
    @future(callout=true)
    public static void calloutInfutureToSendMortgageFlag(set<Id>setCasesIds){
       
        if( !setCasesIds.isEmpty() ) {
            list<Case> lstOfCases = [ SELECT Id
                                            , CaseNumber
                                            , Booking_Unit__r.Registration_ID__c
                                            , Submit_Date__c
                                            , Mortgage_M_Flag_Date__c
                                            , Loan_Offer_Date__c
                                            , NOC_Request_Date__c
                                            , Loan_Amount__c
                                            , Bank_Code__c
                                            , NOC_Received_Date__c
                                            , Mortgage_Flag__c
                                            , Bank_Name__c
                                            , Loan_Agreement_Date__c
                                            , Registration_ID__c
                                            , Mortgage_Equity__c
                                            FROM Case
                                        WHERE Id IN :setCasesIds ] ;

            for( Case caseObj : lstOfCases ) {
                if (String.isNotBlank( caseObj.Mortgage_Equity__c ) 
                && caseObj.Mortgage_Equity__c.equalsIgnoreCase( 'yes' ) ) {
                    caseObj.Mortgage_Flag__c = Label.Mortgage_Task_Mortgage_Equity;
                } else {
                    caseObj.Mortgage_Flag__c = Label.Mortgage_Task_Closure_Flag;
                }
                
                caseObj.Mortgage_M_Flag_Date__c = System.today() ;
            }
            System.debug('~~~~~~~~lstOfCases:::::::::'+lstOfCases);
        
            // Callout to Send M flag and all other details to update in IPMS side.
            MortgageProcessCallout.sendbankDetails(lstOfCases[0],NULL,NULL);

            // Update List of cases with M mortgage flag.
            update lstOfCases ;
            System.debug('~~~~~after update~~~lstOfCases:::::::::'+lstOfCases);
            
        }
    }

    
    public static boolean getApexSettingTocheck(String apexclassName){
        
        // Invoke Apex_Class_ON_OFF_Setting and check wether that class is on/off.
        Map<String,Apex_Class_ON_OFF_Setting__c> apexClassSettingMap =  Apex_Class_ON_OFF_Setting__c.getAll();
        System.debug('apexClassSettingMap::::'+apexClassSettingMap); 
        Boolean apexOnOffSetting;
        if(apexClassSettingMap != NULL && apexClassSettingMap.get(apexclassName) != NULL ){
            apexOnOffSetting = apexClassSettingMap.get(apexclassName).ON_OFF__c;
            System.debug('apexOnOffSetting:::'+apexOnOffSetting);
        }
       
        return apexOnOffSetting;
    }
}