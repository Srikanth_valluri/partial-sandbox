public without sharing class AssignmentFetchUnitDetails {
    public static map<Id,Booking_Unit__c> fetchUnitDetails(set<Id> setBookingUnitId){
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        String contractQuery = 'SELECT';
        for(Schema.SObjectField sfield : schemaMap.get('Booking_Unit__c').getDescribe().fields.getMap().values()) {
            Schema.describefieldresult dfield = sfield.getDescribe();
            contractQuery += ' ';
            contractQuery += dfield.getName();
            contractQuery += ', ';
        }
        contractQuery = contractQuery.removeEnd(', ');
        contractQuery += ' FROM Booking_Unit__c where Id IN : setBookingUnitId';
        system.debug('contractQuery***********'+contractQuery);
        map<Id,Booking_Unit__c> mapId_Unit = new map<Id,Booking_Unit__c>();
        List<Booking_Unit__c> sobjList = Database.query(contractQuery);
        for(Booking_Unit__c objBU : sobjList){
            mapId_Unit.put(objBU.Id, objBU);
        }
        /*
        map<Id,Booking_Unit__c> mapId_Unit = new map<Id,Booking_Unit__c>([Select b.tBits_Ready__c, b.Vendor_ID__c, b.Value__c, b.Utility_Charges__c
                                                   , b.Unpaid_Parking__c, b.Unit_s_Current_Status__c, b.Unit__c
                                                   , b.Unit_Type__c, b.Unit_Selling_Price__c
                                                   , b.Unit_Selling_Price_AED__c, b.Unit_Plan__c, b.Unit_Name__c
                                                   , b.Unit_Location__c, b.Unit_Location_ID__c, b.Unit_Details__c
                                                   , b.Unique_Key__c, b.Under_Assignment__c, b.UAT_Testing_Data__c
                                                   , b.Total_due_Overdue__c, b.Total_Collections__c, b.Token_Paid__c
                                                   , b.Token_Paid_Time__c, b.Token_Deposit_Refunded__c
                                                   , b.Token_Amount_val__c, b.Token_Amount__c, b.Title_Deed__c
                                                   , b.Title_Deed_Issued_Date__c, b.SystemModstamp, b.Stores_the_RP__c
                                                   , b.Status__c, b.Special_Price__c, b.Size__c, b.Service_Charges__c
                                                   , b.Send_Payment_Link__c, b.Selling_Price__c, b.Selling_Price_Sq__c
                                                   , b.Seller_Information__c, b.Scheme__c, b.Sales_Office__c
                                                   , b.SR_Status_Code__c, b.SR_Id__c, b.SR_Handover_Status__c
                                                   , b.SPA_Version_No__c, b.SPA_Url__c, b.SPA_Anticipated_Completion_Date__c
                                                   , b.SOA_Stage_Id__c, b.SOA_Request_ID__c, b.Revised_Price_Area_Variation__c
                                                   , b.Requested_Token_Amount__c, b.Requested_Price__c, b.Requested_Price_Approval_Level__c
                                                   , b.Requested_Price_AED__c, b.Rental_Pool__c, b.Rental_Pool_Type__c
                                                   , b.Rental_Pool_Status__c, b.Rental_Pool_Start_Date__c, b.Rental_Pool_End_Date__c
                                                   , b.Rental_Pool_Agreement__c, b.Release_Check__c, b.Related_Promotion__c
                                                   , b.Related_Promotion_Title__c, b.Related_Campaign__c, b.Related_Campaign_Name__c
                                                   , b.Regular_Bulk_Flag__c, b.Registration_Status__c, b.Registration_Status_Code__c
                                                   , b.Registration_ID__c, b.Registration_DateTime__c, b.Recovery__c, b.Receipt_Stage_Id__c
                                                   , b.Receipt_ID__c, b.Rebate_Given__c, b.Reason_for_not_availing_Offers__c, b.Re_Assigned__c
                                                   , b.RERA_Project__c, b.Property_Name__c, b.Property_Name_Inventory__c, b.Property_Id__c
                                                   , b.Property_Country__c, b.Property_Consultant__c, b.Proof_of_Payment_Submitted__c
                                                   , b.Promoter_Name__c, b.Primary_Buyer_s_Nationality__c, b.Primary_Buyer_s_Name__c
                                                   , b.Primary_Buyer_s_Email__c, b.Primary_Buyer_Country__c, b.Price_when_DP_OK__c
                                                   , b.Price_per_sq_ft__c, b.Previous_Booking_Unit__c, b.Prelaunch__c, b.Plot_Price__c, b.Phone__c
                                                   , b.Permitted_Use__c, b.Penalty_Amount__c, b.Payment_Reschedule_Count__c, b.Payment_Received__c
                                                   , b.Payment_Plan_Changed__c, b.Payment_Method__c, b.Passport_Number__c, b.Passport_Expiry_Date__c
                                                   , b.Party_Id__c, b.Parking_Bay_No__c, b.Paid_Parking__c, b.POA_Guardian_Auth_Signatory__c
                                                   , b.POA_Guardian_Auth_Sign_AR__c, b.PCC_Release__c, b.PCC_Generate__c
                                                   , b.Overdues_as_per_Collectable_amount__c, b.Org_ID__c, b.Online_Payment_Party__c, b.Old_Doc_Ok__c
                                                   , b.Okay_to_Generate_the_Call__c, b.Okay_to_Generate_the_Call_Date__c, b.Offers_Available__c
                                                   , b.OQOOD_Reg_Flag__c, b.OK_to_Print__c, b.No_of_parking__c, b.Next_cash_call__c, b.Nationality__c
                                                   , b.Name, b.NOC_Issued_Date__c, b.NI_Payment_URL__c, b.Mortgage__c, b.Mortgage_Applied__c, b.Max_GFA__c
                                                   , b.Manager_Name__c, b.Manager_IPMS_Employee_ID__c, b.Manager_HR_Employee_ID__c, b.MD_Price__c
                                                   , b.Litigation__c, b.Letter_of_discharge__c, b.Legal_Remarks__c, b.Legal_Doc_Rec_Date__c
                                                   , b.Lease_Expiry_Date__c, b.Lease_Commencement_Date__c, b.Last_Payment_Date__c, b.Last_Name__c
                                                   , b.LastViewedDate, b.LastReferencedDate, b.LastModifiedDate, b.LastModifiedById, b.LastActivityDate
                                                   , b.Key_Handover__c, b.JOPD_Area__c, b.Is_Siebel_Migrated_Data__c, b.Is_Same_Area__c, b.IsDeleted
                                                   , b.Invoice_Sent__c, b.Inventory__c, b.Inventory_Area__c, b.Incomplete_Activities_List__c, b.Id
                                                   , b.IPMS_Status__c, b.IPMS_Employee_ID__c, b.Heard_of_DAMAC__c, b.Handover_Readiness_Checklist__c
                                                   , b.Handover_Notice_Sent_Date__c, b.Handover_Flag__c, b.Handover_Date__c, b.Handover_Complete__c
                                                   , b.Handed_Over__c, b.HR_Employee_ID__c, b.HO_Pack_Url__c, b.HOS_Name__c, b.HOD_Name__c
                                                   , b.Grace_Period__c, b.Good_Standing__c, b.Generate_Handover_Pack__c, b.General_Remarks__c
                                                   , b.Furniture_Package__c, b.Fullcopy_SFId__c, b.Floor_Plan__c, b.Floor_Name__c, b.First_Name__c
                                                   , b.Finance_Flag__c, b.Final_Invoice__c, b.FM_PCC__c, b.Extra_Option_2__c, b.Extra_Option_1__c
                                                   , b.Extra_Amount_2__c, b.Extra_Amount_1__c, b.External_Parking__c, b.Enforcement__c, b.Email__c
                                                   , b.Eligible_For_Rental_Pool__c, b.Eligible_For_Early_Handover__c, b.Early_Handover__c
                                                   , b.Early_Handover_Date__c, b.Earliest_Viewing_Date__c, b.Document_Received__c, b.Doc_OK__c, b.District__c
                                                   , b.Dispute__c, b.Dispute_Flag__c, b.Discount__c, b.Deposit_Received__c, b.Delivery_Mode__c
                                                   , b.Deferred_Until_Date__c, b.Default__c, b.Deal__c, b.Deal_Sr_Id__c, b.Date_of_Birth__c, b.DSR_Flag__c
                                                   , b.DP_Overdue__c, b.DP_OK__c, b.DP_Due_Date__c, b.DLP_Validity__c, b.DLD_registration__c
                                                   , b.DEP_REC_Date__c, b.DAMAC_Sign__c, b.Customer_s_Interest_on_Rental_Pool__c, b.Customer_Status__c
                                                   , b.Customer_Sign__c, b.Customer_Number__c, b.Current_Price_Sft__c, b.CurrencyIsoCode, b.CreatedDate
                                                   , b.CreatedById, b.Counter_Case__c, b.Conversion_Rebate__c, b.Conversion_Rate__c, b.Contract_Status__c
                                                   , b.Construction_Status__c, b.Construction_RERA__c, b.Communications_to_be_Sent__c
                                                   , b.Collectable_as_per_RERA__c, b.Car_Given__c, b.CRM_Remarks__c, b.Building_Name__c, b.Booking__c
                                                   , b.Booking_Unit_Type__c, b.Booking_Unit_No__c, b.Booking_Undergoing_Termination__c, b.Booking_Type__c
                                                   , b.Booking_Location__c, b.Booking_Amended_Date__c, b.Booking_Amended_By__c, b.Billable_Amount__c
                                                   , b.Below_Discount__c, b.Bedroom_Type__c, b.Base_Discounted_Price__c, b.Availed_Offers__c, b.Area__c
                                                   , b.Area_Varied__c, b.Area_Variation__c, b.Approved_By_HOD__c, b.Anticipated_Completion_Date__c
                                                   , b.Agreement_Status__c, b.Agreement_ID__c, b.Agreement_Date__c, b.Agreed_Handover_Date__c
                                                   , b.Agent_Name__c, b.Agency_Name__c, b.Address_Verified__c, b.Address_Verified_Date__c, b.Account_Id__c 
                                               From Booking_Unit__c b
                                               where Id IN : setBookingUnitId]);                                     
        */
        return mapId_Unit;
    }
}