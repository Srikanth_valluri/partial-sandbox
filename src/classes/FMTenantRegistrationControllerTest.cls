@isTest
public class FMTenantRegistrationControllerTest { 
     public static testmethod void testCreateFMCase(){
        Account acctIns=TestDataFactoryFM.createAccount();
        acctIns.Nationality__pc = 'Pakistani';
        acctIns.Passport_Number__pc = '2314515';
        insert acctIns;
        
       
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
        
        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;
        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;
        
        PageReference myVfPage = Page.TenantRegistrationProcessPage;
        
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Nationality__c ='Pakistani';
        fmCaseObj.Passport_Number__c = '2314515';
        fmCaseObj.Tenant__c = acctIns.id;
        insert fmCaseObj;

        SR_Attachments__c objAttach = new SR_Attachments__c();
        objAttach.FM_Case__c = fmCaseObj.id;
        objAttach.Name = 'tets';
        insert objAttach;

        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Tenant_Registration');
        
        FMTenantRegistrationController obj=new FMTenantRegistrationController();
         obj.insertCase();
        //obj.objFMCase=fmCaseObj;
        obj.initializeFMCaseAddDetails();
        obj.objFMCase = fmCaseObj;
        obj.createFmCase();
    }
    
    public static testmethod void submitFMCase(){
        Id profileId = [select id from profile where name = 'System Administrator'].id;
        User u=TestDataFactoryFM.createUser(profileId);
        insert u;
        
        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;
       
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;
        
        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;
        
        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;
        
        /*FM_User__c fmUser=TestDataFactoryFM.createFMUser(u,locObj);
        fmUser.FM_Role__c='FM Manager';
        insert fmUser;
        
        FM_User__c fmUser1=TestDataFactoryFM.createFMUser(u,locObj);
        fmUser1.FM_Role__c='FM Admin';
        insert fmUser1;

        FM_User__c fmUser2=TestDataFactoryFM.createFMUser(u,locObj);
        fmUser2.FM_Role__c='Property Manager';
        insert fmUser2;*/
        PageReference myVfPage = Page.TenantRegistrationProcessPage;
        
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Tenant_Registration';
        fmCaseObj.Booking_Unit__c=buIns.id;
        fmCaseObj.Account__c=acctIns.id;
        //fmCaseObj.Approving_Authorities__c='FM Manager';
        fmCaseObj.Nationality__c ='Pakistani';
        fmCaseObj.Mobile_no__c = '123456789';
        fmCaseObj.Outstanding_service_charges__c = '0';
        fmCaseObj.Move_in_date__c = System.today();
        insert fmCaseObj;
        
        FM_Additional_Detail__c  instanceEMC=new FM_Additional_Detail__c();
        instanceEMC.Emergency_Contact_Case__c=fmCaseObj.id;
        instanceEMC.RecordTypeId=Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
        instanceEMC.Name__c='Test';
        instanceEMC.Relationship__c='Test';
        instanceEMC.Phone__c='121212';
        instanceEMC.Email__c='test@test.com';
        insert instanceEMC;
        
        FM_Additional_Detail__c  instanceRC=new FM_Additional_Detail__c();
        instanceRC.Resident_Case__c =fmCaseObj.id;
        instanceRC.Name__c='Test';
        instanceRC.Phone__c='121212';
        instanceRC.Email__c='test@test.com';
        instanceRC.Age__c=12;
        instanceRC.Nationality__c='Pakistani';
        instanceRC.Passport_Number__c='121212';
        instanceRC.RecordTypeId=Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Resident').getRecordTypeId();
        insert instanceRC;
        
        FM_Additional_Detail__c  instanceVC=new FM_Additional_Detail__c();
        instanceVC.Vehicle_Case__c =fmCaseObj.id;
        instanceVC.Vehicle_Make_Model__c='test';
        instanceVC.Vehicle_Colour__c='test';
        instanceVC.Parking_Slot_Number__c='1';
        instanceVC.Vehicle_Number__c='1';
        instanceVC.Identification_Number__c='1';
        instanceVC.RecordTypeId=Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Vehicle').getRecordTypeId();
        insert instanceVC;

        FM_Additional_Detail__c  instancePC=new FM_Additional_Detail__c();
        instancePC.Pet_Case__c =fmCaseObj.id;
        instancePC.Pet_Name__c='test';
        instancePC.Pet_Type__c='test';
        instancePC.RecordTypeId=Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Pet Details').getRecordTypeId();
        insert instancePC;
        
        Test.setCurrentPage(myVfPage);

        FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');
        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
            'https://api.sendgrid.com/v3/mail/send' => sendGridResponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));

        //ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        //ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        //ApexPages.currentPage().getParameters().put('SRType','Tenant_Registration');
        ApexPages.currentPage().getParameters().put('Id',fmCaseObj.id);
        
        FMTenantRegistrationController obj=new FMTenantRegistrationController();
        
        //obj.objFMCase.id=fmCaseObj.id;
        //obj.initializeFMCaseAddDetails();
        test.startTest();
        obj.strDetailType='Emergency Contact';
        obj.addDetails();
        obj.indexOfNewChildToRemove=0;
        obj.removeDetails();
        obj.objFMCase = FM_Utility.getCaseDetails( fmCaseObj.Id );
        obj.submitFmCase();
        //obj.returnBackToCasePage();
        test.stopTest();
    }

    public static testmethod void submitFMCase1(){
        Id profileId = [select id from profile where name = 'System Administrator'].id;
        User u=TestDataFactoryFM.createUser(profileId);
        insert u;
        
        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;
       
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;
        
        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;
        
        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;

        
        FM_User__c fmUser=TestDataFactoryFM.createFMUser(u,locObj);
        fmUser.FM_Role__c='FM Manager';
        insert fmUser;
        
        FM_User__c fmUser1=TestDataFactoryFM.createFMUser(u,locObj);
        fmUser1.FM_Role__c='FM Admin';
        insert fmUser1;

        FM_User__c fmUser2=TestDataFactoryFM.createFMUser(u,locObj);
        fmUser2.FM_Role__c='Property Manager';
        insert fmUser2;

        PageReference myVfPage = Page.TenantRegistrationProcessPage;
        
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Tenant_Registration';
        fmCaseObj.Booking_Unit__c=buIns.id;
        fmCaseObj.Account__c=acctIns.id;
        //fmCaseObj.Approving_Authorities__c='FM Manager';
        fmCaseObj.Nationality__c ='Pakistani';
        fmCaseObj.Mobile_no__c = '123456789';
        fmCaseObj.Outstanding_service_charges__c = '0';
        fmCaseObj.No_of_Adults__c = '5';
        insert fmCaseObj;

        FM_Additional_Detail__c  instanceRC=new FM_Additional_Detail__c();
        instanceRC.Resident_Case__c =fmCaseObj.id;
        instanceRC.Name__c='Test';
        instanceRC.Phone__c='121212';
        instanceRC.Email__c='test@test.com';
        instanceRC.Age__c=12;
        instanceRC.Nationality__c='Pakistani';
        instanceRC.Passport_Number__c='121212';
        instanceRC.RecordTypeId=Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Resident').getRecordTypeId();
        insert instanceRC;
        
        
        /*
        FM_Additional_Detail__c  instanceEMC=new FM_Additional_Detail__c();
        instanceEMC.Emergency_Contact_Case__c=fmCaseObj.id;
        instanceEMC.RecordTypeId=Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
        instanceEMC.Name__c='Test';
        instanceEMC.Relationship__c='Test';
        instanceEMC.Phone__c='121212';
        instanceEMC.Email__c='test@test.com';
        insert instanceEMC;
        
        FM_Additional_Detail__c  instanceRC=new FM_Additional_Detail__c();
        instanceRC.Resident_Case__c =fmCaseObj.id;
        instanceRC.Name__c='Test';
        instanceRC.Phone__c='121212';
        instanceRC.Email__c='test@test.com';
        instanceRC.Age__c=12;
        instanceRC.Nationality__c='Pakistani';
        instanceRC.Passport_Number__c='121212';
        instanceRC.RecordTypeId=Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Resident').getRecordTypeId();
        insert instanceRC;
        
        FM_Additional_Detail__c  instanceVC=new FM_Additional_Detail__c();
        instanceVC.Vehicle_Case__c =fmCaseObj.id;
        instanceVC.Vehicle_Make_Model__c='test';
        instanceVC.Vehicle_Colour__c='test';
        instanceVC.Parking_Slot_Number__c='1';
        instanceVC.Vehicle_Number__c='1';
        instanceVC.Identification_Number__c='1';
        instanceVC.RecordTypeId=Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Vehicle').getRecordTypeId();
        insert instanceVC;

        FM_Additional_Detail__c  instancePC=new FM_Additional_Detail__c();
        instancePC.Pet_Case__c =fmCaseObj.id;
        instancePC.Pet_Name__c='test';
        instancePC.Pet_Type__c='test';
        instancePC.RecordTypeId=Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Pet Details').getRecordTypeId();
        insert instancePC;
        */
        Test.setCurrentPage(myVfPage);
        //ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        //ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        //ApexPages.currentPage().getParameters().put('SRType','Tenant_Registration');
        ApexPages.currentPage().getParameters().put('Id',fmCaseObj.id);
        
        FMTenantRegistrationController obj=new FMTenantRegistrationController();
        obj.intNumOfVehicles = 5;

        //obj.objFMCase.id=fmCaseObj.id;
        //obj.initializeFMCaseAddDetails();
        
    }

    static testMethod void testMethodExistingCases() {
        
        account acc = new account();
        acc.Name = 'Test Account1';
        acc.party_ID__C = '103903';
        insert acc;

        Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr1 = new NSIBPM__Service_Request__c();
        sr1.NSIBPM__Customer__c = acc.id;
        sr1.RecordTypeId = RecType;
        insert sr1;

        booking__c bk1 = new booking__c();
        bk1.Account__c = acc.id;
        bk1.Deal_SR__c = sr1.id;
        insert bk1;

        booking_unit__c bu1 = new booking_unit__c();
        bu1.Booking__c = bk1.id;
        bu1.Unit_Name__c = 'LSB/10/B1001';
        bu1.Owner__c = acc.id;
        bu1.Property_City__c = 'Dubai';
        insert bu1;

        Id RecordTypeIdFMCase = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Registration').getRecordTypeId();
        
        fm_case__c fmObj1 = new fm_case__c();
        fmObj1.Booking_Unit__c = bu1.id;
        fmObj1.Expected_move_out_date__c = Date.Today();
        fmObj1.Move_in_date__c = Date.Today();
        fmObj1.Tenant__c = acc.id;
        fmObj1.Account__c = acc.id;
        fmObj1.Access_card_required__c = 'Yes';
        fmObj1.Actual_move_in_date__c = system.today();
        fmObj1.recordtypeid = RecordTypeIdFMCase;
        fmObj1.status__c='New';
        fmObj1.Request_Type__c = 'Tenant Registration';
        insert fmObj1;

        fm_case__c fmObj = new fm_case__c();
        fmObj.Booking_Unit__c = bu1.id;
        fmObj.Expected_move_out_date__c = Date.Today();
        fmObj.Move_in_date__c = Date.Today();
        fmObj.Tenant__c = acc.id;
        fmObj.Account__c = acc.id;
        fmObj.Access_card_required__c = 'Yes';
        fmObj.Actual_move_in_date__c = system.today();
        fmObj.recordtypeid = RecordTypeIdFMCase;
        fmObj.Request_Type__c = 'Tenant Registration';
        insert fmObj;
        PageReference myVfPage = Page.TenantRegistrationProcessPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',acc.id);
        ApexPages.currentPage().getParameters().put('SRType','Tenant_Registration');
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));
        ApexPages.currentPage().getParameters().put('UnitId',String.valueOf(bu1.id));
        
        //ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        //ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        //ApexPages.currentPage().getParameters().put('SRType','Tenant_Registration');
        
        FMTenantRegistrationController obj=new FMTenantRegistrationController();
        
    }

    static testMethod void testMethodExistingCases1() {
        
        account acc = new account();
        acc.Name = 'Test Account1';
        acc.party_ID__C = '103903';
        insert acc;

        Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr1 = new NSIBPM__Service_Request__c();
        sr1.NSIBPM__Customer__c = acc.id;
        sr1.RecordTypeId = RecType;
        insert sr1;

        booking__c bk1 = new booking__c();
        bk1.Account__c = acc.id;
        bk1.Deal_SR__c = sr1.id;
        insert bk1;

        booking_unit__c bu1 = new booking_unit__c();
        bu1.Booking__c = bk1.id;
        bu1.Unit_Name__c = 'LSB/10/B1001';
        bu1.Owner__c = acc.id;
        bu1.Property_City__c = 'Dubai';
        insert bu1;

        Id RecordTypeIdFMCase = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Registration').getRecordTypeId();
        
        fm_case__c fmObj1 = new fm_case__c();
        fmObj1.Booking_Unit__c = bu1.id;
        fmObj1.Expected_move_out_date__c = Date.Today();
        fmObj1.Move_in_date__c = Date.Today();
        fmObj1.Tenant__c = acc.id;
        fmObj1.Account__c = acc.id;
        fmObj1.Access_card_required__c = 'Yes';
        fmObj1.Actual_move_in_date__c = system.today();
        fmObj1.recordtypeid = RecordTypeIdFMCase;
        fmObj1.status__c='New';
        fmObj1.Request_Type__c = 'Tenant Registration';
        insert fmObj1;

        fm_case__c fmObj = new fm_case__c();
        fmObj.Booking_Unit__c = bu1.id;
        fmObj.Expected_move_out_date__c = Date.Today();
        fmObj.Move_in_date__c = Date.Today();
        fmObj.Tenant__c = acc.id;
        fmObj.Account__c = acc.id;
        fmObj.Access_card_required__c = 'Yes';
        fmObj.Actual_move_in_date__c = system.today();
        fmObj.recordtypeid = RecordTypeIdFMCase;
        fmObj.Request_Type__c = 'Tenant Registration';
        insert fmObj;
        PageReference myVfPage = Page.TenantRegistrationProcessPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',acc.id);
        ApexPages.currentPage().getParameters().put('SRType','Tenant_Registration');
        //ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));
        ApexPages.currentPage().getParameters().put('UnitId',String.valueOf(bu1.id));
    }
    
    public static testmethod void testUploadDocument(){
        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;
        
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
        
        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;
        
        PageReference myVfPage = Page.TenantRegistrationProcessPage;
        
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Tenant_Registration';
        
        //insert fmCaseObj;
        UploadMultipleDocController.strLabelValue = 'N';



        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Tenant_Registration');
        
        Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
        
        FMTenantRegistrationController obj=new FMTenantRegistrationController();
        obj.insertCase();
        obj.strDocumentBody='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        obj.strDocumentName='Test_Document_Work_Permit.htm';
        //obj.createCaseShowUploadDoc();
        obj.initializeFMCaseAddDetails();
        test.startTest();
        obj.uploadDocument();
        test.stopTest();
    }  
}