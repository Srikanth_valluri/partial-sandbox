public class SendGridErrorHandler {
    public List<Errors> errors;
    public class Errors {
        public String message;
        public String field;
        public String help;
    }
    public static SendGridErrorHandler parse(String json) {
        return (SendGridErrorHandler) System.JSON.deserialize(json, SendGridErrorHandler.class);
    }
}