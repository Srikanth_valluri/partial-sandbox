public class caseRoundRobinAssignment{//AK:for Mortgage CL assignment
    public static Boolean runOnce = false;
    public static Boolean runMerge = false;
    
    public static void assignTicketsRoundRobin(Set<Id> ticketIdsSet){
        /* get list of all the tickets */
        
        system.debug('ticketIdsSet=' + ticketIdsSet);
        Id mortgageCallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Mortgage Calling List').RecordTypeId;
        set<String> setCLType = new set<String>{'Mortgage- Referred by CRM','Referred by Sales','Mortgage- Referred by Collection'};
        
        List<Calling_List__c> ticketList = [Select Id, OwnerId,Name FROM Calling_List__c 
        Where Id IN:ticketIdsSet AND Calling_List_Type__c In : setCLType AND RecordtypeId =: mortgageCallingRecordTypeId];
        
        Integer index;
        Integer ticketNumber;
        Integer agentSize;
        List<User> agentList = new List<User>();
        Set<Id> queueIdsSet = new Set<Id>();
        System.debug('#### ticketList = '+ticketList);
        // Fetch Ids of the group.
        For(Calling_List__c c : ticketList){
            If( !CheckRecursive.SetOfIDs.contains(c.id) 
            && String.valueOf(c.ownerId).startsWith('00G')){
                queueIdsSet.add(c.ownerId);
                CheckRecursive.SetOfIDs.add(c.id);
            }
        }
        // return if Case is already assigned to user
        If(queueIdsSet==null || queueIdsSet.size()==0)return;
        System.debug('#### queueIdsSet = '+queueIdsSet);
        Set<Id> userIdsSet = new Set<Id>();
        // Fetch Ids of the users
        For(GroupMember gm : [Select Id, UserOrGroupId FROM GROUPMEMBER WHERE GroupId IN : queueIdsSet]){
            userIdsSet.add(gm.UserOrGroupId);
        }
        System.debug('#### userIdsSet = '+userIdsSet);
        /* fetch the total no of users for RRD that are active */
       agentList = [Select Id, Name,  Profile.Name From User Where Id In : userIdsSet AND ISACTIVE = true];
// return if there are no active users 
        If(agentList==null || agentList.size()==0)return;
        System.debug('#### agentList = '+agentList);
        For(Calling_List__c c : ticketList){
            if(c.Name !=null){
                ticketNumber = Integer.valueOf(c.Name.substringAfter('-'));
                System.debug('#### ticketNumber = '+ticketNumber);
                agentSize = agentList.size();
                index = Math.MOD(ticketNumber ,agentSize);//+1;
                System.debug('#### index = '+index);
                c.OwnerId = agentList[index].id;
            }
        }
        If(ticketList!=null && ticketList.size()>0){
            System.debug('#### Updating tickets = '+ticketList);
            
            update ticketList;
        }
    }
}