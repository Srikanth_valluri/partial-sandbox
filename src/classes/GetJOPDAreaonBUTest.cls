/******************************************************************************
* Description - Test class developed for GetJOPDAreaon
*
* Version            Date            Author                    Description
* 1.0                04/02/2018           						Initial Draft
********************************************************************************/
@isTest
private class GetJOPDAreaonBUTest {
	static CallEarlyHandoverToGetHandoverDetails.EarlyHandoverMQService1Response obj1 = new CallEarlyHandoverToGetHandoverDetails.EarlyHandoverMQService1Response();
   			
   static testMethod void testPositiveBatch(){
  		/*Insert data for Location*/
  		Location__c loc = new Location__c();
  		loc.Location_ID__c = '83884';
  		loc.Location_Type__c = 'Building';
  		insert loc;
  		
  		/*insert data for inventory*/
  		Inventory__c Inv = new Inventory__c();
  		Inv.Building_Name__c = 'JNU';
  		Inv.Building_Location__c = loc.Id;
  		Insert Inv;
  		
  		Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;  
        System.assert(Acc != null);
        
         NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
  		/*insert data for Booking units*/
  		Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;
        //BU.JOPD_Area__c = 'demo JOPD area';
        BU.Booking__c = Booking.Id;  
        BU.Inventory__c = Inv.Id;
        //JOPD_Area__c
      
         insert BU;
        Test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String,EarlyHandoverMQService1.GetHandoverDetailsResponse_element>();
	        EarlyHandoverMQService1.GetHandoverDetailsResponse_element response1 = new EarlyHandoverMQService1.GetHandoverDetailsResponse_element();
	        response1.return_x ='{"data":[{"ATTRIBUTE3":null,"ATTRIBUTE10":"Y","ATTRIBUTE2":"0","ATTRIBUTE1":"76726",'+
	        		'"ATTRIBUTE14":null,"ATTRIBUTE13":"N","ATTRIBUTE12":null,"ATTRIBUTE11":"Y","ATTRIBUTE9":"0",'+
	        		'"ATTRIBUTE8":"Agreement executed by DAMAC","ATTRIBUTE7":"N","ATTRIBUTE6":null,"ATTRIBUTE5":null,'+
	        		'"ATTRIBUTE4":"780.49","ATTRIBUTE16":null,"ATTRIBUTE15":null}],"message":"Successfully Fetched Data",'+
	        		'"status":"S"}'; 
	        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
	        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
	        obj1 = CallEarlyHandoverToGetHandoverDetails.CallEarlyHandoverMQService1GetHandoverDetails('Registrationid');
	     // System.assert(resp != null);  
			GetJOPDAreaonBU obj = new GetJOPDAreaonBU();
            DataBase.executeBatch(obj); 
            
            
        Test.stopTest();
  }
     static testMethod void testNegativeOfBatch(){
  		/*Insert data for Location*/
  		Location__c loc = new Location__c();
  		loc.Location_ID__c = '83884';
  		loc.Location_Type__c = 'Building';
  		insert loc;
  		
  		/*insert data for inventory*/
  		Inventory__c Inv = new Inventory__c();
  		Inv.Building_Name__c = 'JNU';
  		Inv.Building_Location__c = loc.Id;
  		Insert Inv;
  		
  		Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;  
        System.assert(Acc != null);
        
         NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
  		/*insert data for Booking units*/
  		Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;
        //BU.JOPD_Area__c = 'demo JOPD area';
        BU.Booking__c = Booking.Id;  
        BU.Inventory__c = Inv.Id;
        //JOPD_Area__c
      
         insert BU;
        Test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String,EarlyHandoverMQService1.GetHandoverDetailsResponse_element>();
	        EarlyHandoverMQService1.GetHandoverDetailsResponse_element response1 = new EarlyHandoverMQService1.GetHandoverDetailsResponse_element();
	        response1.return_x ='{"data":[]}'; 
	        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
	        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
	        obj1 = CallEarlyHandoverToGetHandoverDetails.CallEarlyHandoverMQService1GetHandoverDetails('Registrationid');
	     // System.assert(resp != null);  
			GetJOPDAreaonBU obj = new GetJOPDAreaonBU();
            DataBase.executeBatch(obj); 
            
            
        Test.stopTest();
  }
   static testMethod void testNegativeBatch2(){
  		/*Insert data for Location*/
  		Location__c loc = new Location__c();
  		loc.Location_ID__c = '83884';
  		loc.Location_Type__c = 'Building';
  		insert loc;
  		
  		/*insert data for inventory*/
  		Inventory__c Inv = new Inventory__c();
  		Inv.Building_Name__c = 'JNU';
  		Inv.Building_Location__c = loc.Id;
  		Insert Inv;
  		
  		Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;  
        System.assert(Acc != null);
        
         NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
  		/*insert data for Booking units*/
  		Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;
        //BU.JOPD_Area__c = 'demo JOPD area';
        BU.Booking__c = Booking.Id;  
        BU.Inventory__c = Inv.Id;
        //JOPD_Area__c
      
         insert BU;
        Test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String,EarlyHandoverMQService1.GetHandoverDetailsResponse_element>();
	        EarlyHandoverMQService1.GetHandoverDetailsResponse_element response1 = new EarlyHandoverMQService1.GetHandoverDetailsResponse_element();
	        response1.return_x ='{"data":[{"ATTRIBUTE3":null,"ATTRIBUTE10":"Y","ATTRIBUTE2":"0","ATTRIBUTE1":"76726",'+
	        		'"ATTRIBUTE14":null,"ATTRIBUTE13":"N","ATTRIBUTE12":null,"ATTRIBUTE11":"Y","ATTRIBUTE9":"0",'+
	        		'"ATTRIBUTE8":"Agreement executed by DAMAC","ATTRIBUTE7":"N","ATTRIBUTE6":null,"ATTRIBUTE5":null,'+
	        		'"ATTRIBUTE16":null,"ATTRIBUTE15":null}],"message":"Successfully Fetched Data",'+
	        		'"status":"S"}'; 
	        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
	        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
	        obj1 = CallEarlyHandoverToGetHandoverDetails.CallEarlyHandoverMQService1GetHandoverDetails('Registrationid');
	     // System.assert(resp != null);  
			GetJOPDAreaonBU obj = new GetJOPDAreaonBU();
            DataBase.executeBatch(obj); 
            
            
        Test.stopTest();
  }
}