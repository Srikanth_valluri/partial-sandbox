/***********************************************************************************************
 * @Name              : GenerateHCWithESignExtension
 * @Test Class Name   : GenerateHCWithESignExtensionTest
 * @Description       : Controller Class to for GenerateHCEsign.
 * Modification Log
 * VERSION     AUTHOR            DATE            Update Log
 * 1.0         Neha Dave         25/08/2020      Created
***********************************************************************************************/
public with sharing class GenerateHCWithESignExtension {

	public GenerateHCWithESignExtension(ApexPages.standardController controller) {

	}

	/************************************************************************
	* @Description : Method to redirect to GenerateHC visualforce page.
	* @Params      : None
	* @Return      : None
	*************************************************************************/
	public pagereference callGenerateHC(){
		PageReference pageRef = new PageReference('/apex/GenerateHC');
		pageRef.setRedirect(true);
		pageRef.getParameters().put('isEsign','true');
		pageRef.getParameters().put('id',ApexPages.currentPage().getParameters().get('id'));
		return pageRef;
	}
}