@isTest
private without sharing class FmcDocumentsControllerTest {

    @isTest
    static void testNoticeDocuments() {
        User portalUser = CommunityTestDataFactory.createPortalUser();
        Id portalAccountId = [SELECT Id, Contact.AccountId FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Location__c location = new Location__c(
            Name = 'Test Location',
            Location_ID__c = 'LOC',
            Property_Name__c = property.Id,
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0
        );
        insert location;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(portalAccountId, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Unit_name__c = 'Unit Name';
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            unit.Inventory__c = inventory.Id;
            unit.Tenant__c = portalAccountId;
        }
        insert lstBookingUnit;

        insert new Board_Member__c(
            Property__c = property.Id,
            Board_Member__c = portalAccountId
        );

        insert TestDataFactory_CRM.createActiveFT_CS();

        insert new List<SR_Attachments__c> {
            new SR_Attachments__c(
                FM_Recipient__c = 'Everyone',
                Approval_Status__c = 'Approved',
                Document_Type__c = 'Form'
            ),
            new SR_Attachments__c(
                FM_Recipient__c = 'Everyone',
                Approval_Status__c = 'Approved',
                Document_Type__c = 'Notice'
            ),
            new SR_Attachments__c(
                FM_Recipient__c = 'Owners',
                Approval_Status__c = 'Approved',
                Document_Type__c = 'Notice'
            ),
            new SR_Attachments__c(
                FM_Recipient__c = 'Tenants',
                Approval_Status__c = 'Approved',
                Document_Type__c = 'Notice'
            ),
            new SR_Attachments__c(
                FM_Recipient__c = 'Owners and Tenants',
                Approval_Status__c = 'Approved',
                Document_Type__c = 'Notice'
            ),
            new SR_Attachments__c(
                FM_Recipient__c = 'Board Members',
                Approval_Status__c = 'Approved',
                Document_Type__c = 'Notice'
            ),
            new SR_Attachments__c(
                FM_Recipient__c = 'Everyone',
                Approval_Status__c = 'Approved',
                Building__c = location.Id,
                Document_Type__c = 'Notice'
            ),
            new SR_Attachments__c(
                FM_Recipient__c = 'Owners',
                Approval_Status__c = 'Approved',
                Building__c = location.Id,
                Document_Type__c = 'Notice'
            ),
            new SR_Attachments__c(
                FM_Recipient__c = 'Tenants',
                Approval_Status__c = 'Approved',
                Building__c = location.Id,
                Document_Type__c = 'Notice'
            ),
            new SR_Attachments__c(
                FM_Recipient__c = 'Owners and Tenants',
                Approval_Status__c = 'Approved',
                Building__c = location.Id,
                Document_Type__c = 'Notice'
            ),
            new SR_Attachments__c(
                FM_Recipient__c = 'Board Members',
                Approval_Status__c = 'Approved',
                Property__c = property.Id,
                Document_Type__c = 'Notice'
            )
        };

        CustomerCommunityUtils.customerAccountId = portalAccountId;

        Test.startTest();
            Map<String, List<SR_Attachments__c>> noticeDocuments = FmcDocumentsController.fetchDocumentsOfType(
                'Notice'
            );
            noticeDocuments = FmcDocumentsController.fetchDocumentsOfType('Notice', 100);
            Map<String, List<SR_Attachments__c>> formDocuments = FmcDocumentsController.fetchDocumentsOfType('Form');
            noticeDocuments = FmcDocumentsController.fetchDocumentsOfType('Notice', 1);
            noticeDocuments = FmcDocumentsController.fetchDocumentsOfType('Notice', NULL);
        Test.stopTest();

    }

}