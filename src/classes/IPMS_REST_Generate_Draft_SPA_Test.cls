/************************************************************************************************
 * @Name              : IPMS_REST_Generate_Draft_SPA_Test
 * @Description       : Test Class for Generate Draft SPA Webservice related classes
 * Modification Log
 * 1.0    QBurst    29/01/2020        Created Class
 ************************************************************************************************/


@isTest
public class IPMS_REST_Generate_Draft_SPA_Test {
     
    @isTest static void test_method_1() {
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = acc.Id;
        //sr.RecordTypeId = DealRT;
        sr.Agency__c = acc.id;
        sr.Agency_Type__c = 'Corporate';
        insert sr;
        
        New_Step__c nstp1 = new New_Step__c();
        nstp1.Service_Request__c = sr.id;
        nstp1.Step_No__c = 2;
        nstp1.Step_Status__c = 'Awaiting Token Deposit';
        nstp1.Step_Type__c = 'Token Payment';
        insert nstp1;
        
        Booking__c booking = new Booking__c(Account__c = acc.Id, Deal_SR__c = sr.Id);
        insert booking;
        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id, Unit_Name__c = 'Test name 1',
                                                          Registration_ID__c = '1600', 
                                                          Registration_Status__c = 'Active Status',
                                                          Unit_Selling_Price_AED__c = 100);
        insert bookingUnit;
        RetryWebservice.retryDraftSPA(sr.Id);

        Async_IPMS_Rest_Draft_SPA testObj = new Async_IPMS_Rest_Draft_SPA('testurl', bookingUnit);
        testObj.createLog(booking.Id, 'error Message');
    }
}