@isTest
public class Damac_ClickToDialTest
{
    
    static testmethod void getMobileEncrypted ()
    {
        String mobileNumber = '8977365305';
        String mobilePhoneEncrypt = UtilityHelperCls.encryptMobile(mobileNumber);
        
        Damac_ClickToDial.getMobileEncrypted (mobilePhoneEncrypt);
        Inquiry__c inq = New Inquiry__c ();
        inq.First_Name__c = 'testInq';
        inq.Preferred_Language__c = 'English';
        inq.Last_Name__c ='inqLast';
        inq.Inquiry_Source__c = 'Prospecting';
        inq.Primary_Contacts__c = 'Mobile Phone';
        inq.Mobile_CountryCode__c = 'India: 0091';
        inq.Mobile_Phone__c = '1236547890';
        inq.Mobile_Phone_Encrypt__c = '/FOveENPj+lTu18d/dlTN8hURBnIEM6mUu02ilNZcZg='; //'1223467886';
        inq.Email__c = 'test@gmail.com';
        inq.Call_id__c = '123';
        insert inq;
        apexpages.currentpage().getparameters().put ('id', inq.id);
        Damac_ClickToDial obj = new Damac_ClickToDial();
        obj.encryptMobilePhone ();
        Damac_ClickToDial.encryptMobilePhoneAura(mobilePhoneEncrypt);
        
    }
}