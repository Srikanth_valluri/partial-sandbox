@isTest
public class VirtualHandoverDLD_MS_Graph_API_Test {
    @isTest static void testDoGet() {
        API_Integration_Value__c objApiInt = new API_Integration_Value__c();
        objApiInt.name = 'Virtual Handover Graph API';
        objApiInt.Endpoint_URL__c = 'https://login.microsoftonline.com/';
        objApiInt.Tenant_Id__c = '57e43429-428a-4973-9720-e6907ae94def';
        objApiInt.Client_Id__c = 'b8f6c4e9-d493-4a2f-96f3-7d4ea68c1540';
        objApiInt.Client_Secret__c = 'f18Ocz6Phrng4OyW_iC_.R0z.e85a38qy4';
        objApiInt.Scope__c = 'Offline_access%20Calendars.Read%20Calendars.ReadWrite%20OnlineMeetings.Read%20OnlineMeetings.ReadWrite%20user.Read';
        objApiInt.Refresh_Token__c = 'OAQABAAAAAAAGV_bv21oQQ4ROqh0_1-tA4P4IswFvPEVC1Y6u-NplxxTXklGFdsBBIGMYyccIgg-66bzebSXv-4NR1YEglPxSeUQPojaZyFC8kQDkUdOlXVm9oz9oRxWt1YpJFRqpKnOF1zzK9ZZkaVdn2YpOXgtuNePZZdIOhGD-aHk60havFh8RyTVjIAOU0xMjzRqOrZiz0PQOL0DQmX4PMeyNOe_S6iOlI29kk-OaUvBPwPld1Ux9M_Yva-bzNw7Fs8gGRRE_-babB8oILd8VqWPfF_tQ0CQwoIclK2xOj9OGQpEGfN514fLWw3J9PLG1gL9wb0mnLkky3oVj6powQ-WpwDYLL_6AkM0Ovs4omhjdhzeLYmR_Iblw0uhoV8-Ekpjep10wRQElKoYaMwKrhQ6bntEIXqFJ1IgedeBC3soXuCLFaKqCgP_nHb5NZo5ePOiO16kjSAsV2dnBKkjmHDC52mtm1qlFYP-nVFT2JkTHyNCeLMqNbytTJmtt51vTzzgU3eq2VPx1CPUdjw9vxQsbtbt5c49OkaGw-jszWvlTufife1Gmoy1vdkaN-Ol64dyCscfTF2zhbP4p2BEa2pIutsA8h4rV5ov79y-tj2xpPCacsAB2oz-la_QHa_n5aWP3dIuyXhVQ1_uitj8TWNjR2Nx1Vk2-Lz6AoJneGTHWkYQU1zvLh0cOuiDn1imO3pIiEPmtedqNqYu_bI6xsUnaMov5buNbmMjp5AttQuf9q_gRowJwAhPkWxZuLhdIXDA-bx5QaLFVXYRAH-8ym-giz9-vbYm7rmWm-A45oGY-STHpFPzT-8jiE31GspES6mCbQZ_926_iLm1M7gcK_MdsAkMpq5Y1LnsXO-cz0BGltcMnmXDYcrw5JvWlNF-khfSc_gvnl_0SbLZs-JsJii_10OIl0hLkQsaURecr0YZHDwREHsNcId3kqKW1uTA6PKaoyGStIuUBuXwhGREWpTIeK-TpUpUKqnWDF_i_pzJ2wH5AdGvddGgEy0ghyeaMAo8kCWkM4MEy8C-6v6dXiHMvEVdV0RsQkCbzvnrlB_9MV8MSGUxrNKGmm6ot2_7EMRnG4Ff8Hzn95QJgjLtXIaLMVOOsyeTbdoo0G9h_lcfoA0QPwSAA';
        objApiInt.Meeting_EndPoint_URL__c = 'https://graph.microsoft.com/v1.0/me/onlineMeetings';
        objApiInt.Event_Creation_URL__c = 'https://graph.microsoft.com/v1.0/me/calendar/events';
        objApiInt.Redirect_URI__c = 'https://crmuatpro-damacproperties.cs89.force.com/Documents/services/apexrest/DLDTrusteeMeeting';
        objApiInt.Username__c = 'DLDTrustee.Meeting@damacproperties.com';
        insert objApiInt;
        
        RestRequest request = new RestRequest();
        request.requestUri =
            'https://crmuatpro-damacproperties.cs89.force.com/Documents/services/apexrest/DLDTrusteeMeeting';
        request.httpMethod = 'GET';
        request.addParameter('code','OAQABAAIAAAAGV_bv21oQQ4ROqh0_1-tAbE5OIntPNv6b2VALG60_FB4JxIjexPx4kfuNFP_lHFdNkNb_HsUbn-EElGJmES9oLtwaTma0uN4JsDqFN1tPq8crMPGJfOuSrffpJnbmNJ42ovdguv5YWBBNjghfpabCZaU9HwW1OT80kF9uzt_jr7-sUGqcElAheX8R3sgYmJjATS16fT6t66n7kD4jZ6VHNZ_IxI_3VkCkqbnrRCR3DLAUfispNPjcQ0TEHblZqp-eBoexuuNqAMALEt05vLFqfcR4d1pfgd0fk789it2jwZfHRClvOEXIEkxC-IfIEqeU2-R3CEl6GYek-KXyo_1aRmZPfGbHnaaQr_TzwVibD9JtnMdzRUHAuI2WbwD38ZCfb_VWPgWaf55n4IUxlA2Hfdfsobt7hGbj7D_bicdfU8QN1M3AGOa_LHX_aV4XdPq_-rMiXBLPwVa7Djlkg263rt-1DhtD_VOhfMI6DkBjnhCkwmnPijfjrgRAmRCfrKFBAOxF0QcViSrCdm9qKXzkfBZD474XmDjD8taa7fxNGeq1f5Oukf-PK_UYbUweN-sgzUdc_9112b8BbaZlieiUsJdQMPfHQWD2gNAQZflLX2OouZJO7Syyz5MHWXTDAlLbrT9pkafXyJ1jND_pxncRF8J-n8Wuo5sHlVXOJziaSnB7LV4wiNcBKSmZJl8KutJhb0i3qAb5EH6CtRs2BNZq7chuKvupfdbBoHstWenCNHO9TjMspAsj0lbyC-QkNr8gA');
        RestContext.request = request;
        System.Test.startTest();
        	VirtualHandoverDLD_MS_GRAPH_API.doGet();
        System.Test.stopTest();
        
        List<API_Integration_Value__c> lstUpdatedApi = [Select Refresh_Token__c 
                                                          From API_Integration_Value__c 
                                                         Where Id=:objApiInt.Id];
        System.assertNotEquals(objApiInt.Refresh_Token__c, lstUpdatedApi[0].Refresh_Token__c);
    }
}