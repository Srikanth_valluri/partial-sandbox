/*
 * Description : Test class for GenerateTDDocumentsExtension class
 */
@isTest
private class GenerateTDDocumentsExtensionTest {

    /*
     * Description : Test method to check whether the SR Attachments are created for Title Deed case
     */
    @isTest static void testTDDocCreation() {

        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        
        Id hoRecordTypeID = getRecordTypeIdForTD('Handover');
        Id titleDeedRecordTypeID = getRecordTypeIdForTD('Title Deed');

        // Creation of Handover Case
        Case objHOCase = TestDataFactory_CRM.createCase(objAccount.Id , hoRecordTypeID);
        insert objHOCase;

        //Creation of SR Attachment
        SR_Attachments__c objSRAttachment= TestDataFactory_CRM.createCaseDocument(objHOCase.Id,'Test Document');
        insert objSRAttachment;
    
        // Creation of Title Deed Case
        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , titleDeedRecordTypeID);
        objCase.ParentId = objHOCase.Id;
        insert objCase;

        PageReference redirectionUrl;    
        Test.startTest();
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(objCase);
            GenerateTDDocumentsExtension objController = new GenerateTDDocumentsExtension(standardControllerInstance);
            Test.setCurrentPageReference(new PageReference('Page.GenerateTDDocumentsPage'));
            System.currentPageReference().getParameters().put('Id', objCase.Id);
            redirectionUrl = objController.generateSRAttachments();
        Test.stopTest();

        // Check whether the SR Attachment is created or not for Title Deed case
        List<SR_Attachments__c> lstSRAttachments = [SELECT Id FROM SR_Attachments__c WHERE Case__c =: objCase.Id];
        System.assert(lstSRAttachments.size() > 0);
        System.assertEquals(1, lstSRAttachments.size());

        // Check whether redirected to case detail page
        System.assert(redirectionUrl != null);
        System.assertEquals(redirectionUrl.getURL(), '/'+objCase.Id);
    }

    /*
     * Description : Test method to check page message when parent HO case is not present.
     */
    @isTest static void testEmptyParentCaseForTD() {

        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        Id titleDeedRecordTypeID = getRecordTypeIdForTD('Title Deed');

        // Creation of Case
        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , titleDeedRecordTypeID);
        insert objCase;

        Test.startTest();
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(objCase);
            GenerateTDDocumentsExtension objController = new GenerateTDDocumentsExtension(standardControllerInstance);
            Test.setCurrentPageReference(new PageReference('Page.GenerateTDDocumentsPage'));
            System.currentPageReference().getParameters().put('Id', objCase.Id);
            objController.generateSRAttachments();
        Test.stopTest();

        // Check whether the page message is generated or not
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        System.assertEquals(1, pageMessages.size());
    }

    /**
     * Method to get the "Title Deed" record type
     */
    private static Id getRecordTypeIdForTD(String recordTypeLabel) {
      Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
      Id parkingRecordTypeID = caseRecordTypes.get(recordTypeLabel).getRecordTypeId();
      return parkingRecordTypeID;
    }
}