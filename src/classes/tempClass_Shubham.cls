@RestResource(urlMapping='/testAPI_Shubham/*')
global class tempClass_Shubham {

    // //Move In Constants class
    // public static final String HDApp_Constants.commonErrorMsg = 'This may be because of technical error that we\'re working to get fixed. Please try again later.';
    // public static final String HDApp_Constants.NO_JSON = 'No JSON body found in request';
    
    // //Constants used in Work Permit Create SR API 
    // public static final String HDApp_Constants.NO_ACTION = 'No action parameter value passed in request';
    // public static final String HDApp_Constants.INVALID_ACTION = 'Invalid action parameter passed. Expected values are : submit/draft/notify_contractor';
    // public static final String HDApp_Constants.NO_ACCOUNT = 'No account_id found in request';
    // public static final String HDApp_Constants.NO_BOOKING_UNIT = 'No booking_unit_id found in request';
    // public static final String HDApp_Constants.NO_ACCOUNT_REC_FOUND = 'No account record was found';
    // public static final String HDApp_Constants.NO_BU_REC_FOUND = 'No booking unit record was found';
    // public static final String HDApp_Constants.INVALID_PERSON_TO_COLLECT = 'Person To Collect Should be Contractor or Consultant';
    // public static final String HDApp_Constants.CONTRACTOR_NOTIFIED = 'An email with instructions has been sent to the given email id. Please note the SR no. and inform your contractor';
    // public static final String HDApp_Constants.NO_OTP_ENTERED = 'Please enter OTP and submit';
    // public static final String HDApp_Constants.INVALID_OTP = 'Inavlid OTP entered. Please enter correct OTP and submit';

    @HttpPost
    global static CreateSRFinalReturnWrapper createWorkPermitSR() {

        CreateSRFinalReturnWrapper returnResponse = new CreateSRFinalReturnWrapper();
        HDApp_Utility.cls_meta_data objMeta = new HDApp_Utility.cls_meta_data();
        cls_data_create_sr objData = new cls_data_create_sr();

        RestRequest req = RestContext.request;
        Blob body = req.requestBody;
        String jsonString = body.toString();
        System.debug('jsonString'+jsonString);
        System.debug('Request params:' + req.params);

        if(String.isBlank(jsonString)) {
            objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.NO_JSON, HDApp_Constants.NO_JSON, 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        //Deserializeing the Input JSON
        cls_create_sr_input_wrapper objInputWrap = new cls_create_sr_input_wrapper();
        objInputWrap = (cls_create_sr_input_wrapper)JSON.deserialize(jsonString, cls_create_sr_input_wrapper.class);
        System.debug('objInputWrap: ' + objInputWrap);
        //Check action param value
        if(String.isBlank(objInputWrap.action)) {
            objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.commonErrorMsg, HDApp_Constants.NO_ACTION, 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if( !objInputWrap.action.equalsIgnoreCase('submit') && !objInputWrap.action.equalsIgnoreCase('draft') && !objInputWrap.action.equalsIgnoreCase('notify_contractor') ) {
            objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.commonErrorMsg, HDApp_Constants.INVALID_ACTION, 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        if(objInputWrap.action.equalsIgnoreCase('submit') && String.isBlank(objInputWrap.otp_entered) ) {
            objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.NO_OTP_ENTERED, HDApp_Constants.NO_OTP_ENTERED, 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        if( String.isBlank(objInputWrap.account_id) ) {
            objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.commonErrorMsg, HDApp_Constants.NO_ACCOUNT, 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        if( String.isBlank(objInputWrap.booking_unit_id) ) {
            objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.commonErrorMsg, HDApp_Constants.NO_BOOKING_UNIT, 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        //Check & fetch Account & Booking Unit
        List<Account> account = HDApp_Utility.getAccountFromAccountId(objInputWrap.account_id);
        if(account.isEmpty() || account.size() < 1) {
            objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.commonErrorMsg, HDApp_Constants.NO_ACCOUNT_REC_FOUND, 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        } 
        List<Booking_Unit__c> bookingUnit = HDApp_Utility.getUnitsFromBUId(objInputWrap.booking_unit_id);
        if(bookingUnit.isEmpty() || bookingUnit.size() < 1) {
            objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.commonErrorMsg, HDApp_Constants.NO_BU_REC_FOUND, 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        returnResponse = processFMCase(objInputWrap, account[0], bookingUnit[0]);
        System.debug('!! returnResponse : ' + returnResponse);
        //final return
        return returnResponse;
    }

    public static CreateSRFinalReturnWrapper processFMCase( cls_create_sr_input_wrapper objInputWrap, Account account, Booking_Unit__c unit) {

        CreateSRFinalReturnWrapper returnResponse = new CreateSRFinalReturnWrapper();
        HDApp_Utility.cls_meta_data objMeta = new HDApp_Utility.cls_meta_data();
        cls_data_create_sr objData = new cls_data_create_sr();

        try {
            FM_Case__c objFMCase = populateCommonWPData(objInputWrap, account, unit);

            if(objInputWrap.action.equalsIgnoreCase('notify_contractor')) {
                if((new Set<String>{'Contractor','Consultant'}).contains(objFMCase.Person_to_collect__c)){
                    //First saving as Draft;
                    FM_Case__c fmCaseDraft = saveAsDraftWPCase(  objFMCase, objInputWrap, account, unit ); 
                    //Notifying contractor/Consultant
                    FM_Case__c fmCaseInst = notifyContractorWPCase( fmCaseDraft, objInputWrap, account, unit );
                    cls_data_create_sr returnDataWrap = createWPSRReturnWrapper_Success(fmCaseInst);

                    objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.CONTRACTOR_NOTIFIED,'', 7);
                    returnResponse.meta_data = objMeta;
                    returnResponse.data = returnDataWrap;
                    return returnResponse;
                }else{
                    //myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Person To Collect Should be Contractor or Consultant');
                    objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.INVALID_PERSON_TO_COLLECT, HDApp_Constants.INVALID_PERSON_TO_COLLECT, 3);
                    returnResponse.meta_data = objMeta;
                    return returnResponse;
                }
            }
            else if(objInputWrap.action.equalsIgnoreCase('draft')) {
                //calling WP Draftsave method
                FM_Case__c fmCaseDraft = saveAsDraftWPCase(  objFMCase, objInputWrap, account, unit );
                cls_data_create_sr returnDataWrap = createWPSRReturnWrapper_Success(fmCaseDraft);
                objMeta = HDApp_Utility.ReturnMetaResponse( 'Success', 'Success', 1);
                returnResponse.meta_data = objMeta;
                returnResponse.data = returnDataWrap;
                return returnResponse;
            }
            else if(objInputWrap.action.equalsIgnoreCase('submit')) {
                String expectedOTP = '1234';
                Boolean isSMSOTPVerified = verifyWPCustomerSMSOTP(expectedOTP, objInputWrap.otp_entered);
                if(!isSMSOTPVerified) {
                    objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.INVALID_OTP,HDApp_Constants.INVALID_OTP, 6);
                    returnResponse.meta_data = objMeta;
                    return returnResponse;
                }
                //calling SUbmit WP method
                FM_Case__c fmCaseSubmitted = submitWPCase( objFMCase, objInputWrap, account, unit );
                cls_data_create_sr returnDataWrap = createWPSRReturnWrapper_Success(fmCaseSubmitted);
                objMeta = HDApp_Utility.ReturnMetaResponse( 'Successfull','', 1);
                returnResponse.meta_data = objMeta;
                returnResponse.data = returnDataWrap;

                return returnResponse;
            }

            objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.commonErrorMsg, HDApp_Constants.INVALID_ACTION, 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        catch(exception e) {
            objMeta = HDApp_Utility.ReturnMetaResponse( HDApp_Constants.commonErrorMsg, e.getMessage(), 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        
    }

    public static FM_Case__c populateCommonWPData(cls_create_sr_input_wrapper objInputWrap, Account account, Booking_Unit__c unit) {

        FM_Case__c objFMCase = new FM_Case__c();

        objFMCase.Request_Type_DeveloperName__c = 'Apply_for_Work_Permits' ;
        objFMCase.Request_Type__c = 'Work Permit';
        objFMCase.Outstanding_service_charges__c = '0';
        objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName()
                                        .get('Work Permit').getRecordTypeId();
        //objFMCase.Permit_To_Work_For__c = String.join(valuesOfDrawings ,';');
        objFMCase.Origin__c = 'Portal';
        objFMCase.isHelloDamacAppCase__c = true;
        objFMCase.Account__c = account.Id;
        objFMCase.Booking_Unit__c = unit.id;
        //Deposite details
        objFMCase.Security_Deposit_Cheque_Amount__c = unit.Inventory__r.Building_Location__r.Security_Deposit_Cheque_Amount__c != NULL ? unit.Inventory__r.Building_Location__r.Security_Deposit_Cheque_Amount__c : null;
        objFMCase.Garbage_disposal_Amount__c = unit.Inventory__r.Building_Location__r.Garbage_disposal_Amount__c!= NULL ? unit.Inventory__r.Building_Location__r.Garbage_disposal_Amount__c : null;
        objFMCase.Temporary_power_water_connection_Amount__c = unit.Inventory__r.Building_Location__r.Temporary_power_water_connection_Amount__c!= NULL ? unit.Inventory__r.Building_Location__r.Temporary_power_water_connection_Amount__c : null;
        //IPMS callout for DueInvoice
        // FmIpmsRestServices.DueInvoicesResult objResponse = FmIpmsRestServices.getDueInvoices(unit.Registration_Id__c,'',unit.Inventory__r.Property_Code__c );
        // system.debug('== objResult =='+objResponse);
        // if( objResponse != NULL ) {
        //     objFMCase.Outstanding_service_charges__c = String.isNotBlank(
        //                     objResponse.totalDueAmount ) ? objResponse.totalDueAmount : '0';
        //     System.debug('Outstanding_service_charges__c======= : '+ objFMCase.Outstanding_service_charges__c);
        // }
        //Request for NOC can be submitted only on clearance of Service Charges - NEED TO CONFIRM

        //Populating Work Permit specific data
        objFMCase = populateWPCaseDetails( objFMCase, objInputWrap, account, unit);
        
        return objFMCase;
    }

    public static FM_Case__c populateWPCaseDetails(FM_Case__c objFMCase, cls_create_sr_input_wrapper objInputWrap, Account account, Booking_Unit__c unit) {
        if( String.isNotBlank(objInputWrap.sr_id) ) {
            objFMCase.id = objInputWrap.sr_id;
        }
        objFMCase.Work_Permit_Type__c = objInputWrap.type_of_work_permit != null ? objInputWrap.type_of_work_permit : '';
        objFMCase.Description__c =  objInputWrap.purpose_of_request != null ? objInputWrap.purpose_of_request : '';
        objFMCase.Location_of_Work__c = objInputWrap.location != null ? objInputWrap.location : '';
        objFMCase.Person_To_Collect__c = objInputWrap.person_to_collect != null ? objInputWrap.person_to_collect : '';
        objFMCase.Permit_To_Work_For__c = objInputWrap.permit_to_work_for == null ? null : objInputWrap.permit_to_work_for.contains(',') ? String.join(objInputWrap.permit_to_work_for.split(','), ';') : objInputWrap.permit_to_work_for;
        
        if( objInputWrap.contractor_details != null ) {
            objFMCase = populateContractorDetails(objFMCase, objInputWrap, account, unit);
        }

        return objFMCase;
    }   

    public static FM_Case__c populateContractorDetails( FM_Case__c objFMCase, cls_create_sr_input_wrapper objInputWrap,  Account account, Booking_Unit__c unit ) {

        if( objInputWrap.contractor_details != null ) {
            objFMCase.Company_Name__c = objInputWrap.contractor_details.company_name != null ? objInputWrap.contractor_details.company_name : '';
            objFMCase.Contact_person_contractor__c = objInputWrap.contractor_details.contact_person_name != null ? objInputWrap.contractor_details.contact_person_name : '';
            objFMCase.Email_2__c = objInputWrap.contractor_details.contractor_email != null ? objInputWrap.contractor_details.contractor_email : null;
            objFMCase.Mobile_Country_Code_3__c = objInputWrap.contractor_details.country_code != null ? objInputWrap.contractor_details.country_code : null;
            objFMCase.Mobile_no_contractor__c = objInputWrap.contractor_details.contractor_mobile_number != null ? objInputWrap.contractor_details.contractor_mobile_number : null;
            objFMCase.Office_tel_contractor__c = objInputWrap.contractor_details.office_telephone != null ? objInputWrap.contractor_details.office_telephone : '';
            objFMCase.Contractor_Type__c = objInputWrap.contractor_details.contractor_type != null ? objInputWrap.contractor_details.contractor_type : '';
            objFMCase.Number_of_Employees__c = objInputWrap.contractor_details.number_of_employees != null ? String.valueOf(objInputWrap.contractor_details.number_of_employees) : null;
            return objFMCase;
        }
        else {
            return null;
        }
    }

    public static FM_Case__c saveAsDraftWPCase(FM_Case__c objFMCase, cls_create_sr_input_wrapper objInputWrap, Account account, Booking_Unit__c unit ) {
        objFMCase.status__c ='Draft Request';
        if(objInputWrap.action.equalsIgnoreCase('notify_contractor')) {
            return objFMCase;
        }
        else if(objInputWrap.action.equalsIgnoreCase('draft')) {
            FM_Case__c fmCase = upsertFMCase(objFMCase, objInputWrap);
            return fmCase;
        }
        return null;
    }

    public static FM_Case__c notifyContractorWPCase( FM_Case__c objFMCase, cls_create_sr_input_wrapper objInputWrap, Account account, Booking_Unit__c unit ) {
        objFMCase.Is_Contractor_Consultant_Notified__c = true;
        FM_Case__c fmCase = upsertFMCase(objFMCase, objInputWrap);
        return fmCase;
    }

    public static FM_Case__c submitWPCase( FM_Case__c objFMCase, cls_create_sr_input_wrapper objInputWrap, Account account, Booking_Unit__c unit ) {
        FM_Case__c fmCase  = new FM_Case__c();
        objFMCase.Status__c='Submitted';
        objFMCase.Submitted__c=true;
        //Start
        if(objFMCase.Submitted__c==true){
            if( String.isNotBlank( unit.Unit_Name__c ) ) {
                list<FM_User__c> lstUsers = FM_Utility.getApprovingUsers( new set<String> {
                                                        unit.Unit_Name__c.split('/')[0] } );
                System.debug('lstUsers ======'+lstUsers);
                for( FM_User__c objUser : lstUsers ) {
                    if( String.isNotBlank( objUser.FM_Role__c ) ) {
                        if( objUser.FM_Role__c.containsIgnoreCase( 'Manager') ) {
                            objFMCase.FM_Manager_Email__c = objUser.FM_User__r.Email;
                        }
                        else if( objUser.FM_Role__c.containsIgnoreCase( 'Admin')
                                    && objFMCase.Admin__c == NULL ) {
                            objFMCase.Admin__c = objUser.FM_User__c;
                        }
                    }
                }
            }
            System.debug('objFMCase.submitted__c=====1'+objFMCase.Submitted__c);
            String strApprovingUsers='';
            System.debug('objFMCase.Request_Type_DeveloperName__c===='
                                +objFMCase.Request_Type_DeveloperName__c);
            System.debug('objUnit.Property_City__c'+unit.Property_City__c);
            List<FM_Approver__mdt> listApproverUser = FM_Utility.fetchApprovers(
                            objFMCase.Request_Type_DeveloperName__c,unit.Property_City__c);
            System.debug('listApproverUser====='+listApproverUser);
            if(!listApproverUser.isEmpty()) {
                System.debug('----1----');
                for(FM_Approver__mdt fmApproverInstance:listApproverUser) {
                    System.debug('----2----');
                    strApprovingUsers=strApprovingUsers+fmApproverInstance.Role__c+',';
                }
                System.debug('strApprovingUsers==='+strApprovingUsers);
                strApprovingUsers=strApprovingUsers.removeEnd(',');
                objFMCase.Approving_Authorities__c=strApprovingUsers;
                objFMCase.Approval_Status__c='Pending';
                //objFMCase.Submit_for_Approval__c=true;
                //fmCase = upsertFMCase(objFMCase, objInputWrap);
            }
            fmCase = upsertFMCase(objFMCase, objInputWrap);
        }
        //END
        return fmCase;
    }

    public static FM_Case__c upsertFMCase(FM_Case__c objFMCaseToInsert, cls_create_sr_input_wrapper objInputWrap) {
        System.debug('objFMCase before upsert: ' + objFMCaseToInsert);
        upsert objFMCaseToInsert;

        //Populate Employee Details after inserting FM Case
        if( String.isNotBlank(objFMCaseToInsert.id) && objInputWrap.contractor_details.employees_details != null && objInputWrap.contractor_details.employees_details.size() > 0) {
            List<Contractor_Information__c> employeeDetails = upsertEmployeeDetails(objFMCaseToInsert.id, objInputWrap);
        }
        //to fetch the entire FM case which is inserted/updated above;
        FM_Case__c fetchedFMCase = HDApp_Utility.getFMCase(objFMCaseToInsert.id)[0];
        return fetchedFMCase;
    }

    public static List<Contractor_Information__c> upsertEmployeeDetails(String objFMCaseId, cls_create_sr_input_wrapper objInputWrap) {
        List<Contractor_Information__c> lstEmployees = new List<Contractor_Information__c>();
        if(objInputWrap.contractor_details.employees_details != null && objInputWrap.contractor_details.employees_details.size() > 0) {
            for(cls_employees_details objEmpInput : objInputWrap.contractor_details.employees_details) {
                Contractor_Information__c objEmp = new Contractor_Information__c();
                if(String.isNotBlank( objEmpInput.emp_id )) {
                    objEmp.id = objEmpInput.emp_id;
                }
                objEmp.FM_Case__c = objFMCaseId;
                objEmp.Employee_Name__c = objEmpInput.employee_name;
                objEmp.Emirates_ID__c = objEmpInput.emirates_id;
                objEmp.Company_Name__c = objEmpInput.company_name;
                objEmp.Employee_Mobile_Number__c = objEmpInput.employee_mobile_number;
                //No field on object for Employee mobile number;
                lstEmployees.add(objEmp);
            }
            
            if(lstEmployees.size() > 0) {
                upsert lstEmployees;
            }
        }
        System.debug('lstEmployees:' + lstEmployees);
        return lstEmployees;
    }

    public static boolean verifyWPCustomerSMSOTP(String expectedOTP, String otpEntered) {
        System.debug('IN verifyWPCustomerSMSOTP. Expected OTP is :' + expectedOTP + '. Entered OTP is : ' + otpEntered);    
        if(expectedOTP == otpEntered) {
            return true;
        }
        else {
            return true;
        }
    }

    public static cls_data_create_sr createWPSRReturnWrapper_Success(FM_Case__c objFMCase) {
        cls_data_create_sr objSR = new cls_data_create_sr();
        objSR.sr_id = objFMCase.id;
        objSR.sr_number = objFMCase.Name;
        objSR.sr_status = objFMCase.Status__c;
        objSR.unit_name = objFMCase.Booking_Unit__r.Unit_Name__c;
        objSR.unit_id = objFMCase.Booking_Unit__c;
        objSR.type_of_work_permit = objFMCase.Work_Permit_Type__c;
        objSR.permit_to_work_for = objFMCase.Permit_To_Work_For__c == null ? null : objFMCase.Permit_To_Work_For__c.contains(';') ? String.join(objFMCase.Permit_To_Work_For__c.split(';'), ',') : objFMCase.Permit_To_Work_For__c;
        objSR.submission_date = objFMCase.Submission_Date__c != null ? Datetime.newInstance(objFMCase.Submission_Date__c.year(), objFMCase.Submission_Date__c.month(), objFMCase.Submission_Date__c.day()).format('yyyy-MM-dd') : '';
        return objSR;
    }


    /*************************************************************/
    // For Create Work Permit SR API - REQUEST
    /*************************************************************/
    public class cls_create_sr_input_wrapper {
        public String action;   //Draft
        public String sr_id;    //
        public String account_id;   //001asdsddq
        public String booking_unit_id;  //ax4asdsffwf
        public String selected_fitout_sr_id;    //axsdsdd
        public String type_of_work_permit;  //Work Permit - Including Fit out
        public String purpose_of_request;   //
        public String location; //test
        public String person_to_collect;    //Contractor
        public String permit_to_work_for;   //Hot works
        public cls_contractor_details contractor_details;
        public String otp_entered;  //
        //END
    }   
    public class cls_contractor_details {
        public String company_name; //test
        public String contact_person_name;  //test
        public String contractor_email; //test@test.com
        public String country_code; //+971
        public String contractor_mobile_number; //8551121
        public String office_telephone; //2132
        public String contractor_type;  //CLeaning contractor
        public Integer number_of_employees; //2
        public cls_employees_details[] employees_details;
    }
    public class cls_employees_details {
        public String emp_id;
        public String employee_name;    //test
        public String emirates_id;  //784-132132
        public String company_name; //test
        public String employee_country_code;    //+971
        public String employee_mobile_number;   //3215345
    }

    /*************************************************************/
    // For Create Work Permit SR API - RESPONSE
    /*************************************************************/
    public class cls_data_create_sr {
        public String sr_id;    //ax4asdsas
        public String sr_number;    //FMN-125521
        public String sr_status;    //Draft Request
        public String unit_name;    //BD4/20/2010
        public String unit_id;  //a4xsad121s
        public String type_of_work_permit;  //Work Permit - Including fitout
        public String permit_to_work_for;   //Hot Works
        public String submission_date;  //2021-01-05
    }
    /*************************************************************/
    // For Create Work Permit SR API - Main return response
    /*************************************************************/
    global class CreateSRFinalReturnWrapper {
        public cls_data_create_sr data;
        public HDApp_Utility.cls_meta_data meta_data;
    }

}

/* 
INPUT Request :
{
    "action" : "",
    "sr_id" : "",
    "account_id" : "",
    "booking_unit_id" : "",
    "selected_fitout_sr_id" : "",
    "type_of_work_permit" : "",
    "purpose_of_request" : "",
    "location" : "",
    "person_to_collect" : "",
    "permit_to_work_for" : "",
    "otp_entered" : "",
    "contractor_details" : {
        "company_name" : "",
        "contact_person_name" : "",
        "contractor_email" : "",
        "country_code" : "",
        "contractor_mobile_number" : "",
        "office_telephone" : "",
        "contractor_type" : "",
        "number_of_employees" : 1,
        "employees_details" : [
            {
                "emp_id" : "",
                "employee_name" : "",
                "emirates_id" : "",
                "company_name" : "",
                "employee_country_code" : "",
                "employee_mobile_number" : ""
            }
        ]
    }
}
*/