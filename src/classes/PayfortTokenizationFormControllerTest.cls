@isTest
private class PayfortTokenizationFormControllerTest {

	@TestSetup
	private static void testData() {
		//custom setting
		insert new PayfortGatewayCreds__c( Merchant_Identifier__c= '17e98f0b'
										 , Request_Phrase__c= '$2y$10$QzSutSlCY'
										 , ResponsePhrase__c = '$2y$10$V5IdbyFmy'
										 , AccessCode__c='wT7xGi6a4fwntXQyq0Gs'
										 , PurchaseEndpoint__c='https://sbpaymentservices.payfort.com/FortAPI/paymentApi'
										 , CurrencyInPay__c='AED'
										 , SiteBaseURL__c='https://partial-servicecloudtrial-155c0807bf-1580afc5db1.cs80.force.com/Customer'
										 , TokenizationEndpoint__c='https://sbcheckout.PayFort.com/FortAPI/paymentPage'
										 , Language__c='en' );

		NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com',
                                       Payfort_Card_Number__c = '418887*****00002',
                                       Payfort_Token_Name__c = 'abcd1234');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'Test Unit Name',
                                      Registration_ID__c = '3901');
        insert bookingUnit;
	}

	@isTest 
	private static void testPayfortTokenizationForm() {

		Test.startTest();

	        Booking_Unit__c bookingUnit = [SELECT id, Unit_Name__c 
	        							   FROM Booking_Unit__c 
	        							   WHERE Unit_Name__c = 'Test Unit Name'];

	        Account account = [SELECt id
	        						, Party_ID__c
	        						, Email__pc
	        						, Payfort_Card_Number__c
	        						, Payfort_Token_Name__c 
	        				  FROM Account 
	        				  WHERE Party_ID__c = '63062'];

	        //Setting mock for getInstallmentCallout
		     String jsonResponse =    '   {  '  + 
			 '     "response_code": "62000",  '  + 
			 '     "amount": "100100",  '  + 
			 '     "response_message": "Success",  '  + 
			 '     "signature": "5588aa38a8f8db7adb61698bacf8c6d60f386bd94dde48e1e2a23baf539b49d5",  '  + 
			 '     "merchant_identifier": "17e98f0b",  '  + 
			 '     "access_code": "wT7xGi6a4fwntXQyq0Gs",  '  + 
			 '     "query_command": "GET_INSTALLMENTS_PLANS",  '  + 
			 '     "currency": "AED",  '  + 
			 '     "installment_detail": {  '  + 
			 '       "issuer_detail": [  '  + 
			 '         {  '  + 
			 '           "issuer_code": "mxedBP",  '  + 
			 '           "issuer_name_ar": "Bank 1",  '  + 
			 '           "issuer_name_en": "Bank 1 UAE",  '  + 
			 '           "terms_and_condition_ar": "http://www.payfort.com",  '  + 
			 '           "terms_and_condition_en": "http://www.payfort.com",  '  + 
			 '           "country_code": "ARE",  '  + 
			 '           "issuer_logo_ar": "https://sbstatic.payfort.com/frontend/files/logos/issuer/logo_ar_152.png",  '  + 
			 '           "issuer_logo_en": "https://sbstatic.payfort.com/frontend/files/logos/issuer/logo_en_152.png",  '  + 
			 '           "banking_system": "Non Islamic",  '  + 
			 '           "formula": "(amount +(amount *effective rate/100))/period",  '  + 
			 '           "plan_details": [  '  + 
			 '             {  '  + 
			 '               "plan_code": "yAVoM5",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "number_of_installment": 60,  '  + 
			 '               "fees_type": "Percentage",  '  + 
			 '               "fees_amount": 1000,  '  + 
			 '               "processing_fees_type": "Fixed",  '  + 
			 '               "processing_fees_amount": 0,  '  + 
			 '               "rate_type": "Flat",  '  + 
			 '               "plan_merchant_type": "Non Partner",  '  + 
			 '               "plan_type": "Local",  '  + 
			 '               "fee_display_value": 1000,  '  + 
			 '               "minimum_amount": 100,  '  + 
			 '               "maximum_amount": 100000000,  '  + 
			 '               "amountPerMonth": "18.35"  '  + 
			 '             }  '  + 
			 '           ],  '  + 
			 '           "bins": [  '  + 
			 '             {  '  + 
			 '               "bin": "411133",  '  + 
			 '               "country_code": "ARE",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "card_brand_code": "VISA"  '  + 
			 '             },  '  + 
			 '             {  '  + 
			 '               "bin": "555555",  '  + 
			 '               "country_code": "ARE",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "card_brand_code": "Master Card"  '  + 
			 '             }  '  + 
			 '           ],  '  + 
			 '           "confirmation_message_ar": "This is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is random messg",  '  + 
			 '           "disclaimer_message_ar": null,  '  + 
			 '           "processing_fees_message_ar": null,  '  + 
			 '           "confirmation_message_en": "This is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is random messg",  '  + 
			 '           "disclaimer_message_en": null,  '  + 
			 '           "processing_fees_message_en": null  '  + 
			 '         },  '  + 
			 '         {  '  + 
			 '           "issuer_code": "zaQnN1",  '  + 
			 '           "issuer_name_ar": "بنك الإمارات دبي الوطني",  '  + 
			 '           "issuer_name_en": "Emirates NBD UAE",  '  + 
			 '           "terms_and_condition_ar": "https://www.emiratesnbd.com/en/assets/File/CREDIT_CARD_INSTALMENT_PLAN_TC.pdf",  '  + 
			 '           "terms_and_condition_en": "https://www.emiratesnbd.com/en/assets/File/CREDIT_CARD_INSTALMENT_PLAN_TC.pdf",  '  + 
			 '           "country_code": "ARE",  '  + 
			 '           "issuer_logo_ar": "https://sbstatic.payfort.com/frontend/files/logos/issuer/logo_ar_162.png",  '  + 
			 '           "issuer_logo_en": "https://sbstatic.payfort.com/frontend/files/logos/issuer/logo_en_162.png",  '  + 
			 '           "banking_system": "Non Islamic",  '  + 
			 '           "formula": "(amount + (amount * (effective rate/100))) / period",  '  + 
			 '           "plan_details": [  '  + 
			 '             {  '  + 
			 '               "plan_code": "M9mv5Z",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "number_of_installment": 6,  '  + 
			 '               "fees_type": "Percentage",  '  + 
			 '               "fees_amount": 894,  '  + 
			 '               "processing_fees_type": "Fixed",  '  + 
			 '               "processing_fees_amount": 4900,  '  + 
			 '               "rate_type": "Flat",  '  + 
			 '               "plan_merchant_type": "Non Partner",  '  + 
			 '               "plan_type": "Local",  '  + 
			 '               "fee_display_value": 100,  '  + 
			 '               "minimum_amount": 100000,  '  + 
			 '               "maximum_amount": 10000000,  '  + 
			 '               "amountPerMonth": "181.75"  '  + 
			 '             },  '  + 
			 '             {  '  + 
			 '               "plan_code": "PGyD2b",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "number_of_installment": 12,  '  + 
			 '               "fees_type": "Percentage",  '  + 
			 '               "fees_amount": 1788,  '  + 
			 '               "processing_fees_type": "Fixed",  '  + 
			 '               "processing_fees_amount": 4900,  '  + 
			 '               "rate_type": "Flat",  '  + 
			 '               "plan_merchant_type": "Non Partner",  '  + 
			 '               "plan_type": "Local",  '  + 
			 '               "fee_display_value": 100,  '  + 
			 '               "minimum_amount": 100000,  '  + 
			 '               "maximum_amount": 10000000,  '  + 
			 '               "amountPerMonth": "98.33"  '  + 
			 '             },  '  + 
			 '             {  '  + 
			 '               "plan_code": "V1rqnl",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "number_of_installment": 24,  '  + 
			 '               "fees_type": "Percentage",  '  + 
			 '               "fees_amount": 3576,  '  + 
			 '               "processing_fees_type": "Fixed",  '  + 
			 '               "processing_fees_amount": 4900,  '  + 
			 '               "rate_type": "Flat",  '  + 
			 '               "plan_merchant_type": "Non Partner",  '  + 
			 '               "plan_type": "Local",  '  + 
			 '               "fee_display_value": 100,  '  + 
			 '               "minimum_amount": 100000,  '  + 
			 '               "maximum_amount": 10000000,  '  + 
			 '               "amountPerMonth": "56.62"  '  + 
			 '             },  '  + 
			 '             {  '  + 
			 '               "plan_code": "26ge87",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "number_of_installment": 36,  '  + 
			 '               "fees_type": "Percentage",  '  + 
			 '               "fees_amount": 5364,  '  + 
			 '               "processing_fees_type": "Fixed",  '  + 
			 '               "processing_fees_amount": 4900,  '  + 
			 '               "rate_type": "Flat",  '  + 
			 '               "plan_merchant_type": "Non Partner",  '  + 
			 '               "plan_type": "Local",  '  + 
			 '               "fee_display_value": 100,  '  + 
			 '               "minimum_amount": 100000,  '  + 
			 '               "maximum_amount": 10000000,  '  + 
			 '               "amountPerMonth": "42.72"  '  + 
			 '             },  '  + 
			 '             {  '  + 
			 '               "plan_code": "DNsycOSu",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "number_of_installment": 12,  '  + 
			 '               "fees_type": "Fixed",  '  + 
			 '               "fees_amount": 1800,  '  + 
			 '               "processing_fees_type": "Fixed",  '  + 
			 '               "processing_fees_amount": 2000,  '  + 
			 '               "rate_type": "Flat",  '  + 
			 '               "plan_merchant_type": "Non Partner",  '  + 
			 '               "plan_type": "Cross-Border",  '  + 
			 '               "fee_display_value": 200,  '  + 
			 '               "minimum_amount": 100000,  '  + 
			 '               "maximum_amount": 10000000,  '  + 
			 '               "amountPerMonth": "84.92"  '  + 
			 '             },  '  + 
			 '             {  '  + 
			 '               "plan_code": "BN8cEncl",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "number_of_installment": 6,  '  + 
			 '               "fees_type": "Percentage",  '  + 
			 '               "fees_amount": 894,  '  + 
			 '               "processing_fees_type": "Fixed",  '  + 
			 '               "processing_fees_amount": 4900,  '  + 
			 '               "rate_type": "Flat",  '  + 
			 '               "plan_merchant_type": "Non Partner",  '  + 
			 '               "plan_type": "Cross-Border",  '  + 
			 '               "fee_display_value": 100,  '  + 
			 '               "minimum_amount": 100000,  '  + 
			 '               "maximum_amount": 10000000,  '  + 
			 '               "amountPerMonth": "181.75"  '  + 
			 '             },  '  + 
			 '             {  '  + 
			 '               "plan_code": "NCtYPz14",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "number_of_installment": 6,  '  + 
			 '               "fees_type": "Percentage",  '  + 
			 '               "fees_amount": 0,  '  + 
			 '               "processing_fees_type": "Percentage",  '  + 
			 '               "processing_fees_amount": 0,  '  + 
			 '               "rate_type": "Flat",  '  + 
			 '               "plan_merchant_type": "Non Partner",  '  + 
			 '               "plan_type": "Local",  '  + 
			 '               "fee_display_value": 0,  '  + 
			 '               "minimum_amount": 100000,  '  + 
			 '               "maximum_amount": 10000000,  '  + 
			 '               "amountPerMonth": "166.83"  '  + 
			 '             }  '  + 
			 '           ],  '  + 
			 '           "bins": [  '  + 
			 '             {  '  + 
			 '               "bin": "418887",  '  + 
			 '               "country_code": "ARE",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "card_brand_code": "VISA"  '  + 
			 '             },  '  + 
			 '             {  '  + 
			 '               "bin": "467744",  '  + 
			 '               "country_code": "ARE",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "card_brand_code": "Master Card"  '  + 
			 '             },  '  + 
			 '             {  '  + 
			 '               "bin": "512670",  '  + 
			 '               "country_code": "ARE",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "card_brand_code": "Master Card"  '  + 
			 '             },  '  + 
			 '             {  '  + 
			 '               "bin": "552191",  '  + 
			 '               "country_code": "ARE",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "card_brand_code": "Master Card"  '  + 
			 '             },  '  + 
			 '             {  '  + 
			 '               "bin": "400000",  '  + 
			 '               "country_code": "ARE",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "card_brand_code": "VISA"  '  + 
			 '             },  '  + 
			 '             {  '  + 
			 '               "bin": "455643",  '  + 
			 '               "country_code": "ARE",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "card_brand_code": "VISA"  '  + 
			 '             }  '  + 
			 '           ],  '  + 
			 '           "confirmation_message_ar": null,  '  + 
			 '           "disclaimer_message_ar": null,  '  + 
			 '           "processing_fees_message_ar": null,  '  + 
			 '           "confirmation_message_en": "The Bank will convert your transaction into an installment plan within two working days.",  '  + 
			 '           "disclaimer_message_en": null,  '  + 
			 '           "processing_fees_message_en": "The Bank interest rate is calculated per month"  '  + 
			 '         }  '  + 
			 '       ]  '  + 
			 '     },  '  + 
			 '     "status": "62"  '  + 
			 '  }  ';

			FmHttpCalloutMock.Response getInstallmentResponse = new FmHttpCalloutMock.Response(200, 'Success', jsonResponse);

			Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
	            'https://sbpaymentservices.payfort.com/FortAPI/paymentApi' => getInstallmentResponse
	        };

	        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
	        //END of getInstallment callout

	        PageReference pager = page.PayfortTokenizationPage;
	        Test.setCurrentPage(pager);

	        pager.getParameters().put('amount', '10000');
	        pager.getParameters().put('adminfee', '100');
			pager.getParameters().put('amountToPay', '100');
			pager.getParameters().put('unitId', bookingUnit.Id);
			pager.getParameters().put('issuer_code', 'zaQnN1');
			pager.getParameters().put('plan_code', 'NCtYPz14');

			//List<Account> acc = [SELECT id
			//						  , Party_ID__c
			//						  , Payfort_Token_Name__c
			//						  , Payfort_Card_Number__c
			//					FROM Account
			//					WHERE Party_ID__c = '63062'];

			CustomerCommunityUtils.customerAccountId = account.Id;

			PayfortTokenizationFormController objcls = new PayfortTokenizationFormController();
			objcls.getTokenizationSHA_256();
			objcls.GotoDirectPurchase();
			objcls.RedirectPrevPage();
			objcls.lstAccount = new List<Account>{account};
			//objcls.DeleteCardToken();
			objcls.AddPlanIssuerCodeinURL();


			//To cover all wrapper data;
	        PayfortTokenizationFormController.cls_installment_details objInstall = new PayfortTokenizationFormController.cls_installment_details();

	        PayfortTokenizationFormController.cls_issuer_detail cls_issuer_detail = new PayfortTokenizationFormController.cls_issuer_detail();
	        cls_issuer_detail.processing_fees_message_en = 'Test';
	        cls_issuer_detail.confirmation_message_en = 'Test';

	        cls_issuer_detail.banking_system = 'Test';         
	        cls_issuer_detail.issuer_logo_en = 'Test';        
	        cls_issuer_detail.issuer_logo_ar = 'Test';         
	        cls_issuer_detail.country_code = 'Test';          
	        cls_issuer_detail.terms_and_condition_en = 'Test';
	        cls_issuer_detail.terms_and_condition_ar = 'Test'; 
	        cls_issuer_detail.issuer_name_en = 'Test';        
	        cls_issuer_detail.issuer_name_ar = 'Test';         
	        cls_issuer_detail.issuer_code = 'Test';          

	        objInstall.issuer_detail = new List<PayfortTokenizationFormController.cls_issuer_detail>{cls_issuer_detail};


	        PayfortTokenizationFormController.cls_plan_details objPlanDetails = new PayfortTokenizationFormController.cls_plan_details();
	        objPlanDetails.amountPerMonth = 200.00;
	        objPlanDetails.maximum_amount = 1000;
	        objPlanDetails.minimum_amount = 1000;      
	        objPlanDetails.fee_display_value = 1000;      
	        objPlanDetails.plan_type = 'Test';        
	        objPlanDetails.plan_merchant_type = 'Test';
	        objPlanDetails.rate_type = 'Test';
	        objPlanDetails.processing_fees_amount = 1000;      
	        objPlanDetails.processing_fees_type = 'Test'; 
	        objPlanDetails.fees_amount = 1000;
	        objPlanDetails.fees_type = 'Test';           
	        objPlanDetails.plan_code = 'Test';                      

	        cls_issuer_detail.plan_details = new List<PayfortTokenizationFormController.cls_plan_details>{objPlanDetails};

	}

	@isTest 
	private static void testDeleteCardToken() {

		Test.startTest();

	        Booking_Unit__c bookingUnit = [SELECT id, Unit_Name__c 
	        							   FROM Booking_Unit__c 
	        							   WHERE Unit_Name__c = 'Test Unit Name'];

	        Account account = [SELECt id
	        						, Party_ID__c
	        						, Email__pc
	        						, Payfort_Card_Number__c
	        						, Payfort_Token_Name__c 
	        				  FROM Account 
	        				  WHERE Party_ID__c = '63062'];

	  //      String strUpdateTokenJson =  '   {  '  + 
			//'     "token_status": "INACTIVE",  '  + 
			//'     "response_code": "00044",  '  + 
			//'     "response_message": "Token name does not exist",  '  + 
			//'     "service_command": "UPDATE_TOKEN",  '  + 
			//'     "signature": "6adf316213cc5ebd5f5f838de6b6f596b12840c97b3f8217affe57a54b5cf6d3",  '  + 
			//'     "merchant_identifier": "17e98f0b",  '  + 
			//'     "merchant_reference": "RECP-1184",  '  + 
			//'     "access_code": "wT7xGi6a4fwntXQyq0Gs",  '  + 
			//'     "token_name": "abcd1234",  '  + 
			//'     "language": "en",  '  + 
			//'     "status": "00"  '  + 
			//'  }  ' ;  			

			 String strUpdateTokenJson =    '   {  '  + 
			 '     "response_code": "62000",  '  + 
			 '     "amount": "100100",  '  + 
			 '     "response_message": "Success",  '  + 
			 '     "signature": "5588aa38a8f8db7adb61698bacf8c6d60f386bd94dde48e1e2a23baf539b49d5",  '  + 
			 '     "merchant_identifier": "17e98f0b",  '  + 
			 '     "access_code": "wT7xGi6a4fwntXQyq0Gs",  '  + 
			 '     "query_command": "GET_INSTALLMENTS_PLANS",  '  + 
			 '     "currency": "AED",  '  + 
			 '     "installment_detail": {  '  + 
			 '       "issuer_detail": [  '  + 
			 '         {  '  + 
			 '           "issuer_code": "mxedBP",  '  + 
			 '           "issuer_name_ar": "Bank 1",  '  + 
			 '           "issuer_name_en": "Bank 1 UAE",  '  + 
			 '           "terms_and_condition_ar": "http://www.payfort.com",  '  + 
			 '           "terms_and_condition_en": "http://www.payfort.com",  '  + 
			 '           "country_code": "ARE",  '  + 
			 '           "issuer_logo_ar": "https://sbstatic.payfort.com/frontend/files/logos/issuer/logo_ar_152.png",  '  + 
			 '           "issuer_logo_en": "https://sbstatic.payfort.com/frontend/files/logos/issuer/logo_en_152.png",  '  + 
			 '           "banking_system": "Non Islamic",  '  + 
			 '           "formula": "(amount +(amount *effective rate/100))/period",  '  + 
			 '           "plan_details": [  '  + 
			 '             {  '  + 
			 '               "plan_code": "yAVoM5",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "number_of_installment": 60,  '  + 
			 '               "fees_type": "Percentage",  '  + 
			 '               "fees_amount": 1000,  '  + 
			 '               "processing_fees_type": "Fixed",  '  + 
			 '               "processing_fees_amount": 0,  '  + 
			 '               "rate_type": "Flat",  '  + 
			 '               "plan_merchant_type": "Non Partner",  '  + 
			 '               "plan_type": "Local",  '  + 
			 '               "fee_display_value": 1000,  '  + 
			 '               "minimum_amount": 100,  '  + 
			 '               "maximum_amount": 100000000,  '  + 
			 '               "amountPerMonth": "18.35"  '  + 
			 '             }  '  + 
			 '           ],  '  + 
			 '           "bins": [  '  + 
			 '             {  '  + 
			 '               "bin": "411133",  '  + 
			 '               "country_code": "ARE",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "card_brand_code": "VISA"  '  + 
			 '             },  '  + 
			 '             {  '  + 
			 '               "bin": "555555",  '  + 
			 '               "country_code": "ARE",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "card_brand_code": "Master Card"  '  + 
			 '             }  '  + 
			 '           ],  '  + 
			 '           "confirmation_message_ar": "This is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is random messg",  '  + 
			 '           "disclaimer_message_ar": null,  '  + 
			 '           "processing_fees_message_ar": null,  '  + 
			 '           "confirmation_message_en": "This is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is random messg",  '  + 
			 '           "disclaimer_message_en": null,  '  + 
			 '           "processing_fees_message_en": null  '  + 
			 '         },  '  + 
			 '         {  '  + 
			 '           "issuer_code": "zaQnN1",  '  + 
			 '           "issuer_name_ar": "بنك الإمارات دبي الوطني",  '  + 
			 '           "issuer_name_en": "Emirates NBD UAE",  '  + 
			 '           "terms_and_condition_ar": "https://www.emiratesnbd.com/en/assets/File/CREDIT_CARD_INSTALMENT_PLAN_TC.pdf",  '  + 
			 '           "terms_and_condition_en": "https://www.emiratesnbd.com/en/assets/File/CREDIT_CARD_INSTALMENT_PLAN_TC.pdf",  '  + 
			 '           "country_code": "ARE",  '  + 
			 '           "issuer_logo_ar": "https://sbstatic.payfort.com/frontend/files/logos/issuer/logo_ar_162.png",  '  + 
			 '           "issuer_logo_en": "https://sbstatic.payfort.com/frontend/files/logos/issuer/logo_en_162.png",  '  + 
			 '           "banking_system": "Non Islamic",  '  + 
			 '           "formula": "(amount + (amount * (effective rate/100))) / period",  '  + 
			 '           "plan_details": [  '  + 
			 '             {  '  + 
			 '               "plan_code": "M9mv5Z",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "number_of_installment": 6,  '  + 
			 '               "fees_type": "Percentage",  '  + 
			 '               "fees_amount": 894,  '  + 
			 '               "processing_fees_type": "Fixed",  '  + 
			 '               "processing_fees_amount": 4900,  '  + 
			 '               "rate_type": "Flat",  '  + 
			 '               "plan_merchant_type": "Non Partner",  '  + 
			 '               "plan_type": "Local",  '  + 
			 '               "fee_display_value": 100,  '  + 
			 '               "minimum_amount": 100000,  '  + 
			 '               "maximum_amount": 10000000,  '  + 
			 '               "amountPerMonth": "181.75"  '  + 
			 '             },  '  + 
			 '             {  '  + 
			 '               "plan_code": "PGyD2b",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "number_of_installment": 12,  '  + 
			 '               "fees_type": "Percentage",  '  + 
			 '               "fees_amount": 1788,  '  + 
			 '               "processing_fees_type": "Fixed",  '  + 
			 '               "processing_fees_amount": 4900,  '  + 
			 '               "rate_type": "Flat",  '  + 
			 '               "plan_merchant_type": "Non Partner",  '  + 
			 '               "plan_type": "Local",  '  + 
			 '               "fee_display_value": 100,  '  + 
			 '               "minimum_amount": 100000,  '  + 
			 '               "maximum_amount": 10000000,  '  + 
			 '               "amountPerMonth": "98.33"  '  + 
			 '             },  '  + 
			 '             {  '  + 
			 '               "plan_code": "V1rqnl",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "number_of_installment": 24,  '  + 
			 '               "fees_type": "Percentage",  '  + 
			 '               "fees_amount": 3576,  '  + 
			 '               "processing_fees_type": "Fixed",  '  + 
			 '               "processing_fees_amount": 4900,  '  + 
			 '               "rate_type": "Flat",  '  + 
			 '               "plan_merchant_type": "Non Partner",  '  + 
			 '               "plan_type": "Local",  '  + 
			 '               "fee_display_value": 100,  '  + 
			 '               "minimum_amount": 100000,  '  + 
			 '               "maximum_amount": 10000000,  '  + 
			 '               "amountPerMonth": "56.62"  '  + 
			 '             },  '  + 
			 '             {  '  + 
			 '               "plan_code": "26ge87",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "number_of_installment": 36,  '  + 
			 '               "fees_type": "Percentage",  '  + 
			 '               "fees_amount": 5364,  '  + 
			 '               "processing_fees_type": "Fixed",  '  + 
			 '               "processing_fees_amount": 4900,  '  + 
			 '               "rate_type": "Flat",  '  + 
			 '               "plan_merchant_type": "Non Partner",  '  + 
			 '               "plan_type": "Local",  '  + 
			 '               "fee_display_value": 100,  '  + 
			 '               "minimum_amount": 100000,  '  + 
			 '               "maximum_amount": 10000000,  '  + 
			 '               "amountPerMonth": "42.72"  '  + 
			 '             },  '  + 
			 '             {  '  + 
			 '               "plan_code": "DNsycOSu",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "number_of_installment": 12,  '  + 
			 '               "fees_type": "Fixed",  '  + 
			 '               "fees_amount": 1800,  '  + 
			 '               "processing_fees_type": "Fixed",  '  + 
			 '               "processing_fees_amount": 2000,  '  + 
			 '               "rate_type": "Flat",  '  + 
			 '               "plan_merchant_type": "Non Partner",  '  + 
			 '               "plan_type": "Cross-Border",  '  + 
			 '               "fee_display_value": 200,  '  + 
			 '               "minimum_amount": 100000,  '  + 
			 '               "maximum_amount": 10000000,  '  + 
			 '               "amountPerMonth": "84.92"  '  + 
			 '             },  '  + 
			 '             {  '  + 
			 '               "plan_code": "BN8cEncl",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "number_of_installment": 6,  '  + 
			 '               "fees_type": "Percentage",  '  + 
			 '               "fees_amount": 894,  '  + 
			 '               "processing_fees_type": "Fixed",  '  + 
			 '               "processing_fees_amount": 4900,  '  + 
			 '               "rate_type": "Flat",  '  + 
			 '               "plan_merchant_type": "Non Partner",  '  + 
			 '               "plan_type": "Cross-Border",  '  + 
			 '               "fee_display_value": 100,  '  + 
			 '               "minimum_amount": 100000,  '  + 
			 '               "maximum_amount": 10000000,  '  + 
			 '               "amountPerMonth": "181.75"  '  + 
			 '             },  '  + 
			 '             {  '  + 
			 '               "plan_code": "NCtYPz14",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "number_of_installment": 6,  '  + 
			 '               "fees_type": "Percentage",  '  + 
			 '               "fees_amount": 0,  '  + 
			 '               "processing_fees_type": "Percentage",  '  + 
			 '               "processing_fees_amount": 0,  '  + 
			 '               "rate_type": "Flat",  '  + 
			 '               "plan_merchant_type": "Non Partner",  '  + 
			 '               "plan_type": "Local",  '  + 
			 '               "fee_display_value": 0,  '  + 
			 '               "minimum_amount": 100000,  '  + 
			 '               "maximum_amount": 10000000,  '  + 
			 '               "amountPerMonth": "166.83"  '  + 
			 '             }  '  + 
			 '           ],  '  + 
			 '           "bins": [  '  + 
			 '             {  '  + 
			 '               "bin": "418887",  '  + 
			 '               "country_code": "ARE",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "card_brand_code": "VISA"  '  + 
			 '             },  '  + 
			 '             {  '  + 
			 '               "bin": "467744",  '  + 
			 '               "country_code": "ARE",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "card_brand_code": "Master Card"  '  + 
			 '             },  '  + 
			 '             {  '  + 
			 '               "bin": "512670",  '  + 
			 '               "country_code": "ARE",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "card_brand_code": "Master Card"  '  + 
			 '             },  '  + 
			 '             {  '  + 
			 '               "bin": "552191",  '  + 
			 '               "country_code": "ARE",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "card_brand_code": "Master Card"  '  + 
			 '             },  '  + 
			 '             {  '  + 
			 '               "bin": "400000",  '  + 
			 '               "country_code": "ARE",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "card_brand_code": "VISA"  '  + 
			 '             },  '  + 
			 '             {  '  + 
			 '               "bin": "455643",  '  + 
			 '               "country_code": "ARE",  '  + 
			 '               "currency_code": "AED",  '  + 
			 '               "card_brand_code": "VISA"  '  + 
			 '             }  '  + 
			 '           ],  '  + 
			 '           "confirmation_message_ar": null,  '  + 
			 '           "disclaimer_message_ar": null,  '  + 
			 '           "processing_fees_message_ar": null,  '  + 
			 '           "confirmation_message_en": "The Bank will convert your transaction into an installment plan within two working days.",  '  + 
			 '           "disclaimer_message_en": null,  '  + 
			 '           "processing_fees_message_en": "The Bank interest rate is calculated per month"  '  + 
			 '         }  '  + 
			 '       ]  '  + 
			 '     },  '  + 
			 '     "status": "62"  '  + 
			 '  }  ';

			FmHttpCalloutMock.Response updateTokenResponse = new FmHttpCalloutMock.Response(200, 'Success', strUpdateTokenJson);

			Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
	            'https://sbpaymentservices.payfort.com/FortAPI/paymentApi' => updateTokenResponse
	        };

	        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));

	        PageReference pager = page.PayfortTokenizationPage;
	        Test.setCurrentPage(pager);

	        pager.getParameters().put('amount', '10000');
	        pager.getParameters().put('adminfee', '100');
			pager.getParameters().put('amountToPay', '100');
			pager.getParameters().put('unitId', bookingUnit.Id);
			pager.getParameters().put('issuer_code', 'zaQnN1');
			pager.getParameters().put('plan_code', 'NCtYPz14');	  

			//List<Account> acc = [SELECT id
			//						  , Party_ID__c
			//						  , Payfort_Token_Name__c
			//						  , Payfort_Card_Number__c
			//					FROM Account
			//					WHERE Party_ID__c = '63062'];

			CustomerCommunityUtils.customerAccountId = account.Id;

			PayfortTokenizationFormController objcls = new PayfortTokenizationFormController();
			objcls.merchant_reference = 'Test-10';
			objcls.DeleteCardToken();
			Test.stopTest();
    				  
	}	

}