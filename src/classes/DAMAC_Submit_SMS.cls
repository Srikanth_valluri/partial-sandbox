/*
The SMS Controller which Queries the Inquries based on the user Search Criteria 
Developed By: DAMAC IT TEAM
*/

public with sharing class DAMAC_Submit_SMS{
    /*
    public DAMAC_Submit_SMS(ApexPages.StandardController controller) {

    }
    */
    
    //public string Message {get;set;}
    
    public string marketingId;
    
    public integer inqCount {get;set;}    
       
    public campaign_SMS__c newSMSRequest {get;set;}
        
    public list<Campaign_SMS__c> smsHistory {get;set;}
    
    public list<inquiry__c> inquiriesToSendSMS;
    
    public boolean showMessageInput{get;set;}
    
    public string smsBalance {get;set;}
    
    public string CampsmsId {get;set;}
    
    public case c;

    public DAMAC_Submit_SMS(){
        newSMSRequest = new campaign_SMS__c(); 
        inquiriesToSendSMS = new list<inquiry__c>();
        inqCount =0 ;
        showMessageInput = false;        
        this.marketingId = apexpages.currentpage().getparameters().get('id');   
        // Call SMS Hitory method
        smsHistory();
        
    }
    
    // Get the SMS Hitory
    public void smsHistory(){
        smsHistory = new list<Campaign_SMS__c>();
        smsHistory = [select id,
                    SMS_Message__c,
                    createddate,
                    SMS_Response__c,
                    SMS_Queued__c,
                    Total_Mobile_Numbers__c,
                    SMS_Process_Complete__c ,Approval_Status__c,
                    Total_Inquiries_in_the_Date_Range__c,
                    Error_From_Provider__c   
                    from Campaign_SMS__c 
                    where marketing_campaign__c=:marketingId 
                    ORDER BY CREATEDDATE DESC LIMIT 5 ]; // Show 5 History records
    }
    
    public void submitSMS(){
        Savepoint sp = Database.setSavepoint();
        try{
            system.debug(marketingid);system.debug(newSMSRequest.sms_Message__c);system.debug(inquiriesToSendSMS.size());
            if(marketingid != null && newSMSRequest.sms_Message__c != null && inquiriesToSendSMS.size()>0 && newSMSRequest.Scheduled_Date_Time__c != null &&  newSMSRequest.Scheduled_Date_Time__c > system.now()){
                newSMSRequest.marketing_campaign__c = marketingid;   
                newSMSRequest.Total_Inquiries_in_the_Date_Range__c =  inquiriesToSendSMS.size();
                insert newSMSRequest;
                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                req.setObjectId(newSMSRequest.id); 
                Approval.ProcessResult result = Approval.process(req); 
                newSMSRequest = new campaign_SMS__c(); 
                smsHistory();
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'SMS Request Submitted Successfully'));
            }else{
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'Message cannot be blank or no inquiries in the date range or Scheduled date is blank or scheduled date should cannot be in Past'));
            }
        }catch(exception e){
            ApexPages.addMessages(e);
            Database.rollback(sp);
        }
    }
    
   
    
    public void callSMSJob(){
        system.debug(CampsmsId+'>>>>>>>>>>>');
        Campaign_sms__c campSMS = [select id,SMS_Queued__c ,Inquiry_Start_Date__c,Inquiry_End_Date__c,SMS_Message__c from Campaign_sms__c where id=:CampsmsId];
        inquiriesToSendSMS = [select id from inquiry__c where campaign__c=:marketingid and createddate>=:campSMS.Inquiry_start_date__c and createddate<=:campSMS.Inquiry_end_date__c and mobile_phone__c!=null LIMIT 50000];            
        system.debug(marketingid);system.debug(campSMS.SMS_Message__c);system.debug(inquiriesToSendSMS.size());system.debug(CampsmsId);
        if(marketingid != null &&  campSMS.SMS_Message__c != null && inquiriesToSendSMS.size()>0 && CampsmsId != null){
            system.enqueueJob(new Async_Rest_SMS(marketingid,campSMS.SMS_Message__c,inquiriesToSendSMS,CampsmsId));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.info,'Message are Queued Successfully'));
            campSMS.SMS_Queued__c =true;
            update campSMS;
            smsHistory();
            campSMS = new campaign_sms__c();
            
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'Message cannot be blank or no inquiries in the date range'));
            
        }
        
    }
    
    // Filter and Call sms Class based on the User Input values
    public void filterInquries(){        
        //inquiriesToSendSMS = new list<inquiry__c>();
        if(newSMSRequest.Inquiry_start_date__c!= null && newSMSRequest.Inquiry_end_date__c!= null){
            
            inquiriesToSendSMS = [select id from inquiry__c where campaign__c=:marketingid and createddate>=:newSMSRequest.Inquiry_start_date__c and createddate<=:newSMSRequest.Inquiry_end_date__c and mobile_phone__c!=null LIMIT 50000];            
            if(inquiriesToSendSMS.size() >0){
                showMessageInput = true;
                inqCount = inquiriesToSendSMS.size();
                smsHistory();
            }
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.info,'Please select Start Date and End Date'));
        }
    }
    
    /*
    // Get the SMS Balance
    public void smsBalance(){
        //http://ep.securebulksms.com/Developer/api/BulkSMS/get?username=salesforce&password=F@c3b00k@123&bulkid=3040405
        Http h = new Http();
        HttpRequest req = new HttpRequest();        
        req.setEndpoint('http://ep.securebulksms.com/Developer/api/Credits/GetBalance/?username=salesforce&password=F@c3b00k@123'); 
        req.setMethod('GET');
        //req.setheader('Content-Type','application/json'); 
          
        HttpResponse res = h.send(req);
        system.debug(res.getBody()); 
        smsBalance = res.getBody();
    }
    */
    
    
}