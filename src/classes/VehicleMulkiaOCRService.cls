public class VehicleMulkiaOCRService {

    public static String uploadFile(List<String> lstFiles){

        //StaticResource  sRes1 = [SELECT Body 
        //                        FROM StaticResource 
        //                        WHERE Name = 'TestMulkiaFrontImage'];
        //System.debug('sRes1 body: '+sRes1.body);

        //StaticResource  sRes2 = [SELECT Body 
        //                        FROM StaticResource 
        //                        WHERE Name = 'TestMulkiaBackImage'];
        //System.debug('sRes1 body: '+sRes2.body);

        //blob fileBody1 = sRes1.body;
        //blob fileBody2 = sRes2.body;

    
        String boundary = '------------------------700252463407842306668015';

        //Previous logic - 28 Jan
        String header = '--'+boundary+'\nContent-Disposition: form-data; name="file1"; filename="idFront.jpg";\nContent-Type: application/octet-stream';
        
        String footer = '--'+boundary+'--';             
        String headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
        while(headerEncoded.endsWith('='))
        {
         header+=' ';
         headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
        }
        //String bodyEncoded = EncodingUtil.base64Encode(fileBody1);
        String bodyEncoded = lstFiles[0];
        if(bodyEncoded.containsIgnoreCase('base64,')) {
            bodyEncoded = bodyEncoded.substringAfter('base64,');
        }
        System.debug('bodyEncoded:: '+bodyEncoded);
        
   
        Blob bodyBlob = null;
        String last4Bytes = bodyEncoded.substring(bodyEncoded.length()-4,bodyEncoded.length());             
       
       if(last4Bytes.endsWith('==')) {
          
          System.debug('In == if 1');
          last4Bytes = last4Bytes.substring(0,2) + '0K';
          bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
          
          String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
          bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);
        } else if(last4Bytes.endsWith('=')) {
          
          System.debug('In = if 1');
          last4Bytes = last4Bytes.substring(0,3) + 'N';
          bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
          footer = '\n' + footer;
          String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
          bodyblob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);   
               
        } 
        else {
           // Prepend the CR LF to the footer2
           System.debug('In else part 1');
           footer = '\r\n' + footer;
           String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
           bodyblob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);  
         }

        //For Second File
          String header2 = '--'+boundary+'\nContent-Disposition: form-data; name="file2"; filename="idBack.jpg";\nContent-Type: application/octet-stream';
        
         String footer2 = '--'+boundary+'--';             
         String headerEncoded2 = EncodingUtil.base64Encode(Blob.valueOf(header2+'\r\n\r\n'));
         while(headerEncoded2.endsWith('='))
         {
          header2+=' ';
          headerEncoded2 = EncodingUtil.base64Encode(Blob.valueOf(header2+'\r\n\r\n'));
         }
         //String bodyEncoded2 = EncodingUtil.base64Encode(fileBody2);
         String bodyEncoded2 = lstFiles[1];
         if(bodyEncoded2.containsIgnoreCase('base64,')) {
            bodyEncoded2 = bodyEncoded2.substringAfter('base64,');
         }
         System.debug('bodyEncoded2:: '+bodyEncoded2);
        

         Blob bodyblob2 = null;
         String last4Bytes2 = bodyEncoded2.substring(bodyEncoded2.length()-4,bodyEncoded2.length());

        

        if(last4Bytes2.endsWith('==')) {
          
           System.debug('In == if');
           last4Bytes2 = last4Bytes2.substring(0,2) + '0K';
           bodyEncoded2 = bodyEncoded2.substring(0,bodyEncoded2.length()-4) + last4Bytes2;
          
           String footerEncoded2 = EncodingUtil.base64Encode(Blob.valueOf(footer2));
           bodyblob2 = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+headerEncoded2+bodyEncoded2+footerEncoded2);
         } else if(last4Bytes2.endsWith('=')) {
          
           System.debug('In = if');
           last4Bytes2 = last4Bytes2.substring(0,3) + 'N';
           bodyEncoded2 = bodyEncoded2.substring(0,bodyEncoded2.length()-4) + last4Bytes2;
           // We have appended the CR e.g. \r, still need to prepend the line feed to the footer2
           footer2 = '\n' + footer2;
           String footerEncoded2 = EncodingUtil.base64Encode(Blob.valueOf(footer2));
           bodyblob2 = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+headerEncoded2+bodyEncoded2+footerEncoded2);  
         } else {
           
           System.debug('In else part');
           footer2 = '\r\n' + footer2;
           String footerEncoded2 = EncodingUtil.base64Encode(Blob.valueOf(footer2));
           bodyblob2 = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+headerEncoded2+bodyEncoded2+footerEncoded2);  
         }
         
          HttpRequest req = new HttpRequest();
         
          req.setMethod('POST');
          req.setEndpoint('https://falcon.mytbits.com:6081/falcon-rdca/rest/rdcaService/vehicleRegistration');
          req.setBodyAsBlob(bodyblob2);
          req.setHeader('Content-Length',String.valueof(req.getBodyAsBlob().size()));
          //req.setHeader('Content-Length',contentLength);
          req.setHeader('Authorization','clJUMHpmM3laNWZvU2dIUXI1TUtRbk1VODY4S2piYjI');
          req.setHeader('Content-Type','multipart/form-data; boundary='+boundary);
          req.setHeader('Accept-Encoding','gzip, deflate, br');
          req.setHeader('Connection', 'keep-alive');
          req.setHeader('Cache-Control', 'no-cache');
          req.setHeader('Host', 'falcon.mytbits.com:6081');
          req.setHeader('Accept', 'application/json');
          req.setTimeout(120000);

          Http http = new Http();
          HTTPResponse res = http.send(req);   
          System.debug('status = '+res.getStatusCode() );
          //for testing
          //String mockRes = '{"status":"Success","result":"Success","errorCode":"0","errorDescription":null,"serverTime":19141,"fields":[{"key":"Traffic Plate no","value":"123"},{"key":"T. C. No","value":"2115"},{"key":"Owner","value":"Test"},{"key":"Nationality","value":"India"},{"key":"Exp. Date","value":""},{"key":"Reg. Date","value":""},{"key":"Ins. Exp","value":""},{"key":"Policy","value":""},{"key":"Mortage","value":""},{"key":"Model","value":""},{"key":"Origin","value":""},{"key":"Empty Weight","value":""},{"key":"No. of Pass","value":""},{"key":"Veh Type","value":""},{"key":"G. V. W.","value":""},{"key":"Engine No","value":""},{"key":"Chassie No","value":""}]}';
          //res.setBody(mockRes);
          System.debug('Body = '+res.getBody());

        //Parsing logic starts here
        Map<String, Object> responseMap = new Map<String, Object>();

        if(res.getStatusCode() == 200) {
            return res.getBody();
        }
        else {
            return null;
        }
    }   

}