public with sharing class GenerateLUSAILSPACntrl {
   
    public Booking_Unit__c objBu;

    public GenerateLUSAILSPACntrl (ApexPages.StandardController controller) {
        objBu = (Booking_Unit__c)controller.getrecord();
    } 

    public Pagereference getLUSAIL_SPAUrl() {
         
        String LUSAIL_SPAUrl;
        objBu = [SELECT Id, Registration_ID__c,Party_Id__c,Primary_Buyer_s_Name__c FROM Booking_Unit__c WHERE Id =: objBu.Id];
        if(objBu != null && objBu.Registration_ID__c != null) {
            LUSAIL_SPAUrl=IPMSRestServiceDocumentGenration.genrateLUSAIL_SPA_Doc(objBu);
            if(String.isNotBlank(LUSAIL_SPAUrl)){
                system.debug('for insert');
                SR_Attachments__c objAttach = new SR_Attachments__c();
                objAttach.Booking_Unit__c  = objBu.Id;
                objAttach.isValid__c = true;
                objAttach.IsRequired__c = true;
                objAttach.Name = ' LUSAIL_SPA ' ;
                objAttach.Attachment_URL__c = LUSAIL_SPAUrl;
                objAttach.Need_Correction__c =  false;
                insert   objAttach; 
            }
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info, 'Success: Please wait for 5 minutes, system is generating LUSAIL SPA Document.'));
        }else if(objBu.Registration_ID__c == null){
        System.debug('in else');
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please Provide Registration Id')); 
            return null;   
        }
        // Redirect to Payment Plan detail page
        PageReference paymentPlanDetailPage = new PageReference ('/'+objBu.Id);
        paymentPlanDetailPage.setRedirect(true);
        return paymentPlanDetailPage;
    }  

}