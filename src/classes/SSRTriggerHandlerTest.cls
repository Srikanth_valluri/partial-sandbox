@isTest
private without sharing class SSRTriggerHandlerTest {

    public static final map<String, Id> mapRecordTypeId ;
    public static map<ID,ID> mapManagerID ;
    public static map<ID,String> mapManagerRole;
    public static final String strBookingUnitActiveStatus = 'Agreement executed by DAMAC';

    static {
        mapRecordTypeId = new map<String, Id>();
        for(RecordType recTypeInst : [select Id, DeveloperName from RecordType
                                        where SobjectType = 'Sales_Service_Request__c']) {
            mapRecordTypeId.put(recTypeInst.DeveloperName, recTypeInst.Id);
        }

        system.debug('mapRecordTypeId==='+mapRecordTypeId);

        list<User> lstActiveUser = new list<User>();
        mapManagerID = new map<ID,ID>();
        mapManagerRole = new map<ID,String>();
        lstActiveUser = [Select Id,Name,isActive,Managerid,UserType,UserRole.Name,UserRoleID
                                   from User
                                   where isActive = true];

        for(User objUser : lstActiveUser){
            if(objUser.managerid != null){
                mapManagerID.put(objUser.id,objUser.managerid);
            }
            if(objUser.UserRoleID != null){
                mapManagerRole.put(objUser.id,objUser.UserRole.Name);
            }

        }
        system.debug('--mapManagerID--'+mapManagerID.Size());
        system.debug('--mapManagerRole--'+mapManagerRole.Size());
    }

    @isTest static void testMethod1() {

        Test.startTest();
            UserRole roleManagerObj = TestDataFactory_CRM.createRole('Manager', 'Manager');
            insert roleManagerObj;

            Profile managerProfileObj = [SELECT Id FROM Profile WHERE Name='System Administrator'];
            User managerUser = TestDataFactory_CRM.createUser('Manager', managerProfileObj.Id, roleManagerObj.Id);
            insert managerUser;
            
            Profile creProfile = [SELECT Id FROM Profile WHERE Name='Non Elite - CRE'];
            User creUser = TestDataFactory_CRM.createUser('Manager', creProfile.Id, null);
            creUser.ManagerId = managerUser.Id;
            creUser.HOS_ID__c = managerUser.Id;
            insert creUser;

            System.runAs( creUser ) {
                Account objAcc = TestDataFactory_CRM.createPersonAccount();
                insert objAcc ;
                system.debug('objAcc=='+objAcc);

                NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
                insert objDealSR;

                list<Booking__c> listCreateBookingForAccount = TestDataFactory_CRM.createBookingForAccount(null, objDealSR.Id, 1);
                insert listCreateBookingForAccount;

                list<Buyer__c> listBuyer = TestDataFactory_CRM.createBuyer(listCreateBookingForAccount[0].Id, 1, null);
                listBuyer[0].buyer_type__c = 'Individual';
                insert listBuyer;

                List<Booking_Unit__c> bookingUnitList = TestDataFactory_CRM.createBookingUnits( listCreateBookingForAccount, 1);
                bookingUnitList[0].Registration_ID__c = null;
                insert bookingUnitList;

                system.debug('== bookingUnitList =='+bookingUnitList);
                system.debug('== listBuyer =='+listBuyer);
            
            
                List<Sales_Service_Request__c > srList = new List<Sales_Service_Request__c >();
                List<Sales_Service_Request__c > srListToUpdate = new List<Sales_Service_Request__c >();
                
                for( Integer i = 0; i < 2; i++ ) {
                    Sales_Service_Request__c srObj = new Sales_Service_Request__c();

                    srObj.Submit_for_Approval__c = true;
                    srObj.RecordTypeId = mapRecordTypeId.get('Fund_Transfer');
                    srObj.Account__c  = null;
                    srObj.Approval_Status__c = 'Pending';
                    srObj.Credit_Note_Amount__c = 5000;
                    srList.add( srObj );
                }

                insert srList;
                
                for( Sales_Service_Request__c objSSR : srList ) {
                    objSSR.Approving_User_Ids__c  = Label.Sales_Service_Request_Approvers;
                    objSSR.Current_Approving_User_Id__c = creUser.HOS_ID__c;
                    objSSR.Status__c = 'Submitted';
                    srListToUpdate.add( objSSR );
                }
                update srListToUpdate;
               
                
            }
        Test.stopTest();
    }
    
    @isTest static void testMethod2() {

        
            UserRole roleManagerObj = TestDataFactory_CRM.createRole('Manager', 'Manager');
            insert roleManagerObj;

            Profile managerProfileObj = [SELECT Id FROM Profile WHERE Name='System Administrator'];
            User managerUser = TestDataFactory_CRM.createUser('Manager', managerProfileObj.Id, roleManagerObj.Id);
            insert managerUser;
            
            Profile creProfile = [SELECT Id FROM Profile WHERE Name='Non Elite - CRE'];
            User creUser = TestDataFactory_CRM.createUser('Manager', creProfile.Id, null);
            creUser.ManagerId = managerUser.Id;
            creUser.HOS_ID__c = managerUser.Id;
            insert creUser;

            System.runAs( creUser ) {
                Account objAcc = TestDataFactory_CRM.createPersonAccount();
                insert objAcc ;
                system.debug('objAcc=='+objAcc);

                NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
                insert objDealSR;

                list<Booking__c> listCreateBookingForAccount = TestDataFactory_CRM.createBookingForAccount(null, objDealSR.Id, 1);
                insert listCreateBookingForAccount;

                list<Buyer__c> listBuyer = TestDataFactory_CRM.createBuyer(listCreateBookingForAccount[0].Id, 1, null);
                listBuyer[0].buyer_type__c = 'Individual';
                insert listBuyer;

                List<Booking_Unit__c> bookingUnitList = TestDataFactory_CRM.createBookingUnits( listCreateBookingForAccount, 1);
                bookingUnitList[0].Registration_ID__c = null;
                insert bookingUnitList;

                system.debug('== bookingUnitList =='+bookingUnitList);
                system.debug('== listBuyer =='+listBuyer);
            
            
                List<Sales_Service_Request__c > srList = new List<Sales_Service_Request__c >();
                List<Sales_Service_Request__c > srListToUpdate = new List<Sales_Service_Request__c >();
                
                for( Integer i = 0; i < 2; i++ ) {
                    Sales_Service_Request__c srObj = new Sales_Service_Request__c();

                    srObj.Submit_for_Approval__c = true;
                    srObj.RecordTypeId = mapRecordTypeId.get('Fund_Transfer');
                    srObj.Account__c  = null;
                    srObj.Approval_Status__c = 'Pending';
                    srObj.Credit_Note_Amount__c = 5000;
                    srObj.Approving_User_Ids__c  = Label.Sales_Service_Request_Approvers;
                    srObj.Current_Approving_User_Id__c = creUser.HOS_ID__c;
                    srList.add( srObj );
                }

                insert srList;
                
                for( Sales_Service_Request__c objSSR : srList ) {
                    objSSR.Approving_User_Ids__c  = '';
                    objSSR.Current_Approving_User_Id__c = '';
                    objSSR.Status__c = 'Submitted';
                    srListToUpdate.add( objSSR );
                }
                update srListToUpdate;
            }
            
            Test.startTest();
               Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment() );
            Test.stopTest();
    }
}