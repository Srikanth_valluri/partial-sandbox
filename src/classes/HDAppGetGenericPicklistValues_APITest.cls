@isTest
private class HDAppGetGenericPicklistValues_APITest{
    
    @isTest
    static void GetPicklistFieldsTest() {

        String jsonRequest =  '   {  '  + 
        '       "requested_picklist_fields" : [  '  + 
        '           "preferred_language",  '  + 
        '           "other_payment_types"  '  + 
        '       ]  '  + 
        '  }  ' ; 

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getRequestedPicklist';  
        req.requestBody = Blob.valueOf(jsonRequest);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
            HDAppGetGenericPicklistValues_API.GetPicklistFields();
        Test.stopTest();
    }
}