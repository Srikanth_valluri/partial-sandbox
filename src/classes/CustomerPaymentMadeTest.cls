/*
* Description - Test class developed for 'PaymentMade1'
*
* Version            Date            Author            Description
* 1.0                27/11/17        Monali            Initial Draft
*/
@isTest
private class CustomerPaymentMadeTest {
    static testMethod void testMethod1() {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='12345');
        insert objAcc;

        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        Booking__c objBooking = new Booking__c(Account__c=objAcc.Id, Deal_SR__c=dealSR.Id);
        insert objBooking;

        Booking_Unit__c BUObj = new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='Test name',
        Registration_ID__c = '92061', Registration_Status__c = 'Active Status', Unit_Selling_Price_AED__c = 100);
        insert BUObj;

        Test.startTest();

        SOAPCalloutServiceMock.returnToMe = new Map<String, PaymentMade1.customerPaymentMadeResponse_element>();
        PaymentMade1.customerPaymentMadeResponse_element response1 = new PaymentMade1.customerPaymentMadeResponse_element();
        response1.return_x = '{"data":[{"ATTRIBUTE3":"Kanchan Mahajan","ATTRIBUTE10":"10000","ATTRIBUTE2":"176374","ATTRIBUTE1":"57020","ATTRIBUTE9":"0","ATTRIBUTE8":null,"ATTRIBUTE7":null,"ATTRIBUTE6":"05-JUN-2014","ATTRIBUTE5":"Receipt","ATTRIBUTE4":"Cash"}],"message":"[45] Customer Statement Fetched for Reg Id =57020","status":"S"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() ); 

        //PaymentMade1.CustomerPaymentHttpSoap11Endpoint obj = new PaymentMade1.CustomerPaymentHttpSoap11Endpoint();
        //String strResponse = obj.customerPaymentMade('2-'+String.valueOf( Datetime.now().getTime()),'GET_CUSTOMER_PAYMENT_MADE','SFDC', regTerms);

        String strResponse = CustomerPaymentMade.getCustomerPaymentMade(BUObj.Registration_ID__c);

        Test.stopTest();
    }
}