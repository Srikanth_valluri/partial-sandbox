@isTest
public class ValidateEmailAndNumber_Test {

    static testMethod void validateData () {
        ID recordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry Decrypted').getRecordTypeId();      
        Inquiry__c inq = new Inquiry__c ();
        inq.Mobile_phone__c = '7894561230';
        inq.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Mobile_Phone_Encrypt__c = 'WERTYUIYRTYUIYRTYU';
        inq.Class__c = 'test';
        inq.First_Name__c = 'test';
        inq.Inquiry_Status__c = 'Active';
        inq.Inquiry_Source__c ='Agent Referral';  
        inq.RecordtypeId = recordTypeId;
        inq.Class__c = 'test';
        inq.Email__c = 'test@gmail.com';
        inq.Phone_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Nationality__c = 'Australian';
        insert inq;
        
        Nexmo_ZeroBounce_Validity__c obj = new Nexmo_ZeroBounce_Validity__c ();
        obj.Inquiry__c = inq.ID;
        insert obj;
    }

}