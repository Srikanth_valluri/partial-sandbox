/*******************************************************************************************************
Description : Class to resend send Addedndum document
--------------------------------------------------------------------------------------------------------
Version | Date(DD-MM-YYYY)  | Last Modified By  | Comments
--------------------------------------------------------------------------------------------------------
1.0     | 06-02-2020        |Aishwarya Todkar    | Initial Draft
*****************************************************************************************************/
public class ResendVAT_AddendumCtrl {

    Id CaseId;
    Boolean sendAtPrimary;
    public ResendVAT_AddendumCtrl(ApexPages.StandardController controller) {
        Case tempObjCase = (case)controller.getRecord();
        CaseId = tempObjCase.Id;
        sendAtPrimary = true;
    }
    
/********************************************************************************************************
    Method to resend VAT Addedndum mail
*********************************************************************************************************/
    public void resendVAT_Addendum(){

        System.debug('CaseId--' + CaseId);
        List<Case> listCase = new List<Case>( [SELECT
                                                    Id
                                                    , Booking_Unit__c
                                                    , Alternate_Email__c
                                                FROM
                                                    Case
                                                WHERE
                                                    Id =: CaseId
                                                AND
                                                    Booking_Unit__c != null ] );
        System.debug('listCase==>> ' + listCase);                                                    
        if( listCase != null && listCase.size() > 0 ) {
            String emailAddress = sendAtPrimary ? '' : listCase[0].Alternate_Email__c ;
            VAT_AddendumDocNotifier.getBookingUnit( listCase[0].Booking_Unit__c //BU Id
                                                    , CaseId // Case Id
                                                    , true // Is it resend email functionality
                                                    , emailAddress// New Email Id
                                                    , sendAtPrimary
                                                    );
            ApexPages.addmessage(
                new ApexPages.message( ApexPages.severity.INFO, 
                'Mail has been sent successfully!')
            ); 
        }
    }

/********************************************************************************************************
    Method to resend VAT Addedndum mail to primary address
*********************************************************************************************************/
    public void resendEmailAtPrimaryAddress() { 
        sendAtPrimary = true;
        resendVAT_Addendum();
    }

/********************************************************************************************************
    Method to resend VAT Addedndum mail to Revised address
*********************************************************************************************************/
    public void resendEmailAtRevisedAddress() {
        sendAtPrimary = false;
        resendVAT_Addendum();
    }
}