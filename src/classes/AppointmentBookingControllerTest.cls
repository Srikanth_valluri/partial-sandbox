@isTest
private class AppointmentBookingControllerTest {

    static Account  objAcc;
    static Booking__c objBooking;
    static Booking_Unit__c objBookingUnit;
    static NSIBPM__Service_Request__c objSR;
    static Calling_List__c objCalling;
    static Appointment__c objApp;

     static testMethod void initialMethod() {

        Id userId = UserInfo.getUserId();

        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

        objAcc = new Account(Primary_Language__c = 'Arabic'
                            , RecordTypeId = personAccRTId
                            , FirstName='Test FirstName'
                            , LastName='Test LastName'
                            , Email__pc = 'test@t.com'
                            , Email__c = 'test@t.com'
                            , Type='Person'
                            , party_ID__C='123456'
                            , Primary_CRE__c = userId
                            , Secondary_CRE__c = userId
                            , Tertiary_CRE__c = userId);
        insert objAcc;

        objSR = new NSIBPM__Service_Request__c();
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;

        objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;

        objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC';
        insert objBookingUnit;

        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
        OnOffCheck__c = true);

        settingLst2.add(newSetting1);
        insert settingLst2;

        objCalling = new Calling_List__c();
        objCalling.RecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Handover Calling List').getRecordTypeId();
        objCalling.Account_Email__c = 'test@t.com';
        insert objCalling;

        Location__c loc = new Location__c();
        loc.Location_ID__c = '22656';
        loc.Name = 'JNU';
        insert loc;

        objApp = new Appointment__c();
        objApp.Appointment_Date__c = date.newinstance( 2018, 12, 12 );
        objApp.Appointment_End_Date__c = date.newinstance( 2018, 12, 21 );
        objApp.Building__c = loc.id;
        objApp.Processes__c = 'Handover';
        objApp.Sub_Processes__c = 'Documentation';
        objApp.Group_Name__c = 'Handover Queue';
        objApp.Slots__c = '10:00 - 11:00';
        insert objApp;

        Appointment__c objApp1 = new Appointment__c();
        objApp1.Appointment_Date__c = date.newinstance( 2018, 12, 12 );
        objApp1.Appointment_End_Date__c = date.newinstance( 2018, 12, 21 );
        objApp1.Building__c = loc.id;
        objApp1.Processes__c = 'Handover';
        objApp1.Sub_Processes__c = 'Unit Viewing';
        objApp1.Group_Name__c = 'Handover Queue';
        objApp1.Slots__c = '10:00 - 11:00';
        insert objApp1;

        Appointment__c objApp2 = new Appointment__c();
        objApp2.Appointment_Date__c = date.newinstance( 2018, 12, 12  );
        objApp2.Appointment_End_Date__c = date.newinstance( 2018, 12, 12 );
        objApp2.Building__c = loc.id;
        objApp2.Processes__c = 'Handover';
        objApp2.Sub_Processes__c = 'Key Handover';
        objApp2.Group_Name__c = 'Handover Queue';
        objApp2.Slots__c = '10:00 - 11:00';
        insert objApp2;

        Appointment__c objApp3 = new Appointment__c();
        objApp3.Appointment_Date__c = date.newinstance( 2018, 12, 12 );
        objApp3.Appointment_End_Date__c = date.newinstance( 2018, 12, 21 );
        objApp3.Building__c = loc.id;
        objApp3.Processes__c = 'Handover';
        objApp3.Sub_Processes__c = 'Unit Viewing or Key Handover';
        objApp3.Group_Name__c = 'Handover Queue';
        objApp3.Slots__c = '10:00 - 11:00';
        insert objApp3;

        Appointment__c objApp4 = new Appointment__c();
        objApp4.Appointment_Date__c = System.today();
        objApp4.Appointment_End_Date__c = System.today().addDays(7) ;
        objApp4.Building__c = loc.id;
        objApp4.Processes__c = 'Handover';
        objApp4.Sub_Processes__c = 'Unit Viewing or Key Handover';
        objApp4.Group_Name__c = 'Handover Queue';
        objApp4.Slots__c = '10:00 - 11:00';
        insert objApp4;

        Appointment__c objApp5 = new Appointment__c();
        objApp5.Appointment_Date__c = date.newinstance( 2018, 12, 12 );
        objApp5.Appointment_End_Date__c = date.newinstance( 2018, 12, 21 );
        objApp5.Building__c = loc.id;
        objApp5.Processes__c = 'Collections';
        objApp5.Sub_Processes__c = 'Cheque Collection';
        objApp5.Slots__c = '10:00 - 11:00';
        insert objApp5;

        PageReference customerPage = Page.Customer;
        customerPage.getParameters().put('parentId', objCalling.Id);
        Test.setCurrentPageReference(customerPage);

    }

    static testMethod void Test1() {
        initialMethod();

        list<AppointmentSelectionHandler.AppointmentWrapper> listWrap = new list<AppointmentSelectionHandler.AppointmentWrapper>();
        AppointmentSelectionHandler.AppointmentWrapper objWrap = new AppointmentSelectionHandler.AppointmentWrapper();
        objWrap.isSelected = true;
        objWrap.objCL = objCalling;
        objWrap.objApp = objApp;
        listWrap.add(objWrap);

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockCallingList());

        PageReference pageRef = Page.Customer;
        pageRef.getParameters().put('id', String.valueOf(objAcc.Id));
        pageRef.getParameters().put('view', 'appointmentview');
        Test.setCurrentPage(pageRef);

        AppointmentBookingController objAppointmentBookingController = new AppointmentBookingController();
        objAppointmentBookingController.strSelectedAccount = objAcc.id;
        objAppointmentBookingController.getName();
        objAppointmentBookingController.initMethod();
        objAppointmentBookingController.strSelectedProcess = 'Handover';
        objAppointmentBookingController.initiationSlots();
        objAppointmentBookingController.strSelectedSubProcess =  'Key Handover';
        objAppointmentBookingController.strSelectedBookingUnits = objBookingUnit.id;
        objAppointmentBookingController.getCallingLists();
        objAppointmentBookingController.strSelectedDate = '12-12-2018';
        objAppointmentBookingController.lstAppointmentWrapper = listWrap;
        objAppointmentBookingController.getAvailableAppointments();
        objAppointmentBookingController.strSelectedSlot = '10:00 - 11:00';
        objAppointmentBookingController.slotSelected();
        objAppointmentBookingController.visitPurpose = 'test';
        objAppointmentBookingController.strSelectedDate = '12-12-2018';
        objAppointmentBookingController.createAppointment();
        Test.stopTest();
    }

    static testMethod void Test2() {
        initialMethod();

        list<AppointmentSelectionHandler.AppointmentWrapper> listWrap = new list<AppointmentSelectionHandler.AppointmentWrapper>();
        AppointmentSelectionHandler.AppointmentWrapper objWrap = new AppointmentSelectionHandler.AppointmentWrapper();
        objWrap.isSelected = true;
        objWrap.objCL = objCalling;
        objWrap.objApp = objApp;
        listWrap.add(objWrap);

        Date fromDate = Date.newInstance(2018, 11, 10);
        Date toDate = Date.newInstance(2018, 11, 21);
        insert new DAMACHolidaySettings__c(Name = 'Holidays2018',From__c = fromDate, To__c = toDate);

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockCallingList());

        PageReference pageRef = Page.Customer;
        pageRef.getParameters().put('view', 'appointmentview');
        pageRef.getParameters().put('process', '01-Handover');
        pageRef.getParameters().put('subProcess', 'Key Handover');
        pageRef.getParameters().put('unitId', objBookingUnit.id);
        Test.setCurrentPage(pageRef);

        AppointmentBookingController.holidayFunction('12-12-2018');

        AppointmentBookingController objAppointmentBookingController = new AppointmentBookingController();
        objAppointmentBookingController.strSelectedAccount = objAcc.id;
        objAppointmentBookingController.initMethod();
        objAppointmentBookingController.strSelectedProcess = '';
        objAppointmentBookingController.initiationSlots();
        objAppointmentBookingController.strSelectedSubProcess =  '';
        objAppointmentBookingController.strSelectedBookingUnits = objBookingUnit.id;
        objAppointmentBookingController.getCallingLists();
        Test.stopTest();
    }

    static testMethod void Test3() {
        initialMethod();

        list<AppointmentSelectionHandler.AppointmentWrapper> listWrap = new list<AppointmentSelectionHandler.AppointmentWrapper>();
        AppointmentSelectionHandler.AppointmentWrapper objWrap = new AppointmentSelectionHandler.AppointmentWrapper();
        objWrap.isSelected = true;
        objWrap.objCL = objCalling;
        objWrap.objApp = objApp;
        listWrap.add(objWrap);

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockCallingList());

        PageReference pageRef = Page.Customer;
        pageRef.getParameters().put('view', 'appointmentview');
        Test.setCurrentPage(pageRef);

        AppointmentBookingController objAppointmentBookingController = new AppointmentBookingController();
        objAppointmentBookingController.strSelectedAccount = objAcc.id;
        objAppointmentBookingController.initMethod();
        objAppointmentBookingController.strSelectedProcess = 'Cheque Collection';
        objAppointmentBookingController.initiationSlots();
        objAppointmentBookingController.strSelectedSubProcess = 'Cheque Collection';
        objAppointmentBookingController.strSelectedBookingUnits = objBookingUnit.id;
        objAppointmentBookingController.getCallingLists();
        objAppointmentBookingController.strSelectedDate = '12/12/2018';
        objAppointmentBookingController.lstAppointmentWrapper = listWrap;
        objAppointmentBookingController.getAvailableAppointments();
        objAppointmentBookingController.strSelectedSlot = '10:00 - 11:00';
        objAppointmentBookingController.slotSelected();
        objAppointmentBookingController.visitPurpose = 'test';
        objAppointmentBookingController.createAppointment();
        Test.stopTest();
    }
}