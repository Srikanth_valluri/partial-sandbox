/*****************************************************************************************************
* Description - Test class developed for 'HoNotice1'
*
* Version            Date            Author               Description
* 1.0               06/02/2018       Lochan Karle                Initial Draft
 *******************************************************************************************************/
@isTest
public class HoNotice1Test {
  public static testmethod void testHoNotice1(){
    
    Test.startTest();
    HoNotice1.HONoticeHttpSoap11Endpoint obj = new HoNotice1.HONoticeHttpSoap11Endpoint();
    List<HoNotice2.APPSXXDC_PROCESS_SERX1794747X1X5> regTerms =  new List<HoNotice2.APPSXXDC_PROCESS_SERX1794747X1X5>();
    
    SOAPCalloutServiceMock.returnToMe =  new Map<String, HoNotice1.HandOverNoticeResponse_element>();
    HoNotice1.HandOverNoticeResponse_element  response_x = new HoNotice1.HandOverNoticeResponse_element();
    response_x.return_x = 'S';
    SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
    
    test.setMock(WebServiceMock.class , new SOAPCalloutServiceMock());
    
    String respose = obj.HandOverNotice('P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM',regTerms);    
    System.assertEquals(respose,'S');
    Test.stopTest();    
  }
}