@istest
private class SurveyAndQuestionController_Test {

    //test constructor and calls within it
    @isTest(SeeAllData='true')
    private static void testCreateQuestionController(){
        SurveyTestingUtil tu = new SurveyTestingUtil();
        Apexpages.Standardcontroller std;
        SurveyAndQuestionController cqc = new SurveyAndQuestionController(std);
        cqc.addQuestion();
        cqc.getNewQuestionNum();
        cqc.makeNewQuestionLink();
    }

    @isTest(SeeAllData='true')
    //test constructor and calls within it
    private static void testEditQuestion(){
        SurveyTestingUtil tu = new SurveyTestingUtil();
        Apexpages.Standardcontroller std;
        SurveyAndQuestionController cqc = new SurveyAndQuestionController(std);
        cqc.editQuestion();

    }

    @isTest(SeeAllData='true')
    //test the saving of new questions
    private static void testsaveAndNewQuestion(){
        SurveyTestingUtil tu = new SurveyTestingUtil();
        
        Apexpages.Standardcontroller std;
        SurveyAndQuestionController cqc = new SurveyAndQuestionController(std);
        //test saving new question
        cqc.qQuestion = 'THIS IS A NEW QUESTION';
        cqc.qChoices = '1\\n2\\n3\\3';
        cqc.qRequired=true;
        cqc.questionType='Single Select--Vertical';
        cqc.saveAndNewQuestion();
        //edit existing question
        SurveyAndQuestionController cqcI = new SurveyAndQuestionController(std);
        cqcI.editQuestion();
        cqcI.qQuestion = 'THIS IS A NEW QUESTION THAT IS EXTRA LONG SO THE NAME SHORTENING CALL WILL BE USED, THIS SHOULD BE LONG ENOUGH NOW THIS IS A NEW';
        cqcI.qChoices = '1\\n2\\n3\\3';
        cqcI.qRequired=true;
        cqcI.questionType='Single Select--Vertical';
        cqcI.saveAndNewQuestion();
    }

    @isTest(SeeAllData='true')
    private static void testsavesaveQuestion(){
        SurveyTestingUtil tu = new SurveyTestingUtil();
        
        Apexpages.Standardcontroller std;
        SurveyAndQuestionController cqc = new SurveyAndQuestionController(std);
        //test saving new question
        cqc.qQuestion = 'THIS IS A NEW QUESTION';
        cqc.qChoices = '1\\n2\\n3\\3';
        cqc.qRequired=true;
        cqc.questionType='Single Select--Vertical';
        cqc.controllerSavQuestion();
        //edit existing question
        SurveyAndQuestionController cqcI = new SurveyAndQuestionController(std);
        cqcI.editQuestion();
        cqcI.qQuestion = 'THIS IS A NEW QUESTION THAT IS EXTRA LONG SO THE NAME SHORTENING CALL WILL BE USED, THIS SHOULD BE LONG ENOUGH NOW';
        cqcI.qChoices = '1\\n2\\n3\\3';
        cqcI.qRequired=true;
        cqcI.questionType='Single Select--Vertical';
        cqcI.controllerSavQuestion();
    }

    @isTest(SeeAllData='true')
    //test constructor and calls within it
    private static void testPreviewQuestion(){
        SurveyTestingUtil tu = new SurveyTestingUtil();
        
        Apexpages.Standardcontroller std;
        SurveyAndQuestionController cqc = new SurveyAndQuestionController(std);

        cqc.editQuestion();
        cqc.previewQuestion();

        cqc.editQuestion();
        cqc.previewQuestion();

        cqc.editQuestion();
        
        cqc.editQuestion();
        


    }


    

    @isTest(SeeAllData='true')
    private static void testupdateSurveyThankYouAndLink() {
        SurveyTestingUtil tu = new SurveyTestingUtil();
        
        Apexpages.Standardcontroller stc;
        SurveyAndQuestionController vsc = new SurveyAndQuestionController(stc);
        vsc.surveyThankYouText = 'new stuff';
        vsc.surveyThankYouURL = 'more new stff';
        
    }

    //------------------------------------------------------------------------------//

    @isTest(SeeAllData='true')
    private static void testRefreshQuestionList() {
        SurveyTestingUtil tu = new SurveyTestingUtil();
        
        Apexpages.Standardcontroller stc;
        SurveyAndQuestionController vsc = new SurveyAndQuestionController(stc);
        vsc.getAQuestion();

        // Retrieve questions for this survey
        List<Survey_Question__c> sq = new List<Survey_Question__c>();
        sq = [Select id, orderNumber__c from Survey_Question__c];

        
        // Verify that the question with order 1 is not the same as the one retrieved previously
        
        // update the question list, and make sure it has been modified as well
        vsc.refreshQuestionList();
        vsc.setupEditQuestion ();
        vsc.setupQuestionFields ();
        vsc.updateQuestion ();
        vsc.setSurveyNameAndThankYou ('');
        vsc.updateSurveyName ();
        vsc.updateSurveyThankYouAndLink ();
        vsc.updateOrderList ();
        vsc.resultPage ();
        vsc.deleteRefresh ();
        vsc.getResponses ();
        vsc.deleteOldResponses('');
        vsc.questionReference = 'test';
        vsc.stringToSelectOptions('');
        
    }

//------------------------------------------------------------------------------//

    @isTest(SeeAllData='true')
    private static void testDeleteQuestion() {
        SurveyTestingUtil tu = new SurveyTestingUtil();
        
        Apexpages.Standardcontroller stc;
        SurveyAndQuestionController vsc = new SurveyAndQuestionController(stc);


        

    }
//------------------------------------------------------------------------------//


/**/
}