/****************************************************************************************************
* Name               : DeletePreviousTSASharingBatch                                                *
* Description        : Delete Previous TSA's Inquiry Share records                                  *
* Created Date       : 27/06/2018                                                                   *
* Created By         : Eternus                                                                      *
*---------------------------------------------------------------------------------------------------*
* Ver   Date        Author              Description                                                 *
* 1.0   27-06-2018  Craig Lobo          Initial Draft                                               *
****************************************************************************************************/
global class DeletePreviousTSASharingBatch implements Database.Batchable<sObject> {

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([SELECT Id, Telesales_Executive__c
                                           FROM Inquiry__c 
                                          WHERE Telesales_Executive__c != null
                                            AND RecordTypeName__c = 'Inquiry'
        ]);
    }

    global void execute(Database.BatchableContext BC, List<Inquiry__c> scope) {
        System.debug('==scope==' + scope);
        System.debug('==scope==' + scope.size());
        Map<Id, Id> inquiryTSAMap = new Map<Id, Id>();
        List<String> inquiryIdList = new List<String>();
        List<Inquiry__Share> inquiryShareDelete = new List<Inquiry__Share>();

        for (Inquiry__c inq : scope) {
            inquiryTSAMap.put(inq.Id, inq.Telesales_Executive__c);
            inquiryIdList.add(inq.Id);
        }

        if (!inquiryIdList.isEmpty()) {
            for (Inquiry__Share iqShare : [ SELECT Id, ParentId, UserOrGroupId, RowCause 
                                              FROM Inquiry__Share 
                                             WHERE RowCause = 'Assign_TS__c' 
                                               AND ParentId IN :inquiryIdList
            ]) {
                if (inquiryTSAMap.containsKey(iqShare.ParentId)
                    && inquiryTSAMap.get(iqShare.ParentId) != null
                ) {
                    if (iqShare.UserOrGroupId != inquiryTSAMap.get(iqShare.ParentId) ) {
                        inquiryShareDelete.add(iqShare);
                    }
                }
            }
            System.debug('BEFORE >>>>>>' + inquiryShareDelete);
            delete inquiryShareDelete;
            System.debug('AFTRE>>>>>>' + inquiryShareDelete.size());
        }
    }

    global void finish(Database.BatchableContext BC) {
        
    }

}