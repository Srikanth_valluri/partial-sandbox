@isTest
public class Damac_FAServices_Test{
    testMethod static void faServices(){
        Account acc = new Account ();
        acc.Name = 'test';
        acc.Agency_Type__c = 'Corporate';
        acc.Vendor_ID__c = '969696';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        insert acc;
        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal', false, null);
        sr.Eligible_to_Sell_in_Dubai__c = true;
        sr.Agency_Type__c = 'Individual';
        sr.ID_Type__c = 'Passport';
        sr.Token_Amount_AED__c = 40000;
        sr.RecordTypeId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Change Agent').getRecordTypeId();
        sr.Booking_Wizard_Level__c = null;
        sr.Agency_Email_2__c = 'test2@gmail.com';
        sr.Alternate_Agency_Email_2__c = 'test1@gmail.com';
        sr.Country_of_Sale__c = 'UAE';
        sr.Mode_of_Payment__c = 'Cash';
        sr.agency__c = acc.id;
        insert sr;
        Location__c loc = InitializeSRDataTest.createLocation('123', 'Building');
        loc.Property_ID__c = '123';
        insert loc;
        Payment_Plan__c pp = new Payment_Plan__c();
        pp.Effective_From__c = system.today();
        pp.Effective_To__c = system.today();
        pp.Building_Location__c =  loc.Id;
        pp.term_id__c = '1234';
        insert pp;
        Booking__c bk = InitializeSRDataTest.createBooking(sr.id);
        
        insert bk;
        
        Booking_Unit__c bu1 = new Booking_Unit__c();
        bu1.Booking__c = bk.id;
        bu1.Payment_Method__c = 'Cash';
        bu1.Primary_Buyer_s_Email__c = 'test@damac.com';
        bu1.Primary_Buyer_s_Name__c = 'testNSI';
        bu1.Primary_Buyer_s_Nationality__c = 'Russia';
        bu1.Registration_ID__c = '1234';
        bu1.Payment_Plan_Id__c = pp.id;
        bu1.Furniture__c = TRUE ;
        insert bu1;
        Unit_Documents__c testUnitDocuments = new Unit_Documents__c();
        testUnitDocuments.Booking_Unit__c = bu1.Id;
        testUnitDocuments.Document_Name__c = 'Passport Copy';
        testUnitDocuments.Sys_doc_id__c = '32132132131321';
        insert testUnitDocuments;
        Test.startTest();
            Damac_FAServices.doCallout (sr.id);
        Test.stopTest();
    }
}