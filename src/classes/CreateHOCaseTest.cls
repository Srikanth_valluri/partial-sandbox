//AKISHOR
@isTest
private class CreateHOCaseTest{
    static testMethod void test_UndoTest() {  
    
    List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
     TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                    OnOffCheck__c = true); 
    settingLst2.add(newSetting1);
    insert settingLst2;        
             
    // Insert BU
    //Id accntrecid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
    Id dealrecid=Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
    NSIBPM__Service_Request__c Deal = new NSIBPM__Service_Request__c();
    Deal.RecordTypeId=dealrecid;
    insert Deal;
    Booking__c B=new Booking__c();
    B.Booking_Channel__c='Web';
    B.Deal_SR__c=Deal.Id;
    insert B;
    Booking_Unit__c objBU = new Booking_Unit__c();
    objBU.Unit_Name__c='HWT/PT124/XL2265B';
    objBU.Registration_ID__c='71609';
    objBU.Registration_Status_Code__c='LE';
    objBU.Party_Id__c='79658';
    objBU.Booking__c=B.Id;
    insert objBU;
  
         test.startTest();
         PageReference pageRef = Page.CreateHOCasePage;
         Test.setCurrentPage(pageRef);
         ApexPages.StandardController sc = new ApexPages.standardController(objBU);
         CreateHandoverCases controller = new CreateHandoverCases(sc);
         controller.BUId = objBU.Id;
         Pagereference newPg = controller.CreateCase();
         test.stopTest();
     } 
}