/*
* Description - Test class developed for GenerateDocForFMCLCntrl
*/
@isTest
public class GenerateDocForFMCLCntrlTest  {
    static final String TEST_URL = 'https://test.damacgroup.com/test.pdf';
    static Id fmCLRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('FM Collections').RecordTypeId;
    
    static testMethod void testGenerateFMSOA() {
     List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '1234';           
        }
        insert lstBookingUnits;

        List<Calling_List__c> lstCallingLists = createCallingList( fmCLRecordTypeId , 1 ,lstBookingUnits );
        insert lstCallingLists;
        system.debug(' Test lstCallingLists : ' + lstCallingLists);
        
        Test.startTest();
            PageReference popPage = Page.GenerateUnitSOAForFMCL;
            Test.setCurrentPage(popPage);
            popPage.getParameters().put('Id',String.valueOf(lstCallingLists[0].Id));
            ApexPages.StandardController sc = new ApexPages.standardController( lstCallingLists[0] );
            GenerateDocForFMCLCntrl objController = new GenerateDocForFMCLCntrl(sc);
            FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
            response.complete = true;
            response.actions = new List<FmIpmsRestServices.Action>();
            FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
            action.url = TEST_URL;
            response.actions.add(action);

            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

            String responseBody;
            
            objController.generateFMSOA();
            
        Test.stopTest();
    }
    
    static testMethod void testGenerateFMPartySOA() {
     List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '1234';
        }
        insert lstBookingUnits;

        List<Calling_List__c> lstCallingLists = createCallingList( fmCLRecordTypeId , 1 ,lstBookingUnits );
        lstCallingLists[0].Account__c = objAcc.Id;
        lstCallingLists[0].Party_ID__c = '65432';
        insert lstCallingLists;
        system.debug(' Test lstCallingLists : ' + lstCallingLists);
        
        Test.startTest();
            PageReference popPage = Page.GeneratePartyWiseSOAForFMCL;
            Test.setCurrentPage(popPage);
            popPage.getParameters().put('Id',String.valueOf(lstCallingLists[0].Id));
            ApexPages.StandardController sc = new ApexPages.standardController( lstCallingLists[0] );
            GenerateDocForFMCLCntrl objController = new GenerateDocForFMCLCntrl(sc);
            
            FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
            response.complete = true;
            response.actions = new List<FmIpmsRestServices.Action>();
            FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
            action.url = TEST_URL;
            response.actions.add(action);

            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

            String responseBody;

            objController.generateFMPartySOA();
        Test.stopTest();
    }
   
    static testMethod void test_GenerateFMPropertyWiseSOA() {
     List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '1234';
            objUnit.Unit_Name__c = 'AD/201/2015';
        }
        insert lstBookingUnits;

        List<Calling_List__c> lstCallingLists = createCallingList( fmCLRecordTypeId , 1 ,lstBookingUnits );
        lstCallingLists[0].Account__c = objAcc.Id;
        lstCallingLists[0].Unit_Name__c = 'AD/201/2015';
        lstCallingLists[0].Party_ID__c = '65432';
        insert lstCallingLists;
        system.debug(' Test lstCallingLists : ' + lstCallingLists);
        
        Test.startTest();
            PageReference popPage = Page.GeneratePropertyWiseSOAForFMCL;
            Test.setCurrentPage(popPage);
            popPage.getParameters().put('Id',String.valueOf(lstCallingLists[0].Id));
            ApexPages.StandardController sc = new ApexPages.standardController( lstCallingLists[0] );
            GenerateDocForFMCLCntrl objController = new GenerateDocForFMCLCntrl(sc);
            
            FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
            response.complete = true;
            response.actions = new List<FmIpmsRestServices.Action>();
            FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
            action.url = TEST_URL;
            response.actions.add(action);

            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

            String responseBody;
            objController.generateFMPropertyWiseSOA();
        Test.stopTest();
    }
    
    
    //Method for negative scenario
    static testMethod void test_generatestataccountForNegative() {
     List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '';           
        }
        insert lstBookingUnits;

        List<Calling_List__c> lstCallingLists = createCallingList( fmCLRecordTypeId , 1 ,lstBookingUnits );
        lstCallingLists[0].Registration_ID__c = '';
        insert lstCallingLists;
        system.debug(' Test lstCallingLists : ' + lstCallingLists);
        
        Test.startTest();
            PageReference popPage = Page.GenerateUnitSOAForFMCL;
            Test.setCurrentPage(popPage);
            popPage.getParameters().put('Id',String.valueOf(lstCallingLists[0].Id));
            ApexPages.StandardController sc = new ApexPages.standardController( lstCallingLists[0] );
            GenerateDocForFMCLCntrl objController = new GenerateDocForFMCLCntrl(sc);
            FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
            response.complete = true;
            response.actions = new List<FmIpmsRestServices.Action>();
            FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
            action.url = TEST_URL;
            response.actions.add(action);

            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

            String responseBody;
            
            objController.generateFMSOA();
            
        Test.stopTest();
    }
    
    static testMethod void test_generatestatofpenalty() {
     List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Party_ID__c = '';
        insert objAcc ;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '1234';
        }
        insert lstBookingUnits;

        List<Calling_List__c> lstCallingLists = createCallingList( fmCLRecordTypeId , 1 ,lstBookingUnits );
        lstCallingLists[0].Account__c = objAcc.Id;
        lstCallingLists[0].Party_ID__c = '';
        insert lstCallingLists;
        system.debug(' Test lstCallingLists : ' + lstCallingLists);
        
        Test.startTest();
            PageReference popPage = Page.GeneratePartyWiseSOAForFMCL;
            Test.setCurrentPage(popPage);
            popPage.getParameters().put('Id',String.valueOf(lstCallingLists[0].Id));
            ApexPages.StandardController sc = new ApexPages.standardController( lstCallingLists[0] );
            GenerateDocForFMCLCntrl objController = new GenerateDocForFMCLCntrl(sc);
            
            FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
            response.complete = true;
            response.actions = new List<FmIpmsRestServices.Action>();
            FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
            action.url = TEST_URL;
            response.actions.add(action);

            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

            String responseBody;

            objController.generateFMPartySOA();
        Test.stopTest();
    }

   static testMethod void test_generatestatofpenalty1() {
     List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Party_ID__c = '';
        insert objAcc ;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '1234';
            objUnit.Unit_Name__c = '';
        }
        insert lstBookingUnits;

        List<Calling_List__c> lstCallingLists = createCallingList( fmCLRecordTypeId , 1 ,lstBookingUnits );
        lstCallingLists[0].Account__c = objAcc.Id;
        lstCallingLists[0].Unit_Name__c = '';
        lstCallingLists[0].Party_ID__c = '';
        insert lstCallingLists;
        system.debug(' Test lstCallingLists : ' + lstCallingLists);
        
        Test.startTest();
            PageReference popPage = Page.GeneratePropertyWiseSOAForFMCL;
            Test.setCurrentPage(popPage);
            popPage.getParameters().put('Id',String.valueOf(lstCallingLists[0].Id));
            ApexPages.StandardController sc = new ApexPages.standardController( lstCallingLists[0] );
            GenerateDocForFMCLCntrl objController = new GenerateDocForFMCLCntrl(sc);
            
            FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
            response.complete = true;
            response.actions = new List<FmIpmsRestServices.Action>();
            FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
            action.url = TEST_URL;
            response.actions.add(action);

            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

            String responseBody;
            objController.generateFMPropertyWiseSOA();
        Test.stopTest();
    }
    static testMethod void testGenerateFMSOA1() {
     List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '1234';           
        }
        insert lstBookingUnits;

        List<Calling_List__c> lstCallingLists = createCallingList( fmCLRecordTypeId , 1 ,lstBookingUnits );
        insert lstCallingLists;
        system.debug(' Test lstCallingLists : ' + lstCallingLists);
        
        Test.startTest();
            PageReference popPage = Page.GenerateUnitSOAForFMCL;
            Test.setCurrentPage(popPage);
            popPage.getParameters().put('Id',String.valueOf(lstCallingLists[0].Id));
            ApexPages.StandardController sc = new ApexPages.standardController( lstCallingLists[0] );
            GenerateDocForFMCLCntrl objController = new GenerateDocForFMCLCntrl(sc);
            FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
            response.complete = true;
            response.actions = new List<FmIpmsRestServices.Action>();
            FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
            action.url = '';
            response.actions.add(action);

            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

            String responseBody;
            
            objController.generateFMSOA();
            
        Test.stopTest();
    }
    
    static testMethod void testGenerateFMPartySOA1() {
     List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '1234';
        }
        insert lstBookingUnits;

        List<Calling_List__c> lstCallingLists = createCallingList( fmCLRecordTypeId , 1 ,lstBookingUnits );
        lstCallingLists[0].Account__c = objAcc.Id;
        lstCallingLists[0].Party_ID__c = '65432';
        insert lstCallingLists;
        system.debug(' Test lstCallingLists : ' + lstCallingLists);
        
        Test.startTest();
            PageReference popPage = Page.GeneratePartyWiseSOAForFMCL;
            Test.setCurrentPage(popPage);
            popPage.getParameters().put('Id',String.valueOf(lstCallingLists[0].Id));
            ApexPages.StandardController sc = new ApexPages.standardController( lstCallingLists[0] );
            GenerateDocForFMCLCntrl objController = new GenerateDocForFMCLCntrl(sc);
            
            FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
            response.complete = true;
            response.actions = new List<FmIpmsRestServices.Action>();
            FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
            action.url = '';
            response.actions.add(action);

            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

            String responseBody;

            objController.generateFMPartySOA();
        Test.stopTest();
    }
   
    static testMethod void test_GenerateFMPropertyWiseSOA1() {
     List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '1234';
            objUnit.Unit_Name__c = 'AD/201/2015';
        }
        insert lstBookingUnits;

        List<Calling_List__c> lstCallingLists = createCallingList( fmCLRecordTypeId , 1 ,lstBookingUnits );
        lstCallingLists[0].Account__c = objAcc.Id;
        lstCallingLists[0].Unit_Name__c = 'AD/201/2015';
        lstCallingLists[0].Party_ID__c = '65432';
        insert lstCallingLists;
        system.debug(' Test lstCallingLists : ' + lstCallingLists);
        
        Test.startTest();
            PageReference popPage = Page.GeneratePropertyWiseSOAForFMCL;
            Test.setCurrentPage(popPage);
            popPage.getParameters().put('Id',String.valueOf(lstCallingLists[0].Id));
            ApexPages.StandardController sc = new ApexPages.standardController( lstCallingLists[0] );
            GenerateDocForFMCLCntrl objController = new GenerateDocForFMCLCntrl(sc);
            
            FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
            response.complete = true;
            response.actions = new List<FmIpmsRestServices.Action>();
            FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
            action.url = '';
            response.actions.add(action);

            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

            String responseBody;
            objController.generateFMPropertyWiseSOA();
        Test.stopTest();
    }
    /*
     @ Description : To create calling list reocord
     @ Return      : list of calling list to be created
    */
    public static List<Calling_List__c> createCallingList( Id RecordTypeIdCollection, Integer counter , List<Booking_Unit__c> lstBookingUnits ) {
        List<Calling_List__c> lstCallingLists = new List<Calling_List__c>();
        for( Integer i=0; i<counter; i++ ) {
            lstCallingLists.add(new Calling_List__c( Registration_ID__c = '1234' , Inv_Due__c = 0, DM_Due_Amount__c = 0 , RecordTypeId = RecordTypeIdCollection ) );
        }
        return lstCallingLists;
    }
}