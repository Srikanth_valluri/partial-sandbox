public class AsyncReceiptWebserviceSchedulable implements Schedulable {
    
    private List<Id> BookingIds;
    public AsyncReceiptWebserviceSchedulable(List<Id> BookingIdsList){
        BookingIds = BookingIdsList;
    }
    
    public void execute(SchedulableContext sc) {
        system.enqueueJob(new AsyncReceiptWebservice (BookingIds, 'StatusUpdate'));
        
        //Abort the Scheduled class job once the job is queued
        System.abortJob(sc.getTriggerId());
    }
}