@isTest
public class CRMCallUpdateTest{
  
    public static testMethod void CRMAmeyoCallUpdatetest1() {
        
        Profile p2 = [SELECT Id FROM Profile WHERE Name='System Administrator' Limit 1];
        //ID adminRoleId = [ Select id from userRole where name = 'Director'].id;
        UserRole userRoleObj = new UserRole(Name = 'Director', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        User u2 = new User(Alias = 'standt2', Email='stand2@testorg.com', userRoleId=userRoleObj.Id, isActive = true,
                           EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p2.Id,Extension = '1212',
                           TimeZoneSidKey='America/Los_Angeles', UserName='adm@testorg123.com');
        insert u2;
        System.runAs(u2) {
            List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                        OnOffCheck__c = true);
            
            settingLst2.add(newSetting1);
            insert settingLst2;
            
            NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
            insert objDealSR ;
            
            Account objAcc = TestDataFactory_CRM.createPersonAccount();
            insert objAcc ;
            
            List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objDealSR.Id, 1 );
            insert lstBooking ;
            
            List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking, 1 );
            insert lstBookingUnit ;
            
            List<Calling_List__c> lstCalling = TestDataFactory_CRM.createCallingList( 'Appointment_Scheduling', 1 , lstBookingUnit[0] );
            insert lstCalling ;
        
        
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();

            //req.requestURI = '/services/apexrest/callWrapup';
            //req.httpMethod = 'POST';
            //req.requestBody = Blob.valueof(JsonMsg);
            RestContext.request = req;
            RestContext.response= res;
   
            //String accountId = String.ValueOf();
            /**(String CrtObjectId, String ObjectType, String RecordId, 
                                            String Remarks, String CallType
                                            ,String AccountName, String AccountId, String CallURL,
                                            String StartDate, String EndDate, String ACW,String Outcome,
                                            //String AHT, String CallingExt, String CallingCRE,String CallingNum, 
                                            String CalledNumber,String IVRResponse, String CustomerFeedback, 
                                            String PreviewTime, String EncriptedNum, String system_disposition, 
                                            String attempt_time, String talk_time)
                                            **/
                                            
            String encryptedMob = EncrytNumber.encryptNo('00919405883787');
            CRMCallUpdate.CRMAmeyoCallUpdate('Test','Account',objAcc.Id, 
                                            'Test', 'Connected'
                                            ,'Test',objAcc.Id,  'http://test.com', 
                                            '2020-07-19T14:57:48+0400', '2020-07-19T14:57:48+0400','15','Refuse to pay', 
                                            '20', '1212','adm@testorg123.com', '9405883798',
                                            '9405883798','test', 'test', 
                                            'test', encryptedMob,'Test',
                                            'Test','Test');
        }
    }
    
    
    
    public static testMethod void CRMAmeyoCallUpdatetest2() {
        
        Profile p2 = [SELECT Id FROM Profile WHERE Name='System Administrator' Limit 1];
        //ID adminRoleId = [ Select id from userRole where name = 'Director'].id;
        UserRole userRoleObj = new UserRole(Name = 'Director', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        User u2 = new User(Alias = 'standt2', Email='stand2@testorg.com', userRoleId=userRoleObj.Id, isActive = true,
                           EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p2.Id,Extension = '1212',
                           TimeZoneSidKey='America/Los_Angeles', UserName='adm@testorg123.com');
        insert u2;
        System.runAs(u2) {
            List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                        OnOffCheck__c = true);
            
            settingLst2.add(newSetting1);
            insert settingLst2;
            
            NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
            insert objDealSR ;
            
            Account objAcc = TestDataFactory_CRM.createPersonAccount();
            insert objAcc ;
            
            List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objDealSR.Id, 1 );
            insert lstBooking ;
            
            List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking, 1 );
            insert lstBookingUnit ;
            
            //Id CollectionRecordTypeId =  Schema.getGlobalDescribe().get('Calling_List__c').getDescribe().getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
            
            List<Calling_List__c> lstCalling = TestDataFactory_CRM.createCallingList( 'Collections_Calling_List', 1 , lstBookingUnit[0] );
            lstCalling[0].IsHideFromUI__c  = false;
            lstCalling[0].Account__c = objAcc.Id;
            insert lstCalling ;
        
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();

            //req.requestURI = '/services/apexrest/callWrapup';
            //req.httpMethod = 'POST';
            //req.requestBody = Blob.valueof(JsonMsg);
            RestContext.request = req;
            RestContext.response= res;
   
            String encryptedMob = EncrytNumber.encryptNo('00919405883787');
   
            CRMCallUpdate.CRMAmeyoCallUpdate('Test','Calling List', lstCalling[0].Id, 
                                            'Test', 'Connected'
                                            ,'Test',objAcc.Id,  'http://test.com', 
                                            '2020-07-19T14:57:48+0400', '2020-07-19T14:57:48+0400','15','Refuse to pay', 
                                            '20', '1212','adm@testorg123.com', '9405883798',
                                            '9405883798','test', 'test', 
                                            'test', encryptedMob,'Test'
                                            ,'Test','Test');
        }
    }
}