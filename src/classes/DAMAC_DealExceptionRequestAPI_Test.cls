/************************************************************************************************
 * @Name              : DAMAC_DealExceptionRequestAPI_Test
 * @Description       : Test Class for DAMAC_DealExceptionRequestAPI 
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         26/06/2020       Created
***********************************************************************************************/

@isTest
public class DAMAC_DealExceptionRequestAPI_Test {

    @isTest
    static void testAPI(){
        DAMAC_Central_Push_Notifications__c credentials = new DAMAC_Central_Push_Notifications__c ();
        credentials.Email__c = 'test@test.com';
        credentials.Password__c = '12345';
        credentials.device_source__c = 'test';
        credentials.device_os_version__c = '1';
        credentials.app_version__c = '1';
        credentials.device_model__c = '1';
        credentials.app_id__c = 1;
        credentials.Api_Token__c = 'tstess';
        credentials.Project_connect_API_Token__c = 'test';
        credentials.API_URL__c  = 'https://test.com/';
        credentials.is_authorization_required__c = false;
        insert credentials;
        
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/dealexceptionrequest/';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_DealExceptionRequestAPI.doGet();
        String jsonBody = '{"requestId": "wrongid", "action":"approve", "comments":"test class"}';
        RestRequest request2 = new RestRequest();
        request2.requestBody = Blob.valueOf(jsonBody);
        RestResponse res2 = new RestResponse();
        request2.requestUri ='/services/apexrest/dealexceptionrequest/';
        request2.httpMethod = 'POST';
        RestContext.request = request2;
        RestContext.response = res2;
        DAMAC_DealExceptionRequestAPI.doPost();
        
        List<Checklist_Items__c> items = new List<Checklist_Items__c>();
        Checklist_Items__c item = new Checklist_Items__c();
        item.Type__c = 'Deal Exception Request';
        item.Value__c = 'Discount';
        item.Name = 'Test';
        item.Priority__c = 1;
        items.add(item);
        insert items;
               
        Location__c loc = InitializeSRDataTest.createLocation('123', 'Building');
        loc.property_id__c = '123';
        insert loc;

        Property__c newProperty = new Property__c();
        newProperty.Property_ID__c = 1;
        newProperty.Property_Code__c = 'VIR';
        newProperty.Property_Name__c = 'VIRIDIS @ AKOYA OXYGEN';
        newProperty.District__c = 'AL YUFRAH 2';
        newProperty.AR_Transaction_Type__c = 'INV VIR';
        newProperty.Penalty_Transaction_Type__c = 'DM-PENALTIES-VIR';
        newProperty.Brokerage_Distribution_Set__c = '11600';
        newProperty.Sales_Commission_Dist_Set__c = '11601';
        newProperty.Currency_Of_Sale__c = 'AED';
        newProperty.Signature_Col_Customer_Stmt__c = 'Front Line Investment Management Co. LLC';
        newProperty.EOI_Enabled__c = true;
        insert newProperty;

        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.Id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Unit_Location__c = loc.Id;
        lstInv[0].building_location__c = loc.Id;
        lstInv[0].property_id__c = newProperty.Id;
        insert lstInv;
        
        Payment_Plan__c pp = new Payment_Plan__c();
        pp.Effective_From__c = system.today();
        pp.Effective_To__c = system.today(); 
        pp.Building_Location__c =  loc.Id;
        pp.term_id__c = '1234';
        insert pp;
        Payment_Terms__c pt = new Payment_Terms__c();
        pt.Payment_Plan__c = pp.Id;
        pt.Percent_Value__c = '5';
        insert pt;

        List<Deal_Exception_Request__c> derList = new List<Deal_Exception_Request__c>();
        Deal_Exception_Request__c der = new Deal_Exception_Request__c();
        der.Status__c = 'Awaiting Management Approval';
        derList.add(der);

        
        Deal_Exception_Request__c der2 = new Deal_Exception_Request__c();
        der2.Status__c = 'Draft';
        derList.add(der2);
        insert derList;
        String derId = DealExceptionRequest_Remote.createDealExpRequest(lstInv[0].Id);
        Deal_Exception_Request__c der3 = [SELECT Id, Name FROM Deal_Exception_Request__c WHERE Id =: derId LIMIT 1];
        List<Deal_Exception_Unit__c> deuList = new List<Deal_Exception_Unit__c>();
        Deal_Exception_Unit__c unit = new Deal_Exception_Unit__c();
        unit.Deal_Exception_Request__c = der.Id;
        unit.Payment_Plan__c = pp.Id;
        unit.Proposed_Payment_Plan__c = pp.Id;
        unit.Inventory__c = lstInv[0].Id;
        unit.Area_SFT__c = 50;
        deuList.add(unit);
        Deal_Exception_Unit__c unit2 = new Deal_Exception_Unit__c();
        unit2.Deal_Exception_Request__c = der.Id;
        unit2.Payment_Plan__c = pp.Id;
        unit2.Proposed_Payment_Plan__c = pp.Id;
        unit2.Inventory__c = lstInv[0].Id;
        unit2.Area_SFT__c = 50;
        deuList.add(unit2);
        insert deuList;
        
        Approval.ProcessSubmitRequest approvalProcess2 = new Approval.ProcessSubmitRequest();
        approvalProcess2.setObjectId(der3.Id);
        approvalProcess2.setSubmitterId(UserInfo.getUserId());
        Approval.ProcessResult result6 = Approval.process(approvalProcess2);

        der3.status__c = 'Awaiting Management Approval';
        update der3;
        Test.startTest();
        RestRequest request15 = new RestRequest();
        RestResponse res15 = new RestResponse();
        request15.requestUri ='/services/apexrest/dealexceptionrequest/';
        request15.httpMethod = 'GET';
        RestContext.request = request15;
        RestContext.response = res15;
        DAMAC_DealExceptionRequestAPI.doGet();
        jsonBody = '{"requestId": "' + der3.Id + '", "action":"approve", "comments":"test class"}';
        
        RestRequest request14 = new RestRequest();
        RestResponse res14 = new RestResponse();
        request14.requestBody = Blob.valueOf(jsonBody);
        RestContext.request = request14;
        RestContext.response = res14;
        DAMAC_DealExceptionRequestAPI.doPost();
                
        Approval.ProcessSubmitRequest approvalProcess = new Approval.ProcessSubmitRequest();
        approvalProcess.setObjectId(der2.Id);
        approvalProcess.setSubmitterId(UserInfo.getUserId());
        Approval.ProcessResult result = Approval.process(approvalProcess);

        RestRequest request3 = new RestRequest();
        RestResponse res3 = new RestResponse();
        request3.requestUri ='/services/apexrest/dealexceptionrequest/';
        request3.httpMethod = 'GET';
        RestContext.request = request3;
        RestContext.response = res3;
        DAMAC_DealExceptionRequestAPI.doGet();

        
        RestRequest request5 = new RestRequest();
        RestResponse res5 = new RestResponse();
        request5.requestUri ='/services/apexrest/dealexceptionrequest/' +  der2.Id;
        request5.httpMethod = 'GET';
        RestContext.request = request5;
        RestContext.response = res5;
        DAMAC_DealExceptionRequestAPI.doGet();

        jsonBody = '{"requestId": "' + der2.Id + '", "action":"approve", "comments":"test class"}';
        request2.requestBody = Blob.valueOf(jsonBody);
        RestContext.request = request2;
        RestContext.response = res2;
        DAMAC_DealExceptionRequestAPI.doPost();

        jsonBody = '{"requestId": "' + der.Id + '", "action":"approve", "comments":"test class",'
         + '"checkListItems": [{"Priority__c": "1","Item__c": "Discount","Checklist_Value__c": "Yes"},'
         + ' { "Priority__c": "2", "Item__c": "Rebate", "Checklist_Value__c": "No"},'
         + '  {"Priority__c": "11","Item__c": "Others","Checklist_Value__c": "Test Comments"}]}';
        request2.requestBody = Blob.valueOf(jsonBody);
        RestContext.request = request2;
        RestContext.response = res2;
        DAMAC_DealExceptionRequestAPI.doPost();
        Test.stopTest();


        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
        req.setComments('Approving request.');
        req.setAction('Approve'); //This is the action that is approve in your case, you can set it to Reject also
        //Getting Work Item Id
        ProcessInstanceWorkitem pItem = [SELECT Id FROM ProcessInstanceWorkitem 
                                         WHERE ProcessInstance.TargetObjectId =: der2.id];
        req.setWorkitemId(pItem.Id);
        Approval.ProcessResult result2 = Approval.process(req);
        DAMAC_DealExceptionRequestAPI.doPost();
        
        RestRequest request4 = new RestRequest();
        RestResponse res4 = new RestResponse();
        request4.requestUri ='/services/apexrest/dealexceptionrequest/' +  der.Id;
        request4.httpMethod = 'GET';
        RestContext.request = request4;
        RestContext.response = res4;
        DAMAC_DealExceptionRequestAPI.doGet();

    }
}