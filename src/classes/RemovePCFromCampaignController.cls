public class RemovePCFromCampaignController {
    public Id campId;

    public RemovePCFromCampaignController(ApexPages.StandardController controller) {
        campId = controller.getId();
    }

    public PageReference  removePC(){
        delete [SELECT Id
                  FROM Assigned_PC__c
                 WHERE Campaign__c = :campId]; 
        PageReference pageRef = new PageReference('/' + campId);
        pageRef.setRedirect(true);
        return pageRef;
    }
}