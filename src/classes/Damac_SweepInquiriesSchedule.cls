global class Damac_SweepInquiriesSchedule implements Schedulable{
    global void execute(SchedulableContext ctx) {
        Database.executeBatch(new Damac_SweepInquiries (), 1);
    }

}