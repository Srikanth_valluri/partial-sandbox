@isTest
public class FM_PreLegalNoticeEmailBatchTest {

    @isTest
    public static void testEmail_UAE1() {
         //Create Customer record
        Id businessRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account objAcc = new Account(Name = 'Test Account',
                                    recordtypeid = businessRecTypeId,
                                    email__c = 'tt@tfdst.com',
                                    ZBEmailStatus__c = 'Valid',
                                    Agency_Email__c = 'test1@t.com',
                                    Alternate_Agency_Email__c = 'test2@t.com',
                                    Alternate_Email__c = 'test3@t.com',
                                    Email_1__c = 'test@t.com'
                                    );
        insert objAcc;
        
        //Create Property
        Property__c  objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        //Create Inventory 
        Inventory__c  objInventory = TestDataFactory_CRM.createInventory(objProp.Id);
        objInventory.Building_Name__c = 'Test building';
        insert objInventory;
        
        //Create Deal
        NSIBPM__Service_Request__c  objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR;
        
        //Create Bookings
        List<Booking__c> listBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id, 1);
        insert listBookings;
        
        //crearte booking unit
        List<Booking_Unit__c> listBUs =  TestDataFactory_CRM.createBookingUnits(listBookings, 1);
        listBUs[0].Inventory__c = objInventory.Id;
        listBUs[0].Registration_ID__c = 'reg11';
        listBUs[0].Property_Country__c = 'United Arab Emirates';
        listBUs[0].Email__c = '321test@test.com';
        listBUs[0].Building_Name__c = 'OHN';
        listBUs[0].Registration_Status_Code__c= 'MM';
        listBUs[0].Send_FM_Pre_Legal_Notice__c = true;
        listBUs[0].SC_From_RentalIncome__c = false;
        insert listBUs;
        system.debug('listBUs== ' + listBUs);
        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
        Test.StartTest();
        DataBase.executeBatch(new FM_PreLegalNoticeEmailBatch(), 1);
        Test.StopTest();
    }
}