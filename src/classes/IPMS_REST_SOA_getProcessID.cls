/*
Description:
This is the Initial Call out to get the SOA Process ID
On Success the Procecss URL is used to get the SOA,PROMOTION,DP INVOICE,OPTION URLs from IPMS via REST
Developed by : DAMAC IT TEAM
*/

public class IPMS_REST_SOA_getProcessID{
    
    public static string EndPointURL;
    
    public static string SourceSystem;
    
    public IPMS_REST_SOA_getProcessID(){
        // Get the End Point and Source System Information
        IPMS_Integration_Settings__mdt ipms_Rest = [select id,
                            Endpoint_URL__c,
                            Password__c,  
                            Username__c 
                            from IPMS_Integration_Settings__mdt 
                            where DeveloperName ='IPMS_REST' LIMIT 1];
                            
        EndPointURL = ipms_Rest.Endpoint_URL__c;
        SourceSystem = ipms_Rest.Username__c;
    }
    
    @future(callout=true)
    public static void DataForRequest(string bookingId){    
        if(bookingId != '' && bookingId != null){
            list<booking_Unit__c> bookingUnits = [select id,
                                            Registration_id__c 
                                            from Booking_unit__c 
                                            where booking__c=:bookingId and Registration_id__c !=null];
            if(bookingUnits.size()>0){
                prepareJsonBody(bookingUnits,bookingId);
            }
        }
    }
    
    // Prepare Request Body
    public static void prepareJsonBody(list<booking_unit__c> units, string bookingId ){ 
        
        IPMS_Integration_Settings__mdt ipms_Rest = [select id,
                            Endpoint_URL__c,
                            Password__c,  
                            Username__c 
                            from IPMS_Integration_Settings__mdt 
                            where DeveloperName ='IPMS_REST' LIMIT 1];
                            
        EndPointURL = ipms_Rest.Endpoint_URL__c;
        SourceSystem = ipms_Rest.Username__c;
        
        IPMS_JSON_SPA_Request bodyClass = new IPMS_JSON_SPA_Request();
        bodyClass.sourceSystem = SourceSystem;
        bodyClass.extRequestId = System.Now()+'-'+userinfo.getname();
        bodyClass.requestName = 'DP-SOA';
        bodyClass.extSystemObject = 'BOOKING';
        bodyClass.extSystemKey = bookingId;
        
        // Hold the Items in JSON
        list<IPMS_JSON_SPA_Request.RequestLines> items = new list<IPMS_JSON_SPA_Request.RequestLines>();
        
        for(booking_unit__c bu:units){
            IPMS_JSON_SPA_Request.RequestLines item = new IPMS_JSON_SPA_Request.RequestLines();            
            item.subRequestName = 'DP-SOA';
            item.extSystemObject = '';
            item.extSystemKey = bu.id;
            item.attribute1 = bu.registration_id__c;
            item.attribute2 = '';
            item.attribute3 = '';
            item.attribute4 = '';
            item.attribute5 = '';
            item.attribute6 = '';
            item.attribute7 = '';
            item.attribute8 = '';
            item.attribute9 = '';
            item.attribute10 = '';
            items.add(item);
        }
        bodyClass.requestLines = items;        
        String s = JSON.serializepretty(bodyClass);
        system.debug('>>>>REQUESTBODY>>>>'+s);
        POST_SOA_REQUEST(s,bookingId); // Do Callout For SOA 
    }
    
    
    // SOA Callout for Process Id
    public static void POST_SOA_REQUEST(string s,string bookingId){
    
        IPMS_Integration_Settings__mdt ipms_Rest = [select id,
                        Endpoint_URL__c,
                        Password__c,  
                        Username__c 
                        from IPMS_Integration_Settings__mdt 
                        where DeveloperName ='IPMS_REST' LIMIT 1];
                        
        EndPointURL = ipms_Rest.Endpoint_URL__c;
        SourceSystem = ipms_Rest.Username__c;
        
        try{
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(EndPointURL);
            req.setMethod('POST');
            req.setheader('Content-Type','application/json');
            req.setbody(s);
            req.setTimeout (120000);
            system.debug(req.getbody().length());        
            // Send the request, and return a response
            HttpResponse res = h.send(req);
            if(res.getStatusCode() != 201 ){
                system.debug('SUCCESS');
                createLog(bookingId,res.getBody());
            }else{
                system.debug('FAIL');
                system.debug(res.getBody());
                string response = res.getBody();//.substringAfter('=');
                SaveSOAResponse(response,bookingId);  
            }
        }catch(exception e){
            system.debug('CALLOUT EXCEPTION');
            createLog(bookingId,string.valueof(e.getmessage()));
        }
    }
    
    // Save Process Id on Booking and Call the Queue job to Poll for SOA    
    public static void SaveSOAResponse(string processURL,string bookingId){
        booking__c bookingRec = [select id,SOA_Process_Request__c from booking__c where id=:bookingId LIMIT 1];        
        bookingRec.Poll_for_SOA__c = true;
        bookingRec.SOA_Process_Request__c = processURL;
        try{
            update bookingRec;        
            // Call Queue Job to POLL for SOA
            if(processURL != ''){
                system.debug('calling second job');
                system.enqueueJob(new Async_IPMS_Rest_SOA(processURL,bookingId)); 
            }else{
                createLog(bookingId,'No Process URL Found on the Booking');
            }
        }catch(exception e){
            createLog(bookingId,string.valueof(e.getmessage()));
        }       
    }
    
     //Create Log for Error
    public static void createLog(string parentId,String ErrorMessage){
        log__c log = new log__c();
        log.Booking__c = parentId;
        log.Description__c = ErrorMessage;
        insert log;
    }
   
}