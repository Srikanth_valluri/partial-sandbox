/*---------------------------------------------------------------------------------------------------------------------------------------------------
Description: Class to send escalation emails
=====================================================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By      | Comments
-----------------------------------------------------------------------------------------------------------------------------------------------------
1.1     | 13-10-2020        | Aishwarya Todkar      | Added logic for Complaint case
1.1     | 07-12-2020        | Aishwarya Todkar      | Added Draft status check
=====================================================================================================================================================*/
public class EscalateSocialMediaBatch implements Database.Batchable<sObject>
                                                , Database.AllowsCallouts
                                                , Database.Stateful {
    
    public static Id caseRecTypeId, complaintRecTypeId;
    public Map<Id,User> mapOfIdToUser;
    
    public EscalateSocialMediaBatch(){
                                            
        mapOfIdToUser = new Map<Id,User>();      
        List<User> lstActiveUser = [ SELECT
                                        Id
                                        , Name
                                        , Email
                                        , UserType
                                        , UserRole.Name
                                        , UserRoleID
                                        , FirstName
                                        , LastName
                                        , ManagerId
                                        , Manager.FirstName
                                        , Manager.ManagerId
                                        , Manager.LastName
                                    FROM
                                        User 
                                    WHERE
                                        IsPortalEnabled = false 
                                    AND 
                                        isActive = true];
          
        for( User thisUser : lstActiveUser ) {
            mapOfIdToUser.put( thisUser.Id, thisUser );
        }                     
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){

        caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().
                                            get('SocialMedia').getRecordTypeId();
        complaintRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Complaint').getRecordTypeId();
        return Database.getQueryLocator([ SELECT 
                                            Id
                                            , AccountId
                                            , OwnerId
                                            , Status
                                            , Message__c
                                            , CaseNumber
                                            , CC_Escl_Date__c
                                            , Mgr_Escl_Date__c
                                            , HOD_Escl_Date__c
                                            , Committee_Escl_Date__c
                                            , CRE_Escl_Date__c
                                            , CRE_Next_Escl_Date__c
                                            , Creared_Next_Escl_Date__c
                                            , CC_Next_Escl_Date__c
                                            , Mgr_Next_Escl_Date__c
                                            , HOD_Next_Escl_Date__c
                                            , RecordType.Name
                                            , Complaint_Source__c
                                        FROM 
                                            Case 
                                        WHERE
                                            Status != 'Closed'
                                        AND
                                            Status != 'Draft Request'
                                        AND 
                                            ( ( RecordTypeId  =:caseRecTypeId 
                                                AND ( Creared_Next_Escl_Date__c = today 
                                                    OR CC_Next_Escl_Date__c = today 
                                                    OR CRE_Next_Escl_Date__c = today 
                                                    OR Mgr_Next_Escl_Date__c = today 
                                                    OR HOD_Next_Escl_Date__c = today ) ) 
                                            OR ( RecordTypeId =: complaintRecTypeId 
                                                AND ( OwnerId =:  Label.Happiness_Queue_Id 
                                                    OR OwnerId =: Label.Audit_Team_Queue_Id ) 
                                                ) 
                                            )
                                        ]); 
    }
    
    public void execute(Database.BatchableContext BC, List<Case> scope){        
        List<EmailMessage> lstEmails = new List<EmailMessage>();
        List<Case> listCasesToUpdate = new List<Case>();
        for(Case objCase : scope) {
            
            if( objCase.RecordType.Name.equalsIgnoreCase( 'Complaint' )) {
                EmailMessage objEmail = sendEmail( objCase, Null, '' );
                if( objEmail != null ) { 
                    lstEmails.add( objEmail );
                }
            }
            else if( objCase.Creared_Next_Escl_Date__c == system.Today() && !String.valueOF(objCase.OwnerId).StartsWith('005') ) {
                objCase.CC_Escl_Date__c = System.Now();
                EmailMessage objEmail = sendEmail( objCase, Null, Label.Social_Media_Escl_CCMail);
                listCasesToUpdate.add( objCase );
                if( objEmail != null ) { 
                    lstEmails.add( objEmail );
                }
            }
            else if( objCase.CRE_Next_Escl_Date__c == system.Today() ) {
                user objUser = mapOfIdToUser.get(objCase.OwnerId);
                objCase.Mgr_Escl_Date__c = System.Now();
                EmailMessage objEmail = sendEmail(  objCase,  mapOfIdToUser.get(objUser.ManagerId), '' );
                listCasesToUpdate.add( objCase );
                if( objEmail != null ) { 
                    lstEmails.add( objEmail );
                }
            }
            else if( objCase.Mgr_Next_Escl_Date__c == system.Today()   ) {
                objCase.HOD_Escl_Date__c =System.Now();
                EmailMessage objEmail = sendEmail(  objCase,  mapOfIdToUser.get( Label.HOD_Id_for_Social_Escl), '' );
                listCasesToUpdate.add( objCase );
                if( objEmail != null ) { 
                    lstEmails.add( objEmail );
                }
            }
            else if( objCase.HOD_Next_Escl_Date__c == system.Today() ) {
                objCase.Committee_Escl_Date__c =System.Now();
                if(Label.Committee_Id_For_Social_Escl.substringBefore( '-' ).equalsIgnoreCase( 'ON' ) ) {
                    EmailMessage objEmail =sendEmail(  objCase,  mapOfIdToUser.get( Label.Committee_Id_For_Social_Escl.substringAfter( '-' ) ), '' );
                    if( objEmail != null ) { 
                        lstEmails.add( objEmail );
                    }
                }
                listCasesToUpdate.add( objCase );
                
            }
        }
        if( !listCasesToUpdate.isEmpty() ) {
            update listCasesToUpdate;
        }
        if( !lstEmails.isEmpty()){
            insert lstEmails;
        }
    }
    
    public void finish(Database.BatchableContext BC){
    }
    
    public static EmailMessage sendEmail( Case objCase, User ObjUser, String strEmail ) {

        String emailTemplateName = '', metaDataName = '', processType = '';

        if( objCase.RecordTypeId == caseRecTypeId ) {
            metaDataName = 'Social_Media_Escalation';
            processType = 'Social Media';
        }
        if( objCase.RecordType.Name.equalsIgnoreCase( 'Complaint' ) ) {
            
            metaDataName = objCase.OwnerId == Label.Happiness_Queue_Id ? 'Non-Surveys Complaint Emails' 
                                : objCase.OwnerId == Label.Audit_Team_Queue_Id &&  objCase.Complaint_Source__c.equalsIgnoreCase( 'Surveys' ) 
                                ? 'Surveys Complaint Emails' :'';
            processType = 'Complaint';
        }

        System.debug( 'metaDataName==' + metaDataName );

        if( String.isNotBlank( metaDataName ) ) {
            List<SendGrid_Email_Details__mdt> listSendGridDetails = new List<SendGrid_Email_Details__mdt>();
            listSendGridDetails = CRM_SurveyEmailsSender.getSendGridDetails( metaDataName );

            if( listSendGridDetails != null &&  !listSendGridDetails.isEmpty() ) {
                SendGrid_Email_Details__mdt objSendGridCs = listSendGridDetails[0];
                emailTemplateName = objSendGridCs.Email_Template__c;
                List<EmailMessage> lstEmails = new List<EmailMessage>();
                GenericUtility.SendGridWrapper sgWrap = new GenericUtility.SendGridWrapper();
                sgWrap = GenericUtility.prepareSendGridEmail( objSendGridCs );
                
                if( String.isNotBlank( emailTemplateName ) ) {
                    
                    EmailTemplate emailTemplateObj = [SELECT Id
                                                            , Name
                                                            , Subject
                                                            , Body
                                                            , HtmlValue
                                                            , TemplateType
                                                            , BrandTemplateId
                                                        FROM 
                                                            EmailTemplate 
                                                        WHERE 
                                                            Name =: emailTemplateName 
                                                        LIMIT 1 ];
                                                            
                    Attachment[] attachmentLst = new List<Attachment>( [ SELECT 
                                                                            Id
                                                                            , ContentType
                                                                            , Name
                                                                            , Body  
                                                                        From 
                                                                            Attachment 
                                                                        WHERE 
                                                                            parentId = : emailTemplateObj.Id ] );

                    String toAddress = '', contentValue = '', contentBody = '', subject = '';
                    
                    toAddress = ( ObjUser != NULL && String.isNotBlank( ObjUser.Email ) ) ? ObjUser.Email
                                : String.isNotBlank( strEmail) ?  strEmail : '';
                    
                    if( emailTemplateObj != null ) {
                        
                        contentBody =  emailTemplateObj.body;
                        contentValue = emailTemplateObj.htmlValue;
                        subject = emailTemplateObj.Subject;
                        String userName = (ObjUser != NULL && String.isNotBlank(ObjUser.FirstName) && String.isNotBlank(ObjUser.LastName))?
                                            ObjUser.FirstName+' '+ObjUser.LastName:
                                            String.isNotBlank(strEmail) ?  'Team' : '';
                        contentValue = String.isNotBlank( userName ) ? contentValue.replace( '{!userName}', userName ): contentValue.replace( '{!userName}', '' );
                        contentBody = String.isNotBlank( userName ) ? contentBody.replace( '{!userName}', userName ) : contentBody.replace( '{!userName}', '' );
                        
                        sgWrap.toAddress =  String.isNotBlank( toAddress ) ? toAddress + ',' + sgWrap.toAddress : sgWrap.toAddress;
                        sgWrap.contentValue = MergeFieldReplacer.replaceMergeFields( contentValue, 'Generic', objCase.Id ) ;
                        sgWrap.contentBody = MergeFieldReplacer.replaceMergeFields( contentBody, 'Generic', objCase.Id );
                        sgWrap.subject = MergeFieldReplacer.replaceMergeFields( subject, 'Generic', objCase.Id );            
                        sgWrap.listAttachment = attachmentLst;
                        sgWrap.relatedToId = objCase.Id;
                        sgWrap.caseId = objCase.Id;
                        sgWrap.AccountId = objCase.AccountId;
                        sgWrap.processType = processType;

                        System.debug('sgWrap -- ' + sgWrap);
                        //Call generic method to send email via sendGrid
                        if( String.isNotBlank( sgWrap.toAddress ) ) {
                            lstEmails = GenericUtility.sendEmailsBySendGrid( sgWrap );
                            if( !lstEmails.isEmpty() ) {
                                return lstEmails[0];
                            }
                        }
                    }//End emailTemplateObj IF
                }//End emailTemplateName if
            }// End listSendGridDetails if
        }// End metaDataName if
        return null;
    }
}