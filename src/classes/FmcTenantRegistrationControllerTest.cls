@isTest
private class FmcTenantRegistrationControllerTest {

    @testSetup
    public static void testData() {
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
    }

    @isTest
    public static void saveDraftTest() {
        Account customerAccount = TestDataFactoryFM.createAccount();
        insert customerAccount;
        CustomerCommunityUtils.customerAccountId = customerAccount.Id;

        System.debug('customerAccount = ' + JSON.serializePretty(customerAccount));

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(customerAccount);
        insert sr;
        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;
        
        Booking__c  booking = TestDataFactoryFM.createBooking(customerAccount, sr);
        insert booking;

        System.debug('booking = ' + JSON.serializePretty(booking));

        Booking_Unit__c bookingUnit = TestDataFactoryFM.createBookingUnit(customerAccount, booking);
        insert bookingUnit;

        TestDataFactoryFM.insertActiveUnitStatuses();

        System.debug('bookingUnit = ' + JSON.serializePretty(bookingUnit));

        PageReference currentPage = Page.CommunityPortal;
        currentPage.getParameters().put('view', 'TenantRegistration');

        Test.startTest();

            Test.setCurrentPage(currentPage);
            ApexPages.currentPage().getParameters().put('AccountId',customerAccount.Id);
            ApexPages.currentPage().getParameters().put('UnitId',bookingUnit.Id);
            ApexPages.currentPage().getParameters().put('SRType','Tenant_Registration');

            FmcTenantRegistrationController controller = new FmcTenantRegistrationController();
            controller.strSelectedUnit = bookingUnit.Id;
            controller.selectUnit();
            controller.objFMCase.Issue_Date__c=Date.today();
            controller.objFMCase.Contact_person__c='test';
            controller.objFMCase.Description__c='test';
            controller.objFMCase.Contact_person_contractor__c='test';
            controller.objFMCase.Last_Name__c ='test';
            controller.objFMCase.Nationality__c ='Pakistani';
            controller.objFMCase.Passport_Number__c ='A1B2C3D4E';
            controller.objFMCase.Origin__c = 'Portal';
            controller.objFMCase.Mobile_no__c = '5648973217';
            controller.objFMCase.Mobile_Country_Code__c = 'India: 0091';
            controller.strOrigin = 'Portal';
            controller.insertCase();
            //obj.objFMCase=fmCaseObj;
            controller.initializeFMCaseAddDetails();
            controller.saveDraft();
            controller.objFMCase.Tenant_Email__c = 'test@mail.com';
            controller.enteredEmailCode = controller.actualCodes;
            //controller.verifyCodes();

            Id personAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName()
                                    .get('Person Account').getRecordTypeId();
            insert new Account(
                RecordTypeID = personAccountId,
                PersonEmail='a@g.com',
                FirstName = 'Test FName',
                LastName = 'Test LName',
                Passport_Number__pc='A1B2C3D4E',
                Nationality__pc = 'Pakistani',
                Mobile__c='1212121212',
                Address_Line_1__c='test'
            );
            controller.saveDraft();
            controller.objFMCase.Tenant_Email__c = 'test@mail.com';
            controller.enteredEmailCode = controller.actualCodes;
            //controller.verifyCodes();
            //new FmcTenantRegistrationController();

            Database.executebatch(new FmcTenantRegistrationAccountBatch());

        Test.stopTest();
    }

    @isTest
    public static void submitSrTest() {
        Id profileId = [SELECT id from profile where name = 'System Administrator'].id;
        User u=TestDataFactoryFM.createUser(profileId);
        insert u;

        Account customerAccount=TestDataFactoryFM.createAccount();
        insert customerAccount;
        CustomerCommunityUtils.customerAccountId = customerAccount.Id;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(customerAccount);
        insert sr;

        Booking__c  booking = TestDataFactoryFM.createBooking(customerAccount,sr);
        insert booking;

        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;

        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c bookingUnit=TestDataFactoryFM.createBookingUnit(customerAccount,booking);
        insert bookingUnit;

        TestDataFactoryFM.insertActiveUnitStatuses();

        /*FM_User__c fmUser=TestDataFactoryFM.createFMUser(u,locObj);
        fmUser.FM_Role__c='FM Manager';
        insert fmUser;*/

        Test.setMock( HttpCalloutMock.class, new FmHttpCalloutMock(
            200, 'Accepted', 'Accepted', new Map<String, String> {'X-Message-Id' => '1234567890'}
        ));

        PageReference currentPage = Page.CommunityPortal;
        currentPage.getParameters().put('view', 'TenantRegistration');

        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Last_Name__c = 'test';
        fmCaseObj.Request_Type_DeveloperName__c='Tenant_Registration';
        fmCaseObj.Booking_Unit__c=bookingUnit.id;
        fmCaseObj.Account__c=customerAccount.id;
        //fmCaseObj.Approving_Authorities__c='FM Manager';
        fmCaseObj.Nationality__c ='Pakistani';
        fmCaseObj.Passport_Number__c ='A1B2C3D4E';
        fmCaseObj.Status__c ='New';
        fmCaseObj.Mobile_no__c = '5648973217';
              fmCaseObj.Mobile_Country_Code__c = 'India: 0091';
        insert fmCaseObj;

        FM_Additional_Detail__c  instanceEMC = new FM_Additional_Detail__c();
        instanceEMC.Emergency_Contact_Case__c = fmCaseObj.id;
        instanceEMC.RecordTypeId = Schema.SObjectType.FM_Additional_Detail__c
                                .getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
        instanceEMC.Name__c='Test';
        instanceEMC.Relationship__c='Test';
        instanceEMC.Phone__c='121212';
        instanceEMC.Email__c='test@test.com';
        insert instanceEMC;

        FM_Additional_Detail__c  instanceRC=new FM_Additional_Detail__c();
        instanceRC.Resident_Case__c =fmCaseObj.id;
        instanceRC.Name__c='Test';
        instanceRC.Phone__c='121212';
        instanceRC.Email__c='test@test.com';
        instanceRC.Age__c=12;
        instanceRC.Nationality__c='Pakistani';
        instanceRC.Passport_Number__c='121212';
        instanceRC.RecordTypeId = Schema.SObjectType.FM_Additional_Detail__c
                                    .getRecordTypeInfosByName().get('Resident').getRecordTypeId();
        insert instanceRC;

        FM_Additional_Detail__c  instanceVC=new FM_Additional_Detail__c();
        instanceVC.Vehicle_Case__c =fmCaseObj.id;
        instanceVC.Vehicle_Make_Model__c='test';
        instanceVC.Vehicle_Colour__c='test';
        instanceVC.Parking_Slot_Number__c='1';
        instanceVC.Vehicle_Number__c='1';
        instanceVC.Identification_Number__c='1';
        instanceVC.RecordTypeId = Schema.SObjectType.FM_Additional_Detail__c
                                    .getRecordTypeInfosByName().get('Vehicle').getRecordTypeId();
        insert instanceVC;

        Test.setCurrentPage(currentPage);
        //ApexPages.currentPage().getParameters().put('AccountId',customerAccount.Id);
        //ApexPages.currentPage().getParameters().put('UnitId',bookingUnit.Id);
        //ApexPages.currentPage().getParameters().put('SRType','Tenant_Registration');
        ApexPages.currentPage().getParameters().put('Id',fmCaseObj.id);

        FmcTenantRegistrationController controller = new FmcTenantRegistrationController();
        controller.strSelectedUnit = bookingUnit.Id;
        controller.selectUnit();

        //obj.objFMCase.id=fmCaseObj.id;
        //obj.initializeFMCaseAddDetails();
        Test.startTest();
        controller.strDetailType='Emergency Contact';
        controller.addDetails();
        controller.indexOfNewChildToRemove=0;
        controller.removeDetails();

        Id personAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName()
                                .get('Person Account').getRecordTypeId();
        insert new Account(
            RecordTypeID = personAccountId,
            PersonEmail='a@g.com',
            FirstName = 'Test FName',
            LastName = 'Test LName',
            Passport_Number__pc='A1B2C3D4E',
            Nationality__pc = 'Pakistani',
            Mobile__c='1212121212',
            Address_Line_1__c='test'
        );

        controller.objFMCase.Tenant_Email__c = 'test@mail.com';
        controller.sendVerificationCodes();
        controller.enteredEmailCode = controller.actualCodes;
        controller.verifyCodes();

        controller.objFMCase = fmCaseObj;
        controller.strAccountId = customerAccount.id;
        controller.submitSr();
        //obj.returnBackToCasePage();
        Database.executebatch(new FmcTenantRegistrationAccountBatch());
        Test.stopTest();
    }

    @isTest
    public static void uploadDocumentTest() {
        Account customerAccount=TestDataFactoryFM.createAccount();
        insert customerAccount;
        CustomerCommunityUtils.customerAccountId = customerAccount.Id;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(customerAccount);
        insert sr;

        Booking__c  booking = TestDataFactoryFM.createBooking(customerAccount,sr);
        insert booking;

        Booking_Unit__c bookingUnit=TestDataFactoryFM.createBookingUnit(customerAccount,booking);
        insert bookingUnit;

        TestDataFactoryFM.insertActiveUnitStatuses();

        PageReference currentPage = Page.CommunityPortal;
        currentPage.getParameters().put('view', 'TenantRegistration');

        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Tenant_Registration';

        //insert fmCaseObj;

        Test.setCurrentPage(currentPage);
        ApexPages.currentPage().getParameters().put('AccountId',customerAccount.Id);
        ApexPages.currentPage().getParameters().put('UnitId',bookingUnit.Id);
        ApexPages.currentPage().getParameters().put('SRType','Tenant_Registration');

        //Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive2());

        FmcTenantRegistrationController controller = new FmcTenantRegistrationController();
        controller.strSelectedUnit = bookingUnit.Id;
        controller.selectUnit();
        controller.insertCase();
        controller.strDocumentBody='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        controller.strDocumentName='Test_Document_Work_Permit.htm';
        //obj.createCaseShowUploadDoc();
        controller.initializeFMCaseAddDetails();
        test.startTest();
        controller.uploadDocument();
        Database.executebatch(new FmcTenantRegistrationAccountBatch());
        test.stopTest();
    }

    @isTest
    private static void guestTenantRegistrationTest() {
        User guestUser = [SELECT Id FROM User WHERE Name = :FmcUtils.GUEST_USER_NAME];

        Account customerAccount = TestDataFactoryFM.createAccount();
        insert customerAccount;
        CustomerCommunityUtils.customerAccountId = customerAccount.Id;

        System.debug('customerAccount = ' + JSON.serializePretty(customerAccount));
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(customerAccount);
        insert sr;

        Booking__c  booking = TestDataFactoryFM.createBooking(customerAccount, sr);
        insert booking;

        System.debug('booking = ' + JSON.serializePretty(booking));

        Booking_Unit__c bookingUnit = TestDataFactoryFM.createBookingUnit(customerAccount, booking);
        insert bookingUnit;

        TestDataFactoryFM.insertActiveUnitStatuses();

        System.debug('bookingUnit = ' + JSON.serializePretty(bookingUnit));

        PageReference currentPage = Page.CommunityPortal;
        currentPage.getParameters().put('view', 'TenantRegistration');

        Test.setMock( HttpCalloutMock.class, new FmHttpCalloutMock(
            200, 'Accepted', 'Accepted', new Map<String, String> {'X-Message-Id' => '1234567890'}
        ));

        System.runAs(guestUser) {
            Test.startTest();

            Test.setCurrentPage(currentPage);
            ApexPages.currentPage().getParameters().put('AccountId',customerAccount.Id);
            ApexPages.currentPage().getParameters().put('unitId',bookingUnit.Id);
            ApexPages.currentPage().getParameters().put('SRType','Tenant_Registration');

            FmcTenantRegistrationController controller = new FmcTenantRegistrationController();
            controller.init();
            controller.objFMCase.Issue_Date__c=Date.today();
            controller.objFMCase.Contact_person__c='test';
            controller.objFMCase.Description__c='test';
            controller.objFMCase.Contact_person_contractor__c='test';
            controller.objFMCase.Nationality__c ='Pakistani';
            controller.objFMCase.Last_Name__c = 'test';
            controller.objFMCase.Origin__c = 'Portal - Guest';
            controller.objFMCase.Mobile_no__c = '5648973217';
            controller.objFMCase.Mobile_Country_Code__c = 'India: 0091';
            controller.strOrigin = 'Portal - Guest';
            controller.insertCase();
            controller.initializeFMCaseAddDetails();
            controller.saveDraft();

            /*FM_Additional_Detail__c  instanceEMC=new FM_Additional_Detail__c();
            instanceEMC.Emergency_Contact_Case__c=controller.objFMCase.id;
            instanceEMC.RecordTypeId=Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
            instanceEMC.Name__c='Test';
            instanceEMC.Relationship__c='Test';
            instanceEMC.Phone__c='121212';
            instanceEMC.Email__c='test@test.com';
            insert instanceEMC;

            FM_Additional_Detail__c  instanceRC=new FM_Additional_Detail__c();
            instanceRC.Resident_Case__c =controller.objFMCase.id;
            instanceRC.Name__c='Test';
            instanceRC.Phone__c='121212';
            instanceRC.Email__c='test@test.com';
            instanceRC.Age__c=12;
            instanceRC.Nationality__c='Pakistani';
            instanceRC.Passport_Number__c='121212';
            instanceRC.RecordTypeId = Schema.SObjectType.FM_Additional_Detail__c
                                        .getRecordTypeInfosByName().get('Resident').getRecordTypeId();
            insert instanceRC;

            FM_Additional_Detail__c  instanceVC=new FM_Additional_Detail__c();
            instanceVC.Vehicle_Case__c =controller.objFMCase.id;
            instanceVC.Vehicle_Make_Model__c='test';
            instanceVC.Vehicle_Colour__c='test';
            instanceVC.Parking_Slot_Number__c='1';
            instanceVC.Vehicle_Number__c='1';
            instanceVC.Identification_Number__c='1';
            instanceVC.RecordTypeId = Schema.SObjectType.FM_Additional_Detail__c
                                        .getRecordTypeInfosByName().get('Vehicle').getRecordTypeId();
            insert instanceVC;*/

            controller.saveDraft();
            controller.objFMCase.Tenant_Email__c = 'test@mail.com';
            controller.sendVerificationCodes();
            controller.enteredEmailCode = controller.actualCodes;
            controller.verifyCodes();
            Database.executebatch(new FmcTenantRegistrationAccountBatch());
            System.schedule('Tenant Account Creation Job ' + Datetime.now().getTime(),
                                        '0 00 * * * ?', new FmcTenantRegistrationAccountBatch());

            Test.stopTest();

           /* update new FM_Case__c(
                Id = controller.objFMCase.Id,
                Status__c = 'Awaiting Correction'
            );*/
            ApexPages.currentPage().getParameters().put('id',controller.objFMCase.Id);
            FmcTenantRegistrationController correctionController = new FmcTenantRegistrationController();

        }
    }

    public static testmethod void recallFuncBatchTest(){
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Nationality__c ='Pakistani';
        fmCaseObj.FM_Manager_Email__c = 'a@b.com';
        fmCaseObj.FM_Manager_Email__c = '';
        insert fmCaseObj;
        Approval.ProcessSubmitRequest objApprovalSubmitRequest = new Approval.ProcessSubmitRequest();
        objApprovalSubmitRequest.setComments( 'Submit' );
        objApprovalSubmitRequest.setObjectId( fmCaseObj.id );
        objApprovalSubmitRequest.setProcessDefinitionNameOrId( 'FM_Approval_Process' );
        objApprovalSubmitRequest.setSkipEntryCriteria( true );
        objApprovalSubmitRequest.setNextApproverIds( new Id[] { UserInfo.getUserId() } );
        Approval.ProcessResult[] lstApprovalResult = Approval.process(new List<Approval.ProcessSubmitRequest>{objApprovalSubmitRequest});
        FmcTenantRegistrationAccountBatch.recallApprovals(new List<FM_case__c>{fmCaseObj});
    }

    @isTest
    private static void emailVerificationTest() {
        Account customerAccount = TestDataFactoryFM.createAccount();
        insert customerAccount;
        CustomerCommunityUtils.customerAccountId = customerAccount.Id;

        System.debug('customerAccount = ' + JSON.serializePretty(customerAccount));

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(customerAccount);
        insert sr;

        Booking__c  booking = TestDataFactoryFM.createBooking(customerAccount, sr);
        insert booking;

        System.debug('booking = ' + JSON.serializePretty(booking));

        Booking_Unit__c bookingUnit = TestDataFactoryFM.createBookingUnit(customerAccount, booking);
        insert bookingUnit;

        TestDataFactoryFM.insertActiveUnitStatuses();

        System.debug('bookingUnit = ' + JSON.serializePretty(bookingUnit));

        PageReference currentPage = Page.CommunityPortal;
        currentPage.getParameters().put('view', 'TenantRegistration');

        Test.setCurrentPage(currentPage);
        ApexPages.currentPage().getParameters().put('AccountId',customerAccount.Id);
        ApexPages.currentPage().getParameters().put('UnitId',bookingUnit.Id);
        ApexPages.currentPage().getParameters().put('SRType','Tenant_Registration');

        FmcTenantRegistrationController controller = new FmcTenantRegistrationController();
        controller.strSelectedUnit = bookingUnit.Id;
        controller.selectUnit();
        controller.objFMCase.Issue_Date__c=Date.today();
        controller.objFMCase.Contact_person__c='test';
        controller.objFMCase.Description__c='test';
        controller.objFMCase.Contact_person_contractor__c='test';
        controller.objFMCase.Last_Name__c ='test';
        controller.objFMCase.Nationality__c ='Pakistani';
        controller.objFMCase.Passport_Number__c ='A1B2C3D4E';
        controller.objFMCase.Origin__c = 'Portal';
        controller.objFMCase.Mobile_no__c = '5648973217';
        controller.objFMCase.Mobile_Country_Code__c = 'India: 0091';
        controller.objFMCase.First_Name__c = 'John';
        controller.objFMCase.Last_Name__c = 'Doe';
        controller.objFMCase.Tenant_Email__c = 'test@mail.com';
        controller.strOrigin = 'Portal';
        controller.insertCase();

        Test.setMock( HttpCalloutMock.class, new FmHttpCalloutMock(
            200, 'Accepted', 'Accepted', new Map<String, String> {'X-Message-Id' => '1234567890'}
        ));

        Test.startTest();
            controller.emailVerificationCode('1234');
        Test.stopTest();

    }

    @isTest
    private static void guestTenantRegistrationTest2() {
        User guestUser = [SELECT Id FROM User WHERE Name = :FmcUtils.GUEST_USER_NAME];

        Account customerAccount = TestDataFactoryFM.createAccount();
        insert customerAccount;
        CustomerCommunityUtils.customerAccountId = customerAccount.Id;

        System.debug('customerAccount = ' + JSON.serializePretty(customerAccount));

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(customerAccount);
        insert sr;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;

        Booking__c  booking = TestDataFactoryFM.createBooking(customerAccount, sr);
        insert booking;

        System.debug('booking = ' + JSON.serializePretty(booking));

        Booking_Unit__c bookingUnit = TestDataFactoryFM.createBookingUnit(customerAccount, booking);
        insert bookingUnit;

        TestDataFactoryFM.insertActiveUnitStatuses();

        FM_Case__c objCase = new FM_Case__c();
        objCase.CreatedById = guestUser.id;
        objCase.Booking_Unit__c = bookingUnit.id;
        objCase.Origin__c = 'Portal - Guest';
        objCase.Request_Type__c = 'Tenant Registration';
        objCase.Status__c = 'Submitted';
        insert objCase;

        System.debug('bookingUnit = ' + JSON.serializePretty(bookingUnit));

        PageReference currentPage = Page.CommunityPortal;
        currentPage.getParameters().put('view', 'TenantRegistration');

        Test.setMock( HttpCalloutMock.class, new FmHttpCalloutMock(
            200, 'Accepted', 'Accepted', new Map<String, String> {'X-Message-Id' => '1234567890'}
        ));

        System.runAs(guestUser) {
            Test.startTest();

            Test.setCurrentPage(currentPage);
            ApexPages.currentPage().getParameters().put('AccountId',customerAccount.Id);
            ApexPages.currentPage().getParameters().put('unitId',bookingUnit.Id);
            ApexPages.currentPage().getParameters().put('SRType','Tenant_Registration');

            FmcTenantRegistrationController controller = new FmcTenantRegistrationController();
            controller.init();
            controller.objFMCase.Issue_Date__c=Date.today();
            controller.objFMCase.Contact_person__c='test';
            controller.objFMCase.Description__c='test';
            controller.objFMCase.Contact_person_contractor__c='test';
            controller.objFMCase.Nationality__c ='Pakistani';
            controller.objFMCase.Last_Name__c = 'test';
            controller.objFMCase.Origin__c = 'Portal';
            controller.objFMCase.Mobile_no__c = '5648973217';
            controller.objFMCase.Mobile_Country_Code__c = 'India: 0091';
            controller.strOrigin = 'Portal';
            controller.insertCase();
            controller.initializeFMCaseAddDetails();
            controller.saveDraft();

            /*FM_Additional_Detail__c  instanceEMC=new FM_Additional_Detail__c();
            instanceEMC.Emergency_Contact_Case__c=controller.objFMCase.id;
            instanceEMC.RecordTypeId=Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
            instanceEMC.Name__c='Test';
            instanceEMC.Relationship__c='Test';
            instanceEMC.Phone__c='121212';
            instanceEMC.Email__c='test@test.com';
            insert instanceEMC;

            FM_Additional_Detail__c  instanceRC=new FM_Additional_Detail__c();
            instanceRC.Resident_Case__c =controller.objFMCase.id;
            instanceRC.Name__c='Test';
            instanceRC.Phone__c='121212';
            instanceRC.Email__c='test@test.com';
            instanceRC.Age__c=12;
            instanceRC.Nationality__c='Pakistani';
            instanceRC.Passport_Number__c='121212';
            instanceRC.RecordTypeId = Schema.SObjectType.FM_Additional_Detail__c
                                        .getRecordTypeInfosByName().get('Resident').getRecordTypeId();
            insert instanceRC;

            FM_Additional_Detail__c  instanceVC=new FM_Additional_Detail__c();
            instanceVC.Vehicle_Case__c =controller.objFMCase.id;
            instanceVC.Vehicle_Make_Model__c='test';
            instanceVC.Vehicle_Colour__c='test';
            instanceVC.Parking_Slot_Number__c='1';
            instanceVC.Vehicle_Number__c='1';
            instanceVC.Identification_Number__c='1';
            instanceVC.RecordTypeId = Schema.SObjectType.FM_Additional_Detail__c
                                        .getRecordTypeInfosByName().get('Vehicle').getRecordTypeId();
            insert instanceVC;*/

            controller.saveDraft();
            controller.objFMCase.Tenant_Email__c = 'test@mail.com';
            controller.sendVerificationCodes();
            controller.enteredEmailCode = controller.actualCodes;
            controller.verifyCodes();
            Database.executebatch(new FmcTenantRegistrationAccountBatch());
            System.schedule('Tenant Account Creation Job ' + Datetime.now().getTime(),
                                        '0 00 * * * ?', new FmcTenantRegistrationAccountBatch());

            Test.stopTest();
            ApexPages.currentPage().getParameters().put('id',controller.objFMCase.Id);
            FmcTenantRegistrationController correctionController = new FmcTenantRegistrationController();
        }
    }
}