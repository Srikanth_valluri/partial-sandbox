public interface WebServiceAdapter {
    WebServiceAdapter setParam(Map<String,String> parameters);
    WebServiceAdapter call();
    string getResponse();
}