@IsTest
private class StoreDocumentAsAttachmentTOSR_TC {
   private static testmethod void method1() {
        Document document = new Document();
        document.Body = Blob.valueOf('Some Text');
        document.ContentType = '.jpg';
        document.DeveloperName = 'my_document';
        document.IsPublic = true;
        document.Name = 'My Document';
        document.FolderId = userInfo.getUserId();
        insert document;
        NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
        SR.Delivery_mode__c='Email';
        SR.Deal_ID__c='1001';
        sr.ddp_id__c = document.id;
        SR.Registration_Date__c=system.today();
        insert SR;
        //List<string> docList = new List<string>();
        //docList.add(document.id);
        StoreDocumentAsAttachmentTOSR.uploadAttachment(document.id,sr.id);
        
        
   } 
}