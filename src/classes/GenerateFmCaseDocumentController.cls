/**
 * @File Name          : GenerateFmCaseDocumentController.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 10/23/2019, 3:58:14 AM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    10/9/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public without sharing class GenerateFmCaseDocumentController {   
    public String fmCaseId;  
    public fm_case__c objFMCase;
    public List<Attachment> attachmentList; 
    public GenerateFmCaseDocumentController(ApexPages.StandardController stdCon) {
        fmCaseId = stdCon.getId();
        attachmentList = new List<Attachment>();
    }
    public pageReference returnToCase(){
        PageReference Pg = new PageReference('/'+fmCaseId);
        return pg;
    }
    public void callNoticePDFGenerator(){
        objFMCase = [
            SELECT 
                 Name
                , Booking_Unit__c
                , Account__c
                , Account__r.Name
                , Notice_Type__c
                , Date_of_Noticing_Violation__c
                , Booking_Unit__r.Resident__r.Name
                , Unit_Name__c
                , Booking_Unit__r.Property_Name__c
                , Violation_rule_name__c
                , Remedial_Period__c
                , Payble_Fine__c
                , Rectification_Cost_Text__c
            FROM fm_case__c 
            WHERE Id =: fmCaseId];
        callNoticeHtmlDocument(objFMCase);
    } 
    public void callNoticeHtmlDocument(fm_case__c objFMCase){
        PageReference noticeHTMLDocument ;
        String docVersion = '';
        String todaysDate = String.valueOf(System.Today()); 
        todaysDate = todaysDate.substring(0, 10); 
        if(objFMCase.Notice_Type__c != NULL){
            System.debug('inside if notice type not null'+objFMCase);
            if(objFMCase.Notice_Type__c == 'First Notice'){
                objFMCase.First_Notice_Generation_Date__c = System.today();
                
                // Update FM case with First Notice Generation date
                update objFMCase;
                
                System.debug('inside if notice type first notice: '+objFMCase.Notice_Type__c );
                // For First Notice HTML Page
                noticeHTMLDocument = page.FirstNoticeHTMLDocument;
                
                // Convert it into json
                String JSONString = JSON.serialize(objFMCase);
                System.debug('JSONString::::'+JSONString);

                // Call page to send json 
                noticeHTMLDocument.getparameters().put('srJSON', JSONString);
            
                
                Blob FirstNotice_DocBody;  
                try {
                     if(Test.isRunningTest()) { 
                        FirstNotice_DocBody = Blob.valueOf('test');
                    }
                    else{
                        // Get content from pdf
                        System.debug('doc body::::'+noticeHTMLDocument.getContentAsPDF());
                        FirstNotice_DocBody = noticeHTMLDocument.getContentAsPDF();
                        System.debug('body should be fine');
                    } 
                    
                } catch (Exception e) {
                    System.debug('in the catch block');
                    FirstNotice_DocBody = Blob.valueOf('Some Text');
                }
                 
                Attachment firstNotice_Attachment = new Attachment();
                firstNotice_Attachment.Body = FirstNotice_DocBody;
                firstNotice_Attachment.Name = 'First Notice Document' + '.pdf';
                firstNotice_Attachment.IsPrivate = false;
                firstNotice_Attachment.ContentType='application/pdf';
                firstNotice_Attachment.ParentId = fmCaseId;
                attachmentList.add(firstNotice_Attachment);
            }
            if(objFMCase.Notice_Type__c == 'Immediate'){
                 System.debug('inside if notice type immediate'+objFMCase.Notice_Type__c );
                // For First Notice HTML Page
                noticeHTMLDocument = page.ImmediateNoticeHTMLDocument;
                
                // Convert it into json
                String JSONString = JSON.serialize(objFMCase);
                System.debug('JSONString::::'+JSONString);

                // Call page to send json 
                noticeHTMLDocument.getparameters().put('srJSON', JSONString);
                 
                
                Blob immediateNotice_DocBody;  
                try {
                    
                    // Get content from pdf
                    if(Test.isRunningTest()) { 
                        immediateNotice_DocBody = Blob.valueOf('test');
                    }
                    else{
                        System.debug('doc body::::'+noticeHTMLDocument.getContentAsPDF());
                        immediateNotice_DocBody = noticeHTMLDocument.getContentAsPDF();
                        System.debug('body should be fine');
                    }
                   
                } catch (Exception e) {
                    System.debug('in the catch block');
                    immediateNotice_DocBody = Blob.valueOf('Some Text');
                }
                 
                Attachment immedidateNotice_Attachment = new Attachment();
                immedidateNotice_Attachment.Body = immediateNotice_DocBody;
                immedidateNotice_Attachment.Name = 'Immediate Notice Document' + '.pdf';
                immedidateNotice_Attachment.IsPrivate = false;
                immedidateNotice_Attachment.ContentType='application/pdf';
                immedidateNotice_Attachment.ParentId = fmCaseId;
                attachmentList.add(immedidateNotice_Attachment);
            }
             try{
            ApexPages.Message myMsg = new ApexPages.Message(
                ApexPages.Severity.CONFIRM,
                 'Your request for Notice document was successfully submitted.'+
                 ' Please check the documents section for the document in a while.');
              ApexPages.addMessage(myMsg);
                insert attachmentList;
            }
            catch(Exception e){
                System.debug('exception:::::::'+e);
            }

        }
        else {
            ApexPages.Message myMsg = new ApexPages.Message(
                ApexPages.Severity.ERROR,
                 'Please Select Notice Type');
              ApexPages.addMessage(myMsg);
        }

        // insert attachmentList
       
    }
}