public without sharing class CreateWeeklyOrMonthlyReviewCntrl {
    public List<Daily_Collection_Review__c> wsrInstance { get; set; }
    public List<String> headerValues1 { get; set; }
    public List<String> headerValues2 { get; set; }
    public List<String> headerValues3 { get; set; }
    public List<String> headerValues4 { get; set; }
    public List<String> headerValues5 { get; set; }
    public List<String> headerValues6 { get; set; } 
    public List<SelectOption> mangersOptions {get;set;}
    public Map<String,List<Daily_Collection_Review__c>> mapOfTopic1_ParticularsAndTheirValues1 { get; set; }
    public Map<String,List<Daily_Collection_Review__c>> mapOfTopic1_ParticularsAndTheirValues2 { get; set; }
    public Map<String,List<Daily_Collection_Review__c>> mapOfTopic1_ParticularsAndTheirValues3 { get; set; }
    public Map<String,List<Daily_Collection_Review__c>> mapOfTopic1_ParticularsAndTheirValues4 { get; set; }
    public Map<String,List<Daily_Collection_Review__c>> mapOfTopic1_ParticularsAndTheirValues5 { get; set; }
    public Map<String,List<Daily_Collection_Review__c>> mapOfTopic1_ParticularsAndTheirValues6 { get; set; }
    public Daily_Collection_Review__c objWSR {get;set;}
    public Boolean showAlreadyPresentPopUp {get;set;}
    public Id alreadyPresentId {get;set;}
    public Id recordId {get;set;}
    public Boolean showReviewTable {get;set;}
    public Boolean showReviewTable2 {get;set;}
    public Boolean showRemarkPanel1 {get;set;}
    public Boolean showRemarkPanel2 {get;set;}
    public Boolean showPanel1 {get;set;}
    public Boolean showPanel2 {get;set;}
    public Boolean showDraftButton {get;set;}
    public Boolean showSubmitButton {get;set;}
    public Boolean noaccess {get;set;}
    //public Boolean submittedMessage {get;set;}
    //public Boolean savedMessage {get;set;}
    public Boolean fillReviewMonth {get;set;}
    public Boolean fillReviewWeek {get;set;}
    public String message {get;set;}
    public Boolean isSysAdmin {get;set;}
    public Id recordTypeId;
    String toAddress, fromAddress;
    public CreateWeeklyOrMonthlyReviewCntrl(){
        wsrInstance = new List<Daily_Collection_Review__c>();
        headerValues1 = new List<String>();
        headerValues2 = new List<String>();
        headerValues3 = new List<String>();
        headerValues4 = new List<String>();
        headerValues5 = new List<String>();
        headerValues6 = new List<String>();
        showAlreadyPresentPopUp = false;
        //submittedMessage = false;
        //savedMessage = false;
        showReviewTable = false;
        showReviewTable2 = false;
        showRemarkPanel1 = false;
        showRemarkPanel2 = false;
        showPanel1 =false;
        showPanel2 =false;
        noaccess =false;
        fillReviewMonth =false;
        fillReviewWeek =false;
        isSysAdmin = false;
        mapOfTopic1_ParticularsAndTheirValues1 = new Map<String,List<Daily_Collection_Review__c>>();
        mapOfTopic1_ParticularsAndTheirValues2 = new Map<String,List<Daily_Collection_Review__c>>();
        mapOfTopic1_ParticularsAndTheirValues3 = new Map<String,List<Daily_Collection_Review__c>>();
        mapOfTopic1_ParticularsAndTheirValues4 = new Map<String,List<Daily_Collection_Review__c>>();
        mapOfTopic1_ParticularsAndTheirValues5 = new Map<String,List<Daily_Collection_Review__c>>();
        mapOfTopic1_ParticularsAndTheirValues6 = new Map<String,List<Daily_Collection_Review__c>>();
        
    }
    
    public void createWSR(){
        recordTypeId = Schema.SObjectType.Daily_Collection_Review__c.getRecordTypeInfosByDeveloperName().get('Weekly_Review').getRecordTypeId();
        system.debug('Inside WSR');
        //create WSR only if page is invoked from tab
        String urlValue = Apexpages.currentPage().getUrl();
        System.debug('urlValue---'+urlValue);
        
        //page invoked from view link, WSR already present
        if(urlValue.contains('?id')){
            showPanel2 =true;
            String wsrId = ApexPages.CurrentPage().getparameters().get('id');
            System.debug('wsrId------'+wsrId);
           if(String.isNotBlank(wsrId)){
             fillWSR(wsrId);
            }
        }
        //page invoked from tab, create WSR
        else {
            showPanel1 = true;
            WeeklyReviewHODAndManagerMapping__c wsrMapping = WeeklyReviewHODAndManagerMapping__c.getInstance(UserInfo.getUserId());
            System.debug('wsrMapping------'+wsrMapping);
            
            if(wsrMapping != null && wsrMapping.HOD_Id__c != null && wsrMapping.Manager_Id__c != null){
                List<String> managerIds = wsrMapping.Manager_Id__c.split(',');
                List<User> mangersList = [SELECT Id,
                                          Name
                                          FROM User 
                                          WHERE Id IN:managerIds];
                mangersOptions = new List<SelectOption>();
                mangersOptions.add(new SelectOption('','--None--'));
                if(mangersList != Null && mangersList.size() > 0) { 
                    for(User mgrObj:mangersList) {
                        mangersOptions.add(new SelectOption(mgrObj.Id,mgrObj.Name)); 
                    }
                }
                
                Daily_Collection_Review__c objWSR1 = new Daily_Collection_Review__c();
                objWSR1.Weekly_Review_Initiator__c = wsrMapping.HOD_Id__c;
                //objWSR1.Weekly_Review_Manager__c = wsrMapping.Manager_Id__c;
                objWSR1.Meeting_Date__c = System.today();
                objWSR1.Weekly_Review_Status__c = 'In Draft';
                objWSR1.RecordTypeId = recordTypeId;
                insert objWSR1;
                objWSR = [SELECT Id,
                          Weekly_Review_Manager__c,
                          Weekly_Review_Status__c,
                          Review_For__c,
                          Months__c,
                          Weeks__c
                          FROM Daily_Collection_Review__c 
                          WHERE Id=:objWSR1.Id
                          AND RecordTypeId =: recordTypeId
                         ];
                //fillWSR(objWSR.Id);
            }
            else {
                showPanel1 = false;
                showPanel2 = false;
                system.debug('Cannot Fill The WSR');
                noaccess =true;
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Access denied to this Page Contact administrator for support.'));
            }
        }
    }
    
    public void fillReview() {
        List<Daily_Collection_Review__c> monthlyOrWeeklyList = [SELECT Id,
                                                                Weekly_Review_Manager__c,
                                                                Weekly_Review_Status__c,
                                                                Review_For__c,
                                                                Months__c,
                                                                Weeks__c
                                                                FROM Daily_Collection_Review__c 
                                                                WHERE Weekly_Review_Manager__c =: objWSR.Weekly_Review_Manager__c
                                                                AND Months__c =:objWSR.Months__c
                                                                AND (Weeks__c =:objWSR.Weeks__c OR Weeks__c = Null)
                                                                AND Review_For__c=:objWSR.Review_For__c
                                                                AND RecordTypeId =: recordTypeId
                                                               ];
        if(monthlyOrWeeklyList != null && monthlyOrWeeklyList.size() > 0) {
            System.debug('Already Filled Review');
            showAlreadyPresentPopUp =true;
            alreadyPresentId = monthlyOrWeeklyList[0].Id;
            showReviewTable = false;
            showDraftButton = false;
            showSubmitButton =false;
            
        }
        else {
            System.debug('You can fill the review');
            showAlreadyPresentPopUp =false;
            if(objWSR != Null) {
                upsert objWSR;
                fillWSR(objWSR.Id);
            }
           
            
        }
        
    }
    public void fillWSR(Id wsrId) {
       try{ 
        wsrInstance = [SELECT Id,
                       Name,
                       Weekly_Review_Manager__c,
                       Weekly_Review_Initiator__c ,
                       Weekly_Review_Status__c,
                       Review_For__c,
                       Months__c,
                       Weeks__c,
                       Manager_display_strong_command__c,
                       Manager_display_strong_command_freetext__c,
                       Manager_display_strong_performance__c,
                       Manager_display_strong_performance_ft__c,
                       Manager_have_understanding_of_cus__c,
                       Manager_knows_all_customers_freetext__c,
                       Managers_performance_on_assisting_CREs__c,
                       Managers_performance_on_assisting_CRE_ft__c,
                       Managers_performance_on_cases_raised__c,
                       Managers_performance_on_cases_raised_ft__c,
                       Managers_performance_on_Quality_Checks__c,
                       Managers_performance_on_Quality_Check_ft__c,
                       Managers_performance_on_audit__c,
                       Managers_performance_on_audit_ft__c,
                       Managers_performance_on_daily_huddle__c,
                       Managers_performance_on_daily_huddle_ft__c,
                       Managers_team_showing_improvement__c,
                       Managers_team_showing_improvement_ft__c,
                       Managers_Collection_from_365_days_OD__c,
                       Managers_Collections_from_365_days_OD_ft__c,
                       Manager_Collection_from_40_paid_bracket__c,
                       Manager_Collection_from_40_paid_brkt_ft__c,
                       managers_performance_on_Active__c,
                       Managers_performance_on_Active_Promo_ft__c,
                       Managers_team_collected_more_units__c,
                       Managers_team_collected_more_units_ft__c,
                       Managers_Remark_On_Action_Taken__c,
                       Weekly_Review_Manager__r.Name,
                       Weekly_Review_Initiator__r.Name,
                       HOD_Submitted_Date__c,
                       Manager_Submitted_Date__c
                       
                       
                       FROM Daily_Collection_Review__c
                       WHERE Id =:wsrId
                       AND RecordTypeId =: recordTypeId];
        System.debug('wsrInstance---->'+wsrInstance);
        }
       catch(Exception e){
          System.debug(e.getMessage());
        }
        
        //query system admin profile
        /*Profile adminProfile = [Select Id, Name From Profile Where Name = :Label.System_Admin_Profile_Name_for_Collection_Review];
        if(UserInfo.getProfileId() == adminProfile.Id){
            isSysAdmin = true;
        }*/
        
        //fetch custom setting for allowed profiles to view DCR
            List<String> prNames = new List<String>();
            Profiles_allowed_to_view_DCR__c prSetting = Profiles_allowed_to_view_DCR__c.getInstance('Profiles');
            
            if(prSetting != null && prSetting.Profile_Names__c != null){
                System.debug('prSetting-----'+prSetting);
                prNames.addAll(prSetting.Profile_Names__c.split(','));
                System.debug('prNames-----'+prNames);
            
                //set booolean variables
                List<Profile> adminProfileList = [Select Id, Name From Profile Where Name IN :prNames];
                for(Profile adminProfile : adminProfileList){
                    if(UserInfo.getProfileId() == adminProfile.Id){
                        isSysAdmin = true;
                    }
                }
            }else {
                isSysAdmin = false;
            }
            
      if(wsrInstance!=Null && wsrInstance.size() > 0) {
        if(wsrInstance[0].Weekly_Review_Initiator__c == UserInfo.getUserId()) {
            if(wsrInstance[0].Weekly_Review_Status__c == 'In Draft') {
                showReviewTable = true;  
                showDraftButton = true;
                showSubmitButton = true;              
            }
            else if(wsrInstance[0].Weekly_Review_Status__c == 'Closed' || wsrInstance[0].Weekly_Review_Status__c == 'Pending With Manager') {
                showReviewTable2 = true;
                showRemarkPanel1 =true;
                showDraftButton = false;
                showSubmitButton =false;
            }
        }
        /*else if(wsrInstance[0].Weekly_Review_Initiator__c != UserInfo.getUserId() && wsrInstance[0].Weekly_Review_Manager__c != UserInfo.getUserId()) {
            showReviewTable2 = true;
            showRemarkPanel1 =true;            
        }*/
        else if(wsrInstance[0].Weekly_Review_Manager__c == UserInfo.getUserId()) {
            if(wsrInstance[0].Weekly_Review_Status__c == 'In Draft') {
                System.debug('Cannot View Record');
                showPanel1 = false;
                showPanel2 = false;
                noaccess =true;
                //message = 'Saved';
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Access denied to view this record.'));
            }
            else if(wsrInstance[0].Weekly_Review_Status__c == 'Pending With Manager') {
                showReviewTable2 = true;
                showRemarkPanel2 =true;
                showDraftButton = false;
                showSubmitButton = true;
                //message = 'Submitted';
                //submittedMessage  = true;
            }
            else if(wsrInstance[0].Weekly_Review_Status__c == 'Closed') {
                showReviewTable2 = true;
                showRemarkPanel1 = true;  
                showDraftButton = false; 
                showSubmitButton = false;
                //message = 'Submitted'; 
                //submittedMessage  = true;             
            }            
        }
        else if(isSysAdmin){
            showRemarkPanel1 = true;
            showReviewTable2 = true;
            showDraftButton = false;
            showSubmitButton =false;            
        }
        else {
            showPanel1 = false;
            showPanel2 = false;
            noaccess =true;            
        }
        headerValues1 = new List<String>{'Does the Manager display strong command of the Ongoing Promotions ?','Does the Manager have an understanding of all customers managed by his team (count, collections)'};
        headerValues2 = new List<String>{'Does the Manager display strong performance on forecast for the month'}; 
        headerValues3 = new List<String>{'Evaluate the Managers performance on assisting CREs to understand customer requirement and close the collection call by participating in Customer meetings, follow-ups, negotiations etc','Evaluate the Managers performance on cases raised as per customers requirement and meeting organisation objectives'}; 
        headerValues4 = new List<String>{'Evaluate the Managers performance on Quality Checks through daily audit of CRE calls, a minimum of 5 calls per week','Evaluate the Managers performance on audit of Promise to Pay call, a minimum of 5 calls per week'}; 
        headerValues5 = new List<String>{'Evaluate the Managers performance on daily huddle and CRE performance review','Is the Managers team showing overall improvement in Collection Processes leading to a higher conversion ?','Evaluate the managers performance/Conversion on Collections from >365 days OD','Evaluate the managers performance/Conversion on Collections from <40% paid bracket','Evaluate the managers performance/Conversion on Active Promo for the month','Has the team collected from more units/customers vs last month'}; 
        headerValues6 = new List<String>{'Manager Remarks  on Actions Taken'}; 
        for(String queskey :headerValues1){
                                
            mapOfTopic1_ParticularsAndTheirValues1.put(queskey, wsrInstance);
        }
        for(String queskey :headerValues2){
            
            mapOfTopic1_ParticularsAndTheirValues2.put(queskey, wsrInstance);
        } 
        for(String queskey :headerValues3){
            
            mapOfTopic1_ParticularsAndTheirValues3.put(queskey, wsrInstance);
        } 
        for(String queskey :headerValues4){
            
            mapOfTopic1_ParticularsAndTheirValues4.put(queskey, wsrInstance);
        } 
        for(String queskey :headerValues5){
            
            mapOfTopic1_ParticularsAndTheirValues5.put(queskey, wsrInstance);
        } 
        for(String queskey :headerValues6){
            
            mapOfTopic1_ParticularsAndTheirValues6.put(queskey, wsrInstance);
        } 
        
        System.debug('mapOfTopic1_ParticularsAndTheirValues1-->'+mapOfTopic1_ParticularsAndTheirValues1);
       }
        
    }
    
     public void saveResult () {
        System.debug('Inisde Draft');
        draftOrSubmitResult('In Draft');
        
     }
     public void submitResult () {
        if(wsrInstance[0].Weekly_Review_Initiator__c == UserInfo.getUserId()) {
            draftOrSubmitResult('Pending With Manager');
            //sendUserEmail('HOD');
        }
        else if(wsrInstance[0].Weekly_Review_Manager__c == UserInfo.getUserId()) {
            draftOrSubmitResult('Closed');
            //sendUserEmail('Manager');
        }
        
     }
 
     public void draftOrSubmitResult (String status) { 
        System.debug('status'+status);
        System.debug('Inisde DraftOrSubmitResult');
        List<Daily_Collection_Review__c> wsrList = new List<Daily_Collection_Review__c>();
        if(wsrInstance != Null && wsrInstance.size() > 0) {
            for(Daily_Collection_Review__c wsr : wsrInstance) {
                System.debug('Review_For__c-->'+wsr.Review_For__c);
                System.debug('Months__c-->'+wsr.Months__c);
                System.debug('Weeks__c-->'+wsr.Weeks__c);
                System.debug('Weekly_Review_Manager__c-->'+wsr.Weekly_Review_Manager__c);
                System.debug('Weekly_Review_Initiator__c-->'+wsr.Weekly_Review_Initiator__c);
                System.debug('wsrId-->'+wsr.Id);
                System.debug('wsrManager_display_strong_command__c'+wsr.Manager_display_strong_command__c);
                System.debug('wsrManager_display_strong_command_freetext__c'+wsr.Manager_display_strong_command_freetext__c);           
                Daily_Collection_Review__c objWeeklyOrMonthly = new Daily_Collection_Review__c(Id=wsr.Id);
                objWeeklyOrMonthly.Manager_display_strong_command__c = wsr.Manager_display_strong_command__c;
                objWeeklyOrMonthly.Manager_display_strong_command_freetext__c = wsr.Manager_display_strong_command_freetext__c;
                objWeeklyOrMonthly.Manager_display_strong_performance__c = wsr.Manager_display_strong_performance__c;
                objWeeklyOrMonthly.Manager_display_strong_performance_ft__c = wsr.Manager_display_strong_performance_ft__c;
                objWeeklyOrMonthly.Manager_have_understanding_of_cus__c = wsr.Manager_have_understanding_of_cus__c;
                objWeeklyOrMonthly.Manager_knows_all_customers_freetext__c = wsr.Manager_knows_all_customers_freetext__c;
                objWeeklyOrMonthly.Managers_performance_on_assisting_CREs__c = wsr.Managers_performance_on_assisting_CREs__c;
                objWeeklyOrMonthly.Managers_performance_on_assisting_CRE_ft__c = wsr.Managers_performance_on_assisting_CRE_ft__c;
                objWeeklyOrMonthly.Managers_performance_on_cases_raised__c = wsr.Managers_performance_on_cases_raised__c;
                objWeeklyOrMonthly.Managers_performance_on_cases_raised_ft__c = wsr.Managers_performance_on_cases_raised_ft__c;
                objWeeklyOrMonthly.Managers_performance_on_Quality_Checks__c = wsr.Managers_performance_on_Quality_Checks__c;
                objWeeklyOrMonthly.Managers_performance_on_Quality_Check_ft__c = wsr.Managers_performance_on_Quality_Check_ft__c;
                objWeeklyOrMonthly.Managers_performance_on_audit__c = wsr.Managers_performance_on_audit__c;
                objWeeklyOrMonthly.Managers_performance_on_audit_ft__c = wsr.Managers_performance_on_audit_ft__c;
                objWeeklyOrMonthly.Managers_performance_on_daily_huddle__c = wsr.Managers_performance_on_daily_huddle__c;
                objWeeklyOrMonthly.Managers_performance_on_daily_huddle_ft__c = wsr.Managers_performance_on_daily_huddle_ft__c;
                objWeeklyOrMonthly.Managers_team_showing_improvement__c = wsr.Managers_team_showing_improvement__c;
                objWeeklyOrMonthly.Managers_team_showing_improvement_ft__c = wsr.Managers_team_showing_improvement_ft__c;
                objWeeklyOrMonthly.Managers_Collection_from_365_days_OD__c = wsr.Managers_Collection_from_365_days_OD__c;
                objWeeklyOrMonthly.Managers_Collections_from_365_days_OD_ft__c = wsr.Managers_Collections_from_365_days_OD_ft__c;
                objWeeklyOrMonthly.Manager_Collection_from_40_paid_bracket__c = wsr.Manager_Collection_from_40_paid_bracket__c;
                objWeeklyOrMonthly.Manager_Collection_from_40_paid_brkt_ft__c = wsr.Manager_Collection_from_40_paid_brkt_ft__c;
                objWeeklyOrMonthly.managers_performance_on_Active__c = wsr.managers_performance_on_Active__c;
                objWeeklyOrMonthly.Managers_performance_on_Active_Promo_ft__c = wsr.Managers_performance_on_Active_Promo_ft__c;
                objWeeklyOrMonthly.Managers_team_collected_more_units__c = wsr.Managers_team_collected_more_units__c;
                objWeeklyOrMonthly.Managers_team_collected_more_units_ft__c = wsr.Managers_team_collected_more_units_ft__c;
                objWeeklyOrMonthly.Weekly_Review_Manager__c= wsr.Weekly_Review_Manager__c;
                if(status == 'In Draft') {
                    objWeeklyOrMonthly.Weekly_Review_Status__c = 'In Draft';
                    objWeeklyOrMonthly.Pending_With__c = wsr.Weekly_Review_Initiator__c ;
                    //savedMessage = true;
                    //message = 'Saved'; 
                }
                else if(status == 'Pending With Manager'){
                    objWeeklyOrMonthly.Weekly_Review_Status__c = 'Pending With Manager';
                    objWeeklyOrMonthly.Pending_With__c = wsr.Weekly_Review_Manager__c;
                    objWeeklyOrMonthly.HOD_Submitted_Date__c =System.now();
                    //submittedMessage  = true;
                    //message = 'Submitted'; 
                }
                 else if(status == 'Closed'){
                    objWeeklyOrMonthly.Managers_Remark_On_Action_Taken__c = wsr.Managers_Remark_On_Action_Taken__c;
                    objWeeklyOrMonthly.Weekly_Review_Status__c = 'Closed';
                    objWeeklyOrMonthly.Pending_With__c = Null;
                    objWeeklyOrMonthly.Manager_Submitted_Date__c =System.now();
                    //submittedMessage  = true;
                    //message = 'Submitted'; 
                }
                 wsrList.add(objWeeklyOrMonthly);
            }
        }
        try{
            if(wsrList != Null && wsrList.size() > 0) {
                update wsrList;
                if(wsrList[0].Weekly_Review_Status__c == 'In Draft'){
                    message = 'Review Saved Successfully';
                }
                else {
                   message = 'Review Submitted Successfully'; 
                }
                recordId =wsrList[0].Id;
            }
        }
        catch( Exception e ) {
            message = e.getMessage();
        }
        System.debug('wsrList---->'+wsrList);
        
           
    }
    
    public PageReference submitSaveRedirect() {
        //Id recordId = wsrInstance[0].Id;
        PageReference pageRef = new PageReference('/apex/CreateWeeklyOrMonthlyReview?id=' + wsrInstance[0].Id);
        pageRef.setRedirect(true); 
        return pageRef;
    }
    
    public PageReference generatePDF() {
        PageReference pageRef = new PageReference('/apex/CreateWeeklyOrMonthlyReview_PDF?id=' + wsrInstance[0].Id);
        pageRef.setRedirect(true); 
        return pageRef;
    }
    
    public void sendUserEmail(){    
     List<Process_Email_Template__mdt> lstSubmitEmailTemp = [SELECT 
                                                                Process_Name__c
                                                                , Email_Template__c
                                                                , Nationality__c
                                                            FROM 
                                                                Process_Email_Template__mdt
                                                            WHERE 
                                                                Process_Name__c =: 'Weekly Submit Email'
                                                            AND
                                                                Email_Template__c != null 
                                                            LIMIT 1];
            
            EmailTemplate SubmitTemplate;
            system.debug('lstSubmitEmailTemp[0].Email_Template__c -->'+lstSubmitEmailTemp[0].Email_Template__c );
            if( lstSubmitEmailTemp != null && lstSubmitEmailTemp.size() > 0 ) {
                SubmitTemplate = [SELECT 
                                            Id
                                            , Subject
                                            , Body
                                            , HtmlValue
                                            , TemplateType
                                        FROM 
                                            EmailTemplate 
                                        WHERE 
                                            DeveloperName = : lstSubmitEmailTemp[0].Email_Template__c 
                                        LIMIT 1][0];
            system.debug('SubmitTemplate -->'+SubmitTemplate );
            system.debug('lstSubmitEmailTemp[0].Email_Template__c -->'+lstSubmitEmailTemp[0].Email_Template__c );
                
            }
        Map<Id,User> userMap = new Map<Id,User>([SELECT Email 
                                                        , Id 
                                                 FROM User 
                                                 WHERE Id = :wsrInstance[0].Weekly_Review_Initiator__c
                                                 OR Id = :wsrInstance[0].Weekly_Review_Manager__c
                                                ]);
      if(userMap !=Null && userMap.size() > 0) {
        if(wsrInstance[0].Weekly_Review_Initiator__c == UserInfo.getUserId()) {
            List<String> emailIds;
                toAddress = userMap.get(wsrInstance[0].Weekly_Review_Manager__c).Email;
                fromAddress = userMap.get(wsrInstance[0].Weekly_Review_Initiator__c).Email;
                system.debug('From HOD To Manager');
                sendEmail('HOD','Manager',SubmitTemplate,toAddress,fromAddress);
           
        }
         else if(wsrInstance[0].Weekly_Review_Manager__c == UserInfo.getUserId()) {
                toAddress = userMap.get(wsrInstance[0].Weekly_Review_Initiator__c).Email;
                fromAddress = userMap.get(wsrInstance[0].Weekly_Review_Manager__c).Email;
                system.debug('From Manager To HOD');
                sendEmail('Manager','HOD',SubmitTemplate,toAddress,fromAddress);
            
        }
       }
}
EmailMessage sendEmail(String Updater,String Addressee, EmailTemplate template, String toAddress, String fromAddress) {
        
        
        String subject = String.isNotBlank(  template.subject ) ? template.subject : 'Test DCR Email';
        String contentType = 'text/html';
        String bccAddress = '';
        String replyToAddress ='';
        String strCCAddress = '';
        String contentValue = template.htmlValue != null ? replaceMergeFields( Updater, Addressee, template.HtmlValue ) : '' ;
        String contentBody =  template.Body != null ? replaceMergeFields( Updater, Addressee, template.Body ) : '';
        subject = template.Subject != null ? replaceMergeFields( Updater, Addressee, template.Subject ) : '';
        system.debug('contentValue--->'+contentValue);
        system.debug('contentBody--->'+contentBody);
        if( String.isNotBlank( toAddress ) && String.isNotBlank( subject ) 
           && String.isNotBlank( contentBody ) ) {
               
               try {
                   system.debug('in send email');
                   //Sending emails through Sendgrid
                   SendGridEmailService.SendGridResponse objSendGridResponse = 
                       SendGridEmailService.sendEmailService( toAddress,''
                                                             , strCCAddress, ''
                                                             , bccAddress, ''
                                                             , subject, ''
                                                             , fromAddress, ''
                                                             , replyToAddress, ''
                                                             , contentType
                                                             , contentValue, '',
                                                             new List<Attachment>{});
                   
                   String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;
                   system.debug('responseStatus------>'+responseStatus);
               //}
                  /*if( responseStatus == 'Accepted' ) {
                        EmailMessage mail = new EmailMessage();
                        mail.Subject = subject;
                        mail.MessageDate = System.Today();
                        mail.Status = '3';//'Sent';
                        mail.RelatedToId = accountId;
                        mail.Account__c  = accountId;
                        mail.ToAddress = toAddress;
                        mail.FromAddress = fromAddress;
                        mail.TextBody = contentValue;//contentValue.replaceAll('\\<.*?\\>', '');
                        mail.Sent_By_Sendgrid__c = true;
                        mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                        mail.Booking_Unit__c = objBU.Id;
                        mail.CcAddress = strCCAddress;
                        mail.BccAddress = bccAddress;
                        system.debug('Mail obj == ' + mail);
                        return mail;
                    }//End response if*/
                } catch( Exception e ) {

                    //Creating error logs
                    //lstErrorLogs.add( GenericUtility.createErrorLog( e.getMessage(), accountId, objBU.Id, null, null) );
                    system.debug('error while sending email'+e);
                }
               EmailMessage mail = new EmailMessage();
               return mail;
        }
        EmailMessage mail = new EmailMessage();
        return mail;
    }
    
    // replace merge fields with actual content
    
    public String replaceMergeFields( String Updater,String Addressee,String strContents ) {
        
        if( String.isNotBlank( strContents ) ) {
            for( Merge_Field_Mapping__mdt mdt : [ SELECT
                                                    MasterLabel
                                                    , Merge_Field__c
                                                    , Process_Name__c
                                                FROM
                                                    Merge_Field_Mapping__mdt
                                                WHERE
                                                    Process_Name__c = 'Weekly Emails'] ) {
                if( strContents.contains( mdt.Merge_Field__c ) ) {
                    String strVal = getStringValue( mdt.MasterLabel);
                    
                    if( String.isNotBlank( strVal ) ) {
                        if(mdt.Merge_Field__c.contains('Addressee') && !(mdt.Merge_Field__c.contains(Addressee))) {
                            strVal = '';
                        }
                        if(mdt.Merge_Field__c.contains('Updater') && !(mdt.Merge_Field__c.contains(Updater))) {
                            strVal = '';
                        }
                        strContents = strContents.replace( mdt.Merge_Field__c , strVal );
                    }
                    else {
                        strContents = strContents.replace( mdt.Merge_Field__c, '');
                    }
                }                                    
            }
            if(strContents.contains( '{!Link}' )) {
                String Recordlink = URL.getSalesforceBaseUrl().toExternalForm()+'/'+wsrInstance[0].Id;
                strContents = strContents.replace( '{!Link}' , Recordlink );
            }
             if(strContents.contains( '{!RecordName}' )) {
                //String Recordlink = URL.getSalesforceBaseUrl().toExternalForm()+'/'+wsrInstance[0].Id;
                strContents = strContents.replace( '{!RecordName}' , wsrInstance[0].Name);
            }
            if(strContents.contains( '{!Type}' )) {
                //String Recordlink = URL.getSalesforceBaseUrl().toExternalForm()+'/'+wsrInstance[0].Id;
                strContents = strContents.replace('{!Type}', wsrInstance[0].Review_For__c);
            }
            if(strContents.contains( '{!Months}' )) {
                //String Recordlink = URL.getSalesforceBaseUrl().toExternalForm()+'/'+wsrInstance[0].Id;
                strContents = strContents.replace('{!Months}', wsrInstance[0].Months__c);
            }
             if(strContents.contains( '{!Weeks}' )) {
                 if(wsrInstance[0].Weeks__c != Null) {
                     String week = 'Week'+' '+wsrInstance[0].Weeks__c;
                     strContents = strContents.replace('{!Weeks}',week);
                 }
                 else {
                     strContents = strContents.replace('{!Weeks}','');
                 }
                //String Recordlink = URL.getSalesforceBaseUrl().toExternalForm()+'/'+wsrInstance[0].Id;
                
            }
            system.debug('strContents--->'+strContents);

            return strContents;
        }//End of blank contents if

        return null;
    }//End of replaceMergeFields
    
     public String getStringValue( String field_API_Name ) {
        
        if( field_API_Name.contains('.') ) {
            String strAfter = field_API_Name.subStringAfter('.');
            String relatedObject1 = field_API_Name.subStringBefore('.');

                // 1 level parenting
                System.debug('Parenting 1---');
                System.debug('fieldOfRelatedObj-' + strAfter);
                System.debug('related expression--' + relatedObject1 + '.' + strAfter );
                if(wsrInstance[0].getSobject( relatedObject1 ) != null) {
                    return String.valueOf( wsrInstance[0].getSobject( relatedObject1 ).get( strAfter ) );
                }
                return Null;
        }
        else {
            return String.valueOf( wsrInstance[0].get( field_API_Name ) );
        }
    }
    
    
}