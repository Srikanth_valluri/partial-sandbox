@isTest
private class DAMAC_Project_Details_Test {

    @testSetup static void setupData() {
        List<AdditionalFeatures__c> features = new List<AdditionalFeatures__c>();

        AdditionalFeatures__c feature1 = new AdditionalFeatures__c();
        feature1.Name = 'Cafes';
        feature1.Label__c = 'Cafes';
        feature1.Url__c = 'AdditionalFeatures/cafe.png';
        features.add(feature1);

        AdditionalFeatures__c feature2 = new AdditionalFeatures__c();
        feature2.Name = 'Swimmimg Pool';
        feature2.Label__c = 'Swimmimg Pool';
        feature2.Url__c = 'AdditionalFeatures/Swimming_Pool.png';
        features.add(feature2);

        AdditionalFeatures__c feature3 = new AdditionalFeatures__c();
        feature3.Name = 'Spa';
        feature3.Label__c = 'Spa';
        feature3.Url__c = 'AdditionalFeatures/SPA.png';
        features.add(feature3);

        insert features;

        List<NearByImagesList__c> nearByImages = new List<NearByImagesList__c>();
        
        NearByImagesList__c image1 = new NearByImagesList__c();
        image1.Name='Airport';
        image1.Label__c='Airport';
        image1.Url__c='/Airport.png';
        nearByImages.add(image1);

        NearByImagesList__c image2 = new NearByImagesList__c();
        image2.Name='ATM';
        image2.Label__c='ATM';
        image2.Url__c='/ATM.png';
        nearByImages.add(image2);

        NearByImagesList__c image3 = new NearByImagesList__c();
        image3.Name='BusStop';
        image3.Label__c='BusStop';
        image3.Url__c='/BusStop.png';
        nearByImages.add(image3);

        insert nearByImages;

        Marketing_Documents__c mrkDoc = new Marketing_Documents__c();
        mrkDoc.Name = 'Aurum Villas';
        mrkDoc.Has_Multiple_Property__c = true;
        mrkDoc.Marketing_Name__c = 'Aurum Villas';

        insert mrkDoc;

        Property__c firstProp  = new Property__c();
        firstProp.Name = 'COURSETIA @ AKOYA OXYGEN';
        firstProp.Property_Name__c = 'COURSETIA @ AKOYA OXYGEN';
        firstProp.Property_ID__c = 3758;
        firstProp.Currency_Of_Sale__c = 'AED';
        firstProp.Property_Code__c = 'CRT';

        insert firstProp;

        Location__c firstLoc = new Location__c();
        firstLoc.Location_Code__c = 'CRT';
        firstLoc.Name = 'CRT';
        firstLoc.Location_ID__c = '74588';
        firstLoc.Location_Type__c = 'Building';
        firstLoc.Building_Name__c = 'COURSETIA @ AKOYA OXYGEN';

        insert firstLoc;

        //insert data for first payment plan
        Payment_Plan__c objPaymentPlan = new Payment_Plan__c();
        objPaymentPlan.Building_Location__c = firstLoc.Id;
        objPaymentPlan.Effective_From__c = system.today();
        objPaymentPlan.Effective_To__c = system.today().addDays(3);
        insert objPaymentPlan;

        // insert data for first payment terms
        insertPaymentTerms(objPaymentPlan.Id);
        List<Inventory__c> invlst= new List<Inventory__c>();

        Inventory__c inv1 = new Inventory__c();
        inv1.Marketing_Name_Doc__c = mrkDoc.Id;
        inv1.Marketing_Name__c = 'Aurum Villas';
        inv1.Building_Location__c = firstLoc.Id;
        inv1.Property__c = firstProp.Id;
        inv1.RecordTypeId = DamacUtility.getRecordTypeId('Inventory__c','Building');
        inv1.Status__c = 'Released';
        inv1.ACD_Date__c = '2019-03-31';
        inv1.Area_Sqft__c = '2403';
        inv1.Special_Price__c = 2016000.00;
        inv1.Property_Name__c = 'COURSETIA @ AKOYA OXYGEN';
        inv1.IPMS_Bedrooms__c = '1';
        inv1.Currency_of_Sale__c = 'EUR';
        invlst.add(inv1);
        

        Inventory__c inv2 = new Inventory__c();
        inv2.Marketing_Name_Doc__c = mrkDoc.Id;
        inv2.Marketing_Name__c = 'Aurum Villas';
        inv2.Building_Location__c = firstLoc.Id;
        inv2.Property__c = firstProp.Id;
        inv2.RecordTypeId = DamacUtility.getRecordTypeId('Inventory__c','Building');
        inv2.Status__c = 'Released';
        inv2.ACD_Date__c = '2019-03-31';
        inv2.Area_Sqft__c = '2403';
        inv2.Special_Price__c = 2016000.00;
        inv2.Property_Name__c = 'COURSETIA @ AKOYA OXYGEN';
        inv2.Unit_Type__c = 'Unittype';
        inv2.Bedroom_Type__c='Bedroom Type';
        inv2.IPMS_Bedrooms__c='IPMS Bedrooms';
        inv2.View_Type__c='Back';
        inv2.Floor_Package_Type__c = 'Floor';
        inv2.IPMS_Bedrooms__c = '2';
        inv2.Currency_of_Sale__c = 'EUR';
        invlst.add(inv2);

        insert invlst;
        
        Campaign__c camp = new Campaign__c();
        camp.Campaign_Name__c='Test Campaign';
        camp.start_date__c = System.today();
        camp.end_date__c = System.Today().addDays(30);
        camp.Marketing_start_date__c = System.today();
        camp.Marketing_end_date__c = System.Today().addDays(30);
        camp.Language__c = 'English';
        camp.Marketing_Active__c = true;
        camp.Credit_Control_Active__c  = true;
        camp.Sales_Admin_Active__c  = true;
        insert camp;

        Campaign_Inventory__c campaignobj = new Campaign_Inventory__c();
        campaignobj.Campaign__c = camp.id;
        campaignobj.Inventory__c = inv1.id;
        insert campaignobj ;

        Inquiry__c newInquiry = new Inquiry__c();
        newInquiry.Campaign__c = camp.Id;
        newInquiry.First_Name__c = 'Test';
        newInquiry.Last_Name__c = 'Inquiry';
        newInquiry.Mobile_Phone_Encrypt__c = '05789088';
        newInquiry.Mobile_Phone__c = '05789088';
        newInquiry.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        newInquiry.Email__c = 'craig.lobo@eternussolutions.com';
        newInquiry.Preferred_Language__c = 'English';
        newInquiry.Inquiry_Source__c = 'Social' ;
        newInquiry.Inquiry_Status__c = 'New' ;
        insert newInquiry;

        List<Project_Information__c> projectInfoList = new List<Project_Information__c>();

        Project_Information__c prjInfo1 = new Project_Information__c();
        prjInfo1.Marketing_Documents__c = mrkDoc.Id;
        prjInfo1.Features__c = 'Gallery';
        prjInfo1.Label__c = 'Project Plan';
        prjInfo1.Property__c = firstProp.Id;
        prjInfo1.Use_For_Compare__c = true;
        prjInfo1.URL__c = 'https://damacholding--devpronew--c.cs89.content.force.com/servlet/servlet.FileDownload?file=00P0E0000019Ar6';
        projectInfoList.add(prjInfo1);

        Project_Information__c prjInfo2 = new Project_Information__c();
        prjInfo2.Marketing_Documents__c = mrkDoc.Id;
        prjInfo2.Features__c = 'Unit Features';
        prjInfo2.Heading__c = 'Project Plan';
        prjInfo2.Property__c = firstProp.Id;
        prjInfo2.Detail__c = '<ul><li>All rooms feature double glazed windows</li>'+
                             '<li>Ceramic tiled floors throughout</li>'+
                             '<li>Painted plastered walls and soffit</li>'+
                             '<li>Wardrobes in Bedrooms</li></ul>';
        prjInfo2.Order__c = 1;
        projectInfoList.add(prjInfo2);

        Project_Information__c prjInfo3 = new Project_Information__c();
        prjInfo3.Marketing_Documents__c = mrkDoc.Id;
        prjInfo3.Features__c = 'Unit Features';
        prjInfo3.Heading__c = 'Kitchen';
        prjInfo3.Property__c = firstProp.Id;
        prjInfo3.Detail__c = '<ul><li>Kitchens fitted with cabinets and counter tops with space '+
                             'and hookup provisions for white goods</li>'+
                             '<li>Ceramic tiled floors</li>'+
                             '<li>Emulsion paints for walls</li>'+
                             '<li>Laminated kitchen cabinet</li>'+
                             '<li>Stone countertop</li>'+
                             '<li>Stainless Steel sink</li></ul>';
        prjInfo3.Order__c = 2;
        projectInfoList.add(prjInfo3);

        Project_Information__c prjInfo4 = new Project_Information__c();
        prjInfo4.Marketing_Documents__c = mrkDoc.Id;
        prjInfo4.Features__c = 'Unit Features';
        prjInfo4.Heading__c = 'Bathroom Features';
        prjInfo4.Property__c = firstProp.Id;
        prjInfo4.Detail__c = '<ul><li>Floor / Wall ceramic tiles</li>'+
                             '<li>Standard white sanitary ware</li>'+
                             '<li>Standard sanitary fittings and accessories</li>'+
                             '<li>Mirror</li><li>Threshold</li></ul>';
        prjInfo4.Order__c = 3;
        projectInfoList.add(prjInfo4);

        Project_Information__c prjInfo5 = new Project_Information__c();
        prjInfo5.Marketing_Documents__c = mrkDoc.Id;
        prjInfo5.Features__c = 'Project Features';
        prjInfo5.Property__c = firstProp.Id;
        prjInfo5.Project_Features__c = 'Cafes; Spa; Swimmimg Pool';
        projectInfoList.add(prjInfo5);

        Project_Information__c prjInfo6 = new Project_Information__c();
        prjInfo6.Marketing_Documents__c = mrkDoc.Id;
        prjInfo6.Features__c = 'Brochures';
        prjInfo6.Property__c = firstProp.Id;
        prjInfo6.URL__c = 'https://damacholding--devpronew--c.cs89.content.force.com/servlet/servlet.FileDownload?file=00P0E0000019Ar6';
        prjInfo6.Label__c = 'Floor Plans';
        projectInfoList.add(prjInfo6);

        Project_Information__c prjInfo7 = new Project_Information__c();
        prjInfo7.Marketing_Documents__c = mrkDoc.Id;
        prjInfo7.Features__c = 'About';
        prjInfo7.Property__c = firstProp.Id;
        prjInfo7.URL__c = 'https://damacholding--devpronew--c.cs89.content.force.com/servlet/servlet.FileDownload?file=00P0E0000019Ar6';
        prjInfo7.Detail__c = 'Floor Plans';
        projectInfoList.add(prjInfo7);

        Project_Information__c prjInfo8 = new Project_Information__c();
        prjInfo8.Marketing_Documents__c = mrkDoc.Id;
        prjInfo8.Features__c = '360Degree WalkThrough';
        prjInfo8.Property__c = firstProp.Id;
        prjInfo8.URL__c = 'https://damacholding--devpronew--c.cs89.content.force.com/servlet/servlet.FileDownload?file=00P0E0000019Ar6';
        projectInfoList.add(prjInfo8);

        insert projectInfoList;
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id,Phone ='1234567',
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com');
            
        List<To_Currency_Values__c> currencylst = new List<To_Currency_Values__c>();
        To_Currency_Values__c obj1 = new To_Currency_Values__c();
        obj1.Label__c='INR';
        obj1.Name='INR';
        currencylst.add(obj1);
        To_Currency_Values__c obj2 = new To_Currency_Values__c();
        obj2.Label__c='AED';
        obj2.Name='AED';
        currencylst.add(obj2);
        To_Currency_Values__c obj3 = new To_Currency_Values__c();
        obj3.Label__c='BAR';
        obj3.Name='BAR';
        currencylst.add(obj3);
        insert currencylst;
        
        List<Currency_Rate__c> currencyconversionlst = new List<Currency_Rate__c>();
        
        Currency_Rate__c currency1 = new Currency_Rate__c();
        currency1.Conversion_Date__c = System.today();
        currency1.Conversion_Rate__c = 0.12;
        currency1.From_Currency__c = 'AED';
        currency1.To_Currency__c = 'INR';
        currency1.IPMS_Row_ID__c= '12345';
        currencyconversionlst .add(currency1);
        
        Currency_Rate__c currency3 = new Currency_Rate__c();
        currency3.Conversion_Date__c = System.today();
        currency3.Conversion_Rate__c = 0.12;
        currency3.From_Currency__c = 'AED';
        currency3.To_Currency__c = 'EUR';
        currency3.IPMS_Row_ID__c= '12348';
        currencyconversionlst .add(currency3);
        
        Currency_Rate__c currency2 = new Currency_Rate__c();
        currency2.Conversion_Date__c = System.today();
        currency2.Conversion_Rate__c = 0.12;
        currency2.From_Currency__c = 'EUR';
        currency2.To_Currency__c = 'AED';
        currency2.IPMS_Row_ID__c= '12346';
        currencyconversionlst.add(currency2);
        
        insert currencyconversionlst ;    

    }
    //method used to insert payment terms related to payment plan
    private static List<Payment_Terms__c> insertPaymentTerms(Id paymentPlanID)
    {
        List<Payment_Terms__c> lstPaymentTerms = new List<Payment_Terms__c>();

        //insert data for payment terms
        Payment_Terms__c objPaymentTerm1 = new Payment_Terms__c();
        objPaymentTerm1.Payment_Plan__c = paymentPlanID;
        objPaymentTerm1.Installment__c = 'DP';
        objPaymentTerm1.Description__c = 'DEPOSIT';
        objPaymentTerm1.Percent_Value__c = '24';
        objPaymentTerm1.Milestone_Event__c = 'Immediate';
        objPaymentTerm1.Line_ID__c = math.random()+'1';
        lstPaymentTerms.add(objPaymentTerm1);

        Payment_Terms__c objPaymentTerm2 = new Payment_Terms__c();
        objPaymentTerm2.Payment_Plan__c = paymentPlanID;
        objPaymentTerm2.Installment__c = 'I001';
        objPaymentTerm2.Description__c = '1ST INSTALLMENT';
        objPaymentTerm2.Percent_Value__c = '0';
        objPaymentTerm2.Milestone_Event__c = 'Deposit';
        objPaymentTerm1.Line_ID__c = math.random()+'2';
        lstPaymentTerms.add(objPaymentTerm2);

        insert lstPaymentTerms;
        return lstPaymentTerms;

        
    }
    
    @isTest static void test_method_two() {

        List<Inventory__c> selInv = [SELECT
                                        id,
                                        Marketing_Name_Doc__c
                                     FROM Inventory__c
                                     WHERE Marketing_Name__c = 'Aurum Villas'];
        //String selInvStr = selInv[0].Id + ','+selInv[1].Id;
        User usr = [Select id
                    FROM User Limit 1];
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.Project_Details'));
        System.currentPageReference().getParameters().put('id', selInv[0].Id);
        System.currentPageReference().getParameters().put('uid',usr.id );
        Test.setMock(HttpCalloutMock.class, new MockHttpDamacOfferServiceResponse ());

        DAMAC_Project_Details ctrl = new DAMAC_Project_Details();
        System.currentPageReference().getParameters().put('nextorprev','1' );
        ctrl.paymentPlanPagination();
        System.currentPageReference().getParameters().put('nextorprev','-1' );
        ctrl.paymentPlanPagination();
        System.currentPageReference().getParameters().put('nextorprevoffer','1' );
        ctrl.offerlstpagination();
        System.currentPageReference().getParameters().put('nextorprevoffer','-1' );
        ctrl.offerlstpagination();
        System.currentPageReference().getParameters().put('totalPriceVar','20000000' );
        System.currentPageReference().getParameters().put('loanPeriodVal','5' );
        System.currentPageReference().getParameters().put('downPayVar','1000000' );
        System.currentPageReference().getParameters().put('interestRateVar','9' );
        ctrl.mortgageCalculation();
        ctrl.getToCurrencyValues();
        System.currentPageReference().getParameters().put('leftorright','1' );
        System.currentPageReference().getParameters().put('UniqueId','00000000021201254125' );
        ctrl.unitfeaturelistpagination();
        ctrl.tocurrencyselected = 'AED';
        ctrl.changecurrency();
        ctrl.tocurrencyselected = 'INR';
        ctrl.changecurrency();
        ctrl.arearange = '';
        ctrl.pricerange = '';
        ctrl.pricepersqfeet = '';
        ctrl.accountMsgIdStr  = '';
        ctrl.inquiryMsgIdStr = '';
        ctrl.selectedMsgObj    = '';
        ctrl.accountEmailIdStr = '';
        DAMAC_Project_Details.isPortalUser = true;
        DAMAC_Project_Details.getInquiries('test');
        DAMAC_Project_Details.getAccounts('test');
        ctrl.linkTracking();
        ctrl.updateLocation();
        Test.stopTest();
    }
    
    @isTest static void test_method_one() {
        Inquiry__c newInquiry = [SELECT Id, Last_Name__c FROM Inquiry__c WHERE Last_Name__c = 'Inquiry'];
        List<Inventory__c> selInv = [SELECT
                                        id,
                                        Marketing_Name_Doc__c
                                     FROM Inventory__c
                                     WHERE Marketing_Name__c = 'Aurum Villas'];
        //String selInvStr = selInv[0].Id + ','+selInv[1].Id;
        User usr = [Select id
                    FROM User Limit 1];
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.Project_Details'));
        System.currentPageReference().getParameters().put('id', selInv[1].Id);
        System.currentPageReference().getParameters().put('uid',usr.id );
        Test.setMock(HttpCalloutMock.class, new MockHttpDamacOfferServiceResponse ());
        DAMAC_Project_Details ctrl = new DAMAC_Project_Details();
        Test.stopTest();
    }

    @isTest static void test_method_three() {

        Inquiry__c newInquiry = [SELECT Id, Last_Name__c FROM Inquiry__c WHERE Last_Name__c = 'Inquiry'];

        List<Inventory__c> selInv = [SELECT
                                        id,
                                        Marketing_Name_Doc__c
                                     FROM Inventory__c
                                     WHERE Marketing_Name__c = 'Aurum Villas'];

        User usr = [Select id
                    FROM User Limit 1];
        Test.setCurrentPageReference(new PageReference('Page.Project_Details'));
        System.currentPageReference().getParameters().put('id', selInv[0].Id);
        System.currentPageReference().getParameters().put('uid',usr.id );
                
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpDamacOfferServiceResponse ());
        DAMAC_Project_Details ctrl = new DAMAC_Project_Details();
        ctrl.shareType = 'email';
        ctrl.shareRecordId = newInquiry.Id;
        ctrl.shareProjectDetails();
        ctrl.tempVal = 'test';
        Boolean isPortalUser = DAMAC_Project_Details.isPortalUser;
        DamacOfferService.parse('{"data": {"nodeQuery": {"entities": [{"entityTranslation": {"entityId": "748",'
                + '"title": "urban living","fieldOfferSubtitle": "Ready apartments",'
                + '"fieldOfferThumbnail": {"url": "test urlpg", "alt": "Merano Tower"},"path":{"alias": "perfect-the-art-of-urban-living"}}}]}}}');
        Test.stopTest();
    }

}