@isTest
global class IPMS_SPA_Mock_AsyncSPA implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals('http://94.200.40.200:8080/IPMSREST/process/3290', req.getEndpoint());
        //System.assertEquals('POST', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
       // res.setBody('{"requestName":"SPA","extRequestId":"2017-11-05 17:50:51-SF Support","extRequestKey":"a0y0Y000000X3zW","requestStart":"05-Nov-2017 21:48:16","requestEnd":"05-Nov-2017 21:51:09","requestStatus":"C","totalRequestLines":1,"results":[{"processId":337972,"lineRequest":"SPA","lineRequestKey":"a0x0Y000001hhvQQAQ","reportName":"SPA","reportStatus":"P","batch":"GEN-SPA-05NOV17-215051","registrationId":"83538","partyId":null,"custTrxId":null,"reportURL":"https://sftest.deeprootsurface.com/docs/a/39984892/83538_SPA.pdf","processMessage":"Process Completed"}]}');
       res.setBody('{"requestName":"SPA","extRequestId":"2017-11-05 17:50:51-SF Support","extRequestKey":"a0y0Y000000X3zW","requestStart":"05-Nov-2017 21:48:16","requestEnd":"05-Nov-2017 21:51:09","requestStatus":"P","totalRequestLines":1,"results":[{"processId":337972,"lineRequest":"SPA","lineRequestKey":"a0x0Y000001hhvQQAQ","reportName":"SPA","reportStatus":"P","batch":"GEN-SPA-05NOV17-215051","registrationId":"83538","partyId":null,"custTrxId":null,"reportURL":"https://sftest.deeprootsurface.com/docs/a/39984892/83538_SPA.pdf","processMessage":"Process Completed"},{"processId":337972,"lineRequest":"SPA","lineRequestKey":"a0x0Y000001hhvQQAQ","reportName":"PROMOTION","reportStatus":"P","batch":"GEN-SPA-05NOV17-215051","registrationId":"83538","partyId":null,"custTrxId":null,"reportURL":"https://sftest.deeprootsurface.com/docs/a/39984892/83538_SPA.pdf","processMessage":"Process Completed"},{"processId":337972,"lineRequest":"SPA","lineRequestKey":"a0x0Y000001hhvQQAQ","reportName":"DP INVOICE","reportStatus":"P","batch":"GEN-SPA-05NOV17-215051","registrationId":"83538","partyId":null,"custTrxId":null,"reportURL":"https://sftest.deeprootsurface.com/docs/a/39984892/83538_SPA.pdf","processMessage":"Process Completed"},{"processId":337972,"lineRequest":"SPA","lineRequestKey":"a0x0Y000001hhvQQAQ","reportName":"OPTIONS","reportStatus":"P","batch":"GEN-SPA-05NOV17-215051","registrationId":"83538","partyId":null,"custTrxId":null,"reportURL":"https://sftest.deeprootsurface.com/docs/a/39984892/83538_SPA.pdf","processMessage":"Process Completed"},{"processId":337972,"lineRequest":"SOA","lineRequestKey":"a0x0Y000001hhvQQAQ","reportName":"SOA","reportStatus":"P","batch":"GEN-SPA-05NOV17-215051","registrationId":"83538","partyId":null,"custTrxId":null,"reportURL":"https://sftest.deeprootsurface.com/docs/a/39984892/83538_SPA.pdf","processMessage":"Process Completed"}]}');
        res.setStatusCode(200);
        return res;
    }
}