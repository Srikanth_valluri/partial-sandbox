@isTest
private class PayfortTransactionFeedbackServiceTest {
    
    @TestSetup
    public static void testData() {
        //custom setting
        insert new PayfortGatewayCreds__c( Merchant_Identifier__c= '17e98f0b'
                                         , Request_Phrase__c= '$2y$10$QzSutSlCY'
                                         , ResponsePhrase__c = '$2y$10$V5IdbyFmy'
                                         , AccessCode__c='wT7xGi6a4fwntXQyq0Gs'
                                         , PurchaseEndpoint__c='https://sbpaymentservices.payfort.com/FortAPI/paymentApi'
                                         , CurrencyInPay__c='AED'
                                         , SiteBaseURL__c='https://partial-servicecloudtrial-155c0807bf-1580afc5db1.cs80.force.com/Customer'
                                         , TokenizationEndpoint__c='https://sbcheckout.PayFort.com/FortAPI/paymentPage'
                                         , Language__c='en' );
    
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(Agency_ID__c = '1234');
        insert sr;
    
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com',
                                       Payfort_Card_Number__c = '418887*****00002',
                                       Payfort_Token_Name__c = 'abcd1234');
        insert account;
    
        Booking__c booking = new Booking__c( Deal_SR__c = sr.Id
                                           , Account__c = account.Id);
        insert booking;
    
        Booking_Unit__c bookingUnit = new Booking_Unit__c( Booking__c = booking.Id
                                                         , Unit_Name__c = 'Test Unit Name'
                                                         , Registration_ID__c = '3901');
        insert bookingUnit;

        Receipt__c receipt = new receipt__c(isPayfortPayment__c = true, Payment_Status__c = 'Failed');
        insert receipt;
    }
    
    @isTest
    private static void testWebservice() { 

        Test.startTest();

        Receipt__c recp = [SELECT id
                                , Name
                                , isPayfortPayment__c
                                , Payment_Status__c
                                , Direct_Transaction_Feedback_Time__c
                                , Payfort_FortID__c
                                , PG_Error_Code__c
                                , Payfort_Payment_type__c
                                , Payfort_No_of_Installments__c
                                , Payfort_Plan_Code__c
                                , Payfort_Issuer_Code__c
                           FROM Receipt__c
                           WHERE isPayfortPayment__c = true];
    
        //PayfortTransactionFeedbackService objClass = new PayfortTransactionFeedbackService();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/PayfortTransactionFeedback';  //Request URL
        req.httpMethod = 'POST';  
        //req.headers = ''
        req.addParameter('response_code','14000');
        req.addParameter('card_holder_name','Testq');
        req.addParameter('signature','9491aa92b286163d5e0c393354b38b1e1e03cc544b0df80085971daea0fc5ced');
        req.addParameter('merchant_identifier','17e98f0b');
        req.addParameter('access_code','wT7xGi6a4fwntXQyq0Gs');
        req.addParameter('customer_ip','114.143.177.210');
        req.addParameter('plan_code','M9mv5Z');
        req.addParameter('number_of_installments','6');
        req.addParameter('language','en');
        req.addParameter('eci','ECOMMERCE');
        req.addParameter('issuer_code','zaQnN1');
        req.addParameter('installments','HOSTED');
        req.addParameter('merchant_reference', 'test');
        req.addParameter('authorization_code','433112');
        req.addParameter('token_name','370584C550AE11EAACB00E2BD5E42CD6');
        req.addParameter('currency','AED');
        req.addParameter('remember_me','YES');
        req.addParameter('amount','500000');
        req.addParameter('card_number','418887******0002');
        req.addParameter('payment_option','VISA');
        req.addParameter('expiry_date','2105');
        req.addParameter('fort_id','158185197900093282');
        req.addParameter('command','PURCHASE');
        req.addParameter('response_message','Success');
        req.addParameter('customer_email','shubham.suryawanshi@eternussolutions.com');
        req.addParameter('status','14');


        RestContext.request = req;
        RestContext.response= res;
        PayfortTransactionFeedbackService.getPayfortTransactionFeedback();

        Test.stopTest();

    }

}