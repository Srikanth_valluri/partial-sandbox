public without sharing class DroppedCallsCallBackCallingHandler {
    public static void onBeforeInsert( List<Calling_List__c> newCallingLst ) {
        updateDroppedCallsCallBackCRENameOnCreation(newCallingLst);
    }
    public static void updateDroppedCallsCallBackCRENameOnCreation( List<Calling_List__c> lstCallingList ) {
        set<Integer> setCRE_Extensions = new set<Integer>();
        set<String> setCRE_ExtensionsFORCS = new set<String>();
        Map<String,String> mapExtensionVsUser  = new Map<String,String>();
        
        Id devRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Dropped Calls - Call Back').getRecordTypeId();
        if (lstCallingList != null && lstCallingList.size() > 0) {
            system.debug('devRecordTypeId : ' + devRecordTypeId);
            for( Calling_List__c objCalling_List : lstCallingList ) {
                if( objCalling_List.RecordTypeId == devRecordTypeId 
                 && objCalling_List.CRE_Extension__c != null ) {
                    setCRE_Extensions.add( ( Integer )objCalling_List.CRE_Extension__c );
                    setCRE_ExtensionsFORCS.add( String.valueOf(objCalling_List.CRE_Extension__c) );
                }
            }
            
            if( setCRE_Extensions != null && setCRE_Extensions.size() > 0 ) {
                for( User objUser : [ SELECT Id,Name,Extension_Unique__c FROM User Where Extension_Unique__c IN: setCRE_Extensions ] ) {
                    if( String.isNotBlank ( objUser.Name ) && objUser.Extension_Unique__c != null ) { 
                        mapExtensionVsUser.put( String.valueOf( objUser.Extension_Unique__c ) , objUser.Name);
                    }
                }
                for( CRE_Pool_Detail__c objCRE_Pool_Detail : [ SELECT Id,Pool_Name__c,Extension__c FROM CRE_Pool_Detail__c Where Extension__c IN: setCRE_ExtensionsFORCS ] ) {
                    if( String.isNotBlank ( objCRE_Pool_Detail.Pool_Name__c ) && String.isNotBlank (objCRE_Pool_Detail.Extension__c ) ) { 
                        mapExtensionVsUser.put( objCRE_Pool_Detail.Extension__c , objCRE_Pool_Detail.Pool_Name__c);
                    }
                }
                
                for( Collection_Payment_Pool__c objCollection_Pool_Detail : [ SELECT Id,Pool_Name__c,Extension__c FROM Collection_Payment_Pool__c Where Extension__c IN: setCRE_ExtensionsFORCS ] ) {
                    if( String.isNotBlank ( objCollection_Pool_Detail.Pool_Name__c ) && String.isNotBlank (objCollection_Pool_Detail.Extension__c) ) { 
                        mapExtensionVsUser.put( objCollection_Pool_Detail.Extension__c , objCollection_Pool_Detail.Pool_Name__c);
                    }
                }
                
                for( Calling_List__c objCalling_List : lstCallingList ) {
                    if( objCalling_List.RecordTypeId == devRecordTypeId 
                     && objCalling_List.CRE_Extension__c != null ) {
                        if( mapExtensionVsUser.containsKey(String.valueOf(objCalling_List.CRE_Extension__c)) ) {
                            objCalling_List.CRE_Name__c = mapExtensionVsUser.get(String.valueOf(objCalling_List.CRE_Extension__c));
                        }
                    }
                }
            }
        }
    }
}