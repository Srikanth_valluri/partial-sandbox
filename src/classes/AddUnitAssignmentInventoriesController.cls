/***********************************************************************************
 * Controller Class : AddUnitAssignmentInventoriesController
 * Created By : Craig Lobo 
 -----------------------------------------------------------------------------------
 * Description : This is a controller class for AddUnitAssignmentInventories
 *
 * Test Data : AddUnitAssignmentInventoriesTest
 -----------------------------------------------------------------------------------
 * Version History:
 * VERSION     DEVELOPER NAME       DATE                DETAIL FEATURES
   1.0         Craig Lobo           12/12/2017        Initial Development
 **********************************************************************************/
public without sharing class AddUnitAssignmentInventoriesController {
    
    public static final String queryString  = ' SELECT '
                                            + ' Id, Name, Property_Name_2__c, Unit_Type__c, '
                                            + ' Marketing_Name_Doc__r.Name, Floor__c, '
                                            + ' Property__c, Unit__c, Floor_Package_Name__c, ' 
                                            + ' UA_Proposal_Price__c, Special_Price__c, Release_ID__c ' // Adding UA_Proposal_Price__c, Special_Price__c to display on Page
                                            + ' FROM Inventory__c '
                                            + ' WHERE Unit_Assignment__c = NULL '
                                            + ' AND Tagged_To_Unit_Assignment__c = false '
                                            + ' AND Is_Assigned__c = false '
                                            + ' AND EOI__c = NULL '
                                            + ' AND Status__c = \'Released\' ';
                                            
    private List<Inventory__c> inventoriesList;
    public List <InventoryWrapper> inventoryWrapperList     {get; set;}
    public String msgType                                   {get; set;}
    public String isDisplay                                 {get; set;}
    public String message                                   {get; set;}
    public String inventoryName                             {get; set;}
    public Integer noOfRecToDisplay                         {get; set;}
    public Id unitAssignemntRecId                           {get; set;}
    public Id startId                                       {get; set;}
    public Id endId                                         {get; set;}
    public Id prevStartId                                   {get; set;}
    public String searchValue                               {get; set;}
    public String autoComplete                              {get; set;}
    public String packageValue                              {get; set;}
    public String floorValue                                {get; set;}
    public String unitValue                                 {get; set;}
    public Integer totalRecs = 0;
    public Integer offsetVal = 0;
    public Integer LimitSize = 10;

    
    /************************************************************************************************
    * @Description : This is Constructor method                                                     *
    * @Params      :                                                                                *
    * @Return      :                                                                                *
    *************************************************************************************************/
    public AddUnitAssignmentInventoriesController(ApexPages.StandardController controller) {
        System.debug('Unit_Assignment__c ID ====> '+controller.getRecord().id);
        init(controller.getRecord().Id);
    }

    public void init(Id unitAssignemntId){
        if (String.isBlank(message)) {
            isDisplay = 'hide';
        }
        msgType = '';
        message = '';
        inventoryName = '';
        searchValue = '';
        packageValue = '';
        floorValue = '';
        unitValue = '';
        autoComplete = 'Package';
        noOfRecToDisplay = 20;
        unitAssignemntRecId = unitAssignemntId;
        inventoryWrapperList = new List <InventoryWrapper>();
        inventoriesList = new List<Inventory__c>();
        firstBtn();
    }

    public void displayComponent() {
        
        if (String.isNotBlank(searchValue)) {
            packageValue = '';
            floorValue = '';
            unitValue = '';
            if (searchValue.equalsIgnoreCase('Package')) {
                autoComplete = 'Package';
            } else if (searchValue.equalsIgnoreCase('Floor')) {
                autoComplete = 'Floor';
            } else if (searchValue.equalsIgnoreCase('Unit')) {
                autoComplete = 'Unit';
            }
        }
    }


    /************************************************************************************************
    * @Description : This method will return the list of InventoryWrapper                           *
    * @Params      : String                                                                         *
    * @Return      :List<InventoryWrapper>                                                          *
    *************************************************************************************************/
    public List<InventoryWrapper> getInvenories(String pQueryString) {
        List<InventoryWrapper> inventoryWrapList = new List<InventoryWrapper>();
        System.debug('pQueryString>>>> ' + pQueryString);
        if (String.isNotBlank(pQueryString)) {
            for (Inventory__c invLoopObj : DataBase.Query(pQueryString)) { 
                InventoryWrapper invWrap = new InventoryWrapper(invLoopObj.Id, invLoopObj, false);
                invWrap.invObj.UA_Proposal_Price__c = invLoopObj.Special_Price__c; // assiging Special price to Proposal Price, Added by Srikanth.
                inventoryWrapList.add(invWrap);
            }
        }
        return inventoryWrapList;

    }

    /************************************************************************************************
    * @Description : This method will add Inventories to Unit Assignment              *
    * @Params      :                                                                                *
    * @Return      : void.....................                                                      *
    *************************************************************************************************/
    public void addInventoriesToUnitAssignment () {
        try {
            Unit_Assignment__c uAsgn =[Select Id,Name, Deal_Value__c,Proposal_Price__c,
                                        CreatedBy.ID, Agency__c, Agency__r.Vendor_id__c,
                                        createdBy.IPMS_Employee_ID__c,
                                        createdBy.Manager.IPMS_Employee_ID__c,
                                        Reason_For_Unit_Assignment__c
                                        from Unit_Assignment__c where id= :unitAssignemntRecId];
            inventoriesList = new List<Inventory__c>();
            for (InventoryWrapper invWrapLoopObj : inventoryWrapperList) { 
                if (invWrapLoopObj.isSelected == true) {
                    Inventory__c invRec = invWrapLoopObj.invObj;
                    
                    invRec.Unit_Assignment__c = unitAssignemntRecId;                   
                    inventoriesList.add(invRec);
                }
            }
            if (!inventoriesList.isEmpty()) {
                update inventoriesList;
                
                aggregateResult[] groupedResults = [SELECT SUM(Selling_Price__c)sumPrice,
                                                     SUM(UA_Proposal_Price__c)sumProposal, // getting the Sum of Proposal Price
                                                     COUNT(ID)childCount
                                                  FROM 
                                                   Inventory__c 
                                                  WHERE 
                                                   Unit_Assignment__c= :unitAssignemntRecId  
                                                  AND 
                                                    Unit_Assignment__c != null
                                                   ];  
                
                for (AggregateResult ar : groupedResults)  {
                    uAsgn.Deal_Value__c = (Decimal)ar.get('sumPrice');
                    uAsgn.Proposal_Price__c = (Decimal)ar.get('sumProposal');  //Updates the aggregate proposal price of inventories on the UA
                    uAsgn.Inventory_Count__c = (Decimal)ar.get('childCount');  //Updates the No. of inventories count on the parent UA
                }
                update uAsgn ;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, System.label.Add_Inventory));
                clearInventories();
            } else {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,System.label.Select_Inventory ));
            }
        } catch (Exception e) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
        }
    }

    /************************************************************************************************
    * @Description : This method will query on ineventories which are not tagged to unit  assignment*
    * @Params      :                                                                                *
    * @Return      : void.....................                                                      *
    *************************************************************************************************/
    public void searchInventories() {
        inventoryWrapperList.clear();

        if (String.isNotBlank(searchValue)) {
            String invQueryString = queryString;
            if (searchValue.equalsIgnoreCase('Package')) {
                if (String.isNotBlank(packageValue)) {
                    invQueryString += ' AND Floor_Package_Name__c = \''
                                    + packageValue
                                    + '\' LIMIT ' 
                                    + noOfRecToDisplay;
                    inventoryWrapperList = getInvenories(invQueryString);
                }
            } else if (searchValue.equalsIgnoreCase('Floor')) {
                if (String.isNotBlank(floorValue)) {
                    invQueryString += ' AND Floor__c  = \''
                                    + floorValue
                                    + '\' LIMIT ' 
                                    + noOfRecToDisplay;
                    inventoryWrapperList = getInvenories(invQueryString);
                }
            } else if (searchValue.equalsIgnoreCase('Unit')) {
                if (String.isNotBlank(unitValue)) {
                    invQueryString += ' AND Unit__c = \''
                                    + unitValue
                                    + '\' LIMIT ' 
                                    + noOfRecToDisplay;
                    inventoryWrapperList = getInvenories(invQueryString);
                }
            }
        }

    }

    /************************************************************************************************
    * @Description : Method to display all Inventories Avaliable                                    *
    * @Params      :                                                                                *
    * @Return      : void                                                                           *
    *************************************************************************************************/
    public void clearInventories() {
        Firstbtn();
    }

    /************************************************************************************************
    * @Description : Method to Display the success/Error messages                                   *
                     depending on their visiblity, Value and Type                                   *
    * @Params      : pDisplay > show/hide, pType > success/error, pMessage > custom message         *
    * @Return      : void                                                                           *
    *************************************************************************************************/
   /*public void messageBlock(String pDisplay, String pType, String pMessage) {
        isDisplay = pDisplay;
        msgType = pType;
        message = pMessage;
    }*/

    /************************************************************************************************
    * @Description : this method will update the page with new page size                            *
    * @Params      :                                                                              *
    * @Return      : void                                                                           *
    *************************************************************************************************/
    public void updatePage() {
        inventoryWrapperList.clear();
        firstBtn();
    }

    /************************************************************************************************
    * @Description : this method will set hte offsetVal to display first page records               *
    * @Params      :                                                                              *
    * @Return      : void                                                                           *
    *************************************************************************************************/
    public void Firstbtn(){
        inventoryWrapperList.clear();
        String invQueryString   = queryString 
                                +  ' ORDER BY Id Asc LIMIT ' 
                                + noOfRecToDisplay;
        inventoryWrapperList = getInvenories(invQueryString);
        if(inventoryWrapperList != null && !inventoryWrapperList.isEmpty()) {
            prevStartId = inventoryWrapperList[0].recordId;
            startId = inventoryWrapperList[0].recordId;
            endId = inventoryWrapperList[inventoryWrapperList.size()-1].recordId;
        }
    }

    /************************************************************************************************
    * @Description : this method will set hte offsetVal to display previous page records            *
    * @Params      :                                                                              *
    * @Return      : void                                                                           *
    *************************************************************************************************/
    public void prvbtn() {
        if(startId != prevStartId ) {
            
            String invQueryString   = queryString 
                                    + ' AND Id < \'' 
                                    + startId 
                                    + '\' AND Id >= \''
                                    + prevStartId 
                                    + '\''
                                    + ' ORDER BY Id Asc LIMIT '
                                    + noOfRecToDisplay;
            List<InventoryWrapper> tempInvWrapper = getInvenories(invQueryString);
            if(tempInvWrapper != null && !tempInvWrapper.isEmpty()) {

                inventoryWrapperList.clear();
                inventoryWrapperList.addAll(tempInvWrapper);
                startId = tempInvWrapper[0].recordId;
                endId = tempInvWrapper[tempInvWrapper.size()-1].recordId;
                tempInvWrapper.clear();
                
                String invQueryString1  = queryString
                                        + ' AND Id < \'' 
                                        + startId 
                                        + '\' ORDER BY Id DESC LIMIT '
                                        + noOfRecToDisplay;
                List<InventoryWrapper> tempInvWrapper1 = getInvenories(invQueryString1);
                if(tempInvWrapper1 != null && tempInvWrapper1.isEmpty() == false) {
                   prevStartId = tempInvWrapper1[tempInvWrapper1.size() - 1].recordId;
                }
                else {
                    prevStartId = inventoryWrapperList[0].recordId;
                }
            }
        }
    }

    /************************************************************************************************
    * @Description : this method will set hte offsetVal to display next page records                *
    * @Params      :                                                                              *
    * @Return      : void                                                                           *
    *************************************************************************************************/
    public void Nxtbtn() {
        prevStartId = inventoryWrapperList[0].invObj.Id;
        
        String invQueryString   = queryString
                                + ' AND Id > \'' 
                                + endId 
                                + '\'' 
                                + ' ORDER BY Id Asc LIMIT ' 
                                + noOfRecToDisplay;
        List<InventoryWrapper> tempInvWrapper = getInvenories(invQueryString);
        if(tempInvWrapper != null && !tempInvWrapper.isEmpty()) {
            startId = tempInvWrapper[0].recordId;
            endId = tempInvWrapper[tempInvWrapper.size()-1].recordId;
            inventoryWrapperList.clear();
            inventoryWrapperList.addAll(tempInvWrapper);
        }
    }

    /************************************************************************************************
    * @Description : this method will set hte offsetVal to display last page records                *
    * @Params      :                                                                              *
    * @Return      : void                                                                           *
    *************************************************************************************************/
    public void LastBtn() {
       
        String invQueryString   = queryString 
                                + ' ORDER BY Id Desc LIMIT '
                                + noOfRecToDisplay 
                                + noOfRecToDisplay;
        List<InventoryWrapper> tempInvWrapper = getInvenories(invQueryString);
        if(tempInvWrapper.isEmpty() == false && tempInvWrapper.size() >= noOfRecToDisplay) {
            string endId = tempInvWrapper[0].recordId;
            string strtId = tempInvWrapper[noOfRecToDisplay-1].recordId;
            prevStartId = tempInvWrapper[tempInvWrapper.size() - 1].recordId;
            
            String invQueryString1  = queryString 
                                    + ' AND Id >= \''
                                    + strtId 
                                    + '\' AND Id <= \'' 
                                    + endId 
                                    + '\' ORDER BY Id Asc LIMIT ' 
                                    + noOfRecToDisplay;
            inventoryWrapperList = getInvenories(invQueryString1);
        }
        if(inventoryWrapperList != null && inventoryWrapperList.isEmpty() == false) {
            startId = inventoryWrapperList[0].recordId;
            endId = inventoryWrapperList[inventoryWrapperList.size()-1].recordId;
        }
    }

    public Class InventoryWrapper {
        public Id recordId                                  {get; set;}
        public Inventory__c invObj                          {get; set;}
        public Boolean isSelected                           {get; set;}

        public InventoryWrapper(Id pRecordId, Inventory__c pInvObj, Boolean pIsSelected) {
            recordId = pRecordId;
            invObj = pInvObj;
            isSelected = pIsSelected;
        }
    }

}