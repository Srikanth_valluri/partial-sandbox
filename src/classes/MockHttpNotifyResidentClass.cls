@isTest 
global class MockHttpNotifyResidentClass implements HttpCalloutMock{

    global HTTPResponse respond(HTTPRequest req){
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type','application/json');
        res.setBody('{"Success": "OK:"}');
        return res;
    }
}