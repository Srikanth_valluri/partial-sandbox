public with sharing class UnitPaymentTermsController {
    private static final Map<String, String> PAYMENT_ATTRIBUTE_MAP = new Map<String, String> {
        'ATTRIBUTE1' => 'RegistrationId',
        'ATTRIBUTE2' => 'CustomerId',
        'ATTRIBUTE3' => 'CustomerName',
        'ATTRIBUTE4' => 'DocumentNumber',
        'ATTRIBUTE5' => 'DocumentType',
        'ATTRIBUTE6' => 'DocDate',
        'ATTRIBUTE7' => 'DueDate',
        'ATTRIBUTE8' => 'Description',
        'ATTRIBUTE9' => 'Debit',
        'ATTRIBUTE10' => 'Credit'
    };

    @RemoteAction
    public static List<Object> getPaymentTerms(String registrationId) {
        String strPaymentsMade = CustomerPaymentMade.getCustomerPaymentMade(registrationId);
        if (String.isBlank(strPaymentsMade)) {
            strPaymentsMade = '{"data" : []}';
        }
        Map<String, Object> paymentsMadeResponse = (Map<String, Object>) JSON.deserializeUntyped(strPaymentsMade);

        List<Object> paymentsMade = new List<Object>();
        Object paymentsMadeObject = paymentsMadeResponse.get('data');
        if (paymentsMadeObject != NULL) {
            paymentsMade = (List<Object>) paymentsMadeObject;
        }

        for (Object paymentMade : paymentsMade) {
            Map<String, Object> paymentMadeMap = (Map<String, Object>) paymentMade;
            for (String attribute : PAYMENT_ATTRIBUTE_MAP.keySet()) {
                paymentMadeMap.put(
                    PAYMENT_ATTRIBUTE_MAP.get(attribute), String.valueOf(paymentMadeMap.get(attribute))
                );
            }
        }
        System.debug(paymentsMade);
        return paymentsMade;
    }

    public class CustomerPaymentsMade{
        public Payment[] data;
        public String message;
        public String status;
    }

    public class Payment {
        public String ATTRIBUTE1;
        public String ATTRIBUTE2;
        public String ATTRIBUTE3;
        public String ATTRIBUTE4;
        public String ATTRIBUTE5;
        public String ATTRIBUTE6;
        public String ATTRIBUTE7;
        public String ATTRIBUTE8;
        public Decimal ATTRIBUTE9;
        public Decimal ATTRIBUTE10;
    }
}