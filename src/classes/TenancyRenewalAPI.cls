/**********************************************************************************************************************
Description: This API is used for creating Tenancy Renewal FM Case raised from Damac Living App
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By      | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   23-10-2020      | Anand Venkitakrishnan | Created initial draft
1.1     |   11-11-2020      | Shubham Suryawanshi   | Changed the sr_type text to sentence case. (as per bug 2815)
***********************************************************************************************************************/

@RestResource(urlMapping='/InsertTenancyRenewal/*')
global class TenancyRenewalAPI {
    public static String errorMsg;
    public static Integer statusCode;
    public static String unitRegId;
    public static String partyId;
    
    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong',
        7 => 'Service charges overdue!'
  };
  
    @HttpPost
    global static FinalReturnWrapper apiMainMethod() {
        RestRequest req = RestContext.request;
        Blob body = req.requestBody;
        String jsonString = body.toString();
        System.debug('jsonString:'+jsonString);
        System.debug('Request params:'+req.params);
        
        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        FmCaseWrapper objFmWrap = new FmCaseWrapper(); 
        cls_meta_data objMeta = new cls_meta_data();
        cls_data objData = new cls_data();
        
        if(String.isBlank(jsonString)) {
            objMeta = ReturnMetaResponse('No JSON body found in request', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        
        System.debug('jsonString:'+jsonString);
        objFmWrap = (FmCaseWrapper)JSON.deserialize(jsonString, FmCaseWrapper.class);
        System.debug('objFmWrap:'+objFmWrap);
        
        if(objFmWrap.action == null || (!objFmWrap.action.equalsIgnoreCase('draft') && !objFmWrap.action.equalsIgnoreCase('submit') )) {
            objMeta = ReturnMetaResponse('Invalid parameter value in : action', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        
        Boolean isTenant = false;
        
        if(String.isNotBlank(objFmWrap.account_id)) {
            List<User> usr = [SELECT id
                              , Name 
                              , Username
                              , Profile.Name 
                              , Contact.AccountId 
                              FROM User
                              WHERE Contact.AccountId =: objFmWrap.account_id];
            System.debug('usr:'+usr);
            isTenant = usr[0].profile.Name.contains('Tenant Community') ? true : false;
        }
        else {
            objMeta = ReturnMetaResponse('No account_id passed in request body', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        System.debug('isTenant:'+isTenant);
        
        if(objFmWrap != null) {
            System.debug('action:'+objFmWrap.action);
            if(objFmWrap.action.equalsIgnoreCase('submit') && String.isBlank(objFmWrap.id)) {
                objMeta = ReturnMetaResponse('No FM Case id passed in the request', 3);
                returnResponse.meta_data = objMeta;
                return returnResponse;
            }
            
            if(objFmWrap.action.equalsIgnoreCase('submit') && String.isNotBlank(objFmWrap.id)) {
                String missingDocName = verifyMandatoryDocs(objFmWrap.id);
                if(!String.isEmpty(missingDocName)) {
                    String errorMsg = missingDocName + ' upload pending';
                    objMeta = ReturnMetaResponse(errorMsg, 2);
                    returnResponse.meta_data = objMeta;
                    return returnResponse;
                }
            }
            
            if(String.isNotBlank(objFmWrap.passport_number) && objFmWrap.passport_number.length() > 13) {
                objMeta = ReturnMetaResponse('Passport number cannot be more than 13-digits', 3);
                returnResponse.meta_data = objMeta;
                return returnResponse;
            }
            
            if(String.isNotBlank(objFmWrap.email_address) && !validateEmailPattern(objFmWrap.email_address)) {
                objMeta = ReturnMetaResponse('Please enter a valid email address', 3);
                returnResponse.meta_data = objMeta;
                return returnResponse;
            }
            
            if(!objFmWrap.additional_members.isEmpty() && objFmWrap.additional_members.size() > 0) {
                for(cls_additional_members objMem : objFmWrap.additional_members) {
                    if(String.isNotBlank(objMem.member_email_address) && !validateEmailPattern(objMem.member_email_address)) {
                        objMeta = ReturnMetaResponse('Please enter a valid email addresss', 3);
                        returnResponse.meta_data = objMeta;
                        return returnResponse;
                    }   
                }    
            }
            
            if(!objFmWrap.emergency_contact_details.isEmpty() && objFmWrap.emergency_contact_details.size() > 0) {
                for(cls_emergency_contact objMem : objFmWrap.emergency_contact_details) {
                    if(String.isNotBlank(objMem.emergency_email_address) && !validateEmailPattern(objMem.emergency_email_address)) {
                        objMeta = ReturnMetaResponse('Please enter a valid email addresss', 3);
                        returnResponse.meta_data = objMeta;
                        return returnResponse;
                    }   
                }      
            }
            
            FM_Case__c objFM = processFMCase(objFmWrap, objFmWrap.action);
            System.debug('objFM:'+objFm);
            
            System.debug('errorMsg:'+errorMsg);
            System.debug('statusCode:'+statusCode);
            objMeta = ReturnMetaResponse(errorMsg, statusCode);
            
            List<FM_Case__c> tenancyRenewalDetails = objFM != null ? GetTenantRegistrationCase_API.getFMCase(objFM.id) : null;
            System.debug('tenancyRenewalDetails:'+tenancyRenewalDetails);
            
            if(tenancyRenewalDetails != null) {
                objData.sr_id = tenancyRenewalDetails[0].id;
                objData.sr_number = tenancyRenewalDetails[0].Name;
                objData.sr_type = 'Tenancy renewal'; //as per bug 2815
                objData.sr_status = tenancyRenewalDetails[0].Status__c;
                objData.sr_submission_date = tenancyRenewalDetails[0].Submission_Date__c != null ? Datetime.newInstance(tenancyRenewalDetails[0].Submission_Date__c.year(), tenancyRenewalDetails[0].Submission_Date__c.month(), tenancyRenewalDetails[0].Submission_Date__c.day()).format('yyyy-MM-dd') : '';
                objData.fm_case = GetTenantDetailsAPI.fetchTenantRegFmCase(tenancyRenewalDetails, true);
            }
            else {
                objMeta = String.isNotBlank(errorMsg) && statusCode != 1 ? ReturnMetaResponse(errorMsg, statusCode) : ReturnMetaResponse('No FM Case found/Created', 2);
            }
        }
        
        returnResponse.data = objData;
        returnResponse.meta_data = objMeta;
        System.debug('returnResponse:'+returnResponse);
        
        return returnResponse;   
    }
    
    public static boolean validateEmailPattern(String email) {

        System.debug('email:: ' + email);
        Boolean isEmailValid = Pattern.matches('[a-zA-Z0-9._-]+@[a-zA-Z]+.[a-zA-Z]{2,4}[.]{0,1}[a-zA-Z]{0,2}', email) ? true : false;

        System.debug('isEmailValid:: ' + isEmailValid);
        return isEmailValid;
    }
    
    public static boolean isServiceFeeDuePending(Booking_Unit__c objBU) {
        Boolean isDuePresent;
        FmIpmsRestServices.DueInvoicesResult dues = FmIpmsRestServices.getDueInvoices(objBU.Registration_Id__c, objBU.Booking__r.Account__r.Party_ID__c, objBU.Property_Name__c);
        System.debug('-->> Invoice dues.....'+ dues.dueAmountExceptCurrentQuarter);
        
        //Added the CM_Units, ByPassFMReminder__c, SC_From_RentalIncome__c check - Shubham 11/03/2020
        System.debug('due in label :'+label.ServiceChargeDueLimitInAmenityBooking);
    
        Decimal dueLimit = Decimal.valueOf(label.ServiceChargeDueLimitInAmenityBooking);
        System.debug('dueLimit: ' + dueLimit);
    
        //The CM_Units, ByPassFMReminder__c, SC_From_RentalIncome__c check isremoved as per discussed with Shinu- Shubham 01/10/2020 
        if(dues.dueAmountExceptCurrentQuarter == null || dues.dueAmountExceptCurrentQuarter < dueLimit ) {
            isDuePresent = false;
        }
        else {
            isDuePresent = true;
        }
    
        System.debug('isDuePresent :'+isDuePresent);
        return isDuePresent;
    }
    
    public static String verifyMandatoryDocs(String fmCaseId ) {
        System.debug('Inside VerifyMandatoryDocs');
        System.debug('fmCaseId: ' + fmCaseId);
        
        Boolean isDocsValid = false;
        String missingDocName = '';
        
        List<String> onlyMandatoryDocs = new List<String>();
        onlyMandatoryDocs.add('passport');
        onlyMandatoryDocs.add('ejari tenancy registration certificate/lease agreement');
    onlyMandatoryDocs.add('emirates id/visa');
        onlyMandatoryDocs.add('dewa/addc registration certificate');
        
        List<SR_Attachments__c> insertedAttachments = [SELECT Name
                                                       FROM SR_Attachments__c
                                                       WHERE FM_Case__c =: fmCaseId];
        
        Set<String> uploadedDocs = new Set<String>();
        
        for(SR_Attachments__c objA : insertedAttachments) {
            uploadedDocs.add(objA.Name.toLowerCase());
        }
        
        System.debug('uploadedDocs:'+uploadedDocs);
        System.debug('onlyMandatoryDocs:'+onlyMandatoryDocs);
        
        for(String objReqDoc : onlyMandatoryDocs) {
            isDocsValid = uploadedDocs.contains(objReqDoc);
            if(!isDocsValid) {
                missingDocName = objReqDoc;
                break;
            } 
        }
        System.debug('isDocsValid:'+isDocsValid);
        
        if(Test.isRunningTest()) {
            return '';
        }
        
        return missingDocName;
    }
    
    public Static FM_Case__c processFMCase(FmCaseWrapper objFmCaseWrap, String action) {
        List<Booking_Unit__c> unit = new List<Booking_Unit__c>();
        
        if(String.isNotBlank(objFmCaseWrap.booking_unit_id)) {
            unit = [SELECT Id
                    , Unit_Name__c
                    , Booking__r.Account__c
                    , Booking__r.Account__r.Party_Id__c
                    , Registration_Id__c
                    , Property_Name__c
                    , Property_City__c
                    , Resident__c
                    , Tenant__c
                    FROM Booking_Unit__c
                    WHERE Id = :objFmCaseWrap.booking_unit_id
                    LIMIT 1 ];
        }
        else {
            errorMsg = 'Missing parameter value : booking_unit_id in JSON request body';
            statusCode = 3;
            return null;
        }
        
        if(unit.isEmpty() && unit.size() < 1) {
            errorMsg = 'No booking unit found for given booking_unit_id';
            statusCode = 3;
            return null;
        }
        else {
            Boolean isDuePending = false;
            
            if(!Test.isRunningTest()) {
                isDuePending = isServiceFeeDuePending(unit[0]);
            }
            
            if(isDuePending) {
                errorMsg = 'Please pay your service charges and then proceed with your tenancy renewal';
                statusCode = 7;
                return null;
            }
        }
        
        if(String.isBlank(objFmCaseWrap.account_id)) {
            errorMsg = 'No account_id found in request';
            statusCode = 3;
            return null;
        }
        
        Account objAcc = getCurrentAccountDetails(objFmCaseWrap.account_id);
        System.debug('objAcc:'+objAcc);
        
        unitRegId = unit[0].Registration_Id__c;
        partyId = unit[0].Booking__r.Account__r.Party_Id__c;
        
        Location__c objLoc = new Location__c();
        List<Location__c> lstLocs = new list<Location__c>();
        lstLocs = [SELECT Community_Name__c
                   , Move_In_Move_Out_security_cheque_amount__c
                   , Name
                   , ( SELECT FM_User__c
                      , FM_User__r.Email
                      , FM_Role__c
                      , FM_User__r.Name
                      FROM FM_Users__r )
                   FROM Location__c
                   WHERE Name =: unit[0].Unit_Name__c.substringBefore('/')
                   LIMIT 1 ];
        
        if(lstLocs.size() > 0) {
            objLoc = lstLocs[0];
        }
        
        System.debug('objFmCaseWrap:: ' + objFmCaseWrap);
        FM_Case__c objCase = new FM_Case__c();
        
        if(String.isNotBlank(objFmCaseWrap.id)) {       //When case is already created
            objCase.id = objFmCaseWrap.id;
        }
        
        //Populating common entities
        objCase.isHelloDamacAppCase__c = true;    //Check if needed
        objCase.Raised_Date__c = System.now();
        objCase.Origin__c='Portal';          //Check if correct
        objCase.Status__c='New';
        Id devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Renewal').getRecordTypeId();
        objCase.RecordTypeId = devRecordTypeId;
        Boolean isTenant = unit[0].Booking__r.Account__c == unit[0].Resident__c ? false : true;
        objCase.Initiated_by_tenant_owner__c = 'tenant';
        objCase.Request_Type_DeveloperName__c = 'Tenant_Renewal_T';
        objCase.Building_Name__c = objLoc != null ? objLoc.Name : null;
        objCase.Request_Type__c = 'Tenant Renewal';
        objCase.Booking_Unit__c  = unit[0].id;
        objCase.Account__c = unit[0].Booking__r.Account__c;
        //objCase.Account__c = objFmCaseWrap.account_id;
        objCase.Tenant__c = objFmCaseWrap.account_id;
        
        if(action.equalsIgnoreCase('draft') && String.isBlank(objFmCaseWrap.id)) {
            objCase.Status__c = 'New';
            System.debug('Before inserting new');
            insert objCase;
            System.debug('After inserting new');
        }
        if(action.equalsIgnoreCase('draft')) {
            objCase.Status__c = 'In Progress';
        }
        
        //Check if block is correct
        if(action.equalsIgnoreCase('submit')) {
            if(objLoc != NULL && objLoc.FM_Users__r.size() > 0) {
                for(FM_User__c obj: objLoc.FM_Users__r) {
                    if(obj.FM_Role__c == 'FM Admin') {
                        objCase.Admin__c = obj.FM_User__c;
                        // break;
                    }
                    else if(obj.FM_Role__c == 'Property Manager') {
                        objCase.Property_Manager_Email__c = obj.FM_User__r.Email;
                        objCase.Property_Manager_Name__c = obj.FM_User__r.Name;
                        // break;
                    }
                }
            }
            
            List<FM_Approver__mdt> lstApprovers = FM_Utility.fetchApprovers(objCase.Request_Type_DeveloperName__c,unit[0].Property_City__c);
            System.debug('==lstApprovers==' + lstApprovers);
            
            String approvingRoles = '';
            for(FM_Approver__mdt mdt : lstApprovers) {
                approvingRoles += mdt.Role__c + ',';
            }
            approvingRoles = approvingRoles.removeEnd(',');
            objCase.Approving_Authorities__c = approvingRoles;
            
            objCase.Account__c = unit[0].Booking__r.Account__c;
            objCase.Status__c = 'Submitted';
            objCase.Approval_Status__c='Pending';
            objCase.Tenant_Email__c = objAcc != null ? objAcc.Email__pc : null;
            objCase.Submit_for_Approval__c = true;   
            objCase.Submitted__c = true;
        }
        
        objCase = PopulateFMCaseDetails(objCase,objFmCaseWrap);
        
        try {
            System.debug('objCase before insert: ' + objCase);
            upsert objCase;
            
            errorMsg = 'Successful';
            statusCode = 1;
            System.debug('objCase after insert: ' + objCase);
        }
        catch(Exception e) {
            System.debug('Exception: ' + e.getMessage());
            errorMsg = e.getMessage();
            statusCode = 6;
        }
        
        //Populating FM_Additional_Detail__c
        List<FM_Additional_Detail__c> lstFmAdd = new List<FM_Additional_Detail__c>();
        lstFmAdd = PopulateFMAddDetails(objCase, objFmCaseWrap);
        
        if(lstFmAdd.size() > 0) {
            try {
                upsert lstFmAdd;
                errorMsg = 'Successful';
                statusCode = 1;
            }
            catch(Exception e) {
                System.debug('Exception: ' + e.getMessage());
                errorMsg = e.getMessage();
                statusCode = 6;
            }
        }
        
        return objCase;
    }
    
    public static Account getCurrentAccountDetails(String accId) {
        
        if(String.isBlank(accId)) {
            return null;
        }
        
        List<Account> acc = [SELECT id 
                             , Name 
                             , IsPersonAccount 
                             , Email__pc
                             , Email__c 
                             , Party_Id__c
                             FROM Account 
                             WHERE id =: accId];
        return acc.size() > 0 ? acc[0] : null;
    }
  
    /*For Populating FM Case*/
    public static FM_Case__c PopulateFMCaseDetails(FM_Case__c objCase, FmCaseWrapper objFmCaseWrap) {
        objCase.First_Name__c = String.isNotBlank(objFmCaseWrap.first_name) ? objFmCaseWrap.first_name : '';
        objCase.Last_Name__c = String.isNotBlank(objFmCaseWrap.last_name) ? objFmCaseWrap.last_name : '';
        objCase.Gender__c = String.isNotBlank(objFmCaseWrap.gender) ? objFmCaseWrap.gender : '';
        
        objCase.Date_of_Birth__c = String.isNotBlank(objFmCaseWrap.date_of_birth) ? Date.valueOf(objFmCaseWrap.date_of_birth) : null;
        objCase.Nationality__c = String.isNotBlank(objFmCaseWrap.nationality) ? objFmCaseWrap.nationality : '';
        objCase.Passport_Number__c = String.isNotBlank(objFmCaseWrap.passport_number) ? objFmCaseWrap.passport_number : '';
        objCase.Expiry_Date__c = String.isNotBlank(objFmCaseWrap.passport_expiry_date) ? Date.valueOf(objFmCaseWrap.passport_expiry_date) : null;
        objCase.Emirates_Id__c = String.isNotBlank(objFmCaseWrap.emirates_id) ? objFmCaseWrap.emirates_id : '';
    
        objCase.Mobile_Country_Code__c = String.isNotBlank(objFmCaseWrap.mobile_code) ? objFmCaseWrap.mobile_code : '';
        objCase.Mobile_no__c = String.isNotBlank(objFmCaseWrap.mobile_number) ? objFmCaseWrap.mobile_number : '';
        objCase.Tenant_Email__c = String.isNotBlank(objFmCaseWrap.email_address) ? objFmCaseWrap.email_address : '';
        objCase.No_of_Adults__c = String.isNotBlank(objFmCaseWrap.no_of_adults) ? objFmCaseWrap.no_of_adults : '';
        objCase.No_of_Children__c = String.isNotBlank(objFmCaseWrap.no_of_children) ? objFmCaseWrap.no_of_children : '';
        objCase.isPersonWithSpecialNeeds__c = objFmCaseWrap.is_person_with_spl_needs == null ? false : objFmCaseWrap.is_person_with_spl_needs;
        objCase.isHavingPets__c = objFmCaseWrap.is_having_pets == null ? false : objFmCaseWrap.is_having_pets;
    
        objCase.Start_Date__c = String.isNotBlank(objFmCaseWrap.lease_start_date) ? Date.valueOf(objFmCaseWrap.lease_start_date) : null;
        objCase.End_Date__c = String.isNotBlank(objFmCaseWrap.lease_end_date) ? Date.valueOf(objFmCaseWrap.lease_end_date) : null;
        objCase.Ejari_Number__c = String.isNotBlank(objFmCaseWrap.ejari_number) ? objFmCaseWrap.ejari_number : '';

        return objCase;
    }
    
    public static List<FM_Additional_Detail__c> PopulateFMAddDetails( FM_Case__c objCase, FmCaseWrapper objFmCaseWrap) {
        
        List<FM_Additional_Detail__c> lstFmAdd = new List<FM_Additional_Detail__c>();
        
        //Emergency Contact
        if(String.isNotBlank(objCase.Id) && objFmCaseWrap.emergency_contact_details.size() > 0) {
            for(cls_emergency_contact objCont : objFmCaseWrap.emergency_contact_details) {
                System.debug('objCont: ' + objCont);
                if(String.isNotblank(objCont.emergency_email_address) ||
                   String.isNotblank(objCont.emergency_full_name) ||
                   String.isNotblank(objCont.emergency_mobile_number) ||
                   String.isNotblank(objCont.emergency_relationship) ) {
                       
                       FM_Additional_Detail__c objDetContact = new FM_Additional_Detail__c();
                       
                       if(String.isNotBlank(objCont.id)) {
                           objDetContact.id = objCont.id;
                       }
                       
                       objDetContact.RecordTypeId = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
                       //objDetContact.Registration_Country__c = 'United Arab Emirates';
                       objDetContact.Emergency_Contact_Case__c = objCase.Id ;
                       objDetContact.Emergency_Contact_Account__c = objCase.Tenant__c;
                       
                       objDetContact.Name__c = objCont.emergency_full_name;
                       objDetContact.Relationship__c = objCont.emergency_relationship;
                       objDetContact.Email__c = objCont.emergency_email_address;
                       //objDetContact.
                       objDetContact.Mobile__c = objCont.emergency_mobile_number;
                       
                       System.debug('objDetContact: ' +objDetContact);
                       lstFmAdd.add(objDetContact);
                }
            }
        }
        
        //Resident Details
        if(String.isNotBlank(objCase.Id) && objFmCaseWrap.additional_members.size() > 0) {
            for(cls_additional_members objRes : objFmCaseWrap.additional_members) {
                System.debug('objRes: ' + objRes);                
                if(String.isNotBlank(objRes.member_first_name) ||
                   String.isNotBlank(objRes.member_last_name) ||
                   String.isNotBlank(objRes.member_gender) ||
                   String.isNotBlank(objRes.member_date_of_birth) ||
                   String.isNotBlank(objRes.member_nationality) ||
                   String.isNotBlank(objRes.member_passport_number) ||
                   String.isNotBlank(objRes.member_emirates_id) ||
                   String.isNotBlank(objRes.member_mobile_number) ||
                   String.isNotBlank(objRes.member_email_address) ) {
                       
                       FM_Additional_Detail__c objResident = new FM_Additional_Detail__c();
                       
                       if(String.isNotBlank(objRes.id)) {
                           objResident.id = objRes.id;
                       }
                       
                       objResident.RecordTypeId = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Resident').getRecordTypeId();
                       //objResident.Registration_Country__c = 'United Arab Emirates';
                       objResident.Resident_Case__c = objCase.Id;
                       //objDet.Resident_Account__c = objCase.Tenant__c;
                       
                       String memberName  = String.isNotBlank(objRes.member_first_name) ? objRes.member_first_name : ''; 
                       memberName += ' ';
                       memberName += String.isNotBlank(objRes.member_last_name) ? objRes.member_last_name : ''; 
                       objResident.Name__c = memberName;
                       objResident.Nationality__c = objRes.member_nationality;
                       objResident.Gender__c = objRes.member_gender;
                       objResident.Date_of_Birth__c = String.isNotBlank(objRes.member_date_of_birth ) ? Date.valueOf(objRes.member_date_of_birth)  : null;
                       objResident.Passport_Number__c = objRes.member_passport_number;
                       objResident.Identification_Number__c = objRes.member_emirates_id;
                       objResident.Mobile__c = objRes.member_mobile_number;
                       objResident.Email__c = objres.member_email_address;
                       
                       System.debug('objResident: ' + objResident);
                       lstFmAdd.add(objResident);
              }
            }
        }
        
        return lstFmAdd;
    }
    
    public static cls_meta_data ReturnMetaResponse(String message, Integer statusCode) {
        cls_meta_data retMeta = new cls_meta_data();
        retMeta.message = message;
        retMeta.status_code = statusCode;
        retMeta.title = mapStatusCode.get(statusCode);
        retMeta.developer_message = null;
        return retMeta;
    }
      
    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }
    
    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message; 
    }
    
    public class cls_data {
        public String sr_id;
        public String sr_number;
        public String sr_type;
        public String sr_status;
        public String sr_submission_date;
        public GetTenantDetailsAPI.FmCaseWrapper fm_case;
    }
    
    public class FmCaseWrapper {
        public String action;
        public String id;
        public String booking_unit_id;
        public String account_id;
        
        public String first_name;
        public String last_name;
        public String gender;
        public String date_of_birth;
        public String no_of_adults;
        public String no_of_children;
        public Boolean is_person_with_spl_needs;
        public Boolean is_having_pets;
        
        public String nationality;
        public String passport_number;
        public String passport_expiry_date;
        public String emirates_id;
        
        public String mobile_code;
        public String mobile_number;
        public String email_address;
        
        public String lease_start_date;
        public String lease_end_date;
        public String ejari_number;
        
        public cls_additional_members[] additional_members;
        
        public cls_emergency_contact[] emergency_contact_details;
    }
    
    public class cls_additional_members {
        public String id;   //new
        public String member_first_name;
        public String member_last_name;
        public String member_gender;
        public String member_date_of_birth;
        public String member_nationality;
        public String member_passport_number;
        //public String member_passport_expiry_date;
        public String member_emirates_id;
        //public String member_country_code;
        public String member_mobile_number;
        public String member_email_address;
    }
    
    public class cls_emergency_contact {
        public String id;
        public String emergency_full_name;
        public String emergency_relationship;
        public String emergency_email_address;
        public String emergency_mobile_code;
        public String emergency_mobile_number;
    }
    
    public class cls_attachment {
        public String id;
        public String name;
        public String attachment_url;
        public String file_extension;
    }
}