/*****************************************************************************************
* Description - Test class developed for PassportExtractionServiceController
*
* Version            Date            Author            Description
* 1.0                12/12/17        Naresh (accely)           Initial Draft
*******************************************************************************************/

@isTest
public class PassportExtractionServiceControllerTest{

  public static testmethod void getTestInnerClass(){
    
     PassportExtractionServiceController obj =  new PassportExtractionServiceController();
     
     PassportExtractionServiceController.PassportDetails objPassportDetails =  new PassportExtractionServiceController.PassportDetails();
     objPassportDetails.text = 'text';
     objPassportDetails.name = 'name'; 
      
     List<PassportExtractionServiceController.PassportDetails> obPass =  new List<PassportExtractionServiceController.PassportDetails>();
      
     PassportExtractionServiceController.data objdata =  new PassportExtractionServiceController.data();
     objdata.fields = obPass; 
     objdata.message = 'Test Message'; 
     objdata.status = 'S'; 
     objdata.result =  'result';
     System.assert(objdata != null); 
  }


  public static testmethod void getPassportServiceDetailsTest(){
      test.startTest();
      
       SOAPCalloutServiceMock.returnToMe = new Map<String,PassportExtractionService.PassportServiceBase64Response_element>();
       PassportExtractionService.PassportServiceBase64Response_element response_x = new PassportExtractionService.PassportServiceBase64Response_element();
       response_x.return_x = '{'+
		'"fields":["text", "name"],'+
		'"message  ":"message",'+
		'"status ":"S",'+
		'"result ": "result"'+
		''+  
		'}'+
		'';
       SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
       Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
       PassportExtractionServiceController.getPassportServiceDetails('filename' , 'filename');
       System.assert((PassportExtractionServiceController.getPassportServiceDetails('filename' , 'filename')) != null);
       test.stopTest();
        
  }

}