/**
 * Description: Test class for ClientAcknowledgmentFormDrwaloopCntrl 
 */
@isTest
private class ClientAcknowledgmentFormDDPCntrlTest {

    /*
     * Description : Test method to check whether the batch is called or not which generates the Drawloop document.
     */
    @isTest static void testMethod1() {
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
        OnOffCheck__c = true);
        settingLst2.add(newSetting1);
        insert settingLst2;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
          objBookingUnit.Okay_to_release_keys__c = true;
          objBookingUnit.Keys_Released__c = true;
          objBookingUnit.Is_Key_Release_Requested__c = true;

        }
        insert bookingUnitList;


        PageReference redirectionUrl;
        Test.startTest();
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(bookingUnitList[0]);
            ClientAcknowledgmentFormDrwaloopCntrl objController = new ClientAcknowledgmentFormDrwaloopCntrl(standardControllerInstance);
            Test.setCurrentPageReference(new PageReference('Page.Client_Acknowledgment_Form_Drawloop'));
            System.currentPageReference().getParameters().put('Id', bookingUnitList[0].Id);
            
            bookingUnitList[0].Okay_to_release_keys__c = true;
            bookingUnitList[0].Keys_Released__c = true;
            bookingUnitList[0].OK_to_release_Keys_LHO__c  = true;
            bookingUnitList[0].Keys_Released_LHO__c = true;
            update bookingUnitList;
            System.currentPageReference().getParameters().put('Id', bookingUnitList[0].Id);
            redirectionUrl = objController.generateClientAcknowledgmentLetter();

            
        Test.stopTest();

        // Check whether redirected to case detail page
        System.assert(redirectionUrl != null);
        System.assertEquals(redirectionUrl.getURL(), '/'+bookingUnitList[0].Id);
    }
    
    @isTest static void testMethod2() {
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
     
        insert bookingUnitList;


        PageReference redirectionUrl;
        Test.startTest();
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(bookingUnitList[0]);
            ClientAcknowledgmentFormDrwaloopCntrl objController = new ClientAcknowledgmentFormDrwaloopCntrl(standardControllerInstance);
            Test.setCurrentPageReference(new PageReference('Page.Client_Acknowledgment_Form_Drawloop'));
            System.currentPageReference().getParameters().put('Id', bookingUnitList[0].Id);
            redirectionUrl = objController.generateClientAcknowledgmentLetter();
        Test.stopTest();

    }

    @isTest static void testMethod3() {
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
          objBookingUnit.Is_Client_Acknowledgment_Form_Generated__c  = true;
        }
        insert bookingUnitList;


        PageReference redirectionUrl;
        Test.startTest();
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(bookingUnitList[0]);
            ClientAcknowledgmentFormDrwaloopCntrl objController = new ClientAcknowledgmentFormDrwaloopCntrl(standardControllerInstance);
            Test.setCurrentPageReference(new PageReference('Page.Client_Acknowledgment_Form_Drawloop'));
            System.currentPageReference().getParameters().put('Id', bookingUnitList[0].Id);
            redirectionUrl = objController.generateClientAcknowledgmentLetter();
        Test.stopTest();

    }

}