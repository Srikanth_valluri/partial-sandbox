/****************************************************************************************************
* Name          : DAMAC_BITLYURL                                                                    *
* Description   : Class to call the Bitly functions                                                 *
* Created Date  : 16/04/2018                                                                        *
* Created By    : DAMAC IT                                                                          *
* --------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                *
*                                                                                                   *
****************************************************************************************************/
public class DAMAC_BITLYURL{

    //public static Map<String, Object> responseMap;
    
    
    public static string getShortenedURL(string longUrl) {
        string bitlyUrl;
        String token = Label.Bitly_Token; 
        HttpRequest req = new HttpRequest();
        String endPoint = Label.Bitly_EndPoint_URL
                        + '?access_token=' 
                        + token 
                        + '&longUrl=' 
                        + EncodingUtil.urlEncode(longUrl, 'UTF-8' ) 
                        + '&format=json';

            req.setEndpoint(endPoint);
            req.setMethod('GET');
            req.setTimeout(120 * 1000);
            System.debug('endPoint: '+endPoint);
            System.debug('req: '+req);
            Http http = new Http(); 
            if(!test.isrunningtest()){           
                HttpResponse res = http.send(req);        
                system.debug(res.getBody());  
                DAMAC_BITLY_RESPONSE_OBJ cls = DAMAC_BITLY_RESPONSE_OBJ.parse(res.getBody());      
                                
                system.debug('---->'+cls.data.url);
                bitlyUrl = cls.data.url;
            }else{
                //responseMap = (Map<String, Object>)JSON.deserializeUntyped('{"status_code":200,"status_txt":"OK","data":{"url":"https://dam.ac/2T8WK9e","hash":"2T8WK9e","global_hash":"2T8WKpK","long_url":"https://api.whatsapp.com/send?phone=97143019944\u0026text= IQF-114501 &#124; Hi! I would like to know more about the DAMACs promotions and offers!\u0026source=\u0026data=","new_hash":0}}');
            }
            
            system.debug('==bitlyUrl=='+bitlyUrl);
            
            return bitlyUrl;
    }

}