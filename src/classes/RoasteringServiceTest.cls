@isTest
public class RoasteringServiceTest {
    @isTest
    static void getAtendanceDaily(){
        Credentials_details__c creds = new Credentials_details__c();
        creds.Endpoint__c = 'https://inout.damacgroup.com/cosec/api.svc/v2/attendance-daily?action=get;format=json;field-name=userid,WorkingShift,firsthalf,secondhalf,UserName,ProcessDate,Punch1,Punch2,WorkingShift,LateIn,EarlyOut,Overtime,WorkTime;range=user;id=';
        creds.Name ='Roastering Api';
        creds.Password__c = 'Sale$Inter123';
        creds.User_Name__c = 'salesforce';
        insert creds;
        
        
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new RoasteringServiceMockHttp());
            //AttendanceDailyResponse collRes = RoasteringService.getAttendanceDetails('1341');
            RoasteringApiAttendance.getAttendanceDetails('1341',Date.today() - 2,Date.today() +30);
        Test.stopTest();
        //System.assert(collRes != null);
    }
    
    @isTest
    static void getShiftDetails(){
        Credentials_details__c creds = new Credentials_details__c();
        creds.Endpoint__c = 'https://inout.damacgroup.com/cosec/api.svc/v2/shift-details?action=get;format=json;shift-id=';
        creds.Name ='Roastering Shift Api';
        creds.Password__c = 'Sale$Inter123';
        creds.User_Name__c = 'salesforce';
        insert creds;
        
        
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new RoasteringServiceMockHttp());
            //ShiftDetailsResponse  collRes = RoasteringService.getShiftDetails('GS');
            RoasteringApiShift.getShiftDetails('GS');
        Test.stopTest();
        //System.assert(collRes != null);
    }
    
    @isTest
    static void getHolidayDetails(){
        Credentials_details__c creds = new Credentials_details__c();
        creds.Endpoint__c = 'https://inout.damacgroup.com/cosec/api.svc/v2/holiday?action=get;format=json;year=2020;list-type=2;userId=';
        creds.Name ='Roastering Holiday Api';
        creds.Password__c = 'Sale$Inter123';
        creds.User_Name__c = 'salesforce';
        insert creds;
        
        
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new RoasteringServiceMockHttp());
            //ShiftDetailsResponse  collRes = RoasteringService.getShiftDetails('GS');
            RoasteringApiHoliday.getHolidaySchedule('1341','2020');
        Test.stopTest();
        //System.assert(collRes != null);
    }
}