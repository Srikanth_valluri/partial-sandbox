@istest
public class DAMAC_SMS_Tests{
    
    static testmethod void testSMS(){
        inquiry__c inq = new inquiry__c();
        inq.Mobile_phone__c = '98765456789';
        inq.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Mobile_Phone_Encrypt__c = 'WERTYUIYRTYU';
        insert inq;
        inquiry__c inq1 = new inquiry__c();
        inq1.Mobile_phone__c = '987654567897';
        inq1.Mobile_CountryCode__c = 'India: 0091';
        inq1.Mobile_Phone_Encrypt__c = 'WERTYUIYRTYU';
        insert inq1;
         
        list<inquiry__c> inqToSMS = new list<inquiry__c>();
        inqToSMS.add(inq);
        inqToSMS.add(inq1);
        
        Campaign__c camp = new Campaign__c();
        camp.Marketing_Start_Date__c = system.today().adddays(-10);
        camp.Start_Date__c = system.today().adddays(-10);        
        camp.End_Date__c =  system.today().adddays(10);
        camp.Marketing_End_Date__c =  system.today().adddays(10);
        
        
        insert camp;
        
        campaign_sms__c l = new campaign_sms__c();
        l.SMS_Bulk_Id__c ='3037903';
        l.marketing_campaign__c = camp.id;  
        l.Scheduled_Date_Time__c = system.today().adddays(10);       
        insert l;
        
        Test.setMock(HttpCalloutMock.class, new DAMAC_SMS_Mock_Submission());
        Async_Rest_SMS cls = new Async_Rest_SMS(camp.id,'Sample SMS from DAMAC',inqToSMS,l.id);
        Async_Rest_SMS clss = new Async_Rest_SMS(String.valueOf(camp.id),new Set<String>{inq.Mobile_phone__c},'Sample SMS from DAMAC','Inquiry');
        test.starttest();            
            cls.sendSMS(camp.id,'Sample SMS from DAMAC',inqToSMS,l.id,'Inquiry',new Set<String>());  

            System.enqueueJob(cls);          
        test.stoptest();
        
    
    }
    
    static testmethod void testSMS_Status(){
        inquiry__c inq = new inquiry__c();
        inq.Mobile_phone__c = '98765456789';
        inq.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Mobile_Phone_Encrypt__c = 'WERTYUIYRTYU';
        insert inq;
        list<inquiry__c> inqToSMS = new list<inquiry__c>();
        inqToSMS.add(inq);
        
        Campaign__c camp = new Campaign__c();
        camp.Marketing_Start_Date__c = system.today().adddays(-10);
        camp.Start_Date__c = system.today().adddays(-10);        
        camp.End_Date__c =  system.today().adddays(10);
        camp.Marketing_End_Date__c =  system.today().adddays(10);
        insert camp;
        
        campaign_sms__c l = new campaign_sms__c();
        l.SMS_Bulk_Id__c ='3037903';
        l.marketing_campaign__c = camp.id;     
        l.Scheduled_Date_Time__c = system.today().adddays(10);    
        insert l;
        
        
        Test.setMock(HttpCalloutMock.class, new DAMAC_SMS_Mock_BulkId());
        Async_Rest_SMS_Job_Status cls = new Async_Rest_SMS_Job_Status('3037903');
        
        test.starttest();            
            cls.checkStatusbyBulkId('3037903');            
        test.stoptest();
    }
    
    static testmethod void testSMS_Status_Error(){
        inquiry__c inq = new inquiry__c();
        inq.Mobile_phone__c = '98765456789';
        inq.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Mobile_Phone_Encrypt__c = 'WERTYUIYRTYU';
        insert inq;
        list<inquiry__c> inqToSMS = new list<inquiry__c>();
        inqToSMS.add(inq);
        
        Campaign__c camp = new Campaign__c();
        camp.Marketing_Start_Date__c = system.today().adddays(-10);
        camp.Start_Date__c = system.today().adddays(-10);        
        camp.End_Date__c =  system.today().adddays(10);
        camp.Marketing_End_Date__c =  system.today().adddays(10);
        insert camp;
        
        campaign_sms__c l = new campaign_sms__c();
        l.SMS_Bulk_Id__c ='3037903';
        l.marketing_campaign__c = camp.id;  
        l.Scheduled_Date_Time__c = system.today().adddays(10);       
        insert l;
        
        
        Test.setMock(HttpCalloutMock.class, new DAMAC_SMS_Mock_BulkId_Error());
        Async_Rest_SMS_Job_Status cls = new Async_Rest_SMS_Job_Status('3037903');
        test.starttest();            
            cls.checkStatusbyBulkId('3037903');            
        test.stoptest();
    }
    
    
    static testmethod void testSMS_Page(){
        inquiry__c inq = new inquiry__c();
        inq.Mobile_phone__c = '98765456789';
        inq.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Mobile_Phone_Encrypt__c = 'WERTYUIYRTYU';
        insert inq;
        list<inquiry__c> inqToSMS = new list<inquiry__c>();
        inqToSMS.add(inq);
        
        Campaign__c camp = new Campaign__c();
        camp.Marketing_Start_Date__c = system.today().adddays(-10);
        camp.Start_Date__c = system.today().adddays(-10);        
        camp.End_Date__c =  system.today().adddays(10);
        camp.Marketing_End_Date__c =  system.today().adddays(10);
        insert camp;
        
        campaign_sms__c l = new campaign_sms__c();
        l.SMS_Bulk_Id__c ='3037903';
        l.marketing_campaign__c = camp.id;  
        l.inquiry_start_date__c = system.today().adddays(-1);
        l.inquiry_end_date__c = system.today().adddays(1);   
        l.Scheduled_Date_Time__c = system.today().adddays(10); 
        insert l;
        
        
            DAMAC_Submit_SMS cls = new DAMAC_Submit_SMS(); 
            cls.newSMSRequest.Inquiry_start_date__c = system.today().adddays(-1);
            cls.newSMSRequest.Inquiry_end_date__c = system.today().adddays(1);
            cls.filterInquries();
            cls.marketingid = camp.id;
            cls.newSMSRequest.sms_Message__c = 'DFGGFGHJ';
            cls.newSMSRequest.Scheduled_Date_Time__c =system.today().adddays(10); 
            cls.submitSMS();
            cls.CampsmsId = l.id;
            
            cls.callSMSJob();
        
        
    
    }
    
   


}