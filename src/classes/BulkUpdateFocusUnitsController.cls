/*********************************************************************************************************
* Name               : BulkUpdateFocusUnitsController
* Test Class         : 
* Description        : Controller class for BulkUpdateFocusUnits VF Page
* Created Date       : 16/11/2020
* -----------------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst         16/11/2020      Initial Draft.
**********************************************************************************************************/
global With Sharing Class BulkUpdateFocusUnitsController{
    PUBLIC List<String> propertyNames {get; set;}
    public string propertyId {get; set;}
    public boolean propertyExists {get; set;}
    public string propertyName {get; set;}
    Map<String, Property__c> propertyMap;
    Map<String, Inventory__c> invNameMap;
    public List<Inventory__c> inventoryList {get;set;}
    public string unitIds{get;set;}
    public string unitDetails {get; set;}
    public String[] unitIdList{get;set;}

    /*********************************************************************************************
    * @Description : Controller class.
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public BulkUpdateFocusUnitsController() {
        propertyNames = new List<String>();
        propertyName = '';      
        propertyMap = new  Map<String, Property__c>();
        invNameMap = new  Map<String, Inventory__c>();
        inventoryList = new List<Inventory__c> ();
        unitDetails = JSON.serialize(invNameMap.values());
        for(Property__c property: [SELECT Id, Property_Name__c, Property_ID__c 
                                   FROM Property__c 
                                   WHERE Property_Id_is_Negative__c = FALSE
                                   AND Property_Name__c != '']){
            propertyMap.put(property.Property_Name__c, property);
            propertyNames.add(property.Property_Name__c);
        }
        fetchUnitDetails();
    }

    /************************************************************************************************
    * @Description : method to fetch the Unit Details.
    * @Params      : void
    * @Return      : void
    ************************************************************************************************/
   public void fetchUnitDetails(){
        String projName = ApexPages.currentPage().getParameters().get('projName');
        if(projName != null && projName!= ''){
            propertyName = projName;
            Property__c property =  propertyMap.get(projName);
            system.debug('projName: ' + projName);
            fetchPropertyDetails(projName);
        }
    }

    /************************************************************************************************
    * @Description : method to fetch the Unit Details.
    * @Params      : void
    * @Return      : void
    ************************************************************************************************/
   public void updateInventory(){
        String startDate = ApexPages.currentPage().getParameters().get('startDate');
        String endDate = ApexPages.currentPage().getParameters().get('endDate');
        String unitNames = ApexPages.currentPage().getParameters().get('unitNames');
        system.debug('startDate: ' + startDate);
        system.debug('endDate: ' + endDate);
        system.debug('unitNames: ' + unitNames);
        List<Inventory__c> invList = new List<Inventory__c>();
        List<Id> invIdList = new List<Id>();
        if(unitNames != null && unitNames != ''){
            for(String invName: unitNames.split(',')){
                if(invName != '' && invNameMap.containsKey(invName)){
                    Inventory__c inv = invNameMap.get(invName);
                    if(inv.Is_Focus__c != true){
                        invIdList.add(inv.Id);
                        inv.Is_Focus__c = true;
                    }
                    inv.Focus_Start_Date__c  = Date.valueOf(startDate);
                    inv.Focus_End_Date__c = Date.valueOf(endDate);
                    invList.add(inv);
                }
            }
        }
        if(invList.size() > 0){
            try{
                update invList;
                if(invIdList.size() > 0){
                    InventoryProjectUtility.insertListInventoryLog(invIdList, 'Is_Focus__c', 
                                        'false', 'true', 'Bulk Update Focus Units', '');
                }
                fetchPropertyDetails(propertyName);
            } catch(exception e){
                system.debug('Exception : ' + e);
            }
        }
    }

    /************************************************************************************************
    * @Description : method to fetch the  Property Details.
    * @Params      : void
    * @Return      : void
    ************************************************************************************************/
    public void fetchPropertyDetails(String projName) {
        inventoryList = new List < Inventory__c > ();
        if(projName != null && projName != ''){
            for (Inventory__c inv: [SELECT Id, Status__c, Unit_Location__c, 
                                        Bedroom_Type_Code__c, ACD_Date__c,
                                        Unit_Name__c, Building_Name__c, 
                                        Property_Name_2__c,  Property__c,
                                        Focus_Start_Date__c, Focus_End_Date__c, 
                                        Marketing_Name__c, IPMS_Bedrooms__c, 
                                        Special_Price_2__c, Special_Price__c,  
                                        Is_Focus__c, Area_Sqft__c
                                  FROM Inventory__c
                                  WHERE Property_Name_2__c =: projName
                                  AND Unit_Name__c != '']) {
                invNameMap.put(inv.Unit_Name__c, inv);
            }
        }
        system.debug('inventoryList: ' + invNameMap.values());
        unitDetails = JSON.serialize(invNameMap.values());
        system.debug('unitDetails: ' + unitDetails);
    }
}