global class EnrichmentBatchScheduler implements Schedulable {
    
    // Execute at regular intervals
    global void execute(SchedulableContext ctx){
      EnrichmentBatch batch = new EnrichmentBatch();
      Database.executebatch(batch, 100);
    }
}