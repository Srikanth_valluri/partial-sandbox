public with sharing class MyTripsController{
    
    public List<Trip__c> myTrips { get; set; }
    public List<Trip__c> myTopTrips { get; set; }
    public List<Trip__c> myPreviousTrips { get; set; } //Code Added
    public String message { get; set; }
    public String checkInReason { get; set; }
    public Trip__c trip { get; set; }
    public Map <ID, String> startTimemap { get; set; }
    
    
    public MyTripsController(APexPages.standardController stdController) {
        message = '';
        checkInReason = '';
        trip = new Trip__c ();
        startTimemap = new Map<ID, String>();
        loadtrips();
        loadTopTrips();
        loadPreviousTrips(); //Code Added
                    
    }
    
    public void updateLocation () {
        String tripId = apexpages.currentpage().getparameters().get('tripId');
        String type = apexpages.currentpage().getparameters().get('type');
        String lattitude = apexpages.currentpage().getparameters().get('lat');
        String longitude = apexpages.currentpage().getparameters().get('lang');
        String checkInReason = apexPages.currentPage().getParameters().get('reason');
        String tripEvent = apexPages.currentPage().getParameters().get('tripEvent');
        
        message = '';
        
        Trip__c tripRec = new Trip__c ();
        tripRec = [SELECT Vehicle_Tracking_URL__c,InquiryId__c, Check_In_Reason__c, check_In_location__c, Status__c, check_Out_location__c, Check_In_Location__latitude__s, Check_In_Location__Longitude__s,
                        Driver_Check_In_Time__c, Driver_Check_out_Time__c, Car__r.Vehicle_Id__c, Car__r.Imei_No__c, car__r.Vehicle_Tracking_URL__c, 
                        Check_Out_Location__Longitude__s, Check_OUT_Location__latitude__s,Driver_Check_in_Comments__c FROM Trip__c WHERE ID =: tripId];
        
        System.Debug(tripRec.Id);
        HttpResponse res = new HTTPResponse ();
        
        if (!Test.isRunningTest())
            res = Damac_FleetRootService.getVechicleCurrentStatus(tripRec.Car__r.Vehicle_Id__c);
        else {
            res.setStatusCode(200);
        }
        System.Debug(res.getBody());
        
        Map<String, Trip_Tracking_Status__c> trackingStatus = Trip_Tracking_Status__c.getAll();
        
        if (res.getStatusCode() == 200) {
            Damac_VehicleLocationJSON_RESP resp = new Damac_VehicleLocationJSON_RESP ();
            if (!Test.isRunningTest())
                resp = Damac_VehicleLocationJSON_RESP.parse(res.getBody());
            else {    
                List<Damac_VehicleLocationJSON_RESP.cls_results> newObjList = new List<Damac_VehicleLocationJSON_RESP.cls_Results>();
                Damac_VehicleLocationJSON_RESP.cls_results newObj = new Damac_VehicleLocationJSON_RESP.cls_Results ();
                newObj.IMEINo = '861901228004454';
                newObj.TimeStamp = '2019-05-01T18:12:07+04:00';
                newObj.Latitude = 25.142903333333333;
                newObj.Longitude = 55.220083333333335;
                newObj.Angle = 30;
                newObj.Speed = 20;
                newObj.IsEngineOn = false;
                newObj.TotalMilage = 3632000;
                newObj.VehicleID = '50857cd6-f884-4707-aee3-ef5980f76f6a';
                newObj.VehicleName = 'N 49213';
                newObj.VehicleGroupID = '00000000-0000-0000-0000-000000000000';
                newObj.DriverID = '00000000-0000-0000-0000-000000000000';
                newObj.Driver = 'test';
                newObj.CurrentLocation = '';
                newObj.FuelLevel = 0;
                newObj.IsSeatBeltOn = 0;
                newObj.IsDoorOpen = 0;
                newObj.IsAcOn = 0;
                newObj.IsEquipmentOn = 0;
                newObj.EngineTemp = 0;
                newObj.EngineRpm = 0;
                newObj.InStatusFrom = '2019-05-01T18:07:07+04:00';
                newObj.TrackingStatus = 1;
                newObjList.add(newObj);
                
                resp.results = newObjList ;
            }
            
            if (resp.results.size () > 0) {
                Trip_Events__c event = new Trip_Events__c();
                event.Trip__c = triprec.Id;
                event.Time__c =  resp.results[0].timeStamp;
                event.Angle__c =  resp.results[0].Angle != NULL ? String.valueOf(resp.results[0].Angle) : '';
                event.Speed__c =  resp.results[0].Speed != NULL ? String.valueOf(resp.results[0].Speed) : '';
                event.Total_Milage__c = resp.results[0].TotalMilage;
                
                event.Latitude__c = resp.results[0].latitude != NULL ? String.valueOf(resp.results[0].latitude) : '';
                event.Longitude__c = resp.results[0].longitude != NULL ? String.valueOf(resp.results[0].longitude) : '';
                event.Current_Location__c = resp.results[0].CurrentLocation;
                event.Fuel_Level__c = resp.results[0].FuelLevel;
                event.Seat_Belt_On__c = resp.results[0].IsSeatBeltOn != NULL ? (resp.results[0].IsSeatBeltOn == 1 ? true : false) : false;
                event.Is_Door_Open__c = resp.results[0].IsDoorOpen != NULL ? (resp.results[0].IsDoorOpen == 1 ? true : false) : false;
                event.IS_AC_On__c = resp.results[0].IsAcOn != NULL ? (resp.results[0].IsAcOn == 1 ? true : false)  : false;
                event.IsEquipmentOn__c = resp.results[0].IsEquipmentOn != NULL ? (resp.results[0].IsEquipmentOn == 1 ? true : false) : false;
                event.Engine_Temp__c = resp.results[0].EngineTemp;
                event.Engine_RPM__c = resp.results[0].EngineRpm;
                event.IN_Status_From__c = resp.results[0].InStatusFrom;
                
                
                event.Vehicle_Name__c = resp.results[0].vehiclename;
                //event.Vehicle_Group_Name__c = resp.results[0].vehiclegroupname;
                
                event.Tracking_Status__c = resp.results[0].trackingstatus != NULL ? trackingStatus.get(String.valueOf(resp.results[0].trackingstatus)).Status__c : '';
                event.IsEngineOn__c = resp.results[0].isengineon;
                if (type == 'checkIn')
                    event.Check_In_Reason__c = checkInReason ;
                    
                if (type == 'checkOut') {
                    event.Check_in_reason__c = 'CheckOut';
                }
                
                if (tripEvent == 'Cancel Trip') {
                    event.Check_in_reason__c = checkInReason;
                }
                
                insert event;
                System.Debug(event.id);
            }
            
        }
        System.Debug (res.getBody());
        System.Debug (res.getStatusCode());
        
        
        Boolean updateRec = false;
        
        if ( type == 'checkIn' && tripEvent != 'Cancel Trip') {
            tripRec.Check_In_Location__latitude__s = Decimal.valueOf(lattitude);
            tripRec.Check_In_Location__Longitude__s = Decimal.valueOf(longitude);
            tripRec.Driver_Check_In_Time__c = DateTime.NOW();
            tripRec.Status__c = 'In Progress';
            tripRec.check_In_Reason__c = checkInReason;

            updateRec = true;
            message = 'Check in Success.';
        }
        
        if (type == 'checkOut' && tripEvent != 'Cancel Trip') {
            tripRec.Check_Out_Location__latitude__s = Decimal.valueOf(lattitude);
            tripRec.Check_Out_Location__Longitude__s = Decimal.valueOf(longitude);
            tripRec.Driver_Check_Out_Time__c = DateTime.NOW();
            tripRec.Status__c = 'Completed';
            updateRec = true;
            message = 'Check out Success.';
        }
        System.Debug(tripEvent);
        
        if ( tripEvent == 'Cancel Trip') {
            tripRec.Check_In_Location__latitude__s = Decimal.valueOf(lattitude);
            tripRec.Check_In_Location__Longitude__s = Decimal.valueOf(longitude);
            tripRec.Driver_Check_In_Time__c = DateTime.NOW();
            tripRec.Status__c = 'Cancelled';
            tripRec.Is_Cancelled__c = TRUE;
            tripRec.Cancel_Reason__c = checkInReason;
            updateRec = true;
            message = 'Trip Cancelled.';
        }
        
        trip.check_In_Reason__c = NULL;
        if (updateRec)
            Update tripRec;
        
    }
    
    public void loadTrips(){
        myTrips = new List<Trip__c> ();
        myTrips = [SELECT Name, Check_In__c, Car__c, Car__r.name, Check_In_Location__c, Check_Out_Location__c, 
                    Driver_Check_in_Comments__c ,InquiryId__c,
                    Customer_Name__c, Drop_Address__c, Pick_Up_Address__c,
                End_Time__c, Vehicle_Tracking_URL__c, Start_Time__c, Status__c, Trip_Key__c, Cancel_Reason__c , Check_In_Reason__c, Trip_Requester_Comments__c, car__r.Vehicle_Tracking_URL__c, Trip_Type__c, Check_Out_Location__latitude__s, Check_IN_Location__latitude__s 
                FROM TRIP__c 
                where (status__c='Allocated' or status__c='In Progress')
                 AND Driver__r.user__c=:UserInfo.getUserID () 
                 AND Day_only(Start_Time__c) = TODAY  
                 ORDER BY Start_Time__c,status__c,Name DESC]; 
                //status__c='Allocated'
        startTimemap = new Map<ID, String>();
        for (Trip__c trip: myTrips) {
            startTimemap.put(trip.id, trip.start_Time__c != NULL ? trip.Start_Time__c.format() : '');
        }
    
    }
    
    //Starting Code add
    public void loadPreviousTrips(){

        DateTime currentTime = DateTime.Now().addHours(-10);
        myPreviousTrips = new List<Trip__c> ();
        myPreviousTrips = [SELECT Name, Check_In__c, Car__c, Car__r.name, Check_In_Location__c, Check_Out_Location__c, 
                           Driver_Check_in_Comments__c , Driver_Feedback__c, Customer_Name__c, Drop_Address__c, 
                           Pick_Up_Address__c, End_Time__c, Vehicle_Tracking_URL__c, Start_Time__c, Status__c, Trip_Key__c, 
                           Cancel_Reason__c , Check_In_Reason__c, Trip_Requester_Comments__c, car__r.Vehicle_Tracking_URL__c, 
                           Trip_Type__c, Check_Out_Location__latitude__s, Check_IN_Location__latitude__s  FROM TRIP__c WHERE 
                           status__c='In Progress' AND Driver__r.user__c=:UserInfo.getUserID () 
                           AND Day_only(Start_Time__c) = YESTERDAY AND Driver_Check_out_Time__c = null
                           AND Start_Time__c >= : currentTime 
                           ORDER BY Start_Time__c,status__c,Name DESC];
        for (Trip__c trip: myPreviousTrips ) {
            startTimemap.put(trip.id, trip.start_Time__c != NULL ? trip.Start_Time__c.format() : '');
        }
    }
    //Ending Code add
    
    public void loadTopTrips(){
        myTopTrips = new List<Trip__c> ();
        myTopTrips = [SELECT Name, Check_In__c, Car__c, Car__r.name, Check_In_Location__c, Check_Out_Location__c, 
                        Driver_Check_in_Comments__c , Driver_Feedback__c,
                        Customer_Name__c, Drop_Address__c, Pick_Up_Address__c,
                        End_Time__c, Vehicle_Tracking_URL__c, Start_Time__c, Status__c, 
                        Trip_Key__c, Cancel_Reason__c , Check_In_Reason__c, 
                        Trip_Requester_Comments__c, car__r.Vehicle_Tracking_URL__c, Trip_Type__c, Check_Out_Location__latitude__s, 
                        Check_IN_Location__latitude__s 
                        FROM TRIP__c 
                        where (status__c = 'Completed' OR Status__c = 'Cancelled')
                        AND Driver__r.user__c=:UserInfo.getUserID ()                         
                        ORDER BY Start_Time__c,status__c,Name DESC LIMIT 5]; 
        for (Trip__c trip: myTopTrips ) {
            startTimemap.put(trip.id, trip.start_Time__c != NULL ? trip.Start_Time__c.format() : '');
        }
      
    }
    
    public void driverFeedBack () {
        String tripId = apexpages.currentpage().getparameters().get('tripId');
        String feedBack = apexpages.currentpage().getparameters().get('type');
        Trip__c feedBackTrip = new Trip__c ();
        feedBackTrip.Id = tripId;
        feedBackTrip.Driver_Feedback__c = feedBack;
        update feedBackTrip;
        trip = new Trip__c ();
        
        
    }
    
    public pagereference reload(){
        
       
        Loadtrips();
        loadTopTrips();
        return new pagereference('/apex/Mytrips');
    }
}