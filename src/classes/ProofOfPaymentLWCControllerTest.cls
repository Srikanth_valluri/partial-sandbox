@isTest
public class ProofOfPaymentLWCControllerTest {
    @isTest
    static void testReference(){
        Case objCase = new Case();
        insert objCase;
        
        POP_Bank_Details__c objPOPBank = new POP_Bank_Details__c();
        objPOPBank.Case__c = objCase.Id;
        insert objPOPBank;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        //objBooking.Account__c = objAccount.Id;
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Booking__c = objBooking.Id;
        objBU.Registration_ID__c = '12345';
        //objBU.Termination_Sequence__c = objTS.Id;
        insert objBU;
        
        SR_Booking_Unit__c objSRBooking = new SR_Booking_Unit__c();
        //objSRBooking.Registration_ID__c = '12345';
        objSRBooking.Case__c = objCase.Id;
        insert objSRBooking;
        
        Test.startTest();
        ProofOfPaymentLWCController.getReference(objCase.Id);
        ProofOfPaymentLWCController.getBookingUnits(objCase.Id);
        //ProofOfPaymentLWCController.getReceipts();
        //ProofOfPaymentLWCController.getResponse();
        Test.stopTest();
        
    }
    
    @isTest
    static void testReference1(){
        Case objCase = new Case();
        insert objCase;
        
        POP_Bank_Details__c objPOPBank = new POP_Bank_Details__c();
        objPOPBank.Bank_Account_number__c = 12345678;
        objPOPBank.Beneficiary_Name__c = 'test';
        objPOPBank.Beneficiary_Bank__c = 'PNB';
        objPOPBank.Case__c = objCase.Id;
        insert objPOPBank;
        
        Test.startTest();
            ProofOfPaymentLWCController.getReference(objCase.Id);
        Test.stopTest();
    }
    
    @isTest
    static void testGetReceipts(){
        
        //String Json = '{ "P_BANK_REF_NUMBER":"", "P_BANK_ACCOUNT_ID":"14000", "P_DEPOSIT_DATE":"2020-03-01", "P_NOOF_DAYS":"0", "P_AMOUNT":"1500", "P_DP_RANGE_AMOUNT":"0" }';
        String Json = '{"GET_UNIDENTIFIED_RECEIPT_Input":{ "RESTHeader":{ "Responsibility":"ONT_ICP_SUPER_USER", "RespApplication":"ONT", "SecurityGroup":"STANDARD", "NLSLanguage":"AMERICAN" }, "InputParameters":{ "P_BANK_REF_NUMBER":"","P_BANK_ACCOUNT_ID":"10402","P_DEPOSIT_DATE":"2016-03-16","P_NOOF_DAYS": "50","P_AMOUNT" :"5000","P_DP_RANGE_AMOUNT" : "3000"}}}';
        
       
        
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'POP Fetch AR Receipts';
        settings.Endpoint__c = 'http://83.111.194.181:8033/webservices/rest/XXDC_POP_RECEIPTS/get_unidentified_receipt/';
        settings.User_Name__c = 'oracle_user';
        settings.Password__c = 'crp1user';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
        
         RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenities';  
       // req.addParameter('bookingUnitId', '');
        Blob requestBody = Blob.valueOf('requestString');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.setMock(HttpCalloutMock.class, new MockGeneratorApprovalCallout());
        Test.startTest();
        ProofOfPaymentLWCController.getReceipts(Json);
        Test.stopTest();
    }
    
    
    @isTest
    static void testAddReceipts(){
        Test.startTest();
        ProofOfPaymentLWCController.getAddedReceipts('[{"refNum":12345,"BankAccountNumber":1234,"DepositAmount":100000,"receiptNum":12341,"Allocation":"Yes"}]');
        Test.stopTest();
    }
    
    @isTest
    static void testapprovalCallout(){
        String receiptDetails='[{"refNum":"12345","BankAccountNumber":"1234567890","DepositDate":"27/08/2020","DateRange":"100","DepositAmount":"100","receiptNum":"909090","CASH_RECEIPT_ID":"898989","REMARKS":"Test"}]';
        String buDetails='[{"Id":"1234567890","regID":"12345","unitName":"testUnit","amount":"100","regStatus":"test"}]';
        Case objCase = new Case();
        insert objCase;
        
        Task objTask = new Task();
        objTask.Subject = 'Verify Proof of Payment Details in IPMS';
        objTask.Status = 'New';
        objTask.WhatId = objCase.Id;
        objTask.Assigned_User__c = 'Finance';
        objTask.Process_Name__c = 'POP';
        insert objTask;
        
        SR_Attachments__c objSRAttach = new SR_Attachments__c();
        objSRAttach.Name = 'test';
        objSRAttach.Case__c = objCase.Id;
        insert objSRAttach;
        /*Credentials_Details__c settings1 = new Credentials_Details__c();
        settings1.Name = 'office365RestServices';
        settings1.User_Name__c = 'Some Value';
        settings1.Password__c = 'Some Value';
        settings1.Resource__c = 'Some Value';
        settings1.grant_type__c = 'Some Value';
        insert settings1;*/
        
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'POP Approval API';
        settings.Endpoint__c = 'http://83.111.194.181:8033/webservices/rest/XXDC_POP_RECEIPTS/apply_unidentified_receipts/';
        settings.User_Name__c = 'oracle_user';
        settings.Password__c = 'crp1user';
        //settings.Resource__c = 'Some Value';
       // settings.grant_type__c = 'Some Value';
        insert settings;
        Test.setMock(HttpCalloutMock.class, new ProofOfPaymentLWCControllerMockGenerator());
       
        
        //Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
        //Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive2());      
        Test.startTest();
        ProofOfPaymentLWCController.approvalCallout(receiptDetails,buDetails,objCase.Id);
        Test.stopTest();
    }
}