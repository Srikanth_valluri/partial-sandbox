@isTest
public class HD_SendPromoCodeMessage_APITest {
    private static String baseUrl = URL.getOrgDomainUrl().toExternalForm();  /* URL.getSalesforceBaseUrl() */
    
    @isTest
    static void test_SendMailSuccess(){
        Id personAcc_rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account acc_pass = new Account( LastName = 'Test Account1',
                                       Party_ID__c = '639777061',
                                       RecordtypeId = personAcc_rtId,
                                       Email__pc = 'success@mailinator.com');
        insert acc_pass;
        Account acc_fail = new Account( LastName = 'Test Account2',
                                       Party_ID__c = '639777062',
                                       RecordtypeId = personAcc_rtId,
                                       Email__pc = '');
        insert acc_fail;
        
        Map<String, Object> requestMap_post = new Map<String, Object>();
        requestMap_post.put('account_id', NULL);
        requestMap_post.put('promo_code', 'QWERTY$$ASDF');
        requestMap_post.put('send_to_list', NULL); 
        requestMap_post.put('cc_list', NULL); 
        requestMap_post.put('bcc_list', 'v-subin@damacgroup.com');
        
        String reqJSON = JSON.serialize(requestMap_post);
        getNewRestContext_SendEmailPOST(reqJSON);
        
        Test.startTest();
        
        invokeEmailNotificationPost(requestMap_post); /* 1 account_id is NULL */
        
        requestMap_post.put('account_id', 'INVALID_ID');
        invokeEmailNotificationPost(requestMap_post); /* 2 account_id is invalid */
        
        requestMap_post.put('account_id', acc_fail.id);
        invokeEmailNotificationPost(requestMap_post); /* 3 account_id has no email and send_list empty */
        
        requestMap_post.put('send_to_list', 'success@mailinator.com'); 
        invokeEmailNotificationPost(requestMap_post); /* 4 account_id has no email, but send_list not empty */
        
        requestMap_post.put('send_to_list', NULL);
        requestMap_post.put('account_id', acc_pass.id);
        invokeEmailNotificationPost(requestMap_post); /* 5 account_id has email */
        
        Test.stopTest();
    }
    
    @isTest
    static void test_SendMailFail(){
        Id personAcc_rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account acc_fail = new Account( LastName = 'Test Account3',
                                       Party_ID__c = '639777063',
                                       RecordtypeId = personAcc_rtId,
                                       Email__pc = 'fail@mailinator.com');
        insert acc_fail;
        
        Map<String, Object> requestMap_post = new Map<String, Object>();
        requestMap_post.put('account_id', acc_fail.id);
        requestMap_post.put('promo_code', 'QWERTY$$ASDF');
        requestMap_post.put('send_to_list', NULL); 
        requestMap_post.put('cc_list', NULL); 
        requestMap_post.put('bcc_list', 'v-subin@damacgroup.com');
        
        String reqJSON = JSON.serialize(requestMap_post);
        getNewRestContext_SendEmailPOST(reqJSON);
        
        Test.startTest();
        
        invokeEmailNotificationPost(requestMap_post); /* 1 */
        
        Test.stopTest();
    }
    
    private static void invokeEmailNotificationPost(Map<String, Object> requestMap_post){
        HD_SendPromoCodeMessage_API.doPost(
            (String)requestMap_post.get('account_id'), 
            (String)requestMap_post.get('promo_code'),  
            (String)requestMap_post.get('send_to_list'),  
            (String)requestMap_post.get('cc_list'),  
            (String)requestMap_post.get('bcc_list') );
    }
    
    private static void getNewRestContext_SendEmailPOST(String reqJSON){
        RestRequest request = new RestRequest();
        request.requestUri = baseUrl + '/services/apexrest/sendNotification/email';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(reqJSON);
        
        RestResponse response = new RestResponse();
		
        RestContext.request = request;
        RestContext.response = response;
    }
}