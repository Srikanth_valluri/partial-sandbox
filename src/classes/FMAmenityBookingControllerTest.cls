@isTest
private class FMAmenityBookingControllerTest {
    @testSetup
    static void createCommonTestData() {
     Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        Account objAcc = TestDataFactoryFM.createAccount();
        insert objAcc;

        Id personAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account acctInsOwner=new Account(RecordTypeID = personAccountId,PersonEmail='a@g.com',FirstName = 'Test FName Owner',
                                    LastName = 'Test LName',Passport_Number__c='12345678',Mobile__c='1212121212',Address_Line_1__c='test');
        insert acctInsOwner;

        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        objSR = TestDataFactoryFM.createServiceRequest(objAcc);
        insert objSR;

        Booking__c objBooking = TestDataFactoryFM.createBooking(acctInsOwner, objSR);
        insert objBooking;

        Booking_unit__c objBu = TestDataFactoryFM.createBookingUnit(objAcc, objBooking);
        objBu.Owner__c = NULL;
        insert objBu;

        Property__c propObj = TestDataFactoryFM.createProperty();
        insert propObj;

        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Property_name__c = propObj.id;
        insert locObj;

        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        User u = TestDataFactoryFM.createUser(profileId.id);
        u.IPMS_Employee_ID__c = 'IPMS456';
        u.Sales_Office__c = 'AKOYA'; 
        u.ManagerId = UserInfo.getUserId();
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;

        Resource__c objRes = new Resource__c();
        objRes.Name = 'Test Resource';
        objRes.Chargeable_Fee_Slot__c = 10;
        objRes.Non_Operational_Days__c = 'Sunday';
        objRes.Resource_Type__c = 'FM Amenity';
        objRes.No_of_advance_booking_days__c = 2;
        objRes.Resource_Type__c = 'FM Amenity';
        objRes.Deposit__c = 3;
        objRes.No_of_deposit_due_days__c = 1;
        objRes.Is_Available_for_Public__c = false;
        insert objRes;

        Resource_Sharing__c objRS= new Resource_Sharing__c();
        objRS.Building__c = locObj.Id;
        objRS.Resource__c = objRes.Id;
        insert objRS;

        Resource_Slot__c objResSlot1 = new Resource_Slot__c();
        objResSlot1.Resource__c = objRes.Id;
        objResSlot1.Start_Date__c = Date.Today();
        objResSlot1.End_Date__c = Date.Today().addDays(30);
        objResSlot1.Start_Time_p__c = '09:00';
        objResSlot1.End_Time_p__c = '09:30';
        insert objResSlot1;

        Resource_Slot__c objResSlot2 = new Resource_Slot__c();
        objResSlot2.Resource__c = objRes.Id;
        objResSlot2.Start_Date__c = Date.Today();
        objResSlot2.End_Date__c = Date.Today().addDays(30);
        objResSlot2.Start_Time_p__c = '09:00';
        objResSlot2.End_Time_p__c = '09:30';
        insert objResSlot2;

        Non_operational_days__c obj = new Non_operational_days__c();
        obj.Non_operational_date__c = Date.today();
        obj.Resource__c = objRes.Id;
        insert obj;

        Id devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().
                            get('Amenity Booking').getRecordTypeId();

        FM_Case__c objFMCase = new FM_Case__c();
        objFMCase.Booking_Date__c = Date.Today().addDays(2);
        objFMCase.Resource_Booking_Type__c = 'FM Amenity';
        objFMCase.RecordTypeId =devRecordTypeId;
        //objFMCase.Booking_Start_Time_p__c = '10:00';
        //objFMCase.Booking_End_Time_p__c = '10:30';
        objFMCase.Booking_Start_Time_p__c = '09:00';
        objFMCase.Booking_End_Time_p__c = '09:30';
        objFMCase.Amenity_Booking_Status__c = 'Booking Confirmed';
        objFMCase.Resource__c = objRes.id;
        insert objFMCase;
    }
    @isTest static void test_availableResources1() {
        // Implement test code
        List<Account> objAccList =[select id from Account LIMIT 2];
        Booking_Unit__c objBu = [select id from Booking_unit__c LIMIT 1];
        Resource__c objRes = [select id from Resource__c LIMIT 1];
        PageReference myVfPage = Page.FMAmenityBookingPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',objAccList[1].Id);
        ApexPages.currentPage().getParameters().put('UnitId',objBu.Id);
        ApexPages.currentPage().getParameters().put('SRType','Amenity_Booking_T');

        FMAmenityBookingController instance = new FMAmenityBookingController();
        instance.strResource = objRes.Id;
        instance.objFMCase.Resource__c = objRes.Id;
        instance.getNonOperationalDays();
        DateTime dt = Date.Today().addDays(1);
        // dt = System.now();

        instance.strDate = dt.format('dd/MM/yyyy');
        instance.getAvailableSlots();
        instance.bookAmenity();
        //instance.getAvailableSlots();
    }

    @isTest static void test_availableResources2() {
        // Implement test code
        List<Account> objAccList =[select id from Account LIMIT 2];
        Booking_Unit__c objBu = [select id from Booking_unit__c LIMIT 1];
        Resource__c objRes = [select id from Resource__c LIMIT 1];
        PageReference myVfPage = Page.FMAmenityBookingPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',objAccList[1].Id);
        ApexPages.currentPage().getParameters().put('UnitId',objBu.Id);
        ApexPages.currentPage().getParameters().put('SRType','Amenity_Booking_T');

        FMAmenityBookingController instance = new FMAmenityBookingController();
        instance.strResource = objRes.Id;
        instance.objFMCase.Resource__c = objRes.Id;
        instance.getNonOperationalDays();
        DateTime dt = Date.Today().addDays(2);
        // dt = System.now();

        instance.strDate = dt.format('dd/MM/yyyy');
        instance.getAvailableSlots();
        instance.bookAmenity();
        instance.getAvailableSlots();
    }
    @isTest static void test_bookAMenity() {
        // Implement test code
        List<Account> objAccList =[select id from Account LIMIT 2];
        Booking_Unit__c objBu = [select id from Booking_unit__c LIMIT 1];
        Resource__c objRes = [select id from Resource__c LIMIT 1];
        PageReference myVfPage = Page.FMAmenityBookingPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',objAccList[1].Id);
        ApexPages.currentPage().getParameters().put('UnitId',objBu.Id);
        ApexPages.currentPage().getParameters().put('SRType','Amenity_Booking_T');

        FMAmenityBookingController instance = new FMAmenityBookingController();
        instance.strResource = objRes.Id;
        instance.objFMCase.Resource__c = objRes.Id;
        instance.getNonOperationalDays();
        DateTime dt = Date.Today();
        // dt = System.now();

        instance.strDate = dt.format('dd/MM/yyyy');
        instance.getAvailableSlots();
        instance.strSelectedSlot = '09:00 - 09:30';
        instance.bookAmenity();
    }
}