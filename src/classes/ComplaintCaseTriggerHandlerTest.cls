/**
 * Description: Test class for ComplaintCaseTriggerHandler
 */
@isTest
private class ComplaintCaseTriggerHandlerTest {

    /*
     * Description : Test method to check whether the Owner Id of related Tasks are updated or not
     */
    @isTest static void testOwnershipChangeForTask() {

        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        Id complaintRecordTypeID = getRecordTypeIdForComplaint();

        // Creation of Case
        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , complaintRecordTypeID);
        insert objCase;

        objCase = [Select OwnerId FROM Case WHERE Id =:objCase.Id];

        // Creation of Task
        Task objTask = new Task();
        objTask.subject = 'Test Subject';
        objTask.whatId = objCase.Id;
        objTask.status = 'In Progress';
        insert objTask;
        
        // Creation Of User
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User objUser = new User();
        objUser.FirstName = 'test';
        objUser.LastName = 'PC';
        objUser.ProfileId = profileId;
        objUser.email = 'testpc@gmail.com';
        objUser.alias = 'testpc2';
        objUser.emailencodingkey='UTF-8';
        objUser.languagelocalekey='en_US';
        objUser.localesidkey='en_US';
        objUser.country='United Arab Emirates';
        objUser.IsActive =false;
        objUser.timezonesidkey='America/Los_Angeles';
        objUser.username='testpc@account.com';
        objUser.Languages_Known__c='English;Arabic;Hindi;French';
        insert objUser;

        Test.startTest();
            objCase.OwnerId = objUser.Id;
            update objCase;
        Test.stopTest();

        List<Task> lstTask = [SELECT Id, OwnerId From Task where WhatId =: objCase.Id];
        objCase = [SELECT Id, OwnerId From Case WHERE Id =: objCase.Id];

        // Check whether the Owner Id of Task is changed or not.
        System.assert(lstTask != null);
        System.assert(lstTask.size() > 0);
        System.assertEquals(lstTask[0].OwnerId , objCase.OwnerId);
    }

    /**
     * Method to get the "Complaint" record type
     */
    private static Id getRecordTypeIdForComplaint() {
      Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
      Id complaintRecordTypeID = caseRecordTypes.get('Complaint').getRecordTypeId();
      return complaintRecordTypeID;
    }
}