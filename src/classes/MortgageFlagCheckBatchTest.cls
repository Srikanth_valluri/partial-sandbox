@isTest
public class MortgageFlagCheckBatchTest{
     private static testmethod void batchTest(){

        

        User u1 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Collection - CRE'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            UserName = 'puser000@amama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            IPMS_Employee_ID__c = 'IPMS4535356',
            ManagerId = UserInfo.getUserId(),
            Title = 'title',
            Alias = 'alias1',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
           
        );
        insert u1;
        User u2 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Collection - CRE'].Id,
            LastName = 'last',
            Email = 'puser1000@amamama.com',
            IPMS_Employee_ID__c = 'IPMS4234342567',
            ManagerId = u1.Id,
            Manager = u1,
            UserName = 'puser000@amamama11.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
            
        );
        insert u2;
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Collection - CRE'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            IPMS_Employee_ID__c = 'IPMS432535567',
            ManagerId = u1.Id,
            Manager = u1,
            UserName = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
            
        );
        insert u;

        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Mortgage').getRecordTypeId();
        Case objC = new Case();
        objC.OwnerId  = u.Id;
        objC.Mortgage_Flag__c = 'I';
        objC.Extended__c = false;
        objC.Status = 'Submitted';
        objC.Origin = 'Walk-In';
        objC.RecordTypeId = recTypeId;
        
        insert objC;

        Datetime yesterday = Datetime.now().addDays(-90);
        Test.setCreatedDate(objC.Id, yesterday);

        Test.startTest();
            Database.executeBatch(new MortgageFlagCheckBatch(),1);   
        Test.stopTest();



     }
        

}