@isTest
public class MortgageTaskClosureValidationTest{
    private static testmethod void DIFCClosure(){
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Mortgage').getRecordTypeId();
        Case objC = new Case();
        objC.Status = 'New';
        objC.Origin = 'Walk-In';
        objC.RecordTypeId = recTypeId;
        insert objC;
        
        Task objT = new Task();
        objT.Subject = Label.Mortgage_10;
        objT.WhatId = objC.Id;
        objT.Status = 'Not Started';
        objT.Process_Name__c = 'Mortgage';
        objT.OwnerId = UserInfo.getUserId();
        insert objT;
        
        try{
            objT.Status = 'Completed';
            update objT;
        }catch(Exception ex){
        }
    }
}