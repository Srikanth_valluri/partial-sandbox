/*-------------------------------------------------------------------------------------------------
Description: Test class for CreateServiceChargeNoticeBU
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 14-04-2019       | Lochana Karle    | 1. Added functionality to test CreateServiceChargeNoticeBU
=============================================================================================================================
*/
@isTest
private class CreateServiceChargeNoticeBUTest{
    @isTest
    static void itShould(){
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
         
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;        
        
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '1234'; 
            objUnit.FM_Service_Notice__c = true;
            objUnit.Service_Notice_Sent_Date__c  = null;
            objUnit.Earliest_Payment_Date__c = '12/06/2019';
            objUnit.Multiple_Units__c = 'ABC/12/1201, ABC/12/1204, ABC/12/1206';
            objUnit.Total_Payment_Amount__c = '33536.54';
        }
        insert lstBookingUnits;

        insert new IpmsRestServices__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            BaseUrl__c = 'http://0.0.0.0:8080/webservices/rest',
            Username__c = 'username',
            Password__c = 'password',
            Timeout__c = 120000
        );
        
        FmIpmsRestServices.DueInvoicesResponse invoicesResponse = new FmIpmsRestServices.DueInvoicesResponse();
        invoicesResponse.OutputParameters = new FmIpmsRestServices.OutputParameters();
        invoicesResponse.OutputParameters.X_RETURN_STATUS = 'S';
        invoicesResponse.OutputParameters.X_RETURN_MESSAGE = 'Total Records Fetched...1';
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE = new FmIpmsRestServices.ResponseMessage();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM = new List<Map<String, String>>();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM.add(
            new Map<String, String> {
                'ATTRIBUTE1' => 'registrationId',
                'ATTRIBUTE2' => 'unitName',
                'ATTRIBUTE3' => 'projectName',
                'ATTRIBUTE4' => 'customerId',
                'ATTRIBUTE5' => 'orgId',
                'ATTRIBUTE6' => 'partyId',
                'ATTRIBUTE7' => 'partyName',
                'ATTRIBUTE8' => 'trxNumber',
                'ATTRIBUTE9' => 'creationDate',
                'ATTRIBUTE10' => 'callType',
                'ATTRIBUTE11' => '50',
                'ATTRIBUTE12' => '100',
                'ATTRIBUTE13' => '50',
                'ATTRIBUTE14' => FmcUtils.formatAsIpmsDate(Date.today()),
                'ATTRIBUTE15' => 'trxType',
                'ATTRIBUTE16' => '0'
            }
        );
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(invoicesResponse)));

        Test.startTest();
            FmIpmsRestServices.DueInvoicesResult result = FmIpmsRestServices.getDueInvoices('1234', '', '');
            CreateServiceChargeNoticeBU objClass = new CreateServiceChargeNoticeBU();
            Database.Executebatch(objClass);
        Test.stopTest();
        
    }
}