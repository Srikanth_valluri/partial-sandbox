@isTest
private class fmcAmenityBookingControllerTest {
    @testSetup
    static void createCommonTestData() {

         insert new IpmsRestServices__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            BaseUrl__c = 'http://0.0.0.0:8080/webservices/rest',
            Username__c = 'username',
            Password__c = 'password',
            Timeout__c = 120000
        );


        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];

        Account objAcc = TestDataFactoryFM.createAccount();
        insert objAcc;

        Id personAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account acctInsOwner = new Account(RecordTypeID = personAccountId,
                                        PersonEmail='a@g.com',
                                        FirstName = 'Test FName Owner',
                                        LastName = 'Test LName',
                                        Passport_Number__c='12345678',
                                        Mobile__c='1212121212',
                                        Address_Line_1__c='test');
        insert acctInsOwner;

        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        objSR = TestDataFactoryFM.createServiceRequest(objAcc);
        insert objSR;

        Booking__c objBooking = TestDataFactoryFM.createBooking(acctInsOwner, objSR);
        insert objBooking;

        Booking_unit__c objBu = TestDataFactoryFM.createBookingUnit(objAcc, objBooking);
        //objBu.Booking__r.Account__r.Party_ID__c = '1234';
        objBu.Owner__c = NULL;
        insert objBu;

        Property__c propObj = TestDataFactoryFM.createProperty();
        insert propObj;

        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Property_name__c = propObj.id;
        insert locObj;

        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;

        Resource__c objRes = new Resource__c();
        objRes.Chargeable_Fee_Slot__c = 10;
        objRes.Non_Operational_Days__c = 'Sunday';
        objRes.Resource_Type__c = 'FM Amenity';
        objRes.No_of_advance_booking_days__c = 2;
        objRes.Resource_Type__c = 'FM Amenity';
        objRes.Deposit__c = 3;
        objRes.Chargeable_Fee_Slot__c = 5;
        objRes.No_of_deposit_due_days__c = 1;
        objRes.Contact_Person_Email__c = 'test@test.com';   //
        objRes.Contact_Person_Name__c = 'Test'; //
        objRes.Contact_Person_Phone__c = '1234567890';  //
        insert objRes;

        Resource_Sharing__c objRS= new Resource_Sharing__c();
        objRS.Building__c = locObj.Id;
        objRS.Resource__c = objRes.Id;
        insert objRS;

        Resource_Slot__c objResSlot1 = new Resource_Slot__c();
        objResSlot1.Resource__c = objRes.Id;
        objResSlot1.Start_Date__c = Date.Today();
        objResSlot1.End_Date__c = Date.Today().addDays(30);
        objResSlot1.Start_Time_p__c = '09:00';
        objResSlot1.End_Time_p__c = '09:30';
        insert objResSlot1;

        Non_operational_days__c obj = new Non_operational_days__c();
        obj.Non_operational_date__c = Date.today();
        obj.Resource__c = objRes.Id;
        insert obj;

        Id devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Amenity Booking').getRecordTypeId();
        FM_Case__c objFMCase = new FM_Case__c();
        objFMCase.Booking_Date__c = Date.Today();
        objFMCase.Resource_Booking_Type__c = 'FM Amenity';
        objFMCase.RecordTypeId =devRecordTypeId;
        objFMCase.Booking_Start_Time_p__c = '10:00';
        objFMCase.Booking_End_Time_p__c = '10:30';
        objFMCase.Amenity_Booking_Status__c = 'Booking Confirmed';
        insert objFMCase;
    }

    @isTest static void getAvailableResourcesTest() {

        FmIpmsRestServices.DueInvoicesResponse invoicesResponse = new FmIpmsRestServices.DueInvoicesResponse();
        invoicesResponse.OutputParameters = new FmIpmsRestServices.OutputParameters();
        invoicesResponse.OutputParameters.X_RETURN_STATUS = 'S';
        invoicesResponse.OutputParameters.X_RETURN_MESSAGE = 'Total Records Fetched...1';
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE = new FmIpmsRestServices.ResponseMessage();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM = new List<Map<String, String>>();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM.add(
            new Map<String, String> {
                'ATTRIBUTE1' => 'registrationId',
                'ATTRIBUTE2' => 'unitName',
                'ATTRIBUTE3' => 'projectName',
                'ATTRIBUTE4' => 'customerId',
                'ATTRIBUTE5' => 'orgId',
                'ATTRIBUTE6' => 'partyId',
                'ATTRIBUTE7' => 'partyName',
                'ATTRIBUTE8' => 'trxNumber',
                'ATTRIBUTE9' => 'creationDate',
                'ATTRIBUTE10' => 'callType',
                'ATTRIBUTE11' => '50',
                'ATTRIBUTE12' => '100',
                'ATTRIBUTE13' => '50',
                'ATTRIBUTE14' => FmcUtils.formatAsIpmsDate(Date.today()),
                'ATTRIBUTE15' => 'trxType',
                'ATTRIBUTE16' => '0'
            }
        );
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(invoicesResponse)));

        List<Account> objAccList =[select id from Account LIMIT 2];
        Booking_Unit__c objBu = [select id, 
                                        Registration_Id__c, 
                                        Property_Name__c, 
                                        Booking__r.Account__r.Party_ID__c 
                                from Booking_unit__c LIMIT 1];
        Resource__c objRes = [select id from Resource__c LIMIT 1];
        PageReference myVfPage = Page.CommunityPortal;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('view','BookAmenity');
        ApexPages.currentPage().getParameters().put('AccountId',objAccList[1].Id);
        ApexPages.currentPage().getParameters().put('UnitId',objBu.Id);
        ApexPages.currentPage().getParameters().put('SRType','Amenity_Booking_T');
        fmcAmenityBookingController objfmcAmenityBookingController = new fmcAmenityBookingController();

        Test.startTest();
        objfmcAmenityBookingController.strAccountId = objAccList[0].Id;
        objfmcAmenityBookingController.objFMCase.Resource__c = objRes.Id;
        objfmcAmenityBookingController.getUnitDetailsNew();
        objfmcAmenityBookingController.getNonOperationalDays();
        DateTime dt = Date.Today();
        objfmcAmenityBookingController.strDate = dt.format('dd/MM/yyyy');
        objfmcAmenityBookingController.getAvailableSlots();
        objfmcAmenityBookingController.bookAmenity();
        Test.stopTest();
    }

    @isTest static void bookAmenityTest() {

        FmIpmsRestServices.DueInvoicesResponse invoicesResponse = new FmIpmsRestServices.DueInvoicesResponse();
        invoicesResponse.OutputParameters = new FmIpmsRestServices.OutputParameters();
        invoicesResponse.OutputParameters.X_RETURN_STATUS = 'S';
        invoicesResponse.OutputParameters.X_RETURN_MESSAGE = 'Total Records Fetched...1';
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE = new FmIpmsRestServices.ResponseMessage();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM = new List<Map<String, String>>();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM.add(
            new Map<String, String> {
                'ATTRIBUTE1' => 'registrationId',
                'ATTRIBUTE2' => 'unitName',
                'ATTRIBUTE3' => 'projectName',
                'ATTRIBUTE4' => 'customerId',
                'ATTRIBUTE5' => 'orgId',
                'ATTRIBUTE6' => 'partyId',
                'ATTRIBUTE7' => 'partyName',
                'ATTRIBUTE8' => 'trxNumber',
                'ATTRIBUTE9' => 'creationDate',
                'ATTRIBUTE10' => 'callType',
                'ATTRIBUTE11' => '50',
                'ATTRIBUTE12' => '100',
                'ATTRIBUTE13' => '50',
                'ATTRIBUTE14' => FmcUtils.formatAsIpmsDate(Date.today()),
                'ATTRIBUTE15' => 'trxType',
                'ATTRIBUTE16' => '0'
            }
        );
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(invoicesResponse)));

        List<Account> objAccList =[select id from Account LIMIT 2];
        Booking_Unit__c objBu = [select id from Booking_unit__c LIMIT 1];
        Resource__c objRes = [select id from Resource__c LIMIT 1];
        PageReference myVfPage = Page.CommunityPortal;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('view','BookAmenity');
        ApexPages.currentPage().getParameters().put('AccountId',objAccList[1].Id);
        ApexPages.currentPage().getParameters().put('UnitId',objBu.Id);
        ApexPages.currentPage().getParameters().put('SRType','Amenity_Booking_T');
        fmcAmenityBookingController objfmcAmenityBookingController = new fmcAmenityBookingController();

        Test.startTest();
        objfmcAmenityBookingController.strAccountId = objAccList[0].Id;
        objfmcAmenityBookingController.objFMCase.Resource__c = objRes.Id;
        objfmcAmenityBookingController.strResource = objRes.Id; // new line added
        objfmcAmenityBookingController.getNonOperationalDays();
        DateTime dt = Date.Today();
        objfmcAmenityBookingController.strDate = dt.format('dd/MM/yyyy');
        objfmcAmenityBookingController.getAvailableSlots();
        objfmcAmenityBookingController.strSelectedSlot = '09:00 - 09:30';
        objfmcAmenityBookingController.bookAmenity();
        Test.stopTest();
    }

    @isTest static void getAvailableSlotsTest() {

        FmIpmsRestServices.DueInvoicesResponse invoicesResponse = new FmIpmsRestServices.DueInvoicesResponse();
        invoicesResponse.OutputParameters = new FmIpmsRestServices.OutputParameters();
        invoicesResponse.OutputParameters.X_RETURN_STATUS = 'S';
        invoicesResponse.OutputParameters.X_RETURN_MESSAGE = 'Total Records Fetched...1';
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE = new FmIpmsRestServices.ResponseMessage();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM = new List<Map<String, String>>();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM.add(
            new Map<String, String> {
                'ATTRIBUTE1' => 'registrationId',
                'ATTRIBUTE2' => 'unitName',
                'ATTRIBUTE3' => 'projectName',
                'ATTRIBUTE4' => 'customerId',
                'ATTRIBUTE5' => 'orgId',
                'ATTRIBUTE6' => 'partyId',
                'ATTRIBUTE7' => 'partyName',
                'ATTRIBUTE8' => 'trxNumber',
                'ATTRIBUTE9' => 'creationDate',
                'ATTRIBUTE10' => 'callType',
                'ATTRIBUTE11' => '50',
                'ATTRIBUTE12' => '100',
                'ATTRIBUTE13' => '50',
                'ATTRIBUTE14' => FmcUtils.formatAsIpmsDate(Date.today()),
                'ATTRIBUTE15' => 'trxType',
                'ATTRIBUTE16' => '0'
            }
        );
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(invoicesResponse)));

        List<Account> objAccList =[select id from Account LIMIT 2];
        Booking_Unit__c objBu = [select id from Booking_unit__c LIMIT 1];
        Resource__c objRes = [select id from Resource__c LIMIT 1];
        PageReference myVfPage = Page.CommunityPortal;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('view','BookAmenity');
        ApexPages.currentPage().getParameters().put('AccountId',objAccList[1].Id);
        ApexPages.currentPage().getParameters().put('UnitId',objBu.Id);
        ApexPages.currentPage().getParameters().put('SRType','Amenity_Booking_T');
        fmcAmenityBookingController objfmcAmenityBookingController = new fmcAmenityBookingController();

        Test.startTest();
        objfmcAmenityBookingController.strAccountId = objAccList[1].Id;
        objfmcAmenityBookingController.objFMCase.Resource__c = objRes.Id;
        objfmcAmenityBookingController.strResource = objRes.Id; //New Line added
        objfmcAmenityBookingController.getUnitDetailsNew();
        objfmcAmenityBookingController.getNonOperationalDays();
        objfmcAmenityBookingController.getTermsDocumentLink();
        DateTime dt = Date.Today().addDays(2);
        objfmcAmenityBookingController.strDate = dt.format('dd/MM/yyyy');
        objfmcAmenityBookingController.getAvailableSlots();
        objfmcAmenityBookingController.bookAmenity();
        Test.stopTest();
    }

    @isTest static void getAvailableSlotsGuestTest() {

        FmIpmsRestServices.DueInvoicesResponse invoicesResponse = new FmIpmsRestServices.DueInvoicesResponse();
        invoicesResponse.OutputParameters = new FmIpmsRestServices.OutputParameters();
        invoicesResponse.OutputParameters.X_RETURN_STATUS = 'S';
        invoicesResponse.OutputParameters.X_RETURN_MESSAGE = 'Total Records Fetched...1';
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE = new FmIpmsRestServices.ResponseMessage();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM = new List<Map<String, String>>();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM.add(
            new Map<String, String> {
                'ATTRIBUTE1' => 'registrationId',
                'ATTRIBUTE2' => 'unitName',
                'ATTRIBUTE3' => 'projectName',
                'ATTRIBUTE4' => 'customerId',
                'ATTRIBUTE5' => 'orgId',
                'ATTRIBUTE6' => 'partyId',
                'ATTRIBUTE7' => 'partyName',
                'ATTRIBUTE8' => 'trxNumber',
                'ATTRIBUTE9' => 'creationDate',
                'ATTRIBUTE10' => 'callType',
                'ATTRIBUTE11' => '50',
                'ATTRIBUTE12' => '100',
                'ATTRIBUTE13' => '50',
                'ATTRIBUTE14' => FmcUtils.formatAsIpmsDate(Date.today()),
                'ATTRIBUTE15' => 'trxType',
                'ATTRIBUTE16' => '0'
            }
        );
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(invoicesResponse)));

        List<Account> objAccList =[select id from Account LIMIT 2];
        Booking_Unit__c objBu = [select id from Booking_unit__c LIMIT 1];
        Resource__c objRes = [select id from Resource__c LIMIT 1];
        PageReference myVfPage = Page.BookPublicAmenity;
        Test.setCurrentPage(myVfPage);
        // ApexPages.currentPage().getParameters().put('view','BookAmenity');
        ApexPages.currentPage().getParameters().put('AccountId',objAccList[1].Id);
        ApexPages.currentPage().getParameters().put('UnitId',objBu.Id);
        ApexPages.currentPage().getParameters().put('SRType','Amenity_Booking_T');
        fmcAmenityBookingController objfmcAmenityBookingController = new fmcAmenityBookingController();

        Test.startTest();
        objfmcAmenityBookingController.strAccountId = objAccList[1].Id;
        objfmcAmenityBookingController.objFMCase.Resource__c = objRes.Id;
        objfmcAmenityBookingController.strResource = objRes.Id; //New line added
        objfmcAmenityBookingController.getUnitDetailsNew();
        objfmcAmenityBookingController.getNonOperationalDays();
        objfmcAmenityBookingController.getTermsDocumentLink();
        DateTime dt = Date.Today().addDays(2);
        objfmcAmenityBookingController.strDate = dt.format('dd/MM/yyyy');
        objfmcAmenityBookingController.getAvailableSlots();
        objfmcAmenityBookingController.bookAmenity();
        Test.stopTest();
    }
}