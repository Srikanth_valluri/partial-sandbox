@isTest
public class BatchToSendHospitalityTest {
    @isTest
    static void testMethod1(){
        Account objAccount = new Account();
        objAccount.LastName = 'test';
        objAccount.Email__c = 'success@mailinator.com';
        objAccount.Party_ID__c = '1234';
        insert objAccount;
        
        Case objCase = new Case();
        insert objCase;
        
        Location__c objLocation = new Location__c();
        objLocation.Name = 'test';
        objLocation.Location_ID__c = '12345';
        insert objLocation;
        
        Inventory__c objInventory = new Inventory__c();
        objInventory.Building__c = objLocation.Id;
        insert objInventory;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        objBooking.Account__c = objAccount.Id; 
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Registration_ID__c = '3901';
        objBU.Unit_Name__c = 'test/test';
        objBU.Booking__c = objBooking.Id;
        objBU.Recovery__c = 'A';
        objBU.Registration_Status__c = 'Agreement executed by DAMAC';
        objBU.Permitted_Use_New__c = 'Hotel Apartments';
        //objBU.EHO_Notice_Sent__c = true;
        objBU.Inventory__c = objInventory.Id;
        insert objBU;
        
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
        
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        BatchToSendHospitalityStatments objHospitality = new BatchToSendHospitalityStatments();
        DataBase.executeBatch(objHospitality,1); 
    }
}