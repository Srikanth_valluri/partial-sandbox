public class AmendmentTriggerHandler implements TriggerFactoryInterface{
    public void executeAfterInsertTrigger(Map<Id, sObject> mapNewRecords){}
    
    
    public void executeAfterInsertUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){}
    public void executeBeforeInsertUpdateTrigger(list<sObject> lstNewRecords, Map<Id,sObject> mapOldRecords){}
    public void executeBeforeDeleteTrigger(Map<Id, sObject> mapOldRecords){}
    public void executeAfterDeleteTrigger(Map<Id, sObject> mapOldRecords){}

    public void executeBeforeInsertTrigger(List<sObject> newRecordsList){
        List<Amendment__c> updateAmdSROwnerList = new List<Amendment__c>();
        List<Amendment__c> updateAmdInqOwnerList = new List<Amendment__c>();
        try{
            for(Amendment__c objAmd : (List<Amendment__c>)newRecordsList){
                if(objAmd.Service_Request__c != null){
                    updateAmdSROwnerList.add(objAmd);
                }else{
                    if(objAmd.Inquiry__c != null){
                        updateAmdInqOwnerList.add(objAmd);
                    }
                }
            }
            if(!updateAmdSROwnerList.isEmpty()){
                system.debug('#### updateAmdSROwnerList'+updateAmdSROwnerList);
                UpdateAmendmentOwnerToSrOwner(updateAmdSROwnerList);
            }
            if(!updateAmdInqOwnerList.isEmpty()){
                system.debug('#### Inside If');
                UpdateAmendmentOwnerToInqOwner(updateAmdInqOwnerList);
            }
        }catch(Exception ex){
            system.debug('#### Exception at line number = '+ex.getLineNumber()+' , Exception Message = '+ex.getMessage());
        }    
    }

    public void executeBeforeUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){
        List<Amendment__c> updateAmdSROwnerList = new List<Amendment__c>();
        List<Amendment__c> updateAmdInqOwnerList = new List<Amendment__c>();
        try{
            for(Amendment__c objAmd :(List<Amendment__c>)mapNewRecords.values()){
                if(objAmd.Service_Request__c != null){
                    updateAmdSROwnerList.add(objAmd);
                }else{
                    updateAmdInqOwnerList.add(objAmd);
                }
            }
            if(!updateAmdSROwnerList.isEmpty()){
                system.debug('#### Inside If');
                UpdateAmendmentOwnerToSrOwner(updateAmdSROwnerList);
            }
            if(!updateAmdInqOwnerList.isEmpty()){
                system.debug('#### Inside If');
                UpdateAmendmentOwnerToInqOwner(updateAmdInqOwnerList);
            }
        }catch(Exception ex){
            system.debug('#### Exception at line number = '+ex.getLineNumber()+' , Exception Message = '+ex.getMessage());
        }    
    }
    public void executeAfterUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> mapOldRecords){
        
    }
    public void UpdateAmendmentOwnerToSrOwner(List<Amendment__c> newRecords){
        Set<id> srIdSet = new Set<id>();
        system.debug('#### newRecords'+newRecords);
        List<Amendment__c> amendmentList = new List<Amendment__c>();
        for(Amendment__c amdObj : newRecords){
            srIdSet.add(amdObj.Service_Request__c);
        }
        system.debug('#### srIdSet'+srIdSet);
        Map<Id, Id> srMap = new Map<Id, Id>();
        for(NSIBPM__Service_Request__c srObj : [SELECT Id,ownerId
                                                 FROM NSIBPM__Service_Request__c WHERE Id IN :srIdSet]){
            srMap.put(srObj.id,srObj.ownerId);
        }
        if(!srMap.isEmpty()){
            for(Amendment__c amdObj : newRecords){
                amdObj.OwnerId = srMap.get(amdObj.Service_Request__c);
            }
        }
    }
    public void UpdateAmendmentOwnerToInqOwner(List<Amendment__c> newRecords){
        Set<id> inqIdSet = new Set<id>();
        for(Amendment__c amdObj : newRecords){
            inqIdSet.add(amdObj.Inquiry__c);
        }
        Map<Id,Id> inqMap = new Map<Id,Id>();
        for(Inquiry__c inqObj : [SELECT 
                                    id,
                                    ownerId
                                  FROM 
                                    Inquiry__c 
                                  WHERE 
                                    Id 
                                  IN :inqIdSet]){
            inqMap.put(inqObj.id,inqObj.ownerId);
        }
        if(!inqMap.isEmpty()){
            for(Amendment__c amdObj : newRecords){
                amdObj.OwnerId = inqMap.get(amdObj.Inquiry__c);
            }
        }
    }
}