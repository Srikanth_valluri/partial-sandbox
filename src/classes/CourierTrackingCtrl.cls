public without sharing Class CourierTrackingCtrl {
    public String selectedCS{get;set;}
    public String country{get;set;}
    public String accPartyId{get;set;}
    public String trachingNum {get;set;}
    public List<SelectOption> couServices {get;set;}
    public AirwayBillTrackListResponse trackingRes{get;set;}
    public Boolean makeReadonly{get;set;} 
    public List<AirwayBillTrackListResponse.AirwayBillTrackList> trackingBillLst{get;set;}
    public List<AirwayBillTrackListResponse.TrackingLogDetails> trackingLD{get;set;}
    public AramaxTrackingResponseJSON2Apex trackingResAremax{get;set;}
    public Courier_Delivery__c courierInst {get;set;}
    
    public CourierTrackingCtrl(ApexPages.StandardController controller) {
        selectedCS = '';
        trachingNum = '';
        accPartyId = '';
        country = '';
        makeReadonly = false;
        trackingBillLst = new List<AirwayBillTrackListResponse.AirwayBillTrackList>();
        //trackingResAremax = new AramaxTrackingResponseJSON2Apex();
        trackingLD = new List<AirwayBillTrackListResponse.TrackingLogDetails>();
        couServices = new List<SelectOption>();
        couServices.add(new SelectOption('First Flight', 'First Flight'));
        //couServices.add(new SelectOption('Aramex', 'Aramex'));
        couServices.add(new SelectOption('Aramex', 'Aramex Regular Account'));
        couServices.add(new SelectOption('Aramex CDS', 'Aramex CDS'));
        String currentRecordId  = ApexPages.CurrentPage().getparameters().get('id');
        if(currentRecordId != null) {
                             courierInst = [SELECT Id
                                                 , Airway_Bill__c
                                                 , Request_Number__c
                                                 , Account__r.Party_Id__c
                                                 , Country__c
                                                 , Courier_Service__c
                                            FROM Courier_Delivery__c
                                            WHERE Id = :currentRecordId
                                            LIMIT 1];
            selectedCS = courierInst.Courier_Service__c;
            trachingNum = courierInst.Airway_Bill__c;
            accPartyId = courierInst.Account__r.Party_Id__c;
            country = courierInst.Country__c;
            System.debug('courierInst inside if---->'+courierInst);
            makeReadonly = true;
            trackCourier();
        }
    }
    
    public void trackCourier() {
        if(selectedCS == 'First Flight') {
            if(trackingResAremax != Null) {
                trackingResAremax = new AramaxTrackingResponseJSON2Apex(null);
            }
            trackingRes = FirstFlightService.getFirstFlightTracking(trachingNum,accPartyId,country);
            trackingBillLst = trackingRes.AirwayBillTrackList;
            trackingLD = trackingBillLst[0].TrackingLogDetails;
            system.debug('trackingRes--->'+trackingRes);
            system.debug('trackingRes.AirwayBillTrackList--->'+trackingRes.AirwayBillTrackList);
            system.debug('trackingRes.AirwayBillTrackList[0].TrackingLogDetails--->'+trackingRes.AirwayBillTrackList[0].TrackingLogDetails);
            
            
        } else if(selectedCS == 'Aramex' || selectedCS == 'Aramex CDS'  ) {
            if(trackingRes != Null) {
                trackingRes = new AirwayBillTrackListResponse(null);
            }
            if(trackingBillLst != Null) {
                trackingBillLst.clear(); 
            }
            if(trackingLD != Null) {
                trackingLD.clear(); 
            }
            
            trackingResAremax = AramexService.getAramexTracking(trachingNum, country, 'DXB', accPartyId);
            //trackingBillLst = trackingRes.AirwayBillTrackList;
            //trackingLD = trackingBillLst[0].TrackingLogDetails;
            system.debug('trackingResAremax--->'+trackingResAremax);
            //system.debug('trackingRes.AirwayBillTrackList--->'+trackingRes.AirwayBillTrackList);
            //system.debug('trackingRes.AirwayBillTrackList[0].TrackingLogDetails--->'+trackingRes.AirwayBillTrackList[0].TrackingLogDetails);
        }
         
    }
    
    public void populateAccountDetails() {
    
        if(trachingNum != null && String.isNotBlank(trachingNum)) {
            List<Courier_Delivery__c> courierInst = [SELECT Id
                                                       , Airway_Bill__c
                                                       , Request_Number__c
                                                       , Account__r.Party_Id__c
                                                       , Country__c
                                                       , Courier_Service__c
                                                  FROM Courier_Delivery__c
                                                  WHERE Airway_Bill__c = :trachingNum
                                                  LIMIT 1];
            if(courierInst != null && courierInst.size()>0) {
                accPartyId = courierInst[0].Account__r.Party_Id__c;
                country = courierInst[0].Country__c;
            } else {
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please enter a valid Bill Number'));
            }
            
        } 
    }

}