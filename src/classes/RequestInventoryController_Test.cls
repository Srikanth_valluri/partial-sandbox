/**********************************************************************************************
 * @Name              : RequestInventoryController_Test
 * @Description       : Test Class for DAMAC_DealExceptionRequestController
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log
 * 1.1         QBurst         29/06/2020        Created
***********************************************************************************************/
@istest
public with sharing class RequestInventoryController_Test {


    static testmethod void RequestInventoryControllerMethod(){
        Account acc = new Account ();
        acc.LastName = 'test';
        acc.Agency_Type__c = 'Corporate';
        acc.Vendor_ID__c = '767676';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        insert acc;
                
        DAMAC_Central_Push_Notifications__c obj = new DAMAC_Central_Push_Notifications__c();
        obj.Email__c = 'test@test.com';
        obj.Password__c = '1232112';
        obj.device_source__c = 'test';
        obj.device_os_version__c = 'test';
        obj.app_version__c = '1.2';
        obj.device_model__c = 'test';
        obj.Api_Token__c = 'test';
        obj.Project_connect_API_Token__c = 'test';
        obj.app_id__c = 1;
        obj.is_authorization_required__c = false;
        insert obj;

        Unit_Assignment__c uAObj = new Unit_Assignment__c();
         uAObj.Start_Date__c = Date.parse('11/12/17');
         uAObj.End_Date__c =  Date.parse('11/12/18');
         uAObj.Unit_Assignment_Name__c = 'Test US1';
         uAObj.Reason_For_Unit_Assignment__c = 'Test reason';
         uAObj.Active__c = false;
         insert uAObj;

        Agent_Site__c agency = new Agent_Site__c();
        agency.Name = 'UAE';
        agency.Agency__c = acc.id;
        insert agency;
        String filters = 'Project:Test Project';
        String filters2 =  'Product:90210 Boutique Villas;UnitType:FURNISHED APARTMENT,HOTEL,HOTEL APARTMENTS'
                            + 'Project:TestProj;Status:Released;City:Dubai;Location:Dubai;ACD:today;'
                            + 'Bedroom:2BR;ViewType:Front;Units:Test;MinPrice:100;MaxPrice:100000;'
                            + 'MinSft:100;MaxSft:30000;MinPriceSft:100;MaxPriceSft:100000';

        Deal_Exception_Request__c der = new Deal_Exception_Request__c();
        der.RecordTypeId = Schema.SObjectType.Deal_Exception_Request__c.getRecordTypeInfosByName().get('Request Inventory').getRecordTypeId();
        der.Inventory_Request_Filters__c = filters;
        insert der;
        Deal_Exception_Request__c der2 = new Deal_Exception_Request__c();
        der2.Inventory_Request_Filters__c = filters2;
        der2.RecordTypeId = Schema.SObjectType.Deal_Exception_Request__c.getRecordTypeInfosByName().get('Request Inventory').getRecordTypeId();
        insert der2;
        List<Id> derIdList = new List<Id>();
        derIdList.add(der.Id);
        derIdList.add(der2.Id);
        Inquiry__c inq = new Inquiry__c ();
        inq.Activity_Counter__c =101;
        inq.Inquiry_Status__c='Active';
        inq.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        inq.ownerId = userinfo.getUserId();
        inq.Promoter_Name__c = 'Test';
        inq.Telesales_Executive__c = UserInfo.getUserId();
        insert inq;
        Location__c loc = InitializeSRDataTest.createLocation('123', 'Building');
        loc.property_id__c = '123';
        insert loc;

        Property__c newProperty = new Property__c();
        newProperty.Property_ID__c = 1;
        newProperty.Property_Code__c = 'VIR';
        newProperty.Property_Name__c = 'VIRIDIS @ AKOYA OXYGEN';
        newProperty.District__c = 'AL YUFRAH 2';
        newProperty.AR_Transaction_Type__c = 'INV VIR';
        newProperty.Penalty_Transaction_Type__c = 'DM-PENALTIES-VIR';
        newProperty.Brokerage_Distribution_Set__c = '11600';
        newProperty.Sales_Commission_Dist_Set__c = '11601';
        newProperty.Currency_Of_Sale__c = 'AED';
        newProperty.Signature_Col_Customer_Stmt__c = 'Front Line Investment Management Co. LLC';
        newProperty.EOI_Enabled__c = true;
        insert newProperty;

        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.Id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        //lstInv[0].Marketing_Name_Doc__c = 'Test Name';
        lstInv[0].Property_Status__c = 'Restricted';
        lstInv[0].Space_Type_Lookup_Code__c = 'Test';
        lstInv[0].Unit__c = 'Test Unit';
        lstInv[0].property_name__c = 'Test Project';
        lstInv[0].ACD_Date__c = String.valueOf(system.today());
        lstInv[0].Bedroom_Type__c = '2BR';
        lstInv[0].Floor_Package_Name__c = 'Test Package';
        lstInv[0].View_Type__c = 'Test View';
        lstInv[0].District__c = 'Dubai';
        lstInv[0].Marketing_Name__c = 'Damac Heights';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Unit_Location__c = loc.Id;
        lstInv[0].building_location__c = loc.Id;
        lstInv[0].property_id__c = newProperty.Id;
        lstInv[0].Unit_Assignment__c = uAObj.Id;
        lstInv[0].Tagged_To_Unit_Assignment__c = true;
        insert lstInv;

        Id accountRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account a = new Account();
        a.LastName= 'Test Account';
        a.RecordTypeId = accountRTId;
        insert a;

        Currency_Rate__c rate = new Currency_Rate__c();
        rate.To_Currency__c = 'AED';
        rate.Conversion_Date__c = system.today();
        rate.conversion_rate__c = 1;
        rate.From_Currency__c = 'AED';
        insert rate;

        Inventory__c invent = new Inventory__c();
        invent.Inventory_ID__c = '1';
        invent.Building_ID__c = '1';
        invent.Floor_ID__c = '1';
        invent.Marketing_Name__c = 'Damac Heights';
        invent.Address_Id__c = '1';
        invent.EOI__C = NULL;
        invent.Tagged_to_EOI__c = false;
        invent.Is_Assigned__c = false;
        invent.Property_ID__c = newProperty.Id;
        invent.Property__c = newProperty.Id;
        invent.Status__c = 'Released';
        invent.special_price__c = 1000000;
        invent.CurrencyISOCode = 'AED';
        invent.Property_ID__c = '1234'; 
        invent.Floor_Package_ID__c = ''; 
        invent.building_location__c = loc.Id;
        invent.property_id__c = newProperty.Id;
        invent.Unit_Assignment__c = uAObj.Id;
        invent.property_name__c = 'Test Project';
        invent.Tagged_To_Unit_Assignment__c = true;
        insert invent;
        
        List<Deal_Exception_Unit__c> deUnitList = new List<Deal_Exception_Unit__c>(); 
        Deal_Exception_Unit__c deUnit = new Deal_Exception_Unit__c();
        
        deUnit.Deal_Exception_Request__c = der.Id;
        deUnit.Inventory__c = invent.Id;
        deUnitList.add(deUnit);
        Deal_Exception_Unit__c deUnit2 = new Deal_Exception_Unit__c();
        deUnit2.Deal_Exception_Request__c = der2.Id;
        deUnit2.Inventory__c = lstInv[0].Id;
        deUnitList.add(deUnit2);
        insert  deUnitList;

        user rmUser = new User ();
        rmUser.ProfileId = [SELECT Id FROM Profile WHERE Name LIKE '%Property Consultant%' LIMIT 1].Id;
        rmUser.UserName = 'testEmail@testclass.com';
        rmUser.Email = 'testEmail@testclass.com';
        rmUser.Alias = 'tCE';
        rmUser.EmailEncodingKey = 'UTF-8';
        rmUser.LastName = 'Testing';
        rmUser.LanguageLocaleKey = 'en_US';
        rmUser.LocaleSidKey = 'en_US';
        rmUser.TimeZoneSidKey = 'America/Los_Angeles';
        insert rmUser;

        apexpages.currentpage().getparameters().put('inventoryId', invent.Id);
        apexpages.currentpage().getparameters().put('id', der.Id);
        PageReference pageRef = Page.DealExceptionRequest_Create;
        pageRef.getParameters().put('id', String.valueOf(der.Id));
        pageRef.getparameters().put('inventoryId', invent.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(der2);

        test.starttest();

        RequestInventoryController cls = new RequestInventoryController(sc);
        cls.approvalComments = 'Test Approval';
        cls.internalComments = 'Internal Comments';
        cls.fetchApprovedInventories();
        cls.fetchInventories();
        cls.getItems();
        cls.getPropStatus();
        cls.getCities();
        cls.getLocation();
        cls.getUnitTypes();
        cls.getUnits();
        cls.getPropertyNames();
        cls.getAcd();
        cls.getBedRooms();
        cls.getViewTypes();
        cls.getProjctFtrs();
        cls.getPkgNames();
        cls.fetchFilters();
        cls.updateFilterValues(filters);
        
        cls.modifyRequestFilter();
        cls.requestInventory();
        apexpages.currentpage().getparameters().put('selectedInventIds', invent.Id);
        cls.addInventoriesToRequest();
        apexpages.currentpage().getparameters().put('inventAction', 'Approve');
         apexpages.currentpage().getparameters().put('inventIds', invent.Id);
        cls.approveRejectInvRequest();
        apexpages.currentpage().getparameters().put('inventAction', 'Reject');
        cls.approveRejectInvRequest();
        cls.reloadRequestPage();
        cls.approveRejectRequest('Approve', 'test');
        apexpages.currentpage().getparameters().put('applyInventIds', invent.Id);
        cls.applyInventoryShare();
        test.stoptest();
       
        
        ApexPages.StandardController sc2 = new ApexPages.StandardController(der);
        RequestInventoryController cls2 = new RequestInventoryController(sc2);
        System.runAs(rmUser){
            RequestInventoryController cls3 = new RequestInventoryController(sc2);
        }
        apexpages.currentpage().getparameters().put('selectedInventIds', lstInv[0].Id);
        apexpages.currentpage().getparameters().put('inventAction', 'Approve');
        apexpages.currentpage().getparameters().put('inventIds', invent.Id);
        apexpages.currentpage().getparameters().put('inventAction', 'Reject');
        apexpages.currentpage().getparameters().put('applyInventIds', lstInv[0].Id);
        apexpages.currentpage().getparameters().put('applyToAll', 'true');
        cls2.approvalComments = 'Test Approval2';
        cls2.internalComments = 'Internal Comments2';
        cls2.fetchApprovedInventories();
        cls2.fetchInventories();
        cls2.updateFilterValues(filters2);
        cls2.modifyRequestFilter();
        cls2.requestInventory();
        der.status__c = 'Awaiting Sales Admin Review';
        update der;
        ApexPages.StandardController sc3 = new ApexPages.StandardController(der);
        RequestInventoryController cls4 = new RequestInventoryController(sc3);
        cls2.addInventoriesToRequest();
        cls2.approveRejectInvRequest();
        cls2.approveRejectRequest('Approve', 'test');
        RequestInventoryController cls5 = new RequestInventoryController(sc3);
        cls2.applyInventoryShare();
        
        
    }
}