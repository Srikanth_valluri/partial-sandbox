@isTest
public class AppointmentSelectionHandlerTest {
    static Account objAcc;
    static Booking_Unit__c objBookingUnit;
    static void testData() {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

        Booking_Unit_Active_Status__c objBUStatus = new Booking_Unit_Active_Status__c();
        objBUStatus.Name = 'Active Status';
        objBUStatus.Status_Value__c = 'Agreement executed by DAMAC';
        insert objBUStatus;

        objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person',party_ID__C='123456');
        objAcc.Email__pc = 'new@no.com';
        objAcc.PersonMobilePhone ='1234567890';
        insert objAcc;

        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;

        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;

        objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC';
        objBookingUnit.Snag_Status_Onsite__c ='Ready for Handover';
        insert objBookingUnit;
    }
    static testMethod void Test1(){
        testdata();

        Account objAccount = new Account(Id = objAcc.Id);
        objAccount.Primary_CRE__c = UserInfo.getUserId();
        objAccount.Customer_Category__c = 'Elite';
        update objAccount;

        Appointment__c objApp = new Appointment__c();
        objApp.Appointment_Date__c = date.newinstance( 2022, 12, 12 );
        objApp.Processes__c = 'Handover';
        objApp.Sub_Processes__c  = 'Documentation';
        objApp.Group_Name__c = 'Handover Queue';
        objApp.Assigned_CRE__c = UserInfo.getUserId();
        objApp.Slots__c = '10:00 - 11:00';
        insert objApp;

        Appointment__c objApp1 = new Appointment__c();
        objApp1.Processes__c = 'Handover';
        objApp1.Sub_Processes__c  = 'Documentation';
        objApp1.Group_Name__c = 'Handover Queue';
        objApp1.Assigned_CRE__c = UserInfo.getUserId();
        objApp1.Slots__c = '10:00 - 11:00';
        objApp1.Appointment_Date__c = system.today().addDays(-2);
        objApp1.Appointment_End_Date__c = system.today().addDays(2);
        insert objApp1;

        

        test.startTest();
        AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
        list<AppointmentSelectionHandler.AppointmentWrapper> listWrap
            = handler.availableSlots(objAccount.Id, objBookingUnit.Id, 'Handover', 'Documentation', 'CRE', '2022-12-12',false);
        map<Date, list<Appointment__c>> mapApp = handler.getAppointmentforFuture(objAccount.Id, objBookingUnit.Id, 'Handover', 'Documentation', 'Portal',String.valueOf(System.today()),true);
        test.stopTest();
    }

    static testMethod void Test2(){
        testdata();

        Account objAccount = new Account(Id = objAcc.Id);
        objAccount.Primary_CRE__c = UserInfo.getUserId();
        update objAccount;

        Appointment__c objApp = new Appointment__c();
        objApp.Appointment_Date__c = date.newinstance( 2022, 12, 12 );
        objApp.Processes__c = 'Visa NOC';
        objApp.Group_Name__c = 'Handover Queue';
        objApp.Assigned_CRE__c = UserInfo.getUserId();
        objApp.Slots__c = '10:00 - 11:00';
        insert objApp;

        Appointment__c objApp1 = new Appointment__c();
        objApp1.Processes__c = 'Rental Pool';
        objApp1.Sub_Processes__c  = 'Other';
        objApp1.Group_Name__c = 'Handover Queue';
        objApp1.Assigned_CRE__c = UserInfo.getUserId();
        objApp1.Slots__c = '10:00 - 11:00';
        objApp1.Appointment_Date__c = system.today().addDays(-2);
        objApp1.Appointment_End_Date__c = system.today().addDays(2);
        insert objApp1;

        test.startTest();
        AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
        list<AppointmentSelectionHandler.AppointmentWrapper> listWrap
            = handler.availableSlots(objAccount.Id, objBookingUnit.Id, 'Visa NOC', '', 'CRE', '2022-12-12',false);

        test.stopTest();
    }
    static testMethod void Test2v1(){
        testdata();
        
        
        List<DAMACHolidaySettings__c>settingLst2 = new List<DAMACHolidaySettings__c>();
         DAMACHolidaySettings__c newSetting1 = new DAMACHolidaySettings__c(Name= 'Holidays2018',
                                                                            From__c = date.newinstance( 2022, 12, 12 )
                                                                         ,To__c = date.newinstance( 2022, 12, 12 ));
          
        settingLst2.add(newSetting1);
        insert settingLst2;             
        
        Account objAccount = new Account(Id = objAcc.Id);
        objAccount.Primary_CRE__c = UserInfo.getUserId();
        objAccount.Secondary_CRE__c = UserInfo.getUserId();
        objAccount.Tertiary_CRE__c = UserInfo.getUserId();
        update objAccount;

        Appointment__c objApp = new Appointment__c();
        objApp.Appointment_Date__c = date.newinstance( 2022, 12, 12 );
        objApp.Processes__c = 'Visa NOC';
        objApp.Group_Name__c = 'Handover Queue';
        objApp.Assigned_CRE__c = UserInfo.getUserId();
        objApp.Slots__c = '10:00 - 11:00';
        insert objApp;

        Appointment__c objApp1 = new Appointment__c();
        objApp1.Processes__c = 'Rental Pool';
        objApp1.Sub_Processes__c  = 'Other';
        objApp1.Group_Name__c = 'Handover Queue';
        objApp1.Assigned_CRE__c = UserInfo.getUserId();
        objApp1.Slots__c = '10:00 - 11:00';
        objApp1.Appointment_Date__c = system.today().addDays(-2);
        objApp1.Appointment_End_Date__c = system.today().addDays(2);
        insert objApp1;

        test.startTest();
        AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
        handler.strApptLable = 'Y';
        system.debug(' handler.strApptLable v20 : ' + handler.strApptLable);
        list<AppointmentSelectionHandler.AppointmentWrapper> listWrap
            = handler.availableSlots(objAccount.Id, objBookingUnit.Id, 'Visa NOC', '', 'CRE', '2022-12-12',false);

        test.stopTest();
    }
    
    static testMethod void Test2v2(){
        testdata();
        
    
        Account objAccount = new Account(Id = objAcc.Id);
        objAccount.Primary_CRE__c = UserInfo.getUserId();
        objAccount.Secondary_CRE__c = UserInfo.getUserId();
        objAccount.Tertiary_CRE__c = UserInfo.getUserId();
        update objAccount;

        Appointment__c objApp = new Appointment__c();
        objApp.Appointment_Date__c = date.newinstance( 2022, 12, 12 );
        objApp.Processes__c = 'Visa NOC';
        objApp.Group_Name__c = 'Handover Queue';
        objApp.Assigned_CRE__c = UserInfo.getUserId();
        objApp.Slots__c = '10:00 - 11:00';
        insert objApp;

        Appointment__c objApp1 = new Appointment__c();
        objApp1.Processes__c = 'Rental Pool';
        objApp1.Sub_Processes__c  = 'Other';
        objApp1.Group_Name__c = 'Handover Queue';
        objApp1.Assigned_CRE__c = UserInfo.getUserId();
        objApp1.Slots__c = '10:00 - 11:00';
        objApp1.Appointment_Date__c = system.today().addDays(-2);
        objApp1.Appointment_End_Date__c = system.today().addDays(2);
        insert objApp1;

        test.startTest();
        AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
        handler.strApptLable = 'Y';
        system.debug(' handler.strApptLable v20 : ' + handler.strApptLable);
        list<AppointmentSelectionHandler.AppointmentWrapper> listWrap
            = handler.availableSlots(objAccount.Id, objBookingUnit.Id, 'Visa NOC', '', 'CRE', '2022-12-12',false);

        test.stopTest();
    }   
    
    static testMethod void Test3(){
        testdata();

        Account objAccount = new Account(Id = objAcc.Id);
        objAccount.Primary_CRE__c = UserInfo.getUserId();
        update objAccount;

        Appointment__c objApp = new Appointment__c();
        objApp.Appointment_Date__c = date.newinstance( 2022, 12, 12 );
        objApp.Processes__c = 'Collections';
        objApp.Sub_Processes__c = 'Bounced Cheque';
        objApp.Group_Name__c = 'Handover Queue';
        objApp.Assigned_CRE__c = UserInfo.getUserId();
        objApp.Slots__c = '10:00 - 11:00';
        insert objApp;

        Appointment__c objApp1 = new Appointment__c();
        objApp1.Processes__c = 'Rental Pool';
        objApp1.Sub_Processes__c  = 'Other';
        objApp1.Group_Name__c = 'Handover Queue';
        objApp1.Assigned_CRE__c = UserInfo.getUserId();
        objApp1.Slots__c = '10:00 - 11:00';
        objApp1.Appointment_Date__c = system.today().addDays(-2);
        objApp1.Appointment_End_Date__c = system.today().addDays(2);
        insert objApp1;

        test.startTest();
        AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
        list<AppointmentSelectionHandler.AppointmentWrapper> listWrap
            = handler.availableSlots(objAccount.Id, objBookingUnit.Id, 'Collections', 'Bounced Cheque', 'CRE', '2022-12-12',false);

        test.stopTest();
    }

    static testMethod void Test4(){
        testdata();

        Account objAccount = new Account(Id = objAcc.Id);
        objAccount.Primary_CRE__c = UserInfo.getUserId();
        objAccount.Secondary_CRE__c = UserInfo.getUserId();
        objAccount.Tertiary_CRE__c = UserInfo.getUserId();
        objAccount.Primary_Language__c = 'English';
        objAccount.Country__c = 'China';
        objAccount.Customer_Category__c = 'Elite';
        objAccount.Nationality__c = 'Chinese';
        update objAccount;

        Booking_Unit__c objUnit = new Booking_Unit__c(Id=objBookingUnit.Id);
        objUnit.Handover_Flag__c = 'Y';
        objUnit.Handover_Notice_Sent__c = true;
        objUnit.Okay_to_release_keys__c = true;
        objUnit.Handover_Notice_Sent_Date__c = system.today();
        update objUnit;

        Appointment__c objApp = new Appointment__c();
        objApp.Appointment_Date__c = date.newinstance( 2022, 12, 12 );
        objApp.Processes__c = 'Collection';
        objApp.Sub_Processes__c = 'Bounced Cheque';
        objApp.Group_Name__c = 'Handover Queue';
        objApp.Assigned_CRE__c = UserInfo.getUserId();
        objApp.Slots__c = '10:00 - 11:00';
        insert objApp;

        Appointment__c objApp1 = new Appointment__c();
        objApp1.Processes__c = 'Rental Pool';
        objApp1.Sub_Processes__c  = 'Other';
        objApp1.Group_Name__c = 'Handover Queue';
        objApp1.Assigned_CRE__c = UserInfo.getUserId();
        objApp1.Slots__c = '10:00 - 11:00';
        objApp1.Appointment_Date__c = system.today().addDays(-2);
        objApp1.Appointment_End_Date__c = system.today().addDays(2);
        insert objApp1;

        TriggerOnOffCustomSetting__c objOff = new TriggerOnOffCustomSetting__c ();
        objOff.Name = 'CallingListTrigger';
        objOff.OnOffCheck__c = true;
        insert objOff;

        test.startTest();
        AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
        list<AppointmentSelectionHandler.AppointmentWrapper> listWrap
            = handler.availableSlots(objAccount.Id, objBookingUnit.Id, 'Collection', 'Bounced Cheque', 'CRE', '2022-12-12',false);

        test.stopTest();
    }
    
    static testMethod void Test5(){        
        
        Group g1 = new Group(Name='Recovery Queue', type='Queue');
        insert g1;
        
        QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
        insert q1;
        
        GroupMember objGM = new GroupMember();
        objGM.GroupId = g1.Id;
        objGM.UserOrGroupId = UserInfo.getUserId();
        insert objGM;
        
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        User user = new User();
        user.firstName = 'test1';
        user.lastName = 'test2';
        user.profileId = profile1.id;
        user.username = uniqueUserName;
        user.email = 'test@test.com';
        user.LanguageLocaleKey='en_US';
        user.LocaleSidKey='en_US';
        user.TimeZoneSidKey='America/Los_Angeles';
        user.EmailEncodingKey='UTF-8'; 
        user.Alias = 'standt';       
        insert user;
        
        system.runAs(user){ 
            testdata();
                   
            Booking_Unit__c objUnit = new Booking_Unit__c(Id=objBookingUnit.Id);
            objUnit.Recovery__c = 'R';
            update objUnit;
            
            Appointment__c objApp1 = new Appointment__c();
            objApp1.Processes__c = 'Handover';
            objApp1.Sub_Processes__c  = 'Documentation';
            //objApp1.Group_Name__c = 'Handover Queue';
            objApp1.Assigned_CRE__c = UserInfo.getUserId();
            objApp1.Slots__c = '10:00 - 11:00';
            objApp1.Appointment_Date__c = system.today().addDays(-2);
            objApp1.Appointment_End_Date__c = system.today().addDays(2);
            insert objApp1;
    
            TriggerOnOffCustomSetting__c objOff = new TriggerOnOffCustomSetting__c ();
            objOff.Name = 'CallingListTrigger';
            objOff.OnOffCheck__c = true;
            insert objOff;
        }
        test.startTest();
        AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
        list<AppointmentSelectionHandler.AppointmentWrapper> listWrap
            = handler.availableSlots(objAcc.Id, objBookingUnit.Id, 'Handover', 'Documenttion', 'CRE', '2022-12-12',false);
        test.stopTest();
    }

    static testMethod void Test6(){        
        
        Group g1 = new Group(Name='Recovery Queue', type='Queue');
        insert g1;
        
        QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
        insert q1;
        
        GroupMember objGM = new GroupMember();
        objGM.GroupId = g1.Id;
        objGM.UserOrGroupId = UserInfo.getUserId();
        insert objGM;
        
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        User user = new User();
        user.firstName = 'test1';
        user.lastName = 'test2';
        user.profileId = profile1.id;
        user.username = uniqueUserName;
        user.email = 'test@test.com';
        user.LanguageLocaleKey='en_US';
        user.LocaleSidKey='en_US';
        user.TimeZoneSidKey='America/Los_Angeles';
        user.EmailEncodingKey='UTF-8'; 
        user.Alias = 'standt';       
        insert user;
        
        system.runAs(user){ 
            testdata();
                   
            Booking_Unit__c objUnit = new Booking_Unit__c(Id=objBookingUnit.Id);
            objUnit.Recovery__c = 'R';
            update objUnit;
            
            Appointment__c objApp1 = new Appointment__c();
            objApp1.Processes__c = 'Handover';
            objApp1.Sub_Processes__c  = 'Documentation';
            //objApp1.Group_Name__c = 'Handover Queue';
            objApp1.Assigned_CRE__c = UserInfo.getUserId();
            objApp1.Slots__c = '10:00 - 11:00';
            objApp1.Appointment_Date__c = system.today().addDays(-2);
            objApp1.Appointment_End_Date__c = system.today().addDays(2);
            insert objApp1;
    
            TriggerOnOffCustomSetting__c objOff = new TriggerOnOffCustomSetting__c ();
            objOff.Name = 'CallingListTrigger';
            objOff.OnOffCheck__c = true;
            insert objOff;
        }
        test.startTest();
        AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
        handler.strApptLable = 'Y';
        system.debug(' handler.strApptLable 6 : ' + handler.strApptLable);
        list<AppointmentSelectionHandler.AppointmentWrapper> listWrap
            = handler.availableSlots(objAcc.Id, objBookingUnit.Id, 'Handover', 'Documenttion', 'CRE', '2022-12-12',false);
        test.stopTest();
    }
    
    static testMethod void validation1(){
        testdata();

        Appointment__c objApp = new Appointment__c();
        objApp.Appointment_Date__c = date.newinstance( 2022, 12, 12 );
        objApp.Process_Name__c = 'Handover';
        objApp.Sub_Process_Name__c = 'Documentation';
        objApp.Group_Name__c = 'Handover Queue';
        objApp.Slots__c = '10:00 - 11:00';
        insert objApp;

        Date dateTomorrow = system.today().addDays(1);
        string strTomorrow = string.valueOf(dateTomorrow.year()) + '-'+string.valueOf(dateTomorrow.month())+'-'+string.valueOf(dateTomorrow.day());
        test.startTest();
        AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
        list<AppointmentSelectionHandler.AppointmentWrapper> listWrap
            = handler.availableSlots(objAcc.Id, objBookingUnit.Id, 'Handover', 'Key Handover', 'CRE', '2022-12-12',false);
        list<AppointmentSelectionHandler.AppointmentWrapper> listWrap2
            = handler.availableSlots(objAcc.Id, objBookingUnit.Id, 'Handover', 'Key Handover', 'Portal', strTomorrow,false);
        test.stopTest();
    }

    static testMethod void validation2(){
        testdata();

        //Booking_Unit__c objUnit = new Booking_Unit__c(Id=objBookingUnit.Id);
        //objUnit
        Appointment__c objApp = new Appointment__c();
        objApp.Appointment_Date__c = date.newinstance( 2022, 12, 12 );
        objApp.Process_Name__c = 'Handover';
        objApp.Sub_Process_Name__c = 'Documentation';
        objApp.Group_Name__c = 'Handover Queue';
        objApp.Slots__c = '10:00 - 11:00';
        insert objApp;

        Date dateToday = system.today();
        string strToday = string.valueOf(dateToday);
        system.debug('!!!!strTomorrow'+strToday);
        test.startTest();
        AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
        list<AppointmentSelectionHandler.AppointmentWrapper> listWrap2
            = handler.availableSlots(objAcc.Id, objBookingUnit.Id, 'Handover', 'Documentation', 'Portal', strToday,false);

        test.stopTest();
    }
    static testMethod void validation21(){
        testdata();

        //Booking_Unit__c objUnit = new Booking_Unit__c(Id=objBookingUnit.Id);
        //objUnit
        Appointment__c objApp = new Appointment__c();
        objApp.Appointment_Date__c = date.newinstance( 2022, 12, 12 );
        objApp.Process_Name__c = 'Handover';
        objApp.Sub_Process_Name__c = 'Documentation';
        objApp.Group_Name__c = 'Handover Queue';
        objApp.Slots__c = '10:00 - 11:00';
        insert objApp;

        Date dateToday = system.today().addDays(2);
        string strToday = string.valueOf(dateToday);
        system.debug('!!!!strTomorrow'+strToday);
        test.startTest();
        AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
        list<AppointmentSelectionHandler.AppointmentWrapper> listWrap2
            = handler.availableSlots(objAcc.Id, objBookingUnit.Id, 'Handover', 'Documentation', 'Portal', strToday,false);

        test.stopTest();
    }
    static testMethod void validation22(){
        testdata();

        //Booking_Unit__c objUnit = new Booking_Unit__c(Id=objBookingUnit.Id);
        //objUnit
        Appointment__c objApp = new Appointment__c();
        objApp.Appointment_Date__c = date.newinstance( 2019, 10, 15 );
        objApp.Process_Name__c = 'Handover';
        objApp.Sub_Process_Name__c = 'Documentation';
        objApp.Group_Name__c = 'Handover Queue';
        objApp.Slots__c = '10:00 - 11:00';
        insert objApp;

        Date dateToday = system.today();
        string strToday = string.valueOf(dateToday);
        system.debug('!!!!strTomorrow'+strToday);
        test.startTest();
        AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
        list<AppointmentSelectionHandler.AppointmentWrapper> listWrap2
            = handler.availableSlots(objAcc.Id, objBookingUnit.Id, 'Handover', 'Documentation', 'Portal', strToday,false);

        test.stopTest();
    }


    static testMethod void validation3(){
        testdata();

        Booking_Unit__c objUnit = new Booking_Unit__c(Id=objBookingUnit.Id);
        objUnit.Handover_Flag__c = 'Y';
        objUnit.Handover_Notice_Sent__c = false;
        update objUnit;

        Appointment__c objApp = new Appointment__c();
        objApp.Appointment_Date__c = date.newinstance( 2022, 12, 12 );
        objApp.Process_Name__c = 'Handover';
        objApp.Sub_Process_Name__c = 'Documentation';
        objApp.Group_Name__c = 'Handover Queue';
        objApp.Slots__c = '10:00 - 11:00';
        insert objApp;

        Date dateToday = system.today();
        string strToday = string.valueOf(dateToday);
        system.debug('!!!!strTomorrow'+strToday);
        test.startTest();
        AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
        list<AppointmentSelectionHandler.AppointmentWrapper> listWrap2
            = handler.availableSlots(objAcc.Id, objUnit.Id, 'Handover', 'Unit Viewing', 'CRE', strToday,false);

        test.stopTest();
    }

    static testMethod void validation4(){

        User CRM_CommitteeUser = new User(
             ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
             LastName = 'last',
             Email = 'abc@new.com',
             Username = 'abc@new.com' + System.currentTimeMillis(),
             CompanyName = 'TEST',
             Title = 'title',
             Alias = 'alias',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             LocaleSidKey = 'en_US');
        insert CRM_CommitteeUser;
        testdata();

        Booking_Unit__c objUnit = new Booking_Unit__c(Id=objBookingUnit.Id);
        objUnit.Handover_Flag__c = 'Y';
        objUnit.Handover_Notice_Sent__c = false;
        update objUnit;

        Appointment__c objApp = new Appointment__c();
        objApp.Appointment_Date__c = date.newinstance( 2022, 12, 12 );
        objApp.Process_Name__c = 'Handover';
        objApp.Sub_Process_Name__c = 'Documentation';
        objApp.Group_Name__c = 'Handover Queue';
        objApp.Slots__c = '10:00 - 11:00';
        insert objApp;

        Date dateToday = system.today();
        string strToday = string.valueOf(dateToday);
        system.debug('!!!!strTomorrow'+strToday);
        //test.startTest();
        System.runAs(CRM_CommitteeUser) {
            AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
            list<AppointmentSelectionHandler.AppointmentWrapper> listWrap2
                = handler.availableSlots(objAcc.Id, objUnit.Id, 'Handover', 'Unit Viewing', 'CRE', strToday,false);
        }
        //test.stopTest();
    }

    static testMethod void validation5(){

        User CRM_CommitteeUser = new User(
             ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
             LastName = 'last',
             Email = 'abc@new.com',
             Username = 'abc@new.com' + System.currentTimeMillis(),
             CompanyName = 'TEST',
             Title = 'title',
             Alias = 'alias',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             LocaleSidKey = 'en_US');
        insert CRM_CommitteeUser;
        testdata();

        Booking_Unit__c objUnit = new Booking_Unit__c(Id=objBookingUnit.Id);
        objUnit.Handover_Flag__c = 'Y';
        objUnit.Handover_Notice_Sent__c = false;
        update objUnit;

        Appointment__c objApp = new Appointment__c();
        objApp.Appointment_Date__c = date.newinstance( 2022, 12, 12 );
        objApp.Process_Name__c = 'Handover';
        objApp.Sub_Process_Name__c = 'Documentation';
        objApp.Group_Name__c = 'Handover Queue';
        objApp.Slots__c = '10:00 - 11:00';
        insert objApp;

        Date dateToday = system.today();
        string strToday = string.valueOf(dateToday);
        system.debug('!!!!strTomorrow'+strToday);
        //test.startTest();
        System.runAs(CRM_CommitteeUser) {
            AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
            list<AppointmentSelectionHandler.AppointmentWrapper> listWrap2
                = handler.availableSlots(objAcc.Id, objUnit.Id, 'Handover', 'Key Handover', 'CRE', strToday,false);
        }
        //test.stopTest();
    }

    static testMethod void createAppointment1 () {
        testdata();

        Account objAccount = new Account(Id = objAcc.Id);
        objAccount.Primary_CRE__c = UserInfo.getUserId();
        update objAccount;

        Location__c loc = new Location__c();
        loc.Location_ID__c = '22656';
        loc.Name = 'JNU';
        insert loc;

        TriggerOnOffCustomSetting__c objOff = new TriggerOnOffCustomSetting__c ();
        objOff.Name = 'CallingListTrigger';
        objOff.OnOffCheck__c = true;
        insert objOff;

        Appointment__c objApp1 = new Appointment__c();
        objApp1.Processes__c = 'Rental Pool';
        objApp1.Sub_Processes__c  = 'Other';
        objApp1.Group_Name__c = 'Handover Queue';
        objApp1.Assigned_CRE__c = UserInfo.getUserId();
        objApp1.Slots__c = '10:00 - 11:00';
        objApp1.Appointment_Date__c = system.today().addDays(-2);
        objApp1.Appointment_End_Date__c = system.today().addDays(2);
        insert objApp1;

        Date dateToday = system.today();
        string strToday = string.valueOf(dateToday);

        test.startTest();
        AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
        list<AppointmentSelectionHandler.AppointmentWrapper> listWrap
            = handler.availableSlots(objAccount.Id, objBookingUnit.Id, 'Rental Pool', 'Other', 'CRE', strToday,false);
        listWrap[0].isSelected = true;
        strToday = strToday.replaceAll('-','/');
        handler.createAppointments(listWrap, 'purpose', objBookingUnit.Id, objAccount, 'Other', 'Rental Pool', '12/12/2019');
        test.stopTest();
    }

    static testMethod void createAppointment2 () {
        testdata();

        Account objAccount = new Account(Id = objAcc.Id);
        objAccount.Primary_CRE__c = UserInfo.getUserId();
        objAccount.Email__c = 'test@new.com';
        update objAccount;

        Booking_Unit__c objUnit = new Booking_Unit__c(Id=objBookingUnit.Id);
        objUnit.Handover_Flag__c = 'N';
        objUnit.Handover_Notice_Sent__c = true;
        update objUnit;

        Location__c loc = new Location__c();
        loc.Location_ID__c = '22656';
        loc.Name = 'JNU';
        insert loc;

        TriggerOnOffCustomSetting__c objOff = new TriggerOnOffCustomSetting__c ();
        objOff.Name = 'CallingListTrigger';
        objOff.OnOffCheck__c = true;
        insert objOff;

        Appointment__c objApp1 = new Appointment__c();
        objApp1.Processes__c = 'Handover';
        objApp1.Sub_Processes__c  = 'Unit Viewing';
        objApp1.Group_Name__c = 'Collection Queue';
        objApp1.Sub_Process_Name__c = 'Unit Viewing';
        //objApp1.Assigned_CRE__c = UserInfo.getUserId();
        objApp1.Slots__c = '10:00 - 11:00';
        objApp1.Appointment_Date__c = system.today().addDays(15);
        objApp1.Appointment_End_Date__c = system.today().addDays(20);
        insert objApp1;

        Date dateToday = system.today().addDays(16);
        string strToday = string.valueOf(dateToday);

        test.startTest();
        AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
        list<AppointmentSelectionHandler.AppointmentWrapper> listWrap
            = handler.availableSlots(objAccount.Id, objUnit.Id, 'Handover', 'Unit Viewing', 'CRE', strToday,false);
        listWrap[0].isSelected = true;
        strToday = strToday.replaceAll('-','/');
        handler.createAppointments(listWrap, 'purpose', objUnit.Id, objAccount, 'Unit Viewing', 'Handover', '02/12/2019',true);
        test.stopTest();
    }

    static testMethod void createAppointment3 () {
        testdata();

        Account objAccount = new Account(Id = objAcc.Id);
        objAccount.Primary_CRE__c = UserInfo.getUserId();
        objAccount.Email__c = 'test@new.com';
        update objAccount;

        Booking_Unit__c objUnit = new Booking_Unit__c(Id=objBookingUnit.Id);
        objUnit.Handover_Flag__c = 'N';
        objUnit.Handover_Notice_Sent__c = true;
        objUnit.Okay_to_release_keys__c = true;
        objUnit.Snags_completed__c = true;
        objUnit.Snag_Status_Onsite__c ='Ready for Handover';
        update objUnit;

        Location__c loc = new Location__c();
        loc.Location_ID__c = '22656';
        loc.Name = 'JNU';
        insert loc;

        TriggerOnOffCustomSetting__c objOff = new TriggerOnOffCustomSetting__c ();
        objOff.Name = 'CallingListTrigger';
        objOff.OnOffCheck__c = true;
        insert objOff;

        Appointment__c objApp1 = new Appointment__c();
        objApp1.Processes__c = 'Handover';
        objApp1.Sub_Processes__c  = 'Key Handover';
        objApp1.Sub_Process_Name__c = 'Key Handover';
        objApp1.Group_Name__c = 'Collection Queue';
        objApp1.Slots__c = 'test-test';
        //objApp1.Assigned_CRE__c = UserInfo.getUserId();
        objApp1.Slots__c = '10:00 - 11:00';
        objApp1.Appointment_Date__c = system.today().addDays(15);
        objApp1.Appointment_End_Date__c = system.today().addDays(20);
        //objApp1.Snags_completed__c = true;
        insert objApp1;

        Date dateToday = system.today().addDays(16);
        string strToday = string.valueOf(dateToday);

        test.startTest();
        AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
        list<AppointmentSelectionHandler.AppointmentWrapper> listWrap
            = handler.availableSlots(objAccount.Id, objUnit.Id, 'Handover', 'Key Handover', 'CRE', strToday,false);
        listWrap[0].isSelected = true;
        strToday = strToday.replaceAll('-','/');
        handler.createAppointments(listWrap, 'purpose', objUnit.Id, objAccount, 'Key Handover', 'Handover', '02/12/2019');
        test.stopTest();
    }
    static testMethod void createAppointment4 () {
        testdata();

        Account objAccount = new Account(Id = objAcc.Id);
        objAccount.Primary_CRE__c = UserInfo.getUserId();
        objAccount.Email__c = 'test@new.com';
        update objAccount;

        Booking_Unit__c objUnit = new Booking_Unit__c(Id=objBookingUnit.Id);
        objUnit.Handover_Flag__c = 'N';
        objUnit.Handover_Notice_Sent__c = true;
        objUnit.Okay_to_release_keys__c = true;
        objUnit.Snags_completed__c = true;
        objUnit.Snag_Status_Onsite__c ='Ready for Handover';
        update objUnit;

        Location__c loc = new Location__c();
        loc.Location_ID__c = '22656';
        loc.Name = 'JNU';
        insert loc;

        TriggerOnOffCustomSetting__c objOff = new TriggerOnOffCustomSetting__c ();
        objOff.Name = 'CallingListTrigger';
        objOff.OnOffCheck__c = true;
        insert objOff;

        Appointment__c objApp1 = new Appointment__c();
        objApp1.Processes__c = 'Handover';
        objApp1.Sub_Processes__c  = 'Unit Viewing or Key Handover';
        objApp1.Sub_Process_Name__c = 'Documentation';
        objApp1.Group_Name__c = 'Collection Queue';
        //objApp1.Assigned_CRE__c = UserInfo.getUserId();
        objApp1.Slots__c = '10:00 - 11:00';
        objApp1.Appointment_Date__c = system.today().addDays(15);
        objApp1.Appointment_End_Date__c = system.today().addDays(20);
        //objApp1.Snags_completed__c = true;
        insert objApp1;

        Date dateToday = system.today().addDays(16);
        string strToday = string.valueOf(dateToday);

        test.startTest();
        AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
        list<AppointmentSelectionHandler.AppointmentWrapper> listWrap
            = handler.availableSlots(objAccount.Id, objUnit.Id, 'Handover', 'Key Handover', 'CRE', strToday,false);
        // listWrap[0].isSelected = true;
        strToday = strToday.replaceAll('-','/');
        handler.createAppointments(listWrap, 'purpose', objUnit.Id, objAccount, 'Key Handover', 'Handover', '03/12/2019');
        test.stopTest();
    }
}