@isTest
private class ResponseTranscriptTest{
    
    
    @testSetup
    static void setup() {
      
        Call_Log__c objCallLog = new Call_Log__c();
        objCallLog.Call_Recording_URL__c = '34534dfsgabds';
        objCallLog.Calling_Number__c = '342354234';       
        insert objCallLog;
    }
    
    
    
    @isTest static void controllerTest() {
        
        Call_Log__c objCallLog = [Select Id From Call_Log__c];

        Test.startTest();
        //As Per Best Practice it is important to instantiate the Rest Context
        String JsonMsg = objCallLog.Id+'hjkr325sv324'+'string (i will make json into string and send)';
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/ResponseTranscript';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;

        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1));
        ResponseTranscript.TranscriptDataFormat  obj = ResponseTranscript.saveTranscriptLog(objCallLog.Id,'hjkr325sv324','string (i will make json into string and send)');
        Test.stopTest();

    }
}