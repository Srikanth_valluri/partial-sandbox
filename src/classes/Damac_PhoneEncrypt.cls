public class Damac_PhoneEncrypt {
    
    public static String encryptPhoneNumber (String mobileNumber) {
        String key = Label.Waybeo_Key;
        //String key = '3599538814476063';
        Blob presetIV = Blob.valueOf(key);
        Blob cryptoKey = Blob.valueOf(key);
        Blob data = Blob.valueOf(mobileNumber);
        Blob encryptedData = Crypto.encrypt('AES128', cryptoKey, presetIV, data);
        String b64Data = EncodingUtil.convertToHex(encryptedData);
        System.Debug (b64Data);
        return b64Data;
    }
    public static String decryptPhoneNumber (String encryptedMobileNumber) {
        String key = Label.Waybeo_Key;
        Blob cryptoKey = Blob.valueOf(key);
        Blob presetIV = Blob.valueOf(key);
        Blob data = EncodingUtil.convertFROMHex(encryptedMobileNumber);
        Blob decryptedData = Crypto.decrypt('AES128', cryptoKey, presetIV, data);
        String dryptData = decryptedData.toString();
        System.Debug (dryptData);
        return dryptData;
    }
    
   

   
    

}