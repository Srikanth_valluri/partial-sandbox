public class Damac_AsyncSendEmail implements Queueable {
    List <ID> metricsIds = new List <ID> ();
    public Damac_AsyncSendEmail (List <ID> recordIds) {
        metricsIds = recordIds;
    }
    public void execute(QueueableContext context) {
        Damac_SendgridEmailController.sendEmailAsync (metricsIds);        
    }
}