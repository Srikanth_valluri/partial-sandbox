/**********************************************************************************************************************
Description: This API is used for getting the Tenant Details
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    	| Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   23-10-2020		| Anand Venkitakrishnan | Created
***********************************************************************************************************************/

@RestResource(urlMapping='/getTenantDetails/*')
global class GetTenantDetailsAPI {
    
    public static final String NO_FMCASES = 'Please enter valid access code';
    public static final String NO_BU_FOUND = 'The entered access code is not valid';
    public static final String SUBMITTED_CASE = 'Given case is already submitted';
    public static final String SUCCESS = 'Successful';
    public static List<String> invalidStatusForCaseReturning = new List<String>{'Submitted'};
    public static String responseMessage;
    
    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };
    
	/***********************************************************************************************
    Method Name : getTenancyRenewalSRFromId
    Description : Returns the Tenancy Renewal SR (FM Case) details for given fmCaseId
    Parameter(s): None
    Return Type : FinalReturnWrapper (Wrapper)
    ************************************************************************************************/
    @HttpGet
    global static FinalReturnWrapper getTenancyRenewalSRFromId() {
		
        FinalReturnWrapper retunResponse = new FinalReturnWrapper();
        cls_meta_data objMeta = new cls_meta_data();
        cls_data objData = new cls_data();
		
        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);
		
        if(!r.params.containsKey('bookingUnitId')) {
            objMeta.message = 'Missing parameter : bookingUnitId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
            
            retunResponse.meta_data = objMeta;
            return retunResponse;
        }
        else if(r.params.containsKey('bookingUnitId') && String.isBlank(r.params.get('bookingUnitId'))) {
            objMeta.message = 'Missing parameter value: bookingUnitId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
			
            retunResponse.meta_data = objMeta;
            return retunResponse;
        }
        else if(!r.params.containsKey('accountId')) {
            objMeta.message = 'Missing parameter : accountId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
            
            retunResponse.meta_data = objMeta;
            return retunResponse;
        }
        else if(r.params.containsKey('accountId') && String.isBlank(r.params.get('accountId'))) {
            objMeta.message = 'Missing parameter value: accountId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
			
            retunResponse.meta_data = objMeta;
            return retunResponse;
        }
        
        if(String.isNotBlank(r.params.get('fmCaseId'))) {
            List<FM_Case__c> lstFmCase = GetTenantRegistrationCase_API.getFMCase(r.params.get('fmCaseId'));
            
            if(lstFmCase != null && !lstFmCase.isEmpty() && invalidStatusForCaseReturning.contains(lstFmCase[0].Status__c)) {
            	System.debug('lstFmCase[0].Status__c ::' + lstFmCase[0].Status__c);
                
                objMeta.message = SUBMITTED_CASE;
                objMeta.status_code = 2;
                objMeta.title = mapStatusCode.get(2);
                objMeta.developer_message = null;
				
                retunResponse.meta_data = objMeta;
                return retunResponse;
            }
			
            FmCaseWrapper objFmCaseWrapper = fetchTenantRegFmCase(lstFmCase, TRUE);
			
            responseMessage = objFmCaseWrapper != null ? SUCCESS : NO_FMCASES;
            
            System.debug('responseMessage:' + responseMessage);
            objMeta.message = responseMessage;
            objMeta.status_code = responseMessage == SUCCESS ? 1 : 3;
            objMeta.title = mapStatusCode.get(objMeta.status_code);
            objMeta.developer_message = null;
			
            objData.fm_case = responseMessage == SUCCESS ? objFmCaseWrapper : null;
			
            retunResponse.data = objData;
            retunResponse.meta_data = objMeta;
			
            System.debug('retunResponse:'+retunResponse);
        }
		else if(String.isNotBlank(r.params.get('bookingUnitId'))) {
            String accountId = r.params.get('accountId');
            String bookingUnitId = r.params.get('bookingUnitId');
            
            List<FM_Case__c> lstTenantRegnFmCase = [SELECT Id,Name,Booking_Unit__c FROM FM_Case__c WHERE (Tenant__c = :accountId OR Account__c = :accountId) 
                                                    AND Booking_Unit__c = :bookingUnitId 
                                                    AND Request_Type_DeveloperName__c = 'Tenant_Registration' 
                                                    AND Status__c = 'Closed' 
                                                    ORDER BY CreatedDate DESC];
            
            FM_Case__c tenantRegnFmCase = new FM_Case__c();
            
            if(lstTenantRegnFmCase != NULL && lstTenantRegnFmCase.size() > 0) {
                tenantRegnFmCase = lstTenantRegnFmCase[0];
            }
            
            List<FM_Case__c> lstFmCase = GetTenantRegistrationCase_API.getFMCase(tenantRegnFmCase.Id);
            
            FmCaseWrapper objFmCaseWrapper  = fetchTenantRegFmCase(lstFmCase, FALSE);
            
            if(objFmCaseWrapper == NULL) {
                objFmCaseWrapper = new FmCaseWrapper();
            }
            
            responseMessage = objFMCaseWrapper != null ? SUCCESS : NO_FMCASES;
            
            if(objFMCaseWrapper != null) {
                System.debug('bookingUnitId:'+r.params.get('bookingUnitId'));
                System.debug('objFMCase.booking_unit_id:'+objFMCaseWrapper.booking_unit_id);
                System.debug('bookingUnitId length:'+r.params.get('bookingUnitId').length());
				
                if(r.params.get('bookingUnitId').length() >= 15) {
                    if(objFMCaseWrapper.booking_unit_id != NULL) {
                        responseMessage = objFMCaseWrapper.booking_unit_id.substring(0,15) == r.params.get('bookingUnitId').substring(0,15) ? SUCCESS : NO_BU_FOUND;
                    }
                }
                else {
                    responseMessage = NO_BU_FOUND;
                }
            }
			
            System.debug('responseMessage:' + responseMessage);
            objMeta.message = responseMessage;
            objMeta.status_code = responseMessage == SUCCESS ? 1 : 3;
            objMeta.title = mapStatusCode.get(objMeta.status_code);
            objMeta.developer_message = null;
			
            objData.fm_case = responseMessage == SUCCESS ? objFMCaseWrapper : null;
			
            retunResponse.data = objData;
            retunResponse.meta_data = objMeta;
			
            System.debug('retunResponse:'+retunResponse);
        }
        
        return retunResponse;
    }
    
    public static FmCaseWrapper fetchTenantRegFmCase(List<FM_Case__c> lstFmCase, Boolean isRenewalCase) {
        FmCaseWrapper objFmCaseWrapper = new FmCaseWrapper();
        System.debug('lstFmCase:'+lstFmCase);  
        
        if(lstFmCase.size() > 0) {
            if(isRenewalCase) {
                objFmCaseWrapper.id = lstFmCase[0].id;		//Check if correct
                objFmCaseWrapper.fm_case_number = lstFmCase[0].Name;
            }
            
            objFmCaseWrapper.booking_unit_id = lstFmCase[0].Booking_Unit__c;
            objFmCaseWrapper.first_name = lstFmCase[0].First_Name__c;
            objFmCaseWrapper.last_name = lstFmCase[0].Last_Name__c;
            objFmCaseWrapper.gender = lstFmCase[0].Gender__c;
            objFmCaseWrapper.date_of_birth = lstFmCase[0].Date_of_Birth__c != null? Datetime.newInstance(lstFmCase[0].Date_of_Birth__c.year(), lstFmCase[0].Date_of_Birth__c.month(), lstFmCase[0].Date_of_Birth__c.day()).format('yyyy-MM-dd') : '';
            
            objFmCaseWrapper.nationality = lstFmCase[0].Nationality__c;
            objFmCaseWrapper.passport_number = lstFmCase[0].Passport_Number__c;
            objFmCaseWrapper.passport_expiry_date = lstFmCase[0].Expiry_Date__c != null ? Datetime.newInstance(lstFmCase[0].Expiry_Date__c.year(), lstFmCase[0].Expiry_Date__c.month(), lstFmCase[0].Expiry_Date__c.day()).format('yyyy-MM-dd') : '';
            objFmCaseWrapper.emirates_id = lstFmCase[0].Emirates_Id__c;
            objFmCaseWrapper.mobile_code = lstFmCase[0].Mobile_Country_Code__c;
            objFmCaseWrapper.mobile_number = lstFmCase[0].Mobile_no__c;
            objFmCaseWrapper.email_address = lstFmCase[0].Tenant_Email__c;
            objFmCaseWrapper.no_of_adults = lstFmCase[0].No_of_Adults__c;
            objFmCaseWrapper.no_of_children = lstFmCase[0].No_of_Children__c;
            
            if(isRenewalCase) {
                objFmCaseWrapper.lease_start_date = lstFmCase[0].Start_Date__c != null ? Datetime.newInstance(lstFmCase[0].Start_Date__c.year(), lstFmCase[0].Start_Date__c.month(), lstFmCase[0].Start_Date__c.day()).format('yyyy-MM-dd') : '';
                objFmCaseWrapper.lease_end_date = lstFmCase[0].End_Date__c != null ? Datetime.newInstance(lstFmCase[0].End_Date__c.year(), lstFmCase[0].End_Date__c.month(), lstFmCase[0].End_Date__c.day()).format('yyyy-MM-dd') : '';
                objFmCaseWrapper.ejari_number = lstFmCase[0].Ejari_Number__c;
            }
            
            objFmCaseWrapper.is_person_with_spl_needs = lstFmCase[0].isPersonWithSpecialNeeds__c;
            objFmCaseWrapper.is_having_pets = lstFmCase[0].isHavingPets__c;
			
            List<cls_emergency_contact> lstEmer = new List<cls_emergency_contact>();
            
            if(lstFmCase[0].Emergency_Contact_Details__r.size() > 0) {
                for(FM_Additional_Detail__c objFmEmer : lstFmCase[0].Emergency_Contact_Details__r) {
                    cls_emergency_contact objEmer = new cls_emergency_contact();
                    
                    objEmer.id = objFmEmer.id;
                    objEmer.emergency_full_name = objFmEmer.Name__c;
                    objEmer.emergency_relationship = objFmEmer.Relationship__c;
                    objEmer.emergency_email_address = objFmEmer.Email__c;
                    objEmer.emergency_mobile_number = objFmEmer.Mobile__c;
                    
                    lstEmer.add(objEmer);
                }
            }
            
            List<cls_additional_members> lstResidents = new List<cls_additional_members>();
            
            if(lstFmCase[0].Resident_Details__r.size() > 0) {
                for(FM_Additional_Detail__c obj : lstFmCase[0].Resident_Details__r) {
                    cls_additional_members objResident = new cls_additional_members();
                    
                    objResident.id = obj.id;
                    list<String> names = String.isNotBlank(obj.Name__c) ? obj.Name__c.split(' ') : new List<String>();
                    if(names.size() > 0) {
                        objResident.member_first_name = names[0];
                    }
                    if(names.size() == 2) {
                        objResident.member_last_name = names[1];
                    }
                    
                    objResident.member_gender = obj.Gender__c;
                    objResident.member_date_of_birth = obj.Date_of_Birth__c != null ? Datetime.newInstance(obj.Date_of_Birth__c.year(), obj.Date_of_Birth__c.month(), obj.Date_of_Birth__c.day()).format('yyyy-MM-dd') : '';
                    objResident.member_nationality = obj.Nationality__c;
                    objResident.member_passport_number = obj.Passport_Number__c;
                    objResident.member_emirates_id = obj.Identification_Number__c;
                    objResident.member_mobile_number = obj.Mobile__c;
                    objResident.member_email_address = obj.Email__c;

                    lstResidents.add(objResident);
                }
            }
            
            list<cls_attachment> lstAttachments = new list<cls_attachment>();
            
            if(lstFmCase[0].Documents__r.size() > 0) {
                System.debug('lstFmCase[0].Documents__r'+lstFmCase[0].Documents__r);
                for(SR_Attachments__c obj : lstFmCase[0].Documents__r) {
                    cls_attachment objAttach = new cls_attachment();
                    objAttach.id = obj.id;
                    objAttach.name = obj.Name;
                    objAttach.file_extension = obj.Type__c;
                    objAttach.attachment_url = obj.Attachment_URL__c;
                    
                    lstAttachments.add(objAttach);
                }
            }
            
            System.debug('lstResidents:'+lstResidents);
            System.debug('lstEmer:'+lstEmer);
            System.debug('lstAttachments:'+lstAttachments);
            
            objFmCaseWrapper.additional_members = lstResidents;
            objFmCaseWrapper.emergency_contact_details = lstEmer;
            objFmCaseWrapper.attachments = lstAttachments;
        }
        else {
            objFmCaseWrapper = null;
        }         
        
        System.debug('objFmCaseWrapper: ' + objFmCaseWrapper);
        
        return objFmCaseWrapper;        
    }
    
    public FM_Case__c getTenantRgistrationCaseDetails(String accountId,String bookingUnitId) {
        if(String.isNotBlank(accountId) && String.isNotBlank(bookingUnitId)) {
             List<FM_Case__c> lstCase = [SELECT Id
                                         //Case Details
                                         , Account__c
                                         , Booking_Unit__c
                                         , Request_Type__c
                                         , Origin__c
                                         , Booking_Unit__r.Property_City__c
                                         
                                         //Tenant Details
                                         , First_Name__c
                                         , Last_Name__c
                                         , Gender__c
                                         , Date_of_Birth__c
                                         , Nationality__c
                                         , Passport_Number__c
                                         , Expiry_Date__c
                                         , Emirates_Id__c
                                         , People_of_Determination__c
                                         , Tenant__c
                                         
                                         //Employment Info
                                         , Current_Work_Status__c
                                         , Company__c
                                         , Designation__c
                                         
                                         //Lease Details
                                         , Ejari_Number__c
                                         , DEWA_Number__c
                                         , No_of_Adults__c
                                         , No_of_Children__c
                                         , Short_Term_Lease__c
                                         , Access_card_number__c
                                         
                                         //Contact Details
                                         , Mobile_Country_Code__c
                                         , Mobile_Country_Code_2__c
                                         , Mobile_Country_Code_3__c
                                         , Mobile_Country_Code_4__c
                                         , Mobile_Country_Code_5__c
                                         , Mobile_no__c
                                         , Mobile_Phone_2__c
                                         , Mobile_Phone_3__c
                                         , Mobile_Phone_4__c
                                         , Mobile_Phone_5__c
                                         , Email__c
                                         , Email_2__c
                                         , Email_3__c
                                         , Email_4__c
                                         , Email_5__c
                                         , Tenant_Email__c
                                         , FM_Admin_Email__c
                                         , FM_Manager_Email__c
                                         , Start_Date__c
                                         , End_Date__c
                                         
                                         FROM FM_Case__c
                                         WHERE (Tenant__c = :accountId OR Account__c = :accountId)
                                         AND Booking_Unit__c = :bookingUnitId
                                         AND Request_Type_DeveloperName__c = 'Tenant_Registration'
                                         AND Status__c = 'Closed'
                                         ORDER BY CreatedDate DESC
                                         LIMIT 1];
            
            if(lstCase != NULL && !lstCase.isEmpty() ) {
                return lstCase[0];
            }
        }
		
        return NULL;
    }
    
    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }
	
    public class cls_data {
        public FMCaseWrapper fm_case;
    }
    
    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message;   
    }
    
    public class FMCaseWrapper {
        public String id;
        public String fm_case_number;
        public String booking_unit_id;
        public String first_name;
        public String last_name;
        public String gender;
        public String date_of_birth;
        public String no_of_adults;
        public String no_of_children;
        public boolean is_person_with_spl_needs;
        public boolean is_having_pets;
        
        public String nationality;
        public String passport_number;
        public String passport_expiry_date;
        public String emirates_id;
        
        public String mobile_code;
        public String mobile_number;
        public String email_address;
        
        public String lease_start_date;
        public String lease_end_date;
        public String ejari_number;
        
        public cls_additional_members[] additional_members;
        
        public cls_emergency_contact[] emergency_contact_details;
        
        public cls_attachment[] attachments;
    }
    
    public class cls_emergency_contact {
        public String id;
        public String emergency_full_name;
        public String emergency_relationship;
        public String emergency_email_address;
        public String emergency_mobile_code;
        public String emergency_mobile_number;
    }
    
    public class cls_additional_members {
        public String id;
        public String member_first_name;
        public String member_last_name;
        public String member_gender;
        public String member_date_of_birth;
        public String member_nationality;
        public String member_passport_number;
        //public String member_passport_expiry_date;
        public String member_emirates_id;
        //public String member_country_code;
        public String member_mobile_number;
        public String member_email_address;
    }
	
    public class cls_attachment {
        public String id;
        public String name;
        public String attachment_url;
        public String file_extension;
    }
}