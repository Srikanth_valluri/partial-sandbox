@RestResource(urlMapping='/sendNotification/sms')
global class HD_SendNotificationBySMS_API {
    public static Map<Integer, String> statusCodeMap;
    static {
        statusCodeMap = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
        };
    }
    
    @HTTPPost
    global static void doPost(String party_id, String mobile_number, String message, String sender_type){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        RestContext.response.addHeader('Content-Type', 'application/json');
        String errorMsg, exceptionMsg, successMsg;
        
        if(String.isBlank(party_id) && String.isBlank(mobile_number)){
            errorMsg = 'Either party_id or mobile_number should be provided.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
            return;
        }
        
        if(String.isBlank(message)){
            errorMsg = 'Message is not provided.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
            return;
        }
        
        String mobileNumber;
        Account party;
        if(String.isNotBlank(party_id)){
            try{
            	party = [SELECT id, name, IsPersonAccount, Party_ID__c, Party_Type__c, Country__c, 
                     Email__c, Mobile_Country_Code__c, Mobile_Phone_Encrypt__c, Country__pc, 
                     Email__pc, Mobile_Country_Code__pc, Mobile_Phone_Encrypt__pc 
                     FROM Account WHERE Party_ID__c = :party_id LIMIT 1];
            }
            catch(Exception ex){
                system.debug(ex.getMessage());
                system.debug(ex.getStackTraceString());
                exceptionMsg = ex.getStackTraceString();
                party = NULL;
            }
            
            if(NULL == party){
                errorMsg = 'The Party ID provided does not match any Account records.';
                getErrorResponse(2, statusCodeMap.get(2), errorMsg, exceptionMsg);
                return;
            }
            
            mobileNumber = party.IsPersonAccount? party.Mobile_Phone_Encrypt__pc : party.Mobile__c;
            if(String.isBlank(mobileNumber)){
                errorMsg = 'The Party Account does not have a mobile number.';
                getErrorResponse(2, statusCodeMap.get(2), errorMsg, exceptionMsg);
                return;
            }
            /* String countryCode = party.IsPersonAccount? party.Mobile_Country_Code__pc : party.Mobile_Country_Code__c;
            mobileNumber = (String.isNotBlank(countryCode) ? 
                countryCode.substringAfterLast(':').trim() : '') + 
                mobileNumber; */
        }
        else {
            /* CASE: when mobile number is provided directly as a parameter */
            mobileNumber = mobile_number;
        }
        
        /* validate mobile number and send SMS */
        if(mobileNumber.startsWith('00')) {
            mobileNumber = mobileNumber.removeStart('00');
        }else if(mobileNumber.startsWith('0')) {
            mobileNumber = mobileNumber.removeStart('0');
        }
        
        String un_SMS_Service = Label.Damac_CRM_SMS_Service_User_name; /* Label.SF_SMS_Service_User_name */
        String pw_SMS_Service = Label.Damac_CRM_SMS_Service_Password; /* Label.SF_SMS_Service_Password */
        //String senderID_SMS_Service = Label.Damac_CRM_SMS_Service_Sender_Id; /* Label.SF_SMS_Service_Sender_Id */
        String senderID_SMS_Service = SMSClass.getSenderName(un_SMS_Service, mobileNumber, false);
        if(String.isNotBlank(sender_type) && sender_type.equalsIgnoreCase('LOAMS')) {
            un_SMS_Service = Label.FM_SMS_service_username;
            pw_SMS_Service = Label.FM_SMS_service_password;
            //senderID_SMS_Service = Label.FM_SMS_Service_SenderId;
            senderID_SMS_Service = SMSClass.getSenderName(un_SMS_Service, mobileNumber, false);
        }
        
        String encodedMessage = GenericUtility.encodeChar(message);
        
        System.debug('Initiating HTTP callout to send SMS: {mobile# : ' + mobileNumber + ', message : ' + message + '}');
        Http smsHttp = new Http();
        HttpRequest smsRequest = new HttpRequest();
        smsRequest.setMethod('POST' );
        smsRequest.setEndpoint('http://api.smscountry.com/SMSCwebservice_bulk.aspx');
        smsRequest.setBody('user='+ un_SMS_Service 
                           + '&passwd=' + pw_SMS_Service 
                           + '&message=' + encodedMessage 
                           + '&mobilenumber=' + mobileNumber 
                           + '&sid=' + senderID_SMS_Service 
                           + '&MTYPE=LNG&DR=Y'); /* setting POST Request Parameters */
        System.debug('SMS request body = ' + smsRequest.getBody());
        
        HTTPResponse smsResponse;
        String smsServiceResponseBody, smsID;
        Boolean smsSentStatus = false;
        try{
            if(!Test.isRunningTest()) {
                smsResponse = smsHttp.send(smsRequest);
            }
            else {
                smsResponse = new HTTPResponse();
            	smsResponse.setHeader('Content-Type', 'text/plain');
                smsResponse.setStatus('OK');
                smsResponse.setStatusCode(200);
                
                String smsResponseText = (mobileNumber.endsWithIgnoreCase('666')) ? 'ERROR:No Balance' : 'OK:543210123';
                
                smsResponse.setBody(smsResponseText);
            }
            smsServiceResponseBody = smsResponse.getBody();
        	System.debug('SMS response body = ' + smsServiceResponseBody);
            if(String.isNotBlank(smsServiceResponseBody)){
                smsSentStatus = true;
                if(smsServiceResponseBody.contains('OK:')){
                    smsID = smsServiceResponseBody.substringAfter('OK:');
                }
                else {
                    errorMsg = 'SMS service callout failed. Failed to send SMS notification.';
                    getErrorResponse(2, statusCodeMap.get(2), errorMsg, smsServiceResponseBody);
                    return;
                }
            }
            else {
                errorMsg = 'SMS service callout failed. Failed to send SMS notification.';
                getErrorResponse(2, statusCodeMap.get(2), errorMsg, NULL);
                return;
            }
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
            system.debug(ex.getStackTraceString());
            errorMsg = 'SMS service callout failed. Failed to send SMS notification.';
            getErrorResponse(2, statusCodeMap.get(2), errorMsg, ex.getStackTraceString());
            return;
        }
        
        Boolean smsPersistenceStatus = false;
        if(String.isNotBlank(party_id) && NULL != party && smsSentStatus){
            /* update in SMS_History__c record */
            smsPersistenceStatus = persistNotificationSentEvent(mobileNumber, encodedMessage, 
                                                                party.id, NULL, smsSentStatus, 
                                                                smsServiceResponseBody, smsId);
        }
        
        if(!smsServiceResponseBody.contains('OK:')){
            errorMsg = 'SMS service callout failed. Failed to send SMS notification.';
            getErrorResponse(2, statusCodeMap.get(2), errorMsg, smsServiceResponseBody);
            return;
        }
        
        ResponseWrapper responseWrapper = new ResponseWrapper();
        
        cls_meta_data responseMetaData = new cls_meta_data();
        responseMetaData.status_code = 1;
        responseMetaData.title = statusCodeMap.get(1);
        responseMetaData.message = 'Success in sending notification by SMS';
        responseMetaData.developer_message = null;
        responseWrapper.meta_data = responseMetaData;
        
        cls_data responseData = new cls_data();
        responseData.sms_id = smsID;
        responseWrapper.data = responseData;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
    }
    
    private static Boolean persistNotificationSentEvent(String mobileNumber, String encodedMessage, 
    ID accountId, ID callingListId, Boolean smsSentStatus, String smsServiceResponseText, String smsId){
        Id smsHistoryRecTypID = Schema.SObjectType.SMS_History__c
            .getRecordTypeInfosByName().get('SMS History').getRecordTypeId();
        
        SMS_History__c persist_SMS = new SMS_History__c();
        persist_SMS.recordTypeId = smsHistoryRecTypID;
        persist_SMS.Message__c = encodedMessage;
        persist_SMS.Phone_Number__c = mobileNumber;
        persist_SMS.Customer__c = accountId;
        persist_SMS.Calling_List__c = NULL; /* ??? */
        persist_SMS.Description__c = smsServiceResponseText;
        persist_SMS.Is_SMS_Sent__c = smsSentStatus;
        persist_SMS.sms_Id__c = smsId;
        persist_SMS.isFMSMS__c = false;
        
        try{
            insert persist_SMS;
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
            system.debug(ex.getStackTraceString());
            return false;
        }
        
        return true;
    }
    
    private static void getErrorResponse(Integer statusCode, String title, 
    String responseMessage, String devMessage) {
        ResponseWrapper responseWrapper = new ResponseWrapper();
        cls_meta_data responseMetaData = new cls_meta_data();
        responseMetaData.status_code = statusCode;
        responseMetaData.title = title;
        responseMetaData.message = responseMessage;
        responseMetaData.developer_message = devMessage;
        responseWrapper.meta_data = responseMetaData;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
    }
    
    /* Wrapper classes for returning reponse */
    public class ResponseWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_data {
        public String sms_id;
    }

    public class cls_meta_data {
        public Integer status_code;
        public String message;
        public String title;
        public String developer_message;   
    }
}