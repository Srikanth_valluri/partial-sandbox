/*
* Description - Test class developed for 'RentalPoolTerminationHelper'
*
* Version            Date            Author            Description
* 1.0              28/02/2018        Ashish           Initial Draft
*/

@isTest 
private class RentalPoolTerminationHelperTest {

    static testMethod void testGetCaseAndUnitDetailsWrapper() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;

        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        list<Case> lstCases = new list<Case>();
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC';
           Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId() );
           objCase.Booking_Unit__c = objUnit.Id;
           lstCases.add( objCase ); 
        }
        insert lstBookingUnits;
        insert lstCases ;
        
        test.startTest();
        	RentalPoolTerminationHelper.getCaseAndUnitDetailsWrapper( lstBookingUnits[0], new set<String>{ 'Rental Pool Termination' } );
        test.stopTest();
    }
    
    static testMethod void testCreateTaskObject() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId() );
        objCase.OwnerId = UserInfo.getUserId();
        insert objCase ;
        
        test.startTest();
        	RentalPoolTerminationHelper.createTaskObject('', '', '', '', '', objCase, '' );
        test.stopTest();
    }
    
    static testMethod void testExtractBody() {        
        test.startTest();
        	RentalPoolTerminationHelper.extractBody( '' );
        test.stopTest();
    }
    
    static testMethod void testExtractName() {        
        test.startTest();
        	RentalPoolTerminationHelper.extractName( '' );
        test.stopTest();
    }
    
    static testMethod void testCreateAttributesWrapper() {        
        test.startTest();
        	RentalPoolTerminationHelper.createAttributesWrapper( new Booking_Unit__c(), new UnitDetailsService.BookinUnitDetailsWrapper() );
        test.stopTest();
    }
    
    static testMethod void testCreateWrapperObject() {        
        test.startTest();
        	RentalPoolTerminationHelper.createWrapperObject( new Case() ,'' );
        test.stopTest();
    }
}