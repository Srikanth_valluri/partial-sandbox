/***********************************************************************************************
* Description - Test class developed for DocumentationForKeyHandoverService
*
* Version            Date            Author                    Description
* 1.0                17/12/17        Naresh Kaneriya (Accely)   Initial Draft
*********************************************************************************************/


@isTest
public class DocumentationForKeyHandoverServiceTest{

@isTest static void getTest(){
 
      Test.startTest();
      DocumentationForKeyHandoverService.DoumentationForKeyHandoverRuleHttpSoap11Endpoint  obj =  new DocumentationForKeyHandoverService.DoumentationForKeyHandoverRuleHttpSoap11Endpoint(); 
      SOAPCalloutServiceMock.returnToMe = new Map<String,DocumentationForKeyHandoverService.DoumentationForKeyHandoverResponse_element>();
      DocumentationForKeyHandoverService.DoumentationForKeyHandoverResponse_element response1 = new DocumentationForKeyHandoverService.DoumentationForKeyHandoverResponse_element();
      response1.return_x = 'S';
      SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
      Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
      obj.DoumentationForKeyHandover('RegistrationId','ProcessName','SubProcesName','ProjectCity','Project','BuildingCode','PermittedUse','BedroomType','ApplicableUnits','UnitType','Nationality','TypeOfCustomer','POA');
      Test.stopTest();

}

}