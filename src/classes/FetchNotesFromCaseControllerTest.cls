/***********************************************************************
* Description - Test class developed for FetchNotesFromCaseController
*
* Version            Date            Author            Description
* 1.0                12/12/17        Naresh (accely)           Initial Draft
**********************************************************************/

@isTest(seeAllData=false)
public class FetchNotesFromCaseControllerTest{
    
    
    public static testmethod void getTested(){
        Test.startTest(); 
        
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
        Account acc = TestDataFactory_CRM.createPersonAccount();
        insert acc;
        
        Case newCase = TestDataFactory_CRM.createCase(acc.Id ,devRecordTypeId);
        insert newCase; 
        
        ApexPages.currentPage().getParameters().put('id',acc.id);
        
        ContentVersion contentVersion = new ContentVersion(
        Title = 'Test',
        PathOnClient = 'test.jpg',
        VersionData = Blob.valueOf('Test Content'),
        IsMajorVersion = true);
        
        insert contentVersion; 

        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
 
        ContentDocumentLink cdl = New ContentDocumentLink(
            LinkedEntityId = newCase.id, ContentDocumentId = documents[0].Id, shareType = 'I');
            insert cdl ;
                
       
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        FetchNotesFromCaseController obj = new FetchNotesFromCaseController(controller);
        
        obj.fetchNotes(); 
        Test.stopTest(); 
    }
  
    public static testmethod void getTestedInnerClass(){
        Test.startTest();
        Case callObj; 
        ContentNote NoteLst;
        Id NoteId;
        FetchNotesFromCaseController.CaseToNotesWrapper obj = new FetchNotesFromCaseController.CaseToNotesWrapper(callObj,NoteLst,NoteId);
        Test.stopTest();
    }
}