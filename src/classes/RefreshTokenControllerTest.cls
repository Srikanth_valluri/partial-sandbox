/***********************************************************************
* Description - Test class developed for RefreshTokenController
*
* Version            Date            Author        Description
* 1.0                26/11/17        Snehil		   Initial Draft
**********************************************************************/
@isTest
private class RefreshTokenControllerTest
{
	@isTest static void noException() {

		list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;

        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( accountobj[0].Id, dealSR.Id, 1 );
        insert lstBookings;

        List<Booking_Unit__c> bookingUnitsObj = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        insert bookingUnitsObj;

        //String recordTypeDevName = ;
		List<Calling_List__c> listCallingList = TestDataFactory_CRM.createCallingList( 'Collections_Calling_List', 1 , bookingUnitsObj[0] );
     	insert listCallingList;

		ApexPages.StandardController controller = new ApexPages.StandardController( listCallingList[0] );
		RefreshTokenController refreshTokenControllerObj = new RefreshTokenController( controller );
		
		Test.setCurrentPageReference(new PageReference('Page.myPage')); 
		System.currentPageReference().getParameters().put('id', listCallingList[0].Id);
 
		//ApexPages.currentPage().getParameters().set('id', String.valueOf( listCallingList[0].Id ));

		

		Test.startTest();
		
		Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockCallingList());
		
		refreshTokenControllerObj.fetchData();

		Test.stopTest();
	}


	@isTest static void withException() {

		list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;

        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( accountobj[0].Id, dealSR.Id, 1 );
        insert lstBookings;

        List<Booking_Unit__c> bookingUnitsObj = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        insert bookingUnitsObj;

        //String recordTypeDevName = ;
		List<Calling_List__c> listCallingList = TestDataFactory_CRM.createCallingList( 'Collections_Calling_List', 1 , bookingUnitsObj[0] );
     	insert listCallingList;

		ApexPages.StandardController controller = new ApexPages.StandardController( listCallingList[0] );
		RefreshTokenController refreshTokenControllerObj = new RefreshTokenController( controller );
		
		Test.setCurrentPageReference(new PageReference('Page.myPage')); 
		System.currentPageReference().getParameters().put('id', listCallingList[0].Id);
		
		
		refreshTokenControllerObj.fetchData();
	}
}