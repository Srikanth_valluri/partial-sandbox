public without sharing class FmcDocumentsController {

    @RemoteAction
    public static Map<String, List<SR_Attachments__c>> fetchDocumentsOfType(String documentType) {
        return fetchDocumentsOfType(new Set<String> { documentType });
    }

    public static Map<String, List<SR_Attachments__c>> fetchDocumentsOfType(String documentType, Integer docLimit) {
        return fetchDocumentsOfType(new Set<String> { documentType }, docLimit);
    }

    public static Map<String, List<SR_Attachments__c>> fetchDocumentsOfType(Set<String> documentTypes) {
        return fetchDocumentsOfType(documentTypes, 10000);
    }

    public static Map<String, List<SR_Attachments__c>> fetchDocumentsOfType(
        Set<String> documentTypes, Integer docLimit
    ) {
        Set<Id> unitIds = new Set<Id>();
        Set<Id> buildingIds = new Set<Id>();
        Set<Id> leasedBuildingIds = new Set<Id>();
        Set<Id> ownedBuildingIds = new Set<Id>();
        Set<Id> propertyIds = new Set<Id>();
        Set<Id> boardMemberPropertyIds = new Set<Id>();
        for (Booking_Unit__c unit : FmcUtils.queryOwnedUnitsForAccount(
                CustomerCommunityUtils.customerAccountId,
                'Id,Inventory__c, Inventory__r.Building_Location__c,Inventory__r.Building_Location__r.Property_Name__c'
        )) {
            //System.debug('unit.Id = ' + unit.Id);
            unitIds.add(unit.Id);
            if (unit.Inventory__c != NULL) {
                buildingIds.add(unit.Inventory__r.Building_Location__c);
                ownedBuildingIds.add(unit.Inventory__r.Building_Location__c);
                if (unit.Inventory__r.Building_Location__c != NULL
                    && unit.Inventory__r.Building_Location__r.Property_Name__c != NULL
                ) {
                    propertyIds.add(unit.Inventory__r.Building_Location__r.Property_Name__c);
                }
            }
        }
        for (Booking_Unit__c unit : FmcUtils.queryLeasedUnitsForAccount(
                CustomerCommunityUtils.customerAccountId,
                'Id, Inventory__c, Inventory__r.Building_Location__c, Inventory__r.Building_Location__r.Property_Name__c'
        )) {
            //System.debug('unit.Id = ' + unit.Id);
            unitIds.add(unit.Id);
            if (unit.Inventory__c != NULL) {
                buildingIds.add(unit.Inventory__r.Building_Location__c);
                leasedBuildingIds.add(unit.Inventory__r.Building_Location__c);
                if (unit.Inventory__r.Building_Location__c != NULL
                    && unit.Inventory__r.Building_Location__r.Property_Name__c != NULL
                ) {
                    propertyIds.add(unit.Inventory__r.Building_Location__r.Property_Name__c);
                }
            }
        }

        for (Board_Member__c boardMember : [
            SELECT  Id
                    , Property__c
            FROM    Board_Member__c
            WHERE   Board_Member__c = :CustomerCommunityUtils.customerAccountId
        ]) {
            boardMemberPropertyIds.add(boardMember.Property__c);
        }

        if (docLimit == NULL) {
            docLimit = 10000;
        }

        Map<String, List<SR_Attachments__c>> locationWiseDocuments = new Map<String, List<SR_Attachments__c>> {
            'public' => fetchPublicDocumentsOfType(documentTypes, docLimit)
        };

        docLimit -= locationWiseDocuments.get('public').size();

        if (docLimit <= 0) {
            return locationWiseDocuments;
        }

        //System.debug('propertyIds = ' + propertyIds);
        //System.debug('boardMemberPropertyIds = ' + boardMemberPropertyIds);
        //System.debug('buildingIds = ' + buildingIds);
        //System.debug('unitIds = ' + unitIds);
        //System.debug('documentTypes = ' + documentTypes);

        for (SR_Attachments__c doc : [
                SELECT      Id
                            , Building__c
                            , Building__r.Name
                            , Building__r.Building_Name__c
                            , Building__r.Property_Name__c
                            , Booking_Unit__c
                            , Booking_Unit__r.Unit_Name__c
                            , Property__c
                            , Property__r.Name
                            , Name
                            , Document_Type__c
                            , Description__c
                            , LastModifiedDate
                            , Attachment_URL__c
                            , Parent_Id__c
                            , FM_Recipient__c
                            , ( SELECT      Id
                                            , Building__c
                                            , Building__r.Name
                                            , Building__r.Building_Name__c
                                            , Building__r.Property_Name__c
                                            , Booking_Unit__c
                                            , Booking_Unit__r.Unit_Name__c
                                            , Property__c
                                            , Property__r.Name
                                            , Name
                                            , Document_Type__c
                                            , Description__c
                                            , LastModifiedDate
                                            , Attachment_URL__c
                                            , Parent_Id__c
                                            , FM_Recipient__c
                                FROM        Documents__r
                                ORDER BY    LastModifiedDate DESC, Document_Type__c DESC, Building__c
                                LIMIT       1000)
                FROM        SR_Attachments__c
                WHERE       Document_Type__c IN :documentTypes
                            AND Parent_Id__c    = NULL
                            AND (Property__c IN :propertyIds
                            OR Building__c IN :buildingIds
                            OR Booking_Unit__c IN :unitIds
                            OR Property__c IN :boardMemberPropertyIds)
                            AND (FM_Account__c = :CustomerCommunityUtils.customerAccountId OR FM_Account__c = NULL)
                            AND Approval_Status__c = 'Approved'
                            AND FM_Recipient__c IN :getRecipientTypes()
                            AND ((Document_Type__c = 'Notice' AND (Valid_Till__c >= TODAY OR Valid_Till__c = NULL))
                                OR Document_Type__c != 'Notice'
                            )
                ORDER BY    LastModifiedDate DESC, Document_Type__c DESC, Building__c
                LIMIT       :docLimit
        ]) {
            //System.debug('doc.FM_Recipient__c = ' + doc.FM_Recipient__c);
            //System.debug('doc.Building__c = ' + doc.Building__c);
            //System.debug('doc.Property__c = ' + doc.Property__c);
            //System.debug('ownedBuildingIds = ' + ownedBuildingIds);
            //System.debug('leasedBuildingIds = ' + leasedBuildingIds);
            if (/*!'Notice'.equalsIgnoreCase(doc.Document_Type__c)
                ||
                ('Notice'.equalsIgnoreCase(doc.Document_Type__c)
                    && */(
                    'Everyone'.equalsIgnoreCase(doc.FM_Recipient__c)
                    || ('Owners'.equalsIgnoreCase(doc.FM_Recipient__c) && ownedBuildingIds.contains(doc.Building__c))
                    || ('Tenants'.equalsIgnoreCase(doc.FM_Recipient__c) && leasedBuildingIds.contains(doc.Building__c))
                    || ('Owners and Tenants'.equalsIgnoreCase(doc.FM_Recipient__c)
                        && (ownedBuildingIds.contains(doc.Building__c) || leasedBuildingIds.contains(doc.Building__c))
                        )
                    || ('Board Members'.equalsIgnoreCase(doc.FM_Recipient__c)
                        && boardMemberPropertyIds.contains(doc.Building__r.Property_Name__c)
                        )
                    )
                //)
            ) {
                String parentName;
                if (doc.Property__c == NULL) {
                    if (doc.Building__c == NULL) {
                        parentName = doc.Booking_Unit__r.Unit_Name__c;
                    } else {
                        parentName = String.isBlank(doc.Building__r.Building_Name__c)
                                        ? doc.Building__r.Name : doc.Building__r.Building_Name__c;
                    }
                } else {
                    parentName = doc.Property__r.Name;
                }
                List<SR_Attachments__c> lstDocument = locationWiseDocuments.get(parentName);
                if (lstDocument == NULL) {
                    lstDocument = new List<SR_Attachments__c>();
                }
                lstDocument.add(doc);
                locationWiseDocuments.put(parentName, lstDocument);
            }
        }
        return locationWiseDocuments;
    }

    public static List<SR_Attachments__c> fetchPublicDocumentsOfType(Set<String> documentTypes, Integer docLimit) {
        Boolean isOwner = FmcUtils.isOwner();
        Boolean isTenant = FmcUtils.isTenant();
        Boolean isBoardMember = FmcUtils.isBoardMember();
        List<SR_Attachments__c> lstPublicDocuments = new List<SR_Attachments__c>();
        for (SR_Attachments__c doc : [
            SELECT      Id
                        , Building__c
                        , Building__r.Name
                        , Building__r.Building_Name__c
                        , Name
                        , Document_Type__c
                        , Description__c
                        , LastModifiedDate
                        , Attachment_URL__c
                        , Parent_Id__c
                        , FM_Recipient__c
                        , ( SELECT      Id
                                        , Building__c
                                        , Building__r.Name
                                        , Building__r.Building_Name__c
                                        , Name
                                        , Document_Type__c
                                        , Description__c
                                        , LastModifiedDate
                                        , Attachment_URL__c
                                        , Parent_Id__c
                                        , FM_Recipient__c
                            FROM        Documents__r
                            ORDER BY    LastModifiedDate DESC, Document_Type__c DESC, Building__c
                            LIMIT       1000)
            FROM        SR_Attachments__c
            WHERE       Document_Type__c IN :documentTypes
                        AND Parent_Id__c    = NULL
                        AND Building__c     = NULL
                        AND Booking_Unit__c = NULL
                        AND Property__c     = NULL
                        AND Approval_Status__c = 'Approved'
                        AND FM_Recipient__c IN :getRecipientTypes()
                        AND ((Document_Type__c = 'Notice' AND (Valid_Till__c >= TODAY OR Valid_Till__c = NULL))
                            OR Document_Type__c != 'Notice'
                        )
            ORDER BY    LastModifiedDate DESC, Document_Type__c DESC, Building__c
            LIMIT       :docLimit
        ]) {
            if (/*!'Notice'.equalsIgnoreCase(doc.Document_Type__c)
                ||
                ('Notice'.equalsIgnoreCase(doc.Document_Type__c)
                    && */('Everyone'.equalsIgnoreCase(doc.FM_Recipient__c)
                    || ('Owners'.equalsIgnoreCase(doc.FM_Recipient__c) && isOwner)
                    || ('Tenants'.equalsIgnoreCase(doc.FM_Recipient__c) && isTenant)
                    || ('Owners and Tenants'.equalsIgnoreCase(doc.FM_Recipient__c) && (isOwner || isTenant))
                    || ('Board Members'.equalsIgnoreCase(doc.FM_Recipient__c) && isBoardMember)
                    )
                //)

            ) {
                lstPublicDocuments.add(doc);
            }
        }
        return lstPublicDocuments;
    }

    private static Set<String> getRecipientTypes() {
        Set<String> recipientTypes = new Set<String> {'Everyone'};
        Boolean isOwner = FmcUtils.isOwner();
        Boolean isTenant = FmcUtils.isTenant();
        if (isOwner || isTenant) {
            recipientTypes.add('Owners and Tenants');
            if (isOwner) {
                recipientTypes.add('Owners');
            }
            if (isTenant) {
                recipientTypes.add('Tenants');
            }
        }
        if (FmcUtils.isBoardMember()) {
            recipientTypes.add('Board Members');
        }

        return recipientTypes;
    }
}