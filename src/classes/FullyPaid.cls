/***************AKISHOR**23Jul2020****Created for 100 Paid unit*****************/
@RestResource(urlMapping='/FullyPaid/*')

global with sharing class FullyPaid{
@HttpPost
    global static void FullyPaid( String RegId,String PaymentDate, 
                                         String Remarks) {

       ResponseWrapper objResponseWrapper = new ResponseWrapper();
        Set<Id> recTypeIdSet=new Set<Id>();
        for(RecordType rec :[SELECT DeveloperName,Id,Name
                 FROM RecordType
                 WHERE SobjectType = 'Calling List'
                 And (Name='Handover Calling List'
                 OR Name='Early Handover Calling List'
                 )  ]){
      recTypeIdSet.add(rec.Id);
        }

        List<Booking_Unit__c> BUList= [Select Id, Booking__r.Account__c,Registration_Id__c from Booking_Unit__c 
        where Unit_Active__c='Active' AND Registration_Id__c=:RegId limit 1];
        
        if(BUList == NULL || BUList .isEmpty()){
              
                   objResponseWrapper.status = 'Unit does not exists, please pass correct details.';
                   objResponseWrapper.statusCode = '400';
                   RestContext.response.responseBody = Blob.valueOf(JSON.serialize(objResponseWrapper));
        }
       
        if(!BUList.isEmpty()){
           
            if(RegId !='') {
            String Mnth= String.valueOf(date.today().month());
            String Yr = String.valueOf(date.today().Year());   
            List<Booking_Unit__c> UnitLst = new List<Booking_Unit__c>();
            List<Booking_Unit__c> ListBU = [Select Id from Booking_Unit__c where Unit_Active__c='Active' AND Registration_Id__c =: RegId limit 1];         
            List<Calling_List__c> CList = [Select Id from Calling_List__c where RecordType.Name ='Collections Calling List' AND
            Calling_List_Type__c ='Collection Calling' AND
            Registration_Id__c =: RegId AND Collection_Month__c=: Mnth AND Collection_Year__c=: Yr limit 1];
            List<Calling_List__c> listCal = new List<Calling_List__c>();
            if(ListBU.size() > 0)
                {
                  ListBU[0].Fully_Paid__c= true;
                  ListBU[0].Full_Payment_Date__c=date.valueOf(PaymentDate);
                  ListBU[0].IntegrationRemarks__c=Remarks;
                  UnitLst.add(ListBU[0]);
                  Update UnitLst;
                }
                if(CList.size() > 0)
                {
                  CList[0].Call_Outcome__c= 'Paid';
                  CList[0].CRE_Remarks__c='Fully Paid';
                  listCal.add(CList[0]); 
                  Update listCal;
                   //Find NHO List and move in respective Q (Akoya , Non Akoya)                   
                //List<Calling_List__c> NHOList = [Select Id from Calling_List__c where RecordTypeId IN:recTypeIdSet AND Registration_Id__c =: RegId];
                }
               
              }
            }
        
            try{

                        objResponseWrapper.status = 'Details Updated Successfully';
                        objResponseWrapper.statusCode = '200';
                        system.debug(Blob.valueOf(JSON.serialize(objResponseWrapper)));
                        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(objResponseWrapper));
                        //Restcontext.response.statusCode = 200;
                    } catch( Exception ex ) {
                        system.debug( ex.getMessage() );
                        objResponseWrapper.errorMessage = ex.getMessage();
                        objResponseWrapper.statusCode = '400';
                        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(objResponseWrapper));
                        //Restcontext.response.statusCode = 400;
         
                        return;               
                  }  
  }          
        
    //for sending response
    public class ResponseWrapper {
        public String status;
        public String statusCode;
        public String errorMessage;

        public ResponseWrapper() {
            this.status = '';
            this.statusCode = '';
            this.errorMessage = '';
        }
       }
}