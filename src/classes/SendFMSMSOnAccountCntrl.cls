// Class to send sms to FM customer
public without sharing class SendFMSMSOnAccountCntrl extends SendSMSAccountController{
    
    public SendFMSMSOnAccountCntrl( ApexPages.standardController controller ){
        isFMSMS = true;
    } //End constructor
    
   
}