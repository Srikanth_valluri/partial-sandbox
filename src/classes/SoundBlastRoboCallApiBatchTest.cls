@isTest
private class SoundBlastRoboCallApiBatchTest {

    @istest
    static void TestMethod1() {

        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                    OnOffCheck__c = true);
        insert newSetting1;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account(LastName= 'Test Account',
                                      Phone = '1234567890',
                                      RecordtypeId = rtId,
                                      Phone_Country_Code__c = ' India: 0091',
                                      Mobile_Country_Code__c = 'India: 0091',
                                      Mobile_Phone_Encrypt__pc = '8149071353',
                                      Mobile_Phone_Encrypt__c = '1324567890',
                                      Phone_Encrypt__c = 'adswsfds',
                                      Mobile__c = '1234567890',
                                      Primary_Language__c = 'Arabic',
                                      Nationality__c = 'Jordanian',
                                      Nationality__pc='Jordanian'
                                      );
        insert account;

        Robocall_Campaign__c objRoboCamp = new Robocall_Campaign__c(Name = 'test');
        insert objRoboCamp;

        Id collectioncallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Collections Calling List').RecordTypeId;
        String this_Month = String.valueOf( System.Today().month() );
        String this_Year = String.valueOf( System.Today().year() );
        Calling_List__c callList = new Calling_List__c(Account__c = account.id
                                                      , RecordTypeId = collectioncallingRecordTypeId
                                                      , IsHideFromUI__c = false
                                                      ,Call_OutCome__c = 'Unreachable'
                                                      , Collection_Month__c = this_Month
                                                      , Collection_Year__c = this_Year);
        insert callList;

        Added_call__c objAddCall = new Added_call__c(Input_String_For_Add_Call__c = 'Test'
                                                    , Calling_List__c = callList.Id
                                                    , Campaign_Robo__c = objRoboCamp.Id
                                                    ,Primary_Call__c = true
                                                    , Status__c = 'Not Connected');
        insert objAddCall;

        FmHttpCalloutMock.Response processRequestsReponse = new FmHttpCalloutMock.Response(200, 'Success',  '   {  '  + 
        '     "status": "success",  '  + 
        '     "authaccount": "430403a7-acaf-478f-b284-c85b809380a4",  '  + 
        '     "order_id": "16713135",  '  + 
        '     "response": "{\n  \"dnd_count\": \"0\",\n  \"message\": \"call added successfully\",\n  \"sivr_call_ids\": [{\"call_id\":\"7e50d35d-e4f9-45f0-867d-b1d549daacc3\"}]\n}",  '  + 
        '     "headers": {  '  + 
        '       "content-type": "application/json",  '  + 
        '       "x-api-key": "NRMPGEB9aCakjQqBzvRyC1ediT4iVH768rxsDMlo",  '  + 
        '       "authorization": "430403a7-acaf-478f-b284-c85b809380a4"  '  + 
        '     }  '  + 
        '  }  ' );

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response>{
            Label.Robo_Call_URL => processRequestsReponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));

        Added_call__c objAddedCall = [SELECt Id
                                           , Calling_List__r.Account__r.Nationality__pc
                                           , Calling_List__r.Account__r.Nationality__c
                                      FROM Added_call__c
                                      WHERE Campaign_Robo__c =: objRoboCamp.Id];

        Test.StartTest();
        SoundBlastRoboCallApiBatch objBatch = new SoundBlastRoboCallApiBatch(objRoboCamp.Id);
        database.executebatch(objBatch, 100);
        Test.StopTest();

    }

    @isTest
    static void TestMethod2() {

        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                    OnOffCheck__c = true);
        insert newSetting1;

        //Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account(Name= 'Test Account',
                                      Phone = '1234567890',                                     
                                      Phone_Country_Code__c = ' India: 0091',
                                      Mobile_Country_Code__c = 'India: 0091',
                                      Mobile_Phone_Encrypt__c = '1324567890',
                                      Phone_Encrypt__c = 'adswsfds',
                                      Mobile__c = '1234567890',
                                      Nationality__c = 'Jordanian'
                                      );
        insert account;

        Robocall_Campaign__c objRoboCamp = new Robocall_Campaign__c(Name = 'test');
        insert objRoboCamp;

        String this_Month = String.valueOf( System.Today().month() );
        String this_Year = String.valueOf( System.Today().year() );

        Id collectioncallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Collections Calling List').RecordTypeId;
        Calling_List__c callList1 = new Calling_List__c(Account__c = account.id
                                                      , RecordTypeId = collectioncallingRecordTypeId
                                                      , IsHideFromUI__c = false
                                                      ,Call_OutCome__c = 'Unreachable'
                                                      , Collection_Month__c = this_Month
                                                      , Collection_Year__c = this_Year);
        Calling_List__c callList2 = new Calling_List__c(Account__c = account.id
                                                      , RecordTypeId = collectioncallingRecordTypeId
                                                      , IsHideFromUI__c = false
                                                      ,Call_OutCome__c = 'Unreachable'
                                                      , Collection_Month__c = this_Month
                                                      , Collection_Year__c = this_Year);
        Calling_List__c callList3 = new Calling_List__c(Account__c = account.id
                                                      , RecordTypeId = collectioncallingRecordTypeId
                                                      , IsHideFromUI__c = false
                                                      ,Call_OutCome__c = 'Unreachable'
                                                      , Collection_Month__c = this_Month
                                                      , Collection_Year__c = this_Year);

        List<Calling_List__c> listCL = new List<Calling_List__c>{ callList1,callList2,callList3};
        insert listCL;
        Added_call__c objAddCall1 = new Added_call__c(Input_String_For_Add_Call__c = 'Test'
                                                    , Calling_List__c = callList1.Id
                                                    , Campaign_Robo__c = objRoboCamp.Id
                                                    , Primary_Call__c = true
                                                    , Status__c = 'Not Connected');
        Added_call__c objAddCall2 = new Added_call__c(Input_String_For_Add_Call__c = 'Test'
                                                    , Calling_List__c = callList2.Id
                                                    , Campaign_Robo__c = objRoboCamp.Id
                                                    , Primary_Call__c = true
                                                    , Status__c = 'Not Connected');
        Added_call__c objAddCall3 = new Added_call__c(Input_String_For_Add_Call__c = 'Test'
                                                    , Calling_List__c = callList3.Id
                                                    , Campaign_Robo__c = objRoboCamp.Id
                                                    , Primary_Call__c = true
                                                    , Status__c = 'Not Connected');                                            
        List<Added_call__c> listCalls = new List<Added_call__c>{ objAddCall1,objAddCall2,objAddCall3};
        insert listCalls;
        FmHttpCalloutMock.Response processRequestsReponse = new FmHttpCalloutMock.Response(200, 'Success',  '   {  '  + 
        '     "status": "success",  '  + 
        '     "authaccount": "430403a7-acaf-478f-b284-c85b809380a4",  '  + 
        '     "order_id": "16713135",  '  + 
        '     "response": "{\n  \"dnd_count\": \"0\",\n  \"message\": \"call added successfully\",\n  \"sivr_call_ids\": [{\"call_id\":\"7e50d35d-e4f9-45f0-867d-b1d549daacc3\"}]\n}",  '  + 
        '     "headers": {  '  + 
        '       "content-type": "application/json",  '  + 
        '       "x-api-key": "NRMPGEB9aCakjQqBzvRyC1ediT4iVH768rxsDMlo",  '  + 
        '       "authorization": "430403a7-acaf-478f-b284-c85b809380a4"  '  + 
        '     }  '  + 
        '  }  ' );

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response>{
            Label.Robo_Call_URL => processRequestsReponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));

        Test.StartTest();
        SoundBlastRoboCallApiBatch objBatch = new SoundBlastRoboCallApiBatch(objRoboCamp.Id);
        database.executebatch(objBatch, 100);
        Test.StopTest();

    }
}