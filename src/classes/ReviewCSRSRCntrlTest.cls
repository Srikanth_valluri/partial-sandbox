@isTest
public with sharing class ReviewCSRSRCntrlTest {
    public static final String strBookingUnitActiveStatus = 'Agreement executed by DAMAC';
    static testMethod void testScenario1() {
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        List<SR_Booking_Unit__c> srBookingUnitList = new List<SR_Booking_Unit__c>();
        //Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Address_Line_1__c = 'XYZ';
        acc.Country__c = 'Jordan';
        acc.City__c = 'Barcelona';
        acc.State__c = 'lmn';
        //acc.RecordtypeId = rtId;
        //acc.Country__pc  = 'Jordan';
        acc.Zip_Postal_Code__c = '111';
        insert acc;
        
        Case objCase = new Case();
        objCase.Account = acc;
        objCase.Status = 'Submitted';
        objCase.Approval_Status__c = 'Approved';
        //objCase.Is_New_Payment_Terms_Applied__c = True;
        //objCase.NewPaymentTermJSON__c = '[{"strTermID":"145486","strStatus":null,"strRegID":null,"strPaymentDate":null,"strPaidAmount":"98822.84","strLineID":"808896","strInvoiceAmount":"98860.8","strDueAmount":"37.96","percentValue":"24","paymentDate":"06-DEC-2015","name":"Date","mileStoneEventArabic":"فورا","mileStoneEvent":"Immediate","isReceiptPresent":true,"installment":"DP","description":"DEPOSIT","blnNewTerm":null},{"strTermID":"145487","strStatus":null,"strRegID":null,"strPaymentDate":null,"strPaidAmount":null,"strLineID":"808897","strInvoiceAmount":null,"strDueAmount":null,"percentValue":"0","paymentDate":"","name":"Date","mileStoneEventArabic":null,"mileStoneEvent":null,"isReceiptPresent":true,"installment":"I001","description":"1ST INSTALMENT","blnNewTerm":null},{"strTermID":"145488","strStatus":null,"strRegID":null,"strPaymentDate":null,"strPaidAmount":"0","strLineID":"808898","strInvoiceAmount":"49430.4","strDueAmount":"49430.4","percentValue":"10","paymentDate":"03-JUN-2016","name":"Date","mileStoneEventArabic":"عند أو قبل","mileStoneEvent":"On or Before","isReceiptPresent":false,"installment":"I002","description":"2ND INSTALMENT","blnNewTerm":null},{"strTermID":"145489","strStatus":null,"strRegID":null,"strPaymentDate":null,"strPaidAmount":"0","strLineID":"808899","strInvoiceAmount":"49430.4","strDueAmount":"49430.4","percentValue":"10","paymentDate":"02-AUG-2016","name":"Date","mileStoneEventArabic":"عند أو قبل","mileStoneEvent":"On or Before","isReceiptPresent":false,"installment":"I003","description":"3RD INSTALMENT","blnNewTerm":null},{"strTermID":"145490","strStatus":null,"strRegID":null,"strPaymentDate":null,"strPaidAmount":"0","strLineID":"808900","strInvoiceAmount":"49430.4","strDueAmount":"49430.4","percentValue":"10","paymentDate":"30-NOV-2016","name":"Date","mileStoneEventArabic":"عند أو قبل","mileStoneEvent":"On or Before","isReceiptPresent":false,"installment":"I004","description":"4TH INSTALMENT","blnNewTerm":null},{"strTermID":"145491","strStatus":null,"strRegID":null,"strPaymentDate":null,"strPaidAmount":null,"strLineID":"808901","strInvoiceAmount":null,"strDueAmount":null,"percentValue":"50","paymentDate":"30/11/2017","name":"Date","mileStoneEventArabic":"عند أو قبل","mileStoneEvent":"On or Before","isReceiptPresent":false,"installment":"I005","description":"5TH INSTALMENT","blnNewTerm":null}]';
        objCase.Type = 'Question';  
        objCase.Account_Email__c = 'test@gmail.com';
        insert objCase;
        
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;
        
        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        objInventory.Current_Saleable_Area__c = 10;
        objInventory.Current_AC_Area__c = 10;
        objInventory.Current_Upfront_Price__c = 10;
        objInventory.Current_Standard_Price__c = 10;
        insert objInventory;
        
        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;
        
        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(acc.Id,objDealSR.Id,1);
        insert bookingList;
        
        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList)
        {
            objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
            objBookingUnit.Inventory__c = objInventory.Id;
            objBookingUnit.Registration_ID__c = '74712';
            objBookingUnit.Inventory_Sale_Classification__c = 'Easy';
            objBookingUnit.Requested_Price__c = 10;
            objBookingUnit.Area__c = 100;
        }
        insert bookingUnitList;
        
        srBookingUnitList = TestDataFactory_CRM.createSRBookingUnis(objCase.Id , bookingUnitList);
        insert srBookingUnitList;
        Test.startTest();
        PageReference pageRef = Page.ReviewCSRSR;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', objCase.Id);
        ApexPages.StandardController sc = new ApexPages.standardController(objCase);
        ReviewCSRSRCntrl reviewCsr = new ReviewCSRSRCntrl (sc);
        reviewCsr.getBuInvDetails();
        reviewCsr.saveData();
        Test.stopTest();
        
    }
}