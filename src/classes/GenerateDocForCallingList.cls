/********************************************************************************************************************************
* Description - To generate SOA for Calling list                                                                                *
*-------------------------------------------------------------------------------------------------------------------------------*
* Version   Date            Author              Description                                                                     *
*-------------------------------------------------------------------------------------------------------------------------------*
* 1.0       27/11/2017                          Initial Draft                                                                   *  
* 1.1       14/02/2019      Arjun Khatri        1.Made REST callout                                                             *
* 1.2       18/02/2019      Arjun Khatri        1.Added flag for Rest and Soap Callout                                          *
* 1.3       07/10/2020      Aishwarya Todkar    1. Added validations while generating SOA                                       *
*********************************************************************************************************************************/

public class GenerateDocForCallingList {
    Calling_List__c objCallingList;

    public GenerateDocForCallingList(ApexPages.StandardController controller) {
        objCallingList = (Calling_List__c)controller.getrecord();

        objCallingList = [ SELECT
                                Id
                                , Booking_Unit__c
                                , Account__c
                                , Registration_ID__c
                                , Booking_Unit__r.Registration_Status__c
                                , Booking_Unit__r.Unit_Active__c
                                , Booking_Unit__r.Registration_ID__c 
                            FROM 
                                Calling_List__c 
                            WHERE 
                                Id =: objCallingList.id 
                            LIMIT 1 ];
    }
    
/********************************************************************************************************************************
 * Method Name : generatestataccount
 * Description : Generate Statement of Account.
 * Return Type : PageReference
 * Parameter(s): None
********************************************************************************************************************************/    
    public PageReference generateStatAccount(){    
        
        if( String.isBlank( objCallingList.Registration_ID__c ) 
        && String.isBlank( objCallingList.Booking_Unit__r.Registration_ID__c ) ) {
            ApexPages.addmessage(
                new ApexPages.message(
                    ApexPages.severity.Error
                    , 'Please update registration Id to generate Statement Of Account. '
                )
            );
            return null;
        } 
        else if( String.isNotBlank( objCallingList.Booking_Unit__r.Registration_Status__c )
        && objCallingList.Booking_Unit__r.Registration_Status__c.toLowercase().contains( 'rejected' ) ) {
            ApexPages.addmessage(
                new ApexPages.message(
                    ApexPages.severity.Error
                    , 'Statement of Account cannot be generated for a rejected unit.'
                )
            );
        }
        else if( String.isNotBlank( objCallingList.Booking_Unit__r.Unit_Active__c )
        && objCallingList.Booking_Unit__r.Unit_Active__c.equalsIgnoreCase( 'Inactive' ) ) {
            ApexPages.addmessage(
                new ApexPages.message(
                    ApexPages.severity.Error
                    , 'Statement of Account cannot be generated for an inactive unit.'
                )
            );
        }
        else {
            System.debug('Generat_SOA_Using_Rest == ' + Label.Generat_SOA_Using_Rest);
            
            //Call method to generate SOA using REST //Label.Generat_SOA_Using_Rest
            if( Label.Generat_SOA_Using_Rest.equalsIgnoreCase( 'Yes' ) ) {
                Pagereference pg = generateSOAUsingRest();
                return pg;
            }
            
            //Call method to generate SOA using SOAP
            else if( String.isNotBlank( Label.Generat_SOA_Using_Rest ) 
            && Label.Generat_SOA_Using_Rest.equalsIgnoreCase( 'No' ) ) {
                Pagereference pg = generateSOAUsingSoap();
                return pg; 
            }
        }
        return null;
    }
    
/********************************************************************************************************************************
 * Method Name : generateSOAUsingRest
 * Description : Generate Statement of Account Using REST.
 * Return Type : PageReference
 * Parameter(s): None
*********************************************************************************************************************************/ 
    public Pagereference generateSOAUsingRest() {

        /****Added code on 14/02/2019 to make rest callout***/
        String accId, BuId, clId, regid;
        accId = objCallingList.Account__c != null ? String.valueOf(objCallingList.Account__c) : '';
        BuId = objCallingList.Booking_Unit__c != null ? String.valueOf(objCallingList.Booking_Unit__c) : '';
        clId = String.valueOf(objCallingList.Id);
        regId = String.isNotBlank( objCallingList.Registration_ID__c ) ? objCallingList.Registration_ID__c
                : String.isNotBlank( objCallingList.Booking_Unit__r.Registration_ID__c )
                ? objCallingList.Booking_Unit__r.Registration_ID__c : '';

        //Call method to generate DP SOA
        String responseobj = GenerateDPSoaRest.generateDPSoa( regId
                                                            , accId
                                                            , BuId
                                                            , clId);
        System.debug('Rest responseobj *******'+responseobj );
        if( String.isNotBlank( responseobj ) ) {
            if( responseobj.contains('Error :') ) {
                System.debug('responseobj Error : '+responseobj );
                ApexPages.addmessage(
                    new ApexPages.message(ApexPages.severity.ERROR,responseobj)
                );
                return ApexPages.CurrentPage();
            }
            else{
                Pagereference pg = new PageReference(responseobj);
                return pg;
            }
        }
        else {
            ApexPages.addmessage(
                new ApexPages.message(ApexPages.severity.ERROR,'Sorry!Could not get any response!')
            );
            return null;
        }
    }
    
/********************************************************************************************************************************
 * Method Name : generateSOAUsingSoap
 * Description : Generate Statement of Account Using SOAP.
 * Return Type : PageReference
 * Parameter(s): None
*********************************************************************************************************************************/
    public Pagereference generateSOAUsingSoap() {
        GenerateSOAController.soaResponse responseobj 
            = GenerateSOAController.getSOADocument(objCallingList.Registration_ID__c);
        System.debug('SOAP responseobj *******'+responseobj );
       // System.debug('responseobj ***google***'+responseobj.url.contains('google'));
        System.debug('responseobj *url******'+String.isBlank(responseobj.url));
        if( responseobj != null ) {
            if( String.isBlank(responseobj.url ) || responseobj.url == 'null' ) {
                apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.ERROR
                ,'Statement was not generated, please try again.');
            apexpages.addmessage(msg);
                return ApexPages.CurrentPage();
            }
            else{
                Pagereference pg = new PageReference(responseobj.url);
                //pg.setRedirect(true);
                return pg;
            }
        } else {
            ApexPages.addmessage(
                new ApexPages.message(ApexPages.severity.ERROR
                    , 'Statement was not generated, please try again'
                )
            );
            return null;
        }
    }
    
    public PageReference generatestatofpenalty(){
        if( String.isBlank( objCallingList.Registration_ID__c ) ) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please enter registration id for Generating POA'));
            return null;
        } else {  
            PenaltyWaiverService.SopResponseObject sopresponseObject = new PenaltyWaiverService.SopResponseObject();
            sopresponseObject  = PenaltyWaiverService.getSOPDocument(objCallingList.Registration_ID__c);
            if( sopresponseObject != null ) {
                if(  String.isBlank(sopresponseObject.url) || sopresponseObject.url.contains('Error') ){
                     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Statement was not generated, please try again'));
                     return null;
                }
                else{
                    Pagereference pg = new PageReference(sopresponseObject.url);
                    pg.setRedirect(true);
                    return pg;
                }
            } else {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Statement was not generated, please try again'));
                return null;
            }
        }
        return null;
    }
}