/**************************************************************************************************
* Name               : UpdateUnitAssignmentsRankingScheduler                                      *
* Description        : Scheduler class for UpdateUnitAssignmentsRankingBatch batch class.         *
* Created Date       : 06/06/2020                                                                 *
* Created By         : QBurst                                                                     *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR            DATE            COMMENTS                                          *
* 1.0         QBurst      06/06/2020      Initial Draft                                          *
**************************************************************************************************/
global class UpdateUnitAssignmentsRankingScheduler implements Schedulable {
  
    global void execute(SchedulableContext SC) { 
      UpdateUnitAssignmentsRankingBatch updateUserAssignment = new UpdateUnitAssignmentsRankingBatch();
      Database.executeBatch(updateUserAssignment,200);
    }
}