/*
  Written by : CH V Gopinadh
  Test class : NationalityPickListInfo_Test
  Decription : This will send response as picklist values of Preffered Language on Inquiry
*/
@RestResource(urlMapping='/languagepicklist/*')
global class LanguagePickListInfo{
    @HttpGet 
    global static void getNewFundraiser(){
        RestResponse res = RestContext.response;
        RestRequest req = RestContext.request;
        List<String> pickListValuesList = new List<String>();
        Schema.DescribeFieldResult fieldResult = Inquiry__c.Preferred_Language__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }     
        System.debug('pickListValuesListpickListValuesList'+pickListValuesList);
        res.responseBody = blob.valueof(JSON.serialize(pickListValuesList));
        res.statusCode = 200;
                
    }
}