public class BookingUnitEmailUnitSoaLangPdfController {

    public Id recordId {get; set;}
    public String responseUrl {get; set;}
    public String accessToken {get; set;}
    public String pdfFile {get; set;}
    public Blob pdfBlob {get; set;}
    public String emailId {get; set;}

    public BookingUnitEmailUnitSoaLangPdfController(ApexPages.StandardController controller) {
        recordId = controller.getId();
        System.debug('Booking Unit Id in consturctor: '+recordId);
    }

    public PageReference sendUnitSoaLangPdfEmail() {
        FmIpmsRestCoffeeServices.updateCustomSetting = false;

        Booking_Unit__c bookingUnit = [SELECT Id, Booking__r.Account__c, Booking__r.Account__r.Name , Booking__r.Account__r.RecordType.Name, Booking__r.Account__r.Email__c, Booking__r.Account__r.Email__pc, Unit_Name__c , Registration_ID__c
                                               FROM Booking_Unit__c
                                               WHERE Id =: recordId];
        System.debug('Account is: '+bookingUnit.Booking__r.Account__c);

        String registrationId = bookingUnit.Registration_ID__c;
        String accountId = bookingUnit.Booking__r.Account__c;
        System.debug('registrationId is: '+registrationId);
        System.debug('Account ID is: '+accountId);

        //Fetch Email field value as per the Type of Account i.e. Business/Person Account
        if(bookingUnit.Booking__r.Account__r.RecordType.Name == 'Business Account') {
                    System.debug('Its Business Account');
                     emailId = bookingUnit.Booking__r.Account__r.Email__c;
        }
        else if(bookingUnit.Booking__r.Account__r.RecordType.Name == 'Person Account') {
            System.debug('Its Person Account');
             emailId = bookingUnit.Booking__r.Account__r.Email__pc;
        }

        System.debug('Email Id of Account: '+emailId);

        responseUrl = FmIpmsRestCoffeeServices.getUnitSoaInLanguage(registrationId);
        System.debug('responseUrl is: '+responseUrl);

        if(String.isBlank(responseUrl) || responseUrl == null || responseUrl == 'Null') {
            System.debug('In ApexMessage Method');
            ApexPages.Message msg = new ApexPages.Message(ApexPages.SEVERITY.WARNING, 'There was an error processing your request. Please try again...');
            ApexPages.addMessage(msg);
            return null;
        }

        //Http callout

        accessToken = FmIpmsRestCoffeeServices.accessToken;
        System.debug('AccessToken is' + accessToken);

        HttpRequest req = new HttpRequest();
        req.setEndpoint(responseUrl);
        req.setHeader('Accept', 'application/json');
        req.setMethod('GET');
        req.setHeader('Authorization','Bearer' + accessToken);
        req.setTimeout(120000);
        HttpResponse response = new Http().send(req);
        System.debug('Status Code = ' + response.getStatusCode());
        System.debug('Pdf Body = ' + response.getBodyAsBlob());

        ApexPages.Message pageMessage;

        Map<String, String> languageCodeMap = new Map<String, String> {
            'ab' => 'Abkhazian',
            'aa' => 'Afar',
            'af' => 'Afrikaans',
            'ak' => 'Akan',
            'sq' => 'Albanian',
            'am' => 'Amharic',
            'ar' => 'Arabic',
            'an' => 'Aragonese',
            'hy' => 'Armenian',
            'as' => 'Assamese',
            'av' => 'Avaric',
            'ae' => 'Avestan',
            'ay' => 'Aymara',
            'az' => 'Azerbaijani',
            'bm' => 'Bambara',
            'ba' => 'Bashkir',
            'eu' => 'Basque',
            'be' => 'Belarusian',
            'bn' => 'Bengali',
            'bh' => 'Bihari',
            'bi' => 'Bislama',
            'bs' => 'Bosnian',
            'br' => 'Breton',
            'bg' => 'Bulgarian',
            'my' => 'Burmese',
            'ca' => 'Catalan',
            'ch' => 'Chamorro',
            'ce' => 'Chechen',
            'ny' => 'Chichewa',
            'zh' => 'Chinese',
            'cv' => 'Chuvash',
            'kw' => 'Cornish',
            'co' => 'Corsican',
            'cr' => 'Cree',
            'hr' => 'Croatian',
            'cs' => 'Czech',
            'da' => 'Danish',
            'dv' => 'Divehi',
            'nl' => 'Dutch',
            'dz' => 'Dzongkha',
            'en' => 'English',
            'eo' => 'Esperanto',
            'et' => 'Estonian',
            'ee' => 'Ewe',
            'fo' => 'Faroese',
            'fj' => 'Fijian',
            'fi' => 'Finnish',
            'fr' => 'French',
            'ff' => 'Fula',
            'gl' => 'Galician',
            'gd' => 'Gaelic',
            'gv' => 'Gaelic',
            'ka' => 'Georgian',
            'de' => 'German',
            'el' => 'Greek',
            'kl' => 'Greenlandic',
            'gn' => 'Guarani',
            'gu' => 'Gujarati',
            'ht' => 'Haitian Creole',
            'ha' => 'Hausa',
            'he' => 'Hebrew',
            'hz' => 'Herero',
            'hi' => 'Hindi',
            'ho' => 'Hiri Motu',
            'hu' => 'Hungarian',
            'is' => 'Icelandic',
            'io' => 'Ido',
            'ig' => 'Igbo',
            'id' => 'Indonesian',
            'in' => 'Indonesian',
            'ia' => 'Interlingua',
            'ie' => 'Interlingue',
            'iu' => 'Inuktitut',
            'ik' => 'Inupiak',
            'ga' => 'Irish',
            'it' => 'Italian',
            'ja' => 'Japanese',
            'jv' => 'Javanese',
            'kl' => 'Kalaallisut',
            'kn' => 'Kannada',
            'kr' => 'Kanuri',
            'ks' => 'Kashmiri',
            'kk' => 'Kazakh',
            'km' => 'Khmer',
            'ki' => 'Kikuyu',
            'rw' => 'Kinyarwanda',
            'rn' => 'Kirundi',
            'ky' => 'Kyrgyz',
            'kv' => 'Komi',
            'kg' => 'Kongo',
            'ko' => 'Korean',
            'ku' => 'Kurdish',
            'kj' => 'Kwanyama',
            'lo' => 'Lao',
            'la' => 'Latin',
            'lv' => 'Latvian',
            'li' => 'Limburgish',
            'ln' => 'Lingala',
            'lt' => 'Lithuanian',
            'lu' => 'Luga-Katanga',
            'lg' => 'Luganda,Ganda',
            'lb' => 'Luxembourgish',
            'gv' => 'Manx',
            'mk' => 'Macedonian',
            'mg' => 'Malagasy',
            'ms' => 'Malay',
            'ml' => 'Malayalam',
            'mt' => 'Maltese',
            'mi' => 'Maori',
            'mr' => 'Marathi',
            'mh' => 'Marshallese',
            'mo' => 'Moldavian',
            'mn' => 'Mongolian',
            'na' => 'Nauru',
            'nv' => 'Navajo',
            'ng' => 'Ndonga',
            'nd' => 'Northern Ndebele',
            'ne' => 'Nepali',
            'no' => 'Norwegian',
            'nb' => 'Norwegian bokmål',
            'nn' => 'Norwegian nynorsk',
            'ii' => 'Nuosu',
            'oc' => 'Occitan',
            'oj' => 'Ojibwe',
            'cu' => 'Old Church Slavonic',
            'or' => 'Oriya',
            'om' => 'Oromo (Afaan Oromo)',
            'os' => 'Ossetian',
            'pi' => 'Pāli',
            'ps' => 'Pashto',
            'fa' => 'Persian (Farsi)',
            'pl' => 'Polish',
            'pt' => 'Portuguese',
            'pa' => 'Punjabi',
            'qu' => 'Quechua',
            'rm' => 'Romansh',
            'ro' => 'Romanian',
            'ru' => 'Russian',
            'se' => 'Sami',
            'sm' => 'Samoan',
            'sg' => 'Sango',
            'sa' => 'Sanskrit',
            'sr' => 'Serbian',
            'sh' => 'Serbo-Croatian',
            'st' => 'Sesotho',
            'tn' => 'Setswana',
            'sn' => 'Shona',
            'ii' => 'Sichuan Yi',
            'sd' => 'Sindhi',
            'si' => 'Sinhalese',
            'ss' => 'Siswati',
            'sk' => 'Slovak',
            'sl' => 'Slovenian',
            'so' => 'Somali',
            'nr' => 'Southern Ndebele',
            'es' => 'Spanish',
            'su' => 'Sundanese',
            'sw' => 'Swahili (Kiswahili)',
            'ss' => 'Swati',
            'sv' => 'Swedish',
            'tl' => 'Tagalog',
            'ty' => 'Tahitian',
            'tg' => 'Tajik',
            'ta' => 'Tamil',
            'tt' => 'Tatar',
            'te' => 'Telugu',
            'th' => 'Thai',
            'bo' => 'Tibetan',
            'ti' => 'Tigrinya',
            'to' => 'Tonga',
            'ts' => 'Tsonga',
            'tr' => 'Turkish',
            'tk' => 'Turkmen',
            'tw' => 'Twi',
            'ug' => 'Uyghur',
            'uk' => 'Ukrainian',
            'ur' => 'Urdu',
            'uz' => 'Uzbek',
            've' => 'Venda',
            'vi' => 'Vietnamese',
            'vo' => 'Volapük',
            'wa' => 'Wallon',
            'cy' => 'Welsh',
            'wo' => 'Wolof',
            'fy' => 'Western Frisian',
            'xh' => 'Xhosa',
            'yi' => 'Yiddish',
            'ji' => 'Yiddish',
            'yo' => 'Yoruba',
            'za' => 'Zhuang',
            'zu' => 'Zulu'
        };


        if(response.getStatusCode() == 200) {
            pdfBlob =  response.getBodyAsBlob();
            pdfFile = EncodingUtil.base64Encode(pdfBlob);

            //SendGrid
            //Convert name to Capitalized Case
            String str = bookingUnit.Booking__r.Account__r.Name;
            String name = str.toLowerCase();
            List<String> names = name.split(' ');
            for (Integer i = 0; i < names.size(); i++)
                names[i] = names[i].capitalize();
            name = String.join(names, ' ');
            System.debug(name);

            String emailSubject = 'Statement of Account for unit ' + bookingUnit.Unit_Name__c;
            String emailBody = 'Dear ' + name + ',';
            emailBody += '<br/><br/> As requested, please find attached the Statement of Account for unit ' + bookingUnit.Unit_Name__c;
            emailBody += '<br/><br/><br/> Regards,';
            emailBody += '<br/><br/>' + UserInfo.getName();
            emailBody += '<br/> Officer-Client Relations';
            emailBody += '<br/> DAMAC PROPERTIES Co. LLC.';
            emailBody += '<br/> P.O. Box: 2195, Dubai, United Arab Emirates';
            emailBody += '<br/> Telephone: +971 4 237 5000';
            emailBody += '<br/> Fax: +971 4 373 1373';
            emailBody += '<br/> E-mail: <a href="mailto:atyourservice@damacproperties.com"> atyourservice@damacproperties.com</a>';
            emailBody += '<br/> <a href="http://www.damacproperties.com/">http://www.damacproperties.com/</a>';

            String languageName;
            System.debug('Language Code in Email controller is: '+FmIpmsRestCoffeeServices.languageCode);
            if(String.isEmpty(FmIpmsRestCoffeeServices.languageCode) || FmIpmsRestCoffeeServices.languageCode == null || FmIpmsRestCoffeeServices.languageCode == 'Null') {
                languageName = 'English';
            }
            else if(!String.isEmpty(FmIpmsRestCoffeeServices.languageCode)) {
                languageName = languageCodeMap.get(FmIpmsRestCoffeeServices.languageCode);
            }

            Attachment attach = new Attachment();
            attach.contentType = 'application/pdf';
            attach.name = 'Unit SOA in '+languageName+'.pdf';
            attach.body = pdfBlob;

            List<Attachment> attachlst = new List<Attachment>();
            attachlst.add(attach);

            List<OrgWideEmailAddress> orgWideAddressHelloDamac = [
                SELECT  Id
                        , Address
                        , DisplayName
                FROM    OrgWideEmailAddress
                WHERE   DisplayName = :Label.DAMAC_no_replysf_damacgroup_com
            ];
            System.debug('@@@Email Address: ' +  orgWideAddressHelloDamac[0].Address);

            List<OrgWideEmailAddress> bccMailAddress = [
                SELECT Id,
                       Address,
                       DisplayName
                FROM   OrgWideEmailAddress
                WHERE  DisplayName = :Label.Sf_Copy_Bcc_Mail_Id
            ];
            System.debug('@@@Bcc mail Id: ' + bccMailAddress[0].Address);

            Boolean isSandbox = [SELECT IsSandbox FROM Organization].IsSandbox;
            System.debug('IsSandbox is: '+isSandbox);

            if ((isSandbox
                && (emailId.containsIgnoreCase('davidvarghese1989@gmail.com')
                    || emailId.containsIgnoreCase('@damacgroup.com')
                    || emailId.containsIgnoreCase('@eternussolutions.com')
                    || emailId.containsIgnoreCase('@mailinator.com')
                )
            ) || !isSandbox) {
                System.debug('isSandbox & email is True');
                SendGridEmailService.SendGridResponse sendGridResponse = SendGridEmailService.sendEmailService(
                    emailId, '', '', '', bccMailAddress[0].Address , '', emailSubject, '', orgWideAddressHelloDamac[0].Address, '', '', '',
                    'text/html', emailBody, '', attachlst
                );

                if(sendGridResponse.ResponseStatus == 'Accepted') {
                    EmailMessage mail = new EmailMessage();
                    mail.Subject = emailSubject;
                    mail.MessageDate = System.Today();
                    mail.Status = '3';
                    mail.RelatedToId = recordId;//Here it is BookingUnit id
                    mail.ToAddress = emailId;
                    mail.FromAddress = orgWideAddressHelloDamac[0].Address;
                    mail.TextBody = emailBody;
                    mail.CcAddress = '';
                    mail.BccAddress = bccMailAddress[0].Address;
                    mail.Sent_By_Sendgrid__c = true;
                    mail.SentGrid_MessageId__c = sendGridResponse.messageId;
                    mail.Account__c = accountId;
                    mail.SendGrid_Status__c = sendGridResponse.ResponseStatus;
                    insert mail;
                    pageMessage = new ApexPages.Message(
                        ApexPages.SEVERITY.CONFIRM, 'An email has been successfully sent to ' + emailId
                    );
                }
                else {
                    Error_Log__c objError = new Error_Log__c();
                    objError.Account__c = accountId;
                    objError.Error_Details__c = 'Unit SOA Email from SendGrid for registation Id ' + registrationId;
                    objError.Process_Name__c = 'Generic Email';
                    insert objError;
                    pageMessage = new ApexPages.Message(
                        ApexPages.SEVERITY.ERROR, 'There was a problem while sending an email to ' + emailId
                    );
                }

            }

            else {
                System.debug('Unauthorized email. Running in else part');
                pageMessage = new ApexPages.Message(
                    ApexPages.SEVERITY.ERROR, 'There was an error while generating Unit SOA for registration Id ' + registrationId
                );
            }
        }

        ApexPages.addMessage(pageMessage);

        FmIpmsRestCoffeeServices.updateCustomSetting = true;
        FmIpmsRestCoffeeServices.updateBearerToken();//calling updateMethod of Custom Setting after all callouts execution
        return null;

    }
}