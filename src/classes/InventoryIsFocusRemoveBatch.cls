/**************************************************************************************************
* Name         : InventoryIsFocusRemoveBatch
* Test Class  : 
* Description: Batch class to change the Inventory Is Focus Checkbox
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR      DATE
* 1.0         QBurst      27/11/2020
**************************************************************************************************/
public class InventoryIsFocusRemoveBatch implements Database.Batchable<sObject>{

    /*********************************************************************************************
    * @Description: Implementing the start method of batch interface, contains query.
    * @Params      : Database.BatchableContext
    * @Return      : Database.QueryLocator
    *********************************************************************************************/  
    public Database.QueryLocator start(Database.BatchableContext BC){

        return Database.getQueryLocator([SELECT Id, Is_Focus__c
                                                     FROM Inventory__c
                                                WHERE Is_Focus__c = TRUE
                                                AND Focus_End_Date__c  != NULL
                                                AND Focus_End_Date__c >= TODAY]); 
    }

    /*********************************************************************************************
    * @Description: Implementing the execute method of the batch interface, contains the criteria.
    * @Params      : Database.BatchableContext, List<sObject>
    * @Return: void
    *********************************************************************************************/
    public void execute(Database.BatchableContext BC, List<Inventory__c> scope){
        system.debug('scope: ' +  scope);
        List<Inventory__c> invList = new List<Inventory__c>();
        for(Inventory__c inv: scope){
            inv.Is_Focus__c = false;
            invList.add(inv);  
        }
        if(invList.size() > 0){
            update invList;    
        }
    }

    /*********************************************************************************************
    * @Description: Implementing the Finish method, to send an email after job completion.
    * @Params      : Database.BatchableContext
    * @Return: void
    *********************************************************************************************/
    public void finish(Database.BatchableContext BC){
    }
}// End of class.