@isTest
public class CancelHandoverEarlySettlementTest{
    public static final String strBookingUnitActiveStatus = 'Agreement executed by DAMAC';
    public static testmethod void testCase(){
         List<Booking__c> bookingList = new List<Booking__c>();
         List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
         List<SR_Booking_Unit__c> srBookingUnitList = new List<SR_Booking_Unit__c>();
    
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        //Id aoptRecordTypeID = getRecordTypeIdForAOPT();
        
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        insert objInventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
          objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
          objBookingUnit.Inventory__c = objInventory.Id;
          objBookingUnit.Registration_ID__c = '74712';
        }
        system.debug('>>>>>'+bookingUnitList);
        insert bookingUnitList;
        
        Case objCase = new Case();
        objCase.Status = 'Submitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Case_Type__c = 'Proof of Payment'; 
        objCase.Booking_Unit__c = bookingUnitList[0].id;
        objCase.AccountId = objAccount.Id;
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        insert objCase;
        
        srBookingUnitList = TestDataFactory_CRM.createSRBookingUnis(objCase.Id , bookingUnitList);
        system.debug('+++++++++'+srBookingUnitList);
        insert srBookingUnitList;
        
        Task taskObj = new Task();
        taskObj.Status = 'In Progress';
        taskObj.Subject = 'Update AOPT Details in IPMS';
        insert taskObj;
        
        Payment_Plan__c ObjPaymentPlanOld = new Payment_Plan__c();
        ObjPaymentPlanOld.Status__c = 'InActive';
        ObjPaymentPlanOld.Booking_Unit__c = bookingUnitList[0].id;
        insert ObjPaymentPlanOld;

        Payment_Plan__c ObjPaymentPlan = new Payment_Plan__c();
        ObjPaymentPlan.Status__c = 'Active';
        ObjPaymentPlan.Booking_Unit__c = bookingUnitList[0].id;
        ObjPaymentPlan.Parent_Payment_Plan__c = ObjPaymentPlanOld.id;
        insert ObjPaymentPlan;

        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        CancelHandoverEarlySettlement objCancel = new CancelHandoverEarlySettlement(stdController);
        objCancel.caseId = objCase.Id;
        Test.startTest();
    
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT());
        objCancel.cancelHO();
        
        Test.stopTest();        
    }
    
    public static testmethod void testFailureCase(){
         List<Booking__c> bookingList = new List<Booking__c>();
         List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
         List<SR_Booking_Unit__c> srBookingUnitList = new List<SR_Booking_Unit__c>();
    
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        //Id aoptRecordTypeID = getRecordTypeIdForAOPT();
        
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        insert objInventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
          objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
          objBookingUnit.Inventory__c = objInventory.Id;
          objBookingUnit.Registration_ID__c = '74712';
        }
        system.debug('>>>>>'+bookingUnitList);
        insert bookingUnitList;
        
        Case objCase = new Case();
        objCase.Status = 'Submitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Case_Type__c = 'Proof of Payment'; 
        objCase.Booking_Unit__c = bookingUnitList[0].id;
        objCase.AccountId = objAccount.Id;
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        insert objCase;
        
        srBookingUnitList = TestDataFactory_CRM.createSRBookingUnis(objCase.Id , bookingUnitList);
        system.debug('+++++++++'+srBookingUnitList);
        insert srBookingUnitList;
        
        Task taskObj = new Task();
        taskObj.Status = 'In Progress';
        taskObj.Subject = 'Update AOPT Details in IPMS';
        insert taskObj;
        
        Payment_Plan__c ObjPaymentPlanOld = new Payment_Plan__c();
        ObjPaymentPlanOld.Status__c = 'InActive';
        ObjPaymentPlanOld.Booking_Unit__c = bookingUnitList[0].id;
        insert ObjPaymentPlanOld;

        Payment_Plan__c ObjPaymentPlan = new Payment_Plan__c();
        ObjPaymentPlan.Status__c = 'Active';
        ObjPaymentPlan.Booking_Unit__c = bookingUnitList[0].id;
        ObjPaymentPlan.Parent_Payment_Plan__c = ObjPaymentPlanOld.id;
        insert ObjPaymentPlan;

        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        CancelHandoverEarlySettlement objCancel = new CancelHandoverEarlySettlement(stdController);
        objCancel.caseId = objCase.Id;
        Test.startTest();
    
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
        objCancel.cancelHO();
        PaymentPlanReversalMultipleService objCls = new PaymentPlanReversalMultipleService();
        PaymentPlanReversalMultipleService.createExceptionUseCase = true;
        PaymentPlanReversalMultipleService.updatePaymentPlanInIPMS(bookingUnitList[0].Registration_Id__c,
                                                                    '2-001545',objCase);
        Test.stopTest();        
    }
}