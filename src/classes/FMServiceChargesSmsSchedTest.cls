/***********************************************************************************
*   Description :  Test class developed for FMServiceChargesSmsSched.              *
*----------------------------------------------------------------------------------*
*   Revision History:                                                              *
*   Version     Author              Date            Description                    *
*   1.0         Arjun Khatri        06/02/2019      Initial Draft                  *
************************************************************************************/
@isTest
public Class FMServiceChargesSmsSchedTest {
    
    @isTest
    public static void testSchedule() {
        FMServiceChargesSmsSched objSched = new FMServiceChargesSmsSched();
        
        String sch = '0 0 23 * * ?'; 
        Test.StartTest();
        system.schedule('Test Service charges Sms', sch, objSched );
        Test.stopTest();
    }
}