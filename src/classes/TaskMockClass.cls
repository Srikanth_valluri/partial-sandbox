@isTest
global class TaskMockClass implements WebServiceMock{
	global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType){
                wwwFsiCoUkServicesEvolution0409_t32.TaskServiceV3  calloutObj = new   wwwFsiCoUkServicesEvolution0409_t32.TaskServiceV3();
               schemasDatacontractOrg200407FsiConc_t32.TaskDtoV3 authObj = new schemasDatacontractOrg200407FsiConc_t32.TaskDtoV3();
               authObj.Code = '897';
               wwwFsiCoUkServicesEvolution0409_t32.GetTaskByIdResponse_element calloutObj2 = 
               		new wwwFsiCoUkServicesEvolution0409_t32.GetTaskByIdResponse_element();
               calloutObj2.GetTaskByIdResult = authObj;
               response.put('response_x', calloutObj2);
          }
}