global class notifyCREforCalBackScheduler implements Schedulable{
    global void execute(SchedulableContext sc) {
        //invoke the batch class
        if(!Test.isRunningTest()) Database.executeBatch(new notifyCREforCalBack(),20);
    }
}