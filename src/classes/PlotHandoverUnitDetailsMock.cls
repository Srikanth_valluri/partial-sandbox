@isTest

/*
* Revision History: 
* Version   Author          Date        Description.
* 1.1       Swapnil Gholap  12/02/2018  Initial Draft
*/

global class PlotHandoverUnitDetailsMock implements WebServiceMock {
    public integer intResponseNumber = 0 ;
    
    public PlotHandoverUnitDetailsMock() {

    }

    public PlotHandoverUnitDetailsMock( integer intNum ) {
        intResponseNumber = intNum ;
    }
     
    global void doInvoke(
       Object stub,
       Object request,
       Map<String, Object> response,
       String endpoint,
       String soapAction,
       String requestName,
       String responseNS,
       String responseName,
       String responseType) {
                            
       /*RefundsRule.TokenRefundsTransfersResponse_element respElement = new RefundsRule.TokenRefundsTransfersResponse_element();
       respElement.return_x = '{"allowed":"Yes","message":null,"recommendingAuthorityOne":"Manager","recommendingAuthorityTwo":"Manager","recommendingAuthorityThree":"Manager","recommendingAuthorityFour":"Manager","approvingAuthorityOne":"Manager","approvingAuthorityTwo":"Manager","approvingAuthorityThree":"Manager","percToBeRefundedTransferred":null,"deductionFeePsf":null,"deductionFeeFlat":null,"deductionFeePercentage":null}';
       response.put('response_x', respElement);
       */
       
       // unitDetailsController.getUnitDetailValuesResponse_element responseNew = new unitDetailsController.getUnitDetailValuesResponse_element();
       // responseNew.return_x = '["{\"ATTRIBUTE1\":\"74712\",\"ATTRIBUTE2\":\"\",\"ATTRIBUTE3\":\"LE\",\"ATTRIBUTE4\":\"N\",\"ATTRIBUTE5\":\"0\",\"ATTRIBUTE6\":\"\",\"ATTRIBUTE7\":\"\",\"ATTRIBUTE8\":\"253760\",\"ATTRIBUTE9\":\"253760\",\"ATTRIBUTE10\":\"0\",\"ATTRIBUTE11\":\"2525\",\"ATTRIBUTE12\":\"N\",\"ATTRIBUTE13\":\"Y\",\"ATTRIBUTE14\":\"N\",\"ATTRIBUTE15\":\"\",\"ATTRIBUTE16\":\"N\",\"ATTRIBUTE17\":\"N\",\"ATTRIBUTE18\":\"123745600\",\"ATTRIBUTE19\":\"0\",\"ATTRIBUTE20\":\"6.81\",\"ATTRIBUTE21\":\"1\",\"ATTRIBUTE22\":\"Y\",\"ATTRIBUTE23\":\"1523\",\"ATTRIBUTE24\":\"0\",\"ATTRIBUTE25\":\"1523\",\"ATTRIBUTE26\":\"PLATINUM\",\"ATTRIBUTE27\":\"N\",\"ATTRIBUTE28\":\"N\",\"ATTRIBUTE29\":\"OFF-PLAN\",\"ATTRIBUTE30\":\"N\",\"ATTRIBUTE31\":\"N\",\"ATTRIBUTE32\":\"\",\"ATTRIBUTE33\":\"440\",\"ATTRIBUTE34\":\"N\",\"ATTRIBUTE35\":\"51794\",\"ATTRIBUTE36\":\"\",\"ATTRIBUTE37\":\"\",\"ATTRIBUTE38\":\"\",\"ATTRIBUTE39\":\"27-MAR-2016\",\"ATTRIBUTE40\":\"AYKCB/40/4011\",\"ATTRIBUTE41\":\"253760\",\"ATTRIBUTE42\":\"0\",\"ATTRIBUTE43\":\"2525\",\"ATTRIBUTE44\":\"0\",\"ATTRIBUTE45\":\"N\",\"ATTRIBUTE46\":\"N\",\"ATTRIBUTE47\":\"N\",\"ATTRIBUTE48\":\"N\",\"ATTRIBUTE49\":\"Y\",\"ATTRIBUTE50\":\"N\",\"ATTRIBUTE51\":\"Agreement executed by DAMAC\",\"ATTRIBUTE52\":\"00\",\"ATTRIBUTE53\":\"6.81\",\"ATTRIBUTE54\":\"17-AUG-2020\",\"ATTRIBUTE55\":\"31-OCT-2021\",\"ATTRIBUTE56\":\"0\",\"ATTRIBUTE57\":\"0\",\"ATTRIBUTE58\":\"51794\",\"ATTRIBUTE59\":\"20\",\"ATTRIBUTE60\":\".2\",\"ATTRIBUTE61\":\"0\",\"ATTRIBUTE62\":\"0\",\"ATTRIBUTE63\":\"\",\"ATTRIBUTE64\":\"\",\"ATTRIBUTE65\":\"N\",\"ATTRIBUTE66\":\"N\",\"ATTRIBUTE67\":\"N\",\"ATTRIBUTE68\":\"0\",\"ATTRIBUTE69\":\"N\",\"ATTRIBUTE70\":\"\",\"ATTRIBUTE71\":\"\",\"ATTRIBUTE72\":\"1\",\"ATTRIBUTE73\":\"PARK_BAY\",\"ATTRIBUTE74\":\"\",\"ATTRIBUTE75\":\"0\",\"ATTRIBUTE76\":\"0\",\"ATTRIBUTE77\":\"\",\"ATTRIBUTE78\":\"1015040\",\"ATTRIBUTE79\":\"\",\"ATTRIBUTE80\":\"\",\"ATTRIBUTE81\":\"\",\"ATTRIBUTE82\":\"SA \u2013 Execution Pending awaiting Deposit\",\"ATTRIBUTE83\":\"Damac Canal One Property Development LLC\",\"ATTRIBUTE84\":\"N\",\"ATTRIBUTE85\":\"1600\",\"ATTRIBUTE86\":\"0\",\"ATTRIBUTE87\":\"20\",\"ATTRIBUTE88\":\"253760\",\"ATTRIBUTE89\":\"1444\",\"ATTRIBUTE90\":\"AYKCB\",\"ATTRIBUTE91\":\"AYKON CITY TOWER - B\",\"ATTRIBUTE92\":\"\",\"ATTRIBUTE93\":\"\",\"ATTRIBUTE94\":\"1074105\",\"ATTRIBUTE95\":\"N...\",\"ATTRIBUTE96\":\"793\",\"ATTRIBUTE97\":\"Normal\",\"ATTRIBUTE98\":\"27-MAR-2016\",\"ATTRIBUTE99\":\"APARTMENT\",\"ATTRIBUTE100\":\"\",\"ATTRIBUTE101\":\"Y\",\"ATTRIBUTE102\":\"1268800\",\"ATTRIBUTE103\":\"Y\",\"ATTRIBUTE104\":\"Y\",\"ATTRIBUTE105\":\"0\",\"ATTRIBUTE106\":\"27-MAR-2016\",\"ATTRIBUTE107\":\"N\",\"ATTRIBUTE108\":\"AYKON CITY\",\"ATTRIBUTE109\":\"DUBAI\",\"ATTRIBUTE110\":\"One Bedroom\",\"ATTRIBUTE111\":\"HOTEL APARTMENTS\",\"ATTRIBUTE112\":\"\",\"ATTRIBUTE113\":\"\",\"ATTRIBUTE114\":\"\",\"ATTRIBUTE115\":\"\",\"ATTRIBUTE116\":\"\",\"ATTRIBUTE117\":\"\",\"ATTRIBUTE118\":\"\",\"ATTRIBUTE119\":\"\",\"ATTRIBUTE120\":\"\"}"]';
       // response.put('response_x', responseNew);  
        
        //  Litigation Flag  and strPaidPercent < 20 and strPDCCoverage = null
        if( intResponseNumber == 1 ) {
            unitDetailsController.getUnitDetailValuesResponse_element responseNew = new unitDetailsController.getUnitDetailValuesResponse_element();
            responseNew.return_x = '["{\"ATTRIBUTE1\":\"74712\",\"ATTRIBUTE2\":\"\",\"ATTRIBUTE3\":\"LE\",\"ATTRIBUTE4\":\"N\",\"ATTRIBUTE5\":\"0\",\"ATTRIBUTE6\":\"\",\"ATTRIBUTE7\":\"\",\"ATTRIBUTE8\":\"253760\",\"ATTRIBUTE9\":\"253760\",\"ATTRIBUTE10\":\"0\",\"ATTRIBUTE11\":\"2525\",\"ATTRIBUTE12\":\"N\",\"ATTRIBUTE13\":\"Y\",\"ATTRIBUTE14\":\"N\",\"ATTRIBUTE15\":\"\",\"ATTRIBUTE16\":\"N\",\"ATTRIBUTE17\":\"N\",\"ATTRIBUTE18\":\"123745600\",\"ATTRIBUTE19\":\"null\",\"ATTRIBUTE20\":\"6.81\",\"ATTRIBUTE21\":\"1\",\"ATTRIBUTE22\":\"Y\",\"ATTRIBUTE23\":\"1523\",\"ATTRIBUTE24\":\"0\",\"ATTRIBUTE25\":\"1523\",\"ATTRIBUTE26\":\"PLATINUM\",\"ATTRIBUTE27\":\"N\",\"ATTRIBUTE28\":\"N\",\"ATTRIBUTE29\":\"OFF-PLAN\",\"ATTRIBUTE30\":\"N\",\"ATTRIBUTE31\":\"N\",\"ATTRIBUTE32\":\"\",\"ATTRIBUTE33\":\"440\",\"ATTRIBUTE34\":\"N\",\"ATTRIBUTE35\":\"51794\",\"ATTRIBUTE36\":\"\",\"ATTRIBUTE37\":\"\",\"ATTRIBUTE38\":\"\",\"ATTRIBUTE39\":\"27-MAR-2016\",\"ATTRIBUTE40\":\"AYKCB/40/4011\",\"ATTRIBUTE41\":\"253760\",\"ATTRIBUTE42\":\"0\",\"ATTRIBUTE43\":\"2525\",\"ATTRIBUTE44\":\"0\",\"ATTRIBUTE45\":\"Y\",\"ATTRIBUTE46\":\"Y\",\"ATTRIBUTE47\":\"Y\",\"ATTRIBUTE48\":\"Y\",\"ATTRIBUTE49\":\"Y\",\"ATTRIBUTE50\":\"N\",\"ATTRIBUTE51\":\"Agreement executed by DAMAC\",\"ATTRIBUTE52\":\"00\",\"ATTRIBUTE53\":\"6.81\",\"ATTRIBUTE54\":\"17-AUG-2020\",\"ATTRIBUTE55\":\"31-OCT-2021\",\"ATTRIBUTE56\":\"0\",\"ATTRIBUTE57\":\"0\",\"ATTRIBUTE58\":\"51794\",\"ATTRIBUTE59\":\"19\",\"ATTRIBUTE60\":\".2\",\"ATTRIBUTE61\":\"0\",\"ATTRIBUTE62\":\"0\",\"ATTRIBUTE63\":\"\",\"ATTRIBUTE64\":\"\",\"ATTRIBUTE65\":\"N\",\"ATTRIBUTE66\":\"N\",\"ATTRIBUTE67\":\"N\",\"ATTRIBUTE68\":\"0\",\"ATTRIBUTE69\":\"N\",\"ATTRIBUTE70\":\"\",\"ATTRIBUTE71\":\"\",\"ATTRIBUTE72\":\"1\",\"ATTRIBUTE73\":\"PARK_BAY\",\"ATTRIBUTE74\":\"\",\"ATTRIBUTE75\":\"0\",\"ATTRIBUTE76\":\"0\",\"ATTRIBUTE77\":\"\",\"ATTRIBUTE78\":\"1015040\",\"ATTRIBUTE79\":\"\",\"ATTRIBUTE80\":\"\",\"ATTRIBUTE81\":\"\",\"ATTRIBUTE82\":\"SA \u2013 Execution Pending awaiting Deposit\",\"ATTRIBUTE83\":\"Damac Canal One Property Development LLC\",\"ATTRIBUTE84\":\"N\",\"ATTRIBUTE85\":\"1600\",\"ATTRIBUTE86\":\"0\",\"ATTRIBUTE87\":\"20\",\"ATTRIBUTE88\":\"253760\",\"ATTRIBUTE89\":\"1444\",\"ATTRIBUTE90\":\"AYKCB\",\"ATTRIBUTE91\":\"AYKON CITY TOWER - B\",\"ATTRIBUTE92\":\"\",\"ATTRIBUTE93\":\"\",\"ATTRIBUTE94\":\"1074105\",\"ATTRIBUTE95\":\"N...\",\"ATTRIBUTE96\":\"793\",\"ATTRIBUTE97\":\"Normal\",\"ATTRIBUTE98\":\"27-MAR-2016\",\"ATTRIBUTE99\":\"APARTMENT\",\"ATTRIBUTE100\":\"\",\"ATTRIBUTE101\":\"Y\",\"ATTRIBUTE102\":\"1268800\",\"ATTRIBUTE103\":\"Y\",\"ATTRIBUTE104\":\"Y\",\"ATTRIBUTE105\":\"0\",\"ATTRIBUTE106\":\"27-MAR-2016\",\"ATTRIBUTE107\":\"N\",\"ATTRIBUTE108\":\"AYKON CITY\",\"ATTRIBUTE109\":\"DUBAI\",\"ATTRIBUTE110\":\"One Bedroom\",\"ATTRIBUTE111\":\"HOTEL APARTMENTS\",\"ATTRIBUTE112\":\"\",\"ATTRIBUTE113\":\"\",\"ATTRIBUTE114\":\"\",\"ATTRIBUTE115\":\"\",\"ATTRIBUTE116\":\"\",\"ATTRIBUTE117\":\"\",\"ATTRIBUTE118\":\"\",\"ATTRIBUTE119\":\"\",\"ATTRIBUTE120\":\"\"}"]';
            response.put('response_x', responseNew);  
        } 
               
        // strPaidPercent = null
        if( intResponseNumber == 2 ) {
            unitDetailsController.getUnitDetailValuesResponse_element responseNew = new unitDetailsController.getUnitDetailValuesResponse_element();
            responseNew.return_x = '["{\"ATTRIBUTE1\":\"74712\",\"ATTRIBUTE2\":\"\",\"ATTRIBUTE3\":\"LE\",\"ATTRIBUTE4\":\"N\",\"ATTRIBUTE5\":\"0\",\"ATTRIBUTE6\":\"\",\"ATTRIBUTE7\":\"\",\"ATTRIBUTE8\":\"253760\",\"ATTRIBUTE9\":\"253760\",\"ATTRIBUTE10\":\"0\",\"ATTRIBUTE11\":\"2525\",\"ATTRIBUTE12\":\"N\",\"ATTRIBUTE13\":\"Y\",\"ATTRIBUTE14\":\"N\",\"ATTRIBUTE15\":\"\",\"ATTRIBUTE16\":\"N\",\"ATTRIBUTE17\":\"N\",\"ATTRIBUTE18\":\"123745600\",\"ATTRIBUTE19\":\"0\",\"ATTRIBUTE20\":\"6.81\",\"ATTRIBUTE21\":\"1\",\"ATTRIBUTE22\":\"Y\",\"ATTRIBUTE23\":\"1523\",\"ATTRIBUTE24\":\"0\",\"ATTRIBUTE25\":\"1523\",\"ATTRIBUTE26\":\"PLATINUM\",\"ATTRIBUTE27\":\"N\",\"ATTRIBUTE28\":\"N\",\"ATTRIBUTE29\":\"OFF-PLAN\",\"ATTRIBUTE30\":\"N\",\"ATTRIBUTE31\":\"N\",\"ATTRIBUTE32\":\"\",\"ATTRIBUTE33\":\"440\",\"ATTRIBUTE34\":\"N\",\"ATTRIBUTE35\":\"51794\",\"ATTRIBUTE36\":\"\",\"ATTRIBUTE37\":\"\",\"ATTRIBUTE38\":\"\",\"ATTRIBUTE39\":\"27-MAR-2016\",\"ATTRIBUTE40\":\"AYKCB/40/4011\",\"ATTRIBUTE41\":\"253760\",\"ATTRIBUTE42\":\"0\",\"ATTRIBUTE43\":\"2525\",\"ATTRIBUTE44\":\"0\",\"ATTRIBUTE45\":\"Y\",\"ATTRIBUTE46\":\"Y\",\"ATTRIBUTE47\":\"Y\",\"ATTRIBUTE48\":\"Y\",\"ATTRIBUTE49\":\"Y\",\"ATTRIBUTE50\":\"N\",\"ATTRIBUTE51\":\"Agreement executed by DAMAC\",\"ATTRIBUTE52\":\"00\",\"ATTRIBUTE53\":\"6.81\",\"ATTRIBUTE54\":\"17-AUG-2020\",\"ATTRIBUTE55\":\"31-OCT-2021\",\"ATTRIBUTE56\":\"0\",\"ATTRIBUTE57\":\"0\",\"ATTRIBUTE58\":\"51794\",\"ATTRIBUTE59\":\"\",\"ATTRIBUTE60\":\".2\",\"ATTRIBUTE61\":\"0\",\"ATTRIBUTE62\":\"0\",\"ATTRIBUTE63\":\"\",\"ATTRIBUTE64\":\"\",\"ATTRIBUTE65\":\"N\",\"ATTRIBUTE66\":\"N\",\"ATTRIBUTE67\":\"N\",\"ATTRIBUTE68\":\"0\",\"ATTRIBUTE69\":\"N\",\"ATTRIBUTE70\":\"\",\"ATTRIBUTE71\":\"\",\"ATTRIBUTE72\":\"1\",\"ATTRIBUTE73\":\"PARK_BAY\",\"ATTRIBUTE74\":\"\",\"ATTRIBUTE75\":\"0\",\"ATTRIBUTE76\":\"0\",\"ATTRIBUTE77\":\"\",\"ATTRIBUTE78\":\"1015040\",\"ATTRIBUTE79\":\"\",\"ATTRIBUTE80\":\"\",\"ATTRIBUTE81\":\"\",\"ATTRIBUTE82\":\"SA \u2013 Execution Pending awaiting Deposit\",\"ATTRIBUTE83\":\"Damac Canal One Property Development LLC\",\"ATTRIBUTE84\":\"N\",\"ATTRIBUTE85\":\"1600\",\"ATTRIBUTE86\":\"0\",\"ATTRIBUTE87\":\"20\",\"ATTRIBUTE88\":\"253760\",\"ATTRIBUTE89\":\"1444\",\"ATTRIBUTE90\":\"AYKCB\",\"ATTRIBUTE91\":\"AYKON CITY TOWER - B\",\"ATTRIBUTE92\":\"\",\"ATTRIBUTE93\":\"\",\"ATTRIBUTE94\":\"1074105\",\"ATTRIBUTE95\":\"N...\",\"ATTRIBUTE96\":\"793\",\"ATTRIBUTE97\":\"Normal\",\"ATTRIBUTE98\":\"27-MAR-2016\",\"ATTRIBUTE99\":\"APARTMENT\",\"ATTRIBUTE100\":\"\",\"ATTRIBUTE101\":\"Y\",\"ATTRIBUTE102\":\"1268800\",\"ATTRIBUTE103\":\"Y\",\"ATTRIBUTE104\":\"Y\",\"ATTRIBUTE105\":\"0\",\"ATTRIBUTE106\":\"27-MAR-2016\",\"ATTRIBUTE107\":\"N\",\"ATTRIBUTE108\":\"AYKON CITY\",\"ATTRIBUTE109\":\"DUBAI\",\"ATTRIBUTE110\":\"One Bedroom\",\"ATTRIBUTE111\":\"HOTEL APARTMENTS\",\"ATTRIBUTE112\":\"\",\"ATTRIBUTE113\":\"\",\"ATTRIBUTE114\":\"\",\"ATTRIBUTE115\":\"\",\"ATTRIBUTE116\":\"\",\"ATTRIBUTE117\":\"\",\"ATTRIBUTE118\":\"\",\"ATTRIBUTE119\":\"\",\"ATTRIBUTE120\":\"\"}"]';
            response.put('response_x', responseNew);  
        } 
        
        //strPDCCoverage < 50
        if( intResponseNumber == 3 ) {            
            unitDetailsController.getUnitDetailValuesResponse_element responseNew = new unitDetailsController.getUnitDetailValuesResponse_element();
            responseNew.return_x = '["{\"ATTRIBUTE1\":\"74712\",\"ATTRIBUTE2\":\"\",\"ATTRIBUTE3\":\"LE\",\"ATTRIBUTE4\":\"N\",\"ATTRIBUTE5\":\"0\",\"ATTRIBUTE6\":\"\",\"ATTRIBUTE7\":\"\",\"ATTRIBUTE8\":\"253760\",\"ATTRIBUTE9\":\"253760\",\"ATTRIBUTE10\":\"0\",\"ATTRIBUTE11\":\"2525\",\"ATTRIBUTE12\":\"N\",\"ATTRIBUTE13\":\"Y\",\"ATTRIBUTE14\":\"N\",\"ATTRIBUTE15\":\"\",\"ATTRIBUTE16\":\"N\",\"ATTRIBUTE17\":\"N\",\"ATTRIBUTE18\":\"123745600\",\"ATTRIBUTE19\":\"49\",\"ATTRIBUTE20\":\"6.81\",\"ATTRIBUTE21\":\"1\",\"ATTRIBUTE22\":\"Y\",\"ATTRIBUTE23\":\"1523\",\"ATTRIBUTE24\":\"0\",\"ATTRIBUTE25\":\"1523\",\"ATTRIBUTE26\":\"PLATINUM\",\"ATTRIBUTE27\":\"N\",\"ATTRIBUTE28\":\"N\",\"ATTRIBUTE29\":\"OFF-PLAN\",\"ATTRIBUTE30\":\"N\",\"ATTRIBUTE31\":\"N\",\"ATTRIBUTE32\":\"\",\"ATTRIBUTE33\":\"440\",\"ATTRIBUTE34\":\"N\",\"ATTRIBUTE35\":\"51794\",\"ATTRIBUTE36\":\"\",\"ATTRIBUTE37\":\"\",\"ATTRIBUTE38\":\"\",\"ATTRIBUTE39\":\"27-MAR-2016\",\"ATTRIBUTE40\":\"AYKCB/40/4011\",\"ATTRIBUTE41\":\"253760\",\"ATTRIBUTE42\":\"0\",\"ATTRIBUTE43\":\"2525\",\"ATTRIBUTE44\":\"0\",\"ATTRIBUTE45\":\"Y\",\"ATTRIBUTE46\":\"Y\",\"ATTRIBUTE47\":\"Y\",\"ATTRIBUTE48\":\"Y\",\"ATTRIBUTE49\":\"Y\",\"ATTRIBUTE50\":\"N\",\"ATTRIBUTE51\":\"Agreement executed by DAMAC\",\"ATTRIBUTE52\":\"00\",\"ATTRIBUTE53\":\"6.81\",\"ATTRIBUTE54\":\"17-AUG-2020\",\"ATTRIBUTE55\":\"31-OCT-2021\",\"ATTRIBUTE56\":\"0\",\"ATTRIBUTE57\":\"0\",\"ATTRIBUTE58\":\"51794\",\"ATTRIBUTE59\":\"19\",\"ATTRIBUTE60\":\".2\",\"ATTRIBUTE61\":\"0\",\"ATTRIBUTE62\":\"0\",\"ATTRIBUTE63\":\"\",\"ATTRIBUTE64\":\"\",\"ATTRIBUTE65\":\"N\",\"ATTRIBUTE66\":\"N\",\"ATTRIBUTE67\":\"N\",\"ATTRIBUTE68\":\"0\",\"ATTRIBUTE69\":\"N\",\"ATTRIBUTE70\":\"\",\"ATTRIBUTE71\":\"\",\"ATTRIBUTE72\":\"1\",\"ATTRIBUTE73\":\"PARK_BAY\",\"ATTRIBUTE74\":\"\",\"ATTRIBUTE75\":\"0\",\"ATTRIBUTE76\":\"0\",\"ATTRIBUTE77\":\"\",\"ATTRIBUTE78\":\"1015040\",\"ATTRIBUTE79\":\"\",\"ATTRIBUTE80\":\"\",\"ATTRIBUTE81\":\"\",\"ATTRIBUTE82\":\"SA \u2013 Execution Pending awaiting Deposit\",\"ATTRIBUTE83\":\"Damac Canal One Property Development LLC\",\"ATTRIBUTE84\":\"N\",\"ATTRIBUTE85\":\"1600\",\"ATTRIBUTE86\":\"0\",\"ATTRIBUTE87\":\"20\",\"ATTRIBUTE88\":\"253760\",\"ATTRIBUTE89\":\"1444\",\"ATTRIBUTE90\":\"AYKCB\",\"ATTRIBUTE91\":\"AYKON CITY TOWER - B\",\"ATTRIBUTE92\":\"\",\"ATTRIBUTE93\":\"\",\"ATTRIBUTE94\":\"1074105\",\"ATTRIBUTE95\":\"N...\",\"ATTRIBUTE96\":\"793\",\"ATTRIBUTE97\":\"Normal\",\"ATTRIBUTE98\":\"27-MAR-2016\",\"ATTRIBUTE99\":\"APARTMENT\",\"ATTRIBUTE100\":\"\",\"ATTRIBUTE101\":\"Y\",\"ATTRIBUTE102\":\"1268800\",\"ATTRIBUTE103\":\"Y\",\"ATTRIBUTE104\":\"Y\",\"ATTRIBUTE105\":\"0\",\"ATTRIBUTE106\":\"27-MAR-2016\",\"ATTRIBUTE107\":\"N\",\"ATTRIBUTE108\":\"AYKON CITY\",\"ATTRIBUTE109\":\"DUBAI\",\"ATTRIBUTE110\":\"One Bedroom\",\"ATTRIBUTE111\":\"HOTEL APARTMENTS\",\"ATTRIBUTE112\":\"\",\"ATTRIBUTE113\":\"\",\"ATTRIBUTE114\":\"\",\"ATTRIBUTE115\":\"\",\"ATTRIBUTE116\":\"\",\"ATTRIBUTE117\":\"\",\"ATTRIBUTE118\":\"\",\"ATTRIBUTE119\":\"\",\"ATTRIBUTE120\":\"\"}"]';
            response.put('response_x', responseNew);  
        } 
        
        if( intResponseNumber == 4 ) { // community charges Exception
            actionComPlotHandOver.GetPlotHandoverDetailsResponse_element  objReq = new actionComPlotHandOver.GetPlotHandoverDetailsResponse_element ();            
            objReq.return_x = 'getFacilityDueResponse_element:[apex_schema_type_info=(http://action.com, true, true), field_order_type_info=(return_x), return_x={"data":[{"PROC_STATUS":"S","PROC_MESSAGE":null,"ATTRIBUTE2":null,"ATTRIBUTE1":"N","PARAM_ID":"64102","Message_ID":"2-64102"}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}, return_x_type_info=(return, http://action.com, null, 0, 1, true)]';
            response.put('response_x', objReq);  
        } 
        
        if( intResponseNumber == 5 ) { //community charges Success
            actionComPlotHandOver.getFacilityDueResponse_element objReq = new actionComPlotHandOver.getFacilityDueResponse_element();            
            objReq.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":null,"ATTRIBUTE2":"5840.66","ATTRIBUTE1":"Y","PARAM_ID":"10745","Message_ID":"2-10745"}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';
            response.put('response_x', objReq);  
        }
        
        if( intResponseNumber == 6 ) { //community charges Error 1 - "PROC_STATUS":"E"
            actionComPlotHandOver.getFacilityDueResponse_element objReq = new actionComPlotHandOver.getFacilityDueResponse_element();            
            objReq.return_x = '{"data":[{"PROC_STATUS":"E","PROC_MESSAGE":null,"ATTRIBUTE2":"5840.66","ATTRIBUTE1":"Y","PARAM_ID":"10745","Message_ID":"2-10745"}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';
            response.put('response_x', objReq);  
        }
        
        if( intResponseNumber == 7 ) { //community charges Error 2 - "ATTRIBUTE1":"N"
            actionComPlotHandOver.getFacilityDueResponse_element objReq = new actionComPlotHandOver.getFacilityDueResponse_element();            
            objReq.return_x = '{"data":[{"PROC_STATUS":"E","PROC_MESSAGE":null,"ATTRIBUTE2":"5840.66","ATTRIBUTE1":"N","PARAM_ID":"10745","Message_ID":"2-10745"}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';
            response.put('response_x', objReq);  
        }
        
         if( intResponseNumber == 8 ) { //community charges Error 2 - "ATTRIBUTE2":""
            actionComPlotHandOver.getFacilityDueResponse_element objReq = new actionComPlotHandOver.getFacilityDueResponse_element();            
            objReq.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":null,"ATTRIBUTE2":"","ATTRIBUTE1":"Y","PARAM_ID":"10745","Message_ID":"2-10745"}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';
            response.put('response_x', objReq);  
        }
        
        if( intResponseNumber == 9 ) { //community charges Error 3 - "status":"E"
            actionComPlotHandOver.getFacilityDueResponse_element objReq = new actionComPlotHandOver.getFacilityDueResponse_element();            
            objReq.return_x = '{"data":[],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';
            response.put('response_x', objReq);  
        }
        
        if( intResponseNumber == 10 ) { //community charges Error 3 - data.Size() = 0
            actionComPlotHandOver.getFacilityDueResponse_element objReq = new actionComPlotHandOver.getFacilityDueResponse_element();            
            objReq.return_x = '{"data":[],"message":"Process Completed Returning 1 Response Message(s)...","status":"E"}';
            response.put('response_x', objReq);  
        }
        
        if( intResponseNumber == 11 ) { //actionComPlotHandOver  class - GetPlotHandoverDetails()
            actionComPlotHandOver.GetPlotHandoverDetailsResponse_element objReq = new actionComPlotHandOver.GetPlotHandoverDetailsResponse_element();            
            objReq.return_x = '{"data":[],"message":"Process Completed Returning 1 Response Message(s)...","status":"E"}';
            response.put('response_x', objReq);  
        }
        
        if( intResponseNumber == 12 ) { //actionComPlotHandOver  class - UpdatePlotHandoverDetails()
           actionComPlotHandOver.UpdatePlotHandoverDetailsResponse_element objReq = new actionComPlotHandOver.UpdatePlotHandoverDetailsResponse_element();            
            objReq.return_x = '{"data":[],"message":"Process Completed Returning 1 Response Message(s)...","status":"E"}';
            response.put('response_x', objReq);  
        }
        
        if( intResponseNumber == 13 ) { //actionComPlotHandOver  class - commencementCondition()
            actionComPlotHandOver.commencementConditionResponse_element objReq = new actionComPlotHandOver.commencementConditionResponse_element ();            
            objReq.return_x = '{"data":[],"message":"Process Completed Returning 1 Response Message(s)...","status":"E"}';
            response.put('response_x', objReq);  
        }
        
        if( intResponseNumber == 14 ) { //actionComPlotHandOver  class - invoiceAmountAreavariation()
            actionComPlotHandOver.invoiceAmountAreavariationResponse_element  objReq = new actionComPlotHandOver.invoiceAmountAreavariationResponse_element();            
            objReq.return_x = '{"data":[],"message":"Process Completed Returning 1 Response Message(s)...","status":"E"}';
            response.put('response_x', objReq);  
        }
       
   }
   
   
}