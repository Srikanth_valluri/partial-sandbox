/************************************************************************************************
 * @Name              : DAMAC_MSTeamsIntegration
 * @Test Class Name   : DAMAC_MSTeamsIntegration_Test
 * @Description       : Class for accessing Team Meetings/Calendar Events from Microsoft Graph API 
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         26/03/2020       Created
***********************************************************************************************/
global class DAMAC_MSTeamsIntegration{
    public static API_Integration_Value__c apiValue = [SELECT Id, Refresh_Token__c, Client_Id__c, Client_Secret__c,
                                                            Endpoint_URL__c, Redirect_URI__c, Scope__c, Event_Creation_URL__c,
                                                            Tenant_Id__c, Username__c, Password__c, Meeting_EndPoint_URL__c
                                                        FROM API_Integration_Value__c WHERE name = 'Graph API' LIMIT 1];

    /************************************************************************************************
    * @Description : Redirects to Token fetch page on Button Click
    * @Params      : None
    * @Return      : PageReference - returns the Token fetching External Page
    ************************************************************************************************/
    public pageReference getToken(){
        String url = apiValue.Endpoint_URL__c + apiValue.Tenant_Id__c + '/oauth2/v2.0/authorize?'
                     + 'client_id=' + apiValue.Client_Id__c 
                     + '&response_type=code'
                     + '&redirect_uri=' + apiValue.Redirect_URI__c 
                     + '&response_mode=query'
                     + '&scope=' + apiValue.Scope__c
                     + '&state=12345';
        Pagereference pg = new Pagereference(url);
        return pg;
    }

    /************************************************************************************************
    * @Description : Fetches AccessToken using RefreshToken
    * @Params      : None
    * @Return      : String token - Returns Access Token from Microsoft Graph API
    ************************************************************************************************/
    public static String fetchAccessToken(){
        String accesToken = '';
        Http http = new Http();
        String refresh_token = apiValue.Refresh_Token__c;
        HttpRequest tokenReq = new HttpRequest();
        String endPoint = apiValue.Endpoint_URL__c + apiValue.Tenant_Id__c + '/oauth2/v2.0/token';
        String body = 'client_id=' + EncodingUtil.urlEncode(apiValue.Client_Id__c, 'UTF-8')
                    + '&client_secret=' + EncodingUtil.urlEncode(apiValue.Client_Secret__c, 'UTF-8')
                    + '&scope=' + apiValue.Scope__c
                    + '&refresh_token=' + EncodingUtil.urlEncode(apiValue.Refresh_Token__c, 'UTF-8')
                  //  + '&redirect_uri=' + EncodingUtil.urlEncode(apiValue.Redirect_URI__c, 'UTF-8')
                   // + '&username=' + EncodingUtil.urlEncode(apiValue.Refresh_Token__c, 'UTF-8')
                   // + '&password=' + EncodingUtil.urlEncode(apiValue.Refresh_Token__c, 'UTF-8')
                   // + '&grant_type=client_credentials';
                    + '&grant_type=refresh_token';
        tokenReq.setEndpoint(endPoint);
        system.debug('body: ' + body);
        tokenReq.setBody(body);
        tokenReq.setMethod('POST');
        tokenReq.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        HttpResponse tokenRes = new HttpResponse();
        try{
            String responseBody = '';
            if(!Test.isRunningTest()){
                tokenRes = http.send(tokenReq);
                responseBody = tokenRes.getBody();
            } else{
                responseBody = '{"token_type": "Bearer","scope": "Calendars.Read","expires_in": 3599,"ext_expires_in": 3599,'
                                + '"access_token": " accesstoken","refresh_token": "refreshtoken"}';
            }
            system.debug('fetch token response body: ' + responseBody);
            MSTeam_Token_JSON token = MSTeam_Token_JSON.parse(responseBody);
            system.debug('access_token: ' + token.access_token);
            return token.access_token;
        } catch(exception e){
            System.debug('Exception: ' + e);
            return '';
        }
    }

    /************************************************************************************************
    * @Description : Creates OnlineMeeting (Teams Meeting) using Graph API
    * @Params      : 1. String token - AccessToken to access the Graph API Endpoints
                     2. String startTime - Start time of Teams Meeting
                     3. string EndTime - end time of Teams Meeting
    * @Return      : DAMAC_TeamsMeeting_JSON - returns the JSON record of response for Create Meeting
    *                                                from Graph API
    ************************************************************************************************/
    public static DAMAC_TeamsMeeting_JSON createMeeting(String token, String subject, String startTime, string EndTime){
        if(token != ''){
            Http http = new Http();
            HttpRequest meetingReq = new HttpRequest();
            meetingReq.setHeader('Authorization', 'Bearer ' + token);
           // String meetingSubject = 'DAMAC: Meeting with Property Advisor';
            // Create Online Meeting
            string body = '{"startDateTime":"' + startTime + '",'
                      + '"endDateTime":"' + endTime + '",'
                      + '"subject":"' + subject + '"}';
            meetingReq.setEndpoint(apiValue.Meeting_EndPoint_URL__c);
            meetingReq.setMethod('POST');
            system.debug('Meeting Request Body: ' + body);
            meetingReq.setBody(body);
            meetingReq.setHeader('Accept', 'application/json');
            meetingReq.setHeader('Content-Type', 'application/json');
            HttpResponse meetingRes = new HttpResponse();
            String responseBody = '';
            try{
                if(!Test.isRunningTest()){
                    meetingRes = http.send(meetingReq);
                    responseBody = meetingRes.getBody();
                } else{
                    responseBody = '{"id":"testid","startDateTime":"2020-03-27T13:21:03Z","endDateTime":"2020-03-27T13:51:03Z","joinUrl":"joinUrl"}';
                }
                system.debug('create meeting response body: ' + responseBody);
                DAMAC_TeamsMeeting_JSON meetingDetails = DAMAC_TeamsMeeting_JSON.parse(responseBody);
                return meetingDetails;
            } catch(exception e){
                System.debug('Exception: ' + e);
            }
        }
        return null;
    }

    /************************************************************************************************
    * @Description : Invokes createTeamsMeeting() method and create SF Event
    * @Params      : List<Inquiry__c> inqList - List of Inquiries for which Meeting needs to be created
    * @Return      : None
    ************************************************************************************************/
    public static void sendInvitation(List<Inquiry__c> inqList){
        List<Event> eventsToInsert = new List<Event>();
        List<Task> tasksToInsert = new List<Task>();
        String emailList = '';
        Map<Id, String> inqTLEmailMap = new Map<Id, String>();
        for(Inquiry__c inq: inqList){
            if(inq.TSA_TL_of_Owner__c != null && inq.TSA_TL_of_Owner__c != ''){
                inqTLEmailMap.put(inq.TSA_TL_of_Owner__c, '');
            }
        }
        for(User tl: [SELECT Id, Email FROM User WHERE Id IN: inqTLEmailMap.keyset()]){
            inqTLEmailMap.put(tl.Id, tl.Email);
        }
        for(Inquiry__c inq: inqList){
            DateTime startTime , endTime;
            if(inq.Online_Meeting_Start_Date_Time__c != null){
                startTime = inq.Online_Meeting_Start_Date_Time__c;
            } else{
                startTime = inq.Meeting_Due_Date__c;
            }
            if(startTime != null && startTime > DateTime.Now()){
                
                if(inq.Online_Meeting_End_Date_Time__c != null){
                    endTime = inq.Online_Meeting_End_Date_Time__c;
                } else{
                    endTime = startTime.addMinutes(30);
                }
                emailList += inq.email__c;
                if(inq.Owner.Email != null && !emailList.contains(inq.Owner.Email)){
                    emailList += ',' + inq.Owner.Email;
                }
                if(!emailList.contains(UserInfo.getUserEmail())){
                    emailList += ',' + UserInfo.getUserEmail();
                }
                if(inqTLEmailMap.containsKey(inq.TSA_TL_of_Owner__c) && inqTLEmailMap.get(inq.TSA_TL_of_Owner__c) != ''){
                    emailList += ',' + inqTLEmailMap.get(inq.TSA_TL_of_Owner__c);
                }
                String subject = 'DAMAC: Meeting with Property Advisor';
                String messageBody = 'Dear ' + inq.First_Name__c + ',<br/><br/>'
                          + 'Thank you for your interest in DAMAC Properties.'
                          + 'We have scheduled a meeting with our Relationship Manager on ' 
                          + startTime.formatGMT('dd/MM/YYYY HH:mm') + ' (GMT)'
                          + ', to help you find your dream home.';
                /*Event evnt = new Event();
                evnt = createTeamsMeeting(startTime, endTime, emailList, subject, messageBody, inq);
                if(evnt != null){
                    eventsToInsert.add(evnt);
                }
                */
                Task tsk = new Task();
                tsk = createTeamsMeeting(startTime, endTime, emailList, subject, messageBody, inq);
                if(tsk != null){
                    tasksToInsert.add(tsk);
                }
            } else {
                System.debug('Start Time is missing or already expired');
            }
        }
        /*
        system.debug('eventsToInsert: ' + eventsToInsert);
        if(eventsToInsert.size() > 0){
            try{
                insert eventsToInsert;
            } catch(exception e){
                system.debug('Error while Inserting SF Calendar Event: Exception: ' + e);
            }
        }
        */
        system.debug('tasksToInsert: ' + tasksToInsert);
        if(tasksToInsert.size() > 0){
            try{
                insert tasksToInsert;
            } catch(exception e){
                system.debug('Error while Inserting SF Task: Exception: ' + e);
            }
        }
    }

    /************************************************************************************************
    * @Description : Invokes createMeeting() and creates Calendar Event using Graph API
    * @Params      : 1. DateTime startTime - startTime of Calendar Event
                     2. DateTime endTime - endTime of Calendar Event
                     3. String emailList - List of Calendar Event Attendess
                     4. String subject - subject of Calendar Event
                     5. String messageBody - Message content for Calendar Event Invite
                     6. Inquiry__c inq - Related Inquiry record
    * @Return      : 
    ************************************************************************************************/
    public static Task createTeamsMeeting(DateTime startTime, DateTime endTime, String emailList, String subject, 
                                String messageBody, Inquiry__c inq){
        String token = fetchAccessToken();
        String msg = '';
        String stTime = startTime.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
        String enTime = endTime.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
        DAMAC_TeamsMeeting_JSON meetingDetails = createMeeting(token, subject +' [' + inq.Name + ']', stTime, enTime);
        system.debug('meetingDetails: ' + meetingDetails);
        if(meetingDetails != null && meetingDetails.joinURL != null && meetingDetails.joinURL != ''){
            String name = '';
            if(inq.First_Name__c != null && inq.First_Name__c != ''){
                name = inq.First_Name__c;
            }
            if(inq.Last_Name__c != null && inq.Last_Name__c != ''){
                if(name != ''){
                    name += ' ';
                }
                name += inq.Last_Name__c;
            }  
            String attendees = '';
            for(String emailAdr: emailList.split(',')){
                if(attendees != ''){
                    attendees += ' , ';
                }
                attendees += '{"emailAddress": {"address":"' + emailAdr.trim() + '","name": ""},"type": "required"}';
            }
            String body = '{"subject": "' + subject + '",'
                          + '"body": { '
                          + '"contentType": "HTML",'
                          + '"content": "' + messageBody
                          + '<br/><br/> Meeting Link: ' + meetingDetails.joinURL + '<br/><br/>"'
                          + '  },'
                          + '"onlineMeetingUrl": "' + meetingDetails.joinURL + '",'
                          + '"start": {"dateTime": "' + stTime + '","timeZone": "Greenwich Standard Time"},'
                          + '"end": {"dateTime": "' + enTime + '","timeZone": "Greenwich Standard Time"},'
                          + '"attendees": [' + attendees + ']}';  
            Http http = new Http();
            HttpRequest eventReq = new HttpRequest();
            eventReq.setHeader('Authorization', 'Bearer ' + token);
            eventReq.setMethod('POST');
            String endPoint = apiValue.Event_Creation_URL__c;
            eventReq.setEndpoint(endPoint);
            system.debug('endPoint: ' + endPoint);
            system.debug('Meeting Request Body: ' + body);
            eventReq.setBody(body);
            eventReq.setHeader('Accept', 'application/json');
            eventReq.setHeader('Content-Type', 'application/json');
            HttpResponse eventRes = new HttpResponse();
            DAMAC_OutlookEvents_JSON eventDetails = new DAMAC_OutlookEvents_JSON();
            try{
                String responseBody = '';
                if(!Test.isRunningTest()){
                    eventRes = http.send(eventReq);
                    responseBody = eventRes.getBody();
                }else{
                    responseBody = '{"id": "testid","subject": "DAMAC: Meeting with Property Advisor","bodyPreview": "testbody"}';    
                }
                system.debug('create meeting response body: ' + responseBody);
                if(eventRes.getStatusCode() == 201 || Test.isRunningTest()){
                    String jsonBody = responseBody.replace('"end":', '"end_x":');
                    jsonBody = jsonBody.replace('"dateTime":', '"dateTime_x":');
                    jsonBody = jsonBody.replace('"name":', '"name_x":');
                    jsonBody = jsonBody.replace('"time":', '"time_x":');
                    eventDetails = DAMAC_OutlookEvents_JSON.parse(jsonBody);
                    system.debug('eventDetails: ' + eventDetails);
                    msg = 'Event Created Successfully';
                } else{
                    msg = 'Event Creation Failed';
                }
            } catch(exception e){
                System.debug('Exception: ' + e);
            }
            /*
            Event teamsEvent = new Event();
            teamsEvent.Subject = subject; 
            teamsEvent.Type = 'Meeting Outside Office';
            teamsEvent.StartDateTime = startTime;
            teamsEvent.Activity_Type_3__c = 'Meeting Outside Office';
            teamsEvent.EndDateTime = endTime;
            teamsEvent.Description = 'Teams Meeting with Property Advisor\n\n' 
                                 + '\n\nMeeting Link: ' + meetingDetails.joinURL
                                 + '\n\nMeeting Id:' + meetingDetails.Id 
                                 + '\n\nMeeting Email Message:' + messageBody
                                 + '\n\nCalendar Event Id:' + eventDetails.Id; 
            teamsEvent.OwnerId = UserInfo.getUserId();
            teamsEvent.WhatId = inq.Id;
            return teamsEvent;
            */
            Task meetingScheduledTask = new Task();
            meetingScheduledTask.Subject = 'Online Meeting:' + subject;
            meetingScheduledTask.Activity_Type_3__c = 'Online Meeting';
            meetingScheduledTask.ActivityDate = Date.ValueOf (inq.Online_Meeting_Start_Date_Time__c);
            meetingScheduledTask.Call_Start_Time__c = inq.Online_Meeting_Start_Date_Time__c;
            meetingScheduledTask.Call_End_Time__c = inq.Online_Meeting_End_Date_Time__c;
            meetingScheduledTask.WhatID = inq.Id;
            String description = '';
            if(inq.Notes__c != null && inq.Notes__c != ''){
                description = inq.Notes__c + '\n\n';
            }
            description += subject + '\n\nMeeting Link: ' + meetingDetails.joinURL
                                   + '\n\nMeeting Id:' + meetingDetails.Id 
                                   + '\n\nAttendees:' + emailList
                                   + '\n\nMeeting Email Message:' + messageBody
                                   + '\n\nCalendar Event Id:' + eventDetails.Id; 
            meetingScheduledTask.Description = description;
            return meetingScheduledTask;
        }
        return null;
    }

    /************************************************************************************************
    * @Description : Deletes the Calendar Event from Calendar using Graph API
    * @Params      : String eventId - Microsoft Event Id of the Calendar Event to be deleted 
    * @Return      : None
    ************************************************************************************************/
    public static void deleteEvent(String eventId){
        String token = fetchAccessToken();
        Http http = new Http();
        String msg = '';
        HttpRequest eventReq = new HttpRequest();
        eventReq.setHeader('Authorization', 'Bearer ' + token);
        String endPoint = apiValue.Event_Creation_URL__c;
        endPoint += '/' + eventId;
        eventReq.setEndpoint(endPoint);
        system.debug('endPoint: ' + endPoint);
        eventReq.setMethod('DELETE');
        eventReq.setHeader('Accept', 'application/json');
        HttpResponse eventRes = new HttpResponse();
        DAMAC_OutlookEvents_JSON eventDetails = new DAMAC_OutlookEvents_JSON();
        try{
            String responseBody = '';
            if(!Test.isRunningTest()){
                eventRes = http.send(eventReq);
                responseBody = eventRes.getBody();
            } else{
                responseBody = '';
            }
            system.debug('create meeting response body: ' + responseBody);
            if(eventRes.getStatusCode() == 204){
                msg = 'Event Deleted Successfully';
            } else if(eventRes.getStatusCode() == 404){
                msg = 'Event Deletion Failed: Item not found';
            } else{
                msg = 'Event Deletion Failed';
            }
            system.debug('msg: ' + msg);
        } catch(exception e){
            System.debug('Exception: ' + e);
        }
    }

    /************************************************************************************************
    * @Description : Fetches the Calendar Event details using Graph API
    * @Params      : String eventId - Microsoft Event Id of the Calendar event to be fetched
    * @Return      : None
    ************************************************************************************************/
    public static void getEventDetails(String eventId){
        String token = fetchAccessToken();
        String msg = '';
        Http http = new Http();
        HttpRequest eventReq = new HttpRequest();
        eventReq.setHeader('Authorization', 'Bearer ' + token);
        String endPoint = apiValue.Event_Creation_URL__c;
        eventReq.setEndpoint(endPoint + '/' + eventId);
        eventReq.setMethod('GET');
        eventReq.setHeader('Accept', 'application/json');
        HttpResponse eventRes = new HttpResponse();
        try{
            String responseBody = '';
            if(!Test.isRunningTest()){
                eventRes = http.send(eventReq);
                responseBody = eventRes.getBody();
            } else{
                responseBody = '';
            }
            if(eventRes.getStatusCode() == 204 || Test.isRunningTest()){
                DAMAC_OutlookEvents_JSON eventDetails = DAMAC_OutlookEvents_JSON.parse(responseBody);
                system.debug('eventDetails: ' + eventDetails);
                msg = 'Event Fetched Successfully';    
            } else{
                msg = 'Event Fetch Failed';
            }
            system.debug('msg: ' + msg);
            system.debug('create meeting response body: ' + responseBody);
            
        } catch(exception e){
            System.debug('Exception: ' + e);
        }  
    }

    /************************************************************************************************
    * @Description : Webservice class to Invoke Teams Meeting from a button Click
    * @Params      : Id inqId - Id of the related Inquiry
    * @Return      : None
    ************************************************************************************************/
    webservice static void sendInvitation  (Id inqId) {
        List<Inquiry__c> inqList = new List<Inquiry__c>();
        for(Inquiry__c inq: [SELECT Id, Name, Online_Meeting_Start_Date_Time__c, 
                                Owner.Email, Meeting_Due_Date__c, 
                                Online_Meeting_End_Date_Time__c, Notes__c,
                                Email__c, First_Name__c, Last_Name__c,TSA_TL_of_Owner__c
                            FROM Inquiry__c 
                            WHERE Id =: inqId]){
            inqList.add(inq);
        }
        system.debug('inqList: ' + inqList);
        if(inqList.size() > 0){
            sendInvitation(inqList);
        }
    }
    
    /************************************************************************************************
    * @Description : Webservice class to Invoke Teams Meeting from a button Click
    * @Params      : Id inqId - Id of the related Inquiry
    * @Return      : None
    ************************************************************************************************/
    @future(callout=true)
    public static void scheduleMeetingInqList  (List<Id> inqIds) {
        List<Inquiry__c> inqList = new List<Inquiry__c>();
        for(Inquiry__c inq: [SELECT Id, Name, Online_Meeting_Start_Date_Time__c, 
                                Owner.Email, Meeting_Due_Date__c, 
                                Online_Meeting_End_Date_Time__c, Notes__c, 
                                Email__c, First_Name__c, Last_Name__c ,TSA_TL_of_Owner__c
                            FROM Inquiry__c 
                            WHERE Id IN: inqIds]){
            inqList.add(inq);
        }
        system.debug('inqList: ' + inqList);
        if(inqList.size() > 0){
            sendInvitation(inqList);
        }
    }
}