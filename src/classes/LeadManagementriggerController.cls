/***************************************************************************************************
* Description - The apex class is Controller for LeadManagementriggerHandler
*
* Version   Date            Author            Description
* 1.0       22/03/18        Monali            Initial Draft
* 1.1       25/03/18        Monali            Updated logic for custom metadata
* 1.2       03-06-2018      Craig Lobo        Updated the TeleSales Executive on Inquiry
* 1.3       24-09-2020      Srikanth V        When old inquiry and new inquiry both are same changing the status
***************************************************************************************************/

public class LeadManagementriggerController {
    public LeadManagementriggerController() {

    }

    public void LeadManagementUpdate(List<Lead_Management__c> lstLeadMgmt) {
        System.debug('=====LeadManagementriggerController====');
        List<Lead_Management__c> leadMgtLst = new List<Lead_Management__c>();
        List<Buyer__c> buyerList = new List<Buyer__c>();
        List<Inquiry__c> lstInquiryToUpdate = new List<Inquiry__c>();
        Group queueName = [select Id,NAME  from Group where  Type = 'Queue' AND NAME = 'DAMAC Queue' LImit 1];

        for(Lead_Management__c leadObj : lstLeadMgmt){  
            
        
            if(leadObj.Replace_inquiry_on_buyer__c == 'YES'){
                
                // Update the Original Inquiry Status to 'Closed Won'
                if(leadObj.New_Inquiry__c != null) {
                    Inquiry__c newInq = new Inquiry__c(
                        Id=leadObj.New_Inquiry__c,
                        Inquiry_Status__c = 'Closed Won'
                    );
                    lstInquiryToUpdate.add(newInq);
                } 

                // Update the Booking Inquiry owner to Damac Queue
                if(leadObj.New_Inquiry__c != leadObj.Old_Inquiry__c && leadObj.Old_Inquiry__c != null && queueName != null){
                    Inquiry__c oldInq = new Inquiry__c(
                        Id=leadObj.Old_Inquiry__c,
                        OwnerId = queueName.Id
                    );
                    lstInquiryToUpdate.add(oldInq);
                }

                // Update Inquiry on Buyer
                if(leadObj.Buyer__c != null) {
                    Buyer__c updatedBuyer = new Buyer__c(
                        Id = leadObj.Buyer__c, 
                        Inquiry__c = leadObj.New_Inquiry__c
                    );
                    buyerList.add(updatedBuyer);
                }
            }
        }
        try{
            update buyerList;
            System.debug('==lstInquiryToUpdate=='+lstInquiryToUpdate);
            Set<Inquiry__c> tempSet = new Set<Inquiry__c>();
            tempSet.addAll(lstInquiryToUpdate);
            lstInquiryToUpdate = new List<Inquiry__c>();
            lstInquiryToUpdate.addAll(tempSet);
            update lstInquiryToUpdate;
        }
        catch(DmlException excp) {
            System.debug('==excp=='+excp);
        }

    }
}