/* * * * * * * * * * * * * *
*  Class Name:   PaymentTermsReApplyControllerTest
*  Purpose:      Unit test class for PaymentTermsReApplyController Controller
*  Author:       Hardik Mehta - ESPL
*  Company:      ESPL
*  Created Date: 21-Nov-2017
*  Type:         Test Class
* * * * * * * * * * * * */
@isTest
public class PaymentTermsReApplyControllerTest{
  /* * * * * * * * * * * * *
  *  Method Name:  checkStatus
  *  Purpose:      This method is used to check the Payment Status
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 21-Nov-2017
  * * * * * * * * * * * * */ 
    public static testmethod void checkStatus(){   
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;       
        Case objCase = new Case();
        objCase.Status = 'NotSubmitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Case_Type__c = 'Proof of Payment';  
        insert objCase;       
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        PaymentTermsReApplyController  objPayment = new PaymentTermsReApplyController (stdController);
        objPayment.caseId = objCase.Id;
        objPayment.applyPaymentTerms(); 
    }
  /* * * * * * * * * * * * *
  *  Method Name:  checkApproved
  *  Purpose:      This method is used to check when the Case Approval Status is not Approved
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 21-Nov-2017
  * * * * * * * * * * * * */   
       public static testmethod void checkApproved(){   
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;       
        Case objCase = new Case();
        objCase.Status = 'Submitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Case_Type__c = 'Proof of Payment';
        objCase.Approval_Status__c = 'NotApproved';  
        insert objCase;        
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        PaymentTermsReApplyController  objPayment = new PaymentTermsReApplyController (stdController);
        objPayment.caseId = objCase.Id;
        objPayment.applyPaymentTerms(); 
    }
  /* * * * * * * * * * * * *
  *  Method Name:  checkSignedCopy
  *  Purpose:      This method is used to check the Signed copy of the Case
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 21-Nov-2017
  * * * * * * * * * * * * */    
    public static testmethod void checkSignedCopy(){   
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;        
        Case objCase = new Case();
        objCase.Status = 'Submitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Case_Type__c = 'Proof of Payment';
        objCase.Approval_Status__c = 'Approved';  
        insert objCase;        
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        PaymentTermsReApplyController  objPayment = new PaymentTermsReApplyController (stdController);
        objPayment.caseId = objCase.Id;
        objPayment.applyPaymentTerms(); 
    }
  /* * * * * * * * * * * * *
  *  Method Name:  checkAcceptanceLetter
  *  Purpose:      This method is used to check the Acceptance letter of the Case
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 21-Nov-2017
  * * * * * * * * * * * * */    
    public static testmethod void checkAcceptanceLetter(){    
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;        
        Case objCase = new Case();
        objCase.Status = 'Submitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Case_Type__c = 'Proof of Payment';
        objCase.Offer_Acceptance_Letter_Generated__c = True;
        objCase.Approval_Status__c = 'Approved';  
        insert objCase;        
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        PaymentTermsReApplyController  objPayment = new PaymentTermsReApplyController (stdController);
        objPayment.caseId = objCase.Id;
        objPayment.applyPaymentTerms(); 
    }
  /* * * * * * * * * * * * *
  *  Method Name:  checkCaseNotInserted
  *  Purpose:      This method is used to check when the Case in not inserted
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 21-Nov-2017
  * * * * * * * * * * * * */    
    public static testmethod void checkCaseNotInserted(){    
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount; 
        Case objCase = new Case();
        objCase.Status = 'Submitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Case_Type__c = 'Proof of Payment';
        objCase.Offer_Acceptance_Letter_Generated__c = True;
        objCase.Approval_Status__c = 'Approved';  
        insert objCase;       
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        PaymentTermsReApplyController  objPayment = new PaymentTermsReApplyController (stdController);
        objPayment.applyPaymentTerms(); 
    }
  /* * * * * * * * * * * * *
  *  Method Name:  checkCallout
  *  Purpose:      This method is used to check the Apex Callout
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 21-Nov-2017
  * * * * * * * * * * * * */     
    public static testmethod void checkCallout(){    
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;        
        Case objCase = new Case();
        objCase.Status = 'Submitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Case_Type__c = 'Proof of Payment';
        objCase.Offer_Acceptance_Letter_Generated__c = True;
        objCase.O_A_Signed_Copy_Uploaded__c =True;
        objCase.Approval_Status__c = 'Approved';  
        insert objCase;        
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        PaymentTermsReApplyController  objPayment = new PaymentTermsReApplyController (stdController);
        objPayment.caseId = objCase.Id;
        objPayment.applyPaymentTerms(); 
    }
}