public without sharing class FMPCCGeneration {
    
    public static FMPCCGeneration.FMPCCResponse GetFMPCC(String reqName, String regId, String caseId) {
        FMPCCGeneration.FMPCCResponse objDetailsWrapper ;
        FMPCC1.FMPCCHttpSoap11Endpoint objFMPCC = new FMPCC1.FMPCCHttpSoap11Endpoint();
        objFMPCC.timeout_x = 120000 ;
        list<FMPCC2.APPSXXDC_PROCESS_SERX1794747X1X5> lstReg = new list<FMPCC2.APPSXXDC_PROCESS_SERX1794747X1X5>();
        FMPCC2.APPSXXDC_PROCESS_SERX1794747X1X5 reg = new FMPCC2.APPSXXDC_PROCESS_SERX1794747X1X5();
        reg.ATTRIBUTE1 = '';
        reg.ATTRIBUTE10 = '';
        reg.ATTRIBUTE11 = '';
        reg.ATTRIBUTE12 = '';
        reg.ATTRIBUTE13 = '';
        reg.ATTRIBUTE14 = '';
        reg.ATTRIBUTE15 = '';
        reg.ATTRIBUTE16 = '';
        reg.ATTRIBUTE17 = '';
        reg.ATTRIBUTE18 = '';
        reg.ATTRIBUTE19 = '';
        reg.ATTRIBUTE2 = '';
        reg.ATTRIBUTE20 = '';
        reg.ATTRIBUTE21 = '';
        reg.ATTRIBUTE22 = '';
        reg.ATTRIBUTE23 = '';
        reg.ATTRIBUTE24 = '';
        reg.ATTRIBUTE25 = '';
        reg.ATTRIBUTE26 = '';
        reg.ATTRIBUTE27 = '';
        reg.ATTRIBUTE28 = '';
        reg.ATTRIBUTE29 = '';
        reg.ATTRIBUTE3 = caseId;
        reg.ATTRIBUTE30 = '';
        reg.ATTRIBUTE31 = '';
        reg.ATTRIBUTE32 = '';
        reg.ATTRIBUTE33 = '';
        reg.ATTRIBUTE34 = '';
        reg.ATTRIBUTE35 = '';
        reg.ATTRIBUTE36 = '';
        reg.ATTRIBUTE37 = '';
        reg.ATTRIBUTE38 = '';
        reg.ATTRIBUTE39 = '';
        reg.ATTRIBUTE4 = '';
        reg.ATTRIBUTE41 = '';
        reg.ATTRIBUTE42 = '';
        reg.ATTRIBUTE43 = '';
        reg.ATTRIBUTE44 = '';
        reg.ATTRIBUTE45 = '';
        reg.ATTRIBUTE46 = '';
        reg.ATTRIBUTE47 = '';
        reg.ATTRIBUTE48 = '';
        reg.ATTRIBUTE49 = '';
        reg.ATTRIBUTE5 = '';
        reg.ATTRIBUTE50 = '';
        reg.ATTRIBUTE6 = '';
        reg.ATTRIBUTE7 = '';
        reg.ATTRIBUTE8 = '';
        reg.ATTRIBUTE9 = '';
        reg.PARAM_ID = regId;
        lstReg.add(reg);
        system.debug('lstReg'+lstReg);
        string strHand;
        objDetailsWrapper = new FMPCCGeneration.FMPCCResponse();
        try {
            strHand = objFMPCC.fmPcc('123456', reqName,'SFDC',lstReg);
            system.debug('strHand '+strHand );
            JSON2Apex  objEligibleForHandoverResponse = new JSON2Apex ();
            objEligibleForHandoverResponse = (FMPCCGeneration.JSON2Apex)JSON.deserialize(strHand, FMPCCGeneration.JSON2Apex.class);
            String strResponse = strHand.substringAfter('[').substringBeforeLast(']');
            Map<String, object> objUnitDetails = (Map<String, object>)JSON.deserializeUntyped( strResponse.replace('\\','').removeStart('"').removeEnd('"') );
                      system.debug('objUnitDetails '+objUnitDetails );
                    
                    objDetailsWrapper.P_PARAM_ID= String.valueOf( objUnitDetails.get('PARAM_ID') );
                    objDetailsWrapper.P_PROC_STATUS= String.valueOf( objUnitDetails.get('PROC_STATUS') );
                    objDetailsWrapper.P_PROC_MESSAGE= String.valueOf( objUnitDetails.get('PROC_MESSAGE') );
                    objDetailsWrapper.URL= String.valueOf( objUnitDetails.get('ATTRIBUTE1') );
                    //objDetailsWrapper.P_ATTRIBUTE2= String.valueOf( objUnitDetails.get('ATTRIBUTE2') );
                    //objDetailsWrapper.P_ATTRIBUTE3= String.valueOf( objUnitDetails.get('ATTRIBUTE3') );
            system.debug(objDetailsWrapper);
        } catch (Exception e){
            objDetailsWrapper.P_PROC_MESSAGE = strHand;
            objDetailsWrapper.URL = '';
        }
        return objDetailsWrapper ;
    }
    
     public class JSON2Apex {
        public List<FMPCCResponse> data;
        public String message;
        public String status;
    }
    
    public class FMPCCResponse {
        public String P_PARAM_ID{get;set;}
        public String P_PROC_STATUS{get;set;}
        public String P_PROC_MESSAGE{get;set;}
        public String URL{get;set;}
    }
    
}