global without sharing class MakeCTICallCntrl {

    @testVisible private static final String GET                   = 'GET';
    @testVisible private static final String CONTENT_TYPE           = 'Content-Type';
    @testVisible private static final String APPLICATION_JSON       = 'application/json';
    @testVisible private static final String APPLICATION_URLENCODED = 'application/x-www-form-urlencoded';
    @testVisible private static final String APPLICATION_STREAM     = 'application/octet-stream';
     
    global static void doPost() {
        //TODO added code to hide no. at hover 
        Id objId = string.isNotBlank(ApexPages.currentPage().getParameters().get('ContactId'))?
            ApexPages.currentPage().getParameters().get('ContactId') : '';
        system.debug('objId : ' + objId );
        string fieldToquery = string.isNotBlank(ApexPages.currentPage().getParameters().get('Destination'))?
            ApexPages.currentPage().getParameters().get('Destination'):'';
        system.debug('fieldToquery' + fieldToquery );
        String sObjName = objId.getsobjecttype().getDescribe().getName();
        system.debug('sObjName : ' + sObjName );
        String query ;
        SObject  obj;

        if(string.isNotBlank( sObjName ) && string.isNotBlank( objId ) && string.isNotBlank( fieldToquery ) 
            && fieldToquery.contains('.') ){
                system.debug('fieldToquery : ' + fieldToquery );
                string objectToSubquery = fieldToquery.substringBefore('.') ;
                fieldToquery = fieldToquery.substringAfterLast('.');
                system.debug('fieldToquery : ' + fieldToquery );
                query =  ' SELECT Id,'+fieldToquery +' FROM ' + objectToSubquery + ' WHERE AccountId = \'' + objId + '\' LIMIT 1';

        } else if(string.isNotBlank( sObjName ) && string.isNotBlank( objId )){
             query = 'SELECT Id,'+ fieldToquery +' FROM ' + sObjName + ' WHERE Id = \'' + objId + '\' LIMIT 1';
        }
            
        system.debug('query : ' + query );
        if(string.isNotBlank( query ))
            obj = Database.query(query);
        System.debug('obj: ='+obj);
        
        User objUser = [SELECT Id
                             , Extension    
                          FROM User 
                         WHERE Id =: UserInfo.getUserId() 
                         LIMIT 1];
        String strMobile  = string.isNotBlank(ApexPages.currentPage().getParameters().get('Destination'))?
                            ApexPages.currentPage().getParameters().get('Destination'):'';
        String strEndpoint = string.isNotBlank(strMobile)? 'http://94.200.40.220:5010/api/values?to='+obj.get(fieldToquery)+'&extension='+objUser.Extension+'&RecordId='+objId+'&action=makecall':'';
        //String strEndpoint = string.isNotBlank(strMobile)? 'http://94.200.40.220:5010/api/values?to='+obj.get(fieldToquery)+'&extension='+objUser.Extension+'&action=makecall':'';
        //String strEndpoint = 'http://10.1.27.200:5010/api/values?to=00971556695784&extension=6506&action=makecall';
        //String strEndpoint = 'http://94.200.40.220:5010/api/values?to=00971556695784&extension=6506&action=makecall';//shared by rebin as public ip

        system.debug( '<< strEndpoint  : ' + strEndpoint  );
        if(string.isNotBlank(strEndpoint)){
            system.debug( '<< in if call  : '   );
            HttpRequest request = new HttpRequest();
            request.setEndpoint( strEndpoint  );
            request.setMethod( GET ); // Store in static variables
            system.debug( '<< in after GET : '   );
            request.setHeader( CONTENT_TYPE , APPLICATION_STREAM); // Store in static variables
    
            HttpResponse response = new Http().send(request); // create error log - GenericUtility.createErrorLog
            system.debug( '<< in if response : '  +response  );
            if(response.getStatusCode() == 200) {
            }
        
        }else{
        }
        
    }
    

    global Pagereference backToDetailPage(){
        
        pageReference pg = new pageReference('/'+ApexPages.currentPage().getParameters().get('ContactId'));
        pg.setRedirect(TRUE);
        return pg;
    }
}