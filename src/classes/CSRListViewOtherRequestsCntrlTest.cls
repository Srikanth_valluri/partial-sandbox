/****************************************************************************************
Description: CSRListViewOtherRequestsCntrlTest is a test class for the
CSRListViewOtherRequestsCntrl
----------------------------------------------------------------------------------------*
Version     Date        Author              Description                                 *
1.0        04/08/2020   Shruti Nikam        Initial Draft
*****************************************************************************************/

@isTest
public class CSRListViewOtherRequestsCntrlTest {

    /**************************************************************************************
    * Test Method Description: Method to retrive all cases of recordtype: 
                Case Summary - Client Relation and current logged in user
                and to test the functionality of CSRListViewOtherRequests Page
    ***************************************************************************************/
    @IsTest
    static void testApproved(){
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='System Administrator' Limit 1];
        
        UserRole userRoleObj = new UserRole(Name = 'Director', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        User ObjUser = new User(Alias = 'standt1', Email='stan1@testorg.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', 
                                LanguageLocaleKey='en_US', isActive = true,
                                LocaleSidKey='en_US', ProfileId = objProfile.Id, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='sta4@damactest1234.com');
        insert ObjUser;
        
        System.runAs(ObjUser) {
            Account objAcc = TestDataFactory_CRM.createPersonAccount();
            objAcc.Email__c ='test@test.com';
            objAcc.Mobile_Phone_Encrypt__pc = '0097405883798';
            insert objAcc ;
            
            Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Case Summary - Client Relation').getRecordTypeId();
            System.debug('recTypeCOD :'+ recTypeCOD);
            
            // insert Case
            List<case> caseList = new List<case>();
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
            objCase.Approving_User_Id__c = Userinfo.getuserid();
            objCase.Approving_User_Name__c = 'Testing';
            objCase.Approving_User_Role__c = 'Manager';
            objCase.Status = 'Submitted';
            caseList.add(objCase); 
            Case objCase1 = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
            objCase1.Approving_User_Id__c = Userinfo.getuserid();
            objCase1.Approving_User_Name__c = 'Testing';
            objCase1.Approving_User_Role__c = 'Committee';
            objCase1.Status = 'Submitted';
            caseList.add(objCase1) ;
            Case objCase2 = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
            objCase2.Approving_User_Id__c = Userinfo.getuserid();
            objCase2.Approving_User_Name__c = 'Testing';
            objCase2.Approving_User_Role__c = 'HOD';
            objCase2.Status = 'Submitted';
            caseList.add(objCase2);
            Case objCase3 = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
            objCase3.Approving_User_Id__c = Userinfo.getuserid();
            objCase3.Approving_User_Name__c = 'Testing';
            objCase3.Approving_User_Role__c = 'Director';
            objCase3.Status = 'Submitted';
            caseList.add(objCase3);
            insert caseList;
            List<CaseSummaryRequest__c> caseSummaryList = new List<CaseSummaryRequest__c>();
            CaseSummaryRequest__c objCSR = new CaseSummaryRequest__c();
            objCSR.CRE_Comments__c = 'Test';
            objCSR.CRE_Request_History__c = '% of Area Variation to be waived=>550,Amount=>,';
            objCSR.RequestType__c = 'Area Variation Waiver';
            objCSR.Per_Area_Variation_to_be_waived__c = '550'; 
            objCSR.case__c = objCase.Id;
            objCSR.Manager_Request_History__c ='% of Area Variation to be waived=>550';
            objCSR.Director_Request_History__c ='% of Area Variation to be waived=>550';
            objCSR.HOD_Request_History__c ='% of Area Variation to be waived=>550';
            objCSR.HOD_Id__c = Userinfo.getuserid();
            objCSR.Manager_Id__c = Userinfo.getuserid();
            objCSR.Committee_Id__c = Userinfo.getuserid();
            objCSR.Director_Id__c = Userinfo.getuserid();
            objCSR.Manager_Approval_Status__c = 'Approved';
            objCSR.Manager_Approval_Date__c = system.now();
            objCSR.Manager_Approval_Assignment_Date__c = system.now();
            caseSummaryList.add(objCSR);
            CaseSummaryRequest__c objCSR1 = new CaseSummaryRequest__c();
            objCSR1.CRE_Comments__c = 'Test';
            objCSR1.CRE_Request_History__c = '% of Area Variation to be waived=>550,Amount=>,';
            objCSR1.RequestType__c = 'Area Variation Waiver';
            objCSR1.Per_Area_Variation_to_be_waived__c = '550';
            objCSR1.case__c = objCase1.Id;
            objCSR1.Manager_Request_History__c ='% of Area Variation to be waived=>550';
            objCSR1.Director_Request_History__c ='% of Area Variation to be waived=>550';
            objCSR1.HOD_Request_History__c ='% of Area Variation to be waived=>550';
            objCSR1.HOD_Id__c = Userinfo.getuserid();
            objCSR1.Manager_Id__c = Userinfo.getuserid();
            objCSR1.Committee_Id__c = Userinfo.getuserid();
            objCSR1.Director_Id__c = Userinfo.getuserid();
            objCSR1.Committee_Approval_Status__c = 'Approved';
            objCSR1.Committee_Approval_Assignment_Date__c = system.now();
            objCSR1.Committee_Approval_Date__c = system.now();
            caseSummaryList.add(objCSR1);     
            CaseSummaryRequest__c objCSR2 = new CaseSummaryRequest__c();
            objCSR2.CRE_Comments__c = 'Test';
            objCSR2.CRE_Request_History__c = '% of Area Variation to be waived=>550,Amount=>,';
            objCSR2.RequestType__c = 'Area Variation Waiver';
            objCSR2.Per_Area_Variation_to_be_waived__c = '550';
            objCSR2.case__c = objCase2.Id;
            objCSR2.Manager_Request_History__c ='% of Area Variation to be waived=>550';
            objCSR2.Director_Request_History__c ='% of Area Variation to be waived=>550';
            objCSR2.HOD_Request_History__c ='% of Area Variation to be waived=>550';
            objCSR2.HOD_Id__c = Userinfo.getuserid();
            objCSR2.Manager_Id__c = Userinfo.getuserid();
            objCSR2.Committee_Id__c = Userinfo.getuserid();
            objCSR2.Director_Id__c = Userinfo.getuserid();
            objCSR2.HOD_Approval_Status__c = 'Rejected';
            objCSR2.HOD_Approval_Assignment_Date__c = system.now();
            objCSR2.HOD_Approval_Date__c = system.now();
            caseSummaryList.add(objCSR2); 
            CaseSummaryRequest__c objCSR3 = new CaseSummaryRequest__c();
            objCSR3.CRE_Comments__c = 'Test';
            objCSR3.CRE_Request_History__c = '% of Area Variation to be waived=>550,Amount=>,';
            objCSR3.RequestType__c = 'Area Variation Waiver';
            objCSR3.Per_Area_Variation_to_be_waived__c = '550';
            objCSR3.case__c = objCase3.Id;
            objCSR3.Manager_Request_History__c ='% of Area Variation to be waived=>550';
            objCSR3.Director_Request_History__c ='% of Area Variation to be waived=>550';
            objCSR3.HOD_Request_History__c ='% of Area Variation to be waived=>550';
            objCSR3.HOD_Id__c = Userinfo.getuserid();
            objCSR3.Manager_Id__c = Userinfo.getuserid();
            objCSR3.Committee_Id__c = Userinfo.getuserid();
            objCSR3.Director_Id__c = Userinfo.getuserid();
            objCSR3.Director_Approval_Status__c = 'Rejected';
            objCSR3.Director_Approval_Assignment_Date__c = system.now();
            objCSR3.Director_Approval_Date__c = system.now();
            caseSummaryList.add(objCSR3); 
            CaseSummaryRequest__c objCSR4 = new CaseSummaryRequest__c();
            objCSR4.CRE_Comments__c = 'Test';
            objCSR4.CRE_Request_History__c = '% of Area Variation to be waived=>550,Amount=>,';
            objCSR4.RequestType__c = 'Area Variation Waiver';
            objCSR4.Per_Area_Variation_to_be_waived__c = '550';
            objCSR4.case__c = objCase3.Id;
            objCSR4.Manager_Request_History__c ='% of Area Variation to be waived=>550';
            objCSR4.Director_Request_History__c ='% of Area Variation to be waived=>550';
            objCSR4.HOD_Request_History__c ='% of Area Variation to be waived=>550';
            objCSR4.HOD_Id__c = Userinfo.getuserid();
            objCSR4.Manager_Id__c = Userinfo.getuserid();
            objCSR4.Committee_Id__c = Userinfo.getuserid();
            objCSR4.Director_Id__c = Userinfo.getuserid();
            objCSR4.Director_Approval_Status__c = 'Rejected';
            objCSR4.Director_Approval_Assignment_Date__c = system.now();
            objCSR4.Director_Approval_Date__c = system.now();
            caseSummaryList.add(objCSR4); 
            insert caseSummaryList;
                        
            Test.startTest();
            CSRListViewOtherRequestsCntrl obj = new CSRListViewOtherRequestsCntrl();
            obj.userid = 'test';
            CSRListViewOtherRequestsCntrl.getParameterBasedSRCount('Approved', '15');
            CSRListViewOtherRequestsCntrl.getParameterBasedSRList('Rejected', '30');
            CSRListViewOtherRequestsCntrl.getTotalSRCount('All');
            Test.stopTest();
        }
    }
}