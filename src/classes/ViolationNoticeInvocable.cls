public with sharing class ViolationNoticeInvocable {
	@InvocableMethod
	public static void closeViolationNoticeFMCase(List<Task> lstTasks) {
			try {
					System.debug('==lstTasks=='+lstTasks);
					List<Task> lstOtherTasks = new List<Task>();
					lstOtherTasks = [SELECT ID, Subject,whatID, Status FROM Task
							where WHATID =: lstTasks[0].whatID];
					System.debug('==lstOtherTasks=='+lstOtherTasks);

					Integer taskCnt=0;
					for(Task taskObj : lstOtherTasks) {
							if((taskObj.Subject == Label.TaskToCreateVNForPM
							|| taskObj.Subject == Label.TaskLabel_PostFineSoA) && taskObj.Status == 'Completed') {
									taskCnt++;
							}
					}//for
					//Get FM case to close
					List<FM_Case__c> lstFMCases = new List<FM_Case__c>();
					lstFMCases = [SELECT Id from FM_Case__c where ID=:lstTasks[0].whatID AND
												RecordType.DeveloperName ='Violation_Notice' LIMIT 1];
					if((taskCnt ==2 || (lstOtherTasks.size() == taskCnt))&& lstFMCases.size() >0) {
							lstFMCases[0].Status__c = 'Closed';
					}
					update lstFMCases;
			}
			catch(Exception excp) {
					System.debug('==excp=='+excp);
			}
	}
}