@isTest
public with sharing class PromotionsAmountPostingWSDLTest 
{
    static testMethod void init_test_scenario1()
    {
        PromotionsAmountPostingWSDL.CREATE_DM_CM_element objCreateDMCM = new PromotionsAmountPostingWSDL.CREATE_DM_CM_element();
        PromotionsAmountPostingWSDL.updateMortgageResponse_element objMortgageRes = new PromotionsAmountPostingWSDL.updateMortgageResponse_element();
        PromotionsAmountPostingWSDL.CREATE_DM_CMResponse_element objCreateDMCMRes = new PromotionsAmountPostingWSDL.CREATE_DM_CMResponse_element();
        PromotionsAmountPostingWSDL.updateMortgage_element objMortgageElement = new PromotionsAmountPostingWSDL.updateMortgage_element();

          processXxdcProcessServiceWsPlsqlPromotio.APPSXXDC_PROCESS_SERX1794747X1X5[] objRegTermsList = new 
                                           processXxdcProcessServiceWsPlsqlPromotio.APPSXXDC_PROCESS_SERX1794747X1X5[1];
          processXxdcProcessServiceWsPlsqlPromotio.APPSXXDC_PROCESS_SERX1794747X1X5 objRegTerm = new 
                                           processXxdcProcessServiceWsPlsqlPromotio.APPSXXDC_PROCESS_SERX1794747X1X5();

          objRegTerm.PARAM_ID = '12345';
          objRegTerm.ATTRIBUTE1 = '500';
          objRegTerm.ATTRIBUTE2 = 'Promotion Recharges';
          objRegTerm.ATTRIBUTE3 = '3rd Party Re-charges';
          objRegTerm.ATTRIBUTE4 = 'PromotionRecharges12345';
          Datetime objDateTime = System.now();
          objRegTerm.ATTRIBUTE5 = String.valueOf(objDateTime.format('dd-MMM-yyyy').toUpperCase());
          objRegTermsList[0] = objRegTerm;

          Test.startTest();
              Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );
              PromotionsAmountPostingWSDL.ProcessHttpSoap11Endpoint objEndpoint = new PromotionsAmountPostingWSDL.ProcessHttpSoap11Endpoint();
              objEndpoint.CREATE_DM_CM( '2-12345'
                                         ,'CREATE_DM_CM'
                                         ,'SFDC'
                                         , objRegTermsList
                                      );
              objEndpoint.updateMortgage( '2-12345'
                                         ,'CREATE_DM_CM'
                                         ,'SFDC'
                                         , objRegTermsList
                                       );
          Test.stopTest();
    }
}