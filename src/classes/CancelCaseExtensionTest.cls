/**
 * Description: Test class for CancelCaseExtension
 */

@isTest
private class CancelCaseExtensionTest {

    /*
     * Description : Test method to check whether the Case status and Task status is set to Cancelled or not
     */
    @isTest static void testCancellationOfCaseAndTask() {

        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        Id parkingRecordTypeID = getRecordTypeIdForParking();

        // Creation of Case
        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , parkingRecordTypeID);
        insert objCase;

        objCase = [Select OwnerId FROM Case WHERE Id =: objCase.Id];

        // Creation of Task
        Task objTask = new Task();
        objTask.subject = 'Generate Parking Offer & Acceptance Letter';
        objTask.whatId = objCase.Id;
        objTask.status = 'In Progress';
        objTask.Assigned_User__c = 'Finance';
        insert objTask;

        PageReference redirectionUrl;
        Test.startTest();
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT());
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(objCase);
            CancelCaseExtension objController = new CancelCaseExtension(standardControllerInstance);
            Test.setCurrentPageReference(new PageReference('Page.CancelCasePage'));
            System.currentPageReference().getParameters().put('Id', objCase.Id);
            redirectionUrl = objController.cancelNonCRETasks();
        Test.stopTest();

        // Check whether redirected to case detail page
        System.assert(redirectionUrl != null);
        System.assertEquals(redirectionUrl.getURL(), '/'+objCase.Id);

        // Check whether Case is Cancelled
        objCase = [SELECT Id, Status FROM Case WHERE Id =: objCase.Id];
        System.assert(objCase.Status == 'Cancelled');

        // Check whether Task is Cancelled
        objTask = [SELECT Id, Status FROM Task WHERE Id =: objTask.Id];
        System.assert(objTask.Status == 'Cancelled');
        
    }

    /*
     * Description : Test method to check whether the Case is cancelled or not when the open Non-CRE Tasks are not present.
     */
    @isTest static void testCaseWithoutNonCREOpenTask() {

        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        Id parkingRecordTypeID = getRecordTypeIdForParking();

        // Creation of Case
        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , parkingRecordTypeID);
        insert objCase;

        objCase = [Select OwnerId FROM Case WHERE Id =:objCase.Id];

        // Creation of Task
        Task objTask = new Task();
        objTask.subject = 'Generate Parking Offer & Acceptance Letter';
        objTask.whatId = objCase.Id;
        objTask.status = 'In Progress';
        objTask.Assigned_User__c = 'CRE';
        insert objTask;

        PageReference redirectionUrl;
        Test.startTest();
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT());
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(objCase);
            CancelCaseExtension objController = new CancelCaseExtension(standardControllerInstance);
            Test.setCurrentPageReference(new PageReference('Page.CancelCasePage'));
            System.currentPageReference().getParameters().put('Id', objCase.Id);
            redirectionUrl = objController.cancelNonCRETasks();
        Test.stopTest();

        // Check whether Case is Cancelled
        objCase = [SELECT Id, Status FROM Case WHERE Id =: objCase.Id];
        System.assert(objCase.Status == 'Cancelled');
    }

    /**
     * Method to get the "Parking" record type
     */
    private static Id getRecordTypeIdForParking() {
      Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
      Id parkingRecordTypeID = caseRecordTypes.get('Parking').getRecordTypeId();
      return parkingRecordTypeID;
    }
}