@istest
public class TaskTriggerHandlerTest{
    
    
    static testmethod void TaskTriggerHandler_Methods(){
        Skip_Task_Trigger_for_Informatica__c obj = new Skip_Task_Trigger_for_Informatica__c ();
        obj.Enable__c = true;
        insert obj;
        
        Test.startTest();
        // Insert Inquiry        
        Inquiry__c i = new Inquiry__c();
        i.First_Name__c = 'Lead';
        i.Last_Name__c = 'Lead';
        i.Email__c = 'lead@lead.com';
        i.Inquiry_Source__c = 'Web';
        i.Organisation_Name__c = 'abc';
        insert i;
        
        // Insert Inquiry related tasks
        task tsk = new task();
        tsk.whatid = i.id;
        tsk.subject = 'Testing';
        tsk.Calling_Number__c = '971557030756';
        insert tsk;
        
        tsk.subject = 'Updated';
        update tsk;
        Test.stopTest();
        
    }
    static testmethod void TaskTriggerHandler_Methods1(){
        
        Skip_Task_Trigger_for_Informatica__c obj = new Skip_Task_Trigger_for_Informatica__c ();
        obj.Enable__c = true;
        insert obj;
        Test.startTest();
        
        // Insert Inquiry        
        Inquiry__c i = new Inquiry__c();
        i.First_Name__c = 'Lead';
        i.Last_Name__c = 'Lead';
        i.Email__c = 'lead@lead.com';
        i.Inquiry_Source__c = 'Digital';
        i.Organisation_Name__c = 'abc';
        insert i;
        
        // Insert Inquiry related tasks
        task tsk = new task();
        tsk.whatid = i.id;
        tsk.subject = 'Call';
        tsk.Calling_Number__c = '971557030756';
        insert tsk;
        tsk.subject = 'Updated';
        update tsk;
        
        
        // Insert Inquiry related tasks
        task tsk1 = new task();
        tsk1.whatid = i.id;
        tsk1.subject = 'Call';
        tsk1.User_Ext_No__c = '432';
        upsert tsk1;
        
        delete tsk;
        Test.stopTest();
        
    }
    
    static testmethod void TaskTriggerHandler_Methods2(){
        Skip_Task_Trigger_for_Informatica__c obj = new Skip_Task_Trigger_for_Informatica__c ();
        obj.Enable__c = true;
        insert obj;
        
        Profile standardProfile = [SELECT Id FROM profile WHERE Name = 'System Administrator'];
        //UserRole groupHsseRole = [SELECT Id FROM UserRole WHERE Name = 'PC1'];
        User userObject = new User();
        userObject.alias = 'standtest';
        userObject.alias = userObject.alias.subString(0, 8);
        userObject.email = 'standtest@damac.ae';
        userObject.emailencodingkey = 'UTF-8';
        userObject.lastname = 'standtest CTI';
        userObject.languagelocalekey = 'en_US';
        userObject.localesidkey = 'en_US';
        userObject.profileId = standardProfile.Id;
        userObject.timezonesidkey = 'America/Los_Angeles';
        userObject.username = 'standtest@damac.ae';
        //userObject.UserRoleId = groupHsseRole.Id;
        userObject.Extension = '123';
        userObject.Break_Time__c = system.now();
        
        system.runAs(userObject){
            
            Inquiry__c i = new Inquiry__c();
            i.First_Name__c = 'Lead';
            i.Last_Name__c = 'Lead';
            i.Email__c = 'lead@lead.com';
            i.Inquiry_Source__c = 'Digital';
            i.Organisation_Name__c = 'abc';
            insert i;
            
            // Insert Inquiry related tasks
            task tsk = new task();
            tsk.whatid = i.id;
            tsk.subject = 'Call';
            tsk.OwnerId = userObject.Id;
            tsk.User_Ext_No__c = '123';
            tsk.Calling_Number__c = '971557030756';
            insert tsk;
            tsk.Calling_Number__c = '971557030755';
            tsk.subject = 'Updated';
            update tsk;
            tsk.subject = 'Call';
            update tsk;
            
            // Insert Inquiry related tasks
            task tsk1 = new task();
            tsk1.whatid = i.id;
            tsk1.subject = 'Call';
            tsk1.OwnerId = userObject.Id;
            tsk1.User_Ext_No__c = '432';
            upsert tsk1;
            
            delete tsk;
            
        }
        
    }
    
    static testmethod void TaskTriggerHandler_Methods3(){
        Activate_New_Assignment_Process__c process = new Activate_New_Assignment_Process__c ();
         
         process.Activate_Inq_Status_Update_from_Task_Trg__c = true;
         insert process;
         
        Skip_Task_Trigger_for_Informatica__c obj = new Skip_Task_Trigger_for_Informatica__c ();
        obj.Enable__c = true;
        insert obj;
        
        Profile standardProfile = [SELECT Id FROM profile WHERE Name = 'System Administrator'];
        //UserRole groupHsseRole = [SELECT Id FROM UserRole WHERE Name = 'PC1'];
        User userObject = new User();
        userObject.alias = 'standtest';
        userObject.alias = userObject.alias.subString(0, 8);
        userObject.email = 'standtest@damac.ae';
        userObject.emailencodingkey = 'UTF-8';
        userObject.lastname = 'standtest CTI';
        userObject.languagelocalekey = 'en_US';
        userObject.localesidkey = 'en_US';
        userObject.profileId = standardProfile.Id;
        userObject.timezonesidkey = 'America/Los_Angeles';
        userObject.username = 'standtest@damac.ae';
        //userObject.UserRoleId = groupHsseRole.Id;
        userObject.Extension = '123';
        userObject.Break_Time__c = system.now();
        
        system.runAs(userObject){
            
            Inquiry__c i = new Inquiry__c();
            i.First_Name__c = 'Lead';
            i.Last_Name__c = 'Lead';
            i.Email__c = 'lead@lead.com';
            i.Inquiry_Source__c = 'Digital';
            i.Organisation_Name__c = 'abc';
            insert i;
            
            List<Campaign__c> c = TestDataFactory.createCampaignRecords(new List<Campaign__c>{new Campaign__c(RecordTypeId=Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Roadshows').getRecordTypeId())});           
            Virtual_Number__c vn = new Virtual_Number__c();
            vn.Name = '8001234';
            vn.Active__c = true;
            vn.Start_Date__c = system.now().date();
            vn.End_Date__c = system.now().date().addDays(12);
            insert vn;
            
            JO_Campaign_Virtual_Number__c jobj = new JO_Campaign_Virtual_Number__c();
            jobj.Related_Campaign__c = c[0].id;
            jobj.Related_Virtual_Number__c = vn.id;
            insert jobj;
            
            // Insert Inquiry related tasks
            task tsk = new task();
            tsk.whatid = i.id;
            tsk.subject = 'Call';
            tsk.OwnerId = userObject.Id;
            tsk.User_Ext_No__c = '123';
            tsk.Campaign_Number__c = vn.Name;
            tsk.Calling_Number__c = '971557030756';
            insert tsk;
            tsk.subject = 'Updated';
            update tsk;
            
            
            // Insert Inquiry related tasks
            task tsk1 = new task();
            tsk1.whatid = i.id;
            tsk1.subject = 'Call';
            tsk1.OwnerId = userObject.Id;
            tsk1.User_Ext_No__c = '432';
            tsk.Calling_Number__c = '971557030756';
            upsert tsk1;
            
            delete tsk;
        }
    }
    
    
     static testmethod void TaskTriggerHandler_Methods_CRM(){
         Activate_New_Assignment_Process__c process = new Activate_New_Assignment_Process__c ();
         
         process.Activate_Inq_Status_Update_from_Task_Trg__c = true;
         insert process;
         
        Skip_Task_Trigger_for_Informatica__c obj = new Skip_Task_Trigger_for_Informatica__c ();
        obj.Enable__c = true;
        insert obj;
        
        Test.startTest();
        // Insert Inquiry        
        Inquiry__c i = new Inquiry__c();
        i.First_Name__c = 'Lead';
        i.Last_Name__c = 'Lead';
        i.Email__c = 'lead@lead.com';
        i.Inquiry_Source__c = 'Web';
        i.Organisation_Name__c = 'abc';
        insert i;
        
        // Insert Inquiry related tasks
        task tsk = new task();
        tsk.whatid = i.id;
        tsk.subject = 'Testing';
        tsk.Calling_Number__c = '971557030756';
        tsk.Process_Name__c='Early Handover';
        insert tsk;
        
        tsk.subject = 'Updated';
        update tsk;
        Test.stopTest();
        
    }
}