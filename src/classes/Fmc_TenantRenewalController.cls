/*----------------------------------------------------------------------
Description: Controller for Tenant renewal request component

========================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
-----------------------------------------------------------------------
1.0     | 26-07-2018       | Bhanu Gupta   | 1. Initial draft
======================================================================= 
-------------------------------------------------------------------------*/

public without sharing class Fmc_TenantRenewalController extends FMTenantRenewalController {
    public list<SelectOption> lstUnit {get; set;}
    String caseId {get;set;}

    public Fmc_TenantRenewalController(){
        super(false);
        if (FmcUtils.isCurrentView('TenantRenewal')) {
            lstUnit = new list<SelectOption>();
            Map<String, String> params = ApexPages.currentPage().getParameters();
            System.debug('params: '+params);
            strCaseId = params.get('id');
            System.debug('-->> 1: ');
            if (String.isBlank(strCaseId)) {
                List<FM_Case__c> lstCase = [
                        SELECT  Id
                        FROM    FM_Case__c
                        WHERE   Status__c IN ('New', 'Draft Request', 'In Progress')
                            AND Account__c = :strAccountId
                            AND OwnerId = :UserInfo.getUserId()
                            AND Origin__c = 'Portal'
                            AND Request_Type__c = 'Tenant Renewal'
                        LIMIT 1
                ];
                System.debug('lstCase = ' + lstCase);
                if (!lstCase.isEmpty()) {
                    System.debug('-->> 2: ');
                    strCaseId = lstCase[0].Id;
                    objFMCase = FM_Utility.getCaseDetails(strCaseId);
                    lstUnit = new List<SelectOption>{
                        new SelectOption(objFMCase.Booking_Unit__c, objFMCase.Unit_Name__c)
                    };
                    strSelectedUnit = objFMCase.Booking_Unit__c;
                    System.debug('-->> lstUnit: '+ lstUnit);
                    System.debug('-->> objFMCase: '+ objFMCase);
                    System.debug('-->> objFMCase.Unit_Name__c: '+ objFMCase.Unit_Name__c);
                    System.debug('-->> strSelectedUnit: '+ strSelectedUnit);
                    init();
                }
            } else {
                objFMCase = FM_Utility.getCaseDetails(strCaseId);
                lstUnit = new List<SelectOption>{
                    new SelectOption(objFMCase.Booking_Unit__c, objFMCase.Unit_Name__c)
                };
                strSelectedUnit = objFMCase.Booking_Unit__c;
                System.debug('-->> 2 lstUnit: '+ lstUnit);
                System.debug('-->> 2 objFMCase: '+ objFMCase);
                System.debug('-->> 2 objFMCase.Unit_Name__c: '+ objFMCase.Unit_Name__c);
                System.debug('-->> 2 strSelectedUnit: '+ strSelectedUnit);
                init();
            }
            if(String.isBlank(strCaseId)) {
                strAccountId = CustomerCommunityUtils.customerAccountId;
            } else {
                objFMCase = FM_Utility.getCaseDetails( strCaseId );
                strAccountId = objFMCase.Account__c ;
                objFMCase = FM_Utility.getCaseDetails(strCaseId);
            }
            strSRType = FmcUtils.isOwner() ? 'Tenant_Renewal' : 'Tenant_Renewal_T';
            System.debug('== strAccountId ==' + strAccountId );
            System.debug('== strSelectedUnit ==' + strSelectedUnit );
            System.debug('== strFMCaseId ==' + strCaseId );
            lstUnit = getUnitOptions();
            System.debug('-->> 3 lstUnit: '+ lstUnit);
        }
    }

    private static List<SelectOption> getUnitOptions() {
        List<SelectOption> lstOptions = new List<SelectOption>{new SelectOption('', 'Select', false)};
        for (Booking_Unit__c unit : FmcUtils.queryUnitsForAccount(
            CustomerCommunityUtils.customerAccountId, 'Id, Name, Unit_Name__c'
        )) {
            lstOptions.add(new SelectOption(unit.Id, unit.Unit_Name__c));
            System.debug('-->> 3 unit.Unit_Name__c: '+ unit.Unit_Name__c);
            System.debug('-->> 3 unit.Id: '+ unit.Id);
        }
        return lstOptions;
    }

    public override void initializeFMCase() {
        if( objFMCase != NULL ) {
            strOldCaseId = objFMCase.Id ;
            objFMCase.Parent_Case__c = objFMCase.Id ;
            objFMCase.Id = NULL ;
            objFMCase.Status__c = 'New';
            objFMCase.Account__c = strAccountId;
            objFMCase.Request_Type__c = 'Tenant Renewal';
            objFMCase.Request_Type_DeveloperName__c = strSRType;
            objFMCase.Origin__c = 'Portal';
            objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Renewal').getRecordTypeId();
        }
    }

    public override void init() {

        if( String.isNotBlank( strSelectedUnit ) ) {
            objUnit = FM_Utility.getUnitDetails( strSelectedUnit ) ;
        }
        System.debug('strCaseId = ' + strCaseId);
        System.debug('strAccountId = ' + strAccountId);
        System.debug('strSelectedUnit = ' + strSelectedUnit);
        if( String.isBlank( strCaseId ) && String.isNotBlank( strAccountId ) &&
            String.isNotBlank( strSelectedUnit ) ) {
            objFMCase = getTenantRgistrationCaseDetails();
            System.debug('-->> objFMCase = ' + objFMCase);
            if( objFMCase != NULL ) {
                initializeFMCase();
            }
            else {
                objFMCase = new FM_Case__c();
                objFMCase.Status__c = 'New';
                objFMCase.Account__c = strAccountId;
                objFMCase.Request_Type__c = 'Tenant Renewal';
                objFMCase.Request_Type_DeveloperName__c = strSRType;
                objFMCase.Origin__c = 'Portal';
                objFMCase.Booking_Unit__c = strSelectedUnit;
                objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Renewal').getRecordTypeId();
            }
        }

        processDocuments();
        initializeAddDetailsMap();

        if( objUnit != NULL && String.isNotBlank( objUnit.Registration_Id__c ) ) {
          try {
            String strDues = AssignmentEndpoints.fetchAssignmentDues( objUnit.Registration_Id__c );
            if( String.isNotBlank( strDues ) ) {
              Map<String, Object> objWrap1 = (Map<String, Object>)JSON.deserializeUntyped( strDues ) ;
              if( objWrap1 != NULL && objWrap1.containsKey( 'status' ) &&
                String.valueOf( objWrap1.get( 'status' ) ).equalsIgnoreCase( 'S' ) &&
                objWrap1.containsKey( 'FM Balance as per SOA' ) ) {
                objFMCase.Outstanding_service_charges__c = (String)objWrap1.get( 'FM Balance as per SOA' );
              }
            }
          }
          catch( Exception e ) {
            system.debug( '==Exception while fecthing oustanding balance=='+e.getMessage() );
                Error_Log__c objError = FM_Utility.createErrorLog( strAccountId,
                                                                   strSelectedUnit,
                                                                   objFMCase != NULL ? objFMCase.Id : NULL,
                                                                   'Tenant Renewal',
                                                                   e.getMessage() + '\n' + e.getStackTraceString() + '\n' +e.getLineNumber() );
                insert objError ;
          }
        }
    }

    public void selectUnit() {
        System.debug('strSelectedUnit = ' + strSelectedUnit);
        if( String.isNotBlank( strSelectedUnit ) ) {
            init();
            //insertCase();
        }
    }

    public override PageReference createFmCase() {
        if( objFMCase != NULL ) {

            objFMCase.Status__c = 'In Progress';
            upsert objFMCase ;

            updateAdditionalDetails();
            //PageReference objPage = new PageReference('/' + objFMCase.Id );
            //return objPage ;
        }
        return NULL;
    }

    public override PageReference submitFmCase() {
        if( objFMCase != NULL ) {
            objFMCase.Status__c = 'Submitted';
            objFMCase.Submitted__c = true ;

            //Populate the Approving Authorities for TR Case.
            objFMCase.Approving_Authorities__c = '';
            if( String.isNotBlank( objFMCase.Request_Type_DeveloperName__c ) ) {
          for( FM_Approver__mdt objApproMeta : FM_Utility.fetchApprovers(   objFMCase.Request_Type_DeveloperName__c,
                                                    objUnit.Property_City__c ) ) {
            objFMCase.Approving_Authorities__c += objApproMeta.Role__c + ',';
          }
          objFMCase.Approving_Authorities__c = objFMCase.Approving_Authorities__c.removeEnd(',');
        }
        objFMCase.Submit_for_Approval__c = false ;
        objFMCase.Approval_Status__c = 'Pending';

        //Populate the FM Manager email address.
        if( String.isNotBlank( objFMCase.Unit_Name__c ) ) {
          list<FM_User__c> lstUsers = FM_Utility.getApprovingUsers(new set<String> { objFMCase.Unit_Name__c.split('/')[0] } );
          for( FM_User__c objUser : lstUsers ) {
            if( String.isNotBlank( objUser.FM_Role__c )                         && objUser.FM_Role__c.equalsIgnoreCase( 'Manager' ) ) {
              objFMCase.FM_Manager_Email__c = objUser.FM_User__r.Email;
            }
          }
        }

        //Populate customer's email on case.
        if( String.isNotBlank( objFMCase.Account__c ) ) {
          Account objAcc = [ SELECT PersonEmail
                                    , Email__pc
                            FROM Account
                            WHERE Id = :objFMCase.Account__c  ];
          if( objAcc != NULL && ( String.isNotBlank( objAcc.PersonEmail ) ||
                      String.isNotBlank( objAcc.Email__pc ) ) ) {

            objFMCase.Email__c = String.isNotBlank( objAcc.PersonEmail ) ?  objAcc.PersonEmail :
                       String.isNotBlank( objAcc.Email__pc ) ? objAcc.Email__pc : '';

          }
        }
            System.debug('objFMCase before upsert: '+objFMCase);
            upsert objFMCase ;
            updateAdditionalDetails();
            PageReference objPage = new PageReference('/' + objFMCase.Id );
            return objPage ;
        }
        return NULL;
    }

    public PageReference submitSr() {
       PageReference nextPage = submitFmCase();
        if (nextPage != NULL) {
            String unitId = nextPage.getUrl().substringAfterLast('/');
            nextPage = new PageReference(ApexPages.currentPage().getUrl());
            nextPage.getParameters().clear();
            nextPage.getParameters().put('view', 'CaseDetails');
            nextPage.getParameters().put('id', unitId);
            nextPage.setRedirect(true);
        }
        System.debug('nextPage = ' + nextPage);
        System.debug('nextPage Url = ' + nextPage.getUrl());
        return nextPage;
    }
}