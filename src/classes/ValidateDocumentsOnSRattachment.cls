/*
Created By : Swapnil Gholap
Use : Call from process builder
*/

public class ValidateDocumentsOnSRattachment {
    public static map<ID,ID> mapManagerID ;
    public static map<ID,String> mapManagerRole;
    public static integer intCounter = 0;

    @InvocableMethod
    public static void ValidateDocument(List<SR_Attachments__c> lstSRAttachment){
        System.debug('--ValidateDocument lstSRAttachment---'+lstSRAttachment);
        boolean blnCallApproval = false;
        for(SR_Attachments__c objSR : lstSRAttachment){
            System.debug('---Sr-'+objSR.id+'---'+objSR.isValid__c+'---objSr.Case__c '+objSr.Case__c);
        }
        if(lstSRAttachment.Size()>0)
        {
            lstSRAttachment = [Select id,name,isValid__c,Case__c from SR_Attachments__c
                              where Case__c = :
                              lstSRAttachment[0].Case__c];

            System.debug('---lstSRAttachment--'+lstSRAttachment);
            boolean blnFlag = true;
            for(SR_Attachments__c objSR : lstSRAttachment){
                if(objSR.isValid__c == false){
                    blnFlag = false;
                }
            }

            if(blnFlag){
                blnCallApproval = true;
            }
        }
        if(blnCallApproval){

             System.debug('---blnCallApproval--'+blnCallApproval);
             Case objCase = [Select id, Submit_for_Approval__c, Active_Unit_Ids__c,
                            Document_Verified__c, RecordType.name, RecordType.DeveloperName,
                            Status, Roles_from_Rule_Engine__c,
                            Apply_New_Payment_Terms__c, Offer_Acceptance_Letter_Generated__c,
                            O_A_Signed_Copy_Uploaded__c, Approval_Status__c,ownerID
                            from Case
                            where id=: lstSRAttachment[0].Case__c];

             objCase.Document_Verified__c = true;

             // code added on 10-23-2017 by Hardik Mehta for applying new PT in IPMS on closure of Signature Verification task in SF. starts
             /*if( !objCase.Apply_New_Payment_Terms__c && objCase.Status.equalsIgnoreCase('Submitted')
                && objCase.Offer_Acceptance_Letter_Generated__c && objCase.O_A_Signed_Copy_Uploaded__c
                && objCase.Approval_Status__c.equalsIgnoreCase('approved')
               )
             {
                objCase.Apply_New_Payment_Terms__c = true;
                system.debug('apply new payment terms true');
                // call method to create payment terms in IPMS
                try{
                    AOPTServiceRequestHandler.makeCallout(objCase.Id);
                }
                catch(exception ex){
                    Error_Log__c objError = new Error_Log__c();
                     objError.Error_Details__c = 'error: '+ex.getMessage()+'-'+ex.getstackTraceString();
                     objError.Case__c = objCase.id;
                     objError.Process_Name__c = 'AOPT';
                     insert objError; 
                }

             }*/
             // code added on 10-23-2017 by Hardik Mehta for applying new PT in IPMS on closure of Signature Verification task in SF. ends

             /*if( String.isNotBlank( objCase.RecordType.name ) &&  String.isNotBlank( objCase.Status ) &&
                 objCase.RecordType.name.equalsIgnoreCase( 'Penalty_Waiver' ) && objCase.Status.equalsIgnoreCase( 'Submitted' ) ) {
                 objCase.Submit_for_Approval__c = true;
                 objCase.Approving_Authorities__c = 'VP - Operations,General Manager';
             }*/

             if((objCase.RecordType.DeveloperName.equals('Token_Refund') ||
                 objCase.RecordType.DeveloperName.equals('Customer_Refund') ||
                 objCase.RecordType.DeveloperName.equals('Fund_Transfer') ||
                 objCase.RecordType.DeveloperName.equals('Refund_Liability') ||
                 objCase.RecordType.DeveloperName.equals('Fund_Transfer_Active_Units')) &&
                 String.isBlank(objCase.Active_Unit_Ids__c)){
                 objCase.Submit_for_Approval__c = true;
                 //objCase.Approving_Authorities__c = 'VP - Operations,General Manager';
                 if(String.isNotBlank(objCase.Roles_from_Rule_Engine__c)){
                     objCase.Approving_Authorities__c = objCase.Roles_from_Rule_Engine__c;
                 }
             }             

             update objCase;

             // Sales Admin approval process for deactivating Booking Unit only for token refund process
             if(String.isNotBlank(objCase.Active_Unit_Ids__c) && objCase.RecordType.DeveloperName == 'Token_Refund' ){
                 List<Booking_Unit__c> lstBookingUnit = new List<Booking_Unit__c>();
                 lstBookingUnit = [Select id,name,Booking__c,
                                  HOS_Name__c,Manager_Name__c,Property_Consultant__c
                                  from Booking_Unit__c where id=:objCase.Active_Unit_Ids__c];
                 List<ID> lstApproverID = new List<ID>();
                 if(lstBookingUnit.Size()>0){
                     if(String.isNotBlank(lstBookingUnit[0].HOS_Name__c)){ // HOS
                         List<User> lstUser = new List<User>();
                         lstUser = [Select id,name from User where
                                    name =: lstBookingUnit[0].HOS_Name__c AND
                                    isActive = true ];
                         if(lstUser.Size()>0){
                             lstApproverID.add(lstUser[0].id);
                         }
                     }
                     else if(String.isNotBlank(lstBookingUnit[0].Manager_Name__c)){ // DOS
                         List<User> lstUser = new List<User>();
                         lstUser = [Select id,name from User where
                                    name =: lstBookingUnit[0].Manager_Name__c AND
                                    isActive = true ];
                         if(lstUser.Size()>0){
                             lstApproverID.add(lstUser[0].id);
                         }
                     }
                     else{ // HOD
                         initialiseUserMap();
                         ID userID =  getUserID(objCase.ownerID,'HOD' );
                         system.debug('---userID----'+userID) ;
                         if(userID != null){
                             lstApproverID.add(userID);
                         }
                     }

                     /*if(lstBookingUnit[0].Booking__r.Deal_SR__c != null){
                         List<Deal_Team__c> lstDealTeam = new List<Deal_Team__c>();
                         lstDealTeam = [Select id,name,Associated_DOS__c,Associated_HOS__c,Associated_DOS__r.isActive,Associated_HOS__r.isActive
                                       from Deal_Team__c where Associated_Deal__c =: lstBookingUnit[0].Booking__r.Deal_SR__c
                                       limit 1];
                         System.debug('---lstDealTeam--'+lstDealTeam.Size()+'---'+lstDealTeam);

                         if(lstDealTeam != null && lstDealTeam.Size() > 0) {
                             if(lstDealTeam[0].Associated_DOS__c != null && lstDealTeam[0].Associated_DOS__r.isActive){
                                 lstApproverID.add(lstDealTeam[0].Associated_DOS__c);
                             }
                             else if(lstDealTeam[0].Associated_HOS__c != null && lstDealTeam[0].Associated_HOS__r.isActive){
                                 lstApproverID.add(lstDealTeam[0].Associated_HOS__c);
                             }
                             else{
                                 initialiseUserMap();
                                 ID userID =  getUserID(objCase.ownerID,'HOD' );
                                 system.debug('---userID----'+userID) ;
                                 if(userID != null){
                                     lstApproverID.add(userID);
                                 }
                             }
                         }
                         else {
                             initialiseUserMap();
                             ID userID =  getUserID(objCase.ownerID,'HOD' );
                             system.debug('---userID----'+userID) ;
                             if(userID != null){
                                 lstApproverID.add(userID);
                             }

                        }
                 }*/


                 System.debug('---lstApproverID---'+lstApproverID);
                 if(lstApproverID.Size()>0) {
                     System.debug('---ApprovalProcess--Deactivate_Booking_Unit_Approval---');
                     Approval.ProcessSubmitRequest objApprovalSubmitRequest =   new Approval.ProcessSubmitRequest();
                     objApprovalSubmitRequest.setComments('Automatic submit.');
                     objApprovalSubmitRequest.setObjectId(objCase.id);
                     //objApprovalSubmitRequest.setNextApproverIds(lstUser[0].id);
                     objApprovalSubmitRequest.setNextApproverIds(lstApproverID);
                     objApprovalSubmitRequest.setProcessDefinitionNameOrId('Deactivate_Booking_Unit_Approval');
                     objApprovalSubmitRequest.setSkipEntryCriteria(true);
                     // Submit the approval request for the Case
                     Approval.ProcessResult result = Approval.process(objApprovalSubmitRequest);
                 }
                 else{
                     Error_Log__c objError = new Error_Log__c();
                     objError.Error_Details__c = 'error: REQUIRED_FIELD_MISSING, : [nextApproverIds], Message = No user ID present for DOS, HOS and CRE_Head';
                     objError.Case__c = objCase.id;
                     objError.booking_unit__c = objCase.Active_Unit_Ids__c;
                     insert objError;
                 }
             }
         }

      }

   }


   public static void initialiseUserMap(){
       list<User> lstActiveUser = new list<User>();
       mapManagerID = new map<ID,ID>();
       mapManagerRole = new map<ID,String>();
       lstActiveUser = [Select Id,Name,isActive,Managerid,UserType,UserRole.Name,UserRoleID
                       from User
                       where isActive = true];

       for(User objUser : lstActiveUser){
           if(objUser.managerid != null){
               mapManagerID.put(objUser.id,objUser.managerid);
           }
           if(objUser.UserRoleID != null){
               mapManagerRole.put(objUser.id,objUser.UserRole.Name);
           }
       }
       system.debug('--mapManagerID Validate Doc--'+mapManagerID.Size());
       system.debug('--mapManagerRole Validate Doc--'+mapManagerRole.Size());
   }

    public static ID getUserID(ID strUserID,string strRoleName ){
        try{
            ID strManagerID = null;
            intCounter++;
            system.debug('--intCounter--'+intCounter);
            if(mapManagerID.containsKey(strUserID)){
                strManagerID = mapManagerID.get(strUserID);
                if(mapManagerRole.containsKey(strManagerID)){
                    //if(strRoleName == mapManagerRole.get(strManagerID))
                    System.debug('--mapManagerRole Name--'+mapManagerRole.get(strManagerID));
                    if(mapManagerRole.get(strManagerID).contains(strRoleName)){
                        return strManagerID;
                    }
                    else{
                        system.debug('-- again getUserID(strManagerID,strRoleName)--'+strManagerID+' , '+strRoleName);
                        ID strMGRID = null;
                        strMGRID = getUserID(strManagerID, strRoleName);
                        if(strMGRID != null){
                            return strMGRID;
                        }
                    }
                }
                else{
                    return null;
                }
                return null;
            }
            else{
                return strManagerID;
            }
            return null;
        }
        catch(exception ex){
            System.debug('--exception.getNumber--'+ex.getLineNumber()+'---'+ex.getMessage());
            return null;
        }
     }

}