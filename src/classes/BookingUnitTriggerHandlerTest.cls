@isTest
public class BookingUnitTriggerHandlerTest {
    private static final Id DEAL_SR_RECORD_TYPE_ID = DamacUtility.getRecordTypeId('NSIBPM__Service_Request__c', 'Deal');
    @testSetup static void setupData() {
        
        NSIBPM__SR_Template__c srTemplate = InitializeSRDataTest.createSRTemplate('Deal');
        insert srTemplate;
        
        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);
        sr.ID_Type__c = null;
        sr.Agency_Type__c = 'Corporate';
        sr.agency__c=null;
        sr.NSIBPM__SR_Template__c = srTemplate.id;
        insert sr;
        
        NSIBPM__Step_Template__c stpTemplate =  InitializeSRDataTest.createStepTemplate('Deal','MANAGER_APPROVAL');
        insert stptemplate;
        
        List<string> statuses = new list<string>{'UNDER_MANAGER_REVIEW'};
            Map<string,NSIBPM__Status__c> stepStatuses = InitializeSRDataTest.createStepStatus(statuses);
        
        
        NSIBPM__Step__c stp = InitializeSRDataTest.createStep(sr.id,stepStatuses.values()[0].id,stptemplate.id);
        insert stp;
        
        List<Booking__c> lstbk = new List<Booking__c>();
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        insert lstbk;
        
        
        Location__c loc = InitializeSRDataTest.createLocation('123','Building');
        insert loc;       
        
        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Unit_Location__c = loc.id;
        insert lstInv;
        
        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = lstbk[0].id;
        bu.Payment_Method__c = 'Cash';
        bu.Primary_Buyer_s_Email__c = 'raviteja@nsiglobal.com';
        bu.Primary_Buyer_s_Name__c = 'testNSI';
        bu.Primary_Buyer_s_Nationality__c = 'Russia';
        bu.Inventory__c = lstinv[0].id;
        insert bu;
        
        update bu;
        //
        bu.DP_Overdue__c = true;
        bu.Registration_Status_Code__c = 'abc';
        bu.tBits_Ready__c = true;
        bu.Receipt_ID__c = '67878';
        update bu;
        
        NSIBPM__SR_Doc__c srdoc = new NSIBPM__SR_Doc__c();
        srdoc.name= 'ppt';
        srdoc.Booking_Unit__c  = bu.id;
        srdoc.NSIBPM__Service_Request__c = sr.id;
        insert srdoc;
    }
    
    @isTest static void test_method_3() {
        Test.startTest();
        NSIBPM__Service_Request__c sr = [select id,name from NSIBPM__Service_Request__c limit 1];
        List<NSIBPM__Step_Template__c> lststtempl = new List<NSIBPM__Step_Template__c>();
        NSIBPM__Step_Template__c sttempl1 = new NSIBPM__Step_Template__c();
        sttempl1.NSIBPM__Step_RecordType_API_Name__c = 'Deal';
        sttempl1.NSIBPM__Code__c = 'DP_OK';
        lststtempl.add(sttempl1);
        NSIBPM__Step_Template__c sttempl2 = new NSIBPM__Step_Template__c();
        sttempl2.NSIBPM__Step_RecordType_API_Name__c = 'Deal';
        sttempl2.NSIBPM__Code__c = 'TOKEN_PAYMENT';
        lststtempl.add(sttempl2);
        NSIBPM__Step_Template__c sttempl3 = new NSIBPM__Step_Template__c();
        sttempl3.NSIBPM__Step_RecordType_API_Name__c = 'Deal';
        sttempl3.NSIBPM__Code__c = 'SPA_EXECUTION';
        lststtempl.add(sttempl3);
        insert lststtempl;
        
        List<NSIBPM__Status__c> lststpSt = new List<NSIBPM__Status__c>();
        NSIBPM__Status__c stpSt = new NSIBPM__Status__c();
        stpSt.NSIBPM__Code__c = 'DP_OK';
        lststpSt.add(stpst);
        NSIBPM__Status__c stpSt1 = new NSIBPM__Status__c();
        stpSt1.NSIBPM__Code__c = 'TOKEN_DEPOSIT_PAID';
        lststpSt.add(stpst1);
        NSIBPM__Status__c stpSt2 = new NSIBPM__Status__c();
        stpSt2.NSIBPM__Code__c = 'SPA_EXECUTED';
        lststpSt.add(stpSt2);
        NSIBPM__Status__c stpSt3 = new NSIBPM__Status__c();
        stpSt3.NSIBPM__Code__c = 'Test';
        lststpSt.add(stpSt3);
        insert lststpSt;
        
        List<NSIBPM__Step__c> lststp = new List<NSIBPM__Step__c>();
        NSIBPM__Step__c stp1 = new NSIBPM__Step__c();
        stp1.NSIBPM__SR__c = sr.id;
        stp1.NSIBPM__Status__c = stpSt3.id;
        stp1.NSIBPM__Step_Template__c = sttempl1.id;
        lststp.add(stp1);
        NSIBPM__Step__c stp2 = new NSIBPM__Step__c();
        stp2.NSIBPM__SR__c = sr.id;
        stp2.NSIBPM__Status__c = stpSt3.id;
        stp2.NSIBPM__Step_Template__c = sttempl2.id;
        lststp.add(stp2);
        NSIBPM__Step__c stp3 = new NSIBPM__Step__c();
        stp3.NSIBPM__SR__c = sr.id;
        stp3.NSIBPM__Status__c = stpSt3.id;
        stp3.NSIBPM__Step_Template__c = sttempl3.id;
        lststp.add(stp3);
        insert lststp;
        
        
        NSIBPM__SR_Status__c srstatus = new NSIBPM__SR_Status__c();
        srstatus.NSIBPM__Code__c = 'TOKEN_DEPOSIT_PAID';
        insert srstatus;
        
        List<ID> lstBUIDs = new List<Id>();
        for(Booking_Unit__c bu : [select id from Booking_Unit__c]){
            lstBUIDs.add(bu.id);
        }
        BookingUnitTriggerHandler.UpdateSR(lstBUIDs);
        
        
        //create new step
        New_Step__c nstp1 = new New_Step__c();
        nstp1.Service_Request__c = sr.id;
        nstp1.Step_No__c = 2;
        nstp1.Step_Status__c = 'Awaiting Token Deposit';
        nstp1.Step_Type__c = 'Token Payment';
        insert nstp1;
        
        sr.DP_ok__c = true;
        sr.Token_Deposit_Paid_Date_Time__c = system.Datetime.now();        
        update sr;
        BookingUnitTriggerHandler.mpSRRegStatusCodeLE.put(sr.id,true);
        set<id> srids = new set<id>();
        srids.add(sr.id);
        BookingUnitTriggerHandler.UpdateStepStatus(srids);
        
        Test.stopTest();
    }
    
    @isTest static void test_method_4() {
        Test.startTest();
        NSIBPM__Service_Request__c sr = [select id,name from NSIBPM__Service_Request__c limit 1];
        List<NSIBPM__Step_Template__c> lststtempl = new List<NSIBPM__Step_Template__c>();
        NSIBPM__Step_Template__c sttempl1 = new NSIBPM__Step_Template__c();
        sttempl1.NSIBPM__Step_RecordType_API_Name__c = 'Deal';
        sttempl1.NSIBPM__Code__c = 'DP_OK';
        lststtempl.add(sttempl1);
        NSIBPM__Step_Template__c sttempl2 = new NSIBPM__Step_Template__c();
        sttempl2.NSIBPM__Step_RecordType_API_Name__c = 'Deal';
        sttempl2.NSIBPM__Code__c = 'TOKEN_PAYMENT';
        lststtempl.add(sttempl2);
        NSIBPM__Step_Template__c sttempl3 = new NSIBPM__Step_Template__c();
        sttempl3.NSIBPM__Step_RecordType_API_Name__c = 'Deal';
        sttempl3.NSIBPM__Code__c = 'SPA_EXECUTION';
        lststtempl.add(sttempl3);
        insert lststtempl;
        
        List<NSIBPM__Status__c> lststpSt = new List<NSIBPM__Status__c>();
        NSIBPM__Status__c stpSt = new NSIBPM__Status__c();
        stpSt.NSIBPM__Code__c = 'DP_OK';
        lststpSt.add(stpst);
        NSIBPM__Status__c stpSt1 = new NSIBPM__Status__c();
        stpSt1.NSIBPM__Code__c = 'TOKEN_DEPOSIT_PAID';
        lststpSt.add(stpst1);
        NSIBPM__Status__c stpSt2 = new NSIBPM__Status__c();
        stpSt2.NSIBPM__Code__c = 'SPA_EXECUTED';
        lststpSt.add(stpSt2);
        NSIBPM__Status__c stpSt3 = new NSIBPM__Status__c();
        stpSt3.NSIBPM__Code__c = 'Test';
        lststpSt.add(stpSt3);
        insert lststpSt;
        
        List<NSIBPM__Step__c> lststp = new List<NSIBPM__Step__c>();
        NSIBPM__Step__c stp1 = new NSIBPM__Step__c();
        stp1.NSIBPM__SR__c = sr.id;
        stp1.NSIBPM__Status__c = stpSt3.id;
        stp1.NSIBPM__Step_Template__c = sttempl1.id;
        lststp.add(stp1);
        NSIBPM__Step__c stp2 = new NSIBPM__Step__c();
        stp2.NSIBPM__SR__c = sr.id;
        stp2.NSIBPM__Status__c = stpSt3.id;
        stp2.NSIBPM__Step_Template__c = sttempl2.id;
        lststp.add(stp2);
        NSIBPM__Step__c stp3 = new NSIBPM__Step__c();
        stp3.NSIBPM__SR__c = sr.id;
        stp3.NSIBPM__Status__c = stpSt3.id;
        stp3.NSIBPM__Step_Template__c = sttempl3.id;
        lststp.add(stp3);
        insert lststp;
        
        
        NSIBPM__SR_Status__c srstatus = new NSIBPM__SR_Status__c();
        srstatus.NSIBPM__Code__c = 'TOKEN_DEPOSIT_PAID';
        insert srstatus;
        
        List<ID> lstBUIDs = new List<Id>();
        for(Booking_Unit__c bu : [select id from Booking_Unit__c]){
            lstBUIDs.add(bu.id);
        }
        BookingUnitTriggerHandler.UpdateSR(lstBUIDs);
        
        
        //create new step
        New_Step__c nstp2 = new New_Step__c();
        nstp2.Service_Request__c = sr.id;
        nstp2.Step_No__c = 4;
        nstp2.Step_Status__c = 'Awaiting DP';
        nstp2.Step_Type__c = 'DP_OK';
        insert nstp2;
        
        sr.DP_ok__c = true;
        sr.Token_Deposit_Paid_Date_Time__c = system.Datetime.now();        
        update sr;
        BookingUnitTriggerHandler.mpSRRegStatusCodeLE.put(sr.id,true);
        set<id> srids = new set<id>();
        srids.add(sr.id);
        BookingUnitTriggerHandler.UpdateStepStatus(srids);
        
        Test.stopTest();
    }
    
    @isTest static void test_method_5() {
        Test.startTest();
        NSIBPM__Service_Request__c sr = [select id,name from NSIBPM__Service_Request__c limit 1];
        List<NSIBPM__Step_Template__c> lststtempl = new List<NSIBPM__Step_Template__c>();
        NSIBPM__Step_Template__c sttempl1 = new NSIBPM__Step_Template__c();
        sttempl1.NSIBPM__Step_RecordType_API_Name__c = 'Deal';
        sttempl1.NSIBPM__Code__c = 'DP_OK';
        lststtempl.add(sttempl1);
        NSIBPM__Step_Template__c sttempl2 = new NSIBPM__Step_Template__c();
        sttempl2.NSIBPM__Step_RecordType_API_Name__c = 'Deal';
        sttempl2.NSIBPM__Code__c = 'TOKEN_PAYMENT';
        lststtempl.add(sttempl2);
        NSIBPM__Step_Template__c sttempl3 = new NSIBPM__Step_Template__c();
        sttempl3.NSIBPM__Step_RecordType_API_Name__c = 'Deal';
        sttempl3.NSIBPM__Code__c = 'SPA_EXECUTION';
        lststtempl.add(sttempl3);
        insert lststtempl;
        
        List<NSIBPM__Status__c> lststpSt = new List<NSIBPM__Status__c>();
        NSIBPM__Status__c stpSt = new NSIBPM__Status__c();
        stpSt.NSIBPM__Code__c = 'DP_OK';
        lststpSt.add(stpst);
        NSIBPM__Status__c stpSt1 = new NSIBPM__Status__c();
        stpSt1.NSIBPM__Code__c = 'TOKEN_DEPOSIT_PAID';
        lststpSt.add(stpst1);
        NSIBPM__Status__c stpSt2 = new NSIBPM__Status__c();
        stpSt2.NSIBPM__Code__c = 'SPA_EXECUTED';
        lststpSt.add(stpSt2);
        NSIBPM__Status__c stpSt3 = new NSIBPM__Status__c();
        stpSt3.NSIBPM__Code__c = 'Test';
        lststpSt.add(stpSt3);
        insert lststpSt;
        
        List<NSIBPM__Step__c> lststp = new List<NSIBPM__Step__c>();
        NSIBPM__Step__c stp1 = new NSIBPM__Step__c();
        stp1.NSIBPM__SR__c = sr.id;
        stp1.NSIBPM__Status__c = stpSt3.id;
        stp1.NSIBPM__Step_Template__c = sttempl1.id;
        lststp.add(stp1);
        NSIBPM__Step__c stp2 = new NSIBPM__Step__c();
        stp2.NSIBPM__SR__c = sr.id;
        stp2.NSIBPM__Status__c = stpSt3.id;
        stp2.NSIBPM__Step_Template__c = sttempl2.id;
        lststp.add(stp2);
        NSIBPM__Step__c stp3 = new NSIBPM__Step__c();
        stp3.NSIBPM__SR__c = sr.id;
        stp3.NSIBPM__Status__c = stpSt3.id;
        stp3.NSIBPM__Step_Template__c = sttempl3.id;
        lststp.add(stp3);
        insert lststp;
        
        
        NSIBPM__SR_Status__c srstatus = new NSIBPM__SR_Status__c();
        srstatus.NSIBPM__Code__c = 'TOKEN_DEPOSIT_PAID';
        insert srstatus;
        
        List<ID> lstBUIDs = new List<Id>();
        for(Booking_Unit__c bu : [select id from Booking_Unit__c]){
            lstBUIDs.add(bu.id);
        }
        BookingUnitTriggerHandler.UpdateSR(lstBUIDs);
        
        
        //create new step
        New_Step__c nstp3 = new New_Step__c();
        nstp3.Service_Request__c = sr.id;
        nstp3.Step_No__c = 50;
        nstp3.Step_Status__c = 'Awaiting SPA Execution';
        nstp3.Step_Type__c = 'SPA Execution';
        insert nstp3;
        
        sr.DP_ok__c = true;
        sr.Token_Deposit_Paid_Date_Time__c = system.Datetime.now();        
        update sr;
        BookingUnitTriggerHandler.mpSRRegStatusCodeLE.put(sr.id,true);
        set<id> srids = new set<id>();
        srids.add(sr.id);
        BookingUnitTriggerHandler.UpdateStepStatus(srids);
        
        Test.stopTest();
    }
    
    static testmethod void testUpdateCampaignTotal(){
        List<NSIBPM__SR_Status__c> createdSRStatus = TestDataFactory.createSrStatusRecords(new List<NSIBPM__SR_Status__c>{new NSIBPM__SR_Status__c()});
        system.debug(createdSRStatus);
        List<NSIBPM__SR_Template__c> createdSRTemplates = TestDataFactory.createTestTemplateRecords(new List<NSIBPM__SR_Template__c>{
            new NSIBPM__SR_Template__c(Name='Deal', NSIBPM__SR_RecordType_API_Name__c = 'Deal')});
        Test.startTest();
        List<NSIBPM__Service_Request__c> createdSRs = TestDataFactory.createTestServiceRequestRecords(new List<NSIBPM__Service_Request__c>{
            new NSIBPM__Service_Request__c(OwnerId = UserInfo.getUserId(), 
                                            recordTypeId = DEAL_SR_RECORD_TYPE_ID, NSIBPM__Internal_SR_Status__c = createdSRStatus[0].Id, 
                                           NSIBPM__SR_Template__c = createdSRTemplates[0].Id, Registration_Date__c = Date.newInstance(2017,5,25))});
        
        List<Booking__c> bList = TestDataFactory.createBookingRecords(new List<Booking__c> {new Booking__c(Deal_SR__c = createdSRs[0].Id)});
        Id salesEventsRt = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Sales Events').getRecordTypeId();
        
        List<Campaign__c> campaignList = TestDataFactory.createCampaignRecords(new List<Campaign__c>{
                    new Campaign__c(RecordTypeId=salesEventsRt), new Campaign__c(RecordTypeId=salesEventsRt),
                    new Campaign__c(RecordTypeId=salesEventsRt), new Campaign__c(RecordTypeId=salesEventsRt), 
                    new Campaign__c(RecordTypeId=salesEventsRt)});
        
        campaignList[0].Parent_Campaign__c = campaignList[1].Id;
        campaignList[0].Campaign_Sales__c = 2;
        campaignList[1].Campaign_Sales__c = 2;
        campaignList[2].Campaign_Sales__c = 2;
        campaignList[1].Parent_Campaign__c = campaignList[2].Id;
        campaignList[2].Parent_Campaign__c = campaignList[3].Id;
        campaignList[3].Parent_Campaign__c = campaignList[4].Id;
        update campaignList;
        
        List<Booking_Unit__c> buList = TestDataFactory.createBookingUnitRecords(new List<Booking_Unit__c> {
            new Booking_Unit__c(Booking__c = bList[0].Id, Related_Campaign__c = campaignList[0].Id),
                new Booking_Unit__c(Booking__c = bList[0].Id, Related_Campaign__c = campaignList[0].Id,Status__c='New')
                });
        
        buList[0].DP_Overdue__c = true;
        update buList;
        
        Booking_Unit__c bb= new Booking_Unit__c(Booking__c = bList[0].Id, Related_Campaign__c = campaignList[0].Id,
                                    Status__c='New',Requested_Price_AED__c=100);
        insert bb;
        test.stopTest(); 
        map<id,Booking_Unit__c> newmap=new map<id,Booking_Unit__c>([select id,Status__c,Related_Campaign__c from Booking_Unit__c]);
        BookingUnitTriggerHandler.updateCampaignTotal(newmap);
        BookingUnitTriggerHandler.updateParentCampaignSales(newmap);
        
    }
    
        @isTest
    static void testMethod1(){
        Account objAccount = new Account();
        objAccount.LastName = 'test';
        objAccount.Email__c = 'success@mailinator.com';
        objAccount.Party_ID__c = '1234';
        insert objAccount;
        
        Case objCase = new Case();
        insert objCase;
        
        Location__c objLocation = new Location__c();
        objLocation.Name = 'test';
        objLocation.Location_ID__c = '12345';
        insert objLocation;
        
        Inventory__c objInventory = new Inventory__c();
        objInventory.Building__c = objLocation.Id;
        insert objInventory;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        objBooking.Account__c = objAccount.Id; 
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Registration_ID__c = '3901';
        objBU.Unit_Name__c = 'test/test';
        objBU.Booking__c = objBooking.Id;
        objBU.Recovery__c = 'A';
        objBU.Inventory__c = objInventory.Id;
        objBU.Permitted_Use__c = 'Hospitality Usage';       
        System.Test.startTest();
            insert objBU;
        System.Test.stopTest();
    }
    
}