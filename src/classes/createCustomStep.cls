/******************************************************************************************************************
* Name               : createCustomStep
* Description        : This is the custom code class for creating Step For SR         
* Created Date       : 05/12/2017                                                                 
* Created By         : Alok - DAMAC                                                       
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR          DATE                                                              
* 1.0         Alok -DAMAC    05/December/2017                                                     
*******************************************************************************************************************/
public class createCustomStep{
@invocablemethod
public static void createStep(List<ID> ids){ 
    Id srId;
    Id bUnitId;
    String buRegId;
    String updType= 'INSERT';
    NSIBPM__SR_Template__c template = new NSIBPM__SR_Template__c();
    template = [select id,(select id,NSIBPM__Start_Status__c,NSIBPM__Step_Template__c,NSIBPM__SR_Steps__c.OwnerId,NSIBPM__Step_No__c,NSIBPM__SR_Template__r.Name from  NSIBPM__SR_Steps_SR_Template__r where NSIBPM__Step_Template_Code__c='AWAITING_MORE_DOCS'and
    NSIBPM__Start_Status__c!=null and 
    NSIBPM__Step_Template__c!=null and 
    ownerid != null and 
    NSIBPM__Step_No__c!=null LIMIT 1 ) from NSIBPM__SR_Template__c where Name='Deal'limit 1];
    
     if (!ids.isEmpty()){
     List<NSIBPM__Service_Request__c> srSF= [Select Id from NSIBPM__Service_Request__c where id in: ids];
    //List<Booking_Unit__c> srSF= [Select SR_Id__c,Registration_ID__c from Booking_Unit__c where id in: ids];
    if (srSF.Size()>0){
    srId=srSF[0].Id; 
    }
    }
    system.debug(template.NSIBPM__SR_Steps_SR_Template__r +'>>>');
    
    List<NSIBPM__Step__c> srStep = [Select Id from NSIBPM__Step__c where NSIBPM__SR__c=:srId AND Step_Template_Code__c='AWAITING_MORE_DOCS' AND NSIBPM__Step_Status__c='Awaiting More Docs'];
    if(srStep.Size()>0){
    updType='UPDATE';
    }
    if(template.NSIBPM__SR_Steps_SR_Template__r[0] != null && srId !=null && updType=='INSERT'){         
    NSIBPM__Step__c newStep = new NSIBPM__Step__c();
    newStep.NSIBPM__SR__c = srId;
    newStep.NSIBPM__SR_Step__c = template.NSIBPM__SR_Steps_SR_Template__r[0].id;
    newStep.NSIBPM__Status__c =  template.NSIBPM__SR_Steps_SR_Template__r[0].NSIBPM__Start_Status__c;
    newStep.NSIBPM__Step_Template__c = template.NSIBPM__SR_Steps_SR_Template__r[0].NSIBPM__Step_Template__c;
    newStep.OwnerId = template.NSIBPM__SR_Steps_SR_Template__r[0].ownerid;
    newStep.NSIBPM__Step_No__c  = template.NSIBPM__SR_Steps_SR_Template__r[0].NSIBPM__Step_No__c;
    newStep.NSIBPM__Sys_Step_Loop_No__c= template.NSIBPM__SR_Steps_SR_Template__r[0].NSIBPM__Step_No__c+'_1';
    insert newStep;
    }

}
}