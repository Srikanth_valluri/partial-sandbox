@isTest
public class SaveScheduleAppointmentTest {
  public  testmethod static void method()
    {
        SaveScheduleAppointment nation=new SaveScheduleAppointment();
        SaveScheduleAppointment.AppointmentWrapper n=new SaveScheduleAppointment.AppointmentWrapper(); 
        Account a=new Account();
        a.Name='test';
     
        insert a;
        NSIBPM__Service_Request__c cs=new NSIBPM__Service_Request__c();
        insert cs;
         Booking__c bo=new Booking__c();
        bo.Account__c=a.id;
        bo.Deal_SR__c=cs.id;
        insert bo;
        
        Booking_Unit__c b=new Booking_Unit__c();
        b.Booking__c=bo.id;
        b.Unit_Name__c='BSB/14/1403';
        insert b;
        Location__c l=new Location__c ();
        l.Name='BSB';
        l.Location_ID__c='BSB';
        insert l;
        n.AccountID=a.id;
        n.RecordType='Appointment Scheduling';
        n.BookingUnit='BSB/14/1403';
        n.SubProcessName='Documentation';
        n.TimeSlot='10:00 - 11:00';
        n.AppointmentDate=Date.valueOf('2018-06-26');
        TriggerOnOffCustomSetting__c check=new TriggerOnOffCustomSetting__c();
        check.name='CallingListTrigger';
        check.OnOffCheck__c=true;
        insert check;
        
       SaveScheduleAppointment.ScheduleAppointment(n);
       
    }
}