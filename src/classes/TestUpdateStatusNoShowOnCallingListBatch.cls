/**
 * @File Name          : TestUpdateStatusNoShowOnCallingListBatch.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 11/14/2019, 4:44:53 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    11/10/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
private class TestUpdateStatusNoShowOnCallingListBatch{

	@isTest static void testMethod1() {
		Account objAccount = TestDataFactoryFM.createAccount();
        objAccount.Email__c = 'test@test.com'; 
        insert objAccount;
        System.debug('-->> objAccount ## : ' + objAccount );

		NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAccount);
        insert objSR;

        Booking__c objBooking = TestDataFactoryFM.createBooking(objAccount, objSR);
        insert objBooking;
        
        Booking_Unit__c objBooking_Unit = TestDataFactoryFM.createBookingUnit(objAccount, objBooking);
		objBooking_Unit.Snags_Completed__c = False; 
		objBooking_Unit.Snags_Reported__c = False;
        insert objBooking_Unit;

        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
        OnOffCheck__c = true);

        settingLst2.add(newSetting1);
        insert settingLst2;

        Id recordTypeIdWlc = [SELECT Id
            FROM RecordType
            WHERE SObjectType='Calling_List__c' 
            AND DeveloperName='Appointment_Scheduling'
            AND IsActive = TRUE LIMIT 1].Id;

        Calling_List__c objCalling_List = new Calling_List__c(
            RecordTypeId = recordTypeIdWlc,
            Unit_Name__c = 'Test Unit',
            IsHideFromUI__c = false,
            Customer_Name__c = 'Test Customer',
            Registration_ID__c = 'test',
            Calling_List_Status__c = 'New',
            Account__c = objAccount.Id,
			Booking_Unit__c = objBooking_Unit.Id,
			Service_Type1__c = 'Handover',
			Sub_Purpose__c = 'Unit Viewing',
			Appointment_Status__c = 'Confirmed',
            Appointment_Date__c = System.Today()
        );
        insert objCalling_List;
        System.debug('-->> objTask : ' + objCalling_List );
		Test.startTest();
			UpdateStatusNoShowOnCallingListBatch objUpdateStatusNoShowOnCallingListBatch = new UpdateStatusNoShowOnCallingListBatch();
			Database.executeBatch(objUpdateStatusNoShowOnCallingListBatch, 1);
		Test.stopTest();

	}

}