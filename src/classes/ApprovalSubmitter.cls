/****************************************************************************************************
 * Description - Generic class to submit record for Approval
 *---------------------------------------------------------------------------------------------------
 * Version        Date          Author                  Description
 * 1.0            26-03-2020    Aishwarya Todkar        Initial Draft            
 ****************************************************************************************************/
public class ApprovalSubmitter {

/***********************************************************************************************
Description : method to get called from process builder 'Overdue Rebate/Discount SR'
Parameter(s): List of Strings of Approval Details
Return Type : void
************************************************************************************************/
    @InvocableMethod
    public static void submitForApproval( List<String> listApprovalDetails ) {
        Id recordId; 
        String processNameOrId; 
        Id approverId;

        if(  !listApprovalDetails.isEmpty() ) {

            //Format: recordId-ApprovalProcessName-ApproverId
            //Eg.: 5002500000Bi4p1-testProcess-00525000005ebIA
            
            String approvalDetails = listApprovalDetails[0];
            List<String> listApprovalReq = new List<String>();
            listApprovalReq.addAll( approvalDetails.split('-') );
            if( listApprovalReq.size() == 3) {
                recordId = Id.valueOf( listApprovalReq[0] );
                processNameOrId = listApprovalReq[1];
                approverId = Id.valueOf( listApprovalReq[2] );
                submitApprovalReq( recordId, processNameOrId, approverId);
            }
        }
    }

/***********************************************************************************************
Description : Method to submit record for approval 
Parameter(s): Record Id, Approval Process Name / Id, Approver Id
Return Type : void
************************************************************************************************/
    public static void submitApprovalReq( Id recordId, String processNameOrId, Id approverId ) {
        system.debug( 'recordId -- ' + recordId);
        system.debug( 'processNameOrId --' + processNameOrId);
        system.debug( 'approverId--' + approverId);

        if( recordId != null && processNameOrId != null && approverId != null ) {
            Approval.ProcessSubmitRequest req1  = new Approval.ProcessSubmitRequest();
            req1.setObjectId( recordId );
            req1.setNextApproverIds( new Id[] { approverId } );
            req1.setProcessDefinitionNameOrId( processNameOrId );
            req1.setSkipEntryCriteria(true);
            Approval.ProcessResult result;
            if( !Test.isRunningTest() )
                result = Approval.process(req1);
            system.debug('result -- ' + result);
        }
    }
}