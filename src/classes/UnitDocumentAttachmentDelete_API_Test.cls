/************************************************************************************************
 * @Name              : UnitDocumentAttachmentDelete_API_Test
 * @Description       : Test Class for DAMAC_UnitDocumentAttachmentDetails_API
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         03/08/2020       Created
***********************************************************************************************/
@isTest
public class UnitDocumentAttachmentDelete_API_Test{

    @isTest
    static void testAPI(){
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.Mode_of_Payment__c = 'Cheque';
        insert sr;
        
        Booking__c booking = new Booking__c();
        booking.Deal_SR__c = sr.Id;
        insert booking;
        
        Buyer__c buyer = new Buyer__c();
        buyer.Booking__c = booking.Id;
        buyer.First_Name__c = 'John';
        buyer.Last_Name__c = 'Wayne';
        insert buyer;
        
        Unit_Documents__c unit = new Unit_Documents__c();
        unit.Buyer__c = buyer.Id;
        unit.Document_Name__c = 'John-Passport Copy';
        unit.Status__c = 'Pending Upload';
        insert unit;
        
        Blob b = Blob.valueOf('Test Data');     
        Attachment attachment = new Attachment();
        attachment.ParentId = unit.Id;
        attachment.Name = 'Test Attachment for Parent';
        attachment.Body = b;
        
        insert(attachment);
        
        String attachmentId = attachment.Id;
        String attachmentIdNull = '';
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();     
        
        DAMAC_UnitDocumentAttachmentDetails_API.deleteBuyerRequest dbr = new DAMAC_UnitDocumentAttachmentDetails_API.deleteBuyerRequest();
        dbr.attachmentIds = new list<string>{attachment.Id};
        
        req.requestUri = '/deleteAttachmentDetails';
        req.requestBody = blob.valueOf(Json.serialize(dbr));
        req.httpMethod = 'POST';
                
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_UnitDocumentAttachmentDetails_API.getResults();
        
        req.requestUri = '/deleteAttachmentDetails';
        req.requestBody = Blob.valueOf('{"attachmentId" : "'+attachmentIdNull+'"}');
        req.httpMethod = 'POST';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_UnitDocumentAttachmentDetails_API.getResults();
        
        Test.stopTest();
    }
}