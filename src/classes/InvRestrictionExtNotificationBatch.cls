/**************************************************************************************************
* Name         : InvRestrictionExtNotificationBatch
* Test Class  : 
* Description: Batch class to send email notification to Functional User 
*                regarding Inventory Restriction Extension
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR      DATE
* 1.0         QBurst      29/09/2020
**************************************************************************************************/
public class InvRestrictionExtNotificationBatch implements Database.Batchable<sObject>{

    /*********************************************************************************************
    * @Description: Implementing the start method of batch interface, contains query.
    * @Params      : Database.BatchableContext
    * @Return      : Database.QueryLocator
    *********************************************************************************************/  
    public Database.QueryLocator start(Database.BatchableContext BC){
        Integer days = Integer.ValueOf(Label.Inventory_Restriction_Extenstion_Prior_Days);
        Date extNotifyDate = system.today().addDays(days);
        system.debug('extNotifyDate: ' + extNotifyDate);
        return Database.getQueryLocator([SELECT Id, Name, Status__c, Function__c, Request_Processed_Datetime__c,
                                            Functional_User__c, Request_Date__c, Restrict_Reason__c,
                                            Inventory_Request_Filters__c, Functional_User_Comments__c,
                                            Restriction_Request_Name__c, Request_Type__c,
                                            Approved_by_Approver_User_1__c, Approval_User__c,
                                            Approved_by_Approver_User_2__c, Approval_User_2__c,
                                            Approved_by_Approver_User_3__c, Approval_User_3__c,
                                            Inventory_Restriction_Functional_Matrix__c,
                                            Functional_User__r.Email,
                                            Approver_User_1_Comments__c, Approver_User_2_Comments__c,
                                            Approver_User_3_Comments__c, Sales_Admin_Comments__c, Parent_Request__c,
                                            Restriction_End_Date__c, Restriction_Extended__c
                                         FROM Inventory_Restriction_Request__c 
                                         WHERE Restriction_End_Date__c  != NULL
                                             AND Restriction_End_Date__c <=: extNotifyDate
                                             AND Restriction_End_Date__c >= TODAY
                                             AND Restriction_Extended__c = false
                                             AND Status__c = 'Completed'
                                             AND Functional_User__c != NULL]); 
    }

    /*********************************************************************************************
    * @Description: Implementing the execute method of the batch interface, contains the criteria.
    * @Params      : Database.BatchableContext, List<sObject>
    * @Return: void
    *********************************************************************************************/
    public void execute(Database.BatchableContext BC, List<Inventory_Restriction_Request__c> scope){
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        for(Inventory_Restriction_Request__c invResReq: scope){
            List<String> toAddresses = new List<String>();
            if(invResReq.Functional_User__c != null && invResReq.Functional_User__r.Email != null){
                toAddresses.add(invResReq.Functional_User__r.Email);
            }
            if(toAddresses.size() > 0){
                Messaging.SingleEmailMessage mail = new  Messaging.SingleEmailMessage();
                Integer days = System.today().daysBetween(invResReq.Restriction_End_Date__c);
                String body = 'Hi,<br/><br/>';
                
                body += '<br/>Inventory Restriction Request #' + invResReq.Name + ' is expiring in ' + days + ' day(s).';
                
                   
                body += '<br/><br/> IF you want to extend the Restriction, click on the Link and provide the extension details: '
                             + System.URL.getSalesforceBaseUrl().toExternalForm() 
                            + '/apex/InventoryRestrictionRequest?id=' + invResReq.Id;
                String emailSubject = 'Inventory Restriction Request (' + invResReq.Name + ') Expiring Soon';
                
                mail.setToAddresses(toAddresses);
                mail.setSenderDisplayName('Damac Property ');
                mail.setReplyTo('noreply@Damacgroup.com');
                mail.setSubject(emailSubject);
                mail.setSaveAsActivity(false);
                System.debug('...body...' + body);
                mail.setHtmlBody(body);
                mails.add(mail);  
                system.debug('mail: ' + mail);
            }
        }
        Messaging.sendEmail(mails);
    }

    /*********************************************************************************************
    * @Description: Implementing the Finish method, to send an email after job completion.
    * @Params      : Database.BatchableContext
    * @Return: void
    *********************************************************************************************/
    public void finish(Database.BatchableContext BC){
    }
}// End of class.