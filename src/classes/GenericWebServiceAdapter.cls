public virtual class GenericWebServiceAdapter implements WebServiceAdapter {
    
    public Map<String,String> parameters {set;get;}
    
    public WebServiceAdapter setParam(Map<String,String> parametersParam){
        this.parameters = parametersParam;
        return this;
    }
    public virtual WebServiceAdapter call(){
        return this;
    }
    
    public virtual string getResponse() {
        return 'test';
    }
}