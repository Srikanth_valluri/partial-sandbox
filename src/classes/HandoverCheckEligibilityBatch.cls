public without sharing class HandoverCheckEligibilityBatch implements Database.Batchable<sObject>, Database.AllowsCallouts,Database.Stateful {

    String query;
    public Database.QueryLocator start(Database.BatchableContext BC) {
        List<Booking_Unit_Active_Status__c> csActiveValues = Booking_Unit_Active_Status__c.getall().values();
        List<String> lstActiveStatus = new List<String>();
        for(Booking_Unit_Active_Status__c cs : csActiveValues) {
            lstActiveStatus.add(cs.Status_Value__c);
        }
        query = 'Select Id, Registration_ID__c, Approval_Status__c, Recovery__c, JOPD_Area__c, Handover_Flag__c, Handover_Notice_Sent__c, Early_Handover__c, EHO_Notice_Sent__c, Rule_Engine_Message__c, Unit_Name__c, Registration_Status__c, Eligible_for_Handover_Notice_Date__c ';
        query += 'From Booking_Unit__c Where Registration_Status__c IN: lstActiveStatus And JOPD_Area__c != NULL And (Approval_Status__c = null Or Approval_Status__c = \''+ String.escapeSingleQuotes('Legal')+ '\'';
        query += ' Or Approval_Status__c = \''+String.escapeSingleQuotes('CC')+ '\' Or Approval_Status__c = \''+ String.escapeSingleQuotes('NO')+'\' Or Approval_Status__c = \''+String.escapeSingleQuotes('Recovery')+'\')';
        query += ' And Handover_Flag__c != \''+ String.escapeSingleQuotes('Y')+'\' And Handover_Notice_Sent__c = false And Early_Handover__c = false And EHO_Notice_Sent__c = false';
        system.debug('!!!!query'+query);
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<Booking_Unit__c> scope) {
        system.debug('!!!!!!scope'+scope);
        Id buildingRTID = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
        List<String> lstBUIds = new list<String>();
        List<Booking_Unit__c> lstBookingUnitsToUpdate = new List<Booking_Unit__c>();
        List<Task> lstTask = new List<Task>();
        list<String> lstBuildingNames = new list<String>();
        List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
        map<Id, Boolean> mapBUHO = new map<Id, Boolean>();
        Map<String, Integer> mapMajorSnags = new Map<String, Integer>();
        Map<String, Integer> mapAptSnags = new Map<String, Integer>();
        Map<Id, String> mapPreviousStatus = new Map<Id, String>();
        Map<String, Location__c> mapBuildNameRecord = new Map<String, Location__c>();
        for (Booking_Unit__c objBU : scope) {
            lstBUIds.add(objBU.id);
            mapPreviousStatus.put(objBU.Id, objBU.Approval_Status__c);
            String buildingName = objBU.Unit_Name__c.substringBefore('/');
            if (!lstBuildingNames.contains(buildingName) ) {
                lstBuildingNames.add(buildingName);
            }
        }
        
        if (lstBuildingNames != null && lstBuildingNames.size() > 0) {
            for (Location__c objLoc : [Select Id, Name, Finance_Confirmation_Complete__c, RecordTypeId
                                        From Location__c 
                                        Where Name IN: lstBuildingNames And RecordTypeId =: buildingRTID ]) {
                mapBuildNameRecord.put(objLoc.Name, objLoc);                     
            }
        }
        mapBUHO = isEHO(lstBUIds);
        system.debug('!!!!!!!!mapBUHO'+mapBUHO);        
        
        mapMajorSnags = getSnags(lstBUIds);

        mapAptSnags = percSnagged(lstBUIds);
        
        for (Booking_Unit__c bUnit: scope) {
            String strBuilding = bUnit.Unit_Name__c.substringBefore('/');
            
            if (bUnit.Recovery__c != 'R' && bUnit.Recovery__c != 'A') {            
                if (mapBuildNameRecord != null && mapBuildNameRecord.containsKey(strBuilding)
                    &&  mapBuildNameRecord.get(strBuilding) != null) {
                    Location__c objLoc = mapBuildNameRecord.get(strBuilding);
                    if (objLoc != null && objLoc.Finance_Confirmation_Complete__c == true) {
                        String majorSnags = '0';
                        if (mapMajorSnags != null && mapMajorSnags.containsKey(bUnit.Registration_ID__c)){
                            majorSnags = String.valueOf(mapMajorSnags.get(bUnit.Registration_ID__c));
                        }
            
                        String perApartSnagged = '0';
                        if (mapAptSnags != null && mapAptSnags.containsKey(bUnit.Registration_ID__c)){
                            perApartSnagged = String.valueOf(mapAptSnags.get(bUnit.Registration_ID__c) * 100);
                        }
                        
                        String strIsEHO = 'NO';
                        if (mapBUHO.containsKey(bUnit.id)) {
                            if (mapBUHO.get(bUnit.id) == false){
                                strIsEHO = 'Yes';
                            }
                        }
                        
                        CheckEligibleHandover.EligibleHandover response = CheckEligibleHandover.getEligibleHandover( String.valueOf(bUnit.Registration_ID__c),'HANDOVER','','',majorSnags ,
                                                               'Yes','Yes',perApartSnagged ,strIsEHO,'10');
                        
                         if(response != null) {
                            bUnit.Approval_Status__c = response.handoverNoticeAllowed;
                            bUnit.Rule_Engine_Message__c = response.message;
                            lstBookingUnitsToUpdate.add(bUnit);
                         }
                         if (bUnit.Eligible_for_Handover_Notice_Date__c == null && 
                            (bUnit.Approval_Status__c == 'HO' || bUnit.Approval_Status__c == 'EHO' 
                            || bUnit.Approval_Status__c == 'HO - Approved' || bUnit.Approval_Status__c == 'EHO - Approved'
                            || bUnit.Approval_Status__c == 'LHO' || bUnit.Approval_Status__c == 'PLOT_EHO'
                            || bUnit.Approval_Status__c == 'PLOT_EHO - Approved' || bUnit.Approval_Status__c == 'PLOT_HO'
                            || bUnit.Approval_Status__c == 'PLOT_HO - Approved')) {
                            bUnit.Eligible_for_Handover_Notice_Date__c = system.today();
                        }
                    }
                }
            } else if (bUnit.Recovery__c == 'R' || bUnit.Recovery__c == 'A') {
                bUnit.Approval_Status__c = 'Recovery';
                lstBookingUnitsToUpdate.add(bUnit);
            }
            
        }
        
        try {
            if (lstBookingUnitsToUpdate != null && lstBookingUnitsToUpdate.size() > 0) {
                update lstBookingUnitsToUpdate;
             }
        }
        catch(System.DMLException excp) {
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, excp.getMessage()));
        }
        String hoEmails = Label.Handover_Team;
        list<String> lstRecipients = hoEmails.split(',');
        String hoTaskOwner = Label.HO_Task_Owner;
        list<String> lstOwners = hoTaskOwner.split(',');
        OrgWideEmailAddress[] owea;
        EmailTemplate templateId; 
        String mailMessage = '';
        try {
            templateId = [select id, Body, Subject, HtmlValue, name from EmailTemplate where developername = 'Email_to_Handover_Team'];
            owea = [select Id from OrgWideEmailAddress where Address = 'no-replysf@damacgroup.com'];
        }
        catch (Exception e) {
        }
        
        if (lstBookingUnitsToUpdate != null && lstBookingUnitsToUpdate.size() > 0) {
            if (templateId != null) {
                mailMessage =  templateId.HtmlValue;
            }
            for (Booking_Unit__c objBU : lstBookingUnitsToUpdate) {
                if (objBU.Approval_Status__c == 'HO' || objBU.Approval_Status__c == 'EHO' 
                    || objBU.Approval_Status__c == 'LHO' || objBU.Approval_Status__c == 'PLOT_EHO'
                    || objBU.Approval_Status__c == 'PLOT_HO') {
                    for (String ownerId : lstOwners ) {
                        Task objTask = new Task();
                        objTask.WhatId = objBU.Id;
                        objTask.Priority = 'Normal';
                        objTask.Status = 'Not Started';
                        objTask.Subject = 'Handover Eligibility status of Unit '+objBU.Unit_Name__c+' has changed. Please check and send Handover/EHO notice as required.';
                        objTask.Assigned_User__c = 'Handover HOD';
                        objTask.Process_Name__c = 'Handover';
                        objTask.OwnerId = ownerId;
                        objTask.ActivityDate = system.today() + 1;
                        objTask.Type = 'Call';
                        lstTask.add(objTask);
                    }
                    String prevStatus;
                    if (mapPreviousStatus != null && mapPreviousStatus.containsKey(objBU.id)) {
                        prevStatus = mapPreviousStatus.get(objBU.id);
                    }
                    String message;
                    if (!string.isBlank(mailMessage)) {
                        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                        message = mailMessage;
                        message = message.replace('{!Booking_Unit__c.Unit_Name__c}', objBU.Unit_Name__c);
                        message = message.replace('{!Booking_Unit__c.Approval_Status__c}', objBU.Approval_Status__c);
                        if (!string.isBlank(prevStatus)) {
                            message = message.replace('previous status', prevStatus );
                        } else {
                            message = message.replace('from previous status', '');
                        }
                        email.subject = templateId.Subject.replace('{!Booking_Unit__c.Unit_Name__c}', objBU.Unit_Name__c);
                        email.setHtmlBody(message);
                        email.toAddresses = lstRecipients;
                        email.setOrgWideEmailAddressId(owea.get(0).Id);
                        if (!string.isBlank(message)) {
                            messages.add(email);
                        }
                    }                    
                }
            }
        }
        
        if(lstTask != null && lstTask.size() > 0) {
            insert lstTask;
        } 
        
        if (messages != null && messages.size() > 0) {
             Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);//Sending emails
        }
    }
    
    public void finish(Database.BatchableContext BC) {
    
    }
    
    public static map<Id, Boolean> isEHO(list<string> setBUId) {

        map<Id, Boolean> mapBUHO = new map<Id, Boolean>();
        map<Id, Id> mapBUPaymentPlanId = new map<Id, Id>();
        map<Id, List<Payment_Terms__c>> mapPayPlanIdPayTerms = new map<Id, List<Payment_Terms__c>>();
        for (Payment_Plan__c objPaymentPlan : [Select Id, Status__c, Booking_Unit__c From Payment_Plan__c
                                                Where Booking_Unit__c IN: setBUId and Status__c = 'Active']) {
            mapBUPaymentPlanId.put(objPaymentPlan.Booking_Unit__c , objPaymentPlan.Id);
        }

        for (Payment_Terms__c objPT : [Select Id, Installment__c, Description__c, Milestone_Event__c, Payment_Plan__c From Payment_Terms__c
                                        Where Payment_Plan__c IN: mapBUPaymentPlanId.values()]) {
            if (mapPayPlanIdPayTerms.containsKey(objPT.Payment_Plan__c)) {
                List<Payment_Terms__c > lstPT = mapPayPlanIdPayTerms.get(objPT.Payment_Plan__c);
                lstPT.add(objPT);
                mapPayPlanIdPayTerms.put(objPT.Payment_Plan__c, lstPT);
            } else {
                mapPayPlanIdPayTerms.put(objPT.Payment_Plan__c, new List<Payment_Terms__c> { objPT });
            }
         }

         for (Id buId : setBUId) {
             if (mapPayPlanIdPayTerms != null && mapBUPaymentPlanId != null) {
                 if (mapBUPaymentPlanId.containsKey(buId) && mapPayPlanIdPayTerms.containsKey(mapBUPaymentPlanId.get(buId))){
                     Integer sizeOfPT = mapPayPlanIdPayTerms.get(mapBUPaymentPlanId.get(buId)).size();
                     for (Payment_Terms__c objPayterm : mapPayPlanIdPayTerms.get(mapBUPaymentPlanId.get(buId))) {
                         if (objPayterm.Installment__c.contains(String.valueOf(sizeOfPT - 1))){
                            //List<Milestones_For_Handover__c> lstMilestones = Milestones_For_Handover__c.getall().values();
                            Set<String> setHOAllowed = new set<String>();
                            for (Milestones_For_Handover__c objMilestone : Milestones_For_Handover__c.getall().values()) {
                                setHOAllowed.add(objMilestone.Name);
                            }
                            Boolean result = setHOAllowed.contains(objPayterm.Milestone_Event__c);
                            mapBUHO.put(buId, result);
                         }
                     }
                 }
             }
         }
         return mapBUHO;
    }
    
    public Map<String, Integer> getSnags(List<String> bUnitId) {

        Map<String, Integer> mapBUMajorSnags = new Map<String, Integer>();
        for (SNAGs__c objSnag : [Select Id, Booking_Unit__r.Registration_ID__c, Priority__c, Status__c From SNAGs__c
                                        Where Booking_Unit__c In :bUnitId]) {
            if (objSnag.Priority__c == 'High' && objSnag.Status__c != 'Closed') {
                if (mapBUMajorSnags.containsKey(objSnag.Booking_Unit__r.Registration_ID__c)) {
                    Integer noSnag =  mapBUMajorSnags.get(objSnag.Booking_Unit__r.Registration_ID__c);
                    mapBUMajorSnags.put(objSnag.Booking_Unit__r.Registration_ID__c, noSnag + 1);
                } else {
                    mapBUMajorSnags.put(objSnag.Booking_Unit__r.Registration_ID__c, 1);
                }
            }
        }
        return mapBUMajorSnags;
    }
    
    public Map<String, Integer> percSnagged(List<String> bUnitId) {
        Map<String, Integer> mapBUSnagged = new Map<String, Integer>();
        Map<String, Integer> mapBUTotalSnagged = new Map<String, Integer>();
        for (SNAGs__c objSnag : [Select Id, Booking_Unit__r.Registration_ID__c, Priority__c, Status__c From SNAGs__c
                                        Where Booking_Unit__c In :bUnitId]) {

            if (mapBUTotalSnagged.containsKey(objSnag.Booking_Unit__r.Registration_ID__c)) {
                Integer noSnag =  mapBUTotalSnagged.get(objSnag.Booking_Unit__r.Registration_ID__c);
                mapBUTotalSnagged.put(objSnag.Booking_Unit__r.Registration_ID__c, noSnag + 1);
            } else {
                mapBUTotalSnagged.put(objSnag.Booking_Unit__r.Registration_ID__c, 1);
            }
            if (objSnag.Status__c == 'Closed') {
                if (mapBUSnagged.containsKey(objSnag.Booking_Unit__r.Registration_ID__c)) {
                    Integer noSnag =  mapBUSnagged.get(objSnag.Booking_Unit__r.Registration_ID__c);
                    mapBUSnagged.put(objSnag.Booking_Unit__r.Registration_ID__c, noSnag + 1);
                } else {
                    mapBUSnagged.put(objSnag.Booking_Unit__r.Registration_ID__c, 1);
                }
            }
        }

        map<String, Integer> mapPercSnagged = new map<String, Integer>();
        for (String regId : mapBUTotalSnagged.keyset() ){
           Integer perSnagged =  mapBUSnagged.get(regId)/mapBUTotalSnagged.get(regId);
           mapPercSnagged.put(regId, perSnagged);
        }
        return mapPercSnagged;
    }
}