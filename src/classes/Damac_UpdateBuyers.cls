/********************************************************************************************************
* Name               : Damac_UpdateBuyers
* Test Class         : Damac_UpdateBuyers_Test
* Description        : class to Update Buyers
* -----------------------------------------------------------------------------------------------------  
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         SF Support       28/06/2018      Created
* 1.1         QBurst           20/02/2020      Modifications for DP Deals [SOAP to REST]
*********************************************************************************************************/
global class Damac_UpdateBuyers {
    webservice static String updateToIPMS (Id buyerId) {
        // 1.1 starts
        Buyer__c buyer = [SELECT Id, buyer_type__c, Marked_for_Deletion__c, Primary_Buyer__c 
                          FROM Buyer__c WHERE Id =: buyerId LIMIT 1];
        if(buyer.Marked_for_Deletion__c == true){
            return 'Buyer is Marked for Deletion.';
        }
        buyer.Address_Changed__c = true;
        buyer.Contact_Changed__c = true;
        buyer.Name_Passport_Changed__c = true;
        buyer.Nationality_Changed__c = true;
        update buyer;
        Boolean isPerson = true;
        if(buyer.buyer_type__c == 'Corporate'){
            isPerson = false;
        }
        DAMAC_IPMS_PARTY_CREATION.CreateParty('UPDATE_CUSTOMER', isPerson, 
                                      buyer.Primary_Buyer__c, buyer.id, buyer.Primary_Buyer__c);
        return 'Buyer details submitted to IPMS.';
    }

    webservice static String updateArabicDetailsToIPMS (Id buyerId) {
         Buyer__c buyer = [SELECT Id, buyer_type__c, Marked_for_Deletion__c, Primary_Buyer__c 
                          FROM Buyer__c WHERE Id =: buyerId LIMIT 1];
        if(buyer.Marked_for_Deletion__c == true){
            return 'Buyer is Marked for Deletion.';
        }
        Boolean isPerson = true;
        if(buyer.buyer_type__c == 'Corporate'){
            isPerson = false;
        }
        String sObjName = buyer.id.getSObjectType().getDescribe().getName();
        system.debug('sObjName==>'+sObjName);
        list<IPMS_Party_Mapping__mdt> mappingMetadata = new list<IPMS_Party_Mapping__mdt>();
        mappingMetadata = [SELECT Id, developerName, Field_API_Name__c, IPMS_Attribute__c 
                           FROM IPMS_Party_Mapping__mdt 
                           WHERE Object_Name__c =: sObjName];
        system.debug(mappingMetadata); 
        string queryString;
        queryString = 'SELECT Id, Name ';
        
        for(IPMS_Party_Mapping__mdt fld: mappingMetadata){
            system.debug(fld.Field_API_Name__c);
            if (!queryString.contains (fld.Field_API_Name__c))
                queryString += (queryString == '' ? '' : ',') + fld.Field_API_Name__c;
        }
        queryString += ',  Booking__r.Deal_SR__r.is_uk_deal__c'; // 1.1
        queryString += ' FROM ' + sObjName + ' WHERE ID=' + '\'' + buyer.id + '\'' + ' LIMIT 1';
        system.debug(queryString);        
        Buyer__c obj = database.query(queryString);
        // Prepare map to hold the name values of salesforce vs IPMS
        map<string,string> sfFldvalMap = new map<string,string>();
        for(IPMS_Party_Mapping__mdt fld:mappingMetadata){
            sfFldvalMap.put(fld.IPMS_Attribute__c , string.valueof(obj.get(fld.Field_API_Name__c)));
            System.debug (fld.IPMS_Attribute__c+'=='+string.valueof(obj.get(fld.Field_API_Name__c)));
        }
        system.debug('FldMap****='+sfFldvalMap);
        string PartyType ='PERSON';
        if(!isPerson){
            PartyType = 'ORGANIZATION';
        }
        string PrimaryFlag ='Y';
        system.debug('Test 1: ' + obj.Booking__r.Deal_SR__r.is_uk_deal__c);
        DAMAC_IPMS_PARTY_CREATION_JSON jsonClass = new DAMAC_IPMS_PARTY_CREATION_JSON();
        jsonClass.customerLine = new DAMAC_IPMS_PARTY_CREATION_JSON.cls_customerLine();
        jsonClass.customerLine.contactLines = new list<DAMAC_IPMS_PARTY_CREATION_JSON.cls_contactLines>();
        jsonClass.extRequestNumber = buyer.id;
        jsonClass.customerLine.propertyLocation = 'UK';
        if(obj.Booking__r.Deal_SR__r.is_uk_deal__c == false){ // 1.1
            jsonClass.customerLine.propertyLocation = 'DP'; 
        }
        jsonClass.customerLine.subRequestName = 'UPDATE_CUSTOMER'; 
        jsonClass.customerLine.extCustomerNumber = buyer.id;
        jsonClass.customerLine.partyType = PartyType;
        
        jsonClass.customerLine.partyId = sfFldvalMap.get('partyId');
        jsonClass.customerLine.translationLines 
                            = new list<DAMAC_IPMS_PARTY_CREATION_JSON.cls_translationLines>();
        DAMAC_IPMS_PARTY_CREATION_JSON.cls_translationLines arabicTransLine 
                            = new DAMAC_IPMS_PARTY_CREATION_JSON.cls_translationLines();
        arabicTransLine.languageCode = 'AR';
        arabicTransLine.title = sfFldvalMap.get('ar_title');
        arabicTransLine.firstName = sfFldvalMap.get('ar_firstName');
        arabicTransLine.middleName = sfFldvalMap.get('ar_middleName');
        arabicTransLine.lastName = sfFldvalMap.get('ar_lastName');
        arabicTransLine.nationality = sfFldvalMap.get('ar_nationality');
        arabicTransLine.address1 = sfFldvalMap.get('ar_address1');
        arabicTransLine.address2 = sfFldvalMap.get('ar_address2');
        arabicTransLine.address3 = sfFldvalMap.get('ar_address3');
        arabicTransLine.address4 = sfFldvalMap.get('ar_address4');
        arabicTransLine.city = sfFldvalMap.get('ar_city');
        arabicTransLine.country = sfFldvalMap.get('ar_country');
        jsonClass.customerLine.translationLines.add(arabicTransLine);
        List <String> fieldstoUpdate = new List <String> ();
            
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField ('subRequestName', 'UPDATE_CUSTOMER_TRANSLATION');
        String propertyLocation = 'UK';
        if(obj.Booking__r.Deal_SR__r.is_uk_deal__c == false) {
            propertyLocation = 'DP';
        }
                    
        gen.writeStringField ('propertyLocation', propertyLocation);
        gen.writeStringField ('partyId', sfFldvalMap.get('partyId'));
        gen.writeStringField ('countryCode', sfFldvalMap.get('ar_country') != null ? sfFldvalMap.get('ar_country') : '' );
        gen.writeStringField ('languageCode', 'AR');
        gen.writeStringField ('title', sfFldvalMap.get('ar_title') != null ? sfFldvalMap.get('ar_title') : '');
        gen.writeStringField ('firstName', sfFldvalMap.get('ar_firstName') != null ? sfFldvalMap.get('ar_firstName') : '');
        gen.writeStringField ('middleName', sfFldvalMap.get('ar_middleName') != null ? sfFldvalMap.get('ar_middleName') : '');
        gen.writeStringField ('lastName', sfFldvalMap.get('ar_lastName') != null ? sfFldvalMap.get('ar_lastName') : '');
        gen.writeStringField ('nationality', sfFldvalMap.get('ar_nationality') != null ? sfFldvalMap.get('ar_nationality') : '');
        gen.writeStringField ('address1', sfFldvalMap.get('ar_address1') != null ? sfFldvalMap.get('ar_address1') : '');
        gen.writeStringField ('address2', sfFldvalMap.get('ar_address2') != null ? sfFldvalMap.get('ar_address2') : '');
        gen.writeStringField ('address3', sfFldvalMap.get('ar_address3') != null ? sfFldvalMap.get('ar_address3') : '');
        gen.writeStringField ('address4', sfFldvalMap.get('ar_address4') != null ? sfFldvalMap.get('ar_address4') : '');
        gen.writeStringField ('postalCode', sfFldvalMap.get('postalCode') != null ? sfFldvalMap.get('postalCode') : '');
        gen.writeStringField ('city', sfFldvalMap.get('ar_city') != null ? sfFldvalMap.get('ar_city') : '');
        gen.writeStringField ('country', sfFldvalMap.get('ar_country') != null ? sfFldvalMap.get('ar_country') : '');

        gen.writeEndObject();
        String addressChangeJson = gen.getAsString();
        fieldstoUpdate.add ('Arabic_Details_Changed__c');
        
        String requestName = buyer.Primary_Buyer__c ? 'Primary Buyer UPDATE_CUSTOMER_TRANSLATION' : 'Joint Buyer UPDATE_CUSTOMER_TRANSLATION';
        
        DAMAC_IPMS_PARTY_CREATION.doChangePartyCallout (addressChangeJson, fieldstoUpdate, obj.Id, obj.Booking__r.Deal_SR__c, requestName);            
        return 'Buyer details submitted to IPMS.';
    }
}