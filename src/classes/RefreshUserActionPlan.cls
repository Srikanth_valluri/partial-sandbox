/****************************************************************************************************
* Name          : UpdateUserActionPlan                                                              *
* Description   : 1. Class to redirect to the detail page                                           *
* Created Date  : 27/03/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                *
* 1.0         Craig Lobo    29/03/2018      Initial Draft.                                          *
****************************************************************************************************/

public without sharing class RefreshUserActionPlan {

    public String apRefreshId = '';
    public String zsRefreshId = '';

    public RefreshUserActionPlan(ApexPages.StandardController controller) {
        apRefreshId = controller.getRecord().Id;
    }

    /** 
     * Method to refresh the Action Plan values and redirect to the Zero Sales User detail page 
     */
    public PageReference refreshZeroSalesUserPlans() {

        List<String> userActionPlanIdList = new List<String>();
        if(apexpages.currentPage().getParameters().get('zsRecId') != null) {
            zsRefreshId = apexpages.currentPage().getParameters().get('zsRecId');
        }

        PageReference pageRef;
        UpdateUserActionPlan actionPlanInstance = new UpdateUserActionPlan();
        System.debug('zsRefreshId>>>>>>>  ' + zsRefreshId);
        System.debug('apRefreshId>>>>>>>  ' + apRefreshId);

        if (String.isNotBlank(zsRefreshId) && String.isBlank(apRefreshId)) {
            // Called from the Zero Sales User Record
            for (User_Action_Plan__c uap : [ SELECT Id 
                                                FROM User_Action_Plan__c 
                                                WHERE Zero_Sales_User__c = :zsRefreshId
            ]) {
                userActionPlanIdList.add(uap.Id);
            }
            System.debug('userActionPlanIdList>>>>>>>  ' + userActionPlanIdList);

            if(userActionPlanIdList != null
                && !userActionPlanIdList.isEmpty()
            ) {
                actionPlanInstance.refreshUserActionPlanRecords('Parent', userActionPlanIdList);
            }

            // Redirect to the Detail page
            String headerdata = ApexPages.currentPage().getHeaders().get('Host');
            System.debug('headerdata>>>>>>>  ' + headerdata);
            pageRef = new PageReference('https://' + headerdata + '/' + zsRefreshId );

        } else if (String.isNotBlank(apRefreshId) && String.isBlank(zsRefreshId)) {
            // Called from the User Action Plan Record
            userActionPlanIdList.add(apRefreshId);
            actionPlanInstance.refreshUserActionPlanRecords('', userActionPlanIdList);
            
            // Redirect to the Detail page
            String headerdata = ApexPages.currentPage().getHeaders().get('Host');
            System.debug('headerdata>>>>>>>  ' + headerdata);
            pageRef = new PageReference('https://' + headerdata + '/' + apRefreshId );
        
        } else if (String.isBlank(zsRefreshId) && String.isBlank(apRefreshId)) {
            // Called from the User Action Plan list view
            actionPlanInstance.refreshUserActionPlanRecords('All', null);
            Map<String, Schema.SObjectType> m  = Schema.getGlobalDescribe() ;
            Schema.SObjectType s = m.get('User_Action_Plan__c') ;
            Schema.DescribeSObjectResult r = s.getDescribe() ;
            String keyPrefix = r.getKeyPrefix();
            System.debug('keyPrefix>>>>>>>  ' + keyPrefix);

            String headerdata = ApexPages.currentPage().getHeaders().get('Host');
            System.debug('headerdata>>>>>>>  ' + headerdata);
            pageRef = new PageReference('https://' + headerdata + '/' + keyPrefix );
        }
        return pageRef;
    }
}