/*
    Test Class Name: RMMeetingsControllerTest
*/


global without sharing class RMMeetingsController {

    public List < Task > eventsList { get; set; }
    public String inqNumber { get; set; }
    public String inqType { get; set; }
    public Boolean isCommentsEntered { get; set; }
    public String agencyId {get; set; }
    public String agentId {get; set; }
    public String errorMessage { get; set; }
    public List < Task > events { get; set; }
    public Inquiry__c inq { get; set; }
    public Date ownedSince { get; set; }  
    public Date rentedSince { get; set; }
    public String ownedSinceVal { get; set; }  
    public String rentedSinceVal { get; set; }
    public String inqOwnerName { get; set; }
    public Boolean inqSearch { get; set; }
    public Boolean showCheckIn { get; set; }
    public Boolean showInqDetails { get; set; }
    public Boolean showCheckOut { get; set; }
    public Boolean surveyCompleted { get; set; }
    public List < String > meetingOutcomes { get; set; }
    public Map < Id, Inquiry__c > inqDetailsMap { get; set; }
    public Id eventRecId { get; Set; }
    public Id eventRecId2 { get; Set; }
    public Boolean showPanel { get; set; }
    public Task followUpEvent { get; set; }
    public boolean showFilter { get; set; }
    public boolean showTbl1 { get; set; }
    public String inquiryDetails { get; set; }
    public List<Inquiry_Family_Details__c> inquiryFamilyList { get; set; }
    public boolean HOSUser { get; set; }
    public boolean DOSUser { get; set; }
    public string agencyName {get;set;}
    public String agentName { get; set; }
    
    
    // Method to display the locations from the multi select picklist of Interested Location
    public List<SelectOption> getLocation(){   
         
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Inquiry__c.Interested_Loc__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple) {
              options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return options;
    }
    public List<String> getBHKOptions () {
        List <String> bhkOptions = new List <String> ();
        bhkOptions.add ('Studio');
        bhkOptions.add ('1');
        bhkOptions.add ('2');
        bhkOptions.add ('3');
        bhkOptions.add ('4');
        bhkOptions.add ('5');
        bhkOptions.add ('6');
        bhkOptions.add ('7');
        bhkOptions.add ('8');
        bhkOptions.add ('9');
        bhkOptions.add ('10');
        return bhkOptions;
    }
    public List<SelectOption> options { get; set; }
    // Method to display the developement from the multi select picklist of Interested Developement
    public void getDevelopement(){
        String location = ApexPages.currentPage().getParameters().get('location');
        options = new List <SelectOption> ();
        if (location != NULL) {
            if (location.contains(', ')) {
                location = location.removeStart('[').removeEnd(']').replaceAll(', ', ';');
            } else {
                location = location.removeStart('[').removeEnd(']');
            }
            if (location != '') {
                Map<String, List <String>> results = new Map<String, List <String>>();
                Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
                Map<String, Schema.SObjectField> fieldMap = schemaMap.get ('Inquiry__c').getDescribe ().fields.getMap ();
                
                Schema.DescribeFieldResult theFieldResult = fieldMap.get('Interested_Develop__c').getDescribe();
                Schema.sObjectField theField = theFieldResult.getSObjectField();
                
                Schema.DescribeFieldResult ctrlFieldResult = fieldMap.get('Interested_Loc__c').getDescribe();
                Schema.sObjectField ctrlField = ctrlFieldResult.getSObjectField();
                
                results = DependentPickListFields.getDependentOptionsImpl(theField, ctrlField);
                
                Set <SelectOption> optionsSet = new Set <SelectOption> ();
                System.Debug(location);
                System.Debug(results.keySet());
                for (String key : location.split (';')) {
                    System.Debug(key);
                    if (results.containsKey (key)) {
                        for (String val :results.get(key)) {
                            optionsSet.add(new SelectOption(val, val));
                        }
                    }
                    
                }
                options.addAll(optionsSet);
                System.Debug('==='+options);
            }
           
        } else {
            options = new List <SelectOption> ();
        }    
    }
    
    public RMMeetingsController() {
        inqType = '';
        isCommentsEntered = false;
        HOSUser = false;
        agencyId = '';
        agentId = '';
        DOSUser = false;
        agencyname = '';
        agentName = '';
        eventsList = new List < Task > ();
        options = new List<SelectOption>();
        ID userId = UserInfo.getUserID();
        inqNumber = '';
        inquiryDetails = '';
        inqSearch = true;
        showCheckIn = false;
        showInqDetails = false;
        showCheckOut = false;
        errorMessage = '';
        surveyCompleted = false;
        events = new List <Task> ();
        inq = new Inquiry__c();
        inqOwnerName = '';
        inqDetailsMap = new Map <Id, Inquiry__c> ();
        List <Inquiry__c> inqRecords = new List <Inquiry__c> ();
        Set <Id> eventWhatIds = new Set <Id> ();
        inquiryFamilyList = new List<Inquiry_Family_Details__c>();
        
        eventRecId = NULL;
        showPanel = false;
        showFilter = true;
        showTbl1 = true;
        ownedSinceVal = '';
        rentedSinceVal = '';
        followUpEvent = new Task();
        DateTime now = System.now();
        /*if (Site.getSiteId() == null) {
             query += ' AND ownerId =:userId ';
        }*/
        for (Inquiry__c inq :[SELECT ID, Name, Full_Name__c, Meeting_Due_Date__c, Inquiry_Status__c, OwnerId, owner.Name,
                         (SELECT
                            WhatId, what.Name, Activity_Type_3__c, RM_Comments__c, DOS_Comments__c , HOS_Comments__c,
                            End_Date__c, Start_Date__c, Task_Due_Date__c, Activity_Outcome__c
                            FROM Tasks
                            WHERE Activity_Type_3__c = 'Meeting at Office'
                            AND Activity_Outcome__c = NULL
                             AND OwnerId =: userId 
                            Order By CreatedDate Desc LIMIT 30) 
                        FROM Inquiry__c WHERE OwnerId =: UserId LIMIT 300]) {
                        
            for (Task t :inq.Tasks) {
                eventsList.add (t);
                inqDetailsMap.put(inq.Id, inq);
            }
                
        }
        populateInquiry ();
    }
    public void autoPopulateInquiry(){
        ID inqId = apexpages.currentpage().getparameters().get('inquiryId');
        if(inqId != null){
            inqNumber = inqId;
            searchInquiry();
        }
    }
    
    public void populateInquiry () {
        ID inqId = apexpages.currentpage().getparameters().get('inqId');
        eventRecId = apexpages.currentpage().getparameters().get('eventId');
        if (inqId != null && eventRecId  != null) {
            events = [SELECT
                Activity_Type_3__c, whatId,HOS_comments__c,DOS_Comments__c ,
                End_Date__c, Start_Date__c, Task_Due_Date__c, RM_Comments__c, Activity_Outcome__c
                FROM Task
                WHERE 
                What.Type = 'Inquiry__c'
                AND ID =: eventRecId 
                AND WhatId =: inqID                    
            ];
            if(events[0].HOS_comments__c != null){
                isCommentsEntered = true;
            }
            User u = new User ();
            Id userId = UserInfo.getUserId();
            userId = apexpages.currentpage().getparameters().get('userId');
            u = [SELECT UserRole.Name FROM User WHERE ID =: userId ];
            System.debug('u.UserRole.Name'+u.UserRole);
            System.debug('u.UserRole.Name'+u.UserRole.Name);
            if (u != null && u.UserRole != null && u.UserRole.Name != null && u.UserRole.Name.contains ('HOS')) {
                HOSUser = true;
                DOSUser = true;
            }
            if (u != null && u.UserRole != null && u.UserRole.Name != null && u.UserRole.Name.contains ('DOS')) {
                HOSUser = false;
                DOSUser = true;
            }
            
            updateStartTime ();
        }
    }
    
    public void updateHOSComments () {
        Task task = new Task ();
        errorMessage = '';
        User u = new User ();
        Id userId = apexpages.currentpage().getparameters().get('userId');
        u = [SELECT UserRole.Name, Hos_name__c, HOS_Email__c, ManagerId , Manager.Email FROM User WHERE ID =: userId ];
        User inquiryOwner = [SELECT Dos_name__c, DOS_Email__c,HOS_Email__c,HOS_Name__c  FROM USER WHERE ID =: inq.OwnerId];
                    
        for (Task meeting: eventsList) {
            system.debug('meeting' + meeting.HOS_Comments__c);
            system.debug('meeting' + meeting.DOS_Comments__c);
            if (meeting.Id == eventRecId) {
                system.debug (hosUser+'-'+dosUser+'-'+meeting.HOS_Comments__c +'-'+meeting.DOS_Comments__c );
                
                if (meeting.HOS_Comments__c == null && hosUser) {
                    errorMessage = 'Please fill HOS comments.';
                }
                if (meeting.DOS_Comments__c == null && dosUser) {
                    errorMessage = 'Please fill DOS comments.';
                }
                task = meeting;
            }
        }
        system.debug (errorMessage);
        if (errorMessage == '') {
            update task;
            inquiryDetails = 'Comments updated successFully';
            system.debug (u.UserRole);
            
            if (u != null && u.UserRole != null && u.UserRole.Name != null && u.UserRole.Name.contains ('DOS')) {
                system.debug (u);
                if(inquiryOwner.HOS_Email__c != null && inquiryOwner.HOS_Email__c != ''){
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    User us = [SELECT Id FROM User WHERE Email =: inquiryOwner.HOS_Email__c AND IsActive = true];
                    String emailBody = 'Dear '+inquiryOwner.Hos_name__c+',<br/><br/> '+inq.Owner.name+' has completed meeting with '+inq.Name+'. Please click on the link below to view the need analysis details<br/>';
                    if (Site.getSiteId() == null) {
                        emailBody += URL.getSalesforceBaseUrl().toExternalForm() + '/apex/RMMeetings?inqId='+inq.id+'&eventId='+eventRecId+'&userId='+us.Id;
                    }else{
                    String pageIndex = Site.getPathPrefix();
                    emailBody += URL.getSalesforceBaseUrl().toExternalForm()+pageIndex + '/apex/RMMeetings?inqId='+inq.id+'&eventId='+eventRecId+'&userId='+us.Id;
                    }                    
                    mail.setToAddresses(new List<String>{inquiryOwner.Hos_email__c});
                    mail.setSubject('Needs Analysis - '+inq.Name);
                    mail.setUseSignature(false);
                    mail.setHtmlBody(emailBody);
                    Messaging.SendEmailResult [] results = Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{mail});
                    system.debug (results);
                }
            }
        }
    }
       
    public void addInquiryFamily(){
        Inquiry_Family_Details__c eachFamily = new Inquiry_Family_Details__c();
        Integer size = inquiryFamilyList.size();
        eachFamily.Name = String.valueOf(size+1);
        eachFamily.Inquiry__c = inq.id;
        inquiryFamilyList.add(eachFamily);
    }
    
    public void deleteSelectedFamilyMemeber(){
       String family = apexpages.currentpage().getparameters().get('selectedFId');
       for (Integer i = 0 ; i < inquiryFamilyList.size(); i++) {
            if (inquiryFamilyList.get(i).Name == family) {
                inquiryFamilyList.remove(i); 
            }
       }
       for (Integer i = 0 ; i < inquiryFamilyList.size(); i++) {
           inquiryFamilyList.get(i).Name = String.valueOf(i+1);
       
       }
    }
    
    // Method to display the inquiries based on the user search
    @RemoteAction
    public static List < Inquiry__c > searchInquiryName(String pSearchKey) {
        String searchString = '%' +
            pSearchKey +
            '%';
        
        ID userId = UserInfo.getUserID();
         String query = 'SELECT Full_Name__c, Name, Pre_Inquiry_Number__c FROM Inquiry__c'+
                        ' WHERE RecordType.Name = \'Inquiry\'';
            //AND Is_Meeting_Confirmed__c = TRUE
            query += ' AND ownerId =:userId ';
            query += ' AND(First_Name__c LIKE: searchString OR Last_Name__c LIKE: searchString OR Name LIKE: searchString OR(Pre_Inquiry_Number__c LIKE: searchString AND Pre_Inquiry_Number__c != NULL))'+
            + 'ORDER By Name DESC LIMIT 10';
        return Database.Query (query);   
    }
    @RemoteAction
    public static List < Inquiry__c > searchInquiryNameForNeedAnalysis(String pSearchKey) {
        String searchString = '%' +
            pSearchKey +
            '%';        
        ID userId = UserInfo.getUserID();
         String query = 'SELECT Full_Name__c, Name, Pre_Inquiry_Number__c FROM Inquiry__c'+
                        ' WHERE RecordType.Name = \'Inquiry\'';
            //AND Is_Meeting_Confirmed__c = TRUE
            if (Site.getSiteId() == null) {
                 query += ' AND ownerId =:userId ';
            }    
            query += ' AND(First_Name__c LIKE: searchString OR Last_Name__c LIKE: searchString OR Name LIKE: searchString OR (Pre_Inquiry_Number__c LIKE: searchString AND Pre_Inquiry_Number__c != NULL))'+
            + ' ORDER By Name DESC LIMIT 10';
        return Database.Query (query);   
    }
    
    // Utility Method to get all the fields based on object name
    public static string getAllFields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        if (objectType == null)
            return fields;
        
        Map <String, Schema.SObjectField> fieldMap = objectType.getDescribe ().fields.getMap ();
        for (Schema.SObjectField sfield : fieldMap.Values()) {
            fields += sfield.getDescribe().getName ()+ ', ';
        }
        return fields.removeEnd(', '); 
    }
    
    // method to get the events based on the selected Inquiries from the filer button
    public void searchInquiry() {
        showTbl1 = false;
        showPanel = true;
        errorMessage = '';
        system.debug ('==='+inqNumber);
        
        inq = new Inquiry__c();
        if (inqNumber != '' && inqNumber != NULL) {
            try {
                inqSearch = false;
                //inqNumber = inqNumber.trim();
                inq = Database.query('SELECT owner.Name, Agent_Name__r.Name, '+getAllFields('Inquiry__c')+' FROM Inquiry__c '+'WHERE id =: inqNumber ');
                inqOwnerName = inq.owner.name;
                ID userId = inq.ownerId;
                ownedSince = inq.Owned_Since__c;
                rentedSince = inq.Rented_Since__c;
                inq.Inquiry_Type__c = inqType;
                ID inqId = apexpages.currentpage().getparameters().get('inquiryId');
                system.debug (inqId);
                if(inqId != null){
                    inq.Need_Analysis_Created__c = true;
                }
                events = [SELECT
                    Activity_Type_3__c, HOS_Comments__c,WhatId , DOS_Comments__c ,
                    End_Date__c, Start_Date__c, Task_Due_Date__c, RM_Comments__c, Activity_Outcome__c
                    FROM Task
                    WHERE Activity_Type_3__c = 'Meeting at Office'
                    AND Activity_Outcome__c = NULL
                    AND What.Type = 'Inquiry__c'
                    AND OwnerId =: userId
                    AND WhatId =: inq.ID                    
                ];

                System.debug('+++' + events);

                if (events.size() == 0) {
                    Task eachTask = new Task();
                    eachTask.WhatId = inq.id;
                    eachTask.Subject = 'Meeeting';
                    eachTask.Activity_Type_3__c = 'Meeting at Office';
                    eachTask.Start_Date__c = System.now();
                    eachTask.End_Date__c = System.now();
                    eachTask.Task_Due_Date__c =  Date.today();
                    eachTask.OwnerId = userId;
                    insert eachTask;
                    System.debug('eachTask'+eachTask.OwnerId);
                    events = [SELECT
                        Activity_Type_3__c, HOS_Comments__c,WhatId , DOS_Comments__c ,
                        End_Date__c, Start_Date__c, Task_Due_Date__c, RM_Comments__c, Activity_Outcome__c
                        FROM Task
                        WHERE Activity_Type_3__c = 'Meeting at Office'
                        AND Activity_Outcome__c = NULL
                        AND What.Type = 'Inquiry__c'
                        AND OwnerId =: userId
                        AND WhatId =: inq.ID                    
                    ];
                    /*
                    inqSearch = true;
                    inqNumber = '';
                    errorMessage = 'There are no Meetings for this Inquiry.';*/
                }

                showCheckIn = true;
                ApexPages.currentPage().getParameters().put('eventId', events[0].id);
                updateStartTime ();
            } catch (Exception e) {
                inqSearch = true;
                inqNumber = '';
                system.debug(':::Entered Catch::::' + e.getMessage() + ':::::' + e.getLineNumber());
                errorMessage = 'There is no inquiry/Meeting with the search term.';
            }
        } else {
            errorMessage = 'Please choose the inquiry.';
            inqNumber = '';
        }
    }
    
    // Method to start the meeting from the filtered events and from Start meeting button from filtered events
    public void updateStartTime() {
        eventRecId = ApexPages.currentPage().getParameters().get('eventId');
        errorMessage = '';
        
        if (eventRecId != NULL) {

            Task task = [SELECT
                Activity_Type_3__c, WhatId, HOS_Comments__c, DOS_Comments__c ,
                End_Date__c, Start_Date__c, Task_Due_Date__c, RM_Comments__c, Activity_Outcome__c
                FROM Task
                WHERE Id =: eventRecId
                
            ];
            ID whatId = task.whatId;
            inq = Database.query('SELECT owner.name, Agent_Name__r.Name, Agency_Name__r.Name, '+getAllFields('Inquiry__c')+' FROM Inquiry__c '+ ' WHERE Id =: whatId  AND RecordType.Name = \'Inquiry\' LIMIT 1');
            inqOwnerName = inq.owner.name;
            system.debug (inq.Rented_Since__c);
            
            agentName = inq.Agent_Name__r.Name;
            agencyName = inq.Agency_Name__r.Name;
            ownedSince = inq.Owned_Since__c;
            rentedSince = inq.Rented_Since__c;
            
            if (inq.Interested_Location__c != NULL) {
                ApexPages.currentPage().getParameters().put('location', inq.Interested_Location__c);
                
                getDevelopement();
            }
            for (Task meeting: events) {
                if (meeting.Id == eventRecId) {
                    meeting.Start_Date__c = DateTime.Now();
                    task = meeting;
                }
            }

            if (errorMessage == '') {
                showCheckIn = false;
                showInqDetails = true;
                showCheckOut = true;
                showFilter = false;
                showPanel = true;
                if (!apexpages.currentpage().getparameters().containsKey ('inqId')) {
                    update task;
                }
                eventsList = events;
            }
            Date firstVisiteddate = null; 
            integer visitsCount = 0;
            date lastVisiteddate = null;
            for (Task t : [SELECT CreatedDate FROM Task WHERE Activity_Type_3__c = 'Meeting at Office' AND WhatId =:inq.id 
                            AND Activity_Outcome__c LIKE 'Show%' AND Status = 'Completed' order by CreatedDate ASC]) {
                if (firstVisiteddate == null)
                    firstVisiteddate = Date.valueOf (t.createdDate);
                visitsCount += 1;
                lastVisiteddate = Date.valueOf (t.createdDate);
            }
            if (firstVisiteddate == null) {
                firstVisiteddate = Date.Today ();
            }
            if (lastVisiteddate == null) {
                lastVisiteddate = Date.TOday ();
            }
            if (visitsCount == 0) {
                visitsCount = 1;        
            }
            inq.Date_of_1st_visit__c = firstVisiteddate;
            inq.Last_date_of_revisit__c = lastVisiteddate ;
            inq.Total_no_of_revisits__c = visitsCount;
            populateFamilyDetails ();
        }
    }
    
    @RemoteAction 
    global static List<Account> getAgencyDetails(String searchKey){
        List<Account> agencyList = new List<Account>();
        List<Id> accountIdsList = new List<Id>();
        for(Agency_PC__c thisAgency : [SELECT Id, user__c, Agency__c
                                       FROM Agency_PC__c 
                                       WHERE user__c =: UserInfo.getUserId() 
                                       AND  (Agency__r.RecordTypeId =: DamacUtility.getRecordTypeId('Account', 'Individual Agency'))]){
            accountIdsList.add(thisAgency.Agency__c);   
        }
        system.debug('#### accountIdsList = '+accountIdsList);
        for(Account thisAccount : [SELECT Id, Name, RecordTypeId, RecordType.Name,
                                       (SELECT Id, Name, Org_ID_formula__c, Org_ID__c FROM Agent_Sites__r  WHERE End_Date__c = NULL) 
                                   FROM Account 
                                   WHERE Name LIKE: '%' + searchKey + '%' 
                                   AND (Id IN: accountIdsList OR (RecordTypeId =: DamacUtility.getRecordTypeId('Account', 'Corporate Agency') 
                                           AND  Blacklisted__c = false AND  Terminated__c = false))]){
            agencyList.add(thisAccount);    
        }
        return agencyList;
    }
    
    /*
    Query the Corporate Agnecy Details and return results on the Page   
    */
    @RemoteAction 
    global static List<Contact> getCorporateAgents(String selectedCorporateAgency){
        List<Contact> agentsList = new List<Contact>();
        try {
            agentsList = [select id,Name from contact where AccountId=:selectedCorporateAgency];
        } Catch (Exception e) {}
        return agentsList;
    }  

    public void populateFamilyDetails () {
        inquiryFamilyList.addAll([SELECT Name,Age__c ,Location__c ,Relationship__c ,School_College__c FROM Inquiry_Family_Details__c 
                                WHERE Inquiry__c =:inq.id]);
        Integer size = inquiryFamilyList.size();
        if(size == 0){
            Inquiry_Family_Details__c eachFamily = new Inquiry_Family_Details__c();
            eachFamily.Name = String.valueOf(size+1);
            eachFamily.Inquiry__c = inq.id;
            inquiryFamilyList.add(eachFamily);
        }
    }
    // Method to start the meeting from the displayed default list of events
    public void updateStartTime1() {
        errorMessage = '';
        if (eventRecId != NULL) {

            Task task = [SELECT
                Activity_Type_3__c, WhatId, HOS_Comments__c, DOS_Comments__c ,
                End_Date__c, Start_Date__c, Task_Due_Date__c, RM_Comments__c, Activity_Outcome__c
                FROM Task
                WHERE Id =: eventRecId
                
            ];
            events.add(task);

            Id whatId = task.whatId;
            inq = Database.query('SELECT owner.name, Agent_Name__r.Name, Agency_Name__r.Name, '+getAllFields('Inquiry__c')+' FROM Inquiry__c '+ 'WHERE Id =: whatId  AND RecordType.Name = \'Inquiry\'  LIMIT 1');
            inqOwnerName = inq.owner.name;
            agentName = inq.Agent_Name__r.Name;
            agencyName = inq.Agency_Name__r.Name;
            ownedSince = inq.Owned_Since__c;
            rentedSince = inq.Rented_Since__c;
            if (inq.Interested_Location__c != NULL) {
                ApexPages.currentPage().getParameters().put('location', inq.Interested_Location__c);
                
                getDevelopement();
            }
            for (Task meeting: eventsList) {
                if (meeting.Id == eventRecId) {
                    meeting.Start_Date__c = DateTime.Now();
                    task = meeting;
                }
            }

            if (errorMessage == '') {
                showCheckIn = false;
                showInqDetails = true;
                showCheckOut = true;
                showFilter = false;
                update task;
            }
            Date firstVisiteddate = null; 
            integer visitsCount = 0;
            date lastVisiteddate = null;
            for (Task t : [SELECT CreatedDate FROM Task WHERE Activity_Type_3__c = 'Meeting at Office' AND WhatId =:inq.id 
                            AND Activity_Outcome__c LIKE 'Show%' AND Status = 'Completed' order by CreatedDate ASC]) {
                if (firstVisiteddate == null)
                    firstVisiteddate = Date.valueOf (t.createdDate);
                visitsCount += 1;
                lastVisiteddate = Date.valueOf (t.createdDate);
            }
            if (firstVisiteddate == null) {
                firstVisiteddate = Date.Today ();
            }
            if (lastVisiteddate == null) {
                lastVisiteddate = Date.TOday ();
            }
            if (visitsCount == 0) {
                visitsCount = 1;        
            }
            inq.Date_of_1st_visit__c = firstVisiteddate;
            inq.Last_date_of_revisit__c = lastVisiteddate ;
            inq.Total_no_of_revisits__c = visitsCount;
            populateFamilyDetails ();
        }
    }
   
    // Method to update the Task and Inquiry fields and sharing the Inquiry with TSA TL Owner Field end time when End Time button is clicked
    public pageReference UpdateEndTime() {
        errorMessage = '';
        inquiryDetails = '';
        String pageName ='';
        if(!Test.isRunningTest()){
        pageName = ApexPages.currentPage().getUrl().substringBetween('apex/', '?');
        }else{
            pageName = 'Damac_needAnalysis';
        }
        try {
            if (showInqDetails == true) {
                List<Inquiry_Family_Details__c> familyToInsert = new List<Inquiry_Family_Details__c>();
                for(Inquiry_Family_Details__c eachFamily : inquiryFamilyList){
                    if(eachFamily.Age__c != null || eachFamily.Location__c != null || eachFamily.Relationship__c  != null  || eachFamily.School_College__c != null ){
                        familyToInsert.add(eachFamily);
                    }
                }
                if(!familyToInsert.isEmpty()){
                    upsert familyToInsert ;
                }
                inq.Inquiry_Status__c = 'Active';
                ID inqId = apexpages.currentpage().getparameters().get('inquiryId');
                system.debug (inqId);
                if(inqId != null){
                    inq.Need_Analysis_Created__c = true;
                }
                if (inq.Interested_Location__c != NULL) {
                    String location = inq.Interested_Location__c;
                    if (location.contains(', ')) {
                        location = location.removeStart('[').removeEnd(']').replaceAll(', ', ';');
                    } else {
                        location = location.removeStart('[').removeEnd(']');
                    }
                    System.Debug(location);
                    inq.Interested_Location__c = location;
                }
                if (inq.Interested_Development__c != NULL) {
                    String location = inq.Interested_Development__c;
                    if (location.contains(', ')) {
                        location = location.removeStart('[').removeEnd(']').replaceAll(', ', ';');
                    } else {
                        location = location.removeStart('[').removeEnd(']');
                    }
                    inq.Interested_Development__c = location;
                }
                if (inqType != '' && inqType != null)
                    inq.Inquiry_Type__c = inqType;
                if (inq.inquiry_type__c == 'Agent') {
                    inq.Inquiry_Status__c = 'Potential Agent';
                }
                if(pageName == 'Damac_needAnalysis'){
                    if(ownedSinceVal != ''){
                        inq.Owned_Since__c = date.parse(ownedSinceVal) ;
                        inq.Rented_Since__c = null;
                    }
                    if(rentedSinceVal != ''){
                        inq.Rented_Since__c = date.parse(rentedSinceVal) ;
                        inq.Owned_Since__c = null;
                    }
                    if(agencyId != '')
                        inq.Agency_Name__c = agencyId;
                    
                    if(agentId  != '')
                        inq.Agent_Name__c = agentId;
                }
                
                system.debug ('---'+inq.Would_you_like_assistance_on_mortgage__c);
                
                system.debug ('---'+inq.Inquiry_Type__c);
                system.debug ('---'+inqType);
                update inq;
                inquiryDetails = 'Inquiry details updated successFully';
            }
    
            Set < Id > inquiryIds = new Set < Id > ();
            Task task = new Task();
            if (eventRecId != NULL) {
                
                errorMessage = '';
                if (showCheckOut == true) {
                    User currentUser = [SELECT Id,Dos_name__c, DOS_Email__c FROM USER WHERE ID =: inq.OwnerId];
                    system.debug (currentUser);
                    
                    if(currentUser.DOS_Email__c != null && currentUser.DOS_Email__c != ''){
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        String emailBody = 'Dear '+currentUser.Dos_name__c+',<br/><br/> '+inq.Owner.name+' has completed meeting with '+inq.Name+'. Please click on the link below to view the need analysis details<br/>';
                        User u = [SELECT Id FROM User WHERE Email =: currentUser.DOS_Email__c AND IsActive = true LIMIT 1];
                        if (Site.getSiteId() == null) {
                        emailBody += URL.getSalesforceBaseUrl().toExternalForm() + '/apex/RMMeetings?inqId='+inq.id+'&eventId='+eventRecId+'&userId='+u.Id;
                        }else{
                        String pageIndex = Site.getPathPrefix();
                        emailBody += URL.getSalesforceBaseUrl().toExternalForm()+pageIndex + '/apex/RMMeetings?inqId='+inq.id+'&eventId='+eventRecId+'&userId='+u.Id;
                        }
                        mail.setToAddresses(new List<String>{currentUser.Dos_email__c});
                        mail.setSubject('Needs Analysis - '+inq.Name);
                        mail.setUseSignature(false);
                        mail.setHtmlBody(emailBody);
                        Messaging.SendEmailResult [] results = Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{mail});
                        system.debug (results );
                    }
                    
                    for (Task meeting: eventsList) {
                        system.debug('meeting' + meeting.End_date__c);
                        System.debug('meeting.Activity_Outcome__c'+meeting.Activity_Outcome__c);
                        if (meeting.Id == eventRecId) {
                            meeting.End_Date__c = DateTime.Now();
                        }
                        if (meeting.RM_Comments__c == NULL && meeting.Id == eventRecId) {
                            errorMessage = 'Please fill RM Comments';
    
                        }
                        if (meeting.Activity_Outcome__c == NULL && meeting.Id == eventRecId) {
                            errorMessage = 'Please fill Meeting Outcome';
    
                        }
                        if (meeting.Activity_Outcome__c != NULL && meeting.Activity_Type_3__c != NULL) {
                            if ((meeting.Activity_Outcome__c.startsWith('No show') || meeting.Activity_Outcome__c == 'Show - Not interested') &&
                                meeting.Activity_Type_3__c == 'Meeting at Office') {
                                inquiryIds.add(meeting.WhatId);
                            }
                        }
                        if (meeting.Start_Date__c != NULL && meeting.Id == eventRecId &&
                            meeting.Activity_Outcome__c != NULL && meeting.Activity_Type_3__c != NULL) {
                            task = meeting;
    
                        }
    
                    }
    
                    if (errorMessage == '') {
                        showCheckIn = false;
                        showInqDetails = false;
                        showCheckOut = false;
                        surveyCompleted = false;
                        showPanel = false;
                        showFilter = false;
                        update task;
                    }
                    if (inquiryIds.size() > 0) {
                        list < Inquiry__share > shareRecordstoInsert = new list < Inquiry__share > ();
                        List < Inquiry__c > inqRecords = [SELECT TSA_TL_OwnerID_for_Follow_Up__c, Inquiry_Status__c, Pending_TL_Followup__c FROM Inquiry__c WHERE Id in: inquiryIds];
                        for (Inquiry__c inq: inqRecords) {
                            inq.Pending_TL_Followup__c = true;
                            inq.Inquiry_Status__c = 'Active';
                            
                            if (inq.TSA_TL_OwnerID_for_Follow_Up__c != NULL) {
                                Inquiry__share shareInq = new Inquiry__share();
                                shareInq.ParentId = inq.id;
                                shareInq.UserOrGroupId = inq.TSA_TL_OwnerID_for_Follow_Up__c;
                                shareInq.AccessLevel = 'edit';
                                shareInq.RowCause = 'Owner';
                                shareRecordstoInsert.add(shareInq);
                            }
                        }
                        if (shareRecordstoInsert.size () > 0)
                            insert shareRecordstoInsert;
                        update inqRecords;
                    }
                }
            }
             System.debug(inquiryDetails +'-'+errormessage+'-'+surveyCompleted );
            if (errorMessage != '') {
                surveyCompleted = false;
                inquiryDetails = '';
                System.debug(inquiryDetails +'-'+errormessage );
                return null;
            } else {
                 System.debug(inquiryDetails +'-'+errormessage );
                return null;
            }
            
           
        } catch (Exception e) {
            surveyCompleted = false;
            inquiryDetails = '';
            errormessage = e.getmessage ()+' '+e.getLineNumber ();
            return null;
        }
    }
        
    // Method to create the follow up event
    public void createFollowUpEvent() {
        System.Debug(followupEvent);
        
        if (followUpEvent == NUll) {
        } else {
            followUpEvent.WhatId = inq.Id;
    
            insert followUpEvent;
            showCheckIn = false;
            showInqDetails = true;
            showCheckOut = true;
            showFilter = false;
            followUpEvent = new Task();
        }

    }
    
}