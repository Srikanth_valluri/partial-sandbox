public class DEWARefundTasks {
    @InvocableMethod
    public static void updateCreateTask(List<Id> taskIdList) {
        map<Id,Task> mapOfCaseIdToTask = new map<Id,Task>();
        List<Task> lstTask = new List<Task>();
        List<SR_Attachments__c> lstSRAttachment = new List<SR_Attachments__c>();
        List<Case> caseListToUpdate = new list<Case>();
        if(taskIdList != Null && taskIdList.size() > 0) {
           List<Task> taskListFromPb = [Select Id,Subject,WhatId,Status from Task where Id IN: taskIdList];
           for(Task objTask : taskListFromPb) {             
                mapOfCaseIdToTask.put(objTask.WhatId,objTask);              
           }
           if(mapOfCaseIdToTask != Null && mapOfCaseIdToTask.keySet() != Null &&  mapOfCaseIdToTask.keySet().size() > 0) {
            List<Case> caseList = [Select Id,
                                         Old_Bills_Unpaid__c,
                                         Dispatch_Status__c,
                                         Booking_Unit__c,
                                         Booking_Unit__r.Tenant__c,
                                         OwnerId,                                        
                                         Parking_Details_JSON__c,
                                         Booking_unit__r.No_of_parking__c,
                                         Booking_unit__r.Parking_Bay_No__c, 
                                         Submit_for_Approval__c,
                                         CurrencyIsoCode
                                  From Case Where Id IN: mapOfCaseIdToTask.keySet()];
                for(Case objCase : caseList) {
                    if(mapOfCaseIdToTask.containsKey(objCase.Id) && mapOfCaseIdToTask.get(objCase.Id) != Null) {
                            if((mapOfCaseIdToTask.get(objCase.Id).Subject == System.Label.Calculate_Verify_Refund_Amount || 
                                mapOfCaseIdToTask.get(objCase.Id).Subject == System.Label.Calculate_Verify_Refund_Amount_Bills_Unpaid)
                             && mapOfCaseIdToTask.get(objCase.Id).Status == 'Completed') {
                                if(objCase.Booking_Unit__r.Tenant__c != Null) {
                                    SR_Attachments__c objSRAttachment = new SR_Attachments__c();
                                    objSRAttachment.Name = 'Ejari / Tenancy Contract';
                                    objSRAttachment.Case__c = objCase.Id;
                                    lstSRAttachment.add(objSRAttachment);
                                    
                                }
                            }
                            else if(mapOfCaseIdToTask.get(objCase.Id).Subject == System.Label.Inform_Customer_And_Provide_Next_Month_DEWA_Bill
                            && mapOfCaseIdToTask.get(objCase.Id).Status == 'Completed') {
                                if(objCase.Dispatch_Status__c == 'Yes') {
                                    Task objTask = createTask(System.Label.Calculate_Verify_Refund_Amount_Bills_Unpaid,objCase, 'DLP Team');
                                    lstTask.add(objTask);
                                }
                                else {
                                    Task objTask = createTask(System.Label.Do_site_visit_for_issues_rectification,objCase, 'DLP Site Team');
                                    objTask.Show__c = true;
                                    lstTask.add(objTask);
                                }
                            }
                            else if(mapOfCaseIdToTask.get(objCase.Id).Subject == System.Label.Validate_Request_and_Calculations
                            && mapOfCaseIdToTask.get(objCase.Id).Status == 'Rejected') {
                                if(objCase.Old_Bills_Unpaid__c == 'Yes') {
                                    Task objTask = createTask(System.Label.Calculate_Verify_Refund_Amount,objCase,'DLP Team');
                                    objTask.Show__c = true;
                                    lstTask.add(objTask);
                                }
                                else {
                                    Task objTask = createTask(System.Label.Calculate_Verify_Refund_Amount_Bills_Unpaid,objCase,'DLP Team');
                                    lstTask.add(objTask);
                                }
                            }
                            else if(mapOfCaseIdToTask.get(objCase.Id).Subject == System.Label.Validate_Request_and_Calculations
                            && mapOfCaseIdToTask.get(objCase.Id).Status == 'Completed') {
                                String approver = '';
                                String strApprovalAuth = SfRuleEngine.filterSfRuleEngine('DEWA Refund', objCase.Id);
                                System.debug('strApprovalAuth>>>'+strApprovalAuth);
                                if (strApprovalAuth != null && !String.isBlank(strApprovalAuth) ){
                    
                                        approver = strApprovalAuth;
                                        system.debug('approver'+approver);
                             
                                }
                                 if (!string.isBlank(approver)) {
                                    objCase.Roles_from_Rule_Engine__c = approver;
                                    objCase.Approving_Authorities__c = approver;
                                    objCase.Submit_for_Approval__c = true;
                                 }
                                 caseListToUpdate.add(objCase);
                            }                         
                    }
                }
                    if(!lstSRAttachment.isEmpty()) {
                        insert lstSRAttachment;
                    }
                    if(!lstTask.isEmpty()) {
                        insert lstTask;
                    }
                                                
                    if(caseListToUpdate != null && caseListToUpdate.size() > 0) {
                        update caseList;
                    }
           }

        }
    }
    
    public static task createTask(String subject,Case objCase, String assignedUser) {
        Task objTask = new Task();
        objTask.Subject = subject;
        objTask.Assigned_User__c = assignedUser;
        objTask.ActivityDate = Date.today() + 2;
        objTask.OwnerId = objCase.OwnerId;
        objTask.Priority = 'Normal';
        objTask.Status = 'Not Started';
        objTask.WhatId = objCase.Id;
        objTask.CurrencyIsoCode = objCase.CurrencyIsoCode;
        return objTask;
    }
}