public class EOIProcessController {
    public static Inquiry__c insertInquery{get;set;}
    public static EOI_Process__c inserteoi {get; set;}
    public attachment objAttachment{get;set;}
    public blob getfile{get;set;}
    public string filename{get;set;}
    public String contentType{get; set;}
    public String UserName{get;set;}
    public static string insertedEOIName{get;set;}
    public EOIProcessController() {
        insertInquery = new Inquiry__c();
        inserteoi = new EOI_Process__c ();
        /*Property__c defaultProperty = new Property__c();
        defaultProperty = [Select id, name From Property__c Where name =: System.Label.Default_EOI_Property];
        inserteoi.Property__c = defaultProperty.id;*/
        inserteoi.EOI_for_Project__c = System.Label.Default_EOI_Property;
        objAttachment = new Attachment();

    }
    
    public class InquiryDetails {
        public String noUnits;
        public String property;
        public String token;
        public String mode;
        public string comm;
    }
    
    public class EOIDetails {
        public String fname;
        public String lname;
        //public String passportNo;
        //public String pLang;
        public String mCCode;
        public String mNo;
        public string email;        
    }
    
    @remoteAction
    public static EOI_Process__c Save(string EOIid, string inquiryId){
        User loggedinuser = [Select accountid,contactid,id from User where id = :Userinfo.getuserid()];
        System.debug('@@ Eoi @@ ' + EOIid);
        System.debug('@@ inquiryId @@ ' + inquiryId);
        //System.debug('@@@@ stringify @@@ '+JSON.stringify(EOIid));
        EOIDetails eoi = (EOIDetails)JSON.deserialize(EOIid,EOIDetails.class);
        InquiryDetails  inq = (InquiryDetails)JSON.deserialize(inquiryId,InquiryDetails.class);
        
        System.debug('@@@ deserialiaed inq  @@ '+ inq);
        
        System.debug('@@@ deserialized eoi @@@ ' + eoi);
        insertInquery = new Inquiry__c (); 
        insertInquery.First_Name__c = eoi.fname;
        insertInquery.Last_Name__c = eoi.lname;
        //insertInquery.Passport_Number__c = eoi.passportNo;
        //insertInquery.Preferred_Language__c = eoi.pLang;
        insertInquery.Mobile_CountryCode__c = eoi.mCCode;
        insertInquery.Mobile_Phone_Encrypt__c = 'testvalue';
        insertInquery.Mobile_Phone__c = eoi.mNo;
        insertInquery.Email__c = eoi.email;
        //insertInquery.Comments__c = eoi.comm;
        
        insert insertInquery;
        
        System.debug('Inquiry id @@ '+ insertInquery.id);
        
        inserteoi = new EOI_Process__c();
        inserteoi.Inquiry__c = insertInquery.id;
        //inserteoi.Property__c = inq.property;
        inserteoi.EOI_for_Project__c = inq.property;
        inserteoi.No_of_Units__c = inq.noUnits;
        inserteoi.Token_Amount__c = decimal.valueof(inq.token);
        inserteoi.Mode_of_Token_Payment__c = inq.mode;
        inserteoi.Agency__c = loggedinuser.accountid;
        inserteoi.Agency_Contact__c = loggedinuser.contactid;
        inserteoi.EOI_Comments__c = inq.comm;
        insert inserteoi;
        
        system.debug('@@ inserteoi @@ id ' + inserteoi.id);  
         
        EOI_Process__c insertedEOI = new EOI_Process__c ();
        insertedEOI  = [Select id, name From EOI_Process__c Where id =: inserteoi.id];
        system.debug('@@ inserteoi @@ name ' + insertedEOI.name);
        insertedEOIName = insertedEOI.name;
        system.debug('@@ insertedEOIName @@  ' + insertedEOIName);  
        /*Submitting approval process. */
        Approval.ProcessSubmitRequest financeApprovalProcess = new Approval.ProcessSubmitRequest();
        financeApprovalProcess.setComments('Submitting request for approval.');
        financeApprovalProcess.setObjectId(inserteoi.id);
        financeApprovalProcess.setSubmitterId(UserInfo.getUserId());

        financeApprovalProcess.setProcessDefinitionNameOrId('Approve_EOI');
        Approval.ProcessResult result = Approval.process(financeApprovalProcess);
        
        return insertedEOI;    
    }   
    
    
    public static List<SelectOption> getModeofpayment() {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = EOI_Process__c.Mode_of_Token_Payment__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
         options.add(new SelectOption('', '---None---'));
        for( Schema.PicklistEntry f : ple)
        {
            String valueOfPicklist = f.getValue();
            if(!valueOfPicklist.equalsIgnoreCase('Cash') && !valueOfPicklist.equalsIgnoreCase('Credit Card')  ) {
                options.add(new SelectOption(f.getValue(), f.getLabel()));
            }
            
        }       
            return options;
    }
    
    public static List<SelectOption> getNoOfUnits() {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = EOI_Process__c.No_of_Units__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
         options.add(new SelectOption('', '---None---'));
        for( Schema.PicklistEntry f : ple)
        {
            String valueOfPicklist = f.getValue();
            if(!valueOfPicklist.equalsIgnoreCase('0 Unit')) {
                options.add(new SelectOption(f.getValue(), f.getLabel()));
            }
            
        }       
            return options;
    }
    
}