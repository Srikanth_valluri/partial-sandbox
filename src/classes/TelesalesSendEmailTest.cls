/*-------------------------------------------------------------------------------------------------
Description: Test class for private class TelesalesSendEmail

    ============================================================================================================================
        Version | Date(DD-MM-YYYY) | Last Modified By | Comments
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 29-05-2018       | Monali Nagpure   | 1. Test send email functionality
   =============================================================================================================================
*/

@isTest
private class TelesalesSendEmailTest {


    @testSetup static void setup() {
        //Creaing test data
        User PCuser1 = new User(
         ProfileId = [SELECT Id FROM Profile WHERE Name = 'Telesales Team'].Id,
         LastName = 'Telesales User',
         Email = 'puser000@amamama.com',
         Username = 'puser000@amamama.com' + System.currentTimeMillis(),
         CompanyName = 'TEST',
         Title = 'title',
         Alias = 'alias',
         TimeZoneSidKey = 'America/Los_Angeles',
         EmailEncodingKey = 'UTF-8',
         LanguageLocaleKey = 'en_US',
         LocaleSidKey = 'en_US'
        );
        insert PCuser1;
        
        //Creaing test data
        User PCuser = new User(
         ProfileId = [SELECT Id FROM Profile WHERE Name = 'Property Consultant'].Id,
         LastName = 'last',
         Email = 'puser000@amamama.com',
         Username = 'puser000@amamama.com' + System.currentTimeMillis(),
         CompanyName = 'TEST',
         Title = 'title',
         Alias = 'alias',
         TimeZoneSidKey = 'America/Los_Angeles',
         EmailEncodingKey = 'UTF-8',
         LanguageLocaleKey = 'en_US',
         LocaleSidKey = 'en_US',
         TSA_TL__c =   PCuser1.Id 
        );
        insert PCuser;
        
        

        User receptionalistuser = new User(
         ProfileId = [SELECT Id FROM Profile WHERE Name = 'Receptionist'].Id,
         LastName = 'last',
         Email = 'puser000@amamama.com',
         Username = 'puser000@amamama.com' + System.currentTimeMillis(),
         CompanyName = 'TEST',
         Title = 'title',
         Alias = 'alias',
         TimeZoneSidKey = 'America/Los_Angeles',
         EmailEncodingKey = 'UTF-8',
         LanguageLocaleKey = 'en_US',
         LocaleSidKey = 'en_US',
         IsActive = true
        );
        insert receptionalistuser;

        String digitalRecordType=DamacUtility.getRecordTypeId('Campaign__c',
                                                              'Digital');
        Campaign__c campaignObject = new Campaign__c( Lead_Prioritization__c='Priority Digital',
                                                      RecordTypeId=digitalRecordType,Campaign_Type_New__c='Social',
                                                      Campaign_Category_New__c='Facebook',
                                                      Campaign_Sub_Category_1__c='Organic',
                                                      End_Date__c=date.parse('11/10/18'), 
                                                      Marketing_End_Date__c=date.parse('11/10/18'), 
                                                      Marketing_Start_Date__c=date.parse('11/10/16'), 
                                                      Start_Date__c=date.parse('11/10/16'));
        insert campaignObject;
        
        Id devRecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        Inquiry__c inq = new Inquiry__c();
        inq.Mobile_Phone_Encrypt__c = '05789088';
        inq.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Preferred_Language__c = 'English';
        inq.Inquiry_Status__c = 'New' ;
        inq.First_Name__c = 'Test First name';
        inq.Last_Name__c = 'Test Last name';
        inq.Email__c = 'test@gmail.com';
        inq.Inquiry_Source__c = 'Customer Referral';
        inq.Mobile_Phone__c = '456789';
        inq.RecordTypeId = devRecordTypeId;
        inq.Assigned_PC__c = PCuser.Id;
        inq.Inquiry_Status__c = 'Meeting Scheduled';
        inq.Campaign__c = campaignObject.Id;
        insert inq;

        Id devRecordTypeId1 = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
        Inquiry__c preInq = new Inquiry__c();
        preInq.Mobile_Phone_Encrypt__c = '05789088';
        preInq.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        preInq.Preferred_Language__c = 'English';
        preInq.Inquiry_Status__c = 'New' ;
        preInq.First_Name__c = 'Test First name';
        preInq.Last_Name__c = 'Test Last name';
        preInq.Email__c = 'test@gmail.com';
        preInq.Inquiry_Source__c = 'Customer Referral';
        preInq.Mobile_Phone__c = '456789';
        preInq.RecordTypeId = devRecordTypeId1;
        preInq.Assigned_PC__c = PCuser.Id;
        preInq.Campaign__c = campaignObject.Id;
        preInq.Inquiry_Status__c = 'Active';
        //preInq.TSA_TL_of_Owner__c = PCuser.Id;
        System.runAs(PCuser1) {
        insert preInq;
        }

    }

    @isTest static void TaskCompleted() {
        Inquiry__c inq = [SELECT Id, 
                                 Inquiry_Status__c,
                                 RecordType.Name,
                                 TSA_TL_of_Owner__c,
                                 First_Name__c,
                                 Last_Name__c,
                                 Email__c,
                                 Meeting_Due_Date__c,
                                 Sales_Office__c,
                                 Mobile_Phone__c
                            FROM Inquiry__c
                           WHERE RecordType.Name = 'Inquiry'
                             AND Inquiry_Status__c = 'Meeting Scheduled'
                           LIMIT 1
                             ];
        system.debug('====Test class===inq=======' +inq);                     
        List<Task> lstTasks = new List<Task>();
        //for(Integer i=0; i<20; i++) {
            lstTasks.add(new Task(WhatId=inq.Id,
                            ActivityDate = System.today().addDays(2),
                            Status = 'Completed',
                            Type = 'Meeting at Office',
                            Activity_Type_3__c = 'Meeting at Office',
                            Activity_Outcome__c = 'Show - Further Meeting Scheduled',
                            Subject = 'Meeting at Office',
                            is_Owner_changed__c = true
                            ));
        //}

        Test.startTest();
        insert lstTasks;
        TelesalesSendEmail instance = new TelesalesSendEmail();
        instance.MeetingCompletedMail (new Set<Id>{inq.Id});
        instance.RemoveInquiryAccess (new Set <ID> {inq.Id}, new Set <ID> {UserInfo.getUserId()});
        Test.stopTest();
    }
    
    @isTest static void testChangeOwner() {
        Inquiry__c inq = [SELECT Id, 
                                 Inquiry_Status__c,
                                 RecordType.Name,
                                 TSA_TL_of_Owner__c,
                                 First_Name__c,
                                 Last_Name__c,
                                 Email__c,
                                 Meeting_Due_Date__c,
                                 Sales_Office__c,
                                 Mobile_Phone__c
                            FROM Inquiry__c
                           WHERE RecordType.Name = 'Inquiry'
                             AND Inquiry_Status__c = 'Meeting Scheduled'
                           LIMIT 1
                             ];
        system.debug('====Test class===inq=======' +inq);                     
        List<Task> lstTasks = new List<Task>();
        //for(Integer i=0; i<20; i++) {
            lstTasks.add(new Task(WhatId=inq.Id,
                            ActivityDate = System.today().addDays(2),
                            Status = 'Not Started',
                            Type = 'Meeting at Office',
                            Activity_Type_3__c = 'Meeting at Office',
                            Activity_Outcome__c = 'Show - Further Meeting Scheduled',
                            Subject = 'TSA Task',
                            is_Owner_changed__c = false
                            ));
        insert lstTasks;
        Test.startTest();
        update lstTasks;
        Test.stopTest();
    }

    @isTest static void testTaskNotStarted() {
        Inquiry__c inq = [SELECT Id, 
                                 Inquiry_Status__c,
                                 RecordType.Name,
                                 TSA_TL_of_Owner__c,
                                 First_Name__c,
                                 Last_Name__c,
                                 Email__c,
                                 Meeting_Due_Date__c,
                                 Sales_Office__c,
                                 Mobile_Phone__c
                            FROM Inquiry__c
                           WHERE RecordType.Name = 'Pre Inquiry'
                             AND Inquiry_Status__c = 'Active'
                           LIMIT 1
                             ];
        system.debug('====Test class===inq=======' +inq);                     
        List<Task> lstTasks = new List<Task>();
        //for(Integer i=0; i<20; i++) {
            lstTasks.add(new Task(WhatId=inq.Id,
                            ActivityDate = System.today().addDays(2),
                            Status = 'Not Started',
                            Type = 'Meeting at Office',
                            Activity_Type_3__c = 'Meeting at Office',
                            Activity_Outcome__c = 'Show - Further Meeting Scheduled',
                            Subject = 'TSA',
                            is_Owner_changed__c = true
                            ));
        //}

        Test.startTest();
        insert lstTasks;
        Test.stopTest();
    }

    @isTest static void officeMeeting(){

        Task tsk = new Task(ActivityDate = System.today().addDays(2),Status = 'Completed',
                            Type = 'Meeting at Office',Activity_Type_3__c = 'Meeting at Office',
                            Activity_Outcome__c = 'No Show- Not Interested',Subject = 'TSA TL');
        insert tsk;

        Map<String, String> pActvityOutcomeMap = new Map<String, String>();
        pActvityOutcomeMap.put(tsk.Id, 'Show');

        Office_Meeting__c off = new Office_Meeting__c(
            Related_Task__c = tsk.Id,
            Outcome__c = 'Show', 
            Status__c = 'Completed'
        );
        insert off;

        Test.startTest();
        TelesalesSendEmail instance = new TelesalesSendEmail();
        instance.updateOfficeMeetingRecords(pActvityOutcomeMap);
        
        pActvityOutcomeMap.put(tsk.Id, 'No Show');
        instance.updateOfficeMeetingRecords(pActvityOutcomeMap);
        instance.PerformTaskActivity (new List<Task>{tsk});
        
        Test.stopTest();

    }

    @isTest static void testMeetingCancelled(){
        Inquiry__c inq = [SELECT Id, 
                                 Inquiry_Status__c,
                                 RecordType.Name,
                                 TSA_TL_of_Owner__c,
                                 First_Name__c,
                                 Last_Name__c,
                                 Email__c,
                                 Meeting_Due_Date__c,
                                 Sales_Office__c,
                                 Mobile_Phone__c
                            FROM Inquiry__c
                           WHERE RecordType.Name = 'Pre Inquiry'
                             AND Inquiry_Status__c = 'Active'
                           LIMIT 1
                             ];
        system.debug('====Test class===inq=======' +inq);  
        List<User> userList = [SELECT Id,
                                      Profile.Name,
                                      IsActive,
                                      isUserOnLeave__c   
                                FROM User 
                               WHERE Profile.Name = 'Receptionist' 
                                 AND IsActive = true
                                AND isUserOnLeave__c = false];
        system.debug('=Test class=userList====' + userList);

        // Share inquiry with TSA TL
        Inquiry__Share shareObj = new Inquiry__Share();
        shareObj.AccessLevel = 'Edit';
        shareObj.ParentId = inq.Id;
        shareObj.UserOrGroupId = userList[0].Id;
        shareObj.RowCause = Schema.Inquiry__Share.RowCause.Manual;
        insert shareObj;

        List<Task> lstTasks = new List<Task>();
        //for(Integer i=0; i<20; i++) {
            lstTasks.add(new Task(WhatId=inq.Id,
                            ActivityDate = System.today().addDays(2),
                            Status = 'Deferred',
                            Type = 'Meeting at Office',
                            Activity_Type_3__c = 'Meeting at Office',
                            Activity_Outcome__c = 'No Show- Not Interested',
                            Subject = 'TSA TL',
                            is_Owner_changed__c = true
                            ));
        //}

        Test.startTest();
        insert lstTasks;
        Test.stopTest();
    }

}