/**************************************************************************************************
* Name               : Invocable_SalesAuditUpdate
* Test Class         : Invocable_SalesAuditUpdate_Test
* Description        : This is the custom code class for sending Audit Status to IPMS         
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR          DATE            Comments
* 1.0                         16/06/2019      Created
* 1.1         QBurst          08/04/2020      SOAP to REST Changes
**************************************************************************************************/
public class Invocable_SalesAuditUpdate {
    @InvocableMethod
    public static void UpdateBuyer(List<Id> stepId) {
        for (New_Step__c step :[SELECT Id, Service_Request__c, Step_Status__c FROM New_Step__c WHERE Id IN :stepId]) {
            evaluateCustomCode(step);
        }
    }

    public static String evaluateCustomCode(New_Step__c step) {
        String sSalesAuditFlag = 'N';
        if (step.Step_Status__c == 'Sales Audit Approved' || step.Step_Status__c == 'Sales Audit Not Applicable')
        {
            sSalesAuditFlag = 'Y';
        }
        system.debug('SalesAudit' + sSalesAuditFlag);
        String retStr = 'Success';
        List<Id> bookingIds = new List<Id> ();
        try {
            for (Booking__c bookings :[SELECT Id FROM Booking__c WHERE Deal_SR__c = : step.Service_Request__c]) {
                bookingIds.add(bookings.id);
            }
            if (bookingIds.size() > 0) {
                system.debug('#### invoking CC_AgentRegUpdate');
                //system.enqueueJob(new AsyncSalesAuditUpd(bookingIds, 'SalesAuditUpdate', sSalesAuditFlag)); // 1.1
                DAMAC_IPMS_PARTY_CREATION.updateBookingUnitToIPMS(bookingIds, 'UPDATE_AGENT_AUDIT', sSalesAuditFlag); // 1.1
            }
        } catch(Exception e) {
            retStr = 'Error :' + e.getMessage() + '';
        }
        return retStr;
    }
}