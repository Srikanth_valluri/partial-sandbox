@isTest
private class HandoverControllerTest{
    private static testMethod void excessAmount(){
        Case objCase = new Case();
        objCase.Origin = 'Walk-In';
        objCase.Status = 'Open';
        objCase.Pending_Amount__c = -30;
        objCase.DP_SOA_Balance__c = -30;
        insert objCase;
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        HandoverController objClass = new HandoverController(sc);
        objClass.init();
        objClass.objCases.Excess_Amount__c = 10;
        SOAPCalloutServiceMock.returnToMe = new Map<String,TaskCreationWSDL.SRDataToIPMSMultipleResponse_element>();
        TaskCreationWSDL.SRDataToIPMSMultipleResponse_element response1 = new TaskCreationWSDL.SRDataToIPMSMultipleResponse_element();
        response1.return_x = '{'+
    ''+
    '"message":"message",'+
    '"status":"E"'+
    '}';
       SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
       Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());        
          Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockRPTermination(1) );
          objClass.crfAttachmentBody = EncodingUtil.base64Encode(Blob.valueOf('Test'));
          objClass.crfAttachmentName = 'CRF.txt';
        objClass.createExcessAmountTask();
        Test.stopTest();
    }

   private static testMethod void greaterExcessAmount(){
        Case objCase = new Case();
        objCase.Origin = 'Walk-In';
        objCase.Status = 'Open';
        objCase.Pending_Amount__c = -30;
        objCase.DP_SOA_Balance__c = -30;
        insert objCase;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        HandoverController objClass = new HandoverController(sc);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockRPTermination(1) );
        objClass.init();
        objClass.objCases.Excess_Amount__c = 40;  
        objClass.crfAttachmentBody = EncodingUtil.base64Encode(Blob.valueOf('Test'));
        objClass.crfAttachmentName = 'CRF.txt';        
        
        objClass.createExcessAmountTask();
    }

   private static testMethod void greaterExcessAmountForCloseCase(){
        Case objCase = new Case();
        objCase.Origin = 'Walk-In';
        objCase.Status = 'Closed';
        objCase.Pending_Amount__c = -30;
        objCase.DP_SOA_Balance__c = -30;
        insert objCase;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        HandoverController objClass = new HandoverController(sc);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockRPTermination(1) );
        objClass.init();
        objClass.objCases.Excess_Amount__c = 40;  
        objClass.crfAttachmentBody = EncodingUtil.base64Encode(Blob.valueOf('Test'));
        objClass.crfAttachmentName = 'CRF.txt';        
        
        objClass.createExcessAmountTask();
    }

    private static testMethod void noExcessAmount(){
        Case objCase = new Case();
        objCase.Origin = 'Walk-In';
        objCase.Status = 'Open';
        objCase.Pending_Amount__c = 530;
        objCase.DP_SOA_Balance__c = 530;
        insert objCase;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        HandoverController objClass = new HandoverController(sc);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockRPTermination(1) );
        objClass.init();
        objClass.crfAttachmentBody = EncodingUtil.base64Encode(Blob.valueOf('Test'));
        objClass.crfAttachmentName = 'CRF.txt';        
        objClass.createExcessAmountTask();
    }
}