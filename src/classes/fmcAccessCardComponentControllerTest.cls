@isTest
private class fmcAccessCardComponentControllerTest {

    static Booking_unit__c objBu;
    static Account objAcc;

    @testSetup
    static void createCommonTestData() {
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        objAcc = TestDataFactoryFM.createAccount();
        objAcc.party_ID__C = '1039032';
        insert objAcc;

        Id personAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account acctInsOwner=new Account(RecordTypeID = personAccountId,PersonEmail='a@g.com',FirstName = 'Test FName Owner',
                                    LastName = 'Test LName',Passport_Number__c='12345678',Mobile__c='1212121212',Address_Line_1__c='test');
        insert acctInsOwner;

        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        objSR = TestDataFactoryFM.createServiceRequest(objAcc);
        insert objSR;

        Booking__c objBooking = TestDataFactoryFM.createBooking(acctInsOwner, objSR);
        insert objBooking;

        objBu = TestDataFactoryFM.createBookingUnit(objAcc, objBooking);
        objBu.Property_City__c = 'Dubai';
        objBu.Tenant__c = objAcc.id;
        insert objBu;

        Property__c propObj = TestDataFactoryFM.createProperty();
        insert propObj;

        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Property_name__c = propObj.id;
        insert locObj;

        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;

         User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;

        SR_Attachments__c objDoc = new SR_Attachments__c();
        objDoc.Name = 'Test Doc';
        insert objDoc;
    }

    @isTest static void test_newSR() {
        PageReference myVfPage = Page.CommunityPortal;
        Test.setCurrentPage(myVfPage);
        objBu = [SELECT Id from Booking_Unit__c limit 1];
        objAcc = [SELECT Id from Account limit 1];
        System.debug('===objBu==='+objBu);
        ApexPages.currentPage().getParameters().put('view','AccessCards');
        ApexPages.currentPage().getParameters().put('UnitId',objBu.id);
        ApexPages.currentPage().getParameters().put('AccountId',objAcc.id);
        ApexPages.currentPage().getParameters().put('SRType','Request_for_Access_cards');
        fmcAccessCardComponentController controller = new fmcAccessCardComponentController();
        Test.startTest();
        controller.strSelectedUnit = objBu.id;
        controller.strAccountId = objAcc.id;
        controller.getUnitDetailsNew();
        Test.stopTest();
    }

    @isTest static void test_uploadDocument() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = [SELECT Id FROM User WHERE Profile.name = 'System Administrator' AND isActive = True limit 1];
        fmcAccessCardComponentController controller;
        //System.runAs(u) {
            PageReference myVfPage = Page.CommunityPortal;
            Test.setCurrentPage(myVfPage);
            objBu = [SELECT Id from Booking_Unit__c limit 1];
            objAcc = [SELECT Id from Account limit 1];
            System.debug('===objBu==='+objBu);
            ApexPages.currentPage().getParameters().put('view','AccessCards');
            ApexPages.currentPage().getParameters().put('UnitId',objBu.id);
            ApexPages.currentPage().getParameters().put('AccountId',objAcc.id);
            ApexPages.currentPage().getParameters().put('SRType','Request_for_Access_cards');

            Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
            controller = new fmcAccessCardComponentController();
            controller.strSelectedUnit = objBu.id;
            controller.strAccountId = objAcc.id;
            controller.createCaseShowUploadDoc();
            controller.strDocName = 'VfRemoting.Js';
            controller.strDocBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        //}
        Test.startTest();
        controller.strSelectedUnit = objBu.id;
        controller.strAccountId = objAcc.id;
        controller.uploadDocument();
        controller.saveAsDraft();
        Test.stopTest();
    }

    @isTest static void test_deleteDocument() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = [SELECT Id FROM User WHERE Profile.name = 'System Administrator' AND isActive = True limit 1];
        fmcAccessCardComponentController controller;
        //System.runAs(u) {
	        PageReference myVfPage = Page.CommunityPortal;
	        Test.setCurrentPage(myVfPage);
	        objBu = [SELECT Id from Booking_Unit__c limit 1];
	        objAcc = [SELECT Id from Account limit 1];
	        System.debug('===objBu==='+objBu);
	        ApexPages.currentPage().getParameters().put('view','AccessCards');
	        ApexPages.currentPage().getParameters().put('UnitId',objBu.id);
	        ApexPages.currentPage().getParameters().put('AccountId',objAcc.id);
	        ApexPages.currentPage().getParameters().put('SRType','Request_for_Access_cards');

	        Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
	        controller = new fmcAccessCardComponentController();
	        controller.strSelectedUnit = objBu.id;
	        controller.strAccountId = objAcc.id;
	        controller.createCaseShowUploadDoc();
	        controller.strDocName = 'VfRemoting.Js';
	        controller.strDocBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        //}
        Test.startTest();
        controller.strSelectedUnit = objBu.id;
        controller.strAccountId = objAcc.id;
        controller.uploadDocument();
        FM_Case__c obj = [SELECT Id from FM_Case__c LIMIT 1];
        SR_Attachments__c srAtt = [SELECT Id,Name from SR_Attachments__c LIMIT 1];
        controller.docIdToDelete = obj.Id+','+srAtt.Name;
        controller.deleteDocument();
        // controller.submitSR();
        Test.stopTest();
    }

    @isTest static void test_submitSR() {
        fmcAccessCardComponentController controller;
        PageReference myVfPage = Page.CommunityPortal;
        Test.setCurrentPage(myVfPage);
        objBu = [SELECT Id from Booking_Unit__c limit 1];
        objAcc = [SELECT Id from Account limit 1];
        System.debug('===objBu==='+objBu);
        ApexPages.currentPage().getParameters().put('view','AccessCards');
        ApexPages.currentPage().getParameters().put('UnitId',objBu.id);
        ApexPages.currentPage().getParameters().put('AccountId',objAcc.id);
        ApexPages.currentPage().getParameters().put('SRType','Request_for_Access_cards');
        Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
        controller = new fmcAccessCardComponentController();
        Test.startTest();
        controller.strSelectedUnit = objBu.id;
        controller.strAccountId = objAcc.id;
        controller.submitSR();
        Test.stopTest();
    }

     @isTest static void saveDraftSRTest() {
        fmcAccessCardComponentController controller;
        PageReference myVfPage = Page.CommunityPortal;
        Test.setCurrentPage(myVfPage);
        objBu = [SELECT Id from Booking_Unit__c limit 1];
        objAcc = [SELECT Id from Account limit 1];
        System.debug('===objBu==='+objBu);
        ApexPages.currentPage().getParameters().put('view','AccessCards');
        ApexPages.currentPage().getParameters().put('UnitId',objBu.id);
        ApexPages.currentPage().getParameters().put('AccountId',objAcc.id);
        ApexPages.currentPage().getParameters().put('SRType','Request_for_Access_cards');
        Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
        controller = new fmcAccessCardComponentController();
        Test.startTest();
        controller.strSelectedUnit = objBu.id;
        controller.strAccountId = objAcc.id;
        controller.saveAsDraft();
        Test.stopTest();
    }

    @isTest static void saveDraftSRTest2() {
        fmcAccessCardComponentController controller;
        PageReference myVfPage = Page.CommunityPortal;
        Test.setCurrentPage(myVfPage);
        objBu = [SELECT Id from Booking_Unit__c limit 1];
        objAcc = [SELECT Id from Account limit 1];

        FM_Case__c objFmCase = new FM_Case__c();
        objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().
                                    get('Request For Access Card').getRecordTypeId();
        objFMCase.Account__c = objAcc.id;
        objFMCase.Booking_Unit__c = objBu.id;
        objFMCase.Status__c='Draft Request';
        insert objFMCase;

        System.debug('===objBu==='+objBu);
        ApexPages.currentPage().getParameters().put('view','AccessCards');
        ApexPages.currentPage().getParameters().put('id',objFMCase.id);
        ApexPages.currentPage().getParameters().put('UnitId',objBu.id);
        ApexPages.currentPage().getParameters().put('AccountId',objAcc.id);
        ApexPages.currentPage().getParameters().put('SRType','Request_for_Access_cards');
        Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
        controller = new fmcAccessCardComponentController();
        Test.startTest();
        controller.strSelectedUnit = objBu.id;
        controller.strAccountId = objAcc.id;
        controller.saveAsDraft();
        Test.stopTest();
    }
}