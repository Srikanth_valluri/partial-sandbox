public class Damac_AmeyoLeadScore implements Database.Batchable<sObject> {   
    
    public Database.QueryLocator start(Database.BatchableContext batchContext){
        
        Set<String> EventTypes = new Set<String>{'Ameyo Dialer','Ameyo Manual'};
        return Database.getQueryLocator([SELECT Total_Talk_Time__c,Total_Connects__c,Total_Attempts__c ,
                                            (SELECT Sub_Disposition_Type__c,Ameyo_Dialer_Disposition__c,Call_Start_Time__c,
                                             Call_End_Time__c ,CallDurationInSeconds FROM Tasks WHERE Event_Type__c IN : EventTypes) 
                                            FROM Inquiry__c WHERE RecordType.Name = 'Pre Inquiry']);
    }
    public void execute(Database.BatchableContext batchContext, List<Inquiry__c> scope){
        for(Inquiry__c eachInquiry : scope){
            List<Task> eventsForInquiry = new List <Task> ();
            if (!eachInquiry.Tasks.isEmpty ()) {
                eventsForInquiry = eachInquiry.Tasks;
            }
            
            Decimal totalConnects = 0;
            Decimal totalTalkTime = 0;
            if(eventsForInquiry != null && !eventsForInquiry.isEmpty()){
               eachInquiry.Total_Attempts__c = eventsForInquiry.size();
               for(Task eachEvent : eventsForInquiry){
                   if(eachEvent.Ameyo_Dialer_Disposition__c == 'CONNECTED' && eachEvent.Sub_Disposition_Type__c != 'Redirected to Voicemail'){
                       if(eachEvent.Call_Start_Time__c != null && eachEvent.Call_End_Time__c != null){
                            Decimal taskStartDate = eachEvent.Call_Start_Time__c.getTime();
                            Decimal taskEndDate = eachEvent.Call_End_Time__c.getTime();
                            Decimal milliseconds = taskEndDate - taskStartDate;
                            Decimal seconds = milliseconds / 1000;
                            Decimal minutes = seconds / 60;
                            totalTalkTime = totalTalkTime + minutes ;

                       }
                       totalConnects += 1;    
                   }
               }
            }else{
                eachInquiry.Total_Attempts__c = 0;
            }
            eachInquiry.Total_Talk_Time__c = totalTalkTime ;
            eachInquiry.Total_Connects__c = totalConnects ;
        }
        DAMAC_Constants.skip_InquiryTrigger = true;
        update scope;     
    }
   
    public void finish(Database.BatchableContext BC){
    }
        
}