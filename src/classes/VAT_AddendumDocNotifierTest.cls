/**
 * @File Name          : VAT_AddendumDocNotifierTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 2/6/2020, 5:48:10 PM
 * @Modification Log   : 
 * Ver       Date            Author              Modification
 * 1.0    2/6/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
public class VAT_AddendumDocNotifierTest {

    static testMethod  void callConstructor() {                         
        
        Account objAccount = TestDataFactoryFM.createAccount();
        objAccount.Email__c = 'test@test.com';
        objAccount.Email__pc = 'test@test.com';
        objAccount.Email_1__c = 'test@test.com';
        insert objAccount;
        System.debug('-->> objAccount: ' + objAccount);
        System.debug('-->> objAccount.Id: ' + objAccount.Id);
        System.debug('-->> objAccount.IsPersonAccount: ' + objAccount.IsPersonAccount);
        System.debug('-->> objAccount.Person_Business_Email__c: ' + objAccount.Person_Business_Email__c);

        Account objAccount_query = [
            SELECT Id
                 , Person_Business_Email__c 
              FROM Account 
             WHERE Id = :objAccount.Id
        ];
        System.debug('-->> objAccount_query ## : ' + objAccount_query );
        System.debug('-->> objAccount_query Person_Business_Email__c ## : ' + objAccount_query.Person_Business_Email__c );


        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAccount);
        insert objSR;

        Location__c objLocation = TestDataFactoryFM.createLocation();
        insert objLocation;

        Property__c objProperty = TestDataFactoryFM.createProperty();
        insert objProperty;

        Inventory__c objInventory = TestDataFactoryFM.createInventory(objLocation);
        objInventory.Seller_Name__c = 'Test Seller';
        insert objInventory;

        Booking__c objBooking = TestDataFactoryFM.createBooking(objAccount, objSR);
        insert objBooking;
        System.debug('-->> objBooking.Account__c: ' + objBooking.Account__c);
        
        Booking_Unit__c objBooking_Unit = TestDataFactoryFM.createBookingUnit(objAccount, objBooking);
        //objBooking_Unit.Unit_Name__c = 'Test Unit';
        objBooking_Unit.VAT_Addendum_Template__c = 'HO VAT Addendum';
        objBooking_Unit.VAT_Email_Sent__c = false;
        objBooking_Unit.VAT_addendum_Email_Sent_Date__c = System.Today();
        objBooking_Unit.Requested_Price_AED__c = 100;
        objBooking_Unit.Property_Name__c = 'Test Property';
        objBooking_Unit.Inventory__c = objInventory.Id;
        insert objBooking_Unit;


        Attachment attach1=new Attachment();       
        attach1.Name='HO VAT Addendum';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach1.body=bodyBlob;
        attach1.parentId=objBooking_Unit.id;
        insert attach1;

        Attachment attach=new Attachment();       
        attach.Name='Handed Over Cover Letter';
        
        attach.body=bodyBlob;
        attach.parentId=objBooking_Unit.id;
        insert attach;

        System.debug('-->> objBooking_Unit: ' + objBooking_Unit);
        System.debug('-->> objBooking_Unit.Booking__r.Account__r.Person_Business_Email__c: ' + objBooking_Unit.Booking__r.Account__r.Person_Business_Email__c);
        System.debug('-->> objBooking_Unit.Booking__r.Account__c: ' + objBooking_Unit.Booking__r.Account__c);
        System.debug('-->> objBooking_Unit.Booking__c: ' + objBooking_Unit.Booking__c);
        
        Test.startTest();

            List<Id> listOfBUIds = new List<Id>();
            listOfBUIds.add(objBooking_Unit.Id);
            VAT_AddendumDocNotifier.sendAddendumDoc(listOfBUIds);

            FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');
            Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {'https://api.sendgrid.com/v3/mail/send' => sendGridResponse };
            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
            
        Test.stopTest();

    }
        

}