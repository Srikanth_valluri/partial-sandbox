@isTest
private class AgencyPCManagement_Test {
    public static Map<String, Profile> profileInfo = new Map<String, Profile>();

    static{
        profileInfo = DamacUtility.getProfileDetails(new Set<String>{
                                                        'Head of Sales',
                                                        'Director of Sales',
                                                        'Property Consultant'});
    }

    static User getHOS() {
        User  HOS = new User();
        HOS.FirstName = 'test';
        HOS.LastName = 'HOS';
        HOS.ProfileId = profileInfo.get('Head of Sales').Id;
        HOS.email = 'testhos@gmail.com';
        HOS.alias = 'testhos1';
        HOS.emailencodingkey='UTF-8';
        HOS.languagelocalekey='en_US';
        HOS.localesidkey='en_US';
        HOS.country='United Arab Emirates';
        HOS.IsActive =true;
        HOS.timezonesidkey='America/Los_Angeles';
        HOS.username='testhos@account.com';
        HOS.Languages_Known__c='English;Arabic;Hindi;French';
        insert HOS;

        return HOS;
    }

    static User getDOS(String ManagerId) {
        User  DOS = new User();
        DOS.FirstName = 'test';
        DOS.LastName = 'DOS';
        DOS.ProfileId = profileInfo.get('Director of Sales').Id;
        DOS.email = 'testdos@gmail.com';
        DOS.ManagerId = ManagerId;
        DOS.alias = 'testdos2';
        DOS.emailencodingkey='UTF-8';
        DOS.languagelocalekey='en_US';
        DOS.localesidkey='en_US';
        DOS.country='United Arab Emirates';
        DOS.IsActive =true;
        DOS.timezonesidkey='America/Los_Angeles';
        DOS.username='testdos@account.com';
        DOS.Languages_Known__c='English;Arabic;Hindi;French';
        insert DOS;

        return DOS;
    }

    static User getPC(String ManagerId) {

        User  PC = new User();
        PC.FirstName = 'test';
        PC.LastName = 'PC';
        PC.ProfileId = profileInfo.get('Property Consultant').Id;
        PC.email = 'testpc@gmail.com';
        PC.ManagerId = ManagerId;
        PC.alias = 'testpc2';
        PC.emailencodingkey='UTF-8';
        PC.languagelocalekey='en_US';
        PC.localesidkey='en_US';
        PC.country='United Arab Emirates';
        PC.IsActive =true;
        PC.timezonesidkey='America/Los_Angeles';
        PC.username='testpc@account.com';
        PC.Languages_Known__c='English;Arabic;Hindi;French';
        insert PC;

        return PC;
    }

    static testMethod void testAddPC() {
        List<Contact> accContacts = new List<Contact>();
        List<Agency_PC__c> accAgencyPC = new List<Agency_PC__c>();

        User hos = getHOS();

        User dos = getDOS(hos.Id);

        User pc = getPC(dos.Id);

        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.OwnerId = pc.Id;
        acc.Agency_Type__c = 'Corporate';
        acc.recordtypeId = DamacUtility.getRecordTypeId('Account',Label.Account_Corporate_RecordType);
        insert acc;

        for(Integer i=0;i<4;i++) {
            Contact con = new Contact();
            con.FirstName = 'Test';
            con.LastName = 'Contact '+ i;
            con.AccountId = acc.Id;
            accContacts.add(con);
        }
        insert accContacts;

        for(Integer i=0;i<4;i++) {
            Agency_PC__c agencyPc = new Agency_PC__c();
            agencyPc.Agency__c = acc.Id;
            agencyPc.Contact__c = accContacts[i].Id;
            agencyPc.User__c = pc.Id;
            accAgencyPC.add(agencyPc);
        }
        insert accAgencyPC;

        test.startTest();
        AgencyPCManagement_Controller ctrl = new AgencyPCManagement_Controller();
        ctrl.getBrokerClassLst();
        ctrl.loggedInUserProfile = 'Agent Admin Team';
        ctrl.selAgencyName = 'Test Account';
        ctrl.selAgencyType = 'Corporate';
        //ctrl.ownerType = 'Primary Owner';
        ctrl.selAgencyContactName = accContacts[1].Id;
        ctrl.selAgencyPC = dos.Id;
        ctrl.selAgencyPC2 = dos.Id;
        ctrl.selAgencyPC3 = dos.Id;
        ctrl.btnAction = 'AssignPCs';
        ctrl.wrpRecAgency = acc.Id;
        ctrl.wrpRecContact = accContacts[0].Id;
        ctrl.searchAccounts();
        ctrl.nextBtn();
        ctrl.LastBtn();
        ctrl.previousBtn();
        ctrl.buttonActions();
        ctrl.getPCTeam();
        ctrl.getPCTeam2();
        ctrl.getPCTeam3();
        ctrl.refreshPage();
        ctrl.cancelPage();
        ctrl.clearUser();
        ctrl.reasonForTermination  = 'Testing';
        ctrl.submitTermination ();
        try {
            ctrl.createContact (null);
        } catch (Exception e) {}
        ctrl.cancelBrokerUpdate();
        test.stopTest();
    }

    static testMethod void testBrokerClassUpdate() {
        List<Contact> accContacts = new List<Contact>();
        List<Agency_PC__c> accAgencyPC = new List<Agency_PC__c>();
        PC_Console_Users__c objusrconsole = new PC_Console_Users__c();
        objusrconsole.Name = 'Eternuss Eternuss';
        objusrconsole.Operator__c = 'All';
        objusrconsole.Country_of_Incorporation__c = 'India';
        insert objusrconsole;
        User hos = getHOS();

        User dos = getDOS(hos.Id);

        User pc = getPC(dos.Id);

        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.OwnerId = pc.Id;
        acc.Agency_Type__c = 'Corporate';
        acc.recordtypeId = DamacUtility.getRecordTypeId('Account',Label.Account_Corporate_RecordType);
        insert acc;
        list<account> acList = new list<account>();
        acList.add(acc);
        for(Integer i=0;i<10;i++) {
            Contact con = new Contact();
            con.FirstName = 'Test';
            con.LastName = 'Contact '+ i;
            con.AccountId = acc.Id;
            accContacts.add(con);
        }
        insert accContacts;

        for(Integer i=0;i<10;i++) {
            Agency_PC__c agencyPc = new Agency_PC__c();
            agencyPc.Agency__c = acc.Id;
            agencyPc.Contact__c = accContacts[i].Id;
            agencyPc.User__c = pc.Id;
            accAgencyPC.add(agencyPc);
        }
        insert accAgencyPC;

        test.startTest();
        AgencyPCManagement_Controller ctrl = new AgencyPCManagement_Controller();
        ctrl.loggedInUserProfile = 'Agent Executive Manager';
        
        ctrl.selAgencyType = 'Corporate';
        ctrl.currentConsoleUser = objusrconsole;
        
        ctrl.btnAction = 'ChangeBroker';
        //ctrl.btnAction = 'TransferAccount';
        ctrl.ownerType = 'Primary Owner';
        ctrl.wrpRecAgency = acc.Id;
        ctrl.selectedBrokerClass = 'A+';
        //ctrl.selAgencyPC2 = dos.Id;
        ctrl.searchAccounts();
        ctrl.getTotalRecords();
        ctrl.firstBtn();
        for(AgencyPCManagement_Controller.agencyPCWrapper wrpObj :ctrl.wrpLstToShow) {
            wrpObj.isBrokerAccountChecked = true;
        }
        ctrl.getTotalRecords();
        try {
            ctrl.previousBtn();    
        } catch (Exception e) {}
        ctrl.LastBtn();
        ctrl.getPCTeam();
        for(AgencyPCManagement_Controller.agencyPCWrapper wrpObj :ctrl.wrpLstToShow) {
            wrpObj.isBrokerAccountChecked = true;
        }
        ctrl.buttonActions();
       
        test.stopTest();
    }
    static testMethod void testTransfreAccount() {
        List<Contact> accContacts = new List<Contact>();
        List<Agency_PC__c> accAgencyPC = new List<Agency_PC__c>();
        PC_Console_Users__c objusrconsole = new PC_Console_Users__c();
        objusrconsole.Name = 'Eternuss Eternuss';
        objusrconsole.Operator__c = 'All';
        objusrconsole.Country_of_Incorporation__c = 'India';
        insert objusrconsole;
        User hos = getHOS();

        User dos = getDOS(hos.Id);

        User pc = getPC(dos.Id);

        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.OwnerId = pc.Id;
        acc.Agency_Type__c = 'Corporate';
        acc.recordtypeId = DamacUtility.getRecordTypeId('Account',Label.Account_Corporate_RecordType);
        insert acc;
        list<account> acList = new list<account>();
        acList.add(acc);
        for(Integer i=0;i<10;i++) {
            Contact con = new Contact();
            con.FirstName = 'Test';
            con.LastName = 'Contact '+ i;
            con.AccountId = acc.Id;
            accContacts.add(con);
        }
        insert accContacts;

        for(Integer i=0;i<10;i++) {
            Agency_PC__c agencyPc = new Agency_PC__c();
            agencyPc.Agency__c = acc.Id;
            agencyPc.Contact__c = accContacts[i].Id;
            agencyPc.User__c = pc.Id;
            accAgencyPC.add(agencyPc);
        }
        insert accAgencyPC;

        test.startTest();
        AgencyPCManagement_Controller ctrl = new AgencyPCManagement_Controller();
        ctrl.loggedInUserProfile = 'Agent Executive Manager';
        //ctrl.selHOS = hos.Id;
        //ctrl.selDOS = dos.Id;
        ctrl.selAgencyType = 'Corporate';
        ctrl.currentConsoleUser = objusrconsole;
        //ctrl.selUserName = pc.Id;
        ctrl.btnAction = 'TransferAccount';
        ctrl.btnAction = 'TransferAccount';
        ctrl.ownerType = 'Primary Owner';
        ctrl.wrpRecAgency = acc.Id;
        ctrl.selAgencyPC = dos.Id;
        ctrl.selAgencyPC2 = dos.Id;
        //ctrl.selAgencyPC3 = dos.Id;
        ctrl.searchAccounts();
        ctrl.getTotalRecords();
        ctrl.firstBtn();
        for(AgencyPCManagement_Controller.agencyPCWrapper wrpObj :ctrl.wrpLstToShow) {
            wrpObj.isAccountChecked = true;
        }
        ctrl.getTotalRecords();
        //ctrl.selAgencyPC = pc.Id;
        //ctrl.nextBtn();
        try {
            ctrl.previousBtn();
        } catch (Exception e) {}
        ctrl.LastBtn();
        ctrl.getPCTeam();
        for(AgencyPCManagement_Controller.agencyPCWrapper wrpObj :ctrl.wrpLstToShow) {
            wrpObj.isAccountChecked = true;
        }
        ctrl.buttonActions();
        /*ctrl.ownerType = 'Secondary Owner';
        
        ctrl.buttonActions();*/
        //ctrl.sendOwnerChangeEmails(acList,pc,hos,'priChanged');
        test.stopTest();
    }

    static testMethod void testAddAllPC() {
        List<Contact> accContacts = new List<Contact>();
        List<Agency_PC__c> accAgencyPC = new List<Agency_PC__c>();

        User hos = getHOS();

        User dos = getDOS(hos.Id);

        User pc = getPC(dos.Id);

        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.OwnerId = pc.Id;
        acc.Agency_Type__c = 'Corporate';
        acc.recordtypeId = DamacUtility.getRecordTypeId('Account',Label.Account_Corporate_RecordType);
        insert acc;
        
        Contact con = new Contact();
        con.FirstName = 'Test';
        con.LastName = 'Contact '+ 1;
        con.AccountId = acc.Id;
        accContacts.add(con);
        insert accContacts;

        test.startTest();
        AgencyPCManagement_Controller ctrl = new AgencyPCManagement_Controller();
        ctrl.loggedInUserProfile = 'Agent Admin Team';
        ctrl.selAgencyName = 'Test Account';
        ctrl.selAgencyType = 'Corporate';
        //ctrl.ownerType = 'Secondary Owner';
        ctrl.selAgencyPC = dos.Id;
        ctrl.btnAction = 'AddAllPCs';
        ctrl.wrpRecAgency = acc.Id;
        ctrl.wrpRecContact = accContacts[0].Id;
        ctrl.searchAccounts();
        ctrl.buttonActions();
        
        test.stopTest();
    }

    static testMethod void testTransferPCs() {
        List<Contact> accContacts = new List<Contact>();
        List<Agency_PC__c> accAgencyPC = new List<Agency_PC__c>();

        User hos = getHOS();

        User dos = getDOS(hos.Id);

        User pc = getPC(dos.Id);

        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.OwnerId = pc.Id;
        acc.Agency_Type__c = 'Corporate';
        acc.recordtypeId = DamacUtility.getRecordTypeId('Account',Label.Account_Corporate_RecordType);
        insert acc;

        for(Integer i=0;i<4;i++) {
            Contact con = new Contact();
            con.FirstName = 'Test';
            con.LastName = 'Contact '+ i;
            con.AccountId = acc.Id;
            accContacts.add(con);
        }
        insert accContacts;

        for(Integer i=0;i<4;i++) {
            Agency_PC__c agencyPc = new Agency_PC__c();
            agencyPc.Agency__c = acc.Id;
            agencyPc.Contact__c = accContacts[i].Id;
            agencyPc.User__c = pc.Id;
            accAgencyPC.add(agencyPc);
        }
        insert accAgencyPC;

        test.startTest();
        AgencyPCManagement_Controller ctrl = new AgencyPCManagement_Controller();
        ctrl.loggedInUserProfile = 'Agent Admin Team';
        ctrl.selAgencyName = 'Test Account';
        ctrl.selAgencyType = 'Corporate';
        ctrl.selAgencyPC = dos.Id;
        ctrl.btnAction = 'TransferPCs';
        //ctrl.ownerType = 'Secondary Owner';
        ctrl.wrpRecAgency = acc.Id;
        ctrl.wrpRecContact = accContacts[0].Id;
        ctrl.searchAccounts();
        for(AgencyPCManagement_Controller.agencyPCWrapper wrpObj :ctrl.wrpLstToShow) {
            wrpObj.isPCChecked = true;
        }
        ctrl.buttonActions();
        test.stopTest();
    }

    static testMethod void testRemovePCs() {
        List<Contact> accContacts = new List<Contact>();
        List<Agency_PC__c> accAgencyPC = new List<Agency_PC__c>();

        User hos = getHOS();

        User dos = getDOS(hos.Id);

        User pc = getPC(dos.Id);

        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.OwnerId = pc.Id;
        acc.Agency_Type__c = 'Corporate';
        acc.recordtypeId = DamacUtility.getRecordTypeId('Account',Label.Account_Corporate_RecordType);
        insert acc;

        Contact con = new Contact();
        con.FirstName = 'Test';
        con.LastName = 'Contact '+ 1;
        con.AccountId = acc.Id;
        accContacts.add(con);

        insert accContacts;

        Agency_PC__c agencyPc = new Agency_PC__c();
        agencyPc.Agency__c = acc.Id;
        agencyPc.Contact__c = accContacts[0].Id;
        agencyPc.User__c = pc.Id;
        accAgencyPC.add(agencyPc);
        insert accAgencyPC;

        test.startTest();
        AgencyPCManagement_Controller ctrl = new AgencyPCManagement_Controller();
        ctrl.loggedInUserProfile = 'Agent Admin Team';
        ctrl.selAgencyName = 'Test Account';
        ctrl.selAgencyType = 'Corporate';
        ctrl.btnAction = 'RemovePCs';
        //ctrl.ownerType = 'Secondary Owner';
        ctrl.wrpRecAgency = acc.Id;
        ctrl.wrpRecContact = accContacts[0].Id;
        ctrl.searchAccounts();
        ctrl.getBrokerClassLst();
        try {
        AgencyPCManagement_Controller.getIdListForQuery (null);
        }catch (Exception e) {}
        for(AgencyPCManagement_Controller.agencyPCWrapper wrpObj :ctrl.wrpLstToShow) {
            wrpObj.isPCChecked = true;
        }
        ctrl.buttonActions();
        test.stopTest();
    }
    
}