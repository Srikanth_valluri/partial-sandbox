@isTest
public class MyReceiptsControllerTest{

    @TestSetup
    static void customSettingData() {

        insert new IpmsRestServices__c(
                   SetupOwnerId = UserInfo.getOrganizationId(),
                   BaseUrl__c = 'http://83.111.194.181:8045/webservices/rest',
                   Username__c = 'oracle_user',
                   Password__c = 'crp1user',
                   Client_Id__c = '8MABLQM-KJ8I8-1XA58-WWCM1S1',
                   BearerToken__c = 'eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWyiwuVrJSKs5IBKK8bCUdpczEEiUrQ1MzAwtTMwsDIx2l1IoCqIC5oRFIoLQ4tSgvMTcVqM_C19HJJ9BX19vLwtNC1zDC0dRCNzzc2dcw2FCpFgBXRb-1XQAAAA.6Ym224Vwr9AniBeq6gL8OM9u4vGnUB_vbEUVjWojg14',
                   Timeout__c = 120000
                   );
    }

    @isTest
    public static void testfetchMyReceipts() {

        User communityUser = CommunityTestDataFactory.createPortalUser();
        System.debug('User = ' + [SELECT Id, Name, AccountId, ContactId, Account.Party_ID__c, Contact.AccountId, Contact.Account.Party_ID__c FROM User WHERE Id = :communityUser.Id]);

        SOAPCalloutServiceMock.returnToMe = new Map<String, ReceiptDetails1.GetReceiptDetailsResponse_element>();
        ReceiptDetails1.GetReceiptDetailsResponse_element response1 = new ReceiptDetails1.GetReceiptDetailsResponse_element();
        response1.return_x = '{"data":[{"ATTRIBUTE3":"1040788","ATTRIBUTE10":"118265456","ATTRIBUTE2":"CASH","ATTRIBUTE1":"943590","ATTRIBUTE14":"75017","ATTRIBUTE13":"Y","ATTRIBUTE12":"13-DEC-2015","ATTRIBUTE11":"CASH","ATTRIBUTE9":"5000","ATTRIBUTE8":"5000","ATTRIBUTE7":"0","ATTRIBUTE6":"5000","ATTRIBUTE5":"AED","ATTRIBUTE4":"RAGINI RAJAN PATIL","ATTRIBUTE18":null,"ATTRIBUTE17":"DH/32/3204 - Token amount paid by cash","ATTRIBUTE16":"Cheque/Cash to be Remitted","ATTRIBUTE15":null,"ATTRIBUTE19":null}],"message":"[16 Receipts found For Given PartyID]","status":"S"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );

        Object myReciepts;

        Test.startTest();
        System.runAs(communityUser) {
            myReciepts = MyReceiptsController.fetchMyReceipts();
        }
        Test.stopTest();

        System.assertNotEquals(NULL, myReciepts);

    }

    @isTest
    public static void testfetchMyReceiptsNegative() {
        Object myReciepts;
        User communityUser = CommunityTestDataFactory.createPortalUser();
        communityUser = [SELECT Id, AccountId FROM User WHERE Id = :communityUser.Id];
        update new Account(Id = communityUser.AccountId, Party_ID__c = NULL);

        Test.startTest();

        System.runAs(communityUser) {
            myReciepts = MyReceiptsController.fetchMyReceipts();
        }
        Test.stopTest();

        System.assertEquals(NULL, MyReceiptsController.fetchMyReceipts());
    }

    @isTest
    public static void testGenerateURL() {

        FmHttpCalloutMock.Response getAllReceiptUrlResponse = new FmHttpCalloutMock.Response(200, 'Success', '   {  '  +
         '       "responseId": "3072019-10305298",  '  +
         '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         '       "status": "S",  '  +
         '       "responseMessage": "Process Completed",  '  +
         '       "elapsedTimeMs": 8497,  '  +
         '       "responseLines": [  '  +
         '           {  '  +
         '               "documentName": "DPSOA",  '  +
         '               "language": "EN",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
         '           },  '  +
         '           {  '  +
         '               "documentName": "DPSOA_AR",  '  +
         '               "language": "AR",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
         '           }  '  +
         '       ],  '  +
         '       "complete": true  '  +
         '  }  ');

        String DP_RECEIPT_ID_str = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.DP_RECEIPT_ID, new List<String> {'12345'});

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
            DP_RECEIPT_ID_str => getAllReceiptUrlResponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
        Test.startTest();
        MyReceiptsController.GenerateURL('12345');
        Test.stopTest();
    }
}