@isTest
public class RentalPoolAgreementEligibilityTest {
    @isTest static void instanceForClass_EligibleForRentalPool_element() {
        Test.startTest();
        RentalPoolAgreementEligibility.EligibleForRentalPool_element EligibleForRentalPool_element_Obj = 
            new RentalPoolAgreementEligibility.EligibleForRentalPool_element();
        Test.stopTest();
    }

    @isTest static void instanceForClass_EligibleForRentalPoolResponse_element() {
        Test.startTest();
        RentalPoolAgreementEligibility.EligibleForRentalPoolResponse_element EligibleForRentalPoolResponse_element_Obj = 
            new RentalPoolAgreementEligibility.EligibleForRentalPoolResponse_element();
        Test.stopTest();
    }

    @isTest static void instanceForClass_EligibleForRentalPoolRuleHttpSoap11Endpoint() {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(1));
        RentalPoolAgreementEligibility.EligibleForRentalPoolRuleHttpSoap11Endpoint EligibleForRentalPoolRuleHttpSoap11Endpoint_Obj = 
            new RentalPoolAgreementEligibility.EligibleForRentalPoolRuleHttpSoap11Endpoint();
        EligibleForRentalPoolRuleHttpSoap11Endpoint_Obj.EligibleForRentalPool('String RegistrationId','String ProcessName','String SubProcessName','String ProjectCity','String Project','String BuildingCode','String PermittedUse','String BedroomType','String ApplicableUnits','String UnitType','String CustomerType','String ModeofRequest','String AggrementStatus','String UnderTermination','String UnderAssignment');
        Test.stopTest();
    }
}