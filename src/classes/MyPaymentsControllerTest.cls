@isTest
private class MyPaymentsControllerTest {
    @testSetup static void setup() {
        User communityUser = CommunityTestDataFactory.CreatePortalUser();
        communityUser = [SELECT Id, Name, AccountId, ContactId FROM User WHERE Id = :communityUser.Id];
        System.debug('communityUser.AccountId = ' + communityUser.AccountId);
        NSIBPM__Service_Request__c dealSr = TestDataFactory_CRM.createServiceRequest();
        insert dealSr;
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(communityUser.AccountId, dealSr.Id, 1);
        insert lstBooking;
        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;
        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        insert inventory;
        List<Booking_Unit__c> lstUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 4);
        for (Booking_Unit__c unit : lstUnit) {
            unit.Inventory__c = inventory.Id;
            unit.Token_Paid__c = true;
        }
        insert lstUnit;
        List<Payment_Terms__c> lstPaymentTerms = new List<Payment_Terms__c>();
        for (Booking_Unit__c unit : lstUnit) {
            for (Integer i = 1; i <= 3; i++) {
                lstPaymentTerms.add(new Payment_Terms__c(
                    Payment_Amount__c = String.valueOf(i * 100),
                    Payment_Date__c = Date.today().addDays(i),
                    Booking_Unit__c = unit.Id
                ));
            }
        }
        insert lstPaymentTerms;
    }

    @isTest
    static void testController() {
        User communityUser = [  SELECT      Id, Name, AccountId FROM User 
                                WHERE       Profile.Name = :CommunityTestDataFactory.COMMUNITY_USER_PROFILE
                                ORDER BY    CreatedDate DESC
                                LIMIT       1];
        Booking_Unit__c unit = [SELECT Id FROM Booking_Unit__c ORDER BY CreatedDate DESC LIMIT 1];
        System.runAs(communityUser) {
            MyPaymentsController controller = new MyPaymentsController();
            controller.personAccountId = NULL;
            controller.personAccountId = communityUser.AccountId;
            controller.UnitId = unit.Id;
            MyPaymentsController.retrievePaymentTerms(NULL);
        }
    }
}