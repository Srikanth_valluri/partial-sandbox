public  class UnitAssignmentTriggerHandler implements TriggerFactoryInterface{
    
    //Before Trigger Methods
    public void executeBeforeInsertTrigger(list<sObject> lstNewRecords) {
       
    }
    public void executeBeforeUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){}
    public void executeBeforeDeleteTrigger(Map<Id, sObject> mapOldRecords){}
    public void executeBeforeInsertUpdateTrigger(list<sObject> lstNewRecords, Map<Id,sObject> mapOldRecords){}
    
    //After Trigger Methods
    public void executeAfterInsertTrigger(Map<Id, sObject> mapNewRecords){}
    
    public void executeAfterUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){
        List <Unit_Assignment__c> unitAssignmentNewList = mapNewRecords.values();
        
        Set <ID> unitAssignmentIds = new Set <ID> ();
        List <ID> rejectedUnits = new List <ID> ();

        for (Unit_Assignment__c newUnitAssignment : unitAssignmentNewList) {
            Unit_Assignment__c oldUnitAssignment = (Unit_Assignment__c) mapOldRecords.get(newUnitAssignment.Id);
            
            if (oldUnitAssignment.Status__c == 'PC Submitted'
                && newUnitAssignment.Status__c == 'HOD Approved') {
                    unitAssignmentIds .add (newUnitAssignment.id);
                }
                
            if (oldUnitAssignment.Status__c != 'Rejected'
                && newUnitAssignment.Status__c == 'Rejected') {
                    rejectedUnits.add (newUnitAssignment.id);
                }
                
            if (oldUnitAssignment.Status__c == 'HOD Approved'
                && newUnitAssignment.Status__c == 'Vikram Approved') {
                    unitAssignmentIds.add (newUnitAssignment.id);
                }
                
            if (oldUnitAssignment.PC_Approval_Status__c == 'Reassign'
                && newUnitAssignment.PC_Approval_Status__c == 'Confirmed') {
                    unitAssignmentIds.add (newUnitAssignment.id);
                }
                
                
        }
        
        /* To Create Inventory User for UA at first Approval */
        Map <Id, Unit_Assignment__c> unitAssignmentsMap = new Map <Id,Unit_Assignment__c>();
        unitAssignmentsMap = new Map <Id,Unit_Assignment__c>([SELECT Name, CreatedBy.IPMS_Employee_ID__c,
                                                              createdBy.ManagerId,
                                                              CreatedBy.Manager.IPMS_Employee_ID__c, 
                                                              agency__r.Vendor_ID__c, Reason_For_Unit_Assignment__c 
                                                              FROM Unit_Assignment__c WHERE ID IN :unitAssignmentIds ]);
        List<Inventory_User__c> invUserList = new List <Inventory_User__c> ();
        List<Inventory__c> inventoriesList = new List <Inventory__c> ();
        Map <Unit_Assignment__c, List<Inventory__c>> unitAssignmentAndInventoriesMap = new Map <Unit_Assignment__c,List<Inventory__c>>();
        Set <ID> activatedUAs = new Set <ID > ();
        
        for (Inventory__c eachInv: [SELECT 
                                    Inventory_ID__c, Unit_Assignment__c,
                                    Unit_Assignment_Status__c,
                                    Unit_Assignment__r.Name,
                                    Unit_Assignment__r.PC__c,
                                    Unit_Assignment__r.Active__c,
                                    Unit_Assignment__r.Deal_value__c,
                                    Unit_Assignment__r.Approved__c,
                                    Unit_Assignment__r.Status__c,
                                    Unit_Assignment__r.PC_Approval_Status__c 
                                    FROM
                                    Inventory__c
                                    WHERE
                                    Unit_Assignment__c IN: unitAssignmentIds]) 
        {
            
            // On the Final Approval of Vikram 
            if (eachInv.Unit_Assignment__r.Status__c == 'Vikram Approved' && eachInv.Unit_Assignment__r.PC_Approval_Status__c == 'Confirmed') {
                eachInv.Tagged_To_Unit_Assignment__c  = TRUE;
                eachInv.Unit_Assignment_Status__c = Label.UA_Status+' '+eachInv.Unit_Assignment__r.Name;
                inventoriesList.add (eachInv);
                activatedUAs.add (eachInv.Unit_Assignment__c);
                
            }
            
            // This will happens only in First Approval OR If vikram Reassigns the UA
            if (eachInv.Unit_Assignment__r.Status__c == 'HOD Approved' ) 
               
            {
                /* Modified By : Srikanth Valluri
                * Description : Adding Inventory Users for the selected Inventory 
                * Modified on : 16/02/2018
                */
                
                Inventory_User__c invUser = new Inventory_User__c ();    
                invUser.Inventory__c = eachInv.Id;
                invUser.User__c = eachInv.Unit_Assignment__r.Pc__c;
                invUser.Unit_Assignment__c = eachInv.Unit_Assignment__c ;
                invUser.Unique_Key__c = eachInv.Id+'###'+eachInv.Unit_Assignment__r.Pc__c;
                invUserList.add (invUser);
                
                // To send UA to IPMS
                if (unitAssignmentsMap.containsKey (eachInv.Unit_Assignment__c)) {
                    if (unitAssignmentAndInventoriesMap.containsKey(unitAssignmentsMap.get (eachInv.Unit_Assignment__c))) {
                        unitAssignmentAndInventoriesMap.get(unitAssignmentsMap.get (eachInv.Unit_Assignment__c)).add(eachInv);
                    } else {
                        unitAssignmentAndInventoriesMap.put(unitAssignmentsMap.get (eachInv.Unit_Assignment__c), new List<Inventory__c>{eachInv});
                    }     
                } 
            } 
            
            if ( eachInv.Unit_Assignment__r.Status__c == 'Vikram Approved' && eachInv.Unit_Assignment__r.PC_Approval_Status__c == 'Reassign') {
                if (unitAssignmentsMap.containsKey (eachInv.Unit_Assignment__c)) {
                    if (unitAssignmentAndInventoriesMap.containsKey(unitAssignmentsMap.get (eachInv.Unit_Assignment__c))) {
                        unitAssignmentAndInventoriesMap.get(unitAssignmentsMap.get (eachInv.Unit_Assignment__c)).add(eachInv);
                    } else {
                        unitAssignmentAndInventoriesMap.put(unitAssignmentsMap.get (eachInv.Unit_Assignment__c), new List<Inventory__c>{eachInv});
                    }     
                }
            }
        }
        if (activatedUAs.size () > 0)
            updateValidateDate (activatedUAs);
        
        
        // On Final Approval of UA
        if (inventoriesList.size () > 0)
            Update inventoriesList;
        // Deactivating the UA when second Approver is rejected the UA, which will happen single at a time
        if (rejectedUnits.size () > 0) {
            ID jobID = System.enqueueJob(new AsyncDeactivateUnitAssignment (rejectedUnits[0]));
        }
        
        if (invUserList.size () > 0)
            insert invUserList;
        
        // Sending to IPMS when UA is approved at First Approval
        for (Unit_Assignment__c ua :unitAssignmentAndInventoriesMap.keySet ()) {
            AsyncWebserviceToIPMS.createUAJSON (unitAssignmentAndInventoriesMap.get (ua), ua, false, true);
        }
    }
    
    public void executeAfterDeleteTrigger(Map<Id, sObject> mapOldRecords){}
    public void executeAfterInsertUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){}
    
    @Future
    public static void updateValidateDate (Set <ID> activateUAs) {
        Map <String, Unit_Assignment_Rules__mdt> UARulesMap = new Map <String, Unit_Assignment_Rules__mdt> ();
        
        for (Unit_Assignment_Rules__mdt rule: [SELECT Rule_Type__c, Blocking_Hours__c, Deal_Entry__c, Deal_Exit__c FROM Unit_Assignment_Rules__mdt
                                            WHERE Rule_Type__c != NULL AND Blocking_Hours__c != NULL
                                                ]) {
                                                
            UARulesMap.put (rule.Rule_Type__c, rule);
        }
        List <Unit_Assignment__c> uaList = new List <Unit_Assignment__c> ();
        uaList = [ SELECT Unit_Assignment_Validity__c, UA_Type__c FROM Unit_Assignment__c WHERE ID IN :activateUAs ];
        
        List <Unit_Assignment__c> uaToUpdate = new List <Unit_Assignment__c> ();
        // To activate the Timer on UA when it is Active
        for (Unit_Assignment__c unitAssignment :uaList) {
                    
            unitAssignment.Unit_Assignment_Validity__c = System.Now()+ (UARulesMap.get ('Default').Blocking_Hours__c/24);
            if (unitAssignment.UA_Type__c == 'Reassign')
                unitAssignment.Unit_Assignment_Validity__c = System.Now()+ (UARulesMap.get (unitAssignment.UA_Type__c).Blocking_Hours__c/24);    
            
            uaToUpdate.add (unitAssignment);
        
        }
        if (uaToUpdate.size () > 0) {
            Update uaToUpdate;
        }
        
    }
}