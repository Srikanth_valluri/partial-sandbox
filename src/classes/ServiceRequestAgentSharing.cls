/****************************************************************************************************
* Name          : ServiceRequestAgentSharing                                                        *
* Description   : Class to change the Owner of the Records                                          *
* Created Date  : 01/07/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE            COMMENTS                                                *
* 1.1   Twinkle P           01/07/2018      Intial Draft                                            *
*****************************************************************************************************/
public class ServiceRequestAgentSharing {
    public static Boolean BYPASS_UPDATE_SR = false;
    /**
*  Method to set the Record Owner to Queue if the User is Portal User
* 
* @param: newRecordsList: List of Service Request
* 
* @return: NA
*/
    public static void updateTheOwnerToAgentQueue (List<sObject> newRecordsList) {
        
        /* CZL
Set<String> statusCodesSet = new Set<String> {'SUBMITTED'};
Map<String, Id> statusMap = new Map<String, Id>();
for(NSIBPM__SR_Status__c srStatus : [SELECT Id, Name, NSIBPM__Code__c 
FROM NSIBPM__SR_Status__c 
WHERE NSIBPM__Code__c IN : statusCodesSet])
{
statusMap.put(srStatus.NSIBPM__Code__c, srStatus.Id);
}
*/
        
        if(!BYPASS_UPDATE_SR) {
            Set<Id>  ownerIdsForServiceRequest = new Set<Id>();
            for ( NSIBPM__Service_Request__c objServiceRequest : (List<NSIBPM__Service_Request__c>)newRecordsList ) {
                ownerIdsForServiceRequest.add(objServiceRequest.OwnerId);
            }
            if (!ownerIdsForServiceRequest.isEmpty()) {
                Set<Id>  portalUserIds = new Map<Id, User> ([SELECT Id, Name, IsPortalEnabled 
                                                             FROM User 
                                                             WHERE IsActive = true
                                                             AND Id =: ownerIdsForServiceRequest
                                                             AND IsPortalEnabled = true]).keySet();
                
                if (portalUserIds != null && !portalUserIds.isEmpty()) {
                    Id agentUpdateRTId = DamacUtility.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Update');
                    //Schema.Sobjecttype.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Agent Update').getRecordTypeId();
                    Id dealRTId = DamacUtility.getRecordTypeId('NSIBPM__Service_Request__c', 'Deal');
                    //Schema.Sobjecttype.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
                    Id queueId = [SELECT Id 
                                  FROM Group
                                  WHERE Type = 'Queue' 
                                  AND DeveloperNAME = 'Agent_Admin_Team' 
                                  LIMIT 1].Id;
                    User damacUserObj = [SELECT Name,ManagerId  FROM user WHERE Name= 'Damac Admin' LIMIT 1];
                    for( NSIBPM__Service_Request__c objServiceRequest : (List<NSIBPM__Service_Request__c>) newRecordsList) {
                        if(portalUserIds.contains(objServiceRequest.OwnerId)) {
                            //CZL
                            //if (agentUpdateRTId == objServiceRequest.RecordTypeId && objServiceRequest.NSIBPM__Internal_SR_Status__c == statusMap.get('SUBMITTED')) {
                            if (agentUpdateRTId == objServiceRequest.RecordTypeId && objServiceRequest.NSIBPM__Internal_Status_Name__c == 'Submitted') {
                                objServiceRequest.OwnerId = queueId;
                            }
                            if(dealRTId == objServiceRequest.RecordTypeId) {
                                objServiceRequest.Manager_For_Agent_Portal__c = damacUserObj.ManagerId;
                            }
                        }
                    }
                }
            }
        }
    }
    
}