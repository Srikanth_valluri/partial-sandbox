@istest
public class FM_ProceedCaseControllerTest {
    static testMethod void testMethod1(){
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__SR_Status__c objStat = new NSIBPM__SR_Status__c();
        objStat.Name = 'Test';
        objStat.NSIBPM__Code__c = '123';
        insert objStat ;
        
        NSIBPM__Service_Request__c objServReq = TestDataFactory_CRM.createServiceRequest();
        objServReq.NSIBPM__Internal_SR_Status__c = objStat.Id ;
        insert objServReq ;
        
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objServReq.Id,1);
        insert lstBooking ;
        
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus ;
        
        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking ,3);
        for( Booking_Unit__c objUnit : lstBookingUnit  ) {
            objUnit.Resident__c = objAcc.Id ;
            objUnit.Handover_Flag__c = 'Y' ;
        }
        lstBookingUnit[2].Owner__c = objAcc.Id ;
        insert lstBookingUnit ;

        FM_Case__c fmObj  = new FM_Case__c();
        fmObj.Booking_Unit__c = lstBookingUnit[0].id;
        fmObj.Expected_move_out_date__c = Date.Today();
        fmObj.Tenant__c = objAcc.id;
        fmObj.Account__c = objAcc.id;
        fmObj.Request_Type_DeveloperName__c = 'Move_in_Request_T';
        insert fmObj;
        
        FM_ProceedCaseController fmProceed = new FM_ProceedCaseController(new ApexPages.StandardController( fmObj ));
        fmProceed.redirectToHomePage();
    }
}