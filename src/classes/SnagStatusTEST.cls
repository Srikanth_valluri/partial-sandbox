/**
 * Ver       Date            Author              Modification
 * 1.0    9/26/2019         Arsh Dave              Initial Version
**/
@isTest
public class SnagStatusTEST{//AKISHOR

    @isTest
    static void testOpenSnagStatus() {

        Set<Id> recTypeIdSet=new Set<Id>();
        for(RecordType rec :[
            SELECT DeveloperName
                , Id
                , Name
                FROM RecordType
            WHERE SobjectType = 'Case'  
                And ( Name='Handover'
                    OR Name='Early Handover'
                    )  
        ]) {
            recTypeIdSet.add(rec.Id);
        }
        Id HOCase=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc =  new Account(
                RecordTypeId = personAccRTId,
                FirstName='Test FirstName1',
                LastName='Test LastName2',
                Type='Person',
                Email__pc = 'test@test.com'
        );
        insert objAcc ;
        Id dealrecid=Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c Deal = new NSIBPM__Service_Request__c();
        Deal.RecordTypeId=dealrecid;
        insert Deal;

        Booking__c B=new Booking__c();
        B.Account__c = objAcc.Id;
        B.Booking_Channel__c='Web';
        B.Deal_SR__c=Deal.Id;
        insert B;

        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Unit_Name__c='HWT/PT124/XL2265B';
        objBU.Registration_ID__c='71609';
        objBU.Registration_Status_Code__c='LE';
        objBU.Party_Id__c='79658';
        objBU.Booking__c=B.Id;
        insert objBU;

        TriggerOnOffCustomSetting__c objCustomSetting = new TriggerOnOffCustomSetting__c();
        objCustomSetting.Name = 'CallingListTrigger';
        objCustomSetting.OnOffCheck__c = false;
        insert objCustomSetting;

        Id callingListRecTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Appointment Scheduling').getRecordTypeId();

        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Account__c = objAcc.Id;
        objCallingList.Appointment_Start_DateTime__c = System.now();
        objCallingList.Appointment_End_DateTime__c = System.now()+1;
        objCallingList.Booking_Unit__c = objBU.Id;
        objCallingList.Virtual_Viewing__c = true;
        objCallingList.RecordTypeId = callingListRecTypeId;
        insert objCallingList;

        Case sCase = new Case();
        sCase.RecordtypeId=HOCase;//'0122500000018qf';
        sCase.Booking_Unit__c= objBU.Id;
        sCase.AccountId=objAcc.Id;
        // sCase.Registration_ID__c=
        insert sCase;

        //  Inventory__c objInv = [Select Id From Inventory__c];
        Test.setMock(HttpCalloutMock.class, new SRFSnagMock());
        Test.startTest();
            //As Per Best Practice it is important to instantiate the Rest Context
            String JsonMsg = 'xyz';
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();

            req.requestURI = '/services/apexrest/UnitInspection';
            req.httpMethod = 'Patch';
            req.requestBody = Blob.valueof(JsonMsg);
            RestContext.request = req;
            RestContext.response= res;

            String OnsiteHODate = String.valueOf(Date.Today()); 
            String ViewingStat = 'Best';
            String UnitName = 'HWT/PT124/XL2265B';
            String SnagStat =  'Open'; //'Submitted' 'Revised' 'Ready for Handover' 'Handed Over'
            String SnagCompletionTargetDate = String.valueOf(Date.Today());
            String SnagCompletionRevisedTargetDate = String.valueOf(Date.Today());
            Integer TotalSnags = 2;
            Integer OpenSnags = 1;
            Integer ClosedSnags = 1 ;
            String VerifiedSnags = 'Yes Verified';
            String FirstViewingDate = String.valueOf(Date.Today());
            String SecondViewingDate = String.valueOf(Date.Today()); 
            String ThirdViewingDate = String.valueOf(Date.Today());
            String HE1='Aditya';String HE2='Kishor';String HE3='TEST';
            String ActlCompDate = String.valueOf(Date.Today()); 
            SnagStatus.CaptureSnagStatus(OnsiteHODate
                                        , ViewingStat
                                        , UnitName
                                        , SnagStat
                                        , SnagCompletionTargetDate
                                        , SnagCompletionRevisedTargetDate
                                        , TotalSnags
                                        , OpenSnags
                                        , ClosedSnags
                                        , VerifiedSnags
                                        , FirstViewingDate
                                        , SecondViewingDate
                                        , ThirdViewingDate
                                        , HE1
                                        , HE2
                                        , HE3
                                        , 'Test Remarks'
                                        , '8/09/2020'
                                        , ActlCompDate);
        Test.stopTest();

    }

    @isTest
    static void testRevisedSnagStatus() {

        Set<Id> recTypeIdSet=new Set<Id>();
        for(RecordType rec :[
            SELECT DeveloperName
                , Id
                , Name
                FROM RecordType
            WHERE SobjectType = 'Case'  
                And ( Name='Handover'
                    OR Name='Early Handover'
                    )  
        ]) {
            recTypeIdSet.add(rec.Id);
        }
        Id HOCase=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc =  new Account(
                RecordTypeId = personAccRTId,
                FirstName='Test FirstName1',
                LastName='Test LastName2',
                Type='Person' ,Email__pc = 'test@test.com'
        );
        insert objAcc ;
        Id dealrecid=Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c Deal = new NSIBPM__Service_Request__c();
        Deal.RecordTypeId=dealrecid;
        insert Deal;

        Booking__c B=new Booking__c();
        B.Booking_Channel__c='Web';
        B.Deal_SR__c=Deal.Id;B.Account__c = objAcc.Id;
B.Account__c = objAcc.Id;
        insert B;

        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Unit_Name__c='HWT/PT124/XL2265B';
        objBU.Registration_ID__c='71609';
        objBU.Registration_Status_Code__c='LE';
        objBU.Party_Id__c='79658';
        objBU.Booking__c=B.Id;
        insert objBU;

        TriggerOnOffCustomSetting__c objCustomSetting = new TriggerOnOffCustomSetting__c();
        objCustomSetting.Name = 'CallingListTrigger';
        objCustomSetting.OnOffCheck__c = false;
        insert objCustomSetting;

        Id callingListRecTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Appointment Scheduling').getRecordTypeId();

        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Account__c = objAcc.Id;
        objCallingList.Appointment_Start_DateTime__c = System.now();
        objCallingList.Appointment_End_DateTime__c = System.now()+1;
        objCallingList.Booking_Unit__c = objBU.Id;
        objCallingList.Virtual_Viewing__c = true;
        objCallingList.RecordTypeId = callingListRecTypeId;
        insert objCallingList;

        Case sCase = new Case();
        sCase.RecordtypeId=HOCase;//'0122500000018qf';
        sCase.Booking_Unit__c= objBU.Id;
        sCase.AccountId=objAcc.Id;
        insert sCase;

        //  Inventory__c objInv = [Select Id From Inventory__c];
        Test.setMock(HttpCalloutMock.class, new SRFSnagMock());
        Test.startTest();
            //As Per Best Practice it is important to instantiate the Rest Context
            String JsonMsg = 'xyz';
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();

            req.requestURI = '/services/apexrest/UnitInspection';
            req.httpMethod = 'Patch';
            req.requestBody = Blob.valueof(JsonMsg);
            RestContext.request = req;
            RestContext.response= res;

            String OnsiteHODate = String.valueOf(Date.Today()); 
            String ViewingStat = 'Best';
            String UnitName = 'HWT/PT124/XL2265B';
            String SnagStat =  'Revised'; //'Submitted' 'Revised' 'Ready for Handover' 'Handed Over'
            String SnagCompletionTargetDate = String.valueOf(Date.Today());
            String SnagCompletionRevisedTargetDate = String.valueOf(Date.Today());
            Integer TotalSnags = 2;
            Integer OpenSnags = 1;
            Integer ClosedSnags = 1 ;
            String VerifiedSnags = 'Yes Verified';
            String FirstViewingDate = String.valueOf(Date.Today());
            String SecondViewingDate = String.valueOf(Date.Today()); 
            String ThirdViewingDate = String.valueOf(Date.Today());
            String HE1='Aditya';String HE2='Kishor';String HE3='TEST';
            String ActlCompDate = String.valueOf(Date.Today()); 

            SnagStatus.CaptureSnagStatus(OnsiteHODate,ViewingStat,UnitName,SnagStat,SnagCompletionTargetDate,SnagCompletionRevisedTargetDate,TotalSnags,OpenSnags,ClosedSnags,VerifiedSnags,FirstViewingDate,SecondViewingDate,ThirdViewingDate,HE1,HE2,HE3, 'Test Remarks'
                                        , '8/09/2020',ActlCompDate);
        Test.stopTest();

    }

    @isTest
    static void testReadyforHandoverSnagStatus() {

        Set<Id> recTypeIdSet=new Set<Id>();
        for(RecordType rec :[
            SELECT DeveloperName
                , Id
                , Name
                FROM RecordType
            WHERE SobjectType = 'Case'  
                And ( Name='Handover'
                    OR Name='Early Handover'
                    )  
        ]) {
            recTypeIdSet.add(rec.Id);
        }
        Id HOCase=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc =  new Account(
                RecordTypeId = personAccRTId,
                FirstName='Test FirstName1',
                LastName='Test LastName2',
                Type='Person',Email__pc = 'test@test.com'

        );
        insert objAcc ;
        Id dealrecid=Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c Deal = new NSIBPM__Service_Request__c();
        Deal.RecordTypeId=dealrecid;
        insert Deal;

        Booking__c B=new Booking__c();
        B.Booking_Channel__c='Web';
        B.Deal_SR__c=Deal.Id;B.Account__c = objAcc.Id;
        insert B;

        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Unit_Name__c='HWT/PT124/XL2265B';
        objBU.Registration_ID__c='71609';
        objBU.Registration_Status_Code__c='LE';
        objBU.Party_Id__c='79658';
        objBU.Booking__c=B.Id;
        insert objBU;

        TriggerOnOffCustomSetting__c objCustomSetting = new TriggerOnOffCustomSetting__c();
        objCustomSetting.Name = 'CallingListTrigger';
        objCustomSetting.OnOffCheck__c = false;
        insert objCustomSetting;

        Id callingListRecTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Appointment Scheduling').getRecordTypeId();

        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Account__c = objAcc.Id;
        objCallingList.Appointment_Start_DateTime__c = System.now();
        objCallingList.Appointment_End_DateTime__c = System.now()+1;
        objCallingList.Booking_Unit__c = objBU.Id;
        objCallingList.Virtual_Viewing__c = true;
        objCallingList.RecordTypeId = callingListRecTypeId;
        insert objCallingList;

        Case sCase = new Case();
        sCase.RecordtypeId=HOCase;//'0122500000018qf';
        sCase.Booking_Unit__c= objBU.Id;
        sCase.AccountId=objAcc.Id;
        insert sCase;

        //  Inventory__c objInv = [Select Id From Inventory__c];,Email__pc = 'test@test.com'
B.Account__c = objAcc.Id;
        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
            //As Per Best Practice it is important to instantiate the Rest Context
            String JsonMsg = 'xyz';
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();

            req.requestURI = '/services/apexrest/UnitInspection';
            req.httpMethod = 'Patch';
            req.requestBody = Blob.valueof(JsonMsg);
            RestContext.request = req;
            RestContext.response= res;

            String OnsiteHODate = String.valueOf(Date.Today()); 
            String ViewingStat = 'Best';
            String UnitName = 'HWT/PT124/XL2265B';
            String SnagStat =  'Ready for Handover'; //'Submitted' 'Revised' 'Ready for Handover' 'Handed Over'
            String SnagCompletionTargetDate = String.valueOf(Date.Today());
            String SnagCompletionRevisedTargetDate = String.valueOf(Date.Today());
            Integer TotalSnags = 2;
            Integer OpenSnags = 1;
            Integer ClosedSnags = 1 ;
            String VerifiedSnags = 'Yes Verified';
            String FirstViewingDate = String.valueOf(Date.Today());
            String SecondViewingDate = String.valueOf(Date.Today()); 
            String ThirdViewingDate = String.valueOf(Date.Today());
            String HE1='Aditya';String HE2='Kishor';String HE3='TEST';
            String ActlCompDate = String.valueOf(Date.Today()); 


            SnagStatus.CaptureSnagStatus(OnsiteHODate,ViewingStat,UnitName,SnagStat,SnagCompletionTargetDate,SnagCompletionRevisedTargetDate,TotalSnags,OpenSnags,ClosedSnags,VerifiedSnags,FirstViewingDate,SecondViewingDate,ThirdViewingDate,HE1,HE2,HE3, 'Test Remarks'
                                        , '8/09/2020',ActlCompDate);
        Test.stopTest();

    }

    @isTest
    static void testHandedOverSnagStatus() {

        Set<Id> recTypeIdSet=new Set<Id>();
        for(RecordType rec :[
            SELECT DeveloperName
                , Id
                , Name
                FROM RecordType
            WHERE SobjectType = 'Case'  
                And ( Name='Handover'
                    OR Name='Early Handover'
                    )  
        ]) {
            recTypeIdSet.add(rec.Id);
        }
        Id HOCase=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc =  new Account(
                RecordTypeId = personAccRTId,
                FirstName='Test FirstName1',
                LastName='Test LastName2',
                Type='Person',Email__pc = 'test@test.com'

        );
        insert objAcc ;
        Id dealrecid=Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c Deal = new NSIBPM__Service_Request__c();
        Deal.RecordTypeId=dealrecid;
        insert Deal;

        Booking__c B=new Booking__c();
        B.Booking_Channel__c='Web';
        B.Deal_SR__c=Deal.Id;
        insert B;B.Account__c = objAcc.Id;

        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Unit_Name__c='HWT/PT124/XL2265B';
        objBU.Registration_ID__c='71609';
        objBU.Registration_Status_Code__c='LE';
        objBU.Party_Id__c='79658';
        objBU.Booking__c=B.Id;
        insert objBU;

        TriggerOnOffCustomSetting__c objCustomSetting = new TriggerOnOffCustomSetting__c();
        objCustomSetting.Name = 'CallingListTrigger';
        objCustomSetting.OnOffCheck__c = false;
        insert objCustomSetting;

        Id callingListRecTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Appointment Scheduling').getRecordTypeId();

        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Account__c = objAcc.Id;
        objCallingList.Appointment_Start_DateTime__c = System.now();
        objCallingList.Appointment_End_DateTime__c = System.now()+1;
        objCallingList.Booking_Unit__c = objBU.Id;
        objCallingList.Virtual_Viewing__c = true;
        objCallingList.RecordTypeId = callingListRecTypeId;
        insert objCallingList;

        Case sCase = new Case();
        sCase.RecordtypeId=HOCase;//'0122500000018qf';
        sCase.Booking_Unit__c= objBU.Id;
        sCase.AccountId=objAcc.Id;
        insert sCase;

        //  Inventory__c objInv = [Select Id From Inventory__c];
        Test.setMock(HttpCalloutMock.class, new SRFSnagMock());
        Test.startTest();
            //As Per Best Practice it is important to instantiate the Rest Context
            String JsonMsg = 'xyz';
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();

            req.requestURI = '/services/apexrest/UnitInspection';
            req.httpMethod = 'Patch';
            req.requestBody = Blob.valueof(JsonMsg);
            RestContext.request = req;
            RestContext.response= res;

            String OnsiteHODate = String.valueOf(Date.Today()); 
            String ViewingStat = 'Best';
            String UnitName = 'HWT/PT124/XL2265B';
            String SnagStat =  'Handed Over'; //'Submitted' 'Revised' 'Ready for Handover' 'Handed Over'
            String SnagCompletionTargetDate = String.valueOf(Date.Today());
            String SnagCompletionRevisedTargetDate = String.valueOf(Date.Today());
            Integer TotalSnags = 2;
            Integer OpenSnags = 1;
            Integer ClosedSnags = 1 ;
            String VerifiedSnags = 'Yes Verified';
            String FirstViewingDate = String.valueOf(Date.Today());
            String SecondViewingDate = String.valueOf(Date.Today()); 
            String ThirdViewingDate = String.valueOf(Date.Today());
            String HE1='Aditya';String HE2='Kishor';String HE3='TEST';
            String ActlCompDate = String.valueOf(Date.Today()); 

            SnagStatus.CaptureSnagStatus(OnsiteHODate,ViewingStat,UnitName,SnagStat,SnagCompletionTargetDate,SnagCompletionRevisedTargetDate,TotalSnags,OpenSnags,ClosedSnags,VerifiedSnags,FirstViewingDate,SecondViewingDate,ThirdViewingDate,HE1,HE2,HE3, 'Test Remarks'
                                        , '8/09/2020',ActlCompDate);
        Test.stopTest();

    }

    @isTest
    static void testSubmittedSnagStatus() {

        Set<Id> recTypeIdSet=new Set<Id>();
        for(RecordType rec :[
            SELECT DeveloperName
                , Id
                , Name
                FROM RecordType
            WHERE SobjectType = 'Case'  
                And ( Name='Handover'
                    OR Name='Early Handover'
                    )  
        ]) {
            recTypeIdSet.add(rec.Id);
        }
        Id HOCase=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc =  new Account(
                RecordTypeId = personAccRTId,
                FirstName='Test FirstName1',
                LastName='Test LastName2',
                Type='Person',Email__pc = 'test@test.com'

        );
        insert objAcc ;
        Id dealrecid=Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c Deal = new NSIBPM__Service_Request__c();
        Deal.RecordTypeId=dealrecid;
        insert Deal;

        Booking__c B=new Booking__c();
        B.Booking_Channel__c='Web';
        B.Deal_SR__c=Deal.Id;B.Account__c = objAcc.Id;
        insert B;

        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Unit_Name__c='HWT/PT124/XL2265B';
        objBU.Registration_ID__c='71609';
        objBU.Registration_Status_Code__c='LE';
        objBU.Party_Id__c='79658';
        objBU.Booking__c=B.Id;
        insert objBU;

        TriggerOnOffCustomSetting__c objCustomSetting = new TriggerOnOffCustomSetting__c();
        objCustomSetting.Name = 'CallingListTrigger';
        objCustomSetting.OnOffCheck__c = false;
        insert objCustomSetting;

        Id callingListRecTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Appointment Scheduling').getRecordTypeId();

        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Account__c = objAcc.Id;
        objCallingList.Appointment_Start_DateTime__c = System.now();
        objCallingList.Appointment_End_DateTime__c = System.now()+1;
        objCallingList.Booking_Unit__c = objBU.Id;
        objCallingList.Virtual_Viewing__c = true;
        objCallingList.RecordTypeId = callingListRecTypeId;
        insert objCallingList;

        Case sCase = new Case();
        sCase.RecordtypeId=HOCase;//'0122500000018qf';
        sCase.Booking_Unit__c= objBU.Id;
        sCase.AccountId=objAcc.Id;
        insert sCase;

        //  Inventory__c objInv = [Select Id From Inventory__c];
        Test.setMock(HttpCalloutMock.class, new SRFSnagMock());
        Test.startTest();
            //As Per Best Practice it is important to instantiate the Rest Context
            String JsonMsg = 'xyz';
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();

            req.requestURI = '/services/apexrest/UnitInspection';
            req.httpMethod = 'Patch';
            req.requestBody = Blob.valueof(JsonMsg);
            RestContext.request = req;
            RestContext.response= res;

            String OnsiteHODate = String.valueOf(Date.Today()); 
            String ViewingStat = 'Best';
            String UnitName = 'HWT/PT124/XL2265B';
            String SnagStat =  'Submitted'; //'Submitted' 'Revised' 'Ready for Handover' 'Handed Over'
            String SnagCompletionTargetDate = String.valueOf(Date.Today());
            String SnagCompletionRevisedTargetDate = String.valueOf(Date.Today());
            Integer TotalSnags = 2;
            Integer OpenSnags = 1;
            Integer ClosedSnags = 1 ;
            String VerifiedSnags = 'Yes Verified';
            String FirstViewingDate = String.valueOf(Date.Today());
            String SecondViewingDate = String.valueOf(Date.Today()); 
            String ThirdViewingDate = String.valueOf(Date.Today());
            String HE1='Aditya';String HE2='Kishor';String HE3='TEST';
            String ActlCompDate = String.valueOf(Date.Today());
            SnagStatus.CaptureSnagStatus(OnsiteHODate,ViewingStat,UnitName,SnagStat,SnagCompletionTargetDate,SnagCompletionRevisedTargetDate,TotalSnags,OpenSnags,ClosedSnags,VerifiedSnags,FirstViewingDate,SecondViewingDate,ThirdViewingDate,HE1,HE2,HE3, 'Test Remarks'
                                        , '8/09/2020',ActlCompDate);
        Test.stopTest();

    }

    @isTest
    static void testNullRegistrationId() {

        Set<Id> recTypeIdSet=new Set<Id>();
        for(RecordType rec :[
            SELECT DeveloperName
                , Id
                , Name
                FROM RecordType
            WHERE SobjectType = 'Case'  
                And ( Name='Handover'
                    OR Name='Early Handover'
                    )  
        ]) {
            recTypeIdSet.add(rec.Id);
        }
        Id HOCase=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc =  new Account(
                RecordTypeId = personAccRTId,
                FirstName='Test FirstName1',
                LastName='Test LastName2',
                Type='Person',Email__pc = 'test@test.com'

        );
        insert objAcc ;
        Id dealrecid=Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c Deal = new NSIBPM__Service_Request__c();
        Deal.RecordTypeId=dealrecid;
        insert Deal;

        Booking__c B=new Booking__c();
        B.Booking_Channel__c='Web';
        B.Deal_SR__c=Deal.Id;B.Account__c = objAcc.Id;
        insert B;

        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Unit_Name__c='HWT/PT124/XL2265B';
        // objBU.Registration_ID__c='71609';
        objBU.Registration_Status_Code__c='LE';
        objBU.Party_Id__c='79658';
        objBU.Booking__c=B.Id;
        insert objBU;

        TriggerOnOffCustomSetting__c objCustomSetting = new TriggerOnOffCustomSetting__c();
        objCustomSetting.Name = 'CallingListTrigger';
        objCustomSetting.OnOffCheck__c = false;
        insert objCustomSetting;

        Id callingListRecTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Appointment Scheduling').getRecordTypeId();

        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Account__c = objAcc.Id;
        objCallingList.Appointment_Start_DateTime__c = System.now();
        objCallingList.Appointment_End_DateTime__c = System.now()+1;
        objCallingList.Booking_Unit__c = objBU.Id;
        objCallingList.Virtual_Viewing__c = true;
        objCallingList.RecordTypeId = callingListRecTypeId;
        insert objCallingList;

        Case sCase = new Case();
        sCase.RecordtypeId=HOCase;//'0122500000018qf';
        sCase.Booking_Unit__c= objBU.Id;
        sCase.AccountId=objAcc.Id;
        insert sCase;

        //  Inventory__c objInv = [Select Id From Inventory__c];
        Test.setMock(HttpCalloutMock.class, new SRFSnagMock());
        Test.startTest();
            //As Per Best Practice it is important to instantiate the Rest Context
            String JsonMsg = 'xyz';
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();

            req.requestURI = '/services/apexrest/UnitInspection';
            req.httpMethod = 'Patch';
            req.requestBody = Blob.valueof(JsonMsg);
            RestContext.request = req;
            RestContext.response= res;

            String OnsiteHODate = String.valueOf(Date.Today()); 
            String ViewingStat = 'Best';
            String UnitName = 'HWT/PT124/X72295B';
            String SnagStat =  'Submitted'; //'Submitted' 'Revised' 'Ready for Handover' 'Handed Over'
            String SnagCompletionTargetDate = String.valueOf(Date.Today());
            String SnagCompletionRevisedTargetDate = String.valueOf(Date.Today());
            Integer TotalSnags = 2;
            Integer OpenSnags = 1;
            Integer ClosedSnags = 1 ;
            String VerifiedSnags = 'Yes Verified';
            String FirstViewingDate = String.valueOf(Date.Today());
            String SecondViewingDate = String.valueOf(Date.Today()); 
            String ThirdViewingDate = String.valueOf(Date.Today());
            String HE1='Aditya';String HE2='Kishor';String HE3='TEST';
            String ActlCompDate = String.valueOf(Date.Today());
            SnagStatus.CaptureSnagStatus(OnsiteHODate,ViewingStat,UnitName,SnagStat,SnagCompletionTargetDate,SnagCompletionRevisedTargetDate,TotalSnags,OpenSnags,ClosedSnags,VerifiedSnags,FirstViewingDate,SecondViewingDate,ThirdViewingDate,HE1,HE2,HE3, 'Test Remarks'
                                        , '8/09/2020',ActlCompDate);
        Test.stopTest();

    }


    @isTest
    static void TestReportedStat() {
        Set<Id> recTypeIdSet=new Set<Id>();
        for(RecordType rec :[SELECT DeveloperName,Id,Name
                FROM RecordType
                WHERE SobjectType = 'Case'
                And (Name='Handover'
                OR Name='Early Handover'
                )  ]){
        recTypeIdSet.add(rec.Id);
        }
        Id HOCase=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc =  new Account(RecordTypeId = personAccRTId,
                    FirstName='Test FirstName1',
                    LastName='Test LastName2',
                    Type='Person',Email__pc = 'test@test.com'

                    );
        insert objAcc ;
        Id dealrecid=Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c Deal = new NSIBPM__Service_Request__c();
        Deal.RecordTypeId=dealrecid;
        insert Deal;
        Booking__c B=new Booking__c();
        B.Booking_Channel__c='Web';
        B.Deal_SR__c=Deal.Id;B.Account__c = objAcc.Id;
        insert B;
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Unit_Name__c='HWT/PT124/XL2265B';
        objBU.Registration_ID__c='71609';
        objBU.Registration_Status_Code__c='LE';
        objBU.Party_Id__c='79658';
        objBU.Booking__c=B.Id;
        insert objBU;

        TriggerOnOffCustomSetting__c objCustomSetting = new TriggerOnOffCustomSetting__c();
        objCustomSetting.Name = 'CallingListTrigger';
        objCustomSetting.OnOffCheck__c = false;
        insert objCustomSetting;

        Id callingListRecTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Appointment Scheduling').getRecordTypeId();

        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Account__c = objAcc.Id;
        objCallingList.Appointment_Start_DateTime__c = System.now();
        objCallingList.Appointment_End_DateTime__c = System.now()+1;
        objCallingList.Booking_Unit__c = objBU.Id;
        objCallingList.Virtual_Viewing__c = true;
        objCallingList.RecordTypeId = callingListRecTypeId;
        insert objCallingList;

        Case sCase = new Case();
        sCase.RecordtypeId=HOCase;//'0122500000018qf';
        sCase.Booking_Unit__c= objBU.Id;
        sCase.AccountId=objAcc.Id;
        insert sCase;

        //  Inventory__c objInv = [Select Id From Inventory__c];
        Test.setMock(HttpCalloutMock.class, new SRFSnagMock());
        Test.startTest();
            //As Per Best Practice it is important to instantiate the Rest Context
            String JsonMsg = 'xyz';
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();

            req.requestURI = '/services/apexrest/UnitInspection';
            req.httpMethod = 'Patch';
            req.requestBody = Blob.valueof(JsonMsg);
            RestContext.request = req;
            RestContext.response= res;

            String OnsiteHODate = String.valueOf(Date.Today()); 
            String ViewingStat = 'Best';
            String UnitName = 'HWT/PT124/XL2265B';
            String SnagStat =  'Submitted'; //'Submitted' 'Revised' 'Ready for Handover' 'Handed Over'
            String SnagCompletionTargetDate = String.valueOf(Date.Today());
            String SnagCompletionRevisedTargetDate = String.valueOf(Date.Today());
            Integer TotalSnags = 2;
            Integer OpenSnags = 1;
            Integer ClosedSnags = 1 ;
            String VerifiedSnags = 'Yes Verified';
            String FirstViewingDate = String.valueOf(Date.Today());
            String SecondViewingDate = String.valueOf(Date.Today()); 
            String ThirdViewingDate = String.valueOf(Date.Today());
            String HE1='Aditya';String HE2='Kishor';String HE3='TEST';
            String ActlCompDate = String.valueOf(Date.Today());
            SnagStatus.CaptureSnagStatus(OnsiteHODate,ViewingStat,UnitName,SnagStat,SnagCompletionTargetDate,SnagCompletionRevisedTargetDate,TotalSnags,OpenSnags,ClosedSnags,VerifiedSnags,FirstViewingDate,SecondViewingDate,ThirdViewingDate,HE1,HE2,HE3, 'Test Remarks'
                                        , '8/09/2020',ActlCompDate);

        Test.stopTest();
    }
    
    @isTest
    static void TestSendSnagEmail() {
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                      Party_ID__c = '63062',
                                      RecordtypeId = rtId,
                                      Email__pc = 'test@mailinator.com',
                                      Email_2__pc ='test1@mailinator.com',
                                      Email_3__pc ='test2@mailinator.com',
                                      Email_4__pc ='test3@mailinator.com',
                                      Email_5__pc ='test4@mailinator.com');
        insert account;
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;

        //String fmAmount = String.valueOf(Decimal.valueOf(Label.Robo_Call_FM_Outstanding_Amount) + 100.00);
        Booking_unit__c objBU = new Booking_Unit__c (Booking__c = booking.Id,
                                                     Owner__c = account.Id
                                                     ,Send_Bulk_Email__c =true
                                                     , Registration_Status_Code__c ='xx'
                                                     ,Tenant__c = account.Id
                                                     ,Send_Tenant_Email__c = true
                                                     , DLDN_Notice__c ='afa'
                                                     , Unit_Name__c='HWT/PT124/XL2265B'
                                                     );
        insert objBU;
         
        Attachment objAttachment = new Attachment();
        objAttachment.Name = 'Test Document';
        objAttachment.Body = blob.valueOf('test');
        objAttachment.ContentType='image/png';
        objAttachment.ParentId = account.Id;
        insert objAttachment;
        List<Booking_Unit__c> bookingUnitList = [Select Id, Booking__r.Account__c,Booking__r.Account__r.Mobile_Phone_Encrypt__pc,
                                    Booking__r.Account__r.Name,Booking__r.Account__r.Email__pc,Unit_Name__c,SRF_Date__c,Revised_SRF_Date__c,
                                    Registration_Id__c,Onsite_Handover_Date__c,Snags_Reported__c,Snags_Completed__c,Snag_Status_Onsite__c,
                                    Viewing_Status_onsite__c,Total_Snags_Open__c,FirstViewingDate__c,SecondViewingDate__c,
                                    ThirdViewingDate__c,Snag_Completion_Target_Date__c,Snag_Completion_Revised_Target_Date__c,
                                    Total_Snags_Closed__c,Total_Snags_Fixed__c, Calling_List__r.Virtual_Viewing__c, SnagValue__c
                                    from Booking_Unit__c limit 1];
        bookingUnitList.add(objBU);
         //FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');
        Test.startTest();
            //As Per Best Practice it is important to instantiate the Rest Context

            SnagStatus.SendSnagEmail(bookingUnitList,objAttachment,'SNAG_Completion_Template');

        Test.stopTest();
    }
}