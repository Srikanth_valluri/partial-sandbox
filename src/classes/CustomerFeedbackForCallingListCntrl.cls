/**
 * @File Name          : CustomerFeedbackForCallingListCntrl.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 9/4/2019, 7:19:50 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    9/4/2019, 7:19:50 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public without sharing class CustomerFeedbackForCallingListCntrl {
   public String strCallingListId;
    public Calling_List__c objCallingList {get;set;}
    public boolean isHappy {get;set;}
    public boolean isSatisfied {get;set;}
    public boolean isNotSatisfied {get;set;}
    public string customerComments {get;set;}
    public boolean isSubmitted {get;set;}
    
    public CustomerFeedbackForCallingListCntrl(ApexPages.StandardController stdController) {
        system.debug('ala aat');
        isHappy = isSatisfied = isNotSatisfied = isSubmitted = false;
        strCallingListId = ApexPages.currentPage().getParameters().get('Id');
        objCallingList = [Select Id
                        , Name
                        , Customer_Satisfaction_Remainder_1__c
                        , Customer_Satisfaction_Remainder_2__c
                        , Customer_Experience__c
                        , Customer_Comments__c
                     From Calling_List__c 
                     Where Id =: strCallingListId];
        system.debug('objCallingList'+objCallingList);
    }
    
    public PageReference submitFeedback() {
        if(strCallingListId != null){
            if ((isHappy == true && isSatisfied == true) || (isHappy == true && isNotSatisfied == true)
                || (isSatisfied == true && isNotSatisfied == true)){
                ApexPages.addmessage(new ApexPages.message(
                    ApexPages.severity.Warning,'Please select only one option'));
            }
            else if (isHappy == true){
                objCallingList.Customer_Experience__c = 'Happy';
                isSubmitted = true;
            } else if (isSatisfied == true){
                objCallingList.Customer_Experience__c = 'Satisfied';
                isSubmitted = true;
            } else if (isNotSatisfied == true){
                objCallingList.Customer_Experience__c = 'Not Satisfied';
                isSubmitted = true;
            } else if (isHappy == false && isSatisfied == false && isNotSatisfied == false){
                ApexPages.addmessage(new ApexPages.message(
                    ApexPages.severity.Warning,'Please select atleast one option'));
            }
            system.debug('customerComments'+customerComments);
             objCallingList.Customer_Comments__c = customerComments;
           
            
            update objCallingList;
            
        }
        return null;
    }
}