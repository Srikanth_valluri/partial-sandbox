/***********************************************************************************
* Controller Class : ActiveUnitAssignmentController
* Created By : Tejashree Chavan
-----------------------------------------------------------------------------------
* Description : This is a controller class for ActiveUnitAssignment
*
* Test Data : ActiveUnitAssignmentControllerTest
-----------------------------------------------------------------------------------
* Version History:
* VERSION     DEVELOPER NAME       DATE            DETAIL FEATURES
1.0         Tejashree Chavan     12/12/2017        Initial Development
**********************************************************************************/
public class ActiveUnitAssignmentController {
    public String unitID;
    public String buttonVal;
    public String isActive;
    
    
    /************************************************************************************************
* @Description : This is Constructor method                                                     *
* @Params      :                                                                                *
* @Return      :                                                                                *
*************************************************************************************************/
    public ActiveUnitAssignmentController(ApexPages.StandardController controller) {
        unitId = String.valueof(controller.getRecord().get('id'));
        buttonVal = ApexPages.currentPage().getParameters().get('button');
        isActive = ApexPages.currentPage().getParameters().get('isActive');
    }
    /************************************************************************************************
* @Description : This method will set the Unit Assignment to Active on click of Activate button *
* @Params      :                                                                                *
* @Return      :PageReference                                                                   *
*************************************************************************************************/
    public PageReference checkOnLoad() {
        if(buttonVal.equals('deactivate'))
        {
            if (isActive == '1'){
                system.debug('>>>checkOnLoad'+isActive);
                deactivateUnitAssignment(unitID);    
            }else{
                ApexPages.addMessage(new ApexPages.Message(
                    ApexPages.severity.Info, 'This unit assignment is already deactivated.'));
                return null;
            }
            return null;
        }
        return null;
    }
    
    public void deActivateUnitAssignment (Id unitId) {
        
        String success = deActivateUA (unitId, false);
        if (success == 'Success') 
            ApexPages.addMessage(new ApexPages.Message(
                    ApexPages.severity.Info, 'Unit Assignment Deactivated'));
        
        else {
                ApexPages.addMessage(new ApexPages.Message(
                    ApexPages.severity.Info, 'Unit Assignment Deactivation Failed, Please Try Again'));
            }
    }
   
    
    public static String deActivateUA (id unitId, Boolean skipInventoryTrigger) {
        String success;
        if (!String.isBlank(unitId)) {
            Unit_Assignment__c unitAsgnObj = new Unit_Assignment__c ();
            List<Inventory__c> inventoriesList = new  List<Inventory__c>();
            List<Unit_Assignment__c> unitAssignmentList = new  List<Unit_Assignment__c>();
            List<Unit_Assignment__c> unitAssignmentRespList = new  List<Unit_Assignment__c>();
            Set<Id> inventoryIds = new Set<Id>();
            for(Inventory__c invObj : [SELECT id ,
                                       Unit_Assignment_Status__c,
                                       Name, UA_Proposal_Price__c,
                                       Tagged_To_Unit_Assignment__c,
                                       Unit_Assignment__c, Release_ID__c,
                                       Inventory_Id__c
                                       FROM Inventory__c 
                                       WHERE Unit_Assignment__c =:unitId ]) 
            {
                invObj.Unit_Assignment__c = null;
                invObj.UA_Proposal_Price__c = null;            //nullifying the proposal price on inventories on UA deactivation
                invObj.Unit_Assignment_Status__c = null;       //nullifying the assignment status on inventories on UA deactivation
                invObj.Tagged_To_Unit_Assignment__c = false;
                inventoryIds.add(invObj.id);
                inventoriesList.add(invObj);
            }
            List<Inventory_User__c> invUserList = new List<Inventory_User__c>();
            invUserList = [ SELECT User__c, Inventory__c,Unit_Assignment__c FROM Inventory_User__c WHERE Inventory__c IN : inventoryIds ];
            List <Inventory_Users_History__c> invHistory = new List <Inventory_Users_History__c> ();
            for (Inventory_User__c userVal :invUserList) {
                Inventory_Users_History__c invUserHistory = new Inventory_Users_History__c ();
                invUserHistory.User__c = userVal.User__c;
                invUserHistory.Inventory__c = userVal.Inventory__c;
                invUserHistory.Unit_Assignment__c = userVal.Unit_Assignment__c;
                invHistory.add (invUserHistory);
            }
            
            
            
            system.debug('>>>inventoriesList'+inventoriesList);
            unitAsgnObj  = new Unit_Assignment__c ();
            unitAsgnObj = [SELECT Inventory_Count__c, Proposal_Price__c, Deal_Value__c,
                           CreatedBy.ID, Agency__c, Agency__r.Vendor_id__c,  // getting agency, added by Srikanth
                           createdBy.IPMS_Employee_ID__c,  // getting IMPS employee id, added by Srikanth
                           Reason_For_Unit_Assignment__c,Name,                                        
                           createdBy.Manager.IPMS_Employee_ID__c // getting manager's IMPS employee id, added by Srikanth
                           FROM Unit_Assignment__c WHERE ID =: unitId ];
            unitAsgnObj.Active__c = false;
            unitAsgnObj.Proposal_Price__c = NULL;
            unitAsgnObj.Deal_Value__c = NULL;
            success = AsyncWebserviceToIPMS.createUAJSON (inventoriesList, unitAsgnObj , true, false); // Sending Inventories to IPMS, added by Srikanth
            if (Test.isRunningTest ()) 
                success = 'Success';
            if(success.equals('Success')) {
                DAMAC_Constants.skip_InventoryTrigger = TRUE;
                update unitAsgnObj ;
                update inventoriesList;
                delete invUserList;
                insert invHistory;
                return success;
            } else {
                return '';
            }
            
            
        }
        else {
            return '';
        }
    }
}