/*******************************************************************************************************
    * Description : To Send Bulk emails from Account
    *=======================================================================================================
    * Ver      Date-DD/MM/YYYY     Author              Modification
    *=======================================================================================================
    * 1.0      06/10/2019          Aishwarya Todkar    Initial Draft
    *
    * 1.1      13/05/2020          Aishwarya Todkar    1.Attached attachments which was related to template
    *                                                  2.Added Record_Id_To_Test to test on single record
    *********************************************************************************************************/
    global class BulkEmailSenderBatch_Account implements Database.Batchable<sObject>
                                                        , Database.Stateful
                                                        , Database.AllowsCallouts {
        
        //List of Sent emails to have activity history                                                    
        List<EmailMessage> lstEmails;
        
        //List of error logs
        List<Error_Log__c> lstErrorLogs;

        global Database.QueryLocator start(Database.BatchableContext BC){
            System.debug('-->> Inside start');

            String strQuery = 'SELECT Id,Email__c, Email__pc, Person_Business_Email__c, First_Name__c,'
                            + ' Last_Name__c, Nationality__pc ,Mobile_Phone_Encrypt__pc, CRM_Survey_Link__c, '
                            + ' Mobile__c, Name, Nationality__c, isPersonAccount, SurveyCx__c'
                            + ' FROM Account'
                            + ' WHERE Send_Bulk_Email__c = true AND Person_Business_Email__c != null ';
                            // + ' WHERE Send_Bulk_Email__c = true AND ((Email__c != null AND !isPersonAccount) '
                            // + ' OR (Email__pc != null AND isPersonAccount))';
            if( String.isNotBlank ( Label.Record_Id_To_Test ) 
            && Label.Record_Id_To_Test.toLowerCase().contains('on') 
            && String.isNotBlank( Label.Record_Id_To_Test.substringAfter('-') )
            && Label.Record_Id_To_Test.substringAfter('-').startsWith( '001') 
            && !Test.isRunningTest() ) {
                strQuery = strQuery + ' AND Id = \'' + Label.Record_Id_To_Test.substringAfter('-') + '\'  ';
            }
            System.debug('-->> strQuery == ' + strQuery);
            return Database.getQueryLocator(strQuery);
        }

        global void execute(Database.BatchableContext BC, List<Account > listAccount){
            System.debug('-->> listBU: ' + listAccount);
            System.debug('-->> listBU.size(): ' + listAccount.size());

            if( listAccount != null && listAccount.size() > 0 ) {

                //Get email template developer name from custom metadata
                List<Process_Email_Template__mdt> lstEmt = [SELECT 
                                                            Process_Name__c
                                                            , Email_Template__c
                                                            , Nationality__c
                                                        FROM 
                                                            Process_Email_Template__mdt
                                                        WHERE 
                                                            Process_Name__c =: 'Bulk Emails From Account' 
                                                        LIMIT 1];
                if( lstEmt != null && lstEmt.size() > 0 ) {

                    //Get email template
                    List<EmailTemplate> lstTemplate = [SELECT 
                                            ID, Subject, Body, HtmlValue, TemplateType
                                        FROM 
                                            EmailTemplate 
                                        WHERE 
                                            DeveloperName =: lstEmt[0].Email_Template__c 
                                        LIMIT 1];
                    
                    if( lstTemplate != null && lstTemplate.size() > 0 ) {
                        
                        lstEmails = new List<EmailMessage>();
                        lstErrorLogs = new List<Error_Log__c>();
                        
                        for(Account objAccount: listAccount) {

                            //Call Method to send emails through sendgrid
                            EmailMessage mail = sendEmail( objAccount, lstTemplate[0]);
                            if(mail != null )
                                lstEmails.add( mail );
                        }   //END of for
                        
                        System.debug('lstEmails=== ' + lstEmails);
                        if(lstEmails.size() > 0) {
                            if(!Test.isRunningTest()) {
                                insert lstEmails;
                            }
                        }
                    }
                }
            }
        }

    /*********************************************************************************
    * Method Name : sendEmail
    * Description : send emails by sendGrid.
    * Return Type : void
    * Parameter(s): Account, Email template
    **********************************************************************************/
        public EmailMessage sendEmail(Account objAccount, EmailTemplate template) {

            if( objAccount != null && template != null ) {
                
                //Setting email values
                String toAddress = String.isNotBlank( objAccount.Person_Business_Email__c ) ? objAccount.Person_Business_Email__c : '';
                String subject = String.isNotBlank(  template.subject ) ? template.subject : 'Test Bulk Email';
                String contentType = 'text/html';
                String fromAddress = Label.Bulk_Emails_From_Address;
                String bccAddress = Label.SF_Copy_EMail_Address; //Label.SF_Copy_EMail_Address;
                String replyToAddress = Label.Bulk_Emails_Reply_To_Address;
                String strCCAddress = '';
                String contentValue = template.htmlValue != null ? replaceMergeFields( objAccount, template.HtmlValue ) : '' ;
                String contentBody =  template.Body != null ? replaceMergeFields( objAccount, template.Body ) : '';
                List<Attachment> listAttachment = new List<Attachment>();
                listAttachment = [SELECT
                                        Id
                                        , Name
                                        , Body
                                    FROM
                                        Attachment
                                    WHERE
                                        ParentId =: template.Id];

                if( String.isNotBlank( toAddress ) && String.isNotBlank( subject ) 
                && String.isNotBlank( contentValue ) && String.isNotBlank( fromAddress )) {
                    try {
                        //Sending emails through Sendgrid
                        SendGridEmailService.SendGridResponse objSendGridResponse = 
                            SendGridEmailService.sendEmailService( toAddress,''
                                                                    , strCCAddress, ''
                                                                    , bccAddress, ''
                                                                    , subject, ''
                                                                    , fromAddress,''
                                                                    , replyToAddress, ''
                                                                    , contentType
                                                                    , contentValue, ''
                                                                    , listAttachment );
                        String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus; 
                        //Creating Activity History 
                        if ( responseStatus == 'Accepted') {
                            EmailMessage mail = new EmailMessage();
                            mail.Subject = subject;
                            mail.MessageDate = System.Today();
                            mail.Status = '3';//'Sent';
                            mail.RelatedToId = objAccount.Id;
                            mail.Account__c  = objAccount.Id;
                            mail.ToAddress = toAddress;
                            mail.FromAddress = fromAddress;
                            mail.TextBody = contentValue;//contentValue.replaceAll('\\<.*?\\>', '');
                            mail.Sent_By_Sendgrid__c = true;
                            mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                            mail.CcAddress = strCCAddress;
                            mail.BccAddress = bccAddress;
                            system.debug('Mail obj == ' + mail);
                            return mail;
                        }//End response if
                    }//End try
                    catch( Exception e ) {

                        //Creating error logs
                        lstErrorLogs.add( GenericUtility.createErrorLog( e.getMessage(), objAccount.Id, null, null, null) );
                    }
                }//End toAddress if
            }//End objAccount if
            return null;
        }//End SendEmail Method 

    /*********************************************************************************
    * Method Name : replaceMergeFields
    * Description : Replaces the merge fields with actual values.
    * Return Type : String
    * Parameter(s): Account, Email template Contents
    **********************************************************************************/
        public String replaceMergeFields( Account objAccount, String strContents ) {

            if( objAccount != null && String.isNotBlank( strContents ) ) {
                if( strContents.contains( '{!Name}' ) ) {

                    String strCxName = '';
                    // if( objAccount.isPersonAccount ) {
                    //     strCxName = String.isNotBlank( objAccount.First_Name__c ) 
                    //                 && String.isNotBlank( objAccount.Last_Name__c )
                    //                 ? objAccount.First_Name__c + ' ' + objAccount.Last_Name__c : '';
                    //     if( String.isBlank( strCxName ) ) 
                    //         strCxName = objAccount.Name;

                    // }
                    // else {
                        strCxName = String.isNotBlank( objAccount.Name ) ? objAccount.Name : '';
                    // }
                                
                    if( String.isNotBlank( strCxName ) ){
                        strCxName = GenericUtility.getCamelCase( strCxName );
                        strContents = strContents.replace( '{!Name}', strCxName);
                    }
                    else {
                        strContents = strContents.replace( '{!Name}', '');
                    }
                }//End Name
                
                if( strContents.contains( '{!Nationality}' ) ) {
                    
                    String nationality = '';
                    if( objAccount.isPersonAccount ) {
                        nationality = String.isNotBlank( objAccount.Nationality__pc ) 
                                    ? objAccount.Nationality__pc 
                                    : String.isNotBlank( objAccount.Nationality__c ) 
                                    ? objAccount.Nationality__c
                                    : '';
                    }
                    else {
                        nationality = String.isNotBlank( objAccount.Nationality__c ) 
                                    ? objAccount.Nationality__c 
                                    : String.isNotBlank( objAccount.Nationality__pc ) 
                                    ? objAccount.Nationality__pc
                                    : '';
                    }
                    if( String.isNotBlank( nationality ) ) {
                        strContents = strContents.replace( '{!Nationality}', nationality);
                    }
                    else {
                        strContents = strContents.replace( '{!Nationality}', '');
                    }
                }//End Nationality

                if( strContents.contains( '{!Email}' ) ) {
                    if( objAccount.Person_Business_Email__c != null ) {
                        strContents = strContents.replace( '{!Email}', objAccount.Person_Business_Email__c);
                    }
                    else {
                        strContents = strContents.replace( '{!Email}', '');
                    }
                }//End Email

                if( strContents.contains( '{!Mobile}' ) ) {
                    String mobile = '';
                    if( objAccount.isPersonAccount ) {
                        mobile = String.isNotBlank( objAccount.Mobile_Phone_Encrypt__pc  ) 
                                    ? objAccount.Mobile_Phone_Encrypt__pc  
                                    : String.isNotBlank( objAccount.Mobile__c  ) 
                                    ? objAccount.Mobile__c
                                    : '';
                    }
                    else {
                        mobile = String.isNotBlank( objAccount.Mobile__c  ) 
                                    ? objAccount.Mobile__c  
                                    : String.isNotBlank( objAccount.Mobile_Phone_Encrypt__pc ) 
                                    ? objAccount.Mobile_Phone_Encrypt__pc
                                    : '';
                    }
                    if( String.isNotBlank( mobile ) ) {
                        strContents = strContents.replace( '{!Mobile}', mobile);
                    }
                    else {
                        strContents = strContents.replace( '{!Mobile}', '');
                    }
                }//End Mobile

                if( strContents.contains( '{!SurveyCx}' ) ) {
                    if( String.isNotBlank( objAccount.SurveyCx__c ) ) {
                        strContents = strContents.replace( '{!SurveyCx}', objAccount.SurveyCx__c );
                    }
                    else {
                        strContents = strContents.replace( '{!SurveyCx}', '');
                    } 
                }//End SurveyCx

                if( strContents.contains( '{CRM Survey Link}' ) ) {
                    if( String.isNotBlank( objAccount.CRM_Survey_Link__c ) ) {
                        strContents = strContents.replace( '{CRM Survey Link}', objAccount.CRM_Survey_Link__c );
                    }
                    else {
                        strContents = strContents.replace( '{CRM Survey Link}', '');
                    } 
                }//End Record Id

                System.debug( 'Replaced contents = \n ' + strContents );
                return strContents;
            }//End of blank contents if

            return null;
        }//End of replaceMergeFields

        global void finish(Database.BatchableContext BC) {
            System.debug('-->> Finish ' );
        }
    }