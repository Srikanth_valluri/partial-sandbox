Global class Damac_RegenerateSignedDocument {

    Webservice static void generateDocument (ID srId) {
        NSIBPM__Service_Request__c sr = [SELECT Reservation_Form_Attachment_Name__c, Form_Request__c FROM NSIBPM__Service_Request__c 
                                            WHERE ID =: srId];
        String formReq = '';
        Form_Request__c req = new Form_Request__c ();
        if (sr.Form_Request__c != null) {
            formReq  = sr.Form_Request__c;
            req.Id = formReq;
        }
        else {
            if (sr.Reservation_Form_Attachment_Name__c != NULL && sr.Reservation_Form_Attachment_Name__c.startsWith ('F-')) {
                formReq = sr.Reservation_Form_Attachment_Name__c;
                req = [SELECT ID FROM Form_Request__c WHERE Name =: formReq];
            }
        }
        
        
        if (req.Id != null) {
            Boolean flag = false;
            req = [SELECT Service_Request__c FROM Form_Request__c WHERE ID =: req.id];
            if (req.Service_Request__c == null) {
                req.Service_Request__c = srId;
                update req;
                flag = true;
            }
            
            ESign_Details__c detail = new ESign_Details__c ();
            detail = [SELECT Document_Id__c FROM ESign_Details__c WHERE Form_request__c =: req.Id AND Status__c = 'Signed' order By CreatedDate DESC Limit 1];
            if (flag)
                doCallout (detail.Document_Id__c);
            else {
                Damac_SignNowAPI.getDocumentDetails (detail.Document_Id__c);
            }
        }                                            
    }
    
    @Future(callout=True) 
    public static void doCallout (String docId)
    {
        Damac_SignNowAPI.getDocumentDetails (docId);
    
    }
    
    
}