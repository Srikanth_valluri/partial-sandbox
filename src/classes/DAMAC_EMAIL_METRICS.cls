/*
Description:  Rest Response Class for Email CLIENT to read responses
Developed By: DAMAC IT Team
*/

@RestResource(urlMapping='/sendgridemailhook/*')
global without sharing class DAMAC_EMAIL_METRICS{   


    @HttpPost
    global static void doPost(){    
        String responseBody = RestContext.request.requestBody.toString();
        System.Debug ('RESPONSE BODY ::::'+responseBody);
        try{     
            
            Map <ID, List <SendGridResponseParser>> emailmetricsMap = new Map <ID, List <SendGridResponseParser>> ();
            List<SendGridResponseParser> obj = SendGridResponseParser.parse(responseBody );
            for (SendGridResponseParser resp :obj) {
                if (emailmetricsMap.containsKey (resp.uniqueId))
                    emailmetricsMap.get (resp.uniqueId).add (resp);
                else
                    emailmetricsMap.put (resp.uniqueId, new List <SendGridResponseParser> {resp});
            }
            Map <ID, Email_Metrics__c> emailMetrics = new Map <ID, Email_Metrics__c> (
                [SELECT 
                    No_of_Clicks__c, No_of_Openes__c 
                    FROM Email_Metrics__c 
                    WHERE ID IN : emailmetricsMap.keySet ()]);
            
            List <Email_Metrics__c> metricsToUpdate = new List <Email_Metrics__c> ();
            List <Email_Link_Tracking__c> linkTracking = new List <Email_Link_Tracking__c> ();
            for (ID key :emailmetricsMap.keySet ()) {
                Email_Metrics__c metrics = new Email_Metrics__c ();
                metrics.id = key;
                
                for (SendGridResponseParser resp : emailmetricsMap.get (key)) {
                    if (resp.event == 'processed') {
                        metrics.processed__c = TRUE;
                        metrics.Processed_Date_Time__c = timeStampToDate (resp.timeStamp);
                    }
                    if (resp.event == 'delivered') {
                        metrics.delivered__c = TRUE;
                        metrics.Delivered_Date_Time__c = timeStampToDate (resp.timeStamp);
                    }
                        
                    if (resp.event == 'dropped') {
                        metrics.Dropped__c = TRUE;
                        metrics.Dropped_Date_Time__c = timeStampToDate (resp.timeStamp);
                    }
                    if (resp.event == 'deferred') {
                        metrics.Deferred__c = TRUE;
                        metrics.Deferred_Date_Time__c = timeStampToDate (resp.timeStamp);
                    }
                        
                    if (resp.event == 'bounce') {
                        metrics.bounced__c = TRUE;
                        metrics.Bounced_Date_Time__c = timeStampToDate (resp.timeStamp);
                    }
                    if (resp.event == 'open') {
                        if (emailMetrics.get (key).No_of_Openes__c == NULL)
                            metrics.No_of_Openes__c = 1;
                        else
                            metrics.No_of_Openes__c = 1 + emailMetrics.get (key).No_of_Openes__c;
                        metrics.opened__c = TRUE;
                        metrics.Opened_Date_Time__c = timeStampToDate (resp.timeStamp);
                    }
                        
                    if (resp.event == 'click') {
                        if (emailMetrics.get (key).No_of_Clicks__c == NULL)
                            metrics.No_of_Clicks__c = 1;
                        else
                            metrics.No_of_Clicks__c = 1 + emailMetrics.get (key).No_of_Clicks__c;
                        metrics.clicked__c = TRUE;
                        metrics.Clicked_Date_Time__c = timeStampToDate (resp.timeStamp);
                        
                        Email_Link_Tracking__c link = new Email_Link_Tracking__c ();
                        //link.URL__c = resp.url;
                        link.Big_URL__c = resp.url;
                        link.Clicked_Date_Time__c = timeStampToDate (resp.timeStamp);
                        link.Email_Metrics__c = key;
                        linkTracking.add (link);
                    }
                    if (resp.event == 'group_resubscribe') {
                        metrics.ASM_Group_Resubscribe__c = TRUE;
                        metrics.ASM_Group_Resubscribe_Date_Time__c = timeStampToDate (resp.timeStamp);
                    }
                        
                    if (resp.event == 'group_unsubscribe') {
                        metrics.ASM_Group_Unsubscribe__c= TRUE;
                        metrics.ASM_Group_Unsubscribe_Date_Time__c = timeStampToDate (resp.timeStamp);
                    }
                    if (resp.event == 'spamreport') {
                        metrics.Mark_as_Spam__c = TRUE;
                        metrics.Mark_as_Spam_Date_Time__c = timeStampToDate (resp.timeStamp);
                    }
                        
                    if (resp.event == 'unsubscribe') {
                        metrics.Unsubscribed_From__c = TRUE;
                        metrics.Unsubscribed_Date_Time__c = timeStampToDate (resp.timeStamp);
                    }
                }
                metricsToUpdate.add (metrics);                
            }
            if (metricsToUpdate.size () > 0) {
                Damac_Constants.skip_EmailMetricsTrigger = TRUE;
                Update metricsToUpdate;   
             }     
             if (linkTracking.size () > 0) {
                 insert linkTracking;
             }       
        } catch(exception e){
            System.Debug (e.getMessage ());
            map<string,string> respMap = new map<string,string>();
            respMap = RestContext.request.params;
            list<string> emails =  new list<string>{Label.SF_Notifications_Email};
            Messaging.singleEmailmessage Email = new Messaging.singleEmailmessage();
            email.setsubject(' exception in sf ');
            email.SetPlainTextbody(string.valueof(respMap)+ ' SF Message :::'+e.getMessage ());            
            Email.SetToAddresses(emails);    
            Messaging.sendemailResult[] r = Messaging.sendemail(new Messaging.singleEmailmessage[]{email});
        } 
    }
    public static DateTime timeStampToDate (Integer unixTime) {
        DateTime dateInstance = datetime.newInstanceGmt(1970, 1, 1, 0, 0, 0);
        
        return dateInstance.addSeconds(unixTime);
    }
}