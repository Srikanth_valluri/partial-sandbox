@isTest

public class PlotHandoverServiceTest{
    static Account  objAcc;
    static Case objCase;
    static Booking__c objBooking;
    static Booking_Unit__c objBookingUnit;
    static NSIBPM__Service_Request__c objSR;
    static Booking_Unit_Active_Status__c objBUActive;
    static Option__c objOptions;
    static Payment_Plan__c objPaymentPlan;
    static SR_Attachments__c objSRatt ;
    static SR_Booking_Unit__c objSRB;
    
        /*static testMethod void initialMethod(){
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person',party_ID__C='123456');
        insert objAcc;
        
        objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'Test Unit';
        objBookingUnit.Unit_Type__c  = 'PLOT';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        insert objBookingUnit;
        
        objBUActive = new Booking_Unit_Active_Status__c ();
        objBUActive.name = 'Test Active Status';
        objBUActive.Status_Value__c = 'Agreement executed by DAMAC'; 
        insert objBUActive; 
        
        objOptions = new Option__c();
        objOptions.Booking_Unit__c = objBookingUnit.id;
        objOptions.PromotionName__c = 'PromotionName__c';
        objOptions.SchemeName__c = 'SchemeName__c';
        objOptions.OptionsName__c = 'OptionsName__c';
        objOptions.CampaignName__c = 'CampaignName__c';
        insert objOptions;
        
        objPaymentPlan = new Payment_Plan__c();
        objPaymentPlan.Booking_Unit__c = objBookingUnit.id;
        objPaymentPlan.Status__C = '';
        insert objPaymentPlan;
        
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Plot NOC').getRecordTypeId();
        objCase = new Case();
        objCase.AccountID = objAcc.id ;
        objCase.RecordTypeID = devRecordTypeId;
        objCase.Total_Token_Amount__c = 1000;
        objCase.Excess_Amount__c = 5000;
        objCase.POA_Expiry_Date__c = System.today();
        objCase.IsPOA__c = false;
        insert objCase;
        
        objSRatt = new SR_Attachments__c ();
        objSRatt.name = 'Test NOC';
        objSRatt.Case__c = objCase.Id;         
        objSRatt.type__c = 'NOC' ;
        //insert objSRatt;
        
        objSRB = new SR_Booking_Unit__c();
        objSRB.Case__c = objCase.id;
        objSRB.Booking_Unit__c = objBookingUnit.id;   
        insert objSRB;
    }*/
    
    static testMethod void Test1(){       
    
        //initialMethod(); 
      
        test.StartTest();      
        
        Test.setMock( WebServiceMock.class, new PlotHandoverUnitDetailsMock(4));  //  community charges Exception
        
        PlotHandoverService.getDepositAmount('123','123');
        test.StopTest();
    
    }
    static testMethod void Test2(){       
    
        //initialMethod(); 
      
        test.StartTest();      
        
        Test.setMock( WebServiceMock.class, new PlotHandoverUnitDetailsMock(5));  // success
        
        PlotHandoverService.getDepositAmount('123','123');
        test.StopTest();
    
    }
    
     static testMethod void Test3(){       
    
        //initialMethod(); 
      
        test.StartTest();      
        
        Test.setMock( WebServiceMock.class, new PlotHandoverUnitDetailsMock(6));  // Error
        
        PlotHandoverService.getDepositAmount('123','123');
        test.StopTest();
    
    }
    
    static testMethod void Test4(){       
    
        //initialMethod(); 
      
        test.StartTest();      
        
        Test.setMock( WebServiceMock.class, new PlotHandoverUnitDetailsMock(7));  // Error
        
        PlotHandoverService.getDepositAmount('123','123');
        test.StopTest();
    
    }
    
    static testMethod void Test5(){       
    
        //initialMethod(); 
      
        test.StartTest();      
        
        Test.setMock( WebServiceMock.class, new PlotHandoverUnitDetailsMock(8));  // Error
        
        PlotHandoverService.getDepositAmount('123','123');
        test.StopTest();
    
    }
    
    static testMethod void Test6(){       
    
        //initialMethod(); 
      
        test.StartTest();      
        
        Test.setMock( WebServiceMock.class, new PlotHandoverUnitDetailsMock(9));  // Error
        
        PlotHandoverService.getDepositAmount('123','123');
        test.StopTest();
    
    }
    
    static testMethod void Test7(){       
    
        //initialMethod(); 
      
        test.StartTest();      
        
        Test.setMock( WebServiceMock.class, new PlotHandoverUnitDetailsMock(10));  // Error
        
        PlotHandoverService.getDepositAmount('123','123');
        test.StopTest();
    
    }
    
    static testMethod void Test8(){       
    
        //nitialMethod(); 
      
        test.StartTest();      
               
        PlotHandoverService.DataWrapper obj = new PlotHandoverService.DataWrapper();
        obj.ATTRIBUTE1 = 'Test'; 
        obj.ATTRIBUTE2 = 'status'; 
        obj.Message_ID = 'Test'; 
        obj.PARAM_ID = 'Test'; 
        obj.PROC_MESSAGE = 'status'; 
        obj.PROC_STATUS = 'Test';
        
        List<PlotHandoverService.DataWrapper> lstData = new List<PlotHandoverService.DataWrapper> ();
        lstData.add(obj);
        
        PlotHandoverService.Wrapper  obj1 = new PlotHandoverService.Wrapper();
        obj1.message = 'Test'; 
        obj1.status= 'status'; 
        obj1.data  = lstData ;
        
        PlotHandoverService.ResponseWrapper obj3 = new PlotHandoverService.ResponseWrapper();
        obj3.Message = 'Test'; 
        obj3.Status = 'status'; 
        obj3.Amount = 100;        
        obj3.strIsDepositAmountAvailable = 'status';         
        
        test.StopTest();
    
    }

}