Global class Damac_ValidateSR {

    webservice static string validateCountryOfSale (String srChangeAgencyID){
    
        NSIBPM__Service_Request__c serviceRequest = [SELECT NSIBPM__Parent_SR__c,
                                                        Agency__c 
                                                    FROM NSIBPM__Service_Request__c 
                                                    WHERE ID=:srChangeAgencyID ];
        Set<ID> selectedUnitsSet = new Set<ID>();
        for(Booking_Unit__c bookingUnit : [SELECT Inventory__c 
                                            FROM Booking_Unit__c 
                                            WHERE Booking__r.Deal_SR__c =:serviceRequest.NSIBPM__Parent_SR__c 
                                            AND Inventory__c != null])
            selectedUnitsSet.add(bookingUnit.Inventory__c);
        Integer counter = 0;
        String agencyId = serviceRequest.Agency__c,inventoryID;
        Boolean validate = false;
        Set<String> agencyEligibleCountryName = new Set<String>();
        Set<String> agencyEligibleCountry = new Set<String>();
        if(String.isNotBlank(agencyId) && !selectedUnitsSet.isEmpty()){
            for(Account thisAccount : [SELECT Id, Name, 
                                       (SELECT Id, Name, Org_ID_formula__c, Org_ID__c
                                        FROM Agent_Sites__r 
                                        WHERE End_Date__c = NULL) 
                                       FROM Account 
                                       WHERE Id =: agencyId]){
                                           for(Agent_Site__c thisAgentSite : thisAccount.Agent_Sites__r){
                                               if(String.isNotBlank(thisAgentSite.Org_ID_formula__c)){
                                                   agencyEligibleCountry.add(thisAgentSite.Org_ID_formula__c);  
                                               }
                                               if(String.isNotBlank(thisAgentSite.Name)){
                                                   agencyEligibleCountryName.add(thisAgentSite.Name);  
                                               }
                                           }   
                                       }
            
            system.debug('#### agencyEligibleCountry = '+agencyEligibleCountry);
            if(!agencyEligibleCountry.isEmpty() && !selectedUnitsSet.isEmpty()){
                for(Inventory__c thisInventory : [SELECT Id, Location_Code__c, Property_Country__c, Is_Assigned__c, Org_ID__c,
                                                  Floor_Package_ID__c, Floor_Package_Type__c, List_Price_calc__c, Special_Price_calc__c,Selling_Price__c
                                                  FROM Inventory__c 
                                                  WHERE Id IN: selectedUnitsSet]){
                                                      system.debug('#### Org Id = '+thisInventory.Org_ID__c);
                                                      if(String.isNotBlank(thisInventory.Org_ID__c) && 
                                                         agencyEligibleCountry.contains(thisInventory.Org_ID__c)){
                                                             validate = true;    
                                                         }else{
                                                             validate = false;   
                                                             break;
                                                         }   
                                                  }
            }
        }
        
        return validate ? 'Sucess' : 'Error';
    }
}