/*-------------------------------------------------------------------------------------------------
Description: Class to generate Notice & BR Document from drawloop
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 08-04-2018       | Lochan Karle     | 1. Added logic to generate Notice & BR Document from drawloop
=============================================================================================================================
*/
public class FMGenerateNoticeBRDocument {
    public String bookingUnitId;
    public booking_unit__c objBU;
    public FMGenerateNoticeBRDocument(ApexPages.StandardController stdCon){
        bookingUnitId = stdCon.getId();
        objBU = [SELECT Id,
                      Registration_ID__c,
                      FM_Outstanding_Amount__c 
                     FROM booking_unit__c 
                     WHERE Id =: bookingUnitId];
    }
    
    public void callDrawloop(){
        system.debug('!!!callDrawloop');
         if(String.isNotBlank(objBu.Registration_ID__c)) {
             system.debug('!!!!Inside If');
             FmIpmsRestServices.DueInvoicesResult dues = FmIpmsRestServices.getDueInvoices(objBu.Registration_ID__c,'', '');
             system.debug('!!!!dues'+dues);
              if(dues != null && String.isNotBlank(dues.totalDueAmount)) {
                    objBu.FM_Outstanding_Amount__c = dues.totalDueAmount;
                    update objBu;
             }
             executeBatch();
         }         
    }
    
    public pageReference returnToBU(){
        PageReference Pg = new PageReference('/'+bookingUnitId);
        return pg;
    }
    
    public void executeBatch() {
        GenerateBUDrawloopDocumentBatch  objInstance = new GenerateBUDrawloopDocumentBatch (bookingUnitId
                                                                                                , System.Label.Notice_BR_DDPId
                                                                                                , System.Label.Notice_BR_TemplateId);
        Id batchId = Database.ExecuteBatch(objInstance);
        if(String.valueOf(batchId) != '000000000000000'){
            /*SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
            objCaseAttachment.booking_unit__c = bookingUnitId;
            objCaseAttachment.Name = 'Work Permit Document';
            objCaseAttachment.Booking_Unit__c = objBU.Booking_Unit__c;
            insert objCaseAttachment;*/
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Your request for Notice & BR document was successfully submitted. Please check the documents section for the document in a while.');
            ApexPages.addMessage(myMsg);
        }else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Your request for Notice & BR document could not be completed. Please try again later.');
            ApexPages.addMessage(myMsg);
        }
    }
} // end of class