/*
* Test class for SaveSurveyWebService class
*/
@isTest
public class SaveSurveyWebServiceTest{
    /*
    *   When no account is found
    */
    /*@isTest
    public static void processSaveSurveyRequestTest_NoAccount(){
        CreRoutingTest.createCREUsers('creTest@primaryUser.com', '7000');
        SaveSurveyLogic.SaveSurveyRequestBody reqst=
            new SaveSurveyLogic.SaveSurveyRequestBody();
        reqst.callingNumber = '1234567890';
        reqst.extension = '7000';
        reqst.calledDateTime = '11/6/2014 12:10:20';
        reqst.rating = '1';

        String JsonMsg=JSON.serialize(reqst);

        RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.requestUri = '/saveSurvey';
        req.requestBody = Blob.valueOf(JSON.serializePretty(reqst));
        RestContext.request = req;
        RestContext.response = new RestResponse();
        Test.startTest();
        SaveSurveyWebService.doPost();
        Test.stopTest();
        System.assertEquals(400,RestContext.response.statusCode);

        String saveSurveyLogicResponse = RestContext.response.responseBody.toString();
        SaveSurveyLogic.SaveSurveyResponseBody response =
            (SaveSurveyLogic.SaveSurveyResponseBody)JSON.deserializeStrict(
                saveSurveyLogicResponse ,
                SaveSurveyLogic.SaveSurveyResponseBody.Class);
         System.assertEquals(response.errorMessage, 'No Account found for the given calling number');
    }*/

    /*
    *    WHen CRE user is not added
    */
    @isTest
    public static void processSaveSurveyRequestTest_NoUser(){

        SaveSurveyLogic.SaveSurveyRequestBody reqst=
            new SaveSurveyLogic.SaveSurveyRequestBody();
        reqst.callingNumber = '1234567890';
        reqst.extension = '7000';
        reqst.calledDateTime = '11/6/2014 12:10:20';
        reqst.rating = '1';

        String JsonMsg = JSON.serialize(reqst);

        RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.requestUri = '/saveSurvey';
        req.requestBody = Blob.valueOf(JSON.serializePretty(reqst));
        RestContext.request = req;
        RestContext.response = new RestResponse();
        Test.startTest();
        SaveSurveyWebService.doPost();
        Test.stopTest();
        System.assertEquals(400,RestContext.response.statusCode);

        String saveSurveyLogicResponse = RestContext.response.responseBody.toString();
        SaveSurveyLogic.SaveSurveyResponseBody response =
            (SaveSurveyLogic.SaveSurveyResponseBody)JSON.deserializeStrict(
                saveSurveyLogicResponse ,
                SaveSurveyLogic.SaveSurveyResponseBody.Class);
         System.assertEquals(response.errorMessage, 'No CRE User found for the given extension');
    }

    /*
    *   When call date time is invalid
    */
    @isTest
    public static void processSaveSurveyRequestTest_NoCallDateTime(){
        CreRoutingTest.createCREUsers('creTest@primaryUser.com', '7000');
        CreRoutingTest.createRegisteredNonVipAccount('7894561230');

        SaveSurveyLogic.SaveSurveyRequestBody reqst=
            new SaveSurveyLogic.SaveSurveyRequestBody();
        reqst.callingNumber = '7894561230';
        reqst.extension = '7000';
        reqst.calledDateTime = '11/6/2014';
        reqst.rating = '1';

        String JsonMsg = JSON.serialize(reqst);

        RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.requestUri = '/saveSurvey';
        req.requestBody = Blob.valueOf(JSON.serializePretty(reqst));
        RestContext.request = req;
        RestContext.response = new RestResponse();
        Test.startTest();
        SaveSurveyWebService.doPost();
        Test.stopTest();
        System.assertEquals(400,RestContext.response.statusCode);

        String saveSurveyLogicResponse = RestContext.response.responseBody.toString();
        SaveSurveyLogic.SaveSurveyResponseBody response =
            (SaveSurveyLogic.SaveSurveyResponseBody)JSON.deserializeStrict(
                saveSurveyLogicResponse ,
                SaveSurveyLogic.SaveSurveyResponseBody.Class);
         System.assertEquals(response.errorMessage, 'Please enter valid date format (MM/DD/YYYY HH:MM:SS)');
    }

    /*
    *    When all valid data is provided
    */
    @isTest
    public static void processSaveSurveyRequestTest_ValidData(){
        CreRoutingTest.createCREUsers('creTest@primaryUser.com', '7000');
        CreRoutingTest.createRegisteredNonVipAccount('7894561230');

        SaveSurveyLogic.SaveSurveyRequestBody reqst=
            new SaveSurveyLogic.SaveSurveyRequestBody();
        reqst.callingNumber = '7894561230';
        reqst.extension = '7000';
        reqst.calledDateTime = '11/6/2014 12:10:20';
        reqst.rating = '1';

        String JsonMsg = JSON.serialize(reqst);

        RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.requestUri = '/saveSurvey';
        req.requestBody = Blob.valueOf(JSON.serializePretty(reqst));
        RestContext.request = req;
        RestContext.response = new RestResponse();
        Test.startTest();
        SaveSurveyWebService.doPost();
        Test.stopTest();
        System.assertEquals(200,RestContext.response.statusCode);

        String saveSurveyLogicResponse = RestContext.response.responseBody.toString();
        SaveSurveyLogic.SaveSurveyResponseBody response =
            (SaveSurveyLogic.SaveSurveyResponseBody)JSON.deserializeStrict(
                saveSurveyLogicResponse ,
                SaveSurveyLogic.SaveSurveyResponseBody.Class);
        System.assertEquals('Survey Saved', response.status);
    }

     /*
    *    When all valid data is provided(Webservice)
    */
    @isTest
    public static void doPostTest(){
        CreRoutingTest.createCREUsers('creTest@primaryUser.com', '7000');
        CreRoutingTest.createRegisteredNonVipAccount('7894561230');

        SaveSurveyLogic.SaveSurveyRequestBody reqst=
            new SaveSurveyLogic.SaveSurveyRequestBody();
        reqst.callingNumber = '7894561230';
        reqst.extension = '7000';
        reqst.calledDateTime = '11/6/2014 12:10:20';
        reqst.rating = '1';

        String JsonMsg = JSON.serialize(reqst);

        RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.requestUri = '/saveSurvey';
        req.requestBody = Blob.valueOf(JSON.serializePretty(reqst));
        RestContext.request = req;
        RestContext.response = new RestResponse();
        Test.startTest();
        SaveSurveyWebService.doPost();
        Test.stopTest();
        System.assertEquals(200,RestContext.response.statusCode);

        String saveSurveyLogicResponse = RestContext.response.responseBody.toString();
        SaveSurveyLogic.SaveSurveyResponseBody response =
            (SaveSurveyLogic.SaveSurveyResponseBody)JSON.deserializeStrict(
                saveSurveyLogicResponse ,
                SaveSurveyLogic.SaveSurveyResponseBody.Class);
        System.assertEquals('Survey Saved', response.status);
    }
}