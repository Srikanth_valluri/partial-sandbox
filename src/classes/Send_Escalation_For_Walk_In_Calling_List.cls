public without sharing class Send_Escalation_For_Walk_In_Calling_List {
 public static Id Walk_InCallCallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Walk In Calling List').RecordTypeId;
   
    @InvocableMethod
     public static void callFutureToSendEscalation(List<Calling_List__c> lstCallingList) {
        set<Id>ownerIdSet = new Set<Id>();
        System.debug('inside future ::lstCallingList::'+lstCallingList);
         
        Set<Id> addCallingIds = new Set<Id>();
        
        for( Calling_List__c objCallingList : lstCallingList ) {
            if( objCallingList.RecordTypeId == Walk_InCallCallingRecordTypeId ) {
         
                addCallingIds.add( objCallingList.Id );
            }
        }         
        System.debug('insaddCallingIds : '+addCallingIds);
        if(!addCallingIds.isEmpty()){
            UpdateCallingListHandler.callFutureToSendEscalation(addCallingIds);
        }
                 

     }
}