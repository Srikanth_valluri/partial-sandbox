/*
 * Description : This class is used to Schedule UpdateEmailUserNameonPortalUserBatch batch at every midnight
 * Revision History:
 *
 *  Version          Author              Date           Description
 *  1.0              Arjun Khatri        12/07/2018      Initial Draft
 *                                                      
 */
global class ScheduleUpdateEmailUserNameonPortalUser implements Schedulable {
  global void execute(SchedulableContext sc) {
      UpdateEmailUserNameonPortalUserBatch batchInstance = new UpdateEmailUserNameonPortalUserBatch(); 
      database.executebatch(batchInstance,90);
  }
}