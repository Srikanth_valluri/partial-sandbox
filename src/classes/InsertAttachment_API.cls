/**********************************************************************************************************************
Description: This API is used for creating documents(SR_Attachments) under FM Case & Case
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   09-11-2020      | Shubham Suryawanshi | Added method for creating SR Attachment record for Passport & Contact Details update SR - createSRAttachment()
1.1     |   11-11-2020      | Shubham Suryawanshi | Fixed the passport doc upload issue
***********************************************************************************************************************/

@RestResource(urlMapping='/uploadDocToSF/*')
global class InsertAttachment_API {

    public static String errorMsg;
    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        //200 => 'OK',
        //400 => 'Bad Request',
        //401 => 'Unauthorized',
        //500 => 'Internal Server Error' 
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };

    @HttpPost 
    global static FinalReturnWrapper apiMethod() {

        System.debug('inside apiMethod');
        
        FinalReturnWrapper retunResponse = new FinalReturnWrapper();
        cls_meta_data objMeta = new cls_meta_data();
        cls_data objData = new cls_data();
        FM_Case__c objFMCase = new FM_Case__c();
        Case objSRCase;

        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);

        if(!r.params.containsKey('documentName')) {
            objMeta.message = 'Missing parameter : documentName';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
            //objMeta.is_success = false;

            retunResponse.meta_data = objMeta;
            return retunResponse;
        }
        else if(!r.params.containsKey('filename')) {
            objMeta.message = 'Missing parameter : filename';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
            //objMeta.is_success = false;

            retunResponse.meta_data = objMeta;
            return retunResponse;
        }
        else if(r.params.containsKey('documentName') && String.isBlank(r.params.get('documentName'))) {
            objMeta.message = 'Missing parameter value: documentName';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
            //objMeta.is_success = false;

            retunResponse.meta_data = objMeta;
            return retunResponse;
        }
        else if(r.params.containsKey('filename') && String.isBlank(r.params.get('filename'))) {
            objMeta.message = 'Missing parameter value: filename';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
            //objMeta.is_success = false;  

            retunResponse.meta_data = objMeta;
            return retunResponse; 
        }
        else if(r.params.containsKey('attachmentId') && String.isBlank(r.params.get('attachmentId')) ) {
            objMeta.message = 'Missing parameter value: attachmentId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
            //objMeta.is_success = false;  

            retunResponse.meta_data = objMeta;
            return retunResponse;
        }
        else if(r.params.containsKey('fmCaseId') && String.isBlank(r.params.get('fmCaseId'))) {
            //call case creation method
            //objFMCase = createTRCase();
        }
        else if(r.params.containsKey('fmCaseId') && String.isNotBlank(r.params.get('fmCaseId'))) {
            //fetch Fm case;
            objFMCase = getTRCase(r.params.get('fmCaseId'));
        }
        else if(r.params.containsKey('fm_case_id') && String.isNotBlank(r.params.get('fm_case_id'))) {
            String caseId = r.params.get('fm_case_id');
            if(!(caseId instanceOf ID)){
        objMeta.message = 'Invalid parameter: fm_case_id is invalid';
        objMeta.status_code = 3;
        objMeta.title = mapStatusCode.get(3);
        objMeta.developer_message = null; 

        retunResponse.meta_data = objMeta;
        return retunResponse;
      }
            
            // fetch FM_Case record
            objFMCase = getTRCase(r.params.get('fm_case_id'));
            if(NULL == objFMCase) {
        objMeta.message = 'No FM_Case record was found matching given fm_case_id';
        objMeta.status_code = 2;
        objMeta.title = mapStatusCode.get(2);
        objMeta.developer_message = errorMsg; 

        retunResponse.meta_data = objMeta;
            }
            else {
                // upload document and set success/failure response
                retunResponse = uploadCOCDDocumentRemote(r, objFMCase.id, objFMCase.name, objFMCase.Account__r.Party_ID__c);
                
                if(retunResponse.meta_data.status_code == 1) {
                    // upsert SR_Attachments__c
                    String uploadedURL = retunResponse.data.attachment_details.attachment_url;
                    String attachDocId = r.params.get('attachmentId');
                    String docTypeName = r.params.get('documentName');
                    String docFileName = r.params.get('filename');
                    String strFileExtension = '';
                    if( String.isNotBlank(docFileName) && docFileName.lastIndexOf('.') != -1 ) {
                        strFileExtension = docFileName.substring(docFileName.lastIndexOf('.')+1, docFileName.length() );
                        strFileExtension = strFileExtension.substringBefore('&');
                    }
                    
                    SR_Attachments__c objCustAttach = new SR_Attachments__c();
                    if(String.isNotBlank(attachDocId)) {
                        objCustAttach.id = attachDocId;
                    }
                    objCustAttach.Name = docTypeName;
                    objCustAttach.Attachment_URL__c = uploadedURL;
                    objCustAttach.FM_Case__c = objFMCase.id;
                    
                    //Added to handle for POP case
                    if(docTypeName != NULL && docTypeName.containsIgnoreCase('pop document')) {
                        objCustAttach.Type__c = 'POP';
                    }
                    else {
                        objCustAttach.Type__c = strFileExtension;
                    }
                    
                    try{
                        upsert objCustAttach;
                        
                        retunResponse.data.attachment_details.id = objCustAttach.id;
                    }
                    catch(Exception ex){
                        system.debug('QueryException: ' + ex.getMessage());
                        /* No failure attributed for case update failure ??? */
                    }
                    
                    retunResponse.data.attachment_details.fm_case_id = objFMCase.id;
                    retunResponse.data.attachment_details.fm_case_number = objFMCase.name;
                } /* if(retunResponse.meta_data.status_code == 1) */
            } /* else */
            
      return retunResponse;
        }
        else if(r.params.containsKey('case_id') && String.isNotBlank(r.params.get('case_id'))) {
            /* checking conditions for `standard case` object record  */
          String caseId = r.params.get('case_id');
            if(!(caseId instanceOf ID)){
        objMeta.message = 'Invalid parameter: case_id is invalid';
        objMeta.status_code = 3;
        objMeta.title = mapStatusCode.get(3);
        objMeta.developer_message = null; 

        retunResponse.meta_data = objMeta;
        return retunResponse;
      }
            
            // fetch standard case record;
            objSRCase = getSRCase(r.params.get('case_id'));
            if(NULL == objSRCase){
                // set error response for standard case
        objMeta.message = 'No Draft Case record was found matching given case_id';
        objMeta.status_code = 2;
        objMeta.title = mapStatusCode.get(2);
        objMeta.developer_message = errorMsg; 

        retunResponse.meta_data = objMeta;
            }
            else {
                // upload document and set success/failure response
                retunResponse = uploadCOCDDocumentRemote(r, objSRCase.id, objSRCase.caseNumber, objSRCase.Account.Party_ID__c);
                
                if(retunResponse.meta_data.status_code == 1) {
                    String uploadedURL = retunResponse.data.attachment_details.attachment_url;
                    
                    // save returned URL to Case?
                    List<SR_Attachments__c> lstSRAttach = new List<SR_Attachments__c>();
                    String attachmentType = r.params.get('documentName');
                    Case srCase_updateURL  = new Case();
                    srCase_updateURL.id = objSRCase.id;
                    if(attachmentType != NULL && attachmentType.containsIgnoreCase('passport')) {
                        srCase_updateURL.Passport_File_URL__c = uploadedURL;
                        //As per bug 2667 - To create SR Attachmemt record
                        SR_Attachments__c objSRAttach = createSRAttachment(objSRCase.id, objSRCase.AccountId, retunResponse.data.attachment_details.file_extension, 'Customer Passport '+ System.today(), uploadedURL);
                        lstSRAttach.add(objSRAttach);
                    } 
                        
                    if(attachmentType != NULL && attachmentType.containsIgnoreCase('cocd_form')) {
                        srCase_updateURL.CRF_File_URL__c = uploadedURL;
                        //as per bug 2667 - To create SR Attachmemt record
                        SR_Attachments__c objSRAttach = createSRAttachment(objSRCase.id, objSRCase.AccountId, retunResponse.data.attachment_details.file_extension, 'Customer Signed CRF Form '+ System.today(), uploadedURL);
                        lstSRAttach.add(objSRAttach);
                    } 
                        
                    if(attachmentType != NULL && attachmentType.containsIgnoreCase('additional_document')) {
                        srCase_updateURL.Additional_Doc_File_URL__c = uploadedURL; 
                         //as per bug 2667 - To create SR Attachmemt record
                        SR_Attachments__c objSRAttach = createSRAttachment(objSRCase.id, objSRCase.AccountId, retunResponse.data.attachment_details.file_extension, 'Additional Document '+ System.today(), uploadedURL);
                        lstSRAttach.add(objSRAttach);
                    } 
                    
                     //Added to handle for POP case
                    if(attachmentType != NULL && attachmentType.containsIgnoreCase('pop document')) {
                        SR_Attachments__c objSRAttach = createSRAttachment(objSRCase.id, objSRCase.AccountId, 'POP', 'POP Document', uploadedURL);
                        lstSRAttach.add(objSRAttach);
                    }
                    
                    try{
                        update srCase_updateURL;
                        //to create SR_Attachment__c records;
                        if(!lstSRAttach.isEmpty() && lstSRAttach.size() > 0) {
                            insert lstSRAttach;
                        }
                    }
                    catch(Exception ex){
                        system.debug('QueryException: ' + ex.getMessage());
                        /* No failure attributed for case update failure ??? */
                    }
                    
                    retunResponse.data.attachment_details.case_id = objSRCase.id;
                    retunResponse.data.attachment_details.case_number = objSRCase.caseNumber;
                    //Added to handle for POP case
                    if(!lstSRAttach.isEmpty() && lstSRAttach.size() > 0) {
                      retunResponse.data.attachment_details.id = lstSRAttach[0].id;
                    }
                } /* if(retunResponse.meta_data.status_code == 1) */
            } /* else */
            
      return retunResponse;
        }

        if(objFMCase != null) {
            Blob body = r.requestBody;
            System.debug('body:: ' + body);
            String requestString = EncodingUtil.Base64Encode(body);
            System.debug('requestString: '+requestString);

            String strDocumentName = r.params.get('documentName');
            String attachmentId = r.params.containsKey('attachmentId') ? r.params.get('attachmentId') : '';
            System.debug('attachmentId:: ' + attachmentId);


            UploadMultipleDocController.data objResponse = new UploadMultipleDocController.data();
            list<UploadMultipleDocController.MultipleDocRequest> lstWrapper = new list<UploadMultipleDocController.MultipleDocRequest>();
            if(string.isNotBlank( requestString ) ) {
                lstWrapper.add( FM_Utility.makeWrapperObject(  requestString,
                                                               strDocumentName ,
                                                               r.params.get('filename') ,
                                                               objFMCase.Name, '1' ) );
            }
            System.debug('== lstWrapper =='+lstWrapper);
            if( !lstWrapper.isEmpty() ) {
                objResponse = PenaltyWaiverService.uploadDocumentsOnCentralRepo( lstWrapper, 'FM' );
                system.debug('== objResponse document upload =='+objResponse);
                system.debug('== objResponse.data =='+objResponse.data);
                if( objResponse != NULL && objResponse.data != NULL ) {
                    objData.attachment_details = insertCustomAttachment( objFMCase, strDocumentName, objResponse, attachmentId, r.params.get('filename'));
                    objMeta.status_code = 1;
                    objMeta.message = 'Successful';
                }
                else {
                    objMeta.status_code = 2;
                    objMeta.message = 'Failed to upload doc to Office365';
                }
            }
        }
        else {
            objMeta.status_code = 2;
            objMeta.message = 'No FM Case found for given fmCaseId';
        }

        //objMeta.is_success = objMeta.status_code == 1 ? true : false;
        objMeta.title = mapStatusCode.get(objMeta.status_code);
        objMeta.developer_message = null;

        retunResponse.data = objData;
        retunResponse.meta_data = objMeta;

        System.debug('retunResponse:: ' + retunResponse);

        return retunResponse;

    }
    
    private static FinalReturnWrapper uploadCOCDDocumentRemote(RestRequest r, String caseId, String caseNumber, String accPartyId){
        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        cls_meta_data objMeta = new cls_meta_data();
        cls_data objData = new cls_data();
        
        Blob body = r.requestBody;
        String requestString = EncodingUtil.Base64Encode(body);
        if(String.isBlank(requestString)){
            objMeta.message = 'Attached document is empty';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null; 
            
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        
        String fileName = r.params.get('filename');
        String nameOnly = fileName.substring(fileName.lastIndexOf('\\')+1 ).substringBefore('.');
        String strFileExtension = '';
        if(String.isNotBlank(fileName) && fileName.lastIndexOf('.') != -1) {
            strFileExtension = fileName.substring(fileName.lastIndexOf('.') + 1 , fileName.length());
        }
        String currentTimeMilli = String.valueOf(System.currentTimeMillis());
        
        List<UploadCOCDMultipleDocController.MultipleDocRequest> lstMultipleDocReq = new List<UploadCOCDMultipleDocController.MultipleDocRequest>();
        UploadCOCDMultipleDocController.MultipleDocRequest objMultipleDocRequest = new UploadCOCDMultipleDocController.MultipleDocRequest();
        objMultipleDocRequest.category = 'Document';
        objMultipleDocRequest.entityName = 'Damac Service Requests';
        objMultipleDocRequest.base64Binary = requestString;
        objMultipleDocRequest.fileDescription = r.params.get('documentName');
        objMultipleDocRequest.fileId = caseNumber + '-' + currentTimeMilli + '.' + strFileExtension;
        objMultipleDocRequest.fileName = caseNumber + '-' + currentTimeMilli + '.' + strFileExtension;
        
        objMultipleDocRequest.partyId = caseNumber;
        objMultipleDocRequest.sourceId = 'IPMS-' + accPartyId + '-' + nameOnly;
        objMultipleDocRequest.sourceFileName = 'IPMS-' + accPartyId + '-' + nameOnly + '.' + strFileExtension;
        lstMultipleDocReq.add(objMultipleDocRequest);
        
        UploadCOCDMultipleDocController.data objResponse;
        String exceptionMesage = '';
        try{
          objResponse = UploadCOCDMultipleDocController.getMultipleDocUrl(lstMultipleDocReq);
        }
        catch(Exception ex){
            system.debug('QueryException: ' + ex.getMessage());
            exceptionMesage = ex.getStackTraceString(); /* ex.getMessage() */
            objResponse = NULL;
        }
        
        if(NULL == objResponse || NULL == objResponse.data || objResponse.data.size() < 1){
            objMeta.message = 'Failed to upload doc to Office365.';
            objMeta.status_code = 2;
            objMeta.title = mapStatusCode.get(2);
            objMeta.developer_message = exceptionMesage; 
            
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else {
            String uploadedURL = objResponse.data[0].url;
          
            /* Build Success Response */
            objMeta.message = 'Success in upload of doc to Office365.';
            objMeta.status_code = 1;
            objMeta.title = mapStatusCode.get(1);
            objMeta.developer_message = null; 
            returnResponse.meta_data = objMeta;
            
            cls_data responseDataWrapper = new cls_data();
          AttachmentWrapper attachmentData = new AttachmentWrapper();
            attachmentData.attachment_url = uploadedURL;
            attachmentData.file_extension = strFileExtension;
            responseDataWrapper.attachment_details = attachmentData;
            returnResponse.data = responseDataWrapper;
        }
        
        return returnResponse;
    }

    public static Case getSRCase(String caseId) {
        Case srCase;
        try{
            srCase = [SELECT id, caseNumber, recordType.name, Type, Origin, SR_Type__c, AccountId, 
                      Account.name, Account.Party_ID__c, Account.Party_Type__c, CRF_File_URL__c, 
                      Passport_File_URL__c, Additional_Doc_File_URL__c, OD_File_URL__c, POA_File_URL__c 
                      FROM Case WHERE id = :caseId 
                      AND status = 'Draft Request'];
        }
        catch(Exception ex){
            system.debug('QueryException: ' + ex.getMessage());
            errorMsg = ex.getMessage();
        }
        
        return srCase;
    }

    //public static FM_Case__c createTRCase() {

    //    FM_Case__c objCase = new FM_Case__c();
    //    objCase.Status__c = 'New';
    //    objCase.Origin__c = 'Portal - Guest';
    //    objCase.Request_Type__c = 'Tenant Registration';
    //    objCase.Request_Type_DeveloperName__c = 'Tenant_Registration';
    //    objCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Registration').getRecordTypeId();

    //    try {
    //        insert objCase;
    //    }
    //    catch(Exception e) {
    //        System.debug('QueryException e: ' + e.getMessage());
    //        errorMsg = e.getMessage();
    //    }

    //    System.debug('objCase:: ' + objCase);
        
    //    return objCase;
    //}

    public static FM_Case__c getTRCase(String caseId) {
        System.debug('caseId: '+caseId);

        List<FM_Case__c> lstFmCase = new List<FM_Case__c>();
        try {
            lstFmCase = [SELECT id, Name, Account__r.Party_ID__c FROM FM_Case__c WHERE Id =: caseId];
        }   
        catch(QueryException e) {
            System.debug('QueryException e: ' + e.getMessage());
            errorMsg = e.getMessage();
        } 
        
        System.debug('lstFmCase: '+lstFmCase);

        return lstFmCase.size() > 0 ? lstFmCase[0] : null;
    }


    public static AttachmentWrapper insertCustomAttachment( Fm_Case__c fmCase, String docName , UploadMultipleDocController.data objResponse, String attachmentId, String fileName) {
        AttachmentWrapper objAttachWrap = new AttachmentWrapper();
        list<SR_Attachments__c> lstCustomAttachments = new list<SR_Attachments__c>();

        System.debug('fileName:: ' + fileName);

        String strFileExtension;
        if( String.isNotBlank( fileName ) && fileName.lastIndexOf('.') != -1 ) {
            strFileExtension = fileName.substring( fileName.lastIndexOf('.')+1 , fileName.length() );
            strFileExtension = strFileExtension.substringBefore('&');
        }

        System.debug('strFileExtension:: ' + strFileExtension);
        
        for( UploadMultipleDocController.MultipleDocResponse objFile : objResponse.data ) {
            SR_Attachments__c objCustAttach = new SR_Attachments__c();

            if(String.isNotBlank(attachmentId)) {
                objCustAttach.id = attachmentId;
            }
            //objCustAttach.Account__c = strAccountId ;
            objCustAttach.Name = docName;
            objCustAttach.Attachment_URL__c = objFile.url;
            //objCustAttach.Booking_Unit__c = strSelectedUnit ;
            objCustAttach.FM_Case__c = fmCase.Id ;
            objCustAttach.Type__c = strFileExtension;
            lstCustomAttachments.add(objCustAttach);
        }

        System.debug('lstCustomAttachments:: ' + lstCustomAttachments);
        if( !lstCustomAttachments.isEmpty() ) {
            try {
                upsert lstCustomAttachments;

                objAttachWrap.fm_case_id = fmCase.Id;
                objAttachWrap.fm_case_number =fmCase.Name;
                objAttachWrap.id = lstCustomAttachments[0].id; 
                objAttachWrap.attachment_url = lstCustomAttachments[0].Attachment_URL__c;
                objAttachWrap.file_extension = lstCustomAttachments[0].Type__c;
            }
            catch(Exception e) {
                System.debug('QueryException e: ' + e.getMessage());
                errorMsg = e.getMessage();
            }
            
        }
        System.debug('objAttachWrap:: ' + objAttachWrap);
        return objAttachWrap;
    }

    public static SR_Attachments__c createSRAttachment(String caseId, String accountId, String strType, String strAttachmentName, String fileUrl) {
        SR_Attachments__c srAttchmentObj = new SR_Attachments__c(Case__c = caseId,Account__c = accountId,Type__c = strType,Name=strAttachmentName, Attachment_URL__c = fileUrl);
        return srAttchmentObj;
    }


    //HttpGet method for returning preview URL
    @HttpGet
    global static FinalReturnWrapperForURL GetPreviewURL () {
    
        FinalReturnWrapperForURL  returnResponse = new FinalReturnWrapperForURL ();
        cls_data_url  objDataURL = new cls_data_url();
        cls_meta_data_url  objMetaDataURL = new cls_meta_data_url ();
    
        RestRequest r = RestContext.request;
        SYstem.debug('r in Get preview URL:: ' + r);
    
        if(!r.params.containsKey('attachmentURL')) {
    
            objMetaDataURL.message = 'Missing parameter : attachmentURL';
            objMetaDataURL.status_code =  3;
            objMetaDataURL.title = mapStatusCode.get(3);
            objMetaDataURL.developer_message = null;
    
            returnResponse.meta_data = objMetaDataURL;
            return returnResponse;
        }
        else if(r.params.containsKey('attachmentURL') && String.isBlank(r.params.get('attachmentURL'))) {
            objMetaDataURL.message = 'Missing parameter value: attachmentURL';
            objMetaDataURL.status_code = 3;
            objMetaDataURL.title = mapStatusCode.get(3);
            objMetaDataURL.developer_message = null;
    
            returnResponse.meta_data = objMetaDataURL;
            return returnResponse;
        }
        
    
        if(String.isNotBlank(r.params.get('attachmentURL'))) {
            
            String requestURL = r.params.get('attachmentURL');
            String idValue = requestURL.substringAfter('id=');

            try {

                Office365RestService.ResponseWrapper objWrap = Office365RestService.downloadFromOffice365ById(idValue ,'','','','','');
                System.debug('preview URL : ' + objWrap.downloadUrl);

                objMetaDataURL.message = 'successful';
                objMetaDataURL.status_code = 1;
                objDataURL.doc_preview_url = objWrap.downloadUrl;
                //Eg : 'https://damacgroup-my.sharepoint.com/personal/crm_docs_damacgroup_com/_layouts/15/embed.aspx?uniqueId=1935049e-c9dd-4cee-b232-a95b1380a23a&access_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJhdWQiOiIwMDAwMDAwMy0wMDAwLTBmZjEtY2UwMC0wMDAwMDAwMDAwMDAvZGFtYWNncm91cC1teS5zaGFyZXBvaW50LmNvbUA1N2U0MzQyOS00MjhhLTQ5NzMtOTcyMC1lNjkwN2FlOTRkZWYiLCJpc3MiOiIwMDAwMDAwMy0wMDAwLTBmZjEtY2UwMC0wMDAwMDAwMDAwMDAiLCJuYmYiOiIxNTk0NzE5OTY3IiwiZXhwIjoiMTU5NDcyMDI2NyIsImVuZHBvaW50dXJsIjoiOFdoSTBwRFVMRllLT1crMUhKaDA0THRGMXQwQnZieGIwNXB1d1dqdDc4VT0iLCJlbmRwb2ludHVybExlbmd0aCI6IjEzOCIsImlzbG9vcGJhY2siOiJUcnVlIiwiY2lkIjoiTURZMk5UTmhNbVV0WkdNeU5DMDBNR0l4TFRnNU5XRXRZVEUzT0RCbFl6Y3lZMkU1IiwidmVyIjoiaGFzaGVkcHJvb2Z0b2tlbiIsInNpdGVpZCI6IlltTmtNbVJrWlRRdE9EQXpPUzAwTVRka0xUZ3hPRGt0TURJeFlXTTVOVEV4TmpJMSIsImFwcF9kaXNwbGF5bmFtZSI6IkNSTSIsIm5hbWVpZCI6IjQxOGM3MmM3LWU0OTgtNDExNC1hMWIyLTdmYmZkNzg1OTZhMUA1N2U0MzQyOS00MjhhLTQ5NzMtOTcyMC1lNjkwN2FlOTRkZWYiLCJyb2xlcyI6ImFsbHNpdGVzLnJlYWQgYWxsZmlsZXMud3JpdGUgYWxsZmlsZXMucmVhZCBhbGxwcm9maWxlcy5yZWFkIiwidHQiOiIxIiwidXNlUGVyc2lzdGVudENvb2tpZSI6bnVsbH0.eGxwSGVodEV1MTlYKzBXRlFrbDFlSFAwb2tkZWJSdHNaRllHUHI0cHY5VT0';

            }
            catch(Exception e) {
                objMetaDataURL.message = e.getMessage();
                objMetaDataURL.status_code = 2;                

            }
            
        }
        
        objMetaDataURL.title = mapStatusCode.get(objMetaDataURL.status_code);
        objMetaDataURL.developer_message = null;
    
        returnResponse.data = objDataURL;
        returnResponse.meta_data = objMetaDataURL;

        System.debug('returnResponse: ' + returnResponse);
        return returnResponse;
    
    }

    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_data {
        public AttachmentWrapper attachment_details ;
    }

    public class cls_meta_data {
        //public Boolean is_success;
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message;   
    }

    public class AttachmentWrapper {
        public String fm_case_id;
        public String fm_case_number;
        public String id;
        public String attachment_url;
        public String file_extension;
        public String case_id;
        public String case_number;
    }

    /*For HttpGet method*/
    global class FinalReturnWrapperForURL {
        public cls_data_url data;
        public cls_meta_data_url meta_data;
    }

    public class cls_data_url {
        public String doc_preview_url;
    }

    public class cls_meta_data_url {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message; 
    }
}