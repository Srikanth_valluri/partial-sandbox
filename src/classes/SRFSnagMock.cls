public class SRFSnagMock implements HttpCalloutMock{
    public HTTPResponse respond(HTTPRequest req) {
        HTTPResponse res = new HTTPResponse();
        System.debug('Inside Mock class');
        if(req.getEndpoint().startsWith('https://damac.onsiteapp.co')){
            System.debug('Inside onsiteapp');
            res.setBody('Test PDF');
            res.setStatusCode(200);
            res.setHeader('Content-Type', 'application/pdf');
        }else if(req.getEndpoint() == 'https://api.signnow.com/oauth2/token'){
            res.setBody('{ "access_token": "6cd862b817ca919e78d165c4615d1e1031cacd8bde1e6701c748e0abc14708f0" }');
            res.setStatusCode(200);    
        }else if(req.getEndpoint() == 'https://api.signnow.com/document/fieldextract'){
            res.setBody('{ "id": "b5f53127b7174b8e84661785edf8ec7d54013119"}');
            res.setStatusCode(200);
        }else if(req.getEndpoint().startsWith('https://api.signnow.com/document/') &&
                 req.getEndpoint().endsWith('/invite')){
            res.setBody('{ "status": "success"}');
            res.setStatusCode(200);
        }else if(req.getEndpoint().startsWith('https://api.signnow.com/document/')){
            res.setBody('{"id":"938efe81e8fd422eab00c72d6a6c1839949cdf45","signatures":[],"texts":[],"checks":[],"views":[],"attachments":[],"radiobuttons":[],"hyperlinks":[]}');
             res.setStatusCode(200);
        }else if(req.getEndpoint() == 'https://api.signnow.com/api/v2/events'){
            res.setBody('{ "status": "success" }');
            res.setStatusCode(200);
        }else if(req.getEndpoint().endsWith('token')) {
            res.setBody('{'+
                        '"token_type": "Bearer",'+  
                        '"expires_in": "3600",'+
                        '"ext_expires_in": "3600",'+
                        '"expires_on": "1570628201",'+
                        '"not_before": "1570624301",'+
                        '"resource": "https://graph.microsoft.com",'+
                        '"access_token": "eyJ0eXAiOiJKV1QiLCJub25jZSI6IkN3MVpSbEFnNTZKaUt4Mkd2Tl"'+
                        '}');
            res.setStatusCode(200);
         }else if(req.getEndpoint().endsWith('content')) {
             res.setBody('{'+
                         '"@microsoft.graph.downloadUrl": "https://damacgroup-my.sharepoint.com/personal/crm_docs_damacgroup_com/_layouts/15/download",'+
                         '"id": "123456789A",'+
                         '"webUrl": "https://damacgroup-my.sharepoint.com/personal/crm_docs_damacgroup_com/Documents/test.pdf"'+
                         '}');
             res.setStatusCode(200);
         }
        return res;
    }
}