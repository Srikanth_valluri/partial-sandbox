Global class Damac_ParseCSVToResetInquiry {

    webservice static void ParseCSVRecord (ID assignmentRuleId) {
        Attachment att = new Attachment();
        att = [SELECT Name, Body FROM Attachment WHERE ParentId =: assignmentRuleId AND Name LIKE '%ids%' LIMIT 1];
        
        Inquiry_Assignment_Rules__c rule = [SELECT Rule_Running_Status__c, Job_Id__c, Rule_Executed_Date__c, Write_to_CSV__c,Query_Limit__c FROM Inquiry_Assignment_Rules__c WHERE Id =: assignmentRuleId LIMIT 1];
        
        String body = att.body.toString ();

        Set<ID> inqIds = new Set<ID>();
        if (body.contains (',')) {
            Integer nextIndex = 0;
            String val = '';
            Integer count = body.countMatches (',');
            body = body.replaceAll( '\\s+', '');
            for (integer i = 0; i <= count; i++) {                
                if (count == i) {
                    val = body.substringAfterLast (',');
                } else {
                    if (nextIndex != 0) { 
                        val = body.substring (nextIndex, body.indexOf (',', nextIndex));
                    }
                    else {
                        val = body.substring (nextIndex, body.indexOf (','));
                    }
                }
                try {
                    val = val.trim();
                    if (val != '')
                    
                        inqIds.add (val);    
                } catch (Exception e) {}
                nextIndex = nextIndex + val.length ()+1;
            } 
        } else {
            inqIds.add(body);
        }
        System.Debug(inqIds.size());
        //ID jobID = Database.executeBatch(new Damac_ResetInquiry(inqIds, assignmentRuleId, rule.Write_to_CSV__c), 1);
        ID jobID;
        if(rule.Query_Limit__c >0 && rule.Query_Limit__c!= null){
            jobID = Database.executeBatch(new Damac_ResetInquiry(inqIds, assignmentRuleId, rule.Write_to_CSV__c), integer.valueof(rule.Query_Limit__c)); 
        }else{
            jobID = Database.executeBatch(new Damac_ResetInquiry(inqIds, assignmentRuleId, rule.Write_to_CSV__c),1); 
        }
        
        rule.Rule_Running_Status__c = 'Started';
        rule.Job_Id__c = jobId;
        update rule;
           
    }
}