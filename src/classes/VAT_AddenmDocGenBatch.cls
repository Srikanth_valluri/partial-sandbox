/**
 * @File Name          : VAT_AddenmDocGenBatch.cls
 * @Description        : This Batch is used to generate VAT Addendum and cover letter from
 *==============================================================================================
 * Ver         Date                     Author                    Modification
 * 
 *==============================================================================================
 * 1.0    6/25/2019                   Aishwarya Todkar           Initial Version
**/
public without sharing class VAT_AddenmDocGenBatch implements Database.Batchable<sObject>,
                                                                Database.AllowsCallouts, Database.Stateful {
    
    private String query;
    public String SERVER_URL;
    public String SESSION_ID;
    public String recordId;
    public Database.Querylocator start( Database.BatchableContext bc ) {
        
        // Calling Login method to access drawloop. 
        login();  
        
        query = 'SELECT' +
                                        ' Id'+
                                        ', Permitted_Use_Type__c'+
                                        ', Handover_Status__c'+
                                        ', VAT_Addendum_Generated__c'+
                                        ', Is_Cover_Letter_Generated__c'+
                                    ' FROM'+
                                        ' Booking_Unit__c'+
                                    ' WHERE'+
                                        ' Generate_and_Send_Addendum__c = true'+
                                    ' AND'+
                                        ' VAT_Email_Sent__c = false'+
                                    ' AND'+
                                        ' Permitted_Use_Type__c != null' +
                                    ' AND'+
                                        ' Handover_Status__c != null';
                if( Label.VAT_AddenmDocGenBatchLabel.contains('-') 
                && Label.VAT_AddenmDocGenBatchLabel.contains('ON') 
                && Label.VAT_AddenmDocGenBatchLabel.split('-',2)[0] == 'ON'){
                    recordId =  Label.VAT_AddenmDocGenBatchLabel.split('-',2)[1];
                    query +='  AND ID = :recordId';
                }
                //query += ' FOR UPDATE';
                System.debug('recordId-'+ recordId);
        System.debug('VAT_AddenmDocGenBatch query ::: '+Database.getQueryLocator(query));
        return Database.getQueryLocator(query);
    }
    public void execute( Database.BatchableContext bc, list<Booking_Unit__c> listBu ) {
        system.debug('SESSION_ID*********'+SESSION_ID);
        system.debug('listBu*********'+listBu);
        // Call a method to create attachment of cover letter using drawLoop for DP Invoice.
        if( listBu!= NULL && !listBu.isEmpty() )
        createAttachment(listBu);
    }
    public void finish( Database.BatchableContext bc ) {
        
        
    }

    // This Method is used to create attachment of cover letter on dp invoice.
    public void createAttachment(list<Booking_Unit__c> listBu){
        
        List<Booking_Unit__c> listBUToUpdate = new List<Booking_Unit__c>();
        
        Riyadh_Rotana_Drawloop_Doc_Mapping__c dateCS 
            = Riyadh_Rotana_Drawloop_Doc_Mapping__c.getValues('Unit with HO VAT Addendum');
        Riyadh_Rotana_Drawloop_Doc_Mapping__c constructionCS 
            = Riyadh_Rotana_Drawloop_Doc_Mapping__c.getInstance('Unit w/o HO VAT Addendum');
        if(SESSION_ID != null && SESSION_ID != ''){
            system.debug('listBu*********'+listBu);
            for( Booking_Unit__c objBu : listBu ){
            try {
                Loop.loopMessage lm = new Loop.loopMessage();
                Map<string, string> variables = new map<String,String>(); 
                System.debug(' dateCS : ' + dateCS);
                System.debug(' constructionCS : ' + constructionCS);
                    System.debug(' out side for ');
                    
                    if(objBu.Permitted_Use_Type__c.equalsIgnoreCase( 'Units With VAT' )
                        && dateCS != Null
                        && String.isNotBlank(dateCS.Delivery_Option_Id__c)
                        && String.isNotBlank(dateCS.Drawloop_Document_Package_Id__c) ){
                        System.debug(' for Date ');
                        variables.put('deploy', dateCS.Delivery_Option_Id__c); 
                        lm.requests.add(new Loop.loopMessage.loopMessageRequest(
                                        objBu.Id,
                                        dateCS.Drawloop_Document_Package_Id__c,
                                        variables)
                        );

                    }else if (  objBu.Permitted_Use_Type__c.equalsIgnoreCase( 'Units Without VAT' )  
                                 && constructionCS != Null
                                && String.isNotBlank(constructionCS.Delivery_Option_Id__c)
                                && String.isNotBlank(constructionCS.Drawloop_Document_Package_Id__c) ){
                        System.debug(' for Construction ');
                        variables.put('deploy', constructionCS.Delivery_Option_Id__c); 
                        lm.requests.add(new Loop.loopMessage.loopMessageRequest(
                                        objBu.Id,
                                        constructionCS.Drawloop_Document_Package_Id__c,
                                        variables)
                        );
                        
                    }
                
                
                    String response = lm.sendAllRequests();
                    system.debug('== response ==' +response );
                    lm.batchNotification = Loop.loopMessage.Notification.ON_COMPLETE;
                    system.debug('== lm.batchNotification ==' +lm.batchNotification );


                    listBUToUpdate.add( new Booking_Unit__c(Id = objBU.Id, Generating_VAT_From_Batch__c=true));
                    System.debug( 'listBUToUpdate--' + listBUToUpdate);
                    if( !listBUToUpdate.isEmpty() )
                        update listBUToUpdate; 

                } 
                catch (exception e){
                    system.debug('== e exception==' +e.getMessage() );
                }
            }
        }

    }
    //Method used for login purpose to get the session id
    public void login(){
        Admin_Login_for_Drawloop__c mc = Admin_Login_for_Drawloop__c.getOrgDefaults();
        Http h = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://' + mc.Domain__c + '.salesforce.com/services/Soap/u/28.0');
        system.debug('mc.Domain__c'+mc.Domain__c);
        system.debug('mc.Username__c'+mc.Username__c);
        system.debug('mc.Password__c'+mc.Password__c);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        request.setHeader('SOAPAction', '""');
        request.setBody('<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Header/><Body><login xmlns="urn:partner.soap.sforce.com"><username>' + mc.Username__c + '</username><password>' + mc.Password__c + '</password></login></Body></Envelope>');

        HttpResponse response = h.send(request);
        system.debug('----Response Body-----'+response.getBody());

        Dom.XmlNode resultElmt = response.getBodyDocument().getRootElement()
        .getChildElement('Body','http://schemas.xmlsoap.org/soap/envelope/')
        .getChildElement('loginResponse','urn:partner.soap.sforce.com')
        .getChildElement('result','urn:partner.soap.sforce.com');

        SERVER_URL = resultElmt.getChildElement('serverUrl','urn:partner.soap.sforce.com').getText().split('/services')[0];
        SESSION_ID = resultElmt.getChildElement('sessionId','urn:partner.soap.sforce.com').getText();

        system.debug('--SERVER_URL---'+SERVER_URL);
        system.debug('--SESSION_ID---'+SESSION_ID);
        if(Test.isRunningTest()){
            SESSION_ID = 'none';
        }
    }

}