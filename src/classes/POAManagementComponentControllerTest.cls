/*
 * Description - Test class for POAManagementComponentController
 *
 * Version        Date            Author            Description
 * 1.0            27/03/18        Vivek Shinde      Initial Draft
 */
@isTest
private class POAManagementComponentControllerTest {
    static Account  objAcc;
    static Case objCase;
    static Booking__c objBooking;
    static Booking_Unit__c objBookingUnit;
    static NSIBPM__Service_Request__c objSR;
    static Booking_Unit_Active_Status__c objBUActive;
    static POA__c objPOA;
    
    static void initialMethod() {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person',party_ID__C='123456');
        insert objAcc;
        
        objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'Test Unit';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        insert objBookingUnit;
        
        objBUActive = new Booking_Unit_Active_Status__c ();
        objBUActive.name = 'Test Active Status';
        objBUActive.Status_Value__c = 'Agreement executed by DAMAC'; 
        insert objBUActive; 
        
        /*Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Token Refund').getRecordTypeId();
        objCase = new Case();
        objCase.AccountID = objAcc.id ;
        objCase.RecordTypeID = devRecordTypeId;
        objCase.Total_Token_Amount__c = 1000;
        objCase.Excess_Amount__c = 5000;
        objCase.POA_Expiry_Date__c = System.today();
        objCase.IsPOA__c = true;
        insert objCase;*/
        
        objPOA = new POA__c();
        objPOA.Right_Type__c = 'Unit';
        objPOA.First_Name__c = 'TestFN';
        objPOA.Last_Name__c = 'TestLN';
        objPOA.Email_Address__c = 'test@test.com';
        insert objPOA;
        
        POA_Related_Account__c objPRA = new POA_Related_Account__c();
        objPRA.POA__c = objPOA.Id;
        objPRA.Related_to_Account__c = objAcc.Id;
        insert objPRA;
        
        POA_Unit__c objPU = new POA_Unit__c();
        objPU.POA_Related_Account__c = objPRA.Id;
        objPU.POA_Right__c = 'test';
        insert objPU;
    }
    
    static testMethod void unitRightTypeTest() {
        initialMethod();
        
        PageReference pageRef = Page.POAManagementProcessPage;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id); 
        ApexPages.currentPage().getParameters().put('SRType', 'POA'); 
        POAManagementComponentController objClass = new  POAManagementComponentController();
        objClass.registerPOA();
        
        objClass.crfAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.crfAttachmentName = 'C/fakepath/Document_1.txt';
        objClass.PoaAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.PoaAttachmentName = 'C:/fakepath/Document_2.txt';
        objClass.otherAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.otherAttachmentName = 'C:/fakepath/Document_3.txt';       
        
        objClass.strPOAValidTillDate = '01/01/2019';
        objClass.strSelectedRightType = 'Unit';
        objClass.lstSelectedUnits.add(objBookingUnit.Id);
        objClass.lstSelectedRights = new List<String>();
        objClass.lstSelectedRights.add('Test right');
        
        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
        objClass.objPOA.Right_Type__c = 'Unit';
        objClass.objPOA.First_Name__c = 'TestFN';
        objClass.objPOA.Last_Name__c = 'TestLN';
        objClass.objPOA.Email_Address__c = 'test@test.com';
        
        //objClass.objPOA.Right_Type__c = 'Unit';
        objClass.objCase.Customer_First_Name__c = 'TestFN';
        objClass.objCase.Customer_Last_Name__c = 'TestLN';
        objClass.objCase.Email__c = 'test@test.com';
        
        objClass.savePOADetails();
        objClass.showUnitSelectionPanel();
        objClass.strPOAToEdit = objClass.objPOA.Id;
        objClass.editPOA();
        //objClass.deletePOA();
        objClass.cancelPOA();
        Test.stopTest();
        
        List<Task> lstTask = [Select Id, WhatId, Status, Assigned_User__c, Subject From Task];
        system.debug('--lstTask[0].Subject--'+lstTask[0].Subject);
        for(Task objTask: lstTask) {
            objTask.Status = 'Completed';
        }
        update lstTask;
        
        POARightTaskClosure.createPOAUnits(lstTask);
        POAUtility.getPOAListForAccount(objAcc.id);
        POAUtility.POA objPOA = new POAUtility.POA(objAcc.Id, '', '', null);
    }
    
    static testMethod void customerRightTypeTest() {
        initialMethod();
        
        PageReference pageRef = Page.POAManagementProcessPage;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id); 
        ApexPages.currentPage().getParameters().put('SRType', 'POA'); 
        POAManagementComponentController objClass = new  POAManagementComponentController();
        objClass.registerPOA();
        
        objClass.crfAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.crfAttachmentName = 'C/fakepath/Document_1.txt';
        objClass.PoaAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.PoaAttachmentName = 'C:/fakepath/Document_2.txt';
        objClass.otherAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.otherAttachmentName = 'C:/fakepath/Document_3.txt';       
        
        objClass.strPOAValidTillDate = '01/01/2019';
        objClass.strSelectedRightType = 'Customer';
        objClass.lstSelectedUnits.add(objBookingUnit.Id);
        objClass.lstSelectedRights = new List<String>();
        objClass.lstSelectedRights.add('Test right');
        
        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
        objClass.savePOADetails();
        Test.stopTest();
    }
}