/*
    Apex Class : Damac_CreateWeChatInquiryController 
    Test Class Name :Damac_WeChatServices_Test
    Functionality : When user do the followup in wechat, we are sending a public link with VF page for Inquiry creation.
                    If the user already fills the Email value, then we are displaying a message and not giving the edit option.
                    If not user can enter their details and those will update in related Inquiry record.
*/

global class Damac_CreateWeChatInquiryController {
    public Inquiry__c inq { get; set; }
    public String message { get; set; }
    public String msgType { get; set; }
    public String inqTitle { get; set; }
    
    public Damac_CreateWeChatInquiryController (ApexPages.StandardController stdController) {
        inq = new Inquiry__c ();
        inqTitle = '';
        inq = (Inquiry__c) stdController.getRecord();
        ID inqId = apexpages.currentpage().getparameters().get('id');
        System.Debug (inqId);
        if (inqId != NULL) {            
            
            inq = Database.Query ('SELECT '+getAllFields ('Inquiry__c')+' FROM Inquiry__c WHERE ID =: inqId');
            // To check whether user already fills the email value. if yes we are displaying a message and locking the form for edit.
            if (inq.Email__c != null) {
                message = Label.Wechat_Existing_Inquiry;
                msgType = 'success';
            }
        }
        System.Debug (stdController.view().getUrl());
    }
    // To save the inquiry details once user submits the form
    public void save () {
        System.Debug (inqTitle);
        try {
            WeChat_API_Settings__c settings = WeChat_API_Settings__c.getInstance (UserInfo.getUserId ());
            inq.Title__c = inqTitle;
            inq.Campaign__c = settings.Campaign__c;
            inq.Inquiry_Status__c = 'New';
            inq.Inquiry_Source__c = 'Social';
            inq.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
            
            if (inq.Id != NULL)
                update inq;
            
            
            message = Label.Wechat_Inquiry_Thank_you_message;
            msgType = 'success';
        } catch (Exception e) {
            System.Debug (e.getMessage());
            message = e.getMessage();
            msgType = 'error';
        }
    }
    // To clear the toaster notifications on the page.
    public void clearMessages () {
        message = '';
        msgType = '';
    }
    
     /************************************************************************************************
    * @Description : Utility Method to get all fields based on object name for query                *
    * @Params      : Object API                                                                     *
    * @Return      : Object related Field APIs as Comma seperated                                   *
    ************************************************************************************************/
    public static string getAllFields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        
        if (objectType == null)
            return fields;
        
        Map <String, Schema.SObjectField> fieldMap = objectType.getDescribe ().fields.getMap ();
        
        for (Schema.SObjectField sfield : fieldMap.Values()) {
            
            fields += sfield.getDescribe().getName ()+ ', ';
            
        }
        return fields.removeEnd(', '); 
    }
       

}