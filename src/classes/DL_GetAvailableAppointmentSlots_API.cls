/**********************************************************************************************************************
Description: This API is used for getting the Appointment slots for given date, purpose and sub-purpose
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   24-11-2020      | Subin Antony		  | Created initial draft
-----------------------------------------------------------------------------------------------------------------------
1.1     |   10-12-2020      | Subin Antony		  | Modified appointment slot listing logic to exclude booked slots
***********************************************************************************************************************/

@RestResource(urlMapping='/appointment/fetchAvailableSlots')
global class DL_GetAvailableAppointmentSlots_API {
    public static Map<Integer, String> statusCodeMap;
    static {
        statusCodeMap = new Map<Integer, String>{
            1 => 'Success',
            2 => 'Failure',
            3 => 'Bad Request',
            6 => 'Something went wrong',
            7 => 'Info'
        };
    }
	
    @HttpGet
    global static FinalReturnWrapper fetchAvailableAppointmentSlots() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        RestContext.response.addHeader('Content-Type', 'application/json');
        String errorMsg, exceptionMsg, successMsg;
        
        String processName = req.params.containskey('process') ? req.params.get('process') : '';
        String subProcessName = req.params.containskey('sub_process') ? req.params.get('sub_process') : '';
        if(String.isBlank(processName) || String.isBlank(subProcessName)) {
            errorMsg = 'Please select process and sub_process.';
            return getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
        }
        
        String buId = req.params.containskey('bu_id') ? req.params.get('bu_id') : '';
        if(String.isBlank(buId)){
            errorMsg = 'Please select a booking unit for booking appointment.';
            return getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
        }
        
        String appointmentDateString = req.params.containskey('appointment_date_string') ? req.params.get('appointment_date_string') : '';
        Date appointmentDate = NULL;
        try{
            if(String.isNotBlank(appointmentDateString)) {
                appointmentDate = Date.valueOf(appointmentDateString);
            } else {
                errorMsg = 'Please provide a proper appointment date string formatted as \'yyyy-mm-dd\'.';
                return getErrorResponse(3, statusCodeMap.get(3), errorMsg, NULL);
            }
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
            errorMsg = 'Please provide a proper appointment date string formatted as \'yyyy-mm-dd\'.';
            return getErrorResponse(3, statusCodeMap.get(3), errorMsg, ex.getMessage());
        }
        
        String accountId = req.params.containskey('account_id') ? req.params.get('account_id') : '';
        if(String.isBlank(accountId)){
            errorMsg = 'Please provide account_id.';
            return getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
        }
        
        Account accountRecord;
        try{
            accountRecord = [SELECT Id, Name, isPersonAccount, Email__pc, Email__c, 
                             Primary_CRE__c, Secondary_CRE__c, Tertiary_CRE__c, Primary_Language__c 
                             FROM Account WHERE Id = :accountId];
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
        	exceptionMsg = ex.getMessage();
            accountRecord = NULL;
        }
        if(NULL == accountRecord){
            errorMsg = 'The account_id provided does not match any records.';
            return getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
        }
        
        Booking_unit__c bookingUnit;
        try{
            bookingUnit = [Select Id, Name, Property_Name__c, Unit_Name__c, Registration_ID__c, Booking__c, Booking__r.Account__c 
                           FROM Booking_Unit__c WHERE Booking__r.Account__c = :accountId AND Id = :buId];
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
        	exceptionMsg = ex.getMessage();
            bookingUnit = NULL;
        }
        if(NULL == bookingUnit) {
            errorMsg = 'The booking unit ID provided does not match any valid records.';
            return getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
        }
        
        System.debug('....................FETCH_TIME_SLOTS: acc_id > ' + accountRecord.Id);
        System.debug('....................FETCH_TIME_SLOTS: bu_id > ' + buId);
        System.debug('....................FETCH_TIME_SLOTS: process > ' + processName);
        System.debug('....................FETCH_TIME_SLOTS: sub_process > ' + subProcessName);
        System.debug('....................FETCH_TIME_SLOTS: req_date > ' + appointmentDateString);
        
        AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
        List<AppointmentSelectionHandler.AppointmentWrapper> fetchedAppointments;
        try{
            fetchedAppointments = handler.availableSlots(accountRecord.Id, buId, processName, subProcessName, 
                                                         'Mobile', appointmentDateString, false); /* CRE / Portal / Mobile */
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
        	exceptionMsg = ex.getMessage();
            fetchedAppointments = NULL;
        }
        if(NULL == fetchedAppointments || fetchedAppointments.size() < 1) {
            errorMsg = 'Failed to fetch appointment slots for given date. Please try another date.';
            return getErrorResponse(2, statusCodeMap.get(2), errorMsg, exceptionMsg);
        }
        System.debug('....................FETCH_TIME_SLOTS: slot_count > ' + fetchedAppointments.size());
        
        cls_data[] availableSlots = new List<cls_data>();
        for(AppointmentSelectionHandler.AppointmentWrapper fetchedAppointment : fetchedAppointments) {
            Appointment__c objAppointment = fetchedAppointment.objApp;
            if(NULL == fetchedAppointment.objApp && String.isNotBlank(fetchedAppointment.ErrorMessage)) {
                errorMsg = fetchedAppointment.ErrorMessage;
                if(fetchedAppointment.ErrorMessage.containsIgnoreCase('only after 24 hours from the present time')) {
                    Datetime dt = DateTime.newInstance(appointmentDate, Time.newInstance(0, 0, 0, 0));
                    String dayOfWeek = dt.format('EEEE');
                    if(dayOfWeek == 'Wednesday' || dayOfWeek == 'Thursday' || dayOfWeek == 'Friday') 
                        errorMsg += ' Friday and Saturday are excluded for booking appointments.';
                }
                return getErrorResponse(2, statusCodeMap.get(2), errorMsg, null);
            }
            
            Calling_List__c appointmentCallingList = fetchedAppointment.objCL;
            if(NULL != appointmentCallingList && NULL != appointmentCallingList.id) {
                continue; /* skip appointments for which booking is already present for current date */
            }
            
            cls_data slot = new cls_data();
            slot.appointment_id = (NULL != objAppointment) ? objAppointment.id : null;
            slot.appointment_name = (NULL != objAppointment) ? objAppointment.name : null;
            slot.appointment_time_slot = (NULL != objAppointment) ? formatSlotString(objAppointment.Slots__c) : null;
            slot.alt_24hr_slot = (NULL != objAppointment) ? objAppointment.Slots__c : null;
            slot.time_slot_sub = (NULL != objAppointment) ? objAppointment.Time_Slot_Sub__c : null;
            slot.assigned_cre_name = (NULL != objAppointment) ? objAppointment.Assigned_CRE__r.Name : null;
            slot.assigned_cre_id = (NULL != objAppointment) ? objAppointment.Assigned_CRE__c : null;
            slot.calling_list_id = (NULL != appointmentCallingList) ? appointmentCallingList.id : null;
        	System.debug('....................FETCHED_TIME_SLOT: ResponseWrap > ' + slot);
            
            availableSlots.add(slot);
        }
        
        if(NULL == availableSlots || (NULL != availableSlots && availableSlots.size() < 1)) {
            errorMsg = 'All appointment slots for given date are exhausted. Please try with another date.';
            return getErrorResponse(2, statusCodeMap.get(2), errorMsg, exceptionMsg);
        }
        availableSlots.sort();
        
        FinalReturnWrapper responseWrapper = new FinalReturnWrapper();
        cls_meta_data responseMetaData = new cls_meta_data();
        responseMetaData.status_code = 1;
        responseMetaData.title = statusCodeMap.get(1);
        responseMetaData.message = 'Successfully fetched time slots available for appointment booking';
        responseWrapper.meta_data = responseMetaData;
        responseWrapper.data = availableSlots;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
        return responseWrapper;
    }
    
    public static String formatSlotString(String slotString_24H) {
        String formattedString = slotString_24H;
        try {
            String[] times = slotString_24H.split('-');
            String startTimeString = times[0].trim();
            String endTimeString = times[1].trim();
            
            String[] startTimeParts = startTimeString.split(':');
            Integer startHour = Integer.valueOf(startTimeParts[0].trim());
            Integer startMinute = Integer.valueOf(startTimeParts[1].trim());
            String startTimeFormatted = (startHour > 12 ? (startHour - 12) : startHour) + ':' + 
                (startMinute < 10 ? ('0'+startMinute) : (''+startMinute))  + (startHour >= 12 ? 'PM' : 'AM');
            
            String[] endTimeParts = endTimeString.split(':');
            Integer endHour = Integer.valueOf(endTimeParts[0].trim());
            Integer endMinute = Integer.valueOf(endTimeParts[1].trim());
            String endTimeFormatted = (endHour > 12 ? (endHour - 12) : endHour) + ':' + 
                (endMinute < 10 ? ('0'+endMinute) : (''+endMinute)) + (endHour >= 12 ? 'PM' : 'AM');
            
            formattedString = startTimeFormatted + ' - ' + endTimeFormatted;
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
        }
        return formattedString;
    }
    
    private static FinalReturnWrapper getErrorResponse(Integer statusCode, String title, 
    String responseMessage, String devMessage) {
        FinalReturnWrapper responseWrapper = new FinalReturnWrapper();
        cls_meta_data responseMetaData = new cls_meta_data();
        responseMetaData.status_code = statusCode;
        responseMetaData.title = title;
        responseMetaData.message = responseMessage;
        responseMetaData.developer_message = devMessage;
        responseWrapper.meta_data = responseMetaData;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
        return responseWrapper;
    }
    
    global class FinalReturnWrapper {
        public cls_meta_data meta_data;
        public cls_data[] data;
    }
    
    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message; 
    }
    
    public class cls_data implements Comparable {
        public String appointment_id;
        public String appointment_name;
        public String appointment_time_slot;
        public String alt_24hr_slot;
        public Decimal time_slot_sub;
        public String assigned_cre_name;
        public String assigned_cre_id;
        public String calling_list_id;
        
        public Integer compareTo(Object compareTo) {
            if(NULL == compareTo) {
                return -1;
            } 
            else {
                cls_data toCompare = (cls_data)compareTo;
                return this.alt_24hr_slot.compareTo(toCompare.alt_24hr_slot);
            }
        }
    }
}