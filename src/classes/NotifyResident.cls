/**
 * @File Name          : NotifyResident.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 11/17/2019, 1:55:35 PM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    11/17/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
//*****************************AKISHOR 02-Aug-2019: Generic SMS Service for Visitor Mgmt System***************************//
//*****VMS will call to pass below param and send SMS(resident) and OTP to visitor(Guest/contractor/Delivery Guy)*********//

@RestResource(urlMapping='/VisitorSMSService/*')
global with sharing class NotifyResident{

    global ResponseWrapper objRespWrapr{get;set;}

    global class MessageInput{
        global String UnitName='';
        global String VisitorName='';
        global String VisitorCompany='';
        global String VisitorType='';//Conttactor, DeliveryBoy, Guest
        global String VisitorMobile='';
        global String SMSState='Success';
    }
   @HttpPost
   global static ResponseWrapper NotifyResidentOwner( String UnitName
                                                    , String VisitorName
                                                    , String VisitorCompany
                                                    , String VisitorType
                                                    , String VisitorMobile
                                                    , String DocumentID
                                                    , String EntryType
                                                    , String ChekinDate
                                                    , String Nationality
                                                    , String Gender
                                                    , String Age
                                                    , String Designation
                                                    , String WatchList
                                                    , String VisitorLocation
                                                    , String VisitorRecId){
        
        ResponseWrapper objWrap = new ResponseWrapper ();   
      
        if( Label.VMS_Service_Controller.equalsIgnoreCase('Y') ) {
            if( String.isNotBlank( UnitName ) && UnitName.contains('-')) {
                if( UnitName.split('-').size() == 3 ) {
                    UnitName = UnitName.split('-')[2];
                }
            }
            List<Booking_Unit__c> BUList = [Select Id,Dummy_Booking_Unit__c, Inventory__r.Unit_Location__r.Id,Inventory__r.Building_Location__c
                                                    , Unit_Name__c, Total_Payment_Amount__c
                                                    , Bedroom_Type__c , Resident__c,Owner__c,Account_Id__c from Booking_Unit__c 
                                                                    where (Unit_Active__c='Active' OR Dummy_Booking_Unit__c =True) AND Unit_Name__c=:UnitName limit 1];        
            if(BUList.size()> 0) {  
            
            ResidentSMSService sendSMS = new ResidentSMSService();
            //if(BUList[0].Resident__c !=null){
                System.debug('-->> Notify Resident 4: ');
            objWrap = sendSMS.SendSMS2Resident(BUList[0].Resident__c
                                            , VisitorMobile
                                            , BUList[0].Total_Payment_Amount__c
                                            , VisitorType
                                            , BUList[0].Id
                                            , Age
                                            , Designation
                                            , WatchList
                                            , BUList
                                            , DocumentID
                                            , EntryType
                                            , Nationality
                                            , Gender
                                            , VisitorName,VisitorRecId, VisitorLocation, VisitorCompany
                                            );
                                      //}
                                      /*else 
                                      {
                                          //check for units w/o residents
                                            objWrap.errorMessage= 'No Resident in the selected unit';
                                            objWrap.status= 'OK';
                                            objWrap.statusCode='200';                
                                      }*/
                            }//end BuList
                            else {//check for inventory units AK: added on 28Sep2019
                            System.debug('-->> Notify Resident 5: ');
                            List<Inventory__c> InventList =[Select Id, Status__c from Inventory__c where Unit__c=: UnitName AND Status__c !='Sold' limit 1];
                                    if(InventList.size() > 0){
                                        System.debug('-->> Notify Resident 6: ');
                                                    /*objWrap.errorMessage= 'Inventory unit';
                                                    objWrap.status= 'OK';
                                                    objWrap.statusCode='200';  */
                                                    objWrap=sendSMS2Inv(InventList, VisitorMobile);                                                 
                                                }
                                                
                            }
        } else if( Label.VMS_Service_Controller.equalsIgnoreCase('N') ) {
            System.debug('-->> Notify Resident 7: ');
            objWrap.errorMessage = 'Service is not available , please contact system Administrator.';
        }
        System.debug('-->> Notify Resident 8: ');
         return objWrap;
                           
     } //end try        
       //for final response to VMS
       global static ResponseWrapper sendSMS2Inv(List<Inventory__c>InventList, String VisitorMobile){//for inv units
       System.debug('-->> Notify Resident 9: ');
             ResponseWrapper objWrp = new ResponseWrapper();    

            if(VisitorMobile !='' && InventList.size() > 0 &&  VisitorMobile.startsWith('00') ) {
            System.debug('-->> Notify Resident 10: ');
                               
                    HttpRequest req = new HttpRequest();
                    HttpResponse res = new HttpResponse();
                    Http http = new Http();
                    String user = ''; String passwd = '';String strSID = '';                    
                    String SMS ='';
                    SMS = 'Dear Visitor, Please share below OTP at reception for access.\n';
                    SMS = SMS +'OTP :-'+ String.valueOf(Math.random()).substring(8,12);  
                    
                    user = Label.FM_SMS_Service_Username; //'loams.';
                    passwd =  Label.FM_SMS_Service_Password; //'1@#$%qwert';
                    //strSID =  Label.FM_SMS_Service_SenderId; //'LOAMS';
                    strSID = SMSClass.getSenderName(user, VisitorMobile, false);

                    req.setMethod('POST' ); // Method Type
                    req.setEndpoint('http://api.smscountry.com/SMSCwebservice_bulk.aspx'); //SMS WebService
                    //string msgCont = GenericUtility.encodeChar(MsgContent);                                        
                    req.setBody('user='+ user + '&passwd=' + passwd +'&message=' + SMS + '&mobilenumber=' + VisitorMobile + '&sid='+strSID+ '&MTYPE=LNG&DR=Y');                   
                    res = http.send(req);
                    objWrp.errorMessage= 'Inventory unit';
                    objWrp.status= 'OK';
                    objWrp.statusCode='200'; 
                    System.debug('-->> Notify Resident 11: ');        
       }
       return objWrp;  
    }
        global class ResponseWrapper {
            global String status;
            global String statusCode;
            global String errorMessage; global String RecordId;
            global string OTP;
           
            global ResponseWrapper() {
                this.status = '';
                this.statusCode = '';
                this.errorMessage = '';
                this.RecordId = '';
                this.OTP='';
            }
        } 
                 
}//end of serviceng