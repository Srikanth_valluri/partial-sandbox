@isTest
public class FmHttpCalloutMock implements HttpCalloutMock {

    //Maps endpoint to response
    public Map<String, Response> responseMap;

    protected Integer code;
    protected String status;
    protected String body;
    protected Map<String, String> responseHeaders;

    public FmHttpCalloutMock(Integer code, String status, String body, Map<String, String> responseHeaders) {
        this.code = code;
        this.status = status;
        this.body = body;
        this.responseHeaders = responseHeaders;
    }

    public FmHttpCalloutMock(Integer code, String status, String body) {
        this.code = code;
        this.status = status;
        this.body = body;
    }

    public FmHttpCalloutMock(Map<String, Response> responseMap) {
        this.responseMap = responseMap;
    }


    public void setStatusCode(Integer code) {
        this.code = code;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setResponseHeaders(Map<String, String> responseHeaders) {
        this.responseHeaders = responseHeaders;
    }


    public HTTPResponse respond(HTTPRequest req) {

        HttpResponse res = new HttpResponse();

        if (responseMap != NULL) {
            Response response;
            for (String endpoint : responseMap.keySet()) {
                if (req.getEndpoint().containsIgnoreCase(endpoint)) {
                    response = responseMap.get(endpoint);
                    res.setBody(response.body);
                    res.setStatusCode(response.code);
                    res.setStatus(response.status);
                    if (response.responseHeaders != NULL) {
                        for (String key : response.responseHeaders.keySet()) {
                            res.setHeader(key, response.responseHeaders.get(key));
                        }
                    }
                    break;
                }
            }
            if (response == NULL) {
                res.setBody(this.body);
                res.setStatusCode(this.code);
                res.setStatus(this.status);
                if (this.responseHeaders != NULL) {
                    for (String key : this.responseHeaders.keySet()) {
                        res.setHeader(key, this.responseHeaders.get(key));
                    }
                }
            }
        } else {
            res.setBody(this.body);
            res.setStatusCode(this.code);
            res.setStatus(this.status);
            if (this.responseHeaders != NULL) {
                for (String key : this.responseHeaders.keySet()) {
                    res.setHeader(key, this.responseHeaders.get(key));
                }
            }
        }

        return res;
    }

    public class Response {
        public Integer code;
        public String status;
        public String body;
        public Map<String, String> responseHeaders;
        public Response(Integer code, String status, String body, Map<String, String> responseHeaders) {
            this.code = code;
            this.status = status;
            this.body = body;
            this.responseHeaders = responseHeaders;
        }

        public Response(Integer code, String status, String body) {
            this.code = code;
            this.status = status;
            this.body = body;
        }
    }

}