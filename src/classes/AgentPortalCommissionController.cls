public class AgentPortalCommissionController {
    private Contact contactInfo;
    private String accountName;
    public transient List<Booking_Unit__c> AgentBUList{set;get;}// transient keyword added by monali to avoid view state issue for now - 15/02/2018
    public Decimal totalUnitPrice{set;get;}
    public Decimal totalCommissionAmount{set;get;}
    public String filterValue {get;set;}
    public String errorMsg {get;set;}
    public final String queryFields = ' Agency_Name__c,Agent_Name__c,Unit_Name__c, Deal__c ,Name, '
                                    + ' Requested_Price_AED__c, Booking__r.Deal_SR__r.Name, Primary_Buyer_s_Name__c, '
                                    + ' Building_Name__c,Registration_ID__c,Booking__r.Deal_SR__r.DP_ok__c, ' 
                                    + ' Booking__r.Deal_SR__r.Doc_ok__c, Booking_Unit__c.Selling_Price__c, '
                                    + ' (SELECT Id,DP_Amount__c,Amount__c,Invoice_Number__c,Amount_Received__c, '
                                    + ' Commission_Status__c,DP_Amount_Received__c,Payment_Amount__c,Check_Date__c '
                                    + ' FROM Agent_Commissions__r) ';
/**************************************************************************************************
    Method:         AgentPortalCommissionController By Lovel Panchal
    Description:    Constructor executing model of the class 
**************************************************************************************************/

    public AgentPortalCommissionController() {
        resetTable();
    }


    public void resetTable() {
        filterValue = '';
        errorMsg = 'none';
        contactInfo = AgentPortalUtilityQueryManager.getContactInformation();
        Account acc = AgentPortalUtilityQueryManager.getAccountInformation(contactInfo.AccountId);
        //System.debug('acc>>>>'+acc);
        if(null != acc) {
            accountName = acc.Name;
            //System.debug('acc.Name -- Lovel'+accountName);
            AgentBUList = [SELECT Booking__r.Deal_SR__r.Name, Agency_Name__c,
                            Agent_Name__c,Unit_Name__c,Deal__c, Name,Requested_Price_AED__c,
                            Primary_Buyer_s_Name__c,Building_Name__c,Registration_ID__c,
                            Booking__r.Deal_SR__r.DP_ok__c,Booking__r.Deal_SR__r.Doc_ok__c, 
                                (SELECT Id,DP_Amount__c,Amount__c,Invoice_Number__c,Amount_Received__c,
                                Commission_Status__c,DP_Amount_Received__c,Payment_Amount__c,Check_Date__c 
                                FROM Agent_Commissions__r) 
                            FROM Booking_Unit__c where Agency_Name__c = :accountName 
                            AND (NOT SR_Status__c LIKE 'REJECTED%')];
            System.debug('AgentBUList -- Lovel'+AgentBUList);
            System.debug('AgentBUList -- size : '+AgentBUList.size());
        }
    }


    public void filterRecords() {
        //system.debug('Inside filterValue: ' + filterValue);
        errorMsg = 'none'; 
        AgentBUList = new List<Booking_Unit__c>();
        List<Id> buIdList = new List<Id>();
        if (String.isNotBlank(filterValue) && filterValue.length() >= 2) {
            String SOSL_search_string   = ' FIND \'*' 
                                        + filterValue 
                                        + '*\' IN ALL FIELDS '
                                        + ' RETURNING Booking_Unit__c ( ID ) ';
            List<List <Booking_Unit__c>> searchList = search.query(SOSL_search_string);
            
            //system.debug('Inside filterValue searchList: ' + searchList);
            if (!searchList.isEmpty() && searchList != null) {
                for (List<Booking_Unit__c> buObjList : searchList) {
                    for(Booking_Unit__c buObj : buObjList) {
                        buIdList.add(buObj.Id);
                    }
                }
                if (!buIdList.isEmpty()) {
                    String queryString  = ' SELECT '
                                        + queryFields 
                                        + ' FROM Booking_Unit__c '
                                        + ' WHERE Id IN :buIdList '
                                        + ' AND Agency_Name__c = \''
                                        + accountName 
                                        + '\' AND (NOT SR_Status__c LIKE \'REJECTED%\')'; 
                    //system.debug('Inside FILTER queryString: ' + queryString); 
                    AgentBUList = Database.query(queryString);
                    //system.debug('Inside FILTER AgentBUList: ' + AgentBUList); 
                }
            }
        } else {
            errorMsg = 'block';
        }
    }
 
}