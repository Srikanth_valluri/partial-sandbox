@isTest
public class SubmitLoggedInSR_APITest {
    @isTest
    static void testSubmitSr(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/submitLoggedInSr';  
        req.addParameter('recordType', 'Move In');
        req.requestBody= Blob.valueOf('');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        SubmitLoggedInSR_API.SubmitSR();
    }
    /*
    @isTest
    static void testSubmitSrMoveIn(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/submitLoggedInSr';  
        req.addParameter('recordType', 'Move In');
        req.requestBody= Blob.valueOf('test');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        SubmitLoggedInSR_API.SubmitSR();
    }*/
    
    @isTest
    static void testSubmitSrAmenity(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/submitLoggedInSr';  
        req.addParameter('recordType', 'Amenity Booking');
        req.requestBody= Blob.valueOf('test');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        SubmitLoggedInSR_API.SubmitSR();
    }
    
     @isTest
    static void testSubmitSrElse(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/submitLoggedInSr';  
        req.addParameter('recordType', 'test');
        req.requestBody= Blob.valueOf('test');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        SubmitLoggedInSR_API.SubmitSR();
    }
    
     @isTest
    static void testSubmitSrElse1(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/submitLoggedInSr';
        req.requestBody= Blob.valueOf('test');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        SubmitLoggedInSR_API.SubmitSR();
    }
}