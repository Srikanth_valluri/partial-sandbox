public class Resubmitcontroller {
    Case caseobj;
    public Resubmitcontroller(ApexPages.StandardController controller) {
        caseobj = (Case)controller.getrecord();
        System.debug('CAse obje in constructor ' + caseobj);
        caseobj= [Select Approval_Status__c,Status,recordtypeid,IPMS_Updated__c from Case where id =:caseobj.id limit 1]; 
        System.debug('CAse obje in constructor ' + caseobj);
    }
    
    public PageReference callUpdatedetails(){
        Id recId=PenaltyWaiverUtility.getRecordTypeId(PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE,PenaltyWaiverUtility.CASE_SOBJ);
        System.debug('CAse obje ' + caseobj);
        
        Set<id> setcaseId = new Set<id>();
        setcaseId.add(caseobj.id);
        list<Case> lstCase = PenaltyWaiverUtility.getCaseMap( setCaseId ).values() ; 
        list<Task> lstTaskToBeInserted = new list<Task>();
        list<Case> lstCasesToBeUpdated = new list<Case>();
        list<Error_Log__c> lstLogsGenerated = new list<Error_Log__c>();
        for( Case objCase : lstCase ) {
            if(objCase.Approval_Status__c != null && objCase.Approval_Status__c.equalsIgnoreCase('Approved')) {
            if( PenaltyWaiverHelper.isPenaltyChargedLessEqual( objCase.Booking_Unit__r.Registration_ID__c, objCase.Approved_Amount__c ) ) { 
                PenaltyWaiverDetails.APPSXXDC_PROCESS_SERX1794747X1X5 objInnerWrap = new PenaltyWaiverDetails.APPSXXDC_PROCESS_SERX1794747X1X5();
                objInnerWrap.ATTRIBUTE1 = String.valueOf( objCase.Approved_Amount__c );
                objInnerWrap.PARAM_ID = objCase.Booking_Unit__r.Registration_ID__c ;
                actionComUpdated.PenaltyWaiverHttpSoap11Endpoint objCallout = new actionComUpdated.PenaltyWaiverHttpSoap11Endpoint(); 
                String response = objCallout.UpdatePenaltyWaived( '2- ' +String.valueOf( system.now() ), 'WAIVE_PENALTY', 'SFDC', new list<PenaltyWaiverDetails.APPSXXDC_PROCESS_SERX1794747X1X5> { objInnerWrap } );
                system.debug( 'Updating Penalty Details ==' +response );
                if( String.isNotBlank( response ) ) {
                    /*map<String, Object> objResponse = ( map<String, Object>)JSON.deserializeUntyped( response );
                    if( String.isNotBlank( String.valueOf( objResponse.get('status') ) ) && String.valueOf( objResponse.get('status') ).equalsIgnoreCase('S') ) {
                        objCase.IPMS_Updated__c = true ; 
                        objCase.Status = 'Closed' ;
                        lstCasesToBeUpdated.add( objCase );
                        //Send notification to finance
                    }*/
                    outerWrap objResp = ( outerWrap )JSON.deserialize(response.remove(']').remove('['), Resubmitcontroller.outerWrap.class);
                    if( String.isNotBlank( objResp.status ) &&  objResp.status.equalsIgnoreCase('S') &&
                        objResp.data != null && String.isNotBlank( objResp.data.PROC_STATUS ) && objResp.data.PROC_STATUS.equalsIgnoreCase('S') ) {
                        objCase.IPMS_Updated__c = true ; 
                        objCase.Status = 'Closed' ;
                        lstCasesToBeUpdated.add( objCase );
                    }
                    else {
                        Task objTask = TaskUtility.getTask( ( SObject )objCase, 'Updating waiver details in IPMS failed. Close this task and click on Resubmit button', 'CRE', 
                                                             objCase.RecordType.Name, system.today().addDays(1) );
                        objTask.Notes__c = String.valueOf( 'MQ -- ' + objResp.message + ' \nFROM IPMS -- '+objResp.data.PROC_MESSAGE );
                        lstTaskToBeInserted.add( objTask );
                        
                        PenaltyWaiverControllerV2 objControl = new PenaltyWaiverControllerV2();
                        Error_Log__c objLog = objControl.createErrorLogRecord( objCase.AccountId, objCase.Booking_Unit__c, objCase.Id );
                        objLog.Error_Details__c = 'FROM MQ -- ' +objResp.message + ' \nFROM IPMS -- '+objResp.data.PROC_MESSAGE;
                        lstLogsGenerated.add( objLog );
                    }
                }
            }
            else {
                Task objTask = TaskUtility.getTask( ( SObject )objCase, 'Discrepancy between details in IPMS and Salesforce', 'CRE', 
                objCase.RecordType.Name, system.today().addDays(1));
                lstTaskToBeInserted.add( objTask );
                //objCase.Status = 'Closed';
                lstCasesToBeUpdated.add( objCase );
            }
            }
        }
        if( !lstTaskToBeInserted.isEmpty() ) {
            insert lstTaskToBeInserted ;
        }
        if( !lstCasesToBeUpdated.isEmpty() ) {
            update lstCasesToBeUpdated ;
        }
        if( !lstLogsGenerated.isEmpty() ) {
            insert lstLogsGenerated ;
        }
        
        PageReference pg = new Pagereference('/'+caseobj.id);
        pg.setredirect(true);
        return pg;
    }
    
    public class innerWrap {
        public String PROC_STATUS ;
        public String PROC_MESSAGE ;
        public String PARAM_ID ;
        public String Transaction_Number ;
    }
    
    public class outerWrap {
        public innerWrap data ;
        public String message ;
        public String status ;      
    }
    
}