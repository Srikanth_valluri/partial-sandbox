/*
* Description - Test class developed for 'GenerateDGMSOAController'
*
* Version            Date            Author            Description
* 1.0                26/11/17        Lochan            Initial Draft
*/

@isTest
private class GenerateDGMSOAControllerTest {
    static testMethod void testMethod1() {
    	Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
		Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='12345');
		insert objAcc;
		NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
		dealSR = TestDataFactory_CRM.createServiceRequest();
		insert dealSR;
		Booking__c objBooking = new Booking__c(Account__c=objAcc.Id, Deal_SR__c=dealSR.Id);
		insert objBooking;
		Booking_Unit__c BUObj = new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='Test name',
		Registration_ID__c = '92061', Registration_Status__c = 'Active Status', Unit_Selling_Price_AED__c = 100);
		insert BUObj;

        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );
        String regId = BUObj.Registration_ID__c;
		Test.startTest();
		GenerateDGMSOAController.soaResponse obj = GenerateDGMSOAController.getSOADocument(regId);
        obj.status = '';
        obj.url = '';
		Test.stopTest();
    }
     static testMethod void testMethod2() {
    	Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
		Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='12345');
		insert objAcc;
		NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
		dealSR = TestDataFactory_CRM.createServiceRequest();
		insert dealSR;
		Booking__c objBooking = new Booking__c(Account__c=objAcc.Id, Deal_SR__c=dealSR.Id);
		insert objBooking;
		Booking_Unit__c BUObj = new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='Test name',
		Registration_ID__c = '92061', Registration_Status__c = 'Active Status', Unit_Selling_Price_AED__c = 100);
		insert BUObj;

        //Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );
        String regId = BUObj.Registration_ID__c;
		Test.startTest();
		GenerateDGMSOAController.soaResponse obj = GenerateDGMSOAController.getSOADocument(regId);
		Test.stopTest();
    }
}