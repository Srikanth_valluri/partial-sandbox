@isTest
public class InquiryDuplicateCriteriaBatchTest
{
    @testSetup
    static void setup() {
        List<Inquiry__c> inquiryList = new List<Inquiry__c>();
       // insert 10 Inquiry
        Id agenTeamRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Agent Team').getRecordTypeId();
        for (Integer i=0;i<10;i++) {
            inquiryList.add(new Inquiry__c(RecordTypeId = agenTeamRT,Mobile_Phone_Encrypt__c='456123',Mobile_CountryCode__c='American Samoa: 001684',Mobile_Phone__c='1234'+i,Email__c='mk@gmail.com'+i,First_Name__c='Test'+i,Last_Name__c='Last'+i,CR_Number__c='0987'+i,ORN_Number__c='7842'+i,Agency_Type__c='Corporate',Organisation_Name__c = 'Oliver',isDuplicate__c=false));
        }
        insert inquiryList;
    }

    static testmethod void testCriteria1() {
        Inquiry_Duplicate_Criteria__c inqDupObj=new Inquiry_Duplicate_Criteria__c(Name='CR1',CR_Number__c='No Match',Duplicate__c=true,Email__c='No Match',First_Name__c='No Match',Last_Name__c='No Match',Mobile_Phone__c='No Match',Organisation_Name__c='Match',ORN_Number__c='No Match',Object_Name__c='Inquiry');
        insert inqDupObj;
        Test.startTest();
        Id batchJobId = Database.executeBatch(new InquiryDuplicateCheckBatch(),2000);
        Test.stopTest();
    }

    static testmethod void testCriteria3() {
        Inquiry_Duplicate_Criteria__c inqDupObj=new Inquiry_Duplicate_Criteria__c(Name='CR3',CR_Number__c='Match',Duplicate__c=true,Email__c='No Match',First_Name__c='No Match',Last_Name__c='No Match',Mobile_Phone__c='No Match',Organisation_Name__c='Match',ORN_Number__c='Match',Object_Name__c='Inquiry');
        insert inqDupObj;
        Test.startTest();
        Id batchJobId = Database.executeBatch(new InquiryDuplicateCheckBatch(),2000);
        Test.stopTest();
    }
}