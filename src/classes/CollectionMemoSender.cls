/****************************************************************************************
Description : Controller of VF page 'CollectionMemoSender' that sends mail              *
----------------------------------------------------------------------------------------*
Version     Date        Author              Description                                 *
                                                                                        *
1.0         18/03/2020  Aishwarya Todkar    Initial Drafet                              *
1.1         26/03/2020  Aishwarya Todkar    Added logic to send Overdue email           *
*****************************************************************************************/

public Class CollectionMemoSender {

    Static Id caseId;
    public CollectionMemoSender(ApexPages.StandardController controller) {
        Case objCase = ( Case )controller.getRecord();
        caseId = objCase.Id;
    }

/**************************************************************************************
Description : method to send email via SendGrid
Parameters  : None
Return type : void
***************************************************************************************/
    public static PageReference sendCollectionMemo() {
        List<Case> listCaseToUpdate = new List<Case>();
        //Get Case
        List<Case> listCase = [ SELECT 
                                    Id
                                    , Booking_Unit__c
                                    , Advance_Rebate_Amount__c
                                    , Amount_Customer_has_to_pay__c
                                    , X30th_Day__c
                                    , Booking_Unit__r.Booking__r.Account__r.Person_Business_Email__c
                                    , Booking_Unit__r.Booking__r.Account__r.Name
                                    , Booking_Unit__r.Unit_Name__c
                                    , Discount_Type__c
                                    , Email_Sent__c
                                    , RecordTypeId
                                    , RecordType.Name
                                    , Total_Discount_AED__c
                                    , CRE_Minimum_Payment_Amount__c
                                FROM
                                    Case
                                WHERE
                                    Id =: caseId
                                AND 
                                    RecordTypeId != null
                                AND
                                    Booking_Unit__c != null
                                AND 
                                    Booking_Unit__r.Booking__r.Account__r.Person_Business_Email__c != null                               
                                AND ( 
                                        ( 
                                            RecordType.Name = 'Overdue Rebate/Discount'
                                        AND
                                            Total_Discount_AED__c != null
                                        AND
                                            CRE_Minimum_Payment_Amount__c != null

                                        )
                                    OR (   
                                            RecordType.Name = 'Rebate On Advance' 
                                        AND
                                            Advance_Rebate_Amount__c != null
                                        AND
                                            Overdue_Discount__c != null
                                        AND
                                            Discount_Type__c != null
                                        AND
                                            Discount_Type__c = 'Unconditional'
                                        )
                                    )
                                ];
            
        System.debug( 'listCase -- ' + listCase );
        if( listCase != null && listCase.size() > 0 ) {
            
            String sendGridName = listCase[0].RecordType.Name == 'Rebate On Advance' 
                                ? 'Advance_Payment_Collection_Promotion' 
                                : 'Overdue_Collection_Promotion';
            //Get configured email details
            List<SendGrid_Email_Details__mdt> listSendGridDetails = [SELECT
                                                                        From_Address__c
                                                                        , From_Name__c
                                                                        , To_Address__c
                                                                        , To_Name__c
                                                                        , CC_Address__c
                                                                        , CC_Name__c
                                                                        , Bcc_Address__c
                                                                        , Bcc_Name__c
                                                                        , Reply_To_Address__c
                                                                        , Reply_To_Name__c
                                                                        , Email_Template__c
                                                                    FROM
                                                                        SendGrid_Email_Details__mdt
                                                                    WHERE
                                                                        /*MasterLabel = 'Advance Payment Collection Promotion'
                                                                        OR*/
                                                                        DeveloperName =: sendGridName
                                                                    LIMIT 1
                                                                    ];
            
            if( listSendGridDetails != null &&  listSendGridDetails.size() > 0 ) { 
                SendGrid_Email_Details__mdt objSendGridCs = listSendGridDetails[0];
                System.debug( 'objSendGridCs:::  ' + objSendGridCs);
                //Get Email Template
                List<EmailTemplate> listEmailTemplate = new List<EmailTemplate>( [SELECT 
                                                                                        Id
                                                                                        , Name
                                                                                        , Subject
                                                                                        , Body
                                                                                        , HtmlValue
                                                                                        , TemplateType
                                                                                        , DeveloperName
                                                                                    FROM 
                                                                                        EmailTemplate 
                                                                                    WHERE 
                                                                                        Name =: objSendGridCs.Email_Template__c 
                                                                                    LIMIT 1 
                                                                                ] );
                system.debug('listEmailTemplate  === ' + listEmailTemplate );
        
                //Preparing sendgrid email
                if( listEmailTemplate != null && listEmailTemplate.size() > 0 ) {

                    EmailTemplate emailTemplateObj = listEmailTemplate[0];
                    
                    String templateName =  emailTemplateObj.DeveloperName;

                    String toAddress = listCase[0].Booking_Unit__r.Booking__r.Account__r.Person_Business_Email__c;

                    String toName = String.isNotBlank( objSendGridCs.To_Name__c ) ? objSendGridCs.To_Name__c : '';

                    String ccAddress = String.isNotBlank( objSendGridCs.CC_Address__c ) ? objSendGridCs.CC_Address__c : '';

                    String ccName = String.isNotBlank( objSendGridCs.CC_Name__c ) ? objSendGridCs.CC_Name__c : '';

                    String bccAddress = String.isNotBlank( objSendGridCs.Bcc_Address__c ) ? objSendGridCs.Bcc_Address__c : '';

                    String bccName = String.isNotBlank( objSendGridCs.Bcc_Name__c ) ? objSendGridCs.Bcc_Name__c : '';

                    String fromAddress = String.isNotBlank( objSendGridCs.From_Address__c ) ? objSendGridCs.From_Address__c : '';

                    String fromName = String.isNotBlank( objSendGridCs.From_Name__c ) ? objSendGridCs.From_Name__c : '';

                    String replyToAddress = String.isNotBlank( objSendGridCs.Reply_To_Address__c ) ? objSendGridCs.Reply_To_Address__c : '';

                    String replyToName = String.isNotBlank( objSendGridCs.Reply_To_Name__c ) ? objSendGridCs.Reply_To_Name__c : '';

                    String contentType = 'text/html';
                    
                    String subject = replaceMergeFields( emailTemplateObj.Subject      
                                                            , listCase[0] );

                    String contentValue = replaceMergeFields( emailTemplateObj.HtmlValue      
                                                            , listCase[0] );

                    String contentBody = replaceMergeFields( emailTemplateObj.Body      
                                                            , listCase[0] );
                    
                    System.debug( 'contentValue == ' + contentValue);
                    System.debug( 'contentBody == ' + contentBody);
                    //sending an email via SendGrid
                    SendGridEmailService.SendGridResponse objSendGridResponse = 
                                                    SendGridEmailService.sendEmailService(
                                                                                toAddress, toName
                                                                                , ccAddress, ccName
                                                                                , bccAddress, bccName
                                                                                , subject, ''
                                                                                , fromAddress, fromName
                                                                                , replyToAddress, replyToName
                                                                                , contentType
                                                                                , contentValue, ''
                                                                                ,  new List<Attachment>{} );
                    
                    system.debug('objSendGridResponse === ' + objSendGridResponse);
                    
                    String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;
                    System.debug('responseStatus== ' + responseStatus);
                    List<EmailMessage> lstEmails = new List<EmailMessage>(); 

                    //Create reportable activity 
                    if (responseStatus == 'Accepted') {
                        EmailMessage mail = new EmailMessage();
                        mail.Subject = subject;
                        mail.MessageDate = System.Today();
                        mail.Status = '3';//'Sent';
                        mail.RelatedToId = listCase[0].Booking_Unit__r.Booking__r.Account__c;
                        mail.Account__c  = listCase[0].Booking_Unit__r.Booking__r.Account__c;
                        mail.Type__c = 'Promotion Offer';
                        mail.Sub_Type__c = 'Rebate On Advance Payment';
                        mail.Department__c = 'Collection';
                        mail.ToAddress = toAddress;
                        mail.FromAddress = fromAddress;
                        mail.TextBody = contentBody;//contentValue.replaceAll('\\<.*?\\>', '');
                        mail.Sent_By_Sendgrid__c = true;
                        mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                        mail.ParentId = listCase[0].id;
                        mail.CcAddress = ccAddress;
                        mail.BccAddress = bccAddress;
                        lstEmails.add(mail);
                        system.debug('Mail obj == ' + mail);
                        
                        update new Case( Id = caseId, Email_Sent__c = true );
                        insert lstEmails;    
                    }
                    System.debug('lstEmails=== ' + lstEmails);

                } // End listEmailTemplate if
            }//End listSendGridDetails if
        }//End listCase if 
        return new PageReference( '/' + caseId);
    }//Method end

/**************************************************************************************
Description : method to replace merge fields
Parameters  : mail contents, Case
Return type : Replaced Contents
***************************************************************************************/
    public static String replaceMergeFields( String strContent, Case objCase ) {
        if( String.isNotBlank( strContent ) ) {
            if( strContent.contains( '{!Account.Name}' ) ) {
                if( String.isNotBlank ( objCase.Booking_Unit__r.Booking__r.Account__r.Name ) ) {
                    strContent = strContent.replace( '{!Account.Name}', objCase.Booking_Unit__r.Booking__r.Account__r.Name );
                }
                else {
                    strContent = strContent.replace( '{!Account.Name}', '__');
                }
            }

            if( strContent.contains( '{!Case.Advance_Rebate_Amount__c}' ) ) {
                if( objCase.Advance_Rebate_Amount__c != null ) {
                    strContent = strContent.replace( '{!Case.Advance_Rebate_Amount__c}', String.valueOf( objCase.Advance_Rebate_Amount__c ) );
                }
                else {
                    strContent = strContent.replace( '{!Case.Advance_Rebate_Amount__c}', '__');
                }
            }

            if( strContent.contains( '{!Case.Amount_Customer_has_to_pay__c}' ) ) {
                if( objCase.Amount_Customer_has_to_pay__c != null ) {
                    strContent = strContent.replace( '{!Case.Amount_Customer_has_to_pay__c}'
                                                    , String.valueOf( objCase.Amount_Customer_has_to_pay__c ) );
                }
                else {
                    strContent = strContent.replace( '{!Case.Amount_Customer_has_to_pay__c}', '__');
                }
            }

            if( strContent.contains( '{!Case.X30th_Day__c}' ) ) {
                if( objCase.X30th_Day__c != null ) {
                    strContent = strContent.replace( '{!Case.X30th_Day__c}', String.valueOf( objCase.X30th_Day__c ) );
                }
                else {
                    strContent = strContent.replace( '{!Case.X30th_Day__c}', '__');
                }
            }

            if( strContent.contains( '{!Booking_Unit__c.Unit_Name__c}' ) ) {
                if( String.isNotBlank ( objCase.Booking_Unit__r.Unit_Name__c ) ) {
                    strContent = strContent.replace( '{!Booking_Unit__c.Unit_Name__c}', objCase.Booking_Unit__r.Unit_Name__c );
                }
                else {
                    strContent = strContent.replace( '{!Booking_Unit__c.Unit_Name__c}', '__');
                }
            }

            if( strContent.contains( '{!Case.Total_Discount_AED__c}' ) ) {
                if( objCase.Total_Discount_AED__c != null ) {
                    strContent = strContent.replace( '{!Case.Total_Discount_AED__c}', String.valueOf( objCase.Total_Discount_AED__c ) );
                }
                else {
                    strContent = strContent.replace( '{!Case.Total_Discount_AED__c}', '__');
                }
            }

            if( strContent.contains( '{!Case.CRE_Minimum_Payment_Amount__c}' ) ) {
                if( objCase.CRE_Minimum_Payment_Amount__c != null ) {
                    strContent = strContent.replace( '{!Case.CRE_Minimum_Payment_Amount__c}', String.valueOf( objCase.CRE_Minimum_Payment_Amount__c ) );
                }
                else {
                    strContent = strContent.replace( '{!Case.CRE_Minimum_Payment_Amount__c}', '__');
                }
            }
        }
        return strContent;
    }
}