@isTest
private class AramexServiceTest {
    @isTest
    static void testCreateShipping(){
        TriggerOnOffCustomSetting__c triggerSetting = new TriggerOnOffCustomSetting__c(Name= 'CaseTrigger',
                                                                                    OnOffCheck__c = false);
        insert triggerSetting;
        
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'Aramex Shipping';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        settings.Endpoint__c = 'https://ws.dev.aramex.net/ShippingAPI.V2/Shipping/Service_1_0.svc/json/CreateShipments';
        insert settings;
        
        Account objAccount = new Account();
        objAccount.Name = 'test';
        insert objAccount;
        
        Case objCase = new Case();
        objCase.Status = 'Submitted';
        insert objCase;
        
        Courier_Delivery__c objAramex = new Courier_Delivery__c();
        objAramex.Courier_Service__c = 'Aramex'; 
        objAramex.Case__c = objCase.Id;
        objAramex.Account__c = objAccount.Id;
        insert objAramex;
        
        Test.setMock(HttpCalloutMock.class, new CourierServiceMock());
        Test.startTest();
            List<Courier_Delivery__c> lstCourier = AramexService.createShipping(objAramex.Courier_Service__c,new List<Courier_Delivery__c>{objAramex});
        Test.stopTest();
    }
    
    @isTest
    static void testGetAramexTracking(){
        TriggerOnOffCustomSetting__c triggerSetting = new TriggerOnOffCustomSetting__c(Name= 'CaseTrigger',
                                                                                    OnOffCheck__c = false);
        insert triggerSetting;
        
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
        
        Credentials_Details__c creds = new Credentials_Details__c(); 
        creds.Name = 'Aramax Tracking';
        creds.Endpoint__c = 'https://ws.dev.aramex.net/ShippingAPI.V2/Tracking/Service_1_0.svc/json/TrackShipments' ;
        creds.User_Name__c = 'testingapi@aramex.com';
        creds.Password__c = 'R123456789$r';
        insert creds;
        
        Account objAccount = new Account();
        objAccount.Name = 'test';
        objAccount.Country__c = 'India';
        insert objAccount;
        
        Case objCase = new Case();
        objCase.Status = 'Submitted';
        insert objCase;
        
        Courier_Delivery__c objAramex = new Courier_Delivery__c();
        objAramex.Courier_Service__c = 'Aramex'; 
        objAramex.Case__c = objCase.Id;
        objAramex.Account__c = objAccount.Id;
        insert objAramex;
        
        Test.setMock(HttpCalloutMock.class, new CourierServiceMock());
        Test.startTest();
            AramaxTrackingResponseJSON2Apex objResponse = AramexService.getAramexTracking('44097879825', 'AE', '116216', '45796');
        Test.stopTest();
        System.assertNotEquals(null, objResponse);
    }
    
    @isTest
    static void testGetAirWayBillBody(){
        Test.setMock(HttpCalloutMock.class, new CourierServiceMock());
        Test.startTest();
            Blob resp = AramexService.getAirWayBillBody('http://www.sandbox.aramex.com/content/rpt_cache/6c93658d80e1472a82755e38022ce20f.pdf');
        Test.stopTest(); 
        System.assertNotEquals(null, resp);
    }
}