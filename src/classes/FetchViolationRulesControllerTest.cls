@isTest
private class FetchViolationRulesControllerTest {

	@isTest static void test_functionality() {
		Location__c objLoc = new Location__c();
		objLoc = TestDataFactoryFM.createLocation();
		insert objLoc;
		List<Violation_Notice__c> lstVRs = new List<Violation_Notice__c>();
		lstVRs.add(new Violation_Notice__c(
			Name = 'Rule 1',
			Active__c = true,
			Category__c	='General Violations',
			Payble_Fine__c = 100,
			Remedial_Period__c = '7 days',
			Violation_Of_Rule__c = 'Abusing personnel working in the community'
		));
		insert lstVRs;
		Test.startTest();
		ApexPages.StandardController sc = new ApexPages.StandardController(objLoc);
	   	FetchViolationRulesController controller = new FetchViolationRulesController(sc);
		PageReference pageRef = Page.FetchViolationRulesPage;
        pageRef.getParameters().put('id', String.valueOf(objLoc.Id));
        Test.setCurrentPage(pageRef);
		controller.fetchViolationRules();
		Test.stopTest();
	}

}