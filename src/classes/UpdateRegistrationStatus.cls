public without sharing class UpdateRegistrationStatus{
    public static string updateRegistrationStatus(Case objC, String regStatus){
        System.debug('*****objC*****'+objC);
        System.debug('*****regStatus*****'+regStatus);
        String response;
        AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint objClass = new AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint();
        objClass.timeout_x = 120000;
        String strRegStatusResponse = objClass.UpdateRegistrationStatus('2-'+String.valueOf(System.currentTimeMillis()), 
                                                                        'UPDATE_REG_STATUS', 
                                                                        'SFDC', 
                                                                        objC.Registration_ID__c, 
                                                                        regStatus);
        system.debug('UpdateRegistrationStatus strRegStatusResponse==='+strRegStatusResponse);
        if(String.isNotBlank(strRegStatusResponse)){
            map<String, Object> mapDeserializeRegStatus = new map<String, Object>();
            mapDeserializeRegStatus = (map<String, Object>)JSON.deserializeUntyped(strRegStatusResponse);
            if(mapDeserializeRegStatus.get('Status') != 'S'){
                response = 'Error : Error from IPMS for Status Update '+regStatus;
            }else if(mapDeserializeRegStatus.get('Status') == 'S'){
                response = 'Success';
            }
        }
        return response;
    } // end of method
} // end of class