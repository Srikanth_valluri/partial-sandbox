@istest
public class CRMNexmoRestServiceTest {
    static testmethod void test_rest_class_wa_status(){
        CRM_Nexmo_Whats_App_Request__c req = new CRM_Nexmo_Whats_App_Request__c();
        req.Unique_Key__c = '1234567890';
        req.To_number__c = 1234567890;
        req.Last_MTM_Sent_at__c = system.now();
        insert req;
        
        CRM_Nexmo_Whats_App_Message__c msg = new CRM_Nexmo_Whats_App_Message__c();
        msg.CRM_Nexmo_Whats_App_Request__c = req.id;
        msg.Nexmo_Message_uuid__c = '537832e2-4b39-41c6-ae26-4e1af71bfd90';
        insert msg;
            test.starttest();
            // executed
            string reqstr ='{"message_uuid":"537832e2-4b39-41c6-ae26-4e1af71bfd90","to":{"number":"1234567890","type":"whatsapp"},"from":{"number":"447418342136","type":"whatsapp"},"timestamp":"2018-11-01T16:27:51.865Z","status":"read"}';
            CRMNexmoRestService.NEXMO_RESPONSE_PARSER parsecls = CRMNexmoRestService.parse(reqstr);
            string jsonstr = JSON.serialize(parsecls);
            RestRequest requ = new RestRequest(); 
            RestResponse res = new RestResponse();
            requ.requestURI = '/crmNexmohook/status';  
            requ.httpMethod = 'POST';
            requ.requestbody = blob.valueof(jsonstr);
            RestContext.request = requ;
            CRMNexmoRestService.doPost();
        test.stoptest();    
    }
    
     static testmethod void test_rest_class_wa_submitted_status(){
        CRM_Nexmo_Whats_App_Request__c req = new CRM_Nexmo_Whats_App_Request__c();
        req.Unique_Key__c = '1234567890';
        req.To_number__c = 1234567890;
        req.Last_MTM_Sent_at__c = system.now();
        insert req;
        
        CRM_Nexmo_Whats_App_Message__c msg = new CRM_Nexmo_Whats_App_Message__c();
        msg.CRM_Nexmo_Whats_App_Request__c = req.id;
        msg.Nexmo_Message_uuid__c = '537832e2-4b39-41c6-ae26-4e1af71bfd90';
        insert msg;
       
        
        test.starttest();
            // executed
            string reqstr ='{"message_uuid":"537832e2-4b39-41c6-ae26-4e1af71bfd90","to":{"number":"1234567890","type":"whatsapp"},"from":{"number":"447418342136","type":"whatsapp"},"timestamp":"2018-11-01T16:27:51.865Z","status":"submitted"}';
            CRMNexmoRestService.NEXMO_RESPONSE_PARSER parsecls = CRMNexmoRestService.parse(reqstr);
            string jsonstr = JSON.serialize(parsecls);
            RestRequest requ = new RestRequest(); 
            RestResponse res = new RestResponse();
            requ.requestURI = '/crmNexmohook/status';  
            requ.httpMethod = 'POST';
            requ.requestbody = blob.valueof(jsonstr);
            RestContext.request = requ;
            CRMNexmoRestService.doPost();
        test.stoptest();
    
    }
    
    static testmethod void test_rest_class_wa_delivered_status(){
        CRM_Nexmo_Whats_App_Request__c req = new CRM_Nexmo_Whats_App_Request__c();
        req.Unique_Key__c = '1234567890';
        req.To_number__c = 1234567890;
        req.Last_MTM_Sent_at__c = system.now();
        insert req;
        
        CRM_Nexmo_Whats_App_Message__c msg = new CRM_Nexmo_Whats_App_Message__c();
        msg.CRM_Nexmo_Whats_App_Request__c = req.id;
        msg.Nexmo_Message_uuid__c = '537832e2-4b39-41c6-ae26-4e1af71bfd90';
        insert msg;
        
        test.starttest();
            // executed
            string reqstr ='{"message_uuid":"537832e2-4b39-41c6-ae26-4e1af71bfd90","to":{"number":"1234567890","type":"whatsapp"},"from":{"number":"447418342136","type":"whatsapp"},"timestamp":"2018-11-01T16:27:51.865Z","status":"delivered"}';
            CRMNexmoRestService.NEXMO_RESPONSE_PARSER parsecls = CRMNexmoRestService.parse(reqstr);
            string jsonstr = JSON.serialize(parsecls);
            RestRequest requ = new RestRequest(); 
            RestResponse res = new RestResponse();
            requ.requestURI = '/crmNexmohook/status';  
            requ.httpMethod = 'POST';
            requ.requestbody = blob.valueof(jsonstr);
            RestContext.request = requ;
            CRMNexmoRestService.doPost();
        test.stoptest();
    
    }
    
    
        
    static testmethod void test_rest_class_wa_no_status(){
        CRM_Nexmo_Whats_App_Request__c req = new CRM_Nexmo_Whats_App_Request__c();
        req.Unique_Key__c = '1234567890';
        req.To_number__c = 1234567890;
        req.Last_MTM_Sent_at__c = system.now();
        insert req;
        
        CRM_Nexmo_Whats_App_Message__c msg = new CRM_Nexmo_Whats_App_Message__c();
        msg.CRM_Nexmo_Whats_App_Request__c = req.id;
        msg.Nexmo_Message_uuid__c = '537832e2-4b39-41c6-ae26-4e1af71bfd90';
        insert msg;
        
        test.starttest();
            // executed
            string reqstr ='{"message_uuid":"537832e2-4b39-41c6-ae26-4e1af71bfd90","to":{"number":"1234567890","type":"whatsapp"},"from":{"number":"447418342136","type":"whatsapp"},"timestamp":"2018-11-01T16:27:51.865Z","status":""}';
            CRMNexmoRestService.NEXMO_RESPONSE_PARSER parsecls = CRMNexmoRestService.parse(reqstr);
            string jsonstr = JSON.serialize(parsecls);
            RestRequest requ = new RestRequest(); 
            RestResponse res = new RestResponse();
            requ.requestURI = '/crmNexmohook/status';  
            requ.httpMethod = 'POST';
            requ.requestbody = blob.valueof(jsonstr);
            RestContext.request = requ;
            CRMNexmoRestService.doPost();
        test.stoptest();
    
    }
    
    
    static testmethod void test_rest_class_wa_inbound(){    
        
        CRM_Nexmo_Whats_App_Request__c req = new CRM_Nexmo_Whats_App_Request__c();
        req.Unique_Key__c = '1234567890';
        req.To_number__c = 1234567890;
        req.Last_MTM_Sent_at__c = system.now();
        insert req;
        
        CRM_Nexmo_Whats_App_Message__c msg = new CRM_Nexmo_Whats_App_Message__c();
        msg.CRM_Nexmo_Whats_App_Request__c = req.id;        
        insert msg;
        
        test.starttest();
            // executed
            string reqstr ='{"to":{"type_Z":"whatsapp","number_Z":"447418342136"},"timestamp":"2018-11-01T16:28:06.044Z","message_uuid":"4539de9f-3d4c-46e0-9a19-e4719f34c7ea","message":{"content":{"type_Z":"text","text":null,"inboundMessage":"Who"}},"from_Z":"1234567890","direction":"inbound"}';
            CRMNexmoRestService.NEXMO_INB_RESPONSE_PARSER parsecls = CRMNexmoRestService.parse1(reqstr);
            string jsonstr = JSON.serialize(parsecls);
            system.debug('&&&&'+jsonstr);
            RestRequest requ = new RestRequest(); 
            RestResponse res = new RestResponse();
            requ.requestURI = '/crmNexmohook/inbound';  
            requ.httpMethod = 'POST';
            requ.requestbody = blob.valueof(jsonstr);        
            RestContext.request = requ;
            CRMNexmoRestService.doPost();
        test.stoptest();
    }
    
    static testmethod void test_rest_class_wa_inbound_NewCustomer(){
        test.starttest();
            // executed
            string reqstr ='{"to":{"type_Z":"whatsapp","number_Z":"447418342136"},"timestamp":"2018-11-01T16:28:06.044Z","message_uuid":"4539de9f-3d4c-46e0-9a19-e4719f34c7ea","message":{"content":{"type_Z":"text","text":null,"inboundMessage":"Who"}},"from_Z":"1234567890","direction":"inbound"}';
            CRMNexmoRestService.NEXMO_INB_RESPONSE_PARSER parsecls = CRMNexmoRestService.parse1(reqstr);
            string jsonstr = JSON.serialize(parsecls);
            system.debug('&&&&'+jsonstr);
            RestRequest requ = new RestRequest(); 
            RestResponse res = new RestResponse();
            requ.requestURI = '/crmNexmohook/inbound';  
            requ.httpMethod = 'POST';
            requ.requestbody = blob.valueof(jsonstr);
            system.debug('@@@test'+requ.requestbody);
            RestContext.request = requ;
            CRMNexmoRestService.doPost();
        test.stoptest();
    }
    
     static testmethod void test_rest_class_image_NewCustomer(){
        test.starttest();
            // executed
            CRM_Nexmo_Whats_App_Request__c req = new CRM_Nexmo_Whats_App_Request__c();
            req.Unique_Key__c = '1234567890';
            req.To_number__c = 1234567890;
            req.Last_MTM_Sent_at__c = system.now();
            insert req;
            
            CRM_Nexmo_Whats_App_Message__c msg = new CRM_Nexmo_Whats_App_Message__c();
            msg.CRM_Nexmo_Whats_App_Request__c = req.id;
            msg.Nexmo_Message_uuid__c = '537832e2-4b39-41c6-ae26-4e1af71bfd90';
            insert msg;
            string reqstr ='{"to":{"type_Z":"whatsapp","number_Z":"447418342136"},"timestamp":"2018-11-01T16:28:06.044Z","message_uuid":"4539de9f-3d4c-46e0-9a19-e4719f34c7ea","message":{"content":{"type_Z":"image","image":{"url":"https://sf.deeprootsurface.com/docs/t/null-1540217939692.jpg","caption":"Test"},"text":null,"inboundMessage":"Who"}},"from_Z":"1234567890","direction":"inbound"}';
            CRMNexmoRestService.NEXMO_INB_RESPONSE_PARSER parsecls = CRMNexmoRestService.parse1(reqstr);
            string jsonstr = JSON.serialize(parsecls);
            system.debug('&&&&'+jsonstr);
            RestRequest requ = new RestRequest(); 
            RestResponse res = new RestResponse();
            requ.requestURI = '/crmNexmohook/inbound';  
            requ.httpMethod = 'POST';
            requ.requestbody = blob.valueof(jsonstr);
            system.debug('@@@test'+requ.requestbody);
            RestContext.request = requ;
            CRMNexmoRestService.doPost();
            CRMNexmoRestService.doget();
        test.stoptest();
    }
    
    }