@isTest
global with sharing class MaintenanceRequestMock implements WebServiceMock{
     global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType){
               System.debug('=request==='+request);
               if(request instanceof wwwFsiCoUkServicesEvolution0409.Authenticate_element) {
                   wwwFsiCoUkServicesEvolution0409.SecurityService1  calloutObj = new  wwwFsiCoUkServicesEvolution0409.SecurityService1();
                   schemasDatacontractOrg200407FsiPlat.AuthenticationDto authObj = new schemasDatacontractOrg200407FsiPlat.AuthenticationDto();
                   authObj.OperationResult = 'Succeeded';
                   authObj.SessionId = '02p2lvq220wzp555vxny2m55';
                   wwwFsiCoUkServicesEvolution0409.AuthenticateResponse_element  calloutObj1 = new  wwwFsiCoUkServicesEvolution0409.AuthenticateResponse_element();
                   calloutObj1.AuthenticateResult = authObj;
                   response.put('response_x', calloutObj1);
               }
               else if(request instanceof wwwFsiCoUkServicesEvolution0409_t32.GetTaskById_element) {
                     wwwFsiCoUkServicesEvolution0409_t32.TaskServiceV3  calloutObj = new   wwwFsiCoUkServicesEvolution0409_t32.TaskServiceV3();
                       schemasDatacontractOrg200407FsiConc_t32.TaskDtoV3 authObj = new schemasDatacontractOrg200407FsiConc_t32.TaskDtoV3();
                       authObj.LongDescription = 'Test';
                       authObj.Code = 'Test';
                       authObj.ShortDescription = 'A/C is very cold';
                       authObj.Priority = 'P2 - URGENT';
                       authObj.State = 'Active';
                       wwwFsiCoUkServicesEvolution0409_t32.GetTaskByIdResponse_element  calloutObj2 = new  wwwFsiCoUkServicesEvolution0409_t32.GetTaskByIdResponse_element();
                       calloutObj2.GetTaskByIdResult = authObj;
                       response.put('response_x', calloutObj2);
               }
               else if(request instanceof MultipleDocUploadService.DocumentAttachmentMultiple_element) {
                   MultipleDocUploadService.DocumentAttachmentMultipleResponse_element responseNew =
                    new MultipleDocUploadService.DocumentAttachmentMultipleResponse_element();
                    MultipleDocUploadService.DocumentAttachmentMultiple_element req = (MultipleDocUploadService.DocumentAttachmentMultiple_element) request;
                    responseNew.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS]Files : [FMN-00483-1534330389135-violation notice.html]","PARAM_ID":"IPMS-3246-Violation Notice","URL":"https://sftest.deeprootsurface.com/docs/t/FMN-00483-1534330389135-violation notice.html"}],"message":"Service Process Completed","status":"S"}';
                    response.put('response_x', responseNew);
               }

          }
}