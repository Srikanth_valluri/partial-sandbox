/**********************************************************************************************************************
Description: This API is used for submission of appointment booking.
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   25-11-2020      | Subin Antony		  | Created initial draft
-----------------------------------------------------------------------------------------------------------------------
1.1     |   10-12-2020      | Subin Antony		  | Modified appointment slot booking logic to fail request with reference to booked appointment slots
***********************************************************************************************************************/

@RestResource(urlMapping='/appointment/submit')
global class DL_SubmitAppointmentBooking_API {
    public static Map<Integer, String> statusCodeMap;
    static {
        statusCodeMap = new Map<Integer, String>{
            1 => 'Success',
            2 => 'Failure',
            3 => 'Bad Request',
            6 => 'Something went wrong',
            7 => 'Info'
        };
    }
	
    @HttpPost
    global static FinalReturnWrapper submitAppointmentBooking(String process_name, String sub_process_name, String bu_id, 
    String appointment_date_string, String account_id, String selected_appointment_slot, String remarks) {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        RestContext.response.addHeader('Content-Type', 'application/json');
        String errorMsg, exceptionMsg, successMsg;
        
        if(String.isBlank(process_name) || String.isBlank(sub_process_name)) {
            errorMsg = 'Please select process and sub_process.';
            return getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
        }
        
        if(String.isBlank(bu_id)){
            errorMsg = 'Please select a booking unit for booking appointment.';
            return getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
        }
        
        Date appointmentDate = NULL;
        String formattedDateString = NULL;
        try{
            if(String.isNotBlank(appointment_date_string)) {
                appointmentDate = Date.valueOf(appointment_date_string);
                List<String> parts = appointment_date_string.split('-');
                formattedDateString = parts[1] + '/' + parts[2] + '/' + parts[0];
            } else {
                errorMsg = 'Please provide a proper appointment date string formatted as \'yyyy-mm-dd\'.';
                return getErrorResponse(3, statusCodeMap.get(3), errorMsg, NULL);
            }
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
            errorMsg = 'Please provide a proper appointment date string formatted as \'yyyy-mm-dd\'.';
            return getErrorResponse(3, statusCodeMap.get(3), errorMsg, ex.getMessage());
        }
        
        if(String.isBlank(account_id)){
            errorMsg = 'Please provide account_id.';
            return getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
        }
        
        Account accountRecord;
        try{
            accountRecord = [SELECT Id, Name, isPersonAccount, Email__pc, Email__c, 
                             Primary_CRE__c, Secondary_CRE__c, Tertiary_CRE__c, Primary_Language__c 
                             FROM Account WHERE Id = :account_id];
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
        	exceptionMsg = ex.getMessage();
            accountRecord = NULL;
        }
        if(NULL == accountRecord){
            errorMsg = 'The account_id provided does not match any records.';
            return getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
        }
        
        Booking_unit__c bookingUnit;
        try{
            bookingUnit = [Select Id, Name, Property_Name__c, Unit_Name__c, Registration_ID__c, Booking__c, Booking__r.Account__c 
                           FROM Booking_Unit__c WHERE Booking__r.Account__c = :account_id AND Id = :bu_id];
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
        	exceptionMsg = ex.getMessage();
            bookingUnit = NULL;
        }
        if(NULL == bookingUnit) {
            errorMsg = 'The booking unit ID provided does not match any valid records.';
            return getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
        }
        
        if(String.isBlank(selected_appointment_slot)){
            errorMsg = 'Please provide selected_appointment_slot.';
            return getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
        }
        
        System.debug('....................SUBMIT_APPOINTMENT: acc_id > ' + accountRecord.Id);
        System.debug('....................SUBMIT_APPOINTMENT: bu_id > ' + bu_id);
        System.debug('....................SUBMIT_APPOINTMENT: process > ' + process_name);
        System.debug('....................SUBMIT_APPOINTMENT: sub_process > ' + sub_process_name);
        System.debug('....................SUBMIT_APPOINTMENT: req_date > ' + appointment_date_string);
        
        AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
        List<AppointmentSelectionHandler.AppointmentWrapper> availableAppointments;
        try{
            availableAppointments = handler.availableSlots(accountRecord.Id, bu_id, process_name, sub_process_name, 
                                                           'Mobile', appointment_date_string, false); /* CRE / Portal / Mobile */
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
        	exceptionMsg = ex.getMessage();
            availableAppointments = NULL;
        }
        if(NULL == availableAppointments || availableAppointments.size() < 1) {
            errorMsg = 'Available appointment slots exhausted for given date. Please try another date.';
            return getErrorResponse(6, statusCodeMap.get(6), errorMsg, exceptionMsg);
        }
        
        System.debug('....................SUBMIT_APPOINTMENT: available_slot_count > ' + availableAppointments.size());
        List<AppointmentSelectionHandler.AppointmentWrapper> submissionSlots = new List<AppointmentSelectionHandler.AppointmentWrapper>();
        for(AppointmentSelectionHandler.AppointmentWrapper availableAppointment : availableAppointments) {
            Appointment__c objAppointment = availableAppointment.objApp;
            Calling_List__c objCallingList = availableAppointment.objCL;
            
            if(NULL == objAppointment && String.isNotBlank(availableAppointment.ErrorMessage)) {
                errorMsg = availableAppointment.ErrorMessage;
                if(availableAppointment.ErrorMessage.containsIgnoreCase('only after 24 hours from the present time')) {
                    Datetime dt = DateTime.newInstance(appointmentDate, Time.newInstance(0, 0, 0, 0));
                    String dayOfWeek = dt.format('EEEE');
                    if(dayOfWeek == 'Wednesday' || dayOfWeek == 'Thursday' || dayOfWeek == 'Friday') 
                        errorMsg += ' Slots are not available on Friday and Saturday for booking appointments.';
                }
                return getErrorResponse(2, statusCodeMap.get(2), errorMsg, null);
            }
            
            if(NULL != objAppointment && selected_appointment_slot.equalsIgnoreCase(objAppointment.id)) {
                if(NULL != objCallingList && NULL != objCallingList.id) { /* check whether booking already made for this slot */
                    errorMsg = 'Mentioned appointment slot already got booked for another interview. Please try another date/slot.';
                    return getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
                }
                availableAppointment.isSelected = true;
                submissionSlots.add(availableAppointment);
        		System.debug('....................SUBMIT_APPOINTMENT: submittedAppointment_slot > ' + availableAppointment);
                break;
            }
        }
        
        if(submissionSlots.size() < 1) {
        	System.debug('....................SUBMIT_APPOINTMENT: INVALID SLOT SELECTED : !!!ERROR!!!');
            errorMsg = 'Selected appointment slot is invalid or got exhausted. Please try another slot.';
            return getErrorResponse(6, statusCodeMap.get(6), errorMsg, exceptionMsg);
        }
        System.debug('....................SUBMIT_APPOINTMENT: submission_slot_count > ' + submissionSlots.size());
        
        try{
            handler.createAppointments(submissionSlots, remarks, bu_id, accountRecord, sub_process_name, process_name, formattedDateString);
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
        	exceptionMsg = ex.getMessage();
            errorMsg = 'Error while booking appointment. Could not complete processs.';
            // return getErrorResponse(2, statusCodeMap.get(2), errorMsg, exceptionMsg);
        }
        
        // query the created appointment(calling list)
        Id appointSchedRecTypID = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Appointment Scheduling').getRecordTypeId();
        List<Calling_List__c> bookedAppointments;
        try{
            bookedAppointments = [SELECT id, name, Appointment_Status__c, Service_Type__c, Sub_Purpose__c, Remarks__c, 
                                  Account__c, Assigned_CRE__c, Assigned_CRE__r.name, Appointment_Date__c, 
                                  Appointment_Slot__c, Appointment_Start_DateTime__c, Appointment_End_DateTime__c, 
                                  Appointment__c, Appointment__r.name, Appointment__r.Slots__c, 
                                  Booking_Unit__c, Booking_Unit__r.Unit_Name__c, Booking_Unit__r.Registration_ID__c 
                                  FROM Calling_List__c WHERE recordTypeID = :appointSchedRecTypID 
                                  AND Account__c = :account_id 
                                  AND Appointment__c = :selected_appointment_slot ORDER BY CreatedDate DESC];
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
        	exceptionMsg = ex.getMessage();
            bookedAppointments = NULL;
        }
        if(NULL == bookedAppointments || bookedAppointments.size() < 1) {
            errorMsg = 'Appointment booking was submitted. Could not retrive the corresponding calling list record.';
            return getErrorResponse(2, statusCodeMap.get(2), errorMsg, exceptionMsg);
        }
        else {
            Calling_List__c bookedAppointment = bookedAppointments[0];
        	System.debug('....................SUBMIT_APPOINTMENT: persisted_CL > ' + bookedAppointment);
            
            // send success response
            FinalReturnWrapper responseWrapper = new FinalReturnWrapper();
            cls_meta_data responseMetaData = new cls_meta_data();
            responseMetaData.status_code = 1;
            responseMetaData.title = statusCodeMap.get(1);
            responseMetaData.message = 'Successfully submitted the appointment booking.';
            responseWrapper.meta_data = responseMetaData;
            
            cls_data appointmentDetails = new cls_data();
            appointmentDetails.appointment_id = bookedAppointment.id;
            appointmentDetails.appointment_name = bookedAppointment.Appointment__r.name;
            appointmentDetails.appointment_status = (NULL!= bookedAppointment.Appointment_Status__c && 
                                                     bookedAppointment.Appointment_Status__c.equalsIgnoreCase('Confirmed')) ? 
                'Booked' : bookedAppointment.Appointment_Status__c;
            appointmentDetails.assigned_cre_name = bookedAppointment.Assigned_CRE__r.name;
            appointmentDetails.purpose = bookedAppointment.Service_Type__c;
            appointmentDetails.sub_purpose = bookedAppointment.Sub_Purpose__c;
            DateTime bookedDate = bookedAppointment.Appointment_Start_DateTime__c;
            appointmentDetails.appointment_date = (NULL != bookedDate) ? bookedDate.format('yyyy-MM-dd') : appointment_date_string;
            appointmentDetails.appointment_slot = formatSlotString(bookedAppointment.Appointment__r.Slots__c);
            appointmentDetails.booking_unit_name = bookedAppointment.Booking_Unit__r.Unit_Name__c;
            appointmentDetails.booking_unit_id = bookedAppointment.Booking_Unit__c;
            responseWrapper.data = appointmentDetails;
        	System.debug('....................SUBMIT_APPOINTMENT: persisted_appiontment_detail > ' + appointmentDetails);
            
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
            return responseWrapper;
        }
    }
    
    private static FinalReturnWrapper getErrorResponse(Integer statusCode, String title, 
    String responseMessage, String devMessage) {
        FinalReturnWrapper responseWrapper = new FinalReturnWrapper();
        cls_meta_data responseMetaData = new cls_meta_data();
        responseMetaData.status_code = statusCode;
        responseMetaData.title = title;
        responseMetaData.message = responseMessage;
        responseMetaData.developer_message = devMessage;
        responseWrapper.meta_data = responseMetaData;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
        return responseWrapper;
    }
    
    public static String formatSlotString(String slotString_24H) {
        String formattedString = slotString_24H;
        try {
            String[] times = slotString_24H.split('-');
            String startTimeString = times[0].trim();
            String endTimeString = times[1].trim();
            
            String[] startTimeParts = startTimeString.split(':');
            Integer startHour = Integer.valueOf(startTimeParts[0].trim());
            Integer startMinute = Integer.valueOf(startTimeParts[1].trim());
            String startTimeFormatted = (startHour > 12 ? (startHour - 12) : startHour) + ':' + 
                (startMinute < 10 ? ('0'+startMinute) : (''+startMinute))  + (startHour >= 12 ? 'PM' : 'AM');
            
            String[] endTimeParts = endTimeString.split(':');
            Integer endHour = Integer.valueOf(endTimeParts[0].trim());
            Integer endMinute = Integer.valueOf(endTimeParts[1].trim());
            String endTimeFormatted = (endHour > 12 ? (endHour - 12) : endHour) + ':' + 
                (endMinute < 10 ? ('0'+endMinute) : (''+endMinute)) + (endHour >= 12 ? 'PM' : 'AM');
            
            formattedString = startTimeFormatted + ' - ' + endTimeFormatted;
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
        }
        return formattedString;
    }
    
    global class FinalReturnWrapper {
        public cls_meta_data meta_data;
        public cls_data data;
    }
    
    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message; 
    }
    
    public class cls_data {
        public String appointment_id;
        public String appointment_name;
        public String appointment_status;
        public String assigned_cre_name;
        public String purpose;
        public String sub_purpose;
        public String appointment_date;
        public String appointment_slot;
        public String booking_unit_name;
        public String booking_unit_id;
    }
}