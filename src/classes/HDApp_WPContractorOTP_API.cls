/**********************************************************************************************************************
Description: API to send and verify OTP for contractor flow
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By      | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   14-01-2021      | Anand Venkitakrishnan | Created the initial draft
***********************************************************************************************************************/

@RestResource(urlMapping='/contractorOTP/*')
global class HDApp_WPContractorOTP_API {
    @HttpGet
    global static FinalReturnWrapper sendContractorOTP() {
        
        RestRequest r = RestContext.request;
        System.debug('Request Headers:'+r.headers);
        System.debug('Request params:'+r.params);
        
        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        HDApp_Utility.cls_meta_data objMeta = new HDApp_Utility.cls_meta_data();
        
        if(!r.requestURI.equalsIgnoreCase('/contractorOTP/sendOTP')) {
            returnResponse.meta_data = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,'Please use correct API endpoint',3);
            return returnResponse;
        }
        else if(!r.params.containsKey('fmCaseNumber') || (r.params.containsKey('fmCaseNumber') && String.isBlank(r.params.get('fmCaseNumber')))) {
            returnResponse.meta_data = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,'No fmCaseNumber found in request',3);
            return returnResponse;
        }
        
        Integer statusCode;
        String errorMsg = 'Error in generating OTP';
        String responseMessage = 'Please enter the verification code sent on your registered email id';
        String guidCreated;
        String fmCaseId;
        
        try {
            String accountId = r.params.get('accountId');
            String fmCaseNumber = r.params.get('fmCaseNumber');
            String searchString = '%' + fmCaseNumber;
            
            List<FM_Case__c> listFMCase = [SELECT Id,Account__c,Person_To_Collect__c,Contact_person__c,Contact_person_contractor__c,Email__c,Email_2__c,Booking_Unit__r.Booking__r.Account__c 
                                           FROM FM_Case__c 
                                           WHERE Name LIKE :searchString
                                           AND RecordType.DeveloperName = 'Work_Permit'];
            
            if(listFMCase != NULL && !listFMCase.isEmpty()) {
                FM_Case__c objFMCase = listFMCase[0];
                System.debug('objFMCase:'+objFMCase);
                
                String emailCode = String.valueOf((Math.random() * 10000).intValue()).leftPad(4, '0');
                System.debug('emailCode:'+emailCode);
                
                String recipientName = '';
                String emailAddress;
                
                if(objFMCase.Person_To_Collect__c.equals('Consultant')) {
                    if(objFMCase.Contact_person__c != NULL) {
                        recipientName = objFMCase.Contact_person__c;
                    }
                    emailAddress = objFMCase.Email__c;
                }
                else {
                    if(objFMCase.Contact_person_contractor__c != NULL) {
                        recipientName = objFMCase.Contact_person_contractor__c;
                    }
                    emailAddress = objFMCase.Email_2__c;
                }
                System.debug('recipientName:'+recipientName);
                System.debug('emailAddress:'+emailAddress);
                
                if(emailAddress != NULL && String.isNotBlank(emailAddress)) {
                    List<OrgWideEmailAddress> orgWideAddressHelloDamac = [SELECT Id,Address,DisplayName 
                                                                          FROM OrgWideEmailAddress 
                                                                          WHERE Id = :Label.CommunityPortalOrgWideAddressId];
                	
                	String emailSubject = 'Work Permit Request Update Verification Code';
                    
                    String emailBody = 'Hello ' + recipientName + ',';
                    emailBody += '\n\n' + emailCode + ' is the code for verifying your email id.';
                    emailBody += '\n\nPlease enter this code to update the Work Permit Request in Damac Living App.';
                    emailBody += '\n\nRegards,\nLOAMS';
                    
                    SendGridEmailService.SendGridResponse response = SendGridEmailService.sendEmailService(
                        emailAddress, recipientName, '', '', '', '', emailSubject, '',
                        orgWideAddressHelloDamac[0].Address, orgWideAddressHelloDamac[0].DisplayName, '', '', 'text/plain',
                        emailBody, '', new List<Attachment>()
                    );
                    
                    fmCaseId = objFMCase.Id;
                    
                    String sendGridRes = Test.isRunningTest() ? 'Accepted' : response.ResponseStatus;
                    
                    if(sendGridRes == 'Accepted') {
                        EmailMessage mail = new EmailMessage();
                        mail.Subject = emailSubject;
                        mail.MessageDate = System.Today();
                        mail.Status = '3';
                        mail.RelatedToId = fmCaseId;
                        mail.ToAddress = emailAddress;
                        mail.FromAddress = orgWideAddressHelloDamac[0].Address;
                        mail.TextBody = emailBody;
                        mail.CcAddress = '';
                        mail.BccAddress = '';
                        mail.Sent_By_Sendgrid__c = true;
                        mail.SentGrid_MessageId__c = response.messageId;
                        mail.Booking_Unit__c = objFMCase.Booking_Unit__c;
                        mail.Account__c = objFMCase.Account__c;
                        
                        insert mail;
                        
                        guidCreated = insertOTPwithGUID(fmCaseId,emailCode);
                        System.debug('guidCreated:'+guidCreated);
                        
                        responseMessage += ' ' + emailAddress + '. OTP: ' + emailCode;
                        
                        statusCode = 1;
                    }
                    else {
                        Error_Log__c objError = new Error_Log__c();
                        objError.Account__c = objFMCase.Booking_Unit__r.Booking__r.Account__c;
                        objError.FM_Case__c = objFMCase.Id;
                        objError.Error_Details__c = 'WP Contractor/Consultant OTP verification request failed from SendGrid';
                        objError.Process_Name__c = 'Generic Email';
                        
                        insert objError;
                        
                        errorMsg = 'Error in sending email';
                        
                        statusCode = 6;
                    }
                }
                else {
                    statusCode = 6;
                    
                    errorMsg = 'No email address for contractor in the SR';
                }
            }
            else {
                objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,'No FM Case found for given fmCaseNumber',6);
                
                returnResponse.meta_data = objMeta;
                
                return returnResponse;
            }
        }
        catch(Exception e) {
            System.debug('Exception:'+e.getMessage());
            errorMsg = e.getMessage();
            
            statusCode = 6;
        }
        
        if(statusCode == 1) {
            objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.successMsg,responseMessage, 1);
        }
        else {
            objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,errorMsg,6);
        }
        
        cls_data objData = new cls_data();
        objData.fm_case_id = fmCaseId;
        objData.guid = guidCreated;
        
        returnResponse.data = objData;
        returnResponse.meta_data = objMeta;
        
        System.debug('returnResponse:'+returnResponse);
        
        return returnResponse;
    }
    
    public static String insertOTPwithGUID(String fmCaseId,String emailOtp) {
        //Logic to expire existing OTPs on the FM Case
        List<OTP__c> existingOTPs = [SELECT Id,isExpired__c 
                                     FROM OTP__c 
                                     WHERE FM_Case__c =: fmCaseId 
                                     AND FM_Process_Name__c =: HDApp_Constants.wpFMProcessName];
        System.debug('existingOTPs:'+existingOTPs);
        
        for(OTP__c objOTP : existingOTPs) {
            objOTP.isExpired__c = true;
        }
        
        System.debug('existingOTPs after expiring:'+existingOTPs);
        
        if(existingOTPs.size() > 0) {
            update existingOTPs;
        }
        
        String guid = getGUID();
		
        OTP__c objOTP = new OTP__c();
        objOTP.GUID__c = guid;
        objOTP.FM_Email_OTP__c = emailOtp;
        objOTP.FM_Process_Name__c = HDApp_Constants.wpFMProcessName;
        objOTP.FM_Case__c = fmCaseId;
		
        System.debug('objOTP:'+objOTP);
        insert objOTP;
		
        return guid;
    }
	
    public static String getGUID() {
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
        System.debug('guid generated:'+guid);
		
        return guid;
    }
    
    @HttpPost
    global static FinalSRReturnWrapper verifyContractorOTP() {
        
        RestRequest r = RestContext.request;
        System.debug('Request Headers:'+r.headers);
        System.debug('Request params:'+r.params);
        
        FinalSRReturnWrapper returnResponse = new FinalSRReturnWrapper();
        HDApp_Utility.cls_meta_data objMeta = new HDApp_Utility.cls_meta_data();
        
        if(!r.requestURI.equalsIgnoreCase('/contractorOTP/verifyOTP')) {
            returnResponse.meta_data = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,'Please use correct API endpoint',3);
            return returnResponse;
        }
        else if(!r.params.containsKey('fmCaseId') || (r.params.containsKey('fmCaseId') && String.isBlank(r.params.get('fmCaseId')))) {
            returnResponse.meta_data = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,'No fmCaseId found in request',3);
            return returnResponse;
        }
        else if(!r.params.containsKey('otpValue') || (r.params.containsKey('otpValue') && String.isBlank(r.params.get('otpValue')))) {
            returnResponse.meta_data = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,'No otpValue found in request',3);
            return returnResponse;
        }
        else if(!r.params.containsKey('guid') || (r.params.containsKey('guid') && String.isBlank(r.params.get('guid')))) {
            returnResponse.meta_data = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,'No guid found in request',3);
            return returnResponse;
        }
        
        Integer statusCode;
        String errorMsg = 'Error in fetching the SR details';
        
        List<String> listContractorType = new List<String>();
        List<String> listNumberOfEmployees = new List<String>();
        List<DocumentWrapper> listDocumentWrapper = new List<DocumentWrapper>();
        FMCaseWrapper objFMCaseWrapper;
        
        try {
            String fmCaseId = r.params.get('fmCaseId');
            String otpValue = r.params.get('otpValue');
            String guid = r.params.get('guid');
            
            if(verifyOTPs(fmCaseId,otpValue,guid)) {
                List<FM_Case__c> listFMCase = [SELECT Id,Account__c,Booking_Unit__c,Company_Name__c,Contractor_Type__c,Number_of_Employees__c,Person_To_Collect__c,Work_Permit_Type__c, 
                                               Contact_person__c,Email__c,Mobile_Country_Code__c,Mobile_no__c,Office_tel__c, 
                                               Contact_person_contractor__c,Email_2__c,Mobile_Country_Code_3__c,Mobile_no_contractor__c,Office_tel_contractor__c 
                                               FROM FM_Case__c 
                                               WHERE Id =: fmCaseId];
            	
                if(listFMCase != NULL && !listFMCase.isEmpty()) {
                    FM_Case__c objFMCase = listFMCase[0];
                    System.debug('objFMCase:'+objFMCase);
                    
                    listContractorType = HDApp_Utility.getPicklistOptions('FM_Case__c','Contractor_Type__c');
            		listNumberOfEmployees = HDApp_Utility.getPicklistOptions('FM_Case__c','Number_of_Employees__c');
                    
                    Map<String,List<FM_Documents__mdt>> mapDocumentsMetaData = new Map<String,List<FM_Documents__mdt>>();
                    
                    for(FM_Documents__mdt objDocMeta : FM_Utility.getDocumentsList('Apply_for_Work_Permits','')) {
                        if(mapDocumentsMetaData.containsKey(objDocMeta.NOC_Type__c)) {
                            mapDocumentsMetaData.get(objDocMeta.NOC_Type__c).add(objDocMeta);
                        }
                        else {
                            mapDocumentsMetaData.put(objDocMeta.NOC_Type__c,new List<FM_Documents__mdt>{objDocMeta});
                        }
                    }
                    
                    Map<String,List<DocumentWrapper>> mapDocumentWrapper = new Map<String,List<DocumentWrapper>>();
                    String strWorkPermitType = objFMCase.Work_Permit_Type__c;
                    
                    for(String key : mapDocumentsMetaData.keyset()) {
                        if(key.contains(strWorkPermitType)) {
                            if(mapDocumentWrapper.containsKey(strWorkPermitType)) {
                                mapDocumentWrapper.get(strWorkPermitType).addAll(getDocumentWrapperList(mapDocumentsMetaData.get(key),mapDocumentWrapper.get(strWorkPermitType).size()+1));
                            }
                            else {
                                mapDocumentWrapper.put(strWorkPermitType,getDocumentWrapperList(mapDocumentsMetaData.get(key),1));
                            }
                        }
                    }
                    
                    if(mapDocumentWrapper.containsKey(strWorkPermitType)) {
                        listDocumentWrapper = mapDocumentWrapper.get(strWorkPermitType);
                    }
                    
                    objFMCaseWrapper = wrapFMCase(objFMCase);
                    
                    statusCode = 1;
                }
                else {
                    objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,'No FM Case found for given fmCaseId',6);
                    
                    returnResponse.meta_data = objMeta;
                    
                    return returnResponse;
                }
            }
            else {
                errorMsg = 'OTP verification failed';
                
                statusCode = 6;
            }
        }
        catch(Exception e) {
            System.debug('Exception:'+e.getMessage());
            errorMsg = e.getMessage();
            
            statusCode = 6;
        }
        
        if(statusCode == 1 && objFMCaseWrapper != null) {
            objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.successMsg,'', 1);
        }
        else {
            objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,errorMsg,6);
        }
        
        cls_sr_data objData = new cls_sr_data();
        objData.contractor_type = listContractorType;
        objData.number_of_employees = listNumberOfEmployees;
        objData.documents_required = listDocumentWrapper;
        objData.fm_case = objFMCaseWrapper;
        
        returnResponse.data = objData;
        returnResponse.meta_data = objMeta;
        
        System.debug('returnResponse:'+returnResponse);
        
        return returnResponse;
    }
    
    public Static boolean verifyOTPs(String fmCaseId,String otpCode,String guid) {
        Boolean isVerified = false;
        
        List<OTP__c> listOTPs = [SELECT Id,FM_Case__c,FM_Email_OTP__c,FM_Process_Name__c,GUID__c,isExpired__c,CreatedDate 
                                 FROM OTP__c 
                                 WHERE GUID__c =: guid 
                                 AND FM_Case__c =: fmCaseId 
                                 AND FM_Process_Name__c =: HDApp_Constants.wpFMProcessName 
                                 AND isExpired__c = false 
                                 ORDER BY CreatedDate DESC 
                                 LIMIT 1];
        
        System.debug('listOTPs:'+listOTPs);
        
        if(listOTPs != NULL && listOTPs.size() > 0) {
            OTP__c otpRecord = listOTPs[0];
            Boolean isValid = false;
            
            /*15 minutes check starts*/
            Datetime time_15 = otpRecord.CreatedDate.addMinutes(15);
            System.debug('time_15:'+time_15);
            System.debug('now:'+Datetime.now());
            isValid = Datetime.now() <= time_15 ? true : false;
            System.debug('isValid:'+isValid);
            /*15 minutes check end*/
            
            if(isValid) {
                System.debug('inside isValid');
                isValid = otpCode == otpRecord.FM_Email_OTP__c ? true : false;
                System.debug('isValid:'+isValid);
                
                if(isValid) {
                    otpRecord.isExpired__c = true;
                    update otpRecord;
                }
            }
            
            isVerified = isValid;
        }
     	
        return isVerified;
    }
    
    public static FmCaseWrapper wrapFMCase(FM_Case__c objFMCase) {
        FmCaseWrapper objFmCaseWrapper = new FmCaseWrapper();
        
        objFmCaseWrapper.id = objFMCase.id;
        objFmCaseWrapper.account_id = objFMCase.Account__c;
        objFmCaseWrapper.booking_unit_id = objFMCase.Booking_Unit__c;
        
        objFmCaseWrapper.contact_person_company_name = objFMCase.Company_Name__c;
        objFmCaseWrapper.contact_person_type = objFMCase.Contractor_Type__c;
        
        if(objFMCase.Person_To_Collect__c.equals('Consultant')) {
            objFmCaseWrapper.contact_person_name = objFMCase.Contact_person__c;
            objFmCaseWrapper.contact_person_email = objFMCase.Email__c;
            objFmCaseWrapper.contact_person_mobile_country_code = objFMCase.Mobile_Country_Code__c;
            objFmCaseWrapper.contact_person_mobile_number = objFMCase.Mobile_no__c;
            objFmCaseWrapper.contact_person_office_telephone = objFMCase.Office_tel__c;
        }
        else {
            objFmCaseWrapper.contact_person_name = objFMCase.Contact_person_contractor__c;
            objFmCaseWrapper.contact_person_email = objFMCase.Email_2__c;
            objFmCaseWrapper.contact_person_mobile_country_code = objFMCase.Mobile_Country_Code_3__c;
            objFmCaseWrapper.contact_person_mobile_number = objFMCase.Mobile_no_contractor__c;
            objFmCaseWrapper.contact_person_office_telephone = objFMCase.Office_tel_contractor__c;
        }
        
        System.debug('objFmCaseWrapper:'+objFmCaseWrapper);
        
        return objFmCaseWrapper;        
    }
    
    public static List<DocumentWrapper> getDocumentWrapperList(List<FM_Documents__mdt> listDocMeta,Integer documentId) {
        List<DocumentWrapper> listDocumentWrapper = new List<DocumentWrapper>();
       	
        for(FM_Documents__mdt objDocMeta : listDocMeta) {
            DocumentWrapper objDocumentWrapper = new DocumentWrapper();
            objDocumentWrapper.document_name = objDocMeta.Document_Name__c;
            objDocumentWrapper.document_id = documentId++;
            objDocumentWrapper.is_mandatory = objDocMeta.Mandatory__c;
            listDocumentWrapper.add(objDocumentWrapper);
        }
        
        return listDocumentWrapper;
    }
    
    /***********************************/
    // For Send OTP API
    /***********************************/
    global class FinalReturnWrapper {
        public cls_data data;
        public HDApp_Utility.cls_meta_data meta_data;
    }
    
    public class cls_data {
        public String fm_case_id;
        public String guid;
    }
    
    /***********************************/
    // For Verify OTP API
    /***********************************/
    global class FinalSRReturnWrapper {
        public cls_sr_data data;
        public HDApp_Utility.cls_meta_data meta_data;
    }
    
    public class cls_sr_data {
        public List<String> contractor_type;
        public List<String> number_of_employees;
        public List<DocumentWrapper> documents_required;
        public FMCaseWrapper fm_case;
    }
    
    public class DocumentWrapper {
        public String document_name;
        public Integer document_id;
        public Boolean is_mandatory;
    }
    
    public class FMCaseWrapper {
        public String id;
        public String account_id;
        public String booking_unit_id;
        
        public String contact_person_company_name;
        public String contact_person_type;
        
        public String contact_person_name;
        public String contact_person_email;
        public String contact_person_mobile_country_code;
        public String contact_person_mobile_number;
        public String contact_person_office_telephone;
    }
}