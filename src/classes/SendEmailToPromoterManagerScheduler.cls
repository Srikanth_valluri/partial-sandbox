/*************************************************************************************************************************
* Name               : SendEmailToPromoterManagerScheduler                                                               *
* Description        : Scheduler to send reports to Promoter managers emails every 3 hours.           ..................     *
* Created Date       : 24/07/2018                                                                                        *
* Created By         : MOHIT                                                                                             *
* --------------------------------------------------------------------------------------------------------------------   */
public class SendEmailToPromoterManagerScheduler implements Schedulable {
    public void execute(SchedulableContext SC) { 

            database.executeBatch(new SendEmailToPromoterManagerBatch(System.now()));
        
    }
}