global class GenerateAirwayBillBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.Stateful{
    List<Id> courierIds = new List<Id>();
    global GenerateAirwayBillBatch(List<Id> courierDeliveryIds){
        this.courierIds = courierDeliveryIds;
    }
    global List<Courier_Delivery__c> start(Database.BatchableContext BC){
        List<Courier_Delivery__c> courierDeliveryLst = [Select Id
                                                       , Airway_Bill__c
                                                       , Airway_PDF__c 
                                                       , Courier_Service__c
                                                       , Country__c
                                                    From Courier_Delivery__c 
                                                   Where id =: courierIds];
        
        return courierDeliveryLst;
    }
    
    global void execute(Database.BatchableContext BC, List<Courier_Delivery__c> scope){
        for(Courier_Delivery__c courierDeliveryObj : scope) {
          GenerateAirwayBillController objcontroller = new GenerateAirwayBillController(new ApexPages.StandardController(courierDeliveryObj));
          objcontroller.uploadAirwayBill();  
        }
            
    }
    
    global void finish(Database.BatchableContext BC){
    
    }

}