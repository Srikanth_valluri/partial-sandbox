public class DrawloopQueuebleClass implements Queueable, Database.AllowsCallouts{
    public String SERVER_URL;
    public String SESSION_ID;
    private list<Booking_Unit__c> lstUpdatedBU;
    
    public DrawloopQueuebleClass (list<Booking_Unit__c> lstBookingUnit) {
        lstUpdatedBU = new list<Booking_Unit__c>();
        lstUpdatedBU = lstBookingUnit;
    }
    
    public void execute(QueueableContext context) {
        login();
        
        system.debug('SESSION_ID*********'+SESSION_ID);
        
        if(SESSION_ID != null && SESSION_ID != ''){
            system.debug('SESSION ID received*********');
            Loop.loopMessage lm = new Loop.loopMessage();
            lm.sessionId = SESSION_ID;
            system.debug('== List Size ==' +lstUpdatedBU.size() );
            for( Booking_Unit__c objBU : lstUpdatedBU ){
                system.debug('== In for objBU : '+objBU);
                Map<string, string> variables = new map<String,String>(); // MAIN RECORD ID – SAME OBJECT AS THE DDP RECORD TYPE SPECIFIES’ // DDP ID
                variables.put('deploy', Label.Notice_BR_TemplateId );
                lm.requests.add(new Loop.loopMessage.loopMessageRequest(
                                objBU.Id, 
                                Label.Notice_BR_DDPId,
                                variables)); 
            } // end of for loop
            String response = lm.sendAllRequests();
            system.debug('== response ==' +response );          
            
        }
        list<Booking_Unit__c> updateBUlist = new list<Booking_Unit__c>();
        for( Booking_Unit__c objBU : lstUpdatedBU ){
            objBU.Service_Notice_Sent_Date__c = system.today();
            updateBUlist.add(objBU);
        }
        if (updateBUlist != null && !updateBUlist.isEmpty()){
            update updateBUlist;
        }
    }
    
    //Method used for login purpose to get the session id
    public void login(){
        Admin_Login_for_Drawloop__c mc = Admin_Login_for_Drawloop__c.getOrgDefaults();
        Http h = new Http(); 
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://' + mc.Domain__c + '.salesforce.com/services/Soap/u/28.0');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        request.setHeader('SOAPAction', '""');
        request.setBody('<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Header/><Body><login xmlns="urn:partner.soap.sforce.com"><username>' + mc.Username__c + '</username><password>' + mc.Password__c + '</password></login></Body></Envelope>');
        
        HttpResponse response = h.send(request);
        system.debug('----Response Body-----'+response.getBody());
        
        Dom.XmlNode resultElmt = response.getBodyDocument().getRootElement()
        .getChildElement('Body','http://schemas.xmlsoap.org/soap/envelope/')
        .getChildElement('loginResponse','urn:partner.soap.sforce.com')
        .getChildElement('result','urn:partner.soap.sforce.com');
        
        SERVER_URL = resultElmt.getChildElement('serverUrl','urn:partner.soap.sforce.com').getText().split('/services')[0];
        SESSION_ID = resultElmt.getChildElement('sessionId','urn:partner.soap.sforce.com').getText();
        
        system.debug('--SERVER_URL---'+SERVER_URL);
        system.debug('--SESSION_ID---'+SESSION_ID);
        if(Test.isRunningTest()){
            SESSION_ID = 'none';
        }
    }
    
}