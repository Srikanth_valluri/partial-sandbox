/***********************************************************************************************
 * @Name              : InlineBookingUnitExtension
 * @Test Class Name   : InlineBookingUnitExtensionTest
 * @Description       : Controller class for Account Send Email VF page.
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log
 * 1.0         ADM CRM         30/11/2017      Created
 * 2.0         Neha Dave       19/08/2020      Added checks to restrict certain users from sending
 *                                             emails to customer.
***********************************************************************************************/
public with sharing class InlineBookingUnitExtension {
    public String accId                                        {get;set;}
    public List<Account> accountList                           {get;set;}
    public List<Booking__c> bookingList                        {get;set;}
    public List<Booking_Unit__c> bookingUnitList               {get;set;}
    //public List<BUInfo> BUInfoList                             {get;set;}
    public String bookingUnitId                                {get;set;}
    public String buId                                         {get;set;}
    public Boolean isRedirected                                {get;set;}
    public list<SR_Attachments__c> lstSRAttachment;
    public String url;
    public blob urlHPBody;
    public Booking_Unit__c currentBookingUnit;
    public Set<Id> bookingIdSet;
    public boolean blnFMUser {get;set;}
    public boolean sendEmail  {get;set;}

    public InlineBookingUnitExtension(ApexPages.StandardController controller) {
        init();
    }

    public void init(){
        isRedirected = false;
        url = null;
        urlHPBody = null;
        accountList = new List<Account>();
        bookingList = new List<Booking__c>();
        bookingUnitList = new List<Booking_Unit__c>();
        //BUInfoList = new List<BUInfo>();
        lstSRAttachment = new list<SR_Attachments__c>();
        currentBookingUnit = new Booking_Unit__c();
        bookingIdSet = new Set<Id>();
        blnFMUser = false;

        accId = ApexPages.currentPage().getParameters().get('id');
        accId = String.valueOf(accId).substring(0, 15); 
        
        list<String> lstProfiles = new list<String>();
        lstProfiles.addAll(Label.FM_Profile_Names.split(','));
        
         list<User> lstUser = [Select Id
                                   , ProfileId
                                   , Profile.Name 
                              From User 
                              Where Id =:UserInfo.getUserId()
                              and Profile.Name IN : lstProfiles];
        // If current user is not an FM user redirect to Account detail page
        if(lstUser == null || lstUser.isEmpty()){
            blnFMUser = false;
        }else{
            blnFMUser = true;
        }

        for (Booking__c objBooking : [Select Id, 
                                             Account__c
                                        From Booking__c 
                                       Where Account__c =: accId]) {
             bookingIdSet.add(objBooking.Id);                     
        }

        bookingUnitList = [ SELECT Id,
                                   Name,
                                   Booking__c,
                                   Account_Id__c,
                                   Registration_ID__c,
                                   Property_Name__c,
                                   Unit_Name__c,
                                   Selling_Price__c,
                                   Requested_Price__c,
                                   Area__c,
                                   Registration_Status__c,
                                   DP_OK__c,
                                   Doc_OK__c,
                                   Mortgage__c,
                                   Anticipated_Completion_Date__c
                              FROM Booking_Unit__c
                             Where Booking__c IN :bookingIdSet 
                             Limit 950
                             ]; 

        if(bookingUnitList.isEmpty()) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'No records to display.'));
        }
    }

    public void GenerateSOA(){
        bookingUnitId = ApexPages.CurrentPage().getParameters().get('bookingUnitId');
        system.debug('===bookingUnitId= in GenerateSOA====' + bookingUnitId );
        system.debug('===accId= in GenerateSOA====' + accId );
        system.debug('===buId in GenerateSOA====' + buId );

        lstSRAttachment = new list<SR_Attachments__c>();
        currentBookingUnit = new Booking_Unit__c();

        Account objAccount = [ Select Id,
                                      Name,
                                      Email__c
                                 From Account
                                Where Id = :accId
                            ];

        currentBookingUnit =  getBookingUnit(buId);

        GenerateSOAController.soaResponse objSOAResponse = new GenerateSOAController.soaResponse();
        objSOAResponse = GenerateSOAController.getSOADocument(currentBookingUnit.Registration_ID__c);
        System.Debug('=====GenerateSOAController.soaResponse objSOAResponse : ' + objSOAResponse);

        if (objSOAResponse.url != null) {
            url = objSOAResponse.url;
            if (Test.IsRunningTest()) {
                urlHPBody=Blob.valueOf('UNIT.TEST');
            }
            else {
                urlHPBody = getBlob(url);
            }

            SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
            objCaseAttachment.Name = 'Unit SOA ' + currentBookingUnit.Name;
            objCaseAttachment.Attachment_URL__c = url;
            objCaseAttachment.Booking_Unit__c = currentBookingUnit.Id;
            lstSRAttachment.add(objCaseAttachment);
        }
        System.debug('========lstSRAttachment : '+lstSRAttachment );

        try{
            if (lstSRAttachment != null && lstSRAttachment.size()>0){
                insert lstSRAttachment;
                System.debug('======Success==lstSRAttachment : '+lstSRAttachment[0].Id );
                String soaType = 'Unit SOA.pdf';
                SendEmail(urlHPBody, objAccount.Email__c, objAccount.Name, soaType);
            }
        } catch (Exception e) {
            System.debug('========error : '+e );
        }
    }
    public void PenaltySOA(){
        bookingUnitId = ApexPages.CurrentPage().getParameters().get('bookingUnitId');
        system.debug('===bookingUnitId= in PenaltySOA====' + bookingUnitId );
        system.debug('===accId= in PenaltySOA====' + accId );
        system.debug('===buId in PenaltySOA====' + buId );

        lstSRAttachment = new list<SR_Attachments__c>();
        currentBookingUnit = new Booking_Unit__c();

        Account objAccount = [ Select Id,
                                      Name,
                                      Email__c
                                 From Account
                                Where Id = :accId
                            ];
        currentBookingUnit =  getBookingUnit(buId);
        PenaltyWaiverService.SopResponseObject objSOPResponse = new PenaltyWaiverService.SopResponseObject();
        objSOPResponse = PenaltyWaiverService.getSOPDocument(currentBookingUnit.Registration_ID__c);
        System.Debug('=====PenaltyWaiverService.SopResponseObject objSOPResponse : ' + objSOPResponse);

        if (objSOPResponse.url != null) {
            url = objSOPResponse.url;
            if (Test.IsRunningTest()) {
              urlHPBody=Blob.valueOf('UNIT.TEST');
            }
            else {
              urlHPBody = getBlob(url);
            }

            SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
            objCaseAttachment.Name = 'Penalty SOA ' + currentBookingUnit.Name;
            objCaseAttachment.Attachment_URL__c = url;
            objCaseAttachment.Booking_Unit__c = currentBookingUnit.Id;
            lstSRAttachment.add(objCaseAttachment);
        }
        System.debug('========lstSRAttachment: '+lstSRAttachment);

        try{
            if (lstSRAttachment!= null && lstSRAttachment.size()>0){
                insert lstSRAttachment;
                System.debug('======Success==lstSRAttachment: '+lstSRAttachment[0].Id );
                String soaType = 'Penalty SOA.pdf';
                SendEmail(urlHPBody, objAccount.Email__c, objAccount.Name, soaType);
            }
        } catch (Exception e) {
            System.debug('========error : '+e );
        }
    }

    public void DGMSOA(){
        bookingUnitId = ApexPages.CurrentPage().getParameters().get('bookingUnitId');
        system.debug('===DGM_SOA= in DGM_SOA====' + bookingUnitId );
        system.debug('===accId= in DGM_SOA====' + accId );
        system.debug('===buId in DGMSOA====' + buId );
        lstSRAttachment = new list<SR_Attachments__c>();
        currentBookingUnit = new Booking_Unit__c();

        Account objAccount = [ Select Id,
                                      Name,
                                      Email__c
                                 From Account
                                Where Id = :accId
                            ];
        currentBookingUnit =  getBookingUnit(buId);

        GenerateDGMSOAController.soaResponse objSOPResponse = new GenerateDGMSOAController.soaResponse();
        objSOPResponse = GenerateDGMSOAController.getSOADocument(currentBookingUnit.Registration_ID__c);
        System.Debug('=====GenerateDGMSOAController.soaResponse objSOPResponse : ' + objSOPResponse);

        if (objSOPResponse.url != null) {
            url = objSOPResponse.url;
            if (Test.IsRunningTest()) {
              urlHPBody=Blob.valueOf('UNIT.TEST');
            }
            else {
              urlHPBody = getBlob(url);
            }
            SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
            objCaseAttachment.Name = 'FM SOA ' + currentBookingUnit.Name;
            objCaseAttachment.Attachment_URL__c = url;
            objCaseAttachment.Booking_Unit__c = currentBookingUnit.Id;
            lstSRAttachment.add(objCaseAttachment);
        }
        System.debug('========lstSRAttachment : '+lstSRAttachment );

        try{
            if (lstSRAttachment != null && lstSRAttachment.size()>0){
                insert lstSRAttachment;
                System.debug('======Success==lstSRAttachment : '+lstSRAttachment[0].Id );
                String soaType = 'FM SOA.pdf';
                SendEmail(urlHPBody, objAccount.Email__c, objAccount.Name, soaType);
            }
        } catch (Exception e) {
            System.debug('========error : '+e );
        }
    }

    public static Blob getBlob(String url){
        PageReference pageRef = new PageReference(url);
        Blob ret = pageRef.getContentAsPDF();
        return ret;
    }

    public Booking_Unit__c getBookingUnit(String bookingUnitId){
        Booking_Unit__c currentBookingUnit = [ SELECT Id,
                                                      Name,
                                                      Account_Id__c,
                                                      Registration_ID__c,
                                                      Property_Name__c,
                                                      Unit_Name__c,
                                                      Requested_Price__c,
                                                      Selling_Price__c,
                                                      Booking__c,
                                                      Booking__r.Id,
                                                      Booking__r.Name,
                                                      Booking__r.Account__c,
                                                      Booking__r.Account__r.Email__c,
                                                      Booking__r.Account__r.Name
                                                 FROM Booking_Unit__c
                                               WHERE Id = :bookingUnitId
                                              ];
        return currentBookingUnit;
    }

    public void SendEmail(blob file, String email, String accName, String soaType) {
        //New instance of a single email message
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

            // The email template ID used for the email
            //mail.setTemplateId('00X30000001GLJj');
            OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'no-replysf@damacgroup.com'];

            List<String> sendTo = new List<String>();
            if(email !='' || email != null) {
            sendTo.add(email);
            }
            if ( owea.size() > 0 ) {
            mail.setOrgWideEmailAddressId(owea.get(0).Id);
            }

            mail.setSubject('Statement of Account Generated.');
            mail.setToAddresses(sendTo);
            mail.setBccSender(false);
            mail.setUseSignature(false);
            //mail.setReplyTo('test123@acme.com');
            mail.setSenderDisplayName('DAMAC');
            mail.setSaveAsActivity(false);

            String body = 'Dear ' + accName;
            body += '\n Please find attached Statement of Account. ';
            body += '\n \n Thanks & Regards,';
            body += '\n SalesForce Team';
            mail.setHtmlBody(body);

            List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
            Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
            efa.setFileName(soaType);
            efa.setBody(file);
            fileAttachments.add(efa);
            mail.setFileAttachments(fileAttachments);

            try {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            System.debug('===========Mail sent====');
            } catch(Exception e) {
            System.debug('======Error in email sending=========' + e);
            }
    }

    public pagereference redirect() {
        accId = ApexPages.currentPage().getParameters().get('id');
        String personEmail;
        pagereference newpg;
        isRedirected = true;
        String CurrentUserId = UserInfo.getUserId();

        //Added code to set the sendEmail to true/false on the basis of user profiles
        //and DELC on Account field value.
        sendEmail = true;
        Set<Id> profileIdSet = new Set<Id>();
		Set<String> allowedProfileList = new Set<String>();

		for(Allowed_Profiles_for_Call_Email__mdt obj
			:[SELECT Profile_Name__c
				FROM Allowed_Profiles_for_Call_Email__mdt
			 ]
		){
			allowedProfileList.add(obj.Profile_Name__c);
		}

		for (Profile objProf : [Select Id, Name From Profile
								Where Name IN :allowedProfileList
								OR Name LIKE '%FM%'
								OR Name LIKE '%Property%']) {
			ProfileIdSet.add(objProf.Id);
        }

        Account objAccount = [SELECT Id
                                   , Email__pc
                                   , Primary_CRE__c
                                   , Secondary_CRE__c
                                   , Tertiary_CRE__c
                                   , FAM_CAUTION__c
                                   , Name
                                   , bypass_FAM__c
                                   , Units_under_DELC__c
                               FROM Account
                              WHERE Id =: accId
                              LIMIT 1];

        if(ProfileIdSet != null &&
           !ProfileIdSet.contains(UserInfo.getProfileId()) &&
           objAccount.Units_under_DELC__c
        ) {
            sendEmail = false;
        }

        if (objAccount.Email__pc == null) {
            newpg = new PageReference (Label.Email_send_URL+'/_ui/core/email/author/EmailAuthor?p2_lkid='+accId+'&rtype=003&p3_lkid='+accId+'&p5='+''+'&retURL=' + accId);

        } else if (objAccount.Email__pc != null) {
           newpg = new PageReference (Label.Email_send_URL+'/_ui/core/email/author/EmailAuthor?p2_lkid='+accId+'&rtype=003&p3_lkid='+accId+'&p5='+''+'&retURL=' + accId);
        }

        if(String.isNotBlank(objAccount.FAM_CAUTION__c) && Label.FAMFieldValue.containsIgnoreCase(objAccount.FAM_CAUTION__c) && objAccount.bypass_FAM__c == false){
                if(objAccount.Primary_CRE__c == NULL && objAccount.Secondary_CRE__c == NULL && objAccount.Tertiary_CRE__c == NULL){
                    newpg = new PageReference(Label.CSRBaseURL+'/apex/NotAuthorize');
                }else if( !((objAccount.Primary_CRE__c != NULL && CurrentUserId.contains(objAccount.Primary_CRE__c)) ||
                    (objAccount.Secondary_CRE__c != NULL && CurrentUserId.contains(objAccount.Secondary_CRE__c)) ||
                    (objAccount.Tertiary_CRE__c != NULL && CurrentUserId.contains(objAccount.Tertiary_CRE__c))) ){
                    newpg = new PageReference( Label.CSRBaseURL+'/apex/NotAuthorize');
                }
        }

        if(!sendEmail){
            newpg = new PageReference( Label.CSRBaseURL+'/apex/NotAuthorize');
        }

        newpg.setRedirect(false);
        return newpg;
    }
}