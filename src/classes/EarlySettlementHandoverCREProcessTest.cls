@isTest
private class EarlySettlementHandoverCREProcessTest {
    
    static Booking_Unit__c BUObj;
    static Account objAcc;
    
    static void testData() {
        objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        objAcc.Country__c = 'United Arab Emirates';
        insert objAcc ;
        
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        Booking__c objBooking = new Booking__c(Account__c=objAcc.Id, Deal_SR__c=dealSR.Id);
        insert objBooking;

        BUObj = new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='AB/019/TH500', Bedroom_Type__c = '1BR',
        Registration_ID__c = '74712', Registration_Status__c = 'Active Status', Unit_Selling_Price_AED__c = 100,
        Unit_Selling_Price__c = 3456899, Anticipated_Completion_Date__c = system.Today() );
        insert BUObj;
        
        list<Buyer__c> lstBuyer= TestDataFactory_CRM.createBuyer(objBooking.Id, 1, objAcc.Id);
        insert lstBuyer;
        
        Booking_Unit_Active_Status__c objBUStatus = new Booking_Unit_Active_Status__c();
        objBUStatus.Name = 'Active Status';
        objBUStatus.Status_Value__c = 'Active Status';
        insert objBUStatus;
        
        Payment_Plan__c objPaymentPlan = new Payment_Plan__c();
        objPaymentPlan.Booking_Unit__c = BUObj.Id;
        objPaymentPlan.Status__c = 'Active';
        objPaymentPlan.Effective_To__c = System.Today() + 1;
        insert objPaymentPlan;
    }
    
    static testMethod void TestMethod1() {
        testData();
        
         Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();
         Case objCase = new Case();
         objCase.recordTypeId = caseRecordTypeId;
         objCase.AccountId = objAcc.Id;
         objCase.Booking_Unit__c = BUObj.Id;
         insert objCase;
         
        Test.startTest();
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockHandover());        
        Pagereference pageRef = Page.EarlySettlementHandoverProcessPage;
        Test.setCurrentPageReference(pageRef);
        Apexpages.StandardController stdController = new Apexpages.StandardController(objCase); 
        EarlySettlementHandoverCREProcess objController = new EarlySettlementHandoverCREProcess(stdController);
        objController.currentPaymentPlan();
        objController.rowToRemove = '2';
        objController.removeInstallment();
        objController.rowToRemove = '4';
        objController.addInstallment();
        List<EarlySettlementHandoverCREProcess.SelectedNewPaymentTermsByCRE> existingPT = objController.lstNewPaymentTermsDisplay; 
        system.debug('!!!!!existingPT'+existingPT);
        existingPT[0].paymentDate = '05/22/2020'; 
        objController.updatedPT = 'I002';
        objController.validationOnPaymentTerm();
        objController.draftCase();
        objController.submitCase();
        Test.stopTest();
    }
}