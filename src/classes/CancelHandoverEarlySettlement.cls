public without sharing class CancelHandoverEarlySettlement{
    public string caseId;
    public CancelHandoverEarlySettlement(ApexPages.StandardController stdCon){
        caseId = stdCon.getId();
        //objC = (Case)stdCon.getRecord();
    }
    
    public pageReference cancelHO(){
        list<Case> lstCase = [Select Id
                                   , CaseNumber
                                   , Booking_Unit__r.Registration_ID__c
                                   , AccountId
                                   , Is_New_Payment_Terms_Applied__c 
                                   , Status
                                   , Offer_Acceptance_Letter_Generated__c
                                   , RecordTypeId
                                   , RecordType.DeveloperName
                              from Case 
                              where Id =: caseId];
        if(!lstCase.isEmpty()){
            Boolean isPaymentPlanReverse = PaymentPlanReversalMultipleService.updatePaymentPlanInIPMS(lstCase[0].Booking_Unit__r.Registration_ID__c
                                                                                                    , '2-'+lstCase[0].CaseNumber 
                                                                                                    , lstCase[0]);
            system.debug('*****isPaymentPlanReverse*****'+isPaymentPlanReverse);
            if(isPaymentPlanReverse){
                revertPaymentPlan(lstCase[0]);
                return new Pagereference('/'+caseId);
            }else{
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Payment Plan could not be reversed in IPMS. Please contact your administrator.');
                ApexPages.addMessage(myMsg);
                return null;
            }
        }
        return new Pagereference('/'+caseId);
    }
    
    public void revertPaymentPlan(Case objC){
        list<Payment_Plan__c> newPaymentPlanList = new list<Payment_Plan__c>();
        for(Payment_Plan__c objPaymentPlan : [SELECT Id
                                                   , Parent_Payment_Plan__c
                                                   , Status__c 
                                              FROM Payment_Plan__c 
                                              WHERE Booking_Unit__c =:objC.Booking_Unit__c
                                              AND Status__c = 'Active']){
            Payment_Plan__c objNewPaymentPlan = new Payment_Plan__c();
            objNewPaymentPlan.Id = objPaymentPlan.Id;
            objNewPaymentPlan.Status__c = 'InActive';
            newPaymentPlanList.add(objNewPaymentPlan);
            
            Payment_Plan__c objOriginalPP = new Payment_Plan__c();
            objOriginalPP.Id = objPaymentPlan.Parent_Payment_Plan__c;
            objOriginalPP.Status__c = 'Active';
            newPaymentPlanList.add(objOriginalPP);
        }
        system.debug('*****newPaymentPlanList*****'+newPaymentPlanList);
        if(!newPaymentPlanList.isEmpty()){
            update newPaymentPlanList;
        }
    }
}