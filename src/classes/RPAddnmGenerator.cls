/*************************************************************************************************************************************
Description: Controller to generate RP addendum
======================================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By   | Comments
--------------------------------------------------------------------------------------------------------------------------------------
1.0     | 27-01-2021       | Aishwarya Todkar   | 1. Initial Draft
**************************************************************************************************************************************/
public class RPAddnmGenerator {
    static Case objCase;
    public RPAddnmGenerator(ApexPages.StandardController controller) {
        objCase = ( Case )controller.getRecord();
    }

/*************************************************************************************************************************************
 * Description  : method to generate document from drawloop
 * Pararmeter(s): None
 * return       : void
 *************************************************************************************************************************************/
    public static void generateRPAddendm() {

        if( String.isNotBlank( objCase.Additional_Offer_Type__c ) 
        && objCase.Additional_Offer_Type__c.equalsIgnoreCase( 'New 1 year Addendum' ) ) {
            
            Riyadh_Rotana_Drawloop_Doc_Mapping__c  obj 
                = Riyadh_Rotana_Drawloop_Doc_Mapping__c.getInstance('1 Year Rental Pool Addendum');
                system.debug('drawloopvalues:'+obj);

            if( obj != null 
            && String.isNotBlank( obj.Drawloop_Document_Package_Id__c ) 
            && String.isNotBlank( obj.Delivery_Option_Id__c) ) {
                DrawloopDocGen.generateDoc( obj.Drawloop_Document_Package_Id__c
                                        , obj.Delivery_Option_Id__c
                                        , objCase.Id );
            }
            else {
                ApexPages.addmessage( new ApexPages.message( ApexPages.severity.ERROR, 
                    'Drawloop details are not configured correctly' ) );
            }
        }
        else {
            String errMsgg = String.isNotBlank( objCase.Additional_Offer_Type__c ) ?
                            'Rental Pool Addendum cannot be generated for ' + objCase.Additional_Offer_Type__c + ' Additional offer type!'
                            : 'Rental Pool Addendum cannot be generated!' ;
            ApexPages.addmessage( new ApexPages.message( ApexPages.severity.ERROR, errMsgg ) );
        }
    }
}