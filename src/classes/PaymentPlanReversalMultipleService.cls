public without sharing class PaymentPlanReversalMultipleService {
    public static boolean createExceptionUseCase = false;
    
    public PaymentPlanReversalMultipleService(){
        createExceptionUseCase = false;
    }
    
    public class Data {
        public List<PaymentPlanReversalMultipleServiceRes> result         {get;set;}
    }
    
    public class PaymentPlanReversalMultipleServiceRes {
        public String P_REGISTRATION_ID                         {get;set;}
        public String message                                   {get;set;}
        public String P_SR_NUMBER                               {get;set;}
        public String status                                    {get;set;}
    }
    
    public static Boolean updatePaymentPlanInIPMS( String commaSeparatedRegID , String commaSeparatedPSrnumber 
                                              , Case objCase ) {
        system.debug('commaSeparatedRegID : '+commaSeparatedRegID);
        system.debug('commaSeparatedPSrnumber : '+commaSeparatedPSrnumber);
        Boolean isPaymentPlanReverse = false;
        paymentPlanService.AOPTHttpSoap11Endpoint calloutObj = new paymentPlanService.AOPTHttpSoap11Endpoint();
        calloutObj.timeout_x = 120000;
        Data resObj = new Data();

        try{
            system.debug('in try');
            
            String response = calloutObj.PaymentPlanReversalMultiple( commaSeparatedRegID
                                                                  , commaSeparatedPSrnumber );
            system.debug('== response  =='+response );
            resObj = (Data)JSON.deserialize(response, PaymentPlanReversalMultipleService.Data.class);
            system.debug('resObj response === '+ resObj);
            if( resObj.result[0].status.equalsIgnoreCase('S')) {
                //objCase.is_Payment_Plan_Reversed = true;
                isPaymentPlanReverse = true;
            }else {
                //objCase.is_Payment_Plan_Reversed = false;
                isPaymentPlanReverse = false;
            }
            //update objCase;
            if(createExceptionUseCase){
                Account objA = new Account();
                insert objA;
            }
                
        }catch(Exception e){
            system.debug(' Exception : ' + e);
            Error_Log__c errorInst = new Error_Log__c();
            errorInst.Error_Details__c = e.getMessage();
            errorInst.Case__c = objCase.Id;
            if( objCase.AccountId != null ) { 
                errorInst.Account__c = objCase.AccountId;
            }
            if(objCase.RecordTypeId != null
            && objCase.RecordType.DeveloperName != null){
                if(objCase.RecordType.DeveloperName.contains('AOPT')){
                    errorInst.Process_Name__c = 'AOPT';
                }else if(objCase.RecordType.DeveloperName.contains('Handover')){
                    errorInst.Process_Name__c = 'Handover';
                }
            }
            insert errorInst;
            isPaymentPlanReverse = false;
            if(createExceptionUseCase == null || createExceptionUseCase == false){
                resObj.result[0].status = 'Exception';
                resObj.result[0].message = String.valueOf( e ) ;
            }
        }
        return isPaymentPlanReverse;
    }
}