/*
    13/7/19    Arjun Khatri        Updated Calling List owner to 'CRM NHO Q'
*/

public class createLHOCallinglist{
    
    @InvocableMethod 
    public static void invokeApex(list<Id> lstCaseId) {
        map<Id, Calling_List__c> mapCaseIdCallingList = new map<Id, Calling_List__c>();
        list<Calling_List__c> lstCallingLists = new list<Calling_List__c>();
        String arabQueueID, nonArabQueueId;
        String BuildingName;
        String URLsDocument;
        Case lhoCase;
        String LHOCLId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Lease Handover Calling List').getRecordTypeId();
        system.debug('!!!!!LHOCLId'+LHOCLId);
        system.debug('!!!!!lstCaseId'+lstCaseId);
        
        for (Calling_List__c objCall : [Select Id
                                            , RecordTypeId
                                            , Case__c
                                            , Calling_List_Status__c 
                                        From Calling_List__c 
                                        Where Calling_List_Status__c != 'Closed' 
                                        and RecordTypeId =: LHOCLId 
                                        and Case__c IN: lstCaseId]) {
            mapCaseIdCallingList.put(objCall.Case__c, objCall);
        }
        for (SR_Attachments__c objAttach : [Select Id
                                                , Name
                                                , Case__c
                                                , Type__c
                                                , Attachment_URL__c
                                            From SR_Attachments__c
                                            Where Attachment_URL__c != null
                                            and Case__c IN: lstCaseId]) {
            String documentlink = '<a href="'+ objAttach.Attachment_URL__c + '"target="_blank">'+ objAttach.Name + '</a>';
            if (string.isBlank(URLsDocument)) {
                URLsDocument = '<ul><li>' + documentlink + '</li></ul>';
            } else{
                if (URLsDocument.contains('</ul>')) {
                    URLsDocument = URLsDocument.replace('</ul>', '');
                }
                URLsDocument = URLsDocument +'<li>' + documentlink + '</li></ul>';
            }
                                        
        }
        
        List<Group> lstQueues = [select Id,Name
                                from Group
                                where Type = 'Queue'
                                AND (name=: Label.Non_Elite_Non_Arabs_Queue
                                OR Name =: label.Non_Elite_Arabs_Queue)
                                LIMIT 2];

        for(Group grp : lstQueues) {
            if(grp.Name == Label.Non_Elite_Non_Arabs_Queue) {
                arabQueueID = grp.Id;
            }
            else if(grp.Name == Label.Non_Elite_Arabs_Queue) {
                nonArabQueueId = grp.Id;
            }
        }
        
         for (Case objCase : [Select Id
                                , Booking_Unit__c
                                , Booking_Unit__r.Booking__r.Account__r.Primary_CRE__c
                                , OwnerId
                                , Owner.Name
                                , Booking_Unit__r.Booking__r.Account__r.Secondary_CRE__c
                                , Booking_Unit__r.Booking__r.Account__r.Tertiary_CRE__c
                                , Booking_Unit__r.Booking__r.Account__r.Primary_Language__c
                                , Booking_Unit__r.Registration_ID__c
                                , CaseNumber
                                , Unit_Name__c
                                , Monthly_Rent__c
                                , Lease_Commencement_Date__c
                                , Lease_End_Date__c
                                , Completed_Milestones_Percent__c
                                , Account.Passport_Number__c
                                , Account.Passport_Expiry_Date__pc
                                , Parking_Details_JSON__c
                                , Call_Outcome__c
                                , Account.Name
                                , Booking_Unit__r.RGS_Percent__c
                                , AccountId
                              From Case
                              Where Id IN: lstCaseId]) {
            lhoCase = objCase;    
            BuildingName = objCase.Unit_Name__c.substringBefore('/');
            if (mapCaseIdCallingList == null || !mapCaseIdCallingList.containsKey(objCase.id)) {
                 
                    String ownerId = [Select Queue.Id, Queue.Name, Queue.Type from QueueSObject 
                                        WHERE Queue.Type ='Queue' 
                                    AND Queue.Name = 'CRM NHO Q' Limit 1][0].Queue.Id;
                    /*String.isNotBlank(objCase.Booking_Unit__r.Booking__r.Account__r.Primary_CRE__c) ? objCase.Booking_Unit__r.Booking__r.Account__r.Primary_CRE__c :
                    (String.isNotBlank(objCase.Booking_Unit__r.Booking__r.Account__r.Secondary_CRE__c) ? objCase.Booking_Unit__r.Booking__r.Account__r.Secondary_CRE__c :
                    (String.isNotBlank(objCase.Booking_Unit__r.Booking__r.Account__r.Tertiary_CRE__c) ? objCase.Booking_Unit__r.Booking__r.Account__r.Tertiary_CRE__c :
                    (objCase.Booking_Unit__r.Booking__r.Account__r.Primary_Language__c == 'Arabic' ? arabQueueID : nonArabQueueId)));*/
                    
                    Calling_List__c objCL = new Calling_List__c(RecordTypeId = LHOCLId, Registration_ID__c = objCase.Booking_Unit__r.Registration_ID__c,
                        Customer_Flag__c=true, Case__c = objCase.Id );
                        if(String.isNotBlank(ownerId)) {
                             objCL.ownerId = ownerId;
                        }
                       
                    lstCallingLists.add(objCL); 
             }
                              
        }
        
         if (lstCallingLists != null && lstCallingLists.size() > 0) {
             insert lstCallingLists;
         }
         string HoEmail;
         if (!string.isBlank(BuildingName)) {
             for (Location__c objLocation : [Select id
                                                , Name
                                                , Handover_Team_notification_Email__c
                                             From Location__c
                                             Where Name =: BuildingName]) {
                 HoEmail = objLocation.Handover_Team_notification_Email__c;                        
             }
         
         }
         String leaseEmails = Label.Lease_Management_Email;
         list<String> lstRecipients = leaseEmails.split(',');
         if (!string.isBlank(HoEmail)) {
             lstRecipients.add(HoEmail);
         }
         Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
         String mailMessage = '';
         OrgWideEmailAddress[] owea;
         EmailTemplate templateId; 
         try {
             templateId = [select id
                                , Body
                                , Subject
                                , HtmlValue
                                , name 
                           from EmailTemplate 
                           where developername = 'Notify_Lease_Management'];
             owea = [select Id from OrgWideEmailAddress where Address = 'no-replysf@damacgroup.com'];
         }
         catch (Exception e) {
             system.debug('!!!e'+e);
         }
         
         if (templateId != null) {
             mailMessage = templateId.HtmlValue;
             mailMessage = mailMessage.replace('{!Case.Unit_Name__c}', lhoCase.Unit_Name__c);
             mailMessage = mailMessage.replace('{!Case.Account}', lhoCase.Account.Name);
             if (lhoCase.Booking_Unit__r.RGS_Percent__c != null) {
                 mailMessage = mailMessage.replace('{!Booking_Unit__c.RGS_Percent__c}', lhoCase.Booking_Unit__r.RGS_Percent__c);
             } else {
                 mailMessage = mailMessage.replace('{!Booking_Unit__c.RGS_Percent__c}', '');
             }             
             if (lhoCase.Call_Outcome__c != null) {
                 mailMessage = mailMessage.replace('{!Case.Call_Outcome__c}', lhoCase.Call_Outcome__c);
             } else {
                 mailMessage = mailMessage.replace('{!Case.Call_Outcome__c}', '');
             }
             if (lhoCase.Owner.Name != null) {
                 mailMessage = mailMessage.replace('{!Case.OwnerFullName}', lhoCase.Owner.Name);
             } else {
                 mailMessage = mailMessage.replace('{!Case.OwnerFullName}', '');
             }
             if (!string.isBlank(URLsDocument)) {
                mailMessage = mailMessage.replace('{!Case.Rule_Engine_Response__c}', URLsDocument);
             } else {
                mailMessage = mailMessage.replace('{!Case.Rule_Engine_Response__c}', '');
             }
             email.subject = templateId.Subject.replace('{!Case.Unit_Name__c}', lhoCase.Unit_Name__c);
         }
         email.setHtmlBody(mailMessage);
         email.toAddresses = lstRecipients;
         email.setWhatId(lhoCase.AccountId);
         email.setSaveAsActivity(true);
         if ( owea.size() > 0 ) {
                email.setOrgWideEmailAddressId(owea.get(0).Id);
            }
         try {
             Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
             return;
        }
        catch (EmailException e) {
            system.debug('!!!!!!!e'+e);
        }
    }
}