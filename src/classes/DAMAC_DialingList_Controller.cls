/*
    Created By: Subhash, Srikanth V
    Functionality: To be used for Dialer to auto call customers
*/
global without sharing class DAMAC_DialingList_Controller{
    
    public Dialer_Active_Log__c activeLog {get; set; }
    
    public boolean enableDialing {get;set;}
    
    public boolean isUseronline {get;set;}
    
    public String mobileNumber { get; set; }
    
    public list<Dialing_List__c> todialList {get;set;}
    
    public boolean prechecksPass {get;set;}
    
    public transient string userExtension {get;set;}
    
    public inquiry__c inq {get;set;}
    
    public transient list<dialing_list__c> totalDialingList {get;set;}

    public DAMAC_DialingList_Controller(){
        activeLog = new Dialer_Active_Log__c  ();
       prechecksPass = false; 
       prechecksPass = preChecks();
       
    }
    // Method to redial when the call completes
    public void init () {
        activeLog = new Dialer_Active_Log__c  ();
        
        enableDialing = checkDialingEnabled();
        isUseronline  = checkUserStatus();
        if (isUserOnline) {
            activeLog.Off_Reason__c = 'Online';
        } else {
            if (activeLog.Off_Reason__c == NULL)
                activeLog.Off_Reason__c = 'Away';
        }
        boolean isLocked = checkDialingLocked();
        user u = getuserDetails(); 
        userExtension = u.extension;
        if(enableDialing && isUseronline && !isLocked ) {
            getCallingList();
            if(todialList.size()>0){
                prechecksPass = true;
            } else
                prechecksPass = false;
        }           
        
    }
    // Method to check user availability and Dialing list
    public boolean preChecks(){
        boolean result = false;
        enableDialing = checkDialingEnabled();
        isUseronline  = checkUserStatus();
        if (isUserOnline) {
            activeLog.Off_Reason__c = 'Online';
        }
        else {
            if (activeLog.Off_Reason__c == NULL)
                activeLog.Off_Reason__c = 'Away';
        }
        boolean isLocked = checkDialingLocked();
        user u = getuserDetails(); 
        userExtension = u.extension;
        if(enableDialing && isUseronline && !isLocked ){
            getCallingList();
            if(todialList.size()>0){
                result = true;
            }else{
                result = false;
            }
        }else{
            result = false;
        }
        return result;
    }
    
    //Check Dialing Enabled 
    public boolean checkDialingEnabled(){
        //Check Enabled Globally
        
        //Check Enabled on user
        return true;
    }
    
    //Get Total List for the Day
    public void totalDialingListfortoday(){
        totalDialingList = new list<dialing_list__c>();
        totalDialingList = [select id from dialing_list__c where status__c=null and dial_date__c=today and ownerid=:userinfo.getuserid()];
        if(totalDialingList.size() == 20){
            //Reset All Marked for retry
            list<dialing_list__c> toResetList = new list<dialing_list__c>();
            toResetList = [select id from dialing_list__c where status__c='Marked for Retry' and ID Not IN:totalDialingList];
            
            for(dialing_list__c d:toResetList ){
                d.status__c = null;
                d.Retry_Date_Time__c = null;
            }
            update toResetList;
            
        }
        
    
    }
    
    
    // To check user avialability from Assignment Rules
    public boolean checkUserStatus(){
        //Check for Away or Online
        Boolean enableUserDial = false;
        list<Inquiry_User_Assignment_Rules__c> userStat = new list<Inquiry_User_Assignment_Rules__c>();
        userStat = [select id,Available_for_Dailing__c, Off_Reason__c from Inquiry_User_Assignment_Rules__c where SetupOwnerId =:userinfo.getuserid() LIMIT 1]; 
        if(userStat.size() > 0){
            enableUserDial = userStat[0].Available_for_Dailing__c;
            activeLog.Off_Reason__c = userStat[0].Off_Reason__c ;
        }
        Inquiry_User_Assignment_Rules__c globalSetting = Inquiry_User_Assignment_Rules__c.getInstance ();
        if (!globalSetting.Available_for_Dailing__c) {
            enableUserDial = false;
        }
        return enableUserDial;
    }
    // To get user details
    public user getuserDetails(){        
        user u = [select id,extension from user WHERE id=:userinfo.getuserid()];
        return u;
    }
    // Method to get dialing list records which are owned by logged in user based on priority
    public void getCallingList(){
        todialList = new list<Dialing_List__c>();
        todialList = [select id,
                        inquiry__c,
                        Status__c,
                        Call_back_Date_time__c,
                        Call_Id__c,
                        Current_UAE_Time__c,
                        Extension_to_dial__c,
                        Is_in_working_hours__c,
                        Phone_Country_Time__c  ,
                        Dialing_Priority__c,
                        Retry_Count__c,
                        Retry_Date_Time__c,
                        Retry_in_Next_5_Mins__c,
                        Task_Id__c,
                        UTC__c,
                        UTC_Hrs_Difference__c,
                        Wrap_up_Link__c,
                        Wrap_up_Pending__c        
            from Dialing_List__c 
            where ownerid=:userinfo.getuserid() 
            AND status__c=NULL
            /*
            and status__c !='Dialing Complete'
            AND status__c !='In Progress'
            AND status__c ='Marked for Retry'
            AND Status__c != 'Dialing' 
            */
            AND Exclude_from_Dialing__c = FALSE   
            AND Wrap_up_Pending__c=FALSE AND Is_in_working_hours__c=TRUE AND Retry_in_Next_5_Mins__c=TRUE AND Dial_Date__c=TODAY ORDER BY Dialing_Priority__c ASC LIMIT 1]; 
        
        if(todialList.size() == 0){
            todialList = [select id,
                            inquiry__c,
                            Status__c,
                            Call_back_Date_time__c,
                            Call_Id__c,
                            Current_UAE_Time__c,
                            Extension_to_dial__c,
                            Is_in_working_hours__c,
                            Phone_Country_Time__c  ,
                            Dialing_Priority__c,
                            Retry_Count__c,
                            Retry_Date_Time__c,
                            Retry_in_Next_5_Mins__c,
                            Task_Id__c,
                            UTC__c,
                            UTC_Hrs_Difference__c,
                            Wrap_up_Link__c,
                            Wrap_up_Pending__c
                from Dialing_List__c where ownerid=:userinfo.getuserid() 
                and status__c !='Dialing Complete'
                AND status__c !='In Progress'
                AND status__c !='Marked for Retry'
                AND Status__c != 'Dialing'   
                AND Exclude_from_Dialing__c = FALSE 
                AND inquiry__r.Call_back_Date_time__c = null
                AND Wrap_up_Pending__c=FALSE AND Is_in_working_hours__c=TRUE AND Retry_in_Next_5_Mins__c=FALSE AND Dial_Date__c=TODAY ORDER BY Dialing_Priority__c ASC LIMIT 1]; 
        } 
        if (Test.isRunningTest ()) {
               todialList = [select id,inquiry__c from Dialing_List__c where ownerid=:userinfo.getuserid() ];               
        }
        
        /*
        if(todialList.size() == 0){
            todialList = [select id,inquiry__c from Dialing_List__c where ownerid=:userinfo.getuserid() 
                and status__c !='Dialing Complete'
                AND status__c !='In Progress'
                AND status__c ='Marked for Retry'
                AND Status__c != 'Dialing'   
                AND Wrap_up_Pending__c=FALSE AND Is_in_working_hours__c=TRUE AND Retry_Count__c <= 3 ORDER BY Dialing_Priority__c ASC LIMIT 1]; 
        } 
        */
        if (todialList.size () > 0) {
            inq = new Inquiry__c ();
            //inq = [SELECT Mobile_Phone_Encrypt__c FROM Inquiry__c WHERE ID =: todialList[0].Inquiry__c LIMIT 1];
            id inquiryId = todialList[0].Inquiry__c;
            inq = Database.query ('SELECT Campaign__r.Campaign_Description__c,'+getAllFields ('Inquiry__c')
                                  +' FROM Inquiry__c where ID=:inquiryId LIMIT 1');
            String decryptedNumber = UtilityHelperCls.decryptMobile(inq.Mobile_Phone_Encrypt__c);
            mobileNumber = Damac_PhoneEncrypt.encryptPhoneNumber (decryptedNumber);
        }
    }
    
    public void onLoadInquiryDetailsForListenerPage(){
        inq = new Inquiry__c ();
            //inq = [SELECT Mobile_Phone_Encrypt__c FROM Inquiry__c WHERE ID =: todialList[0].Inquiry__c LIMIT 1];
            id inquiryId = apexpages.currentpage().getParameters().get('inqId');
            inq = Database.query ('SELECT Campaign__r.Campaign_Description__c,'+getAllFields ('Inquiry__c')
                                  +' FROM Inquiry__c where ID=:inquiryId LIMIT 1');
    
    }
    
    // remote method to update the inquiry details and task details when customer picks the phone call
    @remoteAction
    global static callDetails checkDetails (String customerCalId, String callId, String userExt, String inqID, String direction, 
        String status, String calledNumber, String did,string eventType) {
        callDetails details = new callDetails ();
            Inquiry__c inq = new Inquiry__c ();
            
            Boolean isNew = false;
            if (inqID != '' && inqID != NULL) {
                inq.Call_ID__c = callID;
                //inq.OwnerId = UserInfo.getUserID ();
                inq.ID = inqID;
                Update inq;
            } else {
                inq.Call_ID__c = callID;
                isNew = true;
                Id preInquiryRecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
                inq.RecordTypeId = preInquiryRecordTypeId;
                inq.Is_New_Waybeo__c = TRUE;
                insert inq;
                inqId = inq.id;
            }
            
            Task t = new Task ();
            inq = [ SELECT Mobile_phone__c, Campaign__c FROM Inquiry__c WHERE ID =:inqId];
            JO_Campaign_Virtual_Number__c vNumber = new JO_Campaign_Virtual_Number__c ();          
            try {
                t = [SELECT ID, sql_ID__c FROM Task WHERE SQL_Id__c =: callID];
    
                vNumber = [SELECT Related_Campaign__c, Related_Virtual_Number__r.Waybeo_Called_Number__c
                         FROM JO_Campaign_Virtual_Number__c WHERE Related_Campaign__c =: inq.Campaign__c LIMIT 1];
                            
            }
            catch (Exception e) {}
            t.Sql_Id__c = callID;
            //t.Event_Type__c = 'Dialer Event';
            t.Event_Type__c = eventType;
            t.whatID = inq.ID;
            t.Calling_Number__c = inq.Mobile_phone__c;
            if (vNumber.iD != NULL)
                t.Campaign_Number__c = vNumber.Related_Virtual_Number__r.Waybeo_Called_Number__c;
            if (direction == 'Outbound') {
                //t.Activity_Type_3__c = 'Call - Outbound';
                t.Activity_Type_3__c = 'Outbound';
                t.Subject = 'Call - Outbound';
                t.Campaign_Number__c = calledNumber;
            }
            if (status == 'Ringing') {
                //t.Activity_Type_3__c = 'Call-Outbound Attempted';
                t.Activity_Type_3__c = 'Outbound';
                t.Subject = 'Call-Outbound Attempted';
            }
            String msg = '';
            try {
                upsert t Sql_Id__c;
                msg = t.id;
            } catch (Exception e) {
                msg = e.getMessage ();
                
                System.Debug ('==='+msg);
                if (msg != NULL) {
                    msg = msg.substringBetween ('id: ', ':');
                    if (test.isRunningTest ()) {
                        t = new Task ();
                        t.Subject='test';
                        t.Status='New';
                        t.SQL_ID__C = '1234';
                        t.Priority='Normal';
                        t.CallType='Outbound';
                        insert t;
                        msg = t.id;
                    }
                    if (!msg.startsWith ('00T')) {
                        msg = '';
                    } else {
                        if (direction == 'Outbound') {                            
                            t = new Task ();
                            t.id = msg;
                            t.Event_Type__c = 'Dialer Event';
                            if (vNumber.iD != NULL)
                                t.Campaign_Number__c = vNumber.Related_Virtual_Number__r.Waybeo_Called_Number__c;
                            t.Calling_Number__c = inq.Mobile_phone__c;
                            t.Campaign_Number__c = calledNumber;
                            t.whatId = inq.id;
                            t.status = 'Not Started';
                            t.Activity_Type_3__c = 'Call - Outbound';
                            t.Subject = 'Call - Outbound';
                            update t;
                        }
                    }
                } else {
                    msg = '';
                }
            }
            if (did != NULL && did != '') {
                Dialing_List__c dialList = new Dialing_List__c ();
                dialList = [SELECT Retry_Count__c, Status__c  FROM Dialing_List__c WHERE id =: DID LIMIT 1];
                dialList.id = did;
                dialList.Task_Id__c = msg;
                if (dialList.Status__c != 'Marked for Retry'){
                    dialList.Status__c = 'In Progress';
                    dialList.Wrap_up_Pending__c = true;
                }
                update dialList;
            }
                
            
            details.inquiryID = inq.ID;
            details.taskID = msg;
            details.isNew = isNew;
            return details;
        
    } 
    global class callDetails {
        public ID inquiryID;
        public ID taskID;
        public Boolean isNew;
    }
    // Remote method to update the Status of Dialing list record based on call Id
    @remoteAction
    global static void unlockDialingCallId(String DID, String didStatus){ // Mark Status to Complete
        Dialing_List__c dialList = new Dialing_List__c ();
        dialList = [SELECT Retry_Count__c, Status__c  FROM Dialing_List__c WHERE dial_date__c=today AND call_id__c =: DID LIMIT 1];
        dialList.Status__c = didStatus;
        if (didStatus == 'Marked for Retry') {
            dialList.Retry_Date_Time__c = System.Now ().addHours (3);
            if (dialList.Retry_Count__c == NULL)
                dialLIst.Retry_Count__c = 1;
            else
                dialList.Retry_Count__c = dialList.Retry_Count__c + 1;
        }
        dialList.id = dialList.id;
       
            
        update dialList;
    }
    // Method to update the status and retry count based on Dialing list record ID
    @remoteAction
    global static void unlockDialing(String DID, String didStatus){ // Mark Status to Complete
        System.Debug (didStatus);
        System.Debug (did);
        Dialing_List__c dialList = new Dialing_List__c ();
        dialList = [SELECT Retry_Count__c, Status__c,Wrap_up_Pending__c   FROM Dialing_List__c WHERE ID =: DID LIMIT 1];
        dialList.Status__c = didStatus;
        if (didStatus == 'Marked for Retry') {
            dialList.Retry_Date_Time__c = System.Now ().addHours (3);
            if (dialList.Retry_Count__c == NULL){
                dialLIst.Retry_Count__c = 1;
            }else{
                dialList.Retry_Count__c = dialList.Retry_Count__c + 1;
            }
            dialLIst.Wrap_up_Pending__c = false;
        }
        //dialList.id = did;
        dialList.id = dialList.id;
        
            
        System.Debug (dialList.Status__c);
        System.Debug (dialList.Id);
        update dialList;
    }
    // Method to check whether Dialing list record is in locked state
    public boolean checkDialingLocked(){
        boolean islocked = false;
        list<Dialing_List__c> lockedList = new list<Dialing_List__c>();
        lockedList = [select id from Dialing_List__c where ownerid=:userinfo.getuserid() and 
               ( status__c = 'In Progress'
                OR status__c = 'Dialing' or Wrap_up_Pending__c=true) and lastmodifieddate=today and dial_date__c=today LIMIT 1];
        if(lockedList.size() > 0){
            islocked = true;
        } 
        return islocked;
    }
    
    public void DialNext(){
        //Do All Pre Checks
        prechecksPass = false;
        prechecksPass = preChecks();
    }
    // to change the avialability of user in assignment rules based on the check box on the dialer page
    public void updateAvailiableStatus(){
        if (activeLog.Off_Reason__c == 'Online')
            isUseronline = true;
        else
            isUseronline = false;
        system.debug('>>>>>>>>>>>>>>>>'+isUseronline);
        Inquiry_User_Assignment_Rules__c r = new Inquiry_User_Assignment_Rules__c();
        r = [select id,Available_for_Dailing__c, Off_Reason__c from Inquiry_User_Assignment_Rules__c where SetupOwnerId =:userinfo.getuserid()];
        System.Debug (r);
        r.Off_Reason__c = activeLog.Off_Reason__c ;
        r.Available_for_Dailing__c = isUseronline;
        update r;
        
        upsertDialerLog (isUseronline, activeLog.Off_Reason__c);
        prechecksPass = preChecks();
    }
    
    
    /* To Get all the Fields of an Object - used in Query String */
    public static string getAllFields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);

        if (objectType == null)
            return fields;

        Map <String, Schema.SObjectField> fieldMap = objectType.getDescribe ().fields.getMap ();

        for (Schema.SObjectField sfield : fieldMap.Values()) {

            fields += sfield.getDescribe().getName ()+ ', ';

        }
        return fields.removeEnd(', ');
    }
    
    
     
    public pagereference reset(){
        list<dialing_list__c> blockedList = new list<dialing_list__c>();
        blockedList = [select id,status__c,Retry_Count__c,Wrap_up_Pending__c from 
                        
                        dialing_list__c where ownerid=:userinfo.getuserid() and (status__c='Dialing' or status__c='In Progress') AND dial_date__c=today LIMIT 1];
        if (Test.isRunningTest ()) {
               todialList = [select id,inquiry__c from Dialing_List__c where ownerid=:userinfo.getuserid() ];
        }
        if(blockedList.size() > 0){
            for(dialing_list__c d:blockedList){
                d.status__c = 'Marked for Retry';
                if(d.Retry_Count__c != null){
                    d.Retry_Count__c = d.Retry_Count__c+1;
                }else{
                    d.Retry_Count__c = 1;
                }
                if(d.wrap_up_pending__c){
                   d.wrap_up_pending__c = false; 
                }
            }
            update blockedList;
        }
        return null;
    }
    
    @Future
    public static void upsertDialerLog (Boolean switchType, String reason) {
        ID userId = UserInfo.getUserID ();
        List <Dialer_Active_Log__c> activityLog = new List <Dialer_Active_Log__c> ();
        activityLog = [SELECT Date__c, Is_On_from_Previous_day__c, Off_Time__c, On_Time__c, User__c, Off_reason__c
                        FROM Dialer_Active_Log__c
                        WHERE Date__c = TODAY AND User__c =: userId Order By CreatedDate Desc LIMIT 1];
        System.Debug (activityLog);

        if (activityLog.size () == 0) {            
            
            Dialer_Active_Log__c log = new Dialer_Active_Log__c ();
            log.User__c = userID;
            log.Date__c = Date.Today ();
            if (switchType) {
                log.On_Time__c = DateTime.Now ();
                log.Off_reason__c = '';  
            } else {                
                log.Off_Time__c = DateTime.Now ();
                log.Off_reason__c = reason;     
                log.Is_On_from_Previous_day__c = TRUE;                  
            }
            insert log;         
            
        } else {
            List <Dialer_Active_Log__c> logsToInsert = new List <Dialer_Active_Log__c> ();
            List <Dialer_Active_Log__c> logsToUpdate = new List <Dialer_Active_Log__c> ();
            for (Dialer_Active_Log__c logRec :activityLog) {
                System.Debug ('====='+logRec);
                if (logRec.On_Time__c != NULL && logRec.Off_Time__c != NULL) {                    
                    Dialer_Active_Log__c log = new Dialer_Active_Log__c ();
                    log.User__c = userID;
                    log.Date__c = Date.Today ();
                    if (switchType) {
                        log.On_Time__c = DateTime.Now ();
                        log.Off_reason__c = '';  
                    } else { 
                        log.On_time__c = DateTime.Now ();
                        log.Off_Time__c = DateTime.Now ();
                        log.Off_reason__c = reason;     
                        
                    }
                    logsToInsert.add (log);
                } else {
                    if (logRec.On_Time__c != NULL && logRec.Off_Time__c == NULL) {
                        logRec.Off_Time__c = DateTime.Now ();
                        logRec.Off_reason__c = reason; 
                        logsToUpdate.add (logRec);
                    }
                    if (logRec.On_Time__c == NULL && logRec.Off_Time__c != NULL) {
                        Dialer_Active_Log__c log = new Dialer_Active_Log__c ();
                        log.User__c = userID;
                        log.Date__c = Date.Today ();
                        if (switchType) {
                            log.On_Time__c = DateTime.Now ();
                            log.Off_reason__c = '';
                        } else {   
                            log.On_time__c = DateTime.Now ();
                            log.Off_Time__c = DateTime.Now ();
                            log.Off_reason__c = reason;     
                        
                        }
                        logsToInsert.add (log);
                    }
                }
                if (logsToInsert.size () > 0)
                    insert logsToInsert;
                if (logsToUpdate.size () > 0)
                    update logsToUpdate;
                    
            }            
        }
    }
    public DAMAC_DialingList_Controller(ApexPages.StandardController controller) {
        user u = getuserDetails(); 
        userExtension = u.extension;
        enableDialing = checkDialingEnabled();
    }




}