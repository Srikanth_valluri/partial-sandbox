/**********************************************************************************************************************
Description: This service is used for getting all the documents list which user needs to upload during Tenant Registration for given BuildingId
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   12-10-2020      | Shubham Suryawanshi | Added the buildingId check in LPG & DC documents logic
***********************************************************************************************************************/

@RestResource(urlMapping='/getDocsForBuilding/*')
global class GetDocumentsForBuildingTenantReg_API {  
    
    public static final String NO_BUILDING = 'No Building found for given buildingId';
    public static final String SUCCESS = 'Successful';
    public static String responseMessage;
    public static Integer statusCode;

    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        //200 => 'OK',
        //400 => 'Bad Request',
        //401 => 'Unauthorized',
        //500 => 'Internal Server Error' 
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };

    @HttpPost
    global static FinalReturnWrapper getDocuments() {

        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);

        List<DocWrapper> documentList = new List<DocWrapper>();

        if(r.params.containsKey('processName') && String.isNotBlank(r.params.get('processName')) 
           && r.params.containsKey('buildingId') && String.isNotBlank(r.params.get('buildingId')) ) {
            
            System.debug('processName : ' + r.params.get('processName'));
            System.debug('buildingId : '+ r.params.get('buildingId'));
            //System.debug('moveInType : ' + r.params.get('moveInType'));

            String processName = r.params.get('processName');
            String buildingId = r.params.get('buildingId');
            String moveInType = r.params.containsKey('moveInType') ? r.params.get('moveInType') : '';
            System.debug('moveInType : ' + moveInType);

            if(processName == 'Tenant Registration') {
                documentList = getTenantRegDocs(buildingId, moveInType);
            }
            else {
                responseMessage = 'Invalid parameter value in processName : '+processName;
                statusCode = 3;
            }

        }
        else {
            //responseMesage handling;
            if( !r.params.containsKey('processName') ) {
                responseMessage = 'Missing parameter: processName';
                statusCode = 3;
            }
            else if(!r.params.containsKey('buildingId')) {
                responseMessage = 'Missing parameter: buildingId';
                statusCode = 3;
            }
            else if( String.isBlank(r.params.get('processName')) ) {
                responseMessage = 'Missing parameter value : processName';
                statusCode = 3;
            }
            else if( String.isBlank(r.params.get('buildingId')) ) {
                responseMessage = 'Missing parameter value : buildingId';
                statusCode = 3;
            }
        }

        cls_data objData = new cls_data();
        objData.document_list = documentList;

        cls_meta_data objMeta = new cls_meta_data();
        //objMeta.is_success = statusCode == 1 ? true : false;
        objMeta.message = responseMessage;
        objMeta.status_code = statusCode;
        objMeta.title = mapStatusCode.get(statusCode);
        objMeta.developer_message = null;

        FinalReturnWrapper objResponse = new FinalReturnWrapper();
        objResponse.data = objData;
        objResponse.meta_data = objMeta;

        System.debug('objResponse:: ' + objResponse);
         return objResponse;
    
    } 


    public static List<DocWrapper> getTenantRegDocs(String buildingId, String moveInType) {

        List<DocWrapper> lstDocs = new List<DocWrapper>();
        List<FM_Documents_Hello_Damac_App__mdt> lstCommonFmDocs = new List<FM_Documents_Hello_Damac_App__mdt>();
        List<District_Cooling_Projects_Hello_Damac_Ap__mdt> lstChilledDocs = new List<District_Cooling_Projects_Hello_Damac_Ap__mdt>();
        List<LPG_Connected_Projects_Hello_Damac_App__mdt> lstLPGDocs = new List<LPG_Connected_Projects_Hello_Damac_App__mdt>();

        List<Location__c> building = [SELECT id , Building_Name__c FROM Location__c WHERE id =: buildingId];

        //List<FM_Process_Hello_Damac_App__mdt> lstProcess = [SELECT id
        //                                                       , label 
        //                                                  FROM FM_Process_Hello_Damac_App__mdt 
        //                                                  WHERE DeveloperName = 'Tenant_Registration'
        //                                                  LIMIT 1];

        if(building.size() > 0) {

            /*As per azure Task : 1773*/
            List<FM_Documents_Hello_Damac_App__mdt> queryLstCommonDocs = [SELECT id
                                                                               , Document_Name__c
                                                                               , Mandatory__c
                                                                               , Unique_Id__c
                                                                               , Process__c 
                                                                               , isMovingCompanyDoc__c
                                                                         FROM FM_Documents_Hello_Damac_App__mdt 
                                                                         WHERE Is_Active__c = true 
                                                                         AND Process__r.DeveloperName = 'Tenant_Registration' 
                                                                         AND IsCommonDocument__c = true];
            System.debug('queryLstCommonDocs' + queryLstCommonDocs);

            if((String.isBlank(moveInType) || moveInType == null) || 
                (String.isNotBlank(moveInType) && !moveInType.equalsIgnoreCase('company')) )  {
                for(FM_Documents_Hello_Damac_App__mdt objMdt : queryLstCommonDocs) {
                    if(objMdt.isMovingCompanyDoc__c == false) {
                        lstCommonFmDocs.add(objMdt);
                    }
                }
            }
            else if(String.isNotBlank(moveInType) && moveInType.equalsIgnoreCase('company')) {
                for(FM_Documents_Hello_Damac_App__mdt objMdt : queryLstCommonDocs) {
                    lstCommonFmDocs.add(objMdt);
                }
            }
            
            System.debug('lstCommonFmDocs' + lstCommonFmDocs);
            /*As per azure Task : 1773 END*/

                               
            if(lstCommonFmDocs.size() > 0 && String.isNotblank(building[0].Building_Name__c)) {
                //lstChilledDocs = [SELECT FM_Documents_Hello_Damac_App__r.Document_Name__c
                //                       , FM_Documents_Hello_Damac_App__r.Mandatory__c
                //                       , FM_Documents_Hello_Damac_App__r.Unique_Id__c 
                //                  FROM District_Cooling_Projects_Hello_Damac_Ap__mdt 
                //                  WHERE Building_Name__c =: building[0].Building_Name__c
                //                  AND FM_Documents_Hello_Damac_App__r.Process__c =: lstCommonFmDocs[0].Process__c];
                
                //lstLPGDocs = [SELECT FM_Documents_Hello_Damac_App__r.Document_Name__c
                //                   , FM_Documents_Hello_Damac_App__r.Mandatory__c
                //                   , FM_Documents_Hello_Damac_App__r.Unique_Id__c  
                //              FROM LPG_Connected_Projects_Hello_Damac_App__mdt 
                //              WHERE Building_Name__c =: building[0].Building_Name__c
                //              AND FM_Documents_Hello_Damac_App__r.Process__c =: lstCommonFmDocs[0].Process__c];                                                                   
                
                /*New implementation for chilled & LPG docs fetching - 07/09/2020*/
                lstChilledDocs = [SELECT FM_Documents_Hello_Damac_App__c
                                       , Building_Record_Id__c
                                       , FM_Documents_Hello_Damac_App__r.Document_Name__c
                                       , FM_Documents_Hello_Damac_App__r.Mandatory__c
                                       , FM_Documents_Hello_Damac_App__r.Unique_Id__c  
                                  FROM District_Cooling_Projects_Hello_Damac_Ap__mdt 
                                  WHERE FM_Documents_Hello_Damac_App__r.Process__c =: lstCommonFmDocs[0].Process__c
                                  AND Building_Record_Id__c != null];
                System.debug('lstChilledDocs' +lstChilledDocs);

                lstLPGDocs = [SELECT FM_Documents_Hello_Damac_App__c
                                   , Building_Record_Id__c
                                   , FM_Documents_Hello_Damac_App__r.Document_Name__c
                                   , FM_Documents_Hello_Damac_App__r.Mandatory__c
                                   , FM_Documents_Hello_Damac_App__r.Unique_Id__c  
                              FROM LPG_Connected_Projects_Hello_Damac_App__mdt 
                              WHERE FM_Documents_Hello_Damac_App__r.Process__c =: lstCommonFmDocs[0].Process__c
                              AND Building_Record_Id__c != null];
                System.debug('lstLPGDocs' +lstLPGDocs);
                //Coming Soon - DEWA Docs for Non-Villa projects
            }
            if(lstCommonFmDocs.size() > 0 ) {
                responseMessage = SUCCESS;
                statusCode = 1;
            }
        }//End of building size check
        else {
            responseMessage = NO_BUILDING;
            statusCode = 1;
        }

        System.debug('lstCommonFmDocs: ' + lstCommonFmDocs);
        System.debug('lstChilledDocs: ' + lstChilledDocs);
        System.debug('lstLPGDocs: ' + lstLPGDocs);
        

        for(FM_Documents_Hello_Damac_App__mdt objCommonDoc : lstCommonFmDocs) {
            DocWrapper objDoc = new DocWrapper();
            System.debug('objCommonDoc.Document_Name__c: ' + objCommonDoc.Document_Name__c);
            System.debug('objCommonDoc.Mandatory__c: ' + objCommonDoc.Mandatory__c);
            System.debug('objCommonDoc.Unique_Id__c: ' + objCommonDoc.Unique_Id__c);

            objDoc.document_name = objCommonDoc.Document_Name__c;
            objDoc.is_mandatory = objCommonDoc.Mandatory__c;
            objDoc.doc_id = objCommonDoc.Unique_Id__c;

            lstDocs.add(objDoc);
        }

        for(District_Cooling_Projects_Hello_Damac_Ap__mdt objChilledDoc : lstChilledDocs) {
            DocWrapper objDoc = new DocWrapper();
            System.debug('objChilledDoc.FM_Documents_Hello_Damac_App__r.Document_Name__c: ' + objChilledDoc.FM_Documents_Hello_Damac_App__r.Document_Name__c);
            System.debug('objChilledDoc.FM_Documents_Hello_Damac_App__r.Mandatory__c: ' + objChilledDoc.FM_Documents_Hello_Damac_App__r.Mandatory__c);
            System.debug('objChilledDoc.FM_Documents_Hello_Damac_App__r.Unique_Id__c: ' + objChilledDoc.FM_Documents_Hello_Damac_App__r.Unique_Id__c);

            //if( objChilledDoc.Building_Record_Id__c.contains(buildingId) ) {
            if( buildingId.contains(objChilledDoc.Building_Record_Id__c) ) {
                objDoc.document_name = objChilledDoc.FM_Documents_Hello_Damac_App__r.Document_Name__c;
                objDoc.is_mandatory = true;
                objDoc.doc_id = objChilledDoc.FM_Documents_Hello_Damac_App__r.Unique_Id__c;

                lstDocs.add(objDoc);
            }
            
        }

        for(LPG_Connected_Projects_Hello_Damac_App__mdt objLPGDoc : lstLPGDocs) {
            DocWrapper objDoc = new DocWrapper();
            System.debug('objLPGDoc.FM_Documents_Hello_Damac_App__r.Document_Name__c: ' + objLPGDoc.FM_Documents_Hello_Damac_App__r.Document_Name__c);
            System.debug('objLPGDoc.FM_Documents_Hello_Damac_App__r.Mandatory__c: ' + objLPGDoc.FM_Documents_Hello_Damac_App__r.Mandatory__c);
            System.debug('objLPGDoc.FM_Documents_Hello_Damac_App__r.Unique_Id__c: ' + objLPGDoc.FM_Documents_Hello_Damac_App__r.Unique_Id__c);
            
            //if( objLPGDoc.Building_Record_Id__c.contains(buildingId) ) {
            if( buildingId.contains(objLPGDoc.Building_Record_Id__c) ) {
                objDoc.document_name = objLPGDoc.FM_Documents_Hello_Damac_App__r.Document_Name__c;
                objDoc.is_mandatory = true;
                objDoc.doc_id = objLPGDoc.FM_Documents_Hello_Damac_App__r.Unique_Id__c;

                lstDocs.add(objDoc);
            }
            
        }

        System.debug('lstDocs:: ' + lstDocs);
        return lstDocs;
    }

    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_data {
        //public String building_id;
        public DocWrapper[] document_list;
    }

    public class cls_meta_data {
        //public Boolean is_success {get; set;} ruled Out
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message;
    }

    public class DocWrapper {
        public String document_name;
        public boolean is_mandatory;
        public String doc_id;
    }

    

}

/*
List<String> documentList = GetDocumentsForBuildingTenantReg_API.getTenantRegDocs('a1J25000000KrH7EAK');
System.debug('documentList:: ' + documentList);
System.debug('documentList.size:: ' + documentList.size());

*/