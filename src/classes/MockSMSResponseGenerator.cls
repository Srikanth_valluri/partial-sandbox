@isTest

public class MockSMSResponseGenerator implements HttpCalloutMock {

    // Implement this interface method
    public HTTPResponse respond(HTTPRequest req) {

        
        // Create a fake response

        HttpResponse res = new HttpResponse();

        res.setHeader('Content-Type', 'application/json');

        res.setBody('{"Status":"OK","Data":{"BulkId":3412690}}');

        res.setStatusCode(200);

        return res;
    }

}