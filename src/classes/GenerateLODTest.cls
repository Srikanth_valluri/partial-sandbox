/******************************************************************************
* Description - Test class developed for GenerateLOD
*
* Version            Date            Author                    Description
* 1.0                17/12/17        Naresh Kaneriya (Accely)   Initial Draft
********************************************************************************/

@isTest
public class GenerateLODTest{
    @istest static void TestMEthod1(){
       Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;  
        System.assert(Acc != null);
        
         NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);  
     
        Case pCas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        pCas.Booking_Unit__c = BU.Id ;
        pCas.Type = 'NOCVisa';
        pCas.IsRunningTest__c = true;
        pCas.Document_Verified__c = true;
        pCas.Total_Snags_Closed__c = 3;
        insert pCas;
        
        Case   Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = BU.Id ;
        Cas.Type = 'NOCVisa';
        Cas.IsRunningTest__c = true;
        Cas.Document_Verified__c = true;
        Cas.Total_Snags_Closed__c = 3;
        Cas.ParentId = pCas.Id;
        insert Cas;
        System.assert(Cas != null);
        
        ApexPages.currentPage().getParameters().put('id',Cas.Id);
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,HandoverMQServices.LetterofDischargeResponse_element>();
        HandoverMQServices.LetterofDischargeResponse_element response1 = new HandoverMQServices.LetterofDischargeResponse_element();
        response1.return_x =    '{'+
        ' "data" : ['+
        ' {'+
        '   "P_PARAM_ID" : "123",'+
        '   "P_PROC_STATUS" : "S",'+
        '   "P_PROC_MESSAGE" : "Message",'+
        '   "URL" : "www.google.com",'+
        '   "P_ATTRIBUTE2" : "123",'+
        '   "P_ATTRIBUTE3" : "321"'+
        '   '+
        '   '+
        '    '+
        ' }'+
        ' ],'+
        '  '+
        '  "message":"Message",'+
        '  "status":"S"'+
        '  '+
        '}';
       
       
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
 
        PageReference pageRef = Page.GeneratePCC;
        pageRef.getParameters().put('id', String.valueOf(Cas.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(Cas);
        GenerateLOD controller = new GenerateLOD(sc);  
       
        pageRef = controller.init();
        Test.stopTest();
     
    }
    
    @istest static void TestParentCaseDocsPending(){
       Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;  
        System.assert(Acc != null);
        
         NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);  
         
        Case pCas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        pCas.Booking_Unit__c = BU.Id ;
        pCas.Type = 'NOCVisa';
        pCas.Document_Verified__c = true;
        pCas.IsRunningTest__c = true;
        pCas.Total_Snags_Closed__c = 3;
        insert pCas;
        
        Case   Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = BU.Id ;
        Cas.Type = 'NOCVisa';
        Cas.Document_Verified__c = false;
        Cas.IsRunningTest__c = true;
        Cas.Total_Snags_Closed__c = null;
        Cas.ParentId = pCas.Id;
        insert Cas;
        System.assert(Cas != null);
        
        ApexPages.currentPage().getParameters().put('id',Cas.Id);
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,HandoverMQServices.LetterofDischargeResponse_element>();
        HandoverMQServices.LetterofDischargeResponse_element response1 = new HandoverMQServices.LetterofDischargeResponse_element();
        response1.return_x =    '{'+
        ' "data" : ['+
        ' {'+
        '   "P_PARAM_ID" : "123",'+
        '   "P_PROC_STATUS" : "S",'+
        '   "P_PROC_MESSAGE" : "Message",'+
        '   "URL" : "www.google.com",'+
        '   "P_ATTRIBUTE2" : "123",'+
        '   "P_ATTRIBUTE3" : "321"'+
        '   '+
        '   '+
        '    '+
        ' }'+
        ' ],'+
        '  '+
        '  "message":"Message",'+
        '  "status":"S"'+
        '  '+
        '}';
       
       
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
 
        PageReference pageRef = Page.GeneratePCC;
        pageRef.getParameters().put('id', String.valueOf(Cas.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(Cas);
        GenerateLOD controller = new GenerateLOD(sc);  
       
        pageRef = controller.init();
        Test.stopTest();
     
    }
    
    @istest static void TestChildCaseDocsPending(){
       Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;  
        System.assert(Acc != null);
        
         NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);  
     
        Case Cas1 = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas1.Booking_Unit__c = BU.Id ;
        Cas1.Type = 'NOCVisa';
        Cas1.Document_Verified__c = false;
        Cas1.IsRunningTest__c = true;
        Cas1.Total_Snags_Closed__c = null;
        insert Cas1;
        
        Case   Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = BU.Id ;
        Cas.Type = 'NOCVisa';
        Cas.Document_Verified__c = false;
        Cas.IsRunningTest__c = true;
        Cas.Total_Snags_Closed__c = null;
        Cas.ParentId = Cas1.Id;
        insert Cas;
        System.assert(Cas != null);
        
        ApexPages.currentPage().getParameters().put('id',Cas.Id);
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,HandoverMQServices.LetterofDischargeResponse_element>();
        HandoverMQServices.LetterofDischargeResponse_element response1 = new HandoverMQServices.LetterofDischargeResponse_element();
        response1.return_x =    '{'+
        ' "data" : ['+
        ' {'+
        '   "P_PARAM_ID" : "123",'+
        '   "P_PROC_STATUS" : "S",'+
        '   "P_PROC_MESSAGE" : "Message",'+
        '   "URL" : "www.google.com",'+
        '   "P_ATTRIBUTE2" : "123",'+
        '   "P_ATTRIBUTE3" : "321"'+
        '   '+
        '   '+
        '    '+
        ' }'+
        ' ],'+
        '  '+
        '  "message":"Message",'+
        '  "status":"S"'+
        '  '+
        '}';
       
       
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
 
        PageReference pageRef = Page.GeneratePCC;
        pageRef.getParameters().put('id', String.valueOf(Cas.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(Cas);
        GenerateLOD controller = new GenerateLOD(sc);  
       
        pageRef = controller.init();
        Test.stopTest();
    }
    
    @istest static void TestNoSnagging(){
       Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;  
        System.assert(Acc != null);
        
         NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);  
        
        Case pCas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        pCas.Booking_Unit__c = BU.Id ;
        pCas.Type = 'NOCVisa';
        pCas.Document_Verified__c = true;
        pCas.IsRunningTest__c = true;
        pCas.Total_Snags_Closed__c = 3;
        insert pCas;
        
        Case   Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = BU.Id ;
        Cas.Type = 'NOCVisa';
        Cas.ParentId = pCas.Id;
        Cas.Document_Verified__c = true;
        Cas.IsRunningTest__c = true;
        Cas.Total_Snags_Closed__c = null;
        Cas.Total_Snags_Open__c = null;
        Cas.Total_Major_Snags__c = null;
        insert Cas;
        System.assert(Cas != null);
        
        ApexPages.currentPage().getParameters().put('id',Cas.Id);
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,HandoverMQServices.LetterofDischargeResponse_element>();
        HandoverMQServices.LetterofDischargeResponse_element response1 = new HandoverMQServices.LetterofDischargeResponse_element();
        response1.return_x =    '{'+
        ' "data" : ['+
        ' {'+
        '   "P_PARAM_ID" : "123",'+
        '   "P_PROC_STATUS" : "S",'+
        '   "P_PROC_MESSAGE" : "Message",'+
        '   "URL" : "www.google.com",'+
        '   "P_ATTRIBUTE2" : "123",'+
        '   "P_ATTRIBUTE3" : "321"'+
        '   '+
        '   '+
        '    '+
        ' }'+
        ' ],'+
        '  '+
        '  "message":"Message",'+
        '  "status":"S"'+
        '  '+
        '}';
       
       
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
 
        PageReference pageRef = Page.GeneratePCC;
        pageRef.getParameters().put('id', String.valueOf(Cas.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(Cas);
        GenerateLOD controller = new GenerateLOD(sc);  
       
        pageRef = controller.init();
        Test.stopTest();
     
    }
    
    @istest static void TestMajorSnagging(){
       Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;  
        System.assert(Acc != null);
        
         NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);  
     
        Case pCas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        pCas.Booking_Unit__c = BU.Id ;
        pCas.Type = 'NOCVisa';
        pCas.Document_Verified__c = true;
        pCas.IsRunningTest__c = true;
        pCas.Total_Snags_Closed__c = 3;
        insert pCas;
        
        Case   Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = BU.Id ;
        Cas.Type = 'NOCVisa';
        Cas.Document_Verified__c = true;
        Cas.IsRunningTest__c = true;
        Cas.Total_Snags_Closed__c = 3;
        Cas.Total_Snags_Open__c = 1;
        Cas.Total_Major_Snags__c = 1;
        Cas.ParentId = pCas.Id;
        insert Cas;
        System.assert(Cas != null);
        
        ApexPages.currentPage().getParameters().put('id',Cas.Id);
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,HandoverMQServices.LetterofDischargeResponse_element>();
        HandoverMQServices.LetterofDischargeResponse_element response1 = new HandoverMQServices.LetterofDischargeResponse_element();
        response1.return_x =    '{'+
        ' "data" : ['+
        ' {'+
        '   "P_PARAM_ID" : "123",'+
        '   "P_PROC_STATUS" : "S",'+
        '   "P_PROC_MESSAGE" : "Message",'+
        '   "URL" : "www.google.com",'+
        '   "P_ATTRIBUTE2" : "123",'+
        '   "P_ATTRIBUTE3" : "321"'+
        '   '+
        '   '+
        '    '+
        ' }'+
        ' ],'+
        '  '+
        '  "message":"Message",'+
        '  "status":"S"'+
        '  '+
        '}';
       
       
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
 
        PageReference pageRef = Page.GeneratePCC;
        pageRef.getParameters().put('id', String.valueOf(Cas.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(Cas);
        GenerateLOD controller = new GenerateLOD(sc);  
       
        pageRef = controller.init();
        Test.stopTest();
     
    }
    
    @istest static void TestHO(){
       Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;  
        System.assert(Acc != null);
        
         NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);  
     
        Case pCas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        pCas.Booking_Unit__c = BU.Id ;
        pCas.Type = 'NOCVisa';
        pCas.Document_Verified__c = true;
        pCas.IsRunningTest__c = true;
        pCas.Payment_Verified__c = true;
        pCas.Total_Snags_Closed__c = 3;
        insert pCas;
        
        Case   Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = BU.Id ;
        Cas.Type = 'NOCVisa';
        Cas.Document_Verified__c = true;
        Cas.IsRunningTest__c = true;
        Cas.Total_Snags_Closed__c = 3;
        Cas.Total_Snags_Open__c = 1;
        Cas.Total_Major_Snags__c = 1;
        Cas.ParentId = pCas.Id;
        insert Cas;
        System.assert(Cas != null);
        
        ApexPages.currentPage().getParameters().put('id',pCas.Id);
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,HandoverMQServices.LetterofDischargeResponse_element>();
        HandoverMQServices.LetterofDischargeResponse_element response1 = new HandoverMQServices.LetterofDischargeResponse_element();
        response1.return_x =    '{'+
        ' "data" : ['+
        ' {'+
        '   "P_PARAM_ID" : "123",'+
        '   "P_PROC_STATUS" : "S",'+
        '   "P_PROC_MESSAGE" : "Message",'+
        '   "URL" : "www.google.com",'+
        '   "P_ATTRIBUTE2" : "123",'+
        '   "P_ATTRIBUTE3" : "321"'+
        '   '+
        '   '+
        '    '+
        ' }'+
        ' ],'+
        '  '+
        '  "message":"Message",'+
        '  "status":"S"'+
        '  '+
        '}';
       
       
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
 
        PageReference pageRef = Page.GeneratePCC;
        pageRef.getParameters().put('id', String.valueOf(pCas.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(pCas);
        GenerateLOD controller = new GenerateLOD(sc);  
       
        pageRef = controller.init();
        Test.stopTest();
     
    }
    
    @istest static void Test10(){
       Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;  
        System.assert(Acc != null);
        
         NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);  
     
        Case pCas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        pCas.Booking_Unit__c = BU.Id ;
        pCas.Type = 'NOCVisa';
        pCas.Document_Verified__c = true;
        pCas.Payment_Verified__c = true;
        pCas.Total_Snags_Closed__c = 3;
        pCas.IsRunningTest__c = true;
        insert pCas;
        
        Case   Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = BU.Id ;
        Cas.Type = 'NOCVisa';
        Cas.Document_Verified__c = true;
        Cas.IsRunningTest__c = true;
        Cas.Total_Snags_Closed__c = 3;
        Cas.Total_Snags_Open__c = 1;
        Cas.Total_Major_Snags__c = 1;
        Cas.ParentId = pCas.Id;
        insert Cas;
        System.assert(Cas != null);
        
        ApexPages.currentPage().getParameters().put('id',Cas.Id);
        
        Test.startTest();       
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock(2) );
 
        PageReference pageRef = Page.GeneratePCC;
        pageRef.getParameters().put('id', String.valueOf(Cas.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(Cas);
        GenerateLOD controller = new GenerateLOD(sc);  
       
        pageRef = controller.init();
        Test.stopTest();
     
    }
}