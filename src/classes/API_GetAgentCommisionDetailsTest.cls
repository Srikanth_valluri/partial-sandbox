@isTest
public class API_GetAgentCommisionDetailsTest {
    private static Contact adminContact;
    private static User portalUser;
    // private static User portalOnlyAgent;
    private static Account adminAccount;
    private static Contact agentContact;
    private static NSIBPM__Service_Request__c srRecord;
    private static Booking__c bookingRecord;
    private static String BU_ID;
    private static String AgentCommisionID;
    
    private static void testData_setup() {
        Id adminRoleId = [SELECT Id FROM UserRole WHERE DeveloperName = 'Chairman' LIMIT 1].Id;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 't156', email='x1234@email.com',
                emailencodingkey='UTF-8', lastname='User 56', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='x1234@email.com',UserRoleId = adminRoleId);
        
        System.RunAs(adminUser) {
            adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
            insert adminAccount;
            
            adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
            insert adminContact;
            
            agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
            insert agentContact;
            
            portalUser = InitialiseTestData.getPortalUser('test45@test.com', adminContact.Id, 'Admin');
            // portalOnlyAgent = InitialiseTestData.getPortalUser('test54@test.com', agentContact.Id, 'Agent');
            
            srRecord = 
                InitialiseTestData.createBookingServiceRequest(true, true, adminContact.AccountId , System.today());

            bookingRecord = 
                InitialiseTestData.createBookingRecords(adminContact.AccountId, srRecord, 30000000,'Pending');

            Booking_Unit__c bookingUnitObject = new Booking_Unit__c();
            bookingUnitObject.Booking__c = bookingRecord.Id;
            //bookingUnitObject.Inventory__c = bookingRecord.Inventory__c;
            bookingUnitObject.Unit_Selling_Price__c = 15000000.00;
            bookingUnitObject.Requested_Price__c = 10000000.00;
            bookingUnitObject.Requested_Token_Amount__c = 40000.00;
            bookingUnitObject.Payment_Method__c = 'Online_Payment';
            bookingUnitObject.Online_Payment_Party__c = 'Third Party';
            bookingUnitObject.No_of_parking__c = 1.00;
            bookingUnitObject.Primary_Buyer_s_Email__c = 'test@test.com';
            bookingUnitObject.Primary_Buyer_s_Name__c = 'Damac Test';
            bookingUnitObject.Primary_Buyer_Country__c = 'United Arab Emirates';
            bookingUnitObject.Primary_Buyer_s_Nationality__c ='Emirati';
            bookingUnitObject.Unique_Key__c = 'BOOKING_UNIT ' + String.valueOf(System.now().millisecond());
            bookingUnitObject.Registration_ID__c = '1234';
            bookingUnitObject.Registration_DateTime__c = System.NOW();
            insert bookingUnitObject;
        	BU_ID = bookingUnitObject.id;
        
            Agent_Commission__c ac1 = new Agent_Commission__c();
            ac1.Agency__c = adminAccount.id;
            ac1.Booking__c = bookingRecord.id;
            ac1.Booking_Unit__c = BU_ID;
            ac1.Amount__c = 1200.00;
            ac1.DP_Amount__c = 40000.00;
            ac1.DP_Amount_Received__c = 40000.00;
            insert ac1;
            AgentCommisionID = ac1.id;
        }
    }
    
    @isTest 
    static void getCommisionTileDataTest() {
        testData_setup();
        Test.startTest();
        
        RestRequest request = new RestRequest();
        request.requestUri = '/services/apexrest/API_GetAgentCommisionTileData';
        request.httpMethod = 'GET';
        
        RestResponse response = new RestResponse();
		
        RestContext.request = request;
        RestContext.response = response;
        
        System.runAs(portalUser) {
            API_GetAgentCommisionDetails.doGET(); /* 0 */
            
            request.addParameter('commision_id', 'INVALID');
            API_GetAgentCommisionDetails.doGET(); /* 1 */
            
            request.addParameter('commision_id', BU_ID);
            API_GetAgentCommisionDetails.doGET(); /* 1i */
            
            request.addParameter('commision_id', AgentCommisionID);
            API_GetAgentCommisionDetails.doGET(); /* 2 */
        }
        
        User thisUser = [SELECT Id from User where Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Agent_Commission__c ac1 = new Agent_Commission__c();
            ac1.id = AgentCommisionID;
            delete ac1;
        }
        
        System.runAs(portalUser) {
        	API_GetAgentCommisionDetails.doGET(); /* 3 */
        }      
        
        Test.stopTest();
    }
}