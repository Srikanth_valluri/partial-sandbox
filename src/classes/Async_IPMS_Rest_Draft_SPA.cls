/************************************************************************************************
 * @Name              : Async_IPMS_Rest_Draft_SPA
 * @Test Class Name   : IPMS_REST_Generate_Draft_SPA_Test
 * @Description       : Queueable webservie class to fetch draft SPA and creates Unit Documents
 * Modification Log
 * 1.0    QBurst    29/01/2020        Created Class
 ************************************************************************************************/

global class Async_IPMS_Rest_Draft_SPA implements Queueable, Database.AllowsCallouts {

    public string processURL;
    public Booking_Unit__c bookingUnit;
    
    /****************************************************************************************
    * @Description : Constructor - values for processURL and BookingUnit record are updated 
    * @Params      : 1. string IPMSprocessURL - URL to be hit for the webservice            
    *                       call to fetch the SPA url.                                      
    *                2. Booking_Unit__c bookingUnit - Booking Unit for which SPA is fetched
    * @Return      : void                                                                   
    ****************************************************************************************/
    public Async_IPMS_Rest_Draft_SPA(string IPMSprocessURL, Booking_Unit__c bookUnit){
        processURL = IPMSprocessURL;
        bookingUnit = bookUnit;
    }
    
    /****************************************************************************
    * @Description : execute method for Queueable Context                       
    * @Params      : QueueableContext context                                   
    * @Return      : void                                                       
    ****************************************************************************/
    public void execute(QueueableContext context) {        
        pollForSPAAsync(processURL, bookingUnit); // Do Callout
    }
    
    /*****************************************************************************************
    * @Description : Method to contain logic to be executed before insert.                   
    * @Params      : 1. String processURL - URL to be hit for the webservice                 
    *                       call to fetch the SPA url.                                       
    *                2. Booking_Unit__c bookingUnit - Booking Unit for which SPA is fetched  
    * @Return      : void                                                                    
    *****************************************************************************************/
    public void pollForSPAAsync(string processURL, Booking_Unit__c bookingUnit){
        string bookingUnitId = bookingUnit.Id;
        string bookingId = bookingUnit.booking__c;
        String regId = bookingUnit.Registration_id__c;
        try{
            string token = getIPMSAccessToken();
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(processURL);
            req.setMethod('GET');
            req.setTimeout(120000);       
            HttpResponse res = new HttpResponse ();
            req.setHeader('Authorization', 'Bearer ' + token);
            req.setHeader('Content-Type', 'application/json');
            system.debug('request: ' + req);
    
            if (!Test.isRunningTest ()) {
                res = h.send(req);
            } else {
                res.setBody ('{"responseId":"28012020-135646961",'
                        + '"responseTime":"Tue Jan 28 13:56:46 GMT+04:00 2020",'
                        + '"status":"S","responseMessage":"Process Completed","elapsedTimeMs":44729,'
                        + '"responseLines":['
                        +'{"language":"EN","documentName":"SCHEME[SPA]","url":"testurl"},'
                        +'{"language":"EN","documentName":"CAMPAIGN[SPA]","url":"testurl"},'
                        +'{"language":"EN","documentName":"OPTIONS[SPA]","url":"testurl"},'
                        +'{"language":"EN","documentName":"PROMOTION[SPA]","url":"testurl"},'
                        +'{"language":"EN","documentName":"SPA[ABN]","url":"testurl"},'
                        +'{"language":"EN","documentName":"SPA[AGS]","url":"testurl"},'
                        +'{"language":"EN","documentName":"SPA[CRT]","url":"testurl"},'
                        +'{"language":"EN","documentName":"SPA[LAGX]","url":"testurl"},'
                        +'{"language":"EN","documentName":"SPA[HLA]","url":"testurl"},'
                        +'{"language":"EN","documentName":"SPA[SPA]","url":"testurl"},'
                        +'{"language":"EN","documentName":"DP INVOICE[SPA]","url":"testurl"},'
                        +'{"language":"EN","documentName":"DP INVOICE","url":"testurl"},'
                        +'{"language":"EN","documentName":"SPA[AGSX]","url":"testurl"},'
                        +'{"language":"EN","documentName":"OPTIONS","url":"testurl"},'
                        +'{"language":"EN","documentName":"SPA","url":"testurl"},'
                        +'{"language":"EN","documentName":"PROMOTION","url":"testurl"}'
                        +']}');
                res.setStatusCode(200);
            }
            system.debug('response: ' + res);
            system.debug('response body: ' + res.getBody());
            if(res.getStatusCode() != 200){
                createLog(bookingId,res.getBody());
            } else{
                IPMS_GenerateDraftSPA_Response respCls = new IPMS_GenerateDraftSPA_Response();
                String json = res.getBody();
                respCls  = IPMS_GenerateDraftSPA_Response.parse(json);                    
                system.debug('RESPONSE*********' + respCls.responseLines.size()); 
                if(respCls != null){
                    Update_SF(processURL, respCls, bookingUnit);
                }
            }
        } catch(exception  e){
            createLog(bookingId,e.getmessage());
        } 
    }
    
    /************************************************************************************************
    * @Description : Method to Process response and create Unit Documents                           
    * @Params      : 1. string processURL - URL needed for repolling                                
    *                2. IPMS_GenerateDraftSPA_Response resp - response wrapper class object         
    *                3. Booking_Unit__c bookingUnit - booking unit for which Draft SPA is generated
    * @Return      : void                                                                           
    ************************************************************************************************/
    public void Update_SF(string processURL, IPMS_GenerateDraftSPA_Response resp, Booking_Unit__c bookingUnit){
        String bookingId = bookingUnit.booking__c;
        boolean pollAgain = false;    
        boolean hasError = false;  
        if(resp.status == 'C'){ // Completed
            pollAgain = false;
        } else if(resp.status == 'P'){ // In Progress
            pollAgain = true;   
        } else if(resp.status == 'E'){ // Error
            hasError = true;
            createLog(bookingId, string.valueof(resp));
        } 
        
        if(!hasError){   // No Error continue parsing     
            list<Unit_Documents__c> docstoinsert = new list<Unit_Documents__c>();
            Unit_Documents__c doc = new Unit_Documents__c();
            doc.document_type__c = 'SPA Docs';
            doc.Registration_Id__c = bookingUnit.Registration_id__c;
            doc.booking_unit__c = bookingUnit.Id;
            doc.Booking__c = bookingId;
            doc.Service_Request__c = bookingUnit.booking__r.Deal_SR__c;
            doc.Document_Name__c = bookingUnit.Registration_id__c + ' - Draft';
            // Process what has come
            if(resp.responseLines.size() > 0){
                for(integer i=0; i<resp.responseLines.size(); i++){
                    if(resp.responseLines[i].documentName == 'SPA'){
                        doc.SPA__c = resp.responseLines[i].url;
                    } else if(resp.responseLines[i].documentName == 'PROMOTION'){
                        doc.PROMOTION__c = resp.responseLines[i].url;
                    } else if(resp.responseLines[i].documentName == 'DP INVOICE'){
                        doc.DP_INVOICE__c = resp.responseLines[i].url;
                    } else if(resp.responseLines[i].documentName == 'OPTIONS'){
                       doc.OPTION__c = resp.responseLines[i].url;  
                    } else if(resp.responseLines[i].documentName == 'SCHEME' 
                            || resp.responseLines[i].documentName == 'SCHEME-SPA'
                            || resp.responseLines[i].documentName == 'SCHEME[SPA]'){
                       doc.scheme__c = resp.responseLines[i].url;  
                    } else if(resp.responseLines[i].documentName == 'CAMPAIGN-SPA' 
                        || resp.responseLines[i].documentName == 'CAMPAIGN[SPA]'){
                        doc.Campaign_SPA__c = resp.responseLines[i].url;  
                    } else if((resp.responseLines[i].documentName == 'DP INVOICE-SPA'
                            || resp.responseLines[i].documentName == 'DP INVOICE[SPA]') 
                                        && (doc.DP_INVOICE__c == null || doc.DP_INVOICE__c == '')){
                        doc.DP_INVOICE__c = resp.responseLines[i].url;  
                    } else if((resp.responseLines[i].documentName == 'OPTIONS-SPA'
                            || resp.responseLines[i].documentName == 'OPTIONS[SPA]') 
                                    && (doc.OPTION__c == null || doc.OPTION__c == '')){
                        doc.OPTION__c = resp.responseLines[i].url;  
                    } else if((resp.responseLines[i].documentName == 'PROMOTION-SPA' 
                            || resp.responseLines[i].documentName == 'PROMOTION[SPA]')
                                        && (doc.PROMOTION__c == null || doc.PROMOTION__c == '')){
                        doc.PROMOTION__c = resp.responseLines[i].url;  
                    } else if(resp.responseLines[i].documentName == 'SPA-ABN'
                        || resp.responseLines[i].documentName == 'SPA[ABN]'){
                        doc.SPA_ABN__c = resp.responseLines[i].url;  
                    } else if(resp.responseLines[i].documentName == 'SPA-AGS'
                        || resp.responseLines[i].documentName == 'SPA[AGS]'){
                        doc.SPA_AGS__c = resp.responseLines[i].url;  
                    } else if(resp.responseLines[i].documentName == 'SPA-AGSX'
                        || resp.responseLines[i].documentName == 'SPA[AGSX]'){
                        doc.SPA_AGSX__c = resp.responseLines[i].url;  
                    } else if(resp.responseLines[i].documentName == 'SPA-CRT'
                        || resp.responseLines[i].documentName == 'SPA[CRT]'){
                        doc.SPA_CRT__c = resp.responseLines[i].url;  
                    } else if(resp.responseLines[i].documentName == 'SPA[LAGX]'
                        || resp.responseLines[i].documentName == 'SPA-LAGX'){
                        doc.SPA_LAGX__c = resp.responseLines[i].url;  
                    } else if((resp.responseLines[i].documentName == 'SPA-SPA'
                            || resp.responseLines[i].documentName == 'SPA[SPA]') 
                                    && (doc.SPA__c == null || doc.SPA__c == '')){
                        doc.SPA__c = resp.responseLines[i].url;  
                    } else if(resp.responseLines[i].documentName == 'SPA-HLA'
                        || resp.responseLines[i].documentName == 'SPA[HLA]'){
                        doc.SPA_HAL__c = resp.responseLines[i].url;  
                    }
                }
                docstoinsert.add(doc);
                system.debug('Docstoinsert: ' + docstoinsert);
                Booking__c booking = new booking__c(Id=bookingUnit.booking__c);
                try{
                    if( pollAgain && (booking.Poll_for_SPA_Count__c <= 100 
                                            || booking.Poll_for_SPA_Count__c == null)){
                        system.debug('Entered Repolling');
                        booking.Poll_for_SPA__c = true;
                        if(booking.Poll_for_SPA_Count__c != null){
                            booking.Poll_for_SPA_Count__c = booking.Poll_for_SPA_Count__c + 1;
                        } else{
                            booking.Poll_for_SPA_Count__c = 1;
                        }
                        system.enqueueJob(new Async_IPMS_Rest_Draft_SPA(ProcessURL, bookingUnit));
                    } else{
                        system.debug('Entered non repolling');
                        booking.Poll_for_SPA__c = false;
                        booking.Poll_for_SPA_Count__c = 0;
                    } 
                    insert docstoinsert;
                    //upsert docstoinsert Registration_Id__c ;
                    update booking;  
                } catch(exception e){
                    createLog(bookingId, string.valueof(e.getMessage()));
                }         
            } 
        } 
    }

    /****************************************************************************
    * @Description : Method to fetch IPMS Access Token                          
    * @Params      : None                                                       
    * @Return      : String Token                                               
    *****************************************************************************/
    public static string getIPMSAccessToken(){
        system.debug('Fetching Token');
        IPMS_Integration_Settings__mdt settings = [SELECT Client_ID__c, Endpoint_URL__c 
                                                   FROM IPMS_Integration_Settings__mdt 
                                                   WHERE DeveloperName = 'IPMS_Draft_SPA_Token' LIMIT 1];
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(settings.Endpoint_URL__c + '/apex/login?client-id=' + settings.Client_ID__c);
        req.setMethod('POST'); 
        req.setTimeout(120000);       
        HttpResponse res = new HttpResponse ();
        if (!Test.isRunningTest ()) {
            res = h.send(req);
        } else {
            res.setBody ('{"token":"eyJhw", "tokenType":"Bearer", "validTill":"02-Aug-2019 at 16:17:30"}');
        }
        system.debug(res.getBody());
        DAAMC_IPMS_ACCESS_TOKEN_RESP cls =  DAAMC_IPMS_ACCESS_TOKEN_RESP.parse(res.getBody());
        system.debug('Token: ' + cls.token);
        return cls.token;
    }
    
    /****************************************************************************
    * @Description : Method to Create Log record in case of any errors.         
    * @Params      : 1. String parentId - Booking Id                            
    *                2. String errorMessage                                     
    * @Return      : void                                                       
    ****************************************************************************/   
    public void createLog(String parentId, String errorMessage){
        try{
            log__c log = new log__c();
            log.Booking__c = parentId;
            log.Description__c = errorMessage;
            insert log;
        } catch(exception e){
            system.debug('Exception: ' + e);
        }
    }
}