@isTest
private class GenericDataRendererControllerTest {
    @isTest
    static void testController() {

        User communityUser = CommunityTestDataFactory.CreatePortalUser();
        communityUser = [SELECT Id, AccountId FROM User WHERE Id = :communityUser.Id];
        NSIBPM__Service_Request__c dealSr = TestDataFactory_CRM.createServiceRequest();
        insert dealSr;
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(communityUser.AccountId, dealSr.Id, 1);
        insert lstBooking;
        List<Booking_Unit__c> lstUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        insert lstUnit;

        insert new Booking_Unit_Active_Status__c(Name = lstUnit[0].Registration_Status__c);
        System.runAs(communityUser) {
            GenericDataRendererController controller = new GenericDataRendererController();
            controller.methodOne();
            insert new Document(
                Name = 'BookingUnitDisplayConfig',
                Body = Blob.valueOf('{' +
                            '"strategy": "class",' +
                            '"recordLimit": "100",' +
                            '"objectName": "Booking_Unit__c",' +
                            '"fieldset": "",' +
                            '"fieldList": [' +
                              '{' +
                                '"title": "Unit Name",' +
                                '"field": "Unit_Name__c",' +
                                '"type": "link",' +
                                '"onClick": "ccUtils.switchView(\'UnitDetail\', \'{!Id}\')",' +
                                '"referencedFields" : "Id"' +
                              '},' +
                              '{' +
                                '"title" : "Project",' +
                                '"field" : "RERA_Project__c"' +
                              '},{' +
                                '"title": "Unit Type",' +
                                '"field": "Unit_Type__c"' +
                              '},' +
                              '{' +
                                '"title" : "Status",' +
                                '"field" : "Registration_Status__c"' +
                              '},' +
                              '{' +
                                '"title" : "Price",' +
                                '"field" : "Requested_Price__c",' +
                                '"type": "currency"' +
                              '}' +
                            '],' +
                            '"displayAs": "table",' +
                            '"dataProviderClass": "BookingUnitDataProvider",' +
                            '"dataList": [{}],' +
                            '"configJSON": "BookingUnitDisplayConfig"' +
                        '}'),
                    ContentType = '.json',
                    FolderId = UserInfo.getUserId(),
                    IsPublic = true
            );

            DataDisplayConfig config = new DataDisplayConfig();
            config.fieldList = new List<Map<String, String>> {
                new Map<String, String> {
                    'field'=> 'Name',
                    'title'=> 'Name',
                    'type'=> 'link',
                    'onClick'=> 'ccUtils.switchView("UnitDetail", "{!Id}")',
                    DataRendererUtils.REFERENCED_FIELDS => 'Id'
                }
            };
            config.detailFieldList = new List<Map<String, String>> {
                new Map<String, String> {
                    'field'=> 'Name',
                    'title'=> 'Name',
                    DataRendererUtils.REFERENCED_FIELDS => 'LastModifiedById'
                }
            };
            config.recordId = lstUnit[0].Id;
            config.objectName = 'Booking_Unit__c';
            config.strategy = 'class';
            config.configJSON = 'BookingUnitDisplayConfig';
            config.dataProviderClass = 'GenericRecordDetailsDataProvider';

            System.debug(config.configJSON);
            System.debug(config.strategy);
            System.debug(config.dataProviderClass);

            config = GenericDataRendererController.getData(config);

            System.assert(!config.dataList.isEmpty());
        }

    }
}