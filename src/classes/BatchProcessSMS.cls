public without sharing class BatchProcessSMS implements Database.Batchable<SObject>,
                                                    Database.AllowsCallouts,
                                                    Database.Stateful 
{
    public string sQry = '';
    public List<Id> lstSMSHistoryIds = new list<Id>();
    
    public BatchProcessSMS(string str, List<Id> lstIds) {
        lstSMSHistoryIds = lstIds;
        sQry = str;
    }

    public Database.Querylocator start( Database.BatchableContext bc ) {
        
        if(string.isBlank(sQry)){
            sQry = 'SELECT Id, Phone_Number__c,FM_Case__c , Message__c, Is_SMS_Sent__c '
                    + ' From SMS_History__c '
                    + ' WHERE Id In : lstSMSHistoryIds '
                    + ' AND Is_SMS_Sent__c = false';
        }
        
        System.debug('Query = ' + sQry);
        return Database.getQueryLocator(sQry);
    }

  public void execute( Database.BatchableContext bc, list<SMS_History__c> lstSMSToSend ) {
      //system.debug('== lstSMSToSend =='+lstSMSToSend);
      list<SMS_History__c> lstSMSHistory = new list<SMS_History__c>();
    if( lstSMSToSend != null && !lstSMSToSend.isEmpty() ) {
        for( SMS_History__c objSMS : lstSMSToSend ) {
            system.debug('== objSMS =='+objSMS);
            system.debug('== objSMS.Is_SMS_Sent__c =='+objSMS.Is_SMS_Sent__c);
            system.debug('== objSMS.Message__c =='+objSMS.Message__c);
            if( objSMS.Is_SMS_Sent__c == false && String.isNotBlank( objSMS.Message__c ) ) {
                system.debug('== objSMS =='+objSMS);

                String strUserName = '';
                String strPassword = '';
                String strSID = '';
                
                if( Label.Generic_SMS_Control.equalsIgnoreCase('FM') ) {
                  strUserName = Label.FM_SMS_Service_Username;
                  strPassword = Label.FM_SMS_Service_Password;
                  //strSID = Label.FM_SMS_Service_SenderId;
                  strSID = SMSClass.getSenderName(strUserName, objSMS.Phone_Number__c, false);
                }else if( Label.Generic_SMS_Control.equalsIgnoreCase('Collection')) {
                  strUserName = Label.SF_SMS_Service_User_name;
                  strPassword = Label.SF_SMS_Service_Password;
                  //strSID = Label.SF_SMS_Service_Sender_Id;
                  strSID = SMSClass.getSenderName(strUserName, objSMS.Phone_Number__c, false);
                }else if( Label.Generic_SMS_Control.equalsIgnoreCase('CRM') ){
                  strUserName = Label.Damac_CRM_SMS_Service_User_name;
                  strPassword = Label.Damac_CRM_SMS_Service_Password;
                  //strSID = Label.Damac_CRM_SMS_Service_Sender_Id
                  strSID = SMSClass.getSenderName(strUserName, objSMS.Phone_Number__c, false);
                }

                HttpRequest req = new HttpRequest();
                req.setMethod('POST' ); // Method Type
                req.setEndpoint('http://api.smscountry.com/SMSCwebservice_bulk.aspx');

                String PhoneNumber = objSMS.Phone_Number__c;
                if(String.isNotBlank ( PhoneNumber ) ) { 
                    if(PhoneNumber.startsWith('00')) {
                        PhoneNumber = PhoneNumber.removeStart('00');
                    }else if(PhoneNumber.startsWith('0')) {
                        PhoneNumber = PhoneNumber.removeStart('0');
                    }
                    system.debug( ' PhoneNumber : ' + PhoneNumber);
                }
                
                req.setBody('user='+ strUserName + '&passwd=' + strPassword +'&message=' 
                            + GenericUtility.encodeChar(objSMS.Message__c) + '&mobilenumber=' 
                            + PhoneNumber + '&sid='+strSID+ '&MTYPE=LNG&DR=Y'); // Request Parameters
                
                Http http = new Http();
                try {
                    system.debug( ' req : ' + req);
                    HTTPResponse res = http.send( req );
                    system.debug( ' res : ' + res);
                    if( String.isNotBlank( res.getBody() ) ) {
                        system.debug( ' res.getBody() : ' + res.getBody());
                        if(String.valueOf(res.getBody()).contains('OK:')){
                            objSMS.Description__c = res.getBody();
                            objSMS.Is_SMS_Sent__c = true ;
                            objSMS.sms_Id__c = res.getBody().substringAfter('OK:');
                            lstSMSHistory.add(objSMS) ; 
                        }
                        else {
                            objSMS.Description__c = res.getBody();
                            lstSMSHistory.add(objSMS) ;
                        }
                    }
                }catch( Exception e ) {
                    objSMS.Description__c = e.getMessage();
                    lstSMSHistory.add(objSMS) ;
                }
            }
        }
        if(!lstSMSHistory.isEmpty()){
            Database.SaveResult [] updateResult = Database.update(lstSMSHistory, false);            
        }
    }
}

    public void finish( Database.BatchableContext bc ) {
        /*if( lstSMSHistory != null && lstSMSHistory.size() >0) {
            system.debug('!!!!lstSMSHistory'+lstSMSHistory);
            Database.SaveResult [] updateResult = Database.update(lstSMSHistory, false);
        }else {
          return;
        }*/
    }
}