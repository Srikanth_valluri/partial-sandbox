@isTest
private class HDApp_UtilityTest {

	@isTest
	static void testMethod1() {
		//this method covers following methods:
		//getAccountFromAccountId()
		//getUnitsFromBUId()
		//getCaseDetails()
		//getAllUnitsFromAccountId()
		//getDependentPicklistValues()
		//getCaseUploadDocumentWrapper ()
		//extractName ()
		//extractType()

		NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1204',
                                      Registration_ID__c = '3901',
                                      Handover_Flag__c = 'Y',
                                      Dummy_Booking_Unit__c = true);
        insert bookingUnit;

        Case objCase = new Case();
        objCase.AccountId = account.Id;
        objCase.Booking_Unit__c = bookingUnit.id; 
        objCase.RecordTypeId = ComplaintProcessUtility.getRecordTypeId();
        insert objCase;

        Case caseInst = [SELECT id, CaseNumber FROM Case WHERE id =: objCase.id];

        Test.startTest();

            List<Account> accList = HDApp_Utility.getAccountFromAccountId(account.id);
            List<Booking_Unit__c> buList = HDApp_Utility.getUnitsFromBUId(bookingUnit.id);
            List<Case> caseList = HDApp_Utility.getCaseDetails(objCase.id); 
            List<Booking_Unit__c> lstBU = HDApp_Utility.getAllUnitsFromAccountId(account.id);
            Map<String, List<String>> mapPickVal = HDApp_Utility.getDependentPicklistValues(Case.Complaint_Sub_Type__c);
            UploadMultipleDocController.MultipleDocRequest uploadDoc = HDApp_Utility.getCaseUploadDocumentWrapper('Test String', 'Test.jpg', caseInst.CaseNumber);
            List<String> lstString = HDApp_Utility.getPicklistOptions('FM_Case__c','Country__c');
        	List<String> options = HDApp_Utility.getPicklistOptions('Account', 'CurrencyIsoCode');
        	HDApp_Utility.cls_meta_data metaResponseTest = HDApp_Utility.ReturnMetaResponse('Success', 'Success', 1);
        	String guidTest = HDApp_Utility.getGUID();

        Test.stopTest();

	}
}