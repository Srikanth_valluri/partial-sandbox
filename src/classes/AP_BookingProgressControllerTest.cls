@isTest
public class AP_BookingProgressControllerTest {
    public static testMethod void Test_1(){
        BookingSRStatus__c bookingSRStatusObj = new BookingSRStatus__c();
        bookingSRStatusObj.Name = 'Awaiting Token Verification';
        bookingSRStatusObj.Status__c = 'SUBMITTED';
        insert bookingSRStatusObj;
        AP_BookingProgressController progress = new AP_BookingProgressController();
        progress.awaitingTokenPayment = true;
        progress.buyersConfirmation = true;
        progress.documentsVerification = true;
        progress.SPASigned = true;
        String statusCode = 'SUBMITTED';
        progress.SRStatusMapping();
        AP_BookingProgressController.SRStatusMapping1(statusCode);
    }
}