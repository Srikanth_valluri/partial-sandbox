/**************************************************************************************************
* Name               : Update_VAT                                                               *
* Description        : Custom code invoked on manager review step on agent related SR to update VAT*
* Created Date       :                                                                            *
* Created By         : ALOK                                                                    *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR        DATE            COMMENTS                                              *
* 1.0         Alok Chauhan  26/11/2017      Initial Draft.                                        *
                              *
**************************************************************************************************/
global without sharing class Update_VAT implements NSIBPM.CustomCodeExecutable{

    public string vatNumber;
    public date  vatRegDate;
    public string vatBusinessType;
    
    public String EvaluateCustomCode(NSIBPM__Service_Request__c SR, NSIBPM__Step__c step) {
        String retStr = 'Success';
        try {
            
            NSIBPM__Service_Request__c currSR = getSRDetails(step.NSIBPM__SR__c);
            if(currSR.NSIBPM__Customer__c !=null){
             vatRegDate = currSR.VAT_Registration_Certificate_Date__c;
             vatNumber = currSR.UAE_Tax_Registration_Number__c;
             System.Debug('VAT Details++++'+vatRegDate+'#####'+vatNumber);
             List<Agent_Site__c> upSite = [SELECT ID from Agent_Site__c where Agency__c=:currSR.NSIBPM__Customer__c and name='UAE' limit 1];
             System.Debug('SiteFound++++++'+upSite);
             if (upSite.Size()>0){
             upSite[0].Registration_Certificate_Date__c =vatRegDate ;
             upSite[0].Tax_Registration_Number__c =vatNumber;
             update upSite;
             } 
            }
        }
        catch (Exception e) {
            retStr = 'Error :' + e.getMessage() + '';
        }
        return retStr;
    }
    public static NSIBPM__Service_Request__c getSRDetails(Id SRId) {
        NSIBPM__Service_Request__c currSR = new NSIBPM__Service_Request__c();
        DescribeSObjectResult describeResult = NSIBPM__Service_Request__c.getSObjectType().getDescribe();
        List<String> fieldNames = new List<String> ( describeResult.fields.getMap().keySet() );
        String query = ' SELECT ' + String.join( fieldNames, ',' ) + ' FROM ' + describeResult.getName();
        query += ' WHERE Id =: SRId LIMIT 1';
        System.debug('==>' + query);
        currSR = Database.query(query);
        return currSR;
    }

}// End of class.