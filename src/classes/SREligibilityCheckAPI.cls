/**********************************************************************************************************************
Description: This API is used for checking the eligibility of a user for raising an SR
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By      | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   17-11-2020      | Anand Venkitakrishnan | Created with eligibility check for Tenancy Renewal SR Type
1.1     |   18-11-2020      | Shubham Suryawanshi   | Created with eligibility check for Fitout/Alterations SR Type
1.2	    |   20-01-2021      | Anand Venkitakrishnan | Added eligibility check for Work Permit SR Type
***********************************************************************************************************************/

@RestResource(urlMapping='/checkSREligibility/*')
global class SREligibilityCheckAPI {
    public static final String SUCCESS = 'Successful';
    public static String responseMessage;
    public static Integer statusCode;
    public static final String TENANCY_RENEWAL = '6';
    public static final String FITOUT_ALTERATIONS = '8';
    public static final String WORK_PERMIT = '11';
    
    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong',
        7 => 'Service charges overdue!'
    };
    
    /***********************************************************************************************
    Method Name : checkSREligibility
    Description : Returns the Tenancy Renewal SR (FM Case) details for given fmCaseId
    Parameter(s): None
    Return Type : FinalReturnWrapper (Wrapper)
    ************************************************************************************************/
    @HttpGet
    global static FinalReturnWrapper checkSREligibility() {
        
        FinalReturnWrapper retunResponse = new FinalReturnWrapper();
        cls_meta_data objMeta = new cls_meta_data();
        cls_data objData = new cls_data();
        Boolean isEligible;
        
        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);
        
        if(!r.params.containsKey('bookingUnitId')) {
            objMeta.message = 'Missing parameter : bookingUnitId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
            
            retunResponse.meta_data = objMeta;
            return retunResponse;
        }
        else if(r.params.containsKey('bookingUnitId') && String.isBlank(r.params.get('bookingUnitId'))) {
            objMeta.message = 'Missing parameter value: bookingUnitId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
            
            retunResponse.meta_data = objMeta;
            return retunResponse;
        }
        else if(!r.params.containsKey('accountId')) {
            objMeta.message = 'Missing parameter : accountId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
            
            retunResponse.meta_data = objMeta;
            return retunResponse;
        }
        else if(r.params.containsKey('accountId') && String.isBlank(r.params.get('accountId'))) {
            objMeta.message = 'Missing parameter value: accountId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
            
            retunResponse.meta_data = objMeta;
            return retunResponse;
        }
        else if(!r.params.containsKey('srTypeId')) {
            objMeta.message = 'Missing parameter : srTypeId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
            
            retunResponse.meta_data = objMeta;
            return retunResponse;
        }
        else if(r.params.containsKey('srTypeId') && String.isBlank(r.params.get('srTypeId'))) {
            //objMeta.message = 'Missing parameter value: srTypeId';
            //objMeta.status_code = 3;
            //objMeta.title = mapStatusCode.get(3);
            //objMeta.developer_message = null;
            
            //retunResponse.meta_data = objMeta;

            //If srTypeId is blank - byPass eligibility check & return TRUE (As discussed with Charith)
            objMeta.message = SUCCESS;
            objMeta.status_code = 1;
            objMeta.title = mapStatusCode.get(objMeta.status_code);
            objMeta.developer_message = null;
            
            objData.is_eligible_for_sr = true;
            
            retunResponse.data = objData;
            retunResponse.meta_data = objMeta;
            return retunResponse;
        }
        
        List<Booking_Unit__c> unit = [SELECT Id
                                      , Unit_Name__c
                                      , Booking__r.Account__c
                                      , Booking__r.Account__r.Party_Id__c
                                      , Registration_Id__c
                                      , Property_Name__c
                                      , Property_City__c
                                      , Resident__c
                                      , Tenant__c
                                      FROM Booking_Unit__c
                                      WHERE Id = :r.params.get('bookingUnitId')
                                      LIMIT 1];
        
        if(unit.isEmpty() && unit.size() < 1) {
            objMeta.message = 'No booking unit found for given booking_unit_id';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
            
            retunResponse.meta_data = objMeta;
            return retunResponse;
        }
        else {
            Boolean isDuePending = false;
            
            String srTypeId = r.params.get('srTypeId');
            
            //For Tenancy Renewal SR type
            if(srTypeId.equals(TENANCY_RENEWAL)) {
                
                isDuePending = isServiceFeeDuePending(unit[0]);
                
                if(isDuePending) {
                    isEligible = false;
                    responseMessage = 'Please pay your service charges and then proceed with your tenancy renewal';
                    statusCode = 7;
                }
                else {
                    isEligible = true;
                    responseMessage = SUCCESS;
                    statusCode = 1;
                }
            }
            // For Fitout/Alterations SR type
            else if(srTypeId.equals(FITOUT_ALTERATIONS)) {
                
                isDuePending = isServiceFeeDuePendingForNOCFitOut(unit[0]);
                System.debug('isDuePending for Noc fitOut:: ' + isDuePending);
                
                if(isDuePending) {
                    isEligible = false;
                    responseMessage = 'Request for NOC can be submitted only on clearance of Service Charges';
                    statusCode = 7;
                }
                else {
                    isEligible = true;
                    responseMessage = SUCCESS;
                    statusCode = 1;
                }
            }
            // For Work Permit SR type
            else if(srTypeId.equals(WORK_PERMIT)) {
                
                isDuePending = isServiceFeeDuePendingForWorkPermit(unit[0]);
                System.debug('isDuePending for Work Permit:'+isDuePending);
                
                if(isDuePending) {
                    isEligible = false;
                    responseMessage = 'Request for Work Permit can be submitted only on clearance of Service Charges';
                    statusCode = 7;
                }
                else {
                    isEligible = true;
                    responseMessage = SUCCESS;
                    statusCode = 1;
                }
            }
            //If none of the above SR ? return true
            else {
                isEligible = true;
                responseMessage = SUCCESS;
                statusCode = 1;
            }
        }
        
        objMeta.message = responseMessage;
        objMeta.status_code = statusCode;
        objMeta.title = mapStatusCode.get(objMeta.status_code);
        objMeta.developer_message = null;
        
        objData.is_eligible_for_sr = isEligible;
        
        retunResponse.data = objData;
        retunResponse.meta_data = objMeta;
        
        System.debug('retunResponse:'+retunResponse);
        
        return retunResponse;
    }
    
    public static boolean isServiceFeeDuePending(Booking_Unit__c objBU) {
        Boolean isDuePresent;
        //FmIpmsRestServices.DueInvoicesResult dues = FmIpmsRestServices.getDueInvoices(objBU.Registration_Id__c, objBU.Booking__r.Account__r.Party_ID__c, objBU.Property_Name__c);
        FmIpmsRestServices.DueInvoicesResult dues = getDuesFromIPMS(objBU);
        
        //Added the CM_Units, ByPassFMReminder__c, SC_From_RentalIncome__c check - Shubham 11/03/2020
        System.debug('due in label :'+label.ServiceChargeDueLimitInAmenityBooking);
        
        Decimal dueLimit = Decimal.valueOf(label.ServiceChargeDueLimitInAmenityBooking);
        System.debug('dueLimit: ' + dueLimit);
        
        //The CM_Units, ByPassFMReminder__c, SC_From_RentalIncome__c check isremoved as per discussed with Shinu- Shubham 01/10/2020 
        if(!Test.isRunningTest() && (dues.dueAmountExceptCurrentQuarter == null || dues.dueAmountExceptCurrentQuarter < dueLimit) ) {
            isDuePresent = false;
        }
        else {
            isDuePresent = true;
        }
        
        System.debug('isDuePresent :'+isDuePresent);
        return isDuePresent;
    }

    public static Boolean isServiceFeeDuePendingForNOCFitOut(Booking_Unit__c objBU) {

        Decimal totalDue;
        FmIpmsRestServices.DueInvoicesResult dues = getDuesFromIPMS(objBU);
        //System.debug('-->> dues.totalDueAmount.....'+ dues.totalDueAmount);

        if(dues != null && dues.totalDueAmount != null) {
            totalDue = Decimal.valueOf(dues.totalDueAmount);
        }
        else {
            totalDue = 0;
        }
        System.debug('totalDue:: ' + totalDue);

        list<FM_process__mdt> lstfmProcessMetadata = [ SELECT Minimum_Outstanding_Charge__c
                                                         FROM FM_process__mdt
                                                        WHERE DeveloperName = 'NOC_for_Fit_out_Alterations' ];


        Decimal decMinServiceCharge = lstfmProcessMetadata != NULL && !lstfmProcessMetadata.isEmpty() &&
                              lstfmProcessMetadata[0].Minimum_Outstanding_Charge__c != NULL ?
                              lstfmProcessMetadata[0].Minimum_Outstanding_Charge__c : 0 ;
        System.debug('== decMinServiceCharge =='+decMinServiceCharge );

        return totalDue > decMinServiceCharge;
    }
	
    //Callout to IPMS for getting dues for given unit
    public static FmIpmsRestServices.DueInvoicesResult getDuesFromIPMS(Booking_Unit__c objBU) {
        if(Test.isRunningTest()) {
            return null;
        }
        FmIpmsRestServices.DueInvoicesResult dues = FmIpmsRestServices.getDueInvoices(objBU.Registration_Id__c, objBU.Booking__r.Account__r.Party_ID__c, objBU.Property_Name__c);
        System.debug('dues:: ' + dues);    
        return dues;
    }
    
    public static Boolean isServiceFeeDuePendingForWorkPermit(Booking_Unit__c objBU) {
		
        Decimal totalDue;
        FmIpmsRestServices.DueInvoicesResult dues = getDuesFromIPMS(objBU);
		
        if(dues != null && dues.totalDueAmount != null) {
            totalDue = Decimal.valueOf(dues.totalDueAmount);
        }
        else {
            totalDue = 0;
        }
        System.debug('totalDue:'+totalDue);
		
        List<FM_process__mdt> listFMProcessMetadata = [SELECT Minimum_Outstanding_Charge__c 
                                                       FROM FM_process__mdt 
                                                       WHERE DeveloperName = 'Apply_for_Work_Permits'];
		
        Decimal minServiceCharge = listFMProcessMetadata != NULL && !listFMProcessMetadata.isEmpty() &&
                              	   listFMProcessMetadata[0].Minimum_Outstanding_Charge__c != NULL ?
                              	   listFMProcessMetadata[0].Minimum_Outstanding_Charge__c : 0 ;
        System.debug('minServiceCharge:'+minServiceCharge);
		
        return totalDue > minServiceCharge;
    }
    
    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }
    
    public class cls_data {
        public Boolean is_eligible_for_sr;
    }
    
    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message;   
    }
}