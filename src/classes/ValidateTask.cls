public with sharing class ValidateTask {
     public static  void validateFollowUpTask(Task taskObj){
        List<Case> caseLst=[ SELECT Id
                                ,Account.Id
                            From Case
                            WHERE id =: taskObj.whatId
                            LIMIT 1];
        List<Note> noteListForAccount =[SELECT Id,
                                            CreatedDate
                                        FROM Note 
                                        WHERE parentId = : caseLst[0].Account.Id
                                        AND CreatedDate = Today 
                                        ORDER BY CreatedDate DESC Limit 1];

        if( ( noteListForAccount == null ||noteListForAccount.size() < 1)  ){
            taskObj.addError('There is no notes for the day');
        }                                
    }
}