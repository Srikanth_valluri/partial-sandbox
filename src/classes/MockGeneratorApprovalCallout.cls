@isTest
global class MockGeneratorApprovalCallout implements HttpCalloutMock{
    
    public static final string SAMPLE_RESPONSE=  '{"OutputParameters": {"@xmlns": "http://xmlns.oracle.com/apps/ont/rest/XXDC_POP_RECEIPTS/get_unidentified_receipt/","@xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance","X_RECEIPTS_DTL_TAB": {"X_RECEIPTS_DTL_TAB_ITEM": [{"DOC_SEQUENCE_VALUE": "118282051","RECEIPT_NUMBER": "DIRECT CHEQUE DEPOSIT# 000023","RECEIPT_AMOUNT": "2500","RECEIPT_DATE": "2016-04-09T00:00:00.000+04:00","REMARKS": "DIRECT CHEQUE DEPOSIT# 000023","BANK_REF_NUMBER": "000023","BANK_ACCOUNT_NUM": "10012387020001","CASH_RECEIPT_ID": "999649"},{"DOC_SEQUENCE_VALUE": "118284615","RECEIPT_NUMBER": "CASH DEPOSIT.","RECEIPT_AMOUNT": "2500","RECEIPT_DATE": "2016-04-25T00:00:00.000+04:00","REMARKS": null,"BANK_REF_NUMBER": "NOREF","BANK_ACCOUNT_NUM": "10012387020001","CASH_RECEIPT_ID": "1005381"}]},"X_RETURN_STATUS": "S","X_RETURN_MESSAGE": "Successfully Fetched Records : 5"}}';
    
    global HTTPResponse respond(HTTPRequest req) {
       
        System.assertEquals('http://83.111.194.181:8033/webservices/rest/XXDC_POP_RECEIPTS/get_unidentified_receipt/', req.getEndpoint());
        
        req.setMethod('POST');
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(SAMPLE_RESPONSE);
        res.setStatusCode(200);
        return res;
    }
}