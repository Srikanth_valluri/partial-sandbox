/*
    Author        : Durga Prasad   
    Description   : Custom Code to check the SPA and Sales Audit Review is open or not
*/
global without sharing class CC_CheckStepExists implements NSIBPM.CustomCodeExecutable {
    public string EvaluateCustomCode(NSIBPM__Service_Request__c SR,NSIBPM__Step__c step){
        string StepExists = 'False';
        integer i = 0;
        if(step!=null && step.NSIBPM__SR__c!=null){
            for(NSIBPM__Step__c stp:[Select Id from NSIBPM__Step__c where NSIBPM__SR__c=:step.NSIBPM__SR__c and (NSIBPM__SR_Step__r.NSIBPM__Step_Template__r.NSIBPM__Code__c='SPA_EXECUTION' or NSIBPM__SR_Step__r.NSIBPM__Step_Template__r.NSIBPM__Code__c='SALES_AUDIT') and Is_Closed__c=false]){
                i = i+1;
            }
        }
        if(i>0)
            StepExists = 'True';
        return StepExists;
    }
}