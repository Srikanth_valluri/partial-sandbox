/*
Test Class : FmTaskInvocableClassTest
*/
public without sharing class FmTaskInvocableClass{
    @invocableMethod
    public static void invokeTask(list<Task> lstTasks){
        Integer i = 1;
        set<Id> setTaskIds = new set<Id>();
        set<Id> setFmCaseIds = new set<Id>();
        set<String> setTaskForUser = new set<String>();
        
        String keyPrefix = FM_Case__c.sobjecttype.getDescribe().getKeyPrefix();
        system.debug('keyPrefix*****'+keyPrefix);
        if(Label.UsersOutsideSalesforce != null){
            setTaskForUser.addAll(Label.UsersOutsideSalesforce.split(','));
        }
        for(Task objT : lstTasks){
            system.debug('******objT*******'+objT.Id);
            if(objT.WhatId != null && setTaskForUser.contains(objT.Assigned_User__c)
            && string.valueOf(objT.WhatId).left(3).equalsIgnoreCase(keyPrefix)){
                system.debug('Key Prefix matches FM Case object************');
                if(i <= 100){
                    setTaskIds.add(objT.Id);
                    setFmCaseIds.add(objT.WhatId);
                    i++;
                }else{
                    FmPushTaskToIpmsHelper objCls = new FmPushTaskToIpmsHelper();
                    objCls.setTaskIds = new set<Id>(setTaskIds);
                    objCls.setFmCaseIds = new set<Id>(setFmCaseIds);
                    //FmPushTaskToIpmsHelper.createIpmsTask(setTaskIds, setFmCaseIds);
                    System.enqueueJob(objCls);
                    i = 1;
                    setTaskIds = new set<Id>();
                    setFmCaseIds = new set<Id>();
                    setTaskIds.add(objT.Id);
                    setFmCaseIds.add(objT.WhatId);
                }
            }
        } // end of for loop
        if(!setTaskIds.isEmpty() && !setFmCaseIds.isEmpty()){
            FmPushTaskToIpmsHelper objCls = new FmPushTaskToIpmsHelper();
            objCls.setTaskIds = new set<Id>(setTaskIds);
            objCls.setFmCaseIds = new set<Id>(setFmCaseIds);
            //FmPushTaskToIpmsHelper.createIpmsTask(setTaskIds, setFmCaseIds);
            System.enqueueJob(objCls);
        }
    } // end of invokeTask method
} // end of class