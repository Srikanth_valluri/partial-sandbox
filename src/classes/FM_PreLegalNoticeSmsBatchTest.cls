@isTest
public class FM_PreLegalNoticeSmsBatchTest {

    @isTest
    public static void testSms() {
         //Create Customer record
        Id businessRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account objAcc = new Account(Name = 'Test Account',
                                    recordtypeid = businessRecTypeId,
                                    email__c = 'tt@tfdst.com',
                                    Mobile__c = '2323232',
                                    ZBEmailStatus__c = 'Valid');
        insert objAcc;
        
        //Create Property
        Property__c  objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        //Create Inventory 
        Inventory__c  objInventory = TestDataFactory_CRM.createInventory(objProp.Id);
        objInventory.Building_Name__c = 'Test building';
        insert objInventory;
        
        //Create Deal
        NSIBPM__Service_Request__c  objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR;
        
        //Create Bookings
        List<Booking__c> listBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id, 1);
        insert listBookings;
        
        //crearte booking unit
        List<Booking_Unit__c> listBUs =  TestDataFactory_CRM.createBookingUnits(listBookings, 1);
        listBUs[0].Inventory__c = objInventory.Id;
        listBUs[0].Registration_ID__c = 'reg11';
        listBUs[0].Property_Country__c = 'United Arab Emirates';
        listBUs[0].Email__c = '321test@test.com';
        listBUs[0].Building_Name__c = 'OHN';
        listBUs[0].Registration_Status_Code__c= 'MM';
        listBUs[0].Send_FM_Pre_Legal_Notice__c = true;
        listBUs[0].SC_From_RentalIncome__c = false;
        insert listBUs;
        system.debug('listBUs== ' + [SELECT Id, FM_Pre_Legal_Notice_sent_date__c, Send_FM_Pre_Legal_Notice__c,Account_Id__c,
                                    Booking__r.Account__r.Mobile_Phone_Encrypt__pc,Building_Name__c,Unit_Name__c, 
                                    Booking__r.Account__r.Name FROM Booking_Unit__c  ]);
        Test.setMock(HttpCalloutMock.class, new FMServiceChargesHttpMock(2));
        Test.StartTest();
        DataBase.executeBatch(new FM_PreLegalNoticeSmsBatch(), 1);
        Test.StopTest();
    }
}