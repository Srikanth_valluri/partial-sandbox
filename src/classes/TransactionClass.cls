/****************************************************************************************
Description: Class to consume Subvention amount service 
----------------------------------------------------------------------------------------
Version     Date           Author               Description                                 
1.0       11/8/2020    Ruchika Choudhary       Initial Draft
*****************************************************************************************/

global with sharing class TransactionClass{

    
    /* Method Description : Method to call service method 
     * Input Parameters : String, Decimal
     * Return Type : void
     */
    global static void createTransaction(String regId, Decimal amt){
        if (regId != null && amt != null) {
             sendHttpRequest(regId, amt);
        }
    }
    
    
    /* Method Description : Method called from Process Builder.
     * Input Parameters : caseID - List of Ids from Process Builder
     * Return Type : void
     */
    @InvocableMethod
    public static void sendDetailsFromPB(List<Id> caseID){
        /*if (cases != null) {
            createTransaction(cases[0].Id, cases[0].Mortgage_Amount__c);
        }*/
        if(caseID != null && caseID.size() > 0) {
            List<Case> parentCase = [ SELECT ID,Mortgage_Amount__c,
                                        Registration_Id__c
                                        FROM Case
                                       WHERE ID IN :caseID
                                    ];
            if (parentCase[0].Registration_Id__c != null && parentCase[0].Mortgage_Amount__c != null) {
                createTransaction(parentCase[0].Registration_Id__c, parentCase[0].Mortgage_Amount__c);
            }
            
        }
    }
    /* Method Description : Method to send Htp Request to IPMS
    * Input Parameters : paramId - Registartion Id from Case
    *                    attribute1 - Mortagege amount from Case.
    * Return Type : void
    */
    @future (Callout = true)
    global static void sendHttpRequest(String paramId, Decimal attribute1 ){
        Credentials_Details__c creds = getCredentials();
        String userName = creds.User_Name__c;
        String password = creds.Password__c; 
        String headerValue = 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(userName + ':' + password));

 

        try {
            
            Http http = new Http();
            HttpResponse response = new HttpResponse();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(creds.Endpoint__c);
            
            request.setMethod('POST');
        
            request.setHeader('Accept', 'application/json');
        
            request.setHeader('Content-Type', 'application/json');
        
            request.setHeader('Accept-Language', 'en-US');
            request.setHeader('Authorization', headerValue);
            request.setBody('{"PROCESS_Input":{ "RESTHeader":{ "Responsibility":"ONT_ICP_SUPER_USER", "RespApplication":"ONT", "SecurityGroup":"STANDARD", "NLSLanguage":"AMERICAN" }, "InputParameters":{ "P_REQUEST_NUMBER":"'+paramId+'", "P_SOURCE_SYSTEM":"SFDC", "P_REQUEST_NAME":"MORTGAGE_INT_CM", "P_REQUEST_MESSAGE": { "P_REQUEST_MESSAGE_ITEM": [ {"PARAM_ID":"'+ paramId +'", "ATTRIBUTE1":"'+attribute1+'" } ] } } }}');
            response = http.send(request);
            System.debug('Req Body -----' + request.getBody()); //important
            System.debug('Response -----' + response.getBody()); //important
            System.debug('Response -----' + response.getStatusCode()); //important
        } catch (Exception e) {
            System.debug('Exception --- ' + e);
        }
    }
    
    
    
   /* Method Description : Method to fetch IPMS creds and endpoint from
    *                      custom setting
    * Return Type : void
    */
    Public Static Credentials_Details__c getCredentials() {
    
       List<Credentials_Details__c> lstCreds = [ SELECT
                                                        Id
                                                        , Name
                                                        , User_Name__c
                                                        , Password__c
                                                        , Endpoint__c
                                                    FROM
                                                        Credentials_Details__c
                                                    WHERE
                                                        Name = 'Mortgage Service' ];
        if( lstCreds != null && lstCreds.size() > 0 ) {
            return lstCreds[0];
        } else {
            return null;
        }
    }
}