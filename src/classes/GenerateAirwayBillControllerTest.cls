@isTest
private class GenerateAirwayBillControllerTest {
	
    @isTest
    public static void uploadAirwayBillTest(){
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
        
        Account objAccount = new Account();
        objAccount.Name = 'test';
        insert objAccount;
        
        Case objCase = new Case();
        objCase.Status = 'Submitted';
        insert objCase;
        
        Courier_Delivery__c objAramex = new Courier_Delivery__c();
        objAramex.Courier_Service__c = 'Aramex'; 
        objAramex.Case__c = objCase.Id;
        objAramex.Account__c = objAccount.Id;
        objAramex.Airway_PDF__c = 'http://www.sandbox.aramex.com/content/rpt_cache/6c93658d80e1472a82755e38022ce20f.pdf';
        insert objAramex;
        
        GenerateAirwayBillController objcon = new GenerateAirwayBillController(new ApexPages.StandardController(objAramex));
        Test.setMock(HttpCalloutMock.class, new CourierServiceMock());
        System.Test.startTest();
            objcon.uploadAirwayBill();
        System.Test.stopTest();
    }
}