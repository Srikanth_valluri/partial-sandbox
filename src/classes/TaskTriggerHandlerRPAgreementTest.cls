@isTest
public class TaskTriggerHandlerRPAgreementTest {
    static Account objAcc;
    static Id recTypeRPAgreement;
    static NSIBPM__Service_Request__c objDealSR;
    static list<Booking__c> listCreateBookingForAccount;
    static list<Booking_Unit__c> listCreateBookingUnit;
    
    static void init() {
        objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Mobile_Phone_Encrypt__c = '9674858963';
        objAcc.Mobile_Country_Code__c = 'India: 0091';
        objAcc.Mobile_Encrypt__c = '9674858963';
        insert objAcc ;
        system.debug('objAcc=='+objAcc);

        recTypeRPAgreement = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Agreement').getRecordTypeId();
        system.debug('recTypeRPAgreement=='+recTypeRPAgreement);

        objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        listCreateBookingForAccount = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objDealSR.Id, 1);
        insert listCreateBookingForAccount;

        listCreateBookingUnit = TestDataFactory_CRM.createBookingUnits(listCreateBookingForAccount, 1);
        insert listCreateBookingUnit;
    }

    @isTest static void testTaskCreateIPMSSuccess() {
        init();

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeRPAgreement);
        objCase.AccountId = objAcc.Id;
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        insert objCase;

        Case caseInsertedObj = [select Id, OwnerId from Case where Id =: objCase.Id];

        Task objTask = new Task();
        objTask.WhatId = caseInsertedObj.Id;
        objTask.Subject = 'POA Document Verification Pending';
        objTask.Assigned_User__c = 'Legal';
        objTask.ActivityDate = date.today().addDays(1);
        objTask.OwnerId = caseInsertedObj.OwnerId;
        objTask.Process_Name__c = 'Rental Pool Agreement';
        objTask.Priority = 'High';
        objTask.Status = 'Not Started';
        insert objTask;

        SR_Attachments__c docObj = new SR_Attachments__c();
        docObj.Name = 'POA Document';
        docObj.Case__c = objCase.Id;
        docObj.isValid__c = false;
        insert docObj;
        
        objTask.Status = 'Completed';
        update objTask;
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(1));
        Test.stopTest();
    }
    
    @isTest static void testTaskCreateIPMSBlankResponse() {
        init();

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeRPAgreement);
        objCase.AccountId = objAcc.Id;
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        insert objCase;

        Case caseInsertedObj = [select Id, OwnerId from Case where Id =: objCase.Id];

        Task objTask = new Task();
        objTask.WhatId = caseInsertedObj.Id;
        objTask.Subject = 'POA Document Verification Pending';
        objTask.Assigned_User__c = 'Legal';
        objTask.ActivityDate = date.today().addDays(1);
        objTask.OwnerId = caseInsertedObj.OwnerId;
        objTask.Process_Name__c = 'Rental Pool Agreement';
        objTask.Priority = 'High';
        objTask.Status = 'Not Started';
        insert objTask;

        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(2));
        Test.stopTest();
    }
    
    @isTest static void testTaskCreateIPMSErrorResponse() {
        init();

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeRPAgreement);
        objCase.AccountId = objAcc.Id;
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        insert objCase;

        Case caseInsertedObj = [select Id, OwnerId from Case where Id =: objCase.Id];

        Task objTask = new Task();
        objTask.WhatId = caseInsertedObj.Id;
        objTask.Subject = 'POA Document Verification Pending';
        objTask.Assigned_User__c = 'Legal';
        objTask.ActivityDate = date.today().addDays(1);
        objTask.OwnerId = caseInsertedObj.OwnerId;
        objTask.Process_Name__c = 'Rental Pool Agreement';
        objTask.Priority = 'High';
        objTask.Status = 'Not Started';
        insert objTask;

        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(3));
        Test.stopTest();
    }
}