@isTest
private class ViolationNoticeEmailSend_TaskTest {

	@isTest static void test_sendViolationNoticeEmail() {
		Id RecordTypeIdFMCase = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Violation Notice').getRecordTypeId();
		FM_Case__c fmObj = new FM_Case__c();
		fmObj.recordtypeid = RecordTypeIdFMCase;
	    insert fmObj;
		//Create task
		Task taskObj = new Task();
		taskObj.Description = 'Test'; //string
	    taskObj.Status ='Completed';
	    taskObj.Subject = Label.TaskToCreateVNForPM;
	    taskObj.WhatId = fmObj.id; //record id
	    insert taskObj;
		List<Task> lstTasks = new List<Task>();
		lstTasks.add(taskObj);
		Test.startTest();
		ViolationNoticeEmailSend_Task.sendViolationNoticeEmail(lstTasks);
		Test.stopTest();
	}

}