/****************************************************************************************************
* Name               : AssociatePaymentPlanController
* Description        : Controller class for Associate Payment Plan VF Page
* Created Date       : 16/10/2020
* Created By         : QBurst
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst         16/10/2020      Initial Draft
****************************************************************************************************/


public class AssociatePaymentPlantoAddOnsController {
    
    public list<SelectOption> allAddons{get; set;}
    public string selAddOn{get; set;}
    public map<id, Alacarte_AddOn__c> mpAddons{get; set;}
    public string addOnDetails{get; set;}

    /*********************************************************************************************
    * @Description : Constructor method
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public AssociatePaymentPlantoAddOnsController() {
      
        allAddons = new list<SelectOption>();
        allAddons.add(new SelectOption('','---None---'));
        mpAddons = new map<Id, Alacarte_AddOn__c>();
        for(Alacarte_AddOn__c ao: [Select id, name, Bedroom_type__c, Unit_Categorization__c, Furnishing_price__c, Pool_price__c from Alacarte_AddOn__c]){
            allAddons.add(new selectOption(ao.Id, ao.Bedroom_type__c+' - '+ao.Unit_Categorization__c));
            mpAddons.put(ao.Id, ao);
        }

        
    }

    public void fetchAddOnDetails(){
        list<AddOnDetailsWrapper> details = new list<AddOnDetailsWrapper>();
        if(selAddOn != '' && selAddOn != null && mpAddons.containsKey(selAddOn)){
            Alacarte_AddOn__c aa = mpAddons.get(selAddOn);
            list<Payment_Plan_Association__c> associcatedPlans = [Select id from Payment_Plan_Association__c where Alacarte_Addon__c =: aa.Id];
            AddOnDetailsWrapper obj = new AddOnDetailsWrapper();
            obj.bedroomType = aa.Bedroom_type__c;
            obj.unitCategorization = aa.Unit_Categorization__c;
            obj.furnishingPrice = aa.Furnishing_Price__c;
            obj.poolPrice = aa.Pool_Price__c;
            obj.hasPlan = associcatedPlans.isEmpty();
            obj.addOnId = aa.Id;
            obj.addOnName = aa.Name;
            details.add(obj);
        }

        addOnDetails = JSON.serialize(details);
    }

    public class AddOnDetailsWrapper{
        public string bedroomType;
        public string unitCategorization;
        public decimal furnishingPrice;
        public decimal poolPrice;
        public boolean hasPlan;
        public string addOnId;
        public string addOnName;
    }

    
}