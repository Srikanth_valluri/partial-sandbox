@isTest
public class ReleaseInventoryBatch_Test {

    public static testmethod void unitTest()
    {
        ReleaseInventoryBatch batch=new ReleaseInventoryBatch();
      	List<Campaign__c> campaingList=  TestDataFactory.createCampaignRecords(new List<Campaign__c> { new Campaign__c() });
        Inventory__c inventiry=new Inventory__c();
        insert inventiry;
    	
        Campaign_Inventory__c cmpInv=new Campaign_Inventory__c();
        cmpInv.End_Date__c=system.today()-3;  
        cmpInv.Campaign__c=campaingList[0].id;
        cmpInv.Inventory__c=inventiry.id;
        
        insert cmpInv;
        
        Database.executeBatch(batch);
    }
  
    public static testmethod void UnitTest2()
    {
        ReleaseInventories scheduler=new ReleaseInventories();
        String sch = '0 0 0 * * ?';
        system.schedule('Test check', sch, scheduler);
    }
    
    public static testmethod void testcase3()
    {
        SelectWrapper obj=new SelectWrapper(new account());
        
    }
}