@RestResource(urlMapping='/SendCaseDetailToMObileApp/*')
 Global class SendCaseDetailToMObileApp
 {
     @HtTPPost
    Global static case SendCaseDetailToMObilesApp(String caseNo)
    {
         Case c=[Select Id,CaseNumber,Status,Type,Unit_Name__c,Contact_Email__c,Contact_Mobile__c,City__c,Country__c,Country_Arabic__c,City_Arabic__c,State_Arabic__c,Address_Arabic_1__c
,Address_Arabic_2__c,Address_Arabic_3__c,Address_Arabic_4__c,Address__c,Address_2__c,Address_3__c,Address_4__c,Postal_Code__c,State__c,Passport_Issue_Place__c
,New_CR__c,Passport_File_URL__c,Additional_Doc_File_URL__c,CRF_File_URL__c,OD_File_URL__c,Passport_Issue_Date__c,CreatedDate,QR_Code__c,Payment_Date__c
,Total_Amount__c,Payment_Currency__c,Payment_Mode__c,Payment_Allocation_Details__c,Complaint_Type__c,Complaint_Sub_Type__c,Description, (
                                SELECT  Id,
                                        Booking_Unit__r.Unit_Name__c
                                FROM    SR_Booking_Units__r
                                ),Buyer__r.name,Buyer__r.Last_Name__c,Buyer__r.First_Name__c FROM Case where CaseNumber =:caseNo];
               
               return c;
    }
 }