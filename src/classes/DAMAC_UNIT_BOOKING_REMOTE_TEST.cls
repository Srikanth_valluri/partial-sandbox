@isTest
public class DAMAC_UNIT_BOOKING_REMOTE_TEST {
    static testMethod void DAMAC_UNIT_BOOKING_REMOTE_TEST_M1 () {
        Account acc = new Account ();
        acc.Agency_Type__c = 'Corporate';
        acc.LastName = 'Test Account';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual Agency').getRecordTypeId();
        acc.Blacklisted__c = false;
        acc.Terminated__c = false;
        acc.Vendor_ID__c = '999988887777';
        insert acc;

        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);        
        sr.Eligible_to_Sell_in_Dubai__c = true;
        sr.Agency_Type__c = 'Individual';
        sr.ID_Type__c = 'Passport';        
        sr.RecordTypeId 
            = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Change Agent').getRecordTypeId();
        sr.Booking_Wizard_Level__c = null;
        sr.Agency_Email_2__c = 'test2@gmail.com';
        sr.Alternate_Agency_Email_2__c = 'test1@gmail.com';
        sr.Country_of_Sale__c = 'UAE';
        sr.Mode_of_Payment__c = 'Cash';
        sr.agency__c = acc.id;
        insert sr;
        
        Agency_PC__c agencyPC = new Agency_PC__c();
        agencyPC.user__c = userinfo.getUserId();
        agencyPC.Agency__c = acc.id;
        insert agencyPC;
        Attachment att = new Attachment();
        att.ParentID = sr.id;
        att.Name = 'test';
        att.contentType = 'jpg';
        att.Body = Blob.valueOf('test.jpg');
        insert att;
        
        DAMAC_UNIT_BOOKING_REMOTE.updateFileName (sr.id, 'test');
        DAMAC_UNIT_BOOKING_REMOTE.updatePassport (sr.id, 'test',null);
        DAMAC_UNIT_BOOKING_REMOTE.updateReservationFormName (sr.id, 'test');
        DAMAC_UNIT_BOOKING_REMOTE.getAgencyDetails ('test');        
        DAMAC_UNIT_BOOKING_REMOTE.getCorporateAgents (acc.id);
        DAMAC_UNIT_BOOKING_REMOTE.getInquiryDetails ('test');
        DAMAC_UNIT_BOOKING_REMOTE.getAccountDetails ('test', 'Individual');
        DAMAC_UNIT_BOOKING_REMOTE.getAccountDetails ('test', 'Individual');
    }
}