/*
* Description - Test class developed for 'AOPTMQService'
*
* Version            Date            Author            Description
* 1.0                21/11/17        Hardik            Initial Draft
*/
@isTest
private class AOPTMQServiceTest {
	static testMethod void mileStoneEventsTestMethod1() {
        
        /*test.StartTest();
		SOAPCalloutServiceMock.returnToMe = new Map<String,AOPTMQClass.PaymentPlanCreationResponse_element>();
        AOPTMQClass.PaymentPlanCreationResponse_element response_x =  new AOPTMQClass.PaymentPlanCreationResponse_element();
        response_x.return_x = '{"message":"Payment Plan is populated","status":"S"}';
           
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
		MileStoneEventsWrapper.MileStoneEvents objMileStone = AOPTMQService.getMasterMilestone('74712');
		test.StopTest();*/
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );
        MileStoneEventsWrapper.MileStoneEvents objMileStone = AOPTMQService.getMasterMilestone('74712');

    }

    static testMethod void mileStoneEventsTestMethod2() {
        test.StartTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,AOPTMQClass.PaymentPlanCreationResponse_element>();
        AOPTMQClass.PaymentPlanCreationResponse_element response_x =  new AOPTMQClass.PaymentPlanCreationResponse_element();
        response_x.return_x = '{"message":"Payment Plan is populated","status":"S"}';
           
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
        MileStoneEventsWrapper.MileStoneEvents objMileStone = AOPTMQService.getMasterMilestone('Test MileStone');
        test.StopTest();
        
    }

    static testMethod void mileStoneOnPaymentTestMethod() {
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );
        MileStonePaymentDetailsWrapper.MileStonePaymentDetails objMileStoneOnPayment = AOPTMQService.getMilestonePaymentDetails('74712');
    }

    static testMethod void checkAOPTEligibilityAllowed() {

        //Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );
        //AOPTMQService.AOPTRuleResponse objAOPTRuleResponse = AOPTMQService.checkAOPTEligibility('','','','','','','','','','','','','','','','','','','','','');

        test.StartTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,AOPTRule.AoptDetailsResponse_element>();
        AOPTRule.AoptDetailsResponse_element response_x =  new AOPTRule.AoptDetailsResponse_element();
        response_x.return_x = '{"allowed":"Yes","message":null,"recommendingAuthorityOne":"One","recommendingAuthorityTwo":"Two","recommendingAuthorityThree":"Three","recommendingAuthorityFour":"Four","approvingAuthorityOne":"One","approvingAuthorityTwo":"Two","approvingAuthorityThree":"Three","percToBeRefundedTransferred":null,"deductionFeePsf":null,"deductionFeeFlat":null,"deductionFeePercentage":null}';
           
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
        AOPTMQService.AOPTRuleResponse objAOPTRuleResponse = AOPTMQService.checkAOPTEligibility('','','','','','','','','','','','','','','','','','','','','');
        test.StopTest();
    }

    static testMethod void checkAOPTEligibilityNotAllowed() {

        //Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );
        //AOPTMQService.AOPTRuleResponse objAOPTRuleResponse = AOPTMQService.checkAOPTEligibility('','','','','','','','','','','','','','','','','','','','','');

        test.StartTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,AOPTRule.AoptDetailsResponse_element>();
        AOPTRule.AoptDetailsResponse_element response_x =  new AOPTRule.AoptDetailsResponse_element();
        response_x.return_x = '{"allowed":"No","message":null,"recommendingAuthorityOne":null,"recommendingAuthorityTwo":null,"recommendingAuthorityThree":null,"recommendingAuthorityFour":null,"approvingAuthorityOne":null,"approvingAuthorityTwo":null,"approvingAuthorityThree":null,"percToBeRefundedTransferred":null,"deductionFeePsf":null,"deductionFeeFlat":null,"deductionFeePercentage":null}';
           
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
        AOPTMQService.AOPTRuleResponse objAOPTRuleResponse = AOPTMQService.checkAOPTEligibility('','','','','','','','','','','','','','','','','','','','','');
        test.StopTest();
    }

    static testMethod void checkAOPTEligibilityAllowed1() {

        //Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );
        //AOPTMQService.AOPTRuleResponse objAOPTRuleResponse = AOPTMQService.checkAOPTEligibility('','','','','','','','','','','','','','','','','','','','','');

        test.StartTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,AOPTRule.AoptDetailsResponse_element>();
        AOPTRule.AoptDetailsResponse_element response_x =  new AOPTRule.AoptDetailsResponse_element();
        response_x.return_x = '{"allowed":"Yes","message":null,"recommendingAuthorityOne":null,"recommendingAuthorityTwo":null,"recommendingAuthorityThree":null,"recommendingAuthorityFour":null,"approvingAuthorityOne":null,"approvingAuthorityTwo":null,"approvingAuthorityThree":null,"percToBeRefundedTransferred":null,"deductionFeePsf":null,"deductionFeeFlat":null,"deductionFeePercentage":null}';
           
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
        AOPTMQService.AOPTRuleResponse objAOPTRuleResponse = AOPTMQService.checkAOPTEligibility('','','','','','','','','','','','','','','','','','','','','');
        test.StopTest();
    }

    static testMethod void checkAOPTEligibility1() {

        //Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );
        //AOPTMQService.AOPTRuleResponse objAOPTRuleResponse = AOPTMQService.checkAOPTEligibility('','','','','','','','','','','','','','','','','','','','','');

        test.StartTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,AOPTRule.AoptDetailsResponse_element>();
        AOPTRule.AoptDetailsResponse_element response_x =  new AOPTRule.AoptDetailsResponse_element();
        response_x.return_x = '';
           
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
        AOPTMQService.AOPTRuleResponse objAOPTRuleResponse = AOPTMQService.checkAOPTEligibility('','','','','','','','','','','','','','','','','','','','','');
        test.StopTest();
    }

    static testMethod void createPaymentPlanIPMSTestMethod() {
        //insert Account
	    Account objAccount = TestDataFactory_CRM.createPersonAccount();
	    objAccount.party_ID__C = '12345';
	    insert objAccount;

	    Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
    	Id aoptRecordTypeID = caseRecordTypes.get('AOPT').getRecordTypeId();

	    Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , aoptRecordTypeID);
	    objCase.Status = 'Submitted';
	    insert objCase;

	    List<paymentPlanCreationXxdcAoptPkgWsP.APPSXXDC_AOPT_PKG_WSX1843128X6X5> lstPaymentTermInIPMS = 
	    		new List<paymentPlanCreationXxdcAoptPkgWsP.APPSXXDC_AOPT_PKG_WSX1843128X6X5>();

	    paymentPlanCreationXxdcAoptPkgWsP.APPSXXDC_AOPT_PKG_WSX1843128X6X5 objWrapper = new paymentPlanCreationXxdcAoptPkgWsP.APPSXXDC_AOPT_PKG_WSX1843128X6X5();
	    objWrapper.DESCRIPTION ='Test DESCRIPTION';
	    objWrapper.EXPECTED_DATE='1/1/2018';
        objWrapper.INSTALLMENT='1';
        objWrapper.LINE_ID='123';
        objWrapper.MILESTONE_EVENT='test EVENT';
        objWrapper.PAYMENT_AMOUNT='12132';
        objWrapper.PAYMENT_DATE='1/1/2018';
        objWrapper.PERCENT_VALUE='10';
        objWrapper.REGISTRATION_ID='74712';
        objWrapper.STATUS='';
        objWrapper.TERM_ID='';
        objWrapper.TRANSFER_AR_INTER_FLAG='';	
        lstPaymentTermInIPMS.add(objWrapper);

        test.StartTest();
		SOAPCalloutServiceMock.returnToMe = new Map<String,AOPTMQClass.PaymentPlanCreationResponse_element>();
		AOPTMQClass.PaymentPlanCreationResponse_element response_x =  new AOPTMQClass.PaymentPlanCreationResponse_element();
	    response_x.return_x = '{"message":"Payment Plan is populated","status":"S"}';
		   
		SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
		
		Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
		String paymentResp = AOPTMQService.createPaymentPlanIPMS('74712',objCase.caseNumber,'AOPT',lstPaymentTermInIPMS);
		test.StopTest();

    }

    static testMethod void generateOfferAccetanceLetterTestMethod2() {
        AOPTDocumentGenerationXsd.DocGenDTO objDocGenDTO = new AOPTDocumentGenerationXsd.DocGenDTO();
        objDocGenDTO.ATTRIBUTE1='';
        objDocGenDTO.regId= '';
        objDocGenDTO.ATTRIBUTE10= '';
        objDocGenDTO.ATTRIBUTE100= '';
        objDocGenDTO.ATTRIBUTE11= '';
        objDocGenDTO.ATTRIBUTE12= '';
        objDocGenDTO.ATTRIBUTE13= '';
        objDocGenDTO.ATTRIBUTE14= '';
        objDocGenDTO.ATTRIBUTE15= '';
        objDocGenDTO.ATTRIBUTE16= '';
        objDocGenDTO.ATTRIBUTE17= '';
        objDocGenDTO.ATTRIBUTE18= '';
        objDocGenDTO.ATTRIBUTE19= '';
        objDocGenDTO.ATTRIBUTE2= '';
        objDocGenDTO.ATTRIBUTE20= '';
        objDocGenDTO.ATTRIBUTE21= '';
        objDocGenDTO.ATTRIBUTE22= '';
        objDocGenDTO.ATTRIBUTE23= '';
        objDocGenDTO.ATTRIBUTE24= '';
        objDocGenDTO.ATTRIBUTE25= '';
        objDocGenDTO.ATTRIBUTE26= '';
        objDocGenDTO.ATTRIBUTE27= '';
        objDocGenDTO.ATTRIBUTE28= '';
        objDocGenDTO.ATTRIBUTE29= '';
        objDocGenDTO.ATTRIBUTE3= '';
        objDocGenDTO.ATTRIBUTE30= '';
        objDocGenDTO.ATTRIBUTE31= '';
        objDocGenDTO.ATTRIBUTE32= '';
        objDocGenDTO.ATTRIBUTE33= '';
        objDocGenDTO.ATTRIBUTE34= '';
        objDocGenDTO.ATTRIBUTE35= '';
        objDocGenDTO.ATTRIBUTE36= '';
        objDocGenDTO.ATTRIBUTE37= '';
        objDocGenDTO.ATTRIBUTE38= '';
        objDocGenDTO.ATTRIBUTE39= '';
        objDocGenDTO.ATTRIBUTE4= '';
        objDocGenDTO.ATTRIBUTE40= '';
        objDocGenDTO.ATTRIBUTE41= '';
        objDocGenDTO.ATTRIBUTE42= '';
        objDocGenDTO.ATTRIBUTE43= '';
        objDocGenDTO.ATTRIBUTE44= '';
        objDocGenDTO.ATTRIBUTE45= '';
        objDocGenDTO.ATTRIBUTE46= '';
        objDocGenDTO.ATTRIBUTE47= '';
        objDocGenDTO.ATTRIBUTE48= '';
        objDocGenDTO.ATTRIBUTE49= '';
        objDocGenDTO.ATTRIBUTE5= '';
        objDocGenDTO.ATTRIBUTE50= '';
        objDocGenDTO.ATTRIBUTE51= '';
        objDocGenDTO.ATTRIBUTE52= '';
        objDocGenDTO.ATTRIBUTE53= '';
        objDocGenDTO.ATTRIBUTE54= '';
        objDocGenDTO.ATTRIBUTE55= '';
        objDocGenDTO.ATTRIBUTE56= '';
        objDocGenDTO.ATTRIBUTE57= '';
        objDocGenDTO.ATTRIBUTE58= '';
        objDocGenDTO.ATTRIBUTE59= '';
        objDocGenDTO.ATTRIBUTE6= '';
        objDocGenDTO.ATTRIBUTE60= '';
        objDocGenDTO.ATTRIBUTE61= '';
        objDocGenDTO.ATTRIBUTE62= '';
        objDocGenDTO.ATTRIBUTE63= '';
        objDocGenDTO.ATTRIBUTE64= '';
        objDocGenDTO.ATTRIBUTE65= '';
        objDocGenDTO.ATTRIBUTE66= '';
        objDocGenDTO.ATTRIBUTE67= '';
        objDocGenDTO.ATTRIBUTE68= '';
        objDocGenDTO.ATTRIBUTE69= '';
        objDocGenDTO.ATTRIBUTE7= '';
        objDocGenDTO.ATTRIBUTE70= '';
        objDocGenDTO.ATTRIBUTE71= '';
        objDocGenDTO.ATTRIBUTE72= '';
        objDocGenDTO.ATTRIBUTE73= '';
        objDocGenDTO.ATTRIBUTE74= '';
        objDocGenDTO.ATTRIBUTE75= '';
        objDocGenDTO.ATTRIBUTE76= '';
        objDocGenDTO.ATTRIBUTE77= '';
        objDocGenDTO.ATTRIBUTE78= '';
        objDocGenDTO.ATTRIBUTE79= '';
        objDocGenDTO.ATTRIBUTE8= '';
        objDocGenDTO.ATTRIBUTE80= '';
        objDocGenDTO.ATTRIBUTE81= '';
        objDocGenDTO.ATTRIBUTE82= '';
        objDocGenDTO.ATTRIBUTE83= '';
        objDocGenDTO.ATTRIBUTE84= '';
        objDocGenDTO.ATTRIBUTE85= '';
        objDocGenDTO.ATTRIBUTE86= '';
        objDocGenDTO.ATTRIBUTE87= '';
        objDocGenDTO.ATTRIBUTE88= '';
        objDocGenDTO.ATTRIBUTE89= '';
        objDocGenDTO.ATTRIBUTE9= '';
        objDocGenDTO.ATTRIBUTE90= '';
        objDocGenDTO.ATTRIBUTE91= '';
        objDocGenDTO.ATTRIBUTE92= '';
        objDocGenDTO.ATTRIBUTE93= '';
        objDocGenDTO.ATTRIBUTE94= '';
        objDocGenDTO.ATTRIBUTE95= '';
        objDocGenDTO.ATTRIBUTE96= '';
        objDocGenDTO.ATTRIBUTE97= '';
        objDocGenDTO.ATTRIBUTE98= '';
        objDocGenDTO.ATTRIBUTE99= '';


        test.StartTest();
		SOAPCalloutServiceMock.returnToMe = new Map<String,AOPTDocumentGeneration.DocGenerationResponse_element>();
		AOPTDocumentGeneration.DocGenerationResponse_element response_x =  new AOPTDocumentGeneration.DocGenerationResponse_element();
	    response_x.return_x = 'https://sftest.deeprootsurface.com/docs/t/IPMS-1039780-Offer%20&%20Acceptance%20Letter-signed.pdf';
		   
		SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
		
		Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
		String respo = AOPTMQService.generateOfferAccetanceLetter(objDocGenDTO);
		test.StopTest();
        
    }


}