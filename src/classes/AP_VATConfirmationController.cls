public without sharing class AP_VATConfirmationController {
    public String BASE_URL = Site.getBaseSecureUrl();
    public string strSRID;
    public string validate {get;set;}
    public NSIBPM__Service_Request__c objSR{get; set;}
    public String vatRegDate               {get; set;}
    public string currentVATregDate {get; set;}
    public String redirectToPageName {get; set;}

    public AP_VATConfirmationController() {
        objSR = new NSIBPM__Service_Request__c();
        if(apexpages.currentPage().getParameters().get('Id') != null) {
            strSRID = apexpages.currentPage().getParameters().get('Id');
        }
        NSIBPM__Service_Request__c currentSR;
        system.debug('strSRID'+strSRID);
        if(string.isnotblank(strSRID)){
            currentSR = [SELECT Id,Agency_Type__c,Agency_Name__c,Eligible_to_Sell_in_Dubai__c,
                                First_Name__c,Last_Name__c,Designation__c,
                                ID_Type__c,ID_Number__c,ID_Issue_Date__c,
                                ID_Expiry_Date__c,Nationality__c,
                                Regeneration_Sites__c,Country_of_Sale__c,
                                Agency_Name_Arabic__c,Corporate_Agency__c,
                                Agency_Short_Name__c,Trade_License_Number__c,NSIBPM__finalizeAmendmentFlg__c,
                                Date_of_Incorporation__c,VAT_Registration_Certificate_Date__c,
                                Country_Of_Incorporation_New__c,Trade_License_Expiry_Date__c,City_Of_Incorporation_New__c,Agency_Email__c,
                                Alternate_Agency_Email__c,UAE_Tax_Registration_Number__c,   
                                Agent_Registration_Type__c,Witness_Address__c,Witness_Name__c,Primary_Language__c 
                        FROM NSIBPM__Service_Request__c 
                        WHERE id =:strSRID];    
            system.debug('currentSR'+currentSR);
            objSR = currentSR;
            if(currentSR != NULL) {
                DateTime doi = currentSR.VAT_Registration_Certificate_Date__c;          
                system.debug('doi'+doi);
                if(doi!= NULL){
                    currentVATregDate = getDate(doi);
                }
                system.debug('currentVATregDate'+currentVATregDate);
            }
        }
    }   

    /*
    * @return: Get date in desired format
    */
    public string getDate(DateTime dt){
        system.debug('dt'+dt);
        Date myDateVar = date.newinstance(dt.year(), dt.month(), dt.day());
        system.debug('myDate1'+myDateVar);
        string strVar = string.valueof(myDateVar);
        system.debug('strVar'+strVar);
        strVar= strVar.substring(0,10);
        system.debug('strVar'+strVar);
        string str1 = strVar.substring(5,7);
        system.debug('str1'+str1);
        string str2 = strVar.substring(8,10);
        system.debug('str2'+str2);
        string str3 = strVar.substring(0,4);
        system.debug('str3'+str3);
        strVar=str1+'/'+str2+'/'+str3;
        system.debug('stld'+strVar);
        return strVar;
    }

    /*
    * @return: Redirect to Next section
    */
    public pageReference submitDetails(){
        system.debug('objSR'+objSR); 

        if(String.isnotBlank(vatRegDate)){
            String[] formatdate = vatRegDate.split('/');
            Date formatissueDate = Date.newInstance(Integer.valueof(formatdate[2].trim()),Integer.valueof(formatdate[0].trim()),Integer.valueof(formatdate[1].trim()));
            objSR.VAT_Registration_Certificate_Date__c = formatissueDate;
        }

        try {     
            upsert objSR;
        } catch(exception e) {          
            validate = e.getdmlMessage(0);
            system.debug('validate'+validate);
            return null;
        }
        system.debug('objSR----------------'+objSR);
        
        PageReference pg;
        if(String.isNotBlank(redirectToPageName)){
            pg = new PageReference(BASE_URL +'/'+redirectToPageName);
            
        }else{
             pg = Page.AP_AddressInformation;
        }
        pg.getParameters().put('id',objSR.id);
        pg.setRedirect(true);
        return pg; 
    }

    /*
    * @return: Redirect to previous section
    */
    public pageReference goToPrevious(){
        
        system.debug('objSR----------------'+objSR);
        PageReference pg = Page.AP_RegBasicInformationPage;
        pg.getParameters().put('id',objSR.id);
        pg.setRedirect(true);
        return pg; 
    }
}