global class CaptureLocationController{
    
    @RemoteAction
    global static void updateCount(decimal lat, decimal lng, string pageName, string address) {
        decimal latitude = lat;
        decimal longitude = lng;
    
        System.debug('>>>>>>>>>>>>>>'+latitude);
        System.debug('>>>>>>>>>>>>>>'+longitude);
        
        String ip;
        string sessionInfo = '';
        
        Map<String,String> currentSessionAttributes = !Test.isRunningTest() ? Auth.SessionManagement.getCurrentSession() : new map<string, string>();
        ip = currentSessionAttributes.containsKey('SourceIp')?currentSessionAttributes.get('SourceIp'):null;
        system.debug('-->'+ip);
        system.debug('>>>>>>>>>>'+currentSessionAttributes);
        for(string s:currentSessionAttributes.keyset()){
            sessionInfo = sessionInfo+'\n'+s+'-'+currentSessionAttributes.get(s);
        }
        system.debug(sessionInfo);
        
        
        list<Page_View__c> pv = new list<Page_View__c>();
        string key = userinfo.getuserid()+'-'+pageName+'-'+system.today().day()+'/'+system.today().Month()+'/'+system.today().year();
        System.debug('>>>>>>key>>>>>>>>'+key);
        pv = [select id,View_Count__c,Unique_Key__c from Page_View__c where Unique_Key__c=:key limit 1];
        System.debug('>>>>>>pv>>>>>>>>>'+pv);
        if(pv.size() == 0){
            Page_View__c p = new Page_View__c();
            p.Unique_Key__c = userinfo.getuserid()+'-'+pageName+'-'+system.today().day()+'/'+system.today().Month()+'/'+system.today().year();
            p.User__c = userinfo.getuserid();
            p.Last_Open_Date_time__c = system.now();
            p.Page_URL__c = pageName;
            p.View_Count__c = 1;    
            p.Log_Date__c = system.today();  
            p.IP_Address__c = ip ;
            p.Session_Info__c = sessionInfo;
            p.Location__latitude__s = latitude;
            p.Location__longitude__s = longitude;
            pv.add(p);  
        }
        else{
            pv[0].Last_Open_Date_time__c = system.now();
            pv[0].View_Count__c = pv[0].View_Count__c+1;  
            pv[0].IP_Address__c = ip ;
            pv[0].Session_Info__c = sessionInfo;
            pv[0].Location__latitude__s = latitude;
            pv[0].Location__longitude__s = longitude;
        }
        
        upsert pv Unique_Key__c;
        
        Geo_Location__c loc = new Geo_Location__c();
        loc.Page_View__c = pv[0].Id;
        loc.Location__longitude__s = longitude;
        loc.Location__latitude__s = latitude;
        loc.Address__c = address;
        insert loc;
        
    }
        
}