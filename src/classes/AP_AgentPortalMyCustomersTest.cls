@isTest(SeeAlldata=false) 
public class AP_AgentPortalMyCustomersTest {
    public static Contact adminContact;
    public static User portalUser;
    public static Contact adminContactNew;
    public static User portalUserNew;
    public static Account adminAccount;
    public static User portalOnlyAgent;
    
    static void init(){
        
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole0066');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 't66', email='xy66z1@email.com',
                                  emailencodingkey='UTF-8', lastname='Us6656', languagelocalekey='en_US',
                                  localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                                  ,timezonesidkey='America/Los_Angeles', username='x6661@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
            
            adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
            insert adminAccount;
            
            adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
            adminContact.Owner__c = false;
            adminContact.Agent_Representative__c= true;
            adminContact.Portal_Administrator__c= false;
            insert adminContact;
            
            adminContactNew = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
            adminContactNew.Owner__c = false;
            adminContactNew.Agent_Representative__c= false;
            adminContactNew.Portal_Administrator__c= false;
            insert adminContactNew;
            
            Contact agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
            insert agentContact;
            portalUserNew = InitialiseTestData.getPortalUser('t11t@test.com', adminContactNew.Id, 'Admin');
            portalUser = InitialiseTestData.getPortalUser('t11t@test.com', adminContact.Id, 'Admin');
            portalOnlyAgent = InitialiseTestData.getPortalUser('t11t1@test.com', agentContact.Id, 'Agent');
            
            System.runAs(portalUser){
                Property__c property = InitialiseTestData.insertProperties();
                InitialiseTestData.createInventoryUser(property);
            }
            
            System.runAs(portalUser){
                Inquiry__c CIL = InitialiseTestData.getInquiryDetails(DAMAC_Constants.CIL_RT,1);
                insert CIL;
            }
            
            System.runAs(portalOnlyAgent){
                Inquiry__c CIL = InitialiseTestData.getInquiryDetails(DAMAC_Constants.CIL_RT,2);
                insert CIL;
            } 
            
            ApexPages.currentPage().getParameters().put('sfdc.tabName','aX982364');
        }
        
    }
    static void initnew(){
        
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole009');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 't44456', email='x4441@email.com',
                                  emailencodingkey='UTF-8', lastname='Us44', languagelocalekey='en_US',
                                  localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                                  ,timezonesidkey='America/Los_Angeles', username='xy4441@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
            
            adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
            insert adminAccount;
            
            adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
            adminContact.Owner__c = true;
            adminContact.Agent_Representative__c= true;
            adminContact.Portal_Administrator__c= true;
            insert adminContact;
            
            adminContactNew = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
            adminContactNew.Owner__c = false;
            adminContactNew.Agent_Representative__c= false;
            adminContactNew.Portal_Administrator__c= false;
            insert adminContactNew;
            
            Contact agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
            insert agentContact;
            
            portalUserNew = InitialiseTestData.getPortalUser('t11t@test.com', adminContactNew.Id, 'Admin');
            portalUser = InitialiseTestData.getPortalUser('test11111010@test.com', adminContact.Id, 'Admin');
            portalOnlyAgent = InitialiseTestData.getPortalUser('test11011010@test.com', agentContact.Id, 'Agent');
            
            System.runAs(portalUser){
                Property__c property = InitialiseTestData.insertProperties();
                InitialiseTestData.createInventoryUser(property);
            }
            
            System.runAs(portalUser){
                Inquiry__c CIL = InitialiseTestData.getInquiryDetails(DAMAC_Constants.CIL_RT,1);
                insert CIL;
            }
            
            System.runAs(portalOnlyAgent){
                Inquiry__c CIL = InitialiseTestData.getInquiryDetails(DAMAC_Constants.CIL_RT,2);
                insert CIL;
            } 
            
            ApexPages.currentPage().getParameters().put('sfdc.tabName','aX982364');
            
        }
        
    }
    @isTest static void showCILForAdminnew(){
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole008');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 't10110', email='x10100@email.com',
                                  emailencodingkey='UTF-8', lastname='U101156', languagelocalekey='en_US',
                                  localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                                  ,timezonesidkey='America/Los_Angeles', username='x10110@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
            Test.startTest();
            initnew();
            
            System.runAs(portalUserNew){
                Inquiry__c CIL = InitialiseTestData.getInquiryDetails(DAMAC_Constants.CIL_RT,1);
                insert CIL;
                List<Inquiry__c> inqList = new List<Inquiry__c>();
                inqList.add(CIL);
                AP_AgentPortalMyCustomers inquiry = new AP_AgentPortalMyCustomers();
                List<SelectOption> statusVal = inquiry.getStatusValues();
                List<SelectOption> agentNameList = inquiry.getAgentsNameList();
                
                AP_AgentPortalMyCustomers.CILDetailsWrapper objCustomerDetails = new AP_AgentPortalMyCustomers.CILDetailsWrapper(inqList,false); 
                // List<AP_AgentPortalMyCustomers.CILDetailsWrapper> objNEw = new List<AP_AgentPortalMyCustomers.CILDetailsWrapper>();
                //objNEw.add(objCustomerDetails);
                objCustomerDetails  = AP_AgentPortalMyCustomers.getCILlst('New','');
                
            }
            System.runAs(portalUser){
                Inquiry__c CIL = InitialiseTestData.getInquiryDetails(DAMAC_Constants.CIL_RT,1);
                insert CIL;
                List<Inquiry__c> inqList = new List<Inquiry__c>();
                inqList.add(CIL);
                AP_AgentPortalMyCustomers inquiry = new AP_AgentPortalMyCustomers();
                List<SelectOption> statusVal = inquiry.getStatusValues();
                List<SelectOption> agentNameList = inquiry.getAgentsNameList();
                
                AP_AgentPortalMyCustomers.CILDetailsWrapper objCustomerDetails = new AP_AgentPortalMyCustomers.CILDetailsWrapper(inqList,false); 
                // List<AP_AgentPortalMyCustomers.CILDetailsWrapper> objNEw = new List<AP_AgentPortalMyCustomers.CILDetailsWrapper>();
                //objNEw.add(objCustomerDetails);
                objCustomerDetails  = AP_AgentPortalMyCustomers.getCILlst('New','');
                
            }
            Test.stopTest();
        }
    }
    public static testmethod void CILDownloadTest(){
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole007');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'te06', email='x0001@email.com',
                                  emailencodingkey='UTF-8', lastname='U00056', languagelocalekey='en_US',
                                  localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                                  ,timezonesidkey='America/Los_Angeles', username='x00z1@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
            init();
            
            AP_AgentPortalMyCustomers obj = new AP_AgentPortalMyCustomers();
            //obj.loadCILData();
            //obj.customListViewId = 'customListViewId';
            
            // Inner Class
            /*AP_AgentPortalMyCustomers.CustomerDetails objCustomerDetails = new AP_AgentPortalMyCustomers.CustomerDetails(); 
            objCustomerDetails.FirstName = 'Test';
            objCustomerDetails.LastName = 'Test';
            objCustomerDetails.Email='test';
            objCustomerDetails.CountryCode='Afghanistan: 0093';
            objCustomerDetails.MobileNo='99999999999';
            objCustomerDetails.Passport='test';
            objCustomerDetails.OwnerID = 'Test';
            objCustomerDetails.AgentName='test';
            objCustomerDetails.PreferredLanguage='test';
            objCustomerDetails.InquiryNo = 'Test';
            objCustomerDetails.CreatedDate = '2018-09-09T00:00:00Z';  
            objCustomerDetails.Comments = 'Test';
            objCustomerDetails.DPOK= true;
            objCustomerDetails.DOCOK= true;
            List<AP_AgentPortalMyCustomers.CustomerDetails> objNEw = new List<AP_AgentPortalMyCustomers.CustomerDetails>();
            objNEw.add(objCustomerDetails);*/
            // obj.getloadCILData();
            //System.debug('....obj.getloadCILData()...'+obj.getloadCILData());
        }
    }
    
    @isTest static void showCILForAdmin(){
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole0031');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 't11', email='x1@email.com',
                                  emailencodingkey='UTF-8', lastname='Us156', languagelocalekey='en_US',
                                  localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                                  ,timezonesidkey='America/Los_Angeles', username='x1@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
            Test.startTest();
            init();
            
            System.runAs(portalUser){
                AP_AgentPortalMyCustomers inquiry = new AP_AgentPortalMyCustomers();
                // inquiry.getloadCILData();
                // //system.assert(inquiry.CILLists.size()==2);
            }
            
            
            Test.stopTest();
        }
    }
    
    @isTest static void showCILForAgent(){
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole0023');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 't12156', email='x1211z1@email.com',
                                  emailencodingkey='UTF-8', lastname='Use1241156', languagelocalekey='en_US',
                                  localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                                  ,timezonesidkey='America/Los_Angeles', username='xy11z341@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
            Test.startTest();
            init();
            
            System.runAs(portalOnlyAgent){
                AP_AgentPortalMyCustomers inquiry = new AP_AgentPortalMyCustomers();
                //inquiry.getloadCILData();
                // //system.assert(inquiry.CILLists.size()==1);
            }
            
            Test.stopTest();
        }
    }
    @isTest static void showCILForAgent2(){
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole001');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 't116', email='x111z1@email.com',
                                  emailencodingkey='UTF-8', lastname='Use156', languagelocalekey='en_US',
                                  localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                                  ,timezonesidkey='America/Los_Angeles', username='x11z1@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
            Test.startTest();
            adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
            insert adminAccount;
            
            adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
            adminContact.Owner__c = false;
            adminContact.Agent_Representative__c= true;
            adminContact.Portal_Administrator__c= false;
            insert adminContact;
            
            Contact agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
            insert agentContact;
            
            portalUser = InitialiseTestData.getPortalUser('test1111@test.com', adminContact.Id, 'Admin');
            portalOnlyAgent = InitialiseTestData.getPortalUser('test2221@test.com', agentContact.Id, 'Agent');
            
            System.runAs(portalUser){
                Property__c property = InitialiseTestData.insertProperties();
                InitialiseTestData.createInventoryUser(property);
            }
            
            System.runAs(portalUser){
                Inquiry__c CIL = InitialiseTestData.getInquiryDetails(DAMAC_Constants.CIL_RT,1);
                insert CIL;
            }
            
            System.runAs(portalOnlyAgent){
                Inquiry__c CIL = InitialiseTestData.getInquiryDetails(DAMAC_Constants.CIL_RT,2);
                insert CIL;
            } 
            
            ApexPages.currentPage().getParameters().put('sfdc.tabName','aX982364');
            
            System.runAs(portalOnlyAgent){
                AP_AgentPortalMyCustomers inquiry = new AP_AgentPortalMyCustomers();
                //inquiry.getloadCILData();
                ////system.assert(inquiry.CILLists.size()==1);
            }
            
            Test.stopTest();
        }
    }
    
    
    
    @isTest static void showCILForAgent3(){
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole002');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 't0006', email='x4541@email.com',
                                  emailencodingkey='UTF-8', lastname='Us4556', languagelocalekey='en_US',
                                  localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                                  ,timezonesidkey='America/Los_Angeles', username='x454z1@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
            Test.startTest();
            adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
            insert adminAccount;
            
            adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
            adminContact.Owner__c = true;
            adminContact.Agent_Representative__c= true;
            adminContact.Portal_Administrator__c= true;
            insert adminContact;
            
            Contact agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
            insert agentContact;
            
            portalUser = InitialiseTestData.getPortalUser('tes333t@test.com', adminContact.Id, 'Admin');
            portalOnlyAgent = InitialiseTestData.getPortalUser('tes333t1@test.com', agentContact.Id, 'Agent');
            
            System.runAs(portalUser){
                Property__c property = InitialiseTestData.insertProperties();
                InitialiseTestData.createInventoryUser(property);
            }
            
            System.runAs(portalUser){
                Inquiry__c CIL = InitialiseTestData.getInquiryDetails(DAMAC_Constants.CIL_RT,1);
                insert CIL;
            }
            
            System.runAs(portalOnlyAgent){
                Inquiry__c CIL = InitialiseTestData.getInquiryDetails(DAMAC_Constants.CIL_RT,2);
                insert CIL;
            } 
            
            ApexPages.currentPage().getParameters().put('sfdc.tabName','aX982364');
            
            System.runAs(portalOnlyAgent){
                AP_AgentPortalMyCustomers inquiry = new AP_AgentPortalMyCustomers();
                // inquiry.getloadCILData();
                ////system.assert(inquiry.CILLists.size()==1);
            }
            
            Test.stopTest();
        }
    }
    
    @isTest static void showCILForSuperUser(){
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole008');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 't10110', email='x10100@email.com',
                                  emailencodingkey='UTF-8', lastname='U101156', languagelocalekey='en_US',
                                  localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                                  ,timezonesidkey='America/Los_Angeles', username='x10110@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
            Test.startTest();
            Id superUserProfileId = [select Id from Profile where name = 'Customer Community - Super User'].Id;
            
            Account superUserAccount = new Account(Name = 'Test Super User', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId());
            insert superUserAccount;
            
            Contact superUserCon = new Contact();
            superUserCon.LastName = 'testSuperUser';
            superUserCon.AccountId = superUserAccount.Id;
            superUserCon.Email = 'superuser@test.com';
            superUserCon.Portal_Administrator__c = true;
            superUserCon.Owner__c = true;
            insert superUserCon;

            User portalUser = new User(alias = 'test457', email= 'superuser@test.com',
                                       emailencodingkey='UTF-8', lastname='User 457', languagelocalekey='en_US',
                                       localesidkey='en_US', profileid = superUserProfileId, country='United Arab Emirates',
                                       IsActive =true, ContactId = superUserCon.Id, timezonesidkey='America/Los_Angeles', 
                                       username = 'superuser@test.com');
            insert portalUser;
            
            
            System.runAs(portalUser){
                Bulk_Upload_Request__c bu = new Bulk_Upload_Request__c();
                bu.Agency__c = superUserAccount.Id;
                bu.Agent__c = superUserCon.Id;
                bu.Status__c = 'Submitted';
				insert bu;
                
                Inquiry__c CIL = InitialiseTestData.getInquiryDetails(DAMAC_Constants.CIL_RT,1);
                CIL.Dataload_Idetifier__c = bu.Name;
                CIL.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
                insert CIL;
                List<Inquiry__c> inqList = new List<Inquiry__c>();
                inqList.add(CIL);
                AP_AgentPortalMyCustomers inquiry = new AP_AgentPortalMyCustomers();
                List<SelectOption> statusVal = inquiry.getStatusValues();
                List<SelectOption> agentNameList = inquiry.getAgentsNameList();
                
                
                List<Bulk_Upload_Request__c> busList = new List<Bulk_Upload_Request__c>();
                busList.add(bu);
                
                List<SelectOption> BUSStatusVal = inquiry.getBUSStatusValues();
                List<SelectOption> BUSAgentsVal = inquiry.getBUSAgentsNameList();
                AP_AgentPortalMyCustomers.CILDetailsWrapper objCustomerDetails = new AP_AgentPortalMyCustomers.CILDetailsWrapper(inqList,false); 
                objCustomerDetails  = AP_AgentPortalMyCustomers.getCILlst('New','');
                
                List<Integer> totalLeadsUploadedCountList;
        		List<Integer> MeetingsScheduledCountList;
                AP_AgentPortalMyCustomers.BUSDetailsWrapper BUSDetails = new AP_AgentPortalMyCustomers.BUSDetailsWrapper(busList,false,totalLeadsUploadedCountList,MeetingsScheduledCountList); 
                
                BUSDetails  = AP_AgentPortalMyCustomers.getBUSlist('Submitted', superUserCon.Id);
                               
                AP_AgentPortalMyCustomers.myObject myObj = new AP_AgentPortalMyCustomers.myObject('','title','description',Date.today(),'createdName','John','Doe','');
                
            }
            Test.stopTest();
        }
    }
}