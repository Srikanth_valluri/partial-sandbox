/*********************************************************************************************************
* Name               : AgentCapsuleFormController
* Test Class         : AgentCapsuleFormController_Test
* Description        : Controller class for AgentCapsuleForm VF Page
* Created Date       : 28/09/2020
* ----------------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst         28/09/2020      Initial Draft.
* 1.1         QBurst         21/10/2020      Added redirection to homepage on Save
**********************************************************************************************************/
public Class AgentCapsuleFormController{
    
    public New_Partnership_Program__c pp {get;set;}
    public Activity_Manager__c activityManager {get;set;}
    public account agentAcc {get; set;}
    
    
    public String meeetingFromAndTill {get; set;}
    public String trainingDetails {get; set;}
    public String meetingFeedback {get; set;}
    public String comments {get; set;}
    public String siteVisitDetails {get; set;}
    public String location {get; set;}
    public String latestMonthOfSale {get; set;}
    public string latestMonthOfActivity {get; set;}
    public string agentId {get; set;}
    public string selAccountId {get; set;}
    public string reasonOthers{get; set;}
    
    public DateTime lastMeetingDate {get; set;}
    public DateTime nextMeetingDate {get; set;}
    public DateTime latestAgentPortalTrainingDate {get; set;}
    public DateTime latestProductTrainingDate {get; set;}
    public DateTime lastSiteVisitDate {get; set;}
    
    public List<String> clientBaseCountryList {get; set;}
    public list<Contact> owners {get; set;}
    public list<Contact> signatories {get; set;}
    public List<Booking_Unit__c> buList{get;set;}
    public List<monthDataWrapper> wrapperList {get; set;}
    public List<SelectOption> reasonList {get;set;}
    
    public Set<String> reasonSet {get;set;}
    
    public Integer removeSelected {get;set;}
    
    public Boolean isOtherReason {get; set;}
    public Boolean hasAccess {get; set;}
    public Boolean userViewPage{get;set;}
    
    public AgentCapsuleFormController(Apexpages.StandardController sc){
        removeSelected = 0;
        owners = new list<Contact>();
        signatories = new list<Contact>();
        meeetingFromAndTill = '';
        clientBaseCountryList = new List<String>();
        reasonList = new List<SelectOption>();
        reasonSet = new Set<String>();
        trainingDetails = reasonOthers  = '';
        isOtherReason = false;
        System.debug('agentId::' + agentId);
        if (apexpages.currentpage() != null) {
            agentId = apexpages.currentpage().getparameters().get('id');
        }
        if(UserInfo.getUiThemeDisplayed() == 'Theme4t'){
            userViewPage = true;
        }
        reasonList.add(new SelectOption('Active with DAMAC','Active with DAMAC'));
        reasonList.add(new SelectOption('Competition Focus','Competition Focus'));
        reasonList.add(new SelectOption('CRM/Commission Issue with DAMAC','CRM/Commission Issue with DAMAC'));
        reasonList.add(new SelectOption('Focused on Non Dubai Real Estate','Focused on Non Dubai Real Estate'));
        reasonList.add(new SelectOption('No Answer/Not Reachable','No Answer/Not Reachable'));
        reasonList.add(new SelectOption('Non Real Estate company','Non Real Estate company'));
        reasonList.add(new SelectOption('Not Focusing on Primary','Not Focusing on Primary'));
        reasonList.add(new SelectOption('Product Constraint','Product Constraint'));
        reasonList.add(new SelectOption('Secondary Focus','Secondary Focus'));
        reasonList.add(new SelectOption('To be Quarantined','To be Quarantined'));
        reasonList.add(new SelectOption('To be Terminated','To be Terminated'));
        reasonList.add(new SelectOption('Focused on DAMAC','Focused on DAMAC'));
        reasonList.add(new SelectOption('Others','Others'));
        reasonSet.add('Active with DAMAC');
        reasonSet.add('Competition Focus');
        reasonSet.add('CRM/Commission Issue with DAMAC');
        reasonSet.add('Focused on Non Dubai Real Estate');
        reasonSet.add('No Answer/Not Reachable');
        reasonSet.add('Non Real Estate company');
        reasonSet.add('Not Focusing on Primary');
        reasonSet.add('Product Constraint');
        reasonSet.add('Secondary Focus');
        reasonSet.add('To be Quarantined');
        reasonSet.add('To be Terminated');
        reasonSet.add('Focused on DAMAC');
        reasonSet.add('Others');   
        if(agentId != null && agentId != ''){
            fetchData(agentId);
        }
    }

    public void mobileViewUpdate(){
        userViewPage = true;
    }
    
    
    public void fetchData(id selRecordId){
        agentId = selRecordId;
        Boolean superUser = false;
        siteVisitDetails = '';
        trainingDetails = '';
        location = '';
        latestMonthOfActivity = '';
        meetingFeedback = '';
        comments = '';
        meeetingFromAndTill = '';
        lastMeetingDate = null;
        latestAgentPortalTrainingDate = null;
        latestProductTrainingDate = null;
        nextMeetingDate = null;
        lastSiteVisitDate = null;
        for(String userId: Label.Agent_Capsule_Form_Super_User_Ids.split(',')){
            System.debug('userId.trim()::' + userId.trim());
            if(UserInfo.getUserId().contains(userId.trim())){
                superUser = true;
            }
        }
        System.debug('UserInfo.getUserId()::' + UserInfo.getUserId());
        System.debug('superUser::' + superUser);
        System.debug('selRecordId::' + selRecordId);
        hasAccess = [SELECT RecordId, HasReadAccess 
                             FROM UserRecordAccess WHERE UserId =: UserInfo.getUserId() AND RecordId =:selRecordId].HasReadAccess;
        if(hasAccess){
            agentAcc = [ SELECT  Id, Broker_Class__c, Name, OwnerId, Owner.Name, 
                          Vendor_ID__c, Reasons_for_Not_Working_with_DAMAC__c,
                          Year_of_Establishment__c, Number_of_Agents__c,
                          Overseas_Office__c, Team_Type__c, Average_PSF_Sold__c,
                          Secondary_Owner__c, Secondary_Owner__r.Name, Team_Nationality__c,
                          Secondary_Owner__r.ManagerId, Secondary_Owner__r.Manager.Name,
                          Agent_Client_Base_Country__c, Top_of_other_Developer_Name__c, No_of_Employees__c,
                          How_many_offices_and_Locations__c, Monthly_Sales_Potential__c, AnnualRevenue,
                          Active_in_the_Market__c, Active_with_DAMAC__c, Annual_Sales_nos_of_broker__c,
                          Average_Client_Budget__c, Avg_Ticket_Size_sold__c, Brokers_Monthly_Potential__c,
                          Medium_of_Marketing__c, Developer_Preference__c, Focus_Primary__c, 
                          Focus_Rental__c, Focus_Secondary__c, DOS__c, Payment_Plan_Preference__c,
                          Sales_withDP__c, Sales_withEmaar__c, Sales_with_MeerasSobha__c, 
                          Sales_with_Other__c, Market_Specialization__c, IsPersonAccount,
                          Current_Database_Strength__c,Average_CPL_Cost_per_Lead__c,Average_Commission__c,
                          Monthly_Marketing_Budget__c,Average_Monthly_Lead_Generation__c,Leeds_Meeting_Percentage__c,
                          Meeting_Sales_Percentage__c,Gross_Net_Percentage__c,Additional_BD_Notes__c,
                          Potential_Target_for_the_Quarter_AED__c
                         FROM Account WHERE Id =: selRecordId];
                        // WITH SECURITY_ENFORCED];
            system.debug('agentAcc: ' + agentAcc);
            if(agentAcc != null && agentAcc.Agent_Client_Base_Country__c != null && agentAcc.Agent_Client_Base_Country__c != ''){
                clientBaseCountryList.addAll(agentAcc.Agent_Client_Base_Country__c.split(';'));

            }
            if(!reasonSet.contains(agentAcc.Reasons_for_Not_Working_with_DAMAC__c)){
                reasonOthers = agentAcc.Reasons_for_Not_Working_with_DAMAC__c;
                agentAcc.Reasons_for_Not_Working_with_DAMAC__c = 'Others';
            }
            
            ActivityHistory latestActivity;
            buList = new List<Booking_Unit__c>([SELECT Id, Name, Registration_Date__c 
                                                                      FROM Booking_Unit__c
                                                                       WHERE (Agency_Name__c =: agentAcc.Name 
                                                                               AND  Registration_Date__c != null) 
                                                                       ORDER BY Registration_Date__c DESC ]);
    
            system.debug('BU List:  ' + buList);
            Account activityList = [SELECT  Id, (SELECT Id, Start_Date__c, StartDateTime, RM_Comments__c, Training_Details__c,
                                                    Description, status__c, Activity_Sub_Type__c, Activity_Type__c,
                                                    EndDateTime, End_Date__c, Location__c, OwnerId, ActivityDate
                                                 FROM ActivityHistories 
                                                 ORDER BY ActivityDate DESC, LastModifiedDate DESC 
                                                 LIMIT 500)
                                    FROM Account WHERE Id =: selRecordId ];
            system.debug('activityList: size ' + activityList.ActivityHistories.size());
            String userProfileName = [select Name from profile where id =: userinfo.getProfileId()].Name;
            for(ActivityHistory ah: activityList.ActivityHistories){
                system.debug('ahhh' + ah.ActivityDate);
                if(ah.OwnerId == UserInfo.getUserId() || superUser || userProfileName == 'System Administrator'){
                    if(latestActivity == null && ah.ActivityDate != null){
                         latestActivity = ah;
                         system.debug('ah.ActivityDate : ' + ah.ActivityDate );
                         DateTime activityDateDT = DateTime.newInstance(ah.ActivityDate.year(), ah.ActivityDate.month(), ah.ActivityDate.day());
                         system.debug('activityDateDT: ' + activityDateDT);
                         latestMonthOfActivity = activityDateDT.format('MMMM') + ' ' + activityDateDT.year();
                         system.debug('latestMonthOfActivity: ' + latestMonthOfActivity);
                         location = ah.Location__c;
                         comments = ah.Description;
                         
                         meetingFeedback = '';
                         if(ah.Description != null){
                             meetingFeedback = ah.Description;
                         }
                         if(ah.RM_Comments__c != null){
                             if(meetingFeedback != ''){
                                 meetingFeedback += ' - ';
                             }
                             meetingFeedback += ah.RM_Comments__c;
                         }
                    }
                    system.debug('latestActivity: ' + latestActivity);
                    if(lastMeetingDate == null && ah.Activity_Type__c == 'Meetings' && ah.ActivityDate != null){
                        lastMeetingDate = ah.ActivityDate;
                        meeetingFromAndTill = ah.ActivityDate.format();
                        if(ah.StartDateTime  != null ){
                            meeetingFromAndTill = ah.StartDateTime.format();
                        }  else if(ah.Start_Date__c  != null ){
                            meeetingFromAndTill = ah.Start_Date__c.format();
                        }
                        if(ah.EndDateTime  != null ){
                            meeetingFromAndTill += ' - ' + ah.EndDateTime.format();
                        }  else if(ah.End_Date__c  != null ){
                            meeetingFromAndTill += ' - ' + ah.End_Date__c.format();
                        }
                    }
                    system.debug('lastMeetingDate: ' + lastMeetingDate);
                    if(latestAgentPortalTrainingDate == null 
                            && (ah.Activity_Type__c == 'Office Trainings' || ah.Activity_Type__c == 'Group Trainings')
                            && ah.Activity_Sub_Type__c == 'Agent Portal' && ah.ActivityDate != null){
                        latestAgentPortalTrainingDate = ah.ActivityDate;
                        trainingDetails = ah.Training_Details__c;
                    }
                    system.debug('latestAgentPortalTrainingDate: ' + latestAgentPortalTrainingDate);
                    if(latestProductTrainingDate == null 
                            && (ah.Activity_Type__c == 'Office Trainings' || ah.Activity_Type__c == 'Group Trainings')
                            && ah.Activity_Sub_Type__c == 'Product' && ah.ActivityDate != null){
                        latestProductTrainingDate = ah.ActivityDate;
                        trainingDetails = ah.Training_Details__c;
                    }
                    system.debug('latestProductTrainingDate: ' + latestProductTrainingDate);
                    if(nextMeetingDate == null && (ah.status__c == 'Not Started' || ah.status__c == 'Planned') && ah.ActivityDate != null){
                        nextMeetingDate = ah.ActivityDate;
                    }
                    system.debug('nextMeetingDate: ' + nextMeetingDate);
                    if(lastSiteVisitDate == null && ah.Activity_Type__c == 'Site Visits' && ah.ActivityDate != null){
                        lastSiteVisitDate = ah.ActivityDate;
                        siteVisitDetails = ah.Activity_Sub_Type__c;
                    }
                    system.debug('lastSiteVisitDate: ' + lastSiteVisitDate);
                }
            }
            system.debug('latestActivity: ' + latestActivity);
            if(latestActivity != null){
                String taskId = '%' + String.valueOf(latestActivity.Id).substring(0, 15) + '%';
                system.debug('taskId: ' + taskId);
                for(Activity_Manager__c am : [SELECT Id, Office_Branding__c, Project_Identified_for_Broker__c,
                                                         Broker_s_Request__c, DAMAC_s_Plan_of_Action__c, 
                                                         Activity_or_Task_Id__c, Sales_Team_Introduced__c
                                                FROM Activity_Manager__c
                                                WHERE Activity_or_Task_Id__c LIKE: taskId]){
                   activityManager = am;
               }
           }
           system.debug('activityManager: ' + activityManager);
           if(activityManager == null){
               activityManager = new Activity_Manager__c();
           }
            if(buList.size() > 0 && buList[0].Registration_Date__c != NULL) {
                DateTime regDateDT = DateTime.newInstance(buList[0].Registration_Date__c.year(),
                                buList[0].Registration_Date__c.month(),  buList[0].Registration_Date__c.day());
                latestMonthOfSale = regDateDT.format('MMMM') + ' ' + buList[0].Registration_Date__c.year(); 
            }

            pp = new New_Partnership_Program__c();
            for(New_Partnership_Program__c prg: [SELECT Id, Account__c, Budget__c, Units_Allocated__c,
                                                    Cost_of_Sale_Under_PP_Unity__c, Current_commission_slab__c,
                                                    Details_of_next_confirmed_Roadshow__c, Exclusivity_Assignment__c,
                                                    Flat_Commission__c, Validity_Flat_Commission__c,
                                                    Mall_Stands__c, Other_Benefits_Validity__c,
                                                    Partnership_Unity_Type__c, Validity__c 
                                                 FROM New_Partnership_Program__c 
                                                 WHERE Account__c =: selRecordId LIMIT 1]){
                pp = prg;
            }
    
            owners = [Select id, name, Birthdate, Nationality__c 
                        from Contact 
                        where accountId =: selRecordId and Owner__c = true 
                        order by createdDate asc];
            signatories = [Select id, name, Email, MobilePhone  
                        from Contact 
                        where accountId =: selRecordId and Authorised_Signatory__c = true 
                        order by createdDate asc];
            integer year = System.today().year() - 1;
            map<string, decimal> mpAmounts = new map<string, decimal>();
            for(AggregateResult ar : [SELECT sum(Total_Booking_Amount__c) tot, CALENDAR_MONTH(Registration_Date__c) month,
                                                        CALENDAR_YEAR(Registration_Date__c) year 
                                        FROM NSIBPM__Service_Request__c 
                                        WHERE Agency__c =: selRecordId and CALENDAR_YEAR(Registration_Date__c)>=:year
                                        GROUP BY CALENDAR_MONTH(Registration_Date__c), CALENDAR_YEAR(Registration_Date__c)
                                        ORDER BY CALENDAR_YEAR(Registration_Date__c), CALENDAR_MONTH(Registration_Date__c)]){
                mpAmounts.put(string.valueOf(ar.get('month'))+'###'+string.valueOf(ar.get('year')), (decimal)ar.get('tot'));
            }
            
            map<string, decimal> mpAmountsNet = new map<string, decimal>();
            for(AggregateResult ar : [SELECT sum(Total_Booking_Amount__c) tot, CALENDAR_MONTH(Registration_Date__c) month,
                                                        CALENDAR_YEAR(Registration_Date__c) year 
                                        FROM NSIBPM__Service_Request__c 
                                        WHERE Agency__c =: selRecordId 
                                            AND CALENDAR_YEAR(Registration_Date__c)>=:year
                                            AND NSIBPM__Internal_Status_Name__c = 'SPA Executed'
                                        GROUP BY CALENDAR_MONTH(Registration_Date__c), CALENDAR_YEAR(Registration_Date__c)
                                        ORDER BY CALENDAR_YEAR(Registration_Date__c), CALENDAR_MONTH(Registration_Date__c)]){
                mpAmountsNet.put(string.valueOf(ar.get('month'))+'###'+string.valueOf(ar.get('year')), (decimal)ar.get('tot'));
            }
    
            data = new list<yearlyData>();
            for(integer j = year; j <=System.today().year(); j++){
                list<monthlyData> monthData = new list<monthlyData>();
                decimal amount = 0;
                decimal netAmount = 0;
                for(integer i=1; i<=12; i++){
                    string key = string.valueOf(i)+'###'+string.valueOf(j);
                    monthData.add(new monthlyData(key, getMonth(i),
                                                  mpAmounts.containsKey(key) ? mpAmounts.get(key) : 0, 
                                                  mpAmountsNet.containsKey(key) ? mpAmountsNet.get(key) : 0, year));
                    amount += (mpAmounts.containsKey(key) ? mpAmounts.get(key) : 0);
                    netAmount += (mpAmountsNet.containsKey(key) ? mpAmountsNet.get(key) : 0);
                }
                data.add(new yearlyData(j, monthData, amount, netAmount));
            }
            wrapperList = new List<monthDataWrapper>();
            Map<String, List<monthlyData>> mDataMap = new Map<String, List<monthlyData>>();
            for(yearlyData yData: data){
                for(monthlyData mData: yData.monthData){
                    List<monthlyData> mDataList = new List<monthlyData>();
                    if(mDataMap.containsKey(mData.monthName)){
                        mDataList = mDataMap.get(mData.monthName);
                    }
                    mDataList.add(mData);
                    mDataMap.put(mData.monthName, mDataList);
                } 
                List<monthlyData> mDataList2 = new List<monthlyData>();
                if(mDataMap.containsKey('Total')){
                    mDataList2 = mDataMap.get('Total');
                }
                monthlyData totalData = new monthlyData('', 'Total', yData.totGrossAmount, yData.totNetAmount, yData.year);
                mDataList2.add(totalData);
                mDataMap.put('Total', mDataList2);  
            }
            system.debug('mDataMap: ' + mDataMap.keyset());
            for(String monthName: mDataMap.keyset()){
                monthDataWrapper wrapper = new monthDataWrapper(monthName, mDataMap.get(monthName)); 
                wrapperList.add(wrapper);   
            }
    
        }
    }
    
    
    public String getMonth(Integer month) {
        return DateTime.newInstanceGMT(Date.newInstance(2000, month, 1), Time.newInstance(0, 0, 0, 0)).formatGMT('MMMM');
    }    

    public list<yearlyData> data{get; set;}
    public class monthlyData{
        public string key{get; set;}
        public string monthName{get; set;}
        public integer year{get; set;}
        public decimal netAmount{get; set;}
        public decimal grossAmount{get; set;}
        public monthlyData(string k, string mName, decimal grossAmt, decimal netAmt, integer y){
            key = k;
            monthName = mName;
            grossAmount = grossAmt;
            netAmount = netAmt;
            year = y;
        }
    }
    
    public class monthDataWrapper{
        public String monthName{get; set;}
        public list<monthlyData> monthData{get; set;}
        public monthDataWrapper(String m, list<monthlyData> mData){
            monthName = m;
            monthData = mData;
        }
    }

    public class yearlyData{
        public integer year{get; set;}
        public list<monthlyData> monthData{get; set;}
        public decimal totNetAmount{get; set;}
         public decimal totGrossAmount{get; set;}
        public yearlyData(integer k, list<monthlyData> mData, decimal gAmt, decimal netAmt){
            year = k;
            monthData = mData;
            totNetAmount = netAmt;
            totGrossAmount = gAmt;
        }
    }

    public void fetchAccountDetails(){
        System.debug('>>>>>>>>>>>>>' + selAccountId);
        fetchData(selAccountId);
    }

    public PageReference saveAgentCapsuleForm(){
        try{
            if(pp.id == null){
                pp.Account__c = agentAcc.id;
                Insert pp;
            } else{
                Update pp;
            }
            if(agentAcc.Reasons_for_Not_Working_with_DAMAC__c == 'Others' && reasonOthers != '')
                agentAcc.Reasons_for_Not_Working_with_DAMAC__c = reasonOthers;
            update agentAcc;
            if(owners.size() > 0){
                update owners;
            }
            if(signatories.size() > 0){
                update signatories;
            }
        } catch(exception e){
            system.debug('Error while updating Account: ' + e);
        }
        String reqURL = System.URL.getSalesforceBaseUrl().toExternalForm();// 1.1  + '/apex/agentCapsuleForm?id=' + agentId;
        PageReference invReqPage = new PageReference(reqURL);
        invReqPage.setRedirect(true);
        return invReqPage;
    }
}