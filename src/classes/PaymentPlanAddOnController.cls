/****************************************************************************************************
* Name               : PaymentPlanAddOnController
* Description        : Controller class for Payment Plan VF Page
* Created Date       : 16/10/2020
* Created By         : DAMAC
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         DAMAC         16/10/2020      Initial Draft
****************************************************************************************************/

global class PaymentPlanAddOnController {
    public Alacarte_AddOn__c addOn{get; set;}
    public string addOnId{get; set;}
    public list<SelectOption> planOptions {get; set;}
    public list<SelectOption> planTypes {get; set;}
    public string payplanJSON {get; set;}
    public string selPlan{get; set;}
    public string selPlanType{get; set;}
    
    public String buildingId {get; set;}
    public Map<String, Id> planNamesMap {get; set;}
    public List<String> planNamesList {get; set;}
    public Integer floorCount {get; set;}
    public Integer unitCount {get; set;}
    public Location__c building {get; set;}
    public Map<String, Date> payPlanEndDateMap { get; set;}
    public String payPlanEndDateMapJSON {get; set;}
    public String propertyName;
    /*********************************************************************************************
    * @Description : Constructor method
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public PaymentPlanAddOnController() {
        addOnId = ApexPages.currentPage().getParameters().get('Id');
        payplanJSON = 'default';
        payPlanEndDateMap = new Map<String, Date>();
        payPlanEndDateMapJSON = JSON.serialize(payPlanEndDateMap);
        planOptions = new list<SelectOption>();
        planTypes = new list<SelectOption>();

        if(addOnId != null && addOnId != ''){
            addOn = [SELECT Id, Name, Furnishing_price__c, Pool_Price__c, Bedroom_type__c, Unit_Categorization__c 
                               FROM Alacarte_AddOn__c 
                               WHERE Id =: addOnId];
            planOptions.add(new SelectOption('','--Select Plan--'));
            planTypes.add(new SelectOption('','--Select Plan Type--'));
            planTypes.add(new SelectOption('Furnishing','Furnishing'));
            planTypes.add(new SelectOption('Pool','Pool'));
            for(Payment_Plan__c payPlan: [SELECT id, Name, Building_ID__c, Booking_Unit__c, Payment_Term_Description__c,
                               Active__c, Effective_To_calculated__c, Effective_from__c,Effective_To__c
                        FROM Payment_Plan__c
                        WHERE Effective_To_calculated__c >= TODAY
                        AND Active__c = TRUE
                        And AddOn_plan__c = true 
                        ORDER BY CreatedDate DESC
                        LIMIT 1000]){
                String planName = payPlan.Name;
                if(payPlan.Payment_Term_Description__c != null  && payPlan.Payment_Term_Description__c != ''){
                    planName += ' - ' + payPlan.Payment_Term_Description__c;
                }
                planOptions.add(new SelectOption(payPlan.Id, planName));
            }
            fetchPayPlans();
        }
    }

    public void fetchPayPlans(){
        List<Payment_Plan__c> payPlanList = new List<Payment_Plan__c>();
        List<Id> payPlanIds = new List<Id>();
        List<Payment_Plan_Association__c> lstAssoc = [SELECT Id, Name, Payment_Plan__c, Payment_Plan__r.Name, 
                                                        Location__c, Effective_From__c, Plan_Type__c,
                                                        Effective_To__c, Property__c,
                                                        Property__r.Property_Name__c
                                                     FROM Payment_Plan_Association__c
                                                     WHERE Alacarte_AddOn__c =: addOnId];
        for(Payment_Plan_Association__c planAssoc : lstAssoc){
            payPlanIds.add(planAssoc.Payment_Plan__c);
            if(!payPlanEndDateMap.containsKey(planAssoc.Payment_Plan__c)){
                payPlanEndDateMap.put(String.valueOf(planAssoc.Payment_Plan__c), planAssoc.Effective_To__c);
            }
        }
        payPlanEndDateMapJSON = JSON.serialize(payPlanEndDateMap);
        system.debug('payPlanIds: ' + payPlanIds);

        map<id, list<Payment_Terms__c>> mpTerms = new map<id, list<Payment_Terms__c>>();
        for(Payment_Plan__c payPlan: [SELECT Id, Building_Location__c, Name, 
                                            Payment_Term_Description__c, Effective_To_calculated__c, 
                                            LastModifiedDate, Effective_To__c,
                                            (SELECT Id, Name,Installment__c,Description__c,Milestone_Event__c, CreatedDate,
                                                Milestone_Event_Arabic__c,Percent_Value__c FROM Payment_Terms__r) 
                                       FROM Payment_Plan__c WHERE Id IN: payPlanIds]){
            mpTerms.put(payPlan.id, payPlan.Payment_Terms__r);
        }
        assocs = new list<PaymentAssociations>();
        for(Payment_Plan_Association__c planAssoc : lstAssoc)
            assocs.add(new PaymentAssociations(planAssoc,mpTerms.get(planAssoc.Payment_plan__c)));
        system.debug('payPlanList'+assocs);
        
        
        payplanJSON = JSON.serialize(assocs);
        system.debug('payplanJSON'+payplanJSON);
    }

    public class PaymentAssociations{
        public Payment_Plan_Association__c planAssoc;
        public list<Payment_Terms__c> paymentTerms;
        public PaymentAssociations(Payment_Plan_Association__c ppa, list<Payment_Terms__c> terms){
            paymentTerms = terms;
            planAssoc = ppa;
        }
    }

    public list<PaymentAssociations> assocs{get;set;}
    public void associatePayPlan(){
        
        String endDate = ApexPages.currentPage().getParameters().get('endDate');
        system.debug('payPlanId'+selPlan);
        system.debug('endDate'+endDate);
        system.debug('selPlanType'+selPlanType);
        /*
        Payment_Plan__c explan = [select Id, Building_ID__c, Name, Building_Location__c,
                                  Effective_From__c,Effective_To__c ,
                                  (SELECT Id, Description__c, Milestone_Event__c, Line_ID__c,
                                                 Milestone_Event_Arabic__c, Percent_Value__c, Payment_Date__c,
                                                 Installment__c, Modified_Percent_Value__c, Seq_No__c, Event_Days__c
                                                 FROM Payment_Terms__r ORDER  BY Installment__c),
                                  Effective_To_calculated__c, TERM_ID__c from Payment_Plan__c Where id=: payPlanId];
        system.debug('explan: ' + explan.Name);
        Payment_Plan__c newPlan = new Payment_Plan__c();
        newplan = explan.clone();
        newplan.Building_ID__c = building.Location_ID__c;
        newPlan.Building_Location__c = buildingId;
        if(endDate != null && endDate != ''){
            newplan.Effective_To__c =  Date.parse(endDate);
        }
        newplan.Parent_Payment_Plan__c = explan.Id;
        insert newplan;
        List<Payment_Terms__c> payTermsToInsert = new List<Payment_Terms__c>();
        for(Payment_Terms__c term: explan.Payment_Terms__r){
            Payment_Terms__c newTerm = term.clone();
            newTerm.Payment_Plan__c = newplan.Id;
            payTermsToInsert.add(newTerm);
        }
        insert payTermsToInsert;
        */
        Payment_Plan_Association__c planAssoc = new Payment_Plan_Association__c();
        planAssoc.Payment_Plan__c = selPlan;
        planAssoc.Alacarte_AddOn__c = addOnId;
        planAssoc.Effective_From__c = system.today();
        planAssoc.Plan_Type__c = selPlanType;
        planAssoc.AddOn_Unique_key__c = addOnId+'###'+selPlan+'###'+selPlanType;
        if(endDate != null && endDate != ''){
            planAssoc.Effective_To__c =  Date.parse(endDate);
        }
        upsert planAssoc planAssoc.AddOn_Unique_key__c;
        fetchPayPlans();
        selPlan = '';
    }

}