/**********************************************************************************************************************
Description: Apex controller for POP LWC component
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By   | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     | 27/07/2020        | Sayali Bhambure   | 1. Initial Draft
***********************************************************************************************************************/
public with sharing class GenericController {

    public static final String STATUS_REJECT = 'Reject';
    public static final String STATUS_REJECT_DUP = 'Reject-Duplicate';
    public static final String TASK_SUBJECT = 'Verify Proof of Payment Details in IPMS';
    public static final String USER_FINANCE = 'Finance';
    public static final String PROCESS_POP = 'POP';

    public GenericController() {

    }

    /**********************************************************************************************************************
    Description : Method to return SR attachments
    Parameter(s): String
    Return Type : list<SR_Attachments__c>
    **********************************************************************************************************************/
    @AuraEnabled
    public static list<SR_Attachments__c> returnFiles(String caseId) {
        return [SELECT Id
                    , Name
                    , Type__c
                    , Verified_By_Finance__c
                    , Attachment_URL__c
                    , Case__r.Status 
                FROM SR_Attachments__c 
                WHERE Case__c =:caseId];
    }

    /**********************************************************************************************************************
    Description : Method to sync uploaded docs to outlook and create SR attch
    Parameter(s): list<Object>, String
    Return Type : void
    **********************************************************************************************************************/
    @AuraEnabled
    public static void syncWithOutlook(list<Object> filesToInsert, String caseId){
        System.debug('filesToInsert>>>>>'+filesToInsert);
        List<String> docURLs = new List<String>();
        List<SR_Attachments__c> lstDocs = new List<SR_Attachments__c>();
        List<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocReq = new List<UploadMultipleDocController.MultipleDocRequest>();
        UploadMultipleDocController.data objResponseData = new UploadMultipleDocController.data();
        if(filesToInsert != null && filesToInsert.size() > 0) {
            for (Object file : filesToInsert) {
                if(file != null) {
                    FileInfo fileData = (FileInfo)JSON.deserialize(JSON.serialize(file), FileInfo.class);
                    UploadMultipleDocController.MultipleDocRequest objMultipleDocRequest = new UploadMultipleDocController.MultipleDocRequest();
                    objMultipleDocRequest.category = 'Document';
                    objMultipleDocRequest.entityName = 'POP SR';
                    objMultipleDocRequest.base64Binary = EncodingUtil.base64Encode(fileData.VersionData);
                    objMultipleDocRequest.fileDescription = fileData.Title;
                    objMultipleDocRequest.fileId = fileData.Title;
                    objMultipleDocRequest.fileName = fileData.Title;
                    objMultipleDocRequest.registrationId = fileData.Title;
                    objMultipleDocRequest.sourceId = fileData.Title;
                    objMultipleDocRequest.sourceFileName = fileData.Title;
                    lstMultipleDocReq.add(objMultipleDocRequest);
                }                
            }
        }
                
        if (lstMultipleDocReq.size() > 0) {            
            objResponseData = UploadMultipleDocController.getMultipleDocUrl(lstMultipleDocReq);           
        }
        if(objResponseData != null && objResponseData.status == 'S' 
            && objResponseData.Data != null 
            && objResponseData.Data.size() > 0) {
            for(UploadMultipleDocController.MultipleDocResponse objData : objResponseData.Data) {
                docURLs.add(objData.url);
                SR_Attachments__c objAttach = new SR_Attachments__c ();
                objAttach.Case__c = caseId;
                objAttach.Attachment_URL__c = objData.url;
                String[] fullName = objData.PARAM_ID.split('seperator');
                objAttach.Name = fullName[1];
                objAttach.isValid__c = true;
                objAttach.Type__c = fullName[0];
                lstDocs.add(objAttach);
            }
            if(!lstDocs.isEmpty()) {
                insert lstDocs;
            }           
        }       
    }

    /**********************************************************************************************************************
    Description : Method to reject task
    Parameter(s): String, String
    Return Type : void
    **********************************************************************************************************************/
    @AuraEnabled
    public static void rejectTask(String caseId, String rejectReason) {
        List<String> statuses = new List<String> { STATUS_REJECT , STATUS_REJECT_DUP };
        List<Task> objTask = [SELECT Id
                                    , WhatId
                                    , Subject
                                    , Assigned_User__c
                                    , Process_Name__c
                                    , Status,Notes__c,Description
                                FROM TASK 
                                WHERE WhatId = :caseId 
                                AND Subject = :TASK_SUBJECT 
                                AND Assigned_User__c = :USER_FINANCE 
                                AND Process_Name__c = :PROCESS_POP 
                                AND Status NOT IN :statuses 
                                ORDER BY CreatedDate DESC];
        
        if(!objTask.isEmpty()) {
            if(rejectReason == 'Duplicate') {
                objTask[0].Status = STATUS_REJECT_DUP;
            }
            else {
                objTask[0].Status = STATUS_REJECT;
            }            
            objTask[0].Description = rejectReason;
            objTask[0].Notes__c = rejectReason;        
            update objTask[0];
        }
    }

    /**********************************************************************************************************************
    Description : Method to update SR attachment
    Parameter(s): List<String>
    Return Type : void
    **********************************************************************************************************************/
    @AuraEnabled
    public static void updateSRAttachemnt(List<String> sRIds) {
        Set<String> docIds = new Set<String>();
        docIds.addAll(sRIds);
        List<SR_Attachments__c> updateList = [SELECT Id, Name, Type__c 
                                                FROM SR_Attachments__c 
                                                WHERE Id IN :docIds];
        if(!updateList.isEmpty()) {
            for(SR_Attachments__c srDoc :updateList) {
                srDoc.Verified_By_Finance__c = true;
            }        
            update updateList;
        }
        
    }
    
    
        /**********************************************************************************************************************
    Description : Method to update SR attachment
    Parameter(s): List<String>
    Return Type : void
    **********************************************************************************************************************/
    @AuraEnabled
    public static void updateCaseStaus(Boolean isErrror, String buttonName, String caseId) {
        List<Case> updatedCaseList = new List<Case>();
        //docIds.addAll(sRIds);
        List<Case> updateList = [SELECT Id, Status
                                 FROM Case
                                 WHERE Id =:caseId];
        if(!updateList.isEmpty()) {
           for(Case objCase : updateList) {
               if(isErrror == true) {
                    
                    if(buttonName == 'subBtn'){
                        objCase.Status = 'Draft';
                    }
                }
                else {
                    
                    if(buttonName == 'subBtn'){
                        objCase.Status = 'Submitted';
                    }
                }
                updatedCaseList.add(objCase);
           }
        }
        
        if(!updatedCaseList.isEmpty()) {
            update updatedCaseList;
        }
        
    }

    // Pass in the endpoint to be used using the string url
    public static Blob getCalloutResponseContents(String url) {
    
        // Instantiate a new http object
        Http h = new Http();
    
        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('GET');
    
        // Send the request, and return a response
        HttpResponse res = h.send(req);
        return res.getBodyAsBlob();
    }

    public class FileInfo {
        public String Title;
        public Blob VersionData;
    }

    public class JSON2ApexWrapper {
        public OutputParameters OutputParameters;
    }

    public class OutputParameters {
        public String xmlns;
        public String xmlnsxsi;
        public X_RECEIPTS_DTL_TAB X_RECEIPTS_DTL_TAB;
        public String X_RETURN_STATUS;
        public String X_RETURN_MESSAGE;
    }
    
    public class X_RECEIPTS_DTL_TAB {
        public List<X_RECEIPTS_DTL_TAB_ITEM> X_RECEIPTS_DTL_TAB_ITEM;
    }
    
    public class X_RECEIPTS_DTL_TAB_ITEM {
        public String DOC_SEQUENCE_VALUE;
        public String RECEIPT_NUMBER;
        public String RECEIPT_AMOUNT;
        public String RECEIPT_DATE;
        public String REMARKS;
        public String BANK_REF_NUMBER;
        public String BANK_ACCOUNT_NUM;
        public String CASH_RECEIPT_ID;
    }
    
    //wrapper for approval response
    public class JSON2ApexApprovalResponse { 
        public OutputParametersResp OutputParameters;
    }

    public class P_RECEPT_OUT_REC {
        public List<P_RECEPT_OUT_REC_ITEM> P_RECEPT_OUT_REC_ITEM;
    }

    public class P_RECEPT_OUT_REC_ITEM {
        public String X_CASH_RECEPT_ID;
        public String X_URL;
        public String X_STATUS;
        public String X_ERROR_DESC;
    }

    public class OutputParametersResp {
        public String appxmlns;
        public String appxmlnsxsi;
        public P_RECEPT_OUT_REC P_RECEPT_OUT_REC;
        public String X_RETURN_STATUS;
        public String X_RETURN_MESSAGE;
    }
}