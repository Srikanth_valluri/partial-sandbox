public without sharing class AssignmentCancellationController {
    String caseId;
    public String errorMessage ;
    public AssignmentCancellationController(ApexPages.StandardController sc){
        caseId = sc.getId();
    }
    
    public AssignmentCancellationController( Id caseIdTemp ) {
      caseId = caseIdTemp ;
      errorMessage = '';
    }
    
    public pagereference checkEligibility(){
        PageReference pgRef;
        list<Case> lstCase = [Select Id, CaseNumber, Buyer__c from Case 
                              where Id =: caseId and (Status = 'Closed'
                              or Status = 'Rejected' or Status = 'Assignment Completed')];
        system.debug('lstCase *************'+lstCase);
        if(lstCase != null && !lstCase.isEmpty()){
            errorMessage = 'Closed or Rejected cases are ineligible for cancellation' ;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Closed or Rejected cases are ineligible for cancellation');
            ApexPages.addMessage(myMsg);
            return null;
        }else{
            list<Case> lstC = [Select Id
                                    , CaseNumber
                                    , Booking_Unit__c
                                    , Booking_Unit__r.Registration_Id__c
                                    , Booking_Unit__r.NOC_Issued_Date__c
                                    , Booking_Unit__r.NOC_Re_Issued_Date__c
                               from Case where Id =:caseId];
            if(lstC != null && !lstC.isEmpty()){
                list<Error_Log__c> listErrorLog = new list<Error_Log__c>();
                map<String, Object> mapDeserializeRegStatus = new map<String, Object>();
                AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint objClass = new AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint();
                objClass.timeout_x = 120000;
                String strRegStatusResponse = '';
                if(!Test.isRunningTest()){
                    strRegStatusResponse = objClass.UpdateRegistrationStatus('2-'+String.valueOf(System.currentTimeMillis()), 'UPDATE_REG_STATUS', 'SFDC', lstC[0].Booking_Unit__r.Registration_ID__c, 'RA');
                }else{
                    strRegStatusResponse = '{"Status":"S","Param_ID":"100922", "PROC_STATUS":"S", "PROC_MESSAGE":"Process Completed Successfully","Availability":"null", "Amount":"null"}';
                }
                system.debug('strRegStatusResponse==='+strRegStatusResponse);
                if(String.isNotBlank(strRegStatusResponse)) {
                    mapDeserializeRegStatus = (map<String, Object>)JSON.deserializeUntyped(strRegStatusResponse);
                    if(mapDeserializeRegStatus.get('PROC_STATUS') != 'S') {
                        errorMessage = 'Error from IPMS for Status Update RA';
                        Error_Log__c objErr = createErrorLogRecord(null,lstC[0].Booking_Unit__c,caseId);
                        objErr.Error_Details__c = 'Error from IPMS for Status Update RA '+mapDeserializeRegStatus.get('message');
                        listErrorLog.add(objErr);
                    } else if(mapDeserializeRegStatus.get('PROC_STATUS') == 'S') { 
                        Case objC = new Case();
                        objC.Id = caseId;
                        objC.Status = 'Cancelled';
                        objC.IsCancelled__c = true;
                        update objC;
                        
                        if(lstC[0].Booking_Unit__c != null) {
                            Booking_Unit__c objBookingUnit = new Booking_Unit__c();
                            objBookingUnit.Id = lstC[0].Booking_Unit__c;
                            objBookingUnit.NOC_Issued_Date__c = null;
                            objBookingUnit.NOC_Re_Issued_Date__c = null;
                            objBookingUnit.Registration_Status__c = 'Agreement Executed by DAMAC';
                            objBookingUnit.Contract_Status__c = 'Agreement Executed by DAMAC';
                            objBookingUnit.Status__c = 'Agreement Executed by DAMAC';
                            objBookingUnit.Re_Assigned__c = false;
                            update objBookingUnit;
                        }
                        list<Task> lstTaskToUpdate = new list<Task>();
                        for(Task objT : [Select t.WhatId, t.Status, t.Id From Task t where t.WhatId =: caseId AND IsClosed = false]){
                            objT.Status = 'Cancelled';
                            lstTaskToUpdate.add(objT);
                        }
                        if(!lstTaskToUpdate.isEmpty()){
                            update lstTaskToUpdate;
                        }               
                    }
                }
                else {
                    errorMessage = 'Error : No Response from IPMS for Status Update RA';
                    Error_Log__c objErr = createErrorLogRecord(null,lstC[0].Booking_Unit__c, caseId);
                    objErr.Error_Details__c = 'Error : No Response from IPMS for Status Update RA';
                    listErrorLog.add(objErr);
                }
            }
            pgRef = new PageReference('/'+caseId);
        }
        return pgRef;
    }
   
    
    public static void insertErrorLog(list<Error_Log__c> listObjErr){
        try{
            insert listObjErr;
        }catch(Exception ex){
            //errorMessage = 'Error : '+ ex.getMessage();
            system.debug('Error Log ex*****'+ex);
        }
    }
    
    public static Error_Log__c createErrorLogRecord(Id accId, Id bookingUnitId, Id caseId){
        Error_Log__c objErr = new Error_Log__c();
        objErr.Account__c = accId;
        objErr.Booking_Unit__c = bookingUnitId;
        objErr.Case__c = caseId;
        objErr.Process_Name__c = 'Assignment';
        return objErr;
    }
    
    public class ObjWrapper {
        public String PARAM_ID {get;set;}
        public String PROC_MESSAGE {get;set;}
        public String PROC_STATUS {get;set;}
        
        public objWrapper(){

        }
    }
} // end of class