@isTest
private class OffReadyMorgageSRTest
{
	private static Id getRecordTypeId(){
		return Schema.SObjectType.Case.getRecordTypeInfosByName().get('Mortgage').getRecordTypeId();
	}
	
	@isTest static void itShould() {
		NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);

        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
         
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;

        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id; 
        BU.Registration_ID__c = '123135'; 
    	BU.Seller_Name__c = 'test';
		BU.Unit_Location_AR__c = 'test';
		BU.Plot_Number__c = 'test';
		BU.Property_Name__c = 'test';
		BU.Property_Name_Arabic__c = 'test';
		BU.Seller_Name_AR__c = 'test';
		BU.Requested_Price__c = 435531;
        insert BU; 

        Buyer__c buyer =  new Buyer__c();
        buyer.Account__c  = Acc.Id;
        buyer.Booking__c   = Booking.Id;
        buyer.Primary_Buyer__c   = false;
        buyer.First_Name__c  = 'Test';      
        buyer.Last_Name__c  = 'Test Last';
        buyer.IPMS_Registration_ID__c ='123456';
        insert buyer;

        Case caseObj = TestDataFactory_CRM.createCase(Acc.Id, getRecordTypeId());
        caseObj.Booking_Unit__c = BU.Id;
        caseObj.Type = 'Mortgage';
        caseObj.Status = 'Submited';
        insert caseObj;

        Case caseObj2 = TestDataFactory_CRM.createCase(Acc.Id, getRecordTypeId());
        caseObj2.Booking_Unit__c = null;
        caseObj2.Type = 'Mortgage';
        caseObj2.Status = 'Submited';
		insert caseObj2;

		OffReadyMorgageSR.OffReadyPlan( new List<Id>{ caseObj.Id } );
		OffReadyMorgageSR.OffReadyPlan( new List<Id>{ caseObj2.Id} );
	}
}