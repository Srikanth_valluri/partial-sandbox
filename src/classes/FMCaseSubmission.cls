public without sharing class FMCaseSubmission {

    @InvocableMethod(label='' description='')
    public static void caseSubmitted (List<Id> lstIDs) {
        //TODO: update method return type and input parameters (they do need to be List)
        System.debug('lstIDs--------'+lstIDs);
        Set<String> setLocationCode=new Set<String>();
        Set<Id> setLocationIds=new Set<Id>();

        List<FM_Case__c> lstFmCase = new List<FM_Case__c>();
        for (FM_Case__c fmCase : [
                SELECT  Id, Unit_Name__c, Booking_Unit__c, Booking_Unit__r.Inventory__c, OwnerId, CurrencyIsoCode,
                        Booking_Unit__r.Inventory__r.Building_Location__c, Request_Type__c, Submitted__c
                FROM    FM_Case__c
                WHERE   Id IN :lstIDs
                    AND (Request_Type__c NOT IN (
                            'Proof Of Payment', 'Change of Contact Details',
                            'Name Nationality Change', 'Passport Detail Update'
                        ) OR (Request_Type__c = 'Proof Of Payment' AND Origin__c = 'Portal'))
         ]) {
            if (fmCase.Booking_Unit__c != NULL
                && fmCase.Booking_Unit__r.Inventory__c != NULL
                && fmCase.Booking_Unit__r.Inventory__r.Building_Location__c != NULL
            ) {
                setLocationIds.add(fmCase.Booking_Unit__r.Inventory__r.Building_Location__c);
            }
            lstFmCase.add(fmCase);
        }

        System.debug('lstFmCase = ' + lstFmCase);

        System.debug('setLocationIds = ' + setLocationIds);
        Map<Id, Map<String, Id>> mapBuildingToRolesToUserId = FM_Utility.getFmUsersByLocationAndRole(setLocationIds);
        System.debug('mapBuildingToRolesToUserId = ' + mapBuildingToRolesToUserId);


        List<Task> lstTask = new List<Task>();

        for (FM_Case__c fmCase : lstFmCase) {

            Task taskInstance = new Task();
            taskInstance.Priority = 'High';
            taskInstance.WhatId = fmCase.Id;
            taskInstance.Status = 'Not Started';
            taskInstance.Process_Name__c = fmCase.Request_Type__c;
            taskInstance.Subject = Label.Subject_For_Admin_Task;
            taskInstance.ActivityDate = Date.today();


            if ('Proof Of Payment'.equalsIgnoreCase(fmCase.Request_Type__c)) {
                //taskInstance.OwnerId = fmCollectionId;
                if (!'NULL'.equalsIgnoreCase(Label.FMCollectionQueueDefaultOwner)) {
                    taskInstance.OwnerId = Label.FMCollectionQueueDefaultOwner;
                }
                taskInstance.Assigned_User__c = 'FM Finance';
                taskInstance.CurrencyIsoCode = fmCase.CurrencyIsoCode;
                taskInstance.Priority = 'Normal';
                taskInstance.Subject = Label.POP_Task_Subject_FM_Finance;
                taskInstance.Type = 'Portal';
            }

            if (fmCase.Booking_Unit__c != NULL
                && fmCase.Booking_Unit__r.Inventory__c != NULL
                && fmCase.Booking_Unit__r.Inventory__r.Building_Location__c != NULL
            ) {

                //Map<String, Id> mapFmUserRoleToUserId = mapBuildingToRolesToUserId.get(
                //    fmCase.Booking_Unit__r.Inventory__r.Building_Location__c
                //);

                //System.debug('mapFmUserRoleToUserId = ' + mapFmUserRoleToUserId);

                //Id fmAdminId, propertyManagerId;

                //if (mapFmUserRoleToUserId != NULL) {
                //    fmAdminId = mapFmUserRoleToUserId.get('FM Admin');
                //    propertyManagerId = mapFmUserRoleToUserId.get('Property Manager');
                //}

                //System.debug('fmAdminId = ' + fmAdminId);
                //System.debug('propertyManagerId = ' + propertyManagerId);

                System.debug('fmCase.Request_Type__c = ' + fmCase.Request_Type__c);
                System.debug('fmCase.Submitted__c = ' + fmCase.Submitted__c);

                if ('Service Charge Enquiry'.equalsIgnoreCase(fmCase.Request_Type__c)
                    && fmCase.Submitted__c == true
                ) {
                    //taskInstance.OwnerId = fmCollectionId;
                    taskInstance.OwnerId = fmCase.OwnerId;
                    taskInstance.Assigned_User__c = 'FM Collection';
                } else if (('Suggestion'.equalsIgnoreCase(fmCase.Request_Type__c)
                    || 'Appointment'.equalsIgnoreCase(fmCase.Request_Type__c))
                    && fmCase.Submitted__c == true
                    /*&& propertyManagerId != NULL*/
                ) {
                    taskInstance.OwnerId = fmCase.OwnerId;
                    taskInstance.Assigned_User__c = 'Property Manager';
                } else if(fmCase.OwnerId != NULL) {
                    taskInstance.OwnerId = fmCase.OwnerId;
                    taskInstance.Assigned_User__c = 'FM Admin';
                }
            }

            if (taskInstance.OwnerId == NULL) {
                taskInstance.OwnerId = Label.FMDefaultCaseOwnerId;
            }

            System.debug('taskInstance--------'+taskInstance);
            lstTask.add(taskInstance);
        }

        System.debug('lstTask--------'+lstTask);

        insert lstTask;

    }
}