@isTest
private class AccountBulkSoaButtonControllerTest {

    @TestSetup
    static void customSettingData() {

        insert new IpmsRestServices__c(
                   SetupOwnerId = UserInfo.getOrganizationId(),
                   BaseUrl__c = 'http://83.111.194.181:8045/webservices/rest',
                   Username__c = 'oracle_user',
                   Password__c = 'crp1user',
                   Client_Id__c = '8MABLQM-KJ8I8-1XA58-WWCM1S1',
                   BearerToken__c = 'eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWyiwuVrJSKs5IBKK8bCUdpczEEiUrQ1MzAwtTMwsDIx2l1IoCqIC5oRFIoLQ4tSgvMTcVqM_C19HJJ9BX19vLwtNC1zDC0dRCNzzc2dcw2FCpFgBXRb-1XQAAAA.6Ym224Vwr9AniBeq6gL8OM9u4vGnUB_vbEUVjWojg14',
                   Timeout__c = 120000
                   );


    }

    @isTest
    static void getBulkSoaUrlNullTest() {
        Account account = new Account(
                          Name = 'Test Account',
                          Party_ID__c = '63062');
        insert account;
        String blobStr = 'Test Blob Response';

        FmHttpCalloutMock.Response bulkSoaMockResponse = new FmHttpCalloutMock.Response(200, 'Success', '   {  '  +
         '       "responseId": "3072019-10305298",  '  +
         '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         '       "status": "S",  '  +
         '       "responseMessage": "Process Completed",  '  +
         '       "elapsedTimeMs": 8497,  '  +
         '       "responseLines": [  '  +
         '           {  '  +
         '               "documentName": "DPSOA",  '  +
         '               "language": "EN",  '  +
         '               "url": " "  '  +
         '           },  '  +
         '           {  '  +
         '               "documentName": "DPSOA_AR",  '  +
         '               "language": "AR",  '  +
         '               "url": " "  '  +
         '           }  '  +
         '       ],  '  +
         '       "complete": true  '  +
         '  }  ');

        FmHttpCalloutMock.Response blobResponse = new FmHttpCalloutMock.Response(200, 'Success', blobStr);

        String BULK_SOA_str = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.BULK_SOA, new List<String> {'63062'});

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
            BULK_SOA_str => bulkSoaMockResponse,
            ' ' => blobResponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));

        Test.startTest();
            PageReference pgRef = Page.accountBulkSoaButton;
            Test.setCurrentPage(pgRef);
            pgRef.getParameters().put('id', account.Id);
            ApexPages.StandardController sc = new ApexPages.standardController(account);

            AccountBulkSoaButtonController obj = new AccountBulkSoaButtonController(sc);
            obj.getBulkSoaUrl();
        Test.stopTest();
    }

    @isTest
    static void getBulkSoaUrlTest() {

        Account account = new Account(
                          Name = 'Test Account',
                          Party_ID__c = '63062');
        insert account;
        String blobStr = 'Test Blob Response';

        FmHttpCalloutMock.Response bulkSoaMockResponse = new FmHttpCalloutMock.Response(200, 'Success', '   {  '  +
         '       "responseId": "3072019-10305298",  '  +
         '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         '       "status": "S",  '  +
         '       "responseMessage": "Process Completed",  '  +
         '       "elapsedTimeMs": 8497,  '  +
         '       "responseLines": [  '  +
         '           {  '  +
         '               "documentName": "DPSOA",  '  +
         '               "language": "EN",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
         '           },  '  +
         '           {  '  +
         '               "documentName": "DPSOA_AR",  '  +
         '               "language": "AR",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
         '           }  '  +
         '       ],  '  +
         '       "complete": true  '  +
         '  }  ');

        FmHttpCalloutMock.Response blobResponse = new FmHttpCalloutMock.Response(200, 'Success', blobStr);

        String BULK_SOA_LANG_str = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.BULK_SOA, new List<String> {'63062'});

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
            BULK_SOA_LANG_str => bulkSoaMockResponse,
            'https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a' => blobResponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));

        Test.startTest();
            PageReference pgRef = Page.accountBulkSoaButton;
            Test.setCurrentPage(pgRef);
            pgRef.getParameters().put('id', account.Id);
            ApexPages.StandardController sc = new ApexPages.standardController(account);

            AccountBulkSoaButtonController obj = new AccountBulkSoaButtonController(sc);
            obj.getBulkSoaUrl();
        Test.stopTest();
    }

}