Public class Damac_VehicleLocationJSON_RESP{
    
    public Integer status;  //200
    public cls_results[] results;
    public String message;  //
    //public class cls_VehicleGroupName {
    //}
    public class cls_results {
        public String IMEINo;   //861901228004454
        public String TimeStamp;    //2019-05-01T18:12:07+04:00
        public Double Latitude; //25.142903333333333
        public Double Longitude;    //55.220083333333335
        public Integer Angle;   //30
        public Integer Speed;   //20
        public boolean IsEngineOn;
        public Integer TotalMilage; //3632000
        public String VehicleID;    //50857cd6-f884-4707-aee3-ef5980f76f6a
        public String VehicleName;  //N 49213
        public String VehicleGroupID;   //00000000-0000-0000-0000-000000000000
        public string VehicleGroupName;
        public String DriverID; //00000000-0000-0000-0000-000000000000
        public String Driver;   //
        public String CurrentLocation;  //
        public Integer FuelLevel;   //0
        public Integer IsSeatBeltOn;    //0
        public Integer IsDoorOpen;  //0
        public Integer IsAcOn;  //0
        public Integer IsEquipmentOn;   //0
        public Integer EngineTemp;  //0
        public Integer EngineRpm;   //0
        public String InStatusFrom; //2019-05-01T18:07:07+04:00
        public Integer TrackingStatus;  //1
    }
    public static Damac_VehicleLocationJSON_RESP parse(String json){
        return (Damac_VehicleLocationJSON_RESP) System.JSON.deserialize(json, Damac_VehicleLocationJSON_RESP.class);
    }
}