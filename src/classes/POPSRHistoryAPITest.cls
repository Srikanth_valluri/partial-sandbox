@isTest
public class POPSRHistoryAPITest {
    @TestSetup
    static void TestData() {
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(Agency_ID__c = '1234');
        insert sr;
        
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;
        
        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];
        
        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Customer Community Login User(Use this)%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
        Database.insert(user1);
        
        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'test';
        locObj.Location_Type__c = 'Building';
        locObj.Location_ID__c = '12345';
        locObj.UAEProp__c = true;
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        invObj.Property_Country__c = 'UNITED ARAB EMIRATES';
        invObj.Marketing_Name_Doc__c = invObj.Id;
        invObj.Building_Location__c = locObj.Id;
        invObj.Property_Status__c = 'Ready'; 
        invObj.Unit_type__c = 'Villa';
        insert invObj;
        
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        Booking_Unit__c bookingUnit1 = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1204',
                                      Registration_ID__c = '3901',
                                      Handover_Flag__c = 'N',
                                      Inventory__c = invObj.id,
                                      Dummy_Booking_Unit__c = true);
        bookingUnitList.add(bookingUnit1);
        Booking_Unit__c bookingUnit2 = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1205',
                                      Registration_ID__c = '3902',
                                      Handover_Flag__c = 'Y',
                                      Inventory__c = invObj.id,
                                      Dummy_Booking_Unit__c = true);
        bookingUnitList.add(bookingUnit2);
        
        insert bookingUnitList;        
    }
    
    @isTest
    static void testPopHistoryInstallment() {
        Account objAcc = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c objBU1 = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        
        Id rtPopId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POP').getRecordTypeId();
        Case objPopCase = new Case();
        objPopCase.Origin = 'Portal';
        objPopCase.isHelloDamacAppCase__c = true;
        objPopCase.AccountId = objAcc.Id;
        objPopCase.RecordtypeId = rtPopId;
        objPopCase.Status = 'Draft Request';
        objPopCase.SR_Type__c = 'Proof of Payment SR';
        objPopCase.Booking_Unit__c = objBU1.Id;
        objPopCase.Payment_Date__c = System.today();
        objPopCase.Payment_Mode__c = 'Bank Transfer';
        objPopCase.Payment_Currency__c = 'AED';
        objPopCase.Total_Amount__c = 100;
        
        Test.startTest();
        
        insert objPopCase;
        
        SR_Booking_Unit__c srBookingUnit = new SR_Booking_Unit__c();
        srBookingUnit.Booking_Unit__c =  objBU1.Id;
        srBookingUnit.Case__c = objPopCase.Id;
        insert srBookingUnit;
        
        SR_Attachments__c srAttach = new SR_Attachments__c();
        srAttach.Name = 'Test';
        srAttach.Attachment_URL__c = 'http://damacproperties.force.com/Documents/apex/GetFile?id=01NUEFKAW4NVD35H4Z7VEKEMYYSTVGULRT';
        srAttach.Type__c = 'POP';
        srAttach.Case__c = objPopCase.Id;
        insert srAttach;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/popSrHistory';
        req.addParameter('accountId',objAcc.Id);
        req.addParameter('popType','installment');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        POPSRHistoryAPI.getPopSRHistory();
        
        Test.stopTest();
    }
    
    @isTest
    static void testPopHistoryServiceCharge() {
        Account objAcc = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c objBU2 = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3902'];
        
        Id rtPopId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Proof Of Payment').getRecordTypeId();
        FM_Case__c objPopFmCase = new FM_Case__c();
        objPopFmCase.Origin__c = 'Portal';
        objPopFmCase.isHelloDamacAppCase__c = true;
        objPopFmCase.Account__c = objAcc.Id;
        objPopFmCase.RecordtypeId = rtPopId;
        objPopFmCase.Status__c = 'Draft Request';
        objPopFmCase.Request_Type__c = 'Proof Of Payment';
        objPopFmCase.Booking_Unit__c = objBU2.Id;
        objPopFmCase.Payment_Date__c = System.today();
        objPopFmCase.Payment_Mode__c = 'Bank Transfer';
        objPopFmCase.Currency__c = 'AED';
        objPopFmCase.Total_Amount__c = 100;
        
        Test.startTest();
        
        insert objPopFmCase;
        
        SR_Booking_Unit__c srBookingUnit = new SR_Booking_Unit__c();
        srBookingUnit.Booking_Unit__c =  objBU2.Id;
        srBookingUnit.FM_Case__c = objPopFmCase.Id;
        insert srBookingUnit;
        
        SR_Attachments__c srAttach = new SR_Attachments__c();
        srAttach.Name = 'Test';
        srAttach.Attachment_URL__c = 'http://damacproperties.force.com/Documents/apex/GetFile?id=01NUEFKAW4NVD35H4Z7VEKEMYYSTVGULRT';
        srAttach.Type__c = 'POP';
        srAttach.FM_Case__c = objPopFmCase.Id;
        insert srAttach;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/popSrHistory';
        req.addParameter('accountId',objAcc.Id);
        req.addParameter('popType','service_charge');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        POPSRHistoryAPI.getPopSRHistory();
        
        Test.stopTest();
    }
    
    @isTest
    static void testAccountIdNull() {
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/popSrHistory';
        req.addParameter('popType','service_charge');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        POPSRHistoryAPI.getPopSRHistory();
        
        Test.stopTest();
    }
    
    @isTest
    static void testAccountIdBlank() {
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/popSrHistory';
        req.addParameter('accountId','');
        req.addParameter('popType','service_charge');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        POPSRHistoryAPI.getPopSRHistory();
        
        Test.stopTest();
    }
    
    @isTest
    static void testPopTypeIdNull() {
        Account objAcc = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/popSrHistory';
        req.addParameter('accountId',objAcc.Id);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        POPSRHistoryAPI.getPopSRHistory();
        
        Test.stopTest();
    }
    
    @isTest
    static void testPopTypeIdBlank() {
        Account objAcc = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/popSrHistory';
        req.addParameter('accountId',objAcc.Id);
        req.addParameter('popType','');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        POPSRHistoryAPI.getPopSRHistory();
        
        Test.stopTest();
    }
}