/*----------------------------------------------------------------------
Description: Controller for move out request component

========================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
-----------------------------------------------------------------------
1.0     | 06-06-2018       | Lochana Rajput   | 1. Initial draft
-----------------------------------------------------------------------
2.0     | 18-07-2018       | Lochana Rajput   | 1. Added method to cancel SR
----------------------------------------------------------------------------------------------
3.0     | 24-12-2018       | Lochana Rajput   | 1. Added role for master community
=======================================================================
*/
global virtual without sharing class MoveOutSRComponentController {
    public FM_Case__c objFMCase{get;set;}
    public String docIdToDelete{get;set;}
    public String strDocBody {get;set;}
    public String strDocName {get;set;}
    // public String securityAmount {get;set;}
    public String unitName {get;set;}
    public Account objAcc{get;set;}
    public Boolean isTenant{get;set;}
    public Location__c objLoc{get;set;}
    public Integer noOfRequDocs{get;set;}
    public String strAccountId;
    public String strDate{get;set;}
    public String strSRType;
    public Boolean isResident{get;set;}
    @TestVisible public String strUnitId {get;set;}
    public Booking_Unit__c objBU {get;set;}
    public Set<String> uploadedDocLst;
    public List<FM_Documents__mdt> lstDocuments{get;set;}
    public list<SR_Attachments__c> uploadedDocuments{get;set;}
    private Id devRecordTypeId;
    public String caseId;
    public Boolean isEligible {get;set;}
    public Boolean isGasClearance {get;set;}

    public MoveOutSRComponentController() {
        isGasClearance = true;
        system.debug('in con');
        isEligible = true;
        objLoc = new Location__c();
        uploadedDocLst = new Set<String>();
        isTenant = false;
        objFMCase = new FM_Case__c();
        objFMCase.Email__c = 'info@apexservices.ae';
        devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Move Out').getRecordTypeId();
        noOfRequDocs =0;
        objBU = new Booking_Unit__c();
        uploadedDocuments = new list<SR_Attachments__c>();
        lstDocuments = new List<FM_Documents__mdt>();
        objAcc = new Account();
        strAccountId = ApexPages.currentPage().getParameters().get('AccountId');
        strSRType = ApexPages.currentPage().getParameters().get('SRType');
        strUnitId = ApexPages.currentPage().getParameters().get('UnitId');
        caseId = ApexPages.currentPage().getParameters().get('Id');
        checkExistingSubmittedCases();
        if(String.isNotBlank(caseId)) {
            System.debug('==caseId='+caseId);
            List<FM_Case__c> lstCase = [
                SELECT Name, Status__c,Id,
                        Company__c,
                        Reason_for_Move_Out__c,
                        Contractor__c,
                        Tenant__c,
                        Booking_Unit__c,
                        Mobile_no_contractor__c,
                        Expected_move_out_date__c
                FROM FM_Case__c
                WHERE Id=: caseId
            ];
            if (!lstCase.isEmpty()) {
                objFMCase = lstCase[0];
                strUnitId = objFMCase.Booking_Unit__c;
                strAccountId = objFMCase.Tenant__c;
                Integer day, month, year;
                strDate = '';
                if(objFMCase.Expected_move_out_date__c != NULL) {
                    day = objFMCase.Expected_move_out_date__c.day();
                    month = objFMCase.Expected_move_out_date__c.month();
                    year = objFMCase.Expected_move_out_date__c.year();
                    // strDate = objFMCase.Expected_move_out_date__c.format();
                    strDate = String.valueOf(day) + '/'+ String.valueOf(month) + '/'+String.valueOf(year);
                }

                //get previously uploaded documents
                uploadedDocuments = [SELECT Type__c,Name, View__c,Attachment_URL__c
                                                from SR_Attachments__c
                                                WHERE FM_Case__c =:objFMCase.Id];
                getUploadedDocuments();
            }
        }
        else {
            system.debug('in else');
            objFMCase.Raised_Date__c = System.now();
            objFMCase.Origin__c='Walk-In';
            objFMCase.Status__c='New';
            objFMCase.RecordTypeId = devRecordTypeId;//Set record type
        }
        //Get booking unit
        if(String.isNotBlank(strUnitId)) {
            objBU = [SELECT Id,
                            Owner__c,
                            Resident__c,
                            Account_Id__c,
                            Property_Name__c,
                            Booking__r.Account__c,
                            Booking__r.Account__r.Name,
                            Property_City__c,
                            Booking__r.Account__r.Mobile_Phone__pc,
                            Booking__r.Account__r.Email__pc,
                            Booking__r.Account__r.IsPersonAccount,
                            Tenant__c,
                            Tenant__r.Name,
                            Tenant__r.Mobile_Phone__pc,
                            Tenant__r.Email__pc,
                            Unit_Name__c
                    FROM Booking_Unit__c
                    WHERE Id =: strUnitId];
            system.debug('objBU==='+objBU);
            // isTenant = objBU.Owner__c == objBU.Resident__c ? false : true;
            isTenant = objBU.Booking__r.Account__c == objBU.Resident__c ? false : true;
            objFmCase.Initiated_by_tenant_owner__c = isTenant ? 'Tenant' : 'Owner';
            system.debug('objFmCase.Initiated_by_tenant_owner__c===='+objFmCase.Initiated_by_tenant_owner__c);
            unitName = objBU.Unit_Name__c;
            //Get community name from Location
            if(objBU != NULL) {
                if(objBU.Unit_Name__c != NULL) {
                    list<Location__c> lstLocs = new list<Location__c>();
                     lstLocs = [SELECT Community_Name__c,
                                      Move_In_Move_Out_security_cheque_amount__c,
                                      Name,Property_Name__r.Name
                                FROM Location__c
                                WHERE Name =: objBU.Unit_Name__c.substringBefore('/') LIMIT 1];
                    system.debug('lstLocs==='+lstLocs);
                    if(lstLocs.size() > 0) {
                        objFmCase.Property_Name1__c = objLoc.Property_Name__r.Name;
                        objLoc = lstLocs[0];
                        objFmCase.Building_Name__c = objLoc.Name;
                    }
                }
            }
        }
        //get account details
        if(String.isNotBlank(strAccountId)) {

            List<Account> lstAcc = new List<Account>();
            lstAcc = [SELECT Id, Party_ID__c,Name, Mobile_Phone__pc,Email__pc, isPersonAccount
                    FROM Account WHERE Id =: strAccountId];
            system.debug('lstAcc==='+lstAcc);
            if(lstAcc.size() >0) {
                objAcc = lstAcc[0];
            }
            system.debug('objAcc==='+objAcc);
        }
        getDocumentsList();//get documents
        objFMCase.Request_Type_DeveloperName__c = isTenant ? 'Move_out_Request_T' : 'Move_out_Request';
        if(objBU.Resident__c == objAcc.Id) {
            system.debug('oin ifbjAcc===');
            isResident=true;
        }
        else {
            isResident = false;
            isEligible = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                'You are not resident on the selected unit'));
        }
        System.debug('==isResidentisResident==='+isResident);
    }

    public String checkExistingSubmittedCases() {

        list<FM_Case__c> lstSubmittedCases = new list<FM_Case__c>();
        System.debug('==strUnitId==' + strUnitId);
        System.debug('==caseId==' + caseId);
        System.debug('==strAccountId==' + strAccountId);
        string urlParam;
        if(string.isNotBlank(strUnitId)){
            if(string.isNotBlank(caseId)){
                lstSubmittedCases = [SELECT id,
                                      name,
                                      Status__c,
                                      Request_Type__c,
                                      booking_unit__r.name,
                                      booking_unit__c
                               FROM FM_Case__c
                               WHERE RecordTypeId =: devRecordTypeId
                               AND Booking_Unit__c =: strUnitId
                               AND id !=: caseId
                               AND Status__c NOT IN ('Submitted','Closed')];
                urlParam = strUnitId;
            } else {
                /*lstSubmittedCases = [SELECT id,
                                      name,
                                      Status__c,
                                      Request_Type__c,
                                      booking_unit__r.name,
                                      booking_unit__c
                               FROM FM_Case__c
                               WHERE RecordTypeId =: devRecordTypeId
                               AND Booking_Unit__c =: strUnitId
                               AND (Status__c NOT IN ('Closed','Rejected', 'Cancelled')
                                    OR (Status__c = 'Closed'
                                        AND account__c =: strAccountId
                                       )
                                   )];*/
                lstSubmittedCases = [SELECT id,
                                            name,
                                            Status__c,
                                            Request_Type__c,
                                            booking_unit__r.name,
                                            booking_unit__c
                                    FROM FM_Case__c
                                    WHERE RecordTypeId =: devRecordTypeId
                                    AND Booking_Unit__c =: strUnitId
                                    //AND (Status__c  = 'Rejected' OR Status__c = 'Cancelled' //updated as Shashank verified from DAMAC
                                    AND (Status__c  = 'Submitted'
                                            OR (Status__c = 'Closed'
                                                AND account__c =: strAccountId
                                                AND Initiated_by_tenant_owner__c = 'Owner'
                                            )
                                            OR (Status__c = 'Closed'
                                                AND Tenant__c =: strAccountId
                                                AND Initiated_by_tenant_owner__c = 'Tenant'
                                            )
                                        )];
                urlParam = strUnitId;
            }

        }
          System.debug('==lstSubmittedCases==' + lstSubmittedCases);
          if(lstSubmittedCases != NULL && !lstSubmittedCases.isEmpty()){
              isEligible = false;
              string errorMsg = createAlreadySubmittedErrorMessage(lstSubmittedCases);
              System.debug('==errorMsg==' + errorMsg);
              //errorMsg = 'That child is too old for the class. Click <a href="www.company.com/policy.html">here</a> to review our policie';

              ApexPages.addmessage(new ApexPages.message(
                               ApexPages.severity.Error,errorMsg));
              return null;
          } else {
              return urlParam;
          }
    }

    public virtual String createAlreadySubmittedErrorMessage(List<FM_Case__c> lstSubmittedCases) {
        string errorMsg = 'Existing move out case for unit '+lstSubmittedCases[0].booking_unit__r.name+' available.<br/>';
        //errorMsg += '<table border="1" style="border-collapse: collapse"><caption><b>Following are the Submitted Cases</b></caption>\n<tr><th>Fm Case Number</th></tr>';

        for(FM_Case__c objCase: lstSubmittedCases){
            string FMCaseURL = '<a href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/'+objCase.id+'">'+objCase.name+'</a>';
            //errorMsg += '<tr><td>' + FMCaseURL + '</td></tr>';
            errorMsg += FMCaseURL+', ';
        }
        //errorMsg += '</table>';
        errorMsg = errorMsg.removeEnd(', ');
        errorMsg += '<br/>You will not be able to proceed further.';
        return errorMsg;
    }

    public list<FM_Case__c> getExistingCases() {

        list<FM_Case__c> lstFMCase = new list<FM_Case__c>();
        for (FM_Case__c objCase : [Select Id
                        , Name
                        , Status__c
                        , Account__c
                        , Tenant__c
                        , RecordTypeId
                     From FM_Case__c
                     Where Account__c =: strAccountId
                     And Status__c = 'Closed'
                     And RecordTypeId =: devRecordTypeId]){
            lstFMCase.add(objCase);
        }

        for (FM_Case__c objCase1 : [Select Id
                                        , Name
                                        , Status__c
                                        , Booking_Unit__c
                                        , RecordTypeId
                                    From FM_Case__c
                                    Where Booking_Unit__c =:strUnitId
                                    And Status__c != 'Closed'
                                    And RecordTypeId =: devRecordTypeId]){
            lstFMCase.add(objCase1);
        }
        return lstFMCase;
    }

    protected MoveOutSRComponentController(Boolean shouldCall) {}

    public void getUploadedDocuments() {
        uploadedDocLst = new Set<String>();
        for(SR_Attachments__c obj : uploadedDocuments) {
            uploadedDocLst.add(obj.Name);
        }
    }
    public void insertCase() {
        System.debug('==isEligible==='+isEligible);
        if(isEligible){
            try {
                objFMCase.Booking_Unit__c = objBU.Id;
                objFMCase.Tenant__c = objAcc.Id;
                // objFMCase.Account__c = objBU.Owner__c;
                objFMCase.Account__c = objBU.Booking__r.Account__c;
                upsert objFMCase;
                System.debug('==iobjFMCase==='+objFMCase);
            }
            catch(Exception excp) {
                system.debug('!!!!excp'+excp);
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                    excp.getMessage() +': ' + excp.getLineNumber()));
            }
        }
    }
    public virtual PageReference submit() {
        system.debug('!!!inside Submit'+objFMCase);
        objFMCase.Property_Name1__c = objBU.Property_Name__c;
        if(objFMCase.Id == NULL) {
            insertCase();
        }
        if(objFMCase.Id != NULL &&
            (objFMCase.Approval_Status__c == 'Approved' || objFMCase.Status__c == 'Closed')) {
          PageReference pageRef = new PageReference('/'+objFMCase.Id);
          return pageRef;
        }
        if(objFMCase.Move_in_move_out_type__c == 'Self move-out') {
            objFMCase.Company__c = objFMCase.Contractor__c = objFMCase.Mobile_no_contractor__c = '';
        }
        //Populate Admin from Locaion(FM User)
        Location__c objLoc = [SELECT Community_Name__c,
                                (SELECT FM_User__c,FM_User__r.Email,FM_Role__c,FM_User__r.Name
                                FROM FM_Users__r
                                /*WHERE FM_Role__c = 'FM Admin'*/)
                                    FROM Location__c
                                    WHERE Name =: objBU.Unit_Name__c.substringBefore('/') LIMIT 1];
        System.debug('==objLoc.FM_Users__r==' + objLoc.FM_Users__r);
        if(objLoc != NULL && objLoc.FM_Users__r.size() > 0) {
            for(FM_User__c obj: objLoc.FM_Users__r) {
                System.debug('==obj.FM_Role__c==' + obj.FM_Role__c);
                if(obj.FM_Role__c == 'FM Admin' || obj.FM_Role__c == 'Master Community Admin') {
                    objFMCase.Admin__c = obj.FM_User__c;
                    // break;
                }
                else if(obj.FM_Role__c == 'Property Manager'
                    || obj.FM_Role__c == 'Master Community Property Manager') {
                    objFMCase.Property_Manager_Email__c = obj.FM_User__r.Email;
                    objFMCase.Property_Manager_Name__c = obj.FM_User__r.Name;
                    // break;
                }
            }
        }
        objFMCase.Request_Type__c = 'Move Out';
        try{
            List<String> lstDate = strDate.split('/');
            System.debug('==lstDate==' + lstDate);
            System.debug('==strDate==' + strDate);
            objFMCase.Expected_move_out_date__c = Date.newinstance(Integer.valueOf(lstDate[2]),
            Integer.valueOf(lstDate[1]),Integer.valueOf(lstDate[0]));
            // objFMCase.Expected_move_out_date__c = Date.parse(strDate);
            list<FM_Approver__mdt> lstApprovers = FM_Utility.fetchApprovers(objFMCase.Request_Type_DeveloperName__c,objBU.Property_City__c);
            System.debug('==lstApprovers==' + lstApprovers);
            String approvingRoles = '';
            for(FM_Approver__mdt mdt : lstApprovers) {
                approvingRoles += mdt.Role__c + ',';
            }
            approvingRoles = approvingRoles.removeEnd(',');
            objFMCase.Approving_Authorities__c = approvingRoles;
            objFMCase.Booking_Unit__c = objBU.Id;
            objFMCase.Tenant__c = objAcc.Id;
            // objFMCase.Account__c = objBU.Owner__c;
            objFMCase.Account__c = objBU.Booking__r.Account__c;
            objFMCase.Status__c = 'Submitted';
            objFMCase.Email_2__c = Label.Customer_Happiness_Centre_email;
            objFMCase.Approval_Status__c='Pending';
            objFMCase.Tenant_Email__c = objAcc.Email__pc;
            objFMCase.Submit_for_Approval__c = true;
            objFMCase.Submitted__c = true;
            System.debug('=objFMCase==='+objFMCase);
            upsert objFMCase;
            return new PageReference('/' + objFMCase.Id);
        }
        catch(Exception excp) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                excp.getMessage() +': ' + excp.getLineNumber()));
        }
        return null;
    }

    public virtual PageReference saveAsDraft() {
        try{
            System.debug('==strDate==='+strDate);
            System.debug('==Move_in_move_out_type__c==' + objFMCase.Move_in_move_out_type__c);
            if(objFMCase.Move_in_move_out_type__c == 'Self move-out') {
                objFMCase.Company__c = objFMCase.Contractor__c = objFMCase.Mobile_no_contractor__c = '';
            }
            List<String> lstDate = strDate.split('/');
            objFMCase.Expected_move_out_date__c = Date.newinstance(Integer.valueOf(lstDate[2]),
            Integer.valueOf(lstDate[1]),Integer.valueOf(lstDate[0]));
            objFMCase.Status__c='New';
            // strDate = string.valueOfGmt(strDate);
            // objFMCase.Expected_move_out_date__c = Date.parse(strDate);
            upsert objFMCase;
            return new PageReference('/' + objFMCase.Id);
        }
        catch(Exception excp) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                excp.getMessage() +': ' + excp.getLineNumber()));
        }
        return null;
    }

    @future(callout=true)
    public static void apexcallout(String smsId) {
        String strUserName= Label.FM_SMS_Service_Username; //'loams.';
        String strPassword= Label.FM_SMS_Service_Password; //'1@#$%qwert';
        String strSID =  Label.FM_SMS_Service_SenderId; //'LOAMS';
        List<SMS_History__c> lstSMSHist = new List<SMS_History__c>();
        lstSMSHist = [SELECT Message__c, Phone_Number__c from SMS_History__c where Id=:smsId];
        SMS_History__c objSMSHist = new SMS_History__c();
        objSMSHist = lstSMSHist[0];
        System.debug('===objSMSHist=='+objSMSHist);
        
        String PhoneNumber = objSMSHist.Phone_Number__c;
        if(PhoneNumber.startsWith('00')) {
            PhoneNumber = PhoneNumber.removeStart('00');
        } else if(PhoneNumber.startsWith('0')) {
            PhoneNumber = PhoneNumber.removeStart('0');
        }

        strSID = SMSClass.getSenderName(strUserName, PhoneNumber, false);

        HttpRequest req = new HttpRequest();
        req.setMethod('POST' ); // Method Type
        req.setEndpoint('http://api.smscountry.com/SMSCwebservice_bulk.aspx');
        req.setBody('user='+ strUserName + '&passwd=' + strPassword +'&message=' 
        + GenericUtility.encodeChar(objSMSHist.Message__c)
        + '&mobilenumber=' + PhoneNumber + '&sid=' + strSID + '&MTYPE=LNG&DR=Y'); // Request Parameters

        Http http = new Http();
        try {
            HTTPResponse res = http.send( req );
            if( String.isNotBlank( res.getBody() ) ) {
                objSMSHist.Description__c = res.getBody();
                objSMSHist.Is_SMS_Sent__c = true ;
                 if(String.valueOf(res.getBody()).contains('OK:')){
                     objSMSHist.sms_Id__c = res.getBody().substringAfter('OK:');
                 }
                // return objSMSHist ;
            }
        }
        catch( Exception e ) {
            objSMSHist.Description__c = e.getMessage();
            objSMSHist.Is_SMS_Sent__c = true ;
            // return objSMSHist ;
        }
        update objSMSHist;
    }

    @InvocableMethod
    public static void sendSMS(List<SMS_History__c> lstSMSHist) {
        System.debug('===InvocableMethod====');
        System.debug('===lstSMSHist===='+lstSMSHist);
        // if(FMCaseTriggerHelper.fromTrigger == false)

        set<Id> setSMSHistoryIds = new set<Id>();
        for( SMS_History__c objSMS : lstSMSHist ) {
         setSMSHistoryIds.add( objSMS.Id );
        }

         list<SMS_History__c> lstSMSHistory = [ SELECT Id
                                                  FROM SMS_History__c
                                                 WHERE Id IN :setSMSHistoryIds
                                                   AND FM_Case__r.RecordType.DeveloperName IN ( 'Tenant_Registration', 'Tenant_Renewal' )
                                                   AND Is_SMS_Sent__c = false ];
         if( lstSMSHistory != NULL && !lstSMSHistory.isEmpty() ) {
            /*list<SMS_History__c> lstSMSHistoryToUpdate = new list<SMS_History__c>();
            for( SMS_History__c objSMS : lstSMSHistory ) {
                if( objSMS.Is_SMS_Sent__c == false && String.isNotBlank( objSMS.Message__c ) ) {
                    system.debug('== objSMS =='+objSMS);
                    lstSMSHistoryToUpdate.add( CustomerNotificationBatchHelper.sendSMSToCustomer( objSMS ) ) ;
                }
            }
            update lstSMSHistoryToUpdate ;*/

         }
         else {
          System.enqueueJob(new SendSMSQueueable(lstSMSHist[0].ID));
         }
    }
    public PageReference uploadDocument() {
        try{
            //if(objFMCase.Id == NULL) {
                 //insertCase();
            //}
            List<String> fileNames = Apexpages.currentPage().getParameters().get('fileName').split(',');
            System.debug('==fileNames==='+fileNames);
            System.debug('==strDocBody==='+strDocBody);
            System.debug('==strDocName==='+strDocName);
            if(String.isNotBlank(strDocBody) && String.isNotBlank(strDocName)) {
                System.debug('==strDocName==='+strDocName);
                List<UploadMultipleDocController.MultipleDocRequest> lstReq = new List<UploadMultipleDocController.MultipleDocRequest>();
                List<SR_Attachments__c> lstAttachments = new List<SR_Attachments__c>();
                SR_Attachments__c objAtt = new SR_Attachments__c();
                objAtt.Name = fileNames[0];//extractName(strDocName);
                objAtt.Type__c = fileNames.size() ==2 ? fileNames[1]: '';
                objAtt.FM_Case__c = objFMCase.Id;
                objAtt.Booking_Unit__c = objBU.Id;
                lstAttachments.add(objAtt);
                System.debug('==lstAttachments==='+lstAttachments);
                UploadMultipleDocController.MultipleDocRequest reqObj = new UploadMultipleDocController.MultipleDocRequest();
                reqObj.category = 'Document';
                reqObj.entityName = 'Damac Service Requests';

                blob objBlob = FM_Utility.extractBody(strDocBody);
                if(objBlob != null){
                    reqObj.base64Binary =  EncodingUtil.base64Encode(objBlob);
                }
                reqObj.fileDescription = fileNames[0];
                reqObj.fileId = objFMCase.Name + '-'+ String.valueOf(System.currentTimeMillis()) +'.'
                + strDocName.substringAfterLast('.');
                reqObj.fileName = objFMCase.Name + '-'
                            + String.valueOf(System.currentTimeMillis())  +'.'+ strDocName.substringAfterLast('.');
                reqObj.registrationId = objFMCase.Name;
                reqObj.sourceFileName = 'IPMS-'+objAcc.party_ID__C+'-'+FM_Utility.extractName(strDocName);
                reqObj.sourceId = 'IPMS-'+objAcc.party_ID__C+'-'+FM_Utility.extractName(strDocName);
                lstReq.add(reqObj);
                System.debug('==strDocName==='+strDocName);
                if(lstReq.size() > 0) {
                    UploadMultipleDocController.data respObj = new UploadMultipleDocController.data();
                    respObj= UploadMultipleDocController.getMultipleDocUrl(lstReq);
                    System.debug('===objData.PARAM_ID==' + respObj);
                    if(respObj != NULL && lstReq.size() > 0) {
                        if(respObj.status == 'Exception'){
                            ApexPages.addmessage(new ApexPages.message(
                                ApexPages.severity.Error,respObj.message));
                            // errorLogger(respObj.message, '', '');
                            return null;
                       }
                       System.debug('===lstAttachment==' + lstAttachments);
                       if(respObj.Data == null || respObj.Data.size()==0){
                           ApexPages.addmessage(new ApexPages.message(
                           ApexPages.severity.Error,'Problems while getting response from document upload'));
                           // errorLogger('Problems while getting response from document upload', '', '');
                           return null;
                       }
                       System.debug('===lstAttachment==' + lstAttachments);
                       Set<SR_Attachments__c> setSRAttachments_Valid = new Set<SR_Attachments__c>();
                        for(SR_Attachments__c att : lstAttachments) {
                            for(UploadMultipleDocController.MultipleDocResponse objData : respObj.data) {
                                System.debug('===objData.PARAM_ID==' + objData.PARAM_ID);
                                System.debug('===lstAttachments==' + 'IPMS-'+objAcc.party_ID__C+'-'+att.name);
                                if('IPMS-'+objAcc.party_ID__C+'-'+FM_Utility.extractName(strDocName) == objData.PARAM_ID){
                                   if(objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url)){
                                       att.Attachment_URL__c = objData.url;
                                       att.FM_Case__c = objFMCase.Id;
                                       setSRAttachments_Valid.add(att);
                                   }
                                   else{
                                       ApexPages.addmessage(new ApexPages.message(
                                       ApexPages.severity.Error,'Problems while getting response from document '+objData.PARAM_ID));
                                       // errorLogger('Problems while getting response from document '+objData.PARAM_ID, '', '');
                                       return null;
                                   }
                               }
                            }
                        }//outer for
                        List<SR_Attachments__c> lstAtt = new List<SR_Attachments__c>();

                        lstAtt.addAll(setSRAttachments_Valid);
                        for(SR_Attachments__c att : lstAtt) {
                            att.FM_Case__c = objFMCase.Id;
                        }
                        upsert lstAtt;
                        System.debug('===objFMCase.Id=='+objFMCase.Id);
                        uploadedDocuments = [SELECT Type__c,Name, View__c,Attachment_URL__c
                                            from SR_Attachments__c
                                            WHERE FM_Case__c =:objFMCase.Id];
                        System.debug('===uploadedDocuments=='+uploadedDocuments);
                        for(Integer i=0; i<lstDocuments.size(); i++) {
                            if(lstDocuments[i].Document_Name__c == fileNames[0]) {
                                lstDocuments.remove(i);
                                break;
                            }
                        }
                        // lstDocuments.remove(lstDocuments.indexOf(fileNames[0]));//removing uploaded doc placeholder
                }//res
            }//req
            }
        }//try
        catch(Exception excp) {
            System.debug('excp==='+excp);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                excp.getMessage() + excp.getLineNumber()));
        }
        strDocBody = '';
        return null;
    }

    public void deleteDocument() {
        try{
            //docIdToDelete contains Document id to delete and its name resp separated by ','
            List<String> tempLst = docIdToDelete.split(',');
            System.debug('tempLst===' + tempLst);
            delete [SELECT Id from SR_Attachments__c where Id=:tempLst[0]];
            uploadedDocuments = [SELECT Type__c,Name, View__c,Attachment_URL__c
                                FROM SR_Attachments__c
                                WHERE FM_Case__c =:objFMCase.Id];
            getUploadedDocuments();
            getDocumentsList();
        }
        catch(System.Exception excp) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                excp.getMessage() + excp.getLineNumber()));
        }
    }
    
    public virtual void gasClearanceNeeded() {
        
        isGasClearance = true;
    }

    public virtual void getDocumentsList() {
        try {
            System.debug('in getDocumentsList===');
            lstDocuments = new list<FM_Documents__mdt>();
            noOfRequDocs=0;
            String query = 'SELECT DeveloperName,MasterLabel,Mandatory__c,Type__c,Document_Name__c FROM FM_Documents__mdt ';
            query += 'WHERE Process__r.MasterLabel = \'Move out Request\' ';
            Set<String> cityValues = new Set<String>();
            cityValues.add('All');
            if(String.isNOTBlank(objBU.Property_City__c)) {
                cityValues.add(objBU.Property_City__c);
            }
            SYstem.debug('cityValues: '+cityValues);
            query += 'AND Is_Active__c = true AND City__c IN : cityValues ';
            query += 'AND Submitted_By__c = ';
            query += isTenant ? '\'Tenant\'' : '\'Owner\'';

            System.debug('isTenant: '+isTenant);
            System.debug('query ='+query);
            System.debug('===Database.query(query)==' +Database.query(query));
            for(FM_Documents__mdt mdt : Database.query(query)) {
                if(mdt.Mandatory__c) {
                    noOfRequDocs++;
                }
                if(!uploadedDocLst.contains(mdt.Document_Name__c)) {
                    lstDocuments.add(mdt);
                }
            }//for
            System.debug('===lstDocuments==' +lstDocuments);
        }
        catch(System.Exception excp) {
            System.debug('===excp==' +excp);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                excp.getMessage() + excp.getLineNumber()));
        }
    }

    public static List<FM_Case__c> cancelSRHelper(String FMCaseId){
        List<FM_Case__c> lstCases = new List<FM_Case__c>();
        try {
            //Recalling approval process
            lstCases = [SELECT Status__c, Id,
                                Request_Type_DeveloperName__c,
                                Booking_Unit__r.Property_City__c
                        FROM FM_Case__c WHERE Id=: FMCaseId];
            if(lstCases.size() > 0) {
                lstCases[0].Status__c = 'Cancelled';
                lstCases[0].Approval_Status__c='';
                lstCases[0].Submit_for_Approval__c = false;
                lstCases[0].Submitted__c = false;
            }
            List<ProcessInstanceWorkitem> piwi = [SELECT Id, ProcessInstanceId,
                                                        ProcessInstance.TargetObjectId
                                                    FROM ProcessInstanceWorkitem
                                                    WHERE ProcessInstance.TargetObjectId =: FMCaseId];
            System.debug('Target Object ID:' +piwi.size());
            if(piwi.size() > 0) {
                Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
                req.setComments('Recalling request');
                req.setAction('Removed');
                req.setWorkitemId(piwi.get(0).Id);
                req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
                Approval.ProcessResult result = Approval.process(req,false);
                System.debug('===result==' +result);
            }

            //Cancel tasks
            List<Task> lstTasks = new List<Task>();
            lstTasks = [SELECT Id, Status from Task WHERE WhatID =: FMCaseId];
            for(Task objTask : lstTasks) {
                objTask.Status = 'Cancelled';
            }
            update lstTasks;
        }
        catch(Exception excp) {
            System.debug('===excp==' +excp);
        }
        return lstCases;
    }

    webservice static void modifySR(String FMCaseId){
        //Recalling existing approval process and cancelling tasks
        List<FM_Case__c> lstCases = new List<FM_Case__c>();
        lstCases = cancelSRHelper(FMCaseId);
        //Submitting for new approval process
        if(lstCases.size() > 0) {
            list<FM_Approver__mdt> lstApprovers = FM_Utility.fetchApprovers(lstCases[0].Request_Type_DeveloperName__c,
                        lstCases[0].Booking_Unit__r.Property_City__c);
            System.debug('==lstApprovers==' + lstApprovers);
            String approvingRoles = '';
            for(FM_Approver__mdt mdt : lstApprovers) {
                approvingRoles += mdt.Role__c + ',';
            }
            approvingRoles = approvingRoles.removeEnd(',');
            lstCases[0].Approving_Authorities__c = approvingRoles;
            lstCases[0].Status__c = 'Submitted';
            lstCases[0].Approval_Status__c='Pending';
            lstCases[0].Submit_for_Approval__c = true;
            lstCases[0].Submitted__c = true;
            update lstCases;
        }
    }
    webservice static void cancelSR(String FMCaseId) {
        //Recalling existing approval process and cancelling tasks
        List<FM_Case__c> lstCases = new List<FM_Case__c>();
        lstCases = cancelSRHelper(FMCaseId);
        update lstCases;
    }
}