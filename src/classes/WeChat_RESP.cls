public class WeChat_RESP{
    public String res_trigger;  
    public cls_app app;
    public String version;
    public cls_messages[] messages;
    public cls_message message;
    public cls_appUser appUser;
    public class cls_app {
        public String res_id;   
    }
    public class cls_message {
        public String res_id;  //5c32f392afb43b00226570f8
    }
    public class cls_messages {
        public String text;
        public String type;
        public String mediaUrl;
        public String mediaType;
        public String role; 
        public Double received;
        public String name; 
        public String authorId;
        public String res_id;   
        public cls_coordinates coordinates;
        public cls_source source;
    }
    public class cls_coordinates {
        public Double res_lat;  
        public Double res_long; 
    }
    public class cls_source {
        public String type; 
        public String integrationId;
        public String id;   
    }
    public class cls_appUser {
        public String res_id;   
        public String userId;   
        public boolean conversationStarted;
    }
    public static WeChat_RESP parse(String json){
        return (WeChat_RESP) System.JSON.deserialize(json, WeChat_RESP.class);
    }

}