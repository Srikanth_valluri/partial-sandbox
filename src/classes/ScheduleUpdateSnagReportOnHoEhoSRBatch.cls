/*
 * Description : This class is used to Schedule UpdateSnagReportOnHoEhoSRBatch batch at every midnight
 * Revision History:
 *
 *  Version          Author              Date           Description
 *  1.0              Arjun Khatri        12/07/2018      Initial Draft
 *                                                      
 */
global class ScheduleUpdateSnagReportOnHoEhoSRBatch implements Schedulable {
  global void execute(SchedulableContext sc) {
      UpdateSnagReportOnHoEhoSRBatch batchInstance = new UpdateSnagReportOnHoEhoSRBatch(); 
      database.executebatch(batchInstance,1);
  }
}