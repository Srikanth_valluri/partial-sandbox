global without sharing class MakeDailerCallCntrl {

    
     
    global static void doPost() {
        
        //TODO added code to hide no. at hover 
        Id objId = string.isNotBlank(ApexPages.currentPage().getParameters().get('objId'))?
            ApexPages.currentPage().getParameters().get('objId') : '';
        system.debug('objId : ' + objId );
        string fieldToquery = string.isNotBlank(ApexPages.currentPage().getParameters().get('fieldToquery'))?
            ApexPages.currentPage().getParameters().get('fieldToquery'):'';
        system.debug('fieldToquery' + fieldToquery );
        String sObjName = objId.getsobjecttype().getDescribe().getName();
        system.debug('sObjName : ' + sObjName );
        String query ;
        SObject  obj;

        if(string.isNotBlank( sObjName ) && string.isNotBlank( objId )){
             query = 'SELECT Id,'+ fieldToquery +' FROM ' + sObjName + ' WHERE Id = \'' + objId + '\' LIMIT 1';
        }
        
        
        system.debug('query : ' + query );
        if(string.isNotBlank( query ))
            obj = Database.query(query);
        System.debug('obj: ='+obj);
        
        String numberToEncrtpt;
        
        
        if(obj != NULL ){
            numberToEncrtpt = String.valueOf(obj.get(fieldToquery));
            
        }
        
        String strenNO = EncrytNumber.encryptNo(numberToEncrtpt);
        
        System.debug('strenNO: ='+strenNO);
        //TODO added code to hide no. at hover 
        /**String objId = string.isNotBlank(ApexPages.currentPage().getParameters().get('CLId'))?
                    ApexPages.currentPage().getParameters().get('CLId') : '';
        
        Calling_List__c objCl = [select id,Encripted_Number__c from Calling_List__c Where Id =: objId LIMIT 1 ];
        System.debug('objCl: ='+objCl);
        **/
        
        
        //system.assert(false,'objCl: ='+objCl);
        if(String.isNotBlank(strenNO) && String.isNotBlank(objId) ){
            String call=    CdDailerRestService.getRequest(strenNO,objId);
            
            System.debug('call: ='+call);
        }
        
    }
    

    global Pagereference backToDetailPage(){
        
        pageReference pg = new pageReference('/'+ApexPages.currentPage().getParameters().get('ContactId'));
        pg.setRedirect(TRUE);
        return pg;
    }
}