public class Damac_TestEmailRequest
{
    public String emailVal { get; set; }
    public String message { get; set; }
    public Damac_TestEmailRequest (ApexPages.standardController stdController) {
        emailVal = '';
    }
    
    public void sendEmail () {
        if (emailVal != '') {
            message = '';
            ID emailReqId = apexpages.currentpage().getparameters().get('id');
            Email_Metrics__c metrics = new Email_Metrics__c ();
            metrics.Email_Request__c = emailReqId;
            metrics.Is_Test_Email__c = TRUE;
            metrics.Email__c = emailVal;
            insert metrics;
            
            emailVal = '';
            metrics = [SELECT Name FROM Email_Metrics__c where id =: metrics.ID];
            message = 'Email sent. '+metrics.Name;
        } else {
            message = 'Please enter the email address';
        }
    }
}