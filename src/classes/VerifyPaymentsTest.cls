@isTest
public class VerifyPaymentsTest{
    static List<Booking_Unit__c> lstBookingUnits;
    
    static void init(){
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest( );
        insert objSR ;
        system.debug('******SR Id*************'+objSR.Id);
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1);
        insert lstBookings ;
        
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        insert lstBookingUnits;
    }
    
    private static testMethod void successfullHandover(){
        init();
        Case objC = new Case();
        objC.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        objC.Origin = 'Web';
        objC.Status = 'New';
        objC.Document_Verified__c = true;
        objC.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objC;
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        list<Id> ids = new list<Id>();
        ids.add(objC.Id);
        VerifyPayments.checkPendingAmountInIPMS(ids);
        Test.stopTest();
    }
    
    private static testMethod void successfullAssignment(){
        init();
        Case objC = new Case();
        objC.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
        objC.Origin = 'Web';
        objC.Status = 'New';
        objC.Document_Verified__c = true;
        objC.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objC;
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        list<Id> ids = new list<Id>();
        ids.add(objC.Id);
        VerifyPayments.checkPendingAmountInIPMS(ids);
        Test.stopTest();
    }
    
    private static testMethod void failure1Assignment(){
        init();
        Case objC = new Case();
        objC.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
        objC.Origin = 'Web';
        objC.Status = 'New';
        objC.Document_Verified__c = true;
        objC.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objC;
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1,1));
        list<Id> ids = new list<Id>();
        ids.add(objC.Id);
        VerifyPayments.checkPendingAmountInIPMS(ids);
        Test.stopTest();
    }
    
    private static testMethod void failure2Assignment(){
        init();
        Case objC = new Case();
        objC.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
        objC.Origin = 'Web';
        objC.Status = 'New';
        objC.Document_Verified__c = true;
        objC.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objC;
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1,2));
        list<Id> ids = new list<Id>();
        ids.add(objC.Id);
        VerifyPayments.checkPendingAmountInIPMS(ids);
        Test.stopTest();
    }
}