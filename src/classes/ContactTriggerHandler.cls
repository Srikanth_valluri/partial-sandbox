/****************************************************************************************************************
* Name                  : ContactTriggerHandler                                                                 *
* Trigger               : ContactTrigger                                                                        *
* Test Class            : ContactTriggerHandler_Test                                                            *
* Description           : This trigger will populate the number of customer portal users for Agency.            *
* Created By            : NSI                                                                                   *               
* Created Date          : 19/Jan/2017                                                                           *   
* ------------------------------------------------------------------------------------------------------------  *
* VERSION     AUTHOR            DATE            DESCRIPTION                                                     *                                        
* 1.0         Sivasankar        19/01/2017      Initial development                                             *
* 2.0         Sivasankar        05/02/2017      Added the method to add the recently added contact to           *
*                                               Inventory Users & Assigned Agent.                               *
* 3.0         Vineet            05/09/2017      Added code to validate and encrypt mobile numbers.              *
* 4.0         Craig Lobo        22-07-2018      Added logic to Update the Portal User with from the Contact     *
* 5.0         Craig Lobo        26-07-2018      Added logic to Create new Portal User on Insert of Contact      *
                                                (Only Agent Rep for Corporate Accounts)                         *
****************************************************************************************************************/
public class ContactTriggerHandler implements TriggerFactoryInterface{ 
    
    /*********************************************************************************************
    * @Description : Method to contain logic to be executed before insert.                       *
    * @Params      : Map<Id,sObject>                                                             *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeBeforeInsertTrigger(List<sObject> newRecordsList){
        try{
            /*Masking the Mobile numbers */
            system.debug('#### here');
            encryptMobileNumbers((List<Contact>)newRecordsList);    
        }catch(Exception ex){
            system.debug('#### Exception at line number = '+ex.getLineNumber()+' , Exception message = '+ex.getMessage());  
        }
    }
    
    /*********************************************************************************************
    * @Description : Method to contain logic to be executed After insert.                        *
    * @Params      : Map<Id,sObject>                                                             *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeAfterInsertTrigger(Map<Id, sObject> newRecordsMap){

        // 5.0 Update the Portal User with from the Contact
        System.debug('executeAfterInsertTrigger >>>> ');  
        List<String> executiveIdList = Label.Agent_Executive_Ids.split(',');
        System.debug('executiveIdList  >>>> ' + executiveIdList );
        System.debug('UserInfo.getProfileId() >>>> ' + UserInfo.getProfileId());
        if (executiveIdList != null && executiveIdList.contains(UserInfo.getProfileId())) {
            System.debug('createPortalUser >>>>' + newRecordsMap.keySet() );
            UpdatePortalUsers.createPortalUser(newRecordsMap.keySet());
        }
    }

    /*********************************************************************************************
    * @Description : Method to contain logic to be executed before update.                       *
    * @Params      : Map<Id,sObject>                                                             *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeBeforeUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){
        try{
            /*Masking the Mobile numbers */
            encryptMobileNumbers((List<Contact>)newRecordsMap.values());    
        }catch(Exception ex){
            system.debug('#### Exception at line number = '+ex.getLineNumber()+' , Exception message = '+ex.getMessage());  
        }
    }

    /*********************************************************************************************
    * @Description : Method to contain logic to be executed After update.                        *
    * @Params      : Map<Id,sObject>                                                             *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeAfterUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){
        System.debug('=====executeAfterUpdateTrigger=====');
        // 4.0 Update the Portal User with from the Contact
        Boolean callUpdateUserPortal = false;
        Set<Id> contactIdList = new Set<Id>();
        for(Id thisKey : newRecordsMap.keySet() ) {
           Contact newContact = (Contact) newRecordsMap.get(thisKey);
           Contact oldContact = (Contact) oldRecordsMap.get(thisKey);
           if(newContact.Email != oldContact.Email
                || newContact.FirstName != oldContact.FirstName
                || newContact.LastName != oldContact.LastName
                || UpdatePortalUsers.getUserProfile(newContact) != UpdatePortalUsers.getUserProfile(oldContact)
            ) {
               System.debug('===value changed===');
               contactIdList.add(newContact.Id);
               callUpdateUserPortal = true;
           }
       }

       if(callUpdateUserPortal && !contactIdList.isEmpty()){
           System.debug('===call update user portal====');
           UpdatePortalUsers.modifyProtalUserRecords(newRecordsMap.keySet());
       }
    }


    /*********************************************************************************************
    * @Description : Method to contain logic to be executed after delete.                        *
    * @Params      : Map<Id,sObject>                                                             *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeAfterDeleteTrigger(Map<Id,sObject> oldRecordsMap){
        Set<Id> agencyIDs = new Set<Id>();
        try{
            for(Contact con : (List<Contact>)oldRecordsMap.values()){
                if(con.AccountID != null) { agencyIDs.add(con.AccountID); }
            }
            if(!agencyIDs.isEmpty()) { updateNoOfPortalUsers(agencyIDs); }
            if(Test.isRunningTest()){
                updateNoOfPortalUsers(agencyIDs);
                Account acc = new Account();
                update acc;
            }
        }catch(exception ex){
            system.debug('#### Exception at line number = '+ex.getLineNumber()+' , Exception message = '+ex.getMessage());
        }
    }

    /************************************************************************************************
    * @Description : Method to aggregrate Assigned PC's to the associated account.                  *
    * @Params      : Set<ID>                                                                        *
    * @Return      : void                                                                           *
    ************************************************************************************************/
    @TestVisible private void updateNoOfPortalUsers(Set<ID> updatedAgencyIDs){
        List<Account> updateAccounts = new List<Account>();
        for(AggregateResult agg : [SELECT AccountID, Count(Id) totPCs 
                                   FROM Contact 
                                   WHERE AccountID IN: updatedAgencyIDs AND 
                                         Salesforce_User__c != null AND 
                                         Salesforce_User__r.isActive = true 
                                   GROUP BY AccountID ]){
                updateAccounts.add(new Account(id=(ID)agg.get('AccountID'),Number_of_PCs_Assigned__c = (Decimal)agg.get('totPCs')));
        }
        if(!updateAccounts.isEmpty()){ update updateAccounts; }
        if(Test.isRunningTest()){
            Account acc = new Account();
            update acc;
        }
    }
    
    /*********************************************************************************************
    * @Description : Method to mask the mobile number and encrypt them accordingly               *
    * @Params      : List<Contact>                                                               *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    @Testvisible private void encryptMobileNumbers(List<Contact> newListContact){
        //iterate the Inquiries for masking
        Id businessContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('DAMAC Contact').getRecordTypeId();
        for(Contact thisContact : newListContact){
            if(String.isNotBlank(thisContact.Mobile_Phone_Encrypt__c) &&  
               String.isNotBlank(thisContact.Mobile_Country_Code__c) && 
               thisContact.Mobile_Phone_Encrypt__c.isNumeric()){
                thisContact.Mobile_Phone_Encrypt__c = UtilityHelperCls.removePreceedingZeroes(thisContact.Mobile_Phone_Encrypt__c);
            }
            if(String.isNotBlank(thisContact.Mobile_Phone_Encrypt_2__c) && 
               String.isNotBlank(thisContact.Mobile_Country_Code_2__c) && 
               thisContact.Mobile_Phone_Encrypt_2__c.isNumeric()){
                thisContact.Mobile_Phone_Encrypt_2__c = UtilityHelperCls.removePreceedingZeroes(thisContact.Mobile_Phone_Encrypt_2__c);
            }
            if(String.isNotBlank(thisContact.Mobile_Phone_Encrypt_3__c) && 
               String.isNotBlank(thisContact.Mobile_Country_Code_3__c) && 
               thisContact.Mobile_Phone_Encrypt_3__c.isNumeric()){
                thisContact.Mobile_Phone_Encrypt_3__c = UtilityHelperCls.removePreceedingZeroes(thisContact.Mobile_Phone_Encrypt_3__c);
            }
            if(String.isNotBlank(thisContact.Mobile_Phone_Encrypt_4__c) && 
               String.isNotBlank(thisContact.Mobile_Country_Code_4__c) && 
               thisContact.Mobile_Phone_Encrypt_4__c.isNumeric()){
                thisContact.Mobile_Phone_Encrypt_4__c = UtilityHelperCls.removePreceedingZeroes(thisContact.Mobile_Phone_Encrypt_4__c);
            }
            if(String.isNotBlank(thisContact.Mobile_Phone_Encrypt_5__c) && 
               String.isNotBlank(thisContact.Mobile_Country_Code_5__c) && 
               thisContact.Mobile_Phone_Encrypt_5__c.isNumeric()){
                thisContact.Mobile_Phone_Encrypt_5__c = UtilityHelperCls.removePreceedingZeroes(thisContact.Mobile_Phone_Encrypt_5__c);
            }
            system.debug('#### here');
            if(thisContact.RecordTypeId == businessContactRecordTypeId && !validateMobileNumbers(thisContact)){
                thisContact.Mobile_Phone__c = (String.isBlank(thisContact.Mobile_Phone__c) ? (String.isBlank(thisContact.Mobile_Country_Code__c) && String.isBlank(thisContact.Mobile_Phone_Encrypt__c) ? '' : ((String.isNotBlank(thisContact.Mobile_Phone_Encrypt__c) && (Trigger.isInsert || thisContact.Mobile_Phone__c == null)) ? (thisContact.Mobile_Country_Code__c.subStringAfter(':')).trim()+''+thisContact.Mobile_Phone_Encrypt__c : thisContact.Mobile_Phone__c)) : thisContact.Mobile_Phone__c);
                thisContact.Mobile_Phone_2__c = (String.isBlank(thisContact.Mobile_Phone_2__c) ? (String.isBlank(thisContact.Mobile_Country_Code_2__c) && String.isBlank(thisContact.Mobile_Phone_Encrypt_2__c) ? '' :  ((String.isNotBlank(thisContact.Mobile_Phone_Encrypt_2__c) && (Trigger.isInsert || thisContact.Mobile_Phone_2__c == null)) ? (thisContact.Mobile_Country_Code_2__c.subStringAfter(':')).trim()+''+thisContact.Mobile_Phone_Encrypt_2__c : thisContact.Mobile_Phone_2__c)) : thisContact.Mobile_Phone_2__c);
                thisContact.Mobile_Phone_3__c = (String.isBlank(thisContact.Mobile_Phone_3__c) ? (String.isBlank(thisContact.Mobile_Country_Code_3__c) && String.isBlank(thisContact.Mobile_Phone_Encrypt_3__c) ? '' :  ((String.isNotBlank(thisContact.Mobile_Phone_Encrypt_3__c) && (Trigger.isInsert || thisContact.Mobile_Phone_3__c == null)) ? (thisContact.Mobile_Country_Code_3__c.subStringAfter(':')).trim()+''+thisContact.Mobile_Phone_Encrypt_3__c : thisContact.Mobile_Phone_3__c)) : thisContact.Mobile_Phone_3__c);
                thisContact.Mobile_Phone_4__c = (String.isBlank(thisContact.Mobile_Phone_4__c) ? (String.isBlank(thisContact.Mobile_Country_Code_4__c) && String.isBlank(thisContact.Mobile_Phone_Encrypt_4__c) ? '' : ((String.isNotBlank(thisContact.Mobile_Phone_Encrypt_4__c) && (Trigger.isInsert || thisContact.Mobile_Phone_4__c == null)) ? (thisContact.Mobile_Country_Code_4__c.subStringAfter(':')).trim()+''+thisContact.Mobile_Phone_Encrypt_4__c : thisContact.Mobile_Phone_4__c)) : thisContact.Mobile_Phone_4__c);
                thisContact.Mobile_Phone_5__c = (String.isBlank(thisContact.Mobile_Phone_5__c) ? (String.isBlank(thisContact.Mobile_Country_Code_5__c) && String.isBlank(thisContact.Mobile_Phone_Encrypt_5__c) ? '' :  ((String.isNotBlank(thisContact.Mobile_Phone_Encrypt_5__c) && (Trigger.isInsert || thisContact.Mobile_Phone_5__c == null)) ? (thisContact.Mobile_Country_Code_5__c.subStringAfter(':')).trim()+''+thisContact.Mobile_Phone_Encrypt_5__c : thisContact.Mobile_Phone_5__c)) : thisContact.Mobile_Phone_5__c);
                //Encrypting Mobile and storing it on the record.
                system.debug('#### Before Encryption = '+thisContact);
                thisContact.Mobile_Phone_Encrypt__c = UtilityHelperCls.encryptMobile(thisContact.Mobile_Phone__c);
                thisContact.Mobile_Phone_Encrypt_2__c = UtilityHelperCls.encryptMobile(thisContact.Mobile_Phone_2__c);
                thisContact.Mobile_Phone_Encrypt_3__c = UtilityHelperCls.encryptMobile(thisContact.Mobile_Phone_3__c);
                thisContact.Mobile_Phone_Encrypt_4__c = UtilityHelperCls.encryptMobile(thisContact.Mobile_Phone_4__c);
                thisContact.Mobile_Phone_Encrypt_5__c = UtilityHelperCls.encryptMobile(thisContact.Mobile_Phone_5__c);
                system.debug('#### After Encryption = '+thisContact);
            }
        }
    }
    
    /*********************************************************************************************
    * @Description : Method to validate the mobile number and country code values                *
    * @Params      : List<Contact>                                                               *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    @TestVisible private Boolean validateMobileNumbers(Contact thisContact){
        Boolean isError = false; 
        if(String.isNotBlank(thisContact.Mobile_Country_Code__c) && String.isBlank(thisContact.Mobile_Phone_Encrypt__c)){
            thisContact.Mobile_Phone_Encrypt__c.addError('Mobile Phone can\'t be blank if country code is selected.');
            isError = true;
        }
        if (String.isBlank(thisContact.Mobile_Country_Code__c) && String.isNotBlank(thisContact.Mobile_Phone_Encrypt__c)){
            thisContact.Mobile_Country_Code__c.addError('Mobile Country Code can\'t be blank if Mobile Phone is populated.');
            isError = true;
        }
        if(String.isNotBlank(thisContact.Mobile_Country_Code_2__c) && String.isBlank(thisContact.Mobile_Phone_Encrypt_2__c)){
            thisContact.Mobile_Phone_Encrypt_2__c.addError('Mobile Phone 2 can\'t be blank if country code 2 is selected.');
            isError = true;
        }
        if (String.isBlank(thisContact.Mobile_Country_Code_2__c) && String.isNotBlank(thisContact.Mobile_Phone_Encrypt_2__c)){
            thisContact.Mobile_Country_Code_2__c.addError('Mobile Country Code can\'t be blank if Mobile Phone 2 is populated.');
            isError = true;
        }
        if(String.isNotBlank(thisContact.Mobile_Country_Code_3__c) && String.isBlank(thisContact.Mobile_Phone_Encrypt_3__c)){
            thisContact.Mobile_Phone_Encrypt_3__c.addError('Mobile Phone 3 can\'t be blank if country code 3 is selected.');
            isError = true;
        }
        if (String.isBlank(thisContact.Mobile_Country_Code_3__c) && String.isNotBlank(thisContact.Mobile_Phone_Encrypt_3__c)){
            thisContact.Mobile_Country_Code_3__c.addError('Mobile Country Code can\'t be blank if Mobile Phone 3 is populated.');
            isError = true;
        }
        if(String.isNotBlank(thisContact.Mobile_Country_Code_4__c) && String.isBlank(thisContact.Mobile_Phone_Encrypt_4__c)){
            thisContact.Mobile_Phone_Encrypt_4__c.addError('Mobile Phone 4 can\'t be blank if country code 4 is selected.');
            isError = true;
        }
        if (String.isBlank(thisContact.Mobile_Country_Code_4__c) && String.isNotBlank(thisContact.Mobile_Phone_Encrypt_4__c)){
            thisContact.Mobile_Country_Code_4__c.addError('Mobile Country Code can\'t be blank if Mobile Phone 4 is populated.');
            isError = true;
        }
        if(String.isNotBlank(thisContact.Mobile_Country_Code_5__c) && String.isBlank(thisContact.Mobile_Phone_Encrypt_5__c)){
            thisContact.Mobile_Phone_Encrypt_5__c.addError('Mobile Phone 5 can\'t be blank if country code 5 is selected.');
            isError = true;
        }
        if (String.isBlank(thisContact.Mobile_Country_Code_5__c) && String.isNotBlank(thisContact.Mobile_Phone_Encrypt_5__c)){
            thisContact.Mobile_Country_Code_5__c.addError('Mobile Country Code can\'t be blank if Mobile Phone 5 is populated.');
            isError = true;
        }
        return isError;
    }
    
    // TO BE Implemented
    public void executeBeforeInsertUpdateTrigger(List<sObject> newRecordsList, Map<Id,sObject> oldRecordsMap){}
    public void executeBeforeDeleteTrigger(Map<Id,sObject> oldRecordsMap){}
    public void executeAfterInsertUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){}

    
}// End of class.