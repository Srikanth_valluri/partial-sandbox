global class DAMAC_sendSMSBatchHelper implements Database.Batchable <SObject>, Database.stateFul,Database.allowscallouts { 

    
    String query = '', mobileNumbers = '';
    ID smsRequestID = null;
    global DAMAC_sendSMSBatchHelper (List <String> campaignIdsList, List <String> countryList, List <String> cityList, 
                                    List <String> typeList,
                                    List <String> categoriesList, List <String> phoneCodesList, List <String> nationalitiesList,
                                    List <String> sourceList,
                                    List <String> classList, List <String> statusList, List <String> recordTypeList, 
                                    DateTime inquiryStartDate, DateTime inquiryEndDate,
                                    ID campaignSMSID) {
    
        smsRequestID = campaignSMSID;
        Send_SMS__c settings = Send_SMS__c.getInstance (UserInfo.getUserID ());
        query = 'SELECT Mobile_Phone_Encrypt__c FROM Inquiry__c '
                            +' WHERE Mobile_Phone_Encrypt__c != null ';
                            /*
                            +' AND Validity__c = \'Valid\''
                            +' AND Reachable__c = \'Reachable\''
                            +' AND Phone_Number_Type__c = \'Mobile\''
                            */
        query += ' AND (RecordType.name != \'CIL\' AND RecordType.name != \'Agent team\') ';
        query += ' AND Duplicate__c = False AND Inquiry_Owner__c != \'Queue\' AND';
        
        if (campaignIdsList.size() > 0) {
            query += '( ';
            for (String key : campaignIdsList)
                query += '  Campaign__c = \''+key+'\' OR ';
                
            
            query = query.removeEND ('OR ');
            query += ' ) AND';
        }
        if (countryList.size () > 0) {
            query += '( ';
            for (String key : countryList )
                query += '  Campaign__r.Country__c = \''+key+'\' OR ';
                
            query = query.removeEND ('OR ');
            query += ' ) AND';
        }
        
        if (cityList.size () > 0) {
            query += '( ';
            for (String key : cityList )
                query += '  Campaign__r.City__c = \''+key+'\' OR ';
                
            query = query.removeEND ('OR ');
            query += ' ) AND';
        }
        if (typeList.size () > 0) {
            query += '( ';
            for (String key : typeList)
                query += '  Campaign__r.Campaign_Type_New__c = \''+key+'\' OR ';
                
            query = query.removeEND ('OR ');
            query += ' ) AND';
        }  
        
        if (categoriesList.size () > 0) {
            query += '( ';
            for (String key : categoriesList)
                query += '  Campaign__r.Campaign_Category_New__c = \''+key+'\' OR ';
                
            query = query.removeEND ('OR ');
            query += ' ) AND';
        }    
            
        if (phoneCodesList.size () > 0) {
            query += '( ';
            for (String key : phoneCodesList)
                query += '  Mobile_CountryCode__c = \''+key+'\' OR ';
                
            query = query.removeEND ('OR ');
            query += ' ) AND';
        }
        
        if (nationalitiesList.size () > 0) {
            query += '( ';
            for (String key : nationalitiesList)
                query += '  Nationality__c = \''+key+'\' OR ';
                
            query = query.removeEND ('OR ');
            query += ' ) AND';
        }
        
        if (sourceList.size () > 0) {
            query += '( ';
            for (String key : sourceList)
                query += '  Inquiry_Source__c = \''+key+'\' OR ';
                
            query = query.removeEND ('OR ');
            query += ' ) AND';
        }
        
        if (classList.size () > 0) {
            query += '( ';
            for (String key : classList)
                query += '  Class__c = \''+key+'\' OR ';
                
            query = query.removeEND ('OR ');
            query += ' ) AND';
        }
        
        if (statusList.size () > 0) {
            query += '( ';
            for (String key : statusList)
                query += '  Inquiry_Status__c = \''+key+'\' OR ';
                
            query = query.removeEND ('OR ');
            query += ' ) AND';
        }
        
        if (recordTypeList.size () > 0) {
            query += '( ';
            for (String key : recordTypeList)
                query += '  recordTypeId = \''+key+'\' OR ';
                
            query = query.removeEND ('OR ');
            query += ' ) AND';
        }
        
        if (inquiryStartDate != null) {  
            String str = inquiryStartDate.format('yyyy-MM-dd\'T\'kk:mm:ss\'z\'');    
            if (inquiryStartDate.hour() == 0) {
                str = str.replace('T24', 'T00');
            }
            
            str = str.removeEnd ('z')+'+0400';          
            query += ' CreatedDate >='+str+' AND';
        }
        if (inquiryEndDate != null) {   
            if (inquiryEndDate.second() == 0)
                inquiryEndDate = inquiryEndDate.addSeconds(60);
            String str = inquiryEndDate.format('yyyy-MM-dd\'T\'kk:mm:ss\'z\'');  
            if (inquiryEndDate.hour() == 0) {
                str = str.replace('T24', 'T00');
            }
            str = str.removeEnd ('z')+'+0400';          
            query += ' CreatedDate <= '+str;
        }
                    
        query = query.removeEnd('AND');
        Integer recordCount = Integer.valueOf (settings.Record_Count__c);
        query += ' LIMIT '+recordCount;
        
        
    }
    global Database.QueryLocator start (Database.BatchableContext BC) {
        if (Test.isRunningTest ())
            query = 'SELECT Mobile_Phone_Encrypt__c from Inquiry__c WHERE Mobile_Phone_Encrypt__c != null LIMIT 1';
        return Database.getQueryLocator (query);
    }
    global void execute (Database.BatchableContext BC, List <Inquiry__c> scope){
        for (Inquiry__c inq :scope) {
            if (inq.Mobile_Phone_Encrypt__c != NULL)
                mobileNumbers += inq.Mobile_Phone_Encrypt__c+',';
        }
    
    }
    global void finish (Database.BatchableContext BC) {
        mobileNumbers = mobileNumbers.removeEnd (',');
        Attachment att = new Attachment ();
        att.Name = 'Inquiry Mobile Numbers.csv';
        att.ParentID = smsRequestID;
        att.contentType = 'csv';
        att.Body = Blob.valueOf (mobileNumbers);
        DAMAC_Constants.Skip_AttachmentTrigger_SMS = TRUE;
        insert att;
    
    }
    
    
}