@isTest
public class FMGatePassProcessControllerTest {
    public static testmethod void createFMCase(){
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        Account acctIns = TestDataFactoryFM.createAccount();
        insert acctIns;
        
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;
        
        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns.Inventory__c = invObj.id;
        insert buIns;
        
        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;
         
        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;
        
        PageReference myVfPage = Page.FMGatePassProcessPage;
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Account__c = acctIns.id;
        Test.setCurrentPage(myVfPage);
        //Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Gate_Pass_Request');
        //ApexPages.currentPage().getParameters().put('Id',Opp.Id);
        FMGatePassProcessController obj=new FMGatePassProcessController();
        obj.objFMCase=fmCaseObj;
        obj.createRequestForWorkPermit();
    }
    
    public static testmethod void submitFMCase(){
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        Account acctIns = TestDataFactoryFM.createAccount();
        insert acctIns;
        
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
       
        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;
        
        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns.Inventory__c = invObj.id;
        insert buIns;
        
        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;
         
        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        fmUser.fm_role__c ='FM Manager';
        insert fmUser;
        
        PageReference myVfPage = Page.FMGatePassProcessPage;
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Email__c = 'test@test.com';
        fmCaseObj.Request_Type_DeveloperName__c='Gate_Pass_Request';
        insert fmCaseObj;
        Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
        
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Gate_Pass_Request');
        FMGatePassProcessController  obj=new FMGatePassProcessController();
        obj.strDocumentBody='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        obj.strDocumentName='Test_Document_Work_Permit.htm';
        
        test.startTest();
        obj.createCaseShowUploadDoc();
        //obj.uploadDocument();
        obj.strFMCaseId=fmCaseObj.id;
        obj.submitRequestForWorkPermit();
        //obj.returnBackToCasePage();
        test.stoptest();
    }
    
    public static testmethod void testInsertAttachment(){
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        Account acctIns = TestDataFactoryFM.createAccount();
        insert acctIns;
        
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;
        
        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns.Inventory__c = invObj.id;
        insert buIns;
        
        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;
         
        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;
        
        PageReference myVfPage = Page.FMGatePassProcessPage;
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Gate_Pass_Request';
        insert fmCaseObj;
        Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
        
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Gate_Pass_Request');
        FMGatePassProcessController  obj=new FMGatePassProcessController();
        obj.strDocumentBody='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        obj.strDocumentName='Test_Document_Work_Permit.htm';
        
        test.startTest();
        obj.strFMCaseId=fmCaseObj.id;
        obj.objFMCase=fmCaseObj;
        obj.uploadDocument();
        obj.submitRequestForWorkPermit();
        //obj.returnBackToCasePage();
        test.stoptest();
    }
    
    public static testmethod void createFMCaseExistingCase(){
        Id RecTypeFMCase = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Work Permit').getRecordTypeId();
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        Account acctIns = TestDataFactoryFM.createAccount();
        insert acctIns;
        
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;
        
        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns.Inventory__c = invObj.id;
        insert buIns;
        
        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;
         
        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;
        
        PageReference myVfPage = Page.FMGatePassProcessPage;
        FM_Case__c fmCaseIns=new Fm_Case__c();
        fmCaseIns.recordtypeid=RecTypeFMCase;
        fmCaseIns.Account__c=acctIns.id;
        fmCaseIns.Booking_Unit__c=buIns.id;
        fmCaseIns.status__c='Closed';
        fmCaseIns.Approval_Status__c='Approved';
        //fmCaseIns.Request_Type__c='Work Permit';
        fmCaseIns.Permit_To_Work_For__c='Hot Works;Confined Spaces';
        insert fmCaseIns;
        //Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Gate_Pass_Request');
        //ApexPages.currentPage().getParameters().put('Id',Opp.Id);
        FMGatePassProcessController obj=new FMGatePassProcessController();
        obj.strDocumentBody='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        obj.strDocumentName='Test_Document_Work_Permit.htm';
        //obj.objFMCase=fmCaseObj;
        test.startTest();
        obj.strFMCaseId=fmCaseObj.id;
        //obj.uploadDocument();
        obj.notifyContractorConsultant();
        obj.createRequestForWorkPermit();
        test.stoptest();
    }
}