@isTest
private class FmcGuestMoveInControllerTest {

    @isTest
    static void testController() {
        insert new Location__c(Name = 'LOC', Location_ID__c = 'LOC');
        System.assertEquals(NULL, FmcGuestMoveInController.lookupBuilding(NULL));
        System.assert(FmcGuestMoveInController.lookupBuilding('test').isEmpty());
        System.assertEquals(NULL, FmcGuestMoveInController.lookupUnit('test', NULL));
        System.assert(FmcGuestMoveInController.lookupUnit('test', 'INVALID_LOC').isEmpty());
    }

}