/*-------------------------------------------------------------------------------------------------
    @Who : Rahul Seth
    @When : 16 March 2018
    @What : This class is controller of PromotersViewSalesTours which shows the records of SalesTour.
*/

public with sharing class PromotersViewSalesToursController {
    @testVisible
    Integer PAGE_SIZE = 10;
    public String searchText{get;set;}
    //Constructor to initiaze
    public PromotersViewSalesToursController () {
        
    }
    
    public Pagereference search() {
        System.debug('===========searchText : ' + searchText);

        String finalSearchValue = '%' + searchText + '%';
        List<Sales_Tours__c> searchSalesTourList = 
            new List<Sales_Tours__c>([SELECT Name,
                                             Inquiry__c,
                                             Inquiry__r.Name,
                                             Tour_Outcome__c,
                                             Comments__c,
                                             Inquiry__r.Last_Name__c,
                                             Inquiry__r.First_Name__c, 
                                             Inquiry__r.Inquiry_Status__c,
                                             Check_In_Date__c,
                                             Check_Out_Date__c,
                                             Inquiry__r.Owner.Name,     
                                             Inquiry__r.Promoter_Name__c
                                        FROM Sales_Tours__c
                                       WHERE Inquiry__r.CreatedBy.Profile.Name in:Label.PromoterCommunityProfile.split(',')
                                         AND ( Tour_Outcome__c LIKE :finalSearchValue
                                               OR Inquiry__r.First_Name__c LIKE :finalSearchValue
                                               OR Inquiry__r.Last_Name__c LIKE :finalSearchValue
                                               OR Inquiry__r.Name LIKE :finalSearchValue
                                             )
            ]);
        System.debug('=====search======searchSalesTourList : ' + searchSalesTourList);
        setCon = new ApexPages.StandardSetController(searchSalesTourList); 
        //setCon.setPageSize(PAGE_SIZE);
            
        return null;
    }

    public ApexPages.StandardSetController setCon {
        get{
            if(setCon == null){
                // size = 10;
                // Get the list of Promoter Profile Names
                List<String> profileNames = Label.PromoterCommunityProfile.split(',');
                User currentUser = [ SELECT Id
                                          , Profile.Name
                                          , Name
                                          , ContactId
                                       FROM User
                                      WHERE Id = :UserInfo.getUserId()
                ];
                // Populate Promoter Name if User Profiel is mentioned in the Promoter Label
                String currentUserId = currentUser.Id;
                if(profileNames != null && !profileNames.isEmpty() && profileNames.contains(currentUser.Profile.Name)) {
                
                    String queryString  = ' SELECT Name, Inquiry__c, Inquiry__r.Name, Tour_Outcome__c, '
                                        + ' Comments__c, Inquiry__r.CreatedBy.Name, Inquiry__r.Last_Name__c, '
                                        + ' Inquiry__r.First_Name__c, Inquiry__r.Inquiry_Status__c, ' 
                                        + ' Inquiry__r.CreatedById, CreatedById, Check_In_Date__c,'
                                        + ' Check_Out_Date__c,Inquiry__r.Owner.Name,Inquiry__r.Promoter_Name__c ' 
                                        + ' FROM Sales_Tours__c '
                                        + ' WHERE Inquiry__r.CreatedBy.Profile.Name IN :profileNames '
                                        + ' AND Inquiry__r.CreatedById = :currentUserId ';
                    setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
                    //setCon.setPageSize(PAGE_SIZE);
                    // noOfRecords = setCon.getResultSize();
                }
            }
            return setCon;
        }set;
    }

    public List<Sales_Tours__c> getlstSalesTours() {
        list<Sales_Tours__c> lstSalesTours = new list<Sales_Tours__c>();
        if(setCon != NULL) {
            System.debug('===='+setCon.getResultSize());
            for(Sales_Tours__c a : (List<Sales_Tours__c>)setCon.getRecords()) {
                System.debug('===>>>=' + a.Inquiry__r.CreatedBy.Name ); 
                lstSalesTours.add(a);
            }
        }

        return lstSalesTours;
    }

}