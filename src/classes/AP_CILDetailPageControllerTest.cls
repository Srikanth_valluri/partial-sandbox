@isTest
public without sharing class  AP_CILDetailPageControllerTest {

    public static testMethod void TestnewCIL(){
        Inquiry_Assignment_Rules__c assRules = new Inquiry_Assignment_Rules__c();
        insert assRules;
        Id devRecordTypeId1 
            = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
        Inquiry__c preInq = new Inquiry__c();
        preInq.Mobile_Phone_Encrypt__c = '05789088';
        preInq.Mobile_Phone_Encrypt_2__c = '05789088';
        preInq.Mobile_Phone_Encrypt_3__c = '05789088';
        preInq.Mobile_Phone_Encrypt_4__c	 = '05789088';
        preInq.Mobile_Phone_Encrypt_5__c	 = '05789088';
        preInq.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        preInq.Mobile_Country_Code_2__c = 'United Arab Emirates: 00971';
        preInq.Mobile_Country_Code_3__c = 'United Arab Emirates: 00971';
        preInq.Mobile_Country_Code_4__c = 'United Arab Emirates: 00971';
        preInq.Mobile_Country_Code_5__c = 'United Arab Emirates: 00971';
        preInq.Preferred_Language__c = 'English';
        preInq.First_Name__c = 'Test First name';
        preInq.Last_Name__c = 'Test Last name';
        preInq.Email__c = 'test@gmail.com';
        preInq.Inquiry_Source__c = 'Customer Referral';
        preInq.Mobile_Phone__c = '456789';
        preInq.RecordTypeId = devRecordTypeId1;
        preInq.Inquiry_Status__c = 'Active';
        preInq.Sales_Office__c = 'AKOYA';
        preInq.Meeting_Due_Date__c = system.now();
        preInq.Type_of_Property_Interested_in__c = 'Hotel';
        preInq.No_Of_Bed_Rooms_Availble_in_SF__c = '6';
        preInq.Completed__c = 'Yes';
        preInq.Budget__c = 'Below 500K';
        preInq.Project__c = 'DAMAC'; 
        preInq.Joint_Income_To_Be_Considered__c = 'Yes';
        preInq.Telesales_Executive__c = Userinfo.getUserId();
        preInq.Inquiry_Assignment_Rules__c = assRules.Id;
        insert preInq;
        //create Deal SR record
        NSIBPM__SR_Template__c SRTemplate = new NSIBPM__SR_Template__c();
        srtemplate.NSIBPM__SR_RecordType_API_Name__c = 'Deal';
        insert SRTemplate;

        NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
        sr.ID_Type__c = null;
        sr.Agency_Type__c = 'Corporate';
        sr.agency__c=null;
        sr.NSIBPM__SR_Template__c = SRTemplate.id;
        insert sr;
        
        Task taskObj = new Task();
        taskObj.WhatId = preInq.Id;
        taskObj.ActivityDate = System.Today();
        //taskObj.OwnerId = pcUser.Id;
        taskObj.Subject= 'test 123';
        taskObj.Status = 'Completed';
        taskObj.Priority = 'Normal';
        taskObj.Activity_Type__c = 'Calls';
        taskObj.Activity_Sub_Type__c = '';
        taskObj.Start_Date__c = System.Today();
        taskObj.End_Date__c = System.Today();
        taskObj.Description = 'Hello';
        //taskObj.Agency_Contacts_Ids__c = conList[0].Id;
        insert taskObj;
        
        PageReference pageRef = Page.AP_CILDetailPage;
        Test.setCurrentPage(pageRef);
        //ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(preInq);
        ApexPages.currentPage().getParameters().put('inquiryId',preInq.id);
        ApexPages.currentPage().getParameters().put('Id',SR.id);

        AP_CILDetailPageController con = new AP_CILDetailPageController();
        con.sendContactDetails();
    }
}