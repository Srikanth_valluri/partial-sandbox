/***************************************************************************************************************
Description: Test class for RoasteringApiBatchScheduler.
________________________________________________________________________________________________________________
Version Date(DD-MM-YYYY)    Author              Description
----------------------------------------------------------------------------------------------------------------
1.0     16-07-2020          Komal Shitole       Initial Draft
****************************************************************************************************************/
@isTest
public class RoasteringApiBatchSchedulerTest {
      static testMethod void testExecute() {
        Test.startTest();
            RoasteringApiBatchScheduler objScheduler = new RoasteringApiBatchScheduler();
            String schCron = '0 30 8 1/1 * ? *';
            String jobId = System.Schedule('Reccord Updated',schCron,objScheduler);
        Test.stopTest();
        
    }
}