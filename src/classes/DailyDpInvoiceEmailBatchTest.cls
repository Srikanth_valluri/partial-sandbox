@isTest
public with sharing class DailyDpInvoiceEmailBatchTest { 
    
    @isTest
    public static void test_Arabic() {
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        Process_Email_Template__mdt emt = [SELECT Process_Name__c, Nationality__c, Email_Template__c
                        FROM Process_Email_Template__mdt
                        WHERE Nationality__c ='Indian'
                        LIMIT 1];
                System.debug('emt !!==' + emt);

        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount2 = TestDataFactory_CRM.createPersonAccount();
        objAccount2.Nationality__c = 'Saudi';
        objAccount2.Email__pc = 'gsauf2@hjfd.com';
        insert objAccount2;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount2.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        insert bookingUnitList;
        
        DP_Invoices__c obj=new DP_Invoices__c();
        obj.Accounts__c = objAccount2.id;
        obj.BookingUnits__c = bookingUnitList[0].id;
        obj.Cover_Letter__c = 'test';
        obj.SOA__c= 'test';
        obj.Trxn_Number__c= 'test';
        obj.TAX_Invoice__c= 'test';
        obj.COCD_Letter__c= 'test';
        obj.Other_Language_TAX_Invoice__c = 'test';
        obj.EmailSent__c = false;
         obj.Milestone__c = 'test';
        obj.Type_of_Milestone__c = 'Date Based';
        obj.Invoice_URLs__c = '<ul><li><a href="https://sftest.deeprootsurface.com/docs/t/a52250000004fG8AAI-1561639757136.pdf" target="_blank">SOA</a></li><li><a href="https://sftest.deeprootsurface.com/docs/t/a52250000004fG8AAI-1561639757140.pdf" target="_blank">Other Language SOA</a></li><li><a href="https://sftest.deeprootsurface.com/docs/t/a52250000004fG8AAI-1561639729249.pdf" target="_blank">Cover Letter</a></li></ul>';
        
        insert obj;

        
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
        DataBase.executeBatch(new DailyDpInvoiceEmailBatch(), 1);
        Test.StopTest();
    }

    @isTest
    public static void test_Chinese() {
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        Process_Email_Template__mdt emt = [SELECT Process_Name__c, Nationality__c, Email_Template__c
                        FROM Process_Email_Template__mdt
                        WHERE Nationality__c ='Indian'
                        LIMIT 1];
                System.debug('emt !!==' + emt);

        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount2 = TestDataFactory_CRM.createPersonAccount();
        objAccount2.Nationality__c = 'Chinese';
        objAccount2.Email__pc = 'gsauf2@hjfd.com';
        insert objAccount2;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount2.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        insert bookingUnitList;
        
        DP_Invoices__c obj=new DP_Invoices__c();
        obj.Accounts__c = objAccount2.id;
        obj.BookingUnits__c = bookingUnitList[0].id;
        obj.Cover_Letter__c = 'test';
        obj.SOA__c= 'test';
        obj.Trxn_Number__c= 'test';
        obj.TAX_Invoice__c= 'test';
        obj.COCD_Letter__c= 'test';
        obj.Milestone__c = 'test';
        obj.Type_of_Milestone__c = 'Construction Based';
        obj.Other_Language_TAX_Invoice__c = 'test';
        obj.EmailSent__c = false;
        obj.Invoice_URLs__c = '<ul><li><a href="https://sftest.deeprootsurface.com/docs/t/a52250000004fG8AAI-1561639757136.pdf" target="_blank">SOA</a></li><li><a href="https://sftest.deeprootsurface.com/docs/t/a52250000004fG8AAI-1561639757140.pdf" target="_blank">Other Language SOA</a></li><li><a href="https://sftest.deeprootsurface.com/docs/t/a52250000004fG8AAI-1561639729249.pdf" target="_blank">Cover Letter</a></li></ul>';
        
        insert obj;

        
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
        DataBase.executeBatch(new DailyDpInvoiceEmailBatch(), 1);
        Test.StopTest();
    }
    @isTest
    public static void test_Russian() {
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        Process_Email_Template__mdt emt = [SELECT Process_Name__c, Nationality__c, Email_Template__c
                        FROM Process_Email_Template__mdt
                        WHERE Nationality__c ='Indian'
                        LIMIT 1];
                System.debug('emt !!==' + emt);

        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount2 = TestDataFactory_CRM.createPersonAccount();
        objAccount2.Nationality__c = 'Russian';
        objAccount2.Email__pc = 'gsauf2@hjfd.com';
        insert objAccount2;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount2.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        insert bookingUnitList;
        
        DP_Invoices__c obj=new DP_Invoices__c();
        obj.Accounts__c = objAccount2.id;
        obj.BookingUnits__c = bookingUnitList[0].id;
        obj.Cover_Letter__c = 'test';
        obj.SOA__c= 'test';
        obj.Trxn_Number__c= 'test';
        obj.TAX_Invoice__c= 'test';
        obj.COCD_Letter__c= 'test';
        obj.Milestone__c = 'test';
        obj.Type_of_Milestone__c = 'Construction Based';
        obj.Other_Language_TAX_Invoice__c = 'test';
        obj.EmailSent__c = false;
        obj.Invoice_URLs__c = '<ul><li><a href="https://sftest.deeprootsurface.com/docs/t/a52250000004fG8AAI-1561639757136.pdf" target="_blank">SOA</a></li><li><a href="https://sftest.deeprootsurface.com/docs/t/a52250000004fG8AAI-1561639757140.pdf" target="_blank">Other Language SOA</a></li><li><a href="https://sftest.deeprootsurface.com/docs/t/a52250000004fG8AAI-1561639729249.pdf" target="_blank">Cover Letter</a></li></ul>';
        
        insert obj;

        
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
        DataBase.executeBatch(new DailyDpInvoiceEmailBatch(), 1);
        Test.StopTest();
    }

    @isTest
    public static void test_BlankUrls() {
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        Process_Email_Template__mdt emt = [SELECT Process_Name__c, Nationality__c, Email_Template__c
                        FROM Process_Email_Template__mdt
                        WHERE Nationality__c ='Indian'
                        LIMIT 1];
                System.debug('emt !!==' + emt);

        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount2 = TestDataFactory_CRM.createBusinessAccount();
        objAccount2.Nationality__c = 'Russian';
        objAccount2.Email__c = 'gsauf2@hjfd.com';
        insert objAccount2;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount2.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        insert bookingUnitList;
        
        DP_Invoices__c obj=new DP_Invoices__c();
        obj.Accounts__c = objAccount2.id;
        obj.BookingUnits__c = bookingUnitList[0].id;
        obj.Cover_Letter__c = 'test';
        obj.SOA__c= 'test';
        obj.Trxn_Number__c= 'test';
        obj.TAX_Invoice__c= 'test';
        obj.COCD_Letter__c= 'test';
        obj.Other_Language_TAX_Invoice__c = 'test';
        obj.EmailSent__c = false;
        obj.Milestone__c = 'test';
        obj.Type_of_Milestone__c = 'Construction Based';
        obj.Registration_Id__c = '102988';
        //obj.Invoice_URLs__c = 'test';
        
        insert obj;

        
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
        DataBase.executeBatch(new DailyDpInvoiceEmailBatch(), 1);
        Test.StopTest();
    }
    @isTest
    public static void test_BlankNationality() {
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        Process_Email_Template__mdt emt = [SELECT Process_Name__c, Nationality__c, Email_Template__c
                        FROM Process_Email_Template__mdt
                        WHERE Nationality__c ='Indian'
                        LIMIT 1];
                System.debug('emt !!==' + emt);

        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount2 = TestDataFactory_CRM.createPersonAccount();
        objAccount2.Nationality__c = '';
        objAccount2.Email__pc = 'gsauf2@hjfd.com';
        insert objAccount2;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount2.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        insert bookingUnitList;
        
        DP_Invoices__c obj=new DP_Invoices__c();
        obj.Accounts__c = objAccount2.id;
        obj.BookingUnits__c = bookingUnitList[0].id;
        obj.Cover_Letter__c = 'test';
        obj.SOA__c= 'test';
        obj.Trxn_Number__c= 'test';
        obj.TAX_Invoice__c= 'test';
        obj.COCD_Letter__c= 'test';
        obj.Other_Language_TAX_Invoice__c = 'test';
        obj.EmailSent__c = false;
        obj.Milestone__c = 'test';
        obj.Type_of_Milestone__c = 'Construction Based';
        obj.Invoice_URLs__c = '<ul><li><a href="https://sftest.deeprootsurface.com/docs/t/a52250000004fG8AAI-1561639757136.pdf" target="_blank">SOA</a></li><li><a href="https://sftest.deeprootsurface.com/docs/t/a52250000004fG8AAI-1561639757140.pdf" target="_blank">Other Language SOA</a></li><li><a href="https://sftest.deeprootsurface.com/docs/t/a52250000004fG8AAI-1561639729249.pdf" target="_blank">Cover Letter</a></li></ul>';
        
        insert obj;

        
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
        DataBase.executeBatch(new DailyDpInvoiceEmailBatch(), 1);
        Test.StopTest();
    }
        @isTest
    public static void test_IndianNationality() {
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        Process_Email_Template__mdt emt = [SELECT Process_Name__c, Nationality__c, Email_Template__c
                        FROM Process_Email_Template__mdt
                        WHERE Nationality__c ='Indian'
                        LIMIT 1];
                System.debug('emt !!==' + emt);

        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount2 = TestDataFactory_CRM.createPersonAccount();
        objAccount2.Nationality__c = 'Indian';
        objAccount2.Email__pc = 'gsauf2@hjfd.com';
        insert objAccount2;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount2.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        insert bookingUnitList;
        
        DP_Invoices__c obj=new DP_Invoices__c();
        obj.Accounts__c = objAccount2.id;
        obj.BookingUnits__c = bookingUnitList[0].id;
        obj.Cover_Letter__c = 'test';
        obj.SOA__c= 'test';
        obj.Trxn_Number__c= 'test';
        obj.TAX_Invoice__c= 'test';
        obj.COCD_Letter__c= 'test';
        obj.Other_Language_TAX_Invoice__c = 'test';
        obj.EmailSent__c = false;
        obj.Milestone__c = 'test';
        obj.Type_of_Milestone__c = 'Construction Based';
        obj.Invoice_URLs__c = '<ul><li><a href="https://sftest.deeprootsurface.com/docs/t/a52250000004fG8AAI-1561639757136.pdf" target="_blank">SOA</a></li><li><a href="https://sftest.deeprootsurface.com/docs/t/a52250000004fG8AAI-1561639757140.pdf" target="_blank">Other Language SOA</a></li><li><a href="https://sftest.deeprootsurface.com/docs/t/a52250000004fG8AAI-1561639729249.pdf" target="_blank">Cover Letter</a></li></ul>';
        
        insert obj;

        
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
        DataBase.executeBatch(new DailyDpInvoiceEmailBatch(), 1);
        Test.StopTest();
    }

}