/******************************************************************************
* Description - Test class developed for GetJOPDAreaonBUScheduler
*
* Version            Date            Author                    Description
* 1.0              29/04/2018                                 Initial Draft
********************************************************************************/
@isTest
private class HandoverCheckEligibilityBatchSchTest {

    static testMethod void testScheduler() {
      Test.startTest();
        HandoverCheckEligibilityBatchScheduler ascsObject = new HandoverCheckEligibilityBatchScheduler();
      String sch = '0 0 23 * * ?'; 
    //  system.schedule('demo ', sch, ascsObject); 
    Test.stopTest();
    }
     static testMethod void testScheduler2() {
      Test.startTest();
        String jobName = 'demo job';
        HandoverCheckEligibilityBatchScheduler ascsObject = new HandoverCheckEligibilityBatchScheduler(jobName);
      String sch = '0 0 23 * * ?'; 
      system.schedule(jobName+System.Now(), sch, ascsObject); 
    Test.stopTest();
    }
}// End of class