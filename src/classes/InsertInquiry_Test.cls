/*******************************************/
/*  Date           Comments
 *  04/03/2018      Test method for InsertInquiry
 
 */
 /*******************************************/
@isTest
public class InsertInquiry_Test{

    public static testmethod void PostMethod(){
        
        
        profile profId = [SELECT Id FROM Profile WHERE Name='Standard User']; // query on Profile to fetch ProfileId
        String orgId = UserInfo.getOrganizationId();
        // Create User
        User tuser = new User();
        tuser.firstname = 'fName';
        tuser.lastName = 'lName';
        tuser.email = 'uniqueName' + '@test' + orgId + '.org';
        tuser.Username = 'uniqueName' + '@test' + orgId + '.org';
        tuser.EmailEncodingKey = 'ISO-8859-1';
        tuser.Alias = 'unique';
        tuser.TimeZoneSidKey = 'America/Los_Angeles';
        tuser.LocaleSidKey = 'en_US';
        tuser.LanguageLocaleKey = 'en_US';
        tuser.ProfileId = profId.id;
        insert tuser;
        
        // Create Request and Response        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/insertInquiry/*';  
        req.httpMethod = 'POST';
        //Create JSON
        req.requestBody = Blob.valueof('{ "sObject":[ { "First_Name__c" : "s", "Mobile_Country_Code_2__c" : "India: 0091", "Mobile_Phone_Encrypt_2__c" : "123", "Country__c" : "Algeria", "Last_Name__c" : "t", "Nationality__c" : "Afghanistan", "Comments__c" : "Testing", "Preferred_Language__c" : "African", "Assigned_PC__c" : "'+tuser.id+'", "OwnerId" : "'+tuser.id+'", "City__c" : "Constantine", "Meeting_Type__c" : "Scheduled Tour", "Tour_Date_Time__c" : "'+system.now()+'", "Email__c" : "g@gmail.com", "Mobile_CountryCode__c" : "India: 0091", "Mobile_Phone_Encrypt__c" : "61" }]}');
        RestContext.request = req;
        RestContext.response= res;
        // Test starts here
        Test.startTest();
        // method calling    
            InsertInquiry.InsertInquiryRecords(); 
        Test.stopTest();
    }
 }