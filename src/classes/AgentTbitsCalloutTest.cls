// Create By :  Naresh Kaneriya
@isTest
private class AgentTbitsCalloutTest {
  private static Account acc;
    static void setupData() {
        acc = new Account(name='testacc');
        insert acc;
    }
    
    @isTest static void test_method_1() {
      setupData();
       AgentTbitsCallout tcObject = new AgentTbitsCallout(new AgentBuyerDetailsController());
        AgentTbitsCallout.cls_errorDescription obj1 = new AgentTbitsCallout.cls_errorDescription('test');
        AgentTbitsCallout.cls_fields obj2 = new AgentTbitsCallout.cls_fields();
        AgentTbitsCallout.cls_ReferenceID obj3 = new AgentTbitsCallout.cls_ReferenceID();
        system.debug('#### acc = '+acc);
        AgentTbitsCallout.getPassportDetails('base64, test====', 'test.png', acc.Id, 'Passport');
        AgentTbitsCallout.getPassportDetails('base64, test=', 'test.png', acc.Id, 'Passport');
        AgentTbitsCallout.getPassportDetails('base64, test', 'test.png', acc.Id, 'Passport');
    }
    
     @isTest static void test_method_2() {
      setupData();
       AgentTbitsCallout tcObject = new AgentTbitsCallout(new AgentBuyerDetailsController());
        AgentTbitsCallout.cls_errorDescription obj1 = new AgentTbitsCallout.cls_errorDescription('test');
        AgentTbitsCallout.cls_fields obj2 = new AgentTbitsCallout.cls_fields();
        AgentTbitsCallout.cls_ReferenceID obj3 = new AgentTbitsCallout.cls_ReferenceID();
        system.debug('#### acc = '+acc);
        AgentTbitsCallout.getPassportDetails('base64, test', 'test.png', acc.Id, 'Id');
    }
    
    
    
    
      // Inner Class
        @isTest static void test_method_3() {
        setupData();
        
        AgentTbitsCallout.wrapperTbitsResponse obj1 = new AgentTbitsCallout.wrapperTbitsResponse();
        AgentTbitsCallout.cls_errorDescription obj3 = new AgentTbitsCallout.cls_errorDescription('OK');
        AgentTbitsCallout.cls_ReferenceID obj4 = new AgentTbitsCallout.cls_ReferenceID();
        
        AgentTbitsCallout.cls_fields obj5 = new AgentTbitsCallout.cls_fields ();
        obj5.valid =  true; 
        obj5.text=  'Test';
        obj5.name=  'PassportNumber';
        
        AgentTbitsCallout.cls_fields obj6 = new AgentTbitsCallout.cls_fields ();
        obj6.valid =  true; 
        obj6.text=  'Test';
        obj6.name=  'NationalId';
        List<AgentTbitsCallout.cls_fields> obj8 = new List<AgentTbitsCallout.cls_fields>();
        obj8.add(obj5);
        obj8.add(obj6 ); 
           
        obj1.result = 'Test';
        obj1.errorCode = 200;
        obj1.errorDescription = obj3;
        obj1.ReferenceID = obj4;   
        obj1.fields = obj8;
        AgentTbitsCallout.getPassportDetails('base64, test', 'test.jpg', acc.Id, 'Id');
        
    }
    
    
    @isTest static void test_method_4() {
      setupData();
       AgentTbitsCallout tcObject = new AgentTbitsCallout(new AgentBuyerDetailsController());
        AgentTbitsCallout.cls_errorDescription obj1 = new AgentTbitsCallout.cls_errorDescription('test');
        AgentTbitsCallout.cls_fields obj2 = new AgentTbitsCallout.cls_fields();
        AgentTbitsCallout.cls_ReferenceID obj3 = new AgentTbitsCallout.cls_ReferenceID();
        system.debug('#### acc = '+acc);
        AgentTbitsCallout.getPassportDetails('base64, test', 'test.png', acc.Id, 'Id');
    }
    
    
    
    
    
}// End of class.