/**************************************************************************************************
* Name               : InventoryShareRemovalController
* Test Class         : InventoryShareRemoveBatchSchedulerTest
* Description        : Controller class to remove the Share records related to Inventories
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR      DATE
* 1.0         QBurst      27/08/2020
**************************************************************************************************/
global class InventoryShareRemovalController{
    public Deal_Exception_Request__c request {Get; set;}
    public string requestId {get; set;}
    public string message {get; set;}
    public boolean error {get; set;}
    public InventoryShareRemovalController(){
        error = false;
        requestId = Apexpages.currentPage().getParameters().get('Id');
        if(requestId != null && requestId != ''){
            request = [SELECT Id, Name, RM__c, RM__r.Name, Cancellation_Reason__c,
                        Inventory_Request_Number__c, Status__c
                     FROM Deal_Exception_Request__c WHERE Id =: requestId];   
            if(request.Status__c != 'Approved'){
                error = true;
                message = 'Only Approved Requests can be cancelled';
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, message));
            }
        }
    }
    public Pagereference cancelInventoryRequest() {
        List<Deal_Exception_Unit__c> reqUnitList = new List<Deal_Exception_Unit__c>();
        reqUnitList = [SELECT Id, Inventory__c, Inventory__r.status__c, Inventory_Share_Time__c, 
                            Inventory_Shared_to_RM__c, Inventory_Initial_Status__c, 
                            Deal_Exception_Request__c, Deal_Exception_Request__r.RM__c
                       FROM Deal_Exception_Unit__c
                       WHERE Inventory__c != NULL 
                       AND Inventory__r.status__c = 'Released'
                       AND Deal_Exception_Request__c =: requestId];
        if(reqUnitList.size() > 0){
            removeShareDetails(reqUnitList);
            sendCancelNotificationToRM(requestId);
            request.status__c = 'Cancelled';
            try{
                update request;
                message = 'Request Cancelled Successfully';
            } catch(exception e){
                system.debug('Exception while updating Request Status: ' + e);
            }
            
            return null;
        } else{
            message = 'No Applicable Inventories Found';
            return  null;
        }
    }

    
    public void sendCancelNotificationToRM(String requestId){
        system.debug('Sending Cancellation Notification to RM');
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new  Messaging.SingleEmailMessage();
        String ccEmail = '';
        List<String> ccEmails = new List<String>();
        String body = 'Hi ' + request.RM__r.Name + ',<br/><br/>';
        body += 'Inventory Request ' + request.Inventory_Request_Number__c + ' has been cancelled.<br/><br/>';
        body += 'Cancellation Reason: ' + request.Cancellation_Reason__c;
        String emailSubject = 'Inventory Request ' + request.Inventory_Request_Number__c 
                        + ' Cancelled ';
                        
        body += '<br/><br/> Link to the Request: ' + System.URL.getSalesforceBaseUrl().toExternalForm() 
                    + '/apex/RequestInventory?id=' + requestId;
        mail.setTargetObjectId(request.RM__c);
        mail.setCcAddresses(ccEmails);
        mail.setSenderDisplayName('Damac Property ');
        mail.setReplyTo('noreply@Damacgroup.com');
        mail.setSubject(emailSubject);
        mail.setSaveAsActivity(false);
        System.debug('...body...' + body);
        mail.setHtmlBody(body);
        mails.add(mail);  
        system.debug('mail: ' + mail);
        Messaging.sendEmail(mails);    
    
    }
    
    public static void removeShareDetails(List<Deal_Exception_Unit__c> reqUnitList){
        Map<Id, Deal_Exception_Unit__c> reqUnitsToUpdate = new  Map<Id, Deal_Exception_Unit__c>();
        Map<Id, Inventory__c> invsToUpdate = new Map<Id, Inventory__c>();
        Map<Id, Inventory_User__c> invUserToDelete = new Map<Id, Inventory_User__c>();
        Map<Id, Inventory__Share> invShareToDelete = new Map<Id, Inventory__Share>();
        Map<Id, Id> reqUnitInvMap = new Map<Id, Id>();
        Map<Id, String> reqUnitRMMap = new Map<Id, String>();
        Map<String, Inventory_User__c> invUserMap = new Map<String, Inventory_User__c>();
        Map<String, Inventory__Share> invShareMap = new Map<String, Inventory__Share>();
        for(Deal_Exception_Unit__c reqUnit: reqUnitList){
            reqUnitInvMap.put(reqUnit.Id, reqUnit.Inventory__c);  
            reqUnitRMMap.put(reqUnit.Id, reqUnit.Deal_Exception_Request__r.RM__c);    
        }
        for(Inventory_User__c invUser: [SELECT Id, Inventory__c, User__c FROM Inventory_User__c
                                        WHERE Inventory__c IN: reqUnitInvMap.values() 
                                            AND User__c IN: reqUnitRMMap.values() ]){
            String key = invUser.Inventory__c + '-' + invUser.User__c;
            invUserMap.put(key, invUser);
        }
        for(Inventory__Share invShare: [SELECT Id, ParentId, UserOrGroupId FROM Inventory__Share
                                        WHERE ParentId IN: reqUnitInvMap.values() 
                                            AND UserOrGroupId IN: reqUnitRMMap.values() ]){
            String key = invShare.ParentId + '-' + invShare.UserOrGroupId;
            invShareMap.put(key, invShare);
        }
        for(Deal_Exception_Unit__c reqUnit: reqUnitList){
            String key = reqUnit.Inventory__c + '-' + reqUnit.Deal_Exception_Request__r.RM__c;
            if(invUserMap.containsKey(key) && invUserMap.get(key) != null){
                invUserToDelete.put(invUserMap.get(key).Id, invUserMap.get(key));
            }
            if(invShareMap.containsKey(key) && invShareMap.get(key) != null){
                invShareToDelete.put(invUserMap.get(key).Id, invShareMap.get(key));
            }
            if(reqUnit.Inventory_Initial_Status__c != '' && reqUnit.Inventory_Initial_Status__c != null 
                                && reqUnit.Inventory__r.status__c != reqUnit.Inventory_Initial_Status__c){
                Inventory__c inv = new Inventory__c();
                inv.Id = reqUnit.Inventory__c;
                inv.Status__c = reqUnit.Inventory_Initial_Status__c;
                invsToUpdate.put(inv.Id, inv);
            }
            reqUnit.Inventory_Shared_to_RM__c = false;
            reqUnitsToUpdate.put(reqUnit.Id, reqUnit);
        }
        try{
            system.debug('invUserToDelete: ' + invUserToDelete);
            system.debug('invShareToDelete: ' + invShareToDelete);
            system.debug('invsToUpdate: ' + invsToUpdate);
            system.debug('reqUnitsToUpdate: ' + reqUnitsToUpdate);
            if(invShareToDelete.values().size() > 0){
                delete invShareToDelete.values();
            }
            if(invUserToDelete.values().size() > 0){
                delete invUserToDelete.values();
            }
            
            if(invsToUpdate.values().size() > 0){
                update invsToUpdate.values();    
            }
            if(reqUnitsToUpdate.values().size() > 0){
                update reqUnitsToUpdate.values();
            }
          } catch(exception e){
            system.debug('Exception while Perfomring DML Operation: ' + e);
        }
    }
}