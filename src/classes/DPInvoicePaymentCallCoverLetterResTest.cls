/**
 * @File Name          : DPInvoicePaymentCallCoverLetterResTest.cls
 * @Description        : Test class for DPInvoicePaymentCallCoverLetterResponse
 * @Author             : Dipika Rajput
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 6/25/2019, 12:52:20 PM
 * @Modification Log   : 
 *==============================================================================================
 * Ver         Date                     Author                    Modification
 * 
 *==============================================================================================
 * 1.0    6/25/2019                   Dipika Rajput           Initial Version
**/
@isTest
private class DPInvoicePaymentCallCoverLetterResTest{
	@isTest
	static void itShould() {
		
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

		//create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        insert bookingUnitList;
        
        DP_Invoices__c obj=new DP_Invoices__c();
	    obj.Accounts__c = objAccount.id;
		obj.BookingUnits__c = bookingUnitList[0].id;
        insert obj;      
        
        List<Attachment> attachList = new List<Attachment>();
        for(Integer i=0;i<1;i++) {
            Attachment attobj=new Attachment();

            attobj.Name=Label.Payment_Call_Cover_Letter+'.pdf';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            attobj.body=bodyBlob;
            attobj.ContentType = 'application/pdf';

            attobj.parentId=obj.id;
            attachList.add(attobj);
        }//end for
        insert attachList;
        
        System.debug('attachList' + attachList);
        System.assertEquals(1, attachList.size());
        
        Test.startTest();
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1));
        Test.stopTest();
        
        DPInvoicePaymentCallCoverLetterResponse controller = new DPInvoicePaymentCallCoverLetterResponse();
        DPInvoicePaymentCallCoverLetterResponse.getAllAttachmentRecordsToPopulateCoverLetter(attachList);
	}
    /*
    @isTest
	static void itShouldAlso() {
		
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

		//create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        insert bookingUnitList;
        
        DP_Invoices__c obj=new DP_Invoices__c();
	    obj.Accounts__c = objAccount.id;
		obj.BookingUnits__c = bookingUnitList[0].id;
        insert obj;      
        
        List<Attachment> attachList = new List<Attachment>();
        for(Integer i=0;i<1;i++) {
            Attachment attobj=new Attachment();

            attobj.Name=Label.Payment_Call_Cover_Letter+'.pdf';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            attobj.body=bodyBlob;
            attobj.ContentType = 'application/pdf';

            attobj.parentId=obj.id;
            attachList.add(attobj);
        }//end for
        insert attachList;
        set<Id> setDPIId = new set<Id>();
        setDPIId.add(obj.id);
        System.debug('attachList' + attachList);
        System.assertEquals(1, attachList.size());
        
        Test.startTest();
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1));
        Test.stopTest();
        
        DPInvoicePaymentCallCoverLetterResponse controller = new DPInvoicePaymentCallCoverLetterResponse();
        DPInvoicePaymentCallCoverLetterResponse.populateCoverLetterOnDpInvoices(
                setDPIId, 
                null, 
                false, 
                Blob.valueOf('Unit Test Attachment Body'), 
                string.valueOf(0));
	}*/
}