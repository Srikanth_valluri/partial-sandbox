/**********************************************************************************************
* Description : Notifies to customer via Email to remind the service charges.                 *
*---------------------------------------------------------------------------------------------*
* Revision History:                                                                           *
* Version   Author              Date        Description                                       *
* 1.0       Aishwarya Todkar    19/02/2019  Initial Draft                                     *
* 2.0       Aishwarya Todkar    03/03/2019  1.Added more filter criterias in SOQL.            *
*                                           2.Added validation for Qatar and JORDAN Countries.*
* 2.1       Arjun Khatri        14/03/2019  1.Validated Arabic email contents.                *
* 2.2       Arjun Khatri        18/03/2019  1.Added validation for 'Board Member Unit'        *
* 3.0       Arjun Khatri        24/03/2019  1.Added 'getCCAddress' method to set all CC       *
*                                           addresses uniquely.                               *
* 3.1       Arjun khatri        28/03/2019  1.Updated text body of EmailMessage               *
* 3.2       Arjun Khatri        30/05/2019  1.Added check for Open POP SR                     *
* 3.3       Arjun Khatri        25/06/2019  1.Added criteria of 'ByPassFMReminder'            *   
* 3.4       Arjun Khatri        03/07/2019  1.Added FM Outstanding Amount in template body    *  
* 3.5       Shashank Maind      27/11/2019  1.Added IPMS service for getting the Unit SOA
                                              to send in email as an attachment               *  
* 3.6       Arjun Khantri       14/01/2019  1.change send Email logic on basis of Country     *

***********************************************************************************************/
global class FMServiceChargesNotifierBatch implements Database.Batchable<sObject>
                                                    , Database.Stateful
                                                    , Database.AllowsCallouts {
    
    EmailTemplate FMAMountTemplate_UAE, FMAMountTemplate_Qatar ,FMAMountTemplate_JORDAN;
    Id personRecTypeId, businessRecTypeId;
    List<EmailMessage> lstEmails;
    Decimal dueAmount = 0.0;
    Set<String> setProperties ;
    Set<String> countrySet;
    
    public  FMServiceChargesNotifierBatch() {
        setProperties = FM_Service_Charges_properties__c.getAll().keySet();
        system.debug('setProperties=='+setProperties);
        
        FMAMountTemplate_UAE = [SELECT 
                                ID, Subject, Body, HtmlValue, TemplateType
                            FROM 
                                EmailTemplate 
                            WHERE 
                                DeveloperName = 'FM_Service_Charges_Reminder_UAE' limit 1];
                                
        FMAMountTemplate_Qatar = [SELECT 
                                ID, Subject, Body, HtmlValue, TemplateType
                            FROM 
                                EmailTemplate 
                            WHERE 
                                DeveloperName = 'FM_Service_Charges_Reminder_Qatar' limit 1];
                                
        FMAMountTemplate_JORDAN = [SELECT 
                                ID, Subject, Body, HtmlValue, TemplateType
                            FROM 
                                EmailTemplate 
                            WHERE 
                                DeveloperName = 'FM_Service_Charges_Reminder_Lebanon' limit 1];                        

        personRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        businessRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        dueAmount = Decimal.valueOf(Label.FM_Service_Charges_Amount);
        
        // TODO added on basis of Label
        String countryLabel = Label.FMServiceChargesNotifierBatch_Country_Label;
        countryLabel.toLowerCase();
        List<String> countryList = new List<String>();
        countryList = countryLabel.split(',');
        countrySet = new Set<String>(countryList);      
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
         System.debug('***FMAmountBUEmailNotifierBatch Start ****');
         String strQuery = 'SELECT Id, Email__c, FM_Outstanding_Amount__c,Account_Id__c, Booking__c, Registration_ID__c,' +
                        ' Booking__r.Account__c, Booking__r.Account__r.recordTypeId, Booking__r.Account__r.Email__c, ' +
                        ' Booking__r.Account__r.Email__pc, Booking__r.Account__r.Name, Booking__r.Account__r.Mobile__c, '+
                        ' Customer_Name__c,Unit_Name__c, Property_Country__c, CurrencyIsoCode, Inventory__r.Property_Country__c, '+
                        ' Booking__r.Account__r.First_Name_Arabic__c, Booking__r.Account__r.Last_Name_Arabic__c, ' +
                        ' Booking__r.Account__r.PersonEmail, Booking__r.Account__r.Agency_Email__c, ' +
                        ' Booking__r.Account__r.Alternate_Agency_Email__c, Booking__r.Account__r.Alternate_Email__c, ' +
                        ' Booking__r.Account__r.Email_1__c, Booking__r.Account__r.Email_1_Person_Name__c, ' +
                        ' Booking__r.Account__r.Email_2__c, Booking__r.Account__r.Email_2_Person_Name__c, ' +
                        ' Booking__r.Account__r.Email_3__c, Booking__r.Account__r.Email_3_Person_Name__c, ' +
                        ' Booking__r.Account__r.Old_Email__c, Booking__r.Account__r.Email_2__pc, ' +
                        ' Booking__r.Account__r.Email_3__pc, Booking__r.Account__r.Email_4__pc, Inventory__r.Building_Name__c, ' +
                        ' Booking__r.Account__r.Email_5__pc, Booking__r.Account__r.Board_Member_Unit__c' +
                        ' FROM Booking_Unit__c' +
                        ' WHERE FM_Outstanding_Amount__c != NULL AND  FM_Outstanding_Amount__c != \'\' ' + 
                        ' AND Registration_ID__c != NULL AND Booking__c != NULL AND Booking__r.Account__c != NULL ' +
                        //' AND(Booking__r.Account__r.ZBEmailStatus__c = \'\' OR Booking__r.Account__r.ZBEmailStatus__c = \'Valid\') ' + 
                        ' AND (Property_Country__c != null  OR Inventory__r.Property_Country__c != null)' +
                        ' AND (Booking__r.Account__r.Email__pc != NULL OR Booking__r.Account__r.Email__c != NULL) ' +
                        ' AND (Booking__r.Account__r.recordTypeId =\'' + personRecTypeId + '\'' +
                        ' OR Booking__r.Account__r.recordTypeId = \'' + businessRecTypeId + '\') ' +
                        ' AND SC_From_RentalIncome__c = False AND  CM_Units__c = NULL AND Handover_Flag__c = \'Y\' ' +
                        ' AND Unit_Active__c = \'Active\' AND Property_Name_Inventory__c NOT IN : setProperties AND Property_Country__c IN: countrySet ' +
                        ' AND ByPassFMReminder__c = false AND FM_Outstanding_Amount_Formula__c >=: dueAmount ';

                        if( String.isNotBlank ( Label.FM_Service_Rec_Id_For_Test ) 
                         && Label.FM_Service_Rec_Id_For_Test.contains('On') 
                         && String.isNotBlank( Label.FM_Service_Rec_Id_For_Test.substringAfter('-') ) ) {
                            strQuery = strQuery + ' AND Id = \''+Label.FM_Service_Rec_Id_For_Test.substringAfter('-')+ '\'  ';
                        }
                        


         System.debug('strQuery == ' + strQuery);
         return Database.getQueryLocator(strQuery);
    }
    
    global void execute(Database.BatchableContext BC, List<Booking_Unit__c > listBU){ 
        System.debug('***FMAmountBUEmailNotifierBatch execute****');
        System.debug('***listBU ===' + listBU);
        lstEmails = new List<EmailMessage>();
    Map<Id, Boolean> mapAccIdToOpenPopSR = new Map<Id, Boolean>();
        Set<Id> setAccId = new Set<Id>();
    
        if( listBU != null && listBU.size() > 0 ) {
            for(Booking_Unit__c objBU :listBU) {
                setAccId.add(objBU.Booking__r.Account__c);
            }
        }
        
        if( setAccId != null && setAccId.size() > 0 ) {
            for(Account  objAcc:[Select Id, 
                                (Select Id from FM_Cases__r 
                                    where recordType.Name = 'Proof Of Payment' And Status__c != 'Closed' limit 1) 
                                FROM account where Id In :setAccId]) {
                if(objAcc.FM_Cases__r.size() > 0) {
                    mapAccIdToOpenPopSR.put(objAcc.Id, false); // Open POP SR exist
                }else {
                    mapAccIdToOpenPopSR.put(objAcc.Id, true); // Open POP SR doesn't exist
                }
            }
        }
        
        System.debug('mapAccIdToOpenPopSR = '+mapAccIdToOpenPopSR);
        if( mapAccIdToOpenPopSR != null && mapAccIdToOpenPopSR.size() > 0 ) {
            for(Booking_Unit__c objBU :listBU) {
                
                if(mapAccIdToOpenPopSR.containsKey(objBU.Booking__r.Account__c)
                &&  mapAccIdToOpenPopSR.get(objBU.Booking__r.Account__c)) {
                    
                    //validate FM Outstanding Amount
                    if(Decimal.valueOf(objBU.FM_Outstanding_Amount__c) >= dueAmount) {
                        
                        /*try {
                            String url = FmIpmsRestServices.getUnitSoaByRegistrationId(objBU.Registration_ID__c);
                            System.debug('url===>' + url);*/
                            //Validate Email details
                            //if(String.isNotBlank(url)) {
                                if(Label.Service_Charges_Email != null 
                                && String.isNotBlank(Label.Service_Charges_Email)) {
                                    // TODO added on basis of Label



                                    //Validate UAE unit
                                    if((objBU.Property_Country__c != null 
                                        && countrySet.contains(objBU.Property_Country__c.toLowerCase())) 
                                    ||(objBU.Inventory__r.Property_Country__c != null 
                                        &&countrySet.contains(objBU.Inventory__r.Property_Country__c.toLowerCase()))
                                    ) {
                                            if(FMAMountTemplate_UAE != null
                                                && (objBU.Property_Country__c.equalsIgnoreCase('United Arab Emirates') 
                                                    || objBU.Inventory__r.Property_Country__c.equalsIgnoreCase('United Arab Emirates'))
                                                ){
                                                    //Call method to send an email to units only in UAE
                                                    sendEmail(objBU, FMAMountTemplate_UAE);
                                            }  
                                            else if( FMAMountTemplate_Qatar != null
                                                && (objBU.Property_Country__c.equalsIgnoreCase('Saudi Arabia') || objBU.Inventory__r.Property_Country__c.equalsIgnoreCase('Saudi Arabia')
                                                    || objBU.Property_Country__c.equalsIgnoreCase('QATAR') || objBU.Inventory__r.Property_Country__c.equalsIgnoreCase('QATAR')
                                                    ) ){
                                                //Call method to send an email to units only in QATAR or AL JAWHARAH
                                                sendEmail(objBU, FMAMountTemplate_Qatar);           
                                            } 
                                            else if( FMAMountTemplate_JORDAN != null
                                                && (objBU.Property_Country__c.equalsIgnoreCase('JORDAN')  || objBU.Property_Country__c.equalsIgnoreCase('Lebanon')
                                                    || objBU.Inventory__r.Property_Country__c.equalsIgnoreCase('JORDAN') || objBU.Inventory__r.Property_Country__c.equalsIgnoreCase('Lebanon')) ){
                                                //Call method to send an email to units only in QATAR or AL JAWHARAH
                                                sendEmail(objBU, FMAMountTemplate_Qatar);           
                                            }

                                    }// End  if

                                    
                                    //Validate QATAR unit
                                    /*else if(((objBU.Property_Country__c != null 
                                            && (objBU.Property_Country__c.equalsIgnoreCase('Saudi Arabia') 
                                                || objBU.Property_Country__c.equalsIgnoreCase('QATAR')))
                                        ||(objBU.Inventory__r.Property_Country__c != null 
                                            && (objBU.Inventory__r.Property_Country__c.equalsIgnoreCase('Saudi Arabia') 
                                                || objBU.Inventory__r.Property_Country__c.equalsIgnoreCase('QATAR'))))
                                    && FMAMountTemplate_Qatar != null) {

                                            //Call method to send an email to units only in QATAR or AL JAWHARAH
                                            sendEmail(objBU, FMAMountTemplate_Qatar);
                                    }//END QATAR if

                                    //Validate JORDAN Unit
                                    else if(((objBU.Property_Country__c != null 
                                            && (objBU.Property_Country__c.equalsIgnoreCase('JORDAN') 
                                                || objBU.Property_Country__c.equalsIgnoreCase('Lebanon')))
                                        ||(objBU.Inventory__r.Property_Country__c != null 
                                            && (objBU.Inventory__r.Property_Country__c.equalsIgnoreCase('JORDAN') 
                                                || objBU.Inventory__r.Property_Country__c.equalsIgnoreCase('Lebanon'))))
                                        && FMAMountTemplate_JORDAN != null) {

                                            //Call method to send an email to units only in QATAR or AL JAWHARAH
                                            sendEmail(objBU, FMAMountTemplate_JORDAN);
                                    }//End JORDAN unit*/
                                }
                            /*}//END Url if
                            else {
                                Error_Log__c objError = new Error_Log__c();
                                objError.Booking_Unit__c = objBU.Id;
                                objError.Error_Details__c = 'Email can not be send because recieved blank SOA URL in response.';
                            }
                        }//End try
                        catch(Exception ex) {
                            Error_Log__c objError = new Error_Log__c();
                            objError.Booking_Unit__c = objBU.Id;
                            objError.Error_Details__c = 'Email can not be send because following error while generating SOA Url :'+
                                                        ex.getMessage();
                        }*/
                    }//End FM Outstanding Amount if
                }//End Open POP SR if
            }//End for loop
        }//End map size if
        
        System.debug('lstEmails=== ' + lstEmails);
        if(lstEmails.size() > 0) {
            if(!Test.isRunningTest()) {
                insert lstEmails;
            }
        }
        
        
    }//End Execute Method
  
/*********************************************************************************
 * Method Name : sendEmail
 * Description : send emails by sendGrid.
 * Return Type : void
 * Parameter(s): Booking Unit, Email template
**********************************************************************************/    
    void sendEmail(Booking_Unit__c objBU, EmailTemplate FMAMountTemplate) {
        //lstEmails = new List<EmailMessage>();
        List<Attachment> lstAttach = new  List<Attachment>();
        System.debug('***FMAMountTemplate ===' + FMAMountTemplate.Id);
        //System.debug('***FMAMountTemplate ===' + FMAMountTemplate.body.replaceAll('\\<.*?\\>', ''));
        String toAddress = '', fromAddress,replyToAddress = '', contentType, contentValue,contentBody = '';
        String subject, FMAMountTemplateId = '', strAccountId, strBUId, bccAddress ='';
        
        strAccountId = objBU.Account_Id__c != null ? String.valueOf(objBU.Account_Id__c) : objBU.Booking__r.Account__c;
        strBUId = String.valueOf(objBU.id);
        bccAddress = Label.SF_Copy_EMail_Address;
        //bccAddress = 'aishatodkar44@gmail.com';
        System.debug('bccAddress !!==' + bccAddress);
        //TODO add code for FM Soa for Attachment
        /**if(String.isNotBlank(objBU.Registration_ID__c )){
            //String accessToken = FmIpmsRestCoffeeServices.getNewBearerToken();
            String responseUrl = FmIpmsRestServices.getUnitSoaByRegistrationId( objBU.Registration_ID__c );
            if(String.isNotBlank(responseUrl)){
                Blob pdfBlob ;
                HttpRequest req = new HttpRequest(); 
                req.setEndpoint(responseUrl);
                //req.setHeader('Accept', 'application/json');
                req.setMethod('GET');
                //req.setHeader('Authorization','Bearer' + accessToken);
                req.setTimeout(120000);
                HttpResponse response = new Http().send(req);
                //System.debug('Header='+ response.getHeaderKeys());
                System.debug('Status Code = ' + response.getStatusCode());
                System.debug('Pdf Body = ' + response.getBodyAsBlob());
                if(response.getStatusCode() == 200 && response.getBodyAsBlob() != null) {
                    pdfBlob =  response.getBodyAsBlob();
                    Attachment attach = new Attachment();
                    attach.contentType = 'application/pdf';
                    attach.name = 'SOA.pdf';
                    System.debug('attach.name'+attach.name);
                    attach.body = pdfBlob;
                    lstAttach.add(attach);
                }

            }
        }**/
        

        // If Person Acount 
        if(objBU.Booking__r.Account__r.recordTypeId == personRecTypeId
        && objBU.Booking__r.Account__r.Email__pc != null) {
            toAddress = objBU.Booking__r.Account__r.Email__pc;
        }
        //If business account
        else if(objBU.Booking__r.Account__r.recordTypeId == businessRecTypeId
        && objBU.Booking__r.Account__r.Email__c != null){
            toAddress = objBU.Booking__r.Account__r.Email__c;
        }
        fromAddress = Label.Service_Charges_Email;
        contentType = 'text/html';
        replyToAddress = Label.Service_Charges_replyTo_Address;
        FMAMountTemplateId = FMAMountTemplate.id != null ? String.valueOf(FMAMountTemplate.id) : '';
        Subject = FMAMountTemplate.subject != null ? FMAMountTemplate.subject : 'Payment Reminder - Service Charges for {Unit number} in {Tower name}';
    
    if(objBU.Inventory__r.Building_Name__c != null) {
      System.debug('in Tower contains if');
      Subject = Subject.replace('{Tower name}', objBU.Inventory__r.Building_Name__c);
    }
    else{
      System.debug('in Tower contains if');
      Subject = Subject.replace('{Tower name}','');
    }
    
    
    
    if(objBU.Unit_Name__c != null)
      Subject = Subject.replace('{Unit number}',objBU.Unit_Name__c);
    else{
      Subject = Subject.replace('{Unit number}','');
    }
    
        if(FMAMountTemplate.htmlValue != null) {
            contentValue = FMAMountTemplate.htmlValue;
            contentBody = FMAMountTemplate.body.replaceAll('\\<.*?\\>', '');
            String strName = getCamelCase(objBU.Booking__r.Account__r.Name);
            if(contentValue.contains('{!Booking_Unit__c.Customer_Name__c}')) {
                //String strName = getCamelCase(objBU.Booking__r.Account__r.Name);
                contentValue = contentValue.replace('{!Booking_Unit__c.Customer_Name__c}'
                                                    , strName);
                contentBody = contentBody.replace('{!Booking_Unit__c.Customer_Name__c}'
                                                    , strName);
            }
            if(contentValue.contains('{!Booking_Unit__c.Unit_Name__c}') && objBU.Unit_Name__c != NULL){
                contentValue = contentValue.replace('{!Booking_Unit__c.Unit_Name__c}', objBU.Unit_Name__c);
                contentBody = contentBody.replace('{!Booking_Unit__c.Unit_Name__c}', objBU.Unit_Name__c);
            }
            //Added on 14/03/19 for Arabic contents
            if(contentValue.contains('<>')) {
                if(String.isBlank(objBU.Booking__r.Account__r.First_Name_Arabic__c) 
                || String.isBlank(objBU.Booking__r.Account__r.Last_Name_Arabic__c)) {
                    contentValue = contentValue.replace('<>', strName); 
                    contentBody = contentBody.replace('<>', strName); 
                }
                else {
                    String arabicName = objBU.Booking__r.Account__r.First_Name_Arabic__c + ' ' + 
                                    objBU.Booking__r.Account__r.Last_Name_Arabic__c;
                    contentValue = contentValue.replace('<>', arabicName);
                    contentBody = contentBody.replace('<>', arabicName);
                }
            }
            
            //Added on 7/3/19 by Arjun Khatri to add Service charges in email template
            String fmAmount = doFormatting(decimal.valueOf(objBU.FM_Outstanding_Amount__c), 2, ',', '.');
            System.debug('objBU.FM_Outstanding_Amount__c === ' + objBU.FM_Outstanding_Amount__c);
            System.debug('objBU.FM_Outstanding_Amount__c === ' + fmAmount);
            if(contentValue.contains('{!Booking_Unit__c.FM_Outstanding_Amount__c}')) {
                String strCurrency = objBU.CurrencyIsoCode != null? objBU.CurrencyIsoCode :'AED';
                
                contentValue = contentValue.replace('{!Booking_Unit__c.FM_Outstanding_Amount__c}'
                            ,  strCurrency + ' ' + fmAmount);
                contentBody = contentBody.replace('{!Booking_Unit__c.FM_Outstanding_Amount__c}'
                            ,  strCurrency + ' ' +fmAmount);
            }
        }

        String strCCAddress = getCCAddress(objBU,toAddress);
        System.debug('strCCAddress = ' + strCCAddress);
        //Call method to send an email
        if(toAddress != '') {       
            SendGridEmailService.SendGridResponse objSendGridResponse = 
                SendGridEmailService.sendEmailService(toAddress,'', strCCAddress, '', bccAddress, ''
                                            , subject, '', fromAddress,
                                            '', replyToAddress, '', contentType, contentValue, '',
                                        lstAttach);//TODO  add list of Attachment
            system.debug('toAddress === ' + toAddress);
            system.debug('fromAddress === ' + fromAddress);
            system.debug('subject === ' + subject);
            system.debug('contentValue === ' + contentValue);
            system.debug('contentType === ' + contentType);
            system.debug('objSendGridResponse === ' + objSendGridResponse);

            String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;

            if (responseStatus == 'Accepted') {
                System.debug('SendGrid response is Accepted');
                EmailMessage mail = new EmailMessage();
                mail.Subject = subject;
                mail.MessageDate = System.Today();
                mail.Status = '3';//'Sent';
                mail.relatedToId = strAccountId;
                mail.Account__c  = strAccountId;
                mail.Type__c = 'FM Payment Reminders';
                mail.ToAddress = toAddress;
                mail.FromAddress = fromAddress;
                mail.TextBody = contentBody;//contentValue.replaceAll('\\<.*?\\>', '');
                mail.Sent_By_Sendgrid__c = true;
                mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                mail.Booking_Unit__c = strBUId;
                mail.CcAddress = strCCAddress;
                mail.BccAddress = bccAddress;
                lstEmails.add(mail);
                system.debug('Mail obj == ' + mail);
            }
        }
        
    }

/*********************************************************************************
 * Method Name : getCamelCase
 * Description : Convert case of Customer name into Camel case.
 * Return Type : String
 * Parameter(s): Customer Name
**********************************************************************************/
    String getCamelCase(String strName) {
        
        strName = strName.toLowerCase();    
        List<String> names = strName.split(' ');
        for (Integer i = 0; i < names.size(); i++)
            names[i] = names[i].capitalize();
        strName = String.join(names, ' ');

        System.debug('getCamelCase strName == ' + strName);
        return strName;
    }

/*********************************************************************************
 * Method Name : getCCAddress
 * Description : To get unique CC addresses.
 * Return Type : String
 * Parameter(s): BU instance
**********************************************************************************/
    String getCCAddress(Booking_Unit__c objBU, String toAddress) {
        Set<String> setCCAddress = new Set<String>();
        if(String.isNotBlank(objBU.Booking__r.Account__r.PersonEmail)
        && !(objBU.Booking__r.Account__r.PersonEmail.equalsIgnoreCase(toAddress)))
            setCCAddress.add(objBU.Booking__r.Account__r.PersonEmail);
    
        if(String.isNotBlank(objBU.Booking__r.Account__r.Agency_Email__c) &&
        !setCCAddress.contains(objBU.Booking__r.Account__r.Agency_Email__c) &&
        !(objBU.Booking__r.Account__r.Agency_Email__c.equalsIgnoreCase(toAddress)))
            setCCAddress.add(objBU.Booking__r.Account__r.Agency_Email__c);
            
        if(String.isNotBlank(objBU.Booking__r.Account__r.Alternate_Agency_Email__c) &&
        !setCCAddress.contains(objBU.Booking__r.Account__r.Alternate_Agency_Email__c) &&
        !(objBU.Booking__r.Account__r.Alternate_Agency_Email__c.equalsIgnoreCase(toAddress)))
            setCCAddress.add(objBU.Booking__r.Account__r.Alternate_Agency_Email__c);
        
        if(String.isNotBlank(objBU.Booking__r.Account__r.Alternate_Email__c) &&
        !setCCAddress.contains(objBU.Booking__r.Account__r.Alternate_Email__c) &&
        !(objBU.Booking__r.Account__r.Alternate_Email__c.equalsIgnoreCase(toAddress)))
            setCCAddress.add(objBU.Booking__r.Account__r.Alternate_Email__c);
        
        if(String.isNotBlank(objBU.Booking__r.Account__r.Email_1__c) &&
        !setCCAddress.contains(objBU.Booking__r.Account__r.Email_1__c) &&
        !(objBU.Booking__r.Account__r.Email_1__c.equalsIgnoreCase(toAddress)))
            setCCAddress.add(objBU.Booking__r.Account__r.Email_1__c);
        
        if(String.isNotBlank(objBU.Booking__r.Account__r.Email_1_Person_Name__c) &&
        !setCCAddress.contains(objBU.Booking__r.Account__r.Email_1_Person_Name__c) &&
        !(objBU.Booking__r.Account__r.Email_1_Person_Name__c.equalsIgnoreCase(toAddress)))
            setCCAddress.add(objBU.Booking__r.Account__r.Email_1_Person_Name__c);
        
        if(String.isNotBlank(objBU.Booking__r.Account__r.Email_2__c) &&
        !setCCAddress.contains(objBU.Booking__r.Account__r.Email_2__c) &&
        !(objBU.Booking__r.Account__r.Email_2__c.equalsIgnoreCase(toAddress)))
            setCCAddress.add(objBU.Booking__r.Account__r.Email_2__c);
        
        if(String.isNotBlank(objBU.Booking__r.Account__r.Email_2_Person_Name__c) &&
        !setCCAddress.contains(objBU.Booking__r.Account__r.Email_2_Person_Name__c) &&
        !(objBU.Booking__r.Account__r.Email_2_Person_Name__c.equalsIgnoreCase(toAddress)))
            setCCAddress.add(objBU.Booking__r.Account__r.Email_2_Person_Name__c);
        
        if(String.isNotBlank(objBU.Booking__r.Account__r.Email_3__c) &&
        !setCCAddress.contains(objBU.Booking__r.Account__r.Email_3__c) &&
        !(objBU.Booking__r.Account__r.Email_3__c).equalsIgnoreCase(toAddress))
            setCCAddress.add(objBU.Booking__r.Account__r.Email_3__c);
        
        if(String.isNotBlank(objBU.Booking__r.Account__r.Email_3_Person_Name__c) &&
        !setCCAddress.contains(objBU.Booking__r.Account__r.Email_3_Person_Name__c) &&
        !(objBU.Booking__r.Account__r.Email_3_Person_Name__c.equalsIgnoreCase(toAddress)))
            setCCAddress.add(objBU.Booking__r.Account__r.Email_3_Person_Name__c);
        
        if(String.isNotBlank(objBU.Booking__r.Account__r.Old_Email__c) &&
        !setCCAddress.contains(objBU.Booking__r.Account__r.Old_Email__c) &&
        !(objBU.Booking__r.Account__r.Old_Email__c.equalsIgnoreCase(toAddress)))
            setCCAddress.add(objBU.Booking__r.Account__r.Old_Email__c);
        
        if(String.isNotBlank(objBU.Booking__r.Account__r.Email_2__pc) &&
        !setCCAddress.contains(objBU.Booking__r.Account__r.Email_2__pc) &&
        !(objBU.Booking__r.Account__r.Email_2__pc.equalsIgnoreCase(toAddress)))
            setCCAddress.add(objBU.Booking__r.Account__r.Email_2__pc);
        
        if(String.isNotBlank(objBU.Booking__r.Account__r.Email_3__pc) &&
        !setCCAddress.contains(objBU.Booking__r.Account__r.Email_3__pc)&&
        !(objBU.Booking__r.Account__r.Email_3__pc.equalsIgnoreCase(toAddress)))
            setCCAddress.add(objBU.Booking__r.Account__r.Email_3__pc);
        
        if(String.isNotBlank(objBU.Booking__r.Account__r.Email_4__pc) &&
        !setCCAddress.contains(objBU.Booking__r.Account__r.Email_4__pc)&&
        !(objBU.Booking__r.Account__r.Email_4__pc.equalsIgnoreCase(toAddress)))
            setCCAddress.add(objBU.Booking__r.Account__r.Email_4__pc);
        
        if(String.isNotBlank(objBU.Booking__r.Account__r.Email_5__pc) &&
        !setCCAddress.contains(objBU.Booking__r.Account__r.Email_5__pc)&&
        !(objBU.Booking__r.Account__r.Email_5__pc.equalsIgnoreCase(toAddress)))
            setCCAddress.add(objBU.Booking__r.Account__r.Email_5__pc);
        
        String strCCAddress = '';
        for(String ccAdd : setCCAddress) {
            strCCAddress += ccAdd + ',';
        }
        system.debug('strCCAddress 1= ' + strCCAddress);
        strCCAddress = strCCAddress.removeEnd(',');
        return strCCAddress;
    }
    
    public static String doFormatting(Decimal val, integer dec, String tSep, String dSep) {
        String s, tmp;
        Integer i = 4 + dec;
     
        // If the number of decimals is zero (0)... prevents the first 1000s seperator from being set at the 4th.
        if(dec==0){
            i--;
        }
     
        s = val.setScale(dec).toPlainString().replace(tSep, dSep);
        while(s.length() > i) {
            tmp = s.substring(0, s.length() - i) + tSep + s.substring(s.length() - i);
            s = tmp;
            i += 4;
        }
     
        // If the number is negative and has a number non-decimal digits divisible by 3, it prevents putting a comma before the 1st digit (ex -300,000.00  comes out -,300,000.00)
        if (s.substring(0,1) == '-') {
            if (s.substring(1,2) == tSep) {
                s = '-' + s.substring(2);
            }
        }
     
        return s;
    }
    global void finish(Database.BatchableContext BC){
        System.debug('***FMAmountBUEmailNotifierBatch finish****');
        
    }
}