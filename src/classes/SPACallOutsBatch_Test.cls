@istest

public class SPACallOutsBatch_Test{
    
    static testmethod void batchtest(){

        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;
        
        NSIBPM__Service_Request__c objSR = InitializeSRDataTest.getSerReq('Deal',true,null);
        insert objSR;
        
        
        Location__c objLoc = InitializeSRDataTest.createLocation('123','Building');
        insert objLoc;   
        
        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(objLoc.id));
        lstInv.add(InitializeSRDataTest.createInventory(objLoc.id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[1].Property_Country__c = 'Lebanon';
        lstInv[1].Inventory_ID__c = '345wer';
        insert lstInv;
        
        Booking__c book = new booking__c();
        book.Deal_SR__c = objSR.id;
        book.Booking_Channel__c = 'Web';
        insert book;
        
        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = book.id;
        bu.Payment_Method__c = 'Cash';
        bu.Primary_Buyer_s_Email__c = 'test@test.com';
        bu.Primary_Buyer_s_Name__c = 'testNSI';
        bu.Primary_Buyer_s_Nationality__c = 'test';
        bu.Inventory__c = lstInv[0].id;
        insert bu;
        
        
       
        Test.startTest();            
            SPACallOutsBatch obj = new SPACallOutsBatch(objSR.id);
            Database.executeBatch(obj,1);
            System.assertEquals(1, Database.query(obj.query).size(), 'Expected results do not match');
        Test.stopTest();
    
    }

}