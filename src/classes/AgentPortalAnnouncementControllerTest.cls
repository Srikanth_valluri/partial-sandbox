/**************************************************************************************************
* Name               : AgentPortalPropertyListControllerTest                                               
* Description        : An apex page controller for AgentPortalPropertyListController                                         
* Created Date       : Naresh Kaneriya                                                                        
* Created By         : 01/09/2017                                                                 
* Last Modified Date :                                                                            
* Last Modified By   :                                                                             
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR                    DATE                                                                    
* 1.0         Naresh kaneriya         01/09/2017                                                           
**************************************************************************************************/
@isTest(seeAllData=false)
public class AgentPortalAnnouncementControllerTest{


 public static testmethod void AgentPortalAnnouncementTest(){
       Announcement__c obj = new Announcement__c ();
        
        obj.Active__c = true;
        obj.Agency_Tier__c='SIlver';
        obj.Agency_Type__c='Corporate';
        obj.Are_Agencies_Notified__c=true;
        obj.Description__c='test';
        obj.Start_Date__c=Date.today();
        obj.End_Date__c=Date.today();        
        obj.Title__c = 'test';
        insert obj;                                  
                                                      
     AgentPortalAnnouncementController  obj2 = new AgentPortalAnnouncementController();
     Component.Apex.commandButton btn = new Component.Apex.commandButton();
     Component.Apex.outputPanel opPanel = new Component.Apex.outputPanel();
     opPanel.id = 'myButtons';
     btn.title = 'First';
     btn.value = 'First';
     btn.onclick = 'First';
     opPanel.childComponents.add(btn);
     btn.rendered = true; 
     obj2.agentCommissionList = null ;
     obj2.notificationList = null ;
     obj2.getNumbers();
     obj2.getMyCommandButtons();
     obj2.refreshGrid();
     obj2.Previous();
     obj2.Next();
     obj2.End();
     obj2.getDisablePrevious();
     obj2.getDisableNext();
     obj2.getTotal_size();
     obj2.getPageNumber();
     obj2.getTotalPages();
     
 }
 
}