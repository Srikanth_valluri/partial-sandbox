@isTest
public class GenerateDrawloopDocumentBatchTest{
    private static testMethod void testOne(){
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        
        Case objCase = new Case();
        objCase.Status = 'New';
        objCase.Origin = 'Web';
        objCase.NOC_Processing_Pending__c = true;
        objCase.RecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Assignment').RecordTypeId;
        insert objCase;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        GenerateDrawloopDocumentBatch objClass = new GenerateDrawloopDocumentBatch(objCase.Id
                                                                                   , System.Label.CRE_NOC_DDP_Template_Id
                                                                                   , System.Label.CRE_NOC_Template_Delivery_Id);
        Database.Executebatch(objClass);
        Test.stopTest();
    }
}