@isTest
public class accountDetailRetrievalTest {
    @isTest
    static void testAccountDetail(){
        Account objAccount = new Account();
        objAccount.Name = 'test';
        insert objAccount;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        objBooking.Account__c = objAccount.Id;
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Booking__c = objBooking.Id;
        insert objBU;
        
        Case objCase = new Case();
        objCase.AccountId = objCase.Id;
        insert objCase;
        
        SR_Booking_Unit__c objSRBooking = new SR_Booking_Unit__c();
        objSRBooking.Case__c = objCase.Id;
        insert objSRBooking;
        
        POP_Bank_Details__c objPOP = new POP_Bank_Details__c();
        objPOP.Case__c = objCase.Id;
        insert objPOP;
        
        Test.startTest();
            accountDetailRetrieval.getAccountDetails(objAccount.Id);
            accountDetailRetrieval.getAllBUs(objAccount.Id);
            accountDetailRetrieval.getBUDetails(objAccount.Id,objCase.Id);
            accountDetailRetrieval.getBCCheque(objAccount.Id);
            accountDetailRetrieval.getExistingSR(objAccount.Id);
        Test.stopTest();
    }
}