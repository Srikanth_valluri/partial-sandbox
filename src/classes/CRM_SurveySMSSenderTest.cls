@isTest
public with sharing class CRM_SurveySMSSenderTest {
    @IsTest
    static void methodName(){
         Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__c ='test@test.com';
        objAcc.Mobile_Phone_Encrypt__pc = '0097405883798';
        insert objAcc ;

          Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POP').getRecordTypeId();
        
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase.Type = 'Change of Contact Details';
        objCase.IsPOA__c = false;
        objCase.OCR_verified__c = true;
        objCase.OQOOD_Fee_Applicable__c = false;
        objCase.Approval_Status__c = 'Approved' ;
        objCase.Status = 'Working';
         insert objCase;

        List<String> phoneNoList = new List<String>();
        phoneNoList.add(objCase.Id+':9898989898');
        String strAbndId='';
        Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockCallingList(1));
        SendSMSAccountService.Sendtextmessage(phoneNoList,'test message content',objAcc.Id,false,strAbndId);

        Test.startTest();
        CRM_SurveySMSSender.sendSurveySMS('test','009405883798',objAcc.Id) ;
        Test.stopTest();
        
    }
}