public without sharing class LoamsPaymentStatusController {

    public String               orderNo                 {get; set;}
    public String               status                  {get; set;}
    public String               statusCode              {get; set;}
    public String               statusMessage           {get; set;}
    public String               amount                  {get; set;}
    public String               retUrl                  {get; set;}
    public Map<String, String>  params                  {get; set;}
    public String               receiptUrl              {get; set;}

    public static final String OTHERS = 'Others';
    public static final String SERVICE_CHARGE = 'Service Charges';

    public LoamsPaymentStatusController() {
        PageReference currentPage = ApexPages.currentPage();
        if (currentPage != NULL) {
            params = currentPage.getParameters();
            retUrl = params.get('retUrl');
            retUrl = String.isBlank(retUrl) ? Site.getPathPrefix()  : retUrl;
            retUrl = String.isBlank(retUrl) ? '/GuestMakePayment'   : retUrl;
            if (!'Guest'.equalsIgnoreCase(UserInfo.getUserType())) {
                retUrl = Site.getPathPrefix();
            }
            getPaymentResponse(params);
        }
    }

    public PageReference updateReceipt() {
        System.debug('params :'+params);
        if (params != NULL) {
            List<FM_Receipt__c> lstFmReceipt = [
                SELECT  Id
                        , Name
                        , Customer_Party_Id__c
                        , Registration_Id__c
                        , Unit_Name__c
                        , Acknowledgement_Id__c
                         //, Total_Amount__c
                         //, Discount__c
                         //, Discount_Applied__c
                        , Amount__c
                        , Bank_Receipt_Number__c
                        , Bank_Reference_Number__c
                        , Billing_Address__c
                        , Billing_City__c
                        , Billing_Country__c
                        , Billing_Email__c
                        , Billing_Name__c
                        , Billing_Phone__c
                        , Billing_State__c
                        , Billing_Zip__c
                        , Card_Holder_Name__c
                        , Card_Name__c
                        , Currency__c
                        , ECI_Value__c
                        , Merchant_Amount__c
                        , Order_Status__c
                        , Payment_Mode__c
                        , Payment_Type__c
                        , Guest_Payment_Type__c
                        , Booking_Unit__c
                        , Booking_Unit__r.Building_Name__c
                        , Guest_Other_Payment_Type__c
                        , Status_Code__c
                        , Status_Message__c
                        , Receipt_created_in_IPMS__c
                        , Made_Call_To_IPMS__c
                        , ( SELECT  Id
                                    , Name
                                    , Booking_Unit__c
                                    , Registration_Id__c
                                    , Unit_Name__c
                                    //, Is_Next_Q1_Invoice__c
                                    //, TRX_Id__c
                            FROM    FM_Unit_Invoice_Payments__r
                        )
                FROM    FM_Receipt__c
                WHERE   Name = :orderNo
            ];
            if (lstFmReceipt.isEmpty()) {
                insert new Error_Log__c(
                    Process_Name__c = 'Update Receipt',
                    Error_Details__c = 'FM Receipt not found, Payment Response : ' + JSON.serialize(params)
                );
                return NULL;
            }

            String merchantAmount = params.get('mer_amount');
            lstFmReceipt[0].Acknowledgement_Id__c       = params.get('acking_id');
            lstFmReceipt[0].Bank_Receipt_Number__c      = params.get('bank_receipt_no');
            lstFmReceipt[0].Bank_Reference_Number__c    = params.get('bank_ref_no');
            lstFmReceipt[0].Billing_Address__c          = params.get('billing_address');
            lstFmReceipt[0].Billing_City__c             = params.get('billing_city');
            lstFmReceipt[0].Billing_Country__c          = params.get('billing_country');
            lstFmReceipt[0].Billing_Email__c            = params.get('billing_email');
            lstFmReceipt[0].Billing_Name__c             = params.get('billing_name');
            lstFmReceipt[0].Billing_Phone__c            = params.get('billing_tel');
            lstFmReceipt[0].Billing_State__c            = params.get('billing_state');
            lstFmReceipt[0].Billing_Zip__c              = params.get('billing_zip');
            lstFmReceipt[0].Card_Holder_Name__c         = params.get('card_holder_name');
            lstFmReceipt[0].Card_Name__c                = params.get('card_name');
            lstFmReceipt[0].Currency__c                 = params.get('currency');
            lstFmReceipt[0].ECI_Value__c                = params.get('eci_value');
            lstFmReceipt[0].Order_Number__c             = params.get('orderNo');
            lstFmReceipt[0].Order_Status__c             = params.get('order_status');
            lstFmReceipt[0].Payment_Mode__c             = params.get('payment_mode');
            lstFmReceipt[0].Payment_Type__c             = params.get('merchant_param1');
            lstFmReceipt[0].Status_Code__c              = params.get('status_code');
            lstFmReceipt[0].Status_Message__c           = params.get('status_message');
            lstFmReceipt[0].Tracking_Id__c              = params.get('tracking_id');
            lstFmReceipt[0].Failure_Message__c          = params.get('failure_message');
            lstFmReceipt[0].Bank_QSI_Number__c          = params.get('bank_qsi_no');
            lstFmReceipt[0].Amount__c                   = String.isBlank(amount) ? NULL : Decimal.valueOf(amount);
            lstFmReceipt[0].Merchant_Amount__c          = String.isBlank(merchantAmount)
                                                            ? NULL : Decimal.valueOf(merchantAmount);

            if ('Success'.equalsIgnoreCase(lstFmReceipt[0].Order_Status__c)) {
                if (lstFmReceipt[0].Receipt_created_in_IPMS__c != 'TRUE'
                    && (Label.FMPortalServiceCharge.equalsIgnoreCase(lstFmReceipt[0].Payment_Type__c)
                        || (Label.FMGuestPayment.equalsIgnoreCase(lstFmReceipt[0].Payment_Type__c)
                            /*&& (SERVICE_CHARGE.equalsIgnoreCase(lstFmReceipt[0].Guest_Payment_Type__c)
                            || OTHERS.equalsIgnoreCase(lstFmReceipt[0].Guest_Payment_Type__c))*/))) {
                    FmIpmsRestServices.CreateReceiptRequest request = new FmIpmsRestServices.CreateReceiptRequest();
                    request.sourceSystem = 'SFDC';
                    request.extRequestNumber = lstFmReceipt[0].Name;
                    request.businessGroup = 'FM';
                    request.partyId = lstFmReceipt[0].Customer_Party_Id__c;
                    request.receiptNumber = lstFmReceipt[0].Name;
                    request.paidAmount = lstFmReceipt[0].Amount__c;
                    request.currencyCode = 'AED';
                    request.applyReceipt = 'Yes';

                    FmIpmsRestServices.ApplicationLine applicationLine = new FmIpmsRestServices.ApplicationLine();
                    applicationLine.registrationId = String.isNotBlank(lstFmReceipt[0].Registration_Id__c)
                                                        ? lstFmReceipt[0].Registration_Id__c : NULL;
                    applicationLine.amountToApply = lstFmReceipt[0].Amount__c;
                    request.applicationLines = new List<FmIpmsRestServices.ApplicationLine> {applicationLine};

                    /*if (Label.AdvanceServiceChargePayment.equalsIgnoreCase(lstFmReceipt[0].Discount_Applied__c)) {
                        request.customerTrxId = lstFmReceipt[0].Bank_Reference_Number__c;
                        request.discountAttribute = '10% Discount';
                    }*/

                    Set<String> registrationIds = new Set<String>();
                    Set<String> unitNames = new Set<String>();
                    if (String.isNotBlank(lstFmReceipt[0].Registration_Id__c)) {
                        registrationIds.add(lstFmReceipt[0].Registration_Id__c);
                        unitNames.add(lstFmReceipt[0].Unit_Name__c);
                    }
                    String customerTrxId;
                    for (FM_Unit_Invoice_Payment__c unitInvoice : lstFmReceipt[0].FM_Unit_Invoice_Payments__r) {
                        registrationIds.add(unitInvoice.Registration_Id__c);
                        unitNames.add(unitInvoice.Unit_Name__c);

                        //if (Label.AdvanceServiceChargePayment.equalsIgnoreCase(lstFmReceipt[0].Discount_Applied__c)
                        //    && unitInvoice.Is_Next_Q1_Invoice__c
                        //) {
                        //    customerTrxId = unitInvoice.TRX_Id__c;
                        //}
                    }

                    String unitName = '';
                    for (String uName : unitNames) {
                        unitName += uName + ',';
                    }
                    unitName = unitName.removeEnd(',');

                    request.comments = (Label.FMGuestPayment.equalsIgnoreCase(lstFmReceipt[0].Payment_Type__c)
                                            ? 'Guest ' : 'Portal ') + 'Payment for Service Charges for ' + unitName;

                    //if (Label.AdvanceServiceChargePayment.equalsIgnoreCase(lstFmReceipt[0].Discount_Applied__c)) {
                    //    request.customerTrxId = customerTrxId;
                    //}
                    request.applicationLines = new List<FmIpmsRestServices.ApplicationLine>();
                    for (String regId : registrationIds) {
                        FmIpmsRestServices.ApplicationLine line = new FmIpmsRestServices.ApplicationLine();
                        line.registrationId = regId;
                        line.amountToApply = lstFmReceipt[0].Amount__c;
                        request.applicationLines.add(line);
                    }


                    if (!OTHERS.equalsIgnoreCase(lstFmReceipt[0].Guest_Payment_Type__c)) {
                        if(lstFmReceipt[0].Made_Call_To_IPMS__c != True) {
                            System.debug('request for FmIpmsRestService is: '+request);
                            lstFmReceipt[0].Made_Call_To_IPMS__c = true;
                            receiptUrl = FmIpmsRestServices.createReceipt(request);
                        }
                    }

                    //System.debug('receiptUrl : '+receiptUrl);

                    if (String.isNotBlank(receiptUrl)) {
                        lstFmReceipt[0].IpmsReceiptUrl__c = receiptUrl;
                        lstFmReceipt[0].Receipt_created_in_IPMS__c = 'TRUE';
                        System.debug('lstFmReceipt[0].Payment_Type__c : '+lstFmReceipt[0].Payment_Type__c);
                        if (Label.FMGuestPayment.equalsIgnoreCase(lstFmReceipt[0].Payment_Type__c)) {
                            System.debug('----Inside If----');
                            System.debug('lstFmReceipt[0].Id : '+lstFmReceipt[0].Id);
                            System.debug('receiptUrl : '+receiptUrl);
                            emailReceiptAndSoa(lstFmReceipt[0].Id, receiptUrl);
                        }
                    } else {
                        lstFmReceipt[0].Receipt_created_in_IPMS__c = 'FALSE';
                        emailReceiptAndSoa(lstFmReceipt[0].Id, '');
                    }
                }
            } else {
                insert new Error_Log__c(
                    Process_Name__c = 'Update Receipt',
                    FM_Receipt__c = lstFmReceipt[0].Id,
                    Error_Details__c = 'Error from Payment Gateway\nresponse:\n' + JSON.serializePretty(params)
                );
            }

            try {
                update lstFmReceipt;
            } catch(Exception excp) {
                insert new Error_Log__c(
                    Process_Name__c = 'Update Receipt',
                    FM_Receipt__c = lstFmReceipt[0].Id,
                    Error_Details__c = lstFmReceipt[0].Id + ': ' + excp.getMessage()
                );
                ApexPages.addMessage(new ApexPages.message(
                    ApexPages.Severity.ERROR, 'Error: Error occured while saving receipt - ' + excp.getMessage()
                ));
            }

            if ('Success'.equalsIgnoreCase(lstFmReceipt[0].Order_Status__c)) {
                PageReference successPage = Page.CommunityPaymentSuccess;
                successPage.getParameters().put('id', lstFmReceipt[0].Id);
                return successPage;
            }
        }
        return NULL;
    }

    public static Boolean emailReceiptAndSoaSync(Id fmReceiptId, String receiptUrl) {
        System.debug('emailReceiptAndSoaSync invoked ');
        List<String> lstBuildingFMEmail = new List<String>();
        String buildingFMEmail;
        FM_Receipt__c fmReceipt = [
            SELECT  Id
                    , Name
                    , Account__r.Name
                    , Customer_Party_Id__c
                    , Registration_Id__c
                    , Unit_Name__c
                    , Amount__c
                    , CustomerEmail__c
                    , Guest_Payment_Type__c
                    , Booking_Unit__c
                    , Booking_Unit__r.Building_Name__c
                    , Payment_Type__c
                    , Order_Status__c
                    , Status_Code__c
                    , Status_Message__c
                    , CreatedDate
                    , Bank_Reference_Number__c
                    , Bank_Receipt_Number__c
            FROM    FM_Receipt__c
            WHERE   Id = :fmReceiptId
        ];

        if(fmReceipt.Booking_Unit__c != NULL && String.isNotBlank(fmReceipt.Booking_Unit__r.Building_Name__c)){
            List<Location__c> lstBuilding = [
                SELECT  Id
                        , Name
                        , FM_Email__c
                FROM    Location__c
                WHERE   Name = :fmReceipt.Booking_Unit__r.Building_Name__c
            ];
            if (!lstBuilding.isEmpty() && String.isNotBlank(lstBuilding[0].FM_Email__c)) {
                buildingFMEmail = lstBuilding[0].FM_Email__c;
                lstBuildingFMEmail.add(buildingFMEmail);
                //System.debug('buildingFMEmail : '+buildingFMEmail);
            }
        }

        System.debug('receiptUrl = ' + receiptUrl);

        // if (String.isNotBlank(receiptUrl)) {

            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

            email.setSubject('Receipt for Payment of Service Charges against ' + fmReceipt.Unit_Name__c + ' - ' + fmReceipt.Name);

            String emailBody = 'Hello ' + fmReceipt.Account__r.Name + ',\n\n\n';
            emailBody += 'We have received a payment of AED '
                        + ((fmReceipt.Amount__c == NULL) ? '' : String.valueOf(fmReceipt.Amount__c.setScale(2)))
                        + ' for Unit ' + fmReceipt.Unit_Name__c + '.\n\n';

            emailBody += 'Payment Type : ' + fmReceipt.Payment_Type__c + '\n';
            emailBody += 'Guest Payment Type : ' + fmReceipt.Guest_Payment_Type__c + '\n';
            emailBody += 'Unit Name : ' + fmReceipt.Unit_Name__c + '\n';
            emailBody += 'Customer Name : ' + fmReceipt.Account__r.Name + '\n';
            emailBody += 'Amount (AED) : ' + fmReceipt.Amount__c + '\n';
            emailBody += 'Status : ' + fmReceipt.Order_Status__c + '\n';
            emailBody += 'Status Code : ' + fmReceipt.Status_Code__c + '\n';
            emailBody += 'Message : ' + fmReceipt.Status_Message__c + '\n';
            emailBody += 'Payment Timestamp : ' + fmReceipt.CreatedDate + '\n';
            emailBody += 'Bank Reference Number : ' + fmReceipt.Bank_Reference_Number__c + '\n';
            emailBody += 'Bank Receipt Number : ' + fmReceipt.Bank_Receipt_Number__c + '\n\n';

      if(SERVICE_CHARGE.equalsIgnoreCase(fmReceipt.Guest_Payment_Type__c)) {
        emailBody += 'Please find the attached Payment Receipt.'
                        + '\n\n';
      }
            emailBody += 'Regards,\nCommunity Portal';
            email.setPlainTextBody(emailBody);

            email.setOrgWideEmailAddressId(Label.CommunityPortalOrgWideAddressId);
            email.setToAddresses(new List<String> {fmReceipt.CustomerEmail__c});
            email.setWhatId(fmReceipt.Id);

            if(OTHERS.equalsIgnoreCase(fmReceipt.Guest_Payment_Type__c)){
                email.setCcAddresses(lstBuildingFMEmail);
            }

            List<Messaging.EmailFileAttachment> attachments = new List<Messaging.EmailFileAttachment>();


            HttpRequest req = new HttpRequest();
            HttpResponse res;
            if (String.isNotBlank(receiptUrl)) {
                req.setEndpoint(receiptUrl);
                req.setMethod('GET');
                res = new Http().send(req);
            }

            if(SERVICE_CHARGE.equalsIgnoreCase(fmReceipt.Guest_Payment_Type__c)) {
                Messaging.EmailFileAttachment receipt = new Messaging.EmailFileAttachment();
                receipt.setFileName(fmReceipt.Name + '.pdf');
                receipt.setBody(/*EncodingUtil.base64Encode(*/res.getBodyAsBlob()/*)*/);
                attachments.add(receipt);
            }

            String unitSoaUrl = FmIpmsRestServices.getUnitSoaByRegistrationId(fmReceipt.Registration_Id__c);
            System.debug('unitSoaUrl = ' + unitSoaUrl);
            if (String.isNotBlank(unitSoaUrl)) {

                req = new HttpRequest();
                req.setEndpoint(unitSoaUrl);
                req.setMethod('GET');
                res = new Http().send(req);

                Messaging.EmailFileAttachment soa = new Messaging.EmailFileAttachment();
                soa.setFileName('Service Charge SOA for ' + fmReceipt.Unit_Name__c + '.pdf');
                soa.setBody(/*EncodingUtil.base64Encode(*/res.getBodyAsBlob()/*)*/);
                attachments.add(soa);
            }

            email.setFileAttachments(attachments);


            List<Messaging.SendEmailResult> sendEmailResult = Messaging.sendEmail(
                new List<Messaging.SingleEmailMessage> {email}
            );

            return sendEmailResult[0].isSuccess();
        // }
        // return false;
    }

    @future(callout=true)
    public static void emailReceiptAndSoa(Id fmReceiptId, String receiptUrl) {
        emailReceiptAndSoaSync(fmReceiptId, receiptUrl);
    }

    public void getPaymentResponse(Map<String, String> params) {
        CCAvenuePaymentGateway gateway = CCAvenuePaymentGateway.getInstance(LoamsCommunityController.GATEWAY_NAME);

        if (gateway == NULL) {
            return;
        }

        params = gateway.getDecryptedResponse(params);

        orderNo = params.get('orderNo');
        status = params.get('order_status');
        statusCode = params.get('status_code');
        statusMessage = params.get('status_message');
        amount = params.get('amount');
        String orderId = params.get('order_id');
        if (String.isBlank(orderNo) && String.isNotBlank(orderId)) {
            orderNo = orderId;
        }
    }
}