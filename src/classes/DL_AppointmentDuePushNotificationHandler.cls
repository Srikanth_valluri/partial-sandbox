/*
Class Name : DL_AppointmentDuePushNotificationHandler
Description : To send push notification asyncronously to CSR Case Approver
Test Class : DL_AppointmentDuePushNotifyHandlerTest

========================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
--------------------------------------------------------------------------------------------------------
1.0     |   01-12-2020      | Subin Antony        | Initial Draft
*******************************************************************************************************/
global class DL_AppointmentDuePushNotificationHandler {
    @testVisible
    private static final String push_notification_sandbox_baseurl = 'https://ptctest.damacgroup.com/hellodamac'; // 'https://stgservices.damacgroup.com';
    @testVisible
    private static final String push_notification_prod_baseurl = 'https://ptc.damacgroup.com/hellodamac-prod'; // 'https://services.damacgroup.com';
    @testVisible
    private static final String push_notification_endpoint = '/api/v1/notifications/trigger-push'; // '/api/PushNotification/PushMessage';
    @testVisible
    private static final String api_token_stg = 'c13d9a4e2be67d853d7428f5a9a952d6'; // 'AD9A501EC7611D124681D5DA0833F540';
    @testVisible
    private static final String api_token_prd = 'c13d9a4e2be67d853d7428f5a9a952d6'; // 'AD9A501EC7611D124681D5DA0833F540';
    @testVisible
    private static final String access_token_stg = 'FE42DF6EBCC6902B26B45D62B567AD19'; // 'FE42DF6EBCC6902B26B45D62B567AD19';
    @testVisible
    private static final String access_token_prd = '89BBB3F7241282C534719C2C875B66BD'; // '89BBB3F7241282C534719C2C875B66BD';
    
    @testVisible
    private static Boolean isSandbox;
    static {
        isSandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }
    
    global class AppointmentDataWrap {
        @InvocableVariable(label='Account ID' required=true)
        public String accountId;
        @InvocableVariable(label='Party ID' required=true)
        public String partyId;
        @InvocableVariable(label='Unit ID' required=true)
        public String unitId;
        @InvocableVariable(label='Registration ID' required=true)
        public String regId;
        @InvocableVariable(label='Calling List ID' required=true)
        public String appointmentId;
        @InvocableVariable(label='Appointment Process' required=true)
        public String processName;
        @InvocableVariable(label='Appointment Start Date and Time' required=true)
        public DateTime appointmentStartDateTime;
        @InvocableVariable(label='Appointment Slot' required=true)
        public String appointmentSlot;
    }
    
    @InvocableMethod(label='Send push notification: Appointment reminder 24 hr prior.')
    public static void sendPushNotification(List<AppointmentDataWrap> appointmentList) {
        for(AppointmentDataWrap appointmentDetail : appointmentList) {
            pushNotificationServiceCallout(appointmentDetail.accountId, 
                                           appointmentDetail.partyId, 
                                           appointmentDetail.unitId, 
                                           appointmentDetail.regId, 
                                           appointmentDetail.appointmentId, 
                                           appointmentDetail.processName, 
                                           appointmentDetail.appointmentStartDateTime.format('dd-MMM-yyyy'), 
                                           appointmentDetail.appointmentSlot);
        }
    }
    
    @future(callout = true)
    public static void pushNotificationServiceCallout(String accountId, String partyId, String unitId, String regId, 
    String appointmentId, String processName, String dateString, String timeSlot) {
        String jsonBody = '   {  '  + 
        '       "email": "'+accountId+'",  '  + 
        '       "message": "Your appointment for '+processName+' on '+dateString+', '+timeSlot+' is scheduled. Hope to see you soon!",  '  + 
        '       "title": "Gentle reminder regarding your appointment!",  '  + 
        '       "payload": {  '  + 
        '           "screen": "appointments",  '  + 
        '           "account_id": "'+accountId+'",  '  + 
        '           "party_id": "'+partyId+'",  '  + 
        '           "unit_id": "'+unitId+'",  '  + 
        '           "reg_id": "'+regId+'",  '  + 
        '           "appointment_id": "'+appointmentId+'",  '  + 
        '       },  '  + 
        '       "total_pending_count": 0,  '  + 
        '       "image_url": "https://services.damacgroup.com/DCAssets/hello-damac/notifications/txn_success.png",  '  + 
        '       "source_application_id": 9,  '  + 
        '       "dest_application_id": 2  '  + 
        '  }  ' ; 
        
        String accessToken = isSandbox ? access_token_stg : access_token_prd;
        String apiToken = isSandbox ? api_token_stg : api_token_prd;
        String endpoint = isSandbox ? push_notification_sandbox_baseurl : push_notification_prod_baseurl;
        endpoint += push_notification_endpoint;
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpoint);
        request.setMethod('POST');
        request.setBody(jsonBody);
        request.setHeader('Access-Token', accessToken);
        request.setHeader('API-Token', apiToken);
        request.setHeader('Content-Type', 'application/json');
        HttpResponse response;
        if(Test.isRunningTest()) {
            response = new HttpResponse();
            response.setStatus('OK');
            response.setStatusCode(200);
            response.setBody('{"meta_data":{"title":"OK","status_code":1,"message":"Successfully pushed message","developer_message":null}}');
        } else {
            response = http.send(request);
        }
        
        System.debug('requestBody:: ' + request.getBody());
        System.debug('response:: ' + response);
        System.debug('response body :: ' + response.getBody());
    }
}