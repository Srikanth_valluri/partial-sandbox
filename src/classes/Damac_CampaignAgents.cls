@RestResource(urlMapping='/CampaignAgents/*')
global without sharing class Damac_CampaignAgents {

    @HttpGET
    global static void doGET (){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String userId = req.params.get('userID');  
        if (userId != NULL && userId != '') {
            User loggedInUser = [SELECT Stand_Id__c FROM USER WHERE Id =: userId ];
            if (loggedInUser.Stand_Id__c != NULL) {
                String campaignId = loggedInUser.Stand_Id__c;
            
                Campaign__c camp = new Campaign__c ();
                camp = [SELECT Campaign_Name__c, Broker_Included__c FROM Campaign__c WHERE ID =: campaignId];
                String responseBody = '{"Campaign Name" : "'+camp.Campaign_Name__c+'", "Broker_Included" : '+camp.Broker_Included__c+', "Assigned_Agents" : ';
                List <AgencyDetails> agenciesList = new List<AgencyDetails> ();
                for (Assigned_Agent__c agent : [SELECT Name, Agency__c, Agency_Name__c, Agent_Email__c,
                                                Contact__c, Contact__r.Name, User__c, User__r.Name 
                                                FROM Assigned_Agent__c 
                                                WHERE Campaign__c =: campaignId AND User__r.IsActive = TRUE])
                {
                    AgencyDetails obj = new AgencyDetails ();
                    obj.agencyName = agent.Agency_Name__c;
                    obj.agencySFId = agent.Agency__c;
                    obj.agentEmail = agent.Agent_Email__c != NULL ? agent.Agent_Email__c : '';
                    obj.agencyContactSFId = agent.Contact__c;
                    obj.agencyContactName = agent.Contact__r.Name;
                    obj.agencyUserSFId = agent.User__c;
                    obj.agencyUserName = agent.User__r.Name;
                    agenciesList.add (obj);
                }
                responseBody += JSON.serialize(agenciesList)+'}';
                System.Debug (responseBody);
                RestContext.response.responseBody = Blob.valueOf(responseBody);
            
            } else {
                RestContext.response.responseBody = Blob.valueOf('X_RETURN_STATUS:No Campaign associated for this User.');
            }
        } else {
            RestContext.response.responseBody = Blob.valueOf('X_RETURN_STATUS:Pass Logged In User Id as a parameter.');
        }
    }
    
    public class AgencyDetails {
        public String agencyName;
        public String agencySFId;
        public String agentEmail;
        public String agencyContactSFId;
        public String agencyContactName;
        public String agencyUserSFId;
        public String agencyUserName;        
    }
}