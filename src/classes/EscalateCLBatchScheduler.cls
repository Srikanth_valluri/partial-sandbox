public class EscalateCLBatchScheduler Implements Schedulable  {
     public void execute(SchedulableContext SC){
        System.debug('inside scheduler');
        EscalateCLBatch batchInst = new EscalateCLBatch();
        Database.executeBatch(batchInst,50);
    }
}