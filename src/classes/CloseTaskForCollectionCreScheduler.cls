global class CloseTaskForCollectionCreScheduler implements Schedulable {
    global void execute(SchedulableContext sc) {
        CloseTaskForCollectionCreBatch batchInstance = new CloseTaskForCollectionCreBatch(); 
        Database.executeBatch(batchInstance);
    }
}