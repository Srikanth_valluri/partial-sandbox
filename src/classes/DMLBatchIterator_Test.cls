@isTest(seeAlldata = true)
global class DMLBatchIterator_Test {

    global static testmethod void m1(){

        database.executebatch(new DMLBatchIterator(new list<sobject>(), 'insert'));
        database.executebatch(new DMLBatchIterator(new list<sobject>(), 'update'));
        database.executebatch(new DMLBatchIterator(new list<sobject>(), 'delete'));

        list<Inquiry__c> records = new list<Inquiry__c>();
        records.add(new Inquiry__c(first_Name__c = 'test1', last_Name__c = 'test2'));
        records.add(new Inquiry__c(first_Name__c = 'test1', last_Name__c = 'test2'));
        records.add(new Inquiry__c(first_Name__c = 'test1', last_Name__c= 'test2'));
        list<sobject> data = new list<sobject>();
        data = (list<sobject>) records;

        database.executebatch(new DMLBatchIterator(data, 'insert'));
        database.executebatch(new DMLBatchIterator(database.query('select id from Inquiry__c limit 1'), 'update'));
        database.executebatch(new DMLBatchIterator(database.query('select id from Inquiry__c limit 1'), 'delete'));
    }

}