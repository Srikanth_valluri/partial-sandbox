/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SendGridWebHookServiceTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        objAcc.Country__c = 'United Arab Emirates';
        insert objAcc ;
        
        list<EmailMessage> lstEmails = new list<EmailMessage>();
        EmailMessage mail = new EmailMessage();
        mail.Subject = 'Test';
        mail.MessageDate = System.Today();
        mail.Status = '3';
        mail.RelatedToId = objAcc.Id;
        mail.ToAddress = 'test@new.com';
        mail.FromAddress = 'test@damac.com';
        mail.TextBody = 'Test TEST1';
        mail.CcAddress = '';
        mail.BccAddress = '';
        mail.Sent_By_Sendgrid__c = true;
        mail.SentGrid_MessageId__c = 'ayfwe236dg8s74cu';
        lstEmails.add(mail);
        
        EmailMessage mail1 = new EmailMessage();
        mail1.Subject = 'Test';
        mail1.MessageDate = System.Today();
        mail1.Status = '3';
        mail1.RelatedToId = objAcc.Id;
        mail1.ToAddress = 'test@test.com';
        mail1.FromAddress = 'test@damac.com';
        mail1.TextBody = 'Test TEST1';
        mail1.CcAddress = '';
        mail1.BccAddress = '';
        mail1.Sent_By_Sendgrid__c = true;
        mail1.SentGrid_MessageId__c = 'sgfqueybdhxQIWIXN';
        lstEmails.add(mail1);
        
        EmailMessage mail2 = new EmailMessage();
        mail2.Subject = 'Test';
        mail2.MessageDate = System.Today();
        mail2.Status = '3';
        mail2.RelatedToId = objAcc.Id;
        mail2.ToAddress = 'new@new.com';
        mail2.FromAddress = 'test@damac.com';
        mail2.TextBody = 'Test TEST1';
        mail2.CcAddress = '';
        mail2.BccAddress = '';
        mail2.Sent_By_Sendgrid__c = true;
        mail2.SentGrid_MessageId__c = 'bwyrgwuyerfgwfcbhd';
        lstEmails.add(mail2);
        
        EmailMessage mail3 = new EmailMessage();
        mail3.Subject = 'Test';
        mail3.MessageDate = System.Today();
        mail3.Status = '3';
        mail3.RelatedToId = objAcc.Id;
        mail3.ToAddress = 'new@testing.com';
        mail3.FromAddress = 'test@damac.com';
        mail3.TextBody = 'Test TEST1';
        mail3.CcAddress = '';
        mail3.BccAddress = '';
        mail3.Sent_By_Sendgrid__c = true;
        mail3.SentGrid_MessageId__c = 'oewurwgetwfetyrt';
        lstEmails.add(mail3);
        insert lstEmails;
        
        list<SendGridWebHookService.eventWrapper> lstWrapper = new list<SendGridWebHookService.eventWrapper>();
        SendGridWebHookService.eventWrapper objWrapper = new SendGridWebHookService.eventWrapper();
        objWrapper.email = 'test@new.com';
        objWrapper.timestamp = 1513299569; 
        objWrapper.smtp_id = 'ashfbugriropigo';
        objWrapper.event = 'open';
        objWrapper.category = 'cat facts';
        objWrapper.sg_event_id = 'fqbuybafsuy74u7';
        objWrapper.sg_message_id = 'ayfwe236dg8s74cu.34vw34738b92n0902';
        objWrapper.response = '250 OK';
        objWrapper.attempt = 'success';
        objWrapper.useragent = 'Mozilla/4.0 (compatible; MSIE 6.1; Windows XP; .NET CLR 1.1.4322; .NET CLR 2.0.50727)';
        objWrapper.ip = '255.255.255.255';
        objWrapper.url = 'http://www.sendgrid.com/';
        objWrapper.reason = '';
        objWrapper.status = 'new'; 
        objWrapper.asm_group_id = 10;  
        objWrapper.sendsubject = 'Test'; 
        
        SendGridWebHookService.eventWrapper objWrapperDelivered = new SendGridWebHookService.eventWrapper();
        objWrapperDelivered.email = 'test@test.com';
        objWrapperDelivered.timestamp = 1513299569; 
        objWrapperDelivered.smtp_id = 'ashfbugriropigo';
        objWrapperDelivered.event = 'delivered';
        objWrapperDelivered.category = 'cat facts';
        objWrapperDelivered.sg_event_id = 'fqbuybafsuy74u7';
        objWrapperDelivered.sg_message_id = 'sgfqueybdhxQIWIXN.34vw34738b92n0902';
        objWrapperDelivered.response = '250 OK';
        objWrapperDelivered.attempt = 'success';
        objWrapperDelivered.useragent = 'Mozilla/4.0 (compatible; MSIE 6.1; Windows XP; .NET CLR 1.1.4322; .NET CLR 2.0.50727)';
        objWrapperDelivered.ip = '255.255.255.255';
        objWrapperDelivered.url = 'http://www.sendgrid.com/';
        objWrapperDelivered.reason = '';
        objWrapperDelivered.status = 'new'; 
        objWrapperDelivered.asm_group_id = 10;  
        objWrapperDelivered.sendsubject = 'Test';
        
        SendGridWebHookService.eventWrapper objWrapperBounced = new SendGridWebHookService.eventWrapper();
        objWrapperBounced.email = 'new@new.com';
        objWrapperBounced.timestamp = 1513299569; 
        objWrapperBounced.smtp_id = 'ashfbugriropigo';
        objWrapperBounced.event = 'bounce';
        objWrapperBounced.category = 'cat facts';
        objWrapperBounced.sg_event_id = 'fqbuybafsuy74u7';
        objWrapperBounced.sg_message_id = 'bwyrgwuyerfgwfcbhd.34vw34738b92n0902';
        objWrapperBounced.response = '250 OK';
        objWrapperBounced.attempt = 'success';
        objWrapperBounced.useragent = 'Mozilla/4.0 (compatible; MSIE 6.1; Windows XP; .NET CLR 1.1.4322; .NET CLR 2.0.50727)';
        objWrapperBounced.ip = '255.255.255.255';
        objWrapperBounced.url = 'http://www.sendgrid.com/';
        objWrapperBounced.reason = 'Email Id is not valid';
        objWrapperBounced.status = 'new'; 
        objWrapperBounced.asm_group_id = 10;  
        objWrapperBounced.sendsubject = 'Test';
        
        SendGridWebHookService.eventWrapper objWrapperClicked = new SendGridWebHookService.eventWrapper();
        objWrapperClicked.email = 'new@testing.com';
        objWrapperClicked.timestamp = 1513299569; 
        objWrapperClicked.smtp_id = 'ashfbugriropigo';
        objWrapperClicked.event = 'click';
        objWrapperClicked.category = 'cat facts';
        objWrapperClicked.sg_event_id = 'fqbuybafsuy74u7';
        objWrapperClicked.sg_message_id = 'oewurwgetwfetyrt.34vw34738b92n0902';
        objWrapperClicked.response = '250 OK';
        objWrapperClicked.attempt = 'success';
        objWrapperClicked.useragent = 'Mozilla/4.0 (compatible; MSIE 6.1; Windows XP; .NET CLR 1.1.4322; .NET CLR 2.0.50727)';
        objWrapperClicked.ip = '255.255.255.255';
        objWrapperClicked.url = 'http://www.sendgrid.com/';
        objWrapperClicked.reason = 'Email Id is not valid';
        objWrapperClicked.status = 'new'; 
        objWrapperClicked.asm_group_id = 10;  
        objWrapperClicked.sendsubject = 'Test';
        
        lstWrapper.add(objWrapper);
        lstWrapper.add(objWrapperDelivered);
        lstWrapper.add(objWrapperBounced);
        lstWrapper.add(objWrapperClicked);
        
        String strJSON = JSON.serialize(lstWrapper);
        
        RestRequest request = new RestRequest();
        request.requestUri = URL.getSalesforceBaseUrl()+'/services/apexrest/webhook';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(strJSON);
        
        RestContext.request = request;
        String response = SendGridWebHookService.doPost();
        
    }
}