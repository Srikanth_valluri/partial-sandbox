/****************************************************************************************************
* Name          : BitlyCallout                                                                      *
* Description   : Class to call the Bitly functions                                                 *
* Created Date  : 16/04/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                *
* 1.0         Craig Lobo    16/04/2018      Initial Draft.                                          *
****************************************************************************************************/
global class BitlyCallout {

    public static Map<String, Object> responseMap;

    /**
      * BitlyCallout
      */
    public static Map<String, Object> getShortenedURL(
        String pURLParameters, 
        String shareType,
        String userType
    ) {

        if (String.isNotBlank(pURLParameters) && String.isNotBlank(shareType)) {
            String pagename;
            if (userType == 'isPortal') {
                if(shareType.equalsIgnoreCase('text')){
                    //pagename='/Project_Details_mobile';
                } else {
                    pagename='/AP_ProjectDetails';
                }
            } else {
                if(shareType.equalsIgnoreCase('text')){
                    pagename='/Project_Details_mobile';
                }else {
                    pagename='/Project_Details';
                }
            }
            String publicLink = Label.Project_Details_Public_Link; 
            //'https://fullcopy-damacproperties.cs87.force.com/ProjectDetails?';
            System.debug('publicLink>>>: '+publicLink);
            if(String.isNotBlank(pagename)){
                publicLink += pagename;
            }
            publicLink += pURLParameters; 
            System.debug('publicLink: '+publicLink);
            System.debug('pURLParameters: '+pURLParameters);
            
            String token = Label.Bitly_Token; //'9a27b7eeace2bc1b04943a72b97ae94433adca0c';
            //String endPointURL = Label.Bitly_EndPoint_URL; //'https://api-ssl.bitly.com';

            HttpRequest req = new HttpRequest();
            String endPoint = Label.Bitly_EndPoint_URL
                            + '?access_token=' 
                            + token 
                            + '&longUrl=' 
                            + EncodingUtil.urlEncode( publicLink, 'UTF-8' ) 
                            + '&format=json';

            req.setEndpoint(endPoint);
            req.setMethod('GET');
            System.debug('endPoint: '+endPoint);
            System.debug('req: '+req);
            Http http = new Http();
            if (!Test.isRunningTest ()) {
                HttpResponse res = http.send(req);
                System.debug('res>>>>>: '+res);
                System.debug('res.getBody()>>>>>: '+res.getBody());
                responseMap = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
                if(responseMap.containskey('status_code') && responseMap.containskey('status_txt')){
                    if(responseMap.get('status_code') != 403 && responseMap.get('status_txt') !='MONTHLY_RATE_LIMIT_EXCEEDED'){
                        System.debug('responseMap>>>>: '+responseMap.get('status_txt'));
                        return responseMap;
                    }else{
                        System.debug('null>>>>>>>>');
                        return null;
                    }
                }
            }
            System.debug('res: '+responseMap);
        }

        return responseMap;
    }

}