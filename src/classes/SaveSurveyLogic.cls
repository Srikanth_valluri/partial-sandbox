/**
 * Logic class for SaveSurveyWebService
 */
public class SaveSurveyLogic {

    /**
     * Method to process request and send the response
     */
    public static void processSaveSurveyRequest() {

        // Get request body
        String requestBody = RestContext.request.requestBody.toString();
        System.debug('============= requestBody : ' + requestBody);
        SaveSurveyResponseBody response = new SaveSurveyResponseBody();

        try {
            // Parse request body
            SaveSurveyRequestBody request = (SaveSurveyRequestBody) JSON.deserializeStrict(
                requestBody,
                SaveSurveyRequestBody.Class
            );

            User cre = getCreForExtension(request.extension);
            if (cre == null) {
                response.errorMessage = 'No CRE User found for the given extension';
                RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
                Restcontext.response.statusCode = 400;
                return;
            }
            Account account = getAccountForCallingNumber(request.callingNumber);
            // commented by AK as requested by Kanu on 15th Dec 2019 at DAMAC Dubai Ofc
            /*if (account == null) {
                response.errorMessage = 'No Account found for the given calling number';
                RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
                Restcontext.response.statusCode = 400;
                return;
            }*/
            DateTime callDateTime = CtiTntegrationUtility.getDateTimeForUserTimeZone(request.calledDateTime);
            if (callDateTime == null) {
                response.errorMessage = 'Please enter valid date format (MM/DD/YYYY HH:MM:SS)';
                RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
                Restcontext.response.statusCode = 400;
                return;
            }
            CRE_Survey_Feedback__c surveyFeedbackList = createSurveyFeedbackRequest(account, cre, request,callDateTime);
        } catch (Exception ex) {
            response.errorMessage = ex.getMessage();
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
            Restcontext.response.statusCode = 400;
            System.debug('============= response : ' + JSON.serialize(response));
            return;
        }
        response.status = 'Survey Saved';

        System.debug('============= response : ' + JSON.serialize(response));
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
        Restcontext.response.statusCode = 200;
    }


    /**
     * Create call back request
     */
    public static CRE_Survey_Feedback__c createSurveyFeedbackRequest(
        Account account,
        User cre,
        SaveSurveyRequestBody request,
        DateTime callDateTime
    ) {
        CRE_Survey_Feedback__c surveyFeedbackList = new CRE_Survey_Feedback__c();
        surveyFeedbackList.CRE__c = cre.Id;
        surveyFeedbackList.Rating__c = Integer.ValueOf(request.rating);
        //surveyFeedbackList.Called_Date_Time__c = DateTime.parse(request.calledDateTime);
        /*DateTime callDateTime = getDateTimeForUserTimeZone(request.calledDateTime);
        if(callDateTime == null) {
            return surveyFeedbackList;
        }*/
        surveyFeedbackList.Called_Date_Time__c = callDateTime;
        surveyFeedbackList.Calling_Number__c = request.callingNumber;
        surveyFeedbackList.Extension__c = Decimal.ValueOf(request.extension);
        if( account != null ) {
            surveyFeedbackList.Account__c = account.Id;
        }

        insert surveyFeedbackList;
        return surveyFeedbackList;
    }

    /**
     * Method get account for the calling number
     */
    public static Account getAccountForCallingNumber(String callingNumber) {
/*
        List<Account> accounts =
            [
                SELECT
                    Id,
                    Phone
                FROM
                    Account
                WHERE
                    Mobile_Phone_Encrypt__pc =: callingNumber
                    OR Mobile_Phone_Encrypt_2__pc =: callingNumber
                    OR Mobile_Phone_Encrypt_3__pc =: callingNumber
                    OR Mobile_Phone_Encrypt_4__pc =: callingNumber
                    OR Mobile_Phone_Encrypt_5__pc =: callingNumber
            ];
*/
        List<Account> accounts = CtiTntegrationUtility.getRegisteredAccount(callingNumber);
        if (accounts.size() > 0) {
            return accounts[0];
        } else {
            return null;
        }
    }

    /**
     * Method to CRE User for the extension
     */
    public static User getCreForExtension(String extension) {
        List<User> creUsers =
            [
                SELECT
                    Id,
                    Extension
                FROM
                    User
                WHERE
                    Extension =: extension
            ];
        if (creUsers.size() > 0) {
            return creUsers[0];
        } else {
            return null;
        }
    }

    /**
    /**
    * Method to get datetime for passed string in user timezone.
    * @param: dateTimeString: Date time value in string format
    * @return: DateTime
    **/
    /*public static DateTime getDateTimeForUserTimeZone(String dateTimeString) {

        DateTime newDateTime;
        if(String.isBlank(dateTimeString)) {
            return newDateTime;
        }
        list<String> spiltStrList = dateTimeString.split(' ');
        if(spiltStrList.size() >= 2) {
            list<String> dateComponentsList = spiltStrList[0].split('/');
            list<String> timeComponentsList = spiltStrList[1].split(':');

            if(dateComponentsList.size() == 3 && timeComponentsList.size() == 3) {
                newDateTime = DateTime.newInstance(Integer.valueOf(dateComponentsList[2]),
                                                   Integer.valueOf(dateComponentsList[0]),
                                                   Integer.valueOf(dateComponentsList[1]),
                                                   Integer.valueOf(timeComponentsList[0]),
                                                   Integer.valueOf(timeComponentsList[1]),
                                                   Integer.valueOf(timeComponentsList[2]));
            }

            return newDateTime;
        }
        return newDateTime;
    }*/

    /**
     * Wrapper class for SaveSurveyWebService request body
     */
    public class SaveSurveyRequestBody {
        public String callingNumber {get; set;}
        public String extension {get; set;}
        public String calledDateTime {get; set;}
        public String rating {get; set;}

        public SaveSurveyRequestBody() {
            this.callingNumber = '';
            this.extension = '';
            this.calledDateTime = '';
            this.rating = '';
        }
    }

    /**
     * Wrapper class for SaveSurveyWebService response body
     */
    public class SaveSurveyResponseBody {
        public String status;
        public String errorCode;
        public String errorMessage;

        public SaveSurveyResponseBody() {
            this.status = '';
            this.errorCode = '';
            this.errorMessage = '';
        }
    }
}