/**
 * @File Name          : MortgageAfterApprovalProcess.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 7/8/2019, 3:19:32 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    7/8/2019, 3:08:02 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public class MortgageAfterApprovalProcess implements Schedulable { //AKISHOR: for sending A flag after manager approval   

    public List<Case> closedTaskLst;

    @InvocableMethod
    public static void SendAMortgageFlagOnApproval(List<Case> closedTaskLst) {

        system.debug('--closedTaskLst:--'+closedTaskLst);
        MortgageAfterApprovalProcess m = new MortgageAfterApprovalProcess();
        m.closedTaskLst = closedTaskLst;
        DateTime dt = system.now().addMinutes(1);
        String day = string.valueOf(dt.day());
        String month = string.valueOf(dt.month());
        String hour = string.valueOf(dt.hour());
        String minute = string.valueOf(dt.minute());
        String second = string.valueOf(dt.second());
        String year = string.valueOf(dt.year());
        String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
        
        String jobID = system.schedule('Send A Mortgage Flag '+ system.now(), strSchedule, m);

        

    }
    
    
    public void execute(SchedulableContext ctx) {
        // Invoke Apex_Class_ON_OFF_Setting and check wether that class is on/off.
        Boolean apexOnOffSetting = getApexSettingTocheck('MortgageAfterApprovalProcess');

        if(apexOnOffSetting){
            System.debug('closedTaskLst:::::::::'+closedTaskLst);
                     
            Set<Id> setCasesIds = new Set<Id>();
            setCasesIds.add(closedTaskLst[0].Id);        
    
            System.debug('~~~~~~~~setCasesIds:::::::::'+setCasesIds);
            calloutInfutureToSendMortgageFlag(setCasesIds);
        }
    }   
    
    @future(callout=true)
    public static void calloutInfutureToSendMortgageFlag(set<Id>setCasesIds){
      System.debug('~~~~~~~~in future :::::::::'+setCasesIds);
      if( !setCasesIds.isEmpty() ) {
          System.debug('~~~~~~~~in future not empty:::::::::'+setCasesIds);
      list<Case> lstOfCases = [ SELECT Id
                                            , CaseNumber
                                            , Booking_Unit__r.Registration_ID__c
                                            , Submit_Date__c
                                            , Mortgage_M_Flag_Date__c
                                            , Loan_Offer_Date__c
                                            , NOC_Request_Date__c
                                            , Loan_Amount__c
                                            , Bank_Code__c
                                            , NOC_Received_Date__c
                                            , Mortgage_Flag__c
                                            , Bank_Name__c
                                            , Approval_Status__c
                                            , Loan_Agreement_Date__c
                                            , Registration_ID__c
                                            , Owner.Name
                                            FROM Case
                                        WHERE Id IN :setCasesIds ] ;

        for( Case caseObj : lstOfCases ) {
            caseObj.Mortgage_Flag__c = Label.Mortgage_Approval_Flag;
            
            if(caseObj.Approval_Status__c == Label.Mortgage_Rejection_Status.trim() ){
                caseObj.Mortgage_Flag__c = Label.Mortgage_Rejection_Flag;
            }
            caseObj.Mortgage_M_Flag_Date__c = System.today() ;
        }
            System.debug('~~~~~~~~lstOfCases:::::::::'+lstOfCases);         
             MortgageProcessCallout.sendbankDetails(lstOfCases[0],NULL,NULL);
              update lstOfCases ;///to get in call out function        
             System.debug('~~~~~after update~~~lstOfCases:::::::::'+lstOfCases);
          
    }
    }
    
  public static boolean getApexSettingTocheck(String apexclassName){
        
        // Invoke Apex_Class_ON_OFF_Setting and check wether that class is on/off.
        Map<String,Apex_Class_ON_OFF_Setting__c> apexClassSettingMap =  Apex_Class_ON_OFF_Setting__c.getAll();
        System.debug('apexClassSettingMap::::'+apexClassSettingMap); 
        Boolean apexOnOffSetting;
        if(apexClassSettingMap != NULL && apexClassSettingMap.get(apexclassName) != NULL ){
            apexOnOffSetting = apexClassSettingMap.get(apexclassName).ON_OFF__c;
            System.debug('apexOnOffSetting:::'+apexOnOffSetting);
        }       
        return apexOnOffSetting;
    }
}