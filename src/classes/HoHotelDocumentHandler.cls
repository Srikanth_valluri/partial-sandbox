/*---------------------------------------------------------------------------------------------------------------------
Description: Document Handler for HO Hotel
=======================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By   | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     | 27-02-2020       | Aishwarya Todkar   | 1. Initial Draft
=======================================================================================================================*/
public with sharing class HoHotelDocumentHandler {
    
/**********************************************************************************************************************
 * Description: method to send email on upload of Signed UMA
 * Pararmeter(s):List of document Id
 * return: None
 * PB: Lease handover Document
 **********************************************************************************************************************/
    @InvocableMethod
    public static void sendUMA ( List<Id> listDocId ) {
        System.debug('listDocId=='+ listDocId);
        if( listDocId != null && listDocId.size()>0 ) {
            for( Id recId : listDocId ) {
                sendMail( recId );
            }
        } 
    }

/**********************************************************************************************************************
 * Description: method to send email via SendGrid
 * Pararmeter(s):document Id
 * return: None
 **********************************************************************************************************************/
    @future( callout=true )
    public static void sendMail( Id recId ) {

        //Get UMA
        List<SR_Attachments__c> listUma = [ SELECT
                                                Id
                                                , Case__c
                                                , Attachment_Url__c
                                            FROM
                                                SR_Attachments__c
                                            WHERE
                                                Id =: recId
                                            AND
                                                Case__c != null
                                            AND
                                                Attachment_Url__c != null
                                            ];

        if( listUma != null && listUma.size() > 0 ) {
            
            //Prepare UMA Attachment
            String FileId = listUma[0].Attachment_Url__c.subStringAfter('GetFile?id=');
            Office365RestService.ResponseWrapper responseWrap = new Office365RestService.ResponseWrapper();
            responseWrap  = Office365RestService.downloadFromOffice365ByIdWhatsapp(FileId,'','','','','');
            System.debug('responseWrap.downloadUrl' + responseWrap);
            
            List<Attachment> lstAttach = new List<Attachment>();
            
            if( responseWrap != null && String.isNotBlank( responseWrap.downloadUrl ) ) {
                HttpRequest req = new HttpRequest();
                req.setEndpoint(responseWrap.downloadUrl);
                req.setHeader('Accept', 'application/json');
                req.setMethod('GET');
                req.setTimeout(120000);
                HttpResponse response = new Http().send(req);
                
                if(response.getStatusCode() == 200) {
                    Blob pdfBlob =  response.getBodyAsBlob();
                    Attachment attach = new Attachment();
                    attach.contentType = 'application/pdf';
                    attach.name = 'Signed UMA.pdf';
                    System.debug('attach.name'+attach.name);
                    attach.body = pdfBlob;
                    lstAttach.add(attach);  
                }
            }

            //Get Case
            List<Case> listCase = [ SELECT 
                                        Id
                                        , Booking_Unit__c
                                        , Booking_Unit__r.Inventory__r.Building_Location__r.Hospitality_Email__c
                                        , Booking_Unit__r.Booking__r.Account__c
                                    FROM
                                        Case
                                    WHERE
                                        Id =: listUma[0].Case__c
                                    AND
                                        Booking_Unit__c != null
                                    AND
                                        Booking_Unit__r.Inventory__c != null
                                    AND
                                        Booking_Unit__r.Inventory__r.Building_Location__c != null
                                    AND
                                        Booking_Unit__r.Inventory__r.Building_Location__r.Hospitality_Email__c != null
                                    ];
            System.debug( 'listCase -- ' + listCase );
            if( listCase != null && listCase.size() > 0 ) {
                
                //Get configured email details
                List<SendGrid_Email_Details__mdt> listSendGridDetails = [SELECT
                                                                            From_Address__c
                                                                            , From_Name__c
                                                                            , To_Address__c
                                                                            , To_Name__c
                                                                            , CC_Address__c
                                                                            , CC_Name__c
                                                                            , Bcc_Address__c
                                                                            , Bcc_Name__c
                                                                            , Reply_To_Address__c
                                                                            , Reply_To_Name__c
                                                                            , Email_Template__c
                                                                        FROM
                                                                            SendGrid_Email_Details__mdt
                                                                        WHERE
                                                                            MasterLabel = 'LHO Hotel - UMA Email'
                                                                        LIMIT 1
                                                                        ];
                
                //CRM_SurveyEmailsSender.getSendGridDetails( 'LHO Hotel - UMA Email' );
                if( listSendGridDetails != null &&  listSendGridDetails.size() > 0 ) { 
                    String hospitalityEmail = listCase[0].Booking_Unit__r.Inventory__r.Building_Location__r.Hospitality_Email__c;
                    SendGrid_Email_Details__mdt objSendGridCs = listSendGridDetails[0];
                    System.debug( 'objSendGridCs:::  ' + objSendGridCs);
                    //Get Email Template
                    List<EmailTemplate> listEmailTemplate = new List<EmailTemplate>( [SELECT 
                                                                                            Id
                                                                                            , Name
                                                                                            , Subject
                                                                                            , Body
                                                                                            , HtmlValue
                                                                                            , TemplateType
                                                                                            , DeveloperName
                                                                                        FROM 
                                                                                            EmailTemplate 
                                                                                        WHERE 
                                                                                            Name =: objSendGridCs.Email_Template__c 
                                                                                        LIMIT 1 
                                                                                    ] );
                    system.debug('listEmailTemplate  === ' + listEmailTemplate );
            
                    //Preparing sendgrid email
                    if( listEmailTemplate != null && listEmailTemplate.size() > 0 ) {

                        EmailTemplate emailTemplateObj = listEmailTemplate[0];
                        
                        String templateName =  emailTemplateObj.DeveloperName;

                        String toAddress = listCase[0].Booking_Unit__r.Inventory__r.Building_Location__r.Hospitality_Email__c;

                        String toName = String.isNotBlank( objSendGridCs.To_Name__c ) ? objSendGridCs.To_Name__c : '';

                        String ccAddress = String.isNotBlank( objSendGridCs.CC_Address__c ) ? objSendGridCs.CC_Address__c : '';

                        String ccName = String.isNotBlank( objSendGridCs.CC_Name__c ) ? objSendGridCs.CC_Name__c : '';

                        String bccAddress = String.isNotBlank( objSendGridCs.Bcc_Address__c ) ? objSendGridCs.Bcc_Address__c : '';

                        String bccName = String.isNotBlank( objSendGridCs.Bcc_Name__c ) ? objSendGridCs.Bcc_Name__c : '';

                        String fromAddress = String.isNotBlank( objSendGridCs.From_Address__c ) ? objSendGridCs.From_Address__c : '';

                        String fromName = String.isNotBlank( objSendGridCs.From_Name__c ) ? objSendGridCs.From_Name__c : '';

                        String replyToAddress = String.isNotBlank( objSendGridCs.Reply_To_Address__c ) ? objSendGridCs.Reply_To_Address__c : '';

                        String replyToName = String.isNotBlank( objSendGridCs.Reply_To_Name__c ) ? objSendGridCs.Reply_To_Name__c : '';

                        String contentType = 'text/html';
                        
                        String subject = emailTemplateObj.Subject;

                        String contentValue = emailTemplateObj.HtmlValue;
                        // CRM_SurveyEmailsSender.replaceMergeFields( 
                        //                                         emailTemplateObj.HtmlValue      
                        //                                         , objLog.Id                     // Record Id
                        //                                         , objLog.OwnerId                // CRE Id
                        //                                         , objLog.Account__c             // Account Id
                        //                                         , objLog.Owner.Name             // CRE Name
                        //                                         , objLog.Account__r.Name        // Customer Name
                        //                                         , templateName                  // Template Name
                        //                                         );

                        String contentBody = emailTemplateObj.Body ;
                        // CRM_SurveyEmailsSender.replaceMergeFields( 
                        //                                         emailTemplateObj.Body   
                        //                                         , objLog.Id                     // Record Id
                        //                                         , objLog.OwnerId                // CRE Id
                        //                                         , objLog.Account__c             // Account Id
                        //                                         , objLog.Owner.Name             // CRE Name
                        //                                         , objLog.Account__r.Name        // Customer Name
                        //                                         , templateName                  // Template Name
                        //                                         );
                        
                    
                        //sending an email via SendGrid
                        SendGridEmailService.SendGridResponse objSendGridResponse = 
                                                        SendGridEmailService.sendEmailService(
                                                                                    toAddress, toName
                                                                                    , ccAddress, ccName
                                                                                    , bccAddress, bccName
                                                                                    , subject, ''
                                                                                    , fromAddress, fromName
                                                                                    , replyToAddress, replyToName
                                                                                    , contentType
                                                                                    , contentValue, ''
                                                                                    ,  lstAttach);
                        
                        system.debug('objSendGridResponse === ' + objSendGridResponse);
                        
                        String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;
                        System.debug('responseStatus== ' + responseStatus);
                        List<EmailMessage> lstEmails = new List<EmailMessage>(); 
                        List<Booking_Unit__c> listBuToUpdate = new List<Booking_Unit__c>();
                        //Create reportable activity 
                        if (responseStatus == 'Accepted') {
                            EmailMessage mail = new EmailMessage();
                            mail.Subject = subject;
                            mail.MessageDate = System.Today();
                            mail.Status = '3';//'Sent';
                            mail.relatedToId = listCase[0].Booking_Unit__r.Booking__r.Account__c;
                            mail.Account__c  = listCase[0].Booking_Unit__r.Booking__r.Account__c;
                            mail.Type__c = 'LHO Hotel-UMA document';
                            mail.ToAddress = toAddress;
                            mail.FromAddress = fromAddress;
                            mail.TextBody = contentBody;//contentValue.replaceAll('\\<.*?\\>', '');
                            mail.Sent_By_Sendgrid__c = true;
                            mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                            mail.parentId = listCase[0].id;
                            mail.CcAddress = ccAddress;
                            mail.BccAddress = bccAddress;
                            lstEmails.add(mail);
                            system.debug('Mail obj == ' + mail);
                            
                            Booking_Unit__c objBu = new Booking_Unit__c( Id = listCase[0].Booking_Unit__c
                                                                , Hotel_UMA_Email_Received_Date__c = system.today() );
                            listBuToUpdate.add( objBu );
                            update listBuToUpdate;
                            insert lstEmails;    
                        }
                        System.debug('lstEmails=== ' + lstEmails);
                         /*if(lstEmails.size() > 0) {
                            if(!Test.isRunningTest()) {
                                insert lstEmails;
                            }
                        }*/


                    } // End listEmailTemplate if
                }//End listSendGridDetails if
            }//End listCase if 
        }// End listUma if 
        //Capture Hotel UMA Email Received Date on BU and case
    }
}