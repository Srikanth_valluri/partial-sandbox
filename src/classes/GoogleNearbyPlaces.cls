public with sharing class GoogleNearbyPlaces {

    // refer https://developers.google.com/places/web-service/intro
    //  and  https://developers.google.com/places/web-service/search

    private static final String OUTPUT_FORMAT       = 'json';
    private static final String PLACES_BASE_URL     = 'https://maps.googleapis.com/maps/api/place/';
    private static final String NEARBY              = 'nearbysearch/' + OUTPUT_FORMAT;
    private static final String DETAILS             = 'details/' + OUTPUT_FORMAT;

    @testVisible
    private static final String API_KEY_UNAVAILABLE = 'API Key not available';

    private static GoogleApiKey__c placesApiKey = GoogleApiKey__c.getInstance('Places');


    public static Response getNearbyPlaces(Request request) {
        if (request == NULL) {
            return NULL;
        }

        Response response = new Response();

        if (!haveApiKey()) {
            response.error_message = API_KEY_UNAVAILABLE;
            return response;
        }

        String params = requestToUrlParams(request);

        String requestUrl = PLACES_BASE_URL + NEARBY + '?' + params;

        HttpResponse res = httpGet(requestUrl);

        try {
            response = (Response) JSON.deserialize(res.getBody(), Response.class);
        } catch(Exception e) {
            response.status = 'error';
            response.error_message = e.getMessage();
        }

        return response;
    }

    public static Place getPlaceDetails(String placeid) {
        if (String.isBlank(placeid)) {
            return NULL;
        }

        Place place = new Place();

        if (!haveApiKey()) {
            place.error_message = API_KEY_UNAVAILABLE;
            return place;
        }

        String requestUrl = PLACES_BASE_URL + DETAILS + '?placeid=' + placeid + '&key=' + placesApiKey.ApiKey__c;

        HttpResponse res = httpGet(requestUrl);

        try {
            place = (Place) JSON.deserialize(res.getBody(), Place.class);
        } catch(Exception e) {
            place.status = 'error';
            place.error_message = e.getMessage();
        }

        return place;
    }

    private static Boolean haveApiKey() {
        return placesApiKey != NULL && String.isNotBlank(placesApiKey.ApiKey__c);
    }

    private static String requestToUrlParams(Request request) {
        String params = '';

        String LOCATION = 'location';
        String RADIUS = 'radius';
        String KEY = 'key';

        params += (LOCATION + '=' + request.location) + '&';
        params += (RADIUS + '=' + request.radius) + '&';
        params += KEY + '=' + placesApiKey.ApiKey__c;

        return params;
    }

    private static HttpResponse httpGet(String requestUrl) {
        HttpRequest req = new HttpRequest();
        req.setEndpoint(requestUrl);
        req.setMethod('GET');

        return new Http().send(req);
    }


    // for nearby places
    public class Request {
        public String location;
        public Integer radius = 1500;
        //public String pagetoken;

        public Request(Decimal latitude, Decimal longitude) {
            this.location = latitude + ',' + longitude;
        }

    }

    public class Response {
        public String error_message;
        public String status;
        public Result[] results;
        //public String[] html_attributions;
        //public String next_page_token;
    }

    public class Result {
        public Geometry geometry;
        public String id;
        public String name;
        public String place_id;
        public Double rating;
        public String reference;
        public String[] types;
        public String vicinity;
        //public String icon;
        //public String scope;
        //public cls_photos[] photos;
    }

    public class Geometry {
        public Location location;
        //public cls_viewport viewport;
    }

    public class Location {
        public Double lat;
        public Double lng;
    }

    // for place details
    public class Place {
        public PlaceDetails result;
        public String status;
        public String error_message;
    }

    public class PlaceDetails {
        public AddressComponents[] address_components;
        public String formatted_address;
        public String formatted_phone_number;
        public Geometry geometry;
        public String id;
        public String international_phone_number;
        public String name;
        public OpeningHours opening_hours;
        public String place_id;
        public Double rating;
        public String reference;
        public String[] types;
        public String url;
        public Integer utc_offset;
        public String vicinity;
        public String website;
        //public String adr_address;
        //public cls_photos[] photos;
        //public String icon;
        //public cls_reviews[] reviews;
        //public String scope;
    }
    public class AddressComponents {
        public String long_name;    //48
        public String short_name;   //48
        public String[] types;
    }
    public class OpeningHours {
        public boolean open_now;
        //public Period[] periods;
        public String[] weekday_text;
    }
    //public class Period {
    //    public Close close;
    //    public Open open;
    //}
    //public class Close {
    //    public Integer day;
    //    public String time;
    //}
    //public class Open {
    //    public Integer day;
    //    public String time;
    //}

    //public class cls_viewport {
    //    public cls_northeast northeast;
    //    public cls_southwest southwest;
    //}
    //
    //class cls_northeast {
    //    public Double lat;  //-33.8652709197085
    //    public Double lng;  //151.1972016802915
    //}
    //class cls_southwest {
    //    public Double lat;  //-33.8679688802915
    //    public Double lng;  //151.1945037197085
    //}

}