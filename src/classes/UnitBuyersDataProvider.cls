public without sharing class UnitBuyersDataProvider implements DataProvider {

    public DataDisplayConfig getData(DataDisplayConfig config) {
        List<Buyer__c> lstUnitBuyer = new List<Buyer__c>();
        if (config.fieldList != NULL) {
            String fields = '';
            for (Map<String,String> fieldMap : config.fieldList) {
                String fieldName = fieldMap.get(DataRendererUtils.FIELD_NAME);
                if (String.isNotBlank(fieldName) && !fields.containsIgnoreCase(fieldName + ',')) {
                    fields += fieldName + ',';
                }
            }

            if (config.detailFieldList != NULL) {
                for (Map<String,String> fieldMap : config.detailFieldList) {
                    String fieldName = fieldMap.get(DataRendererUtils.FIELD_NAME);
                    if (String.isNotBlank(fieldName) && !fields.containsIgnoreCase(fieldName + ',')) {
                        fields += fieldName + ',';
                    }
                }
            }

            fields = fields.removeEnd(',');

            lstUnitBuyer = Database.query(
                ' SELECT ' + fields + ' FROM Buyer__c '
                + (String.isBlank(config.filter) ? '' : config.filter)
                + (String.isBlank(config.recordLimit) ? '' : ' LIMIT ' + config.recordLimit)
            );
        }

        config.dataList = DataRendererUtils.wrapData(config, lstUnitBuyer);

        return config;
    }
}