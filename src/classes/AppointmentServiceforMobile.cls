/****************************************************************************************************
* Name          : AppointmentServiceforMobile                                                       *
* Description   : Class send available slots to Mobile and create appointment on selection          *
* Created Date  : 03-03-2019                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE        COMMENTS                                                    *
* 1.0                                 Initial Draft.                                                *
****************************************************************************************************/

@RestResource(urlMapping='/appointmentMobile')
global class AppointmentServiceforMobile {

    /*
    The method is to get the available appointment slots, use GET method for callout
    The parameters need to send will be as follow:
    accountId : SF accountId of customer
    unitName : SF Id of Booking Unit
    processName : name of process chosen
    subProcessName : name of the subprocess chosen
    selectedDate : the date for which appointment to be booked in 'YYYY-MM-DD'
    this method return list of wrapper AppointmentWrapper

    the url for get should be similar to below
    https://damacholding--partial.cs80.my.salesforce.com/services/apexrest/appointmentMobile?
    accountId=0010Y00000MaKkw&unitName=a0x0Y000001jABn&processName=Handover&subProcessName=Documentation&selectedDate=2019-03-05
    */
    @HttpGet
    global static list<AppointmentWrapper> doGet() {
        list<AppointmentWrapper> lstAppointmentWrapper = new list<AppointmentWrapper>();
        RestRequest req = RestContext.request;
        system.debug('!!!!req'+req);
        if (req != null && req.requestBody != null){
            String accountId = RestContext.request.params.get('accountId');
            String unitName = RestContext.request.params.get('unitName');
            String processName = RestContext.request.params.get('processName');
            String subProcessName = RestContext.request.params.get('subProcessName');
            String selectedDate = RestContext.request.params.get('selectedDate');
            system.debug('!!!!unitName'+unitName);
            system.debug('!!!!processName'+processName);
            if (!string.isBlank(processName)) {
                AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
                for (AppointmentSelectionHandler.AppointmentWrapper objWrap :
                    handler.availableSlots(accountId, unitName, processName, subProcessName, 'Mobile',
                        selectedDate, false)) {
                     system.debug('!!!!!!objWrap'+objWrap);
                     if (!string.isBlank(objWrap.ErrorMessage) ){
                        if (objWrap.objCL == null){
	                        AppointmentWrapper objSelectedWrap =
	                            new AppointmentWrapper(objWrap.objApp, objWrap.objCL, objWrap.ErrorMessage);
	                        lstAppointmentWrapper.add(objSelectedWrap);
                        }
                     } else {
                        AppointmentWrapper objSelectedWrap =
                            new AppointmentWrapper(objWrap.objApp, objWrap.objCL, '');
                            lstAppointmentWrapper.add(objSelectedWrap);
                    }
                }
            }
        }
        String id = RestContext.request.params.get('id');
        return lstAppointmentWrapper;
    }// end of method doGet

    /*
    This is a post method, it will create an appointment record in SF, use POST method for callout
    It expects the request body in json of list of wrapper format of selectedWrapper
    The elements of wrapper are as follows:
    accountId : SF accountId of customer
    unitName : SF Id of Booking Unit
    processName : name of process chosen
    subProcessName : name of the subprocess chosen
    selectedDate : the date for which appointment to be booked in 'YYYY-MM-DD'
    lstWrap : list of selected appointment wrapper in get method, with chosen slots isSelected as true

    this method will return string with value as '200' if success or else '400' if error or exception
    */
    @HttpPost
    global static String doPost() {
        RestRequest req = RestContext.request;
        set<String> setAccountIds = new set<String>();
        map<Id, Account> accountMap = new map<Id, Account>();
        Boolean isException = false;
        if (req != null && req.requestBody != null){
             List<selectionWrapper> lstAppoint =
                (List<selectionWrapper>)JSON.deserialize(req.requestBody.toString(),
                List<selectionWrapper>.class);
             if (!lstAppoint.isEmpty() && lstAppoint != null){
                 for (selectionWrapper objWrap : lstAppoint){
                     setAccountIds.add(objWrap.accountId);
                 }
                 if (!setAccountIds.isEmpty() && setAccountIds != null){
                     accountMap = getAccounts(setAccountIds);
                 }
                 for (selectionWrapper objWrap : lstAppoint){
                     list<AppointmentSelectionHandler.AppointmentWrapper> lstwrappers;
                     for (AppointmentWrapper objAppointWrap: objWrap.lstWrap){
                        lstwrappers = new list<AppointmentSelectionHandler.AppointmentWrapper>();
                         if (objAppointWrap.isSelected == true) {
			                  AppointmentSelectionHandler.AppointmentWrapper newWrap = new
			                      AppointmentSelectionHandler.AppointmentWrapper(objAppointWrap.objApp, objAppointWrap.objCL, '', true);
			                  lstwrappers.add(newWrap);
                         }
                     }
                     try {
                        AppointmentSelectionHandler handler = new AppointmentSelectionHandler();
	                     handler.createAppointments(lstwrappers, '', objWrap.unitName,
	                        accountMap.get(objWrap.accountId), objWrap.subProcessName, objWrap.processName,
	                        correctDateFormat(objWrap.selectedDate));
                     } catch(Exception e){
                         System.debug('An exception occurred: ' + e.getMessage());
                         isException = true;
                     }
                 }
             }
        }
        if (isException == true){
            return '400';
        } else {
            return '200';
        }
    }// end of method doPost

    // this method sends the date in format including /
    public static String correctDateFormat(String strDate) {
         List<String> parts = strDate.split('-');
         String correctDate;
         if (parts.size() == 3){
             correctDate = parts[1]+'/'+parts[2]+'/'+parts[0];
         }
         return correctDate;
    }// end of method correctDateFormat

    // this method gets the additional account information from accountId
    public static map<Id, Account> getAccounts(set<String> AccountIds) {
        map<Id, Account> mapAccounts = new map<Id, Account>();

        for (Account objAccount : [SELECT Id
                                        , Name
                                        , Primary_CRE__c
                                        , Secondary_CRE__c
                                        , Tertiary_CRE__c
                                        , Primary_Language__c
                                        , Email__pc
                                        , Email__c
                                        , isPersonAccount
                                    FROM Account
                                    WHERE Id IN :AccountIds ]){
            mapAccounts.put(objAccount.Id, objAccount);
        }
        return mapAccounts;
    }// end of method getAccounts

    global class selectionWrapper {
        public string accountId;
        public string unitName;
        public string processName;
        public String subProcessName;
        public string selectedDate;
        public list<AppointmentWrapper> lstWrap;
    }// end of selectionWrapper

    global class AppointmentWrapper {
        public Appointment__c objApp {get;set;}
        public Calling_List__c objCL {get;set;}
        public Boolean isSelected {get;set;}
        public String errorMessage {get;set;}

        public AppointmentWrapper() {

        }

        public AppointmentWrapper( Appointment__c tempObjApp, Calling_List__c tempObjCL, String strMessage ) {
            objApp = tempObjApp ;
            objCL = tempObjCL ;
            errorMessage = strMessage;
        }
    } //End of AppointmentWrapper
}