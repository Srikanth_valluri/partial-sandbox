/****************************************************************************************
//Class Name : BOTButtonPageController
//Purpose    : Controller for VF page BOTButtonPage
//
//Description: 
//BOTButtonspage will have the button to delete the below custom setting.
//
//Inquiry User Assignment Rules - PC Poweline only.
//
//Once deleted BOt will import thye new ranking from qilksense and import the same in
//Salesforce. This page will be modified to add buttons in future to perform other Tasks.
*****************************************************************************************/

public class BOTButtonPageController {
    
    
    public BOTButtonPageController(){
        
    }
    
    public pagereference ResetPowerLine(){
        
        delete [select id from Inquiry_User_Assignment_Rules__c where User_Profile__c='Property Consultant' order by Net_Direct_Sales_Rank__c DESC];
        return null;
        
    }
    

}