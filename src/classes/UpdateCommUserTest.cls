@isTest
private class UpdateCommUserTest {//AKISHOR: for comm-user email/user name upd on COD closure

    public static String COMMUNITY_USER_PROFILE = 'Customer Community Login User(Use this)';
    
    static testMethod void test_createuser1 () {
        Id profileId = [select id from profile where name = :COMMUNITY_USER_PROFILE].id;

        Account objAccount = CommunityTestDataFactory.CreatePersonAccount();
        objAccount.Email__pc = 'test@user.com';
        objAccount.PersonEmail = '';
        insert objAccount;
        
        Account acc = [Select Party_ID__c,PersonContactId From Account Where Id = :objAccount.Id];

        User user = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = profileId, country='United States',IsActive =true,
                ContactId = acc.PersonContactId,Party_Id__c = '12345',
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');

        insert user;
        
        //System.runAs(user) {
        Test.startTest();
            UpdateCommUserEmailUserName.UpdateUserNameEmail(new List<String>{'12345'});
        Test.stopTest();
        //}     
    }
 }