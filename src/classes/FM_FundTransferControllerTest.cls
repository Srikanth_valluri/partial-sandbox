@isTest
public class FM_FundTransferControllerTest {
    public static testmethod void createFMCase(){

        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];

        Account acctIns = TestDataFactoryFM.createAccount();
        insert acctIns;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;

        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns.Inventory__c = invObj.id;
        insert buIns;

        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;

        PageReference myVfPage = Page.FM_FundTransfer;

        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Account__c = acctIns.id;

        Test.setCurrentPage(myVfPage);

        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Fund_Transfer_Request');
        //ApexPages.currentPage().getParameters().put('Id',Opp.Id);

        FM_FundTransferController obj=new FM_FundTransferController();
        obj.objFMCase=fmCaseObj;
        obj.saveAsDraft();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        Test.stopTest();
    }
    
    public static testmethod void createFMCaseWithCaseId(){

        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];

        Account acctIns = TestDataFactoryFM.createAccount();
        insert acctIns;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;

        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns.Inventory__c = invObj.id;
        buIns.Handover_Flag__c = 'Y';
        insert buIns;

        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;

        PageReference myVfPage = Page.FM_FundTransfer;

        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Account__c = acctIns.id;
        insert fmCaseObj;
        
        list<SR_Attachments__c> lstAttachments = new list<SR_Attachments__c>();
        SR_Attachments__c objAtt = new SR_Attachments__c();
        objAtt.Name = 'test';
        // objAtt.Type__c = fileNames.size() ==2 ? fileNames[1]: '';
        objAtt.FM_Case__c = fmCaseObj.Id;
        objAtt.Booking_Unit__c = buIns.Id;
        lstAttachments.add(objAtt);
        insert lstAttachments;
                
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('Id',fmCaseObj.Id);
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Fund_Transfer_Request');
        //ApexPages.currentPage().getParameters().put('Id',Opp.Id);

        FM_FundTransferController obj=new FM_FundTransferController();
        //obj.objFMCase=fmCaseObj;
        obj.getToUnits();
        obj.createCaseShowUploadDoc();
        obj.addDocument();
        obj.removeDocument();
        obj.deleteDocument();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        Test.stopTest();
    }
    
    public static testmethod void createFMCaseWithoutCaseId(){

        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];

        Account acctIns = TestDataFactoryFM.createAccount();
        insert acctIns;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
        
        bk.Account__c = acctIns.id;
        update bk;

        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;

        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns.Inventory__c = invObj.id;
        buIns.Handover_Flag__c = 'Y';
        buIns.Owner__c = acctIns.id;
        buIns.Booking__c = bk.id;
        buIns.Authorized_Personnel_Name__c = 'tets';
        buIns.Registration_Status__c = 'ARF with CRD';
        insert buIns;
        
        Booking_Unit__c objBU = [SELECT id,Booking__r.account__c from Booking_Unit__c where id=: buIns.id];
        system.debug('maza class cha BU'+objBU);
        system.debug('maza account'+objBU.Booking__r.account__c);
        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;
        
        Booking_Unit_Active_Status__c csObj = new Booking_Unit_Active_Status__c();
        csObj.Status_Value__c = 'ARF with CRD';
        csObj.name = 'ARF with CRD';
        insert csObj;
        
        PageReference myVfPage = Page.FM_FundTransfer;

        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Account__c = acctIns.id;
        insert fmCaseObj;
        
        list<SR_Attachments__c> lstAttachments = new list<SR_Attachments__c>();
        for(Integer i=0; i<5; i++) {
            SR_Attachments__c objAtt = new SR_Attachments__c();        
            objAtt.Name = 'test';
            // objAtt.Type__c = fileNames.size() ==2 ? fileNames[1]: '';
            objAtt.FM_Case__c = fmCaseObj.Id;
            objAtt.Booking_Unit__c = buIns.Id;
            lstAttachments.add(objAtt);
        }        
        insert lstAttachments;
                
        Test.setCurrentPage(myVfPage);
        //ApexPages.currentPage().getParameters().put('Id',fmCaseObj.Id);
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Fund_Transfer_Request');
        //ApexPages.currentPage().getParameters().put('Id',Opp.Id);

        FM_FundTransferController obj=new FM_FundTransferController();
        //obj.objFMCase=fmCaseObj;
        obj.getToUnits();
        obj.createCaseShowUploadDoc();
        obj.addDocument();
        obj.removeDocument();
        obj.docIdToDelete = lstAttachments[0].id;
        obj.deleteDocument();
        obj.strFMCaseId=fmCaseObj.id;
        obj.objFMCase=fmCaseObj;
        obj.uploadDocument();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        Test.stopTest();
    }

    public static testmethod void submitFMCase(){

        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        Test.startTest();
        Account acctIns = TestDataFactoryFM.createAccount();
        insert acctIns;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;

        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns.Inventory__c = invObj.id;
        insert buIns;

        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        fmUser.fm_role__c ='FM Manager';
        insert fmUser;

        PageReference myVfPage = Page.FM_FundTransfer;

        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Email__c = 'test@test.com';
        fmCaseObj.Request_Type_DeveloperName__c='Fund_Transfer_Request';
        insert fmCaseObj;


        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        Test.setMock( WebServiceMock.class, new MoveOutMockClass() );

        Test.setCurrentPage(myVfPage);
        
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Fund_Transfer_Request');
        //ApexPages.currentPage().getParameters().put('Id',Opp.Id);

        FM_FundTransferController obj=new FM_FundTransferController();
        obj.objFMCase.id=fmCaseObj.id;
        obj.submitSR();
        Test.stopTest();
        //obj.returnBackToCasePage();
        obj.strDocBody='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        obj.strDocName='Test_Document_Work_Permit.htm';
        // obj.createCaseShowUploadDoc();

    }

    public static testmethod void testInsertAttachment(){

        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];

        Account acctIns = TestDataFactoryFM.createAccount();
        insert acctIns;        

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;

        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns.Inventory__c = invObj.id;
        buIns.Registration_Status__c = 'WP Issued';
        insert buIns;

        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;

        PageReference myVfPage = Page.FM_FundTransfer;

        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Fund_Transfer_Request';
        //fmCaseObj.Permit_To_Work_For__c='Hot Works;Confined Spaces';
        insert fmCaseObj;

        Test.startTest();
        Test.setMock( WebServiceMock.class, new MoveOutMockClass() );


        Test.setCurrentPage(myVfPage);

        ApexPages.currentPage().getParameters().put('Id',fmCaseObj.Id);
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Fund_Transfer_Request');

        FM_FundTransferController obj=new FM_FundTransferController();
        obj.strDocBody='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        obj.strDocName='Test_Document_Work_Permit.htm';


        obj.strFMCaseId=fmCaseObj.id;
        obj.objFMCase=fmCaseObj;
        obj.uploadDocument();
        obj.submitSR();
        test.stoptest();

        obj.deleteDocument();

    }

    public static testmethod void testGenerateCRF(){
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        NSIBPM__Service_Request__c objServReq = TestDataFactory_CRM.createServiceRequest();
        insert objServReq ;

        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objServReq.Id,1);
        insert lstBooking ;

        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus ;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking ,3);
        for( Booking_Unit__c objUnit : lstBookingUnit  ) {
            objUnit.Resident__c = objAcc.Id ;
            objUnit.Handover_Flag__c = 'Y' ;
        }
        lstBookingUnit[2].Owner__c = objAcc.Id ;
        insert lstBookingUnit ;

        list<FM_Case__c> lstFMCases = new list<FM_Case__c>();

        Id recTypeId=Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Fund Transfer').getRecordTypeId();
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Fund_Transfer_Request';
        fmCaseObj.Booking_Unit__c=lstBookingUnit[0].id;
        fmCaseObj.Email__c='a@gmail.com';
        fmCaseObj.Mobile_no__c='12121212';
        fmCaseObj.RecordTypeId=recTypeId;
        fmCaseObj.Status__c='Submitted';
        fmCaseObj.Origin__c='Portal';
        fmCaseObj.Approval_Status__c = 'Pending';
        fmCaseObj.Submit_for_Approval__c = true;
        fmCaseObj.Approving_Authorities__c='Property Manager__FM Manager,Master Community Property Manager';
        fmCaseObj.Account__c=objAcc.id;
        fmCaseObj.Current_Approver__c = 'FM Manager__';
        fmCaseObj.Tenant_Email__c = 'test@gmail.com';
        insert fmCaseObj;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());		
        FM_GenerateDrawloopDocumentBatch objClass = new FM_GenerateDrawloopDocumentBatch(fmCaseObj.Id
                                                                                   , System.Label.CRE_NOC_DDP_Template_Id
                                                                                   , System.Label.CRE_NOC_Template_Delivery_Id);
        Database.Executebatch(objClass);
        Test.stopTest();
        
        FM_FundTransferController  obj=new FM_FundTransferController();
        obj.strFMCaseId=fmCaseObj.id;
        obj.objFMCase=fmCaseObj; 
        obj.generateNDA();
        obj.checkDrawloopBatchStatus();
    }
}