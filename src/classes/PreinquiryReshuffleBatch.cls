global class PreinquiryReshuffleBatch implements Database.Batchable<sObject>,Database.Stateful {
    
    global List<TSA_Preinquiry_Reshuffle__mdt> preInquiryConditionMDT = new List<TSA_Preinquiry_Reshuffle__mdt>();
    global String query = '';
    global Integer count = 0;

    global PreinquiryReshuffleBatch(List<TSA_Preinquiry_Reshuffle__mdt> preInquiryConditionMDT) {
        /*this.preInquiryConditionMDT = preInquiryConditionMDT;*/
    }

    /*global PreinquiryReshuffleBatch() {
             preInquiryConditionMDT = [SELECT id,DeveloperName,Based_In__c,
            Days__c,Queue__c,ReassignmentPass__c,Status__c FROM TSA_Preinquiry_Reshuffle__mdt WHERE  Days__c=1];
    }*/

   
    global Database.QueryLocator start(Database.BatchableContext BC) {
       /*
        
        query = ' SELECT '
              + ' Id, Name, Time_Elapsed_Since_Last_Activity__c, '
              + ' Owner_Manager_Id__c, Re_Assignment_Pass_No__c, '
              + ' Last_Activity_Date__c, OwnerId, Owner.Name, '
              + ' Campaign__r.Exclude_TSA_Reshuffle__c, '
              + ' Campaign__r.Campaign_Name__c '
              + ' FROM '
              + ' Inquiry__c '
              + ' WHERE '
              + ' Owner.Name !=\'DAMAC Queue\' '
              + ' AND '
              + ' Owner.Name !=\'Nurturing Queue\' '
              + ' AND '
              + ' RecordType.DeveloperName = \'Pre_Inquiry\' ';
            
        system.debug('>>query1 >>>>'+query ); 
        String whereCondition = whereConditionForQuery(preInquiryConditionMDT[0]);
        query+=whereCondition;
        system.debug('>>whereCondition>>>>'+whereCondition);
        system.debug('>>query2>>>>'+query);
        system.debug('>>Database.getQueryLocator(query)>>>>'+Database.getQueryLocator(query));

        */
        if (Test.isRunningTest()) {
            query = 'SELECT Id FROM Inquiry__c LIMIT 1';
        }
        return Database.getQueryLocator(query);
        
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        /*
        ReshufflePreinquiries reshuffle = new ReshufflePreinquiries();
        system.debug('>>>inexecute>>' + scope);
        reshuffle.reAssignPreInquiryOwner(
            (List<Inquiry__c>)scope,
            Integer.valueOf(preInquiryConditionMDT[0].ReassignmentPass__c),
            preInquiryConditionMDT[0].Queue__c
        );
        */
    }

    global void finish(Database.BatchableContext BC) {
        /*count++;
        system.debug('>>count>>'+preInquiryConditionMDT);
        preInquiryConditionMDT.remove(0);
        if(!preInquiryConditionMDT.isEmpty()){
            PreinquiryReshuffleBatch b = new PreinquiryReshuffleBatch(preInquiryConditionMDT);
            //PreinquiryReshuffleBatch b = new PreinquiryReshuffleBatch();
            database.executebatch(b);
        }*/
    }

    global String whereConditionForQuery(TSA_Preinquiry_Reshuffle__mdt preInquiryConditionMDT){
        /*String whereCondition = '';

        if(preInquiryConditionMDT.Status__c != null){
            whereCondition += ' AND Inquiry_Status__c =\''+ preInquiryConditionMDT.Status__c + '\'';
            system.debug('>>whereCondition>>>>' + whereCondition);
        }

        if(preInquiryConditionMDT.Based_In__c == 'Non UAE'){
            whereCondition += ' AND Mobile_CountryCode__c !=\'United Arab Emirates: 00971\'';
            system.debug('>>whereCondition>>>>' + whereCondition);
        } else{
            if(preInquiryConditionMDT.Based_In__c == 'UAE'){
                whereCondition += ' AND Mobile_CountryCode__c =\'United Arab Emirates: 00971\'';
                system.debug('>>whereCondition>>>>' + whereCondition);
            }
        }

        if(preInquiryConditionMDT.Days__c != null){
            system.debug('>>preInquiryConditionMDT.Days__c>>>>'+preInquiryConditionMDT.Days__c);
            Decimal days = preInquiryConditionMDT.Days__c;
            Decimal interval = days * 10 * 60  *60 * 1000;
            //System.debug('SR dt>>> : ' +System.now());
            DateTime myDT = System.now();
            BusinessHours bh = [SELECT Id FROM BusinessHours WHERE IsDefault=true];
            //Long  ml = (BusinessHours.diff(bh.Id, System.now(), System.now().addDays(10)));
            IF(preInquiryConditionMDT.Days__c ==0){
            myDT =System.now();
            }
            else if (preInquiryConditionMDT.Days__c >=30)
            {
                System.debug('myDT dt>>> : ' +myDT);
             myDT = myDT.addDays(-preInquiryConditionMDT.Days__c.intValue());
             System.debug('myDT dt>>> : ' +myDT);
            }
            else{
            myDT = BusinessHours.add(bh.Id, System.now(), Long.valueOf( '-'+String.valueOf(interval)));
            }
            //date mydate = date.valueOf(system.Today().addDays(integer.valueOf('-'+preInquiryConditionMDT.Days__c)));
            //Datetime myDT = datetime.newInstance(mydate.year(), mydate.month(),mydate.day());
            String strConvertedDate = myDT.format('yyyy-MM-dd\'T\'HH:mm:ss.SSSXXX');
            whereCondition += ' AND Last_Activity_Date__c < ' + strConvertedDate;
            system.debug('>>whereCondition>>>>' + whereCondition);
        }   
        return whereCondition; */
        return null;
    }

}