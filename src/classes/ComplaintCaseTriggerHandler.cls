/*
 * Description: Trigger Handler for Complaint Case which changes the Owner of Open Tasks when Owner of Case is changed
 * Author : Vivek Shinde
 */

public class ComplaintCaseTriggerHandler {

    /**
     * Method used to update the Owner of Tasks
     */
    public void changeOwnerOfTasks(Map<Id, Case> mapOldCases, Map<Id,Case> mapNewCases) {
        Set<Id> setCaseIds = new Set<Id>();
        Map<Id, List<Task>> mapCaseWithTasks = new Map<Id, List<Task>>();
        List<Task> lstTasksToUpdate = new List<Task>();
        String strComplaintRecTypeId = Case.sObjectType.getDescribe().getRecordTypeInfosByDeveloperName().get('Complaint').getRecordTypeId();

        Set<String> setTaskStatus = new Set<String> {'Completed', 'Cancelled', 'Closed', 'Reject' , 'Reject-Duplicate', 'Aborted'};
        
        // Iterate and check whether the Case is of Complaint Type and the new Owner is a User
        for (Case objCase : mapNewCases.values()) {
            Case objOldCase = mapOldCases.get(objCase.Id);
            String strOwnerId = objCase.OwnerId;
            if (objCase.RecordtypeId == strComplaintRecTypeId  && strOwnerId.startsWith('005') && objOldCase.OwnerId != objCase.OwnerId) {
                setCaseIds.add(objCase.Id);
            }
        }
        
        if (!setCaseIds.isEmpty()) {
            
            // Fetch all the Tasks for the Cases
            List<Task> lstTasks = [SELECT Id, OwnerId, Status, WhatId FROM Task WHERE WhatId IN: setCaseIds AND Status NOT IN: setTaskStatus];

            if (lstTasks != null && !lstTasks.isEmpty()) {
                
                // Associate all related Tasks with Case
                for (Task objTask : lstTasks) {
                    if (!mapCaseWithTasks.containsKey(objTask.WhatId)) {
                        mapCaseWithTasks.put(objTask.WhatId, new List<Task> {objTask});

                    } else {
                        List<Task> lstExistingTasks = mapCaseWithTasks.get(objTask.WhatId);
                        lstExistingTasks.add(objTask);
                        mapCaseWithTasks.put(objTask.WhatId, lstExistingTasks);
                    }
                }
                
                if (mapCaseWithTasks.size() > 0) {
                    for (Id objCaseId : mapCaseWithTasks.keySet()) {

                        // Set the Task Owner value
                        Case objCase = mapNewCases.get(objCaseId);
                        List<Task> lstAllTasks =  mapCaseWithTasks.get(objCaseId);
                        if (lstAllTasks != null && !lstAllTasks.isEmpty()) {
                            for (Task objTask : lstAllTasks) {
                                objTask.OwnerId = objCase.OwnerId;
                                lstTasksToUpdate.add(objTask);
                            }                           
                        }
                    }
                }
                
                if (!lstTasksToUpdate.isEmpty()) {
                    update lstTasksToUpdate;
                }
            }
        }
    }
}