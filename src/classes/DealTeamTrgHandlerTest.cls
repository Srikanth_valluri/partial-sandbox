@istest
public class DealTeamTrgHandlerTest{
    
    @testSetup static void setupData() {
        list<string> rolenames = new List<string>{'PC2','HOS2','DOS2','HOD2'};
            Map<string,UserRole> mpRoles = InitializeSRDataTest.createRoles(rolenames);
        
        set<string> stProfileNames = new set<string>{'Property Consultant','Director of Sales','Head of Sales','Head of Department'};
            Map<string,profile> mpProfile = InitializeSRDataTest.getprofiles(stProfileNames);
        
        Map<id,integer> mpprofileidUsercount = new Map<Id,integer>();
        Map<Id,id> mpProfileIDRoleID = new Map<Id,Id>();
        for(profile p : mpprofile.values()){
            mpprofileidUsercount.put(p.id,2);
            if(p.name == 'Property Consultant')
                mpProfileIDRoleID.put(p.id,mpRoles.get('PC2').id);
            if(p.name == 'Director of Sales')
                mpProfileIDRoleID.put(p.id,mpRoles.get('DOS2').id);
            if(p.name == 'Head of Sales')
                mpProfileIDRoleID.put(p.id,mpRoles.get('HOS2').id);
            if(p.name == 'Head of Department')
                mpProfileIDRoleID.put(p.id,mpRoles.get('HOD2').id);
        }
        List<user> lstUsers = InitializeSRDataTest.createInternalUser(mpprofileidUsercount);
        
        for(user u : lstUsers){
            system.debug('--Username>'+u.username);
            u.UserRoleId = mpProfileIDRoleID.get(u.profileid);
        }
        insert lstUsers;
    }
    
    //change agent
    @isTest static void test_method_0() {
        Map<string,List<User>> mpUsers = new Map<string,List<User>>();
        for(user u : [select id,name,userrole.name,CompanyName from user]){
            if(mpUsers.containskey(u.userrole.name)){
                List<User> lstu = mpUsers.get(u.userrole.name);
                lstu.add(u);
                mpUsers.put(u.userrole.name,lstu);
            }
            else{
                List<User> lstu = new List<User>();
                lstu.add(u);
                mpUsers.put(u.userrole.name,lstu);
            }
        }
        
        Account a = new Account();
        a.Name = 'Test Account';        
        insert a;
        
        //Id PerRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        Account a1 = new Account();
        a1.FirstName = 'Test Account';
        a1.LastName = 'Acc';
        a1.Phone_Key__c = 'ABC';
        a1.Email_Key__c = 'TEST@TEST.COM';
       // a1.recordtypeid = PerRecordTypeId;
        insert a1;
        
        NSIBPM__SR_Template__c SRTemplate = new NSIBPM__SR_Template__c();
        srtemplate.NSIBPM__SR_RecordType_API_Name__c = 'Deal';
        insert SRTemplate;
        
        Id RecType1 = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = a.id;
        sr.RecordTypeId = RecType1;
        //sr.Agency__c = a.id;
        sr.NSIBPM__SR_Template__c = SRTemplate.id;
        sr.Agency_Type__c = 'Corporate';        
        sr.Registration_Date__c = system.today();
        sr.doc_ok__c = true;
        sr.DP_ok__c = true;
        insert sr;
        
        user u = mpUsers.get('PC2')[0];
        u.managerid = mpUsers.get('DOS2')[0].id;
        update u;
        
        user u1 = mpUsers.get('DOS2')[0];
        u1.ManagerId = mpUsers.get('HOS2')[0].id;
        update u1;
        
        system.debug(mpUsers.get('HOD2'));
        user u2 = mpUsers.get('HOS2')[0];
        u2.ManagerId = mpUsers.get('HOD2')[0].id;
        update u2;
        
        user u4 = mpUsers.get('PC2')[1];
        u4.managerId = mpUsers.get('HOS2')[0].Id;
        update u4;
            
        
        List<Target__c> createdTargets = TestDataFactory.createTestTargetRecords(new List<Target__c>{
            new Target__c(User__c = u.Id, Month__c='May'), 
                new Target__c(User__c = u.Id, Month__c='June'), 
                new Target__c(User__c = u.Id, Month__c = 'July'), 
                new Target__c(User__c = u1.Id, Month__c='June'),
                new Target__c(User__c = u1.Id, Month__c='May'), 
                new Target__c(User__c = u1.Id, Month__c = 'July'), 
                new Target__c(User__c = u2.Id, Month__c='June'),
                new Target__c(User__c = u2.Id, Month__c='May'), 
                new Target__c(User__c = u2.Id, Month__c = 'July'),
                new Target__c(User__c = u4.Id, Month__c='June'),
                new Target__c(User__c = u4.Id, Month__c='May'), 
                new Target__c(User__c = u4.Id, Month__c = 'July')
                });
                
    Test.startTest();{ 
        List <deal_team__c> dealTeamToInsert = new List <deal_team__c> ();
            deal_team__c dt = new deal_team__c();
            dt.Associated_PC__c = u.id;
            dt.Associated_Deal__c  = sr.id;
            dealTeamToInsert.add (dt);

            deal_team__c dt1 = new deal_team__c();
            dt1.Associated_PC__c = u.id;
            dt1.Associated_DOS__c = u1.id;
            dt1.Associated_Deal__c  = sr.id;
            dt1.Primary_Team__c = true;
            dealTeamToInsert.add (dt1);
            
            deal_team__c dt2 = new deal_team__c();
            dt2.Associated_PC__c = u.id;
            dt2.Associated_DOS__c = u1.id;
            dt2.Associated_HOS__c = u2.Id;
            dt2.Associated_Deal__c  = sr.id;
            dealTeamToInsert.add (dt2);
            
            
            deal_team__c dt3 = new deal_team__c();
            dt3.Associated_PC__c = u.id;
            dt3.Associated_Deal__c  = sr.id;
            dt3.status__c = 'Inactive';
            dealTeamToInsert.add (dt3);
            
            deal_team__c dt4 = new deal_Team__c();
            dt4.Associated_PC__c = u.id;
            dt4.Associated_DOS__c = u1.id;
            dt4.Associated_Deal__c  = sr.id;
            dealTeamToInsert.add (dt4);
            
            deal_team__c dt5 = new deal_Team__c();
            dt5.Associated_PC__c = u4.id;
            dt5.Associated_Deal__c  = sr.id;
            dealTeamToInsert.add (dt5);
            
            insert dealTeamToInsert;
            
            
            dt2.Associated_PC__c = u1.id;
            dt2.Associated_DOS__c = u.id;
            dt2.Associated_HOS__c = u.id;
            dt2.Associated_HOD__c = u.id;
            update dt2;
        }
        Test.stopTest();
    }
}