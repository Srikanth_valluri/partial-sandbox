/**************************************************************************************************
* Name               : DAMAC_Upgrade_Process                                                     *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR          DATE          COMMENTS                                              *
**************************************************************************************************/
public with sharing class DAMAC_Reservation_Upgrade_Process{
     public List <String> paymentTermsFromIPMS { get; set; }
    Map <String, String> paymentTermsFromIPMSMap;
    public Form_Request__c formRequest { get; set; }
    public User userDetails { get; set; }
    public DAMAC_Reservation_Upgrade_Process(ApexPages.StandardController controller) {
        
        srRecord = [select id,Name,SPA_Type__c, Generate_debit_letter__c, Generate_Credit_Letter__c ,Generate_side_Letter__c,SR_Recovery_Status__c, (select id from Bookings__r) from NSIBPM__Service_Request__c where id=:apexpages.currentpage().getparameters().get('id')];
        BookingId = srRecord.bookings__r[0].id;
    }
     public Map <ID, boolean> paymentPlanNewMap { get; set; }
public List <String> tempPaymentTermsDeleted { get; set; }
    
    public string selectedTypeofCandT {get;set;}
       public list<Booking_unit__c> exitUnits {get;set;} 
    
    public list<inventory__c> entryUnits {get;set;} 
    
    public list<Booking_unit__c> selectedexitUnits {get;set;}
    
    public string exitUnitString {get;set;}
    
    public string entryUnitString {get;set;}
    
    public list<inventory__c> selectedentryUnits {get;set;}  
    
    public string exitSearchkey {get;set;}
        
    public integer totalExitUnitsValue {get;set;}
    
    public integer totalExitUnitsValueRequested {get;set;}
    
    public integer totalEntryUnitsValue {get;set;}
    
    public string customPageMessage {get;set;}
    
    public boolean isError{get;set;}
    
    public boolean isSuccess{get;set;}
    
    public boolean isWarning{get;set;}
    
    public string srId{get;set;}
    
    // Data Sheet for C and T
    public NSIBPM__Service_Request__c srRecord {get;set;}
    
    public boolean hasNewBooking {get;set;}
    
    public List<Form_Request_Inventories__c> selectedEntryReqInvs { get; set; }
    
    public set <ID> selectedInventories { get; set; }
    
    public Id recordId { get; set; }
    public String loggedinUser { get; set; }

    public DAMAC_Reservation_Upgrade_Process(){
        hasNewBooking = false;
         paymentPlanNewMap = new Map <ID, boolean> ();
       selectedInventories = new Set <ID> ();
        exitUnits  = new list<Booking_unit__c>();
        entryUnits = new list<inventory__c>();
        selectedexitUnits  = new  list<Booking_unit__c>();
        selectedentryUnits = new list<inventory__c>();
         paymentTermsFromIPMS = new List <String> ();
        paymentTermsFromIPMSMap = new Map <String, String> ();
        tempPaymentTermsDeleted = new List <String> ();
        selectedEntryReqInvs = new List<Form_Request_Inventories__c>();
        totalExitUnitsValue = 0;
        totalEntryUnitsValue = 0;
        totalExitUnitsValueRequested  = 0;
        customPageMessage = null;
        isError = false;
        isSuccess = false;
        isWarning = false;
        loggedinUser = userinfo.getuserId();
            formRequest = new Form_Request__c ();
        
        recordId =apexpages.currentpage().getparameters().get('id');
        userDetails = new User ();
        userDetails = [SELECT isPortalEnabled FROM User WHERE ID =: Userinfo.getUserId ()];
        if(recordId != null){
        formRequest = Database.query ('SELECT inquiry__r.First_Name__c, Inquiry__r.Last_name__c, inquiry__r.Name, Corporate__r.name, '
                                    +getAllFields('Form_request__c')+' FROM Form_request__c WHERE Id =: recordId');
             selectedEntryReqInvs = [SELECT Name,inventory__c, inventory__r.Unit__c, inventory__r.Unit_Name__c ,inventory__r.Special_Price_calc__c 
                         FROM Form_Request_Inventories__c WHERE Form_Request__c =:recordId AND Upgrade_Type__c = 'Entry'];
             entryUnitString = '';            
             for (Form_Request_Inventories__c bu :selectedEntryReqInvs ) {
                 entryUnitString += bu.Id + ',';
                 selectedInventories.add (bu.inventory__c);
             }
             entryUnitString = entryUnitString.removeEnd(',');
         }

        
    }
    @RemoteAction 
    public static List<Inquiry__c> inquiryDetails(String searchKey){
        String key = '%'+searchKey+'%';
        User userObj = [SELECT IsPortalEnabled  FROM user WHERE id=:userinfo.getuserid() LIMIT 1];
        
        if(userObj.IsPortalEnabled) {
            Contact loginContact = AgentPortalUtilityQueryManager.getContactInformation();
            system.debug ('-- Portal Enabled --');
            return [select 
                            id,Name,First_name__c,Last_Name__c,Passport_Expiry_Date__c,Email__c,Phone_CountryCode__c,
                            Nationality__c,Phone__c,Date_of_Birth__c,Title__c,Mobile_Phone__c,Passport_Number__c from inquiry__c 
                            where ( First_Name__c LIKE:key or Last_Name__c LIKE:key or Name LIKE:key ) 
                            and  ( Inquiry_Status__c='Active' OR Inquiry_Status__c='New' OR Inquiry_Status__c='Qualified' 
                            OR Inquiry_Status__c ='Meeting Scheduled' )and (recordtype.Name='CIL' OR recordtype.Name='Inquiry') 
                            and Agent_Name__c =:loginContact.id LIMIT 10]; 
        } else {
    
        
            return [SELECT Name, First_name__c, Last_Name__c, Email__c, Mobile_Phone__c, Passport_Number__c 
                       FROM Inquiry__c 
                       WHERE ( First_Name__c LIKE:key OR Last_Name__c LIKE:key OR Name LIKE:key ) 
                       AND  ( Inquiry_Status__c='Active' 
                             OR Inquiry_Status__c='New' 
                             OR Inquiry_Status__c='Qualified' 
                             OR Inquiry_Status__c ='Meeting Scheduled' OR Inquiry_Status__c ='Negotiation' 
                             OR Inquiry_Status__c ='Negotiation In-Progress' 
                             OR Inquiry_Status__c='Contact Made')
                       AND (recordtype.Name='Inquiry' OR recordtype.Name='Inquiry Decrypted') 
                       AND ownerid=:userinfo.getuserid() 
                       LIMIT 10];  
        }
    }
    @RemoteAction 
    public static List<Account> accountDetails (String searchKey, String btype){
        List<Account> AcconuntList = new List<Account>();
        if(btype == 'Individual'){        
            AcconuntList = [SELECT Name, FirstName, LastName FROM Account 
                            WHERE Ownerid=:userinfo.getuserid() 
                                AND (firstname LIKE:searchKey OR lastName LIKE:searchKey OR name LIKE:'%'+searchkey+'%' ) 
                                AND recordtype.Name='Person Account' LIMIT 10];
        }
        if(btype == 'Corporate'){        
            AcconuntList = [SELECT Name, FirstName, LastName FROM Account 
                            WHERE ownerid=:userinfo.getuserid() 
                                AND (firstname LIKE:searchKey OR lastName LIKE:searchKey OR name LIKE:'%'+searchkey+'%' ) 
                                AND recordtype.Name='Business Account' LIMIT 10];            
        }        
        return AcconuntList;
    }
    public void populateAccountOnFormRequest() {
        String accountId = Apexpages.currentpage().getparameters().get('accountId');
        Account acc = [SELECT Nationality__c, Passport_number__c, Address_line_1__c, country__c, City__c,
                          Organisation_Name__c, CR_Number__c, CR_Registration_Place__c,
                          CR_Registration_Expiry_Date__c, Agency_Email__c, Name
                          FROM Account WHERE Id =: accountId];
        formRequest.Passport_number__c = acc.Passport_number__c;
        formRequest.Address__c = acc.Address_line_1__c;
        formRequest.Country__c = acc.Country__c;
        formRequest.City__c = acc.City__c;
        formRequest.Email__c = acc.Agency_Email__c;
        formRequest.Corporation_Name__c = acc.Organisation_Name__c;
        formRequest.Registration_Number__c = acc.CR_Number__c;
        formRequest.Registered_In__c = acc.CR_Registration_Place__c;
        formRequest.Nationality__c = acc.Nationality__c;
        formRequest.Corporate__c = acc.Id;
        formRequest.Purchaser_Name__c = acc.Name;
    } 
   
    
    public void searchExitUnits(){
        isWarning = false;
        iserror = false;
        CustomPageMessage = null;
        system.debug('exitSearchkeyexitSearchkey'+exitSearchkey);
        if(!string.isblank(exitSearchkey) && exitSearchkey != ''){ 
            string key =  '%'+exitSearchkey+'%';           
            exitUnits = [select id,
                         Name,
                         Inventory__r.unit__c,
                         inventory__r.Unit_Name__c,
                         Booking__r.Deal_sr__r.name,
                         Requested_Price_AED__c,
                         requested_price__C,
                         Total_Collections__c,
                         Total_Collections_New__c,
                         Inventory__r.status__c,   
                         Inventory__r.Off_Plan__c,
                         Booking__r.Deal_sr__r.OwnerId 
                         FROM booking_unit__c 
                         where inventory__r.Unit_Name__c LIKE : key
                         AND inventory__c NOT IN :selectedInventories
                         AND Booking__c != null AND Booking__r.Deal_sr__c != null 
                         LIMIT 5]; 
            if(exitUnits.size() == 0){
                isWarning = true;
                CustomPageMessage = 'No Results returned , Please refine your search and try again';
            }                       
        }else{
            exitUnits = new list<Booking_unit__c>();
        }
        
        system.debug('exitUnits'+exitUnits);
    }
    
    // EXIT    
    public void holdSelectedunits(){
        if(exitUnitString != ''){
            list<string> idHolder = exitUnitString.split(',');
            set<string> tempDeDupeHolder = new set<string>();            
            tempDeDupeHolder.addall(idHolder);
            system.debug(tempDeDupeHolder);
            queryExitUnits(tempDeDupeHolder);            
        }
        searchExitUnits();
    }
    
    
    public void queryExitUnits(set<string> bookingunitIds){ 
        
        list<Booking_unit__c> bu = new list<Booking_unit__c>();
        //bu = [select id,inventory__c,inventory__r.Unit_name__c,requested_price_aed__c,Total_Collections__c from Booking_unit__c where id =:bookingunitIds order by Inventory__r.unit_name__c];        
        string relationshipFields = 'inventory__r.Off_Plan__c, inventory__r.Unit_name__c,inventory__r.status__c,';
        bu = Database.query ('SELECT '+relationshipFields+ getAllFields ('Booking_unit__c') +' from Booking_unit__c where id =:bookingunitIds order by Inventory__r.unit_name__c');
        selectedexitUnits.clear();        
        selectedexitUnits.addall(bu);
        for(booking_unit__c b:bu){
            system.debug(totalExitUnitsValue + '---'+totalExitUnitsValueRequested);
            if ( b.Total_Collections_New__c != null ) {
                totalExitUnitsValue += integer.valueof(b.Total_Collections_New__c);
            } 
            if ( b.requested_price_aed__c != null ) {
                totalExitUnitsValueRequested += integer.valueof(b.requested_price_aed__c);
            }
        }
    }
     // Method to populate inquiry details on Reservation form when inquiry is selected from serach results
    public void populateInquiryOnFormRequest() {
        String inquiryId = Apexpages.currentpage().getparameters().get('inquiryId');
        Inquiry__c inq = [SELECT Nationality__c, Passport_number__c, Address_line_1__c, country__c, City__c,
                          Organisation_Name__c, Email__c, CR_Number__c, CR_Registration_Place__c,
                          CR_Registration_Expiry_Date__c, First_Name__c, Last_Name__c, Mobile_CountryCode__c, Country_of_Permanent_Residence__c
                          FROM Inquiry__c WHERE Id =: formRequest.Inquiry__c];
        formRequest.Nationality__c = inq.Nationality__c;
        formRequest.Mobile_Country_Code__c = inq.Mobile_CountryCode__c;
        formRequest.Address__c = inq.Address_line_1__c;
        formRequest.Email__c = inq.Email__c;
        formRequest.Registration_Number__c = inq.CR_Number__c;
        formRequest.Passport_number__c = inq.Passport_number__c;
        formRequest.Purchaser_Name__c = inq.First_Name__c+'-'+inq.Last_Name__c;
        formRequest.Country__c = inq.Country__c;
        formRequest.City__c = inq.City__c;
        formRequest.Corporation_Name__c = inq.Organisation_Name__c;
        formRequest.Registered_In__c = inq.CR_Registration_Place__c;
        formRequest.Country_of_Permanent_Residence__c = inq.Country_of_Permanent_Residence__c;
            
        
        formRequest.Inquiry__c = inq.Id;


    }
        
    //Validate Based on Type to Proceed
    public void validateToProceed(){    
        isError = false;    
        selectedexitUnits  = new  list<Booking_unit__c>();
        selectedentryUnits = new list<inventory__c>();
        totalExitUnitsValue = 0;
        totalEntryUnitsValue = 0;
        totalExitUnitsValueRequested  = 0;
        holdSelectedunits();
        
        if(selectedexitUnits.size() == 0 ){            
            isError = true;
            customPageMessage = 'Please select Exit Units to Proceed';            
        }
        if(!isError){
            if(formRequest.Buyer_Type__c == null){
                isError = true;
                customPageMessage = 'Please select Buyer Type';
            }
            if(!isError){
                if (formRequest.Inquiry__c == null && formRequest.Corporate__c == null) {
                    isError = true;
                    customPageMessage = 'Please select either Inquiry or Account';
                }
                try {
                
                    if (String.valueOf(formRequest.Inquiry__c).length() == 0 && String.valueOf (formRequest.Corporate__c).length() == 0) {
                        isError = true;
                        customPageMessage = 'Pleae select either Inquiry or Account';
                    }
                } catch (Exception e) {
                    isError = true;
                    customPageMessage = 'Please select either Inquiry or Account';
                }
                if(isError != true){
                    boolean OutputSuccess = prepareStructure(selectedexitUnits);
                    if(!OutputSuccess){
                        isError = true;
                        customPageMessage = 'Error while creating structure';
                    }else{
                        isSuccess = true;
                        customPageMessage = 'Form Request Updated Successfully';
                    } 
                } 
            }          
        }        
    }
    
    
    private string BookingId ;
    // To Create a SR Record, Booking, Booking Units
    public boolean prepareStructure(list<booking_unit__c> bus){
        boolean success = false;
        try{
            DAMAC_Constants.skip_BookingUnitTrigger = TRUE;
            update formRequest;
                List <Form_request_Inventories__c> formInvs = new List <Form_request_Inventories__c> ();
            for (booking_unit__c bu:bus){
                Form_request_Inventories__c formInv = new Form_request_Inventories__c ();
                formInv.Inventory__c = bu.Inventory__c;
                formInv.Form_request__c = recordId;
                formInv.Upgrade_Type__c = 'Exit';
                formInv.Booking_Unit__c = bu.Id;
                formInvs.add (formInv);
            }
            insert formInvs;
            success = true;
        }catch(exception e){     
            System.Debug (e.getMessage ());       
            success = false;
        }
        return success;
    }

    /******************************************************************* UTILITY ********************************************************************/
 
    public static string getAllFields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        
        if (objectType == null)
            return fields;
        
        Map <String, Schema.SObjectField> fieldMap = objectType.getDescribe ().fields.getMap ();
        
        for (Schema.SObjectField sfield : fieldMap.Values()) {
            
            fields += sfield.getDescribe().getName ()+ ', ';
            
        }
        return fields.removeEnd(', '); 
    }
    /************************************************************************ CANCEL AND TRANSFER DATA SHEET ************************************************************************/
    
    /************ Payment plan variables ****/
    public map <string, list <selectoption>> selectPayPlansMap { get; set; }
    map <string, string> buildingIdMap;
    public Date effectiveFrom { get; set; }
    public Boolean showPaymentTerms { get; set; }
    public Date effectiveTo { get; set; }
    public double totalTermValue { get; set; }
    public List<Payment_Terms__c> tempPaymentTerms { get; set; }
    public String pageMessageTitle { get; set; }
    public String pageMessage { get; set; }
    public String pageMessageType { get; set; }
    public string PaymentPlanID {get; set;}
    Map <ID, List <Payment_Plan__c>> newPaymentPlans;
    Map <ID, List <Payment_Terms__c>> newPaymentTerms;
    public string bookingidForupdate {get; set;}
    public String selectedBookingUnitId { get; set; }
    public Integer lastRow { get; set; }
    
    // Buyer Variables
    public Map <ID, Buyer__c> primaryBuyerMap { get; set;}
    public Map <ID, List <Buyer__c>> jointBuyersMap { get; set;}
    public Boolean showJointBuyerLink { get; set; }
    public Boolean showPrimaryBuyerLink { get; set; }
    public Map <Decimal, Buyer_Fields_Meta_Data__mdt> newJointBuyerMetaDataMap { get; set; }
    public Map <String, String> titleMappings { get; set; }
    public Map <ID, String> primaryBuyerBookings { get; set; }
    public Map <ID, String> JointBuyerBookings { get; set; }
    public Integer newPrimaryBuyersCount { get; set; }
    public Map <Decimal, Buyer_Fields_Meta_Data__mdt> buyersMetaDataMap { get; set; }
    public Map <Booking_Unit__c, Id> buWithBookingIdMap { get; set; } 
    public Map <ID, List <Booking_Unit__c>> bookingWithBookingUnits { get; set; }
    public list<booking_unit__c> exitBookingUnits {get;set;}
    
    public list<booking_unit__c> entryBookingUnits {get;set;}
    
    public string selectedDldOption {get;set;}
    
    public string selectedRefundOption {get;set;}
    
    public string selectedBookingOption {get;set;}    
    
    public boolean multipleExitUnits {get;set;}
    
    public boolean multipleEntryUnits {get;set;}
    
    public string selectedPrimaryBuyer {get;set;}  
    
    Set <ID> bookingUnitIds;
    
    public buyer__c primarybuyer {get;set;}
    
    public buyer__c jointbuyer {get;set;}
    
    public string selectedWRULTemplate {get;set;}
    
    public selectOption[] selectedJointBuyers { get; set; }
    
    public selectOption[] alljointBuyers { get; set; }
    
    public list<selectOption> templatecodes {get;set;}
    string ex = 'Exit';
    string en = 'Entry';
    string relationshipFields = 'Inventory__r.Off_Plan__c, Inventory__r.Special_Price__c,Inventory__r.Building_Location__c, inventory__r.status__c,inventory__r.Property_Name_2__c,inventory__r.Unit_name__c,Booking__r.Deal_sr__r.Name,inventory__r.Master_Community_EN__c, inventory__r.Seller_Name__c,inventory__r.Special_price_calc__c,';
    public void loadDataSheet(){
        multipleExitUnits = false;
        multipleEntryUnits = false;
        srId = apexpages.currentpage().getparameters().get('id');
        srRecord = [select id,SPA_Type__c,Generate_debit_letter__c ,Deal_Type__c,Name,WRUL_Template__c,Generate_Credit_Letter__c, Generate_Side_Letter__c,SR_Recovery_Status__c,Form_request__c  from NSIBPM__Service_Request__c where id=:apexpages.currentpage().getparameters().get('id')];
        
        exitBookingUnits = new list<booking_unit__c>();
        entryBookingUnits = new list<booking_unit__c>();
        Set<Id> reqInvs = new Set<Id>();
        for(Form_Request_Inventories__c  inv : [SELECT Booking_Unit__c
                         FROM Form_Request_Inventories__c WHERE Form_Request__c =:srRecord.Form_request__c  AND Upgrade_Type__c = 'Exit']){
         
             reqInvs.add(inv.Booking_Unit__c);
         }
        relationshipFields = 'Inventory__r.Off_Plan__c,inventory__r.Final_Total_Area__c, Inventory__r.Special_Price__c,Inventory__r.Building_Location__c,Inventory__r.View_Type__c, inventory__r.status__c,inventory__r.Property_Name_2__c,inventory__r.Unit_name__c,inventory__r.No_of_Bedrooms__c,Booking__r.Deal_sr__r.Name,inventory__r.Master_Community_EN__c, inventory__r.Seller_Name__c,inventory__r.Special_price_calc__c,';

        exitBookingUnits = Database.query ('SELECT '+relationshipFields+ getAllFields ('Booking_unit__c') +' from Booking_unit__c where Id IN :reqInvs');        
        entryBookingUnits = Database.query ('SELECT '+relationshipFields+ getAllFields ('Booking_unit__c') +' from Booking_unit__c where booking__r.deal_sr__c=\''+srRecord.Id+'\'');        
        if(exitBookingUnits.size() > 1){
            multipleExitUnits = true;
        }
        if(entryBookingUnits.size() > 1){
            multipleEntryUnits = true;
        }
    }
    
    /*public void saveDataSheet(){
        isError =  false;
        try{   
            
            if(showPrimaryBuyerLink){
                isError = true;
                customPageMessage = 'Please select Primary Buyer or Create a New Primary Buyer';
                
            }
            integer totalEntryAllocation = 0;
            integer totalExitAllocation = 0;
            for(booking_unit__c b:entryBookingUnits){
                b.Master_Community_EN__c = b.Inventory__r.Master_Community_EN__c;
                b.seller_Name__c = b.Inventory__r.Seller_Name__c;
                
                System.Debug (b.Allocated_Amount__c);
                totalEntryAllocation  += integer.valueof(b.Allocated_Amount__c);
                if(b.Credit_Amount__c != null){
                    b.Credit_Amount_in_Words__c = NumberToWord.english_number(integer.valueof(b.credit_Amount__c)); 
                }
            }
            Boolean oqooDRegFlag = false;
            for(booking_unit__c b:exitBookingunits){
                b.Master_Community_EN__c = b.Inventory__r.Master_Community_EN__c;
                b.seller_Name__c = b.Inventory__r.Seller_Name__c;
                
                totalExitAllocation += integer.valueof(b.Amount_to_Transfer__c);
                if (b.Other_Amount__c != NULL && b.Waiver_Charges__c != NULL 
                    && b.X4_DLD_AED_20__c != NULL && b.OQOOD__c != NULL) {
                    b.Net_deductions__c = b.Other_Amount__c + b.Waiver_Charges__c + Decimal.valueOf (b.X4_DLD_AED_20__c) + b.OQOOD__c;
                    if(b.Net_deductions__c != null){
                        b.Net_Deductions_in_Words__c = NumberToWord.english_number(integer.valueof(b.Net_deductions__c )); 
                    }
                }
                
                if (oqoodRegFlag == false)
                    oqoodRegFlag = b.OQOOD_Reg_Flag__c;
                
            }
            if (oqoodRegFlag == false) {
                if (srRecord.Generate_debit_letter__c == false) {
                    isError = true;
                    customPageMessage = 'The exit unit is unregistered please select debit letter';
                }
            }
            System.debug(totalExitAllocation  +'>>>>>>>'+totalEntryAllocation);
            
            if (totalEntryAllocation < totalExitAllocation ){
                isError = true;
                customPageMessage = 'Entry units total allocation cannot be less than Exit units total allocation';
            } 
            
            if (srRecord.Generate_Credit_Letter__c) {
                for (Booking_Unit__c bu : entryBookingUnits) {
                    if (bu.Credit_Amount__c == NULL) {
                        isError = true;
                        customPageMessage = 'Please enter Credit Amount.';
                        break;
                    }
                                                
                        
                }
            }
            // Need to check for Buyers for New Bookings
             system.debug('entryBookingUnits=-=-=-'+entryBookingUnits );
            if(!isError){
                
                List <Payment_Plan__c> planstoInsert = new List <Payment_Plan__c> ();
                Map<String, Payment_Plan__c> paymentPlanMap = getPaymentPlan(paymentPlanId);
            
                for (Booking_unit__c bu : entryBookingUnits) {
                    System.Debug ('@@@@@@@@@@@'+bu.Inventory__r.special_price_calc__c);
                    System.Debug ('@@@@@@@@@@@'+bu.requested_price__C);
                    if (bu.Debit_Amount__c != NULL) {
                        bu.Debit_Amount_in_Words__c = NumberToWord.english_number(integer.valueof(bu.Debit_Amount__c )); 
                    }
                    if (bu.Inventory__r.special_price_calc__c != bu.requested_price__C) {
                        srRecord.Requested_Price_Change__c = true;
                    }
                    // if it is new booking and existing payment plan is selected then need to clone the lan and terms and associate to Booking unit
                    if (bu.New_Booking__c) {
                        system.debug (newPaymentPlans);
                        
                        if (newPaymentPlans.containsKey (bu.id)) {
                            for (Payment_Plan__c plan : newPaymentPlans.get (bu.ID)) {
                                if (bu.Payment_plan_id__c == plan.id) {
                                    srRecord.New_Payment_Plan__c = true;
                                }
                            }
                        } else {
                            Payment_Plan__c Newplan = NEW Payment_Plan__c();
                            
                            paymentPlanMap = getPaymentPlan(bu.Payment_plan_ID__c);
                            System.Debug (paymentPlanMap);
                            
                            newplan = paymentPlanMap.get (bu.Payment_plan_ID__c);
                            if (newPlan == null) {
                                newPlan = NEW Payment_Plan__c();
                            }
                            newplan.building_location__c = NULL;
                            newplan.Effective_From__c = NULL;
                            Newplan.TERM_ID__c = '';
                            newplan.ID = NULL;
                            System.debug (bu.Payment_plan_ID__c+'Payment_plan_ID__c'+bu.id);
                            if (bu.payment_plan_id__c != '-None-') {
                                newPlan.Parent_Payment_Plan__c = bu.Payment_plan_ID__c;
                                newplan.Parent_Payment_Plan_ID__c = bu.Payment_plan_ID__c;
                            }
                            newPlan.booking_unit__c = bu.ID;
                            
                            planstoInsert.add (newPlan);
                        }
                    }
                }
                insert planstoInsert;
                
                Set <ID> newPlans = new Set <ID> ();
                for (Payment_Plan__C plan :planstoInsert) {
                    newPlans.add (plan.id);
                }
                planstoInsert = [SELECT Parent_Payment_Plan_ID__c, Booking_Unit__r.Payment_plan_ID__c FROM Payment_Plan__C WHERE ID IN: newPlans];
                // To insert payment terms for the payment plan
                List <Payment_Terms__c> newtermToInsert = new List <Payment_Terms__c> ();
                for (Payment_Plan__C plan :planstoInsert) {
                    
                    System.Debug (plan.Parent_Payment_Plan_ID__c);
                    System.Debug (paymentPlanMap);
                    paymentPlanMap = getPaymentPlan(plan.Parent_Payment_Plan_ID__c);
                    if (paymentPlanMap.containskey (plan.Parent_Payment_Plan_ID__c)) {
                        for (Payment_Terms__c term :paymentPlanMap.get(plan.Parent_Payment_Plan_ID__c).Payment_Terms__r) {
                            Payment_Terms__c newterm = new Payment_Terms__c ();
                            newterm = term.clone ();
                            newterm.id = null;
                            newTerm.Line_ID__c = '';
                            newTerm.Payment_Plan__c = plan.id;
                            if (newTerm.Milestone_Event__c != NULL) {
                                if (paymentTermsFromIPMSMap.containsKey (newTerm.Milestone_Event__c)) {
                                    newTerm.Milestone_Event_Arabic__c = paymentTermsFromIPMSMap.get (newTerm.Milestone_Event__c);
                                }
                            }
                            
                            newtermToInsert.add (newTerm);
                        }
                    }
                }
                insert newtermToInsert;
                //insert invUserToInsert;
                update exitBookingunits;
                System.Debug (entryBookingUnits);
                update entryBookingUnits;
                System.Debug (entryBookingUnits);
                
                WRUL_Templates__mdt template = new WRUL_Templates__mdt();
                if(selectedWRULTemplate != 'Others'){            
                    template = [select id,DeveloperName,template_name__c,DDP_ID__c from WRUL_Templates__mdt where DeveloperName=:selectedWRULTemplate LIMIT 1];
                }
                srRecord.SR_Recovery_Status__c = 'None';
                srRecord.WRUL_Template__c = template.template_name__c;
                srRecord.DDP_ID__c = template.DDP_ID__c;
                
                update srRecord;
                MarkBuyers();
                createDealTeam (srRecord, UserInfo.getUseriD());
                Approval.ProcessSubmitRequest submitReq = new Approval.ProcessSubmitRequest();
                submitReq.setComments('Submitting request for approval.');
                //submitReq.setProcessDefinitionNameOrId('Recovery_Process_For_New_Payment_Plan');
                submitReq.setObjectId(srRecord.ID);
                User u = [ SELECT ManagerID FROM User WHERE ID =: UserInfo.getUserId()];
                if (u.ManagerID != NULL) {
                    submitReq.setNextApproverIds(new Id[] {u.ManagerID});
                    Approval.ProcessResult result = Approval.process(submitReq);
                }
                isSuccess = true;
                CustomPageMessage = 'Recovery Initiated Successfully';
            }
        }catch(exception e){
            isError = true;
            CustomPageMessage = 'Unexpected Error , Please contact Admin '+e.getMessage()+''+e.getLineNumber();
        }
    }*/
}