public class ChangeInquiryOwnerController { 

    public Inquiry__c  inqObj       {get; set;}
    public Boolean isError          {get; set;}
    public Boolean isContent        {get; set;}
    public String myMsg             {get; set;}
    public String idVal;
    private String profileName;

    public ChangeInquiryOwnerController(ApexPages.StandardController controller) {
        isError = false;
        isContent = false;
        myMsg = '';
        inqObj = (Inquiry__c) controller.getRecord();
        inqObj = [SELECT OwnerId ,
                         Inquiry_Status__c
                    FROM Inquiry__c 
                   WHERE Id = :inqObj.id];
    }

    public void updateInquiryOwnerPage() {
        if (inqObj != null) {
            profileName = [SELECT Id,Name
                                FROM Profile
                                WHERE Id =:userinfo.getProfileId()].Name;
            if(profileName.equalsIgnoreCase('Telesales Team')) {
                isContent = true;
            }
            else {
                List<String> changeInquirylst = new List<String>();
                changeInquirylst = Label.ChangeInquiryOwner.split(',');
                Set<String> changeInquirySet = new Set<String>();
                changeInquirySet.addAll(changeInquirylst);
                if (inqObj.Inquiry_Status__c.equalsIgnoreCase('Agent Converted') ) { 
                    myMsg = ' You cannot change Inquiry Owner if the Inquiry Status is \'Agent Converted\'. ';
                    isError = true; 
                } else if (!changeInquirySet.Contains(UserInfo.getName())) {
                    myMsg = ' You do not have permissions to change Owner. ';
                    isError = true;
                } else {
                    isContent = true;
                }
            }
        }
    }

    public PageReference save() {
        if(profileName.equalsIgnoreCase('Telesales Team')) {
            changerOwnerForTSE();
        }
        else {
            inqObj.Inquiry_Status__c = 'Potential Agent';
            update inqObj;
        }
        PageReference endlocation = new PageReference('/' + inqObj.Id);
        return endlocation;
    }

    public void changerOwnerForTSE() {
        List<String> meetingTypeList = new List<String>();
        meetingTypeList = Label.Activity_Type_for_Owner_Change.split(',');
        List<Task> activities = [SELECT Id, Subject, Status
                                FROM Task
                                WHERE WhatId = :inqObj.Id
                                AND Status != 'Completed'
                                AND Activity_Type_3__c IN: meetingTypeList];
        for(Task act : activities) {
            act.OwnerId = inqObj.OwnerId;
        }

        Update inqObj;
        if(activities != null && !activities.isEmpty()) {
            update activities;
        }
    }

}