/**
 * @File Name          : WelcomeCLCBREmailBatch_Task.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 11/7/2019, 4:56:57 PM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    10/21/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
global class WelcomeCLCBREmailBatch_Task implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {

    public static final String STR_ADDRESS = 'no-replysf@damacgroup.com';
    
    String listQuery;

    public WelcomeCLCBREmailBatch_Task(){

    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
         
        listQuery=   'SELECT Id,'+
                        ' Subject,' + 
                        ' Status,' + 
                        ' WhatId,' + 
                        ' CreatedDate,' +
                        ' OwnerId' +
                   ' FROM Task'+
                   ' WHERE Subject = \'Incorrect Contact Details\'' + 
                   ' AND Status =\'Not Started\'' +
                   ' AND WhatId != null' +  
                   ' AND Assigned_User__c =\'RM\'';
                   
        System.debug('-->> listQuery: '+ listQuery);           
        return Database.getQueryLocator(listQuery);
    }
    global void execute(Database.BatchableContext BC, List<Task> taskList) {
        System.debug('taskList:***'+taskList);
        if(System.Label.Bypass_WelcomeCLCBREmailBatchLogic == 'N'){

            List<User> ownerEmailLst = [
                SELECT Id
                    , Email
                    , FirstName
                    , LastName
                    , DOS_Email__c
                    , HOS_Email__c
                    , HOD_Email__c
                FROM User 
                WHERE Id =:taskList[0].OwnerId
            ];
            System.debug('-->> ownerEmailLst: '+ ownerEmailLst);                       
            for(Task taskObj  : taskList){
                System.debug('-->> taskObj.CreatedDate: '+ taskObj.CreatedDate);
                System.debug('-->> System.Today(): '+ System.Today());
                DateTime dT1 = System.now();
                Date myDate1 = date.newinstance(dT1.year(), dT1.month(), dT1.day());
                DateTime dT2 = taskObj.CreatedDate;
                Date myDate2 = date.newinstance(dT2.year(), dT2.month(), dT2.day());
                Integer numberOfDays = (myDate2).daysBetween(myDate1);
                System.debug('-->> numberOfDays: '+ numberOfDays);
                if(numberOfDays == 1){
                    creatEmailMessage(taskObj, ownerEmailLst, false);
                } else if (numberOfDays >= 2) {
                    creatEmailMessage(taskObj, ownerEmailLst, true);  
                }
                
            }
        }
    }
    global void finish(Database.BatchableContext BC) {
    }

    public static void creatEmailMessage(Task taskObj, List<User> ownerEmailLst, Boolean addCCAddress){
           list<string> dpinvoiceListType = new list<string>();
            dpinvoiceListType =label.DpcallingListType.split(',');
            system.debug('Data:::Size:::'+dpinvoiceListType.size() );
            
        System.debug('-->>taskObj in creatEmailMessage: '+ taskObj);
        System.debug('-->>addCCAddress in creatEmailMessage:: '+ addCCAddress);
        String toAddress = '', fromAddress,replyToAddress = '', contentType, contentValue = '',contentBody = '';
        String subject, strAccountId, strRelatedToId, strDPIId, bccAddress ='', strCCAddress;
        String strRemoveFromLabel_1 = System.Label.Remove_Email_From_WelcomeCLCBR_Task_Email;
        System.debug('-->>strRemoveFromLabel_1 : '+ strRemoveFromLabel_1);
        String strRemoveFromLabel = strRemoveFromLabel_1.deleteWhitespace();
        System.debug('-->>strRemoveFromLabel : '+ strRemoveFromLabel);
        List<String> listRemoveFromLabel = new List<String>();
        listRemoveFromLabel = strRemoveFromLabel.split(',');
        System.debug('-->>listRemoveFromLabel : '+ listRemoveFromLabel);
        System.debug('-->>listRemoveFromLabel.size : '+ listRemoveFromLabel.size());
        
        List<Calling_List__c> listCalling_List = [SELECT Id, Name, Calling_List_Type__c,Account__c, Account__r.Name, Unit_Name__c, Registration_ID__c FROM Calling_List__c WHERE Id = :taskObj.WhatId AND Calling_List_Type__c=:dpinvoiceListType];
        System.debug('-->>listCalling_List : '+ listCalling_List);

        fromAddress = STR_ADDRESS;

        bccAddress = Label.Sf_Copy_Bcc_Mail_Id;
        contentType = 'text/html';
        strCCAddress = '';
        System.debug('-->>listRemoveFromLabel.contains : '+ listRemoveFromLabel.contains(ownerEmailLst[0].Email));
        if(!listRemoveFromLabel.contains(ownerEmailLst[0].Email)){
            toAddress = ownerEmailLst[0].Email;
            System.debug('-->>toAddress : '+ toAddress);
        }
        System.debug('-->>toAddress After : '+ toAddress);
        strAccountId = '';
        strRelatedToId = taskObj.WhatId;

        String bodyStr = 'Hi, <br /><br />';
        
        if(addCCAddress){
            subject = 'Alert! Task overdue notification';
            bodyStr += '<br /> This is to notify you that action on task'+ taskObj.Subject +' for '+ listCalling_List[0].Account__r.Name +' is overdue.';
            String s1 = '';
            System.debug('-->>listRemoveFromLabel.contains 1: '+ listRemoveFromLabel.contains(ownerEmailLst[0].DOS_Email__c));
            System.debug('-->>listRemoveFromLabel.contains 2: '+ listRemoveFromLabel.contains(ownerEmailLst[0].HOS_Email__c));
            System.debug('-->>listRemoveFromLabel.contains 3: '+ listRemoveFromLabel.contains(ownerEmailLst[0].HOD_Email__c));
            if(ownerEmailLst[0].DOS_Email__c != NULL && !listRemoveFromLabel.contains(ownerEmailLst[0].DOS_Email__c)){
                s1 += ownerEmailLst[0].DOS_Email__c + ',';
            }
            if(ownerEmailLst[0].HOS_Email__c != NULL && !listRemoveFromLabel.contains(ownerEmailLst[0].HOS_Email__c)){
                s1 += ownerEmailLst[0].HOS_Email__c + ',';
            }
            if(ownerEmailLst[0].HOD_Email__c != NULL && !listRemoveFromLabel.contains(ownerEmailLst[0].HOD_Email__c)){
                s1 += ownerEmailLst[0].HOD_Email__c + ',';
            }
            if(System.Label.WelcomeCLCBREmailBatchCCUser != Null && !listRemoveFromLabel.contains(System.Label.WelcomeCLCBREmailBatchCCUser)){
                s1 += System.Label.WelcomeCLCBREmailBatchCCUser + ',' ;
            }
            System.debug('-->>s1 : '+ s1);
            if(s1 != NULL || s1 != ''){
                strCCAddress =  s1.removeEnd(',');
            }
            System.debug('-->>strCCAddress : '+ strCCAddress);
        } else {
            subject = 'Alert! No action taken – 1 day old';
            bodyStr += '<br /> This is to notify you that you have missed the SLA of taking action on task'+ taskObj.Subject +' for '+ listCalling_List[0].Account__r.Name +' is overdue.';
        }

        bodyStr += '<br /> Please update the task immediately.';
        bodyStr += '<br /> <br /> Related Calling list number:'+ listCalling_List[0].Name +'<br /> Registration Id:'+ listCalling_List[0].Registration_ID__c +' <br /> Unit name:'+ listCalling_List[0].Unit_Name__c ;
        bodyStr += '<br /> <br /> Regards, <br /> Salesforce Team';

        callSendGridFromBatch(toAddress, strCCAddress, bccAddress, subject, fromAddress, replyToAddress, contentType, bodyStr, strAccountId, bodyStr, strRelatedToId);
    }

    public static void callSendGridFromBatch(String toAddress, String strCCAddress, String bccAddress, String subject, String fromAddress, String replyToAddress, String contentType, String contentValue, String strAccountId, String contentBody, String strRelatedToId){
        
        List<EmailMessage> lstEmails = new List<EmailMessage>();
        List<Attachment> lstAttach = new List<Attachment>();
        System.debug('-->> lstAttach: ' + lstAttach);
        // Callout to sendgrid to send an email
        SendGridEmailService.SendGridResponse objSendGridResponse =
            SendGridEmailService.sendEmailService(
                toAddress
                , ''
                , strCCAddress
                , ''
                , bccAddress
                , ''
                , subject
                , ''
                , fromAddress
                , ''
                , replyToAddress
                , ''
                , contentType
                , contentValue
                , ''
                , lstAttach
            );

            System.debug('-->> objSendGridResponse: ' + objSendGridResponse);
            
            if (objSendGridResponse.ResponseStatus == 'Accepted') {
                EmailMessage mail = new EmailMessage();
                mail.Subject = subject;
                mail.MessageDate = System.Today();
                mail.Status = '3';//'Sent';
                mail.RelatedToId = strRelatedToId;
                // mail.Account__c  = strAccountId;
                mail.Type__c = 'Reminder Email';
                mail.ToAddress = toAddress;
                mail.FromAddress = fromAddress;
                mail.TextBody = contentBody; //contentValue.replaceAll('\\<.*?\\>', '');
                mail.Sent_By_Sendgrid__c = true;
                mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                mail.CcAddress = strCCAddress;
                mail.BccAddress = bccAddress;
                lstEmails.add(mail);
                system.debug('Mail obj == ' + mail);
            }
        system.debug('lstEmails = '+ lstEmails );
        if( lstEmails != null && lstEmails.size() > 0 ) {
            insert lstEmails;
        }
    }
}