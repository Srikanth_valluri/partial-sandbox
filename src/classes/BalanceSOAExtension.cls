public without sharing class BalanceSOAExtension {
    public Id caseId;
    private Case caseobj;
    public Case objCase {get;set;}
    public Decimal decTotal = 0.0;
    
    public BalanceSOAExtension(ApexPages.StandardController controller) {
        this.caseId = controller.getId();
         caseobj= (Case)controller.getrecord();
        System.debug('in const:caseId ::'+caseobj);
    }
    
    public pageReference getBalanceSOA() {
        
        List<Task> lsTaskToUpdate = new List<Task>();
        decTotal = 0.0;
        list<Case> listCaseObject = new list<Case>([select Id, Seller__c, Booking_Unit__r.Id
                                                    , Booking_Unit__r.Registration_ID__c
                                                    , Recordtype.DeveloperName
                                                    from Case where Booking_Unit__c != null and Id =: caseId
                                                    and Booking_Unit__r.Registration_ID__c != null]);
        //system.debug('listCaseObject==='+listCaseObject);
        if(!listCaseObject.isEmpty()) {
            
            String strDueResponse = assignmentEndpoints.fetchAssignmentDues(listCaseObject[0].Booking_Unit__r);
            //system.debug('strDueResponse==='+strDueResponse);
            if(String.isNotBlank(strDueResponse)){
                map<String,Object> mapDeserializeDue = (map<String,Object>)JSON.deserializeUntyped(strDueResponse);
                if(mapDeserializeDue.get('status') == 'S'){
                    strDueResponse = strDueResponse.remove('{');
                    strDueResponse = strDueResponse.remove('}');
                    strDueResponse = strDueResponse.remove('"');
                    //lstPayments = new List<paymentInfo>();
                    //system.debug('after all replacements******'+strDueResponse);
                    for(String st : strDueResponse.split(',')){
                        String strKey = st.substringBefore(':').trim();
                        system.debug('*****strKey*****'+strKey);
                        if(!strKey.equalsIgnoreCase('Status')) {
                            //system.debug('st*****'+st);
                            Decimal calAmount = 0.0;
                            //paymentInfo objP = new paymentInfo();
                            //objP.strType = strKey;
                            if(Decimal.valueOf(st.subStringAfter(':').trim()) > 0) {
                               if( listCaseObject[0].Recordtype.Developername == 'Mortgage'
                                 && strKey.contains('Balance as per SOA')
                               ){
                                   //system.debug('*****INSIDE*****');
                                   calAmount = Decimal.valueOf(st.subStringAfter(':').trim());
                               }
                            }
                            else {
                               calAmount = 0.0;
                            }
                            decTotal = decTotal + calAmount;
                            system.debug('decTotal*************'+decTotal);
                            //lstPayments.add(objP);
                        }
                    }
                    Case caseObj = new Case();
                    caseObj.Id = caseId;
                    caseObj.Pending_Amount__c = decTotal;
                    system.debug('====='+listCaseObject[0].Recordtype.Developername);
                    system.debug('====='+dectotal);
                    if(listCaseObject[0].Recordtype.Developername == 'Mortgage'
                    && decTotal <= 0){
                        /*for( Task objTask : [ SELECT WhoId
                                      , WhatId
                                      , Type
                                      , Status
                                      , OwnerId
                                      , Id
                                      , Subject
                                      , CreatedDate
                                      , Description
                                      , Assigned_User__c
                                      , ActivityDate
                                      , Owner.Name 
                                   FROM Task
                                  WHERE WhatId =: listCaseObject[0].Id ] ) {
                            if( objTask.Subject == System.Label.Mortgage_27 ) {
                                objTask.Status = 'Closed';
                                objTask.Completed_DateTime__c = System.Now();
                            }
                            lsTaskToUpdate.add(objTask);
                         }*/
                        caseObj.Payment_Verified__c = true;
                    }
                    update caseObj;
                    
                    
                /*if(!lsTaskToUpdate.isEmpty() ){
                    update lsTaskToUpdate;
                }*/

                }
                //system.debug('====status'+mapDeserializeDue.get('status'));
                else if(mapDeserializeDue.get('status') == 'E'
                && mapDeserializeDue.containsKey('message')){
                    //errorMessage = 'Error : '+mapDeserializeDue.get('message');
                    Error_Log__c objErr = createErrorLogRecord(listCaseObject[0].Seller__c,
                                                    listCaseObject[0].Booking_Unit__r.Id, caseId);
                    objErr.Error_Details__c = 'Error : '+mapDeserializeDue.get('message');
                    //insert objErr;
                    insertErrorLog(objErr);
                }
            }
            else {
                //errorMessage = 'Error : No Response from IPMS for Payment Dues';
                Error_Log__c objErr = createErrorLogRecord(listCaseObject[0].Seller__c,
                                                    listCaseObject[0].Booking_Unit__r.Id, caseId);
                objErr.Error_Details__c = 'Error : No Response from IPMS for Payment Dues';
                //insert objErr;
                insertErrorLog(objErr);
            }
        }
        PageReference  pg = new PageReference ('/'+caseId);
        pg.setRedirect(true);
        return pg;
    }
    public static void insertErrorLog(Error_Log__c objErr){
        try{
            insert objErr;
        }catch(Exception ex){
            system.debug('Error Log ex*****'+ex);
        }
    }

    public static Error_Log__c createErrorLogRecord(Id accId, Id bookingUnitId, Id caseId){
        Error_Log__c objErr = new Error_Log__c();
        objErr.Account__c = accId;
        objErr.Booking_Unit__c = bookingUnitId;
        objErr.Case__c = caseId;
        objErr.Process_Name__c = 'Assignment';
        return objErr;
    }
}