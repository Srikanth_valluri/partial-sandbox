public class AsyncAgentWebserviceSchedulable implements Schedulable {
    
    private List<Id> lstAgencySiteIDsInsert;
    public AsyncAgentWebserviceSchedulable(List<Id> AgencySiteIDsInsertList){
        lstAgencySiteIDsInsert = AgencySiteIDsInsertList;
    }
    
    public void execute(SchedulableContext sc) {
        system.enqueueJob(new AsyncAgentWebservice(lstAgencySiteIDsInsert, 'Agent Site Creation'));
        
        //Abort the Scheduled class job once the job is queued
        System.abortJob(sc.getTriggerId());
    }
}