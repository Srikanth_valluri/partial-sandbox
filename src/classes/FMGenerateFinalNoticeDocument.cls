/*-------------------------------------------------------------------------------------------------
Description: Class to generate Final Notice Document from drawloop
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 07-04-2018       | Deepika Rajput   | 1. Added logic to generate Final Notice Document from drawloop
=============================================================================================================================
*/
public class FMGenerateFinalNoticeDocument {
    public String fmCaseId;
    public fm_case__c objFMCase;
    public FMGenerateFinalNoticeDocument(ApexPages.StandardController stdCon){
        fmCaseId = stdCon.getId();
        objFMCase = [SELECT Id
            , Booking_Unit__c
            , Notice_Type__c
           FROM fm_case__c 
           WHERE Id =: fmCaseId];
    }
    
    public void callDrawloop(){
        executeBatch();
    }
    
    public pageReference returnToCase(){
        PageReference Pg = new PageReference('/'+fmCaseId);
        return pg;
    }
    
    public void executeBatch() {
        system.debug('objFMCase====='+objFMCase);
        FM_GenerateDrawloopDocumentBatch  objInstance;
        objInstance = new FM_GenerateDrawloopDocumentBatch (fmCaseId
                                                            , System.Label.Final_Notice_DDPID
                                                            , System.Label.Final_Notice_TemplateId);
        
        Id batchId = Database.ExecuteBatch(objInstance);
        if(String.valueOf(batchId) != '000000000000000'){      
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Your request for Notice document was successfully submitted. Please check the documents section for the document in a while.');
            ApexPages.addMessage(myMsg);
        }else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Your request for Notice document could not be completed. Please try again later.');
            ApexPages.addMessage(myMsg);
        }
    }
} // end of class