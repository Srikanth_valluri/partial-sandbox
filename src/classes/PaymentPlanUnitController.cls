/****************************************************************************************************
* Name               : PaymentPlanUnitController
* Description        : Controller class for Payment Plan VF Page
* Created Date       : 16/10/2020
* Created By         : QBurst
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst         16/10/2020      Initial Draft
****************************************************************************************************/

    global with sharing class PaymentPlanUnitController {
    public string unitId {get; set;}
    public string floorId {get; set;}
    public string propertyName {get; set;}
    public string buildingId {get; set;}
    public string propertyId {get; set;}
    public string planDetails {get;set;}
    public string gfa {get;set;}
    public string plotArea {get;set;}
    public string suiteArea {get;set;}
    public string sellableArea {get;set;}
    public string balconyArea {get;set;}
    public string garageArea {get;set;}
    public string detailPrice {get;set;}
    public string garageTerrace {get;set;}
    public Location__c unit {get; set;}
    public Map<String, Id> planNamesMap {get; set;}
    public List<String> planNamesList {get; set;}
    public Inventory__c inventory {get; set;}
    public Map<String, Date> payPlanEndDateMap { get; set;}
    public String payPlanEndDateMapJSON {get; set;}
    /*********************************************************************************************
    * @Description : Constructor method
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public PaymentPlanUnitController() {
        unitId = ApexPages.currentPage().getParameters().get('Id');
        propertyName = '';
        buildingId = '';
        floorId = '';
        gfa = '-';
        plotArea = '-';
        sellableArea = '-';
        balconyArea = '-';
        garageArea = '-';
        detailPrice = '-';
        garageTerrace = '-';
        suiteArea = '-';
        planDetails = 'default';
        planNamesMap = new  Map<String, Id>();
        planNamesList = new List<String>();
        payPlanEndDateMap = new Map<String, Date>();
        payPlanEndDateMapJSON = JSON.serialize(payPlanEndDateMap);
        if(unitId != null && unitId != ''){
            system.debug('unitId: ' + unitId);
            unit = [SELECT Name, Location_Code__c, Location_ID__c, 
                         Building_Number__c , Building_Number__r.Building_Name__c,
                         Floor_Number__c, Building_Number__r.Property_Name__c,
                         Building_Number__r.Property_Name__r.Property_Name__c
                     FROM Location__c WHERE Id=:unitId];
            if(unit != null && unit.Building_Number__c != null){
                buildingId = unit.Building_Number__c;
            }
            if(unit != null && unit.Floor_Number__c != null ){
                floorId = unit.Floor_Number__c;
            }
            if(unit != null  && unit.Building_Number__c != null && unit.Building_Number__r.Property_Name__c != null 
                             && unit.Building_Number__r.Property_Name__r.Property_Name__c != ''){
                propertyName = unit.Building_Number__r.Property_Name__r.Property_Name__c;
                propertyId = unit.Building_Number__r.Property_Name__c;
            }
            inventory = [SELECT Id, Status__c, Unit_Location__c,GFA_Area__c,Balcony_Area__c,
                                 Plot_Area__c,Terrace_Area__c, Garage_Area__c, Unit_Area__c,
                                 Selling_Price__c, Garage_Terrace__c, Floor_Location__c,
                                 Building_Location__c, Property__c
                    FROM Inventory__c 
                    WHERE Unit_Location__c =:unitId];
            if(inventory != null){
                if(inventory.Floor_Location__c != null && (floorId == '' || floorId == null)){
                    floorId = inventory.Floor_Location__c;
                }
                if(inventory.Building_Location__c != null && (buildingId == '' || buildingId == null)){
                    buildingId = inventory.Building_Location__c;
                }
                system.debug('propertyId: ' + propertyId);
                if(inventory.Property__c != null && (propertyId == '' || propertyId == null)){
                    propertyId = inventory.Property__c;
                }
                if(inventory.GFA_Area__c != null && inventory.GFA_Area__c != ''){
                    gfa =  String.valueOf(decimal.valueOf(inventory.GFA_Area__c).setScale(2, RoundingMode.HALF_UP));
                }
                if(inventory.Plot_Area__c != null && inventory.Plot_Area__c != ''){
                    plotArea =  String.valueOf(decimal.valueOf(inventory.Plot_Area__c).setScale(2, RoundingMode.HALF_UP));
                }
                if(inventory.Unit_Area__c != null && inventory.Unit_Area__c != ''){
                    sellableArea =  String.valueOf(decimal.valueOf(inventory.Unit_Area__c).setScale(2, RoundingMode.HALF_UP));
                }
                if(inventory.Balcony_Area__c != null && inventory.Balcony_Area__c != ''){
                    balconyArea =  String.valueOf(decimal.valueOf(inventory.Balcony_Area__c).setScale(2, RoundingMode.HALF_UP));
                }
                if(inventory.Garage_Area__c != null ){
                    garageArea =  String.valueOf(inventory.Garage_Area__c.setScale(2, RoundingMode.HALF_UP));
                }
                if(inventory.Selling_Price__c != null){
                    detailPrice =  inventory.Selling_Price__c.setScale(2, RoundingMode.HALF_UP).format();
                }
                if(inventory.Garage_Terrace__c != null && inventory.Garage_Terrace__c != ''){
                    garageTerrace =  String.valueOf(decimal.valueOf(inventory.Garage_Terrace__c).setScale(2, RoundingMode.HALF_UP));
                }
                
            }
            for(Payment_Plan__c payPlan: [SELECT id, Name, Building_ID__c, Booking_Unit__c, Payment_Term_Description__c,
                                                Active__c, Effective_To_calculated__c, Effective_from__c,Effective_To__c
                                         FROM Payment_Plan__c
                                         WHERE Effective_To_calculated__c >= TODAY
                                         AND Active__c = TRUE
                                         ORDER BY CreatedDate DESC
                                         LIMIT 1000]){
                String planName = payPlan.Name;
                if(payPlan.Payment_Term_Description__c != null  && payPlan.Payment_Term_Description__c != ''){
                    planName += ' - ' + payPlan.Payment_Term_Description__c;
                }
                planNamesList.add(planName);
                planNamesMap.put(planName, payPlan.Id);
            }
            fetchPayPlans();
        }
    }

   public void fetchPayPlans(){
        List<Payment_Plan__c> payPlanList = new List<Payment_Plan__c>();
        List<Id> payPlanIds = new List<Id>();
        for(Payment_Plan_Association__c planAssoc : [SELECT Id, Name, Payment_Plan__c, 
                                                        Location__c, Effective_From__c,
                                                        Effective_To__c, Property__c,
                                                        Property__r.Property_Name__c
                                                     FROM Payment_Plan_Association__c
                                                     WHERE Location__c != NULL
                                                     AND Location__c =: unitId]){
            payPlanIds.add(planAssoc.Payment_Plan__c);
            if(!payPlanEndDateMap.containsKey(planAssoc.Payment_Plan__c)){
                payPlanEndDateMap.put(String.valueOf(planAssoc.Payment_Plan__c), planAssoc.Effective_To__c);
            }
        }
        payPlanEndDateMapJSON = JSON.serialize(payPlanEndDateMap);
        system.debug('payPlanIds: ' + payPlanIds);
        for(Payment_Plan__c payPlan: [SELECT Id, Building_Location__c, Name, 
                                            Payment_Term_Description__c, Effective_To_calculated__c, 
                                            LastModifiedDate, Effective_To__c,Effective_From__c,
                                            (SELECT Id, Name,Installment__c,Description__c,Milestone_Event__c, CreatedDate,
                                                Milestone_Event_Arabic__c,Percent_Value__c FROM Payment_Terms__r) 
                                       FROM Payment_Plan__c WHERE Id IN: payPlanIds]){
                payPlanList.add(payPlan);
            }
            system.debug('planList:'+payPlanList);
            planDetails = JSON.serialize(payPlanList);
    }

  public void associatePayPlan(){
       system.debug('eneterd');
        String payPlanId = ApexPages.currentPage().getParameters().get('payPlanId');
        String endDate = ApexPages.currentPage().getParameters().get('endDate');
        System.debug('payPlanId'+payPlanId);
        System.debug('endDate'+endDate);
        /*
        Payment_Plan__c explan = [select Id, Building_ID__c, Name, Building_Location__c,
                                  Effective_From__c,Effective_To__c ,
                                  (SELECT Id, Description__c, Milestone_Event__c, Line_ID__c,
                                                 Milestone_Event_Arabic__c, Percent_Value__c, Payment_Date__c,
                                                 Installment__c, Modified_Percent_Value__c, Seq_No__c, Event_Days__c
                                                 FROM Payment_Terms__r ORDER  BY Installment__c),
                                  Effective_To_calculated__c, TERM_ID__c from Payment_Plan__c Where id=: payPlanId];
        system.debug('explan: ' + explan.Name);
        Payment_Plan__c newPlan = new Payment_Plan__c();
        newplan = explan.clone();
        newplan.Building_ID__c = unit.Location_ID__c;
        newPlan.Building_Location__c = unitId;
        if(endDate != null && endDate != ''){
            newplan.Effective_To__c =  Date.parse(endDate);
        }
        newplan.Parent_Payment_Plan__c = explan.Id;
        insert newplan;
       system.debug('newPlan'+newplan);
        List<Payment_Terms__c> payTermsToInsert = new List<Payment_Terms__c>();
        for(Payment_Terms__c term: explan.Payment_Terms__r){
            Payment_Terms__c newTerm = term.clone();
            newTerm.Payment_Plan__c = newplan.Id;
            payTermsToInsert.add(newTerm);
        }
        insert payTermsToInsert;
        */
        Payment_Plan_Association__c planAssoc = new Payment_Plan_Association__c();
        planAssoc.Payment_Plan__c = payPlanId;
        planAssoc.Location__c = unitId;
        planAssoc.Effective_From__c = system.today();
        if(endDate != null && endDate != ''){
            planAssoc.Effective_To__c =  Date.parse(endDate);
        }
        insert planAssoc;
        fetchPayPlans();
    }
}