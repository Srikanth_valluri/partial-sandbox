@isTest
private class CustomerCommunityUtilsTest {

    @isTest
    static void testPortalUserFunctions() {
        User communityUser = CommunityTestDataFactory.createPortalUser();

        System.runAs(communityUser) {

            System.assert(CustomerCommunityUtils.isPortalUser());
            System.assert(CustomerCommunityUtils.isPortalUser(UserInfo.getUserId()));
            System.assertNotEquals(NULL, CustomerCommunityUtils.getCustomerAccountId());
            System.assertNotEquals(NULL, CustomerCommunityUtils.getCustomerContactId());
            System.assertNotEquals(NULL, CustomerCommunityUtils.getPartyId());
            System.assertNotEquals(NULL, CustomerCommunityUtils.getFullPhotoUrl());
            System.assertEquals(NULL, CustomerCommunityUtils.getPassportExpiryDate());

            System.assert(!CustomerCommunityUtils.doesUserNeedToTakeSurvey());
            insert new CustomerCommunitySettings__c(
                SetupOwnerId = UserInfo.getOrganizationId(),
                LastSurveyCheckPeriodInDays__c = 90
            );
            System.assert(!CustomerCommunityUtils.doesUserNeedToTakeSurvey());


        }
    }

    @isTest
    static void testIsCurrentView() {
        Test.startTest();
            System.assert(!FmcUtils.isCurrentView('HOme'));
        Test.stopTest();
    }

    @isTest
    private static void testUtilityFunctions() {
        Decimal amount = 987654;
        System.assertEquals('987.65 K', CustomerCommunityUtils.getAmountInShortWords(amount));
        amount = 100000;
        System.assertEquals('100 K', CustomerCommunityUtils.getAmountInShortWords(amount));
        amount = 100000.10;
        System.assertEquals('100 K', CustomerCommunityUtils.getAmountInShortWords(amount));
        amount = 100000.00;
        System.assertEquals('100 K', CustomerCommunityUtils.getAmountInShortWords(amount));

        Map<Integer, String> mapIntegerToMonth = new Map<Integer, String> {
            1 => 'JAN', 2 => 'FEB', 3 => 'MAR', 4 => 'APR', 5 => 'MAY', 6 => 'JUN',
            7 => 'JUL', 8 => 'AUG', 9 => 'SEP', 10 => 'OCT', 11 => 'NOV', 12 => 'DEC'
        };

        Date today = Date.today();
        System.assertEquals(today, CustomerCommunityUtils.parseIpmsDateString(
            today.day() + '-' + mapIntegerToMonth.get(today.month()) + '-' + today.year()
        ));

        System.assertEquals(NULL, CustomerCommunityUtils.parseIpmsDateString('-'));
        System.assertEquals(NULL, CustomerCommunityUtils.parseIpmsDateString('A-B-C'));
    }

}