/*
* Version    Author    Date            Description
* 1.0        Vivian    29 Aug 2018     Grant access to Account record for FM Users if User resides in their bldg
*/
public without sharing class AccountDetailOverride{
    public String recordId;
    
    public AccountDetailOverride(ApexPages.StandardController sc){
        recordId = sc.getId();
        system.debug('*****Constructor recordId*****'+recordId);
    }
    
    public pageReference redirectToDetail(){
        PageReference pgRef;
        list<String> lstProfiles = new list<String>();
         if(Test.isRunningTest()){
            lstProfiles.add('System Administrator');
        }else{
            lstProfiles.addAll(Label.FM_Profile_Names.split(','));
        }
        list<User> lstUser = [Select Id
                                   , ProfileId
                                   , Profile.Name 
                              From User 
                              Where Id =:UserInfo.getUserId()
                              and Profile.Name IN : lstProfiles];
        // If current user is not an FM user redirect to Account detail page
        system.debug('*****lstUser*****'+lstUser);
        if(lstUser == null || lstUser.isEmpty()){
            pgRef = new Pagereference('/'+recordId);
            pgRef.getParameters().put('nooverride', '1');
            return pgRef;
        }else{
            set<String> setBldgName = new set<String>();
            for(FM_User__c objUser : [Select Id
                                           , FM_User__c
                                           , Building__c
                                           , Building__r.Name
                                           , Building__r.Location_Code__c
                                      From FM_User__c
                                      Where FM_User__c =: UserInfo.getUserId()]){
                system.debug('*****objUser.Building__c*****'+objUser.Building__c);
                system.debug('*****objUser.Building__r.Name*****'+objUser.Building__r.Name);
                if(objUser.Building__c != null){
                    setBldgName.add('%'+objUser.Building__r.Location_Code__c+'%');
                }
            }
            system.debug('*****setBldgName*****'+setBldgName);
            // If current user is not set as an FM user for any bldg do not grant access
            if(!setBldgName.isEmpty()){
                boolean blnAllowAccess = false;
                for(Booking_Unit__c objBU : [Select Id
                                                  , Building_Name__c
                                                  , Tenant__c
                                                  , Resident__c
                                                  , Booking__c
                                                  , Booking__r.Account__c
                                                  , Unit_Name__c 
                                             From Booking_Unit__c 
                                             Where Unit_Name__c LIKE : setBldgName
                                             and (Resident__c =: recordId
                                             or Booking__r.Account__c =: recordId
                                             or Tenant__c = :recordId) 
                                             LIMIT 1]){
                    system.debug('*****recordId*****'+recordId);
                    system.debug('*****objBU.Resident__c*****'+objBU.Resident__c);
                    system.debug('*****objBU.Booking__r.Account__c*****'+objBU.Booking__r.Account__c);
                    /*
                    if((objBU.Resident__c != null && recordId == objBU.Resident__c)
                    || (objBU.Booking__r.Account__c != null && recordId == objBU.Booking__r.Account__c)){
                    */
                        blnAllowAccess = true;
                        system.debug('*****grant access*****');
                        break;
                    /*
                    }
                    */
                }
                // If units are found for the FM Users bldg redirect to Account detail page
                if(blnAllowAccess){
                    pgRef = new Pagereference('/'+recordId);
                    pgRef.getParameters().put('nooverride', '1');
                    return pgRef;
                }else{
                    // If units are NOT found for the FM Users bldg do not grant access
                    system.debug('*****If units are NOT found for the FM Users bldg do not grant access*****');
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'You do not have access to this Account record. Please contact your Administrator');
                    ApexPages.addMessage(myMsg);
                    return null;
                }
            }else{
                // If current user is not set as an FM user for any bldg do not grant access
                system.debug('*****If current user is not set as an FM user for any bldg do not grant access*****');
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'You do not have access to this Account record. Please contact your Administrator');
                ApexPages.addMessage(myMsg);
                return null;
            }
        }
    } // end of redirectToDetail method
} // end of class