public class Damac_paymentTermsJSON {
    public class OutputParameters {
      
        public X_RESPONSE_MESSAGE X_RESPONSE_MESSAGE;
        public String X_RETURN_STATUS;
        public String X_RETURN_MESSAGE;
    }
    
    public OutputParameters OutputParameters;
    
    public class X_RESPONSE_MESSAGE_ITEM {
        public String ATTRIBUTE1;
        public String ATTRIBUTE2;
        public Object ATTRIBUTE3;
        public Object ATTRIBUTE4;
        public Object ATTRIBUTE5;
        public Object ATTRIBUTE6;
        public Object ATTRIBUTE7;
        public Object ATTRIBUTE8;
        public Object ATTRIBUTE9;
        public Object ATTRIBUTE10;
        public Object ATTRIBUTE11;
        public Object ATTRIBUTE12;
        public Object ATTRIBUTE13;
        public Object ATTRIBUTE14;
        public Object ATTRIBUTE15;
        public Object ATTRIBUTE16;
        public Object ATTRIBUTE17;
        public Object ATTRIBUTE18;
        public Object ATTRIBUTE19;
        public Object ATTRIBUTE20;
        public Object ATTRIBUTE21;
        public Object ATTRIBUTE22;
        public Object ATTRIBUTE23;
        public Object ATTRIBUTE24;
        public Object ATTRIBUTE25;
        public Object ATTRIBUTE26;
        public Object ATTRIBUTE27;
        public Object ATTRIBUTE28;
        public Object ATTRIBUTE29;
        public Object ATTRIBUTE30;
        public Object ATTRIBUTE31;
        public Object ATTRIBUTE32;
        public Object ATTRIBUTE33;
        public Object ATTRIBUTE34;
        public Object ATTRIBUTE35;
        public Object ATTRIBUTE36;
        public Object ATTRIBUTE37;
        public Object ATTRIBUTE38;
        public Object ATTRIBUTE39;
        public Object ATTRIBUTE40;
    }
    
    public class X_RESPONSE_MESSAGE {
        public List<X_RESPONSE_MESSAGE_ITEM> X_RESPONSE_MESSAGE_ITEM;
    }
    
    
    public static Damac_paymentTermsJSON parse(String json) {
        return (Damac_paymentTermsJSON) System.JSON.deserialize(json, Damac_paymentTermsJSON.class);
    }
}