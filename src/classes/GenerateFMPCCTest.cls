/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GenerateFMPCCTest {

    @istest static void TestMEthod1(){
       
        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();

        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);        
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);      
        
        Case Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = BU.Id ;
        Cas.Type = 'NOCVisa';
        insert Cas;
        System.assert(Cas != null);
        
        ApexPages.currentPage().getParameters().put('id',Cas.Id);
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,FMPCC1.fmPccResponse_element>();
        FMPCC1.fmPccResponse_element response1 = new FMPCC1.fmPccResponse_element();
        response1.return_x =    '{'+
        ' "data" : ['+
        ' {'+
        '   "P_PARAM_ID" : "123",'+
        '   "P_PROC_STATUS" : "S",'+
        '   "P_PROC_MESSAGE" : "Message",'+
        '   "URL" : "www.google.com",'+
        '   "P_ATTRIBUTE2" : "123",'+
        '   "P_ATTRIBUTE3" : "321"'+
        '   '+
        '   '+
        '    '+
        ' }'+
        ' ],'+
        '  '+
        '  "message":"Message",'+
        '  "status":"S"'+
        '  '+
        '}';
       
       
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
 
        PageReference pageRef = Page.GenerateFMPCC;
        pageRef.getParameters().put('id', String.valueOf(Cas.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(Cas);
        GenerateFMPCC controller = new GenerateFMPCC(sc);  
        System.debug('---Hi--44--'); 
        pageRef = controller.init();   
        Test.stopTest();
    }
}