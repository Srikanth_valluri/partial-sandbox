@isTest
public class API_Account_Test {

    @isTest 
    static void doGetAccounts_Test() {
        RestRequest req = new RestRequest(); 
        
        req.requestURI = '/api_account';  
        req.httpMethod = 'GET';
        
        req.params.put('offset', '0');
        req.params.put('limit', '10');
        
        RestContext.request = req;
        RestContext.response = new RestResponse();
        
        Test.startTest();
        
        API_Account.doGet();
        
        Test.stopTest();
    }
}