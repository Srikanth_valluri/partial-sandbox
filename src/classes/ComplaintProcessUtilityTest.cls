/***********************************************************************
* Description - Test class developed for ComplaintProcessUtility
*
* Version            Date            Author            Description
* 1.0                12/12/17        Naresh (accely)           Initial Draft
**********************************************************************/
@isTest(SeeAllData=false)
private class ComplaintProcessUtilityTest{

    @testsetup
    public static void TestData(){
        
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Complaint').getRecordTypeId();
        
        Account acc = TestDataFactory_CRM.createPersonAccount();
        insert acc;
        System.assert(acc != null); 
      
        Case newCase = new Case(); 
        newCase.AccountId = acc.Id ;
        newCase.RecordTypeId  = devRecordTypeId;
        newCase.Credit_Note_Amount__c  = 5000;
        insert newCase; 
    }
     
       // Test Method: logError & getRecordTypeId
    public static testmethod void Test_logError(){
        test.StartTest();
        Case c = [select Id from Case where Credit_Note_Amount__c  = 5000];
        ComplaintProcessUtility.getRecordTypeId();
        test.StopTest();
    }
    
     // Test Method: sendSMSNotification
    public static testmethod void Test_sendSMSNotification(){
         string caseid = [select Id from Case where Credit_Note_Amount__c  = 5000].Id;
         test.StartTest();
         Test.setMock(HttpCalloutMock.class, new MockHttpAgentOTPController());  
         ComplaintProcessUtility.sendSMSNotification('Damac', '9856789542');
         test.StopTest();
    }

    public static testmethod void Test_sendSMSNotification1(){
        string caseid = [select Id from Case where Credit_Note_Amount__c  = 5000].Id;
        test.StartTest();
        //Test.setMock(HttpCalloutMock.class, new MockHttpAgentOTPController());  
        try{
            ComplaintProcessUtility.sendSMSNotification('Damac', '9856789542');
        } catch(Exception e) {

        }
        test.StopTest();
        
    }
 }