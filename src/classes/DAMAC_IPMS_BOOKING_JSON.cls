/**************************************************************************************************
 * @Name              : DAMAC_IPMS_BOOKING_JSON
 * @Test Class Name   : DAMAC_IPMS_PARTY_CREATION_TEST
 * @Description       : JSON Class for DAMAC_IPMS_BOOKING_JSON
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0                        14/08/2019       Created
 * 1.1         QBurst         03/02/2020       Modifications for DP Deals  [SOAP to REST]  
 * 1.2         DAMAC          08/04/2020       Added Space Type 
**************************************************************************************************/

public class DAMAC_IPMS_BOOKING_JSON{
    public String extRequestNumber; //SR-21072019    
    public cls_registrationLines[] registrationLines;
    public class cls_registrationLines {
        public String bookingReference; //B-GOOD
        public string subRequestName;
        public String bookingLocation;  //DP
        public String bookingType;  //NW
        public String registrationDate; //14-JUL-2019
        public String partyId;  //2531779
        public String unitCode; //HWT/PT118/XL2259B
        public String paymentHeaderId;  //173699
        public String heardOfDamac; //Gulf News
        public String bookingSource;    //PORTAL
        public String salesOffice;  //PARK-TOWER
        public String communicationMode;    //D
        public String discountPercent;  //20.5
        public String conditionalDiscountPercent;   //2
        public String freeParking;  //1
        public String paidParking;  //0
        public String externalParking;  //0
        public Integer storeRoom;   //0
        public String financeFlag;  //Y
        public Integer leaseRent;   //200
        public String regularOrBulk;    //R
        public String specialPriceSale; //N
        public String bookingRemarks;   //Ensure that the booking is successful within 40 Seconds
        public String spaTemplate;  //Type 1R
        public cls_salesTeam[] salesTeam;
        public cls_jointBuyers[] jointBuyers;
        public cls_offers[] offers;
        public cls_solicitors[] solicitors;
        public string registrationId;//1.1
        public string spaceType; //1.2
    }

    public class cls_salesTeam {
        public String roleCode; //PC
        public String roleOwnerId;  //32057
        public string roleOwnerName;
    }

    public class cls_jointBuyers {
        public String jbPartyId;    //1752251
        public String sharePercentage;  //50
    }

    public class cls_offers {
        public String offerId;
        public String offerCode;    //O
        //public String offerText;    //One Additional Parking
        //public String offerValue;   //50000.00
        //public String adjustInSalePrice;    //N
        //public String costToDamac;  //100
    }

    public class cls_solicitors {
        public String sellerSolicitorId;    //100000
        public String buyerSolicitorName;   //Name of Solicitor
        public String buyerSolicitorFirm;   //Firm Name
        public String buyerSolicitorAddress;    //Address
        public String buyerSolicitorDxNo;   //DX Number
        public String buyerSolicitorEmail;  //sol.1@gmail.com
        public String buyerSolicitorPhone;  //12345678
    }

    public static DAMAC_IPMS_BOOKING_JSON parse(String json){
        return (DAMAC_IPMS_BOOKING_JSON) System.JSON.deserialize(json, DAMAC_IPMS_BOOKING_JSON.class);
    }
}