/**
    31/12/2019      Aishwarya Todkar    Added check for Overdue Rebate/Discount SR to not to 
                                        close Case as requested by Aditya
 */
public with sharing class PenaltyWaiverService { 
    
    public static WrapperBookingUnit getBookingUnitDetails( Booking_Unit__c objUnit ) {
        WrapperBookingUnit objUnitWrap = new WrapperBookingUnit();
        if(objUnit != null){
            objUnitWrap.objBookingUnit = objUnit;
            objUnitWrap.objIPMSDetailsWrapper = UnitDetailsService.getBookingUnitDetails( objUnit.Registration_ID__c );
        }
        return objUnitWrap ;
    }
    
    public static UploadMultipleDocController.data uploadDocumentsOnCentralRepo( list<UploadMultipleDocController.MultipleDocRequest> lstWrapper ) {
        
        UploadMultipleDocController.data objResWrap =  UploadMultipleDocController.getMultipleDocUrl( lstWrapper );
        system.debug(' == objResWrap == '+ objResWrap);
        return objResWrap ;
        //return '';    
    }
    
    public static UploadMultipleDocController.data uploadDocumentsOnCentralRepo( list<UploadMultipleDocController.MultipleDocRequest> lstWrapper, String str) {
        
        UploadMultipleDocController.data objResWrap =  UploadMultipleDocController.getMultipleDocUrl( lstWrapper, str);
        system.debug(' == FM objResWrap == '+ objResWrap);
        return objResWrap ;
        //return '';    
    }
    
    public static PenaltyWaiverService.SopResponseObject getSOPDocument( String strRegId ) {
        GenerateSOPService.PenaltyWaiverHttpSoap11Endpoint objSOPService = new GenerateSOPService.PenaltyWaiverHttpSoap11Endpoint() ;
        objSOPService.timeout_x = 120000;
        String strResponse = objSOPService.GeneratePenaltyStatment( '2-'+String.valueOf( strRegId + system.now() ) , 
                                                    'GET_PENALTY_STATEMENT' ,
                                                    'SFDC' ,
                                                    strRegId ,
                                                    '',
                                                    String.valueOf( strRegId + system.now() ) );
        system.debug( ' == Statment of Penalty Response =='+strResponse );
        SopResponseObject objResponse = (SopResponseObject)JSON.deserialize(strResponse, PenaltyWaiverService.SopResponseObject.class);
        system.debug( objResponse  );
        return objResponse ;
    }
    
    
    
    public static PenaltyWaiverService.ApprovingAuthoritiesWrapper getApprovingAuthorities( Booking_Unit__c objBookingUnit , 
                                                                                            String strCumulativePenaltyWaiverUnit,
                                                                                            String decPercent,
                                                                                            UnitDetailsService.BookinUnitDetailsWrapper objWrap,
                                                                                            String amountRequested ) {
        ApprovingAuthoritiesWrapper objResponse ;
        actionComUpdated.PenaltyWaiverHttpSoap11Endpoint objSOPService = new actionComUpdated.PenaltyWaiverHttpSoap11Endpoint() ;
        objSOPService.timeout_x = 120000;
        String strResponse = objSOPService.PenaltyWaiverDetails( String.valueOf( objBookingUnit.Registration_Id__c )  , 
                                                    'Penalty Waiver' ,
                                                    'Normal' ,
                                                    objWrap.strProjectCity ,
                                                    '',
                                                    objWrap.strProject,
                                                    objWrap.strBuildingCode,
                                                    objWrap.strBedroomType_UnitType,
                                                    objWrap.strUnitType,
                                                    objWrap.strPermittedUse,
                                                    objWrap.strProject,
                                                    objWrap.strReady_Offplan,
                                                    String.valueOf( objBookingUnit.Booking__r.Account__r.Nationality__c ),
                                                    objWrap.strCustomerClassification ,
                                                    objWrap.strAgreementStatus,
                                                    strCumulativePenaltyWaiverUnit,
                                                    decPercent ,
                                                    amountRequested,
                                                    '',
                                                    '',
                                                    '',
                                                    '',
                                                    double.valueOf('0'),
                                                    double.valueOf('0'),
                                                    double.valueOf('0'),
                                                    double.valueOf('0') ); 
        system.debug('== Approving Authorities from rule engine =='+strResponse ); 
        if( String.isNotBlank( strResponse ) ) {
            objResponse = ( ApprovingAuthoritiesWrapper )JSON.deserialize(strResponse, PenaltyWaiverService.ApprovingAuthoritiesWrapper.class);
        }
        return objResponse ;
    }
    
    public static PenaltyWaiverService.CRFOuterWrap getCRFDocument( Booking_Unit__c objUnit, String processName ) {
        processXxdcProcessServiceWsPlsqlSo.APPSXXDC_PROCESS_SERX1794747X1X5 objInnerWrap = new processXxdcProcessServiceWsPlsqlSo.APPSXXDC_PROCESS_SERX1794747X1X5();
        objInnerWrap.ATTRIBUTE3 = processName;
        objInnerWrap.ATTRIBUTE5 = ''; //objUnit.Property_Name_Inventory__c ;
        objInnerWrap.PARAM_ID = objUnit.Registration_ID__c ;
        
        GenerateCRFService.CRFHttpSoap11Endpoint objCallOut = new GenerateCRFService.CRFHttpSoap11Endpoint();
        objCallOut.timeout_x = 120000;
        String strResponse = objCallOut.GetCustomerRequestForm( '2-'+String.valueOf( system.now() ), 'GENERATE_CRF', 'SFDC', new list<processXxdcProcessServiceWsPlsqlSo.APPSXXDC_PROCESS_SERX1794747X1X5> { objInnerWrap } );
        system.debug('== Customer Request Form Response =='+strResponse );
        if( String.isNotBlank( strResponse ) ) {
            CRFOuterWrap objResponse = ( CRFOuterWrap )JSON.deserialize( strResponse.remove(']').remove('['), PenaltyWaiverService.CRFOuterWrap.class);
            return objResponse ; 
        }
        /*Map<String, Object> objWrap1 = (Map<String, Object>)JSON.deserializeUntyped(response) ;
        return String.valueOf( objWrap1.get('data') ).subString( String.valueOf( objWrap1.get('data') ).indexof('ATTRIBUTE1='),String.valueOf( objWrap1.get('data') ).indexof(',') ).remove('ATTRIBUTE1=') ;*/
        return null ;  
    }
    
     @InvocableMethod
    public static void updatePenaltyDetalsInIPMS( list<Case> objCase ) {
        system.debug(' == updatePenaltyDetalsInIPMS method called ==');
        system.debug(' == lstobjCase  =='+objCase );
        set<Id> setCaseId = new set<Id>();
        for( Case objCase1 : objCase  ) {
           setCaseId.add( objCase1.Id );
        }
        if( !setCaseId.isEmpty() ) {
            updateDetails( setCaseId );
        }
    }
    
    @future(callout=true)
    public static void updateDetails( set<Id> setCaseId ) {
        
        list<Case> lstCase = PenaltyWaiverUtility.getCaseMap( setCaseId ).values() ;
        
        if( lstCase != null && lstCase.size() > 0  )
            updateDetailsInIPMS( lstCase );
    }
    
    public static void updateDetailsInIPMS( list<Case> lstCase ) {
        system.debug('=== updateDetailsInIPMS start ===');
        if( lstCase != null && lstCase.size() > 0  ) {
            list<Task> lstTaskToBeInserted = new list<Task>();
            list<Case> lstCasesToBeUpdated = new list<Case>();
            list<Error_Log__c> lstLogsGenerated = new list<Error_Log__c>();
            
            for( Case objCase : lstCase ) {
                if( PenaltyWaiverHelper.isPenaltyChargedLessEqual( objCase.Booking_Unit__r.Registration_ID__c, objCase.Approved_Amount__c ) ) { 
                    PenaltyWaiverDetails.APPSXXDC_PROCESS_SERX1794747X1X5 objInnerWrap = new PenaltyWaiverDetails.APPSXXDC_PROCESS_SERX1794747X1X5();
                    objInnerWrap.ATTRIBUTE1 = String.valueOf( objCase.Approved_Amount__c );
                    objInnerWrap.PARAM_ID = objCase.Booking_Unit__r.Registration_ID__c ;
                    actionComUpdated.PenaltyWaiverHttpSoap11Endpoint objCallout = new actionComUpdated.PenaltyWaiverHttpSoap11Endpoint();
                    objCallOut.timeout_x = 120000; 
                    String response = objCallout.UpdatePenaltyWaived( '2- ' +String.valueOf( system.now() ), 'WAIVE_PENALTY', 'SFDC', new list<PenaltyWaiverDetails.APPSXXDC_PROCESS_SERX1794747X1X5> { objInnerWrap } );
                    system.debug( 'Updating Penalty Details ==' +response );
                    if( String.isNotBlank( response ) ) {
                        /*map<String, Object> objResponse = ( map<String, Object>)JSON.deserializeUntyped( response );
                        if( String.isNotBlank( String.valueOf( objResponse.get('status') ) ) && String.valueOf( objResponse.get('status') ).equalsIgnoreCase('S') ) {
                            objCase.IPMS_Updated__c = true ; 
                            objCase.Status = 'Closed' ;
                            lstCasesToBeUpdated.add( objCase );
                            //Send notification to finance
                        }*/
                        outerWrap objResp = ( outerWrap )JSON.deserialize(response.remove(']').remove('['), PenaltyWaiverService.outerWrap.class);
                        if( String.isNotBlank( objResp.status ) &&  objResp.status.equalsIgnoreCase('S') &&
                            objResp.data != null && String.isNotBlank( objResp.data.PROC_STATUS ) && objResp.data.PROC_STATUS.equalsIgnoreCase('S') ) {
                            System.debug('RecordType == ' + objCase.RecordType.Name);
                            if( !objCase.RecordType.Name.equalsIgnoreCase( 'Overdue Rebate/Discount' ))
                                objCase.Status = 'Closed' ;

                                objCase.IPMS_Updated__c = true ; 
                                lstCasesToBeUpdated.add( objCase );
                                System.debug('Updating case status to close====');
                        }
                        else {
                            Task objTask = TaskUtility.getTask( ( SObject )objCase, 'Updating waiver details in IPMS failed. Close this task and click on Resubmit button', 'CRE', 
                                                                 objCase.RecordType.Name, system.today().addDays(1) );
                            objTask.Notes__c = String.valueOf( 'MQ -- ' + objResp.message + ' \nFROM IPMS -- '+objResp.data.PROC_MESSAGE );
                            lstTaskToBeInserted.add( objTask );
                            
                            //PenaltyWaiverControllerV2 objControl = new PenaltyWaiverControllerV2();
                            //Error_Log__c objLog = objControl.createErrorLogRecord( objCase.AccountId, objCase.Booking_Unit__c, objCase.Id );
                            Error_Log__c objErr = new Error_Log__c();
                            objErr.Account__c = objCase.AccountId;
                            objErr.Booking_Unit__c = objCase.Booking_Unit__c;
                            objErr.Case__c = objCase.Id;
                            objErr.Process_Name__c = 'Penalty Waiver';
                            objErr.Error_Details__c = 'FROM MQ -- ' +objResp.message + ' \nFROM IPMS -- '+objResp.data.PROC_MESSAGE;
                            lstLogsGenerated.add( objErr );
                        }
                    }
                }
                else {
                    Task objTask = TaskUtility.getTask( ( SObject )objCase, 'Discrepancy between details in IPMS and Salesforce', 'CRE', 
                    objCase.RecordType.Name, system.today().addDays(1));
                    lstTaskToBeInserted.add( objTask );
                    if( !objCase.RecordType.Name.equalsIgnoreCase( 'Overdue Rebate/Discount' ))
                        objCase.Status = 'Closed';
                    lstCasesToBeUpdated.add( objCase );
                }
            }
            if( !lstTaskToBeInserted.isEmpty() ) {
                insert lstTaskToBeInserted ;
            }
            if( !lstCasesToBeUpdated.isEmpty() ) {
                update lstCasesToBeUpdated ;
            }
            if( !lstLogsGenerated.isEmpty() ) {
                insert lstLogsGenerated ;
            }
        }
                system.debug('=== updateDetailsInIPMS End===');
    }
    
    
    
    public class SopResponseObject {
        public String Status ;
        public String PROC_STATUS ;
        public String url { get; set; }
        public String PROC_MESSAGE ;
        public String Message ;
        public String PARAM_ID ;
        public String REQUEST_ID ;
        public String STAGE_ID ;
    } 
    
    public class ApprovingAuthoritiesWrapper {
        public String allowed ;
        public String message ;
        public String recommendingAuthorityOne ;
        public String recommendingAuthorityTwo ;
        public String recommendingAuthorityThree ;
        public String recommendingAuthorityFour ;
        public String approvingAuthorityOne ;
        public String approvingAuthorityTwo ;
        public String approvingAuthorityThree ;
        public String adminFeePsf ;
        public String adminFeeFlat ;
        public String adminFeePercentage ; 
    }
    
    public class CRFDocumentWrapper {
        public String PROC_STATUS ;
        public String PROC_MESSAGE ;
        public String ATTRIBUTE3 ;
        public String ATTRIBUTE2;
        public String ATTRIBUTE1;
        public String message;
        public String status;
    }
    
    public class CRFInnerWrap {
        public String PROC_STATUS ;
        public String PROC_MESSAGE ;
        public String ATTRIBUTE1 ;
        public String ATTRIBUTE2 ;
        public String ATTRIBUTE3 ;
    }
    
    public class CRFOuterWrap {
        public CRFInnerWrap data ;
        public String message ;
        public String status ;   
    }
    
    public class innerWrap {
        public String PROC_STATUS ;
        public String PROC_MESSAGE ;
        public String PARAM_ID ;
        public String Transaction_Number ;
    }
    
    public class outerWrap {
        public innerWrap data ;
        public String message ;
        public String status ;      
    }
}