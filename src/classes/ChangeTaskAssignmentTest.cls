/**
 * Ver       Date            Author                 Modification
 * 1.0    10/3/2019         Arsh Dave           Initial Version
**/
@isTest
public class ChangeTaskAssignmentTest{

    @testSetup static void setup(){
        
        UserRole objRole = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert objRole;

        User objUserManager = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'Muser000@amamama.com',
            Username = 'Muser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias_M',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = objRole.Id
        );
        insert objUserManager;

        User objUser = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = objRole.Id,
            ManagerId = objUserManager.Id
        );
        insert objUser;
    }

    static testMethod  void callConstructor() {

        User objUser = [SELECT Id FROM User WHERE Email = 'puser000@amamama.com'];

        Account objAccount = TestDataFactoryFM.createAccount();
        objAccount.Email__c = 'test@test.com'; 
        objAccount.OwnerId = objUser.Id;
        insert objAccount;

        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAccount);
        insert objSR;

        Booking__c objBooking = TestDataFactoryFM.createBooking(objAccount, objSR);
        insert objBooking;
        
        Booking_Unit__c objBooking_Unit = TestDataFactoryFM.createBookingUnit(objAccount, objBooking);
        insert objBooking_Unit;

        FM_Case__c objFMCase = new FM_Case__c(
            Account__c = objAccount.Id,
            Category_GE__c = 'Change of Contact Details',
            Subcategory_GE__c = 'Change registered email id',
            RecordTypeId = System.Label.FM_Case_General_Enquiry
        );
        insert objFMCase;

        List<Task> objTaskTest = [
            SELECT Id
                , Status
                , ActivityDate
                , WhatId
                , OwnerId
                , CreatedDate
                , Subject
             FROM Task
            WHERE What.Id = :objFMCase.Id   
        ];
        
        
        Test.startTest();
            // strPageID = ApexPages.currentPage().getParameters().put('id', objTaskTest.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(objTaskTest[0]);
            ChangeTaskAssignment objChangeTaskAssignment = new ChangeTaskAssignment(sc);
            
            PageReference pageRef = Page.ChangeTaskAssignmentPage;
            pageRef.getParameters().put('id', String.valueOf(objTaskTest[0].Id));
            Test.setCurrentPage(pageRef);
            objChangeTaskAssignment.createChangeTaskAssignment();
            PageReference pageRef_1 = objChangeTaskAssignment.returnToAccount();
        Test.stopTest();
    }
}