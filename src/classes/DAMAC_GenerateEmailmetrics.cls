global class DAMAC_GenerateEmailmetrics implements Database.Batchable<sObject>, Database.Stateful {

    ID emailReqId = NULL;
    String query = NULL;
    
    global DAMAC_GenerateEmailmetrics (ID emailRequestID, String requestQuery) {
        emailReqId = emailRequestID;
        query = requestQuery;
    }
    public static string getAllfields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        if (objectType == null) {
            return fields;
        }
        for (string f :objectType.getDescribe ().fields.getMap ().keySet ())
            fields += f+ ', ';
        return fields.removeEnd(', ');         
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        if (query != '' && query != NULL) {
            query += ' Order BY CreatedDate DESC '; 
        }
        else {
            String fields = getAllfields('Email_Request__c');
            string queryCondition = 'SELECT '+fields+' FROM Email_Request__c'
                                +' WHERE id= \''+emailReqId+'\' LIMIT 1';                            
                                
            Email_Request__c assignmentRule = new Email_Request__c ();
            assignmentRule = Database.query (queryCondition);
            assignmentRule.job_Status__c = 'Started';
            assignmentRule.Job_Started_Date__c = DateTime.Now ();
            update assignmentRule;
            
            fields = getAllfields('Email_Request_Rule_Criteria__c');
            List <Email_Request_Rule_Criteria__c> childRules = new List <Email_Request_Rule_Criteria__c> ();
            childRules = Database.query ('SELECT '+fields+' FROM Email_Request_Rule_Criteria__c '
                                +' WHERE Email_Request__c = \''+emailReqId+'\' Order by Order__c Asc');
                                
            String userLogic = assignmentRule.filter_logic__c;
            query = 'SELECT id FROM '+assignmentRule.Related_Object_API__c+' WHERE ';
            String output;
            Map <String, Email_Request_Rule_Criteria__c> filterMap = new Map <String, Email_Request_Rule_Criteria__c> ();
            for (Email_Request_Rule_Criteria__c con: childRules){
               filterMap.put (String.valueOf (con.order__c), con);
            }
            System.Debug (filterMap);
            
            if (String.isNotBlank (userLogic)) {
                query += +' ( '+ formatQuery (userLogic, filterMap)+' ) Order BY CreatedDate DESC ';            
                system.debug('>>>FINAL QUERY'+query);
                if(assignmentRule.query_Limit__c != null){
                    query += ' LIMIT '+assignmentRule.query_Limit__c;
                }
            }
        }
        return Database.getQueryLocator(query);    
    }
     public String formatQuery (String queryLogic, Map <String, Email_Request_Rule_Criteria__c> rules) {
        String query = '';
        for (String key : queryLogic.split (' ')) {
            if (key.isNumeric ()) {
                System.Debug (key );
                query += +' '+rules.get (key).Query_Condition__c+' ';
            } else
                query += key;
        }
        System.debug (query);
        return query;
    }
    global void execute (Database.BatchableContext BC, List <sObject> scope) {
        Email_Request__c request = new Email_request__c ();
        request = [SELECT Email_Field_API__c, job_Status__c FROM Email_request__c WHERE ID =: emailReqId ];
        
        request.ID = emailReqID;
        request.job_Status__c = 'Procesing';
        update request;
        
        List <Email_Metrics__c> metricsList = new List <Email_Metrics__c> ();
        Set <ID> inqIds = new Set <ID> ();
        for (Sobject inq :scope) {
            ID inqId = inq.Id;
            String objectAPI = inqId.getSObjectType().getDescribe().getName();
            if (objectAPI == 'Inquiry__c') {
                inqIds.add (inqId);
            }
        }    
        Map <ID, Inquiry__c> inquiries = new Map <ID, Inquiry__c> ();
        String fieldAPI = '';
        if (inqIds.size () > 0) {
            fieldAPI = request.Email_Field_API__c;
            if (fieldAPI == null)
                fieldAPI = 'Email__c';
            for (Inquiry__c inquiry : Database.query ('SELECT '+fieldAPI+' FROM Inquiry__c WHERE ID IN :inqIds')) {
                inquiries.put (inquiry.Id, inquiry);
            }
        }
        
        List <String> emailsAdded = new List <String> ();
        for (Email_metrics__c metrics :[SELECT Email__c FROM Email_metrics__c WHERE Email_Request__c =: emailReqId AND Email__c != NULL]) {
            emailsAdded.add (metrics.Email__c);
        }
        for (sObject inq: scope) {
            String email = '';
            Email_Metrics__c metrics = new Email_Metrics__c ();
            metrics.Email_Request__c = emailReqId;
            metrics.SObject_Id__c = inq.ID;
            if (inquiries.containsKey (inq.Id)) {
                metrics.Email__c = ''+inquiries.get (inq.Id).get (fieldAPI);
                email = ''+inquiries.get (inq.Id).get (fieldAPI);
            }
            if (email != '') {
                if (emailsAdded.contains (email)) {
                    metrics.Duplicate__c = true;
                }
            }
            metricsList.add (metrics);
            
        }
        Damac_Constants.skip_EmailMetricsTrigger = TRUE;
        insert metricsList;
        List <ID> metricsIds = new List <ID> ();
        for (Email_metrics__c metrics: metricsList)
            metricsIds.add (metrics.id);
        System.enqueueJob(new Damac_AsyncSendEmail (metricsIds));
    }
    global void finish (Database.BatchableContext BC) {
        Email_Request__c request = new Email_request__c ();
        request.ID = emailReqID;
        request.job_Status__c = 'Completed';
        update request;
    }
    
    
}