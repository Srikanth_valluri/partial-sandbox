@isTest
public class InquiryConvertToAgentControllerTest 
{
	 @isTest static void serviceRequestPageTest()
     {
         Id agenTeamRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Agent Team').getRecordTypeId();
         Inquiry__c inqObj= new Inquiry__c(RecordTypeId=agenTeamRT,Inquiry_Source__c='Agent Referral',Mobile_Phone_Encrypt__c='456123',Mobile_CountryCode__c='American Samoa: 001684',Mobile_Phone__c='1234',Email__c='mk@gmail.com',First_Name__c='Test',Last_Name__c='Last',CR_Number__c='0987',ORN_Number__c='7842',Agency_Type__c='Corporate',Organisation_Name__c = 'Oliver',isDuplicate__c=false);
    	 insert inqObj;
         
         Apexpages.StandardController stdController = new Apexpages.StandardController(inqObj); 
         InquiryConvertToAgentController inqConvertObj=new InquiryConvertToAgentController(stdController);
         inqConvertObj.show=true; 
         inqConvertObj.myMsg = ' Inquiry cannot be converted to Agent, if Inquiry Status is not \'Potential Agent\' ';
         Test.startTest();
         inqConvertObj.inquiryObj =inqObj ;
         PageReference pageRef = Page.InquiryConvertToAgent;         
         inqConvertObj.inquiryId=inqObj.id;
         ApexPages.StandardController sc = new ApexPages.standardController(inqObj);
   	     InquiryConvertToAgentController  controller = new  InquiryConvertToAgentController(sc);
         controller.serviceRequestPage();
         pageRef.getParameters().put('id',inqObj.id);
         
         //controller.serviceRequestPage();
         Test.stopTest();
     }
     @isTest static void serviceRecordTest()
     {
         Id agenTeamRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Agent Team').getRecordTypeId();
         Inquiry__c inqObj= new Inquiry__c(RecordTypeId=agenTeamRT,Inquiry_Source__c='Agent Referral',Mobile_Phone_Encrypt__c='456123',Mobile_CountryCode__c='American Samoa: 001684',Mobile_Phone__c='1234',Email__c='mk@gmail.com',First_Name__c='Test',Last_Name__c='Last',CR_Number__c='0987',ORN_Number__c='7842',Agency_Type__c='Corporate',Organisation_Name__c = 'Oliver',isDuplicate__c=false);
    	 insert inqObj;
         NSIBPM__Service_Request__c srRecord = new NSIBPM__Service_Request__c(Account_Number__c='1234',Inquiry__c =inqObj.id);
         insert srRecord;
         Apexpages.StandardController stdController = new Apexpages.StandardController(inqObj); 
         InquiryConvertToAgentController inqConvertObj=new InquiryConvertToAgentController(stdController);
         inqConvertObj.show=true; 
         inqConvertObj.myMsg = ' Inquiry cannot be converted to Agent, if Inquiry Status is not \'Potential Agent\' ';
         Test.startTest();
         inqConvertObj.inquiryObj =inqObj ;
         PageReference pageRef = Page.InquiryConvertToAgent;         
         inqConvertObj.inquiryId=inqObj.id;
         ApexPages.StandardController sc = new ApexPages.standardController(inqObj);
   	     InquiryConvertToAgentController  controller = new  InquiryConvertToAgentController(sc);
         controller.serviceRequestPage();
         pageRef.getParameters().put('id',inqObj.id);
         
         //controller.serviceRequestPage();
         Test.stopTest();
         
         
     }
    
}