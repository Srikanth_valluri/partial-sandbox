// 11 Sep 2018 - Added record type checks
public without sharing class SendSmsService {
    
    @future (callout=true)
    public static void Sendtextmessage( list<String> lstPhoneNumbers
                                      , String MsgContent
                                      , String callingId ) {
        
        System.debug('callingId:::::::::::^&%^&%$&^'+callingId);
        
        Calling_List__c callObj =  [SELECT Id,
                                            Name,
                                            Account__c,
                                            Account__r.Id,
                                            Customer_Name__c,
                                            RecordType.Name
                                        FROM Calling_List__c
                                        WHERE Id = :Id.valueOf(callingId) LIMIT 1];
        
        System.debug('callObj:::&&&&&'+callObj);
        Id profileId = userinfo.getProfileId();
        String profileName = [Select Id,Name from Profile where Id=:profileId].Name;
        System.debug('profileName == ' + profileName);
        List<SMS_History__c>smsLst = new List<SMS_History__c>();
        
        if(lstPhoneNumbers != null && !lstPhoneNumbers.isEmpty()) {
            for( String PhoneNumber : lstPhoneNumbers ){
                if(PhoneNumber != null) {
                    Id caseId = null;
                    if(PhoneNumber.contains(':')){
                       list<String> lstValues = PhoneNumber.split(':');
                       PhoneNumber = lstValues[1];
                       caseId = lstValues[0];
                    }
                    if(PhoneNumber.startsWith('00')) {
                        PhoneNumber = PhoneNumber.removeStart('00');
                    }
                    else if(PhoneNumber.startsWith('0')) {
                        PhoneNumber = PhoneNumber.removeStart('0');
                    }
                    HttpRequest req = new HttpRequest();
                    HttpResponse res = new HttpResponse();
                    Http http = new Http();
                    String user = '';
                    String passwd = '';
                    String strSID = '';
                    
                    System.debug('callObj.RecordType.Name:::&&&&&'+callObj.RecordType.Name);

                    if( callObj.RecordType.Name == 'FM Collections' ) {
                        user = Label.FM_SMS_Service_Username; //'loams.';
                        passwd =  Label.FM_SMS_Service_Password; //'1@#$%qwert';
                        //strSID =  Label.FM_SMS_Service_SenderId; //'LOAMS';
                        strSID = SMSClass.getSenderName(user, PhoneNumber, false);
                    }else if (callObj.RecordType.Name == 'Appointment Scheduling' 
                    || ( callObj.RecordType.Name == 'Collections Calling List' && !profileName.contains('Collection')) ) {
                        user = Label.Damac_CRM_SMS_Service_User_name;//'damaccrm';
                        passwd = Label.Damac_CRM_SMS_Service_Password;//'D@$al3sF0rc387!';
                        //strSID = Label.Damac_CRM_SMS_Service_Sender_Id;//'DAMAC GRP'; 
                        strSID = SMSClass.getSenderName(user, PhoneNumber, false);
                    }
                    else {
                        user = Label.SF_SMS_Service_User_name;  //'salesforce';
                        passwd = Label.SF_SMS_Service_Password; //'D@$al3sF0rc387!';
                        //strSID = Label.SF_SMS_Service_Sender_Id; //'DAMAC GRP';
                        strSID = SMSClass.getSenderName(user, PhoneNumber, false);
                    }
                    System.debug('user.Name:::&&&&&'+user);
                    System.debug('passwd.Name:::&&&&&'+passwd);
                    System.debug('strSIDName:::&&&&&'+strSID);
                    req.setMethod('POST' ); // Method Type
                    req.setEndpoint('http://api.smscountry.com/SMSCwebservice_bulk.aspx'); 
                    string msgCont = GenericUtility.encodeChar(MsgContent);
                    
                    System.debug('msgCont:'+msgCont);
                     req.setBody('user='+ user + '&passwd=' + passwd +'&message=' + msgCont + '&mobilenumber=' + PhoneNumber 
                                + '&sid='+strSID+ '&MTYPE=LNG&DR=Y'); // Request Parameters
                    try {
                        system.debug('Request Body---'+req.getBody());
                        system.debug('Request Header---'+req.getHeader('Authorization'));
                        system.debug('Request Header---'+req.getHeader('Content-Type'));
                        res = http.send(req);
                        system.debug('Response---'+res);
                        system.debug('Response body---'+res.getBody());
                        if(res.getBody() != null){
                            // Parse Response
                            //if('SMS message(s) sent' == res.getBody()){
                                
                            if(String.valueOf(res.getBody()).contains('OK:')){
                                 system.debug('Message Send Sucessfully');
                                  SMS_History__c smsObj = new SMS_History__c();
                                  smsObj.Message__c = MsgContent;
                                  smsObj.Phone_Number__c = PhoneNumber;
                                  smsObj.Customer__c = callObj.Account__c;
                                  smsObj.Calling_List__c = callObj.Id;
                                  smsObj.Description__c = res.getBody();
                                  smsObj.Is_SMS_Sent__c = true;
                                  smsObj.sms_Id__c = res.getBody().substringAfter('OK:');
                                  if( callObj.RecordType.Name == 'FM Collections' ) {
                                    smsObj.isFMSMS__c = true;
                                  }else {
                                    smsObj.isFMSMS__c = false;
                                  }       
                                  
                                  System.debug('smsObj::::if:'+smsObj);
                                  smsLst.add(smsObj);
                            }
                            else{
                                //if failure
                                  SMS_History__c smsObj = new SMS_History__c();
                                  smsObj.Message__c = MsgContent;
                                  smsObj.Phone_Number__c = PhoneNumber;
                                  smsObj.Customer__c = callObj.Account__c;
                                  smsObj.Calling_List__c = callObj.Id;
                                  smsObj.Description__c = res.getBody();
                                  if( callObj.RecordType.Name == 'FM Collections' ) {
                                    smsObj.isFMSMS__c = true;
                                  }else {
                                    smsObj.isFMSMS__c = false;
                                  }                       
                                  System.debug('smsObj::::else:'+smsObj);
                                  smsLst.add(smsObj);
                            }
                        }
                    } catch(Exception e) {
                        System.debug('Error***** '+ e);
                    }
                }
            }
            if(!smsLst.isEmpty()){
                try{
                    system.debug('In try ');
                    upsert smsLst;
                    system.debug('after upsert = smsLst'+smsLst);
                }catch(exception ex){
                    System.debug('DML Exception*****: '+ ex);
                }
            }
        }
    }
}