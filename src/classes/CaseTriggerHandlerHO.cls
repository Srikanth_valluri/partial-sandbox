public without sharing class CaseTriggerHandlerHO{
    
    public void afterInsert(map<Id,Case> newMap){
        set<Id> setRecordtypeIds = new set<Id>();
        map<Case,String> mapCase_BookingUnit = new map<Case,String>();
        for (Recordtype objRec : [Select Id
                                         , Name
                                         , DeveloperName
                                         , SobjectType
                                  From Recordtype
                                  Where DeveloperName ='Handover'
                                  and SobjectType = 'Case']){
            setRecordtypeIds.add(objRec.Id);
        }

        for(Case objC : newMap.values()){
            if(setRecordtypeIds.contains(objC.RecordtypeId)){
                if(objC.ParentId != null){
                    String bldgName = objC.Unit_Name__c.split('/')[0];
                    mapCase_BookingUnit.put(objC, bldgName);
                }
            }
        }
        system.debug('mapCase_BookingUnit******'+mapCase_BookingUnit);
        if(!mapCase_BookingUnit.isEmpty()){
            generateChildCaseDocs(mapCase_BookingUnit);
        }
    }

    public void generateChildCaseDocs(map<Case,String> mapCase_BookingUnit){
        system.debug('*****generate child case*****');
        map<String,Location__c> mapBookingUnit = new map<String,Location__c>();
        for(Location__c objL : [Select Id
                                    , Name
                                    , Document_01__c
                                    , Document_02__c
                                    , Document_03__c
                                    , Document_04__c
                              from Location__c
                              where Name IN : mapCase_BookingUnit.values()
                              and RecordType.DeveloperName = 'Building']){
            mapBookingUnit.put(objL.Name, objL);
        }
        if(!mapBookingUnit.isEmpty()){
            list<SR_Attachments__c> lstDocs = new list<SR_Attachments__c>();
            for(Case objC : mapCase_BookingUnit.keySet()){
                String bldgName = objC.Unit_Name__c.split('/')[0];
                if(mapBookingUnit.containsKey(mapCase_BookingUnit.get(objC))){
                    system.debug('*****Inside*****');
                    Location__c objUnit = mapBookingUnit.get(mapCase_BookingUnit.get(objC));
                    if(String.isNotBlank(objUnit.Document_01__c)){
                        SR_Attachments__c objDoc = new SR_Attachments__c();
                        objDoc.Case__c = objC.Id;
                        objDoc.IsRequired__c = true;
                        objDoc.Name = objUnit.Document_01__c;
                        objDoc.Booking_Unit__c = objC.Booking_Unit__c;
                        lstDocs.add(objDoc);
                    }
                    if(String.isNotBlank(objUnit.Document_02__c)){
                        SR_Attachments__c objDoc = new SR_Attachments__c();
                        objDoc.Case__c = objC.Id;
                        objDoc.IsRequired__c = true;
                        objDoc.Name = objUnit.Document_02__c;
                        objDoc.Booking_Unit__c = objC.Booking_Unit__c;
                        lstDocs.add(objDoc);
                    }
                    if(String.isNotBlank(objUnit.Document_03__c)){
                        SR_Attachments__c objDoc = new SR_Attachments__c();
                        objDoc.Case__c = objC.Id;
                        objDoc.IsRequired__c = true;
                        objDoc.Name = objUnit.Document_03__c;
                        objDoc.Booking_Unit__c = objC.Booking_Unit__c;
                        lstDocs.add(objDoc);
                    }
                    if(String.isNotBlank(objUnit.Document_04__c)){
                        SR_Attachments__c objDoc = new SR_Attachments__c();
                        objDoc.Case__c = objC.Id;
                        objDoc.IsRequired__c = true;
                        objDoc.Name = objUnit.Document_04__c;
                        objDoc.Booking_Unit__c = objC.Booking_Unit__c;
                        lstDocs.add(objDoc);
                    }
                    SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
                    objCaseAttachment.Case__c = objC.Id;
                    objCaseAttachment.IsRequired__c = true;
                    objCaseAttachment.Name = 'PCC';
                    objCaseAttachment.Booking_Unit__c = objC.Booking_Unit__c;
                    lstDocs.add(objCaseAttachment);
                    /*
                    SR_Attachments__c objCaseAttachment1 = new SR_Attachments__c();
                    objCaseAttachment1.Case__c = objC.Id;
                    objCaseAttachment1.IsRequired__c = true;
                    objCaseAttachment1.Name = 'FM PCC';
                    objCaseAttachment1.Booking_Unit__c = objC.Booking_Unit__c;
                    lstDocs.add(objCaseAttachment1);
                    */
                }
            }
            if(!lstDocs.isEmpty()){
                insert lstDocs;
            }
        }
    }

    public void afterUpdate(map<Id,Case> newMap, map<Id,Case> oldMap){
        set<Id> setRecordtypeIds = new set<Id>();
        //map<Id,Case> mapTitleDeedCases = new map<Id,Case>();
        set<Id> setBUId = new set<Id>();
        set<Id> setCaseId = new set<Id>();
        for (Recordtype objRec : [Select Id
                                         , Name
                                         , DeveloperName
                                         , SobjectType
                                  From Recordtype
                                  Where DeveloperName like 'Handover%'
                                  and SobjectType = 'Case']){
            setRecordtypeIds.add(objRec.Id);
        }

        for(Case objC : newMap.values()){
            if(setRecordtypeIds.contains(objC.RecordtypeId)){
                if(objC.PRC_Document__c && oldMap.get(objC.Id).PRC_Document__c != objC.PRC_Document__c){
                    setCaseId.add(objC.Id);
                    setBUId.add(objC.Booking_Unit__c);
                    system.debug('!!!!!!!setBUId'+setBUId);
                }
            }
        }
        if(!setCaseId.isEmpty()){
            createTitleDeed(setCaseId, setBUId);
        }
    }
    
    @future(callout=true)
    public static void createTitleDeed(set<Id> setIdCase, set<Id> setBUId) {    
        
        Id recType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Title Deed').getRecordTypeId();
        map<Id,Boolean> mapId_Exists = new map<Id,Boolean>();
        map<Id,Boolean> mapBUidExists = new map<Id,Boolean>();
        map<Id,Case> mapId_Case = new map<Id,Case>();
        UnitDetailsService.BookinUnitDetailsWrapper objUnitDetailsWrapper;
        system.debug('!!!!!setBUId'+setBUId);
        
        for (Case objCase : [Select Id
                                    , ParentId
                                    , Booking_Unit__c
                                    , Registration_ID__c
                                    , AccountId
                                    , CurrencyIsoCode
                                    , OwnerId
                                    , Booking_Unit__r.Finance_Flag__c
                             From Case
                             Where Id IN: setIdCase]) {
            mapId_Case.put(objCase.Id, objCase);           
        }
        
        List<Process_Validations__c> validations = Process_Validations__c.getall().values();
        Boolean checkPCC = true;
        Boolean checkDELC = true;
        Boolean checkMortgage = true;
        for (Process_Validations__c objValidation : validations) {
            if (objValidation.Process_Name__c == 'TitleDeed') {
                if (objValidation.Name == 'PCC Check') {
                    if (objValidation.Allow_Validation__c == false) {
                        checkPCC = false; 
                    }
                }
                if (objValidation.Name == 'DELC Flags') {
                    if (objValidation.Allow_Validation__c == false) {
                        checkDELC = false; 
                    }
                }
                if (objValidation.Name == 'Mortgage Flag') {
                    if (objValidation.Allow_Validation__c == false) {
                        checkMortgage = false; 
                    }
                }
            }
        }
        
        if (checkPCC == true) {
            if (!mapId_Case.isEmpty() && mapId_Case.values() != null) {
                list<Case> lstCase = mapId_Case.values();
                if (lstCase[0].Registration_ID__c != null) {
                    objUnitDetailsWrapper = UnitDetailsService.getBookingUnitDetails(lstCase[0].Registration_ID__c);
                    system.debug('!!!!!objUnitDetailsWrapper.strPCC'+objUnitDetailsWrapper.strPCC);
                }
            }
        } else {
            objUnitDetailsWrapper = new UnitDetailsService.BookinUnitDetailsWrapper();
        }
        
        for(Case objC : [Select Id
                                , ParentId
                                , Booking_Unit__c
                                , Booking_Unit__r.Finance_Flag__c
                        From Case
                        Where RecordTypeId =:recType
                        and (ParentId IN : mapId_Case.keySet() or Booking_Unit__c IN:setBUId) ]){
            mapId_Exists.put(objC.ParentId,true);
            mapBUidExists.put(objC.Booking_Unit__c, true);
            system.debug('!!!!mapId_Exists'+mapId_Exists);
        }
        
        Map<Id, Id> mapParentHoToHo = new Map<Id, Id>();
        for(Case objHOCase: mapId_Case.values()) {
            if(objHOCase.ParentId != null) {
                mapParentHoToHo.put(objHOCase.ParentId, objHOCase.Id);
            }
        }
        
        map<Id,list<SR_Attachments__c>> mapId_DocsTemp = new map<Id,list<SR_Attachments__c>>();
        for(SR_Attachments__c objSR : [Select Id
                                              , Name
                                              , Case__c
                                              , IsValid__c
                                              , Attachment_URL__c
                                       from SR_Attachments__c
                                       where Case__c IN : mapId_Case.keySet() OR Case__c IN: mapParentHoToHo.keySet()]){
            if(!mapId_DocsTemp.containsKey(objSR.Case__c)){
                mapId_DocsTemp.put(objSR.Case__c, new list<SR_Attachments__c>{objSR});
            }else{
                mapId_DocsTemp.get(objSR.Case__c).add(objSR);
            }
        }
        
        map<Id,list<SR_Attachments__c>> mapId_Docs = new map<Id,list<SR_Attachments__c>>();
        for(Id idHOCase: mapId_Case.keySet()) {
            if(mapId_DocsTemp.containsKey(idHOCase)) {
                mapId_Docs.put(idHOCase, mapId_DocsTemp.get(idHOCase));
            }
        }
        
        for(Id idParentHOCase: mapParentHoToHo.keySet()) {
            if(mapId_DocsTemp.containsKey(idParentHOCase)) {
                if(mapId_Docs.containsKey(mapParentHoToHo.get(idParentHOCase))) {
                    List<SR_Attachments__c> lstAtt1 = mapId_DocsTemp.get(idParentHOCase);
                    List<SR_Attachments__c> lstAtt2 = mapId_Docs.get(mapParentHoToHo.get(idParentHOCase));
                    lstAtt1.addAll(lstAtt2);
                    mapId_Docs.put(mapParentHoToHo.get(idParentHOCase), lstAtt1);
                }
                else {
                    mapId_Docs.put(mapParentHoToHo.get(idParentHOCase), mapId_DocsTemp.get(idParentHOCase));
                }
            }
        }
        
        system.debug('*****mapId_Exists*****'+mapId_Exists);
        list<Case> lstTitleDeed = new list<Case>();
        Case TDCase;
        for(Case objC  : mapId_Case.values()){
            if(!mapId_Exists.containsKey(objC.Id) && !mapBUidExists.containsKey(objC.Booking_Unit__c) && 
                (checkMortgage == false || (checkMortgage == true && String.isNotBlank(objC.Booking_Unit__r.Finance_Flag__c) && !objC.Booking_Unit__r.Finance_Flag__c.containsIgnoreCase('M')))) {
                if ( checkPCC == false || (checkPCC == true && String.isNotBlank(objUnitDetailsWrapper.strPCC) && !objUnitDetailsWrapper.strPCC.containsIgnoreCase('N'))
                    ||  (checkDELC == false || (checkDELC == true && String.isNotBlank(objUnitDetailsWrapper.strDispute) && !objUnitDetailsWrapper.strDispute.containsIgnoreCase('Y')) ||
                    (String.isNotBlank(objUnitDetailsWrapper.strEnforcement) && !objUnitDetailsWrapper.strEnforcement.containsIgnoreCase('Y')) ||
                    (String.isNotBlank(objUnitDetailsWrapper.strLitigation) && !objUnitDetailsWrapper.strLitigation.containsIgnoreCase('Y')) ||
                    (String.isNotBlank(objUnitDetailsWrapper.strCounterCase) && !objUnitDetailsWrapper.strCounterCase.containsIgnoreCase('Y')))){
                    system.debug('!!!!checkPCC'+checkPCC);
                    system.debug('!!!!checkDELC'+checkDELC);
                    system.debug('!!!!objUnitDetailsWrapper.strPCC'+objUnitDetailsWrapper);
                    Case objCase = new Case();
                    objCase.RecordTypeId = recType;
                    objCase.ParentId = objC.Id;
                    objCase.AccountId = objC.AccountId;
                    objCase.Booking_Unit__c = objC.Booking_Unit__c;
                    objCase.CurrencyIsoCode = objC.CurrencyIsoCode;
                    objCase.Status = 'Submitted';
                    objCase.OwnerId = objC.OwnerId;
                    lstTitleDeed.add(objCase);
                }
            }
        }
        if(!lstTitleDeed.isEmpty()){
            insert lstTitleDeed;
            TDCase =  lstTitleDeed[0];
            list<SR_Attachments__c> titleDeedDocs = new list<SR_Attachments__c>();
            
            for(Case objC : lstTitleDeed){
                if(mapId_Docs.containsKey(objC.ParentId) && mapId_Docs.get(objC.ParentId) != null){
                    for(SR_Attachments__c objS : mapId_Docs.get(objC.ParentId)){
                        SR_Attachments__c obj = new SR_Attachments__c();
                        obj.Name = objS.Name;
                        obj.Case__c = objC.Id;
                        obj.IsValid__c= objS.IsValid__c;
                        obj.Attachment_URL__c= objS.Attachment_URL__c;
                        titleDeedDocs.add(obj);
                    }
                }
                
                // Whenever the Title Deed Case with Submitted status is created, Task to Finance should also be created
                /*Task objTask = TaskUtility.getTask(
                    (SObject)objC,
                    'Check NoDu n arrange TD Pymnt',
                    'Finance',
                    'Title Deed',
                    System.today().addDays(1)
                    );
                lstTasks.add(objTask);*/
            }
            /*if (!lstTasks.isEmpty()) {
                //insert lstTasks;
            }*/
            if(!titleDeedDocs.isEmpty()){
                insert titleDeedDocs;
                /*Set<Id> setTDId = new Set<Id>();
                for(SR_Attachments__c objSRAtt: titleDeedDocs) {
                    setTDId.add(objSRAtt.Id);
                }
                //uploadDocumentsToCentralRepo(setTDId);
                UploadDocToCentralRepoBatch batchInstance = new UploadDocToCentralRepoBatch(setTDId);
                Database.executeBatch(batchInstance, 1);*/
                System.enqueueJob(new TitleDeedFutureImplementation(TDCase)); 
            }
        }
    }

    /*
    // Method to upload the documents attached to title deed case to central repository
    @future(callout=true)
    private static void uploadDocumentsToCentralRepo(Set<Id> setTDId) {
        Set<Id> setCaseId = new Set<Id>();
        Map<Id, List<SR_Attachments__c>> mapCaseToSRAtt = new Map<Id, List<SR_Attachments__c>>();

        for(SR_Attachments__c objSRAtt: [Select Id, Name, Case__c, Attachment_URL__c From SR_Attachments__c Where Id IN: setTDId]) {
            setCaseId.add(objSRAtt.Case__c);

            if(mapCaseToSRAtt.containsKey(objSRAtt.Case__c))
                mapCaseToSRAtt.get(objSRAtt.Case__c).add(objSRAtt);
            else
                mapCaseToSRAtt.put(objSRAtt.Case__c, new List<SR_Attachments__c>{ objSRAtt });
        }

        Map<Id, Case> mapCase = new Map<Id, Case>([Select Id, CaseNumber, Account.Party_ID__c From Case Where Id IN: setCaseId]);

        List<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocRequest = new  List<UploadMultipleDocController.MultipleDocRequest>();

        for(Id idCase: mapCaseToSRAtt.keySet()) {
            Integer intIncrementor = 1;
            for(SR_Attachments__c objSRAtt: mapCaseToSRAtt.get(idCase)) {
                if(String.isNotBlank(objSRAtt.Attachment_URL__c)) {
                    // Perform http callout to fetch blob content from file url
                    HttpRequest req = new HttpRequest();
                    req.setEndpoint(objSRAtt.Attachment_URL__c);
                    req.setTimeout(120000);
                    req.setMethod('GET');

                    Http http = new Http();
                    HTTPResponse res = http.send(req);
                    system.debug('--res--'+res);
                    Blob pdfContent = res.getBodyAsBlob();

                    UploadMultipleDocController.MultipleDocRequest objDocRequest = new UploadMultipleDocController.MultipleDocRequest();
                    objDocRequest.category =  'Document';
                    objDocRequest.entityName = 'Damac Service Requests';
                    objDocRequest.fileDescription  =  objSRAtt.Name;

                    intIncrementor++;
                    objDocRequest.fileId = mapCase.get(objSRAtt.Case__c).CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+objSRAtt.Name;
                    objDocRequest.fileName = mapCase.get(objSRAtt.Case__c).CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+objSRAtt.Name;

                    system.debug('--mapCase.get(objSRAtt.Case__c).CaseNumber--'+mapCase.get(objSRAtt.Case__c).CaseNumber);
                    objDocRequest.registrationId =  mapCase.get(objSRAtt.Case__c).CaseNumber;

                    objDocRequest.sourceFileName  = 'IPMS-'+mapCase.get(objSRAtt.Case__c).Account.party_ID__C+'-'+objSRAtt.Name;
                    objDocRequest.sourceId  =  'IPMS-'+mapCase.get(objSRAtt.Case__c).Account.party_ID__C+'-'+objSRAtt.Name;

                    objDocRequest.base64Binary =  EncodingUtil.base64Encode(pdfContent);
                    lstMultipleDocRequest.add(objDocRequest);
                }
            }
        }

        if(!lstMultipleDocRequest.isEmpty()) {
            UploadMultipleDocController.data objResponseData = new UploadMultipleDocController.data();
            objResponseData = UploadMultipleDocController.getMultipleDocUrl(lstMultipleDocRequest);
            system.debug('--objResponseData--'+objResponseData);
        }
    } */
}