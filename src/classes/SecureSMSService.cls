/****************************************************************************************************
* Name          : SecureSMSService                                                                  *
* Description   : Class to Send SMS                                                                 *
* Created Date  : 17/04/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                *
* 1.0         Craig Lobo    17/04/2018      Initial Draft.                                          *
****************************************************************************************************/
global class SecureSMSService implements Queueable, Database.AllowsCallouts {
    
    global string message;
    global set<string> mobileNos;
    global string senderName;
    global SecureSMSService(String pMessageBody, set<String> pMobileNumberList, string senderName){
        message = pMessageBody;
        mobileNos = pMobileNumberList;
        senderName = senderName;
    }
    
    global void execute(QueueableContext qc){
        sendSecureSMS(message, mobileNos, senderName);
    }

    public static Map<String, Object> responseMap;

    /**
      * SecureSMSService
      */
    public static Map<String, Object> sendSecureSMS(String pMessageBody, set<String> pMobileNumberList, string senderName) {

        if (pMobileNumberList != null && !pMobileNumberList.isEmpty()) {
            Secure_SMS__mdt secureSMSObj = [ SELECT Id
                                                  , User_Name__c
                                                  , Password__c
                                                  , Endpoint__c
                                                  , Sender_Name__c 
                                               FROM Secure_SMS__mdt 
                                              WHERE DeveloperName = 'SMS_BULKSMS' 
                                              LIMIT 1
            ];

            String endPoint = secureSMSObj.Endpoint__c
                            + 'username='
                            + secureSMSObj.User_Name__c
                            + '&password='
                            + secureSMSObj.Password__c;

            DAMAC_SMS_REQUEST_OBJ reqObj = new DAMAC_SMS_REQUEST_OBJ();
            reqObj.Message = pMessageBody;
            reqObj.SenderName = secureSMSObj.Sender_Name__c;
            list<string> mobileNos = new list<string>();
            mobileNos.addAll(pMobileNumberList);
            reqObj.MobileNumbers = mobileNos;

            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(endpoint); 
            req.setMethod('POST');
            req.setheader('Content-Type','application/json'); 
            req.setbody(JSON.serialize(reqObj));
            HttpResponse res = h.send(req);
            system.debug(res.getBody()); 
            responseMap = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
        }
        return responseMap;

    }
    
    public static void prepareData(string message, list<string> mobileNos){
        set<string> uaeNos = new set<string>();
        set<string> saudiNos = new set<string>();
        set<string> others = new set<string>();
        for(string mobilePhone : mobileNos){
            mobilePhone = mobilePhone.removeStart('00');
        
            //Added by Charan on 2nd Nov 2020
            mobilePhone = mobilePhone.removeStart('+');
            if(mobilePhone.startsWith('971'))
                uaeNos.add(mobilePhone);
            else if(mobilePhone.startsWith('966'))
                saudiNos.add(mobilePhone);
            else
                others.add(mobilePhone);
        }
        
        if(!uaeNos.isEmpty()){
            SecureSMSService sss = new SecureSMSService(message, uaeNos, System.label.UAE_Sender_Name);
            System.enqueueJob(sss);
        }
        if(!saudiNos.isEmpty()){
            SecureSMSService sss = new SecureSMSService(message, saudiNos, System.label.Saudi_Sender_Name);
            System.enqueueJob(sss);
        }
        if(!others.isEmpty()){
            SecureSMSService sss = new SecureSMSService(message, others, System.label.Common_Sender_Name);
            System.enqueueJob(sss);
        }
    }

}