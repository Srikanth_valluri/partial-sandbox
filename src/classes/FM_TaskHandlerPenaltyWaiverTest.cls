@isTest
global without sharing class FM_TaskHandlerPenaltyWaiverTest implements HttpCalloutMock {
    
    static testMethod void testMethod1() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__SR_Status__c objStat = new NSIBPM__SR_Status__c();
        objStat.Name = 'Test';
        objStat.NSIBPM__Code__c = '123';
        insert objStat ;
        
        NSIBPM__Service_Request__c objServReq = TestDataFactory_CRM.createServiceRequest();
        objServReq.NSIBPM__Internal_SR_Status__c = objStat.Id ;
        insert objServReq ;
        
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objServReq.Id,1);
        insert lstBooking ;
        
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus ;
        
        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking ,3);
        for( Booking_Unit__c objUnit : lstBookingUnit  ) {
            objUnit.Resident__c = objAcc.Id ;
            //objUnit.Handover_Flag__c = 'Y' ;
            //objUnit.Registration_ID__c = '';
        }
        lstBookingUnit[2].Owner__c = objAcc.Id ;
        insert lstBookingUnit ;
        
        //insert new FM_POP_Field_Reference__c(Use_Cash_Receipt_Id__c = true);
        
        FM_Case__c fmObj  = new FM_Case__c();
        fmObj.Booking_Unit__c = lstBookingUnit[0].id;
        fmObj.Tenant__c = objAcc.id;
        fmObj.Account__c = objAcc.id;
        fmObj.Request_Type_DeveloperName__c = 'Penalty_Waiver';
        insert fmObj;
        
        SR_Booking_Unit__c objJunc = new SR_Booking_Unit__c (Booking_Unit__c = lstBookingUnit[0].Id, FM_Case__c = fmObj.Id) ;
        insert objJunc ;
        
        Task objTask = new Task();
        objTask.WhatId = fmObj.Id ;
        objTask.Priority = 'Low';
        objTask.Status = 'In Progress';
        objTask.ActivityDate = system.today();
        objTask.Subject = Label.POP_Task_Subject_FM_Finance ;
        objTask.Cash_Receipt_Id__c = '123' ;
        insert objTask ;
        
        Test.setMock(HttpCalloutMock.class, new FM_TaskHandlerPenaltyWaiverTest() );
        
        FM_TaskHandlerPenaltyWaiver.sendPWApprovalMail( new list<Task>{ objTask } );
        FM_TaskHandlerPenaltyWaiver.addErrorLogs( objAcc.Id, fmObj.id, 'Testt' );
        
        UploadMultipleDocController.data objResponse = new UploadMultipleDocController.data();
        objResponse.data = new list<UploadMultipleDocController.MultipleDocResponse>();
        FM_TaskHandlerPenaltyWaiver.insertCustomAttachment( fmObj, objResponse , 'Test' );
            
    }
    
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"responseId":"304141","responseTime":"Tue Nov 13 13:40:33 GST 2018","actions":[{"action":"VIEW_FILE","method":"GET","url":"https://ptctest.damacgroup.com/DCOFFEE/show/header/39a63103f36d1280272aedc17b1267cf"}],"complete":true}');
        res.setStatusCode(200);
        return res;
    }
    
}