@isTest
public class DL_AppointmentDuePushNotifyHandlerTest {
    @isTest
    static void  testAppointmentDueNotification() {
        Test.startTest();
        
        List<DL_AppointmentDuePushNotificationHandler.AppointmentDataWrap> argList = 
            new List<DL_AppointmentDuePushNotificationHandler.AppointmentDataWrap>();
        DL_AppointmentDuePushNotificationHandler.AppointmentDataWrap arg1 = new DL_AppointmentDuePushNotificationHandler.AppointmentDataWrap();
        arg1.accountId = '0010Y00000MaLroQAF';
        arg1.partyId = '12345';
        arg1.unitId = 'TEST/UNIT/98765';
        arg1.regId = '12345/98765';
        arg1.appointmentId = 'a371w000001j9FPAAY';
        arg1.processName = 'Cheque collection';
        arg1.appointmentStartDateTime = DateTime.NOW().addDays(3);
        arg1.appointmentSlot = 'a371w000001j9FPAAY';
        argList.add(arg1);
        
        DL_AppointmentDuePushNotificationHandler.sendPushNotification(argList);
        Test.stopTest();
    }
}