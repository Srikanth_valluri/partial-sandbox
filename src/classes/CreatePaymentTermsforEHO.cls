public class CreatePaymentTermsforEHO {

    @future(Callout=true)
    public static void EarlyHandoverPaymentPlanCreationResponce(String caseId) {
        system.debug('!!!!!!inside CreatePaymentTermsforEHO');
         Case ObjCase = [Select Id, Booking_Unit__c, Apply_New_Payment_Terms__c, Booking_Unit__r.Registration_ID__c, caseNumber, NewPaymentTermJSON__c From Case Where Id =: caseId];
         list<SelectedNewPaymentTermsByCRE> lstSelectedPT;
         if (ObjCase.NewPaymentTermJSON__c != null) {
             lstSelectedPT = (List<SelectedNewPaymentTermsByCRE>)JSON.Deserialize(ObjCase.NewPaymentTermJSON__c,List<SelectedNewPaymentTermsByCRE>.class);
         }
          list<HandoverMQServicesXxdcAoptPkgWsP.APPSXXDC_AOPT_PKG_WSX1843128X6X5> lstHandMQS = new list<HandoverMQServicesXxdcAoptPkgWsP.APPSXXDC_AOPT_PKG_WSX1843128X6X5>();
          system.debug('!!!!!!!!lstSelectedPT'+lstSelectedPT);
          if (lstSelectedPT != null && lstSelectedPT.size() > 0) {
              for (SelectedNewPaymentTermsByCRE objPT : lstSelectedPT) {
                  Date paymentDate;
                  if (!String.isBlank(objPT.paymentDate)){
                      paymentDate = date.parse(objPT.paymentDate);
                      system.debug('!!!date'+DateTime.newInstance(paymentDate.year(),paymentDate.month(),paymentDate.day()).format('dd-MMM-YYYY'));
                  }
                  HandoverMQServicesXxdcAoptPkgWsP.APPSXXDC_AOPT_PKG_WSX1843128X6X5 objHandMQS = new HandoverMQServicesXxdcAoptPkgWsP.APPSXXDC_AOPT_PKG_WSX1843128X6X5();
                  objHandMQS.REGISTRATION_ID = ObjCase.Booking_Unit__r.Registration_ID__c;
                  objHandMQS.DESCRIPTION = objPT.description;
                  if (!string.isBlank(objPT.paymentDate)) {
                      //objHandMQS.EXPECTED_DATE = String.valueOf(DateTime.newInstance(paymentDate.year(),paymentDate.month(),paymentDate.day()).format('d-MMM-YYYY'));
                      //objHandMQS.PAYMENT_DATE = String.valueOf(DateTime.newInstance(paymentDate.year(),paymentDate.month(),paymentDate.day()).format('d-MMM-YYYY'));
                      //objHandMQS.PDC_DATE = String.valueOf(DateTime.newInstance(paymentDate.year(),paymentDate.month(),paymentDate.day()).format('d-MMM-YYYY'));
                      objHandMQS.EXPECTED_DATE = getConvertDateTime(objPT.paymentDate);
                      objHandMQS.PAYMENT_DATE = getConvertDateTime(objPT.paymentDate);
                      objHandMQS.PDC_DATE = getConvertDateTime(objPT.paymentDate);
                  }              
                  objHandMQS.INSTALLMENT = objPT.installment;
                  objHandMQS.LINE_ID = objPT.lineId;
                  objHandMQS.MILESTONE_EVENT = objPT.mileStoneEvent;
                  if (!String.isBlank(objPT.paymentAmount) && objPT.isReceiptPresent == true) {  
                      objHandMQS.PAYMENT_AMOUNT = objPT.paymentAmount;         
                  } else if (objPT.isReceiptPresent == false && !String.isBlank(objPT.leaseAmount) && !String.isBlank(objPT.installmentAmount)){
                      Decimal payment = decimal.valueOf(objPT.leaseAmount) + decimal.valueOf(objPT.installmentAmount);
                      objHandMQS.PAYMENT_AMOUNT = string.valueOf(payment);
                      objHandMQS.PDC_AMOUNT = string.valueOf(payment);
                  } else if (objPT.isReceiptPresent == false && String.isBlank(objPT.leaseAmount) && String.isBlank(objPT.installmentAmount)
                      && !String.isBlank(objPT.paymentAmount)) {
                      objHandMQS.PAYMENT_AMOUNT = objPT.paymentAmount; 
                  }                                                                        
                  objHandMQS.PERCENT_VALUE = objPT.percentValue;
                  objHandMQS.TERM_ID = objPT.termId;
                  lstHandMQS.add(objHandMQS);
              }
            }
          
        String P_REGISTRATION_ID = ObjCase.Booking_Unit__r.Registration_ID__c;
        String P_SR_NUMBER = '2-'+ObjCase.CaseNumber;
        String P_SR_TYPE = 'EARLY_HANDOVER';

        HandoverMQServices.HandoverHttpSoap11Endpoint service =  new HandoverMQServices.HandoverHttpSoap11Endpoint();
        service.timeout_x = 120000;
        String res = service.EarlyHandoverPaymentPlanCreation(P_REGISTRATION_ID ,
                                                                P_SR_NUMBER,
                                                                'EARLY_HANDOVER',
                                                                lstHandMQS
                                                            ); 
                                                            
         system.debug('!!!!!!!!res'+res);
         PaymentTermWrapperClass response = (PaymentTermWrapperClass)JSON.deserialize(res,
                                                    PaymentTermWrapperClass.class
                                                );  
          system.debug('!!!!!!!!response'+response);
        if (response.status == 'S') {
            String PPold;
            List<Payment_Plan__c> lstPP = new List<Payment_Plan__c>();
            for (Payment_Plan__c objPaymentPlan : [Select Id, Status__c, Booking_Unit__c From Payment_Plan__c Where Booking_Unit__c =: ObjCase.Booking_Unit__c]){
                objPaymentPlan.Status__c = 'InActive';
                lstPP.add(objPaymentPlan);
                PPold = objPaymentPlan.id;
             }
             if (lstPP != null && lstPP.size()>0){
                 update lstPP;
             }
             
             Case updateCase = new Case(Id = ObjCase.id);
             updateCase.Apply_New_Payment_Terms__c = true;
             update updateCase;
        
            Payment_Plan__c newPaymentPlan = new Payment_Plan__c();
            newPaymentPlan.Status__c = 'Active';
            newPaymentPlan.Booking_Unit__c = ObjCase.Booking_Unit__c;
            newPaymentPlan.Parent_Payment_Plan__c = PPold;
            insert newPaymentPlan;
            
            List<Payment_Terms__c> newPaymentTerms = new List<Payment_Terms__c>();
            if (lstSelectedPT != null && lstSelectedPT.size() > 0) {
                for (SelectedNewPaymentTermsByCRE objSelectNewPaymentTerm : lstSelectedPT) {
                    Payment_Terms__c objPaymentTerm = new Payment_Terms__c();
                    objPaymentTerm.Installment__c = objSelectNewPaymentTerm.installment;
                    objPaymentTerm.Description__c = objSelectNewPaymentTerm.description;
                    objPaymentTerm.Milestone_Event__c = objSelectNewPaymentTerm.mileStoneEvent;
                    objPaymentTerm.Milestone_Event_Arabic__c = objSelectNewPaymentTerm.mileStoneEvent;
                    objPaymentTerm.Percent_Value__c = objSelectNewPaymentTerm.percentValue; 
                    system.debug('objSelectNewPaymentTerm.paymentDate'+objSelectNewPaymentTerm.paymentDate);
                    if (!String.isBlank(objSelectNewPaymentTerm.paymentDate) ) { 
                        objPaymentTerm.Payment_Date__c = Date.parse(objSelectNewPaymentTerm.paymentDate);
                    }
                    objPaymentTerm.Payment_Plan__c = newPaymentPlan.id;
                    if (!String.isBlank(objSelectNewPaymentTerm.paymentAmount) && objSelectNewPaymentTerm.isReceiptPresent == true) {         
                        objPaymentTerm.Payment_Amount__c = objSelectNewPaymentTerm.paymentAmount;
                    } else if (objSelectNewPaymentTerm.isReceiptPresent == false && !String.isBlank(objSelectNewPaymentTerm.leaseAmount) 
                        && !String.isBlank(objSelectNewPaymentTerm.installmentAmount)){
                        Decimal payment = decimal.valueOf(objSelectNewPaymentTerm.leaseAmount) + decimal.valueOf(objSelectNewPaymentTerm.installmentAmount);
                        objPaymentTerm.Payment_Amount__c = string.valueOf(payment);
                    } else if (objSelectNewPaymentTerm.isReceiptPresent == false && String.isBlank(objSelectNewPaymentTerm.leaseAmount) 
                        && String.isBlank(objSelectNewPaymentTerm.installmentAmount) && !String.isBlank(objSelectNewPaymentTerm.paymentAmount)) {
                          objPaymentTerm.Payment_Amount__c = objSelectNewPaymentTerm.paymentAmount; 
                  } 
                    if (!String.isBlank(objSelectNewPaymentTerm.leaseAmount)) {
                        objPaymentTerm.Lease_Amount__c = decimal.valueOf(objSelectNewPaymentTerm.leaseAmount);
                    }
                    if (!String.isBlank(objSelectNewPaymentTerm.installmentAmount)){
                        objPaymentTerm.Installment_Amount__c = decimal.valueOf(objSelectNewPaymentTerm.installmentAmount);
                    }
                    newPaymentTerms.add(objPaymentTerm);
                }            
            }
        
            if (newPaymentTerms != null && newPaymentTerms.size() > 0){
              insert newPaymentTerms;
            }
        } else if (response.status == 'E') {
            Error_Log__c objErr = new Error_Log__c();
            objErr.Account__c = objCase.AccountId;
            objErr.Booking_Unit__c = objCase.Booking_Unit__c;
            objErr.Case__c = objCase.id;
            objErr.Error_Details__c = response.message;
            objErr.Process_Name__c = 'Early Handover';
            insert objErr;
        }         
    }
    
    public static string getConvertDateTime(string strDT) {
        Map<string,string> MapMonthList = new Map<string,string>();
        MapMonthList.put('01','JAN');
        MapMonthList.put('1','JAN');
        MapMonthList.put('02','FEB');
        MapMonthList.put('2','FEB');
        MapMonthList.put('03','MAR');
        MapMonthList.put('3','MAR');
        MapMonthList.put('04','APR');
        MapMonthList.put('4','APR');
        MapMonthList.put('05','MAY');
        MapMonthList.put('5','MAY');
        MapMonthList.put('06','JUN');
        MapMonthList.put('6','JUN');
        MapMonthList.put('07','JUL');
        MapMonthList.put('7','JUL');
        MapMonthList.put('08','AUG');
        MapMonthList.put('8','AUG');
        MapMonthList.put('09','SEP');
        MapMonthList.put('9','SEP');
        MapMonthList.put('10','OCT');
        MapMonthList.put('11','NOV');
        MapMonthList.put('12','DEC');
        //system.debug('strDT'+strDT);
        String[] strDTDivided = strDT.split('/');
        //system.debug('strDTDivided '+strDTDivided.get(1) );
        string month = String.ValueOf(MapMonthList.get(strDTDivided.get(1)));
        string day = strDTDivided.get(0).replace('/', '');
        string year = strDTDivided.get(2);
        
        //string stringDate = year + '-' + month + '-' + day;
        string stringDate = day + '-' + month + '-' + year;
        //system.debug('stringDate '+stringDate );
        return stringDate;
    }
    
    public class PaymentTermWrapperClass {
        String message {get;set;}
        String status  {get;set;}
    }
    
     public class SelectedNewPaymentTermsByCRE {
        public String installment {get;set;}
        public String Id {get;set;}
        public Boolean isReceiptPresent {get;set;}
        public String description {get;set;}
        public String mileStoneEvent {get;set;}
        public String mileStoneEventUpdate {get;set;}
        public String mileStoneArabic {get;set;}
        public String percentValue {get;set;}
        public String paymentAmount {get;set;}
        public String paymentDate {get;set;}
        public String termId {get;set;}
        public String name {get;set;}
        public String lineId {get;set;}
        public Boolean isPDC {get;set;}
        public String installmentAmount {get;set;}
        public String leaseAmount {get;set;}
        public Date PTStartDate {get;set;}
        //public List<SelectOption> lstStructureBasedPP {get;set;}
    }
}