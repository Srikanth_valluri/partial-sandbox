@isTest
public class Damac_sendSMSQueueableTest {
    
    @isTest
    public static void testMethod1(){
        Stand_Inquiry__c inq = new Stand_Inquiry__c ();
        inq.Mobile_Phone_Encrypt__c = '8977365305';
        inq.Mobile_CountryCode__c = 'India: 0091';
        inq.SMS_MSG_Id__c = '9876543210';
        inq.Whats_App_Short_URL__c = 'https://test.com';
        insert inq;
        
        Test.startTest();
            System.enqueueJob( new Damac_sendSMSQueueable( new Set<Id>{ inq.id}));
        Test.stopTest();
    }
}