@isTest
public class RentalPoolDocumentGenerationTest {
    static Account objAcc;
    static Id recTypeRPAgreement;
    static NSIBPM__Service_Request__c objDealSR;
    static list<Booking__c> listCreateBookingForAccount;
    static list<Booking_Unit__c> listCreateBookingUnit;
    
    static void init() {
        objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Mobile_Phone_Encrypt__c = '9674858963';
        objAcc.Mobile_Country_Code__c = 'India: 0091';
        objAcc.Mobile_Encrypt__c = '9674858963';
        insert objAcc ;
        system.debug('objAcc=='+objAcc);

        recTypeRPAgreement = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Agreement').getRecordTypeId();
        system.debug('recTypeRPAgreement=='+recTypeRPAgreement);

        objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        listCreateBookingForAccount = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objDealSR.Id, 1);
        insert listCreateBookingForAccount;

        listCreateBookingUnit = TestDataFactory_CRM.createBookingUnits(listCreateBookingForAccount, 1);
        insert listCreateBookingUnit;
    }

    @isTest static void testGenerateRPDocWithoutDocVerified() {
        init();

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeRPAgreement);
        objCase.AccountId = objAcc.Id;
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        insert objCase;

        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(5));
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        RentalPoolDocumentGeneration objDocExt = new RentalPoolDocumentGeneration(stdController);
        objDocExt.caseId = objCase.Id;
        objDocExt.generateRPAgreementDoc();
        test.StopTest();
    }

    @isTest static void testGenerateRPDocSuccess() {
        init();

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeRPAgreement);
        objCase.AccountId = objAcc.Id;
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        objCase.Document_Verified__c = true;
        objCase.Selected_Rental_Pool_Offer__c = 'Cash Guaranteed Return';
        insert objCase;

        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(5));
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        RentalPoolDocumentGeneration objDocExt = new RentalPoolDocumentGeneration(stdController);
        objDocExt.caseId = objCase.Id;
        objDocExt.generateRPAgreementDoc();
        test.StopTest();
    }

    @isTest static void testGenerateRPDocNoRegId() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Mobile_Phone_Encrypt__c = '9674858963';
        objAcc.Mobile_Country_Code__c = 'India: 0091';
        objAcc.Mobile_Encrypt__c = '9674858963';
        insert objAcc ;
        system.debug('objAcc=='+objAcc);

        Id recTypeRPAgreement = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Agreement').getRecordTypeId();
        system.debug('recTypeRPAgreement=='+recTypeRPAgreement);

        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        list<Booking__c> listCreateBookingForAccount = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objDealSR.Id, 1);
        insert listCreateBookingForAccount;

        list<Booking_Unit__c> listCreateBookingUnit = TestDataFactory_CRM.createBookingUnits(listCreateBookingForAccount, 1);
        listCreateBookingUnit[0].Registration_ID__c = '';
        insert listCreateBookingUnit;

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeRPAgreement);
        objCase.AccountId = objAcc.Id;
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        objCase.Document_Verified__c = true;
        insert objCase;

        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(5));
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        RentalPoolDocumentGeneration objDocExt = new RentalPoolDocumentGeneration(stdController);
        objDocExt.caseId = objCase.Id;
        objDocExt.generateRPAgreementDoc();
        test.StopTest();
    }

    @isTest static void testGenerateRPDocNoResponse() {
        init();

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeRPAgreement);
        objCase.AccountId = objAcc.Id;
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        objCase.Document_Verified__c = true;
        insert objCase;

        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(6));
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        RentalPoolDocumentGeneration objDocExt = new RentalPoolDocumentGeneration(stdController);
        objDocExt.caseId = objCase.Id;
        objDocExt.generateRPAgreementDoc();
        test.StopTest();
    }

    @isTest static void testGenerateRPDocOfferNoResponse() {
        init();

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeRPAgreement);
        objCase.AccountId = objAcc.Id;
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        objCase.Document_Verified__c = true;
        objCase.Selected_Rental_Pool_Offer__c = 'Cash Guaranteed Return';
        insert objCase;

        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(6));
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        RentalPoolDocumentGeneration objDocExt = new RentalPoolDocumentGeneration(stdController);
        objDocExt.caseId = objCase.Id;
        objDocExt.generateRPAgreementDoc();
        test.StopTest();
    }

    

    @isTest static void testGenerateRPDocInnerFailure() {
        init();

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeRPAgreement);
        objCase.AccountId = objAcc.Id;
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        objCase.Document_Verified__c = true;
        insert objCase;

        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(8));
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        RentalPoolDocumentGeneration objDocExt = new RentalPoolDocumentGeneration(stdController);
        objDocExt.caseId = objCase.Id;
        objDocExt.generateRPAgreementDoc();
        test.StopTest();

        RentalPoolDocumentGeneration.RPDocDetailsWrapper caseRpObj = new RentalPoolDocumentGeneration.RPDocDetailsWrapper();
        caseRpObj.ATTRIBUTE1 = 'test';
        caseRpObj.ATTRIBUTE2 = 'test';
        caseRpObj.ATTRIBUTE3 = 'test';
        caseRpObj.ATTRIBUTE4 = 'test';
        caseRpObj.PARAM_ID = 'test';
    }

    @isTest static void testGenerateRPDocInnerOfferFailure() {
        init();

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeRPAgreement);
        objCase.AccountId = objAcc.Id;
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        objCase.Document_Verified__c = true;
        objCase.Selected_Rental_Pool_Offer__c = 'Voucher Guaranteed Return';
        insert objCase;

        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(8));
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        RentalPoolDocumentGeneration objDocExt = new RentalPoolDocumentGeneration(stdController);
        objDocExt.caseId = objCase.Id;
        objDocExt.generateRPAgreementDoc();
        test.StopTest();

        RentalPoolDocumentGeneration.RPDocDetailsWrapper caseRpObj = new RentalPoolDocumentGeneration.RPDocDetailsWrapper();
        caseRpObj.ATTRIBUTE1 = 'test';
        caseRpObj.ATTRIBUTE2 = 'test';
        caseRpObj.ATTRIBUTE3 = 'test';
        caseRpObj.ATTRIBUTE4 = 'test';
        caseRpObj.PARAM_ID = 'test';
    }

    @isTest static void testGenerateRPDocOuterFailure() {
        init();

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeRPAgreement);
        objCase.AccountId = objAcc.Id;
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        objCase.Document_Verified__c = true;
        //objCase.Selected_Rental_Pool_Offer__c = 'Voucher Guaranteed Return';
        insert objCase;

        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(7));
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        RentalPoolDocumentGeneration objDocExt = new RentalPoolDocumentGeneration(stdController);
        objDocExt.caseId = objCase.Id;
        objDocExt.generateRPAgreementDoc();
        test.StopTest();
    }

    @isTest static void testGenerateRPDocOuterOfferFailure() {
        init();

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeRPAgreement);
        objCase.AccountId = objAcc.Id;
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        objCase.Document_Verified__c = true;
        objCase.Selected_Rental_Pool_Offer__c = 'Voucher Guaranteed Return';
        insert objCase;

        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(7));
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        RentalPoolDocumentGeneration objDocExt = new RentalPoolDocumentGeneration(stdController);
        objDocExt.caseId = objCase.Id;
        objDocExt.generateRPAgreementDoc();
        test.StopTest();
    }
}