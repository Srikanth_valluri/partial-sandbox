/*
Description : This class will checks the users to has edit / delete access to the Account based on the profile
Created By : Rahul  On : 27-june-2017
Test Class : Account_New_Override_Controller_Test
Change History : 
*/
public with sharing class Account_New_Override_Controller extends Describe_Sobject_Access{
    
    public Account_New_Override_Controller(ApexPages.StandardController controller){
      sObjectName = 'acc';
      getAccess();
        system.debug('######cancreateRecord='+cancreateRecord);
    }
}