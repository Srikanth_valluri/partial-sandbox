public class CaseStaticInitialization { 
   public static boolean firstRunRegIdCreation = true;
   public static boolean firstRunPartyIdCreation = true;
   public static boolean firstRunTransferBuyer = true;
   public static boolean firstRunPostFinacialDetails = true;
   public static boolean firstRunApprovalTask = true;
   public static boolean firstRunUpdateRegStatus = true;
}