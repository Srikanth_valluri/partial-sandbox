public with Sharing class ChangeOwnerController {

    public Calling_List__c callingList {get;set;}
    List<Calling_List__c> selectedRecords;
    String filterId;
    public ChangeOwnerController(ApexPages.StandardSetController controller) {
        callingList = new Calling_List__c();
        selectedRecords = controller.getSelected();
        System.debug('selectedRecords:::'+selectedRecords);
        filterId = controller.getFilterId();
        System.debug('filterId:::'+filterId);
    }

    public pagereference saveOwner() {
        for(Calling_List__c callInst : selectedRecords) {
            callInst.OwnerId = callingList.OwnerId;
            System.debug(' callInst.OwnerId:::'+ callInst.OwnerId);
        }
        update selectedRecords;
        //System.debug(' ApexPages.currentpage().getParameters()::'+ ApexPages.currentpage().getParameters());
        //String url = ApexPages.currentpage().getParameters().get('fcf');
         // String url = ApexPages.currentpage().getUrl();
        //system.debug('url::'+url);
        return new Pagereference(String.format(Label.Lightning_List_view_URL, new List<String>{filterId}));
    }
}