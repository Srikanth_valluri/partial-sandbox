/**********************************************************************************************
Description: Extension of VF Page 'VATLetterDocGen' to generate VAT Letter from Drawloop
----------------------------------------------------------------------------------------------
Version Date(DD-MM-YYYY)    Author              Version History
-----------------------------------------------------------------------------------------------
1.0     10-11-2019          Aishwarya Todkar    Initial Draft
***********************************************************************************************/
public Class VATLetterDocGenCtrl {
    
    public Id buId{ get; set; }

    public VATLetterDocGenCtrl(ApexPages.StandardController controller) {
         Booking_Unit__c objBU= (Booking_Unit__c)controller.getRecord();
         buId = objBU.Id;
    }

    public void generateVATletter() {
        getDdpDetails( 'VAT Letter', buId);
    }
    
/*********************************************************************************
 * Method Name : getDdpDetails
 * Description : Get DDP details from custom setting
 * Return Type : void
 * Parameter(s): Custom Setting Name, Case Id
**********************************************************************************/    
    public void getDdpDetails( String DdpName, Id buId) {
        if( String.isNotBlank( DdpName ) ) {
            Riyadh_Rotana_Drawloop_Doc_Mapping__c cs = Riyadh_Rotana_Drawloop_Doc_Mapping__c.getInstance( DdpName );
            System.debug( 'cs = ' + cs);
            
            if(cs != null ) {
                
                if( String.isBlank( cs.Drawloop_Document_Package_Id__c )) {
                    ApexPages.addmessage(
                        new ApexPages.message( ApexPages.severity.ERROR, 
                        'Please provide Drawloop Document package Id.')
                    );
                }

                if( String.isBlank( cs.Delivery_Option_Id__c )) {
                    ApexPages.addmessage(
                        new ApexPages.message( ApexPages.severity.ERROR, 
                        'Please provide Delivery Option Id.')
                    );
                }
                
                if (!Test.isRunningTest()) {
                    //Execute drawlooop batch 
                    System.debug( '=== executing batch === ');
                    Database.executeBatch( new GenerateDrawloopDocumentBatch( 
                        String.valueOf( buId ), cs.Drawloop_Document_Package_Id__c, cs.Delivery_Option_Id__c)
                    );
                    ApexPages.addmessage(
                        new ApexPages.message( ApexPages.severity.INFO, 
                        'Batch executed successfully.')
                    );
                }
            }//End CS If
            else {
                ApexPages.addmessage(
                        new ApexPages.message( ApexPages.severity.ERROR, 
                        'Can not find Drawloop Document Package in custom settings.')
                );
            }
        }
    }
}