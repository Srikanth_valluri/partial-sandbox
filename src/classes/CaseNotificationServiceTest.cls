@isTest
private class CaseNotificationServiceTest {

    @testSetup static void setupTestData() {
        Account account = new Account(Name = 'Test Account');
        insert account;

        Schema.RecordTypeInfo caseRecordType;
        for (Schema.RecordTypeInfo rtInfo : Schema.SObjectType.Case.getRecordTypeInfosByName().values()) {
            if (rtInfo.isDefaultRecordTypeMapping()) {
                caseRecordType = rtInfo;
            }
        }

        List<Case> lstCase = new List<Case>();
        for (Integer i = 0; i < 100; i++) {
            lstCase.add(new Case(
                AccountId = account.Id,
                Status = 'New',
                Origin = 'Call',
                Priority = 'Medium',
                RecordTypeId = caseRecordType.getRecordTypeId(),
                Subject = Math.mod(i, 2) == 0 ? '' : 'Test'
            ));
    }
        insert lstCase;
    }

    @isTest
    static void testService() {
        List<Account> lstAccount = [SELECT Id FROM Account ORDER BY CreatedDate DESC LIMIT 1];


        CaseNotificationService service = new CaseNotificationService();

        List<NotificationService.Notification> lstNotificationForNull = service.getNotifications();
        System.assertEquals(NULL, lstNotificationForNull);


        NotificationService.Notification notificationRequest = new NotificationService.Notification();
        notificationRequest.accountId = lstAccount[0].Id;
        List<NotificationService.Notification> lstNotification = service.getNotifications(notificationRequest);
        System.assertEquals([SELECT Id FROM Case WHERE Notifications_Read__c = FALSE].size(), lstNotification.size());

        lstNotification = CaseNotificationService.markRead(lstNotification);
        System.assertEquals([SELECT Id FROM Case WHERE Notifications_Read__c = TRUE].size(), lstNotification.size());

        NotificationService.Notification unreadNotification = service.markUnRead(lstNotification[0]);
        System.assertEquals(1, [SELECT Id FROM Case WHERE Notifications_Read__c = FALSE].size());
    }

}