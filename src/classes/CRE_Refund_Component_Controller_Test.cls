@isTest
/*
* Revision History: 
* Version   Author          Date        Description.
* 1.1       Swapnil Gholap  17/11/2017  Initial Draft
*/

public class CRE_Refund_Component_Controller_Test {
    static Account  objAcc;
    static Case objCase;
    static Booking__c objBooking;
    static Booking_Unit__c objBookingUnit;
    static NSIBPM__Service_Request__c objSR;
    static Booking_Unit_Active_Status__c objBUActive;
    static Option__c objOptions;
    static Payment_Plan__c objPaymentPlan;
    static SR_Attachments__c objSRatt ;
    static SR_Booking_Unit__c objSRB;
    
    static testMethod void initialMethod(){
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person',party_ID__C='123456');
        insert objAcc;
        
        objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'Test Unit';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        insert objBookingUnit;
        
        objBUActive = new Booking_Unit_Active_Status__c ();
        objBUActive.name = 'Test Active Status';
        objBUActive.Status_Value__c = 'Agreement executed by DAMAC'; 
        insert objBUActive; 
        
        objOptions = new Option__c();
        objOptions.Booking_Unit__c = objBookingUnit.id;
        objOptions.PromotionName__c = 'PromotionName__c';
        objOptions.SchemeName__c = 'SchemeName__c';
        objOptions.OptionsName__c = 'OptionsName__c';
        objOptions.CampaignName__c = 'CampaignName__c';
        insert objOptions;
        
        objPaymentPlan = new Payment_Plan__c();
        objPaymentPlan.Booking_Unit__c = objBookingUnit.id;
        objPaymentPlan.Status__C = '';
        insert objPaymentPlan;
        
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Token Refund').getRecordTypeId();
        objCase = new Case();
        objCase.AccountID = objAcc.id ;
        objCase.RecordTypeID = devRecordTypeId;
        objCase.Total_Token_Amount__c = 1000;
        objCase.Excess_Amount__c = 5000;
        objCase.POA_Expiry_Date__c = System.today();
        objCase.IsPOA__c = true;
        insert objCase;
        
        objSRatt = new SR_Attachments__c ();
        objSRatt.name = 'Test NOC';
        objSRatt.Case__c = objCase.Id;         
        objSRatt.type__c = 'NOC' ;
        //insert objSRatt;
        
        objSRB = new SR_Booking_Unit__c();
        objSRB.Case__c = objCase.id;
        objSRB.Booking_Unit__c = objBookingUnit.id;   
        insert objSRB;
    }
    
    static testMethod void Test1(){
        test.StartTest();
        initialMethod();
        
        Case objCase1 = new Case();
        objCase1.AccountID = objAcc.id;
        objCase1.Status = 'New';
        objCase1.Booking_Unit__c = objBookingUnit.id;
        insert objCase1 ;
        
        PageReference pageRef = Page.RefundsProcessPage;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id); 
        CRE_Refund_Component_Controller objClass = new  CRE_Refund_Component_Controller ();
       
        objClass.strAccountID = objAcc.id;
        objClass.strSRType = 'Refunds';
        objClass.getName();
        
        objClass.bookingUnitDetails();
        
        objClass.strSelectedRefundProcess = 'Customer Refund';
        objClass.bookingUnitDetails();
        
        objClass.strSelectedBookingUnit = objBookingUnit.id;
        objClass.bookingUnitDetails();
        
        objPaymentPlan.Status__C = 'Active';
        update objPaymentPlan; 
        
        objClass.blnIsLitigationFlag = true;
        objClass.bookingUnitDetails();               
        
        test.StopTest();
    }
    
    static testMethod void Test1A(){
       
        initialMethod();
        
        Case objCase1 = new Case();
        objCase1.AccountID = objAcc.id;
        objCase1.Status = 'New';
        objCase1.Booking_Unit__c = objBookingUnit.id;
        insert objCase1 ;
        
       
        PageReference pageRef = Page.RefundsProcessPage;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id); 
        CRE_Refund_Component_Controller objClass = new  CRE_Refund_Component_Controller ();
        objClass.strSelectedRefundProcess = 'Token Refunds';
        objClass.strSelectedBookingUnit = objBookingUnit.id;
       
        test.StartTest();
         UploadMultipleDocController.strLabelValue = 'N';
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT());
        objClass.bookingUnitDetails();                            
        
        test.StopTest();
    }
    
    static testMethod void Test2(){
        
        initialMethod();
        
        Case objCase1 = new Case();
        objCase1.AccountID = objAcc.id;
        objCase1.Status = 'Closed';
        objCase1.Booking_Unit__c = objBookingUnit.id;
        insert objCase1 ;
        
        PageReference pageRef = Page.RefundsProcessPage;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id); 
        ApexPages.currentPage().getParameters().put('CaseID', objCase.id); 
        ApexPages.currentPage().getParameters().put('SRType', 'Refunds');
        CRE_Refund_Component_Controller objClass = new  CRE_Refund_Component_Controller ();
                       
        
        SR_Attachments__c objSRatt1 = new SR_Attachments__c ();
        objSRatt1.name = 'Test Power of Attorney';
        objSRatt1.Case__c = objCase.Id;         
        objSRatt1.type__c = 'Power of Attorney' ;
        insert objSRatt1;
        
        objClass.deletePowerAttornyDoc();
        
        objClass.objCase.IsPOA__c = false;        
        objClass.deletePowerAttornyDoc();
        
        list<SR_Attachments__c> lstSRatt = new list<SR_Attachments__c>();
        SR_Attachments__c objSRatt2 = new SR_Attachments__c ();
        objSRatt2.name = 'Test Power of Attorney';
        objSRatt2.Case__c = objCase.Id;         
        objSRatt2.type__c = 'Power of Attorney' ;
        lstSRatt.add(objSRatt2);   
        
        SR_Attachments__c objSRatt3 = new SR_Attachments__c ();
        objSRatt3.name = 'Test NOC';
        objSRatt3.Case__c = objCase.Id;         
        objSRatt3.type__c = 'NOC' ;        
        lstSRatt.add(objSRatt3);  
        
        SR_Attachments__c objSRatt4 = new SR_Attachments__c ();
        objSRatt4.name = 'Test CRF';
        objSRatt4.Case__c = objCase.Id;         
        objSRatt4.type__c = 'CRF Form' ;
        lstSRatt.add(objSRatt4);  
        insert lstSRatt;
        
        test.StartTest();
        objClass.RemoveSelected = lstSRatt[0].id;
        objClass.removeAttachment();
        
        objClass.RemoveSelected = lstSRatt[1].id;
        objClass.removeAttachment();
        
        objClass.RemoveSelected = lstSRatt[2].id;
        objClass.removeAttachment();
        
        //objClass.callCRFdocument();
        
        test.StopTest();
    }
    
     static testMethod void Test3(){
       
        initialMethod();               
                
        PageReference pageRef = Page.RefundsProcessPage;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id); 
        ApexPages.currentPage().getParameters().put('CaseID', objCase.id); 
        ApexPages.currentPage().getParameters().put('SRType', 'Refunds');
        CRE_Refund_Component_Controller objClass = new  CRE_Refund_Component_Controller ();
        objClass.strSelectedRefundProcess = 'Refund Liability'; 
        test.StartTest(); 
        Test.setMock(WebServiceMock.class, new RefundsMock.RefundsMock2()); 
        objClass.showRefundValues();
               
        test.StopTest();
                      
     } 
     
     static testMethod void Test3A(){
       
        initialMethod();               
        
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Customer Refund').getRecordTypeId();
        objCase.recordtypeID = devRecordTypeId;
        update objCase;
                
        PageReference pageRef = Page.RefundsProcessPage;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id); 
        ApexPages.currentPage().getParameters().put('CaseID', objCase.id); 
        ApexPages.currentPage().getParameters().put('SRType', 'Refunds');
        CRE_Refund_Component_Controller objClass = new  CRE_Refund_Component_Controller ();
        Id idEOIRTSR = Schema.SObjectType.Case_Extension__c.getRecordTypeInfosByName().get('Customer Refund').getRecordTypeId();
        Case_Extension__c objCaseExt = new Case_Extension__c();
        objCaseExt.Case__c = objCase.id;
        insert objCaseExt;
        
        test.StartTest();        
        objClass.strSelectedRefundProcess = 'Customer Refund';                
        Test.setMock(WebServiceMock.class, new RefundsExcessAmountMock.RefundsExcessAmountMock1());              
        objClass.showRefundValues();
        
        test.StopTest();
                      
     } 
     
     
     static testMethod void Test33  (){
        test.StartTest();
        initialMethod();
        
        
        
        PageReference pageRef = Page.RefundsProcessPage;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id); 
        ApexPages.currentPage().getParameters().put('CaseID', objCase.id); 
        ApexPages.currentPage().getParameters().put('SRType', 'Refunds');
        CRE_Refund_Component_Controller objClass = new  CRE_Refund_Component_Controller ();
           Id idEOIRTSR = Schema.SObjectType.Case_Extension__c.getRecordTypeInfosByName().get('Customer Refund').getRecordTypeId(); 
        Case_Extension__c objCaseExt = new Case_Extension__c(); 
        objCaseExt.Case__c = objCase.id;    
        insert objCaseExt;
        
        objClass.showRefundValues();
                     
        objClass.strSelectedRefundProcess = 'Customer Refund';
        objClass.showRefundValues();
        
        test.StopTest();
     }       
     
      static testMethod void Test4(){
        test.StartTest();
        initialMethod();
        

        
        PageReference pageRef = Page.RefundsProcessPage;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id); 
        ApexPages.currentPage().getParameters().put('CaseID', objCase.id); 
        ApexPages.currentPage().getParameters().put('SRType', 'Refunds');
        CRE_Refund_Component_Controller objClass = new  CRE_Refund_Component_Controller ();
        
        objClass.crfAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.crfAttachmentName = 'C/fakepath/Document_1.txt';
        objClass.PoaAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.PoaAttachmentName = 'C:/fakepath/Document_2.txt';
        objClass.NocAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.NocAttachmentName = 'C:/fakepath/Document_3.txt';       
        
        objClass.saveDraft();
        objClass.submitSR();
        
        test.StopTest();
     }
     
      static testMethod void Test5(){
        test.StartTest();
        initialMethod();              
        
        PageReference pageRef = Page.RefundsProcessPage;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id); 
        CRE_Refund_Component_Controller objClass = new  CRE_Refund_Component_Controller ();
        objClass.strSelectedRefundProcess = 'AOPT';      
        objClass.strSelectedBookingUnit = objBookingUnit.id;
        objClass.showRefundValues();              
        
        test.StopTest();
    }
    
    static testMethod void Test6(){
       
        initialMethod();              
        
        PageReference pageRef = Page.RefundsProcessPage;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id); 
        CRE_Refund_Component_Controller objClass = new  CRE_Refund_Component_Controller ();
        objClass.strSelectedRefundProcess = 'Token Refund';      
        objClass.strSelectedBookingUnit = objBookingUnit.id;
        
         test.StartTest();
         SOAPCalloutServiceMock.returnToMe = new Map<String, GenerateSOAService.GenCustomerStatementResponse_element>();
         GenerateSOAService.GenCustomerStatementResponse_element response = new GenerateSOAService.GenCustomerStatementResponse_element();
         response.return_x = '{"Status":"S","PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=338397 and Request Id :40187735 ...","Message":"Process Completed Returning 1 Response Message(s)...","PARAM_ID":"92061","REQUEST_ID":"40187735","STAGE_ID":"338397","URL":null}';
         SOAPCalloutServiceMock.returnToMe.put('response_x', response);
         Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
         objClass.insertStatementOfAccount();      
        
         test.StopTest();
    }
    
    static testMethod void Test7(){
       
        initialMethod();              
        
        PageReference pageRef = Page.RefundsProcessPage;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id); 
        CRE_Refund_Component_Controller objClass = new  CRE_Refund_Component_Controller ();
        objClass.strSelectedRefundProcess = 'Token Refund';      
        objClass.strSelectedBookingUnit = objBookingUnit.id;
        
         test.StartTest();
         SOAPCalloutServiceMock.returnToMe = new Map<String, GenerateCRFService.GetCustomerRequestFormResponse_element>();
         GenerateCRFService.GetCustomerRequestFormResponse_element response1 = new GenerateCRFService.GetCustomerRequestFormResponse_element();
         response1.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=403501 and Request Id :40815712 ...","ATTRIBUTE3":"403501","ATTRIBUTE2":"40815712","ATTRIBUTE1":"null","PARAM_ID":"74428"}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';
         SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
         Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
         
         objClass.callCRFdocument();      
        
         test.StopTest();
    }
    
    static testMethod void Test8(){
        
        initialMethod();
        
        Case objCase1 = new Case();
        objCase1.AccountID = objAcc.id;
        objCase1.Status = 'Closed';
        objCase1.Booking_Unit__c = objBookingUnit.id;
        insert objCase1 ;
        
        PageReference pageRef = Page.RefundsProcessPage;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id); 
        ApexPages.currentPage().getParameters().put('CaseID', objCase.id); 
        ApexPages.currentPage().getParameters().put('SRType', 'Refunds');
        CRE_Refund_Component_Controller objClass = new  CRE_Refund_Component_Controller ();
                       
        
        objClass.crfAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.crfAttachmentName = 'C/fakepath/Document_1.txt';
        /*objClass.PoaAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.PoaAttachmentName = 'C:/fakepath/Document_2.txt';*/
        /*objClass.NocAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.NocAttachmentName = 'C:/fakepath/Document_3.txt';  */
        objClass.WireAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.WireAttachmentName = 'C:/fakepath/Document_3.txt';      
        objClass.OtherAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.OtherAttachmentName = 'C:/fakepath/Document_3.txt'; 
                     
        test.StartTest();
         UploadMultipleDocController.strLabelValue = 'N';
        //Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT());
        objClass.saveDraft();
        
        objClass.crfAttachmentBody = '';
        objClass.crfAttachmentName = '';
        objClass.PoaAttachmentBody = '';
        objClass.PoaAttachmentName = '';
        
        //Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT());
        objClass.saveDraft();
        
        
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(3));
        objClass.saveDraft();
        test.StopTest();
    }
    
    static testMethod void Test8A(){
        
        initialMethod();
        
        Case objCase1 = new Case();
        objCase1.AccountID = objAcc.id;
        objCase1.Status = 'Closed';
        objCase1.Booking_Unit__c = objBookingUnit.id;        
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Customer Refund').getRecordTypeId();
        objCase.recordtypeID = devRecordTypeId;
        upsert objCase;
                
        delete objSRB;
        
        PageReference pageRef = Page.RefundsProcessPage;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id); 
        ApexPages.currentPage().getParameters().put('CaseID', objCase.id); 
        ApexPages.currentPage().getParameters().put('SRType', 'Refunds');
        CRE_Refund_Component_Controller objClass = new  CRE_Refund_Component_Controller ();
                       
        objClass.strSelectedBookingUnit = objBookingUnit.id;
        
        objClass.crfAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.crfAttachmentName = 'C/fakepath/Document_1.txt';
        /*objClass.PoaAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.PoaAttachmentName = 'C:/fakepath/Document_2.txt';
        objClass.NocAttachmentBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        objClass.NocAttachmentName = 'C:/fakepath/Document_3.txt';    */   
        
                     
        test.StartTest();
         UploadMultipleDocController.strLabelValue = 'N';
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
        objClass.saveDraft();
        
        objClass.crfAttachmentBody = '';
        objClass.crfAttachmentName = '';
        objClass.PoaAttachmentBody = '';
        objClass.PoaAttachmentName = '';
        
        //Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
        //objClass.saveDraft();
        test.StopTest();
    }
    
      static testMethod void Test9(){
       
        initialMethod();
        

        objCase.Roles_from_Rule_Engine__c = 'VP';
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Customer Refund').getRecordTypeId();
        objCase.recordtypeID = devRecordTypeId;
        update objCase;
        
        PageReference pageRef = Page.RefundsProcessPage;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id); 
        ApexPages.currentPage().getParameters().put('CaseID', objCase.id); 
        ApexPages.currentPage().getParameters().put('SRType', 'Refunds');
        CRE_Refund_Component_Controller objClass = new  CRE_Refund_Component_Controller ();              
        //objClass.strSelectedRefundProcess = 'Token Refund';                
        //objClass.strSelectedBookingUnit = objBookingUnit.id;       
        
        test.StartTest();
        objClass.decExcessAmount = 1000;
        Test.setMock(WebServiceMock.class, new RefundsApprovingAuthoritiesMock()); 
        WrapperBookingUnit objWrapper = new WrapperBookingUnit();
        
        UnitDetailsService.BookinUnitDetailsWrapper objIPMSDetailsWrapper = new  UnitDetailsService.BookinUnitDetailsWrapper();        
        
        objIPMSDetailsWrapper.strCustomerClassification = 'Test';
        objIPMSDetailsWrapper.strPortfolioValue = '200';
        objIPMSDetailsWrapper.strProject = 'Test';
        objIPMSDetailsWrapper.strProjectCity = 'Test';
        objIPMSDetailsWrapper.strBedroomType_UnitType = 'Test';
        objIPMSDetailsWrapper.strBuildingCode = 'Test';
        objIPMSDetailsWrapper.strPermittedUse = 'Test';
        objIPMSDetailsWrapper.strReady_Offplan = 'Test';
        objIPMSDetailsWrapper.strEHOFlag = 'Test';
        objIPMSDetailsWrapper.strHOFlag = 'Test';        
        objWrapper.objIPMSDetailsWrapper = objIPMSDetailsWrapper;        
        
        List<WrapperBookingUnit> lstWrapperBookingUnit1 = new List<WrapperBookingUnit>();
        
        lstWrapperBookingUnit1.add(objWrapper);
        
        objClass.lstWrapperBookingUnit =  lstWrapperBookingUnit1;
        objClass.submitSR();
        
        test.StopTest();
     }
     
     static testMethod void Test12(){
       
        initialMethod();               
                
        PageReference pageRef = Page.RefundsProcessPage;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('AccountID', objAcc.id); 
        //ApexPages.currentPage().getParameters().put('CaseID', objCase.id); 
        ApexPages.currentPage().getParameters().put('SRType', 'Refunds');
        CRE_Refund_Component_Controller objClass = new  CRE_Refund_Component_Controller ();
        objClass.strSelectedRefundProcess = 'Customer Refund'; 
        test.StartTest(); 
        Test.setMock(WebServiceMock.class, new RefundsMock.RefundsMock2()); 
        objClass.showRefundValues();
               
        test.StopTest();
                      
     }
}