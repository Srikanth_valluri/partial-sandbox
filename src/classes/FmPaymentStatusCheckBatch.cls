global class FmPaymentStatusCheckBatch
        implements Database.Batchable<sObject>, Database.AllowsCallouts, Schedulable, Database.Stateful {

    public String startDateTimeStamp   = '2019-06-10T00:00:00Z';

    String query;

    Map<String, String> fmReceiptResponse    = new Map<String, String>();
    Map<String, String> failedReceipts       = new Map<String, String>();

    Set<String> successOrderStatus = new Set<String> { 'Shipped', 'Success' };

    global FmPaymentStatusCheckBatch() {
        query = 'SELECT     Id ' +
                            ' , Name ' +
                            ' , Customer_Party_Id__c ' +
                            ' , Registration_Id__c ' +
                            ' , Unit_Name__c ' +
                            ' , Acknowledgement_Id__c ' +
                             //' , Total_Amount__c ' +
                             //' , Discount__c ' +
                             //' , Discount_Applied__c ' +
                            ' , Amount__c ' +
                            ' , Bank_Receipt_Number__c ' +
                            ' , Bank_Reference_Number__c ' +
                            ' , Billing_Address__c ' +
                            ' , Billing_City__c ' +
                            ' , Billing_Country__c ' +
                            ' , Billing_Email__c ' +
                            ' , Billing_Name__c ' +
                            ' , Billing_Phone__c ' +
                            ' , Billing_State__c ' +
                            ' , Billing_Zip__c ' +
                            ' , Card_Holder_Name__c ' +
                            ' , Card_Name__c ' +
                            ' , Currency__c ' +
                            ' , ECI_Value__c ' +
                            ' , Merchant_Amount__c ' +
                            ' , Order_Status__c ' +
                            ' , Payment_Mode__c ' +
                            ' , Payment_Type__c ' +
                            ' , Guest_Payment_Type__c ' +
                            ' , Booking_Unit__c ' +
                            ' , Booking_Unit__r.Building_Name__c ' +
                            ' , Guest_Other_Payment_Type__c ' +
                            ' , Status_Code__c ' +
                            ' , Status_Message__c ' +
                            ' , Receipt_created_in_IPMS__c ' +
                            ' , ( SELECT  Id ' +
                                        ' , Name ' +
                                        ' , Booking_Unit__c ' +
                                        ' , Registration_Id__c ' +
                                        ' , Unit_Name__c ' +
                                        //, Is_Next_Q1_Invoice__c
                                        //, TRX_Id__c
                                ' FROM    FM_Unit_Invoice_Payments__r ' +
                            ')' +
                ' FROM       FM_Receipt__c ' +
                ' WHERE      (CreatedDate >= ' + this.startDateTimeStamp + ') ' +
                            'AND (Payment_Type__c = \'FM Guest Payment\' ' +
                                    'OR (Payment_Type__c = \'' + Label.FMPortalServiceCharge
                                    + '\' AND CreatedDate >= ' + this.startDateTimeStamp + ')) ' +
                            'AND ((Order_Status__c = NULL OR Order_Status__c = \'\' )' +
                                'OR (Receipt_created_in_IPMS__c != \'TRUE\' ' +
                                    'AND Order_Status__c IN :successOrderStatus ))' +
                ' ORDER BY   CreatedDate DESC '/* +
                'LIMIT      1'*/;
        System.debug('query = ' + query);
    }

    global FmPaymentStatusCheckBatch(String pStartDateTimeStamp) {
        this.startDateTimeStamp = pStartDateTimeStamp;
        query = 'SELECT     Id ' +
                            ' , Name ' +
                            ' , Customer_Party_Id__c ' +
                            ' , Registration_Id__c ' +
                            ' , Unit_Name__c ' +
                            ' , Acknowledgement_Id__c ' +
                             //' , Total_Amount__c ' +
                             //' , Discount__c ' +
                             //' , Discount_Applied__c ' +
                            ' , Amount__c ' +
                            ' , Bank_Receipt_Number__c ' +
                            ' , Bank_Reference_Number__c ' +
                            ' , Billing_Address__c ' +
                            ' , Billing_City__c ' +
                            ' , Billing_Country__c ' +
                            ' , Billing_Email__c ' +
                            ' , Billing_Name__c ' +
                            ' , Billing_Phone__c ' +
                            ' , Billing_State__c ' +
                            ' , Billing_Zip__c ' +
                            ' , Card_Holder_Name__c ' +
                            ' , Card_Name__c ' +
                            ' , Currency__c ' +
                            ' , ECI_Value__c ' +
                            ' , Merchant_Amount__c ' +
                            ' , Order_Status__c ' +
                            ' , Payment_Mode__c ' +
                            ' , Payment_Type__c ' +
                            ' , Guest_Payment_Type__c ' +
                            ' , Booking_Unit__c ' +
                            ' , Booking_Unit__r.Building_Name__c ' +
                            ' , Guest_Other_Payment_Type__c ' +
                            ' , Status_Code__c ' +
                            ' , Status_Message__c ' +
                            ' , Receipt_created_in_IPMS__c ' +
                            ' , ( SELECT  Id ' +
                                        ' , Name ' +
                                        ' , Booking_Unit__c ' +
                                        ' , Registration_Id__c ' +
                                        ' , Unit_Name__c ' +
                                        //, Is_Next_Q1_Invoice__c
                                        //, TRX_Id__c
                                ' FROM    FM_Unit_Invoice_Payments__r ' +
                            ')' +
                'FROM       FM_Receipt__c ' +
                ' WHERE      (CreatedDate >= ' + this.startDateTimeStamp + ') ' +
                            'AND (Payment_Type__c = \'FM Guest Payment\' ' +
                                    'OR (Payment_Type__c = \'' + Label.FMPortalServiceCharge
                                    + '\' AND CreatedDate >= ' + this.startDateTimeStamp + ')) ' +
                            'AND ((Order_Status__c = NULL OR Order_Status__c = \'\' )' +
                                'OR (Receipt_created_in_IPMS__c != \'TRUE\' ' +
                                    'AND Order_Status__c IN :successOrderStatus ))' +
                ' ORDER BY   CreatedDate DESC '/* +
                'LIMIT      1'*/;
        System.debug('pQuery = ' + query);
    }

    global FmPaymentStatusCheckBatch(Boolean overrideQuery, String pQuery) {
        query = pQuery;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<FM_Receipt__c> lstFmReceipt = (List<FM_Receipt__c>) scope;
        if (lstFmReceipt.size() > 1) {
            return;
        }
        FM_Receipt__c fmReceipt = lstFmReceipt[0];

        try {
            checkAndUpdateReceipt(fmReceipt);
        } catch(Exception excp) {
            insert new Error_Log__c(
                Process_Name__c = 'Update Receipt',
                Error_Details__c = fmReceipt.Name + ': '
                                    + 'Exception Type: ' + excp.getTypeName()
                                    + '\nLine Number: ' +  excp.getLineNumber()
                                    + '\nStack Trace: ' + excp.getStackTraceString()
                                    + '\nCause: ' + excp.getCause()
                                    + '\nError Message: ' + excp.getMessage()
            );
        }

    }

    private void checkAndUpdateReceipt(FM_Receipt__c fmReceipt) {
        String orderStatusResponse = getReceiptStatus(fmReceipt.Name);

        CCAvenuePaymentGateway.OrderStatusResult orderStatusResult = deserializeResponse(
            fmReceipt.Name, orderStatusResponse
        );

        if (orderStatusResult == NULL) {
            return;
        }

        fmReceipt = updateReceiptStatus(fmReceipt, orderStatusResult);

        if ((Label.FMPortalServiceCharge.equalsIgnoreCase(fmReceipt.Payment_Type__c)
            || ('FM Guest Payment'.equalsIgnoreCase(fmReceipt.Payment_Type__c)
                && ('Service Charges'.equalsIgnoreCase(fmReceipt.Guest_Payment_Type__c)
                    || String.isBlank(fmReceipt.Guest_Payment_Type__c))))
            && 'Approved'.equalsIgnoreCase(fmReceipt.Status_Message__c)
            && fmReceipt.Receipt_created_in_IPMS__c != 'TRUE'
        ) {
            //createReceiptInIpms(fmReceipt);
        }

        List<Database.SaveResult> lstSaveResult = Database.update(new List<FM_Receipt__c>{fmReceipt}, false);
        for (Database.SaveResult sr : lstSaveResult) {
            if (sr.isSuccess()) {
                fmReceiptResponse.put(fmReceipt.Name, orderStatusResponse);
                continue;
            }
            String fmReceiptError = failedReceipts.get(fmReceipt.Name);
            fmReceiptError = String.isBlank(fmReceiptError) ? '' : fmReceiptError + '\n';
            failedReceipts.put(fmReceipt.Name, fmReceiptError + sr.getId() + ':\n\n' + sr.getErrors());
        }
    }

    private static String getReceiptStatus(String orderNo) {
        return CCAvenuePaymentGateway.getInstance(
            LoamsCommunityController.GATEWAY_NAME
        ).trackOrderStatus(orderNo);
    }

    private CCAvenuePaymentGateway.OrderStatusResult deserializeResponse(String orderNo, String orderStatusResponse) {
        CCAvenuePaymentGateway.OrderStatusResult orderStatusResult;
        try {
            orderStatusResult = (CCAvenuePaymentGateway.OrderStatusResult) JSON.deserialize(
                orderStatusResponse, CCAvenuePaymentGateway.OrderStatusResult.class
            );
        } catch(Exception e) {
            failedReceipts.put(orderNo, e.getMessage() + '\n\n' + orderStatusResponse);
        }
        if (orderStatusResult != NULL
            && (orderStatusResult.status == NULL
                || Decimal.valueOf(orderStatusResult.status) != 0
            )
        ) {
            failedReceipts.put(orderNo, orderStatusResponse);
            orderStatusResult = NULL;
        }
        return orderStatusResult;
    }

    private static FM_Receipt__c updateReceiptStatus(
        FM_Receipt__c fmReceipt, CCAvenuePaymentGateway.OrderStatusResult orderStatusResult
    ) {
        //fmReceipt.Acknowledgement_Id__c       = orderStatusResult.acking_id;
        fmReceipt.Bank_Receipt_Number__c      = orderStatusResult.order_receipt_no;
        fmReceipt.Bank_Reference_Number__c    = orderStatusResult.order_bank_ref_no;
        fmReceipt.Billing_Address__c          = orderStatusResult.order_bill_address;
        fmReceipt.Billing_City__c             = orderStatusResult.order_bill_city;
        fmReceipt.Billing_Country__c          = orderStatusResult.order_bill_country;
        fmReceipt.Billing_Email__c            = orderStatusResult.order_bill_email;
        fmReceipt.Billing_Name__c             = orderStatusResult.order_bill_name;
        fmReceipt.Billing_Phone__c            = orderStatusResult.order_bill_tel;
        fmReceipt.Billing_State__c            = orderStatusResult.order_bill_state;
        fmReceipt.Billing_Zip__c              = orderStatusResult.order_bill_zip;
        //fmReceipt.Card_Holder_Name__c         = orderStatusResult.card_holder_name;
        fmReceipt.Card_Name__c                = orderStatusResult.order_card_name;
        fmReceipt.Currency__c                 = orderStatusResult.order_currncy;
        fmReceipt.ECI_Value__c                = orderStatusResult.eci_value;
        fmReceipt.Order_Number__c             = orderStatusResult.order_no;
        fmReceipt.Order_Status__c             = orderStatusResult.order_status;
        fmReceipt.Payment_Mode__c             = orderStatusResult.order_card_type;
        fmReceipt.Status_Code__c              = orderStatusResult.status;
        fmReceipt.Status_Message__c           = orderStatusResult.order_bank_response;
        //fmReceipt.Payment_Type__c             = orderStatusResult.merchant_param1;

        return fmReceipt;
    }

    /*private Boolean createReceiptInIpms(FM_Receipt__c fmReceipt) {
        FmIpmsRestServices.CreateReceiptRequest request = new FmIpmsRestServices.CreateReceiptRequest();
        request.sourceSystem = 'SFDC';
        request.extRequestNumber = fmReceipt.Name;
        request.businessGroup = 'FM';
        request.partyId = fmReceipt.Customer_Party_Id__c;
        request.receiptNumber = fmReceipt.Name;
        request.paidAmount = fmReceipt.Amount__c;
        request.currencyCode = 'AED';
        request.applyReceipt = 'Yes';

        FmIpmsRestServices.ApplicationLine applicationLine = new FmIpmsRestServices.ApplicationLine();
        applicationLine.registrationId = String.isNotBlank(fmReceipt.Registration_Id__c)
                                            ? fmReceipt.Registration_Id__c : NULL;
        applicationLine.amountToApply = fmReceipt.Amount__c;
        request.applicationLines = new List<FmIpmsRestServices.ApplicationLine> {applicationLine};

        //if (Label.AdvanceServiceChargePayment.equalsIgnoreCase(fmReceipt.Discount_Applied__c)) {
        //    request.customerTrxId = fmReceipt.Bank_Reference_Number__c;
        //    request.discountAttribute = '10% Discount';
        //}

        Set<String> registrationIds = new Set<String>();
        Set<String> unitNames = new Set<String>();
        if (String.isNotBlank(fmReceipt.Registration_Id__c)) {
            registrationIds.add(fmReceipt.Registration_Id__c);
            unitNames.add(fmReceipt.Unit_Name__c);
        }
        String customerTrxId;
        for (FM_Unit_Invoice_Payment__c unitInvoice : fmReceipt.FM_Unit_Invoice_Payments__r) {
            registrationIds.add(unitInvoice.Registration_Id__c);
            unitNames.add(fmReceipt.Unit_Name__c);

            //if (Label.AdvanceServiceChargePayment.equalsIgnoreCase(fmReceipt.Discount_Applied__c)
            //    && unitInvoice.Is_Next_Q1_Invoice__c
            //) {
            //    customerTrxId = unitInvoice.TRX_Id__c;
            //}
        }

        String unitName = '';
        for (String uName : unitNames) {
            unitName += uName + ',';
        }
        unitName = unitName.removeEnd(',');

        request.comments = (Label.FMGuestPayment.equalsIgnoreCase(fmReceipt.Payment_Type__c)
                                ? 'Guest ' : 'Portal ') + 'Payment for Service Charges for ' + unitName;

        //if (Label.AdvanceServiceChargePayment.equalsIgnoreCase(fmReceipt.Discount_Applied__c)) {
        //    request.customerTrxId = customerTrxId;
        //}
        request.applicationLines = new List<FmIpmsRestServices.ApplicationLine>();
        for (String regId : registrationIds) {
            FmIpmsRestServices.ApplicationLine line = new FmIpmsRestServices.ApplicationLine();
            line.registrationId = regId;
            line.amountToApply = fmReceipt.Amount__c;
            request.applicationLines.add(line);
        }

        if (!LoamsPaymentStatusController.OTHERS.equalsIgnoreCase(fmReceipt.Guest_Payment_Type__c)) {
            fmReceipt.IpmsReceiptUrl__c = FmIpmsRestServices.createReceipt(request);
        }

        if (String.isNotBlank(fmReceipt.IpmsReceiptUrl__c)) {
            fmReceipt.Receipt_created_in_IPMS__c = 'TRUE';
            if (Label.FMGuestPayment.equalsIgnoreCase(fmReceipt.Payment_Type__c)) {
                LoamsPaymentStatusController.emailReceiptAndSoaSync(fmReceipt.Id, fmReceipt.IpmsReceiptUrl__c);
            }
            return true;
        } else {
            fmReceipt.Receipt_created_in_IPMS__c = 'FALSE';
            LoamsPaymentStatusController.emailReceiptAndSoaSync(fmReceipt.Id, '');
            String fmReceiptError = failedReceipts.get(fmReceipt.Name);
            fmReceiptError = String.isBlank(fmReceiptError) ? '' : fmReceiptError + '\n';
            failedReceipts.put(fmReceipt.Name, fmReceiptError + 'IPMS Receipt Creation Failed');

            return false;
        }
    }*/

    global void finish(Database.BatchableContext BC) {
        if (fmReceiptResponse.isEmpty() && failedReceipts.isEmpty()) {
            return;
        }

        String emailBody = 'Hello ' + UserInfo.getFirstName() + ',\n\n\n' +
                            + 'Please find the FM Receipt Payment status update result below:\n\n\n';

        if (!fmReceiptResponse.isEmpty()) {
            emailBody += 'Successfully updated FM Receipts:\n\n';
            for (String fmReceiptName : fmReceiptResponse.keySet()) {
                emailBody += fmReceiptName + '\n';
            }
            emailBody += '\n\n';
        }

        if (!failedReceipts.isEmpty()) {
            emailBody += 'FM Receipts failed to update Status:\n\n';
            for (String fmReceiptName : failedReceipts.keySet()) {
                emailBody += fmReceiptName + '\n Error:\n' + failedReceipts.get(fmReceiptName) + '\n\n';
            }
            emailBody += '\n\n';
        }

        emailBody += '\n\nThank You,\n Community Portal';

        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.setOrgWideEmailAddressId(Label.CommunityPortalOrgWideAddressId);
        message.toAddresses = new String[] { UserInfo.getUserEmail() };
        message.subject = 'FM Receipt Payment Status Update Job';
        message.plainTextBody = emailBody;
        Messaging.SendEmailResult[] results = Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {message});

        try {
            insert new Error_Log__c(
                Process_Name__c = 'Update Receipt',
                Error_Details__c = emailBody
            );
        } catch(Exception e) {
            System.debug(e.getMessage());
        }
    }

    global void execute(SchedulableContext sc) {
        Database.executeBatch(new FmPaymentStatusCheckBatch(startDateTimeStamp), 1);
    }

}