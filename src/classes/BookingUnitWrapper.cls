public class BookingUnitWrapper {
    public RETRIEVE_Input RETRIEVE_Input { get; set; }
    
    public BookingUnitWrapper() {
        this.RETRIEVE_Input = new RETRIEVE_Input();
    }

	public class RETRIEVE_Input {
		public RESTHeader RESTHeader;
		public InputParameters InputParameters;
        public RETRIEVE_Input(){
            this.RESTHeader = new RESTHeader();
            this.InputParameters = new InputParameters();
        }
        public void convertToWrapper(string paramId) {
           this.InputParameters.P_REQUEST_MESSAGE.PARAM_ID = paramId;            
        }
	}

	public class P_REQUEST_MESSAGE {
        public String PARAM_ID{get;set;}
        public P_REQUEST_MESSAGE(){
            this.PARAM_ID = '';
        }
	}

	public class RESTHeader {
		public String Responsibility;
		public String RespApplication;
		public String SecurityGroup;
		public String NLSLanguage;
        public RESTHeader(){
            this.Responsibility = 'ONT_ICP_SUPER_USER';
            this.RespApplication = 'ONT';
            this.SecurityGroup = 'STANDARD';
            this.NLSLanguage = 'AMERICAN';
        }
	}

	public class InputParameters {
        public P_REQUEST_MESSAGE P_REQUEST_MESSAGE{get;set;}
		public String P_REQUEST_NUMBER;
		public String P_SOURCE_SYSTEM;
		public String P_REQUEST_NAME;
       
        public InputParameters(){
            this.P_REQUEST_MESSAGE = new P_REQUEST_MESSAGE();
            this.P_REQUEST_NUMBER = 'A1001';
            this.P_SOURCE_SYSTEM = 'SFDC';
            this.P_REQUEST_NAME ='GET_REG_MILESTONE_DETAILS';
           
        }
	}
    
    //response wrapper
    public class ResponseWrapper{
        public OutputParameters OutputParameters {get;set;}
        public ResponseWrapper(){
            this.OutputParameters = new OutputParameters();
        }
    }
    public class OutputParameters {
		public X_RESPONSE_MESSAGE X_RESPONSE_MESSAGE;
		public String X_RETURN_STATUS;
		public String X_RETURN_MESSAGE;
         
	}

	//public OutputParameters OutputParameters;

	public class X_RESPONSE_MESSAGE_ITEM {
		public String ATTRIBUTE1;
		public Object ATTRIBUTE2;
		public String ATTRIBUTE3;
		public String ATTRIBUTE4;
		public String ATTRIBUTE5;
		public String ATTRIBUTE6;
		public String ATTRIBUTE7;
		public String ATTRIBUTE8;
		public String ATTRIBUTE9;
		public String ATTRIBUTE10;
		public String ATTRIBUTE11;
		public String ATTRIBUTE12;
		public String ATTRIBUTE13;
		public String ATTRIBUTE14;
		public Object ATTRIBUTE15;
		public Object ATTRIBUTE16;
		public Object ATTRIBUTE17;
		public Object ATTRIBUTE18;
		public Object ATTRIBUTE19;
		public Object ATTRIBUTE20;
		public Object ATTRIBUTE21;
		public Object ATTRIBUTE22;
		public Object ATTRIBUTE23;
		public Object ATTRIBUTE24;
		public Object ATTRIBUTE25;
		public Object ATTRIBUTE26;
		public Object ATTRIBUTE27;
		public Object ATTRIBUTE28;
		public Object ATTRIBUTE29;
		public Object ATTRIBUTE30;
		public Object ATTRIBUTE31;
		public Object ATTRIBUTE32;
		public Object ATTRIBUTE33;
		public Object ATTRIBUTE34;
		public Object ATTRIBUTE35;
		public Object ATTRIBUTE36;
		public Object ATTRIBUTE37;
		public Object ATTRIBUTE38;
		public Object ATTRIBUTE39;
		public Object ATTRIBUTE40;
	}

	public class X_RESPONSE_MESSAGE {
		public List<X_RESPONSE_MESSAGE_ITEM> X_RESPONSE_MESSAGE_ITEM;
	}
   
	public static BookingUnitWrapper parse(String json) {
		return (BookingUnitWrapper) System.JSON.deserialize(json, BookingUnitWrapper.class);
	}
    
}