@isTest
public with sharing class AP_ContactDetailsFooterControllerTest {
	
		public static User portalUser;
		public static Contact adminContact;
		public static Account adminAccount;

		@isTest
		public static void testFetchSubCatAndArticlesOnLoad1() {
			
			adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
			//adminAccount.Secondary_Owner__c = usr.id;
			insert adminAccount;
			
			adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
			adminContact.Owner__c = true;
			adminContact.Agent_Representative__c= true;
			adminContact.Portal_Administrator__c= true;
			insert adminContact;

			portalUser = InitialiseTestData.getPortalUser('t11t@test.com', adminContact.Id, 'Admin');

			//Create AP_Knowledge__kav
			AP_Knowledge__kav article = new AP_Knowledge__kav(Summary='xyz',Urlname='xyz');
			article.Title = 'Test Title';
			article.Description__c = 'Test Description';
			article.Language = 'en_US';
			insert article;

			

			

			AP_Knowledge__kav newArticle = [SELECT
							Id, Title, KnowledgeArticleId
							From
							AP_Knowledge__kav
							WHERE
							Id =: article.Id
							][0];

			list<KnowledgeArticleViewStat> knowArtObj = new list<KnowledgeArticleViewStat>();
			knowArtObj = [SELECT
                                    Id,NormalizedScore,Parent.Id,ViewCount
                                FROM KnowledgeArticleViewStat
                                WHERE  Parent.Id = :article.id AND Channel = 'Csp' ORDER BY
                                NormalizedScore Desc Limit 4];

			//Publishing Article
			if(newArticle.KnowledgeArticleId != null ) {
			KbManagement.PublishingService.publishArticle(newArticle.KnowledgeArticleId, true);
			}
			DescribeDataCategoryGroupResult[] results = Schema.describeDataCategoryGroups(
			new String[] { 'KnowledgeArticleVersion'}
			);
			
			//Associating data category to knowledge article
			AP_Knowledge__DataCategorySelection dataCat = new AP_Knowledge__DataCategorySelection ();
			System.debug('article.Id - ' + article.Id);
			dataCat.DataCategoryGroupName = results[0].getName();
			dataCat.DataCategoryName = 'FAQ';
			dataCat.ParentId = newArticle.Id;
			insert dataCat;
			//List<ChildCategory> childCategories = new List<ChildCategory>();
			//List<TopCategory> ptopCategories =  new List<TopCategory> ();
			Test.startTest();
			//system.runas(portalUser) {
				AP_ContactDetailsFooterController controller = new AP_ContactDetailsFooterController();
				AP_ContactDetailsFooterController.myObject myObj = new AP_ContactDetailsFooterController.myObject('Test','Test');
				DataCategoryUtil data = new DataCategoryUtil();
				DataCategoryUtil.ChildCategory childCat = new DataCategoryUtil.ChildCategory(null,'','');
				DataCategoryUtil.TopCategory topCategory = new DataCategoryUtil.TopCategory(null,'','');
				DataCategoryUtil.RootObject rootObject = new DataCategoryUtil.RootObject('',null,'','');
				controller.fetchSubCatAndArticlesOnLoad();
			//}
			Test.stopTest();
			
		}
		@isTest
		public static void testFetchSubCatAndArticlesOnLoad2() {
			
			Test.startTest();
			//system.runas(portalUser) {
				AP_ContactDetailsFooterController controller = new AP_ContactDetailsFooterController();
				AP_ContactDetailsFooterController.myObject myObj = new AP_ContactDetailsFooterController.myObject('Test','Test');
			//}
			Test.stopTest();
			
		}

		@isTest
		public static void testGetDataCategoriesStructure() {
			Test.startTest();
			
			AP_ContactDetailsFooterController controller = new AP_ContactDetailsFooterController();
			
			Test.stopTest();

			DescribeDataCategoryGroupResult[] results = Schema.describeDataCategoryGroups(
			new String[] { 'KnowledgeArticleVersion'}
			);

			////system.assertEquals(results.size(), controller.parentCatList.size());
		} 
		@isTest
		public static void testFetchSubCatAndArticlesOnLoad3() {
			
			adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
			//adminAccount.Secondary_Owner__c = usr.id;
			insert adminAccount;
			
			adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
			adminContact.Owner__c = true;
			adminContact.Agent_Representative__c= true;
			adminContact.Portal_Administrator__c= true;
			insert adminContact;

			portalUser = InitialiseTestData.getPortalUser('t11t@test.com', adminContact.Id, 'Admin');

			//Create AP_Knowledge__kav
			AP_Knowledge__kav article = new AP_Knowledge__kav(Summary='xyz',Urlname='xyz');
			article.Title = 'Test Title';
			article.Description__c = 'Test Description';
			article.Language = 'en_US';
			insert article;

			

			

			AP_Knowledge__kav newArticle = [SELECT
							Id, Title, KnowledgeArticleId
							From
							AP_Knowledge__kav
							WHERE
							Id =: article.Id
							][0];

			list<KnowledgeArticleViewStat> knowArtObj = new list<KnowledgeArticleViewStat>();
			knowArtObj = [SELECT
                                    Id,NormalizedScore,Parent.Id,ViewCount
                                FROM KnowledgeArticleViewStat
                                WHERE  Parent.Id = :article.id AND Channel = 'Csp' ORDER BY
                                NormalizedScore Desc Limit 4];

			//Publishing Article
			if(newArticle.KnowledgeArticleId != null ) {
			KbManagement.PublishingService.publishArticle(newArticle.KnowledgeArticleId, true);
			}
			DescribeDataCategoryGroupResult[] results = Schema.describeDataCategoryGroups(
			new String[] { 'KnowledgeArticleVersion'}
			);
			
			//Associating data category to knowledge article
			AP_Knowledge__DataCategorySelection dataCat = new AP_Knowledge__DataCategorySelection ();
			System.debug('article.Id - ' + article.Id);
			dataCat.DataCategoryGroupName = results[0].getName();
			dataCat.DataCategoryName = 'FAQ';
			dataCat.ParentId = newArticle.Id;
			insert dataCat;
			//List<ChildCategory> childCategories = new List<ChildCategory>();
			//List<TopCategory> ptopCategories =  new List<TopCategory> ();
			Test.startTest();
			system.runas(portalUser) {
				AP_ContactDetailsFooterController controller = new AP_ContactDetailsFooterController();
				AP_ContactDetailsFooterController.myObject myObj = new AP_ContactDetailsFooterController.myObject('Test','Test');
				
			}
			Test.stopTest();
			
		}
}