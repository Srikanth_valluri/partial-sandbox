@isTest
public class AOPTMoreInfoTaskCloserValidationTest{
    private static testmethod void taskClosure(){
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('AOPT').getRecordTypeId();

        //insert sample data for Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , recTypeId);
        objCase.Status = 'Submitted';
        insert objCase;

        objCase.OwnerId = UserInfo.getUserId();
        update objCase;
        
    
        Task objTask7 = TaskUtility.getTask((SObject)objCase, 'Offer and Acceptance Letter Verification Required', 'CRE', 
                                'AOPT', system.today().addDays(1));
        objTask7.OwnerId = UserInfo.getUserId();    
        insert objTask7;

        Task objTask8 = TaskUtility.getTask((SObject)objCase, 'Provide Missing details', 'CRE', 
                                'AOPT', system.today().addDays(1));
        objTask8.OwnerId = UserInfo.getUserId();
        objTask8.Parent_Task_Id__c = objTask7.Id;
        insert objTask8;
        
        try{
            objTask8.Status = 'Completed';
            update objTask8;
            AOPTMoreInfoTaskCloserValidation.createTaskIPMSAgain(new List<Task>{objTask8});
        }catch(Exception ex){
        
        }
    }
}