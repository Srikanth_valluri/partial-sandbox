@RestResource(urlMapping='/PayfortTransactionFeedback')
global class PayfortTransactionFeedbackService {

    @HttpPost
    global static void getPayfortTransactionFeedback() {

        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);
        //System.debug('JSON request: '+r.requestBody);
        //System.debug('JSON request str: '+r.requestBody.toString() );

        Map<String, String> mainMap = new Map<String, String>();
        List<String> lstKeyValues = new List<String>();

        for(String objStr : r.params.keySet()) {
            mainMap.put(objStr,String.valueOf(r.params.get(objStr)));

            String objKeyValue = objStr+'='+String.valueOf(r.params.get(objStr));
            System.debug('objStr: '+objKeyValue);
            lstKeyValues.add(objKeyValue);
        }

        System.debug('mainMap: '+mainMap);
        System.debug('lstKeyValues: '+lstKeyValues);
        if(lstKeyValues.size() > 0) {
            lstKeyValues.sort();
            boolean doUpdateReceipt = ValidateSignatureForResponse(lstKeyValues, mainMap.get('signature')); 

            System.debug('doUpdateReceipt : ' + doUpdateReceipt);
            if(doUpdateReceipt) {
                StampTransactionDataonReceipt(mainMap);
            }
        }


        RestResponse res = RestContext.response;
        res.responseBody = Blob.valueOf('Success');
        res.statusCode = 200;
    } 

    private static boolean ValidateSignatureForResponse(List<String> keyValuePairs, String signature) {

        //getting required data from CS
        PayfortGatewayCreds__c objCreds = PayfortGatewayCreds__c.getInstance();
        System.debug('objCreds: '+objCreds);

        String PAYFORT_RESPONSE_PHRASE = objCreds.ResponsePhrase__c;

        String signPattern ='';
        if(keyValuePairs.size() > 0) {
            signPattern += PAYFORT_RESPONSE_PHRASE; 
             for(String objStr : keyValuePairs) {
                if(!objStr.containsIgnoreCase('signature=')) {
                    System.debug('objStr: '+objStr);
                    String decodedStr = EncodingUtil.urlDecode(objStr, 'UTF-8');
                    System.debug('decodedStr: '+decodedStr);
                    signPattern += decodedStr;
                }
            }
            signPattern += PAYFORT_RESPONSE_PHRASE;
        }

        System.debug('signPattern generated : '+signPattern);
        String algorithmName = 'SHA-256';
        Blob hmacData = Crypto.generateDigest(algorithmName, Blob.valueOf(signPattern));
        System.debug('signature generated at our end :'+EncodingUtil.convertToHex(hmacData));
        System.debug('sign from payfort : '+ signature);

        return EncodingUtil.convertToHex(hmacData) == signature ? true : false;
    }

    public static void StampTransactionDataonReceipt(Map<String, String> mainMap) {

        List<Receipt__c> lstReceipt = new List<Receipt__c>();

        if(!Test.isRunningTest()) {
            lstReceipt = [SELECT id
                               , Payfort_FortID__c
                               , Payfort_Issuer_Code__c
                               , Payfort_No_of_Installments__c
                               , Payfort_Payment_type__c
                               , Payfort_Plan_Code__c
                               , Payment_Status__c
                               , PG_Error_Code__c
                               , Card_Type__c
                               , PG_Error_Message__c   
                          FROM Receipt__c
                          WHERE Name =: mainMap.get('merchant_reference')];
        }
        else { // When running in Test class context
            lstReceipt = [SELECT id
                              , isPayfortPayment__c
                              , Payfort_FortID__c
                              , Payfort_Issuer_Code__c
                              , Payfort_No_of_Installments__c
                              , Payfort_Payment_type__c
                              , Payfort_Plan_Code__c
                              , Payment_Status__c
                              , PG_Error_Code__c
                              , Card_Type__c
                              , PG_Error_Message__c   
                          FROM Receipt__c
                          WHERE isPayfortPayment__c = true];
        }
       

        System.debug('lstReceipt : ' + lstReceipt );
        if(lstReceipt.size() > 0 && (!lstReceipt[0].Payment_Status__c.equalsIgnoreCase('Success') || String.isBlank(lstReceipt[0].Payment_Status__c))) {

            lstReceipt[0].Direct_Transaction_Feedback_Time__c = DateTime.now();
            lstReceipt[0].isPayfortDTFUpdate__c = true;

            lstReceipt[0].Payfort_FortID__c = mainMap.containsKey('fort_id') ? mainMap.get('fort_id') : lstReceipt[0].Payfort_FortID__c;
            lstReceipt[0].PG_Error_Code__c = mainMap.containsKey('status') ? mainMap.get('status') : lstReceipt[0].PG_Error_Code__c;

            if(mainMap.get('response_message').equalsIgnoreCase('Success')) {
                    //for successfull transaction
                    lstReceipt[0].Payment_Status__c = mainMap.get('response_message');
            }
            else {
                //for failed transaction
                lstReceipt[0].Payment_Status__c = 'Failed';
                lstReceipt[0].PG_Error_Message__c = mainMap.get('response_message');
            }

            if(mainMap.containsKey('installments')) {
                    lstReceipt[0].Payfort_Payment_type__c = 'Installment';
                    lstReceipt[0].Payfort_No_of_Installments__c = Decimal.valueOf(mainMap.get('number_of_installments'));
                    lstReceipt[0].Payfort_Plan_Code__c = mainMap.get('plan_code');
                    lstReceipt[0].Payfort_Issuer_Code__c = mainMap.get('issuer_code');
            }
            else {
                lstReceipt[0].Payfort_Payment_type__c = 'One-Time Payment';
            }

            System.debug('Beforing updating Receipt : '+lstReceipt[0]);
            update lstReceipt;
        }   
    }
}