public class BookingUnitSoaLangButtonController {

    public Id recordId {get; set;}
    public String responseUrl {get; set;}
    public String accessToken {get; set;}
    public String pdfFile {get; set;}
    public Blob pdfBlob {get; set;}

    public BookingUnitSoaLangButtonController(ApexPages.StandardController controller) {
    	recordId = controller.getId();
        System.debug('Booking Unit Id in consturctor: '+recordId);
    }

    public PageReference getUnitSoaInLanguageUrl() {
        FmIpmsRestCoffeeServices.updateCustomSetting = false;

        Booking_Unit__c bookingUnit = [SELECT Id, Registration_ID__c
                          			   FROM Booking_Unit__c
                           			   WHERE Id =: recordId];
        String registrationId = bookingUnit.Registration_ID__c;
        System.debug('Registration Id is: '+registrationId);

        responseUrl = FmIpmsRestCoffeeServices.getUnitSoaInLanguage(registrationId);
        System.debug('responseUrl is: '+responseUrl);

        if(String.isBlank(responseUrl) || responseUrl == null || responseUrl == 'Null') {
            System.debug('In ApexMessage Method');
            ApexPages.Message msg = new ApexPages.Message(ApexPages.SEVERITY.ERROR, 'There was an error processing your request');
            ApexPages.addMessage(msg);
            return null;
            //ApexPages.addMessages(new ApexPages.Messages(ApexPages.SEVERITY.ERROR, 'Invalid RegistrationId'));
        }

        //Http callout

        accessToken = FmIpmsRestCoffeeServices.accessToken;
        System.debug('AccessToken is' + accessToken);

        HttpRequest req = new HttpRequest();
        req.setEndpoint(responseUrl);
        req.setHeader('Accept', 'application/json');
        req.setMethod('GET');
        req.setHeader('Authorization','Bearer' + accessToken);
        req.setTimeout(120000);
        HttpResponse response = new Http().send(req);
        System.debug('Status Code = ' + response.getStatusCode());
        System.debug('Pdf Body = ' + response.getBodyAsBlob());


        if(response.getStatusCode() == 200) {
            pdfBlob =  response.getBodyAsBlob();
            pdfFile = EncodingUtil.base64Encode(pdfBlob);
        }

        FmIpmsRestCoffeeServices.updateCustomSetting = true;
        FmIpmsRestCoffeeServices.updateBearerToken();      //calling updateMethod of Custom Setting after all callouts execution

        return null;
    }

}