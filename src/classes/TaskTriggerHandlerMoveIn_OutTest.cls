/*-------------------------------------------------------------------------------------------------
Description: Test class for TaskTriggerHandlerMoveIn_Out

============================================================================================================================
    Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
    1.0     | 01-08-2018       | Lochana Rajput   | 1. Added methods to test validation rules
=============================================================================================================================
*/
@isTest
private class TaskTriggerHandlerMoveIn_OutTest {

	@isTest static void test_move_in_date_rule() {
		Id recTypeId=Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Move In').getRecordTypeId();
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Task_Id__c=123;
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Change_of_Contact_Details';
        fmCaseObj.Email__c='a@gmail.com';
        fmCaseObj.Mobile_no__c='12121212';
        fmCaseObj.RecordTypeId=recTypeId;
		fmCaseObj.Submitted__c = true;
        insert fmCaseObj;
		Task objTask = new Task();
		objTask.Subject = Label.Move_in_date_task;
		objTask.WhatId = fmCaseObj.Id;
		insert objTask;
		objTask.Status = 'Completed';
		Test.startTest();
		update objTask;
		Test.stopTest();
	}

	@isTest static void test_move_out_date_rule() {
		Id recTypeId=Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Move Out').getRecordTypeId();
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Task_Id__c=123;
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Change_of_Contact_Details';
        fmCaseObj.Email__c='a@gmail.com';
        fmCaseObj.Mobile_no__c='12121212';
        fmCaseObj.RecordTypeId=recTypeId;
		fmCaseObj.Submitted__c = true;
        insert fmCaseObj;
		Task objTask = new Task();
		objTask.Subject = Label.Update_actual_move_out_date;
		objTask.WhatId = fmCaseObj.Id;
		insert objTask;
		objTask.Status = 'Completed';
		Test.startTest();
		update objTask;
		Test.stopTest();
	}

	@isTest static void test_checkEmailPhoneOnCallingList() {

		List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
		TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
		                                                             OnOffCheck__c = true);
		settingLst2.add(newSetting1);
		insert settingLst2;

		Skip_Task_Trigger_for_Informatica__c settingLst1 = new Skip_Task_Trigger_for_Informatica__c();
		settingLst1.SetupOwnerId=Userinfo.getUserId();
		settingLst1.Enable__c = true;
		insert settingLst1;

		Id callLstRecType = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('COD for Sales').getRecordTypeId();

		Test.startTest();
		Calling_List__c objCall = new Calling_List__c(RecordTypeId = callLstRecType);
		insert objCall;

		Task tsk = new Task( WhatId = objCall.Id, Subject='Incorrect Contact Details', Process_Name__c = 'Welcome Call', Status = 'Not Started');
		insert tsk;

		try{
			tsk.Status = 'Completed';
			update tsk;
		}
		catch(Exception e) {
			Boolean expectedExceptionThrown =  e.getMessage().contains('Please update Phone/Email on calling list before closing the task!!') ? true : false;
			System.AssertEquals(expectedExceptionThrown, true);
		}
		

		Test.stopTest();

	}

	@isTest static void test_checkEmailPhoneOnCallingList2() {

		List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
		TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
		                                                             OnOffCheck__c = true);
		settingLst2.add(newSetting1);
		insert settingLst2;

		Skip_Task_Trigger_for_Informatica__c settingLst1 = new Skip_Task_Trigger_for_Informatica__c();
		settingLst1.SetupOwnerId=Userinfo.getUserId();
		settingLst1.Enable__c = true;
		insert settingLst1;

		Id callLstRecType = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('COD for Sales').getRecordTypeId();

		Test.startTest();
		Calling_List__c objCall = new Calling_List__c(RecordTypeId = callLstRecType, Email_1__c = 'test@test.com');
		insert objCall;

		Task tsk = new Task( WhatId = objCall.Id, Subject='Incorrect Contact Details', Process_Name__c = 'Welcome Call', Status = 'Not Started');
		insert tsk;

		try{
			tsk.Status = 'Completed';
			update tsk;
		}
		catch(Exception e) {
			Boolean expectedExceptionThrown =  e.getMessage().contains('Please update Phone/Email on calling list before closing the task!!') ? true : false;
			System.AssertEquals(expectedExceptionThrown, true);
		}
		

		Test.stopTest();

	}

	@isTest Static void test_HoProcess() {

		Skip_Task_Trigger_for_Informatica__c objCS =new Skip_Task_Trigger_for_Informatica__c();
        objCS.SetupOwnerId=Userinfo.getUserId();
        objCS.Enable__c = true;
        insert objCS;
        Id RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id; 
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);  
        
        Case objC = new Case();
        objC.Booking_Unit__c = BU.Id;
        objC.AccountId = Acc.Id;
        objC.RecordTypeId = RecordTypeId;
        objC.Status = 'New';
        objC.Origin = 'Portal';
        insert objC;
        
        Task tsk = new Task();
        tsk.WhatId = objC.Id;
        tsk.ActivityDate = System.today()+2;
        tsk.Subject = 'Subject';
        tsk.Status = 'Not Started';
        tsk.Assigned_User__c = 'Finance'; 
        tsk.Status = 'Not Started';
        tsk.Process_Name__c = 'Handover';
        insert tsk ;

        list<Task> lstTask =  new list<Task>();
        lstTask.add(tsk);
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,TaskCreationWSDL.SRDataToIPMSMultipleResponse_element>();
        TaskCreationWSDL.SRDataToIPMSMultipleResponse_element response1 = new TaskCreationWSDL.SRDataToIPMSMultipleResponse_element();
        response1.return_x = '{'+
	    ''+
	    '"message":"message",'+
	    '"status":"E"'+
	    '}';
       SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
       Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
       new TaskTriggerHandlerHandover().taskForExternalUser(lstTask);     
       Test.stopTest();
	}

	@isTest static void test_schemasDatacontractOrg200407FsiConc2_t32() {
		schemasDatacontractOrg200407FsiConc2_t32 obj = new schemasDatacontractOrg200407FsiConc2_t32();
		schemasDatacontractOrg200407FsiConc2_t32.F_IN_STEPSDto cls1 =
			new schemasDatacontractOrg200407FsiConc2_t32.F_IN_STEPSDto();
		schemasDatacontractOrg200407FsiConc2_t32.FINFILEDto cls2 =
			new schemasDatacontractOrg200407FsiConc2_t32.FINFILEDto();
		schemasDatacontractOrg200407FsiConc2_t32.ScheduledTaskTemplateDto cls3 =
			new schemasDatacontractOrg200407FsiConc2_t32.ScheduledTaskTemplateDto();


	}
	@isTest static void test_wwwFsiCoUkServicesEvolution0409() {
		wwwFsiCoUkServicesEvolution0409.Authenticate_element cls1 =
			new wwwFsiCoUkServicesEvolution0409.Authenticate_element();
		wwwFsiCoUkServicesEvolution0409.AuthenticateByAccountUserType_element cls2 =
			new wwwFsiCoUkServicesEvolution0409.AuthenticateByAccountUserType_element();
		wwwFsiCoUkServicesEvolution0409.AuthenticateByAccountUserTypeResponse_element cls3 =
			new wwwFsiCoUkServicesEvolution0409.AuthenticateByAccountUserTypeResponse_element();
		wwwFsiCoUkServicesEvolution0409.AuthenticateDatabase_element cls4 =
			new wwwFsiCoUkServicesEvolution0409.AuthenticateDatabase_element();
		wwwFsiCoUkServicesEvolution0409.AuthenticateDatabaseByAccountUserType_element cls5 =
			new wwwFsiCoUkServicesEvolution0409.AuthenticateDatabaseByAccountUserType_element();
		wwwFsiCoUkServicesEvolution0409.AuthenticateDatabaseByAccountUserTypeResponse_element cls6 =
			new wwwFsiCoUkServicesEvolution0409.AuthenticateDatabaseByAccountUserTypeResponse_element();
		wwwFsiCoUkServicesEvolution0409.AuthenticateDatabaseOutlook_element cls7 =
			new wwwFsiCoUkServicesEvolution0409.AuthenticateDatabaseOutlook_element();
		wwwFsiCoUkServicesEvolution0409.AuthenticateDatabaseOutlookResponse_element cls8 =
			new wwwFsiCoUkServicesEvolution0409.AuthenticateDatabaseOutlookResponse_element();
		wwwFsiCoUkServicesEvolution0409.AuthenticateDatabaseResponse_element cls9 =
			new wwwFsiCoUkServicesEvolution0409.AuthenticateDatabaseResponse_element();
		wwwFsiCoUkServicesEvolution0409.AuthenticateOutlook_element cls10 =
			new wwwFsiCoUkServicesEvolution0409.AuthenticateOutlook_element();
		wwwFsiCoUkServicesEvolution0409.AuthenticateOutlookResponse_element cls11 =
			new wwwFsiCoUkServicesEvolution0409.AuthenticateOutlookResponse_element();
		wwwFsiCoUkServicesEvolution0409.AuthenticateResponse_element cls12 =
			new wwwFsiCoUkServicesEvolution0409.AuthenticateResponse_element();
		wwwFsiCoUkServicesEvolution0409.LogOut_element cls13 =
			new wwwFsiCoUkServicesEvolution0409.LogOut_element();
		wwwFsiCoUkServicesEvolution0409.SecurityService cls14 =
			new wwwFsiCoUkServicesEvolution0409.SecurityService();
		wwwFsiCoUkServicesEvolution0409.SecurityService1 cls15 =
			new wwwFsiCoUkServicesEvolution0409.SecurityService1();
		wwwFsiCoUkServicesEvolution0409.LogOutResponse_element cls16 =
			new wwwFsiCoUkServicesEvolution0409.LogOutResponse_element();
			Test.setMock( WebServiceMock.class, new MaintenanceRequestMock() );
		Test.startTest();
		cls15.Authenticate('test','test');
		Test.stopTest();
	}

	@isTest static void test_wwwFsiCoUkServicesEvolution0409_t32() {
		wwwFsiCoUkServicesEvolution0409_t32.CreateBreakdownTask_element c1 =
			new wwwFsiCoUkServicesEvolution0409_t32.CreateBreakdownTask_element();
		wwwFsiCoUkServicesEvolution0409_t32.CreateBreakdownTaskResponse_element c2 =
			new wwwFsiCoUkServicesEvolution0409_t32.CreateBreakdownTaskResponse_element();
		wwwFsiCoUkServicesEvolution0409_t32.CreateInstructionSet_element c3 =
			new wwwFsiCoUkServicesEvolution0409_t32.CreateInstructionSet_element();
		wwwFsiCoUkServicesEvolution0409_t32.CreateInstructionSetResponse_element c4 =
			new wwwFsiCoUkServicesEvolution0409_t32.CreateInstructionSetResponse_element();
		wwwFsiCoUkServicesEvolution0409_t32.CreateInstructionStep_element c5 =
			new wwwFsiCoUkServicesEvolution0409_t32.CreateInstructionStep_element();
		wwwFsiCoUkServicesEvolution0409_t32.CreateInstructionStepResponse_element c6 =
			new wwwFsiCoUkServicesEvolution0409_t32.CreateInstructionStepResponse_element();
		wwwFsiCoUkServicesEvolution0409_t32.CreateScheduledTaskTemplate_element c7 =
			new wwwFsiCoUkServicesEvolution0409_t32.CreateScheduledTaskTemplate_element();
		wwwFsiCoUkServicesEvolution0409_t32.CreateScheduledTaskTemplateResponse_element c8 =
			new wwwFsiCoUkServicesEvolution0409_t32.CreateScheduledTaskTemplateResponse_element();
		wwwFsiCoUkServicesEvolution0409_t32.GetInstructionSetById_element c9 =
			new wwwFsiCoUkServicesEvolution0409_t32.GetInstructionSetById_element();
		wwwFsiCoUkServicesEvolution0409_t32.GetInstructionSetByIdResponse_element c10 =
			new wwwFsiCoUkServicesEvolution0409_t32.GetInstructionSetByIdResponse_element();
		wwwFsiCoUkServicesEvolution0409_t32.GetInstructionStepById_element c11 =
			new wwwFsiCoUkServicesEvolution0409_t32.GetInstructionStepById_element();
		wwwFsiCoUkServicesEvolution0409_t32.GetInstructionStepByIdResponse_element c12 =
			new wwwFsiCoUkServicesEvolution0409_t32.GetInstructionStepByIdResponse_element();
		wwwFsiCoUkServicesEvolution0409_t32.GetScheduledTaskTemplateById_element c13 =
			new wwwFsiCoUkServicesEvolution0409_t32.GetScheduledTaskTemplateById_element();
		wwwFsiCoUkServicesEvolution0409_t32.GetScheduledTaskTemplateByIdResponse_element c14 =
			new wwwFsiCoUkServicesEvolution0409_t32.GetScheduledTaskTemplateByIdResponse_element();
		wwwFsiCoUkServicesEvolution0409_t32.GetTaskById_element c15 =
			new wwwFsiCoUkServicesEvolution0409_t32.GetTaskById_element();
		wwwFsiCoUkServicesEvolution0409_t32.GetTaskByIdResponse_element c16 =
			new wwwFsiCoUkServicesEvolution0409_t32.GetTaskByIdResponse_element();
		wwwFsiCoUkServicesEvolution0409_t32.UpdateInstructionSet_element c17 =
			new wwwFsiCoUkServicesEvolution0409_t32.UpdateInstructionSet_element();
		wwwFsiCoUkServicesEvolution0409_t32.UpdateInstructionSetResponse_element c18 =
			new wwwFsiCoUkServicesEvolution0409_t32.UpdateInstructionSetResponse_element();
		wwwFsiCoUkServicesEvolution0409_t32.UpdateInstructionStep_element c19 =
			new wwwFsiCoUkServicesEvolution0409_t32.UpdateInstructionStep_element();
		wwwFsiCoUkServicesEvolution0409_t32.UpdateInstructionStepResponse_element c20 =
			new wwwFsiCoUkServicesEvolution0409_t32.UpdateInstructionStepResponse_element();
		wwwFsiCoUkServicesEvolution0409_t32.UpdateScheduledTaskTemplate_element c21 =
			new wwwFsiCoUkServicesEvolution0409_t32.UpdateScheduledTaskTemplate_element();
		wwwFsiCoUkServicesEvolution0409_t32.UpdateScheduledTaskTemplateResponse_element c22 =
			new wwwFsiCoUkServicesEvolution0409_t32.UpdateScheduledTaskTemplateResponse_element();
		wwwFsiCoUkServicesEvolution0409_t32.UpdateTask_element c23 =
			new wwwFsiCoUkServicesEvolution0409_t32.UpdateTask_element();
		wwwFsiCoUkServicesEvolution0409_t32.UpdateTaskResponse_element c24 =
			new wwwFsiCoUkServicesEvolution0409_t32.UpdateTaskResponse_element();
		wwwFsiCoUkServicesEvolution0409_t32.TaskServiceV3  obj =
			new wwwFsiCoUkServicesEvolution0409_t32.TaskServiceV3 ();
			Test.setMock(WebServiceMock.class, new TaskMockClass());
		obj.GetTaskById('asassc',122);
	}
	@isTest static void test_schemasDatacontractOrg200407FsiConc_t32() {
		schemasDatacontractOrg200407FsiConc_t32.BreakdownTaskDtoV3 obj = new
		schemasDatacontractOrg200407FsiConc_t32.BreakdownTaskDtoV3();
		schemasDatacontractOrg200407FsiConc_t32.TaskDtoV3 obj1 = new
		schemasDatacontractOrg200407FsiConc_t32.TaskDtoV3();
	}
	@isTest static void test_schemasDatacontractOrg200407FsiPlat() {
		schemasDatacontractOrg200407FsiPlat.AuthenticationDto obj = new
		schemasDatacontractOrg200407FsiPlat.AuthenticationDto();
	}
}