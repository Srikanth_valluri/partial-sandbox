/************************************************************************************************
 * @Name              : DAMAC_TeamsMeeting_JSON
 * @Test Class Name   : DAMAC_MSTeamsIntegration_Test
 * @Description       : JSON Class for OnlineMeeting creation response from Microsoft Graph API 
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         26/03/2020       Created
***********************************************************************************************/
public class DAMAC_TeamsMeeting_JSON{
    public String Id; //e6f12d9e-c55d-4fb1-b6d2-030cb86c9f43_19:meeting_OThhMzY0MmQtYzI5Mi00YmFkLTkwNjctZTM5YWJlNDEwYWI1@thread.v2
    public String startDateTime; //2020-03-23T10:42:03.5227483Z
    public String endDateTime; // 2020-03-23T10:42:03.5227483Z
    public String joinURL;  // https://teams.microsoft.com/l/meetup-join/19%3ameeting_OThhMzY0M/0

    public static DAMAC_TeamsMeeting_JSON parse(String json){
        return (DAMAC_TeamsMeeting_JSON) System.JSON.deserialize(json, DAMAC_TeamsMeeting_JSON.class);
    }
}