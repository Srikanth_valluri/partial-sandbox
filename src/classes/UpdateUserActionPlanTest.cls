/****************************************************************************************************
* Name          : UpdateUserActionPlanTest                                                          *
* Description   : Test class for UpdateUserActionPlan                                               *
* Created Date  : 01/04/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                *
* 1.0         Craig Lobo    01/04/2018      Initial Draft.                                          *
* 1.1         Twinkle P     20/06/2018      Updated changes as per requirement change of Action Plan*
*                                           Status                                                  *
****************************************************************************************************/
@isTest
public class UpdateUserActionPlanTest {


    public static testmethod void test1() {

        Profile profileObj = [SELECT Id FROM Profile WHERE Name = 'Property Consultant' LIMIT 1];
        User userObj = new User(
            Alias = 'stand',
            Email = 'user@testorg.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'Test',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey='en_US',
            ProfileId = profileObj.Id,
            IsActive = true,
            TimeZoneSidKey = 'America/Los_Angeles',
            UserName = 'usersfd@testorg.com'
        );
        insert userObj;

        DateTime todaysDateTime = datetime.now();
        String lastMonthName = todaysDateTime.addmonths(-1).format('MMMMM');
        String secondLastMonthName = todaysDateTime.addmonths(-2).format('MMMMM');

        Zero_Sales_User__c zsObj = new Zero_Sales_User__c(
            Name = userObj.Name,
            HOS_Descision__c = 'Pending'
        );
        insert zsObj;

        User_Action_Plan__c plan1 = new User_Action_Plan__c(
            Name = userObj.Name,
            PC__c = userObj.Id,
            Action_Plan__c = '1st to 25th',
            Zero_Sales_User__c = zsObj.Id
        );
        insert plan1;

        User_Action_Plan__c plan2 = new User_Action_Plan__c(
            Name = userObj.Name,
            PC__c = userObj.Id,
            Action_Plan__c = '1st to 25th',
            Zero_Sales_User__c = zsObj.Id,
            Target_Meetings__c = 5,
            Target_Outbound_Calls__c = 7,
            Target_Prospects_Created__c = 15
        );
        insert plan2;

        User_Action_Plan__c plan3 = new User_Action_Plan__c(
            Name = userObj.Name
        );

        List<String> planIdList = new List<String>();
        planIdList.add(plan1.Id);
        planIdList.add(plan2.Id);

        List<Task> testTaskList = new List<Task>();
        List<Inquiry__c> testInquiryList = new List<Inquiry__c>();

        Inquiry__c inqObj1 = new Inquiry__c(
            Inquiry_Source__c = 'Agent Referral',
            Mobile_Phone_Encrypt__c = '456123',
            Mobile_CountryCode__c = 'American Samoa: 001684',
            Mobile_Phone__c = '1234',
            Email__c = 'mk@gmail.com',
            First_Name__c = 'Test',
            Last_Name__c = 'Last',
            CR_Number__c = '0987',
            ORN_Number__c = '7842',
            Agency_Type__c = 'Corporate',
            Organisation_Name__c = 'Oliver',
            isDuplicate__c = false,
            OwnerId = userObj.Id
        );
        testInquiryList.add(inqObj1);

        Inquiry__c inqObj2 = new Inquiry__c(
            Inquiry_Source__c = 'Agent Referral',
            Mobile_Phone_Encrypt__c = '4561231',
            Mobile_CountryCode__c = 'American Samoa: 001684',
            Mobile_Phone__c = '12341',
            Email__c = 'mk1@gmail.com',
            First_Name__c = 'Test1',
            Last_Name__c = 'Last1',
            CR_Number__c = '09817',
            ORN_Number__c = '78421',
            Agency_Type__c = 'Corporate',
            Organisation_Name__c = 'Oliver1',
            isDuplicate__c = false,
            OwnerId = userObj.Id
        );
        testInquiryList.add(inqObj2);

        insert testInquiryList;
        NSIBPM__SR_Template__c srTemplate = InitializeSRDataTest.createSRTemplate('Deal');
        insert srTemplate;

        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);
        sr.ID_Type__c = null;
        sr.OwnerId = userObj.Id;
        sr.Agency_Type__c = 'Corporate';
        sr.agency__c=null;
        sr.NSIBPM__SR_Template__c = srTemplate.id;
        insert sr;

        Booking__c bkObj = new Booking__c();
        bkObj.Deal_SR__c = sr.id;
        bkObj.Booking_Channel__c = 'Web';
        insert bkObj;

        Location__c loc = InitializeSRDataTest.createLocation('123','Building');
        insert loc;

        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Unit_Location__c = loc.id;
        insert lstInv;

        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = bkObj.id;
        bu.Payment_Method__c = 'Cash';
        bu.Primary_Buyer_s_Email__c = 'raviteja@nsiglobal.com';
        bu.Primary_Buyer_s_Name__c = 'testNSI';
        bu.Primary_Buyer_s_Nationality__c = 'Russia';
        bu.Inventory__c = lstinv[0].id;
        bu.Registration_Status__c = 'test';
        bu.Requested_Price__c = 1500;
        bu.Registration_DateTime__c = System.today().addDays(-1);
        insert bu;

        Task meetingTask1Obj = new Task(
            WhatId = inqObj1.Id,
            ActivityDate = System.Today(),
            OwnerId = userObj.Id,
            Status = 'Completed',
            Priority = 'Normal',
            Activity_Type_3__c = 'Meeting at Office',
            Start_Date__c = System.Today(),
            End_Date__c = System.Today(),
            Description = 'Hello'
        );
        testTaskList.add(meetingTask1Obj);

        Task callTask1Obj = new Task(
            WhatId = inqObj1.Id,
            ActivityDate = System.Today(),
            OwnerId = userObj.Id,
            Status = 'Completed',
            Priority = 'Normal',
            Activity_Type_3__c = 'Call - Outbound',
            Start_Date__c = System.Today(),
            End_Date__c = System.Today(),
            Description = 'Hello'
        );
        testTaskList.add(callTask1Obj);



        Task meetingTask2Obj = new Task(
            WhatId = inqObj1.Id,
            ActivityDate = System.Today(),
            OwnerId = userObj.Id,
            Status = 'Completed',
            Priority = 'Normal',
            Activity_Type_3__c = 'Meeting at Office',
            Start_Date__c = System.Today(),
            End_Date__c = System.Today(),
            Description = 'Hello'
        );
        testTaskList.add(meetingTask2Obj);

        Task callTask2Obj = new Task(
            WhatId = inqObj1.Id,
            ActivityDate = System.Today(),
            OwnerId = userObj.Id,
            Status = 'Completed',
            Priority = 'Normal',
            Activity_Type_3__c = 'Call - Outbound',
            Start_Date__c = System.Today(),
            End_Date__c = System.Today(),
            Description = 'Hello'
        );
        testTaskList.add(callTask2Obj);

        insert testTaskList;

        Date todaysDate = System.today();
        Date createdDate1 = Date.newInstance(todaysDate.year(), todaysDate.month(), 5);
        Date createdDate2 = Date.newInstance(todaysDate.year(), todaysDate.month(), 18);
        Test.setCreatedDate(inqObj1.Id, createdDate1);
        Test.setCreatedDate(meetingTask1Obj.Id, createdDate1);
        Test.setCreatedDate(callTask1Obj.Id, createdDate1);
        Test.setCreatedDate(inqObj2.Id, createdDate2);
        Test.setCreatedDate(meetingTask2Obj.Id, createdDate2);
        Test.setCreatedDate(callTask2Obj.Id, createdDate2);

        Test.startTest();
        UpdateUserActionPlan updateActionPlan = new UpdateUserActionPlan();
        updateActionPlan.refreshUserActionPlanRecords('Parent', planIdList);
        updateActionPlan.refreshUserActionPlanRecords('All', planIdList);
        updateActionPlan.refreshUserActionPlanRecords('', planIdList);
        Test.stopTest();

    }

}