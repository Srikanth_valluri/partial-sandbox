/**************************************************************************************************
* Name               : AsyncVATUpdate 
* Description        : This is the queuable webservice class for VAT Details Update
                                   
* Created Date       : 18/December/2017                                                                 
* Created By         : Alok Chauhan-DAMAC                                                      
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR          DATE                                                              
* 1.0         Alok-DAMAC      18/December/2017                                                        
**************************************************************************************************/

global class AsyncVATUpdate implements Queueable, Database.AllowsCallouts {
    public List<id> recordids = new List<id>();
 
   
    //Defining the constructor
    @TestVisible public AsyncVATUpdate(List<Id> rids){
        recordids=rids;    
    }
    
    public void execute(QueueableContext context) {
    System.Debug('VATUpdateAsync'+recordids);
    sendVATUpdate(recordids);
    }
    
    global static string endurl;
    global static string usrname;
    global static string pwd; 
    /**********************Getting the IPMS integration setting*********************************/ 
    
    public static void getIPMSsetting(String servicename){
        system.debug('***SSS***'+servicename);
        IPMS_Integration_Settings__mdt ipms= [select id,Endpoint_URL__c,Password__c,  Username__c from IPMS_Integration_Settings__mdt where DeveloperName = :servicename];
        endurl=ipms.Endpoint_URL__c;
        usrname=ipms.Username__c;      
        pwd=PasswordCryptoGraphy.DecryptPassword(ipms.Password__c);
        system.debug('***WS_URL***'+endurl);  
    }
    
    /**********************Method to Prepare the Request For WS*********************************/  
    public static string prepareVATRequest(List<id> regids){
    
       //string siteQuery= IPMS_WSHelper.getCreatableFieldsSOQL('Agent_Site__c');
       //siteQuery+=' where Id in :regids';
       List<Agent_Site__c> agntSite= [ Select Name,Id,Agent_Site_Country__c,Agency_ID__c,Tax_Registration_Number__c,Registration_Certificate_Date__c from Agent_Site__c
                                      where Id in :regids];
       String reqno='';
       if(agntSite.size()>0)
       {reqno ='VAT_UPD_'+agntSite[0].name+'-'+IPMS_WSHelper.GetFormattedDateTime(system.now());}
       system.debug('REQ NO==='+reqno);
       
        string body = '';
        body+= IPMS_WSHelper.sSoaHeader('IPMS_webservice_TB',usrname,pwd);         
            body+='<soapenv:Body>';
                body+='<proc:InputParameters>';
                   body+='<proc:P_REQUEST_NUMBER>'+reqno+'</proc:P_REQUEST_NUMBER>';
                    body+='<proc:P_SOURCE_SYSTEM>SFDC</proc:P_SOURCE_SYSTEM>';
                    body+='<proc:P_REQUEST_NAME>SUPPLIER_VAT_REGISTRATION</proc:P_REQUEST_NAME>';
                    body+='<proc:P_REQUEST_MESSAGE>';
                    string sDate;
                    for(Agent_Site__c sSite:agntSite){
                        sDate = DateTime.newInstance(sSite.Registration_Certificate_Date__c.year(),sSite.Registration_Certificate_Date__c.month(),sSite.Registration_Certificate_Date__c.day()).format('dd-MMM-yyyy');
                        body+='<proc:P_REQUEST_MESSAGE_ITEM>';
                            body+='<proc:PARAM_ID>'+sSite.Agency_ID__c+'</proc:PARAM_ID>';                            
                            body+='<proc:ATTRIBUTE1>'+sSite.Agent_Site_Country__c+'</proc:ATTRIBUTE1>';
                            body+='<proc:ATTRIBUTE2>'+sSite.Tax_Registration_Number__c+'</proc:ATTRIBUTE2>';
                            body+='<proc:ATTRIBUTE3>'+sDate+'</proc:ATTRIBUTE3>';
                            //body+='<proc:ATTRIBUTE3>'+sSite.Registration_Certificate_Date__c+'</proc:ATTRIBUTE3>';
                        body+='</proc:P_REQUEST_MESSAGE_ITEM>';
                    }
                    body+='</proc:P_REQUEST_MESSAGE>';
                body+='</proc:InputParameters>';
            body+='</soapenv:Body>';
        body+='</soapenv:Envelope>';
        
        return body.trim();
        
    }
/**********************Callout for Sales Audit Update*********************************/
     webservice static void sendVATUpdate(List<id> siteIds){
        
        getIPMSsetting('IPMS_webservice_TB');
        HTTPRequest req = new HTTPRequest();
        req.setMethod('POST');
        
        String reqXML = prepareVATRequest(siteIds);
        reqXML = reqXML.replaceAll('null', '');
        reqXML = reqXML.trim();
        req.setbody(reqXML);
        req.setEndpoint(endurl);
        System.debug('>>>>>>>>reqXML>>>>>>>>'+reqXML);
        req.setHeader('Content-Type','text/xml');
        req.setTimeout(120000);
        HTTP http = new HTTP();
        if(!Test.isrunningTest()){
            try{
                HTTPResponse res = http.send(req);
                
                System.debug('>>>>>Response>>>>>>'+res.getbody());
                
                parseRegnUpdateResponse(res.getBody());
            }
            catch(Exception ex) {
                System.debug('Callout error: '+ ex);
                Log__c objLog = new Log__c();
                objLog.Description__c ='--Ids=='+siteIds+'-Line No===>'+ex.getLineNumber()+'---Message==>'+ex.getMessage();
                objLog.Type__c = 'Webservice Callout For VAT Update on Site';
                insert objLog;             
            }    
        }
    }
    
     /**********************Parsing the response received for VAT Update Request*********************************/

    public static void parseRegnUpdateResponse(string body){
        
        //String reqId,sBookUnits,status,stageId,doclist = '';
        String multSites,status;
        List<NSIBPM__SR_Doc__c> sRDocList = new List<NSIBPM__SR_Doc__c>();
        
        List<String> sAgentIds= new List<String>();
        Map<string,String> statusMap = new Map<string,string>();
        
        DOM.Document xmlDOC = new DOM.Document();
        xmlDOC.load(body);
        DOM.XMLNode rootElement = xmlDOC.getRootElement();
        
        
        for(Dom.XMLNode child1: rootElement.getChildElements()){
            for(Dom.XMLNode child2: child1.getChildElements()){
                for(Dom.XMLNode child3: child2.getChildElements()){
                    for(Dom.XMLNode child4: child3.getChildElements()){
                       for(Dom.XMLNode child5: child4.getChildElements()){
                            System.debug('>>>>>>>>child5>>>>>>'+child5.getName());
                            if(child5.getName()=='PARAM_ID') 
                            {
                            multSites=child5.getText();
                            sAgentIds.add(multSites);   
                            }
                            if(child5.getName()=='PROC_MESSAGE')
                            {status=child5.getText();  }                           
                            
                       }                          
                       Statusmap.put(multSites,status);                        
                       
                    }
                }
            }
        }
        
 /**********************Callout for VAT Update*****************************************************************/      
             List<Agent_Site__c> updSite = [select id from Agent_Site__c where Id in:Statusmap.keyset()];
             if (updSite.size()>0){
             for(Agent_Site__c uSite:updSite){
             uSite.IPMS_Status__c='VAT Update -'+statusMap.get(uSite.id);               
             update uSite;
              }
            }
        }
        
    }