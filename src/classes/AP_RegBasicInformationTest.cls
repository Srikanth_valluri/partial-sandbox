@istest
public class AP_RegBasicInformationTest {
    @testSetup static void setupData() {

        Account acc = new Account ();
        acc.Name = 'test';
        acc.Agency_Type__c = 'Corporate';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        acc.Vendor_ID__c = '9696961';
        insert acc;

        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);        
        sr.Eligible_to_Sell_in_Dubai__c = true;
        sr.Agency_Type__c = 'Individual';
        sr.ID_Type__c = 'Passport';
        sr.Token_Amount_AED__c = 40000;
        sr.RecordTypeId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Change Agent').getRecordTypeId();
        sr.Booking_Wizard_Level__c = null;
        sr.Agency_Email_2__c = 'test2@gmail.com';
        sr.Alternate_Agency_Email_2__c = 'test1@gmail.com';
        sr.Country_of_Sale__c = 'UAE';
        sr.Mode_of_Payment__c = 'Cash';
        sr.agency__c = acc.id;
        sr.Date_of_Incorporation__c = System.today();
        sr.Trade_License_Expiry_Date__c = System.today().addYears(2);
        sr.RERA_Expiry_Date__c = System.today();
        sr.ID_Issue_Date__c = System.today().addYears(-2);
        sr.ID_Expiry_Date__c = System.today().addYears(1);
        //sr.Booking_from_New_Wizard__c = True;  
        insert sr; 

        NSIBPM__SR_Template__c srTemplate = InitializeSRDataTest.createSRTemplate('Agent Update');
        srTemplate.NSIBPM__SR_RecordType_API_Name__c= 'Agent_Update';
        srTemplate.Name = 'Agent Update';
        insert srTemplate; 

        Campaign__c camp = new Campaign__c();
        camp.Campaign_Name__c='Test Campaign';
        camp.start_date__c = System.today();
        camp.end_date__c = System.Today().addDays(30);
        camp.Marketing_start_date__c = System.today();
        camp.Marketing_end_date__c = System.Today().addDays(30);
        camp.Language__c = 'English';
        camp.Marketing_Active__c = true;
        camp.Credit_Control_Active__c  = true;
        camp.Sales_Admin_Active__c  = true;
        insert camp;

        Inquiry__c newInquiry = new Inquiry__c();
        newInquiry.Campaign__c = camp.Id;
        newInquiry.First_Name__c = 'Testsearch';
        newInquiry.Last_Name__c = 'Inquiry';
        newInquiry.Mobile_Phone_Encrypt__c = '05789088';
        newInquiry.Mobile_Phone__c = '05789088';
        newInquiry.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        newInquiry.Email__c = 'test.lastname@eternussolutions.com';
        newInquiry.Preferred_Language__c = 'English';
        newInquiry.Inquiry_Source__c = 'Social' ;
        newInquiry.Inquiry_Status__c = 'New' ;
        insert newInquiry;

        Amendment__c newAmd =  new Amendment__c();
        newAmd.Owner__c = true;
        newAmd.Shareholding__c =100;            
        newAmd.Authorised_Signatory__c = true;
        newAmd.Agent_Representative__c =true;
        newAmd.Portal_Administrator__c = true;
        newAmd.Email__c ='test@test.com';
        newAmd.ID_Type__c = 'Visa';
        newAmd.OwnerId= UserInfo.getUserId();
        newAmd.Service_Request__c = sr.id;
        newAmd.Broker_Card_Number__c = '15343';
        newAmd.Designation__c = 'CEO';
        newAmd.Broker_Card_Expiry_Date__c = date.today() + 10;
        newAmd.Inquiry__c = newInquiry.Id;
        insert newAmd;

          


    }
    @isTest static void Method1() {
        Account_SR_Field_Mapping__c objCS = new Account_SR_Field_Mapping__c();
        objCS.Is_Person_Account__c = true;
        objCS.Is_Common_to_All_RT__c = true;
        objCS.Account_Field__c = 'Reg_SR_Id__c';
        objCS.RecordType_Name__c = 'Agent_Update';
        objCS.SR_Field__c = 'Id';
        objCS.name = 'Reg_SR_Id__c';
        insert objCS; 
        
        NSIBPM__Service_Request__c sr = [SELECT Id FROM NSIBPM__Service_Request__c LIMIT 1];
        Inquiry__c inq = [SELECT Id FROM Inquiry__c LIMIT 1];
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.AP_RegBasicInformationPage'));
        System.currentPageReference().getParameters().put('Id', sr.Id);
        AP_RegBasicInformation objRegBasicInformation = new AP_RegBasicInformation();
        objRegBasicInformation.moveAmendmentsToSR(sr.Id, inq.Id);
        objRegBasicInformation.getCOS();
        objRegBasicInformation.submitDetails();
        Test.stopTest();        
    }

    @isTest static void Method2() {
        Account acc = [SELECT Id FROM Account LIMIT 1];
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.AP_RegBasicInformationPage'));
        System.currentPageReference().getParameters().put('accId', acc.Id);
        AP_RegBasicInformation objRegBasicInformation = new AP_RegBasicInformation();
        //objRegBasicInformation.moveAmendmentsToSR(Id pSRId, Id pInquiryId);
        objRegBasicInformation.getCOS();
        objRegBasicInformation.submitDetails();
        Test.stopTest();
    }
    
    @isTest static void Method3() {
        Account acc = new Account ();
        acc.Name = 'test';
        acc.Agency_Type__c = 'Corporate';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        acc.Vendor_ID__c = '9696962';
        insert acc;

        Account_SR_Field_Mapping__c objCS = new Account_SR_Field_Mapping__c();
        objCS.Is_Person_Account__c = false;
        objCS.Is_Common_to_All_RT__c = true;
        objCS.Account_Field__c = 'Reg_SR_Id__c';
        objCS.RecordType_Name__c = 'Agent_Update';
        objCS.SR_Field__c = 'Id';
        objCS.name = 'Reg_SR_Id__c';
        insert objCS; 

        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);        
        sr.Eligible_to_Sell_in_Dubai__c = true;
        sr.Agency_Type__c = 'Individual';
        sr.ID_Type__c = 'Passport';
        sr.Token_Amount_AED__c = 40000;
        sr.RecordTypeId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Agent Registration').getRecordTypeId();
        sr.Booking_Wizard_Level__c = null;
        sr.Agency_Email_2__c = 'test2@gmail.com';
        sr.Alternate_Agency_Email_2__c = 'test1@gmail.com';
        sr.Country_of_Sale__c = 'UAE';
        sr.Mode_of_Payment__c = 'Cash';
        sr.agency__c = acc.id;
        sr.Date_of_Incorporation__c = System.today();
        sr.Trade_License_Expiry_Date__c = System.today().addYears(2);
        sr.RERA_Expiry_Date__c = System.today();
        sr.ID_Issue_Date__c = System.today().addYears(-2);
        sr.ID_Expiry_Date__c = System.today().addYears(1);
        sr.City_Of_Incorporation_New__c = 'Dubai';
        sr.Agent_Registration_Type__c = 'Basic';
        insert sr; 
        Inquiry__c inq = [SELECT Id FROM Inquiry__c LIMIT 1];
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.AP_RegBasicInformationPage'));
        System.currentPageReference().getParameters().put('Id', sr.Id);
        AP_RegBasicInformation objRegBasicInformation = new AP_RegBasicInformation();
        objRegBasicInformation.moveAmendmentsToSR(sr.Id, inq.Id);
        objRegBasicInformation.getCOS();
        objRegBasicInformation.expDateVar = '1/1/2019';
        objRegBasicInformation.issueDateVar = '1/1/2018';
        objRegBasicInformation.licExp = '1/1/2019';
        objRegBasicInformation.doincorp = '1/1/2019';
        objRegBasicInformation.agencyType = 'Agent_Update';
        objRegBasicInformation.primaryLang = 'English';
        objRegBasicInformation.submitDetails();
        Test.stopTest();
    }
     @isTest static void Method4(){
         Account acc = new Account ();
            acc.Name = 'test';
            acc.Agency_Type__c = 'Corporate';
            acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
            acc.Trade_License_Number__c = '1234';
            acc.Blacklisted__c = true;
            acc.Inactive__c = false;
            acc.Vendor_ID__c = '969696';
            insert acc;

            NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);        
            sr.Eligible_to_Sell_in_Dubai__c = true;
            sr.Agency_Type__c = 'Corporate';
            sr.ID_Type__c = 'Passport';
            sr.Token_Amount_AED__c = 40000;
            sr.RecordTypeId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Agent Registration').getRecordTypeId();
            sr.Booking_Wizard_Level__c = null;
            sr.Agency_Email_2__c = 'test2@gmail.com';
            sr.Alternate_Agency_Email_2__c = 'test1@gmail.com';
            sr.Country_of_Sale__c = 'UAE';
            sr.Mode_of_Payment__c = 'Cash';
            sr.agency__c = acc.id;
            sr.Trade_License_Number__c = '1234';
            sr.Date_of_Incorporation__c = System.today();
            sr.Trade_License_Expiry_Date__c = System.today().addYears(2);
            sr.RERA_Expiry_Date__c = System.today();
            sr.ID_Issue_Date__c = System.today().addYears(-2);
            sr.ID_Expiry_Date__c = System.today().addYears(1);
            sr.City_Of_Incorporation_New__c = 'Dubai';
            sr.Agent_Registration_Type__c = 'Basic';
            insert sr;
            Inquiry__c inq = [SELECT Id FROM Inquiry__c LIMIT 1];
            Test.startTest();
            Test.setCurrentPageReference(new PageReference('Page.AP_RegBasicInformationPage'));
            System.currentPageReference().getParameters().put('Id', sr.Id);
            AP_RegBasicInformation objRegBasicInformation = new AP_RegBasicInformation();
            objRegBasicInformation.moveAmendmentsToSR(sr.Id, inq.Id);
            objRegBasicInformation.getCOS();
            objRegBasicInformation.submitDetails();
            Test.stopTest(); 
     }
     @isTest static void Method5(){
         Account acc = new Account ();
            acc.Name = 'test';
            acc.Agency_Type__c = 'Corporate';
            acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
            acc.Trade_License_Number__c = '1234';
            acc.Terminated__c = true;
            acc.Inactive__c = false;
            acc.Vendor_ID__c = '9696966';
            insert acc;

            NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);        
            sr.Eligible_to_Sell_in_Dubai__c = true;
            sr.Agency_Type__c = 'Corporate';
            sr.ID_Type__c = 'Passport';
            sr.Token_Amount_AED__c = 40000;
            sr.RecordTypeId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Agent Registration').getRecordTypeId();
            sr.Booking_Wizard_Level__c = null;
            sr.Agency_Email_2__c = 'test2@gmail.com';
            sr.Alternate_Agency_Email_2__c = 'test1@gmail.com';
            sr.Country_of_Sale__c = 'UAE';
            sr.Mode_of_Payment__c = 'Cash';
            sr.agency__c = acc.id;
            sr.Trade_License_Number__c = '1234';
            sr.Date_of_Incorporation__c = System.today();
            sr.Trade_License_Expiry_Date__c = System.today().addYears(2);
            sr.RERA_Expiry_Date__c = System.today();
            sr.ID_Issue_Date__c = System.today().addYears(-2);
            sr.ID_Expiry_Date__c = System.today().addYears(1);
            sr.City_Of_Incorporation_New__c = 'Dubai';
            sr.Agent_Registration_Type__c = 'Basic';
            insert sr;
            Inquiry__c inq = [SELECT Id FROM Inquiry__c LIMIT 1];
            Test.startTest();
            Test.setCurrentPageReference(new PageReference('Page.AP_RegBasicInformationPage'));
            System.currentPageReference().getParameters().put('Id', sr.Id);
            AP_RegBasicInformation objRegBasicInformation = new AP_RegBasicInformation();
            objRegBasicInformation.moveAmendmentsToSR(sr.Id, inq.Id);
            objRegBasicInformation.getCOS();
            objRegBasicInformation.submitDetails();
            Test.stopTest(); 
     }
     @isTest static void Method6(){
         Account acc = new Account ();
            acc.Name = 'test';
            acc.Agency_Type__c = 'Corporate';
            acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
            acc.Trade_License_Number__c = '1234';
            acc.Terminated__c = true;
            acc.Blacklisted__c = true;
            acc.Inactive__c = false;
            acc.Vendor_ID__c = '9696969';
            insert acc;

            NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);        
            sr.Eligible_to_Sell_in_Dubai__c = true;
            sr.Agency_Type__c = 'Corporate';
            sr.ID_Type__c = 'Passport';
            sr.Token_Amount_AED__c = 40000;
            sr.RecordTypeId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Agent Registration').getRecordTypeId();
            sr.Booking_Wizard_Level__c = null;
            sr.Agency_Email_2__c = 'test2@gmail.com';
            sr.Alternate_Agency_Email_2__c = 'test1@gmail.com';
            sr.Country_of_Sale__c = 'UAE';
            sr.Mode_of_Payment__c = 'Cash';
            sr.agency__c = acc.id;
            sr.Trade_License_Number__c = '1234';
            sr.Date_of_Incorporation__c = System.today();
            sr.Trade_License_Expiry_Date__c = System.today().addYears(2);
            sr.RERA_Expiry_Date__c = System.today();
            sr.ID_Issue_Date__c = System.today().addYears(-2);
            sr.ID_Expiry_Date__c = System.today().addYears(1);
            sr.City_Of_Incorporation_New__c = 'Dubai';
            sr.Agent_Registration_Type__c = 'Basic';
            insert sr;
            Inquiry__c inq = [SELECT Id FROM Inquiry__c LIMIT 1];
            Test.startTest();
            Test.setCurrentPageReference(new PageReference('Page.AP_RegBasicInformationPage'));
            System.currentPageReference().getParameters().put('Id', sr.Id);
            AP_RegBasicInformation objRegBasicInformation = new AP_RegBasicInformation();
            objRegBasicInformation.moveAmendmentsToSR(sr.Id, inq.Id);
            objRegBasicInformation.getCOS();
            objRegBasicInformation.submitDetails();
            Test.stopTest(); 
     }
}