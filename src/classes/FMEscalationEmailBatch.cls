/*-------------------------------------------------------------------------------------------------
Description: Batch to send email to Instructors containing list of attendees and details

============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 15-10-2018       | Lochana Rajput   | 1. Initial draft
----------------------------------------------------------------------------------------------------------------------------
2.0     | 10-12-2018       | Lochana Rajput   | 1. Added logic to skip notifications on weekends
----------------------------------------------------------------------------------------------------------------------------
3.0     | 03-01-2019       | Lochana Rajput   | 1. Added logic to send notifications to property director
=============================================================================================================================
*/

public class FMEscalationEmailBatch implements Database.Batchable<sObject>,Database.Stateful {

  /*public static final String strTDStartTag = '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">' ;
  public static final String strTDEndTag = '</td>' ;
  public static final String strTHStartTag = '<th style="border:1px solid black; text-align:center; border-collapse:collapse;">' ;
  public static final String strTHEndTag = '</th>';*/
    public EmailMessageWrapper[] lstWrapper;
    public Database.QueryLocator start(Database.BatchableContext BC) {
        //Get SR names stored as comma separated in custom label FMEscalationSRNames
        //for which need to send escalation emails
        List<String> lstSRnames = new List<String>();
        lstWrapper =   new List<EmailMessageWrapper>();
        lstSRnames = Label.FMEscalationSRNames.split(',');

        Date today = Date.today();
        String query = 'SELECT Id, Name,Property_Director_Email__c,FM_Director_Email__c,Escalation_Date__c,Unit_Name__c,Owner.Name, ';
        query += ' Account__c, Outstanding_service_charges__c,Tenant__c, Tenant__r.Name,Account__r.Name,RecordType.Name, RecordType.DeveloperName, Booking_Unit__r.Property_Name__c, ';
        query += ' Submission_Datetime__c,Building_Name_FR__c,Total_Amount__c,Currency__c,Payment_Mode__c,Origin__c FROM FM_Case__c ';

        query += ' WHERE Escalation_Date__c !=NULL ';
        query += ' AND Escalation_Date__c <=: today ';
        query += ' AND (FM_Director_Email__c != NULL OR Property_Director_Email__c != NULL)';
        query += ' AND Status__c = \'Submitted\' ';
        query += ' AND ((Approval_Status__c != \'Approved\' ';
        query += ' AND RecordType.DeveloperName IN : lstSRnames)  ';
        query += ' OR (RecordType.DeveloperName =\'AGA_Process\'';
        query += ' AND Notice_sent__c = true)) ';
        query += ' order by FM_Director_Email__c, Property_Director_Email__c';
        System.debug('=query==='+query);
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<FM_Case__c> scope) {
            map<String, set<FM_Case__c>> mapFMEmail_FMCaseName = new map<String, set<FM_Case__c>>();
            map<String, List<FM_Case__c>> mapFMPOPCases = new map<String, List<FM_Case__c>>();

            // Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage>();
            System.debug('=scope==='+scope);
            System.debug('=scope==='+scope.size());
            for(FM_Case__c obj : scope) {
                Datetime dt = System.Now();
                String dayOfWeek=dt.format('EEEE');
                if(! Label.FM_Escalation_Weekends.split(',').contains(dayOfWeek)) {
                    if( 'Proof_Of_Payment'.equalsIgnoreCase( obj.RecordType.DeveloperName ) ) {
                      if(mapFMPOPCases.containsKey(obj.FM_Director_Email__c)) {
                          mapFMPOPCases.get(obj.FM_Director_Email__c).add(obj);
                      }
                      else {
                          mapFMPOPCases.put(obj.FM_Director_Email__c, new List<FM_Case__c>{obj});
                      }
                    }
                    else {
                        if(String.isNotBlank(obj.FM_Director_Email__c)) {
                            if(mapFMEmail_FMCaseName.containsKey(obj.FM_Director_Email__c)) {
                                mapFMEmail_FMCaseName.get(obj.FM_Director_Email__c).add(obj);
                            }
                            else {
                                mapFMEmail_FMCaseName.put(obj.FM_Director_Email__c, new set<FM_Case__c>{obj});
                            }
                        }
                        if(String.isNotBlank(obj.Property_Director_Email__c)) {
                            if(mapFMEmail_FMCaseName.containsKey(obj.Property_Director_Email__c)) {
                                mapFMEmail_FMCaseName.get(obj.Property_Director_Email__c).add(obj);
                            }
                            else {
                                mapFMEmail_FMCaseName.put(obj.Property_Director_Email__c, new set<FM_Case__c>{obj});
                            }
                        }
                    }
                }
            }//for
            //Get template name
            // List<EmailTemplate> lstTemplate = [SELECT ID,HtmlValue
            //                                 FROM EmailTemplate
            //                                 WHERE DeveloperName =: Label.FMEscalationTemplateName];

            for(String email : mapFMEmail_FMCaseName.keyset()) {
                String strHTMLBody = 'Hi,<br/>Please note that the following cases are open.';
                strHTMLBody += ':<br/><br/><html><body>';
                strHTMLBody += '<table style="border:1px solid black; text-align:left; border-collapse:collapse;">'+
                +'<tr><th style="border:1px solid black; text-align:center; border-collapse:collapse;">Sr.No</th>'
                + '<th style="border:1px solid black;border-collapse:collapse; text-align:left;">FM Case#</th>'
                + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">SR Type</th>'
                + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Submission Date</th>'
                + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">SR Owner</th>'
                + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Tenant/Owner</th>'
                + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Unit Name</th>'
                + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Property Name</th>'
                + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Days Since Open</th>'
                + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Outstanding service charges</th></tr>';
                Integer serialNumber=1;
                for(FM_Case__c objCase : mapFMEmail_FMCaseName.get(email)) {
                    String acctName = objCase.Account__c == objCase.Tenant__c || (objCase.Tenant__c == NULL)
                                        ? objCase.Account__r.Name : objCase.Tenant__r.Name;
                    String strNA = 'NA';
                    String charges = objCase.Outstanding_service_charges__c != NULL ? objCase.Outstanding_service_charges__c : '';
                    String strUnitName = String.isBlank(objCase.Unit_Name__c) ? strNA : objCase.Unit_Name__c;
                    String strPropertyName = String.isBlank(objCase.Booking_Unit__r.Property_Name__c) ? strNA : objCase.Booking_Unit__r.Property_Name__c;
                    Integer strHrsSinceCaseIsOpen = objCase.Submission_Datetime__c.date().daysBetween(Date.Today());
                    strHTMLBody += '<tr>';
                    strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+serialNumber+'</td>';
                    strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;"><a href=\"'+System.URL.getSalesforceBaseUrl().toExternalForm() + '/'+objCase.Id+'\" target=\"_blank\">'+ objCase.Name+'</a></td>';
                    strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+objCase.RecordType.Name+'</td>';
                    strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+String.valueOf(objCase.Submission_Datetime__c.format('dd/MM/YYYY'))+'</td>';
                    strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+objCase.Owner.Name+'</td>';
                    strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+acctName+'</td>';
                    strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+ strUnitName +'</td>';
                    strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+ strPropertyName +'</td>';
                    strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+ strHrsSinceCaseIsOpen  +'</td>';
                    strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+charges +'</td>';
                    strHTMLBody += '</tr>';
                    serialNumber++;
                    // caseBody  = caseBody.replace('{!FM_Case__c.Id}',objCase.Id);
                    // caseBody  = caseBody.replace('{!FM_Case__c.Name}',objCase.Name);
                    // caseBody  = caseBody.replace('{!FM_Case__c.Submission_Datetime__c}',String.valueOf(date.valueOf(objCase.Submission_Datetime__c)));
                    // caseBody  = caseBody.replace('{!FM_Case__c.Owner_Name__c}',objCase.Owner_Name__c);
                    // caseBody  = caseBody.replace('{!FM_Case__c.Unit_Name__c}',objCase.Unit_Name__c);
                    // caseBody  = caseBody.replace('{!FM_Case__c.Building_Name_FR__c}',objCase.Building_Name_FR__c);
                    // strHTMLBody += '<br/>' + caseBody;
                }
                strHTMLBody += '</table></body></html><br/><br/>Thanks,<br/>DAMAC';
                // Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
                // Messaging.renderStoredEmailTemplate(lstTemplate[0].ID, objCon.Id, obj.Id);
                // emailToSend.setHtmlBody(htmlBody);
                // emailToSend.setTargetObjectId(objCon.Id);
                // emailToSend.setTreatTargetObjectAsRecipient(false);
                // emailToSend.saveAsActivity = false;
                // emailToSend.setWhatId(obj.Id);
                // emailToSend.subject = 'List of Pending FM Cases';
                //Below code is commented to send email in finish
                /*
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.toAddresses = new String[] {email};
                message.optOutPolicy = 'FILTER';
                message.subject = 'List of Pending FM Cases';
                message.HtmlBody = strHTMLBody;
                */
                lstWrapper.add(new EmailMessageWrapper(new String[] {email},'FILTER','List of Pending FM Cases',strHTMLBody));
                // message.plainTextBody = 'Hi,\n';
                // message.plainTextBody += 'Please note that the following cases are open for more than 24 hours.\n';
                // message.plainTextBody += caseNames + '\n\nThanks,\nDAMAC';
                // messages.add(message);
            }//for

            if( !mapFMPOPCases.isEmpty() ) {
              for( String strEmail : mapFMPOPCases.keySet() ) {
                String strHTMLBody = 'Hi,<br/>Please note that the following cases are open.';
                  strHTMLBody += ':<br/><br/><html><body>';
                  strHTMLBody += '<table style="border:1px solid black; text-align:left; border-collapse:collapse;">'+
                  +'<tr><th style="border:1px solid black; text-align:center; border-collapse:collapse;">Sr.No</th>'
                  + '<th style="border:1px solid black;border-collapse:collapse; text-align:left;">FM Case#</th>'
                  + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">SR Type</th>'
                  + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Submission Date</th>'
                  + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">SR Owner</th>'
                  + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Customer Name</th>'
                  + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Amount</th>'
                  + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Currency</th>'
                  + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Transfer Type</th>'
                  + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Source of SR</th>'
                  + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Days Since Open</th></tr>';
                  Integer serialNumber=1;
                  for( FM_Case__c objCase : mapFMPOPCases.get( strEmail ) ) {
                    String acctName = objCase.Account__c == objCase.Tenant__c || (objCase.Tenant__c == NULL) ? objCase.Account__r.Name : objCase.Tenant__r.Name;
                      String strTotalAmount =  String.isNotBlank( String.valueOf( objCase.Total_Amount__c ) ) ? String.valueOf( objCase.Total_Amount__c ) : '' ;
                      String strCurrency = String.isNotBlank( String.valueOf( objCase.Currency__c ) ) ? String.valueOf( objCase.Currency__c ) : '';
                      String strMode = String.isNotBlank( objCase.Payment_Mode__c ) ? objCase.Payment_Mode__c : '' ;
                      String strOrigin = String.isNotBlank( objCase.Origin__c ) ? objCase.Origin__c : '' ;
                      Integer strHrsSinceCaseIsOpen = objCase.Submission_Datetime__c.date().daysBetween(Date.Today());
                      strHTMLBody += '<tr>';
                      strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+serialNumber+'</td>';
                      strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;"><a href=\"'+System.URL.getSalesforceBaseUrl().toExternalForm() + '/'+objCase.Id+'\" target=\"_blank\">'+ objCase.Name+'</a></td>';
                      strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+objCase.RecordType.Name+'</td>';
                      strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+String.valueOf(objCase.Submission_Datetime__c.format('dd/MM/YYYY'))+'</td>';
                      strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+objCase.Owner.Name+'</td>';
                      strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+acctName+'</td>';
                      strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+ strTotalAmount +'</td>';
                      strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+ strCurrency +'</td>';
                      strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+ strMode +'</td>';
                      strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+ strOrigin +'</td>';
                      strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+ strHrsSinceCaseIsOpen  +'</td>';
                      strHTMLBody += '</tr>';
                      serialNumber++;
                  }
                  strHTMLBody += '</table></body></html><br/><br/>';

                  /*Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                  message.toAddresses = new String[] { strEmail };
                  message.optOutPolicy = 'FILTER';
                  message.subject = 'List of Pending FM Cases';
                  message.HtmlBody = strHTMLBody;
                  messages.add(message);*/
                    lstWrapper.add(new EmailMessageWrapper(new String[] { strEmail },'FILTER','List of Pending FM Cases',strHTMLBody));
              }
            }
            // Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        }//execute

    public void finish(Database.BatchableContext BC) {
        List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
        for(EmailMessageWrapper wrapperObj : lstWrapper) {
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = wrapperObj.toAddresses;
            message.optOutPolicy = wrapperObj.optOutPolicy;
            message.subject = wrapperObj.subject;
            message.HtmlBody = wrapperObj.HtmlBody;
            messages.add(message);
        }
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
    }

    public class EmailMessageWrapper {
        List<String> toAddresses;
        String optOutPolicy;
        String subject;
        String HtmlBody;
        public EmailMessageWrapper(List<String> toAddresses,String optOutPolicy,
                        String subject,String HtmlBody) {
            this.toAddresses = toAddresses;
            this.optOutPolicy = optOutPolicy;
            this.subject = subject;
            this.HtmlBody = HtmlBody;
        }
    }
}