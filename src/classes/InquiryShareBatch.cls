/****************************************************************************************************
* Description - The class developed to share community User's record with their manager             *
*---------------------------------------------------------------------------------------------------*
* Ver   Date        Author              Description                                                 *
* 1.0   22/02/2018  Monali              Initial Draft                                               *
* 1.1   07-06-2018  Craig Lobo          Updated the code to fetch records on the time from the CS   *
****************************************************************************************************/

// SHARING DONE FROM INFORMATICA

global class InquiryShareBatch implements Database.Batchable<sObject>, Database.Stateful { 

    //public String INQUIRY_QUERY = ' SELECT Id, Name, OwnerId, CreatedById, '
    //                            + ' CreatedBy.Profile.Name FROM Inquiry__c ';

    /**
     * Start Block: Get the Profiles and the Custom setting to run the batch
     */
    global Database.QueryLocator start(Database.BatchableContext objBatchableContext) { 
       /* System.debug('==InquiryShareBatch start==' );
        String query = '';
        List<String> promoterProfileNames = Label.PromoterCommunityProfile.split(',');
        Set<String> profileset = new Set<String> ();
        for(String profilename : promoterProfileNames ){
            profileset.add(profilename);
        }
        Generic_Switch_Setting__c csRecord = Generic_Switch_Setting__c.getValues('Promoter Sharing');

        if (csRecord != null && csRecord.Active__c && csRecord.Next_Run_Time__c != null) {
            DateTime nextRunTime = csRecord.Next_Run_Time__c;
            String nextRun = String.valueOf(nextRunTime.addMinutes(-2));
            System.debug('==nextRunTime start==' + nextRunTime);
            System.debug('==nextRun start==' + nextRun);
            DateTime dt = nextRunTime.addMinutes(-10);
            String str = dt.format('yyyy-MM-dd\'T\'hh:mm:ss\'z\'');
            System.debug('==nextRun str ==' + str );
            if(Test.isRunningTest()) {
                query = INQUIRY_QUERY;
            } else {
                System.debug('==Else==' );
                 query   = INQUIRY_QUERY
                        + ' WHERE CreatedDate >= ' + str
                        + ' AND CreatedBy.Profile.Name IN :profileset' ;
                        //+ profileset;       
                        
            }
            System.debug('==Else==' + query);
            return Database.getQueryLocator(query);
        }*/
       return Database.getQueryLocator([SELECT Id FROM Inquiry__c LIMIT 1]);
    }

    /**
     * Execute Block: Process the records
     */
    global void execute(Database.BatchableContext objBatchableContext, List<Inquiry__c> listInquiry) {
        /*System.debug('==InquiryShareBatch execute==' + listInquiry);
        if (listInquiry != null) {
            System.debug('---Size of list ' + listInquiry.size() );
            SharePromoterInquiries.insertInquiryShareRecords(listInquiry);
        }*/
    }

    /**
     * Finish Block: Update the Custom setting with the current time for the next set of records
     */
    global void finish(Database.BatchableContext info){
        /*System.debug('==InquiryShareBatch finish==' );
        Generic_Switch_Setting__c csRecord = Generic_Switch_Setting__c.getValues('Promoter Sharing');
        if (csRecord != null) {
            csRecord.Next_Run_Time__c = System.now();
            update csRecord;
        }*/
    } 

}