/******************************************************************************************************************************************
Description: Class to send Escalation Emails
===========================================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By  | Comments
--------------------------------------------------------------------------------------------------------------------------------------------
1.0     | 12-10-2020        | Jyotika Kalra     | 1. Initial Draft

1.0     | 15-10-2020        | Aishwarya Todkar  | 1. Handled Merge Fields
*******************************************************************************************************************************************/
public class ComplaintEscalationNotifier {
    
/******************************************************************************************************************************************
Description : Method to send escalation
Parameter(s): List Ids
Return      : void
Invoked from 'Complaint on Task' process builder
*******************************************************************************************************************************************/
    @InvocableMethod(label='NotifyOnResolveComplaint' description='NotifyOnResolveComplaint')
    public static void notifyOwner(List<DataWrapper> lstDataWrapper) {
        if( !lstDataWrapper.isEmpty() ){
            for( DataWrapper objDataWrapper : lstDataWrapper ){
                 sendEscalationMail( objDataWrapper.Ids, objDataWrapper.Eamil48Hour );
            }
        }
    }
    
    @future (callout = true)
    public static void sendEscalationMail( ID taskId, Boolean Eamil48Hour) {
        
        //Get task
        List<Task> lstTask = new List<Task>( [SELECT 
                                                    Id
                                                    , OwnerId
                                                    , Subject
                                                    , Status
                                                    , WhatId
                                                FROM 
                                                    Task 
                                                WHERE 
                                                    Id =: taskId 
                                                AND 
                                                    Status != 'Completed' 
                                                AND 
                                                    Status != 'Closed' Limit 1
                                            ] );
        
        if( lstTask != null && !lstTask.isEmpty() ){
            
            //get user and manager email Ids
            List<User> lstUser = new List<User> ( [SELECT 
                                                        Id
                                                        ,Email
                                                        ,Manager.Email
                                                    FROM 
                                                        User 
                                                    WHERE 
                                                        Id =: lstTask[0].OwnerId 
                                                    ] );
            
            if( !lstUser.isEmpty() ){

                List<SendGrid_Email_Details__mdt> listSendGridDetails = new List<SendGrid_Email_Details__mdt>();
                String escalationName = Eamil48Hour ? 'Notify Owner_Manager On 48 Hours' : 'Notify Owner_Manager On 72 Hours';
                
                //Call method to get configured sendrid details
                listSendGridDetails = CRM_SurveyEmailsSender.getSendGridDetails( escalationName );
                
                if( !listSendGridDetails.isEmpty() ) {

                    //call method to send email via sendgrid
                    prepareSendGridEmail( listSendGridDetails, lstTask[0], lstUser[0] );
                }
            }// End lstUser if
        }//End lstTask if
    }//End sendEscalationMail method

/******************************************************************************************************************************************
Description : Method to send email via sendGrid
Parameter(s): List SendGrid_Email_Details__mdt, task, user
Return      : void
*******************************************************************************************************************************************/
    public static void prepareSendGridEmail( List<SendGrid_Email_Details__mdt> listSendGridDetails, Task objTask, user objUser ) {

        if( listSendGridDetails != null && !listSendGridDetails.isEmpty() && objTask != null && objUser != null ) {
            
            SendGrid_Email_Details__mdt objSendGridCs = listSendGridDetails[0];
            
            //Get Email Template
            List<EmailTemplate> listEmailTemplate = new List<EmailTemplate>( [SELECT 
                                                                                  Id
                                                                                  ,Name  
                                                                                  ,Subject  
                                                                                  ,Body
                                                                                  ,HtmlValue
                                                                                  ,TemplateType
                                                                              FROM 
                                                                                  EmailTemplate 
                                                                              WHERE 
                                                                                  Name =: objSendGridCs.Email_Template__c limit 1
                                                                             ] );

            if( !listEmailTemplate.isEmpty() ) {

                EmailTemplate emailTemplateObj = listEmailTemplate[0];
                String toAddress;
                if( String.isNotBlank(objUser.Email) && String.isNotBlank(objUser.Manager.Email) ){
                    toAddress = objUser.Email +','+ objUser.Manager.Email;
                } else if( String.isNotBlank(objUser.Email) ) {
                    toAddress = objUser.Email;
                }
                
                //Get configured details in SendGrid Email Details Metadata
                if( String.isNotBlank( toAddress ) ) {

                    GenericUtility.SendGridWrapper sgWrap = new GenericUtility.SendGridWrapper();
                    
                    sgWrap = GenericUtility.prepareSendGridEmail( objSendGridCs );
                    
                    sgWrap.toAddress = String.isNotBlank( sgWrap.toAddress ) ? toAddress + ',' + sgWrap.toAddress : toAddress;
                    
                    sgWrap.contentType = 'text/html';
                    
                    sgWrap.contentValue = MergeFieldReplacer.replaceMergeFields( emailTemplateObj.HtmlValue, 'Generic', objTask.WhatId ) ;
                    
                    sgWrap.contentBody = MergeFieldReplacer.replaceMergeFields( emailTemplateObj.Body, 'Generic', objTask.WhatId );
                    
                    sgWrap.subject = MergeFieldReplacer.replaceMergeFields( emailTemplateObj.Subject, 'Generic', objTask.WhatId );
                    
                    sgWrap.listAttachment = new  List<Attachment>();
                    
                    sgWrap.relatedToId = objTask.WhatId;

                    sgWrap.caseId = objTask.WhatId;

                    sgWrap.processType = 'Complaint';

                    List<EmailMessage> lstEmails = new List<EmailMessage>();

                    //Call generic method to send email via sendGrid
                    lstEmails = GenericUtility.sendEmailsBySendGrid( sgWrap );
                
                    if( !lstEmails.isEmpty() ) {
                        if( !Test.isRunningTest() ) {
                            insert lstEmails;
                        }
                    }
                }// End toAddress if
            }//End listEmailTemplate if
        }//End main if 
    }

/******************************************************************************************************************************************
 * Class Name   : DataWrapper
 * Description  : Wrapper class to send parameters from process builder
******************************************************************************************************************************************/
    public Class DataWrapper{
        @InvocableVariable(required=true)
        public Id Ids;
        @InvocableVariable(required=true)
        public Boolean Eamil48Hour;
    }
}