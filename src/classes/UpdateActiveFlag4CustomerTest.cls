/******************************************************************************
* Description - Test class developed for HandoverSendEmailBatch
*
* Version            Date            Author                    Description
* 1.0                23/07/18        Aditya Kishor            Initial Draft
********************************************************************************/


@isTest
private class UpdateActiveFlag4CustomerTest {

    static testMethod void unitTestOne() {        
        
        Account Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        Acc.Email__pc = 'abc@z.com';
        Acc.Party_Id__c = '56789';
        insert Acc;
        System.assert(Acc != null);
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);        
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'JNU/WE/12';  
        BU.Registration_Status__c  = 'executed by Damac';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;
        BU.Approval_Status__c = 'HO';  
        BU.party_id__c = '56789';
        BU.Registration_Status_Code__c='Test';       
        insert BU;
        System.assert(BU != null);    
        
        Booking_Unit__c BU1 =  new Booking_Unit__c();
        BU1.Unit_Name__c = 'Test Units 123';  
        BU1.Registration_Status__c  = 'Active';
        BU1.Registration_ID__c   = '5678';  
        BU1.Unit_Selling_Price_AED__c  = 100;  
        BU1.Booking__c = Booking.Id;
        BU1.Approval_Status__c = 'EHO';  
        BU1.party_id__c = '56789';
        BU1.Registration_Status_Code__c='Test';      
        insert BU1;
 
        test.startTest();
            Database.executeBatch( new UpdateActiveFlag4Customer () );    
        test.stopTest();
    
    }
}