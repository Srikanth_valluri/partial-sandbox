@isTest
private class Damac_ActivityManager_TEST {

    @isTest
    static void test_method_one() {

        Account acc = createAccount();
        Task newTask = createTask(acc.Id, system.now().addMinutes(330), system.now().addMinutes(360));
        Task newTask1 = createTask(acc.Id, system.now(), system.now().addMinutes(30));        
        Activity_Manager__c activityManager = new Activity_Manager__c();
        activityManager.Task_Owner_Or_Created_By_Id__c = UserInfo.getUserId();
        activityManager.Activity_or_Task_Id__c = newTask.Id;
        insert activityManager;        
/*
        Sales_Tours__c salesTour = new Sales_Tours__c();
        salesTour.Approval_Status__c ='Eligible for Sales Commission';
        salesTour.Check_In_Date__c = system.now()+4;
        salesTour.Check_Out_Date__c = system.now()+6;
        insert salesTour;
*/
        Test.startTest();
        Damac_ActivityManager obj = new Damac_ActivityManager();
        Apexpages.currentpage().getparameters().put('taskID', newTask.Id);
        Apexpages.currentpage().getparameters().put('attendeesId', UserInfo.getUserId());
       // Apexpages.currentpage().getparameters().put('salesTourId', salesTour.Id);
        Apexpages.currentpage().getparameters().put('otherAttendee','test@gmail.com');
        obj.dateCriteria = newTask1;
        obj.ownerIds = new List<String>{UserInfo.getUserId()};                      
        obj.meetingTypeFilter = 'M';
        obj.relatedToFilter = 'Inquiry';
        obj.init();
        obj.pageLoad();
        obj.getTaskDetail();
        obj.createAttendees();
        System.assertEquals([SELECT COUNT() FROM Meeting_Attendees__c WHERE Activity_Manager__c =: activityManager.ID], 2);
        obj.upsertTask();
        System.assertEquals([SELECT COUNT() FROM Task WHERE Id =: newTask.ID], 1);
        Test.stopTest();
    }
    @isTest
    static void test_method_two() {
        Account acc = createAccount();
        Task newTask = createTask(acc.Id, system.now().addMinutes(330), system.now().addMinutes(360));
        Activity_Manager__c activityManager = new Activity_Manager__c();
        activityManager.Activity_or_Task_Id__c = newTask.Id;
        activityManager.Task_Owner_Or_Created_By_Id__c = userInfo.getUserId();
        insert activityManager;

        /*Sales_Tours__c salesTour = new Sales_Tours__c();
        salesTour.Approval_Status__c ='Eligible for Sales Commission';
        insert salesTour;
*/
        Test.startTest();

        Damac_ActivityManager obj = new Damac_ActivityManager();
        Apexpages.currentpage().getparameters().put('taskID', NULL);
        Apexpages.currentpage().getparameters().put('attendeesId', NULL);
        Apexpages.currentpage().getparameters().put('accountId', acc.Id);
        Apexpages.currentpage().getparameters().put('involvedContactsId', acc.Id);
        obj.dateCriteria = newTask;
        obj.ownerIds = new List<String>{UserInfo.getUserId()};
        obj.meetingTypeFilter = '';
        obj.relatedToFilter = 'Inquiry';
        obj.init();
        obj.pageLoad();
        obj.upsertTask();
        System.assertEquals([SELECT COUNT() FROM Task WHERE Id =: newTask.ID], 1);
        Test.stopTest();
    }
    @isTest
    static void test_method_three() {

        Account acc = createAccount();

        Task newTask = createTask(acc.Id, system.now().addMinutes(330), system.now().addMinutes(360));

        Activity_Manager__c activityManager = new Activity_Manager__c();
        activityManager.Task_Owner_Or_Created_By_Id__c = userInfo.getUserId();
        activityManager.Activity_or_Task_Id__c = newTask.Id;
        insert activityManager;

   /*     Sales_Tours__c salesTour = new Sales_Tours__c();
        salesTour.Approval_Status__c ='Eligible for Sales Commission';
        insert salesTour;
*/
        Test.startTest();

        Damac_ActivityManager obj = new Damac_ActivityManager();
        Apexpages.currentpage().getparameters().put('taskID', NULL);
        Apexpages.currentpage().getparameters().put('attendeesId', NULL);
        Apexpages.currentpage().getparameters().put('accountId', acc.Id);
        obj.dateCriteria = newTask;
        obj.ownerIds = new List<String>{UserInfo.getUserId()};
        obj.meetingTypeFilter = 'M';
        obj.relatedToFilter = 'Customer';
        obj.init();
        obj.pageLoad();
        obj.upsertTask();
        
        

        Damac_ActivityManager.getInquiryDetails('test');
        Damac_ActivityManager.getAccountDetails('test', 'Agency');
        System.assertEquals([SELECT COUNT() FROM Task WHERE Id =: newTask.ID], 1);
        Test.stopTest();
    }
    
    @isTest
    static void test_method_four() {

        Account acc = createAccount();
        contact con = new Contact(AccountId = acc.Id, FirstName = 'New', LastName = 'test');
        insert con;

        Task newTask = createTask(acc.Id, system.now().addMinutes(330), system.now().addMinutes(360));
        Activity_Manager__c activityManager = new Activity_Manager__c();
        activityManager.Activity_or_Task_Id__c = newTask.Id;
        activityManager.Task_Owner_Or_Created_By_Id__c = userinfo.getUserId();
        insert activityManager;        
     /*   
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        
        User u = new User(
         ProfileId = [SELECT Id FROM Profile WHERE Name = 'Director of Sales'].Id,
         LastName = 'manager',
         Email = 'manager@gmail.com',
         Username = 'manager@gmail.com' + System.currentTimeMillis(),
         CompanyName = 'TEST',                
         Title = 'Manager',
         Alias = 'aliass',
         TimeZoneSidKey = 'America/Los_Angeles',
         EmailEncodingKey = 'UTF-8',
         LanguageLocaleKey = 'en_US',
         LocaleSidKey = 'en_US',
         UserRoleId = r.Id
);
        insert u;
        
        UserRole r1 = new UserRole(DeveloperName = 'MyCustomRole2', Name = 'My Role2');
        insert r1;
        
         User u1 = new User(
         ProfileId = [SELECT Id FROM Profile WHERE Name = 'Property Consultant'].Id,
         LastName = 'last',
         Email = 'user@gmail.com',
         Username = 'user@gmail.com' + System.currentTimeMillis(),
         CompanyName = 'TEST',
         Manager = u,       
         Title = 'title',
         Alias = 'alias',
         TimeZoneSidKey = 'America/Los_Angeles',
         EmailEncodingKey = 'UTF-8',
         LanguageLocaleKey = 'en_US',
         LocaleSidKey = 'en_US',
         UserRoleId = r1.Id
);
        insert u1;
  */      
        Meeting_Attendees__c meet = new Meeting_Attendees__c();
        meet.Activity_Manager__c = activityManager.Id;     
        meet.Attendee__c = userinfo.getUserId();
        insert meet;
        
        Test.startTest();

        Damac_ActivityManager obj = new Damac_ActivityManager();
        Apexpages.currentpage().getparameters().put('taskID', newTask.Id);
        Apexpages.currentpage().getparameters().put('accountID', acc.Id);
        Apexpages.currentpage().getparameters().put('involvedContactsId', con.Id);
        obj.dateCriteria = newTask;
        obj.ownerIds = new List<String>{UserInfo.getUserId()};
        obj.meetingTypeFilter = 'IM';
        obj.relatedToFilter = 'customer';
        
        PageReference testPage = Page.Damac_ActivityManager;
        Test.setCurrentPage(testPage);
        obj.allRelatedContacts();
    
        obj.init();
        obj.pageLoad();
        obj.upsertTask();
        obj.createAgencyContactsInvolved();
        Damac_ActivityManager.getInquiryDetails('test');
        Damac_ActivityManager.getAccountDetails('test', 'Agency');
        System.assertEquals([SELECT COUNT() FROM Task WHERE Id =: newTask.ID], 1);
        //Double res = obj.offset();
        Test.stopTest();
    }
    @isTest
    static void test_method_five() {

        Account acc = createAccount();
        contact con = new Contact(AccountId = acc.Id, FirstName = 'New', LastName = 'test');
        insert con;

        Task newTask = createTask(acc.Id,  system.now().addMinutes(330), system.now().addMinutes(360));
        Activity_Manager__c activityManager = new Activity_Manager__c();
        activityManager.Activity_or_Task_Id__c = newTask.Id;
        activityManager.Task_Owner_Or_Created_By_Id__c = userinfo.getUserId();
        insert activityManager;
        Meeting_Attendees__c meet = new Meeting_Attendees__c();
        meet.Activity_Manager__c = activityManager.Id;     
        meet.Attendee__c = userinfo.getUserId();
        insert meet;
        
        Test.startTest();

        Damac_ActivityManager obj = new Damac_ActivityManager();
        Apexpages.currentpage().getparameters().put('taskID', newTask.Id);
        Apexpages.currentpage().getparameters().put('accountID', acc.Id);
        Apexpages.currentpage().getparameters().put('involvedContactsId', con.Id);
        obj.dateCriteria = newTask;
        obj.ownerIds = new List<String>{UserInfo.getUserId()};
        obj.meetingTypeFilter = 'ST';
        obj.relatedToFilter = 'Agency';
        
        PageReference testPage = Page.Damac_ActivityManager;
        Test.setCurrentPage(testPage);
        obj.allRelatedContacts();
    
        obj.init();
        obj.pageLoad();
        obj.upsertTask();
        obj.createAgencyContactsInvolved();
        Damac_ActivityManager.getInquiryDetails('test');
        Damac_ActivityManager.getAccountDetails('test', 'Agency');
        System.assertEquals([SELECT COUNT() FROM Task WHERE Id =: newTask.ID], 1);
        //Double res = obj.offset();
        Test.stopTest();
    }
    @isTest
    static void test_method_six() {

        Account acc = createAccount();
        contact con = new Contact(AccountId = acc.Id, FirstName = 'New', LastName = 'test');
        insert con;

        Task newTask = createTask(acc.Id,  system.now().addMinutes(330), system.now().addMinutes(360));
        Activity_Manager__c activityManager = new Activity_Manager__c();
        activityManager.Activity_or_Task_Id__c = newTask.Id;
        activityManager.Task_Owner_Or_Created_By_Id__c = userinfo.getUserId();
        insert activityManager;
        Meeting_Attendees__c meet = new Meeting_Attendees__c();
        meet.Activity_Manager__c = activityManager.Id;     
        meet.Attendee__c = userinfo.getUserId();
        insert meet;
        
        Test.startTest();

        Damac_ActivityManager obj = new Damac_ActivityManager();
        Apexpages.currentpage().getparameters().put('taskID', newTask.Id);
        Apexpages.currentpage().getparameters().put('accountID', acc.Id);
        Apexpages.currentpage().getparameters().put('involvedContactsId', con.Id);
        obj.dateCriteria = newTask;
        obj.ownerIds = new List<String>{UserInfo.getUserId()};
        obj.meetingTypeFilter = 'M';
        obj.relatedToFilter = 'Agency';
        
        PageReference testPage = Page.Damac_ActivityManager;
        Test.setCurrentPage(testPage);
        obj.allRelatedContacts();
    
        obj.init();
        obj.pageLoad();
        obj.upsertTask();
        obj.createAgencyContactsInvolved();
        Damac_ActivityManager.getInquiryDetails('test');
        Damac_ActivityManager.getAccountDetails('test', 'Agency');
        System.assertEquals([SELECT COUNT() FROM Task WHERE Id =: newTask.ID], 1);
        //Double res = obj.offset();
        Test.stopTest();
    }
    
    public static Account createAccount(){
        Id RecTypeCorporateAgency = Schema.SObjectType.Account
                                    .getRecordTypeInfosByName()
                                    .get('Corporate Agency')
                                    .getRecordTypeId();
        Account acc = new account(recordtypeid = RecTypeCorporateAgency, name='test',Trade_License_Number__c = 'test');
        insert acc;
        return acc;

    }
    public static Task createTask(Id WhatIds, Datetime startDate, Datetime endDate) {
        Task objTask = new Task(
            WhatId = WhatIds,
            Subject = 'Last Test',
            Description = 'Last Test',
            Start_date__c = startDate,
            End_Date__c = endDate,
            ActivityDate = Date.Today().addDays(2),
            OwnerId = UserInfo.getuserid(),
            Agency_Contacts_Involved__c = ''
            
        );
        insert objTask;
        return objTask;
    }
    
    
}