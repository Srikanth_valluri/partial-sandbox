@IsTest
private class Damac_InquiryAssignmentClone_Test {
    static testmethod void method01() {
        
        Inquiry_User_Assignment_Rules__c ua1 = new Inquiry_User_Assignment_Rules__c ();
        ua1.Daily__c = 2;
        ua1.Monthly__c =3;
        ua1.Weekly__c =5;
        insert ua1;
        
        Campaign__c camp = new Campaign__c ();
        camp.Start_Date__c = system.today().adddays(-3);
        camp.End_Date__c =  system.today().adddays(3);
        camp.Marketing_Start_Date__c =  system.today().adddays(-3);
        camp.Marketing_End_Date__c =  system.today().adddays(3);
        camp.PCAssignment__c = true;
       // camp.Active__c = true;
        insert camp;
        
       
        
        
        Assigned_PC__c assign = new Assigned_PC__c ();
        assign.User__c = userinfo.getUserId();
        assign.Campaign__c = camp.Id;
        insert assign;
        
        LIST<Inquiry__c> enquirylist = new LIST<Inquiry__c> ();
        Inquiry__c inquiry = new Inquiry__c();
        inquiry.First_Name__c = 'test';
        inquiry.Last_Name__c = 's';
        inquiry.Primary_Contacts__c = 'Mobile Phone';
        inquiry.Preferred_Language__c = 'French';
        inquiry.Inquiry_Source__c = 'Chat';
        inquiry.Mobile_Phone__c = '1234567890';
        inquiry.Mobile_CountryCode__c = 'India: 0091';
        inquiry.Email__c = 'test@test.com';
        inquiry.Mobile_Phone_Encrypt__c = '8734637856';
        inquiry.Class__c = 'Priority Digital';
        inquiry.Campaign__c = camp.Id;
        enquirylist.add(inquiry);
        insert enquirylist;
        
        user u=[select name from user where id=:userinfo.getUserId()];
        system.runAs(u){
        Group grp = new Group();
        grp.name = 'Test Group1';
        grp.Type = 'Regular'; 
        Insert grp; 
 system.debug('222'+grp.id);
        //Create Group Member
        GroupMember grpMem1 = new GroupMember();
        grpMem1.UserOrGroupId = UserInfo.getUserId();
        grpMem1.GroupId = grp.Id;
        Insert grpMem1;
            
        LIST<Inquiry_Assignment_Rules__c> rules = new LIST<Inquiry_Assignment_Rules__c> ();
        Inquiry_Assignment_Rules__c ua = new Inquiry_Assignment_Rules__c ();
        ua.Is_Campaign_Active__c = 'NO';
        ua.Active__C = true;
        ua.Execute_on__c = 'Create';
        ua.Priority__c = 1;
        ua.ownerid = grp.Id;
        ua.Tenure__c = '>1 week';
        rules.add(ua);
            
        Inquiry_Assignment_Rules__c rule1 = new Inquiry_Assignment_Rules__c ();
        rule1.Tenure__c = '<1 week';
        rule1.Active__c = true;
        rule1.Execute_on__c = 'Create';
        rule1.Queue_Assignment__c =true;
        rule1.Fields_to_Apply_Rule__c = 'Class__c';
        rule1.Class__c = 'Priority Digital';
        rules.add(rule1);
        insert rules;
        rule1.Parent_Reassign_Rule__c = ua.id;
        update rule1;
        test.startTest();
        Damac_InquiryAssignmentCloneController.cloneRecord (ua.id, ua.Execute_on__c);
        test.stopTest();
        }
    }
}