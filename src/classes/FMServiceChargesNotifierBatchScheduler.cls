/*********************************************************************************
*   Description :  Executes FMServiceChargesNotifierBatch class.                 *
*--------------------------------------------------------------------------------*
*   Revision History:                                                            *
*   Version     Author              Date            Description                  *
*   1.0         Aishwarya Todkar    20/02/2019      Initial Draft                *
*********************************************************************************/
global class FMServiceChargesNotifierBatchScheduler implements Schedulable {
    global void execute(SchedulableContext sc) {
        //Get the Dates from Label
        /*List<String> listDates = Label.Batch_Scheduling_Dates.split(',');
        System.debug('listDates =' + listDates );
        
        //Format Today's date in dd-mm-yyyy format
        String todayDate = System.today().day() + '-' + System.today().month() + '-' + System.today().year();
        System.debug('todayDate =' + todayDate );
        
        //Execute batch if list of date has today's date
        if(listDates != NULL && listDates.size() > 0 
        && String.isNotBlank(todayDate)&& listDates.contains(todayDate)) {
            System.debug('== contains todayDate ===' );
            database.executebatch(new FMServiceChargesNotifierBatch(),1);
        }*/
		
      FMServiceChargesNotifierBatch batchInstance = new FMServiceChargesNotifierBatch(); 
      database.executebatch(batchInstance,1);
	  
    }
}