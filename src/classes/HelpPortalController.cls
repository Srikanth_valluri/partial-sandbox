public with sharing class HelpPortalController{
    public String Section { get; set; }
    //public String Article {get; set;}

    @RemoteAction
    public static String getInit(String Section){
        list<Customer_Portal_Help__kav> listCustom = new list<Customer_Portal_Help__kav>();
         listCustom=
         [
            Select ArticleBody__c
                 , Section_Name__c 
              from Customer_Portal_Help__kav
             where Section_Name__c = :Section
             LIMIT 1
        ];  
        system.debug('Data__c' + listCustom[0].ArticleBody__c);      
        String Article = listCustom[0].ArticleBody__c;    
        return Article;
    }
}