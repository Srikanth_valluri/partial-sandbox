/***********************************************************************************************************
* Description - The class developed to share community User's record with their manager
*
* Version            Date            Author            Description
* 1.0                04/06/18        Monali            Initial Draft
************************************************************************************************************/

public with sharing class NotificationBatch implements Database.Batchable<sObject>,
                                          Database.Stateful{

    public Database.QueryLocator start(Database.BatchableContext objBatchableContext) { 
        System.debug('==RejectionNotificationBatch start==' );
        Id AgentRegistrationrecordtypeid = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Agent Registration').getRecordTypeId();
        Id AgentUpdaterecordtypeid = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Agent Update').getRecordTypeId();
        String query = 'Select Id, Name, CreatedDate,NSIBPM__Internal_Status_Name__c,Under_Agent_Review_1day_formula__c,Under_Agent_Review_2dayformula__c,Under_Manager_Review_1_day_formula__c,Under_Agent_Review__c,Under_Manager_Review__c From NSIBPM__Service_Request__c Where NSIBPM__Internal_Status_Name__c in (\'Under Agent Executive Review\',\'Under Agent Manager Review\')';
        query += 'And (Under_Agent_Review_1day_formula__c = TODAY OR Under_Agent_Review_2dayformula__c = TODAY OR Under_Manager_Review_1_day_formula__c = TODAY )';
        query += 'And(RecordTypeId = :AgentRegistrationrecordtypeid  OR RecordTypeId =:AgentUpdaterecordtypeid)';
        System.debug('==query execute==' +query );
        if(Test.isRunningTest()){
           query = 'Select Id, Name, CreatedDate,NSIBPM__Internal_Status_Name__c,Under_Agent_Review_1day_formula__c,Under_Agent_Review_2dayformula__c,Under_Manager_Review_1_day_formula__c,Under_Agent_Review__c,Under_Manager_Review__c From NSIBPM__Service_Request__c' ;    
        }
        return Database.getQueryLocator(query);  
    }

    public void execute(Database.BatchableContext objBatchableContext, List<NSIBPM__Service_Request__c> listSR) {
        System.debug('@@list size @@@' + listSR.size());
        System.debug('==listSR execute==' +listSR );
        System.debug('==RejectionNotificationBatch execute==' );
        NotificationBatchHandler.ProcessRecords(listSR);
    }

    public void finish(Database.BatchableContext info){
        System.debug('==RejectionNotificationBatch finish==' );
    } 
}