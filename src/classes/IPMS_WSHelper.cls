global with sharing class IPMS_WSHelper
{

/**********************Preparing the general header for the http request*********************************/
     
 @testvisible public static string sSoaHeader(string servicename,string usrname,string pwd ){
       
     String body ='';
     body+='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xxdc="http://xmlns.oracle.com/apps/ont/soaprovider/plsql/xxdc_process_service_ws/" xmlns:proc="http://xmlns.oracle.com/apps/ont/soaprovider/plsql/xxdc_process_service_ws/process/">';
       body+='<soapenv:Header>';
          body+='<xxdc:SOAHeader>';
             //body+='<!--Optional:-->';
             body+='<xxdc:Responsibility>ONT_ICP_SUPER_USER</xxdc:Responsibility>';
             //body+='<!--Optional:-->';
             body+='<xxdc:RespApplication>ONT</xxdc:RespApplication>';
             //body+='<!--Optional:-->';
             body+='<xxdc:SecurityGroup>standard</xxdc:SecurityGroup>';
             //body+='<!--Optional:-->';
             body+='<xxdc:NLSLanguage>american</xxdc:NLSLanguage>';
             //body+='<!--Optional:-->';
             body+='<xxdc:Org_Id/>';
          body+='</xxdc:SOAHeader>';
          body+='<wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">';
             body+='<wsse:UsernameToken>';
                body+='<wsse:Username>'+usrname+'</wsse:Username>';
                body+='<wsse:Password>'+pwd+'</wsse:Password>';
             body+='</wsse:UsernameToken>';
          body+='</wsse:Security>';
       body+='</soapenv:Header>';
       return body;
    }
  /**********************Get Formatted Date*********************************/  
 
 @testvisible public static string GetFormattedDateTime(DateTime dt){
        String yyyy=string.valueof(dt.year());
        String mm=string.valueof(dt.month());
        String dd=string.valueof(dt.day());
        String hh=string.valueof(dt.hour());
        String mi=string.valueof(dt.minute());
        String ss=string.valueof(dt.second());
        String ms=string.valueof(dt.millisecond());
        
        String formatdate=yyyy+mm+dd+hh+mi+ss+ms;
        return formatdate;
    }
    
     /*******************************************************************************************************************************/
    //Generic method for fields for SOQL query
      @testvisible public static string getCreatableFieldsSOQL(string objectName){
        String selects = '';
        // Get a map of field name and field token
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();
        if (fMap != null){
            for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
               // if (fd.isCreateable()){ // field is creatable
                    selectFields.add(fd.getName());
              //  }
            }
        }
        // contruction of SOQL
        if (!selectFields.isEmpty()){
            for (string s:selectFields){
                selects += s + ',';
            }
            if (selects.endsWith(',')){selects = selects.substring(0,selects.lastIndexOf(','));}
             
        }
        // return constrcucted query
        return 'SELECT ' + selects + ' FROM ' + objectName ;
         
    }
}