/*
Class to do Callout to SMS provider with the Inquiry Mobile Numbers
Developed By: DAMAC IT Team
*/

global class Async_Rest_SMS implements Queueable, Database.AllowsCallouts {


    public string marketingId;
    public string SMSMessage;
    public string CampSMSid;
    public list<inquiry__c> inquiries;
    public Set<String> mobileNumbers;
    public String objectName;
    
    public Async_Rest_SMS(string id, string msg, list<inquiry__c> inq,string campsmsrecId){
        marketingId = id;
        SMSMessage = msg;
        inquiries = inq;
        CampSMSid = campsmsrecId;
    }

    public Async_Rest_SMS(String campsmsrecId,Set<String> mobileNumbers,String msg,String objectName) {
        this.mobileNumbers = mobileNumbers;
        this.objectName = objectName;
        CampSMSid = campsmsrecId;
        SMSMessage = msg;
    }

    public void execute(QueueableContext context) {
        try {
            sendSMS(marketingId,SMSMessage,inquiries,CampSMSid,objectName,mobileNumbers);
        }
        catch (Exception e) {
            system.debug('----exception---'+e.getMessage());
            system.debug('------Exception----'+e.getStackTraceString());
        }
    }

    public string prepareBody(Secure_SMS__mdt SMS_BULKSMS, string senderName, set<string> MobileNumbers, string message, campaign_sms__c schTime){
        // Prepare the SMS Request from Json Object
        DAMAC_SMS_REQUEST_OBJ reqObj = new DAMAC_SMS_REQUEST_OBJ();
        reqObj.Message = message;
        reqObj.SenderName = SMS_BULKSMS.sender_name__c;
        reqObj.ScheduledDate = schTime.Scheduled_Time_for_SMS_Request__c;//string.valueof(system.today());
        reqObj.RemoveDuplicates = true;
        if(String.isNotBlank(objectName)) {
            reqObj.ReferenceName = objectName+'_'+string.valueof(schTime.Scheduled_Time_for_SMS_Request__c);
        }
        else {
            reqObj.ReferenceName = 'MARKETING_'+string.valueof(schTime.Scheduled_Time_for_SMS_Request__c);
        }
        reqObj.ReturnIndividualResponse = false;
        reqObj.AsynchronousSubmission = SMS_BULKSMS.Asynchronous_Submission__c;
        list<string> MobileNumberslst = new list<string>();
        MobileNumberslst.addAll(MobileNumbers);
        reqObj.MobileNumbers = MobileNumberslst;
        return JSON.serialize(reqObj);
    }
    

    // Send SMS Calout
    public void sendSMS(string marketingId,string message,list<inquiry__c> inqs,string campsmsrecId,
                        String objectName,Set<String> mobileNumbers) {

        // Get the Credentials and Dynamic Attributes from the Metadata
        Secure_SMS__mdt SMS_BULKSMS = [select id,User_Name__c,password__c,Endpoint__c,sender_name__c,Asynchronous_Submission__c from Secure_SMS__mdt where DeveloperName='SMS_BULKSMS' LIMIT 1];
        campaign_sms__c schTime = [select id,Scheduled_Time_for_SMS_Request__c from campaign_sms__c where id=:campsmsrecId LIMIT 1];
        
        //Added by Charan on 2nd Nov 2020
        set<string> uaeNos = new set<string>();
        set<string> saudiNos = new set<string>();
        set<string> others = new set<string>();
        
        string bodyStr;
        if(mobileNumbers != null && !mobileNumbers.isEmpty()) {
            for(string mobilePhone : mobileNumbers){
                mobilePhone = mobilePhone.removeStart('00');
                mobilePhone = mobilePhone.removeStart('+');
                if(mobilePhone.startsWith('971'))
                    uaeNos.add(mobilePhone);
                else if(mobilePhone.startsWith('966'))
                    saudiNos.add(mobilePhone);
                else
                    others.add(mobilePhone);
            }
            //MobileNumberslst.addAll(mobileNumbers);
        }
        else {
            list<inquiry__c> allInquiries = [select id, Mobile_Phone__c, Mobile_CountryCode__c from inquiry__c where id in:inqs LIMIT 50000];
            for(inquiry__c i:allInquiries){
                string senderName = system.label.Common_Sender_Name;
                string mobilePhone = i.Mobile_Phone__c;
                if(i.Mobile_CountryCode__c == 'United Arab Emirates: 00971')
                    uaeNos.add(mobilePhone);
                else if(i.Mobile_CountryCode__c == 'Saudi Arabia: 00966')
                    saudiNos.add(mobilePhone);
                else
                    others.add(mobilePhone);
            }
        }
        HTTPResponse uaeResponse = new HTTPResponse ();
        HTTPResponse saudiResponse = new HTTPResponse ();
        HTTPResponse othersResponse = new HTTPResponse ();
        
        string bodytosend = '';
        if(!uaeNos.isEmpty()){
            bodytosend = prepareBody(SMS_BULKSMS, System.label.UAE_Sender_Name, uaeNos, message, schTime);
            uaeResponse = sendRequest(SMS_BULKSMS, bodytosend);
        }
        if(!saudiNos.isEmpty()){
            bodytosend = prepareBody(SMS_BULKSMS, System.label.Saudi_Sender_Name, saudiNos, message, schTime);
            saudiResponse = sendRequest(SMS_BULKSMS, bodytosend);
        }
        if(!others.isEmpty()){
            bodytosend = prepareBody(SMS_BULKSMS, System.label.Common_Sender_Name, others, message, schTime);
            othersResponse = sendRequest(SMS_BULKSMS, bodytosend);
        }
        
        //Need to create Logs based on all 3 responses.
        if (!uaeNos.isEmpty ()) {
            if(uaeResponse.getstatuscode() == 200){
                // Create Success Log.
                createLog(marketingId,message,uaeResponse.getBody(),true,CampSMSid);
            }else {
                // Create Error log
                createLog(marketingId,message,uaeResponse.getBody(),false,CampSMSid);
            }
        }
        if (!saudiNos.isEmpty ()) {
            if(saudiResponse .getstatuscode() == 200){
                // Create Success Log.
                createLog(marketingId,message,saudiResponse .getBody(),true,CampSMSid);
            }else {
                // Create Error log
                createLog(marketingId,message,saudiResponse .getBody(),false,CampSMSid);
            }
        }
        if (!others.isEmpty ()) {
            if(othersResponse .getstatuscode() == 200){
                // Create Success Log.
                createLog(marketingId,message,othersResponse .getBody(),true,CampSMSid);
            }else {
                // Create Error log
                createLog(marketingId,message,othersResponse .getBody(),false,CampSMSid);
            }
        }
        
    }
    
    public HTTPResponse sendRequest(Secure_SMS__mdt SMS_BULKSMS, string bodytosend){
        STRING endpointtosend = SMS_BULKSMS.Endpoint__c+'username='+SMS_BULKSMS.User_Name__c+'&password='+SMS_BULKSMS.password__c;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();        
        req.setEndpoint(endpointtosend); 
        req.setMethod('POST');
        req.setheader('Content-Type','application/json'); 
        req.setbody(bodytosend);  
        HttpResponse res = h.send(req);
        system.debug(res.getBody()); 
        return res;
    }
    
    // Create Log Object
    public void createLog(string marketingId,string message,string smsResponse,boolean success,string campsmsrecId){
        DAMAC_SMS_Get_BulkId respCls = new DAMAC_SMS_Get_BulkId();
        respCls = DAMAC_SMS_Get_BulkId.parse(smsResponse);
        system.debug(respCls.Data.BulkId);
        string bulkid = string.valueof(respCls.Data.BulkId);
        Campaign_sms__c smsLog = [select id,Error_From_Provider__c,Marketing_campaign__c,SMS_Response__c,SMS_Message__c,SMS_Bulk_Id__c,SMS_Queued__c from campaign_sms__c where id=:CampSMSid];
        smsLog.Marketing_campaign__c = marketingId;
        smsLog.SMS_Response__c = smsResponse;
        smsLog.SMS_Message__c = message;
        smslog.SMS_Bulk_Id__c = bulkid;
        if(success){
            smslog.SMS_Queued__c = success;
        }else{
            smslog.Error_From_Provider__c = false;
        }
        
        update smsLog;
        if(bulkid != null && bulkid != ''){
            system.debug('-------bulkid----'+bulkid);
        // When the Bulk Id is generated Call the JobStatus Callout class to get the Status of the SMS Job
            //System.enqueueJob(new Async_Rest_SMS_Job_Status(bulkid));
        }
    }
    
    

}