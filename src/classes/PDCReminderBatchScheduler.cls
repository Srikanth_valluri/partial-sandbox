global class PDCReminderBatchScheduler implements Schedulable {
    
    // Execute at regular intervals
    global void execute(SchedulableContext ctx){
      //String soql = 'SELECT Id, Name FROM Account';
      PDCReminderBatch batch = new PDCReminderBatch();
      Database.executebatch(batch, 200);
    }
}