@isTest
public class ContactDetailsVerificationTest {
    @IsTest
    static void positiveTestCaseForAccount(){
        Id accountRecordId =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAccount = new Account();
        objAccount.LastName= 'Test Account';
        objAccount.Mobile_Country_Code__pc = 'India: 0091';
        objAccount.Mobile_Phone__pc = '9145678905';
        objAccount.Mobile_Country_Code_2__pc = 'India: 0091';
        objAccount.Mobile_Phone_2__pc = '7895679845';
        objAccount.RecordTypeId = accountRecordId;
        insert objAccount;
        ContactDetailsVerification.getAccountDetailsOnInit(objAccount.Id);
    }
    
    @IsTest
    static void positiveTestCaseForCallingList(){
        Id accountRecordId =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAccount = new Account();
        objAccount.LastName= 'Test Account';
        objAccount.Mobile_Country_Code__pc = 'India: 0091';
        objAccount.Mobile_Phone__pc = '9145678905';
        objAccount.Mobile_Country_Code_2__pc = 'India: 0091';
        objAccount.Mobile_Phone_2__pc = '7895679845';
        objAccount.RecordTypeId = accountRecordId;
        insert objAccount;
        
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
        OnOffCheck__c = true);

        settingLst2.add(newSetting1);
        insert settingLst2;
        
        Calling_List__c objCalling = new Calling_List__c();
        objCalling.RecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
        objCalling.Account_Email__c = 'test@t.com';
        objCalling.Account__c = objAccount.Id;
        objCalling.RTP_Escl_Date__c = system.now();        
        insert objCalling;
        
        ContactDetailsVerification.getAccountDetailsOnInit(objCalling.Id);
    
    }
    
    @IsTest
    static void positiveTestCaseForCallingList2(){
        Id accountRecordId =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAccount = new Account();
        objAccount.LastName= 'Test Account';
        objAccount.Mobile_Country_Code__pc = 'India: 0091';
        objAccount.Mobile_Phone__pc = '9145678905';
        objAccount.Mobile_Country_Code_2__pc = 'India: 0091';
        objAccount.Mobile_Phone_2__pc = '7895679845';
        objAccount.RecordTypeId = accountRecordId;
        insert objAccount;
        
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
        OnOffCheck__c = true);

        settingLst2.add(newSetting1);
        insert settingLst2;
        
        Calling_List__c objCalling = new Calling_List__c();
        objCalling.RecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
        objCalling.Account_Email__c = 'test@t.com';
        objCalling.Account__c = objAccount.Id;
        objCalling.RTP_Escl_Date__c = system.now();        
        insert objCalling;
        String fieldList = '[{"fieldName":"Mobile_Phone__pc","isEmail":true,"isVerified":false,"label":"Email","maskedValue":"ram.xxxxxx.com","value":"9145678905"},{"fieldName":"Mobile_Phone_2__pc","isEmail":true,"isVerified":false,"label":"Email","maskedValue":"ram.xxxxxx.com","value":"7895679845"}]';
        ContactDetailsVerification.getAccountdetails(objAccount.Mobile_Phone__pc,'',objCalling.Account__c); 
    }
    
}