/***********************************************************************************************
* Description - Test class developed for CreatePaymentTermsforEHO

*
* Version            Date            Author                    Description
* 1.0                18/12/17        Naresh Kaneriya (Accely)   Initial Draft
*********************************************************************************************/

@isTest
public class CreatePaymentTermsforEHOTest{
  
  @isTest static void getTest(){
  
        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('NOC For Visa').getRecordTypeId();
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null); 
        
        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);
     
        Case   Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Type = 'NOCVisa';
        Cas.NewPaymentTermJSON__c = '[{"termId":"68708","PTStartDate":null,"percentValue":"24","paymentDate":"20/11/2015","paymentAmount":"1759680","name":null,"mileStoneEventUpdate":null,"mileStoneEvent":"Immediate","mileStoneArabic":"فورا","lineId":"380012","leaseAmount":null,"isReceiptPresent":true,"isPDC":null,"installmentAmount":null,"installment":"DP","Id":null,"description":"DEPOSIT"},{"termId":"68708","PTStartDate":null,"percentValue":"0","paymentDate":"","paymentAmount":null,"name":null,"mileStoneEventUpdate":null,"mileStoneEvent":null,"mileStoneArabic":null,"lineId":"380013","leaseAmount":null,"isReceiptPresent":true,"isPDC":null,"installmentAmount":null,"installment":"I001","Id":null,"description":"1ST INSTALLMENT"},{"termId":"68708","PTStartDate":null,"percentValue":"10","paymentDate":"14/11/2016","paymentAmount":"879840","name":null,"mileStoneEventUpdate":null,"mileStoneEvent":"Within 360 days of Sale Date","mileStoneArabic":"خلال 360 يوما من تاريخ البيع","lineId":"380014","leaseAmount":null,"isReceiptPresent":true,"isPDC":null,"installmentAmount":null,"installment":"I002","Id":null,"description":"2ND INSTALLMENT"},{"termId":"68708","PTStartDate":null,"percentValue":"10","paymentDate":"13/5/2017","paymentAmount":"879840","name":null,"mileStoneEventUpdate":null,"mileStoneEvent":"Within 540 days of Sale Date","mileStoneArabic":"خلال 540 يوما من تاريخ البيع","lineId":"538926","leaseAmount":null,"isReceiptPresent":true,"isPDC":null,"installmentAmount":null,"installment":"I003","Id":null,"description":"3RD INSTALLMENT"},{"termId":"68708","PTStartDate":null,"percentValue":"10","paymentDate":"09/11/2017","paymentAmount":"879840","name":null,"mileStoneEventUpdate":null,"mileStoneEvent":"Within 720 days of Sale Date","mileStoneArabic":"خلال 720 يوما من تاريخ البيع","lineId":"538927","leaseAmount":null,"isReceiptPresent":true,"isPDC":null,"installmentAmount":null,"installment":"I004","Id":null,"description":"4TH INSTALLMENT"},{"termId":"68708","PTStartDate":"2018-02-22","percentValue":"10","paymentDate":"08/5/2018","paymentAmount":null,"name":null,"mileStoneEventUpdate":null,"mileStoneEvent":"Within 900 days of Sale Date","mileStoneArabic":"خلال 900 يوما من تاريخ البيع","lineId":"538928","leaseAmount":"8219.18","isReceiptPresent":false,"isPDC":null,"installmentAmount":"871620.82","installment":"I005","Id":null,"description":"5TH INSTALLMENT"},{"termId":"68708","PTStartDate":"2018-05-08","percentValue":"10","paymentDate":"04/11/2018","paymentAmount":null,"name":null,"mileStoneEventUpdate":null,"mileStoneEvent":"Within 1080 days of Sale Date","mileStoneArabic":"خلال 1080 يوما من تاريخ البيع","lineId":"538929","leaseAmount":"19726.03","isReceiptPresent":false,"isPDC":null,"installmentAmount":"860113.97","installment":"I006","Id":null,"description":"6TH INSTALLMENT"},{"termId":"68708","PTStartDate":"2018-11-04","percentValue":"10","paymentDate":"03/5/2019","paymentAmount":null,"name":null,"mileStoneEventUpdate":null,"mileStoneEvent":"Within 1260 days of Sale Date","mileStoneArabic":"خلال 1260 يوما من تاريخ البيع","lineId":"538930","leaseAmount":"19726.03","isReceiptPresent":false,"isPDC":null,"installmentAmount":"860113.97","installment":"I007","Id":null,"description":"7TH INSTALLMENT"},{"termId":"68708","PTStartDate":"2019-05-03","percentValue":"10","paymentDate":"30/10/2019","paymentAmount":null,"name":null,"mileStoneEventUpdate":null,"mileStoneEvent":"Within 1440 days of Sale Date","mileStoneArabic":"خلال 1440 يوما من تاريخ البيعع","lineId":"538931","leaseAmount":"19726.03","isReceiptPresent":false,"isPDC":null,"installmentAmount":"860113.97","installment":"I008","Id":null,"description":"8TH INSTALLMENT"},{"termId":"68708","PTStartDate":"2019-10-30","percentValue":"10","paymentDate":"27/4/2020","paymentAmount":null,"name":null,"mileStoneEventUpdate":null,"mileStoneEvent":"Within 1620 days of Sale Date","mileStoneArabic":"خلال 1620 يوما من تاريخ البيع","lineId":"538932","leaseAmount":"19726.03","isReceiptPresent":false,"isPDC":null,"installmentAmount":"860113.97","installment":"I009","Id":null,"description":"9TH INSTALLMENT"}]';
        insert Cas;
        System.assert(Cas != null);
         
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);  
        set<Id> BUIds = new set<Id>();  
        BUIds.add(BU.Id);
        
        Payment_Terms__c PT = new Payment_Terms__c();
        PT.Booking_Unit__c = BU.Id;
        PT.CRM_Migrated_Data__c = true;
        PT.Description__c = 'Test';
        PT.Event_Days__c = '2';
        PT.Installment__c = '3';
        PT.Milestone_Event__c = 'Event';
        PT.Milestone_Event_Arabic__c = 'Test';
        PT.Registration_Id__c = '1254';        
        PT.Expected_Date__c = System.today()+3;
        PT.Payment_Date__c = System.today();
        PT.PDC_Date__c = System.today();
        insert PT;
        
      Test.startTest();  
      SOAPCalloutServiceMock.returnToMe = new Map<String,HandoverMQServices.EarlyHandoverPaymentPlanCreationResponse_element>();
      HandoverMQServices.EarlyHandoverPaymentPlanCreationResponse_element response1 = new HandoverMQServices.EarlyHandoverPaymentPlanCreationResponse_element();
      response1.return_x = '{'+
        '  "message": "Test", '+
        '    "status":"S"  '+
        '}';
      SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
      Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
      
      CreatePaymentTermsforEHO.EarlyHandoverPaymentPlanCreationResponce(Cas.Id); 
      Test.stopTest();
    }
}