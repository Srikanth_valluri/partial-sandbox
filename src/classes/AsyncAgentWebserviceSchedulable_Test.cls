@isTest
public class AsyncAgentWebserviceSchedulable_Test {
    
    @testSetup 
    static void setupData() {
        Account acc = new Account(name='ABC');
        insert acc; 
        
        Agent_Site__c agentSite = new Agent_Site__c();
        agentSite.NAme = 'UAE';
        agentSite.Agency__c = acc.Id;
        agentSite.Active__c = true;
        insert agentSite;
    }
    
    
    @isTest
    static void testAgentWebserviceSchedulable() {
        Test.startTest();
        
        map<Id, Agent_Site__c> AgentSiteIdList = new map<Id, Agent_Site__c>([Select Id From Agent_Site__c]);
        
        AsyncAgentWebserviceSchedulable scheduledJob = new AsyncAgentWebserviceSchedulable(new List<Id>(AgentSiteIdList.keySet()));
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Account Sales Calculator Check', sch, scheduledJob); 
        Test.stopTest();
    }
}