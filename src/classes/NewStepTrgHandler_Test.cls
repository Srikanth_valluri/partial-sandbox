/**************************************************************************************************
* Name               : NewStepTrgHandler_Test
* Test Class         : Test class for NewStepTrgHandler
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR          DATE            Comments
* 1.0                         31/07/2019      Created
**************************************************************************************************/
@isTest
public class NewStepTrgHandler_Test {
    @testSetup 
    static void setupData() {
        //IPMS_Code_Settings__c custom settings
        List<IPMS_Code_Settings__c> ipmsCodeList = new List<IPMS_Code_Settings__c>();
        IPMS_Code_Settings__c code1 = new IPMS_Code_Settings__c(Name = 'SPA Executed', Step_Type__c = 'SPA Execution', Step_Status__c = 'SPA Executed', 
                                                                Status_Code__c = 'LE', Check_on_DP_OK__c = False, Check_on_Doc_OK__c = False, 
                                                                Doc_OK__c = true, DP_OK__c = true);
        IPMS_Code_Settings__c code2 = new IPMS_Code_Settings__c(Name = 'Mid Office Approved', Step_Type__c = 'Mid Office Approval', 
                                                                Step_Status__c = 'Mid Office Approved',  Status_Code__c = 'AG', 
                                                                Check_on_DP_OK__c = False, Check_on_Doc_OK__c = False, 
                                                                Doc_OK__c = true, DP_OK__c = true);
        IPMS_Code_Settings__c code3 = new IPMS_Code_Settings__c(Name = 'More Docs Uploaded', Step_Type__c = 'Awaiting More Docs', 
                                                                Step_Status__c = 'More Docs Uploaded', Status_Code__c = 'SS', 
                                                                Check_on_DP_OK__c = False, Check_on_Doc_OK__c = False, 
                                                                Doc_OK__c = true, DP_OK__c = true);
        IPMS_Code_Settings__c code4 = new IPMS_Code_Settings__c(Name = 'Docs Ok', Step_Type__c = 'Document Verification', Step_Status__c = 'Docs Ok', 
                                                                Status_Code__c = 'LB', Check_on_DP_OK__c = False, Check_on_Doc_OK__c = False, 
                                                                Doc_OK__c = true, DP_OK__c = true);
        ipmsCodeList.add(code1);
        ipmsCodeList.add(code2);
        ipmsCodeList.add(code3);
        ipmsCodeList.add(code4);  
        insert ipmsCodeList;

        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;

        Id DealRT = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = acc.Id;
        sr.RecordTypeId = DealRT;
        sr.Agency__c = acc.Id;
        sr.Agency_Type__c = 'Corporate';
        insert sr;

        New_Step__c nstp1 = new New_Step__c();
        nstp1.Service_Request__c = sr.id;
        nstp1.Step_No__c = 50;
        nstp1.Step_Status__c = 'Awaiting SPA Execution';
        nstp1.Step_Type__c = 'SPA Execution';
        insert nstp1;

        Booking__c booking = new Booking__c(Account__c = acc.Id, Deal_SR__c = sr.Id);
        insert booking;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id, Unit_Name__c = 'Test name 1',
                                                          Registration_ID__c = '1600', Registration_Status__c = 'Active Status',
                                                          Unit_Selling_Price_AED__c = 100);
        insert bookingUnit;

        //for UK Deal
        List<Document_Template__c> docTemplatesList = new List<Document_Template__c>();
        Document_Template__c docT1 = new Document_Template__c();
        docT1.Name = 'Reservation Form';
        docT1.SR_Record_Type__c = 'Deal';
        docT1.Added_through_Code__c = true;
        docT1.Is_UK__c = true;
        docT1.Step_Type__c = 'Upload Docs';
        docT1.Step_Status__c = 'Awaiting Docs';
        docT1.Document_Type__c = 'Single';
        docTemplatesList.add(docT1);

        Document_Template__c docT2 = new Document_Template__c();
        docT2.Name = 'Reservation Form';
        docT2.SR_Record_Type__c = 'Deal';
        docT2.Added_through_Code__c = true;
        docT2.Is_UK__c = true;
        docT2.Step_Type__c = 'Upload Docs';
        docT2.Step_Status__c = 'Awaiting Docs';
        docT2.Document_Type__c = 'Multi';
        docT2.Related_Object__c = 'Booking_Unit__c';
        docTemplatesList.add(docT2);

        Document_Template__c docT3 = new Document_Template__c();
        docT3.Name = 'Reservation Form';
        docT3.SR_Record_Type__c = 'Deal';
        docT3.Added_through_Code__c = true;
        docT3.Is_UK__c = true;
        docT3.Step_Type__c = 'Upload Docs';
        docT3.Step_Status__c = 'Awaiting Docs';
        docT3.Document_Type__c = 'Multi';
        docT3.Related_Object__c = 'Buyer__c';
        docT3.Buyer_Type__c = 'Individual';
        docT3.Primary_Buyer__c = true;
        docTemplatesList.add(docT3);
        insert docTemplatesList;

        NSIBPM__Service_Request__c ukSR = new NSIBPM__Service_Request__c();
        ukSR.NSIBPM__Customer__c = acc.Id;
        ukSR.RecordTypeId = DealRT;
        ukSR.Agency__c = acc.Id;
        ukSR.Agency_Type__c = 'Corporate';
        ukSR.Is_UK_Deal__c = true;
        insert ukSR;

        Booking__c ukBooking = new Booking__c(Account__c = acc.Id, Deal_SR__c = ukSR.Id);
        insert ukBooking;

        Booking_Unit__c ukBookingUnit = new Booking_Unit__c(Booking__c = ukBooking.Id, Unit_Name__c = 'Test name 2',
                                                            Registration_ID__c = '16700', Registration_Status__c = 'Active Status',
                                                            Unit_Selling_Price_AED__c = 200);
        insert ukBookingUnit;

        Buyer__c buyer = new Buyer__c();
        buyer.Account__c = acc.Id;
        buyer.Buyer_Type__c = 'Individual';
        buyer.Address_Line_1__c = 'Ad1';
        buyer.Country__c =  'United Arab Emirates';
        buyer.City__c = 'Dubai';
        buyer.Booking__c = ukBooking.Id;
        buyer.Date_of_Birth__c = string.valueof(system.today().addyears(-30));
        buyer.Email__c = 'test@test.com';
        buyer.First_Name__c = 'firstname';
        buyer.Primary_Buyer__c = true;
        buyer.Last_Name__c = 'lastname';
        buyer.Nationality__c = 'Indian';
        buyer.Passport_Expiry_Date__c = string.valueof(system.today().addyears(20));
        buyer.Passport_Number__c = 'J0';
        buyer.Phone__c = '569098767';
        buyer.Phone_Country_Code__c = 'United Arab Emirates: 00971';
        buyer.Place_of_Issue__c = 'India';
        buyer.Title__c = 'Mr';
        buyer.EID_No__c = '123456';
        buyer.EID_Expiry_Date__c = null;
        buyer.Primary_Buyer__c = true;
        insert buyer;
        try{
        New_Step__c ukStp1 = new New_Step__c();
        ukStp1.Service_Request__c = ukSR.Id;
        ukStp1.Step_No__c = 3.1;
        ukStp1.Step_Status__c = 'Awaiting Docs';
        ukStp1.Step_Type__c = 'Upload Docs';
        insert ukStp1;
        } catch(exception e){} 
        
        Reservation_Form_Check_List__c checkList = new Reservation_Form_Check_List__c();
        checkList.Checklist_Item__c = 'test item';
        checkList.Subheading__c = 'test subheading';
        checkList.Validated__c = false;
        checkList.Service_Request__c = sr.Id;
        insert checkList;                                                   

    }

    @isTest
    static void test_method1() {
        New_Step__c step = [SELECT Id, Service_Request__c FROM New_Step__c LIMIT 1];
       // step.Step_Status__c = 'SPA Executed';
        //call rest of methods
        New_Step__c stp = new New_Step__c(Service_Request__c = step.Service_Request__c, Step_No__c  = 2.2, 
                            Comments__c='Test Comments', Step_Type__c = 'Mid Office Approval', 
                            Step_Status__c = 'Mid Office Rejected', Is_Closed__c =TRUE);
        insert stp;
         
         
        test.startTest();
        New_Step__c stp2 = new New_Step__c(Service_Request__c = step.Service_Request__c, Step_No__c  = 2.2, 
                            Comments__c='Test Comments', Step_Type__c = 'Mid Office Approval', 
                            Step_Status__c = 'Mid Office Approved', Is_Closed__c =TRUE);
        insert stp2;
        NewStepTrgHandler NewStepHandler = new NewStepTrgHandler();
        NewStepHandler.executeBeforeInsertTrigger(new list<sObject>());
        NewStepHandler.executeBeforeDeleteTrigger(new Map<Id, sObject>());
        NewStepHandler.executeBeforeInsertUpdateTrigger(new list<sObject>(), new Map<Id,sObject>());
        NewStepHandler.executeAfterInsertTrigger(new Map<Id, sObject>());
        NewStepHandler.executeAfterUpdateTrigger(new Map<Id, sObject>(), new Map<Id, sObject>());
        NewStepHandler.executeAfterDeleteTrigger(new Map<Id, sObject>());
        NewStepHandler.executeAfterInsertUpdateTrigger(new Map<Id, sObject>(), new Map<Id, sObject>());
        New_Step__c stp4 = new New_Step__c(Service_Request__c = step.Service_Request__c, Step_No__c  = 2.2, 
                            Step_Type__c = 'Revised URRF Upload', 
                            Step_Status__c = 'Awaiting Revised URRF');
        insert stp4;
        New_Step__c stp5 = new New_Step__c(Service_Request__c = step.Service_Request__c, Step_No__c  = 2.2, 
                            Step_Type__c = 'Revised Token Document Upload', 
                            Step_Status__c = 'Awaiting Token Document Upload');
        insert stp5;
        
        
        test.stopTest();
    }
}