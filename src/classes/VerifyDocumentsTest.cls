/****************************************************************************************
* Description - Test class developed for VerifyDocuments
*
* Version            Date            Author                              Description
* 1.0                23/11/17        Naresh Kaneriya (Accely)           Initial Draft
****************************************************************************************/
@isTest
public class VerifyDocumentsTest{

@isTest static void VerifyDocumentsTest(){
    
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        System.assert(objAccount!=null);
        
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
		Case objCase = TestDataFactory_CRM.createCase(objAccount.Id, caseRecordTypeId);
        insert objCase;
    
        Task TaskToinsert =  new Task();
        TaskToinsert.WhatId = objCase.Id;
        TaskToinsert.Subject  = 'Verification Pending';
        TaskToinsert.ActivityDate  = System.today()+2;
        TaskToinsert.Assigned_User__c  = UserInfo.getUserId();
        TaskToinsert.Process_Name__c  = 'Assignment';
        TaskToinsert.Priority = 'High';
        TaskToinsert.Status = 'Not Started';
        insert TaskToinsert;  

        SR_Attachments__c SrAt =  new SR_Attachments__c();
        SrAt.Case__c = objCase.Id;
        SrAt.isValid__c = true;
        SrAt.Name = TaskToinsert.Subject.replace('Verification Pending','').trim();
        insert SrAt;

        List<ID> ids =  new List<ID>();
        ids.add(TaskToinsert.Id);
        VerifyDocuments.closeTask(ids);
 }
}