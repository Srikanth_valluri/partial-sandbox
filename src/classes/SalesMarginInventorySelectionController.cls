public class SalesMarginInventorySelectionController {
    public string salesMarginId {get;set;}
    
    public transient list<string> prodNames{get;set;}
    public transient list<string> buildLocFilter{get;set;}
    public transient list<string> floorLocFilter{get;set;}
    public transient list<string> propertyNameFilter{get;set;}
    public transient list<string> acdFilter{get;set;}
    public transient list<string> bedRoomFilter{get;set;}
    public transient list<string> viewTypeFilter{get;set;}
    public Integer invSize {get; set;}
    public String status {get; set;}
    public boolean invPresent {get; set;}
    public boolean loadPage {get; set;}
    public Sales_Margins__c salesMargin{get; set;}
    
    List<String> productnameList = new List<String>();
    set<string> propertyNames = new set<string>();
    set<string> acd = new set<string>();
    set<string> bedrooms= new set<string>();
    set<string> viewTypes = new set<string>();
    set<string> buildingLocations = new set<String>();
    set<string> floorLocations = new set<String>();
    public boolean invExists {get; set;}
    
    public SalesMarginInventorySelectionController(){}
    public SalesMarginInventorySelectionController(ApexPages.StandardController controller){
        invSize = 0;
        loadPage = false;
        invExists = false;
        salesMargin = new Sales_Margins__c();
        if (apexpages.currentpage() != null) {
            salesMarginId = apexpages.currentpage().getparameters().get('id');
        }
        if (String.isBlank(salesMarginId)) {
            salesMarginId = controller.getId();
        }
        if(salesMarginId != null && salesMarginId != ''){
            try{
                salesMargin = [SELECT Id, Name,Additional_Discount__c, Avg_PSF_on_AC_Area__c,
                                   DLD_Waiver_Guideline__c, DLD_Waiver_100_Upfront__c,
                                   RG_Guideline__c, RG_100_Upfront__c,
                                   VAT_Guideline__c, VAT_100_Upfront__c,
                                   Effective_Discount_Guideline__c, Effective_Discount_100_Upfront__c,
                                   Effective_Discount_Guideline_Agent__c, Effective_Discount_100_Upfront_Agent__c,
                                   Avg_System_PSF__c, Discount_Policy_Name__c, Max_Discount_On_100_upfront__c,
                                   Max_Suggested_Discount_On_system_PSF__c, 
                                   Net_PSF_AC_Standard__c, Net_PSF_AC_Upfront__c,
                                   Net_PSF_Sellable_Standard__c, Net_PSF_Sellable_Upfront__c,
                                   Net_Standard_Discount__c, Net_Upfront_Discount__c,
                                   PSF_AC_Standard__c, PSF_AC_Upfront__c,
                                   PSF_Sellable_Standard__c, PSF_Sellable_Upfront__c,
                                   Marketing_Name__c, Property_Name__c,
                                   Bedroom_Type__c, View_Type__c,
                                   Building_Location__c , Floor_Location__c, ACD__c
                               FROM Sales_Margins__c WHERE Id =: salesMarginId 
                             //  AND Is_Active__c = TRUE
                               ];
                pageload();
                loadPage = true;
                invExists = true;
            } catch(exception e){
                system.debug('Error while fetching Sales Margin Record: ' + e);
            }
        } else{
            loadPage = true;
             pageload();
        }
    }
    
    public string filterString(List<String> filterList){
        String val = '';
        if(filterList != null && filterList.size() > 0){
            for(String filter: filterList){
                if(val != ''){
                    val += ';';
                }
                val += filter;
            }
        }
        return val;
    }

    public PageReference applySalesMargin(){
        try{
            system.debug('salesMargin: ' +salesMargin);
            if(salesMargin != null){
                salesMargin.Marketing_Name__c = filterString(prodNames);
                salesMargin.Building_Location__c = filterString(buildLocFilter);
                salesMargin.Floor_Location__c = filterString(floorLocFilter);
                salesMargin.Property_Name__c = filterString(propertyNameFilter);
                salesMargin.Bedroom_Type__c = filterString(bedRoomFilter);
                salesMargin.View_Type__c = filterString(viewTypeFilter);
                salesMargin.ACD__c = filterString(acdFilter);
                system.debug('salesMargin: ' +salesMargin);
                upsert salesMargin;
                salesMarginId = salesMargin.Id;
            }
        } catch(exception e){
            system.debug('Exception while upserting Sales Margin: ' + e);
            return null;
        }
        String reqURL = System.URL.getSalesforceBaseUrl().toExternalForm()  + '/' +  salesMarginId;
       //                 + '/apex/SalesMarginInventorySelection';
       // if(salesMarginId != null && salesMarginId != ''){
        //    reqURL += '?id=' + salesMarginId;
       // }
        PageReference invReqPage = new PageReference(reqURL);
        invReqPage.setRedirect(true);
        return invReqPage;
    }

    public void pageload(){
        prodNames = new list<string>();
        buildLocFilter  = new list<string>();
        floorLocFilter  = new list<string>();
        propertyNameFilter  = new list<string>();
        acdFilter = new list<string>();
        bedRoomFilter =  new list<string>();
        viewTypeFilter = new list<String>();
        list<inventory__c> invS = new list<inventory__c>();
        invS  = [SELECT Id, Floor_Package_Name__c, 
                        Bedroom_Type__c, Building_Location__r.Name, property_name__c, 
                        Anticipated_Completion_Date__c, View_Type__c, Floor_Location__r.Name,
                        Marketing_Name_Doc__r.name, Marketing_Name_Doc__c 
                 FROM inventory__c 
                  WHERE Status__c != 'Sold' AND unit_Location__c != null
                 ORDER BY CreatedDate DESC LIMIT 40000 ];
        if(salesMargin != null && salesMarginId != null && salesMarginId != ''){
            if(salesMargin.Marketing_Name__c != null && salesMargin.Marketing_Name__c != ''){
                if(!prodNames.contains(salesMargin.Marketing_Name__c)){
                    prodNames.addAll(salesMargin.Marketing_Name__c.split(';'));
                }
            }
            system.debug('prodNames: ' + prodNames);
            if(salesMargin.Property_Name__c != null && salesMargin.Property_Name__c != ''){
                if(!propertyNameFilter.contains(salesMargin.Property_Name__c)){
                   propertyNameFilter.addAll(salesMargin.Property_Name__c.split(';'));
                }
            }
            system.debug('propertyNameFilter: ' + propertyNameFilter);
            if(salesMargin.Bedroom_Type__c != null && salesMargin.Bedroom_Type__c != ''){
                if(!bedRoomFilter.contains(salesMargin.Bedroom_Type__c)){
                    bedRoomFilter.addAll(salesMargin.Bedroom_Type__c.split(';'));
                }
            }
            system.debug('bedRoomFilter: ' + bedRoomFilter);
            if(salesMargin.View_Type__c != null && salesMargin.View_Type__c != ''){
                if(!viewTypeFilter.contains(salesMargin.View_Type__c)){
                    viewTypeFilter.addAll(salesMargin.View_Type__c.split(';'));
                }
            }
            system.debug('viewTypeFilter: ' + viewTypeFilter);
            if(salesMargin.Building_Location__c != null && salesMargin.Building_Location__c != ''){
                if(!buildLocFilter.contains(salesMargin.Building_Location__c)){
                    buildLocFilter.addAll(salesMargin.Building_Location__c.split(';'));
                }
            }
            system.debug('buildLocFilter: ' + buildLocFilter);
            if(salesMargin.Floor_Location__c != null && salesMargin.Floor_Location__c != ''){
                if(!floorLocFilter.contains(salesMargin.Floor_Location__c)){
                    floorLocFilter.addAll(salesMargin.Floor_Location__c.split(';'));
                }
            }
            system.debug('floorLocFilter: ' + floorLocFilter);
            if(salesMargin.ACD__c != null && salesMargin.ACD__c != ''){
                if(!acdFilter.contains(salesMargin.ACD__c)){
                    acdFilter.addAll(salesMargin.ACD__c.split(';'));
                }
            }
            system.debug('acdFilter: ' + acdFilter);
        }
      
        if(invS.size() > 0){
            for(inventory__c inv: invs){
                if(inv.Marketing_Name_Doc__c != null){
                    productnameList.add(inv.Marketing_Name_Doc__r.name);  
                }
                if(inv.property_name__c != null){
                    propertyNames.add(inv.property_name__c.toUpperCase());
                }
                if(inv.Anticipated_Completion_Date__c != null){
                    acd.add(inv.Anticipated_Completion_Date__c.toUpperCase());
                }
                if(inv.Bedroom_Type__c != null){
                    bedrooms.add(inv.Bedroom_Type__c);
                }  
                if(inv.View_Type__c != null){
                    viewTypes.add(inv.View_Type__c);
                }
                
                if(inv.Floor_Location__r.Name != null){
                    floorLocations.add(inv.Floor_Location__r.Name);
                }
                if(inv.Building_Location__r.Name != null){
                    buildingLocations.add(inv.Building_Location__r.Name);
                }
                
            }
        }
    }
    
    /******************************************************* POPULATE FILTERS ****************************************************/        
    // MARKETING NAME
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        List<String> productnamelst = new List<String>();
        for(String productname: productnameList){
            if(!productnamelst.contains(productname)){
                productnamelst.add(productname);
            }
        }
        productnamelst.sort();
        for(String name: productnamelst){
            options.add(new SelectOption(name, name));
        }
        system.debug('@@ Product Name options==>>>>' + options);
        return options;
       
    }


    //Building Location
    public List<SelectOption> getBuildingLocations(){
        List<String> locList = new List<String>();
        locList.addAll(buildingLocations);
        locList.sort();
        List<SelectOption> options = new List<SelectOption>();
        for(String loc: locList){
            options.add(new SelectOption(loc, loc));
        }
        system.debug('@@ Location options==>>>>' + options);
        return options;
    }
    
        //Floor Location
    public List<SelectOption> getFloorLocations(){
        List<String> locList = new List<String>();
        locList.addAll(floorLocations);
        locList.sort();
        List<SelectOption> options = new List<SelectOption>();
        for(String loc: locList){
            options.add(new SelectOption(loc, loc));
        }
        system.debug('@@ Location options==>>>>' + options);
        return options;
    }
    
 

    //PROPERTY NAMES
    public List<SelectOption> getPropertyNames() {
        List<SelectOption> options = new List<SelectOption>();
        List<String> propertynamessort = new List<String>();
        propertynamessort.addall(propertyNames);
        propertynamessort.sort();
        for(string property: propertynamessort){
            options.add(new SelectOption(property, property));
        }
        system.debug('@@ Property Names options==>>>>' + options);
        return options;
    }

    //ACD
    public List<SelectOption> getAcd() {
        List<SelectOption> options = new List<SelectOption>();
        for(string acdName: acd){
            options.add(new SelectOption(acdName, acdName));
        }
        system.debug('@@ ACD options==>>>>' + options);
        return options;
    }

    // Bed rooms
    public List<SelectOption> getBedRooms() {
        List<SelectOption> options = new List<SelectOption>();
        List<String> bedroomList = new List<String>();
        bedroomList.addall(bedrooms);
        bedroomList.sort();
        for(string bedroom: bedroomList){
            options.add(new SelectOption(bedroom, bedroom));
        }
        system.debug('@@ BedRooms options==>>>>' + options);
        return options;
    }

    // View Types
    public List<SelectOption> getViewTypes() {
        List<SelectOption> options = new List<SelectOption>();
        List<String> viewtypeList = new List<String>();
        viewtypeList.addall(viewTypes);
        viewtypeList.sort();
        for(string viewType: viewtypeList){
            options.add(new SelectOption(viewType, viewType));
        }
        system.debug('@@ View Types options==>>>>' + options);
        return options;
    }

}