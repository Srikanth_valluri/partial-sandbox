/*
 * Description - Class used for performing callout to get the receipt url
 *
 * Version        Date            Author            Description
 * 1.0            21/11/2017      Vivek Shinde      Initial Draft
 */
public with sharing class GenerateReceiptService {
    
    //Method to call a webservice method to get receipt url
    public static List<ReceiptResponse> getReceiptURL(List<GenerateReceiptProcessService.APPSXXDC_PROCESS_SERX1794747X1X5> lstRegTerms){
        GenerateReceipt.ReceiptIPMSHttpSoap11Endpoint objGenRec =
            new GenerateReceipt.ReceiptIPMSHttpSoap11Endpoint();
        objGenRec.timeout_x = 120000;

        try{
            String strResponse = objGenRec.GenerateReceipt('2-' + String.valueOf(System.currentTimeMillis()), 
                'GENERATE_DP_RECEIPT', 'SFDC', lstRegTerms);
            system.debug('== generate receipt Response =='+strResponse );
            GenerateReceiptResponse objResponse = (GenerateReceiptResponse)JSON.deserialize(strResponse, GenerateReceiptResponse.class);
            if(objResponse != null && objResponse.data != null) {
                system.debug('--objResponse.data--'+objResponse.data);
                List<ReceiptResponse> lstResponse = new List<ReceiptResponse>();
                for(Data objRes: objResponse.data) {
                    if(String.isNotBlank(objRes.PROC_STATUS) && objRes.PROC_STATUS.equalsIgnoreCase('S')) {
                        lstResponse.add(new ReceiptResponse(objRes.PARAM_ID, objRes.ATTRIBUTE1));
                    }
                    else {
                        lstResponse.add(new ReceiptResponse('Exception', ''));
                    }
                }
                system.debug('--lstResponse--'+lstResponse);
                return lstResponse;
            }
        } catch (Exception e ) {
            system.debug('--e--'+e.getMessage());
        }
        return new List<ReceiptResponse>{new ReceiptResponse('Exception', '')};
    }


    public class ReceiptResponse {
        public String status {get;set;}
        public String url {get;set;}
        
        public ReceiptResponse(String status, String url) {
            this.status = status;
            this.url = url;
        }
    }
    
    public class GenerateReceiptResponse {
        public List<Data> data;
        public String message;
        public String status;
    }
    
    public class Data {
        public String PROC_STATUS;
        public String PROC_MESSAGE;
        public String ATTRIBUTE3;
        public String ATTRIBUTE2;
        public String ATTRIBUTE1;
        public String PARAM_ID;
        public String Message_ID;
    }
}