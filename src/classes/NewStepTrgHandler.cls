/**************************************************************************************************
* Name               : NewStepTrgHandler
* Test Class         : NewStepTrgHandler_Test
* Description        : Trigger handler for New Step object.
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR          DATE            Comments
* 1.0                         16/06/2019      Created
* 1.1         QBurst          08/04/2020      SOAP to REST Changes
* 1.2         QBurst          16/04/2020      Email Notification to SR Owner on Mid Office Rejection
* 1.3         QBurst          30/04/2020      Modified Mid Office rejection email
* 1.4         QBurst          11/05/2020      Email Notification to SR Owner on Token Document Rejection
* 1.5         Srikanth        10/06/2020      To send documents for second signer when status = Under Manager Review
* 1.6         QBurst          15/07/2020      Added null check for SR CheckList Map
* 1.7         DAMAC           20/07/2020      Added Token paid logic  
**************************************************************************************************/
public class NewStepTrgHandler implements TriggerFactoryInterface {

    private Map<Id, NSIBPM__Service_Request__c> getRelatedSRs(Set<Id> SRIdSet){
        Map<Id, NSIBPM__Service_Request__c> relatedSRs = new Map<Id, NSIBPM__Service_Request__c>([SELECT RecordType.Name, CreatedByID, DP_ok__c, Doc_ok__c, Is_UK_Deal__c 
                                                                                                  FROM NSIBPM__Service_Request__c WHERE Id IN: SRIdSet]);
        return relatedSRs;
    }

    //Before Trigger Methods
    public void executeBeforeInsertTrigger(list<sObject> lstNewRecords){
        updateStepOwner (lstNewRecords);
        updateBookingStatus(lstNewRecords, null, 'before insert');
    }

    public void executeBeforeUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){
        updateBookingStatus(mapNewRecords.values(), mapOldRecords, 'before update');
    }

    public void executeBeforeDeleteTrigger(Map<Id, sObject> mapOldRecords){}
    public void executeBeforeInsertUpdateTrigger(list<sObject> lstNewRecords, Map<Id,sObject> mapOldRecords){}

    //After Trigger Methods
    public void executeAfterInsertTrigger(Map<Id, sObject> mapNewRecords){
        generateDocumentPlaceHolders(mapNewRecords.values());
        updateBookingStatus(mapNewRecords.values(), null, 'after insert');
        sendEmailToPCRejectedURRF(mapNewRecords.values()); // 1.2
        sendTokenDoumentRejectionEmail(mapNewRecords.values());  // 1.4
     //   sendDocumentsForSecondSigner (mapNewRecords.values()); // 1.5
        
    }

    public void executeAfterUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){
        updateBookingStatus(mapNewRecords.values(), mapOldRecords, 'after update');
        //V1.7
        handleBookingUnits((list<New_Step__c>) mapNewRecords.values(), mapOldRecords);
    }

    public void executeAfterDeleteTrigger(Map<Id, sObject> mapOldRecords){}
    public void executeAfterInsertUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){}

    /*****************1.2 starts************************************************/
    public void sendEmailToPCRejectedURRF(List<sObject> lstNewRecords){
        List<New_Step__c> stepListForEmail = new List<New_Step__c>();
        for(sObject step: lstNewRecords){
            New_Step__c newStep = (New_Step__c)step;
            if(newStep.Step_Type__c == 'Revised URRF Upload' && newStep.Step_Status__c ==  'Awaiting Revised URRF'){
                stepListForEmail.add(newStep);
            }
        }
        if(stepListForEmail.size() > 0){
            List<String> srIds = new List<String>();
            Map<String, List<Reservation_Form_Check_List__c>> srCheckListMap = new  Map<String, List<Reservation_Form_Check_List__c>>();
            Map<String, NSIBPM__Service_Request__c> srMap = new Map<String, NSIBPM__Service_Request__c>();
            Map<String, String> srCommentsMap = new Map<String, String>(); // 1.3
            for(New_Step__c step: stepListForEmail){
                srIds.add(step.Service_Request__c);
                srCheckListMap.put(step.Service_Request__c, null);
            }
            // 1.3 starts
            for(New_Step__c oldStep: [SELECT Id, Service_Request__c, Comments__c FROM New_Step__c 
                                        WHERE Service_Request__c IN: srIds 
                                            AND Step_Type__c = 'Mid Office Approval'
                                            AND Step_Status__c = 'Mid Office Rejected'
                                            AND Is_Closed__c = TRUE
                                        ORDER BY LastModifiedDate DESC]){
                if(!srCommentsMap.containsKey(oldStep.Service_Request__c)){
                    srCommentsMap.put(oldStep.Service_Request__c, oldStep.Comments__c);
                }
            }
            // 1.3 ends
            for(NSIBPM__Service_Request__c sr: [SELECT Id, Name, OwnerId, Owner.Name FROM NSIBPM__Service_Request__c WHERE Id IN: srIds]){
                srMap.put(sr.Id, sr);
            }
            
            system.debug('srIds: ' + srIds);
            system.debug('srCheckListMap: ' + srCheckListMap);
            for(Reservation_Form_Check_List__c checkList: [SELECT Id, Checklist_Item__c, Subheading__c, Validated__c, Service_Request__c
                                                           FROM Reservation_Form_Check_List__c 
                                                           WHERE Service_Request__c IN: srIds
                                                           AND Validated__c = FALSE]){
               
                system.debug('checkList: ' + checkList);
                List<Reservation_Form_Check_List__c> rfclList = new  List<Reservation_Form_Check_List__c>();
                if(srCheckListMap.containsKey(checkList.Service_Request__c) && srCheckListMap.get(checkList.Service_Request__c) != null){
                    rfclList = srCheckListMap.get(checkList.Service_Request__c);
                }
                rfclList.add(checkList);
                srCheckListMap.put(checkList.Service_Request__c, rfclList);
            }
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            for(New_Step__c step: stepListForEmail){
                NSIBPM__Service_Request__c sr  = srMap.get(step.Service_Request__c);
                Messaging.SingleEmailMessage mail = new  Messaging.SingleEmailMessage();
                String ccEmail = '';
                List<String> ccEmails = new List<String>();
                String body = 'Hi ' + sr.Owner.Name + ',<br/><br/>';
                String emailSubject = 'Failed URRF Validation: ' + sr.Name;
                Decimal slNo;
                slNo = 0;

                Map<String,List<String>> str = new Map<String,List<String>>();
                set<String> usedSubHeadings = new set<String>();
                body += 'The following validations failed: <br/><br/>' ;
                body += '<table border="1" style="border-collapse: collapse;" cellpadding="5" ><tr><th>Sl No</th><th>Checklist Item</th></tr>';
                if(srCheckListMap != null && srCheckListMap.containsKey(step.Service_Request__c)  
                                            && srCheckListMap.get(step.Service_Request__c) != null) { // 1.6
                    for(Reservation_Form_Check_List__c checkList: srCheckListMap.get(step.Service_Request__c)){
                        if(checkList.Subheading__c != null && checkList.Subheading__c != ''){
                            if(str.containsKey(checkList.Subheading__c)){
                                str.get(checkList.Subheading__c).add(checkList.Checklist_Item__c);
                            } else{             
                                str.put(checkList.Subheading__c,new List<String>{checkList.Checklist_Item__c} );
                            }
                        }
                    }
                    for(Reservation_Form_Check_List__c checkList: srCheckListMap.get(step.Service_Request__c)){
                        if(string.isBlank(checkList.Subheading__c)){
                          slNo = slNo + 1;
                          body += '<tr><td>' + slNo + '</td><td>' + checkList.Checklist_Item__c + '</td></tr>';
                        } else{
                            if(str.containsKey(checkList.Subheading__c) && !usedSubHeadings.contains(checkList.Subheading__c)){
                                usedSubHeadings.add(checkList.Subheading__c);
                                slNo += 1;
                                body += '<tr><td>' + slNo + '</td><td>' + checkList.Subheading__c + '</td></tr>';
                                for(String subValue : str.get(checkList.Subheading__c)){
                                    slNo += 0.1;
                                    body += '<tr><td>    ' + slNo + '</td><td>    ' + subValue + '</td></tr>';
                                }
                                slNo = slNo.round(System.RoundingMode.UP) - 1;
                            }
                        }
                    }
                }
                body += '</table>';
                // 1.3 starts
                system.debug('step.Comments__c : ' + step.Comments__c );
                if(srCommentsMap.containsKey(step.Service_Request__c) 
                        && srCommentsMap.get(step.Service_Request__c) != null
                        && srCommentsMap.get(step.Service_Request__c) != ''){
                    body += '<br/><br/>Comments: ' + srCommentsMap.get(step.Service_Request__c);
                }
                // 1.3 ends
                body += '<br/><br/> Link to the Service Request: ' + System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + sr.Id;
                mail.setTargetObjectId(sr.OwnerId);
                System.debug('...emailCCaddress...' + ccEmail);
                mail.setCcAddresses(ccEmails);
                mail.setSenderDisplayName('Damac Property ');
                mail.setReplyTo('noreply@Damacgroup.com');
                mail.setSubject(emailSubject);
                mail.setSaveAsActivity(false);
                System.debug('...body...' + body);
                mail.setHtmlBody(body);
                mails.add(mail);  
                system.debug('mail: ' + mail);
            }
            Messaging.sendEmail(mails);
        }
    }
    /*****************1.2 ends************************************************/

    /*****************1.4 starts************************************************/
    public void sendTokenDoumentRejectionEmail(List<sObject> lstNewRecords){
        List<New_Step__c> stepListForEmail = new List<New_Step__c>();
        for(sObject step: lstNewRecords){
            New_Step__c newStep = (New_Step__c)step;
            if(newStep.Step_Type__c == 'Revised Token Document Upload' && newStep.Step_Status__c ==  'Awaiting Token Document Upload'){
                stepListForEmail.add(newStep);
            }
        }
        if(stepListForEmail.size() > 0){
            List<String> srIds = new List<String>();
            Map<String, NSIBPM__Service_Request__c> srMap = new Map<String, NSIBPM__Service_Request__c>();
            Map<String, String> srCommentsMap = new Map<String, String>(); // 1.3
            for(New_Step__c step: stepListForEmail){
                srIds.add(step.Service_Request__c);
            }
            // 1.3 starts
            for(New_Step__c oldStep: [SELECT Id, Service_Request__c, Comments__c FROM New_Step__c 
                                        WHERE Service_Request__c IN: srIds 
                                            AND Step_Type__c = 'Token Payment'
                                            AND Step_Status__c = 'Token Document Rejected'
                                            AND Is_Closed__c = TRUE
                                        ORDER BY LastModifiedDate DESC]){
                if(!srCommentsMap.containsKey(oldStep.Service_Request__c)){
                    srCommentsMap.put(oldStep.Service_Request__c, oldStep.Comments__c);
                }
            }
            for(NSIBPM__Service_Request__c sr: [SELECT Id, Name, OwnerId, Owner.Name FROM NSIBPM__Service_Request__c WHERE Id IN: srIds]){
                srMap.put(sr.Id, sr);
            }
            
            system.debug('srIds: ' + srIds);
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            for(New_Step__c step: stepListForEmail){
                NSIBPM__Service_Request__c sr  = srMap.get(step.Service_Request__c);
                Messaging.SingleEmailMessage mail = new  Messaging.SingleEmailMessage();
                String body = 'Hi ' + sr.Owner.Name + ',<br/><br/>';
                String emailSubject = 'Token Document Rejected: ' + sr.Name;
                Decimal slNo;
                body += 'The token document for the Service Request ' + sr.Name + ' got rejected.';
                system.debug('step.Comments__c : ' + step.Comments__c );
                if(srCommentsMap.containsKey(step.Service_Request__c) 
                        && srCommentsMap.get(step.Service_Request__c) != null
                        && srCommentsMap.get(step.Service_Request__c) != ''){
                    body += '<br/><br/>Comments: ' + srCommentsMap.get(step.Service_Request__c);
                }
                body += '<br/><br/> Link to the Service Request: ' + System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + sr.Id;
                mail.setTargetObjectId(sr.OwnerId);
                mail.setSenderDisplayName('Damac Property ');
                mail.setReplyTo('noreply@Damacgroup.com');
                mail.setSubject(emailSubject);
                mail.setSaveAsActivity(false);
                System.debug('...body...' + body);
                mail.setHtmlBody(body);
                mails.add(mail);  
                system.debug('mail: ' + mail);
            }
            Messaging.sendEmail(mails);
        }
    }
    /*****************1.4 ends************************************************/
    
    /*
    public void sendDocumentsForSecondSigner (list<Sobject> lstNewRecords) {
        try {
            Set <ID> srIds = new Set <ID> ();
            for(sObject sObj : lstNewRecords){
                New_Step__c newStep = (New_Step__c)sObj;
                if(newStep.Step_Status__c == 'Under Manager Review' && newStep.Step_Type__c == 'Agent Manager Review'){
                    srIds.add(newStep.Service_Request__c);
                }
            }
            if (srIds.size () > 0) {
                System.enqueueJob (new Damac_signDocumentsForReview(srIds));
            }
        } catch (Exception e) {}
    } */
    
    private void generateDocumentPlaceHolders(List<sObject> lstNewRecords){
        try{
            //for UK steps
            Set<Id> UKSRIdSet = new Set<Id>();
            List<New_Step__c> UKStepsList = new List<New_Step__c>();
            for(sObject sObj : lstNewRecords){
                New_Step__c newStep = (New_Step__c)sObj;
                if(newStep.Is_UK_Deal__c){
                    UKSRIdSet.add(newStep.Service_Request__c);
                    UKStepsList.add(newStep);
                }
            }
            //Select document templates and construct a map of <'type-status', list<templates>>
            map<String, List<Document_Template__c>> documentTemplateMap = new map<String, List<Document_Template__c>>();
            for(Document_Template__c docTemplate : [Select Name, Step_Status__c, Step_Type__c, Document_Type__c, Related_Object__c,
                                                    Buyer_Type__c, Primary_Buyer__c, In_Use__c, Code__c, Company_Doc__c, 
                                                    Conditions_Filter__c, DDP_Delivery_Option__c, Document_Name__c, DDP__c, 
                                                    Evaluate_at_SR_Status__c, Evaluate_at_Step_No__c, Optional__c, 
                                                    Save_Before_Submit__c, SR_Record_Type__c, Sys_Is_Generated_Doc__c, User_Doc__c 
                                                    From Document_Template__c 
                                                    Where SR_Record_Type__c = 'Deal' AND Is_UK__c = true]) {
                String stepKey = docTemplate.Step_Type__c + docTemplate.Step_Status__c;
                if( !documentTemplateMap.containsKey(stepKey)) {
                    documentTemplateMap.put(stepKey, new List<Document_Template__c>());
                }
                documentTemplateMap.get(stepKey).add(docTemplate);
            }
            //Select booking units and construct a map of map<srid, list<booking units>>
            map<String, List<Booking_Unit__c>> bookingUnitsMap = new map<String, List<Booking_Unit__c>>();
            for(Booking_Unit__c bu : [SELECT Booking__r.Deal_SR__c FROM Booking_Unit__c WHERE Booking__r.Deal_SR__c IN: UKSRIdSet]){
                if( !bookingUnitsMap.containsKey(bu.Booking__r.Deal_SR__c)) {
                    bookingUnitsMap.put(bu.Booking__r.Deal_SR__c, new List<Booking_Unit__c>());
                }
                bookingUnitsMap.get(bu.Booking__r.Deal_SR__c).add(bu);
            }
            //Select buyers and construct a map of map<srid, list<buyers>>
            map<String, List<Buyer__c>> buyersMap = new map<String, List<Buyer__c>>();
            for(Buyer__c buyer : [SELECT Booking__r.Deal_SR__c, First_Name__c, Buyer_Type__c, Primary_Buyer__c 
                                  FROM Buyer__c WHERE Booking__r.Deal_SR__c IN: UKSRIdSet]){
                if( !buyersMap.containsKey(buyer.Booking__r.Deal_SR__c)) {
                    buyersMap.put(buyer.Booking__r.Deal_SR__c, new List<Buyer__c>());
                }
                buyersMap.get(buyer.Booking__r.Deal_SR__c).add(buyer);
            }
            //create docs for the UK steps
            List<Unit_Documents__c> unitDocsToInsert = new List<Unit_Documents__c>();
            for(New_Step__c newStep : UKStepsList){
                //get(type-status) from the document template map
                String stepKey = newStep.Step_Type__c + newStep.Step_Status__c;
                if(documentTemplateMap.containsKey(stepKey)){
                    for(Document_Template__c docTemplate : documentTemplateMap.get(stepKey)){
                        //if type = single -> createdoc
                        if(docTemplate.Document_Type__c == 'Single'){
                            unitDocsToInsert.add(createUnitDoc(docTemplate.Name, docTemplate, newStep.Service_Request__c));
                        } else if(docTemplate.Document_Type__c == 'Multi'){
                            //if type = multi, get related object name and create unit docs as many as the object's records number
                            if(docTemplate.Related_Object__c == 'Buyer__c'){
                                for(Buyer__c buyer : buyersMap.get(newStep.Service_Request__c)){
                                    if(buyer.Buyer_Type__c == docTemplate.Buyer_Type__c && buyer.Primary_Buyer__c == docTemplate.Primary_Buyer__c){
                                        unitDocsToInsert.add(createUnitDoc(buyer.First_Name__c + ' ' + docTemplate.Name, 
                                                                    docTemplate, newStep.Service_Request__c));
                                    }
                                }
                            } else if(docTemplate.Related_Object__c == 'Booking_Unit__c'){
                                for(Booking_Unit__c bu : bookingUnitsMap.get(newStep.Service_Request__c)){
                                    unitDocsToInsert.add(createUnitDoc(docTemplate.Name, docTemplate, newStep.Service_Request__c));
                                }
                            }
                        }
                    }
                }
            }
            if(unitDocsToInsert.size() > 0){
                insert unitDocsToInsert;
            }    
        }catch(exception ex){
            system.debug('Exception line: ' + ex.getLineNumber());
            system.debug('Exception message: ' + ex.getMessage());
        }
    }

    public Static Unit_Documents__c createUnitDoc(String docName, Document_Template__c docTmp, Id SRId) {
        Unit_Documents__c objUnitDoc = new Unit_Documents__c();
        if(docName.length() > 80) {
            docName = docName.subString(0, 79);
        }
        objUnitDoc.Document_Name__c = docName;
        objUnitDoc.Service_Request__c = SRId;
        objUnitDoc.Document_Description__c = docTmp.Document_Name__c;
        objUnitDoc.Optional__c = docTmp.Optional__c;
        objUnitDoc.Is_Required__c = !docTmp.Optional__c;
        objUnitDoc.User_Doc__c = docTmp.User_Doc__c;
        objUnitDoc.Document_Template__c = docTmp.Id;
        objUnitDoc.Sys_IsGenerated_Doc__c = docTmp.Sys_Is_Generated_Doc__c;
        objUnitDoc.Status__c = 'Pending Upload';
        return objUnitDoc;
    }

    private void updateBookingStatus(List<sObject> lstNewRecords, Map<Id, sObject> mapOldRecords, String TrgType){
        try{
            String StatusCode;
            List<IPMS_Code_Settings__c> IPMSCodeList = IPMS_Code_Settings__c.getAll().values();
            List<New_Step__c> newStepsList = new List<New_Step__c>();
            for(sObject sObj : lstNewRecords){
                New_Step__c newStep = (New_Step__c)sObj;
                New_Step__c oldStep = new New_Step__c();
                if(TrgType.containsIgnoreCase('update')){
                    oldStep = (New_Step__c)mapOldRecords.get(newStep.Id);
                }
                if(TrgType.containsIgnoreCase('insert') || (TrgType.containsIgnoreCase('update') 
                                            && newStep.Step_Status__c != oldStep.Step_Status__c)){
                    for(IPMS_Code_Settings__c code : IPMSCodeList){
                        if(newStep.Step_Status__c == code.Step_Status__c && 
                            newStep.Step_Type__c == code.Step_Type__c){
                            newStepsList.add(newStep);
                            break;
                        }
                    }
                }
            }
            if(newStepsList.size() > 0){
                //select related service requests
                Set<Id> SRIdSet = new Set<Id>();
                for(sObject sObj : newStepsList){
                    New_Step__c newStep = (New_Step__c)sObj;
                    SRIdSet.add(newStep.Service_Request__c);
                }
                Map<Id, NSIBPM__Service_Request__c> relatedSRs = getRelatedSRs(SRIdSet);
                //select related bookings
                Map<Id, Booking__c> relatedBookings = new Map<Id, Booking__c>();
                for(Booking__c Booking :[SELECT Deal_SR__c, Deal_SR__r.DP_ok__c, Deal_SR__r.Doc_ok__c,
                                             (SELECT Registration_Status__c FROM Booking_Units__r) 
                                         FROM Booking__c 
                                         WHERE Deal_SR__c IN: SRIdSet]){
                    relatedBookings.put(Booking.Deal_SR__c, Booking);
                }
                for(sObject sObj : newStepsList){
                    New_Step__c newStep = (New_Step__c)sObj;
                    New_Step__c oldStep = new New_Step__c();
                    if(TrgType.containsIgnoreCase('update')){
                        oldStep = (New_Step__c)mapOldRecords.get(newStep.Id);
                    }
                    system.debug('Doc_OK ' + relatedSRs.get(newStep.Service_Request__c).Doc_ok__c);
                    system.debug('DP_OK ' + relatedSRs.get(newStep.Service_Request__c).DP_ok__c);
                    StatusCode = '';
                    if(TrgType.containsIgnoreCase('insert') || (TrgType.containsIgnoreCase('update') && newStep.Step_Status__c != oldStep.Step_Status__c)){
                        for(IPMS_Code_Settings__c code : IPMSCodeList){
                            if(newStep.Step_Status__c == code.Step_Status__c && newStep.Step_Type__c == code.Step_Type__c 
                                    && ( !code.Check_on_Doc_OK__c || relatedSRs.get(newStep.Service_Request__c).Doc_ok__c == code.Doc_OK__c) 
                                    && ( !code.Check_on_DP_OK__c || relatedSRs.get(newStep.Service_Request__c).DP_ok__c == code.DP_OK__c)){
                                StatusCode = code.Status_Code__c;
                                break;
                            }
                        }
                        Booking__c Booking;
                        if(relatedBookings.containsKey(newStep.Service_Request__c)) {
                           Booking = relatedBookings.get(newStep.Service_Request__c);
                        }
                        if(String.isNotBlank(StatusCode) && Booking != Null){
                            if(TrgType.containsIgnoreCase('before')){
                                newStep.Booking_Unit_Status_Code__c = StatusCode;
                            } else if(TrgType.containsIgnoreCase('after')){
                                // Invocable_UpdateBookingUnitStatus.updateBookingStatus(new List<Id>{Booking.Id}, StatusCode); 1.1
                                DAMAC_IPMS_PARTY_CREATION.statusUpdate(new List<Id>{Booking.Id}, statusCode, newStep.Id); // 1.1
                            }
                        }
                    }
                }
            }
        } catch(exception ex){
            system.debug('Exception line: ' + ex.getLineNumber());
            system.debug('Exception message: ' + ex.getMessage());
        }
    }

    public static void handleBookingUnits(List<New_Step__c> lstNewRecords, Map<Id, sobject> mapOldRecords){
        set<Id> requestIds = new set<Id>();
        for(New_Step__c step: (List<New_Step__c>) lstNewRecords){
            New_step__c oldRec = (New_Step__c) mapOldRecords.get(step.Id);
            if(step.Step_No__c == 2.00 && step.Step_Status__c == 'Token Deposit Paid' 
                    && step.Step_Type__c == 'Token Payment' && step.Is_Closed__c && step.Is_Closed__c != oldRec.Is_Closed__c){
                requestIds.add(step.Service_Request__c);        
            }
        }

        System.debug('>>>>>>>>>>>>>>>>>requestIds>>>>>>>>>>>>>'+requestIds);
        if(!requestIds.isEmpty()){
            list<Booking_Unit__c> lstUnits = new list<Booking_Unit__c>();
            for(Booking_Unit__c unit: [Select id from Booking_Unit__c where Booking__r.Deal_SR__c in: requestIds]){
                unit.Token_Paid_time__c = System.now();
                unit.Token_Paid__c = true;
                lstUnits.add(unit);
            }

            if(!lstUnits.isEmpty())
                update lstUnits;
        }
    }
    
    public void updateStepOwner (List<sObject> lstNewRecords) {
        Set <ID> srIds = new Set <ID> ();
        for(sObject step: lstNewRecords){
            New_Step__c newStep = (New_Step__c)step;
            srIds.add (newStep.Service_Request__c);
        }
        map <ID, NSIBPM__Service_Request__c> srDetails = getRelatedSrs (srIds);
        Set <ID> createdByIds = new Set <ID> ();
        for(sObject step: lstNewRecords){
            New_Step__c newStep = (New_Step__c)step;
            if ((srDetails.get (newStep.Service_Request__c).recordType.name == 'Agent_Registration'
                || srDetails.get (newStep.Service_Request__c).recordType.name == 'Agent_Update') 
                && (newStep.Step_Type__c == 'Sent For More Info' || newStep.Step_Type__c == 'Agreement Signing' )) {
                
                createdByIds.add (srDetails.get (newStep.Service_Request__c).CreatedById);
            }
        }
        if (!createdByIds.isEmpty ()) {
            Map <ID, User> userDetails = new Map <id, User> ([SELECT Profile.Name FROM User WHERE Id IN: createdByIds]);
            ID queueId = [SELECT Id from Group WHERE Type = 'Queue' AND NAME = 'Agent_RM_Queue'].Id;
            for(sObject step: lstNewRecords){
                New_Step__c newStep = (New_Step__c)step;
                if ( (srDetails.get (newStep.Service_Request__c).recordType.name == 'Agent_Registration'
                    || srDetails.get (newStep.Service_Request__c).recordType.name == 'Agent_Update') 
                    && userDetails.get (srDetails.get (newStep.Service_Request__c).createdById).Profile.Name == 'Agent Executive Team'
                    && (newStep.Step_Type__c == 'Sent For More Info' || newStep.Step_Type__c == 'Agreement Signing' )) {
                    
                    newStep.OwnerId = queueId ;
                }
            }

        }
    }
}