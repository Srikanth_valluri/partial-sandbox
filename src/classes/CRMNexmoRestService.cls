/*
Description:  Rest Response Class for WHATSAPP CLIENT to read data from WHATSAPP
*/
@RestResource(urlMapping='/crmNexmohook/*')
global without sharing class CRMNexmoRestService {
    @HttpPost
    global static void doPost(){    
        try{      
            system.debug(RestContext.request);
            system.debug(RestContext.request.params);
            //system.debug(requestBody.request.params.size());
            /**********************Testing***************************/
                        string str='{'
                +'"message_uuid": "b38aaa27-18fe-4de5-866d-a900601ead0f",'
                +'"to": {'
                    +'"number": "97142375044",'
                    +'"type": "whatsapp"'
                +'},'
                +'"from": {'
                    +'"number": "919754700231",'
                    +'"type": "whatsapp"'
                +'},'
                +'"timestamp": "2020-04-06T14:48:45.374Z",'
                +'"direction": "inbound",'
                +'"message": {'
                    +'"content": {'
                        +'"type": "video",'
                        +'"video": {'
                            +'"url": "https://api.nexmo.com/v3/media/306ac5bd-7d4c-47e0-8a93-09729ca45537"'
                        +'}'
                    +'}'
                +'}'
            +'}';
            /**********************Testing***************************/
            string jsonStr = RestContext.request.requestBody.toString();
            system.debug('jsonStr--------------------'+jsonStr);
            system.debug(RestContext.request.requestURI);// == '/nexmohook/status');
            if(jsonStr != ''){
                if( RestContext.request.requestURI == '/crmNexmohook/status' ){                
                    NEXMO_RESPONSE_PARSER parser= parse(jsonStr);
                    system.debug(parser);
                    system.debug(parser.from_Z );
                    system.debug(parser.status); 
                    system.debug(parser.error);
                    system.debug(parser.message_uuid);
                    if(parser.message_uuid != ''){
                        updateMessagestatus(parser.message_uuid,parser);
                    }
                }
                // if( RestContext.request.requestURI == '/crmNexmohook/inbound' )
                {                
                     NEXMO_INB_RESPONSE_PARSER inbParser = parse1(jsonStr);
                     system.debug('inbParser:::'+inbParser);
                   // NEXMO_INB_RESPONSE_PARSER inbParser = parse1(str);
                   NexemoResponce obj = NexemoResponce.parse(jsonStr);
                   system.debug('obj:::'+obj);
                   
                    if(inbParser.message_uuid != ''){
                        // tagOrCreateInboundMessage(inbParser.message_uuid,inbParser);
                        tagOrCreateInboundMessageTest(inbParser.message_uuid,inbParser,obj);
                        
                    }
                }
            }
            /*
            if (RestContext.request.params.size() > 0) {             
                map<string,string> respMap = new map<string,string>();
                respMap = RestContext.request.params;
                system.debug(respMap);
            }  
            */
            
            }catch(exception e){
                
                system.debug(e.getmessage());
            } 
            
                  
        
        }
        
    @Httpget
    global static void doget(){ 
        system.debug('enetered get');
    }
    
    public static void updateMessagestatus(string Messageid,NEXMO_RESPONSE_PARSER parser){    
        CRM_Nexmo_Whats_App_Message__c msg = [SELECT Id
                                                   , Message_Submitted__c
                                                   , Message_Delivered__c
                                                   , Message_Read__c
                                                   , Message_Failed__c
                                            FROM CRM_Nexmo_Whats_App_Message__c 
                                           WHERE Nexmo_Message_uuid__c=:Messageid LIMIT 1];                
        
        
        if(parser.status == 'submitted') {
            msg.Message_Submitted__c = true;
        }        
        if(parser.status == 'delivered') {
            msg.Message_Delivered__c = true;
        }
        if(parser.status == 'read'){
            msg.Message_Read__c = true;
        }
        if(parser.status != 'submitted' && parser.status != 'delivered' && parser.status != 'read' ){
            msg.Message_Failed__c = true;
        }
        update msg;
    }
    public static void tagOrCreateInboundMessageTest(string Messageid,NEXMO_INB_RESPONSE_PARSER parser,NexemoResponce parser1){
      system.debug('parser:::'+parser);
      system.debug('parser1:::'+parser1);
      long fromNum;
      String strFromNum = '';
        if(!test.isrunningtest()){
            fromNum = long.valueof(parser.from_Z.number_Z);
            strFromNum = string.valueOf(parser.from_Z.number_Z);
        }else{
            fromNum = 1234567890;
        }
        list<CRM_Nexmo_Whats_App_Request__c> matchingParent = new list<CRM_Nexmo_Whats_App_Request__c>();
        matchingParent = [SELECT id,lastmodifiedbyid,ownerid 
                            FROM CRM_Nexmo_Whats_App_Request__c 
                           WHERE To_number__c=:fromNum ORDER BY lastmodifieddate desc  LIMIT 1];
        system.debug('matchingParent:::'+matchingParent);
        boolean tool=true;
        if(matchingParent.size() > 0 && tool==true){
            system.debug('matchingParent greter than 0');
            //tag the Message under the Parent
            CRM_Nexmo_Whats_App_Message__c newInbMsg = new CRM_Nexmo_Whats_App_Message__c();
            newInbMsg.CRM_Nexmo_Whats_App_Request__c = matchingParent[0].id;
            newInbMsg.Direction__c = 'Inbound';
           newInbMsg.Nexmo_Message_Body__c = parser.message.content.inboundMessage;
            
            if (parser.message.content.type_Z == 'image') {
                newInbMsg.message_body_url__c =parser1.message.content.image.url;
            }
            if (parser.message.content.type_Z == 'location') {
               
                newInbMsg.message_body_url__c =parser1.message.content.location.url;
            }
             if (parser.message.content.type_Z == 'audio') {
             
                 newInbMsg.message_body_url__c =parser1.message.content.audio.url;
            }
            if (parser.message.content.type_Z == 'video') {
                
                newInbMsg.message_body_url__c =parser1.message.content.video.url;
            }
            if (parser.message.content.type_Z == 'file') {
               
                newInbMsg.message_body_url__c =parser1.message.content.file.url;
            }            
            
            newInbMsg.Message_type__c = parser.message.content.type_Z;
            // newInbMsg.Nexmo_Message_uuid__c = parser.message_uuid;
            //newInbMsg.ownerid = matchingParent[0].lastmodifiedbyid;        
            newInbMsg.ownerid = matchingParent[0].ownerid;     
            system.debug('newInbMsg');
            insert newInbMsg;
            matchingParent[0].Latest_Inbound_Message_Received_on__c = system.now();
            update matchingParent;
        }else{
            system.debug('ELSE ENTERED');
            
            list<Account> lstParentAcc = new list<Account>();
            lstParentAcc = [SELECT id,lastmodifiedbyid,ownerid 
                            FROM Account 
                           WHERE Mobile_Person_Business__c =: strFromNum ORDER BY lastmodifieddate desc  LIMIT 1];
            
            CRM_Nexmo_Whats_App_Request__c request = new CRM_Nexmo_Whats_App_Request__c();        
            request.To_number__c = fromNum;
            request.Unique_Key__c = string.valueof(fromNum);
            //request.ownerid = userinfo.getuserid();
            request.ownerid = lstParentAcc.size() > 0 && lstParentAcc[0].ownerid != null ? lstParentAcc[0].ownerid : Label.NexmoQueue;
            request.Account__c = lstParentAcc.size() > 0 && lstParentAcc[0].id != null ? lstParentAcc[0].id : Label.NexmoAccount;
            request.Is_incoming_from_New_Number__c = true;        
            upsert request Unique_Key__c;
            // Insert Child
            CRM_Nexmo_Whats_App_Message__c message = new CRM_Nexmo_Whats_App_Message__c();
            message.CRM_Nexmo_Whats_App_Request__c = request.id;
            message.direction__c = 'Inbound';
            // message.Nexmo_Message_uuid__c = parser.message_uuid;
            message.Nexmo_Message_Body__c = parser.message.content.inboundMessage;
            
            if (parser.message.content.type_Z == 'image') {
                
                message.message_body_url__c =parser1.message.content.image.url;
            }
            if (parser.message.content.type_Z == 'location') {
                
                message.message_body_url__c =parser1.message.content.location.url;
            }
            
            if (parser.message.content.type_Z == 'audio') {
                
                 message.message_body_url__c =parser1.message.content.audio.url;
            }
            if (parser.message.content.type_Z == 'video') {
               
                message.message_body_url__c =parser1.message.content.video.url;
            }
            if (parser.message.content.type_Z == 'file') {
                
                message.message_body_url__c =parser1.message.content.file.url;
                
            }                   
            
            message.Nexmo_Message_Request_Body__c = string.valueof(parser);
            message.ownerid = lstParentAcc.size() > 0 && lstParentAcc[0].ownerid != null ? lstParentAcc[0].ownerid : Label.NexmoQueue;
            //message.ownerid = userinfo.getuserid();
            //request.ownerid = Label.NexmoQueue;
            insert message;
        }
    
    }
    /* public static void tagOrCreateInboundMessage(string Messageid,NEXMO_INB_RESPONSE_PARSER parser){
        //system.debug('parser.from_Z.number_Z=='+parser.from_Z.number_Z+'**');
        //system.debug(parser.message);
        //system.debug(parser.message.content);
        //system.debug(parser.message.content.inboundMessage+'inboundMessage inboundMessage inboundMessage ');
        
        system.debug('parser:::'+parser);
        long fromNum;
        if(!test.isrunningtest()){
            fromNum = long.valueof(parser.from_Z.number_Z);
        }else{
            fromNum = 1234567890;
        }
        list<CRM_Nexmo_Whats_App_Request__c> matchingParent = new list<CRM_Nexmo_Whats_App_Request__c>();
        matchingParent = [SELECT id,lastmodifiedbyid,ownerid 
                            FROM CRM_Nexmo_Whats_App_Request__c 
                           WHERE To_number__c=:fromNum ORDER BY lastmodifieddate desc  LIMIT 1];
        if(matchingParent.size() > 0){
            //tag the Message under the Parent
            CRM_Nexmo_Whats_App_Message__c newInbMsg = new CRM_Nexmo_Whats_App_Message__c();
            newInbMsg.CRM_Nexmo_Whats_App_Request__c = matchingParent[0].id;
            newInbMsg.Direction__c = 'Inbound';
            newInbMsg.Nexmo_Message_Body__c = parser.message.content.inboundMessage;
            
            if (parser.message.content.type_Z == 'image') {
                 newInbMsg.message_body_url__c = parser.message.content.image.url;
                newInbMsg.Nexmo_Message_Body__c = parser.message.content.image.caption; 
                // message.message_body_url__c =obj.message.content.image.url;
            }
            if (parser.message.content.type_Z == 'location') {
                newInbMsg.message_body_lat__c = parser.message.content.location.lat;
                newInbMsg.message_body_lng__c = parser.message.content.location.WA_long;
                // message.message_body_url__c =obj.message.content.location.url;
            }
            
            if (parser.message.content.type_Z == 'audio') {
                 newInbMsg.message_body_url__c = parser.message.content.audio.url;
                newInbMsg.Nexmo_Message_Body__c = parser.message.content.audio.caption; 
                // message.message_body_url__c =obj.message.content.audio.url;
            }
            if (parser.message.content.type_Z == 'video') {
                 newInbMsg.message_body_url__c = parser.message.content.video.url;
                newInbMsg.Nexmo_Message_Body__c = parser.message.content.video.caption; 
                // message.message_body_url__c =obj.message.content.video.url;
            }
            if (parser.message.content.type_Z == 'file') {
                 newInbMsg.message_body_url__c = parser.message.content.file.url;
                newInbMsg.Nexmo_Message_Body__c = parser.message.content.file.caption; 
                // message.message_body_url__c =obj.message.content.file.url;
            }            
            
            newInbMsg.Message_type__c = parser.message.content.type_Z;
            newInbMsg.Nexmo_Message_uuid__c = parser.message_uuid;
            //newInbMsg.ownerid = matchingParent[0].lastmodifiedbyid;        
            newInbMsg.ownerid = matchingParent[0].ownerid;        
            insert newInbMsg;
            matchingParent[0].Latest_Inbound_Message_Received_on__c = system.now();
            update matchingParent;
        }else{
            system.debug('ELSE ENTERED');
            
            //boolean isStand = findorCreateMatchingStandInquiry(fromNum,parser.message.content.inboundMessage);
            
            //Create a New Parent 
            // Insert Parent
            CRM_Nexmo_Whats_App_Request__c request = new CRM_Nexmo_Whats_App_Request__c();        
            request.To_number__c = fromNum;
            request.Unique_Key__c = string.valueof(fromNum);
            request.ownerid = userinfo.getuserid();
            request.Is_incoming_from_New_Number__c = true;        
            
            upsert request Unique_Key__c;
            
            
            
            // Insert Child
            CRM_Nexmo_Whats_App_Message__c message = new CRM_Nexmo_Whats_App_Message__c();
            message.CRM_Nexmo_Whats_App_Request__c = request.id;
            message.direction__c = 'Inbound';
            message.Nexmo_Message_uuid__c = parser.message_uuid;
            message.Nexmo_Message_Body__c = parser.message.content.inboundMessage;
            
            if (parser.message.content.type_Z == 'image') {
                 message.message_body_url__c = parser.message.content.image.url;
                message.Nexmo_Message_Body__c = parser.message.content.image.caption; 
                // message.message_body_url__c =obj.message.content.image.url;
            }
            if (parser.message.content.type_Z == 'location') {
                message.message_body_lat__c = parser.message.content.location.lat;
                message.message_body_lng__c = parser.message.content.location.WA_long;
                // message.message_body_url__c =obj.message.content.location.url;
            }
            
            if (parser.message.content.type_Z == 'audio') {
                 message.message_body_url__c = parser.message.content.audio.url;
                message.Nexmo_Message_Body__c = parser.message.content.audio.caption; 
                // message.message_body_url__c =obj.message.content.audio.url;
            }
            if (parser.message.content.type_Z == 'video') {
                 message.message_body_url__c = parser.message.content.video.url;
                message.Nexmo_Message_Body__c = parser.message.content.video.caption; 
                // message.message_body_url__c =obj.message.content.video.url;
            }
            if (parser.message.content.type_Z == 'file') {
                 message.message_body_url__c = parser.message.content.file.url;
                message.Nexmo_Message_Body__c = parser.message.content.file.caption; 
                // message.message_body_url__c =obj.message.content.file.url;
                
            }                   
            
            message.Nexmo_Message_Request_Body__c = string.valueof(parser);
            //message.ownerid = insrtedReq.ownerid;
            message.ownerid = userinfo.getuserid();
            insert message;
        }
    
    } */
    
    /*public static boolean findorCreateMatchingStandInquiry( long CustomerWhatsAppNumber,string message ){    
        boolean isFoundinStand = false;    
        system.debug(message);
        if(test.isRunningtest()){
            message = 'IQF-240948 |';
        }
        string standInqNum = message.SubStringBefore('|').deleteWhitespace();
        system.debug('standInqNum'+standInqNum);
        list<stand_inquiry__c> standInq = new list<stand_inquiry__c>();
        String searchQuery = 'FIND \'' + standInqNum + '\' IN ALL FIELDS RETURNING  stand_inquiry__c(Id,Name,Mobile_Phone_Encrypt__c,NSUUID__c)';
        
        //FIND {000015} IN Name Fields RETURNING stand_inquiry__c(id,name,Mobile_Phone_Encrypt__c) 
        List<List <sObject>> searchList = search.query(searchQuery);
        system.debug(searchList);
        standInq = ((List<stand_inquiry__c>)searchList[0]);
        system.debug(standInq);
        // If Stand Inquiry is found create a Inquiry
        if(standInq.size() > 0){
            system.debug(standInq[0].Mobile_Phone_Encrypt__c);
            system.debug(standInq[0].NSUUID__c);
            standInq[0].Whats_App_Initiated__c = true;
            update standInq[0];
            isFoundinStand = true;
        }
        return isFoundinStand;
    }
    
    public static string getCampaignIdForStandInquiry(string standInquiryName){
        stand_inquiry__c st = new stand_inquiry__c();
        st = [select id,Name, Campaign__c from stand_inquiry__c where Name=:standInquiryName];
        if(st != null){
            return string.valueof(st.Campaign__c);
        }else{
            return '';
        }
    
    }*/

    public class NEXMO_INB_RESPONSE_PARSER {

        public String message_uuid {get;set;} 
        public To1 to {get;set;} 
        public To1 from_Z {get;set;} // in json: from
        public String timestamp {get;set;} 
        public String direction {get;set;} 
        public Message1 message {get;set;} 

        public NEXMO_INB_RESPONSE_PARSER(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'message_uuid') {
                            message_uuid = parser.getText();
                        } else if (text == 'to') {
                            to = new To1(parser);
                        } else if (text == 'from') {
                            from_Z = new To1(parser);
                        } else if (text == 'timestamp') {
                            timestamp = parser.getText();
                        } else if (text == 'direction') {
                            direction = parser.getText();
                        } else if (text == 'message') {
                            message = new Message1(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'NEXMO_INB_RESPONSE_PARSER consuming unrecognized property: '+text);
                            consumeObject1(parser);
                        }
                    }
                }
            }
        }
    }


   public class Message1 {
        public Content1 content {get;set;} 

        public Message1(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'content') {
                            content = new Content1(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Message consuming unrecognized property: '+text);
                            consumeObject1(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Content1 {
        public String type_Z {get;set;} // in json: type
        public String text {get;set;} 
        public String inboundMessage {get;set;} 
        public cls_image1 image;
        public cls_file1 file;
        public cls_audio1 audio;
        public cls_video1 video;
        public cls_location1 location;
        
        public Content1(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'type') {
                            system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
                            type_Z = parser.getText();
                        }else if (text == 'text') {
                            system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
                            text = parser.getText();
                            inboundMessage = parser.getText();
                        } else if (text == 'video') {
                            // system.debug('obj:::'+obj.message.content.video.url);
                        }else if(text == 'audio'){
                            // system.debug('obj:::'+obj.message.content.audio.url);
                        }
                        else if(text == 'file'){
                            // system.debug('obj:::'+obj.message.content.file.url);
                        }
                        else if(text == 'image'){
                            // system.debug('obj:::'+obj.message.content.image.url);
                        }
                        else if(text == 'location'){
                            // system.debug('obj:::'+obj.message.content.location.url);
                        }
                        else {
                            System.debug(LoggingLevel.WARN, 'Content consuming unrecognized property: '+text);
                            consumeObject1(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class cls_location1 {
        public String lat;
        public String WA_long;
    }
    Public class cls_audio1 {
        public String url;  //https://api.nexmo.com/v3/media/09061c8d-93ef-455a-bc2f-892137c0a17f
        public String caption;
    }
    Public class cls_video1 {
        public String url;  //https://api.nexmo.com/v3/media/09061c8d-93ef-455a-bc2f-892137c0a17f
        public String caption;
    }
    Public class cls_image1 {
        public String url;  //https://api.nexmo.com/v3/media/09061c8d-93ef-455a-bc2f-892137c0a17f
        public String caption;
    }
    Public class cls_file1 {
        public String url;  //https://api.nexmo.com/v3/media/09061c8d-93ef-455a-bc2f-892137c0a17f
        public String caption;
    }    
    
    public class To1 {
        public String number_Z {get;set;} // in json: number
        public String type_Z {get;set;} // in json: type

        public To1(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'number') {
                            number_Z = parser.getText();
                        } else if (text == 'type') {
                            type_Z = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'To consuming unrecognized property: '+text);
                            consumeObject1(parser);
                        }
                    }
                }
            }
        }
    }
    
    
    public static NEXMO_INB_RESPONSE_PARSER parse1(String json) {
        System.JSONParser parser = System.JSON.createParser(json);
        return new NEXMO_INB_RESPONSE_PARSER(parser);
    }
    
    public static void consumeObject1(System.JSONParser parser) {
        Integer depth = 0;
        do {
            System.JSONToken curr = parser.getCurrentToken();
            if (curr == System.JSONToken.START_OBJECT || 
                curr == System.JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == System.JSONToken.END_OBJECT ||
                curr == System.JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }

    public class NEXMO_RESPONSE_PARSER{

        public String message_uuid {get;set;} 
        public To to {get;set;} 
        public To from_Z {get;set;} // in json: from
        public String timestamp {get;set;} 
        public String status {get;set;} 
        public Error error {get;set;} 

        public NEXMO_RESPONSE_PARSER(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'message_uuid') {
                            message_uuid = parser.getText();
                        } else if (text == 'to') {
                            to = new To(parser);
                        } else if (text == 'from') {
                            from_Z = new To(parser);
                        } else if (text == 'timestamp') {
                            timestamp = parser.getText();
                        } else if (text == 'status') {
                            status = parser.getText();
                        } else if (text == 'error') {
                            error = new Error(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'JSON2Apex consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
        public class Error {
            public Integer code {get;set;} 
            public String reason {get;set;} 

            public Error(JSONParser parser) {
                while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                    if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                        String text = parser.getText();
                        if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                            if (text == 'code') {
                                code = parser.getIntegerValue();
                            } else if (text == 'reason') {
                                reason = parser.getText();
                            } else {
                                System.debug(LoggingLevel.WARN, 'Error consuming unrecognized property: '+text);
                                consumeObject(parser);
                            }
                        }
                    }
                }
            }
        }
        
        public class To {
            public String number_Z {get;set;} // in json: number
            public String type_Z {get;set;} // in json: type

            public To(JSONParser parser) {
                while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                    if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                        String text = parser.getText();
                        if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                            if (text == 'number') {
                                number_Z = parser.getText();
                            } else if (text == 'type') {
                                type_Z = parser.getText();
                            } else {
                                System.debug(LoggingLevel.WARN, 'To consuming unrecognized property: '+text);
                                consumeObject(parser);
                            }
                        }
                    }
                }
            }
        }
        
        
        public static NEXMO_RESPONSE_PARSER parse(String json) {
            System.JSONParser parser = System.JSON.createParser(json);
            return new NEXMO_RESPONSE_PARSER(parser);
        }
        
        public static void consumeObject(System.JSONParser parser) {
            Integer depth = 0;
            do {
                System.JSONToken curr = parser.getCurrentToken();
                if (curr == System.JSONToken.START_OBJECT || 
                    curr == System.JSONToken.START_ARRAY) {
                    depth++;
                } else if (curr == System.JSONToken.END_OBJECT ||
                    curr == System.JSONToken.END_ARRAY) {
                    depth--;
                }
            } while (depth > 0 && parser.nextToken() != null);
        }
        
    
}