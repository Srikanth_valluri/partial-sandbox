/**********************************************************************************************************************
Description: This API class is used for creating Public(Guest) SR - Tenant Registration
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   26-10-2020      | Shubham Suryawanshi | Added developer_message content in its param
***********************************************************************************************************************/

@RestResource(urlMapping='/submitPublicSr/*')
global class SubmitPublicSR_API {

    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        //200 => 'OK',
        //400 => 'Bad Request',
        //401 => 'Unauthorized',
        //500 => 'Internal Server Error' 
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Technical error'
    };

    @HttpPost 
    global static FinalReturnWrapper SubmitSR() {

        SubmitSRResponseWrapper objSrResponse = new SubmitSRResponseWrapper();
        FinalReturnWrapper retunResponse = new FinalReturnWrapper();

        RestRequest req = RestContext.request;
        Blob body = req.requestBody;
        String requestString = body.toString();
        System.debug('requestString');
        System.debug('Request params:' + req.params);

        if(req.params.containskey('recordType') && String.isNotBlank( req.params.get('recordType') )) {

            String recordType = req.params.get('recordType');
            System.debug('recordType:: ' + recordType);

            /************************************* Amenity Booking ***********************************************/
            if(recordType == 'Amenity Booking') {
                //HDAppSubmitAmenityBooking.SubmitSRResponseWrapper obj = new HDAppSubmitAmenityBooking.SubmitSRResponseWrapper();
                //obj = HDAppSubmitAmenityBooking.SubmitAmenityBookingCase(requestString);
                //objSrResponse = (SubmitSRResponseWrapper)JSON.deserialize(JSON.serialize(obj), SubmitSRResponseWrapper.class);

            }
            /************************************* Amenity Booking ***********************************************/

            /************************************* Tenant Registartion ***********************************************/
            else if(recordType == 'Tenant Registration') {
               
                if(String.isBlank(requestString)) {
                    objSrResponse.status_code = 3;
                    objSrResponse.message = 'No JSON body found';
                }
                else {
                    //Add TR class API;
                    System.debug('calling HDAppInsertTenantRegistrationCase2');
                    HDAppInsertTenantRegistrationCase.SubmitSRResponseWrapper obj = new HDAppInsertTenantRegistrationCase.SubmitSRResponseWrapper();
                    obj = HDAppInsertTenantRegistrationCase.InsertTenantRegCase(requestString);
                    objSrResponse = (SubmitSRResponseWrapper)JSON.deserialize(JSON.serialize(obj), SubmitSRResponseWrapper.class);
                }           
            }
            /************************************* Tenant Registartion ***********************************************/


            else {
                objSrResponse.status_code = 2;
                objSrResponse.status = 'failed';
                objSrResponse.message = 'Unknown value found in paramter recordType : '+req.params.get('recordType');
            }
        }
        else {
            objSrResponse.status_code = 2;
            objSrResponse.status = 'failed';
            objSrResponse.message = 'Missing Parameter : recordType in the request URL';
        }

        cls_meta_data objMeta = new cls_meta_data();
        //objMeta.status = objSrResponse.status;
        objMeta.message = objSrResponse.message;
        objMeta.status_code = objSrResponse.status_code;
        objMeta.developer_message = objSrResponse.developer_message;
        objMeta.title = mapStatusCode.get(objSrResponse.status_code);

        cls_data objData = new cls_data();
        objData.tenant_id = objSrResponse.accountId;
        objData.booking_unit_id = objSrResponse.bookingUnitId;
        objData.fm_case_id = objSrResponse.FmCaseId;
        objData.fm_case_number = objSrResponse.FmCaseNumber;
        objData.fm_case_status = objSrResponse.FmCaseStatus;
        objData.owner_party_id = objSrResponse.ownerPartyId;
        objData.unit_reg_id = objSrResponse.regId;
        //objData.submission_date = objSrResponse.submissionDate;
        objData.fm_case_details = objSrResponse.fm_case_details;

        retunResponse.data = objData;
        retunResponse.meta_data = objMeta;

        System.debug('final return response : ' + retunResponse);
        //return objSrResponse;
        return retunResponse;

    }

    global class SubmitSRResponseWrapper {
        public String status;
        //public String message;
        public String accountId;
        public String bookingUnitId;
        public String FmCaseId;
        public String FmCaseNumber;
        public String FmCaseStatus;
        public String ownerPartyId;
        public String regId;
        public String submissionDate;
        public GetTenantRegistrationCase_API.FMCaseWrapper fm_case_details;

        public String title;
        public String message;
        public String developer_message; //newly added
        public Integer status_code;
    }

    //Wrapper classes for returning reponse
    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_data {
        public String tenant_id;
        public String booking_unit_id;
        public String fm_case_id;
        public String fm_case_number;
        public String fm_case_status;
        public String owner_party_id;
        public String unit_reg_id; 
        //public String submission_date;
        public GetTenantRegistrationCase_API.FMCaseWrapper fm_case_details;
    }

    public class cls_meta_data {
        public Integer status_code;
        public String message;
        public String title;
        public String developer_message;   
    }

}