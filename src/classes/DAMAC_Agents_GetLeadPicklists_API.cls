/************************************************************************************************
 * @Name              : DAMAC_Agents_GetLeadPicklists_API
 * @Test Class Name   : DAMAC_Agents_GetLeadPicklists_API_Test
 * @Description       : RestResource Class to retrive the picklist values based on Fiels API Names
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         28/05/2020       Created
***********************************************************************************************/

@RestResource(urlMapping='/GetLeadPicklists/*')
/*
URL - /services/apexrest/GetLeadPicklists/<Picklist API Name>

*/
global with sharing class DAMAC_Agents_GetLeadPicklists_API{   
    
    public static List<String> depPickListBody;
    public static List<String> pickListBody;
    public static Map<String, List<String>> objJsonBodyMap = new Map<String, List<String>>();
    @HttpGet
    global static void doGet(){
        RestRequest request = RestContext.request;
        String pickListApiName = '';      
        pickListApiName = request.requestURI.substringAfter('GetLeadPicklists/');
        System.Debug ('pickListApiName ::::' + pickListApiName);        
        Map<String, String> taskValuesMap = new Map<String, String>();
        Map<String, String> valuesMap = new Map<String, String>();
        List<String> options = new List<String>();
        List<String> optionals = new List<String>();
        
        Integer statusCode = 204;
        String jsonBody = '';
        try{        
            if(pickListApiName != null && pickListApiName != ''){
                statusCode = 200;
                system.debug('Url>>/GetLeadPicklists/'+pickListApiName );
                // fetching values    
                String objectName = 'Inquiry__c'; 
                if(pickListApiName.contains('-')){
                    objectName = pickListApiName.split('-')[1];
                    pickListApiName = pickListApiName.split('-')[0];
                }
                jsonBody = '{"' + objectName + '": {';
                if(pickListApiName.contains(':')){  
                    List<String> fieldList = pickListApiName.split(':');
                    jsonBody +=  addDependencyPickList(fieldList[0].trim(), fieldList[1].trim(), objectName) ;
                } else{
                    jsonBody += addPickList(pickListApiName, objectName);
                }
                jsonBody += '}}';
            } else{
                jsonBody = '';
                statusCode = 200;
                List<String> inquiryPicklists;
                depPickListBody = new List<String>();
                pickListBody = new List<String>();
                //get all picklist fields from Inquiry
                system.debug('retrieving all the picklists>>'+System.Label.TR_AP_Inquiry_Picklists);
                String objectName = 'Inquiry__c'; 
                if(System.Label.TR_AP_Inquiry_Picklists != '' && System.Label.TR_AP_Inquiry_Picklists != NULL){
                    getPickListBody(System.Label.TR_AP_Inquiry_Picklists, objectName);
                }
                /*
                //Task Activity type field
                objectName = 'task'; 
                if(System.Label.TR_AP_Activity_Picklist_Value != '' && System.Label.TR_AP_Activity_Picklist_Value != NULL){
                    getPickListBody(System.Label.TR_AP_Activity_Picklist_Value, objectName);
                } 
                */
                system.debug('objJsonBodyMap: ' + objJsonBodyMap);
                if(objJsonBodyMap.keyset().size() > 0){
                    jsonBody = '{"data": {';
                    for(String objName: objJsonBodyMap.keyset()){
                        if(jsonBody != '{"data": {'){
                            jsonBody += ',';
                        }
                        jsonBody += '"' + objName + '": {';  
                        Boolean firstItem = true;
                        for(String body2: objJsonBodyMap.get(objName)){
                            if(!firstItem){
                                jsonBody += ',';    
                            }
                            jsonBody += body2;
                            firstItem = false;
                        } 
                        jsonBody += '}';
                        system.debug('jsonBody: ' + jsonBody); 
                    }
                    jsonBody += '}}';
                }
                /*
                if(depPickListBody.size() > 0 || pickListBody.size() > 0){
                    jsonBody = '{"data": {';
                    for(String body2: pickListBody){
                        if(jsonBody != '{"data": {'){
                            jsonBody += ',';    
                        }
                        jsonBody += body2;
                    }
                    for(String body2: depPickListBody){
                        if(jsonBody != '{"data": {'){
                            jsonBody += ',';    
                        }
                        jsonBody += body2;
                    }
                    jsonBody += '}}';
                    system.debug('jsonBody: ' + jsonBody);
                } else{
                    statusCode = 406;
                    jsonBody = 'Please Enter a Valid Field API Name';
                    jsonBody = JSON.Serialize(jsonBody );
                } */
            }    
        } catch(exception e){
            statusCode = 406;
            jsonBody = e.getMessage();    
            jsonBody += '\n'+'Please Enter a Valid Field API Name';
            jsonBody += '\n'+'P.S. If you have not given any API Name in the URI, please check your Custom Label too';
            jsonBody = JSON.Serialize(jsonBody ); 
        }
        system.debug('jsonBody: ' + jsonBody);
        RestResponse response = RestContext.response;
        response.statusCode = statusCode;
        response.addHeader('Content-Type', 'application/json');
        response.responseBody = Blob.valueOf(jsonBody);
    }
    
    public static void getPickListBody(String fieldNames, String objectName){
        
        for(String fieldValue:  fieldNames.Split('\\,')){
            if(fieldValue.contains('-')){
                objectName = fieldValue.split('-')[1];
                fieldValue = fieldValue.split('-')[0];
            }
            List<String> objStringList = new List<String>();
            if(objJsonBodyMap.containsKey(objectName)){
                objStringList = objJsonBodyMap.get(objectName);
            }
            if(fieldValue.contains(':')){
                String depBody = addDependencyPickList(fieldValue.split(':')[0].trim(), fieldValue.split(':')[1].trim(), objectName);
                depPickListBody.add(depBody);
                objStringList.add(depBody);
                
            } else{
                pickListBody.add(addPickList(fieldValue, objectName));
                objStringList.add(addPickList(fieldValue, objectName));
            }
             objJsonBodyMap.put(objectName, objStringList);
        }
    }
    
    public static String addPickList(String fieldName, String objectName){
        String body = '';                                  
        Schema.SObjectType inquiryType = Schema.getGlobalDescribe().get(objectName) ;
        Schema.DescribeSObjectResult inquiryResult = inquiryType.getDescribe() ;
        Map<String,Schema.SObjectField> fields = inquiryResult.fields.getMap() ;
        system.debug('fields >>>>'+fields );
        Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();
        system.debug('fieldResult >>>>'+fieldResult );
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        List<String> options = new List<String>();
        system.debug('ple>>>>'+ple);
        for( Schema.PicklistEntry pickListVal : ple){
            System.debug('PickList Values>>'+pickListVal.getValue());
            body =     ' "' +pickListVal.getValue()+ '"';                    
            options.add(body);
            system.debug('body>>'+options);
        }  
        String jsonBody = '';
        Integer i = 1; 
        for(String bodys: options){
            /*if(jsonBody != ''){
                jsonBody += ', "' + i + '":  ' +  bodys;
            } else{
                jsonBody += '"' + fieldName  + '": { "' + i + '" :'  +  bodys;
            }
            i++; */
            if(jsonBody != ''){
                jsonBody += ', ' +  bodys;
            } else{
                jsonBody += '"' + fieldName  + '": ['  +  bodys;
            }
        }
        if(jsonBody != ''){
            jsonBody += ']';
        }
        return jsonBody;
    }
    //For Dependency Picklists
    public static String addDependencyPickList(String plfieldName, String depFieldName, String obj){
        String depBody = '';
        depbody = '"Activity Types": [';  
         //depbody = '"Activity Types": [{';
        //depbody = '"' + depFieldName + '": {';
        Map<String,List<String>> dependMap =  TStringUtils.GetDependentOptions(obj, depFieldName, plfieldName);
        integer c1 = 1;
        for(String key: dependMap.keyset()){
            if(key != null && key != ''){
                if(c1 > 1){
                        depBody += ',';
                }
                integer c2 = 1;
                depbody += '{'+'"Activity": "' + key + '", "Activity OutComes" :[';  //' + plfieldName + '
                for(String val: dependMap.get(key)){
                    if(c2 > 1){
                        depBody += ',';
                    }
                  //  depbody += '"' + c2 + '":' + '"' + val + '"';
                    depbody += '"' + val + '"';
                    c2++;
                }
                depbody += ']}';
                c1++;
            }
        }
        depbody += ']';
        //depbody += '}]';
        system.debug('depbody: ' + depbody);
        return depBody;
    }
}