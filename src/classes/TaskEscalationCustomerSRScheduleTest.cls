/**
 * Ver       Date            Author      		    Modification
 * 1.0    9/30/2019         Arsh Dave              Initial Version 
 * 2.0    10/08/2019        Arsh Dave              Updated the fields to random fields as new fields cannot be deployet on production  
**/
@isTest
public class TaskEscalationCustomerSRScheduleTest{
    
    @testSetup static void setup(){
        
        UserRole objRole = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert objRole;

        User objUserManager = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'Muser000@amamama.com',
            Username = 'Muser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias_M',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = objRole.Id
        );
        insert objUserManager;

        User objUser = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = objRole.Id,
            ManagerId = objUserManager.Id
        );
        insert objUser;
    }

    static testMethod  void callConstructor() {
        
        User objUser = [SELECT Id, Email FROM User WHERE Email = 'puser000@amamama.com'];

        Account objAccount = TestDataFactoryFM.createAccount();
        objAccount.Email__c = 'test@test.com'; 
        objAccount.OwnerId = objUser.Id;
        insert objAccount;

        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAccount);
        insert objSR;

        Booking__c objBooking = TestDataFactoryFM.createBooking(objAccount, objSR);
        insert objBooking;
        
        Booking_Unit__c objBooking_Unit = TestDataFactoryFM.createBookingUnit(objAccount, objBooking);
        insert objBooking_Unit;

        FM_Case__c objFMCase = new FM_Case__c(
            Account__c = objAccount.Id,
            Category_GE__c = 'Change of Contact Details',
            Subcategory_GE__c = 'Change registered email id',
            RecordTypeId = System.Label.FM_Case_General_Enquiry
        );
        insert objFMCase;

        List<Task> objTask = [
            SELECT Id
                , Status
                , ActivityDate
            FROM Task
            WHERE What.Id = :objFMCase.Id   
        ];

        objTask[0].ActivityDate = System.Today();
        update objTask;

        Test.startTest();
            
            FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');
            Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {'https://api.sendgrid.com/v3/mail/send' => sendGridResponse };
            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
            TaskEscalationCustomerSRProcessSchedule objTaskEscalationCustomerSRProcessSchedule = new TaskEscalationCustomerSRProcessSchedule();
            String sch = '0 0 23 * * ?'; 
            system.schedule('Test Account Sales Calculator Check', sch, objTaskEscalationCustomerSRProcessSchedule);            
        Test.stopTest();
        
    }

}