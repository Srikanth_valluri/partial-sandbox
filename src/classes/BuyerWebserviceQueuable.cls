/*************************************************************************************************************
* Name               : BuyerWebserviceQueuable                                                               *
* Description        : Queueable class to send the Joint Buyer Creation request to IPMS from PC Confirmation *
* Created By         : Srikanth                                                                              *
**************************************************************************************************************/
public class BuyerWebserviceQueuable implements Queueable, Database.AllowsCallouts {
    
    
    public String requestBody;
    public String buyerId;
    public String reqName;    
    
    public BuyerWebserviceQueuable (String reqBody, String bId, String requestName) {
        requestBody = reqBody;
        buyerId = bId;
        reqName = requestName;
        
    }
    public void execute(QueueableContext context) {
        AsyncWebserviceForBuyer.sendToIPMS (requestBody, buyerId, reqName);
    }
}