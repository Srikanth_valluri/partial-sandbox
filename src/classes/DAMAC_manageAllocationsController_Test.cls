@isTest
private class DAMAC_manageAllocationsController_Test {

    static testMethod void testResults() {       
        
        
        Unit_Allocation__c testUnitAllocation = new Unit_Allocation__c();
        testUnitAllocation.Start_Date__c = System.today();
        testUnitAllocation.End_Date__c = System.today().addDays(1);
        insert testUnitAllocation;
        
        Inventory__c testInventory = new Inventory__c();
        testInventory.Status__c = 'Released';
        testInventory.Unit_Allocation__c = testUnitAllocation.Id;
        insert testInventory;
        
            
        Inventory_User__c newInventoryUser = new Inventory_User__c();
        newInventoryUser.Start_Date__c = System.today().addDays(-3);
        newInventoryUser.End_Date__c = System.today().addDays(-1);
        newInventoryUser.CurrencyIsoCode = 'EUR';
        
        newInventoryUser.Inventory__c = testInventory.Id;
        newInventoryUser.User__c = UserInfo.getUserId ();
        newInventoryUser.Unit_Allocation__c = testUnitAllocation.Id;
        insert newInventoryUser;
        
        
        ApexPages.currentPage().getParameters().put('id', testUnitAllocation.Id);
        
        ApexPages.currentPage().getParameters().put('invId', String.valueOf(testInventory.Id));
        ApexPages.currentPage().getParameters().put('invUserId', newInventoryUser.Id);
        ApexPages.currentPage().getParameters().put('invIds', testInventory.Id);
        
        Apexpages.StandardController stdController = new Apexpages.StandardController(testUnitAllocation);
        DAMAC_manageAllocationsController controller = new DAMAC_manageAllocationsController(StdController);
        controller.deleteAllInvUsers();
        controller.deleteInvUser();
        controller.deleteAllInv();
    }
}