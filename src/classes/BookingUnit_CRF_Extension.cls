public class BookingUnit_CRF_Extension {
    public Booking_Unit__c objBookingUnit ;
    
    public BookingUnit_CRF_Extension(ApexPages.StandardController stdController){
        this.objBookingUnit = (Booking_Unit__c)stdController.getRecord();                       
        objBookingUnit = [Select id,name,Property_Name__c,Inventory__c,Booking__c,Manager_Name__c,
                          HOS_Name__c,Registration_ID__c,
                          Property_Consultant__c,Unit_Name__c,CurrencyIsoCode, 
                          Registration_Status__c, Registration_DateTime__c     
                          from Booking_Unit__c where id =: objBookingUnit.id ];
    } 
       
    public pagereference ShowCRF(){
        try{
            PenaltyWaiverService.CRFOuterWrap objCRFResponse = PenaltyWaiverService.getCRFDocument(objBookingUnit , '' );
            String strCRFurl = '';
            if( objCRFResponse != null && String.isNotBlank( objCRFResponse.Status ) && objCRFResponse.Status.equalsIgnoreCase('S') &&
                String.isNotBlank( objCRFResponse.data.ATTRIBUTE1 ) ) {
                strCRFurl = objCRFResponse.data.ATTRIBUTE1;
            }
            else if(  objCRFResponse != null && String.isNotBlank( objCRFResponse.Status ) && 
                !objCRFResponse.Status.equalsIgnoreCase('S') ) {
                ApexPages.addmessage(new ApexPages.message(
                ApexPages.severity.Error,'An exception has occurred while generating the CRF. Please contact the support team.'));  
                return null;          
            }
            if(String.isNotBlank(strCRFurl)){
                 pageReference pgr = new pageReference(strCRFurl);
                 pgr.setRedirect(true);             
                 return pgr;
                
            }
            else{
                return null;
            }
        }
        catch(exception ex){
            ApexPages.addmessage(new ApexPages.message(
            ApexPages.severity.Error,ex.getMessage())); 
            return null; 
        }
                    
    }
    
    public pagereference back(){
         pageReference pgr = new pageReference('/'+objBookingUnit.id);
         pgr.setRedirect(true);
         return pgr;
    }
}