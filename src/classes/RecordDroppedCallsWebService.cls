/**
 * Record Dropped Calls WebService
 */
@RestResource(urlMapping='/recordDroppedCall/*')
global class RecordDroppedCallsWebService {

    /**
     * Method to process post request
     */
    @HttpPost
    global static void doPost() {
        RecordDroppedCallsLogic.processRecordDroppedCallsRequest();
    }
}