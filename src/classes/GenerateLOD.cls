public without sharing class GenerateLOD{
    public String LODurl;
    public GenerateLOD(ApexPages.standardController controller){
       
    }//End constructor
    
    public pagereference init(){
        string strPageID = ApexPages.currentPage().getParameters().get('id');
        case objCase = [select Id, CaseNumber, AccountId, Booking_Unit__c
                       , Booking_Unit__r.Registration_ID__c, Recordtype.DeveloperName
                       , Total_Major_Snags__c, Total_Snags_Open__c , Total_Snags_Closed__c
                       , Document_Verified__c, ParentId, Parent.CaseNumber, Parent.Document_Verified__c
                       from case where id =:strPageID];
        if((objCase.ParentId != null
        && objCase.Recordtype.DeveloperName == 'Handover')
        || (objCase.Recordtype.DeveloperName == 'Early_Handover')){
            CallHandoverMQServices.HandoverMQServicesResponse objLOD;
            objLOD= CallHandoverMQServices.CallHandoverMQServiceLoD(objCase.Booking_Unit__r.Registration_ID__c);
            System.debug('----objLOD----'+objLOD);
            LODurl = objLOD.url;               
            System.debug('----LODurl ----'+LODurl );
            list<SR_Attachments__c> lstCaseAttachment = new list<SR_Attachments__c>();
            list<Error_Log__c> lstErrorLog = new list<Error_Log__c>();
            if (LODurl == null) {
                
                SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
                objCaseAttachment.Case__c = objCase.id;
                objCaseAttachment.Name = objCase.CaseNumber+' Letter of Discharge ' +system.now();
                objCaseAttachment.Booking_Unit__c = objCase.Booking_Unit__c;
                lstCaseAttachment.add(objCaseAttachment);
                
                SR_Attachments__c objCaseAttachment1 = new SR_Attachments__c();
                objCaseAttachment1.Case__c = objCase.id;
                objCaseAttachment1.Name = objCase.CaseNumber+' Signed Letter of Discharge ' +system.now();
                objCaseAttachment1.Booking_Unit__c = objCase.Booking_Unit__c;
                lstCaseAttachment.add(objCaseAttachment1);
                
                Error_Log__c objErr = new Error_Log__c();
                objErr.Account__c = objCase.AccountId;
                objErr.Booking_Unit__c = objCase.Booking_Unit__c;
                objErr.Case__c = objCase.id;
                objErr.Error_Details__c = objLOD.P_PROC_MESSAGE;
                if(objCase.RecordType.DeveloperName.equalsIgnoreCase('Early_Handover')){
                    objErr.Process_Name__c = 'Early Handover';
                }else if(objCase.RecordType.DeveloperName.equalsIgnoreCase('Handover')){
                    objErr.Process_Name__c = 'Handover';
                }
                lstErrorLog.add(objErr);
            } else {
                 SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
                 objCaseAttachment.Case__c = objCase.id;
                 objCaseAttachment.Name = objCase.CaseNumber+' Letter of Discharge ' +system.now();
                 objCaseAttachment.Attachment_URL__c = LODurl;
                 objCaseAttachment.Booking_Unit__c = objCase.Booking_Unit__c;
                 objCaseAttachment.IsValid__c = true;
                 lstCaseAttachment.add(objCaseAttachment);
                 
                 SR_Attachments__c objCaseAttachment1 = new SR_Attachments__c();
                 objCaseAttachment1.Case__c = objCase.id;
                 objCaseAttachment1.Name = objCase.CaseNumber+' Signed Letter of Discharge ' +system.now();
                 objCaseAttachment1.Booking_Unit__c = objCase.Booking_Unit__c;
                 lstCaseAttachment.add(objCaseAttachment1);
                 
            }
            system.debug('lstCaseAttachment '+lstCaseAttachment );
            if (lstCaseAttachment != null && lstCaseAttachment.size()>0){
                list<SR_Attachments__c> lstSR = new list<SR_Attachments__c>();
                lstSR = [Select Id, Name, Case__c,Case__r.CaseNumber
                         from SR_Attachments__c 
                         where Case__c =:strPageID
                         and Name Like '%Letter of Discharge%'];
                if(lstSR.Size()>0){
                    delete lstSR; 
                }          
                insert lstCaseAttachment;
                
            }
            if (lstErrorLog != null && lstErrorLog.size()>0){
                insert lstErrorLog;
            }
            
            list<Case> lstCase = new list<Case>();
            for (Case objCase1: [Select Id, CaseNumber, AccountId, Booking_Unit__c, Handover_Status__c, 
                Early_Handover_Status__c, RecordTypeId, RecordType.DeveloperName From Case Where Id =: strPageID]) {
                if (objCase1.RecordType.DeveloperName == 'Handover') {
                    objCase1.Handover_Status__c = 'Letter of Discharge Handed';
                } else if (objCase1.RecordType.DeveloperName == 'Early_Handover') {
                    objCase1.Early_Handover_Status__c = 'Letter of Discharge Generated';
                }
                lstCase.add(objCase1);
            }
            if (lstCase != null && lstCase.size() > 0){
                update lstCase;
            }  
            pagereference newpg = new Pagereference(URL.getSalesforceBaseUrl().toExternalForm()+'/'+strPageID);
            newpg.setRedirect(true);
            return newpg;
            
        }else{
            if(objCase.ParentId == null && objCase.Recordtype.DeveloperName == 'Handover'){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Letter of Discharge cannot be generated on this Service Request, this Service Request is for Customer document verification. Please go to the unit level Service Requests.');
                ApexPages.addMessage(myMsg);
            }
            return null;
        }
    }//End init
}