public class PromoterTargetsController {
    
    public class Target{
        public integer index{get; set;}
        public Target__c tar{get; set;}
        public string searchText{get; set;}
        public Target(integer i, Target__c t, string s){
            tar = t;
            index = i;
            searchText = s;
        }
    }
    
    public list<Target__c> existing = new list<Target__c>();
    public Target__c filterTarget{get; set;}
    public integer selIndex{get; set;}
    public string searchText{get; set;}
    public string rowIndex{get; set;}
    public list<Target> lstTargets{get; set;}
    public string usersJSON{get; set;}
    public list<SelectOption> campaignOptions{get; set;}
    public map<string, User> mpUsers{get; set;}
    public string PromoterRecordTyeId = Schema.SObjectType.Target__c.getRecordTypeInfosByName().get('Promoter').getRecordTypeId();
    public blob fileBody{get; set;}
    public map<string, Campaign__c> mpCampaigns = new map<string, Campaign__c>();
    
    public PromoterTargetsController(){
        filterTarget = new Target__c();
        prepareData();
    }

    public void prepareData(){
        lstTargets = new list<Target>();
        string monthName = System.now().format('MMMM');
        
        if(filterTarget.Year__c == null)
            filterTarget.Year__c = string.valueOf(System.today().year());
        if(filterTarget.Month__c == null)
            filterTarget.month__c = monthName;
        
        mpUsers = new Map<string, User>();
        for(User u : [Select id, name, HR_Employee_ID__c 
                        from User 
                        where isActive = true and Profile.Name = 'Promoter Community Profile'
                        order by name asc]){
            string searchText = u.name+' - '+(u.HR_Employee_ID__c == null ? 'NA': u.HR_Employee_ID__c);
            mpUsers.put(searchText, u);
            lstTargets.add(new Target(lstTargets.size(), new Target__c(RecordTypeId = PromoterRecordTyeId, User__c = u.Id), searchText));
        }
        
        campaignOptions = new list<SelectOption>();
        campaignOptions.add(new SelectOption('','--Select Campaign--'));
        for(Campaign__c camp : [Select id, name, Campaign_Name__c 
                                from Campaign__c 
                                where End_Date__c >= Today and Sys_Active__c = true 
                                and Campaign_Type_New__c = 'Stands'
                                order by name asc limit 500]){ 
            campaignOptions.add(new SelectOption(camp.Id, camp.name+'-'+camp.Campaign_Name__c));
            mpCampaigns.put(camp.name, camp);
        }
        usersJSON = JSON.serialize(mpUsers.keyset());
        
        filterData();
    }
    /*
    public void addRow(){
        lstTargets.add(new Target(lstTargets.size(), new Target__c(RecordTypeId = PromoterRecordTyeId),''));
    }
    */
    
    public void removeRow(){
        system.debug('>>>>>>>selIndex>>>>>>>>>'+selIndex);
        system.debug('>>>>>>>lstTargets>>>>>>>>>'+lstTargets.size());
        if(selIndex != null){
            list<Target__c> lst2Delete = new list<Target__c>();
            Target targ2Delete = lstTargets[selIndex];
            if(targ2Delete.tar.Id != null)
                lst2Delete.add(targ2Delete.tar);
                
            lstTargets.remove(selIndex);
            list<Target> targetsCopy = lstTargets.clone();
            lstTargets = new list<Target>();
            if(!targetsCopy.isEmpty()){
                for(target targ : targetsCopy){
                    lstTargets.add(new Target(lstTargets.size(), targ.tar, targ.searchText));
                }
            }
            else 
                lstTargets.add(new Target(lstTargets.size(), new Target__c(RecordTypeId = PromoterRecordTyeId),''));        
            
            if(!lst2Delete.isEmpty())
                delete lst2Delete;
        }
    }
    
    public void saveTargets(){
        if(filterTarget.Month__c == null || filterTarget.Year__c == null)
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.Fatal, 'Month and Year are mandatory for saving targets.'));
        else{
            list<Target__c> lst2Insert = new list<Target__c>();
            for(Target targ: lstTargets){
                System.debug('>>>>>Leads>>>>>>'+targ.tar.Target_Leads__c);
                System.debug('>>>>>Tours>>>>>>'+targ.tar.target_Tours__c);
                System.debug('>>>>>User>>>>>>'+targ.tar.User__c);
                if(targ.tar.target_Tours__c != null && targ.tar.Target_Leads__c != null && (targ.tar.target_Tours__c > 0 || targ.tar.Target_Leads__c > 0)){
                    targ.tar.Month__c = filterTarget.Month__c;
                    targ.tar.Year__c = filterTarget.Year__c;
                    targ.tar.Uniq__c = string.valueOf(targ.tar.User__c).substring(0,15)+'-'+targ.tar.Month__c+'-'+targ.tar.Year__c;
                    lst2Insert.add(targ.tar);
                }
            }
            
            list<Database.UpsertResult> results = database.upsert(lst2Insert, Target__c.Uniq__c);
            for(Database.UpsertResult res : results){
                System.debug('>>>>>>Errors>>>>>>>>'+res.getErrors());
                System.debug('>>>>>>Success>>>>>>>>'+res.isSuccess());
            }

            prepareData();
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.info, 'Targets saved successfully.'));
        }
    }
    
    public void readCSVFile(){
        if(fileBody == null || filebody.size() == 0){
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.Fatal, 'Please upload file to process.'));
        }
        else{
            string DataAsString = fileBody.toString();
            list<string> csvLines = DataAsString.split('\n');
            lstTargets = new List<Target>();
            integer rowCount = 0;
            for(integer i=1; i< csvLines.size(); i++){
                if(csvLines[i] != null){
                    list<string> vals = csvLines[i].split(',');
                    System.debug('>>>>>vals>>>>>>>'+vals);
                    Target__c tar = new Target__c(RecordTypeId = PromoterRecordTyeId);
                    tar.User__c = vals[0].trim();
                    tar.Campaign__c = (vals[4] != null && vals[4] != '' && mpCampaigns.containsKey(vals[4].trim()) ? mpCampaigns.get(vals[4].trim()).Id : null);
                    tar.Shift__c = vals[5].trim();
                    tar.Target_Leads__c = integer.valueOf(vals[6].trim());
                    tar.Target_Tours__c = integer.valueOf(vals[7].trim());
                    lstTargets.add(new Target(lstTargets.size(), tar, vals[1]+' - '+vals[3]));
                }
            }
        }
            
    }
    
    public void filterData(){
        System.debug('>>>>>>>>>>>>>>>'+filterTarget.Month__c);
        System.debug('>>>>>>>>>>>>>>>'+filterTarget.Year__c);
        
        if(filterTarget.Month__c != null && filterTarget.Year__c != null){
            list<Target__c> existing = [Select id, Campaign__c, User__c, Shift__c, Target_Leads__c, Target_Tours__c from Target__c 
                            where Month__c =: filterTarget.Month__c and Year__c =: filterTarget.Year__c and RecordTypeId =: PromoterRecordTyeId];
            if(!existing.isEmpty()){
                map<string, Target__c> mpExisting = new map<string, Target__c>();
                for(Target__c tar: existing){
                    mpExisting.put(tar.User__c, tar);
                }
                for(Target targ: lstTargets){
                    if(targ.tar.User__c != null && mpExisting.containsKey(targ.tar.User__c)){
                        targ.tar = mpExisting.get(targ.tar.User__c);
                    }
                }
            }
            else{
                lstTargets = new list<Target>();
                mpUsers = new Map<string, User>();
                for(User u : [Select id, name, HR_Employee_ID__c 
                                from User 
                                where isActive = true and Profile.Name = 'Promoter Community Profile']){
                    string searchText = u.name+' - '+(u.HR_Employee_ID__c == null ? 'NA': u.HR_Employee_ID__c);
                    mpUsers.put(searchText, u);
                    lstTargets.add(new Target(lstTargets.size(), new Target__c(RecordTypeId = PromoterRecordTyeId, User__c = u.Id), searchText));
                }
            }
        }
        
    }
    
    public void userSelected(){
        System.debug('>>>>>>>>>>>>'+rowIndex);
        System.debug('>>>>>>>>>>>>'+searchText);
        
        User userRecord = mpUsers.get(searchText);
        integer index = integer.valueOf(rowIndex);
        System.debug('>>>>>>userRecord>>>>>>>'+userRecord);
        lstTargets[index].tar.User__c = userRecord.Id;
        lstTargets[index].searchText = searchText;
    }
    
}