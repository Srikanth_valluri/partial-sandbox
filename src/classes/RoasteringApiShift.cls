public class RoasteringApiShift {
/***************************************************************************************************************
Description: Method to return Shift details response
Return Type: HTTPResponse 
Parameters : Shift Id
****************************************************************************************************************/
    public static HTTPResponse getShiftDetails(String shiftId) {
        if( String.isNotBlank(shiftId)) {
            Credentials_details__c creds = Credentials_details__c.getInstance('Roastering Shift Api');
            if(creds != null && String.isNotBlank(creds.Endpoint__c) && String.isNotBlank(creds.User_Name__c) && String.isNotBlank(creds.Password__c)) { 
                String endpoint = creds.Endpoint__c +shiftId;
                HTTPResponse resp = GenericHttpResponseFromService.returnResponseFromService(endpoint,'GET',creds.User_Name__c,creds.Password__c,'Basic'); 
                return resp;       
            }
        }
        return null;  
    } 
}