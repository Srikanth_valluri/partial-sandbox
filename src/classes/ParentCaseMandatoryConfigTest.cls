@isTest
public  with sharing class ParentCaseMandatoryConfigTest {
    
    @isTest
    private static void testCheckMandatory() {
        // Test data setup
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Waiver').getRecordTypeId();

        Case objCase = new case( recordTypeId = recTypeId
                                , Waiver_Amount_AED__c = 10 );
        insert objCase;
        // Actual test
        Test.startTest();
        ParentCaseMandatoryConfig.CheckMandatory( 'test', objCase.Id ); 
        Test.stopTest();
    
        // Asserts
    }
}