/*
    Test Class Name: Damac_NeedAnalysisControllerTest
*/


global without sharing class Damac_NeedAnalysisController {

    public String inqNumber { get; set; }
    public String inqType { get; set; }
    public String agencyId {get; set; }
    public String agentId {get; set; }
    public String errorMessage { get; set; }
    public Inquiry__c inq { get; set; }
    public Date ownedSince { get; set; }  
    public Date rentedSince { get; set; }
    public String ownedSinceVal { get; set; }  
    public String rentedSinceVal { get; set; }
    public String inqOwnerName { get; set; }
    public Boolean showInqDetails { get; set; }
    public String inquiryDetails { get; set; }
    public List<Inquiry_Family_Details__c> inquiryFamilyList { get; set; }
    public boolean HOSUser { get; set; }
    public boolean DOSUser { get; set; }
    public string agencyName {get;set;}
    public String agentName { get; set; }
        
    public List<String> getBHKOptions () {
        List <String> bhkOptions = new List <String> ();
        bhkOptions.add ('Studio');
        bhkOptions.add ('1');
        bhkOptions.add ('2');
        bhkOptions.add ('3');
        bhkOptions.add ('4');
        bhkOptions.add ('5');
        bhkOptions.add ('6');
        bhkOptions.add ('7');
        bhkOptions.add ('8');
        bhkOptions.add ('9');
        bhkOptions.add ('10');
        return bhkOptions;
    }
    
    public Damac_NeedAnalysisController () {
        inqType = '';
        HOSUser = false;
        agencyId = '';
        agentId = '';
        DOSUser = false;
        agencyname = '';
        agentName = '';
        inqNumber = '';
        inquiryDetails = '';
        errorMessage = '';
        inq = new Inquiry__c();
        inqOwnerName = '';
        inquiryFamilyList = new List<Inquiry_Family_Details__c>();
       
        ownedSinceVal = '';
        rentedSinceVal = '';
       
    }
    public void addInquiryFamily(){
        Inquiry_Family_Details__c eachFamily = new Inquiry_Family_Details__c();
        Integer size = inquiryFamilyList.size();
        eachFamily.Name = String.valueOf(size+1);
        eachFamily.Inquiry__c = inq.id;
        inquiryFamilyList.add(eachFamily);
    }
    
    public void deleteSelectedFamilyMemeber(){
       String family = apexpages.currentpage().getparameters().get('selectedFId');
       for (Integer i = 0 ; i < inquiryFamilyList.size(); i++) {
            if (inquiryFamilyList.get(i).Name == family) {
                inquiryFamilyList.remove(i); 
            }
       }
       for (Integer i = 0 ; i < inquiryFamilyList.size(); i++) {
           inquiryFamilyList.get(i).Name = String.valueOf(i+1);
       
       }
    }
    @RemoteAction
    public static List < Inquiry__c > searchInquiryNameForNeedAnalysis(String pSearchKey) {
        String searchString = '%' +
            pSearchKey +
            '%';        
        ID userId = UserInfo.getUserID();
         String query = 'SELECT Full_Name__c, Name, Pre_Inquiry_Number__c FROM Inquiry__c'+
                        ' WHERE RecordType.Name = \'Inquiry\'';
            //AND Is_Meeting_Confirmed__c = TRUE
            if (Site.getSiteId() == null) {
                 query += ' AND ownerId =:userId ';
            }    
            query += ' AND(First_Name__c LIKE: searchString OR Last_Name__c LIKE: searchString OR Name LIKE: searchString OR (Pre_Inquiry_Number__c LIKE: searchString AND Pre_Inquiry_Number__c != NULL))'+
            + ' ORDER By Name DESC LIMIT 10';
        return Database.Query (query);   
    }
    
    // Utility Method to get all the fields based on object name
    public static string getAllFields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        if (objectType == null)
            return fields;
        
        Map <String, Schema.SObjectField> fieldMap = objectType.getDescribe ().fields.getMap ();
        for (Schema.SObjectField sfield : fieldMap.Values()) {
            fields += sfield.getDescribe().getName ()+ ', ';
        }
        return fields.removeEnd(', '); 
    }
    
    // method to get the events based on the selected Inquiries from the filer button
    public void searchInquiry() {
        errorMessage = '';
        system.debug ('==='+inqNumber);
        
        inq = new Inquiry__c();
        if (inqNumber != '' && inqNumber != NULL) {
            try {
                inq = Database.query('SELECT owner.Name, Agent_Name__r.Name,Agency_Name__r.Name, '+getAllFields('Inquiry__c')+' FROM Inquiry__c '+'WHERE id =: inqNumber ');
                inqOwnerName = inq.owner.name;
                ID userId = inq.ownerId;
                ownedSince = inq.Owned_Since__c;
                agentName = inq.Agent_Name__r.Name;
                agencyName = inq.Agency_Name__r.Name;
                rentedSince = inq.Rented_Since__c;
                inq.Inquiry_Type__c = inqType;
                populateFamilyDetails ();
            } catch (Exception e) {
                inqNumber = '';
                system.debug(':::Entered Catch::::' + e.getMessage() + ':::::' + e.getLineNumber());
                errorMessage = 'There is no inquiry with the search term.';
            }
        } else {
            errorMessage = 'Please choose the inquiry.';
            inqNumber = '';
        }
    }
    
    @RemoteAction 
    global static List<Account> getAgencyDetails(String searchKey){
        List<Account> agencyList = new List<Account>();
        List<Id> accountIdsList = new List<Id>();
        for(Agency_PC__c thisAgency : [SELECT Id, user__c, Agency__c
                                       FROM Agency_PC__c 
                                       WHERE user__c =: UserInfo.getUserId() 
                                       AND  (Agency__r.RecordTypeId =: DamacUtility.getRecordTypeId('Account', 'Individual Agency'))]){
            accountIdsList.add(thisAgency.Agency__c);   
        }
        system.debug('#### accountIdsList = '+accountIdsList);
        for(Account thisAccount : [SELECT Id, Name, RecordTypeId, RecordType.Name,
                                       (SELECT Id, Name, Org_ID_formula__c, Org_ID__c FROM Agent_Sites__r  WHERE End_Date__c = NULL) 
                                   FROM Account 
                                   WHERE Name LIKE: '%' + searchKey + '%' 
                                   AND (Id IN: accountIdsList OR (RecordTypeId =: DamacUtility.getRecordTypeId('Account', 'Corporate Agency') 
                                           AND  Blacklisted__c = false AND  Terminated__c = false))]){
            agencyList.add(thisAccount);    
        }
        return agencyList;
    }
    
    /*
    Query the Corporate Agnecy Details and return results on the Page   
    */
    @RemoteAction 
    global static List<Contact> getCorporateAgents(String selectedCorporateAgency){
        List<Contact> agentsList = new List<Contact>();
        try {
            agentsList = [select id,Name from contact where AccountId=:selectedCorporateAgency];
        } Catch (Exception e) {}
        return agentsList;
    }  

    public void populateFamilyDetails () {
        inquiryFamilyList.addAll([SELECT Name,Age__c ,Location__c ,Relationship__c ,School_College__c FROM Inquiry_Family_Details__c 
                                WHERE Inquiry__c =:inq.id]);
        Integer size = inquiryFamilyList.size();
        if(size == 0){
            Inquiry_Family_Details__c eachFamily = new Inquiry_Family_Details__c();
            eachFamily.Name = String.valueOf(size+1);
            eachFamily.Inquiry__c = inq.id;
            inquiryFamilyList.add(eachFamily);
        }
    }
    // Method to update the Task and Inquiry fields and sharing the Inquiry with TSA TL Owner Field end time when End Time button is clicked
    public pageReference UpdateEndTime() {
        errorMessage = '';
        inquiryDetails = '';
        String pageName ='';
        if(!Test.isRunningTest()){
        pageName = ApexPages.currentPage().getUrl().substringBetween('apex/', '?');
        }else{
            pageName = 'Damac_needAnalysis';
        }
        try {
                List<Inquiry_Family_Details__c> familyToInsert = new List<Inquiry_Family_Details__c>();
                for(Inquiry_Family_Details__c eachFamily : inquiryFamilyList){
                    if(eachFamily.Age__c != null || eachFamily.Location__c != null || eachFamily.Relationship__c  != null  || eachFamily.School_College__c != null ){
                        familyToInsert.add(eachFamily);
                    }
                }
                if(!familyToInsert.isEmpty()){
                    upsert familyToInsert ;
                }
                inq.Inquiry_Status__c = 'Active';
    
                if (inq.Interested_Location__c != NULL) {
                    String location = inq.Interested_Location__c;
                    if (location.contains(', ')) {
                        location = location.removeStart('[').removeEnd(']').replaceAll(', ', ';');
                    } else {
                        location = location.removeStart('[').removeEnd(']');
                    }
                    System.Debug(location);
                    inq.Interested_Location__c = location;
                }
                if (inq.Interested_Development__c != NULL) {
                    String location = inq.Interested_Development__c;
                    if (location.contains(', ')) {
                        location = location.removeStart('[').removeEnd(']').replaceAll(', ', ';');
                    } else {
                        location = location.removeStart('[').removeEnd(']');
                    }
                    inq.Interested_Development__c = location;
                }
                inq.Inquiry_Type__c = inqType;
                if (inq.inquiry_type__c == 'Agent') {
                    inq.Inquiry_Status__c = 'Potential Agent';
                }
                if(pageName == 'Damac_needAnalysis'){
                    if(ownedSinceVal != ''){
                        inq.Owned_Since__c = date.parse(ownedSinceVal) ;
                        inq.Rented_Since__c = null;
                    }
                    if(rentedSinceVal != ''){
                        inq.Rented_Since__c = date.parse(rentedSinceVal) ;
                        inq.Owned_Since__c = null;
                    }
                    if(agencyId != '')
                        inq.Agency_Name__c = agencyId;
                    
                    if(agentId  != '')
                        inq.Agent_Name__c = agentId;
                }
                
                system.debug ('---'+inq.Would_you_like_assistance_on_mortgage__c);
                update inq;
                inquiryDetails = 'Inquiry details updated successFully';
            return null;            
        } catch (Exception e) {
            inquiryDetails = '';
            errormessage = e.getmessage ()+' '+e.getLineNumber ();
            return null;
        }
    }
}