@isTest
public class AP_SRUtilityTest {
    @testSetup static void setupData() {
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole987');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'qw56', email='xqw@email.com',
                emailencodingkey='UTF-8', lastname='Userqw', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xqw@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
            set<string> stProfileNames = new set<string>{'Customer Community - Owner','Agent Admin Manager'};
            Map<string,profile> mpProfile = InitializeSRDataTest.getprofiles(stProfileNames);

            Map<id,integer> mpprofileidUsercount = new Map<Id,integer>();
            Map<id,integer> mpprofileidUsercountinternal = new Map<Id,integer>();

            for(profile p : mpprofile.values()){
                if(p.name == 'Customer Community - Owner')
                mpprofileidUsercount.put(p.id,1);
            if(p.name == 'Agent Admin Manager')
                mpprofileidUsercountinternal.put(p.id,1);
            }
            List<User> lstUsers = new list<user>();
            lstUsers.addall(InitializeSRDataTest.createportalUser(mpprofileidUsercount));
            lstUsers.addall(InitializeSRDataTest.createInternalUser(mpprofileidUsercountinternal));
            insert lstUsers;
        }
    }

    @isTest static void unitTest_1() {
        Test.startTest();
        {
            Account a = InitialiseTestData.getAdminAccount(50000000,'Test agency');
        
        
            a.Agency_Type__c = 'Individual';
            insert a;

            List<string> statuses = new list<string>{'Approved'};
                Map<string,NSIBPM__SR_Status__c> mpsrStatus =  InitializeSRDataTest.createSRStatus(statuses);
             
            Id adminProfile1 = [SELECT Id FROM Profile WHERE Name = 'NewAgentInquiryBOT Profile'].Id;
             User u = new User(alias = 'qw561', email='xqw@em1ail.com',
                emailencodingkey='UTF-8', lastname='Userqw1', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile1, country='United Arab Emirate1s',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xqw1@email.com');
            Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Agent Registration').getRecordTypeId();
            NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
            //system.runAs(u){
                NSIBPM__SR_Status__c srSt = new NSIBPM__SR_Status__c ();
                srSt.NSIBPM__Code__c = 'Closed';
                srSt.Name = 'Approved';
                insert srSt;
                
                String corporateAgencyRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
                list<Account> accList= testdatafactory.createAccountRecords(new list<Account>{new account(recordTypeID=corporateAgencyRecordTypeId)});
                sr.NSIBPM__Customer__c=acclist[0].id;
                sr.RecordTypeId = rectype;
                sr.Eligible_to_Sell_in_Dubai__c = true;
                sr.Agency_Email_2__c = 'test2@gmail.com';
                sr.Alternate_Agency_Email_2__c = 'test1@gmail.com';
                sr.Country_of_Sale__c = 'UAE;KSA;Lebanon';
                sr.NSIBPM__Internal_SR_Status__c = srSt.id;
                sr.Trade_License_Number__c = '123121331312';
                insert sr;
                
           // }
            Amendment__c amd = InitializeSRDataTest.getAmendment(sr.id);
                insert amd;
            
            Map<Id,Account> mpAccount = new Map<Id,Account>{a.id => a};
            AP_SRUtility.CreateChatterGroup(mpAccount);
            ApexPages.currentPage().getHeaders().put('Host','test');
            ApexPages.currentPage().getHeaders().put('Referer','https://test.com');
            AP_SRUtility.getReferer();
            AP_SRUtility.validateCompanySRSubmit(sr.Id, new Set<Id>{amd.id});
            Id RecTypeAg = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Agent Update').getRecordTypeId();
            sr.RecordTypeId = rectypeAg;
            update sr;
                List<Contact> lstc = new List<Contact>();
            lstc.add(new contact(accountid = sr.NSIBPM__Customer__c,firstname='test1',lastname='test2',Status__c ='Active',Shareholding__c = 0,Owner__c=true));
            lstc.add(new contact(accountid = sr.NSIBPM__Customer__c,firstname='test4',lastname='test3',Status__c ='Active',Shareholding__c = 5,Owner__c=true));
            insert lstc;
            AP_SRUtility.validateCompanySRSubmit(sr.Id, new Set<Id>{amd.id});

        }
        Test.stopTest();
    }

    @isTest static void unitTest_2() {
        Test.startTest();
        {
            NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Agent Registration',false,null);
            SR.Agency__c = null;
            insert sr;
            Amendment__c amd = InitializeSRDataTest.getAmendment(sr.id);
            insert amd;

            List<Amendment__c> lstamd = AP_SRUtility.getAllAmendments(sr.id);

            user u = [select id,name from user where profile.name='Agent Admin Manager' and isactive = true limit 1];
            boolean isacces = false;
            system.runAs(u){
               isacces = AP_SRUtility.isUserwithEditPermissions();
            }
            isacces = AP_SRUtility.isUserwithEditPermissions();
            NSIBPM__Document_Master__c DMobj = new NSIBPM__Document_Master__c();
        DMobj.Name = 'VAT Document UAE';
        DMobj.NSIBPM__Code__c = 'VAT_DOC_UAE';
        insert DMobj;

        NSIBPM__SR_Template__c STobj = new NSIBPM__SR_Template__c();
        STobj.Name = 'VAT Update';
        STobj.NSIBPM__SR_RecordType_API_Name__c = 'Agent_Update';
        STobj.NSIBPM__Priority__c = 100;
        insert STobj; 

        NSIBPM__SR_Template_Docs__c STDobj = new NSIBPM__SR_Template_Docs__c();
        STDobj.NSIBPM__Document_Description_External__c = 'VAT Document UAE';
        STDobj.NSIBPM__SR_Template__c = STobj.Id;
        STDobj.NSIBPM__Document_Master__c = DMobj.id;
        STDobj.NSIBPM__Added_through_Code__c = true;
        STDobj.NSIBPM__On_Submit__c = true;
        insert STDobj;   
            NSIBPM__Service_Request__c objsrtest = AP_SRUtility.getSRDetails(SR.id);
        }
        Test.stopTest();
    }

    @isTest static void unitTest_4() {
        Test.startTest();
        {
            List<Account> lstaccount = new List<account>();
            Id RecTypeIndividualAgency = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual Agency').getRecordTypeId();
            Id RecTypeCorporateAgency = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
            lstaccount.add(new account(recordtypeid = RecTypeIndividualAgency, firstname='test',lastname='test',ID_Type__pc='Passport',ID_Number__pc='12345'));
            lstaccount.add(new account(recordtypeid = RecTypeCorporateAgency, name='test',Trade_License_Number__c = 'test'));
            insert lstaccount;

            NSIBPM__Document_Master__c DMobj = new NSIBPM__Document_Master__c();
            DMobj.Name = 'VAT Document UAE';
            DMobj.NSIBPM__Code__c = 'VAT_DOC_UAE';
            insert DMobj;

            NSIBPM__SR_Template__c STobj = new NSIBPM__SR_Template__c();
            STobj.Name = 'VAT Update';
            STobj.NSIBPM__SR_RecordType_API_Name__c = 'Agent_Update';
            STobj.NSIBPM__Priority__c = 100;
            insert STobj; 

            NSIBPM__SR_Template_Docs__c STDobj = new NSIBPM__SR_Template_Docs__c();
            STDobj.NSIBPM__Document_Description_External__c = 'VAT Document UAE';
            STDobj.NSIBPM__SR_Template__c = STobj.Id;
            STDobj.NSIBPM__Document_Master__c = DMobj.id;
            STDobj.NSIBPM__Added_through_Code__c = true;
            STDobj.NSIBPM__On_Submit__c = true;
            insert STDobj;   
            NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Agent Registration',false,null);
            sr.Agency_Type__c = 'Corporate';
            sr.Agency__c = null;
            sr.Trade_License_Number__c = 'test';
            insert sr;

            List<Amendment__c> lstamd = new List<Amendment__c>();
            lstamd.add(InitializeSRDataTest.getAmendment(sr.id));
            lstamd.add(InitializeSRDataTest.getAmendment(sr.id));
            lstamd.add(InitializeSRDataTest.getAmendment(sr.id));
            lstamd.add(InitializeSRDataTest.getAmendment(sr.id));
            lstamd[0].Authorised_Signatory__c = true;
            lstamd[2].Agent_Representative__c = true;
            lstamd[3].Portal_Administrator__c = true;
            lstamd[3].Owner__c = true;
            lstamd[3].Shareholding__c = 100;
            insert lstamd;

            string str = AP_SRUtility.validateSRSubmit(sr.id);

            lstamd[1].Authorised_Signatory__c = true;
            update lstamd[1];
            str = AP_SRUtility.validateSRSubmit(sr.id);
            

            sr.Agency_Type__c = 'Individual';
            update sr;
           String corporateAgencyRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
            list<Account> accList= testdatafactory.createAccountRecords(new list<Account>{new account(recordTypeID=corporateAgencyRecordTypeId)});
            sr.NSIBPM__Customer__c=acclist[0].id;
            contact cnt=new contact(accountId=accList[0].id,lastName='testCnt');
            insert cnt;
            str = AP_SRUtility.validateSRSubmit(sr.id);
            List<Account> lstDupAcc = AP_SRUtility.checkDupTLNumber(sr.Trade_License_Number__c,'Agent_Registration','Individual','Passport','12345');
            lstDupAcc = AP_SRUtility.checkDupTLNumber(sr.Trade_License_Number__c,'Agent_Registration','Corporate','Passport','12345');
            lstDupAcc =AP_SRUtility.checkDupTLNumber(sr.Trade_License_Number__c,'Agent_Registration');

            List<string> statuses = new list<string>{'fromcode','tocode'};
            Map<string,NSIBPM__Status__c> stepStatuses = InitializeSRDataTest.createStepStatus(statuses);

            Map<string,NSIBPM__SR_Status__c> srStatuses = InitializeSRDataTest.createSRStatus(statuses);

            List<NSIBPM__Transition__c> trsition = new List<NSIBPM__Transition__c>();
            trsition.add(new NSIBPM__Transition__c(NSIBPM__From__c = stepStatuses.values()[0].id, NSIBPM__To__c = stepStatuses.values()[1].id,NSIBPM__Transition_Code__c = 'test11'));
            insert trsition;

            NSIBPM__SR_Steps__c srStep= new NSIBPM__SR_Steps__c();
            insert srStep;

            List<NSIBPM__Step_Transition__c> lstStepTrans = new List<NSIBPM__Step_Transition__c>();
            lstStepTrans.add(new NSIBPM__Step_Transition__c(NSIBPM__SR_Step__c=srStep.id,NSIBPM__Transition__c =trsition[0].id,NSIBPM__SR_Status_Internal__c = srStatuses.values()[0].Id, NSIBPM__SR_Status_External__c = srStatuses.values()[0].Id));
            insert lstStepTrans;

            id trsnid = AP_SRUtility.getSRStatusFromTransition(srStep.id, 'fromcode', 'tocode');

        }
    }

    @isTest static void unitTest_5() {
        

        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'qw56', email='xqw1@email.com',
                emailencodingkey='UTF-8', lastname='Userqw', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xqw11@email.com');
        insert adminUser;
        Test.startTest();
        {
            NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Agent Registration',false,null);
            SR.Agency__c = null;
            String corporateAgencyRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
            account acc=new account();
            acc.Name = 'Test Account';
            acc.Agency_Short_Name__c = 'testShrName';
            acc.RecordTypeId=corporateAgencyRecordTypeId;
            insert acc;
            sr.NSIBPM__Customer__c = acc.id;
            //sr.Agency__c = acc.id;
            sr.Agency_Type__c='Corporate';
            sr.ownerId = adminUser.Id;
            insert sr;

             NSIBPM__Document_Master__c DMobj = new NSIBPM__Document_Master__c();
            DMobj.Name = 'VAT Document UAE';
            DMobj.NSIBPM__Code__c = 'VAT_DOC_UAE';
            insert DMobj;

            NSIBPM__SR_Template__c STobj = new NSIBPM__SR_Template__c();
            STobj.Name = 'VAT Update';
            STobj.NSIBPM__SR_RecordType_API_Name__c = 'Agent_Update';
            STobj.NSIBPM__Priority__c = 100;
            insert STobj; 

            NSIBPM__SR_Template_Docs__c STDobj = new NSIBPM__SR_Template_Docs__c();
            STDobj.NSIBPM__Document_Description_External__c = 'VAT Document UAE';
            STDobj.NSIBPM__SR_Template__c = STobj.Id;
            STDobj.NSIBPM__Document_Master__c = DMobj.id;
            STDobj.NSIBPM__Added_through_Code__c = true;
            STDobj.NSIBPM__On_Submit__c = true;
            insert STDobj;   
            List<Contact> lstc = new List<Contact>();
            lstc.add(new contact(accountid = sr.NSIBPM__Customer__c,firstname='test1',lastname='test2'));
            lstc.add(new contact(accountid = sr.NSIBPM__Customer__c,firstname='test4',lastname='test3'));
            insert lstc;

            List<Amendment__c> lstamd = new List<Amendment__c>();
            lstamd.add(InitializeSRDataTest.getAmendment(sr.id));
            lstamd[0].contact__c = lstc[0].id;
            lstamd[0].ownerId = adminUser.Id;
            insert lstamd;

            AP_SRUtility.getAllAmendmentsForContacts(SR); 

            List<sobject> lstobj = AP_SRUtility.getRecords('Contact','');
        }
        Test.stopTest();
    }

    @isTest static void unitTest_7() {
        Test.startTest();
        {
            List<Account> lstaccount = new List<account>();
            Id RecTypeIndividualAgency = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual Agency').getRecordTypeId();
            Id RecTypeCorporateAgency = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
            lstaccount.add(new account(recordtypeid = RecTypeIndividualAgency, firstname='test',lastname='test',ID_Type__pc='Passport',ID_Number__pc='12345'));
            lstaccount.add(new account(recordtypeid = RecTypeCorporateAgency, name='test',Trade_License_Number__c = 'test'));
            insert lstaccount;

            NSIBPM__Document_Master__c DMobj = new NSIBPM__Document_Master__c();
            DMobj.Name = 'VAT Document UAE';
            DMobj.NSIBPM__Code__c = 'VAT_DOC_UAE';
            insert DMobj;

            List<string> statuses = new list<string>{'Submitted'};
                Map<string,NSIBPM__SR_Status__c> mpsrStatus =  InitializeSRDataTest.createSRStatus(statuses);


            NSIBPM__SR_Template__c STobj = new NSIBPM__SR_Template__c();
            STobj.Name = 'VAT Update';
            STobj.NSIBPM__SR_RecordType_API_Name__c = 'Agent_Update';
            STobj.NSIBPM__Priority__c = 100;
            insert STobj; 

            NSIBPM__SR_Template_Docs__c STDobj = new NSIBPM__SR_Template_Docs__c();
            STDobj.NSIBPM__Document_Description_External__c = 'VAT Document UAE';
            STDobj.NSIBPM__SR_Template__c = STobj.Id;
            STDobj.NSIBPM__Document_Master__c = DMobj.id;
            STDobj.NSIBPM__Added_through_Code__c = true;
            STDobj.NSIBPM__On_Submit__c = true;
            insert STDobj;   
            NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Agent Update',false,null);
            sr.Agency_Type__c = 'Corporate';
            sr.NSIBPM__SR_Template__c = STobj.Id;
            sr.NSIBPM__Internal_SR_Status__c = mpsrStatus.get('Submitted').id;
            sr.Agency__c = null;
            sr.Trade_License_Number__c = 'test';
            upsert sr;

            List<Amendment__c> lstamd = new List<Amendment__c>();
            lstamd.add(InitializeSRDataTest.getAmendment(sr.id));
            lstamd.add(InitializeSRDataTest.getAmendment(sr.id));
            lstamd.add(InitializeSRDataTest.getAmendment(sr.id));
            lstamd.add(InitializeSRDataTest.getAmendment(sr.id));
            lstamd[0].Authorised_Signatory__c = true;
            lstamd[2].Agent_Representative__c = true;
            lstamd[3].Portal_Administrator__c = true;
            lstamd[3].Owner__c = true;
            lstamd[3].Shareholding__c = 100;
            lstamd[3].ID_Expiry_Date__c = System.Today().adddays(+10); 
            lstamd[1].Owner__c = true;
            lstamd[1].Shareholding__c = 0;
            insert lstamd;

            string str = AP_SRUtility.validateSRSubmit(sr.id);

            lstamd[1].Authorised_Signatory__c = true;
            update lstamd[1];
            str = AP_SRUtility.validateSRSubmit(sr.id);

            sr.Agency_Type__c = 'Individual';
            update sr;
           String corporateAgencyRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
            list<Account> accList= testdatafactory.createAccountRecords(new list<Account>{new account(recordTypeID=corporateAgencyRecordTypeId)});
            sr.NSIBPM__Customer__c=acclist[0].id;

           contact cnt=new contact(accountId=accList[0].id,lastName='testCnt');
            insert cnt;
            str = AP_SRUtility.validateSRSubmit(sr.id);

        }
    }


    @isTest static void unitTest_3() {
        
        String corporateAgencyRecordTypeId = 
            Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();

        /*Inquiry__c inq = new Inquiry__c() ;
        inq.First_Name__c = 'test';
        inq.Last_Name__c = 'test';
        inq.Preferred_Language__c = 'Arabic';
        inq.Mobile_Phone__c = '784512369';
        inq.Mobile_Phone_Encrypt__c = '784512369';
        inq.Primary_Contacts__c = 'Mobile Phone';
        inq.Mobile_CountryCode__c = 'India: 0091';
        inq.Email__c = 'aa@bbb.com';
        inq.Agency_Type__c='Individual';
        insert inq;
        */
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.Agency_Short_Name__c = 'testShrName';
        acc.RecordTypeId = corporateAgencyRecordTypeId;
        insert acc;

        Account acc1 = new Account();
        acc1.Name = 'Test Account';
        acc1.Agency_Short_Name__c = 'testShrName';
        acc1.RecordTypeId = corporateAgencyRecordTypeId;
        insert acc1;
        
       
        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Agent Registration', true, null);
        SR.Agency__c = null;
        SR.NSIBPM__Customer__c = acc.id;
        SR.Agency_Type__c = 'Corporate';
        SR.Trade_License_Number__c = '0000000000';
        insert SR;

        NSIBPM__Service_Request__c SR1 = InitializeSRDataTest.getSerReq('Agent Registration', true, null);
        SR1.Agency__c = null;
        SR1.NSIBPM__Customer__c = acc1.id;
        SR1.Agency_Type__c = 'Corporate';
        SR1.Trade_License_Number__c = '888888888';
        SR1.Agency_Corporate_Type__c = 'Real Estate'; 
        SR1.City_Of_Incorporation_New__c = 'Dubai';
        insert SR1;

        Amendment__c amd = InitializeSRDataTest.getAmendment(SR.id);
        amd.Email__c = 'xx@yy.com';
        amd.ID_Type__c = 'Passport';
        amd.ID_Number__c = '11111111';
        amd.ID_Issue_Date__c = System.Today().adddays(-4);
        amd.ID_Expiry_Date__c = System.Today().adddays(10); 
        amd.Owner__c = true;
        amd.Designation__c = 'test';
        amd.Portal_Administrator__c = true;
        amd.Agent_Representative__c = true;
        amd.Authorised_Signatory__c = true;
        amd.Shareholding__c = 100;
        insert amd;

        Amendment__c amd1 = new Amendment__c(
            Service_Request__c = SR.id, 
            Owner__c = true, 
            Agent_Representative__c = true 
        );

        Amendment__c amd2 = new Amendment__c(
            Service_Request__c = SR.id 
        );

        Test.startTest();
            AP_SRUtility.validateAmendment(amd2,SR1);
            AP_SRUtility.validateAmendment(amd1,SR1);
            AP_SRUtility.validateSRSubmit(SR.Id);
            AP_SRUtility.validateSRSubmit(SR1.Id);
            //SR.Inquiry__c = inq.Id;
            update SR;
            AP_SRUtility.validateSRSubmit(SR.Id);
        Test.stopTest();
    }

    @isTest static void unitTest_8() {
        Page_Flow__c pageFlow1 = InitialiseTestData.createPageFlow();
        insert pageFlow1 ;
            Account a = InitialiseTestData.getAdminAccount(50000000,'Test agency');
        
        
            a.Agency_Type__c = 'Individual';
            insert a;

        Page__c page1 = InitialiseTestData.createPageForPageFlow('Process_Flow',pageFlow1.Id,1);
        insert page1;

        List<NSIBPM__SR_Template__c> SRTemplateList =  InitialiseTestData.createTestTemplateRecords(new List<NSIBPM__SR_Template__c>{new NSIBPM__SR_Template__c()});
        List<NSIBPM__SR_Template__c> SRTemplateListNew = new List<NSIBPM__SR_Template__c>();
        for(NSIBPM__SR_Template__c tempObj :SRTemplateList){
            tempObj.NSIBPM__SR_RecordType_API_Name__c = 'Agent_Registration';
            SRTemplateListNew.add(tempObj);
        }
        update SRTemplateListNew;

       /* List<NSIBPM__Service_Request__c> SRList  = 
            InitialiseTestData.createTestServiceRequestRecords(
            new List<NSIBPM__Service_Request__c>{
                new NSIBPM__Service_Request__c(
                recordTypeId = InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Registration'),
                NSIBPM__SR_Template__c = SRTemplateList[0].Id)
            }
            );*/
         List<NSIBPM__Service_Request__c> SRList  = InitialiseTestData.createTestServiceRequestRecords(new List<NSIBPM__Service_Request__c>{
                    new NSIBPM__Service_Request__c(recordTypeId = InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Registration'),
                                                    NSIBPM__SR_Template__c = SRTemplateList[0].Id)});

        NSIBPM__Service_Request__c serviceRequest = SRList[0];
        serviceRequest.NSIBPM__Customer__c=a.id;
        //system.debug('>>>>SRList[0].nsibpm__record_type_name__c>>>'+SRList[0].nsibpm__record_type_name__c);
        upsert serviceRequest;
        system.debug('>>>>serviceRequest.nsibpm__record_type_name__c>>>'+serviceRequest.nsibpm__record_type_name__c);
         NSIBPM__SR_Template__c srTemplate = InitializeSRDataTest.createSRTemplate('Deal');
            srTemplate.NSIBPM__SR_RecordType_API_Name__c = 'Deal';
            insert srTemplate;
        List<NSIBPM__Status__c> createStatus = new List<NSIBPM__Status__c>();
         List<NSIBPM__Status__c> createStatusNew = new List<NSIBPM__Status__c>();
        createStatus = InitialiseTestData.createStatusRecords(
                new List<NSIBPM__Status__c>{
                    new NSIBPM__Status__c(NSIBPM__Code__c = 'AWAITING_ADDITIONAL_INFO', Name = 'AWAITING_ADDITIONAL_INFO'),
                    new NSIBPM__Status__c(NSIBPM__Code__c = 'MORE_INFO_UPDATED', Name = 'MORE_INFO_UPDATED'),
                    new NSIBPM__Status__c(NSIBPM__Code__c = 'AWAITING_FFA_AA', Name = 'AWAITING_FFA_AA'),
                    new NSIBPM__Status__c(NSIBPM__Code__c = 'FFA_AA_UPLOADED', Name = 'FFA_AA_UPLOADED')});
        for(NSIBPM__Status__c statusObj : createStatus){
            statusObj.NSIBPM__Type__c = 'Start';
            createStatusNew.add(statusObj);
        }
        update createStatusNew;
        NSIBPM__SR_Steps__c srStep= new NSIBPM__SR_Steps__c();
        insert srStep;
        
        List<NSIBPM__Step__c> createStepList = InitialiseTestData.createTestStepRecords(
            new List<NSIBPM__Step__c>{
                new NSIBPM__Step__c(NSIBPM__SR__c = SRList[0].id, NSIBPM__Status__c = createStatusNew[0].Id, NSIBPM__SR_Step__c = srStep.id),
                new NSIBPM__Step__c(NSIBPM__SR__c = SRList[0].id, NSIBPM__Status__c = createStatusNew[1].Id, NSIBPM__SR_Step__c = srStep.id)});

        List<Amendment__c> amdList = new List<Amendment__c>();
       for(integer i=0;i<5;i++){
           Amendment__c newAmd =  new Amendment__c();
           if(i==0){
               newAmd.Owner__c = true;
               newAmd.Shareholding__c =100;
           }
           newAmd.Authorised_Signatory__c = true;
           newAmd.Agent_Representative__c =true;
           newAmd.Portal_Administrator__c = true;
           newAmd.Email__c ='test'+i+'@test.com';
           newAmd.ID_Type__c = 'Visa';
           amdList.add(newAmd);
       }
       
       insert amdList;
       
        List<NSIBPM__Document_Master__c> DM_List = InitialiseTestData.createSRDocMasterRecords(5);
        
        NSIBPM__SR_Template_Docs__c SrTemplateDoc = new NSIBPM__SR_Template_Docs__c();
        SrTemplateDoc.NSIBPM__SR_Template__c = SRTemplateListNew[0].Id;
        SrTemplateDoc.NSIBPM__Document_Master__c = DM_List[0].Id;
        SrTemplateDoc.NSIBPM__Added_through_Code__c = true;
        SrTemplateDoc.NSIBPM__On_Submit__c = true;
        insert SrTemplateDoc;
        
        
        Amendment__c newAmd1 =  new Amendment__c();
        newAmd1.Authorised_Signatory__c = true;
        newAmd1.Agent_Representative__c =true;
        newAmd1.Portal_Administrator__c = true;
        newAmd1.Email__c ='test34@test.com';
        newAmd1.ID_Type__c = 'Visa';
        insert newAmd1 ;
       
       NSIBPM__SR_Doc__c srdo = new NSIBPM__SR_Doc__c();
       srdo.Amendment__c = amdList[0].Id;
       srdo.NSIBPM__Service_Request__c = SRList[0].Id;
       insert srdo;
       
        Section__c sec = new Section__c();
        sec.page__c = page1.id;
        insert sec;
        
        Section_Detail__c d = new Section_Detail__c();
        d.section__c = sec.id;
        d.Navigation_Directions__c = 'Forward';
        insert d;

        Document_Template__c objDocTemplate1 = new Document_Template__c();
        objDocTemplate1.Code__c = 'POWER_OF_ATTORNEY';
        objDocTemplate1.In_Use__c = true;
        objDocTemplate1.Conditions_Filter__c = '1 AND 2';
        objDocTemplate1.Document_Name__c = 'Bank statement';
        objDocTemplate1.SR_Record_Type__c = 'Agent_Registration';
        objDocTemplate1.DDP__c = 'a0V0Y000000vv6L';
        objDocTemplate1.DDP_Delivery_Option__c = 'a0T0Y000000yaR8';
        objDocTemplate1.SR_Record_Type__c = 'Agent_Registration';
        objDocTemplate1.User_Doc__c = true;
        insert objDocTemplate1;

        Document_Template__c objDocTemplate2 = new Document_Template__c();
        objDocTemplate2.Code__c = 'VISA_COPY';
        objDocTemplate2.In_Use__c = true;
        objDocTemplate2.Conditions_Filter__c = '1 AND 2';
        objDocTemplate2.Document_Name__c = 'Bank statement';
        objDocTemplate2.SR_Record_Type__c = 'Agent_Registration';
        objDocTemplate2.DDP__c = 'a0V0Y000000vv6L';
        objDocTemplate2.DDP_Delivery_Option__c = 'a0T0Y000000yaR8';
        objDocTemplate2.SR_Record_Type__c = 'Agent_Registration';
        objDocTemplate2.User_Doc__c = true;
        insert objDocTemplate2;

        Document_Template__c objDocTemplate3 = new Document_Template__c();
        objDocTemplate3.Code__c = 'BROKER_CARD';
        objDocTemplate3.In_Use__c = true;
        objDocTemplate3.Conditions_Filter__c = '1 AND 2';
        objDocTemplate3.Document_Name__c = 'Bank statement';
        objDocTemplate3.SR_Record_Type__c = 'Agent_Registration';
        objDocTemplate3.DDP__c = 'a0V0Y000000vv6L';
        objDocTemplate3.DDP_Delivery_Option__c = 'a0T0Y000000yaR8';
        objDocTemplate3.SR_Record_Type__c = 'Agent_Registration';
        objDocTemplate3.User_Doc__c = true;
        insert objDocTemplate3;

        Document_Template__c objDocTemplate4 = new Document_Template__c();
        objDocTemplate4.Code__c = 'NATIONAL_ID_COPY';
        objDocTemplate4.In_Use__c = true;
        objDocTemplate4.Conditions_Filter__c = '1 AND 2';
        objDocTemplate4.Document_Name__c = 'Bank statement';
        objDocTemplate4.SR_Record_Type__c = 'Agent_Registration';
        objDocTemplate4.DDP__c = 'a0V0Y000000vv6L';
        objDocTemplate4.DDP_Delivery_Option__c = 'a0T0Y000000yaR8';
        objDocTemplate4.SR_Record_Type__c = 'Agent_Registration';
        objDocTemplate4.User_Doc__c = true;
        insert objDocTemplate4;
        System.debug('objDocTemplate4-------->' + objDocTemplate4);

        Document_Template__c objDocTemplate5 = new Document_Template__c();
        objDocTemplate5.Code__c = 'PROOF_OF_EMP';
        objDocTemplate5.In_Use__c = true;
        objDocTemplate5.Conditions_Filter__c = '1 AND 2';
        objDocTemplate5.Document_Name__c = 'Bank statement';
        objDocTemplate5.SR_Record_Type__c = 'Agent_Registration';
        objDocTemplate5.DDP__c = 'a0V0Y000000vv6L';
        objDocTemplate5.DDP_Delivery_Option__c = 'a0T0Y000000yaR8';
        objDocTemplate5.SR_Record_Type__c = 'Agent_Registration';
        objDocTemplate5.User_Doc__c = true;
        insert objDocTemplate5;

        Document_Template__c objDocTemplate6 = new Document_Template__c();
        objDocTemplate6.Code__c = 'PASSPORT_COPY';
        objDocTemplate6.In_Use__c = true;
        objDocTemplate6.Conditions_Filter__c = '1 AND 2';
        objDocTemplate6.Document_Name__c = 'Bank statement';
        objDocTemplate6.SR_Record_Type__c = 'Agent_Registration';
        objDocTemplate6.DDP__c = 'a0V0Y000000vv6L';
        objDocTemplate6.DDP_Delivery_Option__c = 'a0T0Y000000yaR8';
        objDocTemplate6.SR_Record_Type__c = 'Agent_Registration';
        objDocTemplate6.User_Doc__c = true;
        insert objDocTemplate6;
        List<Contact> lstc = new List<Contact>();
        lstc.add(new contact(accountid = serviceRequest.NSIBPM__Customer__c,firstname='test1',lastname='test2'));
        lstc.add(new contact(accountid = serviceRequest .NSIBPM__Customer__c,firstname='test4',lastname='test3'));
        insert lstc;

        NSIBPM__Service_Request__c sr1 = [SELECt ID, nsibpm__record_type_name__c,Agency_Type__c,Record_Type_Name__c,Country_Of_Incorporation_New__c,
                                            City_Of_Incorporation_New__c  FROM NSIBPM__Service_Request__c WHERE ID = :serviceRequest.Id];

        Test.startTest();
             AP_SRUtility.createSRDocuments(amdList[0],sr1);
            AP_SRUtility.createUnitDocuments(amdList,sr1,'test');
            amdList[0].Contact__c = lstc[0].id;
            update amdList;
            AP_SRUtility.createUnitDocuments(amdList,sr1,'test');
            AP_SRUtility.deleteSRDocs(amdList[0],'Upsert');
            AP_SRUtility.deleteSRDocs(amdList[0],'Edit'); 
            AP_SRUtility.createSRDoc('Test Doc',SrTemplateDoc,amdList[0].Id,serviceRequest);
            AP_SRUtility.updateStepStatus('AWAITING_ADDITIONAL_INFO','MORE_INFO_UPDATED',SRList[0].Id);
            AP_SRUtility.updateStepStatus('MORE_INFO_UPDATED','MORE_INFO_UPDATED',SRList[0].Id);
        Test.stopTest();

    }


    public static User portalUser;
    public static Contact adminContact;
    public static Account adminAccount;

    @isTest static void unitTest_9() { 
        User eachUser = [SELECT ID FROM User WHERE UserRoleId != null LIMIT 1];
        
        adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
        
        adminAccount.OwnerId = eachUser.Id;
        adminAccount.Agency_Type__c = 'Individual';
        insert adminAccount;
        system.debug('adminAccount = '+adminAccount);
        adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
        adminContact.Owner__c = true;
        adminContact.Agent_Representative__c= true;
        adminContact.Portal_Administrator__c= true;
        adminContact.FirstName = 'test1';
        adminContact.LastName = 'test2';
        insert adminContact;
        portalUser = InitialiseTestData.getPortalUser('t11t@test.com', adminContact.Id, 'Admin');
        Id inquiryRecordTypeId   = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        Inquiry__c newInquiry = new Inquiry__c();
        newInquiry.RecordTypeId = inquiryRecordTypeId ;
        newInquiry.First_Name__c = 'Test';
        newInquiry.Last_Name__c = 'Inquiry';
        newInquiry.Mobile_Phone_Encrypt__c = '05789088';
        newInquiry.Mobile_Phone__c = '05789088';
        newInquiry.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        newInquiry.Email__c = 'test@mail.com';
        newInquiry.Preferred_Language__c = 'English';
        newInquiry.Inquiry_Source__c = 'Social' ;
        newInquiry.Inquiry_Status__c = 'New' ;
        insert newInquiry ;
        /*Test.startTest();
        {*/
            NSIBPM__SR_Template__c srTemplate = InitializeSRDataTest.createSRTemplate('Deal');
            srTemplate.NSIBPM__SR_RecordType_API_Name__c = 'Agent_Update';
            insert srTemplate;


            NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);
            sr.ID_Type__c = null;
            sr.NSIBPM__SR_Template__c = srTemplate.id;
            insert sr;

            List<string> lstDocNames = new List<string>{'TestDoc','TestDoc1'};
            List<NSIBPM__Document_Master__c> lstMdocs = InitializeSRDataTest.createMasterDocs(lstDocNames);
            insert lstMdocs;

            List<NSIBPM__SR_Template_Docs__c> lstTempDocs =  InitializeSRDataTest.createSRTemplateDocs(lstMdocs,srTemplate.id);
            insert lstTempDocs;
         
            Booking__c bk = InitializeSRDataTest.createBooking(sr.id);
            insert bk;

            List<Buyer__c> lstbuyrs = new List<Buyer__c>();
            lstbuyrs.add(InitializeSRDataTest.createBuyer(bk.id,false));
            lstbuyrs.add(InitializeSRDataTest.createBuyer(bk.id,false));
            lstbuyrs.add(InitializeSRDataTest.createBuyer(bk.id,false));
            lstbuyrs[0].Passport_Number__c = '23456';
            lstbuyrs[0].Account__c = adminAccount.id;
            lstbuyrs[0].Inquiry__c = newInquiry.id;
            lstbuyrs[1].Passport_Number__c = '7865';
            lstbuyrs[1].Account__c = adminAccount.id;
            lstbuyrs[1].Inquiry__c = newInquiry.id;
            lstbuyrs[1].Nationality__c = 'qatari';
            lstbuyrs[2].Passport_Number__c = '435678';
            lstbuyrs[2].Account__c = adminAccount.id;
            lstbuyrs[2].Inquiry__c = newInquiry.id;
            lstbuyrs[2].Date_of_Birth__c = '12/3/2000';
            system.runAs(portalUser) {        
                try {        
                    insert lstbuyrs;
                } catch(exception e){
                    system.debug('#### exception = '+e);
                }
            }
            

            Location__c objLoc = InitializeSRDataTest.createLocation('123','Building');
            insert objLoc;

            List<Inventory__c> lstInv = new List<Inventory__c>();
            lstInv.add(InitializeSRDataTest.createInventory(objLoc.id));
            lstInv.add(InitializeSRDataTest.createInventory(objLoc.id));
            lstInv[0].Property_Country__c = 'United Arab Emirates';
            lstInv[0].Property_City__c = 'Dubai';
            lstInv[1].Property_Country__c = 'Lebanon';
            lstInv[1].Inventory_ID__c = '345wer';
            insert lstInv;

            List<Booking_Unit__c> lstBU = new List<Booking_Unit__c>();
            lstBU.add(InitializeSRDataTest.createBookingUnit(bk.id,lstInv[0].id));
            lstBU.add(InitializeSRDataTest.createBookingUnit(bk.id,lstInv[0].id));
            lstBU.add(InitializeSRDataTest.createBookingUnit(bk.id,lstInv[1].id));
            lstBU.add(InitializeSRDataTest.createBookingUnit(bk.id,lstInv[1].id));
            insert lstBU;

            List<Constants__c> lstConstants = new List<Constants__c>();
            lstConstants.add(new Constants__c(Associate_Type__c = 'Individual',Country__c = 'United Arab Emirates',City__c = 'Dubai',Doc_Code__c = 'TestDoc',IsBuyerReqDocs__c = true,Filter_Doc__c='Is UAE'));
            lstConstants.add(new Constants__c(Associate_Type__c = 'Individual',Country__c = 'United Arab Emirates',City__c = 'Dubai',Doc_Code__c = 'TestDoc1',IsBuyerReqDocs__c = true));
            lstConstants.add(new Constants__c(Associate_Type__c = 'Minor',Country__c = 'United Arab Emirates',City__c = 'Dubai',Doc_Code__c = 'TestDoc',IsBuyerReqDocs__c = true));
            lstConstants.add(new Constants__c(Associate_Type__c = 'Individual',Country__c = 'Lebanon',Doc_Code__c = 'TestDoc1',IsBuyerReqDocs__c = true,Filter_Doc__c='Is GCC'));
            lstConstants.add(new Constants__c(Associate_Type__c = 'Individual',Country__c = 'Lebanon',Doc_Code__c = 'TestDoc1',IsBuyerReqDocs__c = true));
            lstConstants.add(new Constants__c(Associate_Type__c = 'Minor',Country__c = 'Lebanon',Doc_Code__c = 'TestDoc',IsBuyerReqDocs__c = true));
            insert lstConstants;

            list<Buyer__c> lstbuyers = AP_SRUtility.getRecords('Buyer__c',' where booking__c =\''+bk.id+'\'');
            Map<Id,list<Buyer__c>> mpBookingIDlstBuyers = new Map<Id,list<Buyer__c>>();
            mpBookingIDlstBuyers.put(bk.id,lstbuyers);
            //Map<Id,List<NSIBPM__SR_Doc__c>> buyerDocs = AP_SRUtility.CreateSrdocsforBuyers(sr.id,mpBookingIDlstBuyers);


           AP_SRUtility.callAsyncReceiptWebservice(sr.id);
           AP_SRUtility.validateSRLOISubmit(sr.id);
           //AP_SRUtility.updateStepStatus('MORE_INFO_UPDATED','MORE_INFO_UPDATED',sr.Id);
        //}
        //Test.stopTest();
    }
}