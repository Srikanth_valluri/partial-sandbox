/*-------------------------------------------------------------------------------------------------
Description: Controller Class to set Key Release Date for LHO

    ============================================================================================================================
        Version | Date(DD-MM-YYYY) | Last Modified By | Comments
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 04-11-2018       | Lochan Karle   | Initial draft
    ----------------------------------------------------------------------------------------------------------------------------
   =============================================================================================================================
*/
public without sharing class GenerateKeyReleaseFormLHO {
    
    private Id bookingUnitId;
    public boolean isOktoKeyRelease {get;set;}
    public string strDate {get;set;}
    
    public GenerateKeyReleaseFormLHO(ApexPages.StandardController controller) {
        bookingUnitId = controller.getid();
        isOktoKeyRelease = false;
    }
    
     public PageReference checkEligiblity() {
        if( String.isNotBlank( bookingUnitId ) ) {
            try {
                Booking_Unit__c objUnit = [ SELECT Id
                                                 , Name
                                                 , OK_to_release_Keys_LHO__c
                                                 , Keys_Released_LHO_Date__c
                                                 , Keys_Released_LHO__c
                                              FROM Booking_Unit__c
                                             WHERE Id = :bookingUnitId ];
                if( objUnit != NULL && objUnit.OK_to_release_Keys_LHO__c ) {
                    isOktoKeyRelease = true;
                    DateTime dayToday = system.today();
                    strDate = dayToday.format('dd-MM-yyyy');
                    return null;
                }
                else {
                    ApexPages.addmessage( 
                        new ApexPages.message( 
                            ApexPages.severity.INFO,'Unit is not ready for key release.') );  
                }
            }
            catch( Exception e ) {
                ApexPages.addmessage( 
                    new ApexPages.message( 
                    ApexPages.severity.ERROR, e.getMessage() ) );
            }
        }
        else {
            ApexPages.addmessage( 
                new ApexPages.message( 
                ApexPages.severity.INFO,'Unit Id not found.') );
        }
        return NULL ;
    }
    
    public pageReference saveUnit() {
        Booking_Unit__c updateBU = new Booking_Unit__c(Id = bookingUnitId);
        updateBU.Keys_Released_LHO__c = true;
        if (!string.isBlank(strDate)) {
            updateBU.Keys_Released_LHO_Date__c = generateDateFromString(strDate);
        }
        update updateBU;
        pagereference newpg = 
            new Pagereference(URL.getSalesforceBaseUrl().toExternalForm()+'/'+bookingUnitId);
        newpg.setRedirect(true);
        return newpg;
    }
    
    public pageReference returnToUnit(){
        pagereference newpg = 
            new Pagereference(URL.getSalesforceBaseUrl().toExternalForm()+'/'+bookingUnitId);
        newpg.setRedirect(true);
        system.debug('newpg********'+newpg);
        return newpg;
    }
    
    public date generateDateFromString(String strDate){
        List<String> parts = strDate.split('-');
        system.debug('!!!!parts'+parts);
        return date.newinstance(integer.valueOf(parts[2]), 
                                integer.valueOf(parts[1]), 
                                integer.valueOf(parts[0]));
    }
}