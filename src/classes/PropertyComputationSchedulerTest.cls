/****************************************************************************************************
* Name          : PropertyComputationSchedulerTest                                                  *
* Description   : Test Class for PropertyComputationScheduler                                       *
* Created Date  : 09-08-2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER     AUTHOR            DATE            COMMENTS                                                *
* 1.0     Twinkle Panjabi   12-08-2018      Initial Draft.                                          *
****************************************************************************************************/
@isTest
private class PropertyComputationSchedulerTest
{

    static testmethod void schedulerTest() 
    {
        Property_Computation__c objPropCom = new Property_Computation__c();
        objPropCom.name = 'Test';
        objPropCom.Number_of_Booking_Units__c = 10;
        objPropCom.Number_of_Clicks__c = 5;
        objPropCom.Number_of_Days__c = 10;
        objPropCom.Number_of_Opens__c = 5;
        objPropCom.Number_of_Records_for_Project_Details__c = 10;
        objPropCom.Number_of_Records_for_Sales_Offer__c = 5;
        objPropCom.IsActive__c = True;
        insert objPropCom;

        String CRON_EXP = '0 0 0 15 3 ? *';
        

        
        Test.startTest();

            String jobId = System.schedule('ScheduleApexClassTest',  CRON_EXP, new PropertyComputationScheduler());
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
            System.assertEquals(CRON_EXP, ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);

        Test.stopTest();
    }
}