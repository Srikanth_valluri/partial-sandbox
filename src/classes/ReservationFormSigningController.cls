/************************************************************************************************
 * @Name              : ReservationFormSigningController
 * @Test Class Name   : ReservationFormSigningController_Test // need to create
 * @Description       : Controller for ReservationFormSigningPDF
 * Modification Log
 * 1.0    Srikanth    03/08/2020        Created Class
 ************************************************************************************************/

global without sharing class ReservationFormSigningController{ 
    public Form_Request__c formRequest {get; set;}
    public List <Form_Request_Inventories__c> formInventories { get; set; }
    public Map <ID, String> grossPricesInWords { get; set; }
    public String reservationFeePaidInWords { get; set; }
    public String currentUser {get;set;}
    public String currentUserEmail {get; set;}
    public String userCurrentTime { get; set; }
    public Integer dd { get; set; }
    public Integer mon { get; set; }
    public Integer year { get; set; }
    public User rmDetails { get; set; }
    
    public ReservationFormSigningController(ApexPages.StandardController controller) {
        userCurrentTime = System.Now().format()+':'+System.Now().Second();
        currentUser = UserInfo.getName();
        currentUserEmail = UserInfo.getUserEmail();
        
        dd = System.today().day();
        mon = System.today().month();
        year = System.today().year();
        rmDetails = new User ();
        try {
            formRequest = new Form_request__c ();
            formInventories = new List <Form_Request_Inventories__c> ();
            grossPricesInWords = new Map <ID, String> ();
            reservationFeePaidInWords = '';
            ID formReqId = Apexpages.currentPage().getParameters().get('recId');
            
            formRequest = Database.query ('SELECT Corporate__r.Name, Inquiry__r.First_Name__c, Inquiry__r.Last_Name__c, '
                                          +Damac_ReservationFormHelper.getAllFields('Form_request__c')
                                          +' FROM Form_request__c WHERE Id =: formReqId');
           
            
            if (formRequest.Email__c != null) {
                formRequest.Email__c = formRequest.Email__c.replaceAll('(^[^@]{3}|(?!^)\\G)[^@]', '$1*');
            }
            if (formRequest.Corporate_Email__c != null) {
                formRequest.Corporate_Email__c = formRequest.Corporate_Email__c.replaceAll('(^[^@]{3}|(?!^)\\G)[^@]', '$1*');
            }
            if (formRequest.X2nd_Applicant_Email__c != null) {
                formRequest.X2nd_Applicant_Email__c = formRequest.X2nd_Applicant_Email__c.replaceAll('(^[^@]{3}|(?!^)\\G)[^@]', '$1*');
            }
            
            if (formRequest.Permanent_Residence_Phone__c != null) {
                formRequest.Permanent_Residence_Phone__c = formRequest.Permanent_Residence_Phone__c.replaceAll('\\d(?=\\d{3})', '*');
                if (formRequest.Permanent_residence_country_code__c != null) {
                    formRequest.Permanent_Residence_Phone__c = formRequest.Permanent_residence_country_code__c+' '+formRequest.Permanent_Residence_Phone__c;
                }
            }
            
            if (formRequest.Mobile_Number__c != null) {
                formRequest.Mobile_Number__c = formRequest.Mobile_Number__c.replaceAll('\\d(?=\\d{3})', '*');
                
                if (formRequest.Mobile_Country_Code__c != null) {
                    formRequest.Mobile_number__c = formRequest.Mobile_Country_Code__c+' '+formRequest.Mobile_Number__c;
                }
                
            }
            if (formRequest.Telephone_Number__c != null) {
                formRequest.Telephone_Number__c = formRequest.Telephone_Number__c.replaceAll('\\d(?=\\d{3})', '*');
                
                if (formRequest.Telephone_Country_code__c!= null) {
                    formRequest.Telephone_Number__c = formRequest.Telephone_Country_code__c+' '+formRequest.Telephone_Number__c ;
                }
            }
            
            for (Form_Request_Inventories__c inv : [SELECT Upgrade_Type__c, Inventory__r.Project_Category__c, Inventory__r.Property_Type__c,
                                                    Inventory__r.Property_City__c, Inventory__r.Unit__c, Inventory__r.Is_mixed_use__c,
                                                    Inventory__r.Area_Sqft_2__c, Inventory__r.IPMS_Bedrooms__c,
                                                    Gross_price__c, Inventory__c, Permitted_usage__c, inventory__r.Currency_of_Sale__c,
                                                    (Select id, Name, Option__c, Costing__c from Inventory_Addons__r where Option__c != 'Garden Space' and Option__c != 'Without Furniture')
                                                    FROM Form_Request_Inventories__c WHERE Form_Request__c =: formRequest.Id
                                                    Order By Upgrade_Type__c DESC]) 
            {
                grossPricesInWords.put (inv.Id, NumberToWord.english_number(Integer.valueOf(inv.Gross_Price__c)));
                formInventories.add (inv);
            }
            if (formRequest.Reservation_Fee_Paid__c != null) {
                reservationFeePaidInWords = NumberToWord.english_number(Integer.valueOf(formRequest.Reservation_Fee_Paid__c));
            }
            
            rmDetails = [SELECT Name, managerId, manager.name, Manager.managerId, Manager.Manager.Name, 
                                        Manager.Manager.ManagerId, Manager.Manager.Manager.name
                         FROM user WHERE ID =:formRequest.OwnerId
                                        AND IsActive = TRUE
                                        LIMIT 1];
        } catch (Exception e) {
            system.debug('Exception: ' + e.getMessage ());
        }
    }
    
    
    webservice static String GeneratePDF (ID formReqId) {
        try {
            NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
            
            Form_request__c req = new Form_request__c ();
            req = [SELECT Name FROM Form_request__c WHERE Id =: formReqId];
            
            PageReference ref = Page.ReservationFormDDP;
            ref.getParameters().put('recId', formReqId);
            Blob b = null;
            if (!Test.isRunningTest ())
                b = ref.getContentAsPDF();           
            else {
                b = Blob.valueOf ('test');
            }
            Unit_Documents__c doc = new Unit_Documents__c ();
            doc.Document_Name__c = 'Promotion Details';
            doc.Document_type__c = 'Booking Docs';
            doc.Document_Description__c = 'Promotion Details.pdf';
            try {
                sr = [SELECT ID FROM NSIBPM__Service_Request__c 
                            WHERE (Form_Request__c =: formReqId OR Reservation_Form_Attachment_Name__c =: req.name) 
                            Order By CreatedDate DESC LIMIT 1];
                doc.Service_Request__c = sr.Id;
            } catch (Exception e) {
                System.debug (e.getMessage());
            }
            insert doc;
            
            Attachment att = new Attachment ();
            att.Body = b;
            att.name = 'Promotion Details.pdf';
            att.ParentId = doc.Id;
            insert att;
            
            doc.Form_request__c = formReqId;
            doc.CV_id__c = null;
            doc.Sys_Doc_ID__c = att.Id;
            doc.Status__c = 'Uploaded';
            update doc;
            
            return 'Promotion details generated successfully.';
        } catch (Exception e) {
            return e.getMessage ();
        }
    }
}