@isTest
public class HelpPortalControllerTest{

    @testSetup
    static void setup() {
        Customer_Portal_Help__kav article = new Customer_Portal_Help__kav(Title = 'Name Nationality Change',
                                                                          UrlName = 'NameNationality-Change',
                                                                          ValidationStatus = 'Validated',
                                                                          ArticleBody__c = '<span style="font-size: 20px;"><span style="color: #FF0000;"><b>Passport Detail Update_Upload Documents</b></span></span>   ',                                                             
                                                                          Section_Name__c = 'Name Nationality Change');
        insert article;
        article = [SELECT KnowledgeArticleId FROM Customer_Portal_Help__kav WHERE Id = :article.Id];
        KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);

    }
    
    static testMethod void TestBulk(){
        //HelpPortalController controller = new HelpPortalController();
        Test.starttest();
        HelpPortalController.getInit('Name Nationality Change');
        Test.stopTest();
        list<Customer_Portal_Help__kav> listCustom1=[
            Select ArticleBody__c
                 , Section_Name__c 
              from Customer_Portal_Help__kav
             where Section_Name__c = 'Name Nationality Change'
             LIMIT 1
        ];
        
        system.assertEquals(1,listCustom1.size());
    }
}