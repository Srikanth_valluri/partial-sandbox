@istest
public class CreateStepOnDPOverDueTest{
    
    /*Commented by CloudzLab
* static testmethod void testCreateStep(){
/*
NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
insert sr;

NSIBPM__Service_Request__c sr1 = new NSIBPM__Service_Request__c();
insert sr1;

NSIBPM__Step_Template__c stpTemp  = new NSIBPM__Step_Template__c();
stpTemp.NSIBPM__Code__c = 'DEAL_AUTO_REJECTION';
stpTemp.NSIBPM__Step_RecordType_API_Name__c = 'DEAL_AUTO_REJECTION';
insert stpTemp;

NSIBPM__Step__c dealrej = new NSIBPM__Step__c();
dealRej.NSIBPM__Step_Template__c = stpTemp.id;
insert dealrej;

NSIBPM__Status__c stat = new NSIBPM__Status__c();
stat.NSIBPM__Code__c = 'DEAL_REJECTION';
insert stat;

NSIBPM__SR_Template__c srtemp = new NSIBPM__SR_Template__c();
srtemp.Name = 'Deal';
srtemp.NSIBPM__SR_RecordType_API_Name__c ='Deal_Rejection';
insert srtemp;

NSIBPM__SR_Steps__c srstep = new NSIBPM__SR_Steps__c();
srstep.NSIBPM__Step_Template__c= stpTemp.id;
srstep.NSIBPM__Start_Status__c=stat.id;
srstep.ownerid = userinfo.getuserid();
srstep.NSIBPM__SR_Template__c = srtemp.id;
srstep.NSIBPM__Step_No__c = 40;
insert srstep;
*
List<NSIBPM__SR_Template__c> SRTemplateList =  InitialiseTestData.createTestTemplateRecords(new List<NSIBPM__SR_Template__c>{new NSIBPM__SR_Template__c()});
Id RecordTypeIdAGENT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
Account a = new Account();
a.recordtypeid=RecordTypeIdAGENT;
a.Name = 'Test Account';
a.Agency_Short_Name__c = 'testShrName';
insert a;

Account a2 = new Account();
a2.recordtypeid=RecordTypeIdAGENT;
a2.Name = 'Test Account';
a2.Agency_Short_Name__c = 'testShrName';
insert a2;

Id RecordTypeIdContact = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();

NSIBPM__Service_Request__c sr = InitializeSRDataTest.getSerReq('Agent Registration',true,null);
sr.recordtypeid=RecordTypeIdContact;
sr.NSIBPM__SR_Template__c = SRTemplateList[0].Id;
sr.Agency__c = a.id;
sr.ID_Type__c = 'Passport';
insert sr;
system.debug('-->'+sr.id);

NSIBPM__Service_Request__c sr1 = InitializeSRDataTest.getSerReq('Agent Registration',true,null);
sr1.recordtypeid=RecordTypeIdContact;
sr1.NSIBPM__SR_Template__c = SRTemplateList[0].Id;
sr1.Agency__c = a2.id;
sr1.ID_Type__c = 'Passport';
insert sr1;

NSIBPM__Step__c stp = InitializeSRDataTest.createStep(sr.id,null,null);
insert stp;
system.debug('-->'+stp.id);

booking__c b1 = new booking__c();
b1.Deal_SR__c = sr1.id;
insert b1;

Booking_Unit__c bu1 = new Booking_Unit__c();
bu1.booking__c = b1.id;        
insert bu1;

booking__c b = new booking__c();
b.Deal_SR__c = sr.id;
insert b;

Booking_Unit__c bu = new Booking_Unit__c();
bu.booking__c = b.id;        
insert bu;


List<Id> srIds=new list<id>();
srIds.add(sr.id);
CreateStepOnDPOverDue.createStep(srIds);
}
*/
    /*
static testmethod void CreateStepOnDPOverDue_Methods(){

NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
insert sr;

NSIBPM__Service_Request__c sr1 = new NSIBPM__Service_Request__c();
insert sr1;

NSIBPM__Step_Template__c stpTemp  = new NSIBPM__Step_Template__c();
stpTemp.NSIBPM__Code__c = 'DEAL_REJECTION';
stpTemp.NSIBPM__Step_RecordType_API_Name__c = 'Deal_Rejection';
insert stpTemp;

NSIBPM__Step__c dealrej = new NSIBPM__Step__c();
dealRej.NSIBPM__Step_Template__c = stpTemp.id;
insert dealrej;

NSIBPM__Status__c stat = new NSIBPM__Status__c();
stat.NSIBPM__Code__c = 'DEAL_REJECTION';
insert stat;

NSIBPM__SR_Template__c srtemp = new NSIBPM__SR_Template__c();
srtemp.Name = 'Deal';
srtemp.NSIBPM__SR_RecordType_API_Name__c ='Deal_Rejection';
insert srtemp;

NSIBPM__SR_Steps__c srstep = new NSIBPM__SR_Steps__c();
srstep.NSIBPM__Step_Template__c= stpTemp.id;
srstep.NSIBPM__Start_Status__c=stat.id;
srstep.ownerid = userinfo.getuserid();
srstep.NSIBPM__SR_Template__c = srtemp.id;
srstep.NSIBPM__Step_No__c = 40;
insert srstep;

booking__c b1 = new booking__c();
b1.Deal_SR__c = sr1.id;
insert b1;

Booking_Unit__c bu1 = new Booking_Unit__c();
bu1.booking__c = b1.id;        
insert bu1;

booking__c b = new booking__c();
b.Deal_SR__c = sr.id;
insert b;

Booking_Unit__c bu = new Booking_Unit__c();
bu.booking__c = b.id;        
insert bu;


List<Id> srIds=new list<id>();
srIds.add(sr1.id);
CreateStepOnDPOverDue.createStepFromButton((srIds)[0]);
CreateStepOnDPOverDue.reinstatementDeal((srIds)[0]);
}

// Re Instatement
static testmethod void reinstatement_Methods(){

NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
insert sr;

NSIBPM__Service_Request__c sr1 = new NSIBPM__Service_Request__c();
insert sr1;

NSIBPM__Step_Template__c stpTemp  = new NSIBPM__Step_Template__c();
stpTemp.NSIBPM__Code__c = 'DEAL_REINSTATEMENT';
stpTemp.NSIBPM__Step_RecordType_API_Name__c = 'DEAL_REINSTATEMENT';
insert stpTemp;

NSIBPM__Step__c dealrej = new NSIBPM__Step__c();
dealRej.NSIBPM__Step_Template__c = stpTemp.id;
insert dealrej;

NSIBPM__Status__c stat = new NSIBPM__Status__c();
stat.NSIBPM__Code__c = 'DEAL_REINSTATEMENT';
insert stat;



NSIBPM__SR_Template__c srtemp = new NSIBPM__SR_Template__c();
srtemp.Name = 'Deal';
srtemp.NSIBPM__SR_RecordType_API_Name__c ='DEAL_REINSTATEMENT';
insert srtemp;

NSIBPM__SR_Steps__c srstep = new NSIBPM__SR_Steps__c();
srstep.NSIBPM__Step_Template__c= stpTemp.id;
srstep.NSIBPM__Start_Status__c=stat.id;
srstep.ownerid = userinfo.getuserid();
srstep.NSIBPM__SR_Template__c = srtemp.id;
srstep.NSIBPM__Step_No__c = 30;
insert srstep;

booking__c b1 = new booking__c();
b1.Deal_SR__c = sr1.id;
insert b1;

Booking_Unit__c bu1 = new Booking_Unit__c();
bu1.booking__c = b1.id;        
insert bu1;

booking__c b = new booking__c();
b.Deal_SR__c = sr.id;
insert b;

Booking_Unit__c bu = new Booking_Unit__c();
bu.booking__c = b.id;        
insert bu;


List<Id> srIds=new list<id>();
srIds.add(sr.id);
CreateStepOnDPOverDue.createStep(srIds);
srIds.clear();
srIds.add(sr1.id);        
CreateStepOnDPOverDue.reinstatementDeal((srIds)[0]);
}

// Re Instatement
static testmethod void reinstatement_Methods1(){

NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
insert sr;

NSIBPM__Service_Request__c sr1 = new NSIBPM__Service_Request__c();
insert sr1;

NSIBPM__Step_Template__c stpTemp  = new NSIBPM__Step_Template__c();
stpTemp.NSIBPM__Code__c = 'DEAL_REINSTATEMENT';
stpTemp.NSIBPM__Step_RecordType_API_Name__c = 'DEAL_REINSTATEMENT';
insert stpTemp;

NSIBPM__Step__c dealrej = new NSIBPM__Step__c();
dealRej.NSIBPM__Step_Template__c = stpTemp.id;
insert dealrej;

NSIBPM__Status__c stat = new NSIBPM__Status__c();
stat.NSIBPM__Code__c = 'DEAL_REINSTATEMENT';
insert stat;



NSIBPM__SR_Template__c srtemp = new NSIBPM__SR_Template__c();
srtemp.Name = 'Deal';
srtemp.NSIBPM__SR_RecordType_API_Name__c ='DEAL_REINSTATEMENT';
insert srtemp;

NSIBPM__SR_Steps__c srstep = new NSIBPM__SR_Steps__c();
srstep.NSIBPM__Step_Template__c= stpTemp.id;
srstep.NSIBPM__Start_Status__c=stat.id;
srstep.ownerid = userinfo.getuserid();
srstep.NSIBPM__SR_Template__c = srtemp.id;
srstep.NSIBPM__Step_No__c = 30;
insert srstep;

booking__c b1 = new booking__c();
b1.Deal_SR__c = sr1.id;
insert b1;

Booking_Unit__c bu1 = new Booking_Unit__c();
bu1.booking__c = b1.id;        
insert bu1;

booking__c b = new booking__c();
b.Deal_SR__c = sr.id;
insert b;

Booking_Unit__c bu = new Booking_Unit__c();
bu.booking__c = b.id;        
insert bu;


CreateStepOnDPOverDue.preChecksForReinstatement(sr1.id);

}

// Re Instatement
static testmethod void reinstatement_Methods2(){

NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
insert sr;

NSIBPM__Service_Request__c sr1 = new NSIBPM__Service_Request__c();
insert sr1;

NSIBPM__Step_Template__c stpTemp  = new NSIBPM__Step_Template__c();
stpTemp.NSIBPM__Code__c = 'UPLOAD_POP';
stpTemp.NSIBPM__Step_RecordType_API_Name__c = 'DEAL_REINSTATEMENT';
insert stpTemp;

NSIBPM__Step__c dealrej = new NSIBPM__Step__c();
dealRej.NSIBPM__Step_Template__c = stpTemp.id;
insert dealrej;

NSIBPM__Status__c stat = new NSIBPM__Status__c();
stat.NSIBPM__Code__c = 'DEAL_REINSTATEMENT';
insert stat;



NSIBPM__SR_Template__c srtemp = new NSIBPM__SR_Template__c();
srtemp.Name = 'Deal';
srtemp.NSIBPM__SR_RecordType_API_Name__c ='DEAL_REINSTATEMENT';
insert srtemp;

NSIBPM__SR_Steps__c srstep = new NSIBPM__SR_Steps__c();
srstep.NSIBPM__Step_Template__c= stpTemp.id;
srstep.NSIBPM__Start_Status__c=stat.id;
srstep.ownerid = userinfo.getuserid();
srstep.NSIBPM__SR_Template__c = srtemp.id;
srstep.NSIBPM__Step_No__c = 30;
insert srstep;

booking__c b1 = new booking__c();
b1.Deal_SR__c = sr1.id;
insert b1;

Booking_Unit__c bu1 = new Booking_Unit__c();
bu1.booking__c = b1.id;        
insert bu1;

booking__c b = new booking__c();
b.Deal_SR__c = sr.id;
insert b;

Booking_Unit__c bu = new Booking_Unit__c();
bu.booking__c = b.id;        
insert bu;


CreateStepOnDPOverDue.reinstatementDeal(sr1.id);

}
*/
    
    //created by Mariam CloudzLab
    
    @testSetup 
    static void setupData() {
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        Id DealRT = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = acc.Id;
        sr.RecordTypeId = DealRT;
        sr.Agency__c = acc.id;
        sr.Agency_Type__c = 'Corporate';
        insert sr;
        
        List<New_Step__c> newStepsList = new List<New_Step__c>();
        New_Step__c nstp1 = new New_Step__c();
        nstp1.Service_Request__c = sr.id;
        nstp1.Step_No__c = 2;
        nstp1.Step_Status__c = 'Awaiting Token Deposit';
        nstp1.Step_Type__c = 'Token Payment';
        newStepsList.add(nstp1);
        
        New_Step__c nstp2 = new New_Step__c();
        nstp2.Service_Request__c = sr.id;
        nstp2.Step_No__c = 3;
        nstp2.Step_Status__c = 'Awaiting PC Confirmation';
        nstp2.Step_Type__c = 'PC Confirmation';
        newStepsList.add(nstp2);
        insert newStepsList;
        
        booking__c b = new booking__c();
        b.Deal_SR__c = sr.id;
        insert b;
        
        Booking_Unit__c bu = new Booking_Unit__c();
        bu.booking__c = b.id;        
        insert bu;
    }
    
    @isTest
    static void testCreateStepOnDPOverDue(){
        Test.startTest();
        
        NSIBPM__Service_Request__c sr = [Select Id From NSIBPM__Service_Request__c Limit 1];
        List<Id> srIds = new list<id>();
        srIds.add(sr.id);
        CreateStepOnDPOverDue.createStepFromButton(sr.Id);
        CreateStepOnDPOverDue.createStep(srIds);
        CreateStepOnDPOverDue.createStepFromButton(sr.Id);
        CreateStepOnDPOverDue.reinstatementDeal(sr.Id);
        CreateStepOnDPOverDue.reinstatementDeal(sr.Id);
        CreateStepOnDPOverDue.preChecksForReinstatement(sr.Id);
        Test.stopTest();
    }
    
}