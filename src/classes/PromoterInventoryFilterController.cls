/*
Name:PromoterInventoryFilterController
Description: Controller to Provide the released Units of a User and Un assigned Inventory
Author: DAMAC IT Team
*/

public class PromoterInventoryFilterController{
               
        public transient list<string> idsselected{get;set;} 
        
        public transient list<string> propStatusToFilter{get;set;}  
        
        public transient list<string> citiesToFilter{get;set;}  
        
        public transient list<string> unitTypeToFilter{get;set;}  
        
        public transient list<string> propertyNameFilter{get;set;}
        
        public transient list<string> acdFilter{get;set;} 
        
        public transient list<string> bedRoomFilter{get;set;}  
                         
        public list<Marketing_Documents__c> marketingNames {get;set;}
         
        public transient list<inventory__c> filteredinventory {get;set;} 
        
        public transient set<string> marketingNamesFiltered{get;set;}
        
        public transient list<string> packageNamesFilter{get;set;}
        
        public transient integer minsft {get;set;}
        
        public transient integer maxsft {get;set;}
        
        public transient integer minprice {get;set;}
        
        public transient integer maxprice {get;set;}
        
        //public transient string floorPackagetype {get;set;}
        
        public transient map<string,inventory__c> headerMap{get;set;}        
        
        @testvisible private list<Inventory_User__c> otherUserinventory;
        
        @testvisible private set<id> otherInventoryIds;
        
        @testvisible private string status='Released';
        
        public string invs{get;set;}
        
        public boolean ismobile{get;set;}
        
        // PDF
        public Map<string, set<String>> mpProperties{get; set;}
        
        public Map<string, List<Inventory__c>> mpAvailable{get; set;}
        
        public Map<string, Map<string, List<Inventory__c>>> mpFinal{get; set;}
        
        public Map<string, Location__c> mpBuildings {get; set;}
        
        public string widthPx{get; set;}
        
        public string ptWidthPx{get; set;}
        
        public Map<String, List<Payment_Plan__c>> mpPaymentPlans{get; set;}
        
        public Map<Id, List<Payment_Terms__c>> mpPaymentTerms{get; set;}
        
        public string planKeyStr{get; set;}
        
        public string pTermKeyStr{get; set;}
        
        //public String propId{set;get;}
        
        public boolean render{get;set;}
        
        public string renderaspdf{get;set;}
        
        //public string invid {get;set;}
                
        public PromoterInventoryFilterController(){
            pageload();
        }
        
        public void isS1(){
            if(UserInfo.getUiTheme() == 'Theme4t' ){
                ismobile = true;
            }else if(UserInfo.getUiTheme() != 'Theme4t' ){
                ismobile = false;
            }
        }
        /****************************************************************************************
        Method:         filterData
        Description:    filter avaliablity as per user applied filters
        **************************************************************************************************/
        
        public void filterData() {
            
            isS1();                                 
            string queryString = 'select id,Area__c ,Floor_Package_Name__c ,Plot_Area__c ,Building_Name__c,Building_ID__c,Location_Code__c ,IPMS_Bedrooms__c ,Bedrooms__c ,Area_sft__c ,Property_Country__c ,Plot_Area_sft__c ,Status__c ,List_Price_calc__c ,Floor_Plan__c,Plot_Plan__c,Special_Price_calc__c ,Price_Per_Sqft__c ,Unit_Plan__c,name,Marketing_name__c,Building_Location__r.Name,Anticipated_Completion_Date__c,property_name__c,Area_Sqft_2__c,';
                   queryString += 'Property_Status__c,Location_Type__c,Special_Price__c,Property_city__c,Unit_Type__c,Unit_Location__r.Name,Bedroom_Type__c ';
                   queryString += 'from inventory__c ';
            if(idsselected !=null || propStatusToFilter !=null || unitTypeToFilter !=null || citiesToFilter !=null || propertyNameFilter !=null){
                queryString += 'where ';
                if(idsselected != null && idsselected.size() > 0 ){
                    queryString += 'Marketing_Name_Doc__r.id IN:idsselected AND';
                }
                if(propStatusToFilter != null && propStatusToFilter.size() >0){
                    queryString += ' Property_Status__c IN:propStatusToFilter AND';
                }
                if(unitTypeToFilter != null && unitTypeToFilter.size() >0){
                    queryString += ' unit_type__c IN:unitTypeToFilter AND';
                }
                if(citiesToFilter != null && citiesToFilter.size() >0){
                    queryString += ' Property_city__c IN:citiesToFilter AND';
                }
                if(propertyNameFilter != null && propertyNameFilter.size()>0){
                    queryString += ' property_name__c IN:propertyNameFilter AND';
                }
                if(acdFilter!= null && acdFilter.size()>0){
                system.debug(acdFIlter+'ACD');                    
                    queryString += ' Anticipated_Completion_Date__c IN:acdFilter AND';
                }
                if(bedRoomFilter!= null && bedRoomFilter.size()>0){
                     queryString += ' Bedroom_Type__c IN:bedRoomFilter AND';
                }
                // SFT Filters
                if(minsft != null && minsft >0){
                    queryString += ' Area_sft__c >=:minsft AND';
                }                
                if(maxsft != null && maxsft >0){
                    queryString += ' Area_sft__c <=:maxsft AND';
                }
                //Price filters
                if(minprice != null && minprice >0){
                    queryString += ' Special_Price__c>=:minprice AND';
                }
                if(maxprice != null && maxprice >0){
                    queryString += ' Special_Price__c<=:maxprice AND';
                }
                //FloorPackage
                /*
                if(floorPackagetype !=null){
                    SYSTEM.DEBUG(floorPackagetype+'floorPackagetype>>>>>>>>>>>');
                    queryString += ' Floor_Package_Type__c=:floorPackagetype AND';
                }
                */
                if(packageNamesFilter != null && packageNamesFilter.size()>0){
                    queryString += ' Floor_Package_Name__c IN:packageNamesFilter AND';
                }
                
            }   
            queryString += ' ID NOT IN:otherInventoryIds AND status__c=:status ORDER BY Special_Price__c  LIMIT 10000';
            system.debug(idsselected +'-'+propStatusToFilter +'-'+unitTypeToFilter+'-'+ citiesToFilter+'-'+ propertyNameFilter);    
            system.debug(minsft+'-'+maxsft+'-'+acdFilter+'-'+idsselected.size() +'-'+propStatusToFilter.size() +'-'+unitTypeToFilter.size()+'-'+ citiesToFilter.size()+'-'+ propertyNameFilter.size());    
            //queryString = queryString.removeEnd('AND');            
            SYSTEM.DEBUG(queryString+'>>>>>>>>>>>>'); 
            filteredinventory = database.query(querystring);
            marketingNamesFiltered = new set<string>();
            headerMap = new map<string,inventory__c>();
            for(inventory__c i:filteredinventory){
                if(i.Building_Name__c!=null){
                    headerMap.put(i.Building_Name__c,i);  
                }
            }            
            //return null;
        }
        
        
        set<string> cities = new set<string>();
        set<string> propStatuses = new set<string>();
        set<string> unitTypes = new set<string>();
        set<string> propertyNames = new set<string>();
        set<string> acd = new set<string>();
        set<string> bedrooms= new set<string>();
        set<string> pkgNames = new set<string>();
        
        
        /****************************************************************************************
        Method:         pageload
        Description:    On Page load show avaliable inventory 
        **************************************************************************************************/
         
        public void pageload(){
            isS1();
            maxsft = 0;
            minsft = 0;
            maxprice = 0;
            minprice = 0;
            //floorPackagetype = '-None-';
            mpProperties = new Map<String,set<String>>();
            mpAvailable = new Map<String, list<Inventory__c>>();
            mpFinal = new Map<string, Map<string, List<Inventory__c>>>();
            mpBuildings = new Map<String,Location__c>();
            mpPaymentTerms = new Map<Id, List<Payment_Terms__c>>();
            mpPaymentPlans = new Map<String, List<Payment_Plan__c>>();
            planKeyStr = '';
            render = true;
            renderaspdf = '';
            otherUserinventory = new list<Inventory_User__c>();
            list<Inventory_User__c> thisUserInventory = new list<Inventory_User__c>();
            thisUserInventory = [select id,inventory__c from inventory_user__c where user__c=:userinfo.getuserid() and inventory__r.status__c='Released'];
            otherUserinventory = [select id,inventory__c from inventory_user__c where User__c!=:userinfo.getuserid() and inventory__r.status__c='Released' ];
            
            set<id> thisPcinvIds = new set<id>();
            if(thisUserInventory.size() >0){
                for(inventory_user__c thisPcInv:thisUserInventory){
                    thisPcinvIds.add(thisPcInv.inventory__c);
                }
            }
            
            
            otherInventoryIds = new set<id>();
            for(inventory_user__c invUs:otherUserinventory){
                if(thisPcinvIds.size()>0){
                    if(!thisPcinvIds.contains(invUs.inventory__c)){
                        otherInventoryIds.add(invUs.inventory__c);
                    }
                }else{
                    otherInventoryIds.add(invUs.inventory__c);
                }
            }
            
            idsselected = new list<string>();
            propStatusToFilter  = new list<string>();
            unitTypeToFilter = new list<string>();
            citiesToFilter  = new list<string>();
            propertyNameFilter  = new list<string>();
            acdFilter = new list<string>();
            filteredinventory = new list<inventory__c>();
            marketingNames = new list<Marketing_Documents__c>();
            bedRoomFilter =  new list<string>();
            packageNamesFilter = new list<String>();
           
             string queryString = '';  
             //string status='Released';           
             queryString =   'select id,Area__c ,Floor_Package_Name__c ,Plot_Area__c ,Building_Name__c,Building_ID__c,Location_Code__c ,IPMS_Bedrooms__c ,Bedrooms__c ,Property_Country__c ,Area_sft__c ,Plot_Area_sft__c ,Status__c ,Floor_Plan__c,List_Price_calc__c ,Plot_Plan__c,Unit_Plan__c,Price_Per_Sqft__c ,Special_Price_calc__c ,name,Marketing_name__c,Building_Location__r.Name,Anticipated_Completion_Date__c,property_name__c,Area_Sqft_2__c,';
             queryString +=  'Property_Status__c,Location_Type__c,Special_Price__c,Property_city__c,Unit_Type__c,Unit_Location__r.Name,Bedroom_Type__c'; 
             queryString +=   ' from inventory__c where status__c=:status AND ID NOT IN:otherInventoryIds and lastmodifieddate<LAST_N_DAYS:2 LIMIT 100';
             
             filteredinventory =  database.query(queryString); 
             list<inventory__c> invS = new list<inventory__c>();
             invS  = [select id,Floor_Package_Name__c ,property_city__c,Bedroom_Type__c,Property_Status__c,Unit_Type__c,property_name__c,Anticipated_Completion_Date__c from inventory__c where status__c='Released' ORDER BY ACD_Date__c asc  LIMIT 10000 ];
            if(invS.size() > 0){                   
                for(inventory__c inv:invs){
                    if(inv.property_city__c != null){
                        cities.add(inv.property_city__c.toUpperCase());
                    }
                    if(inv.Property_Status__c != null){
                        propStatuses.add(inv.Property_Status__c.toUpperCase());
                    }
                    if(inv.Unit_Type__c != null){
                        unitTypes.add(inv.Unit_Type__c.toUpperCase());
                    }
                    if(inv.property_name__c != null){
                        propertyNames.add(inv.property_name__c.toUpperCase());
                    }
                    if(inv.Anticipated_Completion_Date__c !=null){
                        acd.add(inv.Anticipated_Completion_Date__c.toUpperCase());
                    }
                    if(inv.Bedroom_Type__c!=null){
                        bedrooms.add(inv.Bedroom_Type__c );
                    }  
                    if(inv.Floor_Package_Name__c  != null){
                        pkgNames.add(inv.Floor_Package_Name__c);
                    }                             
                }
            }
        }
    
    public Double offset{
        get{
            TimeZone timeZ = UserInfo.getTimeZone();
            return timeZ.getOffset(Datetime.now())/(1000*3600*24.0);
        }
    }
    
        
    /************************************ PDF CODE ****************************************************
    Method:         prepareData
    Description:    retrieve inventory information to display in pdf 
    **************************************************************************************************/
    public void prepareData(){
        filterData();
        system.debug(filteredinventory);
        List<Schema.FieldsetMember> lstMembers = SObjectType.Inventory__c.FieldSets.Project_Unit.getFields();
        integer width= Math.round(100/(lstMembers.size()-1));
        widthpx = width+'%';
        
        lstMembers = SObjectType.Payment_Terms__c.FieldSets.PaymentTerm.getFields();
        width= Math.round(100/lstMembers.size());
        ptWidthPx = width+'%';
        
        
        for(Inventory__c inv: filteredinventory){
             if(!mpAvailable.containsKey(inv.Building_ID__c)){
                mpAvailable.put(inv.Building_ID__c, new List<Inventory__c>{inv});
             }
             else{
                List<Inventory__c> existing = mpAvailable.get(inv.Building_ID__c);
                existing.add(inv);
                mpAvailable.put(inv.Building_ID__c,existing);
             }
        }
        
        for(Location__c loc: [Select id, location_Id__c, Construction_status__c, status__c, Building_name__c from Location__c where Location_Id__c in: mpAvailable.keyset()]){
            mpBuildings.put(loc.location_Id__c, loc);
        }
        
        set<string> buildingIds = new set<string>();
        set<Id> PaymentPlanIds = new set<Id>();
        date currDate = system.today();
        for(Payment_Plan__c pp: [Select id,Building_ID__c from Payment_Plan__c where Building_ID__c in: mpBuildings.keyset() and Effective_From__c<=:currDate and Effective_To_calculated__c>=:currDate]){
            PaymentPlanIds.add(pp.Id);
            buildingIds.add(pp.Building_ID__c);
            if(!mpPaymentPlans.containsKey(pp.Building_ID__c))
                mpPaymentPlans.put(pp.Building_ID__c, new List<Payment_Plan__c>{pp});
            else{
                List<Payment_Plan__c> existing = mpPaymentPlans.get(pp.Building_ID__c);
                existing.add(pp);
                mpPaymentPlans.put(pp.Building_ID__c, existing); 
            }
        }
        
        for(string bId : buildingIds){
            planKeyStr+=bId+';';
        }
        
        set<String> paymentIds = new set<String>();
        string ppQuery = DamacUtility.getCreatableFieldsSOQL('Payment_Terms__c');
        ppQuery+=' where Payment_Plan__c in: PaymentPlanIds order by line_ID__c';
        for(Payment_Terms__c pt: database.query(ppQuery)){
            paymentIds.add(pt.Payment_Plan__c);
            if(!mpPaymentTerms.containsKey(pt.Payment_Plan__c)){
                mpPaymentTerms.put(pt.Payment_Plan__c, new list<Payment_Terms__c>{pt});
            }
            else{
                List<Payment_Terms__c> existing = mpPaymentTerms.get(pt.Payment_Plan__c);
                existing.add(pt);
                mpPaymentTerms.put(pt.Payment_Plan__c, existing);
            }
            
        }
        system.debug(mpProperties);
        system.debug(mpAvailable);
        system.debug(mpFinal);
        system.debug(mpBuildings);
        system.debug(mpPaymentTerms);
        system.debug(mpPaymentPlans);

        for(string pId : paymentIds){
            pTermKeyStr+=pId+';';
        }
        render = false;
        renderaspdf = 'pdf';
        Apexpages.currentPage().getHeaders().put('content-disposition', 'attachment; filename=Availability for'+userinfo.getusername()+'.pdf' );
    }
    
    
    
/* ****************************************************************************** POPULATE FILTERS *********************************************************************************************/        
        // MARKETING NAME    
        public List<SelectOption> getItems() {
            marketingNames = [select id,name from Marketing_Documents__c];
            List<SelectOption> options = new List<SelectOption>();
            for(Marketing_Documents__c a:marketingNames){
                options.add(new SelectOption(a.id,a.Name));            
            }
            return options;
        }
        
        //PROPERTY STATUS 
        public List<SelectOption> getPropStatus() {
            
            List<SelectOption> options = new List<SelectOption>();
            for(string s:propStatuses){
                options.add(new SelectOption(s,s));                
            }           
            
            return options;
        }

        // PROPERTY CITY
        public List<SelectOption> getCities() {
            
            List<SelectOption> options = new List<SelectOption>();
            for(string c:cities){
                options.add(new SelectOption(c,c));                
            }
            return options;
        }
        
        // UNIT TYPES
        public List<SelectOption> getUnitTypes() {
            
            List<SelectOption> options = new List<SelectOption>();
            for(string u:unitTypes){
                options.add(new SelectOption(u,u));
            }
            return options;
        }
        
        //PROPERTY NAMES
        public List<SelectOption> getPropertyNames() {
            
            List<SelectOption> options = new List<SelectOption>();
            for(string u:propertyNames){
                options.add(new SelectOption(u,u));
            }
            return options;
        }
        
        //ACD
        public List<SelectOption> getAcd() {
            
            List<SelectOption> options = new List<SelectOption>();
            for(string u:acd){
                options.add(new SelectOption(u,u));
            }
            return options;
        }
        
        // Bed rooms
        public List<SelectOption> getBedRooms() {
            
            List<SelectOption> options = new List<SelectOption>();
            for(string u:bedrooms){
                options.add(new SelectOption(u,u));
            }
            return options;
        }
        
        // Package Names
        public List<SelectOption> getPkgNames() {
            
            List<SelectOption> options = new List<SelectOption>();
            for(string u:pkgNames){
                options.add(new SelectOption(u,u));
            }
            return options;
        }  
        
       @RemoteAction
        public static List<inventory__c> searchinventory() {
            String userId = UserInfo.getUserId();
            List<inventory__c> lstInquiry = [Select Id,Marketing_Name__c,Property_Name__c,Unit_Type__c,Bedroom_Type__c,Area__c,Plot_Area__c,Special_Price_2__c,Property_Status__c,ACD_Date__c From inventory__c Where OwnerId =: userId];
            return lstInquiry;
        }
             
    }