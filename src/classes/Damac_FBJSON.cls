public class Damac_FBJSON{
    public cls_Keys keys;
    public class cls_Keys {
        public cls_match_keys match_keys;
        public String event_name;   //Other
        public Long event_time;  //1456870902
        public cls_custom_data custom_data;
    }
    public class cls_match_keys {
        public cls_phone[] phone;
        public cls_email[] email;
        public String lead_id;  //NOT HASHED 
    }
    public class cls_phone {
        public String phone_key;
    }
    public class cls_email {
        public String email_key;
    }
    public class cls_custom_data {
        public String lead_event_source;    //salesforce
        public String facebook_lifecycle_stage_name;    //LEAD
        public String lifecycle_stage_name;
    }
    public static Damac_FBJSON parse(String json){
        return (Damac_FBJSON) System.JSON.deserialize(json, Damac_FBJSON.class);
    }
}