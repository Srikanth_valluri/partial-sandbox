@isTest
public class CollectionReminderBatchTest {
    
    @isTest
    public static void test_1() {
        
        insert new IpmsRestServices__c(
           SetupOwnerId = UserInfo.getOrganizationId(),
           BaseUrl__c = 'http://83.111.194.181:8045/webservices/rest',
           Username__c = 'oracle_user',
           Password__c = 'crp1user',
           Client_Id__c = '8MABLQM-KJ8I8-1XA58-WWCM1S1',
           BearerToken__c = 'eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWyiwuVrJSKs5IBKK8bCUdpczEEiUrQ1MzAwtTMwsDIx2l1IoCqIC5oRFIoLQ4tSgvMTcVqM_C19HJJ9BX19vLwtNC1zDC0dRCNzzc2dcw2FCpFgBXRb-1XQAAAA.6Ym224Vwr9AniBeq6gL8OM9u4vGnUB_vbEUVjWojg14',
           Timeout__c = 120000
       );


        // create Account
        Account objAcc = new Account(Name = 'test', Party_Id__c = '43434', nationality__c='Chinese', Email__c = 'test.wewe@fasf.com');
        insert objAcc;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        objBookingUnit.Registration_Status_Code__c='LE';
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        insert objBookingUnit;
        system.debug('>>>>'+objBookingUnit);
        DP_Invoices__c obj=new DP_Invoices__c();
        obj.Accounts__c = objAcc.id;
        obj.BookingUnits__c = objBookingUnit.id;
        obj.Cover_Letter__c = 'test';
        obj.SOA__c= 'test';
        obj.Trxn_Number__c= 'testTrxnNumber';
        obj.TAX_Invoice__c= 'test';
        obj.COCD_Letter__c= 'test';
        obj.Other_Language_TAX_Invoice__c = 'test';
        obj.EmailSent__c = false;
        obj.Invoice_URLs__c = 'test';
        obj.Registration_ID__c = '12345';
        obj.Call_Type__c = 'testCallType';
        obj.Customer_Name__c = 'test';
        obj.Inv_Due__c = '10000';
        obj.Due_Date__c = System.today();
        insert obj;
        system.debug('obj>>>'+obj);
        insert new Collection_Reminders__c(
                        InvoiceDetails__c = obj.Id
                        , Email_Schedule__c = 'On Due'
                        , Email_Triggering_Date__c = system.today()
                    );

        String blobStr = 'Test Blob Response';

        FmHttpCalloutMock.Response unitSoaMockResponse = new FmHttpCalloutMock.Response(200, 'Success', '   {  '  +
         '       "responseId": "3072019-10305298",  '  +
         '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         '       "status": "S",  '  +
         '       "responseMessage": "Process Completed",  '  +
         '       "elapsedTimeMs": 8497,  '  +
         '       "responseLines": [  '  +
         '           {  '  +
         '               "documentName": "DPSOA",  '  +
         '               "language": "EN",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
         '           },  '  +
         '           {  '  +
         '               "documentName": "DPSOA_AR",  '  +
         '               "language": "AR",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
         '           }  '  +
         '       ],  '  +
         '       "complete": true  '  +
         '  }  ');

        String UNIT_SOA_str = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.UNIT_SOA, new List<String> {'3901'});

        FmHttpCalloutMock.Response blobResponse = new FmHttpCalloutMock.Response(200, 'Success', blobStr);
        FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
            UNIT_SOA_str => unitSoaMockResponse,
            'https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a' => blobResponse,
            'https://api.sendgrid.com/v3/mail/send' => sendGridResponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));

        
        Test.startTest();

            Database.executeBatch(new CollectionReminderBatch());
        Test.stopTest();
    }
}