global class DLDAgreementHandler {
    webservice static String generateAgreements (ID SrId) {
        System.Debug (SrId);
        String returnMsg = '';
        String srRecId = String.valueOf (srId).subString (0, 15);
        try {
            List <DLD_Agreement__c> dldAgreements = new List <DLD_Agreement__c> ();
            Map <ID, DLD_Agreement__c> existingAgreements = new Map <ID, DLD_Agreement__c> ();
            try {
                for (DLD_Agreement__c agreement :[SELECT Booking_Unit__c FROM DLD_Agreement__c WHERE Service_Request__c =: srRecId ]) {
                    existingAgreements.put (agreement.Booking_Unit__c, agreement);
                }
            } catch (Exception e) {
                System.Debug (e.getMessage());
            }
            List <Booking_Unit__c> units = new List <Booking_Unit__c> ();
            units = [SELECT Unit_Location__c, Inventory__c FROM Booking_Unit__c where SR_Id__c =: srRecId];
            Set <String> unitNames = new Set <String> ();
            for (Booking_Unit__c unit : Units) {
                unitNames.add (unit.Unit_Location__c);
            }
            map <String, Decimal> unitPropertyIds = new map <string, Decimal> ();
            
            for (DLD_Damac_Property_Mapping__c mapping: [SELECT DAMAC_Unit_Name__c , DLD_Property_Id__c FROM DLD_Damac_Property_Mapping__c WHERE 
                                                        DAMAC_Unit_Name__c IN :unitNames]) {
                unitPropertyIds.put (mapping.DAMAC_Unit_Name__c, mapping.DLD_Property_Id__c);    
            }
            for (Booking_Unit__c unit : Units) {
                System.Debug (existingAgreements.containsKey(unit.id));
                if (!existingAgreements.containsKey(unit.id)) {
                    if (unitPropertyIds.containskey (unit.Unit_location__c)) {
                        DLD_Agreement__c agreement = new DLD_Agreement__c ();
                        agreement.Service_Request__c = srID;
                        agreement.Inventory__c = unit.Inventory__c;
                        agreement.Booking_Unit__c = unit.ID;
                    
                        agreement.Property_Id__c = unitPropertyIds.get (unit.Unit_Location__c);
                        dldAgreements.add (agreement);
                        
                    
                    } else {
                        returnMsg = 'Unit Names under the Booking do not have DLD Property Ids mapped.';
                        break;
                    }
                } else {
                    returnMsg = 'DLD Agreement is already generated for this SR.';
                    break;
                    
                }
            }
            System.Debug (dldAgreements);
            if (dldAgreements.size () > 0) {
                insert dldAgreements;
                returnMsg = 'success';
            }
        } catch (Exception e) {
            System.Debug (e.getMessage());
            returnMsg = e.getmessage ();
        }
        return returnMsg;
    }
    public static void beforeInsertEvent (List <DLD_Agreement__c> records) {
        Set <ID> srIds = new Set <ID> ();
        Set <ID> invIds = new Set <ID> ();
        Set <ID> unitIds = new Set <ID> ();
        for (DLD_Agreement__c agreement :records) {
            if (agreement.Service_Request__c != NULL)
                srIds.add (agreement.Service_Request__c);
            if (agreement.Inventory__c != NULL)
                invIds.add (agreement.Inventory__c);
            if (agreement.Booking_unit__c != NULL)
                unitIds.add (agreement.Booking_Unit__c);
        }    
        
        
        Map <ID, Inventory__c> invDetailsMap = new Map <ID, Inventory__c> (
            [SELECT Property_Type__c, Plot_Number__c, Floor__c, Building_Name__c, 
            Master_Developer_EN__c,Land_Registration_Fee_A__c,
             Plot_Area__c, Plot_Area_sft__c, Parking__c, Balcony_Area__c, Balcony_Area_sft__c,
             Unit_Area__c, Unit_Area_sft__c FROM Inventory__c WHERE ID IN :invIds]);
             
         Map <ID, Booking_Unit__c> unitDetailsMap = new Map <ID, Booking_Unit__c> (
            [SELECT Parking_Bay_No__c, Building_Name_Loop__c,
            Discount__c, SPA_Anticipated_Completion_Date__c, Floor_Name__c,
            Master_Community_EN__c, Seller_Name__c,
            Area_sft__c, Area__c, Plot_Number_Loop__c, Property_Name_Inventory__c,
            Price_per_sq_ft__c            
            FROM Booking_Unit__c WHERE ID IN :unitIds]);
        
        System.Debug (invDetailsMap);
        for (DLD_Agreement__c agreement :records) {
            System.Debug (agreement);
            if (agreement.Booking_Unit__c != NULL && unitDetailsMap.containsKey (agreement.Booking_Unit__c)) {
                agreement.Associated_Parking__c = unitDetailsMap.get (agreement.Booking_Unit__c).Parking_Bay_No__c;
                agreement.Building_Name__c = unitDetailsMap.get (agreement.Booking_Unit__c).Building_Name_Loop__c ;
                agreement.Community__c = unitDetailsMap.get (agreement.Booking_Unit__c).Master_Community_EN__c;
                agreement.Discount__c = unitDetailsMap.get (agreement.Booking_Unit__c).Discount__c != NULL ? unitDetailsMap.get (agreement.Booking_Unit__c).Discount__c : 0;
                agreement.Estimated_Completion_Date__c = unitDetailsMap.get (agreement.Booking_Unit__c).SPA_Anticipated_Completion_Date__c;
                agreement.Floor__c = unitDetailsMap.get (agreement.Booking_Unit__c).Floor_Name__c;
                agreement.Master_Community__c = unitDetailsMap.get (agreement.Booking_Unit__c).Master_Community_EN__c;
                agreement.Master_Developer_Name__c = unitDetailsMap.get (agreement.Booking_Unit__c).Master_Community_EN__c;
                agreement.Name_In_English_Seller__c = unitDetailsMap.get (agreement.Booking_Unit__c).Seller_Name__c;
                //agreement.Number_of_Rooms__c = unitDetailsMap.get (agreement.Booking_Unit__c).Bedrooms__c;
                
                agreement.Plot_Area_Sq_ft__c = unitDetailsMap.get (agreement.Booking_Unit__c).Area_sft__c;
                agreement.Plot_Area_Sq_M__c = unitDetailsMap.get (agreement.Booking_Unit__c).Area__c != NULL ? String.valueOf (unitDetailsMap.get (agreement.Booking_Unit__c).Area__c) : '';
                agreement.Plot_No__c = unitDetailsMap.get (agreement.Booking_Unit__c).Plot_Number_Loop__c;
                agreement.Project_Name__c = unitDetailsMap.get (agreement.Booking_Unit__c).Property_Name_Inventory__c;
                
                agreement.Project_Name__c = unitDetailsMap.get (agreement.Booking_Unit__c).Property_Name_Inventory__c;
                agreement.Project_Name_bank__c = unitDetailsMap.get (agreement.Booking_Unit__c).Property_Name_Inventory__c;
                agreement.Purchase_Rate_per_Sq_ft__c = unitDetailsMap.get (agreement.Booking_Unit__c).Price_per_sq_ft__c != NULL ? String.valueOf (unitDetailsMap.get (agreement.Booking_Unit__c).Price_per_sq_ft__c != NULL) : '';
                agreement.Project_Name__c = unitDetailsMap.get (agreement.Booking_Unit__c).Property_Name_Inventory__c;

                
            }
            if (agreement.Inventory__c != NULL && invDetailsMap.containsKey (agreement.Inventory__c)) {
                agreement.Property_Type__c = invDetailsMap.get (agreement.Inventory__c).Property_Type__c;
                agreement.Plot_No__c = invDetailsMap.get (agreement.Inventory__c).Plot_Number__c;
                agreement.Floor__c = invDetailsMap.get (agreement.Inventory__c).Floor__c;
                agreement.Building_Name__c = invDetailsMap.get (agreement.Inventory__c).Building_Name__c;
                agreement.Plot_Area_Sq_M__c = invDetailsMap.get (agreement.Inventory__c).Plot_Area__c;
                agreement.Developer_Name__c = invDetailsMap.get (agreement.Inventory__c).Master_Developer_EN__c;
                agreement.Developer_Registration_Fees__c = invDetailsMap.get (agreement.Inventory__c).Land_Registration_Fee_A__c != NULL ? invDetailsMap.get (agreement.Inventory__c).Land_Registration_Fee_A__c: 0;
                
                agreement.Plot_Area_Sq_ft__c = invDetailsMap.get (agreement.Inventory__c).Plot_Area_sft__c;
                agreement.Associated_Parking__c = invDetailsMap.get (agreement.Inventory__c).Parking__c;
                agreement.Balcony_Area_Sq_ft__c = invDetailsMap.get (agreement.Inventory__c).Balcony_Area_sft__c;
                agreement.Unit_Net_Area_Sq_M_PropertyDetails__c = invDetailsMap.get (agreement.Inventory__c).Unit_Area__c;
                agreement.Unit_Net_Area_Sq_ft_PropertyDetails__c = invDetailsMap.get (agreement.Inventory__c).Unit_Area_sft__c;
            }
        }
        
    }
    public static void afterInsertEvent (List <DLD_Agreement__c> records) {

        Set <ID> bookingUnitIds = new Set <ID> ();
        for (DLD_Agreement__c agreement :records) {
            if (agreement.Booking_unit__c != NULL) {
                bookingUnitIds.add (agreement.Booking_Unit__c);
            }
        }
        Map <ID, ID> bookingIdMap = new Map <ID, ID> ();
        for (Booking_Unit__c unit :[SELECT Booking__c FROM Booking_Unit__c WHERE ID IN :bookingUnitIds AND Booking__c != NULL]) {
            bookingIdMap.put (unit.Id, unit.Booking__c);
        }
        Map <ID, List <Buyer__c>> srPrimaryBuyerMap = new Map <ID, List <Buyer__c>> ();
        for (Buyer__c buyer:[SELECT First_Name__c, Last_Name__c, Booking__c, 
                             Address_Line_1__c, Phone__c, Email__c, Nationality__c,DOB__c, 
                             City__c, Country__c, Passport_Number__c, Passport_Expiry__c,Buyer_Type__c ,Address_Line_2__c,EID_No__c,title__c,
                             Phone_with_Country_Code__c,Address_Line_3__c,First_Name_Arabic__c,last_Name_Arabic__c
                             FROM Buyer__c 
                             WHERE Booking__c IN :bookingIdMap.values ()])
        {
            if (!srPrimaryBuyerMap.containsKey (buyer.Booking__c))
                srPrimaryBuyerMap.put (buyer.Booking__c, new List <Buyer__c> {buyer});
            else
                srPrimaryBuyerMap.get (buyer.Booking__c).add (buyer);
        }
        Map <ID, List <Payment_Terms__c>> paymentTermsMap = new Map <ID, List <Payment_Terms__c>> ();
        for (Payment_Terms__c terms :[SELECT Name, Payment_Date__c, Modified_Percent_Value__c,
                                        Payment_Plan__r.Booking_Unit__r.Booking__c, Payment_Amount__c
                                        FROM Payment_Terms__c 
                                        WHERE Payment_Plan__r.Booking_Unit__c =: bookingUnitIds])
        {
            if (!paymentTermsMap.containsKey (terms.Payment_Plan__r.Booking_Unit__r.Booking__c))
                paymentTermsMap.put (terms.Payment_Plan__r.Booking_Unit__r.Booking__c, new List <Payment_Terms__c> {terms});
            else
                paymentTermsMap.get (terms.Payment_Plan__r.Booking_Unit__r.Booking__c).add (terms);
        }                                
        
        List <Agreement_Buyers__c> agreementBuyers = new List <Agreement_Buyers__c> ();
        List <DLD_Payment_Plans__c> dldPaymentPlansList = new List <DLD_Payment_Plans__c> ();
        
        for (DLD_Agreement__c agreement :records) {
            if (agreement.Booking_Unit__c != NULL) {
                if (bookingIdMap.containsKey (agreement.Booking_Unit__c)) {
                    if (bookingIdMap.get (agreement.Booking_Unit__c) != NULL) {
                        if (srPrimaryBuyerMap.containsKey (bookingIdMap.get (agreement.Booking_Unit__c))) {
                            for (Buyer__c b :srPrimaryBuyerMap.get (bookingIdMap.get (agreement.Booking_Unit__c))) {
                                Agreement_Buyers__c buyer = new Agreement_Buyers__c ();
                                buyer.Name_In_English__c = (b.First_Name__c != NULL ? b.First_Name__c : '') +' '+(b.Last_Name__c != NULL ? b.Last_Name__c : '');                        
                                buyer.Address__c = b.Address_Line_1__c+','+b.Address_Line_2__c+','+b.Address_Line_3__c+','+b.City__c+','+b.country__c;
                                buyer.Telephone_No__c = b.Phone__c;
                                buyer.Email_Address__c = b.Email__c;
                                buyer.Nationality__c = b.Nationality__c;
                                buyer.City__c = b.City__c;
                                buyer.Passport_No__c = b.Passport_Number__c;
                                buyer.DLD_Agreement__c = agreement.ID;
                                buyer.Date_of_Expiry__c = b.Passport_Expiry__c;
                                if(b.Buyer_Type__c == 'Individual'){
                                    buyer.Buyer_Type__c =  true;
                                }
                                if(b.Buyer_Type__c == 'Corporate'){
                                    buyer.Buyer_Type__c =  false;
                                }
                                // Added on Sep 24
                                buyer.Name_in_arabic__c = (b.First_Name_Arabic__c != NULL ? b.First_Name_Arabic__c: '') +' '+(b.Last_Name_Arabic__c!= NULL ? b.Last_Name_Arabic__c: '');
                                buyer.ID_No__c = b.Passport_Number__c;
                                buyer.UAE_Id_No__c = b.EID_No__c;
                                buyer.title__c = b.title__c;
                                buyer.Mobile_No__c = b.Phone_with_Country_Code__c;
                                buyer.M_Birth_Date__c= b.dob__c;
                                agreementBuyers.add (buyer);
                            }
                        }
                        /*
                        if (paymentTermsMap.containsKey (bookingIdMap.get (agreement.Booking_Unit__c))) {
                            for (Payment_Terms__c b :paymentTermsMap.get (bookingIdMap.get (agreement.Booking_Unit__c))) {
                                DLD_Payment_Plans__c plan = new DLD_Payment_Plans__c ();
                                plan.DLD_Agreement__c = agreement.ID;
                                //plan.PaymentDate__c = b.Payment_Date__c;
                                plan.PaymentPercentage__c = b.Payment_Amount__c != NULL ? Integer.valueOf (b.Payment_Amount__c) : NULL;
                                dldPaymentPlansList.add (plan);
                            }
                        }
                        */
                    }
                }
                
            }
            
        }
        
        insert agreementBuyers;
        //insert dldPaymentPlansList;
    }
}