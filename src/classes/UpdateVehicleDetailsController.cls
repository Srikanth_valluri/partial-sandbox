public class UpdateVehicleDetailsController{


    public  transient map<String, list<FM_Additional_Detail__c>>          mapAdditionalDetails    {get; set;}
    public  List<FM_Additional_Detail__c>                       newLstAddDetail         {get; set;}
    public  list<Update_Vehicle_Details_Doc_Uploads__mdt>       lstDocuments            {get; set;}
    public  list<SR_Attachments__c>                             lstCustomAttachments    {get; set;}
    //public  list<SR_Attachments__c>                             lstAttachToDisplay      {get; set;}
    public  List<SelectOption>                                  lstUnit                 {get; set;}
    //public  List<Booking_Unit__c>                               objUnitList             {get; set;}
    public  String                                              strAccountId            {get; set;}
    public  String                                              strSelectedUnit         {get; set;}
    public  Integer                                             intNumOfVehicles        {get; set;}
    public  Integer                                             intAddNumOfVehicles     {get; set;}
    public  transient String                                    strDocumentBody         {get; set;}
    public  transient String                                    strDocumentName         {get; set;}
    public  String                                              strResponseMap          {get; set;}
    public  Integer                                             indexVal                {get; set;}
    public  Boolean                                             showDataPanel           {get; set;}
    public  Integer                                             ocrDocCount             {get; set;}
    //public  transient String                                    deleteAttRecId          {get; set;} //
    //public  Boolean                                             disableIt               {get; set;} //
    public  Set<SR_Attachments__c>                              setAttach               {get; set;}

    //public  list<SR_Attachments__c>                             testLstAttach           {get; set;} //
    public  List<List<Integer>>                                 lstIndex                {get; set;}
    //public  Integer                                             lstAttachSize           {get; set;} //
    //public  Integer                                             numVehicle; //
    public  Integer intIndex;
    public  Integer countOfDocs = 0;


    @testVisible
    public UpdateVehicleDetailsController() {


        if (!FmcUtils.isCurrentView('UpdateVehicleDetails') && !Test.isRunningTest()) {
            return;
        }

        showDataPanel = false;
        ocrDocCount = 0;
        strAccountId = CustomerCommunityUtils.customerAccountId;

        System.debug('strAccountId: ' + strAccountId);

        lstCustomAttachments = new List<SR_Attachments__c>();
        //lstAttachToDisplay = new List<SR_Attachments__c>();
        //testLstAttach = new List<SR_Attachments__c>(); //
        lstIndex = new List<List<Integer>>();
        //lstAttachSize = 0;

        //
        Set<String> activeStatusSet= Booking_Unit_Active_Status__c.getall().keyset();

        List<Booking_Unit__c> objUnitList = [SELECT Id
                            , Owner__c
                            , Booking__r.Account__c
                            , Booking__r.Account__r.Name
                            , Owner__r.IsPersonAccount
                            , Tenant__r.IsPersonAccount
                            , Owner__r.Name
                            , Tenant__c
                            , Tenant__r.Name
                            , Resident__c
                            , Resident__r.Name
                            , Unit_Name__c
                            , Registration_Id__c
                            , Property_Name__c
                         FROM Booking_Unit__c
                         WHERE  Resident__c = :strAccountId
                         AND ( Handover_Flag__c = 'Y'
                                OR Early_Handover__c = true )
                         AND Unit_Active__c = 'Active' ];


        //

        /*List<Account> account = [SELECT id
                                        , Name
                                        ,(SELECT id
                                               ,Unit_Name__c
                                         FROM Booking_Units__r)
                                FROM Account
                                WHERE Id =: strAccountId];

        System.debug('Account is: ' + account);*/
        System.debug('objUnitList :' + objUnitList);

        if(objUnitList.size() > 0) {
            strSelectedUnit = objUnitList[0].id;
            System.debug('strSelectedUnit in constructor : ' + strSelectedUnit);
        }


        lstUnit = new List<Selectoption>();

        if(objUnitList.size() > 0 ) {
            for(Booking_Unit__c objBU : objUnitList) {
                lstUnit.add(new SelectOption(objBU.Id,objBU.Unit_Name__c));
            }
        }


        System.debug('lstUnit : ' + lstUnit);

        lstDocuments = [SELECT id
                             , MasterLabel
                             , Document_Name__c
                             , IsActive__c
                             , Mandatory__c
                        FROM Update_Vehicle_Details_Doc_Uploads__mdt
                        WHERE IsActive__c = true];
        System.debug('lstDocuments : ' + lstDocuments);
        System.debug('lstDocuments.size() :' + lstDocuments.size());
        //
        initializeAddDetailsMap();
        //
    }

    public void selectUnit() {
        System.debug('strSelectedUnit = ' + strSelectedUnit);
        if( String.isNotBlank( strSelectedUnit )) {
            intNumOfVehicles = 0;
            intAddNumOfVehicles = 0;
            initializeAddDetailsMap();
        }

    }

    public void initializeAddDetailsMap() {
        List<FM_Additional_Detail__c> lstFMAddDetails = [SELECT Id
                                                        , RecordType.Name
                                                        , RecordType.DeveloperName
                                                        , Emergency_Contact_Case__c
                                                        , Resident_Case__c
                                                        , Vehicle_Case__c
                                                        , Vehicle_Number__c
                                                        , Name__c
                                                        , Relationship__c
                                                        , Phone__c
                                                        , Age__c
                                                        , Nationality__c
                                                        , Passport_Number__c
                                                        , Identification_Number__c
                                                        , Vehicle_Make_Model__c
                                                        , Vehicle_Model__c
                                                        , Vehicle_Colour__c
                                                        , Email__c
                                                        , Parking_Slot_Number__c
                                                        , People_of_Determination__c
                                                        , Date_of_Birth__c
                                                        , Gender__c
                                                        , Registration_Country__c
                                                        , Type_of_Vehicle__c
                                                        , Mobile__c
                                                        , Vehicle_Sticker_No__c
                                                        , Pet_Case__c
                                                        , Pet_Name__c
                                                        , Pet_Type__c
                                                        , Description_Of_Pet__c
                                                        , Registration_Expiry_Date__c
                                                        , Place_of_Issue__c
                                                        , T_C_No__c
                                                        , Registration_Date__c
                                                        , Vehicle_Insurance_Expiry__c
                                                        , Policy_No__c
                                                        , Number_of_Passengers__c
                                                        , Owner__c
                                                        , Mortgage_By__c
                                                     FROM FM_Additional_Detail__c
                                                     WHERE IsNewInfo__c = True
                                                     AND (Vehicle_Account__c =: strAccountId OR
                                                     Resident_Account__c =: strAccountId)
                                                     AND Booking_Unit__c =: strSelectedUnit];

        System.debug('lstFMAddDetails: ' + lstFMAddDetails);


        mapAdditionalDetails = new map<String, list<FM_Additional_Detail__c>>();
        if(lstFMAddDetails.size() > 0) {

            for(FM_Additional_Detail__c objAD : lstFMAddDetails) {
                if( mapAdditionalDetails.containsKey( objAD.RecordType.Name ) ) {
                    mapAdditionalDetails.get( objAD.RecordType.Name ).add( objAD );
                }
                else {
                    mapAdditionalDetails.put( objAD.RecordType.Name, new list<FM_Additional_Detail__c>{ objAD } );
                }
            }
            if( mapAdditionalDetails.containsKey( 'Vehicle' ) ) {
                intNumOfVehicles = mapAdditionalDetails.get( 'Vehicle' ).size() ;
            }


        }
        else {
            intNumOfVehicles = 0;
        }

        System.debug('mapAdditionalDetails : ' + mapAdditionalDetails);
    }

    public void initializeNewAddDetails() {
        lstIndex = new List<List<Integer>>();
        System.debug('intAddNumOfVehicles : ' + intAddNumOfVehicles);


        Booking_Unit__c objBU = [SELECT id
                                       ,Unit_Name__c
                                       ,Property_Name__c
                                 FROM Booking_Unit__c
                                 WHERE id =: strSelectedUnit];
        System.debug('objBU in initializeNewAddDetails : ' + objBU.Unit_Name__c);

        //HardCoded list
        List<Integer> innerLstFiller = new List<Integer>{1,2};
        List<Integer> innerLst = new List<Integer>();
        intIndex = 1;

        if(intAddNumOfVehicles > 0) {
            newLstAddDetail = new List<FM_Additional_Detail__c>();
            for(Integer i=0; i<intAddNumOfVehicles; i++) {
                FM_Additional_Detail__c objAD = createAddDetailsObj( 'Vehicle' );
                //
                SR_Attachments__c testObjSR = new SR_Attachments__c();

                objAD.Booking_Unit__c = strSelectedUnit;
                objAD.Vehicle_Account__c = strAccountId;
                objAd.IsNewInfo__c = true;
                objAD.Unit_Name__c = objBU.Unit_Name__c;
                objAD.Property_Name__c = objBU.Property_Name__c;
                newLstAddDetail.add(objAD);

                //testLstAttach.add(testObjSR);
                //
                lstIndex.add(new List<Integer>{1,2});   //new List<Integer>{1,2} instead of innerLstFiller

            }
            System.debug('newLstAddDetail :' + newLstAddDetail);
            System.debug('newLstAddDetail.Size() : ' + newLstAddDetail.size() );
            System.debug('lstIndex before: ' + lstIndex);
            System.debug('lstDocuments.size()1 :' + lstDocuments.size());

            for(Integer j=0; j<intAddNumOfVehicles; j++) {
                System.debug('lstIndex-'+j+' :'+lstIndex[j]);
                lstIndex[j].clear();
                for(Integer k=0; k<lstDocuments.size(); k++) {
                    innerlst.add(intIndex);
                    intIndex++;
                }
                lstIndex[j].addAll(innerlst);
                innerlst.clear();
            }
            System.debug('lstIndex -> ' + lstIndex);
        }
    }

    public FM_Additional_Detail__c createAddDetailsObj( String strRecordTypeName ) {
        FM_Additional_Detail__c objAD = new FM_Additional_Detail__c();
        objAD.RecordTypeId = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get( strRecordTypeName ).getRecordTypeId();
        return objAD ;
    }

    @RemoteAction
    public static Id uploadDocument(
        Id strAccountId, Id strSelectedUnit, String strDocumentName, String strDocumentBody
    ) {
        system.debug('== strAccountId =='+strAccountId);
        system.debug('== strSelectedUnit =='+strSelectedUnit);
        system.debug('== strDocumentName =='+strDocumentName);
        system.debug('== strDocumentBody =='+strDocumentBody);

        //Account objAcc = [SELECT Id
        //                        ,Name
        //                  FROM Account
        //                  WHERE id =: strAccountId];
        //System.debug('objAcc in uploadDocument : ' + objAcc.Name);

        //Booking_Unit__c objBU = [SELECT id
        //                               ,Unit_Name__c
        //                         FROM Booking_Unit__c
        //                         WHERE id =: strSelectedUnit];
        //System.debug('objBU in uploadDocument : ' + objBU.Unit_Name__c);

        if( strSelectedUnit != NULL ) {
            UploadMultipleDocController.data objResponse = new UploadMultipleDocController.data();
            list<UploadMultipleDocController.MultipleDocRequest> lstWrapper = new list<UploadMultipleDocController.MultipleDocRequest>();
            if( String.isNotBlank( strDocumentName ) && String.isNotBlank( strDocumentBody ) ) {
                lstWrapper.add( FM_Utility.makeWrapperObject( EncodingUtil.Base64Encode( RentalPoolTerminationHelper.extractBody( strDocumentBody ) ) ,
                                                                       strDocumentName ,
                                                                       strDocumentName ,
                                                                       strSelectedUnit, '1' ) );
                //calling falcon service;
               

            }
            if( !lstWrapper.isEmpty() ) {
                objResponse = PenaltyWaiverService.uploadDocumentsOnCentralRepo( lstWrapper );
                strDocumentBody = null; //Making base64 file null for view state issue
                lstWrapper = null;
                system.debug('== objResponse document upload =='+objResponse);

                if( objResponse != NULL && objResponse.data != NULL ) {
                    return processCustomAttachment( strSelectedUnit, strAccountId, strDocumentName, objResponse );

                }
                else if( objResponse != NULL && String.isNotBlank( objResponse.status ) && objResponse.status.equalsIgnoreCase( 'Exception' ) ) {
                    Error_Log__c objError = new Error_Log__c(Account__c = strAccountId,
                                                             Booking_Unit__c = strSelectedUnit,
                                                             Error_Details__c = objResponse.message );
                    insert objError ;
                }
            }
        }
        return NULL;
    }

   

    @RemoteAction
    public static String convertToBlob(List<String> strFilesData) {
        //System.debug('strFilesData:: '+strFilesData);
        System.debug('strFilesData.size:: '+strFilesData.size() );

        //Map<String, String> mapFieldValues = new Map<String, String>();
        String strResponse;
        if(strFilesData.size() == 2) {
            strResponse = VehicleMulkiaOCRService.uploadFile(strFilesData);
            System.debug('strResponse: '+strResponse);
        }

        return strResponse;

    }

    public void PopulateFormData() {
        if(strResponseMap.containsIgnoreCase('&quot;')) {
            strResponseMap = strResponseMap.replace('&quot;', '"');
        }
        System.debug('strResponseMap in PopulateFormData method: '+strResponseMap);
        System.debug('IndexVal is:: '+indexVal);

        if(String.isNotBlank(strResponseMap)) {
            Map<String, Object> responseMap = (Map<String, Object>)JSON.deserializeUntyped(strResponseMap);
            System.debug('responseMap: '+responseMap);

            List<Object> lstFields = (List<Object>)responseMap.get('fields');
            System.debug('lstFields: '+lstFields);
            System.debug('lstFields.size:: '+lstFields.size());

            Map<String, String> mapFields = new Map<String,String>();
            for(Object lstObj: lstFields) {
              Map<String, Object> a2 =(Map<String, Object>)lstObj;
              System.debug('a2: '+a2);
              mapFields.put(String.valueOf(a2.get('key')), String.valueOf(a2.get('value')));
            }
            system.debug('mapFields:: '+mapFields);

            //adding to FM_Additional_details object
            newLstAddDetail[indexVal].Vehicle_Number__c = mapFields.get('Traffic Plate no');
            newLstAddDetail[indexVal].Vehicle_Model__c  = mapFields.get('Model');
            newLstAddDetail[indexVal].Vehicle_Make_Model__c = mapFields.get('Veh Type');
            newLstAddDetail[indexVal].Registration_Expiry_Date__c = getDateFormat(mapFields.get('Exp. Date'));
            newLstAddDetail[indexVal].T_C_No__c = mapFields.get('T. C. No');
            newLstAddDetail[indexVal].Owner__c = mapFields.get('Owner');
            //Coming soon
            //newLstAddDetail[indexVal].Nationality__c = mapFields.get('Nationality');
            newLstAddDetail[indexVal].Registration_Date__c = getDateFormat(mapFields.get('Reg. Date'));
            newLstAddDetail[indexVal].Vehicle_Insurance_Expiry__c = getDateFormat(mapFields.get('Ins. Exp'));
            newLstAddDetail[indexVal].Policy_No__c = mapFields.get('Policy');
            newLstAddDetail[indexVal].Mortgage_By__c = mapFields.get('Mortage');
            if(String.isNotBlank(mapFields.get('No. of Pass'))) {
                newLstAddDetail[indexVal].Number_of_Passengers__c = Decimal.ValueOf(mapFields.get('No. of Pass'));
            }
            

            ocrDocCount = ocrDocCount + 1;
            System.debug('ocrDocCount: '+ocrDocCount);
            System.debug('intAddNumOfVehicles: '+intAddNumOfVehicles);

            if(ocrDocCount == intAddNumOfVehicles) {
                showDataPanel = true; //to show form data section after all docs are uploaded to OCR
            }
                       
        }
        else{
            ocrDocCount = ocrDocCount + 1;
            System.debug('ocrDocCount: '+ocrDocCount);
            System.debug('intAddNumOfVehicles: '+intAddNumOfVehicles);

            if(ocrDocCount == intAddNumOfVehicles) {
                showDataPanel = true; //to show form data section after all docs are uploaded to OCR
            }
        }
    }

    public date getDateFormat(String strDate) {

        if(String.isBlank(strDate)) {
            return null;
        }

        System.debug('strDate: '+strDate);
        Date finalDate = date.newInstance(Integer.ValueOf(strDate.substringAfterLast('/')), Integer.ValueOf(strDate.substring(3,5)), Integer.ValueOf(strDate.substring(0,2)) );
        System.debug('finalDate :: '+finalDate);

        return finalDate;
    }

    public static Id processCustomAttachment(
        Id bookingUnitId, Id accountId, String strDocumentName, UploadMultipleDocController.data objResponse
    ) {

        strDocumentName = strDocumentName.substring( 0, strDocumentName.lastIndexOf('.') );

        SR_Attachments__c objCustAttach = new SR_Attachments__c();

        for( UploadMultipleDocController.MultipleDocResponse objFile : objResponse.data ) {
            //objCustAttach.Account__c = accountId ;
            objCustAttach.FM_Account__c = accountId;
            objCustAttach.Attachment_URL__c = objFile.url;
            objCustAttach.Booking_Unit__c = bookingUnitId ;
            //objCustAttach.FM_Case__c = fmCaseId ;
            objCustAttach.Name = strDocumentName ;
            objCustAttach.isValid__c = getDocumentValidity();
            //lstCustomAttachments.add(objCustAttach);
            strDocumentName = '';
            //strDocumentBody = '';
        }

        System.debug('objCustAttach : ' + objCustAttach);

        insert objCustAttach;

        /*if( !lstCustomAttachments.isEmpty() ) {
            //System.debug('In side lstCustomAttachments IF check : ' + lstCustomAttachments);
            //System.debug('In side lstCustomAttachments size : ' + lstCustomAttachments.size());
            //System.debug('countOfDocs before adding: ' + countOfDocs);
            countOfDocs += lstCustomAttachments.size();
            //System.debug('countOfDocs: ' + countOfDocs);
            //lstAttachToDisplay.addAll(lstCustomAttachments);
            //System.debug('lstAttachToDisplay : ' + lstAttachToDisplay);
            insert lstCustomAttachments;
            lstCustomAttachments = new List<SR_Attachments__c>();
            return customAttachmentId;
            //System.debug('After refresh lstCustomAttachments : ' + lstCustomAttachments);

        }*/
        //Integer i = getNoOfVehicle(intAddNumOfVehicles);
        //Integer j = getLstAttachSize();
        return objCustAttach.Id;
    }

    public static Boolean getDocumentValidity() {
        return true;
    }

    public void saveVehicleDetails() {

        //List<FM_Additional_Detail__c> lstFMAddToInsert = new List<FM_Additional_Detail__c>();

        /*if(mapAdditionalDetails.containsKey('Vehicle') && mapAdditionalDetails.get('Vehicle').size() > 0) {
            System.debug('Existing Vehicle Info list: ' + mapAdditionalDetails.get('Vehicle'));
            lstFMAddToInsert.addAll(mapAdditionalDetails.get('Vehicle'));
        }*/

        if(newLstAddDetail.size() > 0) {
            System.debug('New FM Add detail lst before insert :' + newLstAddDetail);
            try{
                insert newLstAddDetail;
                System.debug('After insert (FM Additional Details): ' + newLstAddDetail);
            }catch(DmlException e) {
                System.debug('DML Exception : ' + e.getMessage());
            }
            //lstFMAddToInsert.addAll(newLstAddDetail);
        }

        //if(lstFMAddToInsert.size() > 0) {
        //    try{
        //        insert lstFMAddToInsert;
        //        System.debug('After insert (FM Additional Details): ' + lstFMAddToInsert);
        //    }catch(DmlException e) {
        //        System.debug('DML Exception : ' + e.getMessage());
        //    }
        //}

        // if(lstCustomAttachments.size() > 0) {
        //     System.debug('lstCustomAttachments : ' + lstCustomAttachments);
        //     try {
        //         insert lstCustomAttachments;
        //         System.debug('After insert (SR Attachments): ' + lstCustomAttachments);
        //     }catch(DmlException e) {
        //         System.debug('DML Exception : ' + e.getMessage());
        //     }


        // }


    }

    /*public boolean getshowSavebtn() {
        if(countOfDocs > 0 && intAddNumOfVehicles != null) {
            System.debug('countOfDocs - showSavebtn: ' + countOfDocs);
            Integer intAddNumOfVehiclesMul = intAddNumOfVehicles*2; // *2 for doc docs (Mulkia front & back)
            System.debug('intAddNumOfVehiclesMul' + intAddNumOfVehiclesMul);
            if(countOfDocs == intAddNumOfVehiclesMul){
                return true;
            }
            else{
                return false;
            }
        }
        else {
            return false;
        }

    }*/

    /*public void deleteAttachment() {
        if( String.isNotBlank( deleteAttRecId ) ) {
            System.debug('deleteAttRecId : ' + deleteAttRecId);
            //delete new SR_Attachments__c( Id = deleteAttRecId );

            setAttach = new set<SR_Attachments__c>();
            setAttach.addAll(lstAttachToDisplay);
            System.debug('-->> setAttach : ' + setAttach);
            System.debug('-->> setAttach size : ' + setAttach.size());
            System.debug('-->> lstAttachToDisplay : ' + lstAttachToDisplay);
            System.debug('-->> lstAttachToDisplay size: ' + lstAttachToDisplay.size());
            for(SR_Attachments__c  objAttach: lstAttachToDisplay ) {
               if(objAttach.Id == deleteAttRecId) {
                    System.debug('-->> objAttach.Id : ' + objAttach.Id + ' == ' + deleteAttRecId);
                    setAttach.remove(objAttach);
                    System.debug('-->> setAttach after remove: ' + setAttach);
                    System.debug('-->> setAttach size in loop : ' + setAttach.size());
                    //lstAttachToDisplay.addAll(setAttach);
                }
            }
            lstAttachToDisplay.clear();
            lstAttachToDisplay.addAll(setAttach);
            System.debug('-->> lstAttachToDisplay After: ' + lstAttachToDisplay);
            System.debug('-->> lstAttachToDisplay size After : ' + lstAttachToDisplay.size());
            delete new SR_Attachments__c( Id = deleteAttRecId );
        }

    }*/

}