/************************************************************************************************
 * Description: Utility class for Emergency SMS                                                 *
 ===============================================================================================*
 * Ver      Date            Author                  Modification                                *
 *==============================================================================================*
 * 1.0      08/08/2019      Arjun Khatri            Initial Draft                               *
*************************************************************************************************/
Public Class EmergencySMSUtility {
    
    static String user = Label.FM_SMS_Service_Username;
    static String passwd = Label.FM_SMS_Service_Password;
    static String strSID = Label.FM_SMS_Service_SenderId;

/****************************************************************************************************
Method Name     : sendEmergencySMStoVisitors                                                        *
Description     : Sends emergency sms Visitors                                                      *
Parameter(s)    : Phone Number, SMS Contents                                                        *
Return          : HttpResponse                                                                      *
*****************************************************************************************************/    
    @future(callout=true)
    public static void sendEmergencySMStoVisitors(String phoneNumber, String contentValue) {
        List<SMS_History__c> smsLst = new List<SMS_History__c>();
        //Call method to remove leading zeros from phone number
        List<String> lstPhoneNumber = GenericUtility.removeLeadingZeros( new List<String>{phoneNumber} );
        phoneNumber = lstPhoneNumber != null && lstPhoneNumber.size() > 0 
                    ? lstPhoneNumber[0]
                    :  phoneNumber;
        if(String.isNotBlank(phoneNumber)) {
            HttpResponse res = sendEmergencySms(phoneNumber, contentValue);
            
            if( res.getBody() != null ) {
                    
                // Create SMS history
                SMS_History__c smsObj = new SMS_History__c();
                smsObj.Message__c = contentValue;
                smsObj.Phone_Number__c = PhoneNumber;
                smsObj.Is_Visitor_Sms__c = true;
                smsObj.Description__c = res.getBody();
                System.debug('smsObj::::else:'+smsObj);
                    
                if(String.valueOf(res.getBody()).contains('OK:')) {
                    smsObj.Is_SMS_Sent__c = true;
                    smsObj.sms_Id__c = res.getBody().substringAfter('OK:');
                }
                smsLst.add(smsObj);
            }//End res if    
    
            if(smsLst.size() > 0 ) {
                    insert smsLst;
            }
        }
    }

/****************************************************************************************************
Method Name     : sendEmergencySms                                                                  *
Description     : Genric method to send emergency sms                                               *
Parameter(s)    : Phone Number, SMS Contents                                                        *
Return          : HttpResponse                                                                      *
*****************************************************************************************************/    
    public static HttpResponse  sendEmergencySms(String phoneNumber, String contentValue) {
        
        //Make callout to send emergency sms
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        req.setMethod( 'POST' ); // Method Type
        req.setEndpoint( 'http://api.smscountry.com/SMSCwebservice_bulk.aspx' );
        

        //Call method to remove special characters
        string msgCont = GenericUtility.encodeChar(contentValue);
        system.debug('msgCont = ' + msgCont);

        strSID = SMSClass.getSenderName(user, PhoneNumber, false);

        req.setBody('user='+ user + '&passwd=' + passwd +'&message=' 
                + msgCont + '&mobilenumber=' + PhoneNumber 
                + '&sid='+ strSID + '&MTYPE=LNG&DR=Y'); // Request Parameters
        
        system.debug('req Body---'+req.getBody());
        res = http.send(req);
        system.debug('Response---'+res);
        system.debug('Response body---'+res.getBody());
        
        return res;
    }//End sendEmergencySms method
}