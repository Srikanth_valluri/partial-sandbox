global class UpdateUserForMergeParties implements Schedulable{
    
    global Account masterAccount;
    
    //@InvocableMethod
    global static void UpdateUsers(Account masterAccount) {
        system.debug('masterAccount < : '+masterAccount);
        
        UpdateUserForMergeParties m = new UpdateUserForMergeParties();
        m.masterAccount = masterAccount;
        DateTime dt = system.now().addMinutes(1);
        String day = string.valueOf(dt.day());
        String month = string.valueOf(dt.month());
        String hour = string.valueOf(dt.hour());
        String minute = string.valueOf(dt.minute());
        String second = string.valueOf(dt.second());
        String year = string.valueOf(dt.year());
        String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
    
        String jobID = system.schedule('Merge Parties User Updation Job '+masterAccount.Id + system.now(), strSchedule, m);
    }

    global void execute(SchedulableContext ctx) {
        UpdUser(masterAccount);
    }
    
  //@future
  global static void UpdUser(Account masterAccount) {
        List<User> lstNewUser = new List<User>();
        Id profileId = [select id from profile where name = 'Customer Community Login User(Use this)'].id;


        List<User> lstUserMaster = [SELECT IsActive, IsPortalEnabled ,Name,UserRoleId,Username,Title,localesidkey 
                                   , Party_Id__c,Phone,ManagerId,EmployeeNumber
                                   , IPMS_Employee_ID__c,Address,email ,emailencodingkey ,languagelocalekey
                                   , ContactId ,timezonesidkey ,FirstName ,LastName ,ProfileId,Alias
                                FROM User
                               where Party_Id__c =: masterAccount.Party_ID__c  ];
        system.debug(' lstUserMaster 1 : ' + lstUserMaster);
                                 
        if( lstUserMaster.size() > 0 ) {
            lstUserMaster[0].Party_Id__c = '';
            lstUserMaster[0].IPMS_Employee_ID__c = '';
            system.debug(' lstUserMaster 2 : ' + lstUserMaster);
            update lstUserMaster[0];                
        }

        system.debug(' lstUserMaster 3 : ' + lstUserMaster);
        system.debug(' masterAccount :  '+ masterAccount);
        system.debug(' masterAccount.Contacts[0] :  '+ masterAccount.Contacts[0]);
        User objUserClone = new User();
        objUserClone.IsActive = true;
        //objUserClone.IsPortalEnabled = true;
        String aliasVal = String.valueOf(masterAccount.Contacts[0].Id);
        objUserClone.alias = aliasVal.substring(0, 5);
        objUserClone.Email = masterAccount.Contacts[0].Email != null ? masterAccount.Contacts[0].Email : 'test@test.com' ;
        objUserClone.emailencodingkey = 'UTF-8';
        objUserClone.timezonesidkey = 'Asia/Dubai';
        objUserClone.FirstName = masterAccount.Contacts[0].FirstName != '' ? masterAccount.Contacts[0].FirstName : 'Test First Name' ;
        objUserClone.LastName = masterAccount.Contacts[0].LastName != '' ? masterAccount.Contacts[0].LastName : 'Test Last Name' ;
        objUserClone.ProfileId = profileId;
        objUserClone.languagelocalekey = 'en_US';
        objUserClone.localesidkey = 'en_GB';
        objUserClone.Party_Id__c = masterAccount.Party_ID__c;
        objUserClone.IPMS_Employee_ID__c = masterAccount.Party_ID__c;
        objUserClone.ContactId = masterAccount.Contacts[0].Id;
        objUserClone.username = getUserName(masterAccount.Contacts[0]);
        
        system.debug(' objUserClone:  '+ objUserClone);
        
        
        lstNewUser.add(objUserClone);
        
        
        insert lstNewUser;
  }
    global static String randomWithLimit(Integer upperLimit) {
        Integer rand = Math.round(Math.random() * 1000);
        return String.valueOf(Math.mod(rand, 9999)).leftpad(4, '0');
    }  
    global static string getUserName(Contact c) {
        String username = '';
        if (c.FirstName != NULL) {
            username += c.FirstName.replaceAll( '\\s+', '');
        }
        if (c.LastName != NULL) {
            username += c.LastName.replaceAll( '\\s+', '');
        }
        username += randomWithLimit(9999);

        username += '@damacagents.com';
        return username;
    }     
}