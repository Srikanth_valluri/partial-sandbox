public with sharing class GenerateUndertakingLetter {
    public String caseId;
    public Case objC;
    public GenerateUndertakingLetter(ApexPages.StandardController stdCon){
        caseId = stdCon.getId();
        objC = [Select Id
                     , Booking_Unit__c                     
                     , Registration_ID__c
                from Case 
                where Id =: caseId];
    }
    
    public void callDrawloop(){
        executeBatch();
    }
    
    public pageReference returnToCase(){
        PageReference Pg = new PageReference('/'+caseId);
        return pg;
    }
    
    public void executeBatch(){
        if(String.isNotBlank(objC.Registration_ID__c)){
            GenerateDrawloopDocumentBatch objInstance = new GenerateDrawloopDocumentBatch(caseId
                                                                                         , System.Label.Plot_Undertaking_DDPId
                                                                                         , System.Label.Plot_Undertking_TemplateId);
            Id batchId = Database.ExecuteBatch(objInstance);
            if(String.valueOf(batchId) != '000000000000000'){
                SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
                 objCaseAttachment.Case__c = caseId ;
                 objCaseAttachment.Name = 'Signed Undertaking Letter';
                 objCaseAttachment.Booking_Unit__c = objC.Booking_Unit__c;
                 insert objCaseAttachment;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Your request for Undertaking Letter was successfully submitted. Please check the documents section for the document in a while.');
                ApexPages.addMessage(myMsg);
            }else{
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Your request for Undertaking Letter could not be completed. Please try again later.');
                ApexPages.addMessage(myMsg);
            }
        }else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Regid for the new unit has not been generated yet. Please try again later.');
            ApexPages.addMessage(myMsg);
        }
    }
}