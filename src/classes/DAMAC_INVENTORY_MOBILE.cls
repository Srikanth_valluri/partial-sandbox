/*
* Class Name : DAMAC_INVENTORY_MOBILE 
* Test Class : DAMAC_INVENTORY_MOBILE_TEST
* Description: To display Units, projects, saved units and Sales offer on mobile APP

*/

global class DAMAC_INVENTORY_MOBILE {

    public Integer count = 100;
    public Integer pageNumber = 1;
    /* To display data on page */
    public Transient List <Inventory__c> inventories { get; set; }
    public integer inventoryCount { get; set; }
    public Transient List <WrapperForMarketing> projectsList { get; set; }
    public Transient Map <String, Integer> marketingLocations { get; set; }
    public String inspectionReportURL { get; set; }
    public String longitude { get; set; }
    public String latitude { get; set; }
    public List <Project_Information__c> projInformation { get; set; }
    public List <String> facilites { get; set; }
    public Inventory__c inventory { get; set; }
    public Marketing_Documents__c marketingDoc { get; set; }
    public List <Payment_Plan__c> paymentPlans { get; set; }
    public Map <String, AdditionalFeatures__c> featureURLS { get; set; }
    /* For Filters */
    public set<string> propStatuses { get; set; }
    public set<string> acd { get; set; }
    public Map<String, String> propertyTypeWithImagesmtd { get; set; }
    public Map<String,id> productname_idmap { get; set; }
    public Set <ID> favoriteInventories { get; set; }
    
    public String selectedPropTypes { get; set; } 
    public String selectedDistrict { get; set; } 
    public String selectedCity { get; set; } 
    public String selectedStatus { get; set; } 
    public String selectedAcd { get; set; } 
    public String selectedMarketNames { get; set; } 
    public String selectedUnitTypes { get; set; } 
    public String selectedRooms { get; set; } 
    public String selectedViewTypes { get; set; }
    public String minArea { get; set; }
    public String maxArea { get; set; }
    public String minPrice { get; set; }
    public String maxPrice { get; set; }
    public String minPricePerSq { get; set; }
    public String maxPricePerSq { get; set; }
    
    public set<string> district ;
    public set<string> cities ;
    public set<string> unitTypes ;
    public set<string> bedrooms ;
    public set<string> viewTypes ;
    
    
    // TO load the Unit details when unit details page is loaded
    public void initUnitDetails () {
        favoriteInventories = new Set <ID> ();
        featureURLS = new Map <String, AdditionalFeatures__c> ();
        for (AdditionalFeatures__c feature : [SELECT URL__c, Label__c FROM AdditionalFeatures__c WHERE URL__c != NULL]) {
            featureURLS.put (feature.Label__c, feature);
        }
        
        ID unitId = apexpages.currentpage().getparameters().get('invId');
        paymentPlans = new List <Payment_Plan__c> ();
        inventory = [SELECT Building_Location__c, Unit_Name__c, Area_sft__c, Bedroom_Type__c, Unit_Type__c, View_Type__c, 
                     Property_Status__c, Construction_Status__c,
                     Price__c, Floor_Plan__c, Unit_Plan__c, Special_Price_Tax_Amount__c, Property_name__c,
                     Property_City__c, Property_Country__c, Selling_Price__c, ACD_Date__c,
                     Marketing_Plan__c,Marketing_Name_Doc__c,Inspection_URL__c,property__r.Longitude__c, 
                     property__r.Latitude__c, Total_Price_inc_VAT__c
                     
                     FROM Inventory__c 
                     WHERE id =: unitId];
        for (Agent_Favorite_Inventory__c inv : [SELECT Inventory__c FROM Agent_Favorite_Inventory__c WHERE User__c =: UserInfo.getUserId() AND Inventory__c =: unitId]) 
        {
            favoriteInventories.add (inv.Inventory__c );
        }
        paymentPlans = [SELECT Id, 
                            (SELECT Description__c, Milestone_Event__c, Percentage__c, Payment_Amount__c 
                             FROM Payment_Terms__r ORDER BY SF_Line_ID__c ASC) 
                        FROM Payment_Plan__c 
                        WHERE Building_Location__c =: inventory.Building_Location__c 
                        AND Active__c = TRUE AND Building_Location__c != null 
                        AND Effective_From__c <= TODAY AND Effective_To_calculated__c >= TODAY 
                        AND booking_unit__c = null AND parent_payment_plan__c = null ORDER BY CREATEDDATE ASC ];
        
        marketingDoc = [SELECT Id,
                        (SELECT Name FROM Marketing_Documents__r) 
                        FROM Marketing_Documents__c 
                        WHERE id =: inventory.Marketing_Name_Doc__c];
        projInformation = getProjectInfo (new List<String> {'Gallery'} , marketingDoc.Id);
        facilites = new List <String> ();    
        for (Project_Information__c facility : getProjectInfo (new List<String> {'Project Features'} , marketingDoc.Id)) {
            String feature = facility.Project_Features__c;
            if (feature != null) {
                if (feature.contains (';'))
                    facilites = feature.split (';');
                else
                    facilites.add (feature);
            }
        }
    }
    
    // TO load the Payment plans based on Unit ID
    public void initPaymentPlans(){
        ID unitId = apexpages.currentpage().getparameters().get('invId');
        ID planId = apexpages.currentpage().getparameters().get('planId');
        inventory = [SELECT Marketing_Name_Doc__r.Brochure_URL__c ,Total_Price_inc_VAT__c,Unit_Name__c,Unit_Plan__c, Floor_Plan__c 
                        FROM Inventory__c WHERE id =: unitId];
                        
        paymentPlans = [SELECT Id, 
                            (SELECT Description__c, Milestone_Event__c, Percentage__c, Payment_Amount__c 
                             FROM Payment_Terms__r ORDER BY SF_Line_ID__c ASC) 
                        FROM Payment_Plan__c WHERE Id =: planId ];
        
    }
    
    // TO generate the saleoffer record
    @RemoteAction
    global static Id createSalesOffer(Id invId){
        SalesOffer_Record__c salesOffer = new SalesOffer_Record__c();
        salesOffer.Inventory__c = invId;
        salesOffer.Selected_Inventory_Ids__c = invId;
        salesOffer.Trasaction_User__c = UserInfo.getUserId();
        insert salesOffer;
        
        return salesOffer.Id;
    }
    
    // TO generate Salesoffer record link to view in mobile
    @RemoteAction
    global static String salesOfferLink (id recId) {
        SalesOffer_Record__c salesOffer = [SELECT Name FROM SalesOffer_Record__c WHERE ID =: recId ];
        
        PageReference ref = page.damac_inventorydetail_clone;
        ref.getparameters().put('recId',  salesOffer.id);
        ref.getparameters().put('selectedCurrency', 'AED'); 
        
        ContentVersion cont = new ContentVersion();
        
        cont.Title = salesOffer.Name+'.pdf';
        cont.PathOnClient = salesOffer.Name+'.pdf';
        if (!Test.isRunningTest())
            cont.VersionData = ref.getContent();
        else
            cont.VersionData = Blob.valueOf('test');
        cont.Origin = 'C';                
        insert cont;
        
     /*   ContentDistribution newDistribution = new ContentDistribution(
            ContentVersionId = cont.id, Name = 'External Link',
            PreferencesNotifyOnVisit = false);
        insert newDistribution;
        
        ContentDistribution distributionURL = [SELECT Id, ContentDownloadUrl
                                               FROM ContentDistribution 
                                               WHERE Id =: newDistribution.ID
                                               order BY LastModifiedDate DESC];
        System.Debug (distributionURL.ContentDownloadUrl);
        return distributionURL.ContentDownloadUrl;*/
        
        return cont.id;
    }
    
    
    // To filter the Units/Inventories
    public void initFilterDetails(){
        
        list<inventory__c> invS = new list<inventory__c>();
        invS  = [select id,Floor_Package_Name__c,Selling_Price_excl_VAT__c ,property_city__c,Bedroom_Type__c,Property_Status__c,Unit_Type__c,Unit__c,property_name__c,Anticipated_Completion_Date__c,View_Type__c,District__c,Marketing_Name_Doc__r.name,Marketing_Name_Doc__c from inventory__c where status__c='Released' ORDER BY ACD_Date__c asc LIMIT 10000 ];
        
        if(invS.size() > 0){
            for(inventory__c inv:invs){
                if(inv.Marketing_Name_Doc__c != null){
                    productname_idmap.put(inv.Marketing_Name_Doc__r.name,inv.Marketing_Name_Doc__c);  
                }
                if(inv.property_city__c != null){
                    cities.add(inv.property_city__c.toUpperCase());
                }
                if(inv.Property_Status__c != null){
                    propStatuses.add(inv.Property_Status__c.toUpperCase());
                }
                if(inv.Unit_Type__c != null){
                    unitTypes.add(inv.Unit_Type__c.toUpperCase());
                }
                if(inv.Anticipated_Completion_Date__c !=null){
                    acd.add(inv.Anticipated_Completion_Date__c.toUpperCase());
                }
                if(inv.Bedroom_Type__c !=null){
                    bedrooms.add(inv.Bedroom_Type__c );
                }
                if(inv.View_Type__c != null){
                    viewTypes.add(inv.View_Type__c);
                }
                if(inv.District__c != null){
                    district.add(inv.District__c);
                }
            }
        }
        propertyTypeWithImagesmtd = new Map<String,String>();
        for(ImageBasedOnPropertyType__mdt propertymtd : [SELECT MasterLabel,ImagePath__c FROM ImageBasedOnPropertyType__mdt]){
            if(!propertyTypeWithImagesmtd.containsKey(propertymtd.MasterLabel))
                propertyTypeWithImagesmtd.put(propertymtd.MasterLabel,propertymtd.ImagePath__c);
        }
    }
    
    public List<String> getLocation(){
        List<String> districtlst = new List<String>();
        districtlst.addAll(district);
        districtlst.sort();
        return districtlst;
    }
    public List<String> getCities() {
        List<String> citiessort = new List<String>();
        citiessort.addall(cities);
        citiessort.sort();
        return citiessort;
    }
    public List<String> getUnitTypes() {
        List<String> unittypesort = new List<String>();
        unittypesort.addall(unitTypes);
        unittypesort.sort(); 
        return unittypesort;
    }
    public List<String> getBedRooms() {
        List<String> bedroomsort = new List<String>();
        bedroomsort.addall(bedrooms);
        bedroomsort.sort();
        return bedroomsort;
    }
    public List<String> getViewTypes() {
        List<String> viewtypesort = new List<String>();
        viewtypesort.addall(viewTypes);
        viewtypesort.sort();
        return viewtypesort;
    }
       
    // To display the Inventories
    public void retrieveInventories(){
        inventories = new List <Inventory__c> ();
        
         
            
        String query = 'SELECT Unit_Catagory__c, Unit_Id__c, Marketing_Name_Doc__r.Name, '
            +'Unit_Plan__c, Floor_Plan__c, Marketing_Name_Doc__r.Brochure_URL__c,'
            +'Property_Name__c, Unit_Name__c, Bedroom_Type__c,IPMS_Bedrooms__c, '
            +'Unit_Plan_SF_Link__c,Floor_Plan_SF_Link__c, Plot_Plan_SF_Link__c, '
            +'convertCurrency(Total_Price_inc_VAT__c), Property_Status__c, Area_sft__c, '                          
            +'Marketing_Plan__c, Property_Country__c, Property_City__c, '
            +'Is_Focus__c, Is_New__c,view_type__c, Price_Per_Sqft__c  FROM inventory__c ';
        Set <Id> excludeInvIds = new Set <Id> ();
        
        List <Inventory_User__c> otherUserinvUnit = new List <Inventory_User__c>();
        List <Inventory_User__c> thisUserInvUnit = new List <Inventory_User__c>();

        thisUserInvUnit = [SELECT inventory__c FROM inventory_user__c 
                            WHERE user__c =: userinfo.getuserid() 
                            AND inventory__r.Tagged_To_Unit_Assignment__c = false 
                            AND inventory__r.status__c='Released'];
                            
        otherUserinvUnit = [SELECT inventory__c FROM inventory_user__c 
                            WHERE User__c !=: userinfo.getuserid() 
                            AND inventory__r.Tagged_To_Unit_Assignment__c = false 
                            AND inventory__r.status__c='Released' ];

        Set <Id> thisPcinvIdsUnit = new Set <Id>();
        if (thisUserInvUnit.size() > 0) {
            for (inventory_user__c thisPcInv:thisUserInvUnit){
                thisPcinvIdsUnit.add(thisPcInv.inventory__c);
            }
        }

        Set <Id> otherInventoryIdsUnit = new set<Id>();
        for (inventory_user__c invUs:otherUserinvUnit){
            if (thisPcinvIdsUnit.size()>0){
                if (!thisPcinvIdsUnit.contains(invUs.inventory__c)){
                    excludeInvIds.add(invUs.inventory__c);
                }
            }else{
                excludeInvIds.add(invUs.inventory__c);
            }
        }
        query += ' WHERE Status__c = \'Released\' AND Marketing_Name_Doc__c != null '                                                     
                +' AND Id not IN:excludeInvIds AND Tagged_To_Unit_Assignment__c = FALSE AND ';
                
        List<String> selectedPropTypes1 = selectedPropTypes != 'null'?selectedPropTypes.split(','):new List<String>{};
        List<String> selectedDistrict1 = selectedDistrict != 'null'? selectedDistrict.split(',') : new List<String>{} ;
        List<String> selectedCity1 = selectedCity != 'null' ? selectedCity.split(',') : new List<String>{};
        List<String> selectedStatus1 = selectedStatus != 'null' ? selectedStatus.split(',') : new List<String>{};
        List<String> selectedAcd1 = selectedAcd != 'null' ? selectedAcd.split(',') : new List<String>{} ;
        List<String> selectedMarketNames1 = selectedMarketNames != 'null' ? selectedMarketNames.split(',') : new List<String>{} ;
        List<String> selectedUnitTypes1 = selectedUnitTypes != 'null' ? selectedUnitTypes.split(',') : new List<String>{};
        List<String> selectedRooms1 = selectedRooms != 'null' ? selectedRooms.split(',') : new List<String>{} ;
        List<String> selectedViewTypes1 = selectedViewTypes != 'null' ? selectedViewTypes.split(',') : new List<String>{};
            
        Integer minAreaVal = Integer.valueOf(minArea);
        Integer maxAreaVal = Integer.valueOf(maxArea);
        Integer minPriceVal = Integer.valueOf(minPrice);
        Integer maxPriceVal = Integer.valueOf(maxPrice);
        Integer minPricePerSqVal = Integer.valueOf(minPricePerSq);
        Integer maxPricePerSqVal = Integer.valueOf(maxPricePerSq);
        System.debug('minPriceVal   '+minPriceVal);            
        System.debug('maxPriceVal   '+maxPriceVal);
        
        String returnQueryPart = createQueryString(selectedPropTypes1,selectedDistrict1,selectedCity1,
                                                   selectedStatus1,selectedAcd1,selectedMarketNames1,
                                                   selectedUnitTypes1,selectedRooms1,selectedViewTypes1,
                                                   minAreaVal,maxAreaVal,minPriceVal,maxPriceVal,minPricePerSqVal,maxPricePerSqVal);
        if (returnQueryPart != '')
            query += returnQueryPart;
        System.debug (query);
        inventories = database.query(query);
        inventoryCount = inventories.size ();
        Set <ID> invIds = new Set <ID> ();
        for (Inventory__c invn: inventories) {
            invIds.add (invn.Id);
        }
        for (Agent_Favorite_Inventory__c inv : [SELECT Inventory__c FROM Agent_Favorite_Inventory__c WHERE User__c =: UserInfo.getUserId() AND Inventory__c IN: invIds]) 
        {
            favoriteInventories.add (inv.Inventory__c );
        }
        
    }
    
    // TO prepare the query based on the search filter result
    public String createQueryString(List<String> selectedPropTypes1,List<String> selectedDistrict1,List<String> selectedCity1,
                                        List<String> selectedStatus1,List<String> selectedAcd1,List<String> selectedMarketNames1,
                                        List<String> selectedUnitTypes1,List<String> selectedRooms1,List<String> selectedViewTypes1,
                                        Integer minAreaVal,Integer maxAreaVal,Integer minPriceVal,Integer maxPriceVal,
                                        Integer minPricePerSqVal,Integer maxPricePerSqVal){
        String queryString = '';
        
        if( !selectedPropTypes1.isEmpty()  || !selectedDistrict1.isEmpty() || !selectedCity1.isEmpty() || !selectedStatus1.isEmpty() || !selectedAcd1.isEmpty() || !selectedMarketNames1.isEmpty() || !selectedUnitTypes1.isEmpty() || !selectedRooms1.isEmpty()  || !selectedViewTypes1.isEmpty() 
            || minAreaVal != NULL || maxAreaVal != NULL || minPriceVal != NULL || maxPriceVal != NULL || minPricePerSqVal != NULL || maxPricePerSqVal != NULL){
            
            if(selectedPropTypes1 != null && !selectedPropTypes1.isEmpty() ){
                queryString += ' Property_Type__c IN: selectedPropTypes1 AND ';
            }
            if(selectedMarketNames1 != null && !selectedMarketNames1.isEmpty() ){
                queryString += ' Marketing_Name_Doc__r.id IN: selectedMarketNames1 AND';
            }
            if(selectedStatus1 != null && !selectedStatus1.isEmpty()){
                queryString += ' Property_Status__c IN: selectedStatus1 AND ';
            }
            if(selectedUnitTypes1 != null && !selectedUnitTypes1.isEmpty()){
                queryString += ' unit_type__c IN: selectedUnitTypes1 AND ';
            }
            if(selectedCity1 != null && !selectedCity1.isEmpty()){
                queryString += ' Property_city__c IN: selectedCity1 AND ';
            }
            if(selectedAcd1 != null && !selectedAcd1.isEmpty()){
                queryString += ' Anticipated_Completion_Date__c IN: selectedAcd1 AND ';
            }
            if(selectedRooms1 != null && !selectedRooms1.isEmpty()){
                queryString += ' Bedroom_Type__c IN: selectedRooms1 AND ';
            }
            if(minAreaVal != null && minAreaVal >0){
                queryString += ' Area_sft__c >=:minAreaVal AND ';
            }
            if(maxAreaVal != null && maxAreaVal >0){
                queryString += ' Area_sft__c <=: maxAreaVal AND ';
            }
            // Price/Sq Ft Filters
            if(minPricePerSqVal != null && minPricePerSqVal >0){
                queryString += ' Price_Per_Sqft__c >=: minPricePerSqVal AND ';
            }
            if(maxPricePerSqVal != null && maxPricePerSqVal >0){
                queryString += ' Price_Per_Sqft__c <=: maxPricePerSqVal AND ';
            }
            //Price filters
            if(minPriceVal != null && minPriceVal >0){
                queryString += ' Special_Price__c>=:minPriceVal AND ';
            }
            if(maxPriceVal != null && maxPriceVal >0){
                queryString += ' Special_Price__c<=:maxPriceVal AND ';
            }
            if(selectedViewTypes1 != null && !selectedViewTypes1.isEmpty()){
                queryString += ' View_Type__c IN: selectedViewTypes1 AND ';
            }
            if(selectedDistrict1 != null && !selectedDistrict1.isEmpty()){
                queryString += ' District__c IN: selectedDistrict1 AND ';
            }
        }
        if(queryString != ''){
            queryString = queryString.removeEnd('AND ');
        }
        return queryString;
    }
    
    public void initProjects () {
        getProjects ();
    }  
    // To display the projects when projects tab is loaded
    public void initProjectDetails () {
        projInformation = new List <Project_Information__c> ();
        getProjects();
        featureURLS = new Map <String, AdditionalFeatures__c> ();
        for (AdditionalFeatures__c feature : [SELECT URL__c, Label__c FROM AdditionalFeatures__c WHERE URL__c != NULL]) {
            featureURLS.put (feature.Label__c, feature);
        }
        
        ID projId = apexpages.currentPage().getparameters().get('projId');
        projInformation = getProjectInfo (new List<String> {'Gallery'},projId);
        List <String> features = new List<String> ();
        features.add('Project Features');
        features.add('About');
        facilites = new List <String> ();    
        for (Project_Information__c facility : getProjectInfo (features,projId)) {
            String feature = facility.Project_Features__c;
            if (feature != null) {
                if (feature.contains (';'))
                    facilites = feature.split (';');
                else
                    facilites.add (feature);
            }
        }
    }
    
    // TO load all units when units tab is pressed
    public void initUnits () {
        productname_idmap = new Map<String,Id>();
        cities = new set<string>();
        propStatuses = new set<string>();
        unitTypes = new set<string>();
        acd = new set<string>();
        bedrooms = new set<string>();
        viewTypes = new set<string>();
        district =new set<String>();  
        minArea = '0';
        maxArea = '0';
        minPrice = '0';
        maxPrice = '0';
        minPricePerSq = '0';
        maxPricePerSq = '100000';
            
        ID projId = apexpages.currentpage().getparameters().get('projId');
        
        if (projId == null){
            getInventories ('All');
        } else {
            getProjects ();
            
        }
    }
    
    public void loadFavorites () {
        getInventories ('Favorites');
    }
    
    public void loadUnits () {
        String type = apexpages.currentpage().getparameters().get('type');
        getInventories (type);
    }
    
    public List <Project_Information__c> getProjectInfo (List <String> features , Id projId) {
        String query = 'SELECT '+getAllFields ('Project_Information__c')
                       +' FROM Project_Information__c '
                       + 'WHERE Features__c IN:features AND Marketing_Documents__c = :projId ';
                       
        if (features.contains ('Project Features')) {
            query += ' LIMIT 1';
        }
        return Database.query (query);
    }
    // Utility method to retrive all field apis related to the object name
    public static string getAllFields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);        
        if (objectType == null)
            return fields;        
        Map <String, Schema.SObjectField> fieldMap = objectType.getDescribe ().fields.getMap ();        
        for (Schema.SObjectField sfield : fieldMap.Values()) {            
            fields += sfield.getDescribe().getName ()+ ', ';           
        }
        return fields.removeEnd(', '); 
    }
    
    // TO get all projects
    public void getProjects () {
        inspectionReportURL = ''; longitude = ''; latitude = '';
        String locName = apexpages.currentPage().getparameters().get('locName');
        String docName = apexpages.currentPage().getparameters().get('docName');
        ID projId = apexpages.currentPage().getparameters().get('projId');
        projectsList = new List <WrapperForMarketing> ();
        marketingLocations = new Map <String, Integer> ();
        List <Inventory__c> ReleasedUnits = [SELECT Marketing_Name_Doc__c FROM Inventory__c
                                             WHERE Is_Assigned__c = false AND status__c='Released'];
        List <Inventory_user__c> activeInvUser = [SELECT Inventory__c FROM Inventory_user__c 
                                                  WHERE inventory__c IN :ReleasedUnits];
        Set <Id> excludeInvIds = new Set <Id> ();
        for (inventory_user__c i:activeInvUser) {
            excludeInvIds.add (i.inventory__c);
        }
        Set <Id> marketingDocIds = new Set <Id>();
        for (Inventory__c inv :ReleasedUnits){
            marketingDocIds.add (inv.Marketing_Name_Doc__c);
        }
        String query = 'SELECT Name, Marketing_Plan__c, '
            +' (SELECT Property_Country__c, Property_City__c, district__c, Inspection_URL__c, property__r.Longitude__c, property__r.Latitude__c  '
            +' FROM Inventories__r '
            +' WHERE status__c=\'Released\'' 
            +' AND ID NOT IN: excludeInvIds '
            +' AND Is_Assigned__c = false ';
        if (locName != '' && locName != 'All Locations' && locName != null) {
            query += ' AND Property_Country__c =: locName ';
        }
        
        query +=' ORDER BY Unit_Released_Date_from_IPMS__c desc NULLS LAST ) '
            +' FROM Marketing_Documents__c '
            +' WHERE id in:marketingDocIds ';
        
        if (docName != '' && docName != null) {
            docName = '%'+docName+'%';
            query += ' AND Name LIKE : docName';
        }
        if (projId != null) {
            query += ' AND ID =: projId ';
        }
        
        query += ' ORDER BY Name desc, '
            +' Marketing_plan__c NULLS LAST';
        
        Integer totalCount = 0;
        for (Marketing_Documents__c proj :Database.query (query)){
            if (proj.inventories__r.size () > 0) {
                wrapperForMarketing wrap = new wrapperForMarketing();               
                wrap.location = proj.inventories__r[0].district__c;
                wrap.city = proj.inventories__r[0].Property_City__c;
                wrap.country = proj.inventories__r[0].Property_Country__c; 
                wrap.marketingImage = proj.Marketing_plan__c;
                wrap.marketingName = proj.Name; 
                if (proj.inventories__r[0].property__r.Latitude__c != null)
                    latitude = ''+proj.inventories__r[0].property__r.Latitude__c ;
                if (proj.inventories__r[0].property__r.Longitude__c != null)
                    longitude = ''+proj.inventories__r[0].property__r.Longitude__c;
                wrap.projId = proj.Id;
                inspectionReportURL = proj.inventories__r[0].Inspection_URL__c;
                wrap.avaliableUnitsCount = proj.inventories__r.size();
                totalCount += wrap.avaliableUnitsCount;
                projectsList.add (wrap);
                
                
                if (marketingLocations.containsKey (proj.inventories__r[0].Property_Country__c))
                    marketingLocations.put (proj.inventories__r[0].Property_Country__c, 
                                            marketingLocations.get (proj.inventories__r[0].Property_Country__c) 
                                            + proj.inventories__r.size());
                else
                    marketingLocations.put (proj.inventories__r[0].Property_Country__c, proj.inventories__r.size());
            }
        }
        marketingLocations.put ('All Locations', totalCount);
        if (projId != null) {
            getInventories ('projId');
        }
    }
    
    public class wrapperForMarketing{
        
        public String location { get; set; }
        public String city { get; set; }
        public String country { get; set; }
        Public String marketingImage { get; set; }
        Public String marketingName { get; set; }
        Public Integer avaliableUnitsCount { get; set; }
        Public ID projId { get; set; }
        
        public wrapperForMarketing () {
            location = '';
            city = '';
            country = '';
            marketingName = '';
            marketingImage = '';
            avaliableUnitsCount = 0;
            projId = null;
            
        }
    }
    
    @remoteAction
    global static void addOrRemoveFavorite (ID inventoryId, Boolean isFavorite) {
        if (isFavorite) {
            Agent_Favorite_Inventory__c favoriteInventory = new Agent_Favorite_Inventory__c();
            favoriteInventory.User__c = UserInfo.getUserID ();
            favoriteInventory.Inventory__c = inventoryId;
            favoriteInventory.External_Id__c = UserInfo.getUserID ()+ ''+inventoryId;
            upsert favoriteInventory External_Id__c;
            
        } else {
            Set<String> deleteInventoryIdSet = new Set<String>();
            deleteInventoryIdSet.add (inventoryId);
            try {
                delete [SELECT Id FROM Agent_Favorite_Inventory__c WHERE User__c = :UserInfo.getUserID() AND Inventory__c IN :deleteInventoryIdSet];
            } catch (Exception e) {}
        }

    }
    
    public void getInventories(String type) {
        favoriteInventories = new Set <ID> ();
        inventories = new List <inventory__c> ();
        
        Set <Id> excludeInvIds = new Set <Id> ();
        
        List <Inventory_User__c> otherUserinvUnit = new List <Inventory_User__c>();
        List <Inventory_User__c> thisUserInvUnit = new List <Inventory_User__c>();

        thisUserInvUnit = [SELECT inventory__c FROM inventory_user__c 
                            WHERE user__c =: userinfo.getuserid() 
                            AND inventory__r.Tagged_To_Unit_Assignment__c = false 
                            AND inventory__r.status__c='Released'];
                            
        otherUserinvUnit = [SELECT inventory__c FROM inventory_user__c 
                            WHERE User__c !=: userinfo.getuserid() 
                            AND inventory__r.Tagged_To_Unit_Assignment__c = false 
                            AND inventory__r.status__c='Released' ];

        Set <Id> thisPcinvIdsUnit = new Set <Id>();
        if (thisUserInvUnit.size() > 0) {
            for (inventory_user__c thisPcInv:thisUserInvUnit){
                thisPcinvIdsUnit.add(thisPcInv.inventory__c);
            }
        }

        Set <Id> otherInventoryIdsUnit = new set<Id>();
        for (inventory_user__c invUs:otherUserinvUnit){
            if (thisPcinvIdsUnit.size()>0){
                if (!thisPcinvIdsUnit.contains(invUs.inventory__c)){
                    excludeInvIds.add(invUs.inventory__c);
                }
            }else{
                excludeInvIds.add(invUs.inventory__c);
            }
        }
        
        String countQuery = 'SELECT Count() '+'FROM inventory__c '
                            +'WHERE status__c=\'Released\''
                            //+' AND Is_Assigned__c = false '
                            +' AND Property_Type__c != \'Storage\' ';
        
        String query = 'SELECT '
            +'Unit_Catagory__c, Unit_Id__c, Marketing_Name_Doc__r.Name, Unit_Plan__c, Floor_Plan__c, Marketing_Name_Doc__r.Brochure_URL__c,'
            +'Property_Name__c, Unit_Name__c, Bedroom_Type__c,IPMS_Bedrooms__c, '
            +'Unit_Plan_SF_Link__c,Floor_Plan_SF_Link__c, Plot_Plan_SF_Link__c, '
            +'convertCurrency(Total_Price_inc_VAT__c), Property_Status__c, Area_sft__c, '                          
            +'Marketing_Plan__c, Property_Country__c, Property_City__c, '
            +'Is_Focus__c, Is_New__c,view_type__c, Price_Per_Sqft__c '
            +'FROM inventory__c '
            +'WHERE status__c=\'Released\''
            //+' AND Is_Assigned__c = false '
            +' AND Tagged_To_Unit_Assignment__c = FALSE '
            +' AND Property_Type__c != \'Storage\' ';
        if (type == 'All'  || type == 'LoadAll') {
            query += ' AND IS_New__c = FALSE AND IS_Focus__c = FALSE ';
            countQuery += ' AND IS_New__c = FALSE AND IS_Focus__c = FALSE ';
        }
        
        if (type == 'Popular') {
            query += ' AND IS_Focus__c = TRUE ';
            countQuery += ' AND IS_Focus__c = TRUE ';
        }
        
        if (type == 'New') {
            query += ' AND IS_NEW__C = TRUE ';
            countQuery += ' AND IS_NEW__C = TRUE ';
        }
        
        if (type == 'projId') {
            ID projId = apexpages.currentpage().getparameters().get('projId');
            query += ' AND Marketing_Name_Doc__c =: projId ';
        }
        
        if (type == 'Favorites') {
            Set<Id> inventoryIdSet = new Set<Id>();
            for (Agent_Favorite_Inventory__c favUnit : [SELECT Inventory__c FROM Agent_Favorite_Inventory__c WHERE User__c = :UserInfo.getUserID()]) {
                inventoryIdSet.add(favUnit.Inventory__c);
            }
            query += ' AND ID IN :inventoryIdSet ';
        }
        countQuery += ' AND Marketing_Name_Doc__c != null '                                                     
                    +' AND Id not IN:excludeInvIds ';
            
        if (type != 'LoadAll')
            inventoryCount = DataBase.countQuery (countQuery);
        
        query += ' AND Marketing_Name_Doc__c != null '                                                     
            +' AND Id not IN:excludeInvIds '
            +' Order BY Country_Ordering__c ASC, Unit_Released_Date_from_IPMS__c DESC, Marketing_Name__c,'
            +' Unit_Name__c, District__c, '
            +' Property_City__c, Price__c, Area_sft__c DESC ';
            
        
        if (type != 'LoadAll'){
            query += ' LIMIT 5';
        } else {
            query += ' LIMIT '+count * pageNumber;
        }
        System.debug (query);
        
        inventories = DataBase.query (query);
        System.debug (pageNumber);
        
        if (type == 'LoadAll')
            pageNumber = PageNumber + 1;
            
        Set <ID> invIds = new Set <ID> ();
        for (Inventory__c invn: inventories) {
            invIds.add (invn.Id);
        }
        for (Agent_Favorite_Inventory__c inv : [SELECT Inventory__c FROM Agent_Favorite_Inventory__c WHERE User__c =: UserInfo.getUserId() AND Inventory__c IN: invIds]) 
        {
            favoriteInventories.add (inv.Inventory__c);
        }
        
    }
  
    // To load units when it is searched in mobile
    public void searchInventory () {
        favoriteInventories = new Set <ID> ();
        inventories = new List <inventory__c> ();
        String searchTerm = apexpages.currentpage().getparameters().get('term');
        String type = apexpages.currentpage().getparameters().get('type');
        searchTerm = '%'+searchTerm +'%';
        
        Set <Id> excludeInvIds = new Set <Id> ();
        
        List <Inventory_User__c> otherUserinvUnit = new List <Inventory_User__c>();
        List <Inventory_User__c> thisUserInvUnit = new List <Inventory_User__c>();

        thisUserInvUnit = [SELECT inventory__c FROM inventory_user__c 
                            WHERE user__c =: userinfo.getuserid() 
                            AND inventory__r.Tagged_To_Unit_Assignment__c = false 
                            AND inventory__r.status__c='Released'];
                            
        otherUserinvUnit = [SELECT inventory__c FROM inventory_user__c 
                            WHERE User__c !=: userinfo.getuserid() 
                            AND inventory__r.Tagged_To_Unit_Assignment__c = false 
                            AND inventory__r.status__c='Released' ];

        Set <Id> thisPcinvIdsUnit = new Set <Id>();
        if (thisUserInvUnit.size() > 0) {
            for (inventory_user__c thisPcInv:thisUserInvUnit){
                thisPcinvIdsUnit.add(thisPcInv.inventory__c);
            }
        }

        Set <Id> otherInventoryIdsUnit = new set<Id>();
        for (inventory_user__c invUs:otherUserinvUnit){
            if (thisPcinvIdsUnit.size()>0){
                if (!thisPcinvIdsUnit.contains(invUs.inventory__c)){
                    excludeInvIds.add(invUs.inventory__c);
                }
            }else{
                excludeInvIds.add(invUs.inventory__c);
            }
        }
        
        String query = 'SELECT '
             +'Unit_Catagory__c, Unit_Id__c, Marketing_Name_Doc__r.Name, Unit_Plan__c, Floor_Plan__c, Marketing_Name_Doc__r.Brochure_URL__c,'
            +'Property_Name__c, Unit_Name__c, Bedroom_Type__c,IPMS_Bedrooms__c, '
            +'Unit_Plan_SF_Link__c,Floor_Plan_SF_Link__c, Plot_Plan_SF_Link__c, '
            +'convertCurrency(Total_Price_inc_VAT__c), Property_Status__c, Area_sft__c, '                          
            +'Marketing_Plan__c, Property_Country__c, Property_City__c, '
            +'Is_Focus__c, Is_New__c,view_type__c, Price_Per_Sqft__c '
            +' FROM inventory__c '
            +' WHERE status__c=\'Released\' '
            //+' AND Is_Assigned__c = false '
            +' AND Tagged_To_Unit_Assignment__c = FALSE '
            +' AND Property_Type__c != \'Storage\' AND Marketing_Name_Doc__c != null '
            +' AND (Unit_Name__c LIKE : searchTerm '
            +' OR Marketing_Name__c LIKE : searchTerm) '
            +' AND Id not IN :excludeInvIds ';
        if (type == 'All')
            query += ' AND IS_New__c = FALSE AND IS_Focus__c = FALSE ';
        
        if (type == 'Popular')
            query += ' AND IS_Focus__c = TRUE ';
        
        if (type == 'New')
            query += ' AND IS_NEW__C = TRUE ';
        
        query += ' AND Marketing_Name_Doc__c != null '                                                     
            +' AND Id not IN:excludeInvIds '
            +' Order BY Country_Ordering__c ASC, Unit_Released_Date_from_IPMS__c DESC, Marketing_Name__c, '
            +' Unit_Name__c, District__c, '
            +' Property_City__c, Price__c, Area_sft__c DESC ';
        
        inventories = Database.query (query);
        inventoryCount = inventories.size ();
        Set <ID> invIds = new Set <ID> ();
        for (Inventory__c invn: inventories) {
            invIds.add (invn.Id);
        }
        for (Agent_Favorite_Inventory__c inv : [SELECT Inventory__c FROM Agent_Favorite_Inventory__c WHERE User__c =: UserInfo.getUserId() AND Inventory__c IN: invIds]) 
        {
            favoriteInventories.add (inv.Inventory__c );
        } 
        
    }
}