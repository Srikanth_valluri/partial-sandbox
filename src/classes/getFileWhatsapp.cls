public without sharing class getFileWhatsapp {
    public String fileId{get;set;}
    public string url{get;set;}


    public getFileWhatsapp() {
        system.debug('<<<<<<<<< iun cons' +ApexPages.currentPage().getParameters().get('id'));
      fetchFileFromOffice365();
    }

    public Pagereference fetchFileFromOffice365(){
        fileId =String.isNotBlank(ApexPages.currentPage().getParameters().get('id'))?ApexPages.currentPage().getParameters().get('id'):'';
        System.debug(fileId);
        
        Office365RestService.ResponseWrapper objWrap = Office365RestService.downloadFromOffice365ByIdWhatsapp(fileId, '', '', '', '', '');
        system.debug('objWrap --'+ objWrap );
        if( objWrap != null){
            if( objWrap.Error_log_lst.size() == 0){
            url = String.isNotBlank(objWrap.downloadUrl)?objWrap.downloadUrl:'ERROR' ;
            system.debug(url );
            }else{
                if(objWrap.Error_log_lst != null)
                insert objWrap.Error_log_lst;
            }
        }
        
        
        
        return null;
    }
}