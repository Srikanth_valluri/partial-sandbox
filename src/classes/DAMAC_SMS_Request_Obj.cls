//
// Json object class for SMS Request
//

public class DAMAC_SMS_Request_Obj {

    //public class DAMAC_SMS_REQUEST_OBJ {
        public List<String> MobileNumbers;
        public String Message;
        public String SenderName;
        public String ScheduledDate;
        public Boolean RemoveDuplicates;
        public String ReferenceName;
        public Boolean ReturnIndividualResponse;
        public Boolean AsynchronousSubmission;
    //}

    
    public static DAMAC_SMS_REQUEST_OBJ parse(String json) {
        return (DAMAC_SMS_REQUEST_OBJ) System.JSON.deserialize(json, DAMAC_SMS_REQUEST_OBJ.class);
    }
    
    
}