@isTest
public class Damac_SweepProcess_Test {
    public static testMethod void method1 () {
        Campaign__c camp = New Campaign__c();
            camp.End_Date__c = system.today().addmonths(10);
            camp.Marketing_End_Date__c = system.today().addmonths(10);
            camp.Marketing_Start_Date__c = system.today().addmonths(-10);
            camp.Start_Date__c =  system.today().addmonths(-10);
        insert camp;
        Inquiry__c inq = new Inquiry__c ();
            inq.Activity_Counter__c =101;
            inq.Meeting_Due_Date__c = Date.Today().addDays(-65);
            inq.Inquiry_Status__c='Active';
            inq.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
            inq.campaign__c = camp.id;
            inq.Inquiry_Source__c = 'Customer Referral';
            inq.ownerId = userinfo.getUserId();
        insert inq;
        inq.Pre_InquiryId__c = inq.id;
        update inq;
        
        Damac_SweepProcess obj = new Damac_SweepProcess();
        DataBase.executeBatch(obj);
        
        Damac_SweepProcessSchedule sh1 = new Damac_SweepProcessSchedule();
        sh1.execute(null);  
        
    }
}