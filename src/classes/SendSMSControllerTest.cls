/*
* Description - Test class developed for 'SendSMSController'
*
* Version            Date            Author            Description
* 1.0                26/11/17        Arjun             Initial Draft
*/
@isTest
public class SendSMSControllerTest{
    @testSetup static void setup() {
        EmailTemplate et1 = new EmailTemplate (developerName = 'test_EmailTemplateName', IsActive=true, FolderId = UserInfo.getUserId(),TemplateType= 'text', Name = 'test'); // plus any other fields that you want to set
        insert et1;

    }
    static testMethod void testMethod1() {
        
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                        OnOffCheck__c = true);
            
            settingLst2.add(newSetting1);
            insert settingLst2;
        Id RecordTypeIdCollection = [
            SELECT Id
            FROM RecordType
            WHERE SObjectType='Calling_List__c' 
            AND DeveloperName='Collections_Calling_List'
            AND IsActive = TRUE LIMIT 1
        ].Id;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;

        List<Calling_List__c> lstCallingLists = createCallingList( RecordTypeIdCollection , 1 ,lstBookingUnits );
        insert lstCallingLists;

        ApexPages.StandardController controller = new ApexPages.standardController(lstCallingLists[0]);
               
        PageReference pageRef = Page.SendSMSPage;
        pageRef.getParameters().put('Id', String.valueOf(lstCallingLists[0].Id));
        Test.setCurrentPage(pageRef);

        SendSMSController obj = new SendSMSController(controller);
    }

    static testMethod void testMethod2() {
        
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                        OnOffCheck__c = true);
            
            settingLst2.add(newSetting1);
            insert settingLst2;
        Id RecordTypeIdCollection = [
            SELECT Id
            FROM RecordType
            WHERE SObjectType='Calling_List__c' 
            AND DeveloperName='Collections_Calling_List'
            AND IsActive = TRUE LIMIT 1
        ].Id;
        
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, Name = 'Test Name' ,First_Name__c='Test FirstName', Last_Name__c='Test LastName', Type='Other');
        insert objAcc;
        
        /*Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.PersonEmail = 'a@a.com'; 
        objAcc.Mobile_Phone_Encrypt__c = '243213424'; 
        objAcc.Mobile_Phone_2__c = '242343'; 
        objAcc.Mobile_Phone_3__c = '231123'; 
        objAcc.Mobile_Phone_4__c = '24234'; 
        objAcc.Mobile_Phone_5__c = '2342345235'; 
        insert objAcc;*/

        Contact objCon = new Contact(Lastname='Test Lastname',AccountId = objAcc.Id);
        objCon.Mobile_Phone__c = '009123121323';
        objCon.Mobile_Phone_2__c = '1213322323';
        objCon.Mobile_Phone_3__c = '12132323';
        objCon.Mobile_Phone_4__c = '121324323';
        objCon.Mobile_Phone_5__c = '12123323';
         
        insert objCon;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;

        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;

        List<Calling_List__c> lstCallingLists = createCallingList( RecordTypeIdCollection , 1 ,lstBookingUnits );
        insert lstCallingLists;
        
        ApexPages.StandardController controller = new ApexPages.standardController(lstCallingLists[0]);
               
        PageReference pageRef = Page.SendSMSPage;
        pageRef.getParameters().put('Id', String.valueOf(lstCallingLists[0].Id));
        Test.setCurrentPage(pageRef);

        SendSMSController obj = new SendSMSController(controller);
        obj.addElements();
        obj.removeElements();
        obj.callSMSService();
        List<SelectOption> SO = obj.getMyPersonalTemplateOptions();

        EmailTemplate et = [Select Id from EmailTemplate Where developerName = 'test_EmailTemplateName' Limit 1];
        obj.callingId = lstCallingLists[0].Id; 
        obj.selectedTemplateId = et.Id;
        obj.showContent();
    }
    
    static testMethod void testMethod3() {
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                        OnOffCheck__c = true);
            
            settingLst2.add(newSetting1);
            insert settingLst2;
        Id RecordTypeIdCollection = [
            SELECT Id
            FROM RecordType
            WHERE SObjectType='Calling_List__c' 
            AND DeveloperName='Collections_Calling_List'
            AND IsActive = TRUE LIMIT 1
        ].Id;
        
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;

        List<Calling_List__c> lstCallingLists = createCallingList( RecordTypeIdCollection , 1 ,lstBookingUnits );
        insert lstCallingLists;

        ApexPages.StandardController controller = new ApexPages.standardController(lstCallingLists[0]);
               
        PageReference pageRef = Page.SendSMSPage;
        pageRef.getParameters().put('Id', String.valueOf(lstCallingLists[0].Id));
        Test.setCurrentPage(pageRef);

        SendSMSController obj = new SendSMSController(controller);
        obj.addElements();
        obj.callingId = lstCallingLists[0].Id; 
        obj.templateBody = '';
        obj.callSMSService();

    }    
    
    /*
     @ Description : To create calling list reocord
     @ Return      : list of calling list to be created
    */
    public static List<Calling_List__c> createCallingList( Id RecordTypeIdCollection, Integer counter , List<Booking_Unit__c> lstBookingUnits ) {
        List<Calling_List__c> lstCallingLists = new List<Calling_List__c>();
        for( Integer i=0; i<counter; i++ ) {
            lstCallingLists.add(new Calling_List__c( 
                Registration_ID__c = lstBookingUnits[i].Registration_ID__c 
              , Inv_Due__c = 0, DM_Due_Amount__c = 0 
              , RecordTypeId = RecordTypeIdCollection
              , Mobile_Phone__c = '009123121323'
              , Mobile_Phone_2__c = '1213322323'
              , Mobile_Phone_3__c = '12132323'
              , Mobile_Phone_4__c = '121324323'
              , Mobile_Phone_5__c = '12123323'
              , Phone_1__c = '3545645324'
              , Phone_2__c = '3545645324'
              , Phone_3__c = '3545645342' ) );
        }
        return lstCallingLists;
    }    
}