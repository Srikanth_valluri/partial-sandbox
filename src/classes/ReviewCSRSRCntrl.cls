public class ReviewCSRSRCntrl {
    
    String strCaseId;
    public Map<Id,List<DetailWrapper>> mapId_Booking_Unit {get;set;}
    public List<SelectOption> statusOptions;
    public Map<Id,Id> mapBuInv;
    
    public ReviewCSRSRCntrl(ApexPages.StandardController controller) {
        mapId_Booking_Unit = new Map<Id,List<DetailWrapper>>();
        mapBuInv = new Map<Id,Id>();
        strCaseId = ApexPages.currentPage().getParameters().get('Id');
    }
    
    public void getBuInvDetails() {
        if( String.isNotBlank( strCaseId ) ) {
            
            Schema.DescribeFieldResult statusFieldDescription = Booking_Unit__c.Inventory_Sale_Classification__c.getDescribe();
            statusOptions = new list<SelectOption>();
            statusOptions.add(new SelectOption('--None--','--None--'));
            
            for (Schema.Picklistentry picklistEntry : statusFieldDescription.getPicklistValues())
            {
                statusOptions.add(new SelectOption(pickListEntry.getValue(),pickListEntry.getLabel()));
            }           
            
            for( SR_Booking_Unit__c objSRBU : [SELECT Id,Booking_Unit__c,Booking_Unit__r.Inventory__c
                                                    , Booking_Unit__r.Registration_ID__c
                                                    , Booking_Unit__r.Unit_Name__c
                                                    , Booking_Unit__r.Inventory__r.Bedroom_Type__c
                                                    , Booking_Unit__r.Unit_Type__c
                                                    , Booking_Unit__r.Inventory_Sale_Classification__c
                                                    , Booking_Unit__r.Inventory__r.Current_Saleable_Area__c
                                                    , Booking_Unit__r.Inventory__r.Current_AC_Area__c
                                                    , Booking_Unit__r.Inventory__r.Current_Upfront_Price__c
                                                    , Booking_Unit__r.Inventory__r.Current_Standard_Price__c
                                                    , Booking_Unit__r.Requested_Price__c
                                                    , Booking_Unit__r.Booking__r.Account__r.Country__pc
                                                    , Booking_Unit__r.Booking__r.Account__r.Country__c 
                                                    , Booking_Unit__r.Inventory__r.Area_sft__c
                                                    , Booking_Unit__r.Area__c
                                                    , Booking_Unit__r.Booking__r.Account__r.IsPersonAccount
                                                    , Booking_Unit__r.Price_Drop_on_Upfront_Price__c
                                                    , Booking_Unit__r.Price_Drop_on_Standard_Price__c
                                                    , Booking_Unit__r.Inventory__r.PSF_on_Current_Upfront_Price__c
                                                    , Booking_Unit__r.Inventory__r.PSF_on_Current_Standard_Price__c
                                                FROM SR_Booking_Unit__c
                                                WHERE Case__c =: strCaseId
                                                AND Booking_Unit__c != NULL
                                                AND Booking_Unit__r.Registration_ID__c != NULL]) {
                
                if( objSRBU.Booking_Unit__c != null && objSRBU.Booking_Unit__r.Inventory__c != null ) {
                    mapBuInv.put(objSRBU.Booking_Unit__c , objSRBU.Booking_Unit__r.Inventory__c);
                }
                String strType = objSRBU.Booking_Unit__r.Inventory__r.Bedroom_Type__c+' , '+objSRBU.Booking_Unit__r.Unit_Type__c;
                Decimal saleableArea = Case_Summary_Client_RelationController.getSaleableArea(objSRBU.Booking_Unit__r);
                String PSFonSoldArea = Case_Summary_Client_RelationController.doFormatting((objSRBU.Booking_Unit__r.Requested_Price__c) / (saleableArea) ,2,',','.' );
                if(mapId_Booking_Unit.containsKey(objSRBU.Booking_Unit__c) 
                    && mapId_Booking_Unit.get(objSRBU.Booking_Unit__c) != NULL 
                    &&  mapId_Booking_Unit.get(objSRBU.Booking_Unit__c).size() > 0){                    
                    List<DetailWrapper> lstBU = mapId_Booking_Unit.get(objSRBU.Booking_Unit__c);
                    lstBU.add(new DetailWrapper( objSRBU.Booking_Unit__c
                                               , objSRBU.Booking_Unit__r.Registration_ID__c
                                               , objSRBU.Booking_Unit__r.Unit_Name__c
                                               , strType
                                               , Case_Summary_Client_RelationController.doFormatting(objSRBU.Booking_Unit__r.Requested_Price__c,2,',','.')
                                               , Case_Summary_Client_RelationController.doFormatting(objSRBU.Booking_Unit__r.Area__c,2,',','.')
                                               , Case_Summary_Client_RelationController.doFormatting(objSRBU.Booking_Unit__r.Inventory__r.Area_sft__c,2,',','.')
                                               , objSRBU.Booking_Unit__r.Inventory_Sale_Classification__c
                                               , objSRBU.Booking_Unit__r.Inventory__r.Current_Saleable_Area__c
                                               , objSRBU.Booking_Unit__r.Inventory__r.Current_AC_Area__c
                                               , objSRBU.Booking_Unit__r.Inventory__r.Current_Upfront_Price__c
                                               , objSRBU.Booking_Unit__r.Inventory__r.Current_Standard_Price__c
                                               , PSFonSoldArea
                                               , Case_Summary_Client_RelationController.doFormatting(objSRBU.Booking_Unit__r.Inventory__r.Current_AC_Area__c,2,',','.')
                                               , objSRBU.Booking_Unit__r.Price_Drop_on_Upfront_Price__c
                                               , objSRBU.Booking_Unit__r.Price_Drop_on_Standard_Price__c
                                               , String.valueOf(Case_Summary_Client_RelationController.doFormatting(objSRBU.Booking_Unit__r.Inventory__r.PSF_on_Current_Upfront_Price__c,2,',','.'))
                                               , String.valueOf(Case_Summary_Client_RelationController.doFormatting(objSRBU.Booking_Unit__r.Inventory__r.PSF_on_Current_Standard_Price__c,2,',','.'))
                                               , statusOptions));
                    mapId_Booking_Unit.put(objSRBU.Booking_Unit__c,lstBU);
                } else {
                    List<DetailWrapper> lstBU = new List<DetailWrapper>();
                    lstBU.add(new DetailWrapper(objSRBU.Booking_Unit__c
                                               , objSRBU.Booking_Unit__r.Registration_ID__c
                                               , objSRBU.Booking_Unit__r.Unit_Name__c
                                               ,strType
                                               , Case_Summary_Client_RelationController.doFormatting(objSRBU.Booking_Unit__r.Requested_Price__c,2,',','.')
                                               , Case_Summary_Client_RelationController.doFormatting(objSRBU.Booking_Unit__r.Area__c,2,',','.')
                                               , Case_Summary_Client_RelationController.doFormatting(objSRBU.Booking_Unit__r.Inventory__r.Area_sft__c,2,',','.')
                                               , objSRBU.Booking_Unit__r.Inventory_Sale_Classification__c
                                                    , objSRBU.Booking_Unit__r.Inventory__r.Current_Saleable_Area__c
                                                    , objSRBU.Booking_Unit__r.Inventory__r.Current_AC_Area__c
                                                    , objSRBU.Booking_Unit__r.Inventory__r.Current_Upfront_Price__c
                                                    , objSRBU.Booking_Unit__r.Inventory__r.Current_Standard_Price__c
                                                    , PSFonSoldArea
                                                    , Case_Summary_Client_RelationController.doFormatting(objSRBU.Booking_Unit__r.Inventory__r.Current_AC_Area__c,2,',','.') 
                                                    , objSRBU.Booking_Unit__r.Price_Drop_on_Upfront_Price__c
                                                    , objSRBU.Booking_Unit__r.Price_Drop_on_Standard_Price__c
                                                    , String.valueOf(Case_Summary_Client_RelationController.doFormatting(objSRBU.Booking_Unit__r.Inventory__r.PSF_on_Current_Upfront_Price__c,2,',','.'))
                                                    , String.valueOf(Case_Summary_Client_RelationController.doFormatting(objSRBU.Booking_Unit__r.Inventory__r.PSF_on_Current_Standard_Price__c,2,',','.'))
                                                    , statusOptions));
                    mapId_Booking_Unit.put(objSRBU.Booking_Unit__c,lstBU);
                }
            }
        }
    }
    
    public PageReference saveData() {
        if( mapId_Booking_Unit != null && mapId_Booking_Unit.size() > 0 ) {
            List<Booking_Unit__c> lstBU = new List<Booking_Unit__c>();
            List<Inventory__c> lstInv = new List<Inventory__c>();
            for( Id objId : mapId_Booking_Unit.keySet() ) {
                //system.assert(false,' <<<<<<<<<< : ' + mapId_Booking_Unit.get(objId));
                for( DetailWrapper objSR_Booking_Unit :  mapId_Booking_Unit.get(objId) ) {
                    Booking_Unit__c objBU = new Booking_Unit__c();
                    objBU.Id =  objId;
                    if( String.isNotBlank(objSR_Booking_Unit.strInvSaleClass) 
                     && objSR_Booking_Unit.strInvSaleClass != '--None--') {
                        objBU.Inventory_Sale_Classification__c = objSR_Booking_Unit.strInvSaleClass;
                    }
                    if( mapBuInv.containsKey(objId) 
                    && mapBuInv.get(objId) != NULL  ) {
                        Inventory__c objInv = new Inventory__c();
                        objInv.Id = mapBuInv.get(objId);
                        if( objSR_Booking_Unit.strCurrSaleArea != null ) { 
                            objInv.Current_Saleable_Area__c = objSR_Booking_Unit.strCurrSaleArea;
                        }
                        if( objSR_Booking_Unit.strCurrACArea != null ) {
                            objInv.Current_AC_Area__c = objSR_Booking_Unit.strCurrACArea;
                        }
                        if( objSR_Booking_Unit.strCurrUpfrontPrice != null ) {
                            objInv.Current_Upfront_Price__c = objSR_Booking_Unit.strCurrUpfrontPrice;
                        }
                        if( objSR_Booking_Unit.strCurrStandardPrice != null ) {
                            objInv.Current_Standard_Price__c = objSR_Booking_Unit.strCurrStandardPrice;
                        }   
                        //objBU.Inventory__c = objInv.Id;
                        lstInv.add(objInv);
                    }
                    
                    lstBU.add(objBU);
                }
            }
            if( lstInv != null && lstInv.size() > 0 ) {
                update lstInv;
            }
            if( lstBU != null && lstBU.size() > 0 ) {
                update lstBU;
            }
        }
        PageReference pageRef = new PageReference('/'+strCaseId);
        pageRef.setRedirect(true);
        return pageRef;     
    }
    
    public class DetailWrapper {
        public Id strId         {get;set;}
        public String strRegId      {get;set;}
        public String strUnitName           {get;set;}
        public String type {get;set;}
        public String soldPrice {get;set;}
        public String soldArea {get;set;}   
        public String invArea {get;set;}        
        public String strInvSaleClass       {get;set;}
        public Decimal strCurrSaleArea      {get;set;}
        public Decimal strCurrACArea        {get;set;}
        public Decimal strCurrUpfrontPrice  {get;set;}
        public Decimal strCurrStandardPrice     {get;set;}
        public String PSFonSoldArea {get;set;}
        public String ACArea {get;set;}
        public Decimal priceDropUpFrontPrice {get;set;}
        public Decimal PriceDropStandardPrice {get;set;}
        public String PSFOnUpFrontPrice {get;set;}
        public String PSFOnStandardPpPrice {get;set;}
        
        
        public List<SelectOption> invSaleClassiOption {get;set;}
        
        public DetailWrapper( Id strId
                            , String strRegId
                            , String strUnitName
                            , String type
                            , String soldPrice
                            , String soldArea
                            , String invArea
                            , String strInvSaleClass
                            , Decimal strCurrSaleArea
                            , Decimal strCurrACArea
                            , Decimal strCurrUpfrontPrice
                            , Decimal strCurrStandardPrice
                            , String PSFonSoldArea
                            , String ACArea
                            , Decimal priceDropUpFrontPrice
                            , Decimal PriceDropStandardPrice
                            , String PSFOnUpFrontPrice
                            , String PSFOnStandardPpPrice
                            , List<SelectOption> invSaleClassiOption ) {
            this.strId = strId;
            this.strRegId = strRegId;
            this.strUnitName = strUnitName;
            this.type = type;
            this.soldPrice = soldPrice;
            this.soldArea = soldArea;
            this.invArea = invArea;
            this.strInvSaleClass = strInvSaleClass;
            this.strCurrSaleArea = strCurrSaleArea;
            this.strCurrACArea = strCurrACArea;
            this.strCurrUpfrontPrice = strCurrUpfrontPrice;
            this.strCurrStandardPrice = strCurrStandardPrice;
            this.PSFonSoldArea = PSFonSoldArea;
            this.ACArea = ACArea;
            this.priceDropUpFrontPrice = priceDropUpFrontPrice;
            this.PriceDropStandardPrice = PriceDropStandardPrice;
            this.PSFOnUpFrontPrice = PSFOnUpFrontPrice;
            this.PSFOnStandardPpPrice = PSFOnStandardPpPrice;
            this.invSaleClassiOption = invSaleClassiOption;
            
        }
    }
}