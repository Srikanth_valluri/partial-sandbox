/**
 * Description: Test class for ParkingArchivalTaskUtility
 */
@isTest
private class ParkingArchivalTaskUtilityTest {

    /**
     * Method to test the archival task creation
     */
    static testMethod void checkCreationOfArchivalTask() {

        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        Id parkingRecordTypeID = getRecordTypeIdForParking();

        // Creation of Case
        Case objCase = TestDataFactory_CRM.createCase(objAccount.Id , parkingRecordTypeID);
        insert objCase;

        objCase = [Select OwnerId FROM Case WHERE Id =:objCase.Id];

        Test.startTest();
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT());
            ParkingArchivalTaskUtility.createArchivalTask(new List<Case>{objCase});
        Test.stopTest();

        List<Task> lstTasks = [SELECT Id, Assigned_User__c, CurrencyIsoCode, OwnerId, Priority, Process_Name__c,
            Status, Subject, WhatId  FROM Task WHERE WhatId =: objCase.Id];
        System.assert(lstTasks != null);
        System.assert(lstTasks.size() > 0);
        System.assert(lstTasks[0].Assigned_User__c == 'CDC');
        System.assert(lstTasks[0].CurrencyIsoCode == 'AED');
        System.assert(lstTasks[0].OwnerId == objCase.OwnerId);
        System.assert(lstTasks[0].Priority == 'High');
        System.assert(lstTasks[0].Process_Name__c == 'Parking');
        System.assert(lstTasks[0].Status == 'Not Started');
        System.assert(lstTasks[0].Subject == 'Archival of Documents');
        System.assert(lstTasks[0].WhatId == objCase.Id);
    }

    /**
     * Method to check that no is task being created when any exception occurs
     */
    static testMethod void checkNoTaskCreationOnCase() {

        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        Id parkingRecordTypeID = getRecordTypeIdForParking();

        // Instantation of Case
        Case objCase = new Case();

        Test.startTest();
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT());
            ParkingArchivalTaskUtility.createArchivalTask(new List<Case>{objCase});
        Test.stopTest();

        List<Task> lstTasks = [SELECT Id, Assigned_User__c, CurrencyIsoCode, OwnerId, Priority, Process_Name__c,
            Status, Subject, WhatId  FROM Task WHERE WhatId =: objCase.Id];
        System.assert(lstTasks.size() == 0);
    }

    /**
     * Method to get the "Parking" record type
     */
    private static Id getRecordTypeIdForParking() {
      Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
      Id parkingRecordTypeID = caseRecordTypes.get('Parking').getRecordTypeId();
      return parkingRecordTypeID;
    }

}