public class CampaignDetailsController {
    public class CampaignDetails{
        @AuraEnabled
        public String campaignDescription;
        @AuraEnabled
        public List<ContentDocumentLink> files;
    }
    @AuraEnabled
    public static CampaignDetails getDetails(Id inqId) {
        Inquiry__c inq = new Inquiry__c();
        inq = [SELECT 
               Campaign__c, Campaign__r.Campaign_Description__c 
               FROM 
               Inquiry__c 
               WHERE 
               Id =: inqId];
        CampaignDetails obj = new CampaignDetails();
        obj.campaignDescription = inq.Campaign__r.Campaign_Description__c;
        
        obj.files = [SELECT 
                     LinkedEntityId, ContentDocumentId, Visibility, 
                     ContentDocument.Title
                     FROM 
                     ContentDocumentLink 
                     WHERE 
                     LinkedEntityId =: inq.Campaign__c];
        return obj;
    }

}