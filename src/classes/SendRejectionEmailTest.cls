@isTest
public class SendRejectionEmailTest {
    static testMethod void SendRejectionEmailTest1() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__c ='test@test.com';
        objAcc.Mobile_Phone_Encrypt__pc = '0097405883798';
        insert objAcc ;
         Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POP').getRecordTypeId();
        System.debug('recTypeCOD :'+ recTypeCOD);
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD);
        objCase.status='Rejected';
        insert objCase;
        Test.startTest();
        PageReference myVfPage = Page.SendRejectionEmailPage;
        Test.setCurrentPage(myVfPage);
        myVfPage.getParameters().put('id',objCase.Id);
        

        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        SendRejectionEmail objSendEmail = new SendRejectionEmail(sc);
         objSendEmail.getCase();
         objSendEmail.send();
        Test.stopTest();

    }
    
     static testMethod void SendRejectionEmailTest2() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__c ='test@test.com';
        objAcc.Mobile_Phone_Encrypt__pc = '0097405883798';
        insert objAcc ;
         Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POP').getRecordTypeId();
        System.debug('recTypeCOD :'+ recTypeCOD);
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD);
         objCase.status='New';
        insert objCase;
      Test.startTest();
        PageReference myVfPage = Page.SendRejectionEmailPage;
        Test.setCurrentPage(myVfPage);
        myVfPage.getParameters().put('id',objCase.Id);
        

        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        SendRejectionEmail objSendEmail = new SendRejectionEmail(sc);
        
         objSendEmail.send();
        Test.stopTest();

    }
}