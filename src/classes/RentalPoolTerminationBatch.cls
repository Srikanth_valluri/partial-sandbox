/*
* Description - 
*
* Version            Date            Author            Description
* 1.0              06/02/2018       Ashish Agarwal    Initial Draft
*/
global class RentalPoolTerminationBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Queueable {
    
    private String query;
    private static Id rentalPoolTerminationRTId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Rental Pool Termination').RecordTypeId;
    private map<Id, Id> mapBUIdCase = new map<Id, Id>();
    
    public RentalPoolTerminationBatch( ) {
      
    }
    
    public RentalPoolTerminationBatch( map<Id, Id> mapBUIdCase ) {
      this.mapBUIdCase = mapBUIdCase ;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        query = 'SELECT Id,'+
                        'Status,'+
                        'RecordTypeId,'+
                        'OwnerId,'+
                        'Rental_Pool_Termination_Status__c,'+
                        'Approval_Status__c,'+
                        'Termination_Date__c,'+
                        'CreatedDate,'+
                        'Manager_Id__c,'+
                        'Seller__c,'+
                        'CaseNumber,'+
                        'Booking_Unit__c '+
                   'FROM Case '+
                  'WHERE RecordTypeId = :rentalPoolTerminationRTId  '+
                    'AND Status = \'Closed\''+
                    'AND Booking_Unit__c != null '+
                    'AND Rental_Pool_Termination_Status__c = \'Letter of Termination Dispatched\''+
                    'AND Final_Termination_Date__c != null '    +
                    'AND Final_Termination_Date__c <= TODAY ' +
                    'AND Booking_Unit__r.Rental_Pool__c = true';
        return Database.getQueryLocator(query);
    }
    
    global void execute( Database.BatchableContext BC, List<Case> caseLst ) {
    system.debug(' caseLst : '+ caseLst);
        for( Case objCase : caseLst ){
            mapBUIdCase.put( objCase.Booking_Unit__c, objCase.Id );
        }
        
        if( !mapBUIdCase.isEmpty() ) {
          ID jobID = System.enqueueJob( new RentalPoolTerminationBatch( mapBUIdCase ) );
        }
    }
    
    global void finish( Database.BatchableContext BC ) {
        
    }
    
    global void execute( QueueableContext context ) {
      if( !test.isRunningTest() ) {
        CaseTriggerHandlerRPAssignment.removeOldUnitFromRentalPool( mapBUIdCase );        
      }
    }
}