/*-------------------------------------------------------------------------------------------------
Description: Scheduler for UpdateBouncedChequeCallingListBatch batch

    ============================================================================================================================
        Version | Date(DD-MM-YYYY) | Last Modified By | Comments
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 05-12-2017       | Lochana Rajput   | Added method to get Dues and Overdues using callout and update Calling List
   =============================================================================================================================
*/
public without sharing class BouncedChequeUtility {

    // @InvocableMethod
    // public static void closeCase(List<Task> lstTask) {
    //     Set<Id> callingListIds= new Set<Id>();
    //     List<Case> caseLStToUpdate = new List<Case>();
    //     String callingListPrefix = Calling_List__c.sobjecttype.getDescribe().getKeyPrefix();
    //     for(Task objTask : lstTask) {
    //         //If task is related to calling list
    //         String whatIdStr = objTask.WhatId;
    //         whatIdStr = whatIdStr.substring(0,3);
    //         if(whatIdStr.equalsIgnoreCase(callingListPrefix)) {
    //             callingListIds.add(objTask.WhatId);
    //         }
    //     }
    //     //Get cases to update
    //     for(Calling_List__c objCallList : [SELECT Case__c, Case__r.Status  FROM Calling_List__c WHERE ID IN: callingListIds]) {
    //         caseLStToUpdate.add(new Case(Status = 'Closed', Id=objCallList.Case__c));
    //     }//for
    //     if( !caseLStToUpdate.isEmpty()) {
    //         update caseLStToUpdate;
    //     }
    // }

    public static void updateCallingList(List<Calling_List__c> lstCallingList) {
        Decimal totalAmt;
        List<Case> lstCasesUpdate = new List<Case>();
        System.debug('===lstCallingList===' + lstCallingList);
        String caseId, BUId;
        List<Calling_List__c> callingListToUpdate = new List<Calling_List__c>();
        Map<Id, List<Id>> mapCase_lstCallingListIds = new Map<Id, List<Id>>();
        //Get Case Ids from Calling List
        for(Calling_List__c objCallingList : lstCallingList) {
            if(mapCase_lstCallingListIds.containsKey(objCallingList.Case__c)) {
                mapCase_lstCallingListIds.get(objCallingList.Case__c).add(objCallingList.Id);
            }
            else {
                mapCase_lstCallingListIds.put(objCallingList.Case__c, new Id[] {objCallingList.Id});
            }
        }//for end
        try{
            System.debug('=====mapCase_lstCallingListIds.keySet()===' + mapCase_lstCallingListIds.keySet());
            for(Case objCase : [SELECT ID,
                                (SELECT Booking_Unit__c, Case__c, Booking_Unit__r.Registration_ID__c
                                FROM SR_Booking_Units__r
                                WHERE Booking_Unit__r.Registration_ID__c != NULL)
                                 FROM Case
                                WHERE ID IN : mapCase_lstCallingListIds.keySet()
                                AND ID IN (SELECT Case__c FROM SR_Booking_Unit__C
                                WHERE Booking_Unit__r.Registration_ID__c != NULL)]) {
                totalAmt = null;
                caseId = objCase.Id;
                System.debug('====objCase====' + objCase.Id);
                for(SR_Booking_Unit__c objSRBU : objCase.SR_Booking_Units__r) {
                    String strResponse = assignmentEndpoints.fetchAssignmentDues(
                            new Booking_Unit__c(Registration_ID__c =  objSRBU.Booking_Unit__r.Registration_ID__c));
                    System.debug('====RESPONSE====' + strResponse);

                    Map<String,Object> apexmap = (Map<String, Object>) JSON.deserializeUntyped(strResponse);
                    system.debug('===Balance as per SOA===' + apexmap.get('Balance as per SOA'));
                    if(apexmap.containsKey('Balance as per SOA') && apexmap.get('Balance as per SOA') != NULL
                        && apexmap.get('Balance as per SOA') != '') {
                        totalAmt = totalAmt == null ? 0 : totalAmt;
                        totalAmt += Decimal.valueOf(String.valueOf(apexmap.get('Balance as per SOA')));
                    }
                }
                // if(totalAmt < Decimal.valueOf(Label.Dues_OverduesValue)) {
                //     for(Id objCLId : mapCase_lstCallingListIds.get(objCase.Id)) {
                //         callingListToUpdate.add(new Calling_List__c(Id = objCLId, Call_Outcome__c = 'Paid', Calling_List_Status__c = 'Closed'));
                //     }
                // }

                if(totalAmt != null) {
                    for(Id objCLId : mapCase_lstCallingListIds.get(objCase.Id)) {
                        objCase.Total_Dues__c = totalAmt;
    
                        Calling_List__c objCallList = new Calling_List__c(Id = objCLId);
                        objCallList.Total_Dues__c = totalAmt;
                        if(totalAmt < Decimal.valueOf(Label.Dues_OverduesValue)) {
                            objCallList.Call_Outcome__c = 'Paid';
                            objCallList.Calling_List_Status__c = 'Closed';
                            objCase.Status = 'Closed';
                        }
                        lstCasesUpdate.add(objCase);
                        callingListToUpdate.add(objCallList);
                    }
                }
            }//outer for

            if( ! callingListToUpdate.isEmpty()) {
                    update callingListToUpdate;
                    System.debug('====callingListToUpdate==' + callingListToUpdate);
            }
            if( ! lstCasesUpdate.isEmpty()) {
                    update lstCasesUpdate;
                    System.debug('====lstCasesUpdate==' + lstCasesUpdate);
            }
        }//try
        catch(System.Exception excp) {
            System.debug('====Exception==' + excp);
            Error_Log__c objError = new Error_Log__c();
            objError.Process_Name__c = 'Bounced Cheque';
            objError.Error_Details__c = excp.getMessage();
        }//try
    }
}