public without sharing class MeetingRoomAvailabilityCntrl {
    public List<String> lstOfTiming                         {get;set;}
    public List<String> lstOfMeetingRoom                    {get;set;}
    public Id callingListRecordTypeId                       {get;set;}
    public map<String,Calling_List__c> mapMeetingRoomCL     {get;set;}
    public String strCurrentTime                            {get;set;}
    public String bookingUnitName                           {get;set;}
    public String bookingUnitId                             {get;set;}
    public String accountName                               {get;set;}
    public String accountId                                 {get;set;}
    public String strCLId                                   {get;set;}
    public String strCLName                                 {get;set;}
    public List<Calling_List__c> lstExpressCl               {get;set;}
    private Id devRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Appointment Scheduling').getRecordTypeId();
    private static final String CONFIRMED = 'Confirmed';
    public List<AppointmentWrapper> lstAppointmentWrapper   {get;set;}
    public DateTime systemNowDate {get;set;}
    
    public MeetingRoomAvailabilityCntrl( ApexPages.StandardController controller ) {
        systemNowDate = system.now();

    }

    // JS Remoting action called when searching for a booking unit name
    @RemoteAction
    public static List<Booking_Unit__c> searchBookingUnit(String searchTerm) {
        System.debug('Booking unit Name is: '+searchTerm );
        List<Booking_Unit__c> listBookingUnit = Database.query('Select Id, Unit_Name__c from Booking_Unit__c where Unit_Name__c like \'%' + String.escapeSingleQuotes(searchTerm) + '%\'');
        return listBookingUnit;
    }
    
    // JS Remoting action called when searching for a account name
    @RemoteAction
    public static List<Account> searchAccount(String searchTerm) {
        System.debug('Account Name is: '+searchTerm );
        List<Account> listAcct = Database.query('Select Id, Name from Account where Name like \'%' + String.escapeSingleQuotes(searchTerm) + '%\'');
        return listAcct;
    }   
    
    // JS Remoting action called when searching for a calling list name
    @RemoteAction
    public static List<Calling_List__c> searchCallingList(String searchTerm) {
        System.debug('Calling_List Name is: '+searchTerm );
        List<Calling_List__c> listCL = Database.query('Select Id, Name from Calling_List__c where Name like \'%' + String.escapeSingleQuotes(searchTerm) + '%\'');
        return listCL;
    }
    
    public pageReference populateCLDetails(){
        lstAppointmentWrapper = new list<AppointmentWrapper>();
        //system.assert(false,' bookingUnitName  :  ' + bookingUnitName );
        String strQuery = 'SELECT Id,Service_Type__c,Express_Appointment__c,Sub_Purpose__c,Appointment_Start_DateTime__c,Assigned_CRE__c,Owner.Name,Name,Account__r.Name,Account__c,Booking_Unit_Name__c,Booking_Unit__c FROM Calling_List__c WHERE Appointment_Date__c = TODAY AND RecordTypeId =: devRecordTypeId AND Appointment_Status__c =:CONFIRMED ';
        if( String.isNotBlank(bookingUnitName) ) {
            strQuery += 'AND  Booking_Unit__r.Unit_Name__c =:bookingUnitName ';
        }
        if(String.isNotBlank(accountName)) {
            strQuery += 'AND  Account__r.Name =:accountName ';
        }
        if(String.isNotBlank(strCLName)) {
            strQuery += 'AND  Name =:strCLName ';
        }
        
        for( Calling_List__c objCalling : Database.query(strQuery) ) {
            lstAppointmentWrapper.add(new AppointmentWrapper(objCalling));
        }
        
        //system.assert(false,' lstAppointmentWrapper  :  ' + lstAppointmentWrapper );
        //lstExpressCl = Database.query(strQuery);
        //
        
        
        return null;
    }
    
    public void proceedWalkin() {
        if (lstAppointmentWrapper != null && lstAppointmentWrapper.size() > 0) {
            for (AppointmentWrapper objWrap : lstAppointmentWrapper) {
                if ( objWrap.isSelected == true ) {
                    strCLId = objWrap.objCL.Id;
                    strCLName = objWrap.objCL.Name;
                    bookingUnitName = objWrap.objCL.Booking_Unit_Name__c;
                    bookingUnitId = objWrap.objCL.Booking_Unit__c;
                    accountName = objWrap.objCL.Account__r.Name;
                    accountId = objWrap.objCL.Account__c;
                }
            }
        }
        //system.assert( false,'bookingUnitId : ' +bookingUnitId + '<<<<<<< : strCLId ' + strCLId);
        //init();
    }
    
    public void init() {
        mapMeetingRoomCL = new map<String,Calling_List__c>();
        callingListRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Walk In Calling List').RecordTypeId;
        lstOfMeetingRoom = new List<String>();
        for( Meeting_Rooms_Name__c objMeetingRoom : Meeting_Rooms_Name__c.getall().values()){
            //system.assert( false,'objMeetingRoom : ' +objMeetingRoom.Name );
            if ( String.isNotBlank( objMeetingRoom.Name ) ) {
                lstOfMeetingRoom.add(objMeetingRoom.Name);
            }
        }
        
        
        //lstOfTiming = new List<String>{'08:30-09:30','09:30-10:30','10:30-11:30','11:30-12:30','12:30-13:30','13:30-14:30','14:30-15:30','15:30-16:30','16:30-17:30','17:30-18:30','18:30-19:30','19:30-20:30','20:30-21:30'};
        lstOfTiming = new List<String>{'08:30-08:45','08:45-09:00','09:00-09:15','09:15-09:30','09:30-09:45','09:45-10:00','10:00-10:15','10:15-10:30','10:30-10:45','10:45-11:00','11:00-11:15','11:15-11:30','11:30-11:45','11:45-12:00','12:00-12:15','12:15-12:30','12:30-12:45','12:45-13:00','13:00-13:15','13:15-13:30','13:30-13:45','13:45-14:00','14:00-14:15','14:15-14:30','14:30-14:45','14:45-15:00','15:00-15:15','15:15-15:30','15:30-15:45','15:45-16:00','16:00-16:15','16:15-16:30','16:30-16:45','16:45-17:00','17:00-17:15','17:15-17:30','17:30-17:45','17:45-18:00','18:00-18:15','18:15-18:30'};
        //lstOfMeetingRoom = new List<String>{'M1','M2','M3','M4','M5','M6','M7','M8','M9','M10'};
        
        

            /*if( system.now().format('HH:mm') >= '08:30' && system.now().format('HH:mm') < '09:30' ) {
                strCurrentTime = '08:30-09:30';
            } else if( system.now().format('HH:mm') >= '09:30' && system.now().format('HH:mm') < '10:30' ) {
                strCurrentTime = '09:30-10:30';
            } else if( system.now().format('HH:mm') >= '10:30' && system.now().format('HH:mm') < '11:30' ) {
                strCurrentTime = '10:30-11:30';
            } else if( system.now().format('HH:mm') >= '11:30' && system.now().format('HH:mm') < '12:30' ) {
                strCurrentTime = '11:30-12:30';
            } else if( system.now().format('HH:mm') >= '12:30' && system.now().format('HH:mm') < '13:30' ) {
                strCurrentTime = '12:30-13:30';
            } else if( system.now().format('HH:mm') >= '13:30' && system.now().format('HH:mm') < '14:30' ) {
                strCurrentTime = '13:30-14:30';
            } else if( system.now().format('HH:mm') >= '14:30' && system.now().format('HH:mm') < '15:30' ) {
                strCurrentTime = '14:30-15:30';
            } else if( system.now().format('HH:mm') >= '15:30' && system.now().format('HH:mm') < '16:30' ) {
                strCurrentTime = '15:30-16:30';
            } else if( system.now().format('HH:mm') >= '16:30' && system.now().format('HH:mm') < '17:30' ) {
                strCurrentTime = '16:30-17:30';
            } else if( system.now().format('HH:mm') >= '17:30' && system.now().format('HH:mm') <= '18:30' ) {
                strCurrentTime = '17:30-18:30';
            } else if( system.now().format('HH:mm') >= '18:30' && system.now().format('HH:mm') <= '19:30' ) {
                strCurrentTime = '18:30-19:30';
            } else if( system.now().format('HH:mm') >= '19:30' && system.now().format('HH:mm') <= '20:30' ) {
                strCurrentTime = '19:30-20:30';
            }  else if( system.now().format('HH:mm') >= '20:30' && system.now().format('HH:mm') <= '21:30' ) {
                strCurrentTime = '20:30-21:30';
            }*/
            
            
            if( systemNowDate.format('HH:mm') >= '08:30' && systemNowDate.format('HH:mm') < '08:45' ) {
                strCurrentTime = '08:30-08:45';
            } else if( systemNowDate.format('HH:mm') >= '08:45' && systemNowDate.format('HH:mm') < '09:00' ) {
                strCurrentTime = '08:45-09:00';
            } else if( systemNowDate.format('HH:mm') >= '09:00' && systemNowDate.format('HH:mm') < '09:15' ) {
                strCurrentTime = '09:00-09:15';
            } else if( systemNowDate.format('HH:mm') >= '09:15' && systemNowDate.format('HH:mm') < '09:30' ) {
                strCurrentTime = '09:15-09:30';
            } else if( systemNowDate.format('HH:mm') >= '09:30' && systemNowDate.format('HH:mm') < '09:45' ) {
                strCurrentTime = '09:30-09:45';
            } else if( systemNowDate.format('HH:mm') >= '09:45' && systemNowDate.format('HH:mm') < '10:00' ) {
                strCurrentTime = '09:45-10:00';
            } else if( systemNowDate.format('HH:mm') >= '10:00' && systemNowDate.format('HH:mm') < '10:15' ) {
                strCurrentTime = '10:00-10:15';
            } else if( systemNowDate.format('HH:mm') >= '10:15' && systemNowDate.format('HH:mm') < '10:30' ) {
                strCurrentTime = '10:15-10:30';
            } else if( systemNowDate.format('HH:mm') >= '10:30' && systemNowDate.format('HH:mm') < '10:45' ) {
                strCurrentTime = '10:30-10:45';
            } else if( systemNowDate.format('HH:mm') >= '10:45' && systemNowDate.format('HH:mm') < '11:00' ) {
                strCurrentTime = '10:45-11:00';
            } else if( systemNowDate.format('HH:mm') >= '11:00' && systemNowDate.format('HH:mm') < '11:15' ) {
                strCurrentTime = '11:00-11:15';
            } else if( systemNowDate.format('HH:mm') >= '11:15' && systemNowDate.format('HH:mm') < '11:30' ) {
                strCurrentTime = '11:15-11:30';
            }  else if( systemNowDate.format('HH:mm') >= '11:30' && systemNowDate.format('HH:mm') < '11:45' ) {
                strCurrentTime = '11:30-11:45';
            }  else if( systemNowDate.format('HH:mm') >= '11:45' && systemNowDate.format('HH:mm') < '12:00' ) {
                strCurrentTime = '11:45-12:00';
            }  else if( systemNowDate.format('HH:mm') >= '12:00' && systemNowDate.format('HH:mm') < '12:15' ) {
                strCurrentTime = '12:00-12:15';
            }  else if( systemNowDate.format('HH:mm') >= '12:15' && systemNowDate.format('HH:mm') < '12:30' ) {
                strCurrentTime = '12:15-12:30';
            }  else if( systemNowDate.format('HH:mm') >= '12:30' && systemNowDate.format('HH:mm') < '12:45' ) {
                strCurrentTime = '12:30-12:45';
            }  else if( systemNowDate.format('HH:mm') >= '12:45' && systemNowDate.format('HH:mm') < '13:00' ) {
                strCurrentTime = '12:45-13:00';
            }  else if( systemNowDate.format('HH:mm') >= '13:00' && systemNowDate.format('HH:mm') < '13:15' ) {
                strCurrentTime = '13:00-13:15';
            }  else if( systemNowDate.format('HH:mm') >= '13:15' && systemNowDate.format('HH:mm') < '13:30' ) {
                strCurrentTime = '13:15-13:30';
            }  else if( systemNowDate.format('HH:mm') >= '13:30' && systemNowDate.format('HH:mm') < '13:45' ) {
                strCurrentTime = '13:30-13:45';
            }  else if( systemNowDate.format('HH:mm') >= '13:45' && systemNowDate.format('HH:mm') < '14:00' ) {
                strCurrentTime = '13:45-14:00';
            }  else if( systemNowDate.format('HH:mm') >= '14:00' && systemNowDate.format('HH:mm') < '14:15' ) {
                strCurrentTime = '14:00-14:15';
            }  else if( systemNowDate.format('HH:mm') >= '14:15' && systemNowDate.format('HH:mm') < '14:30' ) {
                strCurrentTime = '14:15-14:30';
            }  else if( systemNowDate.format('HH:mm') >= '14:30' && systemNowDate.format('HH:mm') < '14:45' ) {
                strCurrentTime = '14:30-14:45';
            }  else if( systemNowDate.format('HH:mm') >= '14:45' && systemNowDate.format('HH:mm') < '15:00' ) {
                strCurrentTime = '14:45-15:00';
            }  else if( systemNowDate.format('HH:mm') >= '15:00' && systemNowDate.format('HH:mm') < '15:15' ) {
                strCurrentTime = '15:00-15:15';
            }  else if( systemNowDate.format('HH:mm') >= '15:15' && systemNowDate.format('HH:mm') < '15:30' ) {
                strCurrentTime = '15:15-15:30';
            }  else if( systemNowDate.format('HH:mm') >= '15:30' && systemNowDate.format('HH:mm') < '15:45' ) {
                strCurrentTime = '15:30-15:45';
            }  else if( systemNowDate.format('HH:mm') >= '15:45' && systemNowDate.format('HH:mm') < '16:00' ) {
                strCurrentTime = '15:45-16:00';
            }  else if( systemNowDate.format('HH:mm') >= '16:00' && systemNowDate.format('HH:mm') < '16:15' ) {
                strCurrentTime = '16:00-16:15';
            }  else if( systemNowDate.format('HH:mm') >= '16:15' && systemNowDate.format('HH:mm') < '16:30' ) {
                strCurrentTime = '16:15-16:30';
            }  else if( systemNowDate.format('HH:mm') >= '16:30' && systemNowDate.format('HH:mm') < '16:45' ) {
                strCurrentTime = '16:30-16:45';
            }  else if( systemNowDate.format('HH:mm') >= '16:45' && systemNowDate.format('HH:mm') < '17:00' ) {
                strCurrentTime = '16:45-17:00';
            }  else if( systemNowDate.format('HH:mm') >= '17:00' && systemNowDate.format('HH:mm') < '17:15' ) {
                strCurrentTime = '17:00-17:15';
            }  else if( systemNowDate.format('HH:mm') >= '17:15' && systemNowDate.format('HH:mm') < '17:30' ) {
                strCurrentTime = '17:15-17:30';
            }  else if( systemNowDate.format('HH:mm') >= '17:30' && systemNowDate.format('HH:mm') < '17:45' ) {
                strCurrentTime = '17:30-17:45';
            }  else if( systemNowDate.format('HH:mm') >= '17:45' && systemNowDate.format('HH:mm') < '18:00' ) {
                strCurrentTime = '17:45-18:00';
            }  else if( systemNowDate.format('HH:mm') >= '18:00' && systemNowDate.format('HH:mm') < '18:15' ) {
                strCurrentTime = '18:00-18:15';
            }  else if( systemNowDate.format('HH:mm') >= '18:15' && systemNowDate.format('HH:mm') <= '18:30' ) {
                strCurrentTime = '18:15-18:30';
            }
            
        
        /*for( String objStr : lstOfTiming ) {
            if( String.isNotBlank( objStr ) ) {
                for( String objMeetingRoomStr : lstOfMeetingRoom ) {
                    if( String.isNotBlank( objMeetingRoomStr ) ) {
                        mapMeetingRoomCL.put(objMeetingRoomStr + objStr,null);
                    }
                }
            }
        }*/
        
        for( Calling_List__c objCL :  [ SELECT Id, RecordTypeId, Name,CreatedDate ,Meeting_Room__c,Attribute31__c,Service_start__c,Account__r.Name
                                          FROM Calling_List__c
                                         WHERE RecordTypeId =: callingListRecordTypeId
                                           AND Calling_List_Status__c != 'Closed'
                                           AND Closed_By_Reception__c = false
                                           AND CreatedDate = TODAY ORDER BY CreatedDate ] ) {
            String key;
            /*if( objCL.CreatedDate.format('HH:mm') >= '08:30' && objCL.CreatedDate.format('HH:mm') < '09:30' ) {
                key = '08:30-09:30';
            } else if( objCL.CreatedDate.format('HH:mm') >= '09:30' && objCL.CreatedDate.format('HH:mm') < '10:30' ) {
                key = '09:30-10:30';
            } else if( objCL.CreatedDate.format('HH:mm') >= '10:30' && objCL.CreatedDate.format('HH:mm') < '11:30' ) {
                key = '10:30-11:30';
            } else if( objCL.CreatedDate.format('HH:mm') >= '11:30' && objCL.CreatedDate.format('HH:mm') < '12:30' ) {
                key = '11:30-12:30';
            } else if( objCL.CreatedDate.format('HH:mm') >= '12:30' && objCL.CreatedDate.format('HH:mm') < '13:30' ) {
                key = '12:30-13:30';
            } else if( objCL.CreatedDate.format('HH:mm') >= '13:30' && objCL.CreatedDate.format('HH:mm') < '14:30' ) {
                key = '13:30-14:30';
            } else if( objCL.CreatedDate.format('HH:mm') >= '14:30' && objCL.CreatedDate.format('HH:mm') < '15:30' ) {
                key = '14:30-15:30';
            } else if( objCL.CreatedDate.format('HH:mm') >= '15:30' && objCL.CreatedDate.format('HH:mm') < '16:30' ) {
                key = '15:30-16:30';
            } else if( objCL.CreatedDate.format('HH:mm') >= '16:30' && objCL.CreatedDate.format('HH:mm') < '17:30' ) {
                key = '16:30-17:30';
            } else if( objCL.CreatedDate.format('HH:mm') >= '17:30' && objCL.CreatedDate.format('HH:mm') <= '18:30' ) {
                key = '17:30-18:30';
            } else if( objCL.CreatedDate.format('HH:mm') >= '18:30' && objCL.CreatedDate.format('HH:mm') <= '19:30' ) {
                key = '18:30-19:30';
            } else if( objCL.CreatedDate.format('HH:mm') >= '19:30' && objCL.CreatedDate.format('HH:mm') <= '20:30' ) {
                key = '19:30-20:30';
            } else if( objCL.CreatedDate.format('HH:mm') >= '20:30' && objCL.CreatedDate.format('HH:mm') <= '21:30' ) {
                key = '20:30-21:30';
            }*/
            
            
            if( objCL.CreatedDate.format('HH:mm') >= '08:30' && objCL.CreatedDate.format('HH:mm') < '08:45' ) {
                key = '08:30-08:45';
            } else if( objCL.CreatedDate.format('HH:mm') >= '08:45' && objCL.CreatedDate.format('HH:mm') < '09:00' ) {
                key = '08:45-09:00';
            } else if( objCL.CreatedDate.format('HH:mm') >= '09:00' && objCL.CreatedDate.format('HH:mm') < '09:15' ) {
                key = '09:00-09:15';
            } else if( objCL.CreatedDate.format('HH:mm') >= '09:15' && objCL.CreatedDate.format('HH:mm') < '09:30' ) {
                key = '09:15-09:30';
            } else if( objCL.CreatedDate.format('HH:mm') >= '09:30' && objCL.CreatedDate.format('HH:mm') < '09:45' ) {
                key = '09:30-09:45';
            } else if( objCL.CreatedDate.format('HH:mm') >= '09:45' && objCL.CreatedDate.format('HH:mm') < '10:00' ) {
                key = '09:45-10:00';
            } else if( objCL.CreatedDate.format('HH:mm') >= '10:00' && objCL.CreatedDate.format('HH:mm') < '10:15' ) {
                key = '10:00-10:15';
            } else if( objCL.CreatedDate.format('HH:mm') >= '10:15' && objCL.CreatedDate.format('HH:mm') < '10:30' ) {
                key = '10:15-10:30';
            } else if( objCL.CreatedDate.format('HH:mm') >= '10:30' && objCL.CreatedDate.format('HH:mm') < '10:45' ) {
                key = '10:30-10:45';
            } else if( objCL.CreatedDate.format('HH:mm') >= '10:45' && objCL.CreatedDate.format('HH:mm') < '11:00' ) {
                key = '10:45-11:00';
            } else if( objCL.CreatedDate.format('HH:mm') >= '11:00' && objCL.CreatedDate.format('HH:mm') < '11:15' ) {
                key = '11:00-11:15';
            } else if( objCL.CreatedDate.format('HH:mm') >= '11:15' && objCL.CreatedDate.format('HH:mm') < '11:30' ) {
                key = '11:15-11:30';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '11:30' && objCL.CreatedDate.format('HH:mm') < '11:45' ) {
                key = '11:30-11:45';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '11:45' && objCL.CreatedDate.format('HH:mm') < '12:00' ) {
                key = '11:45-12:00';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '12:00' && objCL.CreatedDate.format('HH:mm') < '12:15' ) {
                key = '12:00-12:15';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '12:15' && objCL.CreatedDate.format('HH:mm') < '12:30' ) {
                key = '12:15-12:30';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '12:30' && objCL.CreatedDate.format('HH:mm') < '12:45' ) {
                key = '12:30-12:45';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '12:45' && objCL.CreatedDate.format('HH:mm') < '13:00' ) {
                key = '12:45-13:00';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '13:00' && objCL.CreatedDate.format('HH:mm') < '13:15' ) {
                key = '13:00-13:15';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '13:15' && objCL.CreatedDate.format('HH:mm') < '13:30' ) {
                key = '13:15-13:30';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '13:30' && objCL.CreatedDate.format('HH:mm') < '13:45' ) {
                key = '13:30-13:45';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '13:45' && objCL.CreatedDate.format('HH:mm') < '14:00' ) {
                key = '13:45-14:00';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '14:00' && objCL.CreatedDate.format('HH:mm') < '14:15' ) {
                key = '14:00-14:15';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '14:15' && objCL.CreatedDate.format('HH:mm') < '14:30' ) {
                key = '14:15-14:30';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '14:30' && objCL.CreatedDate.format('HH:mm') < '14:45' ) {
                key = '14:30-14:45';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '14:45' && objCL.CreatedDate.format('HH:mm') < '15:00' ) {
                key = '14:45-15:00';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '15:00' && objCL.CreatedDate.format('HH:mm') < '15:15' ) {
                key = '15:00-15:15';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '15:15' && objCL.CreatedDate.format('HH:mm') < '15:30' ) {
                key = '15:15-15:30';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '15:30' && objCL.CreatedDate.format('HH:mm') < '15:45' ) {
                key = '15:30-15:45';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '15:45' && objCL.CreatedDate.format('HH:mm') < '16:00' ) {
                key = '15:45-16:00';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '16:00' && objCL.CreatedDate.format('HH:mm') < '16:15' ) {
                key = '16:00-16:15';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '16:15' && objCL.CreatedDate.format('HH:mm') < '16:30' ) {
                key = '16:15-16:30';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '16:30' && objCL.CreatedDate.format('HH:mm') < '16:45' ) {
                key = '16:30-16:45';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '16:45' && objCL.CreatedDate.format('HH:mm') < '17:00' ) {
                key = '16:45-17:00';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '17:00' && objCL.CreatedDate.format('HH:mm') < '17:15' ) {
                key = '17:00-17:15';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '17:15' && objCL.CreatedDate.format('HH:mm') < '17:30' ) {
                key = '17:15-17:30';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '17:30' && objCL.CreatedDate.format('HH:mm') < '17:45' ) {
                key = '17:30-17:45';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '17:45' && objCL.CreatedDate.format('HH:mm') < '18:00' ) {
                key = '17:45-18:00';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '18:00' && objCL.CreatedDate.format('HH:mm') < '18:15' ) {
                key = '18:00-18:15';
            }  else if( objCL.CreatedDate.format('HH:mm') >= '18:15' && objCL.CreatedDate.format('HH:mm') <= '18:30' ) {
                key = '18:15-18:30';
            }           
            
            
            
            if( String.isNotBlank( key ) && String.isNotBlank( objCL.Meeting_Room__c ) ) {
//                mapMeetingRoomCL.put( objCL.Meeting_Room__c + objCL.Attribute31__c, objCL);
                mapMeetingRoomCL.put( objCL.Meeting_Room__c, objCL);
            }
        }
        //system.assert(false,' mapMeetingRoomCL  :  '+ mapMeetingRoomCL);
    }
    public class AppointmentWrapper {
        public Calling_List__c objCL {get;set;}
        public Boolean isSelected    {get;set;}
        public AppointmentWrapper() {
            
        }
        public AppointmentWrapper( Calling_List__c objCL ) {
            this.objCL = objCL ;
            this.isSelected = false;
        }
    }       
    
}