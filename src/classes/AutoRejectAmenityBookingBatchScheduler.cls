/*----------------------------------------------------------------------------
Description: Scheduler for bacth AutoRejectAmenityBookingBatch to run every midnight
========================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------
1.0     | 13-11-2018       | Lochana Rajput   | Reject booking if deposit is not paid
												before due date
==============================================================================================
*/
public class AutoRejectAmenityBookingBatchScheduler implements Schedulable {
	public static String cronExp = '0 0 0 ? * * *';
    public static String jobName = 'AutoRejectAmenityBookingBatchScheduler';
	public void scheduleMyJob() {
		AutoRejectAmenityBookingBatchScheduler batchObj = new AutoRejectAmenityBookingBatchScheduler();
        System.schedule(jobName, cronExp, batchObj);
	}
	public void execute(SchedulableContext sc) {
		AutoRejectAmenityBookingBatch batchObj = new AutoRejectAmenityBookingBatch();
        Database.executeBatch(batchObj, 100);
	}
}