public without sharing class FM_EmailMessageTriggerHandler{
    
    public static void beforeinsert( list<EmailMessage> lstNewEmailMessages ) {
    	//map<Id,list<EmailMessage>> mapCaseIdlistEmailMessage = new map<Id,list<EmailMessage>>();
    	set<Id> setFMCaseIds = new set<Id>();
    	list<EmailMessage> lstEmailMessages = new list<EmailMessage>();
    	
    	for( EmailMessage objEM : lstNewEmailMessages ) {
    		if( String.isNotBlank( objEM.Subject ) && 
    			objEM.Subject.equalsIgnoreCase( Label.FM_POP_Approval_Email_Subject ) && 
    			objEM.Related_FM_Case__c != NULL ) {
    			setFMCaseIds.add( objEM.Related_FM_Case__c );
    			lstEmailMessages.add( objEM );
    		}
    	}
    	
    	if( !setFMCaseIds.isEmpty() || !lstEmailMessages.isEmpty() ) {
    		updatePOPEmailMessageBody( setFMCaseIds, lstEmailMessages );
    	}
    }
    
    public static void afterInsert(map<Id,EmailMessage> newMap){
        map<String,Id> mapToAddress_CaseId = new map<String,Id>();
        for(EmailMessage objEM : newMap.values()){
            if(objEM.ParentId != null
            && string.valueOf(objEM.ParentId).startswith('500')
            && !String.isBlank(objEM.ToAddress)){
                for(String email : objEM.ToAddress.split(';')){
                    mapToAddress_CaseId.put(email.trim(), objEM.ParentId);
                }
                
            }
        }
        system.debug('mapToAddress_CaseId*****'+mapToAddress_CaseId);
        if(!mapToAddress_CaseId.isEmpty()){
            map<String,Id> mapLoamsEmail_DirectorId = new map<String,Id>();
            for(FM_User__c objUser : [Select Id
                                           , Building__c
                                           , FM_Role__c
                                           , FM_User__c
                                           , Building__r.Loams_Email__c
                                      From FM_User__c
                                      Where FM_Role__c = 'Property Director'
                                      and Building__r.Loams_Email__c IN : mapToAddress_CaseId.keySet()]){
                mapLoamsEmail_DirectorId.put(objUser.Building__r.Loams_Email__c, objUser.FM_User__c);
            }
            system.debug('mapLoamsEmail_DirectorId*****'+mapLoamsEmail_DirectorId);
            if(!mapLoamsEmail_DirectorId.isEmpty()){
                //list<Case> lstCaseToUpdate = new list<Case>();
                //set<Id> setCaseToUpdate = new set<Id>();
                map<Id,case> mapOfCaseIdToCase = new map<Id,case>();
                list<FM_Process_Escalation_Matrix__mdt> lstMetadata = [SELECT MasterLabel
                                                                            , QualifiedApiName
                                                                            , Escalate_After_in_hours__c
                                                                       FROM FM_Process_Escalation_Matrix__mdt
                                                                       Where QualifiedApiName = 'FM_Email_to_Case'
                                                                       limit 1];
                system.debug('lstMetadata*****'+lstMetadata);
                if(!lstMetadata.isEmpty()){
                    DateTime dt = system.today();
                    String day = dt.format('EEEE');
                    Integer daysToAdd = 0;
                    if(day.equalsIgnoreCase('Wednesday')
                    || day.equalsIgnoreCase('Thursday')
                    || day.equalsIgnoreCase('Friday')){
                        daysToAdd = 2;
                    }
                    for(String strEmail : mapLoamsEmail_DirectorId.keySet()){
                        if(mapToAddress_CaseId.containsKey(strEmail)){
                            Case objCase = new Case();
                            objCase.Id = mapToAddress_CaseId.get(strEmail);
                            objCase.Property_Director__c = mapLoamsEmail_DirectorId.get(strEmail);
                            Integer escalationDays = Integer.valueOf(lstMetadata[0].Escalate_After_in_hours__c) + daysToAdd;
                            objCase.Escalation_Date__c = Date.today().addDays(escalationDays);
                            objCase.Case_Reopen_Date__c = Date.today();
                            //setCaseToUpdate.add(objCase.Id);
                            mapOfCaseIdToCase.put(objCase.Id,objCase);                         
                        }
                    }
                    //system.debug('setCaseToUpdate*****'+setCaseToUpdate);
                    system.debug('mapOfCaseIdToCase*****'+mapOfCaseIdToCase);
                    /*lstCaseToUpdate = [SELECT Id
                                            , Property_Director__c
                                            , Escalation_Date__c
                                            , Case_Reopen_Date__c
                                       FROM Case
                                       WHERE Id In: setCaseToUpdate];*/                    
                    //system.debug('lstCaseToUpdate*****'+lstCaseToUpdate);
                    if(!mapOfCaseIdToCase.isEmpty()){
                        update mapOfCaseIdToCase.values();
                    }
                } // end of NOT lstMetadata isempty
            } // end of NOT mapLoamsEmail_DirectorId isempty
        } // end of NOT mapToAddress_CaseId isempty
    } // end of afterInsert method
    
    
    private static void updatePOPEmailMessageBody( set<Id> setFMCaseIds, list<EmailMessage> lstEmailMessages ) {
    	list<SR_Attachments__c> lstSRAttachment = getRelatedAttachment( setFMCaseIds );
    	if( !lstSRAttachment.isEmpty() ) {
    		map<Id,list<SR_Attachments__c>> mapCaseSrAttach = getCaseToSrAttachMap( lstSRAttachment );
    		for( EmailMessage objEM : lstEmailMessages ) {
    			if( mapCaseSrAttach.containsKey( objEM.Related_FM_Case__c ) ) {
					objEM.TextBody += '\n\nPlease refer below are the documents :';
    				for( SR_Attachments__c objAttach : mapCaseSrAttach.get( objEM.Related_FM_Case__c ) ) {
    					objEM.TextBody = String.isNotBlank( objEM.TextBody ) ? 
										 objEM.TextBody + '\n' + objAttach.Name + ' : ' + objAttach.Attachment_URL__c :
										 objAttach.Name + ' : ' + objAttach.Attachment_URL__c ;
    				}
    			}
    		}
    	}
    }
    
    private static list<SR_Attachments__c> getRelatedAttachment( set<Id> setFMCaseIds ) {
    	return [ SELECT Id
    				  , Name
    				  , Attachment_URL__c
    				  , Is_sent_in_email__c
    				  , FM_Case__c
    			   FROM SR_Attachments__c
    			  WHERE FM_Case__c IN :setFMCaseIds
    			    AND Is_sent_in_email__c = true ];
    }
    
    private static map<Id,list<SR_Attachments__c>> getCaseToSrAttachMap( list<SR_Attachments__c> lstSRAttachment ) {
    	map<Id,list<SR_Attachments__c>> mapCaseSrAttach = new map<Id,list<SR_Attachments__c>>();
    	for( SR_Attachments__c objAttach : lstSRAttachment ) {
    		if( mapCaseSrAttach.containsKey( objAttach.FM_Case__c ) ) {
    			mapCaseSrAttach.get( objAttach.FM_Case__c ).add( objAttach );
    		}
    		else {
    			mapCaseSrAttach.put( objAttach.FM_Case__c, new list<SR_Attachments__c>{ objAttach } );
    		}
    	}
    	return mapCaseSrAttach ;
    }
    
}