public class FMInvoice_WebServiceAdapter extends GenericWebServiceAdapter implements WebServiceAdapter{
    public String url;
    
    public override WebServiceAdapter call(){
        super.call();
        system.debug('!!!parameters'+parameters);
        String regID_BUID = parameters.get('regID_BUID');
        system.debug('!!!!!!!!regID_BUID'+regID_BUID);
        String quarter = parameters.get('__quarter');
        system.debug('!!!!!!!!quarter'+quarter);
        String BUId, regID;
        Date startDate, endDate;
        Date todayDate = System.Today();
        list<UploadMultipleDocController.MultipleDocRequest> wrapperLst = 
            new list<UploadMultipleDocController.MultipleDocRequest>();
        /*List<Quarters__c> lstQuarters = Quarters__c.getall().values();
        system.debug('!!!!lstQuarters'+lstQuarters);
        for (Quarters__c objQuarter : lstQuarters){
            if (todayDate >= objQuarter.Start_Date__c && todayDate <= objQuarter.End_Date__c){
                startDate = objQuarter.Start_Date__c;
            }
        }*/
        if (quarter != 'None'){
            Quarters__c objQuarter = Quarters__c.getValues(quarter);
            startDate = objQuarter.Start_Date__c;
            endDate = objQuarter.End_Date__c;
        }
        system.debug('!!!!!startDate'+startDate);
        if(String.isNotBlank(regID_BUID)) {
            BUId = regID_BUID.substringBefore('-');
            regID = regID_BUID.substringAfter('-');
        }
        if (startDate != null && endDate != null){
            String strURL = FmIpmsRestServices.getInvoiceByRegistrationIdAndDateRange(regID, startDate, endDate);
            system.debug('!!!!!strURL'+strURL);
            if( String.isNotBlank( strURL ) ) {
                wrapperLst.add( FM_Utility.makeWrapperObject( getDocumentBodyAsString( strURL ) ,
                    Label.FM_Invoice_Letter + '.pdf', Label.FM_Invoice_Letter + '.pdf', BUId, 
                    String.valueOf( wrapperLst.size() + 1)));
            }
            system.debug('!!!!!!wrapperLst'+wrapperLst);
            if( !wrapperLst.isEmpty() ) {
                UploadMultipleDocController.data objResponse = 
                    PenaltyWaiverService.uploadDocumentsOnCentralRepo( wrapperLst, 'FM' );
                system.debug('!!!!!objResponse'+objResponse);
                if( objResponse != NULL ) {
                    if( objResponse.data != NULL && String.isNotBlank( objResponse.status ) ) {
                         for( UploadMultipleDocController.MultipleDocResponse objFile : objResponse.data ) {
                             system.debug('!!!!!objFile'+objFile);
                              this.url = objFile.url;
                         }
                    }
                }
            }   
        }     
        return this;
    }
    public override String getResponse(){
        super.getResponse();
        return this.url;
    }
    
    private static String getDocumentBodyAsString( String strURL ) {
        if( String.isNotBlank( strURL ) ) {
            HttpRequest req = new HttpRequest();
            req.setEndpoint( strURL );
            req.setMethod('GET');
            req.setHeader('Content-Type', 'application/pdf');
            req.setTimeout(60000);
            HTTPResponse res = new Http().send( req );
            //return res.getBody();
            String strBody = EncodingUtil.base64Encode( res.getBodyAsBlob() );
            return strBody ;
        }
        return '';
    }
}