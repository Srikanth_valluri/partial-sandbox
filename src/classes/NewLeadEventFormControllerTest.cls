/************************************************************************************************
 * @Name              : NewLeadEventFormControllerTest
 * @Description       : Test Class for NewLeadEventFormController
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         06/08/2020       Created
***********************************************************************************************/

@isTest
public class NewLeadEventFormControllerTest{

    @isTest
    static void testAPI(){
        
        Account acc = new Account();
        acc.Name = 'Guest Account';
        insert acc;
        
        Contact superUserCon = new Contact();
        superUserCon.LastName = 'testSuperUser';
        superUserCon.AccountId = acc.Id;
        superUserCon.Email = 'superuser@test.com';
        superUserCon.Portal_Administrator__c = true;
        superUserCon.Owner__c = true;        
        insert superUserCon;
        
        Inquiry__c inq = new Inquiry__c ();
        inq.first_name__c = 'test';
        inq.last_name__c = 'test';
        inq.email__c = 'testemailinq@test.com';
        inq.Nationality__c = 'Indian';
        inq.Preferred_Language__c = 'English';
        inq.Mobile_CountryCode__c = 'India: 0091';
        inq.Mobile_Phone_Encrypt__c = '12321321';    
    
        NewLeadEventFormController obj = new NewLeadEventFormController();
        obj.selectedCountryCode = 'India: 0091';
        obj.selectedNationality = 'Indian';
        obj.selectedLanguage = 'English';
        obj.agencyName = acc.Name;
        obj.agentName = superUserCon.Name;
        
        obj.inq = inq;
        NewLeadEventFormController.getAgencyDetails(acc.Name);
        NewLeadEventFormController.getCorporateAgents(acc.Id);
        obj.submitInquiry ();
        system.assertEquals (obj.statusMessage, 'Success');
    }
    @isTest
    public static void createInquiryFail () {
        
        Account acc = new Account();
        acc.Name = 'Guest Account';
        insert acc;
        
        Contact superUserCon = new Contact();
        superUserCon.LastName = 'testSuperUser';
        superUserCon.AccountId = acc.Id;
        superUserCon.Email = 'superuser@test.com';
        superUserCon.Portal_Administrator__c = true;
        superUserCon.Owner__c = true;        
        insert superUserCon;
        
        Inquiry__c inq = new Inquiry__c ();
        inq.first_name__c = 'test';
        inq.last_name__c = 'test';
        inq.email__c = 'testemailinq@test.com';
        inq.Nationality__c = 'Indian';
        inq.Preferred_Language__c = 'English';
        inq.Mobile_CountryCode__c = 'India: 0091';
        inq.Mobile_Phone_Encrypt__c = '12321321';    
    
        NewLeadEventFormController obj = new NewLeadEventFormController();
        obj.selectedNationality = 'Indian';
        obj.selectedLanguage = 'English';
        NewLeadEventFormController.getAgencyDetails('');
        NewLeadEventFormController.getCorporateAgents(acc.Id);
        obj.inq = inq;
        obj.submitInquiry ();
        system.assertNOTEquals (obj.statusMessage, '');
    }
    
}