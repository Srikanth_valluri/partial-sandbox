/**
 * @File Name          : ScheduleFMCaseEscalationBatch.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 8/22/2019, 11:49:39 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/22/2019, 11:49:02 AM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
global without sharing class ScheduleFMCaseEscalationBatch implements Schedulable {
   global void execute(SchedulableContext ctx) {
        FMCaseEscalationViolationNoticeBatch escalationBatchObj = new FMCaseEscalationViolationNoticeBatch();
        database.executeBatch(escalationBatchObj,1);
    }
}