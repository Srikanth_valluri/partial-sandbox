/**********************************************************************************************************************
Description: A service to make Robo call
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By   | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     | 16-04-2020        | Aishwarya Todkar   | 1. Initial Draft
***********************************************************************************************************************/
public class RoboCallService {

    Static String boundary = '------------------------700252463407842306668015';

/**********************************************************************************************************************
Description : Method get request body as blob
Parameter(s): Map<String, String>
Return Type : Blob
**********************************************************************************************************************/
    public static Blob getBodyAsBlob( Map<String, String> mapKeyValue ) {
        
        List<String> lstKeyValue = new List<String>();

        for(String str: mapKeyValue.keyset()) {
            System.debug('str: '+str);
            lstKeyValue.add(str+'='+mapKeyValue.get(str));
        }
        System.debug('lstKeyValue: '+lstKeyValue);

        Integer forCount = 0;
        Integer noOfParams = lstKeyValue.size();
        Blob bodyBlob = null;
        Map<Integer, String> mapIntBolbStr = new Map<Integer, String>();

        System.debug('noOfParams: '+noOfParams);

        for(String objStr : lstKeyValue) {

            forCount++;
            System.debug('forCount: '+forCount);

            System.debug('objStr: '+objStr);
            List<String> strKeyVals = objStr.split('=');
            System.debug('strKeyVals: '+strKeyVals);

            String header = '--'+boundary+'\nContent-Disposition: form-data; name="'+strKeyVals[0]+'"\nContent-Type: application/octet-stream';
          
            String footer = '--'+boundary+'--';             
            String headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
            while(headerEncoded.endsWith('=')) {
                header+=' ';
                headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
            }
            //String bodyEncoded = EncodingUtil.base64Encode(fileBody1);
            String bodyEncoded = EncodingUtil.base64Encode(Blob.valueOf(strKeyVals[1]));
            if(bodyEncoded.containsIgnoreCase('base64,')) {
                bodyEncoded = bodyEncoded.substringAfter('base64,');
            }
            System.debug('bodyEncoded:: '+bodyEncoded);

            
            String last4Bytes = bodyEncoded.substring(bodyEncoded.length()-4,bodyEncoded.length());             
           
           if(last4Bytes.endsWith('==')) {
              
                System.debug('In == if 1');
                last4Bytes = last4Bytes.substring(0,2) + '0K';
                bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
                
                String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
                //bodyBlob += EncodingUtil.base64Decode(headerEncoded+bodyEncoded);
                
                String str = headerEncoded+bodyEncoded+','+footerEncoded;
                mapIntBolbStr.put(forCount, str);

            } else if(last4Bytes.endsWith('=')) {
              
                System.debug('In = if 1');
                last4Bytes = last4Bytes.substring(0,3) + 'N';
                bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
                footer = '\n' + footer;
                String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
                //bodyBlob += EncodingUtil.base64Decode(headerEncoded+bodyEncoded);
                
                String str = headerEncoded+bodyEncoded+','+footerEncoded;
                mapIntBolbStr.put(forCount, str);   
                   
            } 
            else {
                // Prepend the CR LF to the footer2
                System.debug('In else part 1');
                footer = '\r\n' + footer;
                String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
                //bodyBlob += EncodingUtil.base64Decode(headerEncoded+bodyEncoded);
                
                String str = headerEncoded+bodyEncoded+','+footerEncoded;
                mapIntBolbStr.put(forCount, str);
            }
        }

        System.debug('mapIntBolbStr :'+mapIntBolbStr);

        String finalBlobStr = '';
        for(Integer intObj : mapIntBolbStr.keySet()) {

            if(intObj != noOfParams) {
                finalBlobStr += mapIntBolbStr.get(intObj).split(',')[0];   
            }
            else {
                list<String> lstString = mapIntBolbStr.get(intObj).split(',');
                finalBlobStr += lstString[0]+lstString[1];
            }
        }
        System.debug('finalBlobStr: '+finalBlobStr);
        blob finalBlob = EncodingUtil.base64Decode(finalBlobStr);
        return finalBlob;
    }
    
/**********************************************************************************************************************
Description : Method to process robo call API
Parameter(s )  : keyword, endpoint, auth_key
Return Type : HTTPResponse
**********************************************************************************************************************/
    public Static HTTPResponse processRoboRequest( Blob reqBlob, String endpoint, String auth_key ) {
        
        HttpRequest req = new HttpRequest( );
        req.setMethod('POST');
        req.setBodyAsBlob( reqBlob );
        req.setHeader('Content-Length',String.valueof(req.getBodyAsBlob().size()));
        req.setHeader('Content-Type','multipart/form-data; boundary='+boundary);
        req.setHeader( 'auth_key',auth_key );
        req.setEndpoint( endpoint );
        req.setHeader('Accept', 'application/json');
        req.setTimeout(120000);
        HTTPResponse response = new Http().send(req );   
        System.debug( 'status = ' + response.getStatusCode( ) );
        System.debug( 'Response - -  ' + response );
        System.debug( 'Response body - -  ' + response.getBody( ) );
        
        return response;
    }
}