public without sharing class TenantHistoryMaintainence {
    
    private static final Id TENANTREGISTRATIONRECTYPEID = 
                                Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Registration').getRecordTypeId();
                                
    private static final Id TENANTRENEWALRECTYPEID = 
                                Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Renewal').getRecordTypeId();
                                
    
    @InvocableMethod 
    public static void reEvaluateTenantHistory( list<FM_Case__c> lstFMCases ) {
        system.debug('==lstFMCases=='+lstFMCases);
        list<Tenant_History__c> lstTenantHistory = new list<Tenant_History__c>();
        for( FM_Case__c objFMCase : lstFMCases ) {
            system.debug('==objFMCase.RecordTypeId=='+objFMCase.RecordTypeId);
            if( String.isNotBlank( objFMCase.RecordTypeId ) ) {
                //Handle Tenant Registration cases.
                if( objFMCase.RecordTypeId == TENANTREGISTRATIONRECTYPEID ) {
                    lstTenantHistory.addAll( calculateRegistrationHistory( objFMCase ) );
                }
                //Handle Tenant Renewal cases.
                else if( objFMCase.RecordTypeId == TENANTRENEWALRECTYPEID ) {
                    lstTenantHistory.addAll( calculateRenewalHistory( objFMCase ) );
                }
            }
        }
        
        if( !lstTenantHistory.isEmpty() ) {
            upsert lstTenantHistory ;
        }
    }
    
    @testVisible
    private static list<Tenant_History__c> calculateRegistrationHistory( FM_Case__c objFMCase ) {
        list<Tenant_History__c> lstTenantHistory = new list<Tenant_History__c>();
        
        lstTenantHistory.add( createTenantHistory( objFMCase.Booking_Unit__c,
                                                   objFMCase.Account__c,
                                                   objFMCase.Tenant__c,
                                                   false,
                                                   objFMCase.Start_Date__c,
                                                   objFMCase.End_Date__c,
                                                   NULL,
                                                   NULL ) );
                                                   
        return lstTenantHistory ;
    }
    
    @testVisible
    private static list<Tenant_History__c> calculateRenewalHistory( FM_Case__c objFMCase ) {
        system.debug('==calculateRenewalHistory==');
        list<Tenant_History__c> lstTenantHistory = new list<Tenant_History__c>();
        
        //Creating new tenant history record.
        lstTenantHistory.add( createTenantHistory( objFMCase.Booking_Unit__c,
                                                   objFMCase.Account__c,
                                                   objFMCase.Tenant__c,
                                                   true,
                                                   objFMCase.Start_Date__c,
                                                   objFMCase.End_Date__c,
                                                   objFMCase.Start_Date__c,
                                                   NULL ) );
        
        //Deactivating the previous active records                                                 
        for( Tenant_History__c objTenantHistory : getExistingTenantHistory( objFMCase.Booking_Unit__c,
                                                                            objFMCase.Account__c,
                                                                            objFMCase.Tenant__c,
                                                                            true ) ) {
            objTenantHistory.Is_active__c = false ;
            lstTenantHistory.add( objTenantHistory );
        }
        system.debug('==lstTenantHistory=='+lstTenantHistory);
        return lstTenantHistory ;
    }
    
    @testVisible
    private static Tenant_History__c createTenantHistory( Id unitId, 
                                                          Id ownerId, 
                                                          Id tenantId, 
                                                          Boolean isActive,
                                                          Date leaseStartDate,
                                                          Date leaseEndDate,
                                                          Date moveInDate,
                                                          Date moveOutDate ) {
        return new Tenant_History__c( Booking_Unit__c = unitId,
                                      Is_active__c = isActive,
                                      Lease_End_Date__c = leaseEndDate,
                                      Lease_Start_Date__c = leaseStartDate,
                                      Move_in_Date__c = moveInDate,
                                      Move_out_Date__c = moveOutDate,
                                      Owner__c = ownerId,
                                      Tenant__c = tenantId );
    }
    
    @testVisible 
    private static list<Tenant_History__c> getExistingTenantHistory( Id unitId, 
                                                                     Id ownerId, 
                                                                     Id tenantId, 
                                                                     Boolean isActive ) {
        if( String.isNotBlank( unitId ) &&
            String.isNotBlank( ownerId ) &&
            String.isNotBlank( tenantId ) ) {
            return [ SELECT Id
                          , Booking_Unit__c
                          , Is_active__c
                          , Lease_End_Date__c
                          , Lease_Start_Date__c
                          , Move_in_Date__c
                          , Move_out_Date__c
                          , Owner__c
                          , Tenant__c
                          , Name
                       FROM Tenant_History__c
                      WHERE Booking_Unit__c = :unitId
                        AND Owner__c = :ownerId
                        AND Tenant__c = :tenantId
                        AND Is_active__c = :isActive ];
        }
        return new list<Tenant_History__c>();
    }
    
}