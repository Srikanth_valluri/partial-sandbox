public class PromotersController {
    
    public list<Promoter> lstPromoters{get; set;}
    public string csvString{get; set;}
    public class Promoter{
        public string SFIdentifier{get; set;}
        public string Name {get; set;}
        public string Email{get; set;}
        public string HREmployeeId{get; set;}
        public string Shift{get; set;}
        public integer targetLeads{get; set;}
        public integer targetTours{get; set;}
        
        public Promoter(string a, string b, string c, string d, string e, integer f, integer g){
            SFIdentifier = a;
            Name = b;
            Email = c;
            HREmployeeId = d;
            Shift = e;
            targetLeads = f;
            targetTours = g;
        }
    }
    public PromotersController(){
        list<User> promoterUsers = [Select id, name, email, HR_Employee_ID__c 
                    from User 
                    where isActive = true and Profile.Name = 'Promoter Community Profile'
                    order by name asc];
        
        lstPromoters = new list<Promoter>();
        csvString = 'SF Identifier,Name,Email,HR Employee ID, Campaign Name, Shift, Target Leads, Target Tours\n';
        for(User uRec: promoterUsers){
            lstPromoters.add(new Promoter(uRec.Id, uRec.Name, uRec.Email, uRec.HR_Employee_ID__c, '', 0,0));
            csvString += uRec.Id+','+uRec.Name+','+uRec.Email+','+(uRec.HR_Employee_ID__c!= null? uRec.HR_Employee_ID__c:'')+','+''+','+''+',0,0\n';
        }
        System.debug(csvString);
        csvString = csvString.trim();
        
        System.debug(csvString);
    }

}