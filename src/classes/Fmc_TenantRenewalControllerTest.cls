/**
 * @File Name          : Fmc_TenantRenewalControllerTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 1/12/2020, 4:31:38 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/12/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
public with sharing class Fmc_TenantRenewalControllerTest {
    public static testmethod void testCreateFMCase(){
        User communityUser = CommunityTestDataFactory.createPortalUser();
        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;

        PageReference myVfPage = Page.CommunityPortal;
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Nationality__c ='Pakistani';
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Tenant_Renewal');
        ApexPages.currentPage().getParameters().put('view','tenantrenewal');
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAssignment() );
        System.runAs(communityUser) {
            test.startTest();
            Fmc_TenantRenewalController obj=new Fmc_TenantRenewalController();
            obj.strSelectedUnit = buIns.Id;
            obj.selectUnit();
            obj.insertCase();
            obj.initializeFMCaseAddDetails();
            obj.createFmCase();
            test.stopTest();
        }
    }

    public static testmethod void submitFMCase(){
        User communityUser = CommunityTestDataFactory.createPortalUser();
        Id profileId = [select id from profile where name = 'System Administrator'].id;

        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;

        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;

        PageReference myVfPage = Page.CommunityPortal;

        FM_Case__c fmCaseObjTR=new FM_Case__c();
        fmCaseObjTR.Issue_Date__c=Date.today();
        fmCaseObjTR.Contact_person__c='test';
        fmCaseObjTR.Description__c='test';
        fmCaseObjTR.Contact_person_contractor__c='test';
        fmCaseObjTR.Request_Type_DeveloperName__c='Tenant_Registration';
        fmCaseObjTR.Booking_Unit__c=buIns.id;
        fmCaseObjTR.Account__c=acctIns.id;
        fmCaseObjTR.Approving_Authorities__c='FM Manager';
        fmCaseObjTR.Nationality__c ='Pakistani';
        fmCaseObjTR.status__c='Closed';
        insert fmCaseObjTR;

        FM_Case__c fmCaseObjParent=new FM_Case__c();
        fmCaseObjParent.Issue_Date__c=Date.today();
        fmCaseObjParent.Contact_person__c='test';
        fmCaseObjParent.Description__c='test';
        fmCaseObjParent.Contact_person_contractor__c='test';
        fmCaseObjParent.Request_Type_DeveloperName__c='Tenant_Registration';
        fmCaseObjParent.Booking_Unit__c=buIns.id;
        fmCaseObjParent.Account__c=acctIns.id;
        fmCaseObjParent.Approving_Authorities__c='FM Manager';
        fmCaseObjParent.Nationality__c ='Pakistani';
        fmCaseObjParent.Status__c = 'Closed';
        insert fmCaseObjParent;

        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Parent_Case__c=fmCaseObjParent.id;
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Tenant_Renewal';
        fmCaseObj.Booking_Unit__c=buIns.id;
        fmCaseObj.Account__c=acctIns.id;
        fmCaseObj.Approving_Authorities__c='FM Manager';
        fmCaseObj.Nationality__c ='Pakistani';

        FM_Additional_Detail__c  instanceEMC=new FM_Additional_Detail__c();
        instanceEMC.Emergency_Contact_Case__c=fmCaseObjParent.id;
        instanceEMC.RecordTypeId=Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
        instanceEMC.Name__c='Test';
        instanceEMC.Relationship__c='Test';
        instanceEMC.Phone__c='121212';
        instanceEMC.Email__c='test@test.com';
        insert instanceEMC;

        FM_Additional_Detail__c  instanceRC=new FM_Additional_Detail__c();
        instanceRC.Resident_Case__c =fmCaseObjParent.id;
        instanceRC.Name__c='Test';
        instanceRC.Phone__c='121212';
        instanceRC.Email__c='test@test.com';
        instanceRC.Age__c=12;
        instanceRC.Nationality__c='Pakistani';
        instanceRC.Passport_Number__c='121212';
        instanceRC.RecordTypeId=Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Resident').getRecordTypeId();
        insert instanceRC;

        FM_Additional_Detail__c  instanceVC=new FM_Additional_Detail__c();
        instanceVC.Vehicle_Case__c =fmCaseObjParent.id;
        instanceVC.Vehicle_Make_Model__c='test';
        instanceVC.Vehicle_Colour__c='test';
        instanceVC.Parking_Slot_Number__c='1';
        instanceVC.Vehicle_Number__c='1';
        instanceVC.Identification_Number__c='1';
        instanceVC.RecordTypeId=Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Vehicle').getRecordTypeId();
        insert instanceVC;

        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Tenant_Renewal');
        ApexPages.currentPage().getParameters().put('view','tenantrenewal');
        System.runAs(communityUser) {
            Fmc_TenantRenewalController obj=new Fmc_TenantRenewalController();
            test.startTest();
            obj.strSelectedUnit = buIns.Id;
            obj.selectUnit();
            obj.objFMCase = fmCaseObj;
            obj.insertCase();
            obj.strDetailType='Emergency Contact';
            obj.addDetails();
            obj.indexOfNewChildToRemove=0;
            obj.removeDetails();
            obj.submitSr();
            test.stopTest();
        }
    }

    public static testmethod void testUploadDocument(){
        User communityUser = CommunityTestDataFactory.createPortalUser();

        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;

        PageReference myVfPage = Page.CommunityPortal;

        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Booking_Unit__c = buIns.Id ;
        fmCaseObj.Request_Type_DeveloperName__c='Tenant_Renewal';

        insert fmCaseObj;

        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('Id',fmCaseObj.Id);
        ApexPages.currentPage().getParameters().put('view','tenantrenewal');
        Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
        //Set mock response
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive2());



        System.runAs(communityUser) {
            Fmc_TenantRenewalController obj=new Fmc_TenantRenewalController();
            obj.strSelectedUnit = buIns.Id;
            obj.selectUnit();
            obj.insertCase();
            obj.strDocumentBody='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
            obj.strDocumentName='Test_Document_Work_Permit.htm';
            test.startTest();
            obj.uploadDocument();
            test.stopTest();
        }
    }
}