public class AgentAcademyController {
    
    public User loggedInUser{get; set;}
    public Attendee__c attendee{get; set;}
    public list<SelectOption> dateOptions{get; set;}
    public string selDate{get; set;}
    public list<SelectOption> subjectOptions{get; set;}
    public string selSubject{get; set;}
    public list<SelectOption> slotOptions{get; set;}
    public string selTraining{get; set;} 
    public string message{get; set;}
    public Decimal availableSlots { get; set; }
    
    public List <Attendees> attendeesList { get; set; }
    public Map<id, Training__c> trainingDetails { get; set; }
    
    public class Attendees {
        public Attendee__c attendee { get; set; }
        public String trainingDate { get; set; }
        public String attendeeName { get; set; }
        public Integer index { get; set; }
        
        public Attendees () {
            attendee = new Attendee__c ();
            trainingDate = '';
            attendeeName = '';
            index = 0;
        }
    }
    
    public pagereference deleteAttendee() {
        Integer iRemoveRow = Integer.valueOf(Apexpages.currentpage().getParameters().get('index'));
        for(Integer i=0; i<attendeesList.size(); i++) {
            if(attendeesList[i].index == iRemoveRow ) {
                attendeesList.remove(i);     
            }
        }
        return null;
    }
    
    public pagereference addAttendee(){
        system.debug (selTraining+'---'+selSubject +'---'+selDate);
        try{
            if(attendee.Last_name__c == null || attendee.Email__c == null  || attendee.MObile_Country_Code__c == null || attendee.Mobile_Number__c == null ||
                selTraining == '' || selSubject == '' || selDate == '' ){
                ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.fatal, 'All fields are mandatory.'));
            }
            else{
                if (selTraining != '' && selTraining != null) {
                    attendee.Training__c = selTraining;
                    attendee.Agency__c = (loggedInUser.accountId != null? loggedInUser.accountId : null);
                    
                    Attendees obj = new Attendees ();
                    obj.attendee = attendee;
                    obj.attendeeName = attendee.First_name__c+' '+attendee.Last_Name__c;
                   
                    obj.trainingDate = formateDateOracle(trainingDetails.get (attendee.Training__c).Date_of_Training__c);
                    obj.index = attendeesList.size () +1;
                    attendeesList.add (obj);
                    
                    attendee = new Attendee__c ();
                } else {
                    ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.fatal, 'All fields are mandatory.'));
                }
                /*
                selDate = '';
                selSubject = '';
                selTraining = '';
                
                subjectOptions = new List <SelectOption> ();
                slotOptions = new List <selectOption> ();*/
                
            }
        }
        catch(DMLException e){
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.fatal, e.getDMLMessage(0)));
        }
        catch(Exception e){
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.fatal, e.getMessage()+'-'+e.getLineNumber()));
        }
        return null;
    }
    
    public pagereference bookSession (){
        try{
            if(attendee.Last_name__c != null && attendee.Email__c != null  && attendee.MObile_Country_Code__c != null && attendee.Mobile_Number__c != null 
            && selTraining != '' && selSubject != '' && selDate != '' 
             && selTraining != null && selSubject != null && selDate != null){
                addAttendee();
            }
            Integer confirmedCount = 0;
            Integer rejectedCount = 0;
            
            if (attendeesList.size () > 0) {
                List <Attendee__c> attendeesToInsert = new List <Attendee__c> ();
                SET<ID> trainingId = new SET <ID> ();
                for (Attendees att :attendeesList) {
                    trainingId.add (att.Attendee.Training__c);
                }
                
                Map <ID, Decimal> availableSlotsMap = new Map <ID, Decimal> ();
                for (Training__c train : [SELECT slots_available__c FROM Training__c WHERE ID IN :trainingId]) {
                    
                    availableSlotsMap.put (train.Id, train.slots_available__c );
                    
                }
                system.debug (availableSlotsMap);
                
                for (Attendees att : attendeesList) {
                    
                    Decimal avaialbleSlotsCount = availableSlotsMap.get (att.attendee.Training__c);
                    system.debug (avaialbleSlotsCount);
                    
                    if (avaialbleSlotsCount > 0) {
                        att.attendee.Registration_status__c = 'Confirmed';
                        availableSlotsMap.put (att.attendee.Training__c, avaialbleSlotsCount -1);
                        confirmedCount += 1;
                    } else {
                        att.attendee.Registration_status__c = 'Rejected';
                        rejectedCount += 1;
                    }
                   
                    attendeesToInsert.add (att.Attendee);
                }
                
                insert attendeesToInsert;
                
                String statusMessage = '';
                if (confirmedCount > 0)
                    statusMessage = 'Thank you for registering. Your slot has been confirmed, see you there!<br/>Venue: DAMAC Hills Sales Centre.';
                    //<br/>Confirmed : '+confirmedCount ;
                if (rejectedCount > 0) {
                    statusMessage += '<br/>Sorry! Number of attendees exceeds the number of slots available. Kindly book this session again with fewer attendees or book an alternative session.';
                    //<br/>Rejected : '+rejectedCount ;
                }
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.info, statusMessage));
                
                prepareData();
            } else {
                ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.fatal, 'Add atleast one attendee'));
            }
        }
        catch(DMLException e){
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.fatal, e.getDMLMessage(0)));
        }
        catch(Exception e){
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.fatal, e.getMessage()+'-'+e.getLineNumber()));
        }
        return null;
    }
    
    public AgentAcademyController(){
        prepareData();
    }
    
    public void prepareData(){
        selsubject = '';
        selDate = '';
        selTraining = '';
        availableSlots = 0;
        attendee = new Attendee__c();
        loggedInUser = [Select id, Account.Name,accountId from User where id =: UserInfo.getUserId()];
        dateOptions = new list<SelectOption>();
        subjectOptions = new list<SelectOption>();
        slotOptions = new list<SelectOption>();
        set<Date> setDates = new set<Date>();
        trainingDetails = new Map <ID, Training__c> ();
        attendeesList = new List <Attendees> ();
        
        for(Training__c  train : [Select id, name, subject__c, Name__c, Date_of_Training__c,
                                        From_Time_Slot__c, To_Time_Slot__c, Subject__r.name, subject__r.Duration__c  
                                        from Training__c 
                                        where Date_of_Training__c >= TODAY 
                                        and Ready_for_bookings__c = true
                                        order by Date_of_Training__c asc]){
            setDates.add(train.Date_of_Training__c);   
            trainingDetails.put (train.Id, train);                         
        }
        dateOptions.add(new SelectOption('','-Select A Date-'));
        for(date sDate : setDates)
            dateOptions.add(new SelectOption(sDate+'', formateDateOracle(sDate)));
    }
    
    public static string formateDateOracle(Date sforceDate){        
        DateTime d = sforceDate;
        string monthName= d.format('MMM');
        string Day= string.valueof(d.day());
        if(integer.valueof(day) < 10)
            day= '0' + day;
        integer year= d.year();
        return Day+'-'+monthName+'-'+year;
    }
    
    public void fetchAvailableSlots(){
        if (selTraining != null && selTraining != '') {
            Training__c training = [Select Slots_Available__c
                                    from Training__c 
                                    where Id =: selTraining and Ready_for_bookings__c = true];
            availableSlots = training.Slots_Available__c;
            
            
        }
        
    }
    
    public void fetchSubjects(){
        availableSlots = 0;
        if (selDate != null) {
            date selectedDate = Date.valueOf(selDate.trim());
            subjectOptions = new list<SelectOption>();
            subjectOptions.add(new SelectOption('','-Select A Subject-'));
            selSubject = '';
            
            slotOptions = new list<SelectOption>();
            slotOptions.add(new SelectOption('','-Select A Time Slot-'));
            selTraining = '';
            
            for(Training__c  train : [Select id, name, subject__c, Name__c, Date_of_Training__c,
                                            From_Time_Slot__c, To_Time_Slot__c, Subject__r.name, subject__r.Duration__c  
                                            from Training__c 
                                            where Date_of_Training__c =: selectedDate and Ready_for_bookings__c = true])
                subjectOptions.add(new SelectOption(train.subject__c,train.subject__r.name));
        }
    }
    
    public void fetchTimeSlots(){
        availableSlots = 0;
        date selectedDate = Date.valueOf(selDate.trim());
        slotOptions = new list<SelectOption>();
        slotOptions.add(new SelectOption('','-Select A Time Slot-'));
        for(Training__c  train : [Select id, name, subject__c, Name__c, Date_of_Training__c,
                                        From_Time_Slot__c, To_Time_Slot__c, Subject__r.name, subject__r.Duration__c  
                                        from Training__c 
                                        where subject__c =: selSubject and Date_of_Training__c =: selectedDate and Ready_for_bookings__c = true])
            slotOptions.add(new SelectOption(train.Id, train.From_Time_Slot__c +'-'+ train.To_Time_Slot__c));
    }
    
    
    
    public pagereference createAttendee(){
        try{
            if(attendee.Last_name__c == null || attendee.Email__c == null  || attendee.MObile_Country_Code__c == null || attendee.Mobile_Number__c == null ||
                selTraining == '' || selSubject == '' || selTraining == ''){
                ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.fatal, 'All fields are mandatory.'));
            }
            else{
                attendee.Training__c = selTraining;
                attendee.Agency__c = (loggedInUser.accountId != null? loggedInUser.accountId : null);
                insert attendee;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.info, 'Your booking request has been registered. We will get in touch with you shortly.'));
                prepareData();
            }
        }
        catch(DMLException e){
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.fatal, e.getDMLMessage(0)));
        }
        catch(Exception e){
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.fatal, e.getMessage()+'-'+e.getLineNumber()));
        }
        return null;
    }
}