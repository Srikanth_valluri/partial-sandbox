@RestResource(urlMapping='/insertInquiry/*')
global class InsertInquiry{
    @HttpPost 
    global static string InsertInquiryRecords(){
        try{
        RestResponse res = RestContext.response;
        RestRequest req = RestContext.request;
        system.debug(req.requestBody.tostring());
        system.debug(req.headers);
        string datestring = '';
        map<string,object> m = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.tostring());
        list<Inquiry__c> newInsesertList = new list<Inquiry__c>();
            system.debug('m>>>'+m);
        string userid = null;
        for(object obj :(list<object>)m.get('sObject')){
            Inquiry__c newData = new Inquiry__c();
            map<string,object> mapOffileds = (map<string,object>)obj;
            for(string str : mapOffileds.keyset()){
                if(str !='Tour_Date_Time__c')
                    if(str !='Assigned_PC__c')
                        newData.put(str,(string)(mapOffileds.get(str)));
                    else
                        userid  = (string)(mapOffileds.get(str));
                else{
                    
                    TimeZone tz = UserInfo.getTimeZone();
                    datestring = ((string)(mapOffileds.get(str))).replace('T',' ');
                    dateTime dt = datetime.valueOfGMT(((string)(mapOffileds.get(str))).replace('T',' '))- tz.getOffset(DateTime.now()) / (1000 * 3600 * 24.0);
                    newData.put(str,dt);
                }
            }
            

        // Get the list of Promoter Profile Names
        List<String> profileNames = Label.PromoterCommunityProfile.split(',');
        User currentUser = [ SELECT Id
                                  , Profile.Name
                                  , Name
                                  , ContactId
                               FROM User
                              WHERE Id = :UserInfo.getUserId()
        ];

        newData.Created_By_Profile__c = currentUser.Profile.Name;

        // Populate Promoter Name if User Profiel is mentioned in the Promoter Label
        if(profileNames != null 
            && !profileNames.isEmpty()
            && profileNames.contains(currentUser.Profile.Name)
        ) {
            newData.Promoter_Name__c = currentUser.Name;
        }

        // Populate Campaign
        /*if(selectedStand != 'None' && String.isNotBlank(selectedStand)) {
            newData.Campaign__c = ;
        }*/

        // List<StandName__c> lstStand = StandName__c.getall().values();
        List<User> lstuser = [Select Id,Stand_Id__c FROM User WHERE Id=:UserInfo.getUserId() limit 1];
        if(lstuser.size() >0) {
            if(lstuser[0].Stand_Id__c != null && lstuser[0].Stand_Id__c != '') {
                    newData.Campaign__c = lstuser[0].Stand_Id__c;
            }
        }else{
            
        }

        // Assign the Inquiry to the Current User and Inquiry RT as Pre-Inquiry 
        if(newData.Meeting_Type__c == 'Scheduled Tour' 
            || String.isBlank(newData.Meeting_Type__c)
        ) {
            newData.RecordTypeId = 
                Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
            newData.OwnerId = UserInfo.getUserId();
        }
        if(newData.Meeting_Type__c == 'Direct Tour' ) {
                newData.Tour_Date_Time__c = System.now();
        }

        if(String.isNotBlank(userId)) {

            // Assign Owner to PC
            newData.Assigned_PC__c = userId;
            if(newData.Meeting_Type__c == 'Direct Tour' 
                || newData.Meeting_Type__c == 'Meeting On Stand'
            ) {
                newData.OwnerId = userId;
                newData.recordTYpeId = 
                    Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
            }
            }
            newInsesertList.add(newData);
        }
        
        insert newInsesertList;
               
        res.statusCode = 200;
        list<Inquiry__c> listOfInquery = [select Name from Inquiry__c where id =:newInsesertList[0].Id];
        //res.responseBody = blob.valueof(' Inquiry ' + listOfInquery[0].Name + ' has been created successfully ');
        return ' Inquiry ' + listOfInquery[0].Name + ' has been created successfully ';
        }catch(exception e){
           // blob.valueof(string.valueof(e.getmessage()));
           return string.valueof(e.getmessage());
        }       
    }
}