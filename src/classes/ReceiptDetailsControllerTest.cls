/*
* Description - Test class developed for 'ReceiptDetailsController'
*
* Version            Date            Author            Description
* 1.0                28/11/17        Monali            Initial Draft
*/
@isTest
private class ReceiptDetailsControllerTest {
    static testMethod void testMethod1() {
    	Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='12345');
        insert objAcc;
        
        Test.startTest();

        SOAPCalloutServiceMock.returnToMe = new Map<String, ReceiptDetails1.GetReceiptDetailsResponse_element>();
        ReceiptDetails1.GetReceiptDetailsResponse_element response1 = new ReceiptDetails1.GetReceiptDetailsResponse_element();
        response1.return_x = '{"data":[{"ATTRIBUTE3":"1040788","ATTRIBUTE10":"118265456","ATTRIBUTE2":"CASH","ATTRIBUTE1":"943590","ATTRIBUTE14":"75017","ATTRIBUTE13":"Y","ATTRIBUTE12":"13-DEC-2015","ATTRIBUTE11":"CASH","ATTRIBUTE9":"5000","ATTRIBUTE8":"5000","ATTRIBUTE7":"0","ATTRIBUTE6":"5000","ATTRIBUTE5":"AED","ATTRIBUTE4":"RAGINI RAJAN PATIL","ATTRIBUTE18":null,"ATTRIBUTE17":"DH/32/3204 - Token amount paid by cash","ATTRIBUTE16":"Cheque/Cash to be Remitted","ATTRIBUTE15":null,"ATTRIBUTE19":null}],"message":"[16 Receipts found For Given PartyID]","status":"S"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );

        String strResponse = ReceiptDetailsController.GetReceiptDetails(objAcc.party_ID__C);

        Test.stopTest();

    }
}