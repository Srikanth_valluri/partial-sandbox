/*-------------------------------------------------------------------------------------------------
Description: Controller for Case_Summary_Client_Relation Component
============================================================================================================================
Version | Author                | Date(DD-MM-YYYY)  | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     |                       | 11-10-2017        | Draft for Case_Summary_Client_Relation functionality
1.1     | Aishwarya Todkar      | 08-12-2020        | Added Service charge waiver changes
=============================================================================================================================
*/
@RestResource(urlMapping='/ReraPercentUpdUnitLvl/*')
global without sharing class ReraPercentUpdUnitLvl
{  
    @HttpPatch
    global static ResponseWrapper ReraPercentUpdUnitLvl( String buildingName ) {
        ResponseWrapper objResponseWrapper = new ResponseWrapper();
        if( String.isNotBlank( buildingName ) ) {

            List<Booking_Unit__c> listUnits = new List<Booking_Unit__c>();

            listUnits = [ SELECT
                                Unit_Name__c
                                , Anticipated_Completion_Date__c
                            FROM
                                Booking_Unit__c
                            WHERE
                                Building_Name_qlik__c =: buildingName ];
            if( !listUnits.isEmpty() ) {
                objResponseWrapper.status = 'No of units found:' + listUnits.size();
                objResponseWrapper.statusCode = '200';

                String strJson = '[';
                for( Booking_Unit__c objBu : listUnits ) {
                    String tempJson = '';
                    tempJson = '{'
                                + '"UnitName":"' + objBu.Unit_Name__c + '",'
                                + '"ACD":"' + String.valueOf( objBu.Anticipated_Completion_Date__c ) + '"'
                                + '},';
                    strJson += tempJson;
                }
                strJson = strJson.removeEnd( ',' );
                strJson += ']';
                objResponseWrapper.units = strJson;
            }
            else {
                objResponseWrapper.status = 'No unit found.';
                objResponseWrapper.statusCode = '200';
            }
        }
        else {
            objResponseWrapper.status = 'Building name cannot be blank!';
            objResponseWrapper.statusCode = '200';
        }
        return objResponseWrapper;
    }
    /** Commented by Aishwarya T on 26-11-2020 as requested by Aditya*/
    //*****AKISHOR: 11Sep2020 for updating Rera percent@unt lvl*************//    
//     @HttpPatch
//     global static ResponseWrapper ReraPercentUpdUnitLvl(String UnitName, 
//                                 String Percent, String ProjNum, String ACDDate, String ReraInspecDate, 
//                                 String LastRERAPerc, String PlannedPerc, String OriginalACD,String UnitId ) 
//     {
//        //List<String> lstBuilingCode = new List<String>();
       
//        //List <Property__c> ClList = new List<Property__c>(); 
//        List <Booking_Unit__c> BUList = new List<Booking_Unit__c>();   
//        ResponseWrapper objResponseWrapper = new ResponseWrapper();
       
//        if(UnitName!='')// && UnitId!= NULL ){
//         {       
//        //List<Property__c> FinallList =[Select Id, Property_Code__c ,Name from Property__c where Property_Code__c=:BuilingCode limit 1];// IN: lstBuilingCode];
//        //List<Location__c> LocList = [Select Id, Location_Code__c,Property_Name__c from Location__c where Location_Code__c=:BuilingCode];
//         //List<Property__c> FinallList =[Select Id, Property_Code__c ,Name from Property__c where Property_Code__c IN: lstBuilingCode];
//         List<Booking_Unit__c> LocId=[Select Id,Unit_Name__c,Connect_Completion_Percent__c,ACD_Connect__c,ActuaPercent_Connect1__c,
//          ProjectNum_Connect__c,Rera_Inspection_Dt_Connect__c ,PlannedPerc_Connect__c,LastRERAPerc_Connect__c
//          from Booking_Unit__c where Unit_Name__c=:UnitName AND Unit_Active__c='Active'];

//         List<RERA_Percent__c> ReraPercent = new List<RERA_Percent__c>();
//             for (Booking_Unit__c plist : LocId){
//                       plist.Connect_Completion_Percent__c=Percent;
//                       plist.ACD_Connect__c=date.valueOf(ACDDate);
//                       plist.ActuaPercent_Connect1__c=Percent;
//                       plist.OriginalACD_Connect__c = date.valueOf(OriginalACD);
//                       plist.ProjectNum_Connect__c = ProjNum;
//                       plist.Rera_Inspection_Dt_Connect__c = date.valueOf(ReraInspecDate);
//                       plist.PlannedPerc_Connect__c=PlannedPerc;
//                       plist.LastRERAPerc_Connect__c=LastRERAPerc;
//                       BUList.add(plist);
//                 }
        
//        system.debug('list'+BUList);           
//          if(BUList.size() > 0){
//                     try{
//                     update BUList;
//                     RERA_Percent__c ReraP = new RERA_Percent__c();
//                     ReraP.ACD_Connect__c=date.valueOf(ACDDate);ReraP.BU__c=BUList[0].Id;
//                     ReraP.ActuaPercent_Connect__c=Percent; //ReraP.Location__c=LocId[0].Id;
//                     ReraP.LastRERAPerc_Connect__c=LastRERAPerc;ReraP.OriginalACD_Connect__c=date.valueOf(OriginalACD); 
//                     ReraP.PlannedPerc_Connect__c = PlannedPerc;
//                     ReraP.Rera_Inspection_Dt_Connect__c=date.valueOf(ReraInspecDate);
//                     ReraP.Source__c='Connect';ReraPercent.add(ReraP);if(ReraPercent.size() > 0){Insert ReraPercent;}
//                     objResponseWrapper.status = 'Details Updated Sucessfully';
//                     objResponseWrapper.statusCode = '200';
//                 } 
//                 catch( Exception ex ) {
//                     system.debug( ex.getMessage() );
//                     objResponseWrapper.errorMessage = ex.getMessage();
//                     objResponseWrapper.statusCode = '400';
//                 }   
//             }
//    }
//        return objResponseWrapper;

// }
    //for sending response    
    global class ResponseWrapper {
        global String status;
        global String statusCode;
        global String errorMessage;
        global String units; //Added by AT
       
        global ResponseWrapper() {
            this.status = '';
            this.statusCode = '';
            this.errorMessage = '';
            this.units = '';
        }
    }

}