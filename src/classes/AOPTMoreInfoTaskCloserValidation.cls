public without sharing class AOPTMoreInfoTaskCloserValidation {

    @InvocableMethod
    public static void createTaskIPMSAgain( List<Task> lstTask ) {
        set<String> setTaskIds = new set<String>();
        
        for(Task objT : lstTask ){
            if( objT.Status.equalsIgnoreCase('Completed') 
             && objT.Subject.equalsIgnoreCase('Provide Missing details')
             && string.valueOf(objT.WhatId).startsWith('500')
             && objT.Process_Name__c.contains('AOPT')
             && String.isNotBlank(objT.Parent_Task_Id__c)){
                setTaskIds.add(objT.Parent_Task_Id__c);
            }
        }
        
        if( !setTaskIds.isEmpty() ) {
            list<Task> lstTask1 = new list<Task>();
            for(Task objT : [Select t.WhatId
                                  , t.Subject
                                  , t.Status
                                  , t.Process_Name__c
                                  , t.Priority
                                  , t.Id
                                  , t.IPMS_Role__c
                                  , t.CurrencyIsoCode
                                  , t.Assigned_User__c
                                  , t.ActivityDate 
                            From Task t
                            where Id IN : setTaskIds]){
                Task objTask = objT;
                objTask.Id = null;
                objT.Status = 'Not Started';
                objT.ActivityDate = date.today();
                lstTask1.add(objT);
            }
            
            system.debug('lstTask1***********'+lstTask1);
            
            if( !lstTask1.isEmpty() ) {
                insert lstTask1;
            }
        }   
    }
}