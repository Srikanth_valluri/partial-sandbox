/*
 * Description: This batch is used to make callout to IPMS and store the Title Deed Attachements
 */
public class UploadDocToCentralRepoBatch implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {

    Set<Id> setSRAttachmentsId;
    Map<Id, Case> mapAllCases = new Map<Id, Case>();
    Integer intIncrementor = 1;
	String query = 'SELECT Id, Name, Case__c, Attachment_URL__c FROM SR_Attachments__c WHERE Id IN: setSRAttachmentsId';

    public UploadDocToCentralRepoBatch(Set<Id> setAttachmentsId) {
        setSRAttachmentsId = setAttachmentsId;
    }

    public Database.QueryLocator start(Database.BatchableContext BC) {
        if(Test.isRunningTest()) {
        	query += ' Limit 1';
        }
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<SR_Attachments__c> scope) {
        Set<Id> setCaseId = new Set<Id>();
        Id caseId;
        List<SR_Attachments__c> lstAttachments = new List<SR_Attachments__c>();

        // Iterate over the SR Attachments and fetch the Case Id
        for (SR_Attachments__c objAttachment : scope) {
            if (!mapAllCases.containsKey(objAttachment.Case__c)) {
                setCaseId.add(objAttachment.Case__c);
            }
        }

        // If there is a case which is not present in the "mapAllCases" then fetch the case details and
        // push it to the map.
        if (setCaseId.size() > 0) {
            Map<Id, Case> mapNewCases = new Map<Id, Case>([Select Id, CaseNumber, Account.Party_ID__c From Case Where Id IN: setCaseId]);
            mapAllCases.putAll(mapNewCases);
        }

        // Iterate over the "SR Attachments" and make callout to IPMS
        try {
            for(SR_Attachments__c objSRAttachment : scope) {
                caseId = objSRAttachment.Case__c;
                if(String.isNotBlank(objSRAttachment.Attachment_URL__c)) {

                    // Perform http callout to fetch blob content from file url
                    HttpRequest req = new HttpRequest();
                    req.setEndpoint(objSRAttachment.Attachment_URL__c);
                    req.setTimeout(120000);
                    req.setMethod('GET');

                    Http http = new Http();
                    HTTPResponse res = http.send(req);
                    system.debug('--res.getStatus--'+res.getStatus());
                    Blob pdfContent = res.getBodyAsBlob();

                    UploadMultipleDocController.MultipleDocRequest objDocRequest = new UploadMultipleDocController.MultipleDocRequest();
                    objDocRequest.category =  'Document';
                    objDocRequest.entityName = 'Damac Service Requests';
                    objDocRequest.fileDescription = objSRAttachment.Name;

                    intIncrementor++;
                    objDocRequest.fileId = mapAllCases.get(objSRAttachment.Case__c).CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+objSRAttachment.Name;
                    objDocRequest.fileName = mapAllCases.get(objSRAttachment.Case__c).CaseNumber+'-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+'.'+objSRAttachment.Name;

                    objDocRequest.registrationId =  mapAllCases.get(objSRAttachment.Case__c).CaseNumber;

                    objDocRequest.sourceFileName  = 'IPMS-'+mapAllCases.get(objSRAttachment.Case__c).Account.party_ID__C+'-'+objSRAttachment.Name;
                    objDocRequest.sourceId  =  'IPMS-'+mapAllCases.get(objSRAttachment.Case__c).Account.party_ID__C+'-'+objSRAttachment.Name;

                    objDocRequest.base64Binary =  EncodingUtil.base64Encode(pdfContent);

                    UploadMultipleDocController.data objResponseData = new UploadMultipleDocController.data();
                    system.debug('--objResponseData--'+objResponseData);
                    objResponseData = UploadMultipleDocController.getMultipleDocUrl(new List<UploadMultipleDocController.MultipleDocRequest>{objDocRequest});

                    // Set the new url returned from IPMS to SR Attachment object's URL field
                    if (objResponseData != null && objResponseData.data[0].PROC_STATUS == 'S' && String.isNotBlank(objResponseData.data[0].url)) {
                        objSRAttachment.Attachment_URL__c = objResponseData.data[0].url;
                        lstAttachments.add(objSRAttachment);
                    }
                }
            }
            update lstAttachments;
        } catch (Exception objException) {
            Error_Log__c objError = new Error_Log__c(Error_Details__c = objException.getMessage(), Process_Name__c = 'Title Deed', Case__c = caseId);
            insert objError;
        }
    }

    public void finish(Database.BatchableContext BC) {
    }

}