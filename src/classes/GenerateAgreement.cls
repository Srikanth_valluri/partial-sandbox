public with sharing class GenerateAgreement{ 
    
    public GenerateAgreement(ApexPages.standardController controller){      
        
    }//End constructor
    
    public pagereference init(){ 
        string strPageID = ApexPages.currentPage().getParameters().get('id');
        list<Error_Log__c> lstErrorLog = new list<Error_Log__c>();
        List<SR_Attachments__c> lstSpaAttachment = new List<SR_Attachments__c>();
        List<spaDetailsWrapper> lstSPADetails = new List<spaDetailsWrapper>(); 
        list<spaProcessXxdcProcessServiceWsPlsqlSo.APPSXXDC_PROCESS_SERX1794747X1X5> listSPAParam 
                  = new list<spaProcessXxdcProcessServiceWsPlsqlSo.APPSXXDC_PROCESS_SERX1794747X1X5>();
        case objCase = [select CaseNumber, AccountId, Booking_Unit__c, Booking_Unit__r.Registration_ID__c, RecordType.DeveloperName from case where id =:strPageID];
        spaProcessXxdcProcessServiceWsPlsqlSo.APPSXXDC_PROCESS_SERX1794747X1X5 objSPAParam
                  = new spaProcessXxdcProcessServiceWsPlsqlSo.APPSXXDC_PROCESS_SERX1794747X1X5();
              objSPAParam.ATTRIBUTE1 = 'GENERATE';
              objSPAParam.ATTRIBUTE2 = objCase.Booking_Unit__r.Registration_ID__c ;
              objSPAParam.ATTRIBUTE3 = 'LAG';
              objSPAParam.PARAM_ID = '2-'+objCase.CaseNumber;
              system.debug('objSPAParam=='+objSPAParam);
              listSPAParam.add(objSPAParam);
        try{
            spaGeneration.SPAHttpSoap11Endpoint objSPA = new spaGeneration.SPAHttpSoap11Endpoint();
            objSPA.timeout_x = 120000;
            String spaResponse = objSPA.processSPA('2-'+String.valueOf(System.currentTimeMillis()), 'SPA', 'SFDC', listSPAParam);
            system.debug('spaResponse==='+spaResponse);
            if(String.isNotBlank(spaResponse)) {
                map<String, Object> mapDeserializeSPA = (map<String,Object>)JSON.deserializeUntyped(spaResponse);
                system.debug('mapDeserializeSPA==='+mapDeserializeSPA);
                if(mapDeserializeSPA.get('status') == 'S'){
                    String StringSPAData = JSON.serialize(mapDeserializeSPA.get('data'));
                    system.debug('StringSPAData==='+StringSPAData);
                    lstSPADetails = 
                       (List<spaDetailsWrapper>)JSON.deserialize(StringSPAData, List<spaDetailsWrapper>.class);
                        system.debug('lstSPADetails==='+lstSPADetails);
                        
                        
                            for(spaDetailsWrapper spaInst: lstSPADetails) {                        
                               if(spaInst.PROC_STATUS == 'S') {
                                   if(spaInst.ATTRIBUTE1 != 'null' && spaInst.ATTRIBUTE1 != null) {
                                       if(spaInst.ATTRIBUTE3 != 'null' && spaInst.ATTRIBUTE3 != null && spaInst.ATTRIBUTE3 == 'LAG') {
                                           SR_Attachments__c spaAttachment = new SR_Attachments__c();
                                           spaAttachment.Name = 'Standard Lease Agreement Document';
                                           spaAttachment.isValid__c = true;
                                           spaAttachment.IsRequired__c = true;
                                           spaAttachment.Case__c = objCase.Id;
                                           spaAttachment.Attachment_URL__c = spaInst.ATTRIBUTE1;
                                           lstSpaAttachment.add(spaAttachment);
                                           
                                           if (objCase.RecordType.DeveloperName.equalsIgnoreCase('Early_Handover')) {
                                               SR_Attachments__c signedAttachment = new SR_Attachments__c();
                                               signedAttachment.Name = 'Signed Lease Agreement';
                                               signedAttachment.Case__c = objCase.Id;
                                               lstSpaAttachment.add(signedAttachment);
                                           }
                                        }
                                   }
                                   
                               }else if(spaInst.PROC_STATUS == 'E'){
                                    Error_Log__c objErr = new Error_Log__c();
                                    objErr.Account__c = objCase.AccountId;
                                    objErr.Booking_Unit__c = objCase.Booking_Unit__c;
                                    objErr.Case__c = objCase.id;
                                    objErr.Error_Details__c = spaInst.PROC_MESSAGE;
                                    objErr.Process_Name__c = 'Early Handover';
                                    lstErrorLog.add(objErr);
                               }
                            }
                        
                        system.debug('!!!!!!!lstSpaAttachment'+lstSpaAttachment);
                        if (lstSpaAttachment.size() == 0) {
                            system.debug('!!!inside null spa');
                            SR_Attachments__c spaAttachment = new SR_Attachments__c();
                            spaAttachment.Name = 'Standard Lease Agreement Document';
                            spaAttachment.isValid__c = true;
                            spaAttachment.IsRequired__c = true;
                            spaAttachment.Case__c = objCase.Id;
                            spaAttachment.Attachment_URL__c = '';
                            lstSpaAttachment.add(spaAttachment);
                                       
                            if (objCase.RecordType.DeveloperName.equalsIgnoreCase('Early_Handover')) {
                                SR_Attachments__c signedAttachment = new SR_Attachments__c();
                                signedAttachment.Name = 'Signed Lease Agreement';
                                signedAttachment.Case__c = objCase.Id;
                                lstSpaAttachment.add(signedAttachment);
                            }
                        }
                    }
                }
            } catch(Exception ex) {
                Error_Log__c objErr = new Error_Log__c();
                objErr.Account__c = objCase.AccountId;
                objErr.Booking_Unit__c = objCase.Booking_Unit__c;
                objErr.Case__c = objCase.id;
                objErr.Error_Details__c = ex.getMessage();
                objErr.Process_Name__c = 'Early Handover';
                lstErrorLog.add(objErr);
            }      

            system.debug('lstCaseAttachment '+lstSpaAttachment );
            if (lstSpaAttachment != null && lstSpaAttachment.size() >0){
                insert lstSpaAttachment;
            }
            
            if (lstErrorLog != null && lstErrorLog.size()>0){
                insert lstErrorLog;
            }
            list<Case> lstCase = new list<Case>();        
            for (Case objCase1 : [Select Id, Early_Handover_Status__c From Case Where Id =:strPageID]) {
                objCase1.Early_Handover_Status__c = 'Agreement Generated';
                lstcase.add(objCase1);
            }
            update lstCase;                    
            
            pagereference newpg = new Pagereference(URL.getSalesforceBaseUrl().toExternalForm()+'/'+strPageID);
            newpg.setRedirect(true);
            return newpg;
        }
        
    public class spaDetailsWrapper {
        public String PROC_STATUS {get;set;}
        public String PROC_MESSAGE {get;set;}
        public String ATTRIBUTE1 {get;set;}
        public String ATTRIBUTE2 {get;set;}
        public String ATTRIBUTE3 {get;set;}
        public String ATTRIBUTE4 {get;set;}
        public String PARAM_ID {get;set;}

        public spaDetailsWrapper(){
        }
   }                       
}