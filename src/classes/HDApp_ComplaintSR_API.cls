/**********************************************************************************************************************
Description: This API is used for creating Complaints/Suggestion SR raised from Damac Living App
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   14-12-2020      | Shubham Suryawanshi | Created initial draft
***********************************************************************************************************************/
@RestResource(urlMapping='/complaintSR/*')
global class HDApp_ComplaintSR_API {

    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };
    public static Id suggestionRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName()
                                        .get('Suggestion').getRecordTypeId();
    public static final String UNIT_HANDEDOVER = 'Handed Over';
    public static final String UNIT_EARLY_HANDEDOVER = 'Early Handed Over';
    public static final String commonErrorMsg = 'This may be because of technical error that we\'re working to get fixed. Please try again later.';


    /**********************************************************************************************************************
    Description : Method to get the complaints lookup data
    Parameter(s):  NA
    Return Type : FinalReturnWrapper
    **********************************************************************************************************************/
    @HttpGet
    global static FinalReturnWrapper getComplaintsLookupData() {

        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);
        
        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        cls_data objData = new cls_data();
        cls_meta_data objMeta = new cls_meta_data();

        if(!r.params.containsKey('accountId')) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'Missing paramater : accountId', 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(r.params.containsKey('accountId') && String.isBlank(r.params.get('accountId'))) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'Missing paramater value : accountId', 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;   
        }

        //Get all the units for Account
        List<Booking_Unit__c> allUnits = HDApp_Utility.getAllUnitsFromAccountId(r.params.get('accountId'));
        if(allUnits.isEmpty() || allUnits.size() < 1 ) {
            objMeta = ReturnMetaResponse('No Booking Units found','No booking units found for given accountId', 2);
            returnResponse.meta_data = objMeta;
            return returnResponse;  
        }

        //Creating - LookupDataWrapper
        List<LookupDataWrapper> lstLookupData = new List<LookupDataWrapper>();
        
        LookupDataWrapper objLookupData = new LookupDataWrapper();
        objLookupData.type_id = 1;
        objLookupData.name = 'Complaints';   //for Case record
        Map<String, List<String>> mapCaseComplaintTypes = HDApp_Utility.getDependentPicklistValues(Case.Complaint_Sub_Type__c);
        System.debug('mapCaseComplaintTypes: ' + mapCaseComplaintTypes);
        if(!mapCaseComplaintTypes.isEmpty()) {
            //following values are not coming in dependep picklist map, so added manually           
            //mapCaseComplaintTypes.put('Damac CRM', null );
            //mapCaseComplaintTypes.put('Damac - Non CRM', null);
            mapCaseComplaintTypes.put('Maintenance',null);
            objLookupData.lookup_types = createLookupTypesData(mapCaseComplaintTypes);
            objLookupData.lookup_types.sort();
        }
        //Adding document placeholder data as its applicable in customer poratls's - Complaint section
        List<DocWrapper> lstDocs = new List<DocWrapper>();
        DocWrapper objDoc = new DocWrapper();
        objDoc.document_id = 1;
        objDoc.document_name = 'Attachment 1';
        lstDocs.add(objDoc);

        //objDoc = new DocWrapper();
        //objDoc.document_id = 2;
        //objDoc.document_name = 'Attachment 2';
        //lstDocs.add(objDoc); //Commented this as only one document to be uploaded (as discussed with Charith)
        objLookupData.applicable_documents = lstDocs;
        lstLookupData.add(objLookupData);

        objLookupData = new LookupDataWrapper();
        objLookupData.type_id = 2;
        objLookupData.name = 'Suggestions';  //for Fm-Case record
        Map<String, List<String>> mapFmSuggestionTypes = HDApp_Utility.getDependentPicklistValues(FM_Case__c.Suggestion_Sub_Type__c);
        System.debug('mapFmSuggestionTypes: ' + mapFmSuggestionTypes);
        if(!mapFmSuggestionTypes.isEmpty()) {
            //following values are not coming in dependep picklist map, so added manually
            mapFmSuggestionTypes.put('Portal', null);
            mapFmSuggestionTypes.put('Service Charges', null);
            mapFmSuggestionTypes.put('Other', null);
            objLookupData.lookup_types = createLookupTypesData(mapFmSuggestionTypes);
            objLookupData.lookup_types.sort();
        }
        lstLookupData.add(objLookupData);

        objLookupData = new LookupDataWrapper();
        objLookupData.type_id = 3;
        objLookupData.name = 'Complaints';   //for Fm-Case record
        Map<String, List<String>> mapFmComplaintTypes = HDApp_Utility.getDependentPicklistValues(FM_Case__c.Suggestion_Sub_Type__c);
        System.debug('mapFmComplaintTypes: ' + mapFmComplaintTypes);
        if(!mapFmComplaintTypes.isEmpty()) {
            //following values are not coming in dependep picklist map, so added manually
            mapFmComplaintTypes.put('Portal', null);
            mapFmComplaintTypes.put('Service Charges', null);
            mapFmComplaintTypes.put('Other', null);
            objLookupData.lookup_types = createLookupTypesData(mapFmComplaintTypes);
            objLookupData.lookup_types.sort();
        }
        lstLookupData.add(objLookupData);
        System.debug('lstLookupData:: ' + lstLookupData);

        //Creating - unit_list
        List<Integer> intComplaintType_nonHandedOver = new List<Integer>{1};
        List<Integer> intComplaintType_handedOver = new List<Integer>{2,3};

        List<UnitListWrapper> lstUnitWrap = new List<UnitListWrapper>();
        for(Booking_Unit__c objBU : allUnits) {
            UnitListWrapper objUnitWrap = new UnitListWrapper();
            objUnitWrap.unit_name = objBU.Unit_Name__c;
            objUnitWrap.unit_id = objBU.id;
            objUnitWrap.reg_id = objBU.Registration_ID__c;
            objUnitWrap.building_name = objBU.Inventory__r.Building_Location__r.Building_Name__c;
            objUnitWrap.is_handed_over = objBU.Handover_Status__c != UNIT_HANDEDOVER && objBU.Handover_Status__c != UNIT_EARLY_HANDEDOVER ? false : true;
            objUnitWrap.applicable_complaint_type = objUnitWrap.is_handed_over ? intComplaintType_handedOver : intComplaintType_nonHandedOver;

            lstUnitWrap.add(objUnitWrap);
            System.debug('lstUnitWrap: ' + lstUnitWrap);
        }

        List<Case> lstPastComplaints = HDApp_getComplaintSRs_API.getComplaintCases(r.params.get('accountId'));
        List<FM_Case__c> lstPastSuggestion = HDApp_getComplaintSRs_API.getFMCaseSuggestions(r.params.get('accountId'));
        Integer totalPastCases = 0;
        if(lstPastComplaints.size() > 0 || lstPastSuggestion.size() > 0) {
            totalPastCases += lstPastComplaints.size() + lstPastSuggestion.size();
        }

        objData.unit_list = lstUnitWrap;
        objData.total_request_count = totalPastCases;
        objData.lookup_datails = lstLookupData;
        objMeta = ReturnMetaResponse('Success','', 1);

        returnResponse.data = objData;
        returnResponse.meta_data = objMeta;

        return returnResponse;
    }

    public static List<LookupTypesWrapper> createLookupTypesData(Map<String, list<String>> mapValues) {
        
        List<LookupTypesWrapper> lstLookupTypes = new List<LookupTypesWrapper>();
        
        for(String objKey : mapValues.keySet()) {
            LookupTypesWrapper objLTwrap = new LookupTypesWrapper();

            objLTwrap.name = objkey;
            objLTwrap.sub_types = mapValues.get(objKey);
            lstLookupTypes.add(objLTwrap);
        }
        return lstLookupTypes;
    }

    /**********************************************************************************************************************
    Description : Method to submit the complaints
    Parameter(s):  NA
    Return Type : FinalReturnWrapperSubmit
    **********************************************************************************************************************/
    @httpPost
    global static FinalReturnWrapperSubmit submitComplaintSRCase() {

        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);

        FinalReturnWrapperSubmit returnResponse = new FinalReturnWrapperSubmit();
        //ComplaintInputWrapper objInputWrap = new ComplaintInputWrapper();
        cls_meta_data objMeta = new cls_meta_data();
        cls_data_submit objData = new cls_data_submit();

        if(!r.params.containsKey('accountId')) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'Missing paramater : accountId', 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(r.params.containsKey('accountId') && String.isBlank(r.params.get('accountId'))) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'Missing paramater value : accountId', 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;   
        }
        if(!r.params.containsKey('bookingUnitId')) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'Missing paramater : bookingUnitId', 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(r.params.containsKey('bookingUnitId') && String.isBlank(r.params.get('bookingUnitId'))) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'Missing paramater value : bookingUnitId', 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;   
        }
        if(!r.params.containsKey('typeId')) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'Missing paramater : typeId', 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(r.params.containsKey('typeId') && String.isBlank(r.params.get('typeId'))) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'Missing paramater value : typeId', 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;   
        }
        if(!r.params.containsKey('typeName')) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'Missing paramater : typeName', 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(r.params.containsKey('typeName') && String.isBlank(r.params.get('typeName'))) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'Missing paramater value : typeName', 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;   
        }
        System.debug('typeId : ' + r.params.get('typeId'));
        List<Account> account = HDApp_Utility.getAccountFromAccountId(r.params.get('accountId'));
        List<Booking_Unit__c> unitList = HDApp_Utility.getUnitsFromBUId(r.params.get('bookingUnitId'));

        if(account.isEmpty()) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'No account found for paramater value : accountId', 6);
            returnResponse.meta_data = objMeta;
            return returnResponse; 
        }
        if(unitList.isEmpty()) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'No Booking Unit found for paramater value : bookingUnitId', 6);
            returnResponse.meta_data = objMeta;
            return returnResponse; 
        }

        //bifurcation for case creation based on typeId (typeId 1 = Case, typeId 2 = FM-Case)
        if(r.params.get('typeId') == '1') {
            returnResponse = createCaseComplaint(r, account[0], unitList[0]);
        }
        else if(r.params.get('typeId') == '2' || r.params.get('typeId') == '3') {
            returnResponse = createFMSuggestion(r, account[0], unitList[0]);
        }
        else {
            objMeta = ReturnMetaResponse(commonErrorMsg,'Invalid typeId passed. Expected typeId is 1/2/3', 6);
            returnResponse.meta_data = objMeta;
            return returnResponse; 
        }

        return returnResponse;
    }

    public static FinalReturnWrapperSubmit createCaseComplaint( RestRequest req, Account account, Booking_Unit__c unit ) {
    
        FinalReturnWrapperSubmit objRetWrap = new FinalReturnWrapperSubmit();
        cls_meta_data objMeta = new cls_meta_data();
        cls_data_submit objData = new cls_data_submit();

        Case complaintCaseObj = new Case();
        complaintCaseObj.isHelloDamacAppCase__c = true;
        complaintCaseObj.AccountId = account.id;
        complaintCaseObj.Booking_Unit__c = unit.id;
        complaintCaseObj.Type = ComplaintProcessUtility.COMPLAINT_CASE_TYPE;
        complaintCaseObj.RecordTypeId = ComplaintProcessUtility.getRecordTypeId();
        complaintCaseObj.Origin = ComplaintProcessUtility.COMPLAINT_PORTAL_ORIGIN;
        complaintCaseObj.Status=ComplaintProcessUtility.COMPLAINT_SUBMIT_STATUS;
        complaintCaseObj.Origin = 'Portal';
        complaintCaseObj.Account_Email__c = account.IsPersonAccount ? account.Email__pc : account.Email__c ;
        complaintCaseObj.Complaint_Type__c = req.params.get('typeName');
        complaintCaseObj.Complaint_Sub_Type__c = req.params.get('subType');
        complaintCaseObj.Description = req.params.get('description');

        try {
            insert complaintCaseObj;
        }
        catch(Exception e) {
            objMeta = ReturnMetaResponse(commonErrorMsg,e.getMessage(), 6);
            objRetWrap.meta_data = objMeta;
            return objRetWrap;
        }

        //if Doc is present:
        Blob body = req.requestBody;
        String requestString = EncodingUtil.Base64Encode(body);
        System.debug('requestString: '+requestString);
        if(String.isNotBlank(requestString)) {
            
            if(!req.params.containsKey('documentName')) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'Missing paramater : documentName', 6);
            objRetWrap.meta_data = objMeta;
            return objRetWrap;
            }
            else if(req.params.containsKey('documentName') && String.isBlank(req.params.get('documentName'))) {
                objMeta = ReturnMetaResponse(commonErrorMsg,'Missing paramater value : documentName', 6);
                objRetWrap.meta_data = objMeta;
                return objRetWrap;   
            }
            if(!req.params.containsKey('fileName')) {
                objMeta = ReturnMetaResponse(commonErrorMsg,'Missing paramater : fileName', 6);
                objRetWrap.meta_data = objMeta;
                return objRetWrap;
            }
            else if(req.params.containsKey('fileName') && String.isBlank(req.params.get('fileName'))) {
                objMeta = ReturnMetaResponse(commonErrorMsg,'Missing paramater value : fileName', 6);
                objRetWrap.meta_data = objMeta;
                return objRetWrap;   
            }

            //calling future method for doc upload;
            uploadDocumentProcess(requestString, req.params.get('documentName'), req.params.get('fileName'), complaintCaseObj.Id, account.Id);
            
        }//

        Case caseObj = HDApp_Utility.getCaseDetails(complaintCaseObj.id)[0];


        Task objTask = new Task();
        try{
            objTask = TaskUtility.getTask((SObject)caseObj, 'New Complaint Request', 'CRE',
                             'Complaint', system.today().addDays(1));

            objTask.OwnerId = Label.DefaultCaseOwnerId;
            objTask.Priority = 'High';
            objTask.Status = 'In Progress';
            objTask.currencyISOcode = caseObj.currencyISOcode ;
            System.debug('--objTask--Promotions'+objTask);

            insert objTask;
            System.debug('>>>>>>>objTask'+objTask);
        }
        catch(exception e){
            objMeta = ReturnMetaResponse(commonErrorMsg,e.getMessage(), 6);
            objRetWrap.meta_data = objMeta;
            return objRetWrap;
        }

        objData.sr_id = caseObj.id;
        objData.sr_number = caseObj.CaseNumber;
        objData.unit_id = caseObj.Booking_Unit__c;
        objData.unit_name = unit.Unit_Name__c;
        objData.complaint_type = caseObj.Complaint_Type__c;
        objData.complaint_sub_type = caseObj.Complaint_Sub_Type__c;
        objData.submitted_date =  caseObj.CreatedDate != null ? Datetime.newInstance(caseObj.CreatedDate.year(), caseObj.CreatedDate.month(), caseObj.CreatedDate.day()).format('yyyy-MM-dd') : '';
        objMeta = ReturnMetaResponse('success','', 1);

        objRetWrap.meta_data = objMeta;
        objRetWrap.data = objData;
        return objRetWrap;
    }

    public static FinalReturnWrapperSubmit createFMSuggestion( RestRequest req, Account account, Booking_Unit__c unit ) {

        FinalReturnWrapperSubmit objRetWrap = new FinalReturnWrapperSubmit();
        cls_meta_data objMeta = new cls_meta_data();
        cls_data_submit objData = new cls_data_submit();
        Map<Id, Map<String, Id>> mapBuildingToRolesToUserId = new Map<Id, Map<String, Id>>();
        mapBuildingToRolesToUserId = FM_Utility.getFmUsersByLocationAndRole(new Set<Id>{unit.Inventory__r.Building_Location__c});
        Location__c building = unit.Inventory__r.Building_Location__r;
        System.debug('mapBuildingToRolesToUserId: ' + mapBuildingToRolesToUserId);
        System.debug('building: ' + building);

        FM_Case__c objFmCase = new FM_Case__c();
        objFmCase.isHelloDamacAppCase__c = true;
        objFmCase.Origin__c = 'Portal';
        objFmCase.RecordTypeId = suggestionRecordTypeId;
        //objFmCase.Request_Type__c = req.params.get('typeId') == '2' ? 'Suggestion' : 'Complaint';
        objFmCase.Request_Type__c = 'Suggestion';
        objFmCase.Type__c = req.params.get('typeId') == '2' ? 'Suggestion' : 'Complaint';
        objFmCase.Request_Type_DeveloperName__c = 'Suggestion';
        objFmCase.Account__c = account.Id;
        objFMCase.Account_Email__c = account.IsPersonAccount ? account.Email__pc : account.Email__c ;
        objFmCase.Booking_Unit__c = unit.Id;
        objFmCase.Submitted__c = true;
        objFmCase.Status__c = 'Submitted';
        objFmCase.Admin__c = Label.FM_Collection_User_Id;
        objFmCase.Email_2__c = Label.Customer_Happiness_Centre_email;
        objFmCase.Email_3__c = Label.FM_Collection_User_Email;
        objFmCase.Suggestion_Type__c = req.params.get('typeName');
        objFmCase.Suggestion_Sub_Type__c = req.params.get('subType');
        objFMCase.Description__c = req.params.get('description');
        if (building != NULL) {
            objFmCase.Email__c = building.Loams_Email__c;
            objFmCase.Property_Manager__c = mapBuildingToRolesToUserId.get(building.Id).get('Property Manager');
            objFmCase.Property_Director__c = mapBuildingToRolesToUserId.get(building.Id).get('Property Director');
            for (User user : [
                SELECT  Id
                        , Email
                FROM    User
                WHERE   Id = :objFmCase.Property_Manager__c
                    OR  Id = :objFmCase.Property_Director__c
            ]) {
                if (user.Id == objFmCase.Property_Manager__c) {
                    objFmCase.Property_Manager_Email__c = user.Email;
                } else if (user.Id == objFmCase.Property_Director__c) {
                    objFmCase.Property_Director_Email__c = user.Email;
                }
            }
        }

        Datetime today = DateTime.newInstance(Date.today(), Time.newInstance(0, 0, 0, 0));
        String dayOfWeek = today.format('EEEE');
        if (dayOfWeek == 'Sunday' || dayOfWeek == 'Monday'){
            objFmCase.Suggestion_Escalation_Date__c = system.today().addDays(3);
        } else if (dayOfWeek == 'Tuesday' || dayOfWeek == 'Wednesday' || dayOfWeek == 'Thurday' ||
            dayOfWeek == 'Friday'){
            objFmCase.Suggestion_Escalation_Date__c = system.today().addDays(5);
        } else if (dayOfWeek == 'Saturday'){
            objFmCase.Suggestion_Escalation_Date__c = system.today().addDays(4);
        }

        try {
            insert objFmCase;
        }
        catch (Exception e) {
            objMeta = ReturnMetaResponse(commonErrorMsg,e.getMessage(), 6);
            objRetWrap.meta_data = objMeta;
            return objRetWrap;
        }

        List<FM_Case__c> objFetchedFmCase = GetTenantRegistrationCase_API.getFMCase(objFmCase.id);
        objData.sr_id = objFetchedFmCase[0].id;
        objData.sr_number = objFetchedFmCase[0].Name;
        objData.unit_id = objFetchedFmCase[0].Booking_Unit__c;
        objData.unit_name = unit.Unit_Name__c;
        objData.complaint_type = objFetchedFmCase[0].Suggestion_Type__c;
        objData.complaint_sub_type = objFetchedFmCase[0].Suggestion_Sub_Type__c;
        objData.submitted_date =  objFetchedFmCase[0].Submission_Date__c != null ? Datetime.newInstance(objFetchedFmCase[0].Submission_Date__c.year(), objFetchedFmCase[0].Submission_Date__c.month(), objFetchedFmCase[0].Submission_Date__c.day()).format('yyyy-MM-dd') : '';
        objMeta = ReturnMetaResponse('success','', 1);

        objRetWrap.meta_data = objMeta;
        objRetWrap.data = objData;
        return objRetWrap;

    }

    //Future callout
    @future(callout=true)
    public static void uploadDocumentProcess(String requestString, String documentName, String fileNameStr, String caseId,  String accId) {

        String strDocumentName = documentName;
        String fileName = fileNameStr;
        Case caseObj = HDApp_Utility.getCaseDetails(caseId)[0];
        Account account = HDApp_Utility.getAccountFromAccountId(accId)[0];

        List<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocReq = new List<UploadMultipleDocController.MultipleDocRequest>();
        UploadMultipleDocController.MultipleDocRequest objDocWrap = HDApp_Utility.getCaseUploadDocumentWrapper( requestString, fileName, caseObj.CaseNumber );
        if(objDocWrap != null) {
            lstMultipleDocReq.add(objDocWrap);
        }

        UploadMultipleDocController.data objResponseData = new UploadMultipleDocController.data();
        objResponseData = UploadMultipleDocController.getMultipleDocUrl(lstMultipleDocReq);
        system.debug('objResponseData DOC*****'+objResponseData);

        List<Case> caseToUpdate = new List<Case>();
        List<SR_Attachments__c> lstSRAttachToInsert = new List<SR_Attachments__c>();
        if(objResponseData != null && objResponseData.status == 'S' && objResponseData.Data != null && objResponseData.Data.size() > 0) {
            for(UploadMultipleDocController.MultipleDocResponse objResData : objResponseData.Data) {
                System.debug('objResData:: ' + objResData);
                
                if(String.isNotBlank(objResData.url)) {
                    caseObj.Additional_Doc_File_URL__c  = objResData.url;
                    caseToUpdate.add(caseObj);

                    SR_Attachments__c objSRAttach = createSRAttachment(caseObj.id, account.id, extractType(fileName), ComplaintProcessUtility.COMPLAINT_ATT1_NAME , objResData.url );
                    lstSRAttachToInsert.add(objSRAttach);
                    insert objSRAttach;
                }
            }
         }
        if(!caseToUpdate.isEmpty()) {
            try {
                update caseToUpdate;
            }
            catch(exception e){
                System.debug('exception: ' + e.getMessage());
                //objMeta = ReturnMetaResponse(commonErrorMsg,e.getMessage(), 6);
                //objRetWrap.meta_data = objMeta;
                //return objRetWrap;
            }
        }
        if(!lstSRAttachToInsert.isEmpty()) {
            try {
                insert lstSRAttachToInsert;
            }
            catch(exception e){
                System.debug('exception: ' + e.getMessage());
                //objMeta = ReturnMetaResponse(commonErrorMsg,e.getMessage(), 6);
                //objRetWrap.meta_data = objMeta;
                //return objRetWrap;
            }
        }
    }


    public static String extractName( String strName ) {
      return strName.substring( strName.lastIndexOf('\\')+1 ) ;
    }

    public static String extractType(String fileName) {
        String strFileExtension;
        if( String.isNotBlank( fileName ) && fileName.lastIndexOf('.') != -1 ) {
            strFileExtension = fileName.substring( fileName.lastIndexOf('.')+1 , fileName.length() );
            strFileExtension = strFileExtension.substringBefore('&');
        }

        System.debug('strFileExtension:: ' + strFileExtension);
        return strFileExtension;
    }

    public static SR_Attachments__c createSRAttachment(String caseId, String accountId, String strType, String strAttachmentName, String fileUrl) {
        SR_Attachments__c srAttchmentObj = new SR_Attachments__c(Case__c = caseId,Account__c = accountId,Type__c = strType, Name=strAttachmentName, Attachment_URL__c = fileUrl);
        return srAttchmentObj;
    }
    
    public static cls_meta_data ReturnMetaResponse(String message, String devMsg, Integer statusCode) {
        cls_meta_data retMeta = new cls_meta_data();
        retMeta.message = message;
        retMeta.status_code = statusCode;
        retMeta.title = mapStatusCode.get(statusCode);
        retMeta.developer_message = devMsg;
        return retMeta;
    }

    /*************************************************/
    /*     Wrapper classes declarations below        */
    /*************************************************/

    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message;   
    }
 
    /************************************************/
    /* For GET method */
    /************************************************/
    public class cls_data {
        public Integer total_request_count;
        public LookupDataWrapper[] lookup_datails; 
        public UnitListWrapper[] unit_list;
    }

    public class LookupDataWrapper{
        public Integer type_id;
        public String name;
        public LookupTypesWrapper[] lookup_types;
        public DocWrapper[] applicable_documents;
    }

    public class LookupTypesWrapper implements Comparable {
        public String name;
        public String[] sub_types; 
        public Integer compareTo(Object compareTo) {
            LookupTypesWrapper compareInstance = (LookupTypesWrapper)compareTo;
            // The return value of 0 indicates that both elements are equal

            //Ascending
            Integer returnValue = 0;
            if (name > compareInstance.name) {
                // Set return value to a positive value.
                returnValue = 1;
            } else if (name < compareInstance.name) {
                // Set return value to a negative value.
                returnValue = -1;
            }
            return returnValue;       
        }
    }

    public class DocWrapper {
        public Integer document_id;
        public String document_name;
    }

    public class UnitListWrapper{
        public String unit_name;
        public String unit_id;
        public String reg_id;
        public String building_name;
        public boolean is_handed_over;
        public Integer[] applicable_complaint_type;
    }
    /************************************************/
    /* For GET method - END */
    /************************************************/

    /************************************************/
    /* For POST method */
    /************************************************/
    global class FinalReturnWrapperSubmit {
        public cls_data_submit data;
        public cls_meta_data meta_data;
    }

    public class cls_data_submit {
        public String sr_id;
        public String sr_number;
        public String unit_id;
        public String unit_name;
        public String complaint_type;
        public String complaint_sub_type;
        public String submitted_date;
    }
    /************************************************/
    /* For POST method - END */
    /************************************************/

}

/*
INPUT Json for - POST

{
    "accountId" : "",
    "bookingUnitId" : "",
    "typeId" : 1,
    "typeName" : "",
    "subType" : "",
    "description" : ""
}

Output :
{
     "meta_data": {
        "title": "Success",
        "status_code": 1,
        "message": "success",
        "developer_message": null
    },
    "data": {
        "objFmCase_id" : "",
        "objFmCase_number" : "",
        "unit_id" : "",
        "unit_name" : "",
        "complaint_type" : "",
        "complaint_sub_type" : "",
        "submitted_date" : "2020-12-15"
    }
}


/*Dummy code below*/
    /*@HttpPost
    global static tempWrapper tempMethod() {
        tempWrapper objTemp = new tempWrapper();
        RestRequest r = RestContext.request;
        System.debug('r:' + r);
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);

        Blob body = r.requestBody;
        String jsonString = body.toString();
        System.debug('jsonString:'+jsonString);

        return objTemp;
    }

    global class tempWrapper {
        public string response;
    }

*/