@isTest
public with sharing class UpdateJBBuyerOnCRMAllinOneBatchTest {
     @isTest
    static void methodTestBatch1(){

        Account objAcc = new Account( Name = 'Test account'
                                    , Email__c = 'rwqerwe@dfsf.com'
                                    , First_Name__c = 'Test'
                                    , Last_Name__c = 'Test 1');
        insert objAcc;

         NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        objBookingUnit.Registration_Status_Code__c = 'ABS';
        objBookingUnit.MollakId__c = '1005';
        insert objBookingUnit;

        List<Buyer__c> lstBuyers = TestDataFactory_CRM.createBuyer(objBooking.Id, 1, objAcc.Id);
        lstBuyers[0].Primary_Buyer__c = false;
        insert lstBuyers;

        CRMAllinOne__c  objCRMAllinOne = new CRMAllinOne__c();
        objCRMAllinOne.Unit__c = objBookingUnit.Id ;
        objCRMAllinOne.Registration_Id__c = '112233';   
        insert objCRMAllinOne;
        Test.startTest();
        UpdateJBBuyerOnCRMAllinOneBatch obj = new UpdateJBBuyerOnCRMAllinOneBatch();
        DataBase.executeBatch(obj);

       
        Test.stopTest();
       
   }
    
   @isTest
    static void methodTestBatch2(){

        Account objAcc = new Account( Name = 'Test account'
                                    , Email__c = 'rwqerwe@dfsf.com'
                                    , First_Name__c = 'Test'
                                    , Last_Name__c = 'Test 1');
        insert objAcc;

         NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        objBookingUnit.Registration_Status_Code__c = 'ABS';
        objBookingUnit.MollakId__c = '1005';
        insert objBookingUnit;

        List<Buyer__c> lstBuyers = TestDataFactory_CRM.createBuyer(objBooking.Id, 2, objAcc.Id);
        lstBuyers[0].Primary_Buyer__c = false;
        insert lstBuyers;

        CRMAllinOne__c  objCRMAllinOne = new CRMAllinOne__c();
        objCRMAllinOne.Unit__c = objBookingUnit.Id ;
        objCRMAllinOne.Registration_Id__c = '112233';   
        insert objCRMAllinOne;
        Test.startTest();
        UpdateJBBuyerOnCRMAllinOneBatch obj = new UpdateJBBuyerOnCRMAllinOneBatch();
        DataBase.executeBatch(obj);

       
        Test.stopTest();
       
   }
    
    
   @isTest
    static void methodTestBatch3(){

        Account objAcc = new Account( Name = 'Test account'
                                    , Email__c = 'rwqerwe@dfsf.com'
                                    , First_Name__c = 'Test'
                                    , Last_Name__c = 'Test 1');
        insert objAcc;

         NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        objBookingUnit.Registration_Status_Code__c = 'ABS';
        objBookingUnit.MollakId__c = '1005';
        insert objBookingUnit;

        List<Buyer__c> lstBuyers = TestDataFactory_CRM.createBuyer(objBooking.Id, 3, objAcc.Id);
        lstBuyers[0].Primary_Buyer__c = false;
        insert lstBuyers;

        CRMAllinOne__c  objCRMAllinOne = new CRMAllinOne__c();
        objCRMAllinOne.Unit__c = objBookingUnit.Id ;
        objCRMAllinOne.Registration_Id__c = '112233';   
        insert objCRMAllinOne;
        Test.startTest();
        UpdateJBBuyerOnCRMAllinOneBatch obj = new UpdateJBBuyerOnCRMAllinOneBatch();
        DataBase.executeBatch(obj);

       
        Test.stopTest();
       
   }
   
}