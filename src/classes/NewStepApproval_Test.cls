@isTest
public class NewStepApproval_Test {
    
    @testSetup
    static void setupData(){
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        Id DealRT = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = acc.Id;
        sr.RecordTypeId = DealRT;
        sr.Agency__c = acc.id;
        sr.Agency_Type__c = 'Corporate';
        insert sr;
        
        List<Step_Status_Transition__c> StepTransitionList = new List<Step_Status_Transition__c>();
        Step_Status_Transition__c statusTrans1 = 
            new Step_Status_Transition__c(Step_Type__c = 'Manager Approval', Step_No__c = 5, From_Step_Status__c = 'Under Manager Review',
                                          Is_Closed__c = True, Service_Request_Type__c = 'Deal', To_Step_Status__c = 'Manager Approved', 
                                          SR_Internal_Status__c = 'MANAGER_APPROVED', SR_External_Status__c = 'MANAGER_APPROVED');
        StepTransitionList.add(statusTrans1);
        
        Step_Status_Transition__c statusTrans2 = 
            new Step_Status_Transition__c(Step_Type__c = 'Manager Approval', Step_No__c = 5, From_Step_Status__c = 'Under Manager Review',
                                          Is_Closed__c = True, Service_Request_Type__c = 'Deal', To_Step_Status__c = 'Rejected', 
                                          SR_Internal_Status__c = 'REJECTED', SR_External_Status__c = 'REJECTED');
        StepTransitionList.add(statusTrans2);
        
        Step_Status_Transition__c statusTrans3 = 
            new Step_Status_Transition__c(Step_Type__c = 'Manager Approval', Step_No__c = 5, From_Step_Status__c = 'Under Manager Review',
                                          Is_Closed__c = True, Service_Request_Type__c = 'Deal', To_Step_Status__c = 'Requested for Info', 
                                          SR_Internal_Status__c = 'REQUESTED_FOR_INFO', SR_External_Status__c = 'REQUESTED_FOR_INFO');
        StepTransitionList.add(statusTrans3);
        
        insert StepTransitionList;
        
        List<New_Step__c> NewStepList = new List<New_Step__c>();
        New_Step__c nstp1 = new New_Step__c();
        nstp1.Service_Request__c = sr.id;
        nstp1.Step_No__c = 5;
        nstp1.Step_Status__c = 'Under Manager Review';
        nstp1.Step_Type__c = 'Manager Approval';
        NewStepList.add(nstp1);            
        insert NewStepList;
        
        List<Email_Service_Body_Split__c> emailServiceList = new List<Email_Service_Body_Split__c>();
        Email_Service_Body_Split__c custSetting1 = new Email_Service_Body_Split__c(Name = 'All', Active__c = true, Split_By_Value__c = 'All');
        emailServiceList.add(custSetting1);
        Email_Service_Body_Split__c custSetting2 = new Email_Service_Body_Split__c(Name = 'Regard', Active__c = true, Split_By_Value__c = 'Regard');
        emailServiceList.add(custSetting2);
        Email_Service_Body_Split__c custSetting3 = new Email_Service_Body_Split__c(Name = 'Dear', Active__c = true, Split_By_Value__c = 'Dear');
        emailServiceList.add(custSetting3);
        Email_Service_Body_Split__c custSetting4 = new Email_Service_Body_Split__c(Name = 'From', Active__c = true, Split_By_Value__c = 'From');
        emailServiceList.add(custSetting4);
        insert emailServiceList;
    }
    
    @isTest
    static void TestRejection(){
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        
        New_Step__c stp = [SELECT Name, Service_Request__c FROM New_Step__c Limit 1];
        email.fromAddress = 'abcd@damac.com';
        email.subject = 'An SR is Awaiting your approval ['+ stp.Name + ']';
        email.plainTextBody = 'reject [test] Dear';
        NewStepApproval emailClass = new NewStepApproval();
        Messaging.InboundEmailResult emailResult = emailClass.handleInboundEmail(email, new Messaging.InboundEnvelope());
        emailClass.makeSrDocsOptional(stp.Service_Request__c);
    }
    
    @isTest
    static void TestApproval(){
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        New_Step__c stp = [SELECT Name, Service_Request__c FROM New_Step__c Limit 1];
        email.fromAddress = 'abcd@damac.com';
        email.subject = 'An SR is Awaiting your approval ['+ stp.Name + ']';
        email.plainTextBody = 'approve [test] From';
        NewStepApproval emailClass = new NewStepApproval();
        Messaging.InboundEmailResult emailResult = emailClass.handleInboundEmail(email, new Messaging.InboundEnvelope());
    }
    
}