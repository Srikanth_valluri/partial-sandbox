/**
 * @File Name          : CaseSoaButtonController.cls
 * @Description        : Class to generate Statement of Account for Units on case
 * @Author             : Raveena Jain
 * @Group              :
 * @Last Modified By   : Raveena Jain
 * @Last Modified On   : 
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0        24/09/2020            Raveena Jain                 Initial Version
**/
public class CaseSoaButtonController {

    public Id recordId {get; set;}
    public String responseUrl {get; set;}
    public String accessToken {get; set;}
    public String pdfFile {get; set;}
    public Blob pdfBlob {get; set;}
    public sObject objectType {get; set;}
    public String registrationId;

    public CaseSoaButtonController(ApexPages.StandardController controller) {
       recordId = controller.getId();
       objectType = controller.getRecord();
        
    }

    public PageReference getUnitSoaUrlCallingList() {
	
        Calling_List__c callLst = [SELECT id,
                                          Party_ID__c,
                                          Registration_ID__c,
                                          Account__r.Party_ID__c,
                                          Booking_Unit__r.Registration_ID__c
                                    FROM Calling_List__c
                                    WHERE Id =: recordId];

        if(callLst != null) {
            registrationId = callLst.Registration_ID__c;
            if(registrationId == null || registrationId == '') {
                registrationId = callLst.Booking_Unit__r.Registration_ID__c;
            }
        }
        PageReference pg = getUnitSoaUrl();
        return null;
    }

    public PageReference getUnitSoaUrlCase() {
		
       Case cse = [SELECT Id
                        , Booking_Unit__r.Registration_ID__c
                   FROM Case
                   WHERE Id =: recordId];
       
        if(cse != null) {
            registrationId = cse.Booking_Unit__r.Registration_ID__c;
          }
        PageReference pg = getUnitSoaUrl();
        return null;
    }

    public PageReference getUnitSoaUrl() {
        
        FmIpmsRestCoffeeServices.updateCustomSetting = false;

        responseUrl = FmIpmsRestCoffeeServices.getUnitSoa(registrationId);

        if(String.isBlank(responseUrl) || responseUrl == null || responseUrl == 'Null') {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.SEVERITY.ERROR, 'There was an error processing your request');
            ApexPages.addMessage(msg);
            return null;
        }

        //Http callout

        accessToken = FmIpmsRestCoffeeServices.accessToken;

        HttpRequest req = new HttpRequest();
        req.setEndpoint(responseUrl);
        req.setHeader('Accept', 'application/json');
        req.setMethod('GET');
        req.setHeader('Authorization','Bearer' + accessToken);
        req.setTimeout(120000);
        HttpResponse response = new Http().send(req);


        if(response.getStatusCode() == 200) {
            pdfBlob =  response.getBodyAsBlob();
            pdfFile = EncodingUtil.base64Encode(pdfBlob);
            
            //TODO  added to create record of soa genrator
            if(String.valueof(recordId).startsWith('a0x'))
            GenericUtility.createSOACreator(responseUrl,UserInfo.getUserId(),system.now(),registrationId,'Generate Unit SOA[New]',Null,recordId,NULL);
            if(String.valueof(recordId).startsWith('a3A'))
            GenericUtility.createSOACreator(responseUrl,UserInfo.getUserId(),system.now(),registrationId,'Generate Unit SOA[New]',Null,Null,recordId);
        }

        FmIpmsRestCoffeeServices.updateCustomSetting = true;
        FmIpmsRestCoffeeServices.updateBearerToken();      //calling updateMethod of Custom Setting after all callouts execution

        return null;
    }
}