@isTest
public with sharing class AdditionalParkingWSDLTest 
{

  static testMethod void init_test_scenario1()
  {
    AdditionalParkingWSDL.getParkingResponse_element objParkingRes = new AdditionalParkingWSDL.getParkingResponse_element();
    AdditionalParkingWSDL.getParking_element objParkingElement = new AdditionalParkingWSDL.getParking_element();
    AdditionalParkingWSDL.updateParking_element objUpdateParking = new AdditionalParkingWSDL.updateParking_element();
    AdditionalParkingWSDL.updateParkingResponse_element objUpdateParkingRes = new AdditionalParkingWSDL.updateParkingResponse_element();
    AdditionalParkingWSDL.createParkingResponse_element objCreateParkingRes = new AdditionalParkingWSDL.createParkingResponse_element();
    AdditionalParkingWSDL.createParking_element objCreateParking = new AdditionalParkingWSDL.createParking_element();

    Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );

    AdditionalParkingWSDL.ParkingHttpSoap11Endpoint objParkingHttpObj = new AdditionalParkingWSDL.ParkingHttpSoap11Endpoint();
    objParkingHttpObj.updateParking(12345,250,'P-123456',800,'Available','123456');
    objParkingHttpObj.getParking(12345,'Visitor');

    parkingPaymentTermsXxdcParkingInvW.APPSXXDC_PARKING_INVX1844909X3X2[] parkingTerms = new 
    parkingPaymentTermsXxdcParkingInvW.APPSXXDC_PARKING_INVX1844909X3X2[1];

    parkingPaymentTermsXxdcParkingInvW.APPSXXDC_PARKING_INVX1844909X3X2 objParkingTerms = new 
        parkingPaymentTermsXxdcParkingInvW.APPSXXDC_PARKING_INVX1844909X3X2();

        objParkingTerms.AMOUNT = Decimal.valueOf('500');
        objParkingTerms.INSTALLMENT = '1st Installment';
        DateTime objDateTime = (DateTime) JSON.deserialize('25-12-2017T00:00:00', DateTime.class);
        objParkingTerms.PAYMENT_DATE = objDateTime;
        objParkingTerms.PDC_NUMBER = '';
        objParkingTerms.REMARKS = '';
        objParkingTerms.SR_NUMBER = '123456';
        objParkingTerms.UNIT_NAME = 'Test Unit';
        parkingTerms[0] = objParkingTerms;

      objParkingHttpObj.createParking(parkingTerms);

  }
}