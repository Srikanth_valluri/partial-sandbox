/**************************************************************************************************
* Name               : AP_DisplayPropertyRanking 
* Description        : An apex page controller for DisplayPropertyRanking
* Created Date       : 12/08/2018
* Created By         : Nikhil Pote
* Last Modified Date :
* Last Modified By   : 
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR                    DATE
* 1.0         Nikhil Pote               12/08/2018
**************************************************************************************************/

public class AP_DisplayPropertyRanking {
    
    public list<Property_Ranking__c> lstPropRankingDisplay{get;set;}
    public list<AnnouncementsWrapper> recordsList { get; set;}
    public list<AnnouncementsWrapper> tempWrapperList = new list<AnnouncementsWrapper>();
    public string selectedValue{get;set;}
    public String strSelectedLanguage{get;set;}
    /*Contructor to get Properties based on Rank & ignore ranking calculation criteria*/
    
    public AP_DisplayPropertyRanking(AP_AgentPortalNewLaunches controller) {
        system.debug('selectedValue--'+selectedValue);
        recordsList = new list<AnnouncementsWrapper>();
        list<Property_Ranking__c> lstPropRanking = new list<Property_Ranking__c>();
        lstPropRanking = [SELECT id, Inventory__c,Inventory__r.Bedroom_Type__c,Inventory__r.Marketing_Name_Doc__r.Primary_Image__c,Inventory__r.Marketing_Name_Doc__r.name,Inventory__r.Marketing_Name_Doc__r.Marketing_Plan__c,Inventory__r.Property__r.Property_Plan__c,name, Rank__c,Ignore_Calculation__c,createddate 
                          FROM  Property_Ranking__c 
                          WHERE CreatedDate = LAST_N_DAYS:7 Order By createddate ASC];
        system.debug('lstPropRanking++++++++'+lstPropRanking);
        
        list<Property_Ranking__c> lstPropRankingIgnCal = new list<Property_Ranking__c>();
        if(!lstPropRanking.isEmpty()) {
            for(Property_Ranking__c objPropRank: lstPropRanking) {
                if(objPropRank.Ignore_Calculation__c){
                    lstPropRankingIgnCal.add(objPropRank);
                }
            }
        }
        system.debug('lstPropRankingIgnCal++++++++'+lstPropRankingIgnCal);
        user objUser = [ SELECT FullPhotoUrl,LanguageLocaleKey,
                                    ContactId,Contact.Owner__c,Contact.Agent_Representative__c,
                                    Contact.Portal_Administrator__c,Contact.Authorised_Signatory__c,
                                    AccountId,
                                    Account.OwnerId,
                                    Account.Name,
                                    Account.Secondary_Owner__c,
                                    Account.Owner.Name,
                                    Account.Secondary_Owner__r.Name
                            FROM User
                            WHERE Id =: userInfo.getUserId()
        ];
        if(objUser.LanguageLocaleKey != null){
                strSelectedLanguage = objUser.LanguageLocaleKey;
                system.debug('>>>strSelectedLanguage>>>>>>>'+strSelectedLanguage);
            }
            if( apexpages.currentPage().getParameters().get('langCode') != null && strSelectedLanguage == null){
                strSelectedLanguage = apexpages.currentPage().getParameters().get('langCode');
                system.debug('>>>strSelectedLanguage58>>>>>>>'+strSelectedLanguage);
            }
        //AND CreatedDate = LAST_N_DAYS:7
        list<Property_Ranking__c> lstPropRankingRank = new list<Property_Ranking__c>();
        lstPropRankingRank = [SELECT id, Inventory__c,Inventory__r.Bedroom_Type__c,Inventory__r.Marketing_Name_Doc__r.Primary_Image__c,Inventory__r.Marketing_Name_Doc__r.name,Inventory__r.Marketing_Name_Doc__r.Marketing_Plan__c,Inventory__r.Property__r.Property_Plan__c,name,Rank__c ,Ignore_Calculation__c,createddate 
                              FROM Property_Ranking__c 
                              WHERE id in:lstPropRanking 
                              AND Ignore_Calculation__c = False 
                              AND CreatedDate = LAST_N_DAYS:7
                              Order By Rank__c ASC];
        lstPropRankingDisplay = new list<Property_Ranking__c>();
        lstPropRankingDisplay.addAll(lstPropRankingIgnCal);  
        Integer PropertyRankingTortalSize = lstPropRankingIgnCal.size() + lstPropRankingRank.size();
        Integer PropertyRankingSize = PropertyRankingTortalSize ;
        //< 20 ? PropertyRankingTortalSize: 20;
        Integer counter = PropertyRankingSize - lstPropRankingIgnCal.size();
        if(!lstPropRankingRank.isEmpty()) {
            for(Integer i=0; i<counter; i++) {
                lstPropRankingDisplay.add(lstPropRankingRank[i]);
            }
        }
        
        if(!lstPropRankingDisplay.isEmpty()) {
            Map <Decimal, Property_Ranking__c> rankingsMap = new Map <Decimal, Property_Ranking__c> ();
            
            for(Property_Ranking__c objPR: lstPropRankingDisplay) {
                if (rankingsMap.containsKey (objPR.Rank__c)) {
                    if (objPR.CreatedDate >= rankingsMap.get (objPR.Rank__c).createdDate) {
                        rankingsMap.put (objPR.Rank__c, objPR);
                    } 
                }else {
                    rankingsMap.put (objPR.Rank__c, objPR);
                }
            }
            for(Property_Ranking__c objPR: rankingsMap.values()) {
                AnnouncementsWrapper objWrapper = new AnnouncementsWrapper();
                if(objPR.Inventory__r.Marketing_Name_Doc__r.Primary_Image__c){
                    objWrapper.image = objPR.Inventory__r.Marketing_Name_Doc__r.Marketing_Plan__c;
                } else {
                    objWrapper.image = objPR.Inventory__r.Property__r.Property_Plan__c;
                }
                objWrapper.dateToDisplay = date.valueof(objPR.CreatedDate);
                objWrapper.type = 'Popular Properties';
                objWrapper.name = objPR.Inventory__r.Marketing_Name_Doc__r.name;
                objWrapper.bedroomType = objPR.Inventory__r.Bedroom_Type__c;
                objWrapper.recordId = objPR.Inventory__r.Id;
                recordsList.add(objWrapper);
            }   
        }     
        system.debug('recordsList----------'+recordsList);
        
        List<User> userList = [SELECT Id,ContactId, AccountId, Profile.Name,
                               Account.Broker_Class__c,
                               Account.recordTypeId, 
                               Account.Country__c,
                               Account.Agency_Corporate_Type__c
                               ,Account.Agency_Type__c
                               FROM User 
                               WHERE ID =:UserInfo.getUserId() 
                               LIMIT 1];                       
        
        list<Announcement__c> lstAnnouncementNewLaunches = new list<Announcement__c>();
        lstAnnouncementNewLaunches = [SELECT
                                      Id,
                                      Start_Date__c,
                                      End_Date__c,
                                      Name,
                                      CreatedDate,
                                      Title__c,
                                      Type__c,
                                      Description__c
                                      FROM 
                                      Announcement__c
                                      WHERE Type__c = 'New Launches'
                                      AND Active__c = true 
                                      AND (
                                          Account__r.Id =: userList[0].AccountId
                                          OR
                                          (
                                              Account__r.Agency_Type__c = :userList[0].Account.Agency_Type__c AND
                                              Account__r.Agency_Corporate_Type__c =: userList[0].Account.Agency_Corporate_Type__c AND
                                              Account__r.Country__c =: userList[0].Account.Country__c AND
                                              Account__r.Broker_Class__c =: userList[0].Account.Broker_Class__c AND
                                              Account__r.recordTypeId =: userList[0].Account.recordTypeId
                                          )
                                      ) limit 20];
        if(!lstAnnouncementNewLaunches.isEmpty()) {
            for(Announcement__c objAnn: lstAnnouncementNewLaunches) {
                AnnouncementsWrapper objWrapper = new AnnouncementsWrapper();
                if(objAnn.Description__c !=NULL) {
                    String temp = objAnn.Description__c.substringAfter('src=');
                    if(string.IsnotBlank(temp)){
                        temp = temp.substringBefore('%20style');
                    }
                    if(string.IsnotBlank(temp)){
                        temp = temp.substringBetween('"');
                    }
                    if(string.IsnotBlank(temp)){
                        temp = temp.replace('&amp;', '&');
                    }
                    objWrapper.image = temp;
                } else {
                    objWrapper.image = '';
                }
                objWrapper.dateToDisplay = objAnn.Start_Date__c;
                objWrapper.type = objAnn.Type__c;
                objWrapper.name = objAnn.Title__c;
                objWrapper.bedroomType = '';
                objWrapper.recordId = objAnn.Id;
                recordsList.add(objWrapper);
            }
        }
        system.debug('recordsList----------'+recordsList);
        
        list<Announcement__c> lstAnnouncementOffers = new list<Announcement__c>();
        lstAnnouncementOffers = [SELECT
                                 Id,
                                 Start_Date__c,
                                 End_Date__c,
                                 Name,
                                 Type__c,
                                 CreatedDate,
                                 Title__c,
                                 Description__c
                                 FROM 
                                 Announcement__c
                                 WHERE
                                 Start_Date__c <= today                                
                                 
                                 AND 
                                 End_Date__c >= today
                                 
                                 AND Type__c = 'Offer'
                                 
                                 AND 
                                 Active__c = true 
                                 AND (
                                     Account__r.Id =: userList[0].AccountId
                                     OR
                                     (
                                         Account__r.Agency_Type__c = :userList[0].Account.Agency_Type__c AND
                                         Account__r.Agency_Corporate_Type__c =: userList[0].Account.Agency_Corporate_Type__c AND
                                         Account__r.Country__c =: userList[0].Account.Country__c AND
                                         Account__r.Broker_Class__c =: userList[0].Account.Broker_Class__c AND
                                         Account__r.recordTypeId =: userList[0].Account.recordTypeId
                                     )
                                 ) limit 20];
        system.debug('lstAnnouncementOffers---'+lstAnnouncementOffers.size());
        if(!lstAnnouncementOffers.isEmpty()) {
            for(Announcement__c objAnn: lstAnnouncementOffers) {
                AnnouncementsWrapper objWrapper = new AnnouncementsWrapper();
                if(objAnn.Description__c !=NULL) {
                    String temp = objAnn.Description__c.substringAfter('src=');
                    if(string.IsnotBlank(temp)){
                        temp = temp.substringBefore('%20style');
                    }
                    if(string.IsnotBlank(temp)){
                        temp = temp.substringBetween('"');
                    }
                    if(string.IsnotBlank(temp)){
                        temp = temp.replace('&amp;', '&');
                    }
                    objWrapper.image = temp;
                } else {
                    objWrapper.image = '';
                }
                objWrapper.dateToDisplay = objAnn.Start_Date__c;
                objWrapper.type = objAnn.Type__c;
                objWrapper.name = objAnn.Title__c;
                objWrapper.bedroomType = '';
                objWrapper.recordId = objAnn.Id;
                recordsList.add(objWrapper);
            }
        }
        /* else block 09.01.2019 */
        else {
            lstAnnouncementOffers = [SELECT
                                     Id,
                                     Start_Date__c,
                                     End_Date__c,
                                     Name,
                                     Type__c,
                                     CreatedDate,
                                     Title__c,
                                     Description__c
                                     FROM 
                                     Announcement__c
                                     WHERE
                                     Start_Date__c <= today                                
                                     
                                     AND 
                                     End_Date__c >= today
                                     
                                     AND Type__c = 'Offer'
                                     
                                     AND 
                                     Active__c = true 
                                     
                                     limit 20];
            
            if(!lstAnnouncementOffers.isEmpty()) {
                for(Announcement__c objAnn: lstAnnouncementOffers) {
                    AnnouncementsWrapper objWrapper = new AnnouncementsWrapper();
                    if(objAnn.Description__c !=NULL) {
                        String temp = objAnn.Description__c.substringAfter('src=');
                        if(string.IsnotBlank(temp)){
                            temp = temp.substringBefore('%20style');
                        }
                        if(string.IsnotBlank(temp)){
                            temp = temp.substringBetween('"');
                        }
                        if(string.IsnotBlank(temp)){
                            temp = temp.replace('&amp;', '&');
                        }
                        objWrapper.image = temp;
                    } else {
                        objWrapper.image = '';
                    }
                    objWrapper.dateToDisplay = objAnn.Start_Date__c;
                    objWrapper.type = objAnn.Type__c;
                    objWrapper.name = objAnn.Title__c;
                    objWrapper.bedroomType = '';
                    objWrapper.recordId = objAnn.Id;
                    recordsList.add(objWrapper);
                }
            }
        }
        system.debug('recordsList----------'+recordsList.size());
        tempWrapperList.addAll(recordsList);
    }
    
    /****************************************************************************************
* Method : To get Announcement dropdown values
****************************************************************************************/
    public List<SelectOption> getDropdownItems() {
        List<SelectOption> options = new List<SelectOption>();
        if(strSelectedLanguage == 'en_US' || strSelectedLanguage == '' || strSelectedLanguage == null ){
            options.add(new SelectOption('All Announcements','All Announcements'));
            options.add(new SelectOption('Offer','Offer'));
            options.add(new SelectOption('New Launches','New Launches'));
            options.add(new SelectOption('Popular Properties','Popular Properties'));
         }
         if(strSelectedLanguage == 'zh_CN'){
            options.add(new SelectOption('All Announcements','所有声明'));
            options.add(new SelectOption('Offer','优惠'));
            options.add(new SelectOption('New Launches','新盘发布'));
            options.add(new SelectOption('Popular Properties','热卖房源'));
         }  
        
        return options;
    }
    
    /****************************************************************************************
* Method : Wrapper to store Announcements data based on type
****************************************************************************************/
    public class AnnouncementsWrapper{
        public string image { get; set;}
        public date dateToDisplay  { get; set;}
        public string type { get; set;}
        public string name { get; set;}
        public string bedroomType { get; set;}  
        public string recordId { get; set;}      
    }
    
    /****************************************************************************************
* Method : To populated announcements based on selected value
****************************************************************************************/
    public void populateSelectedAnnouncements() {
        system.debug('selectedValue--'+selectedValue);
        recordsList = new list<AnnouncementsWrapper>();
        if(string.IsnotBlank(selectedValue)) {
            if(selectedValue.equals('All Announcements')) {
                recordsList.addAll(tempWrapperList);
            } else if (!selectedValue.equals('Popular Properties')) {
                SYstem.Debug ('==========='+tempWrapperList);
                if(!tempWrapperList.isEmpty()) {
                    recordsList = selectedAnnouncements (selectedValue);
                    /*
                    for(AnnouncementsWrapper objWrapp: tempWrapperList) {
                        System.Debug ('========'+objWrapp.type);
                        if(selectedValue.equals(objWrapp.type)){
                            recordsList.add(objWrapp);
                        }
                    }*/
                }
            } else {
                for(AnnouncementsWrapper objWrapp: tempWrapperList) {
                    System.Debug ('========'+objWrapp.type);
                    if(selectedValue.equals(objWrapp.type)){
                        recordsList.add(objWrapp);
                    }
                }
            }
        }
        system.debug('recordsList type----------'+recordsList.size());
        //return recordsList;
    }
    
    public list<AnnouncementsWrapper> selectedAnnouncements (String type) {
        List<User> userList = [SELECT Id,ContactId, AccountId, Profile.Name,
                               Account.Broker_Class__c,
                               Account.recordTypeId, 
                               Account.Country__c,
                               Account.Agency_Corporate_Type__c
                               ,Account.Agency_Type__c
                               FROM User 
                               WHERE ID =:UserInfo.getUserId() 
                               LIMIT 1];                       
        
        list<Announcement__c> lstAnnouncementNewLaunches = new list<Announcement__c>();
        lstAnnouncementNewLaunches = [SELECT
                                      Id,
                                      Start_Date__c,
                                      End_Date__c,
                                      Name,
                                      CreatedDate,
                                      Title__c,
                                      Type__c,
                                      Description__c
                                      FROM 
                                      Announcement__c
                                      WHERE
                                      Start_Date__c <= today                                
                                      
                                      AND 
                                      End_Date__c >= today
                                      
                                      AND Type__c =: type
                                      
                                      AND 
                                      Active__c = true 
                                      AND (
                                          Account__r.Id =: userList[0].AccountId
                                          OR
                                          (
                                              Account__r.Agency_Type__c = :userList[0].Account.Agency_Type__c AND
                                              Account__r.Agency_Corporate_Type__c =: userList[0].Account.Agency_Corporate_Type__c AND
                                              Account__r.Country__c =: userList[0].Account.Country__c AND
                                              Account__r.Broker_Class__c =: userList[0].Account.Broker_Class__c AND
                                              Account__r.recordTypeId =: userList[0].Account.recordTypeId
                                          )
                                      ) limit 20];
        if(!lstAnnouncementNewLaunches.isEmpty()) {
            for(Announcement__c objAnn: lstAnnouncementNewLaunches) {
                AnnouncementsWrapper objWrapper = new AnnouncementsWrapper();
                if(objAnn.Description__c !=NULL) {
                    String temp = objAnn.Description__c.substringAfter('src=');
                    if(string.IsnotBlank(temp)){
                        temp = temp.substringBefore('%20style');
                    }
                    if(string.IsnotBlank(temp)){
                        temp = temp.substringBetween('"');
                    }
                    if(string.IsnotBlank(temp)){
                        temp = temp.replace('&amp;', '&');
                    }
                    objWrapper.image = temp;
                } else {
                    objWrapper.image = '';
                }
                objWrapper.dateToDisplay = objAnn.Start_Date__c;
                objWrapper.type = objAnn.Type__c;
                objWrapper.name = objAnn.Title__c;
                objWrapper.bedroomType = '';
                objWrapper.recordId = objAnn.Id;
                recordsList.add(objWrapper);
            }
        }
        system.debug('recordsList----------'+recordsList);
        
        list<Announcement__c> lstAnnouncementOffers = new list<Announcement__c>();
        lstAnnouncementOffers = [SELECT
                                 Id,
                                 Start_Date__c,
                                 End_Date__c,
                                 Name,
                                 Type__c,
                                 CreatedDate,
                                 Title__c,
                                 Description__c
                                 FROM 
                                 Announcement__c
                                 WHERE
                                 Start_Date__c <= today                                
                                 
                                 AND 
                                 End_Date__c >= today
                                 
                                 AND Type__c =: type
                                 
                                 AND 
                                 Active__c = true 
                                 AND (
                                     Account__r.Id =: userList[0].AccountId
                                     OR
                                     (
                                         Account__r.Agency_Type__c = :userList[0].Account.Agency_Type__c AND
                                         Account__r.Agency_Corporate_Type__c =: userList[0].Account.Agency_Corporate_Type__c AND
                                         Account__r.Country__c =: userList[0].Account.Country__c AND
                                         Account__r.Broker_Class__c =: userList[0].Account.Broker_Class__c AND
                                         Account__r.recordTypeId =: userList[0].Account.recordTypeId
                                     )
                                 ) limit 20];
        system.debug('lstAnnouncementOffers---'+lstAnnouncementOffers.size());
        if(!lstAnnouncementOffers.isEmpty()) {
            for(Announcement__c objAnn: lstAnnouncementOffers) {
                AnnouncementsWrapper objWrapper = new AnnouncementsWrapper();
                if(objAnn.Description__c !=NULL) {
                    String temp = objAnn.Description__c.substringAfter('src=');
                    if(string.IsnotBlank(temp)){
                        temp = temp.substringBefore('%20style');
                    }
                    if(string.IsnotBlank(temp)){
                        temp = temp.substringBetween('"');
                    }
                    if(string.IsnotBlank(temp)){
                        temp = temp.replace('&amp;', '&');
                    }
                    objWrapper.image = temp;
                } else {
                    objWrapper.image = '';
                }
                objWrapper.dateToDisplay = objAnn.Start_Date__c;
                objWrapper.type = objAnn.Type__c;
                objWrapper.name = objAnn.Title__c;
                objWrapper.bedroomType = '';
                objWrapper.recordId = objAnn.Id;
                recordsList.add(objWrapper);
            }
        }
        /* else block 09.01.2019 */
        else {
            lstAnnouncementOffers = [SELECT
                                     Id,
                                     Start_Date__c,
                                     End_Date__c,
                                     Name,
                                     Type__c,
                                     CreatedDate,
                                     Title__c,
                                     Description__c
                                     FROM 
                                     Announcement__c
                                     WHERE
                                     Start_Date__c <= today                                
                                     
                                     AND 
                                     End_Date__c >= today
                                     
                                     AND Type__c =: type
                                     
                                     AND 
                                     Active__c = true 
                                     
                                     limit 20];
            
            if(!lstAnnouncementOffers.isEmpty()) {
                for(Announcement__c objAnn: lstAnnouncementOffers) {
                    AnnouncementsWrapper objWrapper = new AnnouncementsWrapper();
                    if(objAnn.Description__c !=NULL) {
                        String temp = objAnn.Description__c.substringAfter('src=');
                        if(string.IsnotBlank(temp)){
                            temp = temp.substringBefore('%20style');
                        }
                        if(string.IsnotBlank(temp)){
                            temp = temp.substringBetween('"');
                        }
                        if(string.IsnotBlank(temp)){
                            temp = temp.replace('&amp;', '&');
                        }
                        objWrapper.image = temp;
                    } else {
                        objWrapper.image = '';
                    }
                    objWrapper.dateToDisplay = objAnn.Start_Date__c;
                    objWrapper.type = objAnn.Type__c;
                    objWrapper.name = objAnn.Title__c;
                    objWrapper.bedroomType = '';
                    objWrapper.recordId = objAnn.Id;
                    recordsList.add(objWrapper);
                }
            }
        }
        return recordsList;
    }
    
    
}