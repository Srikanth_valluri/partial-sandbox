/*********************************************************************************************************
* Name               : BulkUpdateAttributesController
* Test Class         : 
* Description        : Controller class for BulkUpdateAttributes VF Page
* Created Date       : 16/11/2020
* -----------------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst         16/11/2020      Initial Draft.
**********************************************************************************************************/
global With Sharing class BulkUpdateAttributesController {
    public string projectWrapperJSON {get; set;}
    public List<projectWrapper> wrapperList;
    public List<String> propertyNames {get; set;}
    Map<String, Property__c> propertyMap;
    Map<String, Integer> buildingCountMap;
    Map<String, Integer> floorCountMap;
    Map<String, Integer> UnitsCountMap;
    Map<String, Integer> inventoryCountMap;
    Map<String, Integer> availableCountMap;
    Map<String, Integer> soldCountMap;
     Map<String, Integer> restrictedCountMap;
    public BulkUpdateAttributesController(){
        propertyNames = new List<String>();
        propertyMap = new  Map<String, Property__c>();
        wrapperList = new List<projectWrapper>();
        buildingCountMap = new Map<String, Integer>();
        floorCountMap = new Map<String, Integer>();
        UnitsCountMap = new Map<String, Integer>();
        inventoryCountMap = new Map<String, Integer>();
        availableCountMap = new Map<String, Integer>();
        soldCountMap = new Map<String, Integer>();
        
        restrictedCountMap= new Map<String, Integer>();
        projectWrapperJSON = JSON.serialize(wrapperList);
        fetchDetails();
    }

    public void fetchDetails(){
      for(Property__c property: [SELECT Id, Property_Name__c 
                                       FROM Property__c 
                                       WHERE Property_Id_is_Negative__c = FALSE
                                       AND Property_Name__c != ''
                                       ORDER BY Property_Name__c ASC
                                       LIMIT 1000]){
            propertyMap.put(property.Property_Name__c, property);
            propertyNames.add(property.Property_Name__c);
        }
        for(Location__c loc: [SELECT Property_Name__r.Property_Name__c
                               FROM Location__c 
                               WHERE Location_Type__c = 'Building'
                               AND Property_Name__r.Property_Name__c IN: propertyNames]){
             Integer count = 0;
             if(buildingCountMap.containsKey(loc.Property_Name__r.Property_Name__c)){
                 count = buildingCountMap.get(loc.Property_Name__r.Property_Name__c);
             }
             count++;
             buildingCountMap.put(loc.Property_Name__r.Property_Name__c, count);
        }
         for(Location__c loc: [SELECT Building_Number__r.Property_Name__r.Property_Name__c
                               FROM Location__c 
                               WHERE Location_Type__c = 'Floor'
                               AND Building_Number__r.Property_Name__r.Property_Name__c IN: propertyNames]){
             Integer floorCount = 0;
             if(floorCountMap.containsKey(loc.Building_Number__r.Property_Name__r.Property_Name__c)){
                 floorCount = floorCountMap.get(loc.Building_Number__r.Property_Name__r.Property_Name__c);
             }
             floorCount++;
             floorCountMap.put(loc.Building_Number__r.Property_Name__r.Property_Name__c, floorCount);
        }
        for(Inventory__c inv: [SELECT Id, Status__c, Property__r.Property_Name__c
                                FROM Inventory__c 
                                WHERE Property__r.Property_Name__c IN: propertyNames 
                                    AND Status__c != NULL 
                                    AND Status__c != '']){
             Integer unitsCount = 0;
             if(UnitsCountMap.containsKey(inv.Property__r.Property_Name__c)){
                 unitsCount = UnitsCountMap.get(inv.Property__r.Property_Name__c);
             }
             unitsCount++;
             UnitsCountMap.put(inv.Property__r.Property_Name__c, unitsCount);
             if(inv.Status__c.toLowerCase() == 'sold'){    
                 Integer soldCount = 0;
                 if(soldCountMap.containsKey(inv.Property__r.Property_Name__c)){
                     soldCount = soldCountMap.get(inv.Property__r.Property_Name__c);
                 }
                 soldCount++;
                 soldCountMap.put(inv.Property__r.Property_Name__c, soldCount);
             } else if(inv.Status__c.toLowerCase().contains('restricted')){     
                 Integer restrictedCount = 0;
                 if(restrictedCountMap.containsKey(inv.Property__r.Property_Name__c)){
                     restrictedCount = restrictedCountMap.get(inv.Property__r.Property_Name__c);
                 }
                 restrictedCount++;
                 restrictedCountMap.put(inv.Property__r.Property_Name__c, restrictedCount);
             } else if(inv.Status__c.toLowerCase() == 'inventory'){
                 Integer invCount = 0;
                 if(inventoryCountMap.containsKey(inv.Property__r.Property_Name__c)){
                     invCount = inventoryCountMap.get(inv.Property__r.Property_Name__c);
                 }
                 invCount++;
                 inventoryCountMap.put(inv.Property__r.Property_Name__c, invCount);
             } else if(inv.Status__c.toLowerCase() == 'available' || inv.Status__c.toLowerCase() == 'released'){
                 Integer availableCount = 0;
                 if(availableCountMap.containsKey(inv.Property__r.Property_Name__c)){
                     availableCount = availableCountMap.get(inv.Property__r.Property_Name__c);
                 }
                 availableCount++;
                 availableCountMap.put(inv.Property__r.Property_Name__c, availableCount);
             } 
        }
        for(String projName: propertyNames){
            projectWrapper wrapper = new projectWrapper();
            Property__c project = propertyMap.get(projName);
            wrapper.projectId = project.Id;
            wrapper.projects = project.Property_Name__c;
            wrapper.type = project.Property_Name__c;
            wrapper.city = project.Property_Name__c;
            Integer bCount = 0, fCount = 0, unCount = 0, invCount = 0, avCount = 0, soldCount = 0, resCount = 0;
            if(buildingCountMap.containsKey(projName)){
                bCount = buildingCountMap.get(projName);
            }
            if(floorCountMap.containsKey(projName)){
                fCount = floorCountMap.get(projName);
            }
            if(UnitsCountMap.containsKey(projName)){
                unCount = UnitsCountMap.get(projName);
            }
            if(inventoryCountMap.containsKey(projName)){
                invCount = inventoryCountMap.get(projName);
            }
            if(availableCountMap.containsKey(projName)){
                avCount = availableCountMap.get(projName);
            }
            if(soldCountMap.containsKey(projName)){
                soldCount = soldCountMap.get(projName);
            }
            if(restrictedCountMap.containsKey(projName)){
                resCount = restrictedCountMap.get(projName);
            }
            wrapper.buildings_clusters = bCount;
            wrapper.floors = fCount;
            wrapper.units = unCount;
            wrapper.inventory = invCount;
            wrapper.available = avCount;
            wrapper.sold = soldCount;
            wrapper.restricted = resCount;
            wrapperList.add(wrapper);
        
        }
        projectWrapperJSON = JSON.serialize(wrapperList);
    }
    
    public class projectWrapper{
        public string projectId;
        public string projects;
        public string type;
        public string city;
        public Integer buildings_clusters;
        public Integer floors;
        public Integer units;
        public Integer inventory;
        public Integer available;
        public Integer sold;
        public Integer restricted;
    }

}