/*
* Description - Test class developed for 'VerifyDocumentOnSRAttachment'
*
* Version            Date            Author            Description
* 1.0                28/11/17        Rohit            Initial Draft
*/
@isTest
public class VerifyDocumentOnSRAttachmentTest {

    public static testMethod void testSRAttachment() {
        Id recTypeAssignment = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(null, recTypeAssignment);
        insert objCase;

        SR_Attachments__c objDocument1 = TestDataFactory_CRM.createCaseDocument(objCase.Id, recTypeAssignment);
        objDocument1.isValid__c = false;
        insert objDocument1;

        SR_Attachments__c objDocument2 = TestDataFactory_CRM.createCaseDocument(objCase.Id, recTypeAssignment);
        objDocument2.isValid__c = false;
        insert objDocument2;

        objDocument1.isValid__c = true;
        objDocument2.isValid__c = true;

        list<SR_Attachments__c> listDocuments = new list<SR_Attachments__c>();
        listDocuments.add(objDocument1);
        listDocuments.add(objDocument2);
        update listDocuments;

        VerifyDocumentOnSRAttachment.VerifyDocument(listDocuments);
    }
}