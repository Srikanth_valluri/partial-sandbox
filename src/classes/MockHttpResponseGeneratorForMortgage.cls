global class MockHttpResponseGeneratorForMortgage implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
       /* System.assertEquals(Label.Mortgage_Url+
                            ':8045/webservices/rest/XXDC_PROCESS_SERVICE_WS/process/'+
                            Label.Mortgage_Initiation_Process_Name+
                            '/', req.getEndpoint());*/
        System.debug('req::::in test :'+req);
        System.assertEquals('POST', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
       // res.setBody('{"example":"test"}');
        res.setStatusCode(200);
        return res;
    }
}