@isTest
public with sharing class AP_SupportPageControllerTest {

	/**
		Test method to test load of case list successfully
	 */
	@isTest
	public static void testloadCasesListSuccess() {

		Account accObj = new Account(Name = 'Test Account');
		insert accObj;
		//Create Agent contact
		Contact contObj = InitialiseTestData.getAdminContact('Test Contact',accObj.Id);
		contObj.Authorised_Signatory__c = true;
		contObj.Owner__c = true;
		insert contObj;

		//Create user test data
		User userObj = InitialiseTestData.getPortalUser('test@testmail.com',contObj.Id);
		insert userObj;

		// Create case__c
		Case__c caseObj = InitialiseTestData.createCases();
		caseObj.recordTypeId =
		Schema.SObjectType.Case__c.getRecordTypeInfosByName().get('Support').getRecordTypeId();
		AP_SupportPageController controller;
		System.runAs(userObj) {
			insert caseObj;
			PageReference pageRef = Page.AP_SupportPage;
			Test.setCurrentPage(pageRef);
			Test.startTest();
			controller = new AP_SupportPageController();
			Test.stopTest();
		}

		//System.assert(controller.caseList.size() > 0);
	}

	/**
		Test method to test load of case list unsuccessfully
	 */
	@isTest
	public static void testloadCasesListUnsuccess() {

		Account accObj = new Account(Name = 'Test Account');
		insert accObj;
		//Create Agent contact
		Contact contObj = InitialiseTestData.getAdminContact('Test Contact',accObj.Id);
		//contObj.Authorised_Signatory__c = true;
		//contObj.Owner__c = true;
		insert contObj;

		//Create user test data
		User userObj = InitialiseTestData.getPortalUser('test@testmail.com',contObj.Id);
		insert userObj;

		PageReference pageRef = Page.AP_SupportPage;
		Test.setCurrentPage(pageRef);
		Test.startTest();
		AP_SupportPageController controller = new AP_SupportPageController();
		Test.stopTest();

		//System.assertEquals(0, controller.caseList.size());
	}

	/**
		Test method to test navigate to Support Detail Page
	 */
	@isTest
	public static void testNavigateToSupportDetail() {

		// Create case__c
		Case__c caseObj = InitialiseTestData.createCases();
		caseObj.recordTypeId =
		Schema.SObjectType.Case__c.getRecordTypeInfosByName().get('Case Comments').getRecordTypeId();
		insert caseObj;

		PageReference pageRef = Page.AP_SupportPage;
		Test.setCurrentPage(pageRef);
		AP_SupportPageController controller = new AP_SupportPageController();
		controller.detailId = caseObj.Id;
		Test.startTest();
		PageReference currentPage = controller.navigateToSupportDetail();
		Test.stopTest();

		//system.assertEquals(currentPage.getUrl(),'/apex/ap_supportdetail?Id='+ caseObj.Id );
	}

	@isTest
	public static void testAddNewTicket() {
		PageReference pageRef = Page.AP_SupportPage;
		Test.setCurrentPage(pageRef);
		AP_SupportPageController controller = new AP_SupportPageController();
		controller.newCase.Support_Category__c = 'My Customer Queries';
		controller.caseSubject = 'Request for Marketing Support';
		controller.supportDesc = 'test Description';
		controller.requestedDateTime = String.valueOf(System.now()+10);
		Test.startTest();
		PageReference supportPage =  controller.addNewTicket();
		Test.stopTest();
	}

	/**
		Test method to test blank support category
	 */
	@isTest
	public static void testFalseCategoryValidation() {
		PageReference pageRef = Page.AP_SupportPage;
		Test.setCurrentPage(pageRef);
		AP_SupportPageController controller = new AP_SupportPageController();
		Test.startTest();
		PageReference supportPage =  controller.addNewTicket();
		Test.stopTest();

		/*System.assertEquals(false, controller.isvalid);
		System.assertEquals('block', controller.errorDisplay);
		System.assertEquals('Error: Please Select Support Category. <br/>', controller.showerror);*/
	}

	/**
		Test method to test blank Subject
	 */
	@isTest
	public static void testFalseSubjectValidation() {
		PageReference pageRef = Page.AP_SupportPage;
		Test.setCurrentPage(pageRef);
		AP_SupportPageController controller = new AP_SupportPageController();
		controller.newCase.Support_Category__c = 'My Customer Queries';
		Test.startTest();
		PageReference supportPage =  controller.addNewTicket();
		Test.stopTest();

		/*System.assertEquals(false, controller.isvalid);
		System.assertEquals('block', controller.errorDisplay);
		System.assertEquals('Error: Please Select Support Subject. <br/>', controller.showerror);*/
	}

	/**
		Test method to test blank Description
	 */
	@isTest
	public static void testFalseDescriptionValidation() {
		PageReference pageRef = Page.AP_SupportPage;
		Test.setCurrentPage(pageRef);
		AP_SupportPageController controller = new AP_SupportPageController();
		controller.newCase.Support_Category__c = 'My Customer Queries';
		controller.caseSubject = 'Booking Related Queries';
		Test.startTest();
		PageReference supportPage =  controller.addNewTicket();
		Test.stopTest();

		/*System.assertEquals(false, controller.isvalid);
		System.assertEquals('block', controller.errorDisplay);
		System.assertEquals('Error: Please fill Support Description. <br/>', controller.showerror);*/
	}

	/**
		Test method to test Subject with 'Request for Marketing Support' value and invalid
		requested date
	 */
	@isTest
	public static void testMarketingSupportSubject1() {
		PageReference pageRef = Page.AP_SupportPage;
		Test.setCurrentPage(pageRef);
		AP_SupportPageController controller = new AP_SupportPageController();
		controller.newCase.Support_Category__c = 'My Customer Queries';
		controller.caseSubject = 'Request for Marketing Support';
		controller.supportDesc = 'test Description';
		Test.startTest();
		PageReference supportPage =  controller.addNewTicket();
		Test.stopTest();

		/*System.assertEquals(false, controller.isvalid);
		System.assertEquals('block', controller.errorDisplay);
		System.assertEquals('Error: Please fill Date and Time. <br/>', controller.showerror);*/
	}

	/**
		Test method to test Subject with 'Request for Marketing Support' value and valid
		requested date
	 */
	@isTest
	public static void testMarketingSupportSubject2() {
		PageReference pageRef = Page.AP_SupportPage;
		Test.setCurrentPage(pageRef);
		AP_SupportPageController controller = new AP_SupportPageController();
		controller.newCase.Support_Category__c = 'My Customer Queries';
		controller.caseSubject = 'Request for Marketing Support';
		controller.supportDesc = 'test Description';
		controller.requestedDateTime = String.valueOf(System.now());
		Test.startTest();
		PageReference supportPage =  controller.addNewTicket();
		Test.stopTest();
	}

	/**
		Test method to test Subject with 'request for site viewing'
	 */
	@isTest
	public static void testSiteViewingSubject() {
		PageReference pageRef = Page.AP_SupportPage;
		Test.setCurrentPage(pageRef);
		AP_SupportPageController controller = new AP_SupportPageController();
		controller.newCase.Support_Category__c = 'My Customer Queries';
		controller.caseSubject = 'request for site viewing';
		controller.supportDesc = 'test Description';
		Test.startTest();
		PageReference supportPage =  controller.addNewTicket();
		Test.stopTest();

		/*System.assertEquals(false, controller.isvalid);
		System.assertEquals('block', controller.errorDisplay);
		System.assertEquals(
		'Error: Please Fill Request Date & Time.<br/>Error: Please fill Project Name. <br/>',
		controller.showerror);*/
	}

	/**
		Test method to test Subject with 'book a hotel'
	 */
	@isTest
	public static void testBookHotelSubject() {
		PageReference pageRef = Page.AP_SupportPage;
		Test.setCurrentPage(pageRef);
		AP_SupportPageController controller = new AP_SupportPageController();
		controller.newCase.Support_Category__c = 'My Customer Queries';
		controller.caseSubject = 'book a hotel';
		controller.supportDesc = 'test Description';
		Test.startTest();
		PageReference supportPage =  controller.addNewTicket();
		Test.stopTest();

		/*System.assertEquals(false, controller.isvalid);
		System.assertEquals('block', controller.errorDisplay);
		System.assertEquals(
		'Error: Please fill No. of Nights. Value should be Numeric and Greater than 0. <br/>',
		controller.showerror);*/
	}

	/**
		Test method to test Subject with 'book a luxury car'
	 */
	@isTest
	public static void testBookLuxuryCarSubject() {
		PageReference pageRef = Page.AP_SupportPage;
		Test.setCurrentPage(pageRef);
		AP_SupportPageController controller = new AP_SupportPageController();
		controller.newCase.Support_Category__c = 'My Customer Queries';
		controller.caseSubject = 'book a luxury car';
		controller.supportDesc = 'test Description';
		Test.startTest();
		PageReference supportPage =  controller.addNewTicket();
		Test.stopTest();

		/*System.assertEquals(false, controller.isvalid);
		System.assertEquals('block', controller.errorDisplay);
		System.assertEquals('Error: Please Fill Request Date & Time.<br/>'+
							'Error: Please fill Pick Up Location. <br/>' +
							'Error: Please fill Drop Off Location. <br/>',
		controller.showerror);*/
	}

	/**
		Test method to test Subject with 'book a meeting room'
	 */
	@isTest
	public static void testBookMarketingRoomSubject() {
		PageReference pageRef = Page.AP_SupportPage;
		Test.setCurrentPage(pageRef);
		AP_SupportPageController controller = new AP_SupportPageController();
		controller.newCase.Support_Category__c = 'My Customer Queries';
		controller.caseSubject = 'book a meeting room';
		controller.supportDesc = 'test Description';
		Test.startTest();
		PageReference supportPage =  controller.addNewTicket();
		Test.stopTest();

		/*System.assertEquals(false, controller.isvalid);
		System.assertEquals('block', controller.errorDisplay);
		System.assertEquals('Error: Please Fill Request Date & Time.<br/>'+
							'Error: Please fill Sales Office. <br/>',
		controller.showerror);*/
	}

	/**
		Test method to test Subject with 'Commission Payment'
	 */
	@isTest
	public static void testCommissionPaymentSubject() {
		PageReference pageRef = Page.AP_SupportPage;
		Test.setCurrentPage(pageRef);
		AP_SupportPageController controller = new AP_SupportPageController();
		controller.newCase.Support_Category__c = 'My Customer Queries';
		controller.caseSubject = 'Commission Payment';
		controller.supportDesc = 'test Description';
		Test.startTest();
		PageReference supportPage =  controller.addNewTicket();
		Test.stopTest();

		/*System.assertEquals(false, controller.isvalid);
		System.assertEquals('block', controller.errorDisplay);
		System.assertEquals('Error: Please fill Unit Number. <br/>',controller.showerror);*/
	}

	/**
		Test method to test Subject with 'request for brochures'
	 */
	@isTest
	public static void testRequestForBrochuresSubject() {
		PageReference pageRef = Page.AP_SupportPage;
		Test.setCurrentPage(pageRef);
		AP_SupportPageController controller = new AP_SupportPageController();
		controller.newCase.Support_Category__c = 'My Customer Queries';
		controller.caseSubject = 'request for brochures';
		controller.supportDesc = 'test Description';
		Test.startTest();
		PageReference supportPage =  controller.addNewTicket();
		Test.stopTest();

		/*System.assertEquals(false, controller.isvalid);
		System.assertEquals('block', controller.errorDisplay);
		System.assertEquals('Error: Please Fill Request Date & Time.<br/>'+
							'Error: Please fill Project Name.<br/>',
		controller.showerror);*/
	}

	/**
		Test method to test Subject with 'query on unit commission'
	 */
	@isTest
	public static void testQueryOnUnitCommissionSubject() {
		PageReference pageRef = Page.AP_SupportPage;
		Test.setCurrentPage(pageRef);
		AP_SupportPageController controller = new AP_SupportPageController();
		controller.newCase.Support_Category__c = 'My Customer Queries';
		controller.caseSubject = 'query on unit commission';
		controller.supportDesc = 'test Description';
		Test.startTest();
		PageReference supportPage =  controller.addNewTicket();
		Test.stopTest();

		/*System.assertEquals(false, controller.isvalid);
		System.assertEquals('block', controller.errorDisplay);
		System.assertEquals('Error: Please fill Unit Number.<br/>', controller.showerror);*/
	}

	/**
	Test ToggleCaseSubjects method with 'My Customer Queries' Support Category
	 */
	@isTest
	public static void testToggleCaseSubjects1() {
		PageReference pageRef = Page.AP_SupportPage;
		Test.setCurrentPage(pageRef);
		AP_SupportPageController controller = new AP_SupportPageController();
		controller.newCase.Support_Category__c = 'My Customer Queries';
		Test.startTest();
		controller.toggleCaseSubjects();
		Test.stopTest();

		/*System.assertEquals('', controller.caseSubjectOptionsList[0].getValue());
		System.assertEquals('Booking Related Queries', controller.caseSubjectOptionsList[1].getValue());
		System.assertEquals('SPA', controller.caseSubjectOptionsList[2].getValue());*/
	}

	/**
	Test ToggleCaseSubjects method with 'My Sales Support' Support Category
	 */
	@isTest
	public static void testToggleCaseSubjects2() {
		PageReference pageRef = Page.AP_SupportPage;
		Test.setCurrentPage(pageRef);
		AP_SupportPageController controller = new AP_SupportPageController();
		controller.newCase.Support_Category__c = 'My Sales Support';
		Test.startTest();
		controller.toggleCaseSubjects();
		Test.stopTest();

		/*System.assertEquals('', controller.caseSubjectOptionsList[0].getValue());
		System.assertEquals('Request for Brochures', controller.caseSubjectOptionsList[1].getValue());
		System.assertEquals('Request for Marketing Support', controller.caseSubjectOptionsList[2].getValue());
		System.assertEquals('Request for site Viewing', controller.caseSubjectOptionsList[3].getValue());*/
	}

	/**
	Test ToggleCaseSubjects method with 'My Support Needs' Support Category
	 */
	@isTest
	public static void testToggleCaseSubjects3() {
		PageReference pageRef = Page.AP_SupportPage;
		Test.setCurrentPage(pageRef);
		AP_SupportPageController controller = new AP_SupportPageController();
		controller.newCase.Support_Category__c = 'My Support Needs';
		Test.startTest();
		controller.toggleCaseSubjects();
		Test.stopTest();

		/*System.assertEquals('', controller.caseSubjectOptionsList[0].getValue());
		System.assertEquals('Agency Agreement', controller.caseSubjectOptionsList[1].getValue());
		System.assertEquals('Book a Meeting Room', controller.caseSubjectOptionsList[2].getValue());
		System.assertEquals('Commission Payment', controller.caseSubjectOptionsList[3].getValue());
		System.assertEquals('Query on Unit Commission', controller.caseSubjectOptionsList[4].getValue());
		System.assertEquals('Request for Training', controller.caseSubjectOptionsList[5].getValue());*/
	}

	@isTest
	public static void testFieldSetValue() {
		PageReference pageRef = Page.AP_SupportPage;
		Test.setCurrentPage(pageRef);
		AP_SupportPageController controller = new AP_SupportPageController();
		controller.caseSubject = 'SPA';
		Test.startTest();
		controller.fieldSetValue();
		Test.stopTest();

		/*System.assert(controller.fieldList.size() > 0);
		System.assert(controller.isAdditionalData);*/
		//assert isAdditionalData
	}
}