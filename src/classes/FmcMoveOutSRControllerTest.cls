@istest
public with sharing class FmcMoveOutSRControllerTest {
	static Account a = new Account();
	static Account a1 = new Account();
	static Booking__c  bk = new  Booking__c();
	static NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
	static Booking_Unit__c bu = new Booking_Unit__c();
	static Booking_Unit__c bu1 = new Booking_Unit__c();
	static Location__c locObj = new Location__c();
	static FM_Case__c fmObj = new FM_Case__c();
    static FM_User__c userObj = new FM_User__c();
	static FM_Additional_Detail__c addDetails = new FM_Additional_Detail__c();
	static SR_Attachments__c srObj = new SR_Attachments__c();
	static Task taskObj = new Task();

	static void SetUp(){

	    a.Name = 'Test Account';
	    a.party_ID__C = '1039032';
	    insert a;

	    Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
	    sr.NSIBPM__Customer__c = a.id;
	    sr.RecordTypeId = RecType;
	    insert sr;

	    bk.Account__c = a.id;
	    bk.Deal_SR__c = sr.id;
	    insert bk;

	    bu.Booking__c = bk.id;
	    bu.Unit_Name__c = 'LSB/10/B1001';
	    bu.Owner__c = a.id;
	    bu.Property_City__c = 'Dubai';
	    insert bu;
	    bu1.Booking__c = bk.id;
	    bu1.Unit_Name__c = 'LSB/10/B1001';
	    bu1.Owner__c = a1.id;
	    bu1.Property_City__c = 'Dubai';
	    insert bu1;

	    locObj.Name  = 'LSB';
	    locObj.Location_ID__c = '83488';
	    insert locObj;

        userObj.Building__c = locObj.id;
        userObj.FM_Role__c = 'FM Admin';
        insert userObj;

	    Id RecordTypeIdFMCase = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Move Out').getRecordTypeId();
	    fmObj.Booking_Unit__c = bu.id;
	    fmObj.Expected_move_out_date__c = Date.Today();
	    fmObj.Move_in_date__c = Date.Today();
	    fmObj.Tenant__c = a.id;
	    fmObj.Actual_move_out_date__c = system.today();
	    fmObj.Account__c = a.id;
	    fmObj.Access_card_required__c = 'Yes';
	    fmObj.recordtypeid = RecordTypeIdFMCase;
	    insert fmObj;

	    srObj.Name = 'Test';
	    srObj.FM_Case__c = fmObj.id;
	    insert srObj;

	    addDetails.Resident_Account__c = a.id;
	    addDetails.Household_Account__c = a.id;
	    addDetails.Pet_Account__c = a.id;
	    addDetails.Vehicle_Account__c = a.id;
	    addDetails.Emergency_Contact_Account__c = a.id;
	    insert addDetails;

	    taskObj.Description = 'Test'; //string
	    taskObj.Status ='Completed';
	    taskObj.Subject = Label.Move_in_date_task;
	    taskObj.WhatId = fmObj.id; //record id
	    insert taskObj;
  	}

    static testMethod void testMethodSubmit(){
        SetUp();
        String fileName = 'TestName,TestType';
        PageReference myVfPage = Page.CommunityPortal;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('UnitId',bu.id);
        ApexPages.currentPage().getParameters().put('AccountId',a.id);
        ApexPages.currentPage().getParameters().put('fileName',fileName);
        ApexPages.currentPage().getParameters().put('view','MoveOut');
        FmcMoveOutSRController moveOutObj = new FmcMoveOutSRController();
        moveOutObj.strUnitId = bu.id;
        moveOutObj.getUnitDetails();
        moveOutObj.docIdToDelete = String.valueOf('srObj.id');
        moveOutObj.objFMCase.Request_Type_DeveloperName__c = 'Move_out_Request';
        moveOutObj.strDate = '12/12/12';
        moveOutObj.submitSr();
    }

    static testMethod void testMethodApexCallout(){
        SMS_History__c smsObj = new SMS_History__c();
        smsObj.Phone_Number__c = '1234567890';
        smsObj.Message__c = 'Test';
        insert smsObj;
        List<SMS_History__c> lstSMSHist = new List<SMS_History__c>();
        lstSMSHist.add(smsObj);

        String fileName = 'TestName,TestType';
        PageReference myVfPage = Page.CommunityPortal;
        Test.setCurrentPage(myVfPage);
        FmcMoveOutSRController moveOutObj = new FmcMoveOutSRController();
        MoveOutSRComponentController.apexcallout(smsObj.id);
        MoveOutSRComponentController.sendSMS(lstSMSHist);
    }

    static testMethod void testMethodSaveDraft(){
        SetUp();
        PageReference myVfPage = Page.CommunityPortal;
        ApexPages.currentPage().getParameters().put('UnitId',bu.id);
        ApexPages.currentPage().getParameters().put('AccountId',a.id);
        ApexPages.currentPage().getParameters().put('view','MoveOut');
        Test.setCurrentPage(myVfPage);
        FmcMoveOutSRController moveOutObj = new FmcMoveOutSRController();
        moveOutObj.strDate = '12/12/12';
        moveOutObj.saveSr();
    }

    static testMethod void testMethodUploadAndDeleteDocument(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = [SELECT Id FROM User WHERE Profile.name = 'System Administrator' AND isActive = True limit 1];
        System.runAs(u){
            SetUp();
            String fileName = 'VfRemotingJs,TestType';
            PageReference myVfPage = Page.CommunityPortal;
            Test.setCurrentPage(myVfPage);
            ApexPages.currentPage().getParameters().put('UnitId',bu.id);
            ApexPages.currentPage().getParameters().put('AccountId',a.id);
            ApexPages.currentPage().getParameters().put('fileName',fileName);
          	ApexPages.currentPage().getParameters().put('Id',String.valueOf(fmObj.id));
          	ApexPages.currentPage().getParameters().put('view','MoveOut');
        }
        Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
        FmcMoveOutSRController moveOutObj = new FmcMoveOutSRController();
        moveOutObj.strDocBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
        moveOutObj.strDocName = 'VfRemotingJs';
        Test.startTest();
            moveOutObj.uploadDocument();
            moveOutObj.deleteDocument();
        Test.stopTest();
        //MoveOutSRComponentController.apexcallout(smsObj.id);
        //MoveOutSRComponentController.sendSMS(lstSMSHist);
    }

    static testMethod void testMethod_modify(){
        // SetUp();
        FM_Case__c fmObj = new FM_Case__c();
        Id RecordTypeIdFMCase = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Move Out').getRecordTypeId();
        fmObj.Booking_Unit__c = bu.id;
        fmObj.Expected_move_out_date__c = Date.Today();
        fmObj.Move_in_date__c = Date.Today();
        fmObj.Tenant__c = a.id;
        fmObj.Admin__c = UserInfo.getUserId();
        fmObj.Actual_move_out_date__c = system.today();
        fmObj.Account__c = a.id;
        fmObj.Tenant_Email__c = 'test@gmail.com';
        fmObj.Access_card_required__c = 'Yes';
        fmObj.recordtypeid = RecordTypeIdFMCase;
        insert fmObj;
        Test.startTest();
        MoveOutSRComponentController.modifySR(fmObj.id);
        MoveOutSRComponentController.cancelSR(fmObj.id);
        Test.stopTest();
    }
}