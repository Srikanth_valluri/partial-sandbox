public without sharing class GenericRelatedListDataProvider implements DataProvider {

    public DataDisplayConfig getData(DataDisplayConfig config) {
        List<Sobject> lstRecord = new List<Sobject>();
        if (String.isNotBlank(config.recordId)) {
            String fields = '';

            for (Map<String,String> fieldMap : config.fieldList) {
                String fieldName = fieldMap.get(DataRendererUtils.FIELD_NAME);
                if (String.isNotBlank(fieldName) && !fields.containsIgnoreCase(fieldName + ',')) {
                    fields += fieldName + ',';
                }
            }

            if (config.detailFieldList != NULL) {
                for (Map<String,String> fieldMap : config.detailFieldList) {
                    String fieldName = fieldMap.get(DataRendererUtils.FIELD_NAME);
                    if (String.isNotBlank(fieldName) && !fields.containsIgnoreCase(fieldName + ',')) {
                        fields += fieldName + ',';
                    }
                }
            }

            fields = fields.removeEnd(',');
            System.debug(
                ' SELECT ' + fields + ' FROM ' + config.objectName
                + ' WHERE ' + config.parentField + ' = \'' + config.recordId + '\''
                + (String.isBlank(config.filter) ? '' : ' AND (' + config.filter + ')')
            );
            lstRecord = Database.query(
                ' SELECT ' + fields + ' FROM ' + config.objectName
                + ' WHERE ' + config.parentField + ' = \'' + config.recordId + '\''
                + (String.isBlank(config.filter) ? '' : ' AND (' + config.filter + ')')
            );
        }

        config.dataList = DataRendererUtils.wrapData(config, lstRecord);
        return config;
    }

}