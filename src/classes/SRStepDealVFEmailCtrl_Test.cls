@istest
public class SRStepDealVFEmailCtrl_Test {

    @testSetup 
    static void setupData() {
        //IPMS_Code_Settings__c custom settings
        List<IPMS_Code_Settings__c> ipmsCodeList = new List<IPMS_Code_Settings__c>();
        IPMS_Code_Settings__c code1 = new IPMS_Code_Settings__c(Name = 'SPA Executed', Step_Type__c = 'SPA Execution', Step_Status__c = 'SPA Executed', 
                                                                Status_Code__c = 'LE', Check_on_DP_OK__c = False, Check_on_Doc_OK__c = False, 
                                                                Doc_OK__c = true, DP_OK__c = true);
        ipmsCodeList.add(code1);
        insert ipmsCodeList;

        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;

        Id DealRT = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = acc.Id;
        sr.RecordTypeId = DealRT;
        sr.Agency__c = acc.Id;
        sr.Agency_Type__c = 'Corporate';
        insert sr;

        New_Step__c nstp1 = new New_Step__c();
        nstp1.Service_Request__c = sr.id;
        nstp1.Step_No__c = 50;
        nstp1.Step_Status__c = 'Awaiting SPA Execution';
        nstp1.Step_Type__c = 'SPA Execution';
        insert nstp1;

        Booking__c booking = new Booking__c(Account__c = acc.Id, Deal_SR__c = sr.Id);
        insert booking;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id, Unit_Name__c = 'Test name 1',
                                                          Registration_ID__c = '1600', Registration_Status__c = 'Active Status',
                                                          Unit_Selling_Price_AED__c = 100);
        insert bookingUnit;
                                                 

    }
    
    @isTest
    static void test_method1() {
        New_Step__c step = [SELECT Id, Service_Request__c FROM New_Step__c LIMIT 1];
        step.Step_Status__c = 'SPA Executed';
        update step;
        //call rest of methods
        test.startTest();
            SRStepDealVFEmailCtrl obj = new SRStepDealVFEmailCtrl ();
            List<Booking__c> bookings = obj.bookings;
            obj.stepId = step.Id;
            bookings = obj.bookings;
        test.stopTest();
        
    }
}