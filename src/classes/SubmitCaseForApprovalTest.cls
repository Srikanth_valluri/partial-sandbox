/*
* Description - Test class developed for SubmitCaseForApproval
*
* Version            Date            Author            Description
* 1.0              21/11/2017      Ashish Agarwal     Initial Draft
*/


@isTest
private class SubmitCaseForApprovalTest {

    static testMethod void myUnitTest() {
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        //NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        //insert objSR ;
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1);
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        //Insert Options
         List<Option__c> lstOptions = TestDataFactory_CRM.createOptions( lstBookingUnits );
         insert lstOptions ;
        
        //Insert Customer Setting Records
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveUnitCustomSetting( new list<String>{ 'Agreement executed by DAMAC' } );
        insert lstActiveStatus ;
        
        //Insert Cases for the units
        list<Case> lstCases = new list<Case>();
        Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
            objCase.Booking_Unit__c = objUnit.Id; 
            objCase.Status = 'Submitted';
            objCase.Roles_from_Rule_Engine__c = 'CRM-Manager';
            lstCases.add( objCase );
        }
        insert lstCases ;
        
        list<SR_Attachments__c> lstAttachment = new list<SR_Attachments__c>();
        for( Case objCase : lstCases ) {
            /*SR_Attachments__c srObj = new SR_Attachments__c();
            srObj.Case__c = objCase.Id;
            srObj.Type__c = PenaltyWaiverUtility.ATTACH_TYPE_FULL_CRF;
            srObj.isValid__c = true;
            insert srObj;
            lstAttachment.add(srObj);*/
            lstAttachment.add( TestDataFactory_CRM.createCaseDocument( objCase.Id, PenaltyWaiverUtility.ATTACH_TYPE_FULL_CRF ) );
        }
        upsert lstAttachment ;
        
        test.startTest();
            SubmitCaseForApproval.submitCase( new list<SR_Attachments__c>{ lstAttachment[0] } );
        test.stopTest();
    }
    
    static testMethod void myUnitTest2() {
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        //Insert Options
         List<Option__c> lstOptions = TestDataFactory_CRM.createOptions( lstBookingUnits );
         insert lstOptions ;
        
        //Insert Customer Setting Records
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveUnitCustomSetting( new list<String>{ 'Agreement executed by DAMAC' } );
        insert lstActiveStatus ;
        
        //Insert Cases for the units
        list<Case> lstCases = new list<Case>();
        Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
            objCase.Booking_Unit__c = objUnit.Id; 
            objCase.Status = 'Submitted';
            objCase.Roles_from_Rule_Engine__c = 'System Approved';
            lstCases.add( objCase );
        }
        insert lstCases ;
        
        list<SR_Attachments__c> lstAttachment = new list<SR_Attachments__c>();
        for( Case objCase : lstCases ) {
            SR_Attachments__c srObj = new SR_Attachments__c();
            srObj.Case__c = objCase.Id;
            srObj.Type__c = PenaltyWaiverUtility.ATTACH_TYPE_FULL_CRF;
            srObj.isValid__c = true;
            insert srObj;
            lstAttachment.add(srObj);
            //lstAttachment.add( TestDataFactory_CRM.createCaseDocument( objCase.Id, PenaltyWaiverUtility.ATTACH_TYPE_FULL_CRF ) );
        }
        upsert lstAttachment ;
        
        test.startTest();
            SubmitCaseForApproval.submitCase( new list<SR_Attachments__c>{ lstAttachment[0] } );
        test.stopTest();
    }
    
    static testMethod void myUnitTest3() {
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        //Insert Options
         List<Option__c> lstOptions = TestDataFactory_CRM.createOptions( lstBookingUnits );
         insert lstOptions ;
        
        //Insert Customer Setting Records
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveUnitCustomSetting( new list<String>{ 'Agreement executed by DAMAC' } );
        insert lstActiveStatus ;
        
        //Insert Cases for the units
        list<Case> lstCases = new list<Case>();
        Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
            objCase.Booking_Unit__c = objUnit.Id;
            objCase.Roles_from_Rule_Engine__c = 'System Approved';
            objCase.Total_Penalty_Waived_for_Customer__c = 137786;
            objCase.Outstanding_Amount__c = 610439;
            objCase.Amount_to_be_waived__c = 63823;
            lstCases.add( objCase );
        }
        insert lstCases ;
        
        list<SR_Attachments__c> lstAttachment = new list<SR_Attachments__c>();
        for( Case objCase : lstCases ) {
            //lstAttachment.add( TestDataFactory_CRM.createCaseDocument( objCase.Id, PenaltyWaiverUtility.ATTACH_TYPE_FULL_CRF ) );
            SR_Attachments__c srObj = new SR_Attachments__c();
            srObj.Case__c = objCase.Id;
            srObj.Type__c = PenaltyWaiverUtility.ATTACH_TYPE_FULL_CRF;
            srObj.isValid__c = true;
            insert srObj;
            lstAttachment.add(srObj);
        }
        upsert lstAttachment ;
        
        test.startTest();
            SubmitCaseForApproval.submitCase( new list<SR_Attachments__c>{ lstAttachment[0] } );
        test.stopTest();
    }
    static testMethod void otherthenPW() {
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        //Insert Options
         List<Option__c> lstOptions = TestDataFactory_CRM.createOptions( lstBookingUnits );
         insert lstOptions ;
        
        //Insert Customer Setting Records
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveUnitCustomSetting( new list<String>{ 'Agreement executed by DAMAC' } );
        insert lstActiveStatus ;
        
        //Insert Cases for the units
        list<Case> lstCases = new list<Case>();
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('AOPT').getRecordTypeId();
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
            objCase.Booking_Unit__c = objUnit.Id; 
            objCase.Status = 'Submitted';
            objCase.Roles_from_Rule_Engine__c = 'System Approved';
            lstCases.add( objCase );
        }
        insert lstCases ;
        
        list<SR_Attachments__c> lstAttachment = new list<SR_Attachments__c>();
        for( Case objCase : lstCases ) {
            SR_Attachments__c srObj = new SR_Attachments__c();
            srObj.Case__c = objCase.Id;
            srObj.Type__c = PenaltyWaiverUtility.ATTACH_TYPE_FULL_CRF;
            srObj.isValid__c = true;
            insert srObj;
            lstAttachment.add(srObj);
            //lstAttachment.add( TestDataFactory_CRM.createCaseDocument( objCase.Id, PenaltyWaiverUtility.ATTACH_TYPE_FULL_CRF ) );
        }
        upsert lstAttachment ;
        
        test.startTest();
            SubmitCaseForApproval.submitCase( new list<SR_Attachments__c>{ lstAttachment[0] } );
        test.stopTest();
    }
}