/*-------------------------------------------------------------------------------------------------
Description: Test class for UnitPaymentTermsController

    ============================================================================================================================
        Version | Date(DD-MM-YYYY) | Last Modified By | Comments
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 23-11-2017       | Lochana Rajput    | 1. To verify Case attachment
   =============================================================================================================================
*/
@isTest
public class UnitPaymentTermsControllerTest {

	@isTest static void test_milestonePaymentDetails() {
		Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT() );
		Test.startTest();
        List<Object> objMileStoneOnPayment = UnitPaymentTermsController.getPaymentTerms('74712');
		Test.stopTest();
	}
}