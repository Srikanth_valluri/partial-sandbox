@isTest
public class GetAmenitiesByUnitAndAccount_APITest {
    @TestSetup
    static void TestData() {
        insert new IpmsRestServices__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            BaseUrl__c = 'http://0.0.0.0:8080/webservices/rest',
            Username__c = 'username',
            Password__c = 'password',
            Timeout__c = 120000
        );
		
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;
		
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account1 = new Account( LastName = 'Test Account1',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test1@mailinator.com');
        insert account1;
		
        Contact con1 = [SELECT id FROM Contact WHERE AccountId=: account1.Id];
		
        Profile tenantProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Tenant Community Login User%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test123@test.com',
                              ContactId = con1.Id,
                              ProfileId = tenantProfile.Id,
                              Alias = 'test123',
                              Email = 'test123@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account1',
                              CommunityNickname = 'test123',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
        Database.insert(user1);
        
        Account account2 = new Account( LastName = 'Test Account2',
                                       Party_ID__c = '63063',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test2@mailinator.com');
        insert account2;
		
        Contact con2 = [SELECT id FROM Contact WHERE AccountId=: account2.Id];
        
        Profile ownerProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Customer Community Login User(Use this)%' Limit 1];
        User user2 = new User(Username = System.now().millisecond() + 'test456@test.com',
                              ContactId = con2.Id,
                              ProfileId = ownerProfile.Id,
                              Alias = 'test456',
                              Email = 'test456@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account2',
                              CommunityNickname = 'test456',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
        Database.insert(user2);
        
        Booking__c booking1 = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account1.Id);
        insert booking1;
        
        Booking__c booking2 = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account2.Id);
        insert booking2;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'Janusia';
        insert locObj;
		
        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;
		
        Booking_Unit__c bookingUnit1 = new Booking_Unit__c(Booking__c = booking1.Id,
                                                          Registration_ID__c = '3901',
                                                          Inventory__c = invObj.Id,
                                                          Unit_Name__c='JNU/SD168/XH2910B',
                                                          Lease_Expiry_Date__c=System.today(),
                                                          Resident__c=account1.Id,
                                                          Tenant__c=account1.Id);
        insert bookingUnit1;
		
        Booking_Unit__c bookingUnit2 = new Booking_Unit__c(Booking__c = booking2.Id,
                                                          Registration_ID__c = '3902',
                                                          Inventory__c = invObj.Id,
                                                          Unit_Name__c='JNU/SD168/XH2910B',
                                                          Lease_Expiry_Date__c=System.today(),
                                                          Resident__c=account2.Id,
                                                          Owner__c=account2.Id);
        insert bookingUnit2;
        
        Property__c propObj = TestDataFactoryFM.createProperty();
        insert propObj;
		
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;
		
        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;
		
        Resource__c objRes = new Resource__c();
        objRes.Non_Operational_Days__c = 'Sunday';
        objRes.Resource_Type__c = 'FM Amenity';
        objRes.No_of_advance_booking_days__c = 2;
        objRes.No_of_deposit_due_days__c = 1;
        objRes.Contact_Person_Email__c = 'test@test.com';   //
        objRes.Contact_Person_Name__c = 'Test'; //
        objRes.Contact_Person_Phone__c = '1234567890';  //
        insert objRes;
		
        Resource_Sharing__c objRS= new Resource_Sharing__c();
        objRS.Building__c = locObj.Id;
        objRS.Resource__c = objRes.Id;
        insert objRS;
		
        Resource_Slot__c objResSlot1 = new Resource_Slot__c();
        objResSlot1.Resource__c = objRes.Id;
        objResSlot1.Start_Date__c = Date.Today();
        objResSlot1.End_Date__c = Date.Today().addDays(30);
        objResSlot1.Start_Time_p__c = '09:00';
        objResSlot1.End_Time_p__c = '09:30';
        insert objResSlot1;
		
        Non_operational_days__c obj = new Non_operational_days__c();
        obj.Non_operational_date__c = Date.today();
        obj.Resource__c = objRes.Id;
        insert obj;
    }
	
    @isTest
    static void testAmenitiesTenant() {
        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
		Account objAcc = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenitiesList';  
        req.addParameter('bookingUnitId',objBU.Id);
        req.addParameter('accountId',objAcc.Id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        FmIpmsRestServices.DueInvoicesResponse invoicesResponse = new FmIpmsRestServices.DueInvoicesResponse();
        invoicesResponse.OutputParameters = new FmIpmsRestServices.OutputParameters();
        invoicesResponse.OutputParameters.X_RETURN_STATUS = 'S';
        invoicesResponse.OutputParameters.X_RETURN_MESSAGE = 'Total Records Fetched...1';
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE = new FmIpmsRestServices.ResponseMessage();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM = new List<Map<String, String>>();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM.add(
            new Map<String, String> {
                'ATTRIBUTE1' => 'registrationId',
                'ATTRIBUTE2' => 'unitName',
                'ATTRIBUTE3' => 'projectName',
                'ATTRIBUTE4' => 'customerId',
                'ATTRIBUTE5' => 'orgId',
                'ATTRIBUTE6' => 'partyId',
                'ATTRIBUTE7' => 'partyName',
                'ATTRIBUTE8' => 'trxNumber',
                'ATTRIBUTE9' => 'creationDate',
                'ATTRIBUTE10' => 'callType',
                'ATTRIBUTE11' => '50',
                'ATTRIBUTE12' => '100',
                'ATTRIBUTE13' => '50',
                'ATTRIBUTE14' => FmcUtils.formatAsIpmsDate(Date.today()),
                'ATTRIBUTE15' => 'trxType',
                'ATTRIBUTE16' => '0'
            }
        );
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(invoicesResponse)));
		
        Test.startTest();
        GetAmenitiesByUnitAndAccount_API.GetAmenitiesForUnit();
        Test.stopTest();
    }
    
    @isTest
    static void testAmenitiesOwner() {
        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3902'];
		Account objAcc = [SELECT Id FROM Account WHERE Party_ID__c = '63063'];
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenitiesList';  
        req.addParameter('bookingUnitId',objBU.Id);
        req.addParameter('accountId',objAcc.Id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        FmIpmsRestServices.DueInvoicesResponse invoicesResponse = new FmIpmsRestServices.DueInvoicesResponse();
        invoicesResponse.OutputParameters = new FmIpmsRestServices.OutputParameters();
        invoicesResponse.OutputParameters.X_RETURN_STATUS = 'S';
        invoicesResponse.OutputParameters.X_RETURN_MESSAGE = 'Total Records Fetched...1';
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE = new FmIpmsRestServices.ResponseMessage();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM = new List<Map<String, String>>();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM.add(
            new Map<String, String> {
                'ATTRIBUTE1' => 'registrationId',
                'ATTRIBUTE2' => 'unitName',
                'ATTRIBUTE3' => 'projectName',
                'ATTRIBUTE4' => 'customerId',
                'ATTRIBUTE5' => 'orgId',
                'ATTRIBUTE6' => 'partyId',
                'ATTRIBUTE7' => 'partyName',
                'ATTRIBUTE8' => 'trxNumber',
                'ATTRIBUTE9' => 'creationDate',
                'ATTRIBUTE10' => 'callType',
                'ATTRIBUTE11' => '50',
                'ATTRIBUTE12' => '100',
                'ATTRIBUTE13' => '50',
                'ATTRIBUTE14' => FmcUtils.formatAsIpmsDate(Date.today()),
                'ATTRIBUTE15' => 'trxType',
                'ATTRIBUTE16' => '0'
            }
        );
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(invoicesResponse)));
		
        Test.startTest();
        GetAmenitiesByUnitAndAccount_API.GetAmenitiesForUnit();
        Test.stopTest();
    }
    
    @isTest
    static void testBookingUnitNull() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenitiesList';
        req.addParameter('accountId','');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        GetAmenitiesByUnitAndAccount_API.GetAmenitiesForUnit();
        Test.stopTest();
    }
    
    @isTest
    static void testBookingUnitBlank() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenitiesList';  
        req.addParameter('bookingUnitId','');
        req.addParameter('accountId','');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        GetAmenitiesByUnitAndAccount_API.GetAmenitiesForUnit();
        Test.stopTest();
    }
    
    @isTest
    static void testAccountNull() {
        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenitiesList';
        req.addParameter('bookingUnitId',objBU.Id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        GetAmenitiesByUnitAndAccount_API.GetAmenitiesForUnit();
        Test.stopTest();
    }
    
    @isTest
    static void testAccountBlank() {
        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenitiesList';  
        req.addParameter('bookingUnitId',objBU.Id);
        req.addParameter('accountId','');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        GetAmenitiesByUnitAndAccount_API.GetAmenitiesForUnit();
        Test.stopTest();
    }
    
    @isTest
    static void testInvalidAccount() {
        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenitiesList';  
        req.addParameter('bookingUnitId',objBU.Id);
        req.addParameter('accountId','1234');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        GetAmenitiesByUnitAndAccount_API.GetAmenitiesForUnit();
        Test.stopTest();
    }
}