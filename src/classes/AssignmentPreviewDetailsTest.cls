@isTest
public class AssignmentPreviewDetailsTest{
    static List<Booking_Unit_Active_Status__c> lstActives;
    static list<RuleEngineDocuments__c> lstRuleDocs;
    static Account objAcc;
    static NSIBPM__Service_Request__c objSR;
    static List<Booking__c> lstBookings;
    static Property__c objProp;
    static Inventory__c objInv;
    static List<Booking_Unit__c> lstBookingUnits;
    static List<Option__c> lstOptions;
    static list<Buyer__c> lstJB;
    static Payment_Plan__c objPP;
    
    static void init() {
        lstActives = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActives;
        
        lstRuleDocs = TestDataFactory_CRM.createRuleDocs();
        insert lstRuleDocs;
        
        // Insert Account        
        objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__pc = 'Indian';
        objAcc.Country__pc = 'India';
        insert objAcc ;
        system.debug('******Acc Id*************'+objAcc.Id);
        
        //Insert Service Request
        objSR = TestDataFactory_CRM.createServiceRequest( );
        insert objSR ;
        system.debug('******SR Id*************'+objSR.Id);
        
        //Insert Bookings
        lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1);
        insert lstBookings ;
        system.debug('******bookings Id*************'+lstBookings);
        
        objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        objInv = TestDataFactory_CRM.createInventory(objProp.Id);
        objInv.Property_Country__c = 'UAE';
        objInv.Property_City__c = 'Dubai';
        objInv.Property_Status__c = 'Ready';
        objInv.Bedroom_Type__c = ' 1 BR';
        insert objInv;
        
        //Insert Booking Units
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        lstBookingUnits[0].Inventory__c = objInv.Id;
        lstBookingUnits[0].Area__c = 35;
        lstBookingUnits[0].Handover_Flag__c = 'Y';
        insert lstBookingUnits;
        system.debug('******Unit Id*************'+lstBookingUnits);
        
        lstOptions = TestDataFactory_CRM.createOptions(lstBookingUnits);
        insert lstOptions;
        
        lstJB = TestDataFactory_CRM.createBuyer(lstBookings[0].Id, 2, objAcc.Id);
        lstJB[0].Buyer_Type__c = 'Individual';
        lstJB[1].Buyer_Type__c = 'Individual';
        insert lstJB;
    }
    
    static testMethod void buyerWithAccountNoMatch(){
        init();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        Case objCase = new Case();
        objCase.Seller__c = objAcc.Id;
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.Buyer__c = lstJB[0].Id;
        objCase.AccountId = objAcc.Id;
        objCase.New_Booking_Unit__c = lstBookingUnits[0].Id;
        insert objCase;
        Pagereference pageRef = Page.AssignmentPreviewDetails;
        pageRef.getParameters().put('Id',objCase.Id);
        Test.setCurrentPageReference(pageRef);
        AssignmentPreviewDetails objClass = new AssignmentPreviewDetails();
        Test.stopTest();
    }
    
    static testMethod void buyerWithAccountMatch(){
        init();
        list<Buyer__c> lstB = TestDataFactory_CRM.createBuyer(lstBookings[0].Id, 1, null);
        lstB[0].Buyer_Type__c = 'Individual';
        lstB[0].Nationality__c = 'Indian';
        lstB[0].Country__c = 'India';
        insert lstB;
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        Case objCase = new Case();
        objCase.Seller__c = objAcc.Id;
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.Buyer__c = lstJB[0].Id;
        objCase.AccountId = objAcc.Id;
        objCase.New_Booking_Unit__c = lstBookingUnits[0].Id;
        insert objCase;
        Pagereference pageRef = Page.AssignmentPreviewDetails;
        pageRef.getParameters().put('Id',objCase.Id);
        Test.setCurrentPageReference(pageRef);
        AssignmentPreviewDetails objClass = new AssignmentPreviewDetails();
        Test.stopTest();
    }
    
    static testMethod void buyerWithoutAccountNoMatch(){
        init();
        list<Buyer__c> lstB = TestDataFactory_CRM.createBuyer(lstBookings[0].Id, 1, null);
        lstB[0].Buyer_Type__c = 'Individual';
        insert lstB;
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        Case objCase = new Case();
        objCase.Seller__c = objAcc.Id;
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.Buyer__c = lstB[0].Id;
        objCase.AccountId = objAcc.Id;
        objCase.New_Booking_Unit__c = lstBookingUnits[0].Id;
        insert objCase;
        Pagereference pageRef = Page.AssignmentPreviewDetails;
        pageRef.getParameters().put('Id',objCase.Id);
        Test.setCurrentPageReference(pageRef);
        AssignmentPreviewDetails objClass = new AssignmentPreviewDetails();
        Test.stopTest();
    }
    
    static testMethod void noCorporateMatch(){
        init();
        list<Buyer__c> lstB = TestDataFactory_CRM.createBuyer(lstBookings[0].Id, 1, null);
        lstB[0].Buyer_Type__c = 'Corporate';
        lstB[0].Organisation_Name__c = 'Org';
        lstB[0].Organisation_Name_Arabic__c = 'Org';
        lstB[0].CR_Number__c = '1234';
        lstB[0].CR_Registration_Place__c = '1234';
        lstB[0].CR_Registration_Place_Arabic__c = '1234';
        lstB[0].CR_Registration_Expiry_Date__c = '12/08/1989';
        lstB[0].Buyer_Type__c = 'Corporate';
        insert lstB;
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        Case objCase = new Case();
        objCase.Seller__c = objAcc.Id;
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.Buyer__c = lstB[0].Id;
        objCase.AccountId = objAcc.Id;
        objCase.New_Booking_Unit__c = lstBookingUnits[0].Id;
        insert objCase;
        Pagereference pageRef = Page.AssignmentPreviewDetails;
        pageRef.getParameters().put('Id',objCase.Id);
        Test.setCurrentPageReference(pageRef);
        AssignmentPreviewDetails objClass = new AssignmentPreviewDetails();
        Test.stopTest();
    }
    
    static testMethod void corporateMatch(){
        init();
        Account objBusiness = TestDataFactory_CRM.createBusinessAccount();
        objBusiness.CR_Number__c = '1234';
        insert objBusiness;
        list<Buyer__c> lstB = TestDataFactory_CRM.createBuyer(lstBookings[0].Id, 1, null);
        lstB[0].Buyer_Type__c = 'Corporate';
        lstB[0].Organisation_Name__c = 'Org';
        lstB[0].Organisation_Name_Arabic__c = 'Org';
        lstB[0].CR_Number__c = '1234';
        lstB[0].CR_Registration_Place__c = '1234';
        lstB[0].CR_Registration_Place_Arabic__c = '1234';
        lstB[0].CR_Registration_Expiry_Date__c = '12/08/1989';
        lstB[0].Buyer_Type__c = 'Corporate';
        insert lstB;
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        Case objCase = new Case();
        objCase.Seller__c = objAcc.Id;
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.Buyer__c = lstB[0].Id;
        objCase.AccountId = objAcc.Id;
        objCase.New_Booking_Unit__c = lstBookingUnits[0].Id;
        insert objCase;
        Pagereference pageRef = Page.AssignmentPreviewDetails;
        pageRef.getParameters().put('Id',objCase.Id);
        Test.setCurrentPageReference(pageRef);
        AssignmentPreviewDetails objClass = new AssignmentPreviewDetails();
        Test.stopTest();
    }
}