public class CaseNotificationService implements IsNotificationService {

    public List<NotificationService.Notification> getNotifications(){ return null;}

    public List<NotificationService.Notification> getNotifications(NotificationService.Notification request){
        List<NotificationService.Notification> notificationList = new List<NotificationService.Notification>();
        if (String.isBlank(request.accountId)) {
            return notificationList;
        }
        Id accountId = request.accountId;
        for(Case caseRecord : [
            SELECT Id
                 , Subject
                 , Status
                 , CaseNumber
                 , RecordType.Name
            FROM Case
            WHERE  Notifications_Read__c = false
              AND  AccountId = :accountId
         ORDER BY  LastModifiedDate DESC
        ]){
            NotificationService.Notification notification = new NotificationService.Notification();
            notification.accountId = accountId;
            String CaseSubject;
            if(caseRecord.Subject != NULL) {
                CaseSubject = caseRecord.Subject;
            } else {
                CaseSubject = '';
            }

            //String status = caseRecord.Status == 'New' ? 'Created' : 'Closed';
            String status = caseRecord.Status;
            notification.title      = ''+caseRecord.CaseNumber +' : Your service request '+ CaseSubject +' is '+status;
            notification.recordId   = caseRecord.Id;
            notification.status     = caseRecord.Status;
            notification.record     = caseRecord;
            notificationList.add(notification);
        }
        return notificationList;
    }

    public static list<NotificationService.Notification> markRead(
        List<NotificationService.Notification> notificationList
    ) {
        List<case> lstCase = new List<Case>();
        for (NotificationService.Notification notification: notificationList) {
            if (String.isNotBlank(notification.recordId)) {
                lstCase.add(new Case(Id=notification.recordId,Notifications_Read__c=true));
            }
        }
        update lstCase;
        return notificationList;
    }

    public NotificationService.Notification markUnRead(NotificationService.Notification notification) {
        update new Case(Id = notification.recordId, Notifications_Read__c = false);
        return notification;
    }
}