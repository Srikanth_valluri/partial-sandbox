public without sharing class FmcViewInvoicesController {
	public List<SelectOption>  units           {get; set;}
    public List<SelectOption>  locations       {get; set;}
    public Static String strSelectedUnit {get; set;}
    public Static String strSelectedlocation {get; set;}
    //public Static Map<Id,String> mapUnitId_RegId;
    //public Static Map<Id,String> mapLocationId_Code;
    public Static Map<Id, SelectOption> mapLocation;

    private static final String GENERIC_SAVE_ERROR = 'There was an error while processing your request. Please try again later.';

    public FmcViewInvoicesController() {
        initiateVariables();
    }

    public void initiateVariables() {
        //mapUnitId_RegId = new Map<Id,String>();
        units = new List<SelectOption>();
        //strSelectedUnit = '';
        //strSelectedlocation = '';
        //strStartDate = '';
        //strEndDate = '';
        locations = new List<SelectOption>();
        locations.add(new SelectOption('', '--None--'));

        mapLocation = new Map<Id, SelectOption>();
        //mapLocationId_Code = new Map<Id,String>();

        units.add(new SelectOption('', '--None--'));
        for (Booking_Unit__c unit : FmcUtils.queryUnitsForAccount(
            CustomerCommunityUtils.customerAccountId,
            'Id, Name, Registration_ID__c, Unit_Name__c, Inventory__c, Inventory__r.Property_Code__c,Inventory__r.Building_Location__c, '
                + 'Inventory__r.Building_Location__r.Id, Inventory__r.Building_Location__r.Name, '
                + 'Inventory__r.Building_Location__r.Building_Name__c, '
                + 'Inventory__r.Building_Location__r.Loams_Email__c, '
                + 'Inventory__r.Building_Location__r.Location_Code__c'
        )) {
            if (String.isBlank(unit.Registration_ID__c)) {
                continue;
            }
            units.add(new SelectOption(unit.Registration_ID__c, unit.Unit_Name__c));
            //mapUnitId_RegId.put(unit.Id, unit.Registration_ID__c);
            if (unit.Inventory__c != NULL && unit.Inventory__r.Building_Location__c != NULL) {
                mapLocation.put(
                    unit.Inventory__r.Building_Location__c, new SelectOption(
                        unit.Inventory__r.Property_Code__c, 
                        String.isBlank(unit.Inventory__r.Building_Location__r.Building_Name__c)
                            ? unit.Inventory__r.Building_Location__r.Name
                            : unit.Inventory__r.Building_Location__r.Building_Name__c
                        /*unit.Inventory__r.Property_Code__c*/
                        /*unit.Inventory__r.Building_Location__c,*/
                        /*String.isBlank(unit.Inventory__r.Building_Location__r.Building_Name__c)
                            ? unit.Inventory__r.Building_Location__r.Name
                            : unit.Inventory__r.Building_Location__r.Building_Name__c,
                        String.isBlank(unit.Inventory__r.Building_Location__r.Building_Name__c)
                            ? unit.Inventory__r.Building_Location__r.Name
                            : unit.Inventory__r.Building_Location__r.Building_Name__c*/
                    )
                );
                /*if(unit.Inventory__r.Building_Location__r.Location_Code__c != Null){
                	mapLocationId_Code.put(unit.Inventory__r.Building_Location__c, unit.Inventory__r.Building_Location__r.Location_Code__c);
                }*/
            }
        }
        locations.addAll(mapLocation.values());
        //system.debug('>>>mapLocationId_Code : '+mapLocationId_Code);

        strSelectedUnit = ApexPages.currentPage().getParameters().get('registrationId');
        if (String.isBlank(strSelectedUnit)) {
            strSelectedUnit = NULL;
        }
    }

    @RemoteAction
    public static String viewInvoice(Date startDate
                                   , Date endDate
                                   , String selectedUnit
                                   , String selectedlocation){
        String returnURL = '';

        system.debug('>>>startDate : '+startDate);
        system.debug('>>>endDate : '+endDate);
		system.debug('>>>selectedUnit : '+selectedUnit);
        system.debug('>>>selectedlocation : '+selectedlocation);

		if(String.isBlank(selectedlocation) && String.isBlank(selectedUnit)){
            if(startDate != null && String.isNotBlank(String.ValueOf(startDate))){
                returnURL = FmIpmsRestServices.getBulkInvoiceByPartyIdAndDate(CustomerCommunityUtils.getPartyId()
                                                         , startDate);
                system.debug('Party Id + date resp: '+returnURL);
            }else{
                //IPMS for all Party ID
                returnURL = FmIpmsRestServices.getBulkInvoiceByPartyId(CustomerCommunityUtils.getPartyId());
                system.debug('All Party Id Only resp: '+returnURL);
            }
        }else if(String.isNotBlank(selectedlocation) && String.isBlank(selectedUnit)){
            if(String.isBlank(String.ValueOf(startDate)) && String.isBlank(String.ValueOf(endDate))
               && startDate == null && endDate == null){
                //system.debug('>> Location : '+mapLocationId_Code);
                returnURL = FmIpmsRestServices.getBulkInvoiceByPartyIdAndProjectCode(CustomerCommunityUtils.getPartyId()
                                                    , String.ValueOf(selectedlocation));
                system.debug('Only PCode : '+returnURL);
            }else if(String.isNotBlank(String.ValueOf(startDate))
                  && String.isNotBlank(String.ValueOf(endDate))
                  && endDate != null && startDate != null){
                returnURL = FmIpmsRestServices.getBulkInvoiceByPartyIdDateRangeAndProjectCode(CustomerCommunityUtils.getPartyId()
                                                                      , startDate
                                                                      , endDate
                                                                      , String.ValueOf(selectedlocation));
            	system.debug('PCode + Both Dates : '+returnURL);
            }
        }else if(String.isBlank(selectedlocation) && String.isNotBlank(selectedUnit)){
            if(startDate != null && String.isNotBlank(String.ValueOf(startDate))){
                returnURL = FmIpmsRestServices.getInvoiceByRegistrationIdAndDate(selectedUnit, startDate);
                system.debug('Reg Id + date resp: '+returnURL);
            }else{
                //IPMS for all Party ID
                returnURL = FmIpmsRestServices.getInvoiceByRegistrationId(selectedUnit);
                system.debug('Only Reg Id resp: '+returnURL);
            }
        }
        system.debug('returnURL out : '+returnURL);
        return returnURL;
    }
}