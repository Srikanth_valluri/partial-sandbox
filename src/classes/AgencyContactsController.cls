public with sharing class AgencyContactsController {

        public List<ContactsWrapper> contactsWrapperList    {get; set;}
        public String accountName                           {get; set;}
        public String loggedinUserId;
        public String accountId;
        public String taskId;
        public Task taskRec;

        /***** CONSTRUCTOR *****/
        public AgencyContactsController(ApexPages.StandardController controller){
            loggedinUserId = UserInfo.getUserId();
            accountId = ApexPages.currentPage().getParameters().get('accId');
            taskId = ApexPages.currentPage().getParameters().get('taskId');
            if (String.isNotBlank(accountId) && String.isNotBlank(taskId)) {
                contactsWrapperList = new List<ContactsWrapper>();
                List<Contact> contactsList = new List<Contact>();
                List<String> contactNamesList = new List<String>();
                List<String> agencyContactNameList = new List<String>();
                ContactsWrapper contactsWrapperObj;
                taskRec = new Task();
                taskRec = [SELECT Id, Agency_Contacts_Involved__c, Activity_Members__c FROM Task
                                WHERE Id = :taskId LIMIT 1];
                if (String.isNotBlank(taskRec.Agency_Contacts_Involved__c)) {
                    agencyContactNameList = taskRec.Agency_Contacts_Involved__c.split(', ');
                }
                contactsList = [SELECT Id, Name, Account.Name
                                  FROM CONTACT
                                 WHERE AccountId = :accountId
                                   AND OwnerId = :loggedinUserId
                                   AND Status__c != 'Cancelled'
                ];
                for (Contact contactObj : contactsList) {
                    accountName = contactObj.Account.Name;
                    contactsWrapperObj = new ContactsWrapper(contactObj.Id, contactObj.Name, false);
                    contactsWrapperList.add(contactsWrapperObj);
                }
            }
        }

        // *****METHOD TO CRAETE NEW CONTACT ACTIVITY RECORDS ON CLISK OF SAVE BUTTON*****
        public void createActivityContactRecords() {
            if (taskRec != null) {
                List<Contact_Activity__c> contactActivityList = new List<Contact_Activity__c>();
                String taskContacts = '';
                Integer totalContacts = 0;
                for (ContactsWrapper wrapContact : contactsWrapperList) {
                    if (wrapContact.isSelected == true) {
                        Contact_Activity__c contactActivityObj = new Contact_Activity__c();
                        contactActivityObj.Name = wrapContact.wrapContactName;
                        contactActivityObj.Contact__c = wrapContact.wrapContactId;
                        contactActivityObj.TaskId__c = taskId;
                        if (taskRec != null) {
                            taskContacts += wrapContact.wrapContactName + ', ';
                            totalContacts++;
                        }
                        contactActivityList.add(contactActivityObj);
                    }
                }
                taskContacts = taskContacts.removeEnd(', ');

                if (String.isNotBlank(taskRec.Agency_Contacts_Involved__c)) {
                    taskRec.Agency_Contacts_Involved__c += ', ' + taskContacts;
                } else {
                    taskRec.Agency_Contacts_Involved__c = taskContacts;
                }
                if (String.isNotBlank(taskRec.Agency_Contacts_Involved__c)) {
                    List<String> contactsInvolvedList = taskRec.Agency_Contacts_Involved__c.split(', ');
                    taskRec.Activity_Members__c = contactsInvolvedList.size();
                }

                if (!contactActivityList.isEmpty()) {
                    insert contactActivityList;
                    update taskRec;
                }
            }
        }

        /************************************************************************************************
        * @Description : Wrapper Class to display the Contacts with a check box on the page             *
        * @Params      :                                                                                *
        * @Return      :                                                                                *
        *************************************************************************************************/
        public class ContactsWrapper {
            public Id wrapContactId                         {get; set;}
            public String wrapContactName                   {get; set;}
            public Boolean isSelected                       {get; set;}

            public ContactsWrapper (Id pContactId, String pContactName, Boolean pSelected) {
                wrapContactId = pContactId;
                wrapContactName = pContactName;
                isSelected = pSelected;
            }
        }

}