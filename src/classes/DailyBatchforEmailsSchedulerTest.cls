@isTest
private class DailyBatchforEmailsSchedulerTest {

    static testMethod void testScheduler() {
      Test.startTest();
        String jobName = 'demo job';
        DailyBatchforEmailsScheduler ascsObject = new DailyBatchforEmailsScheduler (jobName);
      String sch = '0 0 23 * * ?'; 
      system.schedule(jobName+System.Now(), sch, ascsObject); 
    Test.stopTest();
    }
}