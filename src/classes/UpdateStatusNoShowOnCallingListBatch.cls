/**
 * @File Name          : UpdateStatusNoShowOnCallingListBatch.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 11/14/2019, 4:42:26 PM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    11/6/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public with sharing class UpdateStatusNoShowOnCallingListBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{
    
    public String query;
    
    public Database.Querylocator start( Database.BatchableContext bc ) {
        String recordTypeLabel = System.Label.Calling_List_Record_Type_Appointment_Scheduling;
        return Database.getQueryLocator([
            SELECT Id
                 , Booking_Unit__c
                 , Appointment_Status__c 
              FROM Calling_List__c 
             WHERE Sub_Purpose__c = 'Unit Viewing' 
               AND Service_Type1__c = 'Handover' 
               AND RecordTypeId = :recordTypeLabel 
               AND Appointment_Status__c = 'Confirmed'
               AND Appointment_Date__c = TODAY 
               AND Account__c != NULL 
               AND Booking_Unit__c != NULL 
               AND Booking_Unit__r.Snags_Reported__c != TRUE 
               AND Booking_Unit__r.Snags_Completed__c  != TRUE]);
    }

    public void execute( Database.BatchableContext bc, List<Calling_List__c> lstCallingList ) {
        if(!lstCallingList.isEmpty()){
            for(Calling_List__c objCalling_List: lstCallingList){
                objCalling_List.Appointment_Status__c = 'No Show';
            }
        }
        update lstCallingList;
    }

    public void finish( Database.BatchableContext bc ) {

    }
}