@isTest
/*
* Revision History: 
* Version   Author          Date        Description.
* 1.1       Swapnil Gholap  21/11/2017  Initial Draft
*/

public class RefundsRuleTest {    
    static testMethod void Test1(){
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new RefundRuleServiceMock());
               
        RefundsRule.TokenRefundsTransfersRuleHttpSoap11Endpoint obj = new RefundsRule.TokenRefundsTransfersRuleHttpSoap11Endpoint();
        String response = obj.TokenRefundsTransfers('123','TEST','TEST','TEST','TEST','TEST','TEST','TEST','TEST','TEST','TEST','TEST','TEST','TEST','TEST','TEST','TEST','TEST','TEST','TEST','TEST','TEST','TEST','TEST','TEST');
        System.debug('--response--'+response);
    }
}