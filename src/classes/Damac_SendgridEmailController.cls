/*****************************************************************************************************************
* Name              : Damac_SendgridEmailController
* Test Class        : Damac_SendgridEmailController_Test
* Description       : Webservice Class to Send email using SendGrid
* -------------------------------------------------------------------------------------------------------------
* VERSION       AUTHOR          DATE            COMMENTS
* 1.0            QBurst      29/06/2018          Created
* 1.1            QBurst      09/03/2020          Added new custom settings to fetch SendGrid Credentials
****************************************************************************************************************/
Public class Damac_SendgridEmailController {

    @Future (callout = TRUE)
    public static void sendEmailAsync (List <Id> newRecordIds) {
        sendEmail (newRecordIds);
    }

    public static void sendEmail (List <Id> newRecordIds) {
        Map<Id, SendGrid_Credentials_Sender_based__c> reqCredentialsMap 
                                = new Map<Id, SendGrid_Credentials_Sender_based__c>(); // 1.1
        String query = ' SELECT Email_Request__r.Related_Object_API__c, Email_Request__r.Email_Field_API__c, '
                        + ' Email_Request__r.From_Email__c, Email_Request__r.From_Name__c, '
                        + 'Email_Request__r.Reply_To_Email__c, '
                        + 'Email_Request__r.Sender_Account__c,' // 1.1
                        + ' Email_Request__r.Email_Template__c, '
                        + getAllFields ('Email_Metrics__c')
                        +' FROM Email_Metrics__c WHERE ID IN: newRecordIds AND Duplicate__c = false ';
        Sendgrid_Credentials__c credentials = Sendgrid_Credentials__c.getInstance (UserInfo.getUserID ());
        Map <ID, List <Email_tags__c>> emailTagsmap = new Map <ID, List <Email_tags__c>> ();
        Map <String, Set <String>> objectRelatedFields = new Map <String, Set <String>> ();
        Map <ID, String> responseMap = new Map <ID, String> ();
        Map <ID, String> errorResponseMap = new Map <ID, String> ();
        Map <ID, List <Email_Metrics__c>> emailRequestWithMetricIdsMap = new Map <ID, List <Email_Metrics__c>> ();
        Map <Email_Request__c, List <Email_Metrics__c>> emailRequestWithMetricsMap
                                             = new Map <Email_Request__c , List <Email_Metrics__c>> ();
        List <Email_Metrics__c> emailMetricsList = new List <Email_Metrics__c> ();
        emailMetricsList = Database.Query (Query);
        Set <ID> projectDetailRecIds = new Set <ID> ();
        for (Email_Metrics__c metrics: emailMetricsList) {
            if (!emailRequestWithMetricIdsMap.containsKey (metrics.Email_Request__c)) {
                emailRequestWithMetricIdsMap.put (metrics.Email_Request__c, new List <Email_Metrics__c> {metrics});
            } else {
                emailRequestWithMetricIdsMap.get (metrics.Email_request__c).add (metrics);
            }
            // 1.1 starts 
            if(metrics.Email_Request__r.Sender_Account__c != NULL 
             && metrics.Email_Request__r.Sender_Account__c != ''
             && SendGrid_Credentials_Sender_based__c.getValues(metrics.Email_Request__r.Sender_Account__c) != null){
                reqCredentialsMap.put(metrics.Email_Request__c, 
                    SendGrid_Credentials_Sender_based__c.getValues(metrics.Email_Request__r.Sender_Account__c));
            }   
            // 1.1 ends
            if (metrics.Share_Project_Details__c != NULL) {
                projectDetailRecIds.add(metrics.Share_Project_Details__c);  
            }
        }
        System.Debug ('emailRequestWithMetricIdsMap: ' + emailRequestWithMetricIdsMap);
        String pdQuery = 'SELECT ' + getAllFields ('Share_Project_Details__c')
                        + ' FROM Share_Project_Details__c WHERE ID IN :projectDetailRecIds';
        Map <ID, Share_Project_Details__c> projDetailsMap = new Map <ID, Share_Project_Details__c> ();
        for (Share_Project_Details__c pd: Database.Query (pdQuery)) {
            projDetailsMap.put (pd.ID, pd);
        }
        for (Email_Request__c req: [SELECT Email_Field_API__c, From_Email__c, 
                                        From_Name__c, Reply_To_Email__c, 
                                        Email_Template__c, Related_Object_API__c, Template_For__c 
                                    FROM Email_Request__c 
                                    WHERE ID IN :emailRequestWithMetricIdsMap.keySet ()]) {
                                     
            emailRequestWithMetricsMap.put (req, emailRequestWithMetricIdsMap.get (req.ID));
            if (req.Email_Field_API__c != NULL) {
                objectRelatedFields.put (req.Related_Object_API__c, new Set <String> {req.Email_Field_API__c});
            }
        }
        for (Email_Tags__c tag : [SELECT Test_Merge_Data__c, Email_Request__c, 
                                    Email_Request__r.Related_Object_API__c, 
                                    Email_Request__r.Email_Field_API__c,
                                    Send_grid_Template_Tag__c, SF_Field_API__c 
                                  FROM Email_Tags__c 
                                  WHERE Email_Request__c IN :emailRequestWithMetricIdsMap.keySet ()]) {
            if (!emailTagsmap.containsKey (tag.Email_request__c)) {
                emailTagsmap.put (tag.Email_request__c, new List <Email_Tags__c> {tag});
            } else {
                emailTagsmap.get (tag.Email_request__c).add (tag);
            }
            if (!objectRelatedFields.containsKey (tag.Email_Request__r.Related_Object_API__c)) {
                objectRelatedFields.put (tag.Email_Request__r.Related_Object_API__c, 
                                                                new Set <String> {tag.SF_Field_API__c });
            } else {
                objectRelatedFields.get (tag.Email_Request__r.Related_Object_API__c).add (tag.SF_Field_API__c);
            }
        }
        System.Debug ('emailRequestWithMetricsMap: ' + emailRequestWithMetricsMap);
        System.Debug ('emailTagsmap: ' + emailTagsmap);
        Map <String, Set <ID>> objectRelatedRecords = new Map <String, Set <ID>> ();
        
        for (Email_Request__c req :emailRequestWithMetricsMap.keySet ()) {
            for (Email_Metrics__c metrics :emailRequestWithMetricsMap.get (req)) {
                if (!objectRelatedRecords.containsKey (req.Related_Object_API__c)) {
                    objectRelatedRecords.put (req.Related_Object_API__c, new Set <ID> {metrics.sObject_Id__c});
                } else {
                    objectRelatedRecords.get (req.Related_Object_API__c).add (metrics.sObject_Id__c);
                }
            }
        }
        System.Debug ('objectRelatedRecords: ' + objectRelatedRecords );
        System.Debug ('objectRelatedFields: ' + objectRelatedFields );
        Map <ID, Sobject> recordDetailsmap = new Map <ID, SObject> ();
        for (String key :objectRelatedRecords.keySet ()) {
            String fields = '';
            for (String field : objectRelatedFields.get (key)) {
                if (field != 'Email_Metrics_ID') {
                    fields += field + ',';
                }
            }
            fields = fields.removeEnd (',');
            Set<ID> recordIds = objectRelatedRecords.get (key);
            String innerQuery = 'SELECT ' + fields + ' FROM ' + key + ' WHERE ID IN :recordIds';
            System.debug (innerQuery);
            recordDetailsMap = new Map <ID, SObject> (Database.query (innerQuery));
        }
        System.Debug ('recordDetailsMap: ' + recordDetailsMap);
        for (Email_Metrics__c metrics: emailMetricsList) {
            // 1.1 starts
            String email = '', fromEmail = '', fromName = '', replyTo = '', apiKey = '', endPoint = '';
            if(reqCredentialsMap.containsKey(metrics.Email_Request__c)){
                SendGrid_Credentials_Sender_based__c cred = reqCredentialsMap.get(metrics.Email_Request__c);
                fromEmail = cred.From_Email__c;
                fromName = cred.From_Name__c;
                endPoint = cred.End_Point_URL__c;
                apiKey = cred.API_Key__c;
            } else{
                fromEmail = credentials.From_Email__c;
                fromName = credentials.From_Name__c;
                endPoint = credentials.Endpoint_URL__c;
                apiKey = credentials.API_Key__c;
            }
            System.debug('fromEmail: ' + fromEmail);
            System.debug('fromName: ' + fromName);
            System.debug('endPoint: ' + endPoint);
            System.debug('apiKey: ' + apiKey);
            // 1.1 ends
            SObject record = recordDetailsMap.get (metrics.sObject_Id__c);
            if (metrics.Email__c != NULL) {
                email = metrics.Email__c;
            } else {                
                String emailField = metrics.Email_Request__r.Email_Field_API__c;
                email = String.valueOf (record.get (emailField));
            }
            String substitutionValues = '';
            Map <String, String> substitutionTagsMap = new Map <String, String> ();
            if (emailTagsmap.containsKey (metrics.Email_Request__c)) {
                for (Email_Tags__c tag :emailTagsmap.get (metrics.Email_request__c)) {
                    String val = '';
                    String key = '-'+tag.Send_grid_Template_Tag__c+'-';
                    if (key == '-Email_Metrics_ID-') {
                        val = UtilityHelperCls.encryptMobile(metrics.ID);
                    } else {
                        if (metrics.Is_Test_Email__c == TRUE) {
                            val = tag.Test_Merge_Data__c;
                        } else {
                            val = String.valueOf (record.get (tag.SF_Field_API__c));
                        }
                    }
                    if (val == NULL) {
                        val = '';
                    }
                    substitutionTagsMap.put (key, val);
                }
            }
            String pdCommentsVal = '';
            if (metrics.Share_Project_Details__c != NULL) {
                if (projDetailsMap.containskey (metrics.Share_Project_Details__c)) {
                    if (projDetailsMap.get (metrics.Share_Project_Details__c).get ('Comments__c') != NULL) {
                      pdCommentsVal 
                        = String.valueOf(projDetailsMap.get (metrics.Share_Project_Details__c).get('Comments__c')); 
                    }
                }
            }
            substitutionTagsMap.put('-Comments__c-', pdCommentsVal);
            if(metrics.Email_Request__r.From_Email__c != NULL){
                fromEmail = metrics.Email_Request__r.From_Email__c;
            }
            if(metrics.Email_Request__r.From_Name__c != NULL){
                fromName = metrics.Email_Request__r.From_Name__c;
            }
            if(metrics.Email_Request__r.Reply_To_Email__c != NULL){
                replyTo = metrics.Email_Request__r.Reply_To_Email__c;
            }
            System.debug('fromEmail: ' + fromEmail);
            System.debug('fromName: ' + fromName);
            System.debug('replyTo: ' + replyTo);
            if (fromEmail == NULL) {
                fromEmail = '';
            }
            if (fromName == NULL) {
                fromName = '';
            }
            JSONGenerator gen = formatRequestBody (metrics.ID, Email, '', fromName, fromEmail, replyTo,
                                    '', metrics.Email_Request__r.Email_Template__c, substitutionTagsMap, 
                                    metrics.SalesOffer_Record__c,metrics.Email_Request__c);
            HTTPRequest req = new HTTPRequest ();
            req.setEndpoint (endPoint);
            req.setMethod ('POST');
            System.Debug ('Request Body :::: ' + gen.getAsString());
            req.setBody(gen.getAsString());
            req.setHeader('Authorization', 'Bearer ' + apiKey);
            req.setHeader('content-type', 'application/json');
            HTTP http = new HTTP ();
            HTTPResponse res = new HTTPresponse ();
            if (!Test.isRunningTest ()) {
                res = http.send (req);
            } else {
                res.setStatusCode (202);
                res.setheader ('X-Message-Id', 'dfadsfasf11fsdfa');
            }
            if (res.getStatusCode() == 202) {
                responseMap.put (metrics.ID, res.getHeader('X-Message-Id'));
            } else {
                SendGridErrorHandler errorData = SendGridErrorHandler.parse(res.getBody());
                if (errorData != null) {
                    if ( errorData.errors != null) {
                        errorResponseMap.put (metrics.ID, errorData.errors[0].message);
                    }
                }
            }
        }
        List <Email_Metrics__c> metricsToUpdate = new List <Email_Metrics__c> ();
        for (ID key : responseMap.keySet ()) {
            Email_Metrics__c metric = new Email_Metrics__c ();
            metric.ID = key;
            metric.Email_Sent_Date_Time__c = DateTime.Now ();
            metric.Sendgrid_Email_Message_Id__c = responseMap.get (key);
            metric.Error_Response__c = NULL;
            metric.Email_Send__c = TRUE;
            metricsToUpdate.add (metric);
        }
        for (ID key :errorResponseMap.keySet ()) {
            Email_Metrics__c metric = new Email_Metrics__c ();
            metric.ID = key;
            metric.Email_Send__c = TRUE;
            metric.Error_Response__c = errorResponseMap.get (key);
            metricsToUpdate.add (metric);
        }
        Damac_Constants.skip_EmailMetricsTrigger = TRUE;
        try {
            Update metricsToUpdate;
        }catch (Exception e) {}
    }

    public static JSONGenerator formatRequestBody (ID uniqueId, String toAddresses, String name, 
                                        String senderName, String senderEmail, String replyTo, 
                                        String subject, String templateId, 
                                        Map <String, String> substitutionTags, ID offerID,id parentReqId) {
        /**/
        boolean hasAttachment = false;
        list<attachment> EmailRequestAttachments = new list<attachment>();
        EmailRequestAttachments  = [SELECT Id, Name, body, ContentType 
                                    FROM attachment WHERE parentId=: parentReqId];
        if(EmailRequestAttachments.size() > 0 ){
            hasAttachment = true;
        }
        /**/
        System.Debug ('JSON Creation ::::');
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
            gen.writeFieldName('personalizations');
                gen.writeStartArray();
                    gen.writeStartObject();
                        gen.writeFieldName('to');
                            gen.writeStartArray();
                                gen.writeStartObject();
                                    gen.writeStringField('email', toAddresses);
                                    //gen.writeStringField('name', name);
                                gen.writeEndObject();
                            gen.writeEndArray();
                        gen.writeStringField('subject', Subject);

                        gen.writeFieldName('substitutions');
                        gen.writeStartObject();
                        for (String key :substitutionTags.keySet ()) {
                            String val = substitutionTags.get (key);
                            if (key == 'Email_Metrics_ID') {
                                val = uniqueId;
                            }
                            if (val != NULL) {
                                gen.writeStringField(key, val);
                            } else {
                                gen.writeStringField(key, ''); 
                            }
                        }
                        gen.writeEndObject();
                    gen.writeEndObject();
                gen.writeEndArray();
            gen.writeFieldName('from');
            gen.writeStartObject();
            gen.writeStringField('email', senderEmail);
            gen.writeStringField('name', senderName);
            gen.writeEndObject();

            gen.writeFieldName('tracking_settings');
            gen.writeStartObject();
                gen.writeFieldName('click_tracking');
                gen.writeStartObject();
                    gen.writeBooleanField('enable', true);
                gen.writeEndObject();
            gen.writeEndObject();

            if (replyTo != NULL && replyTo.trim() != '') {
                gen.writeFieldName('reply_to');
                gen.writeStartObject();
                gen.writeStringField('email', replyTo);
                gen.writeEndObject();
            }
            gen.writeFieldName('custom_args');
            gen.writeStartObject();
            gen.writeStringField('uniqueId', UniqueId);
            gen.writeEndObject();

            if (offerID != NULL) {
                String attachmentBody;
                if (!Test.isRunningTest ()) {
                    PageReference redirectPage = Page.Damac_InventoryDetail_Clone;
                    redirectPage.getParameters().put('recID', offerid);
                    Blob mailattachment = redirectPage.getContentAsPDF();
                    attachmentBody = EncodingUtil.base64Encode (mailattachment);
                } else {
                    attachmentBody = EncodingUtil.base64Encode (blob.valueOf ('eafsafafas'));
                }
                //Attachments
                gen.writeFieldName('attachments');
                gen.writeStartArray();
                    gen.writeStartObject();
                        gen.writeStringField('content', attachmentBody);
                        gen.writeStringField('type', 'application/pdf');
                        gen.writeStringField('filename','Damac Sales Offer.pdf');
                    gen.writeEndObject();
                gen.writeEndArray();
            }
            //attachments
            if(hasAttachment){
                gen.writeFieldName('attachments');
                gen.writeStartArray();
                for(attachment a: EmailRequestAttachments){
                    gen.writeStartObject();
                    gen.writeStringField('content', EncodingUtil.base64Encode (a.body));
                    gen.writeStringField('type', a.ContentType);
                    gen.writeStringField('filename',a.Name);
                    gen.writeEndObject();
                }
                gen.writeEndArray();
            }
            gen.writeStringField ('template_id', templateID);
        gen.writeEndObject();
        System.Debug ('JSON Creation ::::' + gen);
        return gen;
    }

    public static string getAllfields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        if (objectType == null) {
            return fields;
        }
        for (string f :objectType.getDescribe ().fields.getMap ().keySet ()) {
            fields += f + ', ';
        }
        return fields.removeEnd(', ');
    }
}