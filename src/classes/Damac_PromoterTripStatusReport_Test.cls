@isTest
public class Damac_PromoterTripStatusReport_Test {

     
    
     public static testmethod void getInquryrecordTest1(){
         Inquiry__c inqObj= new Inquiry__c(
               
                Inquiry_Source__c='Agent Referral',
                Mobile_Phone_Encrypt__c='456123',
                Mobile_CountryCode__c='American Samoa: 001684',
                Mobile_Phone__c='1234',
                Email__c='mk@gmail.com',
                First_Name__c='Test',
                Last_Name__c='Last',
                Meeting_Type__c = 'Scheduled Tour'
            ); 
         insert inqObj;
        List<Trip__c> Trips = new List<Trip__c>();
        for (Integer i = 0; i < 25; i++) {
            Trip__c trip= new Trip__c(
               
                inquiry__c = inqObj.Id,
              Drop_Address__c ='American Samoa: 001684',
              Pick_Up_Address__c ='1234',
               Trip_date__c =Date.newInstance(2016, 12, 9),
               Start_Time__c=datetime.newInstance(2014, 9, 15, 12, 30, 0),
             End_Time__c  =datetime.newInstance(2014, 9, 15, 22, 30, 0),
                Status__c = 'Completed'
            );
            Trips.add(Trip);
        }
        insert Trips;
        
        Damac_PromoterTripsStatusReport obj = new Damac_PromoterTripsStatusReport();
        obj.getTourStatus();
        system.assertEquals(false, obj.isSF1);
       // Boolean abc = PromotorSearchController.getisSF1(true);
        obj.searchString = 'Test';
        obj.StatusValue = 'Select Tour Status';
        obj.fstbtn();
        obj.nextbtn();
        obj.endbtn();
       // obj.prvbtn();
        obj.inqClear();
        obj.getnxt();
        obj.getprv();
        obj.sortListASC();
        obj.sortListDSC();
        
        //
        
        
        //
               
        obj.searchString = '';
        obj.searchFrmDte = '2030-03-17';
        
        obj.searchresult();
    }

    public static testmethod void getInquryrecordTest2(){
        List<Inquiry__c> lstInquiry = new List<Inquiry__c>();
        for (Integer i = 0; i < 10; i++) {
            Inquiry__c inqObj= new Inquiry__c(
               
                Inquiry_Source__c='Agent Referral',
                Mobile_Phone_Encrypt__c='456123',
                Mobile_CountryCode__c='American Samoa: 001684',
                Mobile_Phone__c='1234',
                Email__c='mk@gmail.com',
                First_Name__c='Test',
                Last_Name__c='Last',
                Meeting_Type__c = 'Scheduled Tour'
            );
            lstInquiry.add(inqObj);
        }
        insert lstInquiry;

        Damac_PromoterTripsStatusReport obj = new Damac_PromoterTripsStatusReport();
        obj.getTourStatus();
        obj.searchString = '';
        obj.statusValue = 'Cancelled';
        obj.fstbtn();
        obj.nextbtn();
        obj.endbtn();
        //obj.prvbtn();
        obj.inqClear();
    }

 public static testmethod void getInquryrecordTest3(){
        Damac_PromoterTripsStatusReport obj = new Damac_PromoterTripsStatusReport();
 Inquiry__c inqObj= new Inquiry__c(
               
                Inquiry_Source__c='Agent Referral',
                Mobile_Phone_Encrypt__c='456123',
                Mobile_CountryCode__c='American Samoa: 001684',
                Mobile_Phone__c='1234',
                Email__c='mk@gmail.com',
                First_Name__c='Test',
                Last_Name__c='Last',
                Meeting_Type__c = 'Scheduled Tour'
            ); 
         insert inqObj;
        List<Trip__c> Trips = new List<Trip__c>();
        for (Integer i = 0; i < 25; i++) {
            Trip__c trip= new Trip__c(
               
                inquiry__c = inqObj.Id,
              Drop_Address__c ='American Samoa: 001684',
              Pick_Up_Address__c ='1234',
               Trip_date__c =Date.newInstance(2016, 12, 9),
               Start_Time__c=datetime.newInstance(2014, 9, 15, 12, 30, 0),
             End_Time__c  =datetime.newInstance(2014, 9, 15, 22, 30, 0),
                Status__c = 'Completed'
            );
            Trips.add(Trip);
        }
        insert Trips;
        obj.getTourStatus();
        obj.searchString = 'Test';
        obj.StatusValue = 'Completed';
        obj.fstbtn();
        obj.nextbtn();
        obj.endbtn();
        //obj.prvbtn();
        obj.inqClear();
        obj.cancel();
       obj.getTripsrecord();
       obj.getTourStatus();
       obj.searchresult();
       obj.inqClear();
       obj.cancel();
       obj.cancelbtn();
       obj.nextbtn();
       obj.prvbtn();
       obj.fstbtn();
       obj.getprv();
       obj.getnxt();
       obj.sortListASC();
       obj.sortListDSC();
       obj.setRecCount();
            
     
    }
   
 public static testmethod void getInquryrecordTest4(){
       Damac_PromoterTripsStatusReport obj = new Damac_PromoterTripsStatusReport();
         Inquiry__c inqObj= new Inquiry__c(
               
                Inquiry_Source__c='Agent Referral',
                Mobile_Phone_Encrypt__c='456123',
                Mobile_CountryCode__c='American Samoa: 001684',
                Mobile_Phone__c='1234',
                Email__c='mk@gmail.com',
                First_Name__c='Test',
                Last_Name__c='Last',
                Meeting_Type__c = 'Scheduled Tour'
            ); 
         insert inqObj;
        List<Trip__c> Trips = new List<Trip__c>();
        for (Integer i = 0; i < 25; i++) {
            Trip__c trip= new Trip__c(
               
                inquiry__c = inqObj.Id,
              Drop_Address__c ='American Samoa: 001684',
              Pick_Up_Address__c ='1234',
               Trip_date__c =Date.newInstance(2016, 12, 9),
               Start_Time__c=datetime.newInstance(2014, 9, 15, 12, 30, 0),
             End_Time__c  =datetime.newInstance(2014, 9, 15, 22, 30, 0),
                Status__c = 'Completed'
            );
            Trips.add(Trip);
        }
        insert Trips;

        obj.getTourStatus();
        obj.searchString = 'Test';
        obj.StatusValue = 'Cancelled';
        obj.fstbtn();
        obj.nextbtn();
        obj.endbtn();
        //obj.prvbtn();
        obj.inqClear();
        obj.cancel();
       obj.getTripsRecord();
       obj.getTourStatus();
       obj.searchresult();
       obj.inqClear();
       obj.cancel();
       obj.cancelbtn();
       obj.nextbtn();
       obj.prvbtn();
       obj.fstbtn();
       obj.getprv();
       obj.getnxt();
       obj.sortListASC();
       obj.sortListDSC();
       obj.setRecCount();
       
     
    }

  }