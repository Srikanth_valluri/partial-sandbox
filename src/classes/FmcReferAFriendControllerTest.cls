@isTest
private class FmcReferAFriendControllerTest {

    @isTest
    static void testController() {
        Account account = new Account(Name = 'Test Account');
        insert account;

        CustomerCommunityUtils.customerAccountId = account.Id;

        PageReference currentPage = Page.CommunityPortal;
        currentPage.getParameters().put('view', 'ReferFriend');
        Test.setCurrentPage(currentPage);

        Test.startTest();
            FmcReferAFriendController controller = new FmcReferAFriendController();
            controller.save();
        Test.stopTest();
    }

}