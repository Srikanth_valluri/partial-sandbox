@isTest
private class CancelFundTransferControllerTest
{

    static  testmethod void testAutoRun(){
         Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Fund Transfer Active Units').getRecordTypeId();
         list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
         insert accountobj;
         Case objCase =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
         insert objCase;
         test.startTest();
         PageReference pageRef = Page.CancelFundTransfer;
         Test.setCurrentPage(pageRef);
         ApexPages.StandardController sc = new ApexPages.standardController(objCase);
         CancelFundTransferController  controller = new CancelFundTransferController(sc);
         Pagereference newPg = controller.cancelCase();
         //System.assertEquals(null,controller.cancelCase());
         test.stopTest();
    }

    static  testmethod void testAutoRun1() {
         Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Fund Transfer Active Units').getRecordTypeId();
         list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
         insert accountobj;
         Case objCase =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
         objCase.Status='Submitted';
         insert objCase;
         test.startTest();
         PageReference pageRef = Page.CancelFundTransfer;
         Test.setCurrentPage(pageRef);
         ApexPages.StandardController sc = new ApexPages.standardController(objCase);
         CancelFundTransferController  controller = new CancelFundTransferController(sc);
         Pagereference newPg = controller.cancelCase();
         //System.assertEquals(null,controller.cancelCase());
         test.stopTest();
     }

}