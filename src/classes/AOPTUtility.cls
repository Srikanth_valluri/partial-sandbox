public with sharing class AOPTUtility 
{
  //Constructor
  public AOPTUtility() 
  {
  }
  
  // Method to convert date in string format dd/mm/yyyy to date instance
  public static Date getDateInstanceFromString(String strDate) {
      List<String> lstDTString = strDate.split('/');
      Integer intDay = Integer.valueOf(lstDTString[0]);
      Integer intMonth = Integer.valueOf(lstDTString[1]);
      Integer intYear = Integer.valueOf(lstDTString[2]);
      Date dtConvertedDate = Date.newInstance(intYear,intMonth,intDay);
      return dtConvertedDate;
  }
  
  // method used to format date to dd-MMM-YYYY (11-Oct-2017)
  public static String formatDate(String dt)
  {   
      //Date objDateToFormat = Date.parse(dt);
      //String strFormatedDate = DateTime.newInstance(objDateToFormat.year(),objDateToFormat.month(),objDateToFormat.day()).format('d-MMM-yyyy');
      //system.debug('strFormatedDate '+strFormatedDate);
      
      List<String> lstDTString = dt.split('/');
      Integer intDay = Integer.valueOf(lstDTString[0]);
      Integer intMonth = Integer.valueOf(lstDTString[1]);
      Integer intYear = Integer.valueOf(lstDTString[2]);
      String strFormatedDate = DateTime.newInstance(intYear,intMonth,intDay).format('d-MMM-yyyy');
      return strFormatedDate;
  }

  public static Boolean calculateACDExtentsion(String strDateCurACD,String strDateProposed)
  { 
    system.debug('--strCurrentACD--'+strDateCurACD);
    system.debug('--strDateProposed--'+strDateProposed);

    Date objDateToFormat1;
    Date objDateToFormat2;

    if(String.isBlank(strDateCurACD) || String.isBlank(strDateProposed))
    {
      objDateToFormat1 = Date.parse(System.today().format());
      objDateToFormat2 = Date.parse(System.today().format());
    }
    
    if(String.isNotBlank(strDateCurACD) && strDateCurACD.contains('-'))
    {
      objDateToFormat1 = convertDateFormat(strDateCurACD);
    }

    //objDateToFormat1 = Date.parse(strDateCurACD);
    String strFormatedDate = DateTime.newInstance(objDateToFormat1.year(),objDateToFormat1.month(),objDateToFormat1.day()).format('dd/MM/YYYY');
    //objDateToFormat1 = Date.parse(strFormatedDate);
    objDateToFormat1 = getDateInstanceFromString(strFormatedDate);
    //system.debug('strFormatedDate '+strFormatedDate);

    if(String.isNotBlank(strDateProposed) && strDateProposed.contains('-'))
    {
      objDateToFormat2 = convertDateFormat(strDateProposed);
    }
    else
    {
      system.debug('--strDateProposed inside else--'+strDateProposed);
      //objDateToFormat2 = Date.parse(strDateProposed);
      objDateToFormat2 = getDateInstanceFromString(strDateProposed);
    }

    //system.debug('calculateACDExtentsion objDateToFormat1 '+objDateToFormat1);
    //system.debug('calculateACDExtentsion objDateToFormat2 '+objDateToFormat2);
    if(objDateToFormat1 < objDateToFormat2)
    {
      return true;
    }
    return false;
  }

  //Method used to convert date format from dd-mmm-yyyy to SF format.
  public static Date convertDateFormat(String myDate)
  {
    
    String[] myDateOnly = myDate.split(' ');
    String[] strDate = myDateOnly[0].split('-');
    Integer myIntDate = integer.valueOf(strDate[0]);
    String myStrMonth = String.valueOf(strDate[1]);
    Integer myIntYear = integer.valueOf(strDate[2]);

    Integer intMonth;
    if(myStrMonth.equalsIgnoreCase('jan'))
    {
      intMonth = 1;
    }

    if(myStrMonth.equalsIgnoreCase('feb'))
    {
      intMonth = 2;
    }

    if(myStrMonth.equalsIgnoreCase('mar'))
    {
      intMonth = 3;
    }

    if(myStrMonth.equalsIgnoreCase('apr'))
    {
      intMonth = 4;
    }

    if(myStrMonth.equalsIgnoreCase('may'))
    {
      intMonth = 5;
    }

    if(myStrMonth.equalsIgnoreCase('jun'))
    {
      intMonth = 6;
    }

    if(myStrMonth.equalsIgnoreCase('jul'))
    {
      intMonth = 7;
    }

    if(myStrMonth.equalsIgnoreCase('aug'))
    {
      intMonth = 8;
    }

    if(myStrMonth.equalsIgnoreCase('sep'))
    {
      intMonth = 9;
    }

    if(myStrMonth.equalsIgnoreCase('oct'))
    {
      intMonth = 10;
    }

    if(myStrMonth.equalsIgnoreCase('nov'))
    {
      intMonth = 11;
    }

    if(myStrMonth.equalsIgnoreCase('dec'))
    {
      intMonth = 12;
    }

    Date objDate = Date.newInstance(myIntYear, intMonth, myIntDate);
    system.debug('date '+objDate);
    return objDate;
  }

  public static String calculateDateDiff(String strDate1,String strDate2)
  { 
    //system.debug('strDate1 '+strDate1);
    //system.debug('strDate2 '+strDate2);
    Date objDateToFormat1;
    if(String.isNotBlank(strDate1) && strDate1.contains('-'))
    {
      objDateToFormat1 = convertDateFormat(strDate1);
    }
    else if(String.isNotBlank(strDate1))
    {
      //objDateToFormat1 = Date.parse(strDate1);
      objDateToFormat1 = getDateInstanceFromString(strDate1);
    }
    else
    {
      objDateToFormat1 = Date.parse(System.today().format());
    }
    //Date objDateToFormat1 = Date.parse(strDate1);
    String strFormatedDate1 = DateTime.newInstance(objDateToFormat1.year(),objDateToFormat1.month(),objDateToFormat1.day()).format('dd/MM/YYYY');
    //objDateToFormat1 = Date.parse(strFormatedDate1);
    //system.debug('strFormatedDate1 '+strFormatedDate1);

    Date objDateToFormat2;
    if(strDate2.contains('-'))
    {
      objDateToFormat2 = convertDateFormat(strDate2);
    }
    else
    {
      //objDateToFormat2 = Date.parse(strDate2);
      objDateToFormat2 = getDateInstanceFromString(strDate2);
    }
    String strFormatedDate2 = DateTime.newInstance(objDateToFormat2.year(),objDateToFormat2.month(),objDateToFormat2.day()).format('dd/MM/YYYY');
    //system.debug('strFormatedDate2 '+strFormatedDate2);

    Integer days = objDateToFormat1.daysBetween(objDateToFormat2);
    //system.debug('days diff '+days);
    return String.valueOf(days);
  }

  // method used to format the currency string to thousand separator
  /*private static String formatCurrency(String strCurrency)
  {   
      if(String.isBlank(strCurrency))
      {
          return '';
      }
      String strFormattedCurrency = ( Decimal.valueOf(strCurrency==null||strCurrency.trim()==''?'0':strCurrency).setScale(2) + 0.001 ).format();
      String strFormatCompleted = strFormattedCurrency.substring(0,strFormattedCurrency.length()-1);
      System.debug('formatCurrency strFormatCompleted '+strFormatCompleted);
      return strFormatCompleted;
  }
  */
  // this method is used to validate if AOPT SR is allowed to be created for selected Booking Units
  public static Map<String,Object> validateSRInitiaton(Set<Id> bookingUnitIDSet, String strDeveloperName)
  {
    List<Case> lstExistingCase = new List<Case>();
    List<SR_Booking_Unit__c> lstSRBookingUnits = new List<SR_Booking_Unit__c>();

    Set<String> setNotAllowedSRTypes = new Set<String>();

    Map<String,Object> mapExistingCase = new Map<String,Object>();

    setNotAllowedSRTypes.add('Parking');
    setNotAllowedSRTypes.add('AOPT');
    setNotAllowedSRTypes.add(strDeveloperName);
    setNotAllowedSRTypes.add('Change of Details');
    setNotAllowedSRTypes.add('Promotion Package');
    setNotAllowedSRTypes.add('Handover');
    setNotAllowedSRTypes.add('Penalty Waiver');
    setNotAllowedSRTypes.add('Customer Refund');
    setNotAllowedSRTypes.add('Token Refund');
    setNotAllowedSRTypes.add('Rental Pool Agreement');
    setNotAllowedSRTypes.add('Rental Pool Termination');
    setNotAllowedSRTypes.add('Title Deed');
    setNotAllowedSRTypes.add('Cancel & Transfer');
    setNotAllowedSRTypes.add('Plot Handover');
    setNotAllowedSRTypes.add('Cheque Replacement SR');
    
    
    map<Id,Case> mapId_Case = new map<Id,Case>([SELECT c.Id
                                                     , c.Booking_Unit__c
                                                     , c.Booking_Unit__r.Name
                                                     , c.Booking_Unit__r.Unit__c
                                                     , c.AccountId
                                                     , c.CaseNumber
                                                     , c.RecordType.DeveloperName
                                                     , c.RecordType.Name
                                                FROM Case c
                                                WHERE c.Booking_Unit__c IN: bookingUnitIDSet
                                                AND c.Status != 'Closed'
                                                AND c.Status != 'Rejected'
                                                AND c.Status != 'Cancelled'
                                                AND c.RecordType.DeveloperName IN : setNotAllowedSRTypes
                                                ]);
    if(mapId_Case != null && !mapId_Case.isEmpty())
    {
        lstExistingCase.addAll(mapId_Case.values());
    }
    mapExistingCase.put('cases',lstExistingCase);

    for(SR_Booking_Unit__c objSBU : [SELECT s.Id
                                          , s.Case__c
                                          , s.Case__r.Status
                                          ,s.Unit__c
                                          , s.Case__r.CaseNumber
                                          , s.Case__r.RecordType.DeveloperName
                                          , s.Case__r.RecordType.Name
                                          , s.Booking_Unit__c 
                                          , s.Booking_Unit__r.Name 
                                     FROM SR_Booking_Unit__c s
                                     WHERE s.Booking_Unit__c IN: bookingUnitIDSet
                                     AND s.Case__r.Status != 'Closed'
                                     AND s.Case__r.Status != 'Rejected'
                                     AND s.Case__r.Status != 'Cancelled'
                                     AND s.Case__r.RecordType.DeveloperName =: strDeveloperName])
    { 
        system.debug('objSBU '+objSBU.Booking_Unit__r.Name );
        if(!mapId_Case.containsKey(objSBU.Case__c))
        {
            lstSRBookingUnits.add(objSBU);
        }
    }
    mapExistingCase.put('sr',lstSRBookingUnits);

    return mapExistingCase;
  }

  public static void errorLogger(string strErrorMessage, string strCaseID,string strBookingUnitID)
  {
    Error_Log__c objError = new Error_Log__c();
    objError.Error_Details__c = strErrorMessage;
    objError.Process_Name__c = 'AOPT';
    if(String.isNotBlank(strCaseID) && strCaseID.startsWith('500')){
        objError.Case__c = strCaseID;
    }
    if(String.isNotBlank(strBookingUnitID)){
        objError.Booking_unit__c = strBookingUnitID;
    }
    insert objError;
  }

  //method to check if date is in IPMS format or SF Format
  public static Boolean checkDateFormate(String strDate)
  {
      Boolean blbFlag = true;
      if( strDate.containsIgnoreCase('jan') || strDate.containsIgnoreCase('feb') || strDate.containsIgnoreCase('mar') 
          || strDate.containsIgnoreCase('apr') || strDate.containsIgnoreCase('may') || strDate.containsIgnoreCase('jun')
          || strDate.containsIgnoreCase('jul') || strDate.containsIgnoreCase('aug') || strDate.containsIgnoreCase('sep')
          || strDate.containsIgnoreCase('oct') || strDate.containsIgnoreCase('nov') || strDate.containsIgnoreCase('dec')
        )
      {
        blbFlag = false;
      }
      else
      {
          blbFlag = true;
      }
      system.debug('blbFlag '+blbFlag);
      return blbFlag;
  }

  
}