@isTest
public class PromotionSetupControllerTest {
    @testSetup
    public static void InsertData(){ 
        List<Inventory_Filter__c> filterList = new List<Inventory_Filter__c>(); 
        Inventory_Filter__c filter = new Inventory_Filter__c();
        filter.Name = 'City';
        filter.Filter_Values__c = 'DUBAI;RIYADH;ABU DHABI;LONDON;QATAR;JORDAN;RIAD EL SOLH';
        filterList.add(filter);
        Inventory_Filter__c filter2 = new Inventory_Filter__c();
        filter2.Name = 'City';
        filter2.Filter_Values__c = 'DUBAI;RIYADH;ABU DHABI;LONDON;QATAR;JORDAN;RIAD EL SOLH';
        filterList.add(filter2);
        insert filterList;
    }
    
   @isTest
    public static void TestMethod1(){
        Test.startTest();
        PageReference pageRef = Page.PromotionSetup;
        Test.setCurrentPage(pageRef); 
        Promotion_Setup__c  promo = new Promotion_Setup__c ();  
        ApexPages.StandardController std = new ApexPages.StandardController(promo);     
        PromotionSetupController controller = new PromotionSetupController(std);
        promo.Type__c = 'Free Apartment';
        promo.Sub_Type__c = 'Free Apartment';
        promo.Start_Date__c = system.today();
        promo.End_Date__c = system.today();
        promo.Promotion_Type__c = 'Option';
        promo.Promotion_Name__c = 'Test Promo #1';
        insert promo;
        Promotion_Eligibility_Criteria__c criteria = new Promotion_Eligibility_Criteria__c();
        criteria.Condition__c = '=';
        criteria.Object__c = 'Inventory__c';
        criteria.Order__c = 1;
        criteria.Parameter__c = 'Property_City__c';
        criteria.Promotion_Setup__c = promo.Id;
        criteria.Value__c = 'DUBAI';
        criteria.Value_Type__c = 'Text';
        insert criteria;
        Promotion_Execution_Criteria__c criteria2 = new Promotion_Execution_Criteria__c();
        criteria2.Condition__c = '=';
        criteria2.Object__c = 'Inventory__c';
        criteria2.Order__c = 1;
        criteria2.Parameter__c = 'Property_City__c';
        criteria2.Promotion_Setup__c = promo.Id;
        criteria2.Value__c = 'DUBAI';
        criteria2.Value_Type__c = 'Text';
        insert criteria2;
        ApexPages.currentPage().getParameters().put('selPromoId',promo.Id);
        controller.selectPromo();
        controller.addNewEligCriteria();
        controller.addNewExecCriteria();
        controller.updateEligCriteria();
        controller.updateExecCriteria();
        controller.updateStatusToReady();
        ApexPages.currentPage().getParameters().put('objAPI', 'Inventory__c');
        ApexPages.currentPage().getParameters().put('order2', '1');
        controller.updateRelatedObj();
        ApexPages.currentPage().getParameters().put('objAPIExec', 'Inventory__c');
        ApexPages.currentPage().getParameters().put('order2Exec', '1');
        controller.updateRelatedObjExec();
        ApexPages.currentPage().getParameters().put('fieldAPI', 'Property_City__c');
        ApexPages.currentPage().getParameters().put('order', '1');
        controller.getValues();
        ApexPages.currentPage().getParameters().put('fieldAPIExec', 'Property_City__c');
        ApexPages.currentPage().getParameters().put('orderExec', '1');
        controller.getValuesExec();
        ApexPages.currentPage().getParameters().put('templateFile', 'test file');
        ApexPages.currentPage().getParameters().put('templateFileName', 'test file name');
        ApexPages.currentPage().getParameters().put('brand', 'test brand');
        ApexPages.currentPage().getParameters().put('model', 'test model name');
        ApexPages.currentPage().getParameters().put('brand2', 'test brand2');
        ApexPages.currentPage().getParameters().put('value', 'test value name');
        ApexPages.currentPage().getParameters().put('carat', 'test carat');
        ApexPages.currentPage().getParameters().put('weight', 'test weight name');
        ApexPages.currentPage().getParameters().put('value2', 'test value2');
        ApexPages.currentPage().getParameters().put('waived', 'test waived name');
        ApexPages.currentPage().getParameters().put('valueCap', 'test valueCap');
        ApexPages.currentPage().getParameters().put('brand3', 'test brand3 name');
        ApexPages.currentPage().getParameters().put('brand4', 'test brand4');
        ApexPages.currentPage().getParameters().put('valueCap2', 'test valueCap2 name');
        ApexPages.currentPage().getParameters().put('waived2', 'test waived2 name');
        ApexPages.currentPage().getParameters().put('waiverMonths', 'test waiverMonths');
        ApexPages.currentPage().getParameters().put('brand5', 'test brand5 name');
        ApexPages.currentPage().getParameters().put('rentalGuarantee', 'test rentalGuarantee');
        ApexPages.currentPage().getParameters().put('grossOrNet', 'test grossOrNet name');
        ApexPages.currentPage().getParameters().put('capitalPaid', 'test capitalPaid');
        ApexPages.currentPage().getParameters().put('duration', 'test duration name');
        ApexPages.currentPage().getParameters().put('rentalGuaranteeStart', 'test rentalGuaranteeStart');
        ApexPages.currentPage().getParameters().put('payFrequency', 'Daily');
        ApexPages.currentPage().getParameters().put('payOutcome', 'test payOutcome name');
        ApexPages.currentPage().getParameters().put('serviceCharges', 'test serviceCharges');
        ApexPages.currentPage().getParameters().put('fees', 'test fees name');
        
        controller.promo = promo.clone();
        controller.insertPromotion();
        promo.type__c = 'Gift';
        promo.sub_type__c = 'Watch';
        controller.promo = promo.clone();
        controller.insertPromotion();
        promo.sub_type__c = 'Gift Card';
        controller.promo = promo.clone();
        controller.insertPromotion();
        promo.sub_type__c = 'Gold';
        controller.promo = promo.clone();
        controller.insertPromotion();
        promo.sub_type__c = 'Free Ticket';
        controller.promo = promo.clone();
        controller.insertPromotion();
        promo.type__c = 'DLD Promotion';
        promo.sub_type__c = 'DLD Promotion';
        controller.promo = promo.clone();
        controller.insertPromotion();
        promo.type__c = 'Unit Features';
        promo.sub_type__c = 'Unit Features';
        controller.promo = promo.clone();
        controller.insertPromotion();
        promo.type__c = 'Service Charge Waiver';
        promo.sub_type__c = 'Service Charge Waiver';
        controller.promo = promo.clone();
        controller.insertPromotion();
        promo.type__c = 'Rental Guarantee';
        promo.sub_type__c = 'Rental Guarantee';
        controller.promo = promo.clone();
        controller.insertPromotion();
        Test.stopTest();
        
    }

}