public without sharing class HO_KeyHandover {
    //@InvocableMethod
    public static void createCallingList(List<Calling_List__c> lstCallingList, Map<Id, Calling_List__c> mapOldCallingList) {
        System.debug('====lstCallingList==' + lstCallingList);
        List<Calling_List__c> plstCallingList = new List<Calling_List__c>();
        List<Case> lstCases = new List<Case>();
        List<Booking_Unit__c> lstBookinhUnits = new List<Booking_Unit__c>();
        Set<ID> setCaseIds = new Set<ID>();
        Set<ID> setBUIds = new Set<ID>();
        Set<Date> setHolidays = new Set<Date>();
        Id handoverRecId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Handover Calling List').getRecordTypeId();
        Id earlyHandoverRecId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Early Handover Calling List').getRecordTypeId();
        Id devRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Appointment Scheduling').getRecordTypeId();
        
        set<Id> setAccId = new set<Id>();

        list<Calling_List__c> lstCall = new list<Calling_List__c>();
        for(Calling_List__c obj : lstCallingList) {
            Calling_List__c objOldCL = mapOldCallingList.get(obj.id);
            if ((obj.RecordTypeId == handoverRecId || obj.RecordTypeId == earlyHandoverRecId) && 
                (obj.Key_Handover_Appointment_Start_Time__c!= objOldCL.Key_Handover_Appointment_Start_Time__c|| 
                obj.Key_Handover_Appointment_Date__c != objOldCL.Key_Handover_Appointment_Date__c ) 
                && obj.Key_Handover_Appointment_Date__c != null && obj.Key_Handover_Appointment_Start_Time__c != null) {
                lstCall.add(obj);
                setAccId.add(obj.Account__c);
            }
        }
        
        if (lstCall != null && lstCall.size() > 0) {
            for (Holiday objDate : [SELECT ActivityDate FROM Holiday]) {
                setHolidays.add(objDate.ActivityDate);
            }
            map<Id, String> mapAccMail = new map<Id, String>();
            for (Account objAcc : [Select Id, Email__pc, Email__c, isPersonAccount From Account Where Id =: setAccId]) {
                if (objAcc.isPersonAccount == true) {
                    mapAccMail.put(objAcc.id, objAcc.Email__pc);
                } else {
                    mapAccMail.put(objAcc.id, objAcc.Email__c);
                }
            }
            Group objGroup = [select Id,Name
                                    from Group
                                    where Type = 'Queue'
                                    AND Name = 'Handover Queue'
                                    LIMIT 1];
            for(Calling_List__c obj : lstCall) {
                
                    Datetime dt = DateTime.newInstance(obj.Key_Handover_Appointment_Date__c.year(), obj.Key_Handover_Appointment_Date__c.month(), obj.Key_Handover_Appointment_Date__c.day());
                    String dayOfWeek =  dt.format('EEEE'); 
                    system.debug('!!!!!!!dayOfWeek'+dayOfWeek);
                        if (!dayOfWeek.equals('Friday') && !dayOfWeek.equals('Saturday') && (setHolidays != null && !setHolidays.Contains(obj.Key_Handover_Appointment_Date__c))){
                            if( !setCaseIds.contains(obj.Case__c)) {//To avoid duplicates in the list
                                if (obj.Case__c != null) {
                                    lstCases.add(new Case(Id=obj.Case__c, Key_Handover_Appointment_Date__c = obj.Key_Handover_Appointment_Date__c,
                                        Key_Handover_Appointment_Start_Time__c = obj.Key_Handover_Appointment_Start_Time__c));
                                 }
                            }
                            if( !setBUIds.contains(obj.Booking_Unit__c)) {//To avoid duplicates in the list
                                if (obj.Booking_Unit__c != null) {
                                    lstBookinhUnits.add(new Booking_Unit__c(Id=obj.Booking_Unit__c, Key_Handover_Appointment_Date__c = obj.Key_Handover_Appointment_Date__c,
                                        Key_Handover_Appointment_Start_Time__c = obj.Key_Handover_Appointment_Start_Time__c));
                                 }
                            }
                            setCaseIds.add(obj.Case__c);
                            setBUIds.add(obj.Booking_Unit__c);
                            Calling_List__c objCallList = new Calling_List__c();
                            objCallList.Account__c = obj.Account__c;
                            objCallList.Booking_Unit__c = obj.Booking_Unit__c;
                            objCallList.RecordTypeId = devRecordTypeId;
                            objCallList.Calling_List__c = obj.ID;
                            if (obj.RecordTypeId == handoverRecId) {
                                objCallList.Service_Type__c = 'Handover';
                            } else if (obj.RecordTypeId == earlyHandoverRecId) {
                                objCallList.Service_Type__c = 'Early_Handover';
                            }
                            objCallList.Sub_Purpose__c='Key Handover';
                            objCallList.Appointment_Date__c = obj.Key_Handover_Appointment_Date__c;
                            objCallList.Appointment_Slot__c = obj.Key_Handover_Appointment_Start_Time__c;
                            objCallList.Registration_ID__c = obj.Booking_Unit__r.Registration_ID__c;
                            objCallList.Account_Email__c = mapAccMail.get(obj.Account__c);
                            objCallList.ownerId = objGroup.Id;
                            objCallList.Appointment_Status__c = 'Requested';
                            plstCallingList.add(objCallList);
                            /*plstCallingList.add(new Calling_List__c(Account__c = obj.Account__c, Booking_Unit__c = obj.Booking_Unit__c,RecordTypeId = devRecordTypeId,
                                            Calling_List__c = obj.ID,
                                            Service_Type__c = 'Handover',
                                            Sub_Purpose__c='    Key Handover',
                                            Appointment_Date__c = obj.Key_Handover_Appointment_Date__c,
                                            Appointment_Slot__c = obj.Key_Handover_Appointment_Start_Time__c,
                                            Registration_ID__c = obj.Booking_Unit__r.Registration_ID__c,
                                            Account_Email__c = mapAccMail.get(obj.Account__c),
                                            ownerId = objGroup.Id,
                                            Appointment_Status__c = 'Requested'));*/
                    }
                    else {
                            system.debug('!!!!!!!!!inside else');
                            obj.addError('Appointments can not be taken on Weekends and Holidays');
                   }
                
            }//for
            if (plstCallingList != null && plstCallingList.size()>0) {
                insert plstCallingList;
            }        
            if (lstBookinhUnits != null && lstBookinhUnits.size()>0) {
                update lstBookinhUnits;
            }
            if (lstCases != null && lstCases.size() >0) {
                update lstCases;
            }
         }
          if (lstCases != null && lstCases.size() > 0) {
            list<Task> lstTask = new list<Task>();
            
            for (Task objTask : [Select Id, WhatId, Subject, Process_Name__c, Status
                                 From Task 
                                 Where WhatId IN: lstCases and Process_Name__c = 'Early Handover'
                                 and Subject = 'Schedule Handover date' and Status != 'Completed']){
                  objTask.Status = 'Completed';
                  lstTask.add(objTask);
              }
              
              if (lstTask != null && lstTask.size() > 0) {
                  update lstTask;
              }       
         }
         if (plstCallingList != null && plstCallingList.size() > 0) {
            Map<String, String> activeProcessHours = new Map<String, String>();
            for (Active_Process_on_Portal_Meta__mdt objProcess : [Select Id, Appointment_Hr__c,DeveloperName, Label,MasterLabel,NamespacePrefix
                                                                    From Active_Process_on_Portal_Meta__mdt ]){
                activeProcessHours.put(objProcess.DeveloperName, objProcess.Appointment_Hr__c);                                                    
            }
            list<Event> lstEvent = new list<Event>();
            list<Calling_List__c> lstCalling = new list<Calling_List__c>();
            set<Id> setOwnerId = new set<Id>();
            for (Calling_List__c objCalling : [Select id,name,OwnerID, Account__c,Appointment_Date__c,Account_Email__c,
                                                Account__r.Name,Account__r.Email__c,Account__r.Email__pc,
                                                Appointment_Slot__c,Account_Name_for_Walk_In__c,
                                                RecordType.DeveloperName, Assigned_CRE__c,Service_Type__c,
                                                Assigned_CRE__r.id from Calling_List__c where id IN: plstCallingList]) {
                lstCalling.add(objCalling);
                setOwnerId.add(objCalling.OwnerID);
            }
            map<Id, list<GroupMember>> mapOwnerIdGroupMember = new map<Id, list<GroupMember>>();
            for (GroupMember objGM : [Select id,GroupId, Group.name, UserOrGroupId from GroupMember where GroupId =: setOwnerId]) {
                if (mapOwnerIdGroupMember.containsKey(objGM.GroupId)) {
                    list<GroupMember> lstGM = mapOwnerIdGroupMember.get(objGM.GroupId);
                    lstGM.add(objGM);
                    mapOwnerIdGroupMember.put(objGM.GroupId, lstGM);
                } else {
                    mapOwnerIdGroupMember.put(objGM.GroupId, new list<GroupMember> {objGM});
                }
            }
            for (Calling_List__c objCalling : lstCalling) {
                String startTimes = objCalling.Appointment_Slot__c.substring(0,2);
                String AddHours, AddMins ;
                if (objCalling.Service_Type__c == 'Handover') {
                    AddHours = activeProcessHours.get('Handover').substring(0,2);
                    AddMins = activeProcessHours.get('Handover').substring(3,5);
                } else if (objCalling.Service_Type__c == 'Early_Handover'){
                    AddHours = activeProcessHours.get('Early_Handover').substring(0,2);
                    AddMins = activeProcessHours.get('Early_Handover').substring(3,5);
                }
                integer intSlots = (integer.Valueof(startTimes) + integer.Valueof(AddHours));
                
                Datetime StartDT = datetime.newInstance(objCalling.Appointment_Date__c.year(), objCalling.Appointment_Date__c.month(),objCalling.Appointment_Date__c.day(),
                integer.Valueof(startTimes), 00, 00);
                Datetime EndDT = datetime.newInstance(objCalling.Appointment_Date__c.year(), objCalling.Appointment_Date__c.month(),objCalling.Appointment_Date__c.day(),
                intSlots, (integer.Valueof(AddMins)), 00);

                System.debug('----StartDT -------'+StartDT );
                System.debug('----EndDT -------'+EndDT );
                
                Event objEvent = new Event();
                List<GroupMember> lstGroupMember = new List<GroupMember>();
                if (mapOwnerIdGroupMember != null && mapOwnerIdGroupMember.containsKey(objCalling.ownerId)) {
                    lstGroupMember = mapOwnerIdGroupMember.get(objCalling.ownerId);
                }
                if(lstGroupMember.Size()>0){
                    objEvent.OwnerId = lstGroupMember[0].UserOrGroupId ;
                }
                else if(objCalling.OwnerID != null && String.ValueOf(objCalling.OwnerID).startsWithIgnoreCase('005')){ //User as Owner of Calling list(primary,sec,ter)
                    objEvent.OwnerId = objCalling.OwnerID;
                }
                System.debug('----objEvent.OwnerId-------'+objEvent.OwnerId);
                objEvent.Type = 'Meeting';
                objEvent.WhatId = objCalling.id;
                objEvent.StartDateTime = StartDT ;
                objEvent.EndDateTime =  EndDT ;
                objEvent.Subject = 'Appointment Scheduled with Customer:'+objCalling.Account__r.Name;
                objEvent.Description= 'Appointment Scheduled with Customer:'+objCalling.Account__r.Name;
                objEvent.status__c = 'Requested';
                objEvent.IsVisibleInSelfService = true;
                lstEvent.add(objEvent);
            }
            insert lstEvent;
        }
    }
}