/**************************************************************************************************
* Name               : MergePartyIdsUpdate                                                        *
* Description        : To Update master party id to Buyer from Merge Parties Console  *
* Created Date       : 12/02/2018                                                                 *
* Created By         : Srikanth                                                                   *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR            DATE            COMMENTS                                          *
* 1.0         Srikanth         26/01/2018      Initial Draft.                                     *
**************************************************************************************************/
global without sharing class MergePartyIdsUpdate implements Queueable {

    ID masterPartyID;
    public MergePartyIdsUpdate (ID masterAccountId) {
        masterPartyID = masterAccountId; 
        
    }
        
    public void execute (QueueableContext context) {

        if (masterPartyID != NULL) {
            Account masterAccount = NEW Account ();
            masterAccount = [ SELECT Party_ID__c from Account where id =: masterPartyID ];
            List <Buyer__c> buyersToUpdate = new List <Buyer__c> ();
            for (Buyer__c buyer :[ SELECT Account__c, Party_ID__c FROM Buyer__c WHERE ACCOUNT__C =: masterPartyID]) {
                buyer.Account__c = masterAccount.id;
                buyer.Party_ID__c = masterAccount.Party_ID__c ;
                buyersToUpdate.add (buyer);
            }
            Update buyersToUpdate;           
            
        }
    }
    
}