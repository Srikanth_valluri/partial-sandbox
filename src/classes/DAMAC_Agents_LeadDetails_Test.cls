/************************************************************************************************
 * @Name              : DAMAC_Agents_LeadDetails_Test
 * @Description       : Test Class for DAMAC_Agents_LeadDetails
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         01/06/2020       Created
***********************************************************************************************/

@isTest
public class DAMAC_Agents_LeadDetails_Test{
	@isTest
    static void testAPI(){
		Id superUserProfileId = [select Id from Profile where name = 'Customer Community - Super User'].Id;
        Id UserRoleId = [select Id from UserRole where name = 'Chairman'].Id;
        Id RecordTyId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        
                
        User thisUser = [SELECT Id FROM User WHERE Id = : userInfo.getUserId() LIMIT 1];
        thisUser.UserRoleId = UserRoleId;
        update thisUser;          
        
   system.runAs(thisUser){       
       	Account superUserAccount = new Account(Name = 'Test Super User',  RecordTypeId = RecordTyId);
        insert superUserAccount;
        Contact superUserCon = new Contact();
        superUserCon.LastName = 'testSuperUser';
        superUserCon.AccountId = superUserAccount.Id;
        superUserCon.Email = 'superuser@test.com';
        superUserCon.Portal_Administrator__c = true;
        superUserCon.Owner__c = true;
        //superUserCon.User_Type__c =	'Active High Volume Portal';
        insert superUserCon;
        Id AccId = [select Id from Account Limit 1].Id;
        Contact ContId = [select Id from Contact where AccountId =: AccId limit 1];
        User portalUser = new User(alias = 'test457', email= 'superuser@test.com',
                                       emailencodingkey='UTF-8', lastname='User 457', languagelocalekey='en_US',
                                       localesidkey='en_US', profileid = superUserProfileId, country='United Arab Emirates',
                                       IsActive =true, ContactId = ContId.Id, timezonesidkey='America/Los_Angeles', 
                                       username = 'superuser@test.com');
       
        insert portalUser;
       
       Account acc = InitialiseTestData.getCorporateAccount('TestAgency');
        Id RecordTyIds = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        acc.RecordTypeId = RecordTyIds;
        acc.Inquiry_Start_Date__c= datetime.newInstance(2018, 9, 15, 12, 30, 0);
        acc.Inquiry_End_Date__c = datetime.newInstance(2020, 9, 15, 12, 30, 0);
        acc.OwnerId = thisUser.Id;
        insert acc ;
       
		
        Contact con = new Contact();
        con.LastName = 'Corporate Agency test';
        con.AccountId = acc.Id;
        con.Inquiry_Start_Date__c = date.today();
        con.Inquiry_End_Date__c = date.today();
        con.Inquiry_Start_Date__c= datetime.newInstance(2018, 9, 15, 12, 30, 0);
        con.Inquiry_End_Date__c = datetime.newInstance(2020, 9, 15, 12, 30, 0);
      
        insert con ;
       	
       	Campaign__c	camp = new Campaign__c();
       	camp.Campaign_Name__c='testCampaign';
       	camp.Start_Date__c=date.newInstance(2019, 9, 15);
       	camp.End_Date__c=date.newInstance(2020, 9, 15);        
        camp.Marketing_Start_Date__c=date.newInstance(2019, 9, 15);
       	camp.Marketing_End_Date__c =date.newInstance(2020, 9, 15);
        
       	insert camp;
        
        Inquiry__c inq = new Inquiry__c();
        inq.First_Name__c = 'Jithesh';
        inq.Last_Name__c = 'Vasudevan';
        inq.Title__c = 'MR.';      
        inq.Preferred_Language__c= 'English';
        inq.Inquiry_Source__c= 'Agent Referral';
        inq.Mobile_Phone__c = '00917829472432';
        inq.Email__c = 'jithesh@qburst.com';
        inq.Buyer_Type__c = '';
        inq.Nationality__c =  'Indian';
        inq.Agency_Name__c = superUserAccount.Id;
        inq.Agent_Name__c= superUserCon.Id;
       	inq.Inquiry_Status__c='Active';
        inq.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('CIL').getRecordTypeId();
       	inq.Type_of_Property_Interested_in__c='Residential';
       	inq.Budget__c='Between 1 Mn to 1.5 Mn';
       	inq.Financing__c='Cash Buyer-Self';
       	inq.Campaign__c=camp.Id;
       	inq.Passport_Expiry_Date__c=date.newInstance(2030,02,02);
       	inq.Inquiry_Source__c='Stands';
       	inq.Passport_Place_of_Issue__c='Thrissur';
       	inq.Passport_Number__c='AVz54625e';
        insert inq;
       List<Inquiry__c>	inqList	=	new	List<Inquiry__c>();
       inqList.add(inq);
       Task t = new Task();
       t.Subject='TestTask'; 
       t.Activity_Type_3__c='Call-Inbound';
       t.Activity_Outcome__c='Meeting Scheduled';
       t.whatId=inq.Id;
       insert t;
       
       system.runAs(portalUser){   
        String startDate = String.valueOf(Datetime.newInstance(2008, 12, 1, 12, 30, 2));
        String endDate = String.valueOf(Datetime.newInstance(2020, 12, 1, 12, 30, 2));
        Test.startTest();
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.params.put ('agentId', superUserCon.Id);
        request.params.put ('agencyId', superUserAccount.Id);
        request.params.put ('startDate', startDate);
        request.params.put ('endDate', endDate);
        request.params.put ('status', 'Active');
        request.params.put ('isSuperUser', 'true');
        request.requestUri ='/inquiries';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Agents_LeadDetails.doGet();
        
        request.params.put ('agentId', superUserCon.Id);
        request.params.put ('agencyId', superUserAccount.Id);
        request.params.put ('startDate', startDate);
        request.params.put ('endDate', endDate);
        request.params.put ('status', '');
        request.params.put ('isSuperUser', 'true');
        request.requestUri ='/inquiries';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Agents_LeadDetails.doGet();
           
        request.params.put ('agentId', '');
        request.params.put ('agencyId', '');
        request.params.put ('startDate', null);
        request.params.put ('endDate', null);
        request.params.put ('status', '');
        request.params.put ('isSuperUser', 'true');
        request.requestUri ='/inquiries';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Agents_LeadDetails.doGet();
        
        request.requestUri ='/inquiries';
        request.requestBody = Blob.valueOf('{ "inq": { "title__c": "MR.", "First_Name__c": "Johny", "Last_Name__c": "Dwayne", "Email__c": "jithesh@qburst.com", "Primary_Contacts__c": "Mobile Phone", "Mobile_CountryCode__c": "India: 0091", "Mobile_Phone_Encrypt__c": "7829472433", "Passport_Number__c": "L1007279", "Passport_Expiry_Date__c": "2020-12-31", "Passport_Place_of_Issue__c": "COCHIN", "Preferred_Language__c": "English", "Budget__c": "Above 10M", "Time_Frame__c": "Immediate", "Type_of_Property_Interested_in__c": "Hotel", "Financing__c": "Mortgage Buyer", "Inquiry_Source__c": "Agent Referral", "Agency_Name__c": "0010Y00001dECmd", "Agent_Name__c": "0031n00001qZrjX", "Campaign__c": "a121n000001n845" }, "tas": [{ "Subject": "Test Task", "Status": "completed", "Priority": "Normal", "Activity_Type_3__c": "Call-Inbound", "Activity_Outcome__c": "Meeting Scheduled", "ActivityDate": "2020-06-23" }] }');
        request.httpMethod = 'POST';
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Agents_LeadDetails.doPost();  
       	DAMAC_Agents_LeadDetails.RequestWrapper	reqWrapper	=	new	DAMAC_Agents_LeadDetails.RequestWrapper();
        DAMAC_Agents_LeadDetails.ResponseWrapper	resWrapper	=	new	DAMAC_Agents_LeadDetails.ResponseWrapper();
           DAMAC_Agents_LeadDetails.doPost();
    }       
        Test.stopTest();
   }
        
}
    @isTest
    static void testAPI2(){
		Id superUserProfileId = [select Id from Profile where name = 'Customer Community - Agent'].Id;
        Id UserRoleId = [select Id from UserRole where name = 'Chairman'].Id;
        Id RecordTyId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        
                
        User thisUser = [SELECT Id FROM User WHERE Id = : userInfo.getUserId() LIMIT 1];
        thisUser.UserRoleId = UserRoleId;
        update thisUser;          
        
   system.runAs(thisUser){       
       	Account superUserAccount = new Account(Name = 'Test Super User',  RecordTypeId = RecordTyId);
        insert superUserAccount;
        Contact superUserCon = new Contact();
        superUserCon.LastName = 'testSuperUser';
        superUserCon.AccountId = superUserAccount.Id;
        superUserCon.Email = 'superuser@test.com';
        superUserCon.Portal_Administrator__c = true;
        superUserCon.Owner__c = true;
        //superUserCon.User_Type__c =	'Active High Volume Portal';
        insert superUserCon;
        Id AccId = [select Id from Account Limit 1].Id;
        Contact ContId = [select Id from Contact where AccountId =: AccId limit 1];
        User portalUser = new User(alias = 'test457', email= 'superuser@test.com',
                                       emailencodingkey='UTF-8', lastname='User 457', languagelocalekey='en_US',
                                       localesidkey='en_US', profileid = superUserProfileId, country='United Arab Emirates',
                                       IsActive =true, ContactId = ContId.Id, timezonesidkey='America/Los_Angeles', 
                                       username = 'superuser@test.com');
       
        insert portalUser;
       
       Account acc = InitialiseTestData.getCorporateAccount('TestAgency');
        Id RecordTyIds = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        acc.RecordTypeId = RecordTyIds;
        acc.Inquiry_Start_Date__c= datetime.newInstance(2018, 9, 15, 12, 30, 0);
        acc.Inquiry_End_Date__c = datetime.newInstance(2020, 9, 15, 12, 30, 0);
        acc.OwnerId = thisUser.Id;
        insert acc ;
       
		
        Contact con = new Contact();
        con.LastName = 'Corporate Agency test';
        con.AccountId = acc.Id;
        con.Inquiry_Start_Date__c = date.today();
        con.Inquiry_End_Date__c = date.today();
        con.Inquiry_Start_Date__c= datetime.newInstance(2018, 9, 15, 12, 30, 0);
        con.Inquiry_End_Date__c = datetime.newInstance(2020, 9, 15, 12, 30, 0);
      
        insert con ;
       	
       	Campaign__c	camp = new Campaign__c();
       	camp.Campaign_Name__c='testCampaign';
       	camp.Start_Date__c=date.newInstance(2019, 9, 15);
       	camp.End_Date__c=date.newInstance(2020, 9, 15);        
        camp.Marketing_Start_Date__c=date.newInstance(2019, 9, 15);
       	camp.Marketing_End_Date__c =date.newInstance(2020, 9, 15);
       	insert camp;
        
        Inquiry__c inq = new Inquiry__c();
        inq.First_Name__c = 'Jithesh';
        inq.Last_Name__c = 'Vasudevan';
        inq.Title__c = 'MR.';      
        inq.Preferred_Language__c= 'English';
        inq.Inquiry_Source__c= 'Agent Referral';
        inq.Mobile_Phone__c = '00917829472432';
        inq.Email__c = 'jithesh@qburst.com';
        inq.Buyer_Type__c = '';
        inq.Nationality__c =  'Indian';
        inq.Agency_Name__c = superUserAccount.Id;
        inq.Agent_Name__c= superUserCon.Id;
       	inq.Inquiry_Status__c='Active';
        inq.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('CIL').getRecordTypeId();
       	inq.Type_of_Property_Interested_in__c='Residential';
       	inq.Budget__c='Between 1 Mn to 1.5 Mn';
       	inq.Financing__c='Cash Buyer-Self';
       	inq.Campaign__c=camp.Id;
       	inq.Passport_Expiry_Date__c=date.newInstance(2030,02,02);
       	inq.Inquiry_Source__c='Stands';
       	inq.Passport_Place_of_Issue__c='Thrissur';
       	inq.Passport_Number__c='AVz54625e';
        insert inq;
       List<Inquiry__c>	inqList	=	new	List<Inquiry__c>();
       inqList.add(inq);
       Task t = new Task();
       t.Subject='TestTask'; 
       t.Activity_Type_3__c='Call-Inbound';
       t.Activity_Outcome__c='Meeting Scheduled';
       t.whatId=inq.Id;
       insert t;
       
       system.runAs(portalUser){   
        String startDate = String.valueOf(Datetime.newInstance(2008, 12, 1, 12, 30, 2));
        String endDate = String.valueOf(Datetime.newInstance(2020, 12, 1, 12, 30, 2));
        Test.startTest();
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.params.put ('agentId', superUserCon.Id);
        request.params.put ('agencyId', superUserAccount.Id);
        request.params.put ('startDate', startDate);
        request.params.put ('endDate', endDate);
        request.params.put ('status', 'Active');
        request.params.put ('isSuperUser', 'false');
        request.requestUri ='/inquiries';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Agents_LeadDetails.doGet();
           
        request.params.put ('agentId', Con.Id);
        request.params.put ('agencyId', superUserAccount.Id);
        request.params.put ('startDate', startDate);
        request.params.put ('endDate', endDate);
        request.params.put ('status', 'Active');
        request.params.put ('isSuperUser', 'false');
        request.requestUri ='/inquiries';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Agents_LeadDetails.doGet();   
           
        request.params.put ('agentId', superUserCon.Id);
        request.params.put ('agencyId', superUserAccount.Id);
        request.params.put ('startDate', startDate);
        request.params.put ('endDate', endDate);
        request.params.put ('status', '');
        request.params.put ('isSuperUser', 'false');
        request.requestUri ='/inquiries';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Agents_LeadDetails.doGet();   
    }      
        Test.stopTest();
   }
}
    
    @isTest
    static void testAPI3(){
		Id superUserProfileId = [select Id from Profile where name = 'Customer Community - Agent'].Id;
        Id UserRoleId = [select Id from UserRole where name = 'Chairman'].Id;
        Id RecordTyId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        
                
        User thisUser = [SELECT Id FROM User WHERE Id = : userInfo.getUserId() LIMIT 1];
        thisUser.UserRoleId = UserRoleId;
        update thisUser;          
        
   system.runAs(thisUser){       
       	Account superUserAccount = new Account(Name = 'Test Super User',  RecordTypeId = RecordTyId);
        insert superUserAccount;
        Contact superUserCon = new Contact();
        superUserCon.LastName = 'testSuperUser';
        superUserCon.AccountId = superUserAccount.Id;
        superUserCon.Email = 'superuser@test.com';
        superUserCon.Portal_Administrator__c = true;
        superUserCon.Owner__c = true;
        //superUserCon.User_Type__c =	'Active High Volume Portal';
        insert superUserCon;
        Id AccId = [select Id from Account Limit 1].Id;
        Contact ContId = [select Id from Contact where AccountId =: AccId limit 1];
        User portalUser = new User(alias = 'test457', email= 'superuser@test.com',
                                       emailencodingkey='UTF-8', lastname='User 457', languagelocalekey='en_US',
                                       localesidkey='en_US', profileid = superUserProfileId, country='United Arab Emirates',
                                       IsActive =true, ContactId = ContId.Id, timezonesidkey='America/Los_Angeles', 
                                       username = 'superuser@test.com');
       
        insert portalUser;
       
       Account acc = InitialiseTestData.getCorporateAccount('TestAgency');
        Id RecordTyIds = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        acc.RecordTypeId = RecordTyIds;
        acc.Inquiry_Start_Date__c= datetime.newInstance(2018, 9, 15, 12, 30, 0);
        acc.Inquiry_End_Date__c = datetime.newInstance(2020, 9, 15, 12, 30, 0);
        acc.OwnerId = thisUser.Id;
        insert acc ;
       
		
        Contact con = new Contact();
        con.LastName = 'Corporate Agency test';
        con.AccountId = acc.Id;
        con.Inquiry_Start_Date__c = date.today();
        con.Inquiry_End_Date__c = date.today();
        con.Inquiry_Start_Date__c= datetime.newInstance(2018, 9, 15, 12, 30, 0);
        con.Inquiry_End_Date__c = datetime.newInstance(2020, 9, 15, 12, 30, 0);
      
        insert con ;
        
        Inquiry__c inq = new Inquiry__c();
        inq.First_Name__c = 'Jithesh';
        inq.Last_Name__c = 'Vasudevan';
        inq.Title__c = null;      
        inq.Preferred_Language__c= 'English';
        inq.Inquiry_Source__c= 'Agent Referral';
        inq.Mobile_Phone__c = null;
        inq.Email__c = null;
        inq.Buyer_Type__c = null;
        inq.Nationality__c =  null;
        inq.Agency_Name__c = superUserAccount.Id;
        inq.Agent_Name__c= superUserCon.Id;
       	inq.Inquiry_Status__c=null;
        inq.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('CIL').getRecordTypeId();
        inq.Type_of_Property_Interested_in__c=null;
       	inq.Budget__c=null;
       	inq.Financing__c=null;
       	inq.Campaign__c=null;
       	inq.Passport_Expiry_Date__c=null;
       	inq.Inquiry_Source__c=null;
       	inq.Passport_Place_of_Issue__c=null;
       	inq.Passport_Number__c=null;
        insert inq;
       	List<Inquiry__c>	inqList	=	new	List<Inquiry__c>();
       	inqList.add(inq);
       	Task t = new Task();
      /* 	t.Subject='TestTask'; 
       	t.Activity_Type_3__c='Call-Inbound';
       	t.Activity_Outcome__c='Meeting Scheduled';
       	t.whatId=inq.Id;
       	insert t;*/
       
       system.runAs(portalUser){   
        String startDate = String.valueOf(Datetime.newInstance(2008, 12, 1, 12, 30, 2));
        String endDate = String.valueOf(Datetime.newInstance(2020, 12, 1, 12, 30, 2));
        Test.startTest();
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.params.put ('agentId', superUserCon.Id);
        request.params.put ('agencyId', superUserAccount.Id);
        request.params.put ('startDate', startDate);
        request.params.put ('endDate', endDate);
        request.params.put ('status', 'Active');
        request.params.put ('isSuperUser', 'false');
        request.requestUri ='/inquiries';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Agents_LeadDetails.doGet();
           
        request.params.put ('agentId', Con.Id);
        request.params.put ('agencyId', superUserAccount.Id);
        request.params.put ('startDate', startDate);
        request.params.put ('endDate', endDate);
        request.params.put ('status', 'Active');
        request.params.put ('isSuperUser', 'false');
        request.requestUri ='/inquiries';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Agents_LeadDetails.doGet();   
           
        request.params.put ('agentId', superUserCon.Id);
        request.params.put ('agencyId', superUserAccount.Id);
        request.params.put ('startDate', startDate);
        request.params.put ('endDate', endDate);
        request.params.put ('status', '');
        request.params.put ('isSuperUser', 'false');
        request.requestUri ='/inquiries';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_Agents_LeadDetails.doGet();   
    }      
        Test.stopTest();
   }
}
 
}