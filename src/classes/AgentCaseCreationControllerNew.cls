/**************************************************************************************************
* Name               : AgentCaseCreationControllerNew
* Description        : An apex page controller for                                              
* Created Date       : Swapnil Gholap                                                                       
* Created By         : 12/09/2017                                                                 
* Last Modified Date :                                                                            
* Last Modified By   :                                                                            
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR                        DATE                                                                    
* 1.0         Swapnil Gholap            12/09/2017                                                                    
* 1.1         Craig Lobo                11/11/2017                                                                    
**************************************************************************************************/
public without sharing class AgentCaseCreationControllerNew{
    
    /**************************************************************************************************
            Variables used in the class
    **************************************************************************************************/
    public List<Case__c> caseList{set;get;}
    public string CasePrefix{set;get;}
    public String tabName{set;get;}
    public String showError{get; set;}
    public String errorDisplay{get;set;}
    public Case__c newCase {get;set;}
    public List<SelectOption> caseSubjectList {get; set;}
    public Boolean agentFieldHide{get;set;}
    private Set<String> subjectValues;
    public String agentName{get;set;}
    public string fieldSet {get;set;}
    private Map<String, Schema.SObjectType> GlobalDescribeMap;   
    private transient   Schema.FieldSet fieldSetObj ;  
    public List<Schema.FieldSetMember> lstField {get;set;}
    private transient  Schema.DescribeSObjectResult DescribeSObjectResultObj;
    public boolean blnShowAdditionalData {get;set;}
    public Set<Id> AgentCaseId ;
    public Integer numberOfNights {get;set;}
    public Account accountDetails;
    public Boolean isTierBenefits;
    
    public String caseSubject {get;set;}
    public List<SelectOption> caseSubjectOptionsList {get; set;}


    /**************************************************************************************************
        Method:         DamacCaseController
        Description:    Constructor executing model of the class 
    **************************************************************************************************/
    public AgentCaseCreationControllerNew() {
        isTierBenefits = false;
        numberOfNights = 0;
        caseSubject = '';
        caseList = new List<Case__c>();
        newCase = new Case__c(); 
        AgentCaseId =  new Set<Id>();
        accountDetails = new Account();
        showError='';
        errorDisplay='none';
        //fieldSet = '$ObjectType.Case__c.FieldSets.SPA';
        //Schema.FieldSet fieldSetObj = SObjectType.Case__c.FieldSets.getMap().get('SPA');
        blnShowAdditionalData = false;
        if(ApexPages.currentPage().getParameters().containsKey('sfdc.tabName')){
            tabName = ApexPages.currentPage().getParameters().get('sfdc.tabName');
        }
        GlobalDescribeMap = new Map<String, Schema.SObjectType>();
        
        //System.debug('--DescribeSObjectResultObj --'+DescribeSObjectResultObj );
        //System.debug('--DescribeSObjectResultObj Value --'+DescribeSObjectResultObj.FieldSets.getMap().get('book_a_hotel'));
        
        //readFieldSet('Request_for_site_Viewing');        
        
        Id accountIdOfAgent = UtilityQueryManager.getAccountId();
        String firstName = (UserInfo.getFirstName() != null)?UserInfo.getFirstName():'';
        String lastName = (UserInfo.getLastName() != null)?UserInfo.getLastName():'';
        agentName = firstName+' '+lastName;
        caseSubjectList = new List<SelectOption>();
        caseSubjectOptionsList = new List<SelectOption>();
        accountDetails = [ SELECT Id, Agency_Tier__c, Agency_Tier_Q1__c,
                                  Agency_Tier_Q2__c,Agency_Tier_Q3__c,
                                  Agency_Tier_Q4__c 
                             FROM Account 
                            WHERE Id=:accountIdOfAgent
        ];
        System.debug('---accountDetails ---'+accountDetails );
        String strTier = '';
        Integer currentMonth =System.today().month();
        System.debug('.currentMonth....'+currentMonth);
        
        // Added to check Broker rtier Benefits
        if (currentMonth >= 1 && currentMonth <= 3) {
            strTier = accountDetails.Agency_Tier_Q1__c;
        } else if (currentMonth >= 4 && currentMonth <= 6) {
            strTier = accountDetails.Agency_Tier_Q2__c;
        } else if (currentMonth >= 7 && currentMonth <= 9) {
            strTier = accountDetails.Agency_Tier_Q3__c;
        } else if (currentMonth >= 10 && currentMonth <= 12) {
            strTier = accountDetails.Agency_Tier_Q4__c;
        }
        System.debug('.currentMonth....'+currentMonth);
        
        if (strTier == 'PLATINUM' || strTier == 'GOLD' ) {
            isTierBenefits = true;
        } else {
            isTierBenefits = false;
        }

        String agencyTier ; 
        agentFieldHide = false ; 
        
        if(accountDetails.Agency_Tier_Q4__c=='PLATINUM' 
        || accountDetails.Agency_Tier_Q1__c=='PLATINUM' 
        || accountDetails.Agency_Tier_Q2__c=='PLATINUM' 
        ||  accountDetails.Agency_Tier_Q3__c=='PLATINUM'
        ) {
            agencyTier = 'PLATINUM' ;
        } else if(accountDetails.Agency_Tier_Q4__c=='GOLD' 
        || accountDetails.Agency_Tier_Q1__c=='GOLD' 
        || accountDetails.Agency_Tier_Q2__c=='GOLD' 
        ||  accountDetails.Agency_Tier_Q3__c=='GOLD'
        ){
            agencyTier = 'GOLD' ;
        } else if(accountDetails.Agency_Tier_Q4__c=='SILVER' 
        || accountDetails.Agency_Tier_Q1__c=='SILVER' 
        || accountDetails.Agency_Tier_Q2__c=='SILVER' 
        ||  accountDetails.Agency_Tier_Q3__c=='SILVER'
        ) {
            agencyTier = 'SILVER' ;
            agentFieldHide = true ; 
        } else{
            agencyTier = 'N/A' ;
            agentFieldHide = true ;
        }
            
        if(agencyTier !=null && agencyTier!=''){
            System.debug('agentTier test--- '+agencyTier);
            Agency_Tier_Case_Status__c agentTier =  Agency_Tier_Case_Status__c.getValues(agencyTier);
            subjectValues = new Set<String>();
            if(agentTier!=null){
                System.debug('agentTier values--- '+agentTier);
                subjectValues =  DamacUtility.splitMutliSelect(agentTier.Case_Status__c);
            } 
           createDropdownValues();
           toggleCaseSubjects();

        } 
    }

    public void readFieldSet(String fieldSetName)
    {
        try{
            System.debug('--fieldSetName--'+fieldSetName);
            GlobalDescribeMap = Schema.getGlobalDescribe();
            Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get('Case__c');        
            DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        
            fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
            System.debug('--fieldSetObj --'+fieldSetObj);
            lstField = new List<Schema.FieldSetMember>();
            lstField = fieldSetObj.getFields(); // all fields from field set
            System.debug('lstField>>>> : '+lstField);
            blnShowAdditionalData = true;
        }
        catch(exception ex){
              System.debug('Error Message'+ex.getMessage());
              System.debug('@Line Number'+ex.getLineNumber());
            
        }
        
    }
    
    public void fieldSetValue(){ 
        System.debug('--caseSubject--'+ caseSubject);
        newCase.Case_Subject__c = caseSubject;
        System.debug('--fieldSetValue--'+newCase.Case_Subject__c);
        if(String.IsNotBlank(newCase.Case_Subject__c)){
            blnShowAdditionalData = false;
            
            if(newCase.Case_Subject__c == 'SPA'){
                readFieldSet('SPA');
            }
            else if(newCase.Case_Subject__c == 'Request for site Viewing'){
                readFieldSet('Request_for_site_Viewing');
            }
            else if(newCase.Case_Subject__c == 'Book a Hotel'){
                readFieldSet('book_a_hotel');
            }
            else if(newCase.Case_Subject__c == 'Query on Unit Commission'){
                readFieldSet('Query_on_Unit_Commission');
            }            
            else if(newCase.Case_Subject__c == 'Request for Brochures'){
                readFieldSet('Request_for_Brochures');
            }          
            else if(newCase.Case_Subject__c == 'Request for Marketing Support'){
                readFieldSet('Request_for_Marketing_Support');
            }
            else if(newCase.Case_Subject__c == 'Booking Related Queries'){
                readFieldSet('Booking_related_Queries');
            }
            else if(newCase.Case_Subject__c == 'Commission Payment'){
                readFieldSet('Commission_Payment');
            }
            else if(newCase.Case_Subject__c == 'Reinstatements'){
                readFieldSet('Reinstatements');
            }
            else if(newCase.Case_Subject__c == 'Reassignment'){
                readFieldSet('Reassignment');
            }
            else if(newCase.Case_Subject__c == 'Book a Luxury Car'){
                readFieldSet('Book_a_Luxury_Car');
            }
            
            else if(newCase.Case_Subject__c == 'Book a Meeting Room'){
                readFieldSet('Book_a_Meeting_Room');
            }
        }
    }

     private void createDropdownValues(){
        System.debug('NOWWWW ');
        subjectValues.addAll(DamacUtility.splitMutliSelect(LABEL.Support_Subject_Values));
        caseSubjectList.add(new SelectOption('', '--None--'));  
        for(String subVal : subjectValues){
            caseSubjectList.add(new SelectOption(subVal.trim(), subVal.trim())); 
        }
    }

    /* Added Method to toggel Broker Tier Benefits */
    public void toggleCaseSubjects(){
        System.debug('caseSubjectOptionsList>>>> ' + caseSubjectOptionsList);
        System.debug('Support_Category__c>>>> ' + newCase.Support_Category__c);
        List<String> picklistValueList = new List<String>(); 
        if (newCase.Support_Category__c == 'My Customer Queries') { 
            picklistValueList.addAll(DamacUtility.splitMutliSelect(LABEL.Support_Customer_Queries));
        } else if(newCase.Support_Category__c == 'My Sales Support') {
            picklistValueList.addAll(DamacUtility.splitMutliSelect(LABEL.Support_Sales_Support));
        } else if(newCase.Support_Category__c == 'My Support Needs') {
            picklistValueList.addAll(DamacUtility.splitMutliSelect(LABEL.Support_Support_Needs));
        } else {

        }
        System.debug('picklistValueList>>>> ' + picklistValueList);
        caseSubjectOptionsList = new List<SelectOption>();
        caseSubjectOptionsList.add(new SelectOption('', '--None--'));  
        for(String subVal : picklistValueList){
            System.debug('subVal>>>> ' + subVal.trim());
            if (subVal.trim() == 'Book a Luxury Car' || subVal.trim() == 'Book a Hotel') {
                if (isTierBenefits == true) {
                    caseSubjectOptionsList.add(new SelectOption(subVal.trim(), subVal.trim())); 
                } 
            } else {
                caseSubjectOptionsList.add(new SelectOption(subVal.trim(), subVal.trim())); 
            }
        }
        System.debug('caseSubjectOptionsList>>>> ' + caseSubjectOptionsList);
    }



    public PageReference saveCase(){
        System.debug('Save Case ');
		errorDisplay='none';
        if(!validateCaseFields())
            return null;
        try{
          newCase.Case_Subject__c = newCase.Case_Subject__c.trim();
          newCase.Case_Subject__c = caseSubject;
          //newCase.Subject = newCase.Case_Subject__c;
          insert newCase ;
          PageReference newPG = new PageReference('/DetailCasePage?id='+newCase.id);
          newPG.setRedirect(true);
          return newPG ;  
        }
        catch(Exception ex){
           // ApexPages.addMessages(new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage()));
        }
        return null ;
    }
    
        public boolean validateCaseFields(){

        String subVal = newCase.Case_Subject__c;
		showError='';
        System.debug('Entered Validation');

        if(String.isBlank(subVal)){
            errorDisplay='block';
            showError +='Error: Please select Support Subject. <br/>';
            system.debug(showError);
            return false;
        }
        else if(String.isBlank(newCase.Case_Description__c)){
            errorDisplay='block';
            showError +='Error: Please fill Support Description. <br/>';
            system.debug(showError);
            return false;
        }
         else if(subVal.trim().equalsIgnoreCase('Request for Marketing Support')){
            boolean retVal = true;
            retVal = validateRequestedDateTime();

            if (newCase.Date_Time__c == null) {
                errorDisplay='block';
                showError='Error: Please fill Date and Time. <br/>';  
                retVal = retVal && false;
            } else {
                retVal = validateRequestedDateTime();
            }

            return retVal;
        }
        else if(subVal.trim().equalsIgnoreCase('request for site viewing')){
             boolean retVal = true;
            retVal = validateRequestedDateTime();

            if(null == newCase.Project_Name__c){
                errorDisplay='block';
            	showError +='Error: Please fill Project Name. <br/>';
                retVal = retVal && false;
            }

            return retVal;
        }
        else if(subVal.trim().equalsIgnoreCase('book a hotel')){
            boolean retVal = true;
            System.debug('numberOfNights>>>>>' + numberOfNights);
            retVal = validateRequestedDateTime();
            if (numberOfNights !=null && numberOfNights > 0) {
                System.debug('INNN numberOfNights>>>>>');
                newCase.No_of_Nights__c = numberOfNights;
            } else {
                System.debug('ERROR numberOfNights>>>>>');
                errorDisplay = 'block';
                showError = 'Error: Please fill No. of Nights and Value should be numeric and greater than 0. <br/>';  
                return retVal = retVal && false;
                
            }

            return retVal;
        } 
        else if(subVal.trim().equalsIgnoreCase('book a luxury car')){
            boolean retVal = true;
            retVal = validateRequestedDateTime();

            if(String.isBlank(newCase.Pick_Up_Location__c)){
                errorDisplay='block';
            	showError +='Error: Please fill Pick Up Location. <br/>';
                retVal = retVal && false;
            }

            if(String.isBlank(newCase.Drop_Off_Location__c)){
                errorDisplay='block';
            	showError +='Error: Please fill Drop Off Location. <br/>';
                retVal = retVal && false;
            }

            return retVal;
        }
        else if(subVal.trim().equalsIgnoreCase('book a meeting room')){
            boolean retVal = true;
            retVal = validateRequestedDateTime();

            if(String.isBlank(newCase.Sales_Office__c)){
                errorDisplay='block';
            	showError +='Error: Please fill Sales Office. <br/>';
                retVal = retVal && false;
            }

            return retVal;

        }
         else if(subVal.trim().equalsIgnoreCase('Commission Payment')){
            boolean retVal = true;
            if(String.isBlank(newCase.Unit_Number__c)){
                errorDisplay='block';
            	showError +='Error: Please fill Unit Number. <br/>';
                retVal = retVal && false;
            }

            return retVal;

        }
        else if(subVal.equalsIgnoreCase('request for brochures')){ // subVal.trim().equalsIgnoreCase('request for training') || 
            boolean retVal = true;
            retVal = validateRequestedDateTime();

            if(String.isBlank(newCase.Project_Name__c)){
                errorDisplay='block';
            	showError +='Error: Please fill Project Name.<br/>';
                retVal = retVal && false;
            }

            return retVal;
        }
         else if(subVal.trim().equalsIgnoreCase('query on unit commission')){
            
            boolean retVal = true;

            System.debug('**Query on Unit');
           
            if(String.isBlank(newCase.Unit_Number__c)){
                errorDisplay='block';
            	showError +='Error: Please fill Unit Number.<br/>';
                retVal = retVal && false;
            }
            System.debug('**Query on Unit retVal'+retVal );

            return retVal;
        }

        return true;
    }


     public boolean validateRequestedDateTime(){

        if(null == newCase.Date_Time__c){
            errorDisplay='block';
            showError +='Error: Please fill Request Date & Time.<br/>';
            return false;
        }
        else if(newCase.Date_Time__c<=System.now()){
            errorDisplay='block';
            showError +='Error: Request Date & Time field must be a future date.<br/>';
            return false;
        }

        return true;
    }

}