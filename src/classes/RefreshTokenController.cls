public with sharing class RefreshTokenController {
	public String callingListId {get;set;}
	public String regId{get;set;}
	public String filterId;
	public List<Calling_List__c>callingList {get;set;}
   public RefreshTokenController(ApexPages.StandardController controller){
 //  	 filterId = controller.getFilterId();
    	//fetchData();
    }
    public pagereference fetchData(){
    	  callingListId = ApexPages.currentPage().getParameters().get('id');
    	  if(callingListId != null){
    	  	callingList = [SELECT Id,
                            Name,
                            Booking_Unit__c,
                            DM_Due_Amount__c,
                            Inv_Due__c,
                            Amount_Paid__c,
                            Amount_Pending__c,
                            Construction_Status__c,
                            DUE_0_30_DAYS__c,
                            DUE_30_60_DAYS__c,
                            DUE_60_90_DAYS__c,
                            DUE_90_180_DAYS__c,
                            DUE_180_360_DAYS__c,
                            DUE_MORE_THAN_360_DAYS__c,
                            NOT_DUE__c,
                            Status__c,
                            Amount_Received__c,
                            Registration_ID__c,
                            Account__c
                       FROM Calling_List__c
                       WHERE Id = :callingListId
                         LIMIT 1];
    	  }
         try{
	            if(callingList != null && !callingList.isEmpty()) {
	           	  RefreshTokenService.agingBucketResponse agingBucketInst =  RefreshTokenService.getAgingbucket(callingList[0].Registration_ID__c);
	            if(agingBucketInst != null){
		            if(agingBucketInst.ATTRIBUTE2 != 'null')
		        		callingList[0].Amount_Received__c = Decimal.valueOf(agingBucketInst.ATTRIBUTE2);
		        	if(agingBucketInst.ATTRIBUTE5 != 'null')
		        		callingList[0].DM_Due_Amount__c =  Decimal.valueOf(agingBucketInst.ATTRIBUTE5);
		        	if(agingBucketInst.ATTRIBUTE6 != 'null')
		        		callingList[0].Inv_Due__c =  Decimal.valueOf(agingBucketInst.ATTRIBUTE6);
		        	if(agingBucketInst.ATTRIBUTE13 != 'null')
		        		callingList[0].Status__c = agingBucketInst.ATTRIBUTE13;
		        	if(agingBucketInst.ATTRIBUTE14 != 'null')
		        		callingList[0].Amount_Paid__c =  Decimal.valueOf(agingBucketInst.ATTRIBUTE14);
		        	if(agingBucketInst.ATTRIBUTE15 != 'null')
		        		callingList[0].Amount_Pending__c =  Decimal.valueOf(agingBucketInst.ATTRIBUTE15);
		        	if(agingBucketInst.ATTRIBUTE16 != 'null')
		        		callingList[0].Construction_Status__c = agingBucketInst.ATTRIBUTE16;
		        	if( agingBucketInst.ATTRIBUTE17 != 'null'){
		        		callingList[0].DUE_0_30_DAYS__c =  Decimal.valueOf(agingBucketInst.ATTRIBUTE17);
		        	}
		        	
		        	if(agingBucketInst.ATTRIBUTE18 != 'null'){
		        		callingList[0].DUE_30_60_DAYS__c =  Decimal.valueOf(agingBucketInst.ATTRIBUTE18);
		        	}
		        	
		        	if(agingBucketInst.ATTRIBUTE19 != 'null'){
		        		callingList[0].DUE_60_90_DAYS__c =  Decimal.valueOf(agingBucketInst.ATTRIBUTE19);
		        	}
		        	if(agingBucketInst.ATTRIBUTE20 != 'null'){
		        		callingList[0].DUE_90_180_DAYS__c =  Decimal.valueOf(agingBucketInst.ATTRIBUTE20);
		        	}
		        	
		        	if(agingBucketInst.ATTRIBUTE21 != 'null'){
		        		callingList[0].DUE_180_360_DAYS__c =  Decimal.valueOf(agingBucketInst.ATTRIBUTE21);
		        	}
		        	
		        	if(agingBucketInst.ATTRIBUTE22 != 'null'){
		        		callingList[0].DUE_MORE_THAN_360_DAYS__c =  Decimal.valueOf(agingBucketInst.ATTRIBUTE22);
		        	}
		        	if(agingBucketInst.ATTRIBUTE23 != 'null'){
		        		callingList[0].NOT_DUE__c =  Decimal.valueOf(agingBucketInst.ATTRIBUTE23);
		        	}
		        	
		          update callingList[0];
		          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.info,'Data Successfully Updated!!'));
		          System.debug('callingList::::::::::::^^^^^^^^4:^%^%^%'+callingList[0]);
	            }
	            return new Pagereference(String.format(Label.Lightning_Calling_Detail_Page_Url, new List<String>{callingListId}));   
	          }
	      }
          catch(Exception e){
         	Error_Log__c errorInst = new Error_Log__c();
         	errorInst.Error_Details__c = 'No Data Found';
         	errorInst.Booking_Unit__c = callingList[0].Booking_Unit__c;
         	if(callingList[0].Account__c != null){
         		errorInst.Account__c = callingList[0].Account__c;
         	}
         	if(callingList[0].Id != null){
         		errorInst.Calling_List__c = callingList[0].Id;
         	}
         	errorInst.Process_Name__c = 'Calling List';
         	insert errorInst;
          	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'No Data Found'));
          	System.debug('**error*'+e.getMessage());
              return new Pagereference(String.format(Label.Lightning_Calling_Detail_Page_Url, new List<String>{callingListId}));  
    
          }
          return null;
    }
 
}