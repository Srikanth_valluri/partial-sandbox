/****************************************************************************************
 * Class : ReceiptChinaControllerTest 
 * Created By : ES
 -----------------------------------------------------------------------------------
 * Description : Test class for ReceiptChinaController
 -----------------------------------------------------------------------------------
 * Version History:
 * Version    Developer Name    Date          Detail Features
   1.0        Nikhil Pote       30/08/2018    Initial Development
 **********************************************************************************/
@isTest
private class ReceiptChinaControllerTest {

    /****************************************************************************************
    * Method : To check Receipt is getting generated correctly
    ****************************************************************************************/
    static testMethod void myUnitTest() {
        NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
        SR.Delivery_mode__c='Email';
        SR.Deal_ID__c='1001';
        SR.Token_Amount_AED__c = 20000;
        insert SR;
      
        update SR;
        List<NSIBPM__Service_Request__c > listSR = new List<NSIBPM__Service_Request__c >();
        listSR =[select Name
                 from NSIBPM__Service_Request__c 
                 where ID=: SR.ID];                
                 
        System.debug('...SR...'+listSR[0].Name);

        ApexPages.currentPage().getParameters().put('srid',listSR[0].Id);

        Booking__c objB = new Booking__c();
        objB.Deal_SR__c = SR.ID;
        objB.Status__c = 'test';
        insert objB;        
        
        Inquiry__c inquiryRecord = new Inquiry__c();
        inquiryRecord.By_Pass_Validation__c = true;
        inquiryRecord.Party_ID__c = '12345';
        inquiryRecord.Title__c = 'MR.';
        inquiryRecord.Title_Arabic__c ='MR.';
        inquiryRecord.First_Name__c = 'Test';
        inquiryRecord.First_Name_Arabic__c ='Test';
        inquiryRecord.Last_Name__c ='Test';
        inquiryRecord.Last_Name_Arabic__c = 'Test';
        insert inquiryRecord;

        buyer__c b = new buyer__c();
        b.Buyer_Type__c =  'Individual';
        b.Address_Line_1__c =  'Ad1';
        b.Country__c =  'United Arab Emirates';
        b.City__c = 'Dubai' ;
        b.Inquiry__c = inquiryRecord.Id ;
        b.Date_of_Birth__c = string.valueof(system.today().addyears(-30)) ;
        b.Email__c = 'test@test.com';
        b.First_Name__c = 'firstname' ;
        b.Last_Name__c =  'lastname';
        b.Nationality__c = 'Indian' ;
        b.Passport_Expiry_Date__c = string.valueof(system.today().addyears(20)) ;
        b.Passport_Number__c = 'J0565556' ;
        b.Phone__c = '569098767' ;
        b.Phone_Country_Code__c = 'United Arab Emirates: 00971' ;
        b.Place_of_Issue__c =  'India';
        b.Title__c = 'Mr';
        b.Primary_Buyer__c = true;
        b.booking__c = objB.id;
        insert b;

        Booking_Unit__c obj3 = new Booking_Unit__c();
        obj3.Booking__c = objB.ID;
        obj3.Registration_ID__c = '13550';
        insert obj3;
        
        List<Booking_Unit__c> listB = new List<Booking_Unit__c>();
        listB  =[select id,
                        Booking__r.Deal_SR__r.Name,
                        Registration_ID__c
                 from Booking_Unit__c
                 where id=: obj3.ID];                
                 
        System.debug('...listB  ...'+listB);
        
        ReceiptChinaController.ReceiptData objInvData = new ReceiptChinaController.ReceiptData();
        objInvData.buyerName= 'test'; 
        objInvData.address= 'test';
        objInvData.remarks= 'test'; 
        objInvData.buRegIds= '13550';
        objInvData.modeOfPayment= 'Cash'; 
        objInvData.invoiceNumber= 'test123';
        objInvData.invoiceDueDate= system.today(); 
        objInvData.invoiceDate= system.today();
        objInvData.totalAmount= 20000; 
        objInvData.amount= 2000; 
        objInvData.vatAmount= 200; 
        objInvData.amountInWords= 'Hundred'; 

        PageReference pageRef = Page.ReceiptChina; 
        pageRef.getParameters().put('srid', String.valueOf(SR.Id));
        pageRef.getParameters().put('rmrk', String.valueOf('test'));
        pageRef.getParameters().put('add', String.valueOf('test'));
        pageRef.getParameters().put('amount', '122311');
        Test.setCurrentPage(pageRef);  

        ReceiptChinaController objController = new ReceiptChinaController();        
        
        System.assertEquals(objInvData.buRegIds, obj3.Registration_ID__c);  
        
    }
}