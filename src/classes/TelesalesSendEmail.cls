/****************************************************************************************************
* Name          : TelesalesSendEmail                                                                *
* Description   : Class to Send SMS for Telesales Process                                           *
* Created Date  : 23/04/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE            COMMENTS                                                *
* 1.0   Monali Nagpure      23/04/2018      Initial Draft.                                          *
* 1.1   Craig Lobo          31-05-2018      Code cleanup. Removed the logic for Receptionist Task   *
* 1.2   Craig Lobo          13-06-2018      Moved the Completed Email Sending to Workflow           *
* 1.3   Craig Lobo          09-07-2018      Added blank check for Activity Outcome                  *
****************************************************************************************************/
public without sharing class TelesalesSendEmail {

    /**
     * Constructor
     */
    public TelesalesSendEmail(){
        system.debug('TelesalesSendEmail>>>>>> ');
    }

    /**
     * 
     */
    public void PerformTaskActivity(List<Task> listTask){
        Map<String, Schema.SObjectType> sObjectTypeMap = Schema.getGlobalDescribe();
        Set<Id> meetingCompletedInqId = new Set<Id>();
        Set<Id> removeInqAccess = new Set<Id>();
        Map<String, String> actvityOutcomeMap = new Map<String, String>();

        Schema.SObjectType sObjectType = sObjectTypeMap.get('Inquiry__c') ;
        Schema.DescribeSObjectResult sObjectResult = sObjectType.getDescribe() ;
        String keyPrefix = sObjectResult.getKeyPrefix();

        // check if task owner is TSA
        // If TSA then fetch it's TSA TL user
        // Now change owner of current task to TSA TL
        Set<Id> removeInqAccessOwner = new Set<Id>();
        system.debug('=====listTask===' + listTask);

        for(Task taskInst: listTask){
            system.debug('WhatId===' + taskInst.WhatId );
            system.debug('Activity_Type_3__c===' + taskInst.Activity_Type_3__c);
            system.debug('Subject===' + taskInst.Subject );
            if(taskInst.WhatId != null
                && String.valueOf(taskInst.WhatId).startsWith(keyPrefix)
                && taskInst.Activity_Type_3__c == 'Meeting at Office'
                && taskInst.Subject == 'Meeting at Office'
            ){

                if(taskInst.Status == 'Completed') {
                    // After completion of meeting send email about completion
                    if(taskInst.Is_Meeting_completed__c == false){
                        meetingCompletedInqId.add(taskInst.WhatId);
                    }

                    actvityOutcomeMap.put(taskInst.Id, taskInst.Activity_Outcome__c);

                    // Remove access of Inquiry if Client is not interested.
                    removeInqAccess.add(taskInst.WhatId);
                    removeInqAccessOwner.add(taskInst.OwnerId);
                    system.debug('Rmv acs of Inq if Client not interested===' + taskInst);
                }
            }
        }

        updateOfficeMeetingRecords(actvityOutcomeMap);

        if(removeInqAccess.size() > 0 && removeInqAccessOwner.size() > 0)
            RemoveInquiryAccess(removeInqAccess,removeInqAccessOwner);

        if(meetingCompletedInqId.size() > 0) {
            MeetingCompletedMail(meetingCompletedInqId);
        }
    }


    public void updateOfficeMeetingRecords(Map<String, String> pActvityOutcomeMap) {
        List<Office_Meeting__c> offmList = new List<Office_Meeting__c>();
        System.debug('pActvityOutcomeMap>>>>::'+pActvityOutcomeMap);
        System.debug('pActvityOutcomeMap>>>>::'+[SELECT Id, Related_Task__c, Outcome__c, 
                                                 Status__c, Bypass_Validation__c
                                            FROM Office_Meeting__c
                                           WHERE Related_Task__c IN :pActvityOutcomeMap.keySet()]);
        
        for (Office_Meeting__c offmObj : [SELECT Id, Related_Task__c, Outcome__c, 
                                                 Status__c, Bypass_Validation__c
                                            FROM Office_Meeting__c
                                           WHERE Related_Task__c IN :pActvityOutcomeMap.keySet()]
        ) {
            if (pActvityOutcomeMap.containsKey(offmObj.Related_Task__c)) {
                String outcome = pActvityOutcomeMap.get(offmObj.Related_Task__c);
                if (String.isNotBlank(outcome)) {
                    if (outcome.startsWithIgnoreCase('Show')) {
                        offmObj.Outcome__c = 'Show';
                        offmObj.Status__c = 'Completed';
                        offmObj.Bypass_Validation__c = true; 
                        offmList.add(offmObj);
                    }
                    if (outcome.startsWithIgnoreCase('No Show')) {
                        offmObj.Outcome__c = 'No Show';
                        offmObj.Status__c = 'Completed';
                        offmObj.Bypass_Validation__c = true;
                        offmList.add(offmObj);
                    }
                }
            }
        }
        update offmList;
    }


    public void RemoveInquiryAccess(Set<Id> removeInqAccess, Set<Id> removeInqAccessOwner) {
        List<Inquiry__Share> existingShare = new List<Inquiry__Share>();
        // Get existing Inquiry Share Records
        for(Inquiry__Share inqShare : [ SELECT Id , RowCause, UserOrGroupId, ParentId
                                          FROM Inquiry__Share 
                                         WHERE ParentId IN :removeInqAccess
                                           AND UserOrGroupId IN :removeInqAccessOwner
        ]) {
            System.debug('inqShare.RowCause::'+inqShare.RowCause);
            if (inqShare.RowCause == Schema.Inquiry__Share.RowCause.TSA_TL__c
            ) {
                existingShare.add(inqShare);
            }
        }

        try{
            delete existingShare;
            System.debug('Delete  >>>> ' + existingShare);  
        } catch(Exception e){
            system.debug('>>>>> Exception >>>> ' + e);
        }
    }

    /**
     * 
     */
    public void MeetingCompletedMail(Set<Id> meetingCompletedInqId){
        system.debug('MeetingCompletedMail>>>>>>  ' + meetingCompletedInqId);
        List<Task> updatetask = new List<Task>();
        //List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        Map<String, String> mapBodyPhoneNumber = new Map<String, String>();
        Inquiry__c inqInsatnce ;
        List<Inquiry__c> updateInq = new List<Inquiry__c>();

        List<EmailTemplate> completedtemplateList = [SELECT Id 
                                                     FROM EmailTemplate 
                                                    WHERE Name = 'Meeting Completed' 
                                                       OR Name = 'Meeting Completed T3'
                                                 ORDER By Name ASC];

        System.debug('completedtemplateList>>>>>>  ' + completedtemplateList);

        List<String> toAddress;
        for(Inquiry__c inq : [SELECT Id, Inquiry_Status__c, RecordType.Name, TSA_TL_of_Owner__c,
                                     First_Name__c, Last_Name__c, Email__c, Meeting_Due_Date__c,
                                     Sales_Office__c, Mobile_Phone__c,Campaign_Name_Text__c,
                                     (SELECT OwnerId, Owner.Name,  Subject, ActivityDate, Type,
                                             Activity_Type_3__c, Activity_Outcome__c, WhatId, Status,
                                             Is_meeting_confirmed_reminder_sent__c, Show__c 
                                        FROM Tasks
                                       WHERE Status = 'Completed'
                                         AND Activity_Type_3__c = 'Meeting at Office'
                                         AND Is_Meeting_completed__c = false
                                        // AND Show__c = true
                                         ORDER By CreatedDate DESC
                                    )
                                FROM Inquiry__c
                               WHERE Id IN :meetingCompletedInqId
                                 AND RecordType.Name = 'Inquiry'
                                 //AND Inquiry_Status__c = 'Meeting Scheduled'
        ]) {
            system.debug('====MeetingCompletedMail===inq=======' +inq);
            system.debug('====MeetingCompletedMail===inq.Tasks=======' +inq.Tasks);
            toAddress = new List<String>();
            List<Task> inqTaskList = inq.Tasks;
            if(inqTaskList != null && !inqTaskList.isEmpty()) {
                system.debug('===inqTaskList===' +inqTaskList);
                for(Task taskInst : inqTaskList){
                    String outCome = taskInst.Activity_Outcome__c;
                    if (String.isNotBlank(outCome)) {
                        if (outCome.startsWith('Show')) {
                            // Send email for meeting completed
                            // Sent via workflow

                            // Send SMS for meeting completed
                            String body = 'Hi ' + inq.First_Name__c + ' '+ inq.Last_Name__c + ','
                                        + ' Thank you for meeting us. We hope hear from you soon. '
                                        + ' For any details or assistance, please call us @04-3019999';
                            mapBodyPhoneNumber.put(inq.Mobile_Phone__c, body);
                            system.debug('===taskInst===' +taskInst);
                            Task updatetsk = new Task();
                            updatetsk = taskInst;
                            updatetsk.Is_Meeting_completed__c = true;
                            updateTask.add(updatetsk );

                            inqInsatnce = new Inquiry__c();
                            inqInsatnce.Id = inq.Id; 
                            inqInsatnce.Is_Meeting_Completed__c = true; 
                            inqInsatnce.Completed_Conditions__c = true;
                            updateInq.add(inqInsatnce);
                            system.debug('===updateInq===' +updateInq);
                        }
                    }
                }

            }
        }

        try {
            System.debug('*****mapBodyPhoneNumber**************** ' + mapBodyPhoneNumber);
            List<String> pMobileNumberList;
            for(String phoneNo : mapBodyPhoneNumber.keySet()){
                pMobileNumberList = new List<String>();
                pMobileNumberList.add(phoneNo); 
                String emailbody = mapBodyPhoneNumber.get(phoneNo);
                //added by Charan on 2nd Nov 2020
                if (!Test.isRunningTest())
                    SecureSMSService.prepareData(emailbody, pMobileNumberList);
            }

            System.debug('updateTask>>>>>' + updateTask);
            update updateTask;

            System.debug('updateInq>>>>>>' + updateInq);
            update updateInq;

        } catch(Exception e){
            system.debug('Exception>>>>> ' + e );
        }
    }

    /** 
     * Method to send out SMS for Meeting Completeion
     */
    /* 
    @Future(callout=true)
    public static void sendSMS(String body, List<String> pMobileNumberList ){
        try{
            SecureSMSService.sendSecureSMS(body, pMobileNumberList);
            system.debug('SMS send*>>>');
        } catch(Exception ex) {
            system.debug('SMS Exception occured>>>>>');
        }
    }
    */

}