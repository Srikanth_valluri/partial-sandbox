public with sharing class BalanceSOAService{
    
    public static Id mortgageCaseRecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Mortgage').RecordTypeId;
    
    @InvocableMethod
    public static void getBalanceSOA( List<Case> lstCase ) {
        system.debug('lstCase==='+lstCase);
        Set<Id> setCaseId = new Set<Id>();
       
        
        for( case objCaseList : lstCase ) {
            //system.debug(objCaseList.RecordTypeId + ' mortgageCaseRecordTypeId : '+AddressCallingRecordTypeId);
            if( objCaseList.RecordTypeId == mortgageCaseRecordTypeId ) {
                 system.debug('In Address If');
                setCaseId.add( objCaseList.Id );
            }
        }
        if( !setCaseId.isEmpty() ) {
            updateBalanaceSOA(setCaseId);
        }
    }

    @Future(callout=true)   
    public static void updateBalanaceSOA( set<Id> setCaseId ) {

        Decimal decTotal = 0.0;
        //List<Task> lsTaskToUpdate = new List<Task>();
        list<Case> listCaseObject = new list<Case>([select Id, Seller__c, Booking_Unit__r.Id
                                                    , Booking_Unit__r.Registration_ID__c
                                                    , Recordtype.DeveloperName
                                                    from Case where Booking_Unit__c != null and Id IN: setCaseId
                                                    and Booking_Unit__r.Registration_ID__c != null]);
        system.debug('listCaseObject==='+listCaseObject);
        if(!listCaseObject.isEmpty()) {
            String strDueResponse = assignmentEndpoints.fetchAssignmentDues(listCaseObject[0].Booking_Unit__r);
            system.debug('strDueResponse==='+strDueResponse);
            if(String.isNotBlank(strDueResponse)){
                map<String,Object> mapDeserializeDue = (map<String,Object>)JSON.deserializeUntyped(strDueResponse);
                if(mapDeserializeDue.get('status') == 'S'){
                    strDueResponse = strDueResponse.remove('{');
                    strDueResponse = strDueResponse.remove('}');
                    strDueResponse = strDueResponse.remove('"');
                    //lstPayments = new List<paymentInfo>();
                    system.debug('after all replacements******'+strDueResponse);
                    for(String st : strDueResponse.split(',')){
                        String strKey = st.substringBefore(':').trim();
                        system.debug('*****strKey*****'+strKey);
                        if(!strKey.equalsIgnoreCase('Status')) {
                            system.debug('st*****'+st);
                            Decimal calAmount = 0.0;
                            //paymentInfo objP = new paymentInfo();
                            //objP.strType = strKey;
                            if(Decimal.valueOf(st.subStringAfter(':').trim()) > 0) {
                               if( listCaseObject[0].Recordtype.Developername == 'Mortgage'
                               && strKey.contains('Balance as per SOA') ){
                                   system.debug('*****INSIDE*****');
                                   calAmount = Decimal.valueOf(st.subStringAfter(':').trim());
                               }
                            }
                            else {
                               calAmount = 0.0;
                            }
                            decTotal = decTotal + calAmount;
                            system.debug('decTotal*************'+decTotal);
                            //lstPayments.add(objP);
                        }
                    }
                    Case caseObj = new Case();
                    caseObj.Id = listCaseObject[0].Id;
                    caseObj.Pending_Amount__c = decTotal;
                    caseObj.Mortgage_Status__c = 'Mortgage Submit DSR';
                    if(listCaseObject[0].Recordtype.Developername == 'Mortgage'
                    && decTotal <= 0){
                        caseObj.Payment_Verified__c = true;
                    }
                    update caseObj;

                    /*for( Task objTask : [ SELECT WhoId
                                      , WhatId
                                      , Type
                                      , Status
                                      , OwnerId
                                      , Id
                                      , Subject
                                      , CreatedDate
                                      , Description
                                      , Assigned_User__c
                                      , ActivityDate
                                      , Owner.Name 
                                   FROM Task
                                  WHERE WhatId =: listCaseObject[0].Id ] ) {
                            if( objTask.Subject == System.Label.Mortgage_27 ) {
                                objTask.Status = 'Closed';
                                objTask.Completed_DateTime__c = System.Now();
                            }
                            lsTaskToUpdate.add(objTask);
                         }


                 if(!lsTaskToUpdate.isEmpty() ){
                    update lsTaskToUpdate;
                 }*/

                }else if(mapDeserializeDue.get('status') == 'E'
                && mapDeserializeDue.containsKey('message')){
                    //errorMessage = 'Error : '+mapDeserializeDue.get('message');
                    Error_Log__c objErr = new Error_Log__c();
                    objErr.Account__c = listCaseObject[0].Seller__c;
                    objErr.Booking_Unit__c = listCaseObject[0].Booking_Unit__r.Id;
                    objErr.Case__c = listCaseObject[0].Id;
                    objErr.Process_Name__c = 'Mortgage';
                    objErr.Error_Details__c = 'Error : '+mapDeserializeDue.get('message');
                    //insert objErr;
                    insert objErr;

                }
            }
            else {
                //errorMessage = 'Error : No Response from IPMS for Payment Dues';

                    Error_Log__c objErr = new Error_Log__c();
                    objErr.Account__c = listCaseObject[0].Seller__c;
                    objErr.Booking_Unit__c = listCaseObject[0].Booking_Unit__r.Id;
                    objErr.Case__c = listCaseObject[0].Id;
                    objErr.Process_Name__c = 'Mortgage';
                    objErr.Error_Details__c = 'Error : No Response from IPMS for Payment Dues';
                    //insert objErr;
                    insert objErr;
            }
        }

    }
}