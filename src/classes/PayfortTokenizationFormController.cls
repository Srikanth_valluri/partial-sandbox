public class PayfortTokenizationFormController {

    @testVisible private string GET_INSTALLMENT_ENDPOINT;
    @testVisible private string PAYFORT_RESPONSE_PHRASE;
    @testVisible private String PAYFORT_REQUEST_PHRASE;

    public String PAYFORT_MERCHANT_IDENTIFIER               {get; set;}
    public String PAYFORT_ACCESS_CODE                       {get; set;}
    public String PAYFORT_LANGUAGE                          {get; set;}
    public String RETURNURL                                 {get; set;}
    public String SITE_BASEURL;

    public String tokenizationEndpoint                      {get; set;}
    public String signature                                 {get; set;}
    public String merchant_reference                        {get; set;}
    public String service_command                           {get; set;}
    public String return_url                                {get; set;}
    public String selectedAccountId                         {get; set;}
    public Boolean doDirectPurchase                         {get; set;}
    public String payfortCardNumber                         {get; set;}
    public String payfortTokenName                          {get; set;}
    public String cvv                                       {get; set;}
    public Decimal amtToDisplay                             {get; set;}

    public cls_installment_details installDetailsResp       {get; set;}
    public String planCode                                  {get; set;}
    public String issuerCode                                {get; set;}
    public Decimal displayAmt                               {get; set;}
    public String cardBinNumber                             {get; set;}
    public Integer cardNumLength                             {get; set;}
    public Boolean shwPlans                                 {get; set;}
    public List<PayfortTokenizationFormController.cls_plan_details> lstPlansToDisplay       {get; set;}
    public PayfortTokenizationFormController.cls_issuer_detail   issuerToDisplay            {get; set;}

    //public String issuerCode;               
    //public String planCode;
    public String currencyInPay;
    public String amount;
    public Receipt__c Receipt                               {get; set;}    
    public List<Account> lstAccount;  
    public boolean isSecondSHACall;

    public PayfortTokenizationFormController() {

        System.debug('getParameters: '+Apexpages.currentpage().getparameters() );

        //getting required data from CS
        PayfortGatewayCreds__c objCreds = PayfortGatewayCreds__c.getInstance();
        System.debug('objCreds: '+objCreds);

        PAYFORT_MERCHANT_IDENTIFIER = objCreds.Merchant_Identifier__c;
        PAYFORT_REQUEST_PHRASE = objCreds.Request_Phrase__c;
        PAYFORT_RESPONSE_PHRASE = objCreds.ResponsePhrase__c;
        PAYFORT_ACCESS_CODE = objCreds.AccessCode__c;
        PAYFORT_LANGUAGE = objCreds.Language__c;
        RETURNURL = objCreds.SiteBaseURL__c + '/PayfortPurchasePage';
        tokenizationEndpoint = objCreds.TokenizationEndpoint__c;
        currencyInPay = objCreds.CurrencyInPay__c; 
        GET_INSTALLMENT_ENDPOINT = objCreds.PurchaseEndpoint__c;
        SITE_BASEURL = objCreds.SiteBaseURL__c;

        shwPlans = false;
        isSecondSHACall = false;

        if(ApexPages.currentpage().getParameters().containsKey('amount') && String.isNotBlank(ApexPages.currentpage().getParameters().get('amount')) ) {

            Decimal amt;
            amt = Decimal.valueOf(Apexpages.currentpage().getParameters().get('amount')) * 100;
            amount = String.valueOf(amt);

            amtToDisplay = Decimal.valueOf(amount) / 100;
            System.debug('amtToDisplay :'+amtToDisplay);

            System.debug('amount: '+amount);
            RETURNURL = RETURNURL+'?amount='+amount;
            
        }

        //calling GET_INSTALLMENNT_PLAN callout
        getInstallmentsCallout();

        //For installment
        if(ApexPages.currentpage().getParameters().containsKey('issuer_code') && ApexPages.currentpage().getParameters().containsKey('plan_code') && String.isNotBlank(ApexPages.currentpage().getParameters().get('issuer_code')) && String.isNotBlank(ApexPages.currentpage().getParameters().get('plan_code'))) {

            System.debug('inside installment if of tokenization');

            issuerCode = ApexPages.currentpage().getParameters().get('issuer_code');
            planCode = ApexPages.currentpage().getParameters().get('plan_code');

            RETURNURL = RETURNURL+'&issuer_code='+issuerCode+'&plan_code='+planCode;
        }

        doDirectPurchase = false;
        //Receipt = insertReceipt();
        selectedAccountId = CustomerCommunityUtils.customerAccountId;
        System.debug('AccountId:: '+selectedAccountId);
        if(selectedAccountId != null && selectedAccountId != '') {
            lstAccount = [SELECT id
                            , Payfort_Token_Name__c
                            , Payfort_Card_Number__c
                            , Email__pc 
                            , Email__c
                       FROM Account
                       WHERE Id =: selectedAccountId];
            System.debug('lstAccount:: '+lstAccount);

            if(lstAccount.size() > 0 && String.isNotBlank(lstAccount[0].Payfort_Token_Name__c)) {
                doDirectPurchase = true;
                payfortCardNumber = lstAccount[0].Payfort_Card_Number__c;
                payfortTokenName = lstAccount[0].Payfort_Token_Name__c;

                if(String.isNotBlank(payfortCardNumber)) {
                    cardBinNumber = payfortCardNumber.left(6);
                    CompareCardBins();     //for doDirectPurchase;
                }
            }
        }
    }


    public Static Receipt__c insertReceipt() {
        Receipt__c Receipt = new Receipt__c();
        Receipt.isPayfortPayment__c = true;
        if(String.isNotBlank(Apexpages.currentpage().getParameters().get('amountToPay') )) {
            Receipt.Outstanding_Amount__c = Decimal.valueOf(Apexpages.currentpage().getParameters().get('amountToPay'));
        }
        
        if(String.isNotBlank(Apexpages.currentpage().getParameters().get('adminfee') )) {
            Receipt.Administration_Fee__c = Decimal.valueOf(Apexpages.currentpage().getParameters().get('adminfee'));
        }
         
        Receipt.Amount__c = Decimal.valueOf(ApexPages.currentpage().getParameters().get('amount'));
        Receipt.Receipt_Type__c='Card';
        Receipt.Transaction_Date__c=system.now();
        Id rt=Schema.SObjectType.Receipt__c.getRecordTypeInfosByName().get('Card').getRecordTypeId();
        Receipt.RecordTypeId=rt;
        Receipt.Customer__c = CustomerCommunityUtils.customerAccountId;
        Receipt.Origin__c = 'Customer Portal';
        if(String.isNotBlank(ApexPages.currentpage().getParameters().get('unitId'))) {
            Receipt.Booking_Unit__c = ApexPages.currentpage().getParameters().get('unitId');
        }
        
        insert Receipt;
        
        //Fetching back same Receipt
        Receipt = [
            SELECT  Id
                    , Name
                    , Amount__c
            FROM    Receipt__c
            WHERE   Id = :Receipt.Id];
            System.debug('Receipt is::'+Receipt);
            //merchant_reference = Receipt.Name;
            return Receipt;
    }

    public void getTokenizationSHA_256() {

        if(!isSecondSHACall) {
            Receipt__c Receipt = insertReceipt();
            merchant_reference = Receipt.Name;
        }    
        

        String testString = PAYFORT_REQUEST_PHRASE+'access_code='+PAYFORT_ACCESS_CODE+'language='+PAYFORT_LANGUAGE+'merchant_identifier='+PAYFORT_MERCHANT_IDENTIFIER+'merchant_reference='+merchant_reference+'return_url='+RETURNURL+'service_command=TOKENIZATION'+PAYFORT_REQUEST_PHRASE;
        System.debug('Signature pattern created for SHA-256: '+testString);
        String algorithmName = 'SHA-256';
        Blob hmacData = Crypto.generateDigest(algorithmName, Blob.valueOf(testString));
        System.debug('SHA-256 :'+EncodingUtil.convertToHex(hmacData));

        
        if(String.isNotBlank(EncodingUtil.convertToHex(hmacData))) {
            signature = EncodingUtil.convertToHex(hmacData);
        }
        system.debug('returnStr: '+signature);

        //return returnStr;
        //return EncodingUtil.base64Encode(hmacData);
    }

    public PageReference GotoDirectPurchase() {
        //'https://partial-servicecloudtrial-155c0807bf-1580afc5db1.cs80.force.com/Customer/PayfortPurchasePage/?service_command=TOKENIZATION&merchant_reference='+merchRef+'&token_name='+tokenName+'&remember_me=NO&card_security_code='+cvv;

        System.debug('cvv in GotoDirectPurchase: '+cvv);

        PageReference pgRef = Page.PayfortPurchasePage;
        pgRef.getParameters().put('service_command','TOKENIZATION');
        pgRef.getParameters().put('merchant_reference',merchant_reference);
        pgRef.getParameters().put('token_name',payfortTokenName);
        pgRef.getParameters().put('remember_me','NO');
        pgRef.getParameters().put('card_security_code',cvv);
        pgRef.getParameters().put('amount', amount);
        pgRef.getParameters().put('unitId', String.isNotBlank(ApexPages.currentpage().getParameters().get('unitId')) ? ApexPages.currentpage().getParameters().get('unitId') : '');
        pgRef.getParameters().put('calculateSHA', 'NO');

        if(String.isNotBlank(planCode)) {
            pgRef.getParameters().put('issuer_code',issuerToDisplay.issuer_code);
            pgRef.getParameters().put('plan_code',planCode);    
        }
        
        pgRef.setRedirect(true);
        System.debug('pgRef : '+pgRef);
        return pgRef;
    }

    public PageReference RedirectPrevPage() {
        System.debug('going to previous (UnitDetails) page');

        //Deleting FM Receipt in current instance
        List<Receipt__c> lstFmRecp = [SELECT id, Name FROM Receipt__c WHERE Name =: merchant_reference];
        System.debug('lstFmRecp: '+lstFmRecp);

        if(lstFmRecp.size() > 0) {
            delete lstFmRecp;
        }

        String idUnit = String.isNotBlank(ApexPages.currentpage().getParameters().get('unitId')) ? 
                        ApexPages.currentpage().getParameters().get('unitId') : 
                        '';

        String unitDetailsURL = SITE_BASEURL+'?view=UnitDetail&id='+idUnit;
        System.debug('unitDetailsURL: '+unitDetailsURL);
        PageReference prevPage = new PageReference(unitDetailsURL);
        prevPage.setRedirect(true);
        return prevPage;  
    }

    public PageReference DeleteCardToken() {

        //Updating Account (removing TokenName & Card Number)
        System.debug('lstAccount: '+lstAccount);
        if(lstAccount.size() > 0) {

            UpdateTokenCallout(lstAccount[0].Payfort_Token_Name__c);    //calling UpdateToken API to deactivate the saved Token;

            lstAccount[0].Payfort_Token_Name__c = null;
            lstAccount[0].Payfort_Card_Number__c = null;

            update lstAccount;
        }

        //Deleting FM Receipt in current instance
        List<Receipt__c> lstFmRecp = [SELECT id, Name FROM Receipt__c WHERE Name =: merchant_reference];
        if(lstFmRecp.size() > 0) {
            System.debug('Deleting FM receipt: '+lstFmRecp);
            delete lstFmRecp;
        }

        PageReference pgToken = Page.PayfortTokenizationPage;
        
        pgToken.getParameters().put('amount', String.valueOf(amtToDisplay));
        //pgToken.getParameters().put('amount', amount);
        pgToken.getParameters().put('unitId', String.isNotBlank(ApexPages.currentpage().getParameters().get('unitId')) ? ApexPages.currentpage().getParameters().get('unitId') : '');
        pgToken.getParameters().put('amountToPay',Apexpages.currentpage().getParameters().get('amountToPay'));
        pgToken.getParameters().put('adminfee',Apexpages.currentpage().getParameters().get('adminfee'));

        //for installment
        if(String.isNotBlank(issuerCode) && String.isNotBlank(planCode)) {
            pgToken.getParameters().put('issuer_code',issuerCode);
            pgToken.getParameters().put('plan_code',planCode);  
        }

        System.debug('pg ref to purchase from tokenization: '+pgToken);
        pgToken.setRedirect(true);
        return pgToken;

    }

    public void UpdateTokenCallout(string tokenName) {
        //$2y$10$QzSutSlCYaccess_code=wT7xGi6a4fwntXQyq0Gslanguage=enmerchant_identifier=17e98f0bmerchant_reference=RP-001service_command=UPDATE_TOKENtoken_name=1f4076597d004464ad977ac3dfcc45e3$2y$10$QzSutSlCY

        SYstem.debug('saved token: '+tokenName);

        String testString = PAYFORT_REQUEST_PHRASE+'access_code='+PAYFORT_ACCESS_CODE+'language=en'+'merchant_identifier='+PAYFORT_MERCHANT_IDENTIFIER+'merchant_reference='+merchant_reference+'service_command=UPDATE_TOKEN'+'token_name='+tokenName+'token_status=INACTIVE'+PAYFORT_REQUEST_PHRASE;

        System.debug('Signature pattern created for SHA-256 in UpdateToken: '+testString);
        String algorithmName = 'SHA-256';
        Blob hmacData = Crypto.generateDigest(algorithmName, Blob.valueOf(testString));
        System.debug('SHA-256 in UpdateToken :'+EncodingUtil.convertToHex(hmacData));

        JSONGenerator gen = JSON.createGenerator(true);    
            gen.writeStartObject();      
            gen.writeStringField('access_code',PAYFORT_ACCESS_CODE);
            gen.writeStringField('language',PAYFORT_LANGUAGE);
            gen.writeStringField('merchant_identifier',PAYFORT_MERCHANT_IDENTIFIER);
            gen.writeStringField('merchant_reference',merchant_reference);
            gen.writeStringField('service_command','UPDATE_TOKEN');
            gen.writeStringField('signature',EncodingUtil.convertToHex(hmacData));
            gen.writeStringField('token_name',tokenName);
            gen.writeStringField('token_status','INACTIVE');
            gen.writeEndObject();    
        String jsonS = gen.getAsString();
        System.debug('jsonMaterials in UpdateToken'+jsonS);

        HttpRequest req = new HttpRequest();
        req.setEndpoint(GET_INSTALLMENT_ENDPOINT);  //Same endpoint is used for INstallment + UpdateToken
        System.debug('getEndpoint: '+req.getEndpoint());
        req.setHeader('Content-Type', 'application/json');
        req.setMethod('POST');
        req.setBody(jsonS);
        System.debug('req : '+req);
        System.debug('req.getBody() : '+req.getBody());
        HttpResponse response = new Http().send(req);

        System.debug('Status Code in UpdateToken = ' + response.getStatusCode());
        System.debug('Installment body in UpdateToken = ' + response.getBody());
        System.debug('toString body in UpdateToken : '+response.toString());

        Boolean isValid = ValidateSignatureForResponse(response.getBody(), PAYFORT_RESPONSE_PHRASE);
        System.debug('isValid: '+isValid);

    }

     public static boolean ValidateSignatureForResponse(string responseStr, String PAYFORT_RESPONSE_PHRASE) {

        List<String> keyValuePairs = new List<String>();
        String signFromPayfort;

        Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(responseStr);
        System.debug('m in ValidateSignatureForResponse: '+m);

        for(String mStr : m.keySet()) {
            System.debug('mStr: '+mStr);
            keyValuePairs.add(mStr+'='+String.valueOf(m.get(mStr)));
        }

        keyValuePairs.sort();
        System.debug('keyValuePairs: '+keyValuePairs);

        String signPattern ='';
        if(keyValuePairs.size() > 0) {
            signPattern += PAYFORT_RESPONSE_PHRASE; 
             for(String objStr : keyValuePairs) {
                if(!objStr.containsIgnoreCase('signature=')) {
                    System.debug('objStr: '+objStr);
                    String decodedStr = EncodingUtil.urlDecode(objStr, 'UTF-8');
                    System.debug('decodedStr: '+decodedStr);
                    signPattern += decodedStr;
                }

                if(objStr.containsIgnoreCase('signature=')) {
                    signFromPayfort = objStr.substringAfter('signature=');
                }
            }
            signPattern += PAYFORT_RESPONSE_PHRASE;
        }

        System.debug('signPattern generated : '+signPattern);
        String algorithmName = 'SHA-256';
        Blob hmacData = Crypto.generateDigest(algorithmName, Blob.valueOf(signPattern));
        System.debug('signature generated at our end :'+EncodingUtil.convertToHex(hmacData));
        System.debug('sign from payfort : '+ signFromPayfort);

        return EncodingUtil.convertToHex(hmacData) == signFromPayfort ? true : false;
    }

    public void getInstallmentsCallout() {

        String SHA256Sign = getSHA256Signature();
        System.debug('signature is : '+ SHA256Sign);

        //JSON creation
        JSONGenerator gen = JSON.createGenerator(true);    
            gen.writeStartObject();      
            gen.writeStringField('access_code',PAYFORT_ACCESS_CODE);
            gen.writeStringField('amount',amount);
            gen.writeStringField('currency',currencyInPay);
            gen.writeStringField('query_command', 'GET_INSTALLMENTS_PLANS');
            gen.writeStringField('merchant_identifier',PAYFORT_MERCHANT_IDENTIFIER);    
            gen.writeStringField('signature',SHA256Sign);
            gen.writeEndObject();    
        String jsonS = gen.getAsString();
        System.debug('jsonMaterials'+jsonS);

        HttpRequest req = new HttpRequest();
        req.setEndpoint(GET_INSTALLMENT_ENDPOINT);
        System.debug('getEndpoint: '+req.getEndpoint());
        req.setHeader('Content-Type', 'application/json');
        req.setMethod('POST');
        req.setBody(jsonS);
        System.debug('req : '+req);
        System.debug('req.getBody() : '+req.getBody());
        HttpResponse response = new Http().send(req);

        System.debug('Status Code = ' + response.getStatusCode());
        System.debug('Installment body = ' + response.getBody());
        System.debug('toString body: '+response.toString());

        Map<String, Object> mapResponse =(Map<String, Object>)JSON.deserializeUntyped(response.getBody());

        List<Map<String, Object>> lstMap_issuer_detail = new List<Map<String, Object>>();
        Map<String, Object> mapPlanDetails = new Map<String, Object>();

        if(mapResponse.containsKey('response_message') && String.valueOf(mapResponse.get('response_message')).equalsIgnoreCase('Success')) {

            String installment_details = JSON.serializePretty(mapResponse.get('installment_detail'));
            System.debug('String installment_details :'+installment_details);  //deserialize from here

            installDetailsResp = (cls_installment_details) Json.deserialize(installment_details, cls_installment_details.class);

            System.debug('installDetailsResp: '+installDetailsResp);
            System.debug('installDetailsResp.size(): '+installDetailsResp.issuer_detail.size());
            System.debug('installDetailsResp.issuer_detail.plan_details: '+installDetailsResp.issuer_detail[1].plan_details);

            //Sorting
            for(Integer i=0; i<installDetailsResp.issuer_detail.size(); i++) {

                System.debug('plan_details: '+installDetailsResp.issuer_detail[i].plan_details.size());
                
                if(installDetailsResp.issuer_detail[i].plan_details.size() > 0) {
                    installDetailsResp.issuer_detail[i].plan_details.sort();                    
                    System.debug('after sorting: '+installDetailsResp.issuer_detail[i].plan_details);

                    for(Integer j=0; j<installDetailsResp.issuer_detail[i].plan_details.size(); j++) {
                        SYstem.debug('curr. code in for loop:'+installDetailsResp.issuer_detail[i].plan_details[j].currency_code);
                        if(installDetailsResp.issuer_detail[i].plan_details[j].currency_code != 'AED') {
                            installDetailsResp.issuer_detail[i].plan_details.remove(j);     //deleting plan details wihout AED currency;
                        }
                    }
                    //for(cls_plan_details objPlan : installDetailsResp.issuer_detail[i].plan_details) {
                    //  objPlan.amountPerMonth.format();
                    //}
                }

                
            }

        }
        else {
            //error response;
        }
    }

    public string getSHA256Signature() {

        String testString = PAYFORT_REQUEST_PHRASE+'access_code='+PAYFORT_ACCESS_CODE+'amount='+amount+'currency='+currencyInPay+'merchant_identifier='+PAYFORT_MERCHANT_IDENTIFIER+'query_command=GET_INSTALLMENTS_PLANS'+PAYFORT_REQUEST_PHRASE;
        System.debug('Signature pattern created for SHA-256: '+testString);
        String algorithmName = 'SHA-256';
        Blob hmacData = Crypto.generateDigest(algorithmName, Blob.valueOf(testString));
        System.debug('SHA-256 :'+EncodingUtil.convertToHex(hmacData));

        return EncodingUtil.convertToHex(hmacData);
    }

    public void CompareCardBins() {
        System.debug('cardBinNumber: '+cardBinNumber);
        System.debug('cardNumLength: '+cardNumLength);

        if(lstAccount.size() > 0 && String.isNotBlank(lstAccount[0].Payfort_Card_Number__c)) {
            cardNumLength = lstAccount[0].Payfort_Card_Number__c.length();
            System.debug('cardNumLength from acc: '+cardNumLength);
        }
        
        if(installDetailsResp.issuer_detail.size() > 0) {

            Integer j=0;

            for(Integer i=0; i<installDetailsResp.issuer_detail.size(); i++) {
                System.debug('bins size: '+installDetailsResp.issuer_detail[i].bins.size());

                for(j=0; j<installDetailsResp.issuer_detail[i].bins.size(); j++) {
                    System.debug('bins : '+installDetailsResp.issuer_detail[i].bins[j]);

                    if(cardBinNumber == installDetailsResp.issuer_detail[i].bins[j].bin && cardNumLength == 16) {
                        System.debug('bin match found for issuer code: '+installDetailsResp.issuer_detail[i].issuer_code);

                        issuerToDisplay = new PayfortTokenizationFormController.cls_issuer_detail();
                        issuerToDisplay = installDetailsResp.issuer_detail[i];
                        lstPlansToDisplay = new List<PayfortTokenizationFormController.cls_plan_details>();
                        lstPlansToDisplay = installDetailsResp.issuer_detail[i].plan_details;
                        shwPlans = true;
                        System.debug('lstPlansToDisplay: '+lstPlansToDisplay);
                        break;
                    }
                    else {
                        shwPlans = false;
                    }
                }

            }
        }

    }

    public void AddPlanIssuerCodeinURL() {

        System.debug('ActionFunction planCode'+planCode);
        System.debug('RETURNURL on entry :'+RETURNURL);

        if(RETURNURL.containsIgnoreCase('issuer_code=') && RETURNURL.containsIgnoreCase('plan_code=') ) {
            string subStr = RETURNURL.substringAfter('issuer_code=');
            System.debug('subStr: '+subStr);
            RETURNURL = RETURNURL.remove(subStr);
            System.debug('after removing substr: '+RETURNURL);
            
            if(planCode != 'fullPayment') {
                RETURNURL = RETURNURL+issuerToDisplay.issuer_code+'&plan_code='+planCode;   
            }
            
        }
        else if(planCode != 'fullPayment'){

            RETURNURL = RETURNURL+'&issuer_code='+issuerToDisplay.issuer_code+'&plan_code='+planCode;
        }

        isSecondSHACall = true;
        getTokenizationSHA_256();

        System.debug('Final RETURNURL :'+RETURNURL);
    }

    //wrapper class for issuer_details
    public class cls_installment_details {
        public cls_issuer_detail[] issuer_detail    {get; set;}
    }

    public class cls_issuer_detail {
        public String processing_fees_message_en    {get; set;}
        //public cls_disclaimer_message_en disclaimer_message_en;
        public String confirmation_message_en       {get; set;} //This is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is random messg
        //public cls_processing_fees_message_ar processing_fees_message_ar;
        //public cls_disclaimer_message_ar disclaimer_message_ar;
        //public String confirmation_message_ar;    //This is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is a random pe confirmation messageThis is random messg
        public cls_bins[] bins                      {get; set;}
        public cls_plan_details[] plan_details      {get; set;}
        //public String formula;    //(amount +(amount *effective rate/100))/period
        public String banking_system                {get; set;} //Non Islamic
        public String issuer_logo_en                {get; set;} //https://sbstatic.payfort.com/frontend/files/logos/issuer/logo_en_152.png
        public String issuer_logo_ar                {get; set;} //https://sbstatic.payfort.com/frontend/files/logos/issuer/logo_ar_152.png
        public String country_code                  {get; set;} //ARE
        public String terms_and_condition_en        {get; set;} //http://www.payfort.com
        public String terms_and_condition_ar        {get; set;} //http://www.payfort.com
        public String issuer_name_en                {get; set;} //Bank 1 UAE
        public String issuer_name_ar                {get; set;} //Bank 1
        public String issuer_code                   {get; set;} //mxedBP
    }
    //class cls_processing_fees_message_en {
    //}
    class cls_disclaimer_message_en {
    }
    class cls_processing_fees_message_ar {
    }
    class cls_disclaimer_message_ar {
    }
    class cls_bins {
        public String card_brand_code;  //VISA
        public String currency_code;    //AED
        public String country_code; //ARE
        public String bin;  //411133
    }
    public class cls_plan_details implements Comparable {
        public Decimal amountPerMonth               {get; set;} //0.18
        public Integer maximum_amount               {get; set;} //100000000
        public Integer minimum_amount               {get; set;} //100
        public Integer fee_display_value            {get; set;} //1000
        public String plan_type                     {get; set;} //Local
        public String plan_merchant_type            {get; set;} //Non Partner
        public String rate_type                     {get; set;} //Flat
        public Integer processing_fees_amount       {get; set;} //0
        public String processing_fees_type          {get; set;} //Fixed
        public Integer fees_amount                  {get; set;} //1000
        public String fees_type                     {get; set;} //Percentage
        public Integer number_of_installment        {get; set;} //60
        public String currency_code                 {get; set;} //AED
        public String plan_code                     {get; set;} //yAVoM5

        //for sorting plan_details as per No. of Installments

        public Integer compareTo(Object compareTo) {
               // Cast argument to OpportunityWrapper
               cls_plan_details compareInstance = (cls_plan_details)compareTo;
               
               // The return value of 0 indicates that both elements are equal.
               Integer returnValue = 0;
               if (number_of_installment > compareInstance.number_of_installment) {
                   // Set return value to a positive value.
                   returnValue = 1;
               } else if (number_of_installment < compareInstance.number_of_installment) {
                   // Set return value to a negative value.
                   returnValue = -1;
               }
               
               return returnValue;       
           }
    }

}