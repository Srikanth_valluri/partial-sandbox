@isTest
public class Damac_AmeyoLeadScoreSchedular_Test{
    static testMethod Void ameyoLeadScore(){
            Campaign__c camp = new Campaign__c();
            camp.End_Date__c = system.today().addmonths(10);
            camp.Marketing_End_Date__c = system.today().addmonths(10);
            camp.Marketing_Start_Date__c = system.today().addmonths(-10);
            camp.Start_Date__c =  system.today().addmonths(-10);
            insert camp;
    
            Inquiry__c inq = new Inquiry__c ();
            inq.Activity_Counter__c = 101;
            inq.Inquiry_Status__c = 'Active';
            inq.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
            inq.campaign__c = camp.id;
            inq.Telesales_Executive__c = UserInfo.getUserId();
            inq.Promoter_Name__c = 'Test';
            insert inq;
            
           
            
            Task t = new Task ();
            t.whatId = inq.Id;
            t.Event_Type__c = 'Ameyo Dialer';
            t.Subject = 'call';
            t.Ameyo_Dialer_Disposition__c = 'CONNECTED' ;
            t.Sub_Disposition_Type__c = 'Redirected to CONNECT';
            t.Call_Start_Time__c = System.now().addDays(-1);
            t.Call_End_Time__c = System.now();
            insert t;
            
            
            Test.startTest();
            Damac_AmeyoLeadScoreSchedular ameyoLeadScore = new Damac_AmeyoLeadScoreSchedular();
            String cronExp= '0 0 23 * * ?';
            String jobId = System.schedule('UpdateCallDetailsOnInquiryBatch',cronExp,ameyoLeadScore);
            
            Database.executeBatch (new Damac_AmeyoLeadScore(), 1);
            Test.stopTest();
    }
}