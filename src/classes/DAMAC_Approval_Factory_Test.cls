@isTest(seeAllData = true)
public class DAMAC_Approval_Factory_Test {
    /*
    @TestSetup
    static void setupData() {
        
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        Id DealRT = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = acc.Id;
        sr.RecordTypeId = DealRT;
        sr.Agency__c = acc.id;
        sr.Agency_Type__c = 'Corporate';
        insert sr;
        
        List<New_Step__c> newStepsList = new List<New_Step__c>();
        New_Step__c nstp1 = new New_Step__c();
        nstp1.Service_Request__c = sr.id;
        nstp1.Step_No__c = 2;
        nstp1.Step_Status__c = 'Awaiting Token Deposit';
        nstp1.Step_Type__c = 'Token Payment';
        newStepsList.add(nstp1);
        
        New_Step__c nstp2 = new New_Step__c();
        nstp2.Service_Request__c = sr.id;
        nstp2.Step_No__c = 3;
        nstp2.Step_Status__c = 'Awaiting PC Confirmation';
        nstp2.Step_Type__c = 'PC Confirmation';
        newStepsList.add(nstp2);
        insert newStepsList;
        
        booking__c b = new booking__c();
        b.Deal_SR__c = sr.id;
        insert b;
        
        Booking_Unit__c bu = new Booking_Unit__c();
        bu.booking__c = b.id;        
        insert bu;
        CreateStepOnDPOverDue.createStepFromButton(sr.Id);
        CreateStepOnDPOverDue.createStep(new list<Id>{sr.Id});
        CreateStepOnDPOverDue.createStepFromButton(sr.Id);
        CreateStepOnDPOverDue.reinstatementDeal(sr.Id);
    }
    */
    static testmethod void m1(){
        string endpoint = '/services/apexrest/DAMACApprovalFactory/reinstatements';
        DAMAC_Approval_Factory_Test.initializeRestContext(null, endpoint);
        Map<String, String> params = new Map<String, String>();
        RestContext.request.requestBody = Blob.valueOf(JSON.serialize(params));
        test.startTest();
        NSIBPM__Service_Request__c sr = [Select Id, Reinstatement_Status__c, DP_Payment__c, Internal_Status__c From NSIBPM__Service_Request__c Limit 1];
        System.debug('>>>>>>request>>>>>>>>>'+sr);
        DAMAC_Approval_Factory.doPost();
        test.stopTest();
    }

    static testmethod void m2(){
        string endpoint = '/services/apexrest/DAMACApprovalFactory/reinstatementdetail';
        DAMAC_Approval_Factory_Test.initializeRestContext(null, endpoint);
        list<NSIBPM__Service_Request__c> requests = [Select id, NSIBPM__Contact__r.Name, OwnerId, Name, Owner.Name, 
                                                    List_of_Units__c, RM_Team__c, Agency__r.name, Deal_Rejected_Date__c, DP_Payment__c,
                                                    Rejection_Reason__c, Rejection_Comments__c, Total_Booking_Amount__c, Token_Amount_AED__c, Registration_Date__c,
                                                    (Select id, OwnerId, Owner.Name, Owner.Email, Service_Request__c, Change_Status__c, Step_No__c, Is_Closed__c, 
                                                    Step_Status__c, Step_Type__c, Comments__c, CreatedDate, lastmodifiedBy.name, lastModifiedDate  
                                                    from Steps__r where  Step_No__c >= 70  order by createdDate asc) 
                                                    from NSIBPM__Service_Request__c 
                                                    where Reinstatement_Status__c = 'In Progress' and DP_Payment__c = '< 50' 
                                                    and Internal_Status__c = 'HOS_APPROVED_POP'];
        Map<String, String> params = new Map<String, String>();
        params.put('requestId',requests[0].id);
        RestContext.request.requestBody = Blob.valueOf(JSON.serialize(params));
        test.startTest();
        DAMAC_Approval_Factory.doPost();
        test.stopTest();
    }

    static testmethod void m3(){
        string endpoint = '/services/apexrest/DAMACApprovalFactory/manageapproval';
        DAMAC_Approval_Factory_Test.initializeRestContext(null, endpoint);
        list<NSIBPM__Service_Request__c> requests = [Select id, NSIBPM__Contact__r.Name, OwnerId, Name, Owner.Name, 
                                                    List_of_Units__c, RM_Team__c, Agency__r.name, Deal_Rejected_Date__c, DP_Payment__c,
                                                    Rejection_Reason__c, Rejection_Comments__c, Total_Booking_Amount__c, Token_Amount_AED__c, Registration_Date__c,
                                                    (Select id, OwnerId, Owner.Name, Owner.Email, Service_Request__c, Change_Status__c, Step_No__c, Is_Closed__c, 
                                                    Step_Status__c, Step_Type__c, Comments__c, CreatedDate, lastmodifiedBy.name, lastModifiedDate  
                                                    from Steps__r where  Step_No__c >= 70  order by createdDate asc) 
                                                    from NSIBPM__Service_Request__c 
                                                    where Reinstatement_Status__c = 'In Progress' and DP_Payment__c = '< 50' 
                                                    and Internal_Status__c = 'HOS_APPROVED_POP'
                                                    order by createdDate desc];
        Map<String, String> params = new Map<String, String>();
        params.put('requestId',requests[0].id);
        params.put('stepId',requests[0].Steps__r[requests[0].Steps__r.size()-1].id);
        params.put('action','approved');
        params.put('comments','Its approved');

        RestContext.request.requestBody = Blob.valueOf(JSON.serialize(params));
        test.startTest();
        DAMAC_Approval_Factory.doPost();
        test.stopTest();
    }

    static testmethod void m4(){
        string endpoint = 'manageapproval';
        DAMAC_Approval_Factory_Test.initializeRestContext(null, endpoint);
        Map<String, String> params = new Map<String, String>();
        RestContext.request.requestBody = Blob.valueOf(JSON.serialize(params));
        test.startTest();
        DAMAC_Approval_Factory.doPost();
        test.stopTest();
    }

    public static void initializeRestContext(String body, String resourcePath) {
        RestContext.request = initializeRestRequest(body, resourcePath);
        RestContext.response = new RestResponse();        
    }

    public static RestRequest initializeRestRequest(String body, String resourcePath) {
        RestRequest request = new RestRequest();        
        request.resourcePath = resourcePath;
        request.requestURI = resourcePath;        
        if (body != null)
            request.requestBody = Blob.valueOf(body);            
        Map<String, String> headers = new Map<String, String> {};
        request.headers.putAll(headers);
        return request;
    }
    
}