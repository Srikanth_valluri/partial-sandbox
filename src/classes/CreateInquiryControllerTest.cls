/****************************************************************************************************
* Name          : CreateInquiryController Test class                                            *
* Description   :                                                                                   *
* Created Date  :                                                                                   *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE            COMMENTS                                                *
* 1.0   Craig Lobo          27-08-2018      Initial Draft                                           *
* 2.0   Bhanu Gupta         18-11-2018      Updated
****************************************************************************************************/

@isTest
public  class CreateInquiryControllerTest {

    @isTest 
    static void CreateInquiryControllerTest1() {
        Inquiry_Assignment_Rules__c assRules = new Inquiry_Assignment_Rules__c();
        insert assRules;

        Id devRecordTypeId1 
            = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
        Inquiry__c preInq = new Inquiry__c();
        preInq.Mobile_Phone_Encrypt__c = '05789088';
        preInq.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        preInq.Preferred_Language__c = 'English';
        preInq.First_Name__c = 'Test First name';
        preInq.Last_Name__c = 'Test Last name';
        preInq.Email__c = 'test@gmail.com';
        preInq.Inquiry_Source__c = 'Customer Referral';
        preInq.Mobile_Phone__c = '456789';
        preInq.RecordTypeId = devRecordTypeId1;
        preInq.Inquiry_Status__c = 'Active';
        preInq.Sales_Office__c = 'AKOYA';
        preInq.Meeting_Due_Date__c = system.now();
        preInq.Type_of_Property_Interested_in__c = 'Hotel';
        preInq.No_Of_Bed_Rooms_Availble_in_SF__c = '6';
        preInq.Completed__c = 'Yes';
        preInq.Budget__c = 'Below 500K';
        preInq.Project__c = 'DAMAC'; 
        preInq.Joint_Income_To_Be_Considered__c = 'Yes';
        preInq.Telesales_Executive__c = Userinfo.getUserId();
        preInq.Inquiry_Assignment_Rules__c = assRules.Id;
        insert preInq;

        ApexPages.StandardController controller = new ApexPages.StandardController(preInq);
        CreateInquiryController instance = new CreateInquiryController(controller);
        CreateInquiryController.getInquirySource();
        instance.saveReferralInquiry();
    }

    @isTest 
    static void CreateInquiryControllerTelesalesTest() {
         try{
            Profile profle = [SELECT Id FROM Profile WHERE Name LIKE '%Telesales%' LIMIT 1];
            User usr = new User(  firstname = 'testFName',
                            lastName = 'testLName',
                            email = 'testemail8297312@test.org',
                            Username = 'testsuername7887980@test.org',
                            EmailEncodingKey = 'ISO-8859-1',
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            Alias='t980j',
                            LanguageLocaleKey = 'en_US',
                            ProfileId = profle.Id
                         );
            insert usr;
        } catch (exception e){}
        try{
            User tslUser = [SELECT Id FROM User WHERE Profile.Name LIKE '%Telesales%' AND IsActive = TRUE LIMIT 1];
            System.runAs(tslUser){
                Inquiry_Assignment_Rules__c assRules = new Inquiry_Assignment_Rules__c();
                insert assRules;
        
                Id devRecordTypeId1 
                    = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
                Inquiry__c preInq = new Inquiry__c();
                preInq.Mobile_Phone_Encrypt__c = '05789088';
                preInq.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
                preInq.Preferred_Language__c = 'English';
                preInq.First_Name__c = 'Test First name';
                preInq.Last_Name__c = 'Test Last name';
                preInq.Email__c = 'test@gmail.com';
                preInq.Inquiry_Source__c = 'Customer Referral';
                preInq.Mobile_Phone__c = '456789';
                preInq.RecordTypeId = devRecordTypeId1;
                preInq.Inquiry_Status__c = 'Active';
                preInq.Sales_Office__c = 'AKOYA';
                preInq.Type_of_Property_Interested_in__c = 'Hotel';
                preInq.No_Of_Bed_Rooms_Availble_in_SF__c = '6';
                preInq.Completed__c = 'Yes';
                preInq.Budget__c = 'Below 500K';
                preInq.Project__c = 'DAMAC'; 
                preInq.Joint_Income_To_Be_Considered__c = 'Yes';
                preInq.Telesales_Executive__c = Userinfo.getUserId();
                preInq.Inquiry_Assignment_Rules__c = assRules.Id;
                insert preInq;
        
                ApexPages.StandardController controller = new ApexPages.StandardController(preInq);
                CreateInquiryController instance = new CreateInquiryController(controller);
                CreateInquiryController.getInquirySource();
                instance.saveReferralInquiry();
            }
        } catch(exception e){}
    }

    @isTest 
    static void CreateInquiryControllerUKTest() {
        try{
            Profile profle = [SELECT Id FROM Profile WHERE Name LIKE '%Property Consultant UK%' LIMIT 1];
            User usr = new User(  firstname = 'testFName',
                            lastName = 'testLName',
                            email = 'testemail8297312@test.org',
                            Username = 'testsuername7887980@test.org',
                            EmailEncodingKey = 'ISO-8859-1',
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            Alias='t980j',
                            LanguageLocaleKey = 'en_US',
                            ProfileId = profle.Id
                         );
            insert usr;
        } catch (exception e){}
        try{
            User tslUser = [SELECT Id FROM User WHERE Profile.Name LIKE '%Property Consultant UK%' 
                                                 AND IsActive = TRUE LIMIT 1];
            System.runAs(tslUser){
                Inquiry_Assignment_Rules__c assRules = new Inquiry_Assignment_Rules__c();
                insert assRules;
        
                Id devRecordTypeId1 
                    = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
                Inquiry__c preInq = new Inquiry__c();
                preInq.Mobile_Phone_Encrypt__c = '05789088';
                preInq.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
                preInq.Preferred_Language__c = 'English';
                preInq.First_Name__c = 'Test First name';
                preInq.Last_Name__c = 'Test Last name';
                preInq.Email__c = 'test@gmail.com';
                preInq.Inquiry_Source__c = 'Customer Referral';
                preInq.Mobile_Phone__c = '456789';
                preInq.RecordTypeId = devRecordTypeId1;
                preInq.Inquiry_Status__c = 'Active';
                preInq.Sales_Office__c = 'AKOYA';
                preInq.Type_of_Property_Interested_in__c = 'Hotel';
                preInq.No_Of_Bed_Rooms_Availble_in_SF__c = '6';
                preInq.Completed__c = 'Yes';
                preInq.Budget__c = 'Below 500K';
                preInq.Project__c = 'DAMAC'; 
                preInq.Joint_Income_To_Be_Considered__c = 'Yes';
                preInq.Telesales_Executive__c = Userinfo.getUserId();
                preInq.Inquiry_Assignment_Rules__c = assRules.Id;
                insert preInq;
        
                ApexPages.StandardController controller = new ApexPages.StandardController(preInq);
                CreateInquiryController instance = new CreateInquiryController(controller);
                CreateInquiryController.getInquirySource();
                instance.saveReferralInquiry();
            }
        } catch(exception e){}
    }
}