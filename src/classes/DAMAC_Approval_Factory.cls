/************************************************************************************************
 * @Name              : DAMAC_Approval_Factory
 * @Test Class Name   : DAMAC_Approval_Factory_Test
 * @Description       : Base Class to handle services for Approval App
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         Charan Vuyyuru 14/07/2020       Created
 ***********************************************************************************************/

@RestResource(urlMapping='/DAMACApprovalFactory/*')
global class DAMAC_Approval_Factory {
   
    @HttpPost
    global static void doPost(){
    
        RestContextHandler handler = new RestContextHandler(true);
        RestRequest req = handler.restRequest;
        
        try{
            System.debug('>>>>>requestURI>>>>>>>'+req.requestURI);
            String action = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
            System.debug('>>>>>action>>>>>>>'+action);

            Map<String, string> params = handler.getRequestParams();
            System.debug('>>>>>params>>>>>>>'+params); 

            //List of Reinstatements
            if(action.toLowerCase() == 'reinstatements')
                handler = DAMACApprovalUtils.getReinstatements();

            //Get Request Details
            if(action.toLowerCase() == 'reinstatementdetail')
                handler = DAMACApprovalUtils.getDetails(params.get('requestId'));
            
            //Manage Approval
            if(action.toLowerCase() == 'manageapproval')
                handler = DAMACApprovalUtils.handleApproval(params.get('stepId'), params.get('action'), params.get('comments'));
                
            handler.finalize();
        }
        catch (Exception exc) {
            system.debug('>>>>>>>>>>ERROR>>>>>>>>>>>' +exc.getmessage()+'>>>>>>LineNO>>>>>'+exc.getlinenumber());
            handler.response.success = false;
            handler.response.statusCode = WebServiceResponse.STATUS_CODE_SYSTEM_ERROR;
            handler.response.message = exc.getMessage()+'-'+exc.getLineNumber();
            handler.finalize(exc);
        }
    }
}