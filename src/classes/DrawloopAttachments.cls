@RestResource(urlMapping='/drawloopAttachment/*')
global without sharing class DrawloopAttachments {
    
    
    @HTTPPost
    global static void doPost() {
    
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        String reqBody = req.requestBody.toString();
        System.debug (reqBody);
        String sessionId =  reqBody.split ('####')[1];
        reqBody = reqBody.split ('####')[0];
        
        List<String> recordIds = new List<String>();
        if (reqBody.contains(','))
            recordIds = reqBody.split(',');
        else
            recordIds.add (reqBody);
        
        System.debug('listUnitDocumentIds--------->' + recordIds);
        System.enqueueJob(new DrawloopAttachmentQueueable (recordIds));

        
    }
    
    
   
}