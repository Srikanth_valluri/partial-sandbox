@isTest
private class BouncedChequeCallingHandlerTest {
  public static Id bouncedChequeCallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Bounced Cheque').RecordTypeId;
  
     @testSetup()
      private static  void testData(){
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        Calling_List__c CallInst = new Calling_List__c(RecordTypeId = bouncedChequeCallingRecordTypeId,
                                                 Property_Consultant__c = 'Eternus PC',
                                                 Manager_IPMS_Employee_ID__c = '589565',
                                                Call_Outcome__c = 'Recovery list',
                                                 Registration_ID__c = '12851',
                                                   Party_ID__c = '1177917');
         insert CallInst;
        
      }
      private static testMethod void testOnAfterUpdate(){
         Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, 
                       FirstName='Test FirstName1', 
                       LastName='Test LastName2', 
                       Type='Person', 
                       party_ID__C='123456',
                       Primary_CRE__c = null,
                       Tertiary_CRE__c = null,
                       Secondary_CRE__c = null );
        insert objAcc ;
        Calling_List__c bouncedChequeInst = new Calling_List__c(Account__c = objAcc.Id,
                             RecordTypeId = bouncedChequeCallingRecordTypeId,
                                                 Call_Outcome__c = '',
                                                 Registration_ID__c = '99830',
                                                 Party_ID__c = '123456'); 
        insert bouncedChequeInst;
        Calling_List__c  callInst = [SELECT Id,
                            Account__c,
                            RecordTypeId,
                            Call_Outcome__c,
                            Party_ID__c,
                            Registration_ID__c
                         FROM Calling_List__c
                        WHERE Registration_ID__c = '99830'];
       callInst.Call_Outcome__c = 'Incorrect Contact Details';
       update callInst;
      
       callInst.Call_Outcome__c = 'Not Reachable - Email sent for call back';
       update callInst;
       
        List<Calling_List__c>callingLstRec = [SELECT Id,Registration_ID__c, 
                                                  Call_Outcome__c
                                            FROM Calling_List__c
                                            WHERE RecordTypeId = :bouncedChequeCallingRecordTypeId
                                            AND  Registration_ID__c = '12851' limit 1];
       // for(Calling_List__c callInst : callingLstRec){
            callingLstRec[0].Call_Outcome__c = 'Legal/Dispute cases/DLD/RERA Related' ;
            update callingLstRec[0];
        //}
      }
}