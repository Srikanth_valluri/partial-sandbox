/*                                                        *
    * Description        : This is a trigger handler to update Account of related Booking.                                       *
    * Created Date       : 30/11/2017                                                                 *
    * Created By         : Swapnil Gholap                                                                      *
    */
    
    public without sharing class BuyerTriggerHandler {
        
        public void callInsertHelper(List<Buyer__c> lstNewBuyers){        
            System.debug('--BuyerTriggerHandler callInsertHelper---'+lstNewBuyers);
            new BuyerTriggerInsertHelper().helperMethod(lstNewBuyers);  
        }
        
        public void callUpdateHelper(Map<ID,Buyer__c> OldMapBuyers, List<Buyer__c> lstNewBuyers){        
            System.debug('--BuyerTriggerHandler callUpdateHelper---'+OldMapBuyers);
            new BuyerTriggerUpdateHelper().helperMethod(OldMapBuyers, lstNewBuyers);  
        }
        
    }