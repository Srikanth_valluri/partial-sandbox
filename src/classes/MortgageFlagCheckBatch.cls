// Batch Job for Processing the Records


global class MortgageFlagCheckBatch implements Database.Batchable<sObject>
                                                    , Database.Stateful
                                                    , Database.AllowsCallouts {
 
    
    global Database.QueryLocator start(Database.BatchableContext BC){
         
         String strQuery = 'SELECT Id,RecordType.DeveloperName,Mortgage_Flag__c,Status,CreatedDate,Age_Days__c,' +
                        ' Approving_Authorities__c,Roles_from_Rule_Engine__c,Submit_for_Approval__c,Manager_Id__c ' +
                        ' FROM Case' +
                        ' WHERE RecordType.Name = \'Mortgage\' ' +
                        ' AND Status = \'Submitted\' AND Mortgage_Flag__c = \'I\' ';


                    if( String.isNotBlank ( Label.FM_Service_Rec_Id_For_Test ) 
                    && Label.FM_Service_Rec_Id_For_Test.contains('On') 
                    && String.isNotBlank( Label.FM_Service_Rec_Id_For_Test.substringAfter('-') ) ) {
                        strQuery = strQuery + ' AND Id = \''+Label.FM_Service_Rec_Id_For_Test.substringAfter('-')+ '\'  ';
                    }else{
                        strQuery = strQuery + ' AND (Age_Days__c >= 90 AND Age_Days__c < 100) AND Extended__c = false';
                    }
                

         System.debug('strQuery == ' + strQuery);
         return Database.getQueryLocator(strQuery);
    }
    
    global void execute(Database.BatchableContext BC, List<Case > scope){ 
        for(Case caseObj : scope){
                    /*
            caseObj.Approving_Authorities__c = 'Manager';
            caseObj.Roles_from_Rule_Engine__c = 'Manager';
            caseObj.Submit_for_Approval__c = true;
            */
                        
            //user objuser = [SELECT Id, ManagerId,DelegatedApproverId FROM User where Id =: lstCase[0].OwnerId LIMIT 1];  
            System.debug('caseObj.Manager_Id__c == ' + caseObj.Manager_Id__c );    
            if (caseObj.Manager_Id__c != NULL) {
                
                Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                req1.setComments('Approval for Mortgage Extension ');
                req1.setObjectId(caseObj.id);
                req1.setNextApproverIds(new Id[] {caseObj.Manager_Id__c});
                req1.setProcessDefinitionNameOrId('MortageApproval');
                req1.setSkipEntryCriteria(true);
               
                Approval.ProcessResult result = Approval.process(req1);
                
            }
            
            

        }

        //update scope;
    }//End Execute Method
  
    global void finish(Database.BatchableContext BC){
        System.debug('***FMAmountBUEmailNotifierBatch finish****');
        
    }
}