/**********************************************************************************************************************
Description: This API is used for creating FM-Receipt records post successfull payment for 'Service Charge'/'Other' Payments
from Damac living App
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   30-11-2020      | Shubham Suryawanshi | Created initial draft
1.1     |   10-12-2020      | Shubham Suryawanshi | Removed accountId's compulsion & added receipt creation logic for Guest + Logged user scanrios
1.2     |   20-12-2020      | Shubham Suryawanshi | Populated Booking Unit for both Guest & Logged-In user
1.3     |   26-01-2020      | Shubham Suryawanshi | Added methods for getting file data from receipt url & create attacment under FM_Receipts__c
***********************************************************************************************************************/
@RestResource(urlMapping='/createFmReceipt/*')
global class HDApp_createFMReceipt_API {
    
    public static String errorMsg;
    public static Integer statusCode;
    public static String exceptionMsg;
    //public  static final String FM_GUEST_PAYMENT_APP = Label.FMGuestPaymentApp;
    public  static final String FM_GUEST_PAYMENT_APP = Label.FMGuestPayment;
    public  static final String OTHERS = Label.Others;
    public  static final String FM_PORTAL_SERVICE_CHARGE = Label.FMPortalServiceCharge;
    
    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };
    
    @HttpPost
    global static FinalReturnWrapper createFmReceipt() {
        RestRequest req = RestContext.request;
        Blob body = req.requestBody;
        String jsonString = body.toString();
        System.debug('jsonString:'+jsonString);
        System.debug('Request params:'+req.params);
        
        FinalReturnWrapper returnResponse = new FinalReturnWrapper();

        PaymentDetailsWrapper objCompleteWrap = new PaymentDetailsWrapper();
        PaymentWrapper objPayWrap = new PaymentWrapper(); 
        cls_meta_data objMeta = new cls_meta_data();
        cls_data objData = new cls_data();
        
        if(String.isBlank(jsonString)) {
            objMeta = ReturnMetaResponse('No JSON body found in request', '', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        
        System.debug('jsonString:'+jsonString);
        
        objCompleteWrap = (PaymentDetailsWrapper)JSON.deserialize(jsonString,PaymentDetailsWrapper.class);
        //objPayWrap = (PaymentWrapper)JSON.deserialize(jsonString,PaymentWrapper.class);
        if(objCompleteWrap != null) {
            objPayWrap = objCompleteWrap.payment_details;
        }
        System.debug('objPayWrap:'+objPayWrap);
        
        /*Commenting below to allow AccountId as Null also, as per Quick Pay's requirement (discussed with Charith)*/
        //if(objPayWrap.account_id == NULL) {
        //    objMeta = ReturnMetaResponse('Missing parameter : accountId','', 3);
        //    returnResponse.meta_data = objMeta;
        //    return returnResponse;
        //}
        //else if(String.isBlank(objPayWrap.account_id)) {
        //    objMeta = ReturnMetaResponse('Missing parameter value : accountId','', 3);
        //    returnResponse.meta_data = objMeta;
        //    return returnResponse;
        //}

        if(objPayWrap.is_guest_payment == null) {
            objMeta = ReturnMetaResponse('Missing parameter : is_guest_payment','', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        /*Commenting below if-else check. Need to get account from Booking Unit only - as discussed with Charith*/
        //if(objPayWrap.is_guest_payment == false && objPayWrap.account_id == NULL) {
        //    objMeta = ReturnMetaResponse('Missing parameter : accountId','', 3);
        //    returnResponse.meta_data = objMeta;
        //    return returnResponse;
        //}
        //else if( objPayWrap.is_guest_payment == false && String.isBlank(objPayWrap.account_id)) {
        //    objMeta = ReturnMetaResponse('Missing parameter value : accountId','', 3);
        //    returnResponse.meta_data = objMeta;
        //    return returnResponse;
        //}
        if(objPayWrap.is_guest_payment == false && !objPayWrap.payment_type.equalsIgnoreCase('Service Charges')) {
            objMeta = ReturnMetaResponse('Invalid payment_type passed','', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        if(objPayWrap.is_guest_payment == false && (objCompleteWrap.paid_invoice_details == null || objCompleteWrap.paid_invoice_details.size() < 1) ) {
            objMeta = ReturnMetaResponse('No paid_invoice_details passed in request','', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }


        if(objPayWrap.reg_id == NULL) {
            objMeta = ReturnMetaResponse('Missing parameter : reg_id','', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(String.isBlank(objPayWrap.reg_id)) {
            objMeta = ReturnMetaResponse('Missing parameter value : reg_id','', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(objPayWrap.amount == NULL) {
            objMeta = ReturnMetaResponse('Missing parameter : amount','', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        
        /*Commenting below to allow AccountId as Null also, as per Quick Pay's requirement (discussed with Charith)*/
        //if(String.isNotBlank(objPayWrap.account_id)) {
        //    List<User> usr = [SELECT Id,Name,Contact.AccountId,Profile.Name 
        //                      FROM User
        //                      WHERE Contact.AccountId =: objPayWrap.account_id];
        //    System.debug('usr:'+usr);
        //    if(usr.isEmpty()) {
        //        objMeta = ReturnMetaResponse('No valid user for the accountId','', 3);
        //        returnResponse.meta_data = objMeta;
        //        return returnResponse;
        //    }
        //}
        //else {
        //    objMeta = ReturnMetaResponse('No account_id passed in request body','', 3);
        //    returnResponse.meta_data = objMeta;
        //    return returnResponse;
        //}
        
        List<Booking_Unit__c> lstBookingUnit = getBU(objPayWrap.reg_id);
        if(lstBookingUnit.isEmpty() || lstBookingUnit.size() < 1 ) {
            objMeta = ReturnMetaResponse('No booking unit found for given reg_id','', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        System.debug('objPayWrap.other_payment_type:: ' + objPayWrap.other_payment_type+'*');
        Boolean success = false;
        FM_Receipt__c fmReceipt = new FM_Receipt__c();
        
        try {
            Booking_Unit__c bookingUnit = (lstBookingUnit.isEmpty() ? NULL : lstBookingUnit[0]);
            
            fmReceipt.isHelloDamacAppCase__c = true;
            fmReceipt.Account__c = (bookingUnit == NULL ? NULL : lstBookingUnit[0].Booking__r.Account__c);
            //IF-Else as per Guest/Logged-In user
            if(!objPayWrap.is_guest_payment) {
                fmReceipt.Payment_Type__c = FM_PORTAL_SERVICE_CHARGE;
            }
            else {
                fmReceipt.Payment_Type__c = FM_GUEST_PAYMENT_APP;
                fmReceipt.Guest_Payment_Type__c = objPayWrap.payment_type;
                fmReceipt.Guest_Other_Payment_Type__c = OTHERS.equalsIgnoreCase(objPayWrap.payment_type) ? objPayWrap.other_payment_type : NULL;
            }
            
            fmReceipt.Amount__c = objPayWrap.amount;
            fmReceipt.Billing_Name__c = objPayWrap.billing_name;
            fmReceipt.Billing_Email__c = objPayWrap.billing_email;
            fmReceipt.Billing_Phone__c = objPayWrap.billing_phone_code + ' ' + objPayWrap.billing_phone;
            fmReceipt.Billing_Address__c = objPayWrap.billing_address;
            fmReceipt.Billing_City__c = objPayWrap.billing_city;
            fmReceipt.Billing_State__c = objPayWrap.billing_state;
            fmReceipt.Billing_Country__c = objPayWrap.billing_country;
            fmReceipt.Billing_Zip__c = objPayWrap.billing_zip;
            fmReceipt.Booking_Unit__c = bookingUnit.Id;    
            fmReceipt.CustomerAcceptedTermsNConditions__c = true;
            fmReceipt.CustomerEmail__c = (bookingUnit == NULL ? NULL : (bookingUnit.Booking__r.Account__r.IsPersonAccount ? bookingUnit.Booking__r.Account__r.Email__pc : bookingUnit.Booking__r.Account__r.Email__c));
            fmReceipt.Acknowledgement_Id__c = objPayWrap.bank_acknowledgement_id;
            fmReceipt.Bank_Receipt_Number__c = objPayWrap.bank_receipt_number;
            fmReceipt.Bank_Reference_Number__c = objPayWrap.bank_reference_number;
            fmReceipt.Card_Holder_Name__c = objPayWrap.card_holder_name;
            fmReceipt.Card_Name__c = objPayWrap.card_name;
            fmReceipt.Currency__c = objPayWrap.payment_currency;
            fmReceipt.ECI_Value__c = objPayWrap.eci_value;
            fmReceipt.Order_Number__c = objPayWrap.order_number;
            fmReceipt.Order_Status__c = objPayWrap.order_status;
            fmReceipt.Payment_Mode__c = objPayWrap.payment_mode;
            fmReceipt.IpmsReceiptUrl__c = objPayWrap.ipms_receipt_url;

            fmReceipt.Status_Code__c = objPayWrap.status_code;
            fmReceipt.Status_Message__c = objPayWrap.status_message;
            fmReceipt.Tracking_Id__c = objPayWrap.tracking_id;
            fmReceipt.Failure_Message__c = objPayWrap.failure_message;
            fmReceipt.Bank_QSI_Number__c = objPayWrap.bank_qsi_number;
            fmReceipt.Merchant_Amount__c = objPayWrap.merchant_amount != null ? objPayWrap.merchant_amount : null;
            /*Confirm on below*/
            fmReceipt.Receipt_created_in_IPMS__c = String.isNotBlank(objPayWrap.ipms_receipt_url) ? 'TRUE' : null;
            
            /*If NO IPMS receipt Number received in request then mark - Call_IPMS_Receipt_Creation__c as True to include it in batch for IPMS receipt creation*/
            if(String.isNotBlank(objPayWrap.ipms_receipt_number)) {
                fmReceipt.IPMS_Receipt_Number__c = objPayWrap.ipms_receipt_number;
            }
            else {
                fmReceipt.Call_IPMS_Receipt_Creation__c = true;
            }
            
            insert fmReceipt;

            if(!objPayWrap.is_guest_payment && fmReceipt.Id != null && (objCompleteWrap.paid_invoice_details != null || objCompleteWrap.paid_invoice_details.size()  > 0)) {
                List<FM_Unit_Invoice_Payment__c> lstUnitInvoicePayment = new List<FM_Unit_Invoice_Payment__c>();

                Set<String> setRegIds = new Set<String>();
                Map<String, Booking_Unit__c> mapRegIdToBU = new Map<String, Booking_Unit__c>();
                for(InvoiceDetailsWrapper objInvWrap : objCompleteWrap.paid_invoice_details) {
                    setRegIds.add(objInvWrap.registration_id);
                }
                if(setRegIds.size() > 0 ) {
                    for(Booking_Unit__c objBU : [SELECT Id
                                                      , Unit_Name__c
                                                      , Registration_ID__c
                                                      , Booking__r.Account__c 
                                                      , Booking__r.Account__r.Email__pc
                                                      , Booking__r.Account__r.Email__c
                                                      , Booking__r.Account__r.IsPersonAccount
                                                 FROM Booking_Unit__c
                                                 WHERE Registration_ID__c IN: setRegIds]) {

                        if(!mapRegIdToBU.containsKey(objBU.Registration_ID__c)) {
                            mapRegIdToBU.put(objBU.Registration_ID__c, objBU);
                        }
                    }
                }
                else {
                    objMeta = ReturnMetaResponse('No registration_id passed in paid_invoice_details','', 3);
                    returnResponse.meta_data = objMeta;
                    return returnResponse;   
                }
                System.debug('mapRegIdToBU:: ' + mapRegIdToBU);
                if(mapRegIdToBU.isEmpty()) {
                    objMeta = ReturnMetaResponse('No Booking Unit(s) found for registration_id passed in paid_invoice_details','', 3);
                    returnResponse.meta_data = objMeta;
                    return returnResponse;    
                }

                //Populating - FM_Unit_Invoice_Payment__c details
                for(InvoiceDetailsWrapper objInvWrap : objCompleteWrap.paid_invoice_details) {
                    FM_Unit_Invoice_Payment__c objUnitInvoice = new FM_Unit_Invoice_Payment__c();
                    objUnitInvoice.FM_Receipt__c = fmReceipt.Id;
                    objUnitInvoice.Booking_Unit__c = mapRegIdToBU.containsKey(objInvWrap.registration_id) ? mapRegIdToBU.get(objInvWrap.registration_id).Id : null ;
                    objUnitInvoice.Name = objInvWrap.invoice_number;
                    objUnitInvoice.Call_Type__c = objInvWrap.call_type;

                    //Datetime.newInstance(objFM.Move_in_date__c.year(), objFM.Move_in_date__c.month(), objFM.Move_in_date__c.day()).format('yyyy-MM-dd')
                    objUnitInvoice.Creation_Date__c = String.isBlank(objInvWrap.creation_date) ? NULL : Date.valueOf(objInvWrap.creation_date);
                    objUnitInvoice.Due_Date__c = String.isBlank(objInvWrap.due_date) ? NULL : Date.valueOf(objInvWrap.due_date);
                    objUnitInvoice.Party_Id__c = objInvWrap.party_id;
                    objUnitInvoice.Party_Name__c = objInvWrap.party_name;
                    objUnitInvoice.Payment_Amount__c = objInvWrap.due_remaining;
                    objUnitInvoice.Project_Name__c = objInvWrap.project_name;
                    objUnitInvoice.Registration_Id__c = objInvWrap.registration_id;
                    objUnitInvoice.TRX_Number__c = objInvWrap.trx_number;
                    objUnitInvoice.TRX_Type__c = objInvWrap.trx_type;
                    //TRX_Id__c = objInvWrap.trx_id;
                    objUnitInvoice.Unit_Name__c = objInvWrap.unit_name;

                    lstUnitInvoicePayment.add(objUnitInvoice);
                }

                if(lstUnitInvoicePayment.size() > 0) {
                    insert lstUnitInvoicePayment;
                }

            }
            
            success = true;
        }
        catch(Exception e) {
            System.debug('Exception:'+e.getMessage());
            exceptionMsg = e.getMessage();
        }
        
        objData.fm_receipt_id = (fmReceipt != NULL) ? fmReceipt.Id : NULL;
        if(String.isNotBlank(fmReceipt.Id)) {
            List<FM_Receipt__c> lstFmRecp = [SELECT id 
                                                  , Name
                                                  , IpmsReceiptUrl__c
                                                  , Payment_Type__c 
                                             FROM FM_Receipt__c 
                                             WHERE id =: fmReceipt.Id];
            objData.fm_receipt_number = (lstFmRecp.size() > 0) ? lstFmRecp[0].Name : NULL;
            if(lstFmRecp.size() > 0) {
                processAttachmentCreation(JSON.serialize(lstFmRecp[0]));
            }
        }
        
        
        if(success) {
            errorMsg = 'Successful';
            statusCode = 1;
        }
        else {
            errorMsg = 'Unable to create payment receipt';
            statusCode = 2;
        }
        
        System.debug('errorMsg:'+errorMsg);
        System.debug('statusCode:'+statusCode);
        System.debug('exceptionMsg:'+exceptionMsg);
        objMeta = ReturnMetaResponse(errorMsg, exceptionMsg, statusCode);
        
        returnResponse.data = objData;
        returnResponse.meta_data = objMeta;
        System.debug('returnResponse:'+returnResponse);
        
        return returnResponse;
    }

    public static List<Booking_Unit__c> getBU(String regId) {
        List<Booking_Unit__c> lstBU =  [SELECT Id
                                             , Unit_Name__c
                                             , Registration_ID__c
                                             , Booking__r.Account__c 
                                             , Booking__r.Account__r.Email__pc
                                             , Booking__r.Account__r.Email__c
                                             , Booking__r.Account__r.IsPersonAccount
                                        FROM Booking_Unit__c
                                        WHERE Registration_ID__c =: regId
                                        LIMIT 1];
        return lstBU;
    }

    /**********************************************************************************************************************
    Method Name : getFileDataFromIPMSCallout
    Description : Method for IPMS callout to get file data for given fileURL
    Parameter(s):  fileURL
    Return Type : HttpResponse
    **********************************************************************************************************************/
    public static HttpResponse getFileDataFromIPMSCallout(String fileURL) {
        System.debug('fileURL: ' + fileURL);
        if(String.isBlank(fileURL)) {
            return null;
        }
        HttpRequest req = new HttpRequest();
        req.setEndpoint(fileURL);
        req.setHeader('Accept', 'application/json');
        req.setMethod('GET');
        //req.setHeader('Authorization','Bearer' + accessToken);
        req.setTimeout(120000);
        HttpResponse response = new HttpResponse();
        if(!Test.isRunningTest()) {
            response = new Http().send(req);
        }
        else {
            String myString = 'StringToBlob';
            Blob myBlob = Blob.valueof(myString);
            response.setStatusCode(200);
            response.setBodyAsBlob(myBlob);
        }
        System.debug('response: ' + response);
        System.debug('Status Code = ' + response.getStatusCode());
        System.debug('Pdf Body = ' + response.getBodyAsBlob());
        return response;
    }

    /**********************************************************************************************************************
    Method Name : processAttachmentCreation
    Description : Method to get file data from IPMS for given Receipt URL & carry further process
    Parameter(s):  fmReceiptId
    Return Type : NA
    **********************************************************************************************************************/
    @future(callout=true)
    public static void processAttachmentCreation(String strFmReceipt) {
        
        FM_Receipt__c fmReceipt = new FM_Receipt__c();
        fmReceipt = String.isNotBlank(strFmReceipt) ? (FM_Receipt__c)JSON.deserialize(strFmReceipt, FM_Receipt__c.class) : null;

        if( fmReceipt != null ) {
            HttpResponse calloutResponse = getFileDataFromIPMSCallout(fmReceipt.IpmsReceiptUrl__c);

            if(calloutResponse != null) {
                list<UploadMultipleDocController.MultipleDocRequest> lstWrapper = new list<UploadMultipleDocController.MultipleDocRequest>();
                UploadMultipleDocController.data objResponse = new UploadMultipleDocController.data();
                if( calloutResponse.getStatusCode() == 200 && calloutResponse.getBodyAsBlob() != null ) {
                    String requestString = EncodingUtil.Base64Encode(calloutResponse.getBodyAsBlob());
                    if(string.isNotBlank( requestString ) ) {
                        lstWrapper.add( FM_Utility.makeWrapperObject(  requestString,
                                                                    fmReceipt.Name + ' Receipt' ,
                                                                    fmReceipt.Name + ' Receipt.pdf' ,
                                                                    fmReceipt.Name, '1' ) );
                    }
                    System.debug('== lstWrapper =='+lstWrapper);
                    //Inserting SR Attachment
                    if( !lstWrapper.isEmpty() ) {
                        objResponse = PenaltyWaiverService.uploadDocumentsOnCentralRepo( lstWrapper, 'FM' );
                        system.debug('== objResponse document upload =='+objResponse);
                        system.debug('== objResponse.data =='+objResponse.data);
                        if( objResponse != NULL && objResponse.data != NULL ) {
                            list<SR_Attachments__c> objSRAttach = insertCustomAttachment( fmReceipt, fmReceipt.Name + '.pdf', objResponse, fmReceipt.Name + '.pdf');
                        }
                    }
                }
            }
        }                                       
    }
    /**********************************************************************************************************************
    Method Name : insertCustomAttachment
    Description : Method to create SR_Attachment__c record for given FM_Receipt
    Parameter(s):  FM_Receipt__c fmReceipt, String docName , UploadMultipleDocController.data objResponse, String fileName
    Return Type : list<SR_Attachments__c>
    **********************************************************************************************************************/
    public static list<SR_Attachments__c> insertCustomAttachment( FM_Receipt__c fmReceipt, String docName , UploadMultipleDocController.data objResponse, String fileName) {
        
        list<SR_Attachments__c> lstCustomAttachments = new list<SR_Attachments__c>();
        System.debug('fileName:: ' + fileName);

        String strFileExtension;
        if( String.isNotBlank( fileName ) && fileName.lastIndexOf('.') != -1 ) {
            strFileExtension = fileName.substring( fileName.lastIndexOf('.')+1 , fileName.length() );
            strFileExtension = strFileExtension.substringBefore('&');
        }
        System.debug('strFileExtension:: ' + strFileExtension);
        
        for( UploadMultipleDocController.MultipleDocResponse objFile : objResponse.data ) {
            SR_Attachments__c objCustAttach = new SR_Attachments__c();
            //objCustAttach.Account__c = strAccountId ;
            objCustAttach.Name = docName;
            objCustAttach.Attachment_URL__c = objFile.url;
            //objCustAttach.Booking_Unit__c = strSelectedUnit ;
            objCustAttach.FM_Receipt__c = fmReceipt.Id ;
            objCustAttach.Type__c = strFileExtension;
            lstCustomAttachments.add(objCustAttach);
        }

        System.debug('lstCustomAttachments:: ' + lstCustomAttachments);
        if( !lstCustomAttachments.isEmpty() ) {
            try {
                insert lstCustomAttachments;
            }
            catch(Exception e) {
                System.debug('Exception e: ' + e.getMessage());
                errorMsg = e.getMessage();
            }
        }
        System.debug('lstCustomAttachments:: ' + lstCustomAttachments);
        return lstCustomAttachments;
    }

    public class PaymentDetailsWrapper {
        public PaymentWrapper payment_details;
        public InvoiceDetailsWrapper[] paid_invoice_details;
    }
    
    public class PaymentWrapper {
        public boolean is_guest_payment;
        public String account_id;
        //public String booking_unit_id;
        public String reg_id;
        public String payment_type;
        public String other_payment_type;
        public Decimal amount;
        public String billing_name;
        public String billing_address;
        public String billing_city;
        public String billing_country;
        public String billing_email;
        public String billing_phone_code;
        public String billing_phone;
        public String billing_state;
        public String billing_zip;
        public String bank_acknowledgement_id;
        public String bank_receipt_number;
        public String bank_reference_number;
        public String card_holder_name;
        public String card_name;
        public String payment_currency;
        public String eci_value;
        public String order_number;
        public String order_status;
        public String payment_mode;
        public String ipms_receipt_url;
        public String ipms_receipt_number;

        public String status_code;
        public String status_message;
        public String tracking_id;
        public String failure_message;
        public String bank_qsi_number;
        public Decimal merchant_amount;
    }

    public class InvoiceDetailsWrapper {
        public String invoice_number;
        public String call_type;
        public String creation_date;
        public String due_date;
        public String party_id;
        public String party_name;
        public Decimal due_remaining;
        public String project_name;
        public String registration_id;
        public String trx_number;
        public String trx_type;
        public String trx_id;
        public String unit_name;
    }
    
    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }
    
    public static cls_meta_data ReturnMetaResponse(String message, String devMsg, Integer statusCode) {
        cls_meta_data retMeta = new cls_meta_data();
        retMeta.message = message;
        retMeta.status_code = statusCode;
        retMeta.title = mapStatusCode.get(statusCode);
        retMeta.developer_message = devMsg;
        return retMeta;
    }
    
    public class cls_data {
        public String fm_receipt_id;
        public String fm_receipt_number;
    }
    
    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message;   
    }
}


/*
Rough INPUT JSON :

{
    "payment_details" : {
        "account_id" : "0010Y00000Ma7ne",
        "reg_id" : "24057",
        "payment_type" : "Service Charges",
        "other_payment_type" : "",
        "amount" : 10,
        "bank_acknowledgement_id" : "123",
        "bank_receipt_number" : "034121423847",
        "bank_reference_number" : "249529",
        "billing_address" : "dubai , deira",
        "billing_city" : "dubai",
        "billing_country" : "United Arab Emirates",
        "billing_email" : "test@test.com",
        "billing_name" : "test test",
        "billing_phone" : "2132124",
        "billing_state" : "dubai",
        "billing_zip" : "000001",
        "card_holder_name" : "Test",
        "card_name" : "Test",
        "payment_currency" : "AED",
        "eci_value" : "05",
        "order_number" : "FM-RECEIPT-058901",
        "order_status" : "Success",
        "payment_mode" : "Credit card",
        "ipms_receipt_url" : "test@payment.com",
        "ipms_receipt_number" : "12354",
        "status_code" : "00",
        "status_message" : "Approved",
        "tracking_id" : "109016213169",
        "failure_message" : "",
        "bank_qsi_number" : "30000017771",
        "merchant_amount" : 10
    },
    "paid_invoice_details" :[
        {
            "invoice_number" : "7870423",
            "call_type" : "Master Community Levy",
            "creation_date" : "2020-03-03",
            "due_date" : "2020-03-31",
            "party_id" : "151579",
            "party_name" : "Test Test",
            "due_remaining" : 12.00,
            "project_name" : "Test",
            "registration_id" : "24057",
            "trx_number" : "242",
            "trx_type" : "INV",
            "trx_id" : "2124",
            "unit_name" : "BD4/10/1001"
        }
    ]
}


IPMS = https://ptctest.damacgroup.com/COFFEE/apex/document/view/4449cd5782229965134f6e15a82cdcd7
*/