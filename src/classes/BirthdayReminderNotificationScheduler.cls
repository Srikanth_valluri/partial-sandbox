/**************************************************************************************************
* Name               : BirthdayReminderNotificationScheduler
* Description        : Scheduler class for BirthdayReminderNotificationBatch class.
* Created Date       : 17/11/2020
* Created By         : QBurst
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst           17/11/2020      Initial Draft.
**************************************************************************************************/
public class BirthdayReminderNotificationScheduler implements Schedulable {
  public void execute(SchedulableContext SC) {
    BirthdayReminderNotificationBatch reminderBatch = new BirthdayReminderNotificationBatch();
    Database.executeBatch(reminderBatch); 
  }
}// End of class.