/*
    20-05-2019  Arjun Khatri        Added 'Service Charge Reset' Process logic
    17-06-2019  Arjun Khatri        Added 'Overdue Rebate/Discount' Process logic
    27-06-2019  Arjun Khatri        Added 'Rebate On Advance' Process logic
    12-09-2019  Aishwarya Todkar    Added 'Riyadh Rotana Conversion' Process logic
    12-01-2020  Aishwarya Todkar    Added 'Waiver' Process logic
    27/02/2020  Arjun Khatri        Added logic for Case Summary - Client Relation process SR.
*/
public class ProceedCasecontroller {
    Case caseobj;
    //Boolean createPenaltyWaiver;
    public ProceedCasecontroller(ApexPages.StandardController controller)
    {
        caseobj= (Case)controller.getrecord();
        //createPenaltyWaiver = true;
        caseobj = [ Select Id, Accountid, Seller__c, RecordType.DeveloperName,Status,Booking_Unit__c,CreatedDate from Case where id = :caseobj.id limit 1];
    }

    public Pagereference redirectToHomePage()
    {
        PageReference pg = null;

        system.debug('== RecordType =='+caseobj.RecordType.DeveloperName);
        system.debug('== caseobj =='+caseobj);
        system.debug('== caseobj accountId =='+caseobj.Accountid);
        system.debug('== caseobj Seller =='+caseobj.Seller__c);
        if( caseobj != null )
        {
            if(caseobj.RecordType.DeveloperName != null)
            {
                if(!caseobj.RecordType.DeveloperName.contains('Assignment')){
                    if(caseobj.Accountid != null){
                        if(caseobj.RecordType.DeveloperName.equalsIgnoreCase('Penalty_Waiver'))
                        {
                            system.debug('== Inside the penalty waiver page reference == ');
                            pg = callCREPortalHomeForPenalty(caseobj.Accountid,caseobj.Id);
                        }
    
                        else if(caseobj.RecordType.DeveloperName.equalsIgnoreCase('AOPT') || caseobj.RecordType.DeveloperName.equalsIgnoreCase('Early_Settlement'))
                        {
                            String strSRType = caseobj.RecordType.DeveloperName.equalsIgnoreCase('AOPT') ? 'AOPT' : 'EarlySettlement';
                            pg = callCREPortalHomeForAOPT(caseobj.Accountid, caseobj.Id, strSRType);
                        }
                        
                        else if(caseobj.RecordType.DeveloperName.equalsIgnoreCase('Parking'))
                        {
                            pg = callCREPortalHomeForParking(caseobj.Accountid,caseobj.Id);
                        }
    
                        else if(caseobj.RecordType.DeveloperName.equalsIgnoreCase('Promotions'))
                        {
                            pg = callCREPortalHomeForPromotions(caseobj.Accountid,caseobj.Id);
                        }
                        
    
                        else if(caseobj.RecordType.DeveloperName.equalsIgnoreCase('Proof_of_Payment_SR'))
                        {
                            pg = callCREPortalPOP(caseobj.Accountid,caseobj.Id);
                        }
                        else if(caseobj.RecordType.DeveloperName.equalsIgnoreCase('Mortgage'))
                        {
                            pg = callCREMortgage(caseobj.Accountid,caseobj.Id,caseobj.Status,caseobj.Booking_Unit__c);
                        }
                        else if(caseobj.RecordType.DeveloperName.equalsIgnoreCase('NOC_For_Visa'))
                        {
                            pg = callCREPortalNOC(caseobj.Accountid,caseobj.Id);
                        }
                        else if(caseobj.RecordType.DeveloperName.equalsIgnoreCase('Token_Refund') ||
                           caseobj.RecordType.DeveloperName.equalsIgnoreCase('Customer_Refund') ||
                           caseobj.RecordType.DeveloperName.equalsIgnoreCase('Refund_Liability'))
                        {
                            pg = callCREPortalHomeRefunds(caseobj.Accountid,caseobj.Id);
                        }
                        //Redirect to fund transfer page if Case record type is Fund Transfer
                        else if(caseobj.RecordType.DeveloperName.equalsIgnoreCase('Fund_Transfer'))
                        {
                            pg = callCREFundTransfer(caseobj.Accountid,caseobj.Id);
                        }
                        //Redirect to fund transfer page if Case record type is Fund Transfer
                        else if(caseobj.RecordType.DeveloperName.equalsIgnoreCase('Title_Deed'))
                        {
                            pg = callCRETitleDeed(caseobj.Accountid,caseobj.Id);
                        }
                        else if(caseobj.RecordType.DeveloperName.equalsIgnoreCase('Fund_Transfer_Active_Units'))
                        {
                            pg = callCREFundTransferActiveUnits(caseobj.Accountid,caseobj.Id);
                        }
                        
                        else if(caseobj.RecordType.DeveloperName.equalsIgnoreCase('Rental_Pool_Agreement'))
                        {
                            pg = callCRERentalPoolAgreement(caseobj.Accountid,caseobj.Id);
                        }
                        else if(caseobj.RecordType.DeveloperName.equalsIgnoreCase('Rental_Pool_Agreement_Early_Handover'))
                        {
                            pg = callCRERentalPoolAgreementEHO(caseobj.Accountid,caseobj.Id);
                        }
                         else if(caseobj.RecordType.DeveloperName.equalsIgnoreCase('Rental_Pool_Termination'))
                        {
                            pg = callCRERentalPoolTermination(caseobj.Accountid,caseobj.Id);
                        }
                        
                        else if(caseobj.RecordType.DeveloperName.equalsIgnoreCase('Plot_Handover'))
                        {
                            pg = callCREPlotHandover(caseobj.Accountid,caseobj.Id);
                        }
                        
                        else if(caseobj.RecordType.DeveloperName.equalsIgnoreCase('Cheque_Replacement'))
                        {
                            pg = callCREChequeReplacement(caseobj.Accountid,caseobj.Id);
                        }
                        //Redirect to COCD page if Case record type is COCD
                        else if(caseobj.RecordType.DeveloperName.equalsIgnoreCase('Change_of_Details') ||
                            caseobj.RecordType.DeveloperName.equalsIgnoreCase('Change_of_Joint_Buyer') ||
                            caseobj.RecordType.DeveloperName.equalsIgnoreCase('Name_Nationality_Change') ||
                            caseobj.RecordType.DeveloperName.equalsIgnoreCase('Passport_Detail_Update')
                            )
                        {
                            pg = callCRECOD(caseobj.Accountid,caseobj.Id, caseobj.RecordType.DeveloperName,caseobj.Status);
                        } else if (
                                caseobj.RecordType.DeveloperName.equalsIgnoreCase('Early_Handover')                                
                        ) {
                            pg = callLandingPage(caseobj.Accountid, caseobj.Id, caseobj.RecordType.DeveloperName);
                        } else if (caseobj.RecordType.DeveloperName.equalsIgnoreCase('Handover')) {
                            Pg = callEarlySetHO(caseobj.Id);
                        }  else if (caseobj.RecordType.DeveloperName.equalsIgnoreCase('Lease_Handover')) {
                            pg = callLHO(caseobj.Accountid, caseobj.Id, caseobj.RecordType.DeveloperName);
                        }  else if(caseobj.RecordType.DeveloperName.equalsIgnoreCase('Complaint'))
                        {
                            pg = callCREComplaints(caseobj.Accountid,caseobj.Id);
                        }
                        else if(caseobj.RecordType.DeveloperName.equalsIgnoreCase('Service_Charge_Reset')) {
                            pg = callCREServiceChargeReset(caseobj.Accountid,caseobj.Id);
                        }
                        else if(caseobj.RecordType.DeveloperName.equalsIgnoreCase('Overdue_Rebate_Discount')) {
                            pg = callCREOverdue_Rebate(caseobj.Accountid,caseobj.Id);
                        }
                        else if(caseobj.RecordType.DeveloperName.equalsIgnoreCase('Rebate_On_Advance')) {
                            pg = callRebateOnAdvance( caseobj.Accountid, caseobj.Id );
                        }
                        else if(caseobj.RecordType.DeveloperName.equalsIgnoreCase('Riyadh_Rotana_Conversion')) {
                            pg = callRiyadhRotanaConversion( caseobj.Accountid, caseobj.Id );
                        }
                        else if( caseobj.RecordType.DeveloperName.equalsIgnoreCase( 'Waiver' ) ) {
                            pg = callWaiver( caseobj.Accountid, caseobj.Id );
                        }
                        else if( caseobj.RecordType.DeveloperName.equalsIgnoreCase( 'Case_Summary_Client_Relation' ) ) {
                            pg = Case_Summary_Client_Relation( caseobj.Accountid, caseobj.Id );
                        }
                    }else{
                        String errorMessage= 'This SR cannot be proceeded as there is no customer associated with it.';
                        ApexPages.addmessage(new ApexPages.message(
                        ApexPages.severity.Error,errorMessage));
                    }
                }else if(caseobj.RecordType.DeveloperName == 'Assignment' && caseobj.Seller__c != null){
                    pg = callCREPortalHomeForAssignment(caseobj.Seller__c ,caseobj.Id);
                }else if(caseobj.RecordType.DeveloperName == 'Rental_Pool_Assignment' && caseobj.Seller__c != null){
                    system.debug('aya re aya**************');
                    pg = callCREPortalHomeForRPAssignment(caseobj.Seller__c ,caseobj.Id);
                }else{
                    /*
                    *   Show error message if no customer is associated with the SR
                    **/
                    String errorMessage= 'This SR cannot be proceeded as there is no customer associated with it.';
                    ApexPages.addmessage(new ApexPages.message(
                    ApexPages.severity.Error,errorMessage));
                }
            }
            else
            {
                system.debug('accountID or RecordType Id is null');
            }
        }
        else
        {
            system.debug('caseobj is null');
        }
        return pg;
    }

    public PageReference callCRETitleDeed(Id accountID, Id caseID) {
        PageReference pg = new PageReference('/apex/TitleDeedProcessPage?caseId='+caseID+'&AccountId='+accountID + '&SRType=TitleDeed');
        pg.setredirect(true);
        return pg;
    }
    
    public PageReference callCREChequeReplacement(Id accountID, Id caseID) {
        PageReference pg = new PageReference('/apex/ChequeReplacementProcessPage?caseId='+caseID+'&AccountId='+accountID + '&SRType=ChequeReplacement');
        pg.setredirect(true);
        return pg;
    }
    
    public PageReference callCREPlotHandover(Id accountID, Id caseID) {
        PageReference pg = new PageReference('/apex/PlotHandoverProcessPage?AccountId='+accountID+'&CaseID='+caseID+'&SRType=PlotHandover' );
        pg.setredirect(true);
        return pg;
    }
    
    public Pagereference callCREPortalHomeForAssignment(Id accountID, Id caseID){
        PageReference pg = new PageReference('/apex/AssignmentRequestProcessPage?caseId='+caseID+'&AccountId='+accountID + '&SRType=AssignmentRequest');
        //PageReference pg = new PageReference('/apex/AssignmentCREPortal?caseId='+caseID+'&AccountId='+accountID + '&SRType=AssignmentRequest');
        pg.setredirect(true);
        system.debug('assignment redirection redirect method********');
        return pg;
    }
    
    public Pagereference callCREPortalHomeForRPAssignment(Id accountID, Id caseID){
        PageReference pg = new PageReference('/apex/RPAssignmentRequestProcessPage?caseId='+caseID+'&AccountId='+accountID + '&SRType=RentalPoolAssignment');
        //PageReference pg = new PageReference('/apex/AssignmentCREPortal?caseId='+caseID+'&AccountId='+accountID + '&SRType=AssignmentRequest');
        pg.setredirect(true);
        system.debug('RP assignment redirection redirect method********');
        return pg;
    }

    public Pagereference callCREPortalHomeForPenalty(Id accountID, Id caseID)
    {
        system.debug('Inside penalty waiver record type....');
        PageReference pg = new PageReference('/apex/PenaltyWaiverProcessPage?caseId='+caseID+'&AccountId='+accountID + '&SRType=PenaltyWaiver');
        /*pg.getParameters().put('AccountId', caseobj.Accountid );
        pg.getParameters().put('SRType', 'PenaltyWaiver');
        pg.getParameters().put('caseId', caseobj.id);*/
        pg.setredirect(true);
        return pg;
    }

    public Pagereference callCREPortalHomeForAOPT(Id accountID, Id caseID, String strSRType)
    {
        //PageReference pg = new PageReference('/apex/DummyTitleDeedCREPortal?caseID='+caseID+'&AccountId='+accountID + '&SRType=AOPT');
        PageReference pg = new PageReference('/apex/AOPTProcessPage?caseID='+caseID+'&AccountId='+accountID + '&SRType='+strSRType);
        pg.setredirect(true);
        system.debug('in aopt redirect method');
        return pg;
    }

    public Pagereference callCREPortalHomeForParking(Id accountID, Id caseID)
    {
        //PageReference pg = new PageReference('/apex/CREPortalHomeAOPT?AccountId='+accountID+'&caseID='+caseID );
        //PageReference pg = new PageReference('/apex/additional_parkingcre?caseID='+caseID+'&AccountId='+accountID + '&SRType=AdditionalParking');
        PageReference pg = new PageReference('/apex/ParkingRequestProcessPage?caseID='+caseID+'&AccountId='+accountID + '&SRType=AdditionalParking');
        pg.setredirect(true);
        system.debug('in AdditionalParking redirect method');
        return pg;
    }

    public Pagereference callCREPortalHomeForPromotions(Id accountID, Id caseID)
    {
        PageReference pg = new PageReference('/apex/PromotionsProcessRequestPage?caseID='+caseID+'&AccountId='+accountID + '&SRType=Promotions');
        pg.setredirect(true);
        system.debug('in promotions redirect method');
        return pg;
    }
    
    public PageReference callCREFundTransfer(Id accountID, Id caseID) {
        // PageReference pg = new PageReference('/apex/FundTransfer?accID='+accountID+'&caseID='+caseID );
        PageReference pg = new PageReference('/apex/FundTransferProcessPage?caseId='+caseID+'&AccountId='+accountID + '&SRType=FundTransfer');

        pg.setredirect(true);
        system.debug('in callCREFundTransfer redirect method');
        return pg;
    }

    public PageReference callCREFundTransferActiveUnits(Id accountID, Id caseID) {
        // PageReference pg = new PageReference('/apex/FundTransfer?accID='+accountID+'&caseID='+caseID );
        PageReference pg = new PageReference('/apex/FundTransferProcessPage?caseId='+caseID+'&AccountId='+accountID + '&SRType=FundTransferActiveUnits');

        pg.setredirect(true);
        system.debug('in callCREFundTransfer redirect method');
        return pg;
    }
    
    public PageReference callCRERentalPoolAgreement(Id accountID, Id caseID) {
        PageReference pg = new PageReference('/apex/RentalPoolAgreementProcessPage?caseId='+caseID+'&AccountId='+accountID + '&SRType=RentalPoolAgreement');

        pg.setredirect(true);
        system.debug('in callCRERentalPoolAgreement redirect method');
        return pg;
    }
    
    public PageReference callCRERentalPoolAgreementEHO(Id accountID, Id caseID) {
        PageReference pg = new PageReference('/apex/RentalPoolAgreementEHOProcessPage?caseId='+caseID+'&AccountId='+accountID + '&SRType=RentalPoolEHO');

        pg.setredirect(true);
        system.debug('in callCRERentalPoolAgreementEHO redirect method');
        return pg;
    }
    
    public PageReference callCRERentalPoolTermination(Id accountID, Id caseID) {
        PageReference pg = new PageReference('/apex/RentalPoolTerminationProcessPage?caseId='+caseID+'&AccountId='+accountID + '&SRType=RentalPoolTermination');

        pg.setredirect(true);
        system.debug('in callCRERentalPoolAgreement redirect method');
        return pg;
    }
    
    public Pagereference callCREPortalHomeRefunds(Id accountID, Id caseID)
    {
        //PageReference pg = new PageReference('/apex/CREPortalHomeRefunds?AccountId='+accountID+'&caseID='+caseID );
        PageReference pg = new PageReference('/apex/RefundsProcessPage?AccountId='+accountID+'&CaseID='+caseID+'&SRType=Refunds' );
        pg.setredirect(true);
        system.debug('in callCREPortalHomeRefunds redirect method');
        return pg;
    }

    public Pagereference callCREPortalPOP(Id accountID, Id caseID)
    {

        //PageReference pg = new PageReference('/apex/CREPortalPOP?AccountId='+accountID+'&caseID='+caseID);
        //PageReference pg = new PageReference('/apex/POPProcessPage?AccountId='+accountID+'&CaseID='+caseID+'&SRType=ProofOfPayment' );
        PageReference pg = Label.Is_POP_New_Process.ContainsIgnoreCase('Y') ? caseobj.CreatedDate >= date.valueOf(Label.LWC_POP_New_Process_Date) ? new PageReference('/lightning/n/ProofOfPaymentPage?c__accountId='+accountID+'&c__caseId='+caseID) : new PageReference('/apex/POPProcessPage?AccountId='+accountID+'&CaseID='+caseID+'&SRType=ProofOfPayment' ) : new PageReference('/apex/POPProcessPage?AccountId='+accountID+'&CaseID='+caseID+'&SRType=ProofOfPayment' ) ;
        pg.setredirect(true);
        system.debug('in callCREPortalPOPredirect method');
        return pg;
    }
    public Pagereference callCREPortalNOC(Id accountID, Id caseID)
    {
        //PageReference pg = new PageReference('/apex/CREPortalPOP?AccountId='+accountID+'&caseID='+caseID);
        PageReference pg = new PageReference('/apex/NocForVisaProcessPage?AccountId='+accountID+'&CaseID='+caseID+'&SRType=NOCVisa' );
        pg.setredirect(true);
        system.debug('in callCREPortalNOC method');
        return pg;
    }
    public PageReference callCRECOD(Id accountID, Id caseID, String recordType,String status) {
        //PageReference pg = new PageReference('/apex/DummyTitleDeedCREPortal?AccountId='+accountID+'&CaseId='+caseID+'&SRType=COCD&RecType='+recordType+'&Status='+status );
        PageReference pg = new PageReference('/apex/COCDProccessPage?AccountId='+accountID+'&CaseId='+caseID+'&SRType=COCD&RecType='+recordType+'&Status='+status );
        pg.setredirect(true);
        system.debug('in callCRECOD redirect method');
        return pg;
    }

    public PageReference callLandingPage(Id accountID, Id caseID, String recordType) {
        PageReference pg =
            new PageReference(
                '/apex/Early_HandoverProcessPage?AccountId=' + accountID
                + '&CaseId=' + caseID
                + '&SRType=' + recordType
            );
        pg.setredirect(true);
        system.debug('in callLandingPage redirect method');
        return pg;
    }
    
    public PageReference callLHO(Id accountID, Id caseID, String recordType) {
        PageReference pg =
            new PageReference(
                '/apex/Lease_HandoverProcessPage?AccountId=' + accountID
                + '&CaseId=' + caseID
                + '&SRType=' + recordType
            );
        pg.setredirect(true);
        system.debug('in callLHO redirect method');
        return pg;
    }
    
    public PageReference callCREComplaints(Id accountID, Id caseID ) {
        //PageReference pg = new PageReference('/apex/DummyTitleDeedCREPortal?AccountId='+accountID+'&CaseId='+caseID+'&SRType=Complaint' );
        PageReference pg = new PageReference('/apex/ComplaintProccessPage?AccountId='+accountID+'&CaseId='+caseID+'&SRType=Complaint' );
        pg.setredirect(true);
        return pg;
    }
    
    public PageReference callEarlySetHO(Id caseID ) {
        PageReference pg = new PageReference('/apex/EarlySettlementHandoverProcessPage?Id='+caseId);
        pg.setredirect(true);
        return pg;
    }
    
    public PageReference callCREMortgage(Id accountID, Id caseID, String status, Id bUId) {
        PageReference pg = new PageReference('/apex/MortgageProccessPage?AccountId='+accountID+'&CaseId='+caseID+'&SRType=Mortgage'+'&Status='+status+'&bookingUnit='+bUId);
        pg.setredirect(true);
        system.debug('in callCREMortgage redirect method');
        return pg;
        //return null;
    }

    public PageReference callCREServiceChargeReset(Id accountID, Id caseID ) {
        PageReference pg = new PageReference('/apex/resetdateprocesspage?AccountID=' + accountID +'&CaseId='+caseID+'&SRType=SERVICE_CHARGE_RESET');
        pg.setredirect(true);
        system.debug('in callCREServiceChargeReset redirect method');
        return pg;
    }
    
    public PageReference callCREOverdue_Rebate(Id accountID, Id caseID ) {
        PageReference pg = new PageReference('/apex/OverdueRebateDiscProcessPage?AccountID=' + accountID +'&CaseId='+caseID+'&SRType=OVERDUE_REBATE_DISCOUNT');
        pg.setredirect(true);
        system.debug('in callCREOverdue_Rebate redirect method');
        return pg;
    }
    public PageReference callRebateOnAdvance(Id accountID, Id caseID ) {
        PageReference pg = new PageReference('/apex/RebateOnAdvancePage?AccountID=' + accountID +'&CaseId='+caseID+'&SRType=REBATE_ADVANCE');
        pg.setredirect(true);
        system.debug('in callRebateOnAdvance redirect method');
        return pg;
    }

    public PageReference callRiyadhRotanaConversion(Id accountID, Id caseID ) {
        PageReference pg = new PageReference('/apex/RRC_SR_Creation?AccountID=' + accountID +'&CaseId='+caseID+'&SRType=RIYADH_ROTANA_CONVERSION');
        pg.setredirect(true);
        system.debug('in callRiyadhRotanaConversion redirect method');
        return pg;
    }

    public PageReference callWaiver(Id accountID, Id caseID ) {
        PageReference pg = new PageReference('/apex/WaiverSr?AccountID=' + accountID +'&CaseId='+caseID+'&SRType=WAIVER');
        pg.setredirect(true);
        system.debug('in callWaiver redirect method');
        return pg;
    }

    public PageReference Case_Summary_Client_Relation(Id accountID, Id caseID ) {
        PageReference pg = new PageReference('/apex/Case_Summary_Client_Relation?AccountID=' + accountID +'&CaseId='+caseID+'&SRType=Case_Summary_Client_Relation');
        pg.setredirect(true);
        system.debug('in Case_Summary_Client_Relation redirect method');
        return pg;
    }
}