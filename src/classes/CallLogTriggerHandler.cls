public with sharing class CallLogTriggerHandler {
   
   public static void handleTrigger(List<Call_Log__c> listCallLog, System.TriggerOperation triggerEvent) {
    
        switch on triggerEvent {
        
            when AFTER_INSERT {
                CallLogTranscriptHelper.sendToPhpUtility(listCallLog);
            }
            when BEFORE_INSERT {
                //set value on record create
            }
            when AFTER_DELETE {
                //prevent deletion of sensitive data
            }
            when else {
                //do nothing for AFTER_UNDELETE, BEFORE_DELETE, or BEFORE_UPDATE
            }
        }
    }


}