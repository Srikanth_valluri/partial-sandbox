/**
 * @File Name          : TaskTriggerHandlerManager.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 8/20/2019, 12:25:07 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    7/24/2019, 5:35:34 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public class TaskTriggerHandlerManager{

    
    public static void submitFMCaseForApproval(Map<Id,task> newMap, Map<Id,task> oldMap){
        Set<Id> setFMCaseIds=new Set<Id>();
        for(Task objTask : newMap.values()) {
            if((oldMap.get(objTask.Id).status != 'Closed' && oldMap.get(objTask.Id).status != 'Completed')
                && (objTask.status == 'Closed' || objTask.status == 'Completed')
                && objTask.subject=='Verify All Details'
                && objTask.Process_Name__c != 'Work permit') {// work permit check added by Lochan on 21/04/2019
                    setFMCaseIds.add(objTask.WhatID);
                }
        }//for
        List<FM_Case__c> relatedCaseList=new List<FM_Case__c>();
        relatedCaseList=[SELECT unit_name__c
                              , Submit_for_Approval__c
                              , recordtype.name
                        FROM    fm_case__c
                        WHERE   id IN :setFMCaseIds
                        AND Origin__c IN ('Portal', 'Portal - Guest')];
        if(!relatedCaseList.isEmpty()){
            for(FM_Case__c fmCaseIns:relatedCaseList){
                fmCaseIns.Submit_for_Approval__c=true;
            }
        }
        update relatedCaseList;
    }
    public static void createtaskForManager(Map<Id,task> newMap){
        System.debug('Inside createtaskForManager newMap:--'+newMap);
        List<FM_Case__c> taskRelatedCaseList = new List<FM_Case__c>();
        Map<Task,FM_Case__c> taskToFmCaseMap = new Map<Task,FM_Case__c>(); 
        Map<Id,Task> fMCaseIdToTaskMap = new Map<Id,Task>();
        List<Task> listTask = new List<Task>();
        Date todaysDate = System.today();

        for(Task objTask : newMap.values()) {
            fMCaseIdToTaskMap.put(
                objTask.WhatID,
                objTask
            );
        }
        System.debug('fMCaseIdToTaskMap-------'+fMCaseIdToTaskMap);
        System.debug('fMCaseIdToTaskMap.values():-------'+fMCaseIdToTaskMap.keySet());

        // Fetch cases related to Tasks - 
        taskRelatedCaseList = [
            SELECT unit_name__c
                , Submit_for_Approval__c
                , recordtype.name
                , Property_Manager__c
                , Property_Manager_Email__c
                , Property_Director__c
                , Property_Director_Email__c
                , FM_Manager_Email__c
                , Admin__c
                , FM_Admin_Email__c
                , Notice_Type__c
                , Remedial_Period__c
            FROM fm_case__c
            WHERE id IN :fMCaseIdToTaskMap.keySet()
        ];

        // Create map of Tasks to realted Fm cases
        for(fm_case__c fmCaseObj : taskRelatedCaseList){
            taskToFmCaseMap.put(
                fMCaseIdToTaskMap.get(fmCaseObj.Id),
                fmCaseObj
            );
        }
        System.debug('taskToFmCaseMap:-------'+taskToFmCaseMap);

        // Iterate on task To Fm case Map
        for(Task taskinstance : taskToFmCaseMap.keySet()){
            
            // 1. On closure of Violation_upload_Incident_Report- task Update incident Report 
            //from PB -FM Case : POP Document Updated
            System.debug('recusrsive:::taskinstance.Id:::::::: '+taskinstance.Id);
            System.debug('recusrsive::::::::::: '+!checkRecursive.SetOfIDs.contains(taskinstance.Id));
             
            if((taskinstance.status == 'Completed' || taskinstance.status == 'Closed') && 
                taskinstance.subject == label.Violation_upload_Incident_Report &&
                !checkRecursive.SetOfIDs.contains(taskinstance.Id)){
            System.debug('Inside Select_Notice_Type_and_Issue_Violation_Notice_Task	  task creation ');
               
                // Create Select Notice Type and Issue Violation Notice task
                Task taskForManager=new Task();
                taskForManager.Assigned_User__c='Property Manager';
                taskForManager.Priority='Normal';
                taskForManager.WhatID=taskinstance.whatid;
                taskForManager.status='Not Started';
                taskForManager.subject=Label.Select_Notice_Type_and_Issue_Violation_Notice_Task	;
                taskForManager.Process_Name__c=taskinstance.Process_Name__c;
                taskForManager.ActivityDate = todaysDate;
                taskForManager.ownerid = taskToFmCaseMap.get(taskinstance).Property_Manager__c;   
                CheckRecursive.SetOfIDs.add(taskinstance.id);             
                listTask.add(taskForManager);
            }
            /* On closure of Select_Notice_Type_and_Issue_Violation_Notice_Task	 - Select Notice Type and Issue Violation 
            Notice and If notice type is First Notice*/
            else if((taskinstance.status=='Completed' || taskinstance.status=='Closed') &&
                    taskinstance.subject==label.Select_Notice_Type_and_Issue_Violation_Notice_Task && 
                    taskToFmCaseMap.get(taskinstance).Notice_Type__c == 'First Notice' &&
                    !checkRecursive.SetOfIDs.contains(taskinstance.Id)){
                System.debug('Inside Violation_Admin_Task task creation ');
               
                /* Create task for verifying if the violation has been rectified or not based on Remedial Period
                    If Remedial Period is Immediate*/
                if(taskToFmCaseMap.get(taskinstance).Remedial_Period__c == 'Immediate' ){
                    Datetime immediateExecuteTime = (System.now()).addDays(1);
                    CheckRecursive.SetOfIDs.add(taskinstance.id); 
                    ScheduleViolationTaskCreation(immediateExecuteTime,taskinstance,taskToFmCaseMap);
                }
                else if(taskToFmCaseMap.get(taskinstance).Remedial_Period__c == '7 days'){
                    Datetime afterSevenDaysExecuteTime = (System.now()).addDays(7);
                    CheckRecursive.SetOfIDs.add(taskinstance.id); 
                    ScheduleViolationTaskCreation(afterSevenDaysExecuteTime,taskinstance,taskToFmCaseMap);
                }
                
                
               /* Task taskForAdmin=new Task();
                taskForAdmin.Assigned_User__c='FM Admin';
                taskForAdmin.Priority='Normal';
                taskForAdmin.WhatID=taskinstance.whatid;
                taskForAdmin.status='Not Started';
                taskForAdmin.subject=Label.Violation_Admin_Task;
                taskForAdmin.Process_Name__c = taskinstance.Process_Name__c;
                taskForAdmin.ActivityDate = todaysDate;
                taskForAdmin.ownerid = taskToFmCaseMap.get(taskinstance).Admin__c;
                CheckRecursive.SetOfIDs.add(taskinstance.id);
                listTask.add(taskForAdmin);*/

            }
            
            // On closure of Violation_Final_Notice_Task - "Issue the Final Notice" manually 
            // 
            else if((taskinstance.status=='Completed' || taskinstance.status=='Closed') &&
                taskinstance.subject == label.Violation_Final_Notice_Task &&
                !checkRecursive.SetOfIDs.contains(taskinstance.Id)){
                System.debug('Inside TaskLabel_PostFineSoA task creation ');
                // Create task PM to send request for approval to Property Director to post fine to SOA
                Task objTask = new Task();
                objTask.WhatId = taskinstance.whatid;
                //objTask.ActivityDate = System.today().addDays(1);
                objTask.Priority = 'High';
                objTask.Status = 'Not Started';
                objTask.Subject = Label.TaskLabel_PostFineSoA;
                objTask.Process_Name__c = 'Violation Notice';
                objTask.Assigned_User__c='Property Manager';
                objTask.ActivityDate = todaysDate;
                objTask.ownerId = taskToFmCaseMap.get(taskinstance).Property_Manager__c;
                CheckRecursive.SetOfIDs.add(taskinstance.id);
                listTask.add(objTask);
            }
            

            
        }
        System.debug('listTask:::::::::::'+listTask);
        insert listTask;

    }

    /* This method is used to create Schedule Time Depending on the remediat PErio selected */
    public static void ScheduleViolationTaskCreation(DateTime executeTime,Task taskinstance, Map<Task,FM_Case__c> taskToFmCaseMap){
        
        // For a demo you can schedule it for a minute later
       // executeTime = System.now();
        String day = string.valueOf(executeTime.day());
        String month = string.valueOf(executeTime.month());
        String hour = string.valueOf(executeTime.hour());
        //String minute = string.valueOf(executeTime.minute()+1);
        String minute = string.valueOf(executeTime.minute());
        String second = string.valueOf(executeTime.second());
        String year = string.valueOf(executeTime.year());
        String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;

       
        System.debug('strSchedule :'+strSchedule);
        ScheduleViolationProcessTaskCreator scheduleJobInstance = new ScheduleViolationProcessTaskCreator (
            taskinstance,
            taskToFmCaseMap.get(taskinstance).Admin__c
        );
        System.schedule(
            'Create Violation Rectified Task ' + executeTime.getTime(),
            strSchedule,
            scheduleJobInstance
        );
    }
}