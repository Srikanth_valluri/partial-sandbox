/**************************************************************************************************
 *
 --------------------------------------------------------------------------------------------------
 * Description : AssignCaseTo Fam Queue
 * Test Class : 
 **************************************************************************************************/ 
public class AssignCaseToFAMQueue {

    private static final String QUEUE_NAME = Label.FAMQueue;//'FAM_Queue';
    
   
    @InvocableMethod public static void updateCases(List<ID> ids) {
        List<Case> toUpdateCases = new List<Case>();
        List<Account> toUpdateAccounts = new List<Account>();
        List<Group> groupList = [SELECT 
                                    DeveloperName,
                                    Id,
                                    Name 
                                 FROM Group 
                                 WHERE DeveloperName =: QUEUE_NAME ];


                                
        List<Case> casesList = [SELECT 
                                    Id,
                                    OwnerId,AccountId,Subject,Description
                                    ,(SELECT id, ParentId,ToAddress,BccAddress,CcAddress,Subject,TextBody FROM EmailMessages 
                                    )
 
                                FROM Case 
                                WHERE Id IN : ids];
                                
        set<String> toAddrSet = new set<String>();
        for (String strEmail: Label.EmailToRunFAMLogic.split(',')){
            toAddrSet.add(strEmail);
        }
        
                
        Boolean checkFam = false;
        Boolean upadteFam = false;
        String valueToCheck = Label.ValueToCheckOnEmailToReRoute;
        System.debug('valueToCheck'+valueToCheck);
        List<String> listvalueToCheck = new List<String>();
        listvalueToCheck = valueToCheck.split(',');     
        System.debug('listvalueToCheck'+listvalueToCheck);
        
        System.debug('caseObj.EmailMessages=='+casesList);
        System.debug('caseObj.EmailMessages=='+casesList.size());
        
        for(Case caseObj : casesList) {
            System.debug('caseObj.EmailMessages=='+caseObj.EmailMessages);
            System.debug('caseObj.EmailMessages=='+caseObj.EmailMessages.size());
            
                    System.debug('caseObj.Subject='+caseObj.Subject);
                    System.debug('caseObj.Subject='+caseObj.Description);
                    
                    for(String str: listvalueToCheck)
                    if( (String.isNotBlank(caseObj.Subject) && caseObj.Subject.ContainsIgnoreCase(str))
                        ||(String.isNotBlank(caseObj.Description) && caseObj.Description.ContainsIgnoreCase(str)))
                    {
                        upadteFam = true;
                    }
                    
                if(upadteFam){
                    caseObj.OwnerId = groupList[0].Id;
                    if(caseObj.AccountId != Null){
                        Account objAcc = new Account();
                            objAcc.Id = caseObj.AccountId;
                            objAcc.FAM_CAUTION__c = 'FAM';
                            toUpdateAccounts.add(objAcc);
                        
                    }

                }
            
                     
            toUpdateCases.add(caseObj); 
        }
        
        if(toUpdateCases != NULL && toUpdateCases.size()>0)
        update toUpdateCases;
        if(toUpdateAccounts != NULL && toUpdateAccounts.size()>0)
        update  toUpdateAccounts;
    }
}