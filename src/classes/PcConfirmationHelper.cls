public without Sharing class PcConfirmationHelper {
    public static Map <String, List <Inquiry__c>> checkForInquires (Set <String> emails, Set <String> mobileNumbers) {
        try {
            Map <String, List <Inquiry__c>> resultsMap = new Map <String, List <Inquiry__c>> ();
            
            String query = ' SELECT Name, Email__c, Mobile_Phone__c FROM Inquiry__c WHERE ';
            if (emails.size () > 0)
                query += ' Email__c IN :emails OR ';
            if (mobileNumbers.size () > 0)
                query += ' Mobile_Phone__c IN :mobileNumbers ';
            
            query = query.removeEND ('OR ');
            System.Debug (query);
            
            if (emails.size () == 0 && mobileNumbers.size () == 0) {
                return null;
            } 
            else { 
                for (Inquiry__c inquiry :DataBase.query(query+' Order By LastModifiedDate DESC LIMIT 50')) {
                    if (!resultsMap.containsKey (inquiry.Email__c))
                        resultsMap.put (inquiry.Email__c, new List <Inquiry__c>{inquiry});
                    else
                        resultsMap.get (inquiry.Email__c).add (inquiry);
                    
                    if (!resultsMap.containsKey (inquiry.Mobile_Phone__c))
                        resultsMap.put (inquiry.Mobile_Phone__c, new List <Inquiry__c>{inquiry});
                    else
                        resultsMap.get (inquiry.Mobile_Phone__c).add (inquiry);
                    
                }
                return resultsMap;
            }
        }
        catch (Exception e) {
            return null;
        }       
    }
    public static Map <String, List <Account>> checkForAccounts (Set <String> emails, Set <String> mobileNumbers) {
        try {
            Map <String, List <Account>> resultsMap = new Map <String, List <Account>> ();
            String query = ' SELECT Party_ID__c, Name, Email__c, Mobile__c FROM Account WHERE ';
            if (emails.size () > 0)
                query += ' Email__c IN :emails OR ';
            if (mobileNumbers.size () > 0)
                query += ' Mobile__c IN :mobileNumbers ';
            
            query = query.removeEND ('OR ');
            if (emails.size () == 0 && mobileNumbers.size () == 0) {
                return null;
            } 
            else { 
                for (Account acc :Database.Query (query+' Order By LastModifiedDate DESC LIMIT 50')) {
                    if (!resultsMap.containsKey (acc.Email__c))
                        resultsMap.put (acc.Email__c, new List <Account>{acc});
                    else
                        resultsMap.get (acc.Email__c).add (acc);
                    
                    if (!resultsMap.containsKey (acc.Mobile__c))
                        resultsMap.put (acc.Mobile__c, new List <Account>{acc});
                    else
                        resultsMap.get (acc.Mobile__c).add (acc);
                    
                }
                return resultsMap;
            }
        }
        catch (Exception e) {
            return null;
        }       
    }
    public static void changeOwnerShip (Set <ID> accountIds) {
        List <Account> accounts = new List <Account> ();
        ID currentUserId = UserInfo.getUserID ();
        System.Debug (accountIds);
        for (Account acc :[SELECT OwnerID FROM Account WHERE ID IN :accountIds AND OwnerID !=: currentUserId ]) {
            acc.OwnerID = currentUserId ;
            accounts.add (acc);
        }
        Update accounts;
    }
    public static Boolean checkForDPDocAccount (ID accountID) {
        Boolean lockFields = false;
        List <Account> acc = new List <Account> ();
        acc = [ SELECT ID FROM Account WHERE ID =: accountID ];
        if (acc.size () > 0) {
            
            List <buyer__c> srWithDPDOCBuyer = new List <buyer__c> ();
            srWithDPDOCBuyer = [SELECT SR_DP_DOC_OK__c 
                                FROM buyer__c 
                                WHERE SR_DP_DOC_OK__c = true 
                                AND account__c =: acc[0].Id];
            if (srWithDPDOCBuyer.size() > 0) {
                lockFields = true;
            } else {
                lockFields = false;
            }
        }
        return lockFields;
    }
    public static string getAllFields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        
        if (objectType == null)
            return fields;
        
        Map <String, Schema.SObjectField> fieldMap = objectType.getDescribe ().fields.getMap ();
        
        for (Schema.SObjectField sfield : fieldMap.Values()) {
            
            fields += sfield.getDescribe().getName ()+ ', ';
            
        }
        return fields.removeEnd(', '); 
    }
    public static String getAccountBasedOnNationality (String nationality, String passportNumber,string buyerType) {
        try {
            System.Debug (nationality+'==='+passportNumber );
            if (nationality != '' && nationality != null && passportNumber != '' && passportNumber != null) {
                
                List <Account> acc = new List <Account> ();
                string fields = getAllFields('Account');
                string query = '';
                if ( buyerType == 'Corporate' ) {
                    query = 'select '+fields+ ' From Account where Nationality__c = '+'\''+Nationality+'\''+' AND Passport_Number__c = '+'\''+passportNumber+'\''+' Limit 1';
                } else {
                    query = 'select '+fields+ ' From Account where isPersonAccount = true AND Nationality__pc = '+'\''+Nationality+'\''+' AND Passport_Number__pc = '+'\''+passportNumber+'\''+ ' Limit 1';   
                }
                System.debug('qqqqqqqqqqqq'+query);
                acc = database.query(query);
                System.debug('aaaaaaaaaaa'+acc.size());
                if (acc.size () > 0) {

                    Boolean lockFields = false;
                    List <buyer__c> srWithDPDOCBuyer = new List <buyer__c> ();
                    srWithDPDOCBuyer = [SELECT SR_DP_DOC_OK__c 
                                        FROM buyer__c 
                                        WHERE SR_DP_DOC_OK__c = true 
                                        AND account__c =: acc[0].Id];
                    if (srWithDPDOCBuyer.size() > 0) {
                        lockFields = true;
                    } else {
                        lockFields = false;
                    }
                    buyer__c b = new Buyer__c();
                    if ( buyerType == 'Corporate' ) {
                        b = mapBuyerFields1(acc);
                    } else {
                        b = mapBuyerFields2(acc);
                    }
                    String jsonBody = JSON.serialize(b);
                    jsonBody = jsonBody.removeEnd('}');
                    jsonBody = jsonBody+', "disable" : '+lockFields+'}';
                    return jsonBody;
                }
                return '';
            }
            else {
                return '';
            }    
       }
        catch (Exception e) {
            System.Debug (e.getMessage ());
            return '';
        } 
    }
    public static String getAccBasedOnNationality (String nationality, String passportNumber) {
        try {
            System.Debug (nationality+'==='+passportNumber );
            if (nationality != '' && nationality != null && passportNumber != '' && passportNumber != null) {
                
                List <Account> acc = new List <Account> ();
                acc = [SELECT FirstName, LastName, PersonTitle, Title__c, Passport_Number__pc,
                       Nationality__pc, Passport_Expiry_Date__pc, Mobile_Phone_Encrypt__c,
                       Email__pc, Date_of_Birth__c, Address_Line_1__c, Address_Line_2__c,
                       Address_Line_3__c, Address_Line_4__c, Country__c, City__c,
                       Mobile_Country_Code__pc, Address_Line_1_Arabic__c, Address_Line_2_Arabic__c,
                       Address_Line_3_Arabic__c, Address_Line_4_Arabic__c, City_Arabic__c, Country_Arabic__c,
                       CR_Registration_Place_Arabic__c, First_Name_Arabic__c, Last_Name_Arabic__c,
                       Nationality_Arabic__c, Organisation_Name_Arabic__c, Title_Arabic__c,
                       Nationality__c,Passport_Number__c,Passport_Expiry_Date__c,Mobile_Country_Code__c,Email__c,
                       Passport_Issue_Place__c , Party_ID__c                    
                       FROM Account 
                       WHERE isPersonAccount = true
                       AND (Nationality__pc =: Nationality OR Nationality__c =: Nationality)
                       AND (Passport_Number__pc =: passportNumber OR Passport_Number__c =: passportNumber)
                       LIMIT 1];
                
                if (acc.size () > 0) {

                    Boolean lockFields = false;
                    List <buyer__c> srWithDPDOCBuyer = new List <buyer__c> ();
                    srWithDPDOCBuyer = [SELECT SR_DP_DOC_OK__c 
                                        FROM buyer__c 
                                        WHERE SR_DP_DOC_OK__c = true 
                                        AND account__c =: acc[0].Id];
                    if (srWithDPDOCBuyer.size() > 0) {
                        lockFields = true;
                    } else {
                        lockFields = false;
                    }
                    buyer__c b = mapBuyerFields (acc);
                    String jsonBody = JSON.serialize(b);
                    jsonBody = jsonBody.removeEnd('}');
                    jsonBody = jsonBody+', "disable" : '+lockFields+'}';
                    return jsonBody;
                }
                return '';
            }
            else {
                return '';
            }    
        }
        catch (Exception e) {
            System.Debug (e.getMessage ());
            return '';
        }
    }
    
    public static String mapPrimaryInquiry (ID inquiryId) {
        try {
            System.Debug ('inquiryId----------------'+inquiryId);
            List <Inquiry__c> inqList = new List <Inquiry__c> ();
            inqList = [SELECT First_name__c, last_name__c, Title__c, Nationality__c,
                       Passport_Number__c, Passport_Expiry_Date__c, Mobile_CountryCode__c,
                       Mobile_Phone__c, Mobile_Phone_Encrypt__c, Email__c,
                       Date_of_Birth__c, Address_Line_1__c, Address_Line_2__c,
                       address_Line_3__c, Address_Line_4__c,
                       Country__c, City__c, City_new__c,
                       Organisation_Name_Arabic__c, Title_Arabic__c ,
                       Nationality_Arabic__c, Last_Name_Arabic__c,
                       First_Name_Arabic__c, CR_Registration_Place_Arabic__c,
                       Country_Arabic__c, City_Arabic__c ,
                       Address_Line_4_Arabic__c, Address_Line_3_Arabic__c,
                       Address_Line_2_Arabic__c, Address_Line_1_Arabic__c 
                       FROM Inquiry__c 
                       WHERE id =:inquiryId 
                       AND ( Inquiry_Status__c='Active' 
                            OR Inquiry_Status__c='New' 
                            OR Inquiry_Status__c='Qualified' 
                            OR Inquiry_Status__c ='Meeting Scheduled' )
                       AND (recordtype.Name='Inquiry' 
                            OR recordtype.Name='Inquiry Decrypted'
                            OR recordtype.Name='CIL') 
                       AND ownerid=:userinfo.getuserid() ];
            System.Debug ('inqList.size ()----------------'+inqList.size ());
            if (inqList.size() > 0) {
                Boolean lockFields = false;
                
                buyer__c b = mapInquiryFields (inqList);
                String jsonBody = JSON.serialize(b);
                jsonBody = jsonBody.removeEnd('}');
                jsonBody = jsonBody+', "disable" : '+lockFields+'}';
                System.Debug (jsonBody);
                return jsonBody;
            }
            return '';
        }
        catch (Exception e) {
            return '';
        }
        
    }
    public static Buyer__c mapInquiryFields (List <Inquiry__c> acc) {
        Buyer__c b = new Buyer__c ();
        b.Inquiry__c = acc[0].id; 
        b.Title_Arabic__c = acc[0].Title_Arabic__c;
        b.Organisation_Name_Arabic__c = acc [0].Organisation_Name_Arabic__c;
        b.Nationality_Arabic__c = acc[0].Nationality_Arabic__c;
        b.Last_Name_Arabic__c = acc[0].Last_Name_Arabic__c;
        b.First_Name_Arabic__c = acc[0].First_Name_Arabic__c;
        b.CR_Registration_Place_Arabic__c = acc[0].CR_Registration_Place_Arabic__c;
        b.Country_Arabic__c = acc[0].Country_Arabic__c;
        b.City_Arabic__c = acc[0].City_Arabic__c;
        b.Address_Line_4_Arabic__c = acc[0].Address_Line_4_Arabic__c;
        b.Address_Line_3_Arabic__c = acc[0].Address_Line_3_Arabic__c;
        b.Address_Line_2_Arabic__c = acc[0].Address_Line_2_Arabic__c;
        b.Address_Line_1_Arabic__c = acc[0].Address_Line_1_Arabic__c;           
        b.First_name__c = acc[0].first_name__c;                   
        b.last_name__c = acc[0].last_name__c;
        b.Title__c = acc[0].Title__c != null ? acc[0].Title__c : '';
        b.Nationality__c = acc[0].Nationality__c;
        b.Passport_Number__c = acc[0].Passport_Number__c;
        b.Passport_Expiry__c = acc[0].Passport_Expiry_Date__c;
        if (b.passport_expiry__c <= System.today().addDays(30)) {
            b.Passport_Expiry__c = NULL;
        }
        b.Phone_Country_Code__c = acc[0].Mobile_CountryCode__c;
        //b.Phone__c = acc[0].Mobile_Phone_Encrypt__c != NULL ? UtilityHelperCls.decryptMobile(acc[0].Mobile_Phone_Encrypt__c) : '' ;
        b.Email__c = acc[0].Email__c;
        
        b.dob__c = acc[0].Date_of_Birth__c;
        if (b.Dob__c != NULL) {
            if (!validateDateOfBirth (b.DOB__c)) {
                b.dob__c = NULL;
            }
        }
        b.Gender__c = '';
        b.EID_No__c = '';
        b.EID_Expiry_date__c = null;
        b.Address_Line_1__c = acc[0].Address_Line_1__c;
        b.Address_Line_2__c = acc[0].Address_Line_2__c;
        b.address_Line_3__c = acc[0].address_Line_3__c;
        b.Address_Line_4__c = acc[0].Address_Line_4__c;
        b.Country__c = acc[0].Country__c;
        b.City__c = acc[0].City__c == null ? acc[0].City_new__c : '';
        if (b.Address_Line_1__c != null) {
            if (b.Address_Line_1__c.length () < 5)
                b.Address_Line_1__c = null;
        }
        return b;
    }
    
    public static String mapPrimaryAccount (ID accountID) {
        try {
            System.Debug (accountID);
            List <Account> acc = new List <Account> ();
            acc = [SELECT FirstName, LastName, PersonTitle, Title__c, Passport_Number__pc,
                   Nationality__pc, Passport_Expiry_Date__pc, Mobile_Phone_Encrypt__c,
                   Email__pc, Date_of_Birth__c, Address_Line_1__c, Address_Line_2__c,
                   Address_Line_3__c, Address_Line_4__c, Country__c, City__c,
                   Mobile_Country_Code__pc, Address_Line_1_Arabic__c, Address_Line_2_Arabic__c,
                   Address_Line_3_Arabic__c, Address_Line_4_Arabic__c, City_Arabic__c, Country_Arabic__c,
                   CR_Registration_Place_Arabic__c, First_Name_Arabic__c, Last_Name_Arabic__c,
                   Nationality_Arabic__c, Organisation_Name_Arabic__c, Title_Arabic__c,
                   Nationality__c,Passport_Number__c,Passport_Expiry_Date__c,Mobile_Country_Code__c,Email__c,
                   Passport_Issue_Place__c, Party_ID__c
                   FROM Account 
                   WHERE ID =: accountID
                //   AND isPersonAccount = true
                   LIMIT 1];
            
            if (acc.size () > 0) {
                Boolean lockFields = false;
                
                buyer__c b = mapBuyerFields (acc);
                
                List <buyer__c> srWithDPDOCBuyer = new List <buyer__c> ();
                srWithDPDOCBuyer = [SELECT SR_DP_DOC_OK__c 
                                    FROM buyer__c 
                                    WHERE  
                                    
                                    SR_DP_DOC_OK__c = true AND 
                                    account__c =: accountID];
                System.Debug ('Size ---'+srWithDPDOCBuyer.size ());
                if (srWithDPDOCBuyer.size() > 0) {
                    lockFields = true;
                } else {
                    lockFields = false;
                }
                String jsonBody = JSON.serialize(b);
                jsonBody = jsonBody.removeEnd('}');
                jsonBody = jsonBody+', "disable" : '+lockFields+'}';
                System.Debug ('----'+jsonBody);
                return jsonBody;
            }
            return '';
            
        }
        catch (Exception e) {
            return '';
        }
    }
    
    
    public static Buyer__c mapBuyerFields (List <Account> acc) {
        Buyer__c b = new Buyer__c ();
        b.Account__c = acc[0].id; 
        b.Title_Arabic__c = acc[0].Title_Arabic__c;
        b.Organisation_Name_Arabic__c = acc [0].Organisation_Name_Arabic__c;
        b.Nationality_Arabic__c = acc[0].Nationality_Arabic__c;
        b.Last_Name_Arabic__c = acc[0].Last_Name_Arabic__c;
        b.First_Name_Arabic__c = acc[0].First_Name_Arabic__c;
        b.CR_Registration_Place_Arabic__c = acc[0].CR_Registration_Place_Arabic__c;
        b.Country_Arabic__c = acc[0].Country_Arabic__c;
        b.City_Arabic__c = acc[0].City_Arabic__c;
        b.Address_Line_4_Arabic__c = acc[0].Address_Line_4_Arabic__c;
        b.Address_Line_3_Arabic__c = acc[0].Address_Line_3_Arabic__c;
        b.Address_Line_2_Arabic__c = acc[0].Address_Line_2_Arabic__c;
        b.Address_Line_1_Arabic__c = acc[0].Address_Line_1_Arabic__c;           
        b.First_name__c = acc[0].firstname;                   
        b.last_name__c = acc[0].lastname;
        b.Title__c = acc[0].Title__c != null ? acc[0].Title__c :acc[0].PersonTitle;
        b.Nationality__c = acc[0].Nationality__pc;
        b.Passport_Number__c = acc[0].Passport_Number__pc;
        b.Passport_Expiry__c = acc[0].Passport_Expiry_Date__pc;
        if (b.passport_expiry__c <= System.today().addDays(30)) {
            b.Passport_Expiry__c = NULL;
        }
        b.Phone_Country_Code__c = acc[0].Mobile_Country_Code__pc;
        b.Phone__c = acc[0].Mobile_Phone_Encrypt__c != NULL ? UtilityHelperCls.decryptMobile(acc[0].Mobile_Phone_Encrypt__c) : '' ;
        b.Email__c = acc[0].Email__pc;
        
        b.dob__c = acc[0].Date_of_Birth__c;
        if (b.DOB__c != NULL) {
            if (!validateDateOfBirth (b.DOB__c)) {
                b.dob__c = NULL;
            }
        }
        b.Address_Line_1__c = acc[0].Address_Line_1__c;
        b.Address_Line_2__c = acc[0].Address_Line_2__c;
        b.address_Line_3__c = acc[0].address_Line_3__c;
        b.Address_Line_4__c = acc[0].Address_Line_4__c;
        b.Country__c = acc[0].Country__c;
        b.City__c = acc[0].City__c;
        b.Place_Of_Issue__c = acc[0].Passport_Issue_Place__c;
        b.Gender__c = '';
        b.EID_No__c = '';
        b.EID_Expiry_date__c = null;
        b.Party_ID__c = acc[0].Party_ID__c;
        if (b.Address_Line_1__c != null) {
            if (b.Address_Line_1__c.length () < 5)
                b.Address_Line_1__c = null;
        }
        return b;
    }
    public static Buyer__c mapBuyerFields1 (List <Account> acc) {
        Buyer__c b = new Buyer__c ();
        b.Account__c = acc[0].id; 
        b.Title_Arabic__c = acc[0].Title_Arabic__c;
        b.Organisation_Name_Arabic__c = acc [0].Organisation_Name_Arabic__c;
        b.Nationality_Arabic__c = acc[0].Nationality_Arabic__c;
        b.Last_Name_Arabic__c = acc[0].Last_Name_Arabic__c;
        b.First_Name_Arabic__c = acc[0].First_Name_Arabic__c;
        b.CR_Registration_Place_Arabic__c = acc[0].CR_Registration_Place_Arabic__c;
        b.Country_Arabic__c = acc[0].Country_Arabic__c;
        b.City_Arabic__c = acc[0].City_Arabic__c;
        b.Address_Line_4_Arabic__c = acc[0].Address_Line_4_Arabic__c;
        b.Address_Line_3_Arabic__c = acc[0].Address_Line_3_Arabic__c;
        b.Address_Line_2_Arabic__c = acc[0].Address_Line_2_Arabic__c;
        b.Address_Line_1_Arabic__c = acc[0].Address_Line_1_Arabic__c;
        string name = acc[0].Name;
        try {
            List<String> splitStr = name.split(' ',2);          
            b.First_name__c = splitStr[0];
            if (splitStr.size() >= 2) {                    
                b.last_name__c = splitStr[1];
            }
        } catch ( Exception e ) {}
        b.Title__c = acc[0].Title__c != null ? acc[0].Title__c :acc[0].PersonTitle;
        b.Nationality__c = acc[0].Nationality__c;
        b.Passport_Number__c = acc[0].Passport_Number__c;
        b.Passport_Expiry__c = acc[0].Passport_Expiry_Date__c;
        if (b.passport_expiry__c <= System.today().addDays(30)) {
            b.Passport_Expiry__c = NULL;
        }
        b.Phone_Country_Code__c = acc[0].Mobile_Country_Code__c;
        try {
            b.Phone__c = acc[0].Mobile_Phone_Encrypt__c != NULL ? UtilityHelperCls.decryptMobile(acc[0].Mobile_Phone_Encrypt__c) : '' ;
        } Catch ( Exception e ) {
        
        }
        b.Email__c = acc[0].Email__c;
        
        b.dob__c = acc[0].Date_of_Birth__c;
        if (b.DOB__c != NULL) {
            if (!validateDateOfBirth (b.DOB__c)) {
                b.dob__c = NULL;
            }
        }
        b.Address_Line_1__c = acc[0].Address_Line_1__c;
        b.Address_Line_2__c = acc[0].Address_Line_2__c;
        b.address_Line_3__c = acc[0].address_Line_3__c;
        b.Address_Line_4__c = acc[0].Address_Line_4__c;
        b.Country__c = acc[0].Country__c;
        b.City__c = acc[0].City__c;
        b.Place_Of_Issue__c = acc[0].Passport_Issue_Place__c;
        b.Gender__c = acc[0].gender__c;
        b.EID_No__c = '';
        b.EID_Expiry_date__c = null;
        b.Party_ID__c = acc[0].Party_ID__c;
        b.DOB__c = acc[0].Date_of_Birth__c;
        b.Organisation_Name__c = acc[0].Organisation_Name__c;
        b.Organisation_Name_Arabic__c = acc[0].Organisation_Name_Arabic__c;
        b.CR_Number__c = acc[0].CR_Number__c;
        b.CR_Registration_Expiry_Date_New__c = acc[0].CR_Registration_Expiry_Date__c;
        b.CR_Registration_Place__c = acc[0].CR_Registration_Place__c;
        b.CR_Registration_Place_Arabic__c = acc[0].CR_Registration_Place_Arabic__c;
        
        if (b.Address_Line_1__c != null) {
            if (b.Address_Line_1__c.length() < 5)
                b.Address_Line_1__c = null;
        }
        return b;
    }
    public static Buyer__c mapBuyerFields2(List<Account> acc) {
        Buyer__c b = new Buyer__c();
        b.Account__c = acc[0].id; 
        b.Title_Arabic__c = acc[0].Title_Arabic__pc;
        b.Organisation_Name_Arabic__c = acc [0].Organisation_Name_Arabic__c;
        b.Nationality_Arabic__c = acc[0].Nationality_Arabic__pc;
        b.Last_Name_Arabic__c = acc[0].Last_Name_Arabic__pc;
        b.First_Name_Arabic__c = acc[0].First_Name_Arabic__pc;
        b.CR_Registration_Place_Arabic__c = acc[0].CR_Registration_Place_Arabic__pc;
        b.Country_Arabic__c = acc[0].Country_Arabic__pc;
        b.City_Arabic__c = acc[0].City_Arabic__pc;
        b.Address_Line_4_Arabic__c = acc[0].Address_Line_4_Arabic__pc;
        b.Address_Line_3_Arabic__c = acc[0].Address_Line_3_Arabic__pc;
        b.Address_Line_2_Arabic__c = acc[0].Address_Line_2_Arabic__pc;
        b.Address_Line_1_Arabic__c = acc[0].Address_Line_1_Arabic__pc;           
        b.First_name__c = acc[0].firstname;                   
        b.last_name__c = acc[0].lastname;
        b.Title__c = acc[0].Title__c != null ? acc[0].Title__c :acc[0].PersonTitle;
        b.Nationality__c = acc[0].Nationality__pc;
        b.Passport_Number__c = acc[0].Passport_Number__pc;
        b.Passport_Expiry__c = acc[0].Passport_Expiry_Date__pc;
        if (b.passport_expiry__c <= System.today().addDays(30)) {
            b.Passport_Expiry__c = NULL;
        }
        b.Phone_Country_Code__c = acc[0].Mobile_Country_Code__pc;
        System.debug('aaaaaaaaaaaaaaaaa'+acc[0].Mobile_Phone_Encrypt__c);
        System.debug('bbbbbbbbbbbbbb'+acc[0].Mobile_Phone_Encrypt__pc);
        try {
            b.Phone__c = acc[0].Mobile_Phone_Encrypt__pc != NULL ? UtilityHelperCls.decryptMobile(acc[0].Mobile_Phone_Encrypt__pc) : '' ;
        } catch ( Exception e ) {
        
        }
        b.Email__c = acc[0].Email__pc;
        
        b.dob__c = acc[0].Date_of_Birth__pc;
        if (b.DOB__c != NULL) {
            if (!validateDateOfBirth (b.DOB__c)) {
                b.dob__c = NULL;
            }
        }
        b.Address_Line_1__c = acc[0].Address_Line_1__pc;
        b.Address_Line_2__c = acc[0].Address_Line_2__pc;
        b.address_Line_3__c = acc[0].address_Line_3__pc;
        b.Address_Line_4__c = acc[0].Address_Line_4__pc;
        b.Country__c = acc[0].Country__pc;
        b.City__c = acc[0].City__pc;
        b.Place_Of_Issue__c = acc[0].Passport_Issue_Place__pc;
        b.Gender__c = acc[0].gender__c;
        b.EID_No__c = '';
        b.EID_Expiry_date__c = null;
        b.DOB__c = acc[0].Date_of_Birth__pc;
        b.Party_ID__c = acc[0].Party_ID__pc;
        if (b.Address_Line_1__c != null) {
            if (b.Address_Line_1__c.length () < 5)
                b.Address_Line_1__c = null;
        }
        return b;
    }
    public static Boolean validateDateOfBirth (Date dateOfBirth){
        Integer days = dateOfBirth.daysBetween (Date.Today());
        Integer age = Integer.valueOf (days/30);
        return age >= Integer.valueOf (System.Label.Eligible_Age_in_Months) ? true : false;
        
    }
    
}