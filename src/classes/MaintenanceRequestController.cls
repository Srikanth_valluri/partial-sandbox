/*-------------------------------------------------------------------------------------------------
Description: Controller for MaintenanceRequestComponent
============================================================================================================================
    Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
    1.0     | 31-07-2018       | Lochana Rajput   | Added logic for SMS sending
----------------------------------------------------------------------------------------------
    2.0     | 24-12-2018       | Lochana Rajput   | 1. Added role for master community
=============================================================================================================================
*/
global virtual without sharing class MaintenanceRequestController {
  public FM_Case__c objFMCase{get;set;}
  public list<Error_Log__c> lstErrLogs;
  public String strAccountId;
  public Integer NoOFLocations{get;set;}
  public String strSRType;
  public String strUnitId{get;set;}
  public String selectedOption{get;set;}
  public list<SelectOption> lstBuildingOptions{get;set;}
  public list<SelectOption> lstSelectOptions{get;set;}
  public list<SelectOption> lstContractOptions{get;set;}
  public String selectedContractOption{get;set;}
  public String contractOptionForPortal {get;set;}
  public String contractAssignedForFMCase{get;set;}
  public String selectedBuilding;
  public String selectedContract;
  public list<SelectOption> lstLocationOptions{get;set;}
  public String selectedLocation{get;set;}
  public CAFM_Location__c objLoc {get;set;}
  public map<String,String> mapId_Code{get;set;}
  //public map<String,String> mapOdContractIdToContract {get;set;}
  // private CAFM_Building__c cafmBuilding;
  public map<String,CAFM_Building__c> mapType_Building{get;set;}
  public Booking_Unit__c objBU{get;set;}
  public String reporterName{get;set;}
  public String category{get;set;}
  public Boolean showBuildings{get;set;}
  public String discipline{get;set;}
  public String DefaultFMcontract;
  Public String strFmCaseId;
  public map<String,String> mapContractIdvsContractName = new map<String,String>();
  public map<String,String> mapOdContractIdToContract = new map<String,String>();
  public map<String,Id> mapCommonAreaCodevsBuildingId = new map<String,Id>();
  public map<String,Id> mapUnitCodevsBuildingId = new map<String,Id>();
  public map<String,String> mapIdType_Building = new map<String,String>();
  public map<String,Id> mapContractNamevsId = new map<String,Id>();
  
  protected MaintenanceRequestController(Boolean shouldCall) {}

  public MaintenanceRequestController() {
      
      showBuildings = true;
      mapId_Code = new map<String,String>();
      mapType_Building = new map<String,CAFM_Building__c>();
      //mapOdContractIdToContract = new map<String,String>();
      objFMCase = new FM_Case__c();
      objFMCase.Origin__c = 'Walk-In';
      objFMCase.Raised_Date__c = DateTime.now();
      objLoc = new CAFM_Location__c();
      selectedLocation = selectedOption = 'None';
      strAccountId = ApexPages.currentPage().getParameters().get('AccountId');
      strFmCaseId = ApexPages.currentPage().getParameters().get('FMCaseId');
      strSRType = ApexPages.currentPage().getParameters().get('SRType');
      System.debug(strAccountId);
      System.debug(strFmCaseId);
      System.debug(strSRType);
      if(strSRType.contains('Helpdesk')) {
        Id devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Helpdesk').getRecordTypeId();
        objFMCase.RecordTypeId = devRecordTypeId;
      }
      strUnitId = ApexPages.currentPage().getParameters().get('UnitId');
      /**
      Clone case Logic // Parent_Case__c
      */
      if(String.isNotBlank(strFmCaseId)) {
          objFMCase.Parent_Case__c = strFmCaseId;
        FM_Case__c objFMCaseold = [Select Id,Helpdesk_Request_Area__c,CAFM_Building__c,CAFM_Contract__c,CAFM_Location__c
                                    ,Short_Description__c,Long_Description__c
                                    FROM FM_Case__c 
                                    WHERE Id =: strFmCaseId];
                                    
            if(objFMCaseold != Null ){
                selectedOption = objFMCaseold.Helpdesk_Request_Area__c ;
                //objFMCase.Short_Description__c = objFMCaseold.Short_Description__c ;
                objFMCase.Long_Description__c = objFMCaseold.Long_Description__c ;
                lstLocationOptions = new List<SelectOption>();
                lstLocationOptions.add(new SelectOption('None', 'None'));
                  NoOFLocations = 0;
                  //String query = 'SELECT Id, Name,Location_Id__c, Description__c,CAFM_Building__c, ';
                    //query += 'CAFM_Building__r.CAFM_Contract__c, CAFM_Building__r.CAFM_Contract__r.Contract_Id__c FROM CAFM_Location__c ';
                    //query += 'WHERE CAFM_Building__c =: objFMCaseold.CAFM_Building__c';
                    System.debug(objFMCaseold.CAFM_Building__c);
                  for(CAFM_Location__c obj: [SELECT Id, Name,Location_Id__c, Description__c,CAFM_Building__c,
                  CAFM_Building__r.CAFM_Contract__c, CAFM_Building__r.CAFM_Contract__r.Contract_Id__c FROM CAFM_Location__c 
                  WHERE CAFM_Building__c =: objFMCaseold.CAFM_Building__c ]) {
                      NoOFLocations++;
                    lstLocationOptions.add(new SelectOption(obj.Id, obj.Description__c));
                  }
                  selectedLocation= objFMCaseold.CAFM_Location__c ;
                  
                  List<CAFM_Building_Contract__c> lstContract =  [ Select id,Name,CAFM_Contract__c 
                                                                    ,CAFM_Contract__r.Name
                                FROM CAFM_Building_Contract__c WHERE CAFM_Building__c =: objFMCaseold.CAFM_Building__c ];
                    lstContractOptions = new List<SelectOption>();
                    for( CAFM_Building_Contract__c obj: lstContract){
                        lstContractOptions.add(new SelectOption(obj.CAFM_Contract__c,obj.CAFM_Contract__r.Name));
                        mapContractIdvsContractName.put(obj.CAFM_Contract__c,obj.CAFM_Contract__r.Name);
                    }
                selectedContractOption = objFMCaseold.CAFM_Contract__c;
            }                       
        
          
      }
      
      
      //Get reporter name
      if(String.isNotBlank(strAccountId)) {
          List<Account> lstAcc = new List<Account>();
          lstAcc = [SELECT Name from Account where id=:strAccountId];
          if(lstAcc.size() > 0) {
              reporterName = lstAcc[0].Name;
              objFMCase.Reporter_Name__c = reporterName;
          }
      }
      //Get booking unit
      if(String.isNotBlank(strUnitId)) {
          List<Booking_Unit__c> lstBU = new List<Booking_Unit__c>();
          lstBU = [SELECT Unit_Name__c, Id,DLP_End_Date__c,DLP_Start_Date__c
                    FROM Booking_Unit__c WHERE Id =: strUnitId];
          if(lstBU.size() > 0) {
              objBU = lstBU[0];
          }
          System.debug('==objBU.Unit_Name__c===' + objBU.Unit_Name__c);
          //Get Location(Building) using unit name
          if(objBU != NULL && String.isNotBlank(objBU.Unit_Name__c)) {
              lstSelectOptions = new List<SelectOption>();
              lstSelectOptions.add(new SelectOption('None','None'));
              System.debug('==DLP_End_Date__c===' + String.valueOf(objBU.DLP_End_Date__c < Date.Today()));
              System.debug('==DLP_End_Date__c===' + objBU.DLP_End_Date__c);
              System.debug('==Date.Today()===' + Date.Today());
              
              
                  lstSelectOptions.add(new SelectOption('Inside unit','Inside unit'));
                  lstSelectOptions.add(new SelectOption('Common area','Common area'));
              
              List<Location__c> lstLocations = new List<Location__c>();
              String buildingCode = objBU.Unit_Name__c.substringBefore('/');
              System.debug('==buildingCode===' + buildingCode);
              lstLocations = [SELECT Name, ID,CAFM_Common_area__c,CAFM_Unit__c,
                                  (SELECT FM_User__c,FM_Role__c FROM FM_Users__r
                                  WHERE FM_Role__c = 'FM Admin'
                                  OR FM_Role__c='Master Community Admin' LIMIT 1)
                            FROM Location__c
                            WHERE RecordType.Name = 'Building'
                            AND Name =: buildingCode LIMIT 1];
              //Get CAFM buildings
              if(lstLocations.size() >0) {
                Set<Id> setCAFMBuildingIds = new Set<Id>();
                setCAFMBuildingIds.add(lstLocations[0].CAFM_Unit__c);
                setCAFMBuildingIds.add(lstLocations[0].CAFM_Common_area__c);
                  getCAFMBuildings(setCAFMBuildingIds);
                  if(lstLocations[0].FM_Users__r.size() > 0) {
                      objFMCase.OwnerId = lstLocations[0].FM_Users__r[0].FM_User__c;
                  }
              }
              
                  
              
              if(mapType_Building.size() == 0) {
                  showBuildings = false;
                  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'NO CAFM BUILDINGS'));
              }
          }
      }
  }
  public void getCAFMBuildings(set<Id> setCAFMBuildingIds) {
      System.debug('==setCAFMBuildingIds===' + setCAFMBuildingIds);
      for(CAFM_Building__c obj: [SELECT Id, Name,Building_Code__c,Unit_Code__c,
                                        CAFM_Contract__c,
                                        Recordtype.Name,
                                        Common_area_code__c
                                FROM CAFM_Building__c
                                WHERE ID IN: setCAFMBuildingIds]) {
                                    
         System.debug('==obj===' + obj);
        System.debug('==obj.CAFM_Contract__c===' + obj.CAFM_Contract__c);
        
        mapIdType_Building.put(obj.Id,obj.Recordtype.Name);
        
        mapType_Building.put(obj.Recordtype.Name,obj);
        mapId_Code.put(obj.Id, obj.CAFM_Contract__c);
        if(String.isNotBlank(obj.Common_area_code__c))
        mapCommonAreaCodevsBuildingId.put(obj.Common_area_code__c, obj.Id);
        if(String.isNotBlank(obj.Unit_Code__c))
        mapUnitCodevsBuildingId.put(obj.Unit_Code__c, obj.Id);
     }
     System.debug('==lstBuildingOptions===' + lstBuildingOptions);
  }
  public void getCAFMLocations() {
      lstLocationOptions = new list<SelectOption>();
      try {
          String query = 'SELECT Id, Name,Location_Id__c, Description__c,CAFM_Building__c, ';
          query += 'CAFM_Building__r.CAFM_Contract__c, CAFM_Building__r.CAFM_Contract__r.Contract_Id__c FROM CAFM_Location__c ';
          System.debug('=selectedOption==='+selectedOption);
          System.debug('=mapType_Building==='+mapType_Building);
          System.debug('=mapCommonAreaCodevsBuildingId==='+mapCommonAreaCodevsBuildingId);
          System.debug('=mapUnitCodevsBuildingId==='+mapUnitCodevsBuildingId);
          
          objFMCase.Helpdesk_Request_Area__c = selectedOption;
          System.debug('selectedOption==='+selectedOption);
          if(String.isNotBlank(selectedOption)) {
              String buildingSeqNo;
              Id recordTypeDLPId = Schema.SObjectType.CAFM_Contract__c.getRecordTypeInfosByName().get('DLP contract').getRecordTypeId();
              Id recordTypeFMId = Schema.SObjectType.CAFM_Contract__c.getRecordTypeInfosByName().get('FM contract').getRecordTypeId();
              lstContractOptions = new List<SelectOption>();
              lstContractOptions.add(new SelectOption('-None-','-None-'));
              
              if(mapType_Building.containsKey(selectedOption) && selectedOption.equalsIgnoreCase('Inside unit')) {
                  buildingSeqNo =  mapType_Building.get(selectedOption).Unit_Code__c;
                  String unitName= '%' + objBU.Unit_Name__c + '%';
                  query += 'WHERE Building_Sequence_no__c =: buildingSeqNo ';
                  query += 'AND Description__c like : unitName ';
                  //added to fetch contract to display drop down
                    List<CAFM_Building_Contract__c> lstContract =  [ Select id,Name,CAFM_Contract__c
                                                                    ,CAFM_Contract__r.RecordTypeId 
                                                                    ,CAFM_Contract__r.Name
                                                                    ,CAFM_Contract__r.Contract_Id__c
                                FROM CAFM_Building_Contract__c WHERE CAFM_Building__c =: mapUnitCodevsBuildingId.get(buildingSeqNo) ];
                    
                    for( CAFM_Building_Contract__c obj: lstContract) {
                       if(objBU.DLP_Start_date__c != Null && objBU.DLP_End_Date__c != Null 
                         && System.today() >= objBU.DLP_Start_date__c && System.today() <= objBU.DLP_End_Date__c) {
                             if(obj.CAFM_Contract__r.RecordTypeId == recordTypeDLPId) {
                                 lstContractOptions.add(new SelectOption(obj.CAFM_Contract__c,obj.CAFM_Contract__r.Name));
                                 mapContractIdvsContractName.put(obj.CAFM_Contract__c,obj.CAFM_Contract__r.Name);
                                 mapOdContractIdToContract.put(obj.CAFM_Contract__c,obj.CAFM_Contract__r.Contract_Id__c);
                             }
                         }
                         else {
                             //if(obj.CAFM_Contract__r.RecordTypeId == recordTypeFMId) {
                                 lstContractOptions.add(new SelectOption(obj.CAFM_Contract__c,obj.CAFM_Contract__r.Name));
                                 mapContractIdvsContractName.put(obj.CAFM_Contract__c,obj.CAFM_Contract__r.Name);
                                 mapOdContractIdToContract.put(obj.CAFM_Contract__c,obj.CAFM_Contract__r.Contract_Id__c);
                             //}
                             
                         }
                        
                    }
              }
              else if(mapType_Building.containsKey(selectedOption) && selectedOption.equalsIgnoreCase('Common area')) {
                  buildingSeqNo =  mapType_Building.get(selectedOption).Common_area_code__c;
                  query += 'WHERE Building_Sequence_no__c =: buildingSeqNo ';
                  
                     //added to fetch contract to display drop down
                    List<CAFM_Building_Contract__c> lstContract =  [ Select id,Name,CAFM_Contract__c,CAFM_Contract__r.RecordTypeId,CAFM_Contract__r.Default__c
                                ,CAFM_Contract__r.Name,CAFM_Contract__r.Contract_Id__c
                                FROM CAFM_Building_Contract__c WHERE CAFM_Building__c =: mapCommonAreaCodevsBuildingId.get(buildingSeqNo) ];
                    
                    System.debug('lstContract--->'+lstContract);
                    
                    for( CAFM_Building_Contract__c obj: lstContract){
                        if(obj.CAFM_Contract__r.Default__c){
                            DefaultFMcontract =obj.CAFM_Contract__c;
                        }
                       if(obj.CAFM_Contract__r.RecordTypeId == recordTypeFMId) {
                           lstContractOptions.add(new SelectOption(obj.CAFM_Contract__c,obj.CAFM_Contract__r.Name));
                           mapContractIdvsContractName.put(obj.CAFM_Contract__c,obj.CAFM_Contract__r.Name);
                           mapOdContractIdToContract.put(obj.CAFM_Contract__c,obj.CAFM_Contract__r.Contract_Id__c);
                       }
                        
                    }
              }
              
              
              if(String.isNotBlank(buildingSeqNo)) {
                  lstLocationOptions.add(new SelectOption('None', 'None'));
                  NoOFLocations = 0;
                  System.debug('query>>>'+query);
                  for(CAFM_Location__c obj: Database.query(query)) {
                      NoOFLocations++;
                    lstLocationOptions.add(new SelectOption(obj.Id, obj.Description__c));
                    mapId_Code.put(obj.Id, obj.Location_Id__c);
                    mapId_Code.put(obj.CAFM_Building__c, buildingSeqNo);
                    selectedBuilding = obj.CAFM_Building__c;
                    //selectedContract = obj.CAFM_Building__r.CAFM_Contract__c;
                    mapId_Code.put(selectedContract, obj.CAFM_Building__r.CAFM_Contract__r.Contract_Id__c);
                  }//for
              }
              System.debug('=lstLocationOptions==='+lstLocationOptions.size());
              if(NoOFLocations == 0) {
                  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'NO CAFM LOCATIONS'));
              }
        }//if
      }
      catch(System.Exception excp) {
        System.debug('==excp==='+excp);
        insert new Error_Log__c(Error_Details__c =excp.getMessage() + ': '+excp.getLineNumber(),
        FM_Case__c = objFMCase.Id,
        Process_Name__c = 'Helpdesk' );
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,excp.getMessage() + excp.getLineNumber()));
      }
  }


  
  public void saveAsDraft() {
      if(String.isNotBlank(strAccountId)) {
         
        objFMCase.Account__c = strAccountId;
      }
      if(String.isNotBlank(strUnitId)) {
        objFMCase.Booking_Unit__c = strUnitId;
      }
      
      System.debug('==selectedContractOption==='+selectedContractOption); 
      System.debug('==mapContractIdvsContractName==='+mapContractIdvsContractName);
      
     

 
      objFMCase.CAFM_Location__c = selectedLocation;
      objFMCase.status__c = 'Submitted';
      objFMCase.Request_Type_DeveloperName__c = 'Helpdesk';
      objFMCase.Push_To_CAFM__c = true;
     try {
      insert objFMCase;
      }
      catch(Exception e) {
          System.debug('e.getMessage()>>>>'+e.getMessage());
          //ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.FATAL,e.getMessage()));
      }
      System.debug('===objFMCase==' + objFMCase);
  }
  
  public void assignDetailsBuildingAndContract() {
      map<Id,String> contractToContratcId = new map<Id,String>();
      String ContractName = mapContractIdvsContractName.get(selectedContractOption);
      objFMCase.CAFM_Building__c = selectedBuilding;
      /***
      Logic to Stamp DLP in case of portal user 
      */
      List<User> lstPortalUser =  [Select Id from User Where IsPortalEnabled = true AND Id =: userinfo.getuserid()];
      if(!lstPortalUser.isEmpty()){
          List<CAFM_Building__c> lstCAFM_Building = [SELECT id,
                                                    ( Select id,CAFM_Contract__r.Name,CAFM_Contract__c,CAFM_Contract__r.Contract_Id__c 
                                                    FROM CAFM_Buildings_Contracts__r )
                                                    FROM CAFM_Building__c 
                                                    WHERE Id = : selectedBuilding
                                                ];
                                           
            
            if(!lstCAFM_Building.isEmpty()){
                for(CAFM_Building__c objBuild : lstCAFM_Building){
                    for(CAFM_Building_Contract__c objBuildcontract : objBuild.CAFM_Buildings_Contracts__r){
                        mapContractNamevsId.put(objBuildcontract.CAFM_Contract__r.Name,objBuildcontract.CAFM_Contract__c);
                        contractToContratcId.put(objBuildcontract.CAFM_Contract__c,objBuildcontract.CAFM_Contract__r.Contract_Id__c);
                    }
                }
            }
            
            String strDlpConName;
            String strFMConName;
            for(String strconName : mapContractNamevsId.keySet()){
                
                if(strconName.startsWithIgnoreCase('DLP')){
                    strDlpConName = strconName;
                }else if(strconName.startsWithIgnoreCase('FM')){
                    strFMConName = strconName;
                }
            }  
                
        if(selectedOption.equalsIgnoreCase('Inside Unit')) {
           if( objBU.DLP_End_Date__c != NULL 
                && objBU.DLP_End_Date__c >= system.Today() 
                && String.isNotBlank(strDlpConName)){
                objFMCase.CAFM_Contract__c = mapContractNamevsId.get(strDlpConName);
                contractOptionForPortal = objFMCase.CAFM_Contract__c;
                contractAssignedForFMCase = contractToContratcId.get(contractOptionForPortal);
            }
        }
        else if(selectedOption.equalsIgnoreCase('Common area')) {
            Id recordTypeFMId = Schema.SObjectType.CAFM_Contract__c.getRecordTypeInfosByName().get('FM contract').getRecordTypeId();
            List<CAFM_Building_Contract__c> cafmBuildingContractList = [Select Id,
                                                                                    CAFM_Building__c,
                                                                                    CAFM_Contract__c,
                                                                                    CAFM_Contract__r.Default__c,
                                                                                    CAFM_Contract__r.RecordTypeId,
                                                                                    CAFM_Contract__r.Contract_Id__c
                                                                            FROM CAFM_Building_Contract__c Where CAFM_Building__c =:lstCAFM_Building[0].Id
                                                                            And  CAFM_Contract__r.RecordTypeId =:recordTypeFMId
                                                                            ];
            for(CAFM_Building_Contract__c objContrat : cafmBuildingContractList) {
                    if(objContrat.CAFM_Contract__r.Default__c == true) {
                        objFMCase.CAFM_Contract__c = objContrat.CAFM_Contract__c;
                        contractOptionForPortal = objFMCase.CAFM_Contract__c;
                        contractAssignedForFMCase = objContrat.CAFM_Contract__r.Contract_Id__c;
                    }
            }
        }
            
        }else{
            
            /*String ContractName = mapContractIdvsContractName.get(selectedContractOption);
            if( ContractName.startsWithIgnoreCase('FM') && selectedOption.equalsIgnoreCase('Common area') ){
                objFMCase.CAFM_Contract__c = (Id)DefaultFMcontract;
            }else{*/
            objFMCase.CAFM_Contract__c = selectedContractOption;
            contractAssignedForFMCase = mapOdContractIdToContract.get(selectedContractOption);
           
            //}
        }
       /*List<User> lstPortalUser =  [Select Id from User Where IsPortalEnabled = true AND Id =: userinfo.getuserid()];
       System.debug('mapOdContractIdToContract>>>'+mapOdContractIdToContract);
      if(!lstPortalUser.isEmpty()){
          
          contractAssignedForFMCase = mapOdContractIdToContract.get(contractOptionForPortal);
      }
      else {
           contractAssignedForFMCase = mapOdContractIdToContract.get(selectedContractOption);
      }*/
      /*if( objBU.DLP_End_Date__c != NULL 
                && objBU.DLP_End_Date__c >= system.Today() && ContractName.startsWithIgnoreCase('FM')){
             
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
             'You Have Selected FM contract For Valid DLP unit'));
             return Null;
            }else if( (objBU.DLP_End_Date__c == NULL 
                || objBU.DLP_End_Date__c <= system.Today()) && ContractName.startsWithIgnoreCase('DLP')){
                
                     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                 'You Have Selected DLP contract For Invalid DLP unit'));
                 return Null;
        }
      System.debug('Category__c=======' + objFMCase.Category__c);*/
  }
  //Callout to raise ticket in CAFM
  public virtual PageReference submit() {
      
      
      
      lstErrLogs = new list<Error_Log__c>();
      assignDetailsBuildingAndContract();
      //createTaskINCAFM();
      saveAsDraft();
      for(Error_Log__c obj: lstErrLogs) {
          obj.FM_Case__c = objFMCase.Id;
      }
      insert lstErrLogs;
     if(objFMCase.ID != Null) {
      return new PageReference('/'+objFMCase.ID);
      }
      else {
      return null;
      }
  }


@TestVisible
  private static String getSessionIDfromCAFM() {
      try{
          wwwFsiCoUkServicesEvolution0409.SecurityService1 obj = new wwwFsiCoUkServicesEvolution0409.SecurityService1();
          obj.timeout_x = 120000;
          schemasDatacontractOrg200407FsiPlat.AuthenticationDto result = obj.Authenticate(Label.CAFM_Username,Label.CAFM_Password);
          system.debug('>>result>>>'+result);
          if(result != NULL) {
              if(result.OperationResult == 'Succeeded') {
                  System.debug('===result.SessionId=='+result);
                  return result.SessionId;
              }
          }

      }
      catch(System.Exception excp) {
          System.debug('==excp==SESSION ID='+excp);
          insert new Error_Log__c(Error_Details__c =excp.getMessage() + ': '+excp.getLineNumber(),
        // FM_Case__c = objFMCase.Id,
        Process_Name__c = 'Helpdesk' );
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,excp.getMessage() + excp.getLineNumber()));
      }
     return NULL;
  }

  /*public void createTaskINCAFM() {

      String sessionId;
      // objFMCase.Category__c = category;
      // objFMCase.Discipline__c = discipline;
      // System.debug('==objFMCase.Category__c==='+objFMCase.Category__c);
      System.debug('==objFMCase.Discipline__c==='+objFMCase.Discipline__c);
      //Get session id
      if(Test.isRunningTest()){
          sessionId = '02p2lvq220wzp555vxny2m55';
      }else{
          sessionId = getSessionIDfromCAFM();
      }
      System.debug('==sessionId='+sessionId);
      if(String.isNotBlank(sessionId)) {
          try {
              wwwFsiCoUkServicesEvolution0409_t32.TaskServiceV3 tobj = new wwwFsiCoUkServicesEvolution0409_t32.TaskServiceV3();
              schemasDatacontractOrg200407FsiConc_t32.BreakdownTaskDtoV3  taskToCreate = new schemasDatacontractOrg200407FsiConc_t32.BreakdownTaskDtoV3();
              schemasDatacontractOrg200407FsiConc_t32.BreakdownTaskDtoV3 taskCreated = new schemasDatacontractOrg200407FsiConc_t32.BreakdownTaskDtoV3();
              tobj.timeout_x = 120000;
              taskToCreate.BuildingId = Long.valueOf(mapId_Code.get(selectedBuilding));
              taskToCreate.Category = objFMCase.Category__c;//'HVAC';
              //taskToCreate.ContractId = Long.valueOf(mapId_Code.get(selectedContract));
              System.debug('contractAssignedForFMCase>>>>'+contractAssignedForFMCase);
               taskToCreate.ContractId = Long.valueOf(contractAssignedForFMCase);

              // taskToCreate.Description = objFMCase.Description__c;//'Breakdown';
              taskToCreate.Discipline = objFMCase.Discipline__c;//'HVAC TECHNICIAN';
              // taskToCreate.InstructionId = 1;
              taskToCreate.LocationId = Long.valueOf(mapId_Code.get(selectedLocation));//8;
              taskToCreate.LongDescription = objFMCase.Long_Description__c;//'Test task creation through Salesforce';
              taskToCreate.Priority = objFMCase.Task_Priority__c;//'P1 - EMERGENCY';
              String dateFormat = 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'';
              taskToCreate.RaisedDateUtc = (objFMCase.Raised_Date__c.format(dateFormat));
              taskToCreate.ShortDescription = objFMCase.Short_Description__c;//'A/C is noisy';
              taskToCreate.ReporterName = reporterName;//objFMCase.Reporter_Name__c;//'Rajeevan Edavan';
              // taskToCreate.ServiceType = objFMCase.Service_Type__c;//'NonServiceCall';
              System.debug('==taskToCreate==='+taskToCreate);
              taskCreated = tobj.CreateBreakdownTask(sessionId,taskToCreate);
              system.debug('>>>taskCreated>>>'+taskCreated);
              if(String.isBlank(taskCreated.FaultCode) && String.isNotBlank(String.valueOf(taskCreated.TaskId))) {
                  objFMCase.Task_Id__c = taskCreated.TaskId;
              }

              // upsert objFMCase;
              // upsert objBU;
              System.debug('==aa==='+taskCreated);
          }
          catch(System.Exception excp) {
              System.debug('==excp==='+excp);
              lstErrLogs.add(new Error_Log__c(Error_Details__c =excp.getMessage() + ': '+excp.getLineNumber(),
        FM_Case__c = objFMCase.Id,
        Process_Name__c = 'Helpdesk' ));
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,excp.getMessage() + excp.getLineNumber()));
          }
      }
  }*/
  webservice static void fetchTaskDetails(String FMCaseId){
          String sessionId;
          // try {
              // System.debug('===taskId==='+taskId);
              System.debug('===taskId==='+FMCaseId);
              // Query FM case
              List<FM_Case__c> lstFMCases = new List<FM_Case__c>();
              lstFMCases = [SELECT task_id__c, Id from FM_Case__c
                            WHERE Id =: FMCaseId
                            AND task_id__c != NULL];
            if(lstFMCases.size() > 0) {
                //Get session id
                Integer taskID = Integer.valueOf(lstFMCases[0].task_id__c);
                sessionId = getSessionIDfromCAFM();
                schemasDatacontractOrg200407FsiConc_t32.TaskDtoV3 CAFMTaskObj =
                    new schemasDatacontractOrg200407FsiConc_t32.TaskDtoV3();
                  wwwFsiCoUkServicesEvolution0409_t32.TaskServiceV3  obj = new wwwFsiCoUkServicesEvolution0409_t32.TaskServiceV3 ();
                CAFMTaskObj = obj.GetTaskById(sessionId,taskID);
                System.debug('===CAFMTaskObj==='+CAFMTaskObj.LongDescription);
                FM_Case__c objFMCase = new FM_Case__c();
                objFMCase.Id = FMCaseId;
                objFMCase.CAFM_Task_Code__c = CAFMTaskObj.Code;
                objFMCase.Long_Description__c = CAFMTaskObj.LongDescription;
                objFMCase.Task_Priority__c = CAFMTaskObj.Priority;
                objFMCase.Short_Description__c = CAFMTaskObj.ShortDescription;
                objFMCase.CAFM_Task_State__c = CAFMTaskObj.State;
                objFMCase.CAFM_Description__c = CAFMTaskObj.Description;
                
                update objFMCase;
            }

          

      }

}