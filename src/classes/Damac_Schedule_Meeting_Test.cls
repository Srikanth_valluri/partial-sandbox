@isTest 
public class Damac_Schedule_Meeting_Test
{
    static testMethod void testMethod1() {
        Inquiry__c inq = New Inquiry__c ();
        inq.First_Name__c = 'testInq';
        inq.Preferred_Language__c = 'English';
        inq.Last_Name__c ='inqLast';
        inq.Inquiry_Source__c = 'Prospecting';
        inq.Primary_Contacts__c = 'Mobile Phone';
        inq.Mobile_CountryCode__c = 'India: 0091';
        inq.Mobile_Phone__c = '1236547890';
        inq.Mobile_Phone_Encrypt__c = '1223467886';
        inq.Meeting_Due_Date__c = Date.Today ();
        inq.Email__c = 'test@gmail.com';
        insert inq;
        Task t = new Task ();
            t.Subject='Donni';
            t.Status='New';
            t.Priority='Normal';
            t.CallType='Outbound';
        insert t;
            
        ApexPages.currentPage().getParameters().put('id',inq.id);
        ApexPages.standardController std = new ApexPages.standardController (inq);
        Damac_Schedule_Meeting objStd = New Damac_Schedule_Meeting(std);
        Damac_Schedule_Meeting obj = New Damac_Schedule_Meeting();
        obj.getLocation();
        obj.getDevelopement();
        obj.createMeetingFollowup ();
        
    }
    

}