public class UpdateDPCallingListBatchScheduler Implements Schedulable  {
     public void execute(SchedulableContext SC){
        System.debug('inside scheduler');
        UpdateDPCallingListBatch batchInst = new UpdateDPCallingListBatch();
        Database.executeBatch(batchInst);
    }
}