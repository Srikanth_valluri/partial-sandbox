/****************************************************************************************************
* Name          : ServiceRequestAgentSharingTest                                                    *
* Description   : Test Class for ServiceRequestAgentSharingTest                                     *
* Created Date  : 01/07/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE            COMMENTS                                                *
* 1.1   Twinkle P           01/07/2018      Intial Draft                                            *
*****************************************************************************************************/
@isTest 
public class ServiceRequestAgentSharingTest {
    
    @isTest 
    static void test_method_one() {
        Id queueId = [  SELECT Id 
                      FROM Group
                      WHERE Type = 'Queue' 
                      AND DeveloperNAME = 'Agent_Admin_Team' 
                      LIMIT 1].Id;
        //Create account
        Account portalAccount1 = new Account(
            Name = 'TestAccount'
        );
        Database.insert(portalAccount1);
        
        
        //create SR Status
        NSIBPM__SR_Status__c SRStatus = new NSIBPM__SR_Status__c(Name = 'Submitted', NSIBPM__Code__c = 'SUBMITTED');
        insert SRstatus;
        
        //Create contact for the account, which will enable partner portal for account
        Contact contact1 = new Contact(
            FirstName = 'Test',
            Lastname = 'Amit',
            AccountId = portalAccount1.Id,
            Email = 'test@test.com'
        );
        Database.insert(contact1); 
        
        //Create user for the contact
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name = 'Customer Community - Agent + Admin + Auth' Limit 1];
        User user1 = new User(
            Username = 'test12345test@test.com',
            ContactId = contact1.Id,
            ProfileId = portalProfile.Id,
            Alias = 'test123',
            Email = 'test12345@test.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'Kumar',
            CommunityNickname = 'test12345',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US'
        );
        Database.insert(user1); 
        
        system.runAs(user1){
            NSIBPM__Service_Request__c serviceRequest = new NSIBPM__Service_Request__c();
            serviceRequest.DP_ok__c = true;
            serviceRequest.Doc_ok__c = true;
            serviceRequest.Registration_Date__c = System.today();
            serviceRequest.NSIBPM__Internal_SR_Status__c = SRstatus.Id;
            serviceRequest.recordTypeId = Schema.Sobjecttype.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Agent Update').getRecordTypeId();
            insert serviceRequest;
            List<NSIBPM__Service_Request__c> serviceRequestList = [SELECT Id, OwnerId FROM NSIBPM__Service_Request__c LIMIT 2];
            system.assertEquals(queueId, serviceRequestList[0].OwnerId);
        }     
        
    }
    @isTest 
    static void test_method_two() {
        User damacUserObj = [SELECT Name,ManagerId  FROM user WHERE Name= 'Damac Admin' LIMIT 1];
        //Create account
        Account portalAccount1 = new Account(
            Name = 'TestAccount'
        );
        Database.insert(portalAccount1);
        
        //Create contact for the account, which will enable partner portal for account
        Contact contact1 = new Contact(
            FirstName = 'Test',
            Lastname = 'Amit',
            AccountId = portalAccount1.Id,
            Email = 'test@test.com'
        );
        Database.insert(contact1); 
        
        //Create user for the contact
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name = 'Customer Community - Agent + Admin + Auth' Limit 1];
        User user1 = new User(
            Username = 'test12345test@test.com',
            ContactId = contact1.Id,
            ProfileId = portalProfile.Id,
            Alias = 'test123',
            Email = 'test12345@test.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'Kumar',
            CommunityNickname = 'test12345',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US'
        );
        Database.insert(user1); 
        
        system.runAs(user1){
            NSIBPM__Service_Request__c serviceRequest = new NSIBPM__Service_Request__c();
            serviceRequest.DP_ok__c = true;
            serviceRequest.Doc_ok__c = true;
            serviceRequest.Registration_Date__c = System.today();
            serviceRequest.recordTypeId = Schema.Sobjecttype.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
            insert serviceRequest;
            List<NSIBPM__Service_Request__c> serviceRequestList = [SELECT Id, OwnerId,Manager_For_Agent_Portal__c FROM NSIBPM__Service_Request__c LIMIT 2];
            system.assertEquals(damacUserObj.ManagerId, serviceRequestList[0].Manager_For_Agent_Portal__c);
        }     
        
    }
}