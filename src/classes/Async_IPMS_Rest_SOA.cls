/*
Queueble Class to get the SOA URLs from the Process URL stored on Booking
See Also:IPMS_REST_SOA_getProcessID Class prior to this
Developed By: DAMAC IT TEAM
*/

global class Async_IPMS_Rest_SOA implements Queueable, Database.AllowsCallouts {

    public string bookingid;
    
    public string processURL;
    
    public Async_IPMS_Rest_SOA(string IPMSprocessURL,string id){
            bookingid = id;
            processURL = IPMSprocessURL;
    }
    
    public void execute(QueueableContext context) {        
        POLLForSOAAsync(processURL,bookingid); // Do Callout
    }
    
    
    public void POLLForSOAAsync(string processURL,string bookingId){
        try{
            boolean requestSuccess;
            processURL = processURL.remove('"');
            Http h = new Http();
            HttpRequest req = new HttpRequest();        
            req.setEndpoint(processURL);        
            req.setMethod('GET');
            req.setheader('Content-Type','application/json');        
            system.debug(req.getbody().length());        
            // Send the request, and return a response
            HttpResponse res = h.send(req);       
            system.debug(res.getBody());
            //return res.getBody();
            if(res.getStatusCode() != 200){
                requestSuccess = false;
                createLog(bookingId,res.getBody());
            }else{
                IPMS_JSON_SPA_Response respCls = new IPMS_JSON_SPA_Response();
                String json = res.getBody();
                respCls  = IPMS_JSON_SPA_Response.parse(json);                    
                system.debug('RESPONSE*********'+respCls.Results.size()); 
                if(respCls != null){
                    Update_SF(processURL,respCls);
                }
            }
        }catch(exception  e){
            createLog(bookingId,e.getmessage());
        } 
    }
    
    public void Update_SF(string processURL,IPMS_JSON_SPA_Response resp){
        boolean pollAgain=false;    
        boolean hasError=false;  
        string bookingId;  
        bookingId = resp.extRequestKey;        
        string processId = string.valueof(bookingId)+'-'+resp.requestStart;
        if(resp.requestStatus == 'C'){ // Completed
            pollAgain = false;
        }else if(resp.requestStatus == 'P'){ // In Progress
            pollAgain = true;   
        }
        else if(resp.requestStatus == 'E'){ // Error
            hasError = true;
            createLog(bookingId ,string.valueof(resp));
        }
        
        if(!hasError){   // No Error continue parsing     
            map<string,string> SOAMap = new map<string,string>();
            
            // Process what has come
            if(resp.results.size()>0){
                for(integer i=0;i<resp.results.size();i++){
                    if(resp.results[i].reportName == 'SOA'){
                        SOAMap.put(resp.results[i].registrationId,resp.results[i].reportURL);
                    } 
                    if(resp.results[i].reportName == 'SOA-SPA'){
                        SOAMap.put(resp.results[i].registrationId,resp.results[i].reportURL);
                    }  
                                      
                }
                // Prepare RegId Set
                set<string> regIds = new set<string>();                
                regIds.addall(SOAMap.keyset());
                Booking__c booking = new booking__c();
                if(!test.isrunningtest()){
                // Query Units and upsert  Docs
                booking = [select id,
                                Poll_for_SOA__c,
                                Poll_for_SOA_Count__c,
                                (select id,Registration_ID__c,booking__r.Deal_SR__c from Booking_units__r) 
                                from booking__c where id=:bookingId LIMIT 1]; 
                }else{
                    // Query Units and upsert  Docs
                    booking = [select id,
                                Poll_for_SOA__c,
                                Poll_for_SOA_Count__c,
                                (select id,Registration_ID__c,booking__r.Deal_SR__c from Booking_units__r) 
                                from booking__c where Booking_Channel__c='Office' and Deal_SR__r.ownerid=:userinfo.getuserid() LIMIT 1]; 
                }
                
                try{
                    list<Unit_Documents__c> Docstoinsert = new list<Unit_Documents__c>();
                    //list<NSIBPM__SR_Doc__c> srDocs = new list<NSIBPM__SR_Doc__c>();
                    for(booking_unit__c bu:booking.Booking_units__r){
                        /*
                        NSIBPM__SR_Doc__c srDoc = new NSIBPM__SR_Doc__c();
                        srDoc.Name = bu.registration_id__c+'-'+LABEL.SOA_SR_DOC_NAME;
                        srDoc.IPMS_Document_ID__c = bu.registration_id__c+'-'+processId;
                        srDoc.booking_unit__c = bu.id;
                        srDoc.NSIBPM__Service_Request__c = bu.booking__r.Deal_SR__c;
                        srDoc.DP_INVOICE__c = dpMap.get(bu.registration_id__c);
                        srDoc.OPTION__c = optionMap.get(bu.registration_id__c);
                        srDoc.PROMOTION__c = promotionMap.get(bu.registration_id__c);
                        srDoc.SOA__c = SOAMap.get(bu.registration_id__c);
                        srDocs.add(srDoc);
                        */
                        
                        Unit_Documents__c doc = new Unit_Documents__c();
                        doc.Document_type__c = 'SOA';
                        doc.Registration_Id__c = bu.registration_id__c+'-'+processId;
                        doc.booking_unit__c = bu.id;                        
                        doc.SOA__c = SOAMap.get(bu.registration_id__c);
                        doc.Booking__c = booking.id;
                        doc.Service_Request__c = bu.booking__r.Deal_SR__c;
                        Docstoinsert.add(doc);
                        
                    }
                    system.debug('Entered');
                    if( pollAgain && (booking.Poll_for_SOA_Count__c <= 100 || booking.Poll_for_SOA_Count__c == null)){
                        system.debug('Entered');
                        booking.Poll_for_SOA__c = true;
                        if(booking.Poll_for_SOA_Count__c != null){
                            booking.Poll_for_SOA_Count__c = booking.Poll_for_SOA_Count__c+1;
                        }else{
                            booking.Poll_for_SOA_Count__c = 1;
                        }
                        system.enqueueJob(new Async_IPMS_Rest_SOA(ProcessURL,bookingId));
                        //pollmore =true;
                    }else{
                        system.debug('Entered false');
                        booking.Poll_for_SOA__c = false;
                        booking.Poll_for_SOA_Count__c = 0;
                    } 
                    upsert Docstoinsert Registration_Id__c ;
                    //upsert srDocs IPMS_Document_ID__c;
                    update booking;  
                }catch(exception e){
                    createLog(bookingId ,string.valueof(e.getMessage()));
                }         
            } 
        } 
    }
    
    //Create Log for Error
    public void createLog(string parentId,String ErrorMessage){
        try{
            log__c log = new log__c();
            log.Booking__c = parentId;
            log.Description__c = ErrorMessage;
            insert log;
        }catch(exception e){}
    }
    

}