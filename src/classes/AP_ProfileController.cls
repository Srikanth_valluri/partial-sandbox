/**************************************************************************************************
* Name               : AP_ProfileController
* Description        : An apex page controller for AP_Profile
* Created Date       : 1/10/2018
* Created By         : Nikhil Pote
* Last Modified Date :
* Last Modified By   : 
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR                    DATE
* 1.0         Nikhil Pote               1/10/2018
* 1.1         Srikanth V                11/06/2020
*************************************************************************************************/
public without sharing class AP_ProfileController {
    
    public static final String success                                          = 'Update Success!!';
    public static final String error                                            = 'Update Failed!!';
    public Id regRecordTypeId 
        = Schema.Sobjecttype.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Agent Registration').getRecordTypeId();
    public Id updateRecordTypeId 
        = Schema.Sobjecttype.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Agent Update').getRecordTypeId();
    public Boolean showdetails;
    public Boolean showdetailsNone;  
    public static NSIBPM__Service_Request__c serviceRequest { get; set; }
    public Id ContactID;
    public static String oldPassword                                                    {get; set;}
    public static String newPassword                                                    {get; set;}
    public static String confirmNewPassword                                             {get; set;}
    public static String message                                                        {get; set;}
    public String show                                                                  {get; set;}
    public String currentLang                                                           {get; set;}
    public String regNumber                                                             {get; set;}
    public String serviceRequestID                                                      {set; get;}
    public String companyServiceRequestID                                               {set; get;}
    public String serviceRequestStatus                                                  {set; get;}
    public String selectedRole                                                          {set; get;}
    public Date  certiDate                                                              {get; set;}
    public Id accountId                                                                 {get; set;}
    public Boolean isSRPending                                                          {set; get;}
    public Account accountDetail                                                        {set; get;}
    public List<NSIBPM__Service_Request__c> lstSR                                       {get; set;}
    public List<Contact> lstContacts                                                    {set; get;}
    public String strCountryOfSale                                                      {get; set;}
    public String strSelectedLanguage                                                   {get; set;}
    public boolean isAgent {get;set;}

    public List <Partnership_Program__c> damacUnities { get; set; }
    public List <Stand_Partnership_Program__c> partnershipPrograms { get; set; }
    public List<SelectOption> lstRoles {
        get {
            if (lstRoles == null) {
                lstRoles = getlstRoles();
            }
            return lstRoles;
        }
        private set;
    }
    public String getUserPhotoUrl() {
        //return CustomerCommunityUtils.getFullPhotoUrl();
        return '';
    }

    

    
    public AP_ProfileController() {
        isAgent = false;
        accountId = UtilityQueryManager.getAccountId();
        
        /*1.1 To display the  damac Unities and partner ship programs on Profile page of community user */
        damacUnities = new List <Partnership_Program__c> ();
        partnershipPrograms = new List <Stand_Partnership_Program__c> ();
        
        
        
        partnershipPrograms = [SELECT Name, Stand_Location__c, Cost_of_Sales__c, Actual_Sales_Achieved__c, From_Date__c, To_Date__c 
                                FROM Stand_Partnership_Program__c WHERE Account__c =: accountId];
        damacUnities = [SELECT Name, Damac_Unity__c, Start_date__c, End_Date__c FROM Partnership_Program__c WHERE Active__c = TRUE AND Account__c =: accountId ];
        
        strCountryOfSale = '';
        isSRPending = false;
        
        User objUser = [ SELECT toLabel(LanguageLocaleKey), ContactID, AccountId 
                           FROM User 
                          WHERE Id = :UserInfo.getUserId()
        ];
        System.debug('objUser-----'+objUser);
        currentLang = objUser.LanguageLocaleKey;
        System.debug(currentLang);
        ContactID = objUser.ContactID;
        User portaluser = new User();
        Contact portalcon = new Contact();
        strSelectedLanguage = apexpages.currentPage().getParameters().get('langCode');
        portaluser = [Select accountid,contactid,id from User where id = :Userinfo.getuserid()];
        if(portaluser.contactid != null){
            portalcon = [ Select id,Accountid, Owner__c,Agent_Representative__c, Authorised_Signatory__c, Portal_Administrator__c from Contact where id = :portaluser.contactid];
        }
        if(portalcon != null && portalcon.Agent_Representative__c == true && portalcon.Owner__c == false && portalcon.Portal_Administrator__c == false && portalcon.Authorised_Signatory__c == false) {
            isAgent = true;
        }
        if(portalcon != null && portalcon.Agent_Representative__c == true) {
            
            lstSR = [SELECT Id, SR_Type__c, Agency__c, Name, NSIBPM__External_Status_Name__c, 
                        CreatedBy.Name, createdBy.lastname, createdDate , NSIBPM__Internal_Status_Name__c
                   FROM NSIBPM__Service_Request__c
                  WHERE (Agency__c = :objUser.AccountId 
                            OR NSIBPM__Customer__c = :objUser.AccountId
                        ) 
                    AND (RecordType.DeveloperName = 'Agent_Update' 
                            OR RecordType.DeveloperName = 'Agent_Registration'
                        )
                    AND createdBy.id =:userinfo.getuserid()
            ];
        } else {
            lstSR = [SELECT Id, SR_Type__c, Agency__c, Name, NSIBPM__External_Status_Name__c, 
                        CreatedBy.Name, createdBy.lastname, createdDate,NSIBPM__Internal_Status_Name__c
                   FROM NSIBPM__Service_Request__c
                  WHERE (Agency__c = :objUser.AccountId 
                            OR NSIBPM__Customer__c = :objUser.AccountId
                        ) 
                    AND (RecordType.DeveloperName = 'Agent_Update' 
                            OR RecordType.DeveloperName = 'Agent_Registration'
                        )
            ];
        }

        
        System.debug('lstSR-----'+lstSR);
        accountDetail = new Account();
        accountDetail = UtilityQueryManager.getCompanyProfileDetail(accountId);
        if (accountDetail.Country_of_Sale__c != null && accountDetail.Country_of_Sale__c != '') {
            strCountryOfSale = accountDetail.Country_of_Sale__c;
            strCountryOfSale = strCountryOfSale.replaceAll(';',', ');
        }
        init();
        System.debug('accountId----  '+accountId);
        for(NSIBPM__Service_Request__c thisSR : [SELECT NSIBPM__Internal_Status_Name__c,SR_Type__c,
                                                        NSIBPM__Customer__c, NSIBPM__Record_Type_Name__c,
                                                        Id, Name,BPMSR_Template_Name__c 
                                                   FROM NSIBPM__Service_Request__c
                                                  WHERE NSIBPM__Internal_Status_Name__c != 'Approved' 
                                                    AND NSIBPM__Internal_Status_Name__c != 'Rejected'
                                                    AND NSIBPM__Internal_Status_Name__c != 'Closed'
                                                    AND NSIBPM__Customer__c = :accountId
                                                    
                                                    AND (NSIBPM__Record_Type_Name__c = 'Agent_Registration' OR
                                                        NSIBPM__Record_Type_Name__c = 'Agent_Update')
                                                    //BPMSR_Template_Name__c != 'VAT Update' 
                                                    
                                               ORDER BY CreatedDate Desc
                                                  
        ]){
            
            
            
            
            if(thisSR.SR_Type__c == 'Company SR')
                companyServiceRequestID = thisSR.Id;
            
            if(thisSR.SR_Type__c == NULL){
                serviceRequestID = thisSR.Id;
                isSRPending = true;
                serviceRequest = thisSR;
                serviceRequestStatus = thisSR.NSIBPM__Internal_Status_Name__c;
            }
           // serviceRequestID = thisSR.Id;
            
        }
        System.debug('==serviceRequest=='+serviceRequest);
        // Commented because of LOI functionality
        //serviceRequest = UtilityQueryManager.checkPendingSR(accountId);
        //if(null != serviceRequest){
        //    isSRPending = true;
        //    serviceRequestID = serviceRequest.Id;
        //}
        
        Agent_Site__c[] AgSite = [SELECT Tax_Registration_Number__c,Registration_Certificate_Date__c 
                                    FROM Agent_Site__c 
                                   WHERE Agency__c = :accountId 
                                     AND Name = 'UAE'
        ];
        
        System.debug('AgSite----  '+AgSite);
        if (AgSite.Size() > 0) {
            regNumber = AgSite[0].Tax_Registration_Number__c;
            certiDate = AgSite[0].Registration_Certificate_Date__c;
        }
    }
    public void init(){
        System.debug('...ContactID...'+ContactID);
        System.debug('...accountId...'+accountId);
        lstContacts = new List<Contact>();
        List<Contact> lstContacts2 = new List<Contact>();
        List<Contact> lstContacts1 = new List<Contact>();
        for (Contact objCont : UtilityQueryManager.getAllContacts2(ContactID)) {
            System.debug('...con.Status__c...'+objCont.id);
            if(selectedRole != null){
                if(selectedRole == 'Portal_Administrator__c'){
                    if (objCont.Portal_Administrator__c ) {
                        lstContacts2.add(objCont);
                    }
                } else if(selectedRole == 'Agent_Representative__c'){
                    if (objCont.Agent_Representative__c) {
                        lstContacts2.add(objCont);
                    }
                } else if(selectedRole == 'Authorised_Signatory__c'){
                    if (objCont.Authorised_Signatory__c) {
                        lstContacts2.add(objCont);
                    }
                } else if(selectedRole == 'Owner__c'){
                    if (objCont.Owner__c ) {
                        lstContacts2.add(objCont);
                    }
                }
            }else{
                if (objCont.Status__c != 'Cancelled') {
                    lstContacts2.add(objCont);
                }
            }

        }
        for (Contact objContact : UtilityQueryManager.getAllContacts(accountId)) {
            System.debug('...con.Status__c2...'+objContact.Status__c);
            System.debug('...selectedRole..'+selectedRole);
            if(selectedRole != null && selectedRole != 'All' ){
                if(selectedRole == 'Portal_Administrator__c'){
                    if (objContact.Status__c != 'Cancelled' && 
                        objContact.Portal_Administrator__c ) {
                        lstContacts1.add(objContact);
                    }
                } else if(selectedRole == 'Agent_Representative__c'){
                    if (objContact.Status__c != 'Cancelled' 
                        && objContact.Agent_Representative__c) {
                        lstContacts1.add(objContact);
                    }
                } else if(selectedRole == 'Authorised_Signatory__c'){
                    if (objContact.Status__c != 'Cancelled' && 
                        objContact.Authorised_Signatory__c) {
                        lstContacts1.add(objContact);
                    }
                } else if(selectedRole == 'Owner__c'){
                    if (objContact.Status__c != 'Cancelled' && objContact.Owner__c ) {
                        lstContacts1.add(objContact);
                    }
                }
            }else{
                if (objContact.Status__c != 'Cancelled') {
                    System.debug('>>>>InElse');
                    lstContacts1.add(objContact);
                    System.debug('>>>>lstContacts'+lstContacts);
                }
            }
        }
        System.debug('>>>>lstContacts>>>>>'+lstContacts);
        System.debug('>>>>lstContacts2>>>>>'+lstContacts2);
        List<Contact> lstCon= [SELECT Id,
                                            Status__c,
                                            Owner__c,
                                            Authorised_Signatory__c,
                                            Portal_Administrator__c,
                                            Agent_Representative__c,
                                            Salesforce_User__r.username,
                                            Salesforce_User__c
                                       FROM Contact
                                      WHERE ID =: ContactID];
        for(Contact objCon :lstCon) {
            System.debug('...objCon.Owner__c....'+objCon.Owner__c);
            System.debug('..objCon.Portal_Administrator__c....'+objCon.Portal_Administrator__c);
            System.debug('...objCon.Authorised_Signatory__c....'+objCon.Authorised_Signatory__c);
            System.debug('...inside else....');
                if(    objCon.Owner__c 
                    || objCon.Portal_Administrator__c 
                    || objCon.Authorised_Signatory__c 
                ){

                    showdetails = true;
                    showdetailsNone =false;
                } else {
                    System.debug('...inside else....');
                    showdetails = false;
                    showdetailsNone = true;
                }
            }
            System.debug('>>>>showdetails>>>>>'+showdetails);
            System.debug('>>>>showdetailsNone>>>>>'+showdetailsNone);
            if(showdetails) {
                lstContacts.addAll(lstContacts1);
            } else {
                lstContacts.addAll(lstContacts2);
            }

            
    }
    @TestVisible private List<SelectOption> getlstRoles() {
        List<SelectOption> optionList = new List<SelectOption>();
        
        try {
            optionList.add(new selectOption('All', 'All'));
            optionList.add(new selectOption('Portal_Administrator__c', 'Portal Administrator'));
            optionList.add(new selectOption('Agent_Representative__c', 'Agent Representative'));
            optionList.add(new selectOption('Authorised_Signatory__c', 'Authorised Signatory'));
            optionList.add(new selectOption('Owner__c', 'Owner'));
        }
        catch(Exception ex) {
            System.debug('Unable to get field details');
        }
        return optionList;
    }

    @RemoteAction
    public static void clearFields() {
        confirmNewPassword = '';
        newPassword = '';
        oldPassword = '';
        message = '';
    }

    @RemoteAction
    public static String updatePassword(String pNewPassword, String pConfirmNewPassword, String pOldPassword) {
        message = '';

        newPassword = pNewPassword;
        confirmNewPassword = pConfirmNewPassword;
        oldPassword = pOldPassword;
        system.debug('**sidebar confirmNewPassword >>> '+confirmNewPassword);
        system.debug('**sidebar newPassword >>> '+newPassword);
        system.debug('**sidebar oldPassword >>> '+oldPassword);
        if (String.isNotBlank(confirmNewPassword) 
            && String.isNotBlank(newPassword) 
            && String.isNotBlank(oldPassword)
        ) {
            if (newPassword == confirmNewPassword) {
                if (!Test.isRunningTest()) {
                    PageReference pageRef = Site.changePassword(newPassword, confirmNewPassword, oldPassword); 
                }
                List<Apexpages.Message> msgs = ApexPages.getMessages();
                for(Apexpages.Message msg : msgs){
                    String severity = String.valueOf(msg.getSeverity());
                    system.debug('**sidebar severity >>> '+severity);
                    if (severity == 'ERROR') {
                        message = msg.getDetail();
                    }
                }
                if (message == '' ) {
                    message = success; 
                }
            } else {
                message = ' Error: Please enter same value for new password and confirm password ';
            }
        } else {
            message = ' Error: Please enter value for the below fields ';
        }
        return message;

    }
    @RemoteAction
    public static String createAgentSR (ID accId) {
        NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
        sr.Agency_Type__c = 'Corporate';
        sr.RecordTypeId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Agents Portal SR').getRecordTypeId();
        sr.NSIBPM__SR_Template__c = null;                
        sr.NSIBPM__Customer__c = accId;
        sr.SR_Type__c = 'Agent SR';
        insert sr;
        return sr.Id;        
        
    }
    @RemoteAction
    public static String checkCompanySR (ID accId ) {
        String returnId = accId;
        try {
            Boolean registrationSrFound = false;
            Boolean updateSrFound = false;
            for (NSIBPM__Service_Request__c sr :[SELECT NSIBPM__Internal_Status_Name__c, NSIBPM__Customer__r.Vendor_ID__c 
                                                    FROM NSIBPM__Service_Request__c 
                                                WHERE NSIBPM__Record_Type_Name__c = 'Agent_Registration' 
                                               
                                                AND NSIBPM__Customer__c = :accId Order By CreatedDate Desc LIMIT 1])
            {
                registrationSrFound = true;
                if (sr.NSIBPM__Internal_Status_Name__c == 'Draft') {
                    returnId = 'Agent registration is in progress.';
                }
                else if (sr.NSIBPM__Internal_Status_Name__c == 'Submitted') {
                    returnId = 'Agent updation is in progress.';
                }
                if (sr.NSIBPM__Internal_Status_Name__c == 'Approved') {
                    returnId = '';
                }
                if (sr.NSIBPM__Internal_Status_Name__c == 'Closed') {
                    returnId = '';
                }
                if (sr.NSIBPM__Internal_Status_Name__c != 'Closed'  
                    && sr.NSIBPM__Internal_Status_Name__c != 'Draft' && sr.NSIBPM__Internal_Status_Name__c != 'Approved') {
                    returnId = 'Agent registration is in progress.';
                }                
            } 
            
            if (returnId == '' || returnId == accId ) {
                for (NSIBPM__Service_Request__c sr :[SELECT NSIBPM__Internal_Status_Name__c, NSIBPM__Customer__r.Vendor_ID__c 
                                                    FROM NSIBPM__Service_Request__c 
                                                WHERE NSIBPM__Record_Type_Name__c = 'Agent_Update' 
                                                AND SR_Type__c = 'Company SR'
                                                AND NSIBPM__Customer__c = :accId Order By CreatedDate DESC LIMIT 1])
                {
                    updateSrFound = true;
                    System.debug (sr.NSIBPM__Internal_Status_Name__c );
                    
                    if (sr.NSIBPM__Internal_Status_Name__c == 'Draft') {
                        returnId = sr.Id;
                    }
                    else if (sr.NSIBPM__Internal_Status_Name__c == 'Submitted') {
                        returnId = 'Agent updation is in progress.';
                    }
                    else if (sr.NSIBPM__Internal_Status_Name__c == 'Closed') {
                        returnId = accId;
                    }
                    else if (sr.NSIBPM__Internal_Status_Name__c == 'Approved') {
                        returnId = accId;
                    } else {
                        returnId = 'Agent updation is in progress.';
                    }
                    
                }
                if (updateSrFound == false) {
                    returnId = accId;
                }
            } 
            
        } catch (Exception e) {}
        return returnId;
    }
    
    @RemoteAction
    public static String checkEditSR (ID accId) {
        String returnId = accId;
        try {
            for (NSIBPM__Service_Request__c sr :[SELECT NSIBPM__Internal_Status_Name__c, NSIBPM__Customer__r.Vendor_ID__c 
                                                    FROM NSIBPM__Service_Request__c 
                                                WHERE NSIBPM__Record_Type_Name__c = 'Agent_Registration' 
                                                AND SR_Type__c = NULL
                                                AND NSIBPM__Customer__c = :accId Order By CreatedDate Desc LIMIT 1])
            {
                if (sr.NSIBPM__Internal_Status_Name__c == 'Draft') {
                    
                    returnId = sr.Id;
                }
                if (sr.NSIBPM__Internal_Status_Name__c == 'Approved') {
                    returnId = accId;
                }
                if (sr.NSIBPM__Internal_Status_Name__c == 'Closed') {
                    returnId = accId;
                }
                if (sr.NSIBPM__Internal_Status_Name__c == 'Rejected') {
                    returnId = accId;
                }
                if (sr.NSIBPM__Internal_Status_Name__c != 'Closed' 
                    && sr.NSIBPM__Internal_Status_Name__c != 'Draft' 
                    && sr.NSIBPM__Internal_Status_Name__c != 'Approved') {
                    
                    returnId = 'Agent registration is in progress.';
                }                
            } 
            
            if (returnId == accId ) {
                for (NSIBPM__Service_Request__c sr :[SELECT NSIBPM__Internal_Status_Name__c, NSIBPM__Customer__r.Vendor_ID__c 
                                                    FROM NSIBPM__Service_Request__c 
                                                WHERE NSIBPM__Record_Type_Name__c = 'Agent_Update' 
                                                AND SR_Type__c = NULL
                                                AND NSIBPM__Customer__c = :accId Order By CreatedDate DESC LIMIT 1])
                {
                    if (sr.NSIBPM__Internal_Status_Name__c == 'Draft') {
                        returnId = sr.Id;
                    }
                    else if (sr.NSIBPM__Internal_Status_Name__c == 'Submitted') {
                        returnId = 'Agent update is in progress.';
                    }
                    else if (sr.NSIBPM__Internal_Status_Name__c == 'Closed') {
                        returnId = accId;
                    }
                    else if (sr.NSIBPM__Internal_Status_Name__c == 'Rejected') {
                        returnId = accId;
                    }
                    else if (sr.NSIBPM__Internal_Status_Name__c == 'Approved') {
                        returnId = accId;
                    } else {
                        returnId = 'Agent updation is in progress.';
                    }
                    
                }
            } 
            
        } catch (Exception e) {
        
        }
        return returnId;
    }
}