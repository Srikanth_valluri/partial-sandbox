@isTest
private class FmcServiceChargeControllerTest {

    @isTest
    static void testController() {
        User portalUser = CommunityTestDataFactory.createPortalUser();
        Id portalAccountId = [SELECT Id, Contact.AccountId FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;

        System.debug('portalAccountId = ' + portalAccountId);

        Location__c location = new Location__c(
            Name = 'Test Location',
            Location_ID__c = 'LOC'
        );
        insert location;

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(portalAccountId, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            unit.Inventory__c = inventory.Id;
            unit.Property_Country__c = 'United Arab Emirates';
        }
        insert lstBookingUnit;

        insert TestDataFactory_CRM.createActiveFT_CS();

        CustomerCommunityUtils.customerAccountId = portalAccountId;

        PageReference currentPage = Page.CommunityPortal;
        currentPage.getParameters().put('view', 'ServiceCharge');
        Test.setCurrentPage(currentPage);

        Test.startTest();
            System.runAs(portalUser) {
                FmcServiceChargeController controller = new FmcServiceChargeController();
                //System.assert(!controller.lstUnit.isEmpty());
            }
        Test.stopTest();

    }

    @isTest
    static void testPaymentWithGateway() {
        User portalUser = CommunityTestDataFactory.createPortalUser();
        Id portalAccountId = [SELECT Id, Contact.AccountId FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;

        System.debug('portalAccountId = ' + portalAccountId);

        Location__c location = new Location__c(
            Name = 'Test Location',
            Location_ID__c = 'LOC'
        );
        insert location;

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(portalAccountId, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            unit.Inventory__c = inventory.Id;
        }
        insert lstBookingUnit;

        insert TestDataFactory_CRM.createActiveFT_CS();

        insert new PaymentGateway__c(
            Name = LoamsCommunityController.GATEWAY_NAME,
            Url__c = 'https://www.payment-gateway.com',
            MerchantId__c = 'merchant_id',
            AccessCode__c = 'access_code',
            EncryptionKey__c = 'encryption_key'
        );

        CustomerCommunityUtils.customerAccountId = portalAccountId;

        //Commmented on 29/08/19
        Test.startTest();
            System.runAs(portalUser) {
                String paymentPageUrl = FmcServiceChargeController.getPaymentUrl(
                    1,
                    new Map<String, List<FmIpmsRestServices.DueInvoice>> {
                        lstBookingUnit[0].Id
                            => new List<FmIpmsRestServices.DueInvoice> { new FmIpmsRestServices.DueInvoice() }
                    }
                );
                System.assert(String.isNotBlank(paymentPageUrl));
            }
        Test.stopTest();

    }

    @isTest
    static void testPaymentWithoutGateway() {
        User portalUser = CommunityTestDataFactory.createPortalUser();
        Id portalAccountId = [SELECT Id, Contact.AccountId FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;

        System.debug('portalAccountId = ' + portalAccountId);

        Location__c location = new Location__c(
            Name = 'Test Location',
            Location_ID__c = 'LOC'
        );
        insert location;

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(portalAccountId, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            unit.Inventory__c = inventory.Id;
        }
        insert lstBookingUnit;

        insert TestDataFactory_CRM.createActiveFT_CS();

        CustomerCommunityUtils.customerAccountId = portalAccountId;

        Test.startTest();
            System.runAs(portalUser) {
                String paymentPageUrl = FmcServiceChargeController.getPaymentUrl(
                    1,
                    new Map<String, List<FmIpmsRestServices.DueInvoice>> {
                        lstBookingUnit[0].Id
                            => new List<FmIpmsRestServices.DueInvoice> { new FmIpmsRestServices.DueInvoice() }
                    }
                );
                System.assert(String.isBlank(paymentPageUrl));
            }
        Test.stopTest();

    }

    @isTest
    static void testSoaWithoutMock() {
        Test.startTest();
            try {
                 System.assertEquals(NULL, FmcServiceChargeController.generateFmSoa('12345'));
            } catch(Exception e) {
                System.debug('e = ' + e);
                System.debug('e.getMessage() = ' + e.getMessage());
                System.assert(e instanceOf LoamsCommunityException);
            }
        Test.stopTest();
    }

    @isTest
    static void testSoaNullRegId() {
        Test.startTest();
            System.assertEquals(NULL, FmcServiceChargeController.generateFmSoa(NULL));
        Test.stopTest();
    }

    @isTest
    private static void testSoa() {
        String TEST_URL = 'https://test.damacgroup.com/test.pdf';
        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);
        
        //Commented on 29/08/19
        /*insert new IpmsRestServices__c(
                   SetupOwnerId = UserInfo.getOrganizationId(),
                   BaseUrl__c = 'http://83.111.194.181:8045/webservices/rest',
                   Username__c = 'oracle_user',
                   Password__c = 'crp1user',
                   Client_Id__c = '8MABLQM-KJ8I8-1XA58-WWCM1S1',
                   BearerToken__c = 'eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWyiwuVrJSKs5IBKK8bCUdpczEEiUrQ1MzAwtTMwsDIx2l1IoCqIC5oRFIoLQ4tSgvMTcVqM_C19HJJ9BX19vLwtNC1zDC0dRCNzzc2dcw2FCpFgBXRb-1XQAAAA.6Ym224Vwr9AniBeq6gL8OM9u4vGnUB_vbEUVjWojg14',
                   Timeout__c = 120000
                   );

        FmHttpCalloutMock.Response getAllUnitSoaInLangUrlResponse = new FmHttpCalloutMock.Response(200, 'Success', '   {  '  +
         '       "responseId": "3072019-10305298",  '  +
         '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         '       "status": "S",  '  +
         '       "responseMessage": "Process Completed",  '  +
         '       "elapsedTimeMs": 8497,  '  +
         '       "responseLines": [  '  +
         '           {  '  +
         '               "documentName": "DPSOA",  '  +
         '               "language": "EN",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
         '           },  '  +
         '           {  '  +
         '               "documentName": "DPSOA_AR",  '  +
         '               "language": "AR",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
         '           }  '  +
         '       ],  '  +
         '       "complete": true  '  +
         '  }  ');

         String UNIT_SOA_LANG_str = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.UNIT_SOA_LANG, new List<String> {'12345'});

         Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
             UNIT_SOA_LANG_str => getAllUnitSoaInLangUrlResponse
         };

         List<String> strlstForAssert = new List<String>{'https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd-English','https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a-Arabic'};

         Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));*/

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));

        Test.startTest();
            //System.assertEquals(strlstForAssert, FmcServiceChargeController.generateFmSoa('12345'));
            System.assertEquals(TEST_URL, FmcServiceChargeController.generateFmSoa('12345'));
        Test.stopTest();
    }

    @isTest
    static void testFmDuesNullPartyId() {
        Test.startTest();
            try {
                System.assertEquals(NULL, FmcServiceChargeController.fetchFmDues());
            } catch(Exception e) {
                System.debug('e = ' + e);
                System.debug('e.getMessage() = ' + e.getMessage());
                System.assert(e instanceOf LoamsCommunityException);
            }
        Test.stopTest();
    }

    @isTest
    static void testFmDuesWithoutMock() {
        User portalUser = CommunityTestDataFactory.createPortalUser();
        Id portalAccountId = [SELECT Id, Contact.AccountId FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;

        Test.startTest();
            System.runAs(portalUser) {
                System.assertEquals(NULL, FmcServiceChargeController.fetchFmDues());
            }
        Test.stopTest();
    }

    @isTest
    static void testFmDuesWithMock() {
        User portalUser = CommunityTestDataFactory.createPortalUser();
        System.debug('portalUser = ' + portalUser);
        Id portalAccountId = [SELECT Id, Contact.AccountId FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;
        System.debug('portalAccountId = ' + portalAccountId);

        insert new IpmsRestServices__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            BaseUrl__c = 'http://0.0.0.0:8080/webservices/rest',
            Username__c = 'username',
            Password__c = 'password',
            Timeout__c = 120000
        );

        Test.startTest();
            System.runAs(portalUser) {

                FmIpmsRestServices.DueInvoicesResponse invoicesResponse = new FmIpmsRestServices.DueInvoicesResponse();
                invoicesResponse.OutputParameters = new FmIpmsRestServices.OutputParameters();
                invoicesResponse.OutputParameters.X_RETURN_STATUS = 'S';
                invoicesResponse.OutputParameters.X_RETURN_MESSAGE = 'Total Records Fetched...1';
                invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE = new FmIpmsRestServices.ResponseMessage();
                invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM = new List<Map<String, String>>();
                invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM.add(
                    new Map<String, String> {
                        'ATTRIBUTE1' => 'registrationId',
                        'ATTRIBUTE2' => 'unitName',
                        'ATTRIBUTE3' => 'projectName',
                        'ATTRIBUTE4' => 'customerId',
                        'ATTRIBUTE5' => 'orgId',
                        'ATTRIBUTE6' => 'partyId',
                        'ATTRIBUTE7' => 'partyName',
                        'ATTRIBUTE8' => 'trxNumber',
                        'ATTRIBUTE9' => 'creationDate',
                        'ATTRIBUTE10' => 'callType',
                        'ATTRIBUTE11' => '50',
                        'ATTRIBUTE12' => '100',
                        'ATTRIBUTE13' => '50',
                        'ATTRIBUTE14' => FmcUtils.formatAsIpmsDate(Date.today()),
                        'ATTRIBUTE15' => 'trxType',
                        'ATTRIBUTE16' => '0'
                    }
                );
                Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(invoicesResponse)));

                System.debug('isPortalUser in test class = ' + CustomerCommunityUtils.isPortalUser());
                System.debug('Party id in test class = ' + CustomerCommunityUtils.getPartyId());
                FmcServiceChargeController.fetchFmDues();
            }
        Test.stopTest();
    }

}