@isTest 
public with sharing class GetFilesAccountcntrlTest {
   

    static testMethod void testMethod1() {
         Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
         Account testAccount = new Account( Name='Test Account' );
         insert testAccount;
        Case objCase = new Case(AccountId = testAccount.id, Credit_Note_Amount__c=5000, RecordTypeId = recTypeCOD);
        insert objCase ;
        Migrated_Documents_Siebel__c objDoc = new Migrated_Documents_Siebel__c(AccountName__c=testAccount.id,SR__c = objCase.Id,name= 'abc.txt',File_Name__c='abc.txt');
        List <Migrated_Documents_Siebel__c> lstDoc = new List<Migrated_Documents_Siebel__c>();
        insert lstDoc;
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
    //Set mock response
    Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        Test.startTest();
        Test.setCurrentPage(Page.GetFilesForAccount);
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(lstDoc);
        stdSetController.setSelected(lstDoc);
        GetFilesAccountcntrl ext = new GetFilesAccountcntrl(stdSetController);
        ext.fetchFileFromOffice365();
        ext.BackToAccount();
        Test.stopTest();
        }
    static testMethod void testMethod2() {
         Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
         Account testAccount = new Account( Name='Test Account' );
         insert testAccount;
        Case objCase = new Case(AccountId = testAccount.id, Credit_Note_Amount__c=5000, RecordTypeId = recTypeCOD);
        insert objCase ;
        Migrated_Documents_Siebel__c objDoc = new Migrated_Documents_Siebel__c(AccountName__c=testAccount.id,SR__c = objCase.Id,name= 'abc.txt',File_Name__c='abc.txt');
        List <Migrated_Documents_Siebel__c> lstDoc = new List<Migrated_Documents_Siebel__c>();
        lstDoc.add(objDoc);
        insert lstDoc;
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
    //Set mock response
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        Test.startTest();
        Test.setCurrentPage(Page.GetFilesForAccount);
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(lstDoc);
        stdSetController.setSelected(lstDoc);
        GetFilesAccountcntrl ext = new GetFilesAccountcntrl(stdSetController);
        ext.fetchFileFromOffice365();
        ext.BackToAccount();
        Test.stopTest();
        }
    


 
}