@isTest
public class CSRListViewCntrlTest {
    @IsTest
    static void testApproved(){
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='System Administrator' Limit 1];
        
        UserRole userRoleObj = new UserRole(Name = 'Director', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        User ObjUser = new User(Alias = 'standt1', Email='stan1@testorg.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', 
                                LanguageLocaleKey='en_US', isActive = true,
                                LocaleSidKey='en_US', ProfileId = objProfile.Id, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='sta4@damactest1234.com');
        insert ObjUser;
        
        System.runAs(ObjUser) {
            Account objAcc = TestDataFactory_CRM.createPersonAccount();
            objAcc.Email__c ='test@test.com';
            objAcc.Mobile_Phone_Encrypt__pc = '0097405883798';
            insert objAcc ;
            
            Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Case Summary - Client Relation').getRecordTypeId();
            System.debug('recTypeCOD :'+ recTypeCOD);
            
            // insert Case
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
            objCase.Approving_User_Id__c = Userinfo.getuserid();
            objCase.Status = 'Submitted';
            insert objCase;
            
            List<CaseSummaryRequest__c> caseSummaryList = new List<CaseSummaryRequest__c>();
            CaseSummaryRequest__c objCSR = new CaseSummaryRequest__c();
            system.debug('obj :'+objCSR);
            objCSR.CRE_Comments__c = 'Test';
            objCSR.CRE_Request_History__c = '% of Area Variation to be waived=>550,Amount=>,';
            objCSR.RequestType__c = 'Area Variation Waiver';
            objCSR.Per_Area_Variation_to_be_waived__c = '550'; 
            
            objCSR.case__c = objCase.Id;
            objCSR.Manager_Request_History__c ='% of Area Variation to be waived=>550';
            objCSR.Director_Request_History__c ='% of Area Variation to be waived=>550';
            objCSR.HOD_Request_History__c ='% of Area Variation to be waived=>550';
            objCSR.HOD_Id__c = Userinfo.getuserid();
            objCSR.Manager_Id__c = Userinfo.getuserid();
            objCSR.Committee_Id__c = Userinfo.getuserid();
            objCSR.Director_Id__c = Userinfo.getuserid();
            objCSR.Committee_Approval_Status__c = 'Approved';
            caseSummaryList.add(objCSR);
            

            CaseSummaryRequest__c objCSR1 = new CaseSummaryRequest__c();
            system.debug('obj :'+objCSR1);
            objCSR1.CRE_Comments__c = 'Test';
            objCSR1.CRE_Request_History__c = '% of Area Variation to be waived=>550,Amount=>,';
            objCSR1.RequestType__c = 'Area Variation Waiver';
            objCSR1.Per_Area_Variation_to_be_waived__c = '550';
            system.debug('obj :'+objCSR1);
            objCSR1.case__c = objCase.Id;
            objCSR1.Manager_Request_History__c ='% of Area Variation to be waived=>550';
            objCSR1.Director_Request_History__c ='% of Area Variation to be waived=>550';
            objCSR1.HOD_Request_History__c ='% of Area Variation to be waived=>550';
            objCSR1.HOD_Id__c = Userinfo.getuserid();
            objCSR1.Manager_Id__c = Userinfo.getuserid();
            objCSR1.Committee_Id__c = Userinfo.getuserid();
            objCSR1.Director_Id__c = Userinfo.getuserid();
            objCSR1.Committee_Approval_Status__c = 'Approved';
            caseSummaryList.add(objCSR1);     
            insert caseSummaryList;
                        
            Test.startTest();
            CSRListViewCntrl obj = new CSRListViewCntrl();
            obj.userid = 'test';
            obj.caseList = new List<Case>();
            CSRListViewCntrl.getPendingSRCount();
            CSRListViewCntrl.getPendingSRList();
            CSRListViewCntrl.init();
            Test.stopTest();
        }
    }
}