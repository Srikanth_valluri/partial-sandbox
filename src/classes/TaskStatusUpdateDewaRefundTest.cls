@isTest
public class TaskStatusUpdateDewaRefundTest {
    @isTest
    static void testTaskStatusUpdate(){
        List<Case> lstCase = new List<Case>();
        Case objCase = new Case();
        objCase.Bank_Name__c = 'name';
        insert objCase;
        lstCase.add(objCase);
        Case testCase = [Select Id,CaseNumber From Case Where Id =: objCase.Id LIMIT 1];
        
        Task objTask = new Task();
        objTask.WhatId = lstCase[0].Id;
        objTask.Subject = 'test';
        insert objTask;
        
        Test.startTest();
        TaskStatusUpdateDewaRefund.TaskStatusUpdateDewaRefund(testCase.CaseNumber, 'test', 'Success', 'Testing');
        Test.stopTest();
            
            }
    
    @isTest
    static void testTaskStatusUpdateNull(){
        
        List<Case> lstCase = new List<Case>();
        Case objCase = new Case();
        objCase.Bank_Name__c = 'name';
        insert objCase;
        lstCase.add(objCase);
        Case testCase = [Select Id,CaseNumber From Case Where Id =: objCase.Id LIMIT 1];
        
        Task objTask = new Task();
        objTask.WhatId = lstCase[0].Id;
        objTask.Subject = 'test';
        insert objTask;
        
        Test.startTest();
        TaskStatusUpdateDewaRefund.TaskStatusUpdateDewaRefund(null, null, 'Success', 'Testing');
        Test.stopTest();
    }
}