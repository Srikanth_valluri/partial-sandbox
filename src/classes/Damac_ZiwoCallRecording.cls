public with sharing class Damac_ZiwoCallRecording {

    public Damac_ZiwoCallRecording (ApexPages.standardController stdController) {
    
    }

    public String url { get; set; }
    
    public void init () {
        url = '';
        Waybeo_Logs__c log = new Waybeo_Logs__c ();
        ID logId = apexPages.currentPage().getParameters().get ('id');
        
        log = [SELECT Ziwo_callID__c, Ziwo_recordingFile__c FROM Waybeo_Logs__c WHERE Id =: logId];
        
        if (log.Ziwo_callID__c != NULL) {
            url = getURL (log.Ziwo_callID__c);
        }
    }
    
    public void initCallLog () {
        url = '';
        Call_Log__c log = new Call_Log__c ();
        ID logId = apexPages.currentPage().getParameters().get ('id');
        
        log = [SELECT call_ID__c FROM Call_Log__c WHERE Id =: logId];
        
        if (log.call_ID__c != NULL) {
            url = getURL (log.call_ID__c);
        }
    }
    
    public void initTask () {
        url = '';
        Task t = new Task ();
        ID taskId = apexPages.currentPage().getParameters().get ('id');
        t = [SELECT CrtObjectId__c, Recorded_Call__c FROM Task WHERE Recorded_Call__c != NULL AND id =: taskId];
        
        if (t.Recorded_Call__c != NULL) {
            url = getUrl (t.CrtObjectId__c);
        }
    }
    
    public String getURL (String ziwoCallId) {
        Ziwo_settings__c settings = Ziwo_settings__c.getInstance (UserInfo.getUserId());
            
        String accessToken = settings.Access_Token__c;
        Boolean updateToken = false;
        if (settings.Last_logged_In_Time__c != NULL) {
            DateTime lastLoggedInTime = settings.Last_logged_In_Time__c ;
            Long dt1Long = lastLoggedInTime.getTime();
            Long dt2Long = DateTime.now().getTime();
            Long milliseconds = dt2Long - dt1Long;
            Long seconds = milliseconds / 1000;
            Long minutes = seconds / 60;
            Long hours = minutes / 60;
            system.debug (hours+'==='+minutes+'=='+seconds+'==='+milliseconds);
            if (hours >= 24 || accessToken == NULL) {
                accessToken = Damac_ZiwoCallHistory.login();
                updateToken = true;
            }
        } else {
            accessToken = Damac_ZiwoCallHistory.login();
            updateToken = true;
        }
        
        
        
        String url = settings.Endpoint__c+'callHistory/'+ziwoCallId+'/recording?access_token='+accessToken;
        
        if (updateToken) {
            settings = [SELECT Endpoint__c,Access_token__c, Last_logged_in_Time__c FROM Ziwo_settings__c];
            settings.Access_Token__c = accessToken;
            settings.Last_logged_In_Time__c = DateTime.Now ();
            update settings;
        }
        return url;
    }
}