@isTest
public class FullyPaidTest {
    @isTest
    public static void testActiveUnit() {
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                    OnOffCheck__c = false);
        insert newSetting1;
        
        Account objAcc = TestDataFactory_CRM.createBusinessAccount();
        objAcc.Mobile__c ='1234567890';
        insert objAcc;
                
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;

        Booking_Unit_Active_Status__c buStatusObj = new Booking_Unit_Active_Status__c();
        buStatusObj.Name = 'Agreement Rejected By Sales Admin';
        buStatusObj.Status_Value__c  = 'Agreement Rejected By Sales Admin';
        insert buStatusObj;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        //objBookingUnit.Mortgage__c = true;
        objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status_Code__c = 'sdasd';
        objBookingUnit.Registration_DateTime__c = System.now().addDays( 185 );
        insert objBookingUnit;

        Id collectioncallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Collections Calling List').RecordTypeId;
        String this_Month = String.valueOf( System.Today().month() );
        String this_Year = String.valueOf( System.Today().year() );

        Calling_List__c callList = new Calling_List__c(
                                                      Registration_Id__c = '12345'
                                                      , Account__c = objAcc.id
                                                      , RecordTypeId = collectioncallingRecordTypeId
                                                      , IsHideFromUI__c = false
                                                      , Call_OutCome__c = 'Unreachable'
                                                      , Collection_Month__c = this_Month
                                                      , Collection_Year__c = this_Year
                                                      , Calling_List_Type__c = 'Collection Calling');
        insert callList;

        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/services/apexrest/FullyPaid';
        request.httpMethod = 'POST';
        RestContext.request = request;
        RestContext.response = response;
        Test.startTest();
        FullyPaid.FullyPaid( '12345' , String.valueOf(System.Today()), 'Remark');
        Test.stopTest();
    }
    @isTest
    public static void testEmptyUnit() {
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                    OnOffCheck__c = false);
        insert newSetting1;
        

        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/services/apexrest/FullyPaid';
        request.httpMethod = 'POST';
        RestContext.request = request;
        RestContext.response = response;
        Test.startTest();
        FullyPaid.FullyPaid( '12345' , String.valueOf(System.Today()), 'Remark');
        Test.stopTest();
    }
}