public class ExplicitelyCreatePortalUser {

    public String accountId  {get; set;}
    public String contactId  {get; set;}
    public String username  {get; set;}
    public String email  {get; set;}
    public String firstname  {get; set;}
    public String lastname   {get; set;}
    public String mobilePhone  {get; set;}
    public String partyId  {get; set;}
    public String userId {get;set;}
    public String password {get; set;}

    public ExplicitelyCreatePortalUser() {
        System.debug('Inside constructor');
        
    }
    
    public void createUser() {
        String accId = accountId;
        list<Profile> lstProfile;
        CustomerCommunitySettings__c customerCommunitySettings = CustomerCommunitySettings__c.getInstance();
        if(customerCommunitySettings.ProfileName__c != null){
             lstProfile =[SELECT Id,
                                 Name
                            FROM Profile
                           WHERE Name =: customerCommunitySettings.ProfileName__c];
        }
        User u = new User();
        u.Username = username;
        u.Email = email;
        u.FirstName = firstname;
        u.LastName = lastname;
        u.ContactId = contactId;
        u.CommunityNickname = partyId+'name';
         if(lstProfile!= null && !lstProfile.isEmpty()){
            u.ProfileId = lstProfile[0].id;
        }

        u.Phone = mobilePhone;
        u.party_id__c = partyId;
        u.MobilePhone = mobilePhone;
        u.LanguageLocaleKey='en_US';
        u.LocaleSidKey='en_GB';
        u.TimeZoneSidKey='Asia/Dubai';
        u.Alias = '1786016';
        u.EmailEncodingKey = 'UTF-8';
        
        System.debug('u:::' + u );
        System.debug('password: '+password);
        try {
        userId = Site.createPortalUser(u, accId, password, true);
        System.debug('userId: ' + userId);
        }
        catch(Site.ExternalUserCreateException ex) {
           System.debug('ex::'+ex.getMessage());
        }
    }

}