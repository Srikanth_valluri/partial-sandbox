/*
 * Description : This class is used to Schedule UpdateCustomerFlagonCallingListBatch batch at every month
 * Revision History:
 *
 *  Version          Author              Date           Description
 *  1.0                                 5/12/2017         Initial Draft
 *                                                      
 */
global class ScheduleUpdateCustomerFlagonCallingList implements Schedulable {
  global void execute(SchedulableContext sc) {
  UpdateCustomerFlagonCallingListBatch batchInstance = new UpdateCustomerFlagonCallingListBatch(); 
  database.executebatch(batchInstance);
  }
}