public class CaseSummaryRequestTriggerHandler   implements Queueable{
        
        public List<CaseSummaryRequest__c> lstNewCaseSummaryRequest ; 
        public CaseSummaryRequestTriggerHandler(List<CaseSummaryRequest__c> lstNewCaseSummaryRequest){
            this.lstNewCaseSummaryRequest = lstNewCaseSummaryRequest ;  
        }
        
        public void execute(QueueableContext context) { 
        Set<Id> setOfCaseId = new Set<Id>();
        Set<String> setOfRequestType = new Set<String>();
        Map<String,List<Restructure_CSR__mdt>> objMapOfMetadataWithChildStatus = new Map<String,List<Restructure_CSR__mdt>>(); 
        Map<String,List<Restructure_CSR__mdt>> mapOfMetadataWithRequestType = new Map<String,List<Restructure_CSR__mdt>>();
        Map<id,List<SR_Attachments__c>> mapofCaseAndAttachments = new map<id,List<SR_Attachments__c>>(); 
        List<SR_Attachments__c> lstAttachmentsToUpdate = new List<SR_Attachments__c>();
        List<SR_Attachments__c> lstAttachmentsToInsert = new List<SR_Attachments__c>();
        Case objCase =  new Case();
        
        List<Restructure_CSR__mdt> lstOfMetadata = [SELECT Child_Case_FieldName__c, ChildCaseRecordType__c, ChildCaseStatus__c ,NewCaseFieldToMap__c,ParentCaseFieldName__c,Request_Type__c,Status__C FROM Restructure_CSR__mdt];

        for(Restructure_CSR__mdt objRestructureMetadata : lstOfMetadata){
            if(!objMapOfMetadataWithChildStatus.containsKey(objRestructureMetadata.ChildCaseStatus__c)){
                objMapOfMetadataWithChildStatus.put(objRestructureMetadata.ChildCaseStatus__c, new  List <Restructure_CSR__mdt> {objRestructureMetadata});                
            }else{
                objMapOfMetadataWithChildStatus.get(objRestructureMetadata.ChildCaseStatus__c).add(objRestructureMetadata);
            }
            
        }
        
        system.debug('objMapOfMetadataWithChildStatus==='+objMapOfMetadataWithChildStatus);
        
        for(CaseSummaryRequest__c objCaseSummaryRequest : lstNewCaseSummaryRequest) {
            for(Case objCaseRec : [Select id, ParentId, AccountId, Status,(Select id,Name,isValid__c,Attachment_URL__c,CurrencyIsoCode from SR_Attachments__r Limit 1) from case where id = :objCaseSummaryRequest.Case__c]){
            setOfRequestType.add(objCaseSummaryRequest.RequestType__c);
            if(objCaseSummaryRequest.Status__c !='Rejected' && (objCaseRec.Status == 'Partially Approved' || objCaseRec.Status == 'Approved')){
                if(!String.isBlank(objCaseSummaryRequest.AuthoritiesFromSFRuleEngine__c)){
                    List<String> lstAuthorities = objCaseSummaryRequest.AuthoritiesFromSFRuleEngine__c.split(',');    
                    String strFinalAuth;       
                    if (lstAuthorities != null && lstAuthorities.size() > 0) {
                        Integer no = lstAuthorities.size() - 1;
                        strFinalAuth = lstAuthorities[no];   
                    }
                    if( objCaseRec.SR_Attachments__r!=null){
                     mapofCaseAndAttachments.put(objCaseRec.Id, objCaseRec.SR_Attachments__r);
                	}  
                    if(strFinalAuth == 'Manager' && objCaseSummaryRequest.Manager_Approval_Status__C !=null){
                        mapOfMetadataWithRequestType.put(objCaseSummaryRequest.RequestType__c,objMapOfMetadataWithChildStatus.get(objCaseSummaryRequest.Manager_Approval_Status__c));
                    }
                    else if(strFinalAuth == 'Director'){
                        if(objCaseSummaryRequest.Manager_Approval_Status__c == 'Approved with changes'){
                            mapOfMetadataWithRequestType.put(objCaseSummaryRequest.RequestType__c,objMapOfMetadataWithChildStatus.get(objCaseSummaryRequest.Manager_Approval_Status__c));
                        } else {
                            mapOfMetadataWithRequestType.put(objCaseSummaryRequest.RequestType__c,objMapOfMetadataWithChildStatus.get(objCaseSummaryRequest.Director_Approval_Status__c));
                        }
                    }
                    else if(strFinalAuth == 'HOD'){
                        if(objCaseSummaryRequest.Manager_Approval_Status__c == 'Approved with changes'){
                            mapOfMetadataWithRequestType.put(objCaseSummaryRequest.RequestType__c,objMapOfMetadataWithChildStatus.get(objCaseSummaryRequest.Manager_Approval_Status__c));
                        } else  if(objCaseSummaryRequest.Director_Approval_Status__c == 'Approved with changes'){
                            mapOfMetadataWithRequestType.put(objCaseSummaryRequest.RequestType__c,objMapOfMetadataWithChildStatus.get(objCaseSummaryRequest.Director_Approval_Status__c));
                        } else {
                            mapOfMetadataWithRequestType.put(objCaseSummaryRequest.RequestType__c,objMapOfMetadataWithChildStatus.get(objCaseSummaryRequest.HOD_Approval_Status__c));
                        }                    
                    }
                    else if(strFinalAuth == 'Committee'){
                        if(objCaseSummaryRequest.Manager_Approval_Status__c == 'Approved with changes'){
                            mapOfMetadataWithRequestType.put(objCaseSummaryRequest.RequestType__c,objMapOfMetadataWithChildStatus.get(objCaseSummaryRequest.Manager_Approval_Status__c));
                        } else  if(objCaseSummaryRequest.Director_Approval_Status__c == 'Approved with changes'){
                            mapOfMetadataWithRequestType.put(objCaseSummaryRequest.RequestType__c,objMapOfMetadataWithChildStatus.get(objCaseSummaryRequest.Director_Approval_Status__c));
                        } else if(objCaseSummaryRequest.HOD_Approval_Status__c == 'Approved with changes'){
                            mapOfMetadataWithRequestType.put(objCaseSummaryRequest.RequestType__c,objMapOfMetadataWithChildStatus.get(objCaseSummaryRequest.HOD_Approval_Status__c));
                        } else{
                            mapOfMetadataWithRequestType.put(objCaseSummaryRequest.RequestType__c,objMapOfMetadataWithChildStatus.get(objCaseSummaryRequest.Committee_Approval_Status__c));
                        }                   
                    }
                }
                
                List<Restructure_CSR__mdt> lstRestructureCSR = mapOfMetadataWithRequestType.get(objCaseSummaryRequest.RequestType__c);
                for(Restructure_CSR__mdt objRestructure : lstRestructureCSR){
                    if(objRestructure.Request_Type__c == objCaseSummaryRequest.RequestType__c ){
                        if(objRestructure.Child_Case_FieldName__c	!=null){
                            
                            System.debug('test==='+objCaseSummaryRequest.get(objRestructure.Child_Case_FieldName__c	));
                            Schema.SObjectType t = Schema.getGlobalDescribe().get('Case');
                            Schema.DescribeSObjectResult r = t.getDescribe();
                            Schema.DescribeFieldResult f = r.fields.getMap().get(objRestructure.NewCaseFieldToMap__c).getDescribe();
                            system.debug(f.getType());
                            if (f.getType() == Schema.DisplayType.String){
                                objCase.put(objRestructure.NewCaseFieldToMap__c,String.valueof(objCaseSummaryRequest.get(objRestructure.Child_Case_FieldName__c	)));
                                
                            }else if(f.getType() == Schema.DisplayType.DOUBLE){
                                objCase.put(objRestructure.NewCaseFieldToMap__c,Decimal.valueof(String.valueof(objCaseSummaryRequest.get(objRestructure.Child_Case_FieldName__c	))));
                                
                            }else{
                                objCase.put(objRestructure.NewCaseFieldToMap__c,objCaseSummaryRequest.get(objRestructure.Child_Case_FieldName__c	));
                            }
                            
                        } else{
                            objCase.put(objRestructure.NewCaseFieldToMap__c,objCaseRec.get(objRestructure.ParentCaseFieldName__c));
                        }
                        objCase.put('Status',objRestructure.Status__c);
                        objCase.put('RecordTypeId', Schema.SObjectType.Case.getRecordTypeInfosByName().get(objRestructure.ChildCaseRecordType__c).getRecordTypeId());
                    }
                }
            }
        }
        system.debug('lstOfCaseToInsert==='+objCase);
            if(objCase!=null){
               insert objCase;
            }
            
            if(mapofCaseAndAttachments.containskey(objCase.ParentId) && (objCase.Waiver_Type__c =='Service Charge' || objCase.Waiver_Type__c =='Area Variation Waiver' )){
                lstAttachmentsToInsert = mapofCaseAndAttachments.get(objCase.parentId);
                lstAttachmentsToInsert = lstAttachmentsToInsert.deepClone(false,false,false);
                for(SR_Attachments__c objAttachments:lstAttachmentsToInsert){
                    objAttachments.case__c = objCase.id;
                    lstAttachmentsToUpdate.add(objAttachments);
                }
            }
            List<Case> lstCasesToUpdate = new List<Case>();
           String approver = '';
              String strApprovalAuth = SfRuleEngine.filterSfRuleEngine('Case Summary Child', objCase.Id);
                System.debug('strApprovalAuth>>>'+strApprovalAuth);
                if (strApprovalAuth != null && !String.isBlank(strApprovalAuth) ){
                        approver = strApprovalAuth;
                        system.debug('approver'+approver);
             
                }
                 if (!string.isBlank(approver)) {
                    objCase.Roles_from_Rule_Engine__c = approver;
                    objCase.Approving_Authorities__c = approver;
                    objCase.Submit_for_Approval__c = true;
                 }
                 lstCasesToUpdate.add(objCase);
            if(lstCasesToUpdate != null && lstCasesToUpdate.size() > 0) {
                update lstCasesToUpdate;
            }
            if(lstAttachmentsToUpdate!=null && lstAttachmentsToUpdate.size()>0){
                System.debug('lstAttachmentsToUpdate==='+lstAttachmentsToUpdate);
              insert lstAttachmentsToUpdate;
            }
        }
    }
}