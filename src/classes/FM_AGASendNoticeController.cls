/*-------------------------------------------------------------------------------------------------
Description: Controller class to redirect to upload page class for uploading notices/MOM
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 13-01-2019       | Lochana Rajput   | 1. Added necessary parameters to send MOM/Notice
=============================================================================================================================
*/
public with sharing class FM_AGASendNoticeController {
	FM_Case__c mysObject;
    public FM_AGASendNoticeController(ApexPages.StandardController stdController) {
        this.mysObject = (FM_Case__c)stdController.getRecord();
    }

    public PageReference redirect() {
		Map<String, String> mapMeetingType_Notice = new Map<String, String>();
		mapMeetingType_Notice.put('AGA meeting','AGA Notice');
		mapMeetingType_Notice.put('Adjourned AGA meeting','AGA Notice');
		mapMeetingType_Notice.put('Resident Gathering','Resident Gathering Notice');
		mapMeetingType_Notice.put('Board Meeting','Board Meeting Notice');
		System.debug('==************='+(String)mysObject.get('Id'));
		Id fmcaseId = (String)mysObject.get('Id');
		FM_Case__c objFMCase = [SELECT Id,Meeting_Type__c,Property__c FROM FM_Case__c where ID=:fmcaseId];
		PageReference pageRef = new PageReference('/apex/FMUploadDocuments');
		pageRef.getParameters().put('propertyId',objFMCase.Property__c);
		pageRef.getParameters().put('retURL','/'+objFMCase.Id);
        pageRef.getParameters().put('docType','Notice');
		pageRef.getParameters().put('docLevel','building');
		pageRef.getParameters().put('process','AGA');
		pageRef.getParameters().put('fmCaseId',objFMCase.Id);
        if(objFMCase.Meeting_Type__c == 'Adjourned AGA meeting' || objFMCase.Meeting_Type__c == 'AGA meeting') {
            pageRef.getParameters().put('recipient','Owners');
        }
        else if(objFMCase.Meeting_Type__c == 'Board Meeting') {
            pageRef.getParameters().put('recipient','Board Members');
        }
        else if(objFMCase.Meeting_Type__c == 'Board Meeting') {
            pageRef.getParameters().put('recipient','Owners and Tenants');
        }
		return pageRef;
    }

	public PageReference redirectToSendMOM() {
		Map<String, String> mapMeetingType_Notice = new Map<String, String>();
		mapMeetingType_Notice.put('AGA meeting','MOM for AGA Notice');
		mapMeetingType_Notice.put('Adjourned AGA meeting','MOM for AGA Notice');
		mapMeetingType_Notice.put('Resident Gathering','MOM for Board Meeting Notice');
		System.debug('==************='+(String)mysObject.get('Id'));
		Id fmcaseId = (String)mysObject.get('Id');
		FM_Case__c objFMCase = [SELECT Id,Meeting_Type__c,Property__c FROM FM_Case__c where ID=:fmcaseId];
		PageReference pageRef = new PageReference('/apex/FMUploadDocuments');
		pageRef.getParameters().put('propertyId',objFMCase.Property__c);
		pageRef.getParameters().put('retURL','/'+objFMCase.Id);
		pageRef.getParameters().put('docType','MOM');
		pageRef.getParameters().put('docLevel','building');
		pageRef.getParameters().put('process','AGA');
		pageRef.getParameters().put('fmCaseId',objFMCase.Id);
        if(objFMCase.Meeting_Type__c == 'Adjourned AGA meeting' || objFMCase.Meeting_Type__c == 'AGA meeting') {
            pageRef.getParameters().put('recipient','Owners');
        }
        else if(objFMCase.Meeting_Type__c == 'Board Meeting') {
            pageRef.getParameters().put('recipient','Board Members');
        }

		// if(mapMeetingType_Notice.containsKey(objFMCase.Meeting_Type__c)) {
		// 	pageRef.getParameters().put('documentType',objFMCase.Property__c);
		// }
		return pageRef;
    }
}