@isTest
private class ContentVersionTriggerHandlerTest {

    @isTest static void test_method_one() {
        ContentVersion cv=new Contentversion();
        cv.title='ABC';
        cv.PathOnClient ='test';
        Blob b=Blob.valueOf('Unit Test Attachment Body');
        cv.versiondata=EncodingUtil.base64Decode('Unit Test Attachment Body');
        insert cv;
        
        update cv;
        
        ContentVersionTriggerHandler obj = new ContentVersionTriggerHandler ();
        obj.executeBeforeDeleteTrigger (null);
        obj.executeBeforeInsertUpdateTrigger (null, null);
        obj.executeAfterDeleteTrigger (null);
        obj.executeAfterInsertUpdateTrigger (null,null);
        
    }
}