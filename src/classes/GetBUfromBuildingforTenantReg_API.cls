/**********************************************************************************************************************
Description: This service is used for getting all the units for given buildingId in request params
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   29-09-2020      | Shubham Suryawanshi | Added - Unit_Active__c = 'Active' filter in Booking Unit query
***********************************************************************************************************************/

@RestResource(urlMapping='/getUnitsFromBuilding/*')
global class GetBUfromBuildingforTenantReg_API {

    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        //200 => 'OK',
        //400 => 'Bad Request',
        //401 => 'Unauthorized',
        //500 => 'Internal Server Error' 
        1 => 'Success',
        //2 => 'Failure',
        2 => 'Sorry!', /*As per azure bug : 2031*/
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };

     @HttpPost 
     global static FinalReturnWrapper getBookingUnits() {

        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);
        System.debug('Request params  buildingId :' + r.params.get('buildingId'));
        //FinalReturnWrapper objFinalWrap = new FinalReturnWrapper();
        cls_meta_data objMeta = new cls_meta_data();
        List<BUWrapper> lstBUWrapper = new List<BUWrapper>();
        List<Booking_Unit__c> lstBUs = new List<Booking_Unit__c>();
        
        Set<String> setActiveStatuses = Booking_Unit_Active_Status__c.getAll().keySet();
        boolean isTest = test.isRunningTest();
        if(r.params.containsKey('buildingId') && String.isNotBlank(r.params.get('buildingId')) ) {
           
            try {
                lstBUs = [SELECT  Id
                                , Unit_Name__c
                                , Registration_Status__c
                                , Resident_Type__c
                                , Registration_ID__c
                                , (SELECT id, Status__c 
                                   FROM FM_Cases__r 
                                   WHERE Origin__c = 'Portal - Guest'       //Will need to update as per APP
                                   AND Request_Type__c = 'Tenant Registration'
                                   AND Status__c IN ('Draft Request' , 'In Progress' , 'New' , 'Awaiting Correction')
                                   ORDER BY CreatedDate DESC
                                   LIMIT 1 )
                         FROM Booking_Unit__c
                         WHERE Inventory__r.Building_Location__c = :r.params.get('buildingId')
                             AND ((Unit_Active__c = 'Active'
                                    AND (Property_Status__c = 'Ready' OR Inventory__r.Property_Status__c = 'Ready'))
                              OR Dummy_Booking_Unit__c = TRUE)];

                System.debug('lstBUs: ' + lstBUs);
            }
            catch(System.QueryException e){
                System.debug('exception :' + e.getMessage());
                //objMeta.is_success = false;
                objMeta.message = e.getMessage();
                objMeta.status_code = 6;

            }
        
            if(lstBUs.size() > 0) {
                for(Booking_Unit__c objBU : lstBUs) {
                    
                    //String jsonStr = JSON.serialize(objBU);
                    //jsonStr = jsonStr.replaceAll('__c','');
                    //System.debug('jsonStr: ' + jsonStr);
                    
                    //BUWrapper objBuildWrap = (BUWrapper)JSON.deserialize(jsonStr, BUWrapper.class);
                    BUWrapper objBuildWrap = new BUWrapper();
                    objBuildWrap.id = objBu.id;
                    objBuildWrap.unit_name = objBu.Unit_Name__c;
                    objBuildWrap.resident_type = objBu.Resident_Type__c;
                    objBuildWrap.is_case_in_progress = objBu.FM_Cases__r.size() > 0 ? true : false;
                    objBuildWrap.reg_id = objBu.Registration_ID__c;


                    lstBUWrapper.add(objBuildWrap);
                }

                //objMeta.is_success = true;
                objMeta.message = 'Successful';
                objMeta.status_code = 1;
            }
            else {
                //objMeta.is_success = true;
                //objMeta.message = 'No Booking units found for given buildingId';
                //objMeta.message = 'Something went wrong';/*As per azure Bug : 1774*/

                objMeta.message = 'No units available for the selected building'; /*As per azure bug : 2031*/
                objMeta.status_code = 2;
            }
            
        }
        else {
            if( !r.params.containsKey('buildingId') ) {
                //objMeta.is_success = false;
                objMeta.message = 'Missing parameter: buildingId';
                objMeta.status_code = 3;
            }
            else if( String.isBlank(r.params.get('buildingId')) ) {
                //objMeta.is_success = false;
                objMeta.message = 'Missing parameter value : buildingId';
                objMeta.status_code = 3;
            }
        }

        objMeta.title = mapStatusCode.get(objMeta.status_code);

        cls_data objData = new cls_data();
        objData.booking_units = lstBUWrapper;

        FinalReturnWrapper objReturn = new FinalReturnWrapper();
        objReturn.data = objData;
        objReturn.meta_data = objMeta;
        
        System.debug('objReturn: ' + objReturn);
        
        return objReturn;

    }
    
     //global class FinalReturnWrapper {
     //   public String status;
     //   public String message;
     //   public BUWrapper[] BookingUnits;
     //}
     
     public class BUWrapper {
        public String id;
        public String unit_name;
        public String resident_type;
        public Boolean is_case_in_progress; 
        public String reg_id;
        //public String Registration_Status;
     }

     global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_data {
        public BUWrapper[] booking_units;
    }

    public class cls_meta_data {
        //public Boolean is_success {get; set;}
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message;
    }

}