@isTest(seealldata = true)
public class PromoterTargetsController_Test {
    
    static testmethod void m1(){

        PromoterTargetsController obj = new PromoterTargetsController();
        obj.filterData();
        obj.filterTarget.Month__c = 'November';
        obj.filterTarget.Year__c = string.valueof(System.today().Year());
        obj.filterData();

        list<User> lstusers = [Select id, name, HR_Employee_ID__c 
                        from User 
                        where isActive = true and Profile.Name = 'Promoter Community Profile'
                        order by name asc];
        list<Campaign__c> campaigns = [Select id, name, Campaign_Name__c 
                        from Campaign__c 
                        where End_Date__c >= Today and Sys_Active__c = true 
                        and Campaign_Type_New__c = 'Stands'
                        order by name asc];

        obj.saveTargets();
        
        obj.lstTargets[0].searchText = lstusers[0].Name;
        obj.searchText = lstusers[0].Name +' - '+(lstusers[0].HR_Employee_ID__c == null ? 'NA': lstusers[0].HR_Employee_ID__c);
        obj.rowIndex = '0';
        obj.userSelected();
        obj.lstTargets[0].tar.user__c = lstusers[0].Id;
        obj.removeRow();
        obj.searchText = lstusers[0].Name +' - '+(lstusers[0].HR_Employee_ID__c == null ? 'NA': lstusers[0].HR_Employee_ID__c);
        obj.rowIndex = '0';
        obj.userSelected();
        obj.lstTargets[0].tar.Campaign__c = campaigns[0].Id;
        obj.lstTargets[0].tar.shift__c = 'Evening';
        obj.lstTargets[0].tar.Target_Tours__c = 250;
        obj.lstTargets[0].tar.Target_Leads__c = 350;
        obj.saveTargets();

        obj.filterTarget.Month__c = 'November';
        obj.filterTarget.Year__c = string.valueof(System.today().Year());
        obj.filterData();
        obj.selIndex = 0;
        obj.removeRow();
        
        obj.readCSVFile();
        PromotersController obj1 = new PromotersController();
        obj.fileBody = blob.valueOf(obj1.csvString);
        obj.readCSVFile();

    }
}