/**********************************************************************************************************************
* Name               : CampaignTriggerHandler                                                                         *
* Description        : This is a trigger handler for campaign.                                                        *
*                      - Updates the Heard of Damac                                                                   *
* Created Date       : 20/06/2017                                                                                     *
* Created By         : PWC                                                                                            *
* --------------------------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                                  *
* 1.0         Vineet     20/06/2017      Initial Draft.                                                               *
**********************************************************************************************************************/
public class CampaignTriggerHandler implements TriggerFactoryInterface{
  
  /*********************************************************************************************
    * @Description : Method to contain logic to be executed before insert.                       *
    * @Params      : List<sObject>                                                               *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeBeforeInsertTrigger(List<sObject> newRecordsList){ 
        try{
            /* Calling method to get the Heard of Damac code. */
            Campaignhandler.getCode((List<Campaign__c>) newRecordsList);
        }catch(Exception ex){
            system.debug('#### Exception at line number = '+ex.getLineNumber()+' , Exception Message = '+ex.getMessage());  
        }
    }
    
    /*********************************************************************************************
    * @Description : Method to contain logic to be executed after update.                        *
    * @Params      : Map<Id, sObject>, Map<Id, sObject>                                          *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeAfterUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){ 
        UpdateIsAssignedInventory(mapNewRecords,mapOldRecords);
        MarkAllChildsWithParentPriority(mapNewRecords.values());
    }
    
    /*********************************************************************************************
    * @Description : Method to contain logic to be executed before update.                       *
    * @Params      : Map<Id, sObject>, Map<Id, sObject>                                          *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeBeforeUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){
        List<Campaign__c> updatedParentCampaignList = new List<Campaign__c>();
        try{
            for(Id thisCampaignId : mapNewRecords.keySet()){
                Campaign__c newRecord = (Campaign__c)mapNewRecords.get(thisCampaignId);
                Campaign__c oldRecord = (Campaign__c)mapOldRecords.get(thisCampaignId);
                if(newRecord.Marketing_End_Date__c != null && 
                   newRecord.Marketing_End_Date__c != oldRecord.Marketing_End_Date__c && 
                   newRecord.Marketing_End_Date__c < oldRecord.Marketing_End_Date__c){
                    newRecord.MED_Changed_Date__c = Date.today(); 
                }
                /* Checking if the parent id has been changed or not. */
                if((newRecord.Parent_Campaign__c != null && newRecord.Parent_Campaign__c != oldRecord.Parent_Campaign__c) || 
                   (newRecord.End_Date__c != null && newRecord.End_Date__c != oldRecord.End_Date__c)){
                    updatedParentCampaignList.add(newRecord);  
                }
            }  
            if(!updatedParentCampaignList.isEmpty()){
                /* Calling method to get the Heard of Damac code. */
                Campaignhandler.getCode(updatedParentCampaignList);  
            }
            
        }catch(Exception ex){
            system.debug('#### Exception at line number = '+ex.getLineNumber()+' , Exception Message = '+ex.getMessage());  
        }
    }
  
    /****************************************************************************************************************
    * @Description : Method to contain logic to IsAssigned = false in Inventory on campaign marketing date change.  *
    * @Params      : Map<Id, sObject>, Map<Id, sObject>                                                             *
    * @Return      : void                                                                                           *
    ****************************************************************************************************************/
        @TestVisible
    private void UpdateIsAssignedInventory(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){
        List<Inventory__c> ToBeUpdateInv = new List<Inventory__c>(); 
        Map<Id, List<Campaign_Inventory__c>> cmpInvMap = new Map<Id, List<Campaign_Inventory__c>>();
        for(Campaign_Inventory__c cmpInv:[SELECT Id, Name, Inventory__c, Inventory__r.Is_Assigned__c, Campaign__c 
                                          FROM Campaign_Inventory__c 
                                          WHERE Inventory__r.Is_Assigned__c = true AND 
                                                campaign__c =: mapNewRecords.keySet()]){
            if(cmpInvMap.containsKey(cmpInv.campaign__c)){
                cmpInvMap.get(cmpInv.campaign__c).add(cmpInv);
            }else{
                cmpInvMap.put(cmpInv.campaign__c,new List<Campaign_Inventory__c>{cmpInv});
            }
        }
        system.debug('cmpInvMap=='+cmpInvMap);
        for(campaign__c cmp : (List<campaign__c>)mapNewRecords.values()){
            if(cmp.Marketing_End_Date__c != null && cmp.Marketing_End_Date__c <= system.today() && 
               cmp.Marketing_End_Date__c != mapOldRecords.get(cmp.Id).get(Schema.campaign__c.Marketing_End_Date__c)){
                if(cmpInvMap.containsKey(cmp.Id)){
                    for(Campaign_Inventory__c cmpInv:cmpInvMap.get(cmp.Id)){
                        ToBeUpdateInv.add(new Inventory__c(id=cmpInv.Inventory__c,Is_Assigned__c=false));   
                    }
                }
            }
        }
        
        if(!ToBeUpdateInv.isEmpty()){
            DataBase.saveResult[] result=  database.update(ToBeUpdateInv,false);
            for (Database.SaveResult sr : result) {
                if (sr.isSuccess()) {
                    System.debug('#### Successfully update record. ' + sr.getId());
                }else {
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('#### The following error has occurred.');                   
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('#### Fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }
        system.debug('ToBeUpdateInv=='+ToBeUpdateInv);
    }
    
    public void MarkAllChildsWithParentPriority(List<sObject> newRecordsList){
        TriggerFactoryCls.BYPASS_UPDATE_TRIGGER = true;
        SET<ID> campaignIds = new Set <ID> ();
        for (Sobject rec :newRecordsList) {
            campaignIds.add (rec.ID);
        }
        updateLeadPrioritization (campaignIds);
        /*List<Campaign__c> camps = newRecordsList;
        System.Debug (camps);
        list<campaign__c> childCamps = new list<campaign__c>();
        childCamps = [select id,Lead_Prioritization__c,Parent_Campaign__c,Parent_Campaign__r.Lead_Prioritization__c from campaign__c where Parent_campaign__c IN:camps];
        system.debug(childCamps.size() +'childCamps');
        if(childCamps.size() >0 ){
            map<id,string> MapChildstoUpdate = new map<id,string>();            
            for(campaign__c c:childCamps){
                MapChildstoUpdate.put(c.parent_campaign__c, c.Parent_Campaign__r.Lead_Prioritization__c);
            }
            system.debug('MapChildstoUpdate===='+MapChildstoUpdate);
            list<campaign__c> toUpdateChilds = new list<campaign__c>();
            for(campaign__c c:childCamps){
                if(MapChildstoUpdate.containskey(c.parent_campaign__c)){ 
                    if(c.Lead_Prioritization__c == null){                   
                        c.Lead_Prioritization__c = MapChildstoUpdate.get(c.parent_campaign__c);
                    }
                    toUpdateChilds.add(c);
                }
            }
            update toUpdateChilds;
            
        }
        */
    }
    
    public void updateLeadPrioritization (Set <ID> parentCampaingIds) {
        Map <ID, Campaign__c> childCampsMap = new Map <ID, Campaign__c>( [SELECT Lead_Prioritization__c,Parent_Campaign__c,
                                                Parent_Campaign__r.Lead_Prioritization__c 
                                                FROM campaign__c 
                                                WHERE Parent_campaign__c IN :parentCampaingIds]);
        
        List <Campaign__c> toUpdateChilds = new List <Campaign__c> ();
        Set <ID> childCampaignIds = new Set <ID> ();
        if (childCampsMap.keySet ().size () > 0) {
            Map <ID, String> MapChildstoUpdate = new Map <ID, String> ();            
            for (campaign__c c :childCampsMap.values ()){
                MapChildstoUpdate.put (c.parent_campaign__c, c.Parent_Campaign__r.Lead_Prioritization__c);
            }
            for (Campaign__c c :childCampsMap.values ()) {
                if (MapChildstoUpdate.containskey(c.parent_campaign__c)){ 
                    childCampaignIds.add (c.ID);
                    if (c.Lead_Prioritization__c == null){                   
                        c.Lead_Prioritization__c = MapChildstoUpdate.get(c.parent_campaign__c);
                        
                        toUpdateChilds.add (c);
                    }
                }
            }
            Update toUpdateChilds;
            
        } 
        if (childCampaignIds.size () > 0)
            updateLeadPrioritization (childCampaignIds);
    }
    
    //TOBE Implemented
    public void executeBeforeInsertUpdateTrigger(list<sObject> lstNewRecords, Map<Id,sObject> mapOldRecords){ }
    public void executeBeforeDeleteTrigger(Map<Id, sObject> mapOldRecords){ }
    public void executeAfterInsertTrigger(Map<Id, sObject> mapNewRecords){ }
    public void executeAfterDeleteTrigger(Map<Id, sObject> mapOldRecords){ }
    public void executeAfterInsertUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){ }    
    
}// End of class.