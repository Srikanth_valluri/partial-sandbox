@isTest
public class SandboxPostRefreshTest {
    @isTest
    static void testMySandboxPrep(){
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account obj = new Account();
        obj.FirstName = 'Test';
        obj.LastName = 'Test';
        obj.Email__c = 'test@test.com';
        obj.Email__pc = 'test@test.com';
        obj.Email_2__c = 'test@test.com';
        obj.Email_2__pc = 'test@test.com';
        obj.Email_3__c = 'test@test.com';
        obj.Email_3__pc = 'test@test.com';
        obj.Email_4__pc = 'test@test.com';
        obj.Email_5__pc = 'test@test.com';
        obj.PersonEmail = 'test@test.com';
        obj.Mobile__c = '00975674564454';
        obj.Mobile_Phone_Encrypt__pc = '00975674564454';
        obj.RecordTypeId = personAccRTId;
        insert obj;
        
        String digitalRecordType=DamacUtility.getRecordTypeId('Campaign__c', 'Digital');
        Id agenTeamRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        Inquiry__c inqObj= new Inquiry__c(Is_Meeting_Scheduled__c= true, Meeting_Due_Date__c = Date.today (), RecordTypeId=agenTeamRT,Pre_InquiryId__c='123456',Mobile_Phone_Encrypt__c='456123',Mobile_CountryCode__c='American Samoa: 001684',Mobile_Phone__c='1234',Email__c='mk@gmail.com',First_Name__c='Test',Last_Name__c='Last',CR_Number__c='0987',/*ORN_Number__c='7842',Agency_Type__c='Corporate',isDuplicate__c=false,*/Organisation_Name__c = 'Oliver');
        insert inqObj;
        
        FM_Case__c objCase = new FM_Case__c();
        insert objCase;
        
        
        Test.startTest();
        Test.testSandboxPostCopyScript(
            new SandboxPostRefresh(), 
            UserInfo.getOrganizationId(),
            UserInfo.getOrganizationId(), 
            UserInfo.getOrganizationName()
        );
        Test.stopTest();
    }
}