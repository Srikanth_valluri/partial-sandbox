/* * * * * * * * * * * * * *
*  Class Name:   GeneratePromotionsOfferLetterTest
*  Purpose:      Unit test class for GeneratePromotionsOfferLetter Controller
*  Author:       Hardik Mehta - ESPL
*  Company:      ESPL
*  Created Date: 20-Feb-2018
*  Updated Date: 20-Feb-2018
*  Type:         Test Class
* * * * * * * * * * * * */
@isTest
public with sharing class GeneratePromotionsOfferLetterTest 
{
    /* * * * * * * * * * * * *
    *  Method Name:  generateOfferAcceptanceLetterTest
    *  Purpose:      This method is used to unit test functionality generateOfferAcceptanceLetter method
    *  Author:       Hardik Mehta
    *  Company:      ESPL
    *  Created Date: 20-Feb-2018
    * * * * * * * * * * * * */
    static testmethod void generateOfferAcceptanceLetterTest()
    {   
        //UserRole objUserRole = [Select Id from UserRole where name = 'Collection - CRE'];

        //Profile pf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];

        //User usr1 = createTestUser(objUserRole.Id , pf.Id, 'Test FirstName1', 'Test LastName1',null);
        //insert usr1;

        //System.runAs(usr1)
        //{   
            Account objAccount = TestDataFactory_CRM.createPersonAccount();
            insert objAccount;
            
            Case objCase = new Case();
            objCase.AccountId = objAccount.Id;
            objCase.Status = 'Submitted';
            objCase.Is_New_Payment_Terms_Applied__c = True;
            objCase.Type = 'Signed Promotions Offer Letter';  
            objCase.Account_Email__c = 'test@gmail.com';
            objCase.Approval_Status__c = 'Approved';
            //objCase.OwnerId = usr1.Id;
            insert objCase;
            
            objCase = [Select Id,OwnerId From Case where Id =: objCase.Id];

            Test.startTest();
                Task objTask = TestDataFactory_CRM.createTask( (Sobject)objCase, 'Generate Letter For Promotion Request', 'Test', 'Promotion Package', system.today() );
                insert objTask;
                ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
                GeneratePromotionsOfferLetter objGenerate = new GeneratePromotionsOfferLetter(stdController);
                objGenerate.caseId = objCase.Id;
                objGenerate.generateOfferAcceptanceLetter();
            Test.stopTest();
        //}
    }

    /* * * * * * * * * * * * *
    *  Method Name:  generateOfferAcceptanceLetterTest2
    *  Purpose:      This method is used to unit test functionality generateOfferAcceptanceLetter method
    *  Author:       Hardik Mehta
    *  Company:      ESPL
    *  Created Date: 20-Feb-2018
    * * * * * * * * * * * * */
    static testmethod void generateOfferAcceptanceLetterTest2()
    {   
        //UserRole objUserRole = [Select Id from UserRole where name = 'Collection - CRE'];

        //Profile pf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];

        //User usr1 = createTestUser(objUserRole.Id , pf.Id, 'Test FirstName1', 'Test LastName1',null);
        //insert usr1;

        //System.runAs(usr1)
        //{
            Account objAccount = TestDataFactory_CRM.createPersonAccount();
            insert objAccount;
            
            Case objCase = new Case();
            objCase.AccountId = objAccount.Id;
            objCase.Status = 'Submitted';
            objCase.Is_New_Payment_Terms_Applied__c = True;
            objCase.Type = 'Signed Promotions Offer Letter';  
            objCase.Account_Email__c = 'test@gmail.com';
            //objCase.OwnerId = usr1.Id;
            insert objCase;

            objCase = [Select Id,OwnerId From Case where Id =: objCase.Id];
            Test.startTest();
                Task objTask = TestDataFactory_CRM.createTask( (Sobject)objCase, 'Generate Letter For Promotion Request', 'Test', 'Promotion Package', system.today() );
                insert objTask;
                ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
                GeneratePromotionsOfferLetter objGenerate = new GeneratePromotionsOfferLetter(stdController);
                objGenerate.caseId = objCase.Id;
                objGenerate.generateOfferAcceptanceLetter();
            Test.stopTest();
        //}
    }

    /* * * * * * * * * * * * *
    *  Method Name:  generateOfferAcceptanceLetterTest3
    *  Purpose:      This method is used to unit test functionality generateOfferAcceptanceLetter method
    *  Author:       Hardik Mehta
    *  Company:      ESPL
    *  Created Date: 20-Feb-2018
    * * * * * * * * * * * * */
    static testmethod void generateOfferAcceptanceLetterTest3()
    {
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        
        Case objCase = new Case();
        objCase.AccountId = objAccount.Id;
        objCase.Status = 'New';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Type = 'Signed Promotions Offer Letter';  
        objCase.Account_Email__c = 'test@gmail.com';
        insert objCase;
       

        Test.startTest();
            ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
            GeneratePromotionsOfferLetter objGenerate = new GeneratePromotionsOfferLetter(stdController);
            objGenerate.caseId = objCase.Id;
            objGenerate.generateOfferAcceptanceLetter();
        Test.stopTest();
    }

    /* * * * * * * * * * * * *
    *  Method Name:  generateOfferAcceptanceLetterTest4
    *  Purpose:      This method is used to unit test functionality generateOfferAcceptanceLetter method
    *  Author:       Hardik Mehta
    *  Company:      ESPL
    *  Created Date: 20-Feb-2018
    * * * * * * * * * * * * */
    static testmethod void generateOfferAcceptanceLetterTest4()
    {
        
        Case objCase = new Case();

        Test.startTest();
            ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
            GeneratePromotionsOfferLetter objGenerate = new GeneratePromotionsOfferLetter(stdController);
            
            objGenerate.generateOfferAcceptanceLetter();
            objGenerate.caseId = 'a0x25000000BD3m';
        Test.stopTest();
    }

    /* * * * * * * * * * * * *
    *  Method Name:  createTestUser
    *  Purpose:      This method is used to create test user
    *  Author:       Hardik Mehta
    *  Company:      ESPL
    *  Created Date: 20-Feb-2018
    * * * * * * * * * * * * */
    public static User createTestUser(Id roleId , Id profID, String fName, String lName, User objUser)
    {
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
       
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        User tuser = new User(  firstname = fName,
                                lastName = lName,
                                email = uniqueName + '@test' + orgId + '.org',
                                Username = uniqueName + '@test' + orgId + '.org',
                                EmailEncodingKey = 'ISO-8859-1',
                                Alias = uniqueName.substring(18, 23),
                                TimeZoneSidKey = 'America/Los_Angeles',
                                LocaleSidKey = 'en_US',
                                LanguageLocaleKey = 'en_US',
                                ProfileId = profId,
                                UserRoleId = roleId
                             );
        if(objUser != null)
        {
            tuser.ManagerId = objUser.Id;
        }
        return tuser;
    }
}