public without sharing class FullCalendarController {

    private static final String DATE_FORMAT = 'EEE, d MMM yyyy HH:mm:ss z';
    private static final String FORMAT = 'YYYY-MM-dd';
    public String ReceivedAppointQry {get;set;}   

    public FullCalendarController() {}
    @RemoteAction
    public static List<CalendarEvent> getCalendarEvents(String queryStr) {
        list<Appointment__c> newSlots = new list<Appointment__c>();
        String now = (String.valueOf(System.Now().time())).substringBeforelast(':');
        system.debug('now '+now);
        system.debug('>>>queryStr'+queryStr);
        list<Appointment__c> lstAppoints = new list<Appointment__c>();
        if(String.isNotBlank(queryStr)) {
            lstAppoints = Database.Query(queryStr);
        }
        system.debug('>>>lstAppoints'+lstAppoints.size());
        /*String newQuery = queryStr.replace('>=','=');
        newQuery = newQuery.replace(newQuery.substringBetween('= ',' AND'),(DateTime.newInstance(((System.Today()).addDays(1)).year(), ((System.Today()).addDays(1)).month(), ((System.Today()).addDays(1)).day())).format('YYYY-MM-dd'));
        system.debug('>>>newQuery'+newQuery);
        newSlots = Database.Query(newQuery);
                for(Appointment__c appointment : newSlots){
                    system.debug('slotss'+(appointment.Slots__c).substringBefore('-'));
                    if((appointment.Slots__c).substringBefore('-') >= now){
                        system.debug('slot'+(appointment.Slots__c));
                        lstAppoints.add(appointment);
                    }
                }
        system.debug('>>>lstAppoints'+lstAppoints.size());*/
        List<CalendarEvent> lstCalendarEvent = new List<CalendarEvent>();
        Map<String, CalendarEvent> mapCalendarEvents = new Map<String, CalendarEvent>();
        for (Appointment__c appObj : lstAppoints) {
            CalendarEvent event = new CalendarEvent(appObj);
            mapCalendarEvents.put(event.startString + event.endString, event);          
        }
        return mapCalendarEvents.values();
    }

    public class CalendarEvent {
        public String title {get;set;}
        public Boolean allDay {get;set;}
        public String startString {get;set;}
        public String startDate {get;set;}
        public String endString {get;set;}
        public String url {get;set;}
        public String className {get;set;}
        public String eventType {get;set;}

        public CalendarEvent() {}

        public CalendarEvent(Appointment__c objAppoints) {
            //this.title = 'Available Slot';
            this.allDay = false;            
            this.startDate = objAppoints.Appointment_Date__c == NULL ? NULL : Datetime.newInstance(
                objAppoints.Appointment_Date__c.year(), objAppoints.Appointment_Date__c.month(), objAppoints.Appointment_Date__c.day()
            ).format(FORMAT);
            this.startString =objAppoints.Appointment_Date__c == NULL ? NULL : Datetime.newInstance(
                objAppoints.Appointment_Date__c.year(), objAppoints.Appointment_Date__c.month(), 
                objAppoints.Appointment_Date__c.day()).format(FORMAT) + 'T'
                + objAppoints.Slots__c.split('-')[0].remove(' ')+':00' ;
            this.endString = objAppoints.Appointment_Date__c == NULL ? NULL : Datetime.newInstance(
                objAppoints.Appointment_Date__c.year(), objAppoints.Appointment_Date__c.month(), 
                objAppoints.Appointment_Date__c.day()).format(FORMAT)+ 'T'
                + objAppoints.Slots__c.split('-')[1].remove(' ')+':00' ;
            //this.url = '/' + task.Id;
            this.eventType = 'slot';
        }
    }
}