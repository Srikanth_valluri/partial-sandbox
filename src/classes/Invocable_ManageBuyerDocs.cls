public class Invocable_ManageBuyerDocs {
    
    @InvocableMethod
    public static void ManageBuyerDocs(List<Id> stepId) {
        for (New_Step__c step :[SELECT Id, Service_Request__c, Service_Request__r.RecordType.DeveloperName FROM New_Step__c WHERE Id IN :stepId]) {
            EvaluateCustomCode(step);
        }
    }
    
    public static String EvaluateCustomCode(New_Step__c step) {
        system.debug('step-----------------------' + step.Id);
        system.debug('SR-----------------------' + step.Service_Request__c);
        
        try {
            String retStr = 'Success';
            if (step.Service_Request__c != null) {
                createDocsForBuyer(step.Service_Request__c, step.Service_Request__r.RecordType.DeveloperName);
            }
            return retstr;
        }
        catch(exception ex) {
            return ex.getMessage();
        }
    }
    
    static Map<String, ID> uniqueUnitDocsMap;
    
    public static void createDocsForBuyer(Id srid, string recTypeName) {
        //Get all docs
        uniqueUnitDocsMap = new Map<string, Id> ();
        for (Unit_Documents__c unitDoc :[SELECT Name, Unique_Doc_Name__c FROM Unit_Documents__c WHERE Buyer__c != null AND Service_Request__c = :srid]){
            uniqueUnitDocsMap.put(unitDoc.Unique_Doc_Name__c, unitDoc.Id);
        }
        
        List<Constants__c> lstdocsConfig = (List<Constants__c>) srutility.getRecords('Constants__c', ' where IsBuyerReqDocs__c = true');
        system.debug('lstdocsConfig------------------------' + lstdocsConfig);
        Map<string, Map<string, Constants__c>> minordocs = new Map<string, Map<string, Constants__c>> ();
        Map<string, Map<string, Constants__c>> mpUniqCodeReqDocs = new Map<string, Map<string, Constants__c>> ();
        for (Constants__c c : lstdocsConfig) {
            if (c.Associate_Type__c != null && c.Country__c != null) {
                string city = '';
                if (c.City__c != null) {
                    city = c.City__c;
                }
                string key = c.Associate_Type__c + c.Country__c + city;
                key = key.toLowerCase();
                //for associate type corporate and individual
                if (c.Associate_Type__c != 'Minor') {
                    if (mpUniqCodeReqDocs.containsKey(key)) {
                        Map<string, Constants__c> mpc = mpUniqCodeReqDocs.get(key);
                        mpc.put(c.Doc_Code__c, c);
                        mpUniqCodeReqDocs.put(key, mpc);
                    } else {
                        Map<string, Constants__c> mpc = new Map<string, Constants__c> ();
                        mpc.put(c.Doc_Code__c, c);
                        mpUniqCodeReqDocs.put(key, mpc);
                    }
                }
                //for associate type minor
                else if (c.Associate_Type__c == 'Minor') {
                    Map<string, Constants__c> mpc = new Map<string, Constants__c> ();
                    if (minordocs.containsKey(key)) { mpc = minordocs.get(key); }
                    mpc.put(c.Doc_Code__c, c);
                    minordocs.put(key, mpc);
                }
            }
        }
        system.debug('---mpUniqCodeReqDocs->>>>' + mpUniqCodeReqDocs.size());
        system.debug('---minordocs->>>>' + minordocs.size());
        
        Map<id, List<Booking_Unit__c>> mpBKIDlstBU = new Map<id, List<Booking_Unit__c>> ();
        for (Booking__c b :[select id, (select id, Inventory__c, Booking__c, Inventory__r.Property_Country__c, Inventory__r.Property_City__c
                                        from Booking_Units__r where Status__c != 'Removed') from Booking__c where Deal_SR__c = :srid]) {
                                            if (b.Booking_Units__r != null && !b.Booking_Units__r.isempty()) {
                                                mpBKIDlstBU.put(b.id, b.Booking_Units__r);
                                            }
                                            
                                        }
        
        Map<id, List<Buyer__c>> mpBKIDlstByrs = new Map<id, List<Buyer__c>> ();
        for (Booking__c b :[select id, (select id, name, Date_of_Birth__c, DOB__c, Nationality__c, Country__c, Buyer_Type__c,
                                        Organisation_Name__c, Passport_Number__c, Booking__c, First_Name__c,
                                        CR_Number__c
                                        from Buyers__r) from Booking__c where Deal_SR__c = :srid]) {
                                            if (b.Buyers__r != null && !b.Buyers__r.isempty()) {
                                                mpBKIDlstByrs.put(b.id, b.Buyers__r);
                                            }
                                            
                                        }
        
        List<Buyer__c> lstByrs = new List<Buyer__c> ();
        for (id buID : mpBKIDlstByrs.keyset()) {
            lstByrs.addall(mpBKIDlstByrs.get(buID));
        }
        //get unique buyer types
        set<string> stBuyerstype = new set<string> ();
        for (Buyer__c bu : lstByrs) {
            if (bu.Buyer_Type__c != null)
                stBuyerstype.add(bu.Buyer_Type__c);
        }
        
        Map<Id, Map<string, constants__c>> mpBIDTypeReqDocs = new Map<Id, Map<string, constants__c>> ();
        Map<Id, Map<string, constants__c>> mpBIDTypeReqDocsMinor = new Map<Id, Map<string, constants__c>> ();
        
        for (id bID : mpBKIDlstBU.keyset()) {
            Map<string, constants__c> mpTypeReqDocs = new Map<string, constants__c> ();
            Map<string, constants__c> mpTypeReqDocsMinor = new Map<string, constants__c> ();
            
            for (Booking_Unit__c bu : mpBKIDlstBU.get(bID)) {
                if (bu.Inventory__r.Property_Country__c != null) {
                    //for each uniq buyer type and based on country and city get required docs
                    for (string buyrType : stBuyerstype) {
                        string keyDocset = string.valueof(buyrType + bu.Inventory__r.Property_Country__c).tolowercase();
                        string keyDocsetMinor = string.valueof('Minor' + bu.Inventory__r.Property_Country__c).tolowercase();
                        //for associate/individual
                        if (keyDocset != null && keyDocset != '' && mpUniqCodeReqDocs.containskey(keyDocset)) {
                            mpTypeReqDocs.putall(mpUniqCodeReqDocs.get(keyDocset));
                        }
                        if (keyDocsetMinor != null && keyDocsetMinor != '' && mpUniqCodeReqDocs.containskey(keyDocsetMinor)) {
                            mpTypeReqDocsMinor.putall(mpUniqCodeReqDocs.get(keyDocsetMinor));
                        }
                        //for minor
                        if (bu.Inventory__r.Property_City__c != null) {
                            string keywCityDocset = string.valueof(buyrType + bu.Inventory__r.Property_Country__c + bu.Inventory__r.Property_City__c).tolowercase();
                            string keywCityDocsetMinor = string.valueof('Minor' + bu.Inventory__r.Property_Country__c + bu.Inventory__r.Property_City__c).tolowercase();
                            if (keywCityDocset != null && keywCityDocset != '' && mpUniqCodeReqDocs.containskey(keywCityDocset)) {
                                mpTypeReqDocs.putall(mpUniqCodeReqDocs.get(keywCityDocset));
                            }
                            if (keywCityDocsetMinor != null && keywCityDocsetMinor != '' && mpUniqCodeReqDocs.containskey(keywCityDocsetMinor)) {
                                mpTypeReqDocsMinor.putall(mpUniqCodeReqDocs.get(keywCityDocsetMinor));
                            }
                        }
                    }
                }
            }
            //for associate or individual
            if (!mpTypeReqDocs.isempty())
                mpBIDTypeReqDocs.put(bid, mpTypeReqDocs);
            //for minor
            if (!mpTypeReqDocsMinor.isempty())
                mpBIDTypeReqDocsMinor.put(bid, mpTypeReqDocsMinor);
            
        }
        system.debug('---mpBIDTypeReqDocsMinor->>>>' + mpBIDTypeReqDocsMinor.size());
        system.debug('---mpBIDTypeReqDocs->>>>' + mpBIDTypeReqDocs.size());
        //Get all srtemplatedocs
        List<Document_Template__c> lstDocTemplate = [Select Name, Code__c, Sys_Is_Generated_Doc__c, 
                                                     Optional__c, Document_Name__c, Group_No__c,
                                                     Generate_Document__c, Document_Description__c
                                                     From Document_Template__c
                                                     Where SR_Record_Type__c = :recTypeName 
                                                     AND Added_through_Code__c = true];
        
        Map<string, integer> mpGCClist = new Map<string, integer> ();
        for (string strGCC : string.valueof(Label.BuyerNationalityGCCList).split(',')) {
            mpGCClist.put(strGCC.tolowercase(), 1);
        }
        Map<string, Unit_Documents__c> mpuniqKeyDoc = new Map<string, Unit_Documents__c> ();
        for (id bid : mpBKIDlstByrs.keyset()) {
            for (Buyer__c b : mpBKIDlstByrs.get(bid)) {
                string docNameprefixBuyer = '';
                if (b.Buyer_Type__c == 'Corporate') {
                    docNameprefixBuyer = b.Organisation_Name__c;
                } else {
                    docNameprefixBuyer = b.first_name__c;
                }
                
                string locdocKey = b.Passport_Number__c == null ? b.CR_Number__c : b.Passport_Number__c;
                if (locdocKey != null) {
                    boolean isminor = false;
                    
                    if (b.DOB__c != null && b.Buyer_Type__c != 'Corporate') {
                        //Integer days = Date.parse(b.Date_of_Birth__c).daysBetween(Date.Today());
                        //DOB__c
                        Integer days = (b.DOB__c).daysBetween(Date.Today());
                        Integer age = Integer.valueOf(days / 365);
                        //system.debug('---b.Date_of_Birth__c->>>>'+b.Date_of_Birth__c +' --days '+days+'--- age '+age);
                        isminor = age >= 21 ? false : true;
                    }
                    boolean isGCC = false;
                    string filterCond = '';
                    string buyerCtrFilterC = '';
                    if (b.Nationality__c != null && String.isNotBlank(b.Nationality__c)) {
                        isGCC = (mpGCClist.containskey(b.Nationality__c.tolowercase())) ? true : false;
                        if (isGCC) {
                            filterCond = 'Is GCC';
                        } else {
                            filterCond = 'Is Non GCC';
                        }
                    }
                    if (b.Country__c != null && String.isNotBlank(b.Country__c))
                    {
                        if (b.Country__c.tolowercase() == 'united arab emirates') {
                            buyerCtrFilterC = 'Is UAE';
                        } else {
                            buyerCtrFilterC = 'Is Non UAE';
                        }
                    }
                    system.debug('---filterCond->>>>' + filterCond + '-- Minor' + isminor);
                    //If not minor
                    if (mpBIDTypeReqDocs.containskey(b.booking__c) && mpBIDTypeReqDocs.get(b.booking__c) != null && !isminor) {
                        for (Document_Template__c tmpdoc : lstDocTemplate) {
                            String docCode = tmpdoc.Code__c;
                            String docName = tmpdoc.Name;
                            //if doc has to be created.
                            if (mpBIDTypeReqDocs.get(b.booking__c).containskey(docCode)) {
                                Constants__c clocal = mpBIDTypeReqDocs.get(b.booking__c).get(docCode);
                                //if non of the filters are applied
                                if (clocal.Filter_Doc__c == null || clocal.Filter_Doc__c == '') {
                                    system.debug('---inside doc creation->>>>' + docCode);
                                    Unit_Documents__c sdoc = createUnitDoc(docNameprefixBuyer + '-' + docName, tmpdoc, b.id, srid);
                                    mpuniqKeyDoc.put(locdocKey + docCode, sdoc);
                                }
                                if (filterCond != '' && buyerCtrFilterC != '' && clocal.Filter_Doc__c == filterCond && clocal.Buyer_Country__c == buyerCtrFilterC) {
                                    system.debug('---inside doc creation->>>>' + docCode);
                                    Unit_Documents__c sdoc = createUnitDoc(docNameprefixBuyer + '-' + docName, tmpdoc, b.id, srid);
                                    mpuniqKeyDoc.put(locdocKey + docCode, sdoc);
                                }
                                //if any filter, match the values
                                if (filterCond != '' && clocal.Filter_Doc__c == filterCond && clocal.Buyer_Country__c == null) {
                                    system.debug('---inside doc creation->>>>' + docCode);
                                    Unit_Documents__c sdoc = createUnitDoc(docNameprefixBuyer + '-' + docName, tmpdoc, b.id, srid);
                                    mpuniqKeyDoc.put(locdocKey + docCode, sdoc);
                                }
                            }
                        }
                    }
                    //if minor
                    if (mpBIDTypeReqDocsMinor.containskey(b.booking__c) && mpBIDTypeReqDocsMinor.get(b.booking__c) != null && isminor) {
                        for (Document_Template__c tmpdoc : lstDocTemplate) {
                            String docCode = tmpdoc.Code__c;
                            String docName = tmpdoc.Name;
                            //if doc has to be created. for now filters are not applied in case of minor.
                            if (mpBIDTypeReqDocsMinor.get(b.booking__c).containskey(docCode)) {
                                system.debug('---inside doc creation minor->>>>' + docCode);
                                Unit_Documents__c sdoc = createUnitDoc(b.first_name__c + '-' + docName, tmpdoc, b.id, srid);
                                mpuniqKeyDoc.put(locdocKey + docCode, sdoc);
                            }
                        }
                    }
                }
            }
        }
        
        if (mpuniqKeyDoc != null && !mpuniqKeyDoc.isempty()) {
            system.debug('---mpuniqKeyDoc->>>>' + mpuniqKeyDoc.size());
            //upsert mpuniqKeyDoc.values() Unique_SR_Doc_Name__c;
            upsert mpuniqKeyDoc.values();
        }
    }
    
    public static Unit_Documents__c createUnitDoc(String docName, Document_Template__c tmpDoc, Id buyerid, id srID) {
        system.debug('---inside createUnitDoc->>>>');
        Unit_Documents__c unitDoc = new Unit_Documents__c();
        
        if (docName.length() > 80)
            docName = docName.subString(0, 79);
        unitDoc.Document_Name__c = docName;
        unitDoc.Service_Request__c = srID;
        unitDoc.Buyer__c = buyerid;
        unitDoc.Document_Template__c = tmpDoc.Id;
        unitDoc.Sys_Unique_Doc__c = tmpDoc.Name + '_' + buyerid;
        unitDoc.Unique_Doc_Name__c = unitDoc.Sys_Unique_Doc__c;
        if(uniqueUnitDocsMap.containsKey(unitDoc.Unique_Doc_Name__c)) {
            unitDoc.Id = uniqueUnitDocsMap.get(unitDoc.Unique_Doc_Name__c);
        }else{
            unitDoc.Status__c = 'Pending Upload';
            unitDoc.Group_No__c = tmpDoc.Group_No__c;
            unitDoc.Is_Required__c = !tmpDoc.Optional__c;
            unitDoc.Generate_Document__c = tmpDoc.Generate_Document__c;
            unitDoc.Document_Description__c = tmpDoc.Document_Description__c;
            unitDoc.Sys_IsGenerated_Doc__c = tmpDoc.Sys_Is_Generated_Doc__c;
            unitDoc.From_Finalize__c = true;
        }
        system.debug('---unitDoc->>>>' + unitDoc);
        return unitDoc;
    }
}