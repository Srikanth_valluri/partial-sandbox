@isTest
public class AP_CollateralsControllerTest {
    
    static testMethod void testMethodOne(){
        Account accObj = new Account();
        accObj.Name='acc1';
        accObj.Country__c = 'India';
        insert accObj;
        
        Contact cont = new Contact();
        cont.FirstName='Test';
        cont.LastName='Test';
        cont.Accountid = accObj.id;
        insert cont;
        
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersionInsert;
        
        Announcement__c anncObj = new Announcement__c();
        anncObj.Start_Date__c = system.today().adddays(-3);
        anncObj.End_Date__c = system.today().adddays(3);
        anncObj.Title__c = 'Test';
        // anncObj.Account__c = accObj.id;
        anncObj.Active__c = true;
        anncObj.Countries_Include__c = 'India';
        anncObj.Description__c = 'Test';
        anncObj.Agency_Type__c = 'Corporate';
        insert anncObj;
        Announcement_Section__c section = new Announcement_Section__c();
        section.Announcement__c = anncObj.Id;
        section.Type__c = 'Image';
        INSERT section;
        
        section = new Announcement_Section__c();
        section.Announcement__c = anncObj.Id;
        section.Type__c = 'PDF';
        INSERT section;
        
        section = new Announcement_Section__c();
        section.Announcement__c = anncObj.Id;
        section.Type__c = 'Video';
        INSERT section;
        
         section = new Announcement_Section__c();
        section.Announcement__c = anncObj.Id;
        section.Type__c = 'Youtube';
        INSERT section;
        Announcement_Accounts__c obj = new Announcement_Accounts__c ();
        obj.Account__c = accObj.Id;
        obj.Announcement__c = anncObj.id;
        insert obj;
        
        ContentVersion contentVersionSelect = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersionInsert.Id LIMIT 1];
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=anncObj.id;
        contentlink.ShareType= 'I';
        contentlink.ContentDocumentId=documents[0].Id;
        contentlink.Visibility = 'AllUsers'; 
        insert contentlink;
        
        //List<ContentDocument> contentList = createContentDocument(anncObj.id);
        AP_CollateralsController controller = new AP_CollateralsController();
        controller.strSelectedLanguage = 'En';
        controller.weekType = 'THIS_MONTH';
        
    }  
    
    static testMethod void testMethodTWO(){
        Account accObj = new Account();
        accObj.Name='acc1';
        accObj.Country__c = 'India';
        insert accObj;
        
        Contact cont = new Contact();
        cont.FirstName='Test';
        cont.LastName='Test';
        cont.Accountid = accObj.id;
        insert cont;
        
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersionInsert;
        
        Announcement__c anncObj = new Announcement__c();
        anncObj.Start_Date__c = system.today().adddays(-3);
        anncObj.End_Date__c = system.today().adddays(3);
        anncObj.Title__c = 'Test';
        // anncObj.Account__c = accObj.id;
        anncObj.Active__c = true;
        anncObj.Countries_Include__c = 'India';
        anncObj.Description__c = '<url><image>test.com</image><video>test.com</video></url>';
        anncObj.Agency_Type__c = 'All';
        insert anncObj;
        Announcement_Accounts__c obj = new Announcement_Accounts__c ();
        obj.Account__c = accObj.Id;
        obj.Announcement__c = anncObj.id;
        insert obj;
        
        ContentVersion contentVersionSelect = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersionInsert.Id LIMIT 1];
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=anncObj.id;
        contentlink.ShareType= 'I';
        contentlink.ContentDocumentId=documents[0].Id;
        contentlink.Visibility = 'AllUsers'; 
        insert contentlink;
        
        //List<ContentDocument> contentList = createContentDocument(anncObj.id);
        AP_CollateralsController controller = new AP_CollateralsController();
        AP_CollateralsController.fileSizeToString (1028);
        AP_CollateralsController.fileSizeToString (1028*1028);
        controller.updateReadAnnouncements();
    }  
    
}