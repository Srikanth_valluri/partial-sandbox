/*
* Description - Class used as hepler for the main RentalPoolTerminationController class.
*
* Version            Date            Author            Description
* 1.0                                Ashish           Initial Draft
*/

public with sharing class RentalPoolTerminationHelper {
    
    public static PenaltyWaiverHelper.CaseListWrapper getCaseAndUnitDetailsWrapper( Booking_Unit__c objUnit, set<String> setRecordTypesName ){
        PenaltyWaiverHelper.CaseListWrapper objCaseListWrapper = new PenaltyWaiverHelper.CaseListWrapper();
        if( objUnit != null ){
            
            for( Case objCase : PenaltyWaiverUtility.getAllRelatedCases( objUnit.Id ) ) {
                if( String.isNotBlank( objCase.Status ) && objCase.RecordTypeId != null ) {
                    if( objCase.Status.equalsIgnoreCase( 'Closed' ) || objCase.Status.equalsIgnoreCase( 'Rejected' ) || 
                        objCase.Status.equalsIgnoreCase( 'Cancelled' ) ) {
                        objCaseListWrapper.lstCloseCase.add( objCase );
                    }
                    else {
                        objCaseListWrapper.lstOpenCase.add( objCase );
                        if( setRecordTypesName.contains( objCase.RecordType.DeveloperName ) ) {
                            objCaseListWrapper.isAllowedToOpenPenaltyCase = false ;
                            objCaseListWrapper.strOpenSrType = objCase.RecordType.Name ;
                        }
                    }
                    
                    if( objCase.RecordType.DeveloperName.equalsIgnoreCase( 'Rental_Pool_Agreement' ) ) {
                        objCaseListWrapper.caseobj = objCase ;
                    }
                }
            }
        }
        return objCaseListWrapper;
    }
    
    public static String extractName( String strName ) { 
        return strName.substring( strName.lastIndexOf('\\')+1 ) ;
    }
    
    public static Blob extractBody( String strBody ) {
        strBody = EncodingUtil.base64Decode( strBody ).toString();
        return EncodingUtil.base64Decode( strBody.substring( strBody.lastIndexOf(',')+1 ) );
    }
    
    public static Task createTaskObject( String strAssignedUser, String strPriority, 
                                          String strProcessName, String strStatus, String strSubject,
                                          Case objCase, String strCurrencyIsoCode ) {
        Task objTask = new Task();
        objTask.ActivityDate = system.today().addDays(1);
        objTask.Assigned_User__c = strAssignedUser ;
        //objTask.CurrencyIsoCode = strCurrencyIsoCode ;
        objTask.OwnerId = UserInfo.getUserId();
        objTask.Priority = strPriority ;
        objTask.Process_Name__c = strProcessName ;
        objTask.Status = strStatus ;
        objTask.Subject = strSubject ;
        objTask.WhatId = objCase.Id;
        return objTask ;
    }
    
    public static AOPTDocumentGenerationXsd.DocGenDTO createAttributesWrapper( Booking_Unit__c objUnit, UnitDetailsService.BookinUnitDetailsWrapper objUnitdetails ) {
        AOPTDocumentGenerationXsd.DocGenDTO objAttributes = new AOPTDocumentGenerationXsd.DocGenDTO();
        Datetime dt = system.now();
        objAttributes.regId = String.isNotBlank( objUnit.Registration_ID__c ) ? objUnit.Registration_ID__c : '';
        objAttributes.attribute1 = String.isNotBlank( objUnitdetails.strProject ) ? objUnitdetails.strProject : '';
        objAttributes.attribute2 = '';
        objAttributes.attribute3 = '';
        objAttributes.attribute4 = '';
        objAttributes.attribute5 = '';
        objAttributes.attribute6 = dt != null ? String.valueOf(dt.format('dd-MMM-yyyy').toUpperCase()) : '';
                                   
        objAttributes.attribute7 = String.isNotBlank( objUnit.Unit_Name__c ) ? objUnit.Unit_Name__c : '';
        objAttributes.attribute8 = String.isNotBlank( objUnit.Building_Name__c ) ? objUnit.Building_Name__c : '';
        objAttributes.attribute9 = String.isNotBlank( objUnit.Inventory__r.Master_Community_EN__c ) ? objUnit.Inventory__r.Master_Community_EN__c : '';
        objAttributes.attribute10 = String.isNotBlank( objUnit.District__c ) ? objUnit.District__c : '';
        objAttributes.attribute11 = dt != null ? String.valueOf(dt.format('dd-MMM-yyyy').toUpperCase()) : '';
        objAttributes.attribute12 = String.isNotBlank( objUnitdetails.strProject ) ? objUnitdetails.strProject : '';
        return objAttributes;
    }
    
    public static AOPTDocumentGenerationXsd.DocGenDTO createWrapperObject( Case objCase, String strEmail ) {
        AOPTDocumentGenerationXsd.DocGenDTO objAttributes = new AOPTDocumentGenerationXsd.DocGenDTO();
        objAttributes.regId = String.isNotBlank( objCase.Booking_Unit__r.Registration_ID__c ) ? objCase.Booking_Unit__r.Registration_ID__c : '';
        objAttributes.attribute1 = String.valueOf( system.today() ) ;
        objAttributes.attribute2 = String.isNotBlank( objCase.Account.Name ) ? objCase.Account.Name : '';
        if( objCase.Account.IsPersonAccount ) {
            objAttributes.attribute3 = String.isNotBlank( objCase.Account.Country__pc ) ? objCase.Account.Country__pc : '' ;
            objAttributes.attribute4 = String.isNotBlank( objCase.Account.City__pc ) ? objCase.Account.City__pc : '' ;
        }
        else {
            objAttributes.attribute3 = String.isNotBlank( objCase.Account.Country__c ) ? objCase.Account.Country__c : '';
            objAttributes.attribute4 = String.isNotBlank( objCase.Account.City__c ) ? objCase.Account.City__c : '';
        }
        objAttributes.attribute5 = String.isNotBlank( strEmail ) ? strEmail : '';
        objAttributes.attribute6 = String.isNotBlank( objCase.Booking_Unit__r.Unit_Name__c ) ? objCase.Booking_Unit__r.Unit_Name__c :'' ;
        objAttributes.attribute7 = String.isNotBlank( objCase.Account.Name ) ? objCase.Account.Name : '' ;
        objAttributes.attribute8 = String.isNotBlank( String.valueOf( objCase.Final_Termination_Date__c ) ) ? String.valueOf( objCase.Final_Termination_Date__c ) : '';
        objAttributes.attribute9 = '';
        objAttributes.attribute10 = String.isNotBlank( objCase.Booking_Unit__r.Unit_Name__c ) ? objCase.Booking_Unit__r.Unit_Name__c :'' ;
        objAttributes.attribute11 = String.isNotBlank( objCase.Project_Name__c ) ? objCase.Project_Name__c :'' ;
        objAttributes.attribute12 = String.isNotBlank( String.valueOf( objCase.Final_Termination_Date__c ) ) ? String.valueOf( objCase.Final_Termination_Date__c ) : '';
        return objAttributes;
    } 
    
}