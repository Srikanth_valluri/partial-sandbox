@istest
public class DAMAC_Project_Avaliability_Test{


    static testmethod void callMethods(){
        user u = [select id from user where id!=:userInfo.getUserId() LIMIT 1];
        List<Inventory__c> invList=TestDataFactory.CreateInventoryRecords(new List<Inventory__c>{new Inventory__c()});
        List<Inventory__c> invList1=TestDataFactory.CreateInventoryRecords(new List<Inventory__c>{new Inventory__c()});
        invList1[0].status__c = 'Released';
        invList1[0].property_city__c = 'Dubai';
        invList1[0].Property_Status__c = 'Ready';
        invList1[0].Unit_Type__c = 'Hotel';
        invList1[0].property_name__c = 'DAMAC TOWER';
        update invList1[0];
        Inventory_User__c invUser=new Inventory_User__c();
        invUser.Inventory__c=invList[0].id;
        invUser.User__c=userInfo.getUserId();
        insert invUser;
        
        Inventory_User__c invUser1=new Inventory_User__c();
        invUser1.Inventory__c=invList1[0].id;
        invUser1.User__c=u.id;
        insert invUser1;
        
        DAMAC_Project_Avaliability con = new DAMAC_Project_Avaliability();
        
        con.getItems();
        con.getPropStatus();
        con.getCities();
        con.getUnitTypes();
        con.getPropertyNames();
        con.getAcd();
        con.getBedRooms();
        
        con.pageload();
        list<inventory_user__c> invo = new list<inventory_user__c>();
        invo.add(invuser1);
        con.otherUserinventory = invo;        
        con.filterdata();
        con.preparedata();
        
        
    }
}