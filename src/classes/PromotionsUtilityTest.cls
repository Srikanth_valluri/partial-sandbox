/* * * * * * * * * * * * * *
*  Class Name:   PromotionsUtilityTest
*  Purpose:      Unit test class for PromotionsUtility Controller
*  Author:       Hardik Mehta - ESPL
*  Company:      ESPL
*  Created Date: 11-Feb-2018
*  Updated Date: 11-Feb-2018
*  Type:         Test Class
* * * * * * * * * * * * */
@isTest
public with sharing class PromotionsUtilityTest 
{    
    /* * * * * * * * * * * * *
    *  Method Name:  getRecordTypeIdForPromotion
    *  Purpose:      This method is used to get record type ID for Promotion
    *  Author:       Hardik Mehta
    *  Company:      ESPL
    *  Created Date: 11-Feb-2018
    * * * * * * * * * * * * */
    private static Id getRecordTypeIdForPromotion()
    {
        Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id promotionsRecordTypeID = caseRecordTypes.get('Promotions').getRecordTypeId();
        return promotionsRecordTypeID;
    }

    /* * * * * * * * * * * * *
    *  Method Name:  submitRecordForApproval_test
    *  Purpose:      This method is used to unit test functionality submitRecordForApproval method
    *  Author:       Hardik Mehta
    *  Company:      ESPL
    *  Created Date: 11-Feb-2018
    * * * * * * * * * * * * */
    static testMethod void submitRecordForApproval_test()
    {
        
        
        UserRole objUserRole = [Select Id from UserRole where name = 'Collection - CRE'];

        Profile pf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];

        User usr1 = createTestUser(objUserRole.Id , pf.Id, 'Test FirstName1', 'Test LastName1',null);
        insert usr1;
        User usr2 = createTestUser(objUserRole.Id , pf.Id, 'Test FirstName', 'Test LastName',usr1);
        
        //insert usr2;
        //usr2 = [select Id,Manager_Role__c from User where id =: usr2.Id];

        //system.debug('usr2 '+usr2);
        //system.debug('usr2 manager '+usr2.Manager_Role__c);
        Case objCase = new Case();
        System.runAs(usr2)
        {    
            //insert sample data for Account
            Account objAccount = TestDataFactory_CRM.createPersonAccount();
            insert objAccount;

            Id promotionRecordTypeID = getRecordTypeIdForPromotion();

            objCase = TestDataFactory_CRM.createCase(objAccount.Id , promotionRecordTypeID);
            objCase.Status = 'Submitted';
            objCase.Approval_Status__c = 'Approved';
            objCase.Status = 'Submitted';
            objCase.Pending_Amount__c = 8000;
            objCase.Total_Amount__c = 8000;
            insert objCase;

            objCase = [Select Id,OwnerId from Case where id =: objCase.Id];
            
            Task objTask = new Task();
            objTask.OwnerId = objCase.OwnerId;
            objTask.WhatId = objCase.Id;
            objTask.Subject = 'Verify Case Details';
            objTask.ActivityDate = system.today();
            objTask.Assigned_User__c = 'Test';
            objTask.Process_Name__c = 'Promotions';
            objTask.Priority = 'High';
            objTask.Status = 'Completed';
            insert objTask;

            Test.startTest();
                objCase = [Select Id,OwnerId from Case where Id =: objCase.Id];
                PromotionsUtility.submitRecordForApproval(objCase);
                PromotionsUtility.errorLoggerNew('Test error', objCase.Id, '');
                PromotionsUtility.submitRecordForApprovalProtal(new list<Task>{ objTask } );
            Test.stopTest();
        }
    }

    /* * * * * * * * * * * * *
    *  Method Name:  createTestUser
    *  Purpose:      This method is used to create test user
    *  Author:       Hardik Mehta
    *  Company:      ESPL
    *  Created Date: 11-Feb-2018
    * * * * * * * * * * * * */
    public static User createTestUser(Id roleId , Id profID, String fName, String lName, User objUser)
    {
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
       
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        User tuser = new User(  firstname = fName,
                                lastName = lName,
                                email = uniqueName + '@test' + orgId + '.org',
                                Username = uniqueName + '@test' + orgId + '.org',
                                EmailEncodingKey = 'ISO-8859-1',
                                Alias = uniqueName.substring(18, 23),
                                TimeZoneSidKey = 'America/Los_Angeles',
                                LocaleSidKey = 'en_US',
                                LanguageLocaleKey = 'en_US',
                                ProfileId = profId,
                                UserRoleId = roleId
                             );
        if(objUser != null)
        {
            tuser.ManagerId = objUser.Id;
        }
        return tuser;
    }
}