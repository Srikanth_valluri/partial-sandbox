@isTest
private class PopulateCOCDLetterforDpInvoiceBatchTest{
	@isTest
	static void itShould(){
		List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

		//create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        insert bookingUnitList;
        
        DP_Invoices__c obj=new DP_Invoices__c();
	    obj.Accounts__c = objAccount.id;
		obj.BookingUnits__c = bookingUnitList[0].id;
        insert obj;

		Test.startTest();
			SOAPCalloutServiceMock.returnToMe = new Map<String,actionCom.generateCOCDResponse_element>();
			actionCom.generateCOCDResponse_element response_x = new actionCom.generateCOCDResponse_element();
			response_x.return_x = 'S';
			SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
			Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
			PopulateCOCDLetterforDpInvoiceBatch objClass = new PopulateCOCDLetterforDpInvoiceBatch();
			Database.Executebatch(objClass);
		Test.stopTest();
	}
}