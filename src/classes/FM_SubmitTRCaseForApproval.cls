public without sharing class FM_SubmitTRCaseForApproval {

    @InvocableMethod
    public static void sendTrCaseForApproval( list<Task> lstTasks ) {
        system.debug('==FM_SubmitTRCaseForApproval.sendTrCaseForApproval==');
        system.debug('==lstTasks=='+lstTasks);
        list<Id> lstCaseIds = new list<Id>();
        list<FM_Case__c> lstCaseToBeUpdated = new list<FM_Case__c>();
        for( Task objTask : lstTasks ) {
            if( isRelatedToFMCase( objTask ) ) {
                if (!Approval.isLocked(objTask.WhatId)) {
                    FM_Case__c objCase = new FM_Case__c();
                    objCase.Id = objTask.WhatId ;
                    objCase.Approval_Status__c = 'Pending';
                    objCase.Submit_for_Approval__c = true ;
                    lstCaseToBeUpdated.add( objCase );
                    lstCaseIds.add( objTask.WhatId );
                }
            }
        }
        system.debug('==lstCaseIds=='+lstCaseIds);
        if( !lstCaseIds.isEmpty() ) {
            update lstCaseToBeUpdated ;
            FM_ApprovalsHandler.submitCaseForApproval( lstCaseIds );
        }
    }


     //Check if the task is related to the FM Case object.
    private static boolean isRelatedToFMCase( Task objTask ) {
        if( String.isNotBlank( objTask.WhatId ) &&
            String.valueOf( objTask.WhatId ).startsWith( FM_Utility.getObjectKeyPrefix('FM_Case__c') ) ) {
            return true ;
        }
        else {
            return false ;
        }
    }
}