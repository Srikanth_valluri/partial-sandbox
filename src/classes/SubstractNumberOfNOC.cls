/**
 *==================================================================================================
 * Class Name      : SubstractNumberOfNOC
 * Test Class Name : SubstractNumberOfNOCTest
 * Description     :
 * Developer Name  : Pratiksha Narvekar
 * Created Date    : 28-12-2017
 *==================================================================================================
 */
public without sharing class SubstractNumberOfNOC {

	public void OnAfterUpdate(map<Id,Case> newMap, map<Id,Case> oldMap){
	    set<String> setBooingUnitIds = new set<String>();
	    set<String> setCaseIds = new set<String>();
	    
	    map<ID,Booking_Unit__c> mapBooingUnit = new map<ID,Booking_Unit__c>();
	    Id NOCcase = Schema.SObjectType.case.getRecordTypeInfosByName().get('NOC For Visa').getRecordTypeId();
		for(Case objC : newMap.values()){
		    System.debug('<<<<...objC...>>>>'+objC);
		    if(objC.AccountId != null && objC.RecordTypeId == NOCcase
            && objC.Status != oldMap.get(objC.Id).Status && objC.Status =='Cancelled'){
                setBooingUnitIds.add(objC.Booking_Unit__c);
                setCaseIds.add(objC.ID);
            }
		}   
		 system.debug('setBooingUnitIds=='+setBooingUnitIds);
		 system.debug('setCaseIds=='+setCaseIds);
        if(!setBooingUnitIds.isEmpty()){
            list<SR_Attachments__c> listSR = new list<SR_Attachments__c>();
            listSR = [SELECT ID,
                             case__r.Booking_Unit__c,
                             case__r.Booking_Unit__r.Number_Of_Noc__c
                      FROM  SR_Attachments__c 
                      WHERE case__r.Booking_Unit__c in : setBooingUnitIds 
                      AND Case__c in : setCaseIds
                      AND Type__c ='NOC For VISA'];
            System.debug('////listSR///'+listSR+'...listSR.size()...'+listSR.size());
            if(!listSR.isEmpty()){
                for(SR_Attachments__c objSR : listSR){
                    Booking_Unit__c objBU = new Booking_Unit__c();
                    objBU.ID = objSR.case__r.Booking_Unit__c;
                    objBU.Number_Of_Noc__c = (objSR.case__r.Booking_Unit__r.Number_Of_Noc__c !=null && objSR.case__r.Booking_Unit__r.Number_Of_Noc__c != 0 )? objSR.case__r.Booking_Unit__r.Number_Of_Noc__c - listSR.size(): objSR.case__r.Booking_Unit__r.Number_Of_Noc__c;
                    mapBooingUnit.put(objBU.ID,objBU);
                    System.debug('...objBU..'+objBU);
                }
            }
            System.debug('....mapBooingUnit....'+mapBooingUnit);
            database.update(mapBooingUnit.values());
        }
	}

}