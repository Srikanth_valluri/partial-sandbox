/* * * * * * * * * * * * * *
*  Class Name:   AOPTMQClassTest
*  Purpose:      Unit test class for AOPTMQClass
*  Author:       Hardik Mehta - ESPL
*  Company:      ESPL
*  Created Date: 21-Nov-2017
*  Type:         Test Class
* * * * * * * * * * * * */
@isTest
public class AOPTMQClassTest{
  /* * * * * * * * * * * * *
  *  Method Name:  testWSDL
  *  Purpose:      This method is used to check WSDL
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 21-Nov-2017
  * * * * * * * * * * * * */  
    public static testmethod void testWSDL(){
        AOPTMQClass.RegistrationDetails_element  objRegDetails = new AOPTMQClass.RegistrationDetails_element();
        AOPTMQClass.RegistrationDetailsResponse_element objResponse = new AOPTMQClass.RegistrationDetailsResponse_element();        
        AOPTMQClass.DocumentAttachment_element  objAttach = new AOPTMQClass.DocumentAttachment_element ();        
        AOPTMQClass.PaymentPlanCreationResponse_element objPayment = new AOPTMQClass.PaymentPlanCreationResponse_element ();
        AOPTMQClass.getMilestonePaymentDetails_element  objMileStone = new AOPTMQClass.getMilestonePaymentDetails_element ();
        AOPTMQClass.PaymentPlanCreation_element  objPlan = new AOPTMQClass.PaymentPlanCreation_element ();
        AOPTMQClass.PaymentPlanHistory_element  objHistory = new AOPTMQClass.PaymentPlanHistory_element ();
        AOPTMQClass.PaymentPlanReversalResponse_element  objPaymentRev = new AOPTMQClass.PaymentPlanReversalResponse_element ();
        AOPTMQClass.EarlyHandoverPaymentPlanCreation_element  objEarly = new AOPTMQClass.EarlyHandoverPaymentPlanCreation_element ();
        AOPTMQClass.DocumentAttachmentResponse_element  objDocAttach = new AOPTMQClass.DocumentAttachmentResponse_element ();
        AOPTMQClass.PaymentPlanHistoryResponse_element  objPaymentPlanHist = new AOPTMQClass.PaymentPlanHistoryResponse_element ();
        AOPTMQClass.getMasterMilestone_element  objMasterMile  = new AOPTMQClass.getMasterMilestone_element ();
        AOPTMQClass.getMasterMilestoneResponse_element  objMasterMileResponse = new AOPTMQClass.getMasterMilestoneResponse_element ();
        AOPTMQClass.PaymentPlanReversalCurrent_element  objPayReversalCurrent = new AOPTMQClass.PaymentPlanReversalCurrent_element ();
        AOPTMQClass.PaymentPlanReversal_element  objPlanRev = new AOPTMQClass.PaymentPlanReversal_element ();
        AOPTMQClass.getMilestonePaymentDetailsResponse_element  objPaymentDetail = new AOPTMQClass.getMilestonePaymentDetailsResponse_element  ();
        AOPTMQClass.EarlyHandoverPaymentPlanCreationResponse_element  objEarlyHandover = new AOPTMQClass.EarlyHandoverPaymentPlanCreationResponse_element ();
        AOPTMQClass.PaymentPlanReversalCurrentResponse_element  objCurrentResponse = new AOPTMQClass.PaymentPlanReversalCurrentResponse_element ();
        AOPTMQClass.AOPTHttpSoap11Endpoint  objSoap11 = new AOPTMQClass.AOPTHttpSoap11Endpoint ();
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT());        
        paymentPlanCreationXxdcAoptPkgWsP.APPSXXDC_AOPT_PKG_WSX1843128X6X5[] regTerms = new paymentPlanCreationXxdcAoptPkgWsP.APPSXXDC_AOPT_PKG_WSX1843128X6X5[5];        
        paymentPlanCreationXxdcAoptPkgWsP.APPSXXDC_AOPT_PKG_WSX1843128X6X5 objIPMSPT = new paymentPlanCreationXxdcAoptPkgWsP.APPSXXDC_AOPT_PKG_WSX1843128X6X5();        
          objIPMSPT.DESCRIPTION = 'ABCD';
          objIPMSPT.PAYMENT_DATE = '10/4/2017';
          objIPMSPT.PERCENT_VALUE = '88';
          objIPMSPT.PAYMENT_AMOUNT = '68';          
          regTerms[0] = objIPMSPT;        
        objSoap11.PaymentPlanCreation('123','567','ABC',regTerms);        
        objSoap11.EarlyHandoverPaymentPlanCreation('123','567','ABC','587','XYZ','Nothing','14/4/2017','15/9/2018','Event','61','Yes','500');       
        objSoap11.PaymentPlanReversal('123','589');        
        soapencodingTypesDatabindingAxis2Apa.Base64Binary b = new soapencodingTypesDatabindingAxis2Apa.Base64Binary();
        b.base64Binary = 'ABC';
        objSoap11.DocumentAttachment('123','XYZ','WXY','1234567','891','556','ABXC','Nocat','5897','ggopkj','NoDescript',b);
        objSoap11.getMasterMilestone('56987');
        objSoap11.PaymentPlanReversalCurrent('56987','25897');
        objSoap11.getMilestonePaymentDetails('256987');
        objSoap11.RegistrationDetails('87945','145697');
        objSoap11.PaymentPlanHistory('789644','268745');
    }
}