@isTest
public class CallBackRequestCallingHandlerTest{
    public static testmethod void testMethod1() {
    
        User userObj = new User(
                             ProfileId = [SELECT Id FROM Profile WHERE Name = 'Collection - CRE'].Id,
                             LastName = 'test',
                             Email = 'puser000@test.com',
                             Username = 'puser000@test.com' + System.currentTimeMillis(),
                             CompanyName = 'TEST',
                             Title = 'title',
                             isActive = true,
                             Alias = 'alias',
                             Extension = '7023301',
                             TimeZoneSidKey = 'Asia/Dubai',
                             EmailEncodingKey = 'UTF-8',
                             LanguageLocaleKey = 'en_US',
                             LocaleSidKey = 'en_IN'
        );
        insert userObj ;
        
        
        system.runAs(userObj){
            Id devRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Call Back Request').getRecordTypeId();

            Called_Number_List__c objCNL = new Called_Number_List__c();
            objCNL.Name = '5545566';
            insert objCNL; 
           
            List<TriggerOnOffCustomSetting__c> settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                             OnOffCheck__c = true);
              
            settingLst2.add(newSetting1);
            insert settingLst2;
            

            Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
            Account objAcc = new Account(RecordTypeId = personAccRTId, 
                       FirstName='Test FirstName1', 
                       LastName='Test LastName2', 
                       Type='Person', 
                       party_ID__C='12345'
                       );
            insert objAcc ;
            
            Call_Log__c objCall_Log = new Call_Log__c();
            objCall_Log.Calling_Number__c = '123456789';
            objCall_Log.CRE__c = userObj.Id;
            objCall_Log.Account__c = objAcc.Id;
            insert objCall_Log;
            
            Datetime dayBeforeYesterday = Datetime.now().addDays(-2);
            Test.setCreatedDate(objCall_Log.Id, dayBeforeYesterday);
            
            List<Calling_List__c>updateCallingList = new List<Calling_List__c>();
            Calling_List__c callbackCallInst = new Calling_List__c(RecordTypeId = devRecordTypeId,
                                                Customer_Flag__c = true,
                                                Registration_ID__c = '1000',
                                                Party_ID__c = '1177923',
                                                Called_Number__c = '5545566',
                                                Calling_Number__c = '123456789');


            Test.startTest();
                insert callbackCallInst;
            Test.stopTest();
        }
    }
    
    public static testmethod void testMethod2() {
    
        User userObj = new User(
                             ProfileId = [SELECT Id FROM Profile WHERE Name = 'Collection - CRE'].Id,
                             LastName = 'test',
                             Email = 'puser000@test.com',
                             Username = 'puser000@test.com' + System.currentTimeMillis(),
                             CompanyName = 'TEST',
                             Title = 'title',
                             isActive = true,
                             Alias = 'alias',
                             Extension = '7002',
                             TimeZoneSidKey = 'Asia/Dubai',
                             EmailEncodingKey = 'UTF-8',
                             LanguageLocaleKey = 'en_US',
                             LocaleSidKey = 'en_IN'
        );
        insert userObj ;
        
        
        system.runAs(userObj){
            Id devRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Call Back Request').getRecordTypeId();

            List<TriggerOnOffCustomSetting__c> settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                             OnOffCheck__c = true);
              
            settingLst2.add(newSetting1);
            insert settingLst2;
            

            Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
            Account objAcc = new Account(RecordTypeId = personAccRTId, 
                       FirstName='Test FirstName1', 
                       LastName='Test LastName2', 
                       Type='Person', 
                       party_ID__C='12345'
                       );
            insert objAcc ;
            
            Call_Log__c objCall_Log = new Call_Log__c();
            objCall_Log.Calling_Number__c = '123456789';
            objCall_Log.CRE__c = userObj.Id;
            objCall_Log.Account__c = objAcc.Id;
            insert objCall_Log;
            
            Datetime dayBeforeYesterday = Datetime.now().addDays(-2);
            Test.setCreatedDate(objCall_Log.Id, dayBeforeYesterday);
            
            List<Calling_List__c>updateCallingList = new List<Calling_List__c>();
            Calling_List__c callbackCallInst = new Calling_List__c(RecordTypeId = devRecordTypeId,
                                                Customer_Flag__c = true,
                                                Registration_ID__c = '1000',
                                                Party_ID__c = '1177923',
                                                Called_Number__c = '5545566',
                                                Calling_Number__c = '123456789');


            Test.startTest();
                insert callbackCallInst;
            Test.stopTest();
        }
    }
    
    public static testmethod void testMethod3() {
    
        User userObj = new User(
                             ProfileId = [SELECT Id FROM Profile WHERE Name = 'Collection - CRE'].Id,
                             LastName = 'test',
                             Email = 'puser000@test.com',
                             Username = 'puser000@test.com' + System.currentTimeMillis(),
                             CompanyName = 'TEST',
                             Title = 'title',
                             isActive = true,
                             Alias = 'alias',
                             Extension = '7003',
                             TimeZoneSidKey = 'Asia/Dubai',
                             EmailEncodingKey = 'UTF-8',
                             LanguageLocaleKey = 'en_US',
                             LocaleSidKey = 'en_IN'
        );
        insert userObj ;
        
        
        system.runAs(userObj){
            Id devRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Call Back Request').getRecordTypeId();

            List<TriggerOnOffCustomSetting__c> settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                             OnOffCheck__c = true);
              
            settingLst2.add(newSetting1);
            insert settingLst2;
            
            Called_Number_List__c objCNL = new Called_Number_List__c();
            objCNL.Name = '5545566';
            insert objCNL;
            
            Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
            Account objAcc = new Account(RecordTypeId = personAccRTId, 
                       FirstName='Test FirstName1', 
                       LastName='Test LastName2', 
                       Type='Person', 
                       party_ID__C='12345'
                       );
            insert objAcc ;
            
            Call_Log__c objCall_Log = new Call_Log__c();
            objCall_Log.Calling_Number__c = '123456789';
            objCall_Log.CRE__c = userObj.Id;
            objCall_Log.Account__c = objAcc.Id;
            insert objCall_Log;
            
            Datetime dayBeforeYesterday = Datetime.now().addDays(-2);
            Test.setCreatedDate(objCall_Log.Id, dayBeforeYesterday);
            
            List<Calling_List__c>updateCallingList = new List<Calling_List__c>();
            Calling_List__c callbackCallInst = new Calling_List__c(RecordTypeId = devRecordTypeId,
                                                Customer_Flag__c = true,
                                                Registration_ID__c = '1000',
                                                Party_ID__c = '1177923');


            Test.startTest();
                insert callbackCallInst;
            Test.stopTest();
        }
    }
}