/*-------------------------------------------------------------------------------------------------
Description: Scheduler to schedule batch FMAGAMeetingBatch everyday evening 05:00 PM

============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 03-01-2019       | Lochana Rajput   | 1. Added scheduler logic
=============================================================================================================================
*/
public with sharing class FMAGAMeetingBatchScheduler implements Schedulable{
	public static String cronExp = '0 0 17 ? * * *';

	public static String jobName = 'FMAGAMeetingBatch';
	public void scheduleMyJob() {
		FMAGAMeetingBatchScheduler batchObj = new FMAGAMeetingBatchScheduler();
		System.schedule(jobName, cronExp, batchObj);
	}
	public void execute(SchedulableContext sc) {
		FMAGAMeetingBatch batchObj = new FMAGAMeetingBatch();
		Database.executeBatch(batchObj, 200);
	}
}