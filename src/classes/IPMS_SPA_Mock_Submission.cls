@isTest
global class IPMS_SPA_Mock_Submission implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals('http://94.200.40.200:8080/IPMSREST/process/3290', req.getEndpoint());
        //System.assertEquals('POST', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('http://94.200.40.200:8080/IPMSREST/process/3290');
        res.setStatusCode(201);
        return res;
    }
}