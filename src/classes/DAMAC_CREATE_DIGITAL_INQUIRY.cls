@RestResource(urlMapping='/createdigitalinquiry/*')
global with sharing class DAMAC_CREATE_DIGITAL_INQUIRY{
@HttpPut
    global static void createInquiry() {
        
        RestRequest request = RestContext.request;
        RestResponse res = RestContext.response;
        System.debug('Digital IS SENDING=='+request.requestbody.tostring());
        // Deserialize the JSON string into name-value pairs
        
        Map<String, Object> inqReq = (Map<String, Object>)JSON.deserializeUntyped(request.requestbody.tostring());
        Map<String,String> inqReqMap = new Map<String,String>();
        for(String fieldName : inqReq.keySet()) {
            inqReqMap.put(fieldName, String.valueof(inqReq.get(fieldName)));
        }
        try{
            
            // Check for PC Assignment on the Campaign
            boolean isPCAssignment = false;
            String campaignObjectPrefix = Campaign__c.sobjecttype.getDescribe().getKeyPrefix();
            String campaignid = inqReqMap.get('campaignid');
            if(String.isNotBlank(campaignid) && campaignid.startsWith(campaignObjectPrefix)){
                List<Campaign__c> sCampDetails = [SELECT Id,Name,PCAssignment__c
                                                    from Campaign__c 
                                                    where id=:campaignid limit 1];
                if (sCampDetails.size()>0){
                    isPCAssignment = sCampDetails[0].PCAssignment__c;
                }            
            }
            
            sobject newDigitalInquiry = new Digital_inquiry__c();
            // Query Digital Mapping Metadata
            list<Digital_Inquiry_Mapping__mdt> DigitalMappingMetadata = new list<Digital_Inquiry_Mapping__mdt>();        
            DigitalMappingMetadata = [select id,DeveloperName,LQS_Attribute__c,Salesforce_Field_API__c from Digital_Inquiry_Mapping__mdt where Activate_Mapping__c=true];
            system.debug('Mapping size=='+DigitalMappingMetadata.size());
            for(Digital_Inquiry_Mapping__mdt mapping:DigitalMappingMetadata ){
                newDigitalInquiry.put(mapping.Salesforce_Field_API__c,inqReqMap.get(mapping.LQS_Attribute__c));
                if(isPCAssignment){
                    string inquiryrtypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(DAMAC_Constants.INQUIRY_RT).getRecordTypeId();                
                    newDigitalInquiry.put('Record_Type_Id__c',inquiryrtypeId);                
                }else{
                    Id recTypeId = System.Label.PreInquiryRId;
                    newDigitalInquiry.put('Record_Type_Id__c',recTypeId);
                }
            }
            Insert newDigitalInquiry;       
            res.addHeader('Content-Type', 'application/json');
            res.responseBody = Blob.valueOf('{ "status" : "SUCCESS", "Success" : "true"}'); 
            if (Test.isRunningTest ()) {
                List<Campaign__c> sCampDetails = new List <Campaign__c> ();
                System.Debug (sCampDetails[0]);
            }
       }
       catch(Exception ex){
            system.debug('Exception at line number = '+ex.getLineNumber()+', Exception message = '+ex.getmessage());
            String erMsg =ex.getmessage();
            res.addHeader('Content-Type', 'application/json');            
            String retString = '{ "status" : "ERROR", "Success" : "False","Message":'+'"'+ex.getmessage()+'"'+'}';
            res.responseBody = Blob.valueOf(retString);
            // Create Salesforce Log
            Log__c objLog = new Log__c();
            objLog.Description__c = 'Inquiry Details: Firstname = '+inqReqMap.get('firstname')+'; Lastname = '+inqReqMap.get('lastname')+'; Email = '+inqReqMap.get('email')+
                                    '; Phone = '+inqReqMap.get('telephone')+inqReqMap.get('mobilecode')+'; Campaign Id = '+inqReqMap.get('campaignid')+
                                    '; Error at line number = '+ex.getLineNumber()+'; Error message = '+ex.getmessage();
            objLog.Type__c = 'Error while creating inquiry: Service = JSONRestInquiry';
            insert objLog;
        }    
       
    } 
    
  }