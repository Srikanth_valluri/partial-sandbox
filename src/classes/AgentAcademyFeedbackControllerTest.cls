@isTest
public class AgentAcademyFeedbackControllerTest{
    testMethod static void agentFeedback(){
        Training__c trainer = new Training__c ();
        trainer.subject__c = null;
        trainer.Name__c = 'Test';
        trainer.From_Time_Slot__c = '10:00 AM';
        trainer.To_Time_Slot__c = '10:30 AM';
        insert trainer ;
        
        Test.startTest();
            AgentAcademyFeedbackController feedback = new AgentAcademyFeedbackController();
            feedback.submitFeedback();
        Test.stopTest();
    }
}