/****************************************************************************************************
* Name                  : AnnouncementTriggerHandler                                                *
* Description           : This class will Notify all Agency Contacts regarding the Announcement     *
* Created By            : NSI - Sivasankar K                                                        *
* Created Date          : 05/Feb/2017                                                               *
* Last Modified Date    :                                                                           *
* Last Modified By      :                                                                           *
* --------------------------------------------------------------------------------------------------*
* Change    VER     AUTHOR              DATE            Description                                 *
* CH00      1.0     NSI - Sivasankar K  05/02/2017      Initial developmen                          *
* CH01      2.0                                                                                     *
* CH02      3.0                                                                                     *
* CH03      4.0     Craig Lobo          26/03/2018      Update the User Query String                *
                                                        for the Anouncements                        *
* CH04      5.0     Craig Lobo          16/04/2018      Update the User Query to Contact Query      *
                                                        for the Anouncements and also added the new *
                                                        field Description_2__c in the email body    *
****************************************************************************************************/

global with sharing class NotifyAnnouncementToAgencies {

    
    /************************************************************************************************
    * @Description : this method takes the parameter as Map and this will call from AgencyPCTrigger *
    * @Params      : Set<ID>                                                                  *
    * @Return      : void                                                                           *
    *************************************************************************************************/
    WebService Static String notifyAgencyAdminswithanAnnouncement(String newAnnouncementID){
        
        List<string> lstprofilenames = new List<string>();
        for(Announcement_Emails_for_Profiles__c annEmail : Announcement_Emails_for_Profiles__c.getall().values()){
            if(annEmail.Is_Subcribed__c && annEmail.Profile_Name__c != null)
                lstprofilenames.add(annEmail.Profile_Name__c);
        }
        
        Announcement__c Announcement = [SELECT Active__c, Are_Agencies_Notified__c, 
                                               Description_2__c, Description__c, Title__c, 
                                               Start_Date__c, End_Date__c, Header__c, Footer__c, 
                                               Agency_Type__c, Agency_Tier__c 
                                          FROM Announcement__c 
                                         WHERE ID = :newAnnouncementID
        ];

        List<Messaging.SingleEmailMessage> singleEmails = new List<Messaging.SingleEmailMessage>();
        Map<String,Set<ID>> mapOfagencyAdminIDs = new  Map<String,Set<ID>>();
        Set<ID> agencyAdminIDs;
        List<ID> lstAgencyAdminIDs;
        String keyVal = '';

        try {
            List<EmailTemplate> emailTemp = new List<EmailTemplate>([ SELECT Id ,Name, Subject, Body 
                                                                        FROM EmailTemplate 
                                                                       WHERE DeveloperName = 'Announcement_Emails'
            ]);

            if(!Announcement.Active__c){
                return 'Announcement must be active, before notifying agencies.';
            }  else if(Announcement.Agency_Type__c == null && Announcement.Agency_Tier__c == null){
                return 'Please select Agency Type or Agency Tier.';
            } else if(!Announcement.Are_Agencies_Notified__c){

                /*String queryString  = ' SELECT ID, ContactId, Contact.Status__c, '
                                    + ' Contact.Account.Recordtype.Name, '
                                    + ' Contact.Account.Agency_Tier__c FROM USER '
                                    + ' WHERE Profile.Name IN: lstprofilenames '
                                    + ' AND IsActive = True '
                                    + ' AND Contact.Status__c != \'Cancelled\' ';
                */
                String queryString  = ' SELECT ID, Status__c, Email,'
                                    + ' Account.Recordtype.Name, '
                                    + ' Account.Agency_Tier__c FROM Contact '
                                    + ' WHERE Status__c != \'Cancelled\' '
                                    + ' AND Email != \'\' ';

                if(Announcement.Agency_Type__c != null && Announcement.Agency_Tier__c != null){
                    // CH03 26-03-2018 Craig > Contact.Account.Agency_Tier__c to null, '', All Tier
                    queryString += ((Announcement.Agency_Type__c.equalsIgnoreCase('All') 
                                        && Announcement.Agency_Tier__c.equalsIgnoreCase('All')) 
                                    ? '' 
                                    : (Announcement.Agency_Type__c.equalsIgnoreCase('All') 
                                        ? '' 
                                        : 'AND Account.Agency_Type__c =\''
                                        + Announcement.Agency_Type__c
                                        + '\''
                                    )
                                    + ''
                                    + (Announcement.Agency_Tier__c.equalsIgnoreCase('All Tier') 
                                        ? ''
                                        :' AND ( Account.Agency_Tier__c = \''
                                        + Announcement.Agency_Tier__c
                                        + '\' OR Account.Agency_Tier__c = null '
                                        + ' OR Account.Agency_Tier__c = \'\' '
                                        + ' OR Account.Agency_Tier__c = \'All Tier\' )' 
                                    ) 
                                );
                }
                else{
                    queryString += (String.isNotBlank(Announcement.Agency_Type__c) ? (Announcement.Agency_Type__c.equalsIgnoreCase('All') ? '' : ' AND Account.Agency_Type__c =\''+Announcement.Agency_Type__c+'\'') : '');
                    queryString += (String.isNotBlank(Announcement.Agency_Tier__c) ? (Announcement.Agency_Tier__c.equalsIgnoreCase('All Tier') ? '' : ' AND Account.Agency_Tier__c =\''+Announcement.Agency_Tier__c+'\'') : '');
                }
                System.debug('queryString = '+queryString);
                System.debug('Announcement = '+Announcement );

                OrgWideEmailAddress[] owdEmail = [SELECT Id 
                                                    FROM OrgWideEmailAddress 
                                                   WHERE DisplayName = :Label.Announcement_OWD_Email_Display_Name
                ];

                Set<String> emailIdSet = new Set<String>();
                //for(User us : (List<User>)Database.query(queryString)){
                for(Contact contactObj : (List<Contact>)Database.query(queryString)){
                    System.debug('contactObj.Email = '+contactObj.Email);
                    if (!emailIdSet.contains(contactObj.Email)) {
                        emailIdSet.add(contactObj.Email);
                        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                        message.setSubject(Announcement.Title__c);
                        message.setTargetObjectId(contactObj.id);
                        if ( owdEmail.size() > 0 ) { 
                            message.setOrgWideEmailAddressId(owdEmail.get(0).Id);
                        }
                        if(String.isNotBlank(Label.Announcement_Bcc_Email)) {
                            message.setBccAddresses(new List<String>{Label.Announcement_Bcc_Email});
                        }
                        if (String.isNotBlank(Announcement.Description__c)) {
                            message.setPlainTextBody(Announcement.Description__c);
                        }
                        if (String.isNotBlank(Announcement.Description_2__c)) {
                            message.setHtmlBody(Announcement.Description_2__c);
                        }
                        message.setSaveAsActivity(false);
                        singleEmails.add(message);
                    }
                }

                System.debug('singleEmails = '+singleEmails);
                if(!singleEmails.isEmpty() && singleEmails.size() > 0){
                    Messaging.SendEmailResult[] results = Messaging.sendEmail(singleEmails);
                    if (results[0].success) {
                        System.debug('results = '+results);
                        Announcement.Are_Agencies_Notified__c = true;
                        update Announcement;
                        return 'Email has been sent to Agents successfully.';
                    } else {
                        System.debug('The email failed to send: '
                              + results[0].errors[0].message);
                        return 'The email failed to send:'+ results[0].errors[0].message;
                    }
                } 
                return 'No Agency is notified.';
            } else {
                return 'Agents are already notified.';
            }
        } catch(Exception ex){
            return ex.getMessage();
        }
    }
}