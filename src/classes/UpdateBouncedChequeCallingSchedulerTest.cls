/*-------------------------------------------------------------------------------------------------
Description: Test class for UpdateBouncedChequeCallingListScheduler

    ============================================================================================================================
        Version | Date(DD-MM-YYYY) | Last Modified By | Comments
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 11-12-2017       | Lochana Rajput   | 1. Added test method to test scheduler for batch 'UpdateBouncedChequeCallingListBatch'
   =============================================================================================================================
*/
@isTest
private class UpdateBouncedChequeCallingSchedulerTest {

    @isTest static void test_scheduler() {
        Test.startTest();
        //String jobName = 'testJob' + System.now();
        //new UpdateBouncedChequeCallingListScheduler(jobName);
        UpdateBouncedChequeCallingListScheduler objScheduler = new UpdateBouncedChequeCallingListScheduler();
        System.schedule('Test Job', '0 0 0 * * ? *', objScheduler);
        Test.stopTest();
        //CronJobDetail ctd = [SELECT Id, Name, JobType FROM CronJobDetail WHERE Name =: jobName LIMIT 1];
        //CronTrigger ctObj = [SELECT Id, NextFireTime FROM CronTrigger WHERE CronJobDetailId =: ctd.ID];
        //System.debug('==ctObj==' + ctObj);
        //System.assert(ctd != NULL);
    }
}