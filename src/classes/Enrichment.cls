/***********************************************************************************************************
Description: Class to find Accounts related to lst of Email received in request and create
            customer Enrichment and Customer Profiling records.
-----------------------------------------------------------------------------------------------------------*
Version     Date                Author              Description                                 
1.0         27 Jul 2020         Aditya Kishore      Created class and method
2.0         27 Jul 2020         Ruchika Choudhary   Initial Draft
************************************************************************************************************/
@RestResource(urlMapping='/Enrichment/*')

global with sharing class Enrichment{


    /********************************************************************************************************
     Method Description :  Method to find Accounts related to lst of Email received in request and create
            customer Enrichment and Customer Profiling records.
    * Input Parameters : lstEmail - List of Emails in request
                        lstMob - List of Mobile numbers in request.
                        lstAddress- List of Address in request.
                        lstLinkedIn- List of LinkedIn Profiles.
                        lstFacebook- List of Facebook Ids.
                        lstInstagram - List of Instagram Ids.
                        lstTwitter -List of Twitter Profiles.
                        lstCity - List of cities Customer lived in.
                        age - Age of Customer.
                        lstCompany - List of companies.
                        lstDegree - List of Degree
                        lstCollege - List of College names.
                        lstState - List of State names.
                        lstStartDate - List of Start Date
                        lstDesignation - List of Designations.
                        Gender - Gender of Customer.
                        ExternalId - Account Id
    * Return Type : ResponseWrapper - Response with either Success or Error status.
    ********************************************************************************************************/
    @HttpPost
    global static ResponseWrapper createEnrichment( List<String> lstEmail, List<String> lstMob, List<String> lstCcodes,
                                   List<String> lstAddress, List<String> lstLinkedIn,
                                   List<String> lstFacebook, List<String> lstInstagram, List<String> lstTwitter,
                                   List<String> lstCity, Integer age, List<String> lstCompany, List<String> lstDegree,
                                   List<String> lstCollege, List<String> lstState , List<String> lstPOBox,
                                   List<Date> lstStartDate, List<String> lstDesignation, String Gender,
                                   String ExternalId, List<String> lstCountry
                                 ) {

        ResponseWrapper objResponseWrapper = new ResponseWrapper();
        List<Customer_Enrichment__c> custEnrichmentList = new List<Customer_Enrichment__c>();
        List<Customer_Enrichment__c> lstCustomerEnrichment = new List<Customer_Enrichment__c>();
        if (!String.isBlank(ExternalId)) {
            custEnrichmentList =  [
                            SELECT Id,
                                Account__c 
                            FROM Customer_Enrichment__c 
                            WHERE Account__c = :ExternalId
                ];
            if (custEnrichmentList != null && custEnrichmentList.size() != 0 ) {
                for (Customer_Enrichment__c custEnrichment : custEnrichmentList ) {
                    if (!String.isBlank(Gender)) {
                        custEnrichment.Gender__c = Gender;
                    }
                    if (age != null) {
                        custEnrichment.Age__c = age;
                    }
                    for (Integer i = 1; i <= 5 ; i++) {
                        custEnrichment = putCEFields(i,custEnrichment,lstEmail,lstMob, lstAddress, lstLinkedIn,
                                lstInstagram, lstFacebook, lstTwitter, lstState, lstCity, lstPOBox, lstDegree,
                                lstCollege, lstCompany, lstStartDate, lstDesignation, lstCountry
                        );
                    }
                    lstCustomerEnrichment.add(custEnrichment);
                }
            } else {
                Customer_Enrichment__c custEnrichment = new Customer_Enrichment__c();
                custEnrichment.Account__c = ExternalId;
                if (!String.isBlank(Gender)) {
                    custEnrichment.Gender__c = Gender;
                }
                if (age != null) {
                    custEnrichment.Age__c = age;
                }
                for (Integer i = 1; i <= 5 ; i++) {
                    custEnrichment = putCEFields(i,custEnrichment,lstEmail,lstMob, lstAddress, lstLinkedIn,
                            lstInstagram, lstFacebook, lstTwitter, lstState, lstCity, lstPOBox, lstDegree,
                            lstCollege, lstCompany, lstStartDate, lstDesignation, lstCountry
                    );
                }
                lstCustomerEnrichment.add(custEnrichment);
            }
            List<String> lstCEIds = new List<String>();
            try {
                if (lstCustomerEnrichment.size() > 0) {
                    upsert lstCustomerEnrichment;
                }
                List<Customer_Profiling__c> lstCustProfile = new List<Customer_Profiling__c>();
                for (Customer_Enrichment__c  custEnrichment : lstCustomerEnrichment ) {
                    lstCEIds.add(custEnrichment.Id); 
                    Customer_Profiling__c custProfile = new Customer_Profiling__c();
                    custProfile.Enrichment__c = custEnrichment.Id;
                    custProfile.CUSTOMER__c = custEnrichment.Account__c;
                    lstCustProfile.add(custProfile);
                    
                }
                objResponseWrapper.status = 'Details Created Successfully !';
                objResponseWrapper.statusCode = '200';
                objResponseWrapper.errorMessage = ' ';
                objResponseWrapper.lstCustomerEnrichmentId = lstCEIds;
            } catch (Exception e) {
                objResponseWrapper.status = 'Error';
                objResponseWrapper.statusCode = '400';
                objResponseWrapper.errorMessage = 'Insert failed. ';
                objResponseWrapper.lstCustomerEnrichmentId = lstCEIds;
            }
        } else {
            objResponseWrapper.status = 'Error';
            objResponseWrapper.statusCode = '400';
            objResponseWrapper.errorMessage = 'External Id not provided.';
            objResponseWrapper.lstCustomerEnrichmentId = new List<String> ();
        }
            
        return objResponseWrapper;     
    } 

    /*********************************************************************************************************
    * Method Description : Method to put values in field dynamically.
    * Input Parameters :i - Counter 
                        custEnrichment - Customer_Enrichment__c instance
                        lstEmail - List of Emails in request
                        lstMob - List of Mobile numbers in request.
                        lstAddress- List of Address in request.
                        lstLinkedIn- List of LinkedIn Profiles.
                        lstFacebook- List of Facebook Ids.
                        lstInstagram - List of Instagram Ids.
                        lstTwitter -List of Twitter Profiles.
                        lstCity - List of cities Customer lived in.
                        age - Age of Customer.
                        lstCompany - List of companies.
                        lstDegree - List of Degree
                        lstCollege - List of College names.
                        lstState - List of State names.
                        lstStartDate - List of Start Date
                        lstDesignation - List of Designations.
                        Gender - Gender of Customer.
                        ExternalId - Account Id
    * Return Type : custEnrichment - Customer_Enrichment__c instance with values
    **********************************************************************************************************/
    public static Customer_Enrichment__c putCEFields (
            Integer i, Customer_Enrichment__c custEnrichment, List<String> lstEmail, List<String> lstMob,
            List<String> lstAddress, List<String> lstLinkedIn, List<String> lstInstagram,
            List<String> lstFacebook, List<String> lstTwitter, List<String> lstState,List<String>  lstCity,
            List<String> lstPOBox,List<String> lstDegree,List<String> lstCollege,List<String> lstCompany,
            List<Date> lstStartDate, List<String> lstDesignation , List<String> lstCountry
        ) {
        String fieldName;
        //Integer j = i+1;
        fieldName = 'Email_' + i  +'__c';
        if (lstEmail != Null && lstEmail.size() >= i) {
            custEnrichment.put(fieldname, lstEmail[i-1]);
        }
        fieldName = 'Phone_' + i  +'__c';
        if (lstMob != Null && lstMob.size() >= i) {
            custEnrichment.put(fieldname, lstMob[i-1]);
        }
        fieldName = 'Address_Line_' + i  +'__c';
        if (lstAddress != Null && lstAddress.size() >= i) {
            custEnrichment.put(fieldname, lstAddress[i-1]);
        }
        fieldName = 'LinkedIn_' + i  +'__c';
        if (lstLinkedIn != Null && lstLinkedIn.size() >= i) {
            custEnrichment.put(fieldname, lstLinkedIn[i-1]);
        }
        fieldName = 'Instagram_' + i  +'__c';
        if (lstInstagram != Null && lstInstagram.size() >= i) {
            custEnrichment.put(fieldname, lstInstagram[i-1]);
        }
        fieldName = 'Facebook_' + i  +'__c';
        if (lstFacebook != Null && lstFacebook.size() >= i) {
            custEnrichment.put(fieldname, lstFacebook[i-1]);
        }
        fieldName = 'Twitter_' + i  +'__c';
        if (lstTwitter != Null && lstTwitter.size() >= i) {
            custEnrichment.put(fieldname, lstTwitter[i-1]);
        }
        fieldName = 'State_' + i  +'__c';
        if (lstState != Null && lstState.size() >= i) {
            custEnrichment.put(fieldname, lstState[i-1]);
        }
        fieldName = 'City_' +i  +'__c';
        if (lstCity != Null && lstCity.size() >= i) {
            custEnrichment.put(fieldname, lstCity[i-1]);
        }
        fieldName = 'PO_Box_' + i  +'__c';
        if (lstPOBox != Null && lstPOBox.size() >= i) {
            custEnrichment.put(fieldname, lstPOBox[i-1]);
        }
        fieldName = 'Degree_' + i  +'__c';
        if (lstDegree != Null && lstDegree.size() >= i) {
            custEnrichment.put(fieldname, lstDegree[i-1]);
        }
        fieldName = 'College_' + i  +'__c';
        if (lstCollege != Null && lstCollege.size() >= i) {
            custEnrichment.put(fieldname, lstCollege[i-1]);
        }
        fieldName = 'Company_' + i  +'__c';
        if (lstCompany != Null && lstCompany.size() >= i) {
            custEnrichment.put(fieldname, lstCompany[i-1]);
        }
        fieldName = 'Start_Date_' + i  +'__c';
        if (lstStartDate != Null && lstStartDate.size() >= i) {
            custEnrichment.put(fieldname, lstStartDate[i-1]);
        }
        fieldName = 'Designation_' + i  +'__c';
        if (lstDesignation != Null && lstDesignation.size() >= i) {
            custEnrichment.put(fieldname, lstDesignation[i-1]);
        }
        fieldName = 'Country' + i  +'__c';
        if (lstCountry!= Null && lstCountry.size() >= i) {
            custEnrichment.put(fieldname, lstCountry[i-1]);
        }

        return custEnrichment;
    } 


    //for sending response
    global class ResponseWrapper {
        global String status;
        global String statusCode;
        global String errorMessage;
        global List<String> lstCustomerEnrichmentId;

        global ResponseWrapper() {
            this.status = '';
            this.statusCode = '';
            this.errorMessage = '';
        }
    }
}