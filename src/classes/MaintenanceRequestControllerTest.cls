@isTest
private class MaintenanceRequestControllerTest {

    @isTest static void test_functionality() {
        Account objAcc = new Account();
        objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;

        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAcc);
        insert objSR;
        Booking__c objBooking = TestDataFactoryFM.createBooking(objAcc, objSR);
        insert objBooking;
        Booking_unit__c objBU = TestDataFactoryFM.createBookingUnit(objAcc, objBooking);
        insert objBU;
        CAFM_Contract__c objContract = new CAFM_Contract__c(Name='Test Contract', Contract_Id__c = '611');
        insert objContract;
        Id caseRTId = Schema.SObjectType.CAFM_Building__c.getRecordTypeInfosByName().get('Common area').getRecordTypeId();
        CAFM_Building__c objCAFMBuilding = new CAFM_Building__c();
        objCAFMBuilding.Building_Code__c = 'JNU';
        objCAFMBuilding.RecordTypeId = caseRTId;
        objCAFMBuilding.CAFM_Contract__c = objContract.Id;
        objCAFMBuilding.Common_area_code__c = '45';
        // objCAFMBuilding.IPMS_Building__c = objLoc.Id;
        insert objCAFMBuilding;
        Location__c objLoc = new Location__c();
        objLoc = TestDataFactoryFM.createLocation();
        objLoc.CAFM_Common_area__c = objCAFMBuilding.Id;
        insert objLoc;
        CAFM_Location__c objCAFMLocation = new CAFM_Location__c();
        objCAFMLocation.Description__c = 'DFA/1/A102';
        objCAFMLocation.Location_Id__c = '6543';
        objCAFMLocation.CAFM_Building__c = objCAFMBuilding.Id;
        objCAFMLocation.Building_Sequence_no__c = '45';
        insert objCAFMLocation;

        Test.startTest();
        PageReference pageRef = Page.MaintenanceRequestPage;
        pageRef.getParameters().put('UnitId', String.valueOf(objBU.Id));
        pageRef.getParameters().put('AccountId', String.valueOf(objAcc.Id));
        pageRef.getParameters().put('SRType', 'Helpdesk');
        Test.setCurrentPage(pageRef);
        //Test.setMock( WebServiceMock.class, new MaintenanceRequestMock() );
        Test.setMock( WebServiceMock.class, new MaintenanceRequestTaskMock() );
        MaintenanceRequestController controller = new MaintenanceRequestController();
        // controller.objFMCase.Helpdesk_Request_Area__c='Common area';
        controller.selectedOption='Common area';
        controller.selectedLocation=String.valueOf(objCAFMLocation.id);
        controller.getCAFMLocations();
        //controller.getLocationDetails();
        //controller.saveAsDraft();
        controller.submit();
        Test.stopTest();
    }

    @isTest static void test_method_two() {
        Account objAcc = new Account();
        objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        Location__c objLoc = new Location__c();
        objLoc = TestDataFactoryFM.createLocation();
        insert objLoc;
        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAcc);
        insert objSR;
        Booking__c objBooking = TestDataFactoryFM.createBooking(objAcc, objSR);
        insert objBooking;
        Booking_unit__c objBU = TestDataFactoryFM.createBookingUnit(objAcc, objBooking);
        insert objBU;
        CAFM_Contract__c objContract = new CAFM_Contract__c(Name='Test Contract', Contract_Id__c = '611');
        insert objContract;
        Id caseRTId = Schema.SObjectType.CAFM_Building__c.getRecordTypeInfosByName().get('Common area').getRecordTypeId();
        CAFM_Building__c objCAFMBuilding = new CAFM_Building__c();
        objCAFMBuilding.Building_Code__c = 'JNU';
        objCAFMBuilding.RecordTypeId = caseRTId;
        objCAFMBuilding.CAFM_Contract__c = objContract.Id;
        objCAFMBuilding.Common_area_code__c = '45';
        objCAFMBuilding.IPMS_Building__c = objLoc.Id;
        insert objCAFMBuilding;
        CAFM_Location__c objCAFMLocation = new CAFM_Location__c();
        objCAFMLocation.Description__c = 'DFA/1/A102';
        objCAFMLocation.Location_Id__c = '6543';
        objCAFMLocation.CAFM_Building__c = objCAFMBuilding.Id;
        objCAFMLocation.Building_Sequence_no__c = '45';
        insert objCAFMLocation;

        Test.startTest();
        PageReference pageRef = Page.MaintenanceRequestPage;
        pageRef.getParameters().put('UnitId', String.valueOf(objBU.Id));
        pageRef.getParameters().put('AccountId', String.valueOf(objAcc.Id));
        pageRef.getParameters().put('SRType', 'Helpdesk');
        Test.setCurrentPage(pageRef);
        Test.setMock( WebServiceMock.class, new MaintenanceRequestMock() );
        MaintenanceRequestController controller = new MaintenanceRequestController();
        MaintenanceRequestController.getSessionIDfromCAFM();

        Test.stopTest();
    }

    @isTest static void test_getTaskDetails() {
        FM_Case__c obj = new FM_Case__c();
        obj.Task_Id__c = 124;
        insert obj;
        Test.setMock(WebServiceMock.class, new TaskMockClass());
                Test.setMock( WebServiceMock.class, new MaintenanceRequestMock() );
                User objUser = [SELECT Id from User
                                            where IsActive = true
                                            AND Profile.Name = 'System Administrator' LIMIT 1];
                System.runAs(objUser){
                    Test.startTest();
            MaintenanceRequestController.fetchTaskDetails(obj.Id);
            Test.stopTest();
                }

    }
    
  @isTest static void test_functionality1() {
    Account objAcc = new Account();
    objAcc = TestDataFactory_CRM.createPersonAccount();
    insert objAcc;

    NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAcc);
    insert objSR;
    Booking__c objBooking = TestDataFactoryFM.createBooking(objAcc, objSR);
    insert objBooking;
    Booking_unit__c objBU = TestDataFactoryFM.createBookingUnit(objAcc, objBooking);
    insert objBU;
    CAFM_Contract__c objContract = new CAFM_Contract__c(Name='Test Contract', Contract_Id__c = '611');
    insert objContract;
    Id caseRTId = Schema.SObjectType.CAFM_Building__c.getRecordTypeInfosByName().get('Inside unit').getRecordTypeId();
    CAFM_Building__c objCAFMBuilding = new CAFM_Building__c();
    objCAFMBuilding.Building_Code__c = 'JNU';
    objCAFMBuilding.RecordTypeId = caseRTId;
    objCAFMBuilding.CAFM_Contract__c = objContract.Id;
    objCAFMBuilding.Unit_Code__c = '45';
    // objCAFMBuilding.IPMS_Building__c = objLoc.Id;
    insert objCAFMBuilding;
    Location__c objLoc = new Location__c();
    objLoc = TestDataFactoryFM.createLocation();
    objLoc.CAFM_Unit__c = objCAFMBuilding.Id;
    insert objLoc;
    CAFM_Location__c objCAFMLocation = new CAFM_Location__c();
    objCAFMLocation.Description__c = 'DFA/1/A102';
    objCAFMLocation.Location_Id__c = '6543';
    objCAFMLocation.CAFM_Building__c = objCAFMBuilding.Id;
    objCAFMLocation.Building_Sequence_no__c = '45';
    insert objCAFMLocation;
    CAFM_Building_Contract__c obj = new CAFM_Building_Contract__c();
    obj.CAFM_Building__c = objCAFMBuilding.Id;
    obj.CAFM_Contract__c =objContract.Id;
    insert obj;

    Test.startTest();
    PageReference pageRef = Page.MaintenanceRequestPage;
    pageRef.getParameters().put('UnitId', String.valueOf(objBU.Id));
    pageRef.getParameters().put('AccountId', String.valueOf(objAcc.Id));
    pageRef.getParameters().put('SRType', 'Helpdesk');
    Test.setCurrentPage(pageRef);
    //Test.setMock( WebServiceMock.class, new MaintenanceRequestMock() );
    Test.setMock( WebServiceMock.class, new MaintenanceRequestTaskMock() );
    MaintenanceRequestController controller = new MaintenanceRequestController();
    // controller.objFMCase.Helpdesk_Request_Area__c='Inside unit';
    controller.selectedOption='Inside unit';
    controller.selectedLocation=String.valueOf(objCAFMLocation.id);
    controller.getCAFMLocations();
    //controller.getLocationDetails();
    //controller.saveAsDraft();
    controller.submit();
    Test.stopTest();
  }
  
  @isTest static void test_functionality1PortalUser() {
         Account objAcc = new Account();
    objAcc = TestDataFactory_CRM.createPersonAccount();
    insert objAcc;
    Contact con = [SELECT id FROM Contact WHERE AccountId=: objAcc.Id];
    //insert con;
       //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Customer Community Login User(Use this)%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              //ContactId = contact1.Id,
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
 

    NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAcc);
    insert objSR;
    Booking__c objBooking = TestDataFactoryFM.createBooking(objAcc, objSR);
    insert objBooking;
    Booking_unit__c objBU = TestDataFactoryFM.createBookingUnit(objAcc, objBooking);
    insert objBU;
    CAFM_Contract__c objContract = new CAFM_Contract__c(Name='Test Contract', Contract_Id__c = '611');
    insert objContract;
    Id caseRTId = Schema.SObjectType.CAFM_Building__c.getRecordTypeInfosByName().get('Inside unit').getRecordTypeId();
    CAFM_Building__c objCAFMBuilding = new CAFM_Building__c();
    objCAFMBuilding.Building_Code__c = 'JNU';
    objCAFMBuilding.RecordTypeId = caseRTId;
    objCAFMBuilding.CAFM_Contract__c = objContract.Id;
    objCAFMBuilding.Unit_Code__c = '45';
    // objCAFMBuilding.IPMS_Building__c = objLoc.Id;
    insert objCAFMBuilding;
    Location__c objLoc = new Location__c();
    objLoc = TestDataFactoryFM.createLocation();
    objLoc.CAFM_Unit__c = objCAFMBuilding.Id;
    insert objLoc;
    CAFM_Location__c objCAFMLocation = new CAFM_Location__c();
    objCAFMLocation.Description__c = 'DFA/1/A102';
    objCAFMLocation.Location_Id__c = '6543';
    objCAFMLocation.CAFM_Building__c = objCAFMBuilding.Id;
    objCAFMLocation.Building_Sequence_no__c = '45';
    insert objCAFMLocation;
    CAFM_Building_Contract__c obj = new CAFM_Building_Contract__c();
    obj.CAFM_Building__c = objCAFMBuilding.Id;
    obj.CAFM_Contract__c =objContract.Id;
    insert obj;
 System.runAs(user1) {
    Test.startTest();
    PageReference pageRef = Page.MaintenanceRequestPage;
    pageRef.getParameters().put('UnitId', String.valueOf(objBU.Id));
    pageRef.getParameters().put('AccountId', String.valueOf(objAcc.Id));
    pageRef.getParameters().put('SRType', 'Helpdesk');
    Test.setCurrentPage(pageRef);
    //Test.setMock( WebServiceMock.class, new MaintenanceRequestMock() );
    Test.setMock( WebServiceMock.class, new MaintenanceRequestTaskMock() );
    MaintenanceRequestController controller = new MaintenanceRequestController();
    // controller.objFMCase.Helpdesk_Request_Area__c='Inside unit';
    controller.selectedOption='Inside unit';
    controller.selectedLocation=String.valueOf(objCAFMLocation.id);
    controller.getCAFMLocations();
    //controller.getLocationDetails();
    //controller.saveAsDraft();
    controller.submit();
    Test.stopTest();
  }
}

@isTest static void test_functionality1PortalUserForCommonUser() {
        Account objAcc = new Account();
        objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        Contact con = [SELECT id FROM Contact WHERE AccountId=: objAcc.Id];
    
       //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Customer Community Login User(Use this)%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              //ContactId = contact1.Id,
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );


    NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAcc);
        insert objSR;
        Booking__c objBooking = TestDataFactoryFM.createBooking(objAcc, objSR);
        insert objBooking;
        Booking_unit__c objBU = TestDataFactoryFM.createBookingUnit(objAcc, objBooking);
        insert objBU;
        CAFM_Contract__c objContract = new CAFM_Contract__c(Name='Test Contract', Contract_Id__c = '611');
        insert objContract;
        Id caseRTId = Schema.SObjectType.CAFM_Building__c.getRecordTypeInfosByName().get('Common area').getRecordTypeId();
        CAFM_Building__c objCAFMBuilding = new CAFM_Building__c();
        objCAFMBuilding.Building_Code__c = 'JNU';
        objCAFMBuilding.RecordTypeId = caseRTId;
        objCAFMBuilding.CAFM_Contract__c = objContract.Id;
        objCAFMBuilding.Common_area_code__c = '45';
        // objCAFMBuilding.IPMS_Building__c = objLoc.Id;
        insert objCAFMBuilding;
        Location__c objLoc = new Location__c();
        objLoc = TestDataFactoryFM.createLocation();
        objLoc.CAFM_Common_area__c = objCAFMBuilding.Id;
        insert objLoc;
        CAFM_Location__c objCAFMLocation = new CAFM_Location__c();
        objCAFMLocation.Description__c = 'DFA/1/A102';
        objCAFMLocation.Location_Id__c = '6543';
        objCAFMLocation.CAFM_Building__c = objCAFMBuilding.Id;
        objCAFMLocation.Building_Sequence_no__c = '45';
        insert objCAFMLocation;
     System.runAs(user1) {
        Test.startTest();
        PageReference pageRef = Page.MaintenanceRequestPage;
        pageRef.getParameters().put('UnitId', String.valueOf(objBU.Id));
        pageRef.getParameters().put('AccountId', String.valueOf(objAcc.Id));
        pageRef.getParameters().put('SRType', 'Helpdesk');
        Test.setCurrentPage(pageRef);
        //Test.setMock( WebServiceMock.class, new MaintenanceRequestMock() );
        Test.setMock( WebServiceMock.class, new MaintenanceRequestTaskMock() );
        MaintenanceRequestController controller = new MaintenanceRequestController();
        // controller.objFMCase.Helpdesk_Request_Area__c='Common area';
        controller.selectedOption='Common area';
        controller.selectedLocation=String.valueOf(objCAFMLocation.id);
        controller.getCAFMLocations();
        //controller.getLocationDetails();
        //controller.saveAsDraft();
        controller.submit();
        Test.stopTest();
    }
}

@isTest static void test_functionality111() {
        Account objAcc = new Account();
        objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;

        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAcc);
        insert objSR;
        Booking__c objBooking = TestDataFactoryFM.createBooking(objAcc, objSR);
        insert objBooking;
        Booking_unit__c objBU = TestDataFactoryFM.createBookingUnit(objAcc, objBooking);
        objBU.DLP_Start_date__c = System.today() -3;
        objBU.DLP_End_Date__c = System.today() + 3;
        insert objBU;
        
        
       FM_Case__c fmCase = new FM_Case__c(Request_Type_DeveloperName__c = 'Helpdesk',
                                           RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Helpdesk').getRecordTypeId(),
                                           //Request_Type__c = 'Move In',
                                           //Origin__c = 'Portal',
                                           Booking_Unit__c = objBU.Id,
                                           Status__c = 'Draft Request');
        insert fmCase;

        CAFM_Contract__c objContract = new CAFM_Contract__c(Name='Test Contract', Contract_Id__c = '611');
        insert objContract;
        Id caseRTId = Schema.SObjectType.CAFM_Building__c.getRecordTypeInfosByName().get('Common area').getRecordTypeId();
        CAFM_Building__c objCAFMBuilding = new CAFM_Building__c();
        objCAFMBuilding.Building_Code__c = 'JNU';
        objCAFMBuilding.RecordTypeId = caseRTId;
        objCAFMBuilding.CAFM_Contract__c = objContract.Id;
        objCAFMBuilding.Common_area_code__c = '45';
        // objCAFMBuilding.IPMS_Building__c = objLoc.Id;
        insert objCAFMBuilding;
        Location__c objLoc = new Location__c();
        objLoc = TestDataFactoryFM.createLocation();
        objLoc.CAFM_Common_area__c = objCAFMBuilding.Id;
        insert objLoc;
        CAFM_Location__c objCAFMLocation = new CAFM_Location__c();
        objCAFMLocation.Description__c = 'DFA/1/A102';
        objCAFMLocation.Location_Id__c = '6543';
        objCAFMLocation.CAFM_Building__c = objCAFMBuilding.Id;
        objCAFMLocation.Building_Sequence_no__c = '45';
        insert objCAFMLocation;

        Test.startTest();
        PageReference pageRef = Page.MaintenanceRequestPage;
        pageRef.getParameters().put('UnitId', String.valueOf(objBU.Id));
        pageRef.getParameters().put('AccountId', String.valueOf(objAcc.Id));
        pageRef.getParameters().put('SRType', 'Helpdesk');
        pageRef.getParameters().put('FMCaseId', fmCase.Id);
        Test.setCurrentPage(pageRef);
        //Test.setMock( WebServiceMock.class, new MaintenanceRequestMock() );
        Test.setMock( WebServiceMock.class, new MaintenanceRequestTaskMock() );
        MaintenanceRequestController controller = new MaintenanceRequestController();
        // controller.objFMCase.Helpdesk_Request_Area__c='Common area';
        controller.selectedOption='Common area';
        controller.selectedLocation=String.valueOf(objCAFMLocation.id);
        controller.getCAFMLocations();
        //controller.getLocationDetails();
        //controller.saveAsDraft();
        controller.submit();
        Test.stopTest();
    }
  
}