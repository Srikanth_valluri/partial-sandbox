@isTest
global class SOAPCalloutServiceMockRPAgreement implements WebServiceMock {
    public integer intResponseNumber;
    public SOAPCalloutServiceMockRPAgreement() {
        
    }
    public SOAPCalloutServiceMockRPAgreement(integer intNum) {
        intResponseNumber = intNum;
    }
    global void doInvoke(Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

        if(request instanceof RentalPoolAgreementEligibility.EligibleForRentalPool_element ) {
            RentalPoolAgreementEligibility.EligibleForRentalPoolResponse_element responseNew
                = new RentalPoolAgreementEligibility.EligibleForRentalPoolResponse_element();
            if(intResponseNumber == 1) {
                responseNew.return_x = '{"allowed":null,"message":"Others - Need to remove from rule engine","mortgageNOCfromBank":null,"ifPoaTakingHandoverColatePoaPassportResidence":null,"corporateValidTradeLicence":null,"corporateArticleMemorandumOfAssociation":null,"corporateBoardResolution":null,"corporatePoa":null,"signedForm":null,"clearAndValidPassportCopyOfOwner":null,"clearAndValidPassportCopyOfJointOwner":null,"visaOrEntryStampWithUid":null,"copyofValidEmiratesId":null,"copyofValidGccId":null,"handoverChecklistAndLod":null,"keyReleaseForm":null,"checkOriginalSpaAndtakeCopyOfFirstFourPagesOfSpa":null,"areaVariationAddendum":null,"tempOne":"Cash Guaranteed Return","tempTwo":"Voucher Guaranteed Return","tempThree":null,"handoverNoticeAllowed":null,"approvalQueueOne":null,"approvalQueueTwo":null,"approvalQueueThree":null,"eligibleforRentalPool":"Yes"}';
                response.put('response_x', responseNew);
            }
            if(intResponseNumber == 2) {
                responseNew.return_x = '{"allowed":null,"message":"Others - Need to remove from rule engine","mortgageNOCfromBank":null,"ifPoaTakingHandoverColatePoaPassportResidence":null,"corporateValidTradeLicence":null,"corporateArticleMemorandumOfAssociation":null,"corporateBoardResolution":null,"corporatePoa":null,"signedForm":null,"clearAndValidPassportCopyOfOwner":null,"clearAndValidPassportCopyOfJointOwner":null,"visaOrEntryStampWithUid":null,"copyofValidEmiratesId":null,"copyofValidGccId":null,"handoverChecklistAndLod":null,"keyReleaseForm":null,"checkOriginalSpaAndtakeCopyOfFirstFourPagesOfSpa":null,"areaVariationAddendum":null,"tempOne":"Cash Guaranteed Return","tempTwo":"Voucher Guaranteed Return","tempThree":null,"handoverNoticeAllowed":null,"approvalQueueOne":null,"approvalQueueTwo":null,"approvalQueueThree":null,"eligibleforRentalPool":"No"}';
                response.put('response_x', responseNew);
            }
            if(intResponseNumber == 3) {
                responseNew.return_x = '';
                response.put('response_x', responseNew);
            }
            if(intResponseNumber == 4) {
                responseNew.return_x = '{"allowed":null,"message":"Exception","mortgageNOCfromBank":null,"ifPoaTakingHandoverColatePoaPassportResidence":null,"corporateValidTradeLicence":null,"corporateArticleMemorandumOfAssociation":null,"corporateBoardResolution":null,"corporatePoa":null,"signedForm":null,"clearAndValidPassportCopyOfOwner":null,"clearAndValidPassportCopyOfJointOwner":null,"visaOrEntryStampWithUid":null,"copyofValidEmiratesId":null,"copyofValidGccId":null,"handoverChecklistAndLod":null,"keyReleaseForm":null,"checkOriginalSpaAndtakeCopyOfFirstFourPagesOfSpa":null,"areaVariationAddendum":null,"tempOne":"Cash Guaranteed Return","tempTwo":"Voucher Guaranteed Return","tempThree":null,"handoverNoticeAllowed":null,"approvalQueueOne":null,"approvalQueueTwo":null,"approvalQueueThree":null,"eligibleforRentalPool":null}';
                response.put('response_x', responseNew);
            }
            if(intResponseNumber == 5) {
                responseNew.return_x = '{"allowed":null,"message":null,"mortgageNOCfromBank":null,"ifPoaTakingHandoverColatePoaPassportResidence":null,"corporateValidTradeLicence":null,"corporateArticleMemorandumOfAssociation":null,"corporateBoardResolution":null,"corporatePoa":null,"signedForm":null,"clearAndValidPassportCopyOfOwner":null,"clearAndValidPassportCopyOfJointOwner":null,"visaOrEntryStampWithUid":null,"copyofValidEmiratesId":null,"copyofValidGccId":null,"handoverChecklistAndLod":null,"keyReleaseForm":null,"checkOriginalSpaAndtakeCopyOfFirstFourPagesOfSpa":null,"areaVariationAddendum":null,"tempOne":"Cash Guaranteed Return","tempTwo":"Voucher Guaranteed Return","tempThree":null,"handoverNoticeAllowed":null,"approvalQueueOne":null,"approvalQueueTwo":null,"approvalQueueThree":null,"eligibleforRentalPool":null}';
                response.put('response_x', responseNew);
            }
        }
        if(request instanceof RentalPoolAgreementDocumentWSDL.RentalPoolAgreement_element ) {
            RentalPoolAgreementDocumentWSDL.RentalPoolAgreementResponse_element responseNew
                = new RentalPoolAgreementDocumentWSDL.RentalPoolAgreementResponse_element();
            if(intResponseNumber == 1) {
                responseNew.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"Successfully Created Rental pool for registration 57832","ATTRIBUTE3":"Test","ATTRIBUTE2":"Test","ATTRIBUTE1":"Test","PARAM_ID":"57832","ATTRIBUTE4":"Test"}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';
                response.put('response_x', responseNew);
            }
            if(intResponseNumber == 2) {
                responseNew.return_x = '';
                response.put('response_x', responseNew);
            }
            if(intResponseNumber == 3) {
                responseNew.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"Successfully Created Rental pool for registration 57832","ATTRIBUTE3":null,"ATTRIBUTE2":null,"ATTRIBUTE1":null,"PARAM_ID":"57832","ATTRIBUTE4":null}],"message":"Exception","status":"E"}';
                response.put('response_x', responseNew);
            }
            if(intResponseNumber == 4) {
                responseNew.return_x = '{"data":[{"PROC_STATUS":"E","PROC_MESSAGE":"Exception","ATTRIBUTE3":null,"ATTRIBUTE2":null,"ATTRIBUTE1":null,"PARAM_ID":"57832","ATTRIBUTE4":null}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';
                response.put('response_x', responseNew);
            }
            if(intResponseNumber == 5) {
                responseNew.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"File (Type,Size,Name) : PDF-70024-XXDC_RENTAL_SCHM_42463852_1.PDF","ATTRIBUTE3":null,"ATTRIBUTE2":"https://sftest.deeprootsurface.com/docs/e/42463852_RPA.pdf","ATTRIBUTE1":"https://sftest.deeprootsurface.com/docs/e/42463846_RPA_COVER.pdf","PARAM_ID":"57832","ATTRIBUTE4":null}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';
                response.put('response_x', responseNew);
            }
            if(intResponseNumber == 6) {
                responseNew.return_x = '';
                response.put('response_x', responseNew);
            }
            if(intResponseNumber == 7) {
                responseNew.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"File (Type,Size,Name) : PDF-70024-XXDC_RENTAL_SCHM_42463852_1.PDF","ATTRIBUTE3":null,"ATTRIBUTE2":"https://sftest.deeprootsurface.com/docs/e/42463852_RPA.pdf","ATTRIBUTE1":"https://sftest.deeprootsurface.com/docs/e/42463846_RPA_COVER.pdf","PARAM_ID":"57832","ATTRIBUTE4":null}],"message":"Exception","status":"E"}';
                response.put('response_x', responseNew);
            }
            if(intResponseNumber == 8) {
                responseNew.return_x = '{"data":[{"PROC_STATUS":"E","PROC_MESSAGE":"Exception","ATTRIBUTE3":null,"ATTRIBUTE2":"https://sftest.deeprootsurface.com/docs/e/42463852_RPA.pdf","ATTRIBUTE1":"https://sftest.deeprootsurface.com/docs/e/42463846_RPA_COVER.pdf","PARAM_ID":"57832","ATTRIBUTE4":null}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';
                response.put('response_x', responseNew);
            }
        }
        if(request instanceof TaskCreationWSDL.SRDataToIPMSMultiple_element ) {
            TaskCreationWSDL.SRDataToIPMSMultipleResponse_element responseNew
                = new TaskCreationWSDL.SRDataToIPMSMultipleResponse_element();
            if(intResponseNumber == 1) {
                responseNew.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"SR Header Data Updated for SR # :2-006414","PARAM_ID":"2-006414"},{"PROC_STATUS":"S","PROC_MESSAGE":"Created SR Task Data for SR # : Task :Update commencement date in system","PARAM_ID":"2-006414"},{"PROC_STATUS":"S","PROC_MESSAGE":"[UNIT] Created Unit Details for SR #  Reg Id : 57832","PARAM_ID":"2-006414"}],"message":"Process Completed Returning 3 Response Message(s)...","status":"S"}';
                response.put('response_x', responseNew);
            }
            if(intResponseNumber == 2) {
                responseNew.return_x = '';
                response.put('response_x', responseNew);
            }
            if(intResponseNumber == 3) {
                responseNew.return_x = '{"data":[{"PROC_STATUS":"E","PROC_MESSAGE":"SR Header Data Updated for SR # :2-006414","PARAM_ID":"2-006414"},{"PROC_STATUS":"E","PROC_MESSAGE":"Created SR Task Data for SR # : Task :Update commencement date in system","PARAM_ID":"2-006414"},{"PROC_STATUS":"E","PROC_MESSAGE":"[UNIT] Created Unit Details for SR #  Reg Id : 57832","PARAM_ID":"2-006414"}],"message":"Exception","status":"E"}';
                response.put('response_x', responseNew);
            }
        }
        if(request instanceof unitDetailsController.getUnitDetailValues_element){
            unitDetailsController.getUnitDetailValuesResponse_element responseNew = new unitDetailsController.getUnitDetailValuesResponse_element();
            responseNew.return_x = '["{\"ATTRIBUTE1\":\"70479\",\"ATTRIBUTE2\":\"\",\"ATTRIBUTE3\":\"PL\",\"ATTRIBUTE4\":\"N\",\"ATTRIBUTE5\":\"-.01\",\"ATTRIBUTE6\":\"\",\"ATTRIBUTE7\":\"\",\"ATTRIBUTE8\":\"1159496\",\"ATTRIBUTE9\":\"1159496\",\"ATTRIBUTE10\":\"0\",\"ATTRIBUTE11\":\"9\",\"ATTRIBUTE12\":\"N\",\"ATTRIBUTE13\":\"Y\",\"ATTRIBUTE14\":\"N\",\"ATTRIBUTE15\":\"\",\"ATTRIBUTE16\":\"N\",\"ATTRIBUTE17\":\"N\",\"ATTRIBUTE18\":\"1159496\",\"ATTRIBUTE19\":\"0\",\"ATTRIBUTE20\":\"100\",\"ATTRIBUTE21\":\"1\",\"ATTRIBUTE22\":\"Y\",\"ATTRIBUTE23\":\"0\",\"ATTRIBUTE24\":\"0\",\"ATTRIBUTE25\":\"0\",\"ATTRIBUTE26\":\"STANDARD\",\"ATTRIBUTE27\":\"N\",\"ATTRIBUTE28\":\"N\",\"ATTRIBUTE29\":\"READY\",\"ATTRIBUTE30\":\"N\",\"ATTRIBUTE31\":\"N\",\"ATTRIBUTE32\":\"\",\"ATTRIBUTE33\":\"\",\"ATTRIBUTE34\":\"N\",\"ATTRIBUTE35\":\"48242.84\",\"ATTRIBUTE36\":\"\",\"ATTRIBUTE37\":\"\",\"ATTRIBUTE38\":\"\",\"ATTRIBUTE39\":\"10-JUL-2015\",\"ATTRIBUTE40\":\"MAG2/G/G05\",\"ATTRIBUTE41\":\"1159496\",\"ATTRIBUTE42\":\"0\",\"ATTRIBUTE43\":\"9\",\"ATTRIBUTE44\":\"0\",\"ATTRIBUTE45\":\"N\",\"ATTRIBUTE46\":\"N\",\"ATTRIBUTE47\":\"N\",\"ATTRIBUTE48\":\"N\",\"ATTRIBUTE49\":\"Y\",\"ATTRIBUTE50\":\"N\",\"ATTRIBUTE51\":\"Assignment Pending with Legal\",\"ATTRIBUTE52\":\"100\",\"ATTRIBUTE53\":\"100\",\"ATTRIBUTE54\":\"\",\"ATTRIBUTE55\":\"\",\"ATTRIBUTE56\":\"-.01\",\"ATTRIBUTE57\":\"0\",\"ATTRIBUTE58\":\"48242.84\",\"ATTRIBUTE59\":\"100\",\"ATTRIBUTE60\":\"0\",\"ATTRIBUTE61\":\"0\",\"ATTRIBUTE62\":\"0\",\"ATTRIBUTE63\":\"\",\"ATTRIBUTE64\":\"\",\"ATTRIBUTE65\":\"N\",\"ATTRIBUTE66\":\"N\",\"ATTRIBUTE67\":\"N\",\"ATTRIBUTE68\":\"0\",\"ATTRIBUTE69\":\"N\",\"ATTRIBUTE70\":\"\",\"ATTRIBUTE71\":\"\",\"ATTRIBUTE72\":\"1\",\"ATTRIBUTE73\":\"PARK_BAY\",\"ATTRIBUTE74\":\"\",\"ATTRIBUTE75\":\"3\",\"ATTRIBUTE76\":\"0\",\"ATTRIBUTE77\":\"\",\"ATTRIBUTE78\":\"-2\",\"ATTRIBUTE79\":\"\",\"ATTRIBUTE80\":\"\",\"ATTRIBUTE81\":\"\",\"ATTRIBUTE82\":\"SA - Sent to buyer for signature\",\"ATTRIBUTE83\":\"Damac Properties Company Limited\",\"ATTRIBUTE84\":\"Y\",\"ATTRIBUTE85\":\"800\",\"ATTRIBUTE86\":\"0\",\"ATTRIBUTE87\":\"100\",\"ATTRIBUTE88\":\"1159498.16\",\"ATTRIBUTE89\":\"\",\"ATTRIBUTE90\":\"MAG2\",\"ATTRIBUTE91\":\"EMIRATES GARDENS 2- MAGNOLIA 2\",\"ATTRIBUTE92\":\"\",\"ATTRIBUTE93\":\"\",\"ATTRIBUTE94\":\"976032\",\"ATTRIBUTE95\":\"N...\",\"ATTRIBUTE96\":\"1449.37\",\"ATTRIBUTE97\":\"Normal\",\"ATTRIBUTE98\":\"10-JUL-2015\",\"ATTRIBUTE99\":\"APARTMENT\",\"ATTRIBUTE100\":\"1449.36\",\"ATTRIBUTE101\":\"Y\",\"ATTRIBUTE102\":\"1159496\",\"ATTRIBUTE103\":\"Y\",\"ATTRIBUTE104\":\"Y\",\"ATTRIBUTE105\":\"0\",\"ATTRIBUTE106\":\"10-JUL-2015\",\"ATTRIBUTE107\":\"N\",\"ATTRIBUTE108\":\"EMIRATES GARDENS 2\",\"ATTRIBUTE109\":\"DUBAI\",\"ATTRIBUTE110\":\"One Bedroom\",\"ATTRIBUTE111\":\"Residential\",\"ATTRIBUTE112\":\"\",\"ATTRIBUTE113\":\"\",\"ATTRIBUTE114\":\"\",\"ATTRIBUTE115\":\"\",\"ATTRIBUTE116\":\"\",\"ATTRIBUTE117\":\"\",\"ATTRIBUTE118\":\"\",\"ATTRIBUTE119\":\"\",\"ATTRIBUTE120\":\"\"}"]';
            if(intResponseNumber == 1){
                responseNew.return_x = '["{\"ATTRIBUTE1\":\"70479\",\"ATTRIBUTE2\":\"\",\"ATTRIBUTE3\":\"PL\",\"ATTRIBUTE4\":\"N\",\"ATTRIBUTE5\":\"-.01\",\"ATTRIBUTE6\":\"\",\"ATTRIBUTE7\":\"\",\"ATTRIBUTE8\":\"1159496\",\"ATTRIBUTE9\":\"1159496\",\"ATTRIBUTE10\":\"0\",\"ATTRIBUTE11\":\"9\",\"ATTRIBUTE12\":\"N\",\"ATTRIBUTE13\":\"Y\",\"ATTRIBUTE14\":\"N\",\"ATTRIBUTE15\":\"\",\"ATTRIBUTE16\":\"Y\",\"ATTRIBUTE17\":\"Y\",\"ATTRIBUTE18\":\"1159496\",\"ATTRIBUTE19\":\"0\",\"ATTRIBUTE20\":\"100\",\"ATTRIBUTE21\":\"1\",\"ATTRIBUTE22\":\"Y\",\"ATTRIBUTE23\":\"0\",\"ATTRIBUTE24\":\"0\",\"ATTRIBUTE25\":\"0\",\"ATTRIBUTE26\":\"STANDARD\",\"ATTRIBUTE27\":\"N\",\"ATTRIBUTE28\":\"N\",\"ATTRIBUTE29\":\"READY\",\"ATTRIBUTE30\":\"N\",\"ATTRIBUTE31\":\"N\",\"ATTRIBUTE32\":\"\",\"ATTRIBUTE33\":\"\",\"ATTRIBUTE34\":\"N\",\"ATTRIBUTE35\":\"48242.84\",\"ATTRIBUTE36\":\"\",\"ATTRIBUTE37\":\"\",\"ATTRIBUTE38\":\"\",\"ATTRIBUTE39\":\"10-JUL-2015\",\"ATTRIBUTE40\":\"MAG2/G/G05\",\"ATTRIBUTE41\":\"1159496\",\"ATTRIBUTE42\":\"0\",\"ATTRIBUTE43\":\"9\",\"ATTRIBUTE44\":\"0\",\"ATTRIBUTE45\":\"Y\",\"ATTRIBUTE46\":\"Y\",\"ATTRIBUTE47\":\"Y\",\"ATTRIBUTE48\":\"Y\",\"ATTRIBUTE49\":\"Y\",\"ATTRIBUTE50\":\"N\",\"ATTRIBUTE51\":\"Assignment Pending with Legal\",\"ATTRIBUTE52\":\"100\",\"ATTRIBUTE53\":\"100\",\"ATTRIBUTE54\":\"\",\"ATTRIBUTE55\":\"\",\"ATTRIBUTE56\":\"-.01\",\"ATTRIBUTE57\":\"0\",\"ATTRIBUTE58\":\"48242.84\",\"ATTRIBUTE59\":\"100\",\"ATTRIBUTE60\":\"0\",\"ATTRIBUTE61\":\"0\",\"ATTRIBUTE62\":\"0\",\"ATTRIBUTE63\":\"\",\"ATTRIBUTE64\":\"\",\"ATTRIBUTE65\":\"N\",\"ATTRIBUTE66\":\"N\",\"ATTRIBUTE67\":\"N\",\"ATTRIBUTE68\":\"0\",\"ATTRIBUTE69\":\"N\",\"ATTRIBUTE70\":\"\",\"ATTRIBUTE71\":\"\",\"ATTRIBUTE72\":\"1\",\"ATTRIBUTE73\":\"PARK_BAY\",\"ATTRIBUTE74\":\"\",\"ATTRIBUTE75\":\"3\",\"ATTRIBUTE76\":\"0\",\"ATTRIBUTE77\":\"\",\"ATTRIBUTE78\":\"-2\",\"ATTRIBUTE79\":\"\",\"ATTRIBUTE80\":\"\",\"ATTRIBUTE81\":\"\",\"ATTRIBUTE82\":\"SA - Sent to buyer for signature\",\"ATTRIBUTE83\":\"Damac Properties Company Limited\",\"ATTRIBUTE84\":\"Y\",\"ATTRIBUTE85\":\"800\",\"ATTRIBUTE86\":\"0\",\"ATTRIBUTE87\":\"100\",\"ATTRIBUTE88\":\"1159498.16\",\"ATTRIBUTE89\":\"\",\"ATTRIBUTE90\":\"MAG2\",\"ATTRIBUTE91\":\"EMIRATES GARDENS 2- MAGNOLIA 2\",\"ATTRIBUTE92\":\"\",\"ATTRIBUTE93\":\"\",\"ATTRIBUTE94\":\"976032\",\"ATTRIBUTE95\":\"N...\",\"ATTRIBUTE96\":\"1449.37\",\"ATTRIBUTE97\":\"Normal\",\"ATTRIBUTE98\":\"10-JUL-2015\",\"ATTRIBUTE99\":\"APARTMENT\",\"ATTRIBUTE100\":\"1449.36\",\"ATTRIBUTE101\":\"Y\",\"ATTRIBUTE102\":\"1159496\",\"ATTRIBUTE103\":\"Y\",\"ATTRIBUTE104\":\"Y\",\"ATTRIBUTE105\":\"0\",\"ATTRIBUTE106\":\"10-JUL-2015\",\"ATTRIBUTE107\":\"N\",\"ATTRIBUTE108\":\"EMIRATES GARDENS 2\",\"ATTRIBUTE109\":\"DUBAI\",\"ATTRIBUTE110\":\"One Bedroom\",\"ATTRIBUTE111\":\"Residential\",\"ATTRIBUTE112\":\"\",\"ATTRIBUTE113\":\"\",\"ATTRIBUTE114\":\"\",\"ATTRIBUTE115\":\"\",\"ATTRIBUTE116\":\"\",\"ATTRIBUTE117\":\"\",\"ATTRIBUTE118\":\"\",\"ATTRIBUTE119\":\"\",\"ATTRIBUTE120\":\"\"}"]';
            }else if(intResponseNumber == 2){
                responseNew.return_x = '["{\"ATTRIBUTE1\":\"70479\",\"ATTRIBUTE2\":\"\",\"ATTRIBUTE3\":\"PL\",\"ATTRIBUTE4\":\"N\",\"ATTRIBUTE5\":\"-.01\",\"ATTRIBUTE6\":\"\",\"ATTRIBUTE7\":\"\",\"ATTRIBUTE8\":\"1159496\",\"ATTRIBUTE9\":\"1159496\",\"ATTRIBUTE10\":\"0\",\"ATTRIBUTE11\":\"9\",\"ATTRIBUTE12\":\"N\",\"ATTRIBUTE13\":\"Y\",\"ATTRIBUTE14\":\"N\",\"ATTRIBUTE15\":\"\",\"ATTRIBUTE16\":\"Y\",\"ATTRIBUTE17\":\"N\",\"ATTRIBUTE18\":\"1159496\",\"ATTRIBUTE19\":\"0\",\"ATTRIBUTE20\":\"100\",\"ATTRIBUTE21\":\"1\",\"ATTRIBUTE22\":\"Y\",\"ATTRIBUTE23\":\"0\",\"ATTRIBUTE24\":\"0\",\"ATTRIBUTE25\":\"0\",\"ATTRIBUTE26\":\"STANDARD\",\"ATTRIBUTE27\":\"N\",\"ATTRIBUTE28\":\"N\",\"ATTRIBUTE29\":\"READY\",\"ATTRIBUTE30\":\"N\",\"ATTRIBUTE31\":\"N\",\"ATTRIBUTE32\":\"\",\"ATTRIBUTE33\":\"\",\"ATTRIBUTE34\":\"N\",\"ATTRIBUTE35\":\"48242.84\",\"ATTRIBUTE36\":\"\",\"ATTRIBUTE37\":\"\",\"ATTRIBUTE38\":\"\",\"ATTRIBUTE39\":\"10-JUL-2015\",\"ATTRIBUTE40\":\"MAG2/G/G05\",\"ATTRIBUTE41\":\"1159496\",\"ATTRIBUTE42\":\"0\",\"ATTRIBUTE43\":\"9\",\"ATTRIBUTE44\":\"0\",\"ATTRIBUTE45\":\"N\",\"ATTRIBUTE46\":\"N\",\"ATTRIBUTE47\":\"N\",\"ATTRIBUTE48\":\"N\",\"ATTRIBUTE49\":\"Y\",\"ATTRIBUTE50\":\"N\",\"ATTRIBUTE51\":\"Assignment Pending with Legal\",\"ATTRIBUTE52\":\"100\",\"ATTRIBUTE53\":\"100\",\"ATTRIBUTE54\":\"\",\"ATTRIBUTE55\":\"\",\"ATTRIBUTE56\":\"-.01\",\"ATTRIBUTE57\":\"0\",\"ATTRIBUTE58\":\"48242.84\",\"ATTRIBUTE59\":\"100\",\"ATTRIBUTE60\":\"0\",\"ATTRIBUTE61\":\"0\",\"ATTRIBUTE62\":\"0\",\"ATTRIBUTE63\":\"\",\"ATTRIBUTE64\":\"\",\"ATTRIBUTE65\":\"N\",\"ATTRIBUTE66\":\"N\",\"ATTRIBUTE67\":\"N\",\"ATTRIBUTE68\":\"0\",\"ATTRIBUTE69\":\"N\",\"ATTRIBUTE70\":\"\",\"ATTRIBUTE71\":\"\",\"ATTRIBUTE72\":\"1\",\"ATTRIBUTE73\":\"PARK_BAY\",\"ATTRIBUTE74\":\"\",\"ATTRIBUTE75\":\"3\",\"ATTRIBUTE76\":\"0\",\"ATTRIBUTE77\":\"\",\"ATTRIBUTE78\":\"-2\",\"ATTRIBUTE79\":\"\",\"ATTRIBUTE80\":\"\",\"ATTRIBUTE81\":\"\",\"ATTRIBUTE82\":\"SA - Sent to buyer for signature\",\"ATTRIBUTE83\":\"Damac Properties Company Limited\",\"ATTRIBUTE84\":\"Y\",\"ATTRIBUTE85\":\"800\",\"ATTRIBUTE86\":\"0\",\"ATTRIBUTE87\":\"100\",\"ATTRIBUTE88\":\"1159498.16\",\"ATTRIBUTE89\":\"\",\"ATTRIBUTE90\":\"MAG2\",\"ATTRIBUTE91\":\"EMIRATES GARDENS 2- MAGNOLIA 2\",\"ATTRIBUTE92\":\"\",\"ATTRIBUTE93\":\"\",\"ATTRIBUTE94\":\"976032\",\"ATTRIBUTE95\":\"N...\",\"ATTRIBUTE96\":\"1449.37\",\"ATTRIBUTE97\":\"Normal\",\"ATTRIBUTE98\":\"10-JUL-2015\",\"ATTRIBUTE99\":\"APARTMENT\",\"ATTRIBUTE100\":\"1449.36\",\"ATTRIBUTE101\":\"Y\",\"ATTRIBUTE102\":\"1159496\",\"ATTRIBUTE103\":\"Y\",\"ATTRIBUTE104\":\"Y\",\"ATTRIBUTE105\":\"0\",\"ATTRIBUTE106\":\"10-JUL-2015\",\"ATTRIBUTE107\":\"N\",\"ATTRIBUTE108\":\"EMIRATES GARDENS 2\",\"ATTRIBUTE109\":\"DUBAI\",\"ATTRIBUTE110\":\"One Bedroom\",\"ATTRIBUTE111\":\"Residential\",\"ATTRIBUTE112\":\"\",\"ATTRIBUTE113\":\"\",\"ATTRIBUTE114\":\"\",\"ATTRIBUTE115\":\"\",\"ATTRIBUTE116\":\"\",\"ATTRIBUTE117\":\"\",\"ATTRIBUTE118\":\"\",\"ATTRIBUTE119\":\"\",\"ATTRIBUTE120\":\"\"}"]';
            }else if(intResponseNumber == 3){
                responseNew.return_x = '["{\"ATTRIBUTE1\":\"70479\",\"ATTRIBUTE2\":\"\",\"ATTRIBUTE3\":\"PL\",\"ATTRIBUTE4\":\"N\",\"ATTRIBUTE5\":\"-.01\",\"ATTRIBUTE6\":\"\",\"ATTRIBUTE7\":\"\",\"ATTRIBUTE8\":\"1159496\",\"ATTRIBUTE9\":\"1159496\",\"ATTRIBUTE10\":\"0\",\"ATTRIBUTE11\":\"9\",\"ATTRIBUTE12\":\"N\",\"ATTRIBUTE13\":\"Y\",\"ATTRIBUTE14\":\"N\",\"ATTRIBUTE15\":\"\",\"ATTRIBUTE16\":\"N\",\"ATTRIBUTE17\":\"Y\",\"ATTRIBUTE18\":\"1159496\",\"ATTRIBUTE19\":\"0\",\"ATTRIBUTE20\":\"100\",\"ATTRIBUTE21\":\"1\",\"ATTRIBUTE22\":\"Y\",\"ATTRIBUTE23\":\"0\",\"ATTRIBUTE24\":\"0\",\"ATTRIBUTE25\":\"0\",\"ATTRIBUTE26\":\"STANDARD\",\"ATTRIBUTE27\":\"N\",\"ATTRIBUTE28\":\"N\",\"ATTRIBUTE29\":\"READY\",\"ATTRIBUTE30\":\"N\",\"ATTRIBUTE31\":\"N\",\"ATTRIBUTE32\":\"\",\"ATTRIBUTE33\":\"\",\"ATTRIBUTE34\":\"N\",\"ATTRIBUTE35\":\"48242.84\",\"ATTRIBUTE36\":\"\",\"ATTRIBUTE37\":\"\",\"ATTRIBUTE38\":\"\",\"ATTRIBUTE39\":\"10-JUL-2015\",\"ATTRIBUTE40\":\"MAG2/G/G05\",\"ATTRIBUTE41\":\"1159496\",\"ATTRIBUTE42\":\"0\",\"ATTRIBUTE43\":\"9\",\"ATTRIBUTE44\":\"0\",\"ATTRIBUTE45\":\"N\",\"ATTRIBUTE46\":\"N\",\"ATTRIBUTE47\":\"N\",\"ATTRIBUTE48\":\"N\",\"ATTRIBUTE49\":\"Y\",\"ATTRIBUTE50\":\"N\",\"ATTRIBUTE51\":\"Assignment Pending with Legal\",\"ATTRIBUTE52\":\"100\",\"ATTRIBUTE53\":\"100\",\"ATTRIBUTE54\":\"\",\"ATTRIBUTE55\":\"\",\"ATTRIBUTE56\":\"-.01\",\"ATTRIBUTE57\":\"0\",\"ATTRIBUTE58\":\"48242.84\",\"ATTRIBUTE59\":\"100\",\"ATTRIBUTE60\":\"0\",\"ATTRIBUTE61\":\"0\",\"ATTRIBUTE62\":\"0\",\"ATTRIBUTE63\":\"\",\"ATTRIBUTE64\":\"\",\"ATTRIBUTE65\":\"N\",\"ATTRIBUTE66\":\"N\",\"ATTRIBUTE67\":\"N\",\"ATTRIBUTE68\":\"0\",\"ATTRIBUTE69\":\"N\",\"ATTRIBUTE70\":\"\",\"ATTRIBUTE71\":\"\",\"ATTRIBUTE72\":\"1\",\"ATTRIBUTE73\":\"PARK_BAY\",\"ATTRIBUTE74\":\"\",\"ATTRIBUTE75\":\"3\",\"ATTRIBUTE76\":\"0\",\"ATTRIBUTE77\":\"\",\"ATTRIBUTE78\":\"-2\",\"ATTRIBUTE79\":\"\",\"ATTRIBUTE80\":\"\",\"ATTRIBUTE81\":\"\",\"ATTRIBUTE82\":\"SA - Sent to buyer for signature\",\"ATTRIBUTE83\":\"Damac Properties Company Limited\",\"ATTRIBUTE84\":\"Y\",\"ATTRIBUTE85\":\"800\",\"ATTRIBUTE86\":\"0\",\"ATTRIBUTE87\":\"100\",\"ATTRIBUTE88\":\"1159498.16\",\"ATTRIBUTE89\":\"\",\"ATTRIBUTE90\":\"MAG2\",\"ATTRIBUTE91\":\"EMIRATES GARDENS 2- MAGNOLIA 2\",\"ATTRIBUTE92\":\"\",\"ATTRIBUTE93\":\"\",\"ATTRIBUTE94\":\"976032\",\"ATTRIBUTE95\":\"N...\",\"ATTRIBUTE96\":\"1449.37\",\"ATTRIBUTE97\":\"Normal\",\"ATTRIBUTE98\":\"10-JUL-2015\",\"ATTRIBUTE99\":\"APARTMENT\",\"ATTRIBUTE100\":\"1449.36\",\"ATTRIBUTE101\":\"Y\",\"ATTRIBUTE102\":\"1159496\",\"ATTRIBUTE103\":\"Y\",\"ATTRIBUTE104\":\"Y\",\"ATTRIBUTE105\":\"0\",\"ATTRIBUTE106\":\"10-JUL-2015\",\"ATTRIBUTE107\":\"N\",\"ATTRIBUTE108\":\"EMIRATES GARDENS 2\",\"ATTRIBUTE109\":\"DUBAI\",\"ATTRIBUTE110\":\"One Bedroom\",\"ATTRIBUTE111\":\"Residential\",\"ATTRIBUTE112\":\"\",\"ATTRIBUTE113\":\"\",\"ATTRIBUTE114\":\"\",\"ATTRIBUTE115\":\"\",\"ATTRIBUTE116\":\"\",\"ATTRIBUTE117\":\"\",\"ATTRIBUTE118\":\"\",\"ATTRIBUTE119\":\"\",\"ATTRIBUTE120\":\"\"}"]';
            }
            response.put('response_x', responseNew);
        }
        if(request instanceof GenerateSOAService.GenCustomerStatement_element){
          GenerateSOAService.GenCustomerStatementResponse_element responseNew = new GenerateSOAService.GenCustomerStatementResponse_element();
            if(intResponseNumber == 1){
                responseNew.return_x = 'https://www.soaurl.com';
                response.put('response_x', responseNew);
            }
        }
        if(request instanceof GenerateCRFService.GetCustomerRequestForm_element){
          GenerateCRFService.GetCustomerRequestFormResponse_element responseNew = new GenerateCRFService.GetCustomerRequestFormResponse_element();
            if(intResponseNumber == 1){
                responseNew.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=467060 and Request Id :42470368 ...","ATTRIBUTE3":"467060","ATTRIBUTE2":"42470368","ATTRIBUTE1":"https://sftest.deeprootsurface.com/docs/e/42470368_57829_CRF.pdf","PARAM_ID":"57829"}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';
                response.put('response_x', responseNew);
            }
            if(intResponseNumber == 2){
                responseNew.return_x = '{"data":[{"PROC_STATUS":"E","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=467060 and Request Id :42470368 ...","ATTRIBUTE3":"467060","ATTRIBUTE2":"42470368","ATTRIBUTE1":"https://sftest.deeprootsurface.com/docs/e/42470368_57829_CRF.pdf","PARAM_ID":"57829"}],"message":"Process Completed Returning 1 Response Message(s)...","status":"E"}';
                response.put('response_x', responseNew);
            }
        }
        if(request instanceof MultipleDocUploadService.DocumentAttachmentMultiple_element){
          MultipleDocUploadService.DocumentAttachmentMultiple_element request_x = (MultipleDocUploadService.DocumentAttachmentMultiple_element)request;
          system.debug('debug this value******************'+request_x.regTerms);
          List<beanComXsdMultipleDocUpload.DocUploadDTO>  instance = request_x.regTerms;
          system.debug('filename**********'+instance[0].fileName);
          string RegId = instance[0].fileName.substringBetween('IPMS-','-');
          MultipleDocUploadService.DocumentAttachmentMultipleResponse_element responseNew
          = new MultipleDocUploadService.DocumentAttachmentMultipleResponse_element();
          if(intResponseNumber == 1) {
              if(instance[0].fileName.contains('POA')){
                  responseNew.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS]Files : [IPMS-'+RegId+'-BUYER_POA.pdf Processed] PK Value#:2-2-005185","PARAM_ID":"IPMS-'+RegId+'-BUYER_POA","URL":"https://sftest.deeprootsurface.com/docs/t/IPMS-'+RegId+'-BUYER_POA.pdf"},{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS]Files : [IPMS-'+RegId+'-SELLER_POA.pdf Processed] PK Value#:2-2-005185","PARAM_ID":"IPMS-'+RegId+'-SELLER_POA","URL":"https://sftest.deeprootsurface.com/docs/t/IPMS-'+RegId+'-SELLER_POA.pdf"}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';
              }else{
                  responseNew.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS]Files : [IPMS-'+RegId+'-TEST.pdf Processed] PK Value#:2-2-005185","PARAM_ID":"IPMS-'+RegId+'-TEST","URL":"https://sftest.deeprootsurface.com/docs/t/IPMS-'+RegId+'-TEST.pdf"}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';
              }
          }
          if(intResponseNumber == 2) {
            responseNew.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS]Files : [IPMS-'+RegId+'-TEST.pdf Processed] PK Value#:2-2-005185","PARAM_ID":"IPMS-'+RegId+'-TEST","URL":"https://sftest.deeprootsurface.com/docs/t/IPMS-'+RegId+'-TEST.pdf"}],"message":"Process Completed Returning 1 Response Message(s)...","status":"Exception"}';
          }
          response.put('response_x', responseNew);
        }
        if(request instanceof RentalPoolAgreementDocumentWSDL.TERMINATE_RP_RECORD_element){
          RentalPoolAgreementDocumentWSDL.TERMINATE_RP_RECORDResponse_element responseNew = new RentalPoolAgreementDocumentWSDL.TERMINATE_RP_RECORDResponse_element();
            if(intResponseNumber == 1){
                responseNew.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"Successfully Created Rental pool for registration 57832","ATTRIBUTE3":"Test","ATTRIBUTE2":"Test","ATTRIBUTE1":"Test","PARAM_ID":"57832","ATTRIBUTE4":"Test"}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';
                response.put('response_x', responseNew);
            }
            
        }

    }
}