public without sharing class FullCalendarControllerClone {

    private static final String DATE_FORMAT = 'EEE, d MMM yyyy HH:mm:ss z';
    private static final String FORMAT = 'YYYY-MM-dd';

    public FullCalendarControllerClone() {}

    @RemoteAction
    public static List<CalendarEvent> getCalendarEvents(String slctdDate,
                                                        String accountId,
                                                        String unitName,
                                                        String processName,
                                                        String subProcessName,
                                                        String source) {
        list<Appointment__c> newSlots = new list<Appointment__c>();
        String now = (String.valueOf(System.Now().time())).substringBeforelast(':');
        system.debug('now '+now);

        System.debug('--- slctdDate --- : '+slctdDate);
        System.debug('--- accountId --- : '+accountId);
        System.debug('--- unitName --- : '+unitName);
        System.debug('--- processName --- : '+processName);
        System.debug('--- subProcessName --- : '+subProcessName);
        System.debug('--- source --- : '+source);

        Map<Date, list<Appointment__c>> mapDateAppointment = new Map<Date, list<Appointment__c>>();

        AppointmentSelectionHandler objAppointmentSelectionHandler = new AppointmentSelectionHandler();
        mapDateAppointment = objAppointmentSelectionHandler.getAppointmentforFuture(accountId,
                                                                                    unitName,
                                                                                    (processName.contains('-') ? processName.substringAfter('-') : processName),
                                                                                    subProcessName,
                                                                                    source,
                                                                                    slctdDate,
                                                                                    true);
        System.debug('--- mapDateAppointment --- : '+mapDateAppointment);

        Map<String, CalendarEvent> mapCalendarEvents = new Map<String, CalendarEvent>();
        if(mapDateAppointment != NULL && mapDateAppointment.keySet().size() > 0) {
            for(Date objDate : mapDateAppointment.keySet()) {
                if(mapDateAppointment.get(objDate) != NULL) {
                    for(Appointment__c objAppointment : mapDateAppointment.get(objDate)) {
                        CalendarEvent event = new CalendarEvent(objAppointment, objDate);
                        System.debug('--- event.startString + event.endString --- : '+event.startString + event.endString);
                        mapCalendarEvents.put(event.startString + event.endString, event);
                    }
                }
            }
        }
        system.debug('>>> mapCalendarEvents'+ mapCalendarEvents);
        return mapCalendarEvents.values();
    }

    public class CalendarEvent {
        public String title {get;set;}
        public Boolean allDay {get;set;}
        public String startString {get;set;}
        public String startDate {get;set;}
        public String endString {get;set;}
        public String url {get;set;}
        public String className {get;set;}
        public String eventType {get;set;}

        public CalendarEvent() {}

        public CalendarEvent(Appointment__c objAppoints, Date objDate) {
            this.allDay = false;
            if(objAppoints.Appointment_End_Date__c >= objDate) {
                this.startDate = objDate == NULL ? NULL : Datetime.newInstance(
                    objDate.year(), objDate.month(),
                    objDate.day()).format(FORMAT);
                this.startString = objDate == NULL ? NULL : Datetime.newInstance(
                    objDate.year(), objDate.month(), objDate.day()).format(FORMAT) + 'T'
                    + objAppoints.Slots__c.split('-')[0].remove(' ')+':00' ;
                this.endString = objDate == NULL ? NULL : Datetime.newInstance(
                    objDate.year(), objDate.month(), objDate.day()).format(FORMAT)+ 'T'
                    + objAppoints.Slots__c.split('-')[1].remove(' ')+':00' ;
            }
            this.eventType = 'slot';
        }
    }
}