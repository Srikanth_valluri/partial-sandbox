@isTest
private class MortageTaskCreationTest
{
    private static Id getRecordTypeId(){
        return Schema.SObjectType.Case.getRecordTypeInfosByName().get('Mortgage').getRecordTypeId();
    }

    @isTest static void itShould() {


        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
                insert Sr;
                System.assert(Sr != null);

                Account   Acc = TestDataFactory_CRM.createPersonAccount();
                Acc.Nationality__c = 'UAE';
                insert Acc;
                 
                Booking__c Booking =  new Booking__c();
                Booking.AWB_Number__c = 'Test AWB';
                Booking.Account__c = Acc.Id;
                Booking.Deal_SR__c = Sr.Id;
                insert Booking;

                Booking_Unit__c BU =  new Booking_Unit__c();
                BU.Unit_Name__c = 'Test Units';  
                BU.Registration_Status__c  = 'Active';  
                BU.Unit_Selling_Price_AED__c  = 100;  
                BU.Booking__c = Booking.Id; 
                BU.Registration_ID__c = '123135'; 
                insert BU; 

                Buyer__c buyer =  new Buyer__c();
                buyer.Account__c  = Acc.Id;
                buyer.Booking__c   = Booking.Id;
                buyer.Primary_Buyer__c   = false;
                buyer.First_Name__c  = 'Test';      
                buyer.Last_Name__c  = 'Test Last';
                buyer.IPMS_Registration_ID__c ='123456';
                insert buyer;

                Case caseObj = TestDataFactory_CRM.createCase(Acc.Id, getRecordTypeId());
                caseObj.Booking_Unit__c = BU.Id;
        insert caseObj;

        Test.startTest();
            
            Test.setMock(WebServiceMock.class, new MortgageWebServiceCalloutMock());
            MortageTaskCreation.MortgageTask(new Set<String>{ caseObj.Id });
        Test.stopTest();

    }
    
    @isTest static void itShould2() {



        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
                insert Sr;
                System.assert(Sr != null);

                Account   Acc = TestDataFactory_CRM.createPersonAccount();
                Acc.Nationality__c = 'UAE';
                insert Acc;
                 
                Booking__c Booking =  new Booking__c();
                Booking.AWB_Number__c = 'Test AWB';
                Booking.Account__c = Acc.Id;
                Booking.Deal_SR__c = Sr.Id;
                insert Booking;

                Booking_Unit__c BU =  new Booking_Unit__c();
                BU.Unit_Name__c = 'Test Units';  
                BU.Registration_Status__c  = 'Active';  
                BU.Unit_Selling_Price_AED__c  = 100;  
                BU.Booking__c = Booking.Id; 
                BU.Registration_ID__c = '123135'; 
                insert BU; 

                Buyer__c buyer =  new Buyer__c();
                buyer.Account__c  = Acc.Id;
                buyer.Booking__c   = Booking.Id;
                buyer.Primary_Buyer__c   = false;
                buyer.First_Name__c  = 'Test';      
                buyer.Last_Name__c  = 'Test Last';
                buyer.IPMS_Registration_ID__c ='123456';
                insert buyer;

                Case caseObj = TestDataFactory_CRM.createCase(Acc.Id, getRecordTypeId());


        insert caseObj;

        Test.startTest();
                        caseObj.Type = 'Mortgage';
                caseObj.Booking_Unit__c = BU.Id;
                caseObj.status = 'Mortgage Off Plan / Ready';
                update caseObj;
            
            Test.setMock(WebServiceMock.class, new MortgageWebServiceCalloutMock());
            MortageTaskCreation.MortgageTask(new Set<String>{ caseObj.Id });
        Test.stopTest();

    }
}