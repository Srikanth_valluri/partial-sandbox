public without sharing class FetchFmDuesOnCallingListCntrl {
    
    public String callingListId                         {get;set;}
    public Id callingListRecordTypeId                   {get;set;}
    public List<Calling_List__c> callingList            {get;set;}
    public FmIpmsRestServices.DueInvoicesResult dues    {get;set;}
    public List<FmIpmsRestServices.DueInvoice> lstFMDue {get; set;} 
    public FetchFmDuesOnCallingListCntrl(ApexPages.StandardController controller) {
        
    }
     /*This Method is used to fetch all fm dues related to unit*/
    public void init(){
        callingList = new List<Calling_List__c>();
        lstFMDue = new List<FmIpmsRestServices.DueInvoice>();
        callingListRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('FM Collections').RecordTypeId;
        callingListId = ApexPages.currentPage().getParameters().get('id');
        
        System.debug('callingListId:::::::'+callingListId);
        callingList = [SELECT Id,
                            RecordTypeId,
                            Registration_ID__c,Booking_Unit__r.Id
                       FROM Calling_List__c
                       WHERE Id = :callingListId
                         AND RecordTypeId = :callingListRecordTypeId LIMIT 1];
        
        if( String.isNotBlank( callingList[0].Registration_ID__c ) ) {
            dues = FMCallingListCreationBatch.fetchFmDues( callingList[0].Registration_ID__c , callingList[0].Booking_Unit__r.Id );
            if( dues.lstDueInvoice != null && dues.lstDueInvoice.size() > 0 ) {
                lstFMDue = dues.lstDueInvoice;
            }
        }


    }//init
}