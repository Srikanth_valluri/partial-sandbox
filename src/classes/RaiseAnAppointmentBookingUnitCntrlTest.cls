@isTest
public class RaiseAnAppointmentBookingUnitCntrlTest{

    static testMethod void Test1(){
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person',party_ID__C='123456');
        objAcc.Email__pc = 'new@no.com';
        insert objAcc;
                
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        insert objBookingUnit;
        
        Location__c loc = new Location__c();
        loc.Location_ID__c = '22656';
        loc.Name = 'JNU';
        insert loc;
        
        Appointment__c objApp = new Appointment__c();
        objApp.Appointment_Date__c = date.newinstance( 2018, 12, 12 );
        objApp.Building__c = loc.id;
        objApp.Process_Name__c = 'Handover';
        objApp.Sub_Process_Name__c = 'Documentation';
        objApp.Slots__c = '10:00 - 11:00';
        insert objApp;
        
        Appointment__c objApp1 = new Appointment__c();
        objApp1.Appointment_Date__c = date.newinstance( 2018, 12, 12 );
        objApp1.Building__c = loc.id;
        objApp1.Process_Name__c = 'Handover';
        objApp1.Sub_Process_Name__c = 'Unit Viewing';
        objApp1.Slots__c = '10:00 - 11:00';
        insert objApp1;
        
        Appointment__c objApp2 = new Appointment__c();
        objApp2.Appointment_Date__c = date.newinstance( 2018, 12, 12 );
        objApp2.Building__c = loc.id;
        objApp2.Process_Name__c = 'Handover';
        objApp2.Sub_Process_Name__c = 'Key Handover';
        objApp2.Slots__c = '10:00 - 11:00';
        insert objApp2;
        
        TriggerOnOffCustomSetting__c objOff = new TriggerOnOffCustomSetting__c ();
        objOff.Name = 'CallingListTrigger';
        objOff.OnOffCheck__c = true;
        insert objOff;
        
        Id handoverCLId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Handover Calling List').getRecordTypeId();
        Calling_list__c objCL = new Calling_list__c();
        objCL.RecordTypeId = handoverCLId;
        objCL.Account__c = objAcc.id;
        objCL.Booking_Unit__c = objBookingUnit.id;
        insert objCL;

        
        test.startTest();
        PageReference pageRef = Page.RaiseAnAppointmentBookingUnit;
        pageRef.getParameters().put('id', String.valueOf(objBookingUnit.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(objBookingUnit);       
        RaiseAnAppointmentBookingUnitCntrl  obj = new RaiseAnAppointmentBookingUnitCntrl(sc);
        
        test.stopTest();
    }

}