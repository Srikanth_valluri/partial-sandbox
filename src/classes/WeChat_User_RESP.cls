public class WeChat_User_RESP{
    public cls_appUser appUser;
    public class cls_appUser {
        public String res_id;   //deb920657bbc3adc3fec7963
        public String userId;   //steveb@channel5.com
        public String givenName;    //Steve
        public String surname;  //Brule
        public String email;    //steveb@channel5.com
        public String signedUpAt;   //2015-10-08T23:52:11.677Z
        public cls_properties properties;
        public boolean conversationStarted;
        public cls_clients[] clients;
    }
    public class cls_properties {
    }
    public class cls_clients {
        public boolean active;
        public String appVersion;   //1.0
        public String res_id;   //5A7F8343-DF41-46A8-96EC-8583FCB422FB
        public String res_id2;   //5A7F8343-DF41-46A8-96EC-8583FCB422FB
        public String lastSeen; //2016-03-09T19:09:01.431Z
        public String platform; //ios
        public String pushNotificationToken;    //<...>
        public cls_info info;
        public cls_raw raw;
    }
    public class cls_info {
        public String appName;  //ShellApp
        public String devicePlatform;   //x86_64
        public String os;   //iPhone OS
        public String osVersion;    //9.2
    }
    public class cls_raw {
        public String appName;  //ShellApp
        public String devicePlatform;   //x86_64
        public String os;   //iPhone OS
        public String osVersion;    //9.2
    }
    public static WeChat_User_RESP parse(String json){
        return (WeChat_User_RESP) System.JSON.deserialize(json, WeChat_User_RESP.class);
    }

}