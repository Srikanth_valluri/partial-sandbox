public class PenaltySOA_WebServiceAdapter extends GenericWebServiceAdapter implements WebServiceAdapter{

    public String url; 
    
    public override WebServiceAdapter call(){
        String regID_BUID = parameters.get('regID_BUID');
        system.debug('!!!!!!!!regID_BUID'+regID_BUID);
        String BUId, regID;
        
        if(String.isNotBlank(regID_BUID)) {
            BUId = regID_BUID.substringBefore('-');
            regID = regID_BUID.substringAfter('-');
        }
        PenaltyWaiverService.SopResponseObject objPenResponse = PenaltyWaiverService.getSOPDocument(regID);
        if(objPenResponse != NULL && String.isNotBlank(objPenResponse.url)) {
            this.url = objPenResponse.url;
        }
        return this;
    }
    
    public override String getResponse(){
        return this.url;
    }
    
}