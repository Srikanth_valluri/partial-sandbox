public with sharing class SendEmailtoLeaseTeamLHO {
    
    @InvocableMethod 
    public static void invokeApex(list<Id> lstCaseId) {
        String URLsDocument;
        Case lhoCase;
        String BuildingName;
        
        for (SR_Attachments__c objAttach : [Select Id
                                                , Name
                                                , Case__c
                                                , Type__c
                                                , Attachment_URL__c
                                            From SR_Attachments__c
                                            Where Attachment_URL__c != null
                                            and Case__c IN: lstCaseId]) {
            String documentlink = '<a href="'+ objAttach.Attachment_URL__c + '"target="_blank">'+ objAttach.Name + '</a>';
            if (string.isBlank(URLsDocument)) {
                URLsDocument = '<ul><li>' + documentlink + '</li></ul>';
            } else{
                if (URLsDocument.contains('</ul>')) {
                    URLsDocument = URLsDocument.replace('</ul>', '');
                }
                URLsDocument = URLsDocument +'<li>' + documentlink + '</li></ul>';
            }                                       
        }// end of for
        
        for (Case objCase : [Select Id
                                , Booking_Unit__c
                                , Booking_Unit__r.Registration_ID__c
                                , Unit_Name__c
                                , Lease_Commencement_Date__c
                                , Lease_End_Date__c
                                , Completed_Milestones_Percent__c
                                , Call_Outcome__c
                                , Account.Name
                                , Booking_Unit__r.RGS_Percent__c
                                , OwnerId
                                , Owner.Name
                                , AccountId
                              From Case
                              Where Id IN: lstCaseId]) {
            lhoCase = objCase;
            BuildingName = objCase.Unit_Name__c.substringBefore('/');                      
        }// end of for
        
        string HoEmail;
        if (!string.isBlank(BuildingName)) {
            for (Location__c objLocation : [Select id
                                                , Name
                                                , Handover_Team_notification_Email__c
                                             From Location__c
                                             Where Name =: BuildingName]) {
                 HoEmail = objLocation.Handover_Team_notification_Email__c;                        
            }         
        }
        String leaseEmails = Label.Lease_Management_Email;
        list<String> lstRecipients = leaseEmails.split(',');
        if (!string.isBlank(HoEmail)) {
            lstRecipients.add(HoEmail);
        }
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
         String mailMessage = '';
         OrgWideEmailAddress[] owea;
         EmailTemplate templateId; 
         try {
             templateId = [select id, Body, Subject, HtmlValue, name from EmailTemplate where developername = 'Executed_Agreement_for_Lease_Team'];
             owea = [select Id from OrgWideEmailAddress where Address = 'no-replysf@damacgroup.com'];
         }
         catch (Exception e) {
             system.debug('!!!e'+e);
         }
         
         if (templateId != null) {
             mailMessage = templateId.HtmlValue;
             mailMessage = mailMessage.replace('{!Case.Unit_Name__c}', lhoCase.Unit_Name__c);
             mailMessage = mailMessage.replace('{!Case.Account}', lhoCase.Account.Name);
             if (lhoCase.Booking_Unit__r.RGS_Percent__c != null) {
                 mailMessage = mailMessage.replace('{!Booking_Unit__c.RGS_Percent__c}', lhoCase.Booking_Unit__r.RGS_Percent__c);
             } else {
                 mailMessage = mailMessage.replace('{!Booking_Unit__c.RGS_Percent__c}', '');
             }             
             if (lhoCase.Call_Outcome__c != null) {
                 mailMessage = mailMessage.replace('{!Case.Call_Outcome__c}', lhoCase.Call_Outcome__c);
             } else {
                 mailMessage = mailMessage.replace('{!Case.Call_Outcome__c}', '');
             }
             if (!string.isBlank(URLsDocument)) {
                mailMessage = mailMessage.replace('{!Case.Rule_Engine_Response__c}', URLsDocument);
             } else {
                mailMessage = mailMessage.replace('{!Case.Rule_Engine_Response__c}', '');
             }
             if (lhoCase.Owner.Name != null) {
                 mailMessage = mailMessage.replace('{!Case.OwnerFullName}', lhoCase.Owner.Name);
             } else {
                 mailMessage = mailMessage.replace('{!Case.OwnerFullName}', '');
             }
             email.subject = templateId.Subject.replace('{!Case.Unit_Name__c}', lhoCase.Unit_Name__c);
         }
         email.setHtmlBody(mailMessage);
         system.debug('!!!!lstRecipients'+lstRecipients);
         email.toAddresses = lstRecipients;
         email.setWhatId(lhoCase.AccountId);
         email.setSaveAsActivity(true);
         if ( owea.size() > 0 ) {
                email.setOrgWideEmailAddressId(owea.get(0).Id);
            }
         try {
             Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
             return;
        }
        catch (EmailException e) {
            system.debug('!!!!!!!e'+e);
        }
    }
}