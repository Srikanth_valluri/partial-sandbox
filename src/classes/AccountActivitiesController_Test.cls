@isTest
public with sharing class AccountActivitiesController_Test {

    static testmethod void testAccountActivitiesInsertCompleted() {

        // Test Data
        User pcUser = InitialiseTestData.getPropertyConsultantUsers('userABC@test.com');
        insert pcUser;

        Account acc = InitialiseTestData.getCorporateAccount('TestAgency');
        insert acc ;

        List<Contact> conList = new List<Contact>();
        for (Integer i = 0; i < 5; i++) {
            Contact con = new Contact();
            con.LastName = 'Corporate Agency ' + i;
            con.AccountId = acc.Id;
            con.OwnerId = pcUser.Id;
            conList.add(con);
        }
        insert conList ;

        AccountActivitiesController accountActivity;

        // Test the functionality in user context
        System.runAs(pcUser) {
            System.Test.startTest();
            ApexPages.StandardController controller = new ApexPages.StandardController(acc);
            PageReference accountActiviesPage = Page.AccountActivities;
            Test.setCurrentPage(accountActiviesPage);
            ApexPages.currentPage().getParameters().put('accid', acc.Id);
            accountActivity = new AccountActivitiesController(controller);
            accountActivity.completedtaskObj.Activity_Type__c = 'Calls';
            accountActivity.completedtaskObj.End_Date__c = System.Today();
            accountActivity.completedtaskObj.Start_Date__c = System.Today();
            accountActivity.completedtaskObj.Activity_Sub_Type__c = null;
            accountActivity.completedtaskObj.Activity_Outcome__c='Email Delivered';
            accountActivity.saveActivity();
            System.Test.stopTest();
        }
    }

    static testmethod void testAccountActivitiesInsertFollowup() {

        // Test Data
        User pcUser = InitialiseTestData.getPropertyConsultantUsers('userABC@test.com');
        insert pcUser;

        Account acc = InitialiseTestData.getCorporateAccount('TestAgency');
        insert acc ;

        List<Contact> conList = new List<Contact>();
        for (Integer i = 0; i < 5; i++) {
            Contact con = new Contact();
            con.LastName = 'Corporate Agency ' + i;
            con.AccountId = acc.Id;
            con.OwnerId = pcUser.Id;
            conList.add(con);
        }
        insert conList ;

        AccountActivitiesController accountActivity;

        // Test the functionality in user context
        System.runAs(pcUser) {
            System.Test.startTest();
            ApexPages.StandardController controller = new ApexPages.StandardController(acc);
            PageReference accountActiviesPage = Page.AccountActivities;
            Test.setCurrentPage(accountActiviesPage);
            ApexPages.currentPage().getParameters().put('accid', acc.Id);
            accountActivity = new AccountActivitiesController(controller);
            accountActivity.completedtaskObj.Activity_Type__c = 'Calls';
            accountActivity.completedtaskObj.End_Date__c = System.Today();
            accountActivity.completedtaskObj.Start_Date__c = System.Today();
            accountActivity.completedtaskObj.Activity_Sub_Type__c = null;
            accountActivity.isFollowUp = true;
            accountActivity.scheduledtaskObj.Status = 'In Progress';
            accountActivity.scheduledtaskObj.Activity_Type__c = 'Calls';
            accountActivity.scheduledtaskObj.End_Date__c = System.Today();
            accountActivity.scheduledtaskObj.Start_Date__c = System.Today();
            accountActivity.scheduledtaskObj.Activity_Sub_Type__c = null;
            accountActivity.completedtaskObj.Activity_Outcome__c='Email Delivered';
            accountActivity.saveActivity();
            
            System.Test.stopTest();
        }
    }

    static testmethod void testAccountActivitiesUpdate() {

        // Test Data
        User pcUser = InitialiseTestData.getPropertyConsultantUsers('userABC@test.com');
        insert pcUser;

        Account acc = InitialiseTestData.getCorporateAccount('TestAgency');
        insert acc ;

        List<Contact> conList = new List<Contact>();
        for (Integer i = 0; i < 5; i++) {
            Contact con = new Contact();
            con.LastName = 'Corporate Agency ' + i;
            con.AccountId = acc.Id;
            con.OwnerId = pcUser.Id;
            conList.add(con);
        }
        insert conList ;

        Task taskObj = new Task();
        taskObj.WhatId = acc.Id;
        taskObj.ActivityDate = System.Today();
        taskObj.OwnerId = pcUser.Id;
        taskObj.Status = 'Completed';
        taskObj.Priority = 'Normal';
        taskObj.Activity_Type__c = 'Calls';
        taskObj.Activity_Sub_Type__c = '';
        taskObj.Start_Date__c = System.Today();
        taskObj.End_Date__c = System.Today();
        taskObj.Description = 'Hello';
        taskObj.Agency_Contacts_Ids__c = conList[0].Id;
        insert taskObj;

        AccountActivitiesController accountActivity;

        // Test the functionality in user context
        System.runAs(pcUser) {
            System.Test.startTest();
            ApexPages.StandardController controller = new ApexPages.StandardController(acc);
            PageReference accountActiviesPage = Page.AccountActivities;
            Test.setCurrentPage(accountActiviesPage);
            ApexPages.currentPage().getParameters().put('accid', acc.Id);
            ApexPages.currentPage().getParameters().put('taskid', taskObj.Id);
            accountActivity = new AccountActivitiesController(controller);
            accountActivity.completedtaskObj.Activity_Outcome__c='Email Delivered';
            accountActivity.updateActivity();

            Task taskObj1 = new Task();
            Boolean valid = accountActivity.validateActivity(taskObj1);
            System.Test.stopTest();
        }
    }

}