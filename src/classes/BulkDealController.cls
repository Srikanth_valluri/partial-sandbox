Global Class BulkDealController{
    
    public String srId{get;set;}
    public static String bulkDealId{get;set;}
    public String oldBulkDealId{get;set;}
    public String bulkDealName{get;set;}
    public String derSelect{get;set;}
    public String srSelect{get;set;}
     public String updateComments{get;set;}
    
    public String PreviousDealExceptions{get;set;}
    public String PreviousDealExceptionsDetails{get;set;}
    public String PreviousSRDetails{get;set;}
    public String selectedServicesRequest{get;set;}
    public String availableServicesRequest{get;set;}
    public String bulkDealComment{get;set;}
    
    public boolean requestExists {get;set;}
    public boolean IsDERActive{get;set;}
    
    public NSIBPM__Service_Request__c srRecord{get;set;}
    public Deal_Exception_Request__c bulkDealRecord{get;set;}
    
    public boolean allowAdd{get;set;}
     public Map<Id, Attachment> docAttachmentMap {get; set;}
    public Map<Id, Deal_Exception_Request__c > derMap{get;set;}
    public Map<String, Deal_Exception_Request__c > derNameMap{get;set;}
    public Map<Id, NSIBPM__Service_Request__c> srMap{get;set;}
    
    public List<String> clientBaseCountryList{get;set;}
    public List<Unit_Documents__c> listUnitDocuments {get; set;}

    public List<SelectOption> derSelectList{get;set;}
    public List<SelectOption> srSelectList{get;set;}
    
    public Set<String> derSelectedSet{get;set;}
    public Set<String> srSelectedSet{get;set;}
    public Set<String> ExsistingSRSet{get;set;}
    
    public void fetchBulkDeal(){
        bulkDealId = ApexPages.currentPage().getParameters().get('Id');
        bulkDealComment = '';
        updateComments = '';
        system.debug('bulkDealId1: ' +bulkDealId);
        if(bulkDealId == null || bulkDealId == ''){
            Deal_Exception_Request__c dealExpReq = new Deal_Exception_Request__c();
            dealExpReq.Status__c = 'Draft';
            Map<String, User> userNameMap = new Map<String, User>();
            User currentUser;
            for(User usr: [SELECT Id, Name,  DOS_Name__c, HOS_Name__c, HOD_Name__c,Sales_Office__c FROM User WHERE Id =: UserInfo.getUserId()]){
                currentUser = usr;
                userNameMap.put(usr.DOS_Name__c, null);
                userNameMap.put(usr.HOS_Name__c, null);
                userNameMap.put(usr.HOD_Name__c, null);
            }
            dealExpReq.RM__c = currentUser.Id;
            dealExpReq.Sales_Office__c = currentUser.Sales_Office__c;
            for(User usr: [SELECT Id, Name FROM User WHERE Name IN: userNameMap.keyset() AND IsActive = TRUE]){
                userNameMap.put(usr.Name, usr);
            }
            if(userNameMap.containsKey(currentUser.DOS_Name__c) && userNameMap.get(currentUser.DOS_Name__c) != null){
                dealExpReq.DOS__c = userNameMap.get(currentUser.DOS_Name__c).Id;
            }
            if(userNameMap.containsKey(currentUser.HOS_Name__c) && userNameMap.get(currentUser.HOS_Name__c) != null){
                dealExpReq.HOS__c = userNameMap.get(currentUser.HOS_Name__c).Id;
            }
            if(userNameMap.containsKey(currentUser.HOD_Name__c) && userNameMap.get(currentUser.HOD_Name__c) != null){
                dealExpReq.HOD__c = userNameMap.get(currentUser.HOD_Name__c).Id;
            }
            dealExpReq.recordTypeId = Schema.SObjectType.Deal_Exception_Request__c.getRecordTypeInfosByName().get('Bulk Deal').getRecordTypeId();
            insert dealExpReq;
            ApexPages.currentPage().getParameters().put('Id', dealExpReq.Id);
            init();
        }
    }
    
    public BulkDealController(){
        bulkDealId = ApexPages.currentPage().getParameters().get('Id');
        if(bulkDealId != null && bulkDealId != ''){
            init();
        }
        system.debug('bulkDealId3: ' +bulkDealId);
    }
    
    public void init(){
        bulkDealId = ApexPages.currentPage().getParameters().get('Id');
        system.debug('bulkDealId2: ' +bulkDealId);
        
            bulkDealRecord = [select Id, Name, Bulk_Deal_SR_History__c, Status__c, Bulk_Deal_Name__c, Bulk_Deal_Comments__c  
                               from Deal_Exception_Request__c where id =: bulkDealId limit 1];
        // oldBulkDealId = srRecord.Deal_Exception_Request__c;
        allowAdd = true;
        bulkDealName = '';
        requestExists = IsDERActive = false ;
        derSelect = srSelect = PreviousSRDetails = availableServicesRequest = '';
        listUnitDocuments = new List<Unit_Documents__c>();
        ExsistingSRSet = new Set<String>();
        derNameMap = new Map<String, Deal_Exception_Request__c >();
        srMap = new Map<Id, NSIBPM__Service_Request__c>();
        derSelectList = new List<SelectOption>();
        srSelectList = new List<SelectOption>();
        clientBaseCountryList = new List<String>();
        derSelectedSet = new Set<String>();
        srSelectedSet = new Set<String>();
        docAttachmentMap = new Map<Id, Attachment>();
        derSelectList.add(new SelectOption('--None--', '--None--'));
        srSelectList.add(new SelectOption('--None--', '--None--'));
        derMap = new Map<Id, Deal_Exception_Request__c >([select Id, Name, Inquiry__c from Deal_Exception_Request__c  where RecordType.Name = 'Deal Exception']);
        for(Deal_Exception_Request__c der : derMap.Values()){
            derSelectList.add(new SelectOption(der.id, der.Name));
            derNameMap.put(der.Name, der);
        }
        if(oldBulkDealId != null){
            allowAdd = false;
        }
        
        if(bulkDealId == null || bulkDealId == ''){
            requestExists = false;
            
        }else{
            requestExists = true;
            listUnitDocuments = getUnitDocuments();
            Unit_Documents__c unitDocNew = new Unit_Documents__c();
            unitDocNew.Deal_Exception_Request__c  = bulkDealId; 
            listUnitDocuments.add(unitDocNew);    
            for(Unit_Documents__c unitDoc: listUnitDocuments){
                docAttachmentMap.put(unitDoc.Sys_Doc_ID__c, null);
            }
            for(Attachment atch: [SELECT Id, Name, BodyLength FROM Attachment WHERE Id IN: docAttachmentMap.keyset()]){
                docAttachmentMap.put(atch.Id, atch);
            }
        }
        // Load already added SR Requests
        selectedServicesRequest  = PreviousDealExceptions = PreviousDealExceptionsDetails = '';
        if(bulkDealRecord.Bulk_Deal_SR_History__c != null){
            for(NSIBPM__Service_Request__c sr : [SELECT id, Name, Inventory__c,List_of_Units__c, 
                                                    NSIBPM__Internal_Status_Name__c,Bulk_Deal__c 
                                                    FROM NSIBPM__Service_Request__c where Bulk_Deal__c  =: bulkDealId]){
                ExsistingSRSet.add(sr.Name);
                PreviousSRDetails += sr.Name+','+sr.NSIBPM__Internal_Status_Name__c+','+sr.List_of_Units__c+';';
            }
            
            String[] history = bulkDealRecord.Bulk_Deal_SR_History__c.split('<br>');
            Map<id, Deal_Exception_Request__c> preDer = new Map<Id,Deal_Exception_Request__c>([SELECT id, Name, 
                                                        Inquiry__c, Status__c, List_of_Units__c 
                                                      FROM Deal_Exception_Request__c where Bulk_Deal__c =: bulkDealId ]);
            if(preDer.size() > 0){
                Set<Id> invSet = new Set<Id>();
                Set<Id> srSet = new Set<Id>();
                PreviousDealExceptionsDetails  = PreviousDealExceptions  = '';
                for(Deal_Exception_Request__c der : preDer.values()){
                    PreviousDealExceptionsDetails = PreviousDealExceptionsDetails +der.Name+','
                                                +der.Status__c+','+(der.List_of_Units__c != null ? der.List_of_Units__c:'No Units')
                                                +'<br/>';
                    PreviousDealExceptions = PreviousDealExceptions+der.Name +',';
                }
                PreviousDealExceptions = PreviousDealExceptions.removeEnd(',');
                PreviousDealExceptionsDetails = PreviousDealExceptionsDetails.removeEnd('<br/>');
                System.debug(PreviousDealExceptions );
                System.debug(PreviousDealExceptionsDetails );
                for(Deal_Exception_Unit__c deu : [SELECT id, Name, Inventory__c 
                                                  FROM Deal_Exception_Unit__c where Deal_Exception_Request__c IN: preDer.keyset()]){
                    invSet.add(deu.Inventory__c); 
                }
                for(Booking_Unit__c bu : [SELECT id, Name, Inventory__c, Booking__r.Deal_SR__c FROM Booking_Unit__c where Inventory__c IN: invSet]){
                    srSet.add(bu.Booking__r.Deal_SR__c);
                }
                for(NSIBPM__Service_Request__c sr: [SELECT id, Name, Inventory__c, List_of_Units__c, 
                                                        NSIBPM__Internal_Status_Name__c 
                                                    FROM NSIBPM__Service_Request__c where Id IN: srSet 
                                                            AND (NOT NSIBPM__Internal_Status_Name__c LIKE '%Rejected%') 
                                                            AND NSIBPM__Internal_Status_Name__c != 'Draft' AND RecordType.Name = 'Deal']){
                    availableServicesRequest = availableServicesRequest+sr.Name+',';
                }
                availableServicesRequest = availableServicesRequest.removeEnd(',');
            }
            for(String s : ExsistingSRSet){
                selectedServicesRequest = selectedServicesRequest +s+',';
            }
            selectedServicesRequest = selectedServicesRequest.removeEnd(',');
        }
    }
    
    public void getAvailableSR(){
        
        availableServicesRequest = '';
        String currentSelectedDER = ApexPages.currentPage().getParameters().get('param1');
        Map<Id, Deal_Exception_Request__c> tmpDerMap;
        if(currentSelectedDER.contains(',')){
            String[] selectedDER = currentSelectedDER.split(',');
            tmpDerMap = new Map<Id, Deal_Exception_Request__c>([SELECT id, Name, Inquiry__c, Status__c, List_of_Units__c 
                                                 FROM Deal_Exception_Request__c where Name IN: selectedDER]);
        }else{
            tmpDerMap = new Map<Id, Deal_Exception_Request__c>([SELECT id, Name, Inquiry__c, Status__c, List_of_Units__c 
                                                 FROM Deal_Exception_Request__c where Name =: currentSelectedDER]);
        }
        String[] selectedDER = currentSelectedDER.split(',');
        tmpDerMap = new Map<Id, Deal_Exception_Request__c>([SELECT id, Name, Inquiry__c, Status__c, List_of_Units__c 
                                                 FROM Deal_Exception_Request__c where Name IN: selectedDER]);
        if(tmpDerMap.size() > 0){
            Set<Id> invSet = new Set<Id>();
            Set<Id> srSet = new Set<Id>();
            for(Deal_Exception_Unit__c deu : [SELECT id, Name, Inventory__c 
                                                    FROM Deal_Exception_Unit__c where Deal_Exception_Request__c IN: tmpDerMap.keyset()]){
                invSet.add(deu.Inventory__c); 
            }
            for(Booking_Unit__c bu : [SELECT id, Name, Inventory__c, Booking__r.Deal_SR__c FROM Booking_Unit__c where Inventory__c IN: invSet]){
                srSet.add(bu.Booking__r.Deal_SR__c);
            }
            for(NSIBPM__Service_Request__c sr: [SELECT id, Name, Inventory__c,List_of_Units__c, 
                                                     NSIBPM__Internal_Status_Name__c 
                                                    FROM NSIBPM__Service_Request__c where Id IN: srSet 
                                                        AND (NOT NSIBPM__Internal_Status_Name__c LIKE '%Rejected%') 
                                                        AND NSIBPM__Internal_Status_Name__c != 'Draft' AND RecordType.Name = 'Deal']){
                availableServicesRequest = availableServicesRequest+sr.Name+',';
            }
            availableServicesRequest = availableServicesRequest.removeEnd(',');
        }
        System.debug(availableServicesRequest);
    }
    // Query UNIT Documents
    public List<Unit_Documents__c> getUnitDocuments(){
        system.debug('bulkDealId: ' + bulkDealId);
        bulkDealId = bulkDealRecord.Id;
        system.debug('bulkDealId4: ' + bulkDealId);
        List<Unit_Documents__c> unitDocs = new List<Unit_Documents__c>();
        if(bulkDealId != null){
            String docQuery = 'SELECT ' + getAllFields ('Unit_Documents__c') 
                                + ' FROM Unit_Documents__c WHERE Deal_Exception_Request__c =: bulkDealId';
            system.debug('docQuery: ' + docQuery);
            unitDocs = Database.query (docQuery);
        }
       // system.debug('unitDocs: ' + unitDocs);
       // system.debug('unitDocs3: ' + unitDocs.size());
        return unitDocs;
    }
    
        
     public PageReference refreshUnitDocumentList(){
        system.debug('bulkDealRecord: ' + bulkDealRecord);
        listUnitDocuments = getUnitDocuments();
        Unit_Documents__c unitDoc = new Unit_Documents__c();
        unitDoc.Deal_Exception_Request__c  = bulkDealId;
        listUnitDocuments.add(unitDoc);    
        for(Unit_Documents__c unitDoc1: listUnitDocuments){
            docAttachmentMap.put(unitDoc1.Sys_Doc_ID__c, null);
        }
        for(Attachment atch: [SELECT Id, Name, BodyLength FROM Attachment WHERE Id IN: docAttachmentMap.keyset()]){
            docAttachmentMap.put(atch.Id, atch);
        }
        system.debug('docAttachmentMap: ' + docAttachmentMap.size());
        system.debug('listUnitDocuments: ' + listUnitDocuments.size());
        return null;
    }
    
    public void updateUnitDocumentList(){
        listUnitDocuments = getUnitDocuments();
        for(Unit_Documents__c doc: listUnitDocuments){
            docAttachmentMap.put(doc.Sys_Doc_ID__c, null);
        }
        //system.debug('docAttachmentMap1: ' + docAttachmentMap);
        for(Attachment atch: [SELECT Id, Name, BodyLength FROM Attachment WHERE Id IN: docAttachmentMap.keyset()]){
            docAttachmentMap.put(atch.Id, atch);
        }
    }
    
    public void addSR(){
       if(srSelect !=  '--None--' && !srSelectedSet.contains(srMap.get(srSelect).Name)){
            srSelectedSet.add(srMap.get(srSelect).Name);
            srSelect = '';
        }
    }
    public void removeSR(){
        if(srSelect !=  '--None--' && srSelectedSet.contains(srMap.get(srSelect).Name)){
            srSelectedSet.remove(srMap.get(srSelect).Name);
            srSelect = '';
        }
    }
    public void addDER(){
        if(derSelect !=  '--None--' && !derSelectedSet.contains(derMap.get(derSelect).Name)){
            derSelectedSet.add(derMap.get(derSelect).Name);
            derSelect = '';
        }
        getServicesRequests();
    }
    public void removeDER(){
        if(derSelect !=  '--None--' && derSelectedSet.contains(derMap.get(derSelect).Name)){
            derSelectedSet.remove(derMap.get(derSelect).Name);
            derSelect = '';
        }
        getServicesRequests();
    }
    public void getServicesRequests(){
        Set<String> inqSet = new Set<String>();
        for(String s : derSelectedSet){
            if(derNameMap.get(s).Inquiry__c != null)
                inqSet.add(derNameMap.get(s).Inquiry__c);
        }
        srMap = new Map<Id, NSIBPM__Service_Request__c >([select id,Name from NSIBPM__Service_Request__c  where Inquiry__c IN: inqSet]);
        if(srMap.size() > 0){
            srSelectList.clear();
            srSelectList.add(new SelectOption('--None--', '--None--'));
            for(NSIBPM__Service_Request__c sr : srMap.Values()){
                srSelectList.add(new SelectOption(sr.id, sr.Name));
            }
        }
    } 
    
    public PageReference addNewBulkDeal(){
        String msg;
            try{
                bulkDealRecord.RecordTypeId = Schema.SObjectType.Deal_Exception_Request__c.getRecordTypeInfosByName().get('Bulk Deal').getRecordTypeId();
                upsert bulkDealRecord;
                ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.CONFIRM, 'New Deal Created...'));
                String reqURL = System.URL.getSalesforceBaseUrl().toExternalForm() 
                        + '/apex/Bulk_Deal2?id=' + bulkDealRecord.Id;
                 PageReference bulkDealPage = new PageReference(reqURL);
                bulkDealPage.setRedirect(true);
                return bulkDealPage;
            }Catch(Exception e){
                ApexPages.addMessage(new ApexPages.message( ApexPages.Severity.ERROR, e.getMessage()));
                return null;
            }        
    }
    
    /* To Get all the Fields of an Object - used in Query String */
    public static string getAllFields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);

        if (objectType == null) {
            return fields;
        }

        Map <String, Schema.SObjectField> fieldMap = objectType.getDescribe ().fields.getMap ();
        for (Schema.SObjectField sfield : fieldMap.Values()) {
                fields += sfield.getDescribe().getName ()+ ', ';
        }
        return fields.removeEnd(', ');
    }
    
    public Pagereference removeServiceRequests(){
        system.debug('bulkDealRecord.Id: ' + bulkDealRecord.Id);
        List<NSIBPM__Service_Request__c> removeSRList = new List<NSIBPM__Service_Request__c>();
        String history = '<br/>';
        for(NSIBPM__Service_Request__c sr: [SELECT Id, Name, Bulk_Deal__c 
                                            FROM NSIBPM__Service_Request__c 
                                            WHERE Bulk_Deal__c =: bulkDealRecord.Id]){
            
            sr.Bulk_Deal__c = null;
            removeSRList.add(sr);
            history = history + sr.Name + ' is Removed by ' + UserInfo.getName() + ' on ' + System.Now() + '<br/>';    
        }
        if(history != '<br/>'){
            bulkDealRecord.Bulk_Deal_SR_History__c += '\n\n' + history ;
            
        }
        if(bulkDealRecord.Bulk_Deal_Comments__c != null && bulkDealRecord.Bulk_Deal_Comments__c != ''){
            bulkDealRecord.Bulk_Deal_Comments__c += '\n\n' + System.Now()  + ': ' + UserInfo.getName() + ' - ' +updateComments;
        } else{
            bulkDealRecord.Bulk_Deal_Comments__c += System.Now()  + ': ' + UserInfo.getName() + ' - ' +updateComments;
        }
        update bulkDealRecord;
        system.debug('removeSRList: ' + removeSRList);
        if(removeSRList.size() > 0){
            update removeSRList;
        }
        String reqURL = '/' + bulkDealRecord.Id;
        PageReference bulkDealPage = new PageReference(reqURL);
        bulkDealPage.setRedirect(true);
        return bulkDealPage;
    }
    
    public Pagereference saveBulkRequest(){
        List<Deal_Exception_Request__c> updateDERList = new List<Deal_Exception_Request__c>();
        String currentSelectedDER = ApexPages.currentPage().getParameters().get('param2');
        String currentSelectedSR = ApexPages.currentPage().getParameters().get('param1');
        
        Set<String> addedSRNames = new Set<String>();
        Set<String> removedSRNames = new Set<String>();
        System.debug('currentSelectedDER: ' + currentSelectedDER);
        System.debug('currentSelectedSR : ' + currentSelectedSR );
        System.debug('updateComments: ' + updateComments);
         System.debug('bulkDealRecord.Bulk_Deal_Comments__c: ' + bulkDealRecord.Bulk_Deal_Comments__c);
        if(bulkDealRecord.status__c == 'Submitted'){
            if(updateComments != null && updateComments != ''){
                updateComments = System.Now()  + ': ' + UserInfo.getName() + ' - ' +updateComments;
                if(bulkDealRecord.Bulk_Deal_Comments__c != null && bulkDealRecord.Bulk_Deal_Comments__c != ''){
                    updateComments = bulkDealRecord.Bulk_Deal_Comments__c + '\n\n' + updateComments;
                } 
                bulkDealRecord.Bulk_Deal_Comments__c = updateComments;   
            }
        }
        System.debug('bulkDealRecord.Bulk_Deal_Comments__c2: ' + bulkDealRecord.Bulk_Deal_Comments__c);
        if(bulkDealRecord.Bulk_Deal_SR_History__c == null || bulkDealRecord.Bulk_Deal_SR_History__c == ''){
            String history = '';
            String[] tmp = currentSelectedSR.split(',');
            if(currentSelectedDER != ''){
                String[] ders = currentSelectedDER.split(',');
                for(String s : ders ){
                    IF(s != ''){
                        history = history +s+' is Added by '+UserInfo.getName()+' on '+System.Now()+'<br/>';
                    }
                }
                for(Deal_Exception_Request__c tmpDer: [select Id, Name, Bulk_Deal__c from Deal_Exception_Request__c where Name IN: ders]){
                    tmpDer.Bulk_Deal__c = bulkDealRecord.id;
                    updateDERList.add(tmpDer);
                }
            }
            for(String s : tmp){
                IF(s != ''){
                    history = history +s+' is Added by '+UserInfo.getName()+' on '+System.Now()+'<br/>';
                    addedSRNames.add(s);
                }
            }
            bulkDealRecord.Bulk_Deal_SR_History__c = history ;
            bulkDealRecord.Status__c = 'Submitted';
            updateDERList.add(bulkDealRecord);
            
            update updateDERList;
            List<NSIBPM__Service_Request__c> srList = new List<NSIBPM__Service_Request__c>();
            for(NSIBPM__Service_Request__c  sr : [SELECT id, Name, Inventory__c, Deal_Exception_Request__c 
                                                    FROM NSIBPM__Service_Request__c where Name IN: addedSRNames]){
                sr.Bulk_Deal__c = bulkDealRecord.id;
                srList.add(sr);
            }
            if(srList.size()  > 0)
                Update srList;
            String reqURL = '/' + bulkDealRecord.Id;
             PageReference bulkDealPage = new PageReference(reqURL);
            bulkDealPage.setRedirect(true);
            return bulkDealPage;
        }else{
            String history = bulkDealRecord.Bulk_Deal_SR_History__c+'<br/>';
            String[] selectedDER = currentSelectedDER.split(',');
            String[] existingDER = PreviousDealExceptions.split(',');
            String[] derNamesList = new String[]{};
            if(currentSelectedDER != '')
                derNamesList.addAll(selectedDER );
            if(PreviousDealExceptions != '')
                derNamesList.addAll(existingDER);
                
            Map<String, Deal_Exception_Request__c > derMap = new Map<String, Deal_Exception_Request__c>();
            for(Deal_Exception_Request__c der : [select Id, Name, Bulk_Deal__c from Deal_Exception_Request__c where Name IN: derNamesList]){
                derMap.put(der.Name,der);
            }
            
            if(PreviousDealExceptions != ''){
                for(String derName : existingDER){
                    if(!currentSelectedDER.contains(derName)){
                        history = history +derName +' is Removed by '+UserInfo.getName()+' on '+System.Now()+'<br/>';
                        Deal_Exception_Request__c tmp = new Deal_Exception_Request__c();
                        tmp.Id = derMap.get(derName).id;
                        tmp.Bulk_Deal__c = null;
                        updateDERList.add(tmp);
                    }
                }
            }
            if(currentSelectedDER != ''){
                for(String derName : selectedDER){
                    if(!PreviousDealExceptions.contains(derName)){
                        history = history +derName +' is Added by '+UserInfo.getName()+' on '+System.Now()+'<br/>';
                        Deal_Exception_Request__c tmp = new Deal_Exception_Request__c();
                        tmp.Id = derMap.get(derName ).id;
                        tmp.Bulk_Deal__c = bulkDealRecord.id;
                        updateDERList.add(tmp);
                    }
                }
            }
            
            String[] tmp = currentSelectedSR.split(',');
            for(String s : ExsistingSRSet){
                if(s != '' && !tmp.contains(s)){
                    history = history + s +' is Removed by '+UserInfo.getName()+' on '+System.Now()+'<br/>';
                    removedSRNames.add(s);
                }
            }
            for(String s : tmp){
                IF(s != '' && !ExsistingSRSet.contains(s)){
                    history = history + s +' is Added by '+UserInfo.getName()+' on '+System.Now()+'<br/>';
                    addedSRNames.add(s);
                }
            }
            bulkDealRecord.Bulk_Deal_SR_History__c = history ;
            bulkDealRecord.Status__c = 'Submitted';
            
            updateDERList.add(bulkDealRecord);
            
            update updateDERList;
            
            List<NSIBPM__Service_Request__c> srList = new List<NSIBPM__Service_Request__c>();
            for(NSIBPM__Service_Request__c  sr : [SELECT id, Name, Inventory__c, Deal_Exception_Request__c
                                                     FROM NSIBPM__Service_Request__c where Name IN: addedSRNames OR Name IN: removedSRNames]){
                if(addedSRNames.contains(sr.Name)){
                    sr.Bulk_Deal__c = bulkDealRecord.id;
                }
                
                if(removedSRNames.contains(sr.Name)){
                    sr.Bulk_Deal__c = null;
                }
                srList.add(sr);
            }
            if(srList.size()  > 0)
                Update srList;
            String reqURL = '/' + bulkDealRecord.Id;
            PageReference bulkDealPage = new PageReference(reqURL);
            bulkDealPage.setRedirect(true);
            return bulkDealPage;
        }
    }


    public PageReference Cancel(){
        return new Pagereference('/'+srId);
    }
    
      @RemoteAction
      public static String insertUnitDocument(Id requestId,  String docName) {
       // system.debug('requestId----'+requestId);
        if(requestId != null) {
            Unit_Documents__c objUnitDocument = new Unit_Documents__c();
            objUnitDocument.Deal_Exception_Request__c = requestId; 
            insert objUnitDocument;
            return String.valueOf(objUnitDocument.Id);
        } else {
            return 'Error with request Id';
        }
    }
    
      @RemoteAction
      public static String doUploadAttachment(Id unitDocId,  String attachmentId,  String description) {
       // system.debug('unitDocId----'+unitDocId);
        if(unitDocId != null) {
            if(attachmentId == '{}'){
                attachmentId = NULL; 
              system.debug('attachmentId1: ' + attachmentId);   
            }
            if(unitDocId.getSObjectType().getDescribe().getName() == 'Unit_Documents__c') {
                Unit_Documents__c objUnitDocument = new Unit_Documents__c();
                List<String> listAllUnitDocFields = new List<String>();
                listAllUnitDocFields.addAll(DocumentHelper.getAllFieldsFromObject('Unit_Documents__c'));
                String query =  ' SELECT ' +    String.join( listAllUnitDocFields, ',' ) +
                    ' FROM Unit_Documents__c' +
                    ' WHERE ' + ' id = :unitDocId'; 
                system.debug('query----'+query);
                
                objUnitDocument = Database.query(query);

                if(unitDocId != null) {
                    try{
                        objUnitDocument.Status__c = 'Uploaded';
                        objUnitDocument.Customer_Comments__c = description;
                        objUnitDocument.Sys_Doc_Id__c = attachmentId;
                        upsert objUnitDocument;
                    }catch(Exception e){
                        system.debug('>>>>Exception'+e);
                    }
                    return attachmentId;
                } else {
                    return 'Document could not be found';
                }
            } else {
                return 'Document could not be found';
            }
        } else {
            return 'Document Id is null';
        }
    }
}