// Class to raise an appointment from Booking unit details page
public without sharing class RaiseAnAppointmentBookingUnitCntrl extends RaiseAnAppointmentAccountCntrl{
    
    private String strBUId;
    public Booking_Unit__c objBooking_Unit {get;set;}
    
    public RaiseAnAppointmentBookingUnitCntrl( ApexPages.standardController controller ){
        strBUId = ApexPages.currentPage().getParameters().get('id');
        if( String.isNotBlank( strBUId ) ) {
            objBooking_Unit = [  SELECT Id,Name,Property_Name__c,Inventory__c,Booking__r.Account__c
                                      , Booking__c,Manager_Name__c, HOS_Name__c
                                      , Property_Consultant__c,Unit_Name__c,CurrencyIsoCode
                                      , Registration_ID__c,Property_City__c,District__c
                                      , Construction_Status__c,Property_Country__c
                                   FROM Booking_Unit__c
                                  WHERE Id = :strBUId ];
            strAccId = String.valueOf( objBooking_Unit.Booking__r.Account__c);
            strSelectedBookingUnits = objBooking_Unit.Id;
            init();
        }
    } //End constructor
    
   
}