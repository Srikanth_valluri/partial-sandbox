@RestResource(urlMapping='/ReraPercentUpd/*')
global without sharing class ReraPercentUpd
{                          //*****AKISHOR: 7Jan2020 for updating Rera percent*************//    
 @HttpPatch
    global static ResponseWrapper ReraPercentUpd(String BuildingCode, 
                                String CurrentACDDate) 
    {
       //List<String> lstBuilingCode = new List<String>();    
       //tBuilingCode = String.isNotBlank(BuilingCode)?BuilingCode.split(','):NULL;

       List <Location__c> ClList = new List<Location__c>();   
       ResponseWrapper objResponseWrapper = new ResponseWrapper();
       
        if(BuildingCode != NULL ){
             
       //List<Property__c> FinallList =[Select Id, Property_Code__c ,Name from Property__c where Property_Code__c=:BuilingCode limit 1];// IN: lstBuilingCode];
       //List<Location__c> LocList = [Select Id, Location_Code__c,Property_Name__c from Location__c where Location_Code__c=:BuilingCode];
        //List<Property__c> FinallList =[Select Id, Property_Code__c ,Name from Property__c where Property_Code__c IN: lstBuilingCode];
        List<Location__c> LocId=[Select Id,Property_Name__c from Location__c where 
                                 Location_Code__c=:BuildingCode limit 1];
        //List<Property__c> FinallList =[Select Id, Name from Property__c where Id=:LocId[0].Property_Name__c limit 1];// IN: lstBuilingCode];
        //List<RERA_Percent__c> ReraPercent = new List<RERA_Percent__c>();
            for (Location__c plist : LocId){
                      //plist.RERA_Completion_Bot__c=Percent;
                      plist.Current_ACD__c=date.valueOf(CurrentACDDate);
                      //plist.ActuaPercent_Connect__c=Percent;
                      //plist.OriginalACD_Connect__c = date.valueOf(OriginalACD);
                      //plist.ProjectNum_Connect__c = ProjNum;
                      //plist.Rera_Inspection_Dt_Connect__c = date.valueOf(ReraInspecDate);
                      //plist.PlannedPerc_Connect__c=PlannedPerc;
                      //plist.LastRERAPerc_Connect__c=LastRERAPerc;
                      ClList.add(plist);
                }
        
       system.debug('list'+ClList);           
         //if(ClList.size() > 0){
                    try{
                    update ClList;
                    /*RERA_Percent__c ReraP = new RERA_Percent__c();
                    ReraP.ACD_Connect__c=date.valueOf(ACDDate);ReraP.Property__c=FinallList[0].Id;
                    ReraP.ActuaPercent_Connect__c=Percent; ReraP.Location__c=LocId[0].Id;
                    ReraP.LastRERAPerc_Connect__c=LastRERAPerc;ReraP.OriginalACD_Connect__c=date.valueOf(OriginalACD); ReraP.PlannedPerc_Connect__c = PlannedPerc;
                    ReraP.Rera_Inspection_Dt_Connect__c=date.valueOf(ReraInspecDate);
                    ReraP.Source__c='Connect';ReraPercent.add(ReraP);if(ReraPercent.size() > 0){Insert ReraPercent;}*/
                    objResponseWrapper.status = 'Details Updated Sucessfully';
                    objResponseWrapper.statusCode = '200';
                } 
                catch( Exception ex ) {
                    system.debug( ex.getMessage() );
                    objResponseWrapper.errorMessage = ex.getMessage();
                    objResponseWrapper.statusCode = '400';
                }   
            //}
   }
       return objResponseWrapper;

}
    //for sending response    
    global class ResponseWrapper {
        global String status;
        global String statusCode;
        global String errorMessage;
       
        global ResponseWrapper() {
            this.status = '';
            this.statusCode = '';
            this.errorMessage = '';
        }
    }

}