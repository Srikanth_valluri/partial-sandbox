/*
    Apex Class : Damac_WeChatConsoleController 
    Test Class Name :Damac_WeChatConsoleController_Test
    Functionality : To display all wechat inquiries in one place to send/receive message and to update related inquiry
*/

public class Damac_WeChatConsoleController {
    public Boolean createActivity{get;set;}
    public Boolean updateInq{get;set;}
    public String selectedInq {get;set;}
    public Task tObj {get;set;}
    public List <Inquiry__c> wechatInquiries { get; set; } // TO store the inquires owned by logged in user
    public Inquiry__c currentInquiry { get; set; }
    public String msgType { get; set; }
    public String message { get; set; }
    public String propertyMsval { get; set; }
    public Map <Integer, String> propertyDropDowns { get; set; }
    public Map <String, Integer> propertyDropDownsRev { get; set; }
    public Map <String, Boolean> newNotificationMap { get; set; }
    public Damac_WeChatConsoleController () {
        updateInq = true;
        tObj = new Task();
        wechatInquiries = new List <Inquiry__c> ();
        currentInquiry = new Inquiry__c ();
        msgType = '';
        message = '';
        propertyMsval = '';
        newNotificationMap = new Map <String, Boolean> ();
        getPropertyValues ();     
        wechatInquiriesDisplay ();        
        
    }
    //to get the Property picklist values from Inquiry object Property_MS__c field
    public void getPropertyValues () {
        propertyDropDowns = new Map <Integer, String> ();
        propertyDropDownsRev = new Map <String, Integer> ();
        Schema.DescribeFieldResult fieldResult = Inquiry__c.Property_MS__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues(); 
        Integer i = 0;       
        for( Schema.PicklistEntry f : ple)
        {
            propertyDropDowns.put (i, f.getValue ());
            propertyDropDownsRev.put (f.getValue (), i);
            i = i + 1;
        }       
    }
    // To update the current inquiry when salesforce user submits the updated inquiry details
    public void updateCurrentInquiry () {
        clearValues ();
        try {
            System.Debug ('==='+propertyMsval);
            String finalVal = '';
            if (propertyMsval != '' && propertyMsval != NULL) {
                if (propertyMSVal.contains (';')) {
                    for (String val :propertyMsval.split (';')) {
                        if (val != '')
                            finalVal += propertyDropDowns.get (Integer.valueOf (val))+';';
                    }
                } else {
                    finalVal = propertyDropDowns.get(Integer.valueOf (propertyMsVal));
                }
            }
            
            finalVal = finalVal.removeEnd (';');
            
            currentInquiry.Property_Ms__c = finalVal;
            update currentInquiry;
            msgType = 'success';
            message = 'Inquiry Details updated.';
        }
        catch (Exception e) {
            msgType = 'error';
            message = e.getMessage();
        }
    }
    // To clear the notification messages on the screen
    public void clearValues () {
        msgType = '';
        message = '';
    }
    // To display the notification icon when any new message is received from wechat in last 15 seconds
    public void checkforNewNotifications () {
    
        for (Wechat_Deliverability__c rec: [SELECT Name, Wechat_Message__r.Record_Id__c 
                                            FROM Wechat_Deliverability__c 
                                            WHERE SystemModstamp >:Datetime.now().addSeconds(-15)])
        {
            newNotificationMap.put (rec.Wechat_Message__r.Record_Id__c, true);
        }
    }
    // To display the existing Property values on the right side section of Inquiry details
    public void assignPropertyMS (String val) {
        if (val != '' && val != NULL) {
            if (val.contains (';')) {
                for (string i:val.split (';')) {
                    propertyMsval += propertyDropDownsRev.get (i)+';';
                }    
            } else {
                propertyMsVal = propertyDropDownsRev.get (val)+'';
            }
        }
    }
    // To display the wechat inquiries which are owned by current logged in user
    public void wechatInquiriesDisplay () {
        newNotificationMap = new Map <String, Boolean> ();
        ID loggedInUserId = UserInfo.getUserID ();
        wechatInquiries = Database.query ('SELECT '+getAllFields ('inquiry__c')+' FROM Inquiry__c WHERE OwnerId =: loggedInUserId AND WeChat_Id__c != NULL order By LastModifiedDate Desc');
            
        for (Inquiry__c inq : wechatInquiries) {
            newNotificationMap.put (inq.Id, false);
        }
        if (wechatInquiries.size () > 0 && currentInquiry.id == NULL) {
            Apexpages.currentpage().getparameters().put ('inqId', wechatInquiries[0].id);
            currentInquiry = wechatInquiries[0];
            
            assignPropertyMS (currentInquiry.Property_MS__c);
            
        }
        checkforNewNotifications ();
    }
    // Utility method to get all fields related to object
    public static string getAllFields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        if (objectType == null)
            return fields;
        Map <String, Schema.SObjectField> fieldMap = objectType.getDescribe ().fields.getMap ();
        for (Schema.SObjectField sfield : fieldMap.Values()) {
            fields += sfield.getDescribe().getName ()+ ', ';
        }
        return fields.removeEnd(', '); 
    }
    // To get the inquiry details When user clicks on any other inquiry in the console.
    public void checkCurrentInquiry () {
        propertyMsval = '';
        String inqId = apexpages.currentpage().getparameters().get('inqId');
        currentInquiry = Database.query ('SELECT '+getAllFields ('inquiry__c')+' FROM Inquiry__c WHERE WeChat_Id__c != NULL AND ID =:inqId');
        assignPropertyMS (currentInquiry.Property_MS__c);
    }
    // To create an activity related to selected inquiry
    public void insertRelatedActivity () {
            System.debug('>>>tObj'+tObj);
            
        try{
            insert tObj;
            message = 'Activity Created Successfully';
            System.debug('>>>tObj'+tObj);
            createActivity = false;
            updateInq = true;
        }catch(Exception e){
            System.debug('>>>Exception'+e);
            message = 'Error creating activity '+e.getMessage();
        }
    }
    public void createRelatedActivity () {
        System.debug('>>>currentInquiry'+currentInquiry);
        System.debug('>>>selectedInq'+selectedInq);
        createActivity = true;
        updateInq = false;
        try{
            tObj = new Task();
            tObj.Status = 'Open';
            tObj.Subject = 'Wechat activity';
            tObj.Priority = 'Normal';
            if(String.isNotBlank(selectedInq)){
                tObj.WhatId = selectedInq;
            }else{
                tObj.WhatId = currentInquiry.id;
            }
            System.debug('>>>tObj'+tObj);
        }catch(Exception e){
            System.debug('>>>Exception'+e);
        }
    }
    public void cancelInsertActivity(){
        createActivity = false;
        updateInq = true;
    }
}