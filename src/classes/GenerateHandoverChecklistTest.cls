/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GenerateHandoverChecklistTest {

    @istest static void TestHandoverCase(){
       
        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        Location__c objLoc = new Location__c();
        objLoc.Location_Code__c = 'VRD';
        objLoc.Location_ID__c ='23123';
        objLoc.New_Ho_Doc_Type__c = 'Unit';
        insert objLoc;

        Inventory__c objInv = new Inventory__c();
        objInv.Building_Location__c = objLoc.id;
        insert objInv;

        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;

        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;
        BU.Property_Name__c = 'Akoya';
        BU.Unit_Type__c = 'Villa';
        BU.Property_Country__c = 'United Arab Emirates';
        BU.Booking__c = Booking.Id;
        BU.Inventory__c = objInv.Id;
        insert BU;
        System.assert(BU != null);      
        
        Case pCas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        pCas.Booking_Unit__c = BU.Id ;
        pCas.Type = 'NOCVisa';
        pCas.Document_Verified__c = true;
        pCas.Total_Snags_Closed__c = 3;
        insert pCas;
        
        Case Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = BU.Id ;
        Cas.Type = 'NOCVisa';
        Cas.ParentId = pCas.Id;
        insert Cas;

        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c( name='HO Letter Unit',Drawloop_Document_Package_Id__c = 'a0V0E000003CTjT', Delivery_Option_Id__c = 'a0T0E000006rNbm');
        System.assert(Cas != null);
        
        ApexPages.currentPage().getParameters().put('id',Cas.Id);
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,HandoverChecklist1.HandOverCheckListResponse_element>();
        HandoverChecklist1.HandOverCheckListResponse_element response1 = new HandoverChecklist1.HandOverCheckListResponse_element();
        response1.return_x =    '{'+
        ' "data" : ['+
        ' {'+
        '   "P_PARAM_ID" : "123",'+
        '   "P_PROC_STATUS" : "S",'+
        '   "P_PROC_MESSAGE" : "Message",'+
        '   "URL" : "www.google.com",'+
        '   "P_ATTRIBUTE1" : "www.yahoo.com",'+
        '   "P_ATTRIBUTE2" : "123",'+
        '   "P_ATTRIBUTE3" : "321"'+
        '   '+
        '   '+
        '    '+
        ' }'+
        ' ],'+
        '  '+
        '  "message":"Message",'+
        '  "status":"S"'+
        '  '+
        '}';
       
       
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        PageReference pageRef = Page.GenerateHC;
        pageRef.getParameters().put('id', String.valueOf(Cas.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(Cas);
        GenerateHandoverChecklist controller = new GenerateHandoverChecklist(sc);  
        System.debug('---Hi--44--'); 
        pageRef = controller.init(); 
        Test.stopTest();
    }
    
    @istest static void TestMEthod2(){
       
        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();

        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);      
        
        Case pCas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        pCas.Booking_Unit__c = BU.Id ;
        pCas.Type = 'NOCVisa';
        pCas.Document_Verified__c = true;
        pCas.Payment_Verified__c = true;
        insert pCas;
        
        Case Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = BU.Id ;
        Cas.Type = 'NOCVisa';
        //Cas.Payment_Verified__c = true;
        Cas.ParentId = pCas.Id;
        insert Cas;
        
        SR_Attachments__c objSR = new SR_Attachments__c();
        objSR.Name = '1020 Signed Letter of Discharge 1025';
        objSR.Case__c = Cas.Id;
        objSR.isValid__c = true;
        objSR.Attachment_URL__c = 'ww.google.com';
        insert objSR;
        System.assert(Cas != null);
        
        ApexPages.currentPage().getParameters().put('id',Cas.Id);
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,HandoverChecklist1.HandOverCheckListResponse_element>();
        HandoverChecklist1.HandOverCheckListResponse_element response1 = new HandoverChecklist1.HandOverCheckListResponse_element();
        response1.return_x =    '{'+
        ' "data" : ['+
        ' {'+
        '   "P_PARAM_ID" : "123",'+
        '   "P_PROC_STATUS" : "S",'+
        '   "P_PROC_MESSAGE" : "Message",'+
        '   "URL" : "www.google.com",'+
        '   "P_ATTRIBUTE1" : "www.yahoo.com",'+
        '   "P_ATTRIBUTE2" : "123",'+
        '   "P_ATTRIBUTE3" : "321"'+
        '   '+
        '   '+
        '    '+
        ' }'+
        ' ],'+
        '  '+
        '  "message":"Message",'+
        '  "status":"S"'+
        '  '+
        '}';
       
       
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
 
        PageReference pageRef = Page.GenerateHC;
        pageRef.getParameters().put('id', String.valueOf(Cas.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(Cas);
        GenerateHandoverChecklist controller = new GenerateHandoverChecklist(sc);  
        System.debug('---Hi--44--'); 
        pageRef = controller.init(); 
        controller.HCurl = '';
        Test.stopTest();
     
    }
    
    @istest static void tryOnParent(){
       
        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();

        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);      
        
        
        Case Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = BU.Id ;
        Cas.Type = 'NOCVisa';
        Cas.Payment_Verified__c = true;
        insert Cas;
        
        SR_Attachments__c objSR = new SR_Attachments__c();
        objSR.Name = '1020 Signed Letter of Discharge 1025';
        objSR.Case__c = Cas.Id;
        objSR.isValid__c = true;
        objSR.Attachment_URL__c = 'ww.google.com';
        insert objSR;
        System.assert(Cas != null);
        
        ApexPages.currentPage().getParameters().put('id',Cas.Id);
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,HandoverChecklist1.HandOverCheckListResponse_element>();
        HandoverChecklist1.HandOverCheckListResponse_element response1 = new HandoverChecklist1.HandOverCheckListResponse_element();
        response1.return_x =    '{'+
        ' "data" : ['+
        ' {'+
        '   "P_PARAM_ID" : "123",'+
        '   "P_PROC_STATUS" : "S",'+
        '   "P_PROC_MESSAGE" : "Message",'+
        '   "URL" : "www.google.com",'+
        '   "P_ATTRIBUTE1" : "www.yahoo.com",'+
        '   "P_ATTRIBUTE2" : "123",'+
        '   "P_ATTRIBUTE3" : "321"'+
        '   '+
        '   '+
        '    '+
        ' }'+
        ' ],'+
        '  '+
        '  "message":"Message",'+
        '  "status":"S"'+
        '  '+
        '}';
       
       
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
 
        PageReference pageRef = Page.GenerateHC;
        pageRef.getParameters().put('id', String.valueOf(Cas.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(Cas);
        GenerateHandoverChecklist controller = new GenerateHandoverChecklist(sc);  
        System.debug('---Hi--44--'); 
        pageRef = controller.init();   
        Test.stopTest();
    }
    
    @istest static void noSignedDocument(){
       
        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();

        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);      
        
        Case pCas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        pCas.Booking_Unit__c = BU.Id ;
        pCas.Type = 'NOCVisa';
        pCas.Document_Verified__c = true;
        pCas.Payment_Verified__c = true;
        insert pCas;
        
        Case Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = BU.Id ;
        Cas.Type = 'NOCVisa';
        //Cas.Payment_Verified__c = true;
        Cas.ParentId = pCas.Id;
        insert Cas;
        
        ApexPages.currentPage().getParameters().put('id',Cas.Id);
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,HandoverChecklist1.HandOverCheckListResponse_element>();
        HandoverChecklist1.HandOverCheckListResponse_element response1 = new HandoverChecklist1.HandOverCheckListResponse_element();
        response1.return_x =    '{'+
        ' "data" : ['+
        ' {'+
        '   "P_PARAM_ID" : "123",'+
        '   "P_PROC_STATUS" : "S",'+
        '   "P_PROC_MESSAGE" : "Message",'+
        '   "URL" : "www.google.com",'+
        '   "P_ATTRIBUTE1" : "www.yahoo.com",'+
        '   "P_ATTRIBUTE2" : "123",'+
        '   "P_ATTRIBUTE3" : "321"'+
        '   '+
        '   '+
        '    '+
        ' }'+
        ' ],'+
        '  '+
        '  "message":"Message",'+
        '  "status":"S"'+
        '  '+
        '}';
       
       
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
 
        PageReference pageRef = Page.GenerateHC;
        pageRef.getParameters().put('id', String.valueOf(Cas.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(Cas);
        GenerateHandoverChecklist controller = new GenerateHandoverChecklist(sc);  
        System.debug('---Hi--44--'); 
        pageRef = controller.init();   
        Test.stopTest();
     
    }
    
    @istest static void TestHandoverMajorSnagging(){
       
        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);      
        
        Case pCas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        pCas.Booking_Unit__c = BU.Id ;
        pCas.Type = 'NOCVisa';
        pCas.Document_Verified__c = true;
        pCas.Total_Snags_Closed__c = 3;
        insert pCas;
        
        Case   Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = BU.Id ;
        Cas.Type = 'NOCVisa';
        Cas.Document_Verified__c = true;
        Cas.Total_Snags_Closed__c = 3;
        Cas.Total_Snags_Open__c = 1;
        Cas.Total_Major_Snags__c = 1;
        Cas.ParentId = pCas.Id;
        insert Cas;
        System.assert(Cas != null);
        
        ApexPages.currentPage().getParameters().put('id',Cas.Id);
        
        Test.startTest();
        
        SOAPCalloutServiceMock.returnToMe = new Map<String,HandoverChecklist1.HandOverCheckListResponse_element>();
        HandoverChecklist1.HandOverCheckListResponse_element response1 = new HandoverChecklist1.HandOverCheckListResponse_element();
        response1.return_x =    '{'+
        ' "data" : ['+
        ' {'+
        '   "P_PARAM_ID" : "123",'+
        '   "P_PROC_STATUS" : "S",'+
        '   "P_PROC_MESSAGE" : "Message",'+
        '   "URL" : "www.google.com",'+
        '   "P_ATTRIBUTE2" : "123",'+
        '   "P_ATTRIBUTE3" : "321"'+
        '   '+
        '   '+
        '    '+
        ' }'+
        ' ],'+
        '  '+
        '  "message":"Message",'+
        '  "status":"S"'+
        '  '+
        '}';
       
       
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        PageReference pageRef = Page.GenerateHC;
        pageRef.getParameters().put('id', String.valueOf(Cas.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(Cas);
        GenerateHandoverChecklist controller = new GenerateHandoverChecklist(sc);  
        System.debug('---Hi--44--'); 
        pageRef = controller.init();   
        Test.stopTest();
     
    }
    
    @istest static void TestHandoverNoSnagging(){
       
        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);      
        
        Case pCas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        pCas.Booking_Unit__c = BU.Id ;
        pCas.Type = 'NOCVisa';
        pCas.Document_Verified__c = true;
        pCas.Total_Snags_Closed__c = 3;
        insert pCas;
        
        Case   Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = BU.Id ;
        Cas.Type = 'NOCVisa';
        Cas.ParentId = pCas.Id;
        Cas.Document_Verified__c = true;
        Cas.Total_Snags_Closed__c = null;
        Cas.Total_Snags_Open__c = null;
        Cas.Total_Major_Snags__c = null;
        insert Cas;
        System.assert(Cas != null);
        
        ApexPages.currentPage().getParameters().put('id',Cas.Id);
        
        Test.startTest();        
        SOAPCalloutServiceMock.returnToMe = new Map<String,HandoverChecklist1.HandOverCheckListResponse_element>();
        HandoverChecklist1.HandOverCheckListResponse_element response1 = new HandoverChecklist1.HandOverCheckListResponse_element();
        response1.return_x =    '{'+
        ' "data" : ['+
        ' {'+
        '   "P_PARAM_ID" : "123",'+
        '   "P_PROC_STATUS" : "S",'+
        '   "P_PROC_MESSAGE" : "Message",'+
        '   "URL" : "www.google.com",'+
        '   "P_ATTRIBUTE2" : "123",'+
        '   "P_ATTRIBUTE3" : "321"'+
        '   '+
        '   '+
        '    '+
        ' }'+
        ' ],'+
        '  '+
        '  "message":"Message",'+
        '  "status":"S"'+
        '  '+
        '}';
       
       
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        PageReference pageRef = Page.GenerateHC;
        pageRef.getParameters().put('id', String.valueOf(Cas.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(Cas);
        GenerateHandoverChecklist controller = new GenerateHandoverChecklist(sc);  
        System.debug('---Hi--44--'); 
        pageRef = controller.init();   
        Test.stopTest();
     
    }
    
    @istest static void TestHandoverParent(){
       
        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);      
        
        Case pCas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        pCas.Booking_Unit__c = BU.Id ;
        pCas.Type = 'NOCVisa';
        pCas.Document_Verified__c = true;
        pCas.Payment_Verified__c = true;
        pCas.Total_Snags_Closed__c = 3;
        pCas.Total_Major_Snags__c = 0;
        pCas.Total_Snags_Open__c = 0;
        insert pCas;
        
        ApexPages.currentPage().getParameters().put('id',pCas.Id);
        
        Test.startTest();
        
        SOAPCalloutServiceMock.returnToMe = new Map<String,HandoverChecklist1.HandOverCheckListResponse_element>();
        HandoverChecklist1.HandOverCheckListResponse_element response1 = new HandoverChecklist1.HandOverCheckListResponse_element();
        response1.return_x =    '{'+
        ' "data" : ['+
        ' {'+
        '   "P_PARAM_ID" : "123",'+
        '   "P_PROC_STATUS" : "S",'+
        '   "P_PROC_MESSAGE" : "Message",'+
        '   "URL" : "www.google.com",'+
        '   "P_ATTRIBUTE2" : "123",'+
        '   "P_ATTRIBUTE3" : "321"'+
        '   '+
        '   '+
        '    '+
        ' }'+
        ' ],'+
        '  '+
        '  "message":"Message",'+
        '  "status":"S"'+
        '  '+
        '}';
       
       
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        PageReference pageRef = Page.GenerateHC;
        pageRef.getParameters().put('id', String.valueOf(pCas.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(pCas);
        GenerateHandoverChecklist controller = new GenerateHandoverChecklist(sc);  
        System.debug('---Hi--44--'); 
        pageRef = controller.init();   
        Test.stopTest();
     
    }
    
    @istest static void TestHandoverPayment(){
       
        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);      
        
        Case pCas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        pCas.Booking_Unit__c = BU.Id ;
        pCas.Type = 'NOCVisa';
        pCas.Document_Verified__c = true;
        pCas.Payment_Verified__c = true;
        pCas.Total_Snags_Closed__c = 3;
        pCas.Total_Major_Snags__c = 0;
        pCas.Total_Snags_Open__c = 0;
        insert pCas;
        
        Case   Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = BU.Id ;
        Cas.Type = 'NOCVisa';
        Cas.ParentId = pCas.Id;
        Cas.Document_Verified__c = true;
        Cas.Payment_Verified__c = false;
        Cas.Total_Snags_Closed__c = 10;
        Cas.Total_Snags_Open__c = 0;
        Cas.Total_Major_Snags__c = 0;
        insert Cas;
        
        ApexPages.currentPage().getParameters().put('id',Cas.Id);
        
        Test.startTest();
        
        SOAPCalloutServiceMock.returnToMe = new Map<String,HandoverChecklist1.HandOverCheckListResponse_element>();
        HandoverChecklist1.HandOverCheckListResponse_element response1 = new HandoverChecklist1.HandOverCheckListResponse_element();
        response1.return_x =    '{'+
        ' "data" : ['+
        ' {'+
        '   "P_PARAM_ID" : "123",'+
        '   "P_PROC_STATUS" : "S",'+
        '   "P_PROC_MESSAGE" : "Message",'+
        '   "URL" : "www.google.com",'+
        '   "P_ATTRIBUTE2" : "123",'+
        '   "P_ATTRIBUTE3" : "321"'+
        '   '+
        '   '+
        '    '+
        ' }'+
        ' ],'+
        '  '+
        '  "message":"Message",'+
        '  "status":"S"'+
        '  '+
        '}';
       
       
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        PageReference pageRef = Page.GenerateHC;
        pageRef.getParameters().put('id', String.valueOf(Cas.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(Cas);
        GenerateHandoverChecklist controller = new GenerateHandoverChecklist(sc);  
        System.debug('---Hi--44--'); 
        pageRef = controller.init();   
        Test.stopTest();
     
    }
    
     @istest static void Test10(){
       
        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);      
        
        Case pCas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        pCas.Booking_Unit__c = BU.Id ;
        pCas.Type = 'NOCVisa';
        pCas.Document_Verified__c = true;
        pCas.Payment_Verified__c = true;
        pCas.Total_Snags_Closed__c = 3;
        pCas.Total_Major_Snags__c = 0;
        pCas.Total_Snags_Open__c = 0;
        insert pCas;
        
        Case   Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = BU.Id ;
        Cas.Type = 'NOCVisa';
        Cas.ParentId = pCas.Id;
        Cas.Document_Verified__c = true;
        Cas.Payment_Verified__c = false;
        Cas.Total_Snags_Closed__c = 10;
        Cas.Total_Snags_Open__c = 0;
        Cas.Total_Major_Snags__c = 0;
        insert Cas;
        
        ApexPages.currentPage().getParameters().put('id',Cas.Id);
        
        Test.startTest();        
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock(1));  
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        PageReference pageRef = Page.GenerateHC;
        pageRef.getParameters().put('id', String.valueOf(Cas.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(Cas);
        GenerateHandoverChecklist controller = new GenerateHandoverChecklist(sc);          
        pageRef = controller.init();   
        Test.stopTest();
     
    }
    
    /************************************************************************
	* @Description : Method to test the Process Response method.
	*                for E-sign
	* @Params      : None
	* @Return      : None
	*************************************************************************/
    @istest static void TestProcessResponse(){
        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        Location__c objLoc = new Location__c();
        objLoc.Location_Code__c = 'VRD';
        objLoc.Location_ID__c ='23123';
        objLoc.New_Ho_Doc_Type__c = 'Unit';
        insert objLoc;

        Inventory__c objInv = new Inventory__c();
        objInv.Building_Location__c = objLoc.id;
        insert objInv;

        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;

        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;
        BU.Property_Name__c = 'Akoya';
        BU.Unit_Type__c = 'Villa';
        BU.Property_Country__c = 'United Arab Emirates';
        BU.Booking__c = Booking.Id;
        BU.Inventory__c = objInv.Id;
        insert BU;
        System.assert(BU != null);      
        
        Case pCas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        pCas.Booking_Unit__c = BU.Id ;
        pCas.Type = 'NOCVisa';
        pCas.Document_Verified__c = true;
        pCas.Total_Snags_Closed__c = 3;
        insert pCas;
        
        Case Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = BU.Id ;
        Cas.Type = 'NOCVisa';
        Cas.ParentId = pCas.Id;
        Cas.AccountId = Acc.Id;
        insert Cas;

        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c( name='HO Letter Unit',Drawloop_Document_Package_Id__c = 'a0V0E000003CTjT', Delivery_Option_Id__c = 'a0T0E000006rNbm');
        System.assert(Cas != null);
        
        ApexPages.currentPage().getParameters().put('id',Cas.Id);
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,HandoverChecklist1.HandOverCheckListResponse_element>();
        HandoverChecklist1.HandOverCheckListResponse_element response1 = new HandoverChecklist1.HandOverCheckListResponse_element();
        response1.return_x =    '{'+
        ' "data" : ['+
        ' {'+
        '   "P_PARAM_ID" : "123",'+
        '   "P_PROC_STATUS" : "S",'+
        '   "P_PROC_MESSAGE" : "Message",'+
        '   "URL" : "www.google.com",'+
        '   "P_ATTRIBUTE1" : "www.yahoo.com",'+
        '   "P_ATTRIBUTE2" : "123",'+
        '   "P_ATTRIBUTE3" : "321"'+
        '   '+
        '   '+
        '    '+
        ' }'+
        ' ],'+
        '  '+
        '  "message":"Message",'+
        '  "status":"S"'+
        '  '+
        '}';
       
       
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        PageReference pageRef = Page.GenerateHC;
        pageRef.getParameters().put('id', String.valueOf(Cas.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(Cas);
        GenerateHandoverChecklist controller = new GenerateHandoverChecklist(sc);
        pageRef = controller.init(); 
        pageRef = controller.processResponse('Web service callout failed: Unexpected element.'); 
        Test.stopTest();
    }
    
    /************************************************************************
	* @Description : Method to test the Process Response method.
	*                for E-sign
	* @Params      : None
	* @Return      : None
	*************************************************************************/
     @istest static void TestProcessResponse1(){
        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        Account   Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        Location__c objLoc = new Location__c();
        objLoc.Location_Code__c = 'VRD';
        objLoc.Location_ID__c ='23123';
        objLoc.New_Ho_Doc_Type__c = 'Unit';
        insert objLoc;

        Inventory__c objInv = new Inventory__c();
        objInv.Building_Location__c = objLoc.id;
        insert objInv;

        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id;
        Booking.Deal_SR__c = Sr.Id;

        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';
        BU.Registration_ID__c   = '1234';  
        BU.Unit_Selling_Price_AED__c  = 100;
        BU.Property_Name__c = 'Akoya';
        BU.Unit_Type__c = 'Villa';
        BU.Property_Country__c = 'United Arab Emirates';
        BU.Booking__c = Booking.Id;
        BU.Inventory__c = objInv.Id;
        insert BU;
        System.assert(BU != null);      
        
        Case pCas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        pCas.Booking_Unit__c = BU.Id ;
        pCas.Type = 'NOCVisa';
        pCas.Document_Verified__c = true;
        pCas.Total_Snags_Closed__c = 3;
        insert pCas;
        
        Case Cas = TestDataFactory_CRM.createCase(Acc.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = BU.Id ;
        Cas.Type = 'NOCVisa';
        Cas.ParentId = pCas.Id;
        Cas.AccountId = Acc.Id;
        insert Cas;

        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c( name='HO Letter Unit',Drawloop_Document_Package_Id__c = 'a0V0E000003CTjT', Delivery_Option_Id__c = 'a0T0E000006rNbm');
        System.assert(Cas != null);
        
        ApexPages.currentPage().getParameters().put('id',Cas.Id);
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,HandoverChecklist1.HandOverCheckListResponse_element>();
        HandoverChecklist1.HandOverCheckListResponse_element response1 = new HandoverChecklist1.HandOverCheckListResponse_element();
        response1.return_x =    '{'+
        ' "data" : ['+
        ' {'+
        '   "P_PARAM_ID" : "123",'+
        '   "P_PROC_STATUS" : "S",'+
        '   "P_PROC_MESSAGE" : "Message",'+
        '   "URL" : "www.google.com",'+
        '   "P_ATTRIBUTE1" : "www.yahoo.com",'+
        '   "P_ATTRIBUTE2" : "123",'+
        '   "P_ATTRIBUTE3" : "321"'+
        '   '+
        '   '+
        '    '+
        ' }'+
        ' ],'+
        '  '+
        '  "message":"Message",'+
        '  "status":"S"'+
        '  '+
        '}';
       
       
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        PageReference pageRef = Page.GenerateHC;
        pageRef.getParameters().put('id', String.valueOf(Cas.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(Cas);
        GenerateHandoverChecklist controller = new GenerateHandoverChecklist(sc);
        controller.HCurl = 'www.google.com';
        pageRef = controller.init(); 
        pageRef = controller.processResponse('Web service callout failed: Unexpected element.'); 
        Test.stopTest();
    }
}