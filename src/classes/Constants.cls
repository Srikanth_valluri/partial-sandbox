public class Constants {
	
    /* String variables */
    
    public static final string Str_Sprinkler = 'Sprinkler';
    
    /* End of String Variables */
    
    /* Record Types */
    public static final RecordTypeDetails Case_Complaint = new RecordTypeDetails('Case', 'Complaint') ;
    /* End of record types */
    
    
        
    public class RecordTypeDetails{
        public string DeveloperName;
        public string RecordTypeId;
        
        public RecordTypeDetails(string objName, string devName){
            RecordTypeId = Schema.getGlobalDescribe().get(objName).getDescribe().getRecordTypeInfosByDeveloperName().get(devName).getRecordTypeId();
            DeveloperName = devName;
        }
        
    }
        
}