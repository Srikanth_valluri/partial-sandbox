/*******************************************************************
This class is used for OBIEE Salesforce Integration
Created - 15/July/2017
Created By - ALOK -DAMAC
Updated : 
18/09/2017 ALOK DAMAC - Added Condition to Include Leads Director
05/12/2017 ALOK DAMAC - Added KPI Reports
********************************************************************/

public class FetchOBIEEReports {
    /*
    public String repYear{get;set;}
    public String repName{get;set;}
    public String repNamePerf{get;set;}
    public String repMonth{get;set;} 
    public String OpenPageURL1{get; set;}

    public String OpenPageURL2{get; set;}
    public String OpenPageURL3{get; set;}
    public String OpenPageURL4{get; set;}
    public String OpenPageURL5{get; set;}
    public String OpenPageURL6{get; set;}
    
    public String Page {get; set;}
    
   List<Profile> userProfile = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
   public String myProfileName = userProfile[0].Name;     

        public List<SelectOption> getYearList() {
        List<SelectOption> options= new List<SelectOption>();
            options.add(new SelectOption('NA','--- Select ---'));
            options.add(new SelectOption('2012','2012'));
            options.add(new SelectOption('2013','2013'));
            options.add(new SelectOption('2014','2014'));
            options.add(new SelectOption('2015','2015'));
            options.add(new SelectOption('2016','2016'));
            options.add(new SelectOption('2017','2017'));
            options.add(new SelectOption('2018','2018'));
            options.add(new SelectOption('2019','2019'));
            options.add(new SelectOption('2020','2020'));
            return options;
        }
          public List<SelectOption> getPerfReportName() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('NA','--- Select ---')); 
            options.add(new SelectOption('Performance Ranking By Sales','Performance Ranking By Sales'));
            options.add(new SelectOption('Performance Ranking By Product','Performance Ranking By Product'));
            return options;
        }
        public List<SelectOption> getReportName() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('NA','--- Select ---')); 
            if (myProfileName !='Leads Director'){
            options.add(new SelectOption('Sales Report','Sales Report'));}
            options.add(new SelectOption('Daily Activity Report','Daily Activity Report'));
           // options.add(new SelectOption('Gross Sales Summary Report','Gross Sales Summary Report'));
   
            return options;
        }
         public List<SelectOption> getMonthList() {
        List<SelectOption> options= new List<SelectOption>();
            options.add(new SelectOption('NA','--- Select ---'));
            options.add(new SelectOption('01','January'));
            options.add(new SelectOption('02','February'));
            options.add(new SelectOption('03','March'));
            options.add(new SelectOption('04','April'));
            options.add(new SelectOption('05','May'));
            options.add(new SelectOption('06','June'));
            options.add(new SelectOption('07','July'));
            options.add(new SelectOption('08','August'));
            options.add(new SelectOption('09','September'));
            options.add(new SelectOption('10','October'));
            options.add(new SelectOption('11','November'));
            options.add(new SelectOption('12','December'));
            return options;
        }
      public void setYearlist(String repYear) {
            this.repYear= repYear;
        }
      public void setReportName(String repName) {
            this.repName= repName;
        }
        public void setMonthlist(String repMonth) {
            this.repMonth= repMonth;
        }
        public void setPerfReportName(String repNamePerf) {
            this.repNamePerf= repNamePerf;
        }
        
      public void redirect()
    {
        Page= Apexpages.currentPage().getParameters().get('Param1');
        List <User> empIPMSId = [select Id,IPMS_Employee_ID__c from User where Id = :UserInfo.getUserId() limit 1];
            
        if(Page == 'ObieeReports')
        {
           List<Profile> repProfile = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
           String myProfile = repProfile[0].Name;  
           if (RepName =='Sales Report')
           {
           OpenPageURL2='';
           OpenPageURL3='';
           if (myProfile =='MIS User' || myProfile =='MIS User for BP' || myProfile=='Damac Admin'|| myProfile=='System Administrator')
          // if (myProfile =='MIS User' || myProfile =='MIS User for BP' || myProfile=='Damac Admin'|| myProfile=='System Administrator' || myProfile=='Property Consultant' || myProfile=='Head of Sales' || myProfile=='Director of Sales')
           {
           if(repMonth =='NA')
           {OpenPageURL1='https://analytics.damacgroup.com/analytics/saw.dll?Go&Path=/shared/Salesforce Report/Sales Report/Net Sales Summary&Options=d&Action=Navigate&P0=1&P1=eq&P2="Day"."Per Name Year"&P3='+repYear+'&NQUser=sf.user&NQPassword=sfuser@123';}
           else
           {OpenPageURL1='https://analytics.damacgroup.com/analytics/saw.dll?Go&Path=/shared/Salesforce Report/Sales Report/Net Sales Summary&Options=d&Action=Navigate&P0=2&P1=eq&P2="Day"."Per Name Year"&P3='+repYear+'&P4=eq&P5="Day"."Per Name Month"&P6='+repYear+' / '+repMonth+'&NQUser=sf.user&NQPassword=sfuser@123';}
           }//if (RepName =='Sales Report')
           else
           {
           if(repMonth =='NA')
           {OpenPageURL1 = 'https://analytics.damacgroup.com/analytics/saw.dll?Go&Path=/shared/Salesforce Report/Sales Report/Net Sales Summary&Options=d&Action=Navigate&P0=5&P1=eq&P2="Day"."Per Name Year"&P3='+repYear+'&P4=eq&P5="Sales Executive"."PC Id"&P6='+empIPMSId[0].IPMS_Employee_ID__c+'&P7=eq&P8="Sales Executive"."Dos Id"&P9='+empIPMSId[0].IPMS_Employee_ID__c+'&P10=eq&P11="Sales Executive"."Hos Id"&P12='+empIPMSId[0].IPMS_Employee_ID__c+'&P13=eq&P14="Sales Executive"."HOD_ID"&P15='+empIPMSId[0].IPMS_Employee_ID__c+'&NQUser=sf.user&NQPassword=sfuser@123';}
           else
           {OpenPageURL1 = 'https://analytics.damacgroup.com/analytics/saw.dll?Go&Path=/shared/Salesforce Report/Sales Report/Net Sales Summary&Options=d&Action=Navigate&P0=6&P1=eq&P2="Day"."Per Name Year"&P3='+repYear+'&P4=eq&P5="Sales Executive"."PC Id"&P6='+empIPMSId[0].IPMS_Employee_ID__c+'&P7=eq&P8="Sales Executive"."Dos Id"&P9='+empIPMSId[0].IPMS_Employee_ID__c+'&P10=eq&P11="Sales Executive"."Hos Id"&P12='+empIPMSId[0].IPMS_Employee_ID__c+'&P13=eq&P14="Sales Executive"."HOD_ID"&P15='+empIPMSId[0].IPMS_Employee_ID__c+'&P16=eq&P17="Day"."Per Name Month"&P18='+repYear+' / '+repMonth+'&NQUser=sf.user&NQPassword=sfuser@123';}
           }
           }
          // Daily Activity Report 
          // List<Profile> repProfile = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
          // String myProfile = repProfile[0].Name;  
          
           if (RepName =='Daily Activity Report')
           {
           OpenPageURL2='';
           OpenPageURL3='';
           
            if (myProfile =='MIS User' || myProfile =='MIS User for BP' || myProfile=='Damac Admin'|| myProfile=='System Administrator'||myProfile =='Leads Director')
          
           {
        
             OpenPageURL1 = 'https://analytics.damacgroup.com/analytics/saw.dll?Go&Path=/shared/Salesforce Report/Lead and Activity/Daily Activity Report&Options=d&Action=Navigate&P0=6&P1=eq&P2="Dim - Lead Creation Date"."Per Name Year"&P3='+repYear+'&P4=eq&P5="Property Consultant"."PC Id"&P6='+empIPMSId[0].IPMS_Employee_ID__c+'&P7=eq&P8="Property Consultant"."Dos Id"&P9='+empIPMSId[0].IPMS_Employee_ID__c+'&P10=eq&P11="Property Consultant"."Hos Id"&P12='+empIPMSId[0].IPMS_Employee_ID__c+'&P13=eq&P14="Property Consultant"."HOD_ID"&P15='+empIPMSId[0].IPMS_Employee_ID__c+'&P16=eq&P17="Dim - Lead Creation Date"."Per Name Month"&P18='+repYear+' / '+repMonth+'&NQUser=sf.user&NQPassword=sfuser@123';   
          // OpenPageURL='https://analytics.damacgroup.com/analytics/saw.dll?Go&Path=/shared/Salesforce Report/Lead and Activity/Daily Activity Report&Options=d&Action=Navigate&P0=2&P1=eq&P2="Dim - Lead Creation Date"."Per Name Year"&P3='+repYear+'&P4=eq&P5="Dim - Lead Creation Date"."Per Name Month"&P6='+repYear+' / '+repMonth+'&NQUser=sf.user&NQPassword=sfuser@123';
           
           }
           
            else
           {
      
              OpenPageURL1 = 'https://analytics.damacgroup.com/analytics/saw.dll?Go&Path=/shared/Salesforce Report/Lead and Activity/Daily Activity Report&Options=d&Action=Navigate&P0=6&P1=eq&P2="Dim - Lead Creation Date"."Per Name Year"&P3='+repYear+'&P4=eq&P5="Property Consultant"."PC ID"&P6='+empIPMSId[0].IPMS_Employee_ID__c+'&P7=eq&P8="Property Consultant"."Hos ID"&P9='+empIPMSId[0].IPMS_Employee_ID__c+'&P10=eq&P11="Property Consultant"."Dos ID"&P12='+empIPMSId[0].IPMS_Employee_ID__c+'&P13=eq&P14="Property Consultant"."HOD ID"&P15='+empIPMSId[0].IPMS_Employee_ID__c+'&P16=eq&P17="Dim - Lead Creation Date"."Per Name Month"&P18='+repYear+' / '+repMonth+'&NQUser=sf.user&NQPassword=sfuser@123';     
          //  OpenPageURL = 'https://analytics.damacgroup.com/analytics/saw.dll?Go&Path=/shared/Salesforce Report/Lead and Activity/Daily Activity Report&Options=d&Action=Navigate&P0=6&P1=eq&P2="Dim - Lead Creation Date"."Per Name Year"&P3='+repYear+'&P4=eq&P5="Property Consultant"."PC Id"&P6='+empIPMSId[0].IPMS_Employee_ID__c+'&P7=eq&P8="Property Consultant"."Dos Id"&P9='+empIPMSId[0].IPMS_Employee_ID__c+'&P10=eq&P11="Property Consultant"."Hos Id"&P12='+empIPMSId[0].IPMS_Employee_ID__c+'&P13=eq&P14="Property Consultant"."HOD_ID"&P15='+empIPMSId[0].IPMS_Employee_ID__c+'&P16=eq&P17="Dim - Lead Creation Date"."Per Name Month"&P18='+repYear+' / '+repMonth+'&NQUser=sf.user&NQPassword=sfuser@123';
          //  OpenPageURL='https://analytics.damacgroup.com/analytics/saw.dll?Go&Path=/shared/Salesforce Report/Lead and Activity/Daily Activity Report&Options=d&Action=Navigate&P0=2&P1=eq&P2="Dim - Lead Creation Date"."Per Name Year"&P3='+repYear+'&P4=eq&P5="Dim - Lead Creation Date"."Per Name Month"&P6='+repYear+' / '+repMonth+'&NQUser=sf.user&NQPassword=sfuser@123';

            
           }
         
           }
           
           //Daily Activity Report
          
           
           if (RepName =='Gross Sales Summary Report')
           {
           OpenPageURL2='';
           OpenPageURL3='';
           if (myProfile =='MIS User' || myProfile =='MIS User for BA' || myProfile=='Damac Admin'|| myProfile=='System Administrator')
           {
           if(repMonth=='NA')
           {OpenPageURL1='http://dxbhobiapps.damacholding.home:9707/analytics/saw.dll?Go&Path=/shared/Salesforce Report/Sales Report/Gross Sales Summary&Action=Navigate&P0=1&P1=eq&P2="Day"."Per Name Year"&P3='+repYear+'&NQUser=sf.user&NQPassword=sfuser@123';}       
           else      
           {OpenPageURL1 ='http://dxbhobiapps.damacholding.home:9707/analytics/saw.dll?Go&Path=/shared/Salesforce Report/Sales Report/Gross Sales Summary&Action=Navigate&P0=2&P1=eq&P2="Day"."Per Name Year"&P3='+repYear+'&P4=eq&P5="Day"."Per Name Month"&P6='+repYear+' / '+repMonth+'&NQUser=sf.user&NQPassword=sfuser@123';}
           }
           else
           {
           if (repMonth=='NA')
           {OpenPageURL1 = 'http://dxbhobiapps.damacholding.home:9707/analytics/saw.dll?Go&Path=/shared/Salesforce Report/Sales Report/Gross Sales Summary&Action=Navigate&P0=5&P1=eq&P2="Day"."Per Name Year"&P3='+repYear+'&P4=eq&P5="Sales Executive"."PC Id"&P6='+empIPMSId[0].IPMS_Employee_ID__c+'&P7=eq&P8="Sales Executive"."Dos Id"&P9='+empIPMSId[0].IPMS_Employee_ID__c+'&P10=eq&P11="Sales Executive"."Hos Id"&P12='+empIPMSId[0].IPMS_Employee_ID__c+'&P13=eq&P14="Sales Executive"."HOD_ID"&P15='+empIPMSId[0].IPMS_Employee_ID__c+'&NQUser=sf.user&NQPassword=sfuser@123';}
           else
           {OpenPageURL1 = 'http://dxbhobiapps.damacholding.home:9707/analytics/saw.dll?Go&Path=/shared/Salesforce Report/Sales Report/Gross Sales Summary&Action=Navigate&P0=6&P1=eq&P2="Day"."Per Name Year"&P3='+repYear+'&P4=eq&P5="Sales Executive"."PC Id"&P6='+empIPMSId[0].IPMS_Employee_ID__c+'&P7=eq&P8="Sales Executive"."Dos Id"&P9='+empIPMSId[0].IPMS_Employee_ID__c+'&P10=eq&P11="Sales Executive"."Hos Id"&P12='+empIPMSId[0].IPMS_Employee_ID__c+'&P13=eq&P14="Sales Executive"."HOD_ID"&P15='+empIPMSId[0].IPMS_Employee_ID__c+'&P16=eq&P17="Day"."Per Name Month"&P18='+repYear+' / '+repMonth+'&NQUser=sf.user&NQPassword=sfuser@123';}
           }   
           }      
           
    }//if (Page=='ObieeReports')
    if(Page=='PerformanceReports')
    {
    System.Debug('Page++'+Page);
    System.Debug('RepName++'+repNamePerf);
    if (repNamePerf=='Performance Ranking By Sales')
     {
     
     OpenPageURL1='https://analytics.damacgroup.com/analytics/saw.dll?Go&Path=/shared/Sales Performance Dashboard/KPI Dashboard Analytic Library/Performance Ranking/Performance Ranking HOD_Sales&Options=d&NQUser=sf.user&NQPassword=sfuser@123';
     OpenPageURL2='https://analytics.damacgroup.com/analytics/saw.dll?Go&Path=/shared/Sales Performance Dashboard/KPI Dashboard Analytic Library/Performance Ranking/Performance Ranking HOS_Sales&Options=d&NQUser=sf.user&NQPassword=sfuser@123';
     OpenPageURL3='https://analytics.damacgroup.com/analytics/saw.dll?Go&Path=/shared/Sales Performance Dashboard/KPI Dashboard Analytic Library/Performance Ranking/Performance Ranking DOS_Sales&Options=d&NQUser=sf.user&NQPassword=sfuser@123';
     } 
     if (repNamePerf=='Performance Ranking By Product')
     {
   
     OpenPageURL1='https://analytics.damacgroup.com/analytics/saw.dll?Go&Path=/shared/Sales Performance Dashboard/KPI Dashboard Analytic Library/Performance Ranking/Performance Ranking HOD_Products&Options=d&NQUser=sf.user&NQPassword=sfuser@123';
     OpenPageURL2='https://analytics.damacgroup.com/analytics/saw.dll?Go&Path=/shared/Sales Performance Dashboard/KPI Dashboard Analytic Library/Performance Ranking/Performance Ranking HOS_Products&Options=d&NQUser=sf.user&NQPassword=sfuser@123';
     OpenPageURL3='https://analytics.damacgroup.com/analytics/saw.dll?Go&Path=/shared/Sales Performance Dashboard/KPI Dashboard Analytic Library/Performance Ranking/Performance Ranking DOS_Products&Options=d&NQUser=sf.user&NQPassword=sfuser@123';
     }  
    }// if(Page=='PerformanceReports')
    }//redirect*/
    }