public without sharing class NOCExtensionController{
    public Id caseId;
    public Case objCase;
    public Booking_Unit__c objUnit;
    public Decimal decAssignmentFee;
    public NOCExtensionController(ApexPages.StandardController stdCon){
        caseId = stdCon.getId();
    }
    
    public pagereference Confirm(){
        pagereference newpg = new Pagereference('/'+caseId);
        if(decAssignmentFee > 0){
            objCase.NOC_Extension_Approval__c = 'Submitted';
            
        }else{
            objCase.NOC_Extension_Approval__c = 'Approved';
        }
        update objCase;
        newpg.setRedirect(true);
        return newpg;
    }
    
    public void extendNOC(){
        decAssignmentFee = 0;
        list<Case> lstC = [Select Id
                                , Origin
                                , Case_Type__c
                                , Buyer_Type__c
                                , Seller__c
                                , Assignment_Fee__c
                                , Pending_Amount__c
                                , CurrencyISOCode
                                , Booking_Unit__c
                                , Booking_Unit__r.Registration_ID__c
                                , Booking_Unit__r.NOC_Issued_Date__c
                                , Booking_Unit__r.NOC_Re_Issued_Date__c
                                , Booking_Unit__r.Inventory__r.Property_City__c
                                , Booking_Unit__r.Inventory__r.Property__r.Name
                                , Booking_Unit__r.Inventory__r.Building_Code__c
                                , Booking_Unit__r.Inventory__r.Bedroom_Type__c
                                , Booking_Unit__r.Inventory__r.Property_Status__c
                                , Booking_Unit__r.Permitted_Use__c
                                , Booking_Unit__r.Unit_Type__c
                           From Case
                           where Id =: caseId];
        if(lstC != null && !lstC.isEmpty()){
            objCase = lstC[0];
            objUnit = objCase.Booking_Unit__r;
            if(objCase.Booking_Unit__r.NOC_Re_Issued_Date__c == null){
                // post Assignment Fee to IPMS again, on success update Case and BU
                if(fetchPaymentDetails()){
                    if(decAssignmentFee > 0){
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Confirmation will result in an Assignment Fees of '+objCase.CurrencyISOCode+' '+decAssignmentFee+' being posted to Customer\'s SOA');
                        ApexPages.addMessage(myMsg);
                    }
                }else{
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Error : Unable to get response from the Rule Engine for Assignment fees. Please try again later.');
                    ApexPages.addMessage(myMsg);
                }
            }else{
                // add message to say NOC cannot be re-generated again
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'NOC can only be re-generated once. Please contact your administrator.');
                ApexPages.addMessage(myMsg);
            }
        }
    } //end of extendNOC
    
    public boolean fetchPaymentDetails(){
        boolean isSuccessful = false;
        try{
            String errorMessage = '';
            system.debug('View reqn called***************');
            String strFeeResponse = assignmentEndpoints.fetchAssignmentFees(objUnit, objCase, 'Assignment');
            system.debug('strFeeResponse==='+strFeeResponse);
            if(String.isNotBlank(strFeeResponse)
            && !strFeeResponse.contains('Exception occured')){
                map<String,String> mapKey_Value = new map<String,String>();
                map<String,Object> mapDeserializeFees = new map<String,Object>();
                mapKey_Value = parseRuleEngine(strFeeResponse, 'Fees');
                mapDeserializeFees = (map<String,Object>)JSON.deserializeUntyped(strFeeResponse);
                system.debug('mapKey_Value==='+mapKey_Value);
                if(mapDeserializeFees.containsKey('allowed')
                && mapDeserializeFees.get('allowed') == 'Yes'){
                    if(!mapKey_Value.isEmpty()){
                        if(mapKey_Value.containsKey('adminFeeFlat')
                        && mapKey_Value.get('adminFeeFlat') != null
                        && mapKey_Value.get('adminFeeFlat') != 'null'){
                            decimal intVal = decimal.valueOf(mapKey_Value.get('adminFeeFlat').remove('AED').trim());
                            objCase.Assignment_Fee__c = intVal != null ? intVal : 0;
                            objCase.Pending_Amount__c = objCase.Assignment_Fee__c;
                            decAssignmentFee = objCase.Assignment_Fee__c;
                            isSuccessful = true;
                        }else if(mapKey_Value.containsKey('adminFeePsf')
                        && mapKey_Value.get('adminFeePsf') != null
                        && mapKey_Value.get('adminFeePsf') != 'null'){
                            decimal intVal = decimal.valueOf(mapKey_Value.get('adminFeePsf').remove('AED').trim());
                            objCase.Assignment_Fee__c = intVal != null ? (intVal * objUnit.Area__c).setScale(2) : 0;
                            objCase.Pending_Amount__c = objCase.Assignment_Fee__c;
                            decAssignmentFee = objCase.Assignment_Fee__c;
                            isSuccessful = true;
                        }else if(mapKey_Value.containsKey('adminFeePercentage')
                        && mapKey_Value.get('adminFeePercentage') != null
                        && mapKey_Value.get('adminFeePercentage') != 'null'){
                            decimal intVal = decimal.valueOf(mapKey_Value.get('adminFeePercentage').remove('AED').trim());
                            objCase.Assignment_Fee__c = intVal != null ? (intVal * objUnit.Requested_Price__c).setScale(2) : 0;
                            objCase.Pending_Amount__c = objCase.Assignment_Fee__c;
                            decAssignmentFee = objCase.Assignment_Fee__c;
                            isSuccessful = true;
                        }
                        system.debug('*****Assignment Fee*****'+objCase.Assignment_Fee__c);
                    }
                }
                /*
                else if(mapDeserializeFees.containsKey('allowed')
                && mapDeserializeFees.get('message') != null
                && mapDeserializeFees.get('message') != 'null'
                && mapDeserializeFees.get('allowed') == 'No') {
                    errorMessage = 'Error : '+mapDeserializeFees.get('message');
                    Error_Log__c objErr = new Error_Log__c();
                    objErr.Account__c = objCase.Seller__c;
                    objErr.Booking_Unit__c = objCase.Booking_Unit__c;
                    objErr.Case__c = objCase.Id;
                    objErr.Process_Name__c = 'Assignment';
                    objErr.Error_Details__c = errorMessage;
                    insert objErr;
                }
                else {
                    system.debug('Error 01');
                    errorMessage = 'Error : Unable to get response from the Rule Engine for Assignment fees. Please try again later.';
                    Error_Log__c objErr = new Error_Log__c();
                    objErr.Account__c = objCase.Seller__c;
                    objErr.Booking_Unit__c = objCase.Booking_Unit__c;
                    objErr.Case__c = objCase.Id;
                    objErr.Process_Name__c = 'Assignment';
                    objErr.Error_Details__c = errorMessage;
                    insert objErr;
                }
                */
            }else{
                system.debug('Error 02');
                errorMessage = 'Error : Unable to get response from the Rule Engine for Assignment fees. Please try again later.';
                Error_Log__c objErr = new Error_Log__c();
                objErr.Account__c = objCase.Seller__c;
                objErr.Booking_Unit__c = objCase.Booking_Unit__c;
                objErr.Case__c = objCase.Id;
                objErr.Process_Name__c = 'Assignment';
                objErr.Error_Details__c = errorMessage;
                insert objErr;
            }
        }catch(Exception ex){
            system.debug('Error*****'+ex.getMessage());
        }
        system.debug('isSuccessful *****'+isSuccessful);
        return isSuccessful;
    } // end of fetchPaymentDetails
    
    public map<String,String> parseRuleEngine(String ruleEngineResponse, String processFunctionality) {
        map<String,String> mapRuleEngine_Error = new map<String,String>();
        ruleEngineResponse = ruleEngineResponse.remove('{');
        ruleEngineResponse = ruleEngineResponse.remove('}');
        ruleEngineResponse = ruleEngineResponse.remove('"');
        system.debug('ruleEngineResponse==='+ruleEngineResponse);
        map<String,String> mapKey_Value = new map<String,String>();
        system.debug('ruleEngineResponse******'+ruleEngineResponse);
        Integer indexVal = 0;
        boolean blnAllow = false;
        for(String st : ruleEngineResponse.split(',')){
            system.debug('*****st*****'+st);
            String strPre = st.substringBefore(':').trim();
            String strPost = st.subStringAfter(':').trim();
            if(!blnAllow && strPre.equalsIgnoreCase('allowed')
            && strPost.equalsIgnoreCase('Yes')){
                blnAllow = true;
            }
            mapRuleEngine_Error.put(strPre,strPost);

            system.debug('strPre :*******'+strPre);
            system.debug('strPost :*******'+strPost);
            if(processFunctionality == 'Fees' && indexVal >=12 && indexVal <= 14) {
                mapKey_Value.put(strPre,strPost);
            }
            if(!blnAllow && indexVal == 1){
                break;
            }
            indexVal++;
        }
        system.debug('mapKey_Value*************'+mapKey_Value);
        return mapKey_Value;
    }
    
    public pageReference Cancel(){
        pagereference newpg = new Pagereference('/'+caseId);
        newpg.setRedirect(true);
        return newpg;
    }
}