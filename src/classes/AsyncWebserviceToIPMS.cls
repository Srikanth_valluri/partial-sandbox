public class AsyncWebserviceToIPMS {

    public static String createUAJSON (List <Inventory__c> units, Unit_Assignment__c ua, Boolean removeInventory, Boolean isTrigger){ 
        String returnMsg = '';
        IPMSJSONCreation obj = new IPMSJSONCreation ();
        obj.sourceSystem = 'SFDC';
        obj.extRequestNumber = ua.Name;
        List <IPMSJSONCreation.blockLines> items = new List <IPMSJSONCreation.blockLines>();
        for (Inventory__c iu :units){
            IPMSJSONCreation.blockLines item = new IPMSJSONCreation.blockLines ();            
            item.subRequestName = 'UNIT_BLOCK_AVL';
            item.unitId =  iu.Inventory_Id__c;
            item.pcId = ua.CreatedBy.IPMS_Employee_ID__c;
            
            if (ua.CreatedBy.Manager != null)
                item.managerId = ua.CreatedBy.Manager.IPMS_Employee_ID__c;
            if (ua.agency__c != NULL)
                item.agentId = ua.agency__r.Vendor_ID__c;
            
            item.reasonText = ua.Reason_For_Unit_Assignment__c;
            if (removeInventory) {
                item.pcId = '';
                item.managerId = '';
                item.agentId = '';
                
            }
            items.add (item);
        }
        obj.blockLines = items;        
        String reqBody = JSON.serializePretty(obj);
        if (isTrigger == FALSE)
            returnMsg = SendToIPMS (reqBody, ua.id);
        else
            callFutureHandler (reqBody, ua.id);
            
        return returnMsg;
    
    }
    
    
    @Future (Callout = TRUE)
    public static void callFutureHandler (String reqBody, String recId) {
        SendToIPMS (reqBody, recId);
    }
    
    
    public static String SendToIPMS (String reqBody, String recId){
        try{
            String EndPointURL = '';
            try {
                IPMS_Integration_Settings__mdt ipms_Rest = [SELECT Endpoint_URL__c,
                            Password__c,  
                            Username__c 
                            FROM IPMS_Integration_Settings__mdt 
                            WHERE DeveloperName = 'IPMS_UA_REST' LIMIT 1];
                EndPointURL = ipms_Rest.Endpoint_URL__c;
            }
            catch (Exception e) {}          
            if (Test.isRunningTest ()) {
                EndPointURL = 'www.test.com';
            }            
            
            
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint (EndPointURL);
            req.setMethod ('POST');
            req.setheader ('Content-Type', 'application/json');
            req.setbody (reqBody);
            req.setTimeOut (120000);
            System.Debug (EndPointURL);
            System.Debug ('---Request Body---'+reqBody);
            HttpResponse res = h.send(req);
            System.Debug (res.getBody ());
            
            if(res.getStatusCode() == 201 ) {
                String returnMsg = parseResponse (res.getBody ());
                if (returnMsg == 'Success') {
                    createLog (recId, '', 'Success');
                    return 'Success';
                }
                else {
                    createLog (recId, res.getBody(), 'Error');
                    return 'Error';     
                }
            } else {
                createLog (recId, res.getBody(), 'Error');
                return 'Error';
            }
        } catch(exception e){
            System.Debug ('-----'+e.getMessage ());
            createLog (recId, String.valueof (e.getmessage()), 'Error');
            return 'Error';
        }
    }
    
    public Static String parseResponse (String responseBody) {
        String response = '';
        Map <String,Object> jsonMap = (Map<String,Object>)JSON.deserializeUntyped(responseBody);
        
        List <Object> responseLinesList = (List <Object>)jsonMap.get('responseLines'); 
        System.Debug (responseLinesList);
        
        for (Object val : responseLinesList ) {
            JSONParser parser = JSON.createParser(JSON.serialize(val));
            While (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'status')) {
                    parser.nextToken();
                    if (parser.getText () == 'S') {
                        response = 'Success';
                    }
                }
            
            }
            
        }
        return response;
    }
    public static void createLog (string parentId, String ErrorMessage, String type) {
        log__c log = new log__c ();
        log.Unit_Assignment__c = parentId;
        log.Type__c = type;
        log.Description__c = ErrorMessage;
        insert log;
    }
}