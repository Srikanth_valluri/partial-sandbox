public without sharing class ProfileController {
    public transient Contact  customerContact         {get; set;}
    public transient Account  account                 {get; set;}
    public transient Id       customerAccountId       {get; set;}
    public transient Double   currentValue            {get; set;}
    public transient Double   totalInvestment         {get; set;}
    public transient String   customerCategory        {get; set;}
    public transient String   dblCurrentValue         {get; set;}
    public transient String   dblTotalInvestment      {get; set;}
    public transient String   strTotalSellingPrice    {get; set;}
    /*public Date  myDate {get; set;}
    public String dayString {get; set;}*/
    public transient String passportURL {get; set;}
    public Id       unitId                  {get; set{
        unitId = value;
    }}
    public transient List<SR_Attachments__c> lstDoc {get; set;}

    public ProfileController() {
        if (CustomerCommunityUtils.isCurrentView('myprofileview')) {
            customerAccountId = CustomerCommunityUtils.customerAccountId;

            List<Account> lstAccount = [
                SELECT Salutation
                     , Name
                     , Nationality__c
                     , Nationality__pc
                     , Passport_Number__pc
                     , Passport_Number__c
                     , Passport_Expiry_Date__c
                     , Passport_Expiry_Date__pc
                     , Position__c
                     , Sport_Others__c
                     , Music_Show_Others__c
                     , Hobbies_Others__c
                     , Party_ID__c
                     , Email__c
                     , Email__pc
                     , Mobile_Phone_Encrypt__pc
                     , Mobile__c
                     , Blogs_regularly_read__c
                     , Education_Others__c
                     , Mother_Language__c
                     , What_product_improve_your_life__c
                     , Cuisine_Preferences__c
                     , Holiday_Destinations__c
                     , Favorite_Brands__c
                     , Favorite_Cravings__c
                     , No_of_Children__c
                     , Child_1_DOB__c
                     , Child_2_DOB__c
                     , Child_3_DOB__c
                     , Child_4_DOB__c
                     , Child_5_DOB__c
                     , Child_6_DOB__c
                     , Primary_Language__c
                     , Address_Line_1__pc
                     , Address_Line_1__c
                     , Address_Line_2__pc
                     , Address_Line_2__c
                     , Address_Line_3__pc
                     , Address_Line_3__c
                     , Address_Line_4__pc
                     , Address_Line_4__c
                     , City__c
                     , City__pc
                     , Country__c
                     , Country__pc
                     , Party_Type__c
                     , Mobile_Phone_Encrypt_2__pc
                     , Mobile_Phone_Encrypt_3__pc
                     , Mobile_Phone_Encrypt_4__pc
                     , Mobile_Phone_Encrypt_5__pc
                     , Marital_Status__c
                     , Religion__c
                     , Age_of_Birth__c
                     , Disabilities__c
                     , Work_Anniversary__c
                     , Vaccation_city_Others__c
                     , Frequent_Website_visit__c
                     , Type_of_Car__c
                     , Additional_Language__c
                     , Home_Town__c
                     , Hobbies__c
                     , Favorite_Sport__c
                     , Favorite_TV_shows_music_Movie__c
                     , Personal_Interests__c
                     , Favorite_Vacation_City__c
                     , Festivals_Celebrations__c
                     , Favorite_Music__c
                     , Favorite_Personality__c
                     , Date_of_Birth__pc
                     , Email_2__pc
                     , Email_3__pc
                     , Email_4__pc
                     , Email_5__pc
                     , Industry
                     , Favorite_Evenings__c
                     , Company_Name__c
                     , Eductaion__c
                     , Gender__c,
                     (Select Id, Name, Attachment_URL__c,Portal_Document_Type__c
                        From Case_Attachments__r
                        Where Attachment_URL__c != NULL
                        AND Name LIKE '%Passport%' ORDER BY CreatedDate DESC LIMIT 1)
                FROM    Account
                WHERE   Id = :customerAccountId
                LIMIT   1
            ];
            if (lstAccount.isEmpty()) {
                return;
            }
            if(lstAccount[0].Case_Attachments__r != null && !lstAccount[0].Case_Attachments__r.isEmpty()){
                lstDoc = lstAccount[0].Case_Attachments__r;
            }
                if(lstDoc != NULL && !lstDoc.isEmpty()){
                    passportURL = lstDoc[0].Attachment_URL__c;
                }

            account = lstAccount[0];
            Map<String, object> mapCurrentValue = new Map<String, object>();
            mapCurrentValue = DashboardController.getCurrentValue(customerAccountId);
            if(mapCurrentValue != null){
                currentValue = Double.ValueOf(mapCurrentValue.get('dblCurrentValue'));
                totalInvestment = Double.ValueOf(mapCurrentValue.get('dblTotalInvestment'));
                dblCurrentValue = CustomerCommunityUtils.getAmountInShortWords(currentValue);
                dblTotalInvestment = CustomerCommunityUtils.getAmountInShortWords(totalInvestment);
            }

            for (Customer_Category__c custCategory : [  SELECT      Id,
                                                                    Name,
                                                                    Price_of_Category__c,
                                                                    Customer_Category__c
                                                        FROM        Customer_Category__c
                                                        ORDER BY    Price_of_Category__c DESC]
            ) {
                if (totalInvestment <= custCategory.Price_of_Category__c) {
                    customerCategory = custCategory.Customer_Category__c;
                }
            }
        }
    }

    /*public String getPassportDetails(){
        String strURL;
        List<SR_Attachments__c> lstDoc = new List<SR_Attachments__c>();
        lstDoc = [SELECT id, Attachment_URL__c,Portal_Document_Type__c
                  FROM SR_Attachments__c where Account__c =: customerAccountId
                  AND Attachment_URL__c != NULL
                  AND Name LIKE '%Passport%' ORDER BY CreatedDate DESC];

        strURL = lstDoc[0].Attachment_URL__c;
        return strURL;
    }*/

    @RemoteAction
    public static void updateAdditionalContact(Map<String, String> mapAdditionalDetails) {
        Account customerAccount = new Account(Id = CustomerCommunityUtils.customerAccountId);
        for (String fieldName : mapAdditionalDetails.keySet()) {
            system.debug('====fieldName===='+fieldName);
            system.debug('====map fieldName===='+mapAdditionalDetails.get(fieldName));

            if(fieldName != 'Date_of_Birth__pc' && fieldName != 'Work_Anniversary__c'
                && fieldName != 'Child_1_DOB__c' && fieldName != 'Child_2_DOB__c'
                && fieldName != 'Child_3_DOB__c'
                && fieldName != 'Child_4_DOB__c'
                && fieldName != 'Child_5_DOB__c'
                && fieldName != 'Child_6_DOB__c'
                && fieldName != 'Age_of_Birth__c'){
                customerAccount.put(fieldName, mapAdditionalDetails.get(fieldName));
            }else if(mapAdditionalDetails.get(fieldName) != ''){
                system.debug('mapAdditionalDetails'+mapAdditionalDetails.get(fieldName));
                customerAccount.put(fieldName, Date.valueOf( mapAdditionalDetails.get(fieldName)));
            }
        }
        system.debug('====customerAccount===='+customerAccount);
        update customerAccount;
    }

    @RemoteAction
    public static CaseCancellationResponseWrapper cancelServiceRequest( Id caseId ) {
    	system.debug('-- Case Id --'+caseId );
    	if( caseId != null ) {
	    	CaseCancellationResponseWrapper objResponse = new CaseCancellationResponseWrapper();
	    	Case objCase = [ SELECT Id
	    						  , RecordType.Name
	    					   FROM Case
	    					  WHERE Id = :caseId ];
	    	system.debug( '-- Case RecordType Name --'+ objCase.RecordType.Name );
	    	if( String.isNotBlank( objCase.RecordType.Name ) ) {
	    		if( objCase.RecordType.Name.equalsIgnoreCase( 'Change of Details' ) ||
	    			objCase.RecordType.Name.equalsIgnoreCase( 'Change of Joint Buyer' ) ||
	    			objCase.RecordType.Name.equalsIgnoreCase( 'Name Nationality Change' ) ||
	    			objCase.RecordType.Name.equalsIgnoreCase( 'Passport Detail Update' ) ) {
	    			Cancel_CODController objCancelCODRequest = new Cancel_CODController( caseId );
	    			objCancelCODRequest.CancelCODCase();
	    			system.debug('-- Cancellation Message --'+objCancelCODRequest.errorMessage);
	    			if( String.isBlank( objCancelCODRequest.errorMessage ) ) {
	    				objResponse.isSuccess = true ;
    					objResponse.strMessage = 'Case has been cancelled successfully.';
	    			}
	    			else {
	    				objResponse.strMessage = objCancelCODRequest.errorMessage ;
	    			}
	    		}
	    		else if( objCase.RecordType.Name.equalsIgnoreCase( 'Assignment' ) ) {
	    			AssignmentCancellationController objCancelAssRequest = new AssignmentCancellationController( caseId );
	    			objCancelAssRequest.checkEligibility();
	    			if( String.isBlank( objCancelAssRequest.errorMessage ) ) {
	    				objResponse.isSuccess = true ;
    					objResponse.strMessage = 'Case has been cancelled successfully.';
	    			}
	    			else {
	    				objResponse.strMessage = objCancelAssRequest.errorMessage ;
	    			}
	    		}
	    		else if( objCase.RecordType.Name.equalsIgnoreCase( 'POP') ||
                         objCase.RecordType.Name.equalsIgnoreCase( 'Complaint')) {
                    objCase.Status = 'Cancelled';
                    try{
                        update objCase;
                    }
                    catch(Exception e){
                        System.Debug('=Error in update the case : ' + e);
                    }
                    objResponse.isSuccess = true ;
                    objResponse.strMessage = 'Case has been cancelled successfully.';
	    		}
	    	}
	    	system.debug('-- objResponse --'+objResponse);
	    	return objResponse ;
    	}
    	return null ;
    }

    /*public static CaseCancellationResponseWrapper cancelCase(Case objCase){
        CaseCancellationResponseWrapper objResponse = new CaseCancellationResponseWrapper();
        objCase.Status = 'Cancelled' ;
                    try{
                        update objCase;

                    }
                    catch(Exception e){
                        System.Debug('=Error in update the case : ' + e);
                    }
                    objResponse.isSuccess = true ;
                    objResponse.strMessage = 'Case has been cancelled successfully.';

                    return objResponse;
    }*/

    public class CaseCancellationResponseWrapper {
    	public Boolean isSuccess ;
    	public String strMessage ;

    	CaseCancellationResponseWrapper() {
    		isSuccess = false ;
    		strMessage = 'Case could not be cancelled. Please try again later.';
    	}
    }
}