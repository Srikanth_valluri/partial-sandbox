@isTest
global class DAMAC_SMS_Mock_Submission implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"Status":"OK","Data":{"BulkId":3035532}}');
        res.setStatusCode(200);
        return res;
    }
}