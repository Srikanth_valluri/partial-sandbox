/*
Name:DAMAC_Ziwo_Click_to_Call
Desc:The Controller to initiate a Call from salesforce mobile App Click to Call quickAction.
*/

global class DAMAC_Ziwo_Click_to_Call{
    public String ActivityOutcomes { get; set; }
    public String RMComments { get; set; }
    public String Description { get; set; }
    public String ActivityType3 { get; set; }
    public Date nextActivityDate { get; set; }
   
    global Task taskRecord { get; set; }

    // Constructor to initialize the task record when it is loaded from Task page
    global DAMAC_Ziwo_Click_to_Call(ApexPages.standardController stdController) {
        taskRecord = new Task ();
        try {
            taskRecord = [SELECT Activity_Type_3__c , Next_activity_Date__c, Activity_Outcome__c, RM_Comments__c, Description, whatId, whoId From Task WHERE Id =: stdController.getId()];
        } catch (Exception e) {}
        taskRecord.Activity_Type_3__c = 'Call - Outbound';
        ActivityOutcomes = taskRecord.Activity_Outcome__c;
        RMComments = taskRecord.RM_Comments__c;
        Description = taskRecord.Description ;
        ActivityType3 = taskRecord.Activity_Type_3__c ;
    } 
   
    
    public List<SelectOption> getPickListValuesOfActivityOutcomes(){
       List<SelectOption> pickListValuesList= new List<SelectOption>();
        /*Schema.DescribeFieldResult fieldResult = Task.Activity_Outcome__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }*/
        pickListValuesList.add (new SelectOption ('', '--None--'));
        pickListValuesList.add (new SelectOption ('Not connected', 'Not connected'));
        pickListValuesList.add (new SelectOption ('Meeting Scheduled', 'Meeting Scheduled'));
        pickListValuesList.add (new SelectOption ('Interested, but more follow up needed', 'Interested, but more follow up needed'));
        pickListValuesList.add (new SelectOption ('Not interested', 'Not interested'));
        pickListValuesList.add (new selectOption ('Requested Call back', 'Requested Call back'));
        pickListValuesList.add (new selectOption ('Unreachable or phone switched off', 'Unreachable or phone switched off'));
        pickListValuesList.add (new SelectOption ('Did Not Pick Up Call', 'Did Not Pick Up Call'));
        pickListValuesList.add (new SelectOption ('Redirected to Voicemail', 'Redirected to Voicemail'));
        pickListValuesList.add (new SelectOption ('Qualified', 'Qualified'));
        
             
        return pickListValuesList;
    }
    public List<String> getPickListValuesOfActivityType3 (){
       List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Task.Activity_Type_3__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }     
        return pickListValuesList;
    }
    
    global DAMAC_Ziwo_Click_to_Call() {
        taskRecord = new Task ();
        taskRecord.Activity_Type_3__c = 'Call - Outbound';
    }
    
    
    global String getRelatedToName () {
        try {
            ID relatedToId = taskRecord.whatId;
            if (relatedToId == null) {
                relatedToId = taskRecord.whoId;
            }
            String sObjName = relatedToId.getSObjectType().getDescribe().getName();
            
            Ziwo_Object_Settings__mdt objectMapping = new Ziwo_Object_Settings__mdt ();
            objectMapping = [SELECT 
                                Label, Name_Field_API__c
                             FROM Ziwo_Object_Settings__mdt 
                             WHERE object_API__c =: sObjName LIMIT 1];
            String query = 'SELECT '+objectMapping.Name_Field_API__c+' FROM ' + sObjName+' WHERE ID =: relatedToId';
            Sobject inq =  Schema.getGlobalDescribe().get(sObjName).newSObject() ;
            inq = Database.query (query);
            
            return ''+inq.get (objectMapping.Name_Field_API__c);
        } catch (Exception e) {
            return '';
        }
    }
    
    
   
    @remoteAction 
    global static String taskRelatedToName (ID relatedToId) {
        try {
            String sObjName = relatedToId.getSObjectType().getDescribe().getName();
            
            Ziwo_Object_Settings__mdt objectMapping = new Ziwo_Object_Settings__mdt ();
            objectMapping = [SELECT 
                                Label, Name_Field_API__c
                             FROM Ziwo_Object_Settings__mdt 
                             WHERE object_API__c =: sObjName LIMIT 1];
            String query = 'SELECT '+objectMapping.Name_Field_API__c+' FROM ' + sObjName+' WHERE ID =: relatedToId';
            Sobject inq =  Schema.getGlobalDescribe().get(sObjName).newSObject() ;
            inq = Database.query (query);
            
            return ''+inq.get (objectMapping.Name_Field_API__c);
        } catch (Exception e) {
            System.debug (e.getMessage ());
            return '';
        }
    }
    
    /*
        Name:updateTask
        Desc: To Update the task
    */
    global PageReference updateTask () {
        
        update taskRecord;
        ID whatId = null;
        try {
            whatId = [SELECT whatId FROM Task WHERE Id =: taskRecord.Id].whatId;
        } catch (Exception e) {}
        if (whatId == null) {
            whatId = [SELECT whoId FROM Task WHERE Id =: taskRecord.Id].whoId ;
        }
        
        taskRecord = [SELECT Activity_Type_3__c , Next_activity_Date__c, Activity_Outcome__c, RM_Comments__c, Description, whatId, whoId From Task WHERE Id =: taskRecord.Id];
        if (whatId != null && taskRecord.Next_activity_Date__c != null) {
            
            schema.sObjectType sObjType = Schema.getGlobalDescribe().get('Inquiry__c');
            String inquiryPrefix = sObjType.getDescribe().getKeyPrefix();
            
            if (String.valueOf (whatId).startsWith (inquiryPrefix)) {
                Inquiry__c inquiry = new Inquiry__c ();
                inquiry.Id = whatId;
                inquiry.Next_activity_Date__c = taskRecord.Next_activity_Date__c;
                DAMAC_Constants.skip_InquiryTrigger = true;
                update inquiry;
                DAMAC_Constants.skip_InquiryTrigger = false;
            }
        }
        String homePage = apexpages.currentpage().getparameters().get('homePage');
        if (homePage == 'true')
            return new pageReference ('/apex/ziwo_listener_home');
        else        
            return new PageReference ('/'+whatId);
    }
    global void updateTaskFromPromoter () {
        
        if(ActivityOutcomes != '')
            taskRecord.Activity_Outcome__c = ActivityOutcomes;
        if(RMComments != '')
            taskRecord.RM_Comments__c = RMComments;
        if(Description != '')
            taskRecord.Description = Description;
        if(ActivityType3 != '')
            taskRecord.Activity_Type_3__c  = ActivityType3;
        if(nextActivityDate != null)
            taskRecord.Next_activity_Date__c = nextActivityDate ;
        updateTask ();
    }
    
    /*
        Name:callZiwo 
        Desc:To Initiate call via Ziwo API, Create a Task , Do Chatter Post
    */
    @remoteAction
    global static String callZiwo (Id inqId, String phoneFieldAPI) {
        Ziwo_Settings__c settings = Ziwo_Settings__c.getInstance (userInfo.getUserID());
        
        String sObjName = inqId.getSObjectType().getDescribe().getName();
        
        Ziwo_Object_Settings__mdt objectMapping = new Ziwo_Object_Settings__mdt ();
        objectMapping = [SELECT object_API__c, Default_Phone_Field_API__c, Name_Field_API__c, Phone_Field_API__c 
                         FROM Ziwo_Object_Settings__mdt WHERE object_API__c =: sObjName LIMIT 1];
        
        Sobject inq =  Schema.getGlobalDescribe().get(sObjName).newSObject() ;
        String query = 'SELECT '+objectMapping.Default_Phone_Field_API__c+' ,OwnerId, ';
        
        if (objectMapping.Phone_Field_API__c != NULL) {
            if (objectMapping.Phone_Field_API__c.contains (',')) {
                for (String key : objectMapping.Phone_Field_API__c.split (',')) {
                    if (!query.contains (key))
                        query += key+',';
                }
            }
            else {
                if (!query.contains (objectMapping.Phone_Field_API__c))
                    query += objectMapping.Phone_Field_API__c+',';
            } 
        }
        
        query += objectMapping.Name_Field_API__c+' from '+sObjName+' where id=:inqId';
        
        inq = Database.query (query);
        
        String AgentExtnAndDID = getAgentCLIandNumber(UserInfo.getUserId ()) ;//String.valueOf (inq.get ('ownerId')));
        String AgentExtn = AgentExtnAndDID.substringBefore('|');
        String AgentDID = AgentExtnAndDID.substringAfter('|');
        String CustomerPhone = '';
        
        customerPhone = String.valueOf(inq.get (objectMapping.Default_Phone_Field_API__c));
        if (phoneFieldAPI != null && phoneFieldAPI != '') {
            customerPhone = String.valueOf(inq.get (phoneFieldAPI));            
        }        
        
        String endpoint = settings.Endpoint__c+'integrations/cti/agents/'+AgentExtn+'/call/'+CustomerPhone;
        String bodyToPost = 'callerIDNumber='+AgentDID+'&roamingOnly=true';
        
        System.debug(bodyToPost);
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        
        request.setEndpoint(endpoint);
        request.setMethod('POST');
        request.setheader('api_key', settings.API_key__c);
        request.setheader('Content-Type', 'application/x-www-form-urlencoded');
        request.setBody(bodyToPost);
        request.setTimeOut(120000);
        
        HttpResponse response = new HTTPResponse ();
        if (!Test.isRunningTest ())
            response = http.send(request);
        else {
            response.setStatusCode (200);
            response.setBody ('{"content" : {"callId" : "tsets"}}');
        }
        Task t = new Task();    
        System.debug (response.getBody ());
        // If the request is successful, parse the JSON response.
        if (response.getStatusCode() == 200) {
            system.debug(response.getBody());
            
            DAMAC_Ziwo_Click_Resp_Json parsedResp = DAMAC_Ziwo_Click_Resp_Json.parse(response.getBody());
            system.debug(parsedResp.Content.callID);
            
            String callId = parsedResp.Content.callID;
            
            //Create a task            
            t.CrtObjectId__c = callId;
            t.subject = 'Call - Outbound';
            //t.Activity_Type_3__c = 'Outbound';
            if (String.valueOf (inqId).startsWith ('003')) {
                Contact con = new Contact ();
                try {
                    con = [SELECT AccountId FROM Contact WHERE Id =: inqId];
                } catch (Exception e) {}
                t.whoId = inqId;
                t.whatId = con.accountId;
            } else {
                t.whatid = inqId;
            }
            t.ownerId = userinfo.getuserId(); 
            t.event_type__c = 'Ziwo Outbound';
            upsert t;
            Profile userProfile = [SELECT Name FROM Profile WHERE Id =: userinfo.getProfileId()];
            if(userProfile.Name != 'Promoter Community Profile'){
                doChatterPost(userinfo.getuserId(), t.id, 'Outbound', String.valueOf (inq.get(objectMapping.Name_Field_API__c)));
            }
        }
        return t.id+'####'+taskRelatedToName (inqId);
    }
    
    /*
        Name:getAgentCLIandNumber 
        Desc:To get the agent ziwo extension and DID
    */
    global static String getAgentCLIandNumber(id userId){
        string toReturn = '';
        user u = [select id,phone,Ziwo_Extn__c,Ziwo_DID__c from user where id=:userId LIMIT 1];
        if(u.Ziwo_Extn__c != null){
            toReturn = string.valueof(u.Ziwo_Extn__c)+'|'+string.valueof( u.Ziwo_DID__c );
        }
        return toReturn;
    }
    
    /*
        Name:doChatterPost 
        Desc: for posting the chatter message to agent based on call information
    */
    global static void doChatterPost(id userid, id taskId, string direction, string custName){
        
        Custom_Chatter_Feed__c chatterFeed = new Custom_Chatter_Feed__c ();
        try {
            chatterFeed = [SELECT Name FROM Custom_Chatter_Feed__c WHERE OwnerId =: userId LIMIT 1];
        } catch (Exception e) {}
        upsert chatterFeed;
        
        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
        
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        
        mentionSegmentInput.id = userId;
        messageBodyInput.messageSegments.add(mentionSegmentInput);
        
        String codeSnippet = '\n New '+direction+' call to '+custName+' is completed';
        
        
        textSegmentInput.text = codeSnippet;
        messageBodyInput.messageSegments.add(textSegmentInput);
        
        feedItemInput.body = messageBodyInput;
        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        ConnectApi.LinkCapabilityInput linkInput = new ConnectApi.LinkCapabilityInput();
        //linkInput.url = baseUrl+'/apex/ziwo_inbound_wrapup?id='+taskId;
        linkInput.url = '/'+taskId;
        linkInput.urlName = 'Wrap up the call';
        
        ConnectApi.FeedElementCapabilitiesInput feedElementCapabilitiesInput = new ConnectApi.FeedElementCapabilitiesInput();
        feedElementCapabilitiesInput.link = linkInput;
        feedItemInput.capabilities = feedElementCapabilitiesInput;
        
        
        feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
        feedItemInput.subjectId = chatterFeed.Id;
        
        //ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput, null);
        if (!Test.isRunningTest ())
            ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
        
    }
}