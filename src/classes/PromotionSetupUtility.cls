/***********************************************************************************************
* Name               : PromotionSetupUtility
* Test Class         : PromotionSetupUtilityTest
* Description        : Controller class for PromotionSetup VF Page
* Created Date       : 20/01/2021
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst         20/01/2021      Initial Draft.
**************************************************************************************************/
global With Sharing class PromotionSetupUtility {

    public static Map<Id, List<Promotion_Setup__c>> fetchAvailablePromotionsMap(List<Id> invIds){
        Map<Id, List<Promotion_Setup__c>> availablePromoMap = new Map<Id, List<Promotion_Setup__c>> ();
        List<Promotion_Setup__c> promoList = fetchActivePromotions();
        List<String> invFieldsToQuery = new List<String>();
        invFieldsToQuery = fetchinvFieldsToQuery(promoList);
        system.debug('invFieldsToQuery: ' + invFieldsToQuery);
        if(invFieldsToQuery.size() > 0){
            Map<Id, Inventory__c> invMap;
            invMap = fetchInventoryList(invFieldsToQuery, invIds); 
            for(Id invId: invIds){
                List<Promotion_Setup__c> availablePromoList = new List<Promotion_Setup__c> ();
                for(Promotion_Setup__c promo: promoList){
                    if(checkPromoAvailable(invMap.get(invId), promo, invFieldsToQuery)){
                        availablePromoList.add(promo);
                    }
                }  
                availablePromoMap.put(invId, availablePromoList);
            }
        }
        return availablePromoMap;
    }
    
    public static Map<Id, List<Promotion_Setup__c>> fetchAvailablePromotionsMap(List<Id> invIds, 
                            NSIBPM__Service_Request__c sr, List<Buyer__c> buyerList){
        system.debug('sr:' + sr);
        system.debug('buyerList:' + buyerList);
        Map<Id, List<Promotion_Setup__c>> availablePromoMap = new Map<Id, List<Promotion_Setup__c>> ();
        List<Promotion_Setup__c> promoList = fetchActivePromotions();
        List<String> invFieldsToQuery = new List<String>();
        invFieldsToQuery = fetchinvFieldsToQuery(promoList);
        system.debug('invFieldsToQuery: ' + invFieldsToQuery);
        if(invFieldsToQuery.size() > 0){
            Map<Id, Inventory__c> invMap;
            invMap = fetchInventoryList(invFieldsToQuery, invIds); 
            for(Id invId: invIds){
                List<Promotion_Setup__c> availablePromoList = new List<Promotion_Setup__c> ();
                for(Promotion_Setup__c promo: promoList){
                    if(checkPromoAvailableAll(invMap.get(invId), promo, invFieldsToQuery, sr, buyerList)){
                        availablePromoList.add(promo);
                    }
                }  
                availablePromoMap.put(invId, availablePromoList);
            }
        }
        return availablePromoMap;
    }

    public static List<Promotion_Setup__c> fetchAvailablePromotions(Id invId){
        List<Promotion_Setup__c> availablePromoList = new List<Promotion_Setup__c> ();
        List<Promotion_Setup__c> promoList = new List<Promotion_Setup__c> ();
        List<String> invFieldsToQuery = new List<String>();
        promoList = fetchActivePromotions();
        invFieldsToQuery = fetchinvFieldsToQuery(promoList);
        system.debug('invFieldsToQuery: ' + invFieldsToQuery);
        if(invFieldsToQuery.size() > 0){
            Inventory__c inv;
            inv = fetchInventory(invFieldsToQuery, invId); 
            for(Promotion_Setup__c promo: promoList){
                if(checkPromoAvailable(inv, promo, invFieldsToQuery)){
                    availablePromoList.add(promo);
                }
            }   
        }
        
        system.debug('availablePromoList: ' + availablePromoList);
        return availablePromoList;    
    }
    
    public static Boolean checkPromoAvailable(Inventory__c inv, Promotion_Setup__c promo, List<String>  invFieldsToQuery){
        Boolean promoAvailable = false;
        Map<String, Boolean> orderMatchMap = new Map<String, Boolean>();
        for(Promotion_Eligibility_Criteria__c criteria: promo.Promotion_Eligibility_Criteria__r){
            Boolean criteriaMatch = false;
            try{
                if(criteria.object__c.toLowerCase() == 'inventory__c'){
                    if(invFieldsToQuery.contains(criteria.parameter__c.toLowerCase())){
                        criteriaMatch = checkPromoCriteria(String.valueOf(inv.get(criteria.parameter__c)).toLowerCase(), criteria);
                    }
                }
            } catch (exception e){
                system.debug('Exception while Checking Criteria: ' + e);
            }
            orderMatchMap.put(String.valueOf(criteria.Order__c), criteriaMatch);
        }
        String logic = promo.Eligibility_Inventory_Filter_Logic__c;
        //logic = fetchUpdatedLogic(logic, orderMatchMap);
        //promoAvailable = getFinalBooleanValue(logic);
        promoAvailable = fetchUpdatedBoolean(logic, orderMatchMap);
        system.debug('promoAvailable - ' + promo.Promotion_Name__c + ' - '  + promoAvailable);
        return promoAvailable;
    }

    public static Boolean checkPromoAvailableAll(Inventory__c inv, Promotion_Setup__c promo, List<String>  invFieldsToQuery,
                NSIBPM__Service_Request__c sr, List<Buyer__c> buyerList){
        Boolean promoAvailable = false;
        Map<String, Boolean> orderMatchMap = new Map<String, Boolean>();
        for(Promotion_Eligibility_Criteria__c criteria: promo.Promotion_Eligibility_Criteria__r){
            Boolean criteriaMatch = false;
            try{
                if(criteria.object__c.toLowerCase() == 'inventory__c'){
                    if(invFieldsToQuery.contains(criteria.parameter__c.toLowerCase())){
                        criteriaMatch = checkPromoCriteria(String.valueOf(inv.get(criteria.parameter__c)).toLowerCase(), criteria);
                    }
                } else if(criteria.object__c.toLowerCase() == 'buyer__c'){
                    for(Buyer__c buyer: buyerList){
                        if(buyer.get(criteria.parameter__c.toLowerCase()) != null){
                            criteriaMatch = checkPromoCriteria(String.valueOf(buyer.get(criteria.parameter__c.toLowerCase())).toLowerCase(), criteria);
                        }
                    }
                } else if(criteria.object__c.toLowerCase() == 'nsibpm__service_request__c'){
                    if(sr.get(criteria.parameter__c.toLowerCase()) != null){
                        criteriaMatch = checkPromoCriteria(String.valueOf(sr.get(criteria.parameter__c)).toLowerCase(), criteria);
                    }
                }
            } catch (exception e){
                system.debug('Exception while Checking Criteria: ' + e);
            }
            orderMatchMap.put(String.valueOf(criteria.Order__c), criteriaMatch);
        }
        String logic = promo.Eligibility_Criteria_Filter_Logic__c;
        //logic = fetchUpdatedLogic(logic, orderMatchMap);
        //promoAvailable = getFinalBooleanValue(logic);
        promoAvailable = fetchUpdatedBoolean(logic, orderMatchMap);
        system.debug('promoAvailable - ' + promo.Promotion_Name__c + ' - '  + promoAvailable);
        return promoAvailable;
    }
        
    public static Boolean getFinalBooleanValue(String logic){
        Boolean criteriaMatch = false;
        return criteriaMatch;
    }
    


    public static Boolean fetchUpdatedBoolean(String logic, Map<String,Boolean> orderMatchMap){
        system.debug('logic: ' + logic);
        Boolean result = false;
        
        if(logic != null && logic != ''){
            if(isNumeric(logic)){
                if(orderMatchMap.containsKey(logic) && orderMatchMap.get(logic) != null){
                    result = orderMatchMap.get(logic);
                } else {
                    result = false;
                }
            } else {
                Boolean firstVal, secVal;
                String op;
                Integer skip = 0;
                String insideLogic = '';
                for(String val: logic.split(' ')){
                  //  system.debug('firstVal:'  + firstVal + ', op: ' + op + ', secVal:'  + secVal 
                   //         + ', val:'  + val + ', skip:'  + skip + ', insideLogic:'  + insideLogic);
                    if(skip == 0){
                        if(isNumeric(val)){
                            if(orderMatchMap.containsKey(val) && orderMatchMap.get(val) != null){
                                if(firstVal == null){
                                    firstVal = orderMatchMap.get(val);
                                } else if(firstVal != null && op != null && secVal == null){
                                    secVal = orderMatchMap.get(val);
                                    result = getBooleanResult (op, firstVal, secVal);
                                    firstVal = result;
                                    op = null;
                                    secVal = null;
                                } else{
                                    return false;
                                }
                            }
                        } else if(val.toLowerCase() == 'and' || val.toLowerCase() == 'or' && op == null){
                            op = val.toLowerCase();
                        } else if(val.startsWith('(')){
                            if(skip == 0){
                                if(val.length() > 1){
                                    insideLogic +=  ' ' + val.substring(1, val.length());
                                }
                            } else{
                                insideLogic +=  ' ' + val;
                            }
                            skip = skip + val.countMatches('(');
                        } 
                    } else{
                        if(val.startsWith('(')){
                            insideLogic +=  ' ' + val;
                            skip = skip + val.countMatches('(');
                        } else if(val.endsWith(')')){
                            skip = skip - val.countMatches(')');
                            if(skip == 0){
                                if(val.length() > 1){
                                    insideLogic +=  ' ' + val.substring(0, val.length()-1);
                                }
                                Boolean insideVal = fetchUpdatedBoolean(insideLogic, orderMatchMap);
                                if(firstVal == null){
                                    firstVal = insideVal;
                                } else if(firstVal != null && op != null && secVal == null){
                                    secVal = insideVal;
                                    result = getBooleanResult (op, firstVal, secVal);
                                    firstVal = result;
                                    op = null;
                                    secVal = null;
                                }
                                
                                insideLogic = '';
                            } else{
                                insideLogic += ' ' +  val ; 
                            }                            
                        } else{
                            insideLogic += ' ' +  val ; 
                        }
                    }
                    if(firstVal != null && secVal == null && op == null){
                        result = firstVal;
                    }
                  //  system.debug('firstVal2:'  + firstVal + ', op2: ' + op + ', secVal2:'  + secVal 
                    //        + ', val2:'  + val + ', skip2:'  + skip + ', insideLogic2:'  + insideLogic);
                }
            }
        }
        system.debug('logic:'  + logic + ', orderMatchMap: ' + orderMatchMap + ', result:'  + result);
        return result;
    }
    
    public static Boolean getBooleanResult (String op, Boolean val1, Boolean val2){
        Boolean result;
        
        if(op == 'and'){
            result = val1 && val2;
        } else if(op == 'or'){
            result = val1 || val2;
        } else{
            result = false;
        }
        system.debug('op:'  + op + ', val1: ' + val1 + ', val2:'  + val2 + ', result:'  + result);
        return result;
    }
    
    public static Boolean checkPromoCriteria(String fieldVal, Promotion_Eligibility_Criteria__c criteria){
        Boolean criteriaMatch = false;
        if(criteria.Value_Type__c == 'Text'){
            try{
                return checkTextValue(criteria, fieldVal);
            } catch (exception e){
                system.debug('Exception while Checking Text Criteria: ' + e);
            }
        } else if(criteria.Value_Type__c == 'Boolean'){
            try{
                return checkBooleanValue(criteria, Boolean.valueOf(fieldVal));
            } catch (exception e){
                system.debug('Exception while Checking Text Criteria: ' + e);
            }
        } else if(criteria.Value_Type__c == 'Number'){
            try{
                return checkNumberValue(criteria, Decimal.valueOf(fieldVal));
            } catch (exception e){
                system.debug('Exception while Checking Text Criteria: ' + e);
            }
        }
        return criteriaMatch;
    }

    public static Boolean checkTextValue(Promotion_Eligibility_Criteria__c criteria, String invVal){
        String val = criteria.value__c.toLowerCase();
        List<String> valList = val.split(';');
        if(criteria.Condition__c == '='){
            if(valList.contains(invVal)){
                return true; 
            } else{
                return false;
            }
        } else if(criteria.Condition__c == '!='){
            if(!valList.contains(invVal)){
                return true; 
            } else{
                return false;
            }
        } else if(criteria.Condition__c.toLowerCase() == 'like'){
            Boolean retVal = false;
            for(String value: valList){
                if(invVal.contains(value)){
                    retVal = true; 
                } 
            }
            return retVal;
        } else if(criteria.Condition__c.toLowerCase() == 'not like'){
            Boolean retVal = true;
            for(String value: valList){
                if(invVal.contains(value)){
                    retVal = false; 
                } 
            }
            return retVal;
        }
        return false;
    }

    public static Boolean checkBooleanValue(Promotion_Eligibility_Criteria__c criteria, Boolean invVal){
        String val = criteria.value__c.toLowerCase();
        if(criteria.Condition__c == '='){
            if((val == 'true' && invVal) || (val == 'false' && !invVal)){
                return true; 
            } else{
                return false;
            }
        } else if(criteria.Condition__c == '!='){
            if((val == 'false' && invVal) || (val == 'true' && !invVal)){
                return true; 
            } else{
                return false;
            }
        } else{
            return false;
        }
    }    
    public static Boolean checkNumberValue(Promotion_Eligibility_Criteria__c criteria, Decimal invVal){
        Decimal val = Decimal.valueOf(criteria.value__c);
        if(criteria.Condition__c == '='){
            if(val == invVal){
                return true; 
            } else{
                return false;
            }
        } else if(criteria.Condition__c == '!='){
            if(val != invVal){
                return true; 
            } else{
                return false;
            }
        } else if(criteria.Condition__c == '<'){
            if(val < invVal){
                return true; 
            } else{
                return false;
            }
        } else if(criteria.Condition__c == '>'){
            if(val > invVal){
                return true; 
            } else{
                return false;
            }
        } else if(criteria.Condition__c == '<='){
            if(val <= invVal){
                return true; 
            } else{
                return false;
            }
        } else if(criteria.Condition__c == '>='){
            if(val >= invVal){
                return true; 
            } else{
                return false;
            }
        } else{
            return false;
        }
    }
    
    public static Map<Id, Inventory__c> fetchInventoryList(List<String> invFieldsToQuery, List<Id> invIds){
        Map<Id, Inventory__c> invMap = new  Map<Id, Inventory__c>();
        String query = 'SELECT ' + String.join(invFieldsToQuery, ', ');
        query += ' FROM Inventory__c WHERE Id IN: invIds';
        for(Inventory__c inventory: Database.query(query)){
            invMap.put(inventory.Id, inventory);
        }
        return invMap;
    }
    

    public static Inventory__c fetchInventory(List<String> invFieldsToQuery, Id invId){
        Inventory__c inv;
        String query = 'SELECT ' + String.join(invFieldsToQuery, ', ');
        query += ' FROM Inventory__c WHERE Id =: invId';
        for(Inventory__c inventory: Database.query(query)){
            inv = inventory;
        }
        system.debug('inv: ' + inv);
        return inv;
    }
    
    public static List<Promotion_Setup__c> fetchActivePromotions(){
        List<Promotion_Setup__c> promoList = new List<Promotion_Setup__c> ();
        for(Promotion_Setup__c promo: [SELECT Id, Promotion_Name__c, Promotion_Type__c, Adjust_Cost_to_Price__c,
                                        Type__c, Sub_Type__c, Eligibility_Inventory_Filter_Logic__c,
                                        Eligibility_Criteria_Filter_Logic__c, 
                                        (SELECT Id, Name, Condition__c,
                                            Object__c, Order__c,
                                            Parameter__c, Promotion_Setup__c,
                                            Value__c, Value_Type__c
                                         FROM Promotion_Eligibility_Criteria__r)
                                        FROM Promotion_Setup__c
                                        WHERE Active__c = TRUE
                                        and id = 'a9N7Z0000004J3t']){
            if(promo.Eligibility_Inventory_Filter_Logic__c != null
                    && promo.Eligibility_Inventory_Filter_Logic__c != ''){
                promoList.add(promo);
            }
        }
        return promoList;
    }
    
    public static List<String> fetchinvFieldsToQuery(List<Promotion_Setup__c> promoList){
        List<String> invFieldsToQuery = new List<String>();
        for(Promotion_Setup__c promo: promoList){
            if(promo.Eligibility_Inventory_Filter_Logic__c != null
                    && promo.Eligibility_Inventory_Filter_Logic__c != ''){
                
                for(String val: promo.Eligibility_Inventory_Filter_Logic__c.split(' ')){
                    if(val != null && val != '' && isNumeric(val)){
                        invFieldsToQuery = updateinvFieldsToQuery(invFieldsToQuery, promo, val);
                    } else{
                        for(String val2: val.split('\\(')){
                            if(val2 != null && val2 != '' && isNumeric(val2)){
                                invFieldsToQuery = updateinvFieldsToQuery(invFieldsToQuery, promo, val2);
                            } else{
                                for(String val3: val2.split('\\)')){
                                   if(val3 != null && val3 != '' && isNumeric(val3)){
                                        invFieldsToQuery = updateinvFieldsToQuery(invFieldsToQuery, promo, val3);
                                    }
                                }
                            }
                        }
                    }
                }    
            }   
        }
        return invFieldsToQuery;
    }
    
    public static List<String> updateinvFieldsToQuery(List<String> invFieldsToQuery, Promotion_Setup__c promo, String val){
        String fieldAPI = '';
        if(val != null && val != '' && isNumeric(val)){
            for(Promotion_Eligibility_Criteria__c criteria: promo.Promotion_Eligibility_Criteria__r){
                if(String.valueOf(criteria.order__c) == val
                        && criteria.Object__c == 'Inventory__c' 
                        && criteria.parameter__c != null 
                        && criteria.parameter__c != ''){
                    fieldAPI = criteria.parameter__c.toLowerCase();
                    if(fieldAPI != null && fieldAPI != ''  && !invFieldsToQuery.contains(fieldAPI)) {
                        invFieldsToQuery.add(fieldAPI);
                    }
                }
            }
        }
        return invFieldsToQuery;
    }
    
     public static Boolean isNumeric(String s){
        Boolean ReturnValue;
        try{
            Decimal.valueOf(s);
            ReturnValue = TRUE; 
        } catch (Exception e) {
            ReturnValue = FALSE;
        }
        return ReturnValue;
    }
}