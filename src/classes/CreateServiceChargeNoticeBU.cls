public class CreateServiceChargeNoticeBU implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful{
    
     private String query;
     //public String SERVER_URL;
     //public String SESSION_ID;
     
     public Database.Querylocator start( Database.BatchableContext bc ) {
        //login();
        query = 'SELECT Id, Service_Notice_Sent_Date__c, FM_Service_Notice__c, ' +
                        ' Booking__r.Account__r.Mobile_Phone_Encrypt__pc,Building_Name__c,Unit_Name__c, ' +
                        ' FM_Outstanding_Amount__c, Registration_ID__c, Multiple_Units__c, Earliest_Payment_Date__c, '+
                        ' Booking__r.Account__r.Name, Total_Payment_Amount__c FROM Booking_Unit__c '+
                        ' WHERE FM_Service_Notice__c = true AND Multiple_Units__c != null And '+
                        ' Earliest_Payment_Date__c != null AND Total_Payment_Amount__c != null ';
            //And Service_Notice_Sent_Date__c = null
        return Database.getQueryLocator(query);  
     }
     
     public void execute(Database.BatchableContext info, List<Booking_Unit__c> lstBu ){
         list<Booking_Unit__c> lstUpdatedBU = new list<Booking_Unit__c>();
         for (Booking_Unit__c objBU : lstBu){
             if (objBU.Registration_ID__c != null){
                 FmIpmsRestServices.DueInvoicesResult dues = 
                    FmIpmsRestServices.getDueInvoices(objBu.Registration_ID__c,'', '');
                 system.debug('dues'+dues);
                 if(dues != null && String.isNotBlank(dues.totalDueAmount)) {
                     objBu.FM_Outstanding_Amount__c = dues.totalDueAmount;
                     lstUpdatedBU.add(objBU);
                 }
             }
         }// end of for
         
         if (lstUpdatedBU != null && !lstUpdatedBU.isEmpty()){
             update lstUpdatedBU;
             system.debug('maza lstUpdatedBU'+lstUpdatedBU);
             if(!Test.isRunningTest()){
                 System.enqueueJob(new DrawloopQueuebleClass(lstUpdatedBU));
             }
         }
     }
     
     public void finish( Database.BatchableContext bc ) {
        
    }
    
    //Method used for login purpose to get the session id
    /*public void login(){
        Admin_Login_for_Drawloop__c mc = Admin_Login_for_Drawloop__c.getOrgDefaults();
        Http h = new Http(); 
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://' + mc.Domain__c + '.salesforce.com/services/Soap/u/28.0');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        request.setHeader('SOAPAction', '""');
        request.setBody('<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Header/><Body><login xmlns="urn:partner.soap.sforce.com"><username>' + mc.Username__c + '</username><password>' + mc.Password__c + '</password></login></Body></Envelope>');
        
        HttpResponse response = h.send(request);
        system.debug('----Response Body-----'+response.getBody());
        
        Dom.XmlNode resultElmt = response.getBodyDocument().getRootElement()
        .getChildElement('Body','http://schemas.xmlsoap.org/soap/envelope/')
        .getChildElement('loginResponse','urn:partner.soap.sforce.com')
        .getChildElement('result','urn:partner.soap.sforce.com');
        
        SERVER_URL = resultElmt.getChildElement('serverUrl','urn:partner.soap.sforce.com').getText().split('/services')[0];
        SESSION_ID = resultElmt.getChildElement('sessionId','urn:partner.soap.sforce.com').getText();
        
        system.debug('--SERVER_URL---'+SERVER_URL);
        system.debug('--SESSION_ID---'+SESSION_ID);
        if(Test.isRunningTest()){
            SESSION_ID = 'none';
        }
    }*/
}