public without sharing class BookingUnitDataProvider implements DataProvider {

    public DataDisplayConfig getData(DataDisplayConfig config) {
        List<Booking_Unit__c> lstBookingUnit = new List<Booking_Unit__c>();
        Set<String> keys = Booking_Unit_Active_Status__c.getAll().keyset();
        if (config.fieldList != NULL) {
            String fields = 'Id,';
            Set<String> setFields = new Set<String>();
            Schema.SObjectType objectToken = Schema.getGlobalDescribe().get(config.objectName);
            if (objectToken != NULL) {
                setFields = objectToken.getDescribe().fields.getMap().keySet();
            }
            for (Map<String,String> fieldMap : config.fieldList) {
                String fieldName = fieldMap.get(DataRendererUtils.FIELD_NAME);
                if (String.isNotBlank(fieldName) && !fields.containsIgnoreCase(fieldName + ',')) {
                    fields += fieldName + ',';
                }
                String referencedFields = fieldMap.get(DataRendererUtils.REFERENCED_FIELDS);
                if (String.isNotBlank(referencedFields)) {
                    for (String field : referencedFields.toLowerCase().split(',')) {
                        if (setFields.contains(field) && !fields.containsIgnoreCase(field + ',')) {
                            fields += field + ',';
                        }
                    }
                }
            }
            fields = fields.removeEnd(',');
            System.debug( 'BookingUnitDataProvider query = \n' +
                ' SELECT ' + fields + ' FROM Booking_Unit__c '
                + ' WHERE  Booking__r.Account__c  = \''
                        + CustomerCommunityUtils.customerAccountId + '\''
                + (String.isBlank(config.filter) ? '' : ' AND ' + config.filter)
                + ' AND '+'Registration_Status__c IN :keys'
                + (String.isBlank(config.recordLimit) ? '' : ' LIMIT ' + config.recordLimit)
            );
            lstBookingUnit = Database.query(
                ' SELECT ' + fields + ' FROM Booking_Unit__c '
                + ' WHERE  Booking__r.Account__c  = \''
                        + CustomerCommunityUtils.customerAccountId + '\''
                + (String.isBlank(config.filter) ? '' : ' AND ' + config.filter)
                + ' AND '+'Registration_Status__c IN :keys'
                + (String.isBlank(config.recordLimit) ? '' : ' LIMIT ' + config.recordLimit)
            );
        }

        config.dataList = DataRendererUtils.wrapData(config, lstBookingUnit);

        return config;
    }

}