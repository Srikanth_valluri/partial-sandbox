/*
To Generate the Bitly short url for the stand inquiries
*/

public class DAMAC_Get_Bitly_URL_Stand_Inquiries{
    @Future (callout=TRUE)
    public static void generateShortURL(set<id> standInquiryIds) {
        boolean processComplete = false;
        list<stand_inquiry__c> standinqtoUpdate = new list<stand_inquiry__c>();
        for(stand_inquiry__c st:[select id,Name from stand_inquiry__c where id in:standInquiryIds]){                        
            string urlParams = Label.Whatsapp_URL+' '+st.Name+' '+Label.Whatsapp_Default_Message;
            string shortUrl = DAMAC_BITLYURL.getShortenedURL(urlParams);
            system.debug(shortUrl);
            st.Whats_App_Short_URL__c = shortUrl;
            standinqtoUpdate.add(st);
        }    
        update standinqtoUpdate;
    }




}