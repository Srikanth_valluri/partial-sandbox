/*
* Description - Test class developed for 'PaymentMade1'
*
* Version            Date            Author            Description
* 1.0                29/11/17        Monali            Initial Draft
*/
@isTest
private class AsyncPaymentMade1Test {
    static testMethod void testMethod1() {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='12345');
        insert objAcc;

        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        Booking__c objBooking = new Booking__c(Account__c=objAcc.Id, Deal_SR__c=dealSR.Id);
        insert objBooking;

        Booking_Unit__c BUObj = new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='Test name',
        Registration_ID__c = '92061', Registration_Status__c = 'Active Status', Unit_Selling_Price_AED__c = 100);
        insert BUObj;

        Test.startTest();

        PaymentMade2.APPSXXDC_PROCESS_SERX1794747X2X4 regTerms = new PaymentMade2.APPSXXDC_PROCESS_SERX1794747X2X4();
            regTerms.ATTRIBUTE1= '';
			regTerms.ATTRIBUTE10= '';
			regTerms.ATTRIBUTE11= '';
			regTerms.ATTRIBUTE12= '';
			regTerms.ATTRIBUTE13= '';
			regTerms.ATTRIBUTE14= '';
			regTerms.ATTRIBUTE15= '';
			regTerms.ATTRIBUTE16= '';
			regTerms.ATTRIBUTE17= '';
			regTerms.ATTRIBUTE18= '';
			regTerms.ATTRIBUTE19= '';
			regTerms.ATTRIBUTE2= '';
			regTerms.ATTRIBUTE20= '';
			regTerms.ATTRIBUTE21= '';
			regTerms.ATTRIBUTE22= '';
			regTerms.ATTRIBUTE23= '';
			regTerms.ATTRIBUTE24= '';
			regTerms.ATTRIBUTE25= '';
			regTerms.ATTRIBUTE26= '';
			regTerms.ATTRIBUTE27= '';
			regTerms.ATTRIBUTE28= '';
			regTerms.ATTRIBUTE29= '';
			regTerms.ATTRIBUTE3= '';
			regTerms.ATTRIBUTE30= '';
			regTerms.ATTRIBUTE31= '';
			regTerms.ATTRIBUTE32= '';
			regTerms.ATTRIBUTE33= '';
			regTerms.ATTRIBUTE34= '';
			regTerms.ATTRIBUTE35= '';
			regTerms.ATTRIBUTE36= '';
			regTerms.ATTRIBUTE37= '';
			regTerms.ATTRIBUTE38= '';
			regTerms.ATTRIBUTE39= '';
			regTerms.ATTRIBUTE4= '';
			regTerms.ATTRIBUTE41= '';
			regTerms.ATTRIBUTE42= '';
			regTerms.ATTRIBUTE43= '';
			regTerms.ATTRIBUTE44= '';
			regTerms.ATTRIBUTE45= '';
			regTerms.ATTRIBUTE46= '';
			regTerms.ATTRIBUTE47= '';
			regTerms.ATTRIBUTE48= '';
			regTerms.ATTRIBUTE49= '';
			regTerms.ATTRIBUTE5= '';
			regTerms.ATTRIBUTE50= '';
			regTerms.ATTRIBUTE6= '';
			regTerms.ATTRIBUTE7= '';
			regTerms.ATTRIBUTE8= '';
			regTerms.ATTRIBUTE9= '';
			regTerms.PARAM_ID= BUObj.Registration_ID__c;

        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT());
        //System.Continuation continuation,String P_REQUEST_NUMBER,String P_REQUEST_NAME,String P_SOURCE_SYSTEM,PaymentMade2.APPSXXDC_PROCESS_SERX1794747X2X4 regTerms
        AsyncPaymentMade1.AsyncCustomerPaymentHttpSoap11Endpoint objAsync =  new AsyncPaymentMade1.AsyncCustomerPaymentHttpSoap11Endpoint();
        AsyncPaymentMade1.customerPaymentMadeResponse_elementFuture objAsync1 =  new AsyncPaymentMade1.customerPaymentMadeResponse_elementFuture();
        Continuation cont = new Continuation(60);
		objAsync1 = objAsync.beginCustomerPaymentMade(cont,'2-'+String.valueOf( Datetime.now().getTime()),'GET_CUSTOMER_PAYMENT_MADE','SFDC', regTerms);

		//AsyncPaymentMade1.customerPaymentMadeResponse_elementFuture obj1 = new AsyncPaymentMade1.customerPaymentMadeResponse_elementFuture();
		//String res = obj1.getValue();

        Test.stopTest();
    } 
}