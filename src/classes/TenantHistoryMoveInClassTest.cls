@isTest
private class TenantHistoryMoveInClassTest {
	@testSetup static void createTestData() {
		Account objAccTenant = new Account();
		objAccTenant = TestDataFactory_CRM.createPersonAccount();
		insert objAccTenant;
		Account objAccOwner = new Account();
		objAccOwner = TestDataFactory_CRM.createPersonAccount();
		objAccOwner.party_ID__C = '4545';
		insert objAccOwner;
		Location__c objLoc = new Location__c();
		objLoc = TestDataFactoryFM.createLocation();
		insert objLoc;
		NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAccOwner);
		insert objSR;
		Booking__c objBooking = TestDataFactoryFM.createBooking(objAccOwner, objSR);
		insert objBooking;
		Booking_unit__c objBU = TestDataFactoryFM.createBookingUnit(objAccOwner, objBooking);
		insert objBU;
		FM_Case__c obj = new FM_Case__c();
		obj.Origin__c = 'Walk-In';
		obj.Tenant__c = objAccTenant.Id;
		obj.Account__c = objAccOwner.Id;
		obj.Actual_move_in_date__c = Date.today();
		obj.Actual_move_out_date__c = Date.today();
		obj.Booking_Unit__c = objBU.Id;
        insert obj;
		Tenant_History__c objTH = new Tenant_History__c();
		objTH.Tenant__c = objAccTenant.Id;
		objTH.Owner__c = objAccOwner.Id;
		objTH.Lease_Start_Date__c = Date.today();
		objTH.Lease_End_Date__c = Date.today().addDays(1);
		objTH.Booking_Unit__c = objBU.Id;
		insert objTH;
	}

	@isTest static void test_method_one() {
		List<FM_Case__c> lstFMCases = new List<FM_Case__c>();
		lstFMCases = [SELECT Id,Actual_move_in_date__c,Booking_Unit__c,
							Account__c,
							Tenant__c,
							Actual_move_out_date__c
		 				from FM_Case__c];
		Test.startTest();
		TenantHistoryMoveInClass.UpdateTenantHistory(lstFMCases);
		Test.stopTest();
	}

}