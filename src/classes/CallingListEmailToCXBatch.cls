/******************************************************************************************************************************************
Description: Batch to send reminders for PTP date
===========================================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By  | Comments
-------------------------------------------------------------------------------------------------------------------------------------------
1.0     | 30-12-2020        | Akrati Jain  | 1. Initial draft
*******************************************************************************************************************************************/

global class CallingListEmailToCXBatch implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful {
    
    public  String mail;
    public  Date PTP_Date;
    Map<String, SendGrid_Email_Details__mdt> mapSendGridDetails;
    Map<String,EmailTemplate> mapEmailTemplates;
    Map<String, String> mapUserNametoEmail;
    List<EmailMessage> lstEmails;
    List<Calling_List__c> listWelcomeClToUpdate;
    List<Calling_List__c> callingList = new List<Calling_List__c>();
    Exception[] errors = new Exception[0];
    
    
    public CallingListEmailToCXBatch() {
        getConfigs();
    }
    
    public database.querylocator start(Database.BatchableContext BC)
    {
        
        string query = 'select id,Customer_Secondary_Email__c,Account__r.Person_Business_Email__c,IsHideFromUI__c,OwnerId,Account__r.Id,Booking_Unit__r.Id,Booking_Unit__r.name,Promise_To_Pay_Date__c,Booking_Unit__r.Unit_Name__c from Calling_List__c where RecordType.Name = \'Collections Calling List\'  AND Promise_To_Pay_Date__c != null AND Account__r.Id != null AND Booking_Unit__r.Id != null AND Call_Outcome__c  =\'Promise to Pay\' AND IsHideFromUI__c = false';
        return Database.getQueryLocator(query);      
    }
    
    public void execute(Database.BatchableContext BC, List<Calling_List__c> QueryData)
    {
        try{
            lstEmails = new List<EmailMessage>();
            listWelcomeClToUpdate = new List<Calling_List__c>();
            mapUserNametoEmail = new Map<String,String>();
           // getUserEmailIds( QueryData );
            if(QueryData != null ){
                for(Calling_List__c cList : QueryData){
                    if(cList.OwnerId != null && cList.IsHideFromUI__c ==false){
                        mail = cList.Booking_Unit__r.name;
                        if(Date.today().addDays(2) == cList.Promise_To_Pay_Date__c ){
                            callingList.add(cList);
                            sendmail( callingList );
                        }
                    }
                }
            }
            //Update call count on calling list
            if( !listWelcomeClToUpdate.isEmpty() ) {
                update listWelcomeClToUpdate;
            }
            //insert email message
            if( !lstEmails.isEmpty() && !Test.isRunningTest() ) {
                insert lstEmails;
            }
        }
        catch(Exception e) {
            errors.add(e);
            system.debug('error:'+e);
        }
    }
    
    public void finish(Database.BatchableContext BC)
    {
       
    }
    
    public void getConfigs() {
        Set<String> setEmailTemplateNames = new Set<String>();
        mapSendGridDetails= new Map<String,SendGrid_Email_Details__mdt>();
        mapEmailTemplates = new Map<String,EmailTemplate>();
        
        //get configured sendrid details
        for(SendGrid_Email_Details__mdt mdt : [SELECT
                                               MasterLabel
                                               , From_Address__c
                                               , From_Name__c
                                               , To_Address__c
                                               , To_Name__c
                                               , CC_Address__c
                                               , CC_Name__c
                                               , Bcc_Address__c
                                               , Bcc_Name__c
                                               , Reply_To_Address__c
                                               , Reply_To_Name__c
                                               , Email_Template__c
                                               FROM
                                               SendGrid_Email_Details__mdt
                                               WHERE
                                               MasterLabel Like '%PTP Due Reminder%'
                                              ] ) {
                                                  if( mdt.MasterLabel.contains( 'PTP' ) ) {
                                                      mapSendGridDetails.put( 'PTP', mdt );
                                                      setEmailTemplateNames.add( mdt.Email_Template__c );
                                                  }
                                                  
                                              }
        if( !setEmailTemplateNames.isEmpty() ) {
            //Get Email Template
            for( EmailTemplate templateObj : [SELECT 
                                              Id
                                              , Name  
                                              , Subject  
                                              , Body
                                              , HtmlValue
                                              , TemplateType
                                              FROM 
                                              EmailTemplate 
                                              WHERE 
                                              Name IN: setEmailTemplateNames
                                             ]) {
                                                 if( mapSendGridDetails.get( 'PTP' ).Email_Template__c.equalsIgnoreCase( templateObj.name ) ) {
                                                     mapEmailTemplates.put( 'PTP', templateObj );
                                                 }
                                                 
                                             }
        }
    }
    
    
   public void sendmail( list<Calling_List__c> objClist ) {
        for(Calling_List__c objCl:objClist){
            //Set toAddress
            system.debug('mail to send :'+objCl.Account__r.Person_Business_Email__c);
            String toAddress = '';
        
            system.debug('mail :'+objCl.Account__r.Person_Business_Email__c);
           if( String.isNotBlank( objCl.Account__r.Person_Business_Email__c ) ) {
                toAddress += objCl.Account__r.Person_Business_Email__c;
            }
           
            
            toAddress.removeEnd( ',' );
            system.debug('toaddress:'+toAddress);
            EmailTemplate objEmailTemplate;
            SendGrid_Email_Details__mdt objSendGridMdt;
            if( mapEmailTemplates.containskey( 'PTP' ) ) {
                
                objEmailTemplate = mapEmailTemplates.get('PTP');
            }
            if( mapSendGridDetails.containsKey( 'PTP') ) {
                objSendGridMdt = mapSendGridDetails.get( 'PTP' );
            }
            if( objEmailTemplate != null && objSendGridMdt != null ) {
                
                // objEmailTemplate = 
                GenericUtility.SendGridWrapper sgWrap = new GenericUtility.SendGridWrapper();
                
                sgWrap = GenericUtility.prepareSendGridEmail( objSendGridMdt );
                
                sgWrap.toAddress = String.isNotBlank( sgWrap.toAddress ) ? toAddress + ',' + sgWrap.toAddress : toAddress;
                
                sgWrap.contentValue = MergeFieldReplacer.replaceMergeFields( objEmailTemplate.HtmlValue, 'Calling List', objCl.Id ) ;
                
                sgWrap.contentBody = MergeFieldReplacer.replaceMergeFields( objEmailTemplate.Body, 'Calling List', objCl.Id );
                
                sgWrap.subject = 'Payment Due date Reminder Booking unit-'+mail;//MergeFieldReplacer.replaceMergeFields( objEmailTemplate.Subject, 'Calling List', objCl.Id );
                
                sgWrap.listAttachment = new  List<Attachment>();
                
                sgWrap.relatedToId = objCl.Id;
                
                sgWrap.clId = objCl.Id;
                
                sgWrap.processType = 'Collections Calling List';
                
                //Call generic method to send email via sendGrid
                List<EmailMessage> lstEmailsTemp = GenericUtility.sendEmailsBySendGrid( sgWrap );
                if( !lstEmailsTemp.isEmpty() ) {
                    lstEmails.addAll( lstEmailsTemp );
                    objCl.Call_Count__c++;
                    listWelcomeClToUpdate.add( objCl );
                }
            }
        }
    }
}