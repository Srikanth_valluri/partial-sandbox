/**
 * Ver       Date            Author      		    Modification
 * 1.0    9/30/2019         Arsh Dave              Initial Version 

**/
@isTest
public class SendTaskClosureEmailCustomerSRTest{

    @testSetup static void setup(){
        UserRole objRole = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert objRole;

        User objUserManager = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'Muser000@amamama.com',
            Username = 'Muser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias_M',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = objRole.Id
        );
        insert objUserManager;

        User objUser = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = objRole.Id,
            ManagerId = objUserManager.Id
        );
        insert objUser;
    }

    static testMethod  void callConstructor() {
        
        User objUser = [SELECT Id FROM User WHERE Email = 'puser000@amamama.com'];

        Account objAccount = TestDataFactoryFM.createAccount();
        objAccount.Email__c = 'test@test.com'; 
        objAccount.OwnerId = objUser.Id;
        insert objAccount;

        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAccount);
        insert objSR;

        Booking__c objBooking = TestDataFactoryFM.createBooking(objAccount, objSR);
        insert objBooking;
        
        Booking_Unit__c objBooking_Unit = TestDataFactoryFM.createBookingUnit(objAccount, objBooking);
        insert objBooking_Unit;

        System.runAs(objUser){
            FM_Case__c objFMCase = new FM_Case__c(
                Account__c = objAccount.Id,
                Category_GE__c = 'Change of Contact Details',
                Subcategory_GE__c = 'Change registered email id',
                RecordTypeId = System.Label.FM_Case_General_Enquiry
            );
        
            Test.startTest();

                SendTaskClosureEmailCustomerSRProcess objSendTaskClosureEmailCustomerSRProcess = new SendTaskClosureEmailCustomerSRProcess();

                insert objFMCase;
                
                List<Task> objTask = [
                    SELECT Id
                        , Status
                        , ActivityDate
                    FROM Task
                   WHERE What.Id = :objFMCase.Id   
                ];

                objTask[0].ActivityDate = System.Today();
                objTask[0].Description = 'Task Is Closed';
                objTask[0].Self_Employed__c = TRUE;
                objTask[0].Status = 'Completed';

                FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');
                Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {'https://api.sendgrid.com/v3/mail/send' => sendGridResponse };
                Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
                update objTask;
                
            Test.stopTest();
        }
        
    }

    static testMethod  void callNonFutureMethod() {
        
        User objUser = [SELECT Id, Email FROM User WHERE Email = 'puser000@amamama.com'];

        Account objAccount = TestDataFactoryFM.createAccount();
        objAccount.Email__c = 'test@test.com'; 
        objAccount.OwnerId = objUser.Id;
        insert objAccount;

        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objAccount);
        insert objSR;

        Booking__c objBooking = TestDataFactoryFM.createBooking(objAccount, objSR);
        insert objBooking;
        
        Booking_Unit__c objBooking_Unit = TestDataFactoryFM.createBookingUnit(objAccount, objBooking);
        insert objBooking_Unit;

        System.runAs(objUser){
            FM_Case__c objFMCase = new FM_Case__c(
                Account__c = objAccount.Id,
                Category_GE__c = 'Change of Contact Details',
                Subcategory_GE__c = 'Change registered email id',
                RecordTypeId = System.Label.FM_Case_General_Enquiry
            );

            SendTaskClosureEmailCustomerSRProcess objSendTaskClosureEmailCustomerSRProcess = new SendTaskClosureEmailCustomerSRProcess();

            insert objFMCase;
            List<Task> objTask = [
                SELECT Id
                    , Status
                    , ActivityDate
                FROM Task
                WHERE What.Id = :objFMCase.Id   
            ];

            objTask[0].ActivityDate = System.Today();
            objTask[0].Description = 'Task Is Closed';
            objTask[0].Status = 'Completed';
            update objTask;

            Test.startTest();
                String toAddress = '', fromAddress,replyToAddress = '', contentType, contentValue = '',contentBody = '';
                String subject, strAccountId, strRelatedToId, strDPIId, bccAddress ='', strCCAddress;

                toAddress = objUser.Email;
                strAccountId = objAccount.Id;
                strRelatedToId = objAccount.Id;
                fromAddress = 'no-replysf@damacgroup.com';
                bccAddress = Label.Sf_Copy_Bcc_Mail_Id;
                contentType = 'text/html';
                strCCAddress = '';
                contentValue = '<html><body style="font-family: Arial; font-size:16px;"><p style="font-family: Arial; font-size:16px;"> Dear Mr/Ms. Test,';
                contentValue += 'Your Task Has been closed. <br/>Thanks.  <br/>DAMAC Properties</p></body></html>';
                subject = 'Task Closure Email';
                
                FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');
                Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {'https://api.sendgrid.com/v3/mail/send' => sendGridResponse };
                Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
                
                SendTaskClosureEmailCustomerSRProcess.callSendGridFromBatch(toAddress, strCCAddress, bccAddress, subject, fromAddress, replyToAddress, contentType, contentValue, strAccountId, contentBody, strRelatedToId);
                
            Test.stopTest();
        }  
    }
    
}