/********************************************************************************************************************************
 * Description : 
 *===============================================================================================================================
 * Ver      Date-DD/MM/YYYY     Author              Modification
 *===============================================================================================================================
 * 1.1      18-11-2020          Aishwarya Todkar    Removed complaint record type check
*********************************************************************************************************************************/
public without sharing class CaseAssignmentHandler {

    private static final String COLLECTION_QUEUE         = 'Collection Queue';
    private static final String CONTACT_CENTER_QUEUE    = 'Contact Center Queue';
    private static final String ASSIGNMENT_RECORD_TYPE    = 'Assignment';
    private static final String COMPLAINT_RECORD_TYPE    = 'Complaint';
    private static final Set<String> COD_RECORD_TYPES   = new Set<String>{
        'Change of Details', 'Change of Joint Buyer', 'Name Nationality Change', 'Passport Detail Update'
    };

    /**
     * sets OwnerId to Cases
     * @param lstOldCase    Trigger.old from Trigger
     * @param lstCase       Trigger.new from Trigger
     */
    public static void setOwnerId(List<Case> lstOldCase, List<Case> lstCase) {

        Map<String, RecordTypeInfo> mapRtByName = Case.SObjectType.getDescribe().getRecordTypeInfosByName();

        // fetch Case Record Types
        Map<Id, String> recordTypeIds = getCaseRecordTypeNamesById(mapRtByName);

        // for Collection-type Case Record types
        Set<Id> setCollectionRecordTypeId = fetchCollectionRecordTypeIds(mapRtByName);

        // Query Queues
        Map<String, Id> queues = new Map<String, Id>();
        for (QueueSobject queueSobject : fetchCaseQueueSobjects(
            new Set<String>{COLLECTION_QUEUE, CONTACT_CENTER_QUEUE}
        )) {
            queues.put(queueSobject.Queue.Name, queueSobject.QueueId);
        }

        // Maps CaseId To CreatedById
        Map<Id, Id> mapCaseIdToCreatedById = new Map<Id, Id>();

        //List of Cases with Origin as 'Portal'
        List<Case> lstNonCollectionPortalCase = new List<Case>();

        Map<Id, Booking_Unit__c> mapBookingUnit = new Map<Id, Booking_Unit__c>();
        for (Case instCase : lstCase) {
            mapBookingUnit.put(instCase.Booking_Unit__c, NULL);
        }
        mapBookingUnit = new Map<Id, Booking_Unit__c>([
            SELECT  Id, Inventory__c, Inventory__r.Property_City__c
            FROM    Booking_Unit__c
            WHERE Id IN :mapBookingUnit.keySet()
        ]);
        mapBookingUnit.put(NULL, new Booking_Unit__c(Inventory__c = NULL));

        for (Integer i = 0; i < lstCase.size(); i++) {
            Case instCase = lstCase[i];

            if (!'Submitted'.equalsIgnoreCase(instCase.Status)
                || (lstOldCase != NULL && instCase.Status.equalsIgnoreCase(lstOldCase[i].Status))
                || instCase.RecordTypeId == NULL
                || COMPLAINT_RECORD_TYPE.equalsIgnoreCase(recordTypeIds.get(instCase.RecordTypeId)) //Added by AT
            ) {
                continue;
            }

            if (!'Portal'.equalsIgnoreCase(instCase.Origin)) {
                 // Not changing Owner for Non-Portal Cases
                continue;
            }

            // Case Assignment for Cases originated from Customer Portal

            // Contact Center Queue cases
            if (COD_RECORD_TYPES.contains(recordTypeIds.get(instCase.RecordTypeId))
                //|| COMPLAINT_RECORD_TYPE.equalsIgnoreCase(recordTypeIds.get(instCase.RecordTypeId))
                ||  (ASSIGNMENT_RECORD_TYPE.equalsIgnoreCase(recordTypeIds.get(instCase.RecordTypeId))
                    && mapBookingUnit.get(instCase.Booking_Unit__c).Inventory__c != NULL
                    && 'Dubai'.equalsIgnoreCase(
                        mapBookingUnit.get(instCase.Booking_Unit__c).Inventory__r.Property_City__c
                    ))
            ) {
                instCase.OwnerId = queues.get(CONTACT_CENTER_QUEUE);
            } else
            // Collection Cases
            if (setCollectionRecordTypeId.contains(instCase.RecordTypeId)) {
                if (queues.containsKey(COLLECTION_QUEUE)) {
                    instCase.OwnerId = queues.get(COLLECTION_QUEUE);
                }
            } else { // Non-Collection Cases
                mapCaseIdToCreatedById.put(instCase.Id, instCase.CreatedById);
                lstNonCollectionPortalCase.add(instCase);
            }
        }
        if (lstNonCollectionPortalCase.isEmpty()) return;
        // fetch Portal User records
        List<User> lstUser = [
            SELECT  Id,
                    ContactId,
                    Contact.AccountId,
                    Contact.Account.Primary_CRE__c,
                    Contact.Account.Secondary_CRE__c,
                    Contact.Account.Tertiary_CRE__c,
                    Contact.Account.Primary_Language__c
            FROM    User
            WHERE   Id =: System.UserInfo.getUserId()
            AND   IsPortalEnabled = TRUE
        ];

        List<Case> lstNonCreCase = new List<Case>();

        if (lstUser.isEmpty() || lstUser[0].ContactId == NULL || lstUser[0].Contact.AccountId == NULL) {
            return;
        }

        for (Case instCase : lstNonCollectionPortalCase) {
            // Assign to Primary, Secondary, Tertiary CREs
            if (lstUser[0].Contact.Account.Primary_CRE__c != NULL) {
                instCase.OwnerId = lstUser[0].Contact.Account.Primary_CRE__c;
            } else if (lstUser[0].Contact.Account.Secondary_CRE__c != NULL) {
                instCase.OwnerId = lstUser[0].Contact.Account.Secondary_CRE__c;
            } else if (lstUser[0].Contact.Account.Tertiary_CRE__c != NULL) {
                instCase.OwnerId = lstUser[0].Contact.Account.Tertiary_CRE__c;
            } else {
                lstNonCreCase.add(instCase);
            }
        }

        if(lstNonCreCase.isEmpty()) return;

        List<QueueSobject> lstQueue = fetchCaseQueueSobjects(new Set<String>{'Non Elite Arabs Queue', 'Non Elite Non Arabs Queue'});
        if (lstQueue.size() != 2) {
            return;
        }
        // Assign to Arab, Non-Arab Queues
        for (Case instCase : lstNonCreCase) {
            if ('Arabic'.equalsIgnoreCase(lstUser[0].Contact.Account.Primary_Language__c)) {
                instCase.OwnerId = lstQueue[0].QueueId;
            } else {
                instCase.OwnerId = lstQueue[1].QueueId;
            }
        }
    }

    /**
     * Fetches Collection type of Record Type Ids
     * @return Set of Collection type of Record Type Ids
     */
    private static Set<Id> fetchCollectionRecordTypeIds(Map<String,Schema.RecordTypeInfo> mapRtByName) {
        Set<Id> setCollectionRecordTypeId = new Set<Id>();

        // fetch Collection record-types
        for (Case_Record_Type__mdt caseRecordTypeMetadata : [
                SELECT  MasterLabel
                FROM    Case_Record_Type__mdt
                WHERE   IsCollectionCase__c = TRUE
        ]) {
            Schema.RecordTypeInfo recordType = mapRtByName.get(caseRecordTypeMetadata.MasterLabel);
            if (recordType != NULL) {
                setCollectionRecordTypeId.add(recordType.getRecordTypeId());
            }
        }
        return setCollectionRecordTypeId;
    }

    /**
     * fetches Case QueueSobject records for the given RecordTypes
     * @param  setQueueName Set of RecordType Names
     * @return              List of QueueSobject records
     */
    private static List<QueueSobject> fetchCaseQueueSobjects(Set<String> setQueueName) {
        if (setQueueName == NULL) return new List<QueueSobject>();

        return [
            SELECT      Id, Queue.Name, QueueId FROM QueueSobject
            WHERE       SobjectType = 'Case'
                    AND Queue.Name IN :setQueueName
            ORDER BY    Queue.Name
        ];
    }

    /**
     * returns a map of Case RecordType Name to RecordTypeId
     * @return map of Case RecordType Name to RecordTypeId
     */
    public static Map<Id, String> getCaseRecordTypeNamesById(Map<String,Schema.RecordTypeInfo> mapRtByName) {
        Map<Id, String> mapRtNameToId = new Map<Id, String>();
        for (RecordTypeInfo rtInfo : mapRtByName.values()) {
            mapRtNameToId.put(rtInfo.getRecordTypeId(), rtInfo.getName());
        }
        return mapRtNameToId;
    }
}