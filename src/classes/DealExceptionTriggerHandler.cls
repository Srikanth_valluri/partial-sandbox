public class DealExceptionTriggerHandler{

    public DealExceptionTriggerHandler(){}
    /*
    public void actionApprovedRequests(List<Deal_Exception_Request__c> approvedDERList){
        List<Id> deRecordIdList = new List<Id> ();
        for(Deal_Exception_Request__c der: approvedDERList){
            deRecordIdList.add(der.Id);
            
        }
        if(deRecordIdList.size() > 0){
            sendDERApprovalNotification(deRecordIdList);
            updateDERStatus(deRecordIdList);
        }
        
    }*/
    
    public void checkSalesMargins(Map<Id, Deal_Exception_Request__c> derMap){
        Map<Id, List<Id>> derSalesMarginsMap = new Map<Id, List<Id>>();
        Map<Id, String> deUnitPayPlanTypeMap = new Map<Id, String>();
        Map<Id, Boolean> derChairmanApprovalMap = new Map<Id, Boolean>();
        Map<Id, Boolean> derPayPlanDaysOutsideMap = new Map<Id, Boolean>();
        Map<Id, Boolean> managementApprovalMap = new Map<Id, Boolean>();
        Map<Id, String> deUnitPaymentTypeMap = new Map<Id, String>();
        for(Id derId: derMap.keyset()){
            derChairmanApprovalMap.put(derId, false);  
            managementApprovalMap.put(derId, false);
            derPayPlanDaysOutsideMap.put(derId, false);
        }
        Map<Id, Integer> paymentTermsCountMap = new Map<Id, Integer> ();
        Map<Id, Integer> paymentTermsDaysMap = new Map<Id, Integer> ();
        List<Deal_Exception_Unit__c> deUnitList = new List<Deal_Exception_Unit__c>();
        for(Deal_Exception_Unit__c deUnit: [SELECT Id, Name, Sales_Margins__c, 
                                                Deal_Exception_Request__c, Payment_Plan_Type__c,
                                                Inventory__r.Unit_Categorization__c, 
                                                Requested_Price__c, X100_Upfront__c, Discount_Percent__c,
                                                Payment_Plan__c,  Proposed_Payment_Plan__c, 
                                                Price_SqFt_after_discount__c, Deal_Exception_Request__r.RM_Team__c,  
                                                Deal_Exception_Request__r.Agency__c,
                                                Sales_Margins__r.Apply_Agent_Discount__c,
                                                Sales_Margins__r.Effective_Discount_Guideline__c,
                                                Sales_Margins__r.Effective_Discount_100_Upfront__c,
                                                Sales_Margins__r.Effective_Discount_Guideline_Agent__c,
                                                Sales_Margins__r.Effective_Discount_100_Upfront_Agent__c,
                                                Sales_Margins__r.Max_Discount_On_100_upfront__c, 
                                                Sales_Margins__r.Max_Suggested_Discount_On_system_PSF__c,
                                                Sales_Margins__r.Net_Standard_Discount__c, 
                                                Sales_Margins__r.Net_Upfront_Discount__c,
                                                Sales_Margins__r.PSF_Sellable_Standard__c, 
                                                Sales_Margins__r.PSF_AC_Standard__c,
                                                Sales_Margins__r.PSF_Sellable_Upfront__c, 
                                                Sales_Margins__r.PSF_AC_Upfront__c,
                                                Sales_Margins__r.Net_PSF_Sellable_Standard__c, 
                                                Sales_Margins__r.Net_PSF_AC_Standard__c,
                                                Sales_Margins__r.Net_PSF_Sellable_Upfront__c,
                                                 Sales_Margins__r.Net_PSF_AC_Upfront__c,
                                                Sales_Margins__r.Max_Discount_On_100_upfront_Agent__c, 
                                                Sales_Margins__r.Max_Suggested_Discount_Agent__c,
                                                Sales_Margins__r.Net_PSF_AC_Standard_Agent__c, 
                                                Sales_Margins__r.Net_PSF_AC_Upfront_Agent__c,
                                                Sales_Margins__r.Net_PSF_Sellable_Standard_Agent__c, 
                                                Sales_Margins__r.Net_PSF_Sellable_Upfront_Agent__c,
                                                Sales_Margins__r.Net_Standard_Discount_Agent__c, 
                                                Sales_Margins__r.Net_Upfront_Discount_Agent__c,
                                                Sales_Margins__r.PSF_AC_Standard_Agent__c, 
                                                Sales_Margins__r.PSF_AC_Upfront_Agent__c,
                                                Sales_Margins__r.PSF_Sellable_Standard_Agent__c, 
                                                Sales_Margins__r.PSF_Sellable_Upfront_Agent__c
                                            FROM Deal_Exception_Unit__c 
                                            WHERE Deal_Exception_Request__c IN: derMap.keyset()]){
            deUnitList.add(deUnit);
            if(deUnit.Proposed_Payment_Plan__c == null){
                deUnitPayPlanTypeMap.put(deUnit.Id, 'Standard Payment Plan');
            } else {
                deUnitPayPlanTypeMap.put(deUnit.Id, 'Proposed Payment Plan (within Standard Dates)');
            }
            if(deUnit.Proposed_Payment_Plan__c != null){
                paymentTermsCountMap.put(deUnit.Proposed_Payment_Plan__c, 0);
                paymentTermsDaysMap.put(deUnit.Proposed_Payment_Plan__c, 0);
            }
            if(deUnit.Payment_Plan__c != null){
                paymentTermsDaysMap.put(deUnit.Payment_Plan__c, 0);
            }
        }
        for(Payment_Terms__c term: [SELECT Id, Event_days__c, Payment_Plan__c 
                                    FROM Payment_Terms__c WHERE Payment_Plan__c IN: paymentTermsDaysMap.keyset()]){
            if(paymentTermsCountMap.containsKey(term.Payment_Plan__c)){
                paymentTermsCountMap.put(term.Payment_Plan__c, paymentTermsCountMap.get(term.Payment_Plan__c) + 1);
            }
            Integer daysCount = paymentTermsDaysMap.get(term.Payment_Plan__c);
            if(term.Event_days__c != null && term.Event_days__c.isNumeric()){
                if(Integer.valueOf(term.Event_days__c) > daysCount){
                    daysCount = Integer.valueOf(term.Event_days__c);
                }
            }
            paymentTermsDaysMap.put(term.Payment_Plan__c, daysCount);
            
        }
        for(Deal_Exception_Unit__c deUnit: deUnitList){
            deUnitPaymentTypeMap.put(deUnit.Id, 'Standard');
            if(deUnit.Proposed_Payment_Plan__c != null){
                if(paymentTermsCountMap.get(deUnit.Proposed_Payment_Plan__c) == 1){
                    deUnitPaymentTypeMap.put(deUnit.Id, '100% Upfront');
                    deUnitPayPlanTypeMap.put(deUnit.Id, '100% Upfront Payment');
                }
                if(deUnit.Payment_Plan__c != null){
                    if(paymentTermsDaysMap.get(deUnit.Proposed_Payment_Plan__c) > paymentTermsDaysMap.get(deUnit.Payment_Plan__c)){
                        derPayPlanDaysOutsideMap.put(deUnit.Deal_Exception_Request__c, true);
                        deUnitPayPlanTypeMap.put(deUnit.Id, 'Proposed Payment Plan (outside Standard Dates)');
                    }    
                }
                
            }
        }
        system.debug('paymentTermsCountMap: ' + paymentTermsCountMap);
        system.debug('deUnitPaymentTypeMap: ' + deUnitPaymentTypeMap);
        system.debug('derChairmanApprovalMap: ' + derChairmanApprovalMap);
        system.debug('managementApprovalMap: ' + managementApprovalMap);
        for(Deal_Exception_Unit__c deUnit: deUnitList){
            Decimal psfSellableStandard = deUnit.Sales_Margins__r.PSF_Sellable_Standard__c;
            Decimal netPsfSellableStandard = deUnit.Sales_Margins__r.Net_PSF_Sellable_Standard__c;
            Decimal psfSellableUpfront = deUnit.Sales_Margins__r.PSF_Sellable_Upfront__c;
            Decimal netPsfSellableUpfront = deUnit.Sales_Margins__r.Net_PSF_Sellable_Upfront__c;
            Decimal psfACStandard = deUnit.Sales_Margins__r.PSF_AC_Standard__c;
            Decimal netPsfACStandard = deUnit.Sales_Margins__r.Net_PSF_AC_Standard__c;
            Decimal psfACUpfront = deUnit.Sales_Margins__r.PSF_AC_Upfront__c;
            Decimal netPsfACUpfront = deUnit.Sales_Margins__r.Net_PSF_AC_Upfront__c;
            
           // Decimal maxStandardDiscount = deUnit.Sales_Margins__r.Max_Suggested_Discount_On_system_PSF__c;
            Decimal maxStandardDiscount = deUnit.Sales_Margins__r.Effective_Discount_Guideline__c;
            Decimal netStandardDiscount = deUnit.Sales_Margins__r.Net_Standard_Discount__c;
           // Decimal maxUpfrontDiscount = deUnit.Sales_Margins__r.Max_Discount_On_100_upfront__c;
            Decimal maxUpfrontDiscount = deUnit.Sales_Margins__r.Effective_Discount_100_Upfront__c;
            Decimal netUpfrontDiscount = deUnit.Sales_Margins__r.Net_Upfront_Discount__c;  
            
            if(deUnit.Sales_Margins__r.Apply_Agent_Discount__c 
                    && (deUnit.Deal_Exception_Request__r.RM_Team__c == 'Agents Team' 
                            ||  deUnit.Deal_Exception_Request__r.Agency__c != NULL)){
                psfSellableStandard = deUnit.Sales_Margins__r.PSF_Sellable_Standard_Agent__c;
                netPsfSellableStandard = deUnit.Sales_Margins__r.Net_PSF_Sellable_Standard_Agent__c;
                psfSellableUpfront = deUnit.Sales_Margins__r.PSF_Sellable_Upfront_Agent__c;
                netPsfSellableUpfront = deUnit.Sales_Margins__r.Net_PSF_Sellable_Upfront_Agent__c;
                psfACStandard = deUnit.Sales_Margins__r.PSF_AC_Standard_Agent__c;
                netPsfACStandard = deUnit.Sales_Margins__r.Net_PSF_AC_Standard_Agent__c;
                psfACUpfront = deUnit.Sales_Margins__r.PSF_AC_Upfront_Agent__c;
                netPsfACUpfront = deUnit.Sales_Margins__r.Net_PSF_AC_Upfront_Agent__c;
                
                //maxStandardDiscount = deUnit.Sales_Margins__r.Max_Suggested_Discount_Agent__c;
                maxStandardDiscount = deUnit.Sales_Margins__r.Effective_Discount_Guideline_Agent__c;
                netStandardDiscount = deUnit.Sales_Margins__r.Net_Standard_Discount_Agent__c;
               // maxUpfrontDiscount = deUnit.Sales_Margins__r.Max_Discount_On_100_upfront_Agent__c;
                maxUpfrontDiscount = deUnit.Sales_Margins__r.Effective_Discount_100_Upfront_Agent__c;
                netUpfrontDiscount = deUnit.Sales_Margins__r.Net_Upfront_Discount_Agent__c;  
            }         

            if(deUnit.Sales_Margins__c != null && derChairmanApprovalMap.get(deUnit.Deal_Exception_Request__c) == false ){
                if(deUnit.Inventory__r.Unit_Categorization__c == 'Villa'){
                    system.debug(deUnit.Name + ' --Villa--');
                    if(deUnitPaymentTypeMap.get(deUnit.Id) == 'Standard'
                             &&  (deUnit.Price_SqFt_after_discount__c < psfACStandard || deUnit.Discount_Percent__c > maxStandardDiscount )){
                        system.debug('--Standard--');
                        if((deUnit.Price_SqFt_after_discount__c <= netPsfACStandard) || (deUnit.Discount_Percent__c >= netStandardDiscount)){
                            system.debug('--Chariman Approval--');
                            derChairmanApprovalMap.put(deUnit.Deal_Exception_Request__c, true);
                        } else if((deUnit.Price_SqFt_after_discount__c < psfACStandard && deUnit.Price_SqFt_after_discount__c > netPsfACStandard)
                                || (deUnit.Discount_Percent__c > maxStandardDiscount && deUnit.Discount_Percent__c < netStandardDiscount)){
                            system.debug('--Management Approval--');
                            managementApprovalMap.put(deUnit.Deal_Exception_Request__c, true);
                        }
                        
                        /*
                        if(deUnit.Price_SqFt_after_discount__c < psfACStandard){
                            if(deUnit.Price_SqFt_after_discount__c < netPsfACStandard){
                                system.debug('--Chariman Approval--');
                                derChairmanApprovalMap.put(deUnit.Deal_Exception_Request__c, true);
                            } else {
                                system.debug('--Management Approval--');
                                managementApprovalMap.put(deUnit.Deal_Exception_Request__c, true);
                            }
                        } */
                    } else if(deUnitPaymentTypeMap.get(deUnit.Id) == '100% Upfront'
                             &&  (deUnit.Price_SqFt_after_discount__c < psfACUpfront || deUnit.Discount_Percent__c > maxUpfrontDiscount )){
                        system.debug('--100%Upfront--');
                        
                        if((deUnit.Price_SqFt_after_discount__c <= netPsfACUpfront) || (deUnit.Discount_Percent__c >= netUpfrontDiscount)){
                            system.debug('--Chariman Approval--');
                            derChairmanApprovalMap.put(deUnit.Deal_Exception_Request__c, true);
                        } else if((deUnit.Price_SqFt_after_discount__c < psfACUpfront && deUnit.Price_SqFt_after_discount__c > netPsfACUpfront)
                                || (deUnit.Discount_Percent__c > maxUpfrontDiscount && deUnit.Discount_Percent__c < netUpfrontDiscount)){
                            system.debug('--Management Approval--');
                            managementApprovalMap.put(deUnit.Deal_Exception_Request__c, true);
                        }
                        
                        
                        
                        /*
                        if(deUnit.Price_SqFt_after_discount__c < psfACUpfront){
                            if(deUnit.Price_SqFt_after_discount__c < netPsfACUpfront){
                                system.debug('--Chariman Approval--');
                                derChairmanApprovalMap.put(deUnit.Deal_Exception_Request__c, true);
                            } else {
                                system.debug('--Management Approval--');
                                managementApprovalMap.put(deUnit.Deal_Exception_Request__c, true);
                            }
                        }  */
                    }  
                } else if(deUnit.Inventory__r.Unit_Categorization__c == 'Apartment' ){
                    system.debug(deUnit.Name + ' --Apartment--');
                    if(deUnitPaymentTypeMap.get(deUnit.Id) == 'Standard'
                            &&  (deUnit.Price_SqFt_after_discount__c < psfSellableStandard || deUnit.Discount_Percent__c >maxStandardDiscount )){
                        system.debug('--Standard--');
                        if((deUnit.Price_SqFt_after_discount__c <= netPsfSellableStandard) || (deUnit.Discount_Percent__c >= netStandardDiscount)){
                            system.debug('--Chariman Approval--');
                            derChairmanApprovalMap.put(deUnit.Deal_Exception_Request__c, true);
                        } else if((deUnit.Price_SqFt_after_discount__c < psfSellableStandard && deUnit.Price_SqFt_after_discount__c > netPsfSellableStandard)
                                || (deUnit.Discount_Percent__c > maxStandardDiscount && deUnit.Discount_Percent__c < netStandardDiscount)){
                            system.debug('--Management Approval--');
                            managementApprovalMap.put(deUnit.Deal_Exception_Request__c, true);
                        }
                        
                        /*
                        
                        if(deUnit.Price_SqFt_after_discount__c < psfSellableStandard){
                            if(deUnit.Price_SqFt_after_discount__c < netPsfSellableStandard){
                                system.debug('--Chairman Approval--');
                                derChairmanApprovalMap.put(deUnit.Deal_Exception_Request__c, true);
                            } else {
                                system.debug('--Management Approval--');
                                managementApprovalMap.put(deUnit.Deal_Exception_Request__c, true);
                            }
                        }  */
                    } else if(deUnitPaymentTypeMap.get(deUnit.Id) == '100% Upfront'
                            &&  (deUnit.Price_SqFt_after_discount__c < psfSellableUpfront || deUnit.Discount_Percent__c > maxUpfrontDiscount )){
                        system.debug('--100% Upfront--');
                        
                        if((deUnit.Price_SqFt_after_discount__c <= netPsfSellableUpfront) || (deUnit.Discount_Percent__c >= netUpfrontDiscount)){
                            system.debug('--Chariman Approval--');
                            derChairmanApprovalMap.put(deUnit.Deal_Exception_Request__c, true);
                        } else if((deUnit.Price_SqFt_after_discount__c < psfSellableUpfront && deUnit.Price_SqFt_after_discount__c > netPsfSellableUpfront)
                                || (deUnit.Discount_Percent__c > maxUpfrontDiscount && deUnit.Discount_Percent__c < netUpfrontDiscount)){
                            system.debug('--Management Approval--');
                            managementApprovalMap.put(deUnit.Deal_Exception_Request__c, true);
                        }
                        
                        /* 
                        if(deUnit.Price_SqFt_after_discount__c < psfSellableUpfront){
                            if(deUnit.Price_SqFt_after_discount__c < netPsfSellableUpfront){
                                system.debug('--Chairman Approval--');
                                derChairmanApprovalMap.put(deUnit.Deal_Exception_Request__c, true);
                            } else {
                                system.debug('--Management Approval--');
                                managementApprovalMap.put(deUnit.Deal_Exception_Request__c, true);
                            }
                        }  */
                    }  
                }
            }
        }
        system.debug('derChairmanApprovalMap: ' + derChairmanApprovalMap);
        system.debug('managementApprovalMap: ' + managementApprovalMap);
        for(Deal_Exception_Request__c der: derMap.values()){
            if(derChairmanApprovalMap.get(der.Id)){
                der.Sales_Margin_Approver__c = 'Chairman';
            } else if(managementApprovalMap.get(der.Id)){
                der.Sales_Margin_Approver__c = 'Management';
            } else {
                der.Sales_Margin_Approver__c = 'MIS HOD';
            }
            if(derPayPlanDaysOutsideMap.get(der.Id) == true){
                der.Payment_Date_outside_Standard_Date__c = true;
              //  der.Management_Approval__c = true;
            } else { 
                der.Payment_Date_outside_Standard_Date__c = false;
              //  der.Management_Approval__c = false;
            }
        }
        List<Deal_Exception_Unit__c> deUnitUpdateList = new List<Deal_Exception_Unit__c>();
        for(Deal_Exception_Unit__c deUnit: deUnitList){
            Boolean changeMade = false;
            if(deUnit.X100_Upfront__c == false &&  deUnitPaymentTypeMap.get(deUnit.Id) == '100% Upfront'){
                deUnit.X100_Upfront__c = true;
                changeMade = true;
            } else if(deUnit.X100_Upfront__c == true &&  deUnitPaymentTypeMap.get(deUnit.Id) != '100% Upfront'){
                deUnit.X100_Upfront__c = false;
                changeMade = true;
            }
            if(deUnitPayPlanTypeMap.containsKey(deUnit.Id) 
                        && deUnitPayPlanTypeMap.get(deUnit.Id) != deUnit.Payment_Plan_Type__c){
                deUnit.Payment_Plan_Type__c = deUnitPayPlanTypeMap.get(deUnit.Id);
                changeMade = true;
            }
            if(changeMade){
                deUnitUpdateList.add(deUnit);
            }
        }
        system.debug('deUnitUpdateList: ' + deUnitUpdateList);
        if(deUnitUpdateList.size() > 0){
            update deUnitUpdateList;
        }
        
    }
    
    public void updateDERStatus(List<Deal_Exception_Request__c> approvedDERList){
       Set<Id> condApprovedDERIds = new Set<Id>();
       List<Deal_Exception_Request__c> derList = new List<Deal_Exception_Request__c>();
       for(Checklist_Item__c chkListItem: [SELECT Id, Deal_Exception_Request__c FROM Checklist_Item__c
                                           WHERE Deal_Exception_Request__c IN: approvedDERList
                                           ORDER BY CreatedDate]){
           condApprovedDERIds.add(chkListItem.Deal_Exception_Request__c);
       }
       for(Deal_Exception_Request__c der: approvedDERList){
           if(condApprovedDERIds.contains(der.Id)){
               der.status__c = 'Approved Conditionally';
           }
       }
 
    }

     @future
    public static void sendDERApprovalNotification(List<Id> deRecordIdList){
        system.debug('Sending DER Approval Notification to Sales Admin');
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Map<Id, List<Checklist_Item__c>> derCheckListMap = new Map<Id, List<Checklist_Item__c>>();
         for(Checklist_Item__c chkListItem: [SELECT Id, Name, Item__c, Validated__c, Is_Text_Area__c, Checklist_Value__c, Deal_Exception_Request__c
                                         FROM Checklist_Item__c
                                         WHERE Deal_Exception_Request__c IN: deRecordIdList
                                         ORDER BY CreatedDate]){
            List<Checklist_Item__c> checkList = new List<Checklist_Item__c>();
            if(derCheckListMap.containsKey(chkListItem.Deal_Exception_Request__c) && derCheckListMap.get(chkListItem.Deal_Exception_Request__c) != null){
                checkList = derCheckListMap.get(chkListItem.Deal_Exception_Request__c);
            }
            checkList.add(chkListItem);
            derCheckListMap.put(chkListItem.Deal_Exception_Request__c, checkList);
        }
        for(Deal_Exception_Request__c der:  [SELECT Id, Name FROM Deal_Exception_Request__c WHERE Id IN: deRecordIdList]){
            Messaging.SingleEmailMessage mail = new  Messaging.SingleEmailMessage();
            String ccEmail = '';
            List<String> ccEmails = new List<String>();
            String body = 'Hi,<br/><br/>';
            body += '<br/>' + UserInfo.getName() + ' approved the Deal Exception Request ' + der.Name + '.';
            
            if(derCheckListMap.containsKey(der.Id) && derCheckListMap.get(der.Id) != null){
                body += '<br/><br/><b>Checklist </b><br/>';
                for(Checklist_Item__c checkListItem: derCheckListMap.get(der.Id)){
                    body += checkListItem.Item__c + ': ' + checkListItem.Checklist_Value__c + '<br/>';
                }
            }
            body += '<br/><br/> Link to the Deal Exception Request: ' + System.URL.getSalesforceBaseUrl().toExternalForm() 
                    + '/apex/DealExceptionRequest_Create?id=' + der.Id;
            String emailSubject = 'Deal Exception Request (' + der.Name + ') approved';
            List<String> toEmails = new List<String>();
            for(String toAddress: Label.SR_DER_Notification_To_List.split(',')){
                toEmails.add(toAddress.trim());
            } 
            mail.setCcAddresses(ccEmails);
            mail.setToAddresses(toEmails);
            mail.setSenderDisplayName('Damac Property ');
            mail.setReplyTo('noreply@Damacgroup.com');
            mail.setSubject(emailSubject);
            mail.setSaveAsActivity(false);
            System.debug('...body...' + body);
            mail.setHtmlBody(body);
            mails.add(mail);  
            system.debug('mail: ' + mail);
        }
        Messaging.sendEmail(mails);
    }
 
    public void updateRelatedAccInq(List<Deal_Exception_Request__c> derList){
        List<Account> accList = new  List<Account>();
        List<Inquiry__c> inqList = new  List<Inquiry__c>();
        for(Deal_Exception_Request__c derRecord: derList){
            if(derRecord.Status__c == 'Awaiting MIS Review'){
                if(derRecord.Account__c != null){
                    Account acc = new Account();
                    acc.Id = derRecord.Account__c;
                    acc.Deal_Exception_Request__c = derRecord.Id;
                    accList.add(acc);
                 }
                 if(derRecord.Inquiry__c != null){
                     Inquiry__c inq = new Inquiry__c();
                     inq.Id = derRecord.Inquiry__c;
                     inq.Deal_Exception_Request__c = derRecord.Id;
                     inqList.add(inq);
                 }    
            }
            if(derRecord.Status__c == ' Rejected by MIS' || derRecord.Status__c == ' Rejected'){
                if(derRecord.Account__c != null){
                    Account acc = new Account();
                    acc.Id = derRecord.Account__c;
                    acc.Deal_Exception_Request__c = null;
                    accList.add(acc);
                 }
                 if(derRecord.Inquiry__c != null){
                     Inquiry__c inq = new Inquiry__c();
                     inq.Id = derRecord.Inquiry__c;
                     inq.Deal_Exception_Request__c = null;
                     inqList.add(inq);
                 }    
            }
        }
        if(accList.size()  > 0){
            update accList;
        }
        if(inqList.size()  > 0){
            update inqList;
        }
    }
}