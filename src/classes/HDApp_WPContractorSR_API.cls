/**********************************************************************************************************************
Description: API to fetch and save SR details for contractor flow
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By      | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   18-01-2021      | Anand Venkitakrishnan | Created the initial draft
***********************************************************************************************************************/

@RestResource(urlMapping='/contractorSR/*')
global class HDApp_WPContractorSR_API {
    public static String errorMsg;
    public static Integer statusCode;
    
    @HttpGet
    global static FinalReturnWrapper getWorkPermitSRDetails() {
        
        RestRequest r = RestContext.request;
        System.debug('Request Headers:'+r.headers);
        System.debug('Request params:'+r.params);
        
        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        HDApp_Utility.cls_meta_data objMeta = new HDApp_Utility.cls_meta_data();
        
        if(!r.requestURI.equalsIgnoreCase('/contractorSR/getDetails')) {
            returnResponse.meta_data = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,'Please use correct API endpoint',3);
            return returnResponse;
        }
        else if(!r.params.containsKey('fmCaseId') || (r.params.containsKey('fmCaseId') && String.isBlank(r.params.get('fmCaseId')))) {
            returnResponse.meta_data = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,'No fmCaseId found in request',3);
            return returnResponse;
        }
        
        errorMsg = 'Error in fetching the SR details';
        
        FMCaseWrapper objFMCaseWrapper;
        
        try {
            String fmCaseId = r.params.get('fmCaseId');
            
            List<FM_Case__c> listFMCase = fetchSRDetails(fmCaseId);
            
            if(listFMCase != NULL && !listFMCase.isEmpty()) {
                FM_Case__c objFMCase = listFMCase[0];
                System.debug('objFMCase:'+objFMCase);
                
                objFMCaseWrapper = wrapFMCase(objFMCase);
                
                statusCode = 1;
            }
            else {
                objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,'No FM Case found for given fmCaseId',6);
                
                returnResponse.meta_data = objMeta;
                
                return returnResponse;
            }
        }
        catch(Exception e) {
            System.debug('Exception:'+e.getMessage());
            errorMsg = e.getMessage();
            
            statusCode = 6;
        }
        
        if(statusCode == 1 && objFMCaseWrapper != null) {
            objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.successMsg,'', 1);
        }
        else {
            objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,errorMsg,6);
        }
        
        cls_data objData = new cls_data();
        objData.fm_case = objFMCaseWrapper;
        
        returnResponse.data = objData;
        returnResponse.meta_data = objMeta;
        
        System.debug('returnResponse:'+returnResponse);
        
        return returnResponse;
    }
    
    public static List<FM_Case__c> fetchSRDetails(String fmCaseId) {
        List<FM_Case__c> listFMCase = [SELECT Id,Name,Account__c,Booking_Unit__c,Work_Permit_Type__c,Description__c,Location_of_Work__c,Permit_To_Work_For__c, 
                                       Company_Name__c,Contractor_Type__c,Number_of_Employees__c,Person_To_Collect__c, 
                                       Contact_person__c,Email__c,Mobile_Country_Code__c,Mobile_no__c,Office_tel__c, 
                                       Contact_person_contractor__c,Email_2__c,Mobile_Country_Code_3__c,Mobile_no_contractor__c,Office_tel_contractor__c, 
                                       (SELECT Id,Employee_Name__c,Company_Name__c,Emirates_ID__c,Employee_Mobile_Number__c FROM Contractor_Informations__r), 
                                       (SELECT Id,Attachment_url__c,Name,Type__c FROM Documents__r) 
                                       FROM FM_Case__c 
                                       WHERE Id =: fmCaseId];
        
        return listFMCase;
    }
    
    public static FmCaseWrapper wrapFMCase(FM_Case__c objFMCase) {
        FmCaseWrapper objFmCaseWrapper = new FmCaseWrapper();
        
        objFmCaseWrapper.id = objFMCase.id;
        objFmCaseWrapper.account_id = objFMCase.Account__c;
        objFmCaseWrapper.booking_unit_id = objFMCase.Booking_Unit__c;
        
        objFmCaseWrapper.type_of_work_permit = objFMCase.Work_Permit_Type__c;
        objFmCaseWrapper.purpose_of_request = objFMCase.Description__c;
        objFmCaseWrapper.location_of_work = objFMCase.Location_of_Work__c;
        objFmCaseWrapper.person_to_collect = objFMCase.Person_To_Collect__c;
        objFmCaseWrapper.permit_to_work_for = objFMCase.Permit_To_Work_For__c;
        
        objFmCaseWrapper.contact_person_company_name = objFMCase.Company_Name__c;
        objFmCaseWrapper.contact_person_type = objFMCase.Contractor_Type__c;
        objFmCaseWrapper.number_of_employees = objFMCase.Number_of_Employees__c;
        
        if(objFMCase.Person_To_Collect__c.equals('Consultant')) {
            objFmCaseWrapper.contact_person_name = objFMCase.Contact_person__c;
            objFmCaseWrapper.contact_person_email = objFMCase.Email__c;
            objFmCaseWrapper.contact_person_mobile_country_code = objFMCase.Mobile_Country_Code__c;
            objFmCaseWrapper.contact_person_mobile_number = objFMCase.Mobile_no__c;
            objFmCaseWrapper.contact_person_office_telephone = objFMCase.Office_tel__c;
        }
        else {
            objFmCaseWrapper.contact_person_name = objFMCase.Contact_person_contractor__c;
            objFmCaseWrapper.contact_person_email = objFMCase.Email_2__c;
            objFmCaseWrapper.contact_person_mobile_country_code = objFMCase.Mobile_Country_Code_3__c;
            objFmCaseWrapper.contact_person_mobile_number = objFMCase.Mobile_no_contractor__c;
            objFmCaseWrapper.contact_person_office_telephone = objFMCase.Office_tel_contractor__c;
        }
        
        List<cls_employees_details> listEmployeeDetails = new List<cls_employees_details>();
        
        if(objFMCase.Contractor_Informations__r.size() > 0) {
            System.debug('objFMCase.Contractor_Informations__r:'+objFMCase.Contractor_Informations__r);
            for(Contractor_Information__c objContractorInfo : objFMCase.Contractor_Informations__r) {
                cls_employees_details objEmployeeDetails = new cls_employees_details();
                objEmployeeDetails.employee_id = objContractorInfo.Id;
                objEmployeeDetails.employee_name = objContractorInfo.Employee_Name__c;
                objEmployeeDetails.company_name = objContractorInfo.Company_Name__c;
                objEmployeeDetails.emirates_id = objContractorInfo.Emirates_ID__c;
                objEmployeeDetails.mobile_number = objContractorInfo.Employee_Mobile_Number__c;
                
                listEmployeeDetails.add(objEmployeeDetails);
            }
        }
        
        List<cls_attachment> listAttachments = new List<cls_attachment>();
        
        if(objFMCase.Documents__r.size() > 0) {
            System.debug('objFMCase.Documents__r:'+objFMCase.Documents__r);
            for(SR_Attachments__c obj : objFMCase.Documents__r) {
                cls_attachment objAttach = new cls_attachment();
                objAttach.id = obj.Id;
                objAttach.name = obj.Name;
                objAttach.file_extension = obj.Type__c;
                objAttach.attachment_url = obj.Attachment_URL__c;
                
                listAttachments.add(objAttach);
            }
        }
        
        System.debug('listEmployeeDetails:'+listEmployeeDetails);
        System.debug('listAttachments:'+listAttachments);
        
        objFmCaseWrapper.employees_details = listEmployeeDetails;
        objFmCaseWrapper.documents_uploaded = listAttachments;      
        
        System.debug('objFmCaseWrapper:'+objFmCaseWrapper);
        
        return objFmCaseWrapper;        
    }
    
    @HttpPost
    global static FinalSRReturnWrapper saveWorkPermitSRDetails() {
        
        RestRequest r = RestContext.request;
        Blob body = r.requestBody;
        String jsonString = body.toString();
        System.debug('jsonString:'+jsonString);
        
        FinalSRReturnWrapper returnResponse = new FinalSRReturnWrapper();
        HDApp_Utility.cls_meta_data objMeta = new HDApp_Utility.cls_meta_data();
        FMCaseWrapper objFMCaseWrapper = new FMCaseWrapper();
        FMCaseWrapper objResponseWrapper = new FMCaseWrapper();
        
        objFMCaseWrapper = (FMCaseWrapper)JSON.deserialize(jsonString,FMCaseWrapper.class);
        System.debug('objFMCaseWrapper:'+objFMCaseWrapper);
        
        if(!r.requestURI.equalsIgnoreCase('/contractorSR/saveDetails')) {
            returnResponse.meta_data = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,'Please use correct API endpoint',3);
            return returnResponse;
        }
        else if(objFMCaseWrapper.id == null || String.isBlank(objFMCaseWrapper.id)) {
            returnResponse.meta_data = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,'No SR id found in the request',3);
            return returnResponse;
        }
        else if(objFMCaseWrapper.person_to_collect == null || String.isBlank(objFMCaseWrapper.person_to_collect)) {
            returnResponse.meta_data = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,'Missing person_to_collect in the request',3);
            return returnResponse;
        }
        
        errorMsg = 'Error in saving the SR details';
        
        FM_Case__c objFMCase;
        
        try {
            processFMCase(objFMCaseWrapper);
            
            List<FM_Case__c> listFMCase = fetchSRDetails(objFMCaseWrapper.Id);
            
            if(listFMCase != NULL && !listFMCase.isEmpty()) {
                objFMCase = listFMCase[0];
            }
            else {
                objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,'No FM Case found for given fmCaseId',6);
                
                returnResponse.meta_data = objMeta;
                
                return returnResponse;
            }
            
            objResponseWrapper = wrapFMCase(objFMCase);
            
            statusCode = 1;
        }
        catch(Exception e) {
            System.debug('Exception:'+e.getMessage());
            errorMsg = e.getMessage();
            
            statusCode = 6;
        }
        
        if(statusCode == 1 && objResponseWrapper != null) {
            objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.successMsg,'', 1);
        }
        else {
            objMeta = HDApp_Utility.ReturnMetaResponse(HDApp_Constants.commonErrorMsg,errorMsg,6);
        }
        
        cls_sr_data objData = new cls_sr_data();
        objData.sr_id = objFMCase.Id;
        objData.sr_number = objFMCase.Name;
        objData.fm_case = objResponseWrapper;
        
        returnResponse.data = objData;
        returnResponse.meta_data = objMeta;
        
        System.debug('returnResponse:'+returnResponse);
        
        return returnResponse;
    }
    
    public static void processFMCase(FMCaseWrapper objFMCaseWrapper) {
        FM_Case__c objFMCase = new FM_Case__c(Id=objFMCaseWrapper.id);
        
        if(objFmCaseWrapper.person_to_collect.equals('Consultant')) {
            objFMCase.Contact_person__c = objFmCaseWrapper.contact_person_name;
            objFMCase.Email__c = objFmCaseWrapper.contact_person_email;
            objFMCase.Mobile_Country_Code__c = objFmCaseWrapper.contact_person_mobile_country_code;
            objFMCase.Mobile_no__c = objFmCaseWrapper.contact_person_mobile_number;
            objFMCase.Office_tel__c = objFmCaseWrapper.contact_person_office_telephone;
        }
        else {
            objFMCase.Contact_person_contractor__c = objFmCaseWrapper.contact_person_name;
            objFMCase.Email_2__c = objFmCaseWrapper.contact_person_email;
            objFMCase.Mobile_Country_Code_3__c = objFmCaseWrapper.contact_person_mobile_country_code;
            objFMCase.Mobile_no_contractor__c = objFmCaseWrapper.contact_person_mobile_number;
            objFMCase.Office_tel_contractor__c = objFmCaseWrapper.contact_person_office_telephone;
        }
        
        try {
            update objFMCase;
            
            if(String.isNotBlank(objFMCaseWrapper.id) && objFMCaseWrapper.employees_details != null && objFMCaseWrapper.employees_details.size() > 0) {
                List<Contractor_Information__c> employeeDetails = setupEmployeeDetails(objFMCaseWrapper);
            }
            
            errorMsg = 'Successful';
            statusCode = 1;
        }
        catch(Exception e) {
            System.debug('Exception: ' + e.getMessage());
            errorMsg = e.getMessage();
            statusCode = 6;
            
            if(objFMCase.Id != NULL) {
                Error_Log__c errorLog = FM_Utility.createErrorLog(objFMCase.Account__c,NULL,objFMCase.Id,'Work Permit',e.getMessage());
              	insert errorLog;
            }
        }
    }
    
    public static List<Contractor_Information__c> setupEmployeeDetails(FMCaseWrapper objFMCaseWrapper) {
        List<Contractor_Information__c> listEmployees = new List<Contractor_Information__c>();
        
        if(objFMCaseWrapper.employees_details != null && objFMCaseWrapper.employees_details.size() > 0) {
            for(cls_employees_details objEmpInput : objFMCaseWrapper.employees_details) {
                Contractor_Information__c objEmp = new Contractor_Information__c();
                if(String.isNotBlank(objEmpInput.employee_id)) {
                    objEmp.Id = objEmpInput.employee_id;
                }
                objEmp.FM_Case__c = objFMCaseWrapper.id;
                objEmp.Employee_Name__c = objEmpInput.employee_name;
                objEmp.Emirates_ID__c = objEmpInput.emirates_id;
                objEmp.Company_Name__c = objEmpInput.company_name;
                objEmp.Employee_Mobile_Number__c = objEmpInput.mobile_number;
                listEmployees.add(objEmp);
            }
            
            if(listEmployees.size() > 0) {
                upsert listEmployees;
            }
        }
        System.debug('listEmployees:'+listEmployees);
        
        return listEmployees;
    }
    
    /***********************************/
    // For Get SR Details API
    /***********************************/
    global class FinalReturnWrapper {
        public cls_data data;
        public HDApp_Utility.cls_meta_data meta_data;
    }
    
    public class cls_data {
        public FMCaseWrapper fm_case;
    }
    
    public class FMCaseWrapper {
        public String id;
        public String account_id;
        public String booking_unit_id;
        
        public String type_of_work_permit;
        public String purpose_of_request;
        public String location_of_work;
        public String person_to_collect;
        public String permit_to_work_for;
        
        public String contact_person_company_name;
        public String contact_person_type;
        public String number_of_employees;
        
        public String contact_person_name;
        public String contact_person_email;
        public String contact_person_mobile_country_code;
        public String contact_person_mobile_number;
        public String contact_person_office_telephone;
        
        public cls_employees_details[] employees_details;
        
        public cls_attachment[] documents_uploaded;
    }
  	
    public class cls_employees_details {
        public String employee_id;
        public String employee_name;
        public String company_name;
        public String emirates_id;
        public String mobile_number;
    }
    
    public class cls_attachment {
        public String id;
        public String name;
        public String attachment_url;
        public String file_extension;
    }
    
    /***********************************/
    // For Save SR Details API
    /***********************************/
    global class FinalSRReturnWrapper {
        public cls_sr_data data;
        public HDApp_Utility.cls_meta_data meta_data;
    }
    
    public class cls_sr_data {
        public String sr_id;
        public String sr_number;
        public FMCaseWrapper fm_case;
    }
}