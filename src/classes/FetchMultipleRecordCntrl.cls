public with sharing class FetchMultipleRecordCntrl {
    public ApexPages.StandardSetController standardController;
    @testVisible private static final String PUBLIC_SITE_URL    = String.isNotBlank(System.Label.OfficePreviewUrl)?System.Label.OfficePreviewUrl:'';
    @testVisible private static final String SFDC_SITE_URL    = String.isNotBlank(System.Label.SfdcBaseUrl)?System.Label.SfdcBaseUrl:'';
    Public List <String> urlLst {get; set;} 
    public string previouspage;
    public FetchMultipleRecordCntrl(ApexPages.StandardSetController Controller) {
        this.standardController = Controller;
        urlLst = new List <String>();
        previouspage = ApexPages.currentPage().getParameters().get('retURL');
    }
    public void fetchFileFromOffice365(){
//    system.assert(false,'< previouspage  : '+previouspage );
        List<Migrated_Documents_Siebel__c> selecteddocs = [ SELECT Id
                                                 , File_Name__c
                                                 , File_URL__c
                                                 , SR__c
                                              FROM Migrated_Documents_Siebel__c
                                              WHERE Id IN: standardController.getSelected()
                                            ];
            system.debug('................'+ selecteddocs);
        if(selecteddocs.size()>0 && selecteddocs != null ){ 
            for(Migrated_Documents_Siebel__c doc: selecteddocs){
                system.debug('--------->'+doc.File_Name__c);
                string id = Office365RestService.downloadFromOffice365ByName(doc.File_Name__c, '', '', '', '');
                system.debug('---------id>'+id);
                doc.File_URL__c = String.isNotBlank(id)? PUBLIC_SITE_URL + id : doc.File_URL__c;
                String url = String.isNotBlank(id)? PUBLIC_SITE_URL + id : '';
                urlLst.add(url);
                
            }
            system.debug('urlLst===='+urlLst);
            System.debug('selecteddocs: '+JSON.serializePretty(selecteddocs));
            System.debug(Limits.getDMLStatements()+' <dml> '+Limits.getLimitDMLStatements());
            System.debug(Limits.getDMLRows()+' <rows> '+Limits.getLimitDMLRows());
            update selecteddocs;
            
        }else{
            String url = 'Error';
                urlLst.add(url);
            ApexPages.addmessage(new ApexPages.message
                                    (ApexPages.severity.Error, 'Please Select at Least one file'));
            
        }
    }
        public Pagereference BackToCase(){
         List<Migrated_Documents_Siebel__c> selecteddocs = [ SELECT Id
                                                 , File_Name__c
                                                 , File_URL__c
                                                 , SR__c
                                              FROM Migrated_Documents_Siebel__c
                                              WHERE Id IN: standardController.getSelected()
                                            ];
            system.debug('................'+ selecteddocs);
            if(selecteddocs != null && selecteddocs.size() >0){
                string parent = String.isNotBlank(selecteddocs[0].SR__c)?selecteddocs[0].SR__c:'';

            String detailPage = SFDC_SITE_URL+ parent;
            pageReference pg = new pageReference(detailPage);
                    pg.setRedirect(TRUE);
                    return pg;

            }else{
                 String detailPage = SFDC_SITE_URL;
                    pageReference pg = new pageReference(detailPage);
                    pg.setRedirect(TRUE);
                    return pg;

            }
            
        }
         
    
}