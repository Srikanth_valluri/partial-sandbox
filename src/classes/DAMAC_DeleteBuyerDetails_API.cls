/***************************************************************************************************
 * @Name              : DAMAC_DeleteBuyerDetails_API
 * @Test Class Name   : DAMAC_DeleteBuyerDetails_API_Test
 * @Description       : 
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         23/07/2020       Created
****************************************************************************************************/
@RestResource(urlMapping='/deleteBuyerDetails')
global class DAMAC_DeleteBuyerDetails_API{
    @HttpPOST
    global static deleteBuyerResponse getResults(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String reqBody = req.requestBody.toString();
        System.debug (reqBody);
        String statusMessage;
        String statusCode;
        deleteBuyerResponse resp = new deleteBuyerResponse ();
        //String buyerId = req.params.get('buyerId');
        String jsonBody = req.requestBody.toString();
        system.debug('jsonBody: ' + jsonBody);
        deleteBuyerRequest reqst = (deleteBuyerRequest) JSON.deserialize(jsonBody, deleteBuyerRequest.class);               
        system.debug('reqst: ' + reqst);
        List<buyer__c> deleteBuyerList = new List<buyer__c>();
        List<Unit_Documents__c> deleteRelatedUnitDocumentsList = new List<Unit_Documents__c>();
        list<String> requestBuyerIds = reqst.buyerIds;
        //if(reqst.buyerId != NULL && reqst.buyerId != ''){
        if(requestBuyerIds != NULL && !requestBuyerIds.isempty()){
            deleteBuyerList = [SELECT Id FROM Buyer__c WHERE Id in: requestBuyerIds];
            deleteRelatedUnitDocumentsList = [SELECT Id FROM Unit_Documents__c WHERE Buyer__c in: deleteBuyerList];
        }
        else{
            statusMessage = 'There Is No Id Given!!';
            statusCode = '400';
        }
        if(deleteBuyerList.size() > 0){
            delete deleteBuyerList;
            delete deleteRelatedUnitDocumentsList;
            statusCode = '200';
            statusMessage = 'Success';
        }
        else{
            statusCode = '400';
            statusMessage = 'Please Enter A Valid Id!!';
        }
        resp.statusCode = statusCode;
        resp.statusMessage = statusMessage; 
        return resp;
    
    
    }    
    global class deleteBuyerRequest {
        public list<String> buyerIds {get; set;}        
       
    }
    
    global class deleteBuyerResponse {        
        public String StatusCode{get; set;}
        public String StatusMessage{get; set;}
    }    

}