@isTest
private class ViolationNoticeReminderInvocableTest {

	@isTest static void testreminder_sendViolationNoticeEmail() {
		Id RecordTypeIdFMCase = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Violation Notice').getRecordTypeId();
		FM_Case__c fmObj = new FM_Case__c();
		fmObj.recordtypeid = RecordTypeIdFMCase;
	    insert fmObj;
		List<FM_Case__c> lstFMCases = new List<FM_Case__c>();
		lstFMCases.add(fmObj);
		Test.startTest();
		ViolationNoticeReminderInvocable.sendViolationNoticeEmail(lstFMCases);
		Test.stopTest();
	}

}