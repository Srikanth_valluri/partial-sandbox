Public class Damac_SalesOfficeOwnerAllocation {
    
    public Inquiry__c assignOwner (Inquiry__c inquiry, ID campaignId,boolean isTimebasedConversion) {
        
        if (campaignId != NULL) {                
            inquiry = checkForCampaign (inquiry, campaignId, inquiry.Allocation_model__c);
        } else {
            //Check for the Users based on sales office
            inquiry = checkForUser (inquiry, inquiry.Allocation_model__c,isTimebasedConversion);
        }    
        
        return inquiry;
    }
    
    public Inquiry__c checkForCampaign (Inquiry__c inq, ID campaignId, String model) {
        List <Campaign__c> activeCampaigns = new List <Campaign__c> ();
        Map <ID, Inquiry_User_Assignment_Rules__c> userRulesMap = new Map <ID, Inquiry_User_Assignment_Rules__c> ();
        activeCampaigns = [select Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c,
                             Active__c,Language__c from Campaign__c 
                             where id =: campaignID];
                             
        Set <Id> userIds = new Set <Id>();
        Set <Id> allParentCampIds = new Set <Id>();
        if (activeCampaigns.size () > 0) {
            for ( Campaign__c camp : ActiveCampaigns ) {
                set <Id> ParentCampIds = new set <Id>();
                parentCampIds.add (camp.id);
                if ( camp.Parent_Campaign__c != null ) {
                    parentCampIds.add(camp.Parent_Campaign__c);
                }
                if ( camp.Parent_Campaign__r.Parent_Campaign__c != null ) {
                    parentCampIds.add(camp.Parent_Campaign__r.Parent_Campaign__c);
                }
                if ( camp.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c != null ) {
                    parentCampIds.add(camp.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c);
                }
                if ( camp.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c != null) {
                    parentCampIds.add(camp.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c);
                }
                if ( camp.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c != null) {
                    parentCampIds.add(camp.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c);
                }
                if ( camp.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c != null ) {
                    parentCampIds.add(camp.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c);
                }
                allParentCampIds.addAll(parentCampIds);
            }
            List<Assigned_PC__c> assignedPcs = [select User__c,Campaign__c from Assigned_PC__c where Campaign__c in : allParentCampIds];
            
            for (Assigned_PC__c pc : assignedPcs) {
                userIds.add (pc.User__c);
            }
        }

        List <ID> first25Owners = new List <ID> ();
        for (Inquiry_User_Assignment_Rules__c rule :[SELECT 
                                SetupOwnerId,  
                                Net_Direct_Sales_Rank__c
                            FROM 
                                Inquiry_User_Assignment_Rules__c
                            WHERE setupOwnerId != NULL 
                            AND setupOwnerId IN : userIds 
                            Order By Net_Direct_Sales_Rank__c ]) {
            
            userRulesMap.put (rule.setupOwnerId, rule); 
            
        } 

        Map <ID, User> users = new Map <Id, User> ([SELECT Languages_Known__c,Date_of_Joining__c FROM user 
                    WHERE id in : userRulesMap.keySet () 
                    AND IsActive = True
                    AND Is_Blacklisted__c = False
                    AND isUserOnLeave__c = False]);
        List <User> usersToassign = new List <User> ();
        Integer i = 1;
        Activate_New_Assignment_Process__c newProcess = Activate_New_Assignment_Process__c.getInstance (UserInfo.getUserID ());
        for (Id rule :userRulesMap.keySet ()) {
            if (users.containsKey (rule)) {
                User u = users.get (rule);
                if (model == 'Power Line') {
                    if (usersToassign.size () <= newProcess.Power_Line_Range_Ends_At__c)
                        usersToassign.add (u);
                }
    
                if (model == 'Non Power Line') {
                    if (i > newProcess.Non_Power_Line_Starts_At__c) {
                        usersToassign.add (u);
                    }
                }
                i++;
            }
            
        }
         
        System.Debug ( ' SIZE '+usersToassign.size ());          
        if (usersToassign.size () > 0) { 
            Map <Inquiry__c, List <User>> usersToAssignMap = new Map <Inquiry__c, List <User>> ();
            usersTOAssignMap.put (inq, usersToassign);
            Map<Inquiry__c,Id> ownerIds = InquiryOwnerAssignment.assignOwner (usersTOAssignMap);
            inq.OwnerId = ownerIds.get(inq);
        } else {
            inq.OwnerId = Label.Non_Power_Line_Queue_Id;
        }
        return inq;
        
    }
    
    public Inquiry__c checkForUser (Inquiry__c inq, String model, boolean isTimebasedConversion ) {
        Map <ID, Inquiry_User_Assignment_Rules__c> userRulesMap = new Map <ID, Inquiry_User_Assignment_Rules__c> ();
        List <ID> first25Owners = new List <ID> ();
        for (Inquiry_User_Assignment_Rules__c rule :[SELECT 
                                SetupOwnerId,  
                                Net_Direct_Sales_Rank__c
                            FROM 
                                Inquiry_User_Assignment_Rules__c
                            WHERE User_Profile__c = 'Property Consultant' AND setupOwnerId != NULL 
                            Order By Net_Direct_Sales_Rank__c ]) {
             
                userRulesMap.put (rule.setupOwnerId, rule);               
            
        } 
        
        Map <ID, User> users;
        if(!isTimebasedConversion){
             users = new Map <Id, User> ([SELECT Languages_Known__c,Date_of_Joining__c FROM user 
                        WHERE id in : userRulesMap.keySet () 
                        AND IsActive = True
                        AND Is_Blacklisted__c = False
                        AND isUserOnLeave__c = False
                        AND Sales_Office_for_Assignment__c =: inq.Sales_Office__c]);
        }
        if(isTimebasedConversion){
            system.debug('RuleId==>'+inq.inquiry_assignment_rules__c);
            list<inquiry_assignment_rules__c> rule = new list<inquiry_assignment_rules__c>();
            rule = [select id,sales_office__c from inquiry_assignment_rules__c where id=:inq.inquiry_assignment_rules__c and sales_office__c!= null];
            if(rule.size() > 0 ){
                SYSTEM.DEBUG('RULE'+RULE);
                users = new Map <Id, User> ([SELECT Languages_Known__c,Date_of_Joining__c FROM user 
                            WHERE id in : userRulesMap.keySet () 
                            AND IsActive = True
                            AND Is_Blacklisted__c = False
                            AND isUserOnLeave__c = False
                            AND Sales_Office_for_Assignment__c =: rule[0].Sales_Office__c]);
                SYSTEM.DEBUG(USERS);
                
                
            }
        }
                    
        List <User> usersToassign = new List <User> ();
        Integer i = 1;
        Activate_New_Assignment_Process__c newProcess = Activate_New_Assignment_Process__c.getInstance (UserInfo.getUserID ());
        for (Id rule :userRulesMap.keySet ()) {
            if (users.containsKey (rule)) {
                User u = users.get (rule);
                if (model == 'Power Line') {
                    if (usersToassign.size () <= newProcess.Power_Line_Range_Ends_At__c)
                        usersToassign.add (u);
                }
    
                if (model == 'Non Power Line') {
                    if (i > newProcess.Non_Power_Line_Starts_At__c) {
                        usersToassign.add (u);
                    }
                }
                i++;
            }
            
        }
        
        if (usersToassign.size () > 0) { 
            Map <Inquiry__c, List <User>> usersToAssignMap = new Map <Inquiry__c, List <User>> ();
            usersToAssignMap.put (inq, usersToassign);
            Map<Inquiry__c,Id> ownerIds = InquiryOwnerAssignment.assignOwner (usersToAssignMap);
    
            inq.OwnerId = ownerIds.get(inq);
        } else {// If No user from the Non powerline 
            
            //Check for powerline again , even the rule says Non Powerline 
            for (Id rule :userRulesMap.keySet ()) {
                if (users.containsKey (rule)) {
                    User u = users.get (rule);
                    if (model == 'Non Power Line') {
                        if (usersToassign.size () <= newProcess.Power_Line_Range_Ends_At__c){
                            usersToassign.add (u);
                        }
                    }
                    
                }            
            }
            
            if (usersToassign.size () > 0) { 
                Map <Inquiry__c, List <User>> usersToAssignMap = new Map <Inquiry__c, List <User>> ();
                usersToAssignMap.put (inq, usersToassign);
                Map<Inquiry__c,Id> ownerIds = InquiryOwnerAssignment.assignOwner (usersToAssignMap);        
                inq.OwnerId = ownerIds.get(inq);
            }else{// still cannot find in top list of powerline assign to queue            
                inq.OwnerId = Label.Non_Power_Line_Queue_Id;
            }
        }
        return inq;
    }
    
}