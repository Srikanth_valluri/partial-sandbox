public class BulkUploadMetadata {    
    public void updateBulkMetadata(){
        try{
            Metadata.DeployContainer mdContainer = new Metadata.DeployContainer();
            for(Agent_Sign_Positions__mdt objMetadata : [SELECT Id, DeveloperName, MasterLabel, Label, Height__c, 
                                                         Width__c, Document_Name__c FROM Agent_Sign_Positions__mdt 
                                                         WHERE Height__c = NULL 
                                                         AND (Document_Name__c = 'Finder\'s Fee Agreement Download Indv' 
                                                              OR Document_Name__c = 'Finder\'s Fee Agreement Download Corp_Chinese' 
                                                              OR Document_Name__c = 'Finder\'s Fee Agreement for download' 
                                                              OR Document_Name__c = 'Jordan Selling Agent Agreement for download - Corp' 
                                                              OR Document_Name__c = 'Jordan Selling Agent Agreement for download - Indv' 
                                                              OR Document_Name__c = 'KSA Referral Agreement for download - Org' 
                                                              OR Document_Name__c = 'KSA Referral Agreement for download - Indv' 
                                                              OR Document_Name__c = 'Lebanon Finder\'s Fee Download Corp' 
                                                              OR Document_Name__c = 'Lebanon Finder\'s Fee Download Indv' 
                                                              OR Document_Name__c = 'Sale and Lease Agreement Download' 
                                                              OR Document_Name__c = 'UK Selling Agent Agreement for download China' 
                                                              OR Document_Name__c = 'UK Selling Agent Agreement for download - Indv' 
                                                              OR Document_Name__c = 'UK Selling Agent Agreement for download - Org')]) 
            {
                System.debug(':::objMetadata:: ' + objMetadata);
                Metadata.CustomMetadata metadataRec =  new Metadata.CustomMetadata();
                metadataRec.fullName = 'Agent_Sign_Positions__mdt.' + objMetadata.DeveloperName;
                metadataRec.label = objMetadata.MasterLabel;
                Metadata.CustomMetadataValue customFieldtoUpdate = new Metadata.CustomMetadataValue();
                customFieldtoUpdate.field = 'Height__c';
                customFieldtoUpdate.value = 21;
                metadataRec.values.add(customFieldtoUpdate);
                mdContainer.addMetadata(metadataRec);
            }
            System.debug('mdContainer**' + mdContainer);
            Id jobId = Metadata.Operations.enqueueDeployment(mdContainer, null);
            System.debug('jobId***' + jobId);
        } catch(Exception ex) {
            System.debug('exception ' + ex.getMessage());                                 
        }
    }
}