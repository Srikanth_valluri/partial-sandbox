/************************************************************************************************** 
* Created Date       : 13/11/2018                                                                 *
* Created By         : Bhanu Gupta                                                    			  *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR          DATE                                                                *
* 1.0         Bhanu Gupta    13/03/2017                                                           *
**************************************************************************************************/
global without sharing class updateBookingUnitStatus implements NSIBPM.CustomCodeExecutable {
	
	public String EvaluateCustomCode(NSIBPM__Service_Request__c SR, NSIBPM__Step__c step) {
        String retStr = 'Success';
        List<Id> BookingIds= new List<id>();
        Boolean isDPOK ;
        Boolean isDocOK;
        list<Booking_Unit__c> BUlst = new list<Booking_Unit__c>();
        list<Booking_Unit__c> BUlsttoUpdate = new list<Booking_Unit__c>();
        try{

            for(Booking__c Bookings :[select Deal_SR__r.DP_ok__c,
            								Deal_SR__r.Doc_ok__c,
            								(Select id, Registration_Status__c 
                                     		from Booking_Units__r), 
            								id from Booking__c where Deal_SR__c=: step.NSIBPM__SR__c]){
                BookingIds.add(Bookings.id);
                updateBookingStatus(BookingIds);
            }
        }catch (Exception e) {
            retStr = 'Error :' + e.getMessage() + '';
        }
        return retStr;
	}
	@future(callout=true)
	public static void updateBookingStatus(list<Id> bookingIds){
        if(Bookingids != null && BookingIds.size()>0){
            //system.debug('#### invoking CC_UpdateRegnStatus');
            IPMS_Registration_Status_Update.sendRegnUpdate(BookingIds,'STATUS_UPDATE','LB');
        }
    }
}