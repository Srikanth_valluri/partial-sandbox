/**
 * Ver       Date            Author      		    Modification
 * 1.0    1/14/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public class fmcOTPServiceComponentController {

	public String 				SRToProcess 	{get; set;}
	public Boolean 				renderMainBool  {get; set;}
	public String   			strAccountId 	{get; set;}
	public boolean 				allowOTPEnter	{get; set;}
	public String 				OTPEntered		{get; set;}
	public boolean 				proceedToSR 	{get; set;}
	public List<Account> 		lstAccount;
	public String 				smsCode			{get; set;}
	public boolean 				invalidMsg		{get; set;}
	public String 				phoneToShow		{get; set;}

	public fmcOTPServiceComponentController() {
		smsCode = 'test';
		allowOTPEnter = false;
		invalidMsg = false;
		strAccountId = CustomerCommunityUtils.customerAccountId;
		System.debug('strAccountId: ' + strAccountId);

		lstAccount = [SELECT id, 
						     Name,
						     IsPersonAccount,
						     Mobile__c, 
						     Mobile_Phone_Encrypt__pc,
						     Customer_Masked_Mobile__c,
						     Corporate_Customer_Mobile__c
					 FROM Account 
					 WHERE id =:strAccountId];
		System.debug('lstAccount: '+lstAccount);
		
		if(lstAccount.size() > 0) {
			phoneToShow = lstAccount[0].IsPersonAccount ?
						  lstAccount[0].Customer_Masked_Mobile__c :
						  lstAccount[0].Corporate_Customer_Mobile__c;
		    System.debug('phoneToShow: '+phoneToShow);
		}
		
	}

	public void sendVerificationCodes() {
		//System.debug('SR to process : '+SRToProcess);
		System.debug('renderMainBool before: '+renderMainBool);
		renderMainBool = True;
		System.debug('renderMainBool: '+renderMainBool);

		if(lstAccount!= null && lstAccount.size() > 0 && (lstAccount[0].Mobile__c!= null || 
			lstAccount[0].Mobile_Phone_Encrypt__pc != null)) {

			smsCode = String.valueOf((Math.random() * 10000).intValue()).leftPad(4, '0');
			System.debug('smsCode is: '+smsCode);

			SMS_History__c verificationSms = new SMS_History__c();
			verificationSms.Customer__c = strAccountId;
			verificationSms.Phone_Number__c = lstAccount[0].IsPersonAccount ?
											  lstAccount[0].Mobile_Phone_Encrypt__pc :
											  lstAccount[0].Mobile__c;
			verificationSms.Message__c = smsCode + ' is the code for verifying your mobile number. '
			        + 'Please enter this code to submit the Work Permit Request '
			        + 'on My Community Portal';

			//The limit of the name is 80 to save the record. Added the logic to bypass such scenario 		
			String strSMSName =  'Portal Work Permit Verification for '+lstAccount[0].Name;
			if(strSMSName.length() >= 80){
				strSMSName = 'Portal Work Permit Verification for '+lstAccount[0].Id;
			}
			verificationSms.Name = strSMSName;
			System.debug('-->>verificationSms: '+verificationSms);

			insert verificationSms;

			System.debug('sending SMS');
			SendSMSQueueable objSMSQue = new SendSMSQueueable(verificationSms.Id);
			Id jobId = System.enqueueJob(objSMSQue);
			system.debug('jobId' + jobId);
			
			allowOTPEnter = True;
		}
	}

	public void verifyOTP() {
		System.debug('smsCode in verifyOTP is: '+smsCode);
		System.debug('OTPEntered is: '+OTPEntered);

		if(OTPEntered!='' && smsCode!=null && (OTPEntered == smsCode)) {
			System.debug('Verfication Successfull');
			proceedToSR = true;
			allowOTPEnter = false;
			invalidMsg = false;
		}
		else {
			invalidMsg = true;
		}
	}
}