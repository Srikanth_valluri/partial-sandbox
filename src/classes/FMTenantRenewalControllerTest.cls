/**
 * @File Name          : FMTenantRenewalControllerTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 1/22/2020, 2:40:22 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/21/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
public class FMTenantRenewalControllerTest {
    public static testmethod void testCreateFMCase(){
        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;
       
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
        
        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;
        
        PageReference myVfPage = Page.TenantRenewalProcessPage;
        
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Nationality__c ='Pakistani';
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Tenant_Renewal');
        
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAssignment() );
        
        test.startTest();
        FMTenantRenewalController obj=new FMTenantRenewalController();
         obj.insertCase();
        //obj.objFMCase=fmCaseObj;
        obj.initializeFMCaseAddDetails();
        obj.createFmCase();
        test.stopTest();
    }

    public static testmethod void testCreateFMCase1(){
        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;
       
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
        
        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns.Property_City__c = 'Dubai';
        insert buIns;
        
        PageReference myVfPage = Page.TenantRenewalProcessPage;
        
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Nationality__c ='Pakistani';
        fmCaseObj.Booking_Unit__c = buIns.id;
        fmCaseObj.Request_Type_DeveloperName__c = 'Tenant_Renewal';
        fmCaseObj.Account__c = acctIns.id;
        insert fmCaseObj;

        SR_Attachments__c objAttach = new SR_Attachments__c();
        objAttach.FM_Case__c = fmCaseObj.id;
        objAttach.Name = 'Tenancy Contract 2019';
        insert objAttach;

        FM_Additional_Detail__c  instanceEMC=new FM_Additional_Detail__c();
        instanceEMC.Emergency_Contact_Case__c=fmCaseObj.id;
        instanceEMC.RecordTypeId=Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
        instanceEMC.Name__c='Test';
        instanceEMC.Relationship__c='Test';
        instanceEMC.Phone__c='121212';
        instanceEMC.Email__c='test@test.com';
        insert instanceEMC;
        
        FM_Additional_Detail__c  instanceRC=new FM_Additional_Detail__c();
        instanceRC.Resident_Case__c =fmCaseObj.id;
        instanceRC.Name__c='Test';
        instanceRC.Phone__c='121212';
        instanceRC.Email__c='test@test.com';
        instanceRC.Age__c=12;
        instanceRC.Nationality__c='Pakistani';
        instanceRC.Passport_Number__c='121212';
        instanceRC.RecordTypeId=Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Resident').getRecordTypeId();
        insert instanceRC;
        
        FM_Additional_Detail__c  instanceVC=new FM_Additional_Detail__c();
        instanceVC.Vehicle_Case__c =fmCaseObj.id;
        instanceVC.Vehicle_Make_Model__c='test';
        instanceVC.Vehicle_Colour__c='test';
        instanceVC.Parking_Slot_Number__c='1';
        instanceVC.Vehicle_Number__c='1';
        instanceVC.Identification_Number__c='1';
        instanceVC.RecordTypeId=Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Vehicle').getRecordTypeId();
        insert instanceVC;
        

        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('Id',fmCaseObj.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Tenant_Renewal');
        
        
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAssignment() );
        
        test.startTest();
        FMTenantRenewalController obj=new FMTenantRenewalController();
        obj.strCaseId = string.valueof(fmCaseObj.Id);
        obj.objUnit = buIns;
        obj.fetchUploadedDocs(string.valueof(fmCaseObj.Id));
        obj.initializeAddDetailsMap();
        FMTenantRenewalController.getAdditionalDetailsInfo(string.valueof(fmCaseObj.Id),string.valueof(acctIns.Id));
        test.stopTest();
    }
    
    public static testmethod void submitFMCase(){
        Id profileId = [select id from profile where name = 'System Administrator'].id;
        User u=TestDataFactoryFM.createUser(profileId);
        insert u;
        
        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;
       
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;
        
        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;
        
        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;
        
        /*FM_User__c fmUser=TestDataFactoryFM.createFMUser(u,locObj);
        fmUser.FM_Role__c='FM Manager';
        insert fmUser;*/
        
        PageReference myVfPage = Page.TenantRenewalProcessPage;

        FM_Case__c fmCaseObjTR=new FM_Case__c();
        fmCaseObjTR.Issue_Date__c=Date.today();
        fmCaseObjTR.Contact_person__c='test';
        fmCaseObjTR.Description__c='test';
        fmCaseObjTR.Contact_person_contractor__c='test';
        fmCaseObjTR.Request_Type_DeveloperName__c='Tenant_Registration';
        fmCaseObjTR.Booking_Unit__c=buIns.id;
        fmCaseObjTR.Account__c=acctIns.id;
        fmCaseObjTR.Approving_Authorities__c='FM Manager';
        fmCaseObjTR.Nationality__c ='Pakistani';
        fmCaseObjTR.status__c='Closed';
        insert fmCaseObjTR;

        FM_Case__c fmCaseObjParent=new FM_Case__c();
        fmCaseObjParent.Issue_Date__c=Date.today();
        fmCaseObjParent.Contact_person__c='test';
        fmCaseObjParent.Description__c='test';
        fmCaseObjParent.Contact_person_contractor__c='test';
        fmCaseObjParent.Request_Type_DeveloperName__c='Tenant_Registration';
        fmCaseObjParent.Booking_Unit__c=buIns.id;
        fmCaseObjParent.Account__c=acctIns.id;
        fmCaseObjParent.Approving_Authorities__c='FM Manager';
        fmCaseObjParent.Nationality__c ='Pakistani';
        fmCaseObjParent.Status__c = 'Closed';
        insert fmCaseObjParent;
        
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Parent_Case__c=fmCaseObjParent.id;
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Tenant_Renewal';
        fmCaseObj.Booking_Unit__c=buIns.id;
        fmCaseObj.Account__c=acctIns.id;
        fmCaseObj.Approving_Authorities__c='FM Manager';
        fmCaseObj.Nationality__c ='Pakistani';
        //insert fmCaseObj;
        
        FM_Additional_Detail__c  instanceEMC=new FM_Additional_Detail__c();
        instanceEMC.Emergency_Contact_Case__c=fmCaseObjParent.id;
        instanceEMC.RecordTypeId=Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
        instanceEMC.Name__c='Test';
        instanceEMC.Relationship__c='Test';
        instanceEMC.Phone__c='121212';
        instanceEMC.Email__c='test@test.com';
        insert instanceEMC;
        
        FM_Additional_Detail__c  instanceRC=new FM_Additional_Detail__c();
        instanceRC.Resident_Case__c =fmCaseObjParent.id;
        instanceRC.Name__c='Test';
        instanceRC.Phone__c='121212';
        instanceRC.Email__c='test@test.com';
        instanceRC.Age__c=12;
        instanceRC.Nationality__c='Pakistani';
        instanceRC.Passport_Number__c='121212';
        instanceRC.RecordTypeId=Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Resident').getRecordTypeId();
        insert instanceRC;
        
        FM_Additional_Detail__c  instanceVC=new FM_Additional_Detail__c();
        instanceVC.Vehicle_Case__c =fmCaseObjParent.id;
        instanceVC.Vehicle_Make_Model__c='test';
        instanceVC.Vehicle_Colour__c='test';
        instanceVC.Parking_Slot_Number__c='1';
        instanceVC.Vehicle_Number__c='1';
        instanceVC.Identification_Number__c='1';
        instanceVC.RecordTypeId=Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Vehicle').getRecordTypeId();
        insert instanceVC;
        
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Tenant_Renewal');
        //ApexPages.currentPage().getParameters().put('Id',fmCaseObj.id);
        
        FMTenantRenewalController obj=new FMTenantRenewalController();
        
        //obj.objFMCase.id=fmCaseObj.id;
        
        test.startTest();
        obj.objFMCase = fmCaseObj;
        obj.insertCase();
        obj.strDetailType='Emergency Contact';
        obj.addDetails();
        obj.indexOfNewChildToRemove=0;
        obj.removeDetails();
        obj.submitFmCase();
        //obj.returnBackToCasePage();
        test.stopTest();
    }
    
    public static testmethod void testUploadDocument(){
        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;
        
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
        
        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;
        
        PageReference myVfPage = Page.TenantRenewalProcessPage;
        
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Booking_Unit__c = buIns.Id ;
        fmCaseObj.Request_Type_DeveloperName__c='Tenant_Renewal';
        
        insert fmCaseObj;
        
        Test.setCurrentPage(myVfPage);
        //ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        //ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        //ApexPages.currentPage().getParameters().put('SRType','Tenant_Renewal');
        ApexPages.currentPage().getParameters().put('Id',fmCaseObj.Id);
        Test.setMock( WebServiceMock.class, new MoveOutMockClass() );

        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
        //Set mock response
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive2());

        FMTenantRenewalController obj=new FMTenantRenewalController();
        obj.insertCase();
        obj.strDocumentBody='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        obj.strDocumentName='Test_Document_Work_Permit.htm';
        //obj.createCaseShowUploadDoc();
        //obj.initializeFMCaseAddDetails();
        
        test.startTest();
        obj.uploadDocument();
        obj.isPortalEnabled = false;
        
        test.stopTest();
    }  

    public static testmethod void submitFMCase2(){
        Id profileId = [select id from profile where name = 'System Administrator'].id;
        User u=TestDataFactoryFM.createUser(profileId);
        insert u;
        
        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;
       
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;
        
        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;
        
        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;
        
        /*FM_User__c fmUser=TestDataFactoryFM.createFMUser(u,locObj);
        fmUser.FM_Role__c='FM Manager';
        insert fmUser;*/
        
        PageReference myVfPage = Page.TenantRenewalProcessPage;

        FM_Case__c fmCaseObjTR=new FM_Case__c();
        fmCaseObjTR.Issue_Date__c=Date.today();
        fmCaseObjTR.Contact_person__c='test';
        fmCaseObjTR.Description__c='test';
        fmCaseObjTR.Contact_person_contractor__c='test';
        fmCaseObjTR.Request_Type_DeveloperName__c='Tenant_Registration';
        fmCaseObjTR.Booking_Unit__c=buIns.id;
        fmCaseObjTR.Account__c=acctIns.id;
        fmCaseObjTR.Approving_Authorities__c='FM Manager';
        fmCaseObjTR.Nationality__c ='Pakistani';
        fmCaseObjTR.status__c='Closed';
        insert fmCaseObjTR;

        FM_Case__c fmCaseObjParent=new FM_Case__c();
        fmCaseObjParent.Issue_Date__c=Date.today();
        fmCaseObjParent.Contact_person__c='test';
        fmCaseObjParent.Description__c='test';
        fmCaseObjParent.Contact_person_contractor__c='test';
        fmCaseObjParent.Request_Type_DeveloperName__c='Tenant_Registration';
        fmCaseObjParent.Booking_Unit__c=buIns.id;
        fmCaseObjParent.Account__c=acctIns.id;
        fmCaseObjParent.Approving_Authorities__c='FM Manager';
        fmCaseObjParent.Nationality__c ='Pakistani';
        fmCaseObjParent.Status__c = 'Closed';
        insert fmCaseObjParent;
        
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Parent_Case__c=fmCaseObjParent.id;
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Tenant_Renewal';
        fmCaseObj.Booking_Unit__c=buIns.id;
        fmCaseObj.Account__c=acctIns.id;
        fmCaseObj.Approving_Authorities__c='FM Manager';
        fmCaseObj.Nationality__c ='Pakistani';
        //insert fmCaseObj;
        
        FM_Additional_Detail__c  instanceEMC=new FM_Additional_Detail__c();
        instanceEMC.Emergency_Contact_Case__c=fmCaseObjParent.id;
        instanceEMC.RecordTypeId=Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
        instanceEMC.Name__c='Test';
        instanceEMC.Relationship__c='Test';
        instanceEMC.Phone__c='121212';
        instanceEMC.Email__c='test@test.com';
        insert instanceEMC;
        
        FM_Additional_Detail__c  instanceRC=new FM_Additional_Detail__c();
        instanceRC.Resident_Case__c =fmCaseObjParent.id;
        instanceRC.Name__c='Test';
        instanceRC.Phone__c='121212';
        instanceRC.Email__c='test@test.com';
        instanceRC.Age__c=12;
        instanceRC.Nationality__c='Pakistani';
        instanceRC.Passport_Number__c='121212';
        instanceRC.RecordTypeId=Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Resident').getRecordTypeId();
        insert instanceRC;
        
        FM_Additional_Detail__c  instanceVC=new FM_Additional_Detail__c();
        instanceVC.Vehicle_Case__c =fmCaseObjParent.id;
        instanceVC.Vehicle_Make_Model__c='test';
        instanceVC.Vehicle_Colour__c='test';
        instanceVC.Parking_Slot_Number__c='1';
        instanceVC.Vehicle_Number__c='1';
        instanceVC.Identification_Number__c='1';
        instanceVC.RecordTypeId=Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Vehicle').getRecordTypeId();
        insert instanceVC;
        
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Tenant_Renewal');
        //ApexPages.currentPage().getParameters().put('Id',fmCaseObj.id);
        
        FMTenantRenewalController obj=new FMTenantRenewalController();
        
        //obj.objFMCase.id=fmCaseObj.id;
        
        test.startTest();
        obj.objFMCase = fmCaseObj;
        obj.insertCase();
        obj.strDetailType='Pet Details';
        obj.addDetails();
        obj.strDetailType='Resident';
        obj.addDetails();
        obj.indexOfNewChildToRemove=0;
        obj.removeDetails();
        SR_Attachments__c objSR_Attachments_Insert = new SR_Attachments__c(
            Name = 'Test Attachment',
            Description__c = 'Test Description'
        );
        insert objSR_Attachments_Insert;
        SR_Attachments__c objSR_Attachments = [SELECT Id FROM SR_Attachments__c LIMIT 1];
        obj.deleteAttRecId = objSR_Attachments.Id;
        obj.deleteAttachment();
        obj.submitFmCase();
        //obj.returnBackToCasePage();
        test.stopTest();
    }
}