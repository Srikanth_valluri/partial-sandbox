public class DAMAC_PromoterStandChecklistController{
    public Stands_Checklist__c standChecklist { get; set; }
    public String promoterName { get; set; }
    public String standManager { get; set; }
    public Date dldDate { get; set; }
    public Date reraDate { get; set; }
    public String shiftLeader { get; set; }
    public String msgType { get; set; }
    public String msg { get; set; }
    public DAMAC_PromoterStandChecklistController(){
        msgType = '';
        msg = '';
        doinit();
    }
    public void doinit(){
        standChecklist = new Stands_Checklist__c();
        standChecklist.Date__c = Date.Today();
        User standManagerUser = [SELECT Name FROM User WHERE ID =: Label.Stand_Manager_ID];
        standChecklist.Stand_Manager__c = standManagerUser.Id;
        standManager = standManagerUser.Name;
        
        User currentUser = [SELECT Stand_Id__c,Name FROM User WHERE Id =:UserInfo.getUserId()];
        if(currentUser.Stand_Id__c != null && currentUser.Stand_Id__c  != '' && !Test.isRunningTest()){
            Campaign__c eachCampaign = [SELECT Campaign_Name__c,Owner.Name,OwnerId FROM Campaign__c WHERE Id =: currentUser.Stand_Id__c];
            standChecklist.Stand__c  = eachCampaign.Campaign_Name__c;
            
            
            shiftLeader = '';
            
        }
        standChecklist.Promoter_Name__c = currentUser.Id;
        promoterName = currentUser.Name;
    }
    public List<SelectOption> getOptions(){   
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Stands_Checklist__c.IT_Equipments__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple) {
              options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return options;
    }
    public List<SelectOption> getConditions(){   
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Stands_Checklist__c.Stand_Condition__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple) {
              options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return options;
    }
    public void insertStandchecklist(){
        try{
            standChecklist.DLD_Permit_Expiry_Date__c = dldDate;
            standChecklist.RERA_Permit_Expiry_Date__c = reraDate;
            if (standChecklist.IT_Equipments__c != NULL) {
                String location = standChecklist.IT_Equipments__c ;
                if (location.contains(', ')) {
                    location = location.removeStart('[').removeEnd(']').replaceAll(', ', ';');
                } else {
                    location = location.removeStart('[').removeEnd(']');
                }
                System.Debug(location);
                standChecklist.IT_Equipments__c = location;
            }
            if (standChecklist.Stand_Condition__c != NULL) {
                String location = standChecklist.Stand_Condition__c;
                if (location.contains(', ')) {
                    location = location.removeStart('[').removeEnd(']').replaceAll(', ', ';');
                } else {
                    location = location.removeStart('[').removeEnd(']');
                }
                standChecklist.Stand_Condition__c = location;
            }
            if (standCheckList.IT_Equipments__c != null && standChecklist.Stand_Condition__c != null) {
                insert standChecklist ;
                
                msgType = 'success';
                msg = 'Stand Checklist Created Successfully';
                doinit();
            } else {
                msgType = 'error';
                msg = 'Please fill all required information';
            }
            
            system.debug (msg);
         }catch(Exception e){
             msgType = 'error';
            msg = e.getMessage();
             System.debug(e.getMessage());
         }
    } 
    
    @RemoteAction 
    public static List<User> getUserDetails(String searchKey){
        String key = '%' + searchKey + '%';
        String profileName = 'Promoter Community Profile';
        
        return [SELECT Id, Name FROM User WHERE Name LIKE : key AND Profile.name =: profileName  AND IsActive = true LIMIT 10];  
    }   
    
}