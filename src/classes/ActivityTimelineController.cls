/****************************************************************************************************
* Name          : ActivityTimelineController                                                   *
* Description   : 1. Class to display the Sales Tours & Activity related to Inquiry                 *
* Created Date  : 27/03/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                *
* 1.0                       27/03/2018      Initial Draft.                                          *
****************************************************************************************************/
public with sharing class ActivityTimelineController{
  public Inquiry__C inq                                                       {get; set;}
  public List<Task> inquiryTaskList                                           {get; set;}
  public List<Integer> taskIterator                                           {get; set;}
  public Integer taskCounter                                                  {get; set;}
  public Integer tourCounter                                                  {get; set;}
  public List<Sales_Tours__c> inquirySalesToursList                           {get; set;}
  public List<Integer> tourIterator                                           {get; set;}

  public ActivityTimelineController(ApexPages.StandardController stdController) {
      this.inq= (Inquiry__C )stdController.getRecord();
      inquiryTaskList = new List<Task>();
      inquirySalesToursList = new List<Sales_Tours__c>();
      taskIterator = new List<Integer>();
      tourIterator = new List<Integer>();
      inq = [SELECT name
              FROM Inquiry__c
              WHERE ID= :inq.id];
      inquiryTaskList = [  SELECT Id,
                                  WhatId,
                                  OwnerId,
                                  Description,
                                  Status,
                                  Subject,
                                  Priority,
                                  Activity_Type_3__c,
                                  ActivityDate,
                                  CreatedDate,
                                  Activity_Outcome__c,
                                  CreatedBy.Name,
                                  LastModifiedBy.Name,
                                  Is_Created_by_CTI__c,
                         	      Task_Due_Date__c,
                         		  Status__c
                             FROM Task
                            WHERE WhatID = :inq.Id
                        ORDER BY ActivityDate DESC];

      inquirySalesToursList = [SELECT Id,
                                      Name,
                                      Status__c,
                                      Inquiry_PC__c,
                                      Priority__c,
                                      Inquiry_Name__c,
                                      CreatedDate,
                                      Comments__c,
                                      Check_In_Date__c,
                                      Check_Out_Date__c,
                                      Tour_Outcome__c,
                                      Pickup_Location__c
                                 FROM Sales_Tours__c
                                WHERE Inquiry__c = :inq.Id
                                  AND Status__c = 'Not Started'
                                ORDER BY CreatedDate DESC];

      for(integer i=0; i < inquiryTaskList.size(); i++){
            taskIterator.add(i);
      }

      for(integer i=0; i < inquirySalesToursList.size(); i++){
            tourIterator.add(i);
      }

  }

  public void saveTask(){
      update inquiryTaskList[taskCounter];
  }

  public void saveTour(){
      inquirySalesToursList[tourCounter].Status__c = 'Submitted';
      update inquirySalesToursList[tourCounter];
  }

  public Boolean isSF1 {
        get {
            if(String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameHost')) ||
               String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin')) ||
                ApexPages.currentPage().getParameters().get('isdtp') == 'p1' ||
                (ApexPages.currentPage().getParameters().get('retURL') != null
                  && ApexPages.currentPage().getParameters().get('retURL').contains('projectone')
                )
            ) {
                return true;
            }else{
                return false;
            }
        }
    }
	/**
	 * Method to redirect to the Previous Page 
	 */
	public PageReference createTaskRecord() {
		
		PageReference createNewTaskPage = new PageReference('/apex/CreateNewTask?inqId=' +inq.Id);
		createNewTaskPage.setRedirect(true);
		return createNewTaskPage;
	}
}