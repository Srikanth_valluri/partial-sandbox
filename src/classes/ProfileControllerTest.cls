@isTest
private class ProfileControllerTest {

    @isTest
    static void testController() {
        Map<String, String> mapAdditionalDetails = new map<String, String> {'FirstName' => 'Bhanu'};
        User communityUser = CommunityTestDataFactory.CreatePortalUser();
        system.runAs(communityUser) {
            Test.setCurrentPage(Page.Customer);
            ApexPages.currentPage().getParameters().put('view', 'myprofileview');
            ProfileController controller = new ProfileController();
            System.debug(controller.customerContact);
            System.debug(controller.strTotalSellingPrice);
            ProfileController.updateAdditionalContact(mapAdditionalDetails);
        }
    }
    @isTest
    static void testCancelServiceRequest(){
        TestUtility utility = new TestUtility();
        List<Case> caseList = utility.caseInsert(1);
         Test.startTest();
      	ProfileController.cancelServiceRequest(caseList[0].id);
        Test.stopTest();
    }

    @isTest
    static void testCancelSrRequest(){
        TestUtility utility = new TestUtility();
        List<Case> caseList = utility.caseInsertChangeofJointBuyer(1);
        Test.startTest();
      	ProfileController.cancelServiceRequest(caseList[0].id);
        Test.stopTest();
    }

	@isTest
    static void testCancelSRequest(){
        TestUtility utility = new TestUtility();
        List<Case> caseList = utility.caseInsertAssignment(1);
        Test.startTest();
      	ProfileController.cancelServiceRequest(caseList[0].id);
        Test.stopTest();
    }

    @isTest
    static void testCancelSRequestPOP(){
        TestUtility utility = new TestUtility();
        List<Case> caseList = utility.caseInsertPOP(1);
        Test.startTest();
        ProfileController.cancelServiceRequest(caseList[0].id);
        Test.stopTest();
    }

    @isTest
    static void testOther() {
        Test.startTest();
        Test.setCurrentPage(Page.Customer);
        ApexPages.currentPage().getParameters().put('view', 'myprofileview');
        ProfileController controller = new ProfileController();
        System.debug(controller.customerCategory);
        Test.stopTest();
    }
}