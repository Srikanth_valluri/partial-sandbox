@isTest
public class Invocable_ManageDealTeam_Test {
    
    @testSetup 
    static void setupData() {
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.Vendor_ID__c = 'xxxx'; 
        insert acc;
        
        //service requests data
        List<NSIBPM__Service_Request__c> SRList = new List<NSIBPM__Service_Request__c>();
        NSIBPM__Service_Request__c changeAgentSR = new NSIBPM__Service_Request__c();
        changeAgentSR.NSIBPM__Customer__c = acc.Id;
        changeAgentSR.Agency__c = acc.id;
        changeAgentSR.Agency_Type__c = 'Corporate';
        changeAgentSR.RecordTypeId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('Change_Agent').getRecordTypeId();
        SRList.add(changeAgentSR);
        
        NSIBPM__Service_Request__c changeManagerSR = changeAgentSR.clone(false, false, false, false);
        changeManagerSR.RecordTypeId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('Deal').getRecordTypeId();
        SRList.add(changeManagerSR);
        insert SRList;
        
        //new steps data
        List<New_Step__c> newStepsList = new List<New_Step__c>();
        New_Step__c nstp1 = new New_Step__c();
        nstp1.Service_Request__c = changeAgentSR.id;
        nstp1.Step_No__c = 2;
        nstp1.Step_Status__c = 'Awaiting Token Deposit';
        nstp1.Step_Type__c = 'Token Payment';
        newStepsList.add(nstp1);
        
        New_Step__c nstp2 = new New_Step__c();
        nstp2.Service_Request__c = changeManagerSR.id;
        nstp2.Step_No__c = 2;
        nstp2.Step_Status__c = 'Awaiting Token Deposit';
        nstp2.Step_Type__c = 'Token Payment';
        newStepsList.add(nstp2);
        
        insert newStepsList;    
    }
    
    @isTest
    static void test_ManageDealTeam() {
        List<Id> stepsIdList = new List<Id>();
        for(New_Step__c stp :[select id From New_Step__c]){
            stepsIdList.add(stp.Id);    
        }
        Invocable_ManageDealTeam.ManageDealTeam(stepsIdList);
    }
    
    
}