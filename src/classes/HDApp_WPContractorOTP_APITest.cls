@isTest
public class HDApp_WPContractorOTP_APITest {
	@TestSetup
    static void TestData() {
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(Agency_ID__c = '1234');
        insert sr;
        
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;
        
        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];
        
        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Customer Community Login User(Use this)%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
        Database.insert(user1);
        
        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'test';
        locObj.Location_Type__c = 'Building';
        locObj.Location_ID__c = '12345';
        locObj.UAEProp__c = true;
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        invObj.Property_Country__c = 'UNITED ARAB EMIRATES';
        invObj.Marketing_Name_Doc__c = invObj.Id;
        invObj.Building_Location__c = locObj.Id;
        invObj.Property_Status__c = 'Ready'; 
        invObj.Unit_type__c = 'Villa';
        insert invObj;
        
        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1204',
                                      Registration_ID__c = '3901',
                                      Handover_Flag__c = 'N',
                                      Inventory__c = invObj.id,
                                      Dummy_Booking_Unit__c = true);
        insert bookingUnit;
        
        Id rtPopId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Work Permit').getRecordTypeId();
        FM_Case__c objWPFmCaseContractor = new FM_Case__c();
        objWPFmCaseContractor.Origin__c = 'Portal';
        objWPFmCaseContractor.isHelloDamacAppCase__c = true;
        objWPFmCaseContractor.Account__c = account.Id;
        objWPFmCaseContractor.RecordtypeId = rtPopId;
        objWPFmCaseContractor.Status__c = 'Draft Request';
        objWPFmCaseContractor.Request_Type__c = 'Work Permit';
        objWPFmCaseContractor.Booking_Unit__c = bookingUnit.Id;
        objWPFmCaseContractor.Work_Permit_Type__c = 'Work Permit - Including Fit Out';
        objWPFmCaseContractor.Person_To_Collect__c = 'Contractor';
        objWPFmCaseContractor.Contact_person_contractor__c = 'Test Contractor';
        objWPFmCaseContractor.Email_2__c = 'testctr@test.com';
        
        insert objWPFmCaseContractor;
        
        OTP__c otpForContractor = new OTP__c(FM_Case__c=objWPFmCaseContractor.Id,
                                             FM_Process_Name__c='Work Permit', 
                                             FM_Email_OTP__c='1234',
                                             GUID__c='b09e81ef-cf42-13f0-2a3d-e5a0384e4cb5');
        insert otpForContractor;
        
        FM_Case__c objWPFmCaseConsultant = objWPFmCaseContractor.clone();
        objWPFmCaseConsultant.Person_To_Collect__c = 'Consultant';
        objWPFmCaseConsultant.Contact_person__c = 'Test Consultant';
        objWPFmCaseConsultant.Email__c = 'testcst@test.com';
        
        insert objWPFmCaseConsultant;
        
        OTP__c otpForConsultant = new OTP__c(FM_Case__c=objWPFmCaseConsultant.Id,
                                             FM_Process_Name__c='Work Permit', 
                                             FM_Email_OTP__c='1234',
                                             GUID__c='b09e81ef-cf42-13f0-2a3d-e5a0384e4cb5');
        insert otpForConsultant;
    }
    
    @isTest
    static void testSendOTP() {
        Test.startTest();
		
        List<FM_Case__c> listFMCase = [SELECT Id,Name FROM FM_Case__c WHERE Person_To_Collect__c='Contractor' LIMIT 1];
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/contractorOTP/sendOTP';
        req.addParameter('fmCaseNumber',listFMCase[0].Name);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WPContractorOTP_API.sendContractorOTP();
        
        Test.stopTest();
    }
    
    @isTest
    static void testSendOTPWithFMCaseNumberNull() {
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/contractorOTP/sendOTP';
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WPContractorOTP_API.sendContractorOTP();
        
        Test.stopTest();
    }
    
    @isTest
    static void testSendOTPWithInvalidFMCaseNumber() {
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/contractorOTP/sendOTP';
        req.addParameter('fmCaseNumber','abcd');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WPContractorOTP_API.sendContractorOTP();
        
        Test.stopTest();
    }
    
    @isTest
    static void testSendOTPWithNoEmail() {
        Test.startTest();
        
        List<FM_Case__c> listFMCase = [SELECT Id,Name FROM FM_Case__c WHERE Person_To_Collect__c='Consultant' LIMIT 1];
        FM_Case__c objWPFmCase = listFMCase[0];
        objWPFmCase.Email__c = '';
        update objWPFmCase;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/contractorOTP/sendOTP';
        req.addParameter('fmCaseNumber',objWPFmCase.Name);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WPContractorOTP_API.sendContractorOTP();
        
        Test.stopTest();
    }
    
    @isTest
    static void testVerifyOTPForContractor() {
        Test.startTest();
        
        List<FM_Case__c> listFMCase = [SELECT Id,Name FROM FM_Case__c WHERE Person_To_Collect__c='Contractor' LIMIT 1];
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/contractorOTP/verifyOTP';
        req.addParameter('fmCaseId',listFMCase[0].Id);
        req.addParameter('otpValue','1234');
        req.addParameter('guid','b09e81ef-cf42-13f0-2a3d-e5a0384e4cb5');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WPContractorOTP_API.verifyContractorOTP();
        
        Test.stopTest();
    }
    
    @isTest
    static void testVerifyOTPForConsultant() {
        Test.startTest();
        
        List<FM_Case__c> listFMCase = [SELECT Id,Name FROM FM_Case__c WHERE Person_To_Collect__c='Consultant' LIMIT 1];
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/contractorOTP/verifyOTP';
        req.addParameter('fmCaseId',listFMCase[0].Id);
        req.addParameter('otpValue','1234');
        req.addParameter('guid','b09e81ef-cf42-13f0-2a3d-e5a0384e4cb5');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WPContractorOTP_API.verifyContractorOTP();
        
        Test.stopTest();
    }
    
    @isTest
    static void testVerifyOTPWithFMCaseIdNull() {
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/contractorOTP/verifyOTP';
        req.addParameter('otpValue','1234');
        req.addParameter('guid','b09e81ef-cf42-13f0-2a3d-e5a0384e4cb5');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WPContractorOTP_API.verifyContractorOTP();
        
        Test.stopTest();
    }
    
    @isTest
    static void testVerifyOTPWithInvalidOTP() {
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/contractorOTP/verifyOTP';
        req.addParameter('fmCaseId','abcd');
        req.addParameter('otpValue','5678');
        req.addParameter('guid','b09e81ef-cf42-13f0-2a3d-e5a0384e4cb5');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WPContractorOTP_API.verifyContractorOTP();
        
        Test.stopTest();
    }
    
    @isTest
    static void testVerifyOTPWithBlankParams() {
        Test.startTest();
        
        List<FM_Case__c> listFMCase = [SELECT Id,Name FROM FM_Case__c WHERE Person_To_Collect__c='Contractor' LIMIT 1];
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/contractorOTP/verifyOTP';
        req.addParameter('fmCaseId',listFMCase[0].Id);
        req.addParameter('otpValue','');
        req.addParameter('guid','b09e81ef-cf42-13f0-2a3d-e5a0384e4cb5');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WPContractorOTP_API.verifyContractorOTP();
        
        req = new RestRequest();
        req.requestURI = '/contractorOTP/verifyOTP';
        req.addParameter('fmCaseId',listFMCase[0].Id);
        req.addParameter('otpValue','1234');
        req.addParameter('guid','');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WPContractorOTP_API.verifyContractorOTP();
        
        Test.stopTest();
    }
}