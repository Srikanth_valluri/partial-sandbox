/*=============================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-------------------------------------------------------------------------------
1.0     |   04-10-2020      | Shubham Suryawanshi | Created initial draft
1.0     |   05-10-2020      | Subin C Antony      | Added notification cases for 'Key Handover'
1.1     |   07-10-2020      | Shubham Suryawanshi | Moved Handover Document - Approved code to 
                                                    another  method - sendHandoverDocumentApprovedEmail & called - getFormattedDateWithMonth method for date formatting. Ex. 10-Oct-2020  
1.2     |   15-10-2020      | Shubham Suryawanshi | Added null checks  
1.3     |   26-10-2020      | Shubham Suryawanshi | Added prod push-notification API endpoint                                                                                                                                                                                                             
*******************************************************************************/  

public class HDApp_handoverEmailCommunicationHandler {

    @testVisible
    private static final String push_notification_sandbox_baseurl = 'https://ptctest.damacgroup.com';
     @testVisible
    private static final String push_notification_prod_baseurl = 'https://ptc.damacgroup.com';
    @testVisible
    //private static final String push_notification_endpoint = '/hellodamac/api/v1/notifications/trigger-push';
    private static final String push_notification_sandbox_endpoint = '/hellodamac/api/v1/notifications/trigger-push';
    //https://ptc.damacgroup.com/hellodamac-prod/api/v1/notifications/trigger-push
    private static final String push_notification_prod_endpoint = '/hellodamac-prod/api/v1/notifications/trigger-push';
    @testVisible
    private static final String api_token = 'c13d9a4e2be67d853d7428f5a9a952d6';
    public static Id appointmentSchedulingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Appointment Scheduling').RecordTypeId;

    /************ Email Template Ids **************/
    @testVisible
    private static final String UNIT_INSP_APPROVED_TEMP_ID  = 'DH-BUK-UNT-INS-APR';
    @testVisible
    private static final String UNIT_INSP_REJECTED_TEMP_ID = 'DH-BUK-UNT-INS-REJ';
    @testVisible
    private static final String HANDOVER_DOC_APPROVED_TEMP_ID = 'DH-REQ-SGNF-HNDVR-DOC-SR-APR';
    @testVisible
    private static final String HANDOVER_DOC_REJECTED_TEMP_ID = 'DH-REQ-SGNF-HNDVR-DOC-SR-REJ';
    @testVisible
    private static final String HANDOVER_DOC_CANCELLED_TEMP_ID = 'DH-CANC-SGNF-HNDVR-DOC-APPNT';
    @testVisible
    private static final String KEY_HANDOVER_APPROVED_TEMP_ID = 'DH-BUK-KEY-HNDOVR-SR-APR';
    @testVisible
    private static final String KEY_HANDOVER_REJECTED_TEMP_ID = 'DH-BUK-KEY-HNDOVR-SR-REJ';
    /************ Email Template Ids **************/

    /************ Messages **************/
    public static final String UNIT_INSP_APPR_MSG = 'Your unit inspection booking request been approved.';
    public static final String UNIT_INSP_REJ_MSG = 'Your unit inspection booking request been rejected.';
    public static final String HANDOVER_DOC_APPR_MSG = 'Your request to sign off your handover documents has been approved.';
    public static final String HANDOVER_DOC_REJ_MSG = 'Your request to sign off your handover documents has been rejected.';
    public static final String KEY_HANDOVER_APPR_MSG = 'Your key handover booking request been approved.';
    public static final String KEY_HANDOVER_REJ_MSG = 'Your key handover booking request been rejected.';
    /************ Messages **************/

    /************ Titles *****************/
    public static final String BU_INSP_TITLE = 'Book Unit Inspection!';
    public static final String HANDOVER_DOC_TITLE = 'Handover documents sign-off!';
    public static final String BOOK_KEY_HANDOVER_TITLE = 'Book key handover!';
    /************ Titles *****************/

    /************ Images *****************/
    public static final String IMG_UNIT_INSP = 'https://services.damacgroup.com/DCAssets/hello-damac/notifications/handover-booking.png';
    public static final String IMG_HANDOVER_DOC = 'https://services.damacgroup.com/DCAssets/hello-damac/notifications/handover-documents.png';
    public static final String IMG_KEY_HANDOVER = 'https://services.damacgroup.com/DCAssets/hello-damac/notifications/handover-key-handover.png';
    /************ Images *****************/

    public static void sendHandoverEmail(List<Calling_List__c> lstCallingList) {
        
        System.debug('Inside sendHandoverEmail method');
        for(Calling_List__c objCL : [SELECT id,
                                            RecordTypeId,
                                            Service_Type__c,
                                            Sub_Purpose__c,
                                            Appointment_Date__c,
                                            Appointment_Rejection_Reason__c,
                                            Appointment_Status__c,
                                            Booking_Unit__r.Unit_Name__c,
                                            Appointment_Slot__c,
                                            Booking_Unit__r.Booking__r.Account__c,
                                            Booking_Unit__r.Booking__r.Account__r.Party_ID__c,
                                            Booking_Unit__r.Registration_ID__c
                                     FROM Calling_List__c
                                     WHERE Id IN : lstCallingList]) {
            
            //Unit Inspection - Approved

            
            if( (objCL.RecordTypeId != null && 
                 String.isNotBlank(objCL.Service_Type__c) &&
                 String.isNotBlank(objCL.Sub_Purpose__c) &&
                 String.isNotBlank(objCL.Appointment_Status__c) ) &&
                objCL.RecordTypeId == appointmentSchedulingRecordTypeId && 
                objCL.Service_Type__c.equalsIgnoreCase('Handover') && 
                objCL.Sub_Purpose__c.equalsIgnoreCase('Unit Viewing') &&
                objCL.Appointment_Status__c == 'Confirmed') {

                HD_SendNotificationByEmail_API.TemplateMergeData objTMData = new HD_SendNotificationByEmail_API.TemplateMergeData();
                //objTMData.customer_name = '';
                objTMData.unit = objCL.Booking_Unit__r.Unit_Name__c;  
                objTMData.appointment_date = objCL.Appointment_Date__c != null ? 
                                             HDApp_createHandoverAppointmentsHandler.getFormattedDateWithMonth( Datetime.newInstance(objCL.Appointment_Date__c.year(), objCL.Appointment_Date__c.month(), objCL.Appointment_Date__c.day()).format('yyyy-MM-dd') ) : null;
                objTMData.appointment_time = objCL.Appointment_Slot__c != null ? HDApp_getHandoverCurrentStatusHandler.get12HrsFormattedTime(objCL.Appointment_Slot__c).split('-')[0] : null  ;
                objTMData.payment_type = 2; // for footer change

                String strTMData = JSON.serialize(objTMData);
                //Send Email
                sendEmail(objCL.Booking_Unit__r.Booking__r.Account__r.Party_ID__c, UNIT_INSP_APPROVED_TEMP_ID,strTMData);
                //Send Push Notification;
                //String msg = UNIT_INSP_REJ_MSG + ' '+objCL.Appointment_Date__c != null ? Datetime.newInstance(objCL.Appointment_Date__c.year(), objCL.Appointment_Date__c.month(), objCL.Appointment_Date__c.day()).format('dd-MM-yyyy') : null;
                //msg += ' at '+objTMData.appointment_time;
                sendPushNotification(objCL.Booking_Unit__r.Booking__r.Account__c, UNIT_INSP_APPR_MSG, BU_INSP_TITLE, Integer.valueOf(objCL.Booking_Unit__r.Booking__r.Account__r.Party_ID__c), 'handover', '', Integer.valueOf(objCL.Booking_Unit__r.Registration_ID__c), '',IMG_UNIT_INSP);
            }

            ///Unit Inspection - Rejected
            if( (objCL.RecordTypeId != null && 
                 String.isNotBlank(objCL.Service_Type__c) &&
                 String.isNotBlank(objCL.Sub_Purpose__c) &&
                 String.isNotBlank(objCL.Appointment_Status__c) ) &&
                objCL.RecordTypeId == appointmentSchedulingRecordTypeId && 
                objCL.Service_Type__c.equalsIgnoreCase('Handover') && 
                objCL.Sub_Purpose__c.equalsIgnoreCase('Unit Viewing') &&
                objCL.Appointment_Status__c == 'Rejected') {

                HD_SendNotificationByEmail_API.TemplateMergeData objTMData = new HD_SendNotificationByEmail_API.TemplateMergeData();

                objTMData.unit = objCL.Booking_Unit__r.Unit_Name__c;
                objTMData.appointment_rejected_reason = objCL.Appointment_Rejection_Reason__c;
                objTMData.payment_type = 2;

                String strTMData = JSON.serialize(objTMData);
                //Send Email
                sendEmail(objCL.Booking_Unit__r.Booking__r.Account__r.Party_ID__c, UNIT_INSP_REJECTED_TEMP_ID,strTMData);
                //Send Push Notification;
                //String msg = UNIT_INSP_REJ_MSG + ' '+objCL.Appointment_Date__c != null ? Datetime.newInstance(objCL.Appointment_Date__c.year(), objCL.Appointment_Date__c.month(), objCL.Appointment_Date__c.day()).format('dd-MM-yyyy') : null;
                //msg += ' at '+objTMData.appointment_time;
                sendPushNotification(objCL.Booking_Unit__r.Booking__r.Account__c, UNIT_INSP_REJ_MSG, BU_INSP_TITLE, Integer.valueOf(objCL.Booking_Unit__r.Booking__r.Account__r.Party_ID__c), 'handover', '', Integer.valueOf(objCL.Booking_Unit__r.Registration_ID__c), '', IMG_UNIT_INSP);
            }

            //Handover Documents - Rejected
            if( (objCL.RecordTypeId != null && 
                 String.isNotBlank(objCL.Service_Type__c) &&
                 String.isNotBlank(objCL.Sub_Purpose__c) &&
                 String.isNotBlank(objCL.Appointment_Status__c) ) &&
                objCL.RecordTypeId == appointmentSchedulingRecordTypeId && 
                objCL.Service_Type__c.equalsIgnoreCase('Handover') && 
                objCL.Sub_Purpose__c.equalsIgnoreCase('Documentation') &&
                objCL.Appointment_Status__c == 'Rejected') {

                 HD_SendNotificationByEmail_API.TemplateMergeData objTMData = new HD_SendNotificationByEmail_API.TemplateMergeData();
                //objTMData.customer_name = '';
                objTMData.unit = objCL.Booking_Unit__r.Unit_Name__c;  
                objTMData.appointment_rejected_reason = objCL.Appointment_Rejection_Reason__c;
                objTMData.payment_type = 2;


                String strTMData = JSON.serialize(objTMData);
                //Send Email
                sendEmail(objCL.Booking_Unit__r.Booking__r.Account__r.Party_ID__c, HANDOVER_DOC_REJECTED_TEMP_ID,strTMData);
                //Send Push Notification;
                //String msg = HANDOVER_DOC_REJ_MSG + ' '+objCL.Appointment_Date__c != null ? Datetime.newInstance(objCL.Appointment_Date__c.year(), objCL.Appointment_Date__c.month(), objCL.Appointment_Date__c.day()).format('dd-MM-yyyy') : null;
                //msg += ' at '+objTMData.appointment_time;
                sendPushNotification(objCL.Booking_Unit__r.Booking__r.Account__c, HANDOVER_DOC_REJ_MSG, HANDOVER_DOC_TITLE, Integer.valueOf(objCL.Booking_Unit__r.Booking__r.Account__r.Party_ID__c), 'handover', '', Integer.valueOf(objCL.Booking_Unit__r.Registration_ID__c), '', IMG_HANDOVER_DOC);
            }

            //Handover Documents - Cancelled
            if( (objCL.RecordTypeId != null && 
                 String.isNotBlank(objCL.Service_Type__c) &&
                 String.isNotBlank(objCL.Sub_Purpose__c) &&
                 String.isNotBlank(objCL.Appointment_Status__c) ) &&
                objCL.RecordTypeId == appointmentSchedulingRecordTypeId && 
                objCL.Service_Type__c.equalsIgnoreCase('Handover') && 
                objCL.Sub_Purpose__c.equalsIgnoreCase('Documentation') &&
                objCL.Appointment_Status__c == 'Cancelled') {
                HD_SendNotificationByEmail_API.TemplateMergeData objTMData = new HD_SendNotificationByEmail_API.TemplateMergeData();
                objTMData.unit = objCL.Booking_Unit__r.Unit_Name__c;  
                objTMData.payment_type = 2;

                String strTMData = JSON.serialize(objTMData);
                // Send Email
                sendEmail(objCL.Booking_Unit__r.Booking__r.Account__r.Party_ID__c, HANDOVER_DOC_CANCELLED_TEMP_ID, strTMData);
            }

            // Key Handover - Approved
            if( (objCL.RecordTypeId != null && 
                 String.isNotBlank(objCL.Service_Type__c) &&
                 String.isNotBlank(objCL.Sub_Purpose__c) &&
                 String.isNotBlank(objCL.Appointment_Status__c) ) &&
                objCL.RecordTypeId == appointmentSchedulingRecordTypeId && 
                objCL.Service_Type__c.equalsIgnoreCase('Handover') && 
                objCL.Sub_Purpose__c.equalsIgnoreCase('Key Handover') && 
                objCL.Appointment_Status__c == 'Confirmed') {
                HD_SendNotificationByEmail_API.TemplateMergeData objTMData = new HD_SendNotificationByEmail_API.TemplateMergeData();
                objTMData.unit = objCL.Booking_Unit__r.Unit_Name__c;  
                objTMData.appointment_date = objCL.Appointment_Date__c != null ? 
                                             HDApp_createHandoverAppointmentsHandler.getFormattedDateWithMonth( Datetime.newInstance(objCL.Appointment_Date__c.year(), objCL.Appointment_Date__c.month(), objCL.Appointment_Date__c.day()).format('yyyy-MM-dd') ) : null;
                objTMData.appointment_time = objCL.Appointment_Slot__c != null ? HDApp_getHandoverCurrentStatusHandler.get12HrsFormattedTime(objCL.Appointment_Slot__c).split('-')[0] : null  ;
                objTMData.payment_type = 2;

                String strTMData = JSON.serialize(objTMData);
                // Send Email
                sendEmail(objCL.Booking_Unit__r.Booking__r.Account__r.Party_ID__c, KEY_HANDOVER_APPROVED_TEMP_ID, strTMData);
                
                // Send Push Notification
                String appointmentDate = (objCL.Appointment_Date__c != null ? 
                                          Datetime.newInstance(objCL.Appointment_Date__c.year(), 
                                                               objCL.Appointment_Date__c.month(), 
                                                               objCL.Appointment_Date__c.day()).format('dd-MM-yyyy') : 
                                          null);
                //String msg = KEY_HANDOVER_APPR_MSG + ' ' + appointmentDate;
                //msg += ' at '+objTMData.appointment_time;
                sendPushNotification(objCL.Booking_Unit__r.Booking__r.Account__c, KEY_HANDOVER_APPR_MSG, BOOK_KEY_HANDOVER_TITLE, 
                                     Integer.valueOf(objCL.Booking_Unit__r.Booking__r.Account__r.Party_ID__c), 
                                     'handover', '', Integer.valueOf(objCL.Booking_Unit__r.Registration_ID__c), '',IMG_KEY_HANDOVER);
            }

            // Key Handover - Rejected
            if( (objCL.RecordTypeId != null && 
                 String.isNotBlank(objCL.Service_Type__c) &&
                 String.isNotBlank(objCL.Sub_Purpose__c) &&
                 String.isNotBlank(objCL.Appointment_Status__c) ) &&
                objCL.RecordTypeId == appointmentSchedulingRecordTypeId && 
                objCL.Service_Type__c.equalsIgnoreCase('Handover') && 
                objCL.Sub_Purpose__c.equalsIgnoreCase('Key Handover') && 
                objCL.Appointment_Status__c == 'Rejected') {
                HD_SendNotificationByEmail_API.TemplateMergeData objTMData = new HD_SendNotificationByEmail_API.TemplateMergeData();
                objTMData.unit = objCL.Booking_Unit__r.Unit_Name__c;  
                objTMData.appointment_rejected_reason = objCL.Appointment_Rejection_Reason__c;
                objTMData.payment_type = 2;

                String strTMData = JSON.serialize(objTMData);
                // Send Email
                sendEmail(objCL.Booking_Unit__r.Booking__r.Account__r.Party_ID__c, KEY_HANDOVER_REJECTED_TEMP_ID, strTMData);
                
                // Send Push Notification
                String appointmentDate = (objCL.Appointment_Date__c != null ? 
                                          Datetime.newInstance(objCL.Appointment_Date__c.year(), 
                                                               objCL.Appointment_Date__c.month(), 
                                                               objCL.Appointment_Date__c.day()).format('dd-MM-yyyy') : 
                                          null);
                //String msg = KEY_HANDOVER_REJ_MSG + ' ' + appointmentDate;
                //msg += ' at '+objTMData.appointment_time;
                sendPushNotification(objCL.Booking_Unit__r.Booking__r.Account__c, KEY_HANDOVER_REJ_MSG, BOOK_KEY_HANDOVER_TITLE, 
                                     Integer.valueOf(objCL.Booking_Unit__r.Booking__r.Account__r.Party_ID__c), 
                                     'handover', '', Integer.valueOf(objCL.Booking_Unit__r.Registration_ID__c), '',IMG_KEY_HANDOVER);
            }

        }//FOR END
    }

    //This method will be called in After insert trigger
    public static void sendHandoverDocumentApprovedEmail(List<Calling_List__c> lstCallingList) {

        System.debug('Inside sendHandoverDocumentApprovedEmail method');
        for(Calling_List__c objCL : [SELECT id,
                                            RecordTypeId,
                                            Service_Type__c,
                                            Sub_Purpose__c,
                                            Appointment_Date__c,
                                            Appointment_Rejection_Reason__c,
                                            Appointment_Status__c,
                                            Booking_Unit__r.Unit_Name__c,
                                            Appointment_Slot__c,
                                            Booking_Unit__r.Booking__r.Account__c,
                                            Booking_Unit__r.Booking__r.Account__r.Party_ID__c,
                                            Booking_Unit__r.Registration_ID__c
                                     FROM Calling_List__c
                                     WHERE Id IN : lstCallingList]) {

            //Handover Documents - Approved
            if( (objCL.RecordTypeId != null && 
                 String.isNotBlank(objCL.Service_Type__c) &&
                 String.isNotBlank(objCL.Sub_Purpose__c) &&
                 String.isNotBlank(objCL.Appointment_Status__c) ) &&
                objCL.RecordTypeId == appointmentSchedulingRecordTypeId && 
                objCL.Service_Type__c.equalsIgnoreCase('Handover') && 
                objCL.Sub_Purpose__c.equalsIgnoreCase('Documentation') &&
                objCL.Appointment_Status__c == 'Confirmed') {

                 HD_SendNotificationByEmail_API.TemplateMergeData objTMData = new HD_SendNotificationByEmail_API.TemplateMergeData();
                //objTMData.customer_name = '';
                objTMData.unit = objCL.Booking_Unit__r.Unit_Name__c;  
                objTMData.appointment_date = objCL.Appointment_Date__c != null ? 
                                             HDApp_createHandoverAppointmentsHandler.getFormattedDateWithMonth( Datetime.newInstance(objCL.Appointment_Date__c.year(), objCL.Appointment_Date__c.month(), objCL.Appointment_Date__c.day()).format('yyyy-MM-dd') ) : null;
                objTMData.appointment_time = objCL.Appointment_Slot__c != null ? HDApp_getHandoverCurrentStatusHandler.get12HrsFormattedTime(objCL.Appointment_Slot__c).split('-')[0] : null  ;
                objTMData.payment_type = 2;

                String strTMData = JSON.serialize(objTMData);
                //Send Email
                sendEmail(objCL.Booking_Unit__r.Booking__r.Account__r.Party_ID__c, HANDOVER_DOC_APPROVED_TEMP_ID,strTMData);
                
                //send Push Notification
                sendPushNotification(objCL.Booking_Unit__r.Booking__r.Account__c, HANDOVER_DOC_APPR_MSG, HANDOVER_DOC_TITLE, Integer.valueOf(objCL.Booking_Unit__r.Booking__r.Account__r.Party_ID__c), 'handover', '', Integer.valueOf(objCL.Booking_Unit__r.Registration_ID__c), '', IMG_HANDOVER_DOC);
            }
        }
    }

    @future(callout=true)
    public static void sendEmail(String partyId, String templateId, String strTemplateMergeData) {

        System.debug('Inside sendEmail method : ' + partyId);
        HD_SendNotificationByEmail_API.TemplateMergeData objTMData = (HD_SendNotificationByEmail_API.TemplateMergeData)JSON.deserialize(strTemplateMergeData, HD_SendNotificationByEmail_API.TemplateMergeData.class);
        //Calling doPost method to send email;
        Boolean isSandbox = [SELECT IsSandbox FROM Organization].IsSandbox;
        System.debug('IsSandbox is: '+isSandbox);

        if(!Test.isRunningTest()) {
            if(isSandbox) {
                HD_SendNotificationByEmail_API.doPost(partyId, templateId, '', '', 'shinu.sony@damacgroup.com', '', objTMData);
            }
            else {
               HD_SendNotificationByEmail_API.doPost(partyId, templateId, '', '', '', '', objTMData); 
            }
            
        }
    }

    @future(callout=true)
    public static void sendPushNotification(String accountId, String message, String title, Integer partyId, String screenType, String receiptId, Integer regId, String paymentPriority, String imageUrl) {

        //String jsonBody = '   {  '  + 
        //'  "email": "'+accountId+'",  '  + 
        //'  "template_id": 0,  '  + 
        //'  "message": "'+message+'",  '  + 
        //'  "title": "'+title+'",  '  + 
        //'  "payload": {  '  + 
        //'      "account_id": "'+accountId+'",  '  + 
        //'      "party_id": '+partyId+',  '  + 
        //'      "screen": "'+screenType+'",  '  + 
        //'      "receipt_id": '+receiptId+',  '  + 
        //'      "reg_id": '+regId+',  '  + 
        //'      "payment_priority": "'+paymentPriority+'"  '  + 
        //'  },  '  +  
        //'  "image_url": "https://services.damacgroup.com/DCAssets/hello-damac/notifications/txn_success.png",  '  + 
        //'  "source_application_id": 9,  '  + 
        //'  "dest_application_id": 2  '  + 
        //'  }  ' ; 

        String jsonBody = '   {  '  + 
        '       "email": "'+accountId+'",  '  + 
        '       "template_id": 0,  '  + 
        '       "message": "'+message+'",  '  + 
        '       "title": "'+title+'",  '  + 
        '       "payload": {  '  + 
        '           "account_id": "'+accountId+'",  '  + 
        '           "party_id": '+partyId+',  '  + 
        '           "screen": "'+screenType+'",  '  + 
        '           "receipt_id": "'+receiptId+'",  '  + 
        '           "reg_id": '+regId+',  '  + 
        '           "payment_priority": "'+paymentPriority+'"  '  + 
        '       },  '  + 
        '       "image_url": "'+imageUrl+'",  '  + 
        '       "source_application_id": 9,  '  + 
        '       "dest_application_id": 2  '  + 
        '  }  ' ; 

        Boolean isSandbox = [SELECT IsSandbox FROM Organization].IsSandbox;
        System.debug('IsSandbox in push notification is: '+isSandbox);


        String endpoint = isSandbox ? push_notification_sandbox_baseurl : push_notification_prod_baseurl;
        endpoint += isSandbox ? push_notification_sandbox_endpoint : push_notification_prod_endpoint;
        System.debug('endpoint: ' + endpoint);

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpoint);
        request.setMethod('POST');
        request.setBody(jsonBody);
        request.setHeader('API-TOKEN',api_token);
        request.setHeader('Content-Type', 'application/json');
        HttpResponse response;
        if(Test.isRunningTest()) {
            response = new HttpResponse();
        } else {
            response = http.send(request);
        }

        System.debug('requestBody:: ' + request.getBody());
        System.debug('response:: ' + response);
        System.debug('response body :: ' + response.getBody());

    }
 
   
}