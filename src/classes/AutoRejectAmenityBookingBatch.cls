/*----------------------------------------------------------------------------
Description: Apex bacth for to reject bookings if fee is not paid before/on due date
========================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------
1.0     | 12-11-2018       | Lochana Rajput   | Reject booking if deposit is not paid
												before due date
==============================================================================================
*/
public class AutoRejectAmenityBookingBatch implements Database.Batchable<sObject> {

	public Database.QueryLocator start(Database.BatchableContext BC) {
		Id devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().
                            get('Amenity Booking').getRecordTypeId();
        String query = 'SELECT Id, Status__c, Resource_Booking_Due_Date__c';
        query += ' FROM FM_Case__c WHERE RecordTypeId =: devRecordTypeId ';
		query += ' AND Resource_Booking_Due_Date__c != NULL ';
		query += ' AND Resource_Booking_Due_Date__c < TODAY ';
		query += ' AND Booking_Date__c != NULL ';
		query += ' AND Booking_Date__c > TODAY ';
		// query += ' AND Resource_Booking_Due_Date__c < Booking_Date__c ';
		query += ' AND Amenity_Booking_Status__c !=\'Booking Confirmed\'' ;
		return Database.getQueryLocator(query);
	}

   	public void execute(Database.BatchableContext BC, List<FM_Case__c> scope) {
		// List<FM_Case__c> lstUpdateCases = new List<FM_Case__c>();
		try {
			for(FM_Case__c obj : scope) {
				obj.Status__c = 'Rejected';
				obj.Amenity_Booking_Status__c = 'Rejected';
			}
			update scope;
		}
		catch(Exception excp) {
			insert new Error_Log__c(Process_Name__c = 'Amenity Booking',
                Error_Details__c = excp.getMessage());
		}
	}

	public void finish(Database.BatchableContext BC) {}
}