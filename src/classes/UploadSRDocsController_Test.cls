/***************************************************************************************
* Name               : UploadSRDocsControllerTest
* Description        : Test class for UploadSRDocsController.
* Created Date       : 31/01/2018
* Created By         : BIGWORKS
* --------------------------------------------------------------------------------------
* VERSION     AUTHOR                 DATE            COMMENTS
* 1.0         BIGWORKS           31/01/2018           Created         
***************************************************************************************/
@isTest 
private class UploadSRDocsController_Test {
    static NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
    static ContentVersion contentVersion_1;
    static Unit_Documents__c srDoc;
    static void init () {
        Account a = new Account();
        a.Name = 'Test Account';
        a.Agency_Short_Name__c = 'testShrName';
        insert a;

        sr = new NSIBPM__Service_Request__c();
            sr.NSIBPM__Customer__c = a.id;
            sr.Eligible_to_Sell_in_Dubai__c = true;
            sr.Agency_Type__c = 'Individual';
            sr.ID_Type__c = 'Passport';
            sr.Agency__c = a.id;
            sr.Booking_Wizard_Level__c = 'Level 4';
            sr.Agency_Email_2__c = 'test2@gmail.com';
            sr.Alternate_Agency_Email_2__c = 'test1@gmail.com';
            sr.Country_of_Sale__c = 'UAE;KSA;Lebanon';
            insert sr;

        srDoc = new Unit_Documents__c ();
            srDoc.Service_Request__c = sr.id;
            srDoc.Document_Type__c = 'SPA Docs';
            srDoc.Status__c = 'Pending Upload';
            srDoc.Is_Required__c = true;
        insert srDoc;
        Unit_Documents__c srDoc1 = new Unit_Documents__c();
            srDoc1.Service_Request__c = sr.id;
            srDoc1.Document_Type__c = 'SPA Docs';
            srDoc1.Status__c = 'Uploaded';
            srDoc1.Is_Required__c = true;
        insert srDoc1;  

        contentVersion_1 = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert contentVersion_1;
    }

    private static testmethod void midOfficeApprovalTest() {
        init ();
        Test.startTest ();
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(sr);
            ApexPages.currentPage().getParameters().put('Id',sr.id);
            sr.Step_2_1_Mid_Office_Approval_Status__c = 'Mid Office Rejected';
            update sr;
            New_Step__c step = new New_Step__c();
            step.Step_Type__c = 'Revised URRF Upload';
            step.Is_Closed__c = FALSE;
            step.Step_Status__c = 'Awaiting Revised URRF';
            step.Step_No__c = 2.1;
            step.Service_Request__c = sr.Id;
            insert step;  
            UploadSRDocsController obj = new UploadSRDocsController ();
            UploadSRDocsController.uploadFile(contentVersion_1.Id, srDoc.Id); 
            UploadSRDocsController.updateSR (sr.Id);
            UploadSRDocsController.getPassportDetails ('data:image/png;base64,TestingFiles==',
                                                         'test.jpg', srDoc.Id, 'passport');
        Test.stopTest ();
    }

    private static testmethod void awaitingManagerStepTest() {
        init ();
        Test.startTest ();
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(sr);
            ApexPages.currentPage().getParameters().put('Id',sr.id);
            NSIBPM__SR_Status__c srStatus = new NSIBPM__SR_Status__c();
            srStatus.Name = 'Awaiting Manager Approval';
            srStatus.NSIBPM__Code__c = 'Awaiting Manager Approval';
            insert srStatus;
            UploadSRDocsController obj = new UploadSRDocsController ();
            UploadSRDocsController.uploadFile(contentVersion_1.Id, srDoc.Id); 
            UploadSRDocsController.updateSR (sr.Id);
            UploadSRDocsController.getPassportDetails ('data:image/png;base64,TestingFiles=',
                                                             'test.jpg', srDoc.Id, 'passport');
        Test.stopTest ();
    }

    private static testmethod void tokenApprovalTest() {
        init ();
        Test.startTest ();
            sr.Step_2_0_Token_Payment_Status__c = 'Token Document Rejected';
            update sr;
            New_Step__c step = new New_Step__c();
            step.Step_Type__c = 'Revised Token Document Upload';
            step.Is_Closed__c = FALSE;
            step.Step_Status__c = 'Awaiting Token Document Upload';
            step.Step_No__c = 2.1;
            step.Service_Request__c = sr.Id;
            insert step;  
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(sr);
            ApexPages.currentPage().getParameters().put('Id', sr.id);         
            UploadSRDocsController obj = new UploadSRDocsController ();
            UploadSRDocsController.uploadFile(contentVersion_1.Id, srDoc.Id); 
            UploadSRDocsController.updateSR (sr.Id);
            UploadSRDocsController.getPassportDetails ('data:image/png;base64,TestingFiles==',
                                                         'test.jpg', srDoc.Id, 'passport');
        Test.stopTest ();
    }
}