/*
* Description - Test class developed for 'TaskCreationWSDL'
*
* Version            Date          Author            Description
* 1.0                16/11/17      Snehil Karn       Initial Draft
*/

@isTest
public class TaskCreationWSDLTest{

	@isTest static void TaskCreationMethod() {
		//// Insert Accont
  //      Account objAcc = TestDataFactory_CRM.createPersonAccount();
  //      objAcc.Address_Line_1__pc = 'Test Address';
  //      objAcc.Party_Id__c = '231421';
  //      insert objAcc ;
        
  //      //Insert Cases for the units
  //      Case caseObj = new Case();
  //      Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
		//Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
		//objCase.Status = 'Submitted';
		//objCase.Type = 'Change of Contact Details';
  //      objCase.Address__c = objAcc.Address_Line_1__pc;
		//objCase.OQOOD_Fee_Applicable__c = true;
		//objCase.OQOOD_Fee_Payment_Mode__c = 'Cash';
  //      objCase.OQOOD_Fee__c = 200.0;
  //      insert objCase;
  //      String caseNumber = [ Select CaseNumber from Case where id =: objCase.Id ].CaseNumber;
  //      Task objTask = TestDataFactory_CRM.createTask( objCase, 'Test Subject', 'Legal', 'COD', System.today().addDays(1) );
  //      insert objTask;
  //      List<Task> taskList = [ Select Id,Status, CreatedDate, WhatId, Subject, ActivityDate from Task where Id =: objTask.Id ];

		Test.startTest();
    	

    	List<TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5> objTaskBean = new List<TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5>();

    	TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 header = ReturnInitiated();
    	header.PARAM_ID = '354534';
    	header.ATTRIBUTE1 = 'HEADER';
    	header.ATTRIBUTE2 = 'Change Of Details';
    	header.ATTRIBUTE3 = 'In Progress';
    	header.ATTRIBUTE4 = 'CRM';
    	header.ATTRIBUTE5 = '12345';
    	header.ATTRIBUTE7 = String.valueOf( System.now().format('dd-MMM-yyyy').toUpperCase());
    	header.ATTRIBUTE10 = '12345';
    	header.ATTRIBUTE11 = '66543518546';

    	objTaskBean.add( header );

    	TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 task = ReturnInitiated();
    	task.PARAM_ID = '2335465';
        task.ATTRIBUTE1 = 'TASK';
        task.ATTRIBUTE2 = 'Task Subject';
        task.ATTRIBUTE3 = 'In Progress';
        task.ATTRIBUTE4 = 'FINANCEEXECUTIVE';
		task.ATTRIBUTE7 = String.valueOf( System.now().format('dd-MMM-yyyy').toUpperCase());
		task.ATTRIBUTE8 = '3654153453';
		//Datetime dt = taskList[0].ActivityDate;
		task.ATTRIBUTE9 =  String.valueOf( System.now().format('dd-MMM-yyyy').toUpperCase());
		task.ATTRIBUTE15 = 'true';
        task.ATTRIBUTE16 = 'Cash';
        task.ATTRIBUTE17 = '1000';

        objTaskBean.add( task );

    	TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 unit = ReturnInitiated();
    	unit.ATTRIBUTE1 = 'UNITS';
        unit.ATTRIBUTE2 = 'Change Of Details';
    	objTaskBean.add( unit );


		SOAPCalloutServiceMock.returnToMe = new Map<String, TaskCreationWSDL.SRDataToIPMSMultipleResponse_element>();

    	TaskCreationWSDL.SRDataToIPMSMultipleResponse_element response = new TaskCreationWSDL.SRDataToIPMSMultipleResponse_element();
		response.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"SR Header Data Created for SR # :2-005057","PARAM_ID":"2-005057"},{"PROC_STATUS":"S","PROC_MESSAGE":"Created SR Task Data for SR # :2-005057 Task :Verify POA Documents","PARAM_ID":"2-005057"},{"PROC_STATUS":"E","PROC_MESSAGE":"Error: Param Id Not Passed...","PARAM_ID":"NULL"}],"message":"[WARNING] Check the PROC_MESSAGE attribute for Actual Error Message(s), Error Message Count = 1","status":"S"}';

		SOAPCalloutServiceMock.returnToMe.put('response_x', response);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
		TaskCreationWSDL.TaskHttpSoap11Endpoint obj = new TaskCreationWSDL.TaskHttpSoap11Endpoint();

		String resp = obj.SRDataToIPMSMultiple('2-'+string.valueOf(System.currentTimeMillis()) , 'CREATE_SR', 'SFDC', objTaskBean);

		Test.stopTest();
        
	}
	
	@isTest static void TaskCreationMethod1() {
		

		Test.startTest();
    

		SOAPCalloutServiceMock.returnToMe = new Map<String, TaskCreationWSDL.SRDataToIPMSResponse_element>();

    	TaskCreationWSDL.SRDataToIPMSResponse_element response = new TaskCreationWSDL.SRDataToIPMSResponse_element();
		response.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"SR Header Data Created for SR # :2-005057","PARAM_ID":"2-005057"},{"PROC_STATUS":"S","PROC_MESSAGE":"Created SR Task Data for SR # :2-005057 Task :Verify POA Documents","PARAM_ID":"2-005057"},{"PROC_STATUS":"E","PROC_MESSAGE":"Error: Param Id Not Passed...","PARAM_ID":"NULL"}],"message":"[WARNING] Check the PROC_MESSAGE attribute for Actual Error Message(s), Error Message Count = 1","status":"S"}';

		SOAPCalloutServiceMock.returnToMe.put('response_x', response);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
		TaskCreationWSDL.TaskHttpSoap11Endpoint obj = new TaskCreationWSDL.TaskHttpSoap11Endpoint();

		TaskCreationWSDLXsd.TaskBean objTaskBean1 = SRDataToIPMSInitiated();
		String resp = obj.SRDataToIPMS(objTaskBean1);
 
		Test.stopTest();
        
	}


	@isTest static void SingleTaskCreationMethod() {
		
		Test.startTest();
    	


		SOAPCalloutServiceMock.returnToMe = new Map<String, TaskCreationWSDL.IPMSToSFDCResponse_element>();

    	TaskCreationWSDL.IPMSToSFDCResponse_element response = new TaskCreationWSDL.IPMSToSFDCResponse_element();
		response.return_x = true;

		SOAPCalloutServiceMock.returnToMe.put('response_x', response);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
		TaskCreationWSDL.TaskHttpSoap11Endpoint obj = new TaskCreationWSDL.TaskHttpSoap11Endpoint();

		Boolean resp = obj.IPMSToSFDC( 'Submitted', 'Comments' , '3548654134' , '6575698790' );

		Test.stopTest();
        
	}
	


	public static TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 ReturnInitiated(){
		TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 ret = new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
		ret.ATTRIBUTE1 = '';
		ret.ATTRIBUTE10 = '';
		ret.ATTRIBUTE11 = '';
		ret.ATTRIBUTE12 = '';
		ret.ATTRIBUTE13 = '';
		ret.ATTRIBUTE14 = '';
		ret.ATTRIBUTE15 = '';
		ret.ATTRIBUTE16 = '';
		ret.ATTRIBUTE17 = '';
		ret.ATTRIBUTE18 = '';
		ret.ATTRIBUTE19 = '';
		ret.ATTRIBUTE2 = '';
		ret.ATTRIBUTE20 = '';
		ret.ATTRIBUTE21 = '';
		ret.ATTRIBUTE22 = '';
		ret.ATTRIBUTE23 = '';
		ret.ATTRIBUTE24 = '';
		ret.ATTRIBUTE25 = '';
		ret.ATTRIBUTE26 = '';
		ret.ATTRIBUTE27 = '';
		ret.ATTRIBUTE28 = '';
		ret.ATTRIBUTE29 = '';
		ret.ATTRIBUTE3 = '';
		ret.ATTRIBUTE30 = '';
		ret.ATTRIBUTE31 = '';
		ret.ATTRIBUTE32 = '';
		ret.ATTRIBUTE33 = '';
		ret.ATTRIBUTE34 = '';
		ret.ATTRIBUTE35 = '';
		ret.ATTRIBUTE36 = '';
		ret.ATTRIBUTE37 = '';
		ret.ATTRIBUTE38 = '';
		ret.ATTRIBUTE39 = '';
		ret.ATTRIBUTE4 = '';
		ret.ATTRIBUTE41 = '';
		ret.ATTRIBUTE42 = '';
		ret.ATTRIBUTE43 = '';
		ret.ATTRIBUTE44 = '';
		ret.ATTRIBUTE45 = '';
		ret.ATTRIBUTE46 = '';
		ret.ATTRIBUTE47 = '';
		ret.ATTRIBUTE48 = '';
		ret.ATTRIBUTE49 = '';
		ret.ATTRIBUTE5 = ''; 
		ret.ATTRIBUTE50 = '';
		ret.ATTRIBUTE6 = '';
		ret.ATTRIBUTE7 = '';
		ret.ATTRIBUTE8 = '';
		ret.ATTRIBUTE9 = '';
		ret.PARAM_ID = '';

		return ret;
	}

	public static TaskCreationWSDLXsd.TaskBean SRDataToIPMSInitiated(){
		TaskCreationWSDLXsd.TaskBean ret = new TaskCreationWSDLXsd.TaskBean();
		ret.ATTRIBUTE1= '';
        ret.ATTRIBUTE10 ='';
		ret.ATTRIBUTE11='';
		ret.ATTRIBUTE12='';
		ret.ATTRIBUTE13='';
		ret.ATTRIBUTE14='';
		ret.ATTRIBUTE15='';
		ret.ATTRIBUTE16='';
		ret.ATTRIBUTE17='';
		ret.ATTRIBUTE18='';
		ret.ATTRIBUTE19='';
		ret.ATTRIBUTE2='';
		ret.ATTRIBUTE20='';
		ret.ATTRIBUTE21='';
		ret.ATTRIBUTE22='';
		ret.ATTRIBUTE23='';
		ret.ATTRIBUTE24='';
		ret.ATTRIBUTE25='';
		ret.ATTRIBUTE26='';
		ret.ATTRIBUTE27='';
		ret.ATTRIBUTE28='';
		ret.ATTRIBUTE29='';
		ret.ATTRIBUTE3='';
		ret.ATTRIBUTE30='';
		ret.ATTRIBUTE31='';
		ret.ATTRIBUTE32='';
		ret.ATTRIBUTE33='';
		ret.ATTRIBUTE34='';
		ret.ATTRIBUTE35='';
		ret.ATTRIBUTE36='';
		ret.ATTRIBUTE37='';
		ret.ATTRIBUTE38='';
		ret.ATTRIBUTE39='';
		ret.ATTRIBUTE4='';
		ret.ATTRIBUTE40='';
		ret.ATTRIBUTE41='';
		ret.ATTRIBUTE42='';
		ret.ATTRIBUTE43='';
		ret.ATTRIBUTE44='';
		ret.ATTRIBUTE45='';
		ret.ATTRIBUTE46='';
		ret.ATTRIBUTE47='';
		ret.ATTRIBUTE48='';
		ret.ATTRIBUTE49='';
		ret.ATTRIBUTE5='';
		ret.ATTRIBUTE50='';
		ret.ATTRIBUTE6='';
		ret.ATTRIBUTE7='';
		ret.ATTRIBUTE8='';
		ret.ATTRIBUTE9='';
		ret.PARAM_ID='';
		ret.p_REQUEST_NAME='';
		ret.p_REQUEST_NUMBER='';
		ret.p_SOURCE_SYSTEM='';
        return ret;
	}
}