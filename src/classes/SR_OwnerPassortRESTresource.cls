@RestResource(urlMapping='/srCase/owner/passport')
global class SR_OwnerPassortRESTresource {
    public static Map<Integer, String> statusCodeMap = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong',
        7 => 'Draft Exists',
        8 => 'Info',
        9 => 'SR Exist'
    };
    
    @HTTPPost
    global static void doPost(Boolean is_draft, String account_id, String draft_sr_id, String document_number, 
    String document_expiry_date_string, String document_issue_place, String passport_file_url, 
    String additional_doc_file_url) {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        RestContext.response.addHeader('Content-Type', 'application/json');
        String errorMsg, successMsg;
        
        if(String.isBlank(account_id)){
            errorMsg = 'Owner Account ID for the service request is not provided.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, errorMsg);
            return;
        }
        
        if(!is_draft && (String.isBlank(document_number) || String.isBlank(document_expiry_date_string) || 
           String.isBlank(document_issue_place))){
            errorMsg = 'Please provide value for all the fields related to passport.';
            getErrorResponse(8, statusCodeMap.get(8), errorMsg, errorMsg);
            return;
        }
        
        Date expiryDate_New = NULL;
        try{
            if(String.isNotBlank(document_expiry_date_string)) {
                expiryDate_New = Date.valueOf(document_expiry_date_string);
            }
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
            errorMsg = 'Please provide a proper date formatted as \'yyyy-mm-dd\'.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, ex.getMessage());
            return;
        }
        
        Account ownerAccount;
        try{
            ownerAccount = [SELECT id, Name, Name_Arabic__c, IsPersonAccount, Party_ID__c, Party_Type__c, 
                 Primary_CRE__c, Secondary_CRE__c, Tertiary_CRE__c, Primary_Language__c, 
                 CR_Number__c, CR_Registration_Expiry_Date__c, CR_Registration_Place__c, 
                 CR_Registration_Place_Arabic__c, Passport_Number__pc, Passport_Expiry_Date__pc, 
                 Passport_Issue_Place__pc, Passport_Issue_Place_Arabic__pc, Passport_File_URL__c 
                 FROM Account WHERE Id = :account_id];
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
            errorMsg = 'The Owner Account ID provided does not match any records.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, ex.getMessage());
            return;
        }
        
        if(NULL == ownerAccount){
            errorMsg = 'The Owner Account ID provided does not match any records.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, errorMsg);
            return;
        }
        
        Boolean isPersonAccount = ownerAccount.IsPersonAccount;
        if(!is_draft && isPersonAccount && String.isBlank(passport_file_url)){
            errorMsg = 'Please provide passport attachment URL to submit request.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, errorMsg);
            return;
        }
        
        Set<String> allowedStatusSet = new Set<String>{'Closed','Cancelled','Rejected'};
        Set<String> set_SR_RecTypes = 
            new Set<String>{'Passport_Detail_Update','Change_of_Details','Name_Nationality_Change','Change_of_Joint_Buyer'};
        Case currentSR_passport;
        Boolean currSR_isNew = false;
        List<Case> srRequests_existing = [SELECT id, caseNumber, Type, SR_Type__c, Origin, isHelloDamacAppCase__c, status, 
                        RecordTypeId, RecordType.Name, RecordType.DeveloperName, AccountId, Account.Name, 
                        Is_Primary_Customer_Updated__c, New_CR__c, Passport_Issue_Date__c, 
                        Passport_Issue_Place__c, Passport_Issue_Place_Arabic__c, OCR_verified__c, 
                        Additional_Doc_File_URL__c, Passport_File_URL__c 
                        FROM Case 
                        WHERE AccountId = :account_id 
                        AND status NOT IN :allowedStatusSet 
                        AND RecordType.DeveloperName IN :set_SR_RecTypes];
        /* AND RecordType.Name = 'Passport Detail Update' AND SR_Type__c = 'Passport Detail Update SR' AND status = 'Draft Request' */
        if(NULL != srRequests_existing && srRequests_existing.size() > 0 ){
            for(Case draftSR : srRequests_existing){
                String draftSR_ID = String.valueOf(draftSR.id);
                if(String.isNotBlank(draft_sr_id) && draftSR_ID.equals(draft_sr_id) && 
                   draftSR.RecordType.Name == 'Passport Detail Update' && 
                   draftSR.SR_Type__c == 'Passport Detail Update SR' && 
                   draftSR.status == 'Draft Request'){
                    currentSR_passport = draftSR;
                    // break;
                }
                else {
                    errorMsg = 'Another Service Request already exist for this account.';
                    Integer statCod = draftSR.status.equals('Draft Request') ? 7 : 9;
                    getErrorResponse(statCod, statusCodeMap.get(statCod), errorMsg, errorMsg);
                    return;
                }
            }
            
            /* Case when provided `draft_sr_id` does not match any records present */
            if(NULL == currentSR_passport){
                if(String.isNotBlank(draft_sr_id)){
                    errorMsg = 'Provided Draft SR ID is invalid.';
                    system.debug(errorMsg);
                    getErrorResponse(3, statusCodeMap.get(3), errorMsg, errorMsg);
                    return;
                }
                else {
                    errorMsg = 'Another Service Request already exist for this account.';
                    getErrorResponse(9, statusCodeMap.get(9), errorMsg, errorMsg);
                    return;
                }
            }
        }
        else {
            /* Case when no previous draft request SR cases are present. */
            if(String.isNotBlank(draft_sr_id)){
                errorMsg = 'Provided Draft SR ID is invalid.';
                getErrorResponse(3, statusCodeMap.get(3), errorMsg, errorMsg);
                return;
            }
            
            currSR_isNew = true;
            currentSR_passport = new Case();
            currentSR_passport.AccountId = account_id;
            currentSR_passport.recordTypeID = Schema.SObjectType.Case.getRecordTypeInfosByName().get(
                                'Passport Detail Update').getRecordTypeId();
            currentSR_passport.type = 'Passport Detail Update SR';
            currentSR_passport.SR_Type__c = 'Passport Detail Update SR';
            currentSR_passport.Origin = 'Portal'; /* Mobile App */
            currentSR_passport.isHelloDamacAppCase__c = true;
        }
        
        currentSR_passport.status = is_draft ? 'Draft Request' : 'Submitted';
        currentSR_passport.New_CR__c = document_number;
        currentSR_passport.Passport_Issue_Date__c = expiryDate_New; /* Date.parse(expiryDateString) */
        currentSR_passport.Passport_Issue_Place__c = document_issue_place;
        if(String.isNotBlank(passport_file_url))
            currentSR_passport.Passport_File_URL__c = passport_file_url;
        if(String.isNotBlank(additional_doc_file_url))
            currentSR_passport.Additional_Doc_File_URL__c = additional_doc_file_url;
        
        setCaseOwner(ownerAccount, currentSR_passport); /* set case owner from account */
        
        try{
            if(currSR_isNew) insert currentSR_passport;
            else update currentSR_passport;
        }
        catch(Exception ex){
            System.debug(ex.getMessage());
            errorMsg = 'Failed to update SR Case.';
            system.debug(errorMsg);
            getErrorResponse(2, statusCodeMap.get(2), errorMsg, ex.getMessage());
        }
        
        Case updatedSR_case; /* to fetch updated caseNumber, owner, and status ..etc */
        try{
            updatedSR_case = [SELECT id, caseNumber, Type, SR_Type__c, Origin, isHelloDamacAppCase__c, 
                        RecordTypeId, RecordType.Name, status, AccountId, Account.Name, 
                        Is_Primary_Customer_Updated__c, New_CR__c, Passport_Issue_Date__c, 
                        Passport_Issue_Place__c, Passport_Issue_Place_Arabic__c, OCR_verified__c, 
                        Additional_Doc_File_URL__c, Passport_File_URL__c 
                        FROM Case WHERE id = :currentSR_passport.id];
        }
        catch(Exception ex){
            System.debug(ex.getMessage());
            successMsg = 'Succesfully updated SR request for Passport Detail Change.';
            getSuccessResponse(ownerAccount, currentSR_passport, successMsg);
        }
        
        successMsg = 'Succesfully updated SR request for Passport Detail Change.';
        if(NULL != updatedSR_case)
            getSuccessResponse(ownerAccount, updatedSR_case, successMsg);
        else
            getSuccessResponse(ownerAccount, currentSR_passport, successMsg);
    }
    
    @HttpGet
    global static void doGet() {
        SR_contextWrap currentSRContext = getContextSR();
        if(NULL == currentSRContext) {
            return; /* error response body already built in getContextSR() */
        }
        
        String message = '';
        if(NULL == currentSRContext.currentSR_Owner || NULL == currentSRContext.currentSR_Case) {
            message = 'Error while fetching Draft SR Case record.';
            getErrorResponse(6, statusCodeMap.get(6), message, NULL);
        }
        else {
            message = 'Succesfully fetched SR Case for Owner Passport Detail Change.';
            getSuccessResponse(currentSRContext.currentSR_Owner, currentSRContext.currentSR_Case, message);
        }
    }
    
    @HttpDelete
    global static void doDeleteDraft() {
        SR_contextWrap currentSRContext = getContextSR();
        if(NULL == currentSRContext) {
            return; /* error response body already built in getContextSR() */
        }
        
        String message = '';
        if(NULL == currentSRContext.currentSR_Owner || NULL == currentSRContext.currentSR_Case) {
            message = 'Error while fetching Draft SR Case record.';
            getErrorResponse(6, statusCodeMap.get(6), message, NULL);
            return;
        }
        
        Case currentSR_passport = currentSRContext.currentSR_Case;
        currentSR_passport.Status = 'Cancelled';
        
        try{
            update currentSR_passport;
            /* delete currentSR_passport; */
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
            message = 'Error while trying to cancel the Draft SR request';
            getErrorResponse(2, statusCodeMap.get(2), message, (ex.getMessage() + ' : ' + ex.getStackTraceString()));
            return;
        }
        
        message = 'Succesfully cancelled the draft SR Case for Owner Passport Detail Change.';
        getSuccessResponse(currentSRContext.currentSR_Owner, currentSR_passport, message);
    }
    
    private static SR_contextWrap getContextSR() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        RestContext.response.addHeader('Content-Type', 'application/json');
        String errorMsg, successMsg;
        
        String caseNum = req.params.containskey('case_number') ? req.params.get('case_number') : '';
        if(String.isBlank(caseNum)){
            errorMsg = 'Please provide case_number.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, errorMsg);
            return NULL;
        }
        
        String ownerAccountId = req.params.containskey('account_id') ? req.params.get('account_id') : '';
        if(String.isBlank(ownerAccountId)){
            errorMsg = 'Please provide account_id.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, errorMsg);
            return NULL;
        }
        
        Account ownerAccount;
        try{
            ownerAccount = [SELECT id, Name, Name_Arabic__c, IsPersonAccount, Party_ID__c, Party_Type__c, 
                 CR_Number__c, CR_Registration_Expiry_Date__c, CR_Registration_Place__c, 
                 CR_Registration_Place_Arabic__c, Passport_Number__pc, Passport_Expiry_Date__pc, 
                 Passport_Issue_Place__pc, Passport_Issue_Place_Arabic__pc, Passport_File_URL__c 
                 FROM Account WHERE Id = :ownerAccountId];
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
            errorMsg = 'The account_id provided does not match any records.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, ex.getMessage());
            return NULL;
        }
        
        if(NULL == ownerAccount){
            errorMsg = 'The account_id provided does not match any records.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, errorMsg);
            return NULL;
        }
        
        Case currentSR_passport;
        try{
            currentSR_passport = [SELECT id, caseNumber, Type, SR_Type__c, Origin, isHelloDamacAppCase__c, 
                        RecordTypeId, RecordType.Name, status, AccountId, Account.Name, 
                        Is_Primary_Customer_Updated__c, New_CR__c, Passport_Issue_Date__c, 
                        Passport_Issue_Place__c, Passport_Issue_Place_Arabic__c, OCR_verified__c, 
                        Additional_Doc_File_URL__c, Passport_File_URL__c 
                        FROM Case 
                        WHERE AccountId = :ownerAccountId 
                        AND RecordType.Name = 'Passport Detail Update' 
                        AND SR_Type__c = 'Passport Detail Update SR' 
                        AND status = 'Draft Request' 
                        AND caseNumber = :caseNum LIMIT 1];
        }
        catch(Exception ex){
            System.debug(ex.getMessage());
            errorMsg = 'No Matching draft SR records available for provided parameters.';
            getErrorResponse(2, statusCodeMap.get(2), errorMsg, ex.getMessage());
            return NULL;
        }
        if(NULL == currentSR_passport){
            errorMsg = 'The Case Number provided does not match any draft SR records.';
            getErrorResponse(2, statusCodeMap.get(2), errorMsg, errorMsg);
            return NULL;
        }
        
        SR_contextWrap currentSRContext = new SR_contextWrap();
        currentSRContext.currentSR_Owner = ownerAccount;
        currentSRContext.currentSR_Case = currentSR_passport;
        return currentSRContext;
    }
    
    private static void setCaseOwner(Account ownerAccount, Case sr_caseRecord) {
        if (NULL != ownerAccount.Primary_CRE__c) {
            sr_caseRecord.OwnerId = ownerAccount.Primary_CRE__c;
        } else if (NULL != ownerAccount.Secondary_CRE__c) {
            sr_caseRecord.OwnerId = ownerAccount.Secondary_CRE__c;
        } else if (NULL != ownerAccount.Tertiary_CRE__c) {
            sr_caseRecord.OwnerId = ownerAccount.Tertiary_CRE__c;
        } else {
            String queueName = (String.isNotBlank(ownerAccount.Primary_Language__c) && 
                ownerAccount.Primary_Language__c.equalsIgnoreCase('Arabic')) ? 
                'Non Elite Arabs Queue' : 
              'Non Elite Non Arabs Queue';
            
            QueueSobject caseOwnQueue;
            try{
              caseOwnQueue = [SELECT Id, Queue.Name, QueueId FROM QueueSobject 
                                        WHERE SobjectType = 'Case' 
                                        AND Queue.Name = :queueName LIMIT 1];
            }
            catch(Exception ex){
              system.debug(ex.getMessage());
                return;
            }
            
            if(NULL != caseOwnQueue) sr_caseRecord.OwnerId = caseOwnQueue.QueueId;
        }
    }
    
    private static void getSuccessResponse(Account currentSR_Owner, 
    Case currentSR_passport, String successMsg){
        ResponseWrapper responseWrapper = new ResponseWrapper();
        
        cls_meta_data responseMetaData = new cls_meta_data();
        responseMetaData.status_code = 1;
        responseMetaData.title = statusCodeMap.get(1);
        responseMetaData.message = successMsg;
        responseMetaData.developer_message = successMsg;
        responseWrapper.meta_data = responseMetaData;
        
        cls_data responseData = new cls_data();
        responseData.sr_case_id = currentSR_passport.id;
        responseData.sr_case_number = currentSR_passport.caseNumber;
        responseData.sr_case_status = currentSR_passport.status == 'Working' ? 'Submitted' : currentSR_passport.status; /*as per bug 2851*/
        responseData.sr_case_submission_date = String.valueOf(Date.today());
        //responseData.sr_request_type = currentSR_passport.SR_Type__c;
        responseData.sr_request_type = 'Update passport details';
        responseData.sr_origin = currentSR_passport.Origin;
        responseData.sr_account_owner = currentSR_Owner.Name;
        responseData.sr_account_is_person = currentSR_Owner.IsPersonAccount;
        responseData.sr_document_number = currentSR_passport.New_CR__c;
        responseData.sr_document_expiry_date = String.valueOf(currentSR_passport.Passport_Issue_Date__c);
        responseData.sr_document_issue_place = currentSR_passport.Passport_Issue_Place__c;
        responseData.sr_passport_file_url = currentSR_passport.Passport_File_URL__c;
        responseData.sr_additional_doc_file_url = currentSR_passport.Additional_Doc_File_URL__c;
        responseWrapper.data = responseData;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
    }
    
    private static void getErrorResponse(Integer statusCode, String title, String responseMessage, String devMessage) {
        ResponseWrapper responseWrapper = new ResponseWrapper();
        cls_meta_data responseMetaData = new cls_meta_data();
        responseMetaData.status_code = statusCode;
        responseMetaData.title = title;
        responseMetaData.message = responseMessage;
        responseMetaData.developer_message = devMessage;
        responseWrapper.meta_data = responseMetaData;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
    }
    
    private class SR_contextWrap {
        Account currentSR_Owner;
        Case currentSR_Case;
    }

    /* Wrapper classes for returning reponse */
    public class ResponseWrapper {
        public cls_meta_data meta_data;
        public cls_data data;
    }

    public class cls_meta_data {
        public Integer status_code;
        public String message;
        public String title;
        public String developer_message;   
    }

    public class cls_data {
        public String sr_case_id;
        public String sr_case_number;
        public String sr_case_status;
        public String sr_case_submission_date;
        public String sr_request_type;
        public String sr_origin;
        public String sr_account_owner;
        public Boolean sr_account_is_person;
        public String sr_document_number;
        public String sr_document_expiry_date;
        public String sr_document_issue_place;
        public String sr_passport_file_url;
        public String sr_additional_doc_file_url;
    }
}