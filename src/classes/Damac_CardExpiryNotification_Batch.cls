/*
Created Date: 15/06/2020
Schedule Class: Damac_CardExpiryNotificationSch_Acc_Corp (If Account Agency Type is Corporate), 
Damac_CardExpiryNotificationSch_Acc_Indv (If Account Agency Type is Individual)
Test Class: Damac_CardExpiryNotificationBatchTest
Description: Retrieve the records based on specified fields of a sObject and whose expiry date is in next 30 days and send mail
*/
global class Damac_CardExpiryNotification_Batch implements Database.Batchable<sObject>, Database.stateful {
    
    List<String> accExpiryFields = new List<String>();
    List<String> conExpiryFields = new List<String>();
    String accFieldsString = '';
    String conFieldsString = '';
    String emailField = '';
    List<String> customLabelList = System.Label.CardExpiryNotification.split(',');
    
    public Damac_CardExpiryNotification_Batch(List<String> accFields, List<String> conFields, String emailId) 
    {       
        accExpiryFields = accFields;
        conExpiryFields = conFields;
        emailField = emailId;
        for(String str : accFields) {
            accFieldsString += str + ', ';
        }
        accFieldsString = accFieldsString.removeEnd(', ');
        for(String str : conFields) {
            conFieldsString += str + ', ';
        }
        conFieldsString = conFieldsString.removeEnd(', ');
    }
    
    //Getting the records of Account and related Contacts which has expiry date (for some fields) after 30 days from today
    global Database.QueryLocator start(Database.BatchableContext bc) {
        Date afterThirtyDays = Date.today().addDays(+30);
        String query = 'SELECT Name, Owner.Name, ' + emailField + ', ' + accFieldsString
            + ', (SELECT Name, ' + conFieldsString + ' FROM Contacts WHERE User_Type__c IN: customLabelList AND (';
        
        for(String str : conExpiryFields) {
            query += str + ' =: afterThirtyDays OR ';
        }
        query = query.removeEnd(' OR ');
        query += ')) FROM Account WHERE ';
        for(String str : accExpiryFields) {
            query += str + ' =: afterThirtyDays OR ';
        }
        query = query.removeEnd(' OR ');
        
         if(Test.isRunningTest()) {
            query = 'SELECT Name, Owner.Name, ' + emailField + ', ' + accFieldsString
            + ', (SELECT Name, ' + conFieldsString + ' FROM Contacts) FROM Account LIMIT 1'; 
        }
        return Database.getQueryLocator(query);
    }
    
    //Creating the body of email for each Account and its related contact and passing it as parameters to the 
    //sendMailToUsers() method and in that mail is sent
    global void execute(Database.BatchableContext bc, List<Account> recList) {
        Date currentDate = Date.today();
        Integer daysForExpiry = 0;
        
        for(Account acc : recList) {
            String subject =  'Documents to be updated for ' + acc.Name;
            String body = 'Dear, <b>'+acc.Owner.Name+ '</b> <br> <br>';
            body = body + 'Please be informed that the following documents have expired for <b>'+acc.Name+'</b> \n <br> <br>';
            body = body + '<b style="font-size: 20px;"> Company Documents </b> \n <br>';
            body = body + '<table border="1" style="border-collapse: collapse; width:80%">';
            body = body + '<thead> <tr style="width:80%"> <th style="width:40%">Document Name</th> <th style="width:40%">Expiry Date</th> </tr> </thead> <tbody>';
            for(String str : accExpiryFields) {
                if(acc.get(str) != null) {
                    daysForExpiry = currentDate.daysBetween(Date.valueOf(acc.get(str)));
                    if(daysForExpiry == 30) {
                        body = body + '<tr style="width:80%"> <td style="width:40%; text-align:center;">' + getLabelFromAPIName(str, 'Account') + '</td> <td style="width:40%; text-align:center;">' + acc.get(str) + '</td> </tr>';
                    }
                }
            }
            body = body + ' </tbody> </table> <br>';
            if(acc.Contacts.size() > 0) {
                body = body + '<b style="font-size: 20px;"> Personal Documents </b> \n <br>';
                body = body + '<table border="1" style="border-collapse: collapse; width:80%">'; 
                body = body + '<thead> <tr style="width:80%"> <th style="width:40%">Contact Name</th> <th style="width:40%">Document Name</th> <th style="width:40%">Expiry Date</th> </tr> </thead> <tbody>';
                for(Contact con : acc.Contacts) {
                    for(String str : conExpiryFields) {
                        if(con.get(str) != null) {
                            daysForExpiry = currentDate.daysBetween(Date.valueOf(con.get(str)));
                            if(daysForExpiry == 30) {
                                body = body + '<tr style="width:80%"> <td style="width:40%; text-align:center;">' + con.Name + '</td> <td style="width:40%; text-align:center;">' + getLabelFromAPIName(str, 'Contact') + '</td> <td style="width:40%; text-align:center;">' + con.get(str) + '</td> </tr>';                            
                            }
                        }
                    }
                }
            }
            
            body = body + '</tbody> </table> <br>';
            body = body + '<a href="https://damacholding--fullcopy.my.salesforce.com/' + acc.Id 
                + '">Click to view Agency Account</a> <br> <br> Regards';
            sendMailToUsers(subject, body, String.valueOf(acc.get(emailField)));
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        
    }
    
    //To get the field Label based on field API Name
    public String getLabelFromAPIName(String apiName, String objectType) {
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(objectType);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
        return fieldMap.get(apiName).getDescribe().getLabel();
    }
    
    //Mail sending
    public void sendMailToUsers(String emailSubject, String htmBody, String email) {
        List<Messaging.SingleEmailMessage> mailsToBeSent = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage sem = new Messaging.SingleEmailMessage();
        List<String> sendTo = new List<String>();
        sendTo.add(email);
        sem.setToAddresses(sendTo);
        sem.setSubject(emailSubject);
        sem.setHtmlBody(htmBody);
        mailsToBeSent.add(sem);
        Messaging.sendEmail(mailsToBeSent);
    }    
}