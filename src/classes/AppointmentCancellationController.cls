public class AppointmentCancellationController {
            public list<AppointmentWrapper> lstAppointmentWrapper {get;set;}
            Id devRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Appointment Scheduling').getRecordTypeId();
            public AppointmentCancellationController(ApexPages.standardController controller){      
                init();
            }//End constructor
            
            public pagereference init(){ 
                string strPageID = ApexPages.currentPage().getParameters().get('id');
                lstAppointmentWrapper = new list<AppointmentWrapper>();
                for (Calling_List__c objCalling : [Select Id, Name, Booking_Unit_Name__c, RecordTypeId, Account__c, Appointment_Date__c,
                                                  Appointment_Status__c, Appointment__c, Customer_Name__c, Booking_Unit__c, 
                                                  Booking_Unit__r.Registration_ID__c, Account__r.Email__pc, Account__r.Email__c,
                                                  Account__r.isPersonAccount, Account__r.Name, Account__r.Primary_Language__c,
                                                  ownerId, owner.Email, Calling_List__c, Service_Type__c, Sub_Purpose__c, Appointment_Slot__c,
                                                  Handover_Team_Email__c, HO_Calling_list_Owner__c, CRE_Email__c, Account_Email__c,
                                                  (Select id,StartDateTime,EndDateTime,Subject,status__c,description,OwnerId 
                                                   from Events)
                                              From Calling_List__c 
                                              Where RecordTypeId =: devRecordTypeId 
                                              And Calling_List__c =: strPageID 
                                              And Appointment_Status__c != 'Rejected' 
                                              And Appointment_Status__c != 'Cancelled']) {
                      AppointmentWrapper objWrap = new AppointmentWrapper();
                      objWrap.objCL = objCalling;
                      lstAppointmentWrapper.add(objWrap);              
                }
                return null;
            }
            
            public pageReference cancelAppointment() {
                system.debug('!!!!!inside cancel');
                //system.assert(false,'lstAppointmentWrapper ::::::: '+lstAppointmentWrapper);
                list<Calling_List__c> lstCL = new list<Calling_List__c>();
                list<Event> lstEvent = new list<Event>();
                 for (AppointmentWrapper objWrap : lstAppointmentWrapper) {
                     system.debug('!1objWrap.isSelected'+objWrap.isSelected);
                     if (objWrap.isSelected == true) {
                         Calling_List__c CLtoCanel = new Calling_List__c ();
                         CLtoCanel.Id = objWrap.objCL.Id;
                         CLtoCanel.Appointment_Status__c = 'Cancelled';
                         lstCL.add(CLtoCanel);
                        system.debug('!!!objWrap.objCL.Events'+objWrap.objCL.Events.size());
                        if(objWrap.objCL.Events.Size()>0){
                            system.debug('!!!!!!inside if of size');
                            objWrap.objCL.Events[0].status__c = 'Cancelled';
                            lstEvent.add(objWrap.objCL.Events[0]);    
                        }                   
                     }         
                 }
                 
                 if (lstCL.size() > 0) {
                     update lstCL;
                 }
                 if (lstEvent.size() > 0) {
                     update lstEvent;
                 }
                 
                 if (lstCL != null) {
                    sendEmail(lstCL);
                 }
                 init();
                return null;
            }
            public class AppointmentWrapper {
                public Calling_List__c objCL {get;set;}
                public Boolean isSelected{get;set;}
                public AppointmentWrapper(){
                    isSelected = false;
                }
            }
            
            public void sendEmail(List<Calling_List__c> lstAppointment) {
                List<Messaging.SingleEmailMessage> messages =   new List<Messaging.SingleEmailMessage>();
                EmailTemplate reqEmailTemplate =  [SELECT ID, Subject, Body, HtmlValue FROM EmailTemplate WHERE DeveloperName = 'Appointment_Notifications_to_Customer' limit 1];
                OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'no-replysf@damacgroup.com'];
                for (Calling_List__c objCL : [Select Id, Name, Customer_Name__c, Service_Type__c, Appointment_Date__c, Appointment_Slot__c,
                                                    Handover_Team_Email__c, HO_Calling_list_Owner__c, CRE_Email__c, Account_Email__c,
                                                    Appointment_Status__c, Sub_Purpose__c, Booking_Unit_Name__c
                                              From Calling_List__c  Where Id IN: lstAppointment]) {
                    system.debug('!!!!!!!!!!!!objCL'+objCL);
                    Messaging.SingleEmailMessage mail = 
                                    new Messaging.SingleEmailMessage();
                    mail.setWhatId(objCL.Id);
                    mail.setSaveAsActivity(true); 
                    list<String> bccAddress = new list<String>();
                    List<String> sendTo = new List<String>();
                    String body, sub;
                    if (reqEmailTemplate != null) {
                        body = reqEmailTemplate.HtmlValue;
                        sub = reqEmailTemplate.Subject;
                        if (objCL.Customer_Name__c!= null) {
                            body = body.replace('{!Calling_List__c.Customer_Name__c}', objCL.Customer_Name__c);
                        } else {
                            body = body.replace('{!Calling_List__c.Customer_Name__c}', '');
                        }
                         if (objCL.Sub_Purpose__c!= null) {
                            body = body.replace('{!Calling_List__c.Sub_Purpose__c}', objCL.Sub_Purpose__c);
                        } else {
                            body = body.replace('{!Calling_List__c.Sub_Purpose__c}', '');
                        }
                        if (objCL.Appointment_Status__c != null) {
                            body = body.replace('{!Calling_List__c.Appointment_Status__c}', objCL.Appointment_Status__c);
                        } else {
                            body = body.replace('{!Calling_List__c.Appointment_Status__c}', '');
                        }
                        if (objCL.Service_Type__c != null) {
                            body = body.replace('{!Calling_List__c.Service_Type__c}', objCL.Service_Type__c);
                        } else {
                            body = body.replace('{!Calling_List__c.Service_Type__c}', '');
                        }
                        if (objCL.Booking_Unit_Name__c!= null) {
                            body = body.replace('{!Calling_List__c.Booking_Unit_Name__c}', objCL.Booking_Unit_Name__c);
                        } else {
                            body = body.replace('{!Calling_List__c.Booking_Unit_Name__c}', '');
                        }
                        if (objCL.Appointment_Date__c != null) {
                            body = body.replace('{!Calling_List__c.Appointment_Date__c}', String.valueOf(objCL.Appointment_Date__c));
                        } /*else {
                            body = body.replace('{!Calling_List__c.Appointment_Date__c}', null);
                        }*/
                        if (objCL.Appointment_Slot__c != null) {
                            body = body.replace('{!Calling_List__c.Appointment_Slot__c}', objCL.Appointment_Slot__c);
                        } else {
                            body = body.replace('{!Calling_List__c.Appointment_Slot__c}', '');
                        }
                        /*if (objCL.Service_Type__c != null) {
                            mail.subject = reqEmailTemplate.Subject.replace('{!Calling_List__c.Service_Type__c}', objCL.Service_Type__c);
                        } else {
                            mail.subject = reqEmailTemplate.Subject.replace('{!Calling_List__c.Service_Type__c}', '');
                        }
                        if (objCL.Appointment_Status__c != null) {
                            mail.subject = reqEmailTemplate.Subject.replace('{!Calling_List__c.Appointment_Status__c}', objCL.Appointment_Status__c);
                        } else {
                            mail.subject = reqEmailTemplate.Subject.replace('{!Calling_List__c.Appointment_Status__c}', '');
                        }*/
                        body = body.replace(']]>', '');
                        if (objCL.Service_Type__c != null) {
                            sub = sub.replace('{!Calling_List__c.Service_Type__c}', objCL.Service_Type__c);
                        } else {
                            sub = sub.replace('{!Calling_List__c.Service_Type__c}', '');
                        }
                        if (objCL.Appointment_Status__c != null) {
                            sub = sub.replace('{!Calling_List__c.Appointment_Status__c}', objCL.Appointment_Status__c);
                        } else {
                            sub = sub.replace('{!Calling_List__c.Appointment_Status__c}', '');
                        }
                        mail.setHtmlBody(body);
                        mail.setSubject(sub);
                    }
                    if ((objCL.Sub_Purpose__c == 'Key Handover' || objCL.Sub_Purpose__c == 'Unit Viewing' || objCL.Sub_Purpose__c == 'Unit Viewing or Key Handover' ) &&
                        objCL.Handover_Team_Email__c != null) {
                        bccAddress.add(objCL.Handover_Team_Email__c);
                    }
                    if (objCL.HO_Calling_list_Owner__c != null) {
                        bccAddress.add(objCL.HO_Calling_list_Owner__c);
                    }
                    if (objCL.CRE_Email__c != null) {
                        bccAddress.add(objCL.CRE_Email__c);
                    }
                    if (bccAddress != null) {
                        mail.setBccAddresses(bccAddress);
                    }
                    sendTo.add(objCL.Account_Email__c);
                    mail.setToAddresses(sendTo);            
                    messages.add(mail);
                    
                    if ( owea.size() > 0 ) {
                        mail.setOrgWideEmailAddressId(owea.get(0).Id);
                    }
                }
                
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
            }
    
    }