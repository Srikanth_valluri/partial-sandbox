/*
* Description - Test class developed for Schedule EscalateSocialMediaBatchScheduler
*/
@isTest
public class EscalateSocialMediaBatchSchedulerTest {
      static testMethod void testExecute() {
        Test.startTest();
            EscalateSocialMediaBatchScheduler objScheduler = new EscalateSocialMediaBatchScheduler();
            String schCron = '0 30 8 1/1 * ? *';
            String jobId = System.Schedule('Reccord Updated',schCron,objScheduler);
        Test.stopTest();
        
    }
}