@isTest
public class ProposeValueController_TC {
     static testMethod void testMethod1() {
        Unit_Assignment__c us = new Unit_Assignment__c();
            us.Deal_Value__c = 15;
            us.Reason_For_Unit_Assignment__c = 'test';
            us.Start_Date__c = System.today();
            us.Proposal_Price__c = 14;
            us.Unit_Assignment_Name__c = 'test';
         insert us;
        Inventory__c i = new Inventory__c();
            i.Floor__c = 'test';
            i.Unit_Type__c = '20';
            i.Unit_Assignment__c = us.Id;
         insert i;
         
        us.Deal_Value__c = 16;
        us.Proposal_Price__c = 10;
        update us;
         
    ApexPages.CurrentPage().getparameters().put('id', us.id);      
        
        Apexpages.StandardController sc = new Apexpages.StandardController(us);
        ProposeValueController obj = new ProposeValueController(sc); 
        ProposeValueController.InventoryWrapper  objj = new ProposeValueController.InventoryWrapper (i);
        objj.isSelected = true;
        objj.invObj = i;
        obj.inventoryWrapperList = new List<ProposeValueController.InventoryWrapper> {objj};  
        obj.updateInventories();     
    }
}