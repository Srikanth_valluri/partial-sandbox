//
//Json Object class to parse the SMS response and get the Bulk SMS ID
//

public class DAMAC_SMS_Get_BulkId{
    public String Status;   //OK
    public cls_Data Data;
    public class cls_Data {
        public string BulkId;  //3032693
    }
    public static DAMAC_SMS_Get_BulkId parse(String json){
        return (DAMAC_SMS_Get_BulkId) System.JSON.deserialize(json, DAMAC_SMS_Get_BulkId.class);
    }

    
}