@RestResource(urlMapping='/srCase/options')
global class SR_ListOptionsRESTresource {
    public static Map<Integer, String> statusCodeMap;
    static {
        statusCodeMap = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'};
    }
    
    @HttpGet
    global static void doGet() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        RestContext.response.addHeader('Content-Type', 'application/json');
        String errorMsg, exceptionMsg, successMsg;
        
        String ownerAccountId = req.params.containskey('account_id') ? req.params.get('account_id') : '';
        if(String.isBlank(ownerAccountId)){
            errorMsg = 'Please provide account_id.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
            return;
        }
        
        Account accountRecord;
        try{
            accountRecord = [SELECT id, Name, Name_Arabic__c, IsPersonAccount, Party_ID__c, Party_Type__c, 
                 Primary_CRE__c, Secondary_CRE__c, Tertiary_CRE__c, Primary_Language__c, 
                 Country__c, Country_Arabic__c, Address_Line_1__c, Address_Line_1_Arabic__c, 
                 Address_Line_2__c, Address_Line_2_Arabic__c, Address_Line_3__c, Address_Line_3_Arabic__c, 
                 Address_Line_4__c, Address_Line_4_Arabic__c, City__c, City_Arabic__c, State__c, 
                 State_Arabic__c, Postal_Code_Arabic__c, Email__c, Mobile_Country_Code__c, 
                 Mobile_Phone_Encrypt__c, Country__pc, Country_Arabic__pc, Address_Line_1__pc, 
                 Address_Line_2__pc, Address_Line_3__pc, Address_Line_4__pc, Address_Line_1_Arabic__pc, 
                 Address_Line_2_Arabic__pc, Address_Line_3_Arabic__pc, Address_Line_4_Arabic__pc, City__pc, 
                 City_Arabic__pc, BillingPostalCode, Email__pc, Mobile_Country_Code__pc, Mobile_Phone_Encrypt__pc 
                 FROM Account WHERE Id = :ownerAccountId];
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
        	exceptionMsg = ex.getMessage();
        }
        if(NULL == accountRecord){
            errorMsg = 'The account_id provided does not match any records.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
            return;
        }
        
        User currentUser;
        try{
            currentUser = [SELECT id, name, Username, FullPhotoUrl, Profile.Name, Contact.AccountId 
                            FROM User WHERE Contact.AccountId = :ownerAccountId LIMIT 1];
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
        	exceptionMsg = ex.getMessage();
        }
        if(NULL == currentUser){
            errorMsg = 'Failed to fetch corresponding `Portal User` record for the account_id provided.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
            return;
        }
        
        Boolean isOwner = false;
        Boolean isTenant = false;
        String userProfile = currentUser.Profile.Name;
        if(userProfile.containsIgnoreCase('Customer Community Login User')) isOwner = true;
        if(userProfile.containsIgnoreCase('Tenant Community Login User'))  isTenant = true;
        
        if(!isOwner && !isTenant){
            errorMsg = 'User profile for the given account_id qualify neither as `Owner` nor as `Tenant`.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
            return;
        }
        
        if(isOwner){
            List<Case> srDraftCases_existing;
            try{
                srDraftCases_existing = [SELECT id, caseNumber, Type, SR_Type__c, Origin, 
                        RecordTypeId, RecordType.Name, status, AccountId, Account.Name, 
                        Is_Primary_Customer_Updated__c, Additional_Doc_File_URL__c, 
                        Passport_File_URL__c, CRF_File_URL__c 
                        FROM Case 
                        WHERE AccountId = :ownerAccountId 
                        AND RecordType.Name IN ('Passport Detail Update', 'Change of Details') 
                        AND SR_Type__c IN ('Passport Detail Update SR', 'Change of Contact Details')
                        AND status = 'Draft Request'];
            }
            catch(Exception ex){
                system.debug(ex.getMessage());
                srDraftCases_existing = new List<Case>();
            } 
            
            buildOwnerOptionsList(srDraftCases_existing);
        }
        else if(isTenant){
            List<FM_Case__c> srDraftCases_existing;
            try{
                srDraftCases_existing = [SELECT id, name, Type__c, Request_Type__c, Origin__c, 
                        RecordTypeId, RecordType.Name, Status__c, Account__c, Account__r.Name, 
                        Is_Primary_Customer_Updated__c, Additional_Doc_File_URL__c, 
                        Passport_File_URL__c, CRF_File_URL__c 
                        FROM FM_Case__c 
                        WHERE Account__c = :ownerAccountId 
                        AND RecordType.Name IN ('Passport Detail Update') 
                        AND Request_Type__c IN ('Passport Detail Update')
                        AND Status__c = 'Draft Request'];
            }
            catch(Exception ex){
                system.debug(ex.getMessage());
                srDraftCases_existing = new List<FM_Case__c>();
            } 
            
            buildTenantOptionsList(srDraftCases_existing);
        }
    }
    
    private static void buildOwnerOptionsList(List<Case> srDraftCases){
        Case passportSR, cocdSR;
        for(Case srDraft : srDraftCases){
            if(srDraft.RecordType.Name.equals('Change of Details') && 
               srDraft.SR_Type__c.equals('Change of Contact Details')){
                cocdSR = srDraft;
            }
            else if(srDraft.RecordType.Name.equals('Passport Detail Update') && 
                    srDraft.SR_Type__c.equals('Passport Detail Update SR')){
                passportSR = srDraft;
            }
        }
        
        List<cls_data_list_item> responseOptionsList = new List<cls_data_list_item>();
        
        /* Option 1: 'Update Passport Details' */
        cls_data_list_item ownerOption1 = new cls_data_list_item();
        ownerOption1.unique_id = '1';
        ownerOption1.sr_name = 'Update Passport Details';
        ownerOption1.is_draft = (NULL != passportSR && String.isNotBlank(passportSR.CaseNumber));
        ownerOption1.draft_case_no = (NULL != passportSR && String.isNotBlank(passportSR.CaseNumber) ? 
                                     passportSR.CaseNumber : null);
        responseOptionsList.add(ownerOption1);
        
        /* Option 2: 'Update Primary Contact Details' */
        cls_data_list_item ownerOption2 = new cls_data_list_item();
        ownerOption2.unique_id = '2';
        ownerOption2.sr_name = 'Update Primary Contact Details';
        ownerOption2.is_draft = (NULL != cocdSR && String.isNotBlank(cocdSR.CaseNumber));
        ownerOption2.draft_case_no = (NULL != cocdSR && String.isNotBlank(cocdSR.CaseNumber) ? 
                                     cocdSR.CaseNumber : null);
        responseOptionsList.add(ownerOption2);
        
        // TODO add Owner specific options
        
        cls_data_list_wrapper responseOptionsListWrapper = new cls_data_list_wrapper();
        responseOptionsListWrapper.service_requests = responseOptionsList;
        
        cls_meta_data responseMetaData = new cls_meta_data();
        responseMetaData.status_code = 1;
        responseMetaData.title = statusCodeMap.get(1);
        responseMetaData.message = 'Success in fetching SR option list for Owner';
        responseMetaData.developer_message = null;
        
        ResponseWrapper responseWrapper = new ResponseWrapper();
        responseWrapper.meta_data = responseMetaData;
        responseWrapper.data = responseOptionsListWrapper;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
    }
    
    private static void buildTenantOptionsList(List<FM_Case__c> srDraftCases){
        FM_Case__c passportSR;
        for(FM_Case__c srDraft : srDraftCases){
            if(srDraft.RecordType.Name.equals('Passport Detail Update') && 
                    srDraft.Request_Type__c.equals('Passport Detail Update')){
                passportSR = srDraft;
            }
        }
        
        List<cls_data_list_item> responseOptionsList = new List<cls_data_list_item>();
        
        /* Option 1: 'Update Passport Details' */
        cls_data_list_item ownerOption1 = new cls_data_list_item();
        ownerOption1.unique_id = '1';
        ownerOption1.sr_name = 'Update Passport Details';
        ownerOption1.is_draft = (NULL != passportSR && String.isNotBlank(passportSR.name));
        ownerOption1.draft_case_no = (NULL != passportSR && String.isNotBlank(passportSR.name) ? 
                                     passportSR.name : null);
        responseOptionsList.add(ownerOption1);
        
        // TODO add tenant specific options
        
        cls_data_list_wrapper responseOptionsListWrapper = new cls_data_list_wrapper();
        responseOptionsListWrapper.service_requests = responseOptionsList;
        
        cls_meta_data responseMetaData = new cls_meta_data();
        responseMetaData.status_code = 1;
        responseMetaData.title = statusCodeMap.get(1);
        responseMetaData.message = 'Success in fetching SR option list for Tenant';
        responseMetaData.developer_message = null;
        
        ResponseWrapper responseWrapper = new ResponseWrapper();
        responseWrapper.meta_data = responseMetaData;
        responseWrapper.data = responseOptionsListWrapper;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
    }
    
    private static void getErrorResponse(Integer statusCode, String title, 
    String responseMessage, String devMessage) {
        ResponseWrapper responseWrapper = new ResponseWrapper();
        cls_meta_data responseMetaData = new cls_meta_data();
        responseMetaData.status_code = statusCode;
        responseMetaData.title = title;
        responseMetaData.message = responseMessage;
        responseMetaData.developer_message = devMessage;
        responseWrapper.meta_data = responseMetaData;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
    }
    
    /* Wrapper classes for returning reponse */
    public class ResponseWrapper {
        public cls_meta_data meta_data;
        public cls_data_list_wrapper data;
    }

    public class cls_meta_data {
        public Integer status_code;
        public String message;
        public String title;
        public String developer_message;   
    }

    public class cls_data_list_wrapper {
        public List<cls_data_list_item> service_requests;
    }

    public class cls_data_list_item implements Comparable {
        public String unique_id;
        public String sr_name;
        public Boolean is_draft;
        public String draft_case_no;
        
        public Integer compareTo(Object compareTo) {
            cls_data_list_item lstItemToCompare = (cls_data_list_item)compareTo;
            return this.unique_id.compareTo(lstItemToCompare.unique_id);
        }
    }
}