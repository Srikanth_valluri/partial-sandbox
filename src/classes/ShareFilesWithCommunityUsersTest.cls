/****************************************************************************************
 * Class : ShareFilesWithCommunityUsersTest
 * Created By : ES
 -----------------------------------------------------------------------------------
 * Description : Test Class for ShareFilesWithCommunityUsers
 -----------------------------------------------------------------------------------
 * Version History:
 * Version    Developer Name    Date          Detail Features
   1.0        ES                09/08/2018    Initial Development
 **********************************************************************************/
@isTest
private class ShareFilesWithCommunityUsersTest {
    
    /**
     * Method to insert records for Account
     */
    private static Account createAccount(String accName) {
        Account accountInstance = new Account(
            Name = accName
        );
        insert accountInstance;
        return accountInstance;
        
    }
    
    /**
     * Method to insert records for ContentDocumentLink
     */
    private static ContentDocumentLink createCDL(Id acctId) {
        ContentVersion contentVersion = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
            );
        insert contentVersion;
        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :contentVersion.Id];
    
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.LinkedEntityId = acctId;
        cdl.ContentDocumentId = testcontent.ContentDocumentId;
        cdl.ShareType = 'V';
        insert cdl;
        return cdl;
    }
    
    /**
     * Method to test ShareType of Content Document link
     */
     private static testMethod void testShareFilesWithCommunityUsers() {
        //Account acct = createAccount('TEST_ACCT');
        Announcement__c anncObj = new Announcement__c();
        anncObj.Start_Date__c = system.today().adddays(-3);
        anncObj.End_Date__c = system.today().adddays(3);
        anncObj.Title__c = 'Test';
        anncObj.Active__c = true;
        anncObj.Description__c = 'Test';
        anncObj.Agency_Type__c = 'Corporate';
        insert anncObj;
        
        ContentDocumentLink cdl = createCDL(anncObj.id);
        
        ContentDocumentLink cdlInstance = [SELECT Visibility,
                                                  ShareType ,
                                                  LinkedEntityId,
                                                  ContentDocumentId 
                                            FROM ContentDocumentLink c where LinkedEntityId =: anncObj .Id][0];
        System.assert(cdlInstance != null);
        System.assertEquals('I', cdlInstance.ShareType);                                
    }
}