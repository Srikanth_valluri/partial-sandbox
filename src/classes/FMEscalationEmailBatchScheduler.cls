public class FMEscalationEmailBatchScheduler implements Schedulable {

		public static String cronExp = '0 0 8 * * ?';

    public static String jobName = 'FMEscalationEmailBatch';
		public void scheduleMyJob() {
			FMEscalationEmailBatchScheduler batchObj = new FMEscalationEmailBatchScheduler();
	        System.schedule(jobName, cronExp, batchObj);
		}
		public void execute(SchedulableContext sc) {
				FMEscalationEmailBatch batchObj = new FMEscalationEmailBatch();
	      Database.executeBatch(batchObj, 200);
		}
}