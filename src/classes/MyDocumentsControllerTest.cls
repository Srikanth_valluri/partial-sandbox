@isTest
public class MyDocumentsControllerTest {

       @isTest
        public static void testAttachmentListMethod(){
            Account accList =  CommunityTestDataFactory.CreatePersonAccount();
            Case caseList = new Case (
                                          Status='New',
                                          Origin='Call',
                                          Priority='Medium', 
                                          AccountId = accList.id
                                     );
            insert caseList;
            system.debug('caseList' + caseList);

            SR_Attachments__c srList = new 
                                        SR_Attachments__c(
                                                            Name ='Original Sale Agreement / Title Deed', 
                                                            isValid__c= False,
                                                            Case__c = caseList.id
                                                        ) ;
            insert srList;
            system.debug('srList' + srList);
            MyDocumentsController controller = new MyDocumentsController();
            controller.personAccountId= accList.id;
            Test.startTest(); 
            controller.attachmentListMethod();
            Test.stopTest(); 
            String caseId= caseList.id;
            list<SR_Attachments__c> srAttachList = [
                                                     SELECT Id
                                                         , Name
                                                         , IsValid__c
                                                         , Attachment_URL__c
                                                      FROM SR_Attachments__c
                                                     WHERE Case__c IN (SELECT Id
                                                                         FROM Case
                                                                        WHERE AccountId = :controller.personAccountId
                                                              )
                                                    ];
                system.assertEquals(1,srAttachList.size());
       }

}