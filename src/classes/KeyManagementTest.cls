@isTest
public class KeyManagementTest {
    @isTest
    static void testShowUsers(){
        Profile objProfile = [
            SELECT Id 
            FROM Profile 
            WHERE Name='Standard User'
        ];
        User objUser = new User();
        objUser.alias = 'test';
        objUser.username = 'test1234@testorg.com';
        objUser.email = 'test1234@testorg.com';
        objUser.firstName = 'test1';
        objUser.lastName = 'test2';
        objUser.localeSidKey = 'cs';
        objUser.languageLocaleKey = 'en_US';
        objUser.emailEncodingKey = 'Big5';
        objUser.timeZoneSidKey = 'Africa/Cairo';
        objUser.currencyIsoCode = '';
        objUser.profileId = objProfile.Id;
        insert objUser;
        System.runAs( objUser ) {
            
            System.debug( 'Current User: ' + UserInfo.getUserName() );
            System.debug( 'Current Profile: ' + UserInfo.getProfileId() ); 
        }
        
        Test.startTest();
            KeyManagementCtrl objManagementCtrl = new KeyManagementCtrl();
            objManagementCtrl.ownerType = 'User';
            objManagementCtrl.ownerSearchKey = 'test';
            objManagementCtrl.searchOwners();
            objManagementCtrl.keyOwner = objManagementCtrl.owners[0].Id;
            //objManagementCtrl.executeQuery();
        Test.stopTest();
    }
    
    @isTest
    static void testShowAccount(){
        Account objAccount = new Account();
        objAccount.Name = 'test';
        insert objAccount;
        
        Test.StartTest();
            KeyManagementCtrl objManagementCtrl = new KeyManagementCtrl();
            objManagementCtrl.ownerType = 'Account';
            objManagementCtrl.ownerSearchKey = 'test';
            objManagementCtrl.searchOwners();
        	objManagementCtrl.getKeyInventoryDisplayFields();
        	objManagementCtrl.getKeyIssuanceDisplayFields();
        Test.stopTest();
        
    }
    
    @isTest
    static void testKeyIssuance(){
        
        Inventory__c objInventory = new Inventory__c();
        insert objInventory;
        
        Key_Inventory__c objKeyInventory = new Key_Inventory__c();
        objKeyInventory.Inventory__c = objInventory.Id;
        objKeyInventory.Key_Issue_Status__c = 'Issued';
        objKeyInventory.Key_Owner_Address__c = 'test';
        objKeyInventory.Key_Card_Number__c = '12345';
        objKeyInventory.Key_Owner_Email__c = 'test@eternussolutions.com';
        objKeyInventory.Key_Owner_Phone__c = '12345';
        objKeyInventory.Key_Owner_Type__c = 'FM';
        objKeyInventory.Key_Status__c = 'Available';
        objKeyInventory.Key_Sub_Status__c = 'New';
        objKeyInventory.Type_Of_Key__c = 'Key';
        insert objKeyInventory;
        
        Test.StartTest();
            KeyManagementCtrl objManagementCtrl = new KeyManagementCtrl();
            objManagementCtrl.addedKeys = objKeyInventory.Id;

            //objManagementCtrl.executeQuery();
            objManagementCtrl.submitKeyIssuance();
            objManagementCtrl.submitKeyReturn();
            objManagementCtrl.submitInventoryChange();
        Test.stopTest();
    }
}