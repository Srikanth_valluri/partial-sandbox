/*
 * Description: Class used to generate Offer & Acceptance Letter through Drawloop
 */

public class ParkingOfferAcceptanceLetterController {
    public Case objCase;
    private Static String taskSubject;

    public ParkingOfferAcceptanceLetterController(ApexPages.StandardController controller) {
        objCase = (Case)controller.getrecord();
        taskSubject = 'Generate Parking Offer & Acceptance Letter';
    }

    /**
     * Method used to execute Drawloop batch and create Offer & Acceptance Letter
     */
	public pageReference generateOfferAcceptanceLetter() {

        // Check whether task is present or not
        List<Task> lstTasks = [SELECT Id, Status FROM Task WHERE WhatId =: objCase.Id AND Subject =: taskSubject
            AND Status != 'Closed' AND Status != 'Completed'];

        // If the task is present then execute the Drawloop batch
        if (lstTasks.size() > 0) {
            if (!Test.isRunningTest()) {
                Database.executeBatch(new GenerateDrawloopDocumentBatch(String.valueOf(objCase.Id), Label.Parking_Offer_Acceptance_Letter_DDP_Template_Id, Label.Parking_Offer_Acceptance_Letter_Template_Delivery_Id));
            }
        } else {
            ApexPages.addmessage(new ApexPages.message(
                ApexPages.severity.Error,'There is no "Generate Offer & Acceptance Letter" Task present for this case'));
            return null;
        }

        // Redirect to Case detail page
        PageReference  caseDetailPage = new PageReference ('/'+objCase.Id);
        caseDetailPage.setRedirect(true);
        return caseDetailPage;
	}
}