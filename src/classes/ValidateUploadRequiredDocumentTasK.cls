public with sharing class ValidateUploadRequiredDocumentTasK {
    public static void methodValidateUploadRequiredDocumentTasK(Task taskObj){
        
        system.debug('taskObj : '+taskObj);
        system.debug('taskObj.whatId :'+taskObj.whatId);
       
       Case Objcase=[ SELECT id,OwnerId
                            ,( SELECT id
                                ,Attachment_URL__c 
                                From SR_Attachments__r )
                        From Case
                        WHERE id =: taskObj.whatId
                        AND status != 'Closed'
                        AND status != 'Completed'
                        AND status != 'Cancelled'
                        LIMIT 1];

        System.debug('Objcase == ' +  Objcase);
        Boolean showError = false;
        
        Set<Id> setBuId =new Set<Id>();
        if( Objcase != NULL  && Objcase.SR_Attachments__r == NULL && Objcase.SR_Attachments__r.size()>0 ){
          for (SR_Attachments__c objSRAtt : Objcase.SR_Attachments__r) { if( objSRAtt.Attachment_URL__c == NULL ){ showError = true;}  }
        }

        if( showError ){ taskObj.addError('Please upload Requied Document');
        }

           
    }
}