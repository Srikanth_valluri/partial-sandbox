public virtual without sharing class CRE_Cheque_Replacement_Controller{
    public String strSRType {get;set;}
    public String strAccountID {get;set;}
    public boolean blnShowChequeSection {get;set;}
    public String strSelectedProcess{get;set;}
    public List<SelectOption> lstBookingUnitOptions {get;set;}
    public String strSelectedBookingUnit {get;set;}
    public List<ChequeDetailWrapper> lstChequeDetailWrapper {get;set;}
    public List<ChequeDetailWrapper> lstChequeFinalWrapper {get;set;}
    public boolean blnShowChqueDetails {get;set;}
    public boolean blnShowChqueDetails2 {get;set;}
    public boolean blnShowFinalChequeDetails {get;set;}
    //public List<pdcDetailsWrapper> lstPDCDetails {get;set;}
    public Booking_Unit__C objBookingUnit {get;set;}
    public Case objCase {get;set;}
    public boolean blnShowBtn {get;set;}

    public string strCaseID {get; set;}
    public String strSRtypeURL ;
    public String strSelectedAccount;
    public List<Account> lstAccount ;
    public List<Booking_Unit__c> lstBookingUnit;
    public List<ChequeDetailWrapper> lstFinalChequeDetailWrapper;
    public List<Case> lstCase ;

    public CRE_Cheque_Replacement_Controller(){
        blnShowChequeSection = true;
        strSelectedAccount = ApexPages.currentPage().getParameters().get('AccountID');
        strCaseID = ApexPages.currentPage().getParameters().get('caseID');
        strSRtypeURL = ApexPages.currentPage().getParameters().get('SRType');
        blnShowChqueDetails = false;
        blnShowChqueDetails2 = false;
        blnShowFinalChequeDetails = false;
        blnShowBtn = false;

        if(String.isNotBlank(strSelectedAccount) && strSRtypeURL == 'ChequeReplacement'){
            init();
        }
        if(String.isNotBlank(strSelectedAccount) && String.isBlank(strCaseID) && String.isBlank(strSRtypeURL)){
            init(); // for "apex/CREPortalHomeChequeReplacement?AccountID=0017E00000aqdaS"
        }
    }

    protected CRE_Cheque_Replacement_Controller(Boolean shouldCall) {}

    public string getName() {
        System.debug('--getNamest AccountID ---'+strAccountID);
        System.debug('--getNamest strSRType ---'+strSRType);

        if(String.isNotBlank(strAccountID) && String.isNotBlank(strSRType) && strSRType == 'ChequeReplacement'){
            strSelectedAccount = strAccountID;
            init();
        }
        return strAccountID;
    }

    public void init() {
        if(String.isNotBlank(strSelectedAccount)){
            lstBookingUnitOptions = new List<SelectOption>();
            blnShowChequeSection = true;
            lstAccount = new List<Account>();
            strCaseID = ApexPages.currentPage().getParameters().get('caseID');
            lstAccount = [SELECT Id, Name, LastName, FirstName, Salutation, MiddleName,
                              IsPersonAccount,phone,PersonEmail,PersonMobilePhone,Party_ID__c
                              FROM Account where id =: strSelectedAccount];
            if(lstAccount.Size()>0){
                lstBookingUnit = new List<Booking_Unit__c>();
                lstBookingUnit = [Select id,name,Property_Name__c,Inventory__c,Booking__c,
                                  Unit_Name__c,CurrencyIsoCode,
                                  Registration_ID__c,Mortgage__c
                                  from Booking_Unit__c where Booking__r.Account__c =:
                                  strSelectedAccount AND Registration_ID__c != null
                                  AND Unit_Name__c != null];
                if(lstBookingUnit.Size()>0){
                    lstBookingUnitOptions.add(new SelectOption('','--None--' ));
                    for(Booking_Unit__c objBU: lstBookingUnit){
                        lstBookingUnitOptions.add(new SelectOption(objBU.id,objBU.Unit_Name__c ));
                    }
                System.debug('--lstBookingUnit---'+lstBookingUnit.Size()+'---'+lstBookingUnit);
                }
            }
            if(String.isNotBlank(strCaseID)){
                lstCase = [Select id,CaseNumber,RecordType.DeveloperName,RecordType.Name,
                           AccountID, Booking_Unit__c,status,
                           (select id,name,New_Amount__c,New_Bank__c,
                           New_cheque_number__c,New_maturity_date__c,
                           Reason_For_Replacement__c,Receipt_No__c from Case_Post_Dated_Cheques__r)
                           from Case where id =: strCaseID ];
                objCase = lstCase[0];

                if(lstCase.Size()>0){
                    if(lstCase[0].RecordType.DeveloperName == 'Cheque_Replacement' ){
                         strSelectedProcess = lstCase[0].RecordType.name;
                         if(lstCase[0].Booking_Unit__c != null){
                             strSelectedBookingUnit = lstCase[0].Booking_Unit__c;
                         }
                         if(lstCase[0].Case_Post_Dated_Cheques__r.Size()>0){
                             blnShowFinalChequeDetails = true;
                             blnShowChqueDetails = true;
                             blnShowChqueDetails2 = false;
                             objBookingUnit = new Booking_Unit__C();
                             for(Booking_Unit__C objBU : lstBookingUnit ){
                                 if(objBU.id == strSelectedBookingUnit ){
                                     objBookingUnit = objBU;
                                 }
                             }

                             lstChequeFinalWrapper = new List<ChequeDetailWrapper>();
                             for(Case_Post_Dated_Cheque__c objCPD : lstCase[0].Case_Post_Dated_Cheques__r){
                                 ChequeDetailWrapper objwrapper = new ChequeDetailWrapper(1,System.today(), '' ,'',null,false,null,null);
                                 objwrapper.objPDC = objCPD;
                                 if(objCPD.New_maturity_date__c != null){
                                      objWrapper.strMaturityDate = objCPD.New_maturity_date__c.format();
                                 }
                                 lstChequeFinalWrapper.add(objwrapper);
                             }
                         }
                    }
                }

            }
        }
        else{
            blnShowChequeSection = false;
        }
    }

    public void bookingUnitDetails() {
        if(String.isNotBlank(strSelectedProcess)){
            if(String.isBlank(strSelectedBookingUnit)){
                ApexPages.addmessage(new ApexPages.message(
                    ApexPages.severity.Error,'Please select a booking unit'));
            }
            else{
                lstChequeDetailWrapper = new List<ChequeDetailWrapper>();
                List<pdcDetailsWrapper> lstPDCDetails = new list<pdcDetailsWrapper>();
                blnShowChqueDetails = true;
                blnShowChqueDetails2 = true;
                blnShowBtn = false;

                try{
                    objBookingUnit = new Booking_Unit__C();
                    for(Booking_Unit__C objBU : lstBookingUnit ){
                        if(objBU.id == strSelectedBookingUnit ){
                            objBookingUnit = objBU;
                        }
                    }
                    String strPDCResponse = assignmentEndpoints.fetchPDCDetails(objBookingUnit);
                    system.debug('----strPDCResponse---'+strPDCResponse);

                    if(String.isNotBlank(strPDCResponse)){
                        map<String,Object> mapDeserializePDC = (map<String,Object>)JSON.deserializeUntyped(strPDCResponse);
                        system.debug('----mapDeserializePDC -----'+mapDeserializePDC.Size()+'---'+mapDeserializePDC);
                        if(mapDeserializePDC.get('status') == 'S'){
                            String StringPDCData = JSON.serialize(mapDeserializePDC.get('data'));
                            system.debug('----StringPDCData-----'+StringPDCData);
                            if(String.isNotBlank(StringPDCData) && StringPDCData != '[]'){
                                lstPDCDetails =
                                (List<pdcDetailsWrapper>)JSON.deserialize(StringPDCData, List<pdcDetailsWrapper>.class);
                                system.debug('----lstPDCDetails-----'+lstPDCDetails);
                                lstChequeDetailWrapper = populateChequeDetailWrapper(lstPDCDetails);
                              
                            }
                            else{
                                ApexPages.addmessage(new ApexPages.message(
                                ApexPages.severity.Error,'No PDC data available for booking Unit: '+objBookingUnit.Unit_Name__c ));

                                /*String errorMessage = 'No PDC data available for booking Unit; Error Message - '+mapDeserializePDC.get('message') ;
                                Error_Log__c objErr = createErrorLogRecord(strSelectedAccount,strSelectedBookingUnit);
                                objErr.Error_Details__c = errorMessage;*/
                                //insert objErr;
                            }

                        }
                        else if(mapDeserializePDC.get('status') == 'E'){
                            String errorMessage = 'Error : '+mapDeserializePDC.get('message');
                            Error_Log__c objErr = createErrorLogRecord(strSelectedAccount,strSelectedBookingUnit);
                            objErr.Error_Details__c = errorMessage;
                            insert objErr;
                        }
                    }
                    else {
                        String errorMessage = 'Error : No Response from IPMS for PDC Details';
                        Error_Log__c objErr = createErrorLogRecord(strSelectedAccount,strSelectedBookingUnit);
                        objErr.Error_Details__c = errorMessage;
                        insert objErr;
                    }

                    /*ChequeDetailWrapper objwrapper1 = new ChequeDetailWrapper(1,System.today().addDays(1),'CHQ # 458796','12345',5000,false,lstReasonOptions,lstDaysHoldOptions);
                    ChequeDetailWrapper objwrapper2 = new ChequeDetailWrapper(2,System.today().addDays(2),'CHQ # 124578','98745',3300,false,lstReasonOptions,lstDaysHoldOptions);
                    ChequeDetailWrapper objwrapper3 = new ChequeDetailWrapper(3,System.today().addDays(3),'CHQ # 963251','78912',1000,false,lstReasonOptions,lstDaysHoldOptions);
                    ChequeDetailWrapper objwrapper4 = new ChequeDetailWrapper(4,System.today().addDays(4),'CHQ # 197347','37810',7500,false,lstReasonOptions,lstDaysHoldOptions);

                    lstChequeDetailWrapper.add(objwrapper1);
                    lstChequeDetailWrapper.add(objwrapper2);
                    lstChequeDetailWrapper.add(objwrapper3);
                    lstChequeDetailWrapper.add(objwrapper4);
                    */
                    if(strSelectedProcess == 'Cheque Replacement'){
                        blnShowBtn = false;
                    }
                    else{
                        blnShowBtn = true;
                    }

                }
                catch(exception ex){
                    ApexPages.addmessage(new ApexPages.message(
                    ApexPages.severity.Error,ex.getMessage()));
                }

            }
        }
        else{
            ApexPages.addmessage(new ApexPages.message(
                ApexPages.severity.Error,'Please select Type first'));
        }
    }

    public List<ChequeDetailWrapper> populateChequeDetailWrapper(List<PDCDetailsWrapper> lstPDCDetails) {
        List<ChequeDetailWrapper> lstChequeDetailWrapper = new List<ChequeDetailWrapper>();
        if(lstPDCDetails != null && !lstPDCDetails.isEmpty()) {
            Schema.DescribeFieldResult ReasonFieldDescription = Case_Post_Dated_Cheque__c.Reason_For_Replacement__c.getDescribe();
            List<SelectOption> lstReasonOptions = new List<SelectOption>();
            lstReasonOptions.add(new SelectOption('','--None--'));
            for (Schema.Picklistentry picklistEntry : ReasonFieldDescription.getPicklistValues()){
                lstReasonOptions.add(new SelectOption(pickListEntry.getValue(),pickListEntry.getLabel()));
            }

            Schema.DescribeFieldResult DaysHoldFieldDescription = Case_Post_Dated_Cheque__c.Days_to_Hold__c.getDescribe();
            List<SelectOption> lstDaysHoldOptions = new List<SelectOption>();
            lstDaysHoldOptions.add(new SelectOption('','--None--'));
            for (Schema.Picklistentry picklistEntry : DaysHoldFieldDescription.getPicklistValues()){
                lstDaysHoldOptions.add(new SelectOption(pickListEntry.getValue(),pickListEntry.getLabel()));
            }

            
            Integer intCount = 1;
            for(pdcDetailsWrapper objPDC: lstPDCDetails) {
                lstChequeDetailWrapper.add(new ChequeDetailWrapper(1, CustomerCommunityUtils.parseIpmsDateString(objPDC.ATTRIBUTE3),
                    objPDC.ATTRIBUTE2, objPDC.ATTRIBUTE6, String.isNotBlank(objPDC.ATTRIBUTE4) ? Decimal.valueOf(objPDC.ATTRIBUTE4) : null,
                    false, lstReasonOptions, lstDaysHoldOptions));
            }
        }
        return lstChequeDetailWrapper;
    }

    public void replaceCheques(){
        lstChequeFinalWrapper = new List<ChequeDetailWrapper>();
        boolean blnIsSelected = false;
        boolean blnIsSelectedWithReason = true;
        blnShowBtn = false;

        for(ChequeDetailWrapper obj : lstChequeDetailWrapper){
            if(obj.blnIsReplaced){
                if(string.isNotBlank(obj.strSelectedReason)){
                    blnIsSelected = true;

                    lstChequeFinalWrapper.add(obj);

                }
                else{
                     blnIsSelectedWithReason = false;
                     ApexPages.addmessage(new ApexPages.message(
                     ApexPages.severity.Error,'Please select Reason for Cheque No: '+obj.strChequeNo));
                }

            }
        }
        if(blnIsSelected && blnIsSelectedWithReason){
            blnShowFinalChequeDetails = true;
            blnShowBtn = true;
        }
        else{
            ApexPages.addmessage(new ApexPages.message(
            ApexPages.severity.Error,'Please select Cheque Number and Reason for Replacement'));
        }
    }

    public virtual pageReference submitCase(){
        system.debug('inside submitcase');
        try{
            if(String.isNotBlank(strSelectedProcess)){
                if(String.isBlank(strSelectedBookingUnit)){
                    ApexPages.addmessage(new ApexPages.message(
                        ApexPages.severity.Error,'Please select a booking unit'));
                }
                else{
                    system.debug('lstChequeDetailWrapper=='+lstChequeDetailWrapper);
                    if(strSelectedProcess == 'Cheque Hold'){
                        lstChequeFinalWrapper = new List<ChequeDetailWrapper>();
                        boolean blnIsSelected = false;
                        boolean blnIsSelectedWithHoldDays = true;

                        for(ChequeDetailWrapper obj : lstChequeDetailWrapper){
                            if(obj.blnIsReplaced){
                                if(string.isNotBlank(obj.strSelectedDayHold)){
                                    blnIsSelected = true;
                                    lstChequeFinalWrapper.add(obj);
                                }
                                else{
                                    blnIsSelectedWithHoldDays = false;
                                    ApexPages.addmessage(new ApexPages.message(
                                    ApexPages.severity.Error,'Please select Days to Hold for Cheque No: '+obj.strChequeNo));
                                }

                            }
                        }
                        if(blnIsSelected && blnIsSelectedWithHoldDays){

                        }
                        else{
                            ApexPages.addmessage(new ApexPages.message(
                            ApexPages.severity.Error,'Please select Cheque Number and Days to Hold'));
                            return null;
                        }

                    }
                    else if(strSelectedProcess == 'Security Cheque Collection'){
                        lstChequeFinalWrapper = new List<ChequeDetailWrapper>();
                        boolean blnIsSelected = false;

                        for(ChequeDetailWrapper obj : lstChequeDetailWrapper){
                            if(obj.blnIsReplaced){
                                blnIsSelected = true;
                                lstChequeFinalWrapper.add(obj);
                            }
                        }
                        if(blnIsSelected){

                        }
                        else{
                            ApexPages.addmessage(new ApexPages.message(
                            ApexPages.severity.Error,'Please select at least one Cheque Number'));
                            return null;
                        }
                    }
                    /**
                     * Added for Cheque Release
                     */
                    else if(strSelectedProcess == 'Cheque Release'){
                        System.debug('lstChequeDetailWrapper=='+lstChequeDetailWrapper);
                        lstChequeFinalWrapper = new List<ChequeDetailWrapper>();
                        for(ChequeDetailWrapper obj : lstChequeDetailWrapper){ 
                            if( obj.blnIsReplaced ) {
                                system.debug('inside if statement of obj.blnIsReplaced');
                                obj.objPDC.Reason_for_returning_Cheque__c = obj.strSelectedReturnReason;
                                obj.objPDC.Type_of_cheque__c = obj.strSelectedType;
                                lstChequeFinalWrapper.add(obj);
                            }
                        }
                    }
                    system.debug('lstChequeFinalWrapper=='+lstChequeFinalWrapper);
                    if(lstChequeFinalWrapper.Size()>0){
                        Id devRecordTypeId =
                        Schema.SObjectType.Case.getRecordTypeInfosByName().get(strSelectedProcess).getRecordTypeId();
                        objCase  = new Case();
                        objCase.AccountID = strSelectedAccount ;
                        objCase.RecordTypeID = devRecordTypeId;
                        objCase.Origin = 'Walk-In';

                        /*if(strSelectedProcess == 'Cheque Replacement'){
                            objCase.SR_type__c = 'Cheque Replacement SR';
                        }
                        else if(strSelectedProcess == 'Cheque Hold'){
                            objCase.SR_type__c = 'Cheque Hold SR';
                        }
                        else if(strSelectedProcess == 'Security Cheque Collection'){
                            objCase.SR_type__c = 'Security Cheque Collection SR';
                        }*/

                        objCase.SR_type__c = strSelectedProcess == 'Cheque Replacement' ? 'Cheque Replacement SR' : (strSelectedProcess == 'Cheque Hold' ? 'Cheque Hold SR' : 'Security Cheque Collection SR');
                        objCase.booking_unit__c = strSelectedBookingUnit;
                        objCase.Status = 'Submitted';
                        //upsert objCase;
                         if(strSelectedProcess !='Cheque Release')
                         {
                        //For submitting case for approval
                        objCase.Roles_from_Rule_Engine__c = 'Manager,Director,Sales Admin AVP,HOD';
                        objCase.Approving_Authorities__c = 'Manager,Director,Sales Admin AVP,HOD';
                        objCase.Submit_for_Approval__c = true;
                         }
                        upsert objCase;

                        List<Case_Post_Dated_Cheque__c> lstCasePDC = new List<Case_Post_Dated_Cheque__c>();
                        for(ChequeDetailWrapper obj : lstChequeFinalWrapper){
                            obj.objPDC.Case__c = objCase.id;
                            obj.objPDC.Receipt_No__c = obj.strReceiptNo ;
                            obj.objPDC.CurrencyISOCode = objBookingUnit.CurrencyIsoCode;

                            obj.objPDC.Reason_For_Replacement__c = obj.strSelectedReason;
                            if(string.isNotBlank(obj.strMaturityDate)){
                                obj.objPDC.New_maturity_date__c = Date.parse(obj.strMaturityDate);
                            }
                            obj.objPDC.Days_to_Hold__c = obj.strSelectedDayHold;

                            /*if(strSelectedProcess == 'Cheque Replacement'){
                                obj.objPDC.Reason_For_Replacement__c = obj.strSelectedReason;
                                if(string.isNotBlank(obj.strMaturityDate)){
                                    obj.objPDC.New_maturity_date__c = Date.parse(obj.strMaturityDate);
                                }
                            }*/
                            /*else if(strSelectedProcess == 'Cheque Hold'){
                                obj.objPDC.Days_to_Hold__c = obj.strSelectedDayHold;
                            }*/

                            lstCasePDC.add(obj.objPDC);
                        }

                        insert lstCasePDC;

                        /*objCase = [Select id,CaseNumber,subject,RecordType.name, ownerID from Case where id=: objCase.ID ];

                        Task objTask = new Task();
                        if(objCase.RecordType.Name == 'Cheque Replacement'){
                            objTask =  TaskUtility.getTask((SObject)objCase, 'Confirm date to replace', 'Finance',
                                    objCase.RecordType.Name, system.today().addDays(1));
                        }
                        else if(objCase.RecordType.Name == 'Cheque Hold'){
                            objTask =  TaskUtility.getTask((SObject)objCase, 'Hold cheque', 'Finance',
                                    objCase.RecordType.Name, system.today().addDays(1));
                        }
                        else if(objCase.RecordType.Name == 'Security Cheque Collection'){
                            objTask =  TaskUtility.getTask((SObject)objCase, 'Confirm dt for security chq coll', 'Finance',
                                    objCase.RecordType.Name, system.today().addDays(1));
                        }
                        system.debug('--objTask--'+objTask);
                        insert objTask;*/

                        pageReference pgr = new pageReference('/'+objCase.id);
                        return pgr;
                    }

                }
            }
            else{
                ApexPages.addmessage(new ApexPages.message(
                    ApexPages.severity.Error,'Please select Type first'));

            }
            return null;

        }
        catch(exception ex){
            ApexPages.addmessage(new ApexPages.message(
                ApexPages.severity.Error,ex.getMessage()));
                return null;
        }

    }

     public Error_Log__c createErrorLogRecord(Id accId, Id bookingUnitId){
        Error_Log__c objErr = new Error_Log__c();
        objErr.Account__c = accId;
        objErr.Booking_Unit__c = bookingUnitId;
        objErr.Process_Name__c = 'Cheque Replacement';
        return objErr;
    }


     public class pdcDetailsWrapper {
        public String ATTRIBUTE1 {get;set;}
        public String ATTRIBUTE2 {get;set;}
        public String ATTRIBUTE3 {get;set;}
        public String ATTRIBUTE4 {get;set;}
        public String ATTRIBUTE5 {get;set;}
        public String ATTRIBUTE6 {get;set;}
        public String ATTRIBUTE7 {get;set;}
        public String ATTRIBUTE8 {get;set;}

        public pdcDetailsWrapper(){

        }
    }

    public class ChequeDetailWrapper{
        public integer intCounter {get;set;}
        public date datePDC {get;set;}
        public string strChequeNo {get;set;}
        public string strReceiptNo {get;set;}
        public decimal decAmount {get;set;}
        public boolean blnIsReplaced {get;set;}
        public List<SelectOption> lstOptions {get;set;}
        public List<SelectOption> lstDayHoldOptions {get;set;}
        public string strSelectedReason {get;set;}
        public string strSelectedDayHold {get;set;}
        public Case_Post_Dated_Cheque__c objPDC {get;set;}
        public string strMaturityDate {get;set;}
        /** 
         * Added for cheque release process
         * */        
        public List<SelectOption> listChqReturnReasons {get;set;}
        public List<SelectOption> listChqTypes {get;set;}
        public string strSelectedReturnReason {get;set;}
        public string strSelectedType {get;set;}
        
        public ChequeDetailWrapper(integer WintCounter,date WdatePDC,string WstrChequeNo,
                                   string WstrReceiptNo,decimal WdecAmount,
                                   boolean WblnIsReplaced, List<SelectOption> WlstOptions,List<SelectOption> WlstDayHoldOptions ){

            intCounter = WintCounter ;
            datePDC = WdatePDC ;
            strChequeNo = WstrChequeNo ;
            strReceiptNo = WstrReceiptNo ;
            decAmount = WdecAmount ;
            blnIsReplaced = WblnIsReplaced ;
            lstOptions = WlstOptions;
            lstDayHoldOptions = WlstDayHoldOptions;

            objPDC = new Case_Post_Dated_Cheque__c();
            listChqReturnReasons = new List<SelectOption>();
            listChqTypes = new List<SelectOption>();

            for (Schema.Picklistentry picklistEntry : Case_Post_Dated_Cheque__c.Reason_for_returning_Cheque__c.getDescribe().getPicklistValues() ){
                listChqReturnReasons.add(new SelectOption( pickListEntry.getValue(), pickListEntry.getLabel() ) );
            }

            for (Schema.Picklistentry picklistEntry : Case_Post_Dated_Cheque__c.Type_of_cheque__c.getDescribe().getPicklistValues() ){
                listChqTypes.add(new SelectOption( pickListEntry.getValue(), pickListEntry.getLabel() ) );
            }

        }
    }


}