/*
Class Name : API_CommunityChangePassword
Description : To change the community user password
Sample URL : /services/apexrest/changepassword?username=******@*****.com&newpassword=*****
Method     : POST   
Test Class : API_Community_Test              
    
*/

@RestResource(urlMapping='/changepassword/*')
global without sharing class API_CommunityChangePassword{ 

    
    @HttpPost
    global static LoginResponse changePassword() {
        LoginResponse objResponse = new LoginResponse();
        
        String username = RestContext.request.params.get('username');
        String password = RestContext.request.params.get('newpassword');
        User u = new User ();
        try {
            // To check whether requested user is community user or not
            u = [SELECT ID FROM User WHERE userName =: userName AND Profile.Name LIKE '%COMMUNITY%' LIMIT 1];
          //  try {
                // To set the new password for the user
                if (!Test.isRunningTest() && u.Id == UserInfo.getUserId()){
                    System.setPassword (u.Id, password);
                    objResponse.isSuccess = true;
                    objResponse.sessionId = '';
                    objResponse.statusMessage = 'Password changed successfully.';
                    objResponse.statusCode = 200;
                }
                else{
                    objResponse.isSuccess = false;
                    objResponse.sessionId = '';
                    objResponse.statusMessage = ' You do not have the privilege to change others Password.';
                    objResponse.statusCode = 401;
                
                }    
                
           /* } catch (Exception ex) {
                objResponse.isSuccess = false;
                objResponse.sessionId = '';
                objResponse.statusMessage = ex.getMessage ();
                objResponse.statusCode = 400;
            }*/
            
        } catch (Exception e) {
            objResponse.isSuccess = false;
            objResponse.sessionId = '';
            objResponse.statusMessage = 'UserName is not a valid Community user.';
            objResponse.statusCode = 400;
        }
        return objResponse;        
        
    }
    
    // To send the response back to API 
    global class LoginResponse {
        public String sessionId {get; set;}
        public Boolean isSuccess {get; set;}
        public String statusMessage {get; set;}
        public Integer statusCode {get; set;}
    }
}