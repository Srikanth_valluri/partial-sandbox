//
// To read the Bitly Resonse from the Call out Json body
//

public class DAMAC_BITLY_RESPONSE_OBJ {

    public Integer status_code;
    public String status_txt;
    public Data data;

    public class Data {
        public String url;
        public String hash;
        public String global_hash;
        public String long_url;
        public Integer new_hash;
    }

    
    public static DAMAC_BITLY_RESPONSE_OBJ parse(String json) {
        return (DAMAC_BITLY_RESPONSE_OBJ) System.JSON.deserialize(json, DAMAC_BITLY_RESPONSE_OBJ.class);
    }
}