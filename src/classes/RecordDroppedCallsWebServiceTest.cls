@isTest
private class RecordDroppedCallsWebServiceTest{

    @testSetup static void setup() {
        TriggerOnOffCustomSetting__c obj = new TriggerOnOffCustomSetting__c();
        obj.Name = 'CallingListTrigger';
        obj.OnOffCheck__c = true;
        insert obj;
    }

    @isTest static void validUserAccountCheck(){
        User userObj = CreRoutingTest.createCREUsers(
            'puser000@test.com' + System.currentTimeMillis(),
            '9999'
        );
        Account accountObj = CreRoutingTest.createRegisteredNonVipAccount('8787878787');

        RecordDroppedCallsLogic.RecordDroppedCallsRequestBody reqst =
            new RecordDroppedCallsLogic.RecordDroppedCallsRequestBody();
        reqst.dateAndTime = '10/16/2017 23:30:20';
        reqst.calledNumber = '9090909090';
        reqst.callingNumber = accountObj.Mobile_Phone_Encrypt__pc;
        reqst.creExtension = '9999';

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = 'recordDroppedCall';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JSON.serialize(reqst));
        RestContext.request = req;
        RestContext.response= res;

        Test.startTest();
        RecordDroppedCallsLogic.RecordDroppedCallsRequestBody resp =
            new RecordDroppedCallsLogic.RecordDroppedCallsRequestBody();
        RecordDroppedCallsWebService.doPost();
        Test.stopTest();

        String recordCallResponse= RestContext.response.responseBody.toString();
        RecordDroppedCallsLogic.RecordDroppedCallsResponse response =
            (RecordDroppedCallsLogic.RecordDroppedCallsResponse)JSON.deserializeStrict(
                recordCallResponse,
                RecordDroppedCallsLogic.RecordDroppedCallsResponse.Class
            );

        Calling_List__c  callingListObj = [Select Id From Calling_List__c];
        System.assertNotEquals(null,callingListObj);
    }

    @isTest static void invalidUserCheck(){
        Account accountObj = CreRoutingTest.createRegisteredNonVipAccount('8787878787');
        RecordDroppedCallsLogic.RecordDroppedCallsRequestBody reqst =
            new RecordDroppedCallsLogic.RecordDroppedCallsRequestBody();
        reqst.dateAndTime = '10/16/2017 23:30:20';
        reqst.calledNumber = '9090909090';
        reqst.callingNumber = accountObj.Mobile_Phone_Encrypt__pc;
        reqst.creExtension = '9999';

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/recordDroppedCall';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JSON.serialize(reqst));
        RestContext.request = req;
        RestContext.response= res;
        RecordDroppedCallsLogic.RecordDroppedCallsRequestBody resp =
            new RecordDroppedCallsLogic.RecordDroppedCallsRequestBody();

        Test.startTest();
        RecordDroppedCallsWebService.doPost();
        Test.stopTest();

        String recordDropResponse = RestContext.response.responseBody.toString();
        System.assert(recordDropResponse != null);
        RecordDroppedCallsLogic.RecordDroppedCallsResponse response=
            (RecordDroppedCallsLogic.RecordDroppedCallsResponse) JSON.deserializeStrict(
                recordDropResponse,
                RecordDroppedCallsLogic.RecordDroppedCallsResponse.Class
            );

        Calling_List__c  callingListObj = [Select Id From Calling_List__c];
        System.assertNotEquals(null,callingListObj);
    }

    @isTest static void invalidAccountCheck(){
        User userObj = CreRoutingTest.createCREUsers(
            'puser000@test.com' + System.currentTimeMillis(),
            '9999'
        );
        Account accountObj = CreRoutingTest.createRegisteredNonVipAccount('8787878787');

        RecordDroppedCallsLogic.RecordDroppedCallsRequestBody reqst =
            new RecordDroppedCallsLogic.RecordDroppedCallsRequestBody();
        reqst.dateAndTime = '10/16/2017 23:30:20';
        reqst.calledNumber = '9090909090';
        reqst.callingNumber = '8787878787';
        reqst.creExtension = '9999';

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/recordDroppedCall';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JSON.serialize(reqst));
        RestContext.request = req;
        RestContext.response= res;
        RecordDroppedCallsLogic.RecordDroppedCallsRequestBody resp =
            new RecordDroppedCallsLogic.RecordDroppedCallsRequestBody();

        Test.startTest();
        RecordDroppedCallsWebService.doPost();
        Test.stopTest();

        Calling_List__c  callingListObj = [Select Id From Calling_List__c];
        System.assertNotEquals(null,callingListObj);
    }

    @isTest static void badRequestCheck(){
        User userObj = CreRoutingTest.createCREUsers(
            'puser000@test.com' + System.currentTimeMillis(),
            '9999'
        );
        Account accountObj = CreRoutingTest.createRegisteredNonVipAccount('8787878787');

        RecordDroppedCallsLogic.RecordDroppedCallsRequestBody reqst =
            new RecordDroppedCallsLogic.RecordDroppedCallsRequestBody();
        reqst.dateAndTime = '10/16/2017 23:30:AM';
        reqst.calledNumber = '9090909090';
        reqst.callingNumber = '8787878787';
        reqst.creExtension = '9999';
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/recordDroppedCall';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JSON.serialize(reqst));
        RestContext.request = req;
        RestContext.response= res;

        RecordDroppedCallsLogic.RecordDroppedCallsRequestBody resp =
            new RecordDroppedCallsLogic.RecordDroppedCallsRequestBody();
        Test.startTest();
        RecordDroppedCallsWebService.doPost();
        Test.stopTest();

        String recordDropResponse = RestContext.response.responseBody.toString();
        System.assert(recordDropResponse != null);
        RecordDroppedCallsLogic.RecordDroppedCallsResponse response=
            (RecordDroppedCallsLogic.RecordDroppedCallsResponse) JSON.deserializeStrict(
                recordDropResponse,
                RecordDroppedCallsLogic.RecordDroppedCallsResponse.Class
            );
        System.assertEquals(response.errorMessage, 'Invalid integer: AM');
    }

    @isTest static void invalidDateCheck(){
        User userObj = CreRoutingTest.createCREUsers(
            'puser000@test.com' + System.currentTimeMillis(),
            '9999'
        );
        Account accountObj = CreRoutingTest.createRegisteredNonVipAccount('8787878787');

        RecordDroppedCallsLogic.RecordDroppedCallsRequestBody reqst = new RecordDroppedCallsLogic.RecordDroppedCallsRequestBody();
        reqst.dateAndTime = '10/16/2017 23:30 AM';
        reqst.calledNumber = '9090909090';
        reqst.callingNumber = '8787878787';
        reqst.creExtension = '9999';

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/recordDroppedCall';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JSON.serialize(reqst));
        RestContext.request = req;
        RestContext.response= res;
        RecordDroppedCallsLogic.RecordDroppedCallsRequestBody resp =
            new RecordDroppedCallsLogic.RecordDroppedCallsRequestBody();

        Test.startTest();
        RecordDroppedCallsWebService.doPost();
        Test.stopTest();

        String recordDropResponse = RestContext.response.responseBody.toString();
        System.assert(recordDropResponse != null);
        RecordDroppedCallsLogic.RecordDroppedCallsResponse response=
            (RecordDroppedCallsLogic.RecordDroppedCallsResponse) JSON.deserializeStrict(
                recordDropResponse,
                RecordDroppedCallsLogic.RecordDroppedCallsResponse.Class
            );
        System.assertEquals(response.errorMessage, 'Please enter valid date format (MM/DD/YYYY HH:MM:SS)');
    }

    public static testMethod void dropCallForCollections() {
        Collection_Payment_Pool__c collectionQueue = new Collection_Payment_Pool__c(
            Name = '6101',
            Extension__c = '6101',
            Language__c = 'Arabic',
            Pool_Name__c = 'Collection / Payments (Arabic)'
        );
        insert collectionQueue;

        RecordDroppedCallsLogic.RecordDroppedCallsRequestBody reqst =
            new RecordDroppedCallsLogic.RecordDroppedCallsRequestBody();
        reqst.dateAndTime = '10/16/2017 23:30:20';
        reqst.calledNumber = '9090909090';
        reqst.callingNumber = '9191919191';
        reqst.creExtension = '6101';

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = 'recordDroppedCall';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JSON.serialize(reqst));
        RestContext.request = req;
        RestContext.response= res;

        Test.startTest();
        RecordDroppedCallsLogic.RecordDroppedCallsRequestBody resp =
            new RecordDroppedCallsLogic.RecordDroppedCallsRequestBody();
        RecordDroppedCallsWebService.doPost();
        Test.stopTest();

        String recordDropResponse = RestContext.response.responseBody.toString();
        System.assert(recordDropResponse != null);
        RecordDroppedCallsLogic.RecordDroppedCallsResponse response =
            (RecordDroppedCallsLogic.RecordDroppedCallsResponse) JSON.deserializeStrict(
                recordDropResponse,
                RecordDroppedCallsLogic.RecordDroppedCallsResponse.Class
            );
        System.assertEquals('', response.errorMessage);
        System.assertEquals('Call Drop Logged', response.status);

        Calling_List__c  callingListObj = [Select Id From Calling_List__c];
        System.assertNotEquals(null,callingListObj);
    }
}