@isTest
public class UnitDetailControllerTest
{
   @testSetup static void setupData() {

        NSIBPM__Service_Request__c sr = InitializeSRDataTest.getSerReq('Deal',false,null);
        insert sr;

        List<string> statuses = new list<string>{'Draft'};
        Map<string,NSIBPM__SR_Status__c> mpsrStatus =  InitializeSRDataTest.createSRStatus(statuses);

    Location__c objLoc = InitializeSRDataTest.createLocation('123','Building');
    insert objLoc;

        Inventory__c inv = InitializeSRDataTest.createInventory(objLoc.id);
        insert inv;

        Booking__c bk = InitializeSRDataTest.createBooking(sr.id);
        insert bk;

        Booking_Unit__c bu = InitializeSRDataTest.createBookingUnit(bk.id,inv.id);
        bu.Online_Payment_Party__c = 'Third Party';
        insert bu;

        Payment_Plan__c pp = InitializeSRDataTest.createPaymentPlan(bu.id,objLoc.id);
        insert pp;

        Buyer__c b = InitializeSRDataTest.createBuyer(bk.id,false);
        B.Account__c = sr.NSIBPM__Customer__c;
        b.Is_3rd_Party__c = true;
        insert b;

        List<string> lstDocNames = new list<string>{'Third party consent'};
        List<NSIBPM__Document_Master__c> lstmasdocs = InitializeSRDataTest.createMasterDocs(lstDocNames);
        insert lstmasdocs;

        insert new IpmsRestServices__c(
                   SetupOwnerId = UserInfo.getOrganizationId(),
                   BaseUrl__c = 'http://83.111.194.181:8045/webservices/rest',
                   Username__c = 'oracle_user',
                   Password__c = 'crp1user',
                   Client_Id__c = '8MABLQM-KJ8I8-1XA58-WWCM1S1',
                   BearerToken__c = 'eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWyiwuVrJSKs5IBKK8bCUdpczEEiUrQ1MzAwtTMwsDIx2l1IoCqIC5oRFIoLQ4tSgvMTcVqM_C19HJJ9BX19vLwtNC1zDC0dRCNzzc2dcw2FCpFgBXRb-1XQAAAA.6Ym224Vwr9AniBeq6gL8OM9u4vGnUB_vbEUVjWojg14',
                   Timeout__c = 120000
                   );
    }
     @istest static void test_method1(){
        Test.startTest();




        Booking_Unit__c bu = [select id,name from Booking_Unit__c limit 1];
        Account acc = [select id,name from account limit 1];
        Test.setCurrentPage(Page.Customer);
        ApexPages.currentPage().getParameters().put('view', 'unitdetail');
        UnitDetailController objUDC = new UnitDetailController();
        objUDC.unit = bu;
        objUDC.unitId = bu.Id;
        PageReference pageRef = Page.Customer;
      Test.setCurrentPage(pageRef);
      ApexPages.currentPage().getParameters().put('id', bu.Id);
      objUDC.initializeVariables();

        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );
        //UnitDetailController.getStatementOfAccount(bu.Id,'SOA');
        UnitDetailController.getStatementOfAccount(bu.Id,'FM');
        UnitDetailController.chargesDetails('52000');

        /*SOAPCalloutServiceMock.returnToMe = new Map<String, GenerateSOPService.GeneratePenaltyStatmentResponse_element>();
        GenerateSOPService.GeneratePenaltyStatmentResponse_element response1 = new GenerateSOPService.GeneratePenaltyStatmentResponse_element();
        response1.return_x = '{"Status":"S","PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=338396 and Request Id :40185767 ...","Message":"Process Completed Returning 1 Response Message(s)...","PARAM_ID":"78152","REQUEST_ID":"40185767","STAGE_ID":"338396","URL":"https://sftest.deeprootsurface.com/docs/e/40185767_78152_PENALTY.pdf"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        UnitDetailController.getStatementOfAccount(bu.Id,'Penalty');
      */
      CustomerCommunityUtils.customerAccountId = acc.Id;
      string paymentResult = UnitDetailController.doPay(bu.id,20000,'https://www.google.com', 1000, 19000);
      UnitDetailController.Paymentformsubmit(bu.id,20000,'https://www.google.com', 100);
        Test.stopTest();
    }

    @istest static void test_method2() {

        Booking_Unit__c bu = [select id,name from Booking_Unit__c limit 1];
        Account acc = [select id,name from account limit 1];
        Test.setCurrentPage(Page.Customer);
        ApexPages.currentPage().getParameters().put('view', 'unitdetail');
        UnitDetailController objUDC = new UnitDetailController();
        objUDC.unit = bu;
        objUDC.unitId = bu.Id;
        PageReference pageRef = Page.Customer;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', bu.Id);
        objUDC.initializeVariables();

        FmHttpCalloutMock.Response getAllUnitSoaInLangUrlResponse = new FmHttpCalloutMock.Response(200, 'Success', '   {  '  +
         '       "responseId": "3072019-10305298",  '  +
         '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         '       "status": "S",  '  +
         '       "responseMessage": "Process Completed",  '  +
         '       "elapsedTimeMs": 8497,  '  +
         '       "responseLines": [  '  +
         '           {  '  +
         '               "documentName": "DPSOA",  '  +
         '               "language": "EN",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
         '           },  '  +
         '           {  '  +
         '               "documentName": "DPSOA_AR",  '  +
         '               "language": "AR",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
         '           }  '  +
         '       ],  '  +
         '       "complete": true  '  +
         '  }  ');

         String UNIT_SOA_LANG_str = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.UNIT_SOA_LANG, new List<String> {bu.Id});

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
             UNIT_SOA_LANG_str => getAllUnitSoaInLangUrlResponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));

        Test.startTest();
        UnitDetailController.getStatementOfAccount(bu.Id,'SOA');
        Test.stopTest();

    }
}