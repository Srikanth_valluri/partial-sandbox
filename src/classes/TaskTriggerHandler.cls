/**********************************************************************************************************************
* Name               : TaskTriggerHandler                                                                             *
* Description        : This is a trigger handler class for Activities. Has the below functions.                       *
*                      - Assign task to the user based on the user extension number on the task.                      *
*                      - Encrypting the calling number on the task.                                                   *
*                      - Update activity counter on the inquiry.                                                      *
*                      - Update inquiry status and score on task getting assigned to the inquiry.                     *
* Created Date       : 12/01/2017                                                                                     *
* Created By         : NSI                                                                                            *
* --------------------------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE           COMMENTS                                                                   *
* 1.0         Vineet        12/01/2017     Initial Draft                                                              *
* 1.1         Sivasankar    12/03/2017     Updating the activity counter                                              *
* 1.2         Sivasankar    30/03/2017     Assign the 3Cx task to PC using extension                                  *
* 1.3         Sivasankar    04/04/2017     Update the Related to filed for Task - Inquiry                             *
* 1.4         Vineet        18/06/2017     Update the Last Activity date on Inquiry.                                  *         
* 1.5         Rahul-Godara  06/07/2017     Added code to populate activity type 3 fields as that of the type field.   *
* 1.6         Vineet        25/07/2017     Commented lines 34-36 to not assign the tasks to the inquiry real time,    * 
*                                          as now the bach class is running every 5 minutes,                          *
*                                          and we started to get non selective query errors.
* 1.7         Bhanu Gupta   11/12/2018     Updated the Code to stamp First Activity Date on Inquiry                   *
**********************************************************************************************************************/
public class TaskTriggerHandler implements TriggerFactoryInterface{
    
    private final String MEETING_TYPE_1 = 'Face to Face';  
    private final String MEETING_TYPE_2 = 'Visit to Sales Office';
    private final String TASK_TYPE_1 = 'Call';
    private final String HOT_SCORE = 'Hot';
    private final String WARM_SCORE = 'Warm';
        
    /*********************************************************************************************
    * @Description : Method to contain logic to be executed before insert.                       *
    * @Params      : List<sObject>                                                               *
    * @Return      : void                                                                        *
    * @ChangeLog   : v1.2, v1.3                                                                  *
    *********************************************************************************************/
    public void executeBeforeInsertTrigger(List<sObject> newRecordsList){
        try{
            /* Calling method to assign the task to the users based on the extension passed by CTI. */
            assignTasktoExtNoUser(newRecordsList);
            //if(UserInfo.getName().containsIgnoreCase('CTI')){
            //  TasksToInquiryMapping.mapTaskToInquiry(newRecordsList, false);
            //}
            /* Calling method to encrypt the calling number. */
            for(Task thisTask : (List<Task>) newRecordsList){
                if(String.isNotBlank(thisTask.Calling_Number__c)){
                    thisTask.Encrypt_Calling_Number__c = UtilityHelperCls.encryptMobile(thisTask.Calling_Number__c);
                }
                if(String.isBlank(thisTask.Activity_Type_3__c)){
                    thisTask.Activity_Type_3__c = thisTask.type;
                }
            }
        }catch(Exception ex){
            system.debug('#### Exception at line number = '+ex.getLineNumber()+' , Exception message = '+ex.getMessage());
        }
    }
    
    /*********************************************************************************************
    * @Description : Method to contain logic to be executed before update.                       *
    * @Params      : Map<Id, sObject>, Map<Id, sObject>                                          *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeBeforeUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){
        try{
            /* Calling method to encrypt the calling number. */
            for(Id thisTask : newRecordsMap.keySet()){
                Task newRecord = (Task)newRecordsMap.get(thisTask);
                Task oldRecord = (Task)oldRecordsMap.get(thisTask);
                if(String.isNotBlank(newRecord.Calling_Number__c) && 
                   String.isNotBlank(oldRecord.Calling_Number__c) && 
                   newRecord.Calling_Number__c != oldRecord.Calling_Number__c){
                    newRecord.Encrypt_Calling_Number__c = UtilityHelperCls.encryptMobile(newRecord.Calling_Number__c);  
                }
                if(String.isBlank(newRecord.Activity_Type_3__c)){
                    newRecord.Activity_Type_3__c = newRecord.type;
                }
            }
        }catch(Exception ex){
            system.debug('#### Exception at line number = '+ex.getLineNumber()+', Exception message = '+ex.getMessage());
        }
    }
    
    /*********************************************************************************************
    * @Description : Method to contain logic to be executed after insert.                        *
    * @Params      : List<sObject>                                                               *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeAfterInsertTrigger(Map<Id, sObject> newRecordsMap){ 
        try{
            /* Calling method to update inquiry activity counter when a activity is created. */
            updateActivityCounter(newRecordsMap.values());
            updateActivityCounterByCurrentOwner(newRecordsMap.values());
            /* Calling method to update inquiry status when a activity is created. */
            Activate_New_Assignment_Process__c process = Activate_New_Assignment_Process__c.getInstance(UserInfo.getUserID ());
            
            if(process.Activate_Inq_Status_Update_from_Task_Trg__c)
                updateInquiryStatus(newRecordsMap.values());
            if(UserInfo.getName().containsIgnoreCase('CTI') || system.Test.isRunningTest()){
                updateOneHourBreakTimeofUsers(DAMAC_Constants.userExtForBreak); 
            }
            /* Calling method to populate the associated campaign on the inquiry. */
            updateInquiryAssociatedCampaign(newRecordsMap, new Map<Id, sObject>());
             // Execute this only for Telesales Team Lightning Profile
            Id profileId= userinfo.getProfileId();
            String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
            system.debug('ProfileName'+profileName);
            
            
            
            if(process.Activate_Inq_Status_Update_from_Task_Trg__c 
                && (profileName == 'Telesales Team Lightning Profile' || profileName == 'System Administrator' || profileName =='Ameyo CTI' ))
            {
                /* Calling method to update inquiry status when a Ameyo Disposition is present on task */
                updateInquiryStatusByDisposition(newRecordsMap.values());
            }
        }catch(exception ex){
            system.debug('#### Exception at line number = '+ex.getLineNumber()+' , Exception message = '+ex.getMessage());
        }
    }    
    
    /*********************************************************************************************
    * @Description : Method to contain logic to be executed after update.                        *
    * @Params      : Map<Id, sObject>, Map<Id, sObject>                                          *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeAfterUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){
        try{
            /* Calling method to update inquiry activity counter when a activity is created. */
            updateActivityCounter(newRecordsMap.values());
            updateActivityCounterByCurrentOwner(newRecordsMap.values());
            /* Calling method to update inquiry status when a activity is created. */
            //updateInquiryScore(newRecordsMap, oldRecordsMap);
            
            // Execute this only for Telesales Team Lightning Profile
            Id profileId= userinfo.getProfileId();
            String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
            system.debug('ProfileName'+profileName);
            
            Activate_New_Assignment_Process__c process = Activate_New_Assignment_Process__c.getInstance(UserInfo.getUserID ());
            
            if(process.Activate_Inq_Status_Update_from_Task_Trg__c 
                && (profileName == 'Telesales Team Lightning Profile' || profileName == 'System Administrator' || profileName =='Ameyo CTI')){
            /* Calling method to update inquiry status when a Ameyo Disposition is present on task */
                updateInquiryStatusByDisposition(newRecordsMap.values());
            }
        }catch(exception ex){
            system.debug('#### Exception at line number = '+ex.getLineNumber()+' , Exception message = '+ex.getMessage());
        }   
    }
    
    
    /*********************************************************************************************
    * @Description : Update inquiry status to 'Active' when an associated activity is created,   *
    *                Update the inquiry score to 'Hot' whenever a meeting of type is created:    *
    *                - Face to Face                                                              *
    *                - Visit to Sales                                                            *
    * @Params      : List<sObject>                                                               *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void updateInquiryStatus(List<sObject> newRecordsList){
        Map<Id, List<Task>> parentIdTaskListMap = new Map<Id, List<Task>>();
        List<Inquiry__c> updateInquiryList = new List<Inquiry__c>(); 
        for(sObject thisRecord : newRecordsList){            
            Task thisTask = (Task) thisRecord;
            if(thisTask.WhatId != null){
                if(parentIdTaskListMap.containsKey(thisTask.WhatId)){
                    parentIdTaskListMap.get(thisTask.WhatId).add(thisTask);     
                }else{
                    parentIdTaskListMap.put(thisTask.WhatId, new List<Task>{thisTask});
                }
            }
        }     
        if(!parentIdTaskListMap.isEmpty()){
            for(Inquiry__c thisInquiry : getEnquiryList(parentIdTaskListMap.keySet())){
                if(String.isNotBlank(thisInquiry.Inquiry_Status__c) && 
                    thisInquiry.Inquiry_Status__c.equalsIgnoreCase(DAMAC_Constants.INQUIRY_NEW_STATUS)){
                        thisInquiry.By_Pass_Validation__c = true;
                        thisInquiry.Inquiry_Status__c = DAMAC_Constants.INQUIRY_ACTIVE_STATUS;
                }
                if(parentIdTaskListMap.containsKey(thisInquiry.Id)){
                    for(Task thisTask : parentIdTaskListMap.get(thisInquiry.Id)){
                        if(String.isNotBlank(thisTask.Subject) && 
                           (thisTask.Subject.containsIgnoreCase(TASK_TYPE_1))){
                            thisInquiry.Inquiry_Score__c = HOT_SCORE;   
                            thisInquiry.Inquiry_Score_Last_Update__c = system.today();
                            break;
                        }       
                    }   
                } 
                updateInquiryList.add(thisInquiry);
            }   
            if(!updateInquiryList.isEmpty()){
                update updateInquiryList;
            }       
        }   
    }
    
    
    // Ameyo Dispoition - Subhash Oct 13 2019
    public void updateInquiryStatusByDisposition(List<sObject> newRecordsList){
        Map<Id, List<Task>> parentIdTaskListMap = new Map<Id, List<Task>>();
        List<Inquiry__c> updateInquiryList = new List<Inquiry__c>(); 
        
        map<string,string> AmeyoDispistionMapping = new map<string,string>();
        map<string,string> AmeyoDispistionMappingActivityOutcome = new map<string,string>();
        
        map<id,string> inquiryStatusMap = new map<id,string>();
        map<id,string> inquiryStatusMapByActvityOutcome = new map<id,string>();
        
        for(Ameyo_Dispositions_Inquiry_Status__mdt dispositionMtd:[select id,developerName,MasterLabel,Inquiry_Status__c,Activity_outcome__c,Ameyo_Disposition_Name__c from Ameyo_Dispositions_Inquiry_Status__mdt]){
            AmeyoDispistionMapping.put(dispositionMtd.Ameyo_Disposition_Name__c,dispositionMtd.Inquiry_Status__c);   
            AmeyoDispistionMappingActivityOutcome.put(dispositionMtd.Activity_outcome__c,dispositionMtd.Inquiry_Status__c);   
        }
        system.debug('AmeyoDispistionMapping-->'+AmeyoDispistionMapping);
        system.debug('AmeyoDispistionMapping-outcome-->'+AmeyoDispistionMappingActivityOutcome);
        for(sObject thisRecord : newRecordsList){            
            Task thisTask = (Task) thisRecord;
            if(thisTask.WhatId != null){
                if(parentIdTaskListMap.containsKey(thisTask.WhatId)){
                    parentIdTaskListMap.get(thisTask.WhatId).add(thisTask);     
                }else{
                    parentIdTaskListMap.put(thisTask.WhatId, new List<Task>{thisTask});
                }
            }
            if(thisTask.Ameyo_Dialer_Disposition__c != NULL &&  thisTask.Activity_Outcome__c == NULL){
                inquiryStatusMap.put(thisTask.WhatId,AmeyoDispistionMapping.get(thisTask.Ameyo_Dialer_Disposition__c)); 
            }
            if(thisTask.Activity_Outcome__c != NULL){
                inquiryStatusMapByActvityOutcome.put(thisTask.WhatId,AmeyoDispistionMappingActivityOutcome.get(thisTask.Activity_Outcome__c)); 
            }
        }   
       
        List <String> categories = new List <String> ();
        List <ID> inqIds = new List <ID> ();
        if(!parentIdTaskListMap.isEmpty()){
            for(Inquiry__c thisInquiry : getEnquiryListAll(parentIdTaskListMap.keySet())){

                if( thisInquiry.RecordType.name == 'Pre Inquiry'){
                    
                    if(String.isNotBlank(inquiryStatusMap.get(thisInquiry.id))){
                            thisInquiry.By_Pass_Validation__c = true;
                            thisInquiry.Inquiry_Status__c = inquiryStatusMap.get(thisInquiry.id);
                    }
                    //Overwrite with Outcome
                    if(String.isNotBlank(inquiryStatusMapByActvityOutcome.get(thisInquiry.id))){
                            thisInquiry.By_Pass_Validation__c = true;
                            thisInquiry.Inquiry_Status__c = inquiryStatusMapByActvityOutcome.get(thisInquiry.id);
                    }
                    if (Test.isRunningTest()){
                        inqIds.add (thisInquiry.id);
                    }
                    if (thisInquiry.Inquiry_Status__c == 'Qualified') {
                        
                        inqIds.add (thisInquiry.id);
                        if (thisInquiry.Mobile_CountryCode__c != NULL) {
                            categories.add (thisInquiry.Mobile_CountryCode__c);
                        }
                        updateInquiryList.add(thisInquiry);
                    } else {
                        updateInquiryList.add(thisInquiry);
                    }
                }
            }   
            if(!updateInquiryList.isEmpty()){
                TriggerFactoryCls.BYPASS_UPDATE_TRIGGER = true;
                update updateInquiryList; // This list contains the inquiries whos status is not qualified           
            }    
            // For Owner assignment
            if (inqIds.size () > 0) {
                updateInquiryStatus (inqIds, Categories);
            }    
        }   
    }
     public static string getAllfields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        if (objectType == null) {
            return fields;
        }
        for (string f :objectType.getDescribe ().fields.getMap ().keySet ())
            fields += f+ ', ';
        return fields.removeEnd(', '); 
        
    }
    
    @Future
    public static void updateInquiryStatus (List <ID> preInquiryIds, List<String> categories) {

        Activate_New_Assignment_Process__c activateNewProcess = Activate_New_Assignment_Process__c.getInstance(UserInfo.getUserID());
        if (activateNewProcess.Activate__c || Test.isRunningTest()) {
            List <Inquiry__c> inqList = Database.query ('SELECT '+getAllfields('Inquiry__c')+' FROM Inquiry__c WHERE ID IN :preInquiryIds');
            List <Inquiry__c> newInquiriesList = new List <Inquiry__c> ();
            Map <String, String> categoryUpdate = new Map <String, String> ();
            
            if (categories.size () > 0) {
                
                for (Inquiry_Country_Code_Categories__c category : [SELECT Name, Category_Update__c FROM Inquiry_Country_Code_Categories__c 
                                                                    WHERE Name IN :categories]) {
                    categoryUpdate.put (category.Name, category.Category_Update__c);
                }
            }
            for (Inquiry__c newInq :inqList) {
                
                
                if (newInq.Mobile_CountryCode__c != NULL) {
                    if (categoryUpdate.containsKey (newInq.Mobile_CountryCode__c)) 
                        newInq.Rule_For_HOD__c = categoryUpdate.get (newInq.Mobile_CountryCode__c);
                } 
                newInquiriesList.add (newInq);
            }
            
            
            /*
            PreInquiryGetEligiblePCS getPcs = new PreInquiryGetEligiblePCS();
            List<Inquiry__c> newInquiriesUpdateList = new List <Inquiry__c> ();
            newInquiriesUpdateList = getPcs.getEligiblsPCS(newInquiriesList, 'Update');*/
            //TriggerFactoryCls.BYPASS_UPDATE_TRIGGER = true;
            try {
                update newInquiriesList ;
            } catch (Exception e) {
                System.Debug ('Exception ::::'+e.getMessage());
            }
        }
       
    }   
    
    
    /*********************************************************************************************
    * @Description : Method to get inquiry details.                                              *
    * @Params      : Set<Id>                                                                     *
    * @Return      : List<Inquiry__c>                                                            *
    *********************************************************************************************/
    @TestVisible private List<Inquiry__c> getEnquiryList(Set<Id> enquiryIdsSet){
        return ([SELECT Id, Name, Inquiry_Status__c, Inquiry_Score__c 
                 FROM Inquiry__c 
                 WHERE Id IN: enquiryIdsSet AND 
                       Inquiry_Status__c != null AND 
                       (Inquiry_Status__c =: DAMAC_Constants.INQUIRY_NEW_STATUS OR 
                        Inquiry_Status__c =: DAMAC_Constants.INQUIRY_ACTIVE_STATUS)]);       
    }
    
    @TestVisible private List<Inquiry__c> getEnquiryListAll(Set<Id> enquiryIdsSet){
        return ([SELECT Id, Name, Inquiry_Status__c, Inquiry_Score__c,RecordType.name 
                 FROM Inquiry__c 
                 WHERE Id IN: enquiryIdsSet]);       
    }
    
    /*********************************************************************************************
    * @Description : Method to update the activity counter value.                                *
    * @Params      : List<sObject>                                                               *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public static void updateActivityCounter(List<sObject> oldNewrecordsList){
        System.debug('== updateActivityCounter ==');
        String inquiryId = '';
        String currentTaskId = '';
        String inquiryPrefix = DamacUtility.getObjectPrefix('Inquiry__c');
        Integer completedEvents = 0, completedTasks = 0, activeEvents = 0, activeTasks = 0 , currentownerTasks = 0;
        Set<Id> inquiryRecordIdsSet = new Set<Id>();
        List<Inquiry__c> updateInquiries = new List<Inquiry__c>();
        for(sObject activityObj : oldNewrecordsList){
            inquiryId = (String)activityObj.get('WhatId'); 
            //System.debug('== (String)activityObj =='+(String)activityObj.get('Id'));
            currentTaskId = (String)activityObj.get('Id');
            if(String.isNotBlank(inquiryPrefix) && 
               String.isNotBlank(inquiryId) && 
               inquiryId.startsWith(inquiryPrefix)){
                inquiryRecordIdsSet.add(inquiryId);
            }
        }
        if(!inquiryRecordIdsSet.isEmpty()){
            for(Inquiry__c inquiry : [SELECT Id, Activity_Counter__c, Inquiry_Status__c,First_Activity_Date__c,Completed_Activities_Counter__c, All_Activities_Counter__c, 
                                            (SELECT Id, Status__c FROM Events ORDER BY Status__c ASC), 
                                            (SELECT Id, isInquiry__c ,whatId, CreatedDate,Status FROM Tasks) 
                                      FROM Inquiry__c 
                                      WHERE Id IN: inquiryRecordIdsSet]){


                completedEvents = 0;
                completedTasks=0;
                activeEvents = 0;
                activeTasks = 0;
                for(Event thisEvent : inquiry.Events){
                    if(String.isNotBlank(thisEvent.Status__c)){
                        if(thisEvent.Status__c.containsIgnoreCase(DAMAC_Constants.EVENT_COMPLETED_STATUS)){
                            completedEvents ++;
                        }
                        if(thisEvent.Status__c.containsIgnoreCase(DAMAC_Constants.EVENT_PLANNED_STATUS)){
                            activeEvents ++;    
                        }
                    }
                }
                for(Task thisTask : inquiry.Tasks){
                    if(String.isNotBlank(thisTask.Status)){
                        if(thisTask.Status.containsIgnoreCase(DAMAC_Constants.EVENT_COMPLETED_STATUS)){
                            completedTasks ++;
                        }
                        if(DAMAC_Constants.ACTIVE_TASK_STATUS.containsIgnoreCase(thisTask.Status)){
                            activeTasks ++;
                        }
                    }
                }
                Inquiry__c inqObjToUpdate = new Inquiry__c();

                if((inquiry.Activity_Counter__c != (activeEvents + activeTasks + completedEvents + completedTasks)) || 
                   (inquiry.Completed_Activities_Counter__c != (completedEvents + completedTasks)) || 
                   (inquiry.All_Activities_Counter__c != (inquiry.Events.size() + inquiry.Tasks.size()))){
                    inqObjToUpdate.id = inquiry.Id; 
                    //inqObjToUpdate.Activity_Counter__c = ((inquiry.Events.size() - completedEvents)  + (inquiry.Tasks.size() - completedTasks)); 
                    inqObjToUpdate.Completed_Activities_Counter__c = (completedEvents + completedTasks);
                    inqObjToUpdate.All_Activities_Counter__c = (inquiry.Events.size() + inquiry.Tasks.size());
                    inqObjToUpdate.By_Pass_Validation__c = true;
                    inqObjToUpdate.Last_Activity_Date__c = system.now();
                    /*updateInquiries.add(new Inquiry__c(Id = inquiry.Id, 
                                                       Activity_Counter__c = ((inquiry.Events.size() - completedEvents)  + (inquiry.Tasks.size() - completedTasks)), 
                                                       Completed_Activities_Counter__c = (completedEvents + completedTasks), 
                                                       All_Activities_Counter__c = (inquiry.Events.size() + inquiry.Tasks.size()), 
                                                       By_Pass_Validation__c = true,
                                                       Last_Activity_Date__c = system.now()));*/
                }
                //1.7 Added by Bhanu Gupta
                if(inquiry.Inquiry_Status__c != 'Closed Won' && inquiry.Inquiry_Status__c != 'Closed Lost'
                    && inquiry.First_Activity_Date__c == null){
                    list<Task> lstInqTask= new list<Task>();
                    lstInqTask = inquiry.Tasks;
                    System.debug('== lstInqTask =='+lstInqTask);
                    System.debug('== lstInqTaskSize =='+lstInqTask.size());
                    //&& lstInqTask.contains(currentTaskId);
                    if (!(lstInqTask).isEmpty() && lstInqTask.size() == 1 ){
                        for(Task objInqTask : lstInqTask){
                            if(objInqTask.isInquiry__c && objInqTask.whatId == inquiry.id ){
                                if(String.isBlank(inqObjToUpdate.id)){
                                    inqObjToUpdate.id = inquiry.Id;
                                    inqObjToUpdate.First_Activity_Date__c = objInqTask.CreatedDate;
                                }else{
                                    inqObjToUpdate.First_Activity_Date__c = objInqTask.CreatedDate;
                                }
                            }
                        }
                        System.debug('== updateInquiries =='+updateInquiries);
                    }
                }

                updateInquiries.add(inqObjToUpdate);
            }
            if(!updateInquiries.isEmpty()){
                TriggerFactoryCls.BYPASS_UPDATE_TRIGGER = true;
                update updateInquiries;
            }
        }
    }
    
    
    /*Update Activity Counter on Real Time
        Stores Counter Aggregates in Actvity Counter Object    
    */
    
    public static void updateActivityCounterByCurrentOwner(List<sObject> oldNewrecordsList){
        String inquiryId = '';
        String currentTaskId = '';
        String inquiryPrefix = DamacUtility.getObjectPrefix('Inquiry__c');
        Integer currentownerTasks = 0;
        Set<Id> inquiryRecordIdsSet = new Set<Id>();
        for(sObject activityObj : oldNewrecordsList){
            inquiryId = (String)activityObj.get('WhatId');             
            currentTaskId = (String)activityObj.get('Id');
            if(String.isNotBlank(inquiryPrefix) && 
               String.isNotBlank(inquiryId) && 
               inquiryId.startsWith(inquiryPrefix)){
                inquiryRecordIdsSet.add(inquiryId);
            }
        }
        
        if(!inquiryRecordIdsSet.isEmpty()){
            list<activity_counter__c> NewCounterToInsert = new list<activity_counter__c>();
            list<aggregateResult> ar = [select count(id)countval,whatid inq,ownerid own from task where whatId IN:inquiryRecordIdsSet group by whatId,ownerid];
            for(aggregateResult a:ar){
                activity_counter__c ac = new activity_counter__c();
                ac.Count__c = integer.valueof(a.get('countval'));
                ac.Inquiry__c = string.valueof(a.get('inq'));
                ac.Inquiry_Owner__c = string.valueof(a.get('own'));
                ac.Unique_Key__c = string.valueof(a.get('inq'))+string.valueof(a.get('own'))+'By Owner';
                ac.type_of_counter__c = 'By Owner';
                NewCounterToInsert.add(ac);
            }
            upsert NewCounterToInsert Unique_Key__c;
        }
    
    }
    
    
    
    
    
    /*********************************************************************************************
    * @Description : Method to update inquiry Associated Campaign based on task and campaign     *
    * @Params      : Map<Id, sObject>, Map<Id, sObject>                                          *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void updateInquiryAssociatedCampaign(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){
        
        String campaignNumber;
        String TaskInquiryId;
        String inquiryPrefix = Inquiry__c.sObjectType.getDescribe().getKeyPrefix();
        Set<Id> assoCampId;
        List<String> campaignNumberList = new List<String>();
        List<String> inquiryId = new List<String>();
        List<Campaign_Member__c> newCampaignMemberList = new List<Campaign_Member__c>();
        Map<Id, String> inquiryCampaignMap = new Map<Id,String>();
        Map<String, Id> campaignNumberMap = new Map<String, Id>();
        try{
            system.debug('#### => updating Inquiry Associated Campaign');
            for(Task thisTask : (List<Task>)newRecordsMap.values()){
                TaskInquiryId = (String)thisTask.WhatId;
                if(thisTask.WhatId!=null && TaskInquiryId.startsWith(inquiryPrefix)){
                    Task temp = (Task)oldRecordsMap.get(thisTask.Id); 
                    if(Trigger.isInsert || (Trigger.isUpdate && thisTask.Campaign_Number__c!=temp.Campaign_Number__c)){
                        campaignNumberList.add(thisTask.Campaign_Number__c);
                        inquiryId.add(thisTask.WhatId); 
                        inquiryCampaignMap.put(thisTask.WhatId, thisTask.Campaign_Number__c);
                    }
                }
            }
            if(!campaignNumberList.isEmpty()){
                for(JO_Campaign_Virtual_Number__c cvn: [SELECT Id, Related_Campaign__c, Related_Virtual_Number__c, Related_Virtual_Number__r.Name, 
                                                               Related_Campaign__r.CreatedDate 
                                                        FROM JO_Campaign_Virtual_Number__c 
                                                        WHERE Related_Virtual_Number__r.Name IN: campaignNumberList AND 
                                                              Related_Campaign__r.Marketing_Start_Date__c <= TODAY AND 
                                                              Related_Campaign__r.Marketing_End_Date__c >= TODAY 
                                                        ORDER BY Related_Campaign__r.CreatedDate DESC]){
                    if(!campaignNumberMap.containsKey(cvn.Related_Virtual_Number__r.Name)){
                        campaignNumberMap.put(cvn.Related_Virtual_Number__r.Name, cvn.Related_Campaign__c); 
                    }
                } 
            }
            if(!inquiryCampaignMap.isEmpty() && !campaignNumberMap.isEmpty()){
                for(Inquiry__c thisInquiry : [SELECT Id, Campaign__c,
                                                     (SELECT Campaign__c, Inquiry__c FROM Associated_Campaigns__r) 
                                              FROM Inquiry__c 
                                              WHERE Id IN :inquiryCampaignMap.keySet()]){
                    campaignNumber = inquiryCampaignMap.get(thisInquiry.Id);
                    if(thisInquiry.Campaign__c != campaignNumberMap.get(campaignNumber)){
                        assoCampId = new Set<Id>();
                        for(Campaign_Member__c assoCamp : thisInquiry.Associated_Campaigns__r){
                            assoCampId.add(assoCamp.Campaign__c);
                        }
                        if(!assoCampId.contains(campaignNumberMap.get(campaignNumber))){
                            Campaign_Member__c newCampaignMember = new Campaign_Member__c();
                            newCampaignMember.Campaign__c = campaignNumberMap.get(campaignNumber);
                            newCampaignMember.Inquiry__c = thisInquiry.Id;
                            newCampaignMemberList.add(newCampaignMember); 
                        }
                    }
                }
            } 
            if(!newCampaignMemberList.isEmpty()){
                insert newCampaignMemberList;
            }   
        }catch(Exception ex){
            system.debug('#### Exception at line number = '+ex.getLineNumber()+' , Exception message = '+ex.getMessage());
        }
    }
    
    /*********************************************************************************************
    * @Description : Method to Update the Activity Counter value                                 *
    * @Params      : List<sObject>                                                               *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public static void assignTasktoExtNoUser(List<sObject> newRecordsList){
        Set<String> userExtNos = new Set<String>();
        Map<String,ID> mapExtofUserID = new Map<String,ID>();
        for(sObject thisActivity : newRecordsList){
            if(String.isNotBlank((String)thisActivity.get('User_Ext_No__c'))){
                userExtNos.add((String)thisActivity.get('User_Ext_No__c'));
            }
        }
        if(!userExtNos.isEmpty()){
            for(User thisExtUser : [SELECT Id, Name, Extension FROM USER WHERE Extension IN:userExtNos AND IsActive = true]){
                mapExtofUserID.put(thisExtUser.Extension,thisExtUser.ID);
            }
            for(sObject thisActivity : newRecordsList){
                if(String.isNotBlank((String)thisActivity.get('User_Ext_No__c')) && 
                   mapExtofUserID != null && mapExtofUserID.containsKey((String)thisActivity.get('User_Ext_No__c'))){
                    thisActivity.put('OwnerId',mapExtofUserID.get((String)thisActivity.get('User_Ext_No__c')));
                }
            }
        }
    }
    
    /*********************************************************************************************
    * @Description : Method to update one hour break time on users.                              *
    * @Params      : Set<String>                                                                 *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    @future 
    public static void updateOneHourBreakTimeofUsers(Set<String> userExts){
        DAMAC_Constants.userExtForBreak = userExts;
        if(!DAMAC_Constants.userExtForBreak.isEmpty()){
            List<User> updateBreakTime = new List<User>();
            for(User thisUser : [SELECT Id, Break_Time__c FROM USER WHERE Extension IN: DAMAC_Constants.userExtForBreak AND IsActive = true]){
                updateBreakTime.add(new User(Id=thisUser.Id, Break_Time__c=System.now()));
            }
            if(updateBreakTime != null && !updateBreakTime.isEmpty() && updateBreakTime.size() > 0 ){
                update updateBreakTime;
            }
        }
    }
    
    // TOBE Implemented
    public void executeBeforeInsertUpdateTrigger(List<sObject> newRecordsList, Map<Id,sObject> oldRecordsMap){ }
    public void executeBeforeDeleteTrigger(Map<Id,sObject> oldRecordsMap){ }
    public void executeAfterInsertUpdateTrigger(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){ }
    public void executeAfterDeleteTrigger(Map<Id,sObject> oldRecordsMap){ }
}//End of class.