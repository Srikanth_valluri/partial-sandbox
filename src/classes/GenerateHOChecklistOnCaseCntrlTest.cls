/**
 * @File Name          : GenerateHOChecklistOnCaseCntrlTest.cls
 * @Description        : Test class for the apex class GenerateHOChecklistOnCaseCntrl
 * @Author             : Raveena Jain
 * @Group              :
 * @Last Modified By   : Raveena Jain
 * @Last Modified On   : 
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0        27/09/2020            Raveena Jain                 Initial Version
**/

@isTest
public class GenerateHOChecklistOnCaseCntrlTest {
    
    @isTest
    static void GenerateHOChecklistOnCaseTest(){
        
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        system.assertNotEquals(null,objSR);
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        insert objBooking;
        system.assertNotEquals(null,objBooking);
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Unit_Name__c = 'test';
        objBU.Registration_ID__c = 'test';
        objBU.Booking__c = objBooking.Id;
        insert objBU;
        system.assertNotEquals(null,objBU);
        
        Case objCase = new Case();
        objCase.Age__c = '24';
        objCase.Booking_Unit__c = objBU.Id;
        insert objCase;
        system.assertNotEquals(null,objCase);
        
        SR_Attachments__c objSRAttachment = new SR_Attachments__c();
        objSRAttachment.Booking_Unit__c = objBU.Id;
        objSRAttachment.Attachment_URL__c = 'http://damacproperties.force.com/Documents/apex/GetFile?id=01NUEFKARODG2GF63DEBHKMCJBW4KFBPWV';
        objSRAttachment.Name = 'Handover Checklist';
        objSRAttachment.Case__c =  objCase.Id;
        Insert objSRAttachment;
        system.assertNotEquals(null,objSRAttachment);
        
        SR_Attachments__c objSRAttachment1 = new SR_Attachments__c();
        objSRAttachment1.Booking_Unit__c = objBU.Id;
        objSRAttachment1.Attachment_URL__c = '';
        objSRAttachment1.Name = 'Handover Checklist';  
        Insert objSRAttachment1;
        system.assertNotEquals(null,objSRAttachment1);
        
        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.standardController(objCase);
            GenerateHOChecklistOnCaseCntrl obj = new GenerateHOChecklistOnCaseCntrl(sc);
        Test.stopTest();
        
    }
}