@isTest
global class MockHttpAgentOTPController implements HttpCalloutMock {
    
    global HTTPResponse respond(HTTPRequest req) {
      
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('OK:23123');
        res.setStatusCode(200);
        return res;
    }
}