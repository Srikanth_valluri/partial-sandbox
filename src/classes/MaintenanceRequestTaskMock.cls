@isTest
global with sharing class MaintenanceRequestTaskMock implements WebServiceMock{
	global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType){
                wwwFsiCoUkServicesEvolution0409_t32.TaskServiceV3  calloutObj = new   wwwFsiCoUkServicesEvolution0409_t32.TaskServiceV3();
               schemasDatacontractOrg200407FsiConc_t32.BreakdownTaskDtoV3 authObj = new schemasDatacontractOrg200407FsiConc_t32.BreakdownTaskDtoV3();
               authObj.TaskId = Long.valueOf('897');
               wwwFsiCoUkServicesEvolution0409_t32.CreateBreakdownTaskResponse_element  calloutObj2 = new  wwwFsiCoUkServicesEvolution0409_t32.CreateBreakdownTaskResponse_element();
               calloutObj2.CreateBreakdownTaskResult = authObj;
               response.put('responsenew_x', calloutObj2);  
          }
}