/**
 * @File Name          : FmcMoveInSRComponentController.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 1/15/2020, 4:27:25 PM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    1/15/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
 * 2.0    9/23/2020   Komal Shitole                Uncommented methods as values were not getting prepopulated after save as draft
 * 3.0   12/23/2020   Rehan Shaik				   Added Move In record type to be retrieved in FM Case SOQL query
**/
public without sharing class FmcMoveInSRComponentController extends MoveInSRComponentController{
    public List<SelectOption>  units                                        {get; set;}
    public Boolean continueInit                                             {get; set;} 
    //public map<String, list<FM_Additional_Detail__c>> mapAdditionalDetails  {get;set;}
    public String termsLink                                                 {get; set;}

    private static String CASE_ALREADY_SUBMITTED_MESSAGE    = 'This Request is already Submitted';
    private static String CASE_ALREADY_CLOSED_MESSAGE       = 'This Request is already Closed';
    private static String INVALID_CASE_ID                   = 'Invalid Request Initiation';
    private static String INVALID_UNIT_INITIATION           = 'Invalid Request Initiation';

    //@testVisible protected Integer intNoOfAddDetails = 0;

    public FmcMoveInSRComponentController() {
        super(false);
        if (FmcUtils.isCurrentView('MoveIn')) {
            initVariables();
            continueInit = true;
            strSRType = 'Move_in_Request';
            if ('Guest'.equalsIgnoreCase(UserInfo.getUserType())) {
                initForGuestUser();
            } else {
                initVariables();

                System.debug('--- lstDocuments --- : '+lstDocuments);
                //removeSafetyRegulationsDoc();

                Map<String, String> params = ApexPages.currentPage().getParameters();
                fmCaseId = params.get('id');
                if(String.isBlank(fmCaseId)) {
                    strAccountId = CustomerCommunityUtils.customerAccountId;
                } else {
                    retrieveDraftSr();
                    objFMCase = FM_Utility.getCaseDetails( fmCaseId );
                    strAccountId = objFMCase.Account__c;
                    objBU = FM_Utility.getUnitDetails(objFmCase.Booking_Unit__c);
                    units = new List<SelectOption> {
                        new SelectOption(strUnitId, objBU.Unit_Name__c, true) };
                }
                System.debug('---objFMCase---: '+objFMCase);
                System.debug('---objFMCase Id---: '+objFMCase.id);
                if(objFMCase != NULL) {
                    fmCaseId = objFMCase.id;
                }
                System.debug('---fmCaseId---: '+fmCaseId);
                strSRType = 'Move_in_Request';
                initiateVariables();
                //insertCase();
                if(objFMCase != NULL && objFMCase.id != NULL) {
                    termsLink = '/apex/safetyregulationspage?CaseId=' + objFMCase.Id;
                }
                System.debug('--- termsLink --- : '+termsLink);
                removeSafetyRegulationsDoc();
            }
        }
    }

    // public override String createAlreadySubmittedErrorMessage(List<FM_Case__c> lstSubmittedCases) {
    //     return 'This Request is already Submitted';
    // }

    private void initForGuestUser() {
        fmCaseId = ApexPages.currentPage().getParameters().get('Id');
        // URL has Case Id
        if (String.isNotBlank(fmCaseId)) {
            List<FM_Case__c> lstCase = [
                SELECT      Id
                            , Status__c
                FROM        FM_Case__c
                WHERE       Id = :fmCaseId
                        AND Status__c IN ('New', 'Closed', 'Draft Request', 'Awaiting Correction', 'Submitted')
                        AND RecordType.Name = 'Move In'
                        AND Origin__c = 'Portal - Guest'
                        //AND CreatedById = :UserInfo.getUserId()
                ORDER BY    Status__c DESC
            ];
            if (!lstCase.isEmpty()) {
                if (caseIsSubmitted(lstCase[0])) {
                    continueInit = false;
                    return;
                } else {
                    Boolean alreadyClosedCase = false;
                    for (FM_Case__c fmCase : lstCase) {
                        if ('Closed'.equalsIgnoreCase(fmCase.Status__c)) {
                            alreadyClosedCase = true;
                        }
                    }
                    if (alreadyClosedCase) {
                        ApexPages.addMessage(new ApexPages.message(
                                ApexPages.Severity.ERROR, CASE_ALREADY_CLOSED_MESSAGE));
                        continueInit = false;
                    }
                }
                retrieveDraftSr(); //Uncommented on 23/09/2020
                initCaseAndUnit(lstCase[0].Id); //Uncommented on 23/09/2020
                //strAccountId = objBU.Booking__r.Account__c;
                System.debug('objFMCase.Id = ' + objFMCase.Id);
                System.debug('strUnitId = ' + strUnitId);
                System.debug('strAccountId = ' + strAccountId);
                System.debug('strSRType = ' + strSRType);
                //initialize();
            } else {
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, INVALID_CASE_ID));
            }
        } else {
            // URL doesn't have caseId
            strUnitId = ApexPages.currentPage().getParameters().get('unitId');
            if (String.isBlank(strUnitId)) {
                ApexPages.addMessage(new ApexPages.message(
                        ApexPages.Severity.ERROR, INVALID_UNIT_INITIATION));
                continueInit = false;
                return;
            }
            // URL has a unitId
            List<Booking_Unit__c> lstUnit = [ SELECT Id
                                              FROM Booking_Unit__c
                                              WHERE Id = :strUnitId ];
            // if invalid unit id
            if (lstUnit.isEmpty()) {
                ApexPages.addMessage(new ApexPages.message(
                        ApexPages.Severity.ERROR, INVALID_UNIT_INITIATION));
                continueInit = false;
                return;
            }
            // Valid Unit Id
            objBU = FM_Utility.getUnitDetails(strUnitId);
            units = new List<SelectOption>{
                            new SelectOption(strUnitId, objBU.Unit_Name__c, true) };
            strAccountId = objBU.Booking__r.Account__c;
            //strAccountId = NULL;

            // Search existing draft or rejected(Needs Correction) Case
            System.debug('strUnitId = ' + strUnitId);
            System.debug('UserInfo.getUserId() = ' + UserInfo.getUserId());

            List<FM_Case__c> lstCase = [ SELECT Id
                                                , Status__c
                                         FROM FM_Case__c
                                         WHERE Booking_Unit__c = :strUnitId
                                         //AND CreatedById = :UserInfo.getUserId()
                                         AND Status__c IN ('New', 'Closed', 'Draft Request', 'Awaiting Correction', 'Submitted')
                                         AND RecordType.Name = 'Move In'
                                         AND Origin__c = 'Portal - Guest'
                                         ORDER BY Status__c DESC
                                         LIMIT 1
                                       ];

            if (!lstCase.isEmpty()) {
                if (caseIsSubmitted(lstCase[0])) {
                    continueInit = false;
                    return;
                } else {
                    Boolean alreadyClosedCase = false;
                    for (FM_Case__c fmCase : lstCase) {
                        if ('Closed'.equalsIgnoreCase(fmCase.Status__c)) {
                            alreadyClosedCase = true;
                        }
                    }
                    if (alreadyClosedCase) {
                        ApexPages.addMessage(new ApexPages.message(
                                ApexPages.Severity.ERROR, CASE_ALREADY_CLOSED_MESSAGE));
                        continueInit = false;
                    }
                }
                //fmCaseId = lstCase[0].Id;
                //retrieveDraftSr();
                //initCase(lstCase[0].Id);
                //System.debug('objFMCase.Id = ' + objFMCase.Id);
                //System.debug('strUnitId = ' + strUnitId);
                //System.debug('strAccountId = ' + strAccountId);
                //System.debug('strSRType = ' + strSRType);
                //initialize();
            } else {
                // init will be called from component on doc ready
            }
        }
    }

    private static Boolean caseIsSubmitted(FM_Case__c fmCase) {
        if ('Submitted'.equalsIgnoreCase(fmCase.Status__c)) {
            ApexPages.addMessage(new ApexPages.message(
                    ApexPages.Severity.ERROR, CASE_ALREADY_SUBMITTED_MESSAGE));
            return true;
        }
        return false;
    }

    private void initCaseAndUnit(Id caseId) {
        FM_Case__c fmCase = initCase(caseId);
        initUnit(fmCase.Booking_Unit__c);
    }

    private FM_Case__c initCase(Id caseId) {
        System.debug('caseId = ' + caseId);
        fmCaseId = caseId;
        objFmCase = FM_Utility.getCaseDetails(fmCaseId);
        getMoveInDateString();
        System.debug('objFmCase = ' + JSON.serialize(objFmCase));
        return objFmCase;
    }

    private Booking_Unit__c initUnit(Id unitId) {
        strUnitId = unitId;
        if (String.isNotBlank(strUnitId)) {
            objBU = FM_Utility.getUnitDetails(strUnitId);
            units = new List<SelectOption>{
                new SelectOption(strUnitId, objBU.Unit_Name__c, true) };
        }
        return objBU;
    }

    public void initiateVariables() {
        units = new List<SelectOption>();
        units.add(new SelectOption('', '--None--'));
        for (Booking_Unit__c unit : FmcUtils.queryUnitsForAccount(
                    CustomerCommunityUtils.customerAccountId,
                    'Id, Name, Registration_ID__c, Unit_Name__c, Inventory__c, Inventory__r.Building_Location__c, '
                        + 'Inventory__r.Building_Location__r.Id, Inventory__r.Building_Location__r.Name, '
                        + 'Inventory__r.Building_Location__r.Building_Name__c, '
                        + 'Inventory__r.Building_Location__r.Loams_Email__c, '
                        + 'Inventory__r.Building_Location__r.Location_Code__c'
        )) {
            units.add(new SelectOption(unit.Id, unit.Unit_Name__c));
        }
        //initializeAddDetailsMap();
    }

    protected override void setCaseOrigin() {
        objFMCase.Origin__c = 'Guest'.equalsIgnoreCase(UserInfo.getUserType())
                                    ? 'Portal - Guest' : 'Portal';
    }

    public void getUnitDetails(){
        if (lstHouseholdDetails != NULL) {
            lstHouseholdDetails.clear();
        }

        System.debug('==strUnitId==' + strUnitId);
        try {
            List<Booking_Unit__c> lstUnit = [ SELECT Id
                                              FROM Booking_Unit__c
                                              WHERE Id = :strUnitId ];
            // if invalid unit id
            if (lstUnit.isEmpty()) {
                ApexPages.addMessage(new ApexPages.message(
                            ApexPages.Severity.ERROR, INVALID_UNIT_INITIATION));
                continueInit = false;
                return;
            }
            // Valid Unit Id
            objBU = FM_Utility.getUnitDetails(strUnitId);
            units = new List<SelectOption>{
                new SelectOption(strUnitId, objBU.Unit_Name__c, true) };
            strAccountId = objBU.Booking__r.Account__c;

            // Search existing draft or rejected(Needs Correction) Case
            List<FM_Case__c> lstCase = [ SELECT Id
                                                , Status__c
                                         FROM FM_Case__c
                                         WHERE Booking_Unit__c = :strUnitId
                                         //AND CreatedById = :UserInfo.getUserId()
                                         AND Status__c IN ('Awaiting Correction', 'Closed', 'Draft Request', 'Submitted')
                                         AND Origin__c IN ('Portal','Portal - Guest')
                                         AND RecordType.Name = 'Move In'								// 3.0
                                         ORDER BY Status__c DESC
                                         LIMIT 1 ];
            System.debug('objFMCase.Id = ' + objFMCase.Id);
            //New status removed from query

            if (!lstCase.isEmpty()) {
                if (caseIsSubmitted(lstCase[0])) {
                    continueInit = false;
                    return;
                } else {
                    Boolean alreadyClosedCase = false;
                    for (FM_Case__c fmCase : lstCase) {
                        if ('Closed'.equalsIgnoreCase(fmCase.Status__c)) {
                            alreadyClosedCase = true;
                        }
                    }
                    if (alreadyClosedCase) {
                        ApexPages.addMessage(new ApexPages.message(
                                ApexPages.Severity.ERROR, CASE_ALREADY_CLOSED_MESSAGE));
                        continueInit = false;
                    }
                }
                fmCaseId = lstCase[0].Id;
                initCase(fmCaseId);
            } else {
                // init will be called from component on doc ready
            }
            System.debug('objFMCase.Id = ' + objFMCase.Id);
            System.debug('strUnitId = ' + strUnitId);
            System.debug('strAccountId = ' + strAccountId);
            System.debug('strSRType = ' + strSRType);
            initialize();
            insertCase();
            System.debug('---objFMCase Id---: '+objFMCase.id);

            termsLink = '/apex/safetyregulationspage?CaseId=' + objFMCase.Id;
            removeSafetyRegulationsDoc();
        }//try
        catch(System.Exception excp) {
            //System.debug('Inside Catch Block Exception');
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                excp.getMessage() + excp.getLineNumber()));
        }
    }

    protected override void fetchLstDocuments(String query, Set<String> cityValues) {
        for(FM_Documents__mdt mdt : Database.query(query)) {
            if(!uploadedDocLst.contains(mdt.Document_Name__c)) {
                lstDocuments.add(mdt);
            }
        }
    }

    private void removeSafetyRegulationsDoc() {
        System.debug('>>>---termsLink--- : '+termsLink);
        List<FM_Documents__mdt> lstFmDocs = new List<FM_Documents__mdt>();
        lstFmDocs.addAll(lstDocuments);
        for(FM_Documents__mdt objFMDoc : lstFmDocs) {
            if(objFMDoc.Document_Name__c == 'Safety Undertaking Letter') {
                if(lstDocuments.contains(objFMDoc)) {
                    lstDocuments.remove(lstDocuments.indexOf(objFMDoc));
                }
            }
        }
        System.debug('--- lstDocuments --- : '+lstDocuments);
    }

    public override PageReference submit() {
      if(objFMCase.id != NULL){
          objFMCase = [ SELECT id
                            , Approval_Status__c
                            , Status__c
                            , Company__c
                            , Contractor__c
                            , Mobile_no_contractor__c
                            , Move_in_move_out_type__c
                            , Approving_Authorities__c
                     FROM FM_Case__c
                     WHERE id =:objFMCase.id ];
      }
      if(objFMCase.Id != NULL && (objFMCase.Approval_Status__c == 'Approved'
                                    || objFMCase.Status__c == 'Closed')) {
          PageReference pageRef = new PageReference('/'+objFMCase.Id);
          return pageRef;
      }
        if(objFMCase.Move_in_move_out_type__c == 'Self move-in') {
            objFMCase.Company__c = objFMCase.Contractor__c = objFMCase.Mobile_no_contractor__c = '';
        }
        objFMCase.Request_Type__c = 'Move In';
        objFMCase.Request_Type_DeveloperName__c = isTenant ? 'Move_in_Request_T' : 'Move_in_Request';
        objFMCase.Status__c = 'Submitted';
        objFMCase.Submitted__c = true;
        objFMCase.Submit_for_Approval__c = true;    //Uncommented this - 17/05/2020
        objFMCase.Approval_Status__c = 'Pending';
        //Create task for move-in document review
        try {
            System.debug('==strDate==' + strDate);
            List<String> lstDate = strDate.split('/');
            System.debug('==lstDate==' + lstDate);
            objFMCase.Move_in_date__c = Date.newinstance(Integer.valueOf(lstDate[2]),
            Integer.valueOf(lstDate[1]),Integer.valueOf(lstDate[0]));
            list<FM_Approver__mdt> lstApprovers = FM_Utility.fetchApprovers(
                                    objFMCase.Request_Type_DeveloperName__c,objBU.Property_City__c);
            System.debug('==lstApprovers==' + lstApprovers);
            String approvingRoles = '';
            for(FM_Approver__mdt mdt : lstApprovers) {
                approvingRoles += mdt.Role__c + ',';
            }
            approvingRoles = approvingRoles.removeEnd(',');
            List<String> lstRoles = new List<String>();
            lstRoles = approvingRoles.split(',');
            System.debug('==objFMCase.Admin__c==' + objFMCase.Admin__c);
            System.debug('==approvingRoles==' + approvingRoles.split(','));
            System.debug('==approvingRoles==' + approvingRoles);
            objFMCase.Approving_Authorities__c = approvingRoles;
            objFMCase.Accepted_Terms_and_Conditions__c = true;
            if(!Test.isRunningTest()) {
                upsert objFMCase;
            }
            saveDetails();
            //updateAdditionalDetails();
            PageReference pageRef = new PageReference('/'+objFMCase.Id);
            return pageRef;
        }
        catch(System.Exception excp) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                excp.getMessage() + excp.getLineNumber()));
        }
        return NULL;
    }

    public override PageReference saveAsDraft() {
        System.debug('===saveAsDraft==');
        System.debug('strDate = ' + strDate);
        try {
            System.debug('==strDate==' + strDate);
            List<String> lstDate = new List<String>();
            if(String.isNotBlank(strDate.trim())) {
                System.debug('If date is not null');
                lstDate = strDate.split('/');
            }
            System.debug('---lstDate--- : '+lstDate);
            System.debug('---lstDate size--- : '+lstDate.size());
            if( lstDate != NULL && lstDate.size() > 0 && !lstDate.isEmpty()) {
                System.debug('If Date list in not null  ');
                objFMCase.Move_in_date__c = Date.newinstance(Integer.valueOf(lstDate[2]),
                            Integer.valueOf(lstDate[1]),Integer.valueOf(lstDate[0]));
            }
            System.debug('---objFMCase--- : '+objFMCase);
            System.debug('objFMCase.Move_in_date__c = ' + objFMCase.Move_in_date__c);
            objFMCase.Status__c = 'Draft Request';
            if(objFMCase.Move_in_move_out_type__c == 'Self move-in') {
                objFMCase.Company__c = objFMCase.Contractor__c = objFMCase.Mobile_no_contractor__c = '';
            }
            objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get(
                'Move In').getRecordTypeId();
            upsert objFMCase;
            saveDetails();
            //updateAdditionalDetails();
            PageReference pageRef = new PageReference('/'+objFMCase.Id);
            return pageRef;
        }
        catch(System.Exception excp) {
            System.debug('excp===='+excp);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                excp.getMessage() + excp.getLineNumber()));
        }
        return NULL;
    }

    public PageReference submitSr() {
        objFMCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get(
            'Move In').getRecordTypeId();
        PageReference nextPage = submit();
        if (nextPage != NULL) {
            String unitId = nextPage.getUrl().substringAfterLast('/');
            nextPage = new PageReference(ApexPages.currentPage().getUrl());
            nextPage.getParameters().clear();
            nextPage.getParameters().put('view', 'CaseDetails');
            nextPage.getParameters().put('id', unitId);
            nextPage.setRedirect(true);
        }
        System.debug('nextPage = ' + nextPage);
        //System.debug('nextPage Url = ' + nextPage.getUrl());
        return nextPage;
    }

    public PageReference saveSr() {
       PageReference nextPage = saveAsDraft();
        if (nextPage != NULL) {
            String unitId = nextPage.getUrl().substringAfterLast('/');
            nextPage = new PageReference(ApexPages.currentPage().getUrl());
            nextPage.getParameters().clear();
            nextPage.getParameters().put('view', 'CaseDetails');
            nextPage.getParameters().put('id', unitId);
            nextPage.setRedirect(true);
        }
        System.debug('nextPage = ' + nextPage);
       // System.debug('nextPage Url = ' + nextPage.getUrl());
        return nextPage;
    }

    /*public void initializeAddDetailsMap() {
        mapAdditionalDetails = new map<String, list<FM_Additional_Detail__c>>();
        system.debug('---fmCaseId---- : '+fmCaseId);
        if( String.isNotBlank( fmCaseId ) ) {
            for( FM_Additional_Detail__c objAD : [ SELECT Id
                                                        , RecordType.Name
                                                        , RecordType.DeveloperName
                                                        , Emergency_Contact_Case__c
                                                        , Resident_Case__c
                                                        , Vehicle_Case__c
                                                        , Vehicle_Number__c
                                                        , Name__c
                                                        , Relationship__c
                                                        , Phone__c
                                                        , Age__c
                                                        , Nationality__c
                                                        , Passport_Number__c
                                                        , Identification_Number__c
                                                        , Vehicle_Make_Model__c
                                                        , Vehicle_Colour__c
                                                        , Email__c
                                                        , Parking_Slot_Number__c
                                                        , People_of_Determination__c
                                                        , Date_of_Birth__c
                                                        , Gender__c
                                                        , Registration_Country__c
                                                        , Type_of_Vehicle__c
                                                        , Mobile__c
                                                        , Vehicle_Sticker_No__c
                                                        , Pet_Case__c
                                                        , Pet_Name__c
                                                        , Pet_Type__c
                                                        , Description_Of_Pet__c
                                                     FROM FM_Additional_Detail__c
                                                    WHERE Emergency_Contact_Case__c = :fmCaseId
                                                       OR Resident_Case__c = :fmCaseId
                                                       OR Vehicle_Case__c = :fmCaseId
                                                       OR Pet_Case__c = :fmCaseId ] ) {
                if( String.isNotBlank( objAD.Emergency_Contact_Case__c ) ||
                    String.isNotBlank( objAD.Resident_Case__c ) ||
                    String.isNotBlank( objAD.Vehicle_Case__c ) ||
                    String.isNotBlank( objAD.Pet_Case__c ) ) {
                    if( mapAdditionalDetails.containsKey( objAD.RecordType.Name ) ) {
                        mapAdditionalDetails.get( objAD.RecordType.Name ).add( objAD );
                    }
                    else {
                        mapAdditionalDetails.put( objAD.RecordType.Name, new list<FM_Additional_Detail__c>{ objAD } );
                    }
                }
            }
            if( mapAdditionalDetails.containsKey( 'Vehicle' ) ) {
                intNumOfVehicles = mapAdditionalDetails.get( 'Vehicle' ).size() ;
            }
            if( mapAdditionalDetails.containsKey( 'Resident' ) ) {
                intNoOfAddDetails = mapAdditionalDetails.get( 'Resident' ).size() ;
            }
        }
        initializeFMCaseAddDetails();
    }

     public FM_Additional_Detail__c createAddDetailsObj( String strRecordTypeName ) {
        FM_Additional_Detail__c objAD = new FM_Additional_Detail__c();
        objAD.RecordTypeId = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get(
                    strRecordTypeName ).getRecordTypeId();
        return objAD ;
    }

    public override void addDetails() {
        system.debug('>>>----------detailType------- : ' + detailType);
        if( String.isNotBlank( detailType ) ) {
            FM_Additional_Detail__c objDetail = new FM_Additional_Detail__c();
            objDetail.RecordTypeId = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get(detailType).getRecordTypeId();
            system.debug('>>>>RecordTypeId------- : ' + objDetail.RecordTypeId);
            if(detailType.equals('Emergency Contact')  && mapAdditionalDetails != NULL && mapAdditionalDetails.containsKey( detailType ) ){
                 Id devRecordTypeId = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Emergency Contact').getRecordTypeId();
                 // lstEmergencyDetails.add(new FM_Additional_Detail__c(RecordTypeId=devRecordTypeId));
                 objDetail.Emergency_Contact_Case__c = objFMCase.Id ;
                 system.debug('>>>>objDetail.Emergency_Contact_Case__c------- : ' + objDetail.Emergency_Contact_Case__c);
                 mapAdditionalDetails.get( detailType ).add( objDetail ) ;
            }
            else if(detailType.equals('Resident')) {
                Id devRecordTypeId = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Resident').getRecordTypeId();
                lstResidentDetails.add(new FM_Additional_Detail__c(RecordTypeId=devRecordTypeId));
            }
            else if(detailType.equals('Household')) {
                System.debug('Add Household staff row');
                Id devRecordTypeId = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Household').getRecordTypeId();
                lstHouseholdDetails.add(new FM_Additional_Detail__c(RecordTypeId=devRecordTypeId));
            }
            else if(detailType.equals('Vehicle')) {
                Id devRecordTypeId = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Vehicle').getRecordTypeId();
                lstVehicleDetails.add(new FM_Additional_Detail__c(RecordTypeId=devRecordTypeId));
            }
            else if(detailType.equals('Pet Details')) {
                Id devRecordTypeId = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Pet Details').getRecordTypeId();
                lstPetDetails.add(new FM_Additional_Detail__c(RecordTypeId=devRecordTypeId));
            }
        }
    }

     public override void removeDetails() {
        System.debug('===indexVar===' + indexVar);
        try{
            FM_Additional_Detail__c itemTodelete = new FM_Additional_Detail__c();
            if(detailType == 'Resident') {
                itemTodelete = lstResidentDetails.remove(indexVar);
            }
            else if(detailType == 'Household') {
                itemTodelete = lstHouseholdDetails.remove(indexVar);
            }
            else if(detailType == 'Vehicle') {
                itemTodelete = lstVehicleDetails.remove(indexVar);
            }
            else if(detailType == 'Emergency Contact') {
                mapAdditionalDetails.get(detailType).remove( indexVar );
            }
            else if(detailType == 'Pet Details') {
                itemTodelete = lstPetDetails.remove(indexVar);
            }
            if(itemTodelete.Id != NULL) {
                delete itemTodelete;
            }
        }
        catch(System.Exception excp) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                excp.getMessage() + excp.getLineNumber()));
        }
    }

    public void initializeFMCaseAddDetails(){
        System.debug('----mapAdditionalDetails------------: '+mapAdditionalDetails);
        if(mapAdditionalDetails != NULL) {
            if( !mapAdditionalDetails.containsKey( 'Emergency Contact' ) ) {
                FM_Additional_Detail__c objAD = createAddDetailsObj( 'Emergency Contact' );
                mapAdditionalDetails.put(  'Emergency Contact' , new list<FM_Additional_Detail__c>{ objAD } );
            }
            System.debug('----mapAdditionalDetails------------: '+mapAdditionalDetails);
        }

        //Adding Resident Details
        if( objFMCase != NULL ) {
            intNoOfAddDetails = 0;
            intNoOfAddDetails += String.isNotBlank( objFMCase.No_of_Adults__c ) ? Integer.valueOf( objFMCase.No_of_Adults__c ) : 0 ;
            intNoOfAddDetails += String.isNotBlank( objFMCase.No_of_Children__c ) ? Integer.valueOf( objFMCase.No_of_Children__c ) : 0 ;
            intNoOfAddDetails = intNoOfAddDetails>=1 ? intNoOfAddDetails-1 : intNoOfAddDetails ;
            if( !mapAdditionalDetails.containsKey( 'Resident' ) ||
                ( mapAdditionalDetails.containsKey( 'Resident' ) && mapAdditionalDetails.get( 'Resident' ).size() != intNoOfAddDetails ) ) {
                mapAdditionalDetails.put( 'Resident', new list<FM_Additional_Detail__c>() );
            }
            else {
                intNoOfAddDetails = 0;
            }
            for( integer i=0; i<intNoOfAddDetails; i++ ) {
                FM_Additional_Detail__c objAD = createAddDetailsObj( 'Resident' );
                mapAdditionalDetails.get( 'Resident' ).add( objAD );
            }
        }

        //Adding Pet Details
        if( !mapAdditionalDetails.containsKey( 'Pet Details' ) ) {
            FM_Additional_Detail__c objAD = createAddDetailsObj( 'Pet Details' );
            mapAdditionalDetails.put(  'Pet Details' , new list<FM_Additional_Detail__c>{ objAD } );
        }

        //Adding Vehicle Details
        if( !mapAdditionalDetails.containsKey( 'Vehicle' ) && intNumOfVehicles != NULL && intNumOfVehicles > 0 ) {
            mapAdditionalDetails.put( 'Vehicle', new list<FM_Additional_Detail__c>() );
            for( Integer i=0; i<intNumOfVehicles; i++ ) {
                FM_Additional_Detail__c objAD = createAddDetailsObj( 'Vehicle' );
                objAD.Registration_Country__c = 'United Arab Emirates';
                mapAdditionalDetails.get('Vehicle').add( objAD );
            }

        }
        else if( mapAdditionalDetails.containsKey( 'Vehicle' ) &&
                 mapAdditionalDetails.get( 'Vehicle' ).size() != intNumOfVehicles ) {
            mapAdditionalDetails.put( 'Vehicle', new list<FM_Additional_Detail__c>() );
            for( Integer i=0; i<intNumOfVehicles; i++ ) {
                FM_Additional_Detail__c objAD = createAddDetailsObj( 'Vehicle' );
                objAD.Registration_Country__c = 'United Arab Emirates';
                mapAdditionalDetails.get('Vehicle').add( objAD );
            }
        }
        system.debug('==mapAdditionalDetails=='+mapAdditionalDetails);
    }*/

    public PageReference goToTerms() {
        System.debug('>>>---termsLink--- : '+termsLink);
        PageReference pdfPage = new PageReference(termsLink);
        pdfPage.setRedirect(true);
        return pdfPage ;
        //return NULL;
    }

    /*public void updateAdditionalDetails(){
        System.debug('--- mapAdditionalDetails --- : '+mapAdditionalDetails);
        if( mapAdditionalDetails != NULL ) {
            list<FM_Additional_Detail__c> lstAddDetails = new list<FM_Additional_Detail__c>();
            for( String strADType : mapAdditionalDetails.keySet() ) {
                System.debug('--- strADType --- : '+strADType);
                if( mapAdditionalDetails.get( strADType ).size() > 0 ) {
                    if( strADType.equalsIgnoreCase( 'Resident' ) ) {
                        System.debug('Resident Details');
                        for( FM_Additional_Detail__c objDet : mapAdditionalDetails.get( strADType ) ) {
                            if( String.isNotBlank(objDet.Name__c) || String.isNotBlank(objDet.Nationality__c)
                                    || String.isNotBlank(objDet.Passport_Number__c) ){
                                objDet.Resident_Case__c = objFMCase.Id ;
                                objDet.Resident_Account__c = objFMCase.Tenant__c ;
                                lstAddDetails.add( objDet );
                            }
                        }
                    }
                    else if( strADType.equalsIgnoreCase( 'Emergency Contact' ) ) {
                        System.debug('Emergency Contact Details');
                        for( FM_Additional_Detail__c objDet : mapAdditionalDetails.get( strADType ) ) {
                            if( String.isNotBlank(objDet.Name__c) || String.isNotBlank(objDet.Relationship__c)
                                    || String.isNotBlank(objDet.Phone__c) || String.isNotBlank(objDet.Email__c)
                                    || String.isNotBlank(objDet.Mobile__c) ){
                                objDet.Emergency_Contact_Case__c = objFMCase.Id ;
                                objDet.Emergency_Contact_Account__c = objFMCase.Tenant__c ;
                                lstAddDetails.add( objDet );
                            }
                        }
                    }
                    else if( strADType.equalsIgnoreCase( 'Vehicle' ) ) {
                        System.debug('Vehicle Details');
                        for( FM_Additional_Detail__c objDet : mapAdditionalDetails.get( strADType ) ) {
                            if( String.isNotBlank(objDet.Vehicle_Number__c) || String.isNotBlank(objDet.Vehicle_Make_Model__c)
                                    || String.isNotBlank(objDet.Identification_Number__c) || String.isNotBlank(objDet.Vehicle_Colour__c)){
                                objDet.Vehicle_Case__c = objFMCase.Id ;
                                objDet.Vehicle_Account__c = objFMCase.Tenant__c ;
                                lstAddDetails.add( objDet );
                            }
                        }
                    }
                    else if( strADType.equalsIgnoreCase( 'Pet Details' ) ) {
                        System.debug('Pet Details Details');
                        for( FM_Additional_Detail__c objDet : mapAdditionalDetails.get( strADType ) ) {
                            if( String.isNotBlank(objDet.Pet_Name__c) || String.isNotBlank(objDet.Pet_Type__c)
                                    || String.isNotBlank(objDet.Description_Of_Pet__c) ){
                                objDet.Pet_Case__c = objFMCase.Id ;
                                objDet.Pet_Account__c = objFMCase.Tenant__c ;
                                lstAddDetails.add( objDet );
                            }
                        }
                    }
                }
            }

            system.debug('==lstAddDetails=='+lstAddDetails);
            if( !lstAddDetails.isEmpty() ) {
                upsert lstAddDetails ;
            }
        }
    }*/
}