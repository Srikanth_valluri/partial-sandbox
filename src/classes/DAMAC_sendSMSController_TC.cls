@isTest
public class DAMAC_sendSMSController_TC {
    static testMethod void sendSMS () {
        Send_SMS__c sms = new Send_SMS__c ();
        sms.Record_Count__c = 10;
        sms.Record_Limit_message__c = ' Test ';
        sms.Salesforce_Base_URL__c = 'https://damacholding--fullcopy.cs80.my.salesforce.com';
        insert sms;
        
        Campaign__c camp = new Campaign__c();
        camp.Marketing_Start_Date__c = System.today().adddays(-10);
        camp.Start_Date__c = System.today().adddays(-10);        
        camp.End_Date__c =  System.today().adddays(10);
        camp.Marketing_End_Date__c =  System.today().adddays(10);
        camp.Campaign_Name__c = 'T3 - c';   
        camp.Country__c = 'United Arab Emirates';   
        camp.City__c = 'Abu Dhabi';
        camp.Campaign_Type_New__c = 'Stands'; 
        camp.Campaign_Category_New__c = 'Mall';
        insert camp;
        
        ID recordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry Decrypted').getRecordTypeId();      
        Inquiry__c inq = new Inquiry__c ();
        inq.Mobile_phone__c = '98765456789';
        inq.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Mobile_Phone_Encrypt__c = 'WERTYUIYRTYU';
        inq.Class__c = 'test';
        inq.Campaign__c = camp.Id;
        inq.Inquiry_Status__c = 'Active';
        inq.Inquiry_Source__c ='Agent Referral';  
        inq.RecordtypeId = recordTypeId;
        inq.Class__c = 'test';
        inq.Phone_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Nationality__c = 'UAE';
        insert inq;
        
        campaign_sms__c l = new campaign_sms__c();
        l.SMS_Bulk_Id__c ='3037903';
        l.sms_Message__c = 'test';
        l.marketing_campaign__c = camp.id;  
        l.Scheduled_Date_Time__c = System.today().addDays(10);
        l.Inquiry_End_Date__c = System.today().addDays (10);
        l.Inquiry_Start_Date__c = System.today().addDays(-10);    
        insert l;
        
        Attachment att = new Attachment ();
        att.Name = 'Inquiry Mobile Numbers.csv';
        att.ParentID = l.ID;
        att.ContentType = 'csv';
        
        att.Body = Blob.valueOF (EncodingUtil.base64Encode (Blob.valueOf ('11111111')));
        insert att;
        
        DAMAC_sendSMSController obj = new DAMAC_sendSMSController();
        obj.getSMSHistory();
        DAMAC_sendSMSController.getPickListOptions ('Campaign__c', 'Country__c');
        DAMAC_sendSMSController.getValues ('Campaign__c', 'Country__c', '', '');
        DAMAC_sendSMSController.getValues ('Campaign__c', 'Name', 'Select Name, Campaign_Name__c FROM Campaign__c LIMIT 1', 'c');
        DAMAC_sendSMSController.getValues ('Campaign__c', 'Name', 'Select Name, Campaign_Name__c FROM Campaign__c LIMIT 1', 'c');
        DAMAC_sendSMSController.getValues ('Inquiry__c', 'RecordTypeId', 'Select Name FROM recordType Where SObjectType =\'Inquiry__c\'', 'test');
        DAMAC_sendSMSController.getValues ('Inquiry__c', 'Class__c', 'Select Class__c FROM Inquiry__c group by Class__c', 'test');
        
        obj.newSMSRequest.sms_Message__c = 'test';
        obj.newSMSRequest.Scheduled_Date_Time__c = System.today().adddays(10);
        obj.newSMSRequest.Inquiry_Start_Date__c = System.today().adddays(-10);
        obj.newSMSRequest.Inquiry_End_Date__c = System.today().adddays(10);
        obj.campaigns = camp.Id;
        obj.campaignCountry = 'United Arab Emirates,test';
        obj.campaignCity = 'Abu Dhabi,test';
        obj.campaignType = 'Stands,test';
        obj.campaignCategory = 'mall,test';
        obj.inquirySource = 'Agent Referral,test';
        obj.inquiryStatus = 'Active,test';
        obj.inquiryRecordType = recordTypeId;
        obj.inquiryClass = 'test,test';
        obj.phoneCodes = 'United Arab Emirates: 00971,United Arab Emirates: 00971';
        obj.nationalities = 'UAE';
        obj.getFilterRecords ();
        obj.submitForApproval ();
        obj.selectedCampaignSMSID = l.Id;
        obj.callSMSjob ();
        obj.newSMSRequest.sms_Message__c = 'test';
        obj.submitForApproval ();
        obj.selectedCampaignSMSID = NULL;
        obj.callSMSjob ();
        
        
    }
}