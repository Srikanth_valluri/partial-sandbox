/**************************************************************************************************
* Class: ProjectCompareController
* VF Page : ProjectCompare
* Created by : Tejashree Chavan
---------------------------------------------------------------------------------------------------
* Purpose/Methods: Controller of AgencyPCManagement console VF page
---------------------------------------------------------------------------------------------------
* Test Data: ProjectCompareController_Test
----------------------------------------------------------------------------------
* Version History: (All changes and TA reworks should be entered as new row )
* VERSION     DEVELOPER NAME       DATE          DETAIL FEATURES
    1.1       Tejashree Chavan     10/03/2018    Initial Draft
***************************************************************************************************/
public class ProjectCompareController {
    public Inventory__c project1 {get;set;}
    public Inventory__c project2 {get;set;}
    public ProjectInfoWrapper project1Wrapper {get;set;}
    public ProjectInfoWrapper project2Wrapper {get;set;}
    public PaymentPlanWrapper projectPlanWrapper1 {get;set;}
    public PaymentPlanWrapper projectPlanWrapper2 {get;set;}
    public String selProduct {get;set;}
    public String selProject {get;set;}
    public String project {get;set;}
    public List<SelectOption> productList {get;set;}
    public List<SelectOption> projectList {get;set;}

    private Set<String> selectedInvSet = new Set<String>();
    private Set<String> propertySet = new Set<String>();
    private Set<String> marketingNameSet = new Set<String>();
    private Map<String,String> locationMap = new Map<String,String>();

    public ProjectCompareController() {
        productList = new List<SelectOption>();
        projectList = new List<SelectOption>();
        getProduct();
        getProject();
        if(ApexPages.currentPage().getParameters().get('selectedInv') != null) {
            String[] selectedInv = ApexPages.currentPage().getParameters().get('selectedInv').split(',');
            selectedInvSet.addAll(selectedInv);
            getInventories();
            getProjectInfo();
            getPaymentPlan();
        }
    }

    /*********************************************************************************************
    * @Description : this method conatins the logic to get inventories data for comparision      *
    * @Params      : null                                                                        *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void getInventories() {
        if(!selectedInvSet.isEmpty()) {
            List<Inventory__c> selectedInventories = [SELECT
                                                            Id,
                                                            Name,
                                                            Marketing_Name_Doc__c,
                                                            Marketing_Name_Doc__r.Name,
                                                            Marketing_Name__c,
                                                            Property__c,
                                                            Property__r.Name,
                                                            Property_Name_2__c,
                                                            Building_Location__c,
                                                            District__c,
                                                            Property_City__c,
                                                            Property_Country__c,
                                                            Property_Status__c
                                                      FROM
                                                            Inventory__c
                                                      WHERE
                                                            Id IN :selectedInvSet];
            if(selectedInventories != null && !selectedInventories.isEmpty()) {
                project1 = selectedInventories[0];
                project2 = selectedInventories[1];
                for(Inventory__c inv : selectedInventories) {
                    propertySet.add(inv.Property__c);
                    marketingNameSet.add(inv.Marketing_Name_Doc__c);
                    locationMap.put(inv.Marketing_Name_Doc__c+'_'+inv.Property__c,
                                    inv.Building_Location__c);
                }
            }
        }
    }

    /*********************************************************************************************
    * @Description : Method to get project info of selected projects for comparision             *
    * @Params      : null                                                                        *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void getSelProjectInfo() {
        List<Inventory__c> selectedInventories = [SELECT
                                                        Id,
                                                        Name,
                                                        Marketing_Name_Doc__c,
                                                        Marketing_Name_Doc__r.Name,
                                                        Marketing_Name__c,
                                                        Property__c,
                                                        Property__r.Name,
                                                        Property_Name_2__c,
                                                        Building_Location__c,
                                                        District__c,
                                                        Property_City__c,
                                                        Property_Country__c,
                                                        Property_Status__c
                                                  FROM
                                                        Inventory__c
                                                  WHERE
                                                        Marketing_Name_Doc__c = :selProduct
                                                        AND
                                                        Property__c = :selProject
                                                        AND
                                                        Status__c = 'Released'];
        if(selectedInventories != null && !selectedInventories.isEmpty()) {
            if(String.isNotBlank(project) && project.equalsIgnoreCase('First')) {
                project1 = selectedInventories[0];
            }
            else if(String.isNotBlank(project) && project.equalsIgnoreCase('Second')) {
                project2 = selectedInventories[0];
            }
            propertySet.add(selectedInventories[0].Property__c);
            marketingNameSet.add(selectedInventories[0].Marketing_Name_Doc__c);
            locationMap.put(selectedInventories[0].Marketing_Name_Doc__c + '_' +
                            selectedInventories[0].Property__c,
                            selectedInventories[0].Building_Location__c);
            getProjectInfo();
            getPaymentPlan();
        }
        else {
            if(String.isNotBlank(project) && project.equalsIgnoreCase('First')) {
                project1 = null;
                projectPlanWrapper1 = null;
                project1Wrapper = null;
            }
            else if(String.isNotBlank(project) && project.equalsIgnoreCase('Second')) {
                project2 = null;
                projectPlanWrapper2 = null;
                project2Wrapper = null;
            }
        }
    }

    /*********************************************************************************************
    * @Description : Method to form project wrapper of  selected projects for comparision        *
    * @Params      : null                                                                        *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void getProjectInfo() {
        Map<String,List<Project_Information__c>> projectInfoMap = new Map<String,List<Project_Information__c>>();
        Map<String,AdditionalFeatures__c> additionalFeatureSetting = AdditionalFeatures__c.getAll();
        if(!propertySet.isEmpty() && !marketingNameSet.isEmpty()) {
            for(Project_Information__c projectInfo : [SELECT
                                                            Id,
                                                            Name,
                                                            Marketing_Documents__c,
                                                            Project_Features__c,
                                                            Property__c,
                                                            Use_For_Compare__c,
                                                            URL__c,
                                                            Detail__c,
                                                            Features__c,
                                                            Heading__c,
                                                            Order__c
                                                      FROM
                                                            Project_Information__c
                                                      WHERE
                                                            Marketing_Documents__c IN :marketingNameSet
                                                            AND
                                                            Property__c IN :propertySet
                                                      ORDER BY Order__c]) {
                if(!projectInfoMap.containsKey(projectInfo.Marketing_Documents__c+'_'+projectInfo.Property__c)) {
                    projectInfoMap.put(projectInfo.Marketing_Documents__c+'_'+projectInfo.Property__c,
                                       new List<Project_Information__c>{});
                }
                projectInfoMap.get(projectInfo.Marketing_Documents__c+'_'+projectInfo.Property__c).add(projectInfo);
            }
            if(project1 != null && project1.Marketing_Name_Doc__c != null && project1.Property__c != null) {
                project1Wrapper = new ProjectInfoWrapper();
                project1Wrapper.unitFeatures = new List<Project_Information__c>();
                project1Wrapper.projectfeatures = new Map<String,String>();
                project1Wrapper.projectDetails = getProjectDetails(project1.Marketing_Name_Doc__c,
                                                                   project1.Property__c);
                if(projectInfoMap.containsKey(project1.Marketing_Name_Doc__c+'_'+project1.Property__c)) {
                    for(Project_Information__c projectInfo : projectInfoMap.get (
                            project1.Marketing_Name_Doc__c+'_'+project1.Property__c)) {
                        if(projectInfo.Features__c != null && projectInfo.Features__c.equalsIgnoreCase('Gallery') &&
                           projectInfo.URL__c != null &&
                           projectInfo.Use_For_Compare__c) {
                            project1Wrapper.imageURL = projectInfo.URL__c;
                        }
                        if(projectInfo.Features__c != null && projectInfo.Features__c.equalsIgnoreCase('Project Features') &&
                           projectInfo.Project_Features__c != null &&
                           additionalFeatureSetting != null && !additionalFeatureSetting.isEmpty()) {
                            for(String ftr : projectInfo.Project_Features__c.split(';')) {
                                if(additionalFeatureSetting.containsKey(ftr)) {
                                    project1Wrapper.projectfeatures.put(ftr,
                                                                        additionalFeatureSetting.get(ftr).Url__c);
                                }
                            }
                        }
                        if(projectInfo.Features__c != null  && projectInfo.Features__c.equalsIgnoreCase('Unit Features') &&
                           projectInfo.Heading__c != null && projectInfo.Detail__c != null) {
                            project1Wrapper.unitFeatures.add(projectInfo);
                        }
                    }
                }
            }
            if(project2 != null && project2.Marketing_Name_Doc__c != null && project2.Property__c != null) {
                project2Wrapper = new ProjectInfoWrapper();
                project2Wrapper.projectfeatures = new Map<String,String>();
                project2Wrapper.unitFeatures = new List<Project_Information__c>();
                project2Wrapper.projectDetails = getProjectDetails(project2.Marketing_Name_Doc__c,
                                                                   project2.Property__c);
                if(projectInfoMap.containsKey(project2.Marketing_Name_Doc__c+'_'+project2.Property__c)) {
                    for(Project_Information__c projectInfo : projectInfoMap.get (
                            project2.Marketing_Name_Doc__c+'_'+project2.Property__c)) {
                        if(projectInfo.Features__c != null &&
                           projectInfo.Features__c.equalsIgnoreCase('Gallery') &&
                           projectInfo.URL__c != null &&
                           projectInfo.Use_For_Compare__c) {
                            project2Wrapper.imageURL = projectInfo.URL__c;
                        }
                        if(projectInfo.Features__c != null &&
                           projectInfo.Features__c.equalsIgnoreCase('Project Features') &&
                           projectInfo.Project_Features__c != null &&
                           additionalFeatureSetting != null && !additionalFeatureSetting.isEmpty()) {
                            for(String ftr : projectInfo.Project_Features__c.split(';')) {
                                if(additionalFeatureSetting.containsKey(ftr)) {
                                    project2Wrapper.projectfeatures.put(ftr,
                                        additionalFeatureSetting.get(ftr).Url__c);
                                }
                            }
                        }
                        if(projectInfo.Features__c != null &&
                           projectInfo.Features__c.equalsIgnoreCase('Unit Features') &&
                           projectInfo.Heading__c != null && projectInfo.Detail__c != null) {
                            project2Wrapper.unitFeatures.add(projectInfo);
                        }
                    }
                }
            }
        }
    }

    /*********************************************************************************************
    * @Description : Method to form project detail section data of  selected projects for        *
                     comparision                                                                 *
    * @Params      : String - select product, String - selected project                          *
    * @Return      : Map<String,String> - Map of Project Details                                 *
    *********************************************************************************************/
    public Map<String,String> getProjectDetails(String product,String project) {
        Map<String,String> projectDeatilMap = new Map<String,String>();
        List<String> bedrooms = new List<String>();
        Set<String> unitTypes = new Set<String>();
        Set<String> bedRoomTypes = new Set<String>();
        Set<String> viewTypes = new Set<String>();
        String currencySales = '';
        if(String.isNotBlank(product) && String.isNotBlank(project)) {
            for(Inventory__c inv : [SELECT
                                        Id,
                                        Name,
                                        Unit_Type__c,
                                        Bedroom_Type__c,
                                        IPMS_Bedrooms__c,
                                        View_Type__c,
                                        Marketing_Name_Doc__c,
                                        Property__c,
                                        Currency_of_Sale__c
                                    FROM
                                        Inventory__c
                                    WHERE
                                        Marketing_Name_Doc__c = :product
                                        AND
                                        Property__c = :project
                                        AND
                                        Status__c = 'Released']) {
                if(!bedrooms.contains(inv.IPMS_Bedrooms__c)) {
                    bedrooms.add(inv.IPMS_Bedrooms__c);
                }
                unitTypes.add(inv.Unit_Type__c);
                bedRoomTypes.add(inv.Bedroom_Type__c);
                viewTypes.add(inv.View_Type__c);
                currencySales = inv.Currency_of_Sale__c;
            }

            if(!bedrooms.isEmpty()) {
                bedrooms.sort();
                projectDeatilMap.put('No. of Bedrooms',String.join(bedrooms,', '));
            }
            if(!unitTypes.isEmpty()) {
                projectDeatilMap.put('Unit Types',String.join(new List<String>(unitTypes),', '));
            }
            if(!bedRoomTypes.isEmpty()) {
                projectDeatilMap.put('Bedroom Types',String.join(new List<String>(bedRoomTypes),', '));
            }
            if(!viewTypes.isEmpty()) {
                projectDeatilMap.put('Views',String.join(new List<String>(viewTypes),', '));
            }

            for(AggregateResult ar : [SELECT
                                            MIN(Anticipated_Completion_Date__c),
                                            MAX(Anticipated_Completion_Date__c),
                                            MIN(Area_sft__c),
                                            MAX(Area_sft__c),
                                            MIN(Special_Price__c),
                                            MAX(Special_Price__c),
                                            MIN(Price_Per_Sqft__c),
                                            MAX(Price_Per_Sqft__c)
                                      FROM
                                            Inventory__c
                                      WHERE
                                            Marketing_Name_Doc__c = :product
                                            AND
                                            Property__c = :project
                                            AND
                                            Status__c = 'Released'
                                      GROUP BY Marketing_Name_Doc__c,Property__c]) {
                projectDeatilMap.put('ACD',ar.get('expr0') +' - '+ar.get('expr1'));
                projectDeatilMap.put('Area Range',Integer.valueOf(ar.get('expr2')).format() +' - '+
                                                  Integer.valueOf(ar.get('expr3')).format() +' SFT');
                projectDeatilMap.put('Price Range',currencySales+' '+
                                                   Integer.valueOf(ar.get('expr4')).format() +' - '+
                                                   currencySales+' '+
                                                   Integer.valueOf(ar.get('expr5')).format());
                projectDeatilMap.put('Price Per Sq Ft.',currencySales+' '+
                                                        Integer.valueOf(ar.get('expr6')).format() +
                                                        ' - '+currencySales+' '+
                                                        Integer.valueOf(ar.get('expr7')).format());
            }
        }
        return projectDeatilMap;
    }

    /*********************************************************************************************
    * @Description : Method to form available and active products to show in picklist            *
    * @Params      : NULL                                                                        *
    * @Return      : Void                                                                        *
    *********************************************************************************************/
    public void getProduct() {
        productList.add(new SelectOption('NONE','Select New Product'));
        for(Marketing_Documents__c mrkDoc : [SELECT
                                                Id,
                                                Name,
                                                Marketing_Name__c
                                             FROM Marketing_Documents__c
                                             ORDER BY Name ASC]) {
            productList.add(new SelectOption(mrkDoc.Id,mrkDoc.Marketing_Name__c));
        }
    }

    /*********************************************************************************************
    * @Description : Method to form available and active projects to show in picklist            *
    * @Params      : NULL                                                                        *
    * @Return      : Void                                                                        *
    *********************************************************************************************/
    public void getProject() {
        projectList.add(new SelectOption('NONE','Select New Project'));
        Map<Id,String> propertyIdToNameMap = new Map<Id,String>();
        for(Inventory__c objInv : [SELECT
                                       Id,
                                       Property_Status__c,
                                       Unit_Type__c,Unit__c,
                                       Property_Name__c,
                                       Property__c,
                                       Anticipated_Completion_Date__c
                                   FROM Inventory__c
                                   WHERE Status__c='Released'
                                   AND Property__c != NULL
                                   AND Property_Name__c != NULL
                                   ORDER BY Property_Name__c ASC LIMIT 10000 ]){
            propertyIdToNameMap.put(objInv.Property__c,objInv.Property_Name__c);
        }

        // using map as collection size is hitting governor limit of 1000 in dropdown.
        // Also as we are querying from Inventory duplicates Property is coming.
        for(Id propertyId : propertyIdToNameMap.keySet()){
            projectList.add(new SelectOption(propertyId,propertyIdToNameMap.get(propertyId)));
        }
    }

    /*********************************************************************************************
    * @Description : Method to get payment plan to show in payment plan section                  *
    * @Params      : NULL                                                                        *
    * @Return      : Void                                                                        *
    *********************************************************************************************/
    public void getPaymentPlan() {
        Map<String,List<Payment_Terms__c>> locationpaymentTerms = new Map<String,List<Payment_Terms__c>>();
        if(!locationMap.isEmpty()) {
            Set<String> locationSet = new Set<String>(locationMap.values());
            for(Payment_Terms__c paymentTerms : [SELECT
                                                    Id,
                                                    Name,
                                                    Description__c,
                                                    Milestone_Event__c,
                                                    Percent_Value__c,
                                                    Payment_Plan__r.Building_Location__c,
                                                    Line_ID__c
                                                 FROM
                                                    Payment_Terms__c
                                                 WHERE
                                                    Payment_Plan__r.Building_Location__c IN :locationSet
                                                    AND
                                                    Payment_Plan__r.Effective_From__c <= TODAY
                                                    AND
                                                    Payment_Plan__r.Effective_To_calculated__c  >= TODAY
                                                    AND
                                                    Payment_Plan__r.Building_Location__r.Location_Type__c = 'Building'
                                                    ORDER BY Line_ID__c]) {
                if(!locationpaymentTerms.containsKey(paymentTerms.Payment_Plan__r.Building_Location__c)) {
                    locationpaymentTerms.put(paymentTerms.Payment_Plan__r.Building_Location__c,
                                              new List<Payment_Terms__c>{});
                }
                locationpaymentTerms.get(paymentTerms.Payment_Plan__r.Building_Location__c).add(paymentTerms);
            }
            if(project1 != null && project1.Marketing_Name_Doc__c != null && project1.Property__c != null) {
                if(locationMap.containsKey(project1.Marketing_Name_Doc__c+'_'+project1.Property__c) &&
                   locationMap.get(project1.Marketing_Name_Doc__c+'_'+project1.Property__c) != null &&
                   locationpaymentTerms.containsKey(locationMap.get(project1.Marketing_Name_Doc__c +
                                                                    '_'+project1.Property__c))) {
                    projectPlanWrapper1 = new PaymentPlanWrapper();
                    for(Payment_Terms__c term : locationpaymentTerms.get (
                            locationMap.get(project1.Marketing_Name_Doc__c +'_'+
                                            project1.Property__c))) {
                        if(term.Description__c.equalsIgnoreCase('DEPOSIT')) {
                            projectPlanWrapper1.deposit = term;
                        }
                        else {
                            projectPlanWrapper1.paymentTerms.add(term);
                        }
                    }
                }
            }
            if(project2 != null && project2.Marketing_Name_Doc__c != null && project2.Property__c != null) {
                if(locationMap.containsKey(project2.Marketing_Name_Doc__c+'_'+project2.Property__c) &&
                   locationMap.get(project2.Marketing_Name_Doc__c+'_'+project2.Property__c) != null &&
                   locationpaymentTerms.containsKey(locationMap.get(project2.Marketing_Name_Doc__c+
                                                                    '_'+project2.Property__c))) {
                    projectPlanWrapper2 = new PaymentPlanWrapper();
                    for(Payment_Terms__c term : locationpaymentTerms.get(
                            locationMap.get(project2.Marketing_Name_Doc__c+'_'+
                                            project2.Property__c))) {
                        if(term.Description__c.equalsIgnoreCase('DEPOSIT')) {
                            projectPlanWrapper2.deposit = term;
                        }
                        else {
                            projectPlanWrapper2.paymentTerms.add(term);
                        }
                    }
                }
            }
        }
    }

    public class ProjectInfoWrapper {
        public String imageURL {get;set;}
        public Map<String,String> projectfeatures {get;set;}
        public List<Project_Information__c> unitFeatures {get;set;}
        public Map<String,String> projectDetails {get;set;}
        public ProjectInfoWrapper() {
            projectDetails = new Map<String,String>();
        }
    }

    public class PaymentPlanWrapper {
        public Payment_Terms__c deposit {get;set;}
        public List<Payment_Terms__c> paymentTerms {get; set;}
        PaymentPlanWrapper() {
            paymentTerms = new List<Payment_Terms__c>();
        }
    }
}