public class TravelUtilizationController {

    public Travel_Details__c details { get; set; }
    
    public TravelUtilizationController(ApexPages.StandardController stdController) {
        details = new Travel_Details__c();
        details = [SELECT Status__c, Utilization_Status__c, Outcome__c, Reason_for_No_Show__c FROM Travel_Details__c WHERE ID =: stdController.getID()];
        details.Utilization_Status__c = NULL;
    }
    
    public void updateUtilization() {

        if (details.Utilization_Status__c == 'Flyin Completed') {
            details.Status__c = 'Closed';
        }
        update details;
    }
}