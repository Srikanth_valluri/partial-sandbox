/******************************************************************************
* Description - Test class developed for GenerateKRFDrawloopExtension
********************************************************************************/

@isTest
public class GenerateKRFDrawloopExtensionTest{

    @isTest static void testMethod1() {

        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        Id hoPCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover Parent').getRecordTypeId();
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
          objBookingUnit.Okay_to_release_keys__c = true;
          objBookingUnit.Keys_Released__c = true;
        }
        insert bookingUnitList;

        Case   CasP = TestDataFactory_CRM.createCase(objAccount.Id,hoPCaseRecordTypeId);
        Case   Cas = TestDataFactory_CRM.createCase(objAccount.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = bookingUnitList[0].Id ;
        Cas.Type = 'NOCVisa';
        Cas.IsRunningTest__c = true;
        Cas.PCC_Issued__c = true;
        Cas.Type = 'NOCVisa';
        Cas.ParentId = CasP.id;
        insert Cas;

        Task taskObj1 = new Task();
        taskObj1.WhatId = Cas.Id;
        taskObj1.Priority = 'High';
        taskObj1.ActivityDate = System.Today();
        taskObj1.Status = 'Closed';
        taskObj1.Subject = 'Upload DLD Agreement and Ejari';
        taskObj1.Assigned_User__c = 'CRE';
        taskObj1.Process_Name__c = 'Handover';
        taskObj1.Task_Due_Date__c = System.Today();
        taskObj1.Completed_DateTime__c = System.Now();
        insert taskObj1;        
        
        SR_Attachments__c objSR_Attachments = new SR_Attachments__c();
        objSR_Attachments.Name = 'DLD Agreement';
        objSR_Attachments.Case__c = Cas.Id;
        objSR_Attachments.Attachment_URL__c = 'www.google.com';
        insert objSR_Attachments;
        PageReference redirectionUrl;
        Test.startTest();
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));      
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(Cas);
            GenerateKRFDrawloopExtension objController = new GenerateKRFDrawloopExtension(standardControllerInstance);
            Test.setCurrentPageReference(new PageReference('Page.GenerateKRFDrawloop'));
            System.currentPageReference().getParameters().put('Id', Cas.Id);
            redirectionUrl = objController.init();
            objController.returnToCase();
            /*objController.socialChannels = '';
            objController.ChildrenByAge = '';
            objController.holidayDestination = '';
            objController.favMusic = '';
            objController.favCar = '';*/
            
        Test.stopTest();


    }

    @isTest static void testMethod2() {

        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        objAccount.Mobile_Phone_Encrypt__pc = '421414124';
        objAccount.Email__pc = 'test@t.com';
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
          objBookingUnit.Okay_to_release_keys__c = true;
          objBookingUnit.Keys_Released__c = true;
        }
        insert bookingUnitList;
        
        Profile p = [SELECT Id FROM Profile WHERE Name Like 'CRE%'  Limit 1]; 
        User u = new User(Alias = 'standt', Email='standardusertest@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standardusertest11@testorg.com'); 
        insert u;

        Case   Cas = TestDataFactory_CRM.createCase(objAccount.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = bookingUnitList[0].Id ;
        Cas.Type = 'NOCVisa';
        Cas.IsRunningTest__c = true;
        Cas.PCC_Issued__c = true;
        Cas.Bypass_OTP__c = true;
        insert Cas;



        PageReference redirectionUrl;
        Test.startTest();
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));      
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(Cas);
            Test.setCurrentPageReference(new PageReference('Page.GenerateKRFDrawloop'));
            System.currentPageReference().getParameters().put('Id', Cas.Id);
            System.runAs( u ) {
                GenerateKRFDrawloopExtension objController = new GenerateKRFDrawloopExtension(standardControllerInstance);
                redirectionUrl = objController.init();
                objController.returnToCase();   
            }
        Test.stopTest();

    }
    
    @isTest static void testMethod3() {

        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        objAccount.Email__pc = 'test@t.com';
        objAccount.Mobile_Phone_Encrypt__pc = '00034243242';
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
          objBookingUnit.Okay_to_release_keys__c = true;
          objBookingUnit.Keys_Released__c = true;
          objBookingUnit.Property_City__c = 'Dubai';
          
        }
        insert bookingUnitList;

        
        Id hoPCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover Parent').getRecordTypeId();
        Case   CasP = TestDataFactory_CRM.createCase(objAccount.Id,hoPCaseRecordTypeId);
        insert CasP;
        Case   Cas = TestDataFactory_CRM.createCase(objAccount.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = bookingUnitList[0].Id ;
        Cas.Type = 'NOCVisa';
        Cas.IsRunningTest__c = true;
        Cas.PCC_Issued__c = true;
        Cas.ParentId = CasP.Id;
        Cas.Total_Snags_Open__c = 2;
        Cas.Client_Dewa_No__c = '3243';
        Cas.Payment_Verified__c = true;
        insert Cas;

        Profile p = [SELECT Id FROM Profile WHERE Name Like 'CRE%' Limit 1]; 
        /*User u = new User(Alias = 'standt', Email='standardusertest@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standardusertest12@testorg.com'); 
        insert u;*/

        insert new Snag_Validation_Configuration__c(SetupOwnerId=UserInfo.getOrganizationId(), IsActive__c=true,Allowed_Major_Snags__c = 5);

        SR_Attachments__c objSR_Attachments = new SR_Attachments__c();
        objSR_Attachments.Name = 'Signed Handover Checklist';
        objSR_Attachments.Case__c = Cas.Id;
        objSR_Attachments.Attachment_URL__c = 'www.google.com';
        insert objSR_Attachments;

        PageReference redirectionUrl;
        Test.startTest();
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));      
            Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockCallingList(1));
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(Cas);
            
            Test.setCurrentPageReference(new PageReference('Page.GenerateKRFDrawloop'));
            System.currentPageReference().getParameters().put('Id', Cas.Id);
            //System.runAs( u ) {
                GenerateKRFDrawloopExtension objController = new GenerateKRFDrawloopExtension(standardControllerInstance);
                redirectionUrl = objController.init();
                objController.enteredOtp = '12345';
                objController.generateOtp();
                objController.continueKRFGeneration();
                objController.returnToCase();  
            //}
             
        Test.stopTest();


    }
    
    
    @isTest static void testMethod4() {

        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        objAccount.Email__pc = 'test@t.com';
        objAccount.Mobile_Phone_Encrypt__pc = '00034243242';
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
          objBookingUnit.Okay_to_release_keys__c = true;
          objBookingUnit.Keys_Released__c = true;
          objBookingUnit.Property_City__c = 'Dubai';
          
        }
        insert bookingUnitList;

        
        Id hoPCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover Parent').getRecordTypeId();
        Case   CasP = TestDataFactory_CRM.createCase(objAccount.Id,hoPCaseRecordTypeId);
        insert CasP;
        Case   Cas = TestDataFactory_CRM.createCase(objAccount.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = bookingUnitList[0].Id ;
        Cas.Type = 'NOCVisa';
        Cas.IsRunningTest__c = true;
        Cas.PCC_Issued__c = true;
        Cas.ParentId = CasP.Id;
        Cas.Total_Snags_Open__c = 0;
        Cas.Client_Dewa_No__c = '3243';
        Cas.Payment_Verified__c = true;
        insert Cas;

        Profile p = [SELECT Id FROM Profile WHERE Name Like 'CRE%' Limit 1]; 
        /*User u = new User(Alias = 'standt', Email='standardusertest@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standardusertest13@testorg.com'); 
        insert u;*/

        insert new Snag_Validation_Configuration__c(SetupOwnerId=UserInfo.getOrganizationId(), IsActive__c=true,Allowed_Major_Snags__c = 5);

        SR_Attachments__c objSR_Attachments = new SR_Attachments__c();
        objSR_Attachments.Name = 'Signed Handover Checklist';
        objSR_Attachments.Case__c = Cas.Id;
        objSR_Attachments.Attachment_URL__c = 'www.google.com';
        insert objSR_Attachments;

        PageReference redirectionUrl;
        Test.startTest();
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));      
            Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockCallingList(1));
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(Cas);
            
            Test.setCurrentPageReference(new PageReference('Page.GenerateKRFDrawloop'));
            System.currentPageReference().getParameters().put('Id', Cas.Id);
            //System.runAs( u ) {
                GenerateKRFDrawloopExtension objController = new GenerateKRFDrawloopExtension(standardControllerInstance);
                redirectionUrl = objController.init();
                objController.enteredOtp = '12345';
                objController.generateOtp();
                objController.continueKRFGeneration();
                objController.returnToCase();  
            //}
             
        Test.stopTest();


    }
    
     @isTest static void testMethod5() {

        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        objAccount.Email__pc = 'test@t.com';
        objAccount.Mobile_Phone_Encrypt__pc = '00034243242';
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
          objBookingUnit.Okay_to_release_keys__c = true;
          objBookingUnit.Keys_Released__c = true;
          objBookingUnit.Property_City__c = 'Dubai';
          
        }
        insert bookingUnitList;

        
        Id hoPCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover Parent').getRecordTypeId();
        Case   CasP = TestDataFactory_CRM.createCase(objAccount.Id,hoPCaseRecordTypeId);
        insert CasP;
        Case   Cas = TestDataFactory_CRM.createCase(objAccount.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = bookingUnitList[0].Id ;
        Cas.Type = 'NOCVisa';
        Cas.IsRunningTest__c = true;
        Cas.PCC_Issued__c = true;
        Cas.ParentId = CasP.Id;
        Cas.Total_Snags_Open__c = 0;
        Cas.Total_Major_Snags__c = 3;
        Cas.Client_Dewa_No__c = '3243';
        Cas.Payment_Verified__c = true;
        insert Cas;

        Profile p = [SELECT Id FROM Profile WHERE Name Like 'CRE%' Limit 1]; 
        /*User u = new User(Alias = 'standt', Email='standardusertest@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standardusertest14@testorg.com'); 
        insert u;*/

        insert new Snag_Validation_Configuration__c(SetupOwnerId=UserInfo.getOrganizationId(), IsActive__c=true,Allowed_Major_Snags__c = 1);

        SR_Attachments__c objSR_Attachments = new SR_Attachments__c();
        objSR_Attachments.Name = 'Signed Handover Checklist';
        objSR_Attachments.Case__c = Cas.Id;
        objSR_Attachments.Attachment_URL__c = 'www.google.com';
        insert objSR_Attachments;

        PageReference redirectionUrl;
        Test.startTest();
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));      
            Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockCallingList(1));
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(Cas);
            
            Test.setCurrentPageReference(new PageReference('Page.GenerateKRFDrawloop'));
            System.currentPageReference().getParameters().put('Id', Cas.Id);
            //System.runAs( u ) {
                GenerateKRFDrawloopExtension objController = new GenerateKRFDrawloopExtension(standardControllerInstance);
                redirectionUrl = objController.init();
                objController.enteredOtp = '12345';
                objController.generateOtp();
                objController.continueKRFGeneration();
                objController.returnToCase();  
            //}
             
        Test.stopTest();


    }
    
    
    
    @isTest static void testPlotHoKrf() {

        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Property
        Property__c objProp = TestDataFactory_CRM.createProperty();
        objProp.Name = 'AKOYA';
        insert objProp;

        // Create Inventory
        Inventory__c objInv = TestDataFactory_CRM.createInventory( objProp.Id );
        objInv.Building_Code__c = 'PLOT';
        insert objInv;

        //Configure DDP details
        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c(Name = 'Plot Handover KRF'
            , Drawloop_Document_Package_Id__c = '432424'
            , Delivery_Option_Id__c = '43424' );
        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
          objBookingUnit.Okay_to_release_keys__c = true;
          objBookingUnit.Keys_Released__c = true;
            objBookingUnit.Inventory__c = objInv.Id;
            objBookingUnit.Unit_Type__c = 'Plot';
        }
        insert bookingUnitList;

        Case   Cas = TestDataFactory_CRM.createCase(objAccount.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = bookingUnitList[0].Id ;
        Cas.Type = 'NOCVisa';
        Cas.IsRunningTest__c = true;
        Cas.PCC_Issued__c = true;
        insert Cas;



        PageReference redirectionUrl;
        Test.startTest();
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
            Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());      
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(Cas);
            GenerateKRFDrawloopExtension objController = new GenerateKRFDrawloopExtension(standardControllerInstance);
            Test.setCurrentPageReference(new PageReference('Page.GenerateKRFDrawloop'));
            System.currentPageReference().getParameters().put('Id', Cas.Id);
            redirectionUrl = objController.init();
            objController.returnToCase();   
        Test.stopTest();


    }

    @isTest static void testPlotHoKrf_noDDPdetails() {

        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Property
        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;

        // Create Inventory
        Inventory__c objInv = TestDataFactory_CRM.createInventory( objProp.Id );
        objInv.Building_Code__c = 'PLOT';
        insert objInv;

        //Configure DDP details
        /*insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c(Name = 'Plot Handover KRF'
            , Drawloop_Document_Package_Id__c = '432424'
            , Delivery_Option_Id__c = '43424' );*/
        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
          objBookingUnit.Okay_to_release_keys__c = true;
          objBookingUnit.Keys_Released__c = true;
            objBookingUnit.Inventory__c = objInv.Id;
            objBookingUnit.Unit_Type__c = 'Plot';
        }
        insert bookingUnitList;

        Case   Cas = TestDataFactory_CRM.createCase(objAccount.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = bookingUnitList[0].Id ;
        Cas.Type = 'NOCVisa';
        Cas.IsRunningTest__c = true;
        Cas.PCC_Issued__c = true;
        insert Cas;



        PageReference redirectionUrl;
        Test.startTest();
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));      
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(Cas);
            GenerateKRFDrawloopExtension objController = new GenerateKRFDrawloopExtension(standardControllerInstance);
            Test.setCurrentPageReference(new PageReference('Page.GenerateKRFDrawloop'));
            System.currentPageReference().getParameters().put('Id', Cas.Id);
            redirectionUrl = objController.init();
            objController.returnToCase();   
        Test.stopTest();


    }

    @isTest static void testPlotHoKrf_noTemplateId() {

        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Property
        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;

        // Create Inventory
        Inventory__c objInv = TestDataFactory_CRM.createInventory( objProp.Id );
        objInv.Building_Code__c = 'PLOT';
        insert objInv;

        //Configure DDP details
        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c(Name = 'Plot Handover KRF'
            //, Drawloop_Document_Package_Id__c = '432424'
            , Delivery_Option_Id__c = '43424' );
        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
          objBookingUnit.Okay_to_release_keys__c = true;
          objBookingUnit.Keys_Released__c = true;
            objBookingUnit.Inventory__c = objInv.Id;
            objBookingUnit.Unit_Type__c = 'Plot';
        }
        insert bookingUnitList;

        Case   Cas = TestDataFactory_CRM.createCase(objAccount.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = bookingUnitList[0].Id ;
        Cas.Type = 'NOCVisa';
        Cas.IsRunningTest__c = true;
        Cas.PCC_Issued__c = true;
        insert Cas;



        PageReference redirectionUrl;
        Test.startTest();
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));      
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(Cas);
            GenerateKRFDrawloopExtension objController = new GenerateKRFDrawloopExtension(standardControllerInstance);
            Test.setCurrentPageReference(new PageReference('Page.GenerateKRFDrawloop'));
            System.currentPageReference().getParameters().put('Id', Cas.Id);
            redirectionUrl = objController.init();
            objController.returnToCase();   
        Test.stopTest();


    }

    @isTest static void testPlotHoKrf_noDeliveryId() {

        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();

        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Property
        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;

        // Create Inventory
        Inventory__c objInv = TestDataFactory_CRM.createInventory( objProp.Id );
        objInv.Building_Code__c = 'PLOT';
        insert objInv;

        //Configure DDP details
        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c(Name = 'Plot Handover KRF'
            , Drawloop_Document_Package_Id__c = '432424');
            //, Delivery_Option_Id__c = '43424' 
        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
          objBookingUnit.Okay_to_release_keys__c = true;
          objBookingUnit.Keys_Released__c = true;
            objBookingUnit.Inventory__c = objInv.Id;
            objBookingUnit.Unit_Type__c = 'Plot';
        }
        insert bookingUnitList;

        Case   Cas = TestDataFactory_CRM.createCase(objAccount.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = bookingUnitList[0].Id ;
        Cas.Type = 'NOCVisa';
        Cas.IsRunningTest__c = true;
        Cas.PCC_Issued__c = true;
        insert Cas;



        PageReference redirectionUrl;
        Test.startTest();
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));      
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(Cas);
            GenerateKRFDrawloopExtension objController = new GenerateKRFDrawloopExtension(standardControllerInstance);
            Test.setCurrentPageReference(new PageReference('Page.GenerateKRFDrawloop'));
            System.currentPageReference().getParameters().put('Id', Cas.Id);
            redirectionUrl = objController.init();
            objController.returnToCase();   
        Test.stopTest();


    }

    @isTest static void testsendPlotHoKrfEmailNotification() {

        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Property
        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;

        // Create Inventory
        Inventory__c objInv = TestDataFactory_CRM.createInventory( objProp.Id );
        objInv.Building_Code__c = 'PLOT';
        insert objInv;

        //Configure DDP details
        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c(Name = 'Plot Handover KRF'
            , Drawloop_Document_Package_Id__c = '432424');
            //, Delivery_Option_Id__c = '43424' 
        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
          objBookingUnit.Okay_to_release_keys__c = true;
          objBookingUnit.Keys_Released__c = true;
            objBookingUnit.Inventory__c = objInv.Id;
            objBookingUnit.Unit_Type__c = 'Plot';
        }
        insert bookingUnitList;

        Case   Cas = TestDataFactory_CRM.createCase(objAccount.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = bookingUnitList[0].Id ;
        Cas.Type = 'NOCVisa';
        Cas.IsRunningTest__c = true;
        Cas.PCC_Issued__c = true;
        insert Cas;
        
        Test.startTest();
        GenerateKRFDrawloopExtension.sendPlotHoKrfEmailNotification(new List<Id> {Cas.Id});
        Test.stopTest();


    }

    @isTest static void testMethodLHO() {

        Id CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Lease Handover').getRecordTypeId();

        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        objAccount.Email__pc = 'test@t.com';
        objAccount.Mobile_Phone_Encrypt__pc = '00034243242';
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
          objBookingUnit.Okay_to_release_keys__c = true;
          objBookingUnit.Keys_Released__c = true;
          objBookingUnit.Property_City__c = 'Dubai';
          
        }
        insert bookingUnitList;

        
        Id hoPCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        Case   CasP = TestDataFactory_CRM.createCase(objAccount.Id,hoPCaseRecordTypeId);
        insert CasP;
        Case   Cas = TestDataFactory_CRM.createCase(objAccount.Id,CaseRecordTypeId);
        Cas.Booking_Unit__c = bookingUnitList[0].Id ;
        Cas.Type = 'NOCVisa';
        Cas.IsRunningTest__c = true;
        Cas.PCC_Issued__c = true;
        Cas.ParentId = CasP.Id;
        Cas.Total_Snags_Open__c = 0;
        Cas.Client_Dewa_No__c = '3243';
        Cas.Payment_Verified__c = true;
        insert Cas;

        Profile p = [SELECT Id FROM Profile WHERE Name Like 'CRE%' Limit 1]; 
        User u = new User(Alias = 'standt', Email='standardusertest@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standardusertest15@testorg.com'); 
        insert u;

        insert new Snag_Validation_Configuration__c(SetupOwnerId=UserInfo.getOrganizationId(), IsActive__c=true,Allowed_Major_Snags__c = 5);

        SR_Attachments__c objSR_Attachments = new SR_Attachments__c();
        objSR_Attachments.Name = 'Signed Handover Checklist';
        objSR_Attachments.Case__c = Cas.Id;
        objSR_Attachments.Attachment_URL__c = 'www.google.com';
        insert objSR_Attachments;

        PageReference redirectionUrl;
        Test.startTest();
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));      
            Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockCallingList(1));
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(Cas);
            
            Test.setCurrentPageReference(new PageReference('Page.GenerateKRFDrawloop'));
            System.currentPageReference().getParameters().put('Id', Cas.Id);
            System.runAs( u ) {
                GenerateKRFDrawloopExtension objController = new GenerateKRFDrawloopExtension(standardControllerInstance);
                redirectionUrl = objController.init();
                objController.enteredOtp = '12345';
                objController.byPassOtp =true;
                objController.generateOtp();
                objController.continueKRFGeneration();
                objController.returnToCase(); 
                objController.returnHTMLBody('KRF_Message');
                objController.isSignedHOChecklistUploaded();
            }
             
        Test.stopTest();


    }
}