/*
* Description - Test class developed for Schedule EscalateCLBatchScheduler
*/
@isTest
public class EscalateCLBatchSchedulerTest {
      static testMethod void testExecute() {
        Test.startTest();
            EscalateCLBatchScheduler objScheduler = new EscalateCLBatchScheduler();
            String schCron = '0 30 8 1/1 * ? *';
            String jobId = System.Schedule('Reccord Updated',schCron,objScheduler);
        Test.stopTest();
        
    }
}