@isTest
public class PDCReminderBatchTest {
    @isTest
    static void testBatch(){
        Account objAccount = new Account();
        objAccount.Name = 'test';
        objAccount.Party_ID__c = '12345';
        insert objAccount;
        
        Post_Dated_Cheque__c objPost = new Post_Dated_Cheque__c();
        objPost.Cheque_Date__c = System.Today().addDays(3);
        insert objPost;
        
        
        Test.startTest();
            PDCReminderBatch objPDC = new PDCReminderBatch();
            Database.executeBatch(objPDC);
            //objPDC.sendmail(objAccount);
        Test.stopTest();
    }
}