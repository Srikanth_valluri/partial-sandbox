@isTest
private class FmDocumentNotificationBatchTest {

    @testSetup
    static void setupTestData() {
        FmDocumentUploadBatchTriggerTest.setupTestData();
    }

    @isTest
    static void testBatch() {
        List<FM_Document_Upload_Batch__c> lstUploadBatch = new List<FM_Document_Upload_Batch__c>();
        lstUploadBatch.add(FmDocumentUploadBatchTriggerTest.uploadBatchForPropertyLevelDocuments('Everyone'));
        lstUploadBatch.add(FmDocumentUploadBatchTriggerTest.uploadBatchForBuildingLevelDocuments('Everyone'));
        lstUploadBatch.add(FmDocumentUploadBatchTriggerTest.uploadBatchForPropertyLevelDocuments('Owners'));
        lstUploadBatch.add(FmDocumentUploadBatchTriggerTest.uploadBatchForBuildingLevelDocuments('Owners'));
        lstUploadBatch.add(FmDocumentUploadBatchTriggerTest.uploadBatchForPropertyLevelDocuments('Tenants'));
        lstUploadBatch.add(FmDocumentUploadBatchTriggerTest.uploadBatchForBuildingLevelDocuments('Tenants'));
        lstUploadBatch.add(FmDocumentUploadBatchTriggerTest.uploadBatchForPropertyLevelDocuments('Board Members'));
        lstUploadBatch.add(FmDocumentUploadBatchTriggerTest.uploadBatchForBuildingLevelDocuments('Board Members'));
        lstUploadBatch.add(
            FmDocumentUploadBatchTriggerTest.uploadBatchForPropertyLevelDocuments('Owners and Tenants')
        );
        lstUploadBatch.add(
            FmDocumentUploadBatchTriggerTest.uploadBatchForBuildingLevelDocuments('Owners and Tenants')
        );

        for (FM_Document_Upload_Batch__c uploadBatch : lstUploadBatch) {
            uploadBatch.Approval_Status__c = 'Approved';
        }

        upsert lstUploadBatch;

        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());

        Test.startTest();
            FmDocumentUploadBatchTriggerHelper.executeBatchInTest = true;
            FmDocumentUploadBatchTriggerHelper.emailDocumentsToFmRecipients(
                new Map<Id, FM_Document_Upload_Batch__c>(lstUploadBatch).keySet()
            );
        Test.stopTest();
    }
}