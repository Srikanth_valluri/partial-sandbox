/***********************************************************************************************************************************
* Name               : AgentOTPController*
* Description        : Controller class for OTP                                 *
* Created Date       : 05/02/2017                                                                                      *
* Created By         : Naresh (Accely)                                                                                             *
* ---------------------------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR               DATE            COMMENTS                                                               *
* 1.0         Naresh- Accely       14/08/2017      Initial Draft.                                                         *
*
**************************************************************************************************************************************/
@isTest(SeeAllData=false)
public class AgentOTPControllerTest{

  public static testMethod void TestOTP(){
  Test.startTest();
   AgentOTPController obj =  new AgentOTPController();
   obj.resendOTP();
   obj.showScreen5();
   
    OTP__c otp =  new OTP__c();
    otp.Phone_Number__c  = '9955665878'; 
    otp.OTP_Number__c = '12356';
    insert otp;
  
  AgentOTPController.PhoneNumber = otp.Phone_Number__c;
  AgentOTPController.CountryCode = '091'; 
  
  System.debug('OTP Inseeted-------- '+otp);
  AgentOTPController.verifyotp(otp.Phone_Number__c,otp.OTP_Number__c);
  
  AgentOTPController.GenrateOTP(otp.Phone_Number__c);
//  Test.setMock(HttpCalloutMock.class, new MockHttpAgentOTPController());

  Test.stopTest();
  
  
  }

}