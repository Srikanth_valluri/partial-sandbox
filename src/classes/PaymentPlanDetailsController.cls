/*********************************************************************************************************
* Name               : PaymentPlanDetailsController
* Test Class         : PaymentPlanDetailsControllerTest
* Description        : Controller class for PaymentPlanDetails VF Page
* Created Date       : 05/10/2020
* -----------------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst         05/10/2020      Initial Draft.
**********************************************************************************************************/
global With Sharing Class PaymentPlanDetailsController{
    public transient string paymentPlanListJSON {get;set;}
    public transient List <Payment_Plan__c> paymentPlanList { get; set; }
    public string paymentTermsListJSON {get;set;}

    /*********************************************************************************************
    * @Description : Controller class.
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public PaymentPlanDetailsController() {
        system.debug('entered');
        getPaymentPlan();
       
    }
	/************************************************************************************************
    * @Description : method to fetch the Payment Terms.
    * @Params      : void
    * @Return      : void
    ************************************************************************************************/
    public void getPaymentPlan(){
        system.debug('eneterd');
        paymentPlanList = new List<Payment_Plan__c>();
        paymentPlanList = [SELECT id, Name, Building_ID__c, Booking_Unit__c, Payment_Term_Description__c,
                                    Active__c, Effective_To_calculated__c, Effective_from__c,
                           			(SELECT id,Percent_Value__c FROM Payment_Terms__r)
                                     FROM Payment_Plan__c
                                     WHERE Parent_Payment_Plan__c = NULL AND Active__c = true 
                                     AND Effective_To_calculated__c >= TODAY
                                     ORDER BY CreatedDate DESC
                                     LIMIT 20000];
        Decimal perc =0;
        for(Payment_Plan__c plan :paymentPlanList){
            	system.debug(plan.Payment_Terms__r);
        }
       
        paymentPlanListJSON = JSON.serialize(paymentPlanList); 
    }
    /************************************************************************************************
    * @Description : method to fetch the Payment Terms.
    * @Params      : void
    * @Return      : void
    ************************************************************************************************/
    public void getPaymentTerms() {
        String id = ApexPages.currentPage().getParameters().get('planName');
        System.debug('id: ' + id);
        //name = name.trim();
       // Payment_Plan__c plan=[SELECT id from Payment_Plan__c WHERE Payment_Term_Description__c =:name /*Name =: name*/];
       // system.debug('plan: ' + plan);
       List<Payment_Terms__c> paymentTerms = [SELECT Id, Name, Installment__c, Description__c,
                                                   Milestone_Event__c, Milestone_Event_Arabic__c,
                                                   Percent_Value__c 
                                              FROM Payment_Terms__c WHERE Payment_Plan__c =:id];
        system.debug('paymentTerms: ' + paymentTerms);
        paymentTermsListJSON = JSON.serialize(paymentTerms); 
        system.debug('paymentTermsListJSON: ' + paymentTermsListJSON);
    }
    
     /*********************************************************************************************
    * @Description : Method to get the search details
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public void updateSearchQuery() {
        String searchValue = ApexPages.currentPage().getParameters().get('search');
        List<Payment_Plan__c> planList = new List<Payment_Plan__c> ();
        if(searchValue == null || searchValue  == ''){
			getPaymentPlan();          
        } else{
            String key = '%' + searchValue + '%';
           
            planList = [SELECT id, Name, Building_ID__c, Booking_Unit__c, Payment_Term_Description__c,
                                    Active__c, Effective_To_calculated__c, Effective_from__c
                                     FROM Payment_Plan__c
                                     WHERE Parent_Payment_Plan__c = NULL 
                                     AND Effective_To_calculated__c >= TODAY
                        			AND (Name LIKE: key OR Payment_Term_Description__c LIKE: key )                      
                                     ORDER BY CreatedDate DESC
                                     LIMIT 20000];
             paymentPlanListJSON = JSON.serialize(planList);
           
        }
        
    }
    
}