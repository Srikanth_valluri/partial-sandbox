/**
 * @File Name          : PopulateCOCDLetterforDpInvoiceBatch.cls
 * @Description        : This Batch is used to populate COCD letter for DP invoice.
 * @Author             : Dipika Rajput
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 7/4/2019, 12:04:08 PM
 * @Modification Log   : 
 *==============================================================================================
 * Ver         Date                     Author                    Modification
 * 
 *==============================================================================================
 * 1.0    7/4/2019                   Dipika Rajput           Initial Version
**/
public without sharing class PopulateCOCDLetterforDpInvoiceBatch implements Database.Batchable<sObject>,
            Database.AllowsCallouts, Database.Stateful {
    
    private String query; 
    public String cocdFormURL;
    public List<DP_Invoices__c> DpInvoiceUpdateList;
    public List<Error_Log__c> errList;
    public PopulateCOCDLetterforDpInvoiceBatch() {
        DpInvoiceUpdateList = new List<DP_Invoices__c>();
        errList = new List<Error_Log__c>(); 
    }
    public Database.Querylocator start( Database.BatchableContext bc ) {
        query = ' SELECT Id,'+ 
                ' Accounts__c, '+
                ' Cover_Letter__c, '+
                ' SOA__c,'+
                ' TAX_Invoice__c,'+
                ' BookingUnits__r.Registration_ID__c,'+
                ' COCD_Letter__c '+
                ' FROM DP_Invoices__c '+
                ' Where '+
                ' COCD_Letter__c = NULL '+
                ' AND EmailSent__c = false ' ;

         if( String.isNotBlank ( Label.DP_Invoices_Rec_Id_For_Test ) 
                         && Label.DP_Invoices_Rec_Id_For_Test.contains('On') 
                         && String.isNotBlank( Label.DP_Invoices_Rec_Id_For_Test.substringAfter('-') ) ) {
                            query = query + ' AND Id = \''+Label.DP_Invoices_Rec_Id_For_Test.substringAfter('-')+ '\'  ';
         }else{
            query = query +'LIMIT 999';
          }
                        
        
        System.debug('Dp Invoice query ::: '+Database.getQueryLocator(query));
        return Database.getQueryLocator(query);
    }
    public void execute( Database.BatchableContext bc, list<DP_Invoices__c> dpInvoiceRecords ) {
        System.debug('dpInvoiceRecords ::: '+dpInvoiceRecords);
        for(DP_Invoices__c objDPInvoice: dpInvoiceRecords){
            cocdUrlCallout(objDPInvoice);
        }
        System.debug('===DpInvoiceUpdateList execute=== '+DpInvoiceUpdateList);
    }
    public void finish( Database.BatchableContext bc ) {
        System.debug('===DpInvoiceUpdateList finish=== '+DpInvoiceUpdateList);
        if(!DpInvoiceUpdateList.isEmpty()){
            update DpInvoiceUpdateList;
        }
        System.debug('===errList finish=== '+errList);
        if(!errList.isEmpty()) {
            insert errList;
        }
    }

    public void cocdUrlCallout(DP_Invoices__c DPInvoiceObj){
        System.debug('DPInvoiceObj ::: '+DPInvoiceObj);
        if(DPInvoiceObj.Accounts__c != NULL){
            Account accObj = new Account();
            accObj = [SELECT Id
                            , IsPersonAccount
                            , Passport_Expiry_Date__pc
                            , CR_Registration_Expiry_Date__c
                            , Party_ID__c
                            , Address_Line_1__pc
                            , Address_Line_1__c
                            , Address_Line_2__pc
                            , Address_Line_2__c
                            , Address_Line_3__pc
                            , Address_Line_3__c
                            , Address_Line_4__pc
                            , Address_Line_4__c
                            , City__pc,City__c
                            , State__c
                            , BillingPostalCode
                            , Country__pc,Country__c
                            , Mobile_Country_Code__pc
                            , Mobile_Country_Code__c
                            , Mobile_Phone_Encrypt__pc
                            , Mobile__c
                            , Home_Phone_Country_Code__c
                            , Home_Phone_Encrypt__c
                            , Email__pc
                            , Email__c
                            , Passport_Number__pc
                            , CR_Number__c
                    FROM Account
                    WHERE Id =: DPInvoiceObj.Accounts__c];
            System.debug('accObj ::: '+accObj);
            Boolean personAccount = accObj.IsPersonAccount;
            
            //Adding date format for Passport Issue Date (Expiry Date) 
            Date d = personAccount ? accObj.Passport_Expiry_Date__pc : accObj.CR_Registration_Expiry_Date__c;

            String P_REQUEST_NUMBER = String.valueOf( datetime.now().getTime() );
            String P_REQUEST_NAME = 'COCD_LETTER';
            String P_SOURCE_SYSTEM = 'SFDC';
            String IPMSPartyID = accObj.Party_ID__c;
            String Address1 = personAccount ? accObj.Address_Line_1__pc : accObj.Address_Line_1__c;
            String Address2 = personAccount ? accObj.Address_Line_2__pc : accObj.Address_Line_2__c;
            String Address3 = personAccount ? accObj.Address_Line_3__pc : accObj.Address_Line_3__c;
            String Address4 = personAccount ? accObj.Address_Line_4__pc : accObj.Address_Line_4__c;
            String city = personAccount ? accObj.City__pc : accObj.City__c;
            String state = accObj.State__c;
            String postalCode = accObj.BillingPostalCode;
            String country = personAccount ? accObj.Country__pc : accObj.Country__c;
            String MobileCountryCode = personAccount
                                        ?   String.isNotBlank( accObj.Mobile_Country_Code__pc )
                                            ? accObj.Mobile_Country_Code__pc.split(':')[1].removeStart(' 00')
                                            : ''
                                        : String.isNotBlank( accObj.Mobile_Country_Code__c )
                                            ? accObj.Mobile_Country_Code__c.split(':')[1].removeStart(' 00')
                                            : '';
            String MobileAreaCode = '';
            //String MobileNumber = personAccount ? accObj.Mobile_Phone_Encrypt__pc : accObj.Mobile__c;
            String MobileNumber =personAccount ? SeparateCountryCode.getCountryCode(
                accObj.Mobile_Phone_Encrypt__pc,accObj.Mobile_Country_Code__pc
            ): SeparateCountryCode.getCountryCode(accObj.Mobile__c,accObj.Mobile_Country_Code__c);

            String PhoneCountryCode = String.isNotBlank(  accObj.Home_Phone_Country_Code__c)
                                        ? accObj.Home_Phone_Country_Code__c.split(':')[1].removeStart(' 00')
                                        : '';
            String PhoneAreaCode = '';
            String PhoneNumber = accObj.Home_Phone_Encrypt__c;
            String FaxCountryCode = '';
            String FaxAreaCode = '';
            String FaxNumber = '';
            String EmailAddress = personAccount ? accObj.Email__pc : accObj.Email__c;
            String PassportIssueDate = ( d != null ) ? DateTime.newInstance(d.year(),d.month(),d.day()).format('dd-MMM-YYYY') : '';

            String PassportNumnber = personAccount ? accObj.Passport_Number__pc : accObj.CR_Number__c;
            actionCom.COCDHttpSoap11Endpoint cocdFormCalloutObj = new actionCom.COCDHttpSoap11Endpoint();
            cocdFormCalloutObj.timeout_x = 120000;
            try{
                cocdFormURL = cocdFormCalloutObj.generateCOCD(
                      P_REQUEST_NUMBER
                    , P_REQUEST_NAME
                    , P_SOURCE_SYSTEM
                    , IPMSPartyID
                    , Address1
                    , Address2
                    , Address3
                    , Address4
                    , city
                    , state
                    , postalCode
                    , country
                    , MobileCountryCode
                    , MobileAreaCode
                    , MobileNumber
                    , PhoneCountryCode
                    , PhoneAreaCode
                    , PhoneNumber
                    , FaxCountryCode
                    , FaxAreaCode
                    , FaxNumber
                    , EmailAddress
                    , PassportIssueDate
                    , PassportNumnber
                );

                System.debug('===cocdFormURL=== '+cocdFormURL);
                cocdFormURL = ( cocdFormURL.indexOf('~') != -1 && !cocdFormURL.split('~')[0].equalsIgnoreCase('s') ) ? '' : cocdFormURL;
                DPInvoiceObj.COCD_Letter__c = cocdFormURL;
                System.debug('===DPInvoiceObj=== '+DPInvoiceObj);
                DpInvoiceUpdateList.add(DPInvoiceObj);
                System.debug('===DpInvoiceUpdateList in method=== '+DpInvoiceUpdateList);
            } catch(Exception e) {
                System.debug('===cocdFormURL==Exception= '+e);
                //cocdFormURL = 'No URL found from IPMS';
                Error_Log__c errlog =  new Error_Log__c(Process_Name__c='COD', 
                                                        Error_Details__c = e.getMessage() , 
                                                        DP_Invoices__c=DPInvoiceObj.Id != null? DPInvoiceObj.Id :null);
                errList.add(errlog);
                System.debug('===errList in method=== '+errList);
            }            
        }
    }
}