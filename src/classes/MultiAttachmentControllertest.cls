@isTest
public Class MultiAttachmentControllertest{
     
     private static testMethod void testController(){
     
     Inquiry__c inqObj= new Inquiry__c(
     //RecordTypeId=agenTeamRT,
     Inquiry_Source__c='Agent Referral',
     Mobile_Phone_Encrypt__c='456123',
     Mobile_CountryCode__c='American Samoa: 001684',
     Mobile_Phone__c='1234',
     Email__c='mk@gmail.com',
     First_Name__c='Test',
     Last_Name__c='Last',
     Meeting_Type__c = 'Scheduled Tour'
     );
            
     insert inqObj;
            
     Travel_Details__c td = new Travel_Details__c(Inquiry__c=inqObj.id);
     td.From_Date__c = Date.newInstance(2018,07,06);
     td.To_Date__c = Date.newInstance(2018,07,08);
     td.Status__c = 'Draft';
     td.Email_Sent_Time__c = datetime.newInstance(2018, 7,16, 10, 30, 0);
     insert td;
     
     
     Passenger_Details__c passenger = new Passenger_Details__c();
     //passenger.Age__c =23;
     passenger.Passenger_Name__c = 'testname';
     passenger.Travel_Details__c = td.id;
     insert passenger;
     
     MultiAttachmentController controller=new MultiAttachmentController();
     controller.sobjId=passenger.id;
     controller.contentType = 'image/jpeg';
     
     List<Attachment> newAtts=controller.newAttachments;
        newAtts[0].Name='Unit Test 1.jpeg';
        newAtts[0].Description='Unit Test 1';
        newAtts[0].Body=Blob.valueOf('Unit Test 1');
        //newAtts[0].ContentType='image/jpeg';
        
     controller.save();
     controller.getAttachments();
     Attachment attsaved = [Select id from Attachment limit 1];
     String attid = attsaved.id;
     //MultiAttachmentController.deleteAttachment(attid);   
     
    }

}