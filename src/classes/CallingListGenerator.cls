public without sharing class CallingListGenerator {


     //public void onAfterInsert( list<Case> lstNewCases ) {
    //     if( lstNewCases != null && !lstNewCases.isEmpty() ) {
    //         list<Case> lstCasesForCallingList = new list<Case>();
    //         for( Case objCase : lstNewCases ) {
    //             if( String.isNotBlank( objCase.Bounced_Reason__c ) && !objCase.Do_Not_Call__c ) {
    //                 lstCasesForCallingList.add( objCase );
    //             }
    //         }
    //         generateCallingList( lstCasesForCallingList );
    //     }
     //}
    public void onBeforeInsert( list<Case> lstNewCases ) {
        updateBouncedChequeOwnerOnCreation( lstNewCases );
    }

    /*public void onAfterUpdate( list<Case> lstUpdatedCases, map<Id, Case> mapOldCases ) {
        System.debug('=====INSIDE onAfterUpdate===');
        if( lstUpdatedCases != null && !lstUpdatedCases.isEmpty() &&
            mapOldCases != null && !mapOldCases.isEmpty() ) {
            list<Case> lstCasesForCallingList = new list<Case>();
            for( Case objCase : lstUpdatedCases ) {
                System.debug('=====INSIDE FOR===');
                if( objCase.Bounced_Reason__c != null &&
                    String.isBlank( mapOldCases.get( objCase.Id ).Bounced_Reason__c ) &&
                    !objCase.Do_Not_Call__c ) {
                        System.debug('=====INSIDE CALLING LIST===');
                    lstCasesForCallingList.add( objCase );
                }
            }
            generateCallingList( lstCasesForCallingList ) ;
        }
    }*/
    
    public void onAfterInsert(list<Case> lstCases) {
        System.debug('=====INSIDE onAfterInsert===');
        Id bouncedChequeRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Bounced Cheque SR').getRecordTypeId();
        list<Case> lstCasesForCallingList = new list<Case>();
        for(Case objCase: lstCases) {
            if(objCase.RecordTypeId == bouncedChequeRTId) {
                lstCasesForCallingList.add(objCase);
            }
        }
        if(!lstCasesForCallingList.isEmpty()) {
            
            generateCallingList( lstCasesForCallingList) ;
        }
    }

    public void generateCallingList( list<Case> lstCasesForCallingList ) {
        if( lstCasesForCallingList != null && !lstCasesForCallingList.isEmpty() ) {
            list<Calling_List__c> lstBounceChequeCalling = new list<Calling_List__c>();
            //Id ownerId = CallingListUtility.getGroupIdFromDeveloperName( 'Collection_Manager_Queue','Queue' );

            list<ID> lstCaseId = new list<ID>();
            for( Case objCase : lstCasesForCallingList ){
                lstCaseId.add(objCase.id);
            }
            //Callout to update Case
            //CallingListGenerator.updateCase_IsEHOCasefield(lstCaseId);
            
            list<Case> lstCase = new list<Case>();
            lstCase = [Select Id,AccountId, Account.IsPersonAccount, Account.Email__c, Account.Email__pc, Account.Name, CaseNumber, SR_Type__c, Account.Party_ID__c,
                       Booking_Unit__c,RecordType.developerName, OwnerId,
                       (select id,Case__c,Booking_Unit__c from SR_Booking_Units__r where Booking_Unit__c != null)
                       from Case
                       Where Booking_Unit__c = null
                       AND id IN: lstCaseId];

            /*map<ID,List<SR_Booking_Unit__c>> mapSRBookingUnit = new map<ID,List<SR_Booking_Unit__c>>();
            map<ID,List<ID>> mapOpenSR = new map<ID,List<ID>>();
            list<ID> lstBookingUnitID = new list<ID>();
            list<SR_Booking_Unit__c> lstSRbookingUnit = new list<SR_Booking_Unit__c>();

            for(Case objCase : lstCase){
                if(objCase.SR_Booking_Units__r.Size()>0){
                    mapSRBookingUnit.put(objCase.id,objCase.SR_Booking_Units__r);
                    for(SR_Booking_Unit__c objSRbooking : objCase.SR_Booking_Units__r){
                        lstBookingUnitID.add(objSRbooking.Booking_Unit__c);
                    }
                }
            }
            if(!lstBookingUnitID.isEmpty()){
                lstSRbookingUnit = [Select id,Case__c,Case__r.id,Case__r.SR_Type__c,Case__r.CaseNumber,
                                   Case__r.RecordType.developerName,
                                   Booking_Unit__c from SR_Booking_Unit__c where
                                   Booking_Unit__c IN : lstBookingUnitID
                                   AND Booking_Unit__c != null
                                   AND Case__c != null
                                   AND Case__r.Status != 'Closed'
                                   AND Case__r.Status != 'Rejected'
                                   AND Case__r.RecordType.developerName = 'AOPT'];

            }

            for(SR_Booking_Unit__c objSRbookingUnit : lstSRbookingUnit){
                if(mapOpenSR.containsKey(objSRbookingUnit.Booking_Unit__c)){
                    list<ID> lstBookingUnitCasesID = new list<ID>();
                    lstBookingUnitCasesID = mapOpenSR.get(objSRbookingUnit.Booking_Unit__c);
                    lstBookingUnitCasesID.add(objSRbookingUnit.Case__r.id);
                    mapOpenSR.put(objSRbookingUnit.Booking_Unit__c,lstBookingUnitCasesID);
                }
                else{
                    list<ID> lstBookingUnitNewCaseID = new list<ID>();
                    lstBookingUnitNewCaseID.add(objSRbookingUnit.Case__r.id);
                    mapOpenSR.put(objSRbookingUnit.Booking_Unit__c,lstBookingUnitNewCaseID);
                }
            }

            for( Case objCase : lstCasesForCallingList ) {
                Calling_List__c objCalling = new Calling_List__c();
                objCalling.Calling_List_Type__c = 'BC Calling';
                objCalling.Case__c = objCase.Id ;
                objCalling.Calling_List_Status__c = 'New';


                if(!lstSRbookingUnit.isEmpty()){
                    if(mapSRBookingUnit.containsKey(objCase.id)){
                        list<SR_Booking_Unit__c> lstSRBookingUnitCase = new list<SR_Booking_Unit__c>();
                        lstSRBookingUnitCase = mapSRBookingUnit.get(objCase.id);
                        objCalling.AOPT_Open_Cases_ID__c = '';
                        Set<ID> setAOPTid = new Set<ID>();

                        for(SR_Booking_Unit__c objSRbooking : lstSRBookingUnitCase){
                            if(mapOpenSR.containsKey(objSRbooking.booking_unit__c)){
                                objCalling.AOPT_Under_Process__c = true;
                                List<ID> lstID = new List<ID>();
                                lstID = mapOpenSR.get(objSRbooking.booking_unit__c);
                                setAOPTid.addAll(lstID);

                            }
                        }
                        for(ID objID : setAOPTid){
                            objCalling.AOPT_Open_Cases_ID__c += objID+',';
                        }
                        if(objCalling.AOPT_Open_Cases_ID__c.Contains(',')){
                            objCalling.AOPT_Open_Cases_ID__c = objCalling.AOPT_Open_Cases_ID__c.removeEnd(',');
                        }
                    }
                }

                if( objCase.AccountId != null ) {
                    objCalling.Account__c = objCase.AccountId ;
                }
                if( String.isNotBlank( ownerId ) ) {
                    objCalling.OwnerId = ownerId ;
                }
                objCalling.Call_Date__c = system.now();
                lstBounceChequeCalling.add( objCalling );
            }

            if( lstBounceChequeCalling != null && !lstBounceChequeCalling.isEmpty() ) {
                insert lstBounceChequeCalling ;

                list<AOPT_Case__c> lstAOPTcase = new list<AOPT_Case__c>();

                for(Calling_List__c objCall : lstBounceChequeCalling){
                    if(String.isNotBlank(objCall.AOPT_Open_Cases_ID__c)){
                        list<String> lstAOPTcaseId = new list<String>();
                        if(objCall.AOPT_Open_Cases_ID__c.contains(',')){
                            lstAOPTcaseId = objCall.AOPT_Open_Cases_ID__c.split(',');
                        }
                        else{
                            lstAOPTcaseId.add(objCall.AOPT_Open_Cases_ID__c); // if only 1 id present
                        }
                        for(String strCaseId : lstAOPTcaseId){
                            AOPT_Case__c objAOPTcase = new AOPT_Case__c();
                            objAOPTcase.Case__c = strCaseId;
                            objAOPTcase.Calling_List__c = objCall.id;
                            lstAOPTcase.add(objAOPTcase);
                        }

                    }
                }
                if(lstAOPTcase.Size()>0){
                    insert lstAOPTcase;
                }
            }*/
            Id idBCCallingListRT = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Bounced Cheque').getRecordTypeId();
            System.debug('=====lstCase===' + lstCase);
            for(Case objCase: lstCase) {
                Calling_List__c objCallList = new Calling_List__c();
                objCallList.RecordTypeId = idBCCallingListRT;
                objCallList.Case__c = objCase.Id;
                objCallList.Account__c = objCase.AccountId;
                objCallList.Calling_List_Type__c = 'BC Calling';
                objCallList.Calling_List_Status__c = 'New';
                //objCallList.Call_Back_Date__c = system.now();
                objCallList.Customer_Flag__c = true;
                objCallList.Account_Email__c = objCase.Account.IsPersonAccount ? objCase.Account.Email__pc : objCase.Account.Email__c ;
                objCallList.Customer_Name__c = objCase.Account.Name;
                objCallList.OwnerId = objCase.OwnerId;
                objCallList.Party_ID__c = objCase.Account.Party_ID__c;
                lstBounceChequeCalling.add(objCallList);
            }

            if(!lstBounceChequeCalling.isEmpty()) {
                insert lstBounceChequeCalling;
            }
        }
    }
    
    /*
    @future(Callout=true)
    public static void updateCase_IsEHOCasefield(List<Id> setCaseIds) {
        Id bouncedChequeRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Bounced Cheque SR').getRecordTypeId();
        Set<String> setRegIds = new Set<String>();
        List<Case> lstCases = new List<Case>();
        List<Case> lstCasesToUpdate = new List<Case>();
        Map<String, Boolean> mapRegIdStatus = new Map<String, Boolean>();
        //Map<String, String> mapRegId_CaseID = new Map<String, String>();

        lstCases = [SELECT Id,
                    (SELECT Booking_Unit__r.Registration_ID__c
                    FROM SR_Booking_Units__r
                    WHERE Booking_Unit__r.Registration_ID__c != NULL)
                    FROM Case
                    WHERE ID IN: setCaseIds
                    AND RecordTypeId =: bouncedChequeRTId];


        for(Case objCase : lstCases) {
            for(SR_Booking_Unit__c srBU : objCase.SR_Booking_Units__r) {
                setRegIds.add(srBU.Booking_Unit__r.Registration_ID__c);
                //mapRegId_CaseID.put(srBU.Booking_Unit__r.Registration_ID__c, objCase.ID);
            }
        }


        //Callout to get unit status for the booking units
        for(String regId : setRegIds) {
            UnitDetailsService.BookinUnitDetailsWrapper bookingWrapperObj = UnitDetailsService.getBookingUnitDetails(regId);

            System.debug('==bookingWrapperObj===' + bookingWrapperObj);
            if(bookingWrapperObj != NULL && String.isNotBlank(bookingWrapperObj.strPaidPercent)
                        && String.isNotBlank(bookingWrapperObj.strHOFlag)
                        && String.isNotBlank(bookingWrapperObj.strEHOFlag)) {
                            System.debug('==IN IF===' + Decimal.valueOf(bookingWrapperObj.strPaidPercent));
                if((bookingWrapperObj.strHOFlag.containsIgnoreCase('Y') || bookingWrapperObj.strEHOFlag.containsIgnoreCase('Y'))
                        && Decimal.valueOf(bookingWrapperObj.strPaidPercent) < 99.5) {
                            System.debug('==IN IF===' + Decimal.valueOf(bookingWrapperObj.strPaidPercent));
                            mapRegIdStatus.put(regId, true);
                }
            }
        }

        for(Case objCase: lstCases) {
            Case objCaseToUpdate = new Case(Id = objCase.Id);
            objCaseToUpdate.Is_EHO_Case__c = false;
            for(SR_Booking_Unit__c objSRBU: objCase.SR_Booking_Units__r) {
                if(mapRegIdStatus.containsKey(objSRBU.Booking_Unit__r.Registration_ID__c)) {

                    objCaseToUpdate.Is_EHO_Case__c = true;
                    lstCasesToUpdate.add(objCaseToUpdate);
                    break;
                }
            }
        }
        if( !lstCasesToUpdate.isEmpty()) {
            try {
                Update lstCasesToUpdate;
            }
            catch(System.DmlException excp) {
                System.debug('===excp===' + excp.getMessage());
            }
        }
    }*/

    //Method to assign owner of case on the basis of related Collection Calling list
    public void updateBouncedChequeOwnerOnCreation( List<Case> lstCase ) {
        Id devRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Collections Calling List').getRecordTypeId();
        Id bouncedChequeRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Bounced Cheque SR').getRecordTypeId();
    
        Map<Id,List<Calling_List__c>> mapAccIdToobjCalling_List  = new Map<Id,List<Calling_List__c>>();
        List<Case> lstCaseToUpdate = new List<Case>();
        set<Id> setAccId = new set<Id>();
        set<Id> setCaseId = new set<Id>();
        
        if (lstCase != null && lstCase.size() > 0) {
            for( Case objCase : lstCase) {
                if( objCase.RecordTypeId == bouncedChequeRTId && objCase.AccountId != null ) {
                    setAccId.add( objCase.AccountId );
                    setCaseId.add( objCase.Id );
                }
            } //End of for
            
            if (setAccId != null && setAccId.size() > 0) {
                for( Calling_List__c objCalling_List : [ Select Id
                                                              , RecordTypeId
                                                              , Account__c
                                                              , OwnerId
                                                           From Calling_List__c
                                                          WHERE RecordTypeId =: devRecordTypeId 
                                                            AND Account__c IN: setAccId
                                                            AND IsHideFromUI__c = false
                                                            AND Customer_Flag__c = true
                                                            AND Calling_List_Status__c != 'Closed'] ) {
                    if( string.valueOf(objCalling_List.OwnerId).startsWith('005') ) {
                        if( mapAccIdToobjCalling_List.containsKey(objCalling_List.Account__c) && mapAccIdToobjCalling_List.get( objCalling_List.Account__c ) != null ) {
                            List<Calling_List__c> lstCL = mapAccIdToobjCalling_List.get( objCalling_List.Account__c );
                            lstCL.add(objCalling_List);
                            mapAccIdToobjCalling_List.put( objCalling_List.Account__c,lstCL );
                        } else {
                            mapAccIdToobjCalling_List.put( objCalling_List.Account__c,new List<Calling_List__c>{objCalling_List});
                        }
                    }
                }
                
                Id collectionQueueId = EmailMessageTriggerHandler.getGroupIdFromDeveloperName( 'Collection_Queue','Queue' );
                Id requestForEliteQueueId = EmailMessageTriggerHandler.getGroupIdFromDeveloperName( 'Request_for_Elite','Queue' );

                
                
                for( Case objCase : lstCase ) {
                    if( objCase.RecordTypeId == bouncedChequeRTId && objCase.AccountId != null ) {
                        if( mapAccIdToobjCalling_List.containsKey(objCase.AccountId) ) {
                            objCase.OwnerId =  mapAccIdToobjCalling_List.get(objCase.AccountId)[0].OwnerId;
                        }else if( objCase.Customer_Category__c == 'Elite' 
                                || objCase.Customer_Category__c == 'Top Broker' ) {
                            objCase.OwnerId =  requestForEliteQueueId;
                        }
                        else {
                            objCase.OwnerId =  collectionQueueId;
                        }
                    }
                }
                
            }
            

            

            
        } //End of main if
    }    
    

}