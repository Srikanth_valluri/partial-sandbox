// Class used to fetch all the Sr attachments of Parent Case
public class FetchSRAttachmentController {
    
    // Variable to store SR Id
    public String childSRId                                 {get; set;}
    // List to store parent SR Attachments
    public List<SR_Attachments__c> lstParentSRAttachments   {get; set;}
    // Object to Store child and Parent Sr details
    public Case childAndParetSRDetails                      {get; set;}
    
    
    public FetchSRAttachmentController(ApexPages.StandardController controller) {
        childSRId = ApexPages.currentPage().getParameters().get('id');
    } // End of Constructor
    
    public void init() {
        lstParentSRAttachments = new List<SR_Attachments__c>();
        childAndParetSRDetails =  getChildAndParentSRDetails(childSRId);
        system.debug('----------childAndParetSRDetails : '+ childAndParetSRDetails );
        if( childAndParetSRDetails.ParentId != null ) {
            for (SR_Attachments__c objSR_Attachments : [SELECT Id, 
                                                         Name, 
                                                         Type__c, 
                                                         IsRequired__c, 
                                                         isValid__c, 
                                                         Supporting_Document__c,
                                                         View__c, 
                                                         Attachment_URL__c,
                                                         Attachment__c
                                                    FROM SR_Attachments__c
                                                   WHERE Case__c != null 
                                                     AND Case__c = :childAndParetSRDetails.ParentId ]
            ) {
                System.Debug('=====objSR_Attachments : ' + objSR_Attachments);
                lstParentSRAttachments.add(objSR_Attachments);
            }
            
            System.Debug('=====lstParentSRAttachments : ' + lstParentSRAttachments);
            
            if( lstParentSRAttachments.size() <= 0 ) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'No SR Attachment to display.'));
            }
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please Populate Parent SR'));
        }
    }//End of init method
    
    public Case getChildAndParentSRDetails( String childSRId ) {
        Case currentSR = [ SELECT Id
                                , ParentId
                                , AccountId
                             FROM Case
                            WHERE Id = :childSRId
                         ];
        return currentSR;
    }//End of getChildAndParentSRDetails method
    
}// End of FetchSRAttachmentController class