@isTest
public class Damac_RmRosterController_Test {
    @isTest static void RmRoasterMethod ()
    {
        RM_Roster__c roaster = new RM_Roster__c();
        
        roaster.Date__c = system.today();
        roaster.Selected__c = false;
        roaster.UniqueId__c = system.today() + '-' + userInfo.getUserId();
        roaster.Available_RM__c = userInfo.getUserId();
        insert roaster;
        
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(roaster);
        Damac_RmRosterController roasterController = new Damac_RmRosterController(sc);
        roasterController.record = roaster;
        roasterController.selectedRms = roaster.Id;
        roasterController.filterRecords();
        roasterController.saveRMRoster();
        roasterController.getRMs();
        roasterController.getAvailableRms ();
        
        
        Stand_Inquiry__c inq = new Stand_Inquiry__c ();
            inq.First_Name__c = 'Test on Jan07-';
        inq.Assigned_pc__c = UserInfo.getUserId();
        inq.Tour_Date_Time__c = DateTime.Now();
        insert inq;
        
        Test.stopTest();
        
    }
    @isTest static void RmRoasterMethod2 ()
    {
        RM_Roster__c roaster = new RM_Roster__c();
        
        roaster.Date__c = system.today();
        roaster.Selected__c = false;
        roaster.UniqueId__c = system.today() + '-' + userInfo.getUserId();
        roaster.Available_RM__c = userInfo.getUserId();
        insert roaster;
        
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(roaster);
        Damac_RmRosterController roasterController = new Damac_RmRosterController(sc);
        roasterController.record = roaster;
        roasterController.filterRecords();
        
        
        Test.stopTest();
        
    }
    @isTest static void RmRoasterMethod3 ()
    {
        RM_Roster__c roaster = new RM_Roster__c();
        
        roaster.Date__c = system.today();
        roaster.Selected__c = false;
        roaster.UniqueId__c = system.today() + '-' + userInfo.getUserId();
        roaster.Available_RM__c = userInfo.getUserId();
        insert roaster;
        
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(roaster);
        Damac_RmRosterController roasterController = new Damac_RmRosterController(sc);
        roasterController.record = roaster;
        roasterController.record.Date__c = null;
        roasterController.selectedRms = roaster.Id;
        roasterController.filterRecords();
        
        
        Test.stopTest();
        
    }

}