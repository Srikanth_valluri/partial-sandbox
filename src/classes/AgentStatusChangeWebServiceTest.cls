@isTest
private class AgentStatusChangeWebServiceTest{

    @isTest static void userStatusUpdateCheck(){

        User userObj = new User(
                             ProfileId = [SELECT Id FROM Profile WHERE Name = 'Property Consultant'].Id,
                             LastName = 'test',
                             Email = 'puser000@test.com',
                             Username = 'puser000@test.com' + System.currentTimeMillis(),
                             CompanyName = 'TEST',
                             Title = 'title',
                             isActive = true,
                             Alias = 'alias',
                             Extension = '7001',
                             TimeZoneSidKey = 'Asia/Dubai',
                             EmailEncodingKey = 'UTF-8',
                             LanguageLocaleKey = 'en_US',
                             LocaleSidKey = 'en_IN'
        );
        insert userObj ;

        AgentStatusChangeLogic.AgentStatusChangeRequestBody reqst = new AgentStatusChangeLogic.AgentStatusChangeRequestBody();
        reqst.dateAndtime = '11/6/2014 12:00:00';
        reqst.creExtension = '7001';
        reqst.newStatus = 'Lunch Break';

        String JsonMsg=JSON.serialize(reqst);

        Test.startTest();

        //As Per Best Practice it is important to instantiate the Rest Context

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/agentStatus';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;

//        Test.setMock(HttpCalloutMock.class, new CallLogWebServiceMock());

        AgentStatusChangeLogic.AgentStatusChangeRequestBody resp = new AgentStatusChangeLogic.AgentStatusChangeRequestBody();
        AgentStatusChangeWebService.doPost();
        Test.stopTest();
        User userObj1 = [Select Id,Status__c From User where Id =: userObj.Id ];
        System.assertNotEquals(null,userObj1);
        System.assertEquals(userObj1.Status__c , 'Lunch Break');
    }

     @isTest static void userStatusUpdateInvalidUserCheck(){

        AgentStatusChangeLogic.AgentStatusChangeRequestBody reqst = new AgentStatusChangeLogic.AgentStatusChangeRequestBody();
        reqst.dateAndtime = '11/6/2014 12:00:00';
        reqst.creExtension = '70001';
        reqst.newStatus = 'Lunch Break';

        String JsonMsg=JSON.serialize(reqst);

        Test.startTest();

        //As Per Best Practice it is important to instantiate the Rest Context

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/agentStatus';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;

//        Test.setMock(HttpCalloutMock.class, new CallLogWebServiceMock());

        AgentStatusChangeLogic.AgentStatusChangeRequestBody resp = new AgentStatusChangeLogic.AgentStatusChangeRequestBody();
        AgentStatusChangeWebService.doPost();
        Test.stopTest();

        String agentStatusResponse = RestContext.response.responseBody.toString();
        System.assert(agentStatusResponse != null);
        AgentStatusChangeLogic.AgentStatusChangeResponseBody response=
            (AgentStatusChangeLogic.AgentStatusChangeResponseBody) JSON.deserializeStrict(
                agentStatusResponse ,
                AgentStatusChangeLogic.AgentStatusChangeResponseBody.Class
            );
        //System.assertEquals(response.errorCode, '400');
        System.assertEquals(response.errorMessage, 'No CRE User found for the given extension');
    }

    @isTest static void badRequest(){

        User userObj = new User(
                             ProfileId = [SELECT Id FROM Profile WHERE Name = 'Property Consultant'].Id,
                             LastName = 'test',
                             Email = 'puser000@test.com',
                             Username = 'puser000@test.com' + System.currentTimeMillis(),
                             CompanyName = 'TEST',
                             Title = 'title',
                             isActive = true,
                             Alias = 'alias',
                             Extension = '7001',
                             TimeZoneSidKey = 'Asia/Dubai',
                             EmailEncodingKey = 'UTF-8',
                             LanguageLocaleKey = 'en_US',
                             LocaleSidKey = 'en_IN'
        );
        insert userObj ;

        userObj.Status__c = '';
        update userObj;

       AgentStatusChangeLogic.AgentStatusChangeRequestBody reqst = new AgentStatusChangeLogic.AgentStatusChangeRequestBody();
       reqst.dateAndtime = '11/6/201490000 ';
       reqst.creExtension = '7009ui78787878';
       reqst.newStatus = '';

       String JsonMsg=JSON.serialize(reqst);

       Test.startTest();

       //As Per Best Practice it is important to instantiate the Rest Context

       RestRequest req = new RestRequest();
       RestResponse res = new RestResponse();

       req.requestURI = '/services/apexrest/agentStatus';
       req.httpMethod = 'POST';
       req.requestBody = Blob.valueof(JsonMsg);
       RestContext.request = req;
       RestContext.response= res;

//       Test.setMock(HttpCalloutMock.class, new CallLogWebServiceMock());

       AgentStatusChangeLogic.AgentStatusChangeRequestBody resp = new AgentStatusChangeLogic.AgentStatusChangeRequestBody();
       AgentStatusChangeWebService.doPost();
       Test.stopTest();

       String agentStatusResponse = RestContext.response.responseBody.toString();
       System.assert(agentStatusResponse != null);
       AgentStatusChangeLogic.AgentStatusChangeResponseBody response=
           (AgentStatusChangeLogic.AgentStatusChangeResponseBody) JSON.deserializeStrict(
               agentStatusResponse ,
               AgentStatusChangeLogic.AgentStatusChangeResponseBody.Class
           );
       //System.assertEquals(response.errorCode, '400');
       System.assertEquals(response.errorMessage, 'No CRE User found for the given extension');
   }


}