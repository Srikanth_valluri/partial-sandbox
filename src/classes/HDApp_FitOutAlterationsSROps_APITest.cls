@isTest
public class HDApp_FitOutAlterationsSROps_APITest {
    @isTest
    static void testFitOutAlterationSR(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/fitOutAlterationsOps'; 
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        HDApp_FitOutAlterationsSROps_API.getSRTypeDetails();
        Test.stopTest();
    }
    
    @isTest
    static void testFitOutAlterationSRBlankUnit(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/fitOutAlterationsOps'; 
        req.httpMethod = 'GET';
        req.addParameter('bookingUnitId', '');
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        HDApp_FitOutAlterationsSROps_API.getSRTypeDetails();
        Test.stopTest();
    }

     @isTest
    static void testFitOutAlterationSRGETMethod(){

        Account objAccount = new Account();
        objAccount.Name = 'test';
        insert objAccount;
        
        Property__c objProperty = new Property__c();
        objProperty.Property_ID__c = 1234567;
        insert objProperty;

        Location__c objLoc = TestDataFactoryFM.createLocation();
        objLoc.New_Ho_Doc_Type__c = 'Villa';
        Upsert objLoc;
        
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Building_Location__c = objLoc.id;
        insert objInventory;
        
        //NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //insert objSR;
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert objSR; 
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        objBooking.Account__c = objAccount.Id;
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Registration_ID__c = '3901';
        objBU.Booking__c = objBooking.Id;
        objBU.Inventory__c = objInventory.Id;
        insert objBU;

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/fitOutAlterationsOps'; 
        req.httpMethod = 'GET';
        req.addParameter('bookingUnitId', objBU.Id);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        HDApp_FitOutAlterationsSROps_API.getSRTypeDetails();
        Test.stopTest();
    }

    @isTest
    static void testFitOutDraftSRFmCasePut(){
        Account objAccount = new Account();
        objAccount.Name = 'test';
        insert objAccount;
        
        Property__c objProperty = new Property__c();
        objProperty.Property_ID__c = 1234567;
        insert objProperty;
        
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        insert objInventory;
        
        //NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //insert objSR;
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert objSR; 
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        objBooking.Account__c = objAccount.Id;
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Registration_ID__c = '3901';
        objBU.Booking__c = objBooking.Id;
        objBU.Inventory__c = objInventory.Id;
        insert objBU;
        
        String jsonString =  '   {  '  + 
                             '       "action" : "draft",  '  + 
                             '       "id" : "",  '  + 
                             '       "booking_unit_id" : "'+objBU.id+'",  '  + 
                             '       "account_id" : "'+objAccount.id+'",  '  + 
                             '       "type_of_work_id" : 2,  '  + 
                             '       "type_of_work" : "Swimming pool",  '  + 
                             '       "noc_type" : "Authority Noc",  '  + 
                             '       "collection_by" : "Self",  '  + 
                             '       "description" : "Testing",  '  + 
                             '       "contractor_name" : "test",  '  + 
                             '       "contractor_email" : "test@test.com",  '  + 
                             '       "contractor_mobile_country_code" : "India: 0091",  '  + 
                             '       "contractor_mobile" : "12345687"  '  + 
                             '  }  ' ; 
        

        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/fitOutAlterationsOps'; 
        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueof(jsonString);
        req.addParameter('bookingUnitId', objBU.id);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.starttest();
        //HDApp_FitOutAlterationsSROps_API.getSRTypeDetails();
        HDApp_FitOutAlterationsSROps_API.createDraftFitOutSR();
        Test.stopTest();
    }

    @isTest
    static void testFitOutDraftSRFmCasePut2() {

        String jsonString =  '   {  '  + 
                             '       "action" : "",  '  + 
                             '       "id" : "",  '  + 
                             '       "booking_unit_id" : "1234",  '  + 
                             '       "account_id" : "1234",  '  + 
                             '       "type_of_work_id" : 2,  '  + 
                             '       "type_of_work" : "Swimming pool",  '  + 
                             '       "noc_type" : "Authority Noc",  '  + 
                             '       "collection_by" : "Self",  '  + 
                             '       "description" : "Testing",  '  + 
                             '       "contractor_name" : "test",  '  + 
                             '       "contractor_email" : "test@test.com",  '  + 
                             '       "contractor_mobile_country_code" : "India: 0091",  '  + 
                             '       "contractor_mobile" : "12345687"  '  + 
                             '  }  ' ; 
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/fitOutAlterationsOps'; 
        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueof(jsonString);
        //req.addParameter('bookingUnitId', objBU.id);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.starttest();
        HDApp_FitOutAlterationsSROps_API.createDraftFitOutSR();
        Test.stopTest();
    }

    @isTest
    static void testFitOutDraftSRFmCasePut3() {

        String jsonString = '';
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/fitOutAlterationsOps'; 
        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueof(jsonString);
        //req.addParameter('bookingUnitId', objBU.id);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.starttest();
        HDApp_FitOutAlterationsSROps_API.createDraftFitOutSR();
        Test.stopTest();
    }
    
     @isTest
    static void testFitOutDraftSRFmCasePatch(){
        Account objAccount = new Account();
        objAccount.Name = 'test';
        insert objAccount;
        
        Property__c objProperty = new Property__c();
        objProperty.Property_ID__c = 1234567;
        insert objProperty;
        
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        insert objInventory;
        
        //NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //insert objSR;
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert objSR; 
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        objBooking.Account__c = objAccount.Id;
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Registration_ID__c = '3901';
        objBU.Booking__c = objBooking.Id;
        objBU.Inventory__c = objInventory.Id;
        insert objBU;
        
        FM_Case__c objFMCase = new FM_Case__c();
        objFMCase.Type_of_Alteration__c = 'Lighting';
        objFMCase.Type_of_NOC__c = 'NOC for Fit-out';
        objFMCase.Description__c = 'Tesing NOC';
        objFMCase.Person_To_Collect__c = 'Contractor';
        objFMCase.Contact_person_contractor__c = 'Jyotika';
        objFMCase.Email_2__c = 'xyz@gmsil.com';
        objFMCase.Mobile_Country_Code_2__c = 'India: 0091';
        objFMCase.Mobile_no_contractor__c = '7895264785';
        insert objFMCase;
        
        String jsonString =      '   {  '  + 
                                 '       "action" : "submit",  '  + 
                                 '       "id" : "'+objFMCase.Id+'",  '  + 
                                 '       "receipt_number" : "12365456",  '  + 
                                 '       "receipt_url" : "https://ptctest.damacgroup.com/COFFEE/apex/document/view/782cb06e304b3dc91cc45120f75a3b09"  '  + 
                                 '  }  ' ;  
        

        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/fitOutAlterationsOps'; 
        req.httpMethod = 'PATCH';
        req.requestBody = Blob.valueof(jsonString);
        req.addParameter('bookingUnitId', objBU.id);
        RestContext.request = req;
        RestContext.response = res;
        
        
        Test.startTest();
        //HDApp_FitOutAlterationsSROps_API.clsDataCreator(objFmCase);
        HDApp_FitOutAlterationsSROps_API.submitFitOutSR();
        Test.stopTest();
    }

    @isTest
    static void testFitOutDraftSRFmCasePatch2(){

        String jsonString = '' ;

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/fitOutAlterationsOps'; 
        req.httpMethod = 'PATCH';
        req.requestBody = Blob.valueof(jsonString);
        //req.addParameter('bookingUnitId', objBU.id);
        RestContext.request = req;
        RestContext.response = res;
        
        
        Test.startTest();
        HDApp_FitOutAlterationsSROps_API.submitFitOutSR();
        Test.stopTest();
    }

    @isTest
    static void testFitOutDraftSRFmCasePatch3(){

        String jsonString =      '   {  '  + 
                                 '       "action" : "",  '  + 
                                 '       "id" : "1234",  '  + 
                                 '       "receipt_number" : "12365456",  '  + 
                                 '       "receipt_url" : "https://ptctest.damacgroup.com/COFFEE/apex/document/view/782cb06e304b3dc91cc45120f75a3b09"  '  + 
                                 '  }  ' ; 

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/fitOutAlterationsOps'; 
        req.httpMethod = 'PATCH';
        req.requestBody = Blob.valueof(jsonString);
        //req.addParameter('bookingUnitId', objBU.id);
        RestContext.request = req;
        RestContext.response = res;
        
        
        Test.startTest();
        HDApp_FitOutAlterationsSROps_API.submitFitOutSR();
        Test.stopTest();
    }

    @isTest
    static void testFitOutDraftSRFmCasePatch4(){

        String jsonString =      '   {  '  + 
                                 '       "action" : "submit",  '  + 
                                 '       "id" : "",  '  + 
                                 '       "receipt_number" : "12365456",  '  + 
                                 '       "receipt_url" : "https://ptctest.damacgroup.com/COFFEE/apex/document/view/782cb06e304b3dc91cc45120f75a3b09"  '  + 
                                 '  }  ' ; 

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/fitOutAlterationsOps'; 
        req.httpMethod = 'PATCH';
        req.requestBody = Blob.valueof(jsonString);
        //req.addParameter('bookingUnitId', objBU.id);
        RestContext.request = req;
        RestContext.response = res;
        
        
        Test.startTest();
        HDApp_FitOutAlterationsSROps_API.submitFitOutSR();
        Test.stopTest();
    }
    
     @isTest
    static void testFitOutDraftSRFmCasePost(){
        Account objAccount = new Account();
        objAccount.Name = 'test';
        insert objAccount;
        
        Property__c objProperty = new Property__c();
        objProperty.Property_ID__c = 1234567;
        insert objProperty;
        
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        insert objInventory;
        
        //NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //insert objSR;
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert objSR; 
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        objBooking.Account__c = objAccount.Id;
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Registration_ID__c = '3901';
        objBU.Booking__c = objBooking.Id;
        objBU.Inventory__c = objInventory.Id;
        insert objBU;
        
        //FM_Case__c objFMCase = new FM_Case__c();
        //objFMCase.Type_of_Alteration__c = 'Lighting';
        //objFMCase.Type_of_NOC__c = 'NOC for Fit-out';
        //objFMCase.Description__c = 'Tesing NOC';
        //objFMCase.Person_To_Collect__c = 'Contractor';
        //objFMCase.Contact_person_contractor__c = 'Jyotika';
        //objFMCase.Email_2__c = 'xyz@gmsil.com';
        //objFMCase.Mobile_Country_Code_2__c = 'India: 0091';
        //objFMCase.Mobile_no_contractor__c = '7895264785';
        //insert objFMCase;
        
        String jsonString =      '   {  '  + 
                                 '       "booking_unit_id" : "'+objBU.id+'",  '  + 
                                 '       "type_of_work_id" : 1,  '  + 
                                 '       "type_of_work" : "Swimming pool"  '  + 
                                 '  }  ' ; 
        

        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/fitOutAlterationsOps'; 
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(jsonString);
        //req.addParameter('bookingUnitId', objBU.id);
        RestContext.request = req;
        RestContext.response = res;
      
        
        Test.startTest();
        HDApp_FitOutAlterationsSROps_API.getWorkTypeAmountDetails();
        
        Test.stopTest();
    }
}