/**************************************************************************************************
* Name               : CommissionSlabReminderScheduler
* Description        : Scheduler class for CommissionSlabReminderBatch class.
* Created Date       : 17/11/2020
* Created By         : QBurst
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst           17/11/2020      Initial Draft.
**************************************************************************************************/
public class CommissionSlabReminderScheduler implements Schedulable {
  public void execute(SchedulableContext SC) {
    CommissionSlabReminderBatch reminderBatch = new CommissionSlabReminderBatch();
    Database.executeBatch(reminderBatch); 
  }
}// End of class.