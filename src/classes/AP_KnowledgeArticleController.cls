public class AP_KnowledgeArticleController {
    public static String labelVal {get; set;}
    public String parentCategory {get; set;}
    public String videoSrc {get; set;}
    public String suCategoryName {get; set;}
    public List<String> parentCatList {get; set;}
    public String knowledgeId  {get; set;}
    public Map<String,String> subCategoryLabelAndNameMap {get; set;}
    public Map<String,String> mapOfTitleAndDocs {get; set;}
   // public Map<String,list<Test_Knowledge__kav>> subCatAndKnowArtMap {get; set;}
    public Map<String,List<myObject>> subCatAndKnowArtMap {get; set;}
    public static  Map<String, List<String>> dataCategoriesAndSubCatMap {get; set;}
    public List<DataCategoryUtil.RootObject> dataCategoriesStructure {get; set;}
    public Set<String> parentCatName {get; set;}
    public String strSelectedLanguage {get; set;}
    private static Set<String> articleFieldSets = new Set<String> {
        'Id','Title','Description__c',
        'KnowledgeArticleId','PublishStatus',
        'Language','CreatedDate'
    };
    public AP_KnowledgeArticleController () {
        parentCatName = new Set<String>();
         parentCatList = new List<String>();
        knowledgeId = checkStringValueAndAssign(apexpages.currentpage().getparameters().get('knowledgeId'));
        strSelectedLanguage = apexpages.currentPage().getParameters().get('langCode');
        system.debug('knowledgeId===' + knowledgeId);
        //labelVal = apexpages.currentpage().getparameters().get('dataCategory');
        mapOfTitleAndDocs = new Map<String,String>();
        fetchSubCatAndArticlesOnLoad();
        Map<String,AP_AgentPortalDocs__c> agentPortalDocsMap = AP_AgentPortalDocs__c.getall();
        for(AP_AgentPortalDocs__c apDocs : agentPortalDocsMap.values()){
           
                mapOfTitleAndDocs.put(apDocs.Title__c,apDocs.Source__c);
                      
                
        }
        system.debug('>>>>>mapOfTitleAndDocs>>'+mapOfTitleAndDocs);
     
       // getDataCategoriesStructure();
    }
    public String checkStringValueAndAssign(String paramVal) {
        if(String.isNotBlank(paramVal)) {
            paramVal = paramVal;

        } else {
            paramVal = '';
        }
        return paramVal;
    }
    public  Void getDataCategoriesStructure() {
        dataCategoriesStructure =
            DataCategoryUtil.getDataCategoriesList();
        system.debug('>>>>>dataCategoriesStructure>>>'+dataCategoriesStructure);
        for(DataCategoryUtil.RootObject dataCategory : dataCategoriesStructure) {
            parentCatList.add(dataCategory.name);
        }
        system.debug('>>>>>parentCatList>>>'+parentCatList);
        //fetchSubCatAndArticlesOnLoad(parentCatList[0]);

    }
    
    public   void fetchSubCatAndArticlesOnLoad() {
        dataCategoriesAndSubCatMap = new Map<String, List<String>>();
        subCatAndKnowArtMap = new Map<String,List<myObject>>();
        parentCategory = Label.AP_FAQ;
        system.debug('>>>>>parentVal>>>'+parentCategory);
        system.debug('>>>>>managerIdWithInqCountmap>>>'+dataCategoriesAndSubCatMap);
        List<DataCategoryUtil.RootObject> dataCategoriesStructureDisplay = new List<DataCategoryUtil.RootObject>();
        dataCategoriesStructureDisplay = DataCategoryUtil.getDataCategoriesList();
        system.debug('>>>>>dataCategoriesStructureDisplay>>>'+dataCategoriesStructureDisplay);
        for(DataCategoryUtil.RootObject dataCategory : dataCategoriesStructureDisplay) {
            system.debug('>>>>>dataCategory.topCategories[0].childCategories>>>'+dataCategory.topCategories[0].childCategories);
          for( DataCategoryUtil.ChildCategory childCategory :
                dataCategory.topCategories[0].childCategories
            ) {
                if(dataCategoriesAndSubCatMap.containsKey(dataCategory.name)){
                    //subCategoryLab = dataCategoriesAndSubCatMap.get(dataCategory.name);
                    //subCategoryLab.add(childCategory.label);
                    dataCategoriesAndSubCatMap.get(dataCategory.name).add(childCategory.label);
                }else{
                    dataCategoriesAndSubCatMap.put(dataCategory.name,new List<String> {childCategory.label});
                }
            }
        }
        system.debug('>>>>>managerIdWithInqCountmap>>>'+dataCategoriesAndSubCatMap);

        //Map<Decimal,List<String>> maptitleAndId= new Map<Decimal,List<String>>();
         Set<id> knowledgeid = new Set<id>();
        List<AP_Knowledge__kav> knowledgeDetails = new List<AP_Knowledge__kav>();
         if( String.isNotBlank(parentCategory)){
            for(String childCategory :  dataCategoriesAndSubCatMap.get(parentCategory)){
                 system.debug('>>parentCategory>>>'+parentCategory);
                system.debug('>>childCategory>>>'+childCategory);
                String filters = createFilter();
                filters += ' WITH DATA CATEGORY '
                    + String.escapeSingleQuotes(parentCategory)
                    + '__c ABOVE '
                    + String.escapeSingleQuotes(String.valueOf(childCategory))+'__c ';
                    system.debug('>>articleFieldSets>>>'+articleFieldSets);
                String queryForKnowledge = queryBuilder(
                                        'AP_Knowledge__kav',
                                        articleFieldSets,
                                        filters,
                                        null,
                                        null,
                                        null
                                    );
                 system.debug('>>>>queryForKnowledge>>>'+queryForKnowledge);
                knowledgeDetails = fetchRecords(queryForKnowledge);
                system.debug('>>>>knowledgeDetails>>>'+knowledgeDetails);

                for(AP_Knowledge__kav knowledge : knowledgeDetails) {
                        system.debug('>>>>knowledge>>>'+knowledge.Title);
                        system.debug('>>>>knowledge>>>'+knowledge.Description__c);
                        if(subCatAndKnowArtMap.containsKey(childCategory)){
                            knowledgeid.add(knowledge.KnowledgeArticleId);
                            system.debug('>>>>>knowledge.id>>>'+knowledge.id);
                            //system.debug('>>>>>knowledgeStat>>>'+knowledgeStat);
                            //subCategoryLab = dataCategoriesAndSubCatMap.get(dataCategory.name);
                            //subCategoryLab.add(childCategory.label);
                            subCatAndKnowArtMap.get(childCategory).add(new myObject(knowledge.KnowledgeArticleId,knowledge.Title, knowledge.Description__c,Date.valueOf(knowledge.CreatedDate),knowledge.createdBy.Name,knowledge.createdBy.FirstName,knowledge.createdBy.LastName,knowledge.createdBy.FullPhotoUrl)) ;
                        }else{
                            subCatAndKnowArtMap.put(childCategory,new List<myObject>{new myObject(knowledge.KnowledgeArticleId,knowledge.Title, knowledge.Description__c,Date.valueOf(knowledge.CreatedDate),knowledge.createdBy.Name,knowledge.createdBy.FirstName,knowledge.createdBy.LastName,knowledge.createdBy.FullPhotoUrl)});
                        }
                        system.debug('>>>>>subCatAndKnowArtMap==='+subCatAndKnowArtMap);

                    }
                }
                system.debug('>>>>>knowledgeid.id>>>'+knowledgeid);
                List<KnowledgeArticleViewStat> knowledgeStat = [SELECT
                                                                    Id,NormalizedScore,Parent.Id,ViewCount
                                                                FROM KnowledgeArticleViewStat
                                                                WHERE  Parent.Id IN :knowledgeid AND Channel = 'App' ORDER BY
                                                                NormalizedScore Desc Limit 4];
                system.debug('>>>>>knowledgeStat>>>'+knowledgeStat);
            }else {
                ApexPages.addmessage(
                    new ApexPages.Message(ApexPages.Severity.Warning, 'Error')
                );
            }
            system.debug('>>>>>subCatAndKnowArtMap>>>'+subCatAndKnowArtMap);
    }


    public static String createFilter() {
         String filters =  ' PublishStatus = \''+ label.Online + '\' ';
         //String filters =  ' PublishStatus = Online';
         //filters += ' AND Language = \'' + Constant.LANG_CODE + '\' ';
         return filters;
     }
     public  String queryBuilder(
        String objectName,
        Set<String> fieldName,
        String filters,
        String sortExpression,
        Integer recordLimit,
        Integer recordOffset
    ) {
        system.debug('>>fieldName>>>'+fieldName);
        String fieldsNameFromSet = SetToString(fieldName);
        system.debug('>>fieldsNameFromSet>>>'+fieldsNameFromSet);
        fieldsNameFromSet = fieldsNameFromSet + ',CreatedDate,createdBy.Name,createdBy.firstname,createdBy.lastName,createdBy.FullPhotoUrl';
        return ('SELECT ' +
                fieldsNameFromSet +
                ' FROM ' +  objectName +
            (String.isNotBlank(filters) ?
                (' WHERE ' + filters  ) :''
            ) +
            (String.isNotBlank(sortExpression) ?
                (' ORDER BY ' +  sortExpression) :''
            ) +
            (recordLimit != null ?
                (' LIMIT ' + recordLimit) :''
            ) +
            (recordOffset != null ?
                (' OFFSET ' + recordOffset)
                 :''
                )
        );
    }

    public static List<sObject> fetchRecords(String query) {
        return Database.query(query);
    }

    public static String setToString(Set<String> fieldStr) {
        String setforQuery = '' ;
        //Converted Set to List as Set can't be used as iterable object
        setforQuery = String.join(new List<String>(fieldStr),',');
        setforQuery = setforQuery.substringBeforeLast(',');
        return setforQuery;
    }

     public class myObject {
         public String knowledgeId { get; set; }
        public String title { get; set; }
        public String description { get; set; }
        public Date createdDate { get; set; }
        public String createdName { get; set; }
        public String firstName { get; set; }
        public String lastName { get; set; }
        public string photoURL {get;set;}
        public myObject(String knowledgeId,String title, String description,Date createdDate,String createdName,String firstName,String lastName, string photoURL) {
            this.knowledgeId = knowledgeId;
            this.title = title;
            this.description = description;
            this.createdDate = createdDate;
            this.createdName = createdName;
            this.firstName = firstName;
            this.lastName = lastName;
            this.photoURL = photoURL;
        }
    }
}