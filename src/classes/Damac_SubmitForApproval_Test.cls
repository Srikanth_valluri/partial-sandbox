@isTest
public class Damac_SubmitForApproval_Test{
    testMethod static void submitForApproval(){
        Inquiry__c newInquiry = new Inquiry__c();
        newInquiry.First_Name__c = 'Test';
        newInquiry.Last_Name__c = 'Inquiry';
        newInquiry.Mobile_Phone_Encrypt__c = '05789088';
        newInquiry.Mobile_Phone__c = '05789088';
        newInquiry.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        newInquiry.Email__c = 'craig.lobo@eternussolutions.com';
        newInquiry.Preferred_Language__c = 'English';
        newInquiry.Inquiry_Source__c = 'Social' ;
        newInquiry.Inquiry_Status__c = 'Qualified' ;
        newInquiry.Inquiry_Status__c = null ;
        insert newInquiry;

        Sales_Tours__c ts =new Sales_Tours__c();
        ts.Inquiry__c = newInquiry.Id;
        ts.Tour_Outcome__c = 'Show - Options given and considering';
        ts.Pickup_Location__c = 'Park Towers';
        ts.Check_In_Date__c = System.now();
        ts.Check_Out_Date__c = System.now();
        ts.Comments__c = 'Test Tour';
        ts.Status__c = 'Submitted';
        ts.Is_Approved__c = false;
        
        insert ts;
        Test.startTest();
            ts.Qualified_Status__c = 'Disqualification In Progress';
            ts.Approval_Comment_Check__c = 'Requested';
            ts.HOS_user__c = UserInfo.getUserId();
            update ts;
            
            ts.Disqualify_Status__c = 'HT Rejected' ;
            update ts;
        Test.stopTest();
    }
}