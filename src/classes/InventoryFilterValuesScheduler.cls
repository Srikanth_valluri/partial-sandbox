/**************************************************************************************************
* Name               : InventoryFilterValuesBatch
* Test Class         : 
* Description        : Scheduler class for InventoryFilterValuesBatch class.
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst        27/11/2020      Initial Draft.
**************************************************************************************************/
public with sharing class InventoryFilterValuesScheduler implements Schedulable{

    public void execute(SchedulableContext SC) {
        InventoryFilterValuesBatch batchObject = new InventoryFilterValuesBatch();
        Database.executeBatch(batchObject, 200); 
    }
}