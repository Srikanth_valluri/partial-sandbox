@isTest
private class UpdateFMInvDueDailyBatchTest {
    
    static testMethod void test_UpdateFMInvDueDailyBatch1 () {
        
        
        TriggerOnOffCustomSetting__c objSetting = new TriggerOnOffCustomSetting__c();
        objSetting.Name = 'CallingListTrigger';
        objSetting.OnOffCheck__c = false ;
        insert objSetting ;  
        
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        insert objAcc;
        System.assert(objAcc != null);
    
        
        Id FMCollectionRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('FM Collections').RecordTypeId;
        Calling_List__c callObj = new Calling_List__c( Account__c = objAcc.Id,Registration_ID__c = '12851',Invoice_Original_Due__c=10
                                                     , RecordTypeId = FMCollectionRecordTypeId ,Calling_List_Type__c = 'FM Calling List');
        insert callObj;
        
       insert new IpmsRestServices__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            BaseUrl__c = 'http://0.0.0.0:8080/webservices/rest',
            Username__c = 'username',
            Password__c = 'password',
            Timeout__c = 120000
        );

        FmIpmsRestServices.DueInvoicesResponse invoicesResponse = new FmIpmsRestServices.DueInvoicesResponse();
        invoicesResponse.OutputParameters = new FmIpmsRestServices.OutputParameters();
        invoicesResponse.OutputParameters.X_RETURN_STATUS = 'S';
        invoicesResponse.OutputParameters.X_RETURN_MESSAGE = 'Total Records Fetched...1';
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE = new FmIpmsRestServices.ResponseMessage();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM = new List<Map<String, String>>();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM.add(
            new Map<String, String> {
                'ATTRIBUTE1' => 'registrationId',
                'ATTRIBUTE2' => 'unitName',
                'ATTRIBUTE3' => 'projectName',
                'ATTRIBUTE4' => 'customerId',
                'ATTRIBUTE5' => 'orgId',
                'ATTRIBUTE6' => 'partyId',
                'ATTRIBUTE7' => 'partyName',
                'ATTRIBUTE8' => 'trxNumber',
                'ATTRIBUTE9' => 'creationDate',
                'ATTRIBUTE10' => 'callType',
                'ATTRIBUTE11' => '50',
                'ATTRIBUTE12' => '100',
                'ATTRIBUTE13' => '50',
                'ATTRIBUTE14' => FmcUtils.formatAsIpmsDate(Date.today()),
                'ATTRIBUTE15' => 'trxType',
                'ATTRIBUTE16' => '0'
            }
        );
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(invoicesResponse)));     
        
        
        UpdateFMInvDueDailyBatch batchCls = new UpdateFMInvDueDailyBatch();     
        
        Test.startTest();            
            Database.executeBatch(batchCls,1);
        Test.stopTest();
        
    }
}