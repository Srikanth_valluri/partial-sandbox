@isTest(SeeAlldata=false)
public class ReshufflePreinquiriesTest {

    public static testmethod void testMethod1() {
        /*
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 

        User u1 = new User(
            Alias = 'manager1', 
            Email = 'manager1@testorg.com', 
            EmailEncodingKey = 'UTF-8', 
            LastName ='Tesdsting', 
            LanguageLocaleKey ='en_US', 
            LocaleSidKey = 'en_US', 
            ProfileId = p.Id, 
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = 'manager1@testorg.com'
        );
        insert u1;

        User u2 = new User(
            Alias = 'manager2', 
            Email = 'manager2@testorg.com', 
            EmailEncodingKey='UTF-8', 
            LastName = 'Tesdssting', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey='en_US', 
            ProfileId = p.Id, 
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName='manager2@testorg.com'
        );
        insert u2;

        User u3 = new User(
            Alias = 'standt', 
            Email = 'managerUser1@testorg.com', 
            EmailEncodingKey='UTF-8', 
            LastName = 'Tesdssting', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey='en_US', 
            ProfileId = p.Id, 
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName='managerUser1@testorg.com',
            ManagerID = u1.id
        );
        insert u3;

        User u4 = new User(
            Alias = 'standt', 
            Email = 'managerUser2@testorg.com', 
            EmailEncodingKey='UTF-8', 
            LastName = 'Tesdssting', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey='en_US', 
            ProfileId = p.Id, 
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName='managerUser2@testorg.com',
            ManagerID = u2.id
        );
        insert u4;

        TS_Assignment_Settings__c csInstance1 = new TS_Assignment_Settings__c(
            Name = 'Test1',
            Owner_Id__c = u3.id,
            Round_Robin_Id__c = 2,
            Telesales_Executive__c = 'abc',
            Type__c = 'Stands'
        );
        insert csInstance1;

        TS_Assignment_Settings__c csInstance2 = new TS_Assignment_Settings__c(
            Name = 'Test2',
            Owner_Id__c = u4.id,
            Round_Robin_Id__c = 2,
            Telesales_Executive__c = 'abc',
            Type__c = 'Stands'
        );
        insert csInstance2;

        List<TSA_Preinquiry_Reshuffle__mdt> preInquiryConditionMDT = [SELECT Id, DeveloperName, Based_In__c,
            Days__c, Queue__c, ReassignmentPass__c, Status__c FROM TSA_Preinquiry_Reshuffle__mdt];
        Id preInq = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
        List<Inquiry__c> inqList = new List<Inquiry__c>();

        System.runAs(u3) {
            
            Inquiry__c inqObj = new Inquiry__c(
                RecordTypeId = preInq,
                Mobile_CountryCode__c = 'American Samoa: 001684',
                Mobile_Phone__c = '1234',
                Mobile_Phone_Encrypt__c = '123456789',
                Email__c = 'mk@gmail.com',
                First_Name__c = 'Test',
                Last_Name__c = 'Last',
                Inquiry_Status__c = 'Unreachable - Not responding',
                Last_Activity_Date__c = System.now()
            );
            insert inqObj;
            inqList.add(inqObj);
        }

        System.runAs(u4) {
            Inquiry__c inqObjNew = new Inquiry__c(
                RecordTypeId = preInq,
                Mobile_CountryCode__c = 'American Samoa: 001684',
                Mobile_Phone__c = '1234',
                Mobile_Phone_Encrypt__c = '123456789123',
                Email__c = 'mk@gmail123.com',
                First_Name__c = 'Test'
                ,Last_Name__c ='Last',
                Inquiry_Status__c = 'Unreachable - Not responding',
                Last_Activity_Date__c = System.now().Date().addDays(-10)
            );
            insert inqObjNew;
            Test.setCreatedDate(inqObjNew.Id, System.now().Date().addDays(-10));
            inqObjNew.Last_Activity_Date__c = System.now().Date().addDays(-10);
            update inqObjNew; 
            inqList.add(inqObjNew);
        }

        System.Test.startTest();
        
        preInqInstance.reAssignPreInquiryOwner(inqList, 5, 'Nurturing_Queue');
        System.Test.stopTest(); 
        */
        ReshufflePreinquiries preInqInstance = new ReshufflePreinquiries();
        preInqInstance.reAssignPreInquiryOwner (null, 0 ,'');
    }
}