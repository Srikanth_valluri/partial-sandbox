/****************************************************************************************************
* Name               : Agent_LOI_Controller                                                         *
* Description        : This class belongs to the LOI Documents                                      *
* Created Date       : 05/09/2018                                                                   *
* Created By         : ESPL                                                                         *
* --------------------------------------------------------------------------------------------------*
* VERSION   AUTHOR            DATE          COMMENTS                                                *
* 1.0       CRAIG             05/09/2018    Initial Draft.                                          *
****************************************************************************************************/
global without sharing class Agent_LOI_Controller {

    public NSIBPM__Service_Request__c srObj                                             {get; set;}
    public Date todaysDate                                                              {get; set;}

    public Agent_LOI_Controller() {
        todaysDate = System.Today();
        srObj = new NSIBPM__Service_Request__c();
        String pJSONString = Apexpages.currentPage().getParameters().get('srJSON');
        NSIBPM__Service_Request__c deserializedSR = 
            (NSIBPM__Service_Request__c)JSON.deserialize(pJSONString, NSIBPM__Service_Request__c.class);
        system.debug('---deserializedSR>>>>>>  ' + deserializedSR); 
        srObj = deserializedSR;
        /*srObj = [SELECT Id, Agency_Corporate_Type__c,
                Trade_License_Number__c, ORN_Number__c,
                Country_of_Sale__c, Zip_Postal_Code__c,
                CreatedDate,First_Name__c,Last_Name__c,
                Agency_Type__c,Country__c,City__c,Street__c,
                Street2__c,Agency_Name__c,P_O_Box_No__c,
                 P_O_Box_Zip_Postal_Code__c, Emirate_State__c 
                FROM NSIBPM__Service_Request__c 
                WHERE Id = 'a0M9E000002edMR']; */
    }

}