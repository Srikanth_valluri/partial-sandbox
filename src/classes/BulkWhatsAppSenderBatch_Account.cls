/*******************************************************************************************************
* Description : To Send Bulk whatsapp from Account
*=======================================================================================================
* Ver      Date-DD/MM/YYYY     Author              Modification
*=======================================================================================================
* 1.0      26/10/2020          Arjun Khatri        Initial Draft
* Database.executeBatch(new BulkWhatsAppSenderBatch_Account(), 1);
*********************************************************************************************************/
global class BulkWhatsAppSenderBatch_Account implements Database.Batchable<sObject>
                                                        , Database.Stateful
                                                        , Database.AllowsCallouts {
        
        //List of CRM_Nexmo_Whats_App_Request                                                    
        List<CRM_Nexmo_Whats_App_Request__c> lstCRM_Nexmo_Whats_App_Request;
        List<CRM_Nexmo_Whats_App_Message__c> lstCRM_Nexmo_Whats_App_Message;
        Map<Id,CRM_Nexmo_Whats_App_Message__c> mapAccIdCRM_Nexmo_Whats_App_Message = new Map<Id,CRM_Nexmo_Whats_App_Message__c>();
        
        global Database.QueryLocator start(Database.BatchableContext BC){
            System.debug('-->> Inside start');

            String strQuery = 'SELECT Id,Mobile_Person_Business__c, First_Name__c,'
                            + ' Last_Name__c, Nationality__pc ,Mobile_Phone_Encrypt__pc, '
                            + ' Mobile__c, Name, Nationality__c, isPersonAccount '
                            + ' FROM Account'
                            + ' WHERE Send_Bulk_Email__c = true AND Mobile_Person_Business__c != null ';
            if( String.isNotBlank ( Label.Record_Id_To_Test ) 
            && Label.Record_Id_To_Test.toLowerCase().contains('on') 
            && String.isNotBlank( Label.Record_Id_To_Test.substringAfter('-') )
            && Label.Record_Id_To_Test.substringAfter('-').startsWith( '001') 
            && !Test.isRunningTest() ) {
                strQuery = strQuery + ' AND Id = \'' + Label.Record_Id_To_Test.substringAfter('-') + '\'  ';
            }
            System.debug('-->> strQuery == ' + strQuery);
            return Database.getQueryLocator(strQuery);
        }

        global void execute( Database.BatchableContext BC, List<Account > listAccount ) {
            System.debug('-->> listAccount : ' + listAccount);
            System.debug('-->> listAccount.size(): ' + listAccount.size());
            CRM_WA_Credential__mdt  credentials = new CRM_WA_Credential__mdt ();
            map<string,string> templatesMap = new map<string,string>();
            CRMNexmoWhatsAppController objCRMNexmoWhatsAppController = new CRMNexmoWhatsAppController();
        
            
            if( listAccount != null && listAccount.size() > 0 ) {
                lstCRM_Nexmo_Whats_App_Request = new List<CRM_Nexmo_Whats_App_Request__c>();
                lstCRM_Nexmo_Whats_App_Message = new List<CRM_Nexmo_Whats_App_Message__c>(); 
                
                String templateName = Label.BulkWhatsAppTemplateName.substringBefore('-');//'survey';
                
                if( String.isNotBlank(templateName) ) {
                    templatesMap = objCRMNexmoWhatsAppController.getTemplate(templateName);
                }
                
                credentials = [SELECT Id
                                , API_Key__c
                                , API_Secret__c
                                , WA_Account__c
                                , Endpoint_URL__c
                             FROM CRM_WA_Credential__mdt 
                            WHERE WA_Account__c =: Label.BulkWhatsAppTemplateName.substringAfter('-') LIMIT 1];
                
                for( Account objAccount: listAccount ) {
                    //Call Method to send whatsapp through nexmo
                    WhatsAppResponseWrapper objWrap = sendMessageWithMTM( credentials
                                                          , templateName
                                                          , objAccount
                                                          , templatesMap);
                    if( objWrap.objCRM_Nexmo_Whats_App_Request != null ) {
                        lstCRM_Nexmo_Whats_App_Request.add( objWrap.objCRM_Nexmo_Whats_App_Request );
                    }
                    if( objWrap.objCRM_Nexmo_Whats_App_Message != null ) {
                        mapAccIdCRM_Nexmo_Whats_App_Message.put( objAccount.Id , objWrap.objCRM_Nexmo_Whats_App_Message );
                    }
                }   //END of for
                System.debug('lstCRM_Nexmo_Whats_App_Request===: ' + lstCRM_Nexmo_Whats_App_Request);
                if(lstCRM_Nexmo_Whats_App_Request.size() > 0) {
                    if(!Test.isRunningTest()) {
                        upsert lstCRM_Nexmo_Whats_App_Request Unique_Key__c ;
                        if( mapAccIdCRM_Nexmo_Whats_App_Message != null ) {
                            for( CRM_Nexmo_Whats_App_Request__c objCRM_Nexmo_Whats_App_Request: lstCRM_Nexmo_Whats_App_Request ) {
                                if( mapAccIdCRM_Nexmo_Whats_App_Message.containsKey( objCRM_Nexmo_Whats_App_Request.Account__c ) ) {
                                    CRM_Nexmo_Whats_App_Message__c message = mapAccIdCRM_Nexmo_Whats_App_Message.get( objCRM_Nexmo_Whats_App_Request.Account__c );
                                    message.CRM_Nexmo_Whats_App_Request__c = objCRM_Nexmo_Whats_App_Request.id;
                                    lstCRM_Nexmo_Whats_App_Message.add(message);
                                }
                            }
                            if( lstCRM_Nexmo_Whats_App_Message.size() > 0 ) {
                                insert lstCRM_Nexmo_Whats_App_Message;
                            }
                        }
                    }
                }
            }
        }

    /*********************************************************************************
    * Method Name : sendMessageWithMTM
    * Description : Send New Message to Nexmo Server.
    * Return Type : void
    * Parameter(s): 
    **********************************************************************************/
    public WhatsAppResponseWrapper sendMessageWithMTM( CRM_WA_Credential__mdt  credentials
                                  , String selectedTemplate 
                                  , Account objAccount
                                  , map<string,string> templatesMap ){
        
        CRMNexmoWhatsAppController objCRMNexmoWhatsAppController = new CRMNexmoWhatsAppController();
        WhatsAppResponseWrapper objWrap = new WhatsAppResponseWrapper();
        if( String.isNotBlank ( selectedTemplate ) ) {
            system.debug( ' <<< selectedTemplate <<< ' + selectedTemplate);
            
            //string JWTToken = PrepareJWTToken();        
            if( credentials != null 
             && credentials.API_Secret__c != null
             && credentials.API_Key__c != null ) {
                system.debug(credentials.API_Secret__c);
                system.debug(credentials.API_Key__c);
                string forBase64 = credentials.API_Key__c+':'+credentials.API_Secret__c;
                blob blobforBase64 = blob.valueof(forBase64);
                string header = EncodingUtil.base64Encode(blobforBase64);   
                
                string bodyStr = bodyTosend(true,selectedTemplate,credentials,objAccount,templatesMap);
                system.debug('bodyStr>>> : '+bodyStr);
                HttpRequest req = new HttpRequest();            
                req.setEndpoint(credentials.Endpoint_URL__c);
                req.setHeader('Authorization','Basic '+header);
                req.setBody(bodyStr);
                req.setHeader('Content-Type','application/json');
                req.setMethod('POST');        
                Http http = new Http();
                HTTPResponse res;
                if( !test.isRunningTest() ) {
                    res = http.send(req);
                    System.debug('req :: >> :: ' + req);
                    System.debug('res.getBody() :: >> :: ' + res.getBody());
                    
                    if( res.getstatuscode() == 200 ||  res.getstatuscode() == 202 ){
                        CRMNexmoWhatsAppController.NEXMO_MESSGAE_RESPONSE resp = objCRMNexmoWhatsAppController.parse(string.valueof(res.getBody()));
                        system.debug('message_uuid>>>>'+resp.message_uuid);
                        objWrap = createRequest(resp.message_uuid,templatesMap.get('preview'),bodyStr,true,objAccount);
                    }
                }else{
                    CRMNexmoWhatsAppController.NEXMO_MESSGAE_RESPONSE resp = objCRMNexmoWhatsAppController.parse(string.valueof('{"message_uuid":"54af4397-0ea7-48c5-bc9a-d7551bfb2bfb"}'));                
                }
                //waMessage = null;
            }
        }  
        return objWrap;
    }

    /*********************************************************************************
    * Method Name : bodyTosend
    * Description : Create body to send bulk whatsapp to cx
    * Return Type : String
    * Parameter(s): 
    **********************************************************************************/
    public string bodyTosend( boolean isWithMTM
                            , string templateName 
                            , CRM_WA_Credential__mdt  credentials
                            , Account objAccount
                            , map<string,string> templatesMap ) {
        CRMNexmoWhatsAppController objCRMNexmoWhatsAppController = new CRMNexmoWhatsAppController();
        String jsonString;
    
        system.debug('templatesMap='+templatesMap);
        
        Decimal toNum = decimal.valueof(objAccount.Mobile_Person_Business__c.removestart('00')) ;  

        if( isWithMTM ) {
            jsonString =  objCRMNexmoWhatsAppController.buildRequestBody_template( string.valueof(toNum),  credentials.WA_Account__c
                                                   , templatesMap.get('namespace')+':'+templateName 
                                                   , objAccount.Name );
            
        }else{ // NO MTM
        }
        return jsonString;
    }
    
     public WhatsAppResponseWrapper createRequest( string messageuuid
                              , string messagetext
                              , string reqBody
                              , boolean iswithMTM 
                              , Account objAccount ){
        WhatsAppResponseWrapper objWrap = new WhatsAppResponseWrapper();
        // Insert Parent
        CRM_Nexmo_Whats_App_Request__c request = new CRM_Nexmo_Whats_App_Request__c();
        request.Account__c = objAccount.Id;
        request.Record_Name__c = objAccount.Name;
        if(objAccount.Mobile_Person_Business__c != null && String.isNotBlank(objAccount.Mobile_Person_Business__c)){
        request.To_number__c = decimal.valueof(objAccount.Mobile_Person_Business__c.removeStart('00'));
        request.Unique_Key__c =  objAccount.Mobile_Person_Business__c.removestart('00')+userinfo.getuserid();
        }
        request.ownerid = userinfo.getuserid();
        request.Last_Message_Sent__c = system.now();
        //request.Last_Message__c = wamessage;
        if(iswithMTM){
            request.Last_MTM_Sent_at__c = system.now();            
        }
        //upsert request Unique_Key__c;
        objWrap.objCRM_Nexmo_Whats_App_Request = new CRM_Nexmo_Whats_App_Request__c();
        objWrap.objCRM_Nexmo_Whats_App_Request = request;
        // Insert Child
        CRM_Nexmo_Whats_App_Message__c message = new CRM_Nexmo_Whats_App_Message__c();
        //message.CRM_Nexmo_Whats_App_Request__c = request.id;
        message.direction__c = 'Outbound';
        message.Nexmo_Message_uuid__c = messageuuid;
        message.Nexmo_Message_Body__c = messagetext;
        
        message.Nexmo_Message_Request_Body__c = reqBody;
        
        system.debug('message:::'+message);
        //insert message;
        objWrap.objCRM_Nexmo_Whats_App_Message = new CRM_Nexmo_Whats_App_Message__c();
        objWrap.objCRM_Nexmo_Whats_App_Message = message;
        
        
        return objWrap;
    }
    
    
    global void finish(Database.BatchableContext BC) {
        System.debug('-->> Finish ' );
    }
    
    global class WhatsAppResponseWrapper {
        global CRM_Nexmo_Whats_App_Request__c objCRM_Nexmo_Whats_App_Request;
        global CRM_Nexmo_Whats_App_Message__c objCRM_Nexmo_Whats_App_Message;
        global WhatsAppResponseWrapper() {
            this.objCRM_Nexmo_Whats_App_Request = new CRM_Nexmo_Whats_App_Request__c();
            this.objCRM_Nexmo_Whats_App_Message = new CRM_Nexmo_Whats_App_Message__c();
        }
    }
}