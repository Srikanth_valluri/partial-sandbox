/*
SPA using REST Approach invoked from the SPA Step
Developed By DAMAC Team
*/
global without sharing class CC_InvokeSPA_REST implements NSIBPM.CustomCodeExecutable {

    public String EvaluateCustomCode(NSIBPM__Service_Request__c SR, NSIBPM__Step__c step) {
        String retStr = 'Success';
        List<Id> BookingIds= new List<Id>();
        try{
            for(Booking_Unit__c BU :[select id,booking__c from Booking_Unit__c where Status__c!='Removed' and Booking__r.Deal_SR__c=: step.NSIBPM__SR__c]){                
                bookingIds.add(BU.booking__c);                
            } 
            for(id i:bookingIds){     
                //IPMS_REST_SPA_getProcessID cls = new IPMS_REST_SPA_getProcessID();
                IPMS_REST_SPA_getProcessID.DataForRequest(i);
            }
        }
        catch (Exception ex) {
            retStr = '#### Exception at line number = '+ex.getLineNumber()+' , Exception Message = '+ex.getMessage();
        }
        return retStr;
    }
}// End of class.