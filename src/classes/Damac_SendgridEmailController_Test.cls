@istest
public class Damac_SendgridEmailController_Test {

    static testmethod void callMethods(){
        Sendgrid_Credentials__c credentials = new Sendgrid_Credentials__c ();
        credentials.Endpoint_URL__c = 'test.com';
        credentials.API_Key__c = 'testcom';
        credentials.From_name__c = 'test';
        credentials.From_Email__c = 'test@test.com';
        insert credentials;
        
        Id accountRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        
        Account a = new Account();
        a.LastName= 'Test Account';
        a.RecordTypeId = accountRTId;
        a.Agency_Short_Name__c = 'testShrName';
        a.Party_ID__c = '789456';
        a.Email__c = 'test@gmail.com';
        a.Mobile__c = '7894561230';
        a.Title__c = 'Ms.';
        a.Nationality__c = 'Australian';
        a.Passport_Number__c = '123654';
        insert a;
        
        Share_Project_Details__c obj = new Share_Project_Details__c();
        obj.Account__c = a.id;
        obj.Sent_To_Email__c ='test@test.com';
        insert obj;
        
        Email_Request__c req = new Email_Request__c ();
        req.Template_for__c = 'Sales Offer Inquiry';
        req.Email_template__c = 'b70c2c19-b7d8-4eb4-acd1-e3f0883f438b';
        req.Substitution_tags__c = '-Name-';
        req.Related_Object_API__c = 'Inquiry__c';
        insert req;
        
        List <Email_tags__c> tags = new List <EMail_Tags__c> ();
        Email_tags__c tag = new EMail_Tags__c ();
        tag.Email_Request__c = req.ID;
        tag.Send_grid_Template_Tag__c = 'ID';
        tag.SF_Field_Api__c = 'First_Name__c';
        tags.add (tag);
        
        Inquiry__c newInquiry = new Inquiry__c();
        newInquiry.First_Name__c = 'Testsearch';
        newInquiry.Last_Name__c = 'Inquiry';
        newInquiry.Mobile_Phone_Encrypt__c = '05789088';
        newInquiry.Mobile_Phone__c = '05789088';
        newInquiry.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        newInquiry.Email__c = 'test.lastname@eternussolutions.com';
        newInquiry.Preferred_Language__c = 'English';
        newInquiry.Inquiry_Source__c = 'Social' ;
        newInquiry.Inquiry_Status__c = 'New' ;
        insert newInquiry;
        
        Email_tags__c tag1 = new EMail_Tags__c ();
        tag1.Email_Request__c = req.ID;
        tag1.Send_grid_Template_Tag__c = 'Email_Metrics_ID';
        tag1.SF_Field_Api__c = 'Email_Metrics_ID';
        tags.add (tag1);        
        insert tags;
        
        SalesOffer_Record__c rec = new SalesOffer_Record__c ();
        insert rec;
        
        Email_metrics__c metrics = new Email_metrics__c ();
        metrics.Email_request__c = req.id;
        metrics.Email_Send__c = false;
        metrics.Share_Project_Details__c = obj.id;
        metrics.SObject_ID__c = newInquiry.ID;
        metrics.Email__c = 'Srikanth.v@bigworks.co';
        metrics.SalesOffer_Record__c = rec.id;
        insert metrics;
        
        metrics.Email_Send__c = true;
        Update metrics;
        
        
        
    }
}