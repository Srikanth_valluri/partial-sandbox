@isTest
private class NoAttachmentsInPopEmailBatchOnCaseTest {

    @testSetup
    static void dataForTest() {
        insert new IpmsRestServices__c(
                   SetupOwnerId = UserInfo.getOrganizationId(),
                   BaseUrl__c = 'http://83.111.194.181:8045/webservices/rest',
                   Username__c = 'oracle_user',
                   Password__c = 'crp1user',
                   Client_Id__c = '8MABLQM-KJ8I8-1XA58-WWCM1S1',
                   BearerToken__c = 'eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWyiwuVrJSKs5IBKK8bCUdpczEEiUrQ1MzAwtTMwsDIx2l1IoCqIC5oRFIoLQ4tSgvMTcVqM_C19HJJ9BX19vLwtNC1zDC0dRCNzzc2dcw2FCpFgBXRb-1XQAAAA.6Ym224Vwr9AniBeq6gL8OM9u4vGnUB_vbEUVjWojg14',
                   Timeout__c = 120000
                   );

       Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
       Account account = new Account( LastName = 'Test Account',
                                      Party_ID__c = '63062',
                                      RecordtypeId = rtId,
                                      Email__pc = 'test@mailinator.com');
        insert account;

        for(Integer i=0; i<5; i++) {
            Case cs = new Case(AccountId = account.Id, No_Attachments_in_POP_Email__c = true);
            insert cs;

            Task task = new Task(WhatId = cs.Id, Cash_Receipt_Id__c = '123'+i);
            insert task;
        }
    }

    @isTest
    static void testBatchMethod() {

        FmHttpCalloutMock.Response urlResponse = new FmHttpCalloutMock.Response(200, 'Success', '   {  '  +
         '       "responseId": "3072019-10305298",  '  +
         '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         '       "status": "S",  '  +
         '       "responseMessage": "Process Completed",  '  +
         '       "elapsedTimeMs": 8497,  '  +
         '       "responseLines": [  '  +
         '           {  '  +
         '               "documentName": "DPSOA",  '  +
         '               "language": "EN",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
         '           },  '  +
         '           {  '  +
         '               "documentName": "DPSOA_AR",  '  +
         '               "language": "AR",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
         '           }  '  +
         '       ],  '  +
         '       "complete": true  '  +
         '  }  ');

        String BULK_SOA_IN_LANG_str = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.BULK_SOA_LANG, new List<String> {'63062'});

        String Receipt_URL_str1 = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.DP_RECEIPT_ID,new List<String> {'1230'});
        String Receipt_URL_str2 = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.DP_RECEIPT_ID, new List<String> {'1231'});
        String Receipt_URL_str3 = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.DP_RECEIPT_ID, new List<String> {'1232'});
        String Receipt_URL_str4 = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.DP_RECEIPT_ID, new List<String> {'1233'});
        String Receipt_URL_str5 = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.DP_RECEIPT_ID, new List<String> {'1234'});

        String blobStr = 'Test blob String';
        FmHttpCalloutMock.Response blobResponse = new FmHttpCalloutMock.Response(200, 'Success', blobStr);
        FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
            BULK_SOA_IN_LANG_str => urlResponse,
            Receipt_URL_str1 => urlResponse,
            Receipt_URL_str2 => urlResponse,
            Receipt_URL_str3 => urlResponse,
            Receipt_URL_str4 => urlResponse,
            Receipt_URL_str5 => urlResponse,
            'https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd' =>
            blobResponse,
            'https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a' =>
            blobResponse,
            'https://api.sendgrid.com/v3/mail/send' => sendGridResponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));

        Test.startTest();
            NoAttachmentsInPopEmailBatchOnCase objBatch = new NoAttachmentsInPopEmailBatchOnCase();
            Database.executeBatch(objBatch);
        Test.stopTest();

    }

}