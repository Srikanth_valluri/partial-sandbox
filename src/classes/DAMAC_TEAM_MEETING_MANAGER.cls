/*
*   Class NAME          : DAMAC_TEAM_MEETING_MANAGER
*   Functionality       : To display the team related tasks and salestours
*/
public class DAMAC_TEAM_MEETING_MANAGER {
    public String meetingTypeFilter { get; set; }
    public String relatedToFilter { get; set; }
    public List<Activity_Manager__c> activityManager { get; set; }
    public String message { get; set; }
    public List<SelectOption> myTeam { get; set; }
    public List<String> myTeamIds { get; set; }
    public Task dateCriteria { get; set; }
    public Map<ID, Sobject> activityORSTDetails { get; set; }
    public Map<ID, String> recordTypeMap { get; set; }
    public Map<ID, String> namesMap { get; set; }
    public Task taskRecord{ get; set; }
    public Activity_Manager__c managerRec { get; set;}
    
    public DAMAC_TEAM_MEETING_MANAGER() {
        message = '';
        dateCriteria = new Task();
        dateCriteria.Start_Date__c = DateTime.Now();
        dateCriteria.End_Date__c = DateTime.Now();
        activityORSTDetails = new Map<ID, SObject>();
        meetingTypeFilter = '';
        relatedToFilter = '';
        myTeamIds = new List<String> ();
        myTeam = new List<SelectOption>();
        taskRecord = new Task();
        managerRec = new Activity_Manager__c();
        myTeam();
        init();
    }

    // To get the team based on logged in user ID
    public void myTeam (){
        activityManager = new List<Activity_Manager__c>();
        ID userId = UserInfo.getUserID();
        User u = new User();
        u = [SELECT Name, UserRole.Name, Profile.Name FROM User WHERE ID =: userId];
        
        for (User uTeam :[SELECT NAME From User Where 
                (ManagerId =: u.Id OR Manager.ManagerId =: u.ID OR Manager.Manager.ManagerId =:u.ID)
                AND ManagerId != NULL order By Name Asc]) {
            myTeam.add (new SelectOption (uTeam.Id, uTeam.Name, false)); 
            myTeamIds.add(uTeam.ID);
        }

    }

    // Method to get the tasks and sales tours 
    public void init() {
        message = '';
        namesMap = new Map<ID, String>();
        recordTypeMap = new Map<ID, String>();
        activityManager = new List<Activity_Manager__c>();
        Date startDateVal = dateCriteria.Start_Date__c.Date();
        Integer dt1 = startDateVal.daysBetween(System.today());
        if (dt1 <= 30) {
        
            if (meetingTypeFilter != 'ST')
                activities();
            if (meetingTypeFilter == 'ST' || meetingTypeFilter == '')
                salesTours();
        } else {
            message = 'Start Date should not be less than 30 days.';
        }
    }

    // Method to get the tasks based on start date and end date
    public void activities(){
        DateTime startDate = dateCriteria.Start_Date__c;
        DateTime endDate = dateCriteria.End_Date__c;
        
        String startDateStr = startDate+'';
        startDateStr = startDateStr.replace(' ', 'T')+'.000+0000';
        
        String endDateStr = endDate+'';
        endDateStr = endDateStr.replace(' ', 'T')+'.000+0000';
        
        Date startDateVal = startDate.Date();
        Date endDateVal = endDate.date();
        
        
        Set <ID> userIds = new Set<ID>();
        for (user u: [SELECT Name FROM USER WHERE (ID IN: myTeamIds 
                                                OR ManagerId IN: myTeamIds 
                                                OR Manager.ManagerId IN: myTeamIds 
                                                OR Manager.Manager.ManagerId IN :myTeamIds)]){
            userIds.add(u.ID);
        }
        System.Debug(':::: Start Time ::: '+startDate+' ::: End Time :::'+endDate);        
        System.Debug(':::::::: Map ::::::'+activityORSTDetails.keySet());
        List<Task> taskList = new List<Task>();
        String accRecord = 'Account';
        String inquiryRecord = 'Inquiry__c';
        String agencyRecordType = 'Agency';
        String statusVal = dateCriteria.Status;
        String query = 'SELECT WhatId, Activity_Type_3__c, Status, Activity_Type__c,'
                    +'Activity_Outcome__c, Activity_Sub_Type__c, ownerId, Owner.Name, '
                    +'CreatedDate, Start_Date__c, End_Date__c, Description, Subject, DOS_Comments__c, HOS_Comments__c, RM_Comments__c '
                    +'FROM Task '
                    +'WHERE ownerId IN:userIds '
                    +'AND ((Start_Date__c >='+startDateStr+'  AND End_Date__c <='+endDateStr+')  OR (((Start_Date__c = NULL OR End_Date__c = NULL) AND (ActivityDate >=: startDateVal  AND ActivityDate <=: endDateVal )))) ';
        if (statusVal != '' && statusVal != NULL && statusVal != '--None--')
            query += ' AND Status =: statusVal ';
        if (meetingTypeFilter == 'M' || meetingTypeFilter == '' || meetingTypeFilter == '--None--') {
            if (relatedToFilter == 'Inquiry') { 
                query += 'AND WhatId != NULL AND What.type =: inquiryRecord ';
            } else if (relatedToFilter == 'Customer') {
                query += 'AND WhatId != NULL AND What.type =: accRecord ';
            } else if (relatedToFilter == 'Agency') {
                query += 'AND WhatId != NULL AND What.type =: accRecord ';
            } else {
                query += 'AND ((WhatId != NULL AND  ( What.type =:accRecord OR What.type =: inquiryRecord) ) OR whatId = NULL )';
            }
            
            
        }
        
        if (meetingTypeFilter == 'IM') {
           query += 'AND WhatId = NULL ';
        }

        query += 'ORDER BY Owner.Name ASC NULLS LAST ';
        taskList = Database.query(query);
        Set<Id> accountIds = new Set<Id>();
        Set<ID> inqIds = new Set<ID>();
        
        if(taskList.size() > 0) {
            for (Task t : taskList) {
                Activity_Manager__c manager = new Activity_Manager__c();
                recordTypeMap.put (t.id, '');
                if (t.whatId != NULL) {
                    if (String.valueOf(t.whatId).startsWith ('001')) {
                        accountIds.add(t.WhatId);
                        manager.Related_to_Account__c = t.WhatId;
                        manager.Purpose_of_Meeting_Customer__c = t.Subject;
                    } 
                    else if (String.valueOf(t.whatId).startsWith ('a1A')) {
                        inqIds.add(t.whatId);
                        manager.Related_to_Inquiry__c = t.whatId;
                        manager.Purpose_of_Meeting_Inquiry__c = t.Subject;
                    }
                } else {
                    manager.Purpose_of_Meeting_Internal__c = t.Subject;
                }
                manager.Activity_or_Task_Id__c = t.id;
                activityORSTDetails.put (t.Id, t);
                activityManager.add(manager);
                namesMap.put(t.whatID, '');
            } 
            if (inqIds.size() > 0) {
                for (Inquiry__c inq : [SELECT Full_Name__c FROM Inquiry__c WHERE ID IN: inqIds]){
                    activityORSTDetails.put (inq.Id, inq);
                }
            }
            if(accountIds.size() > 0) {
                List<Activity_Manager__c> activityManagerToRemove = new List<Activity_Manager__c>();
                Map<Id,String> accountRecordTypeMap = new Map<Id,String>();
                for(Account acc :  [SELECT Id,RecordType.Name, Name
                                    FROM Account 
                                    WHERE Id IN: accountIds]) {
                    namesMap.put (acc.id, acc.Name);
                    accountRecordTypeMap.put(acc.Id, acc.RecordType.Name);
                }
                for(Activity_Manager__c managerRec : activityManager) {
                    if(managerRec.Related_to_Account__c != NULL) {
                        recordTypeMap.put(managerRec.Activity_or_Task_Id__c, accountRecordTypeMap.get(managerRec.Related_to_Account__c));
                    }
                    if(managerRec.Related_to_Account__c != NULL  && relatedToFilter == 'Agency') {
                        
                        if(accountRecordTypeMap.get(managerRec.Related_to_Account__c).contains(agencyRecordType)) {
                            
                            activityManagerToRemove.add(managerRec);  
                        }
                    }
                    
                    if(managerRec.Related_to_Account__c != NULL  && relatedToFilter == 'Customer') {
                        
                        if(!accountRecordTypeMap.get(managerRec.Related_to_Account__c).contains(agencyRecordType)) {
                            activityManagerToRemove.add(managerRec);  
                        }
                    }
                    
                    
                }
                if(relatedToFilter == 'Agency') {
                    activityManager = activityManagerToRemove;
                }
                
                if(relatedToFilter == 'Customer') {
                    activityManager = activityManagerToRemove;
                }
            } 
        }
        System.Debug(recordTypeMap);
    }
    
    
     // Method to get the comments given by the user
    public void getTaskComments(){
         managerRec.Purpose_of_Meeting_Internal__c = '';
        String taskID = Apexpages.currentpage().getparameters().get('taskID');
        taskRecord = new Task();
        if (taskID != NULL && taskID != '') {
            taskRecord = [SELECT WhatId, what.Type, Activity_Type_3__c, ActivityDate,  
                            Activity_Outcome__c, Status, Subject,
                            Activity_Type__c, Activity_Sub_Type__c,
                            CreatedDate, Start_Date__c, End_Date__c, Description, DOS_Comments__c, HOS_Comments__c, RM_Comments__c
                            FROM Task 
                            WHERE id =: taskID
                            ORDER BY WHATID, CreatedDate NULLS Last];
            system.debug('taskRecord.RM_Comments__c'+taskRecord.RM_Comments__c);
            if (taskRecord.WhatId == NULL)
                managerRec.Purpose_of_Meeting_Internal__c = taskRecord.Subject;
            managerRec.Related_to_Account__c = NULL;
            managerRec.Related_to_Inquiry__c = NULL;
            if (taskRecord.whatId != NULL) {
                if (taskRecord.What.Type == 'Account') {
                    managerRec.Related_to_Account__c = taskRecord.WhatID;    
                }
                if (taskRecord.What.Type == 'Inquiry__c') {
                    managerRec.Related_to_Inquiry__c = taskRecord.WhatID;    
                }
            }
            
        }
    
    }
    
    public void upsertTask(){
        System.Debug(taskRecord);
        System.Debug(taskRecord.Start_Date__c);
        System.Debug(taskRecord.End_Date__c);
        Task updatingTask = new Task();
        String taskID = Apexpages.currentpage().getparameters().get('taskID');
        System.Debug(taskID);
       
        if(taskID != NULL && taskID != '') {
            taskRecord.id = taskID;
               
            updatingTask = [SELECT Id, Description, RM_Comments__c, DOS_Comments__c, HOS_Comments__c FROM Task WHERE ID =: taskID];
            
            if (taskRecord.DOS_Comments__c != NULL) {
                
                updatingTask.DOS_Comments__c = taskRecord.DOS_Comments__c ;
                
            }
            if (taskRecord.RM_Comments__c != NULL) { 
            
                updatingTask.RM_Comments__c = taskRecord.RM_Comments__c;
                
            }
            if (taskRecord.HOS_Comments__c != NULL) { 
            
                updatingTask.HOS_Comments__c = taskRecord.HOS_Comments__c;
                
            }
            if (taskRecord.Description != NULL) { 
             
                updatingTask.Description = taskRecord.Description;
                
            }
            
           
        }
   
        update updatingTask;
        
    }
     
    
     /************************************************************************************************
* @Description : Utility Method to get all fields based on object name for query                *
* @Params      : Object API                                                                     *
* @Return      : Object related Field APIs as Comma seperated                                   *
************************************************************************************************/
    public static string getAllFields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        
        if (objectType == null)
            return fields;
        
        Map <String, Schema.SObjectField> fieldMap = objectType.getDescribe ().fields.getMap ();
        
        for (Schema.SObjectField sfield : fieldMap.Values()) {
            
            fields += sfield.getDescribe().getName ()+ ', ';
            
        }
        return fields.removeEnd(', '); 
    }
    
    
    // Method to get the sales tours based on check in date and check out date
    public void salesTours(){
        DateTime startDate = dateCriteria.Start_Date__c;
        DateTime endDate = dateCriteria.End_Date__c;
        
        String startDateStr = startDate+'';
        startDateStr = startDateStr.replace(' ', 'T')+'.000+0000';
        
        String endDateStr = endDate+'';
        endDateStr = endDateStr.replace(' ', 'T')+'.000+0000';
        
        List<Sales_Tours__c> salesTourList = new List<Sales_Tours__c>();
        Set <ID> userIds = new Set<ID>();
        for (user u: [SELECT Name FROM USER WHERE (ID IN: myTeamIds 
                                                OR ManagerId IN: myTeamIds 
                                                OR Manager.ManagerId IN: myTeamIds 
                                                OR Manager.Manager.ManagerId IN :myTeamIds)]){
            userIds.add(u.ID);
        }
        String query = 'SELECT '+getAllFields('Sales_Tours__c')
                            +', Owner.Name, Inquiry__r.Full_Name__c FROM Sales_Tours__c '
                            +' WHERE Property_Consultant__c IN: userIds '
                            +'AND CreatedDate  >= '+startDateStr 
                            +'AND CreatedDate <= '+endDateStr
                            +'AND Inquiry_Owner_Match__c = TRUE ';

        if (meetingTypeFilter == 'ST' || meetingTypeFilter == '' || meetingTypeFilter == '--None--') {
            if (relatedToFilter == 'Inquiry'){
                query += 'AND Inquiry__c != NULL ' ;
            } 
        }
        query += ' Order BY Owner.Name ASC NULLS LAST';
        salesTourList = Database.query(query);
        if(salesTourList.size() > 0) {
            for (Sales_Tours__c tours : salesTourList) {
                recordTypeMap.put (tours.id, '');
                Activity_Manager__c manager = new Activity_Manager__c();
                manager.Related_to_Sales_Tours__c = tours.id;
                manager.Activity_or_Task_Id__c = tours.id;
                activityManager.add(manager);
                activityORSTDetails.put (tours.Id, tours);
                System.Debug(':::::::: Map ::::::'+activityORSTDetails.keySet());
            }       
        }
    }
    public Double offset{get{
        TimeZone tz = UserInfo.getTimeZone();
        //Milliseconds to Day
        return tz.getOffset(DateTime.now()) / (1000 * 3600 * 24.0);
    }}
}