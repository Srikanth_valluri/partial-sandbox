@RestResource(urlMapping='/SaveUpdatePassportDetails/*')
 Global class SaveUpdatePassportDetails
 {
     @HtTPPost
    Global static Case SaveUpdatePassport(PassPortUpdateWraper passPortUpdateWraper)
    {
     Case cocdSR=new Case();
       try{
       
        cocdSR.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(passPortUpdateWraper.RecordType).getRecordTypeId();
       Group g=[select Id,name from Group where Type = 'Queue' and name='Contact Center Queue'];
        cocdSR.AccountId =passPortUpdateWraper.AccountID;
       // cocdSR.New_CR__c =passPortUpdateWraper;
        cocdSR.Passport_Issue_Date__c = passPortUpdateWraper.PassportIssuedDate;
        cocdSR.Passport_Issue_Place__c = passPortUpdateWraper.PassportIssuedPlace;
        cocdSR.Passport_Issue_Place_Arabic__c = passPortUpdateWraper.PassportIssuedPlaceArabic;
        cocdSR.Status=passPortUpdateWraper.status;
        cocdSR.Origin=passPortUpdateWraper.origin;
        cocdSR.Additional_Doc_File_URL__c=passPortUpdateWraper.AdditionalDocFileUrl;
        cocdSR.Passport_File_URL__c=passPortUpdateWraper.PassportFileUrl;
        cocdSR.Type = 'Passport Detail Update SR';
        cocdSR.SR_Type__c = 'Passport Detail Update SR';
        cocdSR.New_CR__c=passPortUpdateWraper.passportNo;
         cocdSR.id=passPortUpdateWraper.salesforceId;
         cocdSR.ownerId=g.id;
        //cocdSR.SR_Type__c
        upsert cocdSR;
       //   PushNotificationToMobileApp.PushNotificationSend(cocdSR.CaseNumber,passPortUpdateWraper.fcm);
        system.debug('*****case id '+cocdSR.id);
        
        }catch(Exception e)
        {
           system.debug('*****case Error '+e.getMessage());
           cocdSR.Payment_Terms_Error__c=e.getMessage();
          return cocdSR; 
        }
        
        Case caseList = [Select id,OwnerId, Status, Origin, Additional_Doc_File_URL__c from Case Where id = :cocdSR.id];
         if(cocdSR.status=='Submitted')
                    { 
         Task objTask = new Task();
                try{
                objTask = TaskUtility.getTask((SObject)caseList, 'Verify COD Documents', 'CRE', 
                               'Complaint', system.today().addDays(1));
              //  objTask.Parent_Task_Id__c=complaintCaseObj.id;
                objTask.OwnerId = Label.DefaultCaseOwnerId;
                objTask.Priority = 'High';
                objTask.Status = 'In Progress';
               // objTask.currencyISOcode = caseList.currencyISOcode ;
                system.debug('--objTask--Promotions'+objTask);
                
                  insert objTask;
                  system.debug('>>>>>>>objTask'+objTask);
                  
                  
                      if(String.isNotBlank(cocdSR.Passport_File_URL__c))
                {
                SR_Attachments__c srAttchmentObj = new SR_Attachments__c(Case__c = cocdSR.id,Account__c = cocdSR.AccountId,Type__c = extractType(cocdSR.Passport_File_URL__c),Name= 'Customer Passport '+ System.today(), Attachment_URL__c = cocdSR.Passport_File_URL__c);
                insert srAttchmentObj;
                }
                if(String.isNotBlank(cocdSR.Additional_Doc_File_URL__c))
                {
                SR_Attachments__c srAttchmentObj = new SR_Attachments__c(Case__c = cocdSR.id,Account__c = cocdSR.AccountId,Type__c = extractType(cocdSR.Additional_Doc_File_URL__c),Name= 'Additional Document '+ System.today(), Attachment_URL__c =  cocdSR.Additional_Doc_File_URL__c);
                insert srAttchmentObj;
                }
                
                
                }  
                catch(exception e){
                  System.debug('task exception '+ e);
                }  
                }                      
       return cocdSR; 
    }
    
   
    
         public static String extractType( String strName ) {
        strName = strName.substring( strName.lastIndexOf('\\')+1 );
        return strName.substring( strName.lastIndexOf('.')+1 ) ;
    } 
        
        
 Global class PassPortUpdateWraper
  {
        public String RecordType;
        public String AccountID;
        public Date PassportIssuedDate;  
        public String PassportIssuedPlace;
        public String PassportIssuedPlaceArabic;
        public String status;
        public String origin;
        public String PassportFileUrl;
        public String AdditionalDocFileUrl;
        public String fcm;
        public String salesforceId;
        public String passportNo;
  }
}