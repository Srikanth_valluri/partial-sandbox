public class AP_AgentPortalNewCILController {
    public Inquiry__c insertInquery{get;set;}
    public String UserName{get;set;}
    public String passExp {get;set;}
    public String firstName;
    public String lastName;
    public String emailId;
    public String mobileNo;
    public String countryCode;
    public Integer marketinCampaignListsize {get;set;}
    //public List<Campaign__c> marketinCampaignList { get; set;}
    public string marketingCampaign         { get; set;}
    public String strSelectedLanguage {get; set;}
    
    public List<SelectOption> getRMs() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--None--'));
        for (User u : [SELECT Name FROM user WHERE IsActive = TRUE 
                        AND Profile.Name LIKE '%Property Consultant%'])
        {
            options.add(new SelectOption(u.Id, u.Name));
        }
        return options;
    }
    public AP_AgentPortalNewCILController() {
        insertInquery = new Inquiry__c();
        UserName = userInfo.getName(); 
        marketingCampaign  = '';
        firstName = checkStringValueAndAssign(Apexpages.currentpage().getparameters().get('firstName'));
        lastName = checkStringValueAndAssign(Apexpages.currentpage().getparameters().get('lastName'));
        emailId = checkStringValueAndAssign(Apexpages.currentpage().getparameters().get('emailId'));
        mobileNo = checkStringValueAndAssign(Apexpages.currentpage().getparameters().get('mobileNo'));
        countryCode = checkStringValueAndAssign(Apexpages.currentpage().getparameters().get('countryCode'));
        strSelectedLanguage = checkStringValueAndAssign(Apexpages.currentpage().getparameters().get('langCode'));
        marketinCampaignListsize = 0;
        //system.debug('>>>>marketinCampaignListsize1>>>>>'+marketinCampaignListsize);
        insertInquery.First_Name__c = firstName;
        insertInquery.Last_Name__c = lastName;
        insertInquery.Email__c = emailId;
        insertInquery.Mobile_Phone_Encrypt__c = mobileNo;
        insertInquery.Mobile_CountryCode__c = countryCode;
        
        /*User objUser =[Select Name
                        From User 
                        WHERE ID =: currentUser
                        Limit 1];
        UserName= objUser.Name; */
    }
    public List<SelectOption> getMarketingCampaigns() {
        List<Assigned_Agent__c> assignedAgentList = new List<Assigned_Agent__c>();
        assignedAgentList = [SELECT 
                                id,Campaign__c,Campaign__r.Campaign_Name__c,User__c
                            From 
                                Assigned_Agent__c
                            WHERE 
                                User__c =:UserInfo.getUserId()
                                AND 
                                    Campaign__r.Eligible_for_Agent_Portal__c = true
                                AND 
                                    Campaign__r.Marketing_Start_Date__c <=today  
                                AND 
                                   Campaign__r.Marketing_End_Date__c >= today 
                                AND
                                   Campaign__r.Active__c = true ]; 
        List<SelectOption> marketinCampaignNameList = new List<SelectOption>();
        /*marketinCampaignList = new List<Campaign__c>();
        marketinCampaignList = [SELECT
                                    id,Campaign_Name__c
                                From
                                    Campaign__c 
                                WHERE
                                    Active__c = true
                                AND 
                                    Eligible_for_Agent_Portal__c = true
                                AND 
                                    Marketing_Start_Date__c <=today  
                                AND 
                                    Marketing_End_Date__c >= today ];*/

        //system.debug('>>>>marketinCampaignList>>>>>'+marketinCampaignList);
        //marketinCampaignListsize = marketinCampaignList.size();
        //system.debug('>>>>marketinCampaignListsize>>>>>'+marketinCampaignListsize);
        for( Assigned_Agent__c assignedObj : assignedAgentList)
        {
            marketinCampaignNameList.add(new SelectOption(assignedObj.Campaign__c, assignedObj.Campaign__r.Campaign_Name__c)); 
        }       
        return marketinCampaignNameList;
    } 
    public String checkStringValueAndAssign(String paramVal) {
        if(String.isNotBlank(paramVal)) {
            paramVal = paramVal;

        } else {
            paramVal = '';
        }
        return paramVal;
    }
    public PageReference Save(){
        //database.insert(insertInquery);
        System.debug('...marketinCampaignListsize....'+marketinCampaignListsize);
        ID uId = UserInfo.getUserID(); 
        List<Inquiry__c> listInquiry =new List<Inquiry__c>(); 
        List<User> listUser = new List<User>();
        listUser = [select ContactID from User where Id =:uId];
        if(String.isnotBlank(passExp)){
            String[] formatdate = passExp.split('/');
            Date formatissueDate = Date.newInstance(Integer.valueof(formatdate[2].trim()),Integer.valueof(formatdate[0].trim()),Integer.valueof(formatdate[1].trim()));
            insertInquery.Passport_Expiry_Date__c = formatissueDate;
        }
        if(marketinCampaignListsize == 0){
            insertInquery.Sourcing__c = 'Self Sourced' ;
        }
        insertInquery.Agent_Name__c = listUser[0].ContactID;
        System.debug('...marketingCampaign....'+marketingCampaign);
        insertInquery.Campaign__c = marketingCampaign;
        Contact loginContact = AgentPortalUtilityQueryManager.getContactInformation();
        insertInquery.Agency_Name__c = loginContact.AccountId;
        //insertInquery.Inquiry_Source__c = 'Agent Referral';
        System.debug('>>>>>>insertInquery>>>>>>>>'+insertInquery);
        boolean updateLater = false;
        Inquiry__c inq2Update = new Inquiry__c();
        if(insertInquery.RM__c != null){
            updateLater = true;
            inq2Update.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(DAMAC_Constants.INQUIRY_RT).getRecordTypeId();
            inq2Update.OwnerId = insertInquery.RM__c;
        }
        listInquiry.add(insertInquery);       
        Database.SaveResult[] srList = Database.insert(listInquiry, false);
        System.debug('...srList....'+srList);
        PageReference pageRef;
        //PageReference pageRef;
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                System.debug('Successfully inserted cil. cil ID: ' + sr.getId());
                if(updateLater){
                    inq2Update.Id = sr.getId();
                    update inq2Update;
                }
                
                
                pageRef = Page.AP_AgentPortalLeads;
                //pageRef = new PageReference('/AP_AgentPortalLeads');
                pageRef.setRedirect(true);
                
            }
            else {
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, err.getMessage()));
                    System.debug('cil fields that affected this error: ' + err.getFields());
                    //pageRef = new PageReference('/AgentPortal#NewCILID');
                }
            }
        }

        return pageRef;
    }
}