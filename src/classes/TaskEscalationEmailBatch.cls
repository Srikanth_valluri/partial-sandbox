/**
 * Ver       Date            Author                 Modification
 * 1.0    9/23/2019         Arsh Dave              Initial Version                  
 * 2.0    10/08/2019        Arsh Dave              Updated the fields to random fields as new fields cannot be deployet on production   
**/
global class TaskEscalationEmailBatch implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful{
    
    public String query;
    public Map<Id, Set<Task>> mapEscalatedUserIdVsTaskDetails;
    public List<Task> listTaskUpdated;

    public TaskEscalationEmailBatch(Map<Id, Set<Task>> mapEscalatedUserIdVsTaskDetails, List<Task> listTaskUpdated){
        this.mapEscalatedUserIdVsTaskDetails = mapEscalatedUserIdVsTaskDetails;
        this.listTaskUpdated = listTaskUpdated;
        System.debug('-->> TaskEscalationEmailBatch INside mapEscalatedUserIdVsTaskDetails : '  );
    }

    public Database.Querylocator start( Database.BatchableContext bc ) {
        query = 'SELECT Id, OwnerId, Owner.name, Status, WhatId, ActivityDate, Age__c, First_Property__c, CreatedDate, Subject '+
                'FROM Task '+
                'Where Status != \'Completed\' AND Process_Name__c = \'General Inquiry\' AND First_Property__c = TRUE LIMIT 1';
        System.debug('-->> INside query  TaskEscalationEmailBatch: '  + query);
        return Database.getQueryLocator(query);
    }

    public void execute( Database.BatchableContext bc, list<Task> lstTask ) {
        String toAddress = '', fromAddress,replyToAddress = '', contentType, contentValue = '',contentBody = '';
        String subject, strAccountId, strRelatedToId, strDPIId, bccAddress ='', strCCAddress;
        System.debug('-->> INside Finish : '  );
        fromAddress = 'no-replysf@damacgroup.com';
        bccAddress = Label.Sf_Copy_Bcc_Mail_Id;
        contentType = 'text/html';
        strCCAddress = '';
        subject = System.Label.TaskEscalation_Emai_Subject_GE;

        Map<Id, User> userMap = new Map<Id, User>([
            SELECT Id
                 , Name
                 , Email 
              FROM User 
             WHERE ID IN:mapEscalatedUserIdVsTaskDetails.keySet()
        ]);
        System.debug('-->> userMap batch : ' + userMap );
        Set<Id> fmCasesIdsToQuery = new Set<Id>();
        for(Set<Task> setTaskDetails: mapEscalatedUserIdVsTaskDetails.values() ){
            System.debug('-->> setTaskDetails : ' + setTaskDetails );
            for(Task objLoopTask: setTaskDetails){
                System.debug('-->> objLoopTask : ' + objLoopTask );
                fmCasesIdsToQuery.add(objLoopTask.WhatId);
            }
        }
        Map<Id, FM_Case__c> mapIdVsfmcases = new Map<Id, FM_Case__c>([
            SELECT Id
                 , Name
                 , CreatedDate
                 , Category_GE__c
                 , Subcategory_GE__c
                 , Booking_Unit__c
                 , Account__c 
              FROM FM_Case__c 
             WHERE ID IN :fmCasesIdsToQuery
        ]);
         System.debug('-->> mapIdVsfmcases : ' + mapIdVsfmcases );
        
        for(Id userId : mapEscalatedUserIdVsTaskDetails.keySet()) {
            System.debug('-->> userId : ' + userId);
            User objuser = userMap.get(userId);
            System.debug('-->> objuser : ' + objuser);
            toAddress = objuser.Email;
            String bodyStr = '<html><body>Hi ' + objuser.Name + ' Please take a look on the escalated cases as below:<br/><br/> <hr/> '

            + '<table style="border:1px solid black; text-align:left; border-collapse:collapse; word-wrap: normal;">'
            +'<tr><th style="border:1px solid black; text-align:center; border-collapse:collapse; word-wrap: normal;">Subject</th>'
            +'<th style="border:1px solid black; text-align:center; border-collapse:collapse; word-wrap: normal;">Status</th>'
            +'<th style="border:1px solid black; text-align:center; border-collapse:collapse; word-wrap: normal;">FM Case Number</th>'
            +'<th style="border:1px solid black; text-align:center; border-collapse:collapse; word-wrap: normal;">Current Level</th>'
            +'<th style="border:1px solid black; text-align:center; border-collapse:collapse; word-wrap: normal;">Created Date</th>'
            +'<th style="border:1px solid black; text-align:center; border-collapse:collapse; word-wrap: normal;">Assigned To</th><tr>';
            for(Task task : mapEscalatedUserIdVsTaskDetails.get(userId )) {
                FM_Case__c objFM_Case = mapIdVsfmcases.get(task.WhatId);
                strAccountId = objFM_Case.Account__c;
                strRelatedToId = objFM_Case.Account__c;
                bodyStr += '<tr><td style="border:1px solid black;border-collapse:collapse; text-align:left; word-wrap: normal;">'
                + task.Subject +'</td><td style="border:1px solid black;border-collapse:collapse; text-align:left; word-wrap: normal;">'
                + task.Status +'</td><td style="border:1px solid black;border-collapse:collapse; text-align:left; word-wrap: normal;">'
                + objFM_Case.Name +'</td><td style="border:1px solid black;border-collapse:collapse; text-align:left; word-wrap: normal;">'
                + task.Age__c +'</td><td style="border:1px solid black;border-collapse:collapse; text-align:left; word-wrap: normal;">'
                + objFM_Case.CreatedDate +'</td><td style="border:1px solid black;border-collapse:collapse; text-align:left; word-wrap: normal;">'
                + task.Owner.Name +'</td><tr>';
            }
            bodyStr += '</table> <br/> <hr/></body></html>';

            SendTaskClosureEmailCustomerSRProcess.callSendGridFromBatch(toAddress, strCCAddress, bccAddress, subject, fromAddress, replyToAddress, contentType, bodyStr, strAccountId, bodyStr, strRelatedToId);

        }

    }

    public void finish( Database.BatchableContext bc ) {
        if(!listTaskUpdated.isEmpty()){
            update listTaskUpdated;
        }
    }

}