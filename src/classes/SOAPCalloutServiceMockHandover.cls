@isTest
global class SOAPCalloutServiceMockHandover implements WebServiceMock {

    /*public integer intResponseNumber ;

    global SOAPCalloutServiceMockHandover() {

    }

    global SOAPCalloutServiceMockHandover( integer intNum ) {
        intResponseNumber = intNum ;
    }*/

    global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
               system.debug('!!!!!!!!!!!!request****************************'+request);
      if( request instanceof AOPTMQClass.getMilestonePaymentDetails_element ) {
            AOPTMQClass.getMilestonePaymentDetailsResponse_element responseNew = new AOPTMQClass.getMilestonePaymentDetailsResponse_element();

            responseNew.return_x = '{"REG_TERM_PYMNT_TABLE":[{"REGISTRATION_ID":"92061","DUE_DATE":"16-NOV-2015","MILESTEON_PERCENT_VALUE":"24","PAID_AMOUNT":"300048","PAID_PERCENTAGE":"100","DUE_AMOUNT":"0","LINE_ID":"824779","MILESTONE_EVENT_AR":"فورا","DESCRIPTION":"DEPOSIT","MILESTONE_EVENT":"Immediate","INVOICE_AMOUNT":"300048","INSTALLMENT":"DP","TERM_ID":"147938"},{"REGISTRATION_ID":"74177","DUE_DATE":null,"MILESTEON_PERCENT_VALUE":"0","PAID_AMOUNT":null,"PAID_PERCENTAGE":"0","DUE_AMOUNT":null,"LINE_ID":"824778","MILESTONE_EVENT_AR":null,"DESCRIPTION":"1ST INSTALMENT","MILESTONE_EVENT":"30","INVOICE_AMOUNT":null,"INSTALLMENT":"I001","TERM_ID":"147937"},{"REGISTRATION_ID":"74177","DUE_DATE":"14-MAY-2016","MILESTEON_PERCENT_VALUE":"10","PAID_AMOUNT":"150024","PAID_PERCENTAGE":"100","DUE_AMOUNT":"0","LINE_ID":"824780","MILESTONE_EVENT_AR":"خلال 180 يوما من تاريخ البيع","DESCRIPTION":"2ND INSTALMENT","MILESTONE_EVENT":"Within 180 Days of Sale Date","INVOICE_AMOUNT":"150024","INSTALLMENT":"I002","TERM_ID":"147939"},{"REGISTRATION_ID":"74177","DUE_DATE":"10-NOV-2016","MILESTEON_PERCENT_VALUE":"10","PAID_AMOUNT":"150024","PAID_PERCENTAGE":"99","DUE_AMOUNT":"0","LINE_ID":"824781","MILESTONE_EVENT_AR":"خلال 360 يوما من تاريخ البيع","DESCRIPTION":"3RD INSTALMENT","MILESTONE_EVENT":"Within 360 days of Sale Date","INVOICE_AMOUNT":"150024","INSTALLMENT":"I003","TERM_ID":"147940"},{"REGISTRATION_ID":"74177","DUE_DATE":null,"MILESTEON_PERCENT_VALUE":"10","PAID_AMOUNT":null,"PAID_PERCENTAGE":"0","DUE_AMOUNT":null,"LINE_ID":"824782","MILESTONE_EVENT_AR":"عند إستكمال 40 ٪ من المشروع","DESCRIPTION":"4TH INSTALMENT","MILESTONE_EVENT":"On 40% Of Project Completion","INVOICE_AMOUNT":null,"INSTALLMENT":"I004","TERM_ID":"147941"},{"REGISTRATION_ID":"74177","DUE_DATE":null,"MILESTEON_PERCENT_VALUE":"10","PAID_AMOUNT":null,"PAID_PERCENTAGE":"0","DUE_AMOUNT":null,"LINE_ID":"824783","MILESTONE_EVENT_AR":"عند إستكمال 60 ٪ من المشروع","DESCRIPTION":"5TH INSTALMENT","MILESTONE_EVENT":"On 60% Of Project Completion","INVOICE_AMOUNT":null,"INSTALLMENT":"I005","TERM_ID":"147942"},{"REGISTRATION_ID":"74177","DUE_DATE":null,"MILESTEON_PERCENT_VALUE":"10","PAID_AMOUNT":null,"PAID_PERCENTAGE":"0","DUE_AMOUNT":null,"LINE_ID":"824784","MILESTONE_EVENT_AR":"عند إستكمال 70 ٪ من المشروع","DESCRIPTION":"6TH INSTALMENT","MILESTONE_EVENT":"On 70% Of Project Completion","INVOICE_AMOUNT":null,"INSTALLMENT":"I006","TERM_ID":"147943"},{"REGISTRATION_ID":"74177","DUE_DATE":null,"MILESTEON_PERCENT_VALUE":"10","PAID_AMOUNT":null,"PAID_PERCENTAGE":"0","DUE_AMOUNT":null,"LINE_ID":"824785","MILESTONE_EVENT_AR":"عند إستكمال 80 ٪ من المشروع","DESCRIPTION":"7TH INSTALMENT","MILESTONE_EVENT":"On 80% Of Project Completion","INVOICE_AMOUNT":null,"INSTALLMENT":"I007","TERM_ID":"147944"},{"REGISTRATION_ID":"74177","DUE_DATE":null,"MILESTEON_PERCENT_VALUE":"20","PAID_AMOUNT":null,"PAID_PERCENTAGE":"0","DUE_AMOUNT":null,"LINE_ID":"824786","MILESTONE_EVENT_AR":"عند الاكمال","DESCRIPTION":"8TH INSTALMENT","MILESTONE_EVENT":"On Completion","INVOICE_AMOUNT":null,"INSTALLMENT":"I008","TERM_ID":"147945"}],"message":"Successfully Fetched Details for RegID74177","status":"S"}';
            response.put('response_x', responseNew);
        }
        
        if (request instanceof CustomerPayment.customerPaymentMade_element) {
            CustomerPayment.customerPaymentMadeResponse_element  response1 = new CustomerPayment.customerPaymentMadeResponse_element ();
            response1.return_x = '{"data":[{"ATTRIBUTE3":"152486","ATTRIBUTE10":"DEPOSIT","ATTRIBUTE2":"70315","ATTRIBUTE1":"74365","ATTRIBUTE11":null,"ATTRIBUTE9":null,"ATTRIBUTE8":"Immediate","ATTRIBUTE7":null,"ATTRIBUTE6":"21-NOV-2015","ATTRIBUTE5":"24","ATTRIBUTE4":"DP"},{"ATTRIBUTE3":"152487","ATTRIBUTE10":"1ST INSTALLMENT","ATTRIBUTE2":"70315","ATTRIBUTE1":"74365","ATTRIBUTE11":null,"ATTRIBUTE9":null,"ATTRIBUTE8":null,"ATTRIBUTE7":null,"ATTRIBUTE6":null,"ATTRIBUTE5":"0","ATTRIBUTE4":"I001"},{"ATTRIBUTE3":"152488","ATTRIBUTE10":"2ND INSTALLMENT","ATTRIBUTE2":"70315","ATTRIBUTE1":"74365","ATTRIBUTE11":"180","ATTRIBUTE9":null,"ATTRIBUTE8":"Within 180 Days of Sale Date","ATTRIBUTE7":null,"ATTRIBUTE6":"19-MAY-2016","ATTRIBUTE5":"10","ATTRIBUTE4":"I002"},{"ATTRIBUTE3":"152489","ATTRIBUTE10":"3RD INSTALLMENT","ATTRIBUTE2":"70315","ATTRIBUTE1":"74365","ATTRIBUTE11":"360","ATTRIBUTE9":null,"ATTRIBUTE8":"Within 360 days of Sale Date","ATTRIBUTE7":null,"ATTRIBUTE6":"15-NOV-2016","ATTRIBUTE5":"10","ATTRIBUTE4":"I003"},{"ATTRIBUTE3":"152490","ATTRIBUTE10":"4TH INSTALLMENT","ATTRIBUTE2":"70315","ATTRIBUTE1":"74365","ATTRIBUTE11":"420","ATTRIBUTE9":null,"ATTRIBUTE8":"Within 420 days of Sale Date","ATTRIBUTE7":null,"ATTRIBUTE6":"14-JAN-2017","ATTRIBUTE5":"10","ATTRIBUTE4":"I004"},{"ATTRIBUTE3":"152491","ATTRIBUTE10":"5TH INSTALLMENT","ATTRIBUTE2":"70315","ATTRIBUTE1":"74365","ATTRIBUTE11":"540","ATTRIBUTE9":null,"ATTRIBUTE8":"Within 540 days of Sale Date","ATTRIBUTE7":null,"ATTRIBUTE6":"14-MAY-2017","ATTRIBUTE5":"10","ATTRIBUTE4":"I005"},{"ATTRIBUTE3":"152492","ATTRIBUTE10":"6TH INSTALLMENT","ATTRIBUTE2":"70315","ATTRIBUTE1":"74365","ATTRIBUTE11":"630","ATTRIBUTE9":null,"ATTRIBUTE8":"Within 630 days of Sale Date","ATTRIBUTE7":null,"ATTRIBUTE6":"12-AUG-2017","ATTRIBUTE5":"10","ATTRIBUTE4":"I006"},{"ATTRIBUTE3":"152493","ATTRIBUTE10":"7TH INSTALLMENT","ATTRIBUTE2":"70315","ATTRIBUTE1":"74365","ATTRIBUTE11":"720","ATTRIBUTE9":null,"ATTRIBUTE8":"Within 720 days of Sale Date","ATTRIBUTE7":null,"ATTRIBUTE6":"10-NOV-2017","ATTRIBUTE5":"10","ATTRIBUTE4":"I007"},{"ATTRIBUTE3":"152494","ATTRIBUTE10":"8TH INSTALLMENT","ATTRIBUTE2":"70315","ATTRIBUTE1":"74365","ATTRIBUTE11":"900","ATTRIBUTE9":null,"ATTRIBUTE8":"Within 900 days of Sale Date","ATTRIBUTE7":null,"ATTRIBUTE6":"09-MAY-2018","ATTRIBUTE5":"10","ATTRIBUTE4":"I008"},{"ATTRIBUTE3":"152495","ATTRIBUTE10":"9TH INSTALLMENT","ATTRIBUTE2":"70315","ATTRIBUTE1":"74365","ATTRIBUTE11":"1080","ATTRIBUTE9":null,"ATTRIBUTE8":"Within 1080 days of Sale Date","ATTRIBUTE7":null,"ATTRIBUTE6":"05-NOV-2025","ATTRIBUTE5":"10","ATTRIBUTE4":"I009"}],"message":"[10] Reg Term Records Fetched for Reg Id =74365","status":"S"}';
            response.put('response_x', response1);        
        }
       if(request instanceof unitDetailsController.getUnitDetailValues_element){
            unitDetailsController.getUnitDetailValuesResponse_element responseNew = new unitDetailsController.getUnitDetailValuesResponse_element();
            responseNew.return_x = '["{\"ATTRIBUTE1\":\"70479\",\"ATTRIBUTE2\":\"\",\"ATTRIBUTE3\":\"PL\",\"ATTRIBUTE4\":\"N\",\"ATTRIBUTE5\":\"-.01\",\"ATTRIBUTE6\":\"\",\"ATTRIBUTE7\":\"\",\"ATTRIBUTE8\":\"1159496\",\"ATTRIBUTE9\":\"1159496\",\"ATTRIBUTE10\":\"0\",\"ATTRIBUTE11\":\"9\",\"ATTRIBUTE12\":\"N\",\"ATTRIBUTE13\":\"Y\",\"ATTRIBUTE14\":\"N\",\"ATTRIBUTE15\":\"\",\"ATTRIBUTE16\":\"N\",\"ATTRIBUTE17\":\"N\",\"ATTRIBUTE18\":\"1159496\",\"ATTRIBUTE19\":\"0\",\"ATTRIBUTE20\":\"100\",\"ATTRIBUTE21\":\"1\",\"ATTRIBUTE22\":\"Y\",\"ATTRIBUTE23\":\"0\",\"ATTRIBUTE24\":\"0\",\"ATTRIBUTE25\":\"0\",\"ATTRIBUTE26\":\"STANDARD\",\"ATTRIBUTE27\":\"N\",\"ATTRIBUTE28\":\"N\",\"ATTRIBUTE29\":\"READY\",\"ATTRIBUTE30\":\"N\",\"ATTRIBUTE31\":\"N\",\"ATTRIBUTE32\":\"\",\"ATTRIBUTE33\":\"\",\"ATTRIBUTE34\":\"N\",\"ATTRIBUTE35\":\"48242.84\",\"ATTRIBUTE36\":\"\",\"ATTRIBUTE37\":\"\",\"ATTRIBUTE38\":\"\",\"ATTRIBUTE39\":\"10-JUL-2015\",\"ATTRIBUTE40\":\"MAG2/G/G05\",\"ATTRIBUTE41\":\"1159496\",\"ATTRIBUTE42\":\"0\",\"ATTRIBUTE43\":\"9\",\"ATTRIBUTE44\":\"0\",\"ATTRIBUTE45\":\"N\",\"ATTRIBUTE46\":\"N\",\"ATTRIBUTE47\":\"N\",\"ATTRIBUTE48\":\"N\",\"ATTRIBUTE49\":\"Y\",\"ATTRIBUTE50\":\"N\",\"ATTRIBUTE51\":\"Assignment Pending with Legal\",\"ATTRIBUTE52\":\"100\",\"ATTRIBUTE53\":\"100\",\"ATTRIBUTE54\":\"\",\"ATTRIBUTE55\":\"25-MAR-2018\",\"ATTRIBUTE56\":\"-.01\",\"ATTRIBUTE57\":\"0\",\"ATTRIBUTE58\":\"48242.84\",\"ATTRIBUTE59\":\"100\",\"ATTRIBUTE60\":\"0\",\"ATTRIBUTE61\":\"0\",\"ATTRIBUTE62\":\"0\",\"ATTRIBUTE63\":\"\",\"ATTRIBUTE64\":\"\",\"ATTRIBUTE65\":\"N\",\"ATTRIBUTE66\":\"N\",\"ATTRIBUTE67\":\"N\",\"ATTRIBUTE68\":\"0\",\"ATTRIBUTE69\":\"N\",\"ATTRIBUTE70\":\"\",\"ATTRIBUTE71\":\"\",\"ATTRIBUTE72\":\"1\",\"ATTRIBUTE73\":\"PARK_BAY\",\"ATTRIBUTE74\":\"\",\"ATTRIBUTE75\":\"3\",\"ATTRIBUTE76\":\"0\",\"ATTRIBUTE77\":\"\",\"ATTRIBUTE78\":\"-2\",\"ATTRIBUTE79\":\"\",\"ATTRIBUTE80\":\"\",\"ATTRIBUTE81\":\"\",\"ATTRIBUTE82\":\"SA - Sent to buyer for signature\",\"ATTRIBUTE83\":\"Damac Properties Company Limited\",\"ATTRIBUTE84\":\"Y\",\"ATTRIBUTE85\":\"800\",\"ATTRIBUTE86\":\"0\",\"ATTRIBUTE87\":\"100\",\"ATTRIBUTE88\":\"1159498.16\",\"ATTRIBUTE89\":\"\",\"ATTRIBUTE90\":\"MAG2\",\"ATTRIBUTE91\":\"EMIRATES GARDENS 2- MAGNOLIA 2\",\"ATTRIBUTE92\":\"\",\"ATTRIBUTE93\":\"\",\"ATTRIBUTE94\":\"976032\",\"ATTRIBUTE95\":\"N...\",\"ATTRIBUTE96\":\"1449.37\",\"ATTRIBUTE97\":\"Normal\",\"ATTRIBUTE98\":\"10-JUL-2015\",\"ATTRIBUTE99\":\"APARTMENT\",\"ATTRIBUTE100\":\"1449.36\",\"ATTRIBUTE101\":\"Y\",\"ATTRIBUTE102\":\"1159496\",\"ATTRIBUTE103\":\"Y\",\"ATTRIBUTE104\":\"Y\",\"ATTRIBUTE105\":\"0\",\"ATTRIBUTE106\":\"10-JUL-2015\",\"ATTRIBUTE107\":\"N\",\"ATTRIBUTE108\":\"EMIRATES GARDENS 2\",\"ATTRIBUTE109\":\"DUBAI\",\"ATTRIBUTE110\":\"One Bedroom\",\"ATTRIBUTE111\":\"Residential\",\"ATTRIBUTE112\":\"\",\"ATTRIBUTE113\":\"\",\"ATTRIBUTE114\":\"\",\"ATTRIBUTE115\":\"\",\"ATTRIBUTE116\":\"\",\"ATTRIBUTE117\":\"\",\"ATTRIBUTE118\":\"\",\"ATTRIBUTE119\":\"\",\"ATTRIBUTE120\":\"\"}"]';
            response.put('response_x', responseNew);
            system.debug('!!!!!!!!!response unit details'+response);
        }
        if (request instanceof DocumentationForKeyHandoverService.DoumentationForKeyHandover_element) {
            system.debug('*****HO MOCK Called*****');
            DocumentationForKeyHandoverService.DoumentationForKeyHandoverResponse_element response1 = 
            new DocumentationForKeyHandoverService.DoumentationForKeyHandoverResponse_element();
            response1.return_x = '{"allowed":"Yes","message":null,"mortgageNOCfromBank":"Not Required","ifPoaTakingHandoverColatePoaPassportResidence":"Mandatory","corporateValidTradeLicence":"Not Required","corporateArticleMemorandumOfAssociation":"Not Required","corporateBoardResolution":"Not Required","corporatePoa":"Mandatory","signedForm":"Not Required","clearAndValidPassportCopyOfOwner":"Mandatory","clearAndValidPassportCopyOfJointOwner":"Not Required","visaOrEntryStampWithUid":"Mandatory","copyofValidEmiratesId":"Not Required","copyofValidGccId":"Not Required","handoverChecklistAndLod":"Mandatory","keyReleaseForm":"Mandatory","checkOriginalSpaAndtakeCopyOfFirstFourPagesOfSpa":"Mandatory","areaVariationAddendum":"Mandatory","tempOne":"Mandatory","tempTwo":null,"tempThree":null,"handoverNoticeAllowed":null,"approvalQueueOne":null,"approvalQueueTwo":null,"approvalQueueThree":null,"eligibleforRentalPool":null}';
            //response1.put('response_x', response1);
            response.put('response_x', response1);
        }
        if (request instanceof AOPTRule.AoptDetailsResponse_element) {           
            AOPTRule.AoptDetailsResponse_element response_x =  new AOPTRule.AoptDetailsResponse_element();
            response_x.return_x = '{"allowed":"Yes","message":null,"recommendingAuthorityOne":"One","recommendingAuthorityTwo":"Two","recommendingAuthorityThree":"Three","recommendingAuthorityFour":"Four","approvingAuthorityOne":"One","approvingAuthorityTwo":"Two","approvingAuthorityThree":"Three","percToBeRefundedTransferred":null,"deductionFeePsf":null,"deductionFeeFlat":null,"deductionFeePercentage":null}';
           
            SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);     
        } 
         if(request instanceof MultipleDocUploadService.DocumentAttachmentMultiple_element){
          MultipleDocUploadService.DocumentAttachmentMultiple_element request_x = (MultipleDocUploadService.DocumentAttachmentMultiple_element)request;
          system.debug('debug this value******************'+request_x.regTerms);
          List<beanComXsdMultipleDocUpload.DocUploadDTO>  instance = request_x.regTerms;
          system.debug('filename**********'+instance[0].fileName);
          string RegId = instance[0].fileName.substringBetween('IPMS-','-');
          MultipleDocUploadService.DocumentAttachmentMultipleResponse_element responseNew
          = new MultipleDocUploadService.DocumentAttachmentMultipleResponse_element();
          if(instance[0].fileName.contains('POA')){
              responseNew.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS]Files : [IPMS-'+RegId+'-BUYER_POA.pdf Processed] PK Value#:2-2-005185","PARAM_ID":"IPMS-'+RegId+'-BUYER_POA","URL":"https://sftest.deeprootsurface.com/docs/t/IPMS-'+RegId+'-BUYER_POA.pdf"},{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS]Files : [IPMS-'+RegId+'-SELLER_POA.pdf Processed] PK Value#:2-2-005185","PARAM_ID":"IPMS-'+RegId+'-SELLER_POA","URL":"https://sftest.deeprootsurface.com/docs/t/IPMS-'+RegId+'-SELLER_POA.pdf"}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';
          }else{
              responseNew.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS]Files : [IPMS-'+RegId+'-TEST.pdf Processed] PK Value#:2-2-005185","PARAM_ID":"IPMS-'+RegId+'-TEST","URL":"https://sftest.deeprootsurface.com/docs/t/IPMS-'+RegId+'-TEST.pdf"}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';
          }
          response.put('response_x', responseNew);
        } 
        
        if(request instanceof spaGeneration.processSPA_element) {
            spaGeneration.processSPAResponse_element respElement = new spaGeneration.processSPAResponse_element();
            respElement.return_x = '{"data":[{"PROC_STATUS":"E","PROC_MESSAGE":"[EX_ERROR]Process with Id=466228 and Request Id :42069149 Aborting...","ATTRIBUTE3":"SPA","ATTRIBUTE2":null,"ATTRIBUTE1":"https://sftest.deeprootsurface.com/docs/t/IPMS-005681-1513694208030.xlsx","PARAM_ID":"2-005644","ATTRIBUTE4":null}],"MESSAGE_ID":"2-1514964001837","message":"[WARNING] Check the PROC_MESSAGE attribute for Actual Error Message(s), Error Message Count = 1","status":"S"}';
            response.put('response_x', respElement);    
        }
        
        if ( request instanceof SnagWSDL.defects_element) {
            SnagWSDL.defectsResponse_element respElement = new SnagWSDL.defectsResponse_element();
            respElement.return_X = '[{"ID":90006,"GUID":"E642984D-FAEF-4A4E-A93B-7162A0032F69","DrawingID":448,"DrawingTitle":"SSSD44704","Status":"Open","CurrentStatus":"MCClosed","UserDescription":"Handed over apartments hence closed.","LocationID":4756,"Location":"Entrance","LocationExtRef":"SS/SD44/704","LocationGUID":"","XCoord":163,"YCoord":498,"PriorityID":3,"Priority":"High","DueBy":"2016-12-16T08:04:00","DaysLeft":-450,"GroupID":2,"GroupName":"Project Mgmt Team Client Inspections","GroupInitials":"PMT","DateClosed":null,"BigPhoto":null,"Lattitude":0.0,"Longitude":0.0,"CreatedByID":21,"CreatedBy":"CREP Mahmoud Bseiso","CreatedDate":"2016-12-20T08:04:00","SDCategoryID":3,"SDCategory":"Civil","SDCode":"STD012","SDType":"Walls","SDID":71,"SDDescription":"Finish & plumpness of plastering and paint.","DateSync":"2017-11-15T06:14:30.737","MPGUID":"","FCGUID":"","FutureDefectGUID":"","PreviousDefectGUID":"","ProjectTitle":"AKOYA Villas - Silverspring","ContractNo":"GHANTOOT","Role":1,"UGUserID":85,"HistoryCount":3,"LastChanged":"2017-03-27T10:17:38.247","PhotoExists":true},{"ID":89983,"GUID":"8623985F-7C3F-4E6C-8236-702F1ECB30D5","DrawingID":448,"DrawingTitle":"SSSD44704","Status":"Open","CurrentStatus":"MCClosed","UserDescription":"Handed over apartments hence closed.","LocationID":4756,"Location":"Entrance","LocationExtRef":"SS/SD44/704","LocationGUID":"","XCoord":152,"YCoord":544,"PriorityID":3,"Priority":"High","DueBy":"2016-12-16T07:47:00","DaysLeft":-450,"GroupID":2,"GroupName":"Project Mgmt Team Client Inspections","GroupInitials":"PMT","DateClosed":null,"BigPhoto":null,"Lattitude":0.0,"Longitude":0.0,"CreatedByID":21,"CreatedBy":"CREP Mahmoud Bseiso","CreatedDate":"2016-12-20T07:47:00","SDCategoryID":4,"SDCategory":"Joinery","SDCode":"STD015","SDType":"Doors","SDID":98,"SDDescription":"Door Frame, Architrave & Shuters - Paint Finish/cleaning","DateSync":"2017-11-15T06:14:30.737","MPGUID":"","FCGUID":"","FutureDefectGUID":"","PreviousDefectGUID":"","ProjectTitle":"AKOYA Villas - Silverspring","ContractNo":"GHANTOOT","Role":1,"UGUserID":85,"HistoryCount":3,"LastChanged":"2017-03-27T10:17:42.217","PhotoExists":true}]';
            response.put('response_x', respElement);
        }
        
        if (request instanceof SnagWSDL.LocationExtRef_element) {
            SnagWSDL.LocationExtRefResponse_element respElement = new SnagWSDL.LocationExtRefResponse_element();
            respElement.return_X = ' [{"LocID":4756,"DrwgID":448,"Xcoord":111,"Ycoord":516,"Location":"Entrance","Datestamp":"2016-05-31T21:27:00Z",'+
                ' "Lat":0.0,"Lng":0.0,"LocGUID":"7121008a-38e2-40b0-bf81-6ece51dcb2a2","DateSync":"2017-03-26T11:42:40.01Z","ExtRef":"SS/SD44/704",'+
                ' "Closed":false,"RFDI":false,"LocationTypeID":0}]';
            response.put('response_x', respElement);
        }
    }
}