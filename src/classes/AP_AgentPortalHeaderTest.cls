@isTest
public class AP_AgentPortalHeaderTest {
  public static Contact adminContact;
    public static User portalUser;
    public static Account adminAccount;
    public static User portalOnlyAgent;
    public static Contact agentContact;
    
    static void init(){
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole1');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 't446', email='xyz441@email.com',
                emailencodingkey='UTF-8', lastname='User 45446', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz144@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
        adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
        insert adminAccount;
        
        adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
        insert adminContact;
        
        agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
        insert agentContact;
        
        portalUser = InitialiseTestData.getPortalUser('test99@test.com', adminContact.Id, 'Admin');
        portalOnlyAgent = InitialiseTestData.getPortalUser('test2221@test.com', agentContact.Id, 'Agent');
        
        Announcement__c futureannouncement = InitialiseTestData.createAnnouncement(System.now().Date(),System.now().Date().addDays(30),true,
                                                                  'SILVER','All');
        insert futureannouncement;
        
        System.runAs(portalUser){
          Id RecordTypeIdCampaign = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Agent Deal').getRecordTypeId();
            Campaign__c campaign = new Campaign__c();
            campaign.Sales_Admin_Active__c = true;
            campaign.Marketing_Active__c = true;
            campaign.Credit_Control_Active__c = true;
            campaign.Campaign_Description__c = 'This is a test campaign';
            campaign.Campaign_Location__c = 'United Arab Emirates';
            campaign.Status__c = 'In Progress';
            campaign.Campaign_Name__c = 'Test123';
            campaign.Start_Date__c=System.now().Date();
            campaign.End_Date__c = System.now().Date().addDays(10);
            campaign.Marketing_Start_Date__c = System.now().Date();
            campaign.Marketing_End_Date__c = System.now().Date().addDays(10);
            campaign.Lead_Prioritization__c	 ='General Digital';
            campaign.recordtypeid=RecordTypeIdCampaign;
            insert campaign;

            Assigned_Agent__c assignedAgents = new Assigned_Agent__c();
            assignedAgents.End_Date__c = System.now().Date().addDays(5);
            assignedAgents.Start_Date__c = System.now().Date();
            assignedAgents.User__c = portalUser.Id;
            assignedAgents.Campaign__c = campaign.Id;
            insert assignedAgents;
        }
        }
       
    }
    
    @isTest static void showNotificationToAdmin(){
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole2');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 't336', email='xyz133@email.com',
                emailencodingkey='UTF-8', lastname='User 45336', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz133@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
        Test.startTest();
        init();
        System.runAs(portalUser){
            Notification__c notification = InitialiseTestData.createNotification(adminAccount.Id,adminContact.Id);
            AP_AgentPortalHeader sidebar = new AP_AgentPortalHeader ();
            sidebar.latestAnnouncement = null;
            sidebar.noAnnounements = true;
            sidebar.isAuthorisedToUpload = true;
           
        }
        
        Test.stopTest();
        }
    }
    
    @isTest static void showNotificationToAgent(){
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole3');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'te26', email='xyz221@email.com',
                emailencodingkey='UTF-8', lastname='User 45226', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz221@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
        Test.startTest();
        init();
        
        System.runAs(portalOnlyAgent){
            Notification__c notification = InitialiseTestData.createNotification(adminAccount.Id,agentContact.Id);
           AP_AgentPortalHeader sidebar = new AP_AgentPortalHeader ();
        }
        
        Test.stopTest();
        }
    }
    @isTest static void clearFieldsTest(){
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'te56', email='xyz111@email.com',
                emailencodingkey='UTF-8', lastname='User 41156', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz111@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
        
        Test.startTest();
        init();
        
        System.runAs(portalOnlyAgent){
            Notification__c notification = InitialiseTestData.createNotification(adminAccount.Id,agentContact.Id);
           AP_AgentPortalHeader sidebar = new AP_AgentPortalHeader ();
        }
        AP_AgentPortalHeader.clearFields();
        AP_AgentPortalHeader.updatePassword('12345678','12345678','23456789');
        AP_AgentPortalHeader.updatePassword('','','23456789');
        Test.stopTest();
        }
    }


}