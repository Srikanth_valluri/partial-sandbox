// Created By : Naresh Kaneriya (Accely)
public class MortageTaskCreation{
    
    @future(CallOut=true)
    public static void MortgageTask(Set<string> CaseListId){
         
        Set<string> BookingId=new  Set<string>();
        Booking_Unit__c BUlist =new  Booking_Unit__c();
        List<Task> TaskList=new  List<Task>();
        Map<Id ,Case> CaseList =new  Map<Id,Case>();
        List<string> RegID =new  List<string>();
        UnitDetailsService.BookinUnitDetailsWrapper objDetailsWrapper=new  UnitDetailsService.BookinUnitDetailsWrapper();
            
        for(Case c: [SELECT id,Type,status,OwnerId ,Booking_Unit__r.Registration_ID__c ,RecordType.Name,AccountId,Booking_Unit__c FROM Case WHERE Id=:CaseListId]){
            
             System.debug('--c.RecordType.Name-- '+c.RecordType.Name);
             System.debug('--- c.Type---- '+c.Type);
             System.debug('---c.AccountId---- '+c.AccountId);
             System.debug('--c.Booking_Unit__c-- '+c.Booking_Unit__c);
             BookingId.add(c.Booking_Unit__c);
             CaseList.put(c.Id,c);
             RegID.add(c.Booking_Unit__r.Registration_ID__c);
     		  
        }
        System.debug('-RegIDRegID- '+RegID);
        
        // To get Booking Unit Details  
        if(!BookingId.isEmpty())
       
          if(!RegID.isEmpty()){
          
            objDetailsWrapper=UnitDetailsService.getBookingUnitDetails(RegID[0]);
            System.debug('---------- Response---  '+objDetailsWrapper.strReady_Offplan);
        }
        
        
        if(string.isNotBlank(objDetailsWrapper.strReady_Offplan)){
            for(Case c: CaseList.values()){
                System.debug(' ----------------------- Task inseted------------- ');
                if(objDetailsWrapper.strReady_Offplan.EqualsIgnoreCase('OFF-PLAN') || objDetailsWrapper.strReady_Offplan.EqualsIgnoreCase('READY') ){
                    
                    System.debug('---------------Off Plan-----------');
                       // Task Number 21 Or 26 
                       Task newTask=new  Task();
                        newTask.WhatId=c.Id;  
                        newTask.ActivityDate=System.TODAY()+2;   
                        newTask.Status='Not Started';
                        newTask.Subject = objDetailsWrapper.strReady_Offplan.EqualsIgnoreCase('Off Plan') ? System.Label.Mortgage_21 : System.Label.Mortgage_26 ;
                        newTask.OwnerId=c.OwnerId;
                        newTask.Priority='Normal';
                        TaskList.add(newTask);    
                    
                }  
             }
        }
        try{
        if(!TaskList.isEmpty())
           Insert TaskList;
		}
		catch(Exception ex){
			System.debug('---Error Msg--- '+ex.getMessage());
			System.debug('---Line Number -- '+ex.getLineNumber());
		}
        System.debug('---- TaskList-- '+TaskList);
    }

}