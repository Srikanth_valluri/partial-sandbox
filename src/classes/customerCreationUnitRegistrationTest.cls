/*
Test class for customerCreationUnitRegistration
*/
@isTest
public class customerCreationUnitRegistrationTest {
    
    // Method for APPSXXDC_SFDC_REGISTX1809884X1X1 class
    @isTest
    public static void instanceCreationForClass_APPSXXDC_SFDC_REGISTX1809884X1X1 () {
        Test.startTest();
        customerCreationUnitRegistration.APPSXXDC_SFDC_REGISTX1809884X1X1 APPSXXDC_SFDC_REGISTX1809884X1X1_Obj  = 
            new customerCreationUnitRegistration.APPSXXDC_SFDC_REGISTX1809884X1X1 () ;
         Test.stopTest();
    }
    
    // Method for APPSXXDC_SFDC_REGISX1809884X1X42 class
    @isTest
    public static void instanceCreationForClass_APPSXXDC_SFDC_REGISX1809884X1X42 () {
        Test.startTest();
        customerCreationUnitRegistration.APPSXXDC_SFDC_REGISX1809884X1X42 APPSXXDC_SFDC_REGISX1809884X1X42_Obj =
            new customerCreationUnitRegistration.APPSXXDC_SFDC_REGISX1809884X1X42 () ;
        Test.stopTest();  
        
    }
    
    // Method for APPSXXDC_SFDC_REGIX1809884X1X106 class
    @isTest
    public static void instanceCreationForClass_APPSXXDC_SFDC_REGIX1809884X1X106  () {
        Test.startTest();
        customerCreationUnitRegistration.APPSXXDC_SFDC_REGIX1809884X1X106 APPSXXDC_SFDC_REGIX1809884X1X106_Obj =
            new customerCreationUnitRegistration.APPSXXDC_SFDC_REGIX1809884X1X106 () ;
         Test.stopTest();
    }
    
    // Method for APPSXXDC_SFDC_REGISX1809884X1X77 class
    @isTest
    public static void instanceCreationForClass_APPSXXDC_SFDC_REGISX1809884X1X77 () {
        Test.startTest();
        customerCreationUnitRegistration.APPSXXDC_SFDC_REGISX1809884X1X77 APPSXXDC_SFDC_REGISX1809884X1X77_Obj =
            new customerCreationUnitRegistration.APPSXXDC_SFDC_REGISX1809884X1X77 () ;
        Test.stopTest();
    }
    
    // Method for APPSXXDC_SFDC_REGISX1809884X1X23 class
    @isTest
    public static void instanceCreationForClass_APPSXXDC_SFDC_REGISX1809884X1X23 () {
        Test.startTest();
        customerCreationUnitRegistration.APPSXXDC_SFDC_REGISX1809884X1X23 APPSXXDC_SFDC_REGISX1809884X1X23_Obj = 
            new customerCreationUnitRegistration.APPSXXDC_SFDC_REGISX1809884X1X23 () ;
         Test.stopTest();
    }
     
}