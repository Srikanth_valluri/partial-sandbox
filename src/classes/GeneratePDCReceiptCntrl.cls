public class GeneratePDCReceiptCntrl {
    public static Id pdcId {get;set;}
    public static String showResponse {get;set;}
    public static List<String> lstReceiptURLs {get;set;}
    
    public GeneratePDCReceiptCntrl(ApexPages.StandardController stdController){
        pdcId = (Id)stdController.getRecord().Id;
    }
    
      public void init(){
        
        System.debug('pdcId----->'+pdcId); 
        
        lstReceiptURLs = new List<String>();
        showResponse = '';
        List<Case_Post_Dated_Cheque__c> listPDC = [SELECT IPMS_Cash_Receipt_Id__c
                                                   FROM Case_Post_Dated_Cheque__c
                                                   WHERE Id =: pdcId];
        
        System.debug('listPDC----->'+listPDC);
        System.debug('listPDC[0].IPMS_Cash_Receipt_Id__c----->'+listPDC[0].IPMS_Cash_Receipt_Id__c);
        
        if(listPDC[0].IPMS_Cash_Receipt_Id__c != null && listPDC[0].IPMS_Cash_Receipt_Id__c !=''){
            for(Case_Post_Dated_Cheque__c objPDC : listPDC){
                
                System.debug('objPDC----->'+objPDC);
                
                lstReceiptURLs = FmIpmsRestCoffeeServices.getAllReceiptUrl(String.valueOf(objPDC.IPMS_Cash_Receipt_Id__c));
                
                System.debug('lstReceiptURLs----->'+lstReceiptURLs);
                
                if(lstReceiptURLs.isEmpty()){
                    showResponse = 'URL is not present for the cash Receipt Id';
                    ApexPages.addmessage( new ApexPages.message( ApexPages.severity.INFO 
                                                                ,showResponse
                                                               )
                                        );
                }
                System.debug('lstReceiptURLs----->'+lstReceiptURLs);
            }
        }
        else{
            showResponse = 'Cash Receipt Id not present';
            ApexPages.addmessage( new ApexPages.message( ApexPages.severity.INFO 
                                                        ,showResponse
                                                       )
                                );
        }
        
    }
}