@isTest
private class FetchDocumentURLTest
{
	static  testmethod void testAutoRun(){
		Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
		list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
	    insert accountobj;
	    Case caseobj =TestDataFactory_CRM.createCase(accountobj[0].id,caseRecordTypeId);
		insert caseobj;
		SR_Attachments__c dcobj = TestDataFactory_CRM.createCaseDocument(caseobj.id,'docDeveloperName');
		insert dcobj;
		test.startTest();
		FetchDocumentURL.fetchDocURL('docDeveloperName');
		test.stopTest();
	}

 
}