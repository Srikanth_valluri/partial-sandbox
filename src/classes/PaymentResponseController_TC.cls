@isTest 
Private class PaymentResponseController_TC {
    static testMethod void PaymentResponseControllerMethod() {
        Receipt__c R = new Receipt__c();
        //R.Customer__c=objAccount.id;
        R.Amount__c=100; 
        R.Receipt_Type__c='Card';
        R.Transaction_Date__c=system.now();           
        
        Id rt=Schema.SObjectType.Receipt__c.getRecordTypeInfosByName().get('Card').getRecordTypeId();
        R.RecordTypeId=rt;
        insert R;
        
        Test.starttest();
        Receipt__c newR =[select id,name,Payment_Gateway_Ref_No__c,Amount__c,Card_Amount__c from Receipt__c where Receipt_Type__c= 'Card' limit 1];
        
        PageReference pageRef = Page.CC_ThankYouPage;
        Test.setCurrentPage(pageRef);
        System.debug('sad'+newR.Name);
        ApexPages.currentPage().getParameters().put('orderNo',newR.Name);
        
        PaymentResponseController response = NEW PaymentResponseController ();
        response.generateReciept();
        Test.stopTest();
    }
      static testMethod void PaymentResponseControllerMethod2() {
        Receipt__c R = new Receipt__c();
        //R.Customer__c=objAccount.id;
        R.Amount__c=100; 
        R.Receipt_Type__c='Card';
        R.Payment_Response__c ='test';
        R.Transaction_Date__c=system.now();           
        
        Id rt=Schema.SObjectType.Receipt__c.getRecordTypeInfosByName().get('Card').getRecordTypeId();
        R.RecordTypeId=rt;
        insert R;
        
        Test.starttest();
        Receipt__c newR =[select id,name,Payment_Gateway_Ref_No__c,Amount__c,Card_Amount__c from Receipt__c where Receipt_Type__c= 'Card' limit 1];
        
        PageReference pageRef = Page.CC_ThankYouPage;
        Test.setCurrentPage(pageRef);
        System.debug('sad'+newR.Name);
        ApexPages.currentPage().getParameters().put('orderNo',newR.Name);
         ApexPages.currentPage().getParameters().put('encResp','test');
        PaymentResponseController response = NEW PaymentResponseController ();
        response.generateReciept();
        Test.stopTest();
    }
    static testMethod void PaymentResponseControllerMethodNew() {
        Receipt__c R = new Receipt__c();
        //R.Customer__c=objAccount.id;
        R.Amount__c=100; 
        R.Receipt_Type__c='Card';
        R.Payment_Response__c ='test';
        R.Transaction_Date__c=system.now();           
        
        Id rt=Schema.SObjectType.Receipt__c.getRecordTypeInfosByName().get('Card').getRecordTypeId();
        R.RecordTypeId=rt;
        insert R;
        
        Test.starttest();
        Receipt__c newR =[select id,name,Payment_Gateway_Ref_No__c,Amount__c,Card_Amount__c from Receipt__c where Receipt_Type__c= 'Card' limit 1];
        
        PageReference pageRef = Page.CC_ThankYouPage;
        Test.setCurrentPage(pageRef);
        System.debug('sad'+newR.Name);
        ApexPages.currentPage().getParameters().put('receiptNo',newR.Name);
         //ApexPages.currentPage().getParameters().put('encResp','test');
        PaymentResponseController response = NEW PaymentResponseController ();
        response.generateReciept();
        Test.stopTest();
    }

    static testMethod void PaymentResponseControllerMethod3() {
        Id perInqRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();

        Inquiry__c inqObj= new Inquiry__c(
            First_Name__c = 'Test', 
            Last_Name__c = 'Last', 
            Mobile_Phone_Encrypt__c = '456123', 
            Mobile_CountryCode__c = 'American Samoa: 001684', 
            Mobile_Phone__c = '1234',
            Email__c = 'mk@gmail.com',
            Agency_Type__c = 'Individual',
            RecordTypeId = perInqRT
        );
        insert inqObj;

        Travel_Details__c td = new Travel_Details__c();
        td.Inquiry__c = inqObj.Id;
        insert td;

        Id rt=Schema.SObjectType.Receipt__c.getRecordTypeInfosByName().get('Card').getRecordTypeId();
        Receipt__c R = new Receipt__c();
        R.Amount__c = 100; 
        R.Receipt_Type__c = 'Card';
        R.Payment_Response__c ='test';
        R.Transaction_Date__c = system.now();
        R.RecordTypeId = rt;
        R.Travel_Details__c = td.Id;
        insert R;

        Test.startTest();
        Receipt__c newR =[SELECT Id, Name FROM Receipt__c WHERE Receipt_Type__c = 'Card' LIMIT 1];
        
        PageReference pageRef = Page.CC_ThankYouPage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('orderNo', newR.Name);
         ApexPages.currentPage().getParameters().put('encResp', 'test');
        PaymentResponseController response = new PaymentResponseController();
        response.generateReciept();
        response.generateReciept();
        Test.stopTest();
    }
}