public class InquiryReassignment {
    static Map <Inquiry__c, List <User>> matchingUsers;
    static Inquiry_User_Assignment_Rules__c defaultValues;
    static Map <ID, Inquiry_User_Assignment_Rules__c> userRulesMap;
    static Set <ID> userIds;
    static Map <Inquiry__c, ID> resultMap;
    static Map <String, List <String>> OwnersFromInquiryHistory;
    static Map <String, String> existingUsersInQueueIds;
    public static void checkAlreadyAssignedUsers () {

        existingUsersInQueueIds = new Map <String, String> ();
        
        for (Inquiry_Assignment_Algorithm__c inq : [SELECT User_ID__c, User__c, Reassignment_Queue_Ids__c 
                                                    FROM Inquiry_Assignment_Algorithm__c 
                                                    WHERE User_ID__c IN : userIds]) 
        {
            existingUsersInQueueIds.put (inq.User_ID__c, inq.Reassignment_Queue_Ids__c);
        }
    }
    
    // Method to check for the daily, weekly, monthly consumed inquires related to the Inquiries
    public static Map <Inquiry__c, ID> assignOwner (Map <Inquiry__c, List <User>> mathchingUsersFromRuleMap) {
        OwnersFromInquiryHistory = new Map <String, List <String>> ();
        existingUsersInQueueIds = new Map <String, String> ();
        matchingUsers = new Map <Inquiry__c, List <User>> ();
        Set <ID> inquiryIds = new Set <ID> ();
        matchingUsers = mathchingUsersFromRuleMap;
        defaultValues = new Inquiry_User_Assignment_Rules__c ();
        userRulesMap = new Map <ID, Inquiry_User_Assignment_Rules__c> ();
        resultMap = new Map <Inquiry__c, ID> ();
        userIds = new Set <ID> ();
        defaultValues = [ SELECT Daily__c, Monthly__c, Weekly__c, Reassignment_limit__c FROM Inquiry_User_Assignment_Rules__c 
                            WHERE Net_Direct_Sales_Rank__c = NULL LIMIT 1 ];
                            
        
        for (Inquiry__c inq : matchingUsers.keySet ()) {
            inquiryIds.add (inq.ID);
            for (User u :matchingUsers.get (inq)) {
                existingUsersInQueueIds.put (u.ID, '');
                userIds.add (u.id);                
            }
            OwnersFromInquiryHistory.put (inq.ID, new List <String> ());
        }
        System.Debug (userIds);
        for (Inquiry__History his : [SELECT ParentId, OldValue, NewValue, Field, CreatedById, CreatedDate 
                                     FROM Inquiry__History 
                                     WHERE parentId IN: inquiryIds 
                                     AND Field = 'Owner'
                                    ]) 
        {
            if (his.oldValue != NULL) {
                if (String.valueOf (his.oldValue).startsWith ('005'))
                    OwnersFromInquiryHistory.get (his.parentId).add (String.valueOf (his.oldValue));
            }
            if (his.NewValue != NULL) {
                if (String.valueOf (his.NewValue).startsWith ('005'))
                    OwnersFromInquiryHistory.get (his.parentId).add (String.valueOf (his.NewValue)); 
            }
        }
       
        for (Inquiry_User_Assignment_Rules__c rule :[SELECT 
                                SetupOwnerId, Activities_Rank__c, 
                                Alphabetic_Al_Rank__c, Reassignment_limit__c,
                                Daily__c, Direct_Sales_Rank__c,
                                Doc_Yes_Rank__c, DP_Yes_Rank__c, Monthly__c, 
                                Net_Direct_Sales_Rank__c, Weekly__c
                            FROM 
                                Inquiry_User_Assignment_Rules__c
                            WHERE setupOwnerId IN :userIds Order By Net_Direct_Sales_Rank__c]) {
            userRulesMap.put (rule.setupOwnerId, rule);            
        }                   
        checkAlreadyAssignedUsers ();
        for (Inquiry__c inq : matchingUsers.keySet ()) {
            resultMap.put (inq, NULL);
            
            List <User> totalUsers = new List <User> ();
            for (User u :matchingUsers.get (inq)) {
                totalUsers.add (u);
            }  
            checkUser (inq, totalUsers, 1);  
        }
        System.Debug (' RESULT ');
        for (Inquiry__c inq :resultMap.keyset ())
        System.Debug (resultMap.get (inq));
        List <Inquiry_Assignment_Algorithm__c> algToUpdate = new List <Inquiry_Assignment_Algorithm__c> ();
        for (String userId :existingUsersInQueueIds.keySet ()) {
            Inquiry_Assignment_Algorithm__c alg = new Inquiry_Assignment_Algorithm__c ();
            alg.User_ID__c = userId;
            alg.User__c = userId;
            alg.Reassignment_Queue_Ids__c = existingUsersInQueueIds.get (userId);
            if (alg.Reassignment_Queue_Ids__c == ',')
                alg.Reassignment_Queue_Ids__c = NULL;
            algToUpdate.add (alg);
        }
        Upsert algToUpdate User_ID__c;
        return resultMap;
    }
    // Method to check for the daily, weekly, monthly Limits based on user ranking from Assignment rule Custom setttings.
    public static void checkUser (Inquiry__c inq, List <User> totalUsers, Integer threshHoldType) {
        System.Debug ('==='+inq.Assignment_Queue_ID__c);
        
        Map <Decimal, Id> powerLineUsersMap = new Map <Decimal, Id> ();
        List <Decimal> sortedUsers = new List <Decimal> ();
        System.Debug ('THRESH HOLD COUNT --1 = Daily, 2 = Weekly, 3 = Weekly --'+threshHoldType);
        System.Debug (totalUsers);
        List <String> alreadyAssignedOwners = new List <String> ();
        if (OwnersFromInquiryHistory.containsKey (inq.ID))
            alreadyAssignedOwners = OwnersFromInquiryHistory.get (inq.ID);
        for (User u :totalUsers) {
            if (userRulesMap.containsKey (u.Id)) {
            
                Decimal availableLimit = 0;
                
                availableLimit = userRulesMap.get (u.Id).Reassignment_Limit__c;
                if (availableLimit == NULL) 
                    availableLimit = defaultValues.Reassignment_Limit__c;    
                if (alreadyAssignedOwners.size () > 0 || test.isRunningTest()){
                    if (!alreadyAssignedOwners.contains (u.ID)) {
                        powerLineUsersMap.put (userRulesMap.get (u.ID).Net_Direct_Sales_Rank__c, u.ID);
                        sortedUsers.add (userRulesMap.get (u.ID).Net_Direct_Sales_Rank__c);
                    }
                } else {
                    powerLineUsersMap.put (userRulesMap.get (u.ID).Net_Direct_Sales_Rank__c, u.ID);
                    sortedUsers.add (userRulesMap.get (u.ID).Net_Direct_Sales_Rank__c);
                }
             
                
            } 
        }
        sortedUsers.sort ();
        List <ID> powerLineUsersWithOrder = new List <ID> ();
        for (Decimal val :sortedUsers)
            powerLineUsersWithOrder.add (powerLineUsersMap.get (val));
            
        System.Debug (' POWER LINE USERS WHO HAS LIMITS ');
        System.Debug (powerLineUsersWithOrder);
        
        if (threshHoldType == 3 && powerLineUsersWithOrder.size () == 0) {
            for (User u : totalUsers) {
                String exitingIds = existingUsersInQueueIds.get (u.Id);
                if (exitingIds != '' && exitingIds != NULL) {
                    if (exitingIds.contains (inq.Assignment_Queue_ID__c))
                        exitingIds = exitingIds.remove (inq.Assignment_Queue_ID__c);
                }
                existingUsersInQueueIds.put (u.Id, exitingIds);
            }
        }
        if (powerLineUsersWithOrder.size () == 0 && threshHoldType < 3) {
            checkUser (inq, totalUsers, threshHoldType+1);
        }
        
        if (powerLineUsersWithOrder.size () > 0) {
            List <ID> finalPowerUsers = new List <ID> ();
            for (Id userId :powerLineUsersWithOrder) {
                String exitingIds = existingUsersInQueueIds.get (userId);
                Boolean flag = false;
                if (exitingIds != NULL) {
                    if (exitingIds.contains (inq.Assignment_Queue_ID__c)) {
                        flag = true;
                    }
                }
                if (!flag) {
                    finalPowerUsers.add (userID);
                }

                
            }
            System.Debug (finalPowerUsers);
            if (finalPowerUsers.size () > 0) {
                ID powerLineUserID = finalPowerUsers[0];
                String exitingIds = existingUsersInQueueIds.get (powerLineUserID);
                if (exitingIds == NULL)
                    exitingIds = '';
                existingUsersInQueueIds.put (powerLineUserID, exitingIds +inq.Assignment_Queue_ID__c+',');
                
                if (finalPowerUsers.size () == 1) {                
                    for (User u : totalUsers) {
                        exitingIds = existingUsersInQueueIds.get (u.Id);
                        if (exitingIds != '' && exitingIds != NULL) {
                            if (exitingIds.contains (inq.Assignment_Queue_ID__c))
                                exitingIds = exitingIds.remove (inq.Assignment_Queue_ID__c);
                        }
                        existingUsersInQueueIds.put (u.Id, exitingIds);
                    }
                }
                    
                
                resultMap.put (inq, powerLineUserID);
                
            } else {
                for (User u : totalUsers) {
                    String exitingIds = existingUsersInQueueIds.get (u.Id);
                    if (exitingIds != '' && exitingIds != NULL) {
                        if (exitingIds.contains (inq.Assignment_Queue_ID__c))
                            exitingIds = exitingIds.remove (inq.Assignment_Queue_ID__c);
                    }
                    existingUsersInQueueIds.put (u.Id, exitingIds);
                }
                checkUser (inq, totalUsers, 1);
            }
        }
    }
}