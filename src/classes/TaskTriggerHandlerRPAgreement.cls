public without sharing class TaskTriggerHandlerRPAgreement {
    public void checkRPTaskAssignedUser(list<Task> listTask) {
        Integer counterVal;
        set<Id> setTaskCaseId = new set<Id>();
        set<Id> setTaskForCallout = new set<Id>();
        set<String> setTaskForUser = new set<String>();

        if(Label.UsersOutsideSalesforce != null) {
            setTaskForUser.addAll(Label.UsersOutsideSalesforce.split(','));
            system.debug('setTaskForUser==='+setTaskForUser);
            counterVal = 1;
            for(Task taskInst: listTask) {
                if(taskInst.Process_Name__c != null && taskInst.Assigned_User__c != null
                    && taskInst.WhatId != null && taskInst.Process_Name__c.startsWithIgnoreCase('Rental Pool Agreement')
                    && setTaskForUser.contains(taskInst.Assigned_User__c) && String.valueOf(taskInst.WhatId).startsWith('500')) {
                     if(counterVal <= 100) {
                        setTaskForCallout.add(taskInst.Id);
                     }
                     else {
                        createTaskOnIPMS(setTaskForCallout, setTaskCaseId);
                        setTaskForCallout = new set<Id>();
                        setTaskForCallout.add(taskInst.Id);
                     }
                     setTaskCaseId.add(taskInst.WhatId);
                     counterVal++;
                }
            }
            if(!setTaskForCallout.isEmpty() && !setTaskCaseId.isEmpty()) {
                createTaskOnIPMS(setTaskForCallout, setTaskCaseId);
            }
        }
    }
    
    public void checkRPTaskCompletion(list<Task> listTask, map<Id, Task> mapOldTask) {
         set<Id> setCaseIdPOA = new set<Id>();
         set<String> setCasePOA = new set<String>();
         set<String> setTaskForUser = new set<String>();
         //get the assigned user for IPMS 
         if(Label.UsersOutsideSalesforce != null) {
            setTaskForUser.addAll(Label.UsersOutsideSalesforce.split(','));
            system.debug('setTaskForUser==='+setTaskForUser);
         }
         for(Task taskInst: listTask) {
            if(taskInst.WhatId != null && String.valueOf(taskInst.WhatId).startsWith('500')
                && taskInst.Status.equalsIgnoreCase('Completed') && taskInst.Status != mapOldTask.get(taskInst.Id).Status
                && taskInst.Process_Name__c != null && taskInst.Process_Name__c.startsWithIgnoreCase('Rental Pool Agreement') 
                && taskInst.Subject != null) {

                if(taskInst.Subject.equalsIgnoreCase('POA Document Verification Pending')) {
                    setCaseIdPOA.add(taskInst.WhatId);
                    setCasePOA.add(taskInst.WhatId+'-'+taskInst.Subject.removeEnd(' Verification Pending'));
                }
            }
            if(!setCaseIdPOA.isEmpty() && !setCasePOA.isEmpty()) {
                updateCaseAttachment(setCaseIdPOA, setCasePOA, 'POA');
            }
         }
     }

     public void updateCaseAttachment(set<Id> setCaseId, set<String> setCaseProcess, String processName) {
        list<SR_Attachments__c> listUpdateCaseAttach = new list<SR_Attachments__c>(); 
        for(SR_Attachments__c caseAttachInst: [select Id, Name, Case__c, isValid__c, Attachment_URL__c 
                                                from SR_Attachments__c 
                                                where Case__c IN: setCaseId]) {
            if(setCaseProcess.contains(caseAttachInst.Case__c+'-'+caseAttachInst.Name)) {
                if(processName.equalsIgnoreCase('POA')) {
                    caseAttachInst.isValid__c = true;
                    listUpdateCaseAttach.add(caseAttachInst);
                }
            }
        }
        if(!listUpdateCaseAttach.isEmpty()) {
            update listUpdateCaseAttach;
        }
        system.debug('listUpdateCaseAttach==='+listUpdateCaseAttach);
     }

    @future(callout=true)
    public static void createTaskOnIPMS(set<Id> setTaskForCallout, set<Id> setTaskCaseId) {
        list<Error_Log__c> listErrorLog = new list<Error_Log__c>();
        map<String, Object> mapDeserializeTask = new map<String, Object>();
        system.debug('Rental Pool Agreement Task Creation********************');
        map<Id, Case> mapCaseDetails = new map<Id, Case>([select Id, Status, Booking_Unit__r.Registration_ID__c, 
                            CaseNumber, Booking_Unit__r.Unit_Details__c, Booking_Unit__r.Rental_Pool__c, 
                            Purpose_of_POA__c, Booking_Unit__r.Property_City__c,
                            Booking_Unit__r.Inventory__r.Property_Status__c, Booking_Unit__r.Property_Country__c,
                            Booking_Unit__r.Inventory__r.Property__r.DIFC__c, Account.Party_ID__c,
                            Account.FirstName, Account.LastName, Account.Nationality__pc,
                            AccountId, Booking_Unit__c, Selected_Rental_Pool_Offer__c, Document_Version__c
                            from Case where Id IN: setTaskCaseId]);

        list<Task> lsTaskToUpdate = new list<Task>();
        for(Task objTask : [Select t.WhoId
                                 , t.WhatId
                                 , t.Type
                                 , t.Status
                                 , t.OwnerId
                                 , t.Id
                                 , t.IPMS_Role__c
                                 , t.Subject
                                 , t.CreatedDate
                                 , t.Description
                                 , t.Assigned_User__c
                                 , t.ActivityDate
                                 , t.Owner.Name
                                 , t.Task_Error_Details__c
                                 , t.Pushed_to_IPMS__c
                                 , t.Document_URL__c
                                 , t.Process_Name__c
                                 From Task t
                                 where t.Id IN : setTaskForCallout]) {
            list<TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5> listObjBeans = 
                new list<TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5>();
            TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objHeaderBean = 
                new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
            String reqNo = '2-'+string.valueOf(System.currentTimeMillis());
            objHeaderBean.PARAM_ID = '2-'+mapCaseDetails.get(objTask.WhatId).CaseNumber;
            system.debug('param id*****'+objHeaderBean.PARAM_ID);
            objHeaderBean.ATTRIBUTE1 = 'HEADER';
            //objHeaderBean.ATTRIBUTE2 = 'Rental Pool Agreement';
            objHeaderBean.ATTRIBUTE2 = objTask.Process_Name__c;
            if(objTask.Status != 'Cancelled'){
                objHeaderBean.ATTRIBUTE3 = 'Open';
            }else{
                objHeaderBean.ATTRIBUTE3 = 'Closed';
            }
            system.debug('objTask.Status*****'+objTask.Status);
            objHeaderBean.ATTRIBUTE4 = objTask.Owner.Name;
            objHeaderBean.ATTRIBUTE5 = mapCaseDetails.get(objTask.WhatId).Account.Party_ID__c;
            system.debug('party*****'+mapCaseDetails.get(objTask.WhatId).Account.Party_ID__c);
            objHeaderBean.ATTRIBUTE6 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Registration_ID__c;
            system.debug('regid*****'+mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Registration_ID__c);
            objHeaderBean.ATTRIBUTE7 = String.valueOf(objTask.CreatedDate.format('dd-MMM-yyyy').toUpperCase()); // format this as DD-MON- YYYY
            system.debug('date*****'+String.valueOf(objTask.CreatedDate.format('dd-MMM-yyyy').toUpperCase()));
            objHeaderBean.ATTRIBUTE8 = '';
            objHeaderBean.ATTRIBUTE9 = '';
            objHeaderBean.ATTRIBUTE10 = '';
            objHeaderBean.ATTRIBUTE11 = objTask.WhatId;
            objHeaderBean.ATTRIBUTE12 = '';
            objHeaderBean.ATTRIBUTE13 = '';
            objHeaderBean.ATTRIBUTE14 = '';
            objHeaderBean.ATTRIBUTE15 = '';
            objHeaderBean.ATTRIBUTE16 = '';
            objHeaderBean.ATTRIBUTE17 = '';
            objHeaderBean.ATTRIBUTE18 = '';
            objHeaderBean.ATTRIBUTE19 = '';
            objHeaderBean.ATTRIBUTE20 = '';
            objHeaderBean.ATTRIBUTE21 = '';
            objHeaderBean.ATTRIBUTE22 = '';
            objHeaderBean.ATTRIBUTE23 = '';
            objHeaderBean.ATTRIBUTE24 = '';
            objHeaderBean.ATTRIBUTE25 = '';
            objHeaderBean.ATTRIBUTE26 = '';
            objHeaderBean.ATTRIBUTE27 = '';
            objHeaderBean.ATTRIBUTE28 = '';
            objHeaderBean.ATTRIBUTE29 = '';
            objHeaderBean.ATTRIBUTE30 = '';
            objHeaderBean.ATTRIBUTE31 = '';
            objHeaderBean.ATTRIBUTE32 = '';
            objHeaderBean.ATTRIBUTE33 = '';
            objHeaderBean.ATTRIBUTE34 = '';
            objHeaderBean.ATTRIBUTE35 = '';
            objHeaderBean.ATTRIBUTE36 = '';
            objHeaderBean.ATTRIBUTE37 = '';
            objHeaderBean.ATTRIBUTE38 = '';
            objHeaderBean.ATTRIBUTE39 = '';
            //objHeaderBean.ATTRIBUTE40 = '';
            objHeaderBean.ATTRIBUTE41 = '';
            objHeaderBean.ATTRIBUTE42 = '';
            objHeaderBean.ATTRIBUTE43 = '';
            objHeaderBean.ATTRIBUTE44 = '';
            objHeaderBean.ATTRIBUTE45 = '';
            objHeaderBean.ATTRIBUTE46 = '';
            objHeaderBean.ATTRIBUTE47 = '';
            objHeaderBean.ATTRIBUTE48 = '';
            objHeaderBean.ATTRIBUTE49 = '';
            objHeaderBean.ATTRIBUTE50 = '';
            listObjBeans.add(objHeaderBean);

            // create TASK bean
            TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objTaskBean = new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
            objTaskBean.PARAM_ID = '2-'+mapCaseDetails.get(objTask.WhatId).CaseNumber;
            objTaskBean.ATTRIBUTE1 = 'TASK';
            objTaskBean.ATTRIBUTE2 = objTask.Subject;
            system.debug('subject*****'+objTask.Subject);
            if(objTask.Status != 'Cancelled'){
                objTaskBean.ATTRIBUTE3 = 'Open';
            }else{
                objTaskBean.ATTRIBUTE3 = 'Closed';
            }
            //objTaskBean.ATTRIBUTE3 = objTask.Status;
            system.debug('status*****'+objTask.Status);
            objTaskBean.ATTRIBUTE4 = objTask.IPMS_Role__c;
            objTaskBean.ATTRIBUTE5 = '';
            objTaskBean.ATTRIBUTE6 = '';
            objTaskBean.ATTRIBUTE7 = String.valueOf(objTask.CreatedDate.format('dd-MMM-yyyy').toUpperCase()); // format this as DD-MON- YYYY
            system.debug('created date*****'+String.valueOf(objTask.CreatedDate.format('dd-MMM-yyyy').toUpperCase()));
            objTaskBean.ATTRIBUTE8 = objTask.Id;
            system.debug('task id*****'+objTask.Id);
            Datetime dt = objTask.ActivityDate;
            objTaskBean.ATTRIBUTE9 = String.valueOf(dt.format('dd-MMM-yyyy').toUpperCase());
            system.debug('due date*****'+String.valueOf(dt.format('dd-MMM-yyyy').toUpperCase()));
            objTaskBean.ATTRIBUTE10 = '';
            objTaskBean.ATTRIBUTE11 = objTask.WhatId;
            objTaskBean.ATTRIBUTE12 = objTask.Subject;
            objTaskBean.ATTRIBUTE13 = objTask.Status;
            objTaskBean.ATTRIBUTE14 = objTask.Type;
            objTaskBean.ATTRIBUTE15 = objTask.Description;
            objTaskBean.ATTRIBUTE16 = objTask.Assigned_User__c;
            objTaskBean.ATTRIBUTE17 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Unit_Details__c;
            objTaskBean.ATTRIBUTE18 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Property_City__c;
            objTaskBean.ATTRIBUTE19 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Inventory__r.Property_Status__c;
            objTaskBean.ATTRIBUTE20 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Property_Country__c;
            objTaskBean.ATTRIBUTE21 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Inventory__r.Property__r.DIFC__c == true ? 'Yes' : 'No';
            objTaskBean.ATTRIBUTE22 = objTask.Document_URL__c;
            objTaskBean.ATTRIBUTE23 = mapCaseDetails.get(objTask.WhatId).Purpose_of_POA__c;
            objTaskBean.ATTRIBUTE24 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Rental_Pool__c ? 'Yes' : 'No';
            if(objTask.Subject == Label.TaskVerifyAgreementDocument || objTask.Subject == Label.TaskHospitalityInspection) {
                objTaskBean.ATTRIBUTE25 = mapCaseDetails.get(objTask.WhatId).Selected_Rental_Pool_Offer__c != null ? mapCaseDetails.get(objTask.WhatId).Selected_Rental_Pool_Offer__c : '';
                objTaskBean.ATTRIBUTE26 = mapCaseDetails.get(objTask.WhatId).Document_Version__c != null ? mapCaseDetails.get(objTask.WhatId).Document_Version__c : '';
            }
            else {
                objTaskBean.ATTRIBUTE25 = '';
                objTaskBean.ATTRIBUTE26 = '';
            }
            system.debug('ATTRIBUTE25==='+objTaskBean.ATTRIBUTE25);
            system.debug('ATTRIBUTE26==='+objTaskBean.ATTRIBUTE26);
            objTaskBean.ATTRIBUTE27 = '';
            objTaskBean.ATTRIBUTE28 = '';
            objTaskBean.ATTRIBUTE29 = '';
            objTaskBean.ATTRIBUTE30 = '';
            objTaskBean.ATTRIBUTE31 = '';
            objTaskBean.ATTRIBUTE32 = '';
            objTaskBean.ATTRIBUTE33 = '';
            objTaskBean.ATTRIBUTE34 = '';
            objTaskBean.ATTRIBUTE35 = '';
            objTaskBean.ATTRIBUTE36 = '';
            objTaskBean.ATTRIBUTE37 = '';
            objTaskBean.ATTRIBUTE38 = '';
            objTaskBean.ATTRIBUTE39 = '';
            objTaskBean.ATTRIBUTE41 = '';
            objTaskBean.ATTRIBUTE42 = '';
            objTaskBean.ATTRIBUTE43 = '';
            objTaskBean.ATTRIBUTE44 = '';
            objTaskBean.ATTRIBUTE45 = '';
            objTaskBean.ATTRIBUTE46 = '';
            objTaskBean.ATTRIBUTE47 = '';
            objTaskBean.ATTRIBUTE48 = '';
            objTaskBean.ATTRIBUTE49 = '';
            objTaskBean.ATTRIBUTE50 = '';
            listObjBeans.add(objTaskBean);

            // create UNIT bean
            TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objUnitBean = new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
            objUnitBean.PARAM_ID = '2-'+mapCaseDetails.get(objTask.WhatId).CaseNumber;
            objUnitBean.ATTRIBUTE1 = 'UNITS';
            //objUnitBean.ATTRIBUTE2 = 'Rental Pool Agreement';
            objUnitBean.ATTRIBUTE2 = objTask.Process_Name__c;
            objUnitBean.ATTRIBUTE3 = '';
            objUnitBean.ATTRIBUTE4 = '';
            objUnitBean.ATTRIBUTE5 = '';
            objUnitBean.ATTRIBUTE6 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Registration_ID__c;
            system.debug('reg*****'+mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Registration_ID__c);
            objUnitBean.ATTRIBUTE7 = '';
            objUnitBean.ATTRIBUTE8 = '';
            objUnitBean.ATTRIBUTE9 = '';
            objUnitBean.ATTRIBUTE10 = '';
            objUnitBean.ATTRIBUTE11 = '';
            objUnitBean.ATTRIBUTE12 = '';
            objUnitBean.ATTRIBUTE13 = '';
            objUnitBean.ATTRIBUTE14 = '';
            objUnitBean.ATTRIBUTE15 = '';
            objUnitBean.ATTRIBUTE16 = '';
            objUnitBean.ATTRIBUTE17 = '';
            objUnitBean.ATTRIBUTE18 = '';
            objUnitBean.ATTRIBUTE19 = '';
            objUnitBean.ATTRIBUTE20 = '';
            objUnitBean.ATTRIBUTE21 = '';
            objUnitBean.ATTRIBUTE22 = '';
            objUnitBean.ATTRIBUTE23 = '';
            objUnitBean.ATTRIBUTE24 = '';
            objUnitBean.ATTRIBUTE25 = '';
            objUnitBean.ATTRIBUTE26 = '';
            objUnitBean.ATTRIBUTE27 = '';
            objUnitBean.ATTRIBUTE28 = '';
            objUnitBean.ATTRIBUTE29 = '';
            objUnitBean.ATTRIBUTE30 = '';
            objUnitBean.ATTRIBUTE31 = '';
            objUnitBean.ATTRIBUTE32 = '';
            objUnitBean.ATTRIBUTE33 = '';
            objUnitBean.ATTRIBUTE34 = '';
            objUnitBean.ATTRIBUTE35 = '';
            objUnitBean.ATTRIBUTE36 = '';
            objUnitBean.ATTRIBUTE37 = '';
            objUnitBean.ATTRIBUTE38 = '';
            objUnitBean.ATTRIBUTE39 = '';
            objUnitBean.ATTRIBUTE41 = '';
            objUnitBean.ATTRIBUTE42 = '';
            objUnitBean.ATTRIBUTE43 = '';
            objUnitBean.ATTRIBUTE44 = '';
            objUnitBean.ATTRIBUTE45 = '';
            objUnitBean.ATTRIBUTE46 = '';
            objUnitBean.ATTRIBUTE47 = '';
            objUnitBean.ATTRIBUTE48 = '';
            objUnitBean.ATTRIBUTE49 = '';
            objUnitBean.ATTRIBUTE50 = '';
            listObjBeans.add(objUnitBean);
            
            TaskCreationWSDL.TaskHttpSoap11Endpoint objClass = new TaskCreationWSDL.TaskHttpSoap11Endpoint();
            objClass.timeout_x = 120000;
            String response = objClass.SRDataToIPMSMultiple(reqNo, 'CREATE_SR', 'SFDC', listObjBeans);
            system.debug('resp*****'+response);
            if(String.isNotBlank(response)) {
				Task taskObj = new Task();
                innerClass IC = (innerClass)JSON.deserialize(response, innerClass.class);
                system.debug('IC*****'+IC);
				taskObj.Id = objTask.Id;
                if(IC.status.EqualsIgnoreCase('S')){
                    taskObj.Pushed_to_IPMS__c = true;
                }else{
                    taskObj.Pushed_to_IPMS__c = false;
                    taskObj.Update_IPMS__c = false;
                    taskObj.Task_Error_Details__c = IC.status;
                    system.debug('objTask=='+objTask);
					system.debug('taskObj=='+taskObj);
                    Error_Log__c objErr = createErrorLogRecord(mapCaseDetails.get(objTask.WhatId).AccountId, mapCaseDetails.get(objTask.WhatId).Booking_Unit__c, objTask.WhatId);
                    objErr.Error_Details__c = objTask.Subject+' Failed with '+IC.message;
                    listErrorLog.add(objErr);
                }
				lsTaskToUpdate.add(taskObj);
            }
            else {
                Error_Log__c objErr = createErrorLogRecord(mapCaseDetails.get(objTask.WhatId).AccountId, mapCaseDetails.get(objTask.WhatId).Booking_Unit__c, objTask.WhatId);
                objErr.Error_Details__c = objTask.Subject+' Failed with Error : No Response from IPMS for Task Creation';
                listErrorLog.add(objErr);
            }

            
        }
        if(!lsTaskToUpdate.isEmpty()){
            update lsTaskToUpdate;
        }
        if(!listErrorLog.isEmpty()) {
            insertErrorLog(listErrorLog);
        }
    }

    public class innerClass{
        public string message;
        public string status;
        
        public innerClass(){
            
        }
    }

    public static void insertErrorLog(list<Error_Log__c> listObjErr){
        try{
            insert listObjErr;
        }catch(Exception ex){
            //errorMessage = 'Error : '+ ex.getMessage();
            system.debug('Error Log ex*****'+ex);
        }
    }

    public static Error_Log__c createErrorLogRecord(Id accId, Id bookingUnitId, Id caseId){
        Error_Log__c objErr = new Error_Log__c();
        objErr.Account__c = accId;
        objErr.Booking_Unit__c = bookingUnitId;
        objErr.Case__c = caseId;
        return objErr;
    }
}