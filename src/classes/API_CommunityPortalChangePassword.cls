/*
Class Name : API_CommunityChangePassword
Description : To change the community user password
Sample URL : /services/apexrest/changepassword?username=******@*****.com&newpassword=*****
Method     : POST   
Test Class : API_CommunityPortalTest              
    
*/

@RestResource(urlMapping='/changepasswordPortal/*')
global without sharing class API_CommunityPortalChangePassword{ 

    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        //200 => 'OK',
        //400 => 'Bad Request',
        //401 => 'Unauthorized',
        //500 => 'Internal Server Error' 
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Technical error'
    };
    
    
    @HttpPost
    global static FinalReturnWrapper changePassword() {
        LoginResponse objResponse = new LoginResponse();
        FinalReturnWrapper retunResponse = new FinalReturnWrapper();
        
        //Error handling in the beginning
        //if( !RestContext.request.params.containsKey('username') ) {
        //    objResponse.isSuccess = false;
        //    objResponse.sessionId = '';
        //    objResponse.statusMessage = 'Missing parameter : username';
        //    objResponse.statusCode = 3;
        //    return returnObjWrapper(objResponse);
        //}
        //For accountId as input param:
        if( !RestContext.request.params.containsKey('accountId') ) {
            objResponse.isSuccess = false;
            objResponse.sessionId = '';
            objResponse.statusMessage = 'Missing parameter : accountId';
            objResponse.statusCode = 3;
            return returnObjWrapper(objResponse);
        }
        //For accountId as input param: END

        else if(!RestContext.request.params.containsKey('newpassword')) {
            objResponse.isSuccess = false;
            objResponse.sessionId = '';
            objResponse.statusMessage = 'Missing parameter : newpassword';
            objResponse.statusCode = 3;
            return returnObjWrapper(objResponse);
        }
        //else if(RestContext.request.params.containsKey('username') && String.isBlank(RestContext.request.params.get('username'))) {
        //    objResponse.isSuccess = false;
        //    objResponse.sessionId = '';
        //    objResponse.statusMessage = 'Missing value in parameter : username';
        //    objResponse.statusCode = 3;
        //    return returnObjWrapper(objResponse);
        //}
        //For accountId as input param :
         else if(RestContext.request.params.containsKey('accountId') && String.isBlank(RestContext.request.params.get('accountId'))) {
            objResponse.isSuccess = false;
            objResponse.sessionId = '';
            objResponse.statusMessage = 'Missing value in parameter : accountId';
            objResponse.statusCode = 3;
            return returnObjWrapper(objResponse);
        }
        //For accountId as input param : END

        else if(RestContext.request.params.containsKey('newpassword') && String.isBlank(RestContext.request.params.get('newpassword'))) {
            objResponse.isSuccess = false;
            objResponse.sessionId = '';
            objResponse.statusMessage = 'Missing value in parameter : newpassword';
            objResponse.statusCode = 3;
            return returnObjWrapper(objResponse);
        }

        else if(RestContext.request.params.containsKey('newpassword') && String.isNotBlank(RestContext.request.params.get('newpassword')) && (RestContext.request.params.get('newpassword').contains('&') || RestContext.request.params.get('newpassword').contains('<') ) ) {
            objResponse.isSuccess = false;
            objResponse.sessionId = '';
            objResponse.statusMessage = 'Password should not contain characters - \'&\' and \'<\'';
            objResponse.statusCode = 3;
            return returnObjWrapper(objResponse);
        }

        //System.debug('username1: '+RestContext.request.params.get('username')+'*');
        //System.debug('password1: '+RestContext.request.params.get('newpassword')+'*');
        //For accountId as input param :
        System.debug('accountId: '+RestContext.request.params.get('accountId')+'*');

        //String username = RestContext.request.params.get('username').trim();
        String password = RestContext.request.params.get('newpassword').trim();
        //For accountId as input param :
        String accountId = RestContext.request.params.get('accountId').trim();

        //System.debug('username: '+username+'*');
        System.debug('password: '+password+'*');
        //For accountId as input param :
        System.debug('accountId: '+accountId+'*');


        User u = new User ();
        try {
            // To check whether requested user is community user or not
            //u = [SELECT ID FROM User WHERE userName =: userName AND Profile.Name LIKE '%COMMUNITY%' LIMIT 1];
            //if(username.contains('@')) {
            //    u = [SELECT Profile.Name, AccountId, ContactId,Account.Name,Contact.Name FROM User WHERE userName =: userName AND Profile.Name LIKE '%COMMUNITY%' LIMIT 1];
            //}
            //else{
            //    u = [SELECT UserName, Profile.Name, AccountId, ContactId,Account.Name,Contact.Name FROM User WHERE IsActive = TRUE
            //                      AND Contact.Account.Party_ID__c =: username AND Profile.Name LIKE '%COMMUNITY%' LIMIT 1];
            //    username = u.UserName;                  
            //}

            //For accountId as input param :
            if(String.isNotBlank(accountId)) {
                u = [SELECT id, Profile.Name, AccountId, ContactId,Account.Name,Contact.Name FROM User WHERE Contact.AccountId =: accountId AND Profile.Name LIKE '%COMMUNITY%' LIMIT 1];
                if(u.Id != userInfo.getUserId()){
                    objResponse.isSuccess = false;
                    objResponse.sessionId = '';
                    objResponse.statusMessage = 'Please provide valid accountid';
                    objResponse.statusCode = 3;
                    return returnObjWrapper(objResponse);
                }
            }    
            //For accountId as input param : END

            //System.debug('username 2: ' + username);
            System.debug('u: ' + u);
            
            try {
                // To set the new password for the user
                if (!Test.isRunningTest())
                    System.setPassword (u.Id, password);
                    
                objResponse.isSuccess = true;
                objResponse.sessionId = '';
                objResponse.statusMessage = 'Password changed successfully.';
                objResponse.statusCode = 1;
            } catch (Exception ex) {
                objResponse.isSuccess = false;
                objResponse.sessionId = '';
                objResponse.statusMessage = ex.getMessage ();
                objResponse.statusCode = 3;
            }
            
        } catch (Exception e) {
            objResponse.isSuccess = false;
            objResponse.sessionId = '';
            objResponse.statusMessage = 'No user found for given accountId';
            objResponse.statusCode = 4;
        }

        retunResponse = returnObjWrapper(objResponse);
        System.debug('retunResponse-'+retunResponse);

        //return objResponse;
        return retunResponse;        
        
    }

    public static FinalReturnWrapper returnObjWrapper(LoginResponse objResponse) {
        //Wrapper to return
        cls_data objData = new cls_data();
        objData.session_id = objResponse.sessionId;

        cls_meta_data objMeta = new cls_meta_data();
        //objMeta.is_success = objResponse.isSuccess;
        //objMeta.message = mapStatusCode.get(objResponse.statusCode);
        objMeta.message = objResponse.statusMessage;
        objMeta.status_code = objResponse.statusCode;
        //objMeta.title = objResponse.statusMessage;
        objMeta.title = mapStatusCode.get(objResponse.statusCode);
        objMeta.developer_message = null;

        FinalReturnWrapper objFinal = new FinalReturnWrapper();
        //objFinal.data = objData;
        objFinal.meta_data = objMeta;

        return objFinal;
    }
    
    // To send the response back to API 
    global class LoginResponse {
        public String sessionId;
        public Boolean isSuccess;
        public String statusMessage;
        public Integer statusCode;
    }

    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_data {
        public String session_id;
    }

    public class cls_meta_data {
        //public Boolean is_success;
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message;   
    }
}