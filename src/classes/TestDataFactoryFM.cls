@isTest
public class TestDataFactoryFM {
    public static Account createAccount(){
        Id personAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account acctIns=new Account(RecordTypeID = personAccountId,PersonEmail='a@g.com',FirstName = 'Test FName',
                                    LastName = 'Test LName',Passport_Number__c='123456789',Mobile__c='1212121212',Address_Line_1__c='test');
        return acctIns;
    }

    public static NSIBPM__Service_Request__c createServiceRequest(Account acctIns){
        Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = acctIns.id;
        sr.RecordTypeId = RecType;
        return sr;
    }

    public static Booking__c createBooking(Account acctIns,NSIBPM__Service_Request__c sr){
        Booking__c  bk = new  Booking__c();
        bk.Account__c = acctIns.id;
        bk.Deal_SR__c = sr.id;
        return bk;
    }

    public static Booking_unit__c createBookingUnit(Account acctIns,Booking__c bk){
        Booking_Unit__c buIns=new Booking_unit__c(
            Owner__c=acctIns.id,
            Resident__c=acctIns.id,
            Booking__c=bk.id,
            Unit_Name__c='JNU/SD168/XH2910B',
            Registration_Id__c='74655',
            Property_Name__c='JANUSIA@AKOYA OXYGEN',
            Property_City__c='All',
            Registration_Status__c = 'Active',
            Handover_Flag__c = 'Y',
            Early_Handover__c = true
        );
        return buIns;
    }

    public static List<Booking_Unit_Active_Status__c> insertActiveUnitStatuses() {
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>{
            new Booking_Unit_Active_Status__c(Status_Value__c='Active', Name='Active')
        };
        insert lstActiveStatus;
        return lstActiveStatus;
    }

    public static User createUser(Id profileId){
        User u= new User(alias = 'test123', email='test123@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = profileId, country='United States',IsActive =true,
                    timezonesidkey='America/Los_Angeles', username='testermee@noemail.com');
        return u;
    }

    public static Location__c createLocation(){
        Location__c locObj = new Location__c();
        locObj.Name  = 'JNU';
        locObj.Location_ID__c = '83488';
        locObj.As_Built_Drawing_Fee__c = 100;
        locObj.Security_Deposit_Cheque_Amount__c = 100;
        locObj.Garbage_disposal_Amount__c = 100;
        locObj.Temporary_power_water_connection_Amount__c = 100;
        locObj.Short_Duration_Gate_Pass_Fee__c = 100;
        locObj.Long_Duration_Gate_Pass_Fee__c = 200;
        return locObj;
    }

    public static Inventory__c createInventory(Location__c locObj){
        Inventory__c invObj=new Inventory__c();
        invObj.Property_City__c='All';
        invObj.Building_Location__c=locObj.id;
        return invObj;
    }

    public static FM_User__c createFMUser(User u,Location__c locObj){
        FM_User__c fmUser=new FM_User__c();
        fmUser.FM_User__c=u.id;
        fmUser.FM_Role__c='FM Admin';
        fmUser.Building__c=locObj.id;
        return fmUser;
    }

    public static list<FM_Case__c> createFMCase(Integer cnt){
        list<FM_Case__c> lstC = new list<FM_Case__c>();
        for(Integer i = 0; i < cnt; i++){
            FM_Case__c objC = new FM_Case__c();
            lstC.add(objC);
        }
        return lstC;
    }

    public static FM_Case__c createFMCaseAcc(Id AccountId,Id RecordTypeIdCase){
        //FM_Case__c lstC = new FM_Case__c();
        //for(Integer i = 0; i < cnt; i++){
        FM_Case__c objC = new FM_Case__c();
        objC.Account__c = AccountId;
        objC.RecordTypeId = RecordTypeIdCase;
        //lstC.add(objC);
        //}
        return objC;
    }
	public static Property__c createProperty(){
        Property__c propObj = new Property__c();
        propObj.Property_ID__c = 30;
        propObj.Property_Name__c = 'Park Towers';
        return propObj;
    }

    public static list<SR_Booking_Unit__c> createSRBookingUnits(Id BookingUnitId, Id FmCaseId, Integer cnt){
        list<SR_Booking_Unit__c> lstSRBU = new list<SR_Booking_Unit__c>();
        for(Integer i=0; i<cnt; i++){
            SR_Booking_Unit__c objBU = new SR_Booking_Unit__c();
            objBU.Booking_Unit__c = BookingUnitId;
            objBU.FM_Case__c = FmCaseId;
            lstSRBU.add(objBU);
        }
        return lstSRBU;
    }
}