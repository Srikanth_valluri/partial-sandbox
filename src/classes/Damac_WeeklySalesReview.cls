/************************************************************************************************
 * @Name              : Damac_WeeklySalesReview
 * @Test Class Name   : Damac_WeeklySalesReview_Test
 * @Description       : Controller Class for  Damac_WeeklySalesReview VF Page 
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log
 * 1.0         QBurst         15/05/2020       Created
***********************************************************************************************/
Public without sharing class  Damac_WeeklySalesReview {
    public boolean hosMode {get; set;}
    public boolean dosMode {get; set;}
    public boolean hodMode {get; set;}
    public boolean extUserMode {get; set;}
    public Date wsrDate {get; set;}
    public String wsrStatus {get; set;}
    public boolean reviewExists {get; set;}  
    public String dosId;
    public String hosId;
    public String hodId;
    public String errorMsg;
    public Integer rmSize {get; set;}
    public Integer colSize {get; set;}
    public List<User> rmList {get; set;}
    public boolean rmMissing {get; set;}
    public List<SelectOption> checklistOption {get; set;}
    public List<SelectOption> resultsOption {get; set;}
    public List<WeeklySalesReview> wsrRecordWrapper {get; set;}
    public List<WeeklySalesReview> wsrRecordWrapperRM {get; set;}
    public static List <String> topicList {get; set;}
    public boolean editMode {get; set;}
    public boolean isSubmit {get; set;}
    public boolean noAccess {get; set;}
    public Map<String, Boolean> meetingPresentMap {get; set;}
    public Map<Boolean, String> booleanMap {get; set;}
    public User dos {get; set;}
    public User hos {get; set;}
    public User hod {get; set;}
    public Boolean wrapperExists {get; set;}
    public Boolean rmWrapperExists {get; set;}
    public boolean isReviewer {get; set;}
    public String createdUser {get; set;}
    public Boolean hodEditMode {get; set;}
    public Boolean viewHODComments {get; set;}
    public Weekly_Sales_Review__c wsrRecord {get; set;}
    public String wsrId {get; set;}
    public Map<String, Boolean> rmActiveMap {get; set;}
    public Date weekEnd {get; set;}
    public List<String> rmIdList {get; set;}   

    public  Damac_WeeklySalesReview(){
        reviewExists = false;
        rmSize = 0;
        colSize = 0;
        errorMsg = '';
        rmIdList = new List<String>();
        hodEditMode = false;
        wrapperExists = false;
        rmWrapperExists = false;
        viewHODComments = false;
        extUserMode = false;
        isSubmit = false;
        isReviewer = false;
        noAccess = true;
        dosMode = false;
        hosMode = false;
        hodMode = false;
        editMode = false;
        dosId = '';
        hosId = '';
        hodId = '';
        wsrStatus = 'Draft';
        createdUser = '';
        rmMissing = false;
        wsrDate = system.today();
        wsrDate = wsrDate.toStartOfWeek().addDays(-1);
        weekEnd = wsrDate.addDays(5);
        rmActiveMap = new Map<String, Boolean>();
        meetingPresentMap = new Map<String, Boolean>();
        booleanMap = new Map<Boolean, String>();
        booleanMap.put(true, 'Yes');
        booleanMap.put(false, 'No');
        rmList = new List<User>();
        topicList = new List<String>();
        resultsOption = new List<SelectOption>();
        resultsOption.add(new SelectOption('Exceed Expectations', 'Exceed Expectations'));
        resultsOption.add(new SelectOption('Meets Expectations', 'Meets Expectations'));
        resultsOption.add(new SelectOption('Needs Improvement', 'Needs Improvement'));
        checklistOption = new List<SelectOption>();
        checklistOption.add(new SelectOption('No', 'No'));
        checklistOption.add(new SelectOption('Yes', 'Yes'));
        wsrRecordWrapper = new List<WeeklySalesReview>();
        User currentUser = [SELECT Profile.Name, Id, Name, User_Role__c, 
                                DOS_Name__c, HOS_Name__c, HOD_Name__c
                            FROM User WHERE Id =: UserInfo.getUserId()];
        system.debug('currentUser.Profile.Name: ' + currentUser.Profile.Name);
        if(currentUser.Profile.Name == 'Head of Sales' || currentUser.Profile.Name == 'Director of Sales'){
            noAccess = false;
            if(currentUser.Profile.Name == 'Director of Sales'){
                dosMode = true;
                createdUser = 'DOS';
            } else{
                hosMode = true;
                createdUser = 'HOS';    
            }
        }
        system.debug('hosMode: ' + hosMode + ', dosMode: ' + dosMode + ', hodMode: ' + hodMode );
        
        wsrId = apexpages.currentpage().getparameters().get('id');
        system.debug('wsrId: ' + wsrId);
        if(dosMode && (dosId == null || dosId == '')){
            dosId = UserInfo.getUserId();
        }
        if(hosMode && (hosId == null || hosId == '')){
            hosId = UserInfo.getUserId();
        }
        if(hodMode && (hodId == null || hodId == '')){
            hodId = UserInfo.getUserId();
        }
        if(wsrId != NULL && wsrId != ''){
            fetchWSR();
        }
        system.debug('hosId: ' + hosId + ', dosId: ' + dosId + ', hodId: ' + hodId );
        system.debug('noAccess: ' + noAccess);
        if(noAccess == false){
            system.debug('dosId: ' + dosId + ', dos: ' + dos);
            if(dosId != null && dosId != '' && dos == null){
                dos = [SELECT Id, Name, Manager.Id, Manager.Name, SmallPhotoUrl,
                            DOS_Name__c, HOS_Name__c, HOD_Name__c
                       FROM User WHERE Id =: dosId];
            }
            if(hos == null && hosId != null && hosId != ''){
                hos = [SELECT Id, Name, Manager.Id, Manager.Name, Manager.Profile.Name, SmallPhotoUrl FROM User WHERE Id =: hosId];
            }
            if(hod == null && hodId != null && hodId != ''){
                hod = [SELECT Id, Name, SmallPhotoUrl FROM User WHERE Id =: hodId];
            }

            if(currentUser != null && currentUser.HOS_Name__c != '' && hos == null){
                hos = [SELECT Id, Name, Manager.Id, Manager.Name, SmallPhotoUrl 
                        FROM User WHERE Name =: currentUser.HOS_Name__c AND IsActive = TRUE];
                hosId = hos.Id;
                if (createdUser == 'DOS' && String.valueOf(currentUser.Id) == hosId){
                    noAccess = true;
                }
            }
            if(currentUser != null && currentUser.HOD_Name__c != '' && hod == null){
                hod = [SELECT Id, Name, Manager.Id, Manager.Name, SmallPhotoUrl 
                       FROM User WHERE  Name =: currentUser.HOD_Name__c AND IsActive = TRUE];
                hodId = hod.Id;
                if(createdUser == 'HOS' && String.valueOf(currentUser.Id) == hodId){
                    noAccess = true;
                }
            }
            system.debug('createdUser: ' + createdUser);
            system.debug('currentUser.Id: ' + currentUser.Id);
            system.debug('hosId: ' + hosId);
            system.debug('hos: ' + hos);
            system.debug('hodId: ' + hodId);
            system.debug('hod: ' + hod);
            system.debug('dosId: ' + dosId);
            system.debug('dos: ' + dos);
            if(hos != null && noAccess == false){
                if(dosId != null && dosId != ''){
                    for(User rm: [SELECT Id, Name, SmallPhotoUrl 
                                  FROM User 
                                  WHERE Manager.Id =: dosId
                                  AND IsActive = TRUE
                                  ORDER BY Name]){
                        rmList.add(rm);
                        rmActiveMap.put(rm.Id, true);
                        meetingPresentMap.put(rm.Id, true);
                    }
                }
                if(dosMode){
                    meetingPresentMap.put(dosId, true);
                    meetingPresentMap.put(hosId, true);
                    meetingPresentMap.put(hodId, true);
                }
                if((hodMode && createdUser == 'HOS') ||(hosMode && createdUser == 'DOS') ){
                    isReviewer = true;
                }
                system.debug('rmList: ' + rmList);
                system.debug('editMode: ' + editMode);
                rmSize = rmList.size();
                if(createdUser == 'DOS' && rmSize == 0){
                    rmMissing = true;
                }
                colSize = rmSize;
                fetchWSRDetails();
                if(wsrStatus != 'Draft'  && !(wsrStatus == 'Submitted' && (dosMode || extUserMode))){
                    colSize++;
                }
            } else{
                noaccess = true;
                errorMsg = 'You are not allowed to create Weekly Sales Review.';
            }
        }
    }

    public void fetchWSR(){
        system.debug('wsrId: ' + wsrId);
        system.debug('wsrDate: ' + wsrDate);
        system.debug('hosId: ' + hosId);
        system.debug('dosId: ' + dosId);
        List<Weekly_Sales_Review__c> reviewList = new  List<Weekly_Sales_Review__c>();
        if(wsrId != null && wsrId != ''){
            system.debug('wsrId3: ' + wsrId);
            reviewList = [SELECT Id, Name, DOS__c, DOS__r.Name, HOS__c, HOS__r.Name, Review_Date__c, 
                                   HOD__c, HOD__r.Name, Review_Status__c, Review_Week_Details__c, Evaluation_User__c,
                                   HOS_Present__c, HOD_Present__c, DOS_Present__c
                           FROM Weekly_Sales_Review__c 
                           WHERE Id =: wsrId];
        }
        system.debug('Review List: ' + reviewList);
        if(reviewList.size() == 0){
            reviewList = [SELECT Id, Name, DOS__c, DOS__r.Name, HOS__c, HOS__r.Name, Review_Date__c, 
                                   HOD__c, HOD__r.Name, Review_Status__c, Review_Week_Details__c, Evaluation_User__c,
                                   HOS_Present__c, HOD_Present__c, DOS_Present__c
                           FROM Weekly_Sales_Review__c 
                           WHERE Review_Date__c =: wsrDate AND Evaluation_User__c =: createdUser 
                               AND ((Evaluation_User__c = 'DOS' AND DOS__c =: dosId) OR (Evaluation_User__c = 'HOS' AND HOS__c =: hosId) )];
        }
        system.debug('Review List2: ' + reviewList);
        for(Weekly_Sales_Review__c review: reviewList){
            system.debug('reviewId: ' + review.Id);
            noAccess = false;
            wsrRecord = review;
            wsrId = review.Id;
            dosId = review.DOS__c;
            hosId = review.hOS__c;
            hodId = review.HOD__c;
            meetingPresentMap.put(review.DOS__c, review.DOS_Present__c);
            meetingPresentMap.put(review.HOS__c, review.HOS_Present__c);
            meetingPresentMap.put(review.HOD__c, review.HOD_Present__c);
            if(hosId == UserInfo.getUserId()){
                hosMode = true;
            } else{
                hosMode = false;
            }
            if(dosId == UserInfo.getUserId()){
                dosMode = true;
            } else{
                dosMode = false;
            }
            if(hodId == UserInfo.getUserId()){
                hodMode = true;
            } else{
                hodMode = false;
            }
            wsrStatus = review.Review_Status__c;
            reviewExists = true;
            wsrDate = review.Review_Date__c;
            createdUser = review.Evaluation_User__c;
            system.debug('createdUser: ' + createdUser + 'wsrStatus: ' + wsrStatus);
            system.debug('dosMode: ' + dosMode + ', hosMode: ' + hosMode + ', hodMode ' + hodMode);
            if(createdUser == 'DOS'){
                if((dosMode && wsrStatus == 'Draft') || (hosMode && wsrStatus == 'Submitted')){
                    editMode = true;
                } else if(wsrStatus == 'Draft'){
                    noAccess = true;
                }
            }
            if(createdUser == 'HOS'){
                if((hosMode && wsrStatus == 'Draft')){
                    editMode = true;
                } else if(wsrStatus == 'Draft'){
                    noAccess = true;
                }
                if(hodMode && wsrStatus == 'Submitted'){
                    viewHODComments = true;
                    hodEditMode = true;
                }
                if(wsrStatus != 'Draft' && wsrStatus != 'Submitted'){
                    viewHODComments = true;
                }
            }
            if(!hosMode && !hodMode && !dosMode){
                editMode = false;
                hodEditMode = false;
                extUserMode = true;
                if(wsrStatus == 'Draft'){
                    noAccess = true;
                }
            }
        }
    }
 
    public void fetchWSRDetails(){
        Integer i = 0;
        wsrRecordWrapper = new List<WeeklySalesReview>();
        wsrRecordWrapperRM = new List<WeeklySalesReview>();
        fetchWSR();
        system.debug('meetingPesentMap3: ' + meetingPresentMap);
        
        for(Weekly_Sales_Review_Details__c reviewDetails: [SELECT Id, Name, Boolean_Result__c, Remarks__c, Review_Result__c,
                                                                Weekly_Sales_Review__c, Remarks_Required__c, Reviewer_Comments__c,
                                                                Weekly_Sales_Review__r.Review_Status__c, Evaluation_User__c,
                                                                Particular__c, Topic__c, 
                                                                Sl_No__c, Sub_Sl_No__c, Reviewer_Remarks__c,
                                                                        (SELECT Id, Name, RM__c, RM__r.Name, Weekly_Sales_Review__c, 
                                                                                RM_Active__c, Review_Result__c, RM_Present__c
                                                                         FROM Weekly_Sales_Review_Records__r
                                                                         ORDER BY RM__r.Name)
                                                           FROM Weekly_Sales_Review_Details__c 
                                                           WHERE Weekly_Sales_Review__c =: wsrId
                                                           ORDER BY Sl_No__c ASC, Sub_Sl_No__c ASC ]){
            WeeklySalesReview newWrapperRecord = new WeeklySalesReview(reviewDetails);
            Boolean extView = false;
            if(reviewDetails.Evaluation_User__c == 'DOS' && wsrStatus != 'Submitted'){
                extView = true;
            }
            if(reviewDetails.Evaluation_User__c == 'HOS' && wsrStatus != 'Draft'){
                extView = true;
            }
            if(reviewDetails.Evaluation_User__c == 'RM'){
                wsrRecordWrapperRM.add(newWrapperRecord);
            } else if( (dosMode == false && (extUserMode == false || extView)) || (wsrStatus != 'Draft' && wsrStatus != 'Submitted')){
                wsrRecordWrapper.add(newWrapperRecord);
            }
            for(Weekly_Sales_Review_Record__c rec: reviewDetails.Weekly_Sales_Review_Records__r){
                rmActiveMap.put(rec.RM__c, rec.RM_Active__c);
                meetingPresentMap.put(rec.RM__c, rec.RM_Present__c);
            }
            
            if(reviewDetails.Remarks_Required__c){
                WeeklySalesReview newWrapperRecord2 = new WeeklySalesReview(reviewDetails, true);
                if(reviewDetails.Evaluation_User__c == 'RM'){
                    wsrRecordWrapperRM.add(newWrapperRecord2);
                } else if( (dosMode == false && (extUserMode == false || extView))  ||  (wsrStatus != 'Draft' && wsrStatus != 'Submitted')){
                    wsrRecordWrapper.add(newWrapperRecord2);
                }
            }
        }
        for(WeeklySalesReview wsr: wsrRecordWrapperRM){    
            for(WSRRecordsWrapper wsrWrapper: wsr.wsrRecsWrapper){  
                if(!rmIdList.contains(wsrWrapper.rmId)){    
                    rmIdList.add(wsrWrapper.rmId);  
                }   
            }   
        }   
        system.debug('rmIdList: ' + rmIdList); 
        if(wsrRecordWrapperRM.size() > 0){    
            List<String> rmIdExcludeList = new List<String>();  
            for(User rm: rmList ){  
                if(!rmIdList.contains(rm.Id)){  
                    rmIdExcludeList.add(rm.Id); 
                }   
            }   
            rmList = new List<User>(); 
            system.debug('dosId: ' +dosId); 
            system.debug('rmIdExcludeList: ' +rmIdExcludeList); 
            system.debug('dosId: ' +dosId); 
            
            for(User rm: [SELECT Id, Name, SmallPhotoUrl    
                              FROM User     
                              WHERE (Manager.Id =: dosId    
                              AND IsActive = TRUE   
                              AND Id NOT IN: rmIdExcludeList)   
                              OR Id IN: rmIdList    
                              ORDER BY Name LIMIT 50]){  
                rmList.add(rm); 
            }   
            rmSize = rmList.size(); 
        }
        if(wsrRecordWrapper.size() > 0){
            wrapperExists = true;
        }
        if(wsrRecordWrapperRM.size() > 0){
            rmwrapperExists = true;
        }
        weekEnd = wsrDate.addDays(5);
        system.debug('wsrRecordWrapper: ' + wsrRecordWrapper);
        system.debug('wsrRecordWrapper.size(): ' + wsrRecordWrapper.size());
        system.debug('reviewExists: ' + reviewExists);
        system.debug('meetingPesentMap2: ' + meetingPresentMap);
        system.debug('wsrStatus: ' + wsrStatus);
    }

    public PageReference createWSR(){
        List<Weekly_Sales_Review_Details__c> wsrDetailsList = new List<Weekly_Sales_Review_Details__c>();
        List<Weekly_Sales_Review_Detail__mdt> itemsList = new List<Weekly_Sales_Review_Detail__mdt>();
        for(Weekly_Sales_Review_Detail__mdt reviewItem: [SELECT Id, Particular__c, Topic__c, Sl_No__c, Sub_Sl_No__c, 
                                                            Evaluation_User__c, Boolean_Result__c, Remarks_Required__c
                                                        FROM Weekly_Sales_Review_Detail__mdt]){
            itemsList.add(reviewItem);
        }
        String absIds =  apexpages.currentpage().getparameters().get('absentList');
        if(absIds != null){
            for(String memberId: absIds.split(';')){
                meetingPresentMap.put(memberId, false);
            }
        }
        system.debug('itemsList: ' + itemsList);
        Weekly_Sales_Review__c review = new Weekly_Sales_Review__c();
        if(dosId != null && dosId != ''){
            review.DOS__c = dosId;
        }
        if(hosId != null && hosId != ''){
            review.HOS__c = hosId;
        }
        if(hodId != null && hodId != ''){
            review.HOD__c = hodId;
        }
        if(dosMode){
            if(review.HOD__c != null && meetingPresentMap.containsKey(review.HOD__c) ){
                review.HOD_Present__c = meetingPresentMap.get(review.HOD__c);
            }
            if(review.HOS__c != null && meetingPresentMap.containsKey(review.HOS__c) ){
                review.HOS_Present__c = meetingPresentMap.get(review.HOS__c);
            }
            if(review.DOS__c != null && meetingPresentMap.containsKey(review.DOS__c) ){
                review.DOS_Present__c = meetingPresentMap.get(review.DOS__c);
            }
        }
        review.Review_Date__c = wsrDate;
        review.Evaluation_User__c = createdUser;
        review.Review_Status__c = 'Draft';
        insert review;
        system.debug('meetingPresentMap: ' + meetingPresentMap);
        for(Weekly_Sales_Review_Detail__mdt reviewItem: itemsList){
            if(createdUser == 'DOS' && (reviewItem.Evaluation_User__c == 'RM' || reviewItem.Evaluation_User__c == 'DOS')
                    || (createdUser == 'HOS' && reviewItem.Evaluation_User__c == 'HOS')){
                Weekly_Sales_Review_Details__c wsrDetail = new Weekly_Sales_Review_Details__c();
                wsrDetail.Weekly_Sales_Review__c = review.Id;
                wsrDetail.Particular__c = reviewItem.Particular__c;
                wsrDetail.Topic__c = reviewItem.Topic__c;
                wsrDetail.Remarks_Required__c = reviewItem.Remarks_Required__c;
                wsrDetail.Boolean_Result__c = reviewItem.Boolean_Result__c;
                wsrDetail.Sl_No__c = reviewItem.Sl_No__c;
                wsrDetail.Sub_Sl_No__c = reviewItem.Sub_Sl_No__c;
                wsrDetail.Evaluation_User__c = reviewItem.Evaluation_User__c;
                wsrDetailsList.add(wsrDetail);
            }
        }
        system.debug('wsrDetailsList: ' + wsrDetailsList);
        if(wsrDetailsList.size() > 0){
            insert wsrDetailsList;
        }
        List<Weekly_Sales_Review_Record__c> wsrRecList = new List<Weekly_Sales_Review_Record__c>();
        system.debug('rmList: ' + rmList);
        for(User rm: rmList){
            for(Weekly_Sales_Review_Details__c wsrDetail: wsrDetailsList){
                if(wsrDetail.Evaluation_User__c == 'RM'){
                    Weekly_Sales_Review_Record__c rec = new Weekly_Sales_Review_Record__c();
                    rec.Weekly_Sales_Review__c = review.Id;
                    rec.Weekly_Sales_Review_Detail__c = wsrDetail.Id;
                    rec.RM__c = rm.Id;
                    if(meetingPresentMap.containsKey(rec.RM__c) ){
                        rec.RM_Present__c = meetingPresentMap.get(rec.RM__c );
                    }
                    if(wsrDetail.Boolean_Result__c){
                        rec.Review_Result__c = 'No';
                    }
                    wsrRecList.add(rec);
                }
            }
        }
        system.debug('wsrRecList: ' + wsrRecList);
        if(wsrRecList.size() > 0){
            insert wsrRecList;
        }
        PageReference pageRef = new PageReference('/apex/DAMAC_WeeklySalesReview?id=' + review.Id);
        pageRef.setRedirect(true); 
        return pageRef;
    } 

    public PageReference closeWSR(){
        saveHOSWSR();
        if(wsrRecord != null){
             updateWSRToPendingAck();
            sendClosedNotificationEmail();
        }
        PageReference pageRef = new PageReference('/apex/DAMAC_WeeklySalesReview?id=' + wsrId);
        pageRef.setRedirect(true); 
        return pageRef;
    } 

    public void closeWSRByDOS(){
        updateWSRToClosed();
        fetchWSRDetails();
    } 

    public void updateWSRToPendingAck(){
        if(wsrRecord != null){
            wsrRecord.Review_Status__c = 'Pending Acknowledgement';
            wsrStatus = wsrRecord.Review_Status__c;
            update wsrRecord;
        }
    } 

    public void updateWSRToClosed(){
        if(wsrRecord != null){
            wsrRecord.Review_Status__c = 'Closed';
            wsrStatus = wsrRecord.Review_Status__c;
            update wsrRecord;
        }
    } 

    public void saveHOSWSR(){
        system.debug('Inside saveHOSWSR method');
        updateWSRDetails();
        updateWSRRecords();
        fetchWSRDetails();
    }

    public PageReference submitWSR(){
        isSubmit = true;
        upateStatusToSubmitted();
        saveWSR();
        PageReference pageRef = new PageReference('/apex/DAMAC_WeeklySalesReview?id=' + wsrId);
        pageRef.setRedirect(true); 
        return pageRef;
    }
    
    public void upateStatusToSubmitted(){
        if(wsrRecord != null){
            wsrRecord.Review_Status__c = 'Submitted';
            wsrStatus = wsrRecord.Review_Status__c;
            update wsrRecord;
            sendSubmitNotificationEmail();
        }
    }
    
    public void updateWSRDetails(){
        Map<Id, Weekly_Sales_Review_Details__c> wsrDetailsToUpdate = new Map<Id, Weekly_Sales_Review_Details__c>();
        for(WeeklySalesReview wrapper: wsrRecordWrapper){
            Weekly_Sales_Review_Details__c wsrDetail = wrapper.wsrDetail;
            if(wrapper.remarksRow == false){
                wsrDetail.Review_Result__c = wrapper.reviewResult;
                wsrDetail.Reviewer_Comments__c = wrapper.reviewerComments;
            } else{
                wsrDetail.Remarks__c = wrapper.remarks;
                wsrDetail.Reviewer_Remarks__c = wrapper.reviewerRemarks;
            }
            wsrDetailsToUpdate.put(wsrDetail.Id, wsrDetail);
        }
        for(WeeklySalesReview wrapper: wsrRecordWrapperRM){
            system.debug('Updated wrapper: ' + wrapper);
            Weekly_Sales_Review_Details__c wsrDetail = wrapper.wsrDetail;
            if(wrapper.remarksRow == false){
                wsrDetail.Reviewer_Comments__c = wrapper.reviewerComments;
            } else{
                wsrDetail.Remarks__c = wrapper.remarks;
                wsrDetail.Reviewer_Remarks__c = wrapper.reviewerRemarks;
            }
            wsrDetailsToUpdate.put(wsrDetail.Id, wsrDetail);
        }
        system.debug('wsrDetailsToUpdate: ' + wsrDetailsToUpdate);
        if(wsrDetailsToUpdate.keyset().size() > 0){
            update wsrDetailsToUpdate.values();
        }
    }
    
    public void updateWSRRecords(){
        String psIds = '';
        String activeIds = '';
        if(dosMode){
            if(isSubmit){
                psIds = apexpages.currentpage().getparameters().get('presentIdList');
                activeIds = apexpages.currentpage().getparameters().get('activeIdList');
            } else{
                psIds = apexpages.currentpage().getparameters().get('presentIdsList');
                activeIds = apexpages.currentpage().getparameters().get('activeIdsList');
            }
            for(String memberId: rmActiveMap.keyset()){
                rmActiveMap.put(memberId, false);
            }
            for(String memberId: meetingPresentMap.keyset()){
                meetingPresentMap.put(memberId, false);
            }
            if(activeIds != null && activeIds != ''){
                for(String memberId: activeIds.split(';')){
                    rmActiveMap.put(memberId, true);
                }
            }
            if(psIds != null && psIds != ''){
                for(String memberId: psIds.split(';')){
                    meetingPresentMap.put(memberId, true);
                }
            }
            if(wsrRecord.HOD__c != null && meetingPresentMap.containsKey(wsrRecord.HOD__c) ){
                wsrRecord.HOD_Present__c = meetingPresentMap.get(wsrRecord.HOD__c);
            }
            if(wsrRecord.HOS__c != null && meetingPresentMap.containsKey(wsrRecord.HOS__c) ){
                wsrRecord.HOS_Present__c = meetingPresentMap.get(wsrRecord.HOS__c);
            }
            if(wsrRecord.DOS__c != null && meetingPresentMap.containsKey(wsrRecord.DOS__c) ){
                wsrRecord.DOS_Present__c = meetingPresentMap.get(wsrRecord.DOS__c);
            }
            update wsrRecord;
        }
        Map<Id, Weekly_Sales_Review_Record__c> recsToUpdate = new Map<Id, Weekly_Sales_Review_Record__c>();
        for(WeeklySalesReview wrapper: wsrRecordWrapperRM){
            if(wrapper.remarksRow == false){
                for(WSRRecordsWrapper recWrapper: wrapper.wsrRecsWrapper){
                    Weekly_Sales_Review_Record__c rec = recWrapper.record;
                    rec.review_result__c = recWrapper.reviewResult;
                    if(dosMode){
                        if(rmActiveMap.containsKey(rec.RM__c)){
                            rec.RM_Active__c = rmActiveMap.get(rec.RM__c);
                        }
                        if(meetingPresentMap.containsKey(rec.RM__c)){
                            rec.RM_Present__c = meetingPresentMap.get(rec.RM__c);
                        }
                    }
                    recsToUpdate.put(rec.Id, rec);
                }
            }
        }
        system.debug('recsToUpdate: ' + recsToUpdate);
        if(recsToUpdate.keyset().size() > 0){
            update recsToUpdate.values();
        }
    }

    public void saveWSR(){
        updateWSRRecords();
        updateWSRDetails();
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'Weekly Sales Review Saved successfully'));
        fetchWSRDetails();
    }

    public void sendSubmitNotificationEmail(){
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new  Messaging.SingleEmailMessage();
        Date meetingDate = wsrRecord.Review_Date__c;
        String senderUserName ='';
        String receiverName = '';
        String targetId = '';
        if(createdUser == 'DOS'){
            senderUserName = dos.Name;
            receiverName = hos.Name;
            targetId  = hos.Id;
        } else if(createdUser == 'HOS'){
            senderUserName = hos.Name;
            receiverName = hod.Name;
            targetId  = hod.Id;
        }
        
        String body = 'Hi ' + receiverName + ',<br/><br/>';
        body += '<br/>' + senderUserName + ' submitted the Weekly Sales Review for the week ' 
                + String.valueOf(meetingDate) + ' to ' + + String.valueOf(meetingDate.addDays(5)) + '.';
        body += '<br/><br/> Link to the WSR: ' + System.URL.getSalesforceBaseUrl().toExternalForm()
                         + '/apex/DAMAC_WeeklySalesReview?id=' + wsrRecord.Id;
        String emailSubject = senderUserName + ' - Weekly Sales Review (' + String.valueOf(meetingDate) + ') Submitted';
        mail.setTargetObjectId(targetId);
        mail.setSenderDisplayName('Damac Property ');
        mail.setReplyTo('noreply@Damacgroup.com');
        mail.setSubject(emailSubject);
        mail.setSaveAsActivity(false);
        System.debug('...body...' + body);
        mail.setHtmlBody(body);
        mails.add(mail);  
        system.debug('mail: ' + mail);
        Messaging.sendEmail(mails);
    }

    public void sendClosedNotificationEmail(){
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new  Messaging.SingleEmailMessage();
        Date meetingDate = wsrRecord.Review_Date__c;
        String senderUserName ='';
        String receiverName = '';
        String targetId = '';
        if(createdUser == 'DOS'){
            senderUserName = hos.Name;
            receiverName = dos.Name;
            targetId = dos.Id;
        } else if(createdUser == 'HOS'){
            senderUserName = hod.Name;
            receiverName = hos.Name;
            targetId = hos.Id;
        }
        String body = 'Hi ' + receiverName + ',<br/><br/>';
        body += '<br/>' + senderUserName + ' Reviewed the WSR ' + String.valueOf(meetingDate);
        String emailSubject = senderUserName + ' - Weekly Sales Review (' + String.valueOf(meetingDate) + ') ';
        body += ' and Send back for Acknowledgement.';
        emailSubject += 'Pending Acknowledgement';
        body += '<br/><br/> Link to the WSR: ' + System.URL.getSalesforceBaseUrl().toExternalForm() + '/apex/DAMAC_WeeklySalesReview?id=' + wsrRecord.Id;
        mail.setTargetObjectId(targetId);
        mail.setSenderDisplayName('Damac Property ');
        mail.setReplyTo('noreply@Damacgroup.com');
        mail.setSubject(emailSubject);
        mail.setSaveAsActivity(false);
        System.debug('...body...' + body);
        mail.setHtmlBody(body);
        mails.add(mail);  
        system.debug('mail: ' + mail);
        Messaging.sendEmail(mails);
    }


    public class WeeklySalesReview{
        public Weekly_Sales_Review_Details__c wsrDetail {get;set;}
        public List<WSRRecordsWrapper> wsrRecsWrapper {get; set;}
        public String slNo {get;set;}
        public String topic {get;set;}
        public String evenString {get; set;}
        public String reviewResult {get;set;}
        public String remarks {get;set;}
        public boolean isBooleanResult {get;set;}
        public String subSlNo {get;set;}
        public boolean remarksReq {get;set;}
        public String particular {get;set;}
        public String reviewType {get; set;}
        public boolean showTopic {get; set;}
        public integer rowspan {get; set;}
        public String reviewerRemarks {get; set;}
        public String reviewerComments {get; set;}
        public Boolean remarksRow {get; set;}
        public string hodComments {get; set;}

        public WeeklySalesReview(Weekly_Sales_Review_Details__c wsrDetailRec, Boolean wsrReveiew){
            showTopic = false;
            reviewType = '';
            remarksRow = true;
            remarks = wsrDetailRec.Remarks__c;
            reviewerRemarks = wsrDetailRec.Reviewer_Remarks__c;
            wsrRecsWrapper = new List<WSRRecordsWrapper>();
            slNo = String.valueOf(wsrDetailRec.Sl_No__c);
            if(math.mod(Integer.valueOf(wsrDetailRec.Sl_No__c), 2) == 0){
                evenString = 'even';
            } else{
                evenString = 'odd';
            }
            subSlNo = '';  
            remarksReq = true;
            wsrDetail = wsrDetailRec;
        }

        public WeeklySalesReview(Weekly_Sales_Review_Details__c wsrDetailRec){
            wsrDetail = wsrDetailRec;
            rowspan = 1;
            reviewType = '';
            showTopic = true;
            remarksRow = false;
            isBooleanResult = wsrDetailRec.Boolean_Result__c;
            wsrRecsWrapper = new List<WSRRecordsWrapper>();
            reviewerComments = wsrDetail.Reviewer_Comments__c;
            slNo = String.valueOf(wsrDetailRec.Sl_No__c);
            if(math.mod(Integer.valueOf(wsrDetailRec.Sl_No__c), 2) == 0){
                evenString = 'even';
            } else{
                evenString = 'odd';
            }
            reviewResult = wsrDetailRec.Review_Result__c;
            if(reviewResult == null || reviewResult == ''){
                if(isBooleanResult){
                    reviewResult = 'No';
                } else{
                    reviewResult = 'Needs Improvement';
                }
            }
            reviewType = wsrDetailRec.Evaluation_User__c;
            subSlNo = String.valueOf(wsrDetailRec.Sub_Sl_No__c);
            topic = wsrDetailRec.Topic__c;
            remarksReq = false;
            particular =  wsrDetailRec.Particular__c;
            if(topicList == null){
                topicList = new List<String>();    
            }
            String key = wsrDetailRec.Evaluation_User__c + '-' + wsrDetailRec.Topic__c ;
            if(topicList.contains(key)){
                showTopic = false;
            } else{
                topicList.add(key);
            }
            for(Weekly_Sales_Review_Record__c recs: wsrDetailRec.Weekly_Sales_Review_Records__r){
                WSRRecordsWrapper newRec = new WSRRecordsWrapper(recs, isBooleanResult);
                wsrRecsWrapper.add(newRec);
            }
            system.debug('wsrRecsWrapper: ' + wsrRecsWrapper);
        }
    }

    public class WSRRecordsWrapper{
        public String rmId{get; set;}
        public Weekly_Sales_Review_Record__c record {get; set;}
        public string reviewResult {get; set;}
        public String reviewerComments {get; set;}
        public WSRRecordsWrapper(Weekly_Sales_Review_Record__c rec, Boolean isBooleanResult){
            record = rec;
            rmId = record.RM__c;
            reviewResult = record.review_result__c;
            if(reviewResult == null || reviewResult == ''){
                if(isBooleanResult){
                    reviewResult = 'No';
                } else{
                    reviewResult = 'Needs Improvement';
                }
            }
            
        }
    }
}