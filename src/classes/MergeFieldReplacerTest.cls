@isTest
public with sharing class MergeFieldReplacerTest{
    @TestSetup
    static void createTestData() {
        // create Account
        Account objAcc = new Account(Name = 'test', Party_Id__c = '43434');
        insert objAcc;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        insert objBookingUnit;

        Case objCase = new Case();
        objCase.Booking_Unit__c = objBookingUnit.Id;
        insert objCase;
    }
    
    @IsTest
    static void testReplaceMergeFields(){
        
        Case objCase = [SELECT Id from Case LIMIT 1];
        Test.startTest();
        MergeFieldReplacer.replaceMergeFields( '{!Case.CaseNumber}  {!Customer_Name__c}', 'Generic', objCase.Id );
        Test.stopTest();
        
    }
    
    @isTest
    private static void testParenting1() {
        // Test data setup
        Case objCase = [SELECT Id,Booking_Unit__c from Case LIMIT 1];
        // Actual test
        Test.startTest();
        MergeFieldReplacer.getStringValue( 'Booking_Unit__c', objCase);
        Test.stopTest();
    }

    @isTest
    private static void testParenting2() {
        // Test data setup
        Case objCase = [SELECT Id,Booking_Unit__r.Booking__c from Case LIMIT 1];
        // Actual test
        Test.startTest();
        MergeFieldReplacer.getStringValue( 'Booking_Unit__r.Booking__c', objCase);
        Test.stopTest();
    }

    @isTest
    private static void testParenting3() {
        // Test data setup
        Case objCase = [SELECT Id,Booking_Unit__r.Booking__r.Account__r.Name from Case LIMIT 1];
        // Actual test
        Test.startTest();
        MergeFieldReplacer.getStringValue( 'Booking_Unit__r.Booking__r.Account__r.Name', objCase);
        Test.stopTest();
    
    }
}