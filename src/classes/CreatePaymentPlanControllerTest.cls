@isTest
public class CreatePaymentPlanControllerTest {    
    @isTest
    public static void TestMethod1(){
      PageReference pageRef = Page.CreatePaymentPlan;
      Test.setCurrentPage(pageRef);
      Test.startTest();
      String inputDate = string.valueof(system.today());
      CreatePaymentPlanController controller= new CreatePaymentPlanController();
      Apexpages.currentpage().getparameters().put('desc','Test Desc');
      Apexpages.currentPage().getparameters().put('startDate','14/10/2020');
      Apexpages.currentpage().getparameters().put('endDate', '14/10/2020');
      Apexpages.currentPage().getparameters().put('countLines','12');
      Apexpages.currentpage().getparameters().put('percentage', '100');
      Apexpages.currentPage().getparameters().put('planType','Unit'); 
      ApexPages.currentPage().getParameters().put('milestoneType','Immediate');
	  ApexPages.currentPage().getParameters().put('percent','100');		
	  ApexPages.currentPage().getParameters().put('mileStone','Immediate');
   	  ApexPages.currentPage().getParameters().put('event','Immediate');		
	  ApexPages.currentPage().getParameters().put('paymentDate','14/10/2020');       
      ApexPages.currentPage().getParameters().put('eventdate','AgreementDate');		
      ApexPages.currentPage().getParameters().put('paymentInDays','10');
  	  ApexPages.currentPage().getParameters().put('description','Deposit');
      controller.showReviewPage();
      controller.createPaymentTerms();
      controller.insertPaymentDetails();
      
      controller.UpdatePaymentTerms();
      controller.UpdatePaymentTermsList();
      controller.DeletePaymentTerms();
      Test.stopTest();
       
    }

}