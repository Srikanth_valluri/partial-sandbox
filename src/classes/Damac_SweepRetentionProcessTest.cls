@isTest
private class Damac_SweepRetentionProcessTest{
    static testMethod void method1() {
        User owner = new User();
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole11');
            insert userRoleObj;
            Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
            owner = new User(alias = 't4546', email='xyz4211@email.com',New_Team__c = 'Retention Team',
                    emailencodingkey='UTF-8', lastname='U 4546', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                    ,timezonesidkey='America/Los_Angeles', username='xyz4121@email.com',UserRoleId = userRoleObj.Id);
            insert owner;
        }
        Inquiry__c testInquiry = new Inquiry__c();
        testInquiry.First_Name__c = 'test';
        testInquiry.ownerid = owner.Id;
        testInquiry.Is_Retention__c  = true;
        insert testInquiry;
        
        Inquiry__c testInquiry1 = new Inquiry__c();
        testInquiry1.First_Name__c = 'test';
        testInquiry1.Pre_InquiryId__c  = testInquiry.Id;
        testInquiry1.ownerid = owner.Id;
        testInquiry1.Is_Retention__c  = true;
        insert testInquiry1;
        
        Test.startTest();
        Damac_SweepRetentionProcess obj = new Damac_SweepRetentionProcess();
        DataBase.executeBatch(obj);
        Test.stopTest();
        system.assertEquals(testInquiry1.Pre_InquiryId__c , testInquiry.Id);

    }
}