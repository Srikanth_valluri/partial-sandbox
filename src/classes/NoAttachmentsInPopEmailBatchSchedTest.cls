@isTest
public class NoAttachmentsInPopEmailBatchSchedTest {

    @isTest
    public static void testSchedule() {
        NoAttachmentsInPopEmailBatchOnCaseSched objSch = new NoAttachmentsInPopEmailBatchOnCaseSched();

        String sch = '0 0 23 * * ?';
        Test.StartTest();
        system.schedule('Test NoAttachmentsInPopEmailBatchSched', sch, objSch );
        Test.stopTest();
    }

}