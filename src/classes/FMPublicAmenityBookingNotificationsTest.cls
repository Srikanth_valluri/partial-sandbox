/*-------------------------------------------------------------------------------------------------
Description: Test class for FMPublicAmenityBookingNotifications
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 10-03-2019       | Lochana Rajput   | 1. Added test method to check the functionality
=============================================================================================================================
*/
@isTest
private class FMPublicAmenityBookingNotificationsTest {

	@isTest static void test_notification() {
		List<FM_Case__c> lstFMCases = new List<FM_Case__c>();
		List<Resource__c> lstResources = new List<Resource__c>();
		List<User> lstUsers = [SELECT Id from User where isActive = true AND Email like '%@damac%' LIMIT 2];
		for(Integer i=0; i<20; i++) {
			lstResources.add(new Resource__c(Public_Amenity_Name__c = 'Test' + i,
			Is_Available_for_Public__c = true, Amenity_Admin__c = lstUsers[0].Id,
			Amenity_Manager__c = lstUsers[1].Id));
		}
		insert lstResources;
		for(Integer i=0; i<20; i++) {
			if(i>3) {
				lstFMCases.add(new FM_Case__c(Resource__c=lstResources[i].Id,
					Amenity_Booking_Status__c = 'Booking Confirmed'));
			}
			if(i>7) {
				lstFMCases.add(new FM_Case__c(Resource__c=lstResources[i].Id,
					Amenity_Booking_Status__c = 'Cancelled'));
			}
			else {
				lstFMCases.add(new FM_Case__c(Resource__c=lstResources[i].Id,
					Amenity_Booking_Status__c = 'Rejected'));
			}

		}
		insert lstFMCases;
		insert new Contact(lastName='test');
		Set<Id> setRecordIds = new Map<Id, FM_Case__c>(lstFMCases).keySet();
		List<Id> lstRecIds = new List<Id>();
		lstRecIds.addAll(setRecordIds);
		Test.startTest();
		FMPublicAmenityBookingNotifications.sendNotificationEmails(lstRecIds);
		Test.stopTest();
	}

}