@isTest
public class AttachmentHandlerTest {

    @isTest
    public static void testsendVATLetter() {
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account(Name = 'Test Account', Email__c = 'test321@ttt.com',Send_Bulk_Email__c = true);
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;

        Booking_unit__c objBU = new Booking_Unit__c (Booking__c = booking.Id);
        insert objBU;

        Attachment att = new Attachment ( Name = 'VAT Letter', ParentId = objBU.Id, Body = blob.valueOf( 'TEST') );
        insert att;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
        AttachmentHandler.sendVATLetter( att.Id );
        Test.stopTest();
    }

    @isTest
    public static void cloneAttachmentToChildCases(){
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;

        Id personAccountRTId =
            Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( FirstName = 'Test'
                                     , LastName = 'Account'
                                     , RecordTypeId = personAccountRTId);
        insert account;

        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(Agency_ID__c = '1234');
        insert sr;

        Booking__c booking = new Booking__c(Account__c = account.Id, Deal_SR__c = sr.Id);
        insert booking;

        Booking_Unit__c bUObj = new Booking_Unit__c (Booking__c = booking.Id);
        insert bUObj;

        Id handoverCaseRTId =
            Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        Case caseObj = TestDataFactory_CRM.createCase(account.Id,handoverCaseRTId);
        caseObj.Booking_Unit__c = bUObj.Id;
        insert caseObj;

        bUObj.HO_EHO_PHO_CaseId__c = caseObj.Id;

        Attachment att = new Attachment ( Name = 'Handover Checklist.pdf'
                                        , ParentId = bUObj.Id
                                        , Body = blob.valueOf( 'Handover Checklist')
                                        );

        Test.startTest();
            UploadMultipleDocController.strLabelValue ='N';
            Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive2());
            insert att;
        Test.stopTest();

        /*Attachment newAtt = [SELECT Id
                               , Name
                               , Body
                               , ParentId
                            FROM Attachment
                           WHERE ParentId = :caseObj.Id
                             AND Name = 'Handover Checklist.pdf'
                         ];

        System.assertEquals(att.Name,newAtt.Name,'The Name of the attachment should'
                            +' be same as the cloned attachment');
        System.assertEquals(att.Body,newAtt.Body,'The Body of the attachment should'
                            +' be same as the cloned attachment');
        System.assertEquals(bUObj.HO_EHO_PHO_CaseId__c,newAtt.ParentId,'The Parent of the attachment should'
                            +' be same as the HO_EHO_PHO_CaseId__c field value on booking unit.');*/
    }

    @isTest
    public static void cloneAttToRelHandoverCase(){
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;

        Id personAccountRTId =
            Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( FirstName = 'Test'
                                     , LastName = 'Account'
                                     , RecordTypeId = personAccountRTId);
        insert account;

        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(Agency_ID__c = '1234');
        insert sr;

        Booking__c booking = new Booking__c(Account__c = account.Id, Deal_SR__c = sr.Id);
        insert booking;

        Booking_Unit__c bUObj = new Booking_Unit__c (Booking__c = booking.Id);
        insert bUObj;

        Id handoverCaseRTId =
            Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        Case caseHandoverObj = TestDataFactory_CRM.createCase(account.Id,handoverCaseRTId);
        caseHandoverObj.Booking_Unit__c = bUObj.Id;
        insert caseHandoverObj;

        Id earlyhandoverCaseRTId =
            Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();
        Case caseEarlyHandoverObj = TestDataFactory_CRM.createCase(account.Id,earlyhandoverCaseRTId);
        caseEarlyHandoverObj.Booking_Unit__c = bUObj.Id;
        insert caseEarlyHandoverObj;

        Attachment att = new Attachment ( Name = 'Handover Checklist.pdf'
                                        , ParentId = bUObj.Id
                                        , Body = blob.valueOf( 'Handover Checklist')
                                        );

        Test.startTest();
            UploadMultipleDocController.strLabelValue ='N';
            Test.setMock(HttpCalloutMock.class,
                new Office365RestServiceMock.Office365RestServiceMockPositive2());
            insert att;
        Test.stopTest();

        /*Attachment newAtt = [SELECT Id
                               , Name
                               , Body
                               , ParentId
                            FROM Attachment
                           WHERE ParentId = :caseHandoverObj.Id
                             AND Name = 'Handover CheckList.pdf'
                         ];

        System.assertEquals(att.Name,newAtt.Name,'The Name of the attachment should'
                            +' be same as the cloned attachment');
        System.assertEquals(att.Body,newAtt.Body,'The Body of the attachment should'
                            +' be same as the cloned attachment');
        System.assertEquals(caseHandoverObj.Id,newAtt.ParentId,'The Parent of the attachment should'
                            +' be same as the most recent Handover case field value on booking unit.');*/
    }

    @isTest
    public static void cloneAttToRelEarlyHandoverCase(){
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;

        Id personAccountRTId =
            Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( FirstName = 'Test'
                                     , LastName = 'Account'
                                     , RecordTypeId = personAccountRTId);
        insert account;

        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(Agency_ID__c = '1234');
        insert sr;

        Booking__c booking = new Booking__c(Account__c = account.Id, Deal_SR__c = sr.Id);
        insert booking;

        Booking_Unit__c bUObj = new Booking_Unit__c (Booking__c = booking.Id);
        insert bUObj;

        Id handoverCaseRTId =
            Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();
        Case caseEarlyHandoverObj = TestDataFactory_CRM.createCase(account.Id,handoverCaseRTId);
        caseEarlyHandoverObj.Booking_Unit__c = bUObj.Id;
        insert caseEarlyHandoverObj;

        Attachment att = new Attachment ( Name = 'Handover CheckList.pdf'
                                        , ParentId = bUObj.Id
                                        , Body = blob.valueOf( 'Handover Checklist')
                                        );

        Test.startTest();
            UploadMultipleDocController.strLabelValue ='N';
            Test.setMock(HttpCalloutMock.class,
                new Office365RestServiceMock.Office365RestServiceMockPositive2());
            insert att;
        Test.stopTest();

        Attachment newAtt = [SELECT Id
                               , Name
                               , Body
                               , ParentId
                            FROM Attachment
                           //WHERE ParentId = :caseEarlyHandoverObj.Id
                             WHERE ParentId = :bUObj.Id
                             AND Name = 'Handover CheckList.pdf'
                         ];

        System.assertEquals(att.Name,newAtt.Name,'The Name of the attachment should'
                            +' be same as the cloned attachment');
        System.assertEquals(att.Body,newAtt.Body,'The Body of the attachment should'
                            +' be same as the cloned attachment');
        System.assertEquals(bUObj.Id,newAtt.ParentId,'The Parent of the attachment should'
                            +' be same as the most recent case of type early handover case if  field'
                            +' value on booking unit.');
    }

    // @isTest
    // public static void cloneBulkAttToChildCases(){
    //     Credentials_Details__c settings = new Credentials_Details__c();
    //     settings.Name = 'office365RestServices';
    //     settings.User_Name__c = 'Some Value';
    //     settings.Password__c = 'Some Value';
    //     settings.Resource__c = 'Some Value';
    //     settings.grant_type__c = 'Some Value';
    //     insert settings;

    //     Integer recordCount = 100;
    //     Id personAccountRTId =
    //         Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
    //     Account account = new Account( FirstName = 'Test'
    //                                  , LastName = 'Account'
    //                                  , RecordTypeId = personAccountRTId);
    //     insert account;

    //     NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(Agency_ID__c = '1234');
    //     insert sr;

    //     Booking__c booking = new Booking__c(Account__c = account.Id, Deal_SR__c = sr.Id);
    //     insert booking;

    //     List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
    //     for(Integer i=0; i<100; i++){
    //         Booking_Unit__c bUObj = new Booking_Unit__c (Booking__c = booking.Id);
    //         bookingUnitList.add(bUObj);
    //     }
    //     insert bookingUnitList;

    //     Id handoverCaseRTId =
    //         Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
    //     List<Case> caseList = new List<Case>();
    //     for(Integer i=0; i<100; i++){
    //         Case caseObj = TestDataFactory_CRM.createCase(account.Id,handoverCaseRTId);
    //         caseObj.Booking_Unit__c = bookingUnitList[I].Id;
    //         caseList.add(caseObj);
    //         bookingUnitList[i].HO_EHO_PHO_CaseId__c = caseObj.Id;
    //     }
    //     insert caseList;

    //     update bookingUnitList;

    //     List<Attachment> attachmentList = new List<Attachment>();
    //     for(Integer i=0; i<100; i++){
    //         Attachment att = new Attachment ( Name = 'Handover Checklist.pdf'
    //                                         , ParentId = bookingUnitList[i].Id
    //                                         , Body = blob.valueOf( 'Handover Checklist')
    //                                         );
    //         attachmentList.add(att);
    //     }

    //     Test.startTest();
    //         UploadMultipleDocController.strLabelValue ='N';
    //         Test.setMock(HttpCalloutMock.class,
    //             new Office365RestServiceMock.Office365RestServiceMockPositive2());
    //         insert attachmentList;
    //     Test.stopTest();

    //     List<Attachment> newAttList = [SELECT Id
    //                                         , Name
    //                                         , Body
    //                                         , ParentId
    //                                      FROM Attachment
    //                                     WHERE ParentId IN :caseList
    //                                       AND Name = 'Handover Checklist.pdf'
    //                                     ];

    //     System.assertEquals('Handover Checklist.pdf', newAttList[0].Name, 'The Name of the attachment should'
    //                         +' be same as the cloned attachment');
    //     System.assertEquals(attachmentList.size(), newAttList.size(),'All the attachments of type Handover'
    //                        +'case should be cloned to the case record');
    // }

}