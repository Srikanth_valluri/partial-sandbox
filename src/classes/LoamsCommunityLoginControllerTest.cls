@isTest
private class LoamsCommunityLoginControllerTest {

    @testSetup
    static void setup() {
        insert new LoamsCommunitySettings__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            OwnerProfileName__c = CommunityTestDataFactory.COMMUNITY_USER_PROFILE,
            TenantProfileName__c = CommunityTestDataFactory.COMMUNITY_TENANT_USER_PROFILE,
            GuestUserName__c = FmcUtils.GUEST_USER_NAME,
            BaseURL__c = System.Label.DAMAC_LIVING +'/community'
        );
    }

    @isTest
    static void testGuestPaymentLink() {
        System.assertNotEquals(NULL, new LoamsCommunityLoginController().gotoGuestPayment());
        System.assertEquals(
            Page.GuestMakePayment.getUrl(), new LoamsCommunityLoginController().gotoGuestPayment().getUrl()
        );
    }

    @isTest
    private static void testLoginFromCustomerCommunity() {
        insert new PaymentGateway__c(
            Name = 'LOAMS Portal',
            Url__c = 'https://www.payment-gateway.com',
            MerchantId__c = 'merchant_id',
            AccessCode__c = 'access_code',
            EncryptionKey__c = 'encryption_key'
        );

        PageReference communityPortalLoginPage = Page.CommunityPortalLogin;
        communityPortalLoginPage.getParameters().put('username', UserInfo.getUserName());
        communityPortalLoginPage.getParameters().put('password', EncodingUtil.convertToHex(Crypto.encryptWithManagedIV(
            'AES128',
            Crypto.generateDigest('MD5', Blob.valueOf('encryption_key')),
            Blob.valueOf('password')
        )));
        Test.setCurrentPage(communityPortalLoginPage);
        LoamsCommunityLoginController controller = new LoamsCommunityLoginController();

        Test.startTest();
            controller.authenticateUser();
        Test.stopTest();
    }

    @IsTest
    public static void testLogin() {
        LoamsCommunityLoginController controller = new LoamsCommunityLoginController();
        list<Contact> lstContact = new list<Contact>();
        Id accountId;
        Id contactId;
        User u = new User();
        controller.firstName = 'ABC';
        controller.lastName = 'DEF';
        controller.model.email='abc@test.com';
         controller.model.phone='9876543210';
        controller.communityNickname='abc@1';
        controller.confirmPassword = 'abc123';
        controller.model.userName = '1451584';
        controller.password = 'abcd1234';
        controller.model.password = 'abcd1234';
        controller.model.partyId='1451580';
        controller.isValidPassword();

        Test.startTest();
            controller.doLogin();
            controller.model.userName = UserInfo.getUserName();
            controller.doLogin();
        Test.stopTest();

        string strUsername;

        if(controller.model.username != null && controller.model.username.contains('@')){
            if(!controller.checkUsername(controller.model.username)){
                controller.toastrError = 'The entered username is wrong please enter a correct username/PartyId';
                //return null;
            }else{
                strUsername = controller.username;
            }
        }else{
            strUsername = controller.fetchUsername(controller.model.username);
            if(strUsername == null){
              controller.toastrError = 'Please enter a correct Party Id.';
              //system.debug('Please enter a correct username doLogin');
              //return null;
            }
        }
        if(controller.model.partyId != null){
        system.debug('model.partyId'+controller.model.partyId);
        lstContact = [SELECT Id,
                             Account.Party_ID__c,
                             AccountId
                        FROM Contact
                       WHERE Account.Party_ID__c =: controller.model.partyId
                         AND (Is_Primary__c = true
                             OR
                             Account.isPersonAccount = true)];
        if(!lstContact.isEmpty() && lstContact.size() == 1){
                Accountid = lstContact[0].AccountId;
                controller.partyId = lstContact[0].Account.Party_ID__c;
                contactId = lstContact[0].id;
          }
        }
    }

    @IsTest
    public static void testDoLogin() {
        LoamsCommunityLoginController controller = new LoamsCommunityLoginController();
        User u = new User();

        controller.userName = 'srmaind@damac.com';
        controller.password = 'abcd1234';

        Test.startTest();
        controller.doLogin();
        Test.stopTest();

        string strUsername;

        if(controller.username != null && controller.username.contains('@')){
            if(!controller.checkUsername(controller.username)){
                controller.toastrError = 'The entered username is wrong please enter a correct username/PartyId';
                //return null;
            }else{
                strUsername = controller.username;
            }
        }else{
            strUsername = controller.fetchUsername(controller.username);
            if(strUsername == null){
              controller.toastrError = 'Please enter a correct Party Id.';
              //system.debug('Please enter a correct username doLogin');
              //return null;
            }
        }
        Id networkId = Network.getNetworkId();
        controller.lstNetwork= [SELECT  Id
                                     , Name
                                 FROM  Network
                                WHERE  Name = 'Damac Customer Community Portal'
                                    AND Id = NULL];
    }

     @IsTest
    public static void testSignUp() {
        LoamsCommunityLoginController controller = new LoamsCommunityLoginController();
        controller.model.firstName = 'FirstName';
        controller.model.lastName = 'LastName';
        controller.model.email = 'test@force.com';
        controller.model.phone = '5652';
        controller.model.password = 'abcd1234';
        controller.model.firstPassword = 'abcd1234';
        controller.model.confirmPassword = 'abcd1234';
        controller.model.partyid = '1451580';
        System.debug('controller.model.firstPassword = ' + controller.model.firstPassword);
        System.debug('controller.model.confirmPassword = ' + controller.model.confirmPassword);
        Test.startTest();
        controller.registerUser();
        Test.stopTest();

        Boolean messageFound = false;
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        //System.assertNotEquals(0, pageMessages.size());
        for(ApexPages.Message message : pageMessages) {
            if(message.getDetail().contains(Label.site.passwords_dont_match)) {
                messageFound = true;
            }
        }
        //System.assert(messageFound);
    }

    @IsTest
    public static void testSignUptwoAccounts() {
        TestUtility utility= new TestUtility();

        Id personAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName()
                                .get('Person Account').getRecordTypeId();
        List<Account> acclist = new List<Account>();
        for (Integer i=0; i<2; i++) {
           Account obj=new Account();
           obj.RecordTypeId=personAccountId;
           obj.party_Id__c='145158'+i;
           obj.firstName='firstName';
           obj.lastName='lastName';
           obj.Email__c='test@test.com';
           obj.Email__pc='test@test.com';
           obj.PersonEmail='test@test.com';
           obj.Mobile__c='5652';
           obj.Mobile_Phone_Encrypt__pc='5652';
           accList.add(obj);
        }//end for
        insert accList;
        LoamsCommunityLoginController controller = new LoamsCommunityLoginController();
        controller.model.firstName = 'FirstName';
        controller.model.lastName = 'LastName';
        controller.model.email = 'test@test.com';
        controller.model.phone = '5652';
        controller.model.password = 'abcd1234';
        controller.model.firstpassword = 'abcd1234';
        controller.model.confirmPassword = 'abcd1234';

        Test.startTest();
        controller.registerUser();
        Test.stopTest();

        list<Account> lstAccount = new list<Account>();
        Id Accountid;
        string partyId;
        lstAccount = [SELECT id,
                             Party_ID__c
                        FROM account
                       WHERE PersonEmail =: controller.model.email
                         AND Mobile__c =: controller.model.phone];
        system.assertEquals(2,lstAccount.size());
        if(!lstAccount.isEmpty() && lstAccount.size()== 1){
                Accountid = lstAccount[0].id;
                partyId = lstAccount[0].Party_ID__c;
        }
        if(!lstAccount.isEmpty() && lstAccount.size() > 1 ){
            System.debug('controller.model = ' + JSON.serialize(controller.model));
            lstAccount  = [SELECT id,
                                 Party_ID__c
                            FROM account
                           WHERE PersonEmail =: controller.model.email
                             AND Mobile__c =: controller.model.phone
                             AND firstname =:controller.model.firstName
                             AND lastName =: controller.model.lastName];
            if(!lstAccount.isEmpty() && lstAccount.size() == 1){
                Accountid = lstAccount[0].id;
                partyId = lstAccount[0].Party_ID__c;
            }
            //system.assertEquals(true, controller.model.hasMulipleAccounts);
        }
    }

    @IsTest
    public static void testSignUpOneAccount() {
        TestUtility utility= new TestUtility();
        list<Account> accList= utility.accInsrt(1);
        LoamsCommunityLoginController controller = new LoamsCommunityLoginController();
        controller.model.firstName = 'FirstName';
        controller.model.lastName = 'LastName';
        controller.model.email = 'test@test.com';
        controller.model.phone = '5652';
        controller.model.password = 'abcd1234';
        controller.model.firstpassword = 'abcd1234';
        controller.model.confirmPassword = 'abcd1234';

        Test.startTest();
        controller.registerUser();
        Test.stopTest();

        list<Account> lstAccount = new list<Account>();
        Id Accountid;
        string partyId;
        lstAccount = [SELECT id,
                             Party_ID__c
                        FROM account
                       WHERE PersonEmail =: controller.model.email
                         AND Mobile__c =: controller.model.phone];
        system.assertEquals(1,lstAccount.size());
        if(!lstAccount.isEmpty() && lstAccount.size()== 1){
            Accountid = lstAccount[0].id;
            partyId = lstAccount[0].Party_ID__c;
        }

    }

    @IsTest
    public static void testSignUpPartyId() {
        TestUtility utility= new TestUtility();
        list<Account> accList= utility.accInsrt(1);
        LoamsCommunityLoginController controller = new LoamsCommunityLoginController();
        controller.model.firstName = 'FirstName';
        controller.model.lastName = 'LastName';
        controller.model.email = 'test@test.com';
        controller.model.phone = '5652';
        controller.model.partyId='1451580';
        controller.model.password = 'abcd1234';
        controller.model.firstpassword = 'abcd1234';
        controller.model.confirmPassword = 'abcd1234';

        Test.startTest();
        controller.registerUser();
        Test.stopTest();

        list<Account> lstAccount = new list<Account>();
        Id Accountid;
        string partyId;
        lstAccount = [SELECT id,
                             Party_ID__c
                        FROM account
                       WHERE Party_ID__c=: controller.model.partyId ];
        if(!lstAccount.isEmpty() && lstAccount.size() == 1){
            Accountid = lstAccount[0].id;
            partyId = lstAccount[0].Party_ID__c;
        }
    }

    @IsTest
    public static void testSignUpPasswordMatch() {
        LoamsCommunityLoginController controller = new LoamsCommunityLoginController();
        controller.model.firstName = 'FirstName';
        controller.model.lastName = 'LastName';
        controller.model.email = 'test@test.com';
        controller.model.phone = '5652';
        controller.model.partyId='1451580';
        controller.model.password = 'abcd1234';
        controller.model.firstpassword = 'abcd1234';
        controller.model.confirmPassword = 'abcd123';

        Test.startTest();
        controller.registerUser();
        Test.stopTest();
    }

    @isTest
    public static void testNullUsernameForgotPassword() {
        LoamsCommunityLoginController controller = new LoamsCommunityLoginController();
        User u = new User();

        controller.model.userName = '';
        Test.startTest();
        controller.forgotPassword();
        Test.stopTest();
        controller.toastrError = 'Please enter username';

        Boolean messageFound = false;
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        //System.assertNotEquals(0, pageMessages.size());
        for(ApexPages.Message message : pageMessages) {
            if(message.getDetail().contains('Please enter username')) {
                messageFound = true;
            }
        }
        //System.assert(messageFound);
    }

    @isTest
    public static void testForgotPassword() {
        LoamsCommunityLoginController controller = new LoamsCommunityLoginController();
        //TestUtility utility= new TestUtility();
        //list<Account> accList= utility.accInsrt(1);

        controller.model.userName = '1451584';
        Test.startTest();
        controller.forgotPassword();
        Test.stopTest();
        controller.toastrError = 'An email is sent to your register email Id';
        controller.Username= controller.fetchUsername(controller.Username);
        String strPartyId = controller.Username;
        list<user> listuser = [SELECT Id,
                                      Username,
                                      Email,
                                      Contact.Account.Party_ID__c
                                FROM  User
                                WHERE IsActive = TRUE
                                  AND Contact.Account.Party_ID__c = :strPartyId];
      //system.assertEquals(0,listuser.size());

    }

    @isTest
    public static void testForgotPasswordCase() {
        LoamsCommunityLoginController controller = new LoamsCommunityLoginController();
        //TestUtility utility= new TestUtility();
        //list<Account> accList= utility.accInsrt(1);

        controller.model.userName = 'srmaind@damac.com';
        Test.startTest();
        controller.forgotPassword();
        Test.stopTest();
        if(controller.userName!= null){
            controller.toastrError = 'An email is sent to your reister email Id';
            //success = Site.forgotPassword(username);
        }

    }

     @isTest
     public static void testInitializeAccount() {
        TestUtility utility= new TestUtility();
        list<Account> accList= utility.accInsrt(1);
        LoamsCommunityLoginController controller = new LoamsCommunityLoginController();

        controller.partyId = '1451580';

        Test.startTest();
        //controller.InitializeAccount(controller.partyId);
        Test.stopTest();

        LoamsCommunityLoginController.AccountSearchModel model = new LoamsCommunityLoginController.AccountSearchModel();
        model.communityNickname = NULL;
        model.chosenAccount = NULL;
        model.username = NULL;
        model.password1 = NULL;
        System.debug(model.communityNickname);
        System.debug(model.chosenAccount);
        System.debug(model.username);
        System.debug(model.password1);

        try {
            LoamsCommunityLoginController.getOffersData();
        } catch(Exception e) {
            System.debug(e.getMessage());
        }

        list<Account> accInstance= new list<Account>();
        if(controller.partyid !=null){
            accInstance=[SELECT firstName,
                                lastName,
                                Email__c,
                                phone
                           FROM Account
                          WHERE Party_Id__c = :controller.partyId
                          LIMIT 1 ];
            system.assertEquals(1,accInstance.size());
            if(accInstance!= null && !accInstance.isEmpty()){
                controller.model.firstName= accInstance[0].firstName;
                controller.model.lastName= accInstance[0].lastName;
                controller.model.email=accInstance[0].Email__c;
                controller.model.phone=accInstance[0].Phone;
                System.debug(controller.email);
                System.debug(controller.phone);
            }
        }
    }
}