@isTest
public class API_UploadSRAttachment_Test {
    @testSetup static void setup() {
        Account acc = new Account ();
        acc.Name = 'test';
        acc.Agency_Type__c = 'Corporate';
        acc.Vendor_ID__c = '969696';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        insert acc;
        
        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);        
        sr.Eligible_to_Sell_in_Dubai__c = true;
        sr.Agency_Type__c = 'Individual';
        sr.ID_Type__c = 'Passport';
        sr.Agent_Name__c = UserInfo.getUserId();
        sr.Token_Amount_AED__c = 40000;
        sr.RecordTypeId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Change Agent').getRecordTypeId();
        sr.Booking_Wizard_Level__c = null;
        sr.Agency_Email_2__c = 'test2@gmail.com';
        sr.Alternate_Agency_Email_2__c = 'test1@gmail.com';
        sr.Country_of_Sale__c = 'UAE';
        sr.Mode_of_Payment__c = 'Cash';
        sr.agency__c = acc.id;
        insert sr;
        
    }
    
    @isTest static void API_UploadSRAttachment() {
        NSIBPM__Service_Request__c sr = [SELECT Name FROM NSIBPM__Service_Request__c LIMIT 1];
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/api_uploadSRAttachment';  
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf ('{"SRId": "'+sr.Id+'", "Name" : "test.jpg"}');
        RestContext.request = req;
        RestContext.response = res;
        
        API_UploadSRAttachment.AttachmentRequestDTO request = new API_UploadSRAttachment.AttachmentRequestDTO ();
        request.SRId = sr.Id;
        request.name = 'test.jpg';
        
        API_UploadSRAttachment.doPost (request);
        
    }
}