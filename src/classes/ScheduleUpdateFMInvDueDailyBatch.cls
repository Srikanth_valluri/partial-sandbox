/*
 * Description : This class is used to Schedule UpdateFMInvDueDailyBatch batch at every 3 
 *                                                      
 */
global class ScheduleUpdateFMInvDueDailyBatch implements Schedulable {
  global void execute(SchedulableContext sc) {
      UpdateFMInvDueDailyBatch batchInstance = new UpdateFMInvDueDailyBatch(); 
      database.executebatch(batchInstance,5);
  }
}