@isTest
public class Damac_SecureSMS_Test {
    public static testMethod void method1() {
        Stand_Inquiry__c inq = new Stand_Inquiry__c ();
        inq.Mobile_Phone_Encrypt__c = '8977365305';
        inq.Mobile_CountryCode__c = 'India: 0091';
        inq.SMS_MSG_Id__c = '9876543210';
        insert inq;
        inq.Whats_App_Short_URL__c = 'https://test.com';
        update inq;
        
        Digital_Inquiry__c Diginq = new Digital_Inquiry__c();
        Diginq.Mobile_Phone_Encrypt__c = '8977365305';
        Diginq.Mobile_CountryCode__c= 'India: 0091';
        Diginq.SMS_MSG_Id__c = '9876543210';
        Diginq.Record_Type_Id__c = '0120Y000000FNkA';
        insert Diginq;
        
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/SMSResponse';  
        req.httpMethod = 'GET';
        req.params.put('InquiryId', inq.id);
        req.params.put('qMsgRef', '9876543210');
        req.params.put('qString', inq.Id);
        req.params.put('qSubmitDate', '1802191559');
        req.params.put('qDoneDate', '1802191559');
        req.params.put('qStatus', 'Testing');
        RestContext.request = req;
        RestContext.response = res;           
        Damac_SecureSMSCallback obj = new Damac_SecureSMSCallback ();
        Damac_SecureSMSCallback.SMSResponse();
        Damac_sendSMS.sendSecureMessage (new Set <ID> {inq.id},'Stand_inquiry__c');
        Damac_SendSMS.sendSecureMessageSync (new Set <ID> {inq.id}, true, 'Stand_Inquiry__c', '');    
        Damac_sendSMS.sendSecureMessage (new Set <ID> {Diginq.id},'Digital_inquiry__c');
        Damac_SecureSMSCallback.findObjectNameFromRecordIdPrefix('500');
        
    }
}