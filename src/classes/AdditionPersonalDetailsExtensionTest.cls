/***********************************************************************************************
 * @Name              : AdditionPersonalDetailsExtensionTest
 * @Description       : Test class developed for 'AdditionPersonalDetailsExtension'
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log
 * 1.0        20/11/17        Lochan            Initial Draft
 * 1.1        28/02/19        Arjun Khatri      Tested masking of mobile numbers.
***********************************************************************************************/
@isTest(SeeAllData=false)
public class AdditionPersonalDetailsExtensionTest {
	public static final String CALL_NOW_BUTTON_SET_TO_FALSE= 'The value should be true when the'
		+' Dispute Flag has values D,E,L,C & BC and Profiles must contain FM/Property or if it is a'
		+' Collection Profile';

	static testMethod void testMethod1() {
		// Insert Accont
		Account objAcc = TestDataFactory_CRM.createPersonAccount();
		objAcc.Mobile_Phone_Encrypt__pc = '123123123';
		objAcc.Mobile_Phone_Encrypt_2__pc = '123123123';
		objAcc.Mobile_Phone_Encrypt_3__pc = '123123123';
		objAcc.Mobile_Phone_Encrypt_4__pc = '123123123';
		objAcc.Mobile_Phone_Encrypt_5__pc = '123123123';
		objAcc.Telephone__c = '123123123';
		insert objAcc ;

		//Insert Service Request
		NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
		objSR.Agency__c = objAcc.Id ;
		insert objSR ;

		Id dealId = objSR.Deal_ID__c;
		//Insert Bookings
		List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id,1 );
		insert lstBookings ;  
		 
		//Insert Booking Units
		List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings,1 );
		insert lstBookingUnits;
		
		PageReference pageRef = Page.AdditionPersonalDetails;
		pageRef.getParameters().put('id', String.valueOf(objAcc.Id));
		Test.setCurrentPage(pageRef);

		ApexPages.standardController controller = new ApexPages.standardController(objAcc);
		AdditionPersonalDetailsExtension obj = new AdditionPersonalDetailsExtension(controller);
	}

	static testMethod void testMethod2() {
		  UserRole userRole = [Select Id, Name From UserRole Where Name = 'Contact Center - Manager' limit 1];
		  User user = new User();
		  user.ProfileID = [Select Id From Profile Where Name='System Administrator'].id;
		  user.EmailEncodingKey = 'ISO-8859-1';
		  user.LanguageLocaleKey = 'en_US';
		  user.TimeZoneSidKey = 'America/New_York';
		  user.LocaleSidKey = 'en_US';
		  user.FirstName = 'first';
		  user.LastName = 'last';
		  user.Username = 'test126097@appirio.com';   
		  user.CommunityNickname = 'testUser123';
		  user.Alias = 't1';
		  user.Email = 'no@email.com';
		  user.IsActive = true;
		  user.userRoleId = userRole.Id; 
		 // user.ContactId = c.Id;
		 
		  insert user;
		  
		System.runAs(user) { 
		Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
		Account objAcc = new Account(RecordTypeId = personAccRTId
									, Name = 'Test Name' 
									, First_Name__c='Test FirstName'
									, Last_Name__c='Test LastName'
									, Type='Other'
									, Mobile__c = '123123123');
		insert objAcc;
		
		//Insert Service Request
		NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
		objSR.Agency__c = objAcc.Id ;
		insert objSR ; 

		Id dealId = objSR.Deal_ID__c;
		//Insert Bookings 
		List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id,1 );
		insert lstBookings ;  
		 
		//Insert Booking Units
		List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings,1 );
		for(Booking_Unit__c BU : lstBookingUnits) {
			//BU.Registration_ID__c = '70524';
			BU.Requested_Price__c = 10;
		}
		insert lstBookingUnits;
			
		Contact objCon = new Contact(Lastname='Test Lastname',AccountId = objAcc.Id);
		objCon.Mobile_Phone__c = 'India: 0091 121323';
		objCon.Mobile_Phone_2__c = '1213231111';
		objCon.Mobile_Phone_3__c = '1213231111';
		objCon.Mobile_Phone_4__c = '1213231111';
		objCon.Mobile_Phone_5__c = '1213231111';
		objCon.Mobile_Country_Code_2__c = 'India: 0091';
		objCon.Mobile_Country_Code__c = 'India: 0091'; 
		insert objCon;
  
		Id p = [select id from profile where name='Customer Community Login User'].id;
		
		
		ContentDocumentLink cl;
		User user1;
		user1 = new User(alias = 'test123', email='test123@noemail.com',
				emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
				localesidkey='en_US', profileid = p, country='United States',IsActive =true,
				ContactId = objCon.Id , 
				timezonesidkey='America/Los_Angeles', username='tester79gfhjtfjht566@noemail.com');
			insert user1;
		
		Customer_Category__c customer = new Customer_Category__c(Name='Test',Price_of_Category__c=11);
		insert customer;

		ContentVersion cv = new ContentVersion(
			Title = 'Penguins',
			PathOnClient = 'Penguins.jpg',
			VersionData = Blob.valueOf('Test Content'),
			IsMajorVersion = true
		);
		insert cv;

		ContentVersion cv1 = [select ContentDocumentId from ContentVersion where Id =: cv.Id];
		cl = new ContentDocumentLink(LinkedEntityId = objAcc.Id, ContentDocumentId = cv1.ContentDocumentId, ShareType = 'I');
		insert cl;  
		test.startTest();
		ConnectApi.Photo photo = AdditionPersonalDetailsExtension.setPhotoFromVersion(cl.Id,user1.Id);

		PageReference pageRef = Page.AdditionPersonalDetails;
		pageRef.getParameters().put('id', String.valueOf(objAcc.Id));
		Test.setCurrentPage(pageRef);
		Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT());

		ApexPages.standardController controller = new ApexPages.standardController(objAcc);
		AdditionPersonalDetailsExtension obj = new AdditionPersonalDetailsExtension(controller);
		test.stopTest();

		}
	   }

	/************************************************************************
	* @Description : Test method to verify if the displayCallButton is set to
	*                true when the user profile belongs to Collections and
	*                DELC on Account field value is true.
	* @Params      : None
	* @Return      : None
	*************************************************************************/
	@isTest
	public static void collectionCREProfileTest(){
		UserRole userRole = [Select Id
								  , Name
							   FROM UserRole
							  WHERE Name = 'Collection - CRE'
							  LIMIT 1];
		User user = new User();
		user.ProfileID = [SELECT Id
							FROM Profile
						   WHERE Name='Collection - CRE'].id;
		user.EmailEncodingKey = 'ISO-8859-1';
		user.LanguageLocaleKey = 'en_US';
		user.TimeZoneSidKey = 'America/New_York';
		user.LocaleSidKey = 'en_US';
		user.FirstName = 'first';
		user.LastName = 'last';
		user.Username = 'test126097@appirio.com';
		user.CommunityNickname = 'testUser123';
		user.Alias = 't1';
		user.Email = 'no@email.com';
		user.IsActive = true;
		user.userRoleId = userRole.Id;
		insert user;

		System.runAs(user) {

			Id personAccRTId =
				Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
				Account objAcc = new Account(RecordTypeId = personAccRTId
				, FirstName = 'Test FirstName'
				, LastName = 'Test LastName'
				, Units_under_DELC__c = true);
			insert objAcc;

			//Insert Service Request
			NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
			objSR.Agency__c = objAcc.Id ;
			insert objSR ;

			Id dealId = objSR.Deal_ID__c;
			//Insert Bookings
			List<Booking__c> lstBookings =
				TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id,1);
			insert lstBookings ;

			//Insert Booking Units
			List<Booking_Unit__c> lstBookingUnits =
				TestDataFactory_CRM.createBookingUnits(lstBookings,1);
			for(Booking_Unit__c  buObj :lstBookingUnits){
				buObj.Dispute_Flag__c = 'BC';
			}

			insert lstBookingUnits;

			PageReference pageRef = Page.AdditionPersonalDetails;
			pageRef.getParameters().put('id', String.valueOf(objAcc.Id));
			Test.setCurrentPage(pageRef);
			ApexPages.standardController controller = new ApexPages.standardController(objAcc);
			AdditionPersonalDetailsExtension obj = new AdditionPersonalDetailsExtension(controller);

			System.AssertEquals(true, obj.displayCallButton, CALL_NOW_BUTTON_SET_TO_FALSE);
		}
	}

	/************************************************************************
	* @Description : Test method to verify if the displayCallButton is set to
	*                true when the user profile belongs to Collections and
	*                DELC on Account field value is true.
	* @Params      : None
	* @Return      : None
	*************************************************************************/
	@isTest
	public static void collectionManagerProfileTest(){
		UserRole userRole = [Select Id
								  , Name
							   FROM UserRole
							  WHERE Name = 'Collection - Manager'
							  LIMIT 1];
		User user = new User();
		user.ProfileID = [SELECT Id
							FROM Profile
						   WHERE Name='Collection - Manager'].id;
		user.EmailEncodingKey = 'ISO-8859-1';
		user.LanguageLocaleKey = 'en_US';
		user.TimeZoneSidKey = 'America/New_York';
		user.LocaleSidKey = 'en_US';
		user.FirstName = 'first';
		user.LastName = 'last';
		user.Username = 'test126097@appirio.com';
		user.CommunityNickname = 'testUser123';
		user.Alias = 't1';
		user.Email = 'no@email.com';
		user.IsActive = true;
		user.userRoleId = userRole.Id;
		insert user;

		System.runAs(user) {
			Id personAccRTId =
				Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
				Account objAcc = new Account(RecordTypeId = personAccRTId
											, FirstName = 'Test FirstName'
											, LastName = 'Test LastName'
											, Units_under_DELC__c = true);
			insert objAcc;

			//Insert Service Request
			NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
			objSR.Agency__c = objAcc.Id ;
			insert objSR ;

			Id dealId = objSR.Deal_ID__c;
			//Insert Bookings
			List<Booking__c> lstBookings =
				TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id,1);
			insert lstBookings ;

			//Insert Booking Units
			List<Booking_Unit__c> lstBookingUnits =
				TestDataFactory_CRM.createBookingUnits(lstBookings,1);
			for(Booking_Unit__c  buObj :lstBookingUnits){
				buObj.Dispute_Flag__c = 'BC';
			}

			insert lstBookingUnits;

			PageReference pageRef = Page.AdditionPersonalDetails;
			pageRef.getParameters().put('id', String.valueOf(objAcc.Id));
			Test.setCurrentPage(pageRef);
			ApexPages.standardController controller = new ApexPages.standardController(objAcc);
			AdditionPersonalDetailsExtension obj = new AdditionPersonalDetailsExtension(controller);

			System.AssertEquals(true, obj.displayCallButton, CALL_NOW_BUTTON_SET_TO_FALSE);
		}
	}

	/************************************************************************
	* @Description : Test method to verify if the displayCallButton is set to
	*                true when the user profile contains 'FM' and
	*                DELC on Account field value is true.
	* @Params      : None
	* @Return      : None
	*************************************************************************/
	@isTest
	public static void fMProfileTest(){
		UserRole userRole = [Select Id
								  , Name
							   FROM UserRole
							  WHERE Name = 'FM Director'
							  LIMIT 1];
		User user = new User();
		user.ProfileID = [SELECT Id
							FROM Profile
						   WHERE Name='FM Director'].id;
		user.EmailEncodingKey = 'ISO-8859-1';
		user.LanguageLocaleKey = 'en_US';
		user.TimeZoneSidKey = 'America/New_York';
		user.LocaleSidKey = 'en_US';
		user.FirstName = 'first';
		user.LastName = 'last';
		user.Username = 'test126097@appirio.com';
		user.CommunityNickname = 'testUser123';
		user.Alias = 't1';
		user.Email = 'no@email.com';
		user.IsActive = true;
		user.userRoleId = userRole.Id;
		insert user;

		System.runAs(user) {

			Id personAccRTId =
				Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
				Account objAcc = new Account(RecordTypeId = personAccRTId
											, FirstName = 'Test FirstName'
											, LastName = 'Test LastName'
											, Units_under_DELC__c = true);
			insert objAcc;

			//Insert Service Request
			NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
			objSR.Agency__c = objAcc.Id ;
			insert objSR ;

			Id dealId = objSR.Deal_ID__c;
			//Insert Bookings
			List<Booking__c> lstBookings =
				TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id,1);
			insert lstBookings ;

			//Insert Booking Units
			List<Booking_Unit__c> lstBookingUnits =
				TestDataFactory_CRM.createBookingUnits(lstBookings,1);
			for(Booking_Unit__c  buObj :lstBookingUnits){
				buObj.Dispute_Flag__c = 'BC';
			}

			insert lstBookingUnits;

			PageReference pageRef = Page.AdditionPersonalDetails;
			pageRef.getParameters().put('id', String.valueOf(objAcc.Id));
			Test.setCurrentPage(pageRef);
			ApexPages.standardController controller = new ApexPages.standardController(objAcc);
			AdditionPersonalDetailsExtension obj = new AdditionPersonalDetailsExtension(controller);

			System.AssertEquals(true, obj.displayCallButton, CALL_NOW_BUTTON_SET_TO_FALSE);
		}
	}

	/************************************************************************
	* @Description : Test method to verify if the displayCallButton is set to
	*                true when the user profile belongs to Collections and
	*                DELC on Account field value is False.
	* @Params      : None
	* @Return      : None
	*************************************************************************/
	@isTest
	public static void unitsUnderDELCFalseTest(){
		UserRole userRole = [Select Id
								  , Name
							   FROM UserRole
							  WHERE Name = 'Collection - Manager'
							  LIMIT 1];
		User user = new User();
		user.ProfileID = [SELECT Id
							FROM Profile
						   WHERE Name='Collection - Manager'].id;
		user.EmailEncodingKey = 'ISO-8859-1';
		user.LanguageLocaleKey = 'en_US';
		user.TimeZoneSidKey = 'America/New_York';
		user.LocaleSidKey = 'en_US';
		user.FirstName = 'first';
		user.LastName = 'last';
		user.Username = 'test126097@appirio.com';
		user.CommunityNickname = 'testUser123';
		user.Alias = 't1';
		user.Email = 'no@email.com';
		user.IsActive = true;
		user.userRoleId = userRole.Id;
		insert user;

		System.runAs(user) {

			Id personAccRTId =
				Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
				Account objAcc = new Account(RecordTypeId = personAccRTId
											, FirstName = 'Test FirstName'
											, LastName = 'Test LastName'
											, Units_under_DELC__c = false);
			insert objAcc;

			//Insert Service Request
			NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
			objSR.Agency__c = objAcc.Id ;
			insert objSR ;

			Id dealId = objSR.Deal_ID__c;
			//Insert Bookings
			List<Booking__c> lstBookings =
				TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id,1);
			insert lstBookings ;

			//Insert Booking Units
			List<Booking_Unit__c> lstBookingUnits =
				TestDataFactory_CRM.createBookingUnits(lstBookings,1);
			insert lstBookingUnits;

			PageReference pageRef = Page.AdditionPersonalDetails;
			pageRef.getParameters().put('id', String.valueOf(objAcc.Id));
			Test.setCurrentPage(pageRef);
			ApexPages.standardController controller = new ApexPages.standardController(objAcc);
			AdditionPersonalDetailsExtension obj = new AdditionPersonalDetailsExtension(controller);

			System.AssertEquals(true, obj.displayCallButton, CALL_NOW_BUTTON_SET_TO_FALSE);
		}
	}

	/************************************************************************
	* @Description : Test method to verify if the displayCallButton is set to
	*                true when the user profile is not allowed and
	*                DELC on Account field value is true.
	* @Params      : None
	* @Return      : None
	*************************************************************************/
	@isTest
	public static void otherProfilesTest(){
		User user = new User();
		user.ProfileID = [SELECT Id
							FROM Profile
						   WHERE Name='Ameyo CTI'].id;
		user.EmailEncodingKey = 'ISO-8859-1';
		user.LanguageLocaleKey = 'en_US';
		user.TimeZoneSidKey = 'America/New_York';
		user.LocaleSidKey = 'en_US';
		user.FirstName = 'first';
		user.LastName = 'last';
		user.Username = 'test126097@appirio.com';
		user.CommunityNickname = 'testUser123';
		user.Alias = 't1';
		user.Email = 'no@email.com';
		user.IsActive = true;
		insert user;

		System.runAs(user) {
			Id personAccRTId =
				Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
				Account objAcc = new Account(RecordTypeId = personAccRTId
											, FirstName = 'Test FirstName'
											, LastName = 'Test LastName'
											, Units_under_DELC__c = true);
			insert objAcc;

			//Insert Service Request
			NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
			objSR.Agency__c = objAcc.Id ;
			insert objSR ;

			Id dealId = objSR.Deal_ID__c;
			//Insert Bookings
			List<Booking__c> lstBookings =
				TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id,1);
			insert lstBookings ;

			//Insert Booking Units
			List<Booking_Unit__c> lstBookingUnits =
				TestDataFactory_CRM.createBookingUnits(lstBookings,1);
			insert lstBookingUnits;

			PageReference pageRef = Page.AdditionPersonalDetails;
			pageRef.getParameters().put('id', String.valueOf(objAcc.Id));
			Test.setCurrentPage(pageRef);
			ApexPages.standardController controller = new ApexPages.standardController(objAcc);
			AdditionPersonalDetailsExtension obj = new AdditionPersonalDetailsExtension(controller);

			System.AssertEquals(false, obj.displayCallButton, CALL_NOW_BUTTON_SET_TO_FALSE);
		}
	}

	/************************************************************************
	* @Description : Test method to verify if the displayCallButton is set to
	*                true when the user profile contains FM and
	*                DELC on Account field value is False.
	* @Params      : None
	* @Return      : None
	*************************************************************************/
	@isTest
	public static void containsFMTest(){
		User user = new User();
		user.ProfileID = [SELECT Id
							FROM Profile
						   WHERE Name='FM Collections'].id;
		user.EmailEncodingKey = 'ISO-8859-1';
		user.LanguageLocaleKey = 'en_US';
		user.TimeZoneSidKey = 'America/New_York';
		user.LocaleSidKey = 'en_US';
		user.FirstName = 'first';
		user.LastName = 'last';
		user.Username = 'test126097@appirio.com';
		user.CommunityNickname = 'testUser123';
		user.Alias = 't1';
		user.Email = 'no@email.com';
		user.IsActive = true;
		insert user;

		System.runAs(user) {
			Id personAccRTId =
				Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
				Account objAcc = new Account(RecordTypeId = personAccRTId
											, FirstName = 'Test FirstName'
											, LastName = 'Test LastName'
											, Units_under_DELC__c = true);
			insert objAcc;

			//Insert Service Request
			NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
			objSR.Agency__c = objAcc.Id ;
			insert objSR ;

			Id dealId = objSR.Deal_ID__c;
			//Insert Bookings
			List<Booking__c> lstBookings =
				TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id,1);
			insert lstBookings ;

			//Insert Booking Units
			List<Booking_Unit__c> lstBookingUnits =
				TestDataFactory_CRM.createBookingUnits(lstBookings,1);
			insert lstBookingUnits;

			PageReference pageRef = Page.AdditionPersonalDetails;
			pageRef.getParameters().put('id', String.valueOf(objAcc.Id));
			Test.setCurrentPage(pageRef);
			ApexPages.standardController controller = new ApexPages.standardController(objAcc);
			AdditionPersonalDetailsExtension obj = new AdditionPersonalDetailsExtension(controller);

			System.AssertEquals(true, obj.displayCallButton, CALL_NOW_BUTTON_SET_TO_FALSE);
		}
	}
}