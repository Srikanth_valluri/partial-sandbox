//#TODO replace cx name in email and sms
//#TODO check for cl lookup in emailmessage and sms history object
public with sharing class WellcomeCallNotification {
    public static Id welcomeCallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get(
                                                    'Welcome Calling List').RecordTypeId;
    
    @InvocableMethod
    public Static void getCallingLst( List<Id> lstClId ) {
        if( lstClId != null && lstClId.size() > 0 ) {
            Set<Id> setClId = new Set<Id>();
             System.debug('lstClId = ' + lstClId);
            for( Calling_List__c objCl : [ 
                                    SELECT Id,
                                        Welcome_Call_Outcome__c
                                      FROM Calling_List__c
                                     WHERE Id IN : lstClId And Welcome_Call_Outcome__c != NULL
                                     AND RecordTypeId =: welcomeCallingRecordTypeId
                                     //#TODO add record type check
            ] ) {
                if(objCl.Welcome_Call_Outcome__c.containsIgnoreCase('Invalid contact details')
                    || objCl.Welcome_Call_Outcome__c.containsIgnoreCase('Unreachable ')
                    || objCl.Welcome_Call_Outcome__c.containsIgnoreCase('In Progress') 
                ) {
                     
                     setClId.add(objCl.Id);

                }
            }
            
            if( setClId != null && setClId.size() > 0 ) {
                executeNotificationLogic( setClId );
            }
        }
    }


    public Static void executeNotificationLogic( Set<Id> setClId ) {
         System.debug('setClId = ' + setClId);
        if( setClId != null && setClId.size() > 0 ) {
            sendEmail( setClId );
            sendSMS( setClId );
            whatsapp( setClId );
        }
    }

    
//Send Email function********AK:uncommented on 14 jan 21******************//
    
    @Future(callout=true)
    public static void sendEmail( Set<Id> setClId ) {
         System.debug('setClIdsetClId = ' + setClId);
        List<Attachment> lstAttach = new List<Attachment>();
        //#TODO move below soql to sepearte method
        Calling_List__c objCl = queryCallingList( setClId );

        EmailTemplate emailTemplateObj = queryEmailTemplate('Welcome calling list Email') ;

        if(objCl != NULL && emailTemplateObj != NULL 
            && objCl.Account__r.Person_Business_Email__c != ''){
            
            String toAddress = '', fromAddress,replyToAddress = '', contentType, contentValue,contentBody = '';
            String subject, strAccountId,customerName,  bccAddress ='', strCCAddress;
            
            strAccountId = objCl.Account__c;
            
            toAddress = objCl.Account__r.Person_Business_Email__c;
            
            System.debug('toAddress = ' + toAddress);

            fromAddress = 'atyourservice@damacproperties.com'; //#TODO Confirm with client
            bccAddress = Label.Sf_Copy_Bcc_Mail_Id;
            contentType = 'text/html';
            strCCAddress = '';
            system.debug(' emailTemplateObj : '+ emailTemplateObj );
        
            if(emailTemplateObj.Subject != NULL){
                subject = emailTemplateObj.Subject;
            }
            
            if(emailTemplateObj.body != NULL){
                    contentBody =  emailTemplateObj.body;
            }
            
            if(emailTemplateObj.htmlValue != NULL){
                contentValue = emailTemplateObj.htmlValue;
            }
            
            
            customerName = objCl.Account__r.Name != null ? objCl.Account__r.Name : '';
            contentBody = String.isNotBlank(customerName)?contentBody.replace('{CustomerName}', customerName):contentBody.replace('{CustomerName}', '');
            contentValue = String.isNotBlank(customerName)?contentValue.replace('{CustomerName}', customerName):contentBody.replace('{CustomerName}', '');


            String strUnitName = objCl.Booking_Unit__r.Name != null ? objCl.Booking_Unit__r.Name : '';
            contentBody = String.isNotBlank(strUnitName)?contentBody.replace('{UnitName}', strUnitName):contentBody.replace('{UnitName}', '');
            contentValue = String.isNotBlank(strUnitName)?contentValue.replace('{UnitName}', strUnitName):contentBody.replace('{UnitName}', '');

            String strProperty = objCl.Booking_Unit__r.Property_Name__c != null ? objCl.Booking_Unit__r.Property_Name__c : '';
            contentBody = String.isNotBlank(strProperty)?contentBody.replace('{Property}', strProperty):contentBody.replace('{Property}', '');
            contentValue = String.isNotBlank(strProperty)?contentValue.replace('{Property}', strProperty):contentBody.replace('{Property}', strProperty);

            // Callout to sendgrid to send an email
                SendGridEmailService.SendGridResponse objSendGridResponse =
                    SendGridEmailService.sendEmailService(
                        toAddress
                        , ''
                        , strCCAddress
                        , ''
                        , bccAddress
                        , ''
                        , subject
                        , ''
                        , fromAddress
                        , ''
                        , replyToAddress
                        , ''
                        , contentType
                        , contentValue
                        , ''
                        , lstAttach
                    );

            System.debug('objSendGridResponse == ' + objSendGridResponse);
            //Create Email Activity
            //#TODO check for CL lookup on EmailMessage
            String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;
            if (responseStatus == 'Accepted') {
                EmailMessage mail = new EmailMessage();
                mail.Subject = subject;
                mail.MessageDate = System.Today();
                mail.Status = '3';//'Sent';
                mail.RelatedToId = strAccountId;
                mail.Account__c  = strAccountId;
                mail.Type__c = 'Wellcome Calling LIst email';
                mail.ToAddress = toAddress;
                mail.FromAddress = fromAddress;
                mail.TextBody = contentBody; //contentValue.replaceAll('\\<.*?\\>', '');
                mail.Sent_By_Sendgrid__c = true;
                mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                mail.CcAddress = strCCAddress;
                mail.BccAddress = bccAddress;
                insert mail;
                system.debug('Mail obj == ' + mail);
            }//End Email Activity Creation
                
        }   //End emailTemplateObj IF
    }
    
    
    
    @Future(callout=true)
    public static void sendSMS( Set<Id> setClId ) {

        Calling_List__c objCl = queryCallingList( setClId );

        EmailTemplate emailTemplateObj = queryEmailTemplate('Welcome calling list SMS') ;
        
        if(objCl != NULL && emailTemplateObj != NULL 
            && (objCl.Account__r.Mobile_Phone_Encrypt__pc != null || objCl.Account__r.Mobile__c != null ) ){
                String strPhoneNumber  = '', contentValue,contentBody = '';
            
            String user,passwd,strSID , strAccountId ,customerName ,dueAmount,UnitName ;
            
            strAccountId = objCl.Account__c;
            
            System.debug('objCl.Account.isPersonAccount = ' + objCl.Account__r.isPersonAccount);
            System.debug('objCl.Account.Mobile_Phone_Encrypt__pc = ' + objCl.Account__r.Mobile_Phone_Encrypt__pc);
            System.debug('objCl.Account.Mobile__c = ' + objCl.Account__r.Mobile__c);

            if(emailTemplateObj.body != NULL){
                    contentBody =  emailTemplateObj.body;
            }
            if(emailTemplateObj.htmlValue != NULL){
                contentValue = emailTemplateObj.htmlValue;
            }

            // If Person Acount
            if( objCl.Account__r.isPersonAccount
            && objCl.Account__r.Mobile_Phone_Encrypt__pc != null ) {
                strPhoneNumber = objCl.Account__r.Mobile_Phone_Encrypt__pc;
            }

            //If business account
            else if( objCl.Account__r.Mobile__c != null ) {
                strPhoneNumber = objCl.Account__r.Mobile__c;
            }
            
            if(strPhoneNumber.startsWith('00')) {
                strPhoneNumber = strPhoneNumber.removeStart('00');
            }
            else if(strPhoneNumber.startsWith('0')) {
                strPhoneNumber = strPhoneNumber.removeStart('0');
            }
            System.debug('strPhoneNumber = ' + strPhoneNumber);
            System.debug('emailTemplateObj = ' + emailTemplateObj);

            

            customerName = objCl.Account__r.Name != null ? objCl.Account__r.Name : '';
            contentBody = String.isNotBlank(customerName)?contentBody.replace('{CustomerName}', customerName):contentBody.replace('{CustomerName}', '');
            contentValue = String.isNotBlank(customerName)?contentValue.replace('{CustomerName}', customerName):contentBody.replace('{CustomerName}', '');


            String strUnitName = objCl.Booking_Unit__r.Name != null ? objCl.Booking_Unit__r.Name : '';
            contentBody = String.isNotBlank(strUnitName)?contentBody.replace('{UnitName}', strUnitName):contentBody.replace('{UnitName}', '');
            contentValue = String.isNotBlank(strUnitName)?contentValue.replace('{UnitName}', strUnitName):contentBody.replace('{UnitName}', '');

            String strProperty = objCl.Booking_Unit__r.Property_Name__c != null ? objCl.Booking_Unit__r.Property_Name__c : '';
            contentBody = String.isNotBlank(strProperty)?contentBody.replace('{Property}', strProperty):contentBody.replace('{Property}', '');
            contentValue = String.isNotBlank(strProperty)?contentValue.replace('{Property}', strProperty):contentBody.replace('{Property}', strProperty);

            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http http = new Http();
            user = Label.Damac_CRM_SMS_Service_User_name;//'damaccrm';
            passwd = Label.Damac_CRM_SMS_Service_Password;//'D@$al3sF0rc387!';
            strSID = Label.Damac_CRM_SMS_Service_Sender_Id;//'DAMAC GRP';

            req.setMethod('POST' ); // Method Type
            req.setEndpoint('http://api.smscountry.com/SMSCwebservice_bulk.aspx'); 

            string msgCont = GenericUtility.encodeChar(contentBody);
            System.debug('msgCont:'+msgCont);
            req.setBody('user='+ user + '&passwd=' + passwd +'&message=' + msgCont + '&mobilenumber=' + strPhoneNumber + '&sid='+strSID+ '&MTYPE=LNG&DR=Y'); // Request Parameters
            
            
            
            try {
                system.debug('Request Body---'+req.getBody());
                system.debug('Request Header---'+req.getHeader('Authorization'));
                system.debug('Request Header---'+req.getHeader('Content-Type'));
                res = http.send(req);
                system.debug('Response---'+res.getBody());
                if( res.getBody() != null ) {

                    if( String.valueOf( res.getBody() ).contains('OK:') ) {
                        system.debug('Message Send Sucessfully');
                        SMS_History__c smsObj = new SMS_History__c();
                        smsObj.Message__c = contentBody;
                        smsObj.Phone_Number__c = strPhoneNumber;
                        if( strAccountId !='' ) {
                            smsObj.Customer__c = strAccountId;
                        }   
                        smsObj.Calling_List__c = objCl.Id;     
                        smsObj.Is_SMS_Sent__c = true;
                        smsObj.isFMSMS__c = false;
                        smsObj.sms_Id__c = res.getBody().substringAfter('OK:');
                        smsObj.Description__c = res.getBody();
                        System.debug('smsObj::::if:'+smsObj);
                        insert smsObj; 
                    }
                    else{
                        //if failure
                        SMS_History__c smsObj = new SMS_History__c();
                        smsObj.Message__c = contentBody;
                        smsObj.Phone_Number__c = strPhoneNumber;
                        if( strAccountId !='' ) {
                            smsObj.Customer__c = strAccountId;
                        }
                        smsObj.isFMSMS__c = false;
                        smsObj.Description__c = res.getBody();
                        System.debug('smsObj::::else:'+smsObj);
                        insert smsObj;
                    }
                }
            } catch(Exception e) {
                System.debug('Error***** '+ e);
            }
        }
    }


     @Future(callout=true)
    public static void whatsapp( Set<Id> setClId ) {
        
            
            
            String toMobNum, fromMob,contents ;
        map<string,string> credentials = CRMNexmoWhatsAppController.getCredentials();
        
        Calling_List__c objCl = queryCallingList( setClId );

        // If Person Acount
        if( objCl.Account__r.isPersonAccount
        && objCl.Account__r.Mobile_Phone_Encrypt__pc != null ) {
            toMobNum = objCl.Account__r.Mobile_Phone_Encrypt__pc;
        }

        //If business account
        else if( objCl.Account__r.Mobile__c != null ) {
            toMobNum = objCl.Account__r.Mobile__c;
        }
        
        if(toMobNum.startsWith('00')) {
            toMobNum = toMobNum.removeStart('00');
        }
        else if(toMobNum.startsWith('0')) {
            toMobNum = toMobNum.removeStart('0');
        }

        fromMob = credentials.get('whatsAppnumber');
        //#TODO ask from client for template
        contents ='Test whatsapp ';

        //allTemplates = [select id,DeveloperName,MasterLabel,Template_Preview__c FROM CRM_Nexmo_Templates__mdt];
        String reqBody = '{ "to": { "type": "whatsapp", "number": "'
            + toMobNum
            + '" },"from": { "type": "whatsapp", "number": "'
            + fromMob 
            + '" },"message": {'
            + '"content": {"type": "text", "text": "' + contents + '"}}}';



        PageReference pageRef = Page.CRMNexmoWhatsApp;
            //'0012500001DMyZa'
            //pageRef.getParameters().put('id', String.valueOf(objCl.Account__c));
            //pageRef.getParameters().put('id', '0012500001DMyZa');
            
            //Test.setCurrentPage(pageRef);
            //System.debug('pageRef'+pageRef); 

            PageReference pagr= new PageReference('/apex/CRMNexmoWhatsApp?id='+'0012500001DMyZa');          
            pagr.setRedirect(false);

            String mataDataLabel = Label.MetaData_Label_for_Whatsapp_Welcome_Calling;
            CRM_Nexmo_Templates__mdt templateMtd;
            if(String.isNotBlank(mataDataLabel)){
                 templateMtd = [select id,developerName,MasterLabel,
                Template_Preview__c,Fallback_Locale__c,Template_Namespace__c,Template_Parameters__c 
                from CRM_Nexmo_Templates__mdt where MasterLabel=:mataDataLabel LIMIT 1];
            }
            

            if( templateMtd != NULL){
                CRMNexmoWhatsAppController controller = new CRMNexmoWhatsAppController(objCl.Account__c);
                controller.selectedTemplate = templateMtd.developerName;
                controller.selectedWhatsAppNumber = toMobNum;
                if(!Test.isRunningTest()){
                    controller.sendMessageWithMTM();
                }
            }
            
            

        /*if(credentials != null && credentials.size() > 0){ 
            
            HttpRequest req = new HttpRequest();            
            req.setEndpoint(credentials.get('endpoint'));
            req.setHeader('Authorization','Basic '+credentials.get('authHeader'));
            
            req.setBody(reqBody);
            req.setHeader('Content-Type','application/json');
            req.setMethod('POST');        
            Http http = new Http();
            HTTPResponse res;
            if(!test.isRunningTest()){
                res = http.send(req);
                System.debug(req);
                System.debug(res.getBody());
                System.debug(res.getstatuscode());
                if( res.getstatuscode() == 200 ||  res.getstatuscode() == 202 ){
                    NEXMO_MESSGAE_RESPONSE resp = parse(string.valueof(res.getBody()));
                    system.debug('message_uuid>>>>'+resp.message_uuid);
                    
                }
            }else{
                NEXMO_MESSGAE_RESPONSE resp = parse(string.valueof('{"message_uuid":"54af4397-0ea7-48c5-bc9a-d7551bfb2bfb"}'));                
            }
        }*/
    }  
    //method to query calling list 
    public static Calling_List__c queryCallingList(Set<Id> setClId) {
        Calling_List__c objCl = new Calling_List__c();
        if( setClId != null && setClId.size() > 0 ) {
        objCl = [SELECT Id
                        , Welcome_Call_Outcome__c
                        , Account__r.Person_Business_Email__c  
                        , Account__r.Name
                        , Account__r.Mobile_Phone_Encrypt__pc
                        , Account__r.Mobile__c
                        , Account__c
                        , Account__r.isPersonAccount
                        , Booking_Unit__r.Name
                        , Booking_Unit__r.Property_Name__c
                FROM Calling_List__c 
                WHERE Id IN : setClId Limit 1 
            ];
        }
        return objCl;
    }
    // method to query email template
    public static EmailTemplate queryEmailTemplate(String tempName) {
        EmailTemplate emailTemplateObj = new EmailTemplate();
        if( tempName != null && tempName != '' ) {
            emailTemplateObj = [SELECT Id
                                        , Subject
                                        , Body
                                        , Name
                                        , HtmlValue
                                        , TemplateType
                                        , BrandTemplateId
                                        FROM EmailTemplate 
                                        WHERE Name =:tempName 
                                        LIMIT 1 
                                ];

        }
        return emailTemplateObj;
    }
         
    
    

}