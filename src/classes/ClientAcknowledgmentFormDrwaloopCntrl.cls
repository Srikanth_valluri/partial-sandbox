/******************************************************************************************
* Description - Class used to Client Acknowledgment Form through Drawloop.                *
*                                                                                         *
* Version           Date            Author          Description                           *
* 1.0               07/08/2018                      Initial Draft                         *
* 1.1               27/01/2019      Arjun Khatri    1.Added condition of Keys Released LHO*
* 1.2               31/12/2020      Shubham S       1.Added additional 
                                                     Is_Key_Release_Requested__c flag 
                                                     check for client Acknowledgement Form*
*******************************************************************************************/
public class ClientAcknowledgmentFormDrwaloopCntrl {
    public Booking_Unit__c objBu;
    
    public ClientAcknowledgmentFormDrwaloopCntrl(ApexPages.StandardController controller) {
        objBu = (Booking_Unit__c)controller.getrecord();
    }

    /**
     * Method used to execute Drawloop batch and create Offer & Acceptance Letter
     */
    public pageReference generateClientAcknowledgmentLetter() {
        system.debug('<<< in generateClientAcknowledgmentLetter >>> ' );
        system.debug('<<< in objBu  >>> ' +objBu.Id);
        
        List<Booking_Unit__c> lstBU = [SELECT 
                                        Id 
                                        , Is_Client_Acknowledgment_Form_Generated__c
                                        , Okay_to_release_keys__c 
                                        , Keys_Released__c
                                        , Keys_Released_LHO__c
                                        , OK_to_release_Keys_LHO__c 
                                        , Is_Key_Release_Requested__c
                                    FROM
                                        Booking_Unit__c
                                    WHERE 
                                        Id =: objBu.Id LIMIT 1];
        
        //Is_Key_Release_Requested__c - check is additionally added. This flag is marked true from Damac Living App's Digital Handover page - 31-12-2020
        if( lstBU[0].Is_Client_Acknowledgment_Form_Generated__c == false ) {
            if( ( (lstBU[0].Okay_to_release_keys__c == true 
                       && lstBU[0].Keys_Released__c == true )
                  || ( lstBU[0].Keys_Released_LHO__c == true 
                       && lstBU[0].OK_to_release_Keys_LHO__c == true )
                ) && lstBU[0].Is_Key_Release_Requested__c ) {
                if (!Test.isRunningTest()) {
                    Database.executeBatch(
                        new GenerateBUDrawloopDocumentBatch(
                            String.valueOf(lstBU[0].Id)
                            , Label.Client_Acknowledgment_Form_DDP_Template_Id
                            , Label.Client_Acknowledgment_Form_DDP_Delivery_Id )
                    );
                }

                // Redirect to bu detail page
                PageReference buDetailPage = new PageReference ('/'+lstBU[0].Id);
                buDetailPage.setRedirect(true);
                return buDetailPage;
            }else {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,
                'You can not generate Client Acknowledgment Letter, as this unit is not marked as okay to key release & is key release requested'));
                return null;
            }
        }else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,
            'Generation of Client Acknowledgment Letter has already been done for the selected Unit'));
            return null;
        }
    }
    
}