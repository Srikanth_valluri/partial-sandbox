public without sharing class GenerateKRF{
    public boolean blnContinue{get;set;}
    string strPageID;
    Case objCase;
    public String KRFurl;
    public string strPopupMessage {get;set;}
    public GenerateKRF(ApexPages.standardController controller){
       
    }//End constructor
    
    public pagereference init(){
        strPageID = ApexPages.currentPage().getParameters().get('id');
        boolean blnValid = false;
        blnContinue = false;
        strPopupMessage = '';
        objCase = [select Id, AccountId
                        , Booking_Unit__c
                        , Booking_Unit__r.Total_due_Overdue__c
                        , Booking_Unit__r.Amount_Paid__c
                        , Booking_Unit__r.Requested_Price__c
                        , Booking_Unit__r.CurrencyIsoCode
                        , Booking_Unit__r.Registration_Status__c
                        , Booking_Unit__r.Property_City__c
                        , CaseNumber
                        , Parent.CaseNumber
                        , ParentId
                        //, Client_Dewa_No__c 
                        , Booking_Unit__r.Registration_ID__c 
                        , RecordType.DeveloperName
                        , RecordType.Name
                        , Document_Verified__c
                        , Parent.Document_Verified__c
                        , Payment_Verified__c
                        , Total_Major_Snags__c
                        , Total_Snags_Closed__c
                        , Total_Snags_Open__c
                        from Case 
                        where id =:strPageID];
        if((objCase.ParentId != null && objCase.Recordtype.DeveloperName == 'Handover')
        || objCase.Recordtype.DeveloperName == 'Early_Handover'
        /*|| objCase.Recordtype.DeveloperName == 'Plot_Handover'*/){
            if(objCase.Recordtype.DeveloperName == 'Handover'){
                Snag_Validation_Configuration__c CS = Snag_Validation_Configuration__c.getOrgDefaults();
                // objCase.Parent.Document_Verified__c && objCase.Document_Verified__c 
                if(objCase.ParentId != null && objCase.Payment_Verified__c){
                    system.debug('CS.IsActive__c**********'+CS.IsActive__c);
                    if(CS.IsActive__c){
                        if((objCase.Total_Major_Snags__c == null || objCase.Total_Major_Snags__c <= CS.Allowed_Major_Snags__c)
                        && ((objCase.Total_Snags_Closed__c != null && objCase.Total_Snags_Closed__c >=1) 
                        || (objCase.Total_Snags_Open__c != null && objCase.Total_Snags_Open__c >= 1))){
                            //blnValid = isSignedHOChecklistUploaded();
                            blnValid = true;
                        }else{
                             if(objCase.Total_Major_Snags__c >= CS.Allowed_Major_Snags__c){
                                //blnValid = isSignedHOChecklistUploaded();
                                blnValid = true;
                                if(blnValid){
                                    blnContinue = true;
                                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'The selected unit has '+objCase.Total_Major_Snags__c+' major snags. Do you still want to continue?');
                                    ApexPages.addMessage(myMsg);
                                    strPopupMessage = returnHTMLBody('KRF_Message');
                                    system.debug('*****strPopupMessage*****'+strPopupMessage);
                                }else{
                                    blnValid = false;
                                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Please upload Signed Handover Checklist. You will be unable to proceed.');
                                    ApexPages.addMessage(myMsg);
                                }
                             }else if(objCase.Recordtype.DeveloperName == 'Handover'){
                                blnContinue = true;
                                strPopupMessage = returnHTMLBody('KRF_Message');
                                //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'No snaging has been performed for the selected unit. You will be unable to proceed.');
                                //ApexPages.addMessage(myMsg);
                             }
                        }
                    }else{
                        // Snags Validation Inactive
                        //blnValid = isSignedHOChecklistUploaded();
                        blnValid = true;
                        if(!blnValid){
                            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Please upload Signed Handover Checklist. You will be unable to proceed.');
                            ApexPages.addMessage(myMsg);
                        }
                    }
                }else{
                    /*
                    if(objCase.ParentId != null && !objCase.Parent.Document_Verified__c){
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Key Release Form can only be generated after all required documents for the parent Service Request '+objCase.Parent.CaseNumber+' are verified.');
                        ApexPages.addMessage(myMsg);
                    }else if(objCase.ParentId != null && !objCase.Document_Verified__c){
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Key Release Form can only be generated after all documents are verified.');
                        ApexPages.addMessage(myMsg);
                    }else */
                    if(objCase.ParentId != null && !objCase.Payment_Verified__c){
                        //blnValid = true;
                        blnValid = false;
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Payment verification is still pending. Click on Verify Payments button in the case to refresh pending amount if customer has already paid. Check pending amount before trying again.');
                        ApexPages.addMessage(myMsg);
                    }
                    //return null; 
                }
            }else{
                blnValid = true;
            }
            system.debug('*****blnValid*****'+blnValid);
            if(blnValid){
                if(objCase.Recordtype.DeveloperName == 'Handover' && objCase.Booking_Unit__r.Registration_Status__c != 'Agreement Executed by Damac'){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Status does not equal Agreement Executed by Damac');
                    ApexPages.addMessage(myMsg);
                    return null;
                }else{
                    system.debug('*****Everything was valid*****');
                    blnContinue = true;
                    strPopupMessage = returnHTMLBody('KRF_Message');
                    system.debug('*****strPopupMessage*****'+strPopupMessage);
                    //return GenerateKRF_IPMS(objCase);
                    return null;
                }
            }else{
                return null;
            }
        }else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Key Release Form cannot be generated on this Service Request, this Service Request is for Customer document verification. Please go to the unit level Service Requests.');
            ApexPages.addMessage(myMsg);
            return null;
        }
    }//End init
    
    public pageReference continueKRFGeneration(){
        return GenerateKRF_IPMS(objCase);
    }
    
    public pageReference GenerateKRF_IPMS(Case objCase){
        CallHandoverMQServices.HandoverMQServicesResponse objKRF;
        objKRF= CallHandoverMQServices.CallHandoverMQServiceKeyReleaseForm(objCase.Booking_Unit__r.Registration_ID__c);
        KRFurl = objKRF.url;       
        return processResponse(objKRF.P_PROC_MESSAGE);
        /*if( objCase.RecordType.DeveloperName == 'Handover' || objCase.RecordType.DeveloperName == 'Early_Handover' ) {
            
            if( objCase.Booking_Unit__r.Property_City__c == 'Dubai' && String.isBlank(objCase.Client_Dewa_No__c )) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please marked Dewa no. before proceeding to generate Key Release form '));
                return null;
            } else {
            
                if (!Test.isRunningTest()) {
                    Database.executeBatch(new GenerateDrawloopDocumentBatch(String.valueOf(objCase.Id),Label.Key_Release_Authorization_Form_DDP_Template_Id,Label.Key_Release_Authorization_Form_DDP_Delivery_Id ));
                }
        
                return returnToCase();
            }
        }else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Key Release Form can only be generate for Handover or early handover type of SR'));
            return null;
        }*/
    }
    
    public pageReference processResponse(String errorDetails){
        list<SR_Attachments__c> lstCaseAttachment = new list<SR_Attachments__c>();
        list<Error_Log__c> lstErrorLog = new list<Error_Log__c>();
        Booking_Unit__c objBU;
        system.debug('*****KRFurl*****'+KRFurl);
        if(String.isBlank(KRFurl)){
            Error_Log__c objErr = new Error_Log__c();
            objErr.Account__c = objCase.AccountId;
            objErr.Booking_Unit__c = objCase.Booking_Unit__c;
            objErr.Case__c = objCase.id;
            objErr.Error_Details__c = errorDetails;
            objErr.Process_Name__c = objCase.RecordType.Name;
            lstErrorLog.add(objErr);
        }else{
            list<String> lstRegIds = new list<String>();
            system.debug('*****REGID*****'+objCase.Booking_Unit__r.Registration_ID__c);
            lstRegIds.add(objCase.Booking_Unit__r.Registration_ID__c);
            
            if( objCase.Recordtype.DeveloperName == 'Handover' /*|| objCase.Recordtype.DeveloperName == 'Plot_Handover'*/) {
                CallEarlyHandoverMQService1.CallEarlyHandoverMQService1Name('Y','N',lstRegIds);
            }else if( objCase.Recordtype.DeveloperName == 'Early_Handover'){
                CallEarlyHandoverMQService1.CallEarlyHandoverMQService1Name('E','N',lstRegIds);
            }
            
            Id keyId = fetchDocument('Key Release Form');
            Id signedKeyId = fetchDocument('Signed Key Release Form');
            SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
            objCaseAttachment.Case__c = objCase.id;
            objCaseAttachment.Name = objCase.CaseNumber+' Key Release Form ' +system.now();
            objCaseAttachment.Attachment_URL__c = KRFurl;
            objCaseAttachment.Booking_Unit__c = objCase.Booking_Unit__c;
            objCaseAttachment.IsValid__c = true;
            objCaseAttachment.Id = keyId;
            lstCaseAttachment.add(objCaseAttachment);
            
            SR_Attachments__c objCaseAttachment1 = new SR_Attachments__c();
            objCaseAttachment1.Case__c = objCase.id;
            objCaseAttachment1.Name = objCase.CaseNumber+' Signed Key Release Form ' +system.now();
            objCaseAttachment1.Booking_Unit__c = objCase.Booking_Unit__c;
            objCaseAttachment1.Id = signedKeyId;
            lstCaseAttachment.add(objCaseAttachment1);
            
            objBU = new Booking_Unit__c();
            objBU.Id = objCase.Booking_Unit__c;
            if( objCase.Recordtype.DeveloperName == 'Handover' /*|| objCase.Recordtype.DeveloperName == 'Plot_Handover'*/) {
                objBU.Handover_Flag__c = 'Y';
                objBU.Handover_Date__c = date.today();
            }else if( objCase.Recordtype.DeveloperName == 'Early_Handover'){
                objBU.Early_Handover__c = true;
                objBU.Early_Handover_Date__c = date.today();
            }
            objBU.Okay_to_release_keys__c = true;
        }
        system.debug('lstCaseAttachment '+lstCaseAttachment );
        if (lstCaseAttachment != null && lstCaseAttachment.size()>0){
            upsert lstCaseAttachment;
            
            list<Case> lstCase = new list<Case>();
            for (Case objCase1: [Select Id, CaseNumber, AccountId, Handover_Status__c, Booking_Unit__c, Early_Handover_Status__c, 
                RecordTypeId, RecordType.DeveloperName From Case Where Id =: objCase.Id]) {
                if (objCase1.RecordType.DeveloperName == 'Handover') {
                    objCase1.Handover_Status__c = 'Key Release Form Generated';
                } /*else if (objCase1.RecordType.DeveloperName == 'Early_Handover') {
                    objCase1.Early_Handover_Status__c = 'Key Release Form Generated';
                }*/
                lstCase.add(objCase1);
            }
            update lstCase;
            update objBU;
        }
        if (lstErrorLog != null && lstErrorLog.size()>0){
            insert lstErrorLog;
        }
        
         list<Task> lstTask = new list<Task>();
         for (Task objTask: [Select Id, Status, Subject, Process_Name__c 
                             From Task 
                             Where Status != 'Completed'
                             And Process_Name__c  = 'Early Handover'
                             And Subject = 'Verify HO checklist and Generate Key Release form'
                             And WhatId =: strPageID]) {
            objTask.Status = 'Completed';
            lstTask.add(objTask);                                
        }
        if (lstTask != null && lstTask.size() > 0) {
            update lstTask;
        }     
        return returnToCase();
    }
    
    public pageReference returnToCase(){
        pagereference newpg = new Pagereference(URL.getSalesforceBaseUrl().toExternalForm()+'/'+objCase.Id);
        newpg.setRedirect(true);
        system.debug('newpg********'+newpg);
        return newpg;
    }
    
    public Id fetchDocument(String docName){
        docName= '%'+docName+'%';
        list<SR_Attachments__c> lstSR = [Select Id, Name, Case__c 
                                        from SR_Attachments__c 
                                        where Case__c =:objCase.Id
                                        and Name Like :docName limit 1];
        if(lstSR != null && !lstSR.isEmpty()){
            return lstSR[0].Id;
        }else{
            return null;
        }
    }
    
    public string returnHTMLBody(String templateName){
        // 'KRF_Message'
        return [Select e.Name
                     , e.IsActive
                     , e.Id
                     , e.HtmlValue
                     , e.DeveloperName
                     , e.Body 
                From EmailTemplate e 
                where e.DeveloperName =:templateName limit 1][0].HtmlValue;
    } 
    /*
    public boolean isSignedHOChecklistUploaded(){
        Integer intCount = 0;
        for(SR_Attachments__c objSR : [Select Id
                                                , Name
                                                , Case__c
                                                , Attachment_URL__c 
                                                , isValid__c
                                         From SR_Attachments__c
                                         where Case__c =: strPageID
                                         and Name LIKE '%Signed%'
                                         and Attachment_URL__c != null
                                         and isValid__c = true]){
            //if(objSR.Name.contains('Signed Letter of Discharge')){
                //intCount++;
            //}
            
            if(objSR.Name.contains('Signed Handover Checklist')){
                intCount++;
            }
        } // end of for loop
        system.debug('END of intCount *****'+intCount);
        if(intCount >=1){
            return true;
        }else{
            return false;
        }
    } // end of isSignedHOChecklistUploaded
    */
}