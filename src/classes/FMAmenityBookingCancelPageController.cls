public with sharing class FMAmenityBookingCancelPageController {
		public FMAmenityBookingCancelPageController(ApexPages.StandardController sc) {}

		public PageReference cancelAmenityBooking() {
				String FMCaseId = ApexPages.currentPage().getParameters().get('id');
				List<FM_Case__c> lstFMCases = new List<FM_Case__c>();
                lstFMCases = [SELECT Resource_Booking_Deposit__c, Id,
							 		Resource_Booking_Chargeable_Fee__c,
                                    Booking_Unit__c,
                                    Tenant__c,
                                    Admin__c,
                                    Booking_Unit__r.Unit_Name__c,
                                    (SELECT Id from Tasks
                                    WHERE Subject =: Label.FM_CollectFeeTaskLabel
                                    AND (Status = 'Closed' OR Status = 'Completed'))
							FROM FM_Case__c
                            WHERE Id =: FMCaseId];
            if(lstFMCases.size() > 0) {
                try{
                    System.debug('===lstFMCases[0].Tasks.size()=='+lstFMCases[0].Tasks.size());

                    //When deposit/chargeable fee was paid, create task to refund it
    				if(((lstFMCases[0].Resource_Booking_Deposit__c != NULL && lstFMCases[0].Resource_Booking_Deposit__c > 0)
                      || (lstFMCases[0].Resource_Booking_Chargeable_Fee__c != NULL && lstFMCases[0].Resource_Booking_Chargeable_Fee__c>0))
                      && lstFMCases[0].Tasks.size() > 0) {
                         // if(lstFMCases[0].Booking_Unit__c != NULL) {
                         //     List<Location__c> lstBuildings = new List<Location__c>();
                         //     lstBuildings = [SELECT Id,Name,Property_Name__c,
                         //                        (SELECT FM_User__c,FM_User__r.Email,FM_Role__c,FM_User__r.Name
                         //                        FROM FM_Users__r
                         //                        WHERE FM_Role__c = 'FM Admin' LIMIT 1)
                         //                    FROM Location__c
                         //                    WHERE Name =: lstFMCases[0].Booking_Unit__r.Unit_Name__c.split('/')[0]
                         //                    AND RecordType.DeveloperName = 'Building' LIMIT 1];
                         //    if(lstBuildings.size() > 0 && lstBuildings[0].FM_Users__r.size() > 0) {
                         //        Task objRefundTask = new Task();
                         //        objRefundTask.OwnerId = lstBuildings[0].FM_Users__r[0].FM_User__c;
                         //    }
                         // }
                    if(lstFMCases[0].Admin__c != NULL) {
                        Task objRefundTask = new Task();
                        objRefundTask.OwnerId = lstFMCases[0].Admin__c;
                        objRefundTask.whatId = lstFMCases[0].Id;
                        objRefundTask.Subject = Label.FM_RefundFeeTaskLabel;
                        objRefundTask.Status = 'Not Started';
                        objRefundTask.Priority = 'Normal';
                        objRefundTask.Process_Name__c = 'Amenity Booking';
                        objRefundTask.ActivityDate = Date.today().addDays(2);
                        objRefundTask.Assigned_User__c = 'FM Admin';
                        insert objRefundTask;
                    }
					lstFMCases[0].Amenity_Booking_Status__c = 'Cancel request raised';
					lstFMCases[0].Status__c = 'Submitted';
					update lstFMCases[0];
                }
                else {//Cancel booking by updating FM Case status to 'Cancelled'
                    lstFMCases[0].Amenity_Booking_Status__c = 'Cancelled';
					lstFMCases[0].Status__c = 'Cancelled';
                    update lstFMCases[0];
                }
            }
            catch(Exception excp) {
                insert new Error_Log__c(Process_Name__c = 'Amenity Booking',
                Account__c = lstFMCases[0].Tenant__c,Booking_Unit__c = lstFMCases[0].Booking_Unit__c,
                FM_Case__c = lstFMCases[0].Id,
                Error_Details__c = excp.getMessage());
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,
                    excp.getMessage() + excp.getLineNumber()));
            }
            return new PageReference('/' + lstFMCases[0].Id);
		}
        return NULL;
	}//method
}