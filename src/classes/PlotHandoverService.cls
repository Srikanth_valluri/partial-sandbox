public class PlotHandoverService {
    
   
     //Method to perform callout to fetch 1 year Deposit Amount of Community Charges 
     public static ResponseWrapper getDepositAmount(String strRegId, String caseID) {
         actionComPlotHandOver.PlotHandoverHttpSoap11Endpoint  objReq = new actionComPlotHandOver.PlotHandoverHttpSoap11Endpoint ();
         objReq.timeout_x = 120000;
         ResponseWrapper objRes = new ResponseWrapper();
         try {             
             String strResponse = objReq.getFacilityDue('2-'+strRegId,'FACILITY_DUE_YN','SFDC',strRegId,'2-'+caseID,'','','','','');
             System.debug('--strResponse ---'+strResponse );             
             if(String.isNotBlank(strResponse)) {
                 Wrapper objResponseWrapper = (Wrapper)JSON.deserialize(strResponse, Wrapper.class);
                 System.debug('--objResponseWrapper ---'+objResponseWrapper);
                 if(objResponseWrapper != null){
                      if(String.isNotBlank(objResponseWrapper.status) && objResponseWrapper.status.equalsIgnoreCase('S')) {
                          objRes.status = objResponseWrapper.status;
                          System.debug('--objResponseWrapper data---'+objResponseWrapper.data.Size()+'----'+objResponseWrapper.data);
                          if(String.isNotBlank(objResponseWrapper.message)){
                               objRes.message = objResponseWrapper.message;
                          }                          
                          if(objResponseWrapper.data.Size() > 0){
                               if(String.isNotBlank(objResponseWrapper.data[0].ATTRIBUTE1) 
                               && objResponseWrapper.data[0].ATTRIBUTE1.equalsIgnoreCase('Y')){
                                   objRes.strIsDepositAmountAvailable = 'Yes';
                               }
                               else{
                                   objRes.status = 'E';
                                   objRes.message = '1 year Deposit Amount not available';
                                   objRes.Amount = 0;
                                   return objRes;
                               }                                 
                               
                               if(String.isNotBlank(objResponseWrapper.data[0].PROC_STATUS) 
                               && objResponseWrapper.data[0].PROC_STATUS.equalsIgnoreCase('S')){
                                   
                               } 
                               else{
                                   objRes.status = 'E';
                                   objRes.message = objResponseWrapper.data[0].PROC_MESSAGE;
                                   objRes.Amount = 0;
                                   return objRes;
                               }                               
                               if(String.isNotBlank(objResponseWrapper.data[0].ATTRIBUTE2)){
                                   objRes.Amount = Decimal.valueOf(objResponseWrapper.data[0].ATTRIBUTE2);
                               }
                               else{
                                   objRes.status = 'E';
                                   objRes.message = '1 year Deposit Amount not available';
                                   objRes.Amount = 0;
                                   return objRes;
                               }
                           }
                           else{
                               objRes.status = 'E';
                               objRes.message = '1 year Deposit Amount not available';
                               objRes.Amount = 0;
                               return objRes;
                           }
                      }
                      else{
                          objRes.status = 'E';
                          objRes.message = ' 1 year Deposit callout response status does not have success value';
                          objRes.Amount = 0;
                          return objRes;
                      }
                 }
                 else{
                     objRes.status = 'E';
                     objRes.message = '1 year Deposit callout response does not have any value';
                     objRes.Amount = 0;
                     return objRes;
                 }
                 return objRes;
             }
             return objRes;
         } 
         catch(exception ex){
             System.debug('----exception----'+ex);
             objRes.status = 'exception';
             objRes.message = ex.getMessage();
             objRes.Amount = 0;
             return objRes;             
                         
        }     
     }
    
    public Class Wrapper {
        public String message;
        public String status;        
        public List<DataWrapper> data ;
                
    }
    
    public Class DataWrapper{
        public String ATTRIBUTE1;
        public String ATTRIBUTE2;
        public String Message_ID;
        public String PARAM_ID;
        public String PROC_MESSAGE;
        public String PROC_STATUS;
    }
    
    public  Class ResponseWrapper{
        public String Message;
        public String Status;
        public Decimal Amount; 
        public String strIsDepositAmountAvailable;      
    }
    
}