@isTest
public class FmcGenerateReceiptControllerTest {
    static final String TEST_URL = 'https://test.damacgroup.com/test.pdf';
  @isTest
    public static void testfetchMyReceipts() {

        User portalUser = CommunityTestDataFactory.createPortalUser();
        Id portalAccountId = [SELECT Id, Name, AccountId, ContactId, Account.Party_ID__c, Contact.AccountId, Contact.Account.Party_ID__c FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;

        System.debug('portalAccountId = ' + portalAccountId);

        Location__c location = new Location__c(
            Name = 'Test Location',
            Location_ID__c = 'LOC',
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0
        );
        insert location;

        insert new List<SR_Attachments__c> {
            new SR_Attachments__c(
                Building__c = location.Id,
                Document_Type__c = 'Notice'
            )
        };

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(portalAccountId, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            unit.Inventory__c = inventory.Id;
        }
        insert lstBookingUnit;

        insert TestDataFactory_CRM.createActiveFT_CS();

        CustomerCommunityUtils.customerAccountId = portalAccountId;

        /*FmIpmsRestServices.DueInvoicesResponse response = new FmIpmsRestServices.DueInvoicesResponse();
        response.OutputParameters = new FmIpmsRestServices.OutputParameters();

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(
            200, 'success', JSON.serialize(response), NULL
        ));*/

        Test.startTest();
        Object myReciepts;
        System.runAs(portalUser) {
            myReciepts = FmcGenerateReceiptController.fetchMyReceipts();
        }
        Test.stopTest();

        //System.assertNotEquals(NULL, myReciepts);

    }

    @isTest
    public static void testfetchMyReceiptsNegative() {

        User portalUser = CommunityTestDataFactory.createPortalUser();
        portalUser = [SELECT Id, Name, AccountId, ContactId, Account.Party_ID__c, Contact.AccountId, Contact.Account.Party_ID__c FROM User WHERE Id = :portalUser.Id];
        update new Account(Id = portalUser.Contact.AccountId, Party_ID__c = '1234');

        Id portalAccountId = portalUser.Contact.AccountId;

        System.debug('portalAccountId = ' + portalAccountId);

        Location__c location = new Location__c(
            Name = 'Test Location',
            Location_ID__c = 'LOC',
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0
        );
        insert location;

        insert new List<SR_Attachments__c> {
            new SR_Attachments__c(
                Building__c = location.Id,
                Document_Type__c = 'Notice'
            )
        };

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(portalAccountId, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            unit.Inventory__c = inventory.Id;
        }
        insert lstBookingUnit;

        insert TestDataFactory_CRM.createActiveFT_CS();

        CustomerCommunityUtils.customerAccountId = portalAccountId;
        CustomerCommunityUtils.user = portalUser;

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', '{"responseId":"3246","responseTime":"Wed Aug 15 16:32:07 GST 2018","responseLines":[{"cashReceiptId":970958,"receiptType":"CASH","partyId":3246,"customerName":"Faisal Ali Habib Sajwani","currencyCode":"AED","enteredAmount":36116.0,"unAppliedAmount":0.0,"appliedAmount":36116.0,"functionalAmount":36116.0,"receiptNumber":"1101162302","receiptReference":"FGB CHQ NO 294562","receiptDate":"10-Feb-2016","orgId":112,"businessGroup":"FM","paymentMethod":"Cheque/Cash to be Remitted","comment":"BD4/13/1303A","actions":[{"action":"GENERATE_RECEIPT","method":"GET","url":"https://ptctest.damacgroup.com/DCOFFEE/report/RECEIPT/PUBLIC_APP/NO_KEY/RECEIPT_ID/970958"}]}]}'));

        String responseBody;
        //responseBody = FmIpmsRestServices.getReceiptsForPartyId('1234');

        /*FmIpmsRestServices.DueInvoicesResponse response = new FmIpmsRestServices.DueInvoicesResponse();
        response.OutputParameters = new FmIpmsRestServices.OutputParameters();

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(
            200, 'success', JSON.serialize(response), NULL
        ));*/

        Test.startTest();
        Object myReciepts;
        System.runAs(portalUser) {
            responseBody = FmIpmsRestServices.getReceiptsForPartyId('1234');
            myReciepts = FmcGenerateReceiptController.fetchMyReceipts();
        }
        Test.stopTest();

        //System.assertEquals(NULL, FmcGenerateReceiptController.fetchMyReceipts());
    }

    @isTest
    public static void testGenerateURL() {
        FmIpmsRestServices.Response response = new FmIpmsRestServices.Response();
        response.complete = true;
        response.actions = new List<FmIpmsRestServices.Action>();
        FmIpmsRestServices.Action action = new FmIpmsRestServices.Action();
        action.url = TEST_URL;
        response.actions.add(action);

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(response)));
        Test.startTest();
        FmcGenerateReceiptController.GenerateURL('1234');
        Test.stopTest();
    }
}