/**************************************************************************
* Description - Test class developed for InquiryShareBatch
*
* Version            Date            Author            Description
* 1.0                25/02/18        Monali            Initial Draft
***************************************************************************/
@isTest
public class InquiryShareBatchTest {
    
    public static testmethod void batchTest1(){ 
        List<Inquiry__c> lstInquiry = new List<Inquiry__c>();
        for (Integer i = 0; i < 25; i++) {
            Inquiry__c inquiryRec = InitialiseTestData.getInquiryDetails('Inquiry', 1);
            lstInquiry.add(inquiryRec);
        }
        insert lstInquiry;

        Test.startTest();
        Generic_Switch_Setting__c cs = new Generic_Switch_Setting__c();
        cs.Active__c = true;
        cs.Next_Run_Time__c = System.now();
        cs.Name = 'Promoter Sharing';
        cs.Cron_Expression__c = '0 0 * * * ?';
        insert cs;
        Database.executebatch(new InquiryShareBatch());
        SharePromoterInquiries sharing = new SharePromoterInquiries();
        //SharePromoterInquiries.createInquiryShare(lstInquiry[0].Id, UserInfo.getUserId()); 
        Test.stopTest();
    }

    public static testmethod void batchTest2(){ 
        List<Inquiry__c> lstInquiry = new List<Inquiry__c>();
        for (Integer i = 0; i < 25; i++) {
            Inquiry__c inquiryRec = InitialiseTestData.getInquiryDetails('Inquiry', 1);
            lstInquiry.add(inquiryRec);
        }
        insert lstInquiry;

        Test.startTest();
        Generic_Switch_Setting__c cs = new Generic_Switch_Setting__c();
        cs.Active__c = true;
        cs.Next_Run_Time__c = System.now();
        cs.Name = 'Promoter Sharing';
        cs.Cron_Expression__c = '0 0 * * * ?';
        insert cs;
        System.schedule('InquiryShareBatch '+system.now(),'0 0 * * * ?',new InquiryShareSchedular());
        Test.stopTest();
    }

}