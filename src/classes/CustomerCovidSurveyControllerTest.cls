@isTest
private class CustomerCovidSurveyControllerTest {

    @isTest
    static void testSurvey() {
        
        
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
            Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='12345');
            insert objAcc;
            NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
            dealSR = TestDataFactory_CRM.createServiceRequest();
            insert dealSR;
            Booking__c objBooking = new Booking__c(Account__c=objAcc.Id, Deal_SR__c=dealSR.Id);
            insert objBooking;
            Booking_Unit__c BUObj = new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='Test name',
                                Registration_ID__c = '92061', Registration_Status__c = 'Active Status', Unit_Selling_Price_AED__c = 100);
            insert BUObj;
            
            
            
        CustomerCovidSurveyController controller = new CustomerCovidSurveyController();
        Survey_CRM__c survey = new Survey_CRM__c(
            Name = Label.FMCustomerCovidSurveyForm,
            Description__c = 'Test Description',
            Thank_You_Text__c = 'Thank You',
            Is_Active__c = TRUE
        );
        insert survey;

        List<Survey_Question_CRM__c> questions = new List<Survey_Question_CRM__c>();
        

        questions.add(new Survey_Question_CRM__c(
            Survey__c = survey.Id,
            OrderNumber__c = 1,
            Order_Number_Displayed__c = '1',
            Question__c = 'Please list what kind of additional control measures which can be implemented in your area?',
            Required__c = True,
            Type__c = 'Free Text'
            
        ));
        
        insert questions;
        
        Test.setCurrentPageReference(new PageReference('Page.CustomerCovidSurvey')); 
        System.currentPageReference().getParameters().put('unitId', BUObj.id);

        controller = new CustomerCovidSurveyController();

        System.debug('controller.questions = ' + controller.questions);

        

        controller.questions[0].selectedOption = 'Test';
        controller.questions[0].choiceForAdditionalTextbox = 'Test';
        controller.questions[0].choiceForAdditionalTextbox  = 'Test';
        controller.questions[0].additionalResponse  = 'Test';
        

        controller.submitSurvey();
    }

    @isTest
    private static void testSearchBookingUnit() {
        System.assert(CustomerCovidSurveyController.searchBookingUnit('test').isEmpty());
    }

}