public class CreateDocCompareRecord {
	
	@InvocableMethod 
    public static void invokeApex(list<Id> setDocs) {
    	set<Id> setDocIds = new set<Id>();
    	for(SR_Attachments__c objSR : [Select Id 
    									, Attachment_URL__c
    									, Case__c
    									, Name
    									, Type__c
    									From SR_Attachments__c
    									Where Id IN:setDocs]) {
    		if (objSR.Attachment_URL__c != null && objSR.Case__c != null && 
    			(objSR.Name.contains('Signed') || objSR.Type__c.contains('Signed'))) {
    			setDocIds.add(objSR.Id);	
    		}    										
		} 
		system.debug('!!!!!setDocIds'+setDocIds);
		if(!setDocIds.isEmpty()){
        	callcompareDocs(setDocIds);
        }   	
    }
    
    @future(callout=true)
     public static void callcompareDocs(set<Id> setDocIds) {
     	set<Id> setCaseId = new set<Id>();
    	map<SR_Attachments__c, String> mapAttachment = new map<SR_Attachments__c, String>();
    	map<String, SR_Attachments__c> mapOriginalAttach = new map<String, SR_Attachments__c>();
     	if(!setDocIds.isEmpty()){
            for(SR_Attachments__c objSR : [Select Id
                                            , IsValid__c
                                            , Name
                                            , Case__c
                                            , Case__r.RecordTypeId
                                            , Case__r.RecordType.Name
                                            , Case__r.RecordType.DeveloperName
                                            , Attachment_URL__c 
                                       from SR_Attachments__c 
                                       where Id IN : setDocIds
                                       and Attachment_URL__c != null
                                       and Case__c != null
                                       and Case__r.RecordTypeId != null
                                       and Case__r.RecordType.DeveloperName = 'AOPT']){
            	String attachName = objSR.Name;  
            	string existingDocName =  attachName.substringAfter('Signed ');           	                                       
            	setCaseId.add(objSR.Case__c);
            	mapAttachment.put(objSR, existingDocName);        
				system.debug('!!!!!mapAttachment'+mapAttachment);               	
    		}// end of for
    	}// end of if
    
	    if (!mapAttachment.isEmpty() && !mapAttachment.values().isEmpty()&& !setCaseId.isEmpty()) {
	    	system.debug('!!!!!mapAttachment.Values()'+mapAttachment.Values());
			system.debug('!!!!!mazi query'+[Select Id
	                                            , IsValid__c
	                                            , Name
	                                            , Case__c
	                                            , Case__r.RecordTypeId
	                                            , Case__r.RecordType.Name
	                                            , Case__r.RecordType.DeveloperName
	                                            , Attachment_URL__c 
	                                            , Type__c
	                                       from SR_Attachments__c 
	                                       where Attachment_URL__c != null
	                                       and Case__c IN: setCaseId
	                                       and (Name IN: mapAttachment.Values()
	                                       or Type__c IN: mapAttachment.Values())
	                                       ]);
	    	for (SR_Attachments__c objExistingSR : [Select Id
	                                            , IsValid__c
	                                            , Name
	                                            , Case__c
	                                            , Case__r.RecordTypeId
	                                            , Case__r.RecordType.Name
	                                            , Case__r.RecordType.DeveloperName
	                                            , Attachment_URL__c 
	                                            , Type__c
	                                       from SR_Attachments__c 
	                                       where Attachment_URL__c != null
	                                       and Case__c IN: setCaseId
	                                       and (Name IN: mapAttachment.Values()
	                                       or Type__c IN: mapAttachment.Values())
	                                       ]) {
				system.debug('ala for madhe====='+objExistingSR);
	        	mapOriginalAttach.put(objExistingSR.Name, objExistingSR);                      	
	    	}
	    }// end of if
	    list<Document_Accuracy__c> lstDocumentAccuracy = new list<Document_Accuracy__c>();
	    list<Error_Log__c> lstErrorLog = new list<Error_Log__c>();
		system.debug('maza mapAttachment.keySet()====='+mapAttachment.keySet());
	    for (SR_Attachments__c objSR : mapAttachment.keySet()) {
	    	SR_Attachments__c existingSR;
			system.debug('maza mapOriginalAttach====='+mapOriginalAttach);
			system.debug('maza mapAttachment.ContainsKey(objSR)====='+mapAttachment.ContainsKey(objSR));
			system.debug('maza mapOriginalAttach.ContainsKey(mapAttachment.get(objSR))====='+mapOriginalAttach.ContainsKey(mapAttachment.get(objSR)));
	    	system.debug('maza mapAttachment====='+mapAttachment);
			system.debug('maza mapAttachment.get(objSR)====='+mapAttachment.get(objSR));
			if (mapOriginalAttach != null && mapAttachment != null && mapAttachment.ContainsKey(objSR)
	    		&& mapOriginalAttach.ContainsKey(mapAttachment.get(objSR))) {
				system.debug('ala if madhe=====');
	    		existingSR = mapOriginalAttach.get(mapAttachment.get(objSR));
				system.debug('maza existingSR====='+existingSR);
	    	}
	    	
	    	if (existingSR != null){
	    		system.debug('!!!!!!!!1inisde if');
	    		DocumentComparisonRestService.DocComparison objComarison = 
	    			DocumentComparisonRestService.compareDocs(existingSR.Attachment_URL__c, 
	    				objSR.Attachment_URL__c, objSR.Case__c);
	    		if (objComarison != null){
	    			Document_Accuracy__c objDA = createDocumentAccRecord(objComarison, 
	    				existingSR.Attachment_URL__c, objSR.Attachment_URL__c, objSR.Case__c);
	    			lstDocumentAccuracy.add(objDA);
	    		} else {
	    			Error_Log__c objError = createErrorLogRecord(objSR.Case__c);
	    			lstErrorLog.add(objError);
	    		}
	    	}
	    }
	    
	    if (!lstDocumentAccuracy.isEmpty()){
	    	insert lstDocumentAccuracy;
	    }
	    if (!lstErrorLog.isEmpty()){
	    	insert lstErrorLog;
	    }
	}
	
    public static Error_Log__c createErrorLogRecord(Id caseId) {
	 	Error_Log__c objError = new Error_Log__c();
	 	objError.Case__c = caseId;
	 	objError.Error_Details__c = 'Error in Document Comparison Service.';
	 	return objError;
	 }
	 
	 public static Document_Accuracy__c createDocumentAccRecord(DocumentComparisonRestService.DocComparison objComarison
	 	, String originalURL, String newURL, Id caseId){
	 	Document_Accuracy__c objDA = new Document_Accuracy__c();
	 	objDA.Accuracy__c = objComarison.accuracy;
	 	objDA.Additional_Characters__c = objComarison.addChar;
	 	objDA.Case__c = caseId;
	 	objDA.Deleted_Characters__c = objComarison.deleteChar;
	 	objDA.Difference_Report_URL__c = objComarison.diffReport;
	 	objDA.Original_Document_URL__c = originalURL;
	 	objDA.Page_Accuracy__c = objComarison.pageAccuracy;
	 	objDA.Signed_Document_URL__c = newURL;
	 	objDA.Total_Characters__c = objComarison.totalChar;
	 	return objDA;
	 }
}