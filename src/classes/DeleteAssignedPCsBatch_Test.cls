@isTest
public class DeleteAssignedPCsBatch_Test {

    public testmethod static void DeleteAssignedPCsBatch()
    {
        List<campaign__c> cmpList=testdatafactory.createCampaignRecords(new List<campaign__c>{new campaign__c()});
            Inventory__c inventory = InitialiseTestData.getInventoryDetails('3456','1234','2345',9086,7650); 
     insert inventory ;
		
        Campaign_Inventory__c cmpinv=new Campaign_Inventory__c();
        cmpinv.Campaign__c=cmpList[0].id;
        cmpinv.End_Date__c=system.today()-1;
        cmpinv.Inventory__c=inventory.id;
        cmpinv.Start_Date__c=system.today();
        
        insert cmpinv;
        
            Assigned_PC__c assign=new Assigned_PC__c();
        assign.User__c=userInfo.getUserId();
        assign.Campaign__c=cmpList[0].id;
        assign.End_Date__c=system.today()-1;
        insert assign;
        
        update assign;
        delete assign;
        //database.executeBatch(new DeleteAssignedPCsBatch());
    }
    
    
    public testmethod static void DeleteAssignedPCsBatchScheduler()
    {
        DeleteAssignedPCsBatchScheduler scheduler=new DeleteAssignedPCsBatchScheduler();
    
        String sch = '0 0 0 * * ?';
        //system.schedule('Test check', sch, scheduler);
        
    }
}