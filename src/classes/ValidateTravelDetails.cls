public with sharing class ValidateTravelDetails {
    
    public static String POP_ERROR = ' Please Upload POP Before updating the Status. ';

    public static void validateTravelDetailsPOP(Map<Id, sObject> pTravelDetailsNewMap) {
        System.debug('pTravelDetailsNewMap>>>>> ' + pTravelDetailsNewMap); 
        Map<Id, Attachment> travelDetailAttachmentMap = new Map<Id, Attachment>();
        List<User> userList = [SELECT Id, Profile.Name 
                                 FROM User
                                WHERE Id = :UserInfo.getUserId()
        ];
        if (userList != null && !userList.isEmpty()) {
            if (userList[0].Profile.Name == 'Flyin Admin') {
                for (Attachment att : [ SELECT Id, ParentId 
                                          FROM Attachment 
                                         WHERE ParentId IN :pTravelDetailsNewMap.keySet()
                ]) {
                    System.debug('att>>>>> ' + att); 
                    travelDetailAttachmentMap.put(att.ParentId, att);
                }
                System.debug('travelDetailAttachmentMap>>>>> ' + travelDetailAttachmentMap);
                for (sObject sObj : pTravelDetailsNewMap.values()) {
                    System.debug('sObj>>>>> ' + sObj); 
                    Travel_Details__c tdObj = (Travel_Details__c) sObj;
                    System.debug('tdObj.Status__c>>>>> ' + tdObj.Status__c);
                    if (tdObj.Status__c == 'Token Paid'
                        && !travelDetailAttachmentMap.containskey(tdObj.Id)
                    ) {
                        tdObj.addError(POP_ERROR);
                    } else {
                        System.debug('VALID !! >>> ');
                    }
                }
                System.debug('Completed>>>>> ');
            }
        }
    }

}