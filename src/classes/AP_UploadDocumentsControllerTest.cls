@isTest
private class AP_UploadDocumentsControllerTest
{
    @testSetup static void setupData() {
        SR_Process__c cs = new SR_Process__c();
        cs.Name = 'Test CS';
        cs.New_Document_Process__c = true;
        insert cs;
        
        NSIBPM__Document_Master__c LOI_DM = new NSIBPM__Document_Master__c(
            NSIBPM__Code__c = 'LOI_AGREEMENT',
            NSIBPM__Available_to_client__c = true
        );
        insert LOI_DM;
        
        
        
        Page_Flow__c pf = new Page_Flow__c();
        pf.Name = 'Pre-Approval';
        pf.Flow_Description__c   = 'Pre-Approval - Company Formation';
        pf.Master_Object__c  = 'NSIBPM__Service_Request__c';
        pf.Record_Type_API_Name__c  = 'RAKFTZ_Pre_Approval';
        pf.Requirements__c = 'Test';
        insert pf;
        
        Page__c page = new Page__c();
        page.No_Quick_navigation__c = true;
        page.Is_Custom_Component__c = true; 
        page.Migration_Rec_Id__c  = '';
        page.Page_Description__c = 'PRE-APPROVAL FOR VISA SERVICES';
        page.Page_Flow__c = pf.id;
        page.Page_Order__c = 1;
        page.Page_Title__c = 'Package';
        
        page.Render_By_Default__c = true;
        page.VF_Page_API_Name__c = 'Process_Flow';
        page.What_Id__c = '';
        insert page;
        
        Section__c  sec = new Section__c();
        sec.Name = 'Section name';
        sec.Order__c = 1;
        sec.Section_Title__c = 'Sectoin title';
        sec.Section_Type__c = 'PageBlockSection';
        sec.Layout__c  = '2';
        sec.Page__c = page.Id;
        insert sec;
        
        Section_Detail__c sd = new Section_Detail__c();
        sd.Order__c = 1;
        sd.Name = 'Save';
        sd.Navigation_Directions__c = 'Forward';
        sd.Component_Label__c   = 'Save';
        sd.Component_Type__c = 'Command Button';
        sd.Section__c = sec.Id;
        insert sd;
        
        
        
        
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        
        Id srRecordTypeId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Agent Registration').getRecordTypeId();
        
        NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
        SR.NSIBPM__Customer__c  = a.Id;
        SR.RecordTypeId = srRecordTypeId;
        SR.NSIBPM__Email__c = 'test@nsigulf.com';
        SR.NSIBPM__Send_SMS_to_Mobile__c= '12123';
        SR.COUNTRY_OF_SALE__C = 'UAE;Lebanon;Jordan';
        SR.filled_page_ids__c = 'Company-05,Docs-06';
        SR.Agency_Type__c = 'Individual';
        insert SR;
        
        NSIBPM__SR_Status__c srStatus = new NSIBPM__SR_Status__c();
        srStatus.Name = 'Submitted';
        srStatus.NSIBPM__Code__c = 'Success';
        insert srStatus;
        
        Amendment__c amd = new Amendment__c();
        amd.Service_Request__c = SR.Id;
        insert amd;
        
        Amendment__c amd1 = new Amendment__c();
        amd1.Service_Request__c = SR.Id;
        insert amd1;
        
        NSIBPM__Document_Master__c Dm = new NSIBPM__Document_Master__c();
        Dm.Name = 'Test DM';
        Dm.NSIBPM__Code__c = 'Bank Statement';
        Dm.NSIBPM__Document_Type_Code__c = 'Doc Type';
        insert Dm;
        
        List<NSIBPM__SR_Doc__c> SRDoc_List = createSRDocs(SR.Id, Dm);     
        
    }
    
    
    public static List<NSIBPM__SR_Doc__c> createSRDocs(Id SRId,NSIBPM__Document_Master__c Dm)
    {
        List<NSIBPM__SR_Doc__c> SRDoc_List = new List<NSIBPM__SR_Doc__c>();
        NSIBPM__SR_Doc__c SRDoc;
        for(Integer i=0;i<10;i++)
        {
            SRDoc = new NSIBPM__SR_Doc__c();
            SRDoc.Name = 'Test' + i;
            SRDoc.NSIBPM__Document_Master__c = Dm.Id;
            SRDoc.NSIBPM__Service_Request__c = SRId;
            SRDoc_List.add(SRDoc); 
        }
        insert SRDoc_List;
        return SRDoc_List;
    }
    
    @isTest static void test_method_one() {
        // Implement test code
        // 
        NSIBPM__Service_Request__c  SR = [Select Id from NSIBPM__Service_Request__c];
        Page_Flow__c pf = [Select id From Page_Flow__c LIMIT 1 ];
        Page__c p =  [Select Id from Page__c LIMIT 1];
        Section__c sec = [Select id from Section__c LIMIT 1];
        Section_Detail__c sd =  [Select id from Section_Detail__c LIMIT 1];
        List<Amendment__c> amdList = [Select Id from Amendment__c];
        List<NSIBPM__SR_Doc__c> SRDoc_List = [Select Id from NSIBPM__SR_Doc__c];
        PageReference Process_Flow = Page.Process_Flow;
        Test.setCurrentPageReference(Process_Flow);
        
        Process_Flow.getParameters().put('FlowId',pf.Id);
        Process_Flow.getParameters().put('Type','RAKFTZ_Pre_Approval');
        Process_Flow.getParameters().put('id',SR.Id);
        Process_Flow.getParameters().put('PageId',p.Id);
        
        Attachment objAttachment = new Attachment();
        objAttachment.Name = 'Test Document';
        objAttachment.Body = blob.valueOf('test');
        objAttachment.ContentType='image/png';
        objAttachment.ParentId = SRDoc_List[0].id;
        insert objAttachment;
        
        Test.startTest();
        PageReference pageRef = Page.AP_UploadDocuments;
        pageRef.getparameters().put('EditCompanySR' , 'true');
        pageRef.getparameters().put('Id' , SR.id);
        Test.setCurrentPage(pageRef);
        
        AP_UploadDocumentsController cls = new AP_UploadDocumentsController();
        cls.strAppId = SR.Id;
        
        Unit_Documents__c testUnitDocuments = new Unit_Documents__c();
        testUnitDocuments.Service_Request__c = SR.Id;
        insert testUnitDocuments;
        
        //AP_UploadDocumentsController.getAttachment(objAttachment.Id);
        AP_UploadDocumentsController.doUploadAttachment(SRDoc_List[0].Id,'TestBody','strAttachmentName',objAttachment.id,'test');
        AP_UploadDocumentsController.doUploadAttachment(testUnitDocuments.Id,'TestBody','strAttachmentName',objAttachment.id,'test');
        AP_UploadDocumentsController.uploadAttachment(SRDoc_List[0].Id, objAttachment.id,'test');
        AP_UploadDocumentsController.uploadAttachment(testUnitDocuments.Id, objAttachment.id,'test');
        
        PageReference q;
        apexpages.currentpage().getparameters().put('Id' , SR.id);
        
        q = cls.updateSr();
        //cls.generateLOIAgreements();
        
        PageReference s = new PageReference('/AP_ConfirmRequest?id='+SR.id);
        ////system.assertEquals(q.getURL(), s.getURL());
        
        String generatedContent = AP_UploadDocumentsController.generateContentDocument(objAttachment.Body, objAttachment.Name, SR.id);
        cls.PrepareGenDocs();
        //cls.generateLOIAgreementsNew();
        Test.stopTest();
        
        
    }   
    
    @isTest static void test_method_two() {
        SR_Process__c cs = new SR_Process__c();
        cs.Name = 'Test CS';
        cs.New_Document_Process__c = false;
        insert cs;
        
        Id testUser = [SELECT id FROM User WHERE UserRole.Name != null LIMIT 1].id;
        Account a = new Account();
        a.Name = 'Test Account';
        a.OwnerId = testUser;
        insert a;
        
        NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
        SR.NSIBPM__Customer__c  = a.Id;
        SR.NSIBPM__Email__c = 'test@nsigulf.com';
        SR.NSIBPM__Send_SMS_to_Mobile__c= '12123';
        SR.COUNTRY_OF_SALE__C = 'UAE;Lebanon;Jordan';
        SR.filled_page_ids__c = 'Docs-07';
        SR.Agency_Type__c = 'Individual';
        
        insert SR;
        List<NSIBPM__Service_Request__c> listNSIB = new List<NSIBPM__Service_Request__c>();
        listNSIB.add(SR);
        
        
        
        NSIBPM__Document_Master__c Dm1 = new NSIBPM__Document_Master__c();
        Dm1.Name = 'Test DM1';
        Dm1.NSIBPM__Code__c = 'Bank Statement1';
        Dm1.NSIBPM__Document_Type_Code__c = 'Doc Type1';
        insert Dm1;
        
        List<NSIBPM__SR_Doc__c> SRDoc_List = createSRDocs(SR.Id, Dm1);
        
        Attachment objAttachment = new Attachment();
        objAttachment.Name = 'Test Document';
        objAttachment.Body = blob.valueOf('test');
        objAttachment.ContentType='image/png';
        objAttachment.ParentId = SRDoc_List[0].id;
        
        Amendment__c amd = new Amendment__c();
        amd.Last_Name__c = 'test';
        amd.First_Name__c = 'test';
        amd.Email__c = 'test'+DateTime.Now().getTIme()+'@test.com';
        amd.ID_Type__c = 'Passport';
        amd.ID_Number__c = '1234567890';
        amd.ID_Issue_Date__c = Date.parse('10/10/2019');
        amd.ID_Expiry_Date__c = Date.parse('12/12/2020');
        amd.Mobile_Country_Code__c = 'India: 0091';
        amd.Mobile__c = '1234567899';
        amd.Designation__c = 'testing';
        amd.Portal_Administrator__c = true;
        amd.Agent_Representative__c = true;
        amd.Service_Request__c = SR.Id;
        insert amd;
        
        PageReference pageRef = Page.AP_UploadDocuments;
        pageRef.getparameters().put('EditAgentSR' , 'true');
        pageRef.getparameters().put('Id' , SR.id);
        Test.setCurrentPage(pageRef);
        apexpages.currentpage().getparameters().put('Id' , SR.id); 
        Id pRecordTypeId = Schema.SObjectType.Unit_Documents__c.getRecordTypeInfosByName().get('General').getRecordTypeId();
        AP_UploadDocumentsController cls = new AP_UploadDocumentsController();
        AP_UploadDocumentsController.doUploadAttachment(SRDoc_List[0].Id,'TestBody','strAttachmentName',objAttachment.id,'test');
        cls.generateLOIDocumentHelperNew('UAE', SR, '', pRecordTypeId);
        cls.generateLOIDocumentHelperNew('Lebanon', SR, '', pRecordTypeId);
        cls.generateLOIDocumentHelperNew('Jordan', SR, '', pRecordTypeId);
        cls.generateLOIAgreementsNew();
        cls.init();
        cls.updateSr();
    }
    
    @isTest static void test_method_four () {
        SR_Process__c cs = new SR_Process__c();
        cs.Name = 'Test CS';
        cs.New_Document_Process__c = false;
        insert cs;
        
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        
        // Id srRecordTypeId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Agent Registration').getRecordTypeId();
        
        NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
        SR.NSIBPM__Customer__c  = a.Id;
        SR.NSIBPM__Email__c = 'test@nsigulf.com';
        //SR.RecordTypeId = srRecordTypeId;
        SR.NSIBPM__Send_SMS_to_Mobile__c= '12123';
        SR.Agent_Registration_Type__c  = 'LOI';
        SR.COUNTRY_OF_SALE__C = 'UAE;Lebanon;Jordan';
        SR.filled_page_ids__c = 'Company-05,Docs-06';
        SR.Agency_Type__c = 'Corporate';
        SR.Country_Of_Incorporation_New__c = 'United Arab Emirates';
        insert SR;
        List<NSIBPM__Service_Request__c> listNSIB = new List<NSIBPM__Service_Request__c>();
        listNSIB.add(SR);
        
        NSIBPM__SR_Status__c srStatus1 = new NSIBPM__SR_Status__c();
        srStatus1.Name = 'LOI Submitted';
        srStatus1.NSIBPM__Code__c = 'LOI';
        insert srStatus1;
        
        NSIBPM__Document_Master__c Dm1 = new NSIBPM__Document_Master__c();
        Dm1.Name = 'Test DM1';
        Dm1.NSIBPM__Code__c = 'Bank Statement1';
        Dm1.NSIBPM__Document_Type_Code__c = 'Doc Type1';
        insert Dm1;
        
        List<NSIBPM__SR_Doc__c> SRDoc_List = createSRDocs(SR.Id, Dm1);
        
        Attachment objAttachment = new Attachment();
        objAttachment.Name = 'Test Document';
        objAttachment.Body = blob.valueOf('test');
        objAttachment.ContentType='image/png';
        objAttachment.ParentId = SRDoc_List[0].id;
        
        PageReference pageRef = Page.AP_UploadDocuments;
        pageRef.getparameters().put('EditCompanySR' , 'true');
        pageRef.getparameters().put('Id' , SR.id);
        Test.setCurrentPage(pageRef);
        apexpages.currentpage().getparameters().put('Id' , SR.id); 
        Id pRecordTypeId = Schema.SObjectType.Unit_Documents__c.getRecordTypeInfosByName().get('General').getRecordTypeId();
        AP_UploadDocumentsController cls = new AP_UploadDocumentsController();
        cls.isUnitDoc = true;
        cls.strActionId = '1234567899';
        AP_UploadDocumentsController.doUploadAttachment(SRDoc_List[0].Id,'TestBody','strAttachmentName',objAttachment.id,'test');
        cls.generateLOIDocumentHelperNew('UAE', SR, '', pRecordTypeId);
        cls.generateLOIDocumentHelperNew('Lebanon', SR, '', pRecordTypeId);
        cls.generateLOIDocumentHelperNew('Jordan', SR, '', pRecordTypeId);
        cls.generateLOIAgreementsNew();
        cls.init();
        cls.updateSr();
    }
    
    @isTest static void test_method_six() {
        SR_Process__c cs = new SR_Process__c();
        cs.Name = 'Test CS';
        cs.New_Document_Process__c = false;
        insert cs;
        
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        
        Id srRecordTypeId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Agent Update').getRecordTypeId();
        
        NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
        SR.NSIBPM__Customer__c  = a.Id;
        SR.NSIBPM__Email__c = 'test@nsigulf.com';
        //SR.RecordTypeId = srRecordTypeId;
        SR.NSIBPM__Send_SMS_to_Mobile__c= '12123';
        SR.Agent_Registration_Type__c  = 'LOI';
        SR.COUNTRY_OF_SALE__C = 'UAE';
        SR.filled_page_ids__c = 'Company-05,Docs-06';
        SR.Agency_Type__c = 'Corporate';
        SR.Country_Of_Incorporation_New__c = 'United Arab Emirates';
        insert SR;
        List<NSIBPM__Service_Request__c> listNSIB = new List<NSIBPM__Service_Request__c>();
        listNSIB.add(SR);
        
        NSIBPM__SR_Status__c srStatus1 = new NSIBPM__SR_Status__c();
        srStatus1.Name = 'LOI Submitted';
        srStatus1.NSIBPM__Code__c = 'LOI';
        insert srStatus1;
        
        NSIBPM__Document_Master__c Dm1 = new NSIBPM__Document_Master__c();
        Dm1.Name = 'Test DM1';
        Dm1.NSIBPM__Code__c = 'Bank Statement1';
        Dm1.NSIBPM__Document_Type_Code__c = 'Doc Type1';
        insert Dm1;
        
        List<NSIBPM__SR_Doc__c> SRDoc_List = createSRDocs(SR.Id, Dm1);
        
        Attachment objAttachment = new Attachment();
        objAttachment.Name = 'Test Document';
        objAttachment.Body = blob.valueOf('test');
        objAttachment.ContentType='image/png';
        objAttachment.ParentId = SRDoc_List[0].id;
        
        PageReference pageRef = Page.AP_UploadDocuments;
        pageRef.getparameters().put('EditCompanySR' , 'true');
        pageRef.getparameters().put('Id' , SR.id);
        Test.setCurrentPage(pageRef);
        apexpages.currentpage().getparameters().put('Id' , SR.id); 
        Id pRecordTypeId = Schema.SObjectType.Unit_Documents__c.getRecordTypeInfosByName().get('General').getRecordTypeId();
        AP_UploadDocumentsController cls = new AP_UploadDocumentsController();
        cls.isUnitDoc = true;
        cls.strActionId = '1234567899';
        AP_UploadDocumentsController.doUploadAttachment(SRDoc_List[0].Id,'TestBody','strAttachmentName',objAttachment.id,'test');
        cls.generateLOIDocumentHelperNew('UAE', SR, '', pRecordTypeId);
        cls.generateLOIDocumentHelperNew('Lebanon', SR, '', pRecordTypeId);
        cls.generateLOIDocumentHelperNew('Jordan', SR, '', pRecordTypeId);
        cls.generateLOIAgreementsNew();
        cls.init();
        cls.updateSr();
    }
    
    @isTest static void test_method_three() {
        SR_Process__c cs = new SR_Process__c();
        cs.Name = 'Test CS';
        cs.New_Document_Process__c = false;
        insert cs;
        
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
        SR.NSIBPM__Customer__c  = a.Id;
        
        SR.NSIBPM__Email__c = 'test@nsigulf.com';
        SR.NSIBPM__Send_SMS_to_Mobile__c= '12123';
        SR.COUNTRY_OF_SALE__C = 'UAE;Lebanon;Jordan';
        SR.Primary_Language__c = 'Chinese'; 
        SR.filled_page_ids__c = 'Doc-07';
        SR.Agency_Type__c = 'Individual';
        insert SR;
        List<NSIBPM__Service_Request__c> listNSIB = new List<NSIBPM__Service_Request__c>();
        listNSIB.add(SR);
        
        
        
        NSIBPM__Document_Master__c Dm1 = new NSIBPM__Document_Master__c();
        Dm1.Name = 'Test DM1';
        Dm1.NSIBPM__Code__c = 'Bank Statement1';
        Dm1.NSIBPM__Document_Type_Code__c = '';
        insert Dm1;
        
        List<NSIBPM__SR_Doc__c> SRDoc_List = createSRDocs(SR.Id, Dm1);
        
        Attachment objAttachment = new Attachment();
        objAttachment.Name = 'Test Document';
        objAttachment.Body = blob.valueOf('test');
        objAttachment.ContentType='image/png';
        objAttachment.ParentId = SRDoc_List[0].id;
        
        apexpages.currentpage().getparameters().put('Id' , SR.id); 
        
        Id pRecordTypeId = Schema.SObjectType.Unit_Documents__c.getRecordTypeInfosByName().get('General').getRecordTypeId();
        AP_UploadDocumentsController cls = new AP_UploadDocumentsController();
        
        AP_UploadDocumentsController.doUploadAttachment(SRDoc_List[0].Id,'TestBody','strAttachmentName',objAttachment.id,'test');
        
        cls.generateLOIAgreementsNew();
        
        cls.generateLOIDocumentHelperNew('Lebanon', SR, '', pRecordTypeId);
        cls.generateLOIDocumentHelperNew('Jordan', SR, '', pRecordTypeId);
        cls.isUnitDoc = true;
        
        cls.init();
        cls.updateSr();
    }
    
    @isTest static void test_method_five(){
        SR_Process__c cs = new SR_Process__c();
        cs.Name = 'Test CS';
        cs.New_Document_Process__c = false;
        insert cs;
        
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
        SR.NSIBPM__Customer__c  = a.Id;
        
        SR.NSIBPM__Email__c = 'test@nsigulf.com';
        SR.NSIBPM__Send_SMS_to_Mobile__c= '12123';
        SR.COUNTRY_OF_SALE__C = 'UAE;Lebanon;Jordan';
        SR.Primary_Language__c = 'Chinese'; 
        SR.filled_page_ids__c = 'Doc-07';
        SR.Agency_Type__c = 'Individual';
        insert SR;
        List<NSIBPM__Service_Request__c> listNSIB = new List<NSIBPM__Service_Request__c>();
        listNSIB.add(SR);
        
        
        
        NSIBPM__Document_Master__c Dm1 = new NSIBPM__Document_Master__c();
        Dm1.Name = 'Test DM1';
        Dm1.NSIBPM__Code__c = 'Bank Statement1';
        Dm1.NSIBPM__Document_Type_Code__c = '';
        insert Dm1;
        
        List<NSIBPM__SR_Doc__c> SRDoc_List = createSRDocs(SR.Id, Dm1);
        
        Attachment objAttachment = new Attachment();
        objAttachment.Name = 'Test Document';
        objAttachment.Body = blob.valueOf('test');
        objAttachment.ContentType='image/png';
        objAttachment.ParentId = SRDoc_List[0].id;
        
        apexpages.currentpage().getparameters().put('Id' , SR.id); 
        
        Id pRecordTypeId = Schema.SObjectType.Unit_Documents__c.getRecordTypeInfosByName().get('General').getRecordTypeId();
        AP_UploadDocumentsController cls = new AP_UploadDocumentsController();
        cls.isUnitDoc = true;
        AP_UploadDocumentsController.doUploadAttachment(SRDoc_List[0].Id,'TestBody','strAttachmentName',objAttachment.id,'test');
        
        cls.generateLOIAgreementsNew();
        
        cls.generateLOIDocumentHelperNew('Lebanon', SR, '', pRecordTypeId);
        cls.generateLOIDocumentHelperNew('Jordan', SR, '', pRecordTypeId);
        cls.isUnitDoc = true;
        
        cls.init();
        cls.updateSr();
    }
    
    @isTest static void test_method_seven () {
        SR_Process__c cs = new SR_Process__c();
        cs.Name = 'Test CS';
        cs.New_Document_Process__c = false;
        insert cs;
        
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        
        // Id srRecordTypeId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Agent Registration').getRecordTypeId();
        
        NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
        SR.NSIBPM__Customer__c  = a.Id;
        SR.NSIBPM__Email__c = 'test@nsigulf.com';
        //SR.RecordTypeId = srRecordTypeId;
        SR.NSIBPM__Send_SMS_to_Mobile__c= '12123';
        SR.Agent_Registration_Type__c  = 'Basic';
        SR.COUNTRY_OF_SALE__C = 'UAE;Lebanon;Jordan';
        SR.filled_page_ids__c = 'Company-05,Docs-06';
        SR.Agency_Type__c = 'Corporate';
        SR.Country_Of_Incorporation_New__c = 'United Arab Emirates';
        insert SR;
        List<NSIBPM__Service_Request__c> listNSIB = new List<NSIBPM__Service_Request__c>();
        listNSIB.add(SR);
        
        NSIBPM__SR_Status__c srStatus1 = new NSIBPM__SR_Status__c();
        srStatus1.Name = 'LOI Submitted';
        srStatus1.NSIBPM__Code__c = 'LOI';
        insert srStatus1;
        
        NSIBPM__Document_Master__c Dm1 = new NSIBPM__Document_Master__c();
        Dm1.Name = 'Test DM1';
        Dm1.NSIBPM__Code__c = 'Bank Statement1';
        Dm1.NSIBPM__Document_Type_Code__c = 'Doc Type1';
        insert Dm1;
        
        List<NSIBPM__SR_Doc__c> SRDoc_List = createSRDocs(SR.Id, Dm1);
        
        Attachment objAttachment = new Attachment();
        objAttachment.Name = 'Test Document';
        objAttachment.Body = blob.valueOf('test');
        objAttachment.ContentType='image/png';
        objAttachment.ParentId = SRDoc_List[0].id;
        
        PageReference pageRef = Page.AP_UploadDocuments;
        pageRef.getparameters().put('EditCompanySR' , 'true');
        pageRef.getparameters().put('Id' , SR.id);
        Test.setCurrentPage(pageRef);
        apexpages.currentpage().getparameters().put('Id' , SR.id); 
        Id pRecordTypeId = Schema.SObjectType.Unit_Documents__c.getRecordTypeInfosByName().get('General').getRecordTypeId();
        AP_UploadDocumentsController cls = new AP_UploadDocumentsController();
        cls.isUnitDoc = true;
        cls.strActionId = '1234567899';
        AP_UploadDocumentsController.doUploadAttachment(SRDoc_List[0].Id,'TestBody','strAttachmentName',objAttachment.id,'test');
        cls.generateLOIDocumentHelperNew('UAE', SR, '', pRecordTypeId);
        cls.generateLOIDocumentHelperNew('Lebanon', SR, '', pRecordTypeId);
        cls.generateLOIDocumentHelperNew('Jordan', SR, '', pRecordTypeId);
        cls.generateLOIAgreementsNew();
        cls.init();
        cls.updateSr();
        
        cls.isUnitDoc = false;
        String generatedContent = AP_UploadDocumentsController.generateContentDocument(objAttachment.Body, objAttachment.Name, SR.id);
        cls.PrepareGenDocs();
    }
}