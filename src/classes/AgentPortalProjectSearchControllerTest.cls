// Creayed By :  Naresh Kaneriya
@isTest
public class AgentPortalProjectSearchControllerTest {
    
    public static Contact adminContact;
    public static User portalUser;
    public static Account adminAccount;
    public static User portalOnlyAgent;
    
    static void init(){
 UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test456', email='xyz1@email.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz1@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
        adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
        insert adminAccount;
        
        adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
        insert adminContact;
        
        Contact agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
        insert agentContact;
        
        portalUser = InitialiseTestData.getPortalUser('test123@test.com', adminContact.Id, 'Admin');
        portalOnlyAgent = InitialiseTestData.getPortalUser('test789@test.com', agentContact.Id, 'Agent');
        
        System.runAs(portalUser){
           Property__c property = InitialiseTestData.insertProperties();
            InitialiseTestData.createInventoryUser(property);
        }
        }
    }
       // Inner Class
       @isTest static void ProjectWrapperTest(){
       
       AgentPortalProjectSearchController.ProjectWrapper obj  = new AgentPortalProjectSearchController.ProjectWrapper('marketingName' , 'district' , 'propertyName' , 'propertyId');
      }
    
    
    @isTest static void showProjects(){
         UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole5');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'te656', email='xyz661@email.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz661@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
        Test.startTest();
        init();
        System.runAs(portalUser){
            AgentPortalProjectSearchController obj  = new AgentPortalProjectSearchController();
        }
        Test.stopTest();
        }
    }
    
    @isTest static void filterProjects(){
         UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole9');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'te0456', email='xyz100@email.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz100@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
        Test.startTest();
        init();
        
        System.runAs(portalUser){
            ApexPages.currentPage().getParameters().put('locationNamesSelected','\'Dubai\'');
            ApexPages.currentPage().getParameters().put('projectTypeSelected','\'Residential\'');
            ApexPages.currentPage().getParameters().put('BedroomsSelected','\'1\'');
            ApexPages.currentPage().getParameters().put('sfdc.tabName','\'1\'');
            ApexPages.currentPage().getParameters().put('Bedrooms','1');
            ApexPages.currentPage().getParameters().put('type','1');
            ApexPages.currentPage().getParameters().put('Location','1');
            AgentPortalProjectSearchController obj = new AgentPortalProjectSearchController();
            obj.filterProjects();
            
        }
        
        Test.stopTest();
        }
    }
    
    
}