@isTest
private class CCAvenuePaymentGatewayTest {

    private static final String GATEWAY_NAME = 'test';
    private static final String GATEWAY_URL = 'https://www.payment-gateway.com';
    private static final String GATEWAY_MERCHANT_ID = 'merchant_id';
    private static final String GATEWAY_ACCESS_CODE = 'access_code';
    private static final String GATEWAY_ENCRYPTION_KEY = 'encryption_key';//'ac24310eb79685df';
    private static final String PERSON_ACCOUNT = 'Person Account';

    @testSetup
    private static void setupTestData() {
        PaymentGateway__c gateway = new PaymentGateway__c(
            Name = GATEWAY_NAME,
            Url__c = GATEWAY_URL,
            MerchantId__c = GATEWAY_MERCHANT_ID,
            AccessCode__c = GATEWAY_ACCESS_CODE,
            EncryptionKey__c = GATEWAY_ENCRYPTION_KEY
        );
        insert gateway;
    }

    @isTest
    static void testPaymentTransactionPositive() {
        Id personAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

        Account testAccount = new Account(
            FirstName = 'Test',
            LastName = 'Test',
            Address_Line_1__pc = 'Test',
            City__pc = 'Test',
            Country__pc = 'United Arab Emirates',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(PERSON_ACCOUNT).getRecordTypeId()
        );
        insert testAccount;

        CCAvenuePaymentGateway gateway = CCAvenuePaymentGateway.getInstance(GATEWAY_NAME);

        Test.startTest();
            String paymentUrl = gateway.initiateTransaction(
                '12321', 100, 'www.google.com', testAccount.Id, LoamsMakePaymentController.FM_GUEST_PAYMENT
            );
            paymentUrl = gateway.initiateTransaction(
                '12321', 100, 'www.google.com', 'Test', 'Address', 'City', 'State', '00000', 'Country'
            );
            paymentUrl = gateway.initiateTransaction(
                '12321', 100, 'www.google.com', 'Test', 'test@mail.com', '0000000000'
                , 'Address', 'City', 'State', '00000', 'Country'
            );
            paymentUrl = gateway.initiateTransaction('12321', 100, 'www.google.com', testAccount.Id);

            Map<String, String> params = new Map<String, String>();
            for (String urlParam : paymentUrl.substringAfter('?').split('&')) {
                List<String> param = urlParam.split('=');
                if (param.size() > 1) {
                    if (CCAvenuePaymentGateway.ENC_REQUEST.equalsIgnoreCase(param[0])) {
                        params.put(CCAvenuePaymentGateway.ENC_RESP, param[1]);
                    } else {
                        params.put(param[0], param[1]);
                    }
                }
            }

            params = gateway.getDecryptedResponse(params);

        Test.stopTest();

        System.assert(params.containsKey(CCAvenuePaymentGateway.AMOUNT));

    }

    @isTest
    static void testTrackOrderStatusPositive() {
        CCAvenuePaymentGateway gateway = CCAvenuePaymentGateway.getInstance(GATEWAY_NAME);

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(
            200, 'Success',
            CCAvenuePaymentGateway.STATUS + '=0&' + CCAvenuePaymentGateway.ENC_RESPONSE + '='
                + gateway.encrypt(JSON.serialize(new CCAvenuePaymentGateway.OrderStatusResult()))
        ));

        String orderStatusJson;
        Test.startTest();
            orderStatusJson = gateway.trackOrderStatus('12321');
        Test.stopTest();

        System.assert(String.isNotBlank(orderStatusJson));
    }

    @isTest
    static void testNegative() {
        CCAvenuePaymentGateway gateway = CCAvenuePaymentGateway.getInstance(GATEWAY_NAME);
        System.assertEquals(NULL, gateway.getDecryptedResponse(NULL));
        System.assertNotEquals(NULL, gateway.getDecryptedResponse(new Map<String, String>()));
    }


}