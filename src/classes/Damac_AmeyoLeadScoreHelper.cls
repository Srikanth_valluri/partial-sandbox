public class Damac_AmeyoLeadScoreHelper implements Database.Batchable<sObject> {   


    public Database.QueryLocator start(Database.BatchableContext batchContext){
        
        Set<String> activityTypes = new Set<String>{'Ameyo Dialer','Ameyo Manual'};
        return Database.getQueryLocator([SELECT WhatId, Sub_Disposition_Type__c,Ameyo_Dialer_Disposition__c, CallDurationInSeconds 
                                        FROM TASK 
                                        WHERE Event_Type__c IN : activityTypes
                                        AND LastModifiedDate = TODAY
                                        AND WhatId != null]);
    }
    public void execute(Database.BatchableContext batchContext, List<Task> scope){
        List <ID> inquiryIds = new List <ID> ();
        for (Task eachTask : scope) {
            String whatId = eachTask.whatId;
            String objectName = SchemaGlobalDescribe.findObjectNameFromRecordIdPrefix(whatId.subString (0, 3)); 
            if (objectName == 'Inquiry__c') {
                inquiryIds.add (whatId);
            }
        }
        if (!inquiryIds.isEmpty ()) {
            List <Inquiry__c> inquiriesToUpdate = new List <Inquiry__c> ();
            Set<String> eventTypes = new Set<String>{'Ameyo Dialer','Ameyo Manual'};
            for(Inquiry__c eachInquiry : [SELECT Total_Talk_Time__c, Total_Connects__c, Total_Attempts__c, 
                                            (SELECT Sub_Disposition_Type__c,Ameyo_Dialer_Disposition__c, Call_Start_Time__c 
                                                ,Call_End_Time__c 
                                                FROM Tasks WHERE Event_Type__c IN : eventTypes) 
                                        FROM Inquiry__c 
                                        WHERE RecordType.Name = 'Pre Inquiry']){
                List<Task> eventsForInquiry = new List <Task> ();
                if (!eachInquiry.Tasks.isEmpty ()) {
                    eventsForInquiry = eachInquiry.Tasks;
                }
                
                Decimal totalConnects = 0;
                Decimal totalTalkTime = 0;
                if (eventsForInquiry != null && !eventsForInquiry.isEmpty()) {
                   eachInquiry.Total_Attempts__c = eventsForInquiry.size();
                   for (Task eachEvent : eventsForInquiry) {
                       if (eachEvent.Ameyo_Dialer_Disposition__c == 'CONNECTED' && eachEvent.Sub_Disposition_Type__c != 'Redirected to Voicemail'){
                            if(eachEvent.Call_Start_Time__c != null && eachEvent.Call_End_Time__c != null){
                                Decimal taskStartDate = eachEvent.Call_Start_Time__c.getTime();
                                Decimal taskEndDate = eachEvent.Call_End_Time__c.getTime();
                                Decimal milliseconds = taskEndDate - taskStartDate;
                                Decimal seconds = milliseconds / 1000;
                                Decimal minutes = seconds / 60;
                                totalTalkTime = totalTalkTime + minutes ;
                           }
                           totalConnects += 1;    
                       }
                   }
                } else {
                    eachInquiry.Total_Attempts__c = 0;
                }
                eachInquiry.Total_Talk_Time__c = totalTalkTime ;
                eachInquiry.Total_Connects__c = totalConnects ;
                
                inquiriesToUpdate.add (eachInquiry);
            }
            DAMAC_Constants.skip_InquiryTrigger = true;
            update inquiriesToUpdate;  
        }
    }
    public void finish(Database.BatchableContext BC){
    }
}