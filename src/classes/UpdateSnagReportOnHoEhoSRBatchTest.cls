@isTest
private class UpdateSnagReportOnHoEhoSRBatchTest {

    static testMethod void test_UpdateSnagReportOnHoEhoSRBatch () {

        UpdateSnagReportOnHoEhoSRBatch batchCls = new UpdateSnagReportOnHoEhoSRBatch();

        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount(); 
        objAcc.Nationality__c = 'UAE';
        objAcc.EMail__c = 'abc@new.com';
        insert objAcc;
        System.assert(objAcc != null);
    
        NSIBPM__Service_Request__c objReq = TestDataFactory_CRM.createServiceRequest();
        insert objReq ;

        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp ;
        
        Location__c objLoc = new Location__c();
        objLoc.RecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
        objLoc.Handover_Team_notification_Email__c = 'test@test.com';
        objLoc.Location_ID__c = '1234';
        insert objLoc ;
            
        Inventory__c objInv = TestDataFactory_CRM.createInventory( objProp.Id );
        objInv.Building_Location__c = objLoc.Id;
        insert objInv ;     
        
        Case objCase = new Case();
        objCase.Origin = 'Email';
        objCase.Subject = 'Test email';
        objCase.AccountId = objAcc.id;
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        /*objCase.Id = System.Label.ToDelete;
		update objCase;*/
		insert objCase;

        List<Booking__c> lstBooking =  TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objReq.Id,1 );
        insert lstBooking ;
        
        List<Booking_Unit__c> lstUnits = TestDataFactory_CRM.createBookingUnits( lstBooking, 1 );
        for( Booking_Unit__c objUnit : lstUnits ) {
            objUnit.Inventory__c = objInv.Id ;
            objUnit.FM_Email__c = 'test@test.com';
            objUnit.Loams_Email__c = 'test@test.com';
            objUnit.Registration_Status_Code__c = 'trtert';
            objUnit.Registration_Status__c = lstActiveStatus[0].Status_Value__c;
        }
        insert lstUnits ;    
        
        Case objCase11 = TestDataFactory_CRM.createCase( objAcc.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId() );
        objCase11.OwnerId = UserInfo.getUserId();
        objCase11.Booking_Unit__c = lstUnits[0].Id;
        insert objCase11 ;
        
        
        EmailMessage[] newEmail1 = new EmailMessage[0];
        newEmail1.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            ToAddress = 'site.inspection@damacgroup.com',
            Incoming = True,
            Subject = '[SRF]-[SUBMITTED]-[12/04/2018]-['+lstUnits[0].Unit_Name__c +']-[3]-[0]-[3]-[0]-[]-[CREP amr.mohamed]-[]', 
            TextBody = '23456 ', 
            ParentId = objCase.Id)); 
        insert newEmail1;
        
        Attachment attobj=new Attachment();
        attobj.Name='test';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attobj.body=bodyBlob;
        attobj.ContentType = 'application/pdf';
        attobj.parentId=newEmail1[0].Id;
        insert attobj;
        System.debug('attobj' + attobj);
    
        system.debug('test bu = ' + [Select id, Registration_Status__c,Unit_Active__c,Unit_Name__c from Booking_Unit__c]);
    
        Test.startTest();
        
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT());
            Test.setMock(WebServiceMock.class, new RefundsMock.RefundsMock2()); 
            Test.setMock(WebServiceMock.class, new RefundsExcessAmountMock.RefundsExcessAmountMock1());
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
            Test.setMock(WebServiceMock.class, new RefundsApprovingAuthoritiesMock());
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(3)); 
            
            Database.executeBatch(batchCls,1);
        Test.stopTest();
    }
}