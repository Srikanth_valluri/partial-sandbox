/*
* Description - Test class developed for 'Receipt_DetailsController'
*
* Version            Date            Author            Description
* 1.0                28/11/17        Monali            Initial Draft
*/
@isTest
private class Receipt_DetailsControllerTest {
    static testMethod void testMethod1() {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='12345');
        insert objAcc;
        
        Test.startTest();
        PageReference pageRef = Page.BookingUnitPage;
        pageRef.getParameters().put('id', String.valueOf(objAcc.Id));
        Test.setCurrentPage(pageRef);

        SOAPCalloutServiceMock.returnToMe = new Map<String, ReceiptDetails1.GetReceiptDetailsResponse_element>();
        ReceiptDetails1.GetReceiptDetailsResponse_element response1 = new ReceiptDetails1.GetReceiptDetailsResponse_element();
        response1.return_x = '{"data":[{"ATTRIBUTE3":"1040788","ATTRIBUTE10":"118265456","ATTRIBUTE2":"CASH","ATTRIBUTE1":"943590","ATTRIBUTE14":"75017","ATTRIBUTE13":"Y","ATTRIBUTE12":"13-DEC-2015","ATTRIBUTE11":"CASH","ATTRIBUTE9":"5000","ATTRIBUTE8":"5000","ATTRIBUTE7":"0","ATTRIBUTE6":"5000","ATTRIBUTE5":"AED","ATTRIBUTE4":"RAGINI RAJAN PATIL","ATTRIBUTE18":null,"ATTRIBUTE17":"DH/32/3204 - Token amount paid by cash","ATTRIBUTE16":"Cheque/Cash to be Remitted","ATTRIBUTE15":null,"ATTRIBUTE19":null}],"message":"[16 Receipts found For Given PartyID]","status":"S"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );

       	ApexPages.StandardController controller = new ApexPages.standardController(objAcc);
        Receipt_DetailsController obj = new Receipt_DetailsController(controller);

        Receipt_DetailsController.ReceiptDetailsWrapper objWrap = new Receipt_DetailsController.ReceiptDetailsWrapper();
        objWrap.ATTRIBUTE1 = '';
        objWrap.ATTRIBUTE2 = '';
        objWrap.ATTRIBUTE3 = '';
        objWrap.ATTRIBUTE4 = '';
        objWrap.ATTRIBUTE5 = '';
        objWrap.ATTRIBUTE6 = '';
        objWrap.ATTRIBUTE7 = '';
        objWrap.ATTRIBUTE8 = '';
        objWrap.ATTRIBUTE9 = '';
        objWrap.ATTRIBUTE10 = '';
        objWrap.ATTRIBUTE11 = '';
        objWrap.ATTRIBUTE12 = '';
        objWrap.ATTRIBUTE13 = '';
        objWrap.ATTRIBUTE14 = '';
        objWrap.ATTRIBUTE15 = '';
        objWrap.ATTRIBUTE16 = '';
        objWrap.ATTRIBUTE17 = '';
        objWrap.ATTRIBUTE18 = '';
        objWrap.ATTRIBUTE19 = '';
        objWrap.ATTRIBUTE20 = '';

        Error_Log__c objErr = obj.createErrorLogRecord(objAcc.Id);
        obj.init();
        obj.GenerateURL();
        Test.stopTest();
    }

    static testMethod void testMethod2() {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='12345');
        insert objAcc;
        
        Test.startTest();
        PageReference pageRef = Page.BookingUnitPage;
        pageRef.getParameters().put('id', String.valueOf(objAcc.Id));
        Test.setCurrentPage(pageRef);

        SOAPCalloutServiceMock.returnToMe = new Map<String, ReceiptDetails1.GetReceiptDetailsResponse_element>();
        ReceiptDetails1.GetReceiptDetailsResponse_element response1 = new ReceiptDetails1.GetReceiptDetailsResponse_element();
        response1.return_x = '{"data":[{"ATTRIBUTE3":"1040788","ATTRIBUTE10":"118265456","ATTRIBUTE2":"CASH","ATTRIBUTE1":"943590","ATTRIBUTE14":"75017","ATTRIBUTE13":"Y","ATTRIBUTE12":"13-DEC-2015","ATTRIBUTE11":"CASH","ATTRIBUTE9":"5000","ATTRIBUTE8":"5000","ATTRIBUTE7":"0","ATTRIBUTE6":"5000","ATTRIBUTE5":"AED","ATTRIBUTE4":"RAGINI RAJAN PATIL","ATTRIBUTE18":null,"ATTRIBUTE17":"DH/32/3204 - Token amount paid by cash","ATTRIBUTE16":"Cheque/Cash to be Remitted","ATTRIBUTE15":null,"ATTRIBUTE19":null}],"message":"Error in reposne","status":"E"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );

       	ApexPages.StandardController controller = new ApexPages.standardController(objAcc);
        Receipt_DetailsController obj = new Receipt_DetailsController(controller);
        obj.init();
        obj.GenerateURL();
        Test.stopTest();
    }

    static testMethod void testMethod3() {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='12345');
        insert objAcc;
        
        Test.startTest();
        PageReference pageRef = Page.BookingUnitPage;
        pageRef.getParameters().put('id', String.valueOf(objAcc.Id));
        Test.setCurrentPage(pageRef);

        SOAPCalloutServiceMock.returnToMe = new Map<String, ReceiptDetails1.GetReceiptDetailsResponse_element>();
        ReceiptDetails1.GetReceiptDetailsResponse_element response1 = new ReceiptDetails1.GetReceiptDetailsResponse_element();
        response1.return_x = '';
        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );

       	ApexPages.StandardController controller = new ApexPages.standardController(objAcc);
        Receipt_DetailsController obj = new Receipt_DetailsController(controller);
        obj.init();
        obj.GenerateURL();
        Test.stopTest();
    }
}