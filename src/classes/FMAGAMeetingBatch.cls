/*-------------------------------------------------------------------------------------------------
Description: Batch to crete tasks for different AGA meetings
====================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------
1.0     | 31-12-2018       | Lochana Rajput   | 1. Added logic to create tasks
====================================================================================================
*/
public class FMAGAMeetingBatch implements Database.Batchable<sObject> {

	public Database.QueryLocator start(Database.BatchableContext BC) {
		Date today = Date.today();
		String query = 'SELECT Id,Property__c, ';
		query += ' Meeting_Date__c,Meeting_Time__c,Meeting_Venue__c,Type_of_Meeting__c';
		query += ' FROM AGA_Schedule__c ';
		query += ' WHERE Property__c != NULL';
		query += ' AND Meeting_Date__c =: today';
		return Database.getQueryLocator(query);
	}

   	public void execute(Database.BatchableContext BC, List<AGA_Schedule__c> scope) {
		List<FM_Case__c> lstFMCases = new List<FM_Case__c>();
		Map<Id, Id> mapPropertyId_FMUserId = new Map<Id, Id>();
		Set<Id> propertyIds = new Set<Id>();
		Set<Id> AGAScheduleIds = new Set<Id>();
		Map<Id, String> mapPropertyId_FMUserRole = new Map<Id, String>();
		for(AGA_Schedule__c obj : scope) {
			propertyIds.add(obj.Property__c);
			AGAScheduleIds.add(obj.Id);
		}
		List<Task> lstTasks = new List<Task>();
		for(Location__c obj : [SELECT Id,Property_Name__c,(SELECT FM_User__c,FM_Role__c from FM_Users__r
								WHERE (FM_Role__c = 'Property Manager'
									OR FM_Role__c = 'Master Community Property Manager') LIMIT 1)
							   FROM Location__c
							   WHERE Recordtype.Name = 'Building'
							   AND Property_Name__c IN: propertyIds]) {
			if(obj.FM_Users__r.size() > 0) {
			   	mapPropertyId_FMUserId.put(obj.Property_Name__c, obj.FM_Users__r[0].FM_User__c);
			   	mapPropertyId_FMUserRole.put(obj.Property_Name__c, obj.FM_Users__r[0].FM_Role__c);
		   	}
	   	}
		lstFMCases = [SELECT Id, Name,Property__c,Meeting_Type__c
					  FROM FM_Case__c
					  WHERE AGA_Schedule__c IN : AGAScheduleIds];
		System.debug('==lstFMCases==='+lstFMCases);
		for(FM_Case__c obj : lstFMCases) {
			//Create tasks to check if meeting was conducted
			// if(obj.Meeting_Type__c == 'AGA meeting' || obj.Meeting_Type__c == 'Board Meeting'
			// || obj.Meeting_Type__c == 'Resident Gathering') {
				Task objTask = new Task();
				objTask.whatId = obj.Id;
				objTask.Subject = Label.FM_MeetingConductedLabel;
				objTask.Status = 'Not Started';
				objTask.Priority = 'Normal';
				objTask.Process_Name__c = 'AGA Process';
				objTask.ActivityDate = Date.today().addDays(2);
				if(mapPropertyId_FMUserRole.containsKey(obj.Property__c)) {
					objTask.Assigned_User__c = mapPropertyId_FMUserRole.get(obj.Property__c);
				}
				else if(mapPropertyId_FMUserId.containsKey(obj.Property__c)) {
					objTask.OwnerID = mapPropertyId_FMUserId.get(obj.Property__c);
				}
				lstTasks.add(objTask);
			// }
		}//for
		insert lstTasks;
	}

	public void finish(Database.BatchableContext BC) {

	}

}