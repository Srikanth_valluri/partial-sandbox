global class DAMAC_WA_REMOTE{

    public DAMAC_WA_REMOTE(DAMAC_SEND_WA_MESSAGE controller) {

    }
    public DAMAC_WA_REMOTE(NEXMO_WA_MESSAGING controller) {

    }
    


    
        @remoteAction
        global Static String getAttachmentURL (Id attachmentId) {
            System.debug ('attachmentId : '+attachmentId);
            
            ContentVersion attachment = [SELECT ContentDocumentId, PathOnClient, ContentUrl FROM ContentVersion WHERE Id=: attachmentId];
            System.Debug (attachment);
            
            
            ContentDistribution cd = new ContentDistribution();
            cd.name = attachment.PathOnClient;
            cd.ContentVersionId = attachment.Id;
            cd.PreferencesAllowOriginalDownload = false;
            cd.PreferencesAllowPDFDownload = false;
            cd.PreferencesAllowViewInBrowser = true;
            insert cd;
            System.Debug (cd);
            
            cd = [SELECT DistributionPublicUrl FROM ContentDistribution WHERE ID = :cd.Id];
            System.debug(cd.DistributionPublicUrl);
            
            return cd.DistributionPublicUrl;
        }
}