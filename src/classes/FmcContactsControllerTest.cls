@isTest
private class FmcContactsControllerTest {

    @isTest
    static void testController() {
        User portalUser = CommunityTestDataFactory.createPortalUser();
        Id portalAccountId = [SELECT Id, Contact.AccountId FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;

        System.debug('portalAccountId = ' + portalAccountId);

        Location__c location = new Location__c(
            Name = 'Test Location',
            Location_ID__c = 'LOC',
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0
        );
        insert location;

        insert new List<FM_User__c> {
            new FM_User__c(
                Building__c = location.Id,
                FM_Role__c = 'FM Admin',
                FM_User__c = UserInfo.getUserId(),
                Is_Emergency_Contact__c = true
            ),
            new FM_User__c(
                Building__c = location.Id,
                FM_Role__c = 'Property Manager',
                FM_User__c = UserInfo.getUserId(),
                Is_Emergency_Contact__c = false
            )
        };

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(portalAccountId, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            unit.Inventory__c = inventory.Id;
        }
        insert lstBookingUnit;

        insert TestDataFactory_CRM.createActiveFT_CS();

        CustomerCommunityUtils.customerAccountId = portalAccountId;

        PageReference currentPage = Page.CommunityPortal;
        currentPage.getParameters().put('view', 'Contacts');
        Test.setCurrentPage(currentPage);

        Test.startTest();
            System.runAs(portalUser) {
                FmcContactsController controller = new FmcContactsController();
            }
        Test.stopTest();
    }

    @isTest
    private static void testGoogleNearbyPlacesNullId() {
        Test.startTest();
            GoogleNearbyPlaces.Response response = FmcContactsController.getNearbyPlaces(NULL);
            System.assertEquals(NULL, response);
        Test.stopTest();
    }

    @isTest
    private static void testGoogleNearbyPlacesWithoutCoordinates() {
        Location__c location = new Location__c(
            Name = 'Test Location',
            Location_ID__c = 'LOC'
        );
        insert location;

        Test.startTest();
            GoogleNearbyPlaces.Response response = FmcContactsController.getNearbyPlaces(location.Id);
            System.assertEquals(NULL, response);
        Test.stopTest();
    }

    @isTest
    private static void testGoogleNearbyPlacesWithCoordinates() {
        Location__c location = new Location__c(
            Name = 'Test Location',
            Location_ID__c = 'LOC',
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s = 0
        );
        insert location;

        Test.startTest();
            GoogleNearbyPlaces.Response response = FmcContactsController.getNearbyPlaces(location.Id);
            System.assertNotEquals(NULL, response);
        Test.stopTest();
    }

    @isTest
    private static void testGooglePlaceDetailsNullId() {
        Test.startTest();
            GoogleNearbyPlaces.Place response = FmcContactsController.getPlaceDetails(NULL);
            System.assertEquals(NULL, response);
        Test.stopTest();
    }

    @isTest
    private static void testGooglePlaceDetailsWithPlaceId() {
        Test.startTest();
            GoogleNearbyPlaces.Place response = FmcContactsController.getPlaceDetails('place_id');
            System.assertNotEquals(NULL, response);
        Test.stopTest();
    }

}