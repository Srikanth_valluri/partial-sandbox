public without sharing class AgentPortalIntEmail {
	public String ToEmail{get;set;}
	public String CCEmail{get;set;}
	public String EmailSubject{get;set;}
	public String Emailbody{get;set;} 
	public String UnintPlans{get;set;} 
	public String Collaterals{get;set;} 
	public string invid{set;get;}
	
	public string UnitPlan{set;get;}
	public string FloorPlan{set;get;}
	public string StrMD{set;get;}
	
	public Inventory__c inventory{set;get;}
    public AgentPortalIntEmail(ApexPages.StandardController controller) {
    	//ToEmail = Userinfo.getUserEmail();
    	invid = controller.getId();
        system.debug('***invid'+invid);
        for(Inventory__c thisInventory:[SELECT Selling_Price__c,List_Price_calc__c, Special_Price_calc__c,Property_Type__c,
                                        Building_Name__c,Marketing_Name__c,Unit__c,Property_Status__c,
                                        Unit_Type__c,IPMS_Bedrooms__c,Area_Sqft_2__c,Price_Per_Sqft__c,
                                        Anticipated_Completion_Date__c,Construction_Status__c,
                                        Land_Registration_Fee_A__c,Title_Deed_B__c,
                                        OQOOD_Fee_C__c,DSR_Fees_D__c,Total_Fees_to_DLD__c,
                                        Unit_Plan__c,Floor_Plan__c,Plot_Plan__c,Floor_Package_Name__c,Floor_Package_Type__c,
                                        Plot_Area__c,Bedroom_Type__c,Floor__c
                                        from Inventory__c 
                                    WHERE Id = : invid]){
                inventory = thisInventory;                                 
       }
       List<Inventory__c> listINT = new List<Inventory__c>();
       listINT.add(inventory);
       for(Inventory__c objINT : listINT){
       	   if(!String.isEmpty(objINT.Unit__c)){
       	   	  UnitPlan = objINT.Unit__c.remove('/');
       	   	  System.debug('...UnitPlan...'+UnitPlan);
       	   } 
       	   if(!String.isEmpty(objINT.Floor__c)){
       	   	  FloorPlan = objINT.Floor__c.remove('/');
       	   	  System.debug('...FloorPlan...'+FloorPlan);
       	   }
       	   if(!String.isEmpty(objINT.Marketing_Name__c)){
       	   	  StrMD = objINT.Marketing_Name__c.remove(' ');
       	   	 // StrMD = StrMD.remove('-');
       	   	  StrMD = StrMD.remove('/');
       	   	  StrMD = StrMD.toLowerCase();
       	   	  System.debug('...StrMD...'+StrMD);
       	   }
       }
        
    }
    public void SendEmail(){

    	// Reference the attachment page, pass in the account ID
		PageReference pdf = Page.AgentPortalUnitDetailsPDF;
		pdf.getParameters().put('id',invid);
		pdf.setRedirect(true);
		Blob b;
		if(!test.isRunningTest()){
		b = pdf.getContent();
		}
		else{
			b = blob.valueof('TEST');
		}
		Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
		efa.setFileName('attachment.pdf');
		efa.setBody(b);
    	System.debug('...Emailbody...'+Emailbody);
    	System.debug('...inside the method..,');
    	Id strID = (Apexpages.currentPage().getParameters().get('Id'));
    	List<string> SendTo=new  List<string>();
        List<Messaging.SingleEmailMessage> mails=new  List<Messaging.SingleEmailMessage>();
    	System.debug('...ToEmail...'+ToEmail);
    	System.debug('...CCEmail...'+CCEmail);
    	System.debug('...EmailSubject...'+EmailSubject);
    	System.debug('...Emailbody...'+Emailbody);
    	//To address
    	List<String> emailaddress = new List<String>();
    	if(!String.isEmpty(ToEmail) && ToEmail.Contains(',')){
    	emailaddress= ToEmail.split(',');
    	}
    	else{
    		emailaddress.add(ToEmail);
    	}
    	
    	//cc address
    	List<String> emailCCaddress = new List<String>();
    	if(!String.isEmpty(CCEmail) && CCEmail.Contains(',')){
    	emailCCaddress= CCEmail.split(',');
    	}
    	else{
    		emailCCaddress.add(CCEmail);
    	}
    	try{
                //SendTo.add(emailaddress);
               // emailaddress.add('pratiksha.n@accely.com');
                System.debug('...SendTo...'+SendTo);
                Messaging.SingleEmailMessage mail=new  Messaging.SingleEmailMessage();
                if(!String.isEmpty(ToEmail)){
                System.debug('...emailaddress...'+emailaddress);
                mail.setToAddresses(emailaddress);
                }
                if(!String.isEmpty(CCEmail)){
                System.debug('...emailCCaddress...'+emailCCaddress);
                mail.setCcAddresses(emailCCaddress);
                } 
                mail.setSenderDisplayName('Damac Property ');
                mail.setReplyTo('noreply@Damacgroup.com');
                mail.setSubject(EmailSubject);
                mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
                System.debug('...body...'+Emailbody);
                mail.setHtmlBody(Emailbody);
                mails.add(mail);  
                Messaging.sendEmail(mails);
        
    } 
      
    catch(Exception Ex){
        System.debug('Error Message in AgentEmailSend---- '+Ex.getMessage());
        System.debug('Error Message @Line Number---- '+Ex.getLineNumber());
    }
    	
    }
}