global class UpdateEmailUserNameonPortalUserBatch implements Database.Batchable<sObject> {
    String query;
    public static Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
     
    global Database.QueryLocator start(Database.BatchableContext BC) {
    
        query = 'SELECT Id,PersonEmail,Party_ID__c,Email__pc,RecordTypeId,OwnerId FROM Account WHERE RecordTypeId =: personAccRTId AND Party_ID__c != \'\' AND Email__pc != null AND IsCustomerPortal = true ';
        
        //AND Id = \'0012500000hAWTQAA4\'
        
        system.debug( ' query : ' + query );
        
        return Database.getQueryLocator(query);
    }
    
    global void execute( Database.BatchableContext BC, List<Account> accLsLst ) {
        system.debug( ' accLsLst : ' + accLsLst.size() );

        List<Account> lstAcc = new List<Account>();
        Set<String> setPartyId = new Set<String>();
        Map<String,Account> mapPartyIdAcc  = new Map<String,Account>();     
        
        system.debug( ' accLsLst : ' + accLsLst );
        for( Account accObj : accLsLst ) {
            system.debug( ' accObj : ' + accObj );
            if( accObj.RecordTypeId == personAccRTId 
                && accObj.Party_ID__c != '' 
                && accObj.Email__pc != null
                && string.valueOf(accObj.OwnerId).startsWith('005') ) {
        

                setPartyId.add( accObj.Party_ID__c );
                mapPartyIdAcc.put(accObj.Party_ID__c ,accObj);
                
                if( accObj.PersonEmail != null) {
                    if( accObj.Email__pc != accObj.PersonEmail ) {
                        accObj.PersonEmail = accObj.Email__pc;
                    }
                }else{
                    accObj.PersonEmail = accObj.Email__pc;
                }
                
                lstAcc.add(accObj);
            }
        }
        
        system.debug( ' setPartyId : ' + setPartyId );
        system.debug( ' setPartyId.size() : ' + setPartyId.size() );
        system.debug( ' mapPartyIdAcc : ' + mapPartyIdAcc );
        system.debug( ' mapPartyIdAcc.size() : ' + mapPartyIdAcc.size() );
        
        if ( setPartyId != null && setPartyId.size() > 0) {
            
            Id profileId = [select id from profile where name = 'Customer Community Login User(Use this)' LIMIT 1].id;
            
            system.debug( ' profileId : ' + profileId );
                
            
            List<User> lstUser = new List<User>();

            for( User objUser : [SELECT Id ,IsActive,Email,Username,Party_Id__c,Is_User_Updated__c
                                   FROM User
                                  WHERE IsActive = true
                                    AND Party_Id__c IN: setPartyId
                                    AND IsPortalEnabled = true 
                                    AND ProfileId =: profileId ] ) {
                system.debug( ' objUser : ' + objUser );
                if( objUser.Party_ID__c != '' 
                    && mapPartyIdAcc.get(objUser.Party_ID__c).Email__pc != null ) {
                    
                    if( objUser.Email != null ) {
                        if( mapPartyIdAcc.get(objUser.Party_ID__c).Email__pc != objUser.Email ) {
                            objUser.Email = mapPartyIdAcc.get(objUser.Party_ID__c).Email__pc;
                            objUser.Is_User_Updated__c = true;
                        }
                    }else{
                        objUser.Email = mapPartyIdAcc.get(objUser.Party_ID__c).Email__pc;
                        objUser.Is_User_Updated__c = true;
                    }

                    if( objUser.Username != null ) {
                        if( mapPartyIdAcc.get(objUser.Party_ID__c).Email__pc != objUser.Username ) {
                            objUser.Username = mapPartyIdAcc.get(objUser.Party_ID__c).Email__pc;
                            objUser.Is_User_Updated__c = true;
                        }
                    }else{
                        objUser.Username = mapPartyIdAcc.get(objUser.Party_ID__c).Email__pc;
                        objUser.Is_User_Updated__c = true;
                    }
                    
                    
                    lstUser.add(objUser);
                }
            }


            
            if ( lstUser != null && lstUser.size() > 0) {
                //update lstUser;
                Database.SaveResult[] srList = Database.update(lstUser, false);
                Set<Id> errorIds = new Set<Id>();
                List<Error_Log__c> lstError = new List<Error_Log__c>();
                for (Database.SaveResult sr : srList) {
                    system.debug('sr : '+sr);
                    if (!sr.isSuccess()) {
                        errorIds.add(sr.getId());
                        Error_Log__c objError = new Error_Log__c();
                        objError.Error_Details__c = sr.getId() + sr.getErrors()[0].getMessage();
                        System.debug('sr.getId(): ' + sr.getId());
                        lstError.add(objError);
                    }
                    
                    System.debug('errorIds: ' + errorIds);
                }
                if ( lstError != null && lstError.size() > 0) {
                    //insert lstError;
                    //createErrorLog(errorIds);
                }
            }
        }
        
        /*if ( lstAcc != null && lstAcc.size() > 0) {
            update lstAcc;
        }*/

        
        

    }
    
    /*@future(callout = false)
    public static void updateUser( Set<Id> accId ) {
        Set<String> setPartyId = new Set<String>();
        Map<String,Account> mapPartyIdAcc  = new Map<String,Account>();
    
        for( Account accObj :[SELECT Id,PersonEmail,Party_ID__c,Email__pc,RecordTypeId,OwnerId FROM Account WHERE Id IN:accId ] ) {
            system.debug( ' accObj : ' + accObj );
            if( accObj.Party_ID__c != '' ) {
                setPartyId.add( accObj.Party_ID__c );
                mapPartyIdAcc.put(accObj.Party_ID__c ,accObj);
            }
        }
        if ( setPartyId != null && setPartyId.size() > 0) {
            
            Id profileId = [select id from profile where name = 'Customer Community Login User(Use this)'].id;
            List<User> lstUser = new List<User>();

            for( User objUser : [SELECT Id ,IsActive,Email,Username,Party_Id__c
                                   FROM User
                                  WHERE IsActive = true
                                    AND Party_Id__c IN: setPartyId
                                    AND IsPortalEnabled = true 
                                    AND ProfileId =: profileId ] ) {
                if( objUser.Party_ID__c != '' 
                    && mapPartyIdAcc.get(objUser.Party_ID__c).Email__pc != null ) {
                    
                    if( objUser.Email != null ) {
                        if( mapPartyIdAcc.get(objUser.Party_ID__c).Email__pc != objUser.Email ) {
                            objUser.Email = mapPartyIdAcc.get(objUser.Party_ID__c).Email__pc;
                        }
                    }else{
                        objUser.Email = mapPartyIdAcc.get(objUser.Party_ID__c).Email__pc;
                    }

                    if( objUser.Username != null ) {
                        if( mapPartyIdAcc.get(objUser.Party_ID__c).Email__pc != objUser.Username ) {
                            objUser.Username = mapPartyIdAcc.get(objUser.Party_ID__c).Email__pc;
                        }
                    }else{
                        objUser.Username = mapPartyIdAcc.get(objUser.Party_ID__c).Email__pc;
                    }
                    
                    
                    lstUser.add(objUser);
                }
            }


            
            if ( lstUser != null && lstUser.size() > 0) {
                //update lstUser;
                Database.SaveResult[] srList = Database.update(lstUser, false);
                Set<Id> errorIds = new Set<Id>();
                List<Error_Log__c> lstError = new List<Error_Log__c>();
                for (Database.SaveResult sr : srList) {
                    system.debug('sr : '+sr);
                    if (!sr.isSuccess()) {
                        errorIds.add(sr.getId());
                        Error_Log__c objError = new Error_Log__c();
                        objError.Error_Details__c = sr.getId() + sr.getErrors()[0].getMessage();
                        System.debug('sr.getId(): ' + sr.getId());
                        lstError.add(objError);
                    }
                    
                    System.debug('errorIds: ' + errorIds);
                }
                if ( lstError != null && lstError.size() > 0) {
                    insert lstError;
                    //createErrorLog(errorIds);
                }
            }
        }
    }*/
    
    
    /*@future(callout = false)
    public static void createErrorLog( Set<Id> errorIds ) {
        for ( Database.SaveResult sr : [Select ] ) {
            system.debug('sr : '+sr);
            if (!sr.isSuccess()) {
                errorIds.add(sr.getId());
                Error_Log__c objError = new Error_Log__c();
                objError.Error_Details__c = sr.getId() + sr.getErrors()[0].getMessage();
                System.debug('sr.getId(): ' + sr.getId());
                lstError.add(objError);
            }
            
            System.debug('errorIds: ' + errorIds);
        }
        if ( lstError != null && lstError.size() > 0) {
            insert lstError;
        }
    }*/
    
    global void finish(Database.BatchableContext BC) {

    }
}