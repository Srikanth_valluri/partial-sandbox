/*
* Description - Test class developed for 'RentalPoolTerminationService'
*
* Version            Date            Author            Description
* 1.0              28/02/2018        Ashish           Initial Draft
*/

@isTest 
private class RentalPoolTerminationServiceTest {

    static testMethod void testGenerateLetterOfTermination1() {
        // TO DO: implement unit test
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId() );
        objCase.OwnerId = UserInfo.getUserId();
        insert objCase ;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        test.startTest();
            RentalPoolTerminationService objServ = new RentalPoolTerminationService( stdController );
            objServ.generateLetterOfTermination();
        test.stopTest();        
    }
    
    static testMethod void testGenerateLetterOfTermination2() {
        // TO DO: implement unit test
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId() );
        objCase.OwnerId = UserInfo.getUserId();
        objCase.Status = 'HE Approved';
        objCase.Rental_Pool_Termination_Status__c = 'Approved from Hospitality Executive';
        objCase.Final_Settlement_Amount__c = 10 ;
        objCase.Final_Termination_Date__c = system.today();
        insert objCase ;
        
        Task objTask = TestDataFactory_CRM.createTask( (sObject)objCase, ' Letter of Termination ','CRE', 'Rental Pool Termination', system.today() );
        insert objTask ;
        
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockRPTermination() );
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        test.startTest();
            RentalPoolTerminationService objServ = new RentalPoolTerminationService( stdController );
            objServ.generateLetterOfTermination();
        test.stopTest();        
    }
    
    static testMethod void testFetchFMCharges() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId() );
        objCase.OwnerId = UserInfo.getUserId();
        insert objCase ;
        
        NSIBPM__Service_Request__c objReq = TestDataFactory_CRM.createServiceRequest();
        insert objReq ;
        
        List<Booking__c> lstBooking =  TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objReq.Id,1 );
        insert lstBooking ;
        
        List<Booking_Unit__c> lstUnits = TestDataFactory_CRM.createBookingUnits( lstBooking, 1 );
        insert lstUnits ;
        
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockRPTermination() );
        test.startTest();
            RentalPoolTerminationService.fetchFMCharges( lstUnits[0] );
            RentalPoolTerminationService.fetchFMCharges( null );
        test.stopTest();
    }
    
    static testMethod void testGenerateCustomerRequestForm() {
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockRPTermination() );
        test.startTest();
            RentalPoolTerminationService.generateCustomerRequestForm( new Booking_Unit__c(), new UnitDetailsService.BookinUnitDetailsWrapper() );
        test.stopTest();
    }
}