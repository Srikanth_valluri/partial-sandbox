/****************************************************************************************************
* Name          : UpdatePortalUsers                                                                 *
* Description   : Create and Update Portal User Records in Cse of any change in the Contact Records *
* Created Date  : 22-07-2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE        COMMENTS                                                    *
* 1.0   Craig Lobo          22-07-2018  Initial Draft.                                              *
*****************************************************************************************************/
public without sharing class UpdatePortalUsers {

    @future
    public static void modifyProtalUserRecords(Set<Id> contactIdSet) {
        System.debug('contactIdSet >>>>> ' + contactIdSet);
        if (contactIdSet != null && !contactIdSet.isEmpty() ) {
            List<Contact> contactsList = [SELECT Id, Name, Email, Owner__c, Portal_Administrator__c, 
                                                 Authorised_Signatory__c, Agent_Representative__c, 
                                                 FirstName, LastName
                                            FROM Contact
                                           WHERE Id IN :contactIdSet
            ];
            System.debug('contactsList >>>>> ' + contactsList);

            Map<Id, User> contactIdUserMap = new Map<Id, User>();
            for (User userRec : [  SELECT Id, Name, Email, ProfileId, Profile.Name, ContactId,
                                          FirstName, LastName 
                                     FROM User
                                    WHERE ContactId IN :contactIdSet
                                      AND isActive = true
                                      AND ContactId != null
            ]) {
                contactIdUserMap.put(userRec.ContactId, userRec);
            }
            System.debug('contactIdUserMap >>>>> ' + contactIdUserMap);

            if (!contactIdUserMap.isEmpty()) {
                Map<String, Id> profileMap = getCommunityProfiles();
                System.debug('profileMap >>>>> ' + profileMap);
                if(profileMap != null && !profileMap.isEmpty()) {

                    for (Contact conRec : contactsList) {
                        System.debug('conRec.Email >>>>> ' + conRec.Id);
                        if (contactIdUserMap.containsKey(conRec.Id)) {

                            // Map Contact Email to User Email
                            if (contactIdUserMap.get(conRec.Id).Email != conRec.Email) {
                                System.debug('conRec.Email >>>>> ' + conRec.Email);
                                contactIdUserMap.get(conRec.Id).Email = conRec.Email;
                            }

                            // Map Contact First Name to User First Name
                            if (contactIdUserMap.get(conRec.Id).FirstName != conRec.FirstName) {
                                System.debug('conRec.FirstName >>>>> ' + conRec.FirstName);
                                contactIdUserMap.get(conRec.Id).FirstName = conRec.FirstName;
                            }

                            // Map Contact Last Name to User Last Name
                            if (contactIdUserMap.get(conRec.Id).LastName != conRec.LastName) {
                                System.debug('conRec.LastName >>>>> ' + conRec.LastName);
                                contactIdUserMap.get(conRec.Id).LastName = conRec.LastName;
                            }

                            // Map Contact Profile to User Profile
                            String updatedProfileName = getUserProfile(conRec);
                            if (String.isNotBlank(updatedProfileName)) {
                                if (profileMap.containsKey(updatedProfileName)) {
                                    System.debug('profileMap.get(updatedProfileName) >>>>> ' + profileMap.get(updatedProfileName));
                                    contactIdUserMap.get(conRec.Id).ProfileId = profileMap.get(updatedProfileName);
                                }
                            }
                        }
                    }
                    System.debug('contactIdUserMap.values() >>>>> ' + contactIdUserMap.values());
                    System.debug('UpdatePortalUsers'+contactIdUserMap.values());
                    update contactIdUserMap.values();
                }
            }
        }
    }

    //@future
    public static void createPortalUser(Set<Id> pContactsList) {
        System.debug('#### pContactsList = ' + pContactsList);
        System.debug('#### getProfileId = ' + UserInfo.getProfileId());
        List<User> portalUserList = new List<User>();
        List<Contact> contactsList = new List<Contact>();
        contactsList = [ SELECT Id, Name, Email, Owner__c, Portal_Administrator__c, 
                                Authorised_Signatory__c, Agent_Representative__c, 
                                FirstName, LastName, Account_RecordType__c, AmendmentID__c,
                                Account.Name, Account.Agency_Tier__c, Account.Eligible_For_Tier_Program__c,
                                MobilePhone
                           FROM Contact
                          WHERE Id IN :pContactsList
        ];
        System.debug('contactsList >>>> ' + contactsList);

        Id agentProfileId = [SELECT Id, Name 
                               FROM Profile 
                              WHERE Name = 'Customer Community - Agent' 
                              LIMIT 1 ].Id;
        System.debug('agentProfileId >>>> ' + agentProfileId);

        Database.DMLOptions dlo = new Database.DMLOptions();
        dlo.EmailHeader.triggerUserEmail = true;
        dlo.EmailHeader.triggerAutoResponseEmail = true;
        
        for (Contact con : contactsList) {
            // Check if User belongs to Corporate Agency and added externally (Not via SR, Only Agents)
            System.debug('con >>>> ' + con);
            if (con.Account_RecordType__c == 'Corporate Agency'
                && con.Agent_Representative__c
                && con.AmendmentID__c == null
                && !con.Owner__c
                && !con.Portal_Administrator__c
                && !con.Authorised_Signatory__c
            ) {
                
                try {
                    // New User
                    User portalUser = new User();
                    String aliasVal = String.valueOf(con.Id);
                    portalUser.Alias = aliasVal.substring(0, 5);
                    portalUser.Email = con.Email;
                    portalUser.Username = con.Email;
                    //portalUser.username = getUserName(con);
                    portalUser.Emailencodingkey = 'UTF-8';
                    portalUser.Timezonesidkey = 'Asia/Dubai';
                    portalUser.FirstName = con.FirstName;
                    portalUser.LastName = con.LastName;
                    portalUser.ContactId = con.Id;
                    portalUser.Languagelocalekey = 'en_US';
                    portalUser.Localesidkey = 'en_GB';
                    portalUser.ProfileId = agentProfileId;
                    portalUser.Account_Name__c = con.Account.Name;
                    portalUser.MobilePhone = con.MobilePhone;
                    portalUser.Agency_Tier_Status__c = con.Account.Agency_Tier__c;
                    portalUser.Eligible_for_Tier_Program__c = con.Account.Eligible_For_Tier_Program__c;
                    portalUser.DefaultCurrencyIsoCode = 'AED';
                    portalUser.setOptions(dlo);
                    System.debug('User >>>> ' + portalUser);
                    portalUserList.add(portalUser);
                }catch(exception ex){
                    system.debug('####HOLA Exception at line number = '+ex.getLineNumber()+' , Exception message = '+ex.getMessage());
                }
            }
        }
        System.debug('portalUserList==>' + portalUserList);
        if (!portalUserList.isEmpty()) {
            insert portalUserList;
        }
    }

    public static String getUserProfile(Contact con) {
        String profileName = 'Customer Community - Agent';
        if (con.Portal_Administrator__c) {
            profileName = 'Customer Community - Admin';
        }
        if (con.Agent_Representative__c) {
            profileName = 'Customer Community - Agent';
        }
        if (con.Authorised_Signatory__c) {
            profileName = 'Customer Community - Auth Officer';
        }
        if (con.Owner__c) {
            profileName = 'Customer Community - Owner';
        }
        if (con.Agent_Representative__c && con.Portal_Administrator__c) {
            profileName = 'Customer Community - Agent + Admin';
        }
        if (con.Authorised_Signatory__c && con.Portal_Administrator__c) {
            profileName = 'Customer Community - Auth + Admin';
        }
        if (con.Authorised_Signatory__c && con.Agent_Representative__c) {
            profileName = 'Customer Community - Auth + Agent';
        }
        if (con.Agent_Representative__c && con.Portal_Administrator__c && con.Authorised_Signatory__c) {
            profileName = 'Customer Community - Agent + Admin + Auth';
        }
        return profileName;
    }

    public static Map<String, Id> getCommunityProfiles() {
        List<String> portalProfileName = new List<String> {
            'Customer Community - Admin', 
            'Customer Community - Agent', 
            'Customer Community - Agent + Admin', 
            'Customer Community - Agent + Admin + Auth', 
            'Customer Community - Auth + Admin',
            'Customer Community - Auth + Agent', 
            'Customer Community - Owner',
            'Customer Community - Auth Officer' //ALOK 27/Dec/2017 Added for Portal User Creation Bug
        };
        Map<String, Id> profileMap = new Map<String, Id>();
        for (Profile p : [SELECT Id, Name FROM Profile WHERE Name IN :portalProfileName]) {
            profileMap.put(p.Name, p.Id);
        }
        return profileMap;
    }

    /*public static string getUserName(Contact c) {
        String username = '';
        if (c.FirstName != NULL) {
            username += c.FirstName.replaceAll( '\\s+', '');
        }
        if (c.LastName != NULL) {
            username += c.LastName.replaceAll( '\\s+', '');
        }
        username += randomWithLimit(9999);
        username += '@damacagents.com';
        return username;
    }*/

    /*public static String randomWithLimit(Integer upperLimit) {
        Integer rand = Math.round(Math.random() * 1000);
        return String.valueOf(Math.mod(rand, 9999)).leftpad(4, '0');
    }*/

}