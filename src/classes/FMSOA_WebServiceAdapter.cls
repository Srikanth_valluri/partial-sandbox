public class FMSOA_WebServiceAdapter extends GenericWebServiceAdapter implements WebServiceAdapter{
    
    public String url;
    
    public override WebServiceAdapter call(){
        super.call();
        String regID_BUID = parameters.get('regID_BUID');
        system.debug('!!!!!!!!regID_BUID'+regID_BUID);
        String BUId, regID;
        list<UploadMultipleDocController.MultipleDocRequest> wrapperLst = 
            new list<UploadMultipleDocController.MultipleDocRequest>();
        if(String.isNotBlank(regID_BUID)) {
            BUId = regID_BUID.substringBefore('-');
            regID = regID_BUID.substringAfter('-');
        }
        String strURL = FmIpmsRestServices.getUnitSoaByRegistrationId(regID) ;
        if( String.isNotBlank( strURL ) ) {
            wrapperLst.add( FM_Utility.makeWrapperObject( getDocumentBodyAsString( strURL ) ,
                Label.FM_SOA + '.pdf', Label.FM_SOA + '.pdf', BUId, 
                String.valueOf( wrapperLst.size() + 1 ) ) );
        }
        if( !wrapperLst.isEmpty() ) {
            UploadMultipleDocController.data objResponse = 
                PenaltyWaiverService.uploadDocumentsOnCentralRepo( wrapperLst, 'FM' );
            if( objResponse != NULL ) {
                if( objResponse.data != NULL && String.isNotBlank( objResponse.status ) ) {
                     for( UploadMultipleDocController.MultipleDocResponse objFile : objResponse.data ) {
                          this.url = objFile.url;
                     }
                }
            }
        }
        return this;
    }
    
    public override String getResponse(){
        super.getResponse();
        return this.url;
    }
    
    private static String getDocumentBodyAsString( String strURL ) {
        if( String.isNotBlank( strURL ) ) {
            HttpRequest req = new HttpRequest();
            req.setEndpoint( strURL );
            req.setMethod('GET');
            req.setHeader('Content-Type', 'application/pdf');
            req.setTimeout(60000);
            HTTPResponse res = new Http().send( req );
            //return res.getBody();
            String strBody = EncodingUtil.base64Encode( res.getBodyAsBlob() );
            return strBody ;
        }
        return '';
    }
}