@isTest
Public Class Damac_AssignRecommendedRmBatch_Test {
    public static testMethod void method1() {
        
        
        Id inquiryRecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
        Inquiry__c inq = New Inquiry__c ();
            inq.First_Name__c = 'testInq';
            inq.Preferred_Language__c = 'English';
            inq.Last_Name__c ='inqLast';
            inq.Inquiry_Source__c = 'Prospecting';
            inq.Primary_Contacts__c = 'Mobile Phone';
            inq.Mobile_CountryCode__c = 'India: 0091';
            inq.Mobile_Phone__c = '1236547890';
            inq.Mobile_Phone_Encrypt__c = '1223467886';
            inq.Email__c = 'test@gmail.com';
            inq.recordTypeId = inquiryRecordTypeId;
            //inq.Sales_Office__c = 'RIYADH';            
        insert inq;
        Office_Meeting__c offcMeet = New Office_Meeting__c ();
            offcMeet.Inquiry__c = inq.id;
            offcMeet.CreatedDate  = system.today();
        insert offcMeet;
        Damac_AssignRecommendedRmBatch obj = new Damac_AssignRecommendedRmBatch();
        Database.executeBatch(new Damac_AssignRecommendedRmBatch(), 1);
    }
}