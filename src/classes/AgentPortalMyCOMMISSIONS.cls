/*
Name : Pratiksha Narvekar
Date : 2-08-2017
*/
 
public with sharing  class AgentPortalMyCOMMISSIONS{
public Object AmountPaid{get;set;}
public Integer AmountINPROGRESS{get;set;}
   /*
    public List<Agent_Commission__c> getAgentCommissionValue(){  
       List<Agent_Commission__c> agentCommision  = new List<Agent_Commission__c>();      
        String userID = userinfo.getUserid();
        List<User> objUser = new List<User>();
         objUser = [select ID,
                          AccountId
                   from User
                   where ID =: userID];
         System.debug('...objUser ...'+objUser+'..acc...'+objUser[0].AccountId );
        agentCommision =[select ID,
                                Customer_Name__c,
                                Agency__c,
                                Booking_Unit__r.Name,
                                Commission_Status__c,
                                Amount__c,
                                Check_Date__c
                         from Agent_Commission__c
                         where Agency__c =: objUser[0].AccountId];
        System.debug('...agentCommision 2...'+agentCommision);
        return agentCommision;
    }
    
    */
    public List<Commissions> getAgentCommission(){  
    List<Agent_Commission__c> agentCommision  = new List<Agent_Commission__c>(); 
    List<AggregateResult> agentCommision2  = new List<AggregateResult>(); 
    List<Commissions> listCommision  = new List<Commissions>();     
        String userID = userinfo.getUserid();
        List<User> objUser = new List<User>();
         objUser = [select ID,
                          AccountId
                   from User
                   where ID =: userID];
         System.debug('...objUser ...'+objUser+'..acc...'+objUser[0].AccountId );
        agentCommision =[select ID,
                                Customer_Name__c,
                                Agency__c,
                                Booking_Unit__r.Name,
                                Commission_Status__c,
                                Amount__c,
                                Check_Date__c
                         from Agent_Commission__c
                         where Agency__c =: objUser[0].AccountId];
          
        agentCommision2 =[select sum(Amount__c)sum
                         from Agent_Commission__c
                         where Agency__c =: objUser[0].AccountId];
        AmountPaid = agentCommision2 [0].get('sum');   
                     
         if(!agentCommision.isEmpty()){
           for(Agent_Commission__c objComm : agentCommision){
               Commissions objCommissions = new Commissions();
               objCommissions.CustomerName = objComm.Customer_Name__c;
               objCommissions.UnitNo = objComm.Booking_Unit__r.Name;
               objCommissions.Amount = objComm.Amount__c;
               objCommissions.DateValue = objComm.Check_Date__c;   
               listCommision.add(objCommissions);
           }
         
         }
        System.debug('...agentCommision 2...'+agentCommision+'...listCommision  ...'+listCommision  );
        return listCommision  ;
    } 
    
    public class Commissions{
        public String CustomerName{get;set;}
        public String UnitNo{get;set;}
        public Decimal Amount{get;set;}
        public Date DateValue{get;set;}
    }
}