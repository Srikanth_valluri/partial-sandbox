@isTest
public class DAMAC_AMEYO_REST_SERVICES_TEST
{
    
    static testmethod void testRestServices()
    {
        Id agenTeamRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Agent Team').getRecordTypeId();
        Inquiry__c inqObj= new Inquiry__c(RecordTypeId=agenTeamRT,Pre_InquiryId__c='123456',Mobile_Phone_Encrypt__c='456123',Mobile_CountryCode__c='American Samoa: 001684',mobile_phone__c='1234',email__c='mk@gmail.com',First_Name__c='Test',Last_Name__c='Last',CR_Number__c='0987',ORN_Number__c='7842',Agency_Type__c='Corporate',isDuplicate__c=false,Organisation_Name__c = 'Oliver');
        insert inqObj;
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        String fedIden = [SELECT FederationIdentifier FROM User WHERE FederationIdentifier != NULL LIMIT 1].FederationIdentifier;
        req.requestBody = Blob.valueOf('{"inqId" : "'+inqObj.Id+'", "ssoId" : "'+fedIden+'"}') ;
        req.requestURI = '//*'; 
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_AMEYO_REST_SERVICES.doPOST();
        
        Id RSRecordTypeId = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Digital').getRecordTypeId();
        Virtual_Number__c v = new Virtual_Number__c();
        v.Active__c = true;
        v.name = '045205111';
        v.End_Date__c = System.today().addDays(60);
        v.Start_Date__c = System.today().addDays(-60);
        insert v;
        
        Campaign__c camp = new Campaign__c();
        camp.RecordTypeId = RSRecordTypeId;
        camp.Campaign_Name__c='Test Campaign';
        camp.start_date__c = System.today();
        camp.end_date__c = System.Today().addDays(30);
        camp.Marketing_start_date__c = System.today();
        camp.Marketing_end_date__c = System.Today().addDays(30);
        camp.Language__c = 'English';
        camp.Lead_Prioritization__c = 'Priority Digital';
        insert camp;
        
        JO_Campaign_Virtual_Number__c vn = new JO_Campaign_Virtual_Number__c();
        vn.Related_Campaign__c = camp.id;
        vn.Related_Virtual_Number__c = v.id;
        insert vn;
        
        req.params.put('cnum', 'db63441e3d2a71043ef5123ea5c40f6d');
        req.params.put('cdnum', '045205111');

        DAMAC_AMEYO_REST_SERVICES.doget();
        
       
    }
    
    static testmethod void temethodNeg(){
    
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '//*'; 
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_AMEYO_REST_SERVICES.doget();
        
    }
    
    static testmethod void temethodNeg1(){
        
        Id RSRecordTypeId = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Digital').getRecordTypeId();
        Virtual_Number__c v = new Virtual_Number__c();
        v.Active__c = true;
        v.name = '045205111';
        v.End_Date__c = System.today().addDays(60);
        v.Start_Date__c = System.today().addDays(-60);
        insert v;
        
        Campaign__c camp = new Campaign__c();
        camp.RecordTypeId = RSRecordTypeId;
        camp.Campaign_Name__c='Test Campaign';
        camp.start_date__c = System.today();
        camp.end_date__c = System.Today().addDays(30);
        camp.Marketing_start_date__c = System.today();
        camp.Marketing_end_date__c = System.Today().addDays(30);
        camp.Language__c = 'English';
        camp.Lead_Prioritization__c = 'Priority Digital';
        insert camp;
        
        JO_Campaign_Virtual_Number__c vn = new JO_Campaign_Virtual_Number__c();
        vn.Related_Campaign__c = camp.id;
        vn.Related_Virtual_Number__c = v.id;
        insert vn;
        
         // Setup test data
        // Create a unique UserName
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='Ameyo CTI'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles',UserName=uniqueUserName);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        //req.requestURI = '/ameyoAPI/*'; 
        req.params.put('cnum', '9a5268294693f8d79955b692d66b9ca6'); // Encrypted value for 11
        req.params.put('cdnum', '045205111');
        RestContext.request = req;
        RestContext.response = res;
        
        test.starttest();
        System.runAs(u) {
            DAMAC_AMEYO_REST_SERVICES.doget();
        }
        test.stoptest();
        
    }
    
    static testmethod void testRestServices2()
    {
        Id agenTeamRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Agent Team').getRecordTypeId();
        Inquiry__c inqObj= new Inquiry__c(RecordTypeId=agenTeamRT,Pre_InquiryId__c='123456',Mobile_Phone_Encrypt__c='456123',Mobile_CountryCode__c='American Samoa: 001684',mobile_phone__c='1234',email__c='mk@gmail.com',First_Name__c='Test',Last_Name__c='Last',CR_Number__c='0987',ORN_Number__c='7842',Agency_Type__c='Corporate',isDuplicate__c=false,Organisation_Name__c = 'Oliver');
        insert inqObj;
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        String fedIden = [SELECT FederationIdentifier FROM User WHERE FederationIdentifier != NULL LIMIT 1].FederationIdentifier;
        req.requestBody = Blob.valueOf('{"inqId" : "", "ssoId" : "'+fedIden+'"}') ;
        req.requestURI = '//*'; 
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_AMEYO_REST_SERVICES.doPOST();

        req.requestBody = Blob.valueOf('{"inqId" : "'+inqObj.Id+'", "ssoId" : "423452345"}') ;
        DAMAC_AMEYO_REST_SERVICES.doPOST();

        req.requestBody = Blob.valueOf('{"inquiryId" : "'+inqObj.Id+'", "ssoId1" : "423452345"}') ;
        DAMAC_AMEYO_REST_SERVICES.doPOST();
        
        Id RSRecordTypeId = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Digital').getRecordTypeId();
        Virtual_Number__c v = new Virtual_Number__c();
        v.Active__c = true;
        v.name = '045205111';
        v.End_Date__c = System.today().addDays(60);
        v.Start_Date__c = System.today().addDays(-60);
        insert v;
        
        Campaign__c camp = new Campaign__c();
        camp.RecordTypeId = RSRecordTypeId;
        camp.Campaign_Name__c='Test Campaign';
        camp.start_date__c = System.today();
        camp.end_date__c = System.Today().addDays(30);
        camp.Marketing_start_date__c = System.today();
        camp.Marketing_end_date__c = System.Today().addDays(30);
        camp.Language__c = 'English';
        camp.Lead_Prioritization__c = 'Priority Digital';
        insert camp;
        
        req.params.put('cnum', 'hello2q32454235');
        req.params.put('cdnum', '045205111');

        DAMAC_AMEYO_REST_SERVICES.doget();
        
        JO_Campaign_Virtual_Number__c vn = new JO_Campaign_Virtual_Number__c();
        vn.Related_Campaign__c = camp.id;
        vn.Related_Virtual_Number__c = v.id;
        insert vn;
        
        req.params.put('cnum', 'db63441e3d2a71043ef5123ea5c40f6d');
        DAMAC_AMEYO_REST_SERVICES.doget();
        
       
    }
    
}