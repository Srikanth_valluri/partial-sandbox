public class createEHOCallinglist{

    //@InvocableMethod 
    public void invokeApex(Set<Id> setCaseId) {
        //Set<Id> setCaseId = new Set<Id>();
        map<Id, Calling_List__c> mapCaseIdCallingList = new map<Id, Calling_List__c>();
        list<Calling_List__c> lstCallingLists = new list<Calling_List__c>();
        Case taskCase;
        String arabQueueID, nonArabQueueId;
        String EHOCLId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Early Handover Calling List').getRecordTypeId();
         /*for (Task objTask : lstTask){
            setCaseId.add(objTask.WhatId);            
         }*/
         
         if (setCaseId != null && setCaseId.size() > 0) {
             for (Calling_List__c objCall : [Select Id, RecordTypeId, Case__c, Calling_List_Status__c 
                                             From Calling_List__c 
                                             Where Calling_List_Status__c != 'Closed' and RecordTypeId =: EHOCLId 
                                             and Case__c IN: setCaseId ]) {
                 mapCaseIdCallingList.put(objCall.Case__c, objCall);
             }
         }
         List<Group> lstQueues = [select Id,Name
                                from Group
                                where Type = 'Queue'
                                AND (name=: Label.Non_Elite_Non_Arabs_Queue
                                OR Name =: label.Non_Elite_Arabs_Queue)
                                LIMIT 2];

        for(Group grp : lstQueues) {
            if(grp.Name == Label.Non_Elite_Non_Arabs_Queue) {
                arabQueueID = grp.Id;
            }
            else if(grp.Name == Label.Non_Elite_Arabs_Queue) {
                nonArabQueueId = grp.Id;
            }
        }
         for (Case objCase : [Select Id, Booking_Unit__c, Booking_Unit__r.Booking__r.Account__r.Primary_CRE__c, OwnerId,
                              Booking_Unit__r.Booking__r.Account__r.Secondary_CRE__c, Booking_Unit__r.Booking__r.Account__r.Tertiary_CRE__c,
                              Booking_Unit__r.Booking__r.Account__r.Primary_Language__c, Booking_Unit__r.Registration_ID__c,
                              CaseNumber, Unit_Name__c, Monthly_Rent__c, Lease_Commencement_Date__c, Lease_End_Date__c, Completed_Milestones_Percent__c,
                              Account.Passport_Number__c, Account.Passport_Expiry_Date__pc, Parking_Details_JSON__c, Handover_Milestone_Date__c
                              From Case
                              Where Id IN: setCaseId ]) {
              taskCase = objCase;           
             if (mapCaseIdCallingList == null || !mapCaseIdCallingList.containsKey(objCase.id)) {
                 
                    String ownerId = String.isNotBlank(objCase.Booking_Unit__r.Booking__r.Account__r.Primary_CRE__c) ? objCase.Booking_Unit__r.Booking__r.Account__r.Primary_CRE__c :
                    (String.isNotBlank(objCase.Booking_Unit__r.Booking__r.Account__r.Secondary_CRE__c) ? objCase.Booking_Unit__r.Booking__r.Account__r.Secondary_CRE__c :
                    (String.isNotBlank(objCase.Booking_Unit__r.Booking__r.Account__r.Tertiary_CRE__c) ? objCase.Booking_Unit__r.Booking__r.Account__r.Tertiary_CRE__c :
                    (objCase.Booking_Unit__r.Booking__r.Account__r.Primary_Language__c == 'Arabic' ? arabQueueID : nonArabQueueId)));
                    lstCallingLists.add(new Calling_List__c(RecordTypeId = EHOCLId, Registration_ID__c = objCase.Booking_Unit__r.Registration_ID__c,
                        OwnerId=ownerId, Customer_Flag__c=true, Case__c = objCase.Id )); 
             }
         }
         
         if (lstCallingLists != null && lstCallingLists.size() > 0) {
            // insert lstCallingLists;
         }
         
         
         String cdcEmails = Label.CDC_EMails;
         list<String> lstRecipients = cdcEmails.split(',');
         Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
         String mailMessage = '';
         OrgWideEmailAddress[] owea;
         EmailTemplate templateId; 
         try {
             templateId = [select id, Body, Subject, HtmlValue, name from EmailTemplate where developername = 'Email_to_CDC_for_EHO_Documents'];
             owea = [select Id from OrgWideEmailAddress where Address = 'no-replysf@damacgroup.com'];
         }
         catch (Exception e) {
         }
         if (templateId != null) {
             mailMessage = templateId.HtmlValue;
             mailMessage = mailMessage.replace('{!Case.CaseNumber}', taskCase.CaseNumber);
             if (taskCase.Monthly_Rent__c != null) {
                 mailMessage = mailMessage.replace('{!Case.Monthly_Rent__c}', String.valueOf(taskCase.Monthly_Rent__c));
             }
             if (taskCase.Lease_Commencement_Date__c != null) {
                 mailMessage = mailMessage.replace('{!Case.Lease_Commencement_Date__c}', String.valueOf(taskCase.Lease_Commencement_Date__c));
             }
             if (taskCase.Lease_End_Date__c != null) {
                 mailMessage = mailMessage.replace('{!Case.Lease_End_Date__c}', String.valueOf(taskCase.Lease_End_Date__c));
             }
             if (taskCase.Handover_Milestone_Date__c != null) {
                 mailMessage = mailMessage.replace('{!Case.Completed_Milestones_Percent__c}', taskCase.Handover_Milestone_Date__c);
             }
             if (taskCase.Parking_Details_JSON__c != null) {
                 mailMessage = mailMessage.replace('{!Case.Parking_Details_JSON__c}', taskCase.Parking_Details_JSON__c);
             }
             if (taskCase.Account.Passport_Number__c != null) {
                 mailMessage = mailMessage.replace('{!Account.Passport_Number__c}', taskCase.Account.Passport_Number__c);
             }
             
             email.subject = templateId.Subject.replace('{!Case.CaseNumber}', taskCase.CaseNumber);
          }
         system.debug('!!!!!!!!!!templateId'+templateId);
         email.setHtmlBody(mailMessage);
         email.toAddresses = lstRecipients;
         if ( owea.size() > 0 ) {
                email.setOrgWideEmailAddressId(owea.get(0).Id);
            }
         try {
             Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
             return;
        }
        catch (EmailException e) {
            system.debug('!!!!!!!e'+e);
        }
    }
 
}