/**************************************************************************************************
* Name               : AgentPortalMyCOMMISSIONSTest
* Description        :                                               
* Created Date       : Naresh(Accely)                                                                       
* Created By         : 6/09/2017                                                                 
* Last Modified Date :                                                                            
* Last Modified By   :                                                                             
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR                    DATE                                                                    
* 1.1         Naresh kaneriya      6/09/2017                                                             
**************************************************************************************************/
@isTest(SeeAllData=false)  
public class AgentPortalMyCOMMISSIONSTest{
    private static Contact adminContact;
    private static User portalUser;
    private static Account adminAccount;

    static void init(){
       adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
        insert adminAccount;
        
        system.debug('--adminAccount--'+adminAccount.Agency_Tier__c);
        
        adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
        insert adminContact;
        
        Contact agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
        insert agentContact;
        
        portalUser = InitialiseTestData.getPortalUser('test@test.com', adminContact.Id, 'Admin');
    }

public static testMethod void  PortalMyCOMMISSIONSTest(){
         init();
        System.runAs(portalUser){
            Agent_Commission__c agentCommission = InitialiseTestData.createAgentCommission(adminAccount.Id,System.now().Date(),System.now().Date());
            insert agentCommission;

           AgentPortalMyCOMMISSIONS obj = new AgentPortalMyCOMMISSIONS();
           obj.getAgentCommission();
         }   
     
        // Inner Class
        AgentPortalMyCOMMISSIONS.Commissions  objCommissions = new AgentPortalMyCOMMISSIONS.Commissions();
        objCommissions.CustomerName = 'CustomerName ' ;
        objCommissions.UnitNo= 'UnitNo' ;
        objCommissions.Amount= 20.36 ;
        objCommissions.DateValue= Date.today();
  }

}