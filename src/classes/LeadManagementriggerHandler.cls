/***************************************************************************************************
* Description - The apex class is Handler for LeadManagementrigger
*
* Version   Date            Author            Description
* 1.0       22/03/18        Monali            Initial Draft
* 1.1       02/03/20        QBurst            Changed the Approval Status condition
*1.2        24/09/2020      Srikanth V        Update the Inquiry when update campaign check box is checked and status is approved
***************************************************************************************************/


public class LeadManagementriggerHandler {
    public LeadManagementriggerHandler() {

    }

    public void UpdateLeadManagement(List<Lead_Management__c> lstLeadMgmt){
        List<Id> leadManagementIdList = new List<Id>();
        List<Id> bookingUnitIdsList = new List<Id>();
        List<Id> bookingIdList = new List<Id>();
        List<Lead_Management__c> lstLeadToupdate = new List<Lead_Management__c>();
        LeadManagementriggerController objController = new LeadManagementriggerController();

        System.debug('=====LeadManagementriggerHandler====');
        
        List <Inquiry__c> inquiriesList = new List <Inquiry__c> ();
        for(Lead_Management__c lead :lstLeadMgmt) {
            //1.2
            if (lead.Update_campaign_on_inquiry__c && lead.Status__c == 'Approved') {
                Inquiry__c inq = new Inquiry__c ();
                inq.Id = lead.Old_Inquiry__c;
                inq.Campaign__c = lead.Campaign__c;
                inquiriesList.add (inq);
            }
            
            if(lead.Status__c == 'Approved') {
                System.debug('=====lead==Approved==' +lead);
                lstLeadToupdate.add(lead);
            }
            if (
            //lead.Approval_Status__c == 'Approved By MIS User for BP'  // 1.1
            lead.Approval_Status__c == 'Approved By Managing Director' // 1.1
                    && lead.Status__c == 'Approved') {
                leadManagementIdList.add(lead.Id);
                bookingUnitIdsList.add(lead.Booking_Unit__c);
            }
        }
        //1.2
        if (inquiriesList.size () > 0) {
            update inquiriesList;
        }
        if(lstLeadToupdate.size() > 0) {
            if (!Test.isRunningTest()) {
                objController.LeadManagementUpdate(lstLeadToupdate);
            }
            System.debug('=====call controller from Handler====');
        }

        // Craig 30-05-2018 - Update the Lead Data in IPMS
        if(!leadManagementIdList.isEmpty() && !bookingUnitIdsList.isEmpty()) {
            if (!Test.isRunningTest()) {
                UpdateBookingUnitToIPMS.updateBookingUnitsInIPMS(leadManagementIdList);
            }
        }

    }
}