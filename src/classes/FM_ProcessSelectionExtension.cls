/*
 * Description - Extension class for ProcessSelectionPage visualforce page
 *
 * Version        Date            Author            Description
 * 1.0          15/05/2018      Ashish Agarwal      Initial Draft
 * 2.0          26/12/2018      Lochana Rajput      Added code escape apostrophe is account name
 * 2.1          12/01/2021      Aishwarya Todkar    Queries DLP End Date
 */
public without sharing class FM_ProcessSelectionExtension  {

    public String strAccountName                         {get;set;}
    public String strSRType                                {get;set;}
    public String strSRTypeNew                                {get;set;}
    public Id selectedAccountId                         {get;set;}
    public String selectedCaseId                            {get;set;}
    public Boolean isAccountSelected                    {get;set;}
    public Account account                              {get;set;}
    public Boolean isOrgAccount                      {get;set;}
    public Boolean isFieldDisabled                      {get; set;}
    public List<POAUtility.POA> lstPOA                          {get; set;}
    public String strSelectedUnit {get; set;}
    public map<Id,Booking_Unit__c> mapUnitsOwned {get; set;}
    public map<Id,Booking_Unit__c> mapMyUnits {get; set;}
    public list<SelectOption> lstUnits {get; set;}
    public list<SelectOption> listCRESRs                {get;set;}
    public List<Booking_Unit__c> objUnitList            {get; set;}
    private map<String, FM_Process__mdt> mapProcesses ;
    public List<Id> objUnitOwner                        {get; set;}
    public List<Id> objUnitRes                          {get; set;}
      // Instance fields
    public String searchTerm {get; set;}
    public String selectedAccount {get; set;}
    private String loggedInUserProfileName;

    public Id selectedCRE                               {get;set;}

    public FM_ProcessSelectionExtension sharedInstance {
        get{
            System.debug('-->> ');
            return this;
        }
        set;
    }

    public FM_ProcessSelectionExtension(ApexPages.StandardController cases) {
        User objuser = [SELECT Profile.Name from User WHERE id=: UserInfo.getUserId()];
        loggedInUserProfileName = objuser.Profile.Name;
        init();
    }

    public void init() {

        strSRType = ApexPages.currentPage().getParameters().get('SRType');
        String currentPageURL = ApexPages.currentPage().getURL();

        system.debug('strSRType == '+strSRType);
        system.debug('selectedAccountIdbef'+selectedAccountId);

        selectedAccountId = ApexPages.currentPage().getParameters().get('AccountId');
        system.debug('selectedAccountId'+selectedAccountId);

        selectedCaseId = ApexPages.currentPage().getParameters().get('Id');
        system.debug('selectedCaseId'+selectedCaseId);

        strSelectedUnit = ApexPages.currentPage().getParameters().get('UnitId');

        if(strSelectedUnit==NULL){
          strSelectedUnit='';
        }

        if(String.isNotBlank(selectedCaseId) && selectedCaseId.startsWith('001')) {
            selectedAccountId = selectedCaseId;
            selectedCaseId = null;
        }

        system.debug('selectedAccountId*****'+selectedAccountId);
        if(selectedAccountId == NULL && strSelectedUnit == ''
        && strSRType == NULL && selectedCaseId != NULL){
            List<FM_Case__c> objFMCase = new List<FM_Case__c>();
            objFMCase = [SELECT Account__c
                              , RecordType.DeveloperName
                              , Booking_Unit__c
                              , Account__r.IsPersonAccount
                              , Tenant__r.IsPersonAccount
                              , Booking_Unit__r.Owner__c
                              , Booking_Unit__r.Tenant__c
                              , Request_Type__c
                              , Request_Type_DeveloperName__c
                              , Tenant__c
                              , Fund_Transfer_From_Unit__c
                           FROM FM_Case__c
                          WHERE Id = :selectedCaseId LIMIT 1];
            if(objFMCase.size() > 0) {
                selectedAccountId  = objFMCase[0].Account__c;
                strSelectedUnit = objFMCase[0].Booking_Unit__c;
                System.debug('======objFMCase[0].RecordType.DeveloperName========' + objFMCase[0].RecordType.DeveloperName);
                System.debug('======objFMCase[0].Fund_Transfer_From_Unit__c========' + objFMCase[0].Fund_Transfer_From_Unit__c);
                if(objFMCase[0].RecordType.DeveloperName == 'Fund_Transfer') {
                    strSelectedUnit = objFMCase[0].Fund_Transfer_From_Unit__c;
                }
                if(/*objFMCase[0].RecordType.DeveloperName == 'Tenant_Registration' && */currentPageURL.contains('MoveInRequestPage')
                || currentPageURL.contains('MoveOutRequestPage') ) {
                    strSRType = objFMCase[0].Booking_Unit__r.Owner__c == selectedAccountId ? 'Move_in_Request' : 'Move_in_Request_T';
                    selectedAccountId  = objFMCase[0].Tenant__c;
                }
                else {
                    strSRType =  objFMCase[0].Request_Type_DeveloperName__c ;
                }
            }
            //system.debug('objFMCase[0].RecordType.DeveloperName =='+objFMCase[0].RecordType.DeveloperName);
        }



        isOrgAccount = true;
        //account = new Account();
        lstPOA = new List<POAUtility.POA>();

        if ( String.isNotBlank(selectedAccountId) ) {
            selectAccount();
        } else {
            isAccountSelected = false;
        }

        if ( String.isNotBlank( selectedAccountId ) && String.isNotBlank( strSRType ) && account != null) {
            isFieldDisabled = true;
        } else {
            isFieldDisabled = false;
        }
        if( strSelectedUnit != null ) {
            system.debug('**************CALLING**********');
            fetchEligibleSRTypes();
        }
        //if( String.isNotBlank( strSelectedUnit ) ) {
         //   fetchEligibleSRTypes();
      //  }
    }

    public Pagereference redirectToProcessPage(){
        String strPageUrl ;
        if( String.isNotBlank( strSRType ) && mapProcesses != NULL && mapProcesses.containsKey( strSRType ) && !strSelectedUnit.equals('None') ){
            if(strSRType == 'Amenity_Booking' && String.isNotBlank( mapProcesses.get( 'Amenity_Booking' ).Process_Page_Name__c )){
                FmIpmsRestServices.DueInvoicesResult dues = FmIpmsRestServices.getDueInvoices(mapUnitsOwned.get(strSelectedUnit).Registration_Id__c, mapUnitsOwned.get(strSelectedUnit).Booking__r.Account__r.Party_ID__c, mapUnitsOwned.get(strSelectedUnit).Property_Name__c);
                System.debug('-->> Invoice dues.....'+ dues.dueAmountExceptCurrentQuarter);
                if(dues.dueAmountExceptCurrentQuarter == null || dues.dueAmountExceptCurrentQuarter < 3000 ){
                    strPageUrl = '/apex/' + mapProcesses.get( strSRType ).Process_Page_Name__c + '?AccountId=' + selectedAccountId + '&SRType=' + strSRType + '&UnitId=' + strSelectedUnit;
                } else {
                    System.debug('-->> Invoice Zero AYA.....');
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'NOTE: There is outstanding service charges on this unit. Please clear the dues to book the Amenity.'));
                    return null;
                }
            } else {
                if( String.isNotBlank( mapProcesses.get( strSRType ).Process_Page_Name__c ) ) {
                    strPageUrl = '/apex/' + mapProcesses.get( strSRType ).Process_Page_Name__c + '?AccountId=' + selectedAccountId + '&SRType=' + strSRType + '&UnitId=' + strSelectedUnit;
                }
            }
            
        }
        system.debug('--strPageUrl--'+strPageUrl);

        if( String.isNotBlank(strPageUrl) ) {
            Pagereference pageRef = new Pagereference(strPageUrl);
            pageRef.setRedirect(true);
            return pageRef;
        }
        return null;
    }

    public Pagereference redirectToPOPProcessPage(){
        String strPageUrl ;
        selectAccount();
        fetchPOPDetails();
        //system.debug('--mapProcesses.containsKey( Label.FMPopDevelopername )--'+mapProcesses.containsKey( Label.FMPopDevelopername ));
        system.debug('--mapProcesses--'+mapProcesses);
        if(  mapProcesses != NULL && mapProcesses.containsKey( Label.FMPopDevelopername ) ) {
            if( String.isNotBlank( mapProcesses.get( Label.FMPopDevelopername ).Process_Page_Name__c ) ) {
                strPageUrl = '/apex/' + mapProcesses.get( Label.FMPopDevelopername ).Process_Page_Name__c + '?AccountId=' + selectedAccountId + '&SRType=' + Label.FMPopDevelopername + '&UnitId=' + strSelectedUnit;
            }
        }
        if( String.isNotBlank(strPageUrl) ) {
            Pagereference pageRef = new Pagereference(strPageUrl);
            pageRef.setRedirect(true);
            return pageRef;
        }
        return null;
    }
    //Removed button from UI, added fund transfer in dropdown for FM Collections user
    /*public Pagereference redirectToFundTransferProcessPage(){
        String strPageUrl ;
        selectAccount();
        mapProcesses = new map<String, FM_Process__mdt>();

                for( FM_Process__mdt objData : [ SELECT Masterlabel
                                                      , DeveloperName
                                                      , Process_Page_Name__c
                                                   FROM FM_Process__mdt
                                                  WHERE FM_Portal_Actor__r.MasterLabel = 'Owner'
                                                    AND Active_for_CRE__c = true ] ) {
                    mapProcesses.put( objData.DeveloperName, objData );
                }
        //fetchPOPDetails();
        //system.debug('--mapProcesses.containsKey( Label.FMPopDevelopername )--'+mapProcesses.containsKey( Label.FMPopDevelopername ));
        system.debug('--mapProcesses--'+mapProcesses);
        // if(  mapProcesses != NULL && mapProcesses.containsKey( 'Fund_Transfer_Request' ) ) {
        //     if( String.isNotBlank( mapProcesses.get( 'Fund_Transfer_Request' ).Process_Page_Name__c ) ) {

                strPageUrl = '/apex/' + mapProcesses.get('Fund_Transfer_Request').Process_Page_Name__c + '?AccountId=' + selectedAccountId + '&SRType=Fund_Transfer_Request';//+ '&UnitId=' + strSelectedUnit;
        //     }
        // }
        if( String.isNotBlank(strPageUrl) ) {
            Pagereference pageRef = new Pagereference(strPageUrl);
            pageRef.setRedirect(true);
            return pageRef;
        }
        return null;
    }*/

    public void selectAccount(){
        isAccountSelected = true;
        account = [ SELECT Id
                   , Title__c
                   , Name
                   , Corporate_Customer_Mobile__c
                   , Customer_Masked_Mobile__c
                   , Customer_Email__c
                   , Party_ID__c
                   , Nationality__c
                   , Passport_Number__c
                   , IsPersonAccount
                   , PersonMobilePhone
                   , Salutation
                   , Phone
                   , PersonEmail
                   , Nationality__pc
                   , SLA__c
                   , Organisation_Name__c
                   , CR_Number__c
                   , Mobile__c
                   , Email__c
                   , Email__pc
                   , PersonMailingStreet
                   , PersonMailingPostalCode
                   , PersonMailingCity
                   , PersonMailingState
                   , PersonMailingCountry
                   , Address_Line_1__c
                   , Address_Line_2__c
                   , Address_Line_3__c
                   , Address_Line_4__c
                   , Party_Type__c,
                   Mobile_Phone_Encrypt__pc,
                   Address_Line_1__pc,
                   Address_Line_2__pc,
                   Address_Line_3__pc,
                   Address_Line_4__pc,
                   City__c,
                   Country__c,
                   City__pc,
                   Country__pc
                   ,Passport_Number__pc
                   //Joint Buyer info pending
                   FROM Account
                   WHERE Id = :selectedAccountId];
        strAccountName = account.Name;
        if(strAccountName != NULL && strAccountName.contains('\'')) {
            strAccountName= strAccountName.replace('\'', '\\\'');
        }
        List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        String guestUserProfileName='';
        if(!PROFILE.isEmpty()){
            guestUserProfileName= PROFILE[0].Name;
        }
        if(guestUserProfileName=='Facilities Management Site Profile'){
            account.id=null;
        }
        lstPOA = POAUtility.getPOAListForAccount(selectedAccountId);
        if(account.Party_Type__c == 'PERSON') {
            isOrgAccount = false;
        }
        system.debug('isAccountSelected '+isAccountSelected);
        fetchUnits();
    }

    public void fetchUnits(){
        List<String> lstHOStatusValues = new List<String>{NULL, ''};
        lstHOStatusValues.addAll(Label.FM_Booking_Unit_Handover_Status.split(','));
        Set<String> activeStatusSet= Booking_Unit_Active_Status__c.getall().keyset();
        mapUnitsOwned = new map<Id,Booking_Unit__c>();
        objUnitOwner = new List<Id>();
        objUnitRes = new List<Id>();
        mapMyUnits = new map<Id,Booking_Unit__c>();
        lstUnits = new list<SelectOption>();
        lstUnits.add(new selectOption('None','--None--'));
        objUnitList = [SELECT Id
                            , Owner__c
                            , Booking__r.Account__c
                            , Booking__r.Account__r.Name
                            , Booking__r.Account__r.Party_ID__c
                            , Owner__r.IsPersonAccount
                            , Tenant__r.IsPersonAccount
                            , Owner__r.Name
                            , Tenant__c
                            , Tenant__r.Name
                            , Resident__c
                            , Resident__r.Name
                            , Unit_Name__c
                            , Registration_Id__c
                            , Property_Name__c
                            , DLP_End_Date__c
                         FROM Booking_Unit__c
                      WHERE (Resident__c = :selectedAccountId
                                OR Booking__r.Account__c = :selectedAccountId
                                OR Tenant__c = :selectedAccountId 
                            )
                            AND( 
                                (Handover_Status__c NOT IN : lstHOStatusValues
                                    AND Registration_Status__c IN :activeStatusSet
                                ) 
                                OR Dummy_Booking_Unit__c = true
                            )
                    ];
        for( Booking_Unit__c objBU : objUnitList ){
            if( objBU.Booking__r.Account__c == selectedAccountId ){
                objUnitOwner.add(objBU.Id);
                mapUnitsOwned.put(objBU.Id,objBU);
            }
            else if( objBU.Tenant__c == selectedAccountId || objBU.Resident__c == selectedAccountId ){
              system.debug('==Inside tenant==');
                 objUnitRes.add(objBU.Id);
                 mapMyUnits.put(objBU.Id,objBU);
            }
            lstUnits.add(new selectOption(objBU.Id,objBU.Unit_Name__c));
        }
    }
    public void fetchPOPDetails(){
        Booking_Unit__c objUnit ;
        if( mapUnitsOwned != NULL  ) {
            objUnit = mapUnitsOwned.get( objUnitOwner[0] );
        }
        else if( mapMyUnits != NULL  ) {
            objUnit = mapMyUnits.get( objUnitRes[0] );
        }
        if( objUnit != NULL && account != NULL) {
            String strCustomerType ;
            if( String.isNotBlank( objUnit.Booking__r.Account__r.Name ) && objUnit.Booking__r.Account__r.Name.equalsIgnoreCase( account.Name ) ) {
                strCustomerType = 'Owner';
            }
            else if( String.isNotBlank( objUnit.Tenant__r.Name ) && objUnit.Tenant__r.Name.equalsIgnoreCase( account.Name ) ) {
                strCustomerType = 'Tenant';
            }
            system.debug('== strCustomerType =='+strCustomerType);
            system.debug('== account.Name =='+account.Name);
            if( String.isNotBlank( strCustomerType ) ){
                mapProcesses = new map<String, FM_Process__mdt>();

                for( FM_Process__mdt objData : [ SELECT Masterlabel
                                                      , DeveloperName
                                                      , Process_Page_Name__c
                                                   FROM FM_Process__mdt
                                                  WHERE FM_Portal_Actor__r.MasterLabel = :strCustomerType
                                                    AND Active_for_CRE__c = true ] ) {
                    mapProcesses.put( objData.DeveloperName, objData );
                }
            }
        }
    }
    public void fetchEligibleSRTypes() {
        Booking_Unit__c objUnit ;
        system.debug('== strSelectedUnit =='+strSelectedUnit);
        if(String.isNotBlank(strSelectedUnit) && !strSelectedUnit.equals('None')){
            system.debug('KYU AYA*******');
            if( mapUnitsOwned != NULL && mapUnitsOwned.containsKey( strSelectedUnit ) ) {
                system.debug('KYU IF ME AYA*******');
                objUnit = mapUnitsOwned.get( strSelectedUnit );
            }
            else if( mapMyUnits != NULL && mapMyUnits.containsKey( strSelectedUnit ) ) {
                system.debug('KYU ELSE ME AYA*******');
                objUnit = mapMyUnits.get( strSelectedUnit );
            }

        system.debug('== objUnit =='+objUnit);
        if( objUnit != NULL && account != NULL) {
            String strCustomerType ;
            if( String.isNotBlank( objUnit.Booking__r.Account__r.Name ) && objUnit.Booking__r.Account__r.Name.equalsIgnoreCase( account.Name ) ) {
                strCustomerType = 'Owner';
                system.debug('== 11111 =='+account.Name);
            }
            else if( String.isNotBlank( objUnit.Tenant__r.Name ) && objUnit.Tenant__r.Name.equalsIgnoreCase( account.Name ) ) {
                strCustomerType = 'Tenant';
                system.debug('== 222222 =='+account.Name);
            }
            if(String.isNotBlank(strSRType) && strSRType.contains('_T') && String.isBlank(strCustomerType)) {
                strCustomerType = 'Tenant';
                system.debug('== 3333333 =='+account.Name);
            }
            system.debug('== strCustomerType =='+strCustomerType);
            system.debug('== account.Name =='+account.Name);
            if( String.isNotBlank( strCustomerType ) ){
                listCRESRs = new list<SelectOption>();
                list<SelectOption> listSRsFMCollections = new list<SelectOption>();
                list<SelectOption> listSRsOther = new list<SelectOption>();
                listCRESRs.add(new selectOption('', '- None -'));

                mapProcesses = new map<String, FM_Process__mdt>();
                List<String> lstProfiles = new List<String>();
                lstProfiles = Label.FMCollectionsProfileName.split(',');
                List<String> lstSRS = new List<String>();
                lstSRS = Label.FMCollectionsProfileSRs.split(',');

                for( FM_Process__mdt objData : [ SELECT Masterlabel
                                                      , DeveloperName
                                                      , Process_Page_Name__c
                                                   FROM FM_Process__mdt
                                                  WHERE FM_Portal_Actor__r.MasterLabel = :strCustomerType
                                                    AND Active_for_CRE__c = true ] ) {
                                                    system.debug('DeveloperName'+objData.DeveloperName);
                                                    system.debug('Masterlabel'+objData.Masterlabel);
                    if(lstProfiles.contains(loggedInUserProfileName)
                        && lstSRS.contains(objData.DeveloperName)) {
                        listSRsFMCollections.add( new selectOption( objData.DeveloperName, objData.Masterlabel ) );
                    }
                    else if( !lstSRS.contains(objData.DeveloperName)){
                        listSRsOther.add( new selectOption( objData.DeveloperName, objData.Masterlabel ) );
                    }
                    mapProcesses.put( objData.DeveloperName, objData );
                }
                if(lstProfiles.contains(loggedInUserProfileName)) {
                    listCRESRs.addAll(listSRsFMCollections);
                }
                else {
                    listCRESRs.addAll(listSRsOther);
                }
                system.debug('listCRESRs'+listCRESRs);
                Integer listCRESRSSize = listCRESRs.Size();
                system.debug('listCRESRSSize '+listCRESRSSize );
                for(Integer i=0 ; i<listCRESRSSize ; i++){
                    system.debug('==listCRESRs[i] =='+listCRESRs[i].getValue());
                    if(listCRESRs[i].getValue() == Label.FMPopDevelopername){
                        listCRESRs.remove(i);
                        break;
                    }
                }
                listCRESRs.sort();
            }
        }
        }
    }
}