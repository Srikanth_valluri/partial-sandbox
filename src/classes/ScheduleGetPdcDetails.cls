global class ScheduleGetPdcDetails implements Schedulable{

    global void execute(SchedulableContext ctx) {
        GetPDCDetails.fetchPdcDetails();
    }
}