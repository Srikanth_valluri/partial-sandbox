// Close the appointment CL and create new callback request CL if cx didnt come on appointment time
public with sharing class NoShowCntrl{
    public String callingListId {get;set;}
    public List<Calling_List__c> callingList {get;set;}
    //private static final Id WALKIN_PROFILE_ID =  [select ProfileId  from User where Profile.Name = 'Walk In'].ProfileId;
    //private static final Id BISAN_USER_ID =  [select Id from User where Name = 'Bisan Saqallah' AND Email = 'bisan.saqallah@damacgroup.com' ].Id;
    //public static Id walkInRecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Walk In Calling List').RecordTypeId;
        
    public List<Calling_List__c> updateCallingList {get;set;}
    public NoShowCntrl(ApexPages.StandardController controller) {
    //  updateServiceDate();
    }
    public Pagereference createAndUpdateCallBackReqCL(){
     callingList = new List<Calling_List__c>();
     updateCallingList = new List<Calling_List__c>();
     callingListId = ApexPages.currentPage().getParameters().get('id');
     system.debug( ' callingListId : ' + callingListId);
//     If( UserInfo.getProfileId() != WALKIN_PROFILE_ID && UserInfo.getUserId ) {
     callingList = [SELECT Id,
                        RecordTypeId,Customer_Name__c,Service_Type__c,Service_Type1__c,
                        Closed_By_Reception__c,Sub_Purpose__c,Sub_Process1__c,
                        Closure_by_Reception_Date__c,Appointment_Date__c,
                        Assigned_CRE__c,Appointment_Slot__c,
                        Case__c,Calling_List_Status__c ,Remarks__c,
                        Service_start__c,Account_Email__c,Party_ID__c,
                        CreatedDate,Appointment_Start_DateTime__c,
                        Account__c,Appointment_End_DateTime__c,
                        Appointment_Status__c,isNoShow__c
                   FROM Calling_List__c
                  WHERE Id = :callingListId
                   // AND RecordTypeId = :walkInRecordTypeId
                     ];
        if( !callingList.isEmpty() && callingList.size() > 0  ) {
            if( callingList[0].isNoShow__c == true ) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'No show has already been reported for the selected appointment'));
                return null;
            }else if(callingList[0].Assigned_CRE__c == null ) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'You should populate Assigned CRE before no show'));
                return null;
            }else {
                List<Calling_List__c> lstcreateClone = createClone(callingList);
                if( !lstcreateClone.isEmpty() && lstcreateClone.size() > 0) {
                    sendEmail(callingList);
                    sendSMS(callingListId);
                    for(Calling_List__c callObj : callingList) {
                        callObj.isNoShow__c = true;
                        callObj.Appointment_Status__c = 'Completed';
                        callObj.Additional_Comments__c = 'System Closed as No Show';
                        updateCallingList.add(callObj);
                    }
                   update updateCallingList;
                   PageReference ReturnPage = new PageReference('/' +lstcreateClone[0].Id); 
                   ReturnPage.setRedirect(true); 
                   return ReturnPage;      
                }
            }
        }
        return null;
      // return new Pagereference(String.format(Label.Lightning_Calling_Detail_Page_Url, new List<String>{callingListId}));             
    }
    
    public static List<Calling_List__c>  createClone( List<Calling_List__c> toBeCloneCallingLst ) {
        System.debug('inside clone method');
        Id callBackCallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Call Back Request').RecordTypeId;
        List<Calling_List__c> clonedCallingLst = new list<Calling_List__c>() ;
        if( toBeCloneCallingLst != null && !toBeCloneCallingLst.isEmpty() ) {
            for(Calling_List__c toBecloneInst : toBeCloneCallingLst){
                Calling_List__c clonedObj = toBecloneInst.clone(false,false,false,false);
                clonedObj.Calling_List__c = toBecloneInst.Id;
                clonedObj.RecordTypeId = callBackCallingRecordTypeId;
                clonedObj.OwnerId = toBecloneInst.Assigned_CRE__c;
                clonedObj.Calling_List_Status__c = 'New';
                clonedCallingLst.add(clonedObj);
                
            }
        }
        insert clonedCallingLst;
        return clonedCallingLst;
    }
    
    public void sendEmail(List<Calling_List__c> lstAppointment) {
        List<Messaging.SingleEmailMessage> messages =   new List<Messaging.SingleEmailMessage>();
        EmailTemplate reqEmailTemplate =  [SELECT ID, Subject, Body, HtmlValue FROM EmailTemplate WHERE DeveloperName = 'No_Show_Notification_to_Customers' limit 1];
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'no-replysf@damacgroup.com'];
        for (Calling_List__c objCL : [Select Id, Name, Customer_Name__c, Service_Type__c, Appointment_Date__c, Appointment_Slot__c,
                                             Handover_Team_Email__c, HO_Calling_list_Owner__c, CRE_Email__c, Account_Email__c,
                                             Appointment_Status__c, Sub_Purpose__c, Booking_Unit_Name__c
                                      From Calling_List__c  Where Id IN: lstAppointment]) {
            Messaging.SingleEmailMessage mail = 
                            new Messaging.SingleEmailMessage();
            mail.setWhatId(objCL.Id);
            mail.setSaveAsActivity(true);
            list<String> bccAddress = new list<String>();
            List<String> sendTo = new List<String>();
            String body, sub;
            if (reqEmailTemplate != null) {
                body = reqEmailTemplate.HtmlValue;
                sub = reqEmailTemplate.Subject;
                if (objCL.Customer_Name__c!= null) {
                    body = body.replace('{!Calling_List__c.Customer_Name__c}', objCL.Customer_Name__c);
                } else {
                    body = body.replace('{!Calling_List__c.Customer_Name__c}', '');
                }
                
                if (objCL.Appointment_Slot__c != null) {
                    body = body.replace('{!Calling_List__c.Appointment_Slot__c}', objCL.Appointment_Slot__c);
                } else {
                    body = body.replace('{!Calling_List__c.Appointment_Slot__c}', '');
                }
                if (objCL.Appointment_Date__c != null) {
                    body = body.replace('{!Calling_List__c.Appointment_Date__c}', String.valueOf(objCL.Appointment_Date__c));
                } else {
                    body = body.replace('{!Calling_List__c.Appointment_Date__c}', null);
                }
                
                
                
                body = body.replace(']]>', '');
                if (objCL.Service_Type__c != null) {
                    sub = sub.replace('{!Calling_List__c.Service_Type__c}', objCL.Service_Type__c);
                } else {
                    sub = sub.replace('{!Calling_List__c.Service_Type__c}', '');
                }
                mail.setHtmlBody(body);
                mail.setSubject(sub);
            }
  
            if (objCL.CRE_Email__c != null) {
                bccAddress.add(objCL.CRE_Email__c);
            }
            if (bccAddress != null) {
                mail.setBccAddresses(bccAddress);
            }
            sendTo.add(objCL.Account_Email__c);
            mail.setToAddresses(sendTo);            
            messages.add(mail);
            
            if ( owea.size() > 0 ) {
                mail.setOrgWideEmailAddressId(owea.get(0).Id);
            }
        }
        
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
    }
    @future (callout=true)
    public static void sendSMS(String callingListId ){
        List<SMS_History__c>smsLst = new List<SMS_History__c>();
        for (Calling_List__c objCL : [Select Id, Name, Customer_Name__c, Service_Type__c, Appointment_Date__c, Appointment_Slot__c,
                                             Handover_Team_Email__c, HO_Calling_list_Owner__c, CRE_Email__c, Account_Email__c,
                                             Appointment_Status__c, Sub_Purpose__c, Booking_Unit_Name__c,Account__r.IsPersonAccount,
                                             Account__r.Mobile_Phone_Encrypt__pc , Account__r.Mobile__c,Account__c
                                      From Calling_List__c  Where Id =: callingListId]) {
            
            String strPhoneNumber = objCL.Account__r.IsPersonAccount ? objCL.Account__r.Mobile_Phone_Encrypt__pc : objCL.Account__r.Mobile__c;
            if(String.isNotBlank( strPhoneNumber ) ) {
                if(strPhoneNumber.startsWith('00')) {
                    strPhoneNumber = strPhoneNumber.removeStart('00');
                }
                else if(strPhoneNumber.startsWith('0')) {
                    strPhoneNumber = strPhoneNumber.removeStart('0');
                }
                system.debug('strPhoneNumber : '+strPhoneNumber);
                HttpRequest req = new HttpRequest();
                HttpResponse res = new HttpResponse();
                Http http = new Http(); 
                /*String user= 'salesforce';
                String passwd= 'D@$al3sF0rc387!';*/

                String user = Label.Damac_CRM_SMS_Service_User_name;//'damaccrm';
                String passwd = Label.Damac_CRM_SMS_Service_Password;//'D@$al3sF0rc387!';
                //String strSID = Label.Damac_CRM_SMS_Service_Sender_Id;//'DAMAC GRP'; 
                String strSID = SMSClass.getSenderName(user, strPhoneNumber, false);
                
                req.setMethod('POST' ); // Method Type
                req.setEndpoint('http://api.smscountry.com/SMSCwebservice_bulk.aspx'); 
                string msgCont = 'Dear Customer, We missed you at your appointment time of '+objCL.Appointment_Slot__c
                            +' ,'+objCL.Appointment_Date__c
                            +' .You are requested to book a new appointment at least 1 day in advance at '+System.Label.DAMAC_LIVING+'.Or at +97142375000. Thank you.';
                System.debug('msgCont:'+msgCont);
                String tempMsgCont = GenericUtility.encodeChar(msgCont);
                req.setBody('user='+ user + '&passwd=' + passwd +'&message=' + tempMsgCont + '&mobilenumber=' 
                + strPhoneNumber + '&sid=' + strSID + '&MTYPE=LNG&DR=Y'); // Request Parameters
                try {
                    system.debug('Request Body---'+req.getBody());
                    system.debug('Request Header---'+req.getHeader('Authorization'));
                    system.debug('Request Header---'+req.getHeader('Content-Type'));
                    res = http.send(req);
                    system.debug('Response---'+res.getBody());
                    if(res.getBody() != null){
                        // Parse Response
                        //if('SMS message(s) sent' == res.getBody()){
                        SMS_History__c smsObj = new SMS_History__c();
                        smsObj.Message__c = msgCont;
                        smsObj.Phone_Number__c = strPhoneNumber;
                        smsObj.Customer__c = objCL.Account__c;
                        smsObj.Calling_List__c = objCL.Id;
                        smsObj.Description__c = res.getBody();
                        if(String.valueOf(res.getBody()).contains('OK:')){
                            system.debug('Message Send Sucessfully');
                            smsObj.Is_SMS_Sent__c = true;
                            smsObj.sms_Id__c = res.getBody().substringAfter('OK:');
                            System.debug('smsObj::::if:'+smsObj);
                        }
                        smsLst.add(smsObj);
                    }
                }catch(Exception e) {
                    System.debug('Error***** '+ e);
                }
            }else{
                SMS_History__c smsObj = new SMS_History__c();
                smsObj.Message__c = '';
                smsObj.Phone_Number__c = '';
                smsObj.Customer__c = objCL.Account__c;
                smsObj.Calling_List__c = objCL.Id;
                smsObj.Description__c = 'While sending No show SMS, Customer Phone no. is missing.';
                System.debug('smsObj::::else:'+smsObj);
                smsLst.add(smsObj);
            }
        }
        if(!smsLst.isEmpty()){
            try{
                upsert smsLst;
            }catch(exception ex){
                System.debug('DML Exception*****: '+ ex);
            }
        }
    }
}