public without sharing class AP_AddBuyersDetailsController {

    public String srId                                                                  {get; set;}
    public Integer primaryBuyerNo                                                       {get; set;}
    public Integer jointBuyerNo                                                         {get; set;}
    public Decimal totalUnits                                                           {get; set;}
    public Decimal totalpriceAED                                                        {get; set;}
    public Buyer__c primaryBuyerObj                                                     {get; set;}
    public Buyer__c jointBuyerObj                                                       {get; set;}
    public List<NSIBPM__SR_Doc__c> primaryBuyerDocs                                     {get; set;}
    public List<NSIBPM__SR_Doc__c> jointBuyerDocs                                       {get; set;}
    public Booking__c bookingObj                                                        {get; set;}
    public List<Booking_unit__c> bookingUnitList                                        {get; set;}
    public string statusDisplayname                                                     {get; set;}
    public String srCurrentStatus                                                       {get; set;}
    public String strSelectedLanguage                                                   {get; set;}
    public AP_AddBuyersDetailsController() {
        totalpriceAED = 0;
        totalUnits = 0;
        primaryBuyerNo = 0;
        jointBuyerNo = 0;
        primaryBuyerObj = new Buyer__c();
        primaryBuyerDocs = new List<NSIBPM__SR_Doc__c>();
        jointBuyerObj = new Buyer__c();
        jointBuyerDocs = new List<NSIBPM__SR_Doc__c>();
        bookingUnitList = new List<Booking_unit__c>();
        srId = apexpages.currentPage().getParameters().get ('id');
        strSelectedLanguage = apexpages.currentPage().getParameters().get('langCode');
        system.debug('>>>>srId>>>>'+srId);
        // SR Id null check
        if (String.isNotBlank(srId)) {
            
            // Get all Booking related to the SR
            for (Booking__c bookLoopObj: [ SELECT Id, Deal_SR__c, Deal_SR__r.Name,
                                                 Deal_SR__r.NSIBPM__External_Status_Code__c
                                            FROM Booking__c
                                           WHERE Deal_SR__c = :srId
                                           LIMIT 1
            ]) {
                bookingObj = bookLoopObj;
            }
            NSIBPM__Service_Request__c objCurrentSR = new NSIBPM__Service_Request__c();
            objCurrentSR = [SELECT Id, NSIBPM__Due_Date__c, NSIBPM__Internal_Status_Name__c, 
                                   NSIBPM__External_Status_Name__c, NSIBPM__External_Status_Code__c,
                                   NSIBPM__Internal_SR_Status__r.Name, Name, 
                                   (SELECT Id FROM Bookings__r)
                              FROM NSIBPM__Service_Request__c
                             WHERE id =: srId];
            system.debug('objCurrentSR--'+objCurrentSR);
            system.debug('objCurrentSR--'+objCurrentSR.NSIBPM__External_Status_Code__c);
            srCurrentStatus = objCurrentSR.NSIBPM__External_Status_Code__c;
            //statusDisplayname = AP_BookingProgressController.SRStatusMapping1(objCurrentSR.NSIBPM__External_Status_Code__c);
            string statusName;
            statusName = AP_BookingProgressController.SRStatusMapping1(objCurrentSR.NSIBPM__External_Status_Code__c);
            if(String.isBlank(statusName)) {
                statusDisplayname = 'Awaiting Token Verification';
            } else {
                statusDisplayname = statusName;
            }
            system.debug('statusDispalyname--'+statusDisplayname);
        
            if (bookingObj != null) {
                
                // Get all Buyers related to the SR/Booking
                for(Buyer__c buyerLoopObj : [SELECT Id, Service_Request__c, 
                                                    First_Name__c,Last_Name__c, Primary_Buyer__c,Nationality__c
                                               FROM Buyer__c
                                              WHERE Booking__c = :bookingObj.Id
                                                AND Buyer_Type__c != '' 
                ]) { 
                    system.debug('>>>>buyerLoopObj>>>>'+buyerLoopObj);
                    if(buyerLoopObj.Primary_Buyer__c) {
                        primaryBuyerObj = buyerLoopObj;
                        system.debug('>>>>primaryBuyerObj>>>>'+primaryBuyerObj);
                        primaryBuyerNo += 1;
                    } else {
                        jointBuyerObj = buyerLoopObj;
                        jointBuyerNo += 1;
                        system.debug('>>>>jointBuyerObj>>>>'+jointBuyerObj);
                    }
                }

                // Get all Docs related to the Buyers
                for (NSIBPM__SR_Doc__c srDoc : [ SELECT Name, NSIBPM__Service_Request__r.Name,NSIBPM__Document_Name__c, 
                                                        NSIBPM__Document_Description__c,
                                                        Preview_Download_Document__c,
                                                        NSIBPM__Document_Type__c, NSIBPM__Status__c,
                                                        NSIBPM__Doc_ID__c, NSIBPM__Is_Not_Required__c
                                                   FROM NSIBPM__SR_Doc__c 
                                                  WHERE NSIBPM__Service_Request__c = :srId 
                                                    AND NSIBPM__Status__c = 'Uploaded' 
                                                    AND ( Name LIKE :primaryBuyerObj.First_Name__c + '%'
                                                        OR Name LIKE :jointBuyerObj.First_Name__c + '%'
                                                        )
                ]) {
                    system.debug('>>>>srDoc.Name>>>>'+srDoc.Name);
                    String buyerName = srDoc.Name.split('-')[0];
                    system.debug('>>>>buyerName>>>>'+buyerName);
                    if (buyerName == primaryBuyerObj.First_Name__c) {
                        primaryBuyerDocs.add(srDoc);
                    } else {
                        jointBuyerDocs.add(srDoc);
                    }
                }
                system.debug('>>>>primaryBuyerDocs>>>>'+primaryBuyerDocs);
                system.debug('>>>>jointBuyerDocs>>>>'+jointBuyerDocs);

                // Get all Booking Units related to the SR/Booking
                bookingUnitList = [SELECT Name, Unit_Location__c, Registration_ID__c, Booking__c,
                                          Unit_Name__c, Requested_Price_AED__c,
                                          Inventory__r.Marketing_Name_Doc__r.Primary_Image__c,
                                          Inventory__r.Marketing_Name_Doc__r.Marketing_Plan__c,
                                          Inventory__r.Property__r.Property_Plan__c
                                     FROM Booking_unit__c 
                                    WHERE Booking__c = :bookingObj.Id
                ];
                system.debug('bookingUnitList >>> 1 ' + bookingUnitList);
                for (Booking_unit__c buLoopObj : bookingUnitList) {
                    totalUnits += 1;
                    system.debug('buLoopObj.Requested_Price_AED__c >>>  ' + buLoopObj.Requested_Price_AED__c);
                    totalpriceAED += buLoopObj.Requested_Price_AED__c;
                }
                system.debug('totalpriceAED >>>  ' + totalpriceAED);
                system.debug('bookingUnitList >>>  ' + bookingUnitList);
                system.debug('primaryBuyerNo >>>  ' + primaryBuyerNo);
                system.debug('jointBuyerNo >>>  ' + jointBuyerNo);
            } else {
                 bookingObj = new Booking__c();
            }
        }
    }

}