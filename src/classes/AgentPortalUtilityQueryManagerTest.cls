/**************************************************************************************************
* Name               : AgentPortalUtilityQueryManagerTest                                                        *
* Description        : Test Class for AgentPortalUtilityQueryManagerTest                          *
* Created Date       : 01/09/2017                                                                 *
* Created By         : Naresh Kaneriya                                                                       *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR                    DATE                                                                    *
* 1.0        Naresh Kaneriya        01/09/2017                                                              *
**************************************************************************************************/
@isTest(SeeAllData = false) 
private class AgentPortalUtilityQueryManagerTest {

    static testMethod void myUnitTest() {
      Profile p2 = [SELECT Id FROM Profile WHERE Name='System Administrator'];
      ID adminRoleId = [ Select id from userRole where name = 'Chairman'].id;
      User u2 = new User(Alias = 'standt2', Email='stand2@testorg.com', userRoleId=adminRoleId, isActive = true,
                    EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p2.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='adm@testorg123.com');
    insert u2;
    User u1 = new User(Alias = 'standt1', Email='stan1@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', isActive = true,
            LocaleSidKey='en_US', ProfileId = p2.Id, ManagerID = u2.id,
            TimeZoneSidKey='America/Los_Angeles', UserName='sta0@damactest1234.com');
        insert u1;
        System.runAs(u2) {
          Campaign__c thisCampaign = InitialiseTestData.createCampaign();
          insert thisCampaign;
          Campaign__c thisCampaign1 = InitialiseTestData.createCampaign();
          insert thisCampaign1;
          AgentPortalUtilityQueryManager.getUsersCampaignDetails(new set<ID>{thisCampaign.id});
          
          AgentPortalUtilityQueryManager.getObjectRecord('Campaign__c',new set<ID>{thisCampaign.id,thisCampaign1.id});
          AgentPortalUtilityQueryManager.getSharingRecord('Campaign__c',new set<String>{thisCampaign.id,thisCampaign1.id});
          AgentPortalUtilityQueryManager.getUserChildDetails(new Set<ID>{u2.id});
          AgentPortalUtilityQueryManager.getUserManagerDetails(new Set<Id> {u1.id});
          AgentPortalUtilityQueryManager.getUserShareMap(new Set<Id> {u1.id});
          Notification__c notifycation = InitialiseTestData.createNotification(null,null);
          //insert notifycation;
          AgentPortalUtilityQueryManager.getNotifications('Active__c = true');
          AgentPortalUtilityQueryManager.getNotificationDescription(notifycation.id);
          AgentPortalUtilityQueryManager.markNotificationAsRead(notifycation.id);
          AgentPortalUtilityQueryManager.markNotificationAsRead(null);
          AgentPortalUtilityQueryManager.getContactInformation();
          AgentPortalUtilityQueryManager.getAccountId();
          Account acc = InitialiseTestData.getBlacklistedAccount('test');
          //acc.Agency_Tier__c = 'All';
          insert acc;
          AgentPortalUtilityQueryManager.getAccountInformation(acc.id);
          Announcement__c announcement = InitialiseTestData.createAnnouncement(Date.today(),Date.today().addDays(2),true,'All','All');
          //announcement.Agency__c = acc.id;
          insert announcement;
          AgentPortalUtilityQueryManager.getLatestAnnouncement(acc);
          AgentPortalUtilityQueryManager.getAllAnnouncements(acc);
          AgentPortalUtilityQueryManager.getLatestCampaign();
          //Assigned_Agent__c agent = InitialiseTestData.assignCampaignsToAgents(Date.today().addDays(2),Date.today(),u2.id);
          AgentPortalUtilityQueryManager.getLatestCampaign();
          Property__c pro = InitialiseTestData.insertProperties();
          InitialiseTestData.createInventoryUser(pro);
          AgentPortalUtilityQueryManager.getAllProjectTypes();
          AgentPortalUtilityQueryManager.getProjectLists(new Set<ID>{pro.id});
          AgentPortalUtilityQueryManager.getAllFields(Campaign__c.SObjectType.getDescribe());
          AgentPortalUtilityQueryManager.getInventoryIDs('Select Id,Inventory__c from Inventory_User__c');
          AgentPortalUtilityQueryManager.getPropertyIDs(new Set<Id> {pro.id});
          AgentPortalUtilityQueryManager.getAllCities();
          map<ID,inventory__c> mapin = new Map<ID,Inventory__c>([select id, name from Inventory__c]);
          AgentPortalUtilityQueryManager.getAddressLocation(new Set<Id> {pro.id},mapin.keySet());
          
          List<Group> lstGroups = InitialiseTestData.createGroupRecords(new List<Group> {new Group()});
          GroupMember gm = new GroupMember(UserOrGroupId = u1.id,GroupId = lstGroups[0].id);
          insert gm;
          AgentPortalUtilityQueryManager.getUserIdsFromGroups(new Set<Id> {lstGroups[0].id});
          Page_Flow__c  pf= InitialiseTestData.createPageFlow();
          insert pf;
          AgentPortalUtilityQueryManager.getProcessFlowId(pf.id);
          AgentPortalUtilityQueryManager.getAllContacts(acc.id);
          AgentPortalUtilityQueryManager.getCompanyProfileDetail(acc.id);
          AgentPortalUtilityQueryManager.getProfileName();
          Id InquiryRecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
          AgentPortalUtilityQueryManager.getCILs('',InquiryRecordTypeId,new Set<Id> {u2.id});
          
          Account A1 = new Account(Name = 'Test Account', Agency_Type__c = 'Corporate');
            insert A1;
        
            Contact C1 = new Contact( AccountID = A1.id, FirstName = 'Test', LastName = 'User1', 
            email = 'test-user@fakeemail.com' );
            insert C1; 
            
            Contact C2 = new Contact( AccountID = A1.id, FirstName = 'Test', LastName = 'User2', 
            email = 'test-user1@fakeemail.com' );
            insert C2;
            
            Contact C3 = new Contact( AccountID = A1.id, FirstName = 'Test', LastName = 'User3', 
            email = 'test-user2@fakeemail.com' );
            insert C3;
            ID ProfileID = [ Select id,UserType from Profile where name = 'Customer Community - Admin'].id;
            ID consultantId = [ Select id,UserType from Profile where name = 'Property Consultant'].id;
            User u4 = new User( email='test-user5@fakeemail.com', contactid = c1.id, profileid = profileID, 
                      UserName='test-user4@fakeemail.com', alias='tuser1', CommunityNickName='tuser5', isActive = true,
                      TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
                      LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User');
            insert u4;
    
            User u3 = new User( email='test-user3@fakeemail.com', profileid = consultantId, 
                      UserName='test-user3@fakeemail.com', alias='tuser3',  isActive = true,
                      TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
                      LanguageLocaleKey='en_US', FirstName = 'Test213', LastName = 'User213');
            insert u3;
          
          AgentPortalUtilityQueryManager.getAllUsers(A1.id);
          
          Attachment att = new Attachment(body=blob.valueOf('test'),Name='test',parentID = A1.id, ContentType='image/png');
          insert att;
          AgentPortalUtilityQueryManager.getTheAttachmentCount(A1.id);
          
          Case__c cas = InitialiseTestData.createCases();
          cas.Support_Category__c = 'Test';
          insert cas;
          AgentPortalUtilityQueryManager.getCases( 'Status__c = \'New\'',new Set<Id> ());
          
          AgentPortalUtilityQueryManager.getAllBedRooms();
          AgentPortalUtilityQueryManager.getInventoryList('Name From Inventory__c ',new Set<ID>());
          AgentPortalUtilityQueryManager.getAnnouncementRequest(announcement.id,acc.id);
          AgentPortalUtilityQueryManager.getPriceRange();
          AgentPortalUtilityQueryManager.getAnnouncementDetail(announcement.id);
          AgentPortalUtilityQueryManager.getAllUsersOfAccounts(new List<Id> {A1.id,acc.id});
          AgentPortalUtilityQueryManager.checkPendingSR(acc.id);
          AgentPortalUtilityQueryManager.getAllGeneralInventories('Select Id,Name from Inventory__c');
          AgentPortalUtilityQueryManager.getMinMaxPrice(pro.id);
          AgentPortalUtilityQueryManager.getCommission(A1.id);
    }
    }
}