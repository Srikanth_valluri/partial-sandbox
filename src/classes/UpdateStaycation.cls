/****************************************************************************************
Description: Update staycation when Stacae/RP is
              true on Inventory record.
----------------------------------------------------------------------------------------
Version     Date           Author               Description                                 
1.0       16/8/2020    Ruchika Choudhary       Initial Draft
*****************************************************************************************/

global with sharing class UpdateStaycation {

    
    /* Method Description : Method to call Update staycation service when Stacae/RP is
                            true on Inventory record.
     * Input Parameters : String- Unit Id on Inventory
                          Boolean - Staycae/RP value on Inventory record
     * Return Type : void
     */
    global static void sendUnitDetails(String unit, Boolean flag ){
        
        String attr1;
        if (flag == true) {
            attr1 = 'Y';
        }else {
             attr1 = 'N';
        }
        if (!String.isBlank(unit)) {
            sendHttpRequest(unit, attr1);
        }
        
     }
     
     
    /* Method Description : Invocable method called from Process Builder
                            (Call Update Staycation service)
     * Input Parameters : units - List of Inventory from Process Builder
     * Return Type : void
     */
    @InvocableMethod
    public static void sendUbitDetailsFromPB(List<Inventory__c> units){
        if (units[0].Unit_ID__c != null && units[0].UpdateStaycation__c != null){
            sendUnitDetails(units[0].Unit_ID__c , units[0].UpdateStaycation__c);
         }
    }
      
      
   /* Method Description : Method to send Htp Request to IPMS
    * Input Parameters : paramId - Unit Id on Inventory
    *                    attribute1 - Y or N value 
    * Return Type : void
    */
    @future (Callout = true)
    global static void sendHttpRequest(String paramId, String attribute1 ){
        Credentials_Details__c creds = getCredentials();
        String userName = creds.User_Name__c;
        String password = creds.Password__c; 
        String headerValue = 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(userName + ':' + password));
        try {
            
            Http http = new Http();
            HttpResponse response = new HttpResponse();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(creds.Endpoint__c);
            
            request.setMethod('POST');
        
            request.setHeader('Accept', 'application/json');
        
            request.setHeader('Content-Type', 'application/json');
        
            request.setHeader('Accept-Language', 'en-US');
        
            request.setHeader('Authorization', headerValue);
            request.setBody('{"PROCESS_Input":{ "RESTHeader":{ "Responsibility":"ONT_ICP_SUPER_USER", "RespApplication":"ONT", "SecurityGroup":"STANDARD", "NLSLanguage":"AMERICAN" }, "InputParameters":{ "P_REQUEST_NUMBER":"'+paramId+'", "P_SOURCE_SYSTEM":"SFDC", "P_REQUEST_NAME":"UPDATE_STAYCATION", "P_REQUEST_MESSAGE": { "P_REQUEST_MESSAGE_ITEM": [ {"PARAM_ID":"'+ paramId +'", "ATTRIBUTE1":"'+attribute1+'" } ] } } }}');
            response = http.send(request);
            System.debug('Req Body -----' + request);
            System.debug('Response -----' + response.getBody());
            System.debug('Response -----' + response.getStatusCode());
        } catch (Exception e) {
            System.debug('Exception --- ' + e);
        }
    }
    
    
    
   /* Method Description : Method to fetch IPMS creds and endpoint from
    *                      custom setting
    * Return Type : void
    */
    Public Static Credentials_Details__c getCredentials() {
     List<Credentials_Details__c> lstCreds = [ SELECT
                                                        Id
                                                        , Name
                                                        , User_Name__c
                                                        , Password__c
                                                        , Endpoint__c
                                                    FROM
                                                        Credentials_Details__c
                                                    WHERE
                                                        Name = :System.Label.Update_Staycation ];
        if( lstCreds != null && lstCreds.size() > 0 ) {
            return lstCreds[0];
        } else {
            return null;
        }
     }
    
}