@isTest
public class HDApp_FitOutSROperations_APITest {
    @isTest
    static void testFitOutDraftSR(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getfitOutSROps'; 
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        HDApp_FitOutSROperations_API.getFitOutDraftSR();
        Test.stopTest();
    }
    
    @isTest
    static void testFitOutDraftSRFmCaseBlank(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getfitOutSROps'; 
        req.httpMethod = 'POST';
        req.addParameter('fmCaseId', '');
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        HDApp_FitOutSROperations_API.getFitOutDraftSR();
        Test.stopTest();
    }
    
    @isTest
    static void testFitOutDraftSRFmCase(){
        
        Account objAccount = new Account();
        objAccount.Name = 'test';
        insert objAccount;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        objBooking.Account__c = objAccount.Id;
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Registration_ID__c = '3901';
        objBU.Booking__c = objBooking.Id;
        insert objBU;
        
        FM_Case__c objFMCase = new FM_Case__c();
        objFMCase.Type_of_Alteration__c = 'Lighting';
        objFMCase.Type_of_NOC__c = 'NOC for Fit-out';
        objFMCase.Description__c = 'Tesing NOC';
        objFMCase.Person_To_Collect__c = 'Contractor';
        objFMCase.Contact_person_contractor__c = 'Jyotika';
        objFMCase.Email_2__c = 'xyz@gmsil.com';
        objFMCase.Mobile_Country_Code_2__c = 'India: 0091';
        objFMCase.Mobile_no_contractor__c = '7895264785';
        insert objFMCase;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getfitOutSROps'; 
        req.httpMethod = 'POST';
        req.addParameter('fmCaseId', objFMCase.Id);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        HDApp_FitOutSROperations_API.getFitOutDraftSR();
        Test.stopTest();
    }
    
}