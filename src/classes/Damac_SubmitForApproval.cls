public class Damac_SubmitForApproval {

    
    @Future
    public static void submitApproval (List <ID> tourIds) {
        for(ProcessInstanceWorkitem workItem  : [Select p.Id, ProcessInstance.TargetObjectId  
                                from ProcessInstanceWorkitem p where p.ProcessInstance.TargetObjectId IN :tourIds]){
            for (integer i = 0; i < tourIds.Size (); i++) {
                if (workItem.ProcessInstance.TargetObjectId == tourIds[i]) {
                    tourIds.remove (i);
                }
            }
        }
    
        List <Approval.ProcessSubmitRequest> requests = new List <Approval.ProcessSubmitRequest> ();
        for (Sales_Tours__c tour : [SELECT Ownerid, Disqualify_Status__c FROM Sales_Tours__c 
                                    WHERE id IN: tourIds]) {
            if (tour.Disqualify_Status__c == 'HT Rejected') {
                
                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();  
                req.setSubmitterId (tour.OwnerId);        
                req.setComments('Submitting approval');        
                req.setObjectId(tour.Id);
                requests.add (req);
            }
        }
        try {
                Approval.process(requests); 
        }catch (Exception e) {
            System.debug (e.getMessage ());
        }
    }
}