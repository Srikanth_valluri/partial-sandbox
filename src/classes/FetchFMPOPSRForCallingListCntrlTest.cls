@isTest
public with sharing class FetchFMPOPSRForCallingListCntrlTest {
     @testSetup
    static void allTheDataForThisTestClass(){
       List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        Id caseRecordTypeId = Schema.SobjectType.FM_Case__c.RecordTypeInfosByName.get('Proof Of Payment').RecordTypeId;
        Id fmRecTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('FM Collections').RecordTypeId;
        Account accObj = new Account(Name = 'Miss. Madina Alieva');
        insert accObj;
        
        List<FM_Case__c>caseLst = new List<FM_Case__c>();
        caseLst.add( new FM_Case__c(Account__c = accObj.id,RecordTypeId = caseRecordTypeId,Status__c = 'New'));
        caseLst.add( new FM_Case__c(Account__c = accObj.id,RecordTypeId = caseRecordTypeId,Status__c = 'New'));
        caseLst.add( new FM_Case__c(Account__c = accObj.id,RecordTypeId = caseRecordTypeId,Status__c = 'New'));
        insert caseLst;
        
        Calling_List__c callObj = new Calling_List__c(RecordTypeId = fmRecTypeId, Account__c = accObj.Id,Registration_ID__c = '12851');
        insert callObj;
        
    }
    public static testMethod void testfetchPOPSRs(){
        Calling_List__c callObjInst = [SELECT Id,
                                              Account__c,
                                              Registration_ID__c
                                         FROM Calling_List__c
                                         LIMIT 1];
        System.debug('inside test class:callObjInst::'+callObjInst);
        System.assertNotEquals(null,callObjInst.Id);
        Test.startTest();
            PageReference popPage = Page.FetchFMPOPSRForCallingList;
            Test.setCurrentPage(popPage);
            popPage.getParameters().put('Id',String.valueOf(callObjInst.Id));
            ApexPages.StandardController sc = new ApexPages.StandardController(callObjInst);
            FetchFMPOPSRForCallingListCntrl controllerObj = new FetchFMPOPSRForCallingListCntrl(sc);
        Test.stopTest();
        //controllerObj.fetchCases();
    }
}