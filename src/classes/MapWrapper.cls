global Class MapWrapper{
     public Double lat;
     public Double lng;
     public String name;
     public List<String> formattedAddress;
      public String contact;
      public String status;
     public MapWrapper(Double lat,Double lng,String name,List<String> formattedAddress,String contact){
         this.lat= lat;
         this.lng= lng;
         this.name= name;
         this.formattedAddress=formattedAddress;
         this.contact=contact;
        // this.status=status;
     }
     public MapWrapper(Double lat,Double lng,String name,List<String> formattedAddress,String contact,String status){
         this.lat= lat;
         this.lng= lng;
         this.name= name;
         this.formattedAddress=formattedAddress;
         this.contact=contact;
         this.status=status;
     }

}