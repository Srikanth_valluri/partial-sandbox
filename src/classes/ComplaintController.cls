public virtual without sharing class ComplaintController {
  public List<Booking_Unit__c> bookingUnitList{get; set;}
  public Booking_Unit__c bookingUnitDetails{get; set;}
  public UnitDetailsService.BookinUnitDetailsWrapper bookingUnitDetailsFromIPMS{get; set;}
  public List<Account> accountsList{get; set;}
  public String selectedAcc{get; set;} //this will be used if accountId is selected from component dropdown
  public String strSRType{get; set;}
  public String selectedBookingUnitId{get; set;}
  public String strSelectedCategory {get;set;}
  public String complaintFormURL{get; set;}
  public String soaComplaintFormURL{get; set;}
  public List<SelectOption> lstCategories {get;set;}
  public list<Case> lstExistingCase {get;set;}
  public Case complaintCaseObj{get; set;}
  public String selectedUnit {get; set;}
  public Account accountObj;
  public transient String crfAttachmentBody { get; set; }
  public transient String crfAttachmentName { get; set; }
  public Boolean crfAttachmentUploaded = false;
  public transient String attachment1Body { get; set; }
  public transient String attachment1Name { get; set; }
  public Boolean attachment1Uploaded = false;
  public transient String attachment2Body { get; set; }
  public transient String attachment2Name { get; set; }
  public Boolean attachment2Uploaded = false;
  public String caseNumber = '';
  public boolean isError  { get; set; }
  public String errorMessage = '';
  public String ComplaintWaitMessagePanel {get;set;}
  public String ComplaintWaitMessageCSS {get; set;}
  public Boolean blnFlag;
  public ComplaintController() {
      System.debug('ComplaintController selectedAcc======= '+selectedAcc);
      System.debug('ComplaintController strSRType======= '+strSRType);

      //init();
  }

  protected ComplaintController(Boolean shouldCall) {}

  public String getName() {
      System.debug('--getNames AccountID ---'+selectedAcc);
      System.debug('--getNames strSRType ---'+strSRType);
      if(String.isNotBlank(selectedAcc)){
          iserror= true;
          ComplaintWaitMessagePanel = '';
          //init();
          if(String.isNotBlank(selectedAcc) && String.isNotBlank(strSRType) && strSRType == 'Complaint'){
              init();
          }
      }
      return selectedAcc;
  }

  public virtual void init(){
      System.debug('ComplaintController initselectedAcc======= '+selectedAcc);
      System.debug('ComplaintController init strSRType======= '+strSRType);
      complaintCaseObj = new Case(AccountId=selectedAcc
                                ,Type=ComplaintProcessUtility.COMPLAINT_CASE_TYPE
                                ,RecordTypeId=ComplaintProcessUtility.getRecordTypeId()
                                ,Status=ComplaintProcessUtility.COMPLAINT_INITIAL_STATUS
                                ,Origin=ComplaintProcessUtility.COMPLAINT_PORTAL_ORIGIN);

      accountObj = new Account();
      bookingUnitList = new List<Booking_Unit__c>();
      isError = true;
      selectedUnit = '';
      bookingUnitDetailsFromIPMS = new UnitDetailsService.BookinUnitDetailsWrapper();
      //accountsList = new List<Account>();
      //accountsList =  [SELECT Name, Id, IsPersonAccount FROM Account];

      lstCategories = new list<SelectOption>();
          lstCategories.add(new selectOption('Unit Details', 'Unit Details'));
          lstCategories.add(new selectOption('Flags', 'Flags'));
          lstCategories.add(new selectOption('Unit Status', 'Unit Status'));
          lstCategories.add(new selectOption('Open SRs', 'Open SRs'));
      strSelectedCategory = 'Unit Details';
  }



  public void getUpdatedUnitDetails(){
    String bookingUnitId = Apexpages.currentpage().getparameters().get('bookingUnitId');
        if(!String.isBlank(bookingUnitId) && bookingUnitId != 'none'){
            bookingUnitDetails = [SELECT Name,Id,Inventory__c,
                                    Property_Name__c,
                                    Inventory__r.Property_Status__c,
                                    Inventory__r.Property_City__c,
                                    Bedroom_Type__c,
                                    Unit_Type__c,
                                    Permitted_Use__c,
                                    Inventory__r.Building_Location__r.Status__c,
                                    Rental_Pool_Status__c,
                                    Area__c,
                                    Inventory__r.Master_Developer_EN__c,
                                    Inventory__r.Building_Location__c,
                                    Inventory_Area__c,
                                    Unit_Details__c,
                                    Unit_Selling_Price__c,
                                    Booking_Type__c,
                                    Plot_Price__c,
                                    DP_OK__c,
                                    Doc_OK__c,
                                    Booking__r.CreatedDate,
                                    Agreement_Date__c,
                                    Inventory__r.Unit_Plan__c,
                                    Inventory__r.Floor_Plan__c,
                                    JOPD_Area__c,
                                    Mortgage__c,
                                    PCC_Release__c,
                                    Selling_Price__c,
                                    Unit_s_Current_Status__c,
                                    OQOOD_Reg_Flag__c,
                                    Early_Handover__c,
                                    Handover_Complete__c,
                                    Unit_Selling_Price_AED__c,
                                    DP_Overdue__c,
                                  Registration_ID__c,
                                  Property_Name_Inventory__c
                                FROM Booking_Unit__c WHERE Id =: bookingUnitId];
        }
      System.debug(bookingUnitDetails);
      bookingUnitDetailsFromIPMS = UnitDetailsService.getBookingUnitDetails( bookingUnitDetails.Registration_ID__c );
      checkExistingSRExists( bookingUnitDetails.Registration_ID__c );
      //if( !bookingUnitDetailsFromIPMS.isEmpty() ){
      //  if(
      //    bookingUnitDetailsFromIPMS.strDispute == 'Y' ||
      //    bookingUnitDetailsFromIPMS.strEnforcement == 'Y' ||
      //    bookingUnitDetailsFromIPMS.strLitigation == 'Y' ||
      //    bookingUnitDetailsFromIPMS.strCounterCase == 'Y' ||
      //    ){
      //    isError = true;
      //    errorMessage= 'This SR cannot be proceeded as there is no customer associated with it. Please create a new COD SR.';
      //          ApexPages.addmessage(new ApexPages.message(
      //           ApexPages.severity.Error,errorMessage));
      //  }
      //}
      //System.assert(false, 'bookingUnitDetailsFromIPMS === '+bookingUnitDetailsFromIPMS);
  }

  public void checkExistingSRExists(String strSelectedUnit){
        set<String> setAllowedSRTypes = new set<String>();
        setAllowedSRTypes.add('Complaint');
        //setAllowedSRTypes.add('Name_Nationality_Change');
        //setAllowedSRTypes.add('Change_of_Joint_Buyer');

        lstExistingCase = new list<Case>();
        map<Id,Case> mapId_Case = new map<Id,Case>([Select c.Id
                                                         , c.Booking_Unit__c
                                                         , c.AccountId
                                                         , c.CaseNumber
                                                         , c.RecordType.DeveloperName
                                                         , c.RecordType.Name
                                                    From Case c
                                                    where c.Booking_Unit__c =: strSelectedUnit
                                                    and c.Status != 'Closed'
                                                    and c.Status != 'Rejected'
                                                    and c.Status != 'Cancelled'
                                                    and c.RecordType.Name IN : setAllowedSRTypes]);
        if(mapId_Case != null && !mapId_Case.isEmpty()){
            lstExistingCase.addAll(mapId_Case.values());
        }
        /*for(SR_Booking_Unit__c objSBU : [Select s.Id
                                              , s.Case__c
                                              , s.Case__r.Status
                                              , s.Case__r.CaseNumber
                                              , s.Case__r.RecordType.DeveloperName
                                              , s.Case__r.RecordType.Name
                                              , s.Booking_Unit__c
                                         From SR_Booking_Unit__c s
                                         where s.Booking_Unit__c =:strSelectedUnit
                                         and s.Case__r.Status != 'Closed'
                                         and s.Case__r.Status != 'Rejected'
                                         and s.Case__r.RecordType.DeveloperName = 'AOPT']){
            if(!mapId_Case.containsKey(objSBU.Case__c)){
                Case objCase = objSBU.Case__r;
                //objCase.Id = objSBU.Case__c;
                //objCase.CaseNumber = objSBU.Case__r.CaseNumber;
                lstExistingCase.add(objCase);
            }
        }*/


        //return lstExistingCase;
    }

  public void getCustomerFlagsFromIPMS(){

    accountObj = [SELECT Party_Id__c, IsPersonAccount, Email__pc, Email__c FROM Account WHERE Id =: selectedAcc];
    ComplaintWaitMessageCSS = '';
    ComplaintWaitMessagePanel= '';
    try{
      actionCom.COCDHttpSoap11Endpoint cocdHttpSoap11Endpoint = new actionCom.COCDHttpSoap11Endpoint();
      cocdHttpSoap11Endpoint.timeout_x = 120000;
      String response = COCDHttpSoap11Endpoint.COCDApprovalsRequired(accountObj.Party_Id__c,'','COD','PRIMARYCONTACT','','','','','','','','','','');
      //String response = '{"regId":"1050872","adminFee":null,"allowed":"No","message":"Testing message, user not allowed","signedRequestForm":null,"cocdForm":null,"selfAttestedPassportCopy":null,"selfattestedPPCopy":null,"selfAttestedEmiratesID":null,"selfAttestedGccID":null,"letterOfDecleration":null,"courtOrder":null,"companyAuthorisedSignatorySupportingDocuments":null,"tempOne":null,"tempTwo":null,"tempThree":null,"recommendingAuthorityOne":null,"recommendingAuthorityTwo":null,"recommendingAuthorityThree":null,"recommendingAuthorityFour":null,"approvingAuthorityOne":null,"approvingAuthorityTwo":null,"approvingAuthorityThree":null}';
      System.debug('----response----' + response);
        if (response != null){
            COCDFinalUpdate.RuleEngineResponse ruleEngineResponse = (COCDFinalUpdate.RuleEngineResponse)JSON.deserialize(response, COCDFinalUpdate.RuleEngineResponse.class);
            if(ruleEngineResponse.allowed =='No' || ruleEngineResponse.allowed == null || ruleEngineResponse.allowed == 'null'){
                errorMessage=ruleEngineResponse.message;
                isError=true;
                ApexPages.addmessage(new ApexPages.message(
                  ApexPages.severity.Error,errorMessage));
            }
            else{
                isError=false;
                getUnitListMethod();
            }
        }
        else{
            isError = true;
            errorMessage = ComplaintProcessUtility.COMPLAINT_EXCEPTION_MESSAGE;
            ApexPages.addmessage(new ApexPages.message(
              ApexPages.severity.Error,errorMessage));
        }
      } catch( Exception e){
        isError = true;
        errorMessage = ComplaintProcessUtility.COMPLAINT_EXCEPTION_MESSAGE;
        ApexPages.addmessage(new ApexPages.message(
          ApexPages.severity.Error,errorMessage));
      }
  }

  public void getUnitListMethod(){
    System.debug('---- in getUnitList ----' );
    System.debug('----selectedAcc in getUnitList ----' + selectedAcc);
      String caseId = Apexpages.currentpage().getparameters().get('CaseId');
      System.debug('----caseId in getUnitList ----' + caseId);
        if( String.isNotBlank( caseId )  ){
          complaintCaseObj = [SELECT AccountId, Booking_Unit__c,
                                Complaint_Type__c, Description, CaseNumber,
                                Complaint_Sub_Type__c
                              FROM Case WHERE Id =: caseId ];

          System.debug( 'complaintCaseObj.Booking_Unit__c ' + complaintCaseObj.Booking_Unit__c );

          selectedUnit = complaintCaseObj.Booking_Unit__c;
          if( String.isNotBlank( complaintCaseObj.Booking_Unit__c ) ){
            bookingUnitList = [ SELECT  Unit_Name__c
                                  , Id
                                  , Booking__c
                                FROM Booking_Unit__c
                                WHERE Id = : complaintCaseObj.Booking_Unit__c
                                AND Re_Assigned__c = false ];

          } else {
            getUnits();
          }

          System.debug( 'bookingUnitList in Proceed Case condition ' + bookingUnitList );
        } else if(!String.isBlank(selectedAcc)){

          //bookingUnitList = [SELECT Inventory__r.Unit_Location__r.Unit_Name__c,Unit_Details__c, Id, Booking__c FROM Booking_Unit__c WHERE Booking__r.Account__c =: selectedAcc AND Re_Assigned__c = false];
          //accountObj = [SELECT Party_Id__c FROM Account WHERE Id =: selectedAcc];
          //getCustomerFlagsFromIPMS( accountObj.Party_Id__c );

System.debug('----isError in getUnitList ----' + isError);

          if( !isError ){

            getUnits();
            system.debug('== complaintCaseObj =='+complaintCaseObj);
            complaintCaseObj.AccountId = selectedAcc;
            complaintCaseObj.Account_Email__c = accountObj.IsPersonAccount ? accountObj.Email__pc : accountObj.Email__c ;
            upsert complaintCaseObj;
            caseNumber = [select CaseNumber FROM Case Where Id =: complaintCaseObj.Id ].CaseNumber;
            System.debug('bookingUnitList ==== '+ bookingUnitList);
          }
          //} else {
          //  ApexPages.addmessage(new ApexPages.message(
          //       ApexPages.severity.Error,errorMessage));
          //}
        }
    }

    public void getUnits(){
        Set<Id> bookingIds = new Set<Id>();
        system.debug('== selectedAcc =='+selectedAcc);

        for( Buyer__c buyerObj : [ SELECT
                      Booking__c
                  FROM Buyer__c
                  WHERE Account__c =: selectedAcc ] ){
          bookingIds.add(buyerObj.Booking__c);
        }
    system.debug('== bookingIds =='+bookingIds);
      if( !bookingIds.isEmpty() ){
        bookingUnitList = [ SELECT  Unit_Name__c
                            , Id
                            , Booking__c
                          FROM Booking_Unit__c
                          WHERE Booking__c IN : bookingIds
                          AND Re_Assigned__c = false ];


      }
    }


    public void getComplaintCRF(){
      //write callout logic
      PenaltyWaiverService.CRFOuterWrap complaintFormWrapper = PenaltyWaiverService.getCRFDocument( bookingUnitDetails, ComplaintProcessUtility.PROCESS_NAME);
      if( 'E'.equalsIgnoreCase( complaintFormWrapper.status ) || 'E'.equalsIgnoreCase( complaintFormWrapper.data.PROC_STATUS ) ){
        complaintFormURL = ComplaintProcessUtility.COMPLAINT_NO_URL_MESSAGE;

        insert new Error_Log__c(Case__c=complaintCaseObj.Id,Error_Details__c=complaintFormWrapper.data.PROC_MESSAGE,Process_Name__c='Complaints',Booking_Unit__c=bookingUnitDetails.Id);

      } else {
        complaintFormURL = complaintFormWrapper.data.ATTRIBUTE1;
      }

      system.debug( 'complaintFormWrapper  ====== '+complaintFormWrapper );
      //complaintFormURL =
  }

  public void getComplaintSOAURL(){
    GenerateSOAController.soaResponse response = GenerateSOAController.getSOADocument( bookingUnitDetails.Registration_ID__c );
    if( 'Exception'.equalsIgnoreCase( response.status ) ){
      soaComplaintFormURL = ComplaintProcessUtility.COMPLAINT_NO_URL_MESSAGE;
      insert new Error_Log__c(Case__c=complaintCaseObj.Id,Error_Details__c=response.PROC_MESSAGE,Process_Name__c='Complaints',Booking_Unit__c=bookingUnitDetails.Id);
    } else {
      soaComplaintFormURL = response.url;
    }

  }

    public virtual PageReference complaintSubmitCase(){
      complaintCaseObj.Status=ComplaintProcessUtility.COMPLAINT_SUBMIT_STATUS;
      //Boolean blnFlag = true;//
      saveComplaintCase();
      if(!blnFlag) {
          return null;
      }

      PageReference pageRef = new PageReference('/'+complaintCaseObj.Id);
      pageRef.setRedirect(true);
      return pageRef;
    }

    public virtual PageReference saveComplaintCase() {
      System.debug('=======saveComplaintCase=======');
      blnFlag = true;
      Integer i=1;
      if( String.isBlank( complaintCaseObj.AccountId ) ) {  complaintCaseObj.AccountId = selectedAcc;  }
      if( String.isBlank( complaintCaseObj.Booking_Unit__c ) ) {
        complaintCaseObj.Booking_Unit__c = selectedBookingUnitId;
      }
      try {
        String crfFileName = '';
        String attachment1 = '';
        String attachment2 = '';
        List<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocReq = new List<UploadMultipleDocController.MultipleDocRequest>();
        if(String.isNotBlank( crfAttachmentBody ) && String.isNotBlank( crfAttachmentName ) && String.isBlank( complaintCaseObj.CRF_File_URL__c ) ){
          UploadMultipleDocController.MultipleDocRequest objMultipleDocRequest =
          new UploadMultipleDocController.MultipleDocRequest();
          objMultipleDocRequest.category = 'Document';
          objMultipleDocRequest.entityName = 'Damac Service Requests';
          objMultipleDocRequest.base64Binary = EncodingUtil.base64Encode(extractBody(crfAttachmentBody));
          objMultipleDocRequest.fileDescription = extractType(crfAttachmentName);
          /*objMultipleDocRequest.fileId = 'IPMS-'+bookingUnitDetails.Registration_ID__c+'-'+extractName(crfAttachmentName);
          objMultipleDocRequest.fileName = 'CRFfile';*/

          objMultipleDocRequest.fileId = caseNumber+'-'+String.valueOf(System.currentTimeMillis() + i)+'.'+extractType(crfAttachmentName);
          objMultipleDocRequest.fileName = caseNumber+'-'+String.valueOf(System.currentTimeMillis() + i)+'.'+extractType(crfAttachmentName);
          i++;

          //caseNumber+'-'+String.valueOf(System.currentTimeMillis() + i)+'.'+extractType(crfAttachmentName);

          objMultipleDocRequest.registrationId = caseNumber;
          objMultipleDocRequest.sourceId = 'IPMS-'+caseNumber+'-'+extractName(crfAttachmentName);
          objMultipleDocRequest.sourceFileName = 'IPMS-'+caseNumber+'-'+extractName(crfAttachmentName);

          crfFileName = objMultipleDocRequest.sourceFileName;
          lstMultipleDocReq.add(objMultipleDocRequest);
        }

        if(String.isNotBlank( attachment1Body ) && String.isNotBlank( attachment1Name ) && String.isBlank( complaintCaseObj.Additional_Doc_File_URL__c ) ){
          UploadMultipleDocController.MultipleDocRequest objMultipleDocRequest =
          new UploadMultipleDocController.MultipleDocRequest();
          objMultipleDocRequest.category = 'Document';
          objMultipleDocRequest.entityName = 'Damac Service Requests';
          objMultipleDocRequest.base64Binary = EncodingUtil.base64Encode(extractBody(attachment1Body));
          objMultipleDocRequest.fileDescription = extractType(attachment1Name);
          /*objMultipleDocRequest.fileId = 'IPMS-'+bookingUnitDetails.Registration_ID__c+'-'+extractName(attachment1Name);
          objMultipleDocRequest.fileName = 'Attachment1';*/

          objMultipleDocRequest.fileId = caseNumber+'-'+String.valueOf(System.currentTimeMillis() + i)+'.'+extractType(attachment1Name);
          objMultipleDocRequest.fileName = caseNumber+'-'+String.valueOf(System.currentTimeMillis() + i)+'.'+extractType(attachment1Name);
          i++;

          objMultipleDocRequest.registrationId = caseNumber;
          objMultipleDocRequest.sourceId = 'IPMS-'+caseNumber+'-'+extractName(attachment1Name);
          objMultipleDocRequest.sourceFileName = 'IPMS-'+caseNumber+'-'+extractName(attachment1Name);
          attachment1 = objMultipleDocRequest.sourceFileName;
          lstMultipleDocReq.add(objMultipleDocRequest);
        }

        if(String.isNotBlank( attachment2Body ) && String.isNotBlank( attachment2Name ) && String.isBlank( complaintCaseObj.OD_File_URL__c ) ){
          UploadMultipleDocController.MultipleDocRequest objMultipleDocRequest =
          new UploadMultipleDocController.MultipleDocRequest();
          objMultipleDocRequest.category = 'Document';
          objMultipleDocRequest.entityName = 'Damac Service Requests';
          objMultipleDocRequest.base64Binary = EncodingUtil.base64Encode(extractBody(attachment2Body));
          objMultipleDocRequest.fileDescription = extractType(attachment2Name);
          /*objMultipleDocRequest.fileId = 'IPMS-'+bookingUnitDetails.Registration_ID__c+'-'+extractName(attachment2Name);
          objMultipleDocRequest.fileName = 'Attachment2';*/

          objMultipleDocRequest.fileId = caseNumber+'-'+String.valueOf(System.currentTimeMillis() + i)+'.'+extractType(attachment2Name);
          objMultipleDocRequest.fileName = caseNumber+'-'+String.valueOf(System.currentTimeMillis() + i)+'.'+extractType(attachment2Name);
          i++;

          objMultipleDocRequest.registrationId = caseNumber;
          objMultipleDocRequest.sourceId = 'IPMS-'+caseNumber+'-'+extractName(attachment2Name);
          objMultipleDocRequest.sourceFileName = 'IPMS-'+caseNumber+'-'+extractName(attachment2Name);
          attachment2 = objMultipleDocRequest.sourceFileName;
          lstMultipleDocReq.add(objMultipleDocRequest);
        }


        UploadMultipleDocController.data objResponseData = new UploadMultipleDocController.data();
        objResponseData = UploadMultipleDocController.getMultipleDocUrl(lstMultipleDocReq);
        system.debug('objResponseData DOC*****'+objResponseData);
        if(objResponseData != null && objResponseData.status == 'S'
        && objResponseData.Data != null && objResponseData.Data.size() > 0){

          for(UploadMultipleDocController.MultipleDocResponse objData : objResponseData.Data){
            if(String.isNotBlank(objData.url) && String.isNotBlank( crfFileName ) && (objData.PARAM_ID.containsIgnoreCase(crfFileName))){
              complaintCaseObj.CRF_File_URL__c  = objData.url;
            }

            if(String.isNotBlank(objData.url) && String.isNotBlank( attachment1 ) && (objData.PARAM_ID.containsIgnoreCase(attachment1))){
              complaintCaseObj.Additional_Doc_File_URL__c  = objData.url;
            }

            if(String.isNotBlank(objData.url) && String.isNotBlank( attachment2 ) && (objData.PARAM_ID.containsIgnoreCase(attachment2))){
              complaintCaseObj.OD_File_URL__c  = objData.url;
            }
          }

          upsert complaintCaseObj;

          if( !crfAttachmentUploaded && String.isNotBlank( crfAttachmentBody ) && String.isNotBlank( crfAttachmentName ) ){
            uploadAttachment( complaintCaseObj.Id, extractType(crfAttachmentName), ComplaintProcessUtility.COMPLAINT_CRF_NAME, complaintCaseObj.CRF_File_URL__c );
            crfAttachmentBody = null;
            crfAttachmentName = null;

          }

          if( !attachment1Uploaded && String.isNotBlank( attachment1Body ) && String.isNotBlank( attachment1Name ) ){
            uploadAttachment( complaintCaseObj.Id, extractType(attachment1Name), ComplaintProcessUtility.COMPLAINT_ATT1_NAME, complaintCaseObj.Additional_Doc_File_URL__c );
            attachment1Body = null;
            attachment1Name = null;
          }

          if( !attachment2Uploaded && String.isNotBlank( attachment2Body ) && String.isNotBlank( attachment2Name ) ){
            uploadAttachment( complaintCaseObj.Id, extractType(attachment2Name), ComplaintProcessUtility.COMPLAINT_ATT2_NAME, complaintCaseObj.OD_File_URL__c );
            attachment2Body = null;
            attachment2Name = null;
          }
        }
      } catch(Exception excGen) {
          blnFlag = false;
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, excGen.getMessage()));
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, excGen.getStackTraceString()));
      }
      System.debug('======= end saveComplaintCase=======');
      //return blnFlag;
      return null;
    }

    public void uploadAttachment( Id parentId, String strType, String strAttachmentName, String fileUrl ) {
      System.debug('----selectedAcc in uploadAttachment ----' + selectedAcc);
      SR_Attachments__c srAttchmentObj = new SR_Attachments__c(Case__c = parentId,Account__c = selectedAcc,Type__c = strType,Name=strAttachmentName, Attachment_URL__c=fileUrl);
      insert srAttchmentObj;


      /*SR_Attachments__c srAttchmentObj = new SR_Attachments__c(Case__c = parentId,Type__c = strType,Name=strAttachmentName);
      insert srAttchmentObj;

      Attachment objAttach = new Attachment();
      objAttach.Body = objBody;
      objAttach.Name = strType + strfileName;
      objAttach.ParentId = srAttchmentObj.Id;

      insert objAttach;

      srAttchmentObj.Attachment__c = objAttach.Id;
      update srAttchmentObj;*/
    }

    @testVisible
    public String extractName( String strName ) {
      return strName.substring( strName.lastIndexOf('\\')+1 ) ;
    }

    @testVisible
    public Blob extractBody( String strBody ) {
      strBody = EncodingUtil.base64Decode( strBody ).toString();
      return EncodingUtil.base64Decode( strBody.substring( strBody.lastIndexOf(',')+1 ) );
    }

    @testVisible
    public String extractType( String strName ) {
      strName = strName.substring( strName.lastIndexOf('\\')+1 );
      return strName.substring( strName.lastIndexOf('.')+1 ) ;
    }
}