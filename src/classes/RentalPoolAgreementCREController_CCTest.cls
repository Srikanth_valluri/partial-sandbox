@isTest
public with sharing class RentalPoolAgreementCREController_CCTest {
	static Account objAcc;
    static Id recTypeRPAgreement;
    static NSIBPM__Service_Request__c objDealSR;
    static list<Booking__c> listCreateBookingForAccount;
    static list<Booking_Unit__c> listCreateBookingUnit;
    static Payment_Plan__c objPP;
    
    static void init() {
        objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Mobile_Phone_Encrypt__c = '9674858963';
        objAcc.Mobile_Country_Code__c = 'India: 0091';
        objAcc.Mobile_Encrypt__c = '9674858963';
        insert objAcc ;
        system.debug('objAcc=='+objAcc);

        recTypeRPAgreement = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Agreement').getRecordTypeId();
        system.debug('recTypeRPAgreement=='+recTypeRPAgreement);

        objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        listCreateBookingForAccount = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objDealSR.Id, 1);
        insert listCreateBookingForAccount;

        listCreateBookingUnit = TestDataFactory_CRM.createBookingUnits(listCreateBookingForAccount, 1);
        listCreateBookingUnit[0].Registration_Status__c = 'Agreement executed by DAMAC';
        listCreateBookingUnit[0].Rental_Pool__c = false;
        insert listCreateBookingUnit;
    }

    @isTest static void testRentalPoolInitProcessEligible() {
        init();
        objPP = new Payment_Plan__c();
        objPP.Booking_Unit__c = listCreateBookingUnit[0].Id;
        objPP.Status__c = 'Active';
        insert objPP;
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(1));
        RentalPoolAgreementCREController_CC rentalObj = new RentalPoolAgreementCREController_CC();
        rentalObj.strAccountID = objAcc.Id;
        rentalObj.strSRType = 'RentalPool';
        rentalObj.getName();
        rentalObj.strSelectedUnit = listCreateBookingUnit[0].Id;
        rentalObj.getDetails();
        rentalObj.generateSoA();
        rentalObj.generateFMSoA();
        rentalObj.getCRFdocument();
        Test.stopTest();
    }

    @isTest static void testRentalPoolInitProcessNotEligible() {
        init();
        objPP = new Payment_Plan__c();
        objPP.Booking_Unit__c = listCreateBookingUnit[0].Id;
        objPP.Status__c = 'Active';
        insert objPP;
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(2));
        RentalPoolAgreementCREController_CC rentalObj = new RentalPoolAgreementCREController_CC();
        rentalObj.strAccountID = objAcc.Id;
        rentalObj.strSRType = 'RentalPool';
        rentalObj.getName();
        rentalObj.strSelectedUnit = listCreateBookingUnit[0].Id;
        rentalObj.getDetails();
        Test.stopTest();
    }

    @isTest static void testRentalPoolInitProcessNoResponse() {
        init();
        objPP = new Payment_Plan__c();
        objPP.Booking_Unit__c = listCreateBookingUnit[0].Id;
        objPP.Status__c = 'Active';
        insert objPP;
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(3));
        RentalPoolAgreementCREController_CC rentalObj = new RentalPoolAgreementCREController_CC();
        rentalObj.strAccountID = objAcc.Id;
        rentalObj.strSRType = 'RentalPool';
        rentalObj.getName();
        rentalObj.strSelectedUnit = listCreateBookingUnit[0].Id;
        rentalObj.getDetails();
        Test.stopTest();
    }

    @isTest static void testRentalPoolInitProcessWithExcepMsg() {
        init();
        objPP = new Payment_Plan__c();
        objPP.Booking_Unit__c = listCreateBookingUnit[0].Id;
        objPP.Status__c = 'Active';
        insert objPP;
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(4));
        RentalPoolAgreementCREController_CC rentalObj = new RentalPoolAgreementCREController_CC();
        rentalObj.strAccountID = objAcc.Id;
        rentalObj.strSRType = 'RentalPool';
        rentalObj.getName();
        rentalObj.strSelectedUnit = listCreateBookingUnit[0].Id;
        rentalObj.getDetails();
        Test.stopTest();
    }

    @isTest static void testRentalPoolInitProcessWithoutExcepMsg() {
        init();
        objPP = new Payment_Plan__c();
        objPP.Booking_Unit__c = listCreateBookingUnit[0].Id;
        objPP.Status__c = 'Active';
        insert objPP;
        
        
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(5));
        RentalPoolAgreementCREController_CC rentalObj = new RentalPoolAgreementCREController_CC();
        rentalObj.strAccountID = objAcc.Id;
        rentalObj.strSRType = 'RentalPool';
        rentalObj.getName();
        rentalObj.strSelectedUnit = listCreateBookingUnit[0].Id;
        rentalObj.getDetails();
        Test.stopTest();
    }
    
    @isTest static void testRentalPoolProceedRP() {
        init();
        objPP = new Payment_Plan__c();
        objPP.Booking_Unit__c = listCreateBookingUnit[0].Id;
        objPP.Status__c = 'Active';
        insert objPP;
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(1));
        RentalPoolAgreementCREController_CC rentalObj = new RentalPoolAgreementCREController_CC();
        rentalObj.strAccountID = objAcc.Id;
        rentalObj.strPOAAttachmentBody = 'ZGF0YTphcHBsaWNhdGlvbi9wZGY7YmFzZTY0LEpWQkVSaTB4TGpRS0plTGp6OU1LTkNBd0lHOWlhaUE4UEM5R2FXeDBaWEl2Um14aGRHVkVaV052WkdVdlRHVnVaM1JvSURFeU1ETStQbk4wY21WaGJRcDRuSTFYVFhQYk5oQzk4MWZzclVsSEF3T2srSldiWWllSFRPTW10cWFkVHRNRFJFSVdQQ0FwRTZRVC8vc3VDRklDUlZuMGVDYkdrdnNlM2o0dUZzNlQ5M0h0VVloVEg5YTU5Mm50ZmZlZVBFcG9FSWZ3MC9QaEM3NTg5QmlGcjk2Ly8xSEl2U0NDT0l5ZzhNTFlybFMzQ2lKQ2w3Z09Sa3Y3ZnVmOTdaVkltcVRVaC9HditzRWhqQklTRHBSUlRHSVRoTVFQTzY1UnVQTzJ2eCtCSnk5dEdFWWtzWkZac2dIVUU5dTN4YXU1VnNzWU9LZ3JKdXBHcE9mQVdHMjhESm1wT29uOHVLcytRWk93Zko4bFpKbEF4QkpEekxxRnNvdllzaDh5WXJmNEpDUnAyc01jam1uNklkT0ZoMkdBRWkzYW9acG1ENGtqTUh2THZtNEpIV3hVNmNWTjV5cys2cDlJUEZ0dlFIMFRoZjZTeE5abnUxUjJHWkMwZ3c1WmZleFlOU0JIUkZQRWtEd2lZRzlHc3hQb3FXeEh5bVN2cytJcG1COXNNNlQyZzc3cExiQVBEWTl2dXRmRVhlc3pOeG9xT0lMWk5KbEZySTl3U1VuZ2VPY2YzeGNYc3Eya1U3QWpzNWpJZEtsZkljQU5ndTdCVUhNZkh0RitTcUpqR1gwMEJiTnBjc3g2OTVoWnNwT0s3ZHZpMVZ3clp3eDBCQllUZ1M3dFdiaVZ5T2pJckQ1V2J1eDh0ajV5aGFmTUxkbEdRMzVLWFMxOTFJSHhEcm42aklaUlZMWGVlcXpyT2daQnNDUXBnelFrT09yWGhmZnVTeVhMQmo2Mkw2TCtUY085ZkNoNTA5Yml4enU1QmI3Zks1bnhqUkkvM3I5ZlA1cTdLS0JCLzcwUFIyZjQvSDFvUHI4ZDNaaDZDQzVwWWdFQ0k4eE5DUTVpSTJvaXA5KzlJL0NCamVCbXFxSDVhZFJCcjF2ZFZJV280VTQ4dFVJMzhMbXFDeGMrM2Q4UStJeEVkdThiM29qTCtjTXNQR0srMWRXanlKckxzR0VLSG1HcmZRTzNGVGtwN293NjJ0M2ZCc0xZRmFOWFBtWHhaZFNnMFVGZUJnenFqb0JiTkk2ckdlZERQTjVSNzd5U0FsdnBSalJjS2oxcmVlZ25KRWpHMyt5V0Z6UGVIMGJxQWV3MDhCbDhRdEpnaE1mekV5WXUvclZPZi8xemhBeFBtL1ZvUmVtY3I3M2dJMllHZ0Jzc0k1OUVOdm5QWm9kMTdldHFMK3BHQ2cwL1piT2JKMWpHQThITjZ1dnFlcVpYYURkTGx2aTNFN09nZjRTZWdaZ2o2eUp1cXpIZzFIY2pDakY5NDNlZXYyQTFleVc0RmxCZzQ4aXFISHQvdG11QzFDZExTM0xmYnN5aGcycDdPT3NmWUtVMXpnekROenljYWVEQXpHMTJkblNNa2FueDZBVHIrNFRaNXYranFrVUJjcS9iQXZKS1ZUVm8yUUMyWTdPQXJDbzFDaFU0eUlEbmNpOTFKc3NIRUVvMkJHNWJwWGdCaFFRdGlnWGtNbXVRUXBaUXRtVUdUNjNVQzh3VXBpSjhYdkJmc21qMXZES2FtSzR6eW1xcFcwM2d1cTM1UmhvSm9vRlNibmI0ajFZRS9wTFBIQmw3MGJ4c3hBS2V4VTVtcmVMd0xCdU9YNGRyellGbkM1QWNIMHNOUmFYUUh5bEE0UU1rL3lhVVFvRkNvMit3eGNMbUZmcnBrZ1MyZDFEVTNwVForZlRVOGdiRUE0cDhObHRzV29WbGE1RXZvTUZLR21sMnNMSkV6ZEcvZTVIRHZsWFBzdVMxWTFRdU9icUpWRWlheS82aGtnOVkxYnkwaEpLbHZSMWxnZWN1bDZqR2ZBa3kyK1Ara2c0SGI2VVVsQ0lUNkYzOWd2Wm1yVkdoUWUrcVZ1V3dFYWl1NGRsTzVMT3NEQy90L3B5dEs0UGNTdlFiT1Y2Z0g3cDNRbkZ6Z25ENjdubmRtSjNtanBMOWN5TVpydHpydTYvdzZaZkkya1kraTI2UXdvZkxGT2JhcGpHa0NZbDdpcXJvU3J4YVpVYkxXd1RFaHl2L3VQZmgwajhJTVA4Qi91NzlENmpyZmVNS1pXNWtjM1J5WldGdENtVnVaRzlpYWdveElEQWdiMkpxUER3dlEyOXVkR1Z1ZEhNZ05DQXdJRkl2Vkhsd1pTOVFZV2RsTDFKbGMyOTFjbU5sY3p3OEwxQnliMk5UWlhRZ1d5OVFSRVlnTDFSbGVIUWdMMGx0WVdkbFFpQXZTVzFoWjJWRElDOUpiV0ZuWlVsZEwwWnZiblE4UEM5R01TQXlJREFnVWk5R01pQXpJREFnVWo0K1BqNHZVR0Z5Wlc1MElEVWdNQ0JTTDAxbFpHbGhRbTk0V3pBZ01DQTJNVElnTnpreVhUNCtDbVZ1Wkc5aWFnbzJJREFnYjJKcUlEdzhMMFpwYkhSbGNpOUdiR0YwWlVSbFkyOWtaUzlNWlc1bmRHZ2dPVEkrUG5OMGNtVmhiUXA0bkN2a2NncmhNbEF3dHpSU0NFbmhjZzNoQ3VRcTVETFFNekEyTjFVbzV6SlM4QUpLWm5FWkdpajRja1hIR2lpa2NCbWJLWmlibWlua2NwbWFRMWc1WUpheG1aNkJDWkJ0ak1LRXlHZHdoWFBsZ1EwTzVBSUErUm9XVlFwbGJtUnpkSEpsWVcwS1pXNWtiMkpxQ2pjZ01DQnZZbW84UEM5RGIyNTBaVzUwY3lBMklEQWdVaTlVZVhCbEwxQmhaMlV2VW1WemIzVnlZMlZ6UER3dlVISnZZMU5sZENCYkwxQkVSaUF2VkdWNGRDQXZTVzFoWjJWQ0lDOUpiV0ZuWlVNZ0wwbHRZV2RsU1YwK1BpOVFZWEpsYm5RZ05TQXdJRkl2VFdWa2FXRkNiM2hiTUNBd0lEWXhNaUEzT1RKZFBqNEtaVzVrYjJKcUNqTWdNQ0J2WW1vOFBDOVRkV0owZVhCbEwxUjVjR1V4TDFSNWNHVXZSbTl1ZEM5Q1lYTmxSbTl1ZEM5SVpXeDJaWFJwWTJFdlJXNWpiMlJwYm1jdlYybHVRVzV6YVVWdVkyOWthVzVuUGo0S1pXNWtiMkpxQ2pJZ01DQnZZbW84UEM5VGRXSjBlWEJsTDFSNWNHVXhMMVI1Y0dVdlJtOXVkQzlDWVhObFJtOXVkQzlJWld4MlpYUnBZMkV0UW05c1pDOUZibU52WkdsdVp5OVhhVzVCYm5OcFJXNWpiMlJwYm1jK1BncGxibVJ2WW1vS05TQXdJRzlpYWp3OEwwdHBaSE5iTVNBd0lGSWdOeUF3SUZKZEwxUjVjR1V2VUdGblpYTXZRMjkxYm5RZ01qNCtDbVZ1Wkc5aWFnbzRJREFnYjJKcVBEd3ZWSGx3WlM5RFlYUmhiRzluTDFCaFoyVnpJRFVnTUNCU1BqNEtaVzVrYjJKcUNqa2dNQ0J2WW1vOFBDOU5iMlJFWVhSbEtFUTZNakF4TnpFd01URXlNREEyTWpCYUtTOURjbVZoZEdsdmJrUmhkR1VvUkRveU1ERTNNVEF4TVRJd01EWXlNRm9wTDFCeWIyUjFZMlZ5S0dsVVpYaDBJREl1TUM0NElGd29ZbmtnYkc5M1lXZHBaUzVqYjIxY0tTaytQZ3BsYm1Sdlltb0tlSEpsWmdvd0lERXdDakF3TURBd01EQXdNREFnTmpVMU16VWdaaUFLTURBd01EQXdNVEk0TmlBd01EQXdNQ0J1SUFvd01EQXdNREF4T0RNMElEQXdNREF3SUc0Z0NqQXdNREF3TURFM05EY2dNREF3TURBZ2JpQUtNREF3TURBd01EQXhOU0F3TURBd01DQnVJQW93TURBd01EQXhPVEkySURBd01EQXdJRzRnQ2pBd01EQXdNREUwTlRFZ01EQXdNREFnYmlBS01EQXdNREF3TVRZd09TQXdNREF3TUNCdUlBb3dNREF3TURBeE9UZ3lJREF3TURBd0lHNGdDakF3TURBd01ESXdNallnTURBd01EQWdiaUFLZEhKaGFXeGxjZ284UEM5SmJtWnZJRGtnTUNCU0wwbEVJRnM4WWpjellUWXhaakkyWm1Vd05UTXdZV0V5T1RjMU9XUXhNR1JpWkRFM09EaytQRGczWVRnM05ERTVaVFUzTTJJNVpHTTVaakF4WkRaa1lUQTJNamt5T1dKbVBsMHZVbTl2ZENBNElEQWdVaTlUYVhwbElERXdQajRLYzNSaGNuUjRjbVZtQ2pJeE5EVUtKU1ZGVDBZSw';
        rentalObj.strPOAAttachmentName = 'POA Name';
        rentalObj.strCRFAttachmentBody = 'ZGF0YTphcHBsaWNhdGlvbi9wZGY7YmFzZTY0LEpWQkVSaTB4TGpRS0plTGp6OU1LTkNBd0lHOWlhaUE4UEM5R2FXeDBaWEl2Um14aGRHVkVaV052WkdVdlRHVnVaM1JvSURFeU1ETStQbk4wY21WaGJRcDRuSTFYVFhQYk5oQzk4MWZzclVsSEF3T2srSldiWWllSFRPTW10cWFkVHRNRFJFSVdQQ0FwRTZRVC8vc3VDRklDUlZuMGVDYkdrdnNlM2o0dUZzNlQ5M0h0VVloVEg5YTU5Mm50ZmZlZVBFcG9FSWZ3MC9QaEM3NTg5QmlGcjk2Ly8xSEl2U0NDT0l5ZzhNTFlybFMzQ2lKQ2w3Z09Sa3Y3ZnVmOTdaVkltcVRVaC9HditzRWhqQklTRHBSUlRHSVRoTVFQTzY1UnVQTzJ2eCtCSnk5dEdFWWtzWkZac2dIVUU5dTN4YXU1VnNzWU9LZ3JKdXBHcE9mQVdHMjhESm1wT29uOHVLcytRWk93Zko4bFpKbEF4QkpEekxxRnNvdllzaDh5WXJmNEpDUnAyc01jam1uNklkT0ZoMkdBRWkzYW9acG1ENGtqTUh2THZtNEpIV3hVNmNWTjV5cys2cDlJUEZ0dlFIMFRoZjZTeE5abnUxUjJHWkMwZ3c1WmZleFlOU0JIUkZQRWtEd2lZRzlHc3hQb3FXeEh5bVN2cytJcG1COXNNNlQyZzc3cExiQVBEWTl2dXRmRVhlc3pOeG9xT0lMWk5KbEZySTl3U1VuZ2VPY2YzeGNYc3Eya1U3QWpzNWpJZEtsZkljQU5ndTdCVUhNZkh0RitTcUpqR1gwMEJiTnBjc3g2OTVoWnNwT0s3ZHZpMVZ3clp3eDBCQllUZ1M3dFdiaVZ5T2pJckQ1V2J1eDh0ajV5aGFmTUxkbEdRMzVLWFMxOTFJSHhEcm42aklaUlZMWGVlcXpyT2daQnNDUXBnelFrT09yWGhmZnVTeVhMQmo2Mkw2TCtUY085ZkNoNTA5Yml4enU1QmI3Zks1bnhqUkkvM3I5ZlA1cTdLS0JCLzcwUFIyZjQvSDFvUHI4ZDNaaDZDQzVwWWdFQ0k4eE5DUTVpSTJvaXA5KzlJL0NCamVCbXFxSDVhZFJCcjF2ZFZJV280VTQ4dFVJMzhMbXFDeGMrM2Q4UStJeEVkdThiM29qTCtjTXNQR0srMWRXanlKckxzR0VLSG1HcmZRTzNGVGtwN293NjJ0M2ZCc0xZRmFOWFBtWHhaZFNnMFVGZUJnenFqb0JiTkk2ckdlZERQTjVSNzd5U0FsdnBSalJjS2oxcmVlZ25KRWpHMyt5V0Z6UGVIMGJxQWV3MDhCbDhRdEpnaE1mekV5WXUvclZPZi8xemhBeFBtL1ZvUmVtY3I3M2dJMllHZ0Jzc0k1OUVOdm5QWm9kMTdldHFMK3BHQ2cwL1piT2JKMWpHQThITjZ1dnFlcVpYYURkTGx2aTNFN09nZjRTZWdaZ2o2eUp1cXpIZzFIY2pDakY5NDNlZXYyQTFleVc0RmxCZzQ4aXFISHQvdG11QzFDZExTM0xmYnN5aGcycDdPT3NmWUtVMXpnekROenljYWVEQXpHMTJkblNNa2FueDZBVHIrNFRaNXYranFrVUJjcS9iQXZKS1ZUVm8yUUMyWTdPQXJDbzFDaFU0eUlEbmNpOTFKc3NIRUVvMkJHNWJwWGdCaFFRdGlnWGtNbXVRUXBaUXRtVUdUNjNVQzh3VXBpSjhYdkJmc21qMXZES2FtSzR6eW1xcFcwM2d1cTM1UmhvSm9vRlNibmI0ajFZRS9wTFBIQmw3MGJ4c3hBS2V4VTVtcmVMd0xCdU9YNGRyellGbkM1QWNIMHNOUmFYUUh5bEE0UU1rL3lhVVFvRkNvMit3eGNMbUZmcnBrZ1MyZDFEVTNwVForZlRVOGdiRUE0cDhObHRzV29WbGE1RXZvTUZLR21sMnNMSkV6ZEcvZTVIRHZsWFBzdVMxWTFRdU9icUpWRWlheS82aGtnOVkxYnkwaEpLbHZSMWxnZWN1bDZqR2ZBa3kyK1Ara2c0SGI2VVVsQ0lUNkYzOWd2Wm1yVkdoUWUrcVZ1V3dFYWl1NGRsTzVMT3NEQy90L3B5dEs0UGNTdlFiT1Y2Z0g3cDNRbkZ6Z25ENjdubmRtSjNtanBMOWN5TVpydHpydTYvdzZaZkkya1kraTI2UXdvZkxGT2JhcGpHa0NZbDdpcXJvU3J4YVpVYkxXd1RFaHl2L3VQZmgwajhJTVA4Qi91NzlENmpyZmVNS1pXNWtjM1J5WldGdENtVnVaRzlpYWdveElEQWdiMkpxUER3dlEyOXVkR1Z1ZEhNZ05DQXdJRkl2Vkhsd1pTOVFZV2RsTDFKbGMyOTFjbU5sY3p3OEwxQnliMk5UWlhRZ1d5OVFSRVlnTDFSbGVIUWdMMGx0WVdkbFFpQXZTVzFoWjJWRElDOUpiV0ZuWlVsZEwwWnZiblE4UEM5R01TQXlJREFnVWk5R01pQXpJREFnVWo0K1BqNHZVR0Z5Wlc1MElEVWdNQ0JTTDAxbFpHbGhRbTk0V3pBZ01DQTJNVElnTnpreVhUNCtDbVZ1Wkc5aWFnbzJJREFnYjJKcUlEdzhMMFpwYkhSbGNpOUdiR0YwWlVSbFkyOWtaUzlNWlc1bmRHZ2dPVEkrUG5OMGNtVmhiUXA0bkN2a2NncmhNbEF3dHpSU0NFbmhjZzNoQ3VRcTVETFFNekEyTjFVbzV6SlM4QUpLWm5FWkdpajRja1hIR2lpa2NCbWJLWmlibWlua2NwbWFRMWc1WUpheG1aNkJDWkJ0ak1LRXlHZHdoWFBsZ1EwTzVBSUErUm9XVlFwbGJtUnpkSEpsWVcwS1pXNWtiMkpxQ2pjZ01DQnZZbW84UEM5RGIyNTBaVzUwY3lBMklEQWdVaTlVZVhCbEwxQmhaMlV2VW1WemIzVnlZMlZ6UER3dlVISnZZMU5sZENCYkwxQkVSaUF2VkdWNGRDQXZTVzFoWjJWQ0lDOUpiV0ZuWlVNZ0wwbHRZV2RsU1YwK1BpOVFZWEpsYm5RZ05TQXdJRkl2VFdWa2FXRkNiM2hiTUNBd0lEWXhNaUEzT1RKZFBqNEtaVzVrYjJKcUNqTWdNQ0J2WW1vOFBDOVRkV0owZVhCbEwxUjVjR1V4TDFSNWNHVXZSbTl1ZEM5Q1lYTmxSbTl1ZEM5SVpXeDJaWFJwWTJFdlJXNWpiMlJwYm1jdlYybHVRVzV6YVVWdVkyOWthVzVuUGo0S1pXNWtiMkpxQ2pJZ01DQnZZbW84UEM5VGRXSjBlWEJsTDFSNWNHVXhMMVI1Y0dVdlJtOXVkQzlDWVhObFJtOXVkQzlJWld4MlpYUnBZMkV0UW05c1pDOUZibU52WkdsdVp5OVhhVzVCYm5OcFJXNWpiMlJwYm1jK1BncGxibVJ2WW1vS05TQXdJRzlpYWp3OEwwdHBaSE5iTVNBd0lGSWdOeUF3SUZKZEwxUjVjR1V2VUdGblpYTXZRMjkxYm5RZ01qNCtDbVZ1Wkc5aWFnbzRJREFnYjJKcVBEd3ZWSGx3WlM5RFlYUmhiRzluTDFCaFoyVnpJRFVnTUNCU1BqNEtaVzVrYjJKcUNqa2dNQ0J2WW1vOFBDOU5iMlJFWVhSbEtFUTZNakF4TnpFd01URXlNREEyTWpCYUtTOURjbVZoZEdsdmJrUmhkR1VvUkRveU1ERTNNVEF4TVRJd01EWXlNRm9wTDFCeWIyUjFZMlZ5S0dsVVpYaDBJREl1TUM0NElGd29ZbmtnYkc5M1lXZHBaUzVqYjIxY0tTaytQZ3BsYm1Sdlltb0tlSEpsWmdvd0lERXdDakF3TURBd01EQXdNREFnTmpVMU16VWdaaUFLTURBd01EQXdNVEk0TmlBd01EQXdNQ0J1SUFvd01EQXdNREF4T0RNMElEQXdNREF3SUc0Z0NqQXdNREF3TURFM05EY2dNREF3TURBZ2JpQUtNREF3TURBd01EQXhOU0F3TURBd01DQnVJQW93TURBd01EQXhPVEkySURBd01EQXdJRzRnQ2pBd01EQXdNREUwTlRFZ01EQXdNREFnYmlBS01EQXdNREF3TVRZd09TQXdNREF3TUNCdUlBb3dNREF3TURBeE9UZ3lJREF3TURBd0lHNGdDakF3TURBd01ESXdNallnTURBd01EQWdiaUFLZEhKaGFXeGxjZ284UEM5SmJtWnZJRGtnTUNCU0wwbEVJRnM4WWpjellUWXhaakkyWm1Vd05UTXdZV0V5T1RjMU9XUXhNR1JpWkRFM09EaytQRGczWVRnM05ERTVaVFUzTTJJNVpHTTVaakF4WkRaa1lUQTJNamt5T1dKbVBsMHZVbTl2ZENBNElEQWdVaTlUYVhwbElERXdQajRLYzNSaGNuUjRjbVZtQ2pJeE5EVUtKU1ZGVDBZSw';
        rentalObj.strCRFAttachmentName = 'CRF Name';
        rentalObj.strSRType = 'RentalPool';
        rentalObj.getName();
        rentalObj.strSelectedUnit = listCreateBookingUnit[0].Id;
        rentalObj.getDetails();
        rentalObj.generateSoA();
        rentalObj.generateFMSoA();
        rentalObj.getCRFdocument();
        rentalObj.proceedCase();
        //rentalObj.submitSR();
        Test.stopTest();
    }

    @isTest static void testRentalPoolSubmitRP() {
        init();
        objPP = new Payment_Plan__c();
        objPP.Booking_Unit__c = listCreateBookingUnit[0].Id;
        objPP.Status__c = 'Active';
        insert objPP;
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(1));
        RentalPoolAgreementCREController_CC rentalObj = new RentalPoolAgreementCREController_CC();
        rentalObj.strAccountID = objAcc.Id;
        rentalObj.strPOAAttachmentBody = 'ZGF0YTphcHBsaWNhdGlvbi9wZGY7YmFzZTY0LEpWQkVSaTB4TGpRS0plTGp6OU1LTkNBd0lHOWlhaUE4UEM5R2FXeDBaWEl2Um14aGRHVkVaV052WkdVdlRHVnVaM1JvSURFeU1ETStQbk4wY21WaGJRcDRuSTFYVFhQYk5oQzk4MWZzclVsSEF3T2srSldiWWllSFRPTW10cWFkVHRNRFJFSVdQQ0FwRTZRVC8vc3VDRklDUlZuMGVDYkdrdnNlM2o0dUZzNlQ5M0h0VVloVEg5YTU5Mm50ZmZlZVBFcG9FSWZ3MC9QaEM3NTg5QmlGcjk2Ly8xSEl2U0NDT0l5ZzhNTFlybFMzQ2lKQ2w3Z09Sa3Y3ZnVmOTdaVkltcVRVaC9HditzRWhqQklTRHBSUlRHSVRoTVFQTzY1UnVQTzJ2eCtCSnk5dEdFWWtzWkZac2dIVUU5dTN4YXU1VnNzWU9LZ3JKdXBHcE9mQVdHMjhESm1wT29uOHVLcytRWk93Zko4bFpKbEF4QkpEekxxRnNvdllzaDh5WXJmNEpDUnAyc01jam1uNklkT0ZoMkdBRWkzYW9acG1ENGtqTUh2THZtNEpIV3hVNmNWTjV5cys2cDlJUEZ0dlFIMFRoZjZTeE5abnUxUjJHWkMwZ3c1WmZleFlOU0JIUkZQRWtEd2lZRzlHc3hQb3FXeEh5bVN2cytJcG1COXNNNlQyZzc3cExiQVBEWTl2dXRmRVhlc3pOeG9xT0lMWk5KbEZySTl3U1VuZ2VPY2YzeGNYc3Eya1U3QWpzNWpJZEtsZkljQU5ndTdCVUhNZkh0RitTcUpqR1gwMEJiTnBjc3g2OTVoWnNwT0s3ZHZpMVZ3clp3eDBCQllUZ1M3dFdiaVZ5T2pJckQ1V2J1eDh0ajV5aGFmTUxkbEdRMzVLWFMxOTFJSHhEcm42aklaUlZMWGVlcXpyT2daQnNDUXBnelFrT09yWGhmZnVTeVhMQmo2Mkw2TCtUY085ZkNoNTA5Yml4enU1QmI3Zks1bnhqUkkvM3I5ZlA1cTdLS0JCLzcwUFIyZjQvSDFvUHI4ZDNaaDZDQzVwWWdFQ0k4eE5DUTVpSTJvaXA5KzlJL0NCamVCbXFxSDVhZFJCcjF2ZFZJV280VTQ4dFVJMzhMbXFDeGMrM2Q4UStJeEVkdThiM29qTCtjTXNQR0srMWRXanlKckxzR0VLSG1HcmZRTzNGVGtwN293NjJ0M2ZCc0xZRmFOWFBtWHhaZFNnMFVGZUJnenFqb0JiTkk2ckdlZERQTjVSNzd5U0FsdnBSalJjS2oxcmVlZ25KRWpHMyt5V0Z6UGVIMGJxQWV3MDhCbDhRdEpnaE1mekV5WXUvclZPZi8xemhBeFBtL1ZvUmVtY3I3M2dJMllHZ0Jzc0k1OUVOdm5QWm9kMTdldHFMK3BHQ2cwL1piT2JKMWpHQThITjZ1dnFlcVpYYURkTGx2aTNFN09nZjRTZWdaZ2o2eUp1cXpIZzFIY2pDakY5NDNlZXYyQTFleVc0RmxCZzQ4aXFISHQvdG11QzFDZExTM0xmYnN5aGcycDdPT3NmWUtVMXpnekROenljYWVEQXpHMTJkblNNa2FueDZBVHIrNFRaNXYranFrVUJjcS9iQXZKS1ZUVm8yUUMyWTdPQXJDbzFDaFU0eUlEbmNpOTFKc3NIRUVvMkJHNWJwWGdCaFFRdGlnWGtNbXVRUXBaUXRtVUdUNjNVQzh3VXBpSjhYdkJmc21qMXZES2FtSzR6eW1xcFcwM2d1cTM1UmhvSm9vRlNibmI0ajFZRS9wTFBIQmw3MGJ4c3hBS2V4VTVtcmVMd0xCdU9YNGRyellGbkM1QWNIMHNOUmFYUUh5bEE0UU1rL3lhVVFvRkNvMit3eGNMbUZmcnBrZ1MyZDFEVTNwVForZlRVOGdiRUE0cDhObHRzV29WbGE1RXZvTUZLR21sMnNMSkV6ZEcvZTVIRHZsWFBzdVMxWTFRdU9icUpWRWlheS82aGtnOVkxYnkwaEpLbHZSMWxnZWN1bDZqR2ZBa3kyK1Ara2c0SGI2VVVsQ0lUNkYzOWd2Wm1yVkdoUWUrcVZ1V3dFYWl1NGRsTzVMT3NEQy90L3B5dEs0UGNTdlFiT1Y2Z0g3cDNRbkZ6Z25ENjdubmRtSjNtanBMOWN5TVpydHpydTYvdzZaZkkya1kraTI2UXdvZkxGT2JhcGpHa0NZbDdpcXJvU3J4YVpVYkxXd1RFaHl2L3VQZmgwajhJTVA4Qi91NzlENmpyZmVNS1pXNWtjM1J5WldGdENtVnVaRzlpYWdveElEQWdiMkpxUER3dlEyOXVkR1Z1ZEhNZ05DQXdJRkl2Vkhsd1pTOVFZV2RsTDFKbGMyOTFjbU5sY3p3OEwxQnliMk5UWlhRZ1d5OVFSRVlnTDFSbGVIUWdMMGx0WVdkbFFpQXZTVzFoWjJWRElDOUpiV0ZuWlVsZEwwWnZiblE4UEM5R01TQXlJREFnVWk5R01pQXpJREFnVWo0K1BqNHZVR0Z5Wlc1MElEVWdNQ0JTTDAxbFpHbGhRbTk0V3pBZ01DQTJNVElnTnpreVhUNCtDbVZ1Wkc5aWFnbzJJREFnYjJKcUlEdzhMMFpwYkhSbGNpOUdiR0YwWlVSbFkyOWtaUzlNWlc1bmRHZ2dPVEkrUG5OMGNtVmhiUXA0bkN2a2NncmhNbEF3dHpSU0NFbmhjZzNoQ3VRcTVETFFNekEyTjFVbzV6SlM4QUpLWm5FWkdpajRja1hIR2lpa2NCbWJLWmlibWlua2NwbWFRMWc1WUpheG1aNkJDWkJ0ak1LRXlHZHdoWFBsZ1EwTzVBSUErUm9XVlFwbGJtUnpkSEpsWVcwS1pXNWtiMkpxQ2pjZ01DQnZZbW84UEM5RGIyNTBaVzUwY3lBMklEQWdVaTlVZVhCbEwxQmhaMlV2VW1WemIzVnlZMlZ6UER3dlVISnZZMU5sZENCYkwxQkVSaUF2VkdWNGRDQXZTVzFoWjJWQ0lDOUpiV0ZuWlVNZ0wwbHRZV2RsU1YwK1BpOVFZWEpsYm5RZ05TQXdJRkl2VFdWa2FXRkNiM2hiTUNBd0lEWXhNaUEzT1RKZFBqNEtaVzVrYjJKcUNqTWdNQ0J2WW1vOFBDOVRkV0owZVhCbEwxUjVjR1V4TDFSNWNHVXZSbTl1ZEM5Q1lYTmxSbTl1ZEM5SVpXeDJaWFJwWTJFdlJXNWpiMlJwYm1jdlYybHVRVzV6YVVWdVkyOWthVzVuUGo0S1pXNWtiMkpxQ2pJZ01DQnZZbW84UEM5VGRXSjBlWEJsTDFSNWNHVXhMMVI1Y0dVdlJtOXVkQzlDWVhObFJtOXVkQzlJWld4MlpYUnBZMkV0UW05c1pDOUZibU52WkdsdVp5OVhhVzVCYm5OcFJXNWpiMlJwYm1jK1BncGxibVJ2WW1vS05TQXdJRzlpYWp3OEwwdHBaSE5iTVNBd0lGSWdOeUF3SUZKZEwxUjVjR1V2VUdGblpYTXZRMjkxYm5RZ01qNCtDbVZ1Wkc5aWFnbzRJREFnYjJKcVBEd3ZWSGx3WlM5RFlYUmhiRzluTDFCaFoyVnpJRFVnTUNCU1BqNEtaVzVrYjJKcUNqa2dNQ0J2WW1vOFBDOU5iMlJFWVhSbEtFUTZNakF4TnpFd01URXlNREEyTWpCYUtTOURjbVZoZEdsdmJrUmhkR1VvUkRveU1ERTNNVEF4TVRJd01EWXlNRm9wTDFCeWIyUjFZMlZ5S0dsVVpYaDBJREl1TUM0NElGd29ZbmtnYkc5M1lXZHBaUzVqYjIxY0tTaytQZ3BsYm1Sdlltb0tlSEpsWmdvd0lERXdDakF3TURBd01EQXdNREFnTmpVMU16VWdaaUFLTURBd01EQXdNVEk0TmlBd01EQXdNQ0J1SUFvd01EQXdNREF4T0RNMElEQXdNREF3SUc0Z0NqQXdNREF3TURFM05EY2dNREF3TURBZ2JpQUtNREF3TURBd01EQXhOU0F3TURBd01DQnVJQW93TURBd01EQXhPVEkySURBd01EQXdJRzRnQ2pBd01EQXdNREUwTlRFZ01EQXdNREFnYmlBS01EQXdNREF3TVRZd09TQXdNREF3TUNCdUlBb3dNREF3TURBeE9UZ3lJREF3TURBd0lHNGdDakF3TURBd01ESXdNallnTURBd01EQWdiaUFLZEhKaGFXeGxjZ284UEM5SmJtWnZJRGtnTUNCU0wwbEVJRnM4WWpjellUWXhaakkyWm1Vd05UTXdZV0V5T1RjMU9XUXhNR1JpWkRFM09EaytQRGczWVRnM05ERTVaVFUzTTJJNVpHTTVaakF4WkRaa1lUQTJNamt5T1dKbVBsMHZVbTl2ZENBNElEQWdVaTlUYVhwbElERXdQajRLYzNSaGNuUjRjbVZtQ2pJeE5EVUtKU1ZGVDBZSw';
        rentalObj.strPOAAttachmentName = 'POA Name';
        rentalObj.strCRFAttachmentBody = 'ZGF0YTphcHBsaWNhdGlvbi9wZGY7YmFzZTY0LEpWQkVSaTB4TGpRS0plTGp6OU1LTkNBd0lHOWlhaUE4UEM5R2FXeDBaWEl2Um14aGRHVkVaV052WkdVdlRHVnVaM1JvSURFeU1ETStQbk4wY21WaGJRcDRuSTFYVFhQYk5oQzk4MWZzclVsSEF3T2srSldiWWllSFRPTW10cWFkVHRNRFJFSVdQQ0FwRTZRVC8vc3VDRklDUlZuMGVDYkdrdnNlM2o0dUZzNlQ5M0h0VVloVEg5YTU5Mm50ZmZlZVBFcG9FSWZ3MC9QaEM3NTg5QmlGcjk2Ly8xSEl2U0NDT0l5ZzhNTFlybFMzQ2lKQ2w3Z09Sa3Y3ZnVmOTdaVkltcVRVaC9HditzRWhqQklTRHBSUlRHSVRoTVFQTzY1UnVQTzJ2eCtCSnk5dEdFWWtzWkZac2dIVUU5dTN4YXU1VnNzWU9LZ3JKdXBHcE9mQVdHMjhESm1wT29uOHVLcytRWk93Zko4bFpKbEF4QkpEekxxRnNvdllzaDh5WXJmNEpDUnAyc01jam1uNklkT0ZoMkdBRWkzYW9acG1ENGtqTUh2THZtNEpIV3hVNmNWTjV5cys2cDlJUEZ0dlFIMFRoZjZTeE5abnUxUjJHWkMwZ3c1WmZleFlOU0JIUkZQRWtEd2lZRzlHc3hQb3FXeEh5bVN2cytJcG1COXNNNlQyZzc3cExiQVBEWTl2dXRmRVhlc3pOeG9xT0lMWk5KbEZySTl3U1VuZ2VPY2YzeGNYc3Eya1U3QWpzNWpJZEtsZkljQU5ndTdCVUhNZkh0RitTcUpqR1gwMEJiTnBjc3g2OTVoWnNwT0s3ZHZpMVZ3clp3eDBCQllUZ1M3dFdiaVZ5T2pJckQ1V2J1eDh0ajV5aGFmTUxkbEdRMzVLWFMxOTFJSHhEcm42aklaUlZMWGVlcXpyT2daQnNDUXBnelFrT09yWGhmZnVTeVhMQmo2Mkw2TCtUY085ZkNoNTA5Yml4enU1QmI3Zks1bnhqUkkvM3I5ZlA1cTdLS0JCLzcwUFIyZjQvSDFvUHI4ZDNaaDZDQzVwWWdFQ0k4eE5DUTVpSTJvaXA5KzlJL0NCamVCbXFxSDVhZFJCcjF2ZFZJV280VTQ4dFVJMzhMbXFDeGMrM2Q4UStJeEVkdThiM29qTCtjTXNQR0srMWRXanlKckxzR0VLSG1HcmZRTzNGVGtwN293NjJ0M2ZCc0xZRmFOWFBtWHhaZFNnMFVGZUJnenFqb0JiTkk2ckdlZERQTjVSNzd5U0FsdnBSalJjS2oxcmVlZ25KRWpHMyt5V0Z6UGVIMGJxQWV3MDhCbDhRdEpnaE1mekV5WXUvclZPZi8xemhBeFBtL1ZvUmVtY3I3M2dJMllHZ0Jzc0k1OUVOdm5QWm9kMTdldHFMK3BHQ2cwL1piT2JKMWpHQThITjZ1dnFlcVpYYURkTGx2aTNFN09nZjRTZWdaZ2o2eUp1cXpIZzFIY2pDakY5NDNlZXYyQTFleVc0RmxCZzQ4aXFISHQvdG11QzFDZExTM0xmYnN5aGcycDdPT3NmWUtVMXpnekROenljYWVEQXpHMTJkblNNa2FueDZBVHIrNFRaNXYranFrVUJjcS9iQXZKS1ZUVm8yUUMyWTdPQXJDbzFDaFU0eUlEbmNpOTFKc3NIRUVvMkJHNWJwWGdCaFFRdGlnWGtNbXVRUXBaUXRtVUdUNjNVQzh3VXBpSjhYdkJmc21qMXZES2FtSzR6eW1xcFcwM2d1cTM1UmhvSm9vRlNibmI0ajFZRS9wTFBIQmw3MGJ4c3hBS2V4VTVtcmVMd0xCdU9YNGRyellGbkM1QWNIMHNOUmFYUUh5bEE0UU1rL3lhVVFvRkNvMit3eGNMbUZmcnBrZ1MyZDFEVTNwVForZlRVOGdiRUE0cDhObHRzV29WbGE1RXZvTUZLR21sMnNMSkV6ZEcvZTVIRHZsWFBzdVMxWTFRdU9icUpWRWlheS82aGtnOVkxYnkwaEpLbHZSMWxnZWN1bDZqR2ZBa3kyK1Ara2c0SGI2VVVsQ0lUNkYzOWd2Wm1yVkdoUWUrcVZ1V3dFYWl1NGRsTzVMT3NEQy90L3B5dEs0UGNTdlFiT1Y2Z0g3cDNRbkZ6Z25ENjdubmRtSjNtanBMOWN5TVpydHpydTYvdzZaZkkya1kraTI2UXdvZkxGT2JhcGpHa0NZbDdpcXJvU3J4YVpVYkxXd1RFaHl2L3VQZmgwajhJTVA4Qi91NzlENmpyZmVNS1pXNWtjM1J5WldGdENtVnVaRzlpYWdveElEQWdiMkpxUER3dlEyOXVkR1Z1ZEhNZ05DQXdJRkl2Vkhsd1pTOVFZV2RsTDFKbGMyOTFjbU5sY3p3OEwxQnliMk5UWlhRZ1d5OVFSRVlnTDFSbGVIUWdMMGx0WVdkbFFpQXZTVzFoWjJWRElDOUpiV0ZuWlVsZEwwWnZiblE4UEM5R01TQXlJREFnVWk5R01pQXpJREFnVWo0K1BqNHZVR0Z5Wlc1MElEVWdNQ0JTTDAxbFpHbGhRbTk0V3pBZ01DQTJNVElnTnpreVhUNCtDbVZ1Wkc5aWFnbzJJREFnYjJKcUlEdzhMMFpwYkhSbGNpOUdiR0YwWlVSbFkyOWtaUzlNWlc1bmRHZ2dPVEkrUG5OMGNtVmhiUXA0bkN2a2NncmhNbEF3dHpSU0NFbmhjZzNoQ3VRcTVETFFNekEyTjFVbzV6SlM4QUpLWm5FWkdpajRja1hIR2lpa2NCbWJLWmlibWlua2NwbWFRMWc1WUpheG1aNkJDWkJ0ak1LRXlHZHdoWFBsZ1EwTzVBSUErUm9XVlFwbGJtUnpkSEpsWVcwS1pXNWtiMkpxQ2pjZ01DQnZZbW84UEM5RGIyNTBaVzUwY3lBMklEQWdVaTlVZVhCbEwxQmhaMlV2VW1WemIzVnlZMlZ6UER3dlVISnZZMU5sZENCYkwxQkVSaUF2VkdWNGRDQXZTVzFoWjJWQ0lDOUpiV0ZuWlVNZ0wwbHRZV2RsU1YwK1BpOVFZWEpsYm5RZ05TQXdJRkl2VFdWa2FXRkNiM2hiTUNBd0lEWXhNaUEzT1RKZFBqNEtaVzVrYjJKcUNqTWdNQ0J2WW1vOFBDOVRkV0owZVhCbEwxUjVjR1V4TDFSNWNHVXZSbTl1ZEM5Q1lYTmxSbTl1ZEM5SVpXeDJaWFJwWTJFdlJXNWpiMlJwYm1jdlYybHVRVzV6YVVWdVkyOWthVzVuUGo0S1pXNWtiMkpxQ2pJZ01DQnZZbW84UEM5VGRXSjBlWEJsTDFSNWNHVXhMMVI1Y0dVdlJtOXVkQzlDWVhObFJtOXVkQzlJWld4MlpYUnBZMkV0UW05c1pDOUZibU52WkdsdVp5OVhhVzVCYm5OcFJXNWpiMlJwYm1jK1BncGxibVJ2WW1vS05TQXdJRzlpYWp3OEwwdHBaSE5iTVNBd0lGSWdOeUF3SUZKZEwxUjVjR1V2VUdGblpYTXZRMjkxYm5RZ01qNCtDbVZ1Wkc5aWFnbzRJREFnYjJKcVBEd3ZWSGx3WlM5RFlYUmhiRzluTDFCaFoyVnpJRFVnTUNCU1BqNEtaVzVrYjJKcUNqa2dNQ0J2WW1vOFBDOU5iMlJFWVhSbEtFUTZNakF4TnpFd01URXlNREEyTWpCYUtTOURjbVZoZEdsdmJrUmhkR1VvUkRveU1ERTNNVEF4TVRJd01EWXlNRm9wTDFCeWIyUjFZMlZ5S0dsVVpYaDBJREl1TUM0NElGd29ZbmtnYkc5M1lXZHBaUzVqYjIxY0tTaytQZ3BsYm1Sdlltb0tlSEpsWmdvd0lERXdDakF3TURBd01EQXdNREFnTmpVMU16VWdaaUFLTURBd01EQXdNVEk0TmlBd01EQXdNQ0J1SUFvd01EQXdNREF4T0RNMElEQXdNREF3SUc0Z0NqQXdNREF3TURFM05EY2dNREF3TURBZ2JpQUtNREF3TURBd01EQXhOU0F3TURBd01DQnVJQW93TURBd01EQXhPVEkySURBd01EQXdJRzRnQ2pBd01EQXdNREUwTlRFZ01EQXdNREFnYmlBS01EQXdNREF3TVRZd09TQXdNREF3TUNCdUlBb3dNREF3TURBeE9UZ3lJREF3TURBd0lHNGdDakF3TURBd01ESXdNallnTURBd01EQWdiaUFLZEhKaGFXeGxjZ284UEM5SmJtWnZJRGtnTUNCU0wwbEVJRnM4WWpjellUWXhaakkyWm1Vd05UTXdZV0V5T1RjMU9XUXhNR1JpWkRFM09EaytQRGczWVRnM05ERTVaVFUzTTJJNVpHTTVaakF4WkRaa1lUQTJNamt5T1dKbVBsMHZVbTl2ZENBNElEQWdVaTlUYVhwbElERXdQajRLYzNSaGNuUjRjbVZtQ2pJeE5EVUtKU1ZGVDBZSw';
        rentalObj.strCRFAttachmentName = 'CRF Name';
        rentalObj.strSRType = 'RentalPool';
        rentalObj.getName();
        rentalObj.strSelectedUnit = listCreateBookingUnit[0].Id;
        rentalObj.getDetails();
        rentalObj.generateSoA();
        rentalObj.generateFMSoA();
        rentalObj.getCRFdocument();
        //rentalObj.proceedCase();
        rentalObj.submitSR();
        Test.stopTest();
    }

    @isTest static void testRentalPoolWithCase() {
        init();
        objPP = new Payment_Plan__c();
        objPP.Booking_Unit__c = listCreateBookingUnit[0].Id;
        objPP.Status__c = 'Active';
        insert objPP;

        Case objCase = TestDataFactory_CRM.createCase(null, recTypeRPAgreement);
        objCase.AccountId = objAcc.Id;
        objCase.Booking_Unit__c = listCreateBookingUnit[0].Id;
        insert objCase;

        SR_Attachments__c doc1 = new SR_Attachments__c();
        doc1.Type__c = 'Power Of Attorney';
        doc1.Case__c = objCase.Id;
        insert doc1;

        SR_Attachments__c doc2 = new SR_Attachments__c();
        doc2.Type__c = 'CRF Form';
        doc2.Case__c = objCase.Id;
        insert doc2;

        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(1));
        RentalPoolAgreementCREController_CC rentalObj = new RentalPoolAgreementCREController_CC();
        rentalObj.strCaseId = objCase.Id;
        rentalObj.strAccountID = objAcc.Id;
        rentalObj.strSRType = 'RentalPool';
        rentalObj.getName();
        rentalObj.strSelectedUnit = listCreateBookingUnit[0].Id;
        rentalObj.SRAttachmentToRemove = doc1.Id;
        rentalObj.removeAttachment();
        rentalObj.SRAttachmentToRemove = doc2.Id;
        rentalObj.removeAttachment();
        rentalObj.generateDateFromString('15/02/2018');
        Test.stopTest();
    }
}