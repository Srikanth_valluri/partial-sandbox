/*
Class to do callout and get the SMS Job Status and update it on the Respective Log Object
Developed By: DAMAC IT TEMA
*/

global class Async_Rest_SMS_Job_Status implements Queueable, Database.AllowsCallouts {

    public string smsbulkid; 
    
    public boolean pollAgain;
    
    global Async_Rest_SMS_Job_Status(string bulkId){
        smsbulkid = bulkId;
        system.debug(smsbulkid);
        system.debug(bulkId);
               
    }
    public void execute(QueueableContext context) {
        checkStatusbyBulkId(smsbulkid);
        system.debug(smsbulkid);
    }
    
    // Do Callout by passing the Bulk Id
    public void checkStatusbyBulkId(string bulkId){
        Secure_SMS__mdt SMS_BULKSMS_STATUS = [select id,User_Name__c,password__c,Endpoint__c from Secure_SMS__mdt where DeveloperName='SMS_BULKSMS_STATUS' LIMIT 1];
        system.debug(bulkId);
        string endpoint = SMS_BULKSMS_STATUS.Endpoint__c+'username='+SMS_BULKSMS_STATUS.User_Name__c+'&password='+SMS_BULKSMS_STATUS.password__c+'&bulkid='+bulkId; //'http://ep.securebulksms.com/Developer/api/BulkSMS/get?username=salesforce&password=F@c3b00k@123&bulkid='+bulkId;
        // Call out.
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);                
        req.setMethod('GET');
        req.setheader('Content-Type','application/json');         
        HttpResponse res = h.send(req);
        system.debug(res.getBody()); 
        if(res.getstatuscode() == 200){
            string resp = res.getBody();
            update_SF(bulkId,resp); // Parse and Update response in SF
        }
        else if(res.getstatuscode() != 200){
            Campaign_SMS__c log = [select id,SMS_Response__c from Campaign_SMS__c where SMS_Bulk_Id__c=:bulkId LIMIT 1];
            log.SMS_Response__c = log.SMS_Response__c+res.getBody();
            update log;
        }        
    }
    
    // Parse and Update Response parms in salesforce
    public void update_SF(string bulkId,string resp){
        DAMAC_SMS_Job_Status_Resp respCls = new DAMAC_SMS_Job_Status_Resp();
        respCls = DAMAC_SMS_Job_Status_Resp.parse(resp);
        system.debug(respCls.Data.BulkStatus);
        if(respCls.Data.BulkStatus == 'Error'){
            Campaign_SMS__c log = [select id,Error_From_Provider__c,SMS_Queued__c ,SMS_Response__c,Total_Mobile_Numbers__c,Total_Numbers_Processed__c,SMS_Process_Complete__c,Total_Valid_Numbers__c   from Campaign_SMS__c where SMS_Bulk_Id__c=:bulkId LIMIT 1];
            log.SMS_Response__c = 'Status of the SMS is - Error';
            log.Total_Mobile_Numbers__c = respCls.Data.TotalNumbers;
            log.Total_Numbers_Processed__c = respCls.Data.TotalProcessed;
            log.Total_Valid_Numbers__c =respCls.Data.TotalValid;
            log.SMS_Process_Complete__c = false;
            log.Error_From_Provider__c = true;
            log.SMS_Queued__c = false;
            update log;
            
        }else if(respCls.Data.BulkStatus != 'Finished' && !test.isrunningtest()){ //Poll again if the Job is still Porcessing
            pollAgain = true;            
            System.enqueueJob(new Async_Rest_SMS_Job_Status(bulkid));
        }else if(respCls.Data.BulkStatus == 'Finished'){
            Campaign_SMS__c log = [select id,Error_From_Provider__c,SMS_Queued__c ,SMS_Response__c,Total_Mobile_Numbers__c,Total_Numbers_Processed__c,SMS_Process_Complete__c,Total_Valid_Numbers__c   from Campaign_SMS__c where SMS_Bulk_Id__c=:bulkId LIMIT 1];
            log.SMS_Response__c = 'Status of the SMS is - Tota lNumbers: '+respCls.Data.TotalNumbers+' Total Processed: '+respCls.Data.TotalProcessed+' Total Valid: '+respCls.Data.TotalValid;
            log.Total_Mobile_Numbers__c = respCls.Data.TotalNumbers;
            log.Total_Numbers_Processed__c = respCls.Data.TotalProcessed;
            log.Total_Valid_Numbers__c =respCls.Data.TotalValid;
            log.SMS_Process_Complete__c =true;
            update log;
        }
    
    }

}