public class Damac_ZiwoLoginResp{
    public boolean result;
    public cls_content content;
    public cls_info info;
    public class cls_content {
        public Integer id;  //6
        public String firstName;    //tojo
        public String lastName; //antony
        public String username; //tojo.antony@damacgroup.com
        public String status;   //active
        public String type; //admin
        public cls_photo photo;
        public Integer roleId;  //2
        public String lastLoginAt;  //2020-02-11T11:01:41.127Z
        public cls_contactNumber contactNumber;
        public String createdAt;    //2020-01-28T06:25:53.786Z
        public String updatedAt;    //2020-02-11T11:01:41.127Z
        public cls_role role;
        public cls_roles[] roles;
        public String access_token; //e1ee6b50-4cbd-11ea-a9da-8ba1001b5198
        public String profileType;  //users
    }
    public class cls_photo {
    }
    public class cls_contactNumber {
    }
    public class cls_role {
        public Integer id;  //2
        public String name; //admin
        public cls_permissions permissions;
    }
    public class cls_permissions {
        public cls_live live;
        public cls_audit audit;
        public cls_roles roles;
        public cls_users users;
        public cls_zdial zdial;
        public cls_agents agents;
        public cls_queues queues;
        public cls_skills skills;
        public cls_apiKeys apiKeys;
        public cls_billing billing;
        public cls_numbers numbers;
        public cls_gateways gateways;
        public cls_settings settings;
        public cls_uploader uploader;
        public cls_dashboard dashboard;
        public cls_eavesdrop eavesdrop;
        public cls_ivreditor ivreditor;
        public cls_positions positions;
        public cls_blacklists blacklists;
        public cls_recordings recordings;
        public cls_statistics statistics;
        public cls_callHistory callHistory;
        public cls_agentReports agentReports;
        public cls_callTrackers callTrackers;
        public cls_dialplanEditor dialplanEditor;
    }
    public class cls_live {
    }
    public class cls_audit {
        
    }
    public class cls_roles {
        
    }
    public class cls_users {
        
    }
    public class cls_zdial {
       
    }
    public class cls_agents {
        
    }
    public class cls_queues {
        
    }
    public class cls_skills {
        
    }
    public class cls_apiKeys {
        
    }
    public class cls_billing {
       
    }
    public class cls_numbers {
       
    }
    public class cls_gateways {
       
    }
    public class cls_settings {
        
    }
    public class cls_uploader {
       
    }
    public class cls_dashboard {
    
    }
    public class cls_eavesdrop {
        public boolean listen;
        public boolean whisperBoth;
        public boolean whisperAgent;
    }
    public class cls_ivreditor {
        
    }
    public class cls_positions {
     
    }
    public class cls_blacklists {
     
    }
    public class cls_recordings {
     
    }
    public class cls_statistics {
     
    }
    public class cls_callHistory {
     
    }
    public class cls_agentReports {
    
    }
    public class cls_callTrackers {
      
    }
    public class cls_dialplanEditor {
       
    }
    public class cls_info {
    }
    public static Damac_ZiwoLoginResp parse(String json){
        return (Damac_ZiwoLoginResp) System.JSON.deserialize(json, Damac_ZiwoLoginResp.class);
    }

    
}