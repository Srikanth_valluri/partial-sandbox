/*
    Test class name :Damac_TourGuideControllerTest
    Functionality : to fill the Tour guide details all booking units whose registration date is less than 7 days and belongs to the specified Nationality
*/

public with sharing class Damac_TourGuideController {

    public Booking_Unit__c unit { get; set; }
    public String message { get; set; }
    public String themeType { get; set; }
    public boolean isError { get; set; }
    public String srId { get; set; }
    public Boolean lockFields { get; set; }
    
    public Damac_TourGuideController () {
        unit = new Booking_Unit__c ();
        message = '';
        isError = false;
        themeType = '';
        lockFields = false;
        srId = '';
        srId = apexpages.currentpage().getparameters().get('id');
        List <Buyer__c> buyer = new List <Buyer__c>();
        // To get the buyers based on china or Hing kong or macao nationality 
        try {
            buyer = [SELECT Nationality__c FROM Buyer__c WHERE Booking__r.Deal_SR__c =: srId
                                    AND Primary_Buyer__c = TRUE
                                    AND (Phone_Country_Code__c = 'China: 0086' OR Phone_Country_Code__c = 'Hong Kong: 00852'
                                        OR Phone_Country_Code__c = 'Macao: 00853')];
        } catch (Exception e) {
            isError = true;
            themeType = 'slds-theme_error';
            message = Label.China_No_Buyers;
        }
        if (buyer.size () == 0) {
            isError = true;
            themeType = 'slds-theme_error';
            message = Label.China_No_Buyers;
        }
        if (!isError) {
            // Getting booking units whose registration age is less than 7 days.
            for (Booking_Unit__c bu:[SELECT Registration_Age__c, Tour_Guide_Name__c, Tour_Guide_Email__c, 
                                            Tour_Guide_Mobile_Phone_No__c, Tour_Guide_Passport_Number__c,
                                            Tour_Guide_Mobile_Country_Code__c
                                            FROM Booking_Unit__c
                                            WHERE Booking__r.Deal_SR__c =: srId])
            {
                System.Debug (bu.Registration_Age__c);
                if (bu.Registration_Age__c > 7) {
                    isError = true;
                    themeType = 'slds-theme_error';
                    message = Label.China_Tour_Guide_Validatity;
                    break;
                } else {
                    // If tour guide details already filled then making the fields readonly
                    unit.Tour_Guide_Name__c = bu.Tour_Guide_Name__c;
                    unit.Tour_Guide_Email__c = bu.Tour_Guide_Email__c;
                    unit.Tour_Guide_Mobile_Phone_No__c = bu.Tour_Guide_Mobile_Phone_No__c;
                    unit.Tour_Guide_Passport_Number__c = bu.Tour_Guide_Passport_Number__c;
                    unit.Tour_Guide_Mobile_Country_Code__c = bu.Tour_Guide_Mobile_Country_Code__c;
                    if (unit.Tour_Guide_Name__c != NULL 
                        && unit.Tour_Guide_Email__c != NULL
                        && unit.Tour_Guide_Mobile_Phone_No__c != NULL 
                        && unit.Tour_Guide_Passport_Number__c != NULL
                        && unit.Tour_Guide_Mobile_Country_Code__c != NULL)
                        lockFields = true;
                }
            }        
        }
    } 
    // To save the Tour guide details on booking units related to the SR
    public void save () {
        message = '';
        isError = false;
            
        try {
            ID srId = apexpages.currentpage().getparameters().get('id');
            List <Booking_unit__c> buList = new List <Booking_Unit__c> ();
            
            if (unit.Tour_Guide_Name__c != NULL 
                && unit.Tour_Guide_Email__c != NULL
                && unit.Tour_Guide_Mobile_Phone_No__c != NULL 
                && unit.Tour_Guide_Passport_Number__c != NULL
                && unit.Tour_Guide_Mobile_Country_Code__c != NULL) {
                for (Booking_Unit__c bu:[SELECT Tour_Guide_Name__c, Tour_Guide_Email__c, Tour_Guide_Mobile_Country_Code__c,
                                                Tour_Guide_Mobile_Phone_No__c, Tour_Guide_Passport_Number__c
                                                FROM Booking_Unit__c
                                                WHERE Booking__r.Deal_SR__c =: srId])
                {
                    bu.Tour_Guide_Name__c = unit.Tour_Guide_Name__c;
                    bu.Tour_Guide_Email__c = unit.Tour_Guide_Email__c;
                    bu.Tour_Guide_Mobile_Phone_No__c = unit.Tour_Guide_Mobile_Phone_No__c;
                    bu.Tour_Guide_Passport_Number__c = unit.Tour_Guide_Passport_Number__c;
                    bu.Tour_Guide_Mobile_Country_Code__c = unit.Tour_Guide_Mobile_Country_Code__c;
                    buList.add (bu);
                }
                update buList;
                isError = false;
                message = Label.China_Tour_Guide_Added;
                themeType = 'slds-theme_success';
                lockFields = true;
            } else {
                themeType = 'slds-theme_error';
                isError = false;
                message = 'Required Fields are missing.';
            }
        } Catch (Exception e) {
            isError = false;
            themeType = 'slds-theme_error';
            message = e.getMessage ()+' '+e.getLineNumber ();
        }
        
    }   
}