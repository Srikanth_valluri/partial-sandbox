/*
* SR -> All Bookings -> All Booking Units -> if(DP_OK__c) is checked then on SR -> DP_OK__c should be checked.
*
* VERSION     AUTHOR        DATE            COMMENTS        
*
* V 2.0       07/07/2017    rahul-godara        PopulatevaluesInBookingUnit() for populate hos,dos,hod in booking unit  
* V 2.1       18/7/2016     rahul-godara        Populating Rejection Status from SR in PopulatevaluesInBookingUnit()
* V 2.2      07/05/2020    QBurst              Commented out the code for updating the Token Deposit Paid field
**************************************************************************************************/
public class BookingUnitTriggerHandler {
    public static Map<Id,Boolean> mpSRRegStatusCodeLE;
    public static Map<Id,Boolean> mpSRRegStatusCodeAR;
    
    private static User getUserDetails(){
        User currentUser = [SELECT Id, HR_Employee_ID__c, IPMS_Employee_ID__c, HOS_Name__c, Sales_Office__c, ManagerId, Manager.Name, Manager.HR_Employee_ID__c, 
        Manager.IPMS_Employee_ID__c FROM User WHERE Id=: UserInfo.getUserId() LIMIT 1];
        return currentUser; 
    }
    
    public static void updateCampaignTotal(Map<Id, Booking_Unit__c> newBookingUnit){
        Map<Id, Decimal> campaignTotalMap = new Map<Id, Decimal>();
        Set<Id> campaignIds = new Set<Id>();
        List<Booking_Unit__c> bookingUnitsList = new List<Booking_Unit__c>();
        List<Campaign__c> updateCampaignList = new List<Campaign__c>();
        Map<Id, List<Booking_Unit__c>> campaignBookingMap = new Map<Id, List<Booking_Unit__c>>();
        
        system.debug('$$$ updateCampaignTotal()$$$'+newBookingUnit);
        try{
            for(Booking_Unit__c thisBookingUnit : newBookingUnit.values()){
                if(String.isNotBlank(thisBookingUnit.Status__c) && !thisBookingUnit.Status__c.equalsIgnoreCase('Removed') && 
                   String.isNotBlank(thisBookingUnit.Related_Campaign__c )){ campaignIds.add(thisBookingUnit.Related_Campaign__c);
                }
            }
            
            for(Booking_Unit__c thisBookingUnit : [SELECT Id, Related_Campaign__c, Requested_Price_AED__c, Status__c FROM Booking_Unit__c WHERE Related_Campaign__c IN :campaignIds]){
                if(campaignBookingMap.containsKey(thisBookingUnit.Related_Campaign__c)){
                    campaignBookingMap.get(thisBookingUnit.Related_Campaign__c).add(thisBookingUnit);       
                }else{
                    campaignBookingMap.put(thisBookingUnit.Related_Campaign__c, new List<Booking_Unit__c>{ thisBookingUnit });  
                }   
            }
            
            Decimal totalSum;
            if(!campaignBookingMap.isEmpty()){
                for(Id campaignId : campaignBookingMap.keySet()){
                    Campaign__c thisCampaign = new Campaign__c();
                    thisCampaign.Id = campaignId;
                    totalSum = 0.00;
                    for(Booking_Unit__c thisBookingUnit: campaignBookingMap.get(campaignId)){
                        totalSum += thisBookingUnit.Requested_Price_AED__c;
                    }
                    system.debug(totalSum);
                    thisCampaign.Campaign_Sales__c = totalSum;
                    thisCampaign.Total_Sales__c = totalSum;
                    updateCampaignList.add(thisCampaign);
                }   
            }
    
            if(!updateCampaignList.isEmpty()){
                update updateCampaignList;  
            }
        }catch(Exception ex){
            System.debug('Exception at line number = '+ex.getLineNumber()+', Exception message = '+ex.getMessage());
        }
    } 
    
    public static void updateParentCampaignSales(Map<Id, Booking_Unit__c> newRecordsMap){
        set<Id> campaignIds = new Set<id>();
        Map<Id, List<Campaign__c>> childCampaignMap = new Map<Id, List<Campaign__c>>();
        Map<Id, Decimal> campaignUnitsNumberMap = new Map<Id, Decimal>();
        List<Campaign__c> campaignList = new List<Campaign__c>();
        List<Campaign__c> totalLeadsUpdateList = new List<Campaign__c>();
        
        try{
            for(Campaign__c thisCamp : [SELECT Id, Total_No_Of_Leads__c, Campaign_Sales__c FROM Campaign__c]){
                campaignIds.add(thisCamp.Id);
            }
            system.debug(campaignIds);
            if(!campaignIds.isEmpty()){
                for(Campaign__c thisCampaign : [SELECT Id, Name, Parent_Campaign__c, Campaign_Sales__c,
                                                Parent_Campaign__r.Parent_Campaign__c, 
                                                Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c,
                                                Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c, 
                                                Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c
                                                FROM Campaign__c WHERE Parent_Campaign__c IN:campaignIds 
                                                OR Parent_Campaign__r.Parent_Campaign__c IN: campaignIds 
                                                OR Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c IN: campaignIds
                                                   OR Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c IN: campaignIds
                                                OR Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c IN: campaignIds])
                {
                    if(thisCampaign.Parent_Campaign__c != null && childCampaignMap.containsKey(thisCampaign.Parent_Campaign__c)){
                        childCampaignMap.get(thisCampaign.Parent_Campaign__c).add(thisCampaign);
                    }else if(thisCampaign.Parent_Campaign__c != null){
                        childCampaignMap.put(thisCampaign.Parent_Campaign__c, new List<Campaign__c>{thisCampaign});
                    }
                    if(thisCampaign.Parent_Campaign__r.Parent_Campaign__c != null && childCampaignMap.containsKey(thisCampaign.Parent_Campaign__r.Parent_Campaign__c)){
                        childCampaignMap.get(thisCampaign.Parent_Campaign__r.Parent_Campaign__c).add(thisCampaign);
                    }else if(thisCampaign.Parent_Campaign__r.Parent_Campaign__c != null){
                        childCampaignMap.put(thisCampaign.Parent_Campaign__r.Parent_Campaign__c , new List<Campaign__c>{thisCampaign});
                    }
                    if(thisCampaign.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c != null && childCampaignMap.containsKey(thisCampaign.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c)){
                           childCampaignMap.get(thisCampaign.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c).add(thisCampaign);
                    }else if(thisCampaign.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c != null){
                        childCampaignMap.put(thisCampaign.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c, new List<Campaign__c>{thisCampaign});
                    }
                    if(thisCampaign.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c != null && childCampaignMap.containsKey(thisCampaign.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c)){
                        childCampaignMap.get(thisCampaign.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c).add(thisCampaign);
                    }else if(thisCampaign.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c != null){
                        childCampaignMap.put(thisCampaign.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c, new List<Campaign__c>{thisCampaign});
                    }
                    if(thisCampaign.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c != null && childCampaignMap.containsKey(thisCampaign.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c)){
                        childCampaignMap.get(thisCampaign.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c).add(thisCampaign);
                    }else if(thisCampaign.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c != null){
                        childCampaignMap.put(thisCampaign.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c, new List<Campaign__c>{thisCampaign});
                    }
                }
            }
            system.debug(childCampaignMap);
            if(!childCampaignMap.isEmpty()){
                for(Id campaignId : childCampaignMap.keySet()){
                    List<Campaign__c> childCampaignList = childCampaignMap.get(campaignId);
                    if(childCampaignList != null){
                        for(Campaign__c thisCampaign : childCampaignList){
                            if(thisCampaign.Campaign_Sales__c !=null){
                                if(campaignUnitsNumberMap.containsKey(campaignId)){
                                    campaignUnitsNumberMap.put(campaignId, campaignUnitsNumberMap.get(campaignId) + thisCampaign.Campaign_Sales__c);
                                }else{
                                    campaignUnitsNumberMap.put(campaignId, thisCampaign.Campaign_Sales__c);
                                }
                            }
                        }
                    }
                }
            }
            
            if(!campaignUnitsNumberMap.isEmpty()){
                for(Campaign__c thisCampaign : [SELECT Id, Name, Total_Sales__c, Campaign_Sales__c FROM Campaign__c WHERE Id IN: campaignUnitsNumberMap.keyset()]){
                    thisCampaign.Total_Sales__c = campaignUnitsNumberMap.get(thisCampaign.Id);
                    if(thisCampaign.Campaign_Sales__c != null){
                        thisCampaign.Total_Sales__c += thisCampaign.Campaign_Sales__c;
                    }
                    campaignList.add(thisCampaign);
                }
            }
            system.debug(campaignList);
            if(!campaignList.isEmpty()){
                update campaignList;
            }
        }catch(Exception ex){
            system.debug('Exception at line number = '+ex.getLineNumber()+', Exception message = '+ex.getMessage());
        }
    }
    
    public static void updateBookingUnitType(Map<Id, Booking_Unit__c> newBookingUnitMap, Map<Id, Booking_Unit__c> oldBookingUnitMap){
         
        List<Booking_Unit__c> updateBookingUnitList = new List<Booking_Unit__c>();
        try{
            if((Trigger.isUpdate || Trigger.isInsert) && newBookingUnitMap!=null && !newBookingUnitMap.isEmpty()){
                system.debug('inside the if');
                for(Booking_Unit__c thisBookingUnit : newBookingUnitMap.values()){
                    if(String.isNotBlank(thisBookingUnit.Primary_Buyer_Country__c) && String.isNotBlank(thisBookingUnit.Property_Country__c)){
                        if(thisBookingUnit.Primary_Buyer_Country__c.equalsIgnoreCase(thisBookingUnit.Property_Country__c)){ 
                            thisBookingUnit.Booking_Unit_Type__c = 'Local';
                        }else{ thisBookingUnit.Booking_Unit_Type__c = 'International';
                        }
                        updateBookingUnitList.add(thisBookingUnit);
                    }
                }
            }
        }catch(Exception ex){
            System.debug('Exception at line number = '+ex.getLineNumber()+', Exception message = '+ex.getMessage());
        }
    }
    
    public static void updateBookingUnits(List<Booking_Unit__c> bookingUnitList){
        
        List<String> bookingIdsList = new List<String>();
        User currentUser = getUserDetails();
        for(Booking_Unit__c BU: bookingUnitList){
            BU.HR_Employee_ID__c = currentUser.HR_Employee_ID__c;
            BU.IPMS_Employee_ID__c = currentUser.IPMS_Employee_ID__c;
            BU.HOS_Name__c = currentUser.HOS_Name__c;
            BU.Sales_Office__c = currentUser.Sales_Office__c;
            if(currentUser.ManagerId != null){
                BU.Manager_Name__c = currentUser.Manager.Name;
                BU.Manager_IPMS_Employee_ID__c = currentUser.Manager.IPMS_Employee_ID__c;
                BU.Manager_HR_Employee_ID__c = currentUser.Manager.HR_Employee_ID__c;
            }
            if (BU.Booking__c != null) {
                bookingIdsList.add(BU.Booking__c);
            }
        }
        //List<Buyer__c> buyersList
        Map<Id, String> bookingBuyerInfoMap = new Map<Id, String>();
        for(Buyer__c buyerObj : [ SELECT Id, Primary_Buyer__c, Inquiry__r.Fly_in_TSA__c,
                                        Inquiry__r.Fly_in_TSA__r.Name,
                                        Inquiry__r.Stand_Manager__c,Booking__c
                                    FROM Buyer__c 
                                   WHERE Booking__c IN :bookingIdsList
                                     AND  Primary_Buyer__c = true
                                     AND Inquiry__c != null
        ]) {
            String flyinTSA = buyerObj.Inquiry__r.Fly_in_TSA__c;
            String promoterManager = buyerObj.Inquiry__r.Stand_Manager__c;
            String flyinTSAName = buyerObj.Inquiry__r.Fly_in_TSA__r.Name;
            bookingBuyerInfoMap.put(buyerObj.Booking__c, (String.isNotBlank(flyinTSA) ? flyinTSA : ' ') + '-' + (String.isNotBlank(promoterManager) ? promoterManager : ' ') + '-' + (String.isNotBlank(flyinTSAName) ? flyinTSAName : ' '));
        }
        System.debug('>>>bookingBuyerInfoMap'+bookingBuyerInfoMap);
        
        for (Booking_Unit__c buObj : bookingUnitList) { 
            if (buObj.Booking__c != null) {
                if (bookingBuyerInfoMap.containskey(buObj.Booking__c) 
                    && bookingBuyerInfoMap.get(buObj.Booking__c) != null
                ) {
                    List<String> infoList = bookingBuyerInfoMap.get(buObj.Booking__c).split('-');
                    System.debug('>>>infoList'+infoList);
                    System.debug('>>>infoList'+infoList.size());
                    System.debug('>>>infoList0'+infoList[0]);
                    if (infoList != null) {
                        if(String.isNotBlank(infoList[0])){buObj.Flyin_TSA__c = infoList[0];
                        }
                        if(String.isNotBlank(infoList[1])){buObj.Stand_Manager__c = infoList[1];
                        }
                        if(String.isNotBlank(infoList[2])){buObj.Flyin_TSA_Name__c = infoList[2];
                        }
                        
                    }
                    
                }
            }
        }
    }


    public static void UpdateSR(List<ID> lstBUIDs){
        //Get all recent Update/Inserted BU
        Set<Id> SRIds = new Set<Id>();
        for(Booking_Unit__c BU : [select id,name,Booking__c,DP_OK__c,Booking__r.Deal_SR__c from Booking_Unit__c where id in : lstBUIDs and Status__c !='Removed'])
        {
            if(BU.Booking__c != null && BU.Booking__r.Deal_SR__c != null){
                SRIds.add(BU.Booking__r.Deal_SR__c);
            }
        }
        //Prepare map of SR ID with its respective Bookings
        Map<Id,List<Booking__c>> mpSRIDBookings = new Map<Id,List<Booking__c>>();
        for(Booking__c b : [select id,(select id,DP_OK__c,name,Token_Paid__c,Registration_Status_Code__c from Booking_Units__r where Status__c !='Removed'),Deal_SR__c from Booking__c where Deal_SR__c in : SRIds])
        {
            if(mpSRIDBookings.containsKey(b.Deal_SR__c)){
                List<Booking__c> tmp = mpSRIDBookings.get(b.Deal_SR__c);
                tmp.add(b);
                mpSRIDBookings.put(b.Deal_SR__c,tmp);
            }
            else{
                List<Booking__c> tmp = new List<Booking__c>();
                tmp.add(b);
                mpSRIDBookings.put(b.Deal_SR__c,tmp);
            }
        }
        List<NSIBPM__Service_Request__c> lstSRtoUpdate = new List<NSIBPM__Service_Request__c>();
        Map<Id,Boolean> mpSRVal = new Map<Id,Boolean>();
        Map<Id,Boolean> mpSRValTokenPaid = new Map<Id,Boolean>();
        mpSRRegStatusCodeLE = new Map<Id,Boolean>();
        mpSRRegStatusCodeAR = new Map<Id,Boolean>();
        //get all SR's for which the bookings units are updated
        for(id srID : mpSRIDBookings.keyset()){
            mpSRVal.put(srID,true);
            mpSRValTokenPaid.put(srID,true);
            mpSRRegStatusCodeLE.put(srID,true);
            mpSRRegStatusCodeAR.put(srID,true);
            //For each sr get all bookings.
            for(Booking__c b : mpSRIDBookings.get(srID))
            {
                //for each booking get all booking units.
                for(Booking_Unit__c bu: b.Booking_Units__r){
                    if(!bu.DP_OK__c){
                        mpSRVal.put(srID,false);
                    }
                    if(!bu.Token_Paid__c){
                        mpSRValTokenPaid.put(srID,false);
                    }
                    if(bu.Registration_Status_Code__c != 'LE'){
                        mpSRRegStatusCodeLE.put(srID,false);
                    }
                    if(bu.Registration_Status_Code__c != 'AR'){
                        mpSRRegStatusCodeAR.put(srID,false);
                    }
                }
            }
        }
        //Update SR's with its field.
        if(mpSRVal != null && !mpSRVal.isempty()){
            List<NSIBPM__Service_Request__c> lstSRs = [select id,name,DP_ok__c,Token_Deposit_Paid_Date_Time__c from NSIBPM__Service_Request__c where id in : mpSRVal.keyset()];
            for(NSIBPM__Service_Request__c sr : lstSRs){
                boolean ischange = false;
                NSIBPM__Service_Request__c objsr = new NSIBPM__Service_Request__c();
                if(sr.DP_ok__c != mpSRVal.get(sr.id)){
                    objsr.DP_ok__c = mpSRVal.get(sr.id);
                    ischange=true;
                }
                if(sr.Token_Deposit_Paid_Date_Time__c == null && mpSRValTokenPaid.get(sr.id)){
                   // objsr.Token_Deposit_Paid_Date_Time__c= mpSRValTokenPaid.get(sr.id) ? system.now(): null; // 2.2 commented out
                    //ischange=true; // 2.2
                }
                if(ischange){
                    objsr.id = sr.id;
                    lstSRtoUpdate.add(objsr);
                }                               
            }
            update lstSRtoUpdate;
            UpdateStepStatus(mpSRVal.keyset());
        }else if(mpSRRegStatusCodeLE != null && !mpSRRegStatusCodeLE.isempty()){
            updatestepstatus(mpSRRegStatusCodeLE.keyset());
        }
    }
    
    
    Public static void UpdateStepStatus(set<Id> stSRIds){
        //Update any open steps related to DP OK
        List<String> lststepandStpStatusConstants = String.valueof(Label.MapSteptoStepStatus).split(',');
        Map<string,string> mpStepCodeStatusCode = new Map<string,string>();
        
        for(integer i =1;i<= lststepandStpStatusConstants.size()/2 ;i++){
            integer indx = (i*2)-1;
            mpStepCodeStatusCode.put(string.valueof(lststepandStpStatusConstants[indx-1]),string.valueof(lststepandStpStatusConstants[indx]));
                
        } 
        Map<string,Id> mpstatus = new Map<string,Id>();
        Map<id,string> srIdStatusUpdate = new Map<id,string>();
        for(NSIBPM__Status__c stpStatus : [select id,name,NSIBPM__Code__c from NSIBPM__Status__c where NSIBPM__Code__c in : mpStepCodeStatusCode.values()]){
            mpstatus.put(stpStatus.NSIBPM__Code__c,stpStatus.id);
        }
        system.debug('---mpstatus-->'+mpstatus);
       /* List<NSIBPM__Step__c> lststepstoUpdate = new List<NSIBPM__Step__c>();
        for(NSIBPM__Step__c stp:[SELECT Id, Name,NSIBPM__Status__c,NSIBPM__SR__r.DP_ok__c,NSIBPM__Step_Template__r.NSIBPM__Code__c,NSIBPM__SR__r.id,
                                 NSIBPM__SR__r.Token_Deposit_Paid_Date_Time__c FROM NSIBPM__Step__c where 
                                 NSIBPM__SR__c != null and (NSIBPM__SR__c in : stSRIds) and NSIBPM__SR__r.NSIBPM__SR_Template__c!=null and IsDeleted=false
                                 and NSIBPM__Step_Template__r.NSIBPM__Code__c != null and NSIBPM__Step_Template__r.NSIBPM__Code__c in : mpStepCodeStatusCode.keyset()]){
                                     string statuscode = (mpStepCodeStatusCode.containskey(stp.NSIBPM__Step_Template__r.NSIBPM__Code__c)) ? mpStepCodeStatusCode.get(stp.NSIBPM__Step_Template__r.NSIBPM__Code__c) : '';
                                     id statusID = (statuscode != '' && mpstatus.containskey(statuscode)) ? mpstatus.get(statuscode) : null;
                                     system.debug('--->'+stp.NSIBPM__Step_Template__r.NSIBPM__Code__c+' -- '+statuscode+' -- '+statusID );
                                     if(statusID != null){
                                         if(stp.NSIBPM__SR__r.DP_ok__c == true && stp.NSIBPM__Status__c != statusID && stp.NSIBPM__Step_Template__r.NSIBPM__Code__c == 'DP_OK'){
                                             stp.NSIBPM__Status__c = statusID;
                                             lststepstoUpdate.add(stp);
                                         }
                                         else if(stp.NSIBPM__SR__r.Token_Deposit_Paid_Date_Time__c != null && stp.NSIBPM__Status__c != statusID && stp.NSIBPM__Step_Template__r.NSIBPM__Code__c == 'TOKEN_PAYMENT'){
                                             stp.NSIBPM__Status__c = statusID;
                                             lststepstoUpdate.add(stp);
                                             srIdStatusUpdate.put(stp.NSIBPM__SR__c,label.SR_Status_Token_Deposit_Paid);
                                         }
                                         else if(mpSRRegStatusCodeLE.get(stp.NSIBPM__SR__r.id) && stp.NSIBPM__Status__c != statusID && stp.NSIBPM__Step_Template__r.NSIBPM__Code__c == 'SPA_EXECUTION'){
                                             stp.NSIBPM__Status__c = statusID;
                                             lststepstoUpdate.add(stp);
                                             srIdStatusUpdate.put(stp.NSIBPM__SR__c,label.SPA_EXECUTED);
                                         }
                                     }
                                     system.debug('---lststepstoUpdate DP OK->'+lststepstoUpdate);
                                 }

        
        if(lststepstoUpdate != null && !lststepstoUpdate.isempty()){
            system.debug('---lststepstoUpdate size->'+lststepstoUpdate.size());
            update lststepstoUpdate;
            system.debug('---lststepstoUpdate->'+lststepstoUpdate);
        }*/


        List<New_Step__c> newStepsToUpdateList = new List<New_Step__c>();
        for(New_Step__c stp : [SELECT Id, Name, Step_Status__c, Step_Type__c, Service_Request__r.DP_ok__c, 
                               Service_Request__c, Service_Request__r.Token_Deposit_Paid_Date_Time__c 
                               FROM New_Step__c 
                               WHERE Service_Request__c != null 
                               AND Service_Request__c IN : stSRIds 
                               AND Step_Type__c IN ('DP_OK', 'Token Payment', 'SPA Execution')]){
            system.debug('--->'+stp.Step_Type__c+' -- '+stp.Step_Status__c+' -- ');

            if(stp.Service_Request__r.DP_ok__c && stp.Step_Status__c != 'DP OK' && stp.Step_Type__c == 'DP_OK'){
                stp.Step_Status__c = 'DP OK';
                stp.Is_Closed__c = true;
                newStepsToUpdateList.add(stp);
            }
            else if(stp.Service_Request__r.Token_Deposit_Paid_Date_Time__c != null && stp.Step_Status__c != 'Token Deposit Paid' && stp.Step_Type__c == 'Token Payment'){
                stp.Step_Status__c = 'Token Deposit Paid';
                stp.Is_Closed__c = true;
                newStepsToUpdateList.add(stp);
            }
            else if(mpSRRegStatusCodeLE.get(stp.Service_Request__c) && stp.Step_Status__c != 'SPA Executed' && stp.Step_Type__c == 'SPA Execution'){
                stp.Step_Status__c = 'SPA Executed';
                stp.Is_Closed__c = true;
                newStepsToUpdateList.add(stp);
            }
            system.debug('---lststepstoUpdate DP OK->'+newStepsToUpdateList);
        }

        if(newStepsToUpdateList != null && !newStepsToUpdateList.isEmpty()){
            system.debug('---lststepstoUpdate size->' + newStepsToUpdateList.size());
            update newStepsToUpdateList;
            system.debug('---lststepstoUpdate->' + newStepsToUpdateList);
        }

        
        set<Id> SRIdsStepstoCls = new set<Id>();
        for(id sridRej : stSRIds){
            if(mpSRRegStatusCodeAR.containsKey(sridRej) && mpSRRegStatusCodeAR.get(sridRej)){
                srIdStatusUpdate.put(sridRej,label.SR_Status_REJECTED);
                SRIdsStepstoCls.add(sridRej);
            }
        }
        
        if(srIdStatusUpdate != null && !srIdStatusUpdate.isempty()){
            List<NSIBPM__Service_Request__c> lstSRtoUpdate = new List<NSIBPM__Service_Request__c>();
            Map<string,Id> mpStatusCodeStatusid = new Map<string,id>();
            for(NSIBPM__SR_Status__c srSt : [select id,name,NSIBPM__Code__c from NSIBPM__SR_Status__c where NSIBPM__Code__c in : srIdStatusUpdate.values()]){
                mpStatusCodeStatusid.put(srst.NSIBPM__Code__c,srst.id);
            }
            system.debug('---mpStatusCodeStatusid->'+mpStatusCodeStatusid);
            for(id srid : srIdStatusUpdate.keyset()){
                if(srIdStatusUpdate.get(srid) != null && mpStatusCodeStatusid.containskey(srIdStatusUpdate.get(srid))){
                    id SRStatId = mpStatusCodeStatusid.get(srIdStatusUpdate.get(srid));
                    lstSRtoUpdate.add(new NSIBPM__Service_Request__c(id=srID,NSIBPM__Internal_SR_Status__c = SRStatId,NSIBPM__External_SR_Status__c = SRStatId));
                    system.debug('---lstSRtoUpdate SR->'+lstSRtoUpdate);
                }
            }
            if(null != lstSRtoUpdate && !lstSRtoUpdate.isempty()){
                update lstSRtoUpdate;
            }
            
            if(SRIdsStepstoCls != null && !SRIdsStepstoCls.isempty()){
                NSIBPM__Status__c cancelledStatus = [select id from NSIBPM__Status__c  where NSIBPM__Code__c='CANCELLED' LIMIT 1];
                //Get all steps of SR which are Open and set status to Cancelled
                list<NSIBPM__Step__c> getAllsteps = new list<NSIBPM__Step__c>();
                getAllsteps = [select id,NSIBPM__Status__c from NSIBPM__Step__c where NSIBPM__SR__c in : SRIdsStepstoCls and Is_Closed__c=:false];
                if(getAllsteps.size()>0){
                    for(NSIBPM__Step__c stp:getAllsteps){
                        stp.NSIBPM__Status__c = cancelledStatus.id;
                    }
                    update getAllsteps;
                }   
            } 
        }
    }
    
    public static void updateDpOverdue(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap){
        List<Id> dealIds = new List<Id>();
        Set<Id> bookingIds = new Set<Id>();
        List<Booking__c> bookingList = new List<Booking__c>();
        List<NSIBPM__Service_Request__c> dealList = new List<NSIBPM__Service_Request__c>();
        for(Booking_Unit__c newBookingUnit : (List<Booking_Unit__c>)newRecordsMap.values()){
            Booking_Unit__c oldBookingUnit = (Booking_Unit__c)oldRecordsMap.get(newBookingUnit.Id);
            if(newBookingUnit != null && oldBookingUnit != null && oldBookingUnit.DP_Overdue__c == false && newBookingUnit.DP_Overdue__c != oldBookingUnit.DP_Overdue__c){
                bookingIds.add(newBookingUnit.Booking__c);  
            }
        }
        
        if(!bookingIds.isEmpty()){
            for(Booking__c thisBooking : [SELECT Id, Name, DP_Overdue__c, Deal_SR__c, Deal_SR__r.DP_Overdue__c FROM Booking__c WHERE Id IN :bookingIds]){
                if(thisBooking.DP_Overdue__c == false){
                    thisBooking.DP_Overdue__c = true;
                    bookingList.add(thisBooking);
                    if(thisBooking.Deal_SR__r.DP_Overdue__c == false){
                        NSIBPM__Service_Request__c thisDeal = new NSIBPM__Service_Request__c();
                        thisDeal.Id = thisBooking.Deal_SR__c;
                        thisDeal.DP_Overdue__c = true;
                        dealIds.add(thisDeal.Id);
                        dealList.add(thisDeal);
                    }
                }
            }
        }
        
        if(!bookingList.isEmpty()){
            update bookingList;
        }
        
        if(!dealList.isEmpty()){
            update dealList;
            CreateStepOnDPOverDue.createStep(dealIds);
        }
    }
    
    /*********************************************************************************************
    * @Description : Method to populate values on booking unit.                                  *
    * @Params      : List<Booking_Unit__c>                                                       *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public static void PopulatevaluesInBookingUnit(List<Booking_Unit__c> newBookingUnitList){
        Set<Id> bookingIds = new Set<Id>();
        Map<Id, List<booking_Unit__c>> bookingAndUnitMap = new Map<Id, List<booking_Unit__c>>();
        Map<Id, List<booking__c>> DealbookingIdsMap = new Map<Id, List<booking__c>>();
        for(Booking_Unit__c Unit:newBookingUnitList){
            bookingIds.add(Unit.Booking__c);
            if(!bookingAndUnitMap.containsKey(Unit.Booking__c)){
                bookingAndUnitMap.put(Unit.Booking__c, new List<booking_Unit__c>{Unit});
            }else{
                bookingAndUnitMap.get(Unit.Booking__c).add(Unit);
            }
        }
            
        for(booking__c booking : [SELECT Id, Deal_SR__c, Deal_SR__r.Rejection_Status__c FROM Booking__c WHERE Id =: bookingAndUnitMap.keySet()]){
            if(DealbookingIdsMap.containsKey(booking.Deal_SR__c)){
                DealbookingIdsMap.get(booking.Deal_SR__c).add(booking);
            }else{
                DealbookingIdsMap.put(booking.Deal_SR__c, new List<booking__c>{booking});
            }
        }
          /* Assuming that Primary_Team__c=true always one record under a deal. */
        for(Deal_Team__c dt : [SELECT Id, Associated_Deal__c, Name, Primary_Team__c, Associated_PC__r.name, 
                                      Associated_HOS__r.name, Associated_HOD__r.name, Associated_DOS__r.name 
                               FROM Deal_Team__c 
                               WHERE Associated_Deal__c IN: DealbookingIdsMap.keySet() AND Primary_Team__c =: true]){
            if(DealbookingIdsMap.containsKey(dt.Associated_Deal__c)){
                for(booking__c booking : DealbookingIdsMap.get(dt.Associated_Deal__c)){
                    if(bookingAndUnitMap.containsKey(booking.Id)){
                        for(booking_Unit__c unit : bookingAndUnitMap.get(booking.id)){
                            if(string.isNotBlank(dt.Associated_HOS__r.name)){
                                unit.HOS_Name__c = dt.Associated_HOS__r.name;
                            }
                            if(string.isNotBlank(dt.Associated_DOS__r.name)){
                                   unit.DOS_Name__c = dt.Associated_DOS__r.name;
                            }
                            if(string.isNotBlank(dt.Associated_HOD__r.name)){
                                unit.HOD_Name__c = dt.Associated_HOD__r.name;
                            }
                            Unit.SR_Rejection_Status__c = booking.Deal_SR__r.Rejection_Status__c;
                        }   
                    }
                }   
            }
        }
    }
}