/**************************************************************************************************
* Name               : CancelDealRejectionSteps
* Description        : Class to Cancel all the closed steps under SR when a Cancelled step is updated to cancelled.
* Test Class         : CancelDealRejectionStepsTest
* Created By         : DAMAC IT
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR               DATE             Comments
* 1.0                            16/06/2019         Created
* 1.1         QBurst             02/04/2020         SOAP to REST change in Webservice call
**************************************************************************************************/
global without sharing class CancelDealRejectionSteps implements NSIBPM.CustomCodeExecutable {
    public String EvaluateCustomCode(NSIBPM__Service_Request__c SR, NSIBPM__Step__c step){
        String retStr = 'Success';
        List<Id> bookingIds = new List<Id>();
        try{
            NSIBPM__Service_Request__c objSR = SRUtility.getSRDetails(step.NSIBPM__SR__c);
            NSIBPM__Status__c cancelledStatus = [SELECT Id FROM NSIBPM__Status__c  WHERE NSIBPM__Code__c = 'CANCELLED' LIMIT 1];
            //Get all steps of SR which are Open and set status to Cancelled
            list<NSIBPM__Step__c> getAllsteps = new list<NSIBPM__Step__c>();
            getAllsteps = [SELECT Id, NSIBPM__Status__c FROM NSIBPM__Step__c 
                           WHERE NSIBPM__SR__c =: objSR.id AND Id !=: step.id 
                           AND Is_Closed__c =: false AND NSIBPM__Step_Status__c != 'Sales Audit Rejected' 
                           LIMIT 50000];
            if(getAllsteps.size() > 0){
                for(NSIBPM__Step__c stp: getAllsteps){
                    stp.NSIBPM__Status__c = cancelledStatus.id;
                }
                update getAllsteps;
            }     
            for(Booking__c bookings :[SELECT Id FROM Booking__c WHERE Deal_SR__c =: step.NSIBPM__SR__c]){
                bookingIds.add(bookings.id);
            }
            if(bookingIds.size() > 0){
                system.debug('#### invoking CancelDealRejectionSteps');
                DAMAC_IPMS_PARTY_CREATION.statusUpdate(bookingIds, '', step.Id); // 1.1
            }
            return retStr;
        } catch(exception ex){
            return ex.getMessage();
        }
    }
}