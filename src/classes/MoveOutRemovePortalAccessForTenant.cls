/*--------------------------------------------------------------------------------------------
Description: Invocable class for Move out used in PB - "FM Move out"
=============================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------
v1.0     | 22-05-2019       | Eternus   | 1. Initial draft
==============================================================================================
*/

public with sharing class MoveOutRemovePortalAccessForTenant {
	@InvocableMethod
    public static void removePortalAccessForTenant(List<FM_Case__c> lstFMCases) {
            
		System.debug('====lstFMCases[0] ====' + lstFMCases[0]);
		calloutmethod(lstFMCases[0].id);
    }

	@future
	public static void calloutmethod(ID currentCase){
		System.debug('====ala future madhe ====' + currentCase);
		FM_Case__c objFMCase = new FM_Case__c();
		objFMCase = [SELECT Id,
							Tenant__r.PersonContactId
					 FROM FM_Case__c
					 WHERE id =:currentCase];
		System.debug('====objFMCase ====' + objFMCase);
		user objUser = new user();
		if(objFMCase != null){
			objUser = [SELECT Id,
						  	  contactId,
							  isactive
				   	   FROM user
				       WHERE contactId =:objFMCase.Tenant__r.PersonContactId];
			System.debug('====objUser ====' + objUser);
			if(objUser != null){
				objUser.isactive = false;
				update objUser;
				System.debug('====update zalela objUser ====' + objUser);
			}
		}
	}
}