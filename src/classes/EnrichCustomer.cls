public without sharing class EnrichCustomer {
    public FM_Case__c caseInst;
    public String accId = '';
    public String buId = '';
    public Account accInst{get; set;}
    public Booking_Unit__c buInst{get; set;}
    public Id profileId;
    public String profileName{get; set;}
    public Account accInitialInst;
    public User userInst {get;set;}
    
    public EnrichCustomer(ApexPages.StandardController controller) {
        caseInst = new FM_Case__c();
        buId = ApexPages.CurrentPage().getparameters().get('id');
        profileId = userinfo.getProfileId();
        profileName = [Select Id,Name from Profile where Id=:profileId].Name;
        userInst = [Select Id,
                            ManagerId
                    FROM  User
                    WHERE Id =:UserInfo.getUserId()
                    LIMIT 1];
        buInst = [SELECT Id,
                         Tenant__c
                 FROM   Booking_Unit__c
                 WHERE Id = :buId
                 LIMIT 1];
        if(String.isNotBlank(buInst.Tenant__c)) {
               accInst = [SELECT Id,
                          Customer_Category__c,
                          Subcategory__c,
                          Brief_Profile_Biography__c
                   FROM Account 
                   WHERE Id = :buInst.Tenant__c
                   LIMIT 1];
        accInitialInst = new Account(Customer_Category__c = accInst.Customer_Category__c,
                                     Subcategory__c = accInst.Subcategory__c,
                                     Brief_Profile_Biography__c = accInst.Brief_Profile_Biography__c);
        }        
     
        //accInitialInst = accInst;
        
    }
    
    public PageReference submitCase() {
        system.debug('accInitialInst-->'+accInitialInst);
        system.debug('accInst-->'+accInst);
        if(accInitialInst.Customer_Category__c != accInst.Customer_Category__c ||
           accInitialInst.Subcategory__c != accInst.Subcategory__c ||
           accInitialInst.Brief_Profile_Biography__c != accInst.Brief_Profile_Biography__c) {
            caseInst.Account__c = accInst.Id;
            caseInst.RecordTypeId = Schema.getGlobalDescribe().get('FM_Case__c').getDescribe().getRecordTypeInfosByName().get('Enrich Tenant').getRecordTypeId();
            caseInst.New_Customer_Category__c = accInst.Customer_Category__c;
            caseInst.New_Sub_Category__c = accInst.Subcategory__c;
            caseInst.New_Brief_Profile_Biography__c = accInst.Brief_Profile_Biography__c;
            system.debug('caseInst--->'+caseInst);
            insert caseInst;
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Case Created Successfully.Redirecting to the case record page...!'));
            PageReference pg = new PageReference('/' + caseInst.id);
            pg.setRedirect(true);
            return pg;
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Category field values cannot be same '));
            return null;
        }
    }
    
    public PageReference onCancel() {
        PageReference pageRef = new PageReference('/'+buId);
        pageRef.setRedirect(true);
        return pageRef;
    }

}