/*
* Created on Thu Apr 13 2017
* Author: Rajil Ravindran
* Gets the Step Processes and the Time specs of the Service Requests 
*/
public with sharing class ProgressIndicatorController{
    public string strTemplateId = '';
    public string strSRID = '';
    public map<string,string> MapGroupData{get;set;}
    public list<StepGroupWrap> lstGroupWrap{get;set;}
    public string GroupWidth{get;set;} //Steps Group width to align on the SR Page
    public string totalTime {get;set;} //Total Timetaken for SR
    public string totalSLA {get;set;}  // SLA for the Serive Request
    public string srStatus {get;set;} //Serivce Request Status
    public ProgressIndicatorController(ApexPages.StandardController stdController) {
        Id dealId = DamacUtility.getRecordTypeId('NSIBPM__Service_Request__c', 'Deal');
        Id changeAgentId = DamacUtility.getRecordTypeId('NSIBPM__Service_Request__c', 'Change Agent');
        MapGroupData = new map<string,string>();
        Datetime submittedDateTime;
        Datetime closedDateTime;
        lstGroupWrap = new list<StepGroupWrap>();
        strSRID = String.escapeSingleQuotes(apexpages.currentPage().getParameters().get('Id'));
        if(strSRID != null){
            for(NSIBPM__Service_Request__c sr:[select id, RecordTypeId, CreatedDate, NSIBPM__SR_Template__c, Closed_DateTime__c, Registration_DateTime__c, NSIBPM__Submitted_DateTime__c,NSIBPM__Internal_SR_Status__r.Name from NSIBPM__Service_Request__c where Id=:strSRID]){
                strTemplateId = sr.NSIBPM__SR_Template__c;
                if(sr.RecordTypeId == dealId ){
                    submittedDateTime = sr.Registration_DateTime__c;
                }else if (sr.RecordTypeId == changeAgentId || sr.NSIBPM__Submitted_DateTime__c == null){
                    submittedDateTime = sr.CreatedDate;
                }else{
                    submittedDateTime = sr.NSIBPM__Submitted_DateTime__c;
                }
                if(sr.Closed_DateTime__c != null){
                    closedDateTime = sr.Closed_DateTime__c;
                }
                srStatus=sr.NSIBPM__Internal_SR_Status__r.Name;
            }
            Datetime currentDateTime = DateTime.now();
            if(submittedDateTime != null && closedDateTime == null)
            {
                Long seconds = (currentDateTime.getTime() - submittedDateTime.getTime()) / 1000;
                Long minutes = seconds / 60;
                Long hours = minutes / 60;
                totalTime = String.valueOf(hours) + ' hour(s)';
            } else if(submittedDateTime != null && closedDateTime != null){
                
                Long seconds = (closedDateTime.getTime() - submittedDateTime.getTime()) / 1000;
                Long minutes = seconds / 60;
                Long hours = minutes / 60;
                totalTime = String.valueOf(hours) + ' hour(s)';
            }
            //Prepares the Step Group 
            PrepareGroups();
        }
        
    }
    public void PrepareGroups(){
        if(strTemplateId!=null && strTemplateId!=''){
            StepGroupWrap objGrpWrap;
            map<string,integer> mapGroupIndex = new map<string,integer>();
            map<string,list<NSIBPM__SR_Steps__c>> MapGroupSteps = new map<string,list<NSIBPM__SR_Steps__c>>();
            for(NSIBPM__SR_Steps__c SRStp : [select Id,Name,Step_Type__c,NSIBPM__Step_No__c,NSIBPM__Summary__c, Render_On_Step_Creation__c, Group_Name__c,Step_Instructions__c, NSIBPM__SR_template__r.NSIBPM__Estimated_Hours__c,(select Id,NSIBPM__SR__c,SLA__c,Actual_Time_Taken_Mins__c,NSIBPM__Closed_Date__c,NSIBPM__Status_Type__c,NSIBPM__SR_Step__c,NSIBPM__Sys_Step_Loop_No__c,NSIBPM__Summary__c, CreatedDate from NSIBPM__Steps__r where NSIBPM__SR__c=:strSRID ORDER BY CreatedDate DESC) from NSIBPM__SR_Steps__c where NSIBPM__SR_Template__c=:strTemplateId and Group_Name__c!=null and NSIBPM__Active__c=true  order by NSIBPM__Step_No__c]){
                system.debug(SRStp);
                list<NSIBPM__SR_Steps__c> lstSRSteps = new list<NSIBPM__SR_Steps__c>();
                if(MapGroupSteps.get(SRStp.Group_Name__c)==null){
                    objGrpWrap = new StepGroupWrap();
                    objGrpWrap.GroupLabel = SRStp.Group_Name__c;
                    objGrpWrap.GroupShortLabel = string.valueOf(lstGroupWrap.size()+1);
                    totalSLA = SRStp.NSIBPM__SR_template__r.NSIBPM__Estimated_Hours__c != null ? string.valueOf(SRStp.NSIBPM__SR_template__r.NSIBPM__Estimated_Hours__c) + ' hour(s)' : '0';
                    if(!SRStp.Render_On_Step_Creation__c ){
                        lstGroupWrap.add(objGrpWrap);
                    }else if (SRStp.Render_On_Step_Creation__c && !(SRStp.NSIBPM__Steps__r).isEmpty()){
                        lstGroupWrap.add(objGrpWrap);
                    }
                }else{
                    lstSRSteps = MapGroupSteps.get(SRStp.Group_Name__c); 
                }
                lstSRSteps.add(SRStp);
                MapGroupSteps.put(SRStp.Group_Name__c,lstSRSteps);
            }

            if(lstGroupWrap!=null && lstGroupWrap.size()>0 && MapGroupSteps!=null && MapGroupSteps.size()>0){
                for(StepGroupWrap SGW:lstGroupWrap){
                    list<StepGroupChildData> lstChilds = new list<StepGroupChildData>();
                    for(NSIBPM__SR_Steps__c SRStep:MapGroupSteps.get(SGW.GroupLabel)){
                        if(SRStep.NSIBPM__Steps__r!=null && SRStep.NSIBPM__Steps__r.size()>0){
                            //FIX to stop creating new labels to denote recreated steps.
                            //for(NSIBPM__Step__c stp:SRStep.NSIBPM__Steps__r){
                                NSIBPM__Step__c stp = SRStep.NSIBPM__Steps__r[0];
                                StepGroupChildData CD = new StepGroupChildData();
                                CD.hasStep = true;
                                if(SRStep.Step_Type__c!=null && SRStep.Step_Type__c=='Document Check')
                                    CD.DocumentCheckStep = true;
                                else if(SRStep.Step_Type__c!=null && SRStep.Step_Type__c=='Quick Action')
                                    CD.QuickActionStep = true;
                                CD.StepSummary = SRStep.NSIBPM__Summary__c;
                                CD.StepId = stp.Id;
                                CD.ActionType = SRStep.Step_Type__c;
                                CD.SRID = stp.NSIBPM__SR__c;
                                CD.StepLoop = stp.NSIBPM__Sys_Step_Loop_No__c;
                                CD.helpText = SRStep.Step_Instructions__c;
                                if(stp.NSIBPM__Status_Type__c!='End'){
                                    SGW.CurrentGroup = true;
                                    CD.IsOpen = true;
                                }else if(stp.NSIBPM__Status_Type__c=='End'){
                                    CD.IsClosed = true;
                                }
                                if(stp.Actual_Time_Taken_Mins__c!=null && stp.Actual_Time_Taken_Mins__c > 59) //Consider only hours, ignoring minutes
                                {
                                    decimal actualTimeInMins = stp.Actual_Time_Taken_Mins__c/60;
                                    CD.SLA = string.valueOf(math.floor(actualTimeInMins))+' hour(s)';
                                }
                                CD.createdDate = stp.CreatedDate;
                                lstChilds.add(CD);
                            //}
                        }else{
                            StepGroupChildData CD = new StepGroupChildData();
                            SGW.Passed_Group = false;
                            CD.hasStep = false;
                            CD.StepSummary = SRStep.NSIBPM__Summary__c;
                            CD.helpText = SRStep.Step_Instructions__c;
                            lstChilds.add(CD);
                        }
                        
                    }
                    //lstChilds.sort();
                    system.debug(lstChilds);
                    SGW.lstChilds = lstChilds;
                }
            }
            system.debug('lstGroupWrap===>'+lstGroupWrap);
            if(lstGroupWrap!=null && lstGroupWrap.size()>0)
            {
                Decimal dLstGroupSize = lstGroupWrap.size();
                Decimal dGroupWidth = 100/dLstGroupSize;
                GroupWidth = String.valueOf(dGroupWidth.setScale(3))+'%';
            }
            system.debug('GroupWidth===>'+GroupWidth);
        }
    }
    
    public class StepGroupWrap{
        public string GroupLabel{get;set;}
        public string GroupShortLabel{get;set;}
        public boolean CurrentGroup{get;set;}
        public boolean Passed_Group{get;set;}
        public list<StepGroupChildData> lstChilds{get;set;}
        public StepGroupWrap(){
            lstChilds = new list<StepGroupChildData>();
            CurrentGroup = false;
            Passed_Group = true;
        }
    }
    public class StepGroupChildData implements Comparable{
        public string StepSummary{get;set;}
        public string StepId{get;set;}
        public string SRID{get;set;}
        public string StepLoop{get;set;}
        public boolean hasStep{get;set;}
        public boolean IsOpen{get;set;}
        public string helpText {get;set;}
        public boolean IsClosed{get;set;}
        public string SLA{get;set;}
        public boolean QuickActionStep{get;set;}
        public boolean DocumentCheckStep{get;set;}
        public DateTime createdDate{get; set;}
        public string ActionType{get;set;}
        public StepGroupChildData(){
            IsOpen = false;
            IsClosed = false;
            DocumentCheckStep = false;
            QuickActionStep = false;
            ActionType = '';
            helpText = '';
            createdDate = DateTime.newInstance(2099, 12, 25);
        }  
        public StepGroupChildData(DateTime createdDateValue){
            IsOpen = false;
            IsClosed = false;
            DocumentCheckStep = false;
            QuickActionStep = false;
            ActionType = '';
            helpText = '';
            createdDate = createdDateValue ;
        }  
        //custom sort
        public Integer compareTo(Object compareTo){
            StepGroupChildData compareToObj = (StepGroupChildData) compareTo;
            Integer returnValue = 0;
            if(createdDate > compareToObj.createdDate){
                returnValue = 1;
            } else if(createdDate < compareToObj.createdDate){
                returnValue = -1;
            }
            return returnValue;
        }
    }
}