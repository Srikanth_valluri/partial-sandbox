@isTest
public class DAMAC_WAServicesChatBot_Test {
    static testMethod void Damac_WeServicesChatBot ()
    {
        ID recordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();  
        Inquiry__c inq = new Inquiry__c ();
        inq.Mobile_phone__c = '1234567890';
        inq.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Mobile_Phone_Encrypt__c = 'WERTYUIYRTYUIYRTYU';
        inq.Class__c = 'test';
        inq.First_Name__c = 'test';
        inq.Last_Name__c = 'test';
        inq.OwnerId = userInfo.getuserID ();
        inq.Inquiry_Status__c = 'Active';
        inq.Inquiry_Source__c ='Agent Referral';  
        inq.RecordtypeId = recordTypeId;
        inq.Class__c = 'test';
        inq.Email__c = 'test@gmail.com';
        inq.Phone_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Nationality__c = 'Australian';
        inq.weChat_Id__c = 'WERTYUIYRTYUIYRTYU';
        insert inq;   
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.requestURI = '/WAService/search';  
        req.addParameter('mobile', '12345678901');
        req.httpMethod = 'GET';
        
        RestContext.request = req;
        RestContext.response = res; 
        
        
        DAMAC_WAServicesChatBot obj = new  DAMAC_WAServicesChatBot();
        
        DAMAC_WAServicesChatBot.getResults();
        
    }
    static testmethod void method02(){
          
        Inquiry_User_Assignment_Rules__c ua1 = new Inquiry_User_Assignment_Rules__c ();
        ua1.Daily__c = 2;
        ua1.Monthly__c =3;
        ua1.Weekly__c =5;
        ua1.Available_For_WA__c = true;
        insert ua1;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.requestURI = '/WAService/activeUsers';  
        req.httpMethod = 'GET';
        
        RestContext.request = req;
        RestContext.response = res; 
        
        
        DAMAC_WAServicesChatBot obj = new  DAMAC_WAServicesChatBot();
        
        DAMAC_WAServicesChatBot.getResults();
    }
    
    static testmethod void methodPOST (){
          
        ID recordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();  
        Inquiry__c inq = new Inquiry__c ();
        inq.Mobile_phone__c = '1234567890';
        inq.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Mobile_Phone_Encrypt__c = 'WERTYUIYRTYUIYRTYU';
        inq.Class__c = 'test';
        inq.First_Name__c = 'test';
        inq.Last_Name__c = 'test';
        inq.OwnerId = userInfo.getuserID ();
        inq.Inquiry_Status__c = 'Active';
        inq.Inquiry_Source__c ='Agent Referral';  
        inq.RecordtypeId = recordTypeId;
        inq.Class__c = 'test';
        inq.Email__c = 'test@gmail.com';
        inq.Phone_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Nationality__c = 'Australian';
        inq.weChat_Id__c = 'WERTYUIYRTYUIYRTYU';
        insert inq; 
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.requestURI = '/WAService/message';  
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf ('{"trigger" : "event", "InqId" : "'+inq.Id+'",  "_id" : "WERTYUIYRTYUIYRTYU", "messages": [{"source" : {"type" : "test"}}], "appUser" : {"conversationStarted" : true, "givenName" : "test", "surName" : "test", "email" : "test@test.com"}}');
                
        RestContext.request = req;
        RestContext.response = res; 
        
        
        DAMAC_WAServicesChatBot obj = new  DAMAC_WAServicesChatBot();
        
        DAMAC_WAServicesChatBot.createWAMessage();
    }
}