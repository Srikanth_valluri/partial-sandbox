public class DAMAC_AMEYO_CALLBACK_RESPONSE{

    public BeanResponse beanResponse;

    public class BeanResponse {
        public String id;
        public Integer campaignId;
        public String callBackTime;
        public Boolean selfCallback;
        public CallBackProperties callBackProperties;
        public String userId;
        public String phone;
        public Integer customerId;
    }

    public class CallBackProperties {
        public String customerId;
        public String phone;
    }

    
    public static DAMAC_AMEYO_CALLBACK_RESPONSE parse(String json) {
        return (DAMAC_AMEYO_CALLBACK_RESPONSE) System.JSON.deserialize(json, DAMAC_AMEYO_CALLBACK_RESPONSE.class);
    }
}