/**********************************************************************************************************************
Description: This API is used for getting the Appointment purposes and sub-purposes
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   20-11-2020      | Anand Venkitakrishnan | Created initial draft
***********************************************************************************************************************/

@RestResource(urlMapping='/appointmentPurposeDetails/*')
global class AppointmentPurposeDetailsAPI {
    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };
	
    @HttpGet
    global static FinalReturnWrapper getAppointmentPurposeDetails() {
        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        
        RestRequest r = RestContext.request;
        System.debug('Request params:'+r.params);
        
        cls_meta_data objMeta = new cls_meta_data();
        
        List<cls_data> appointmentPurposeDetails = new List<cls_data>();
        
        if(!r.params.containsKey('accountId')) {
            objMeta = ReturnMetaResponse('Missing parameter : accountId','',3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(r.params.containsKey('accountId') && String.isBlank(r.params.get('accountId'))) {
            objMeta = ReturnMetaResponse('Missing parameter value : accountId','',3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        
        String accountId = r.params.get('accountId');
        
        List<User> usr = [SELECT Id,Name,Contact.AccountId 
                          FROM User
                          WHERE Contact.AccountId =: accountId];
        System.debug('usr:'+usr);
		        
        if(usr.isEmpty()) {
            objMeta = ReturnMetaResponse('No valid user for the accountId','',3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
		
        appointmentPurposeDetails = getPortalAppointmentPurposesAndSubpurposes(); /* getPurposeDetailsFromNewMDT() */
        objMeta = ReturnMetaResponse('Successful','',1);
        
        returnResponse.meta_data = objMeta;
        returnResponse.data = appointmentPurposeDetails;
        
        System.debug('returnResponse: ' + returnResponse);
        return returnResponse;
    }
    
    public static List<cls_data> getPortalAppointmentPurposesAndSubpurposes() {
        List<cls_data> portalPurposeList = new List<cls_data>();

        List<Active_Process_on_Portal_Meta__mdt> lstActivePortalProcess = [SELECT id, MasterLabel, CRE_MasterLabel__c, DeveloperName, 
                                                 Appointment_Hr__c, Unique_code_number__c, (SELECT id, MasterLabel, DeveloperName, 
                                                 Unique_code_number__c FROM Sub_Processes_for_Portal__r) 
                                                 FROM Active_Process_on_Portal_Meta__mdt 
                                                 WHERE  CRE_MasterLabel__c != NULL 
                                                 AND CRE_MasterLabel__c NOT IN ('Handover','Rotana','Recovery') 
                                                 ORDER BY MasterLabel];
        if(NULL != lstActivePortalProcess && lstActivePortalProcess.size() > 0) {
            for(Active_Process_on_Portal_Meta__mdt portalAppointmentProcess : lstActivePortalProcess) {
                cls_data purpose = new cls_data();
                purpose.name = portalAppointmentProcess.CRE_MasterLabel__c;
                purpose.id = Integer.valueOf(portalAppointmentProcess.Unique_code_number__c);
                purpose.sf_id = portalAppointmentProcess.id;
                purpose.hours = portalAppointmentProcess.Appointment_Hr__c;
                
                List<Sub_Process_for_Portal__mdt> subProcesssList = portalAppointmentProcess.Sub_Processes_for_Portal__r;
                List<cls_data> subPurposesForProcess = new List<cls_data>();
                if(NULL != subProcesssList && subProcesssList.size() > 0) {
                    for(Sub_Process_for_Portal__mdt subProcess : subProcesssList) {
                        cls_data subPurpose = new cls_data();
                        subPurpose.name = subProcess.MasterLabel;
                        subPurpose.id = Integer.valueOf(subProcess.Unique_code_number__c);
                        subPurpose.sf_id = subProcess.id;
                        subPurpose.hours = portalAppointmentProcess.Appointment_Hr__c;
                    	subPurposesForProcess.add(subPurpose);
                    }
                }
                else {
                    cls_data subPurpose = new cls_data();
                    subPurpose.name = portalAppointmentProcess.CRE_MasterLabel__c;
                    subPurpose.id = Integer.valueOf(portalAppointmentProcess.Unique_code_number__c);
                    subPurpose.sf_id = portalAppointmentProcess.id;
                    subPurpose.hours = portalAppointmentProcess.Appointment_Hr__c;
                    subPurposesForProcess.add(subPurpose);
                }
                purpose.sub_purposes = subPurposesForProcess;
                
                portalPurposeList.add(purpose);
            } /* for(Active_Process_on_Portal_Meta__mdt portalAppointmentProcess : lstActivePortalProcess) */
        }
        
        return portalPurposeList;
    }
	
    public static cls_meta_data ReturnMetaResponse(String message, String devMsg, Integer statusCode) {
        cls_meta_data retMeta = new cls_meta_data();
        retMeta.message = message;
        retMeta.status_code = statusCode;
        retMeta.title = mapStatusCode.get(statusCode);
        retMeta.developer_message = devMsg;
        return retMeta;
    }
    
    global class FinalReturnWrapper {
        public cls_data[] data;
        public cls_meta_data meta_data;
    }
    
    public class cls_data {
        public String name;
        public Integer id;
        public String sf_id;
        public String hours;
        public cls_data[] sub_purposes;
    }
    
    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message; 
    }
}