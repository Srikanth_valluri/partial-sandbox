global with sharing class MobileAppLoginController {
/**************************************************************************************************
            Variables used in the class
**************************************************************************************************/

  public string username{set;get;}
  public string password{set;get;}
  public boolean rememberMe{set;get;}
    public string registrationPageID{set;get;}
   public string toastrError {get; set;}
   public string email{set;get;}
   public string firstName{set;get;}
   public string lastName{set;get;}
   public string phonenumber{set;get;}
   public string password1{set;get;}
   public string password2{set;get;}
   
/**************************************************************************************************
    Method:         DamacLoginController
    Description:    Constructor executing model of the class 
**************************************************************************************************/

  public MobileAppLoginController() {
    rememberMe = false;
       
        if (ApexPages.currentPage().getCookies().get('username') != NULL)
            username = ApexPages.currentPage().getCookies().get('username').getValue();
        if (ApexPages.currentPage().getCookies().get('password') != NULL) {
            string codedpassword = ApexPages.currentPage().getCookies().get('password').getValue();
            system.debug('codedpassword ::' + codedpassword );
            blob blobcodedpassword =  EncodingUtil.base64Decode(codedpassword);
            password = blobcodedpassword.toString();
            system.debug('password::' + password);
        }

        system.debug('in controller..');
       
        registrationPageID = UtilityQueryManager.getProcessFlowId(LABEL.Agent_Portal_Registration_Page_Flow_Name);


  }
/**************************************************************************************************
    Method:         redirectToHome
    Description:    If the user is already logged in - redirect to portal landing page
**************************************************************************************************/

    public PageReference redirectToHome(){
        if(UserInfo.getUserType() != 'Guest'){
            return new PageReference('/AgentPortal');
        }

        return null;
    }

/**************************************************************************************************
    Method:         login
    Description:    Site login functionality
**************************************************************************************************/

  global PageReference login() {
      system.debug('in debug.....');
    blob blobPassword = blob.valueof(password);
    string codedPassword = EncodingUtil.base64Encode(blobPassword);
    if (rememberMe == true) {
            cookie Name = new Cookie('username', Username, null, 365, false);
            cookie PasswordCokies = new Cookie('password', codedPassword, null, 365, false);
            ApexPages.currentPage().setCookies(new Cookie[] {Name, PasswordCokies});

        }
      return Site.login(username,password, ApexPages.currentPage().getParameters().get('startURL'));
        //return Site.login(username,password,'');
  }


 // Method to reset the password
    public PageReference forgotPassword() {
        toastrError = '';
        boolean success = false;
        string strUsername;
        strUsername = apexpages.currentpage().getparameters().get('EmailAddress');

        system.debug('--username'+strUsername );
        if(strUsername  == null || string.isempty(strUsername )){
          toastrError = 'Please enter username';
          return null;
        }
        if(strUsername  != null){
            if(strUsername .contains('@')){
                if(!checkUsername(strUsername )){
                    toastrError = 'The entered username is wrong please enter a correct username/PartyId';
                    return null;
                }
                username = strUsername ;
            }else{
                username = fetchUsername(strUsername );
            }
            if(username!= null){
                toastrError = 'An email is sent to your register email Id';
                system.debug('username---'+username);
                success = Site.forgotPassword(username);
                system.debug('success'+success);
            }
        }
        //PageReference pr = new PageReference(ApexPages.currentPage().getParameters().get('startURL'));
        //PageReference pr = new PageReference('https://devpro-servicecloudtrial-155c0807bf-1580afc5db1.cs86.force.com/Customer');
       // pr.setRedirect(true);
        if (success) {
            ApexPages.addMessage(new ApexPages.message(
                ApexPages.Severity.CONFIRM, 'An email is sent to your registered email id'
            ));
            //system.debug('pr---'+pr);
        }else{
           toastrError = 'Please enter a valid username/PartyId.';
        }
        //system.debug('pr---'+pr);
        return null;
    }//End of forgotPassword

    //Method to fetch the username if ucer enters a party Id.
    public string fetchUsername(String strPartyId){
        list<user> listuser = [SELECT Id,
                                      Username,
                                      Email,
                                      Contact.Account.Party_ID__c
                                FROM  User
                                WHERE IsActive = TRUE
                                  AND Contact.Account.Party_ID__c = :strPartyId];
        system.debug('listuser--'+listUser);
        if(listuser.isEmpty()){
            return null;
        }
        else{
            return listuser[0].Username;
        }
    }//End of fetchUsername

    public Boolean checkUsername(string username){
        system.debug('username-----'+username);
        list<user> listUser = [SELECT Id,
                                      Username
                                 FROM User
                                WHERE Username=:username];
        if(listUser.isEmpty()){

            return false;
        }else{
            system.debug('model.username----'+listUser);
            return true;
        }
    }//End of checkUsername
    
    public void Register()
    {
       Mobile_Registration__c mr=new Mobile_Registration__c();
       mr.Email__c=email;
       mr.First_Name__c=firstName;
       mr.Last_Name__c=lastName;
       mr.Phone_Number__c=phonenumber;
       mr.Password__c=password1;
       mr.Conform_Password__c=password2;
       insert mr;
       if(mr.id!=null)
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Thank you for registering with us. We are reviewing your request and details will be confirmed to you shortly.'));

       
    }
}