/****************************************************************************************************
 Name          : DamacOfferService
 Description   : JSON Class for Damac Offers  
 Test Class    : MockHttpDamacOfferServiceResponse
 --------------------------------------------------------------------------------------------------
 VER   AUTHOR             DATE        COMMENTS                                                    
 1.0   SF Support      22/03/2018     Initial Draft.
 1.1   QBurst          17/03/2020     Commented out existing and added new json class 
                                                                 based on modified response.
****************************************************************************************************/
public Class DamacOfferService{
    // 1.1 starts
    public cls_data data;
    public class cls_data {
        public cls_nodeQuery nodeQuery;
    }
    public class cls_nodeQuery {
        public cls_entities[] entities;
    }
    public class cls_entities {
        public cls_entityTranslation entityTranslation;    
    }
    public class cls_entityTranslation {
        public string entityId;
        public string title;
        //public Integer created;
       // public string fieldOfferStartDateValue;
        //public string fieldOfferAvailableUntilValu;
        public string fieldOfferSubtitle;
        public cls_fieldOfferThumbnail fieldOfferThumbnail;
        public cls_path path;
   }
    public class cls_fieldOfferThumbnail {
        public string url;
        public string alt;
    }
    public class cls_path {
        public string alias;
    }
    
    public static DamacOfferService parse(String json){
        return (DamacOfferService) System.JSON.deserialize(json, DamacOfferService.class);
    }
    // 1.1 ends
}

/*
    public List<Nodes> nodes {get;set;}

    public DamacOfferService(JSONParser parser) {
        system.debug('parser.nextToken(): ' + parser.nextToken());
        while (parser.nextToken() != System.JSONToken.END_OBJECT) {
            system.debug('parser.getCurrentToken(): ' + parser.getCurrentToken());
            if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                    if (text == 'nodes') {
                        nodes = arrayOfNodes(parser);
                    } 
                }
            }
        }
    }
    
    public class Node {
        public String title {get;set;} 
        public String summary {get;set;} 
        public String main_image {get;set;} 
        public String Link {get;set;} 

        public Node(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'title') {
                            title = parser.getText();
                        } else if (text == 'summary') {
                            summary = parser.getText();
                        } else if (text == 'main_image') {
                            main_image = parser.getText();
                        }else if (text == 'Link') {
                            Link = parser.getText();
                        } 
                    }
                }
            }
        }
    }
    
    public class Nodes {
        public Node node {get;set;} 

        public Nodes(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'node') {
                            node = new Node(parser);
                        }
                    }
                }
            }
        }
    }
    
    private static List<Nodes> arrayOfNodes(System.JSONParser p) {
        List<Nodes> res = new List<Nodes>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Nodes(p));
        }
        return res;
    }
}
*/