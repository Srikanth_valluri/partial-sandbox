public class InquiryCretionOnAssignment {
    
  @InvocableMethod(label='InquiryCretionOnAssignment' description='create Inquiry on Assignment SR' category='Task')
    public static void createInquiry(List<ID> ids) {
         Inquiry__c objInquiry ;
        list<Case> lstC = [Select Id
                                    , CaseNumber
                                    , Origin
                                    , Status
                                    , Assignment_Fee__c
                                    , LOA_Submitted__c
                                    , Buyer_Type__c
                                    , Relationship_with_Seller__c
                                    , Case_Type__c
                                    , Buyer_Info__c
                                    , Seller_POA__c
                                    , Buyer_POA_Country_Code__c
                                    , Buyer_POA_Phone__c
                                    , Seller_POA_Country_Code__c
                                    , Seller_POA_Phone__c
                                    , POA_Name_Seller__c
                                    , Purpose_of_POA_Seller__c
                                    , POA_Issued_By_Seller__c
                                    , Buyer_POA__c
                                    , POA_Name__c
                                    , Purpose_of_POA__c
                                    , POA_Issued_By__c
                                    , POA_Expiry_Date__c
                                    , POA_Expiry_Date_Seller__c
                                    , Seller__c
                                    , Percent_to_be_waived__c
                                    , Buyer__c
                                    , Buyer__r.Booking__c
                                    , Buyer__r.Organisation_Name__c
                                    , Buyer__r.CR_Registration_Expiry_Date__c
                                    , Buyer__r.CR_Registration_Place__c
                                    , Buyer__r.Organisation_Name_Arabic__c
                                    , Buyer__r.CR_Number__c
                                    , Buyer__r.CR_Registration_Place_Arabic__c
                                    , Buyer__r.Title__c
                                    , Buyer__r.First_Name__c
                                    , Buyer__r.Last_Name__c
                                    , Buyer__r.Nationality__c
                                    , Buyer__r.Passport_Number__c
                                    , Buyer__r.Place_of_Issue__c
                                    , Buyer__r.Address_Line_1__c
                                    , Buyer__r.Address_Line_2__c
                                    , Buyer__r.Address_Line_3__c
                                    , Buyer__r.Address_Line_4__c
                                    , Buyer__r.Country__c
                                    , Buyer__r.City__c
                                    , Buyer__r.Date_of_Birth__c
                                    , Buyer__r.Title_Arabic__c
                                    , Buyer__r.First_Name_Arabic__c
                                    , Buyer__r.Last_Name_Arabic__c
                                    , Buyer__r.Nationality_Arabic__c
                                    , Buyer__r.Passport_Expiry_Date__c
                                    , Buyer__r.Place_of_Issue_Arabic__c
                                    , Buyer__r.Address_Line_1_Arabic__c
                                    , Buyer__r.Address_Line_2_Arabic__c
                                    , Buyer__r.Address_Line_3_Arabic__c
                                    , Buyer__r.Address_Line_4_Arabic__c
                                    , Buyer__r.Country_Arabic__c
                                    , Buyer__r.City_Arabic__c
                                    , Buyer__r.Phone_Country_Code__c
                                    , Buyer__r.Phone__c
                                    , Buyer__r.Email__c
                                    ,Buyer__r.Primary_Language__c
                                    , Booking_Unit__c
                                    , Booking_Unit__r.CurrencyIsoCode
                                    , Booking_Unit__r.Booking__r.Deal_SR__c
                                    , Booking_Unit__r.Registration_ID__c
                                    , Booking_Unit__r.Name
                                    , Booking_Unit__r.Booking__c
                                    , Booking_Unit__r.Unit_Details__c
                                    , Booking_Unit__r.Unit_Type__c
                                    , Booking_Unit__r.Booking__r.Account__c
                                    , Booking_Unit__r.Inventory__c
                                    , Booking_Unit__r.Inventory__r.Property__r.Name
                                    , Booking_Unit__r.Inventory__r.Unit__c
                                    , Booking_Unit__r.Inventory__r.Property_City__c
                                    , Booking_Unit__r.Inventory__r.Unit_Type__c
                                    , Booking_Unit__r.Inventory__r.Property_Status__c
                                    , Booking_Unit__r.Inventory__r.Building_Code__c
                                    , Booking_Unit__r.Inventory__r.Bedroom_Type__c
                                    , Booking_Unit__r.Permitted_Use__c
                                    , Booking_Unit__r.Rental_Pool__c
                                    , Booking_Unit__r.Requested_Price__c
                                    , Booking_Unit__r.Booking_Undergoing_Termination__c
                                    , Booking_Unit__r.NOC_Issued_Date__c
                                    , Booking_Unit__r.Dispute__c
                                    , Booking_Unit__r.Enforcement__c
                                    , Booking_Unit__r.Litigation__c
                                    , Booking_Unit__r.Counter_Case__c
                                    , Booking_Unit__r.Mortgage__c
                                    , Booking_Unit__r.Re_Assigned__c
                                    , Booking_Unit__r.Penalty_Amount__c
                                    , Booking_Unit__r.Token_Amount_val__c
                                    , Booking_Unit__r.Booking_Type__c
                                    , Booking_Unit__r.DP_Overdue__c
                                    , Booking_Unit__r.Plot_Price__c
                                    , Booking_Unit__r.Booking__r.CreatedDate
                                    , Booking_Unit__r.Agreement_Date__c
                                    , Booking_Unit__r.JOPD_Area__c
                                    , Booking_Unit__r.Area__c
                                    , Booking_Unit__r.DP_OK__c
                                    , Booking_Unit__r.Doc_OK__c
                                    , Booking_Unit__r.OQOOD_Reg_Flag__c
                                    , Booking_Unit__r.Early_Handover__c
                                    , Booking_Unit__r.Handover_Flag__c
                                    , Booking_Unit__r.PCC_Release__c
                                    , Booking_Unit__r.Construction_Status__c
                                    , Booking_Unit__r.Area_Varied__c
                                    , Booking_Unit__r.Title_Deed__c
                                    , Booking_Unit__r.Under_Assignment__c
                                    , Booking_Unit__r.Regular_Bulk_Flag__c
                                    , Booking_Unit__r.Registration_DateTime__c
                                    , Booking_Unit__r.Manager_Name__c
                                    , Booking_Unit__r.HOS_Name__c
                                    , Booking_Unit__r.Property_Consultant__c
                                    , Booking_Unit__r.Inventory__r.Floor_Plan__c
                                    , Booking_Unit__r.Inventory__r.Unit_Plan__c
                                    , Booking_Unit__r.Finance_Flag__c
                                    from Case
                                    where Id =: ids[0]
                                    limit 1];
                                    
        Buyer__c objBuyer = lstC[0].Buyer__r;
        Booking__c objBooking = new Booking__c();
        
        if(lstC[0].Buyer__r.Booking__c != null){
            objBooking.Id = lstC[0].Buyer__r.Booking__c;
        }
        
        list<jointBuyer> lstJointBuyer = new list<jointBuyer>();
        
        for(Buyer__c objJB : [Select b.Title__c
                                   , b.CreatedDate
                                   , b.LastModifiedDate
                                   , b.Title_Arabic__c
                                   , b.Place_of_Issue__c
                                   , b.Place_of_Issue_Arabic__c
                                   , b.Phone__c
                                   , b.Phone_Country_Code__c
                                   , b.Passport_Number__c
                                   , b.Passport_Expiry_Date__c
                                   , b.Party_ID__c
                                   , b.Organisation_Name__c
                                   , b.Organisation_Name_Arabic__c
                                   , b.Nationality__c
                                   , b.Nationality_Arabic__c
                                   , b.Name
                                   , b.Last_Name__c
                                 , b.Last_Name_Arabic__c
                                 , b.Id
                                 , b.IPMS_Status__c
                                 , b.IPMS_Buyer_ID__c
                                 , b.First_Name__c
                                 , b.First_Name_Arabic__c
                                 , b.Email__c
                                 , b.Date_of_Birth__c
                                 , b.Country__c
                                 , b.Country_Arabic__c
                                 , b.City__c
                                 , b.City_Arabic__c
                                 , b.CR_Registration_Place__c
                                 , b.CR_Registration_Place_Arabic__c
                                 , b.CR_Registration_Expiry_Date__c
                                 , b.CR_Number__c
                                 , b.Buyer_Type__c
                                 , b.Buyer_ID__c
                                 , b.Booking__c
                                 , b.Address_Line_4__c
                                 , b.Address_Line_4_Arabic__c
                                 , b.Address_Line_3__c
                                 , b.Address_Line_3_Arabic__c
                                 , b.Address_Line_2__c
                                 , b.Address_Line_2_Arabic__c
                                 , b.Address_Line_1__c
                                 , b.Address_Line_1_Arabic__c
                        From Buyer__c b
                        where b.Booking__c = :objBuyer.Booking__c
                        and b.Primary_Buyer__c = false
                        order by LastModifiedDate desc]){
                         jointBuyer newJB = new jointBuyer();
                         newJB.objJointBuyer = objJB;
                        
                         lstJointBuyer.add(newJB);
                         
                     }
    
        objInquiry = new Inquiry__c();
        objInquiry.RecordtypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Assignment Inquiry').getRecordTypeId();
        objInquiry.Inquiry_Source__c = 'Assignment';
        objInquiry.First_Name__c = objBuyer.First_Name__c;
        objInquiry.Last_Name__c = objBuyer.Last_Name__c;
        //objInquiry.Preferred_Language__c = 'English';
        system.debug('objBuyer.Primary_Language__c********'+objBuyer.Primary_Language__c);
        objInquiry.Preferred_Language__c = objBuyer.Primary_Language__c;
        objInquiry.Primary_Contacts__c = 'Mobile Phone';
        objInquiry.Mobile_CountryCode__c = objBuyer.Phone_Country_Code__c;
        objInquiry.Mobile_Phone__c = objBuyer.Phone__c;
        objInquiry.Mobile_Phone_Encrypt__c = objBuyer.Phone__c;
        objInquiry.Email__c = objBuyer.Email__c;
        objInquiry.Inquiry_Status__c = 'Closed Won';
        //insert objInquiry; //commented by Vivek

        //Code to query existing inquiry added by Vivek
        String strInquiryId = Label.InquiryId;
        List<Inquiry__c> lstInquiry = [Select Id From Inquiry__c
            Where Id =: strInquiryId limit 1];
        system.debug('--lstInquiry--'+lstInquiry);
        if(lstInquiry != null && !lstInquiry.isEmpty()) {
            objInquiry = lstInquiry[0];
        }
        else {
            insert objInquiry;
        }

        system.debug('B inside else*****');
        objBuyer.Booking__c = (objBooking.Id != NULL) ? objBooking.Id : NULL;
        objBuyer.Buyer_Type__c = lstC[0].Buyer_Type__c;
        objBuyer.Primary_Buyer__c = true;
        objBuyer.Inquiry__c = objInquiry.Id;
        upsert objBuyer;
        //}
        //objCase.Buyer__c = objBuyer.Id;
        //upsert objBuyer;
        list<Buyer__c> lstJB = new list<Buyer__c>();
        if(lstJointBuyer != null){
            for(jointBuyer objJB : lstJointBuyer){
                Buyer__c newJB;
                system.debug('JB inside else*****');
                newJB = objJB.objJointBuyer;
                newJB.Inquiry__c = objInquiry.Id;
                //}
                newJB.Buyer_Type__c = lstC[0].Buyer_Type__c;
                newJB.Booking__c = objBooking.Id;
                newJB.Primary_Buyer__c = false;
                lstJB.add(newJB);
            }
            if(!lstJB.isEmpty()){
                upsert lstJB;
            }
        }
         lstC[0].Buyer__c = objBuyer.Id;
        
        update lstC;

    }
    public class jointBuyer {
        public Integer intIndex {get;set;}
        public Buyer__c objJointBuyer {get;set;}

        public jointBuyer(){

        }
    }
}