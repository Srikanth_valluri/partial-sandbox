public with sharing class CCAvenuePaymentGateway {

    // params
    @testVisible private static final String ENC_REQUEST            = 'encRequest';
    @testVisible private static final String ENC_RESP               = 'encResp';
    @testVisible private static final String ENC_RESPONSE           = 'enc_response';
    @testVisible private static final String STATUS                 = 'status';
    @testVisible private static final String TRANSACTION_ID         = 'tid';
    @testVisible private static final String MERCHANT_ID            = 'merchant_id';
    @testVisible private static final String ORDER_ID               = 'order_id';
    @testVisible private static final String ORDER_NO               = 'order_no';
    @testVisible private static final String AMOUNT                 = 'amount';
    @testVisible private static final String PARAM_CURRENCY         = 'currency';
    @testVisible private static final String BILLING_NAME           = 'billing_name';
    @testVisible private static final String BILLING_EMAIL          = 'billing_email';
    @testVisible private static final String BILLING_TEL            = 'billing_tel';
    @testVisible private static final String BILLING_ADDRESS        = 'billing_address';
    @testVisible private static final String BILLING_CITY           = 'billing_city';
    @testVisible private static final String BILLING_STATE          = 'billing_state';
    @testVisible private static final String BILLING_ZIP            = 'billing_zip';
    @testVisible private static final String BILLING_COUNTRY        = 'billing_country';
    @testVisible private static final String REDIRECT_URL           = 'redirect_url';
    @testVisible private static final String CANCEL_URL             = 'cancel_url';
    @testVisible private static final String MERCHANT_PARAM_1       = 'merchant_param1';
    @testVisible private static final String MERCHANT_PARAM_2       = 'merchant_param2';

    @testVisible private static final String SI_TYPE                = 'si_type';
    @testVisible private static final String SI_MER_REF_NO          = 'si_mer_ref_no';
    @testVisible private static final String SI_IS_SETUP_AMT        = 'si_is_setup_amt';
    @testVisible private static final String SI_START_DATE          = 'si_start_date';

    @testVisible private static final String SI_SUB_REF_NO          = 'si_sub_ref_no';
    @testVisible private static final String SI_MER_CHARGE_REF_NO   = 'si_mer_charge_ref_no';
    @testVisible private static final String SI_CURRENCY            = 'si_currency';
    @testVisible private static final String SI_AMOUNT              = 'si_amount';
    @testVisible private static final String CHARGE_SI   = 'chargeSI';

    private static final String INITIATE_TRANSACTION    = 'initiateTransaction';
    private static final String ORDER_STATUS_TRACKER    = 'orderStatusTracker';
    private static final String ACCESS_CODE             = 'access_code';
    private static final String REQUEST_TYPE            = 'request_type';
    private static final String RESPONSE_TYPE           = 'response_type';
    private static final String COMMAND                 = 'command';
    private static final String REFERENCE_NO            = 'reference_no';
    private static final String TYPE_JSON               = 'JSON';
    private static final String TYPE_STRING             = 'STRING';
    private static final String ENCRYPTION_ALGORITHM    = 'AES128';
    private static final String HASH_ALGORITHM          = 'MD5';
    private static final String INITIALIZATION_VECTOR   = '000102030405060708090a0b0c0d0e0f';

    private static String callbackPage = 'CustomerPaymentStatusUpdate';

    private static Map<String, CCAvenuePaymentGateway> instances = new Map<String, CCAvenuePaymentGateway>();

    private Integer counter = 0;
    private PaymentGateway__c gateway;


    private CCAvenuePaymentGateway(PaymentGateway__c gateway) {
        this.gateway = gateway;
    }

    /**
     * returns instance of class with the PaymentGateway__c record
     * @param  name Payment Gateway record name
     * @return      PaymentGateway__c record of the requested name
     */
    public static CCAvenuePaymentGateway getInstance(String name) {
        //  first check in the local map
        CCAvenuePaymentGateway instance = instances.get(name);
        // if not present in map, instantiate a new object and add to the map
        if (instance == null) {
            PaymentGateway__c gateway = PaymentGateway__c.getInstance(name);
            if (gateway != NULL) {
                instance = new CCAvenuePaymentGateway(gateway);
                instances.put(name, instance);
            }
        }
        return instance;
    }

    public String initiateTransaction(String orderId, Decimal amount, String baseUrl, Id accountId) {
        Account account = [
            SELECT  Id, Name, Address_Line_1__pc, City__pc, Country__pc,
                    IsPersonAccount, Email__pc, Email__c, Mobile_Phone_Encrypt__pc, Mobile__c
            FROM    Account
            WHERE   Id = :accountId
        ];
        return initiateTransaction(orderId, amount, baseUrl, account);
    }

    public String initiateTransaction(
        String orderId, Decimal amount, String baseUrl, Id accountId, String merchantParam1
    ) {
        Account account = [
            SELECT  Id, Name, Address_Line_1__pc, City__pc, Country__pc,
                    IsPersonAccount, Email__pc, Email__c, Mobile_Phone_Encrypt__pc, Mobile__c
            FROM    Account
            WHERE   Id = :accountId
        ];
        return initiateTransaction(orderId, amount, baseUrl, account, merchantParam1);
    }

    public String initiateTransaction(String orderId, Decimal amount, String baseUrl, Account account) {
        return initiateTransaction(orderId, amount, baseUrl, account, '');
    }

    public String initiateTransaction(
        String orderId, Decimal amount, String baseUrl, Account account, String merchantParam1
    ) {
        String country = DamacUtility.getCountryCode(account.Country__pc);

        Map<String, String> mapStates = new Map<String, String>();
        Map<String,String> mapPO = new Map<String, String>();
        for (NI_State_PO_values__mdt statePostalCode :[
                SELECT Id, DeveloperName, State__c, PO__c FROM NI_State_PO_values__mdt
        ]) {
            mapStates.put(statePostalCode.DeveloperName, statePostalCode.State__c);
            mapPO.put(statePostalCode.DeveloperName, statePostalCode.PO__c);
        }

        String countryCode, state, postalCode = '';
        if ('US'.equalsIgnoreCase(country) ||'CA'.equalsIgnoreCase(country)) {
            countryCode = country;
        } else {
            countryCode = 'Others';
        }

        state = mapStates.get(countryCode);
        postalCode = mapPO.get(countryCode);

        String email = '', phone = '';
        if (account.IsPersonAccount) {
            email = account.Email__pc;
            phone = account.Mobile_Phone_Encrypt__pc;
        } else {
            email = account.Email__c;
            phone = account.Mobile__c;
        }

        return initiateTransaction(
            orderId,
            amount,
            baseUrl,
            (String.isBlank(account.Name) ? '' : account.Name),
            (String.isBlank(email) ? '' : email),
            (String.isBlank(phone) ? '' : phone),
            (String.isBlank(account.Address_Line_1__pc) ? '' : account.Address_Line_1__pc),
            (String.isBlank(account.City__pc) ? '' : account.City__pc),
            (String.isBlank(state) ? '' : state),
            (String.isBlank(postalCode) ? '' : postalCode),
            (String.isBlank(country) ? '' : country),
            merchantParam1
        );
    }

    public String initiateTransaction(
        String orderId, Decimal amount, String redirectUrl, String name,
        String address, String city, String state, String zip, String country
    ) {
        return initiateTransaction(
            orderId, amount, redirectUrl, name, '', '', address, city, state, zip, country
        );
    }

    public String initiateTransaction(
        String orderId, Decimal amount, String redirectUrl, String name, String email, String phone,
        String address, String city, String state, String zip, String country
    ) {
        return initiateTransaction(
            orderId, amount, redirectUrl, name, email, phone, address, city, state, zip, country, ''
        );
    }

    public String initiateTransaction(
        String orderId, Decimal amount, String redirectUrl, String name, String email, String phone,
        String address, String city, String state, String zip, String country, String merchantParam1
    ) {
        return initiateTransaction(
            orderId, amount, redirectUrl, name, email, phone, address, city, state, zip, country, merchantParam1, ''
        );
    }

    public String initiateTransaction(
        String orderId, Decimal amount, String redirectUrl, String name, String email, String phone,
        String address, String city, String state, String zip, String country, String merchantParam1,
        String merchantParam2
    ) {
        //String baseSecureUrl = Site.getBaseSecureUrl();

        /*orderId = 'FM-RECEIPT-00000728';
        amount = 4631.08;
        name = 'Umkalthoum Mubarak Yaslam Sumaida';
        email = 'altemimi00@gmail.com';
        phone = '00971559955800';
        address = 'Mohamed Bin Zayed City';
        city = 'Abu Dhabi';
        state = 'NA';
        zip = '0';
        country = 'AE';
        redirectUrl = 'https://customers.damacproperties.com/community/CommunityPaymentStatus?retUrl=/community';
        merchantParam1 = 'FM Portal Service Charge';*/

        String request = 'tid='
                + DateTime.now().getTime() + '' + (counter++) + '' + Integer.valueOf((Math.random() * 10000));
        request += '&'+ CCAvenuePaymentGateway.MERCHANT_ID + '=' + gateway.MerchantId__c;
        request += '&'+ CCAvenuePaymentGateway.ORDER_ID         + '=' + orderId;
        request += '&'+ CCAvenuePaymentGateway.AMOUNT           + '=' + amount;
        request += '&'+ CCAvenuePaymentGateway.PARAM_CURRENCY   + '=' + 'AED';
        request += '&'+ CCAvenuePaymentGateway.BILLING_NAME     + '=' + name;
        request += '&'+ CCAvenuePaymentGateway.BILLING_EMAIL    + '=' + email;
        request += '&'+ CCAvenuePaymentGateway.BILLING_TEL      + '=' + phone;
        request += '&'+ CCAvenuePaymentGateway.BILLING_ADDRESS  + '=' + address;
        request += '&'+ CCAvenuePaymentGateway.BILLING_CITY     + '=' + city;
        request += '&'+ CCAvenuePaymentGateway.BILLING_STATE    + '=' + state;
        request += '&'+ CCAvenuePaymentGateway.BILLING_ZIP      + '=' + zip;
        request += '&'+ CCAvenuePaymentGateway.BILLING_COUNTRY  + '=' + country;
        request += '&'+ CCAvenuePaymentGateway.REDIRECT_URL     + '=' + EncodingUtil.urlEncode(redirectUrl, 'UTF-8');
        request += '&'+ CCAvenuePaymentGateway.CANCEL_URL       + '=' + EncodingUtil.urlEncode(redirectUrl, 'UTF-8');
        request += '&'+ CCAvenuePaymentGateway.MERCHANT_PARAM_1 + '=' + merchantParam1;
        request += '&'+ CCAvenuePaymentGateway.MERCHANT_PARAM_2 + '=' + merchantParam2;

        System.debug('request = ' + request);
        String enc_request = encrypt(request);

        Map<String, String> params = new Map<String, String>();
        params.put(CCAvenuePaymentGateway.COMMAND, INITIATE_TRANSACTION);
        params.put(CCAvenuePaymentGateway.ENC_REQUEST, enc_request);
        params.put(CCAvenuePaymentGateway.ACCESS_CODE, gateway.AccessCode__c);

        return generatePaymentUrl(params);
    }

    public String trackOrderStatus(String orderNo) {
        String request = '';
        if (String.isNotBlank(orderNo)) {
            request += CCAvenuePaymentGateway.ORDER_NO + '=' + orderNo;
        }
        request = JSON.serialize(new Map<String, String> {
            CCAvenuePaymentGateway.ORDER_NO => orderNo
        });

        System.debug('request = ' + request);

        String enc_request = EncodingUtil.convertToHex(
            Crypto.encrypt(
                ENCRYPTION_ALGORITHM,
                Crypto.generateDigest(HASH_ALGORITHM, Blob.valueOf(gateway.EncryptionKey__c)),
                EncodingUtil.convertFromHex(INITIALIZATION_VECTOR),
                Blob.valueOf(request)
            )
        );

        Map<String, String> params = new Map<String, String>();
        params.put(CCAvenuePaymentGateway.COMMAND, ORDER_STATUS_TRACKER);
        params.put('enc_request', enc_request);
        params.put(CCAvenuePaymentGateway.ACCESS_CODE, gateway.AccessCode__c);
        params.put(CCAvenuePaymentGateway.REQUEST_TYPE, CCAvenuePaymentGateway.TYPE_JSON);
        params.put(CCAvenuePaymentGateway.RESPONSE_TYPE, CCAvenuePaymentGateway.TYPE_JSON);
        params.put('version', '1.1');

        String paymentStatusUrl = generatePaymentUrl(Label.CCAvenueStatusAPIURL, params);

        HttpRequest req = new HttpRequest();
        req.setEndPoint(paymentStatusUrl);
        req.setMethod('POST');
        HttpResponse res = new Http().send(req);
        System.debug('response code = ' + res.getStatusCode());
        System.debug('response body = ' + res.getBody());

        Map<String, String> responseParams = new Map<String, String>();

        for (String urlParam : res.getBody().split('&')) {
            System.debug('urlParam = ' + urlParam);
            List<String> param = urlParam.split('=');
            System.debug('param = ' + param);
            if (param.size() > 1) {
                responseParams.put(param[0], param[1]);
            }
        }
        System.debug('responseParams = ' + responseParams);

        String responseStatus = responseParams.get(STATUS);
        String encryptedResponse = responseParams.get(ENC_RESPONSE).trim();

        if (responseStatus != '0') {
            return encryptedResponse;
        }

        String responseBody = Crypto.decrypt(
            ENCRYPTION_ALGORITHM,
            Crypto.generateDigest(HASH_ALGORITHM, Blob.valueOf(gateway.EncryptionKey__c)),
            EncodingUtil.convertFromHex(INITIALIZATION_VECTOR),
            EncodingUtil.convertFromHex(encryptedResponse)
        ).toString();

        System.debug('responseBody = ' + responseBody);

        return responseBody;
    }

    /*public String createSI(
        String orderId, Decimal amount, String redirectUrl, BillingDetails billingDetails, String merchantParam1,
        SIConfig siConfig
    ) {
        //String baseSecureUrl = Site.getBaseSecureUrl();
        String request ='tid='
                + DateTime.now().getTime() + '' + (counter++) + '' + Integer.valueOf((Math.random() * 10000));
        request +=  '&'+ CCAvenuePaymentGateway.MERCHANT_ID + '=' + gateway.MerchantId__c;
        request +=  '&'+ CCAvenuePaymentGateway.ORDER_ID         + '=' + orderId;
        request +=  '&'+ CCAvenuePaymentGateway.AMOUNT           + '=' + amount;
        request +=  '&'+ CCAvenuePaymentGateway.PARAM_CURRENCY   + '=' + 'AED';
        request +=  '&'+ CCAvenuePaymentGateway.BILLING_NAME     + '=' + billingDetails.name;
        request +=  '&'+ CCAvenuePaymentGateway.BILLING_EMAIL    + '=' + billingDetails.email;
        request +=  '&'+ CCAvenuePaymentGateway.BILLING_TEL      + '=' + billingDetails.phone;
        request +=  '&'+ CCAvenuePaymentGateway.BILLING_ADDRESS  + '=' + billingDetails.address;
        request +=  '&'+ CCAvenuePaymentGateway.BILLING_CITY     + '=' + billingDetails.city;
        request +=  '&'+ CCAvenuePaymentGateway.BILLING_STATE    + '=' + billingDetails.state;
        request +=  '&'+ CCAvenuePaymentGateway.BILLING_ZIP      + '=' + billingDetails.zip;
        request +=  '&'+ CCAvenuePaymentGateway.BILLING_COUNTRY  + '=' + billingDetails.country;
        request +=  '&'+ CCAvenuePaymentGateway.REDIRECT_URL     + '=' + EncodingUtil.urlEncode(redirectUrl, 'UTF-8');
        request +=  '&'+ CCAvenuePaymentGateway.CANCEL_URL       + '=' + EncodingUtil.urlEncode(redirectUrl, 'UTF-8');
        request +=  '&'+ CCAvenuePaymentGateway.MERCHANT_PARAM_1 + '=' + merchantParam1;

        request +=  '&'+ CCAvenuePaymentGateway.SI_TYPE         + '=' + siConfig.si_type;
        request +=  '&'+ CCAvenuePaymentGateway.SI_IS_SETUP_AMT + '=' + siConfig.si_is_setup_amt;
        request +=  '&'+ CCAvenuePaymentGateway.SI_START_DATE   + '=' + siConfig.si_start_date;
        request +=  String.isBlank(siConfig.si_mer_ref_no) ? ''
                :   '&'+ CCAvenuePaymentGateway.SI_MER_REF_NO   + '=' + siConfig.si_mer_ref_no;

        System.debug('request = ' + request);
        //throw new LoamsCommunityException(request);
        String enc_request = encrypt(request);

        Map<String, String> params = new Map<String, String>();
        params.put(CCAvenuePaymentGateway.COMMAND, INITIATE_TRANSACTION);
        params.put(CCAvenuePaymentGateway.ENC_REQUEST, enc_request);
        params.put(CCAvenuePaymentGateway.ACCESS_CODE, gateway.AccessCode__c);

        return generatePaymentUrl(params);
    }

    public HttpRequest chargeSI(SIChargeRequest siChargeRequest) {
        siChargeRequest.si_amount = siChargeRequest.si_amount.setScale(2);

        HttpRequest req = new HttpRequest();

        String request ='';

        request = JSON.serialize(siChargeRequest);

        //request = siChargeRequest.si_sub_ref_no + '|' + siChargeRequest.si_mer_charge_ref_no + '|' + siChargeRequest.si_currency + '|' + siChargeRequest.si_amount + '|';

        //request = '{\n\t"si_sub_ref_no":"' + siChargeRequest.si_sub_ref_no + '",\n\t"si_mer_charge_ref_no":"' + 'SI-' + siChargeRequest.si_mer_charge_ref_no + '",\n\t"si_amount":' + siChargeRequest.si_amount + ',\n\t"si_currency":"' + siChargeRequest.si_currency + '"\n}';


        System.debug('request = ' + request);
        //throw new LoamsCommunityException(request);
        String enc_request = EncodingUtil.convertToHex(
            Crypto.encrypt(
                ENCRYPTION_ALGORITHM,
                Crypto.generateDigest(HASH_ALGORITHM, Blob.valueOf(gateway.EncryptionKey__c)),
                EncodingUtil.convertFromHex(INITIALIZATION_VECTOR),
                Blob.valueOf(request)
            )
        );
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, request));

        Map<String, String> params = new Map<String, String>();
        params.put(CCAvenuePaymentGateway.COMMAND, CHARGE_SI);
        params.put('enc_request', enc_request);
        params.put(CCAvenuePaymentGateway.ACCESS_CODE, gateway.AccessCode__c);
        params.put(CCAvenuePaymentGateway.REQUEST_TYPE, TYPE_JSON);
        params.put(CCAvenuePaymentGateway.RESPONSE_TYPE, TYPE_JSON);
        params.put('version','1.1');

        String urlParams = '?';
        for (String key : params.keySet()) {
            urlParams += key + '=' + params.get(key) + '&';
        }
        urlParams = urlParams.removeEnd('&');
        System.debug('final Request URL = ' + Label.CCAvenueSIChargeUrl + urlParams);
        req.setEndpoint(Label.CCAvenueSIChargeUrl + urlParams);
        //ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, decryptWithManagedIV(enc_request)));
        //System.debug('decrypted request = ' + decryptWithManagedIV(enc_request));
        //System.debug('decrypted request = ' + decryptWithManagedIV('64e9c64e23342bc9e6db4645d14d500346a28ed7f29b68651fd80e09235c22f63eb82e7504e8ad29b69a467f6a0ad0a62616f845a3797ce0a7b9570a59ef7db703069d6a3853f185de892b382a970beecc7db97f0035a07b0d0dfa16e610cf155c60072d108eff4ca21cfb5e4b2a6ec6c0a55dd66cb8ef130a1958f33313964c54192caa579c8f08d7c101bf7cc80070'));
        req.setBody(enc_request);
        return req;
    }*/

    public Map<String, String> getDecryptedResponse(Map<String, String> params) {
        if (params == NULL) {
            return params;
        }
        String encResp = params.remove(ENC_RESP);
        if (String.isBlank(encResp)) {
            return params;
        }
        String response = decrypt(encResp);
        for (String urlParam : response.split('&')) {
            List<String> param = urlParam.split('=');
            if (param.size() > 1) {
                params.put(param[0], param[1]);
            }
        }
        System.debug('params = ' + JSON.serialize(params));
        return params;
    }

    private String generatePaymentUrl(Map<String, String> params) {
        String urlParams = '?';
        for (String key : params.keySet()) {
            urlParams += key + '=' + params.get(key) + '&';
        }
        urlParams = urlParams.removeEnd('&');
        System.debug('final Request URL = ' + gateway.Url__c + urlParams);
        return gateway.Url__c + urlParams;
    }

    private String generatePaymentUrl(String baseUrl, Map<String, String> params) {
        String urlParams = '?';
        for (String key : params.keySet()) {
            urlParams += key + '=' + params.get(key) + '&';
        }
        urlParams = urlParams.removeEnd('&');
        System.debug('final Request URL = ' + baseUrl + urlParams);
        return baseUrl + urlParams;
    }

    public String encrypt(String clearText) {
        return EncodingUtil.convertToHex(
            Crypto.encrypt(
                ENCRYPTION_ALGORITHM,
                Crypto.generateDigest(HASH_ALGORITHM, Blob.valueOf(gateway.EncryptionKey__c)),
                EncodingUtil.convertFromHex(INITIALIZATION_VECTOR),
                Blob.valueOf(clearText)
            )
        );
    }

    public String decrypt(String cipherText) {
        return Crypto.decrypt(
            ENCRYPTION_ALGORITHM,
            Crypto.generateDigest(HASH_ALGORITHM, Blob.valueOf(gateway.EncryptionKey__c)),
            EncodingUtil.convertFromHex(INITIALIZATION_VECTOR),
            EncodingUtil.convertFromHex(cipherText)
        ).toString();
    }

    /*public String encryptWithManagedIV(String clearText) {
        return EncodingUtil.convertToHex(
            Crypto.encryptWithManagedIV(
                ENCRYPTION_ALGORITHM,
                Crypto.generateDigest(HASH_ALGORITHM, Blob.valueOf(gateway.EncryptionKey__c)),
                Blob.valueOf(clearText)
            )
        );
    }*/

    /*public String decryptWithManagedIV(String cipherText) {
        return Crypto.decryptWithManagedIV(
            ENCRYPTION_ALGORITHM,
            Crypto.generateDigest(HASH_ALGORITHM, Blob.valueOf(gateway.EncryptionKey__c)),
            EncodingUtil.convertFromHex(cipherText)
        ).toString();
    }*/

    public class OrderStatusResult {
        public String reference_no; //107007069210
        public String order_no; //500119294
        public String order_currncy;    //AED
        public Decimal order_amt;   //259
        public String order_date_time;  //2018-12-05 13:54:24.87
        public String order_bill_name;  //Dubai
        public String order_bill_address;   //nasir al mansoori building behind Dubai driving institute
        public String order_bill_zip;   //
        public String order_bill_tel;   //971548765432
        public String order_bill_email; //siddiqui.208@gmail.com
        public String order_bill_country;   //United Arab Emirates
        public String order_ship_name;  //Dubai
        public String order_ship_address;   //nasir al mansoori building behind Dubai driving institute
        public String order_ship_country;   //United Arab Emirates
        public String order_ship_tel;   //
        public String order_bill_city;  //Dubai
        public String order_bill_state; //
        public String order_ship_city;  //Dubai
        public String order_ship_state; //
        public String order_ship_zip;   //
        public String order_ship_email; //
        public String order_notes;  //
        public String order_ip; //14.141.13.37
        public String order_status; //Shipped
        public String order_fraud_status;   //NA
        public String order_status_date_time;   //2018-12-05 13:55:16.257
        public Decimal order_capt_amt;  //259
        public String order_card_name;  //MasterCard
        public String order_delivery_details;   //
        public Decimal order_fee_perc;  //3
        public Double order_fee_perc_value; //7.77
        public Decimal order_fee_flat;  //1
        public Decimal order_gross_amt; //259
        public Decimal order_discount;  //0
        public Double order_tax;    //0.4385
        public String order_bank_ref_no;    //406932
        public String order_gtw_id; //MSH
        public String order_bank_response;  //Approved
        public String order_option_type;    //OPTCRDC
        public Decimal order_TDS;   //0
        public String order_device_type;    //PC
        public String order_receipt_no; //833920406932
        public String order_bank_mid;   //TEST800206
        public String order_bank_qsi_no;    //2090010284
        public String eci_value;    //05
        public String order_card_type;  //CRDC
        public String card_no;  //5123452346
        public String error_desc;   //
        public String status;  //0
        public String error_code;   //
    }

    /*public class BillingDetails {
        public String name      {get; set;}
        public String email     {get; set;}
        public String phone     {get; set;}
        public String address   {get; set;}
        public String city      {get; set;}
        public String state     {get; set;}
        public String zip       {get; set;}
        public String country   {get; set;}
    }

    public class SIConfig {
        public String si_type           {get; set;}
        public String si_mer_ref_no     {get; set;}
        public String si_is_setup_amt   {get; set;}
        public String si_start_date     {get; set;}
    }

    public class SIChargeRequest {
        public String   si_sub_ref_no           {get; set;} //'SI1504020001109',
        public String   si_mer_charge_ref_no    {get; set;} //'ABCD1234',
        public Decimal  si_amount               {get; set;} //200.00,
        public String   si_currency             {get; set;} //'INR'
    }*/

}