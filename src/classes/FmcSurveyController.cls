public without sharing class FmcSurveyController {
    public List<SurveyQuestion> allQuestions    {get; set;}
    public String userId                        {get; set;}
    public String userName                      {get; set;}
    public String  surveyThankYouText           {get; set;}
    public Boolean thankYouRendered             {get; set;}
    public Boolean isError                      {get; set;}
    public Survey_CRM__c survey                 {get; set;}
    public Boolean isPortalSurvey               {get; set;}
    public Boolean isPortalExitSurvey           {get; set;}
    public Boolean canSkipSurvey                {get; set;}
    public Survey_Taken_CRM__c surveyTaken      {get; set;}
    public String customer_taking_Survey        {get; set;}
    public String bookingUnitName               {get;set;}
    public Boolean hasMessages                  { get {
                                                    return ApexPages.hasMessages();
                                                }}


    public FmcSurveyController() {
        isPortalSurvey = false;
        String surveyType = ApexPages.currentPage().getParameters().get('type');
        if(surveyType != null && (surveyType.equalsIgnoreCase('Portal - LOAMS') || surveyType.equalsIgnoreCase('PortalExitSurvey'))){
            isPortalSurvey = true;
            isPortalExitSurvey = false;
        }else{
            surveyType = 'Walk-In';
        }

        if(surveyType != null && surveyType.equalsIgnoreCase('PortalExitSurvey')){
            isPortalExitSurvey = true;
        }

        List<Survey_CRM__c> surveyList = [Select Id,Name,Hide_Survey_Name__c,Survey_Header__c,Thank_You_Text__c,Description__c from Survey_CRM__c
                                      where Is_Active__c = True and Name = 'FM Portal Survey' AND Type__c =: surveyType and End_Date__c > :System.Today() and Start_Date__c < :System.Today()];

        system.debug('surveyList'+surveyList);
        surveyTaken = new Survey_Taken_CRM__c();

        allQuestions = new List<SurveyQuestion>();

        if(surveyList != null && surveyList.size()>0){


            survey = surveyList[0];
            system.debug('survey'+survey);
            // Retrieve all necessary information to be displayed on the page
            getQuestion();

            userId = UserInfo.getUserId();
            userName = UserInfo.getName();

            thankYouRendered = false;
            isError = false;
        }

        List<Account> lstAccount = [SELECT  Id,
                                            CanSkipSurvey__c
                                    FROM    Account
                                    WHERE   Id = :CustomerCommunityUtils.customerAccountId];
        //canSkipSurvey = !lstAccount.isEmpty() && lstAccount[0].CanSkipSurvey__c;
        canSkipSurvey = true;

    }

    // JS Remoting action called when searching for a booking unit name
    @RemoteAction
    public static List<Booking_Unit__c> searchBookingUnit(String searchTerm) {
        System.debug('Booking unit Name is: '+searchTerm );
        List<Booking_Unit__c> listBookingUnit = Database.query('Select Id, Unit_Name__c from Booking_Unit__c where Unit_Name__c like \'%' + String.escapeSingleQuotes(searchTerm) + '%\'');
        return listBookingUnit;
    }


    public List<SurveyQuestion> getQuestion() {

        List<Survey_Question_CRM__c> allQuestionsObject = [Select s.Type__c, s.Id, s.Survey__c, s.Required__c, s.Question__c, s.Order_Number_Displayed__c,
                                                       s.OrderNumber__c, s.Name, s.Choices__c,Choice_for_Additional_Textbox__c,Survey_Question__c,Additional_Description__c, Arabic_Question__c,Arabic_Order_Number_Displayed__c
                                                       From Survey_Question_CRM__c s
                                                       WHERE s.Survey__c =: survey.Id ORDER BY s.OrderNumber__c];
        allQuestions = new List<SurveyQuestion>();
        List<Survey_Question_CRM__c> parentQuestions = new List<Survey_Question_CRM__c>();
        Map<Id,List<Survey_Question_CRM__c>> mapParentIdToChildQuestions = new Map<Id,List<Survey_Question_CRM__c>>();
        for (Survey_Question_CRM__c q : allQuestionsObject){
            if(q.Survey_Question__c == null){
                parentQuestions.add(q);
            }else{
                if(mapParentIdToChildQuestions.get(q.Survey_Question__c) == null){
                    mapParentIdToChildQuestions.put(q.Survey_Question__c,new List<Survey_Question_CRM__c>());
                }
                mapParentIdToChildQuestions.get(q.Survey_Question__c).add(q);
            }
        }

        for (Survey_Question_CRM__c q : parentQuestions){

            List<SurveyQuestion> subQuestionWrapper = new List<SurveyQuestion>();
            List<Survey_Question_CRM__c> subQuestions = mapParentIdToChildQuestions.get(q.Id);
            if(subQuestions != null && subQuestions.size() >0){
                for (Survey_Question_CRM__c question : subQuestions){
                    subQuestionWrapper.add(new SurveyQuestion(question,NULL));
                }
            }

            SurveyQuestion theQ = new SurveyQuestion(q,subQuestionWrapper);
            allQuestions.add(theQ);
        }
        return allQuestions;
    }

    public void checkLoamsQuestions(){
        allQuestions = new List<SurveyQuestion>();
        List<Survey_CRM__c> surveyList = new List<Survey_CRM__c>();
        if(surveyTaken.Purpose_of_Visit__c != null && surveyTaken.Purpose_of_Visit__c.equalsIgnoreCase('LOAMS')){
            surveyList = [Select Id,Name,Hide_Survey_Name__c,Survey_Header__c,Thank_You_Text__c,Description__c
                          from Survey_CRM__c where Is_Active__c = True and Type__c = 'Portal - LOAMS' and End_Date__c > :System.Today() and Start_Date__c < :System.Today()
                          ORDER BY CreatedDate ASC];
        } else {
            surveyList = [Select Id,Name,Hide_Survey_Name__c,Survey_Header__c,Thank_You_Text__c,Description__c
                          from Survey_CRM__c where Is_Active__c = True and Type__c = 'Portal - LOAMS' and End_Date__c > :System.Today() and Start_Date__c < :System.Today()];
        }
        system.debug('surveyList2'+surveyList);
        if(surveyList != null && surveyList.size()>0){


            survey = surveyList[0];
            system.debug('survey2'+survey);
            allQuestions = new List<SurveyQuestion>();
            getQuestion();
        }
    }

    public List<Booking_Unit__c> listBookingUnit;

    public pageReference populateCustomerDetails(){
        system.debug(' surveyTaken.Booking_Unit__c  :  ' + surveyTaken.Booking_Unit__c );
        listBookingUnit = [ Select Id , Inventory__r.Property__r.Name
                               , Inventory__r.Property__c
                               , Booking__r.Account__c
                               , Booking__r.Account__r.Phone
                               , Booking__r.Account__r.Name
                            from Booking_Unit__c where Id = : surveyTaken.Booking_Unit__c];
         System.debug('listBookingUnit >>> ' + listBookingUnit);
         System.debug('listBookingUnit[0].Inventory__r.Property__c >>> ' + listBookingUnit[0].Inventory__r.Property__r.Name);
         System.debug('listBookingUnit[0].Booking__r.Account__c >>> ' + listBookingUnit[0].Booking__r.Account__c);
         System.debug('listBookingUnit[0].Booking__r.Account__r.Phone >>> ' + listBookingUnit[0].Booking__r.Account__r.Phone);
         System.debug('listBookingUnit[0].Booking__r.Account__r.Name >>> ' + listBookingUnit[0].Booking__r.Account__r.Name);

        if(listBookingUnit != null && listBookingUnit.size() > 0){
            if(surveyTaken.Property__c == null){
                surveyTaken.Property__c = listBookingUnit[0].Inventory__r.Property__c;
            }
            if( String.isBlank( surveyTaken.Property_Name__c ) ){
                surveyTaken.Property_Name__c = listBookingUnit[0].Inventory__r.Property__r.Name;
            }

            if( surveyTaken.Customer_taking_Survey__c == null ){
                surveyTaken.Customer_taking_Survey__c = listBookingUnit[0].Booking__r.Account__c;
            }
            if( String.isBlank( surveyTaken.Customer_Name__c ) ){
                surveyTaken.Customer_Name__c = listBookingUnit[0].Booking__r.Account__r.Name;
            }
            if(surveyTaken.Phone__c == null){
                surveyTaken.Phone__c = listBookingUnit[0].Booking__r.Account__r.Phone;
            }
        }
        System.debug('surveyTaken.Customer_Name__c >>> ' + surveyTaken.Customer_Name__c);
        System.debug('surveyTaken.Customer_taking_Survey__c >>> ' + surveyTaken.Customer_taking_Survey__c);
        return null;
    }

    public PageReference skipSurvey() {
        if(isPortalExitSurvey){
            PageReference pageRef = new PageReference('/secur/logout.jsp');
            pageRef.setRedirect(true);
            return pageRef;
        }
        update new Account(
            Id = CustomerCommunityUtils.customerAccountId,
            Survey_skipped_date__c = DateTime.now()
        );
        return new PageReference(Site.getPathPrefix());
    }

    public String getStrQuestions() {
        Map<Id, String> mapQuestion = new Map<Id, String>();
        for (SurveyQuestion question : allQuestions) {
            mapQuestion.put(question.Id, question.selectedOption);
        }
        return JSON.serialize(mapQuestion);
    }

    public void submitResults() {
        System.debug('inside submit::::::::##');
        try {
            List <Survey_Question_Response_CRM__c> sqrList = new List<Survey_Question_Response_CRM__c>();

            List<SurveyQuestion> allCombinedQuestions = new List<SurveyQuestion>();
            allCombinedQuestions.addAll(allQuestions);
            System.debug('allQuestions::::'+allQuestions);
            for (SurveyQuestion question : allQuestions) {
               System.debug('question.isSubQuestion:::'+question.isSubQuestion);
                if(question.isSubQuestion){
                    allCombinedQuestions.addAll(question.subQuestions);
                }
            }
            isError = false;
            System.debug('allCombinedQuestions:::'+allCombinedQuestions);
            for (SurveyQuestion q : allCombinedQuestions) {
                System.debug('q.surveyQuestion.Type__c:::'+q.surveyQuestion.Type__c);
                if(q.surveyQuestion.Type__c != 'Section'){
                    Survey_Question_Response_CRM__c sqr = new Survey_Question_Response_CRM__c();
                    if(q.choiceForAdditionalTextbox != null && q.choiceForAdditionalTextbox != ''
                        && q.additionalResponse != ''){
                            sqr.Additional_Response__c = q.additionalResponse;
                    }
                    if (q.renderSelectRadio) {
                        if (q.required && (q.selectedOption == null || q.selectedOption == '')) {
                            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill out all required fields marked with asterisk'));
                            isError = true;
                            return;
                        }

                        if (q.selectedOption == null || q.selectedOption == '') {
                            sqr.Response__c = '';
                        }
                        else {
                            sqr.Response__c = q.singleOptions.get(Integer.valueOf(q.selectedOption)).getLabel();
                        }
                        sqr.Survey_Question__c = q.Id;
                        System.debug('sqr@@@@@@'+sqr);
                        sqrList.add(sqr);
                    }
                    else if (q.renderRatings) {
                        system.debug('>>>>>>Inner q.selectedOption : ' + q.selectedOption);
                        if (q.required && (q.selectedOption == null || q.selectedOption == '')) {
                            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill out all required fields marked with asterisk'));
                            isError = true;
                            return;
                        }
                        if (q.selectedOption == null || q.selectedOption == '') {
                            sqr.Response__c = '';
                        }
                        else {
                            if (q.selectedOption == null || q.selectedOption == '') {
                                sqr.Response__c = '';
                            }
                            else {
                                sqr.Response__c = q.singleOptions.get(Integer.valueOf(q.selectedOption)+1).getLabel();
                            }
                        }
                        sqr.Survey_Question__c = q.Id;
                        sqrList.add(sqr);
                    } else if (q.renderPicklist) {

                        if (q.required && (q.selectedOption == null || q.selectedOption == '')) {
                            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill out all required fields marked with asterisk'));
                            isError = true;
                            return;
                        }

                        if (q.selectedOption == null || q.selectedOption == '') {
                            sqr.Response__c = '';
                        } else {
                            sqr.Response__c = q.singleOptions.get(Integer.valueOf(q.selectedOption)+1).getLabel();
                              System.debug(' sqr.  sqr.Response__c::renderPicklist:jhol:'+sqr.Response__c);

                         }
                        sqr.Survey_Question__c = q.Id;
                        sqrList.add(sqr);
                    } else if (q.renderFreeText) {
                        if (q.required && q.choices == '') {
                            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill out all required fields marked with asterisk'));
                            isError = true;
                            return;
                        }

                        sqr.Response__c = q.choices;
                        sqr.Survey_Question__c = q.Id;
                        sqrList.add(sqr);
                    } else if (q.renderSelectCheckboxes) {
                        if (q.required && (q.selectedOptions == null || q.selectedOptions.size() == 0)) {
                            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill out all required fields marked with asterisk'));
                            isError = true;
                            return;
                        }
                        List<String>checkBoxLst = new List<String>();
                        for (String opt : q.selectedOptions) {
                            sqr = new Survey_Question_Response_CRM__c();
                            if (opt == '' || opt == null) {
                                sqr.Response__c = '';
                            }
                            else {
                                sqr.Response__c = q.multiOptions.get(Integer.valueOf(opt)).getLabel();
                               // String optionInst =  q.multiOptions.get(Integer.valueOf(opt)).getLabel();
                                checkBoxLst.add(sqr.Response__c);
                              }
                            sqr.Response__c  = '';
                            for(String checkboxInst : checkBoxLst){

                                sqr.Response__c += checkboxInst + ',';
                            }
                            sqr.Response__c = sqr.Response__c.removeEnd(',');
                            sqr.Survey_Question__c = q.Id;
                        }
                        sqrList.add(sqr);
                    }
                    else if (q.renderSelectRow) {
                        if (q.required && (q.selectedOption == null || q.selectedOption == '')) {
                            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill out all required fields marked with asterisk'));
                            isError = true;
                            return;
                        }

                        if (q.selectedOption == null || q.selectedOption == '') {
                            sqr.Response__c = '';
                        }
                        else {
                            sqr.Response__c = q.rowOptions.get(Integer.valueOf(q.selectedOption)).getLabel();
                        }

                        sqr.Survey_Question__c = q.Id;
                        sqrList.add(sqr);
                    }
                    system.debug('>>>>>>^************^sqr : '+sqr);
                    system.debug('>>>>>>&&&&&&&&&&&&&&sqr.Response__c : '+sqr.Response__c);
                    system.debug('>>>>>^^^^^^^^^^^^^^^>q.Id : '+q.Id);

                    if (q.choiceForAdditionalTextbox != NULL) {
                    }

                    if (q.choiceForAdditionalTextbox != NULL
                        && String.isBlank(sqr.Additional_Response__c)) {
                        List<String> optionList = q.choiceForAdditionalTextbox.split(';');
                        for (String option : optionList) {
                            if (option.equalsIgnoreCase(sqr.Response__c)) {
                                Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill out all required fields marked with asterisk'));
                                isError = true;
                                return;
                            }
                        }
                    }
                }
            }
            if (CustomerCommunityUtils.customerAccountId != NULL) {
                surveyTaken.Customer_taking_Survey__c = CustomerCommunityUtils.customerAccountId;
            }
            if(isPortalSurvey) {
                surveyTaken.User_taking_Survey__c = UserInfo.getUserId();
            }
            else {
                system.debug(' isError : ' + isError );
                system.debug(' surveyTaken.Booking_Unit__c : ' + surveyTaken.Booking_Unit__c );

                surveyTaken.Customer_taking_Survey__c = listBookingUnit[0].Booking__r.Account__c;
                surveyTaken.Property__c = listBookingUnit[0].Inventory__r.Property__c;

                if ( surveyTaken.Booking_Unit__c == null  ) {
                    Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill out all required fields marked with asterisk'));
                    isError = true;
                    return;
                }

                if ( String.isBlank( surveyTaken.Customer_Name__c )  ) {
                    Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill out all required fields marked with asterisk'));
                    isError = true;
                    return;
                }

                if ( String.isBlank( surveyTaken.Property_Name__c )   ) {
                    Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill out all required fields marked with asterisk'));
                    isError = true;
                    return;
                }

                if ( surveyTaken.Phone__c == null  ) {
                    Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill out all required fields marked with asterisk'));
                    isError = true;
                    return;
                }
                if( surveyTaken.Purpose_of_Visit__c == null ) {
                    Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill out all required fields marked with asterisk'));
                    isError = true;
                    return;
                }
            }
            surveyTaken.Survey__c = survey.Id;
            insert surveyTaken;

            for (Survey_Question_Response_CRM__c sqr : sqrList)
            {
                sqr.Survey_Taker__c = surveyTaken.Id;
            }
            insert sqrList;
            isError = false;
            thankYouRendered=true;


        }catch(Exception e){
            System.debug('Exception: ' + e.getMessage() + e.getStackTraceString() );
            isError = true;
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Some error occured while saving response'));
        }

    }
}