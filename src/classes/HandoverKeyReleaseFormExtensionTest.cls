/**
 * @File Name          : HandoverKeyReleaseFormExtensionTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 10/31/2019, 11:41:51 AM
 * @Modification Log   : 
 * Ver       Date            Author              Modification
 * 1.0    10/6/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
private class HandoverKeyReleaseFormExtensionTest {
    
    @testSetup
    static void setup() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId() );
        objCase.OwnerId = UserInfo.getUserId();
        insert objCase ;
        
        NSIBPM__Service_Request__c objReq = TestDataFactory_CRM.createServiceRequest();
        insert objReq ;
        
        List<Booking__c> lstBooking =  TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objReq.Id,1 );
        insert lstBooking ;
        
        List<Booking_Unit__c> lstUnits = TestDataFactory_CRM.createBookingUnits( lstBooking, 1 );
        insert lstUnits ;
        
        SR_Attachments__c objAttach = TestDataFactory_CRM.createCaseDocument( null, 'Key Release Form' );
        objAttach.Booking_unit__c = lstUnits[0].Id ;
        insert objAttach ;
        
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
    }
    
    static testMethod void testConstructor() {
        list<Booking_Unit__c> lstUnits = [ SELECT Id FROM Booking_Unit__c LIMIT 1 ];
     
        ApexPages.StandardController stdController = new ApexPages.StandardController( lstUnits[0] )  ;
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        test.startTest();
            HandoverKeyReleaseFormExtension objServ = new HandoverKeyReleaseFormExtension( stdController );
        test.stopTest();   
    }
    
    static testMethod void testSave1() {
      list<Booking_Unit__c> lstUnits = [ SELECT Id FROM Booking_Unit__c LIMIT 1 ];
     
        ApexPages.StandardController stdController = new ApexPages.StandardController( lstUnits[0] )  ;
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockRPTermination(1) );
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        test.startTest();
            HandoverKeyReleaseFormExtension objServ = new HandoverKeyReleaseFormExtension( stdController );
          objServ.keyDocBody = EncodingUtil.base64Encode(Blob.valueOf('Test'));
            objServ.keyDocName = 'Tony.txt';
            objServ.keyDocBodyPassport = EncodingUtil.base64Encode(Blob.valueOf('Test part 2'));
            objServ.keyDocNamePassport = 'Tony Part 2.txt';
          objServ.objKeyAttach = NULL ;
            objServ.uploadFileToServer();
            objServ.saveDraft();
          objServ.save();
        test.stopTest();       
    }
    
    static testMethod void testSave2() {
      list<Booking_Unit__c> lstUnits = [ SELECT Id FROM Booking_Unit__c LIMIT 1 ];
     
        ApexPages.StandardController stdController = new ApexPages.StandardController( lstUnits[0] )  ;
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockRPTermination(3) );
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        test.startTest();
            HandoverKeyReleaseFormExtension objServ = new HandoverKeyReleaseFormExtension( stdController );
          objServ.keyDocBody = EncodingUtil.base64Encode(Blob.valueOf('Test'));
            objServ.keyDocName = 'Tony.txt';
            objServ.objKeyAttach = NULL ;
            objServ.uploadFileToServer();
            objServ.keyDocBodyPassport = EncodingUtil.base64Encode(Blob.valueOf('Test part 2'));
            objServ.keyDocNamePassport = 'Tony Part 2.txt';
            objServ.uploadFileToServer();
            objServ.saveDraft();
          objServ.save();
        test.stopTest();       
    }

    static testMethod void testSave3() {
      list<Booking_Unit__c> lstUnits = [ SELECT Id FROM Booking_Unit__c LIMIT 1 ];
     
        ApexPages.StandardController stdController = new ApexPages.StandardController( lstUnits[0] )  ;
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockRPTermination(3) );
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        test.startTest();
            HandoverKeyReleaseFormExtension objServ = new HandoverKeyReleaseFormExtension( stdController );
          objServ.keyDocBody = EncodingUtil.base64Encode(Blob.valueOf('Test'));
            objServ.keyDocName = 'Tony.txt';
            objServ.objKeyAttach = NULL ;
            objServ.uploadFileToServer();
            objServ.keyDocBodyPassport = EncodingUtil.base64Encode(Blob.valueOf('Test part 2'));
            objServ.keyDocNamePassport = 'Tony Part 2.txt';
            objServ.generateDoc();
            /*objServ.uploadFileToServer();
            objServ.saveDraft();
          objServ.save();*/
        test.stopTest();       
    }

    static testMethod void testSave4() {
      list<Booking_Unit__c> lstUnits = [ SELECT Id FROM Booking_Unit__c LIMIT 1 ];
     
        ApexPages.StandardController stdController = new ApexPages.StandardController( lstUnits[0] )  ;
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockRPTermination(3) );
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        test.startTest();
            HandoverKeyReleaseFormExtension objServ = new HandoverKeyReleaseFormExtension( stdController );
          objServ.keyDocBody = EncodingUtil.base64Encode(Blob.valueOf('Test'));
            objServ.keyDocName = 'Tony.txt';
            objServ.objKeyAttach = NULL ;
            objServ.uploadFileToServer();
            objServ.keyDocBodyPassport = EncodingUtil.base64Encode(Blob.valueOf('Test part 2'));
            objServ.keyDocNamePassport = 'Tony Part 2.txt';
            objServ.uploadFileToServer();
            objServ.saveDraft();
            objServ.selectedUnit.Key_Release_Date__c = Date.newInstance(2020, 2, 17);
          objServ.save();
        test.stopTest();       
    }

    static testMethod void testSave5() {
      list<Booking_Unit__c> lstUnits = [ SELECT Id FROM Booking_Unit__c LIMIT 1 ];
     
        ApexPages.StandardController stdController = new ApexPages.StandardController( lstUnits[0] )  ;
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockRPTermination(1) );
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        test.startTest();
            HandoverKeyReleaseFormExtension objServ = new HandoverKeyReleaseFormExtension( stdController );
          objServ.keyDocBody = EncodingUtil.base64Encode(Blob.valueOf('Test'));
            objServ.keyDocName = 'Tony.txt';
            objServ.objKeyAttach = NULL ;
            objServ.uploadFileToServer();
            objServ.keyDocBodyPassport = EncodingUtil.base64Encode(Blob.valueOf('Test part 2'));
            objServ.keyDocNamePassport = 'Tony Part 2.txt';
            objServ.saveDraft();
            objServ.selectedUnit.Key_Release_Date__c = Date.newInstance(2020, 2, 17);
          objServ.save();
        test.stopTest();       
    }

    static testMethod void testSave6() {
      list<Booking_Unit__c> lstUnits = [ SELECT Id FROM Booking_Unit__c LIMIT 1 ];
     
        ApexPages.StandardController stdController = new ApexPages.StandardController( lstUnits[0] )  ;
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockRPTermination(1) );
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        test.startTest();
            HandoverKeyReleaseFormExtension objServ = new HandoverKeyReleaseFormExtension( stdController );
          objServ.keyDocBody = EncodingUtil.base64Encode(Blob.valueOf('Test'));
            objServ.keyDocName = 'Tony.txt';
            objServ.objKeyAttach = NULL ;
            objServ.uploadFileToServer();
            objServ.keyDocBodyPassport = EncodingUtil.base64Encode(Blob.valueOf('Test part 2'));
            objServ.keyDocNamePassport = 'Tony Part 2.txt';
            objServ.checkForBothVariable = 2;
            objServ.uploadFileToServer();
            objServ.checkForBothVariable = 2;
            objServ.isSuccessfull= true;
            objServ.saveDraft();
          objServ.save();
        test.stopTest();       
    }
    /*add to make code coverage*/
    static testMethod void testSave7() {
      list<Booking_Unit__c> lstUnits = [ SELECT Id,Key_Release_Date__c FROM Booking_Unit__c LIMIT 1 ];
      lstUnits[0].Key_Release_Date__c = System.today();
      lstUnits[0].Okay_to_release_keys__c = true;
      update lstUnits;
     
        ApexPages.StandardController stdController = new ApexPages.StandardController( lstUnits[0] )  ;
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockRPTermination(1) );
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        test.startTest();
            HandoverKeyReleaseFormExtension objServ = new HandoverKeyReleaseFormExtension( stdController );
          objServ.keyDocBody = EncodingUtil.base64Encode(Blob.valueOf('Test'));
            objServ.keyDocName = 'Tony.txt';
            objServ.objKeyAttach = NULL ;
            objServ.uploadFileToServer();
            objServ.keyDocBodyPassport = EncodingUtil.base64Encode(Blob.valueOf('Test part 2'));
            objServ.keyDocNamePassport = 'Tony Part 2.txt';
            objServ.checkForBothVariable = 2;
            objServ.uploadFileToServer();
            objServ.checkForBothVariable = 2;
            objServ.isSuccessfull= true;
            objServ.saveDraft();
            objServ.generateDoc();
          objServ.save();
        test.stopTest();       
    }
    
  static testMethod void testBack() {
        list<Booking_Unit__c> lstUnits = [ SELECT Id FROM Booking_Unit__c LIMIT 1 ];
     
        ApexPages.StandardController stdController = new ApexPages.StandardController( lstUnits[0] )  ;
        test.startTest();
            HandoverKeyReleaseFormExtension objServ = new HandoverKeyReleaseFormExtension( stdController );
          objServ.back();
        test.stopTest();   
    }

  static testMethod void testExtractBody() { 
        test.startTest();
          HandoverKeyReleaseFormExtension.extractBody( '' );
        test.stopTest();
    }
    
    static testMethod void testExtractName() {        
        test.startTest();
          HandoverKeyReleaseFormExtension.extractName( '' );
        test.stopTest();
    }    
    
}