/*********************************************************************************************
 * Description : Controller of Vf page 'Generate_Docs_For_RRC_SR' to generate Drawloop 
                    documents for Riyadh Rotana Conversion SR.
 *==========================================================================================
 * Ver      Date            Author                  Modification
 *==========================================================================================
 * 1.0      09/11/2019      Aishwarya Todkar        Initial Draft
 * 2.0      05/12/2019      Aishwarya Todkar        Created placeholders 
********************************************************************************************/
public class Generate_Docs_For_RRC_SR_Ctrl {

    @TestVisible Id caseId;

    public Generate_Docs_For_RRC_SR_Ctrl(ApexPages.StandardController controller) {
        //this.objCase = (Case)controller.getRecord();
        caseId = ApexPages.currentpage().getParameters().get('id');
    }

    public PageReference  generateDrawloopDocuments() {
        String strBuilding = '';
        String strSigned = '';
        String drawloopCsKey = '';
        Case objCase = new Case();
        List<SR_Attachments__c> lstPlaceholders = new List<SR_Attachments__c>();

        if( String.isNotBlank( String.valueOf( caseId) ) ) {
            objCase = [SELECT
                            Id
                            , Building__c
                            , Signed_SPA_AGS__c
                            , Booking_Unit__c
                            , Registration_Id__c
                            , Owner_Interested_In_Hotel__c
                        FROM
                            Case
                        WHERE
                            Id =: caseId];

            //Creating Tags of client Id and passport
            lstPlaceholders.add( new SR_Attachments__c
                                    ( Name = 'Client ID'
                                    , Case__c = caseId
                                    , Booking_Unit__c = objCase.Booking_Unit__c
                                    )
            );
            lstPlaceholders.add( new SR_Attachments__c 
                                    ( Name = 'Passport'
                                    , Case__c = caseId
                                    , Booking_Unit__c = objCase.Booking_Unit__c
                                    )
            );

            Map<String, Riyadh_Rotana_Document_Criteria_Mapping__c> mapRrcDocCriteria = 
                new Map<String, Riyadh_Rotana_Document_Criteria_Mapping__c>();
            for(Riyadh_Rotana_Document_Criteria_Mapping__c cs : Riyadh_Rotana_Document_Criteria_Mapping__c.getall().values()) {
                mapRrcDocCriteria.put(cs.Name, cs);
            }
            //Getting Drawloop details
            Map<String, Riyadh_Rotana_Drawloop_Doc_Mapping__c> mapRRC_To_drawloopDoc = 
                new Map<String, Riyadh_Rotana_Drawloop_Doc_Mapping__c>();
            
            for(Riyadh_Rotana_Drawloop_Doc_Mapping__c cs : Riyadh_Rotana_Drawloop_Doc_Mapping__c.getall().values()) {
                mapRRC_To_drawloopDoc.put(cs.Name, cs);
            }

            System.debug(' mapRRC_To_drawloopDoc = ' + mapRRC_To_drawloopDoc);
            System.debug('objCase == ' + objCase);
            if(objCase != null ) {
                if( String.isBlank(objCase.Building__c) ) {
                    ApexPages.addmessage(
                        new ApexPages.message(ApexPages.severity.Error,'Building can not be blank!')
                    );
                    return null;
                }
                
                if( String.isBlank(objCase.Signed_SPA_AGS__c) ) {
                    ApexPages.addmessage(
                        new ApexPages.message(ApexPages.severity.Error,'Agreement type can not be blank!')
                    );
                    return null;
                }

                if( String.isNotBlank(objCase.Building__c) && String.isNotBlank(objCase.Signed_SPA_AGS__c) ) {
                    strBuilding = String.valueOf( objCase.Building__c.subString( 0, 1 ) );
                    strSigned = String.valueOf( objCase.Signed_SPA_AGS__c.subString( 0, 1 ) );
                    
                    if( objCase.Owner_Interested_In_Hotel__c.equalsIgnoreCase( 'Interested' ) ) {
                        drawloopCsKey = 'I' + '-' + strBuilding + '-' + strSigned;
                    }
                    else {
                        drawloopCsKey = 'N-I';
                    }

                    System.debug('drawloopCsKey == ' + drawloopCsKey);

                    if( mapRrcDocCriteria.containsKey( drawloopCsKey ) && mapRrcDocCriteria.get( drawloopCsKey ) != null ) {
                        String commaSep_DocName = mapRrcDocCriteria.get( drawloopCsKey ).Required_Document_Names__c;
                        if( String.isNotBlank( commaSep_DocName ) ) {
                            Set<String> docNames = new Set<String>();
                            if( commaSep_DocName.contains(',') ) {
                                for ( String doc : commaSep_DocName.split(',') )
                                    docNames.add(doc);
                            }
                            else {
                                docNames = new Set<String> { commaSep_DocName };
                            }
                            System.debug( 'docNames == ' + docNames);

                            if ( docNames != null && docNames.size() > 0 
                            && mapRRC_To_drawloopDoc != null 
                            && mapRRC_To_drawloopDoc.size() > 0 ) {
                                for( String doc : docNames ) {
                                    if( mapRRC_To_drawloopDoc.containsKey( doc) ) {
                                        Riyadh_Rotana_Drawloop_Doc_Mapping__c rrcObj = mapRRC_To_drawloopDoc.get( doc );
                                        String ddpId = rrcObj.Drawloop_Document_Package_Id__c;
                                        String deliveryOptId =  rrcObj.Delivery_Option_Id__c;

                                        if( String.isNotBlank( ddpId ) && String.isNotBlank( deliveryOptId ) ) {

                                            //Creating Placeholders
                                            if( !doc.equalsIgnoreCase( 'RRC 8' ) ) {
                                                
                                                String placeholderName = objCase.Registration_Id__c == null
                                                                        ? 'Signed ' + rrcObj.Document_Name__c + '-' + String.valueOf( System.now() )
                                                                        : objCase.Registration_Id__c + '-Signed ' + rrcObj.Document_Name__c 
                                                                            + '-' + String.valueOf( System.now() );
                                                lstPlaceholders.add( new SR_Attachments__c 
                                                                        ( Name = placeholderName
                                                                        , Case__c = caseId
                                                                        , Booking_Unit__c = objCase.Booking_Unit__c
                                                                        )
                                                                    );    
                                            }
                                            if( !Test.isRunningTest() )
                                                Database.executeBatch( new  GenerateDrawloopDocumentBatch( caseId, ddpId, deliveryOptId ) );
                                        }
                                    }
                                }//End for
                                
                                System.debug( 'lstPlaceholders == ' + lstPlaceholders);
                                if( lstPlaceholders != null && lstPlaceholders.size() > 0 )
                                    insert lstPlaceholders;
                            }// End mapRRC_To_drawloopDoc if
                        }//End commaSep_DocName if
                    }// End drawloopCsKey if
                }//End building and agreement type if
                return new PageReference('/' + objCase.Id);
            }// End Objcase if 
            return null;
        }// End caseId if
        return null;
    }
}