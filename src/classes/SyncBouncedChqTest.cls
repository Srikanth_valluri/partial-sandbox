@isTest
public class SyncBouncedChqTest {
    @testSetup static void testSetupMethod() {
    
        /*EmailTemplate template = new EmailTemplate (developerName = 'BounceCheckEmailTemplate',  
                                             TemplateType= 'Text', 
                                             FolderId = UserInfo.getUserId(),
                                             Name = 'Bounce Check Email Template'); 

        insert template;*/
        
        List<TriggerOnOffCustomSetting__c>settingLst = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                    OnOffCheck__c = true);
        
        settingLst.add(newSetting1);
        insert settingLst;
        
        Account objAccount = new Account();
        objAccount.Name = 'test';
        objAccount.Party_ID__c = '12345';
        insert objAccount;
        
        Case objCase = new Case();
        objCase.AccountId = objAccount.Id;
        insert objCase;
        
        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Calling_List_Type__c = 'BC Calling';
        objCallingList.Account__c = objAccount.Id;
        objCallingList.Case__c = objCase.Id;
        insert objCallingList;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Unit_Name__c ='test';
        objBU.Property_Name__c = 'test';
        objBU.Registration_ID__c = '12345678';
        objBU.Booking__c = objBooking.Id;
        insert objBU;
    }

    @isTest
    static void testSyncBounceCheq1(){
    
        Booking_Unit__c objBU = [SELECT Id,Registration_ID__c FROM Booking_Unit__c WHERE Registration_ID__c = '12345678'];
        
        Account objAccount = [SELECT Id FROM Account WHERE Party_ID__c = '12345'];
        objAccount.Email__c = 'test@test.com';
        update objAccount;
        Test.startTest();
        //SyncBouncedChq objSync = new SyncBouncedChq();
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {

            EmailTemplate template = new EmailTemplate (developerName = 'BounceCheckEmailTemplate',  
                                                 TemplateType= 'Text', 
                                                 FolderId = UserInfo.getUserId(),
                                                 Body = 'Test Body',
                                                 Name = 'Bounce Check Email Template'); 
    
            insert template;
        }
        SyncBouncedChq.SyncBouncedChq(new List<String>{objBU.Registration_Id__c}, 'CreateSR', '12345', '14789', 500.0, System.today(), 'testBank', '78945', '587945','testReason');
        Test.stopTest();
    }
    @isTest
    static void testSyncBounceCheq2(){
        
        Booking_Unit__c objBU = [SELECT Id,Registration_ID__c FROM Booking_Unit__c WHERE Registration_ID__c = '12345678'];
        Account objAccount = [SELECT Id FROM Account WHERE Party_ID__c = '12345'];
        Case objCase = [SELECT Id FROM Case WHERE AccountId = :objAccount.Id];
        objCase.Receipt_Id__c = '78945';
        objCase.Status = 'Submitted';
        update objCase;
        Test.startTest();
        
        //SyncBouncedChq objSync = new SyncBouncedChq();
        SyncBouncedChq.SyncBouncedChq(new List<String>{objBU.Registration_Id__c}, 'UpdateSR', '12345', '14789', 500.0, System.today(), 'testBank', '78945', '587945','testReason');
        Test.stopTest();
    }
    @isTest
    static void testSyncBounceCheq3(){
        
        Booking_Unit__c objBU = [SELECT Id,Registration_ID__c FROM Booking_Unit__c WHERE Registration_ID__c = '12345678'];
        Account objAccount = [SELECT Id FROM Account WHERE Party_ID__c = '12345'];
        Case objCase = [SELECT Id FROM Case WHERE AccountId = :objAccount.Id];
        objCase.Receipt_Id__c = '78945';
        objCase.Status = 'Submitted';
        update objCase;
        Test.startTest();
       
        //SyncBouncedChq objSync = new SyncBouncedChq();
        SyncBouncedChq.SyncBouncedChq(new List<String>{objBU.Registration_Id__c}, 'CloseSR', '12345', '14789', 500.0, System.today(), 'testBank', '78945', '587945','testReason');
        Test.stopTest();
    }
    @isTest
    static void testSyncBounceCheq4(){
         
        Booking_Unit__c objBU = [SELECT Id,Registration_ID__c FROM Booking_Unit__c WHERE Registration_ID__c = '12345678'];
        Account objAccount = [SELECT Id FROM Account WHERE Party_ID__c = '12345'];
        Case objCase = [SELECT Id,SR_with_Legal__c,Status FROM Case WHERE AccountId = :objAccount.Id];
        objCase.Receipt_Id__c = '78945';
        objCase.Status = 'Submitted';
        objCase.SR_with_Legal__c = true;
        update objCase;
        Test.startTest();
        Case objCase1 = [SELECT Id,SR_with_Legal__c,Status FROM Case WHERE AccountId = :objAccount.Id];
        system.debug('objCase1 >>>>>'+objCase1);
        //SyncBouncedChq objSync = new SyncBouncedChq();
        SyncBouncedChq.SyncBouncedChq(new List<String>{objBU.Registration_Id__c}, 'Paid', '12345', '14789', 500.0, System.today(), 'testBank', '78945', '587945','testReason');
        Test.stopTest();
    }
}