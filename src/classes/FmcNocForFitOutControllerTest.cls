@isTest
private class FmcNocForFitOutControllerTest {

    public static testmethod void testScenario1() {
        Account acctIns=new Account(Name='test Account');
        insert acctIns;
        CustomerCommunityUtils.customerAccountId = acctIns.Id;

        Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = acctIns.id;
        sr.RecordTypeId = RecType;
        insert sr;

        Booking__c  bk = new  Booking__c();
        bk.Account__c = acctIns.id;
        bk.Deal_SR__c = sr.id;
        insert bk;

        Booking_Unit__c buIns=new Booking_unit__c(
            Owner__c=acctIns.id,Resident__c=acctIns.id,Booking__c=bk.id, Unit_Name__c='JNU/SD168/XH2910B',
            Registration_Id__c='74655', Property_Name__c='JANUSIA@AKOYA OXYGEN', JOPD_Area__c = '1000'
        );
        insert buIns;
        
        Property__c objProperty = new Property__c();
        objProperty.Name = 'PARK TOWERS';
        objProperty.Property_ID__c = 67890;
        insert objProperty;

        Location__c locObj = new Location__c();
        locObj.Name  = 'JNU';
        locObj.Location_ID__c = '83488';
        locObj.As_Built_Drawing_Fee__c=100;
        locObj.Drawing_Review_Charges__c = 5 ;
        locObj.Property_Name__c = objProperty.Id;
        insert locObj;

        SR_Attachments__c newAttach = new SR_Attachments__c();
        newAttach.Name = 'Test';
        insert newAttach ;

        PageReference myVfPage = Page.NOCForFitOutProcessPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('unitId', buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','NOC_For_FitOut');
        ApexPages.currentPage().getParameters().put('view','NOCforFitOutAlterations');
        //ApexPages.currentPage().getParameters().put('Id',Opp.Id);
        FmcNocForFitOutController obj = new FmcNocForFitOutController();
        obj.strSelectedUnit = buIns.Id;
        obj.selectUnit();
        obj.insertCase();
        obj.saveAsDraft();
        obj.deleteAttRecId = newAttach.Id ;
        obj.mapUploadedDocs = new map<String,SR_Attachments__c>();
        obj.mapUploadedDocs.put(newAttach.Name, newAttach );
        obj.deleteAttachment();
        obj = new FmcNocForFitOutController();
    }

    public static testmethod void submitFMCase() {
        Account acctIns=new Account(Name='test Account');
        insert acctIns;
        CustomerCommunityUtils.customerAccountId = acctIns.Id;

        Id RecType = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = acctIns.id;
        sr.RecordTypeId = RecType;
        insert sr;

        Booking__c  bk = new  Booking__c();
        bk.Account__c = acctIns.id;
        bk.Deal_SR__c = sr.id;
        insert bk;

        Booking_Unit__c buIns=new Booking_unit__c(
            Owner__c=acctIns.id,Resident__c=acctIns.id,Booking__c=bk.id, Unit_Name__c='JNU/SD168/XH2910B',
            Registration_Id__c='74655', Property_Name__c='JANUSIA@AKOYA OXYGEN',Property_City__c='Dubai',
            JOPD_Area__c = '1000'
        );
        insert buIns;

        Property__c objProperty = new Property__c();
        objProperty.Name = 'PARK TOWERS';
        objProperty.Property_ID__c = 12345;
        insert objProperty;
        
        Location__c locObj = new Location__c();
        locObj.Name  = 'JNU';
        locObj.Location_ID__c = '83488';
        locObj.As_Built_Drawing_Fee__c=100;
        locObj.Major_Work_Charges__c=100;
        locObj.Minor_Work_Charges__c=100;
        locObj.Drawing_Review_Charges__c = 5 ;
        locObj.Property_Name__c = objProperty.Id;
        insert locObj;

        PageReference myVfPage = Page.NOCForFitOutProcessPage;
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Person_To_Collect__c = 'CONTRACTOR';
        fmCaseObj.Email__c = 'abc@test.com';
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='NOC_For_FitOut';
        fmCaseObj.Account__c=acctIns.id;
        fmCaseObj.Booking_Unit__c=buIns.id;
        fmCaseObj.Outstanding_service_charges__c = '0';
        insert fmCaseObj;
        Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('id',fmCaseObj.Id);
        ApexPages.currentPage().getParameters().put('view','NOCforFitOutAlterations');
        //ApexPages.currentPage().getParameters().put('Id',Opp.Id);
        FmcNocForFitOutController obj = new FmcNocForFitOutController();
        obj.strSelectedUnit = buIns.Id;
        obj.selectUnit();
        obj.strDocumentBody='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        obj.strDocumentName='Test_Document_Work_Permit.htm';
        test.startTest();

        obj.uploadDocument();
        obj.objFMCase.Person_To_Collect__c = 'CONTRACTOR';  
        obj.objFMCase.Email_2__c = 'abc@test.com';
        obj.notifyContractorConsultant();
        obj.submitRequest();
        obj.checkCommonElements( new list<String>{'Test'}, new list<String>{'Test'} );

        test.stoptest();
    }

}