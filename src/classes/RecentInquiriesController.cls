/****************************************************************************************************
* Name          : RecentInquiriesController                                                         *
* Description   : Class to display the Recent Inquiries related to the user                         *
* Created Date  : 25/06/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                *
* 1.0         Twinkle P     25/06/2018      Initial Draft.                                          *
****************************************************************************************************/
public with sharing class RecentInquiriesController {
    public List<Inquiry__c> inquiryDetails {get; set;}
    public list<selectOption> records      {get; set;}
    public integer numberLimit             {get; set;}

    public RecentInquiriesController() {
        records=new List<selectOption>();
        records.add(new selectoption('5','5'));
   		records.add(new selectoption('10','10'));
   		records.add(new selectoption('20','20'));
        numberLimit=5;
        inquiryDetails = new List<Inquiry__c>();
        inquiryDetails = fetchInquiriesDetails();
        
    }
    /**
     * Method to get all the recent inquries
     */
    private List<Inquiry__c> fetchInquiriesDetails () {
        return [SELECT Id, Assigned_Date__c ,Name,  
                Last_Name__c,
                First_Name__c,
                Inquiry_Status__c,
                Inquiry_Source__c
                FROM Inquiry__c 
                WHERE OwnerId= : userinfo.getuserid()
                AND Inquiry_Status__c != 'Closed Won'
                ORDER BY CreatedDate DESC  LIMIT: numberLimit];
    }
    public pageReference showRecords(){
        inquiryDetails = fetchInquiriesDetails();
   	    return null;
    }
}