/****************************************************************************************************
* VER   AUTHOR              DATE            Description                                             *
* 1.1   Craig Lobo          08-07-2018      Added code to generate receipt for Flyin Travel Details *
****************************************************************************************************/

public without sharing class PaymentAPIController {

    public Decimal amount                   { get; set; }
    public String billing_name              { get; set; }
    public String billing_address           { get; set; }
    public String billing_city              { get; set; }
    public String billing_state             { get; set; }
    public String billing_zip               { get; set; }
    public String billing_country           { get; set; }
    public String billing_tel               { get; set; }
    public String billing_email             { get; set; }
    public String order_ID                  { get; set; }
    public String url                       { get; set; }
    public Boolean termsAndConditions       { get; set; }
    public Boolean payNow                   { get; set; }
    public String srRecordID;
    public String detailsId;
    public Buyer__c buyer;
    public Travel_Details__c tdRecord;

    public PaymentAPIController () {
        System.debug('PaymentAPIController>>>> ' ); 
        termsAndConditions = FALSE;
        payNow = false;
        detailsId = apexpages.currentpage().getparameters().get('detailsId');
        System.debug('detailsId>>>> ' + detailsId); 
        srRecordID = apexpages.currentpage().getparameters().get('id');
        if (srRecordID != NULL) {
            if (srRecordID.length () == 18) {
                buyer = new Buyer__c ();
                buyer = Database.query ('SELECT '+getAllFields ('Buyer__c')
                                             + ',Booking__r.Deal_SR__r.Token_Amount_AED__c  FROM Buyer__c '
                                             + ' WHERE Booking__r.Deal_SR__c =: srRecordId AND Primary_buyer__c = TRUE LIMIT 1');

                billing_name = buyer.Title__c+' '+buyer.First_Name__c +' '+buyer.Last_Name__c;
                billing_address = buyer.Address_Line_1__c != 'To Update' ? buyer.Address_Line_1__c : '';
                billing_city = buyer.City__c != 'To Update' ? buyer.City__c : '';
                billing_country = buyer.Country__c != 'To Update' ? buyer.Country__c : '';
                billing_tel = buyer.Phone__c != 'To Update' ? buyer.Phone__c : '';
                billing_email = buyer.Email__c != 'To Update' ? buyer.Email__c : '';
                amount = buyer.Booking__r.Deal_SR__r.Token_Amount_AED__c;
                
            }
        } else {
            // Added by Craig
            if (detailsId != null) {
                billing_name = billing_country = billing_tel = billing_email = '';
                amount = 0;
                System.debug('detailsId>>>> ' + detailsId); 
                tdRecord = new Travel_Details__c();
                tdRecord = Database.query( 'SELECT Inquiry__r.Mobile_Phone__c, Inquiry__r.Country__c, ' 
                         + getAllFields ('Travel_Details__c') 
                         + ' FROM Travel_Details__c ' 
                         + ' WHERE Id = \'' 
                         + detailsId 
                         + '\' LIMIT 1');
                System.debug('tdRecord>>>> ' + tdRecord); 
                billing_name = tdRecord.Inquiry_Name__c;
                billing_address = '';
                billing_city = '';
                billing_country = tdRecord.Inquiry__r.Country__c;
                billing_tel = tdRecord.Inquiry__r.Mobile_Phone__c;
                billing_email = tdRecord.Inquiry_Email__c;
                amount = tdRecord.Token_Amount__c;
            }
            // Added by Craig
        }
    }
    
    public PageReference generatePayment () {
        try {
            System.Debug (termsAndConditions);
            System.debug('generatePayment>>>> '); 
            if (termsAndConditions) {
                //Create a receipt record with values entered by the customer
                Receipt__c R = new Receipt__c();
                if (buyer != null) {
                    R.Amount__c = buyer.Booking__r.Deal_SR__r.Token_Amount_AED__c; 
                    R.Service_Request__c = srRecordID;       
                } else if (tdRecord != null) {
                    R.Amount__c = amount; 
                    R.Travel_Details__c = tdRecord.Id;
                }
                R.Receipt_Type__c = 'Card';
                R.Transaction_Date__c = system.now();

                Id rt = Schema.SObjectType.Receipt__c.getRecordTypeInfosByName().get('Card').getRecordTypeId();
                R.RecordTypeId = rt;
                insert R;

                Receipt__c receipt = new Receipt__c ();
                receipt = [ SELECT Name FROM Receipt__c WHERE ID = :r.ID ];

                Long tid = System.Now ().getTime();
                Payment_Gateway_Credentials__mdt credentials = new Payment_Gateway_Credentials__mdt ();
                credentials = [SELECT Developername, Redirect_URL__c, Gateway_URL__c, Access_Code__c, ENC_Key__c, Merchant_ID__c, Label 
                                FROM Payment_Gateway_Credentials__mdt 
                                WHERE Label = 'CCAvenue' LIMIT 1];
                String PLAIN_TEXT = 'tid='+tid+'&merchant_id='+credentials.Merchant_ID__c
                        +'&order_id='+receipt.Name
                        +'&amount='+amount
                        +'&currency=AED'
                        +'&redirect_url='+credentials.Redirect_URL__c
                        +'&cancel_url='+credentials.Redirect_URL__c
                        +'&language=EN'
                        +'&billing_name='+billing_name
                        +'&billing_address='+billing_address
                        +'&billing_city='+billing_city
                        +'&billing_state='+billing_state
                        +'&billing_zip='+billing_zip
                        +'&billing_country='+billing_country
                        +'&billing_tel='+billing_tel
                        +'&billing_email='+billing_email
                        +'&response_type=JSON'
                        +'&integration_type=iframe_normal';

                Blob cryptoKey = Blob.valueOf(credentials.ENC_Key__c);        
                Blob hash = Crypto.generateDigest('MD5', cryptoKey );        
                Blob data = Blob.valueOf (PLAIN_TEXT);        
                Blob encryptedData = Crypto.encryptWithManagedIV('AES128', hash , data);        
                String encRequest = EncodingUtil.convertToHex (encryptedData );
                url = credentials.Gateway_URL__c+'/transaction/transaction.do?command=initiateTransaction'
                            +'&merchant_id='+credentials.Merchant_ID__c+'&encRequest='
                            +encRequest+'&access_code='+credentials.Access_Code__c;
                System.debug('generatePayment URL>>>> ' + URL);  
                if (buyer != null) {
                    return new PageReference (URL);
                } else if (tdRecord != null) {
                    payNow = true;
                }
                return null;
            } else {
                ApexPages.addMessage
                    (new ApexPages.Message(ApexPages.Severity.ERROR, 'Please check terms and conditions')
                );
                return null;
            }
        }
        catch (Exception e) {
            return null;
        }
    }
    
     public pageReference cancel () {
        if (buyer != null) {
            return new pageReference ('/'+srRecordID);
        } else if (tdRecord != null) {
            // Added by Craig
            return null;
        }
        return null;
        //return new pageReference ('/'+srRecordID);
    }
    /************************************************************************************************
    * @Description : Utility Method to get all fields based on object name for query                *
    * @Params      : Object API                                                                     *
    * @Return      : Object related Field APIs as Comma seperated                                   *
    ************************************************************************************************/
    public static string getAllFields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        
        if (objectType == null)
            return fields;

        Map <String, Schema.SObjectField> fieldMap = objectType.getDescribe ().fields.getMap ();
        for (Schema.SObjectField sfield : fieldMap.Values()) {
            fields += sfield.getDescribe().getName ()+ ', ';
        }
        return fields.removeEnd(', '); 
    }
}