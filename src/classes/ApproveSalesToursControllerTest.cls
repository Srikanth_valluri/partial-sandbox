@isTest
public with sharing class ApproveSalesToursControllerTest {
        
    @isTest static void method1(){

        Campaign__c camp = new Campaign__c();
        camp.Campaign_Name__c='Test Campaign';
        camp.Start_date__c = System.today();
        camp.End_date__c = System.Today().addDays(30);
        camp.Marketing_start_date__c = System.today();
        camp.Marketing_end_date__c = System.Today().addDays(30);
        camp.Language__c = 'English';
        camp.Marketing_Active__c = true;
        camp.Credit_Control_Active__c  = true;
        camp.Sales_Admin_Active__c  = true;
        camp.TSA_Team__c = 'Stands';
        insert camp;

        Inquiry__c newInquiry = new Inquiry__c();
        newInquiry.Campaign__c = camp.Id;
        newInquiry.First_Name__c = 'Test';
        newInquiry.Last_Name__c = 'Inquiry';
        newInquiry.Mobile_Phone_Encrypt__c = '05789088';
        newInquiry.Mobile_Phone__c = '05789088';
        newInquiry.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        newInquiry.Email__c = 'craig.lobo@eternussolutions.com';
        newInquiry.Preferred_Language__c = 'English';
        newInquiry.Inquiry_Source__c = 'Social' ;
        newInquiry.Inquiry_Status__c = 'Qualified' ;
        newInquiry.Inquiry_Status__c = null ;
        insert newInquiry;

        Sales_Tours__c ts =new Sales_Tours__c();
        ts.Inquiry__c = newInquiry.Id;
        ts.Tour_Outcome__c = 'Show - Options given and considering';
        ts.Pickup_Location__c = 'Park Towers';
        ts.Check_In_Date__c = System.now();
        ts.Check_Out_Date__c = System.now();
        ts.Comments__c = 'Test Tour';
        ts.Status__c = 'Submitted';
        ts.Is_Approved__c = false;
        insert ts;

        Test.startTest();
        ApexPages.StandardController stdController = new ApexPages.StandardController(ts);
        ApproveSalesToursController instance = new ApproveSalesToursController(stdController);
        instance.searchToursByPC();
        instance.approveSalesTours();
        instance.salesToursList[0].Is_Approved__c = true; 
        instance.approveSalesTours();
        Test.stopTest();
    }
}