@isTest
public Class FlyinDetailsControllerTest{

    static testmethod void testmethod_1(){
         
         Inquiry__c inqObj= new Inquiry__c(
         //RecordTypeId=agenTeamRT,
         Inquiry_Source__c='Agent Referral',
         Mobile_Phone_Encrypt__c='456123',
         Mobile_CountryCode__c='American Samoa: 001684',
         Mobile_Phone__c='1234',
         Email__c='mk@gmail.com',
         First_Name__c='Test',
         Last_Name__c='Last',
         Meeting_Type__c = 'Scheduled Tour'
         );
            
         insert inqObj;
            
         Travel_Details__c td = new Travel_Details__c(Inquiry__c=inqObj.id);
         td.From_Date__c = Date.newInstance(2018,07,06);
         td.To_Date__c = Date.newInstance(2018,07,08);
         td.Status__c = 'Draft';
         td.Email_Sent_Time__c = datetime.newInstance(2018, 7,16, 10, 30, 0);
         insert td;
            
         ApexPages.StandardController sc = new ApexPages.StandardController(td);
         Test.startTest();
             
             Test.setCurrentPageReference(new PageReference('Page.FlyinDetails')); 
             System.currentPageReference().getParameters().put('recid', td.id);
             FlyinDetailsController cont = new FlyinDetailsController(sc);
             cont.departureDate = '07/10/2018';
             cont.returnDate = '07/12/2018';
             cont.saveTravelDetails();
             cont.sendTravelDetails();
             cont.getPassengerIdTypeoptions();
             cont.getClassofTraveloptions();
             cont.getTransportoptions();
             //cont.getDepartureOptions();
             cont.getArrivalOptions();
             cont.getServiceTypeOptions();
             //cont.updateDetailsconfirmed();
             cont.getValidVisaOptions();
             
             System.currentPageReference().getParameters().put('passengerfisrtname', 'firstname');
             System.currentPageReference().getParameters().put('passengerlastname', 'lastname');
             System.currentPageReference().getParameters().put('passengerdob', '07/10/1989');
             System.currentPageReference().getParameters().put('passengervalidvisa', 'Yes');
             System.currentPageReference().getParameters().put('passengeridtype', 'Passport');
             System.currentPageReference().getParameters().put('passengeridnum', '1234');
             System.currentPageReference().getParameters().put('passengerexpiddate', '07/10/2022');
             System.currentPageReference().getParameters().put('travelrecid', td.id);
             cont.savePassengerDetail();
             
             //FlyinDetailsController.savePassengerDetails('fisrtname','lastname','07/10/1989','Yes','Passport','1234','07/10/2022',td.id);
             
            Profile p = [SELECT Id FROM Profile WHERE Name='DAMAC Profile']; 
            User u = new User(Alias = 'standt', Email='standardflyinuser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='flyinuser', LanguageLocaleKey='en_US', 
            TimeZoneSidKey='America/Los_Angeles',LocaleSidKey='en_US', ProfileId = p.Id, 
            UserName='standarduserflyindetail@testorg.com');
            
            System.runas(u){
                cont.updateDetailsconfirmed();
                System.currentPageReference().getParameters().put('val', 'true');
                //cont.updateDetailsconfirmed();
            } 
            cont.updateDetailsconfirmed();
            cont.createNewTravelDetailRecord();   
         Test.stopTest();   
    
    }

    static testmethod void testmethod_2(){
        Inquiry__c inqObj= new Inquiry__c(
         //RecordTypeId=agenTeamRT,
         Inquiry_Source__c='Agent Referral',
         Mobile_Phone_Encrypt__c='456123',
         Mobile_CountryCode__c='American Samoa: 001684',
         Mobile_Phone__c='1234',
         Email__c='mk@gmail.com',
         First_Name__c='Test',
         Last_Name__c='Last',
         Meeting_Type__c = 'Scheduled Tour'
         );
            
         insert inqObj;
         
         Travel_Details__c td = new Travel_Details__c(Inquiry__c=inqObj.id);
         td.From_Date__c = Date.newInstance(2018,07,06);
         td.To_Date__c = Date.newInstance(2018,07,08);
         td.Status__c = 'Draft';
         td.Email_Sent_Time__c = datetime.newInstance(2018, 7,16, 10, 30, 0);
         td.Inquiry__c =inqObj.id;
         insert td;
             
         Test.setCurrentPageReference(new PageReference('Page.FlyinDetails')); 
         System.currentPageReference().getParameters().put('inqid',inqObj.id);
         
         ApexPages.StandardController sc = new ApexPages.StandardController(td);
         FlyinDetailsController cont = new FlyinDetailsController(sc);   
         FlyinDetailsController.getDepatureCities('test');
         cont.showuploadpage();
         
         Apexpages.currentpage().getparameters().put('recid', td.id);
         cont.confirmTicketStatus();
    
    }
}