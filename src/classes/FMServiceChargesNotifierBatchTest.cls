/************************************************************************************
* Description - Test Class for class FMServiceChargesNotifierBatch                  *
*                                                                                   *
* Version   Date            Author              Description                         *
* 1.0       19/02/2019      Aishwarya Todkar    Initial Draft.                      *
* 1.1       04/03/2019      Aishwarya Todkar    1.Tested more filter criterias in   * 
*                                                 SOQL.                             *
*                                               2.Tested validation for Qatar and   * 
*                                                 JORDAN Countries.                 *   
*************************************************************************************/
@isTest
public class FMServiceChargesNotifierBatchTest {

    @isTest
    public static void testEmail_UAE1() {
        
        List<FM_Service_Charges_properties__c> settingLst2 = new List<FM_Service_Charges_properties__c>();
        FM_Service_Charges_properties__c newSetting1 = new FM_Service_Charges_properties__c(Name= 'abc');
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        
         //Create Customer record
        Id businessRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account objAcc = new Account(Name = 'Test Account',
                                    recordtypeid = businessRecTypeId,
                                    email__c = 'tt@tfdst.com',
                                    ZBEmailStatus__c = 'Valid',
                                    Agency_Email__c = 'test1@t.com',
                                    Alternate_Agency_Email__c = 'test2@t.com',
                                    Alternate_Email__c = 'test3@t.com',
                                    Email_1__c = 'test@t.com',
                                    Email_1_Person_Name__c = 'test4@t.com',
                                    Email_2__c = 'test@t.com',
                                    Email_2_Person_Name__c = 'test4@t.com',
                                    Email_3__c = 'test@t.com',
                                    Email_3_Person_Name__c = 'test4@t.com',
                                    Old_Email__c = 'test5@t.com'
                                    //Email_2__pc = 'test5@t.com',
                                    //Email_3__pc = 'test5@t.com',
                                    //Email_4__pc = 'test5@t.com',
                                    //Email_5__pc = 'test5@t.com'
                                    );
        insert objAcc;
        
        //Create Property
        Property__c  objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        //Create Inventory 
        Inventory__c  objInventory = TestDataFactory_CRM.createInventory(objProp.Id);
        insert objInventory;
        
        //Create Deal
        NSIBPM__Service_Request__c  objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR;
        
        //Create Bookings
        List<Booking__c> listBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id, 1);
        insert listBookings;
        
        //crearte booking unit
        List<Booking_Unit__c> listBUs =  TestDataFactory_CRM.createBookingUnits(listBookings, 1);
        listBUs[0].Inventory__c = objInventory.Id;
        listBUs[0].FM_Outstanding_Amount__c = '4000';
        listBUs[0].Property_Country__c = 'United Arab Emirates';
        listBUs[0].Email__c = '321test@test.com';
        listBUs[0].Handover_Flag__c = 'Y';
        listBUs[0].Registration_Status_Code__c= 'MM';
        //listBUs[0].Registration_Status__c= 'executed';
        listBUs[0].SC_From_RentalIncome__c = false;
        listBUs[0].ByPassFMReminder__c = false;
        insert listBUs;
        system.debug('listBUs== ' + listBUs);
        //Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());

        String UNIT_SOA_str = String.format(FmIpmsRestServices.SF_REST_BASEURL+FmIpmsRestServices.UNIT_SOA_BY_REGISTRATIONID, new List<String> {listBUs[0].Registration_ID__c});
        FmHttpCalloutMock.Response unitSoaMockResponse = new FmHttpCalloutMock.Response(200, 'Success',  '   {  '  + 
        '       "responseId": "5544836",  '  + 
        '       "responseTime": "Tue Nov 26 14:34:50 GMT+04:00 2019",  '  + 
        '       "actions": [  '  + 
        '           {  '  + 
        '               "action": "VIEW_FILE",  '  + 
        '               "method": "GET",  '  + 
        '               "url": "https://ptc.damacgroup.com/DCOFFEE/show/header/466b5a90811f3b7b4689b1741679aff5"  '  + 
        '           }  '  + 
        '       ],  '  + 
        '       "complete": true  '  + 
        '  }  ');

        String blobStr = 'Test Blob Response';
        FmHttpCalloutMock.Response blobResponse = new FmHttpCalloutMock.Response(200, 'Success', blobStr);
        FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(202, 'Accepted', '');

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
            UNIT_SOA_str => unitSoaMockResponse,
            'https://ptc.damacgroup.com/DCOFFEE/show/header/466b5a90811f3b7b4689b1741679aff5' => blobResponse,
            'https://api.sendgrid.com/v3/mail/send' => sendGridResponse
        };
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));

        Test.StartTest();
        DataBase.executeBatch(new FMServiceChargesNotifierBatch(), 1);
        Test.StopTest();
    }
    @isTest
    public static void testEmail_UAE2() {
        
        List<FM_Service_Charges_properties__c> settingLst2 = new List<FM_Service_Charges_properties__c>();
        FM_Service_Charges_properties__c newSetting1 = new FM_Service_Charges_properties__c(Name= 'abc');
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
         //Create Customer record
         Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        //Create Customer record
        Account objAcc = new Account(FirstName='Test FirstName'
                                    , LastName='Test LastName'
                                    , Email__pc = 'test123@tmail.com'
                                    , recordTypeId = personAccRTId
                                    , ZBEmailStatus__c = 'Valid'
                                    , Email_2__pc = 'test@t.com');
        insert objAcc;
        
        //Create Property
        Property__c  objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        //Create Inventory 
        Inventory__c  objInventory = TestDataFactory_CRM.createInventory(objProp.Id);
        insert objInventory;
        
        //Create Deal
        NSIBPM__Service_Request__c  objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR;
        
        //Create Bookings
        List<Booking__c> listBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id, 1);
        insert listBookings;
        
        //crearte booking unit
        List<Booking_Unit__c> listBUs =  TestDataFactory_CRM.createBookingUnits(listBookings, 1);
        listBUs[0].Inventory__c = objInventory.Id;
        listBUs[0].FM_Outstanding_Amount__c = '1';
        listBUs[0].Email__c = '321test@test.com';
        listBUs[0].Property_Country__c = 'United Arab Emirates';
        listBUs[0].Handover_Flag__c = 'Y';
        listBUs[0].Registration_Status_Code__c= 'MM';
        //listBUs[0].Registration_Status__c= 'executed';
        listBUs[0].SC_From_RentalIncome__c = false;
        insert listBUs;
        system.debug('listBUs== ' + listBUs);
        //Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());

        String UNIT_SOA_str = String.format(FmIpmsRestServices.SF_REST_BASEURL+FmIpmsRestServices.UNIT_SOA_BY_REGISTRATIONID, new List<String> {listBUs[0].Registration_ID__c});
        FmHttpCalloutMock.Response unitSoaMockResponse = new FmHttpCalloutMock.Response(200, 'Success',  '   {  '  + 
        '       "responseId": "5544836",  '  + 
        '       "responseTime": "Tue Nov 26 14:34:50 GMT+04:00 2019",  '  + 
        '       "actions": [  '  + 
        '           {  '  + 
        '               "action": "VIEW_FILE",  '  + 
        '               "method": "GET",  '  + 
        '               "url": "https://ptc.damacgroup.com/DCOFFEE/show/header/466b5a90811f3b7b4689b1741679aff5"  '  + 
        '           }  '  + 
        '       ],  '  + 
        '       "complete": true  '  + 
        '  }  ');

        String blobStr = 'Test Blob Response';
        FmHttpCalloutMock.Response blobResponse = new FmHttpCalloutMock.Response(200, 'Success', blobStr);
        FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(202, 'Accepted', '');

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
            UNIT_SOA_str => unitSoaMockResponse,
            'https://ptc.damacgroup.com/DCOFFEE/show/header/466b5a90811f3b7b4689b1741679aff5' => blobResponse,
            'https://api.sendgrid.com/v3/mail/send' => sendGridResponse
        };
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));

        Test.StartTest();
        DataBase.executeBatch(new FMServiceChargesNotifierBatch(), 1);
        Test.StopTest();
    }

    @isTest
    public static void testQatar() {
        
        List<FM_Service_Charges_properties__c> settingLst2 = new List<FM_Service_Charges_properties__c>();
        FM_Service_Charges_properties__c newSetting1 = new FM_Service_Charges_properties__c(Name= 'abc');
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
         //Create Customer record
        Id businessRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account objAcc = new Account(Name = 'Test Account',
                                    recordtypeid = businessRecTypeId,
                                    email__c = 'tt@tfdst.com',
                                    ZBEmailStatus__c = 'Valid');
        insert objAcc;
        
        //Create Property
        Property__c  objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        //Create Inventory 
        Inventory__c  objInventory = TestDataFactory_CRM.createInventory(objProp.Id);
        insert objInventory;
        
        //Create Deal
        NSIBPM__Service_Request__c  objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR;
        
        //Create Bookings
        List<Booking__c> listBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id, 1);
        insert listBookings;
        
        //crearte booking unit
        List<Booking_Unit__c> listBUs =  TestDataFactory_CRM.createBookingUnits(listBookings, 1);
        listBUs[0].Inventory__c = objInventory.Id;
        listBUs[0].FM_Outstanding_Amount__c = '1000';
        listBUs[0].Property_Country__c = 'Qatar';
        listBUs[0].Email__c = '321test@test.com';
        listBUs[0].Handover_Flag__c = 'Y';
        listBUs[0].Registration_Status_Code__c= 'MM';
        //listBUs[0].Registration_Status__c= 'executed';
        listBUs[0].SC_From_RentalIncome__c = false;
        insert listBUs;
        system.debug('listBUs== ' + listBUs);
        //Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());

        String UNIT_SOA_str = String.format(FmIpmsRestServices.SF_REST_BASEURL+FmIpmsRestServices.UNIT_SOA_BY_REGISTRATIONID, new List<String> {listBUs[0].Registration_ID__c});
        FmHttpCalloutMock.Response unitSoaMockResponse = new FmHttpCalloutMock.Response(200, 'Success',  '   {  '  + 
        '       "responseId": "5544836",  '  + 
        '       "responseTime": "Tue Nov 26 14:34:50 GMT+04:00 2019",  '  + 
        '       "actions": [  '  + 
        '           {  '  + 
        '               "action": "VIEW_FILE",  '  + 
        '               "method": "GET",  '  + 
        '               "url": "https://ptc.damacgroup.com/DCOFFEE/show/header/466b5a90811f3b7b4689b1741679aff5"  '  + 
        '           }  '  + 
        '       ],  '  + 
        '       "complete": true  '  + 
        '  }  ');

        String blobStr = 'Test Blob Response';
        FmHttpCalloutMock.Response blobResponse = new FmHttpCalloutMock.Response(200, 'Success', blobStr);
        FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(202, 'Accepted', '');

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
            UNIT_SOA_str => unitSoaMockResponse,
            'https://ptc.damacgroup.com/DCOFFEE/show/header/466b5a90811f3b7b4689b1741679aff5' => blobResponse,
            'https://api.sendgrid.com/v3/mail/send' => sendGridResponse
        };
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));

        Test.StartTest();
        DataBase.executeBatch(new FMServiceChargesNotifierBatch(), 1);
        Test.StopTest();
    }
    @isTest
    public static void testEmail_Lebanon() {
        
        List<FM_Service_Charges_properties__c> settingLst2 = new List<FM_Service_Charges_properties__c>();
        FM_Service_Charges_properties__c newSetting1 = new FM_Service_Charges_properties__c(Name= 'abc');
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
         //Create Customer record
        Id businessRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account objAcc = new Account(Name = 'Test Account',
                                    recordtypeid = businessRecTypeId,
                                    email__c = 'tt@tfdst.com',
                                    ZBEmailStatus__c = 'Valid');
        insert objAcc;
        
        //Create Property
        Property__c  objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        //Create Inventory 
        Inventory__c  objInventory = TestDataFactory_CRM.createInventory(objProp.Id);
        insert objInventory;
        
        //Create Deal
        NSIBPM__Service_Request__c  objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR;
        
        //Create Bookings
        List<Booking__c> listBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id, 1);
        insert listBookings;
        
        //crearte booking unit
        List<Booking_Unit__c> listBUs =  TestDataFactory_CRM.createBookingUnits(listBookings, 1);
        listBUs[0].Inventory__c = objInventory.Id;
        listBUs[0].FM_Outstanding_Amount__c = '1000';
        listBUs[0].Property_Country__c = 'Lebanon';
        listBUs[0].Email__c = '321test@test.com';
        listBUs[0].Handover_Flag__c = 'Y';
        listBUs[0].Registration_Status_Code__c= 'MM';
        //listBUs[0].Registration_Status__c= 'executed';
        listBUs[0].SC_From_RentalIncome__c = false;
        insert listBUs;
        system.debug('listBUs== ' + listBUs);
        //Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());

        String UNIT_SOA_str = String.format(FmIpmsRestServices.SF_REST_BASEURL+FmIpmsRestServices.UNIT_SOA_BY_REGISTRATIONID, new List<String> {listBUs[0].Registration_ID__c});
        FmHttpCalloutMock.Response unitSoaMockResponse = new FmHttpCalloutMock.Response(200, 'Success',  '   {  '  + 
        '       "responseId": "5544836",  '  + 
        '       "responseTime": "Tue Nov 26 14:34:50 GMT+04:00 2019",  '  + 
        '       "actions": [  '  + 
        '           {  '  + 
        '               "action": "VIEW_FILE",  '  + 
        '               "method": "GET",  '  + 
        '               "url": "https://ptc.damacgroup.com/DCOFFEE/show/header/466b5a90811f3b7b4689b1741679aff5"  '  + 
        '           }  '  + 
        '       ],  '  + 
        '       "complete": true  '  + 
        '  }  ');

        String blobStr = 'Test Blob Response';
        FmHttpCalloutMock.Response blobResponse = new FmHttpCalloutMock.Response(200, 'Success', blobStr);
        FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(202, 'Accepted', '');

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
            UNIT_SOA_str => unitSoaMockResponse,
            'https://ptc.damacgroup.com/DCOFFEE/show/header/466b5a90811f3b7b4689b1741679aff5' => blobResponse,
            'https://api.sendgrid.com/v3/mail/send' => sendGridResponse
        };
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
            
        Test.StartTest();
        DataBase.executeBatch(new FMServiceChargesNotifierBatch(), 1);
        Test.StopTest();
    }
    
    /*@isTest
    public static void testBulkEmail() {
         //Create Customer record
        Account objAcc = new Account(Name = 'Test Account');
        insert objAcc;
        
        //Create Property
        Property__c  objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        //Create Inventory 
        Inventory__c  objInventory = TestDataFactory_CRM.createInventory(objProp.Id);
        insert objInventory;
        
        //Create Deal
        NSIBPM__Service_Request__c  objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR;
        
        //Create Bookings
        List<Booking__c> listBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id, 1);
        insert listBookings;
        
        //crearte booking unit
        List<Booking_Unit__c> listBUs =  TestDataFactory_CRM.createBookingUnits(listBookings, 101);
        
        //listBUs[0].Inventory__c = objInventory.Id;
        integer index = 1;
        for(Booking_Unit__c objBU : listBUs) {
            objBU.FM_Outstanding_Amount__c = String.valueOf(index);
            objBU.Email__c = index + 'test@test.com';
        }
        insert listBUs;
        system.debug('listBUs== ' + listBUs);
        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
        Test.StartTest();
        DataBase.executeBatch(new FMServiceChargesNotifierBatch(), 101);
        Test.StopTest();
    }*/
}