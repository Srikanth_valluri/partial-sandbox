/**************************************************************************************************
* Name               : Invocable_UpdateRegnStatus
* Description        : Invocable Class to Update Registration Status
* Test Class         : Invocable_UpdateRegnStatus_Test                        
* Created By         : DAMAC IT                                                     
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR               DATE             Comments
* 1.0                            16/06/2019         Created
* 1.1         QBurst             02/04/2020         SOAP to REST change in Webservice call
**************************************************************************************************/
public class Invocable_UpdateRegnStatus {
    @InvocableMethod
    public static void UpdateBuyer(List<Id> stepId) {
        for (New_Step__c step :[SELECT Id, Service_Request__c FROM New_Step__c WHERE Id IN : stepId]) {
            evaluateCustomCode(step);
        }
    }

    public static String evaluateCustomCode(New_Step__c step) {
        String retStr = 'Success';
        List<Id> bookingIds = new List<Id>();
        try {
            for (Booking__c bookings :[SELECT Id FROM Booking__c WHERE Deal_SR__c = : step.Service_Request__c]) {
                bookingIds.add(bookings.Id);
            }
            if (bookingIds.size() > 0) {
                system.debug('#### invoking CC_UpdateRegnStatus');
                //system.enqueueJob(new AsyncReceiptWebservice(bookingIds, 'StatusUpdate'));// commented out as part of 1.1
                 DAMAC_IPMS_PARTY_CREATION.statusUpdate(bookingIds, '', step.Id); // 1.1
            }
        } catch(Exception e) {
            retStr = 'Error :' + e.getMessage() + '';
        }
        return retStr;
    }
}