/*--------------------------------------------------------------------------------------------------
Description: Controller for New_Inquiry

====================================================================================================
 Version | Date(DD-MM-YYYY) | Last Modified By | Comments                                           
----------------------------------------------------------------------------------------------------
 1.0     |        |    | 1. Initial draft                                   
----------------------------------------------------------------------------------------------------
=================================================================================================
*/
public without sharing class NewInqControllerdup {
    public Inquiry__c objInquiry                    {get; set;}
    public String userId                            {get; set;}
    public String msgType                           {get; set;}
    public String requiredCls                       {get; set;}
    public String msg                               {get; set;}
    public String selectedStand                     {get; set;}
    public Boolean showMessage                      {get; set;}
    public Boolean showInqPanel                      {get; set;}
    public InquiryShareWithManager inqShareObj;
    public string selectedStandStr{get;set;}
    map<String,String> mapStandVal_Label;

    public String selectedCountryCode{get;set;}
    public String selectedAlternateCode{get;set;}
    public String selectedNationality{get;set;}
    public String selectedCountry{get;set;}
    public String selectedCity{get;set;}
    public String selectedLanguage{get;set;}
	public List<String> lstCity{get;set;}    
    public static Map<String, List<String>> controllingInfo;
    
    public String RM_Email{get;set;}
    public List<SelectOption> lstInquirySource{get;set;}
    public List<SelectOption> lstTitle{get;set;}
    public List<SelectOption> lstNationality{get;set;}
    public List<SelectOption> lstCountryPerRes{get;set;}
    public List<SelectOption> lstCityPerRes{get;set;}
    public List<SelectOption> lstBudget{get;set;}
    public List<SelectOption> lstNoBedRooms{get;set;}
    public List<SelectOption> lstPrimaryContacts{get;set;}
    public List<SelectOption> lstPreferredLanguage{get;set;}
    public List<SelectOption> lstAccompaniedByAgent{get;set;}
    public List<SelectOption> lstHowDidYouHearAboutUs{get;set;}
    public List<SelectOption> lstPropertyTypeInterested{get;set;}
    public List<SelectOption> lstPropertyRequiredFor{get;set;}
    public List<SelectOption> lstMobileCountryCode{get;set;}
    
    

    public NewInqControllerdup() {
        showInqPanel = false;
        mapStandVal_Label = new map<String,String>();
        showMessage = false;
        objInquiry = new Inquiry__c();
        
        selectedCountryCode = '';
        selectedAlternateCode = '';
        selectedNationality = '';
        selectedCountry = '';
        selectedCity = '';
        selectedLanguage = '';
        controllingInfo = new Map<String, List<String>>();

        lstInquirySource = new List<SelectOption>();
        lstTitle = new List<SelectOption>();
        lstNationality = new List<SelectOption>();
        lstCountryPerRes = new List<SelectOption>();
        lstCityPerRes = new List<SelectOption>();
        lstBudget = new List<SelectOption>();
        lstNoBedRooms = new List<SelectOption>();
        lstPrimaryContacts = new List<SelectOption>();
        lstPreferredLanguage = new List<SelectOption>();
        lstAccompaniedByAgent = new List<SelectOption>();
        lstHowDidYouHearAboutUs = new List<SelectOption>();
        lstPropertyTypeInterested = new List<SelectOption>();
        lstPropertyRequiredFor = new List<SelectOption>();
        lstMobileCountryCode = new List<SelectOption>();
                
        buildPicklist();
    }

    public void buildPicklist() {
        lstInquirySource = getPicklistValues('Inquiry__c', 'Inquiry_Source__c');
        lstPrimaryContacts = getPicklistValues('Inquiry__c', 'Primary_Contacts__c');
        lstTitle = getPicklistValues('Inquiry__c', 'Title__c');
        lstNationality = getPicklistValues('Inquiry__c', 'Nationality__c');
        lstCountryPerRes = getPicklistValues('Inquiry__c', 'Country_of_Permanent_Residence__c');
        lstCityPerRes = getPicklistValues('Inquiry__c', 'City_of_Permanent_Residence__c');
        lstBudget = getPicklistValues('Inquiry__c', 'Budget__c');
        lstNoBedRooms = getPicklistValues('Inquiry__c', 'No_Of_Bed_Rooms_Availble_in_SF__c');
        lstPreferredLanguage = getPicklistValues('Inquiry__c', 'Preferred_Language__c');
        lstAccompaniedByAgent = getPicklistValues('Inquiry__c', 'Accompanied_By_Agent__c');
        lstHowDidYouHearAboutUs = getPicklistValues('Inquiry__c', 'How_did_you_hear_about_us__c');
        lstPropertyTypeInterested = getPicklistValues('Inquiry__c', 'Property_Type_Interested__c');
        lstPropertyRequiredFor = getPicklistValues('Inquiry__c', 'Property_Required_For__c');
        lstMobileCountryCode = getPicklistValues('Inquiry__c', 'Mobile_CountryCode__c');
    }

    public pageReference cancel() {
        pageReference pg = new pageReference('/home/home.jsp');
        return pg;
    }

    public static List<SelectOption> getPicklistValues(String ObjectApi_name,String Field_name){
        List<SelectOption> lstPickvals=new List<SelectOption>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType();
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe();
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
        List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues();
        lstPickvals.add(new SelectOption('None','--None--'));
        for (Schema.PicklistEntry a : pick_list_values) {
            lstPickvals.add(new SelectOption(a.getValue(),a.getValue()));
        }
        return lstPickvals;
    }

    public static void getFieldDependencies(String objectName, String controllingField, String dependentField)
    {
        controllingInfo = new Map<String, List<String>>();

        Schema.SObjectType objType = Schema.getGlobalDescribe().get(objectName);

        Schema.DescribeSObjectResult describeResult = objType.getDescribe();
        Schema.DescribeFieldResult controllingFieldInfo = describeResult.fields.getMap().get(controllingField).getDescribe();
        Schema.DescribeFieldResult dependentFieldInfo = describeResult.fields.getMap().get(dependentField).getDescribe();

        List<Schema.PicklistEntry> controllingValues = controllingFieldInfo.getPicklistValues();
        List<Schema.PicklistEntry> dependentValues = dependentFieldInfo.getPicklistValues();

        for(Schema.PicklistEntry currControllingValue : controllingValues)
        {
            //System.debug('ControllingField: Label:' + currControllingValue.getLabel());
            controllingInfo.put(currControllingValue.getValue(), new List<String>());
        }
        for(Schema.PicklistEntry currDependentValue : dependentValues)
        {
            String jsonString = JSON.serialize(currDependentValue);
            MyPickListInfo info = (MyPickListInfo) JSON.deserialize(jsonString, MyPickListInfo.class);
            String hexString = EncodingUtil.convertToHex(EncodingUtil.base64Decode(info.validFor)).toUpperCase();
			Integer baseCount = 0;

            for(Integer curr : hexString.getChars())
            {
                Integer val = 0;
                if(curr >= 65)
                {
                    val = curr - 65 + 10;
                }
                else
                {
                    val = curr - 48;
                }
                if((val & 8) == 8)
                {
                    controllingInfo.get(controllingValues[baseCount + 0].getValue()).add(currDependentValue.getValue());
                }
                if((val & 4) == 4)
                {
                    controllingInfo.get(controllingValues[baseCount + 1].getValue()).add(currDependentValue.getValue());
                }
                if((val & 2) == 2)
                {
                    controllingInfo.get(controllingValues[baseCount + 2].getValue()).add(currDependentValue.getValue());
                }
                if((val & 1) == 1)
                {
                    controllingInfo.get(controllingValues[baseCount + 3].getValue()).add(currDependentValue.getValue());
                }
                baseCount += 4;
            }
        }
    }

    public void updateCityList() {
        getFieldDependencies('Inquiry__c','Country__c','City_new__c');
        if(controllingInfo.containsKey(selectedCountry.trim())){
            lstCity = controllingInfo.get(selectedCountry.trim());
        } else {
            System.debug('=Not ====contains ===' + selectedCountry);
        }
    }

    public pageReference submitInquiry() {
        objInquiry.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByDeveloperName().get('Inquiry').getRecordTypeId();
        objInquiry.OwnerId = UserInfo.getUserId();
        showMessage = true;
        system.debug('objInquiry ==> '+objInquiry);
        try {
            //insert objInquiry;
            //objInquiry = new Inquiry__c();
            //RM_Email = '';
            //userId = null;
            msgType = 'success';
            msg = ' Inquiry has been created successfully';
            ApexPages.addmessage(new ApexPages.message(
                ApexPages.severity.Info,
                'Inquiry has been created successfully'
            ));
        } catch(System.DmlException excp) {
            msgType = 'error';
            msg = excp.getMessage() 
                + ': '
                + excp.getLineNumber();
            ApexPages.addmessage(new ApexPages.message(
                ApexPages.severity.Error, 
                excp.getMessage() + ': ' + excp.getLineNumber()
            ));
        }
        return null;
    }

    public class MyPickListInfo
   {
        public String validFor;
   }
}