/**********************************************************************************************************************
Class Name : API_GetAgentCommisionDetails 
Description: To fetch full details of an agent's commision for a specific deal.
Test Class : API_GetAgentCommisionDetailsTest
========================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.1     |   15-12-2020      | Subin C Antony	  | Initial Draft
***********************************************************************************************************************/

@RestResource(urlMapping='/API_GetAgentCommisionDetails')
global without sharing class API_GetAgentCommisionDetails {

    @HttpGET
    global static void doGET() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        RestContext.response.addHeader('Content-Type', 'application/json');
        String errorMsg;
        
        String commisionId = req.params.containskey('commision_id') ? req.params.get('commision_id') : '';
        if(String.isBlank(commisionId)){
            errorMsg = 'Please provide the id of the commision record for which details should be fetched.';
            getErrorResponse(3, errorMsg, errorMsg);
            return;
        }
        
        Agent_Commission__c commisionObj;
        try {
            commisionObj = [SELECT id, name, Agency__r.name, Broker_Name__c, Amount__c, DP_Amount__c, DP_Amount_Received__c, 
                            Percent_Applied__c, Booking_Unit__c, Booking_Unit__r.Unit_Location__c, 
                            Booking_Unit__r.Property_Name_Inventory__c, Booking_Unit__r.Registration_Date__c, 
                            Booking_Unit__r.Registration_ID__c, Booking__r.Deal_Sr__r.NSIBPM__External_Status_Code__c, 
                            Booking__r.Deal_SR__r.Agent_Name__c, Booking__r.Deal_SR__r.Agent_Name__r.name, 
                            Booking__r.Deal_SR__r.Seibel_Agent_No__c 
                            FROM Agent_Commission__c WHERE id = :commisionId LIMIT 1];
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
            commisionObj = NULL;
        }
        if(NULL == commisionObj) {
            errorMsg = 'Could not find record with given id.';
            getErrorResponse(2, errorMsg, errorMsg);
            return;
        }
        
        String agentName = (NULL == commisionObj.Booking__r.Deal_SR__r.Agent_Name__c ? 
                            '' : commisionObj.Booking__r.Deal_SR__r.Agent_Name__r.name); /* commisionObj.Broker_Name__c */
        String agentID = (NULL == commisionObj.Booking__r.Deal_SR__r.Seibel_Agent_No__c ? 
                              commisionObj.Booking__r.Deal_SR__r.Agent_Name__c : commisionObj.Booking__r.Deal_SR__r.Seibel_Agent_No__c);
        String agencyName = (NULL == commisionObj.Agency__c ? '' : commisionObj.Agency__r.name);
        String projectName = (NULL == commisionObj.Booking_Unit__c ? '' : commisionObj.Booking_Unit__r.Property_Name_Inventory__c);
        String unitName = (NULL == commisionObj.Booking_Unit__c ? '' : commisionObj.Booking_Unit__r.Unit_Location__c);
        Decimal commisionAmount = (NULL == commisionObj.Amount__c ? 0.00 : commisionObj.Amount__c);
        Decimal dealAmount = (NULL == commisionObj.DP_Amount__c ? 0.00 : commisionObj.DP_Amount__c);
        Decimal commisionRate = (dealAmount <= 0.00 ? 0.00 : (100 * commisionAmount)/dealAmount);
        commisionRate = commisionRate.setScale(2, RoundingMode.HALF_UP);
        Date salesDate = commisionObj.Booking_Unit__r.Registration_Date__c;
        DateTime saleDT = (NULL == salesDate ? NULL : DateTime.newInstance(salesDate, Time.newInstance(12, 0, 0, 0)));
        
        Set <String> regIdSet = new Set<String>();
        if(NULL == commisionObj.Booking_Unit__c || NULL == commisionObj.Booking_Unit__r.Registration_ID__c) {
            errorMsg = 'Could not retrieve Registration ID for the BU.';
            getErrorResponse(3, errorMsg, errorMsg);
            return;
        }
        
        // TODO IPMS/EPM callout for further detail
        regIdSet.add(commisionObj.Booking_Unit__r.Registration_ID__c);
        IPMS_ResponseWraper remotePaymentStatusResponse = getAgentCommissionDetailFromIPMS(regIdSet);
        Map <String, IPMS_ResponseDataItem> paymentStatusMapByRegId = new Map<String, IPMS_ResponseDataItem>();
        if(remotePaymentStatusResponse.meta_data.status_code == 1 && NULL != remotePaymentStatusResponse.data && remotePaymentStatusResponse.data.size() > 0) {
            for(IPMS_ResponseDataItem item : remotePaymentStatusResponse.data){
                if(!paymentStatusMapByRegId.containsKey(item.registration_id)) {
                    paymentStatusMapByRegId.put(item.registration_id, item);
                }
                else {
                    // TODO handle 2 items with same regID: (considering only the first one now) !!!!!
                }
            }
        }
        System.debug('IPMS_DATA_MAP  : ............................................ ' + paymentStatusMapByRegId);
        IPMS_ResponseDataItem IPMS_Details = paymentStatusMapByRegId.get(commisionObj.Booking_Unit__r.Registration_ID__c);
        if(Test.isRunningTest()) {
            IPMS_Details = paymentStatusMapByRegId.get('105646'); // mock response regID
        }
        if(NULL == IPMS_Details) {
            errorMsg = 'Could not retrieve commision policy detail from IPMS/EPM.';
            system.debug(errorMsg);
            // getErrorResponse(2, errorMsg, errorMsg);
            // return;
        }
        
        ResponseWrapper responseWrapper = new ResponseWrapper();
        
        HDApp_Utility.cls_meta_data responseMetaData = HDApp_Utility.ReturnMetaResponse(
            'Success in fetching commission details', NULL, 1);
        responseWrapper.meta_data = responseMetaData;
        
        cls_data responseData = new cls_data();
        responseData.commision_id = commisionObj.id;
        responseData.commision_name = commisionObj.name;
        responseData.project_name = getInProperCase(projectName);
        responseData.unit_name = unitName;
        responseData.registration_id = commisionObj.Booking_Unit__r.Registration_ID__c;
        responseData.agent_name = getInProperCase(agentName);
        responseData.agent_id = agentID;
        responseData.agency_name = getInProperCase(agencyName);
        responseData.sale_date = salesDate;
        responseData.sale_date_string = (NULL == saleDT ? '' : saleDT.format('dd.MM.yyyy'));
        responseData.agent_commision = commisionAmount;
        responseData.brokerage = 'AED ' + commisionAmount;
        responseData.agent_commision_rate = commisionRate + ' %';
        if(NULL != IPMS_Details) {
            DateTime epmSaleDT = Datetime.valueOf(IPMS_Details.sale_date);
            DateTime epmDocDT = Datetime.valueOf(IPMS_Details.document_date);
            
            responseData.agent_id = IPMS_Details.agent_id;
            responseData.registration_id = IPMS_Details.registration_id;
            responseData.unit_name = IPMS_Details.unique_unit_code;
            responseData.agent_commision = IPMS_Details.brokerage;
            responseData.brokerage = 'AED ' + IPMS_Details.brokerage;
            responseData.agent_commision_rate = IPMS_Details.rate;
            responseData.policy_ref_no = IPMS_Details.policy_ref_no;
            responseData.sale_date = epmSaleDT.date();
            responseData.sale_date_string = (NULL == epmSaleDT ? '' : epmSaleDT.format('dd.MM.yyyy'));
            responseData.document_date = epmDocDT.date();
            responseData.document_date_string = (NULL == epmDocDT ? '' : epmDocDT.format('dd.MM.yyyy'));
        }
        else {
            responseMetaData.message = 'WARNING: Could not fetch policy details on commission from IPMS/EPM'; // COMMENT LATER
        }
        responseWrapper.data = responseData;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
    }
    
    public static String getInProperCase(String name) {
        if(String.isBlank(name)) {
            return name;
        } else {
            // name = name.trim().toLowerCase().capitalize();
            name = name.trim().toLowerCase();
        }
        
        String[] words = name.split(' ');
        String properName = '', seperator = '';
        for(String word : words) {
            word = word.capitalize();
            properName += (seperator + word);
            seperator = ' ';
        }
        
        return properName;
    }
    
    private static void getErrorResponse(Integer statusCode, String responseMessage, String devMessage) {
        ResponseWrapper responseWrapper = new ResponseWrapper();
        HDApp_Utility.cls_meta_data responseMetaData = HDApp_Utility.ReturnMetaResponse(
            responseMessage, devMessage, statusCode);
        responseWrapper.meta_data = responseMetaData;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
    }
    
    private static IPMS_ResponseWraper getAgentCommissionDetailFromIPMS(Set<String> regIds) {
        IPMS_ResponseWraper responseWraper = new IPMS_ResponseWraper();
        List <IPMS_ResponseDataItem> responseDataList = new List<IPMS_ResponseDataItem>();
        
        if(regIds.size() < 1) {
            responseWraper.meta_data = HDApp_Utility.ReturnMetaResponse('Empty list of Arguments', NULL, 3);
        }
        else {
            String regIdStringQuery = '';
            String seperator = '';
            for(String regId : regIds){
                regIdStringQuery += (seperator + regId);
                seperator = ',';
            }
            System.debug('REG_ID_QUERY_String : ............................................ ' + regIdStringQuery);
            
            HttpRequest paymentStatusRequest = new HttpRequest();
            // String baseUrl = HDApp_Utility.IsSandbox ? 'https://ptctest.damacgroup.com/agent-app' : 'https://ptc.damacgroup.com/agentapp-prod';
            String baseUrl = HDApp_Utility.IsSandbox ? 'https://ptc.damacgroup.com/agentapp-prod' : 'https://ptc.damacgroup.com/agentapp-prod'; // CHANGE STG bseURL Later
            paymentStatusRequest.setEndpoint(baseUrl + '/api/v1/customers/commission-details?reg-id=' + regIdStringQuery);
            paymentStatusRequest.setMethod('GET');
            paymentStatusRequest.setTimeout(120000);
            
            HttpResponse paymentStatusResponse;
            String exceptionMsg;
            try{
                if(!Test.isRunningTest()) {
                    paymentStatusResponse = new Http().send(paymentStatusRequest);
                }
                else { // for test excecution context
                    paymentStatusResponse  = new HttpResponse();
                    paymentStatusResponse.setStatus('OK');
                    paymentStatusResponse.setStatusCode(200);
                    paymentStatusResponse.setBody('{"data":[{"brokerage":6704,"rate":"[4%],[1%]","agent_name":"BIG POCKETS COMMERCIAL BROKERS L.L.C","agent_id":607120,"policy_ref_no":"B-10,B-20","registration_id":105646,"unique_unit_code":"AYKCC/32/3218","sale_date":"2019-02-03 00:00:00.0","document_date":"2019-02-03 00:00:00.0"}],"meta_data":{"status_code":1,"message":"Success","title":"Success","developer_message":null}}');
                }
                
                system.debug('IPMS_CALLOUT_Response : ............................................ ' + paymentStatusResponse);
                system.debug('IPMS_CALLOUT_ResponseStatus : ............................................ ' + paymentStatusResponse.getStatusCode());
                system.debug('IPMS_CALLOUT_ResponseBody : ............................................ ' + paymentStatusResponse.getBody());
                if(paymentStatusResponse.getStatusCode() == 200 && NULL != paymentStatusResponse.getBody() && 
                   !(paymentStatusResponse.getBody().equals(''))){
                    responseWraper = (IPMS_ResponseWraper)JSON.deserialize(paymentStatusResponse.getBody(), IPMS_ResponseWraper.class);
                }
            }
            catch(Exception ex){
                system.debug(ex.getMessage());
                system.debug(ex.getStackTraceString());
                responseWraper = NULL;
                exceptionMsg = ex.getMessage() + ex.getStackTraceString();
            }
            
            if(NULL == responseWraper || NULL == responseWraper.data || responseWraper.data.size() < 1) {
                responseWraper = new IPMS_ResponseWraper();
                responseWraper.meta_data = HDApp_Utility.ReturnMetaResponse('Error while fetching data', exceptionMsg, 2);
            }
        }
        
        System.debug('IPMS_CALLOUT_RESPONSE_WRAP : ................................... ' + responseWraper);
        return responseWraper;
    }
    
    public class IPMS_ResponseWraper {
        public HDApp_Utility.cls_meta_data meta_data;
        public List <IPMS_ResponseDataItem> data;
    }
    
    public class IPMS_ResponseDataItem {
        public String registration_id;
        public String agent_id;
        public String agent_name;
        public String unique_unit_code;
        public String sale_date;
        public String document_date;
        public String policy_ref_no;
        public String rate;
        public Decimal brokerage;
    }

    /* Wrapper classes for returning reponse */
    public class ResponseWrapper {
        public HDApp_Utility.cls_meta_data meta_data;
        public cls_data data;
    }

    public class cls_data {
        public String commision_id;
        public String commision_name;
        public String project_name;
        public String unit_name;
        public String registration_id;
        public String agent_name;
        public String agent_id;
        public String agency_name;
        public Date sale_date;
        public String sale_date_string;
        public Date document_date;
        public String document_date_string;
        public Decimal agent_commision;
        public String brokerage;
        public String agent_commision_rate;
        public String policy_ref_no;
    }
}