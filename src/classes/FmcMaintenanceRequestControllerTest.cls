@isTest
public class FmcMaintenanceRequestControllerTest {
    @isTest static void test_method_one() {
        User portalUser = CommunityTestDataFactory.createPortalUser();
        Id portalAccountId = [SELECT Id, Contact.AccountId FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;

        System.debug('portalAccountId = ' + portalAccountId);



        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;


        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(portalAccountId, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Unit_Name__c = 'JNU/SD168/XH2910B';
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            //unit.Inventory__c = inventory.Id;
        }
        insert lstBookingUnit;

        insert TestDataFactory_CRM.createActiveFT_CS();

        CAFM_Contract__c objContract = new CAFM_Contract__c(Name='Test Contract', Contract_Id__c = '611');
        insert objContract;
        Id caseRTId = Schema.SObjectType.CAFM_Building__c.getRecordTypeInfosByName().get('Common area').getRecordTypeId();
        CAFM_Building__c objCAFMBuilding = new CAFM_Building__c();
        objCAFMBuilding.Building_Code__c = 'JNU';
        objCAFMBuilding.RecordTypeId = caseRTId;
        objCAFMBuilding.CAFM_Contract__c = objContract.Id;
        objCAFMBuilding.Common_area_code__c = '45';
        //objCAFMBuilding.IPMS_Building__c = location.Id;
        insert objCAFMBuilding;
        
       Location__c location = new Location__c(
            Name = 'JNU',
            Building_Name__c = 'Test Building',
            Location_ID__c = 'LOC',
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0,
            CAFM_Common_area__c = objCAFMBuilding.Id
        );
        insert location;
        
        
        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        CAFM_Location__c objCAFMLocation = new CAFM_Location__c();
        objCAFMLocation.Description__c = 'DFA/1/A102';
        objCAFMLocation.Location_Id__c = '6543';
        objCAFMLocation.CAFM_Building__c = objCAFMBuilding.Id;
        objCAFMLocation.Building_Sequence_no__c = '45';
        insert objCAFMLocation;

        Test.startTest();

        CustomerCommunityUtils.customerAccountId = portalAccountId;

        PageReference pageRef = Page.CommunityPortal;
        pageRef.getParameters().put('view', 'HelpdeskRequest');
        Test.setCurrentPage(pageRef);
        //Test.setMock( WebServiceMock.class, new MaintenanceRequestMock() );
        Test.setMock( WebServiceMock.class, new MaintenanceRequestTaskMock() );
        System.runAs(portalUser) {
            FmcMaintenanceRequestController controller = new FmcMaintenanceRequestController();
            controller.strUnitId = String.valueOf(lstBookingUnit[0].Id);
            controller.selectUnit();
            controller.selectedOption='Common area';
            controller.selectedLocation=String.valueOf(objCAFMLocation.id);
            controller.getCAFMLocations();
            //controller.getLocationDetails();
            //controller.saveAsDraft();
            controller.submit();
        }
        Test.stopTest();
    }

    @isTest static void test_method_two() {
        User portalUser = CommunityTestDataFactory.createPortalUser();
        Id portalAccountId = [SELECT Id, Contact.AccountId FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;

        System.debug('portalAccountId = ' + portalAccountId);

        Location__c location = new Location__c(
            Name = 'Test Location',
            Building_Name__c = 'Test Building',
            Location_ID__c = 'LOC',
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0
        );
        insert location;

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(portalAccountId, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Unit_Name__c = 'Unit Name';
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            unit.Inventory__c = inventory.Id;
        }
        insert lstBookingUnit;

        insert TestDataFactory_CRM.createActiveFT_CS();

        CAFM_Contract__c objContract = new CAFM_Contract__c(Name='Test Contract', Contract_Id__c = '611');
        insert objContract;
        Id caseRTId = Schema.SObjectType.CAFM_Building__c.getRecordTypeInfosByName().get('Common area').getRecordTypeId();
        CAFM_Building__c objCAFMBuilding = new CAFM_Building__c();
        objCAFMBuilding.Building_Code__c = 'JNU';
        objCAFMBuilding.RecordTypeId = caseRTId;
        objCAFMBuilding.CAFM_Contract__c = objContract.Id;
        objCAFMBuilding.Common_area_code__c = '45';
        objCAFMBuilding.IPMS_Building__c = location.Id;
        insert objCAFMBuilding;
        CAFM_Location__c objCAFMLocation = new CAFM_Location__c();
        objCAFMLocation.Description__c = 'DFA/1/A102';
        objCAFMLocation.Location_Id__c = '6543';
        objCAFMLocation.CAFM_Building__c = objCAFMBuilding.Id;
        objCAFMLocation.Building_Sequence_no__c = '45';
        insert objCAFMLocation;

        Test.startTest();

        CustomerCommunityUtils.customerAccountId = portalAccountId;

        PageReference pageRef = Page.CommunityPortal;
        pageRef.getParameters().put('view', 'HelpdeskRequest');
        Test.setCurrentPage(pageRef);
        //Test.setMock( WebServiceMock.class, new MaintenanceRequestMock() );
        Test.setMock( WebServiceMock.class, new MaintenanceRequestTaskMock() );
        System.runAs(portalUser) {
            FmcMaintenanceRequestController controller = new FmcMaintenanceRequestController();
            controller.strUnitId = String.valueOf(lstBookingUnit[0].Id);
            controller.selectUnit();
            controller.selectedOption='Common area';
            controller.selectedLocation=String.valueOf(objCAFMLocation.id);
            FmcMaintenanceRequestController.getSessionIDfromCAFM();
        }
        Test.stopTest();
    }

    @isTest static void test_getTaskDetails() {
        FM_Case__c obj = new FM_Case__c();
        insert obj;
        Test.setMock(WebServiceMock.class, new TaskMockClass());
        Test.startTest();
        //FmcMaintenanceRequestController.fetchTaskDetails(123,obj.Id);
        Test.stopTest();
    }
}