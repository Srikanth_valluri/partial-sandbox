@isTest
public class AttachmentHandlerUploadDocumentTest {
    
    static testMethod void testAttachments1()
    {
        TestUtility utility = new TestUtility();
        List<Case> caseList = new List<Case>();
        list<id> caseId = new list<id>();
        
        
        Case obj=new Case();

                       obj.Status='New';
                       obj.Origin='Call';
                       obj.Priority='Medium';
                       obj.RecordTypeId = Schema.getGlobalDescribe().get('Case').getDescribe().getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
                       insert obj;
                       caseList.add(obj);
       for (Case caseObj : caseList) {
            caseId.add(caseObj.id);
        }

        List<Attachment> attachList = new List<Attachment>();
        for(Integer i=0;i<100;i++)
            {
                Attachment attobj=new Attachment();

                    attobj.Name='OVERDUE AMOUNT REBATE LETTER';
                    Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
                    attobj.body=bodyBlob;
                    attobj.ContentType = 'application/pdf';

                    attobj.parentId=caseId[0];
                    attachList.add(attobj);
           }//end for
        insert attachList;
        System.debug('attachList' + attachList);
        System.assertEquals(100, attachList.size());
        UploadMultipleDocController.strLabelValue  = 'N';
        Test.startTest();
        UploadMultipleDocController.strLabelValue  = 'N';
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1));
        Test.stopTest();
        AttachmentHandlerUploadDocument controller = new AttachmentHandlerUploadDocument();
        AttachmentHandlerUploadDocument.createSRAttachment(attachList);
    }


    static testMethod void testAttachments2()
    {
        TestUtility utility = new TestUtility();
        List<Case> caseList = new List<Case>();
        list<id> caseId = new list<id>();
        
        
        Account objAcc = TestDataFactory_CRM.createBusinessAccount();
        objAcc.Mobile__c ='1234567890';
        insert objAcc;
                
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'DEA/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        insert objBookingUnit;
    
        Case obj = new Case();
        obj.Status='New';
        obj.Origin='Call';
        obj.Priority='Medium';
        obj.RecordTypeId = Schema.getGlobalDescribe().get('Case').getDescribe().getRecordTypeInfosByName().get('Assignment').getRecordTypeId();
        insert obj;

        List<Buyer__c> lstBuyer = TestDataFactory_CRM.createBuyer(objBooking.Id, 1, objAcc.Id);
        lstBuyer[0].Case__c = obj.Id;
        insert lstBuyer;

        Attachment attobj=new Attachment();
        attobj.Name = 'NOC For Visa';
        Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
        attobj.body = bodyBlob;
        attobj.ContentType = 'application/pdf';
        attobj.parentId = lstBuyer[0].Id;
        insert attobj;

        UploadMultipleDocController.strLabelValue  = 'N';
        Test.startTest();
        UploadMultipleDocController.strLabelValue  = 'N';
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1));
        Test.stopTest();
        AttachmentHandlerUploadDocument controller = new AttachmentHandlerUploadDocument();
        AttachmentHandlerUploadDocument.createSRAttachment( new List<Attachment> { attobj } );
    }
    
    static testMethod void testAttachments3()
    {
        Case obj = new Case();
        obj.Status='New';
        obj.Origin='Call';
        obj.Priority='Medium';
        obj.RecordTypeId = Schema.getGlobalDescribe().get('Case').getDescribe().getRecordTypeInfosByName().get('Riyadh Rotana Conversion').getRecordTypeId();
        insert obj;

        Attachment attobj=new Attachment();
        attobj.Name = 'Deed of Adherence';
        Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
        attobj.body = bodyBlob;
        attobj.ContentType = 'application/pdf';
        //attobj.parentId = lstBuyer[0].Id;
        attobj.parentId = obj.Id;
        insert attobj;

        UploadMultipleDocController.strLabelValue  = 'N';
        Test.startTest();
        UploadMultipleDocController.strLabelValue  = 'N';
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1));
        Test.stopTest();
        AttachmentHandlerUploadDocument controller = new AttachmentHandlerUploadDocument();
        AttachmentHandlerUploadDocument.createSRAttachment( new List<Attachment> { attobj } );
    }

}