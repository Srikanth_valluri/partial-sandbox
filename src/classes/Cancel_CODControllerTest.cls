/***********************************************************************
* Description - Test class developed for Cancel_CODController
*
* Version            Date            Author            Description
* 1.0                19/12/17        Monali            Initial Draft
**********************************************************************/
@isTest
private class Cancel_CODControllerTest {
    static testMethod void testNewCase() {
        Id RecordTypeIdCase=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
		Account AccData=TestDataFactory_CRM.createPersonAccount();
		Insert AccData;
		
		Case CaseData=TestDataFactory_CRM.createCase(AccData.Id,RecordTypeIdCase);
		CaseData.Status = 'New';
		Insert CaseData;

		PageReference pageRef = Page.CancelCODCase; 
        pageRef.getParameters().put('id', String.valueOf(CaseData.Id));

		ApexPages.StandardController controller = new ApexPages.standardController(CaseData);
        Cancel_CODController obj = new Cancel_CODController(controller);
        obj.caseId = CaseData.Id;
        obj.errorMessage = 'Test Error message';
        PageReference pageRef1 = obj.CancelCODCase();
    }

    static testMethod void testCancelledCase() {
        Id RecordTypeIdCase=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
		Account AccData=TestDataFactory_CRM.createPersonAccount();
		Insert AccData;
		
		Case CaseData=TestDataFactory_CRM.createCase(AccData.Id,RecordTypeIdCase);
		CaseData.Status = 'Cancelled';
		Insert CaseData;

		PageReference pageRef = Page.CancelCODCase; 
        pageRef.getParameters().put('id', String.valueOf(CaseData.Id));

		ApexPages.StandardController controller = new ApexPages.standardController(CaseData);
        Cancel_CODController obj = new Cancel_CODController(controller);
        obj.caseId = CaseData.Id;
        obj.errorMessage = 'Test Error message';
        PageReference pageRef1 = obj.CancelCODCase();
    }

    static testMethod void testApprovalCase() {
        Id RecordTypeIdCase=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
		Account AccData=TestDataFactory_CRM.createPersonAccount();
		Insert AccData;

		Case CaseData=TestDataFactory_CRM.createCase(AccData.Id,RecordTypeIdCase);
		CaseData.Status = 'New';
		Insert CaseData;

		User user1 = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		// Create an approval request for the Case
        Approval.ProcessSubmitRequest req1 = 
            new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(CaseData.id);
        
        // Submit on behalf of a specific submitter
        req1.setSubmitterId(user1.Id); 
        
        // Submit the record to specific process and skip the criteria evaluation
        req1.setProcessDefinitionNameOrId('Dynamic_Approval_Process');
        req1.setSkipEntryCriteria(true);
        req1.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        
        // Submit the approval request for the account
        Approval.ProcessResult result = Approval.process(req1);

		PageReference pageRef = Page.CancelCODCase; 
        pageRef.getParameters().put('id', String.valueOf(CaseData.Id));

		ApexPages.StandardController controller = new ApexPages.standardController(CaseData);
        Cancel_CODController obj = new Cancel_CODController(controller);
        obj.caseId = CaseData.Id;
        obj.errorMessage = 'Test Error message';
        PageReference pageRef1 = obj.CancelCODCase();
    }
}