/****************************************************************************************
 * Class : CreateProofOfPaymentController
 * Created By : ES
 -----------------------------------------------------------------------------------
 * Description : Handler for CreateProofOfPaymentPage
 -----------------------------------------------------------------------------------
 * Version History:
 * Version    Developer Name    Date          Detail Features
   1.0        Nikhil Pote       15/08/2018    Initial Development
 **********************************************************************************/
public class CreateProofOfPaymentController{
    public static Proof_of_Payment__c insertPOP{get;set;}    
    public attachment objAttachment{get;set;}
    public String UserName{get;set;}
    public static id parentId;
    public list<Booking_Unit__c> lstSRRelatedBookingUnits{get;set;}
    public Boolean displayInputputText{get;set;}
    public Boolean chkBx{get;set;}
    public String input{get;set;}
    public string SRName {get;set;}
    public String bookingUnitId {get;set;}
    public CreateProofOfPaymentController() {
        insertPOP = new Proof_of_Payment__c ();   
        getTheParameterValues();
        //parentId = ApexPages.currentPage().getParameters().get('id'); 
        insertPOP.Deal_SR__c = parentId;  
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        objSR = [Select id,name from NSIBPM__Service_Request__c where id =: parentId];
        SRName = objSR.name;
        objAttachment = new Attachment();
        system.debug('@@ insertPOP@@ id 222' + insertPOP);
        lstSRRelatedBookingUnits = new list<Booking_Unit__c>();
        lstSRRelatedBookingUnits = [Select id,Total_Collections__c,Unit_Location__c,Unit_Selling_Price__c,Requested_Token_Amount__c, Unit_Name__c, Price_per_sq_ft__c,Amount_Paid__c,Credit_Amount_in_Words__c,Total_due_Overdue__c, name, Booking__c, Booking__r.Id, Booking__r.Deal_SR__c From Booking_Unit__c Where Booking__r.Deal_SR__c =: parentId];
        system.debug('lstSRRelatedBookingUnits ----'+lstSRRelatedBookingUnits);
        //displayInputputText = false;
    }     
    /**
     * Method to fetch the details as per Parameters passed 
     */  
    private void getTheParameterValues() {
        bookingUnitId = checkStringValueAndAssign (Apexpages.currentpage().getparameters().get('bookingId'));
        if(String.isNotBlank(bookingUnitId)) {
            parentId = [SELECT Id,Booking__r.Deal_SR__r.Id FROM Booking_Unit__c  WHERE Id=:bookingUnitId LIMIT 1].Booking__r.Deal_SR__r.Id;
        } else {
            parentId = ApexPages.currentPage().getParameters().get('id');
        }
        String tokenAmt = checkStringValueAndAssign (Apexpages.currentpage().getparameters().get('tokenAmt'));
        if(String.isNotBlank(tokenAmt)) {
            insertPOP.Payment_Amount__c = Decimal.valueOf(tokenAmt);
        }
        

    }
   /**
     * Method to Check if the String is Valid and assign the values accordingly
     * 
     * @param: String Value
     *
     * @return: String Value as per Conditions
     */
    public String checkStringValueAndAssign(String paramVal) {
        if(String.isNotBlank(paramVal)) {
            paramVal = paramVal;

        } else {
            paramVal = '';
        }
        return paramVal;
    }
    public class POPDetails {
        public String dealSR1;
        public String bu;
        public String curr;
        public String pa;
        public String pd;
        public String pm;
        public String pr;
        //public String fs;
        //public String fr;
        //public String status;
        //public String impsName;
        //public String impsStatus;
    
    }
    
    public static List<SelectOption> getBookingUnits() {
        system.debug('parentId----'+parentId);
        list<Booking_Unit__c> lstBookingUnits = new list<Booking_Unit__c>();
        lstBookingUnits = [Select id,Unit_Location__c,Total_Collections__c, name, Booking__c, Booking__r.Id, Booking__r.Deal_SR__c From Booking_Unit__c Where Booking__r.Deal_SR__c =: parentId];
        system.debug('lstBookingUnits----'+lstBookingUnits);
    
        List<SelectOption> options = new List<SelectOption>();
        
        options.add(new SelectOption('None', '---None---'));
        if(!lstBookingUnits.isEmpty()) {
            for(Booking_Unit__c objBU : lstBookingUnits)
            {                
                options.add(new SelectOption(objBU.Id,objBU.name));                
            }  
        }
        system.debug('options--------'+options);     
        return options;
    }
    
    @remoteAction
    public static Proof_of_Payment__c Save(string POPId){
        system.debug('popId---------'+POPId); 
        system.debug('parentId----'+parentId);       
        User loggedinuser = [Select accountid,contactid,id from User where id = :Userinfo.getuserid()];
        POPDetails pop = (POPDetails)JSON.deserialize(POPId,POPDetails.class);
        system.debug('pop---------'+pop);
        /*
        String[] myDateOnly = pop.pd.split(' ');
        String[] strDate = myDateOnly[0].split('/');
        Integer myIntDate = integer.valueOf(strDate[1]);
        Integer myIntMonth = integer.valueOf(strDate[0]);
        Integer myIntYear = integer.valueOf(strDate[2]);
        Date d = Date.newInstance(myIntYear, myIntMonth, myIntDate);*/
        
        system.debug('pop.pd------'+pop.pd);
        system.debug('date.valueOf(pop.pd)------'+date.parse(pop.pd));
        insertPOP = new Proof_of_Payment__c();
        insertPOP.Deal_SR__c = pop.dealSR1;
        insertPOP.Currency__c = pop.curr;
        insertPOP.Payment_Amount__c = decimal.valueOf(pop.pa);       
        if(String.isNotBlank(pop.bu) && !pop.bu.equals('None')){
            insertPOP.Booking_Unit__c = pop.bu;
        }     
        insertPOP.Payment_Date__c= date.parse(pop.pd);
        insertPOP.Payment_Mode__c= pop.pm;
        insertPOP.Payment_Remarks__c= pop.pr;
        insertPOP.Status__c= 'SR Submitted to Finance';
        //insertPOP.Finance_Remarks__c= pop.fr;
        //insertPOP.Finance_Status__c= pop.fs;        
        //insertPOP.IPMS_SR_Name__c= pop.impsName;
        //insertPOP.IPMS_Status__c= pop.impsStatus;
        
        system.debug('@@ insertPOP@@ idb4 ' + insertPOP);  
        insert insertPOP;        
        system.debug('@@ insertPOP@@ id ' + insertPOP);  
        
        return insertPOP;
    }   
    
}