/**

Pratiksha Narvekar
 */
@isTest(SeeAllData=false)
private class AgentCaseCreationControllerNew_Test {

  public static contact agentContact ;
  public static User portalUser ;
  public static Case__c newCase = new Case__c();
  public static Agency_Tier_Case_Status__c agencyTier ;


        @isTest static void createCasePortalUserForTier11() {
    
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole11');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 't4546', email='xyz4211@email.com',
                emailencodingkey='UTF-8', lastname='U 4546', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz4121@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {

            Test.StartTest();
            Account newAcc1 = InitialiseTestData.getCorporateAccount('Agent 3');
            newAcc1.Quarter_3_Sales__c = 2000000 ;
            insert newAcc1 ;
            
            Contact agentContact2  = new Contact();
                agentContact2.LastName = 'Agent 3';
                agentContact2.AccountId = newAcc1.Id ; 
                insert agentContact2 ;    
                    
                User portalUser2 = InitialiseTestData.getPortalUser('test567@email.com',agentContact2.Id);
                insert portalUser2;
            
            system.runAs(portalUser2) {
                    AgentCaseCreationControllerNew casePage = new AgentCaseCreationControllerNew();  
                    casePage.CasePrefix ='test';
                    casePage.tabName ='test';
                    casePage.fieldSet ='test'; 
                    casePage.caseSubject = 'Book a Hotel' ;
                    newCase.Priority__c = 'Medium' ;
                    
                    newCase.Case_Description__c = 'Testing Case Creation from Page' ;
                    newCase.Date_Time__c = null ;
                    newCase.Case_Subject__c = 'Book a Hotel' ;
                    insert newCase;
                    casePage.newCase = newCase;
                    casePage.saveCase();
                    casePage.readFieldSet('test');
                    casePage.fieldSetValue();
                    casePage.caseSubject = 'Request for site Viewing' ;
                    casePage.fieldSetValue();
                    casePage.caseSubject = 'SPA' ;
                    casePage.fieldSetValue();
                    casePage.caseSubject = 'Query on Unit Commission' ;
                    casePage.fieldSetValue();
                    casePage.caseSubject = 'Request for Brochures' ;
                    casePage.fieldSetValue();

                    newCase.Date_Time__c = System.now().addDays(-5);
                    casePage.validateRequestedDateTime();
                    }
                Test.stopTest();
        }
      }
      





        @isTest static void createCasePortalUserForTier() {
    
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test4546', email='xyz41@email.com',
                emailencodingkey='UTF-8', lastname='User 4546', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz41@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {

            Test.StartTest();
            Account newAcc1 = InitialiseTestData.getCorporateAccount('Agent 3');
            newAcc1.Quarter_3_Sales__c = 2000000 ;
            insert newAcc1 ;
            
            Contact agentContact2  = new Contact();
                agentContact2.LastName = 'Agent 3';
                agentContact2.AccountId = newAcc1.Id ; 
                insert agentContact2 ;    
                    
                User portalUser2 = InitialiseTestData.getPortalUser('test567@email.com',agentContact2.Id);
                insert portalUser2;
            
            system.runAs(portalUser2) {
                    AgentCaseCreationControllerNew casePage = new AgentCaseCreationControllerNew();  
                    casePage.CasePrefix ='test';
                    casePage.tabName ='test';
                    casePage.fieldSet ='test'; 
                    
                    newCase.Case_Subject__c = 'Book a Hotel' ;
                    newCase.Priority__c = 'Medium' ;
                    
                    newCase.Case_Description__c = 'Testing Case Creation from Page' ;
                    newCase.Date_Time__c = Datetime.now()+5 ;
                    
                    insert newCase;
                    casePage.newCase = newCase;
                    casePage.saveCase();
                    casePage.readFieldSet('test');
                    casePage.fieldSetValue();

                    casePage.validateRequestedDateTime();
                    
                    }
                Test.stopTest();
        }
      }
      
    @isTest static void method2() {
    
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test4536', email='xyz31@email.com',
                emailencodingkey='UTF-8', lastname='User 4536', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz31@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {



           Test.StartTest();
           Account newAcc1 = InitialiseTestData.getCorporateAccount('Agent 3');
           newAcc1.Quarter_3_Sales__c = 2000000 ;
           insert newAcc1 ;
           
          Contact agentContact2  = new Contact();
              agentContact2.LastName = 'Agent 3';
              agentContact2.AccountId = newAcc1.Id ; 
              insert agentContact2 ;    
                
              User portalUser2 = InitialiseTestData.getPortalUser('test567@email.com',agentContact2.Id);
              insert portalUser2;
           
           system.runAs(portalUser2) {
                 AgentCaseCreationControllerNew casePage = new AgentCaseCreationControllerNew();  
                 casePage.CasePrefix ='test';
                 casePage.tabName ='test';
                 casePage.fieldSet ='test'; 
                 
                 newCase.Case_Subject__c = 'SPA' ;
                 newCase.Priority__c = 'Medium' ;
                 
                 newCase.Case_Description__c = 'Testing Case Creation from Page' ;
                 newCase.Date_Time__c = Datetime.now()+5 ;
                 
                 insert newCase;
                 casePage.newCase = newCase;
                 casePage.saveCase();
                 casePage.readFieldSet('test');
                 casePage.fieldSetValue();
                 casePage.validateRequestedDateTime();
                 
                }
               Test.stopTest();
        }
      }
      
     @isTest static void method3() {

         UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test4256', email='xyz21@email.com',
                emailencodingkey='UTF-8', lastname='User 4256', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz21@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
    
           Test.StartTest();
           Account newAcc1 = InitialiseTestData.getCorporateAccount('Agent 3');
           newAcc1.Quarter_3_Sales__c = 2000000 ;
           insert newAcc1 ;
           
          Contact agentContact2  = new Contact();
              agentContact2.LastName = 'Agent 3';
              agentContact2.AccountId = newAcc1.Id ; 
              insert agentContact2 ;    
                
              User portalUser2 = InitialiseTestData.getPortalUser('test567@email.com',agentContact2.Id);
              insert portalUser2;
           
           system.runAs(portalUser2) {
                 AgentCaseCreationControllerNew casePage = new AgentCaseCreationControllerNew();  
                 casePage.CasePrefix ='test';
                 casePage.tabName ='test';
                 casePage.fieldSet ='test'; 
                 
                 newCase.Case_Subject__c = 'Request for site Viewing' ;
                 newCase.Priority__c = 'Medium' ;
                 
                 newCase.Case_Description__c = 'Testing Case Creation from Page' ;
                 newCase.Date_Time__c = Datetime.now()+5 ;
                 
                 insert newCase;
                 casePage.newCase = newCase;
                 casePage.saveCase();
                 casePage.readFieldSet('test');
                 casePage.fieldSetValue();
                 casePage.validateRequestedDateTime();
                 
                }
               Test.stopTest();

        }
      }

       @isTest static void method4() {
    
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test4560', email='xyz10@email.com',
                emailencodingkey='UTF-8', lastname='User 4560', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz10@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {

           Test.StartTest();
           Account newAcc1 = InitialiseTestData.getCorporateAccount('Agent 3');
           newAcc1.Quarter_3_Sales__c = 2000000 ;
           insert newAcc1 ;
           
          Contact agentContact2  = new Contact();
              agentContact2.LastName = 'Agent 3';
              agentContact2.AccountId = newAcc1.Id ; 
              insert agentContact2 ;    
                
              User portalUser2 = InitialiseTestData.getPortalUser('test567@email.com',agentContact2.Id);
              insert portalUser2;
           
           system.runAs(portalUser2) {
                 newCase.Case_Subject__c = 'Query on Unit Commission' ;
                 newCase.Priority__c = 'Medium' ;                 
                 newCase.Case_Description__c = 'Testing Case Creation from Page' ;
                 newCase.Date_Time__c = Datetime.now()+5 ;                 
                 insert newCase;
                 
                 AgentCaseCreationControllerNew casePage = new AgentCaseCreationControllerNew();  
                 casePage.CasePrefix ='test';
                 casePage.tabName ='test';
                 casePage.fieldSet ='test';  
                 casePage.newCase = newCase;
                 casePage.saveCase();
                 casePage.readFieldSet('test');
                 casePage.fieldSetValue();
                 casePage.validateRequestedDateTime();
                 
                }
               Test.stopTest();
        }
      }
      
      
      //5
             @isTest static void method5() {
    
            UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test4569', email='xyz19@email.com',
                emailencodingkey='UTF-8', lastname='User 4569', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz19@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {

           Test.StartTest();
           Account newAcc1 = InitialiseTestData.getCorporateAccount('Agent 3');
           newAcc1.Quarter_3_Sales__c = 2000000 ;
           insert newAcc1 ;
           
          Contact agentContact2  = new Contact();
              agentContact2.LastName = 'Agent 3';
              agentContact2.AccountId = newAcc1.Id ; 
              insert agentContact2 ;    
                
              User portalUser2 = InitialiseTestData.getPortalUser('test567@email.com',agentContact2.Id);
              insert portalUser2;
           
           system.runAs(portalUser2) {
                 newCase.Case_Subject__c = 'Query on Unit Commission' ;
                 newCase.Priority__c = 'Medium' ;                 
                 newCase.Case_Description__c = 'Testing Case Creation from Page' ;
                 newCase.Date_Time__c = Datetime.now()+5 ;                 
                 insert newCase;
                 
                 AgentCaseCreationControllerNew casePage = new AgentCaseCreationControllerNew();  
                 casePage.CasePrefix ='test';
                 casePage.tabName ='test';
                 casePage.fieldSet ='test';  
                 casePage.newCase = newCase;
                 casePage.saveCase();
                 casePage.readFieldSet('test');
                 casePage.fieldSetValue();
                 casePage.validateRequestedDateTime();
                 
                }
               Test.stopTest();
        }
      }
      

      
          //6
             @isTest static void method6() {
    

        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test4568', email='xyz18@email.com',
                emailencodingkey='UTF-8', lastname='User 4568', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz18@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {

           Test.StartTest();
           Account newAcc1 = InitialiseTestData.getCorporateAccount('Agent 3');
           newAcc1.Quarter_3_Sales__c = 2000000 ;
           insert newAcc1 ;
           
          Contact agentContact2  = new Contact();
              agentContact2.LastName = 'Agent 3';
              agentContact2.AccountId = newAcc1.Id ; 
              insert agentContact2 ;    
                
              User portalUser2 = InitialiseTestData.getPortalUser('test567@email.com',agentContact2.Id);
              insert portalUser2;
           
           system.runAs(portalUser2) {
                 newCase.Case_Subject__c = 'Request for Brochures' ;
                 newCase.Priority__c = 'Medium' ;                 
                 newCase.Case_Description__c = 'Testing Case Creation from Page' ;
                 newCase.Date_Time__c = Datetime.now()+5 ;                 
                 insert newCase;
                 
                 AgentCaseCreationControllerNew casePage = new AgentCaseCreationControllerNew();  
                 casePage.CasePrefix ='test';
                 casePage.tabName ='test';
                 casePage.fieldSet ='test';  
                 casePage.newCase = newCase;
                 casePage.saveCase();
                 casePage.readFieldSet('test');
                 casePage.fieldSetValue();
                 casePage.validateRequestedDateTime();
                 
                }
               Test.stopTest();
        }
      }
      
          //7
             @isTest static void method7() {
    
           UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test4567', email='xyz17@email.com',
                emailencodingkey='UTF-8', lastname='User 4567', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz17@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
           Test.StartTest();
           Account newAcc1 = InitialiseTestData.getCorporateAccount('Agent 3');
           newAcc1.Quarter_3_Sales__c = 2000000 ;
           insert newAcc1 ;
           
          Contact agentContact2  = new Contact();
              agentContact2.LastName = 'Agent 3';
              agentContact2.AccountId = newAcc1.Id ; 
              insert agentContact2 ;    
                
              User portalUser2 = InitialiseTestData.getPortalUser('test567@email.com',agentContact2.Id);
              insert portalUser2;
           
           system.runAs(portalUser2) {
                 newCase.Case_Subject__c = 'Request for Marketing Support' ;
                 newCase.Priority__c = 'Medium' ;                 
                 newCase.Case_Description__c = 'Testing Case Creation from Page' ;
                 newCase.Date_Time__c = Datetime.now()+5 ;                 
                 insert newCase;
                 
                 AgentCaseCreationControllerNew casePage = new AgentCaseCreationControllerNew();  
                 casePage.CasePrefix ='test';
                 casePage.tabName ='test';
                 casePage.fieldSet ='test';  
                 casePage.newCase = newCase;
                 casePage.saveCase();
                 casePage.readFieldSet('test');
                 casePage.fieldSetValue();
                 casePage.validateRequestedDateTime();
                 
                }
               Test.stopTest();
        }
      }
      
          //8
    @isTest static void method8() {
    
    UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test4566', email='xyz16@email.com',
                emailencodingkey='UTF-8', lastname='User 4566', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz16@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
           Test.StartTest();
           Account newAcc1 = InitialiseTestData.getCorporateAccount('Agent 3');
           newAcc1.Quarter_3_Sales__c = 2000000 ;
           insert newAcc1 ;
           
          Contact agentContact2  = new Contact();
              agentContact2.LastName = 'Agent 3';
              agentContact2.AccountId = newAcc1.Id ; 
              insert agentContact2 ;    
                
              User portalUser2 = InitialiseTestData.getPortalUser('test567@email.com',agentContact2.Id);
              insert portalUser2;
           
           system.runAs(portalUser2) {
                 newCase.Case_Subject__c = 'Booking Related Queries' ;
                 newCase.Priority__c = 'Medium' ;                 
                 newCase.Case_Description__c = 'Testing Case Creation from Page' ;
                 newCase.Date_Time__c = Datetime.now()+5 ;                 
                 insert newCase;
                 
                 AgentCaseCreationControllerNew casePage = new AgentCaseCreationControllerNew();  
                 casePage.CasePrefix ='test';
                 casePage.tabName ='test';
                 casePage.fieldSet ='test';  
                 casePage.newCase = newCase;
                 casePage.saveCase();
                 casePage.readFieldSet('test');
                 casePage.fieldSetValue();
                 casePage.validateRequestedDateTime();
                 
                }
               Test.stopTest();
        }
      }
          //9
             @isTest static void method9() {
    
           UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test4565', email='xyz15@email.com',
                emailencodingkey='UTF-8', lastname='User 4565', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz15@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
           
           Test.StartTest();
           Account newAcc1 = InitialiseTestData.getCorporateAccount('Agent 3');
           newAcc1.Quarter_3_Sales__c = 2000000 ;
           insert newAcc1 ;
           
          Contact agentContact2  = new Contact();
              agentContact2.LastName = 'Agent 3';
              agentContact2.AccountId = newAcc1.Id ; 
              insert agentContact2 ;    
                
              User portalUser2 = InitialiseTestData.getPortalUser('test567@email.com',agentContact2.Id);
              insert portalUser2;
           
           system.runAs(portalUser2) {
                 newCase.Case_Subject__c = 'Commission Payment' ;
                 newCase.Priority__c = 'Medium' ;                 
                 newCase.Case_Description__c = 'Testing Case Creation from Page' ;
                 newCase.Date_Time__c = Datetime.now()+5 ;                 
                 insert newCase;
                 
                 AgentCaseCreationControllerNew casePage = new AgentCaseCreationControllerNew();  
                 casePage.CasePrefix ='test';
                 casePage.tabName ='test';
                 casePage.fieldSet ='test';  
                 casePage.newCase = newCase;
                 casePage.saveCase();
                 casePage.readFieldSet('test');
                 casePage.fieldSetValue();
                 casePage.validateRequestedDateTime();
                 
                }
               Test.stopTest();
        }
      }
      
          //10
             @isTest static void method10() {
    
           
           UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test4564', email='xyz14@email.com',
                emailencodingkey='UTF-8', lastname='User 4564', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz14@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
           Test.StartTest();
           Account newAcc1 = InitialiseTestData.getCorporateAccount('Agent 3');
           newAcc1.Quarter_3_Sales__c = 2000000 ;
           insert newAcc1 ;
           
          Contact agentContact2  = new Contact();
              agentContact2.LastName = 'Agent 3';
              agentContact2.AccountId = newAcc1.Id ; 
              insert agentContact2 ;    
                
              User portalUser2 = InitialiseTestData.getPortalUser('test567@email.com',agentContact2.Id);
              insert portalUser2;
           
           system.runAs(portalUser2) {
                 newCase.Case_Subject__c = 'Reinstatements' ;
                 newCase.Priority__c = 'Medium' ;                 
                 newCase.Case_Description__c = 'Testing Case Creation from Page' ;
                 newCase.Date_Time__c = Datetime.now()+5 ;                 
                 insert newCase;
                 
                 AgentCaseCreationControllerNew casePage = new AgentCaseCreationControllerNew();  
                 casePage.CasePrefix ='test';
                 casePage.tabName ='test';
                 casePage.fieldSet ='test';  
                 casePage.newCase = newCase;
                 casePage.saveCase();
                 casePage.readFieldSet('test');
                 casePage.fieldSetValue();
                 casePage.validateRequestedDateTime();
                 
                }
               Test.stopTest();
        }
      }
      
          //11
             @isTest static void method11() {
    
           UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test4563', email='xyz13@email.com',
                emailencodingkey='UTF-8', lastname='User 4563', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz13@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
           
           Test.StartTest();
           Account newAcc1 = InitialiseTestData.getCorporateAccount('Agent 3');
           newAcc1.Quarter_3_Sales__c = 2000000 ;
           insert newAcc1 ;
           
          Contact agentContact2  = new Contact();
              agentContact2.LastName = 'Agent 3';
              agentContact2.AccountId = newAcc1.Id ; 
              insert agentContact2 ;    
                
              User portalUser2 = InitialiseTestData.getPortalUser('test567@email.com',agentContact2.Id);
              insert portalUser2;
           
           system.runAs(portalUser2) {
                 newCase.Case_Subject__c = 'Reassignment' ;
                 newCase.Priority__c = 'Medium' ;                 
                 newCase.Case_Description__c = 'Testing Case Creation from Page' ;
                 newCase.Date_Time__c = Datetime.now()+5 ;                 
                 insert newCase;
                 
                 AgentCaseCreationControllerNew casePage = new AgentCaseCreationControllerNew();  
                 casePage.CasePrefix ='test';
                 casePage.tabName ='test';
                 casePage.fieldSet ='test';  
                 casePage.newCase = newCase;
                 casePage.saveCase();
                 casePage.readFieldSet('test');
                 casePage.fieldSetValue();
                 casePage.validateRequestedDateTime();
                 
                }
               Test.stopTest();
        }
      }
      
              //12
             @isTest static void method12() {
    
           UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test4562', email='xyz12@email.com',
                emailencodingkey='UTF-8', lastname='User 4562', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz12@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {Test.StartTest();
           Account newAcc1 = InitialiseTestData.getCorporateAccount('Agent 3');
           newAcc1.Quarter_3_Sales__c = 2000000 ;
           insert newAcc1 ;
           
          Contact agentContact2  = new Contact();
              agentContact2.LastName = 'Agent 3';
              agentContact2.AccountId = newAcc1.Id ; 
              insert agentContact2 ;    
                
              User portalUser2 = InitialiseTestData.getPortalUser('test567@email.com',agentContact2.Id);
              insert portalUser2;
           
           system.runAs(portalUser2) {
                 newCase.Case_Subject__c = 'Book a Luxury Car' ;
                 newCase.Priority__c = 'Medium' ;                 
                 newCase.Case_Description__c = 'Testing Case Creation from Page' ;
                 newCase.Date_Time__c = Datetime.now()+5 ;                 
                 insert newCase;
                 
                 AgentCaseCreationControllerNew casePage = new AgentCaseCreationControllerNew();  
                 casePage.CasePrefix ='test';
                 casePage.tabName ='test';
                 casePage.fieldSet ='test';  
                 casePage.newCase = newCase;
                 casePage.saveCase();
                 casePage.readFieldSet('test');
                 casePage.fieldSetValue();
                 casePage.validateRequestedDateTime();
                 
                }
               Test.stopTest();
        }
      }
      
              //13
             @isTest static void method13() {
    
           UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test4561', email='xyz11@email.com',
                emailencodingkey='UTF-8', lastname='User 4561', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz11@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
            
            Test.StartTest();
           Account newAcc1 = InitialiseTestData.getCorporateAccount('Agent 3');
           newAcc1.Quarter_3_Sales__c = 2000000 ;
           insert newAcc1 ;
           
          Contact agentContact2  = new Contact();
              agentContact2.LastName = 'Agent 3';
              agentContact2.AccountId = newAcc1.Id ; 
              insert agentContact2 ;    
                
              User portalUser2 = InitialiseTestData.getPortalUser('test567@email.com',agentContact2.Id);
              insert portalUser2;
           
           system.runAs(portalUser2) {
                 newCase.Case_Subject__c = 'Book a Meeting Room' ;
                 newCase.Priority__c = 'Medium' ;                 
                 newCase.Case_Description__c = 'Testing Case Creation from Page' ;
                 newCase.Date_Time__c = Datetime.now()+5 ;                 
                 insert newCase;
                 
                 AgentCaseCreationControllerNew casePage = new AgentCaseCreationControllerNew();  
                 casePage.CasePrefix ='test';
                 casePage.tabName ='test';
                 casePage.fieldSet ='test';  
                 casePage.newCase = newCase;
                 casePage.saveCase();
                 casePage.readFieldSet('test');
                 casePage.fieldSetValue();
                 casePage.validateRequestedDateTime();
                 
                }
               Test.stopTest();
        }
      }
}