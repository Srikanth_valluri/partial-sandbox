/**********************************************************************************************************************
* Name               : RestServiceToCreateEnquiry                                                                     *
* Description        : This is a webservice to insert inquiries into salesforce from digital source.                  *
* Created Date       : 01/31/2017                                                                                     *
* Created By         : PWC                                                                                            *
* --------------------------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                                  *
*1.0         Charan         01/31/2017      Initial Draft.   
*1.1          Alok          04/Oct/2017     Added Code to Assign PreInquiry for RS
*1.2          Alok          08/Oct/2017     Added Code to not Assign PreInq to Qatar RS
*1.3          Alok          09/Oct/2017     Added 3 Additional Fields For WS                                          *
*1.4          Alok          16/Feb/2018     Added Conditon for DTP Routing
*1.5          Alok          05/March/2018   Added Code For Multiple Campaign Routing
*1.6          Alok          23/March/2018   Added Code For TSA Team auto allocation                                   *
**********************************************************************************************************************/
@RestResource(urlMapping='/DAMACEnquiry/*') 
global class RestServiceToCreateEnquiry{
    
    /********************************************************************************************* 
    * @Description : Constructor.                                                                *
    * @Params      : void                                                                        *
    * @Return      : void                                                                        *
    *********************************************************************************************/ 
    @HttpPost
    global static ResponseHandler createEnquiry(String firstname, String lastname, String email, String title, String telephone, 
                                                String Source, String UTMSource, String UTMMedium, String UTMCampaign, String HeardOfDAMAC, 
                                                String message, String mobileCode,String city, String nationality, String country, String campaignid, string phonenumbertype, string validity,string Reachable, String ref_partyId){
        ResponseHandler res = new ResponseHandler();
        String campaignObjectPrefix = Campaign__c.sobjecttype.getDescribe().getKeyPrefix();
        String sCampType='',sParentCampRecType='',gParentCampRectype='',campCountry='';
        String campName='',parentCampName='',gParentCampName='';
        String TSATeam='';
        try{
            Inquiry__c newInquiryRecord = new Inquiry__c();
            newInquiryRecord.ref_partyid__c = ref_partyId;
            newInquiryRecord.First_Name__c = firstname;
            newInquiryRecord.Last_Name__c = lastname;
            newInquiryRecord.Email__c = email;
            newInquiryRecord.Title__c = title;
            newInquiryRecord.Inquiry_source__c = Source;
            newInquiryRecord.UTM_Source__c = UTMSource;
            newInquiryRecord.UTM_medium__c = UTMMedium;
            newInquiryRecord.UTM_campaign__c = UTMCampaign;
            newInquiryRecord.Heard_Of_Damac__c = HeardOfDAMAC;
            newInquiryRecord.Description__c = message;
            newInquiryRecord.Mobile_CountryCode__c = String.isNotBlank(mobileCode) ? mobileCode : 'Unknown';
            newInquiryRecord.Mobile_Phone_Encrypt__c = telephone;
            newInquiryRecord.city__c = city;
            newInquiryRecord.Country__c = country;
            newInquiryRecord.Nationality__c = nationality;
            newInquiryRecord.Inquiry_status__c = 'New';
            newInquiryRecord.Phone_Number_Type__c = phonenumbertype;
            newInquiryRecord.Validity__c = validity;
            newInquiryRecord.Reachable__c = Reachable;
            if(String.isNotBlank(campaignid) && campaignid.startsWith(campaignObjectPrefix)){
             newInquiryRecord.Campaign__c = campaignid;
            //START :ALOK DAMAC 27/Sep/2017 Added For Telesales
            List<Campaign__c> sCampDetails = [SELECT Id,TSA_Team__c,Campaign_Record_Type__c,Parent_Campaign_Rec_Type__c,Grand_Parent_Campaign_Rec_Type__c,Country__c,Parent_s_Parent_Campaign_Name__c,Parent_Campaign_Name__c,Campaign_Name__c from Campaign__c where id=:campaignid limit 1];
            if (sCampDetails.size()>0){
            sParentCampRecType= sCampDetails[0].Parent_Campaign_Rec_Type__c;
            sCampType = sCampDetails[0].Campaign_Record_Type__c;
            gParentCampRectype = sCampDetails[0].Grand_Parent_Campaign_Rec_Type__c;
            campCountry = sCampDetails[0].Country__c;
            campName = sCampDetails[0].Campaign_Name__c;
            parentCampName= sCampDetails[0].Parent_Campaign_Name__c;
            gParentCampName=sCampDetails[0].Parent_s_Parent_Campaign_Name__c;
            TSATeam =sCampDetails[0].TSA_Team__c;
            //String dtpCamp = System.Label.DTPCampaignName;
            Id recTypeId = System.Label.PreInquiryRId;
            System.Debug('Parent Camp Rec Type>>>'+sParentCampRecType+'Camp Type>>>>>'+sCampType);
            /*String camp1= System.Label.TSCampName;
            
            set<string> tsCamps= new set<string>();
            tsCamps.addall(Label.TSCampName.split(','));
            System.Debug('TSCamps'+tsCamps);*/
            //IF(tsCamps.contains(campName)||tsCamps.contains(parentCampName)||tsCamps.contains(gParentCampName))
            System.Debug('TSATeams<<<<<<>>>>'+TSATeam);
            IF(TSATeam !=null)
            {
            System.Debug('Inside TSA IS NOT NULL');
            newInquiryRecord.RecordTypeId =recTypeId;
            newInquiryRecord.TSA_Team__c=TSATeam ;
            }
                        
            /*if(campName==camp1||parentCampName == camp1||gParentCampName==camp1){
            newInquiryRecord.RecordTypeId =recTypeId ;
            }*/
            if (sParentCampRecType=='Roadshows' || sCampType=='Roadshows' || gParentCampRectype=='Roadshows') {
            //Id recTypeId ='0123E0000008q3y';
            System.Debug('Rec Type Id<<<<>>>>'+ recTypeId);
            if(campCountry !='Qatar'){
            newInquiryRecord.RecordTypeId =recTypeId ;
            }
            newInquiryRecord.isRoadshowCampaign__c='Y';
            }
           /* if(campName==dtpCamp ||parentCampName == dtpCamp ||gParentCampName==dtpCamp){
            newInquiryRecord.RecordTypeId =recTypeId ;
            newInquiryRecord.isDTP__c=true;
            }*/
            //END :ALOK DAMAC 27/Sep/2017 Added For Telesales
            }
            }
            insert newInquiryRecord;
            res.status = 'SUCCESS'; 
            res.success = true;
        }catch(Exception ex){
            system.debug('Exception at line number = '+ex.getLineNumber()+', Exception message = '+ex.getmessage());
            res.status = 'ERROR';
            res.success = false;
            res.message = ex.getmessage();
            Log__c objLog = new Log__c();
            objLog.Description__c = 'Inquiry Details: Firstname = '+firstname+'; Lastname = '+lastname+'; Email = '+email+
                                    '; Phone = '+telephone+mobileCode+'; Campaign Id = '+campaignid+
                                    '; Error at line number = '+ex.getLineNumber()+'; Error message = '+ex.getmessage();
            objLog.Type__c = 'Error while creating inquiry: Service = RestServiceToCreateEnquiry';
            insert objLog;
        }
        return res;        
    }
    
    /********************************************************************************************* 
    * @Description : Response Handler wrapper class.                                             *
    *********************************************************************************************/ 
    global class ResponseHandler {
        public Boolean success {get; set;}
        public String status {get; set;}
        public String message {get; set;}
    }
}// End of class.