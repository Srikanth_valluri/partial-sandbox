global class AssignTSATeamInquiriesBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    global String query = '';
    global Boolean isAbort;
    global Integer batchSize = 50;


    global AssignTSATeamInquiriesBatch() {
        /*isAbort = false;
        batchSize = Integer.valueOf(Label.TSA_Batch_Size);*/
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
         /*List<Group> g1 = [select Id from Group where  Type = 'Queue' AND NAME = 'Telesales Team'];
         Id telesalesId = g1[0].id;
        query = ' SELECT '
              + ' Id, Name, OwnerId, Owner.Name, TS_Type__c '
              + ' FROM '
              + ' Inquiry__c '
              + ' WHERE '
              + ' OwnerId =: telesalesId ';
             // + ' Owner.Name = \'Telesales Team\' ';
        System.debug('>>query1 >>>>'+query );
       
        */
        if (Test.isRunningTest ()) {
            query = 'SELECT id from Inquiry__c LIMIT 1';
        }
        return Database.getQueryLocator(query);
    }

       global void execute(Database.BatchableContext BC, List<sObject> scope) {
            /*System.debug(' ONLY scope>>>>' + scope.size());
            System.debug(' batchSize>>>>' + batchSize );
            if (scope.size() > batchSize) { 
               isAbort = true;
               System.debug('ABOVE scope>>>>' + scope.size());
            } else {
                System.debug(' BEELOW scope>>>>' + scope );
                ReAssignTelesalesTeamInquiry assignTSA = new ReAssignTelesalesTeamInquiry();
                assignTSA.updateInquiryOwner(scope);
            }
            */

    }
    
    global void finish(Database.BatchableContext BC) {
        /*System.debug('FINISH 3>>>>' +isAbort );
        if (isAbort) {
            //reInitializeTSABatch();
            AssignTSATeamInquiriesBatch assignTSABatch = new AssignTSATeamInquiriesBatch();
            Database.executeBatch(assignTSABatch, 100);
        }*/
        
    }

    /*global void reInitializeTSABatch() {
        System.debug('IN isAbort 1>>>>' + isAbort);
        for (AsyncApexJob aJob : [ SELECT Id, Status, ApexClass.Name 
                                     FROM AsyncApexJob 
                                    WHERE ApexClass.Name = 'AssignTSATeamInquiriesBatch' 
                                      AND Status != 'Completed'
        ]) { 
            System.debug('aStatusJob>>>>' + aJob.Status );
            System.debug('aJob>>>>' + aJob.ApexClass.Name );
            System.AbortJob(aJob.Id);
        }
        System.debug('IN isAbort 2>>>>' + isAbort);
    }*/

}