/*
Class Name : API_CommunityPortalResetPassword
Description : To resest the password for Community user from the API.
Sample URL: /services/apexrest/resetpasswordPortal?username=*****@*****.com
Method     : POST   
Test Class : API_CommunityPortalTest           
    
*/

@RestResource(urlMapping='/resetpasswordPortal/*')
global without sharing class API_CommunityPortalResetPassword{ 

    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        //200 => 'OK',
        //400 => 'Bad Request',
        //401 => 'Unauthorized',
        //500 => 'Internal Server Error' 
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        //4 => 'Username is not correct',
        4 => 'Invalid username',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };

    
    @HttpPost
    global static FinalReturnWrapper resetPassword() {
        LoginResponse objResponse = new LoginResponse();
        FinalReturnWrapper retunResponse = new FinalReturnWrapper();

        //Error handling in the beginning
        if( !RestContext.request.params.containsKey('username') ) {
            objResponse.isSuccess = false;
            objResponse.sessionId = '';
            objResponse.statusMessage = 'Missing parameter : username';
            objResponse.statusCode = 3;
            return returnObjWrapper(objResponse);
        }
        else if( String.isBlank( RestContext.request.params.get('username') )) {
            objResponse.isSuccess = false;
            objResponse.sessionId = '';
            objResponse.statusMessage = 'Missing parameter value: username';
            objResponse.statusCode = 3;
            return returnObjWrapper(objResponse);
        }
        
        String username = RestContext.request.params.get('username');
        System.debug('username: ' + username+'*');
        User u = new User ();
        try {
            //u = [SELECT ID FROM User WHERE userName =: userName AND Profile.Name LIKE '%COMMUNITY%' LIMIT 1];
            if(username.contains('@')) {
                u = [SELECT Profile.Name
                          , is_Reset_Password_Initiated__c
                          , Login_Invalid_Attempt_Timestamp__c
                          , Community_User_Invalid_Login_Attempts__c
                          , AccountId
                          , ContactId
                          , Account.Name
                          , Contact.Name 
                     FROM User 
                     WHERE userName =: userName 
                     AND Profile.Name 
                     LIKE '%COMMUNITY%' 
                     LIMIT 1];
            }
            else{
                u = [SELECT UserName
                          , is_Reset_Password_Initiated__c
                          , Login_Invalid_Attempt_Timestamp__c
                          , Community_User_Invalid_Login_Attempts__c
                          , Profile.Name
                          , AccountId
                          , ContactId
                          , Account.Name
                          , Contact.Name 
                     FROM User 
                     WHERE IsActive = TRUE
                     AND Contact.Account.Party_ID__c =: username 
                     AND Profile.Name LIKE '%COMMUNITY%' 
                     LIMIT 1];
                username = u.UserName;                  
            }

            System.debug('username 2: ' + username);
            System.debug('u: ' + u);
            
            if (!Test.isRunningTest())
                System.resetPassword(u.Id, true);
            
            objResponse.isSuccess = true;
            objResponse.sessionId = '';
            objResponse.statusMessage = 'Password Reset email has been initiated.';
            objResponse.statusCode = 1;

            if(!u.is_Reset_Password_Initiated__c) {
                u.is_Reset_Password_Initiated__c = true;    //used post account unblocking due to invalid attempts
                u.Login_Invalid_Attempt_Timestamp__c = null;
                u.Community_User_Invalid_Login_Attempts__c = 0;
                update u;
            }
            
        } catch (Exception e) {
            objResponse.isSuccess = false;
            objResponse.sessionId = '';
            //objResponse.statusMessage = 'UserName is not a valid Community user.';
            objResponse.statusMessage = 'Please provide a valid username';/*As per bug : 1863*/
            objResponse.statusCode = 4;
        }

        retunResponse = returnObjWrapper(objResponse);
        System.debug('retunResponse-'+retunResponse);

        //return objResponse;        
        return retunResponse;
        
    }

    public static FinalReturnWrapper returnObjWrapper(LoginResponse objResponse) {
        //Wrapper to return
        cls_data objData = new cls_data();
        objData.session_id = objResponse.sessionId;

        cls_meta_data objMeta = new cls_meta_data();
        //objMeta.is_success = objResponse.isSuccess;
        //objMeta.message = mapStatusCode.get(objResponse.statusCode);
        objMeta.message = objResponse.statusMessage;
        objMeta.status_code = objResponse.statusCode;
        //objMeta.title = objResponse.statusMessage;
        objMeta.title = mapStatusCode.get(objResponse.statusCode);
        objMeta.developer_message = null;

        FinalReturnWrapper objFinal = new FinalReturnWrapper();
        //objFinal.data = objData;
        objFinal.meta_data = objMeta;

        return objFinal;
    }

    global class LoginResponse {
        public String sessionId;
        public Boolean isSuccess;
        public String statusMessage;
        public Integer statusCode;
    }

    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_data {
        public String session_id;
    }

    public class cls_meta_data {
        //public Boolean is_success;
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message;  
    }
}