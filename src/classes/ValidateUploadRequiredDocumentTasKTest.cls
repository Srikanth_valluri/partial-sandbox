@isTest
public class ValidateUploadRequiredDocumentTasKTest {
     static testMethod void test1(){
       Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POP').getRecordTypeId();

        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
            objCase.Type = 'Change of Contact Details';
            objCase.IsPOA__c = false;
            objCase.OCR_verified__c = true;
            objCase.OQOOD_Fee_Applicable__c = false;
            objCase.Approval_Status__c = 'Approved' ;
            objCase.Status = 'Working';
            insert objCase;
        Task taskObj = new Task();
            taskObj.WhatId = objCase.Id;
            taskObj.ActivityDate = System.Today();
            taskObj.Status = 'Completed';
            taskObj.Priority = 'Normal';
            taskObj.Activity_Type__c = 'Calls';
            taskObj.Activity_Sub_Type__c = '';
            taskObj.Start_Date__c = System.Today();
            taskObj.End_Date__c = System.Today();
            taskObj.Description = 'Hello';
            insert taskObj;
        SR_Attachments__c objAttach = new SR_Attachments__c();
        objAttach.Account__c  = objAcc.Id;
        objAttach.Case__c = objCase.Id;
        objAttach.isValid__c = true;
        objAttach.Description__c = 'Abctd';
        objAttach.IsRequired__c = True;
        objAttach.Name = 'Test';
        objAttach.type__c = 'Test';
        objAttach.isValid__c = true;
        objAttach.Attachment_URL__c = NULL;
        objAttach.Need_Correction__c =  False;
        insert objAttach;
        //ValidateUploadRequiredDocumentTasK  objValidation = new ValidateUploadRequiredDocumentTasK();
        ValidateUploadRequiredDocumentTasK.methodValidateUploadRequiredDocumentTasK(taskObj);
        
        
     }

}