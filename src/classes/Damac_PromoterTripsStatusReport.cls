public class Damac_PromoterTripsStatusReport {
    
    public List<Trip__c> tripsList{get; set;}
    public String searchString{get; set;}
    public String statusValue {get; set;}
    public List<Trip__c> listTrips ;
    public Integer counter;
    public Decimal offset{get;set;} 
    public Integer recNum{get; set;}
    public Integer limits;
    public Integer startIndex {get; set;}
    public Integer endIndex{get; set;}
    public Boolean showMessage {get;set;}
    public String msg{get;set;}
    public String msgType{get;set;}
    public String searchFrmDte{get;set;}
    public Datetime startDate;
    
    public Damac_PromoterTripsStatusReport(){
        showMessage = false;
        statusValue = '';
        tripsList = new List<Trip__c>() ;
        listTrips = new List<Trip__c>() ;
        counter = 0 ;
        recNum = 0 ;
        limits = 100 ;
        endIndex = limits;
        startIndex = 1;
        searchString = '';
        getTripsRecord() ;
        searchFrmDte = '';
        startDate = datetime.valueOf('2017-01-01 00:00:00');
        TimeZone tz = UserInfo.getTimeZone();
        offset = tz.getOffset(DateTime.now()) / (1000 * 3600 * 24.0);
    }
    
    // Method to get Trips record on page load
    public void getTripsRecord(){
        setRecCount();
        ID ownerId = UserInfo.getUserId ();
        
        tripsList = [ SELECT Trip_date__c, Status__c, Start_Time__c, End_Time__c, Inquiry__c, Driver_Car_Name__c,
                     Inquiry__r.Name, Inquiry__r.Full_Name__c, Drop_Address__c, Pick_Up_Address__c
                     FROM Trip__c
                     WHERE CreatedById =: ownerId 
                     ORDER BY Start_Time__c DESC NULLS LAST
                     LIMIT :limits OFFSET: counter];
        
        if(counter == 0) {
            startIndex = 1;
            if(tripsList.size() < 1){
                startIndex = 0;
                endIndex = 0;
            } else if(startIndex == 1){
                if(recNum < 20 && recNum != 0) {
                    endIndex = recNum;
                } else {
                    endIndex = startIndex + 19;
                }
            }
        }
    }
    
    public List<SelectOption> getTourStatus()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        Schema.DescribeFieldResult fieldResult = Trip__c.Status__c.getDescribe();
        List<Schema.PicklistEntry> tourStatusList = fieldResult.getPicklistValues();
        options.add(new SelectOption('Select Tour Status', 'Select Tour Status'));   
        options.add(new SelectOption('None', 'None'));          
        
        for( Schema.PicklistEntry pickListVal : tourStatusList)
        {
            options.add(new SelectOption(pickListVal.getLabel(), pickListVal.getValue()));
        }       
        return options;
    }
    
    public Boolean isSF1 {
        get {                   
            if(String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameHost')) ||
               String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin')) ||
               ApexPages.currentPage().getParameters().get('isdtp') == 'p1' ||
               (ApexPages.currentPage().getParameters().get('retURL') != null 
                && ApexPages.currentPage().getParameters().get('retURL').contains('projectone') 
               )
              ) {
                  return true;
              }else{
                  return false;
              }
        }
    }
    
    public void searchresult(){
        if(String.isNotBlank(searchFrmDte)) {
            searchFrmDte = searchFrmDte + ' 04:00:01';
            startDate = datetime.valueOf(searchFrmDte);
        }       
        ID ownerId = UserInfo.getUserID();
        if(String.isNotBlank(searchString) && statusValue.equals('Select Tour Status') ) {
            
            listTrips = [SELECT Name
                         FROM Trip__c
                         WHERE 
                         (Inquiry__r.Name LIKE :'%' + searchString + '%'
                          OR Inquiry__r.Owner.Name LIKE :'%' + searchString + '%'
                          OR Inquiry__r.First_Name__c LIKE :'%' + searchString + '%'
                          OR Inquiry__r.Last_Name__c LIKE :'%' + searchString + '%' 
                         )
                         AND CreatedById =: ownerId 
                         AND Start_Time__c >= :startDate 
                         ORDER BY Start_Time__c DESC NULLS LAST
                        ];
            recNum = listTrips.size();    
            
            tripsList = [SELECT 
                         Trip_date__c, Driver_Car_Name__c, Status__c, Start_Time__c, End_Time__c, Inquiry__c,
                         Inquiry__r.Name, Inquiry__r.Full_Name__c, Drop_Address__c, Pick_Up_Address__c
                         FROM Trip__c
                         WHERE 
                         (Inquiry__r.Name LIKE :'%' + searchString + '%'
                          OR Inquiry__r.Owner.Name LIKE :'%' + searchString + '%'
                          OR Inquiry__r.First_Name__c LIKE :'%' + searchString + '%'
                          OR Inquiry__r.Last_Name__c LIKE :'%' + searchString + '%' 
                         )
                         AND CreatedById =: ownerId 
                         AND Start_Time__c >= :startDate 
                         ORDER BY Start_Time__c DESC NULLS LAST
                         LIMIT :limits OFFSET: counter
                        ];                
        } else if(String.isNotBlank(searchString) && String.isNotBlank(statusValue)){
            if(statusValue.equals('None')) 
                statusValue= null;
           
            listTrips = [ SELECT Id
                         FROM Trip__c
                         
                         WHERE 
                         (((Status__c =: statusValue ))
                          AND (( Inquiry__r.Name LIKE :'%' + searchString + '%'
                                OR Inquiry__r.Name = '')
                               OR (Inquiry__r.Owner.Name LIKE :'%' + searchString + '%'
                                   OR Inquiry__r.Owner.Name = '')
                               OR (Inquiry__r.First_Name__c LIKE :'%' + searchString + '%'
                                   OR Inquiry__r.First_Name__c ='')
                               OR (Inquiry__r.Last_Name__c LIKE :'%' + searchString + '%'
                                   OR Inquiry__r.Last_Name__c ='')
                              )
                         )
                         AND Start_Time__c >= :startDate 
                         AND CreatedById =: ownerId 
                         ORDER BY Start_Time__c DESC NULLS LAST
                        ];
            recNum = listtrips.size() ; 
            
            //counter = 0;
            TripsList = [SELECT 
                         Trip_date__c, Driver_Car_Name__c, Status__c, Start_Time__c, End_Time__c, Inquiry__c,
                         Inquiry__r.Name, Inquiry__r.Full_Name__c, Drop_Address__c, Pick_Up_Address__c
                         FROM Trip__c
                         WHERE
                         (((Status__c=: StatusValue ))
                          AND (( Inquiry__r.Name LIKE :'%' + searchString + '%'
                                OR Inquiry__r.Name = '')
                               OR (Inquiry__r.Owner.Name LIKE :'%' + searchString + '%'
                                   OR Inquiry__r.Owner.Name = '')
                               OR (Inquiry__r.First_Name__c LIKE :'%' + searchString + '%'
                                   OR Inquiry__r.First_Name__c ='')
                               OR (Inquiry__r.Last_Name__c LIKE :'%' + searchString + '%'
                                   OR Inquiry__r.Last_Name__c ='')
                              )
                         )
                         AND Start_Time__c >= :startDate 
                         AND CreatedById =: ownerId 
                         ORDER BY Start_Time__c DESC NULLS LAST
                         LIMIT :limits OFFSET: counter
                        ];
            //recNum = inquiryList.size() ;
            System.debug('==========222  recNum '+recNum ); 
        } else if (!statusValue.equals('Select Tour Status') && String.isBlank(searchString)){
            if(statusValue.equals('None')) 
                statusValue = null;
            
            listTrips = [ SELECT Id
                         FROM Trip__c                         
                         WHERE 
                         Status__c =: statusValue
                         AND CreatedById =: ownerId 
                         AND Start_Time__c >= :startDate 
                         ORDER BY Start_Time__c DESC NULLS LAST
                        ];
            recNum = listTrips.size() ;
            
            tripsList = [SELECT 
                         Trip_date__c, Driver_Car_Name__c, Status__c, Start_Time__c, End_Time__c, Inquiry__c,
                         Inquiry__r.Name, Inquiry__r.Full_Name__c, Drop_Address__c, Pick_Up_Address__c
                         FROM Trip__c
                         WHERE 
                         Status__c =: statusValue
                         AND CreatedById =: ownerId 
                         AND Start_Time__c >= :startDate 
                         ORDER BY Start_Time__c DESC NULLS LAST
                         LIMIT :limits OFFSET: counter
                        ];
        } else if(String.isBlank(searchString) && statusValue.equals('Select Tour Status') && (String.isNotBlank(searchFrmDte))) {
            
            listTrips = [ SELECT Id
                         FROM Trip__c
                         WHERE Start_Time__c >= :startDate 
                         AND CreatedById =: ownerId 
                         ORDER BY Start_Time__c DESC NULLS LAST
                         LIMIT :limits OFFSET: counter
                        ];
            recNum = listTrips.size() ;
            
            tripsList = [SELECT  
                         Trip_date__c, Driver_Car_Name__c, Status__c, Start_Time__c, End_Time__c, Inquiry__c,
                         Inquiry__r.Name, Inquiry__r.Full_Name__c, Drop_Address__c, Pick_Up_Address__c
                         FROM Trip__c   
                         WHERE 
                         Start_Time__c >= :startDate 
                         AND CreatedById =: ownerId 
                         ORDER BY Start_Time__c DESC NULLS LAST
                         LIMIT :limits OFFSET: counter
                        ];
        }
        else{
            setRecCount();                   
            tripsList = [SELECT  
                         Trip_date__c, Driver_Car_Name__c, Status__c, Start_Time__c, End_Time__c, Inquiry__c,
                         Inquiry__r.Name, Inquiry__r.Full_Name__c, Drop_Address__c, Pick_Up_Address__c
                         FROM Trip__c
                         WHERE 
                         Start_Time__c >= :startDate 
                         AND CreatedById =: ownerId 
                         ORDER BY Start_Time__c DESC NULLS LAST
                         LIMIT :limits OFFSET: counter
                        ];
        }
        
        if(counter == 0) {
            startIndex = 1;
            if(listTrips.size() < 1){
                startIndex = 0;
                endIndex = 0;
            } else if(startIndex == 1){
                if(recNum < 20 && recNum != 0) {
                    endIndex = recNum;
                } else {
                    endIndex = startIndex + 19;
                }
            }            
        }
    }
    
    // Method to clear search result and initialize data
    public void inqClear(){
        searchString ='';
        searchFrmDte = '';
        statusValue = 'Select Trip Status';
        searchresult();
    }
    
    // Method to redirect to home page
    public pageReference cancel() {
        pageReference pg = new pageReference('/home/home.jsp');
        return pg;
    }
    // Method to redirect to Community Home Page.
    public pageReference cancelbtn() {
        pageReference pg = new pageReference('/PromoterDashboardPage');
        return pg;
    }
    
    // Method for next button click functionality
    public void nextbtn(){
        counter += limits ;
        startIndex = counter;
        if((startIndex+20) > recNum)
            endIndex = recNum;
        else
            endIndex = startIndex+20;
        searchresult();
    }
    
    // Method for previous button click functionality
    public void prvbtn(){
        counter -= limits ;
        if(counter == 0) {
            startIndex = 1;
            endIndex = 20;
        }
        else {
            startIndex = counter;
            endIndex = startIndex+20;
        }
        
        searchresult();
    }
    
    // Method for first button click functionality
    public void fstbtn(){
        counter = 0;
        startIndex = 1;
        endIndex = 20;
        searchresult();
    }
    
    // Method for Last button click functionality
    public void endbtn(){
        counter = recNum - math.mod(recNum,limits);
        startIndex = counter;
        endIndex = recNum;
        searchresult();
    }
    
    // Method for previous button click functionality
    public boolean getprv(){
        if(counter == 0)
            return true;
        else
            return false;
    }
    
    // Method for next button click functionality
    public boolean getnxt(){
        if((counter + limits) > recNum)
            return true;
        else
            return false;
    }
    
    // Method for ASC Sort functionality
    public void sortListASC(){
        ID OwnerId = UserInfo.getUserID();
        tripsList = [SELECT 
                     Trip_date__c, Driver_Car_Name__c, Status__c, Start_Time__c, End_Time__c, Inquiry__c,
                     Inquiry__r.Name, Inquiry__r.Full_Name__c, Drop_Address__c, Pick_Up_Address__c
                     FROM Trip__c
                     WHERE CreatedById =: ownerId 
                     ORDER BY Start_Time__c ASC NULLS LAST
                     LIMIT :limits OFFSET: counter
                    ];
    }
    
    // Method for DSC Sort functionality
    public void sortListDSC(){
        searchresult();
    }
    
    // Method to set record count
    public void setRecCount(){
        ID OwnerId = UserInfo.getUserID();
        listTrips = [SELECT Id
                     FROM Trip__c
                     WHERE CreatedById =: ownerId 
                    ];
        recNum = listTrips.size() ;
    }
}