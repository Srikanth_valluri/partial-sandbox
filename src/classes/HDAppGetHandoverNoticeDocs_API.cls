@RestResource(urlMapping='/getHandoverNoticeDocument/*')
global class HDAppGetHandoverNoticeDocs_API {
    
    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };
    
    @HttpGet
    global static FinalReturnWrapper MainResponseCreator() {
        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);
        
        
        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        cls_data objData = new cls_data();
        cls_meta_data objMeta = new cls_meta_data();
        
        if(!r.params.containsKey('bookingUnitId')) {
            objMeta = ReturnMetaResponse('Missing parameter : bookingUnitId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;      
        }
        
        else if(r.params.containsKey('bookingUnitId') && String.isBlank(r.params.get('bookingUnitId'))) {
            objMeta = ReturnMetaResponse('Missing parameter value : bookingUnitId', 3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        
        if(r.params.containsKey('bookingUnitId') && String.isNotBlank(r.params.get('bookingUnitId'))) {
            
            try {
                //Main logic
                List<Booking_Unit__c> lstBU = [SELECT id,
                                                      Handover_Notice_URLs__c
                                               FROM Booking_Unit__c
                                               WHERE id=: r.params.get('bookingUnitId')];

                System.debug('lstBU:: ' + lstBU);                               

                if(!lstBU.isEmpty() && lstBU.size() > 0) {

                    if(lstBU[0].Handover_Notice_URLs__c != null ) {

                        objData.document_list = getDocNameAndURL(lstBU[0]);
                        objMeta = ReturnMetaResponse('successfull', 1);
                    }
                    else {
                        objMeta = ReturnMetaResponse('No Handover Notice URLs found on given bookingUnitId', 2);
                        returnResponse.meta_data = objMeta;
                        return returnResponse;
                    }
                }
                else{
                    objMeta = ReturnMetaResponse('Invalid bookingUnitId passed', 3);
                    returnResponse.meta_data = objMeta;
                    return returnResponse;
                }
                //Main logic Ends
            }
            catch(Exception e) {
                objMeta = ReturnMetaResponse(e.getMessage(), 6);
                returnResponse.meta_data = objMeta;
                return returnResponse;
            }
            
        }

        returnResponse.meta_data = objMeta;
        returnResponse.data = objData;
        System.debug('returnResponse:: ' + returnResponse);
        
        return returnResponse;
    }

    public static List<docWrapper> getDocNameAndURL(Booking_Unit__c objBU) {
        String str = objBU.Handover_Notice_URLs__c.remove('<ul>').remove('<li>').remove('</li>').remove('<a href=').remove('</ul>').replaceAll('target="_blank">','-').replaceAll('</a>',',');
        System.debug('resultant string ---> ' + str);

        List<String> lstDocNameURLs = str.split(',');

        System.debug('lstDocName&URLs::: ' + lstDocNameURLs);

        List<docWrapper> document_list = new List<docWrapper>();

        for(String objStr : lstDocNameURLs) {
           docWrapper objDoc = new docWrapper();
           objDoc.document_url = objStr.split('-')[0].remove('"').trim();
           objDoc.document_name = objStr.split('-')[1];

           document_list.add(objDoc);
        }

        System.debug('document_list ==== ' + document_list);
        return document_list;
    }

    
    public static cls_meta_data ReturnMetaResponse(String message, Integer statusCode) {
        cls_meta_data retMeta = new cls_meta_data(); 
        retMeta.message = message;
        retMeta.status_code = statusCode;
        retMeta.title = mapStatusCode.get(statusCode);
        retMeta.developer_message = null;
        
        return retMeta;
        
    }
    
    
    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }
    
    
    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message;   
    }
    
    
    public class cls_data {
        public docWrapper[] document_list;
    }
    
    public class docWrapper {
        public String document_name;
        public String document_url;
    }
}