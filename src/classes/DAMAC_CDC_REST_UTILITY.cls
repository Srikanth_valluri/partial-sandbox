/*
Description: Rest Resporce Class for CDC team to Read the Registration Data from Salesforce
             Update Document status data to salesforce.
Developed By: DAMAC IT Team
*/

@RestResource(urlMapping='/sfdetails/*')
global without sharing class DAMAC_CDC_REST_UTILITY{    

  
  @HttpPost
    global static void doPost(){
        DAMAC_CDC_POST_RESP_UNIT postResp = new DAMAC_CDC_POST_RESP_UNIT();
        String jsonStr = null;        
        boolean Toupdatebu = false;
        boolean sendError = false;        
        if (null != RestContext.request.requestBody) {
            jsonStr = RestContext.request.requestBody.toString();
            system.debug(jsonStr);            
            if(jsonStr != '' && jsonStr.length()>0){ // Check if Body is there                
                DAMAC_CDC_POST_OBJ_UNIT toupdate = new DAMAC_CDC_POST_OBJ_UNIT(); // Object Class to Parse the Body
                toupdate = DAMAC_CDC_POST_OBJ_UNIT.parse(jsonStr);
                system.debug(toupdate +'>>>>>>>>>>>');
                string regid = toupdate.regid; // read the Reg Id
                if(!string.isblank(regid) && regid !=''){ // If Regid Get the Booking Unit details
                    booking_unit__c booku = [select id,
                                                Deal__c,
                                                Booking__r.Deal_SR__r.Name,
                                                booking__r.deal_sr__r.Sales_Audit_Status__c,
                                                SR_Status__c,
                                                cdc_status__c,
                                                cdc_comments__c,
                                                Unit_Location__c,
                                                SR_DP_OK__c,
                                                Document_Check_in_Date_Time__c 
                                                from booking_unit__c 
                                                where registration_id__c=:regid limit 1];
                    if(!string.isblank(toupdate.cdcstatus) && toupdate.cdcstatus!=''){
                        string salesAuditStatus = booku.booking__r.deal_sr__r.Sales_Audit_Status__c; 
                        // checkin -update SR
                        if( (toupdate.cdcstatus == 'checkin' || toupdate.cdcstatus == 'mismatch' || toupdate.cdcstatus == 'recheckin') && ( booku.SR_Status__c == 'Booking Docs Under Review' || booku.SR_Status__c == 'Docs Sent' ||  booku.SR_Status__c == 'Docs Received' || booku.SR_Status__c == 'Being Followed up' || booku.SR_Status__c == 'Manager Approved' || booku.SR_Status__c == 'Agreement Generated' || booku.SR_Status__c == 'Reinstatement Approved' ) ){ // 'SS' , mismatch - SF , Executed - LE
                            Toupdatebu = true;
                            system.debug('checkin');
                        }else {
                            sendError = true;
                            system.debug('checkinerror');
                        }
                        
                        //Verified
                        if( (toupdate.cdcstatus == 'verified' ) && ( booku.SR_Status__c == 'Booking Docs Under Review' || booku.SR_Status__c == 'Docs Received' ||  booku.SR_Status__c == 'Docs Sent' || booku.SR_Status__c == 'Manager Approved' || booku.SR_Status__c == 'Being Followed Up' || booku.SR_Status__c == 'Agreement Generated' || booku.SR_Status__c == 'Reinstatement Approved') ){ // 'SS' , mismatch - SF , Executed - LE
                            Toupdatebu = true; 
                            system.debug('verified');                           
                        }else {
                            sendError = true;
                            system.debug('verified error');  
                        }
                        
                        // Executed
                        if( (toupdate.cdcstatus == 'executed' ) && ( booku.SR_Status__c == 'Docs Verified') &&  (booku.SR_DP_OK__c) ){ // 'SS' , mismatch - SF , Executed - LE       ( salesAuditStatus=='' || salesAuditStatus== null || salesAuditStatus=='Approved' || salesAuditStatus=='Not Applicable') &&                  
                            Toupdatebu = true;
                            system.debug('executed');                           
                        }else {
                            sendError = true;
                            system.debug('executed error'); 
                        }
                    }else if(string.isblank(toupdate.cdcstatus) && toupdate.cdcstatus==''){
                        //booku.cdc_status__c = toupdate.cdcstatus; 
                        //Toupdatebu = true;
                    }
                    
                    try{
                        system.debug('is boolean '+Toupdatebu);                        
                        if(Toupdatebu){
                            // Check in and time                             
                            if( toupdate.cdcstatus == 'checkin' &&  string.valueof(booku.Document_Check_in_Date_Time__c) != ''){
                                booku.Document_Check_in_Date_Time__c = toupdate.checkindatetime;                                
                                // IPMS Status
                                // Update IPMS Status on checkin
                                list<id> regIds = new list<id>();
                                regIds.add(booku.booking__c);                                
                                system.enqueueJob(new IPMS_Registration_Status_Update(regIds,'STATUS_UPDATE','SS'));  
                            }
                            //Update Comments
                            if(!string.isblank(toupdate.cdccomments) && toupdate.cdccomments!=''){
                                booku.cdc_comments__c  = toupdate.cdccomments;
                            }
                            // If not recheckin- pass status which is coming from Service
                            if(toupdate.cdcstatus != 'recheckin'){
                                booku.cdc_status__c = toupdate.cdcstatus;
                            }
                            // If recheckin - pass checkin
                            if(toupdate.cdcstatus == 'recheckin'){
                                booku.cdc_status__c = 'checkin';
                            }
                            update booku;   
                            postResp.regid =  regid;
                            postResp.STATUS = 'Success';
                            postResp.Message = 'CDC status updated successfully for Registration Id '+regid; 
                            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(postResp));  
                        }
                        if(sendError && !Toupdatebu){
                            postResp.regid =  regid;
                            postResp.STATUS = 'Error';
                            postResp.Message = 'Cannot update CDC status as ' + booku.Booking__r.Deal_SR__r.Name + ' Internal Status ('+booku.SR_Status__c+') or Sales Audit status ('+booku.booking__r.deal_sr__r.Sales_Audit_Status__c+') not allowed at this time for '+booku.Unit_Location__c;                    
                            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(postResp));                            
                        }
                        
                    }catch(exception e){
                        postResp.regid =  regid;
                        postResp.STATUS = 'Error';
                        postResp.Message = e.getmessage()+' Please contact Salesforce Administrator';
                        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(postResp));                       
                    }
                    
                }
            }else{ 
                postResp.regid =null;
                postResp.STATUS = 'Error';
                postResp.Message = 'Request Failed missing body';                
                RestContext.response.responseBody = Blob.valueOf(JSON.serialize(postResp));                        
            }
        }else{
            postResp.regid =null;
            postResp.STATUS = 'Error';
            postResp.Message = 'Request body is missing';                
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(postResp));            
        }
    }
    
    
    @HttpGet
    global static void doGet(){       
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String regid = req.params.get('regid');  
        String srnum = req.params.get('srnum');
        String rtype = req.params.get('rtype');   
            
        
        if(!String.isBlank(regid) && regid != '' && !String.isBlank(srnum) && srnum != ''){
            RestContext.response.responseBody = Blob.valueOf('X_RETURN_STATUS:Pass any one Parameter either Regid or Srnum');
        }else{
            if(!String.isBlank(regid) && regid != '' && !String.isBlank(rtype) && rtype != '' ){
                regid = String.escapeSingleQuotes(regid); 
                String qry = 'SELECT Id,Registration_ID__c,sr_status__c FROM booking_unit__c WHERE Registration_ID__c=\'' + String.escapeSingleQuotes(regid)+'\'';
                system.debug('@REGID>>>'+regid);
                list<booking_unit__c> result = database.query(qry);        
                string respStr;
                if(result.size()> 0 && result.size() == 1){                    
                    if(!String.isBlank(rtype) && rtype != ''){
                        RestContext.response.responseBody = Blob.valueOf(sendBookingUnitDetails(regId,rtype));
                    }else{
                        RestContext.response.responseBody = Blob.valueOf('X_RETURN_STATUS:Please pass rtype Parm Ex: rtype=dt for Registration Details or rtype=pt for Payment Terms');
                    }                    
                }else{            
                    RestContext.response.responseBody = Blob.valueOf('X_RETURN_STATUS:Registration Id not found in Salesforce'+regId);
                } 
            } 
            // SR Id 
            if(!String.isBlank(srnum) && srnum != ''){ 
                srnum = String.escapeSingleQuotes(srnum);  
                String qry = 'SELECT Id FROM NSIBPM__Service_Request__c WHERE Name=\'' + String.escapeSingleQuotes(srnum)+'\'';              
                list<NSIBPM__Service_Request__c> result = database.query(qry); 
                if(result.size()> 0 && result.size() == 1){
                    //respStr = result[0].Id+'- Success';            
                    //respStr = resp(regId);
                    RestContext.response.responseBody = Blob.valueOf(sendSRDetails(srnum));
                }else{            
                    RestContext.response.responseBody = Blob.valueOf('X_RETURN_STATUS:SR not found in Salesforce-'+srnum);
                }
            }  
        }    
    }
    
    // Prepare Response for GET by Reg id
    public static string sendBookingUnitDetails(string regId,string rtype){     
        string responseAsJsonStr;   
        booking_unit__c bu = [select id,
                                    cdc_status__c,
                                    booking__c,
                                    booking__r.deal_sr__r.name,
                                    booking__r.deal_sr__r.NSIBPM__Internal_Status_Name__c,
                                    registration_id__c,
                                    Registration_DateTime__c,
                                    Unit_Location__c,
                                    Inventory__r.Property_Name__c,
                                    Registration_Status__c,
                                    Inventory__r.Seller_Name__c,
                                    Primary_Buyer_s_Name__c,
                                    Primary_Buyer_s_Email__c,                                    
                                    Primary_Buyer_Country__c,                                    
                                    Primary_Buyer_s_Nationality__c,                                    
                                    passport_Number__c,
                                    Doc_OK__c, 
                                    DP_OK__c,
                                    Dispute_Flag__c, 
                                    Finance_Flag__c, 
                                    Size__c,
                                    Permitted_Use__c,
                                    Area_sft__c,
                                    Requested_Price__c,//Appartment price
                                    inventory__r.CM_Price_Per_Sqft__c                                    
                                    from booking_unit__c where Registration_ID__c=:regid  LIMIT 1]; 
                                    
        list<buyer__c> primaryBuyer = new list<buyer__c>();                          
        primaryBuyer = [select id,
                                    Phone__c,
                                    City__c,
                                    Address_Line_1__c,
                                    Address_Line_2__c,
                                    Address_Line_3__c,
                                    Address_Line_4__c,
                                    passport_number__c,
                                    Phone_Country_Code__c                                    
                                    from buyer__c 
                                    where booking__c=:bu.booking__c and 
                                    Primary_Buyer__c=true LIMIT 1];
        system.debug('>>>>>>>>>>'+primaryBuyer);
        payment_plan__c p = [select id from payment_plan__c where booking_unit__c=:bu.id limit 1];
        list<payment_terms__c> pterms = [select id,payment_plan__c,booking_unit__c,Installment__c,Milestone_Event__c,Percent_Value__c,Expected_Date__c,Payment_Date__c from payment_terms__c  where payment_plan__c=:p.id];
        if(rtype == 'dt'){
            DAMAC_CDC_RESPONSE_OBJ_UNIT resp = new DAMAC_CDC_RESPONSE_OBJ_UNIT();
            string phoneStr = '';
            if(!string.isblank(primaryBuyer[0].Phone_Country_Code__c) && primaryBuyer[0].Phone_Country_Code__c != null){
                phoneStr = primaryBuyer[0].Phone_Country_Code__c;
                phoneStr = phoneStr.substringafter(':').trim();
            }
            
            resp.REGISTRATION_ID = bu.registration_id__c ;  
            resp.REGISTRATION_DATE = string.valueof(bu.Registration_DateTime__c);    
            resp.UNIT_NAME = bu.Unit_Location__c;    
            resp.PROJECT_NAME = bu.Inventory__r.Property_Name__c; 
            resp.STATUS = bu.Registration_Status__c ;   
            resp.SELLER_NAME= bu.inventory__r.Seller_Name__c;
            resp.CUSTOMER_NAME = bu.Primary_Buyer_s_Name__c;    
            resp.EMAIL_ADDRESS = bu.Primary_Buyer_s_Email__c;    
            resp.ADDRESS1= primaryBuyer[0].Address_Line_1__c; 
            resp.ADDRESS2= primaryBuyer[0].Address_Line_2__c;
            resp.ADDRESS3= primaryBuyer[0].Address_Line_3__c;
            resp.ADDRESS4= primaryBuyer[0].Address_Line_4__c;
            resp.CITY= primaryBuyer[0].City__c; 
            resp.COUNTRY = bu.Primary_Buyer_Country__c;  
            resp.POSTAL_CODE = ''; // Blank No Mapping
            resp.PRIMARY_PHONE_NUM = phoneStr+primaryBuyer[0].Phone__c;    
            resp.PHONE_NUMBER = phoneStr+primaryBuyer[0].Phone__c;
            resp.FAX_NUMBER= ''; // Blank No Mapping
            resp.NATIONALITY= bu.Primary_Buyer_s_Nationality__c;  //CANADIAN
            resp.PASSPORT_NO= primaryBuyer[0].Passport_Number__c;
            resp.DOCUMENTS_OK = string.valueof(bu.Doc_OK__c); 
            resp.DEPOSIT_RECEIVED = string.valueof(bu.DP_OK__c); 
            resp.DISPUTE = bu.Dispute_Flag__c;
            resp.FINANCE = bu.Finance_Flag__c;
            resp.SIZE_OF_APARTMENT = string.valueof(bu.Area_sft__c); 
            resp.PERMITTED_USE = bu.Permitted_Use__c;   
            resp.APARTMENT_PRICE = string.valueof(bu.Requested_Price__c);
            resp.PARKING_ADDENDUM = ''; // No Mapping
            resp.CM_PRICE = string.valueof(bu.inventory__r.CM_Price_Per_Sqft__c);
            resp.PRESENT_CM_PRICE= ''; // No mapping
            resp.EFF_DATE_OF_CURR_CM_PRICE= ''; //No Mapping
            resp.X_RETURN_STATUS = 'S';
            resp.SFSRNUMBER = bu.booking__r.deal_sr__r.name;
            resp.SFSRSTATUS = bu.booking__r.deal_sr__r.NSIBPM__Internal_Status_Name__c;
            resp.CDCSTATUS = BU.cdc_status__c;            
            //resp.PaymentTerms = items;
            responseAsJsonStr = JSON.serialize(resp);              
        }
        if(rtype == 'pt'){
            DAMAC_CDC_RESPONSE_OBJ_PAYTERMS resp1 = new DAMAC_CDC_RESPONSE_OBJ_PAYTERMS();
            list<DAMAC_CDC_RESPONSE_OBJ_PAYTERMS.PAYMENT_TERMS> items = new list<DAMAC_CDC_RESPONSE_OBJ_PAYTERMS.PAYMENT_TERMS>();        
            // Payment Terms
            for(payment_terms__c pt:pterms){
                DAMAC_CDC_RESPONSE_OBJ_PAYTERMS.PAYMENT_TERMS item = new DAMAC_CDC_RESPONSE_OBJ_PAYTERMS.PAYMENT_TERMS(); 
                item.INSTALLMENT = pt.Installment__c;
                item.EVENT = pt.Milestone_Event__c;
                item.EXPECTED_DATE = string.valueof(pt.Expected_Date__c);
                item.PERCENT = pt.Percent_Value__c;
                item.PAYMENT_DATE = string.valueof(pt.Payment_Date__c);
                items.add(item);                     
            }  
            resp1.PAYMENT_TERMS = items;
            responseAsJsonStr = JSON.serialize(resp1);                 
        }
        return responseAsJsonStr;     
    }
    
    // Prepare Response for GET by Reg id
    public static string sendSRDetails(string srNum){     
        string responseAsJsonStr;  
        list<booking_unit__c> bus = [select id,registration_id__c,CDC_Status__c from booking_unit__c where booking__r.deal_sr__r.name =:srnum];
        DAMAC_CDC_RESPONSE_OBJ_SR resp = new DAMAC_CDC_RESPONSE_OBJ_SR();
        resp.SFSRNUMBER =  srNum;
        
        list<DAMAC_CDC_RESPONSE_OBJ_SR.BookingUnits> units = new list<DAMAC_CDC_RESPONSE_OBJ_SR.BookingUnits>();
        for(booking_unit__c b:bus){
            DAMAC_CDC_RESPONSE_OBJ_SR.BookingUnits unit = new DAMAC_CDC_RESPONSE_OBJ_SR.BookingUnits(); 
            unit.regId = b.registration_id__c;
            unit.cdcstatus = b.CDC_Status__c;
            units.add(unit);
        }
        resp.bookingunits = units;
        responseAsJsonStr = JSON.serialize(resp); 
        return responseAsJsonStr;
        
    }
}