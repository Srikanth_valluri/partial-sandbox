@isTest
public class CaseTriggerHandlerRPAssignmentTest{
    static List<Booking_Unit_Active_Status__c> lstActives;
    static list<RuleEngineDocuments__c> lstRuleDocs;
    static Account objAcc;
    static NSIBPM__Service_Request__c objSR;
    static List<Booking__c> lstBookings;
    static Property__c objProp;
    static Inventory__c objInv;
    static List<Booking_Unit__c> lstBookingUnits;
    static List<Option__c> lstOptions;
    static list<Buyer__c> lstJB;
    static Payment_Plan__c objPP;
    
    static void init() {
        lstActives = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActives;
        
        lstRuleDocs = TestDataFactory_CRM.createRuleDocs();
        insert lstRuleDocs;
        
        // Insert Account        
        objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        system.debug('******Acc Id*************'+objAcc.Id);
        
        //Insert Service Request
        objSR = TestDataFactory_CRM.createServiceRequest( );
        insert objSR ;
        system.debug('******SR Id*************'+objSR.Id);
        
        //Insert Bookings
        lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1);
        insert lstBookings ;
        system.debug('******bookings Id*************'+lstBookings);
        
        objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        objInv = TestDataFactory_CRM.createInventory(objProp.Id);
        objInv.Property_Country__c = 'UAE';
        objInv.Property_City__c = 'Dubai';
        objInv.Property_Status__c = 'Ready';
        objInv.Bedroom_Type__c = ' 1 BR';
        insert objInv;
        
        //Insert Booking Units
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 2);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        lstBookingUnits[0].Inventory__c = objInv.Id;
        lstBookingUnits[0].Area__c = 35;
        lstBookingUnits[0].Handover_Flag__c = 'Y';
        insert lstBookingUnits;
        system.debug('******Unit Id*************'+lstBookingUnits);
        
        lstOptions = TestDataFactory_CRM.createOptions(lstBookingUnits);
        insert lstOptions;
        
        lstJB = TestDataFactory_CRM.createBuyer(lstBookings[0].Id, 2, objAcc.Id);
        insert lstJB;
    }
    
    private static testMethod void testWithCommencement(){
        init();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(1));
        
        Id recTypeAssignment = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Assignment').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(null, recTypeAssignment);
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.New_Booking_Unit__c = lstBookingUnits[1].Id;
        objCase.Commencement_Date__c = date.today();
        insert objCase;
        Test.startTest();
        objCase.Status = 'Closed';
        set<Id> tempSet = new set<Id>();
        tempSet.add(objCase.Id);
        CaseTriggerHandlerRPAssignment.processCase(tempSet);
        //update objCase;
        
        Test.stopTest();
    }
    
    private static testMethod void testWithoutCommencement(){
        init();
        
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(1));
        Id recTypeAssignment = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Assignment').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(null, recTypeAssignment);
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.New_Booking_Unit__c = lstBookingUnits[1].Id;
        //objCase.Commencement_Date__c = date.today();
        insert objCase;
        Test.startTest();
        objCase.Status = 'Closed';
        set<Id> tempSet = new set<Id>();
        tempSet.add(objCase.Id);
        CaseTriggerHandlerRPAssignment.processCase(tempSet);
        //update objCase;
        
        Test.stopTest();
    }
    
    private static testMethod void testError2(){
        init();
        
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(2));
        Id recTypeAssignment = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Assignment').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(null, recTypeAssignment);
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.New_Booking_Unit__c = lstBookingUnits[1].Id;
        //objCase.Commencement_Date__c = date.today();
        insert objCase;
        Test.startTest();
        objCase.Status = 'Closed';
        set<Id> tempSet = new set<Id>();
        tempSet.add(objCase.Id);
        CaseTriggerHandlerRPAssignment.processCase(tempSet);
        //update objCase;
        
        Test.stopTest();
    }
    
    private static testMethod void testError3(){
        init();
        
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(3));
        Id recTypeAssignment = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Assignment').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(null, recTypeAssignment);
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.New_Booking_Unit__c = lstBookingUnits[1].Id;
        //objCase.Commencement_Date__c = date.today();
        insert objCase;
        Test.startTest();
        objCase.Status = 'Closed';
        set<Id> tempSet = new set<Id>();
        tempSet.add(objCase.Id);
        CaseTriggerHandlerRPAssignment.processCase(tempSet);
        //update objCase;
        
        Test.stopTest();
    }
    
    private static testMethod void testError4(){
        init();
        
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockRPAgreement(4));
        Id recTypeAssignment = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Assignment').getRecordTypeId();
        Case objCase = TestDataFactory_CRM.createCase(null, recTypeAssignment);
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.New_Booking_Unit__c = lstBookingUnits[1].Id;
        //objCase.Commencement_Date__c = date.today();
        insert objCase;
        Test.startTest();
        objCase.Status = 'Closed';
        set<Id> tempSet = new set<Id>();
        tempSet.add(objCase.Id);
        CaseTriggerHandlerRPAssignment.processCase(tempSet);
        //update objCase;
        
        Test.stopTest();
    }
}