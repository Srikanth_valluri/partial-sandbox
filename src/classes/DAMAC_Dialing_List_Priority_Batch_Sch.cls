global class DAMAC_Dialing_List_Priority_Batch_Sch implements Schedulable {
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new DAMAC_Dialing_List_Priority_Batch(new List <ID> ()), 1);
    }
}