/*
Description:  Rest Response Class for Email CLIENT to read responses
Developed By: DAMAC IT Team
*/

@RestResource(urlMapping='/api/*')
global without sharing class DAMAC_WAYBEO_REST_SERVICE{   
    
    @HttpPost
    global static void doPost() 
    { 
        try {
            RestRequest req = RestContext.request; 
            req.params.get('op');  
            system.debug('BODY>>>'+req.requestBody.tostring());            
            String statusVal = '';
            Boolean missingCountryCode = false;
            if(req.requestBody.tostring().length() > 0) {             
                DAMAC_WAYBEO_REST_REQUEST readPostCls = new DAMAC_WAYBEO_REST_REQUEST();
                readPostCls = DAMAC_WAYBEO_REST_REQUEST.parse(req.requestBody.tostring());
                try {
                    String params = '';
                    for (String val :req.params.keySet ()) {
                        params += val+' = '+req.params.get (val)+',';
                    }  
                    params = params.removeEnd (',');
                    Map <String, String> logDetails = new Map <String, String> ();
                    logDetails.put ('Body', req.requestBody.tostring());
                    logDetails.put ('Type', 'POST');
                    logDetails.put ('URL', req.requestURI);
                    logDetails.put ('Params', params);
                    logDetails.put ('Answered_time', readPostCls.answered_time);
                    logDetails.put ('Called_No', readPostCls.called_no );
                    logDetails.put ('Call_ID', readPostCls.callId);
                    logDetails.put ('Calling_Number', '');
                    logDetails.put ('completed_time', readPostCls.completed_time);
                    logDetails.put ('customer_clid', readPostCls.customer_clid );
                    logDetails.put ('Direction', readPostCls.direction);
                    logDetails.put ('Extension', readPostCLs.extension);
                    logDetails.put ('Inquiry_Id', readPostCls.inquiry_id);
                    logDetails.put ('is_missed', readPostCls.is_missed);
                    logDetails.put ('Mobile', '');
                    logDetails.put ('Recording_url', readPostCls.recording_url);
                    logDetails.put ('Wrap_Up', readPostCls.wrapup);
                    boolean isSuccess = createWaybeoLog (logDetails);
                    
                    // Success Operations
                    if (req.requestURI == '/api/create' && isSuccess) {
                        statusVal =  '{"Success": "Responses Logged Successfully for Create Call", "Call_Id": "'+readPostCls.callId+'"}';
                    }
                    if (req.requestURI == '/api/update' && isSuccess ) {                   
                        statusVal =  '{"Success": "Responses Logged Successfully for Update Call", "Call_id": "'+readPostCls.callId+'"}';
                    }
                    // Failed Operations
                    if (req.requestURI == '/api/create' && !isSuccess) {
                        statusVal =  '{"Success": "Response Logging failed for Create Call", "Call_Id": "'+readPostCls.callId+'"}';
                    }
                    if (req.requestURI == '/api/update' && !isSuccess ) {                   
                        statusVal =  '{"Success": "Responses Logging failed for Update Call", "Call_id": "'+readPostCls.callId+'"}';
                    }
                    
                }catch (Exception e) {
                    statusVal =  '{"Error": "Salesforce Exception Reading the Request Body", "ErrorMessage": "'+e.getMessage()+'"}';
                }
            }            
        } catch (Exception e) {
            createLog(NULL, e.getMessage ());
        }
    }
    
    // Error Log
    public static void createLog (id parentId, String msg) {
        Log__c log = new Log__c ();
        log.Description__c = msg;
        log.Notification_Email__c = Label.Log_Notification_Email;
        log.Type__c = 'waybeo';
        insert log;
        
    }
    //@Future
    public static boolean createWaybeoLog (Map <String, String> logDetails) {
        boolean isSuccess = false;
        try{
            Waybeo_Logs__c log = new Waybeo_Logs__c ();
            log.Name =   logDetails.get ('Call_ID');  
            log.Request_Body__c = logDetails.get ('Body');
            log.Request_Type__c = logDetails.get ('Type');
            log.Request_URL__c = logDetails.get ('URL');
            log.Request_Params__c = logDetails.get ('Params');
            log.Answered_time__c = logDetails.get ('Answered_time');
            log.Called_No__c = logDetails.get ('Called_No');
            log.Call_ID__c = logDetails.get ('Call_ID');
            log.Calling_Number__c = logDetails.get ('Calling_Number');
            log.completed_time__c = logDetails.get ('completed_time');
            log.customer_clid__c = logDetails.get ('customer_clid');
            log.Direction__c = logDetails.get ('Direction');
            log.Extension__c = logDetails.get ('Extension');
            log.Inquiry_Id__c = logDetails.get ('Inquiry_Id');
            log.is_missed__c = logDetails.get ('is_missed');
            log.Mobile__c = logDetails.get ('Mobile');
            log.Recording_url__c = logDetails.get ('Recording_url');
            log.Wrap_Up__c = logDetails.get ('Wrap_Up');
            insert log;
            isSuccess = true;
        }catch(exception e){
            system.debug('Error====='+e.getMessage());
        }
        return isSuccess;
    }
}