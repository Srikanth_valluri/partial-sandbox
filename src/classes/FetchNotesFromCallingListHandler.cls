/***********************************************************************
* 
* Version            Date            Author            Description
* 1.0               18/12/17                            Initial Draft
**********************************************************************/
public with sharing class FetchNotesFromCallingListHandler {
    public String accountId{get;set;}
    public static List<Case>lstOfAllCase{get;set;}
    public static List<Account>AccountList{get;set;}
    public static List<Calling_List__c>lstOfAllCallingList{get;set;}
    public static Map<Id,Calling_List__c>mapOFIdsToCallingLst{get;set;}
    public static List<Note>callingToNotesLst{get;set;}
    public static Map<Id,Case>mapOfIdsToCase{get;set;}
    public static Map<Id,Account> mapAccIdToAccount{get;set;}
    public static List<Note>noteListFromNotesAndAttachmentForAccount {get;set;}
    public static List<Note>noteListFromNotesAndAttachmentForCalling {get;set;}
    public static List<Note>noteListFromNotesAndAttachmentForCases {get;set;}
    public static Map<Id,List<ContentDocumentLink>>callingToNoteLst{get;set;}
    public static list<ContentDocumentLink> contentDocLinkLstForCalling{get;set;}
    public static list<ContentDocumentLink> contentDocLinkLstForCases{get;set;}
    public static list<ContentDocumentLink>contentDocLinkLstForAccounts{get;set;}
    public static List<CalllingToNotesWrapper>lstCalllingToNotesWrapperForCalling{get;set;}
    public static List<CalllingToNotesWrapper>lstCalllingToNotesWrapperForAccounts{get;set;}
    public static List<CalllingToNotesWrapper>lstCalllingToNotesWrapperForCases{get;set;}
    public static List<CalllingToNotesWrapper>lstOfWrapperForAccounts{get;set;}
    public static List<CalllingToNotesWrapper>lstOfWrapperForCallings{get;set;}
    public static List<CalllingToNotesWrapper>lstOfWrapperForCases{get;set;}
    public FetchNotesFromCallingListHandler(ApexPages.StandardController controller) {
        fetchNotes();
    }
      /*This Method is used to fetch all the Notes of all the calling list*/
    public void fetchNotes(){
         lstCalllingToNotesWrapperForCalling = new List<CalllingToNotesWrapper>();
         lstCalllingToNotesWrapperForAccounts = new List<CalllingToNotesWrapper>();
         lstCalllingToNotesWrapperForCases = new List<CalllingToNotesWrapper>();
         lstOfWrapperForAccounts = new List<CalllingToNotesWrapper>();
         lstOfWrapperForCallings = new List<CalllingToNotesWrapper>();
         lstOfWrapperForCases = new List<CalllingToNotesWrapper>();
         AccountList = new List<Account>();
         mapOfIdsToCase = new Map<Id,Case>();
         lstOfAllCase = new List<Case>();
         contentDocLinkLstForCalling = new list<ContentDocumentLink>();
         contentDocLinkLstForAccounts = new list<ContentDocumentLink>();
         contentDocLinkLstForCases = new list<ContentDocumentLink>();
         noteListFromNotesAndAttachmentForAccount = new List<Note>();
         noteListFromNotesAndAttachmentForCalling = new List<Note>();
         noteListFromNotesAndAttachmentForCases = new List<Note>();
         callingToNotesLst = new List<Note>();
         lstOfAllCallingList = new List<Calling_List__c>();
         mapOFIdsToCallingLst = new Map<Id,Calling_List__c>();
         callingToNoteLst = new Map<Id,List<ContentDocumentLink>>();
         accountId = ApexPages.currentPage().getParameters().get('id');
         System.debug('accountId:'+accountId);
         mapAccIdToAccount = new Map<Id,Account>();
         set<Id>callIdLst = new Set<Id>();
         set<Id>caseIdLst = new Set<Id>();
         List<Id>notesIdLstForCalling = new List<Id>();
         List<Id>notesIdLstForAccounts = new List<Id>();
         List<Id>notesIdLstForCases = new List<Id>();
         
         AccountList = [SELECT Id,
                                name,
                                RecordType.Name,
                                ownerId,
                                owner.Name,
                                (SELECT Id,
                                        CaseNumber,
                                        RecordType.Name
                                   FROM Cases),
                                (SELECT Id,
                                        Name,
                                        ownerId,
                                        owner.Name,
                                        RecordType.Name
                                   FROM Calling_List__r)
                            FROM Account
                            WHERE Id = :accountId];
        
         for(Account accInst : AccountList){
            if(accInst.Calling_List__r != null && !accInst.Calling_List__r.isEmpty()){
                lstOfAllCallingList.addAll(accInst.Calling_List__r);
            }
            if(accInst.Cases != null && !accInst.Cases.isEmpty()){
                lstOfAllCase.addAll(accInst.Cases);
            }
            if(accInst != null){
                mapAccIdToAccount.put(accInst.Id,accInst);
            }
         }
         if(lstOfAllCallingList != null && !lstOfAllCallingList.isEmpty()){
             for(Calling_List__c callInst : lstOfAllCallingList){
                mapOFIdsToCallingLst.put(callInst.Id,callInst);
             }
         }
         if(lstOfAllCase != null && !lstOfAllCase.isEmpty()){
           for(Case caseInst : lstOfAllCase){
             mapOFIdsToCase.put(caseInst.Id,caseInst);
           }
         }
         if(mapAccIdToAccount != null & !mapAccIdToAccount.isEmpty()){
            /*fetch notes from notes and attachments from account*/
            noteListFromNotesAndAttachmentForAccount = [SELECT Id,
                                                                Body,
                                                                Title,
                                                                CreatedDate,
                                                                OwnerId,
                                                                Owner.Name,
                                                                parentId 
                                                           FROM Note 
                                                           WHERE parentId = : AccountId];
            
                for(Note noteForAcc: noteListFromNotesAndAttachmentForAccount){
                    lstOfWrapperForAccounts.add(new CalllingToNotesWrapper('Account',
                                                                            mapAccIdToAccount.get(noteForAcc.parentId),
                                                                            null,
                                                                            null,
                                                                            null));
                }
         }
         contentDocLinkLstForAccounts = [SELECT Id ,
                                                    LinkedEntityId,
                                                    ContentDocumentId,
                                                    ContentDocument.title,
                                                    ContentDocument.Description  
                                               FROM ContentDocumentLink 
                                              WHERE LinkedEntityId 
                                               = : AccountId];
       
         if(mapOFIdsToCallingLst != null && !mapOFIdsToCallingLst.isEmpty()){
             callIdLst.addAll(mapOFIdsToCallingLst.keySet());
              /*fetch notes from notes and attachments from calling list*/
                noteListFromNotesAndAttachmentForCalling = [SELECT Id,
                                                                Body,
                                                                Title,
                                                                CreatedDate,
                                                                OwnerId,
                                                                Owner.Name,
                                                                parentId 
                                                           FROM Note 
                                                           WHERE parentId IN : callIdLst];
                
                for(Note noteForCalling: noteListFromNotesAndAttachmentForCalling){
                    lstOfWrapperForCallings.add(new CalllingToNotesWrapper('Calling List',
                                                                            mapOFIdsToCallingLst.get(noteForCalling.parentId),
                                                                            null,
                                                                            null,
                                                                            null));
                }
                /**/
                contentDocLinkLstForCalling = [SELECT Id ,
                                                      LinkedEntityId,
                                                      ContentDocumentId,
                                                      ContentDocument.title,
                                                      ContentDocument.Description  
                                                 FROM ContentDocumentLink 
                                                WHERE LinkedEntityId 
                                                  IN : callIdLst];
            System.debug('contentDocLinkLstForCalling:::calling:!!'+contentDocLinkLstForCalling);
         }
         if(mapOFIdsToCase != null && !mapOFIdsToCase.isEmpty()){
                caseIdLst.addAll(mapOFIdsToCase.keySet());
                 /*fetch notes from notes and attachments for calling list*/
                noteListFromNotesAndAttachmentForCases = [SELECT Id,
                                                                Body,
                                                                Title,
                                                                CreatedDate,
                                                                OwnerId,
                                                                Owner.Name,
                                                                parentId 
                                                           FROM Note 
                                                           WHERE parentId IN : caseIdLst];
                
                for(Note noteForCases : noteListFromNotesAndAttachmentForCases){
                    lstOfWrapperForCases.add(new CalllingToNotesWrapper('Cases',
                                                                            mapOFIdsToCase.get(noteForCases.parentId),
                                                                            null,
                                                                            null,
                                                                            null));
                }
                /**/
                contentDocLinkLstForCases =  [SELECT Id ,
                                                LinkedEntityId,
                                                ContentDocumentId,
                                                ContentDocument.title,
                                                ContentDocument.Description  
                                           FROM ContentDocumentLink 
                                          WHERE LinkedEntityId 
                                           IN : caseIdLst];
            System.debug('contentDocLinkLstForCases::cases::!!'+contentDocLinkLstForCases);
         }
         /*fill wrapper list with notes for accounts*/
         for(Integer index = 0; index < lstOfWrapperForAccounts.size(); index ++) {
            lstOfWrapperForAccounts[index].NoteListToMove = noteListFromNotesAndAttachmentForAccount[index];
         }   
         
         /*fill wrapper list with notes for calling list*/
         for(Integer index = 0; index < lstOfWrapperForCallings.size(); index ++) {
            lstOfWrapperForCallings[index].NoteListToMove = noteListFromNotesAndAttachmentForCalling[index];
         } 
         System.debug('lstOfWrapperForCallings::!!!'+lstOfWrapperForCallings);
          /*fill wrapper list with notes for cases*/
         for(Integer index = 0; index < lstOfWrapperForCases.size(); index ++) {
            lstOfWrapperForCases[index].NoteListToMove = noteListFromNotesAndAttachmentForCases[index];
         } 
         
         for(ContentDocumentLink linkObjForAccounts : contentDocLinkLstForAccounts){
            notesIdLstForAccounts.add(linkObjForAccounts.contentDocumentId);
            lstCalllingToNotesWrapperForAccounts.add(new CalllingToNotesWrapper('Account',
                                                                                mapAccIdToAccount.get(linkObjForAccounts.LinkedEntityId),
                                                                                null,
                                                                                null,
                                                                                linkObjForAccounts.contentDocumentId));
         }
         for(ContentDocumentLink linkObjForCalling : contentDocLinkLstForCalling){
            notesIdLstForCalling.add(linkObjForCalling.contentDocumentId);
            lstCalllingToNotesWrapperForCalling.add(new CalllingToNotesWrapper('Calling List',
                                                                                mapOFIdsToCallingLst.get(linkObjForCalling.LinkedEntityId),
                                                                                null,
                                                                                null,
                                                                                linkObjForCalling.contentDocumentId));
         }
         for(ContentDocumentLink linkObjForCases : contentDocLinkLstForCases){
            notesIdLstForCases.add(linkObjForCases.contentDocumentId);
            lstCalllingToNotesWrapperForCases.add(new CalllingToNotesWrapper('Cases',
                                                                              mapOFIdsToCase.get(linkObjForCases.LinkedEntityId),
                                                                              null,
                                                                              null,
                                                                              linkObjForCases.contentDocumentId));
         }
         Map<Id,ContentNote>IdToNoteMapForAccounts = new Map<Id,ContentNote>([SELECT Id,
                                                                                     Title,
                                                                                    CreatedDate,
                                                                                    TextPreview,
                                                                                    OwnerId, 
                                                                                    Owner.Name  
                                                                                FROM ContentNote 
                                                                               WHERE Id 
                                                                                  IN :notesIdLstForAccounts
                                                                            ORDER BY CreatedDate 
                                                                                desc
                                                                               limit 950]);
         Map<Id,ContentNote>IdToNoteMapForCalling = new Map<Id,ContentNote>([SELECT Id,
                                                                                    Title,
                                                                                    CreatedDate,
                                                                                    TextPreview,
                                                                                    OwnerId, 
                                                                                    Owner.Name 
                                                                               FROM ContentNote 
                                                                              WHERE Id 
                                                                                 IN :notesIdLstForCalling
                                                                           ORDER BY CreatedDate 
                                                                                desc
                                                                               limit 950]);
        
         Map<Id,ContentNote>IdToNoteMapForCases = new Map<Id,ContentNote>([SELECT Id,
                                                                                    Title,
                                                                                    CreatedDate,
                                                                                    TextPreview,
                                                                                    OwnerId, 
                                                                                    Owner.Name
                                                                               FROM ContentNote 
                                                                              WHERE Id 
                                                                                 IN :notesIdLstForCases
                                                                            ORDER BY CreatedDate 
                                                                                desc
                                                                               limit 950]);
        
    
        for(Integer index = 0; index < lstCalllingToNotesWrapperForAccounts.size(); index ++) {
             if(IdToNoteMapForAccounts.containsKey(lstCalllingToNotesWrapperForAccounts[index].NoteId)){
                lstCalllingToNotesWrapperForAccounts[index].NoteLst = IdToNoteMapForAccounts.get(lstCalllingToNotesWrapperForAccounts[index].NoteId);
             }
        }                              
        for(Integer index = 0; index < lstCalllingToNotesWrapperForCalling.size(); index ++) {
             if(IdToNoteMapForCalling.containsKey(lstCalllingToNotesWrapperForCalling[index].NoteId)){
                lstCalllingToNotesWrapperForCalling[index].NoteLst = IdToNoteMapForCalling.get(lstCalllingToNotesWrapperForCalling[index].NoteId);
             }
        }
        for(Integer index = 0; index < lstCalllingToNotesWrapperForCases.size(); index ++) {
             if(IdToNoteMapForCases.containsKey(lstCalllingToNotesWrapperForCases[index].NoteId)){
                lstCalllingToNotesWrapperForCases[index].NoteLst = IdToNoteMapForCases.get(lstCalllingToNotesWrapperForCases[index].NoteId);
             }
        }
        System.debug('lstCalllingToNotesWrapperForCases::::'+lstCalllingToNotesWrapperForCases);
    }
    
    public class CalllingToNotesWrapper{
        public Case caseObj{get;set;}
        public Account accObj{get;set;}
        public Calling_List__c callObj{get;set;}
        public ContentNote NoteLst{get;set;}
        public Note NoteListToMove {get;set;}
        public Id NoteId{get;set;}
        public String sobjectName {get;set;}
         public CalllingToNotesWrapper(String sobjectNameW, Calling_List__c callObjW,ContentNote NoteLstW,Note NoteListToMoveW ,Id NoteIdW){
            sobjectName = sobjectNameW;
            callObj = callObjW;
            NoteLst = NoteLstW;
            NoteListToMove = NoteListToMoveW;
            NoteId = NoteIdW;
        }
        public CalllingToNotesWrapper( String sobjectNameW,Case caseObjW,ContentNote NoteLstW,Note NoteListToMoveW, Id NoteIdW){
            sobjectName = sobjectNameW;
            caseObj = caseObjW;
            NoteLst = NoteLstW;
            NoteListToMove = NoteListToMoveW;
            NoteId = NoteIdW;
        }
        public CalllingToNotesWrapper( String sobjectNameW,Account accObjW,ContentNote NoteLstW,Note NoteListToMoveW,Id NoteIdW){
            sobjectName = sobjectNameW;
            accObj = accObjW;
            NoteLst = NoteLstW;
            NoteListToMove = NoteListToMoveW;
            NoteId = NoteIdW;
        }
        
        
    }

}