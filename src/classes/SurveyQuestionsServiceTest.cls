/*
* Test class for SurveyQuestionsService class
*/
@isTest
public class SurveyQuestionsServiceTest{
    /*
    *   When no account is found
    */
    @isTest
    public static void getsurveyQuestionsTest_NoAccount(){
        CreRoutingTest.createCREUsers('creTest@primaryUser.com', '7000');
        

        RestRequest req = new RestRequest();
        req.httpMethod = 'POST';
        req.requestUri = '/surveyQuestions';
        req.requestBody = Blob.valueOf('Portal');
        RestContext.request = req;
        RestContext.response = new RestResponse();
        Survey_CRM__c sCRM=new Survey_CRM__c();
        sCRM.Is_Active__c=true;
        sCRM.Type__c='Portal';
        sCRM.End_Date__c=Date.Today().addDays(10);
        sCRM.Start_Date__c=Date.Today().addDays(-10);
        insert sCRM;
        Survey_Question_CRM__c sQuestion=new Survey_Question_CRM__c();
        sQuestion.Name='Share Feedback';
        sQuestion.Question__c='From your experience, please share your feedback by marking appropriate boxes below';
        sQuestion.Type__c='Section';
        sQuestion.Survey__c=sCRM.Id;
        sQuestion.Required__c=true;
        sQuestion.Order_Number_Displayed__c='1';
        sQuestion.OrderNumber__c=1;
        //sQuestion.Choices__c='Once;Twice;3 - 5 Times;More Than 5 Times;NA';
        insert sQuestion; 
        
        Survey_Question_CRM__c sQuestion1=new Survey_Question_CRM__c();
        sQuestion1.Name='Times DAMAC Contacted';
        sQuestion1.Question__c='How many times did you contact / visit our customer service department before your problem was resolved?';
        sQuestion1.Type__c='Single Select--Horizontal';
        sQuestion1.Survey__c=sCRM.Id;
        sQuestion1.Required__c=true;
        sQuestion1.Order_Number_Displayed__c='1';
        sQuestion1.OrderNumber__c=1;
        sQuestion1.Choices__c='Once;Twice;3 - 5 Times;More Than 5 Times;NA';
        sQuestion1.Survey_Question__c=sQuestion.Id;
        insert sQuestion1; 
        Test.startTest();
        SurveyQuestionsService.getsurveyQuestions();
        Test.stopTest();
        /*String saveSurveyLogicResponse = RestContext.response.responseBody.toString();
        System.debug('response..'+saveSurveyLogicResponse);
         List<SurveyQuestionWrapper> response1 =
            (List<SurveyQuestionWrapper>)JSON.deserializeStrict(
                saveSurveyLogicResponse ,
                SurveyQuestionWrapper.Class);
                
                System.debug('response..'+response1);
        System.assertEquals(400,RestContext.response.statusCode);

        String saveSurveyLogicResponse = RestContext.response.responseBody.toString();
        SaveSurveyLogic.SaveSurveyResponseBody response =
            (SaveSurveyLogic.SaveSurveyResponseBody)JSON.deserializeStrict(
                saveSurveyLogicResponse ,
                SaveSurveyLogic.SaveSurveyResponseBody.Class);
         System.assertEquals(response.errorMessage, 'No Account found for the given calling number');*/
    }
}