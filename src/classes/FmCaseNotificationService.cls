public with sharing class FmCaseNotificationService implements IsNotificationService {
    public List<NotificationService.Notification> getNotifications() { return null;}

    public List<NotificationService.Notification> getNotifications(NotificationService.Notification request) {
        List<NotificationService.Notification> notificationList = new List<NotificationService.Notification>();
        if (String.isBlank(request.accountId)) {
            return notificationList;
        }
        Id accountId = request.accountId;
        for(FM_Case__c sr : [
            SELECT      Id
                        , Name
                        , RecordType.Name
                        , Status__c
                        , Booking_Unit__r.Unit_Name__c
            FROM        FM_Case__c
            WHERE       RecordType.Name != 'Violation Notice'
                        AND Notifications_Read__c = false
                        AND (Account__c = :accountId
                            OR Booking_Unit__r.Booking__r.Account__c = :accountId
                            OR Booking_Unit__r.Tenant__c = :accountId)
            ORDER BY    LastModifiedDate DESC
        ]) {
            NotificationService.Notification notification = new NotificationService.Notification();
            notification.accountId = accountId;
            //String status = caseRecord.Status == 'New' ? 'Created' : 'Closed';
            notification.title      = sr.Name + ' ' + sr.RecordType.Name + ' Request is ' + sr.Status__c;
            notification.recordId   = sr.Id;
            notification.status     = sr.Status__c;
            notification.record     = sr;
            notificationList.add(notification);
        }
        return notificationList;
    }

    @RemoteAction
    public static list<NotificationService.Notification> markRead(
        List<NotificationService.Notification> notificationList
    ) {
        List<FM_Case__c> lstNotice = new List<FM_Case__c>();
        for (NotificationService.Notification notification: notificationList) {
            if (String.isNotBlank(notification.recordId)) {
                lstNotice.add(new FM_Case__c(Id=notification.recordId, Notifications_Read__c=true));
                notification.isRead = true;
            }
        }
        update lstNotice;
        return notificationList;
    }

    public NotificationService.Notification markUnread(NotificationService.Notification notification) {
        update new FM_Case__c(Id = notification.recordId, Notifications_Read__c = false);
        notification.isRead = true;
        return notification;
    }
}