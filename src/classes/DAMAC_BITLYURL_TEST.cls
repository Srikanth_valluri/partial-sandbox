@isTest
public class DAMAC_BITLYURL_TEST{
    
    @isTest static void test_method_one() {
        Stand_Inquiry__c inq = new Stand_Inquiry__c ();
        inq.Mobile_Phone_Encrypt__c = '8977365305';
        inq.Mobile_CountryCode__c = 'India: 0091';
        inq.SMS_MSG_Id__c = '9876543210';
        insert inq;
        
        set<id> inqId = new set<id>();
        inqId.add(inq.id);
        
        Test.startTest();        
            string shortUrl = DAMAC_BITLYURL.getShortenedURL('https://damacholding--fullcopy.cs84.my.salesforce.com');
            DAMAC_Get_Bitly_URL_Stand_Inquiries.generateShortURL(inqId);
        Test.stopTest();
    }
}