/************************************************************************************************
 * @Name              : ApplySalesMarginToInventoriesTest
 * @Description       : Test Class for ApplySalesMarginToInventoriesController
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         19/08/2020       Created
***********************************************************************************************/

@isTest
public class ApplySalesMarginToInventoriesTest{
  @isTest
    static void testApplySalesMargin(){
        
        Location__c loc = new Location__c();
        loc.Name = 'VCT';
        loc.Location_ID__c = '270791';
        insert loc;
        
        Sales_Margins__c salesMargin = new Sales_Margins__c();
        salesMargin.Marketing_Name__c = 'BAIT AL ASEEL';
        salesMargin.Bedroom_Type__c = '3';
        salesMargin.Building_Location__c = 'VCT';
        salesMargin.ACD__c = '2021-12-30';
        salesMargin.Property_Name__c = 'VICTORIA @AKOYA OXYGEN';
        salesMargin.View_Type__c = 'Back';
        salesMargin.Is_Active__c = true;
        salesMargin.Avg_System_PSF__c = 881.19;
        salesMargin.Floor_Location__c = 'VCT';
        insert salesMargin;
        
        Inventory__c inv = new Inventory__c();
        inv.Marketing_Name__c = 'BAIT AL ASEEL';
        inv.Bedroom_Type__c = '3';
        inv.Building_Location__c = loc.Id;
        inv.ACD_Date__c = '2021-12-30';
        inv.Sales_Margins__c = salesMargin.id;
        inv.Property_Name__c = 'VICTORIA @AKOYA OXYGEN';
        inv.View_Type__c = 'Back';       
        inv.Floor_Location__c = loc.Id;
        inv.Status__c = 'Released';        
        inv.Current_AC_Area__c = 1670;
        inv.List_Price__c = 1291000.00;
        inv.Current_AC_Area__c= 1670;
        inv.Current_Saleable_Area__c = 2296;
        inv.Current_Standard_Price__c = 1328400;
        insert inv;
        
        Inventory__c inv2 = new Inventory__c();
        inv2.Marketing_Name__c = 'BAIT AL ASEEL';
        inv2.Bedroom_Type__c = '3';
        inv2.Building_Location__c = loc.Id;
        inv2.ACD_Date__c = '2021-12-30';
        inv2.Property_Name__c = 'VICTORIA @AKOYA OXYGEN';
        inv2.View_Type__c = 'Back';       
        inv2.Floor_Location__c = loc.Id;
        inv2.Status__c = 'Released';
        inv2.Sales_Margins__c = salesMargin.id;
        inv2.Current_AC_Area__c = 1670;
        inv2.List_Price__c = 1291000.00;
        inv2.Current_AC_Area__c= 1670;
        inv2.Current_Saleable_Area__c = 2296;
        inv2.Current_Standard_Price__c = 1328400;
        insert inv2;
        
        
       
        
               
        ApplySalesMarginToInventoriesController.applyToInventories(salesMargin.Id);
        ApplySalesMarginToInventoriesController.updatePSF(salesMargin.Id);
        
    }
}