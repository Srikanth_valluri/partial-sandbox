@isTest
public class AgencyContactListViewControllerTest {
    @isTest static void test_method_one(){
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles',
        UserName=uniqueUserName);
        System.runAs(u) {
            List<Account> accList = new List<Account>();
            Account acc = InitialiseTestData.getCorporateAccount('Acc-21');
        	acc.Trade_License_Number__c='123';
            acc.Agency_Type__c = Label.Agency_Type_Individual;
            accList.add(acc);
        	insert accList ; 
 
			Id RecTypeId= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual Agency').getRecordTypeId();
            Account Accnt = new Account(
  				RecordTypeID=RecTypeId,
  				FirstName='Test FName',
  				LastName='Test FName',
  				PersonMailingStreet='Test FName',
  				PersonMailingPostalCode='412234',
               	PersonMailingCity='SFO',
                  PersonEmail='test@yahoo.com',
                  PersonHomePhone='1234567',
                  PersonMobilePhone='12345678' ,
                Agency_Type__c = Label.Agency_Type_Individual
);
 
insert Accnt;
            //ApexPages.StandardSetController controller = new ApexPages.StandardSetController(acc);
         	AgencyContactListViewController agencyContactObj = new AgencyContactListViewController(new ApexPages.StandardSetController(accList));
        
        	List<Contact> conList = new List<Contact>();
        	Contact con = InitialiseTestData.getCorporateAgencyContact('Process Flow Agency');
        	conList.add(con);
        	insert conList ; 
            agencyContactObj.getIndividualAgencyContacts();
            AgencyContactListViewController.IndividualWrapper wrapper =new AgencyContactListViewController.IndividualWrapper(); 
            
                        
        }           
    }
}