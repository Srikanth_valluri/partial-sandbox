/************************************************************************************************
 * @File Name          : DailyDpInvoiceEmailBatch.cls                                           *
 * @Description        : This Batch is used to populate COCD letter for DP invoice.             *
 * @Author             : Dipika Rajput                                                          *
 * @Group              :                                                                        *
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 12/18/2019, 2:26:21 PM
 * @Modification Log   :                                                                        * 
 *===============================================================================================
 * Ver      Date            Author                  Modification                                *
 *===============================================================================================
 * 1.0                      Dipika Rajput           Initial Version                             *
 * 1.1      21/07/2019      Aishwarya Todkar        Sending mails based on nationality          *
*************************************************************************************************/
public without sharing class DailyDpInvoiceEmailBatch implements Database.Batchable<sObject>
                                                                , Database.AllowsCallouts
                                                                , Database.Stateful { 
    private String query;
     public String recordId;
    Set<String> setArabicNationality = new Set<String> {'saudi','jordanian','omani', 'uae' , 'kuwaiti', 'qatari', 'lebanese', 'libyan' , 'Kazak', 'egyption', 'egyptianiraqi', 'iranian'};
    Set<String> setChinesenationality = new Set<String> {'chinese', 'hongkong', 'vietnam', 'singaporean', 'republic of china', 'taiwanese'};
    List<DP_Invoices__c> lstInvoicesTUpdate;
    List<EmailMessage> lstEmails;
    public String regId;
    public List<Attachment> attachmentUrlsForEmailMessage; 
    public Database.Querylocator start( Database.BatchableContext bc ) {
        recordId =  Label.dailyDpBatchForSingleUser.split('-',2)[1];
        query = ' SELECT Id,'+ 
                ' Name,'+
                ' OwnerId,'+ 
                ' BookingUnits__c,'+
                ' Unit_Name__c,'+
                ' Description__c,'+
                ' Registration_Id__c,'+
                ' Property_Name__c,'+ 
                ' Trxn_Number__c,'+
                ' Party_Id__c,'+
                ' Due_Date__c,'+
                ' Milestone__c,'+
                ' Type_of_Milestone__c,'+
                ' Customer_Name__c,'+ 
                ' Email_To_Sent__c,'+
                ' Invoice_Date__c,'+
                ' Other_Language_TAX_Invoice__c,'+
                ' Other_Language_SOA__c,'+
                ' AccountName__c,'+
                ' AccountNo__c,'+
                ' BankName__c,'+
                ' Seller_Name__c,'+
                ' IBAN__c,'+
                ' SWIFT__c,'+
                ' BranchName__c,'+
                ' Email__c,'+
                ' EmailSent__c,'+
                ' Accounts__c, '+
                ' Accounts__r.Nationality__c, '+
                ' Accounts__r.Primary_Language__c, '+
                ' Accounts__r.Nationality__pc, '+
                ' Accounts__r.recordTypeId, '+
                ' Accounts__r.email__c, '+
                ' Accounts__r.email__pc, '+
                ' Accounts__r.First_Name_Arabic__c, '+
                ' Accounts__r.Last_Name_Arabic__c, '+
                ' Accounts__r.Name, '+
                ' Accounts__r.Salutation ,' +
                ' Cover_Letter__c, '+
                ' SOA__c,'+
                ' TAX_Invoice__c,'+
                ' BookingUnits__r.Registration_ID__c,'+
                ' BookingUnits__r.Booking__r.Account__c,'+
                ' BookingUnits__r.Inventory__r.Building_Name__c,'+
                ' BookingUnits__r.Unit_Name__c,'+
                ' COCD_Letter__c,'+
                ' Invoice_URLs__c' +
                ' FROM DP_Invoices__c '+
                ' Where ';
                if(Label.dailyDpBatchForSingleUser.split('-',2)[0] == 'ON'){
                    query +=' ID = :recordId';
                }
                else{
                     query += ' EmailSent__c = FALSE AND Email_To_Sent__c != \'Hold\' '+
                              ' AND TAX_Invoice__c != NULL AND SOA__c != NULL AND Cover_Letter__c != NULL'+
                              ' order by Accounts__r.name';
                }
               // ' TAX_Invoice__c = NULL '+
              //  ' AND Other_Language_TAX_Invoice__c = NULL';
        System.debug('Dp Invoice query ::: '+Database.getQueryLocator(query));
        return Database.getQueryLocator(query);
    }
    public void execute( Database.BatchableContext bc, list<DP_Invoices__c> dpInvoiceRecords ) {
       
        lstEmails = new List<EmailMessage>();
        lstInvoicesTUpdate = new List<DP_Invoices__c>();
        attachmentUrlsForEmailMessage = new List<Attachment>();
        System.debug('DailyDpInvoiceEmailBatch  execute dpInvoiceRecords  == ' + dpInvoiceRecords );
        for(DP_Invoices__c objInvoice: dpInvoiceRecords){ 
            sendEmail(objInvoice); 
        }

        System.debug('lstEmails=== ' + lstEmails);
        if(lstEmails.size() > 0) {
            if(!Test.isRunningTest()) {
                insert lstEmails;
            }
            System.debug('lstEmails==after insert= ' + lstEmails);
        }
        //Insert attachment for emailMerssage
        System.debug('--attachmentUrlsForEmailMessage--'+attachmentUrlsForEmailMessage);
        for(emailMessage msgObj : lstEmails){
            for(attachment attachObj : attachmentUrlsForEmailMessage){
                attachObj.parentId = msgObj.Id;
            }
        }
        insert attachmentUrlsForEmailMessage;

        if(lstInvoicesTUpdate.size() > 0){ update lstInvoicesTUpdate;}
    }
    public void finish( Database.BatchableContext bc ) {
    }

    public void sendEmail(DP_Invoices__c objDPInvoice) {
        List<Attachment>attachmentUrls = new List<Attachment>();
        
        Id personRecTypeId, businessRecTypeId;
        personRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        businessRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        
        
        if(objDPInvoice.Accounts__c != NULL){
            System.debug('objDPInvoice.Accounts__c !!==' + objDPInvoice.Accounts__c);
            
            String emailTemplateName;Process_Email_Template__mdt emt;EmailTemplate emailTemplateObj;
            String toAddress = '', fromAddress,replyToAddress = '', contentType, contentValue,contentBody = '';String subject, strAccountId, strDPIId, bccAddress ='';
            string nationality = objDPInvoice.Accounts__r.Nationality__c != NULL ? objDPInvoice.Accounts__r.Nationality__c : objDPInvoice.Accounts__r.Nationality__pc;
            System.debug('nationality !!==' + nationality);
            if(string.isnotblank(nationality)) { 
                // Get Email template for UAE
                if(setArabicNationality.contains(nationality.toLowercase())) {
                    
                    // Fetch email template name based on milestone type
                    if(String.isNotBlank(objDPInvoice.Milestone__c) && String.isNotBlank(objDPInvoice.Type_of_Milestone__c)){

                        emailTemplateName = ProcessEmailTemplateSelector.getEmailTemplateNameForDpInvoice(
                            'Invoice Process',
                            'UAE',
                            objDPInvoice.Type_of_Milestone__c
                        );
                    }
                    
                    if(string.isnotblank(emailTemplateName)){
                        // Get Email Template record Based on template Name
                        emailTemplateObj = ProcessEmailTemplateSelector.fetchEmailTemplateRecord(emailTemplateName);
                    }
                }
                // Get Email template for Chinese
                else if(setChinesenationality.contains(nationality.toLowercase())) {
                  //  emailTemplateName = ProcessEmailTemplateSelector.getEmailTemplateName( 'Invoice Process', 'Chinese');
                   
                   // Fetch email template name based on milestone type
                    if(String.isNotBlank(objDPInvoice.Milestone__c) && String.isNotBlank(objDPInvoice.Type_of_Milestone__c)){

                        emailTemplateName = ProcessEmailTemplateSelector.getEmailTemplateNameForDpInvoice(
                            'Invoice Process',
                            'Chinese',
                            objDPInvoice.Type_of_Milestone__c
                        );
                    }
                    if(string.isnotblank(emailTemplateName)){
                        // Get Email Template record Based on template Name
                        emailTemplateObj = ProcessEmailTemplateSelector.fetchEmailTemplateRecord(emailTemplateName);
                    }
                }
                // Get Email template for Russian
                else if(nationality.equalsIgnoreCase('Russian')) {
                    //emailTemplateName = ProcessEmailTemplateSelector.getEmailTemplateName('Invoice Process', 'Russian');
                    
                    // Fetch email template name based on milestone type
                    if(String.isNotBlank(objDPInvoice.Milestone__c) && String.isNotBlank(objDPInvoice.Type_of_Milestone__c)){
                        emailTemplateName = ProcessEmailTemplateSelector.getEmailTemplateNameForDpInvoice(
                            'Invoice Process',
                            'Russian',
                            objDPInvoice.Type_of_Milestone__c
                        );
                    }
                    if(string.isnotblank(emailTemplateName)){
                        // Get Email Template record Based on template Name
                        emailTemplateObj = ProcessEmailTemplateSelector.fetchEmailTemplateRecord(emailTemplateName);
                    }
                }
                // Get Email template for Indian 
                else {
                   // emailTemplateName = ProcessEmailTemplateSelector.getEmailTemplateName('Invoice Process','Indian');
                    
                    // Fetch email template name based on milestone type
                    if(String.isNotBlank(objDPInvoice.Milestone__c) && String.isNotBlank(objDPInvoice.Type_of_Milestone__c)){
                        emailTemplateName = ProcessEmailTemplateSelector.getEmailTemplateNameForDpInvoice(
                            'Invoice Process',
                            'Indian',
                            objDPInvoice.Type_of_Milestone__c
                        );
                    } 
                    System.debug('emailTemplateName :::'+emailTemplateName );
                    if(string.isnotblank(emailTemplateName)){ 
                        // Get Email Template record Based on template Name
                        emailTemplateObj = ProcessEmailTemplateSelector.fetchEmailTemplateRecord(emailTemplateName);
                         System.debug('emailTemplateObj :::'+emailTemplateObj );
                    }
                }
            }
            // If not nationality present 
            else {
                 //emailTemplateName = ProcessEmailTemplateSelector.getEmailTemplateName('Invoice Process','Indian');
                   // Fetch email template name based on milestone type
                    if(String.isNotBlank(objDPInvoice.Milestone__c) && String.isNotBlank(objDPInvoice.Type_of_Milestone__c)){
                        emailTemplateName = ProcessEmailTemplateSelector.getEmailTemplateNameForDpInvoice(
                            'Invoice Process',
                            'Indian',
                            objDPInvoice.Type_of_Milestone__c
                        );
                    } 
                    System.debug('emailTemplateName :::'+emailTemplateName );
                    if(string.isnotblank(emailTemplateName)){
                        // Get Email Template record Based on template Name
                        emailTemplateObj = ProcessEmailTemplateSelector.fetchEmailTemplateRecord(emailTemplateName);
                         System.debug('emailTemplateObj :::'+emailTemplateObj );
                         
                    }
            }
            strAccountId = objDPInvoice.Accounts__c != null ? String.valueOf(objDPInvoice.Accounts__c ) : objDPInvoice.BookingUnits__r.Booking__r.Account__c;
            
            strDPIId = String.valueOf(objDPInvoice.id);
            bccAddress = Label.Sf_Copy_Bcc_Mail_Id; //SF_Copy_EMail_Address;
            //System.debug('bccAddress !!==' + bccAddress);

            // If Person Acount 
            if(objDPInvoice.Accounts__r.recordTypeId == personRecTypeId && objDPInvoice.Accounts__r.Email__pc != null) {
                 toAddress = objDPInvoice.Accounts__r.Email__pc;
            }

            //If business account
            else if(objDPInvoice.Accounts__r.recordTypeId == businessRecTypeId && objDPInvoice.Accounts__r.Email__c != null){  
                toAddress = objDPInvoice.Accounts__r.Email__c;
            }
            System.debug('toAddress = ' + toAddress);
            fromAddress = Label.AT_Your_Service;
            contentType = 'text/html';
            replyToAddress = Label.AT_Your_Service;
            // This method is used to replace html merge fields with values
            /*if(emailTemplateObj.htmlValue != NULL) {
                contentValue = ReplaceMergeFieldsOfEmailTemplate.replaceHtmlContent(
                    string.valueOf(emailTemplateObj.HtmlValue),'DP_Invoices__c',objDPInvoice.id
                );
            }*/
            String strCCAddress = '';
            System.debug('emailTemplateObj = ' + emailTemplateObj);       
            if(toAddress != '' && emailTemplateObj != null) {

                Subject = emailTemplateObj.subject != null ? emailTemplateObj.subject : 'Daily Invoice Mail';
                System.debug('SubjectL:::'+Subject);
                if(Subject.contains('{!Description}') && objDPInvoice.Description__c!= NULL){
                    Subject =Subject.replace('{!Description}',objDPInvoice.Description__c);
                }
                if(Subject.contains('{!UnitName}') && objDPInvoice.Unit_Name__c!= NULL){
                     Subject =Subject.replace('{!UnitName}',objDPInvoice.Unit_Name__c);
                }
                if(Subject.contains('{!DP_Invoices__c.Milestone}') && objDPInvoice.Milestone__c!= NULL){
                     Subject =Subject.replace('{!DP_Invoices__c.Milestone}',objDPInvoice.Milestone__c);
                }
                if(Subject.contains('{!DP_Invoices__c.Milestone}') && objDPInvoice.Milestone__c == NULL){
                     Subject =Subject.replace('{!DP_Invoices__c.Milestone}','');
                }
                System.debug('after replacement SubjectL:::'+Subject);
                if(emailTemplateObj.body != NULL){
                    contentBody = emailTemplateObj.body;
                }
                if(emailTemplateObj.htmlValue != NULL){
                    contentValue = emailTemplateObj.htmlValue;
                }    
                if(string.isblank(contentValue)){
                    contentValue='No HTML Body';
                }   
                System.debug('objDPInvoice.Invoice_URLs__c == ' + objDPInvoice.Invoice_URLs__c);

                 // TODO  add code fetch Attachment related to email template 
                if( emailTemplateObj != NULL ){
                    system.debug('emailTemplateObj' + emailTemplateObj);
                        Attachment[] attachList = [SELECT Id,ContentType,Name, Body From Attachment where parentId = :emailTemplateObj.Id];
                        system.debug('attachList' + attachList);
                    if( attachList != NUll && attachList.size() > 0)
                        attachmentUrls.addAll(attachList) ; 
                }
                
                if(String.isNotBlank(objDPInvoice.Invoice_URLs__c)) {
                   // contentBody = contentBody.replace('{!DP_Invoices__c.Invoice_URLs__c}', objDPInvoice.Invoice_URLs__c);
                   // contentValue = contentValue.replace('{!DP_Invoices__c.Invoice_URLs__c}', objDPInvoice.Invoice_URLs__c);
                    System.debug('---objDPInvoice.Invoice_URLs__c--'+objDPInvoice.Invoice_URLs__c);
                    List <Attachment> attachList = new List <Attachment>();
                    attachList = getSplittedUrls(objDPInvoice.Invoice_URLs__c);
                    attachmentUrls.addAll(attachList) ;  
                    System.debug('---attachmentUrls--'+attachmentUrls);
                }
                
                else {
                    contentBody = contentBody.replace('{!DP_Invoices__c.Invoice_URLs__c}', '');
                    contentBody = contentBody.replace('Please find attached a copy of your latest statement of account.', '');
                    contentValue = contentValue.replace('Please find attached a copy of your latest statement of account.', '');
                    contentValue = contentValue.replace('{!DP_Invoices__c.Invoice_URLs__c}', '');
                }
                
                if(contentValue.contains('{!CustomerSalution}') && objDPInvoice.Accounts__r.Salutation != NULL){
                     contentValue = contentValue.replace('{!CustomerSalution}', objDPInvoice.Accounts__r.Salutation);
                     contentBody = contentBody.replace('{!CustomerSalution}', objDPInvoice.Accounts__r.Salutation);
                }
                if(contentValue.contains('{!CustomerSalution}') && objDPInvoice.Accounts__r.Salutation == NULL){
                     contentValue = contentValue.replace('{!CustomerSalution}', '');
                     contentBody = contentBody.replace('{!CustomerSalution}', '');
                }
                
                


                contentValue = contentValue.replace('{!CustomerName}', GenericUtility.getCamelCase(objDPInvoice.Accounts__r.Name));
                contentBody = contentBody.replace('{!CustomerName}', GenericUtility.getCamelCase(objDPInvoice.Accounts__r.Name));
                
                if(!Test.isRunningTest()){
                    if(objDPInvoice.Property_Name__c != NULL){
                        contentValue = contentValue.replace( '{!DP_Invoices__c.Property_Name__c}', GenericUtility.getCamelCase(objDPInvoice.Property_Name__c)
                        );
                        contentBody = contentBody.replace('{!DP_Invoices__c.Property_Name__c}',  GenericUtility.getCamelCase(objDPInvoice.Property_Name__c)
                        );
                    }
                    if(objDPInvoice.Seller_Name__c!= NULL){
                        contentValue = contentValue.replace('{!DP_Invoices__c.Seller_Name__c}', GenericUtility.getCamelCase(objDPInvoice.Seller_Name__c));
                        contentBody = contentBody.replace('{!DP_Invoices__c.Seller_Name__c}', GenericUtility.getCamelCase(objDPInvoice.Seller_Name__c));
                    }
                }
                if(objDPInvoice.Due_Date__c!= NULL){ 
                    contentValue = contentValue.replace('{!DP_Invoices__c.Due_Date__c}',String.valueOf(objDPInvoice.Due_Date__c));
                    contentBody = contentBody.replace('{!DP_Invoices__c.Due_Date__c}', String.valueOf(objDPInvoice.Due_Date__c));
                }
                if(objDPInvoice.Milestone__c!= NULL){ 
                    contentValue = contentValue.replace('{!DP_Invoices__c.Milestone__c}',String.valueOf(objDPInvoice.Milestone__c));
                    contentBody = contentBody.replace('{!DP_Invoices__c.Milestone__c}', String.valueOf(objDPInvoice.Milestone__c));
                }
                if(objDPInvoice.Type_of_Milestone__c!= NULL){ 
                    contentValue = contentValue.replace('{!DP_Invoices__c.Type_of_Milestone__c}',String.valueOf(objDPInvoice.Type_of_Milestone__c));
                    contentBody = contentBody.replace('{!DP_Invoices__c.Type_of_Milestone__c}', String.valueOf(objDPInvoice.Type_of_Milestone__c));
                }

               
               
                system.debug('attachmentUrls' + attachmentUrls);
                
                
                System.debug('contentValue = ' + contentValue);
                // Callout to sendgrid to send an email
                SendGridEmailService.SendGridResponse objSendGridResponse =  
                    SendGridEmailService.sendEmailService(toAddress,
                    '', 
                    strCCAddress, 
                    '', 
                    bccAddress, 
                    '', 
                    subject,
                    '',
                    fromAddress,
                    '',
                    replyToAddress,
                    '',
                    contentType,
                    contentValue,
                    '',
                    attachmentUrls
                );
                system.debug('toAddress === ' + toAddress);
                system.debug('fromAddress === ' + fromAddress);
                system.debug('subject === ' + subject);
                system.debug('contentBody === ' + contentBody);
                system.debug('contentType === ' + contentType);
                system.debug('objSendGridResponse === ' + objSendGridResponse);
                String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;
                if( responseStatus == 'Accepted' ) {
                    EmailMessage mail = new EmailMessage();
                    mail.Subject = subject;
                    mail.MessageDate = System.Today();
                    mail.Status = '3';//'Sent';
                    mail.RelatedToId = strAccountId;
                    mail.Account__c  = strAccountId;
                    mail.Type__c = 'FM Invoice';
                    mail.ToAddress = toAddress;
                    mail.FromAddress = fromAddress;
                    mail.TextBody = contentBody; //contentValue.replaceAll('\\<.*?\\>', '');
                    mail.Sent_By_Sendgrid__c = true;
                    mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                    mail.DP_Invoices__c = strDPIId;
                    mail.CcAddress = strCCAddress;
                    mail.BccAddress = bccAddress;
                     
                    

                    // Create the list of attachments with respect to email message
                    for(attachment attachRecord : attachmentUrls){
                        attachment attachObj = new attachment();
                        //attachObj.ParentId  = mail.Id;
                        attachObj.contentType = attachRecord.contentType;
                        // System.debug('attach.name'+urlNameMap.get(responseUrl));
                        attachObj.name = attachRecord.name;
                        System.debug('attachObj.name'+attachObj.name);
                        attachObj.body = attachRecord.body;
                        attachmentUrlsForEmailMessage.add(attachObj);
                       /* if(! attachmentUrlsForEmailMessage.containsKey(mail)){
                            attachmentUrlsForEmailMessage.put(mail,new List<Attachment>{attachObj});  
                        }
                        else{
                            attachmentUrlsForEmailMessage.get(mail).add(attachObj);
                        }*/
                        
                    }
                    
                    lstEmails.add(mail);

                   
                 
                    system.debug('Mail obj == ' + mail);

                    objDPInvoice.EmailSent__c = true;
                    lstInvoicesTUpdate.add(objDPInvoice);
                }
            }
        }
    }
    
   
    List<Attachment> getSplittedUrls(String invoiceUrl) {
        System.debug('invoiceUrl:::::::::::'+invoiceUrl);
        String allUrls ='';
        List<Attachment> attachmentList = new List<Attachment>();
        Map<String,String> nameToUrlMap = new Map<String,String>();
        List<String> splittedStrLst = invoiceUrl.split('</li>');
        System.debug('splittedStrLst::::::::'+splittedStrLst);
        for(String singleUrlObj : splittedStrLst) {
            if(singleUrlObj.contains('<li><a href=')){
                String strAttUrl = singleUrlObj.subStringAfter('<li><a href="').subStringBefore('" target=');
                System.debug('strAttUrl:::::::::::'+strAttUrl);
                String strAttName = singleUrlObj.subStringAfter('target="_blank">').subStringBefore('</a>');
                System.debug('strAttName:::::::::::'+strAttName);
                if(!nameToUrlMap.containsKey(strAttName)){
                  //  if(strAttUrl.startsWith('https://')){
                        nameToUrlMap.put(strAttName,strAttUrl);
                   // }
                }
            }
           
        }
        for(String strAttName : nameToUrlMap.keySet()){
          /* PageReference pageRef = new PageReference(nameToUrlMap.get(strAttName));
            Attachment att = new Attachment();
            if(Test.isRunningTest()) { 
                att.Body = blob.valueOf('Unit.Test');
            } else {
                att.Body = pageRef.getContentAsPDF();
            }
            System.debug('nameToUrlMap.get(strAttName):::'+nameToUrlMap.get(strAttName));
            // att.Body = blob.valueOf(nameToUrlMap.get(strAttName));
            att.Name = strAttName + '.pdf';
            System.debug('att blob = '+ att);  */

            /* Blob pdfBlob ;
            HttpRequest req = new HttpRequest(); 
            req.setEndpoint(nameToUrlMap.get(strAttName));
            //req.setHeader('Accept', 'application/json');
            req.setMethod('GET');
            //req.setHeader('Authorization','Bearer' + accessToken);
            req.setTimeout(120000);
            HttpResponse response = new Http().send(req);
            //System.debug('Header='+ response.getHeaderKeys());
            System.debug('Status Code = ' + response.getStatusCode());
            System.debug('Pdf Body = ' + response.getBodyAsBlob());
            if(response.getStatusCode() == 200 && response.getBodyAsBlob() != null) {
                pdfBlob =  response.getBodyAsBlob();
                Attachment attach = new Attachment();
                attach.contentType = 'application/pdf';
                attach.name = strAttName + '.pdf';
                System.debug('attach.name'+attach.name);
                attach.body = pdfBlob;

                attachmentList.add(attach);
                System.debug('attachmentList:::::::::::'+attachmentList);
            }*/
            HttpRequest req = new HttpRequest();
            Blob pdfBlob;
            if(nameToUrlMap.get(strAttName).contains('GetFile')){
                System.debug('IN GetFile  ' );
                String FileId = nameToUrlMap.get(strAttName).subStringAfter('GetFile?id=');
                system.debug('FileId-'+FileId);
                Office365RestService.ResponseWrapper responseWrap = new Office365RestService.ResponseWrapper();
                responseWrap  = Office365RestService.downloadFromOffice365ByIdWhatsapp(FileId,'','','','','');
                System.debug('responseWrap.downloadUrl' + responseWrap.downloadUrl);
                req.setEndpoint(responseWrap.downloadUrl);

            }else{
                req.setEndpoint(nameToUrlMap.get(strAttName));
            }
            req.setHeader('Accept', 'application/json');
            req.setMethod('GET');
            //req.setHeader('Authorization','Bearer' + accessToken);
            req.setTimeout(120000);
            HttpResponse response = new Http().send(req);
            //System.debug('Header='+ response.getHeaderKeys());
            System.debug('Status Code = ' + response.getStatusCode());
            System.debug('Pdf Body = ' + response.getBodyAsBlob());
            if(response.getStatusCode() == 200) {
                pdfBlob =  response.getBodyAsBlob();
                Attachment attach = new Attachment();
                attach.contentType = 'application/pdf';
               // System.debug('attach.name'+urlNameMap.get(responseUrl));
                attach.name = strAttName+'.pdf';
                System.debug('attach.name'+attach.name);
                attach.body = pdfBlob;
                attachmentList.add(attach);  
            }
        }
         
       return attachmentList;
    }
    
  /* List<Attachment> getAttachments(String invoiceUrl) {
        System.debug('getAttachments= ' + invoiceUrl);
        String strAttUrl = invoiceUrl.subStringAfter('<a href="').subStringBefore('" target=');
        String strAttName = invoiceUrl.subStringAfter('target="_blank">').subStringBefore('</a></li>');

        List<Attachment> lstAttachs = new List<Attachment>();
        //Attachment att
        //lstAttachs.add(createAttachments());

        System.debug('strAttUrl= ' + strAttUrl);
        System.debug('strAttName= ' + strAttName);

        PageReference pageRef = new PageReference(strAttUrl);
        Attachment att = new Attachment();
        att.Body = pageRef.getContentAsPDF();
        att.Name = strAttName + '.pdf';
        System.debug('att blob = '+ att);  
        
        return new List<Attachment>{ att };
       // return null;
    }*/
}