/*
Description:  Rest Response Class for WHATSAPP CLIENT to read data from WHATSAPP
Developed By: Venkata Subhash K
*/


@RestResource(urlMapping='/nexmohook/*')
global without sharing class NEXMO_WA_REST_SERVICES {
    
    @HttpPost
    global static void doPost(){ 
        System.debug ('::::: BODY :::::'+RestContext.request.requestBody.toString() );
        System.debug ('::::: URI :::::'+RestContext.request.requestURI);
        System.debug ('::::: Path ::::'+RestContext.request.resourcePath);
        
        System.debug (':::: Headers ::::: '+ RestContext.request.headers);
        try {
            if (RestContext.request.headers != null) {
                for (String header : RestContext.request.headers.keySet ()) {
                    System.debug ('::: Header ::::'+header+' ::::: '+RestContext.request.headers.get (header));
                }
            }
        } catch(Exception e) {
            System.debug ('::: Exception ::::'+e.getMessage()+'::::'+e.getLineNumber());            
        }
        System.debug (':::: Params ::::: '+ RestContext.request.Params);
        try {
            if (RestContext.request.Params != null) {
                for (String param : RestContext.request.Params.keySet ()) {
                    System.debug ('::: Param ::::'+param+' ::::: '+RestContext.request.Params.get (param));
                }
            }
        } catch(Exception e) {
            System.debug ('::: Exception ::::'+e.getMessage()+'::::'+e.getLineNumber());            
        }
        
        
        try{      
            String jsonStr = RestContext.request.requestBody.toString();
            System.debug (jsonStr);
            if(jsonStr != ''){
                if( RestContext.request.requestURI == '/nexmohook/status' ){                
                    NEXMO_RESPONSE_PARSER parser= NEXMO_RESPONSE_PARSER.parse(jsonStr);
                    if(parser.message_uuid != ''){
                        updateMessagestatus(parser.message_uuid,parser);
                    }
                }
                if( RestContext.request.requestURI == '/nexmohook/inbound' ){    
                    jsonStr = jsonStr.replace ('to', 'WA_to').replace ('from', 'WA_from').replace ('number', 'WA_number').replace('long', 'WA_long');
                    NEXMO_INB_RESPONSE_PARSER obj = NEXMO_INB_RESPONSE_PARSER.parse (jsonStr);
                    if (obj.message_uuid != '') {
                        tagOrCreateInboundMessage(obj.message_uuid, obj);
                    }
            
                }
            }
            
        } catch(exception e){
            System.debug(e.getmessage()+'-'+e.getLineNumber());
        } 
    }
    public static void updateMessagestatus(string Messageid,NEXMO_RESPONSE_PARSER parser){    
        Nexmo_Whats_App_Message__c msg = [select id,Nexmo_Whats_App_Request__r.Record_Id__c  from Nexmo_Whats_App_Message__c where Nexmo_Message_uuid__c=:Messageid LIMIT 1];                
        if(parser.status == 'submitted'){
            msg.Message_Submitted__c = true;
        }        
        if(parser.status == 'delivered'){
            msg.Message_Delivered__c = true;
        }
        if(parser.status == 'read'){
            msg.Message_Read__c = true;
        }
        if(parser.status != 'submitted' && parser.status != 'delivered' && parser.status != 'read' ){
            msg.Message_Failed__c = true;
        }
        update msg;
    }
    
    public static void tagOrCreateInboundMessage(string Messageid, NEXMO_INB_RESPONSE_PARSER parser){
        long fromNum;
        if(!test.isrunningtest()){
            fromNum = long.valueof(parser.WA_from.WA_number);
        }else{
            fromNum = 1234567890;
        }
        list<Nexmo_Whats_App_Request__c> matchingParent = new list<Nexmo_Whats_App_Request__c>();
        matchingParent = [select id,lastmodifiedbyid,ownerid, Owner.IsActive from Nexmo_Whats_App_Request__c where To_number__c=:fromNum ORDER BY lastmodifieddate desc  LIMIT 1];
        Inquiry__c matchedInquiry = findMatchingInquiry(fromNum,parser.message.content.text);
        Id templateId =  [select id, name from EmailTemplate where developername = 'Nexmo_Email_Notification'].id; 


        if(matchingParent.size() > 0){
            List<Nexmo_Whats_App_Message__c> existingWAMessages = new List<Nexmo_Whats_App_Message__c>();
            if(matchedInquiry != null && matchedInquiry.Id != null){
                existingWAMessages = [SELECT Name FROM Nexmo_Whats_App_Message__c WHERE Nexmo_Whats_App_Request__r.Id =: matchingParent[0].id];
            }
            
            Nexmo_Whats_App_Message__c newInbMsg = new Nexmo_Whats_App_Message__c();
            newInbMsg.Nexmo_Whats_App_Request__c = matchingParent[0].id;
            newInbMsg.Direction__c = 'Inbound';
            newInbMsg.Nexmo_Message_Body__c = parser.message.content.text;
            newInbMsg.Message_type__c = parser.message.content.type;
            if (parser.message.content.type == 'image') {
                newInbMsg.message_body_url__c = parser.message.content.image.url;
                newInbMsg.Nexmo_Message_Body__c = parser.message.content.image.caption;
            }
            if (parser.message.content.type == 'file') {
                newInbMsg.message_body_url__c = parser.message.content.file.url;
                newInbMsg.Nexmo_Message_Body__c = parser.message.content.file.caption;
            }
            if (parser.message.content.type == 'audio') {
                newInbMsg.message_body_url__c = parser.message.content.audio.url;
                newInbMsg.Nexmo_Message_Body__c = parser.message.content.audio.caption;
            }
            if (parser.message.content.type == 'video') {
                newInbMsg.message_body_url__c = parser.message.content.video.url;
                newInbMsg.Nexmo_Message_Body__c = parser.message.content.video.caption;
            }
            if (parser.message.content.type == 'location') {
                newInbMsg.message_body_lat__c = parser.message.content.location.lat;
                newInbMsg.message_body_lng__c = parser.message.content.location.WA_long;
            }
            newInbMsg.Nexmo_Message_uuid__c = parser.message_uuid;
            //newInbMsg.ownerid = matchingParent[0].lastmodifiedbyid;        
            newInbMsg.ownerid = matchingParent[0].ownerid; 
            System.debug('came here');
            if(existingWAMessages != null && !existingWAMessages.isEmpty()){
                Boolean isActive = true;
                String ownerId = matchedInquiry.OwnerId;
                if(ownerId.startsWith('00G')){
                    Group matchQueue = [SELECT Name,Type,email FROM Group WHERE  id =: matchedInquiry.OwnerId];
                    if(matchQueue.type == 'Queue'){
                        isActive = false;
                    } 
                }else if(matchedInquiry.Owner.isActive == false){
                    isActive = false;   
                }
                if(isActive == false){
                    if(matchedInquiry.Duplicate__c == false){
                        Group assignQueue = [SELECT Name,email FROM Group WHERE  Name = 'Lead Service'];
                        if(assignQueue.Email != null && assignQueue.Email != null){
                            sendEmailNotification(templateId,null,matchedInquiry.Id,assignQueue.Email);
                        }
                        newInbMsg.ownerid = assignQueue.Id; 
                        System.debug('Entered for existing is active false and is dup flase');
                    }else{
                        Inquiry__c parentInq = [SELECT Name,OwnerId,Owner.Email FROM Inquiry__c WHERE ID =: matchedInquiry.Related_Inquiry__c ];
                        newInbMsg.ownerid =  parentInq.OwnerId;
                        System.debug('Entered for existing is active false and is dup true');
                          if(parentInq.Owner.Email != null){
                              sendEmailNotification(templateId,null,parentInq.Id,parentInq.Owner.Email);
                          }
                    }
                }else{
                    newInbMsg.ownerid =  matchedInquiry.OwnerId;                   
                    System.debug('Entered for existing is active true');
                }
            }else{
                if(matchedInquiry.Duplicate__c == false){
                    newInbMsg.ownerid =  matchedInquiry.OwnerId;
                    if(matchedInquiry.Owner.Email != null)
                        sendEmailNotification(templateId,null,matchedInquiry.Id,matchedInquiry.Owner.Email);
                         System.debug('Entered for existing nd new conversation and is dup flase');
               }else{
                    Inquiry__c parentInq = [SELECT Name,OwnerId,Owner.Email FROM Inquiry__c WHERE ID =: matchedInquiry.Related_Inquiry__c ];
                    newInbMsg.ownerid =  parentInq.OwnerId;
                    System.debug('Entered for existing and new conversation and is dup true');
                    if(parentInq.Owner.Email != null)
                        sendEmailNotification(templateId,null,parentInq.Id,parentInq.Owner.Email);
                } 
            }
            
            insert newInbMsg;
            System.debug('came here'+newInbMsg.Id);
            matchingParent[0].Latest_Inbound_Message_Received_on__c = system.now();
            update matchingParent;
        } else {
            boolean isStand = findorCreateMatchingStandInquiry(fromNum,parser.message.content.text);
            
            Nexmo_Whats_App_Request__c request = new Nexmo_Whats_App_Request__c();        
            request.To_number__c = fromNum;
            request.Unique_Key__c = string.valueof(fromNum);
            request.ownerid = userinfo.getuserid();
            request.Is_incoming_from_New_Number__c = true; 
            // Added by Srikanth to assign the owner using round robin
            try {
                request.ownerId = NEXMO_WA_OwnerAssignment.assignOwner ();       
            } catch (Exception e) {}
            
            request.Create_Lead__c = false; 
            if(label.Nexmo_Create_Leads == 'Yes'){
                request.Create_Lead__c = true; 
            }
            
            if(isStand==true){
                request.Source__c = 'Stands';
                request.Source_Reference__c = getCampaignIdForStandInquiry(parser.message.content.text.SubStringBefore('|').deleteWhitespace());
            }
            
            upsert request Unique_Key__c;
            
            Nexmo_Whats_App_Request__c insrtedReq = [select id,Name,ownerid from Nexmo_Whats_App_Request__c where id=:request.id LIMIT 1];
            insrtedReq.Record_Id__c = insrtedReq.id;
            insrtedReq.Record_Name__c = insrtedReq.Name;
            update insrtedReq;
            
            // Insert Child
            Nexmo_Whats_App_Message__c message = new Nexmo_Whats_App_Message__c();
            message.Nexmo_Whats_App_Request__c = request.id;
            message.direction__c = 'Inbound';
            message.Nexmo_Message_uuid__c = parser.message_uuid;
            message.Nexmo_Message_Body__c = parser.message.content.text;
            
            if (parser.message.content.type == 'image') {
                message.message_body_url__c = parser.message.content.image.url;
                message.Nexmo_Message_Body__c = parser.message.content.image.caption;
            }
            if (parser.message.content.type == 'location') {
                message.message_body_lat__c = parser.message.content.location.lat;
                message.message_body_lng__c = parser.message.content.location.WA_long;
            }
            
            if (parser.message.content.type == 'audio') {
                message.message_body_url__c = parser.message.content.audio.url;
                message.Nexmo_Message_Body__c = parser.message.content.audio.caption;
            }
            if (parser.message.content.type == 'video') {
                message.message_body_url__c = parser.message.content.video.url;
                message.Nexmo_Message_Body__c = parser.message.content.video.caption;
            }
            if (parser.message.content.type == 'file') {
                message.message_body_url__c = parser.message.content.file.url;
                message.Nexmo_Message_Body__c = parser.message.content.file.caption;
            }
            message.Message_type__c = parser.message.content.type;
            message.Nexmo_Message_Request_Body__c = string.valueof(parser);
            message.ownerid = insrtedReq.ownerid;
            insert message;
        }
        
    }
    
    public static boolean findorCreateMatchingStandInquiry( long CustomerWhatsAppNumber,string message ){    
        boolean isFoundinStand = false;    
        system.debug(message);
        if(test.isRunningtest()){
            message = 'IQF-240948 |';
        }
        string standInqNum = message.SubStringBefore('|').deleteWhitespace();
        list<stand_inquiry__c> standInq = new list<stand_inquiry__c>();
        String searchQuery = 'FIND \'' + standInqNum + '\' IN ALL FIELDS RETURNING  stand_inquiry__c(Id,Name,Mobile_Phone_Encrypt__c,NSUUID__c)';
        
        List<List <sObject>> searchList = search.query(searchQuery);
        system.debug(searchList);
        standInq = ((List<stand_inquiry__c>)searchList[0]);
        if(standInq.size() > 0){
            standInq[0].Whats_App_Initiated__c = true;
            update standInq[0];
            isFoundinStand = true;
        }
        return isFoundinStand;
    }
    
    public static Inquiry__c findMatchingInquiry ( long CustomerWhatsAppNumber,string message ){
        string standInqNum = message.SubStringBefore('|').deleteWhitespace();
        list<Inquiry__c> existingInqs = new list<Inquiry__c>();
        String searchQuery = 'FIND \'' + standInqNum + '\' IN ALL FIELDS RETURNING  Inquiry__c (Id,Name,OwnerId,Duplicate__c,Related_Inquiry__c,Owner.isActive,Owner.Email)';
        
        List<List <sObject>> searchList = search.query(searchQuery);
        system.debug(searchList);
        existingInqs = ((List<Inquiry__c>)searchList[0]);
        
        return existingInqs[0];
    
    }
    
    public static string getCampaignIdForStandInquiry(string standInquiryName){
        stand_inquiry__c st = new stand_inquiry__c();
        try {
            st = [select id,Name, Campaign__c from stand_inquiry__c where Name=:standInquiryName];
            if(st != null){
                return string.valueof(st.Campaign__c);
            }else{
                return '';
            }
        }
        catch (Exception e) {
            return '';
        }
        
    }
     
    public static void sendEmailNotification(Id templateId,Id ownerId,Id objId,String toAddresses){
        Messaging.SingleEmailMessage mail = Messaging.renderStoredEmailTemplate(templateId, ownerId, objId);
            mail.setToAddresses(new List<String>{toAddresses});
            mail.setSenderDisplayName('Damac Nexmo ');
            mail.setReplyTo('noreply@Damacgroup.com');
            mail.setTemplateId(templateId);
            mail.setSaveAsActivity(false);

        Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{mail});
    }

}