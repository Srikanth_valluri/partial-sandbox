/**
 * @File Name          : ViolationNoticeEmailSendInvocableTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 9/26/2019, 4:44:10 PM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    9/26/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
private class ViolationNoticeEmailSendInvocableTest {
@isTest static void test_sendViolationNoticeEmail() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', ProfileId = p.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testdmacorg.com');
            System.debug('=templateObj===='+[SELECT count() from EmailTemplate]);
            System.debug('=templateObj===='+[SELECT count() from EmailTemplate WHERE DeveloperName =: Label.ViolationNoticeEmailTemplateName]);
            // EmailTemplate et = new EmailTemplate();
            // et.isActive = true;
            // et.Name = 'testTemplate';
            // et.DeveloperName = Label.ViolationNoticeEmailTemplateName;
            // et.TemplateType = 'text';
            // et.FolderId = UserInfo.getUserId();
            // et.Body = '';
            //
            // insert et;
            // EmailTemplate templateObj = new EmailTemplate (developerName = Label.ViolationNoticeEmailTemplateName,
            //   FolderId = UserInfo.getUserId(),TemplateType= 'Text', Name = Label.ViolationNoticeEmailTemplateName);
            // insert templateObj;
            // System.debug('=templateObj===='+templateObj);
            // EmailTemplate objTemplateId = [SELECT ID
            //                             FROM EmailTemplate
            //                             WHERE DeveloperName =: Label.ViolationNoticeEmailTemplateName];
            // System.debug('=objTemplateId===='+objTemplateId);
        List<FM_Case__c> lstFMCases = new List<FM_Case__c>();
        List<Id> caseIdList = new List<Id>();
        System.runAs(u) {
        Contact con = new Contact(LastName = 'TEst');
        insert con;
        
        // Start 
            Property__c objProperty = new Property__c();
            objProperty.Name = 'Test Project';
            objProperty.Property_Name__c = 'Test Property';
            objProperty.Property_ID__c = 3431;
            objProperty.CurrencyIsoCode = 'AED';
            insert objProperty;
    
            Account objAcc = new Account();
            objAcc = TestDataFactory_CRM.createPersonAccount();
            insert objAcc;
            
            NSIBPM__Service_Request__c objServReq = TestDataFactory_CRM.createServiceRequest();
            insert objServReq ;
        
            List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objServReq.Id,1);
            insert lstBooking ;
            
            List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
            insert lstActiveStatus ;
    
            List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking ,3);
            for( Booking_Unit__c objUnit : lstBookingUnit  ) {
                objUnit.Resident__c = objAcc.Id ;
                objUnit.Handover_Flag__c = 'Y' ;
                objUnit.Unit_Name__c = 'DSR/79/7903';
            }
            lstBookingUnit[2].Owner__c = objAcc.Id ;
            insert lstBookingUnit ;
    
            Location__c objLoc = new Location__c();
            objLoc = TestDataFactoryFM.createLocation();
            Id recTypeIdLoc=Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
            objLoc.RecordTypeId = recTypeIdLoc;
            objLoc.Name = 'DSR';
            objLoc.Property_Name__c = objProperty.id;
            objLoc.Loams_Email__c = 'test@test.com';
            insert objLoc;
        // End
        
        Id VIOLATION_NOTICE_RECORD_TYPE_ID = DamacUtility.getRecordTypeId('FM_Case__c', 'Violation Notice');
        FM_Case__c obj1 = new FM_Case__c();
        obj1.Email__c = 'tenant@gmail.com';
        obj1.Tenant_Email__c = 'owner@gmail.com';
        obj1.recordtypeid = VIOLATION_NOTICE_RECORD_TYPE_ID;
        obj1.Booking_Unit__c=lstBookingUnit[0].id;
        obj1.Notice_Type__c = 'First Notice';
        lstFMCases.add(obj1);        
        FM_Case__c obj2 = new FM_Case__c();
        obj2.Tenant_Email__c = 'owner@gmail.com';
        obj2.recordtypeid = VIOLATION_NOTICE_RECORD_TYPE_ID;
        obj2.Approval_Status__c = 'Approved';
        //lstFMCases.add(obj2);        
        insert lstFMCases;
            for(fm_case__c obj: lstFMCases){
                caseIdList.add(obj.id);
            }
        }
        Document objDoc = new Document();
        objDoc.id = Label.Violation_List_document_Id;
        objDoc.Body = Blob.valueOf('test body');
        upsert objDoc;
        
        SR_Attachments__c objCustAttach1 = new SR_Attachments__c();
        objCustAttach1.FM_Case__c = lstFMCases[0].Id ;
        objCustAttach1.Name = 'attach 1';
        objCustAttach1.Attachment_URL__c  = 'wwwdamacpropertiescom';
        insert objCustAttach1 ;
        SR_Attachments__c objCustAttach2 = new SR_Attachments__c();
        objCustAttach2.FM_Case__c = lstFMCases[0].Id ;
        objCustAttach2.Name = 'attach 2';
        objCustAttach2.Attachment_URL__c  = 'wwwdamacpropertiescom';
        insert objCustAttach2 ;
        
        SR_Attachments__c objCustAttach3 = new SR_Attachments__c();
        objCustAttach3.FM_Case__c = lstFMCases[0].Id ;
        objCustAttach3.Name = 'attach 2';
        objCustAttach3.Attachment_URL__c  = 'wwwdamacpropertiescom';
        insert objCustAttach3;
        
        Blob b = blob.valueof('Unit.test');
        List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        efa.setFileName('FileName');
        efa.setBody(b);
    
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTemplateId('00X7F000001Jsnb');
        mail.setTargetObjectId('0057F000000r5pG');
        mail.setSaveAsActivity(false);
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] { efa });
        emailList.add(mail);
    
        Test.startTest();
       // Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
		UploadMultipleDocController.strLabelValue = 'N';
        SOAPCalloutServiceMock.returnToMe = new Map<String,MultipleDocUploadService.DocumentAttachmentMultipleResponse_element>();
        MultipleDocUploadService.DocumentAttachmentMultipleResponse_element response_x = new MultipleDocUploadService.DocumentAttachmentMultipleResponse_element();
        response_x.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS]Files : [IPMS-12345--Test1.pdf Processed] PK Value#:2-2-005058 Additionalfile","PARAM_ID":"IPMS-12345-Test1.pdf","URL":"https://sftest.deeprootsurface.com/docs/t/IPMS-12345-Test1.pdf"}],"message":"Process Completed Returning 2 Response Message(s)...","status":"S"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
		
        ViolationNoticeEmailSendInvocable.initiateEmailAndSMS(caseIdList);
        Test.stopTest();
    }


}