/************************************************************************************************
 * @Name              : DAMAC_DeleteBuyerDetails_API_Test
 * @Description       : Test Class for DAMAC_DeleteBuyerDetails_API
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         23/07/2020       Created
***********************************************************************************************/

@isTest
public class DAMAC_DeleteBuyerDetails_API_Test{

@isTest
    static void testAPI(){
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.Mode_of_Payment__c = 'Cheque';
        insert sr;
        
        Booking__c booking = new Booking__c();
        booking.Deal_SR__c = sr.Id;
        insert booking;
        
        Buyer__c buyer = new Buyer__c();
        buyer.Booking__c = booking.Id;
        buyer.First_Name__c = 'John';
        buyer.Last_Name__c = 'Wayne';
        insert buyer;
        String buyerId = buyer.Id;
        String buyerIdNull = '';
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();     
        
        req.requestUri = '/deleteBuyerDetails';
        req.requestBody = Blob.valueOf('{"buyerId" : "'+buyerId+'"}');
        req.httpMethod = 'POST';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_DeleteBuyerDetails_API.getResults();
        
        req.requestUri = '/deleteBuyerDetails';
        req.requestBody = Blob.valueOf('{"buyerId" : "'+buyerIdNull+'"}');
        req.httpMethod = 'POST';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_DeleteBuyerDetails_API.getResults();
        
        Test.stopTest();
    
    }
}