global class SendGridResponseMock implements HttpCalloutMock {
  global HTTPResponse respond(HTTPRequest req) {
      system.debug('*****req'+req);
    HttpResponse res = new HttpResponse();
    res.setHeader('Content-Type', 'application/json');
    res.setStatus('Accepted');
    res.setStatusCode(202);
    return res;
  }
}