/**************************************************************************************************
* Name               : ResetSalesAudit 
                                                 
**************************************************************************************************/
global without sharing class ResetSalesAudit implements NSIBPM.CustomCodeExecutable{
    public String EvaluateCustomCode(NSIBPM__Service_Request__c SR, NSIBPM__Step__c step) {
        String sSalesAuditFlag='';
        String stepSt='';
       
        String retStr = 'Success';
        Id sSRId=step.Parent_SR__c;
        
        System.Debug('Step Aayo'+step.Parent_SR__c);
        
        List<Id> BookingIds= new List<id>();
        try{
            for(Booking__c Bookings :[select id from Booking__c where Deal_SR__c=: step.Parent_SR__c]){
                BookingIds.add(Bookings.id);
            }
            if(BookingIds.size()>0){
                system.debug('#### invoking CC_AgentRegUpdate');
                system.enqueueJob(new AsyncSalesAuditUpd(BookingIds,'SalesAuditUpdate',sSalesAuditFlag));
            }
        
        } catch (Exception e) {
            retStr = 'Error :' + e.getMessage() + '';
        }
        return retStr;
    }
}