public class GenerateAirwayBillController {
    public String airwayBillNumber {get; set;}
    Id courierId;
    Courier_Delivery__c ar;
    public GenerateAirwayBillController(ApexPages.StandardController stdController) {
        courierId = stdController.getId();
    }
    
    public pageReference uploadAirwayBill(){
        list<Courier_Delivery__c> updateARCList = new list<Courier_Delivery__c>();
        list<SR_Attachments__c> insertSrAttachment = new list<SR_Attachments__c>(); 
        List < UploadMultipleDocController.MultipleDocRequest > lstMultipleDocReq = new List < UploadMultipleDocController.MultipleDocRequest > ();
        UploadMultipleDocController.data respObj;
        
        if(String.isNotBlank(courierId)){
            ar = [Select Id
                       , Airway_Bill__c
                       , Airway_PDF__c 
                       , Courier_Service__c
                       , Country__c
                    From Courier_Delivery__c 
                   Where id=:courierId];
        } else if(String.isNotBlank(airwayBillNumber)){
            ar = [Select Id
                       , Airway_Bill__c 
                       , Courier_Service__c
                       , Country__c
                    From Courier_Delivery__c 
                   Where Airway_Bill__c =:airwayBillNumber];   
        }
        
        if(ar == null){
            return null;    
        }
        
        UploadMultipleDocController.MultipleDocRequest reqObjAramex = new UploadMultipleDocController.MultipleDocRequest();
            Blob pdfBlob;
            if(ar.Courier_Service__c == 'First Flight'){
                pdfBlob = FirstFlightService.getAirwayBillInPDF(ar.Airway_Bill__c, ar.Country__c);    
            }else if(ar.Courier_Service__c == 'Aramex' || ar.Courier_Service__c == 'Aramex CDS'){
                pdfBlob = AramexService.getAirWayBillBody(ar.Airway_PDF__c);    
            }
            if(pdfBlob != null){
                reqObjAramex.base64Binary = EncodingUtil.base64Encode(pdfBlob);
                reqObjAramex.category = 'Document';
                reqObjAramex.entityName = ar.Courier_Service__c+'Shippment Detail';
                reqObjAramex.fileDescription = ar.Courier_Service__c+'Shippment Detail';
                reqObjAramex.fileId = ar.Id + String.valueOf(System.currentTimeMillis())+'.'+'pdf';
                reqObjAramex.fileName = ar.Id + String.valueOf(System.currentTimeMillis())+'.'+'pdf';
                
                reqObjAramex.registrationId = ar.Id;
                reqObjAramex.sourceFileName = ar.Id + String.valueOf(System.currentTimeMillis())+'.'+'pdf';
                reqObjAramex.sourceId = ar.Id + String.valueOf(System.currentTimeMillis())+'.'+'pdf';
                lstMultipleDocReq.add(reqObjAramex);
            
                respObj = new UploadMultipleDocController.data();
               //if( !Test.isRunningTest() ) { 
                respObj = UploadMultipleDocController.getMultipleDocUrl(lstMultipleDocReq);
                System.debug('===>respObj.data'+respObj.data);
               //}
            }
        
        if(respObj != null && respObj.data !=null){
            for (UploadMultipleDocController.MultipleDocResponse objData2: respObj.data) {
                system.debug('***objData.PARAM_ID : '+objData2.PARAM_ID);
                system.debug('objData.url : '+objData2.url);
                if (objData2.PROC_STATUS == 'S' && String.isNotBlank(objData2.url)) {
                    ar.Airway_PDF__c = objData2.url;
                    SR_Attachments__c objCourierAttachment = new SR_Attachments__c();
                    objCourierAttachment.Name = ar.Courier_Service__c+'Airway Bill';
                    objCourierAttachment.Type__c = 'Airway Bill';
                    objCourierAttachment.Attachment_URL__c = objData2.url;
                    //if(String.isNotBlank(courierId){
                        //objCourierAttachment.Courier_Details__c = ar.Id;
                    //}
                    insertSrAttachment.add(objCourierAttachment);
                    updateARCList.add(ar);
                    system.debug('objData.url : '+objData2.url);
                } 
            }
        }
        
        String strUrl = '';
        if(String.isNotBlank(courierId)){
            if(updateARCList.size()>0){
                update updateARCList;
            } 
            if(insertSrAttachment.size()>0){
                insert insertSrAttachment;
                strUrl = insertSrAttachment[0].Attachment_URL__c;
            }
            
        } else if(String.isNotBlank(airwayBillNumber)){
            if(insertSrAttachment.size()>0){
                strUrl = insertSrAttachment[0].Attachment_URL__c; 
            }
        }
        return redirectToCourier(strUrl);
    }
    
    /* Method to redirect on case
     * Input Parameters : No parameters
     * Return Type : pageReference 
     */
    public pageReference redirectToCourier(String strUrl){
        PageReference pg = new PageReference(strUrl);
        pg.setRedirect(true);
        return pg;    
    }
}