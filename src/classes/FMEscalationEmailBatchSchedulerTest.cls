@isTest
private class FMEscalationEmailBatchSchedulerTest {

    @isTest static void test_batch() {
        List<FM_Case__c> lstFMCases = new List<FM_Case__c>();
        Id RecType = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Registration').getRecordTypeId();
        for(Integer i=0;i<5;i++) {
            lstFMCases.add(new FM_Case__c(Status__c='Submitted',Escalation_Date__c=Date.today(),
                    FM_Director_Email__c = 'test'+i+'@gmail.com', RecordTypeId=RecType));
        }
        Id RecTypePOP = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Proof Of Payment').getRecordTypeId();
        for(Integer i=0;i<5;i++) {
            lstFMCases.add(new FM_Case__c(Status__c='Submitted',Escalation_Date__c=Date.today(),
                    FM_Director_Email__c = 'test'+i+'@gmail.com', RecordTypeId=RecTypePOP ));
        }
        insert lstFMCases;
        Test.startTest();
        new FMEscalationEmailBatchScheduler().scheduleMyJob();
        new FM_EmailMessageBatchScheduler().scheduleMyJob();
        Test.stopTest();
    }

}