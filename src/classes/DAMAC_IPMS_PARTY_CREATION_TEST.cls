/************************************************************************************************
 * @Name              : DAMAC_IPMS_PARTY_CREATION_TEST
 * @Description       : Test Class for DAMAC_IPMS_PARTY_CREATION, IPMS_PartyId_Creation_Response, DAMAC_IPMS_BOOKING_JSON,
 *                       DAMAC_IPMS_BOOKING_UPDATE_JSON, DAMAC_IPMS_PARTY_CREATION_JSON ,
 *                       DAMAC_IPMS_PARTY_CREATION_RESPONSE
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log
 * 1.0         QBurst         07/04/2020       Created
***********************************************************************************************/
@istest
public with sharing class DAMAC_IPMS_PARTY_CREATION_TEST {
    public static testmethod void testMethod1(){
        Account acc = new Account ();
        acc.LastName = 'test';
        acc.Agency_Type__c = 'Corporate';
        acc.Vendor_ID__c = '767676';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        insert acc;

        Agent_Site__c agency = new Agent_Site__c();
        agency.Name = 'UAE';
        agency.Agency__c = acc.id;
        insert agency;

        NSIBPM__SR_Template__c srTemplate = InitializeSRDataTest.createSRTemplate('Deal');
        insert srTemplate;
        
        NSIBPM__SR_Status__c srStatus = new NSIBPM__SR_Status__c();
        srStatus.NSIBPM__Code__c = 'SUBMITTED';
        insert srStatus;

        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);
        sr.Eligible_to_Sell_in_Dubai__c = true;
        sr.Agency_Type__c = 'Individual';
        sr.ID_Type__c = 'Passport';
        sr.Token_Amount_AED__c = 40000;
        sr.RecordTypeId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Change Agent').getRecordTypeId();
        sr.Booking_Wizard_Level__c = null;
        sr.Agency_Email_2__c = 'test2@gmail.com';
        sr.Alternate_Agency_Email_2__c = 'test1@gmail.com';
        sr.Country_of_Sale__c = 'UAE';
        sr.Mode_of_Payment__c = 'Online_Payment';
        sr.agency__c = acc.id;
        insert sr;

        NSIBPM__Step_Template__c stpTemplate =  InitializeSRDataTest.createStepTemplate('Deal','MANAGER_APPROVAL');
        insert stptemplate;

        List<string> statuses = new list<string>{'UNDER_MANAGER_REVIEW'};
        Map<string,NSIBPM__Status__c> stepStatuses = InitializeSRDataTest.createStepStatus(statuses);
        NSIBPM__Step__c stp = InitializeSRDataTest.createStep(sr.id,stepStatuses.values()[0].id,stptemplate.id);
        //insert stp;

        Campaign__c camp = new Campaign__c();
        camp.End_Date__c = system.today().addmonths(10);
        camp.Marketing_End_Date__c = system.today().addmonths(10);
        camp.Marketing_Start_Date__c = system.today().addmonths(-10);
        camp.Start_Date__c =  system.today().addmonths(-10);
        insert camp;

        Inquiry__c inq = new Inquiry__c ();
        inq.Activity_Counter__c =101;
        inq.Inquiry_Status__c='Active';
        inq.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        inq.campaign__c = camp.id;
        inq.ownerId = userinfo.getUserId();
        inq.Promoter_Name__c = 'Test';
        inq.Telesales_Executive__c = UserInfo.getUserId();
        insert inq;

        List<Booking__c> lstbk = new List<Booking__c>();
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        lstbk[0].Inquiry_Account_Id__c = inq.id;
        insert lstbk;

        Location__c loc = InitializeSRDataTest.createLocation('123','Building');
        loc.property_id__c = '123';
        insert loc;

        Property__c newProperty = new Property__c();
        newProperty.Property_ID__c  = 1;
        newProperty.Property_Code__c    = 'VIR' ;
        newProperty.Property_Name__c    = 'VIRIDIS @ AKOYA OXYGEN' ;
        newProperty.District__c = 'AL YUFRAH 2' ;
        newProperty.AR_Transaction_Type__c  = 'INV VIR' ;
        newProperty.Penalty_Transaction_Type__c = 'DM-PENALTIES-VIR' ;
        newProperty.Brokerage_Distribution_Set__c   = '11600' ;
        newProperty.Sales_Commission_Dist_Set__c    = '11601' ;
        newProperty.Currency_Of_Sale__c = 'AED' ;
        newProperty.Signature_Col_Customer_Stmt__c  = 'Front Line Investment Management Co. LLC' ;
        newProperty.EOI_Enabled__c = true;
        insert newProperty;

        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Unit_Location__c = loc.id;
        lstInv[0].building_location__c = loc.id;
        lstInv[0].property_id__c = newProperty.id;
        insert lstInv;

        Id accountRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account a = new Account();
        a.LastName= 'Test Account';
        a.RecordTypeId = accountRTId;
        insert a;

        buyer__c buyer = new buyer__c();
        buyer.Buyer_Type__c =  'Individual';
        buyer.Address_Line_1__c =  'Ad1';
        buyer.Country__c =  'United Arab Emirates';
        buyer.City__c = 'Dubai' ;
        buyer.Account__c = acc.id;
        buyer.inquiry__c = inq.id;
        buyer.dob__c = system.today().addyears(-30);
        buyer.Email__c = 'test@test.com';
        buyer.First_Name__c = 'firstname' ;
        buyer.Last_Name__c =  'lastname';
        buyer.Nationality__c = 'Indian' ;
        buyer.Passport_Expiry__c = system.today().addyears(20) ;
        buyer.Passport_Number__c = 'J0565556' ;
        buyer.Phone__c = '569098767' ;
        buyer.Phone_Country_Code__c = 'United Arab Emirates: 00971' ;
        buyer.Place_of_Issue__c =  'India';
        buyer.Title__c = 'Mr';
        buyer.booking__c = lstbk[0].id;
        buyer.Primary_Buyer__c =true;
        buyer.Unique_Key__c = lstbk[0].id+''+inq.id;
        buyer.Arabic_Details_Changed__c = true;
        buyer.Contact_Changed__c = true;
        buyer.Address_Changed__c = true;
        buyer.Passport_Changed__c = true;
        buyer.Nationality_Changed__c  = true;
        buyer.party_id__c = '1331312';
        
        insert buyer;

        Currency_Rate__c rate = new Currency_Rate__c();
        rate.To_Currency__c = 'AED';
        rate.Conversion_Date__c = system.today();
        rate.conversion_rate__c = 1;
        rate.From_Currency__c = 'AED';
        insert rate;

        Inventory__c invent = new Inventory__c();
        invent.Inventory_ID__c = '1';
        invent.Building_ID__c = '1';
        invent.Floor_ID__c = '1';
        invent.Marketing_Name__c = 'Damac Heights';
        invent.Address_Id__c = '1' ;
        invent.EOI__C = NULL;
        invent.Tagged_to_EOI__c = false;
        invent.Is_Assigned__c = false;
        invent.Property_ID__c = newProperty.ID;
        invent.Property__c = newProperty.Id;
        invent.Status__c = 'Released';
        invent.special_price__c = 1000000;
        invent.CurrencyISOCode = 'AED';
        invent.Property_ID__c = '1234';
        invent.Floor_Package_ID__c = '';
        invent.building_location__c = loc.id;
        invent.property_id__c = newProperty.id;
        insert invent;

        Campaign__c c = new Campaign__c();
        c.Start_Date__c = system.today().addDays(-1);
        c.End_Date__c = system.today().addDays(1);
        c.Marketing_Start_Date__c = system.today().addDays(-1);
        c.Marketing_End_Date__c = system.today().addDays(1);
        c.RecordTypeID = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Roadshows').getRecordTypeId();   
        insert c; 

        Campaign_Inventory__c campInv = new Campaign_Inventory__c ();
        campInv.Inventory__c = invent.Id;
        campInv.Start_Date__c = Date.Today ();
        campInv.End_Date__c = Date.Today ();
        campInv.Campaign__c = c.Id;
        insert campInv;

        Payment_Plan__c pp = new Payment_Plan__c();
        pp.Effective_From__c = system.today();
        pp.Effective_To__c = system.today(); 
        pp.Building_Location__c =  loc.id;  
        pp.term_id__c = '1234';  
        insert pp;

        Available_options__c opt = new Available_options__c();
        opt.type_of_option__c = 'Scheme';
        opt.Promotion_Option__c = 'test';
        opt.discount_type__c = 'psf';
        opt.Price_Value__c = 1;
        opt.Percent_Value__c = 5;
        insert opt;

        selected_units__c sel = new selected_units__c();
        sel.service_request__c = sr.id;
        sel.Payment_Plan__c = pp.ID;
        sel.inventory__c = invent.id;
        sel.Requested_Price_AED__c = 1000000;
        sel.Selected_Campaign__c = opt.id;
        insert sel;

        Available_options__c opt1 = new Available_options__c();
        opt1.type_of_option__c = 'Option';
        opt1.Promotion_Option__c = 'test';
        opt1.discount_type__c = 'psf';
        opt1.Price_Value__c = 1;
        opt1.selected_units__c = sel.id;
        insert opt1;

        Payment_Terms__c pt = new Payment_Terms__c();
        pt.Payment_Plan__c = pp.id;
        pt.Percent_Value__c = '5';
        insert pt;

        DAMAC_Constants.skip_BookingUnitTrigger = true;
        List<Booking_Unit__c> BUList = new List<Booking_Unit__c>();
        Booking_Unit__c bu1 = new Booking_Unit__c();
        bu1.Booking__c = lstbk[0].id;
        bu1.Payment_Method__c = 'Cash';
        bu1.Primary_Buyer_s_Email__c = 'test@damac.com';
        bu1.Primary_Buyer_s_Name__c = 'testNSI';
        bu1.Primary_Buyer_s_Nationality__c = 'Russia';
        bu1.Inventory__c = invent.id;
        bu1.Registration_ID__c = '1234';
        bu1.Payment_Plan_Id__c = pp.id;
        BUList.add(bu1);

        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = lstbk[0].id;
        bu.Payment_Method__c = 'Cash';
        bu.Primary_Buyer_s_Email__c = 'test@damac.com';
        bu.Primary_Buyer_s_Name__c = 'testNSI';
        bu.Primary_Buyer_s_Nationality__c = 'Russia';
        bu.Inventory__c = invent.id;
        bu.Registration_ID__c = 'test123'; 
        bu.Payment_Plan_Id__c = pp.id;
        BUList.add(bu);
        insert BUList;
        test.starttest();
        DAMAC_IPMS_PARTY_CREATION.CreateParty('NEW_CUSTOMER', true, true, buyer.Id, true);
        buyer.Primary_Buyer__c = false;
        DAMAC_IPMS_PARTY_CREATION.CreateParty('NEW_CUSTOMER', true, true, buyer.Id, true);
        DAMAC_IPMS_BOOKING_JSON.parse('{"extRequestNumber" : "'+sr.id+'"}');
        DAMAC_IPMS_PARTY_CREATION_JSON.parse('{"extRequestNumber" : "'+sr.id+'"}');
        buyer.Passport_Expiry__c =  buyer.Passport_Expiry__c.addMonths(1);
        update buyer;
        DAMAC_IPMS_PARTY_CREATION.CreateParty('NEW_CUSTOMER', false, true, buyer.Id, true);
        String dateString = '';
        Date testDate =  buyer.Passport_Expiry__c.addMonths(1);
        dateString = DAMAC_IPMS_PARTY_CREATION.getDatetext(testDate);
        for(Integer i=0; i<10; i++){
            testDate = testDate.addMonths(1);
            dateString = DAMAC_IPMS_PARTY_CREATION.getDatetext(testDate);
        }
        for(Integer i=0; i<12; i++){
            testDate = testDate.addMonths(1);
            dateString = Damac_UKRegistration.getDatetext(testDate);
        }
        List<Id> bookingIdList = new List<Id>();
        bookingIdList.add(lstbk[0].id);
        DAMAC_IPMS_PARTY_CREATION.CreateParty('UPDATE_CUSTOMER', false, true, buyer.Id, true);
        DAMAC_IPMS_PARTY_CREATION.statusUpdate(bookingIdList, 'AG', null);
        DAMAC_IPMS_PARTY_CREATION.updateBookingUnitToIPMS(bookingIdList, 'UPDATE_AGENT_AUDIT', 'Y');
        DAMAC_IPMS_PARTY_CREATION.updateBookingUnitToIPMS(bookingIdList, 'UPDATE_DOC_OK', 'Y');
        DAMAC_IPMS_PARTY_CREATION.updateBookingUnitToIPMS(bookingIdList, 'GENERATE_CALL', 'Y');
        
        
        test.stoptest();
    }
    public static testmethod void testMethod2(){
        DAMAC_ContactLines_REQ obj = new DAMAC_ContactLines_REQ ();
        List<DAMAC_ContactLines_REQ.cls_contactLines> linesList = new List <DAMAC_ContactLines_REQ.cls_contactLines>();
        DAMAC_ContactLines_REQ.cls_contactLines lines = new DAMAC_ContactLines_REQ.cls_contactLines();
        lines.primaryFlag = 'Y';
        lines.contactType = 'MOBILE';
        lines.countryCode = '91';
        lines.areaCode = '80';
        lines.phoneNumber = '22312';
        lines.email = 'tes@test.com';
        lines.url = 'test.com';
        linesList.add (lines);
        obj.contactLines = linesList;
        obj.subRequestName = 'test';
        obj.propertyLocation = 'SP';
        obj.partyId = '34324131';
        
        
        
    
    }
}