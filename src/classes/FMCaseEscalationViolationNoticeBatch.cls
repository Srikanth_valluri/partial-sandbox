/**
 * @File Name          : FMCaseEscalationBatchForViolationNoticeProcess.cls
 * @Description        : This batch will be scheduled daily 
 *                         to fetch all those records which has no of days for case escaltion greater than 3. 
 * @Author             : Dipika Rajput
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 8/27/2019, 12:35:56 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    7/15/2019, 3:30:47 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public without sharing class FMCaseEscalationViolationNoticeBatch implements Database.Batchable<sObject>,
    Database.Stateful{
    public String query;
    public String recordId;
    
    // This List will be used to maintain the fm case to which escalation is sent and marked them as true
    public List<FM_Case__c> fmCaseEscaltionUpdateList;
    public List<EmailMessageWrapper> lstWrapper;
    public Integer NoOfEscDays = Integer.valueOf(
                    Label.Number_of_days_for_case_escalation_for_Violation_Process);
    private static final Id VIOLATION_NOTICE_RECORDTYPEID = DamacUtility.getRecordTypeId(
        'FM_Case__c', 'Violation Notice'
    );
    
    public FMCaseEscalationViolationNoticeBatch(){
        lstWrapper =   new List<EmailMessageWrapper>();
        fmCaseEscaltionUpdateList =  new List<FM_Case__c>();
    }
    public Database.Querylocator start( Database.BatchableContext bc ) {
       //lstWrapper =   new List<EmailMessageWrapper>(); 
       //recordId = 'a4025000000VKe7';
       System.debug('VIOLATION_NOTICE_RECORDTYPEID:::::::::'+VIOLATION_NOTICE_RECORDTYPEID);
        query = ' SELECT Id,'+
                ' Name,'+
                ' Select_Notice_Type_Task_Created_Date__c,'+
                ' No_of_Days_for_Case_Escalation__c,'+
                ' Property_Director_Email__c,'+ 
                ' Tenant__c,'+
                ' Is_Violation_Case_Escalation_Sent__c,'+
                ' Owner.Name,'+
                ' Owner.Email,'+
                ' RecordType.Name,'+
                ' Account__r.Name,'+
                ' Tenant__r.Name,'+
                ' Account__c'+
                ' FROM FM_Case__c '+
                ' WHERE RecordTypeId =:VIOLATION_NOTICE_RECORDTYPEID '+
                //' AND Id=:recordId'+
                //' limit 1';
                ' AND No_of_Days_for_Case_Escalation__c >: NoOfEscDays'+
                ' AND Is_Violation_Case_Escalation_Sent__c = false'
           ;
        System.debug('--query------------'+query);
               
        return Database.getQueryLocator(query);
    }
    public void execute( Database.BatchableContext bc, list<FM_Case__c> fmCaseRecords ) {
        System.debug('--This batch will be scheduled daily to fetch all those records which has no of days for case escaltion greater than 3--'+fmCaseRecords);
       
        // On 4th day sent an email to the Property Director, cc Case Initiator
        sendEscalationEmail(fmCaseRecords);
    }
    public void finish( Database.BatchableContext bc ) {
        if(lstWrapper != NULL){
            List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
            
            for(EmailMessageWrapper wrapperObj : lstWrapper) {
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.toAddresses = new String[]{wrapperObj.toAddress};
               // message.toAddresses = new String[]{'dipika.rajput@eternussolutions.com'};
               // message.optOutPolicy = wrapperObj.optOutPolicy;
                message.subject = wrapperObj.subject;
                message.HtmlBody = wrapperObj.HtmlBody;
                messages.add(message);
            }

            // Send an case escalaion email
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
            System.debug('results::::::::'+results);

            System.debug('lstWrapper::'+lstWrapper.size());
            System.debug('lstWrapper::'+lstWrapper);
            System.debug('fmCaseEscaltionUpdateSet to list update::'+fmCaseEscaltionUpdateList);
            System.debug('fmCaseEscaltionUpdateList to list update:size:'+fmCaseEscaltionUpdateList.size());

            // Update fm case list for which we have done sending an email
            if( !fmCaseEscaltionUpdateList.isEmpty()){

                // Update list
                update fmCaseEscaltionUpdateList;
            }
            /*for(EmailMessageWrapper wrapperObj : lstWrapper) {
               
               // Call sendgrid service to send an email to Property Director
                SendGridEmailService.SendGridResponse objSendGridResponse = 
                SendGridEmailService.sendEmailService(
                    //wrapperObj.toAddress,
                    'dipika.rajput@eternussolutions.com',
                    '', 
                    '', 
                    '', 
                    '', 
                    '', 
                    'Case Escalation mail', 
                    '', 
                    Label.Service_Charges_Email,
                    '', 
                    Label.Service_Charges_replyTo_Address, 
                    '', 
                    'text/html', 
                    wrapperObj.HtmlBody, 
                    '',
                    new List<Attachment>{}
                );
            System.debug('objSendGridResponse----------'+objSendGridResponse);
            }*/
        }
        
    }
    
    // This method is used to Case Escalation Emails to Property Director and Case Initiator
    public void sendEscalationEmail(list<FM_Case__c> fmCaseRecords ){
        System.debug('fmCaseRecords:----'+fmCaseRecords);
        Map<String,List<FM_Case__c>> propertyDirctorEmailToListOfCasesMap = new Map<String,List<FM_Case__c>>();
        Map<String,List<FM_Case__c>> caseInitiatorToListOfcaseMap = new Map<String,List<FM_Case__c>>();
        if(fmCaseRecords != NULL && !fmCaseRecords.isEmpty()){
            for(FM_Case__c objFMCase : fmCaseRecords){
                System.debug('objFMCase:----'+objFMCase);
               
               // Populate Is_Violation_Case_Escalation_Sent field on Fm case as it will be sending in an email
               objFMCase.Is_Violation_Case_Escalation_Sent__c = true;
               
               // Add it into stateful list and update the list
                if( !fmCaseEscaltionUpdateList.contains(objFMCase)){
                    fmCaseEscaltionUpdateList.add(objFMCase);
                }
             
               // Here we are popualting map of Property Directors email to their corresponding List of cases.
                if(!propertyDirctorEmailToListOfCasesMap.containsKey(objFMCase.Property_Director_Email__c)){
                   propertyDirctorEmailToListOfCasesMap.put(
                       objFMCase.Property_Director_Email__c,
                       new List<FM_Case__c>{objFMCase}
                   );
               }
               else{
                    propertyDirctorEmailToListOfCasesMap.get(
                       objFMCase.Property_Director_Email__c
                    ).add(objFMCase);
               }

               // Populate map of case initiator email with corresponsing list of cases.
                if(!caseInitiatorToListOfcaseMap.containsKey(objFMCase.Owner.email)){
                   caseInitiatorToListOfcaseMap.put(
                       objFMCase.Owner.email,
                       new List<FM_Case__c>{objFMCase}
                   );
                }
                else{
                    caseInitiatorToListOfcaseMap.get(
                       objFMCase.Owner.email
                    ).add(objFMCase);
                }
            }
             System.debug('----caseInitiatorToListOfcaseMap--'+caseInitiatorToListOfcaseMap);
           
            System.debug('___propertyDirctorEmailToListOfCasesMap--'+propertyDirctorEmailToListOfCasesMap);
            
            // This method is used to send an Email with respective cases to PD
            if(!propertyDirctorEmailToListOfCasesMap.isEmpty()){
                formTableOfCasesForOWnerOrPD(propertyDirctorEmailToListOfCasesMap);
            }

            // If case Initiator Map is not null call method to create an email for corresponding case initiators
            if(!caseInitiatorToListOfcaseMap.isEmpty()){
                formTableOfCasesForOWnerOrPD(caseInitiatorToListOfcaseMap);
            }
           
        }
    }

    // This method is used to send an Email with respective cases to PD
    public void formTableOfCasesForOWnerOrPD(
        Map<String,List<FM_Case__c>> emailToListOfCaseMap
    ){
        //lstWrapper = new List<EmailMessageWrapper>();
        if(!emailToListOfCaseMap.isEmpty()){
            for( String strEmail : emailToListOfCaseMap.keySet() ) {
                System.debug('strEmail:-------'+strEmail);
                String strHTMLBody = 'Hi,<br/>Please note that the following cases are Escalated.';
                strHTMLBody += ':<br/><br/><html><body>';
                strHTMLBody += '<table style="border:1px solid black; text-align:left; border-collapse:collapse;">'+
                  +'<tr><th style="border:1px solid black; text-align:center; border-collapse:collapse;">Sr.No</th>'
                  + '<th style="border:1px solid black;border-collapse:collapse; text-align:left;">FM Case#</th>'
                  + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">SR Type</th>'
                  + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">SR Owner</th>'
                  + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Customer Name</th></tr>';
                Integer serialNumber=1;
                for( FM_Case__c objCase : emailToListOfCaseMap.get( strEmail ) ) {
                      System.debug('objCase:-------'+objCase);
                      String acctName = objCase.Account__c == objCase.Tenant__c || (objCase.Tenant__c == NULL) ? objCase.Account__r.Name : objCase.Tenant__r.Name;
                      strHTMLBody += '<tr>';
                      strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+serialNumber+'</td>';
                      strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;"><a href=\"'+System.URL.getSalesforceBaseUrl().toExternalForm() + '/'+objCase.Id+'\" target=\"_blank\">'+ objCase.Name+'</a></td>';
                      strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+objCase.RecordType.Name+'</td>';
                      strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+objCase.Owner.Name+'</td>';
                      strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+acctName+'</td>';
                     
                      strHTMLBody += '</tr>';
                      serialNumber++;
                }
                strHTMLBody += '</table></body></html><br/><br/>';
                System.debug('strEmail:::::: before adding it to wrapper :'+strEmail);
                System.debug('strHTMLBody:::::: before adding it to wrapper :'+strHTMLBody);
                if(String.isNotBlank(strEmail) && String.isNotBlank(strHTMLBody)){
                    EmailMessageWrapper wrapperObj = new EmailMessageWrapper(
                        strEmail,
                        'List of Escalation FM Cases',
                        strHTMLBody
                    );
                    System.debug('wrapperObj---------'+wrapperObj);
                    lstWrapper.add(wrapperObj);
                }
                System.debug('lstWrapper---------'+lstWrapper);
                
            }
        }
    }

    // this wrapper class is used to send an emails
    public class EmailMessageWrapper {
        String toAddress;
        String subject;
        String HtmlBody;
        public EmailMessageWrapper(String toAddress,
                        String subject,String HtmlBody) {
            this.toAddress = toAddress;
            this.subject = subject;
            this.HtmlBody = HtmlBody;
        }
    }

}