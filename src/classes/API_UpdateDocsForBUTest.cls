@isTest
public class API_UpdateDocsForBUTest {
    public static String baseUrl = URL.getOrgDomainUrl().toExternalForm(); /* URL.getSalesforceBaseUrl() */
    
    @testSetup static void setup() {
        Account acc = new Account ();
        acc.Name = 'test';
        acc.Agency_Type__c = 'Corporate';
        acc.Vendor_ID__c = '96969666';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        insert acc;
        
        Property__c newProperty = new Property__c();
        newProperty.Property_ID__c  = 1;
        newProperty.Property_Code__c    = 'VIR' ;
        newProperty.Property_Name__c    = 'VIRIDIS @ AKOYA OXYGEN' ;
        newProperty.District__c = 'AL YUFRAH 2' ;
        newProperty.AR_Transaction_Type__c  = 'INV VIR' ;
        newProperty.Penalty_Transaction_Type__c = 'DM-PENALTIES-VIR' ;
        newProperty.Brokerage_Distribution_Set__c   = '11600' ;
        newProperty.Sales_Commission_Dist_Set__c    = '11601' ;
        newProperty.Currency_Of_Sale__c = 'AED' ;
        newProperty.Signature_Col_Customer_Stmt__c  = 'Front Line Investment Management Co. LLC' ;
        newProperty.EOI_Enabled__c = true;
        insert newProperty;
        
        Location__c loc = InitializeSRDataTest.createLocation('123','Building');
        loc.Property_ID__c = '123';
        insert loc;   
        
        Inventory__c invent = new Inventory__c();
        invent.CM_Price_Per_Sqft__c = 20;
        invent.Inventory_ID__c = '1';
        invent.Building_ID__c = '1';
        invent.Floor_ID__c = '1';
        invent.Marketing_Name__c = 'Damac Heights';
        invent.Address_Id__c = '1' ;
        invent.EOI__C = NULL;
        invent.Tagged_to_EOI__c = false;
        invent.Is_Assigned__c = false;
        invent.Property_ID__c = newProperty.ID;
        invent.Property__c = newProperty.Id;
        invent.Status__c = 'Released';
        invent.special_price__c = 1000000;
        invent.CurrencyISOCode = 'AED';
        invent.Property_ID__c = '1234'; 
        invent.Floor_Package_ID__c = ''; 
        invent.building_location__c = loc.id;
        invent.property_id__c = newProperty.id;
        insert invent;
        
        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal',false,null);        
        sr.Eligible_to_Sell_in_Dubai__c = true;
        sr.Agency_Type__c = 'Individual';
        sr.ID_Type__c = 'Passport';
        sr.Agent_Name__c = UserInfo.getUserId();
        sr.Token_Amount_AED__c = 40000;
        sr.RecordTypeId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Change Agent').getRecordTypeId();
        sr.Booking_Wizard_Level__c = null;
        sr.Agency_Email_2__c = 'test2@gmail.com';
        sr.Alternate_Agency_Email_2__c = 'test1@gmail.com';
        sr.Country_of_Sale__c = 'UAE';
        sr.Mode_of_Payment__c = 'Cash';
        sr.agency__c = acc.id;
        insert sr;
        
        List<Booking__c> lstbk = new List<Booking__c>();
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        lstbk.add(InitializeSRDataTest.createBooking(sr.id));
        lstbk[0].Deal_SR__c = sr.id;
        insert lstbk;
        
        buyer__c b = new buyer__c();
        b.Buyer_Type__c =  'Individual';
        b.Address_Line_1__c =  'Ad1';
        b.Country__c =  'United Arab Emirates';
        b.City__c = 'Dubai' ;       
        b.dob__c = system.today().addyears(-30);
        b.Email__c = 'test@test.com';
        b.First_Name__c = 'firstname' ;
        b.Last_Name__c =  'lastname';
        b.Nationality__c = 'Indian' ;
        b.Passport_Expiry__c = system.today().addyears(20) ;
        b.Passport_Number__c = 'J0565556' ;
        b.Phone__c = '569098767' ;
        b.Phone_Country_Code__c = 'United Arab Emirates: 00971' ;
        b.Place_of_Issue__c =  'India';
        b.Title__c = 'Mr';
        b.booking__c = lstbk[0].id;
        b.Primary_Buyer__c =true;
        b.Account__c = acc.id;
        b.Unique_Key__c = lstbk[0].id+' '+acc.id;
        insert b;
        
        Payment_Plan__c pp = new Payment_Plan__c();        
        pp.Effective_From__c = system.today();
        pp.Effective_To__c = system.today(); 
        pp.Building_Location__c =  loc.id;  
        pp.term_id__c = '1234';   
        insert pp; 
        
        DAMAC_Constants.skip_BookingUnitTrigger = true;
        List<Booking_Unit__c> BUList = new List<Booking_Unit__c>();
        Booking_Unit__c bu1 = new Booking_Unit__c();
        bu1.Booking__c = lstbk[0].id;
        bu1.Payment_Method__c = 'Cash';
        bu1.Primary_Buyer_s_Email__c = 'test@damac.com';
        bu1.Primary_Buyer_s_Name__c = 'testNSI';
        bu1.Primary_Buyer_s_Nationality__c = 'Russia';
        bu1.Inventory__c = invent.id;
        bu1.Registration_ID__c = '1234'; 
        bu1.Payment_Plan_Id__c = pp.id;  
        BUList.add(bu1);
        Booking_Unit__c bu2 = new Booking_Unit__c();
        bu2.Booking__c = lstbk[0].id;
        bu2.Payment_Method__c = 'Cash';
        bu2.Primary_Buyer_s_Email__c = 'test1@damac.com';
        bu2.Primary_Buyer_s_Name__c = 'test1NSI';
        bu2.Primary_Buyer_s_Nationality__c = 'Russia';  
        BUList.add(bu2);
        insert BUList;
        
        List<Attachment> invAttachList = new List<Attachment>();
        Blob bodyBlob;
        Attachment attach1 = new Attachment();   	
    	attach1.Name = 'Unit Test Attachment - Tax Inv';
    	bodyBlob = Blob.valueOf('Unit Test Attachment Body - Tax Inv');
    	attach1.body = bodyBlob;
        attach1.parentId = bu1.id;
        invAttachList.add(attach1);
        Attachment attach2 = new Attachment();   	
    	attach2.Name = 'Unit Test Attachment - VAT Inv';
    	bodyBlob = Blob.valueOf('Unit Test Attachment Body - VAT Inv');
    	attach2.body = bodyBlob;
        attach2.parentId = bu1.id;
        invAttachList.add(attach2);
        insert invAttachList;
    }
    
    @isTest
    static void testUploadDocsForBU() {
        ID buId = [SELECT ID FROM Booking_Unit__c WHERE Registration_ID__c != NULL LIMIT 1].Id;
        ID buId_noReg = [SELECT ID FROM Booking_Unit__c WHERE Registration_ID__c = NULL LIMIT 1].Id;
        List<Attachment> attachList = [SELECT id FROM Attachment WHERE parentId = :buId];
        
        Map<String, Object> requestMap_post = new Map<String, Object>();
        requestMap_post.put('bookingUnitId', NULL); /* buId */ 
        requestMap_post.put('invoiceReferenceNumber', NULL); 
        requestMap_post.put('invoiceDate', '31-01-2021'); 
        requestMap_post.put('invoiceAmount', 500.00); 
        requestMap_post.put('taxInvoiceAttachmentID', NULL);
        requestMap_post.put('vatCertificateAttachmentID', NULL);
        
        String reqJSON = JSON.serialize(requestMap_post);
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/API_UpdateDocsForBU'; 
        req.httpMethod = 'POST';
		req.requestBody = Blob.valueof(reqJSON);
        RestContext.request = req;
        RestContext.response = res;
        
        callPostMethod(requestMap_post); /* 1 */
        
        requestMap_post.put('bookingUnitId', 'invalid');
        callPostMethod(requestMap_post); /* 2 */
        
        requestMap_post.put('invoiceReferenceNumber', 'PRVA/32/3202A');
        callPostMethod(requestMap_post); /* 3 */
        
        requestMap_post.put('invoiceDate', '2021-01-31');
        callPostMethod(requestMap_post); /* 4 */
        
        requestMap_post.put('bookingUnitId', buId_noReg);
        callPostMethod(requestMap_post); /* 5 */
        
        requestMap_post.put('bookingUnitId', buId);
        callPostMethod(requestMap_post); /* 6 */
        
        requestMap_post.put('taxInvoiceAttachmentID', 'invalid');
        callPostMethod(requestMap_post); /* 7 */
        
        requestMap_post.put('taxInvoiceAttachmentID', (NULL != attachList && attachList.size() > 0 ? attachList[0].id : NULL));
        requestMap_post.put('vatCertificateAttachmentID',(NULL != attachList && attachList.size() > 1 ? attachList[1].id : NULL));
        callPostMethod(requestMap_post); /* 8 */
        
        Test.stopTest();
    }
    
    private static void callPostMethod(Map<String, Object> requestMap_post){
        API_UpdateDocsForBU.doPost(
        (String)requestMap_post.get('bookingUnitId'), 
        (String)requestMap_post.get('invoiceReferenceNumber'), 
        (String)requestMap_post.get('invoiceDate'), 
        (Decimal)requestMap_post.get('invoiceAmount'), 
        (String)requestMap_post.get('taxInvoiceAttachmentID'), 
        (String)requestMap_post.get('vatCertificateAttachmentID')
        );
    }
}