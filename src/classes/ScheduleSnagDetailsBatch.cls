/*
 * Description : This class is used to Schedule SnagDetailsBatch batch at every midnight
 * Revision History:
 *
 *  Version          Author              Date           Description
 *  1.0              Arjun Khatri        31/01/2019     Initial Draft                          
 */
global class ScheduleSnagDetailsBatch implements Schedulable {
  global void execute(SchedulableContext sc) {
     
      database.executebatch(new SnagDetailsBatch(), 1);
  }
}