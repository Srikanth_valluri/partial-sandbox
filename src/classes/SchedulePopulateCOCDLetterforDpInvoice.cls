/**
 * @File Name          : SchedulePopulateCOCDLetterforDpInvoice.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 12/12/2019, 12:05:02 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/9/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public class SchedulePopulateCOCDLetterforDpInvoice implements Schedulable {
    public void execute(SchedulableContext SC){
    	PopulateCOCDLetterforDpInvoiceBatch batchInst = new PopulateCOCDLetterforDpInvoiceBatch();
    	Database.executeBatch(batchInst,1);
    }
}