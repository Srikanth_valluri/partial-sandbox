public without sharing class FmDocumentUploadBatchTriggerHandler {

    private boolean m_isExecuting = false;
    private integer BatchSize = 0;

    public FmDocumentUploadBatchTriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }

    public void OnAfterUpdate(
        FM_Document_Upload_Batch__c[] oldRecords, FM_Document_Upload_Batch__c[] updatedRecords,
        Map<ID, FM_Document_Upload_Batch__c> recordMap
    ) {
        Set<Id> setApprovedDocumentUploadBatchId = new Set<Id>();
        Set<String> setDocumentTypesToNotifyRecipient = new Set<String>{'notice', 'mom', 'newsletter'};
        for (FM_Document_Upload_Batch__c oldFmDocUploadBatch : oldRecords) {
            FM_Document_Upload_Batch__c fmDocUploadBatch = recordMap.get(oldFmDocUploadBatch.Id);
            if (String.isNotBlank(fmDocUploadBatch.Document_Type__c)
                && setDocumentTypesToNotifyRecipient.contains(fmDocUploadBatch.Document_Type__c.toLowerCase())
                && String.isNotBlank(fmDocUploadBatch.Document_Level__c)
                && 'Pending'.equalsIgnoreCase(oldFmDocUploadBatch.Approval_Status__c)
                && 'Approved'.equalsIgnoreCase(recordMap.get(fmDocUploadBatch.Id).Approval_Status__c)
            ) {
                if ('Global'.equalsIgnoreCase(fmDocUploadBatch.Document_Level__c)) {
                    Database.executeBatch(new FmGlobalDocumentEmailNotificationBatch(fmDocUploadBatch.Id), 5);
                } else if('Everyone except DTPC'.equalsIgnoreCase(fmDocUploadBatch.Document_Level__c)) {
                    Database.executeBatch(new FmGlobalDocumentEmailNotificationBatch(fmDocUploadBatch.Id, fmDocUploadBatch.Document_Level__c), 5);
                } else {
                    setApprovedDocumentUploadBatchId.add(fmDocUploadBatch.Id);
                }
            }
        }
        //System.debug('setApprovedDocumentUploadBatchId : '+setApprovedDocumentUploadBatchId);
        if (!setApprovedDocumentUploadBatchId.isEmpty()) {
            FmDocumentUploadBatchTriggerHelper.emailDocumentsToFmRecipients(setApprovedDocumentUploadBatchId);
        }
    }
}