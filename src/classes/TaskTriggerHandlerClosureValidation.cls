public without sharing class TaskTriggerHandlerClosureValidation{
    public void beforeUpdate(map<Id,Task> newMap, map<Id,Task> oldMap){
        set<Id> setCaseIds = new set<Id>();
        set<Id> setRiyadhSRId = new set<Id>();//AT
        set<Id> setOverdueSRId = new set<Id>();//AT
        for(Task objT : newMap.values()){
            if(objT.Status.equalsIgnoreCase('Completed')
            && objT.Status != oldMap.get(objT.Id).Status
            && (objT.Subject == Label.DIFC_Task_Print_NOC
            || objT.Subject == Label.Dubai_Task_Print_NOC)
            && string.valueOf(objT.WhatId).startsWith('500')
            && objT.Process_Name__c.contains('Assignment')){
                setCaseIds.add(objT.WhatId);
            }

            //Validating Riyadh SR Task
            if(objT.WhatId != null
            && objT.Process_Name__c != null
            && objT.Process_Name__c.equalsIgnoreCase('Riyadh Rotana Conversion')
            && string.valueOf(objT.WhatId).startsWith('500')
            && objT.Subject != null
            && objT.Subject.equalsIgnoreCase('Update whether customer is interested in the hotel operation or not')
            && ( objT.Status.equalsIgnoreCase('Completed') || objT.Status.equalsIgnoreCase('Closed') ) ) {
                setRiyadhSRId.add(objT.WhatId);
            }

            //Validating Overdue Payment Confirmation Task
            if(objT.WhatId != null
            && objT.Process_Name__c != null
            && objT.Process_Name__c.equalsIgnoreCase('Overdue Rebate/Discount')
            && string.valueOf(objT.WhatId).startsWith('500')
            && objT.Subject != null
            && objT.Subject.equalsIgnoreCase('Confirm payment received by DAMAC as per offer letter')
            && ( objT.Status.equalsIgnoreCase('Completed') || objT.Status.equalsIgnoreCase('Closed') ) ) {
                setOverdueSRId.add(objT.WhatId);
            }
        }
        system.debug('setCaseIds**********'+setCaseIds);
        if(!setCaseIds.isEmpty()){
            map<Id,boolean> mapCase_NOCUploaded = new map<Id,boolean>();
            map<Id,boolean> mapCase_NewRegIdCreated = new map<Id,boolean>();
            for(Case objC : [Select Id
                                  , RecordTypeId
                                  , RecordType.DeveloperName
                                  , New_Booking_Unit__c
                                  , New_Booking_Unit__r.Registration_ID__c
                                  , (Select Id
                                          , Name
                                          , Attachment_URL__c
                                          , Case__c
                                          , isValid__c 
                                     From SR_Attachments__r
                                     where Name Like 'NOC Document %'
                                     and isValid__c = true
                                     and Attachment_URL__c != null
                                     order by Createddate desc limit 1)
                             from Case 
                             where Id IN : setCaseIds 
                             and (RecordType.DeveloperName = 'Assignment'
                             or RecordType.DeveloperName = 'Rental_Pool_Assignment')]){
                if(objC.New_Booking_Unit__r.Registration_ID__c != null){
                    mapCase_NewRegIdCreated.put(objC.Id,true);
                    for(SR_Attachments__c objDoc : objC.SR_Attachments__r){
                        mapCase_NOCUploaded.put(objC.Id,true);
                    }
                }
            } // end of for loop
            
            system.debug('mapCase_NOCUploaded*****'+mapCase_NOCUploaded);
            for(Task objT : newMap.values()){
                if(objT.Status.equalsIgnoreCase('Completed')
                && objT.Status != oldMap.get(objT.Id).Status
                && (objT.Subject == Label.DIFC_Task_Print_NOC
                || objT.Subject == Label.Dubai_Task_Print_NOC)
                && string.valueOf(objT.WhatId).startsWith('500')
                && objT.Process_Name__c.contains('Assignment')){
                    if(!mapCase_NOCUploaded.containsKey(objT.WhatId)){
                        system.debug('Error tak aya*********');
                        objT.addError('Error : This task cannot be closed unless a valid NOC is uploaded on the Case');
                    }
                    if(!mapCase_NewRegIdCreated.containsKey(objT.WhatId)){
                        objT.addError('Error : RegId for the new unit has not been created yet. Please contact your administrator.');
                    }
                }
            } // end of for loop
        } //end of setCaseIds not empty
        if( setRiyadhSRId.size() > 0 ) {
            validateRiyadhTask( setRiyadhSRId, newMap);
        }
        if( setOverdueSRId.size() > 0 ) {
            validateOverdueTask( setOverdueSRId, newMap);
        }
        
        
    } // end of beforeUpdate
    
    public void afterUpdate(map<Id,Task> newMap, map<Id,Task> oldMap){
        set<String> setTaskIds = new set<String>();
        for(Task objT : newMap.values()){
            if(!CheckRecursive.SetOfIDs.contains(objT.Id)){
                if(objT.Subject != null
                && objT.Status.equalsIgnoreCase('Completed')
                && objT.Status != oldMap.get(objT.Id).Status
                && objT.Subject.contains('- Rejected')
                && string.valueOf(objT.WhatId).startsWith('500')
                && objT.Process_Name__c.contains('Assignment')
                && String.isNotBlank(objT.Parent_Task_Id__c)){
                    setTaskIds.add(objT.Parent_Task_Id__c);
                    CheckRecursive.SetOfIDs.add(objT.Id);
                }
            }
        } // end of for loop
        system.debug('setTaskIds***********'+setTaskIds);
        if(!setTaskIds.isEmpty()){
            list<Task> lstTask = new list<Task>();
            for(Task objT : [Select t.WhatId
                                  , t.Subject
                                  , t.Status
                                  , t.Process_Name__c
                                  , t.Priority
                                  , t.Id
                                  , t.IPMS_Role__c
                                  , t.CurrencyIsoCode
                                  , t.Assigned_User__c
                                  , t.ActivityDate 
                            From Task t
                            where Id IN : setTaskIds]){
                Task objTask = objT;
                objTask.Id = null;
                objT.Status = 'Not Started';
                objT.ActivityDate = date.today();
                lstTask.add(objT);
            }
            system.debug('lstTask***********'+lstTask);
            if(!lstTask.isEmpty()){
                insert lstTask;
            }
        }
    } // end of afterUpdate

    public void validateRiyadhTask( Set<Id> setRiyadhSRId, map<Id,Task> newMap ) {

        Set<Id> setCaseId = new Set<Id>();
        if( setRiyadhSRId.size() > 0 ) {
            for( Case objCase : [SELECT
                                    Id
                                    , Owner_Interested_In_Hotel__c
                                    , RecordType.Name
                                FROM
                                    Case
                                WHERE
                                    Id IN : setRiyadhSRId
                                AND
                                    RecordTypeId != null
                                AND
                                    RecordType.Name = 'Riyadh Rotana Conversion'
                                AND
                                    Owner_Interested_In_Hotel__c = null
                                ]) {
                setCaseId.add( objCase.Id );                        
            }

            if( setCaseId.size() > 0 ) { 
                for( Id taskId : newMap.keySet() ) {
                    if( setCaseId.contains( newMap.get(taskId).WhatId ) )
                        newMap.get(taskId).addError('Please Update whether customer is interested in the hotel operation or not.');
                }
            }
        }
    }// end of validateRiyadhTask

    public void validateOverdueTask ( Set<Id> setOverdueSRId, map<Id,Task> newMap ) {
        Set<Id> setCaseId = new Set<Id>();
        if( setOverdueSRId.size() > 0 ) {
            for( Case objCase : [SELECT
                                        Id
                                        , Is_Overdue_Rebate_Letter_Genrated__c
                                        , RecordType.Name
                                    FROM
                                        Case
                                    WHERE
                                        Id IN : setOverdueSRId
                                    AND
                                        RecordTypeId != null
                                    AND
                                        RecordType.Name = 'Overdue Rebate/Discount'
                                    AND
                                        Is_Overdue_Rebate_Letter_Genrated__c = false
                                    ]) {
                    setCaseId.add( objCase.Id );                        
            }

            if( setCaseId.size() > 0 ) { 
                for( Id taskId : newMap.keySet() ) {
                    if( setCaseId.contains( newMap.get(taskId).WhatId ) )
                        newMap.get(taskId).addError('Please upload Signed Overdue Rebate Letter.');
                }
            }
        }
    }
}