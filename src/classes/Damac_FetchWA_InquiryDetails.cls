Public class Damac_FetchWA_InquiryDetails {

    public static Inquiry__c doCallout (String refId, Inquiry__c inq) {
        Whatsapp_Lead_Info__c credentials = Whatsapp_Lead_Info__c.getInstance (UserInfo.getuserID());
        
        HTTPRequest req = new HTTPRequest ();
        req.setEndpoint (credentials.Endpoint__c+refid);
        req.setHeader ('Authorization', credentials.Authorization__c);
        req.setTimeOut (120000);
        req.setMethod ('GET');
        
        HTTP http = new HTTP ();
        HTTPResponse res = new HTTPResponse ();
        if (!Test.isRunningTest ())
            res = http.send (req);
        System.debug (res);
        System.debug (res.getBody ());
        
        if (Test.isRunningTest ()) {
            res.setStatusCode (200);
            Campaign__c camp = new Campaign__c();
            camp.End_Date__c = system.today().addmonths(10);
            camp.Marketing_End_Date__c = system.today().addmonths(10);
            camp.Marketing_Start_Date__c = system.today().addmonths(-10);
            camp.Start_Date__c =  system.today().addmonths(-10);
            insert camp;
            res.setBody ('{"status": true, "data": {"campaignId": "'+camp.Id+'"}}');
        }
        
        if (res.getStatusCode () == 200) {
            Map <String, Object> resJson = (Map <String, Object>) JSON.deserializeUntyped (res.getBody ());
            if (resJson.get ('status') == true) {
                Map <String, Object> dataJson = (Map <String, Object>) JSON.deserializeUntyped (JSON.serialize (resJson.get ('data')));
                ID campaignId = String.valueOf (dataJson.get ('campaignId'));
                
                inq.campaign__c = campaignId;                
                inq.ga_client_id__c = String.valueOf (dataJson.get ('GAClientId'));
                inq.IP_Address__c = String.valueOf (dataJson.get ('ipAddress'));
                inq.Sourced_Country__c = String.valueOf (dataJson.get ('countryNameSync'));
                inq.Sourced_City__c = String.valueOf (dataJson.get ('citySync'));
                inq.countryCodeSync__c = String.valueOf (dataJson.get ('countryCodeSync'));
                inq.Project_Name__c = String.valueOf (dataJson.get ('projectName'));
                inq.GCLID__c = String.valueOf (dataJson.get ('GCLID'));
                inq.Ref_ID__c = String.valueOf (dataJson.get ('refID'));
                inq.Is_Converted__c = String.valueOf (dataJson.get ('isConverted'));
                inq.converted_Date__c = String.valueOf (dataJson.get ('convertedDate'));
                
                
            }
        }
        system.debug (inq);
        return inq;
    }
}