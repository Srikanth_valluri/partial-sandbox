/**********************************************************************************************************************
* Name                  : AgencyPCTriggerHandler                                                                      *
* Test Class            : AgencyPCTriggerHandlerTest                                                                  *
* Description           : This is a handler class for agency pc trigger.                                              *
* Created By            : NSI                                                                                         *
* Created Date          : 12/01/2017                                                                                  *
* --------------------------------------------------------------------------------------------------------------------*
* VERSION       AUTHOR          DATE            COMMENTS                                                              *
* 1.0           Sivasankar      12/01/2017      Initial development                                                   *
**********************************************************************************************************************/
public class AgencyPCTriggerHandler implements TriggerFactoryInterface{

    /*********************************************************************************************
    * @Description : Method to contain logic to be executed before delete.                       *
    * @Params      : List<sObject>                                                               *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeBeforeDeleteTrigger(Map<Id, sObject> mapOldRecords){
        deletePcSharing(mapOldRecords);
    }

    /*********************************************************************************************
    * @Description : Method to contain logic to be executed after insert.                        *
    * @Params      : List<sObject>                                                               *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeAfterInsertTrigger(Map<Id, sObject> mapNewRecords){
        executeOnAfterInsertUpdateDelete(mapNewRecords.keySet());
        addAgencyPcToGroup(mapNewRecords.values());
        providePcAccess(mapNewRecords,NULL);
    }

    /*********************************************************************************************
    * @Description : Method to contain logic to be executed after update.                        *
    * @Params      : List<sObject>                                                               *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeAfterUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){
        executeOnAfterInsertUpdateDelete(mapNewRecords.keySet());
        providePcAccess(mapNewRecords,mapOldRecords);
    }

    /*********************************************************************************************
    * @Description : Method to contain logic to be executed after delete.                        *
    * @Params      : List<sObject>                                                               *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void executeAfterDeleteTrigger(Map<Id, sObject> mapOldRecords){
        executeOnAfterInsertUpdateDelete(mapOldRecords.keySet());
    }

    /*********************************************************************************************
    * @Description : Method to add agency pc to the collaboration group.                         *
    * @Params      : List<Agency_PC__c>                                                          *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public void addAgencyPcToGroup(List<Agency_PC__c> allAgencyPC){
        Set<Id> stIds = new Set<Id>();
        for(Agency_PC__c ag : allAgencyPC){
            if(ag.Agency__c != null){
                stIds.add(ag.Agency__c);
            }
        }
        if(!stIds.isEmpty()){
            Map<Id, Account> mpaccount = new Map<Id, Account>([SELECT Id, Name, Agency_Short_Name__c, UniqueNo__c FROM Account WHERE Id IN: stids]);
            //prepare a map of collabaration grp name => account id
            Map<String, Id> mpcolgrpNameaccid = new Map<String, Id>();
            for(account acc : mpaccount.values()){
                string str = (acc.Agency_Short_Name__c != null ? acc.Agency_Short_Name__c : '' );
                if(str.length() > 27){
                    str = str.substring(0,27);
                }
                mpcolgrpNameaccid.put(str+acc.UniqueNo__c+'-Sales', acc.Id);
            }
            system.debug('-->mpcolgrpNameaccid'+mpcolgrpNameaccid);

            Set<Id> userIdSet = new Set<Id>();
            //prepare a map if accid to list of agency pc's
            Map<Id, List<Agency_PC__c>> mpaccidlstagencies = new Map<Id, List<Agency_PC__c>>();
            for(Agency_PC__c aPC: allAgencyPC){
                if(mpaccidlstagencies.containsKey(aPC.Agency__c)){
                    mpaccidlstagencies.get(aPC.agency__c).add(apc);
                }else{
                    mpaccidlstagencies.put(aPC.agency__c, new List<Agency_PC__c>{apc});
                }
                userIdSet.add(aPC.User__c);
            }
            system.debug('-->userIdSet'+userIdSet);

            Profile guestProfile = [SELECT Id FROM Profile WHERE Name = 'New Registration Profile' LIMIT 1];
            Map<Id, User> userIdMap = new Map<Id, User>(
                [SELECT Id, Profile.Name FROM USER WHERE Id IN :userIdSet]);

            //Prepare map of account id => collabarationgroup id
            Map<Id, Id> mpaccidColgrpid = new Map<Id, Id>();
            if(mpcolgrpNameaccid != null && !mpcolgrpNameaccid.isempty()){
                for(CollaborationGroup cg : [SELECT Id, Name FROM CollaborationGroup WHERE Name IN: mpcolgrpNameaccid.keyset()]){
                    mpaccidColgrpid.put(mpcolgrpNameaccid.get(cg.name), cg.id);
                }
            }
            system.debug('-->mpaccidColgrpid'+mpaccidColgrpid);

            List<CollaborationGroupMember> lstCgm = new List<CollaborationGroupMember>();
            //iterate through all agency pc of an account and add them to their respective collabaration group.
            for(id accid : mpaccidlstagencies.keyset()){
                if(mpaccidlstagencies.get(accid) != null
                    && !mpaccidlstagencies.get(accid).isempty()
                    && mpaccidColgrpid.containskey(accid)
                ){
                    for(Agency_PC__c apc : mpaccidlstagencies.get(accid)){
                        if (userIdMap.get(apc.User__c).ProfileId != guestProfile.Id) {
                            system.debug('-->mapProfile' + userIdMap.get(apc.User__c).ProfileId  );
                            system.debug('-->guestProfile.Id' + guestProfile.Id );
                            CollaborationGroupMember cgm = new CollaborationGroupMember();
                            cgm.CollaborationGroupId = mpaccidColgrpid.get(accid);
                            cgm.memberId = apc.User__c;
                            lstCgm.add(cgm);
                        }
                    }
                }
            }
            system.debug('-->lstCgm'+lstCgm);
            if(lstCgm != null && !lstCgm.isempty()){
                //insert lstCgm;
                database.insert (lstCgm, false);
            }
        }
    }

    /*********************************************************************************************
    * @Description : Method to share individual accounts to property consultant.                 *
    * @Params      : Map<Id, sObject>, Map<Id, sObject>                                          *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public static void providePcAccess(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){
        List<Agency_PC__c> lsttoConsider = new List<Agency_PC__c>();
        List<AccountSharingUtility.JsonWrapper> sharingRecordList = new List<AccountSharingUtility.JsonWrapper>();
        Map<Id, Set<Id>> agencyAssociatedUserMap = new Map<Id, Set<Id>>();

        /* Prepare map of account ids to the set of associated users. */
        for(sObject objrec : mapNewRecords.values()){
            Agency_PC__c newRecord = (Agency_PC__c)objrec;
            Agency_PC__c oldRecord = null;
            if(mapOldRecords != null && !mapOldRecords.isempty()){
                oldRecord = (Agency_PC__c)mapOldRecords.get(objrec.id);
            }
            if((Trigger.isInsert && newRecord.Agency__c != null) ||
               (Trigger.isUpdate && newRecord.Agency__c != null && oldRecord != null &&
                (newRecord.Agency__c != oldRecord.Agency__c ||
                 newRecord.User__c != oldRecord.User__c))){
                if(agencyAssociatedUserMap.containskey(newRecord.Agency__c)){
                    agencyAssociatedUserMap.get(newRecord.Agency__c).add(newRecord.User__c);
                }else{
                    agencyAssociatedUserMap.put(newRecord.Agency__c, new Set<Id>{newRecord.User__c});
                }
            }
        }
        Set<Id> userId = new Set<Id>();
        Map <Id, Id> accIdOwnerIdMap = new Map <Id, Id>();
        Map <Id, Set<Id>> accIdUserSetMap = new Map <Id, Set<Id>>();
        if(agencyAssociatedUserMap != null && !agencyAssociatedUserMap.isempty()){
            //share only the individual accounts to users
            Map<Id, Account> mpAccountIndividual
                = new Map<Id, Account>([SELECT Id, Name, OwnerId
                                          FROM Account
                                         WHERE Id IN: agencyAssociatedUserMap.keyset()
                                           AND ( RecordType.Name = 'Individual Agency'
                                                OR RecordType.Name = 'Individual Agency - Terminated'
                                                OR RecordType.Name = 'Individual Agency - Blacklisted'
                                                )
                                         ]);
            /*Map<Id, Account> mpAccountIndividual
                = new Map<Id, Account>([SELECT Id, Name, OwnerId FROM Account 
                                        WHERE recordtype.Name = 'Individual Agency' 
                                        AND Id IN: agencyAssociatedUserMap.keyset()]);
            */
            if(mpAccountIndividual != null && !mpAccountIndividual.isEmpty()){
                for(Id accountId : mpAccountIndividual.keyset()){
                    if(agencyAssociatedUserMap.get(accountId) != null && !agencyAssociatedUserMap.get(accountId).isEmpty()){
                        //sharingRecordList.add(new AccountSharingUtility.JsonWrapper(accountId, agencyAssociatedUserMap.get(accountId)));
                        accIdUserSetMap.put(accountId, agencyAssociatedUserMap.get(accountId));
                        accIdOwnerIdMap.put(accountId, mpAccountIndividual.get(accountId).OwnerId);
                    }
                }
            }
        }
        if (!accIdUserSetMap.isEmpty()) {
            shareAccount(accIdUserSetMap, accIdOwnerIdMap);
        }

        /*system.debug('#### sharingRecordList = '+sharingRecordList);
        if(!sharingRecordList.isempty()){
            //AccountSharingUtility.shareAccount(json.serialize(sharingRecordList));
        }*/
    }


    public static void shareAccount(Map <Id, Set<Id>> pAccIdUserSetMap, Map <Id, Id> pAccIdOwnerIdMap ){
        try{
            List<AccountShare> lstaccSharetoInsert = new List<AccountShare>();
            if(pAccIdUserSetMap != null && !pAccIdUserSetMap.isEmpty()
                && pAccIdOwnerIdMap != null && !pAccIdOwnerIdMap.isEmpty()
            ){
                for(Id accId : pAccIdUserSetMap.keySet()) {
                    if (pAccIdOwnerIdMap.containsKey(accId)) {
                        String accOwnerId = pAccIdOwnerIdMap.get(accId);
                        if (String.isNotBlank(accOwnerId)) {
                            for (Id userId:  pAccIdUserSetMap.get(accId)) {
                                if (accOwnerId != userId) {
                                    AccountShare accshare = new AccountShare();
                                    accshare.AccountId = accId;
                                    accshare.UserOrGroupId = userId;
                                    accshare.AccountAccessLevel = 'Edit';
                                    accshare.OpportunityAccessLevel = 'Edit';
                                    lstaccSharetoInsert.add(accshare);
                                }
                            }
                        }
                    }
                }
            }
            if(lstaccSharetoInsert != null && !lstaccSharetoInsert.isempty()){
                //insert lstaccSharetoInsert;
                database.insert (lstaccSharetoInsert, false);
            }
        }
        catch(exception ex){
            system.debug('--->Error in account sharing -->'+ex.getMessage());
        }
    }


    /*********************************************************************************************
    * @Description : Method to delete the sharing of individual accounts with PC.                *
    * @Params      : Map<Id, sObject>                                                            *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    public static void deletePcSharing(Map<Id, sObject> mapOldRecords){
        Map<string,integer> mpAccIdUserId = new Map<string,integer>();
        Set<Id> staccid = new Set<Id>();
        for(sObject objrec : mapOldRecords.values()){
            Agency_PC__c newrec = (Agency_PC__c)objrec;
            if(newrec.User__c != null && newrec.Agency__c != null){
                mpAccIdUserId.put(string.valueof(newrec.User__c)+string.valueof(newrec.Agency__c),1);
                staccid.add(newrec.Agency__c);
            }
        }
        if(mpAccIdUserId != null && !mpAccIdUserId.isempty()){
            List<AccountShare> lstAccShare = [SELECT Id, UserOrGroupId, AccountId, RowCause FROM AccountShare WHERE AccountId =: staccid AND RowCause != 'Owner'];
            if(lstAccShare != null && !lstAccShare.isempty()){
                List<AccountShare> lstAccSharetodelete = new List<AccountShare>();
                for(AccountShare accshare : lstAccShare){
                    if(mpAccIdUserId.containsKey(string.valueof(accshare.UserOrGroupId)+string.valueof(accshare.AccountId))){
                        lstAccSharetodelete.add(accshare);
                    }
                }
                if(lstAccSharetodelete != null && !lstAccSharetodelete.isempty()){
                    delete lstAccSharetodelete;
                    database.emptyRecycleBin(lstAccSharetodelete);
                }
            }
        }
    }

    /*********************************************************************************************
    * @Description : Method to contain logic to be executed before delete.                       *
    * @Params      : Set<Id>                                                                     *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    @TestVisible private void executeOnAfterInsertUpdateDelete(Set<Id> updatedAgencyIDs){
        List<Account> updateAccounts = new List<Account>();
        try{
            //Aggregate query to get the number of active agency users are assigned to the account
            for(AggregateResult PC : [SELECT Agency__c, Count(Id) totPCs
                                      FROM Agency_PC__c
                                      WHERE ID IN: updatedAgencyIDs AND
                                            User__c != null AND
                                            User__r.isActive = true
                                      GROUP BY Agency__c]){
                    updateAccounts.add(new Account(Id = (ID)PC.get('Agency__c'), Number_of_PCs_Assigned__c = (Decimal)PC.get('totPCs')));
            }
            //update the records if the list is not blank
            system.debug('------updateAccounts----'+updateAccounts);
            if(!updateAccounts.isEmpty()){
                update updateAccounts;
            }
        }catch(Exception ex){
            if(!Trigger.isDelete)
                Trigger.new[0].addError(''+ex.getMessage());
            else Trigger.old[0].addError(''+ex.getMessage());
        }
    }

    // TOBE:Implemented
    public void executeBeforeInsertTrigger(list<sObject> lstNewRecords){}
    public void executeBeforeUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){}
    public void executeBeforeInsertUpdateTrigger(list<sObject> lstNewRecords, Map<Id,sObject> mapOldRecords){}
    public void executeAfterInsertUpdateTrigger(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){}
}// End of class.