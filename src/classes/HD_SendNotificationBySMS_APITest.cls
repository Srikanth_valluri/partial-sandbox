@isTest
public class HD_SendNotificationBySMS_APITest {
    private static String baseUrl = URL.getOrgDomainUrl().toExternalForm(); /* URL.getSalesforceBaseUrl() */
    
    @testSetup
    static void setup(){
        Id personAcc_rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account acc1 = new Account(LastName = 'Test Account1', 
                                   Party_ID__c = '63061', 
                                   RecordtypeId = personAcc_rtId, 
                                   Mobile_Country_Code__pc = 'United Arab Emirates: 00971', 
                                   Mobile_Phone_Encrypt__pc = '9988770777', 
                                   Email__pc = 'success@mailinator.com');
        insert acc1;
        
        Account acc2 = new Account(LastName = 'Test Account2', 
                                   Party_ID__c = '63062', 
                                   RecordtypeId = personAcc_rtId, 
                                   Mobile_Country_Code__pc = 'United Arab Emirates: 00971', 
                                   Mobile_Phone_Encrypt__pc = '9988770666', 
                                   Email__pc = 'fail@mailinator.com');
        insert acc2;
    }
    
    @isTest
    static void test_SendMessage(){
        Map<String, Object> requestMap_post = new Map<String, Object>();
        requestMap_post.put('party_id', '');
        requestMap_post.put('mobile_number', '');
        requestMap_post.put('message', NULL);
        
        String reqJSON = JSON.serialize(requestMap_post);
        RestRequest request = new RestRequest();
        request.requestUri = baseUrl + '/services/apexrest/sendNotification/sms';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(reqJSON);
        
        RestResponse response = new RestResponse();
		
        RestContext.request = request;
        RestContext.response = response;
        
        Test.startTest();
        
        invokeSMS_NotificationPost(requestMap_post); /* 1 Both party_id & mobile_number empty */
        
        requestMap_post.put('mobile_number', '09719988770666');
        invokeSMS_NotificationPost(requestMap_post); /* 2 Blank message, given mobile number */
        
        requestMap_post.put('mobile_number', '09719988770666');
        requestMap_post.put('message', 'TEST Message');
        invokeSMS_NotificationPost(requestMap_post); /* 3 Fail, given mobile_number and message */
        
        requestMap_post.put('mobile_number', '09719988770777');
        requestMap_post.put('message', 'TEST Message');
        invokeSMS_NotificationPost(requestMap_post); /* 4 Success, given mobile_number and message */
        
        requestMap_post.put('mobile_number', '');
        requestMap_post.put('party_id', '63060');
        invokeSMS_NotificationPost(requestMap_post); /* 5 Invalid party_id and message */
        
        requestMap_post.put('party_id', '63062');
        invokeSMS_NotificationPost(requestMap_post); /* 6 Fail, valid party_id and message */
        
        requestMap_post.put('party_id', '63061');
        invokeSMS_NotificationPost(requestMap_post); /* 7 Success, valid party_id and message */
        
        Test.stopTest();
    }
    
    private static void invokeSMS_NotificationPost(Map<String, Object> requestMap_post) {
        HD_SendNotificationBySMS_API.doPost(
            (String)requestMap_post.get('party_id'), 
            (String)requestMap_post.get('mobile_number'), 
            (String)requestMap_post.get('message'), 
        	'LOAMS');
    }
}