/**
 * Logic class for Agent Status Change webservice
 */
public class AgentStatusChangeLogic {

    /**
     * Method to process request and send the response
     */
    public static void processAgentStatusChangeRequest() {
        AgentStatusChangeResponseBody response = new AgentStatusChangeResponseBody();

        // Get request body
        String requestBody = RestContext.request.requestBody.toString();
        System.debug('=========== requestBody : ' + requestBody);

        try {
            // Parse request body
            AgentStatusChangeRequestBody request = (AgentStatusChangeRequestBody) JSON.deserializeStrict(
                requestBody,
                AgentStatusChangeRequestBody.Class
            );
            System.debug('===== request : ' + request);
            System.debug('===== request : ...status----'+request.newStatus);

            User cre = getCreForExtension(request.creExtension);
            if (cre == null) {
                response.errorMessage = 'No CRE User found for the given extension';
                RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
                Restcontext.response.statusCode = 400;
                return;
            }
/*
            Boolean statusFound = checkIfStatusValuePresent(request.newStatus);
            if (!statusFound) {
                response.errorMessage = 'The status is invalid';
                RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
                Restcontext.response.statusCode = 400;
                return;
            }
*/
            cre.Status__c = request.newStatus;
            System.debug('============ cre.Status__c : ' + cre.Status__c);
            update cre;
        } catch (DmlException ex) {
            response.errorMessage = ex.getMessage();
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
            Restcontext.response.statusCode = 400;
            System.debug('============= response : ' + JSON.serialize(response));
            return;
        }

        response.status = 'CRE User status updated';
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
        Restcontext.response.statusCode = 200;
    }

    /**
     * Method to CRE User for the extension
     */
    public static User getCreForExtension(String extension) {
        List<User> creUsers =
            [
                SELECT
                    Id,
                    Extension
                FROM
                    User
                WHERE
                    Extension =: extension
            ];
        if (creUsers.size() > 0) {
            return creUsers[0];
        } else {
            return null;
        }
    }

    /*public static Boolean checkIfStatusValuePresent(String status) {
        Boolean statusFound = false;
        List<Schema.PicklistEntry> userStatusList = User.fields.Status__c.getDescribe().getpicklistvalues();
        for (Schema.PicklistEntry userStatus : userStatusList) {
            if (userStatus.getValue() == status) {
                statusFound = true;
                return statusFound;
            }
        }
        return statusFound;
    }*/

    /**
     * Wrapper class for AgentStatusChangeWebService request body
     */
    public class AgentStatusChangeRequestBody {
        public String dateAndTime {get; set;}
        public String creExtension {get; set;}
        public String newStatus {get; set;}

        public AgentStatusChangeRequestBody() {
            this.dateAndtime = '';
            this.creExtension = '';
            this.newStatus = '';
        }
    }

    /**
     * Wrapper class for AgentStatusChangeWebService response body
     */
    public class AgentStatusChangeResponseBody {
        public String status;
        public String errorCode;
        public String errorMessage;

        public AgentStatusChangeResponseBody() {
            this.status = '';
            this.errorCode = '';
            this.errorMessage = '';
        }
    }
}