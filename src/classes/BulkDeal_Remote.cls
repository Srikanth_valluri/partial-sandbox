global class BulkDeal_Remote{
    public BulkDeal_Remote(ApexPages.StandardController controller) {   
    }
        public BulkDeal_Remote(BulkDealController controller) {}    

    public static string preBookId { get; set; }
    public static Boolean isPortalUser {
        get {        
            User userObj = [SELECT IsPortalEnabled  FROM user WHERE id=:userinfo.getuserid() LIMIT 1];
            return userObj.IsPortalEnabled;
        } 
        set;
    }
    
    /*
    Query the Individual Agnecy Details and return results on the Page
    */
    @RemoteAction 
    global static List<Account> getAgencyDetails(String searchKey){
        List<Account> agencyList = new List<Account>();
        List<Id> accountIdsList = new List<Id>();
        for(Agency_PC__c thisAgency : [SELECT Id, user__c, Agency__c
                                       FROM Agency_PC__c 
                                       WHERE user__c =: UserInfo.getUserId() 
                                       AND  (Agency__r.RecordTypeId =: DamacUtility.getRecordTypeId('Account', 'Individual Agency'))]){
            accountIdsList.add(thisAgency.Agency__c);   
        }
        system.debug('#### accountIdsList = '+accountIdsList);
        for(Account thisAccount : [SELECT Id, Name, RecordTypeId, RecordType.Name,
                                       (SELECT Id, Name, Org_ID_formula__c, Org_ID__c FROM Agent_Sites__r  WHERE End_Date__c = NULL) 
                                   FROM Account 
                                   WHERE Name LIKE: '%' + searchKey + '%' 
                                   AND (Id IN: accountIdsList OR (RecordTypeId =: DamacUtility.getRecordTypeId('Account', 'Corporate Agency') 
                                           AND  Blacklisted__c = false AND  Terminated__c = false))]){
            agencyList.add(thisAccount);    
        }
        return agencyList;
    }
    
    /*
    Query the Corporate Agnecy Details and return results on the Page   
    */
    @RemoteAction 
    global static List<Contact> getCorporateAgents(String selectedCorporateAgency){
        List<Contact> agentsList = new List<Contact>();
        try {
            agentsList = [select id,Name from contact where AccountId=:selectedCorporateAgency];
        } Catch (Exception e) {}
        return agentsList;
    }  
    
        
    @RemoteAction 
    global static List<Deal_Exception_Request__c> getDealExceptionDetails(String searchKey, String selectedDER){
        Set<String> selectedDERIds = new Set<String>();
        if(selectedDER != null && selectedDER != ''){
            String[] tmp = selectedDER.split(',');
            for(String s : tmp){
                IF(s != NULL)
                    selectedDERIds.add(s);
            }
        }
        List<Deal_Exception_Request__c> dealExceptionList= new List<Deal_Exception_Request__c>();
        dealExceptionList = [SELECT id, Name, Inquiry__c, Status__c ,List_of_Units__c 
                            FROM Deal_Exception_Request__c where Name LIKE: '%' + searchkey + '%' 
                                    AND RecordType.Name = 'Deal Exception' AND Name NOT IN: selectedDERIds 
                                    AND Status__c ='Approved' AND Bulk_Deal__c = null];  
        return dealExceptionList ;
    }
    
    @RemoteAction 
    global static List<NSIBPM__Service_Request__c> getServiceRequest(String searchKey, String selectedDER, String SelectedSR){
        Set<ID> invSet = new Set<ID>();
        Set<ID> srSet = new Set<ID>();
        Set<String> srNames = new Set<String>();
        if(selectedDER != '' && selectedDER != null){
            Set<String> selectedDERIds = new Set<String>();
            String[] tmp = selectedDER.split(',');
            for(String s : tmp){
                IF(s != NULL)
                    selectedDERIds.add(s);
            }
            for(Deal_Exception_Unit__c deu : [SELECT id, Name, Inventory__c 
                                              FROM Deal_Exception_Unit__c 
                                              where Deal_Exception_Request__r.Name =: selectedDERIds AND Inventory__c != null]){
                invSet.add(deu.Inventory__c); 
            }
            for(Booking_Unit__c bu : [SELECT id, Name, Inventory__c, Booking__r.Deal_SR__c FROM Booking_Unit__c where Inventory__c IN: invSet]){
                srSet.add(bu.Booking__r.Deal_SR__c);
            }
        }
        if(SelectedSR != null){
            String[] tmp = SelectedSR.split(',');
            for(String s : tmp){
                IF(s != NULL)
                    srNames.add(s);
            }
        }
        if(selectedDER != '' && selectedDER != null){
            return [SELECT id, Name, Inventory__c, List_of_Units__c, 
                        NSIBPM__Internal_Status_Name__c 
                    FROM NSIBPM__Service_Request__c 
                    where Name LIKE: '%' + searchkey + '%' AND Id IN: srSet 
                            AND Name NOT IN: srNames AND (NOT NSIBPM__Internal_Status_Name__c LIKE '%Rejected%') 
                            AND NSIBPM__Internal_Status_Name__c != 'Draft' AND Bulk_Deal__c = null AND RecordType.Name = 'Deal' limit 1000];
        }else{
            return [SELECT id, Name, Inventory__c, List_of_Units__c,
                         NSIBPM__Internal_Status_Name__c 
                    FROM NSIBPM__Service_Request__c 
                    where Name LIKE: '%' + searchkey + '%' AND Name NOT IN: srNames 
                    AND (NOT NSIBPM__Internal_Status_Name__c LIKE '%Rejected%') AND Bulk_Deal__c = null  AND NSIBPM__Internal_Status_Name__c != 'Draft' 
                    AND RecordType.Name = 'Deal' limit 1000];
        }
        
    }
}