@isTest
public class GetAmenitiesByBookingUnit_APITest {
    
    @TestSetup
    static void TestData() {
        insert new IpmsRestServices__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            BaseUrl__c = 'http://0.0.0.0:8080/webservices/rest',
            Username__c = 'username',
            Password__c = 'password',
            Timeout__c = 120000
        );

        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'Janusia';
        insert locObj;

        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id,
                                                          Registration_ID__c = '3901',
                                                          Inventory__c = invObj.Id,
                                                          Unit_Name__c='JNU/SD168/XH2910B');
        insert bookingUnit;

        Property__c propObj = TestDataFactoryFM.createProperty();
        insert propObj;

        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;

        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;

        Resource__c objRes = new Resource__c();
        objRes.Non_Operational_Days__c = 'Sunday';
        objRes.Resource_Type__c = 'FM Amenity';
        objRes.No_of_advance_booking_days__c = 2;
        objRes.No_of_deposit_due_days__c = 1;
        objRes.Contact_Person_Email__c = 'test@test.com';   //
        objRes.Contact_Person_Name__c = 'Test'; //
        objRes.Contact_Person_Phone__c = '1234567890';  //
        insert objRes;

        Resource_Sharing__c objRS= new Resource_Sharing__c();
        objRS.Building__c = locObj.Id;
        objRS.Resource__c = objRes.Id;
        insert objRS;

        Resource_Slot__c objResSlot1 = new Resource_Slot__c();
        objResSlot1.Resource__c = objRes.Id;
        objResSlot1.Start_Date__c = Date.Today();
        objResSlot1.End_Date__c = Date.Today().addDays(30);
        objResSlot1.Start_Time_p__c = '09:00';
        objResSlot1.End_Time_p__c = '09:30';
        insert objResSlot1;

        Non_operational_days__c obj = new Non_operational_days__c();
        obj.Non_operational_date__c = Date.today();
        obj.Resource__c = objRes.Id;
        insert obj;
    }

    @isTest
    static void testAmenities(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenities';  
       // req.addParameter('bookingUnitId', '');
        //Blob requestBody = Blob.valueOf('requestString');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
            GetAmenitiesByBookingUnit_API.GetAmenitiesForUnit();
        Test.stopTest();
    }
    
     @isTest
    static void testAmenitiesBlank(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenities';  
        req.addParameter('bookingUnitId', '');
        //Blob requestBody = Blob.valueOf('requestString');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
            GetAmenitiesByBookingUnit_API.GetAmenitiesForUnit();
        Test.stopTest();
    }
    
     @isTest
    static void testAmenitiesNotBlank(){

        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenities';  
        req.addParameter('bookingUnitId', objBU.Id);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        FmIpmsRestServices.DueInvoicesResponse invoicesResponse = new FmIpmsRestServices.DueInvoicesResponse();
        invoicesResponse.OutputParameters = new FmIpmsRestServices.OutputParameters();
        invoicesResponse.OutputParameters.X_RETURN_STATUS = 'S';
        invoicesResponse.OutputParameters.X_RETURN_MESSAGE = 'Total Records Fetched...1';
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE = new FmIpmsRestServices.ResponseMessage();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM = new List<Map<String, String>>();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM.add(
            new Map<String, String> {
                'ATTRIBUTE1' => 'registrationId',
                'ATTRIBUTE2' => 'unitName',
                'ATTRIBUTE3' => 'projectName',
                'ATTRIBUTE4' => 'customerId',
                'ATTRIBUTE5' => 'orgId',
                'ATTRIBUTE6' => 'partyId',
                'ATTRIBUTE7' => 'partyName',
                'ATTRIBUTE8' => 'trxNumber',
                'ATTRIBUTE9' => 'creationDate',
                'ATTRIBUTE10' => 'callType',
                'ATTRIBUTE11' => '50',
                'ATTRIBUTE12' => '100',
                'ATTRIBUTE13' => '50',
                'ATTRIBUTE14' => FmcUtils.formatAsIpmsDate(Date.today()),
                'ATTRIBUTE15' => 'trxType',
                'ATTRIBUTE16' => '0'
            }
        );
        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(invoicesResponse)));


        Test.startTest();
            GetAmenitiesByBookingUnit_API.GetAmenitiesForUnit();
            //GetAmenitiesByBookingUnit_API.isServiceFeeDuePending(objBU);
        Test.stopTest();
    }

     @isTest
    static void GetSingleAmenityForUnitTest(){
        
        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        Resource__c objRes = [SELECT id FROM Resource__c WHERE Resource_Type__c = 'FM Amenity'];

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenities';  
        req.addParameter('bookingUnitId', objBU.Id);
        req.addParameter('amenityId', objRes.Id);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        

        Test.startTest();
            //GetAmenitiesByBookingUnit_API.GetAmenitiesForUnit();
            GetAmenitiesByBookingUnit_API.GetSingleAmenityForUnit();
        Test.stopTest();
    }

    @isTest
    static void MissingBookingUnitId() {
    
        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        Resource__c objRes = [SELECT id FROM Resource__c WHERE Resource_Type__c = 'FM Amenity'];

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenities';  
        //req.addParameter('bookingUnitId', bookingUnit.Id);
        req.addParameter('amenityId', objRes.Id);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        

        Test.startTest();
            //GetAmenitiesByBookingUnit_API.GetAmenitiesForUnit();
            GetAmenitiesByBookingUnit_API.GetSingleAmenityForUnit();
        Test.stopTest();
    }

    @isTest
    static void NullBookingUnitId() {

        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        Resource__c objRes = [SELECT id FROM Resource__c WHERE Resource_Type__c = 'FM Amenity'];

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenities';  
        req.addParameter('bookingUnitId', '');
        req.addParameter('amenityId', objRes.Id);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        

        Test.startTest();
            //GetAmenitiesByBookingUnit_API.GetAmenitiesForUnit();
            GetAmenitiesByBookingUnit_API.GetSingleAmenityForUnit();
        Test.stopTest();
    }

    @isTest
    static void MissingAmenityId() {

        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        Resource__c objRes = [SELECT id FROM Resource__c WHERE Resource_Type__c = 'FM Amenity'];

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenities';  
        req.addParameter('bookingUnitId', objBU.Id);
        //req.addParameter('amenityId', objRes.Id);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        

        Test.startTest();
            //GetAmenitiesByBookingUnit_API.GetAmenitiesForUnit();
            GetAmenitiesByBookingUnit_API.GetSingleAmenityForUnit();
        Test.stopTest();
    }

    @isTest
    static void NullAmenityId() {

        Booking_Unit__c objBU = [SELECT id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        Resource__c objRes = [SELECT id FROM Resource__c WHERE Resource_Type__c = 'FM Amenity'];
        
        

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAmenities';  
        req.addParameter('bookingUnitId', objBU.Id);
        req.addParameter('amenityId', '');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        

        Test.startTest();
            //GetAmenitiesByBookingUnit_API.GetAmenitiesForUnit();
            GetAmenitiesByBookingUnit_API.GetSingleAmenityForUnit();
        Test.stopTest();
    }
}