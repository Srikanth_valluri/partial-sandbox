@RestResource(urlMapping='/SendAppointmentsToMObileApp/*')
 Global class SendAppointmentsToMObileApp
 {
   
     @HtTPPost
    Global static List<Calling_List__c> SendAppointments(String userName)
    {
    
        User ur=[select id,Contact.AccountId from user where userName=:userName];
        
            Account objAcc = [Select id,name from Account where id =: ur.Contact.AccountId];
           List<Calling_List__c> lstCalling = new List<Calling_List__c>();
            lstCalling = [Select id,name,OwnerID, Account__c,Appointment_Date__c,Account_Email__c,
                             Account__r.Name,Account__r.Email__c,Account__r.PersonEmail,
                             Appointment_Slot__c,Account_Name_for_Walk_In__c,Appointment_Status__c,Booking_Unit_Name__c,
                             Registration_ID__c,
                             RecordType.DeveloperName, Assigned_CRE__c,Assigned_CRE__r.name,Service_Type__c, 
                             Sub_Purpose__c,
                             Assigned_CRE__r.id from Calling_List__c where Account__c =: ur.Contact.AccountId
                             AND RecordType.DeveloperName = 'Appointment_Scheduling' order By name Desc ];
                             
           return lstCalling;
                                                              
    }
 }