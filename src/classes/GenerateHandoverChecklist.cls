/*******************************************************************************************************************************
 * @Name              : GenerateHandoverChecklist
 * @Test Class Name   : GenerateHandoverChecklistTest
 * @Description       : Class to generate handover checklist document based on certain parameters.
 * ------------------------------------------------------------------------------------------------------------------------------
 * VERSION  | AUTHOR            | DATE          | Update Log
 * ------------------------------------------------------------------------------------------------------------------------------
 * 1.0      |                   | 27/02/2018    | Initial Draft
 * 2.0      | Aishwarya Todkar  | 30/09/2020    | Added method to generation checklist with E-Sign
 * 2.1      | Aishwarya Todkar  | 06/10/2020    | Updated AV calculations
*********************************************************************************************************************************/
public without sharing class GenerateHandoverChecklist {
    public String HCurl;
    Case objCase;
    String strPageID;
    String drawloopFlag;
    public Boolean isEsign;
    public Boolean fromIPMS;
    public Static String HANDOVER = 'Handover';
    public Static String EARLY_HANDOVER = 'Early_Handover';
    public Static String COMPLETED = 'Completed';
    public Static String SIGNED_HANDOVER_CHECKLIST = 'Signed Handover Checklist';
    public Static String HANDOVER_CHECKLIST = 'Handover Checklist';
    public Static String IS_ESIGN = 'isEsign';
    public Static String FROM_IPMS = 'fromIPMS';
    public Static String GENERATE_HANDOVER_CHECKLIST = 'Generate Handover Checklist';
    public Static String VILLA = 'Villa';
    public Static String CASE_EARLY_HANDOVER = 'Early Handover';
    public Static String CASE_VAR = 'Case';

    public GenerateHandoverChecklist(ApexPages.standardController controller){

    }//End constructor

    /*****************************************************************************************************************************
    * @Description : Method to set parameters for Generating handover Checklist
    *                and executing batch to generate the the documents.
    * @Params      : None
    * @Return      : None
    *****************************************************************************************************************************/
    public pagereference init(){
        isEsign = false;
        fromIPMS = false;
        if(ApexPages.currentPage().getParameters().containsKey(IS_ESIGN)){
            isEsign =  Boolean.valueOf(ApexPages.currentPage().getParameters().get(IS_ESIGN));
        }
        if(ApexPages.currentPage().getParameters().containsKey(FROM_IPMS)){
            fromIPMS =  Boolean.valueOf(ApexPages.currentPage().getParameters().get(FROM_IPMS));
        }
        strPageID = ApexPages.currentPage().getParameters().get('id');
        objCase = [select Id
                        , AccountId
                        , Booking_Unit__c
                        , Payment_Verified__c
                        , CaseNumber
                        , ParentId
                        , RecordType.DeveloperName
                        , RecordType.Name
                        , Booking_Unit__r.Registration_ID__c
                        , Early_Handover_Status__c
                        , Documents_Uploaded__c
                        , Booking_Unit__r.Unit_Type__c
                        , Booking_Unit__r.Property_Name__c
                        , Booking_Unit__r.Property_Country__c
                        , Booking_Unit__r.Property_City__c
                        , Booking_Unit__r.Building_Name__c
                        , Booking_Unit__r.District__c
                        , Booking_Unit__r.Permitted_Use__c
                        , Booking_Unit__r.Area_Variation_Formula__c
                        , Booking_Unit__r.Inventory__c
                        , Booking_Unit__r.Inventory__r.Building_Location__c
                        , Booking_Unit__r.Inventory__r.Building_Location__r.New_Ho_Doc_Type__c
                From 
                    Case
                Where 
                    Id =:strPageID];

        //call method to generatw VAT letter
        generateHOLetter( objCase );

        if((objCase.ParentId != null
        && objCase.Recordtype.DeveloperName.equalsIgnoreCase(HANDOVER))
        || (objCase.Recordtype.DeveloperName.equalsIgnoreCase(EARLY_HANDOVER))) {

            HandoverChecklistGeneration.HCResponse objHC;
            // for normal handover send  HOCHECKLIST instead of E-HOCHECKLIST
            String processName = '';
            if(objCase.Recordtype.DeveloperName.contains('Early')){
                processName = 'E-HOCHECKLIST';
            }else{
                processName = 'HOCHECKLIST';
            }

            drawloopFlag = Label.CheckList_DrawLoop_Control;

            if( !fromIPMS ){

                //Generating HO checklist from drawloop
                Boolean isAv = objCase.Booking_Unit__r.Area_Variation_Formula__c == null
                            || objCase.Booking_Unit__r.Area_Variation_Formula__c ==  0 ? false : true;
                if( isEsign) {

                    //Call method to generat HO checklist with E-Signature
                    CheckListGenrator.genrateVirtualHOCheckList( objCase.Booking_Unit__r.Unit_Type__c
                                                                , objCase.Booking_Unit__r.Property_Name__c
                                                                , objCase.Booking_Unit__r.Property_Country__c
                                                                , objCase.Booking_Unit__r.Building_Name__c
                                                                , objCase.Booking_Unit__r.District__c
                                                                , objCase.Booking_Unit__r.Property_City__c
                                                                , objCase.Booking_Unit__r.Permitted_Use__c
                                                                , objCase.Booking_Unit__c
                                                                , CASE_VAR
                                                                , objCase.Id
                                                                , isAv );
                }
                else {

                    //Call method to generat HO checklist without E-Signature
                    CheckListGenrator.genrateHOCheckList( objCase.Booking_Unit__r.Unit_Type__c
                                                        , objCase.Booking_Unit__r.Property_Name__c
                                                        , objCase.Booking_Unit__r.Property_Country__c
                                                        , objCase.Booking_Unit__r.Building_Name__c
                                                        , objCase.Booking_Unit__r.District__c
                                                        , objCase.Booking_Unit__r.Property_City__c
                                                        , objCase.Booking_Unit__r.Permitted_Use__c
                                                        , objCase.Booking_Unit__c
                                                        , CASE_VAR
                                                        , objCase.Id
                                                        , isAv);
                }
                return new PageReference('/' + objCase.Id);
            }
            else{

                //Generating HO checklist from IPMS
                objHC = HandoverChecklistGeneration.GetHandoverChecklist(processName, objCase.Booking_Unit__r.Registration_ID__c);
                HCurl = objHC.url;
                return processResponse(objHC.P_PROC_MESSAGE);
            }
        }
        else if(objCase.ParentId == null && objCase.Recordtype.DeveloperName.equalsIgnoreCase(HANDOVER)){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,
                    'Key Handover Checklist cannot be generated on this Service Request,'
                    +' this Service Request is for Customer document verification. Please go to the unit level Service Requests.');
            ApexPages.addMessage(myMsg);
            return null;
        }
        else{
            return null;
        }
    }

    /*****************************************************************************************************************************
    * @Description : Method to process response.
    * @Params      : None
    * @Return      : None
    *****************************************************************************************************************************/
    public pagereference processResponse(String errorDetails){
        list<SR_Attachments__c> lstCaseAttachment = new list<SR_Attachments__c>();
        list<Error_Log__c> lstErrorLog = new list<Error_Log__c>();

        if (String.isBlank(HCurl)) {
            Error_Log__c objErr = new Error_Log__c();
            objErr.Account__c = objCase.AccountId;
            objErr.Booking_Unit__c = objCase.Booking_Unit__c;
            objErr.Case__c = objCase.id;
            objErr.Error_Details__c = errorDetails;
            if (objCase.RecordType.Name.equalsIgnoreCase(EARLY_HANDOVER)) {
                objErr.Process_Name__c = CASE_EARLY_HANDOVER;
            } else if (objCase.RecordType.Name.equalsIgnoreCase(HANDOVER)) {
                objErr.Process_Name__c = HANDOVER;
            }
            lstErrorLog.add(objErr);
        } else {
            Id checklistId = fetchDocument(HANDOVER_CHECKLIST);
            Id signedChecklistId = fetchDocument(SIGNED_HANDOVER_CHECKLIST);
            SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
            objCaseAttachment.Case__c = objCase.id;
            objCaseAttachment.Name = objCase.CaseNumber+HANDOVER_CHECKLIST +system.now();
            objCaseAttachment.Attachment_URL__c = HCurl;
            objCaseAttachment.Booking_Unit__c = objCase.Booking_Unit__c;
            objCaseAttachment.IsValid__c = true;
            objCaseAttachment.Id = checklistId;
            lstCaseAttachment.add(objCaseAttachment);

            SR_Attachments__c objCaseAttachment1 = new SR_Attachments__c();
            objCaseAttachment1.Case__c = objCase.id;
            objCaseAttachment1.Name = objCase.CaseNumber+ SIGNED_HANDOVER_CHECKLIST +system.now();
            objCaseAttachment1.Booking_Unit__c = objCase.Booking_Unit__c;
            objCaseAttachment1.Id = signedChecklistId;
            lstCaseAttachment.add(objCaseAttachment1);

            objCase.Documents_Uploaded__c = true;
        }

        if (lstCaseAttachment != null && lstCaseAttachment.size()>0){
            upsert lstCaseAttachment;
        }
        if (lstErrorLog != null && lstErrorLog.size()>0){
            insert lstErrorLog;
        }
        if(objCase.Documents_Uploaded__c){
            update objCase;
        }

        list<Task> lstTask = new list<Task>();
        for (Task objTask: [Select Id, Status, Subject, Process_Name__c
                            From Task
                            Where Status != :COMPLETED
                            And Process_Name__c  = :CASE_EARLY_HANDOVER
                            And Subject = :GENERATE_HANDOVER_CHECKLIST
                            And WhatId = :strPageID]) {
            objTask.Status = COMPLETED;
            lstTask.add(objTask);
        }
        if (lstTask != null && lstTask.size() > 0) {
            update lstTask;
        }
        pagereference newpg = new Pagereference(URL.getSalesforceBaseUrl().toExternalForm()+'/'+strPageID);
        newpg.setRedirect(false);
        return newpg;
    }

    /*****************************************************************************************************************************
    * @Description : Method to fetch the document from the related Case.
    * @Params      : String DocumentName
    * @Return      : None
    *****************************************************************************************************************************/
    public Id fetchDocument(String docName){
        docName= '%'+docName+'%';
        list<SR_Attachments__c> lstSR = [Select Id, Name, Case__c
                                        from SR_Attachments__c
                                        where Case__c =:objCase.Id
                                        and Name Like :docName limit 1];
        if(lstSR != null && !lstSR.isEmpty()){
            return lstSR[0].Id;
        }else{
            return null;
        }
    }

    /*****************************************************************************************************************************
    * @Description : Method to generate HO Letters.
    * @Params      : Sobject Case
    * @Return      : None
    *****************************************************************************************************************************/
    public static void generateHOLetter( Case objCase ) {
        if( objCase != null && objCase.Booking_Unit__c != null
        && objCase.Booking_Unit__r.Inventory__c != null
        && objCase.Booking_Unit__r.Inventory__r.Building_Location__c != null
        && String.isNotBlank( objCase.Booking_Unit__r.Inventory__r.Building_Location__r.New_Ho_Doc_Type__c )
        && ( objCase.Booking_Unit__r.Inventory__r.Building_Location__r.New_Ho_Doc_Type__c.equalsIgnoreCase(VILLA)
            || objCase.Booking_Unit__r.Inventory__r.Building_Location__r.New_Ho_Doc_Type__c.equalsIgnoreCase( 'Unit' )
            || objCase.Booking_Unit__r.Inventory__r.Building_Location__r.New_Ho_Doc_Type__c.equalsIgnoreCase( 'Apartment' ) )
        ) {

            String csName = '';
            csName = objCase.Booking_Unit__r.Inventory__r.Building_Location__r.New_Ho_Doc_Type__c.equalsIgnoreCase(VILLA) ?
                                                                                            'HO Letter Villa' : 'HO Letter Unit';

            Riyadh_Rotana_Drawloop_Doc_Mapping__c cs = Riyadh_Rotana_Drawloop_Doc_Mapping__c.getInstance( csName );
            if(cs != null ) {

                if( String.isBlank( cs.Drawloop_Document_Package_Id__c )) {
                    ApexPages.addmessage(
                        new ApexPages.message( ApexPages.severity.ERROR,
                        'Please provide Drawloop Document package Id of ' + csName )
                    );
                }

                else if( String.isBlank( cs.Delivery_Option_Id__c )) {
                    ApexPages.addmessage(
                        new ApexPages.message( ApexPages.severity.ERROR,
                        'Please provide Delivery Option Id of ' + csName)
                    );
                }
                else {
                    try {
                        DrawloopDocGen.generateDoc( cs.Drawloop_Document_Package_Id__c
                                                    , cs.Delivery_Option_Id__c
                                                    , objCase.Id );
                        ApexPages.addmessage(
                            new ApexPages.message( ApexPages.severity.INFO,
                            csName + ' generated!')
                        );
                    }
                    catch (Exception ex ) {
                        ApexPages.addmessage(
                            new ApexPages.message( ApexPages.severity.ERROR,
                            ex.getMessage() )
                        );
                    }
                }
            }//End cs If
            else {
                ApexPages.addmessage(
                    new ApexPages.message( ApexPages.severity.ERROR,
                    'Can not find Drawloop Document Package - ' + csName)
                );
            }
        }   //End CaseId if
    }
}