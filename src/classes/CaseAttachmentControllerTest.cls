@isTest
public class CaseAttachmentControllerTest{
    public static testmethod void testController(){
        Account objAccount = TestDataFactory_CRM.createPersonAccount(); 
        insert objAccount;
        
        Case objCase = new Case();
        objCase.Status = 'NotSubmitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Case_Type__c = 'Proof of Payment';  
        insert objCase;
        
        SR_Attachments__c objAttach = new SR_Attachments__c();
        objAttach.Case__c  = objCase.Id;
        objAttach.isValid__c = true;
        objAttach.Description__c = 'Abctd';
        objAttach.IsRequired__c = True;
        objAttach.Name = 'Test';
        objAttach.Need_Correction__c =  False;
        insert objAttach;

        test.StartTest();
        Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockAssignment());
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        ApexPages.StandardController stdController = new ApexPages.StandardController(objAttach);
        CaseAttachmentController objDocExt = new CaseAttachmentController (stdController);
        objDocExt.needCorrection = False;
        objDocExt.selectedType= 'Abc';
        objDocExt.description= 'abcefg';
        objDocExt.fileName= 'file';
        objDocExt.fileType= 'PDF';
        objDocExt.selectedAwesomeness = False;
        objDocExt.damacSigned = True;
        objDocExt.fileBody = Blob.valueOf('thisIsBlob');

        /*objDocExt.saveCustomAttachment();
        objDocExt.saveStandardAttachment(objAttach.Id);*/
       // objDocExt.extractName('Thisis.String');
        //objDocExt.extractType('This.is\\String');
        //objDocExt.processUpload();
        objDocExt.saveCaseAttachmentURL();        
        objDocExt.back();
        objDocExt.rend = true;
        objDocExt.rend1 = false;
        objDocExt.commandLinkAction();
        CaseAttachmentController objNew = new CaseAttachmentController();
        objNew.rend = false;
        objNew.rend1 = true;
        objNew.commandLinkAction();
        test.StopTest();
        //objDocExt.updateDetails();
    }
    
    public static testmethod void noFileTest(){
        Account objAccount = TestDataFactory_CRM.createPersonAccount(); 
        insert objAccount;
        
        Case objCase = new Case();
        objCase.Status = 'NotSubmitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Case_Type__c = 'Proof of Payment';  
        insert objCase;
        
        SR_Attachments__c objAttach = new SR_Attachments__c();
        objAttach.Case__c  = objCase.Id;
        objAttach.isValid__c = true;
        objAttach.Description__c = 'Abctd';
        objAttach.IsRequired__c = True;
        objAttach.Name = 'Test';
        objAttach.Need_Correction__c =  False;
        insert objAttach;

        test.StartTest();
        Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockAssignment());
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        ApexPages.StandardController stdController = new ApexPages.StandardController(objAttach);
        CaseAttachmentController objDocExt = new CaseAttachmentController (stdController);
        objDocExt.needCorrection = False;
        objDocExt.selectedType= 'Abc';
        objDocExt.description= 'abcefg';
        objDocExt.fileName= null;
        objDocExt.fileType= null;
        objDocExt.selectedAwesomeness = False;
        objDocExt.damacSigned = True;
        objDocExt.fileBody = Blob.valueOf('thisIsBlob');

        /*objDocExt.saveCustomAttachment();
        objDocExt.saveStandardAttachment(objAttach.Id);*/
       // objDocExt.extractName('Thisis.String');
        //objDocExt.extractType('This.is\\String');
        //objDocExt.processUpload();
        objDocExt.saveCaseAttachmentURL();        
        objDocExt.back();
        objDocExt.rend = true;
        objDocExt.rend1 = false;
        objDocExt.commandLinkAction();
        CaseAttachmentController objNew = new CaseAttachmentController();
        objNew.rend = false;
        objNew.rend1 = true;
        objNew.commandLinkAction();
        test.StopTest();
        //objDocExt.updateDetails();
    }
    
    /**
     * Test method to check whether the PRC Document field is updated or not on Case.
     */
    public static testmethod void testPRCDocumentUpdationOnCase() {
        
        // Creation of Case
        Case objCase = new Case();
        objCase.Status = 'NotSubmitted';
        objCase.recordTypeId = getRecordTypeIdHandoverCase();
        insert objCase;
        
        // Creation of Task
        Task objTask = new Task();
        objTask.subject = 'Upload All Handover Documents';
        objTask.ActivityDate = System.Today();
        objTask.WhatId = objCase.Id;
        objTask.status = 'In Progress';
        insert objTask;
        
        Test.startTest();
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
            objTask.status = 'Completed';
            update objTask;
            CaseAttachmentController.updatePRCDocumentOnCase(new List<Task>{objTask});
        Test.stopTest();
        
        List<Case> lstCases = [SELECT Id, PRC_Document__c FROM Case where Id =: objCase.Id];
        
        // Check if the PRC Document field on Case is set to true or not
        System.assert(lstCases.size() > 0);
        System.assert(lstCases[0].PRC_Document__c == true);
    }

    /**
     * Method to get the "Non-SR" record type
     */
    private static Id getRecordTypeIdHandoverCase() {
      Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
      Id handoverTypeID = caseRecordTypes.get('Handover').getRecordTypeId();
      return handoverTypeID;
    }
}