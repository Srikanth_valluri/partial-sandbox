public without sharing class FmcBookingUnitDataProvider implements DataProvider {

    public DataDisplayConfig getData(DataDisplayConfig config) {
        List<Booking_Unit__c> lstBookingUnit = new List<Booking_Unit__c>();

        if (config.fieldList != NULL) {
            String fields = 'Id,';
            Set<String> setFields = new Set<String>();
            Schema.SObjectType objectToken = Schema.getGlobalDescribe().get(config.objectName);
            if (objectToken != NULL) {
                setFields = objectToken.getDescribe().fields.getMap().keySet();
            }
            for (Map<String,String> fieldMap : config.fieldList) {
                String fieldName = fieldMap.get(DataRendererUtils.FIELD_NAME);
                if (String.isNotBlank(fieldName) && !fields.containsIgnoreCase(fieldName + ',')) {
                    fields += fieldName + ',';
                }
                String referencedFields = fieldMap.get(DataRendererUtils.REFERENCED_FIELDS);
                if (String.isNotBlank(referencedFields)) {
                    for (String field : referencedFields.toLowerCase().split(',')) {
                        if (setFields.contains(field) && !fields.containsIgnoreCase(field + ',')) {
                            fields += field + ',';
                        }
                    }
                }
            }
            fields = fields.removeEnd(',');
            //System.debug( 'FmcBookingUnitDataProvider query = \n' +
            //    ' SELECT ' + fields + ' FROM Booking_Unit__c '
            //    + ' WHERE  (Owner__c     = \'' + CustomerCommunityUtils.customerAccountId + '\' '
            //        + ' OR Tenant__c    = \'' + CustomerCommunityUtils.customerAccountId + '\' '
            //        + ' OR Resident__c  = \'' + CustomerCommunityUtils.customerAccountId + '\') '
            //        + ' AND (Handover_Flag__c = \'Y\' OR Early_Handover__c = true) '
            //        + ' AND Registration_Status__c IN :setActiveStatuses '
            //    + (String.isBlank(config.filter) ? '' : ' AND ' + config.filter)
            //    + (String.isBlank(config.recordLimit) ? '' : ' LIMIT ' + config.recordLimit)
            //);
            if (String.isBlank(config.filter)) {
                lstBookingUnit = FmcUtils.queryUnitsForAccount(CustomerCommunityUtils.customerAccountId, fields);
            } else if (config.filter.containsIgnoreCase('Booking__r.Account__c')) {
                lstBookingUnit = FmcUtils.queryOwnedUnitsForAccount(CustomerCommunityUtils.customerAccountId, fields);
            } else if (config.filter.containsIgnoreCase('Tenant__c')) {
                lstBookingUnit = FmcUtils.queryLeasedUnitsForAccount(CustomerCommunityUtils.customerAccountId, fields);
            }
        }

        config.dataList = DataRendererUtils.wrapData(config, lstBookingUnit);

        return config;
    }

}