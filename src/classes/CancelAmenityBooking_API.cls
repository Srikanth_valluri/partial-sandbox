@RestResource(urlMapping='/cancelAmenityBooking/*')
global class CancelAmenityBooking_API {

    public static final String NO_FMCASE = 'No FM Case record found for given FmCaseId';
    public static final String SUCCESS = 'Booking cancelled';
    
    public static String responseMessage;
    public static Integer statusCode;

    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };

    @HttpPost 
    global static FinalReturnWrapper CancelBooking() {

        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);

        CancelAmenityWrapper objReturn = new CancelAmenityWrapper();
        FinalReturnWrapper returnResponse = new FinalReturnWrapper();

        cls_data objData = new cls_data();
        cls_meta_data objMeta = new cls_meta_data();

        if(!r.params.containsKey('fmCaseId')) {

            objMeta.message = 'Missing parameter : fmCaseId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
           //objMeta.is_success = false;

            returnResponse.meta_data = objMeta;
            return returnResponse;

        }
        else if(r.params.containsKey('fmCaseId') && String.isBlank(r.params.get('fmCaseId'))) {
            objMeta.message = 'Missing parameter value: fmCaseId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
           //objMeta.is_success = false;

            returnResponse.meta_data = objMeta;
            return returnResponse;

        }

        /*Actual logic*/
        if(r.params.containsKey('fmCaseId') && String.isNotBlank(r.params.get('fmCaseId'))) {

            System.debug('fmCaseId: ' + r.params.get('fmCaseId'));

            List<FM_Case__c> lstFMCase = [SELECT id
                                               , Name
                                               , Status__c
                                               , Amenity_Booking_Status__c
                                               , Booking_Start_Time_p__c
                                               , Booking_End_Time_p__c
                                               , Booking_Date__c
                                          FROM FM_Case__c
                                          WHERE id=: r.params.get('fmCaseId')];

            System.debug('lstFMCase: ' + lstFMCase);

            if(lstFMCase.isEmpty()) {
                responseMessage = NO_FMCASE;
                statusCode = 3;
            }

            if(!lstFMCase.isEmpty()) {

                List<String> bookingStartTime = lstFMCase[0].Booking_Start_Time_p__c.split(':');
                System.debug('bookingStartTime: ' + bookingStartTime);

                Datetime bookingDateTime = Datetime.newInstance(lstFMCase[0].Booking_Date__c.year(), lstFMCase[0].Booking_Date__c.month(), lstFMCase[0].Booking_Date__c.day()).addHours(Integer.valueOf(bookingStartTime[0])).addMinutes(Integer.valueOf(bookingStartTime[1]));
                System.debug('bookingDateTime: ' + bookingDateTime);
                System.debug('Datetime.now(): ' + Datetime.now());


                if(lstFMCase[0].Status__c == 'Cancelled' && lstFMCase[0].Amenity_Booking_Status__c == 'Cancelled' ) {
                    responseMessage = 'FMCase has been already cancelled';
                    statusCode = 2;
                    objReturn.FmCaseId = lstFMCase[0].Id;
                    objReturn.FmCaseStatus = lstFMCase[0].Status__c;
                    objReturn.FmCaseNumber = lstFMCase[0].Name;
                }
                else if(bookingDateTime < Datetime.now()) {
                    objMeta.message = 'Booking date already in past ';
                    objMeta.status_code = 3;
                    objMeta.title = mapStatusCode.get(3);
                    objMeta.developer_message = null;
                   //objMeta.is_success = false;

                    returnResponse.meta_data = objMeta;
                    return returnResponse;
                }
                //else if(lstFMCase[0].Status__c == 'Closed') {
                //  responseMessage = 'FMCase has been already closed';
                //  objReturn.FmCaseId = lstFMCase[0].Id;
                //  objReturn.FmCaseStatus = lstFMCase[0].Status__c;
                //  objReturn.FmCaseNumber = lstFMCase[0].Name;
                //}
                else {
                    lstFMCase[0].Status__c = 'Cancelled';
                    lstFMCase[0].Amenity_Booking_Status__c = 'Cancelled';
                    try {
                        update lstFMCase;
                        responseMessage = SUCCESS;
                        statusCode = 1;
                        objReturn.FmCaseId = lstFMCase[0].Id;
                        objReturn.FmCaseStatus = lstFMCase[0].Status__c;
                        objReturn.FmCaseNumber = lstFMCase[0].Name;
                    }
                    catch(System.Exception excp) {
                        insert new Error_Log__c(Process_Name__c = 'Amenity Booking',
                                                Error_Details__c = excp.getMessage() + ' in CancelAmenityBooking_API for FMCaseId : '+ lstFMCase[0].id);
                        //responseMessage = excp.getMessage() + ' in CancelAmenityBooking_API for FMCaseId : '+ lstFMCase[0].id;
                    } 
                }
            }
        }

        //Wrapper filling for final response
        //cls_meta_data objMeta = new cls_meta_data();
        objMeta.status_code = statusCode;
        objMeta.message = responseMessage;
        objMeta.title = mapStatusCode.get(statusCode);
        objMeta.developer_message = null;

        //cls_data objData = new cls_data();
        //objData.fm_case_id = objReturn.FmCaseId;
        //objData.fm_case_status = objReturn.FmCaseStatus;
        //objData.fm_case_number =objReturn.FmCaseNumber;

        returnResponse.data = objData;
        returnResponse.meta_data = objMeta;

        System.debug('returnResponse: ' + returnResponse);
        //return objReturn;
        return returnResponse;
    }

    global class CancelAmenityWrapper {
        public String status;
        public String message;
        public String FmCaseId;
        public String FmCaseStatus;
        public String FmCaseNumber;
    }

    //Wrapper classes for returning reponse
    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_data {
        //public String fm_case_id;
        //public String fm_case_status;
        //public String fm_case_number;
    }

    public class cls_meta_data {
        //public String status;
        //public String message;   

        public String message;
        public Integer status_code;
        public String title;
        public String developer_message; 
    }

}