/********************************************************************************************************************************
 * Description : Controller class of visualforce page "GenerateHOChecklistOnCase" and to display Handover Checklist 
 *               document on Case on click of the button "Generate HO Checklist"
 *===============================================================================================================================
 * Ver      Date-DD/MM/YYYY     Author              Modification
 *===============================================================================================================================
 * 1.0     27/09/2020         Raveena Jain        Intial Draft
*********************************************************************************************************************************/
public class GenerateHOChecklistOnCaseCntrl {
    
    public static final String HANDOVER_CHECKLIST='Handover Checklist%';
    public String urlToRedirect {get;set;}
    
    public GenerateHOChecklistOnCaseCntrl(ApexPages.StandardController stdController) {
        Id caseId = (Id)stdController.getRecord().Id;
        System.debug('stdController.getRecord().Id---->'+stdController.getRecord().Id);
         
        System.debug('caseId---->'+caseId);
        Case cse = [SELECT Booking_Unit__c 
                    FROM Case 
                    WHERE Id=:caseId 
                    LIMIT 1
                    ];
         System.debug('cse---->'+cse);
         System.debug('cse.Booking_Unit__c---->'+cse.Booking_Unit__c);
        
        if(cse != null && cse.Booking_Unit__c != null){
            Id bookingUnitId = cse.Booking_Unit__c;
            
            List<SR_Attachments__c> srAttachmentList=[SELECT Id
                                                            ,Attachment_URL__c 
                                                      FROM SR_Attachments__c 
                                                      WHERE Booking_Unit__c =: bookingUnitId
                                                      AND Name LIKE :HANDOVER_CHECKLIST 
                                                      ORDER BY LastModifiedDate Desc
                                                      LIMIT 1
                                                      ];
            
             System.debug('srAttachmentList---->'+srAttachmentList);
            if(!srAttachmentList.isEmpty() && srAttachmentList[0].Attachment_URL__c != null){
                urlToRedirect = srAttachmentList[0].Attachment_URL__c;   
                 System.debug('urlToRedirect---->'+urlToRedirect);
            }
        }
        if(urlToRedirect == '' || urlToRedirect == null){
            ApexPages.addmessage( new ApexPages.message( ApexPages.severity.INFO 
                , 'No documents associated with this case!!' 
                )
            );
        }
    }
}