@isTest
public class GetCustomerLoamsOutstandingBalCntrlTest {
    
     public class GetCustomerLoamsOutstandingBalCntrlMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            req.setMethod('POST');
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            String Json = '{"OutputParameters": {"@xmlns": "http://xmlns.oracle.com/apps/ont/rest/XXDC_AR_BALANCE_PKG/ar_bal_against_reg_id/","@xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance","X_REGISTRATION_ID": "33443","X_BALANCE_AMOUNT": "21501.83","X_RETURN_STATUS": "S","X_ERROR_MSG": "Customer Balance Against Registration ID: 33443 is: 21501.83"}}';
            res.setBody(Json);
            res.setStatusCode(200);
            return res;
        }
    }
    @isTest
    static void testCustomerloamsOutstadingBal(){
        
        Case objCase = new Case();
        ObjCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Service_Charge_Reset').getRecordTypeId();
        insert objCase;
        System.assertNotEquals(null, objCase.Id);
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        System.assertNotEquals(null, objSR.Id);
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        insert objBooking;
        System.assertNotEquals(null, objBooking.Id);
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Registration_ID__c = 'test';
        objBU.Booking__c = objBooking.Id;
        insert objBU;
        System.assertNotEquals(null, objBU.Id);
        
        SR_Booking_Unit__c objSRBooking = new SR_Booking_Unit__c();
        objSRBooking.Case__c = objCase.Id;
		objSRBooking.Booking_Unit__c = objBU.Id;
        objSRBooking.Reset_Start_Date__c = System.Today();
        objSRBooking.Reset_End_Date__c = System.Today();
        insert objSRBooking;
        System.assertNotEquals(null, objSRBooking.Id);
       
        Credentials_Details__c objCred = new Credentials_Details__c();
        objCred.Name = 'CustomerLOAMSOutstandingBalance';
        objCred.Endpoint__c = 'Some Value ';
        objCred.User_Name__c = 'oracle_user';
        objCred.Password__c = 'crp1user';
        insert objCred;
        
        Test.setMock(HttpCalloutMock.class, new GetCustomerLoamsOutstandingBalCntrlMock());
        
        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
            GetCustomerLoamsOutstandingBalCntrl objController = new GetCustomerLoamsOutstandingBalCntrl(sc);
            //objController.init();
            GetCustomerLoamsOutstandingBalCntrl.init();
        Test.stopTest();
        
    }
}