@isTest
public class PcConfirmationHelper_Test {
    static testMethod void myUnitTest() {
        Id accountRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account a = new Account();
            a.LastName= 'Test Account';
            a.RecordTypeId = accountRTId;
            a.Agency_Short_Name__c = 'testShrName';
            a.Party_ID__c = '789456';
            a.Email__c = 'test@gmail.com';
            a.Mobile__c = '7894561230';
            a.Title__c = 'Ms.';
            a.Nationality__c = 'Australian';
            a.Passport_Number__c = '123654';
        insert a;
        NSIBPM__Service_Request__c servceReq = new NSIBPM__Service_Request__c();
            servceReq.Agency__c = a.id;
        insert servceReq;
        Booking__c bookng = new Booking__c ();
            bookng.Deal_SR__c = servceReq.id;
        insert bookng;
        ID recordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry Decrypted').getRecordTypeId();      
        Inquiry__c inq = new Inquiry__c ();
            inq.Mobile_phone__c = '7894561230';
            inq.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
            inq.Mobile_Phone_Encrypt__c = 'WERTYUIYRTYUIYRTYU';
            inq.Class__c = 'test';
            inq.Inquiry_Status__c = 'Active';
            inq.Inquiry_Source__c ='Agent Referral';  
            inq.RecordtypeId = recordTypeId;
            inq.Class__c = 'test';
            inq.Email__c = 'test@gmail.com';
            inq.Phone_CountryCode__c = 'United Arab Emirates: 00971';
            inq.Nationality__c = 'Australian';
        insert inq;
        System.assertEquals('Active',inq.Inquiry_Status__c );
        
        buyer__c buyer= new buyer__c ();
            buyer.account__c = a.id;
            buyer.Booking__c = bookng.id;
            buyer.Address_Line_1__c = 'stre';
            buyer.DOB__c = Date.Today ().addYears (-30);
        insert buyer;
        Set<String> mobNumSet = new Set<String> ();
            mobNumSet.add('7894561230');
        Set<String> emailSet = new Set<String> ();
            emailSet.add('test@gmail.com');
        PcConfirmationHelper pcConfirmHelper = new PcConfirmationHelper ();
        PcConfirmationHelper.checkForInquires (emailSet,mobNumSet);
        PcConfirmationHelper.checkForAccounts (emailSet,mobNumSet);
        PcConfirmationHelper.getAccBasedOnNationality('Australian','123654');
        PcConfirmationHelper.getAccountBasedOnNationality('Australian','123654','Individual');
         
        PcConfirmationHelper.mapPrimaryInquiry (inq.id);
        // PcConfirmationHelper.mapInquiryFields (new List<inquiry__c> {inq});
        PcConfirmationHelper.mapPrimaryAccount (a.id);
        PcConfirmationHelper.mapBuyerFields (new List<Account>{a});
        PcConfirmationHelper.mapBuyerFields1(new List<Account>{a});
        PcConfirmationHelper.mapBuyerFields2(new List<Account>{a});
        PcConfirmationHelper.validateDateOfBirth (System.today());
    }
}