/* * * * * * * * * * * * * *
*  Class Name:   AssignmentFetchUnitDetailsTest
*  Purpose:      Unit test class for AssignmentFetchUnitDetails Controller
*  Author:       Hardik Mehta - ESPL
*  Company:      ESPL
*  Created Date: 23-Nov-2017
*  Updated Date: 23-Nov-2017
*  Type:         Test Class
* * * * * * * * * * * * */
@isTest
private class AssignmentFetchUnitDetailsTest 
{

  public static final String strBookingUnitActiveStatus = 'Agreement executed by DAMAC';

  /* * * * * * * * * * * * *
  *  Method Name:  fetchUnitDetails_test
  *  Purpose:      This method is used to unit test functionality in fetchUnitDetails method
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 23-Nov-2017
  * * * * * * * * * * * * */
  static testMethod void fetchUnitDetails_test()
  {
    //insert sample data for Account
    Account objAccount = TestDataFactory_CRM.createPersonAccount();
    insert objAccount;

    List<Booking__c> bookingList = new List<Booking__c>();
    List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();

    //insert Booking Unit Active Status custom setting record
    insertBookingUnitActiveStatus();

    //create Deal SR record
    NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
    insert objDealSR;

    //create Booking record for above created Deal and Account
    bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
    insert bookingList;

    //create Property data
    Property__c objProperty = new Property__c();
    objProperty.Name = 'Test Project';
    objProperty.Property_ID__c = 3431;
    objProperty.CurrencyIsoCode = 'AED';
    insert objProperty;

    //create data for Inventory
    Inventory__c objInventory = new Inventory__c();
    objInventory.Property__c = objProperty.Id;
    objInventory.Unit__c = '1345';
    insert objInventory;

    //create Booking Units record for above created Bookings
    bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
    for(Booking_Unit__c objBookingUnit : bookingUnitList)
    {
      objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
      objBookingUnit.Inventory__c = objInventory.Id;
      objBookingUnit.Registration_ID__c = '74712';
    }
    insert bookingUnitList;

    Set<Id> bookingUnitIdSet = new Set<Id>();
    bookingUnitIdSet.add(bookingUnitList[0].Id);

    Test.startTest();

      AssignmentFetchUnitDetails.fetchUnitDetails(bookingUnitIdSet); 
    Test.stopTest();
  }

  /* * * * * * * * * * * * *
  *  Method Name:  insertBookingUnitActiveStatus
  *  Purpose:      This method is used to insert custom setting record for Booking Unit Active Status.
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 23-Nov-2017
  * * * * * * * * * * * * */
  private static void insertBookingUnitActiveStatus()
  {
    List<Booking_Unit_Active_Status__c> buActiveStatusList = new List<Booking_Unit_Active_Status__c>();

    Booking_Unit_Active_Status__c objBUActiveStatus = new Booking_Unit_Active_Status__c();
    objBUActiveStatus.Name = strBookingUnitActiveStatus;
    objBUActiveStatus.Status_Value__c = strBookingUnitActiveStatus;
    buActiveStatusList.add(objBUActiveStatus);

    insert buActiveStatusList;
  }
  
  
}