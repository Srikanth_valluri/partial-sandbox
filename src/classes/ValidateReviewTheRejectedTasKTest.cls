@isTest
public with sharing class ValidateReviewTheRejectedTasKTest {
   
    @isTest
    static void testmethod1(){
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Collection - CRE' LIMIT 1];
        
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
     	User usr = new User(Alias = 'standt', Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = profileId.Id,
        TimeZoneSidKey='America/Los_Angeles',
         UserName=uniqueUserName);
        
        System.runAs(usr){
            // Insert Accont
            Account objAcc = TestDataFactory_CRM.createPersonAccount();
            insert objAcc ;
            
            Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POP').getRecordTypeId();
            
            // insert Case
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
            objCase.Type = 'Change of Contact Details';
            objCase.IsPOA__c = false;
            objCase.OCR_verified__c = true;
            objCase.OQOOD_Fee_Applicable__c = false;
            objCase.Approval_Status__c = 'Approved' ;
            objCase.Status = 'Working';
            insert objCase;

            //Insert Service Request
            NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
            objSR.Agency__c = objAcc.Id ;
            insert objSR ;

            Id dealId = objSR.Deal_ID__c;
            //Insert Bookings
            List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id,1 );
            insert lstBookings ; 

            //Insert Booking Units
            List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings,1 );
            lstBookingUnits[0].Registration_ID__c = '1520';
            insert lstBookingUnits;

            List<SR_Booking_Unit__c> listSBU = new List<SR_Booking_Unit__c>();
            listSBU = TestDataFactory_CRM.createSRBookingUnis(objCase.ID,lstBookingUnits);
            insert listSBU;

            List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                            OnOffCheck__c = false);
            settingLst2.add(newSetting1);
            insert settingLst2;

            List<Calling_List__c> lstCallingList = new List<Calling_List__c>();
            lstCallingList = TestDataFactory_CRM.createCallingList('Collections_Calling_List', 1 ,lstBookingUnits[0] );
            lstCallingList[0].IsHideFromUI__c = false;
            lstCallingList[0].IsHideFalse_Relationship__c = objAcc.Id;
            lstCallingList[0].Account__c = objAcc.Id;
            lstCallingList[0].Booking_Unit__c = lstBookingUnits[0].Id;
            lstCallingList[0].Registration_ID__c = '1520';
            insert lstCallingList;
            System.debug('lstCallingList : '+ lstCallingList);
            
            Task objTask = new task();
            objTask.WhatId = objCase.id;
            objTask.subject = 'Review the rejected finance task';
            objTask.status = 'In progress';
            objTask.Process_Name__c = 'POP';
            insert objTask;
            
            Test.startTest();
            objTask.status = 'Completed';
            try{
                update objTask;
                ValidateReviewTheRejectedTasK.methodValidateReviewTheRejectedTask(objTask);
               }
            catch(Exception exceptionObject){
                System.assertEquals(
                    exceptionObject.getMessage().contains('Please call/email and update the notes cl no.  to proceed for task closer'),
                    true,
                    'This is the error added.'
                );
            }
            Test.stopTest();
        
        }   
    }
}