@isTest(SeeAllData=true)
public class Invocable_createParentSRStep_Test {
    static testMethod void test_EvaluateCustomCode_UseCase1(){
        
        NSIBPM__Service_Request__c  sSR1 = new NSIBPM__Service_Request__c ();
        insert sSR1;
        NSIBPM__Service_Request__c  sSR2 = new NSIBPM__Service_Request__c ();
        sSR2.NSIBPM__Parent_SR__c = sSR1.Id;
        insert sSR2;
        New_Step__c sStep = new New_Step__c();
        sStep.Service_Request__c = sSR2.Id;
        List<Id> stepIds = new List<Id>();
        stepIds.add(sStep.Id);
        Invocable_createParentSRStep.createParentSRStep(stepIds);
        Invocable_createParentSRStep.EvaluateCustomCode(sStep);
    }
    static testMethod void test_EvaluateCustomCode_UseCase2(){
        
        NSIBPM__Service_Request__c  sSR1 = new NSIBPM__Service_Request__c ();
        insert sSR1;
        New_Step__c sStep = new New_Step__c();
        sStep.Service_Request__c = sSR1.Id;
        List<Id> stepIds = new List<Id>();
        stepIds.add(sStep.Id);
        Invocable_createParentSRStep.createParentSRStep(stepIds);
        Invocable_createParentSRStep.EvaluateCustomCode(sStep);
    } 
}