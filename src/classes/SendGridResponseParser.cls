public class SendGridResponseParser {

    public String email;
    public String event;
    public String sg_event_id;
    public String sg_message_id;
    public String smtp_id;
    public Integer timestamp;
    public String uniqueId;
    public String ip;
    public String response;
    public String url;
    public Integer tls;

    
    public static List<SendGridResponseParser> parse(String json) {
        return (List<SendGridResponseParser>) System.JSON.deserialize(json, List<SendGridResponseParser>.class);
    }
}