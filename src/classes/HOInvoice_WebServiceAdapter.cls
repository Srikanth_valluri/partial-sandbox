public class HOInvoice_WebServiceAdapter extends GenericWebServiceAdapter implements WebServiceAdapter{

    public String url; 
    
    public override WebServiceAdapter call(){
        String regID_BUID = parameters.get('regID_BUID');
        system.debug('!!!!!!!!regID_BUID'+regID_BUID);
        String BUId, regID, StartDate;
        Datetime startTime;
        
        if(String.isNotBlank(regID_BUID)) {
            BUId = regID_BUID.substringBefore('-');
            regID = regID_BUID.substringAfter('-');
        }
        
        for (Booking_Unit__c objBU : [Select Id, Registration_DateTime__c From Booking_Unit__c Where Id =: BUId]) {
            startTime = objBU.Registration_DateTime__c;
        }
        if (startTime != null) {
            StartDate = startTime.format('dd-MMM-yyyy');
        }
        HO_DocGeneration.HO_DocResponse objHOInvoice =
            HO_DocGeneration.GetHOInvoice('HO_INVOICE' , regID , StartDate , Datetime.now().format('dd-MMM-yyyy'));
        System.debug('==objHOInvoice ===' + objHOInvoice );
        if(objHOInvoice != NULL && String.isNotBlank(objHOInvoice.URL)) {
            this.url = objHOInvoice.URL;
        }
        
        return this;
    }
    
    public override String getResponse(){
        return this.url;
    }
    
}