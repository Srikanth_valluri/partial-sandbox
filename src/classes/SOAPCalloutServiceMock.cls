@isTest 
global with sharing class SOAPCalloutServiceMock implements WebServiceMock {
    
    public static Map<String, Object> returnToMe;
    public integer intResponseNumber = 0 ;
    
    public SOAPCalloutServiceMock () {

    }

    public SOAPCalloutServiceMock ( integer intNum ) {
        intResponseNumber = intNum ;
    }
    
    global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
        
        if( intResponseNumber == 1 ) { // for class "GenerateHandoverChecklist"          
         
            HandoverChecklist1.HandOverCheckListResponse_element objRes = new HandoverChecklist1.HandOverCheckListResponse_element();
            objRes.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=626168 and Request Id :45271311 ...","ATTRIBUTE3":"626168","ATTRIBUTE2":"45271311","ATTRIBUTE1":"https://sftest.deeprootsurface.com/docs/e/XDCREG_DETAILS_45271250_1.PDF","PARAM_ID":"SFDC-74365","ATTRIBUTE4":null}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';
            response.put('response_x', objRes);  
        } 
        else if( intResponseNumber == 2 ) { // for class "GenerateLODTest"          
         
            HandoverMQServices.LetterofDischargeResponse_element objRes = new HandoverMQServices.LetterofDischargeResponse_element();
            objRes.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"File (Type,Size,Name) : PDF-157187-XDCREG_DETAILS_45271954_1.PDF","ATTRIBUTE3":null,"ATTRIBUTE2":null,"ATTRIBUTE1":"https://sf.deeprootsurface.com/docs/e/45271954_LOD.pdf","PARAM_ID":"74365","ATTRIBUTE4":null}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';
            response.put('response_x', objRes);  
        } 
        else{
            response.putAll(returnToMe);
        }
   }
}