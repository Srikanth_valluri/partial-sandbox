@isTest
private class HDApp_MoveOutSROperations_APITest {

    @isTest
    static void deleteMoveOutSRTestPositive(){
        
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'Test Unit Name',
                                      Registration_ID__c = '3901');
        insert bookingUnit;
        
        FM_Case__c objFMCase = new FM_Case__c();
        objFMCase.Request_Type__c = 'Move Out';
        objFMCase.Origin__c = 'Portal';
        objFMCase.isHelloDamacAppCase__c = true;
        objFMCase.Account__c = account.Id;
        objFMCase.Booking_unit__c = bookingUnit.Id;
        insert objFMCase;
        
        Test.startTest();
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/moveOutSrOps';  
            req.addParameter('fmCaseId',objFMCase.Id);
            req.httpMethod = 'DELETE';
            RestContext.request = req;
            RestContext.response = res;
            
            HDApp_MoveOutSROperations_API.deleteMoveOutSR();
            
        Test.stoptest();
        
    }

    @isTest
    static void deleteMoveOutSRTestNegative1(){
        
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'Test Unit Name',
                                      Registration_ID__c = '3901');
        insert bookingUnit;
        
        FM_Case__c objFMCase = new FM_Case__c();
        objFMCase.Request_Type__c = 'Move Out';
        objFMCase.Origin__c = 'Portal';
        objFMCase.isHelloDamacAppCase__c = true;
        objFMCase.Account__c = account.Id;
        objFMCase.Booking_unit__c = bookingUnit.Id;
        insert objFMCase;
        
        Test.startTest();
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/moveOutSrOps';  
            req.addParameter('fmCaseId','');
            req.httpMethod = 'DELETE';
            RestContext.request = req;
            RestContext.response = res;
            
            HDApp_MoveOutSROperations_API.deleteMoveOutSR();
            
        Test.stoptest();
        
    }

    @isTest
    static void deleteMoveOutSRTestNegative2(){
        
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'Test Unit Name',
                                      Registration_ID__c = '3901');
        insert bookingUnit;
        
        FM_Case__c objFMCase = new FM_Case__c();
        objFMCase.Request_Type__c = 'Move Out';
        objFMCase.Origin__c = 'Portal';
        objFMCase.isHelloDamacAppCase__c = true;
        objFMCase.Account__c = account.Id;
        objFMCase.Booking_unit__c = bookingUnit.Id;
        insert objFMCase;
        
        Test.startTest();
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/moveOutSrOps';  
            //req.addParameter('fmCaseId',objFMCase.Id);
            req.httpMethod = 'DELETE';
            RestContext.request = req;
            RestContext.response = res;
            
            HDApp_MoveOutSROperations_API.deleteMoveOutSR();
            
        Test.stoptest();
        
    }

    @isTest
    static void deleteMoveOutSRTestNegative3(){
        
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'Test Unit Name',
                                      Registration_ID__c = '3901');
        insert bookingUnit;
        
        FM_Case__c objFMCase = new FM_Case__c();
        objFMCase.Request_Type__c = 'Move Out';
        objFMCase.Origin__c = 'Portal';
        objFMCase.isHelloDamacAppCase__c = true;
        objFMCase.Account__c = account.Id;
        objFMCase.Booking_unit__c = bookingUnit.Id;
        insert objFMCase;
        
        Test.startTest();
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/moveOutSrOps';  
            req.addParameter('fmCaseId','1234');
            req.httpMethod = 'DELETE';
            RestContext.request = req;
            RestContext.response = res;
            
            HDApp_MoveOutSROperations_API.deleteMoveOutSR();
            
        Test.stoptest();
    }

/***********************************************************************************************/
    
    @isTest
    static void getMoveOutDraftSRPositive(){
        
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'Test Unit Name',
                                      Registration_ID__c = '3901');
        insert bookingUnit;
        
        FM_Case__c objFMCase = new FM_Case__c();
        objFMCase.Request_Type__c = 'Move Out';
        objFMCase.Origin__c = 'Portal';
        objFMCase.isHelloDamacAppCase__c = true;
        objFMCase.Account__c = account.Id;
        objFMCase.Booking_unit__c = bookingUnit.Id;
        insert objFMCase;
        
        Test.startTest();
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/moveOutSrOps';  
            req.addParameter('fmCaseId',objFMCase.Id);
            req.httpMethod = 'GET';
            RestContext.request = req;
            RestContext.response = res;
            
            HDApp_MoveOutSROperations_API.getMoveOutDraftSR();
            
        Test.stoptest();
        
    }

    @isTest
    static void getMoveOutDraftSRNegative1(){
        
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'Test Unit Name',
                                      Registration_ID__c = '3901');
        insert bookingUnit;
        
        FM_Case__c objFMCase = new FM_Case__c();
        objFMCase.Request_Type__c = 'Move Out';
        objFMCase.Origin__c = 'Portal';
        objFMCase.isHelloDamacAppCase__c = true;
        objFMCase.Account__c = account.Id;
        objFMCase.Booking_unit__c = bookingUnit.Id;
        insert objFMCase;
        
        Test.startTest();
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/moveOutSrOps';  
            req.addParameter('fmCaseId','');
            req.httpMethod = 'GET';
            RestContext.request = req;
            RestContext.response = res;
            
            HDApp_MoveOutSROperations_API.getMoveOutDraftSR();
            
        Test.stoptest();
        
    }

    @isTest
    static void getMoveOutDraftSRNegative2(){
        
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'Test Unit Name',
                                      Registration_ID__c = '3901');
        insert bookingUnit;
        
        FM_Case__c objFMCase = new FM_Case__c();
        objFMCase.Request_Type__c = 'Move Out';
        objFMCase.Origin__c = 'Portal';
        objFMCase.isHelloDamacAppCase__c = true;
        objFMCase.Account__c = account.Id;
        objFMCase.Booking_unit__c = bookingUnit.Id;
        insert objFMCase;
        
        Test.startTest();
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/moveOutSrOps';  
            //req.addParameter('fmCaseId',objFMCase.Id);
            req.httpMethod = 'GET';
            RestContext.request = req;
            RestContext.response = res;
            
            HDApp_MoveOutSROperations_API.getMoveOutDraftSR();
            
        Test.stoptest();
        
    }

    @isTest
    static void getMoveOutDraftSRNegative3(){
        
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;



        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'Test Unit Name',
                                      Registration_ID__c = '3901');
        insert bookingUnit;
        
        FM_Case__c objFMCase = new FM_Case__c();
        objFMCase.Request_Type__c = 'Move Out';
        objFMCase.Origin__c = 'Portal';
        objFMCase.isHelloDamacAppCase__c = true;
        objFMCase.Account__c = account.Id;
        objFMCase.Booking_unit__c = bookingUnit.Id;
        insert objFMCase;
        
        Test.startTest();
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/moveOutSrOps';  
            req.addParameter('fmCaseId','1234');
            req.httpMethod = 'GET';
            RestContext.request = req;
            RestContext.response = res;
            
            HDApp_MoveOutSROperations_API.getMoveOutDraftSR();
            
        Test.stoptest();
    }

}