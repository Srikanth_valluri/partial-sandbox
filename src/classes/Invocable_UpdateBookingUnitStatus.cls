/**************************************************************************************************
* Name               : Invocable_UpdateBookingUnitStatus
* Description        : Invocable Class to Update Booking Unit Status
* Test Class         : Invocable_UpdateBookingUnitStatus                        
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR               DATE             Comments
* 1.0                            16/06/2019         Created
* 1.1         QBurst             02/04/2020         SOAP to REST change in Webservice call
**************************************************************************************************/
public class Invocable_UpdateBookingUnitStatus  {
    @InvocableMethod
    public static void UpdateBookingUnitStatus(List<Id> stepId) {
        for (New_Step__c step :[SELECT Id, Service_Request__c, Step_Status__c, Step_Type__c,
                                    Service_Request__r.DP_ok__c, Service_Request__r.Doc_ok__c
                                FROM New_Step__c WHERE Id IN :stepId]) {
            evaluateCustomCode(step);
        }
    }

    public static String evaluateCustomCode(New_Step__c step) {
        String retStr = 'Success';
        List<Id> bookingIds = new List<Id>();
        try {
            String statusCode;
            List<IPMS_Code_Settings__c> ipmsCodeList = IPMS_Code_Settings__c.getAll().values();
            for(IPMS_Code_Settings__c code : ipmsCodeList ){
                if(step.Step_Status__c == code.Step_Status__c && step.Step_Type__c == code.Step_Type__c &&
                       ( !code.Check_on_Doc_OK__c || step.Service_Request__r.Doc_ok__c == code.Doc_OK__c) &&
                       ( !code.Check_on_DP_OK__c || step.Service_Request__r.DP_ok__c == code.DP_OK__c)){
                   statusCode = code.Status_Code__c;
                   break;
               }
            }
            if (statusCode == null) {
                statusCode = '';
            }
            for(Booking__c Booking : [SELECT Id, Deal_SR__r.DP_ok__c, Deal_SR__r.Doc_ok__c,
                                         (SELECT Id, Registration_Status__c FROM Booking_Units__r) 
                                     FROM Booking__c 
                                     WHERE Deal_SR__c =: step.Service_Request__c]) {
               // Invocable_UpdateBookingUnitStatus.updateBookingStatus(new List<Id>{Booking.Id}, statusCode);
               DAMAC_IPMS_PARTY_CREATION.statusUpdate(bookingIds, statusCode, step.Id); // 1.1
            }
            step.Booking_Unit_Status_Code__c = statusCode;
            update step;
        } catch (Exception e) {
            system.debug('exception line: ' + e.getLineNumber());
            system.debug('exception message: ' + e.getMessage());
            retStr = 'Error :' + e.getMessage() + '';
        }
        return retStr;
    }

    @future(callout=true)
    public static void updateBookingStatus(list<Id> bookingIds, String StatusCode){
        if (StatusCode.equalsIgnoreCase('empty')) StatusCode = '';
        if(Bookingids != null && BookingIds.size() > 0){
            IPMS_Registration_Status_Update.sendRegnUpdate(BookingIds, 'STATUS_UPDATE', StatusCode);
        }
    }
}