/***************************************************************************************************************
 * Description : Class to Send Bulk emails from BU
 *==============================================================================================================
 * Ver      Date-DD/MM/YYYY     Author              Modification
 *==============================================================================================================
 * 1.0      06/10/2019          Aishwarya Todkar    Initial Draft
 *
 * 1.1      13/11/2019          Aishwarya Todkar    Added more merge fields to replace  
 *
 * 2.0      23/04/2020          Aishwarya Todkar    1.Added Unit Active check in query
 *                                                  2.Added Record_Id_To_Test to test on single record
 *                                                  3.Added logic to send mail to tenant
****************************************************************************************************************/
global class BulkEmailSenderBatch_BU implements Database.Batchable<sObject>
                                                    , Database.Stateful
                                                    , Database.AllowsCallouts {

    //List of Sent emails to have activity history                                                    
    List<EmailMessage> lstEmails;
    
    //List of error logs
    List<Error_Log__c> lstErrorLogs;

    global Database.QueryLocator start(Database.BatchableContext BC){
        System.debug('-->> Inside start');
        Set<String> setApiNames = new Set<String>();
        setApiNames.add('Id');
        setApiNames.add('Booking__r.Account__r.Person_Business_Email__c');
        setApiNames.add('Send_Bulk_Email__c');
        setApiNames.add('Send_Tenant_Email__c');
        setApiNames.add('Tenant__c');
        setApiNames.add('Tenant__r.Person_Business_Email__c');
        setApiNames.add(' Booking__c');
        setApiNames.add('Booking__r.Account__c');

        for( Merge_Field_Mapping__mdt mdt : [ SELECT
                                                MasterLabel
                                                , Merge_Field__c
                                                , Process_Name__c
                                                , Field_API_Name__c
                                            FROM
                                                Merge_Field_Mapping__mdt
                                            WHERE
                                                Process_Name__c = 'BU Bulk Emails'] ) {
            setApiNames.add( mdt.Field_API_Name__c );
        }
        String strQuery = 'SELECT ';
            for( String apiName : setApiNames ) {
                strQuery += apiName + ' ,';
            }
            strQuery = strQuery.removeEnd( ',');
            strQuery = strQuery 
                    + ' FROM Booking_Unit__c'
                    + ' WHERE '
                    + ' ( ( Send_Bulk_Email__c = true '
                    + ' AND Booking__r.Account__c != null ' 
                    + ' AND Booking__r.Account__r.Person_Business_Email__c != null )'
                    + ' OR '
                    + '( Send_Tenant_Email__c = true '
                    + ' AND Tenant__c != null ' 
                    + ' AND Tenant__r.Person_Business_Email__c != null ) ) '
                    + ' AND Unit_Active__c != null'
                    + ' AND Unit_Active__c = \'Active\'';

                        /*Id, Booking__r.Account__c, Booking__r.Account__r.Person_Business_Email__c,'
                        + ' Tenant__c, Tenant__r.Person_Business_Email__c,Send_Tenant_Email__c, Send_Bulk_Email__c,'
                        + ' Unit_Name__c, District__c, Property_Country__c, FM_Outstanding_Amount__c,'
                        + ' Property_Name_Inventory__c, Booking__r.Account__r.Name, SurveyCx__c,'
                        + ' Beneficiary_Name__c, Beneficiary_Bank__c, Bank_Branch__c, Beneficiary_Number__c,'
                        + ' IBAN_Num__c, Bank_Swift__c, Primary_Buyer_s_Name__c, DLDN_Notice__c '*/
                        
        if( String.isNotBlank ( Label.Record_Id_To_Test ) 
        && Label.Record_Id_To_Test.toLowerCase().contains('on') 
        && String.isNotBlank( Label.Record_Id_To_Test.substringAfter('-') )
        && Label.Record_Id_To_Test.substringAfter('-').startsWith( 'a0x') 
        && !Test.isRunningTest() ) {
            strQuery = strQuery + ' AND Id = \'' + Label.Record_Id_To_Test.substringAfter('-') + '\'  ';
        }

        System.debug('-->> strQuery == ' + strQuery);
        return Database.getQueryLocator( strQuery );
    }

    global void execute( Database.BatchableContext BC, List<Booking_Unit__c> listBU ) {
        System.debug('-->> listBU: ' + listBU);
        System.debug('-->> listBU.size(): ' + listBU.size());

        
        if( listBU != null && listBU.size() > 0 ) {

            Set<String> setOfTemplates = new Set<String>();

            /*************************************************** Template for customer *******************************************************/
            List<Process_Email_Template__mdt> lstEmtForCx = [SELECT 
                                                                Process_Name__c
                                                                , Email_Template__c
                                                                , Nationality__c
                                                            FROM 
                                                                Process_Email_Template__mdt
                                                            WHERE 
                                                                Process_Name__c =: 'Bulk Emails From BU'
                                                            AND
                                                                Email_Template__c != null 
                                                            LIMIT 1];
            
            List<EmailTemplate> listTemplateForCx;
            if( lstEmtForCx != null && lstEmtForCx.size() > 0 ) {
                listTemplateForCx = [SELECT 
                                            Id
                                            , Subject
                                            , Body
                                            , HtmlValue
                                            , TemplateType
                                        FROM 
                                            EmailTemplate 
                                        WHERE 
                                            DeveloperName = : lstEmtForCx[0].Email_Template__c 
                                        LIMIT 1];
                
            }

            /*************************************************** Template for Tenant *******************************************************/
            List<Process_Email_Template__mdt> lstEmtForTenant = [SELECT 
                                                                    Process_Name__c
                                                                    , Email_Template__c
                                                                    , Nationality__c
                                                                FROM 
                                                                    Process_Email_Template__mdt
                                                                WHERE 
                                                                    Process_Name__c =: 'Bulk Emails From BU To Tenant'
                                                                AND
                                                                    Email_Template__c != null 
                                                                LIMIT 1];

            List<EmailTemplate> listTemplateForTenant;
            if( lstEmtForTenant != null && lstEmtForTenant.size() > 0 ) {
                listTemplateForTenant = [SELECT 
                                            Id
                                            , Subject
                                            , Body
                                            , HtmlValue
                                            , TemplateType
                                        FROM 
                                            EmailTemplate 
                                        WHERE 
                                            DeveloperName = : lstEmtForTenant[0].Email_Template__c 
                                        LIMIT 1];
                
            }

            System.debug('lstEmtForCx---'+ lstEmtForCx);
            System.debug('listTemplateForCx---'+ listTemplateForCx);
            System.debug('lstEmtForTenant---'+ lstEmtForTenant);
            System.debug('listTemplateForTenant---'+ listTemplateForTenant);
            
            if( listTemplateForCx != null || listTemplateForTenant != null ) {
                lstEmails = new List<EmailMessage>();
                for( Booking_Unit__c objBU : listBU ) {

                    //Call Method to send emails through sendgrid
                    if( objBU.Send_Bulk_Email__c
                    && objBU.Booking__r.Account__c != null
                    && objBU.Booking__r.Account__r.Person_Business_Email__c != null
                    && listTemplateForCx != null 
                    && listTemplateForCx.size() > 0 ) {

                        System.debug( ' mail to Customer --- ');
                        //Send mail to customer
                        EmailMessage mail = sendEmail( objBU
                                                        , objBU.Booking__r.Account__c
                                                        , listTemplateForCx[0]
                                                        , objBU.Booking__r.Account__r.Person_Business_Email__c );
                        if( mail != null )
                            lstEmails.add( mail );
                    }//End customer if
                    if( objBU.Send_Tenant_Email__c
                    && objBU.Tenant__c != null
                    && objBU.Tenant__r.Person_Business_Email__c != null
                    && listTemplateForTenant != null
                    && listTemplateForTenant.size() > 0 ) {
                        
                        System.debug( ' mail to tenant --- ');
                        //Send mail to tenant
                        EmailMessage mail = sendEmail( objBU
                                                        , objBU.Tenant__c
                                                        , listTemplateForTenant[0]
                                                        , objBU.Tenant__r.Person_Business_Email__c );
                        if( mail != null )
                            lstEmails.add( mail );
                    }// End tenant if                    
                }//END of for
                
                System.debug('lstEmails=== ' + lstEmails);
                if(lstEmails.size() > 0) {
                    if(!Test.isRunningTest()) {
                        insert lstEmails;
                    }
                }
            }//End of templates if
        }//End listBU if
    }

/****************************************************************************************************************
 * Method Name : sendEmail
 * Description : send emails by sendGrid.
 * Return Type : void
 * Parameter(s): Booking Unit, Email template
*****************************************************************************************************************/    
    EmailMessage sendEmail(Booking_Unit__c objBU, Id accountId, EmailTemplate template, String toAddress) {

        if( objBU != null && objBU.Booking__c != null && objBU.Booking__r.Account__c != null && template != null ) {

            String subject = String.isNotBlank(  template.subject ) ? template.subject : 'Test Bulk Email';
            String contentType = 'text/html';
            String fromAddress = Label.Bulk_Emails_From_Address;
            String bccAddress = Label.SF_Copy_EMail_Address;//
            String replyToAddress = Label.Bulk_Emails_Reply_To_Address;
            String strCCAddress = '';
            String contentValue = template.htmlValue != null ? replaceMergeFields( objBU, template.HtmlValue ) : '' ;
            String contentBody =  template.Body != null ? replaceMergeFields( objBU, template.Body ) : '';
            subject = template.Subject != null ? replaceMergeFields( objBU, template.Subject ) : '';
            if( String.isNotBlank( toAddress ) && String.isNotBlank( subject ) 
            && String.isNotBlank( contentValue ) && String.isNotBlank( fromAddress )) {
                
                try {
                    
                    //Sending emails through Sendgrid
                    SendGridEmailService.SendGridResponse objSendGridResponse = 
                        SendGridEmailService.sendEmailService( toAddress,''
                                                            , strCCAddress, ''
                                                            , bccAddress, ''
                                                            , subject, ''
                                                            , fromAddress, ''
                                                            , replyToAddress, ''
                                                            , contentType
                                                            , contentValue, '',
                                                            new List<Attachment>{});
                    
                    String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;
                    
                    if( responseStatus == 'Accepted' ) {
                        EmailMessage mail = new EmailMessage();
                        mail.Subject = subject;
                        mail.MessageDate = System.Today();
                        mail.Status = '3';//'Sent';
                        mail.RelatedToId = accountId;
                        mail.Account__c  = accountId;
                        mail.ToAddress = toAddress;
                        mail.FromAddress = fromAddress;
                        mail.TextBody = contentValue;//contentValue.replaceAll('\\<.*?\\>', '');
                        mail.Sent_By_Sendgrid__c = true;
                        mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                        mail.Booking_Unit__c = objBU.Id;
                        mail.CcAddress = strCCAddress;
                        mail.BccAddress = bccAddress;
                        system.debug('Mail obj == ' + mail);
                        return mail;
                    }//End response if
                }//End try
                catch( Exception e ) {

                    //Creating error logs
                    lstErrorLogs.add( GenericUtility.createErrorLog( e.getMessage(), accountId, objBU.Id, null, null) );
                }
            }//End toAddress if
        }//End objBU if
        return null;
    }//End SendEmail Method

/*********************************************************************************
 * Method Name : replaceMergeFields
 * Description : Replaces the merge fields with actual values.
 * Return Type : String
 * Parameter(s): Booking Unit, Email template Contents
**********************************************************************************/
    public String replaceMergeFields( Booking_Unit__c objBU, String strContents ) {
        
        if( objBU != null && String.isNotBlank( strContents ) ) {
            for( Merge_Field_Mapping__mdt mdt : [ SELECT
                                                    MasterLabel
                                                    , Merge_Field__c
                                                    , Process_Name__c
                                                    , Field_API_Name__c
                                                FROM
                                                    Merge_Field_Mapping__mdt
                                                WHERE
                                                    Process_Name__c = 'BU Bulk Emails'] ) {
                if( strContents.contains( mdt.Merge_Field__c ) ) {
                    String strVal = getStringValue( mdt.Field_API_Name__c, objBU);
                    
                    if( String.isNotBlank( strVal ) ) {
                        if( mdt.Field_API_Name__c.equalsIgnoreCase( '{!Customer_Name__c}' )) {
                            strVal = GenericUtility.getCamelCase( strVal );
                        }
                        strContents = strContents.replace( mdt.Merge_Field__c , strVal );
                    }
                    else {
                        strContents = strContents.replace( mdt.Merge_Field__c, '');
                    }
                }                                    
            }
        

            return strContents;
        }//End of blank contents if

        return null;
    }//End of replaceMergeFields

/**********************************************************************************************
Method Description  : Method to return String value from record
Parameters          : Api Names, sObject Instance
Return Type         : String
**********************************************************************************************/
    public static String getStringValue( String field_API_Name, Booking_Unit__c obj ) {
        
        if( field_API_Name.contains('.') ) {
            String strAfter = field_API_Name.subStringAfter('.');
            String relatedObject1 = field_API_Name.subStringBefore('.');
            if( strAfter.contains('.') ) {
                
                String relatedObject2 = strAfter.subStringBefore('.');
                strAfter = strAfter.subStringAfter('.');

                if( strAfter.contains('.') ) {

                    // 3 level parenting
                    String relatedObject3 = strAfter.subStringBefore('.');
                    String fieldOfRelatedObj = strAfter.subStringAfter('.');
                    System.debug('Parenting 3---');
                    System.debug('fieldOfRelatedObj-'+fieldOfRelatedObj);
                    System.debug('related expression--' + relatedObject1 + '.' + relatedObject2 + '.' + relatedObject3 + '.' + fieldOfRelatedObj );
                    return String.valueOf( obj.getSobject( relatedObject1 ).getSobject( relatedObject2 ).getSobject( relatedObject3 ).get( fieldOfRelatedObj ) );
                }
                else {

                    // 2 level parenting
                    String fieldOfRelatedObj = strAfter;
                    System.debug('Parenting 2---');
                    System.debug('fieldOfRelatedObj-'+fieldOfRelatedObj);
                    System.debug('related expression--' + relatedObject1 + '.' + relatedObject2 + '.' + fieldOfRelatedObj );
                    return String.valueOf( obj.getSobject( relatedObject1 ).getSobject( relatedObject2 ).get( fieldOfRelatedObj ) );
                }
            }
            else {

                // 1 level parenting
                System.debug('Parenting 1---');
                System.debug('fieldOfRelatedObj-' + strAfter);
                System.debug('related expression--' + relatedObject1 + '.' + strAfter );
                return String.valueOf( obj.getSobject( relatedObject1 ).get( strAfter ) );
            }
        }
        else {
            return String.valueOf( obj.get( field_API_Name ) );
        }
    }

    global void finish(Database.BatchableContext BC) {
        System.debug('-->> Finish ' );
    }
}