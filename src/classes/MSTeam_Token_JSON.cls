/************************************************************************************************
 * @Name              : MSTeam_Token_JSON
 * @Test Class Name   : DAMAC_MSTeamsIntegration_Test
 * @Description       : JSON Class for token response from Microsoft Graph API 
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         26/03/2020       Created
***********************************************************************************************/
public class MSTeam_Token_JSON{
    public String access_token; //o4yG77TqeaerQkCWKC7o1QaiygQ9
    public string refresh_token; //o4yG77TqeaerQkCWKC7o1QaiygQ9
    public String token_type; //Bearer
    public Integer expires_in;  //3600
    public Integer ext_expires_in; //3600
    public String Scope; 

    public static MSTeam_Token_JSON parse(String json){
        return (MSTeam_Token_JSON) System.JSON.deserialize(json, MSTeam_Token_JSON.class);
    }
}