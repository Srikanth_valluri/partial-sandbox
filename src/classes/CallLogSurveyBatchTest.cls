@isTest
Public Class CallLogSurveyBatchTest {
    
    @IsTest
    Public Static void  methodName() {
        Account objAcc = new Account( Name = 'Test'
                                , Mobile__c = '0009122323232'
                                , Email__c = 'test.232@t.com');
        insert objAcc;
        
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User usr = new User(Alias = 'he'
                            , Email='abc.123@test.com'
                            , EmailEncodingKey='UTF-8'
                            , LastName='Hi'
                            , LanguageLocaleKey='en_US' 
                            , LocaleSidKey='en_US'
                            , ProfileId = p.Id 
                            , TimeZoneSidKey='America/Los_Angeles'
                            , UserName='abc.123@test.com');
        insert usr;
        
        Call_Log__c objCall = new Call_Log__c( Account__c = objAcc.Id
                                            , Calling_Number__c = '009829392'
                                            , Called_Number__c = '009829391'
                                            , Call_Type__c = 'Inbound'
                                            , CRE__c = usr.Id
                                            , CRM_Survey_Sent__c = false);
        insert objCall;
        
        String csName = [SELECT Call_Type__c,CRE_Profile_Id__c from Call_log__c where Id=:objCall.id][0].CRE_Profile_Id__c;
        system.debug('Test Profile name -' + csName);
        //[Select profile.name from user where id =: usr.id][0].Profile.Name;
        CRE_Profiles__c objCS = new CRE_Profiles__c ( Process_Name__c='Call Log Survey');
        objCs.Name = csName;
        insert objCs;
        insert new Credentials_Details__c( Name = 'Rebrandly', Password__c = 'test', EndPoint__c = 'https://tt231dgsg.com');
        Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockCallingList(1));
        Test.setMock(HttpCalloutMock.class, new RebrandlyServiceHttpMock() );
        Test.startTest();
        Database.executeBatch( new CallLogSurveyBatch( ) );
        Test.stopTest();
        
    }
}