public with sharing class AP_AgentPortalMyCustomers {

    public string statusValue         { get; set;}
    public String agentIdVal          { get; set;}
    public string BUSStatusValue         { get; set;}
    public String BUSAgentIdVal          { get; set;}
    public String parentCategory {get; set;}
    public String videoSrc {get; set;}
    public String suCategoryName {get; set;}
    public List<String> parentCatList {get; set;}
    public String strSelectedLanguage  {set; get;}
    public List<Selectoption> listLanguages {set; get;}
    public static user usr{get; set;}
   
    public Map<String,String> subCategoryLabelAndNameMap {get; set;}
    public Map<String,String> mapOfTitleAndDocs {get; set;}
   // public Map<String,list<Test_Knowledge__kav>> subCatAndKnowArtMap {get; set;}
    public Map<String,List<myObject>> subCatAndKnowArtMap {get; set;}
    public List<myObject> myObjList {get; set;}
    public static  Map<String, List<String>> dataCategoriesAndSubCatMap {get; set;}
    public List<DataCategoryUtil.RootObject> dataCategoriesStructure {get; set;}
    public Set<String> parentCatName {get; set;}
    private static Set<String> articleFieldSets = new Set<String> {
        'Id','Title','Description__c',
        'KnowledgeArticleId','PublishStatus',
        'Language','CreatedDate'
    };
    
    //Constructor 
    public AP_AgentPortalMyCustomers() {
        usr = [select id, accountId, contactId, Profile.Name from user where id=: UserInfo.getUserId()];
        system.debug('usr '+usr );
        parentCatName = new Set<String>();
        parentCatList = new List<String>();
        listLanguages = new List<SelectOption>();
        //labelVal = apexpages.currentpage().getparameters().get('dataCategory');
        if( apexpages.currentPage().getParameters().get('langCode') != null){
            strSelectedLanguage = apexpages.currentPage().getParameters().get('langCode');
        }
        mapOfTitleAndDocs = new Map<String,String>();
        fetchSubCatAndArticlesOnLoad();
        Map<String,AP_AgentPortalDocs__c> agentPortalDocsMap = AP_AgentPortalDocs__c.getall();
        for(AP_AgentPortalDocs__c apDocs : agentPortalDocsMap.values()){
           
                mapOfTitleAndDocs.put(apDocs.Title__c,apDocs.Source__c);
                      
                
        }
        system.debug('>>>>>mapOfTitleAndDocs>>'+mapOfTitleAndDocs);

        /*for(AP_Languages__mdt objLanguage : [SELECT DeveloperName,
                                                    MasterLabel
                                            FROM AP_Languages__mdt])
        {
            if(objLanguage.MasterLabel != 'English') {
                listLanguages.add(new selectOption(objLanguage.DeveloperName,objLanguage.MasterLabel));
            }
        }
        listLanguages.add(0,new selectOption('en_US','  English'));*/
    }

    public List<SelectOption> getStatusValues() {
        List<SelectOption> statusList = new List<SelectOption>();
        statusList.add(new SelectOption('Status','Status'));
       Schema.DescribeFieldResult fieldResult = Inquiry__c.Inquiry_Status__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            statusList.add(new SelectOption(f.getValue(), f.getLabel())); 
        }       
        return statusList;
    }  
    
    public List<SelectOption> getBUSStatusValues() {
        List<SelectOption> busStatusList = new List<SelectOption>();
        busStatusList.add(new SelectOption('Status','Status'));
       Schema.DescribeFieldResult fieldResult = Bulk_Upload_Request__c.Status__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            busStatusList.add(new SelectOption(f.getValue(), f.getLabel())); 
        }       
        return busStatusList;
    }


    public List<SelectOption> getAgentsNameList() {
        Id inquiryRecordTypeId = 
            Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(DAMAC_Constants.CIL_RT).getRecordTypeId();
        Id inquiryRecordTypeId1 = 
            Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(DAMAC_Constants.INQUIRY_RT).getRecordTypeId();
        Id inquiryRecordTypeId3 = 
            Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(DAMAC_Constants.INQUIRY_PreInq_RT ).getRecordTypeId();
        List<SelectOption> agentsNameList = new List<SelectOption>();
        agentsNameList.add(new SelectOption('Agents','Agents'));
        Contact loginContact = AgentPortalUtilityQueryManager.getContactInformation();
        System.debug('...loginContact ...'+loginContact);
        
        //usr = [select id, accountId, contactId from user where id=: UserInfo.getUserId()];
        list<account> lstAccount = new list<account>();
        lstAccount = [SELECT id FROM Account WHERE Id =: usr.accountId];
        system.debug('lstAccount '+lstAccount );
        list<contact> lstContact = new list<contact>();
        lstContact = [SELECT id,createdby.id FROM contact WHERE accountId =:lstAccount[0].id];
        system.debug('lstContact '+lstContact);
        
        list<user> lstUser = new list<user>();
        lstUser = [select id from user where contactId in: lstContact];
        system.debug('lstUser'+lstUser);
        set<id> createdBySet = new set<id>();
        for (contact con: lstContact) {
            createdBySet.add(con.id);
        }
        system.debug('createdBySet'+createdBySet);
        list<id> lstId = new list<id>();
        for(id objId:createdBySet){
            lstId.add(objId);
        }
        system.debug('lstId'+lstId);
        //{
        //string setStr = createdBySet.removeEnd('}');
        //setStr = createdBySet.removestart('{');
        //}
        //system.debug('setStr'+setStr);
        List<Inquiry__c> inqCILLists = new List<Inquiry__c>();
        
        
        //listCustomerDetails = new List<CustomerDetails>();
        
        //if(null != loginContact && ( loginContact.Authorised_Signatory__c ||  loginContact.Owner__c || loginContact.Portal_Administrator__c)){
        if(usr.Profile.Name == 'Customer Community - Super User'){
            //super user
            Set<Id> userIds = AgentPortalUtilityQueryManager.getAllContact(loginContact.AccountID);      
            system.debug('***loadCILData entered - userIds'+userIds);
            string condition = 'AND Agent_Name__c IN :userIds';
            System.debug('...condition...'+condition+'...inquiryRecordTypeId...'+inquiryRecordTypeId+'...userIds...'+userIds);
            if(!Test.isRunningTest()) {
                String query = ' SELECT Id, First_Name__c, Agent_Name__r.Name,Agent_Name__c,Campaign_Name_Text__c, '
                             + ' Last_Name__c,Email__c,Mobile_CountryCode__c,Mobile_Phone_5__c, Inquiry_Status__c,Selected_Campaign_Name__c,'
                             + ' Passport_Number__c, Preferred_Language__c, Inquiry_Owner__c, '
                             + ' Name, Comments__c, CreatedDate, Mobile_Phone__c, Agency_Name__c '
                             + ' FROM Inquiry__c '
                             + ' WHERE Agent_Name__c IN :lstContact' //CreatedBy.Id IN :lstUser
                             + ' AND Agency_Name__c = \''
                             + loginContact.AccountId
                             + '\''
                             + ' AND (RecordTypeId = \''
                             + inquiryRecordTypeId
                             + '\'' 
                             + ' OR RecordTypeId = \''
                             + inquiryRecordTypeId1 
                             + '\''
                             + ' OR RecordTypeId = \''
                             + inquiryRecordTypeId3                             
                             +'\')';
                         
                 query = query + ' ORDER BY CreatedDate DESC ';
                System.debug('...query...'+query);
                inqCILLists =  Database.query(query);
            } else{
                inqCILLists = [Select Id, First_Name__c, Agent_Name__r.Name, Agent_Name__c, Campaign_Name_Text__c,
                            Last_Name__c,Email__c, Mobile_Phone__c, Comments__c, Name,Inquiry_Status__c,Selected_Campaign_Name__c,
                            Mobile_CountryCode__c, Mobile_Phone_5__c, CreatedDate, 
                            Passport_Number__c,Preferred_Language__c, Inquiry_Owner__c
                            FROM Inquiry__c WHERE createdby.id in: lstUser];
            }
            //AgentPortalUtilityQueryManager.getCILs(condition,inquiryRecordTypeId,userIds);
            
        }else{
            //not a super user
            String query = ' SELECT Id, First_Name__c, Agent_Name__r.Name,Agent_Name__c,Campaign_Name_Text__c, '
                             + ' Last_Name__c,Email__c,Mobile_CountryCode__c,Mobile_Phone_5__c, Inquiry_Status__c,Selected_Campaign_Name__c,'
                             + ' Passport_Number__c, Preferred_Language__c, Inquiry_Owner__c, '
                             + ' Name, Comments__c, CreatedDate, Mobile_Phone__c, Agency_Name__c '
                             + ' FROM Inquiry__c '
                             + ' WHERE Agent_Name__c = \''
                             + usr.ContactId
                             + '\''
                             + ' AND Agency_Name__c = \''
                             + loginContact.AccountId
                             + '\''
                             + ' AND (RecordTypeId = \''
                             + inquiryRecordTypeId
                             + '\'' 
                             + ' OR RecordTypeId = \''
                             + inquiryRecordTypeId1 
                             + '\''
                             + ' OR RecordTypeId = \''
                             + inquiryRecordTypeId3   
                             + '\')';
                         
                 query = query + ' ORDER BY CreatedDate DESC ';
                System.debug('...query...'+query);
                inqCILLists =  Database.query(query);
                 System.debug('inqCILLists >>>>'+inqCILLists );
        }
         system.debug('inqCILLists'+inqCILLists);
         for(Inquiry__c objInq : inqCILLists ) {
             if(objInq.Agent_Name__c != null && !agentsNameList.contains(new SelectOption(objInq.Agent_Name__c,objInq.Agent_Name__r.Name))){
                 agentsNameList.add(new SelectOption(objInq.Agent_Name__c,objInq.Agent_Name__r.Name));
             }
           
        }
        system.debug('agentsNameList'+agentsNameList);
        return agentsNameList;
    //}
    }

public List<SelectOption> getBUSAgentsNameList() {
        
        List<SelectOption> BUSAgentsNameList = new List<SelectOption>();
        BUSAgentsNameList.add(new SelectOption('Agents','Agents'));
        Contact loginContact = AgentPortalUtilityQueryManager.getContactInformation();
        System.debug('...loginContact ...'+loginContact);
        
        //usr = [select id, accountId, contactId from user where id=: UserInfo.getUserId()];
        List<Account> listAccount = new List<Account>();
        listAccount = [SELECT Id FROM Account WHERE Id =: usr.accountId];
        system.debug('listAccount '+listAccount );
        List<Contact> listContact = new List<Contact>();
        listContact = [SELECT Id,CreatedBy.Id FROM Contact WHERE AccountId =:listAccount[0].Id];
        system.debug('listContact'+listContact);
        
        List<user> listUser = new List<user>();
        listUser= [SELECT Id FROM User WHERE contactId in: listContact];
        system.debug('listUser'+listUser);
        set<Id> createdBySet = new set<Id>();
        for (Contact con: listContact) {
            createdBySet.add(con.Id);
        }
        system.debug('createdBySet'+createdBySet);
        list<Id> listId = new list<Id>();
        for(Id objId:createdBySet){
            listId.add(objId);
        }
        system.debug('listId'+listId);
        //{
        //string setStr = createdBySet.removeEnd('}');
        //setStr = createdBySet.removestart('{');
        //}
        //system.debug('setStr'+setStr);
        List<Bulk_Upload_Request__c> busObjLists = new List<Bulk_Upload_Request__c>();
        
        
        //listCustomerDetails = new List<CustomerDetails>();
        
        //if(null != loginContact && ( loginContact.Authorised_Signatory__c ||  loginContact.Owner__c || loginContact.Portal_Administrator__c)){
        if(usr.Profile.Name == 'Customer Community - Super User'){
            //super user
            Set<Id> userIds = AgentPortalUtilityQueryManager.getAllContact(loginContact.AccountID);      
            system.debug('***loadBUSData entered - userIds'+userIds);
            string condition = 'AND Agent__c IN :userIds';
            System.debug('...condition...'+condition+'...userIds...'+userIds);
            if(!Test.isRunningTest()) {
                 String query = ' SELECT Id, Name, Agency__c, Agent__c,Comments__c,Agent__r.Name, '
                         + ' Marketing_Campaign__c,RM_Email__c,Status__c,'                         
                         + ' CreatedDate '
                         + ' FROM Bulk_Upload_Request__c'
                         + ' WHERE Agent__c IN :listContact' 
                         + ' AND Agency__c = \''
                         + loginContact.AccountId
                         + '\'';                             
                         
                 query = query + ' ORDER BY CreatedDate DESC ';
                System.debug('...query...'+query);
                busObjLists =  Database.query(query);
            } else{
                busObjLists = [Select Id, Name, Agency__c, Agent__c,Comments__c,Agent__r.Name,
                            Marketing_Campaign__c,RM_Email__c,Status__c,
                            CreatedDate 
                            FROM Bulk_Upload_Request__c WHERE CreatedBy.Id in: listUser];
            }
            //AgentPortalUtilityQueryManager.getCILs(condition,inquiryRecordTypeId,userIds);
            
        }else{
            //not a super user
            String query = ' SELECT Id, Name, Agency__c, Agent__c,Comments__c,Agent__r.Name, '
                         + ' Marketing_Campaign__c,RM_Email__c,Status__c,'                         
                         + ' CreatedDate '
                         + ' FROM Bulk_Upload_Request__c'                         
                         + ' WHERE Agent__c = \''
                         + usr.ContactId
                         + '\''
                         + ' AND Agency__c= \''
                         + loginContact.AccountId
                         + '\'';
                         
                query = query + ' ORDER BY CreatedDate DESC ';
                System.debug('...query...'+query);
                busObjLists =  Database.query(query);
                System.debug('busObjLists >>>>'+busObjLists );
        }
         system.debug('busObjLists '+busObjLists );
         for(Bulk_Upload_Request__c objBUS : busObjLists) {
             if(objBUS .Agent__c != null && !BUSAgentsNameList.contains(new SelectOption(objBUS.Agent__c,objBUS.Agent__r.Name))){
                 BUSAgentsNameList.add(new SelectOption(objBUS.Agent__c,objBUS.Agent__r.Name));
             }
           
        }
        system.debug('BUSAgentsNameList'+BUSAgentsNameList);
        return BUSAgentsNameList;
    //}
    }






    /**
     * Method to fetch the List of CIl as per access
     */
    @RemoteAction
    public static CILDetailsWrapper getCILlst(String statusValue,String agentIdVal){
        System.debug('>statusValue>'+statusValue);
        System.debug('==agentIdVal=='+agentIdVal);

        Boolean conOwner = false;

        Id inquiryRecordTypeId = 
            Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(DAMAC_Constants.CIL_RT).getRecordTypeId();
        Id inquiryRecordTypeId1 = 
            Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(DAMAC_Constants.INQUIRY_RT).getRecordTypeId();
        Id inquiryRecordTypeId3 = 
            Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(DAMAC_Constants.INQUIRY_PreInq_RT ).getRecordTypeId();
        usr = [select id, accountId, contactId, Profile.Name from user where id=: UserInfo.getUserId()];
        Contact loginContact = AgentPortalUtilityQueryManager.getContactInformation();
        system.debug('loginContact >>'+loginContact );

        List<Inquiry__c> cilLists = new List<Inquiry__c>(); 
        
        //if( loginContact != null && ( loginContact.Authorised_Signatory__c || loginContact.Owner__c || loginContact.Portal_Administrator__c) ){
        if(usr.Profile.Name == 'Customer Community - Super User'){
            conOwner = true;
            Set<Id> userIds = AgentPortalUtilityQueryManager.getAllContact(loginContact.AccountId);   

            String condition = 'AND Agent_Name__c IN :userIds';
            String query = ' SELECT Id, First_Name__c, Agent_Name__r.Name,Agent_Name__c,Campaign_Name_Text__c, '
                         + ' Last_Name__c,Email__c,Mobile_CountryCode__c,Mobile_Phone_5__c, Inquiry_Status__c,Selected_Campaign_Name__c,'
                         + ' Passport_Number__c, Preferred_Language__c, Inquiry_Owner__c, '
                         + ' Name, Comments__c, CreatedDate, Mobile_Phone__c '
                         + ' FROM Inquiry__c '
                         + ' WHERE '
                         + '  Agency_Name__c = \''
                         + loginContact.AccountId
                         + '\''
                         + ' AND  (RecordTypeId = \''
                         + inquiryRecordTypeId 
                         + '\''
                         + ' OR RecordTypeId = \''
                         + inquiryRecordTypeId1 
                         + '\''
                         + ' OR RecordTypeId = \''
                         + inquiryRecordTypeId3   
                         + '\')';
            if(String.isNotBlank(statusValue) && ! statusValue.equals('Status') ){
                query = query 
                         + ' AND Inquiry_Status__c = \'' 
                        + statusValue 
                        + '\' ';
            } 
            if(String.isNotBlank(agentIdVal)  && !agentIdVal.equals('Agents') ){
                query = query 
                        + ' AND Agent_Name__c = \'' 
                        + agentIdVal 
                        + '\' ';
            }                 
            query = query + ' ORDER BY CreatedDate ';

            cilLists =  Database.query(query);
            system.debug('cilLists >>>'+cilLists );
            
        } else if(loginContact != null) {
            //usr = [select id, contactId from user where id=: UserInfo.getUserId()];
            system.debug('111'+usr);
            String condition = ' AND Agent_Name__c = \'' //CreatedById = \'' 
                             + usr.contactId //UserInfo.getUserId()
                             + '\'';
            if(String.isNotBlank(statusValue) && ! statusValue.equals('Status') ){
                    condition = condition 
                              + ' AND Inquiry_Status__c = \'' 
                              + statusValue 
                              + '\' ';
            } 

            String query = 'SELECT ';
      
            String condition2 ='';
            query += ' Id, First_Name__c, Agent_Name__r.Name , Agent_Name__c,Campaign_Name_Text__c, ' 
                   + ' Last_Name__c, Email__c, Mobile_CountryCode__c, Mobile_Phone__c, '
                   + ' Mobile_Phone_5__c, Passport_Number__c,Preferred_Language__c, Inquiry_Status__c,Selected_Campaign_Name__c,'
                   + ' Inquiry_Owner__c, Name, Comments__c, CreatedDate '
                   + ' FROM Inquiry__c WHERE (RecordTypeId = \''
                   + inquiryRecordTypeId 
                   + '\''
                   + ' OR RecordTypeId = \''
                   + inquiryRecordTypeId1 
                   + '\''
                   + ' OR RecordTypeId = \''
                   + inquiryRecordTypeId3  
                   + '\')'
                   + condition
                   + ' ORDER BY CreatedDate  ';
                   System.debug('Access query>>>'+query);
                
                 
            cilLists =  Database.query(query);
            System.debug('cilLists >>>>'+cilLists );
        }

        CILDetailsWrapper wrapperDetails = new CILDetailsWrapper( cilLists, conOwner);
        return wrapperDetails;
//}
    }
   public void fetchSubCatAndArticlesOnLoad() {
        dataCategoriesAndSubCatMap = new Map<String, List<String>>();
        subCatAndKnowArtMap = new Map<String,List<myObject>>();
        myObjList = new List<myObject>();
        parentCategory = Label.AP_FAQ;
        system.debug('>>>>>parentVal>>>'+parentCategory);
        system.debug('>>>>>managerIdWithInqCountmap>>>'+dataCategoriesAndSubCatMap);
        List<DataCategoryUtil.RootObject> dataCategoriesStructureDisplay = new List<DataCategoryUtil.RootObject>();
        dataCategoriesStructureDisplay = DataCategoryUtil.getDataCategoriesList();
        system.debug('>>>>>dataCategoriesStructureDisplay>>>'+dataCategoriesStructureDisplay);
        for(DataCategoryUtil.RootObject dataCategory : dataCategoriesStructureDisplay) {
            system.debug('>>>>>dataCategory.topCategories[0].childCategories>>>'+dataCategory.topCategories[0].childCategories);
          for( DataCategoryUtil.ChildCategory childCategory :
                dataCategory.topCategories[0].childCategories
            ) {
                if(dataCategoriesAndSubCatMap.containsKey(dataCategory.name)){
                    //subCategoryLab = dataCategoriesAndSubCatMap.get(dataCategory.name);
                    //subCategoryLab.add(childCategory.label);
                    dataCategoriesAndSubCatMap.get(dataCategory.name).add(childCategory.label);
                }else{
                    dataCategoriesAndSubCatMap.put(dataCategory.name,new List<String> {childCategory.label});
                }
            }
        }
        system.debug('>>>>>managerIdWithInqCountmap>>>'+dataCategoriesAndSubCatMap);

        //Map<Decimal,List<String>> maptitleAndId= new Map<Decimal,List<String>>();
        Set<id> knowledgeid = new Set<id>();
        List<AP_Knowledge__kav> knowledgeDetails = new List<AP_Knowledge__kav>();
         if( String.isNotBlank(parentCategory)){
            for(String childCategory :  dataCategoriesAndSubCatMap.get(parentCategory)){
                 system.debug('>>parentCategory>>>'+parentCategory);
                system.debug('>>childCategory>>>'+childCategory);
                String knowledgeType = 'CIL';
                String filters = AP_KnowledgeArticleController.createFilter();
                filters += 'AND Knowledge_Type__c=\''+ knowledgeType + '\' ' 
                    +' WITH DATA CATEGORY '
                    + String.escapeSingleQuotes(parentCategory)
                    + '__c ABOVE '
                    + String.escapeSingleQuotes(String.valueOf(childCategory))+'__c ';
                    system.debug('>>articleFieldSets>>>'+articleFieldSets);
                String queryForKnowledge = queryBuilder(
                                        'AP_Knowledge__kav',
                                        articleFieldSets,
                                        filters,
                                        null,
                                        null,
                                        null
                                    );
                 system.debug('>>>>queryForKnowledge>>>'+queryForKnowledge);
                knowledgeDetails = AP_KnowledgeArticleController.fetchRecords(queryForKnowledge);
                system.debug('>>>>knowledgeDetails>>>'+knowledgeDetails);

                for(AP_Knowledge__kav knowledge : knowledgeDetails) {
                        system.debug('>>>>knowledge>>>'+knowledge.Title);
                        system.debug('>>>>knowledge>>>'+knowledge.Description__c);
                        if(subCatAndKnowArtMap.containsKey(childCategory)){
                            knowledgeid.add(knowledge.KnowledgeArticleId);
                            system.debug('>>>>>knowledge.id>>>'+knowledge.id);
                            //system.debug('>>>>>knowledgeStat>>>'+knowledgeStat);
                            //subCategoryLab = dataCategoriesAndSubCatMap.get(dataCategory.name);
                            //subCategoryLab.add(childCategory.label);
                            subCatAndKnowArtMap.get(childCategory).add(new myObject(knowledge.KnowledgeArticleId,knowledge.Title, knowledge.Description__c,Date.valueOf(knowledge.CreatedDate),knowledge.createdBy.Name,knowledge.createdBy.FirstName,knowledge.createdBy.LastName,knowledge.createdBy.FullPhotoUrl)) ;
                            //myObjList.add(new myObject(knowledge.KnowledgeArticleId,knowledge.Title, knowledge.Description__c,Date.valueOf(knowledge.CreatedDate),knowledge.createdBy.Name,knowledge.createdBy.FirstName,knowledge.createdBy.LastName,knowledge.createdBy.FullPhotoUrl))
                        }else{
                            subCatAndKnowArtMap.put(childCategory,new List<myObject>{new myObject(knowledge.KnowledgeArticleId,knowledge.Title, knowledge.Description__c,Date.valueOf(knowledge.CreatedDate),knowledge.createdBy.Name,knowledge.createdBy.FirstName,knowledge.createdBy.LastName,knowledge.createdBy.FullPhotoUrl)});
                        }
                        system.debug('>>>>>subCatAndKnowArtMap==='+subCatAndKnowArtMap);

                    }
                    for(list<myObject> instance: subCatAndKnowArtMap.values()) {
                        myObjList.addAll(instance);
                    }
                    system.debug('>>>>>myObjList==='+myObjList);
                }
                system.debug('>>>>>knowledgeid.id>>>'+knowledgeid);
                List<KnowledgeArticleViewStat> knowledgeStat = [SELECT
                                                                    Id,NormalizedScore,Parent.Id,ViewCount
                                                                FROM KnowledgeArticleViewStat
                                                                WHERE  Parent.Id IN :knowledgeid AND Channel = 'App' ORDER BY
                                                                NormalizedScore Desc Limit 4];
                system.debug('>>>>>knowledgeStat>>>'+knowledgeStat);
            }else {
                ApexPages.addmessage(
                    new ApexPages.Message(ApexPages.Severity.Warning, 'Error')
                );
            }
            system.debug('>>>>>subCatAndKnowArtMap>>>'+subCatAndKnowArtMap);
    }
    public  String queryBuilder(
        String objectName,
        Set<String> fieldName,
        String filters,
        String sortExpression,
        Integer recordLimit,
        Integer recordOffset
    ) {
        system.debug('>>fieldName>>>'+fieldName);
        String fieldsNameFromSet = AP_KnowledgeArticleController.SetToString(fieldName);
        system.debug('>>fieldsNameFromSet>>>'+fieldsNameFromSet);
        fieldsNameFromSet = fieldsNameFromSet + ',CreatedDate,createdBy.Name,createdBy.firstname,createdBy.lastName,createdById,createdBy.FullPhotoUrl';
        system.debug('>>fieldsNameFromSetFinal>>>'+fieldsNameFromSet);
        return ('SELECT ' +
                fieldsNameFromSet +
                ' FROM ' +  objectName +
            (String.isNotBlank(filters) ?
                (' WHERE ' + filters  ) :''
            ) +
            (String.isNotBlank(sortExpression) ?
                (' ORDER BY ' +  sortExpression) :''
            ) +
            (recordLimit != null ?
                (' LIMIT ' + recordLimit) :''
            ) +
            (recordOffset != null ?
                (' OFFSET ' + recordOffset)
                 :''
                )
        );
    }


    //Wrapper Used for Remoting
    public class CILDetailsWrapper {
        List<Inquiry__c> customerDetails;
        Boolean conOwner;

        public CILDetailsWrapper(List<Inquiry__c> customerDetails, Boolean conOwner) {
            this.conOwner = conOwner;
            this.customerDetails = customerDetails;
        }
    }
    
    //Wrapper Used for Remoting
    public class BUSDetailsWrapper {
        List<Bulk_Upload_Request__c> BUSDetails;
        Boolean conOwner;
        List<Integer> totalLeadsUploadedCountList;
        List<Integer> MeetingsScheduledCountList;

        public BUSDetailsWrapper (List<Bulk_Upload_Request__c> BUSDetails, Boolean conOwner, List<Integer> totalLeadsUploadedCountList, List<Integer> MeetingsScheduledCountList) {
            this.conOwner = conOwner;
            this.BUSDetails = BUSDetails;
            this.totalLeadsUploadedCountList = totalLeadsUploadedCountList;
            this.MeetingsScheduledCountList = MeetingsScheduledCountList;
        }
    }
    
     public class myObject {
        public String knowledgeId { get; set; }
        public String title { get; set; }
        public String description { get; set; }
        public Date createdDate { get; set; }
        public String createdName { get; set; }
        public String firstName { get; set; }
        public String lastName { get; set; }
        public string photoURL {get;set;}
        public myObject(String knowledgeId,String title, String description,Date createdDate,String createdName,String firstName,String lastName, string photoURL) {
            this.knowledgeId = knowledgeId;
            this.title = title;
            this.description = description;
            this.createdDate = createdDate;
            this.createdName = createdName;
            this.firstName = firstName;
            this.lastName = lastName;
            this.photoURL = photoURL;
           
        }
     }
    //Wrapper for CIL Details
    /*public class CustomerDetails {
        public string Id;
        public string FirstName;
        public string LastName;
        public string Email;
        public string InquiryStatus;
        public string ContactInfo;
        public string CountryCode;
        public string MobileNo;
        public string Passport;
        public string OwnerID;
        public string InquiryNo;
        public string AgentName;
        public string CreatedDate;
        public string Comments;
        public string PreferredLanguage;
        public boolean DPOK;
        public boolean DOCOK;
    }*/

    /**
     * Method to get the List of Bulk Upload Requests as per access
     */
    @RemoteAction
    public static BUSDetailsWrapper getBUSlist(String BUSStatusValue,String BUSAgentIdVal){
        System.debug('>BUSStatusValue>'+BUSStatusValue);
        System.debug('==BUSAgentIdVal=='+BUSAgentIdVal);

        Boolean conOwner = false;

       List<Bulk_Upload_Request__c> BUSLists = new List<Bulk_Upload_Request__c>();  
        usr = [SELECT Id, AccountId, ContactId, Profile.Name FROM User WHERE Id =: UserInfo.getUserId()];
        Contact loginContact = AgentPortalUtilityQueryManager.getContactInformation();
        system.debug('loginContact >>'+loginContact );

       // List<Inquiry__c> BUSLists = new List<Inquiry__c>(); 
        
        //if( loginContact != null && ( loginContact.Authorised_Signatory__c || loginContact.Owner__c || loginContact.Portal_Administrator__c) ){
        if(usr.Profile.Name == 'Customer Community - Super User'){
            conOwner = true;
            Set<Id> userIds = AgentPortalUtilityQueryManager.getAllContact(loginContact.AccountId);   

            String condition = 'AND Agent__c IN :userIds';
            String query = ' SELECT Id, Name, Agency__c, Agent__c,Comments__c,Agent__r.Name,'
                         + ' Marketing_Campaign__c,RM_Email__c,Status__c,'                         
                         + ' CreatedDate '
                         + ' FROM Bulk_Upload_Request__c'
                         + ' WHERE '
                         + ' Agency__c = \''
                         + loginContact.AccountId
                         + '\'';
            if(String.isNotBlank(BUSStatusValue) && ! BUSStatusValue.equals('Status') ){
                query = query 
                         + ' AND Status__c = \'' 
                         + BUSStatusValue
                         + '\'';
            system.debug('BUS Status query >>>'+query );
            } 
            if(String.isNotBlank(BUSAgentIdVal)  && !BUSAgentIdVal.equals('Agents') ){
                query = query 
                        + ' AND Agent__c = \'' 
                        + BUSAgentIdVal
                        + '\'';
            system.debug('BUS Agent query >>>'+query );
            }                 
            query = query + ' ORDER BY CreatedDate ';
            system.debug('query >>>'+query );
            BUSLists =  Database.query(query);
            system.debug('BUSLists >>>'+BUSLists );
            
        } else if(loginContact != null) {
            //usr = [select id, contactId from user where id=: UserInfo.getUserId()];
            system.debug('111'+usr);
            String condition = ' WHERE Agent__c = \'' //CreatedById = \'' 
                             + usr.contactId //UserInfo.getUserId()
                             + '\'';
            if(String.isNotBlank(BUSStatusValue) && ! BUSStatusValue.equals('Status') ){
                    condition = condition 
                              + ' AND Status__c = \'' 
                              + BUSStatusValue
                              + '\' ';
            } 

            String query = '';
      
            String condition2 ='';
            query += ' SELECT Id, Name, Agency__c, Agent__c,Comments__c,Agent__r.Name,'
                         + ' Marketing_Campaign__c,RM_Email__c,Status__c,'                         
                         + ' CreatedDate '
                         + ' FROM Bulk_Upload_Request__c' 
                         + condition
                         + ' ORDER BY CreatedDate ';
                         
           System.debug('Access query>>>'+query);
                
                 
            BUSLists =  Database.query(query);
            
            System.debug('BUSLists >>>>'+BUSLists );
        }
        
        List<Integer> totalLeadsUploadedCountList = new List<Integer> ();
        List<Integer> MeetingsScheduledCountList = new List<Integer> ();
        
        Map<String, Integer> totalLeadsUploadedCountMap = new  Map<String, Integer>();
        Map<String, Integer> MeetingsScheduledCountMap = new  Map<String, Integer>();
        
        for(Bulk_Upload_Request__c uploadReq: BUSLists){
            totalLeadsUploadedCountMap.put(uploadReq.Name, 0);    
        }
        for(Inquiry__c inq: [SELECT Id, Dataload_Idetifier__c FROM Inquiry__c WHERE Dataload_Idetifier__c IN: totalLeadsUploadedCountMap.keyset() AND RecordTypeName__c = 'Pre Inquiry']){
            Integer count = 0;
            if(totalLeadsUploadedCountMap.containsKey(inq.Dataload_Idetifier__c)){
                count = totalLeadsUploadedCountMap.get(inq.Dataload_Idetifier__c);
            }
            count++;
            totalLeadsUploadedCountMap.put(inq.Dataload_Idetifier__c, count);
        }
        
        for(Bulk_Upload_Request__c uploadReq: BUSLists){
            if(totalLeadsUploadedCountMap.containsKey(uploadReq.Name)){
                totalLeadsUploadedCountList.add(totalLeadsUploadedCountMap.get(uploadReq.Name));
            } else{
                totalLeadsUploadedCountList.add(0);
            }
        }
        
        // meeting Scheduled count
        for(Bulk_Upload_Request__c meetingReq: BUSLists){
            MeetingsScheduledCountMap.put(meetingReq.Name, 0);    
        }
        for(Inquiry__c inq: [SELECT Id, Dataload_Idetifier__c FROM Inquiry__c WHERE Dataload_Idetifier__c IN: MeetingsScheduledCountMap.keyset() AND Inquiry_Status__c = 'Meeting Scheduled' AND RecordTypeName__c = 'Pre Inquiry']){
            Integer meetingCount = 0;
            if(MeetingsScheduledCountMap.containsKey(inq.Dataload_Idetifier__c)){
                meetingCount = MeetingsScheduledCountMap.get(inq.Dataload_Idetifier__c);
            }
            meetingCount++;
            MeetingsScheduledCountMap.put(inq.Dataload_Idetifier__c, meetingCount);
        }
        
        for(Bulk_Upload_Request__c meetingReq: BUSLists){
            if(MeetingsScheduledCountMap.containsKey(meetingReq.Name)){
                MeetingsScheduledCountList.add(MeetingsScheduledCountMap.get(meetingReq.Name));
            } else{
                MeetingsScheduledCountList.add(0);
            }
        }
        
        
        
        BUSDetailsWrapper wrapperDetails = new BUSDetailsWrapper( BUSLists , conOwner, totalLeadsUploadedCountList, MeetingsScheduledCountList);
        return wrapperDetails;
//}
    }



}