/****************************************************************************************************
* Name          : FetchProjectDetailsRelatedToIdWebService                                          *
* Description   : Class to display Property Computation records                                     *
* Created Date  : 26-08-2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER     AUTHOR            DATE            COMMENTS                                                *
* 1.0     Mohit Khurana     26-08-2018      Initial Draft.                                          *
* 1.1     Nikhil Pote       02-09-2018      Update Include parking                                  *
****************************************************************************************************/
@RestResource(urlMapping='/fetchPropertyDetails/*')
global class FetchProjectDetailsRelatedToIdWebService {
    @HttpGet
    global static List<ResponseResult> getProjectsDetails() {
        String projectId = RestContext.request.params.get('projectId');
        system.debug('projectId----------------'+projectId);
        system.debug('projectId----------'+decimal.valueOf(projectId));
        List<Property__c> properties = [ SELECT  Id,Name,Property_ID__c,Property_Code__c,Property_Name__c,
                                                RERA_Project_Number__c,Latitude__c,Longitude__c,
                                                Property_Plan__c, Description__c,Property_Country__c,
                                                (  
                                                    SELECT  Id,unit_type__c,Parking__c,View_Type__c,Property_Status__c,Rera_Percentage__c,Price_Per_Sqft__c,
                                                            Marketing_Name__c,Plot_Area_sft__c,Special_Price__c,Special_Price_Tax_Amount__c,
                                                            Anticipated_Completion_Date__c ,Marketing_Name_Doc__r.Primary_Image__c,Property__r.Property_Plan_Label__c
                                                            ,Marketing_Name_Doc__r.Marketing_Plan__c,Use_Custom_Floor_Plan__c,Custom_Floor_Plan__c ,Floor_Plan__c
                                                            ,Use_Custom_Unit_Plan__c,Custom_Unit_Plan__c ,Unit_Plan__c ,Unit_Location__r.Name,Bedroom_Type__c,
                                                            Use_Custom_Plot_Plan__c,Custom_Plot_Plan__c ,Plot_Plan__c,IPMS_Bedrooms__c,
                                                            Property__r.Property_Plan__c,Marketing_Name_Doc__r.Marketing_Plan_Label__c ,
                                                            Area_sft__c 
                                                    FROM Inventories__r 
                                                    WHERE  Status__c = 'Released'  
                                                ) 
                                        FROM Property__c 
                                        WHERE 
                                            Property_ID__c =: decimal.valueOf(projectId)
                                        LIMIT 2000
                                    ];

        
        system.debug('properties'+properties);
        List<ResponseResult> resultList = new List<ResponseResult>();
        system.debug('properties'+properties);
        for(Property__c objProp : properties) {
            system.debug('objProp.Inventories__r------------------------------'+objProp.Inventories__r);
            for(Inventory__c invObj :objProp.Inventories__r){
                List<Inventory__c> inventoriesList = new List<Inventory__c>();
                inventoriesList.addAll(objProp.Inventories__r);     
                String includeParking =  invObj.Parking__c; 
                system.debug('includeParking'+includeParking);        
                String unitType =  invObj.unit_type__c;
                String unitLocName = invObj.Unit_Location__r.Name;
                String bedroomType = invObj.Bedroom_Type__c;
                String ipmsBedrooms =  invObj.IPMS_Bedrooms__c;
                String viewType =  invObj.View_Type__c;
                String propertyStatus =  invObj.Property_Status__c;
                String reraPercentage =  invObj.Rera_Percentage__c;
                Decimal pricePerSqFeet =  invObj.Price_Per_Sqft__c;
                Decimal plotArPerSqFeet = invObj.Plot_Area_sft__c;
                Decimal areaInSqFt = invObj.Area_sft__c;
                Decimal specialPrice =  invObj.Special_Price__c;
                Decimal specialPriceTax = invObj.Special_Price_Tax_Amount__c;
                String marketingPlan = invObj.Marketing_Name_Doc__r.Primary_Image__c?invObj.Property__r.Property_Plan__c:invObj.Marketing_Name_Doc__r.Marketing_Plan__c;
                String floorPlan =  invObj.Use_Custom_Floor_Plan__c?invObj.Custom_Floor_Plan__c:invObj.Floor_Plan__c;
                String unitPlan = invObj.Use_Custom_Unit_Plan__c?invObj.Custom_Unit_Plan__c:invObj.Unit_Plan__c;
                String plotPlan =  invObj.Use_Custom_Plot_Plan__c?invObj.Custom_Plot_Plan__c:invObj.Plot_Plan__c;
                String anticipatedCompletionDate =  invObj.Anticipated_Completion_Date__c;
                resultList.add(
                    new ResponseResult ( 
                                        objProp.Name,
                                        objProp.Property_Name__c,
                                        unitLocName,
                                        bedroomType,
                                        ipmsBedrooms,
                                        unitType,
                                        includeParking,
                                        viewType,
                                        propertyStatus,
                                        reraPercentage,
                                        pricePerSqFeet,
                                        plotArPerSqFeet,
                                        areaInSqFt,
                                        specialPrice,
                                        specialPriceTax,
                                        anticipatedCompletionDate,
                                        marketingPlan,
                                        floorPlan,
                                        unitPlan,
                                        plotPlan
                                    )
                );
            }
            
        }
        system.debug('resultList'+resultList);
        return resultList;
    }
    //Wrapper Class for JSON Response
    global class ResponseResult {
        public String PROJECT_NAME;
        public String PROPERTY_NAME;
        public String UNIT_LOC_NAME;
        public String BEDROOM_TYPE;
        public String IPMS_BEDROOMS;
        public String UNIT_TYPE;
        public String INCLUDE_PARKING;
        public String VIEW_TYPE;
        public String PROPERTY_STATUS;
        public String RERA_PERCENTAGE;
        public Decimal PRICE_PERSQFT;
        public Decimal PLOTAREA_PERSQFT;
        public Decimal AREA_IN_SQFT;
        public Decimal SPECIAL_PRICE;
        public Decimal SPECIAL_PRICE_TAX;
        public String HANDOVER_DATE;
        public String MARKETING_PLAN;
        public String FLOOR_PLAN;
        public String UNIT_PLAN;
        public String PLOT_PLAN;
        global ResponseResult( 
                            String PROJECT_NAME,
                            String PROPERTY_NAME,
                            String UNIT_LOC_NAME, 
                            String BEDROOM_TYPE,
                            String IPMS_BEDROOMS,
                            String UNIT_TYPE,
                            String INCLUDE_PARKING,
                            String VIEW_TYPE,
                            String PROPERTY_STATUS,
                            String RERA_PERCENTAGE,
                            Decimal PRICE_PERSQFT,
                            Decimal PLOTAREA_PERSQFT,
                            Decimal AREA_IN_SQFT,
                            Decimal SPECIAL_PRICE,
                            Decimal SPECIAL_PRICE_TAX,
                            String HANDOVER_DATE,
                            String MARKETING_PLAN,
                            String FLOOR_PLAN,
                            String UNIT_PLAN,
                            String PLOT_PLAN
        ) {
            this.PROJECT_NAME = PROJECT_NAME;
            this.PROPERTY_NAME = PROPERTY_NAME;
            this.UNIT_LOC_NAME = UNIT_LOC_NAME;
            this.BEDROOM_TYPE = BEDROOM_TYPE;
            this.IPMS_BEDROOMS = IPMS_BEDROOMS;
            this.UNIT_TYPE = UNIT_TYPE;
            this.INCLUDE_PARKING = INCLUDE_PARKING;
            this.VIEW_TYPE = VIEW_TYPE;
            this.PROPERTY_STATUS = PROPERTY_STATUS;
            this.RERA_PERCENTAGE = RERA_PERCENTAGE;
            this.PRICE_PERSQFT = PRICE_PERSQFT;
            this.PLOTAREA_PERSQFT = PLOTAREA_PERSQFT;
            this.HANDOVER_DATE = HANDOVER_DATE;
            this.SPECIAL_PRICE = SPECIAL_PRICE;
            this.SPECIAL_PRICE_TAX = SPECIAL_PRICE_TAX;
            this.MARKETING_PLAN = MARKETING_PLAN;
            this.FLOOR_PLAN = FLOOR_PLAN;
            this.UNIT_PLAN = UNIT_PLAN;
            this.PLOT_PLAN = PLOT_PLAN;
            this.AREA_IN_SQFT = AREA_IN_SQFT;
        }
    }
    ///services/apexrest/fetchPropertyDetails
}