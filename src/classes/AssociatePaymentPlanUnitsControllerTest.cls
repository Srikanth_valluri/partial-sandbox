@isTest
public class AssociatePaymentPlanUnitsControllerTest {
    @testSetup
    public static void InsertData(){
        Location__c loc = new Location__c();
        loc.Name = 'AS';
        loc.Location_Type__c = 'Floor';
        loc.Location_Code__c = 'AS';
        loc.Building_Name__c = 'AS1';
        loc.Location_ID__c = '133';
        insert loc;
        Location__c locUnit = new Location__c();
        locUnit.Name = 'AS1';
        locUnit.Location_Code__c = 'AS1';
        locUnit.Building_Name__c = 'AS2';
        locUnit.Location_Type__c = 'Floor';
        locUnit.Floor_Number__c = loc.Id;
        locUnit.Location_ID__c = '134';
        locUnit.RecordTypeId = '0120Y000000DniDQAS';
        insert locUnit;
        Payment_Plan__c plan = new Payment_Plan__c();
        plan.Building_ID__c = '123';
        plan.Payment_Term_Description__c = 'Test Desc';
        plan.Effective_From__c = System.today();
        plan.Effective_To__c = System.today()+1;
        plan.Building_Location__c = locUnit.Id;
        insert plan;
        Payment_Terms__c terms= new Payment_Terms__c();
        terms.Installment__c='DP';
        terms.Description__c='Test Desc';
        terms.Milestone_Event__c='Imeediate';
        terms.Percent_Value__c='10.00';
        terms.Payment_Plan__c=plan.Id;
        insert terms;
        Inventory__c inv = new Inventory__c();
        inv.Status__c = 'Inventory';
        inv.Unit_Location__c = locUnit.id;
        inv.Plot_Area__c = '100.10';
        inv.Terrace_Area__c = '100';
        inv.Balcony_Area__c = '100';
        inv.Gross_Area__c = '1';
        inv.Garage_Area__c = 10;
        inv.Price__c = '100';
        insert inv;
        
        
    }
    
 	@isTest
    public static void TestMethod1(){
        Location__c loc = [SELECT id FROM Location__c WHERE Name=: 'AS'];
        Test.startTest();
        PageReference pageRef = Page.associatePaymentPlan_Units;
      	Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',loc.Id);
        AssociatePaymentPlanUnitsController controller = new AssociatePaymentPlanUnitsController();
               
        Test.stopTest();
        
    }

}