@isTest
public class CLRebateCalculatorHandler_Test {
    
    @isTest
    private static void CLRebateCalculatorHandlerTest() {
        insert new Credentials_Details__c (
            SetupOwnerId = UserInfo.getOrganizationId(),
            Name = 'Rebate Calculator',
            Endpoint__c= 'http://83.111.194.181:8033/webservices/rest/XXDC_PROCESS_SERVICE_WS/retrieve/',
            User_Name__c= 'oracle_user',
            Password__c = 'crp1user' 
        );
        
     // Create common test accounts
            Account objAcc = TestDataFactory_CRM.createPersonAccount();
            objAcc.Email__c ='test@test.com';
            objAcc.Mobile_Phone_Encrypt__pc = '0097405883798';
            insert objAcc ;
    
    //Insert Service Request
         NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
         objSR.Agency__c = objAcc.Id ;
         insert objSR ;
 
         Id dealId = objSR.Deal_ID__c;
         //Insert Bookings
         List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id,1 );
         insert lstBookings ; 
 
         //Insert Booking Units
         List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings,1 );
         lstBookingUnits[0].Registration_ID__c = '1520';
         insert lstBookingUnits;
         
           
         FmIpmsRestServices.DueInvoicesResponse invoicesResponse = new FmIpmsRestServices.DueInvoicesResponse();
        invoicesResponse.OutputParameters = new FmIpmsRestServices.OutputParameters();
        invoicesResponse.OutputParameters.X_RETURN_STATUS = 'S';
        invoicesResponse.OutputParameters.X_RETURN_MESSAGE = 'Total Records Fetched...1';
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE = new FmIpmsRestServices.ResponseMessage();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM = new List<Map<String, String>>();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM.add(
            new Map<String, String> {
                'ATTRIBUTE1' => '1520',
                'ATTRIBUTE2' => 'unitName',
                'ATTRIBUTE3' => 'projectName',
                'ATTRIBUTE4' => 'customerId',
                'ATTRIBUTE5' => 'orgId',
                'ATTRIBUTE6' => 'partyId',
                'ATTRIBUTE7' => 'partyName'
               
            }
        );
         
         Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(invoicesResponse)));

        Test.startTest();
        CLRebateCalculatorHandler rcObj = new CLRebateCalculatorHandler();
        rcObj.getDetails();
        rcObj.calculateRebateAmount();
        Test.stopTest();
}
    
    
    /*private class RestMock implements HttpCalloutMock {

        public HTTPResponse respond(HTTPRequest req) {
            String fullJson = '';

            HTTPResponse res = new HTTPResponse();
            res.setHeader('Content-Type', 'text/json');
            res.setBody(fullJson);
            res.setStatusCode(200);
            return res;
        }
    }
    
    @isTest
    private static void RebateCalculatorHandlerTest() {
        
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        // Create common test accounts
        Account objAcc = new Account(RecordTypeId = personAccRTId, Name = 'Test Name' ,First_Name__c='Test FirstName', Last_Name__c='Test LastName', Type='Other',Country__c = 'Jordan');
        insert objAcc ;
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        objSR.Agency__c = objAcc.Id ;
        insert objSR ;

        Id dealId = objSR.Deal_ID__c;
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id,1 );
        insert lstBookings ; 
        
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings,1 );
        lstBookingUnits[0].Registration_ID__c = '85651';
        lstBookingUnits[0].Requested_Price__c = 1000;
        lstBookingUnits[0].Area__c = 100;
        lstBookingUnits[0].Registration_Status_Code__c = 'WW';
        //lstBookingUnits[0].Registration_DateTime__c = lastYearDate;
        lstBookingUnits[0].Unit_Name__c = 'Test';
        
        Test.setMock(HttpCalloutMock.class, new RestMock());
        Test.startTest();
        insert lstBookingUnits;        
        RebateCalculatorHandler rcObj = new RebateCalculatorHandler();
        //rcObj.getDetails();        
        Test.stopTest();
    }*/
}