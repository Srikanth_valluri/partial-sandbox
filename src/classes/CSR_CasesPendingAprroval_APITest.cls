@isTest
public class CSR_CasesPendingAprroval_APITest {
    @isTest
    static void  testGetPendingCasesCountAPI() {
        String baseUrl = URL.getOrgDomainUrl().toExternalForm(); /* URL.getSalesforceBaseUrl() */

        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(alias = 'test456', email='xyz1@email.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = adminProfile, country='United Arab Emirates',IsActive =true
                ,timezonesidkey='America/Los_Angeles', username='xyz1@email.com',UserRoleId = userRoleObj.Id);
        System.RunAs(adminUser) {
            // User Insert 
            User pcUser = InitialiseTestData.getPropertyConsultantUsers('userABC@test.com');
            insert pcUser;
            
            // Create common test accounts
            Account objAcc = TestDataFactory_CRM.createPersonAccount();
            objAcc.Email__c ='test@test.com';
            objAcc.Mobile_Phone_Encrypt__pc = '0097405883798';
            insert objAcc ;
            
            Test.startTest();
            
            RestRequest request = new RestRequest();
            request.requestUri = baseUrl + '/services/apexrest/csrCase/pending';
            request.httpMethod = 'GET';
            
            RestResponse response = new RestResponse();
            
            RestContext.request = request;
            RestContext.response = response;
            
            CSR_CasesPendingAprroval_API.doGet(); /* 1 */
            
            request.addParameter('approver_email', 'invalid@test.com');
            CSR_CasesPendingAprroval_API.doGet(); /* 2 */
            
            request.addParameter('approver_email', 'userABC@test.com');
            CSR_CasesPendingAprroval_API.doGet(); /* 3 */
            
            Id recTypeCSR = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Case Summary - Client Relation').getRecordTypeId();
            System.debug('recTypeCOD :'+ recTypeCSR);
            // insert CSR Case
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCSR);
            objCase.Approving_User_Id__c = pcUser.id;
            objCase.Approving_User_Role__c = 'Manager';
            objCase.status = 'Submitted';
            insert objCase;
            
            CSR_CasesPendingAprroval_API.doGet(); /* 4 */
            
            Test.stopTest();
        } /* run as admin */
    }
}