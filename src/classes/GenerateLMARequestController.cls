public with sharing class GenerateLMARequestController {
    public String caseId;
    public Case objC;
    public GenerateLMARequestController(ApexPages.StandardController stdCon){
        caseId = stdCon.getId();
        objC = [Select Id
                     , Booking_Unit__c                     
                     , Registration_ID__c
                     , Handover__c
                     , Call_Outcome__c
                     , RGS_Option__c
                from Case 
                where Id =: caseId];
    }
    
    public void callDrawloop(){
        executeBatch();
    }
    
    public pageReference returnToCase(){
        PageReference Pg = new PageReference('/'+caseId);
        return pg;
    }
    
    public void executeBatch(){
        if(String.isNotBlank(objC.Registration_ID__c)){
            GenerateDrawloopDocumentBatch objInstance;
            if (objC.Handover__c == true || 
                (objC.Call_Outcome__c == 'Normal Handed over but without Rental Guarantee'
                || objC.Call_Outcome__c == 'Normal Handed over but with Rental Guarantee'
                || objC.RGS_Option__c == 'Option 1 – Opted out of RGS')){
                if (objC.RGS_Option__c == 'Option 2 – In RGS') {
                    objInstance =
                        new GenerateDrawloopDocumentBatch(caseId
                            , System.Label.LMA_Request_DDP
                            , System.Label.LMA_Request_Template);
                    Id batchId = Database.ExecuteBatch(objInstance);
                    if(String.valueOf(batchId) != '000000000000000'){
                        SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
                         objCaseAttachment.Case__c = caseId ;
                         objCaseAttachment.Name = 'Signed LMA Request';
                         objCaseAttachment.Booking_Unit__c = objC.Booking_Unit__c;
                         insert objCaseAttachment;
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Your request for LMA Request was successfully submitted. Please check the documents section for the document in a while.');
                        ApexPages.addMessage(myMsg);
                    }else{
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Your request for LMA Request could not be completed. Please try again later.');
                        ApexPages.addMessage(myMsg);
                    }
                } else {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Request document is not required.');
                    ApexPages.addMessage(myMsg);
                }
            } else {
                ApexPages.Message myMsg = 
                    new ApexPages.Message(
                        ApexPages.Severity.ERROR,
                        'Finance Task needs to be comleted before generating Request.');
                ApexPages.addMessage(myMsg);
            }
        }else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Regid for the new unit has not been generated yet. Please try again later.');
            ApexPages.addMessage(myMsg);
        }
    }
}