public without sharing class FM_ProceedCaseController {
    
    private FM_Case__c objCase ; 
    
    public FM_ProceedCaseController( ApexPages.StandardController controller ) {
        objCase = (FM_Case__c)controller.getrecord();
        objCase = [ SELECT Id, 
                           Account__c,
                           RecordType.DeveloperName,
                           Booking_Unit__c,
                           Request_Type__c,
                           Request_Type_DeveloperName__c 
                      FROM FM_Case__c
                     WHERE Id = :objCase.Id ];
    }
    
    public Pagereference redirectToHomePage() {
        if( objCase != NULL && String.isNotBlank( objCase.Request_Type_DeveloperName__c ) ) {
            FM_Process__mdt objProcMeta = getProcessDetails( objCase.Request_Type_DeveloperName__c );
            if( objProcMeta != NULL && String.isNotBlank( objProcMeta.Process_Page_Name__c ) ) {
                //PageReference page = new PageReference( '/apex/' + objProcMeta.Process_Page_Name__c + '?' + 'AccountId=' + objCase.Account__c + '&UnitId=' + objCase.Booking_Unit__c + '&SRType=' + objProcMeta.DeveloperName + '&CaseId=' + objCase.Id );
                PageReference page = new PageReference( '/apex/' + objProcMeta.Process_Page_Name__c + '?' + 'Id=' + objCase.Id );
                page.setredirect(true);
                system.debug('==page=='+page);
                return page;
            }
        }
        return NULL ;
    }
    
    @testVisible
    private FM_Process__mdt getProcessDetails( String strProcessName ) {
        list<FM_Process__mdt> lstProcesses = [ SELECT Id
                                                    , Process_Page_Name__c
                                                    , MasterLabel
                                                    , DeveloperName
                                                 FROM FM_Process__mdt
                                                WHERE DeveloperName = :strProcessName ];
        if( !lstProcesses.isEmpty() ) {
            return lstProcesses[0];
        }
        return NULL ;
    }
}