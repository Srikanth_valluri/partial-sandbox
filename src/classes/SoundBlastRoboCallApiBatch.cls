/**********************************************************************************************************************
Description: Batch to trigger sound blast Robo call for Added calls.
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By   | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     | 02-04-2020        | Aishwarya Todkar   | 1. Initial Draft

1.1     | 20-05-2020        | Aishwarya Todkar   | 1. Called only once to CX if multiple Calls are Added

1.2     | 20-07-2020        | Aishwarya Todkar   | 1. Skipped connected calls
                                                   2. Added Current Month and year check
***********************************************************************************************************************/
global class SoundBlastRoboCallApiBatch implements Database.Batchable<sObject>
                                                , Database.Stateful
                                                , Database.AllowsCallouts {
    Static String endpoint = Label.Robo_Call_URL;
    Static String auth_key = Label.Robocall_Authorization;
    Static Set<String> arabicNationalities;
    Id campaignId;
    Map<Id, Id> mapAccToPlacedCall; 
    Map<Id, List<Added_call__c>> mapPlacedCallToSkippedCall; 

    public SoundBlastRoboCallApiBatch( Id campaignId ) {

        this.campaignId = campaignId;
        mapPlacedCallToSkippedCall = new Map<Id,List<Added_call__c>>();
        mapAccToPlacedCall = new Map<Id, Id>();

        arabicNationalities = new Set<String>( );
        String strArabicNationalities = Label.Arabic_Nationalities;
        for( String nationality : strArabicNationalities.split( ',' ) ) {
            arabicNationalities.add( nationality.trim( ).toLowerCase( ) );
        }
        
        System.debug( '- - >> arabicNationalities : ' + arabicNationalities );
    }
    
    global Database.QueryLocator start( Database.BatchableContext BC ) {
        Set<String> notReachableValuesSet = new Set<String>();
        

        if( Label.Not_Reachable_Values_Set.contains( ',' ) )
            notReachableValuesSet.addAll( Label.Not_Reachable_Values_Set.split(',') );
        else
            notReachableValuesSet.add( Label.Not_Reachable_Values_Set);

        if( Test.isRunningTest() )
            notReachableValuesSet.add('Unreachable');  

        System.debug( '- - >> notReachableValuesSet : ' + notReachableValuesSet );
        System.debug( '- - >> campaignObjId : ' + campaignId );
        String this_Month = String.valueOf( System.today().month() );
        String this_Year = String.valueOf( System.today().year() );

        String query = 'SELECT'
                    + ' Id'
                    + ' , Input_String_For_Add_Call__c'
                    + ' , Calling_List__r.Account__r.Primary_Language__c'
                    + ' , Calling_List__r.Account__r.Nationality__c'
                    + ' , Calling_List__r.Account__r.Mobile_Person_Business__c'
                    + ' , Calling_List__r.Account__r.isPersonAccount'
                    + ' , Calling_List__r.Account__r.Nationality__pc'
                    + ' from Added_call__c where '
                    + ' Calling_List__c != null '
                    + ' AND Calling_List__r.recordTypeId != null '
                    + ' AND Calling_List__r.recordType.Name = \'Collections Calling List\' '
                    + ' AND Calling_List__r.Account__c != null '
                    + ' AND Calling_List__r.Account__r.Mobile_Person_Business__c != null '
                    + ' AND ( Calling_List__r.Account__r.Primary_Language__c != null '
                    + ' OR Calling_List__r.Account__r.Nationality__c != null'
                    + ' OR Calling_List__r.Account__r.Nationality__pc != null )'
                    + ' AND Primary_Call__c = true'
                    + ' AND Calling_List__r.Call_OutCome__c != null '
                    + ' AND Calling_List__r.Call_OutCome__c IN : notReachableValuesSet '
                    + ' AND Campaign_Robo__c = \'' + campaignId + '\''
                    + ' AND Status__c != \'Connected\''                     // 20-07-2020  | Aishwarya Todkar   | 1. Skipped connected calls
                    + ' AND Calling_List__r.Collection_Month__c != null '   // 20-07-2020  | Aishwarya Todkar   | 2. Added Current Month and year check
                    + ' AND Calling_List__r.Collection_Month__c = \'' + this_Month + '\''
                    + ' AND Calling_List__r.Collection_Year__c != null '
                    + ' AND Calling_List__r.Collection_Year__c = \'' + this_Year + '\'';
        
        String testRecId = Label.Record_Id_To_Test;
        if( String.isNotBlank ( testRecId ) 
            && testRecId.toLowerCase().contains('on') 
            && String.isNotBlank( testRecId.substringAfter('-') )
            && testRecId.substringAfter('-').startsWith( 'a37' ) 
            && !Test.isRunningTest() ) {
            query = query + ' AND Id = \'' + testRecId.substringAfter('-') + '\'';
        }
        
        System.debug( 'query- - - ' + query );
        return Database.getQueryLocator( query );
    }
    
    global void execute( Database.BatchableContext BC, List<Added_call__c> listAddedCalls ) {
        
        System.debug( listAddedCalls.size() + ' - listAddedCalls --- ' + listAddedCalls);
        List<Added_call__c> listCallsToUpdate = new List<Added_call__c>( );

        if( !listAddedCalls.isEmpty( ) ) {
            List<Calling_List__c> listCLToUpdate = new List<Calling_List__c>();
            for( Added_call__c objCall : listAddedCalls ) {
                
                if( mapAccToPlacedCall != null
                && mapAccToPlacedCall.size() > 0
                && mapAccToPlacedCall.containsKey( objCall.Calling_List__r.Account__c )) {

                    System.debug('--- Secondary Calls to skip ---');
                    //Skipping calls 
                    if(mapPlacedCallToSkippedCall != null
                    && mapPlacedCallToSkippedCall.containsKey( mapAccToPlacedCall.get( objCall.Calling_List__r.Account__c ) ) ) {
                        
                        System.debug('--- Multiple skipped calls ---');
                        objCall.Parent_Added_Call_Id__c = mapAccToPlacedCall.get( objCall.Calling_List__r.Account__c );
                        
                        String strKeyword =  getKeyword( objCall );
                        if( String.isNotBlank( strKeyword ) )
                            objCall.Input_String_For_Add_Call__c = strKeyword;

                        listCallsToUpdate.add( objCall );

                        mapPlacedCallToSkippedCall.get( mapAccToPlacedCall.get( objCall.Calling_List__r.Account__c ) ).add( objCall );

                    }
                    else {
                        
                        System.debug('--- First skipped call ---');
                        objCall.Parent_Added_Call_Id__c = mapAccToPlacedCall.get( objCall.Calling_List__r.Account__c );

                        String strKeyword =  getKeyword( objCall );
                        if( String.isNotBlank( strKeyword ) )
                            objCall.Input_String_For_Add_Call__c = strKeyword;
                        listCallsToUpdate.add( objCall );

                        mapPlacedCallToSkippedCall.put( 
                            mapAccToPlacedCall.get( objCall.Calling_List__r.Account__c )
                            , new List<Added_call__c> { objCall }
                        );
                    }
                }
                else {

                    System.debug('--- First Call ---');
                    //Placing call for first Added call
                    mapAccToPlacedCall.put( objCall.Calling_List__r.Account__c, objCall.Id );

                    String strKeyword = placeCall( objCall );
                    if( String.isNotBlank( strKeyword ) )
                        listCallsToUpdate.add( objCall );
                }
            }//End For loop
            
            System.debug( 'mapAccToPlacedCall==' + mapAccToPlacedCall);
            System.debug( 'mapPlacedCallToSkippedCall==' + mapPlacedCallToSkippedCall);

            System.debug( listCallsToUpdate.size() + ' - listCallsToUpdate==> ' +  listCallsToUpdate );
            if( !listCallsToUpdate.isEmpty() )  
                Database.update( listCallsToUpdate );

        } //End listAddedCalls if
    }//End Method
    
/**********************************************************************************************************************
Description : Method to place robo call
Parameter(s): Added_call__c instance
Return Type : String
**********************************************************************************************************************/
    public static String placeCall( Added_call__c objCall ) {
        //Call method to get keyword param
        String strKeyword =  getKeyword( objCall );
        
        if( String.isNotBlank( strKeyword ) ) {
            
            //Map containing all the key-Value pairs for making the RoboCall callout
            Map<String, String> mapKeyVal = new Map<String, String>{'keyword'=>strKeyword, 'file'=>'csv'};
            
            try {

                //Call Method get request body as blob
                Blob bodyAsBlob = RoboCallService.getBodyAsBlob( mapKeyVal );
                
                //Call method to request robo call API
                if( bodyAsBlob != null ) {
                    HttpResponse response = RoboCallService.processRoboRequest( bodyAsBlob
                                                                                , endpoint
                                                                                , auth_key );
                    
                    if( response != null && response.getStatusCode() == 200 ) {
                        
                        //Update request keyword in Added call
                        if( objCall.Input_String_For_Add_Call__c == null 
                        || objCall.Input_String_For_Add_Call__c != strKeyword ) {
                           objCall.Input_String_For_Add_Call__c = strKeyword;

                           return strKeyword;
                            //listCallsToUpdate.add( objCall );    
                        }
                    }//End Response if
                }//End bodyAsBlob if
            }//End Try
            catch( Exception e) {
                System.debug( e.getMessage() );
            }
        }

        return null;
    }

/**********************************************************************************************************************
Description : Method to prepare keyWord parameter
Parameter(s): Added_call__c instance
Return Type : String
**********************************************************************************************************************/
    public static String getKeyword( Added_call__c objCall ) {
        String strKeyword = '';
        String mobile = objCall.Calling_List__r.Account__r.Mobile_Person_Business__c;
        String language = '';
        String Use_Default_Language = Test.isRunningTest()? 'no' : Label.Use_Default_Language;
        if( Use_Default_Language.equalsIgnoreCase('yes') )
            language = 'English';
        else {
            if( String.isNotBlank( objCall.Calling_List__r.Account__r.Primary_Language__c ) ) {
                language = objCall.Calling_List__r.Account__r.Primary_Language__c;
            }
            else {
                String nationality;
                if( objCall.Calling_List__r.Account__r.IsPersonAccount ) {
                    nationality = String.isNotBlank( objCall.Calling_List__r.Account__r.Nationality__pc )   
                                ? objCall.Calling_List__r.Account__r.Nationality__pc
                                : String.isNotBlank( objCall.Calling_List__r.Account__r.Nationality__c )  
                                ? objCall.Calling_List__r.Account__r.Nationality__c 
                                : '';
                }
                else {
                    nationality = String.isNotBlank( objCall.Calling_List__r.Account__r.Nationality__c )   
                                ? objCall.Calling_List__r.Account__r.Nationality__c
                                : String.isNotBlank( objCall.Calling_List__r.Account__r.Nationality__pc )  
                                ? objCall.Calling_List__r.Account__r.Nationality__pc 
                                : '';
                }
                language = String.isNotBlank( nationality ) && arabicNationalities.contains ( nationality.toLowerCase() )   
                        ? 'arabic' : 'English'; 
            }
        }
        
        if( mobile.startsWith('00')) {
            mobile = mobile.removeStart('00');
            mobile = '+' + mobile;
        }
        if( String.isNotBlank( mobile ) && String.isNotBlank( language ) )  
            strKeyword = mobile + ',' + language + ',' + objCall.Id + ',sound_blast';
        
        System.debug( 'strKeyword == ' + strKeyword );
        return strKeyword;
    }

    global void finish( Database.BatchableContext BC ) {
        System.debug( '- - >> Finish ' );
    }
}