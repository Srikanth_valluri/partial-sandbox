/************************************************************************************************
 * @Name              : CreateTeamsMeetingController_Test
 * @Description       : Test Class for CreateTeamsMeetingController 
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         24/03/2020       Created
***********************************************************************************************/

@isTest
public class CreateTeamsMeetingController_Test {

    @isTest
    static void testCreateMeeting(){
        API_Integration_Value__c apiValue = new API_Integration_Value__c();
        apiValue.Refresh_Token__c = 'refreshtoken'; 
        apiValue.Client_Id__c = 'testclientid';
        apiValue.Client_Secret__c = 'testclientsecret';
        apiValue.Endpoint_URL__c = 'testendpointurl';
        apiValue.Redirect_URI__c = 'testredirecturi'; 
        apiValue.Scope__c = 'testscope'; 
        apiValue.Event_Creation_URL__c = 'testeventcreationurl';
        apiValue.Tenant_Id__c = 'testtenantid'; 
        apiValue.Username__c = 'testusername';
        apiValue.Password__c = 'testpassword';
        apiValue.Meeting_EndPoint_URL__c = 'endpointurl';
        apiValue.name = 'Graph API';
        insert apiValue;
        
        Id inquiryRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        Inquiry__c inqObj = new Inquiry__c(Pre_InquiryId__c='123456',
                                            Mobile_Phone_Encrypt__c='456123',RecordTypeId = inquiryRT,
                                            Mobile_CountryCode__c='American Samoa: 001684',
                                            Mobile_Phone__c='1234',Email__c='mk@gmail.com',
                                            First_Name__c='Test',Last_Name__c='Last',
                                            CR_Number__c='0987',Organisation_Name__c = 'Oliver',
                                            Online_Meeting_Start_Date_Time__c = DateTime.now().addHours(2),
                                            Online_Meeting_End_Date_Time__c = DateTime.now().addHours(3)
                                            );
        inqObj.Assigned_PC__c = UserInfo.getuserId(); 
        inqObj.Meeting_Type__c = 'Direct Tour';
        inqObj.Tour_Date_Time__c = system.now();
        insert inqObj;
        
        Task teamsTask = new Task();
        teamsTask.Subject = 'subject'; 
        teamsTask.Type = 'Meeting Outside Office';
        teamsTask.Call_Start_Time__c = DateTime.now().addHours(2);
        teamsTask.Activity_Type_3__c = 'Meeting Outside Office';
        teamsTask.Call_End_Time__c = DateTime.now().addHours(3);
        teamsTask.Description = 'Teams Meeting with Property Advisor\n\n'; 
        teamsTask.OwnerId = UserInfo.getUserId();
        teamsTask.WhatId = inqObj.Id;
        insert teamsTask;

        Test.startTest();
        PageReference pageRef = Page.CreateTeamsMeeting;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', inqObj.Id);
        CreateTeamsMeetingController obj = new CreateTeamsMeetingController();
        obj.updateEndDateTime();
        obj.createMeeting();
        ApexPages.currentPage().getParameters().put('id', teamsTask.Id);
        CreateTeamsMeetingController obj2 = new CreateTeamsMeetingController();
        obj2.createMeeting();
        obj2.updateSuccess = true;
        obj2.updateEvent();
        Test.stopTest();
        
    }
}