@isTest
public without sharing class EnrichmentTest{
    @isTest
    public static void testCreateEnrichment() {
        List<Account> lstAcc = new List<Account>();
        String RecTypeId= [select Id from RecordType where (Name='Person Account') and (SobjectType='Account')].Id;
        for (Integer i = 1 ; i <= 5 ; i++) {
            Account acc = new Account();
            acc.RecordTypeID=RecTypeId;
            acc.FirstName='Test FName' + i;
            acc.LastName='Test LName';
            acc.PersonEmail = 'test' + i + '@gmail.com';
            lstAcc.add(acc);
        }
        insert lstAcc;
        List<String> lstEmails = new List<String>{'test1@gmail.com','test2@gmail.com','test2@gmail.com','test4@gmail.com','test5@gmail.com'};
        List<String> lstMob = new List<String>{'9090909091','9090909092','9090909093','9090909094','9090909095'};
        List<String> lstCodes = new List<String>{'code1','code2','code3','code4','code5'};
        List<String> lstAddress = new List<String>{'Street1','Street2','Street','Street4','Street5'};
        List<String> lstLinkedIn = new List<String>{'LinkedIn1','LinkedIn2','LinkedIn3','LinkedIn4','LinkedIn5'};
        List<String> lstFacebook = new List<String>{'Facebook1','Facebook2','Facebook3','Facebook4','Facebook5'};
        List<String> lstInstagram = new List<String>{'Instagram1','Instagram2','Instagram3','Instagram4','Instagram5'};
        List<String> lstTwitter = new List<String>{'Twitter1','Twitter2','Twitter3','Twitter4','Twitter5'};
        List<String> lstCity = new List<String>{'City1','City2','City3','Amritsar','Antwerp'};
        Integer age = 25;
        List<String> lstCompany = new List<String>{'Company1','Company2','Company3','Company4','Company5'};
        List<String> lstDegree = new List<String>{'Degree1','Degree2','Degree3','Degree4','Degree5'};
        List<String> lstCollege = new List<String>{'College1', 'College2', 'College3', 'College4'};
        List<String> lstState = new List<String>{'State1', 'State2', 'State3', 'State4', 'State5'};
        List<String> lstPOBox = new List<String>{'POBox1', 'POBox2', 'POBox3', 'POBox4'};
        List<Date> lstStartDate = new List<Date>{System.today().addDays(1), System.today().addDays(2), System.today().addDays(3)};
        List<String> lstDesignation = new List<String>{'Designation1', 'Designation2', 'Designation3'};
        List<String> lstCountry = new List<String>{'Designation1', 'Designation2', 'Designation3'};
        String Gender = 'Male';
        String ExternalId = lstAcc[0].Id;
        
        Enrichment.ResponseWrapper objRes = Enrichment.createEnrichment(lstEmails,lstMob,lstCodes, lstAddress,lstLinkedIn,lstFacebook,lstInstagram,lstTwitter, lstCity, age, lstCompany, lstDegree,
                                   lstCollege, lstState , lstPOBox, lstStartDate, lstDesignation, Gender, ExternalId, lstCountry );
        
        System.assertEquals('400', objRes.statusCode);
        System.assertEquals('Error', objRes.status);
        
        lstCity = new List<String>{'Abu Dhabi','Agra','Ahmedabad','Amritsar','Antwerp'};
        Enrichment.ResponseWrapper objRes1 = Enrichment.createEnrichment(lstEmails,lstMob,lstCodes, lstAddress,lstLinkedIn,lstFacebook,lstInstagram,lstTwitter, lstCity, age, lstCompany, lstDegree,
                                   lstCollege, lstState , lstPOBox, lstStartDate, lstDesignation, Gender, ExternalId, lstCountry );
        
        System.assertEquals('200', objRes1.statusCode);
        System.assertEquals('Details Created Successfully !', objRes1.status);
    }
}