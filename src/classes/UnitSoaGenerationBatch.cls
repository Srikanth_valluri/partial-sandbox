/****************************************************************************************
Description : To generate Unit SOA On BU                                                *
----------------------------------------------------------------------------------------*
Version     Date        Author              Description                                 *
                                                                                        *
1.0         5/9/2019   Aishwarya Todkar    Initial Drafet                              *
*****************************************************************************************/
public without sharing class UnitSoaGenerationBatch implements Database.Batchable<sObject>
                                                            , Database.Stateful
                                                            , Database.AllowsCallouts {

    private list<String> lstBUIdNew;

    public UnitSoaGenerationBatch(List<String> lstBookingUnitIds) {
            lstBUIdNew = new list<String>();
            lstBUIdNew.addAll(lstBookingUnitIds);
    }

    public Database.QueryLocator start(Database.BatchableContext BC) {
        system.debug('!!!!lstBUIdNew'+lstBUIdNew);
        String query;
        query = 'SELECT Id,Booking__r.Account__r.Email__c,Registration_ID__c, Unit_Name__c, Unit_Type__c, ';
        query += 'Approval_Status__c, Inventory__r.Building_Location__r.Additional_Document_Name__c, ';
        query += 'Inventory__c, Inventory__r.Building_Location__c, URLs_of_Document__c, No_of_URL_Documents__c ';
        query += 'From Booking_Unit__c Where Id IN: lstBUIdNew';
        
        system.debug('query ==' + query);
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List< Booking_Unit__c > lstBu) {
        System.debug('execute lstBu =' +lstBu);
        List<Booking_Unit__c> lstBuToUpdate = new List<Booking_Unit__c>();
        DisplayRelatedUnitsController.avoidErrorLogCreation = true;
        for( Booking_Unit__c objBU : lstBu ) {
            if( objBU.Registration_ID__c != null ) {

                String soaUrl;
                if(Test.isRunningTest()) {
                    soaUrl = '<ul><li> <a href="https://test" target="_blank">Statement Of Account</a></li></ul>';
                }
                else {
                    soaUrl = FmIpmsRestCoffeeServices.getUnitSoa( objBU.Registration_ID__c );
                }

                system.debug('soaUrl == ' + soaUrl);
                if( String.isNotBlank(soaUrl)) {
                    String soaUrlLink = '<li> ' + '<a href="' + soaUrl + '" target="_blank">' 
                                            + 'Statement Of Account' + '</a>' + '</li>' ;

                    system.debug('soaUrlLink == ' + soaUrlLink);

                    //if( String.isBlank(objBU.URLs_of_Document__c) ) {
                        objBU.URLs_of_Document__c = '<ul>' + soaUrlLink +'</ul>';
                        objBU.No_of_URL_Documents__c = 1;
                        lstBuToUpdate.add(objBU);
                    /*}
                    else {
                        if( objBU.URLs_of_Document__c.contains('Statement Of Account') ) {
                            String docUrls = objBU.URLs_of_Document__c;

                            String soaUrlTemp1 = docUrls.substringBefore('Statement Of Account');
                            System.debug( 'soaUrlTemp1 substringBefore == ' + soaUrlTemp1);
                            System.debug( 'soaUrlTemp1.lastIndexOf(<li>) == ' + soaUrlTemp1.lastIndexOf('<li>'));
                            soaUrlTemp1 = soaUrlTemp1.substring( 0, soaUrlTemp1.lastIndexOf('<li>'));
                            //+ soaUrlLink
                            String soaUrlTemp2 = docUrls.substringAfter('Statement Of Account');
                            soaUrlTemp2 = soaUrlTemp2.substring(soaUrlTemp2.indexOf('</li>') + 5 , soaUrlTemp2.length()); 

                            System.debug('soaUrlTemp1== ' + soaUrlTemp1);
                            System.debug('soaUrlTemp2== ' + soaUrlTemp2);

                            String newSoaLink = soaUrlTemp1 + soaUrlLink + soaUrlTemp2;

                            System.debug('newSoaLink== ' + newSoaLink);
                            objBU.URLs_of_Document__c = newSoaLink;
                            //objBU.No_of_URL_Documents__c = objBU.No_of_URL_Documents__c != null ? objBU.No_of_URL_Documents__c + 1 : 1;
                            lstBuToUpdate.add(objBU);
                        }
                        else if( !objBU.URLs_of_Document__c.contains('Statement Of Account') ) {
                            String docUrls = objBU.URLs_of_Document__c;
                            docUrls = docUrls.removeEnd('</ul>');
                            docUrls = docUrls + soaUrlLink + '</ul>';
                            system.debug('docUrls == ' + docUrls);
                            objBU.URLs_of_Document__c = docUrls;
                            objBU.No_of_URL_Documents__c = objBU.No_of_URL_Documents__c != null ? objBU.No_of_URL_Documents__c + 1 : 1;
                            lstBuToUpdate.add(objBU);
                        }
                    }*/
                }
            } 
        }//End for

        if( lstBuToUpdate != null && lstBuToUpdate.size() > 0 ) {
            update lstBuToUpdate;
        }
    }

    public void finish(Database.BatchableContext BC) {
        DisplayRelatedUnitsController.avoidErrorLogCreation = false;
    }
}