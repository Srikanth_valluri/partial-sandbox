/**************************************************************************************************
* Name               : InventoryIsFocusRemoveScheduler
* Test Class         : 
* Description        : Scheduler class for InventoryIsFocusRemoveBatch class.
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst        27/11/2020      Initial Draft.
**************************************************************************************************/
public with sharing class InventoryIsFocusRemoveScheduler implements Schedulable{

    public void execute(SchedulableContext SC) {
        InventoryIsFocusRemoveBatch batchObject = new InventoryIsFocusRemoveBatch();
        Database.executeBatch(batchObject, 200); 
    }
}