/************************************************************************************************
 * @Name              : DAMAC_Inventory_External_Pagination_Test
 * @Description       : Test Class for DAMAC_Inventory_External_Pagination
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         25/06/2020       Created
***********************************************************************************************/

@isTest
public class DAMAC_Inventory_External_Pagination_Test{

    @isTest
    static void testAPI(){
        Marketing_Documents__c mark = new Marketing_Documents__c();
        mark.Name = '90210 Boutique Villas';
        insert mark;
        
        Inventory__c inv = new Inventory__c();
        inv.Unit_Plan_SF_Link__c = 'https://damacholding--c.eu18.content.force.com/servlet/servlet.FileDownload?file=00P1n00001F7TB4EAN';
        inv.Tagged_To_Unit_Assignment__c = FALSE;
        inv.Marketing_Name__c = 'some string';
        inv.Status__c = 'Released';
        inv.Is_Assigned__c = false;
        inv.Marketing_Name_Doc__c = mark.Id;
        inv.Plot_Plan_SF_Link__c = 'https://damacholding--c.eu18.content.force.com/servlet/servlet.FileDownload?file=00P1n00001F7TB5EAN';
        inv.Property_Name__c = 'DAMAC HILLS - THE FIELD';
        inv.Unit_Id__c = '199539';
        inv.Bedroom_Type__c = 'TH-L-A';
        inv.IPMS_Bedrooms__c = '4';
        inv.Unit_Type__c = 'Residential Area';
        inv.View_Type__c = 'Golf Back';
        inv.Property_Status__c = '';
		inv.IS_Focus__c = true;        
        inv.ACD_Date__c = '';
        inv.Plot_Area__c = '';
        inv.Floor_Plan__c = '';
        inv.Unit_Plan__c = '';
        inv.Plot_Plan__c = '';
        inv.Unit_Catagory__c = '';
        inv.Rera_Percentage__c = ''; 
        inv.Special_Price__c = 1.0;
        inv.Special_Price_Tax_Amount__c = 1.0;
        inv.Property_country__c = 'DUBAI';
        inv.Property_City__c = 'DUBAI'; 
        inv.District__c = 'ABU DHABI';
        insert inv;
       
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();          
        req.requestBody = blob.valueOf('{"Anticipated_Completion_Date__c": ["2020 Q1", "2020 Q2", "2020 Q3", "2020 Q4", "2021 Q1", "2021 Q2", "2021 Q4", "2022 Q4"],"Property_Status__c": ["OFF-PLAN", "READY"],"Property_city__c": "DUBAI","District__c": ["ABU DHABI", "AKOYA OXYGEN", "BUSINESS BAY", "DAMAC HILLS", "DUBAI SOUTH"]},"min_Area_sft__c":"100","max_Area_sft__c":"1000","max_Price_Per_Sqft__c": "1200","min_Price_Per_Sqft__c": "100","Sort":"price_low_to_high"');
        String reqBody = req.requestBody.toString();
        Map<String, Object> jsonResponse = (Map<String, Object>) JSON.deserializeUntyped(reqBody);
        
        
        req.params.put ('type', 'popular');
        req.requestUri ='/queryInventoryPagination';                
        req.httpMethod = 'POST';       
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_Inventory_External_Pagination.getResults();
        
        req.params.put ('type', 'new');
        req.requestUri ='/queryInventoryPagination';                
        req.httpMethod = 'POST';       
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_Inventory_External_Pagination.getResults();
        
        req.params.put ('type', 'general');
        req.requestUri ='/queryInventoryPagination';                
        req.httpMethod = 'POST';       
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_Inventory_External_Pagination.getResults();
        
        req.params.put ('pageSize', '50');
        req.params.put ('pageOffset', '0');
        req.params.put ('type', 'new');
        req.params.put ('searchTerm', 'ab');
        req.requestUri ='/queryInventoryPagination';                
        req.httpMethod = 'POST';       
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_Inventory_External_Pagination.getResults(); 
        
        req.params.put ('pageSize', '50');
        req.params.put ('pageOffset', '0');
        req.params.put ('type', 'general');
        req.params.put ('searchTerm', 'ab');
        req.requestUri ='/queryInventoryPagination';                
        req.httpMethod = 'POST';       
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_Inventory_External_Pagination.getResults(); 
        
        req.params.put ('pageSize', '50');
        req.params.put ('pageOffset', '0');
        req.params.put ('type', 'popular');
        req.params.put ('searchTerm', 'ab');
        req.requestUri ='/queryInventoryPagination';                
        req.httpMethod = 'POST';       
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_Inventory_External_Pagination.getResults(); 
        
        //to display normal pagination with type popular
        req.params.put ('pageSize', '50');
        req.params.put ('pageOffset', '0');
        req.params.put ('type', 'popular');
        req.requestUri ='/queryInventoryPagination';                
        req.httpMethod = 'POST';       
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_Inventory_External_Pagination.getResults(); 
        
        req.params.put ('pageSize', '50');
        req.params.put ('pageOffset', '0');
        req.params.put ('type', 'new');
        req.requestUri ='/queryInventoryPagination';                
        req.httpMethod = 'POST';       
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_Inventory_External_Pagination.getResults();
        
        req.params.put ('pageSize', '50');
        req.params.put ('pageOffset', '0');
        req.params.put ('type', 'general');
        req.requestUri ='/queryInventoryPagination';                
        req.httpMethod = 'POST';       
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_Inventory_External_Pagination.getResults();
        
        //to display filters applied
        req.params.put ('pageSize', '50');
        req.params.put ('pageOffset', '0');        
        req.requestUri ='/queryInventoryPagination';        
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_Inventory_External_Pagination.getResults(); 
        
        //to display total count
        req.params.put ('IsTotalCountNeeded', 'true');                
        req.requestUri ='/queryInventoryPagination';
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_Inventory_External_Pagination.getResults(); 
        
        req.params.put ('type', 'popular');
        req.requestUri ='/queryInventoryPaginationWrongURL';                
        req.httpMethod = 'POST';       
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_Inventory_External_Pagination.getResults();
        
        req.params.put ('pageSize', '50');
        req.params.put ('pageOffset', '0'); 
        req.params.put ('type', 'popular');
        req.params.put ('IsTotalCountNeeded', 'false');
        req.requestUri = '/queryInventoryPagination';
        req.requestBody = Blob.valueOf('{"min_Area_sft__c" : "10"}');
        req.httpMethod = 'POST';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External_Pagination.getResults();
        
        req.requestUri = '/queryInventoryPagination';
        req.requestBody = Blob.valueOf('{"max_Area_sft__c" : "100"}');
        req.httpMethod = 'POST';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External_Pagination.getResults();
        
        req.requestUri = '/queryInventoryPagination';
        req.requestBody = Blob.valueOf('{"No_of_bedrooms__c":"(STUDIO, 1BR, 2BR)"}');
        req.httpMethod = 'POST';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External_Pagination.getResults();
        
        req.requestUri = '/queryInventoryPagination';
        req.requestBody = Blob.valueOf('{"sort" : "price_high_to_low"}');
        req.httpMethod = 'POST';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External_Pagination.getResults();
        
        req.requestUri = '/queryInventoryPagination';
        req.requestBody = Blob.valueOf('{"sort" : "price_high_to_low"}');
        req.httpMethod = 'POST';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External_Pagination.getResults();
        
        req.requestUri = '/queryInventoryPagination';
        req.requestBody = Blob.valueOf('{"sort" : "area_low_to_high"}');
        req.httpMethod = 'POST';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External_Pagination.getResults();
        
        req.requestUri = '/queryInventoryPagination';
        req.requestBody = Blob.valueOf('{"sort" : "area_high_to_low"}');
        req.httpMethod = 'POST';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External_Pagination.getResults();
        
        req.requestUri = '/queryInventoryPagination';
        req.requestBody = Blob.valueOf('{"sort" : "bedroom_low_to_high", "No_of_bedrooms__c":"(STUDIO, 1BR, 2BR)"}');
        req.httpMethod = 'POST';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External_Pagination.getResults();
        
        req.requestUri = '/queryInventoryPagination';
        req.requestBody = Blob.valueOf('{"sort" : "bedroom_high_to_low", "No_of_bedrooms__c":"(STUDIO, 1BR, 2BR)"}');
        req.httpMethod = 'POST';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External_Pagination.getResults();
        
        Test.stopTest();
        
        
            
            
            
    }
    static void testAPI2(){
        Marketing_Documents__c mark = new Marketing_Documents__c();
        mark.Name = '90210 Boutique Villas';
        insert mark;
        
        Inventory__c inv = new Inventory__c();
        inv.Unit_Plan_SF_Link__c = 'https://damacholding--c.eu18.content.force.com/servlet/servlet.FileDownload?file=00P1n00001F7TB4EAN';
        inv.Tagged_To_Unit_Assignment__c = FALSE;
        inv.Marketing_Name__c = 'some string';
        inv.Status__c = 'Released';
        inv.Is_Assigned__c = false;
        inv.Marketing_Name_Doc__c = mark.Id;
        inv.Plot_Plan_SF_Link__c = 'https://damacholding--c.eu18.content.force.com/servlet/servlet.FileDownload?file=00P1n00001F7TB5EAN';
        inv.Property_Name__c = 'DAMAC HILLS - THE FIELD';
        inv.Unit_Id__c = '199539';
        inv.Bedroom_Type__c = 'TH-L-A';
        inv.IPMS_Bedrooms__c = '4';
        inv.Unit_Type__c = 'Residential Area';
        inv.View_Type__c = 'Golf Back';
        inv.Property_Status__c = '';
		inv.IS_Focus__c = true;        
        inv.ACD_Date__c = '';
        inv.Plot_Area__c = '';
        inv.Floor_Plan__c = '';
        inv.Unit_Plan__c = '';
        inv.Plot_Plan__c = '';
        inv.Unit_Catagory__c = '';
        inv.Rera_Percentage__c = ''; 
        inv.Special_Price__c = 1.0;
        inv.Special_Price_Tax_Amount__c = 1.0;
        inv.Property_country__c = 'DUBAI';
        inv.Property_City__c = 'DUBAI'; 
        inv.District__c = 'ABU DHABI';
        insert inv;
       
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();          
       /* req.requestBody = blob.valueOf('{"Anticipated_Completion_Date__c": ["2020 Q1", "2020 Q2", "2020 Q3", "2020 Q4", "2021 Q1", "2021 Q2", "2021 Q4", "2022 Q4"],"Property_Status__c": ["OFF-PLAN", "READY"],"Property_city__c": "DUBAI","District__c": ["ABU DHABI", "AKOYA OXYGEN", "BUSINESS BAY", "DAMAC HILLS", "DUBAI SOUTH"]},"min_Area_sft__c":"100","max_Area_sft__c":"1000","max_Price_Per_Sqft__c": "1200","min_Price_Per_Sqft__c": "100","Sort":"price_low_to_high"');
        String reqBody = req.requestBody.toString();
        //Map<String, Object> jsonResponse = (Map<String, Object>) JSON.deserializeUntyped(reqBody);
        
        
        req.params.put ('type', 'popular');
        req.requestUri ='/queryInventoryPagination';                
        req.httpMethod = 'POST';       
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_Inventory_External_Pagination.getResults();
        
        req.params.put ('type', 'new');
        req.requestUri ='/queryInventoryPagination';                
        req.httpMethod = 'POST';       
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_Inventory_External_Pagination.getResults();
        
        req.params.put ('type', 'general');
        req.requestUri ='/queryInventoryPagination';                
        req.httpMethod = 'POST';       
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_Inventory_External_Pagination.getResults();
        
        req.params.put ('pageSize', '50');
        req.params.put ('pageOffset', '0');
        req.params.put ('type', 'new');
        req.params.put ('searchTerm', 'ab');
        req.requestUri ='/queryInventoryPagination';                
        req.httpMethod = 'POST';       
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_Inventory_External_Pagination.getResults(); 
        
        req.params.put ('pageSize', '50');
        req.params.put ('pageOffset', '0');
        req.params.put ('type', 'general');
        req.params.put ('searchTerm', 'ab');
        req.requestUri ='/queryInventoryPagination';                
        req.httpMethod = 'POST';       
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_Inventory_External_Pagination.getResults(); 
        
        req.params.put ('pageSize', '50');
        req.params.put ('pageOffset', '0');
        req.params.put ('type', 'popular');
        req.params.put ('searchTerm', 'ab');
        req.requestUri ='/queryInventoryPagination';                
        req.httpMethod = 'POST';       
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_Inventory_External_Pagination.getResults(); 
        
        //to display normal pagination with type popular
        req.params.put ('pageSize', '50');
        req.params.put ('pageOffset', '0');
        req.params.put ('type', 'popular');
        req.requestUri ='/queryInventoryPagination';                
        req.httpMethod = 'POST';       
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_Inventory_External_Pagination.getResults(); 
        
        req.params.put ('pageSize', '50');
        req.params.put ('pageOffset', '0');
        req.params.put ('type', 'new');
        req.requestUri ='/queryInventoryPagination';                
        req.httpMethod = 'POST';       
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_Inventory_External_Pagination.getResults();
        
        req.params.put ('pageSize', '50');
        req.params.put ('pageOffset', '0');
        req.params.put ('type', 'general');
        req.requestUri ='/queryInventoryPagination';                
        req.httpMethod = 'POST';       
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_Inventory_External_Pagination.getResults();
        
        //to display filters applied
        req.params.put ('pageSize', '50');
        req.params.put ('pageOffset', '0');        
        req.requestUri ='/queryInventoryPagination/applyFilters';        
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_Inventory_External_Pagination.getResults(); 
        
        //to display total count
        req.params.put ('IsTotalCountNeeded', 'true');                
        req.requestUri ='/queryInventoryPagination';
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_Inventory_External_Pagination.getResults(); 
        
        req.params.put ('type', 'popular');
        req.requestUri ='/queryInventoryPaginationWrongURL';                
        req.httpMethod = 'POST';       
        RestContext.request = req;
        RestContext.response = res;
        DAMAC_Inventory_External_Pagination.getResults();*/
        
        req.params.put ('pageSize', '50');
        req.params.put ('pageOffset', '0'); 
        req.params.put ('type', 'popular');
        req.params.put ('IsTotalCountNeeded', 'false');
        req.requestUri = '/queryInventoryPagination';
        req.requestBody = Blob.valueOf('{"min_Area_sft__c" : "10"}');
        req.httpMethod = 'POST';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External_Pagination.getResults();
        
        req.requestUri = '/queryInventoryPagination';
        req.requestBody = Blob.valueOf('{"max_Area_sft__c" : "100"}');
        req.httpMethod = 'POST';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External_Pagination.getResults();
        
        req.requestUri = '/queryInventoryPagination';
        req.requestBody = Blob.valueOf('{"sort" : "price_low_to_high"}');
        req.httpMethod = 'POST';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External_Pagination.getResults();
        
        req.requestUri = '/queryInventoryPagination';
        req.requestBody = Blob.valueOf('{"sort" : "price_high_to_low"}');
        req.httpMethod = 'POST';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External_Pagination.getResults();
        
        req.requestUri = '/queryInventoryPagination';
        req.requestBody = Blob.valueOf('{"sort" : "area_low_to_high"}');
        req.httpMethod = 'POST';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External_Pagination.getResults();
        
        req.requestUri = '/queryInventoryPagination';
        req.requestBody = Blob.valueOf('{"sort" : "area_high_to_low", "No_of_bedrooms__c":"(STUDIO, 2BR)"}');
        req.httpMethod = 'POST';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External_Pagination.getResults();
        
        req.requestUri = '/queryInventoryPagination';
        req.requestBody = Blob.valueOf('{"sort" : "bedroom_low_to_high", "No_of_bedrooms__c":"(STUDIO, 2BR)"}');
        req.httpMethod = 'POST';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External_Pagination.getResults();
        
        req.requestUri = '/queryInventoryPagination';
        req.requestBody = Blob.valueOf('{"sort" : "bedroom_high_to_low"}');
        req.httpMethod = 'POST';        
        RestContext.request = req;
        RestContext.response= res;        
        DAMAC_Inventory_External_Pagination.getResults();
        
        Test.stopTest();
        
        
            
            
            
    }
    
    
}