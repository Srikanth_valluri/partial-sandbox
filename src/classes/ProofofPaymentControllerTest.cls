@isTest
public class ProofofPaymentControllerTest {
    static Account objAccount = new Account();
    static NSIBPM__Service_Request__c objNSR = new NSIBPM__Service_Request__c();
    static List<Booking__c> objBooking = new List<Booking__c>();
    static List<Booking_Unit__c> objBU = new List<Booking_Unit__c>();
    static List<Booking_Unit_Active_Status__c> objBUA =new List<Booking_Unit_Active_Status__c>();
    static SR_Attachments__c objSA = new SR_Attachments__c();
    static List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
    static Case objCase = new Case();
    static List<SR_Booking_Unit__c> listSBU = new List<SR_Booking_Unit__c>();

    static void SetUp(){
   // Test.startTest();
        objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        Id POPcase = Schema.SObjectType.case.getRecordTypeInfosByName().get('POP').getRecordTypeId();
        objCase = TestDataFactory_CRM.createCase(objAccount.Id,POPcase);
        objCase.Payment_Date__c = Date.Today();
        objCase.Payment_Mode__c = 'Cash';
        objCase.Payment_Allocation_Details__c = 'test';
        objCase.Total_Amount__c = 2000;
        objCase.Payment_Currency__c = 'AED';
        insert objCase;

        objNSR = TestDataFactory_CRM.createServiceRequest();
        insert objNSR;
        objBooking = TestDataFactory_CRM.createBookingForAccount(objAccount.ID, objNSR.ID, 2);
        insert objBooking;

        lstActiveStatus = TestDataFactory_CRM.createActiveUnitCustomSetting( new list<String>{ 'Agreement executed by DAMAC' } );
        insert lstActiveStatus ;

        objBU = TestDataFactory_CRM.createBookingUnits(objBooking, 4);
        for( Booking_Unit__c objUnit : objBU ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC';
        }
        insert objBU;

        objBUA = TestDataFactory_CRM.createActiveFT_CS();
        insert objBUA;

        objSA = TestDataFactory_CRM.createCaseDocument(objCase.ID,'POP');
        insert objSA;

        listSBU = TestDataFactory_CRM.createSRBookingUnis(objCase.ID,objBU);
        insert listSBU;

        System.debug('...listSBU...'+listSBU);
       // Test.stopTest();
    }

  static void CaseTest(){
      ProofofPaymentController objProofofPaymentController = new ProofofPaymentController();
      objProofofPaymentController.newCaseID = objCase.CaseNumber;
      objProofofPaymentController.strAccountID = objAccount.Id;
      objProofofPaymentController.strSRType ='ProofOfPayment';

      objProofofPaymentController.getName();
      objProofofPaymentController.getFivecaseAttachments();
      objProofofPaymentController.createCasemethod();
      objProofofPaymentController.fileBody= 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
      objProofofPaymentController.fileName= 'C/fakepath/Document_1.txt';
      SOAPCalloutServiceMock.returnToMe = new Map<String, MultipleDocUploadService.DocumentAttachmentMultipleResponse_element>();
      MultipleDocUploadService.DocumentAttachmentMultipleResponse_element response = new MultipleDocUploadService.DocumentAttachmentMultipleResponse_element();
      response.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"SR Header Data Created for SR # :2-005180","PARAM_ID":"2-005180"},{"PROC_STATUS":"S","PROC_MESSAGE":"Created SR Task Data for SR # :2-005180 Task :Verify Proof of Payment Details in IPMS","PARAM_ID":"2-005180"},{"PROC_STATUS":"S","PROC_MESSAGE":"[UNIT] Created Unit Details for SR # 2-005180 Reg Id : 60652","PARAM_ID":"2-005180"},{"PROC_STATUS":"S","PROC_MESSAGE":"[UNIT] Created Unit Details for SR # 2-005180 Reg Id : 60301","PARAM_ID":"2-005180"}],"message":"Process Completed Returning 4 Response Message(s)","status":"S"}';
      SOAPCalloutServiceMock.returnToMe.put('response_x', response);
      Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
      response.return_x = '';
      SOAPCalloutServiceMock.returnToMe.put('response_x', response);
      Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
      Test.startTest();

      Test.stopTest();
      list<case> listcase = new list<case>();
      listcase.add(objCase);
      objProofofPaymentController.saveNewSRBookingUNIT(objBU);      

      objProofofPaymentController.RemoveSelected = objSA.Id;
      objProofofPaymentController.removeAttachment();
      ProofofPaymentController.BookingUnitWrapper objA = new ProofofPaymentController.BookingUnitWrapper(objBU[0]);
      objA.objBookingUnit = objBU[0];
      objA.selected = true;
      objProofofPaymentController.getPaymentModeOptions();
      objProofofPaymentController.getCurrency();
      objProofofPaymentController.isLightningMode=true;
       ProofofPaymentController.UnitDetailsWrapper obj = new ProofofPaymentController.UnitDetailsWrapper();
        obj.strPrice=0.00;
        obj.bookingunit='Test';
        obj.CurrencyCode='Test';
        obj.strArea='Test';
        obj.strAmountPaid='Test';
        obj.strTotalOutstanding='Test';
        obj.strAmountOverdue='Test';
              //objProofofPaymentController.getBookingUnit();      
      //objProofofPaymentController.checkResult();
      //objProofofPaymentController.errorLogger('test','',objBU);
  }

  public static testmethod void withCaseID(){
      SetUp();
      PageReference pageRef = Page.DummyTitleDeedCREPortal;
      Test.setCurrentPage(pageRef);
      ApexPages.currentPage().getParameters().put('AccountId', objAccount.Id);
      ApexPages.currentPage().getParameters().put('SRType', 'ProofOfPayment');
      ApexPages.currentPage().getParameters().put('CaseID', objCase.ID);
      CaseTest();
    }
     public static testmethod void WithoutCaseID(){
      SetUp();
      PageReference pageRef = Page.DummyTitleDeedCREPortal;
      Test.setCurrentPage(pageRef);
      ApexPages.currentPage().getParameters().put('AccountId', objAccount.Id);
      ApexPages.currentPage().getParameters().put('SRType', 'ProofOfPayment');
      ApexPages.currentPage().getParameters().put('CaseID', '');
      CaseTest();
    }
    public static testmethod void errorLoggerTest(){
        SetUp();
        ProofofPaymentController obj = new ProofofPaymentController();
        //obj.errorLogger('Test',objCase.id ,objBU);
    }

      public static testmethod void pagemessage(){

        ProofofPaymentController obj = new ProofofPaymentController();
        obj.PaymentMode = 'None';
        obj.PaymentCurrency = 'None';
        obj.fileBody = '';
        obj.PaymentDate = null;        
        obj.createCasemethod();
    }
    
     static testMethod void Test1(){
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person',party_ID__C='123456');
        insert objAcc;
        
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POP').getRecordTypeId();
        objCase = new Case();
        objCase.AccountID = objAcc.id ;
        objCase.RecordTypeID = devRecordTypeId;        
        insert objCase;
        
        objCase = [Select id,CaseNumber from Case where id=: objCase.id];
        
          PageReference pageRef = Page.DummyTitleDeedCREPortal;
          Test.setCurrentPage(pageRef); 
          ApexPages.currentPage().getParameters().put('AccountID', objAcc.id); 
          ApexPages.currentPage().getParameters().put('CaseID', objCase.id); 
          ApexPages.currentPage().getParameters().put('SRType', 'ProofOfPayment');
        
          ProofofPaymentController objClass = new  ProofofPaymentController();
          objClass.OtherDocAttachmentBody= 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
          objClass.OtherDocAttachmentName = 'C/fakepath/Document_1.txt';
          objClass.objOldCase = objCase;
          UploadMultipleDocController.strLabelValue  = 'N';
          test.StartTest();
              Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
              objClass.addAttachment();
                          
          test.StopTest();    
     }
     
     static testMethod void Test2(){
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person',party_ID__C='123456');
        insert objAcc;
        
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POP').getRecordTypeId();
        objCase = new Case();
        objCase.AccountID = objAcc.id ;
        objCase.RecordTypeID = devRecordTypeId;
        objCase.Payment_Mode__c = 'Cash';        
        insert objCase;
        
        objCase = [Select id,CaseNumber from Case where id=: objCase.id];
        
          PageReference pageRef = Page.DummyTitleDeedCREPortal;
          Test.setCurrentPage(pageRef); 
          ApexPages.currentPage().getParameters().put('AccountID', objAcc.id); 
          ApexPages.currentPage().getParameters().put('CaseID', objCase.id); 
          ApexPages.currentPage().getParameters().put('SRType', 'ProofOfPayment');
        
          ProofofPaymentController objClass = new  ProofofPaymentController();         
          objClass.objOldCase = objCase;
              objClass.fileBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
              objClass.fileName = 'C/fakepath/Document_1.txt';
              
          UploadMultipleDocController.strLabelValue  = 'N';
          test.StartTest();
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));  
              objClass.createCaseMethod();
              objClass.getMarkOptions();
          test.StopTest();    
     }
     
     static testMethod void Test3(){
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person',party_ID__C='123456');
        insert objAcc;
        
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POP').getRecordTypeId();
        objCase = new Case();
        objCase.AccountID = objAcc.id ;
        objCase.RecordTypeID = devRecordTypeId;
        objCase.Payment_Mode__c = 'Cheque';        
        insert objCase;
        
        objCase = [Select id,CaseNumber from Case where id=: objCase.id];
        
          PageReference pageRef = Page.DummyTitleDeedCREPortal;
          Test.setCurrentPage(pageRef); 
          ApexPages.currentPage().getParameters().put('AccountID', objAcc.id); 
          ApexPages.currentPage().getParameters().put('CaseID', objCase.id); 
          ApexPages.currentPage().getParameters().put('SRType', 'ProofOfPayment');
        
          ProofofPaymentController objClass = new  ProofofPaymentController();         
          objClass.objOldCase = objCase;
              objClass.fileBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
              objClass.fileName = 'C/fakepath/Document_1.txt';
              
          UploadMultipleDocController.strLabelValue  = 'N';
          test.StartTest();
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));  
              objClass.createCaseMethod();
              objClass.getMarkOptions();
          test.StopTest();    
     }
     
      static testMethod void Test4(){
        //Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(FirstName='Test FirstName', LastName='Test LastName',party_ID__C='123456');
        insert objAcc;
        
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POP').getRecordTypeId();
        objCase = new Case();
        objCase.AccountID = objAcc.id ;
        objCase.RecordTypeID = devRecordTypeId;
        //objCase.Payment_Mode__c = 'Cheque';        
        insert objCase;
        
        objCase = [Select id,CaseNumber from Case where id=: objCase.id];
        
          PageReference pageRef = Page.DummyTitleDeedCREPortal;
          Test.setCurrentPage(pageRef); 
          ApexPages.currentPage().getParameters().put('AccountID', objAcc.id); 
          ApexPages.currentPage().getParameters().put('CaseID', objCase.id); 
          ApexPages.currentPage().getParameters().put('SRType', 'ProofOfPayment');
        
          ProofofPaymentController objClass = new  ProofofPaymentController();         
          objClass.objOldCase = objCase;
              objClass.fileBody = 'ZGF0YTp0ZXh0L3BsYWluO2Jhc2U2NCxWVzVrWlhKMFlXdHBibWNnUkc5amRXMWxiblE9';
              objClass.fileName = 'C/fakepath/Document_1.txt';
              
          UploadMultipleDocController.strLabelValue  = 'N';
          test.StartTest();
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));  
              objClass.createCaseMethod();
              objClass.getMarkOptions();
          test.StopTest();    
     }
}