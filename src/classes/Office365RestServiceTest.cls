/**
* @File Name          : Office365RestService.cls
* @Description        : 
* @Author             : Arjun@espl.com
* @Group              : 
* @Last Modified By   : Arjun@espl.com
* @Last Modified On   : 10/8/2019, 5:50:18 PM
* @Modification Log   : 
* Ver       Date            Author                 Modification
* 1.0       10/8/2019       Arjun@espl.com         Initial Version
**/
@isTest
public class Office365RestServiceTest {
    @TestSetup
    static void makeData(){
        Credentials_Details__c settings = new Credentials_Details__c();
        settings.Name = 'office365RestServices';
        settings.User_Name__c = 'Some Value';
        settings.Password__c = 'Some Value';
        settings.Resource__c = 'Some Value';
        settings.grant_type__c = 'Some Value';
        insert settings;
    }
    //testing upload
   @isTest
    static void uploadToOffice365ResultTest() {
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive2());
        System.Test.startTest();
        Office365RestService.ResponseWrapper ResponseWrapperobj = new Office365RestService.ResponseWrapper();
        ResponseWrapperobj= Office365RestService.uploadToOffice365Result(Blob.valueOf('test'),'test.txt','','','','');
        system.assertEquals('https://damacgroup-my.sharepoint.com/personal/crm_docs_damacgroup_com/_layouts/15/download',
                             ResponseWrapperobj.downloadUrl,'checking downloadUrl' 
                           );
        system.assertEquals('123456789A', ResponseWrapperobj.id,'checking id');
        system.assertEquals('https://damacgroup-my.sharepoint.com/personal/crm_docs_damacgroup_com/Documents/test.pdf',
                             ResponseWrapperobj.webUrl, 'checking webUrl'
                           );
        System.Test.stopTest();

    }

    @isTest
    static void uploadToOffice365ResultTestNagative() {
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockNegative());
        System.Test.startTest();
        Office365RestService.ResponseWrapper ResponseWrapperobj = new Office365RestService.ResponseWrapper();
        ResponseWrapperobj= Office365RestService.uploadToOffice365Result(Blob.valueOf('test'),'test.txt','','','','');
        System.Test.stopTest();

    }
    //testing download
    @isTest
    static void downloadFromOffice365TestPositive() {
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        System.Test.startTest();
        Office365RestService.ResponseWrapper ResponseWrapperobj = new Office365RestService.ResponseWrapper();
        ResponseWrapperobj=  Office365RestService.downloadFromOffice365('01NUEFKAQZLBYOSPWUERG2WXC7FPDB23C2','test.png','','','','');
        /*system.assertEquals('https://damacgroup-my.sharepoint.com/personal/crm_docs_damacgroup_com/_layouts/15/', 
                             ResponseWrapperobj.downloadUrl,'checking downloadUrl'
                           );*/
        System.Test.stopTest();
    }
    @isTest
    static void downloadFromOffice365TestNagative() {
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockNegative());
        System.Test.startTest();
        Office365RestService.ResponseWrapper ResponseWrapperobj = new Office365RestService.ResponseWrapper();
        ResponseWrapperobj=  Office365RestService.downloadFromOffice365('01NUEFKAQZLBYOSPWUERG2WXC7FPDB23C2','test.png','','','','');
        System.Test.stopTest();
    }
    @isTest
    static void downloadFromOffice365ByNameTestPositive() {
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        System.Test.startTest();
        Office365RestService.ResponseWrapper ResponseWrapperobj = new Office365RestService.ResponseWrapper();
        String result=  Office365RestService.downloadFromOffice365ByName('test.png','','','','');
        System.Test.stopTest();
    }

    @isTest
    static void downloadFromOffice365ByIdWhatsappTest() {
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive());
        System.Test.startTest();
        Office365RestService.ResponseWrapper ResponseWrapperobj = new Office365RestService.ResponseWrapper();
        ResponseWrapperobj=  Office365RestService.downloadFromOffice365ByIdWhatsapp('01NUEFKAQZLBYOSPWUERG2WXC7FPDB23C2','test.png','','','','');
        System.Test.stopTest();

    }
    
        
}