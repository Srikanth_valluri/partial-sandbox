/*
 * Test class name : Damac_SRAttachmentView_Test
 * Description : To display Attachments, booking docs, unit docs, buyer, unit info, reservation form checklist
 * Created By : Srikanth V
 */

Public with sharing class Damac_SRAttachmentView {
    
    public NSIBPM__Service_Request__c SRDetails { get; set; }
    public List <Attachment> attachments { get; set; }
    public List <Unit_Documents__c> bookingDocs { get; set; }
    public List <Unit_Documents__c> unitDocs { get; set; }
    public String attachmentURL { get; set; }
    public Attachment selectedAttachment { get; set; }
    public List <Buyer__c> primaryBuyer { get; set; }
    public List <Buyer__c> jointBuyers { get; set; }
    public New_Step__c stepDetails { get; set; }
    public Deal_Team__c dealTeam { get; set; }
    public List <Booking_Unit__c> bookingUnits { get; set; }
    
    // For checklist
    public Map<String, String> checkListMap {get; set;}
    public Map<String, String> prioritySubheadingMap {get; set;}
    public Set<String> prioritySet {get; set;}
    public Map<String, List<String>> checkListPriorityMap {get; set;}
    public Map<String, Reservation_Form_Check_List__c> checkListRecordMap {get; set;}
    public Unit_documents__c tokenUDoc { get; set; }
    public Unit_documents__c reservationUDoc { get; set; }
    
    public ID SRID;
    
    // Init method to load the records when page loads based on SRID
    public void init () {
        SRID = apexpages.currentpage().getparameters().get('id');
        attachmentURL = '';
        selectedAttachment = new Attachment ();
        SRDetails = new NSIBPM__Service_Request__c ();
        SRDetails = [SELECT Name, IS_UK_Deal__c, NSIBPM__Internal_Status_Name__c, Mode_of_Payment__c, Token_Amount_AED__c,
                     Reservation_Form_Attachment_Name__c, Token_Attachment_Name__c
                     FROM NSIBPM__Service_Request__c WHERE ID =: sRID];
        
        getBuyers ();
        getCheckList ();
        getDealTeam ();
        getBookingUnits ();
        
        loadForms ();
        loadBookingDocs ();
        getUnitDocs ();
        upsertViewCount (SRID, SRID, 'apex/DAMAC_SRAttachmens?id='+SRID, false, '', 'DAMAC_SRAttachments');
            
    }
    
    // To load the token on SR token page when it is loaded by finance team
    public void loadTokenRecord () {
        tokenUDoc = new Unit_Documents__c ();
        SRDetails = new NSIBPM__Service_Request__c ();
        SRID = apexpages.currentpage().getparameters().get('id');
        
        SRDetails = [SELECT Name, NSIBPM__Internal_Status_Name__c, Mode_of_Payment__c, Token_Amount_AED__c,
                     Reservation_Form_Attachment_Name__c, Token_Attachment_Name__c
                     FROM NSIBPM__Service_Request__c WHERE ID =: sRID];
        Attachment tokenForm = new Attachment ();
        try {
            
            tokenForm = [SELECT Name, ParentId, createdDate, createdBy.Name FROM Attachment 
                         WHERE Name =: srDetails.Token_Attachment_Name__c
                         AND ParentId =: SRID order By createdDate DESC LIMIT 1];
        } catch (Exception e) {}
        if (tokenForm.ID != null) {
            List <Unit_documents__c> tokenDocs = new List <Unit_documents__c> ();
            tokenDocs = [SELECT Name, CV_Id__c, Service_Request__c, Document_Name__c, 
                         Document_Description__c, Sys_Doc_ID__c, Document_type__c
                         FROM Unit_documents__c WHERE Service_Request__c =: SRID AND Document_type__c = 'Booking Docs'
                         AND Document_Name__c = 'Token Copy'];
            if (tokenDocs.size () == 0){
                
                tokenUDoc.Service_Request__c = SRID;
                tokenUDoc.Document_Name__c = 'Token Copy';
                tokenUDoc.Document_Description__c = tokenForm.Name;
                tokenUDoc.Sys_Doc_ID__c = tokenForm.ID;
                tokenUDoc.Document_type__c = 'Booking Docs';
                tokenUDoc.Booking_Doc_CreatedBy__c = tokenForm.createdBy.Name;
                tokenUDoc.Booking_Doc_CreatedDate__c = tokenForm.CreatedDate;
                insert tokenUDoc;    
            } else {
                tokenUDoc = tokenDocs[0];
                if (tokenUDoc.Document_Description__c == null)
                    tokenUDoc.Document_Description__c = tokenForm.Name;
                tokenUDoc.Booking_Doc_CreatedBy__c = tokenForm.createdBy.Name;
                tokenUDoc.Booking_Doc_CreatedDate__c = tokenForm.CreatedDate;
            }
        }
    }
    
    // To load booking unit document records
    public void loadBookingDocs () {
        attachments = new List <Attachment> ();
        bookingDocs = new List <Unit_Documents__c> ();
        
        List <ID> tokenResIds = new List <ID >();
        if (tokenUDoc != null)
            tokenResIds.add (tokenUDoc.Sys_Doc_ID__c);
        if (reservationUDoc != null)
            tokenResIds.add (reservationUDoc.Sys_Doc_ID__c);
        
        /*attachments = [SELECT Name, ParentID, bodylength, CreatedBy.Name, CreatedDate 
                       FROM Attachment 
                       WHERE parentId =: SRID 
                       AND ID NOT IN: tokenResIds
                       order By CreatedDate DESC];*/
        try {
            bookingDocs = [SELECT Document_Name__c, createdBy.Name, Created_Date_Time__c, Sys_Doc_ID__c, CV_id__c,
                           Booking_Doc_CreatedBy__c, Booking_Doc_CreatedDate__c, IS_Invalid__c, Document_type__c
                           FROM Unit_Documents__c
                           WHERE Service_Request__c =: SRID
                           AND (Sys_Doc_ID__c != NULL OR CV_id__c != NULL)
                           AND Document_type__c = 'Booking Docs'
                           AND Status__c = 'Uploaded' Order By Booking_Doc_CreatedDate__c, createdDate DESC];
           Map <ID, ID> contentDocs = new Map <ID, ID> ();
           Set <ID> contentDoc = new Set <ID> ();
           for (Unit_Documents__c doc : BookingDocs) {
           
               if (doc.Sys_Doc_ID__c != null && doc.CV_id__c == null) {
                   if (doc.Sys_Doc_ID__c.startsWith ('069')) {
                       contentDoc.add (doc.Sys_Doc_ID__c);
                   }
               }
           }
           for (ContentVersion version : [SELECT ContentDocumentId FROM ContentVersion WHERE ContentDocumentId IN:contentDoc AND IsLatest = TRUE]) {
               contentDocs.put (version.ContentDocumentId, version.Id);
           }
           for (Unit_Documents__c doc : BookingDocs) {
           
               if (doc.Sys_Doc_ID__c != null && doc.CV_id__c == null) {
                   if (doc.Sys_Doc_ID__c.startsWith ('069')) {
                       if (contentDocs.containsKey (doc.Sys_Doc_ID__c))
                           doc.CV_Id__c = contentDocs.get (doc.Sys_Doc_ID__c);
                   }
               }
           }
           
        } catch (Exception e) {}
        
    }
    
    @remoteAction
    public static String contentPublicUrl (Id contentVersionId) {
        try {
            ContentDistribution cd = new ContentDistribution();
            cd.Name = 'Document';
            cd.ContentVersionId = contentVersionId;
            cd.PreferencesAllowViewInBrowser = true;
            cd.PreferencesLinkLatestVersion = true;
            
            cd.PreferencesNotifyOnVisit = false;
            cd.PreferencesAllowOriginalDownload = false;
            cd.PreferencesAllowPDFDownload = false;
            cd.PreferencesPasswordRequired = false;
            insert cd;
            
            cd = [SELECT DistributionPublicUrl FROM ContentDistribution WHERE Id =: cd.id];
            return cd.DistributionPublicUrl;
        } catch (Exception e) {
            return e.getMessage()+' '+e.getLineNumber();
        }
    }
    
    // To take the token, reservation forms and create Unit documents under SR
    public void loadForms () {
        tokenUDoc = new Unit_Documents__c ();
        reservationUDoc = new Unit_Documents__c ();
        Attachment tokenForm = new Attachment ();
        try {
            
            tokenForm = [SELECT Name, ParentId, createdDate, createdBy.Name FROM Attachment 
                         WHERE Name =: srDetails.Token_Attachment_Name__c
                         AND ParentId =: SRID order By createdDate DESC LIMIT 1];
        } catch (Exception e) {}
        
        Attachment reservationForm = new Attachment ();
        try {
            reservationForm = [SELECT Name, ParentId, createdDate, createdBy.Name FROM Attachment 
                               WHERE Name =: srDetails.Reservation_Form_Attachment_Name__c
                               AND ParentId =: SRID order By createdDate DESC LIMIT 1];
        } catch (Exception e) {}
        
        if (tokenForm.ID != null) {
            List <Unit_documents__c> tokenDocs = new List <Unit_documents__c> ();
            tokenDocs = [SELECT Name, CV_Id__c, Service_Request__c, Document_Name__c, 
                         Document_Description__c, Sys_Doc_ID__c, Document_type__c, 
                         Booking_Doc_CreatedBy__c, Booking_Doc_CreatedDate__c
                         FROM Unit_documents__c WHERE Service_Request__c =: SRID AND Document_type__c = 'Booking Docs'
                         AND Document_Name__c = 'Token Copy'
                         Order by createddate DESC];
            if (tokenDocs.size () == 0){
                
                tokenUDoc.Service_Request__c = SRID;
                tokenUDoc.Document_Name__c = 'Token Copy';
                tokenUDoc.Document_Description__c = tokenForm.Name;
                tokenUDoc.Sys_Doc_ID__c = tokenForm.ID;
                tokenUDoc.Document_type__c = 'Booking Docs';
                tokenUDoc.Booking_Doc_CreatedBy__c = tokenForm.createdBy.Name;
                tokenUDoc.Booking_Doc_CreatedDate__c = tokenForm.CreatedDate;
                insert tokenUDoc;    
            } else {
                tokenUDoc = tokenDocs[0];
                if (tokenUDoc.Document_Description__c == null)
                    tokenUDoc.Document_Description__c = tokenForm.Name;
                tokenUDoc.Booking_Doc_CreatedBy__c = tokenForm.createdBy.Name;
                tokenUDoc.Booking_Doc_CreatedDate__c = tokenForm.CreatedDate;
            }
        }
        
        if (reservationForm.ID != null) {
            List <Unit_documents__c> reservationDocs = new List <Unit_documents__c> ();
            reservationDocs = [SELECT Name, CV_Id__c, Service_Request__c, Document_Name__c, 
                               Document_Description__c, Sys_Doc_ID__c, Document_type__c,
                               Booking_Doc_CreatedBy__c, Booking_Doc_CreatedDate__c
                               FROM Unit_documents__c WHERE Service_Request__c =: SRID AND Document_type__c = 'Booking Docs'
                               AND Document_Name__c = 'Reservation Copy' order by createdDate DESC];
            if (reservationDocs.size () == 0){
                
                reservationUDoc.Service_Request__c = SRID;
                reservationUDoc.Document_Name__c = 'Reservation Copy';
                reservationUDoc.Document_Description__c = reservationForm.Name;
                reservationUDoc.Sys_Doc_ID__c = reservationForm.ID;
                reservationUDoc.Document_type__c = 'Booking Docs';
                reservationUDoc.Booking_Doc_CreatedBy__c = reservationForm.createdBy.Name;
                reservationUDoc.Booking_Doc_CreatedDate__c = reservationForm.CreatedDate;
                insert reservationUDoc;    
            } else {
                reservationUDoc = reservationDocs[0];
                reservationUDoc.Booking_Doc_CreatedBy__c = reservationForm.createdBy.Name;
                if (reservationUDoc.Document_Description__c == null)
                    reservationUDoc.Document_Description__c = reservationForm.Name+'.pdf';
                reservationUDoc.Booking_Doc_CreatedDate__c = reservationForm.CreatedDate;
            }
        }
        
        if (reservationForm.Id == null) {   
            List <Unit_documents__c> reservationDocs = new List <Unit_documents__c> (); 
            reservationDocs = [SELECT Id, createdBy.Name, CreatedDate, Name, CV_Id__c, Service_Request__c, Document_Name__c,    
                                       Document_Description__c, Sys_Doc_ID__c, Document_type__c,    
                                       Booking_Doc_CreatedBy__c, Booking_Doc_CreatedDate__c 
                                FROM Unit_documents__c WHERE Service_Request__c =: srId     
                                AND Document_Name__c = 'Reservation Copy'   
                                AND Document_type__c = 'Booking Docs' 
                                Order By CreatedDate DESC  
                                LIMIT 1];   
                                
            if (reservationDocs.size () > 0){   
                reservationUDoc = reservationDocs[0];   
                if (reservationUDoc.Document_Description__c == null)
                    reservationUDoc.Document_Description__c = reservationUDoc.Name+'.pdf';
                reservationUDoc.Booking_Doc_CreatedBy__c = reservationUDoc.createdBy.Name;  
                reservationUDoc.Booking_Doc_CreatedDate__c = reservationUDoc.CreatedDate;       
            }   
        }
    
    }
    
    // To update the Content version id of token/reservation form in unit document records
    @remoteAction
    public static void updateDocCVID (String unitDocId, String fileId) {
        Unit_Documents__c doc = new Unit_Documents__c ();
        doc.Id = unitDocId;
        doc.CV_Id__c = fileId;
        doc.Status__c = 'Uploaded';
        update doc;
        ContentVersion version = [SELECT ContentDocumentId FROM ContentVersion WHERE id =: fileId ];
        ContentDocumentLink cd = New ContentDocumentLink(
            LinkedEntityId = unitDocId, 
            ContentDocumentId = version.ContentDocumentId, 
            shareType = 'V');
        insert cd;
        
    }
    
    // To load the unit document records
    public void getUnitDocs () {
        unitDocs = new List <Unit_Documents__c> ();
        try {
            unitDocs = [SELECT Document_Name__c, createdBy.Name, Created_Date_Time__c, Sys_Doc_ID__c, CV_id__c, IS_Invalid__c
                        FROM Unit_Documents__c
                        WHERE Service_Request__c =: SRID AND Document_Template__c != NULL 
                        AND (Sys_Doc_ID__c != NULL OR CV_id__c != NULL)
                        AND Document_type__c != 'Booking Docs'
                        AND Status__c = 'Uploaded' order By Created_Date_Time__c DESC];
            
        } catch (Exception e) {}
    
    }
    /*
    // To make the unit document as Invalid adn creating a place holder of new unit document
    public void makeUnitsInvalid () {
        String unitIds = apexpages.currentpage().getparameters().get('unitIds');
        List <ID> unitsToInvalid = new List <ID> ();
        if (unitIds.contains(';'))
            unitsToInvalid = unitIds.split(';');
        else
            unitsToInvalid.add (unitIds);
        
        List <Unit_Documents__c> invalidDocs = new List <Unit_Documents__c> ();
        List <Unit_Documents__c> clonedInvalidDocs = new List <Unit_Documents__c> ();
        for (Unit_Documents__c docs: [SELECT Service_Request__c, Buyer__c, Booking__c, Booking_Unit__c,
                                     Status__c, Document_Name__c, Document_Template__c, Document_type__c, Is_Invalid__c
                                      FROM Unit_Documents__c WHERE ID IN: unitsToInvalid]) 
        {
            docs.Is_Invalid__c = true;
            invalidDocs.add (docs);
            
            Unit_Documents__c invalidDoc = new Unit_Documents__c ();
            invalidDoc.Service_Request__c = docs.Service_Request__c;
            invalidDoc.Buyer__c = docs.Buyer__c;
            invalidDoc.Booking__c = docs.Booking__c;
            invalidDoc.Booking_Unit__c = docs.Booking_Unit__c;
            invalidDoc.Document_Name__c = docs.Document_Name__c;
            invalidDoc.Document_Template__c = docs.Document_Template__c;
            invalidDoc.Document_type__c = docs.Document_type__c;
            invalidDoc.Is_Invalid__c = false;
            invalidDoc.Id = NULL;
            invalidDoc.Status__c = 'Pending Upload';
            clonedInvalidDocs.add (invalidDoc);           
            
        }
        update invalidDocs;
        insert clonedInvalidDocs;
        getUnitDocs();
    }
    */
    
    // To get the booking units associated to the SR
    public void getBookingUnits () {
        bookingUnits = new List <Booking_unit__c> ();
        try {
            bookingUnits = [SELECT 
                            Booking_Unit_Type__c, Inventory__r.District__c, 
                            Inventory__r.IPMS_Bedrooms__c, Inventory__r.Check_List_Type__c,
                            Inventory__r.Unit_Name__c, Inventory__r.Unit_Type__c,
                            Inventory__r.Area_sft__c, Requested_Price_AED__c 
                            FROM Booking_unit__c 
                            WHERE Booking__r.Deal_SR__c =: SRID];
        } catch (Exception e) {}
        
    }
    
    
    // To display the deal team
    public void getDealTeam () {
        dealTeam = new Deal_Team__c ();
        try {
            dealTeam = [SELECT Associated_PC__c, Associated_PC__r.Name, Associated_PC__r.SmallPhotoUrl,
                        Associated_HOS__c, Associated_HOS__r.Name, Associated_HOS__r.SmallPhotoUrl,
                        Associated_HOD__c, Associated_HOD__r.Name, Associated_HOD__r.SmallPhotoUrl
                        FROM Deal_Team__c 
                        WHERE Associated_Deal__c =: SRID
                        AND PC_Team__c = 'PC Team'];
        } catch (Exception e) {}
    }
    
    
    // To get the checklist details and displaying in order wise based on custom meta data stored
    public void getCheckList () {
        stepDetails = new New_Step__c ();
        
        checkListMap = new Map<String, String>();
        prioritySubheadingMap = new Map<String, String>();
        prioritySet = new Set<String>();
        checkListPriorityMap = new Map<String, List<String>>();
        checkListRecordMap = new Map<String, Reservation_Form_Check_List__c> ();
                
        try {
            stepDetails = [SELECT Name, Is_Closed__c, Service_Request__c FROM New_Step__c 
                           WHERE 
                           Step_Status__c = 'Awaiting Mid Office Approval' 
                           AND Service_Request__c =: SRID];
        } catch (Exception e) {}
        
        if (stepDetails.ID == NULL) {
            for (Reservation_Form_Check_List__c chkList: [SELECT Id, Checklist_Item__c, Service_Request__c, Validated__c, Subheading__c
                                                         FROM Reservation_Form_Check_List__c
                                                         WHERE Service_Request__c =: SRID]){
                checkListRecordMap.put(chkList.Checklist_Item__c, chkList);
                
            }
            if (checkListRecordMap.size () > 0) {
                for(Mid_Office_Approval_Checklist__mdt item: [SELECT Id, MasterLabel, Checklist_Item__c, Subheading__c,
                                                                    Item_Priority__c, Item_Sublist_Priority__c
                                                               FROM Mid_Office_Approval_Checklist__mdt
                                                               ORDER BY Item_Priority__c ASC, Item_Sublist_Priority__c ASC])
                {
                    String subheading = 'Others';
                    List<string> itemList = new List<String>();
                    
                    if(item.subheading__c != null && item.subheading__c != ''){
                        subheading = item.subheading__c;
                        prioritySubheadingMap.put(item.Checklist_Item__c, String.valueOf(item.Item_Sublist_Priority__c));
                    }
                    String key = String.valueOf(item.Item_Priority__c);
                    if(checkListPriorityMap.containsKey(key)){
                        itemList = checkListPriorityMap.get(key);
                    }
                    if(!prioritySubheadingMap.containsKey(key)){
                        prioritySubheadingMap.put(key, subheading);
                    }
                    
                    itemList.add(item.Checklist_Item__c);
                    checkListPriorityMap.put(key, itemList);
                    String response = 'No';
                    if (checkListRecordMap.containsKey (item.Checklist_Item__c)) {
                        if (checkListRecordMap.get(item.Checklist_Item__c).Validated__c){
                            response = 'Yes';
                        }
                    }
                    checkListMap.put(item.Checklist_Item__c, response);
                }
                prioritySet = checkListPriorityMap.keyset();
            }
            
        }
        
    }
    
    
    // To get primary, joint buyer information
    public void getBuyers () {
        primaryBuyer = new List <Buyer__c> ();
        jointBuyers = new List <Buyer__c> ();
        try {
            primaryBuyer = [SELECT Primary_buyer__c, Title__c, First_Name__c, Last_Name__c, 
                            Email__c, Phone__c, Nationality__c, Passport_Number__c,
                            City__c, Country__c, Address_Line_1__c, Address_Line_2__c, Address_Line_3__c,
                            Address_Line_4__c
                            FROM Buyer__c WHERE Booking__r.Deal_SR__c =: SRID AND Primary_buyer__c = TRUE ];
            
            jointBuyers  = [SELECT Primary_buyer__c, Title__c, First_Name__c, Last_Name__c, 
                            Email__c, Phone__c, Nationality__c, Passport_Number__c,
                            City__c, Country__c, Address_Line_1__c, Address_Line_2__c, Address_Line_3__c,
                            Address_Line_4__c
                            FROM Buyer__c WHERE Booking__r.Deal_SR__c =: SRID AND Primary_buyer__c = FALSE ];
        } catch (Exception e) {}
    }
    
    // To get the content version id when it is uploaded from page
    @remoteAction
    public static ID getRelatedParentId (id docId, Id srId) {

        ContentVersion version = [SELECT ContentDocumentId FROM ContentVersion WHERE id =: docId ];
        ContentDocumentLink cd = New ContentDocumentLink(
            LinkedEntityId = srId, 
            ContentDocumentId = version.ContentDocumentId, 
            shareType = 'V');
        insert cd;
        
        return version.Id;
        
    }
    
    // to update the page view count when the document is previewed
    public void previewAttachment () {
        String attachmentId = apexpages.currentpage().getparameters().get('attachmentId');
        String fileId = apexpages.currentpage().getparameters().get('fileId');
        String source = apexpages.currentpage().getparameters().get('source');
        try {
            selectedAttachment = [SELECT Name, ParentId, CreatedBy.Name, CreatedDate FROM Attachment WHERE ID =: attachmentId ];
            attachmentURL = 'servlet/servlet.FileDownload?file='+attachmentId;
        } catch (Exception e) {
            attachmentURL = attachmentId;            
        }
        upsertViewCount (SRDetails.ID, attachmentId, attachmentURL, true, fileId, source);
    }
    
    
    // To get the file id from page view record when it is already loaded
    @remoteAction
    public static String getFileId (String srId, String attachmentId) {
        try {
            String fileId = '';
            
            Page_View__c view = [SELECT file_id__c FROM Page_View__c WHERE Is_File_View__c = TRUE AND attachment_id__c =: attachmentId 
                                 AND User__c =: UserInfo.getUserId() LIMIT 1];
            if (view.file_id__c != null) {
                fileId = view.file_id__c;
                String attachmentURL = 'servlet/servlet.FileDownload?file='+attachmentId;
                upsertViewCount (SRID, attachmentId, attachmentURL, true, fileId, 'DAMAC_SRAttachments');
            }
            return fileId;
        } catch (Exception e) {
            
            return '';
        }
    }
        
    // To track page view/attachment/file views
    public static void upsertViewCount (ID SRID, String type, String URL, Boolean isFileView, String fileId, String source) {
        String ip;
        string sessionInfo='';
        if(!Test.isRunningTest()){
            Map <String, String> currentSessionAttributes = Auth.SessionManagement.getCurrentSession();
            ip = currentSessionAttributes.get('SourceIp');
            
            for(string s:currentSessionAttributes.keyset()){
                sessionInfo = sessionInfo+'\n'+s+'-'+currentSessionAttributes.get(s);
            }
        }
        
        String objectName = 'NSIBPM__Service_Request__c';
        
        NSIBPM__Service_Request__c SRDetails = [SELECT Name, NSIBPM__Internal_Status_Name__c 
                                                FROM NSIBPM__Service_Request__c 
                                                WHERE ID =: sRID];
        list<Page_View__c> pv = new list<Page_View__c>();
        
        String key = userinfo.getuserid()+'-'+source+'-'+type+'-'+system.today().day()+'/'+system.today().Month()+'/'+system.today().year();
        
        pv = [SELECT View_Count__c,Unique_Key__c from Page_View__c where Unique_Key__c=:key limit 1];
        if(pv.size() == 0 ){
            Page_View__c p = new Page_View__c();
            p.Unique_Key__c = key;
            p.User__c = userinfo.getuserid();
            p.Last_Open_Date_time__c = system.now();
            p.Page_URL__c = string.valueof(system.URL.getOrgDomainUrl().gethost())+'/'+URL;
            p.View_Count__c = 1;    
            p.Log_Date__c = system.today();  
            p.IP_Address__c = ip ;
            p.Session_Info__c = sessionInfo;
            p.source__c = source;
            p.record_id__c = SRID;
            try {
                if (p.record_id__c != NULL) {
                    ID recordId = p.record_id__c;
                    String sObjectType = recordId.getSobjectType().getDescribe().getName();
                    Sobject acc = Database.query('SELECT RecordTypeId FROM '+sObjectType+' WHERE ID =: recordId');
                    String recordTypeId = ''+acc.get('RecordTypeId');
                    p.record_type_id__c = recordTypeId;
                    p.record_type_name__c = Schema.getGlobalDescribe().get(sObjectType).getDescribe().getRecordTypeInfosById().get(recordTypeId).getName();
                }
            } catch (Exception e) {}
            p.Is_File_View__c = isFileView;
            if (isFileView) {
                p.attachment_id__c = type;
                p.file_id__c = fileId;
            }
            p.record_name__c = SRDetails.Name;
            P.Object_Name__c = objectName;
            pv.add(p);  
        }else{
            pv[0].Last_Open_Date_time__c = system.now();
            pv[0].View_Count__c = pv[0].View_Count__c+1;  
            pv[0].IP_Address__c = ip ;
            pv[0].Session_Info__c = sessionInfo;
        }
        
        upsert pv Unique_Key__c;
    }
    
    @remoteAction
    public static String getDocumentName ( Id attachmentId, String docName) {
        Attachment att = new Attachment ();
        att = [SELECT name FROM Attachment WHERE Id =: attachmentId];
        String fileName = att.name;
        String fileType = filename.substring(filename.lastIndexOf('.') + 1);
        return docName+'.'+fileType;
    }
}