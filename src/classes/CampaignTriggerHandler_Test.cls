@isTest
public class CampaignTriggerHandler_Test {

    public static testmethod void UnitTest()
    {
        List<campaign__c> cmpList = testdatafactory.createCampaignRecords(new List<campaign__c>{new campaign__c(End_Date__c=system.today()+2,Marketing_End_Date__c=system.today()+2),new campaign__c()});
        List<campaign__c> cmpList1 = testdatafactory.createCampaignRecords(new List<campaign__c>{new campaign__c(Parent_Campaign__c=cmpList[0].id, End_Date__c=system.today()+2,Marketing_End_Date__c=system.today()+2),new campaign__c()});
        //select id,name,Inventory__c,Inventory__r.Is_Assigned__c,campaign__c from Campaign_Inventory__c where Inventory__r.Is_Assigned__c=true
        update cmpList ;
        System.Debug (cmpList1[0].Parent_Campaign__c);
        CampaignTriggerHandler obj = new CampaignTriggerHandler ();
        obj.MarkAllChildsWithParentPriority (cmpList);
        Inventory__c inv=new Inventory__c();
        inv.Is_Assigned__c=true;
        insert inv;
        
        Campaign_Inventory__c cmpInv=new Campaign_Inventory__c();
        cmpInv.Campaign__c=cmpList[0].id;
        cmpInv.Inventory__c=inv.id;
        insert cmpInv;
        
        Map <ID, Sobject> newMap = new Map <ID, Sobject> ();
        Map <ID, Sobject> oldMap = new Map <ID, Sobject> ();
        oldMap.put (cmpList[0].id, cmpList[0]);
        cmpList[0].Parent_Campaign__c=cmpList[1].id;
        cmpList[0].End_Date__c=system.today();
        cmpList[0].Marketing_End_Date__c=system.today();
        update cmpList;
        newMap.put (cmpList[0].id, cmpList[0]);
        
        obj.UpdateIsAssignedInventory (newMap, oldMap);
        CampaignTriggerHandler controller=new CampaignTriggerHandler();
       // controller.executeAfterDeleteTrigger(new list<sObject>());
        
        controller.executeBeforeInsertUpdateTrigger(new list<sObject>(), new Map<Id,sObject>());
        controller.executeBeforeDeleteTrigger(new Map<Id, sObject>());
        controller.executeAfterInsertTrigger(new Map<Id,sObject>());
        controller.executeAfterUpdateTrigger(new Map<Id,sObject>(), new Map<Id,sObject>());
        controller.executeAfterDeleteTrigger(new Map<Id,sObject>());
    
    }
}