@isTest
public class HDApp_WorkPermitOTP_APITest {
	@TestSetup
    static void TestData() {
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(Agency_ID__c = '1234');
        insert sr;
        
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account(LastName = 'Test Account',
                                      Party_ID__c = '63062',
                                      RecordtypeId = rtId,
                                      Email__pc = 'test@mailinator.com',
                                      Mobile__c='11111111111');
        insert account;
        
        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];
        
        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Customer Community Login User(Use this)%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
        Database.insert(user1);
        
        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'test';
        locObj.Location_Type__c = 'Building';
        locObj.Location_ID__c = '12345';
        locObj.UAEProp__c = true;
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        invObj.Property_Country__c = 'UNITED ARAB EMIRATES';
        invObj.Marketing_Name_Doc__c = invObj.Id;
        invObj.Building_Location__c = locObj.Id;
        invObj.Property_Status__c = 'Ready'; 
        invObj.Unit_type__c = 'Villa';
        insert invObj;
        
        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1204',
                                      Registration_ID__c = '3901',
                                      Handover_Flag__c = 'N',
                                      Inventory__c = invObj.id,
                                      Dummy_Booking_Unit__c = true);
        insert bookingUnit;
        
        Id rtPopId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Work Permit').getRecordTypeId();
        FM_Case__c objWPFmCaseContractor = new FM_Case__c();
        objWPFmCaseContractor.Origin__c = 'Portal';
        objWPFmCaseContractor.isHelloDamacAppCase__c = true;
        objWPFmCaseContractor.Account__c = account.Id;
        objWPFmCaseContractor.RecordtypeId = rtPopId;
        objWPFmCaseContractor.Status__c = 'Draft Request';
        objWPFmCaseContractor.Request_Type__c = 'Work Permit';
        objWPFmCaseContractor.Booking_Unit__c = bookingUnit.Id;
        objWPFmCaseContractor.Work_Permit_Type__c = 'Work Permit - Including Fit Out';
        objWPFmCaseContractor.Person_To_Collect__c = 'Contractor';
        objWPFmCaseContractor.Contact_person_contractor__c = 'Test Contractor';
        objWPFmCaseContractor.Email_2__c = 'testctr@test.com';
        
        insert objWPFmCaseContractor;
    }
    
    @isTest
    static void testSendOTP() {
        Account account = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c bookingUnit = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        
        Test.startTest();
		
        List<FM_Case__c> listFMCase = [SELECT Id,Name FROM FM_Case__c WHERE Person_To_Collect__c='Contractor' LIMIT 1];
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/workPermitOTP/srSubmission';
        req.addParameter('accountId',account.Id);
        req.addParameter('bookingUnitId',bookingUnit.Id);
        req.addParameter('fmCaseId',listFMCase[0].Id);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WorkPermitOTP_API.getSRSubmissionOTP();
        
        Test.stopTest();
    }
    
    @isTest
    static void testSendOTPWithNoAccountId() {
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/workPermitOTP/srSubmission';
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WorkPermitOTP_API.getSRSubmissionOTP();
        
        Test.stopTest();
    }
    
    @isTest
    static void testSendOTPWithNoBookingUnitId() {
        Account account = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/workPermitOTP/srSubmission';
        req.httpMethod = 'GET';
        req.addParameter('accountId',account.Id);
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WorkPermitOTP_API.getSRSubmissionOTP();
        
        Test.stopTest();
    }
    
    @isTest
    static void testSendOTPWithNoFMCaseId() {
        Account account = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c bookingUnit = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/workPermitOTP/srSubmission';
        req.httpMethod = 'GET';
        req.addParameter('accountId',account.Id);
        req.addParameter('bookingUnitId',bookingUnit.Id);
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WorkPermitOTP_API.getSRSubmissionOTP();
        
        Test.stopTest();
    }
    
    @isTest
    static void testSendOTPWithInvalidFMCaseId() {
        Account account = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c bookingUnit = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/workPermitOTP/srSubmission';
        req.httpMethod = 'GET';
        req.addParameter('accountId',account.Id);
        req.addParameter('bookingUnitId',bookingUnit.Id);
        req.addParameter('fmCaseId','abcdefgh');
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WorkPermitOTP_API.getSRSubmissionOTP();
        
        Test.stopTest();
    }
}