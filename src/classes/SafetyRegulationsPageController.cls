public without sharing class SafetyRegulationsPageController {

    public  FM_Case__c      objFmCase;
    public  List<String>    lstTerms            {get;set;}
    public  String          dateStr             {get;set;}
    public  List<String>    lstHeader           {get;set;}
    public  List<String>    lstFooter           {get;set;}
    public  String          recepient           {get;set;}
    public  String          subject             {get;set;}
    public  String          signatureName       {get;set;}
    public  String          dateStrFooter       {get;set;}
    public  Id              caseId;

    public  String          DAMAC_HILLS = 'DAMAC Hills Community Management';

    public SafetyRegulationsPageController() {
        DateTime objDateTime = System.Now();
        dateStr = objDateTime.format('dd MMMMM yyyy');
        dateStrFooter = objDateTime.format('dd MMMMM yyyy HH:mm','Asia/Dubai');

        getCaseDetails();
        getContent();
    }

    public PageReference getCaseDetails() {
        System.debug('>>>---caseId--- : '+caseId);
        caseId = ApexPages.currentPage().getParameters().get('CaseId');
        System.debug('>>>---caseId--- : '+caseId);
        objFmCase = new FM_Case__c();

        if(caseId != NULL && String.isNotBlank(caseId)) {
            objFmCase = [ SELECT Account__c
                                , Account__r.Name
                                , Booking_Unit__c
                                , Booking_Unit__r.Unit_Name__c
                                , Booking_Unit__r.Property_Name__c
                                , Tenant__c
                                , Tenant__r.Name
                                , Request_Type_DeveloperName__c
                          FROM FM_Case__c
                          WHERE id = :caseId ];
        }
        System.debug('>>>---objFmCase--- : '+objFmCase);
        return NULL;
    }

    public PageReference getContent() {

        List<Safety_Undertaking_Content__mdt> lstUndertakingContent =
                                                new List<Safety_Undertaking_Content__mdt>();
        if(objFmCase.Booking_Unit__r.Property_Name__c.containsIgnoreCase('DAMAC Hills')) {
            lstUndertakingContent = [ SELECT id
                                         , Safety_Undertaking_Body__c
                                         , Safety_Undertaking_Letter__c
                                         , Safety_Undertaking_Letter__r.Recipient__c
                                         , Safety_Undertaking_Letter__r.Subject__c
                                         , Order__c
                                         , Body__c
                                  FROM Safety_Undertaking_Content__mdt
                                  WHERE Safety_Undertaking_Letter__r.Recipient__c = :DAMAC_HILLS
                                  ORDER BY Safety_Undertaking_Body__c, Order__c
                                ];
        }
        else {
            lstUndertakingContent = [ SELECT id
                                         , Safety_Undertaking_Body__c
                                         , Safety_Undertaking_Letter__c
                                         , Safety_Undertaking_Letter__r.Recipient__c
                                         , Safety_Undertaking_Letter__r.Subject__c
                                         , Order__c
                                         , Body__c
                                  FROM Safety_Undertaking_Content__mdt
                                  WHERE Safety_Undertaking_Letter__r.Recipient__c != :DAMAC_HILLS
                                  ORDER BY Safety_Undertaking_Body__c, Order__c
                                ];
        }

        System.debug('>>>---lstUndertakingContent--- : '+lstUndertakingContent);

        List<String> headerList = new List<String>();
        lstTerms = new List<String>();
        lstHeader = new List<String>();
        lstFooter = new List<String>();
        for(Safety_Undertaking_Content__mdt objContent : lstUndertakingContent) {
             System.debug('>>>---objContent--- : '+objContent);
             recepient = objContent.Safety_Undertaking_Letter__r.Recipient__c;
             subject = objContent.Safety_Undertaking_Letter__r.Subject__c;

             if(objContent.Safety_Undertaking_Body__c == '1Header') {
                headerList.add(objContent.Body__c);
             }
             if(objContent.Safety_Undertaking_Body__c == '2Terms') {
                lstTerms.add(objContent.Body__c);
             }
             if(objContent.Safety_Undertaking_Body__c == '3Footer') {
                lstFooter.add(objContent.Body__c);
             }
        }
        System.debug('>>>---headerList--- : '+headerList);
        System.debug('>>>---lstTerms--- : '+lstTerms);
        System.debug('>>>---lstFooter--- : '+lstFooter);

        String tenantName = '';
        if( String.isNotBlank( ApexPages.currentPage().getParameters().get('tenantName')) ) {
            tenantName = ApexPages.currentPage().getParameters().get('tenantName');
        }
        System.debug('--- tenantName --- : '+tenantName);

        for(String str : headerList) {
            String replacedStr = '';
            if(String.isNotBlank(tenantName)) {
                System.debug('Replace with Tenant Name');
                replacedStr = str.replace('{!Account}',tenantName);
                signatureName = tenantName;
            }else {
                System.debug('Replace with Account Name');
                if(objFmCase.Request_Type_DeveloperName__c == 'Move_in_Request') {
                  replacedStr = str.replace('{!Account}', objFmCase.Account__r.Name);
                  signatureName = objFmCase.Account__r.Name;
                }else if(objFmCase.Request_Type_DeveloperName__c == 'Move_in_Request_T') {
                  replacedStr = str.replace('{!Account}', objFmCase.Tenant__r.Name);
                  signatureName = objFmCase.Tenant__r.Name;
                }
            }
            replacedStr = replacedStr.replace('{!Unit}',objFmCase.Booking_Unit__r.Unit_Name__c);
            replacedStr = replacedStr.replace('{!Property}',objFmCase.Booking_Unit__r.Property_Name__c);
            System.debug('>>>---replacedStr--- : '+replacedStr);
            lstHeader.add(replacedStr);
            System.debug('>>>---lstHeader--- : '+lstHeader);
        }
        System.debug('>>>---lstHeader--- : '+lstHeader);
        return NULL;
    }
}