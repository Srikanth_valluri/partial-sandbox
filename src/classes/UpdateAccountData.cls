/****************************************************************************************************
* Name               : UpdateAccountData                                                            *
* Description        : This is the apex class for updating Buyer data with Account data             *
* Created Date       : 14/06/2018                                                                   *
* Created By         : Eternus                                                                      *
* -----------------------------------------------------------------------------------------------   *
* VER   AUTHOR          DATE            DESCRIPTION                                                 *
* 1.0   Monali          14/06/2018      Initial Draft                                               *
* 1.1   Craig           24-06-2018      commented the code to update the Mobile Number              *
****************************************************************************************************/
public with sharing class UpdateAccountData {
    public static void UpdateAccountDataOnBuyer(Map<Id, sObject> mapNewRecords, Map<Id, sObject> mapOldRecords){
        System.debug('==========UpdateAccountDataOnBuyer=============');
        List<Buyer__c> updateBuyerList = new List<Buyer__c>();
        Map<Id, List<Buyer__c>> mapAccountIdBuyer = new Map<Id, List<Buyer__c>>();

        for(Id thisKey : mapNewRecords.keySet()){
            Buyer__c newBuyer = (Buyer__c)mapNewRecords.get(thisKey);
            Buyer__c oldBuyer = (Buyer__c)mapOldRecords.get(thisKey);
            System.debug('==========OldBuyer==='+oldBuyer);
            System.debug('==========NewBuyer==='+newBuyer);
            // oldBuyer.Service_Request__c == null &&         Removed by Gopinadh
               
            if( newBuyer.Service_Request__c != null
                && newBuyer.Account__c != null
            ) {
            //if( newBuyer.Service_Request__c != null && newBuyer.Account__c != null){
                System.debug('========Service request on buyer is updated==='+newBuyer);    
                
                if(!mapAccountIdBuyer.containsKey(newBuyer.Account__c)){
                    updateBuyerList = new List<Buyer__c>();
                    updateBuyerList.add(newBuyer);
                    mapAccountIdBuyer.put(newBuyer.Account__c,updateBuyerList);
                } else {
                    updateBuyerList = new List<Buyer__c>();
                    updateBuyerList = mapAccountIdBuyer.get(newBuyer.Account__c);
                    updateBuyerList.add(newBuyer);
                    mapAccountIdBuyer.put(newBuyer.Account__c,updateBuyerList);
                }
            }
        }
        System.debug('==========mapAccountIdBuyer==='+mapAccountIdBuyer);
        if(mapAccountIdBuyer.size() > 0){
            UpdateBuyer(mapAccountIdBuyer);
        }
    }

    public static void UpdateBuyer(Map<Id, List<Buyer__c>> mapAccountIdBuyer) {
        List<Buyer__c> updateBuyerList = new List<Buyer__c>();
        System.debug('====UpdateBuyer=mapAccountIdBuyer===='+mapAccountIdBuyer);
        List<Account> listAccount = [SELECT Id, Name, IsPersonAccount, First_Name__c,MiddleName,FirstName,Mobile_Phone_Encrypt__pc,
                                            LastName, Last_Name__c, Email__pc, Email__c, Mobile__c,Middle_Name__c,
                                            Mobile_Country_Code__c, Passport_Number__pc, Passport_Number__c,
                                            Nationality__pc, Nationality__c, City__c, City__pc,
                                            Country__pc, Country__c, Address_Line_1__pc,
                                            Address_Line_2__pc, Address_Line_3__pc,
                                            Address_Line_4__pc, Address_Line_1__c,
                                            Address_Line_2__c, Address_Line_3__c,
                                            Address_Line_4__c, PersonTitle,
                                            P_O_Box_Zip_Postal_Code__c
                                       FROM Account
                                      WHERE Id IN :mapAccountIdBuyer.keySet()
        ];
        System.debug('====UpdateBuyer=listAccount===='+listAccount);
        for(Account account : listAccount){
            System.debug('====account=='+account);
            for(Buyer__c buyer : mapAccountIdBuyer.get(account.Id)){
                buyer.Title__c = account.PersonTitle != null ? account.PersonTitle : buyer.Title__c;
                //buyer.Phone__c = account.Mobile__c != null ? account.Mobile__c : buyer.Phone__c;
                //buyer.Phone_Country_Code__c = account.Mobile_Country_Code__c != null ? account.Mobile_Country_Code__c : buyer.Phone_Country_Code__c;
                buyer.Email__c = account.Email__pc != null ? account.Email__pc : account.Email__c;
                buyer.Zip_Code__c = account.P_O_Box_Zip_Postal_Code__c;
                System.debug('sssssssssssssssss'+account.IsPersonAccount);
                if(account.IsPersonAccount){
                    if ( buyer.Phone_Country_Code__c == null && buyer.Phone_with_Country_Code__c == null ) {
                        buyer.Phone__c = account.Mobile_Phone_Encrypt__pc;
                    }
                    buyer.First_Name__c = account.FirstName != null ? account.FirstName : buyer.First_Name__c;
                    buyer.Middle_Name__c = account.MiddleName != null ? account.MiddleName : buyer.Middle_Name__c;
                    buyer.Last_Name__c = account.LastName != null ? account.LastName : buyer.Last_Name__c;
                    buyer.Passport_Number__c = account.Passport_Number__pc != null ? account.Passport_Number__pc : buyer.Passport_Number__c;
                    buyer.Nationality__c = account.Nationality__pc != null ? account.Nationality__pc : buyer.Nationality__c;
                    buyer.Address_Line_1__c = account.Address_Line_1__pc != null ? account.Address_Line_1__pc : buyer.Address_Line_1__c;
                    buyer.Address_Line_2__c = account.Address_Line_2__pc != null ? account.Address_Line_2__pc : buyer.Address_Line_2__c;
                    buyer.Address_Line_3__c = account.Address_Line_3__pc != null ? account.Address_Line_3__pc : buyer.Address_Line_3__c;
                    buyer.Address_Line_4__c = account.Address_Line_4__pc != null ? account.Address_Line_4__pc : buyer.Address_Line_4__c;
                    buyer.Country__c = account.Country__pc != null ? account.Country__pc : buyer.Country__c;
                    if ( buyer.Country__c != null ) {
                        if (account.City__pc != null) {
                            List <String> cityList = getCityValues(buyer.Country__c);
                            if ( cityList.contains(account.City__pc) ) {
                                buyer.City__c = account.City__pc ;                        
                            } else {
                                buyer.City__c = null;
                            }
                            buyer.City_Text__c = account.City__pc;
                        } else if ( account.City__c != null ) {
                            List <String> cityList = getCityValues(buyer.Country__c);
                            if ( cityList.contains(account.City__c) ) {
                                buyer.City__c = account.City__c ;                        
                            } else {
                                buyer.City__c = null;
                            }
                            buyer.City_Text__c = account.City__c;
                        }
                    }
                    
                    
                } else {
                    buyer.First_Name__c = account.First_Name__c != null ? account.First_Name__c : buyer.First_Name__c;
                    buyer.Middle_Name__c = account.Middle_Name__c != null ? account.Middle_Name__c : buyer.Middle_Name__c;
                    buyer.Last_Name__c = account.Last_Name__c != null ? account.Last_Name__c : buyer.Last_Name__c;
                    buyer.Passport_Number__c = account.Passport_Number__c != null ? account.Passport_Number__c : buyer.Passport_Number__c;
                    buyer.Nationality__c = account.Nationality__c != null ? account.Nationality__c : buyer.Nationality__c;
                    buyer.Address_Line_1__c = account.Address_Line_1__c != null ? account.Address_Line_1__c : buyer.Address_Line_1__c;
                    buyer.Address_Line_2__c = account.Address_Line_2__c != null ? account.Address_Line_2__c : buyer.Address_Line_2__c;
                    buyer.Address_Line_3__c = account.Address_Line_3__c != null ? account.Address_Line_3__c : buyer.Address_Line_3__c;
                    buyer.Address_Line_4__c = account.Address_Line_4__c != null ? account.Address_Line_4__c : buyer.Address_Line_4__c;
                    buyer.Country__c = account.Country__c != null ? account.Country__c : buyer.Country__c;
                    if (account.City__c != null) {
                        List <String> cityList = getCityValues(buyer.Country__c);
                        if ( cityList.contains(account.City__c) ) {
                            buyer.City__c = account.City__c ;                        
                        }
                    }
                    buyer.City_Text__c = account.City__c;
                    
                     
                }
                updateBuyerList.add(buyer);
            }
        }
        System.debug('====updateBuyerList=='+updateBuyerList);
    }
    public static List <String> getCityValues  (String countryVal) {
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get ('Buyer__c').getDescribe ().fields.getMap ();
        
        Schema.DescribeFieldResult theFieldResult = fieldMap.get('City__c').getDescribe();
        Schema.sObjectField theField = theFieldResult.getSObjectField();
        
        Schema.DescribeFieldResult ctrlFieldResult = fieldMap.get('Country__c').getDescribe();
        Schema.sObjectField ctrlField = ctrlFieldResult.getSObjectField();
        
        
        Map<String, List<String>> results = DependentPickListFields.getDependentOptionsImpl(theField, ctrlField);
        if (results.containsKey (countryVal)) {
            return results.get (countryVal);
        }
        else
            return null;
    }
}