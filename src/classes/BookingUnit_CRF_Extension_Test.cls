@isTest
public class BookingUnit_CRF_Extension_Test {
    static Account  objAcc;
    static Case objCase;
    static NSIBPM__Service_Request__c objSR;
    static Booking__c objBooking;
    static Booking_Unit__c objBookingUnit;
    
    static testMethod void Test1(){
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person',party_ID__C='123456');
        insert objAcc; 
        
        objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'Test Unit';
        objBookingUnit.Booking__c  = objBooking.id;        
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        insert objBookingUnit;
        
        
        test.StartTest();
         SOAPCalloutServiceMock.returnToMe = new Map<String, GenerateCRFService.GetCustomerRequestFormResponse_element>();
         GenerateCRFService.GetCustomerRequestFormResponse_element response1 = new GenerateCRFService.GetCustomerRequestFormResponse_element();
         response1.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=403501 and Request Id :40815712 ...","ATTRIBUTE3":"403501","ATTRIBUTE2":"40815712","ATTRIBUTE1":"null","PARAM_ID":"74428"}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';
         SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
         Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
         
         ApexPages.StandardController sc = new ApexPages.StandardController(objBookingUnit);                 
         BookingUnit_CRF_Extension objClass = new BookingUnit_CRF_Extension(sc);
         
         objClass.ShowCRF(); 
         
         objClass.back();      
        
         test.StopTest();
         
        
    }
    
     static testMethod void Test2(){
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person',party_ID__C='123456');
        insert objAcc; 
        
        objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'Test Unit';
        objBookingUnit.Booking__c  = objBooking.id;        
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        insert objBookingUnit;
        
        
        test.StartTest();
         SOAPCalloutServiceMock.returnToMe = new Map<String, GenerateCRFService.GetCustomerRequestFormResponse_element>();
         GenerateCRFService.GetCustomerRequestFormResponse_element response1 = new GenerateCRFService.GetCustomerRequestFormResponse_element();
         response1.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=403501 and Request Id :40815712 ...","ATTRIBUTE3":"403501","ATTRIBUTE2":"40815712","ATTRIBUTE1":"null","PARAM_ID":"74428"}],"message":"Process Completed Returning 1 Response Message(s)...","status":"E"}';
         SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
         Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
         
         ApexPages.StandardController sc = new ApexPages.StandardController(objBookingUnit);                 
         BookingUnit_CRF_Extension objClass = new BookingUnit_CRF_Extension(sc);
         
         objClass.ShowCRF(); 
                       
        
         test.StopTest();                 
    }
           
}