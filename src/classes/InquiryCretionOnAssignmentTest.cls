@isTest
public class InquiryCretionOnAssignmentTest{
    private static testMethod void testOne(){
        
        //RecordTypeIdContact = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        //SRTemplateList =  InitialiseTestData.createTestTemplateRecords(new List<NSIBPM__SR_Template__c>{new NSIBPM__SR_Template__c()});
        Account a = new Account();
        a.Name = 'TestAccount';
        a.Agency_Short_Name__c = 'testShrName';
        insert a;
        
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        //sr.recordtypeid=RecordTypeIdContact;
        //sr.NSIBPM__SR_Template__c = SRTemplateList[0].Id;
        sr.Eligible_to_Sell_in_Dubai__c = true;
        sr.Agency_Type__c = 'Individual';
        sr.ID_Type__c = 'Passport';
        sr.Agency__c = a.id;
        sr.Agency_Email_2__c = 'test2@gmail.com';
        sr.Alternate_Agency_Email_2__c = 'test1@gmail.com';
        sr.Country_of_Sale__c = 'UAE;KSA;Lebanon';
        insert sr;
        
        Property__c onjprop = new Property__c();
        onjprop.name='DAMAC HEIGHTS';
        onjprop.Property_ID__c =2971;
        insert onjprop;
        
        Booking__c book = new booking__c();
        book.Deal_SR__c = sr.id;
        book.Booking_Channel__c = 'Web';
        insert book;
        
        Location__c loc = new Location__c();
        loc.Location_Code__c =  'test';
        loc.Location_ID__c = 'testloc2010';
        insert loc;
        
        Inventory__c  inv = new Inventory__c ();
        //inventory
        inv.Unit_Location__c = loc.id;
        inv.Property_Country__c = 'UAE';
        inv.Property__c = onjprop.id;
        inv.Marketing_Name__c ='DAMAC HEIGHTS';
        inv.Status__c = 'Released';
        inv.Is_Assigned__c = false;
        inv.List_Price__c = 900;
        upsert inv;
        
        Booking_Unit__c bu = new Booking_Unit__c();    
        bu.Booking__c = book.id;
        bu.Payment_Method__c = 'Cash';
        bu.Primary_Buyer_s_Email__c = 'raviteja@nsiglobal.com';
        bu.Primary_Buyer_s_Name__c = 'testNSI';
        bu.Primary_Buyer_s_Nationality__c = 'test';
        bu.Inventory__c = inv.id;
        insert bu;
        
        
        Inquiry__c inquiryRecord = new Inquiry__c();
        inquiryRecord.By_Pass_Validation__c = true;
        inquiryRecord.Party_ID__c = '12345';
        inquiryRecord.Title__c = 'MR.';
        inquiryRecord.Title_Arabic__c ='MR.';
        inquiryRecord.First_Name__c = 'Test';
        inquiryRecord.First_Name_Arabic__c ='Test';
        inquiryRecord.Last_Name__c ='Test';
        inquiryRecord.Last_Name_Arabic__c = 'Test';
        insert inquiryRecord;
        
        buyer__c b = new buyer__c();        
        b.Buyer_Type__c =  'Individual';
        b.Address_Line_1__c =  'Ad1';
        b.Country__c =  'United Arab Emirates';
        b.City__c = 'Dubai' ;
        b.Inquiry__c =inquiryRecord.Id ;
        b.Date_of_Birth__c = string.valueof(system.today().addyears(-30)) ;
        b.Email__c = 'test@test.com';
        b.First_Name__c = 'firstname' ;
        b.Primary_Buyer__c = true;
        b.Last_Name__c =  'lastname';
        b.Nationality__c = 'Indian' ;
        b.Passport_Expiry_Date__c = string.valueof(system.today().addyears(20)) ;
        b.Passport_Number__c = 'J0565556' ;
        b.Phone__c = '569098767' ;
        b.Phone_Country_Code__c = 'United Arab Emirates: 00971' ;
        b.Place_of_Issue__c =  'India';
        b.Title__c = 'Mr';
        b.booking__c = book.id;
        b.Primary_Buyer__c = false;
        insert b;
        
        Case objCase = new Case();
        objCase.Account = a;
        objCase.Status = 'Submitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Type = 'Offer & Acceptance Letter';  
        objCase.Account_Email__c = 'test@gmail.com';
        objCase.Buyer__c = b.Id;
        insert objCase;
        List<Id> caseIds = new List<Id>();
        caseIds.add(objCase.Id);
        InquiryCretionOnAssignment obj = new InquiryCretionOnAssignment();
        InquiryCretionOnAssignment.createInquiry(caseIds);
    }
}