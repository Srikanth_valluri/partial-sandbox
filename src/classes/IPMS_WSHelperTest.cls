@istest
public class IPMS_WSHelperTest{

static testmethod void IPSMWSHelper(){
test.starttest();
string sbody = '';
sbody =IPMS_WSHelper.sSoaHeader('IPMS_webservice','oracleuser','testpassword');
IPMS_WSHelper.GetFormattedDateTime(System.now());
IPMS_WSHelper.getCreatableFieldsSOQL('Booking_Unit__c');

String body='';
body+= '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xxdc="http://xmlns.oracle.com/apps/ont/soaprovider/plsql/xxdc_process_service_ws/" xmlns:proc="http://xmlns.oracle.com/apps/ont/soaprovider/plsql/xxdc_process_service_ws/process/">';
       body+='<soapenv:Header>';
          body+='<xxdc:SOAHeader>';
             //body+='<!--Optional:-->';
             body+='<xxdc:Responsibility>ONT_ICP_SUPER_USER</xxdc:Responsibility>';
             //body+='<!--Optional:-->';
             body+='<xxdc:RespApplication>ONT</xxdc:RespApplication>';
             //body+='<!--Optional:-->';
             body+='<xxdc:SecurityGroup>standard</xxdc:SecurityGroup>';
             //body+='<!--Optional:-->';
             body+='<xxdc:NLSLanguage>american</xxdc:NLSLanguage>';
             //body+='<!--Optional:-->';
             body+='<xxdc:Org_Id/>';
          body+='</xxdc:SOAHeader>';
          body+='<wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">';
             body+='<wsse:UsernameToken>';
                body+='<wsse:Username>oracleuser</wsse:Username>';
                body+='<wsse:Password>testpassword</wsse:Password>';
             body+='</wsse:UsernameToken>';
          body+='</wsse:Security>';
       body+='</soapenv:Header>';
  System.AssertEquals(body,sBody);     
test.stoptest();
}
}