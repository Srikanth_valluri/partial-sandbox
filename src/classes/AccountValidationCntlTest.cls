@isTest
public class AccountValidationCntlTest {
public static final String strBookingUnitActiveStatus = 'Agreement executed by DAMAC';
    @IsTest
    static void positiveTestCase1(){
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        
        Location__c objL = new Location__c();
        objL.Name = 'DFA';
        objL.Location_Id__c = 'DFA';
        objL.Building_Name__c = 'test';
        objL.Location_Type__c = 'Building';
        insert objL;
        AccountValidationCntl.lookupBuilding('test');
        
    }
    
    @IsTest
    static void positiveTestCase2(){
         List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Address_Line_1__c = 'XYZ';
        acc.Country__c = 'Canada';
        acc.City__c = 'Barcelona';
        acc.State__c = 'lmn';
        acc.Zip_Postal_Code__c = '111';
        insert acc;
        
        Location__c objL = new Location__c();
        objL.Name = 'DFA';
        objL.Location_Id__c = 'DFA';
        objL.Building_Name__c = 'test';
        objL.Location_Type__c = 'Building';
        insert objL;
        
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        objInventory.Building_Location__c = objL.Id;
        insert objInventory;
        
         NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;
        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(acc.Id,objDealSR.Id,1);
        insert bookingList;
        
       
        
        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList)
        {
          objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
          objBookingUnit.Inventory__c = objInventory.Id;
          objBookingUnit.Registration_ID__c = '74712';
          objBookingUnit.Early_Handover__c = true;
        }
        insert bookingUnitList;
        AccountValidationCntl objAccountValidation = new AccountValidationCntl();

        AccountValidationCntl.lookupUnit('test',objL.Id);
        
    }
    
    @IsTest
    static void positiveTestCase3(){
         List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Address_Line_1__c = 'XYZ';
        acc.Country__c = 'Canada';
        acc.City__c = 'Barcelona';
        acc.State__c = 'lmn';
        acc.Zip_Postal_Code__c = '111';
        acc.Mobile__c = '911234567890';
        insert acc;
        
        Location__c objL = new Location__c();
        objL.Name = 'DFA';
        objL.Location_Id__c = 'DFA';
        objL.Building_Name__c = 'test';
        objL.Location_Type__c = 'Building';
        insert objL;
        
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        objInventory.Building_Location__c = objL.Id;
        insert objInventory;
        
         NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;
        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(acc.Id,objDealSR.Id,1);
        insert bookingList;
        
       
        
        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList)
        {
          objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
          objBookingUnit.Inventory__c = objInventory.Id;
          objBookingUnit.Registration_ID__c = '74712';
          objBookingUnit.Early_Handover__c = true;
        }
        insert bookingUnitList;
        AccountValidationCntl objAccountValidation = new AccountValidationCntl();
        objAccountValidation.phone = '911234567890';
        objAccountValidation.checkDraftCase();
        AccountValidationCntl.lookupUnit('test',objL.Id);
        
    }
    
     @IsTest
    static void positiveTestCase4(){
         List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Address_Line_1__c = 'XYZ';
        acc.Country__c = 'Canada';
        acc.City__c = 'Barcelona';
        acc.State__c = 'lmn';
        acc.Zip_Postal_Code__c = '111';
        acc.Mobile__c = '911234567890';
        acc.Email__c = 'Test@gmail.com';
        insert acc;
        
        Location__c objL = new Location__c();
        objL.Name = 'DFA';
        objL.Location_Id__c = 'DFA';
        objL.Building_Name__c = 'test';
        objL.Location_Type__c = 'Building';
        insert objL;
        
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        objInventory.Building_Location__c = objL.Id;
        insert objInventory;
        
         NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;
        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(acc.Id,objDealSR.Id,1);
        insert bookingList;
        
       
        
        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList)
        {
          objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
          objBookingUnit.Inventory__c = objInventory.Id;
          objBookingUnit.Registration_ID__c = '74712';
          objBookingUnit.Early_Handover__c = true;
        }
        insert bookingUnitList;
        AccountValidationCntl objAccountValidation = new AccountValidationCntl();
        objAccountValidation.phone = '911234567890';
        objAccountValidation.email = 'Test@gmail';
        objAccountValidation.checkDraftCase();
        AccountValidationCntl.lookupUnit('test',objL.Id);
        
    }
    
     @IsTest
    static void positiveTestCase5(){
         List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Address_Line_1__c = 'XYZ';
        acc.Country__c = 'Canada';
        acc.City__c = 'Barcelona';
        acc.State__c = 'lmn';
        acc.Zip_Postal_Code__c = '111';
        //acc.Mobile__c = '911234567890';
        acc.Email__c = 'Test@gmail.com';
        insert acc;
        
        Location__c objL = new Location__c();
        objL.Name = 'DFA';
        objL.Location_Id__c = 'DFA';
        objL.Building_Name__c = 'test';
        objL.Location_Type__c = 'Building';
        insert objL;
        
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        objInventory.Building_Location__c = objL.Id;
        insert objInventory;
        
         NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;
        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(acc.Id,objDealSR.Id,1);
        insert bookingList;
        
       
        
        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList)
        {
          objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
          objBookingUnit.Inventory__c = objInventory.Id;
          objBookingUnit.Registration_ID__c = '74712';
          objBookingUnit.Early_Handover__c = true;
        }
        insert bookingUnitList;
        AccountValidationCntl objAccountValidation = new AccountValidationCntl();
        //objAccountValidation.phone = '911234567890';
        objAccountValidation.email = 'Test@gmail';
        objAccountValidation.checkDraftCase();
        AccountValidationCntl.lookupUnit('test',objL.Id);
        
    }
}