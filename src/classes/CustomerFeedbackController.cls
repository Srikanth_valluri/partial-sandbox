public without sharing class CustomerFeedbackController {
    public String strFMCaseId;
    public FM_Case__c objFMCase {get;set;}
    public boolean isHappy {get;set;}
    public boolean isSatisfied {get;set;}
    public boolean isNotSatisfied {get;set;}
    public string moveInexperience {get;set;}
    public boolean isSubmitted {get;set;}
    
    public CustomerFeedbackController(ApexPages.StandardController stdController) {
        system.debug('ala aat');
        isHappy = isSatisfied = isNotSatisfied = isSubmitted = false;
        strFMCaseId = ApexPages.currentPage().getParameters().get('Id');
        objFMCase = [Select Id
                        , Name
                        , Description__c
                        , Request_Type__c
                        , Feedback_to_customer__c
                        , Submission_Date__c
                        , Customer_Experience__c
                        , Suggestion_Sub_Type__c
                        , Suggestion_Type__c
                        , Move_In_experience__c
                        , Actual_move_in_date__c
                        , Move_in_date__c
                        , Move_in_Type__c
                        , Access_card_required__c
                        , Move_in_move_out_type__c
                        , Expected_move_out_date__c
                        , Actual_move_out_date__c
                     From FM_Case__c 
                     Where Id =: strFMCaseId];
        system.debug('objFMCase'+objFMCase);
    }
    
    public PageReference submitFeedback() {
        try{
            if(strFMCaseId != null){
                if ((isHappy == true && isSatisfied == true) || (isHappy == true && isNotSatisfied == true)
                    || (isSatisfied == true && isNotSatisfied == true)){
                    ApexPages.addmessage(new ApexPages.message(
                        ApexPages.severity.Warning,'Please select only one option'));
                }
                else if (isHappy == true){
                    objFMCase.Customer_Experience__c = 'Happy';
                    isSubmitted = true;
                } else if (isSatisfied == true){
                    objFMCase.Customer_Experience__c = 'Satisfied';
                    isSubmitted = true;
                } else if (isNotSatisfied == true){
                    objFMCase.Customer_Experience__c = 'Not Satisfied';
                    isSubmitted = true;
                } else if (isHappy == false && isSatisfied == false && isNotSatisfied == false){
                    ApexPages.addmessage(new ApexPages.message(
                        ApexPages.severity.Warning,'Please select atleast one option'));
                }
                system.debug('moveInexperience'+moveInexperience);
                objFMCase.Move_In_experience__c = moveInexperience;
                update objFMCase;
                
            }
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));
        }
        return null;
    }
}