@isTest
public class AccountAttachmentControllerTest {
	 public static testmethod void testController() {
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        
       // Id recId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
        Account objAcc = new Account();
        objAcc.Name = 'demo';
        objAcc.AccountNumber = '123';
       	insert objAcc;
        
        SR_Attachments__c objAttach = new SR_Attachments__c();
        objAttach.Account__c  = objAcc.Id;
        objAttach.isValid__c = true;
        objAttach.Description__c = 'Abctd';
        objAttach.IsRequired__c = True;
        objAttach.Name = 'Test';
        objAttach.type__c = 'Test';
        objAttach.isValid__c = true;
        //objAttach.Attachment_URL__c = 'www.salesforce.com';
        objAttach.Need_Correction__c =  False;
        insert objAttach;
        
        PageReference pageRef = Page.SRAttachment;
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(objAcc);
        AccountAttachmentController objDocExt = new AccountAttachmentController(stdController);
        objDocExt.selectedAwesomeness = true; 
        objDocExt.selectedType = 'test'; 
        objDocExt.fileName = 'test';         
        objDocExt.fileBody = Blob.valueOf('Unit Test Attachment Body');
        objDocExt.selectedDocType = 'none';        
        objDocExt.processUpload();
        //objDocExt.saveCustomAttachment();
        objDocExt.extractType('string');
        objDocExt.extractName('string');
        objDocExt.back();
    }
    public static testmethod void testController1() {
         Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        
       // Id recId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
        Account objAcc = new Account();
        objAcc.AccountNumber = '123';
       	 objAcc.Name = 'demo';
       	insert objAcc;
        
        
        SR_Attachments__c objAttach = new SR_Attachments__c();
        objAttach.Account__c  = objAcc.Id;
        objAttach.isValid__c = true;
        objAttach.Description__c = 'Abctd';
        objAttach.IsRequired__c = True;
        objAttach.Name = 'Test';
        objAttach.type__c = 'Test';
        objAttach.isValid__c = true;
        //objAttach.Attachment_URL__c = 'www.salesforce.com';
        objAttach.Need_Correction__c =  False;
        insert objAttach;
        
        PageReference pageRef = Page.SRAttachment;
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(objAcc);
        AccountAttachmentController objDocExt = new AccountAttachmentController(stdController);
        objDocExt.selectedAwesomeness = true; 
        objDocExt.selectedType = 'test'; 
        objDocExt.fileName = 'test';         
        objDocExt.fileBody = Blob.valueOf('Unit Test Attachment Body');        
        objDocExt.processUpload();
        //objDocExt.saveCustomAttachment();
        objDocExt.extractType('string');
        objDocExt.extractName('string');
        objDocExt.back();
    }

    public static testmethod void testController2() {
         Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        
       // Id recId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
        Account objAcc = new Account();
        objAcc.AccountNumber = '123';
       	 objAcc.Name = 'demo';
       	insert objAcc;
        
        SR_Attachments__c objAttach = new SR_Attachments__c();
        objAttach.Account__c  = objAcc.Id;
        objAttach.isValid__c = true;
        objAttach.Description__c = 'Abctd';
        objAttach.IsRequired__c = True;
        objAttach.Name = 'Test';
        objAttach.type__c = 'Test';
        objAttach.isValid__c = true;
        //objAttach.Attachment_URL__c = 'www.salesforce.com';
        objAttach.Need_Correction__c =  False;
        insert objAttach;
        
        PageReference pageRef = Page.SRAttachment;
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(objAcc);
        AccountAttachmentController objDocExt = new AccountAttachmentController(stdController);
        objDocExt.selectedAwesomeness = true; 
        objDocExt.selectedType = 'test'; 
        objDocExt.fileName = 'test';         
        objDocExt.fileBody = Blob.valueOf('Unit Test Attachment Body'); 
        SOAPCalloutServiceMock.returnToMe = new Map<String,MultipleDocUploadService.DocumentAttachmentMultipleResponse_element>();
        MultipleDocUploadService.DocumentAttachmentMultipleResponse_element response_x = new MultipleDocUploadService.DocumentAttachmentMultipleResponse_element();
        response_x.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS]Files : [IPMS-12345--Test1.pdf Processed] PK Value#:2-2-005058 Additionalfile","PARAM_ID":"IPMS-12345-Test1.pdf","URL":"https://sftest.deeprootsurface.com/docs/t/IPMS-12345-Test1.pdf"}],"message":"Process Completed Returning 2 Response Message(s)...","status":"S"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
               
        //Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(2));
        test.startTest();
        objDocExt.processUpload();
        test.stopTest();

        //objDocExt.saveCustomAttachment();
        objDocExt.extractType('string');
        objDocExt.extractName('string');
        objDocExt.back();
    }

    public static testmethod void testController4() {
         Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        
       // Id recId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
        Account objAcc = new Account();
        objAcc.AccountNumber = '123';
       	 objAcc.Name = 'demo';
       	insert objAcc;
        
        SR_Attachments__c objAttach = new SR_Attachments__c();
        objAttach.Account__c  = objAcc.Id;
        objAttach.isValid__c = true;
        objAttach.Description__c = 'Abctd';
        objAttach.IsRequired__c = True;
        objAttach.Name = 'Test';
        objAttach.type__c = 'Test';
        objAttach.isValid__c = true;
        //objAttach.Attachment_URL__c = 'www.salesforce.com';
        objAttach.Need_Correction__c =  False;
        insert objAttach;
        
        PageReference pageRef = Page.SRAttachment;
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(objAcc);
        AccountAttachmentController objDocExt = new AccountAttachmentController(stdController);
        objDocExt.selectedAwesomeness = true; 
        objDocExt.selectedType = 'test'; 
        objDocExt.fileName = 'test';         
        objDocExt.fileBody = Blob.valueOf('Unit Test Attachment Body'); 
        SOAPCalloutServiceMock.returnToMe = new Map<String,MultipleDocUploadService.DocumentAttachmentMultipleResponse_element>();
        MultipleDocUploadService.DocumentAttachmentMultipleResponse_element response_x = new MultipleDocUploadService.DocumentAttachmentMultipleResponse_element();
        response_x.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS]Files : [IPMS-12345--Test1.pdf Processed] PK Value#:2-2-005058 Additionalfile","PARAM_ID":"IPMS-12345-Test1.pdf","URL":"https://sftest.deeprootsurface.com/docs/t/IPMS-12345-Test1.pdf"}],"message":"Process Completed Returning 2 Response Message(s)...","status":"S"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
               
        //Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(2));
        test.startTest();
        objDocExt.selectedDocType = 'POA document';
        objDocExt.processUpload();
        test.stopTest();


        //objDocExt.saveCustomAttachment();
        objDocExt.extractType('string');
        objDocExt.extractName('string');
        objDocExt.back();
    }

    public static testmethod void testController3() {
      Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        
       // Id recId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
        Account objAcc = new Account();
        objAcc.AccountNumber = '123';
       	 objAcc.Name = 'demo';
       	insert objAcc;
        
        SR_Attachments__c objAttach = new SR_Attachments__c();
        objAttach.Account__c  = objAcc.Id;
        objAttach.isValid__c = true;
        objAttach.Description__c = 'Abctd';
        objAttach.IsRequired__c = True;
        objAttach.Name = 'Test';
        objAttach.type__c = 'Test';
        objAttach.isValid__c = true;
        //objAttach.Attachment_URL__c = 'www.salesforce.com';
        objAttach.Need_Correction__c =  False;
        insert objAttach;
        
        PageReference pageRef = Page.SRAttachment;
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(objAcc);
        AccountAttachmentController objDocExt = new AccountAttachmentController(stdController);
        objDocExt.selectedAwesomeness = true; 
        objDocExt.selectedType = 'test'; 
        objDocExt.fileName = 'test';         
        objDocExt.fileBody = Blob.valueOf('Unit Test Attachment Body'); 
        SOAPCalloutServiceMock.returnToMe = new Map<String,MultipleDocUploadService.DocumentAttachmentMultipleResponse_element>();
        MultipleDocUploadService.DocumentAttachmentMultipleResponse_element response_x = new MultipleDocUploadService.DocumentAttachmentMultipleResponse_element();
        response_x.return_x = '{"data":[{"PROC_STATUS":"E","PROC_MESSAGE":"[SUCCESS]Files : [IPMS-12345--Test1.pdf Processed] PK Value#:2-2-005058 Additionalfile","PARAM_ID":"IPMS-12345-Test1.pdf","URL":""}],"message":"Process Completed Returning 2 Response Message(s)...","status":"E"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
               
        //Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(2));
        test.startTest();
        objDocExt.processUpload();
        test.stopTest();

        //objDocExt.saveCustomAttachment();
        objDocExt.extractType('string');
        objDocExt.extractName('string');
        objDocExt.back();
    }
     public static testmethod void testController5() {
      Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        
       // Id recId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
        Account objAcc = new Account();
        objAcc.AccountNumber = '123';
       	objAcc.Name = 'demo';
       	objAcc.Document_Type__c = '';
       	insert objAcc;
        
        SR_Attachments__c objAttach = new SR_Attachments__c();
        objAttach.Account__c  = objAcc.Id;
        objAttach.isValid__c = true;
        objAttach.Description__c = 'Abctd';
        objAttach.IsRequired__c = True;
        objAttach.Name = 'Test';
        objAttach.type__c = 'Test';
        objAttach.isValid__c = true;
        //objAttach.Attachment_URL__c = 'www.salesforce.com';
        objAttach.Need_Correction__c =  False;
        insert objAttach;
        
        PageReference pageRef = Page.SRAttachment;
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(objAcc);
        AccountAttachmentController objDocExt = new AccountAttachmentController(stdController);
        objDocExt.selectedAwesomeness = true; 
        //objDocExt.selectedType = ''; 
        objDocExt.fileName = 'test';         
        objDocExt.fileBody = Blob.valueOf('Unit Test Attachment Body'); 
        SOAPCalloutServiceMock.returnToMe = new Map<String,MultipleDocUploadService.DocumentAttachmentMultipleResponse_element>();
        MultipleDocUploadService.DocumentAttachmentMultipleResponse_element response_x = new MultipleDocUploadService.DocumentAttachmentMultipleResponse_element();
        response_x.return_x = '{"data":[{"PROC_STATUS":"E","PROC_MESSAGE":"[SUCCESS]Files : [IPMS-12345--Test1.pdf Processed] PK Value#:2-2-005058 Additionalfile","PARAM_ID":"IPMS-12345-Test1.pdf","URL":""}],"message":"Process Completed Returning 2 Response Message(s)...","status":"E"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
               
        //Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(2));
        test.startTest();
        objDocExt.processUpload();
        test.stopTest();

        //objDocExt.saveCustomAttachment();
        objDocExt.extractType('string');
        objDocExt.extractName('string');
        objDocExt.back();
    }
    
    
    
}