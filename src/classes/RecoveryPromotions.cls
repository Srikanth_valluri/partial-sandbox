public with sharing class RecoveryPromotions {
    //AKISHOR
    public List<Option__c> lstoptions {get; set;}
    public Calling_List__c currCallList;
    public String callListId {get; set;}
    
    public RecoveryPromotions( ApexPages.StandardController controller ) {
        callListId = ApexPages.currentPage().getParameters().get('id');//Calling list id
        system.debug('----------callingListId  : '+ callListId);
    }

    public void pickpromotions() {
        lstoptions=new list<Option__c>();
        currCallList=fnGetCallList(callListId);
        system.debug('----------CurrentcallingListId  : '+ currCallList);
        if(currCallList.Booking_Unit__C!=null) {
            for(Option__c option: [ select id
                                         , Name
                                         , Booking_Unit__r.Party_Id__c
                                         , OptionsName__c
                                         , CampaignName__c
                                         , PromotionName__c
                                         , SchemeName__c
                                         , UnitID__c
                                      from Option__c 
                                     where Booking_Unit__c =: currCallList.booking_unit__c ])
            {
                lstoptions.add(option);
            }
            system.debug('----OptionList  : '+ lstoptions);
            if(lstoptions.size()<=0) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'No Options Available.'));
            }
        }
        else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'No Unit Available on Calling List.'));
        }

    }
    public Calling_list__C fnGetCallList(String callListId){
        Calling_List__C currCallList = [select id, booking_unit__C,account__C,Property_Name__c,Unit_Name__c
            from calling_list__C where id=:callListId];
        return currCallList;
    }

}