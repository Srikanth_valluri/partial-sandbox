@isTest
public class FMPenaltyWaiverControllerTest {
     public static testmethod void testCreateFMCase(){
        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;

        PageReference myVfPage = Page.FMPenaltyWaiverProcessPage;

        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Nationality__c ='Indian';
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Penalty_Waiver');
        //FmIpmsRestServices.DueInvoicesResult objResult = new FmIpmsRestServices.DueInvoicesResult();

        FM_Additional_Detail__c objAddPenaltyDetails =
                        new FM_Additional_Detail__c( RecordTypeId = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Penalty Waiver').getRecordTypeId(),
                                                     Penalty_Date__c = '19-FEB-2018',
                                                     Charged_Amount__c = 11000,
                                                     Paid_Amount__c = 1000,
                                                     Due_Remaining__c = 10000,
                                                     Transaction_Reference__c = 'BD4/7/702AQ1-18',
                                                     Amount_to_be_waived__c = 0,
                                                     Percent_to_be_waived__c = 10 );

        FMPenaltyWaiverController obj=new FMPenaltyWaiverController();
         obj.insertCase();
         //obj.calculatePendingDueInvoices(objResult );
         obj.lstPendingInvoices.add(objAddPenaltyDetails);
        //obj.objFMCase=fmCaseObj;
        obj.savePenaltyWaiverCase();

    }

    public static testmethod void submitFMCase(){
        Id profileId = [select id from profile where name = 'System Administrator'].id;
        User u=TestDataFactoryFM.createUser(profileId);
        insert u;

        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;

        Inventory__c invObj=TestDataFactoryFM.createInventory(locObj);
        insert invObj;

        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;

        FM_User__c fmUser=TestDataFactoryFM.createFMUser(u,locObj);
        fmUser.FM_Role__c='FM Manager';
        insert fmUser;

        FM_User__c fmUser1=TestDataFactoryFM.createFMUser(u,locObj);
        fmUser1.FM_Role__c='FM Admin';
        insert fmUser1;

        PageReference myVfPage = Page.FMPenaltyWaiverProcessPage;

        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Penalty_Waiver';
        fmCaseObj.Booking_Unit__c=buIns.id;
        fmCaseObj.Account__c=acctIns.id;
        //fmCaseObj.Approving_Authorities__c='FM Manager';
        fmCaseObj.Nationality__c ='Indian';
        fmCaseObj.Mobile_no__c = '123456789';
        fmCaseObj.Outstanding_service_charges__c = '0';
        insert fmCaseObj;


        SR_Attachments__c objCustAttach = new SR_Attachments__c();
        objCustAttach.FM_Case__c = fmCaseObj.Id ;
        objCustAttach.Booking_Unit__c = buIns.Id ;
        insert objCustAttach ;


        Test.setCurrentPage(myVfPage);
        //ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        //ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        //ApexPages.currentPage().getParameters().put('SRType','Tenant_Registration');
        ApexPages.currentPage().getParameters().put('Id',fmCaseObj.id);
        ApexPages.currentPage().getParameters().put('deleteAttRecId','052020202020215');
        FM_Additional_Detail__c objAddPenaltyDetails =
                        new FM_Additional_Detail__c( RecordTypeId = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Penalty Waiver').getRecordTypeId(),
                                                     Penalty_Date__c = '19-FEB-2018',
                                                     Charged_Amount__c = 11000,
                                                     Paid_Amount__c = 1000,
                                                     Due_Remaining__c = 10000,
                                                     Transaction_Reference__c = 'BD4/7/702AQ1-18',
                                                     Amount_to_be_waived__c = 0,
                                                     Percent_to_be_waived__c = 10 );


        FMPenaltyWaiverController obj=new FMPenaltyWaiverController();

        //obj.objFMCase.id=fmCaseObj.id;
        //obj.initializeFMCaseAddDetails();
        test.startTest();
        //obj.strDetailType='Emergency Contact';
        //obj.addDetails();
        //obj.indexOfNewChildToRemove=0;
        //obj.removeDetails();
        obj.objFMCase = FM_Utility.getCaseDetails( fmCaseObj.Id );
        obj.deleteAttRecId = objCustAttach.Id ;
        obj.deleteAttachment();
        obj.lstPendingInvoices.add(objAddPenaltyDetails);
        obj.submitPenaltyWaiverCase();
        //obj.returnBackToCasePage();
        test.stopTest();
    }

    public static testmethod void testUploadDocument(){
        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;

        PageReference myVfPage = Page.FMPenaltyWaiverProcessPage;

        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Penalty_Waiver';

        //insert fmCaseObj;

        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Penalty_Waiver');

        Test.setMock( WebServiceMock.class, new MoveOutMockClass() );

        FMPenaltyWaiverController obj=new FMPenaltyWaiverController();
        obj.insertCase();
        obj.strDocumentBody='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        obj.strDocumentName='Test_Document_Work_Permit.htm';
        //obj.createCaseShowUploadDoc();
        //obj.initializeFMCaseAddDetails();
        obj.savePenaltyWaiverCase();
        test.startTest();
        obj.uploadDocument();
        obj.addCustomDocuments();
        obj.removeCustomDocuments();
        test.stopTest();
    }

    public static testmethod void testgetNumberEquivalentOfMonth(){
        FMPenaltyWaiverController obj=new FMPenaltyWaiverController();
        obj.getNumberEquivalentOfMonth('JAN');
        obj.getNumberEquivalentOfMonth('MAR');
        obj.getNumberEquivalentOfMonth('APR');
        obj.getNumberEquivalentOfMonth('MAY');
        obj.getNumberEquivalentOfMonth('JUN');
        obj.getNumberEquivalentOfMonth('JUL');
        obj.getNumberEquivalentOfMonth('AUG');
        obj.getNumberEquivalentOfMonth('SEP');
        obj.getNumberEquivalentOfMonth('OCT');
        obj.getNumberEquivalentOfMonth('NOV');
        obj.getNumberEquivalentOfMonth('DEC');
        obj.getNumberEquivalentOfMonth('');

    }

    public static testmethod void testcalculatePendingDueInvoices(){
        Account acctIns=TestDataFactoryFM.createAccount();
        insert acctIns;

        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;

        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;

        Booking_Unit__c buIns=TestDataFactoryFM.createBookingUnit(acctIns,bk);
        insert buIns;

        PageReference myVfPage = Page.FMPenaltyWaiverProcessPage;

        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Nationality__c ='Indian';
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Penalty_Waiver');
        //FmIpmsRestServices.DueInvoicesResult objResult = new FmIpmsRestServices.DueInvoicesResult();
        FmIpmsRestServices dueInvoiceObj = new FmIpmsRestServices();
        FmIpmsRestServices.DueInvoice instance = new FmIpmsRestServices.DueInvoice();
        instance.callType = 'Interest';
        instance.creationDate = '20-JAN-2019';
        instance.chargeAmount = 1000;
        instance.paidAmount = 100;
        instance.dueRemaining = 900;
        instance.trxNumber = 'ABC';
        List<FmIpmsRestServices.DueInvoice> lstInvoice = new List<FmIpmsRestServices.DueInvoice>();
        lstInvoice.add(instance);
        FmIpmsRestServices.DueInvoicesResponse response = new FmIpmsRestServices.DueInvoicesResponse();
        FmIpmsRestServices.OutputParameters opIns = new FmIpmsRestServices.OutputParameters();
        opIns.X_RETURN_STATUS = '200';
        opIns.X_RETURN_MESSAGE = 'Test';
        response.OutputParameters = opIns;
        FmIpmsRestServices.DueInvoicesResult objResult = new FmIpmsRestServices.DueInvoicesResult(response);
        objResult.lstDueInvoice = lstInvoice;

        FM_Additional_Detail__c objAddPenaltyDetails =
                        new FM_Additional_Detail__c( RecordTypeId = Schema.SObjectType.FM_Additional_Detail__c.getRecordTypeInfosByName().get('Penalty Waiver').getRecordTypeId(),
                                                     Penalty_Date__c = '19-FEB-2018',
                                                     Charged_Amount__c = 11000,
                                                     Paid_Amount__c = 1000,
                                                     Due_Remaining__c = 10000,
                                                     Transaction_Reference__c = 'BD4/7/702AQ1-18',
                                                     Amount_to_be_waived__c = 0,
                                                     Percent_to_be_waived__c = 10 );

        FMPenaltyWaiverController obj=new FMPenaltyWaiverController();
         obj.insertCase();
         obj.calculatePendingDueInvoices(objResult);
         //obj.calculatePendingDueInvoices(objResult );
         //obj.lstPendingInvoices.add(objAddPenaltyDetails);
        //obj.objFMCase=fmCaseObj;
        obj.savePenaltyWaiverCase();

    }
    @isTest public static void testCRFGeneration() {        
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        NSIBPM__Service_Request__c objServReq = TestDataFactory_CRM.createServiceRequest();
        insert objServReq ;

        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objServReq.Id,1);
        insert lstBooking ;

        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus ;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking ,3);
        for( Booking_Unit__c objUnit : lstBookingUnit  ) {
            objUnit.Resident__c = objAcc.Id ;
            objUnit.Handover_Flag__c = 'Y' ;
        }
        lstBookingUnit[2].Owner__c = objAcc.Id ;
        insert lstBookingUnit ;

        list<FM_Case__c> lstFMCases = new list<FM_Case__c>();

        Id recTypeId=Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Tenant Registration').getRecordTypeId();
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Tenant_Registration';
        fmCaseObj.Booking_Unit__c=lstBookingUnit[0].id;
        fmCaseObj.Email__c='a@gmail.com';
        fmCaseObj.Mobile_no__c='12121212';
        fmCaseObj.RecordTypeId=recTypeId;
        fmCaseObj.Status__c='Submitted';
        fmCaseObj.Origin__c='Portal';
        fmCaseObj.Approval_Status__c = 'Pending';
        fmCaseObj.Submit_for_Approval__c = true;
        fmCaseObj.Approving_Authorities__c='Property Manager__FM Manager,Master Community Property Manager';
        fmCaseObj.Account__c=objAcc.id;
        fmCaseObj.Current_Approver__c = 'FM Manager__';
        fmCaseObj.Tenant_Email__c = 'test@gmail.com';
        insert fmCaseObj;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());    
        FM_GenerateDrawloopDocumentBatch objClass = new FM_GenerateDrawloopDocumentBatch(fmCaseObj.Id
                                                                                   , System.Label.CRE_NOC_DDP_Template_Id
                                                                                   , System.Label.CRE_NOC_Template_Delivery_Id);
        Database.Executebatch(objClass);
        Test.stopTest();
        
        FMPenaltyWaiverController  obj=new FMPenaltyWaiverController();
        obj.strCaseId=fmCaseObj.id;
        obj.objFMCase=fmCaseObj; 
        obj.generateNDA();
        obj.checkDrawloopBatchStatus();
    }
}