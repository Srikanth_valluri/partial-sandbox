global class DAMAC_InquiryAssignmentBatch implements Database.Batchable<sObject>, Database.stateFul {

    ID sfQueueID = NULL;
    Boolean skipUserLimits = false;
    public static string getAllfields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        if (objectType == null) {
            return fields;
        }
        for (string f :objectType.getDescribe ().fields.getMap ().keySet ())
            fields += f+ ', ';
        return fields.removeEnd(', '); 
        
    }
    global DAMAC_InquiryAssignmentBatch (ID queueID, Boolean userLimits) {
        sfQueueID = queueID;
        skipUserLimits = userLimits;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        String fields = getAllfields('Inquiry__c');
        String query = 'SELECT '+fields+' FROM INQUIRY__c WHERE OwnerID =: sfQueueID';
        return Database.getQueryLocator(query);
    
    }
    global void execute (Database.BatchableContext BC, List <Inquiry__c> scope) {
        Map <inquiry__c, list<User>> eligibleUsers = new Map <inquiry__c, list<User>>  ();
        Map <Inquiry__c, ID> inquiryOwnerMap = new Map <Inquiry__c, ID> ();
        List <Inquiry__c> inqToUpdate = new List <Inquiry__c> ();
        if (skipUserLimits == FALSE) {
            PreInquiryGetEligiblePCS getPcs = new PreInquiryGetEligiblePCS();
            inqToUpdate = getPcs.getEligiblsPCS (scope, 'Create'); 
        }
        if (skipUserLimits) {
            List <ID> groupMemberIdsList = new List <ID> ();
            List <User> groupMemberList = new List <User> ();
            for (groupMember g : [select userorgroupid, groupid from groupmember where groupid=:sfQueueID]) {
                if (String.valueOf (g.userorgroupid).startsWith ('005'))
                    groupMemberIdsList.add (g.userorgroupid);
            }
            groupMemberList = [SELECT Is_Blacklisted__c, isUserOnLeave__c FROM User WHERE isUserOnLeave__c != TRUE AND Is_Blacklisted__c != TRUE AND ID IN: groupMemberIdsList];
            for (Inquiry__c inq : scope) {
                eligibleUsers.put (inq, groupMemberList);
            }
            System.Debug (eligibleUsers);
            inquiryOwnerMap = InquiryOwnerAssignment.reshuffleOwner(eligibleUsers);
            
            for (Inquiry__c inq : inquiryOwnerMap.keySet ()) {
                inq.ownerID = inquiryOwnerMap.get (inq);
                if (inq.OwnerID != NULL)
                    inqToUpdate.add (inq);
            }
            System.Debug (inqToUpdate);
        }
        DAMAC_Constants.skip_InquiryTrigger = TRUE;
        if (inqToUpdate.size () > 0)
            Update inqToUpdate;
    }
    global void finish (Database.BatchableContext BC) {
        
    }
}