/*
 * Description - Test class for GenerateReceiptService
 *
 * Version        Date            Author            Description
 * 1.0            22/11/2017      Vivek Shinde      Initial Draft
 */
@isTest
private class GenerateReceiptServiceTest {

    public static testmethod void getReceiptUrlTest(){
    
        Test.startTest();
        GenerateReceipt.ReceiptIPMSHttpSoap11Endpoint objGenRec =
            new GenerateReceipt.ReceiptIPMSHttpSoap11Endpoint();
            
        List<GenerateReceiptProcessService.APPSXXDC_PROCESS_SERX1794747X1X5> lstRegTerms =
            new List<GenerateReceiptProcessService.APPSXXDC_PROCESS_SERX1794747X1X5>();
        GenerateReceiptProcessService.APPSXXDC_PROCESS_SERX1794747X1X5 objRegTerm = 
            new GenerateReceiptProcessService.APPSXXDC_PROCESS_SERX1794747X1X5();
        lstRegTerms.add(objRegTerm);
    
        SOAPCalloutServiceMock.returnToMe =  new Map<String, GenerateReceipt.GenerateReceiptResponse_element>();
        GenerateReceipt.GenerateReceiptResponse_element response_x = new GenerateReceipt.GenerateReceiptResponse_element();
        response_x.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=340219 and Request Id :40238955 ...","ATTRIBUTE3":"340219","ATTRIBUTE2":"40238955","ATTRIBUTE1":"https://sftest.deeprootsurface.com/docs/e/40238955_274522_RECEIPT.pdf","PARAM_ID":"2-274522","Message_ID":"2-274522"}],"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
    
        test.setMock(WebServiceMock.class , new SOAPCalloutServiceMock());
    
        List<GenerateReceiptService.ReceiptResponse> lstResponse = GenerateReceiptService.getReceiptURL(lstRegTerms);
    
        System.assert(lstResponse != null);
        Test.stopTest(); 
    } 
}