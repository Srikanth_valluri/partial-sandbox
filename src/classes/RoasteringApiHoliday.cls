public class RoasteringApiHoliday {
/***************************************************************************************************************
Description: Method to return Holiday Schedule response
Return Type: HTTPResponse 
Parameters : IPMS Employee Id Of User, currentYear
****************************************************************************************************************/    
     public static HTTPResponse getHolidaySchedule(String ipmsUserId, String currentYear) {
        if( String.isNotBlank(ipmsUserId)) {
            Credentials_details__c creds = Credentials_details__c.getInstance('Roastering Holiday Api');
            if(creds != null && String.isNotBlank(creds.Endpoint__c) && String.isNotBlank(creds.User_Name__c) && String.isNotBlank(creds.Password__c)) { 
                String endpoint = creds.Endpoint__c +ipmsUserId+';year='+currentYear+';list-type=2';
                HTTPResponse resp = GenericHttpResponseFromService.returnResponseFromService(endpoint,'GET',creds.User_Name__c,creds.Password__c,'Basic');
                return resp;         
            }
        }
        return null;  
    } 
}