global class Damac_SweepNewInquiriesSchedule implements Schedulable{
    global void execute(SchedulableContext ctx) {
        Database.executeBatch(new Damac_SweepNewInquiries (), 1);
    }

}