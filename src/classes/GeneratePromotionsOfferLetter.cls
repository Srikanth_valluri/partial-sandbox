public with sharing class GeneratePromotionsOfferLetter 
{

  public Id caseId;
  public Case objCase {get;set;}
  public Boolean blnIsOfferLetterGenerated {get;set;}

  public SR_Attachments__c spaAttachment = new SR_Attachments__c();

  //constructor
  public GeneratePromotionsOfferLetter(ApexPages.StandardController controller) 
  {
    caseId = ApexPages.currentPage().getParameters().get('id');
    //generateOfferAcceptanceLetter();
    blnIsOfferLetterGenerated = false;
  }

    public PageReference generateOfferAcceptanceLetter()
    {
        if(String.isNotBlank(caseId))
        {
            try
            {
                objCase = [ select Id
                                   ,Approval_Status__c
                                   ,Status 
                                   ,CaseNumber 
                                   ,Booking_Unit__c
                                   ,OwnerId
                            from Case where Id =: caseId
                          ];

                system.debug('status '+objCase.Status);
                
                /*List<SR_Attachments__c> lstSRAttachmentCase = [Select id from SR_Attachments__c where Case__c = :caseId and Type__c = 'Signed Promotions Offer Letter'];

                if((lstSRAttachmentCase != null && lstSRAttachmentCase.size() > 0))
                {
                    blnIsOfferLetterGenerated = true;
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error :Promotions Offer Acceptance Letter Already generated for this SR.'));
                    return null;
                }
                */
                if(objCase != null)
                {
                    if( objCase.Status != null  && (objCase.Status != 'Submitted' && objCase.Status != 'Manager Approved'))
                    {
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Error :You cannot generate Promotions Offer Letter now as SR is not submitted.'));
                        return null;
                    }

                    if( String.isBlank(objCase.Approval_Status__c) || (String.isNotBlank(objCase.Approval_Status__c) && objCase.Approval_Status__c != 'Approved'))
                    {
                       ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Error :You cannot generate Promotions Offer Letter now as approval is pending.'));
                       return null; 
                    }
                }

                // here we will fetch the placeholder already generated for Promotions Offer letter
                // and also close task for generate offer letter.
                // create new place holder for uploading singed promotions offer letter and task for 
                // uploading signed offer letter for CRE.

                // create place holder for Offer Letter of Promotions
                SR_Attachments__c objSRAttachment = new SR_Attachments__c();
                objSRAttachment.Name = 'Promotions Offer Letter';
                objSRAttachment.Booking_Unit__c = objCase.Booking_Unit__c;
                objSRAttachment.Case__c = objCase.Id;
                objSRAttachment.Description__c = 'Promotions Offer Letter';
                objSRAttachment.Document_Type__c = 'Promotions Offer Letter';
                objSRAttachment.isValid__c = true;
                objSRAttachment.Type__c = 'Promotions Offer Letter';
                insert objSRAttachment;

                // create place holder for signed Offer Letter of Promotions
                SR_Attachments__c objSRAttachmentSigned = new SR_Attachments__c();
                objSRAttachmentSigned.Name = 'Signed Promotions Offer Letter';
                objSRAttachmentSigned.Booking_Unit__c = objCase.Booking_Unit__c;
                objSRAttachmentSigned.Case__c = objCase.Id;
                objSRAttachmentSigned.Description__c = 'Signed Promotions Offer Letter';
                objSRAttachmentSigned.Document_Type__c = 'Signed Promotions Offer Letter';
                objSRAttachmentSigned.isValid__c = true;
                objSRAttachmentSigned.Type__c = 'Signed Promotions Offer Letter';
                insert objSRAttachmentSigned;

                List<Task> lstTask = new List<Task>();

                // create new taskf or uploading signed offer and acceptance letter
                Task objTask = TaskUtility.getTask((SObject)objCase, 'Upload the Signed Promotions Offer Letter', 'CRE', 
                            'Promotion Package', system.today().addDays(1));
                system.debug('objTask '+objTask);
                //insert objTask;
                lstTask.add(objTask);

                Task objTaskToClose = [Select 
                                             Id,Status
                                       FROM 
                                             Task 
                                       WHERE
                                             Subject = 'Generate Letter For Promotion Request' 
                                       AND WhatId =: objCase.Id LIMIT 1
                                      ];
                system.debug('objTaskToClose '+objTaskToClose);
                objTaskToClose.Status = 'Completed';
                //update objTaskToClose;
                lstTask.add(objTaskToClose);

                system.debug('lstTask '+lstTask);
                upsert lstTask;

                PageReference  pg = new PageReference ('/'+objCase.Id);
                pg.setRedirect(true);
                system.debug('after set pg');
                return pg;
            }
            catch(Exception exp)
            {   
                system.debug('exp '+exp.getMessage());
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error generating document :'+exp.getMessage()));
            }
        }
        else
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Case ID not present for generating Promotions Offer Letter'));
        }
        return null;
    }
}