public with sharing class FmcLeasedUnitController {

    public  FM_Request__c               req                     {get; set;}

    private static final String SR_SUBMIT_SUCCESS = 'Thank you for submitting the details. You will be contacted shortly by us!';
    private static final String GENERIC_SAVE_ERROR = 'There was an error while processing your request. Please try again later.';

    private Id leasedUnitRecordTypeId = Schema.SObjectType.FM_Request__c.getRecordTypeInfosByName()
                                        .get('Leased Unit').getRecordTypeId();
    private Id                          customerAccountId   =   CustomerCommunityUtils.customerAccountId;
    private Account customerAccount;

    public FmcLeasedUnitController() {
        if (FmcUtils.isCurrentView('RequestLeasedUnit')) {
            initializeVariables();
        }
    }

    public void initializeVariables() {

        customerAccountId = CustomerCommunityUtils.customerAccountId;
        customerAccount = [
            SELECT  Id, Name, Party_Type__c, IsPersonAccount, Email__pc, Email__c
                    , Mobile_Phone_Encrypt__pc, Mobile__c, Mobile_Country_Code__c, Mobile_Country_Code__pc
            FROM    Account
            WHERE   Id = :customerAccountId];

        req = new FM_Request__c(
            Source__c = 'LOAMS Portal',
            RecordTypeId = leasedUnitRecordTypeId,
            Request_Type__c = 'Leased Unit',
            Account__c = customerAccountId,
            Email__c = customerAccount.IsPersonAccount ? customerAccount.Email__pc : customerAccount.Email__c,
            Mobile_Country_Code__c = customerAccount.IsPersonAccount ? customerAccount.Mobile_Country_Code__pc : customerAccount.Mobile_Country_Code__c,
            Mobile_no__c = customerAccount.IsPersonAccount ? customerAccount.Mobile_Phone_Encrypt__pc : customerAccount.Mobile__c,
            Assigned_User_Email__c = Label.FmUnitLeasingEmail
        );
    }

    public void submitRequest() {

        req.Status__c = 'Submitted';
        req.Source__c = 'LOAMS Portal';
        req.RecordTypeId = leasedUnitRecordTypeId;
        req.Request_Type__c = 'Leased Unit';
        req.Account__c = customerAccountId;
        req.Email__c = customerAccount.IsPersonAccount ? customerAccount.Email__pc : customerAccount.Email__c;
        req.Mobile_Country_Code__c = customerAccount.IsPersonAccount ? customerAccount.Mobile_Country_Code__pc : customerAccount.Mobile_Country_Code__c;
        req.Mobile_no__c = customerAccount.IsPersonAccount ? customerAccount.Mobile_Phone_Encrypt__pc : customerAccount.Mobile__c;
        req.Assigned_User_Email__c = Label.FmUnitLeasingEmail;

        try {
            upsert req;
        } catch(Exception excp) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, GENERIC_SAVE_ERROR));
            return;
        }
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, SR_SUBMIT_SUCCESS));
    }

}