public class Invocable_ConvertInquiry  {
    @InvocableMethod
    public static void ConvertInquiry(List<Id> stepId) {
        for(New_Step__c step : [SELECT Id, Service_Request__c FROM New_Step__c WHERE Id IN :stepId]) {
            EvaluateCustomCode(step);
        }
    }
    
    public static String EvaluateCustomCode(New_Step__c step) {
        system.debug('Invocable==== convertinquiry');
        
        String retStr = 'Success';
        Set<Id> inquiryIdsSet= new Set<Id>();
        Set<Id> agentinquiryIdsSet= new Set<Id>();
        try {
            
            for (Buyer__c buyer : [SELECT Inquiry__c, Booking__r.Deal_SR__r.Deal_Type__c FROM Buyer__c WHERE Inquiry__c != null AND Booking__r.Deal_SR__c = :step.Service_Request__c]) {
                //inquiryIdsSet.add(buyer.Inquiry__c);
                
                if (buyer.Booking__r.Deal_SR__r.Deal_Type__c != 'Agent Portal Deal')
                    inquiryIdsSet.add(buyer.ID);
                else {
                    agentinquiryIdsSet.add(buyer.Inquiry__c);
                }
                
            }
            
            system.debug('II==== ' + inquiryIdsSet);
            
            if(!inquiryIdsSet.isEmpty()) {
                BuyerConversionProcess.convertBuyer(inquiryIdsSet);
            }
            
            if(!agentinquiryIdsSet.isEmpty()) {
                Invocable_ConvertInquiry.FutureConvertInquiry(step.Service_Request__c);
            }
            
        } catch (Exception e) {
            retStr = 'Error :' + e.getMessage() + '';
            Log__c objLog = new Log__c(Type__c = 'Error converting Inquiry', Description__c ='Ids==' + step.Service_Request__c + '-Line No===>' + e.getLineNumber() + '---Message==>' + e.getMessage() );
            insert objLog;
        }
        
        return retStr;
    }
    
    @Future (callout = TRUE)
        public static void FutureConvertInquiry (id SRID) {
        IPMS_Integration_Settings__mdt settings = new IPMS_Integration_Settings__mdt ();
        settings = [SELECT Endpoint_URL__c FROM IPMS_Integration_Settings__mdt WHERE Label = 'Convert_Inquiry' AND Endpoint_URL__c != NULL LIMIT 1];
        HttpRequest req = new HTTPRequest ();
        req.setEndpoint (settings.Endpoint_URL__c +'/services/apexrest/UpdateInquiry');
        req.setMethod ('POST');
        req.setHeader('Content-Type', 'application/json');
        req.setBody ('{"srId" : "'+SRID+'"}');
        
        Http h = new HTTP ();
        System.Debug (req);
        HTTPresponse res = new HTTPResponse ();
        if (!Test.isRunningTest ())
            res = h.send (req);
        
        System.debug (res.getbody ());
    }
}