@isTest
public class BulkSOAExtensionTest {
    static Id collectioncallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Collections Calling List').RecordTypeId;
     
    static testMethod void testBulkSOA() {
        Account objAcc = new Account();
        objAcc.LastName = 'test';
        objAcc.Party_Id__c = '234225';
        insert objAcc;
        
                                          
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(objAcc);
        BulkSOAExtension objExt = new BulkSOAExtension(sc);
        
        Test.setMock(HttpCalloutMock.class, new BulkSOAMock());
        
        objExt.getUnitInformation();
        objExt.fetchPreviousUnits();
        objExt.fetchNextUnits();
        
        objExt.strLineURL = objExt.objResponse.units[0].lineURL;
        
        objExt.getTransactionDetails();
        objExt.getTransactionInformation();
        objExt.fetchPreviousTransactions();
        objExt.fetchNextTransactions();
        
        BulkSOAExtensionService objServiceExt = new BulkSOAExtensionService();
        BulkSOAExtensionService.CustomerDetails objCustDetails = new BulkSOAExtensionService.CustomerDetails();
        BulkSOAExtensionService.ProcessAttributes objProcessAttr = new BulkSOAExtensionService.ProcessAttributes();
        BulkSOAExtensionService.UnitDetails objUnitDetails = new BulkSOAExtensionService.UnitDetails();
        BulkSOAExtensionService.BankDetails objBankDetails = new BulkSOAExtensionService.BankDetails();
        BulkSOAExtensionService.TransacDetails objTrans = new BulkSOAExtensionService.TransacDetails();
        BulkSOAExtensionService.TransactionDetails objTranDetails = new BulkSOAExtensionService.TransactionDetails();
        
        Test.stopTest();
    }
    static testMethod void testBulkSOA2() {
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        Account objAcc = new Account();
        objAcc.LastName = 'test';
        objAcc.Party_Id__c = '234225';
        insert objAcc;
        
        Calling_List__c collectionCallInst = new Calling_List__c(RecordTypeId = collectioncallingRecordTypeId,
                                                 Call_Outcome__c = 'Recovery list',
                                                 Registration_ID__c = '12851',
                                                  Party_ID__c = '234225');
        insert collectionCallInst;                                        
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(collectionCallInst);
        BulkSOAExtension objExt = new BulkSOAExtension(sc);
        
        Test.setMock(HttpCalloutMock.class, new BulkSOAMock());
        
        objExt.getUnitInformation();
        //objExt.fetchPreviousUnits();
        //objExt.fetchNextUnits();
        
        objExt.strLineURL = objExt.objResponse.units[0].lineURL;
        
        objExt.getTransactionDetails();
        objExt.getTransactionInformation();
        objExt.fetchPreviousTransactions();
        objExt.fetchNextTransactions();
        
        BulkSOAExtensionService objServiceExt = new BulkSOAExtensionService();
        BulkSOAExtensionService.CustomerDetails objCustDetails = new BulkSOAExtensionService.CustomerDetails();
        BulkSOAExtensionService.ProcessAttributes objProcessAttr = new BulkSOAExtensionService.ProcessAttributes();
        BulkSOAExtensionService.UnitDetails objUnitDetails = new BulkSOAExtensionService.UnitDetails();
        BulkSOAExtensionService.BankDetails objBankDetails = new BulkSOAExtensionService.BankDetails();
        BulkSOAExtensionService.TransacDetails objTrans = new BulkSOAExtensionService.TransacDetails();
        BulkSOAExtensionService.TransactionDetails objTranDetails = new BulkSOAExtensionService.TransactionDetails();
        
        Test.stopTest();
    }
}