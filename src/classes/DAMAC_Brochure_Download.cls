/*****************************************************************************************************************
* Name              : DAMAC_Brochure_Download
* Test Class        : DAMAC_Brochure_Download_Test
* Description       : Webservice Class to create Website Subscriber in Salesforce (Brochure Download)   
* -------------------------------------------------------------------------------------------------------------
* VERSION       AUTHOR          DATE            COMMENTS
* 1.0            QBurst      10/03/2020          Created
*****************************************************************************************************************/

@RestResource(urlMapping='/brochuredownload/*')
global with sharing class DAMAC_Brochure_Download{

    @HttpPOST
    global static void createWebsiteSubscriber() {
        RestRequest request = RestContext.request;
        RestResponse res = RestContext.response;
        System.debug('Request String == ' + request.requestbody.tostring());
        // Deserialize the JSON string into name-value pairs
        Map<String, Object> jsonObject
                 = (Map<String, Object>)JSON.deserializeUntyped(request.requestbody.tostring());
        String retString = '';
        system.debug('jsonObject: ' + jsonObject);
        if(jsonObject.containsKey('email')) {
            try{
                Website_Subscriber__c subscriber = new Website_Subscriber__c();
                for(Website_Subscriber_Field_Mappings__c mapping: 
                            Website_Subscriber_Field_Mappings__c.getAll().values()){
                    if(mapping.Brochure_Download__c == true && jsonObject.containsKey(mapping.Name)
                                                            && jsonObject.get(mapping.Name) != ''){
                       
                        system.debug('mapping.Field_API__c: ' + mapping.Field_API__c);
                        if(mapping.Field_Type__c == 'Text') {
                            subscriber.put(mapping.Field_API__c, String.valueOf(jsonObject.get(mapping.Name)));
                            if(mapping.Lookup_Field_API__c != '' && mapping.Lookup_Field_API__c != null
                                && mapping.Lookup_Object_API__c != '' && mapping.Lookup_Object_API__c != null) {
                                system.debug('mapping.Lookup_Field_API__c: ' + mapping.Lookup_Field_API__c);
                                system.debug('mapping.Lookup_Object_API__c: ' + mapping.Lookup_Object_API__c);
                                String query = 'SELECT Id FROM ' + mapping.Lookup_Object_API__c
                                             + ' WHERE Id = \'' + String.valueOf(jsonObject.get(mapping.Name))
                                             + '\' LIMIT 1';
                                for (List<SObject> sobj : Database.query(query)) { 
                                    subscriber.put(mapping.Lookup_Field_API__c, 
                                        String.valueOf(jsonObject.get(mapping.Name)));
                                }
                            }
                        } else if(mapping.Field_Type__c == 'DateTime') {
                            subscriber.put(mapping.Field_API__c, 
                                    DateTime.valueOf(String.valueOf(jsonObject.get(mapping.Name))));
                        }
                    }
                }
                subscriber.Subscriber_Type__c = 'Brochure Download';
                insert subscriber;
                Email_Request_Mappings__c emailReq 
                    = Email_Request_Mappings__c.getValues('Download Brochure');
                if(Email_Request_Mappings__c.getValues(subscriber.R1_Title__c) != null){
                    emailReq = Email_Request_Mappings__c.getValues(subscriber.R1_Title__c);
                }
                Email_Metrics__c metrics = new Email_Metrics__c ();
                metrics.Email_Request__c = emailReq.Email_Request_Id__c;
                metrics.SObject_Id__c = subscriber.Id;
                metrics.Website_Subscriber__c = subscriber.Id;
                //Damac_Constants.skip_EmailMetricsTrigger = TRUE;
                if(!Test.isRunningTest()){
                    insert metrics;
                }
                res.addHeader('Content-Type', 'application/json');
                retString = '{ "status" : "SUCCESS", "Success" : "true"}';
                res.responseBody = Blob.valueOf(retString); 
                if(Test.isRunningTest()) {
                    DMLException ex = new DMLException();
                    throw ex;
                }
           } catch(Exception ex){
                system.debug('Exception at line number = ' + ex.getLineNumber()
                                    + ', Exception message = ' + ex.getmessage());
                res.addHeader('Content-Type', 'application/json');            
                retString = '{ "status" : "ERROR", "Success" : "False","Message":'
                                                        + '"' + ex.getmessage() + '"' + '}';
                res.responseBody = Blob.valueOf(retString);
                // Create Salesforce Log
                Log__c objLog = new Log__c();
                objLog.Description__c = 'Error at line number = ' + ex.getLineNumber()
                                        + '; Error message = ' + ex.getmessage();
                objLog.Type__c = 'Error while adding download brochure subscribers';
                insert objLog;
            } 
        } else {
            res.addHeader('Content-Type', 'application/json');            
            retString = '{ "status" : "ERROR", "Success" : "false","Message":"Email Parameter missing"}';
            res.responseBody = Blob.valueOf(retString);
        } 
        system.debug('retString: ' + retString);  
    } 
}