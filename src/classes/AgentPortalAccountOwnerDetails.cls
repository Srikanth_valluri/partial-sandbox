/* 
* Name               : AgentPortalAccountOwnerDetails                                           
* Description        : Controller class for AgentPortalOwnerDetails page.                                      
* Created Date       :  18-09-2017                                                                    
* Created By         : Pratiksha Narvekar                                                                
* Last Modified Date :                                                                            
* Last Modified By   :                                                                            
*  
* VERSION     AUTHOR                    DATE                                                                    
* 1.0         Pratiksha Narvekar        18-09-2017 
                                                                 
 --*/

public without sharing class AgentPortalAccountOwnerDetails {
    public Id AccID{get;set;}
    
    public AgentPortalAccountOwnerDetails(){
    } 
    public List<Account> getOwnerList(){
        List<Account> listAccount = new List<Account>();
        System.debug('...inside the methos..');
        AccID = AgentPortalUtilityQueryManager.getAccountId();
        
        listAccount = [select OwnerId,
                              owner.Name,
                              owner.Email, 
                              owner.Phone,
                              owner.MobilePhone,
                              owner.Extension,
                              owner.Sales_Office__c
                       FROM Account
                       WHERE ID =: AccID];
        System.debug('...listAccount...'+listAccount);   
        return listAccount;           
    }
}