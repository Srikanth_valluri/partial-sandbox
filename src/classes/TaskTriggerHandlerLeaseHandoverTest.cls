@isTest

private without sharing class TaskTriggerHandlerLeaseHandoverTest{

    @isTest static void getTest(){
   
        Id LocRecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
        Id RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Lease Handover').getRecordTypeId();
        
        NSIBPM__Service_Request__c  Sr = TestDataFactory_CRM.createServiceRequest();
        insert Sr;
        System.assert(Sr != null);
        
        Account Acc = TestDataFactory_CRM.createPersonAccount();
        Acc.Nationality__c = 'UAE';
        insert Acc;
        System.assert(Acc != null);        
        
        Location__c objLocation = new Location__c();
        objLocation.Name = 'ABC';
        objLocation.Location_ID__c = '12345';
        objLocation.RecordTypeId = LocRecordTypeId;
        insert objLocation;
        
        Booking__c Booking =  new Booking__c();
        Booking.AWB_Number__c = 'Test AWB';
        Booking.Account__c = Acc.Id; 
        Booking.Deal_SR__c = Sr.Id;
        insert Booking;
        System.assert(Booking != null);
        
        Booking_Unit__c BU =  new Booking_Unit__c();
        BU.Unit_Name__c = 'Test Units';  
        BU.Registration_Status__c  = 'Active';  
        BU.Unit_Selling_Price_AED__c  = 100;  
        BU.Booking__c = Booking.Id;  
        insert BU;
        System.assert(BU != null);  
        
        Case objC = new Case();
        objC.Booking_Unit__c = BU.Id;
        objC.AccountId = Acc.Id;
        objC.RecordTypeId = RecordTypeId;
        objC.Status = 'New';
        objC.Origin = 'Portal';
        insert objC;
        
        Task tsk = new Task();
        tsk.WhatId = objC.Id;
        tsk.ActivityDate = System.today()+2;
        tsk.Subject = 'Subject';
        tsk.Status = 'Not Started';
        tsk.Assigned_User__c = 'Finance'; 
        tsk.Status = 'Not Started';
        tsk.Process_Name__c = 'Lease Handover';
        insert tsk ;  
        
        list<Task> lstTask =  new list<Task>();
        lstTask.add(tsk);
        
        Test.startTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,TaskCreationWSDL.SRDataToIPMSMultipleResponse_element>();
        TaskCreationWSDL.SRDataToIPMSMultipleResponse_element response1 = new TaskCreationWSDL.SRDataToIPMSMultipleResponse_element();
        response1.return_x = '{'+
    ''+
    '"message":"message",'+
    '"status":"E"'+
    '}';
       SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
       Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
       new TaskTriggerHandlerLeaseHandover().taskForExternalUser(lstTask);     
       Test.stopTest();
    
 }

}