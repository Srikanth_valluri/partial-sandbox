public without sharing class SnagTriggerHandler{
    public static void afterInsert(map<Id,SNAGs__c> newMap){
        set<Id> setCaseIds = new set<Id>();
        for(SNAGs__c objS : newMap.values()){
            if(objS.Case__c != null){
                setCaseIds.add(objS.Case__c);
            }
        }
        if(!setCaseIds.isEmpty()){
            rollupSnags(setCaseIds);
        }
    } // end of afterInsert
    
    public static void afterUpdate(map<Id,SNAGs__c> newMap, map<Id,SNAGs__c> oldMap){
        set<Id> setCaseIds = new set<Id>();
        for(SNAGs__c objS : newMap.values()){
            if(objS.Case__c != null 
            && (objS.Defect_Current_Status__c != oldMap.get(objS.Id).Defect_Current_Status__c
            || objS.Priority__c != oldMap.get(objS.Id).Priority__c)){
                setCaseIds.add(objS.Case__c);
            }
        }
        if(!setCaseIds.isEmpty()){
            rollupSnags(setCaseIds);
        }
    } // end of afterUpdate
    
    public static void afterDelete(map<Id,SNAGs__c> oldMap){
        set<Id> setCaseIds = new set<Id>();
        for(SNAGs__c objS : oldMap.values()){
            if(objS.Case__c != null){
                setCaseIds.add(objS.Case__c);
            }
        }
        if(!setCaseIds.isEmpty()){
            rollupSnags(setCaseIds);
        }
    } // end of afterDelete
    
    public static void rollupSnags(set<Id> setCaseIds){
        list<Case> lstCaseToUpdate = new list<Case>();
        for(Case objCase : [Select Id
                                   , Total_Snags_Open__c
                                   , Total_Snags_Closed__c
                                   ,(Select Id
                                           , Defect_Current_Status__c
                                           , Priority__c
                                     From SNAGs__r) 
                           From Case
                           Where Id IN : setCaseIds]){
            Integer intOpen = 0;
            Integer intClosed = 0;
            Integer intMajorSnags = 0;
            for(SNAGs__c objS : objCase.SNAGs__r){
                if(objS.Defect_Current_Status__c != null
                && (objS.Defect_Current_Status__c.equalsIgnoreCase('Completed')
                || objS.Defect_Current_Status__c.equalsIgnoreCase('Rejected')
                || objS.Defect_Current_Status__c.equalsIgnoreCase('Closed')
                || objS.Defect_Current_Status__c.equalsIgnoreCase('Fixed'))){
                    intClosed = intClosed + 1;
                }else{
                    intOpen = intOpen + 1;
                }
                if(objS.Priority__c != null
                && objS.Priority__c.equalsIgnoreCase('High')){
                    intMajorSnags = intMajorSnags + 1;
                }
            }
            objCase.Total_Snags_Open__c = intOpen;
            objCase.Total_Snags_Closed__c = intClosed;
            objCase.Total_Major_Snags__c = intMajorSnags;
            lstCaseToUpdate.add(objCase);
        }
        if(!lstCaseToUpdate.isEmpty()){
            update lstCaseToUpdate;
        }
    } // end of rollupSnags
}