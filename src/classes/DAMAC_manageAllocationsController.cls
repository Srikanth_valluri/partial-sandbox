public class DAMAC_manageAllocationsController {
    public Map <ID, List<Inventory_User__c>> invUserMap {get;set;}
    public List <InventoryWrap> invWrapper { get; set; }
    public List <Inventory__c> inventories { get; set; }
    public List <String> statusVal { get; set; }
    public DAMAC_manageAllocationsController(ApexPages.StandardController controller) {
        Id unitAllocId = ApexPages.currentPage().getParameters().get('id');
        statusVal = New List <String> {'Released', 'Restricted_Apartments_In_Inventory', 'Apartments_In_Inventory', 'Restricted', 'Inventory'};
        set<ID> invIds = New Set<ID> ();
        inventories = new List <Inventory__c> ();
        invWrapper = new List <InventoryWrap> ();
        invUserMap = New Map <ID, List<Inventory_User__c>> (); 
        for(Inventory__c inv : [SELECT Unit_Name__c, Status__c, Unit_Allocation__c FROM Inventory__c WHERE Unit_Allocation__c =: unitAllocId AND Status__c IN : statusVal]) {
            invIds.add(inv.id);
            inventories.add (inv);
            Boolean showDelete = statusVal.contains (inv.status__c);
            InventoryWrap obj = new InventoryWrap ();
            obj.inventory = inv;
            obj.deleteIcon = showDelete;
            invWrapper.add (obj);
            invUserMap.put (inv.ID, new List <Inventory_User__c> ());
        }
        
        
        if (invIds.size () > 0) {
            for (Inventory_User__c  invUser : [SELECT Inventory__r.Status__c, User__c, User__r.New_Team__c, User_profile__c, 
                                                User__r.Profile.Name, Start_Date__c, End_Date__c, User__r.Name, Name, Inventory__c 
                                                FROM Inventory_User__c  WHERE Unit_Allocation__c =: unitAllocId]) {
                if (!invUserMap.containsKey (invUser.Inventory__c))
                    invUserMap.put (invUser.Inventory__c, new List <Inventory_User__c> {invUser});
                else
                    invUserMap.get (invUser.Inventory__c).add (invUser);
            }
        }
    }
    public void deleteAllInvUsers () {
        String invId = ApexPages.currentPage().getParameters().get('invId');
        try { 
            delete invUserMap.get (invId);
        } catch (Exception e) {}
    }
    public void deleteInvUser () {
        String invUserId = ApexPages.currentPage().getParameters().get('invUserId');
        try {
            delete [SELECT ID FROM Inventory_User__c WHERE ID =: invUserId];
        } catch (Exception e) {}
    }
    public void deleteAllInv () {
        String invIds = ApexPages.currentPage().getParameters().get('invIds');  
        List <String> inventoryids = new List <string> ();
        if (invIds.contains (',')) {
            inventoryids = invIds.split (',');
        } else 
            inventoryids.add (invIds);
        List <Inventory_User__c> usersToDelete = new List <Inventory_User__c> ();
        for (String key :inventoryids) {
            for (Inventory_User__c u :invUserMap.get (key))
                usersToDelete.add (u);
        }  
        try {  
            delete usersToDelete;
        }catch (Exception e) {}
    }
    
    public class InventoryWrap {
        public Inventory__c inventory {get; set;}
        public Boolean deleteIcon {get;set;}
    }
    
}