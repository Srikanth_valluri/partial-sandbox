/***********************************************************************************************
* Name               : AgentPortalPopularSearches                                                  
* Description        : Controller class for AgentPortalPopularSearches.                            
* Created Date       : 29/08/2017                                                                 
* Created By         : Pratiksha Narvekar                                                        
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR              DATE                                                           
* 1.0         Pratiksha Narvekar  29/08/2017                                                      
**************************************************************************************************/
public with sharing class AgentPortalPopularSearches {
    public AgentPortalPopularSearches(){
    	 
    }
    public static List<String> getStudioStudioTypes(){

      List<String> StudioType = new List<String>();
      for(AggregateResult thisType:[SELECT Unit_Type__c 
                                    FROM Inventory__c 
                                    WHERE Unit_Type__c != null
                                    AND Unit_Type__c = 'Studio' 
                                    GROUP BY Unit_Type__c ]){
                  
            String typeString = (String)thisType.get('Unit_Type__c');
            if(null != typeString){
              StudioType.add(DamacUtility.capitalizeEachWord(typeString)); 
            }
                     
      }
      system.debug('***StudioType'+StudioType);
      return StudioType;
    }
    
    public static List<String> getAllDubaiApartments(){
      List<String> cities = new List<String>();

      for(AggregateResult thisCity:[SELECT Address__r.City__c city
                                    FROM Inventory__c
                                    WHERE Address__r.City__c != null
                                    AND Address__r.City__c ='Dubai'
                                    AND Property_Status__c = 'Ready'
                                    AND Unit_Type__c ='Apartments'
                                    GROUP BY Address__r.City__c]){
        String city = (String)thisCity.get('city');

        if(null != city)
          cities.add(DamacUtility.capitalizeEachWord(city));
      }

      return cities;

    }
    
   public static List<String> getAllDubaiVillas(){
      List<String> cities = new List<String>();

      for(AggregateResult thisCity:[SELECT Address__r.City__c city
                                    FROM Inventory__c
                                    WHERE Address__r.City__c != null
                                    AND Address__r.City__c ='Dubai'
                                    AND Property_Status__c = 'Ready'
                                    AND Unit_Type__c ='Villas'
                                    GROUP BY Address__r.City__c]){
        String city = (String)thisCity.get('city');

        if(null != city)
          cities.add(DamacUtility.capitalizeEachWord(city));
      }

      return cities;

    }
    
      public static List<String> getAllOneBHKInventories(){
      List<String> cities = new List<String>();

      for(AggregateResult thisCity:[SELECT Address__r.City__c city
                                    FROM Inventory__c
                                    WHERE Address__r.City__c != null
                                    AND Address__r.City__c ='Dubai'
                                    AND Property_Status__c = 'Ready'
                                    AND Unit_Type__c ='Villas'
                                    GROUP BY Address__r.City__c]){
        String city = (String)thisCity.get('city');

        if(null != city)
          cities.add(DamacUtility.capitalizeEachWord(city));
      }

      return cities;

    }
       public static List<String> getAllTwoBHKInventories(){
      List<String> cities = new List<String>();

      for(AggregateResult thisCity:[SELECT Address__r.City__c city
                                    FROM Inventory__c
                                    WHERE Address__r.City__c != null
                                    AND Address__r.City__c ='Dubai'
                                    AND Property_Status__c = 'Ready'
                                    AND Unit_Type__c ='Villas'
                                    GROUP BY Address__r.City__c]){
        String city = (String)thisCity.get('city');

        if(null != city)
          cities.add(DamacUtility.capitalizeEachWord(city));
      }

      return cities;

    }
       public static List<String> getAllOneMillionVillas(){
      List<String> cities = new List<String>();

      for(AggregateResult thisCity:[SELECT Address__r.City__c city
                                    FROM Inventory__c
                                    WHERE Address__r.City__c != null
                                    AND Address__r.City__c ='Dubai'
                                    AND Property_Status__c = 'Ready'
                                    AND Unit_Type__c ='Villas'
                                    GROUP BY Address__r.City__c]){
        String city = (String)thisCity.get('city');

        if(null != city)
          cities.add(DamacUtility.capitalizeEachWord(city));
      }

      return cities;

    }
       public static List<String> getAllHotelRoomsIndubai(){
      List<String> cities = new List<String>();

      for(AggregateResult thisCity:[SELECT Address__r.City__c city
                                    FROM Inventory__c
                                    WHERE Address__r.City__c != null
                                    AND Address__r.City__c ='Dubai'
                                    AND Property_Status__c = 'Ready'
                                    AND Unit_Type__c ='Villas'
                                    GROUP BY Address__r.City__c]){
        String city = (String)thisCity.get('city');

        if(null != city)
          cities.add(DamacUtility.capitalizeEachWord(city));
      }

      return cities;

    }
    public static List<String> getAllHotelApartmentsdubai(){
      List<String> cities = new List<String>();

      for(AggregateResult thisCity:[SELECT Address__r.City__c city
                                    FROM Inventory__c
                                    WHERE Address__r.City__c != null
                                    AND Address__r.City__c ='Dubai'
                                    AND Property_Status__c = 'Ready'
                                    AND Unit_Type__c ='Villas'
                                    GROUP BY Address__r.City__c]){
        String city = (String)thisCity.get('city');

        if(null != city)
          cities.add(DamacUtility.capitalizeEachWord(city));
      }

      return cities;

    }
    
}