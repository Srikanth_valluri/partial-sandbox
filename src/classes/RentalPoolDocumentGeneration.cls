public without sharing class RentalPoolDocumentGeneration {
    public Id caseId;
    public Case objCase {get;set;}

    public RentalPoolDocumentGeneration(ApexPages.StandardController controller) {
        caseId = ApexPages.currentPage().getParameters().get('id');
    }

    /*public Pagereference generateRPAgreementDoc() {
        PageReference pg;
        pg = new PageReference('/'+caseId);
        pg.setRedirect(true);
        return pg;
    }*/

    public Pagereference generateRPAgreementDoc() {
        String regId;
        String offerUrl;
        String offerCode;
        String selectedOffer;
        String rentalDocResponse;
        String rentalOfferResponse;
        Boolean existingDoc = false;
        objCase = new Case();
        List<SR_Attachments__c> lstRPDoc = new List<SR_Attachments__c>();
        List<RPDocDetailsWrapper> lstRPDocDetails = new List<RPDocDetailsWrapper>();
        List<RPDocDetailsWrapper> lstRPDocOfferDetails = new List<RPDocDetailsWrapper>();
        
        objCase = [select Id, Document_Verified__c
                        , Selected_Rental_Pool_Offer__c
                        , Booking_Unit__r.Registration_ID__c
                        , New_Booking_Unit__c
                        , New_Booking_Unit__r.Registration_ID__c
                        , RecordType.DeveloperName
                        , Rental_Pool_Agreement_Document_Generated__c
                        , Additional_Offer_Type__c
                   from Case 
                   where Id =: caseId 
                   and Booking_Unit__c != null];
        list<SR_Attachments__c> docInst = [select Id, Name from SR_Attachments__c 
                                          where Case__c =: caseId];
                                          //and Name like 'Signed Rental Pool Agreement%'];

        for(SR_Attachments__c srDocInst: docInst) {
            String DocName = srDocInst.Name;
            if(DocName.startsWithIgnoreCase('Signed Rental Pool Agreement')) {
                existingDoc = true;
            }
        }
        /*if(docInst != null && !docInst.isEmpty()) {
            existingDoc = true;
        }*/
        if(!objCase.Document_Verified__c && !docInst.isEmpty()) {
             ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error : Please Verify all document before Agreement Generation');
             ApexPages.addMessage(myMsg);
             return null;
        }
        
        if((objCase.RecordType.DeveloperName.equalsIgnoreCase('Rental_Pool_Assignment')
        && objCase.New_Booking_Unit__r.Registration_Id__c != null)
        || ((objCase.RecordType.DeveloperName.equalsIgnoreCase('Rental_Pool_Agreement') 
        || objCase.RecordType.DeveloperName.equalsIgnoreCase('Rental_Pool_Agreement_Early_Handover')) 
        && objCase.Booking_Unit__r.Registration_ID__c != null)) {
            list<RentalPoolAgreementDocumentWSDLxxdc.APPSXXDC_PROCESS_SERX1794747X1X5> listDocAttributes 
                = new list<RentalPoolAgreementDocumentWSDLxxdc.APPSXXDC_PROCESS_SERX1794747X1X5>();
            list<RentalPoolAgreementDocumentWSDLxxdc.APPSXXDC_PROCESS_SERX1794747X1X5> listOfferAttributes 
                = new list<RentalPoolAgreementDocumentWSDLxxdc.APPSXXDC_PROCESS_SERX1794747X1X5>();
            if(objCase.RecordType.DeveloperName.equalsIgnoreCase('Rental_Pool_Assignment')){
                regId = objCase.New_Booking_Unit__r.Registration_Id__c;
            }else{
                regId = objCase.Booking_Unit__r.Registration_ID__c;
            }
          /*list<RentalPoolAgreementDocumentWSDLxxdc.APPSXXDC_PROCESS_SERX1794747X1X5> listDocAttributes 
              = new list<RentalPoolAgreementDocumentWSDLxxdc.APPSXXDC_PROCESS_SERX1794747X1X5>();
          RentalPoolAgreementDocumentWSDLxxdc.APPSXXDC_PROCESS_SERX1794747X1X5 docAttributeObj 
              = new RentalPoolAgreementDocumentWSDLxxdc.APPSXXDC_PROCESS_SERX1794747X1X5();
          docAttributeObj.ATTRIBUTE1 = 'HTL_RPA';
          docAttributeObj.ATTRIBUTE2 = Datetime.now().format('dd-MMM-YY');
          //docAttributeObj.ATTRIBUTE2 = '02-Feb-18';
          docAttributeObj.ATTRIBUTE3 = '';
          docAttributeObj.ATTRIBUTE4 = '';
          if(objCase.RecordType.DeveloperName.equalsIgnoreCase('Rental_Pool_Assignment')){
              docAttributeObj.PARAM_ID = objCase.New_Booking_Unit__r.Registration_Id__c;
              
          }else{
              docAttributeObj.PARAM_ID = objCase.Booking_Unit__r.Registration_ID__c;
          }
          system.debug('docAttributeObj==='+docAttributeObj);
          listDocAttributes.add(docAttributeObj);
          system.debug('listDocAttributes==='+listDocAttributes);*/
          listDocAttributes = setDocAttributes('HTL_RPA', Datetime.now().format('dd-MMM-YY'), '', '', regId);

          if(String.isNotBlank(objCase.Selected_Rental_Pool_Offer__c) && !objCase.Selected_Rental_Pool_Offer__c.equalsIgnoreCase('Actual Returns')) {
            selectedOffer = objCase.Selected_Rental_Pool_Offer__c;
            /*if(selectedOffer.equalsIgnoreCase('Cash Guaranteed Return')) {
               offerCode = 'XDCADCHG';
            }
            if(selectedOffer.equalsIgnoreCase('Voucher Guaranteed Return')) {
               offerCode = 'XDCADVCG';
            }*/
            listOfferAttributes = setDocAttributes(selectedOffer, '', '', '', regId);
          }
  
          RentalPoolAgreementDocumentWSDL.RPAHttpSoap11Endpoint rentalObj = new RentalPoolAgreementDocumentWSDL.RPAHttpSoap11Endpoint();
          rentalObj.timeout_x = 120000;
          system.debug('rentalObj==='+rentalObj);
          system.debug('reqNumber==='+String.valueOf(System.currentTimeMillis()));
          rentalDocResponse = rentalObj.RentalPoolAgreement(String.valueOf(System.currentTimeMillis()),'RPA','SFDC',listDocAttributes);
          system.debug('rentalDocResponse==='+rentalDocResponse);
          if(!listOfferAttributes.isEmpty()) {
               rentalOfferResponse = rentalObj.RentalPoolAgreement(String.valueOf(System.currentTimeMillis()),'GET_ADDENDUM_DOC','SFDC',listOfferAttributes);
               if(String.isNotBlank(rentalOfferResponse)) {
                    map<String, Object> mapDeserializeRPDocOffer = (map<String,Object>)JSON.deserializeUntyped(rentalOfferResponse);
                    system.debug('mapDeserializeRPDocOffer==='+mapDeserializeRPDocOffer);
                    if(mapDeserializeRPDocOffer.get('status') == 'S') {
                        String StringRPDataOffer = JSON.serialize(mapDeserializeRPDocOffer.get('data'));
                        system.debug('StringRPDataOffer==='+StringRPDataOffer);
                        lstRPDocOfferDetails = 
                            (List<RPDocDetailsWrapper>)JSON.deserialize(StringRPDataOffer, List<RPDocDetailsWrapper>.class);
                        system.debug('lstRPDocOfferDetails==='+lstRPDocOfferDetails);
                        for(RPDocDetailsWrapper rpaInst: lstRPDocOfferDetails) {
                            if(rpaInst.PROC_STATUS == 'S') {
                                if(rpaInst.ATTRIBUTE1 != null && rpaInst.ATTRIBUTE1 != 'null') {
                                    offerUrl = rpaInst.ATTRIBUTE1;
                                }
                            }
                            else {
                                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error : '+rpaInst.PROC_MESSAGE);
                                ApexPages.addMessage(myMsg);
                                return null;
                            }
                        }
                    }
                    else {
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error : '+mapDeserializeRPDocOffer.get('message'));
                        ApexPages.addMessage(myMsg);
                        return null;
                    }
                }
                else {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No Response from IPMS');
                    ApexPages.addMessage(myMsg);
                    return null;
                }
                system.debug('offerUrl==='+offerUrl);
            }
          if(String.isNotBlank(rentalDocResponse)) {
              map<String, Object> mapDeserializeRPDoc = (map<String,Object>)JSON.deserializeUntyped(rentalDocResponse);
                system.debug('mapDeserializeRPDoc==='+mapDeserializeRPDoc);
                if(mapDeserializeRPDoc.get('status') == 'S') {
                    String StringRPData = JSON.serialize(mapDeserializeRPDoc.get('data'));
                    system.debug('StringRPData==='+StringRPData);
                    lstRPDocDetails = 
                        (List<RPDocDetailsWrapper>)JSON.deserialize(StringRPData, List<RPDocDetailsWrapper>.class);
                    system.debug('lstRPDocDetails==='+lstRPDocDetails);
                    for(RPDocDetailsWrapper rpaInst: lstRPDocDetails) {
                        if(rpaInst.PROC_STATUS == 'S') {
                            if(rpaInst.ATTRIBUTE1 != null && rpaInst.ATTRIBUTE1 != 'null') {
                                SR_Attachments__c coveringLetterObj = new SR_Attachments__c();
                                coveringLetterObj.Name = 'Rental Pool Agreement Covering Letter '+system.now();
                                coveringLetterObj.isValid__c = true;
                              coveringLetterObj.IsRequired__c = true;
                              coveringLetterObj.Case__c = objCase.Id;
                              coveringLetterObj.Attachment_URL__c = rpaInst.ATTRIBUTE1;
                              lstRPDoc.add(coveringLetterObj);
                            }
                            if(rpaInst.ATTRIBUTE2 != null && rpaInst.ATTRIBUTE2 != 'null') {
                                SR_Attachments__c agreementObj = new SR_Attachments__c();
                                agreementObj.Name = 'Rental Pool Agreement Document '+system.now();
                                agreementObj.isValid__c = true;
                                agreementObj.IsRequired__c = true;
                                agreementObj.Case__c = objCase.Id;
                                agreementObj.Attachment_URL__c = rpaInst.ATTRIBUTE2;
                                lstRPDoc.add(agreementObj);
                                objCase.Rental_Pool_Agreement_Document_Generated__c = true;
                                
                                if(rpaInst.ATTRIBUTE3 != null && rpaInst.ATTRIBUTE3 != 'null') {
                                    objCase.Document_Version__c = rpaInst.ATTRIBUTE3;
                                }
                                
                                if(String.isNotBlank(offerUrl)) {
                                    SR_Attachments__c agreementOfferObj = new SR_Attachments__c();
                                    agreementOfferObj.Name = 'Rental Pool Agreement Document Addendum '+system.now();
                                    agreementOfferObj.isValid__c = true;
                                    agreementOfferObj.IsRequired__c = true;
                                    agreementOfferObj.Case__c = objCase.Id;
                                    agreementOfferObj.Attachment_URL__c = offerUrl;
                                    lstRPDoc.add(agreementOfferObj);
                                }

                                if(!existingDoc) {
                                    SR_Attachments__c signedAgreementObj = new SR_Attachments__c();
                                  signedAgreementObj.Name = Label.RP_Agreement_Name+' '+system.now();
                                  signedAgreementObj.isValid__c = false;
                                  signedAgreementObj.IsRequired__c = true;
                                  signedAgreementObj.Case__c = objCase.Id;
                                  lstRPDoc.add(signedAgreementObj);
                                }
                            }
                        }
                        else {
                            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error : '+rpaInst.PROC_MESSAGE);
                        ApexPages.addMessage(myMsg);
                        return null;
                        }
                    }
                    if(!lstRPDoc.isEmpty()) {
                        insert lstRPDoc;
                        update objCase;
                    }
                }
                else {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error : '+mapDeserializeRPDoc.get('message'));
                  ApexPages.addMessage(myMsg);
                  return null;
                }
          }
          else {
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No Response from IPMS');
              ApexPages.addMessage(myMsg);
              return null;
          }
        }
        else {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Regid for the new unit has not been generated yet. Please try again later.');
            ApexPages.addMessage(myMsg);
            return null;
        }
        
        PageReference  pg = new PageReference ('/'+caseId);
        pg.setRedirect(true);
        return pg;
    }
    
    /**
     * This method generates the Rental Pool Addendum Documents.
     */
    public PageReference generateRentalPoolAddendum() {
        objCase = [SELECT Id, RecordType.DeveloperName, Booking_Unit__r.Registration_ID__c, Additional_Offer_Type__c FROM Case WHERE Id =: caseId AND Booking_Unit__c != null];

        // Drawloop Document is created if the selected additional offer is "Cash Addendum 1 Year TU" or "Cash Addendum 1 Year Non TU" or "Actual Return 1 Year"
        
        try {
            if (objCase.RecordType.DeveloperName.equalsIgnoreCase('Rental_Pool_Agreement')
                && objCase.Booking_Unit__r.Registration_ID__c != null
                && String.isNotBlank(objCase.Additional_Offer_Type__c)
            ) {
        
                // Drawloop Document created if the selected offer is "Cash Addendum 1 Year TU" or "Cash Addendum 1 Year Non TU"
                if (objCase.Additional_Offer_Type__c == 'Cash Addendum 1 Year TU'
                    || objCase.Additional_Offer_Type__c == 'Cash Addendum 1 Year Non TU') {
                        Database.executeBatch(new GenerateDrawloopDocumentBatch(String.valueOf(objCase.Id), 
                            Label.Rental_Pool_Agreement_Cash_Addendum_DDP_Template_Id, Label.Rental_Pool_Agreement_Cash_Addendum_Template_Delivery_Id));
                }
                
                // Drawloop Document created if the selected offer is "Actual Return 1 Year"
                else if (objCase.Additional_Offer_Type__c == 'Actual Return 1 Year') {
                    Database.executeBatch(new GenerateDrawloopDocumentBatch(String.valueOf(objCase.Id), 
                        Label.Rental_Pool_Agreement_Actual_Return_DDP_Template_Id, Label.Rental_Pool_Agreement_Actual_Return_Template_Delivery_Id));
                }
            }           
        } catch (Exception objException) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Some problem has been encountered.Please contact System Administrator for furthur details.');
            ApexPages.addMessage(myMsg);
            return null;
        }

        PageReference objPageReference = new PageReference ('/'+caseId);
        return objPageReference;
    }

    public list<RentalPoolAgreementDocumentWSDLxxdc.APPSXXDC_PROCESS_SERX1794747X1X5> setDocAttributes(
            String att1, String att2, String att3, String att4, String paramId) {
        list<RentalPoolAgreementDocumentWSDLxxdc.APPSXXDC_PROCESS_SERX1794747X1X5> listDocAttributes 
                = new list<RentalPoolAgreementDocumentWSDLxxdc.APPSXXDC_PROCESS_SERX1794747X1X5>();
        RentalPoolAgreementDocumentWSDLxxdc.APPSXXDC_PROCESS_SERX1794747X1X5 docAttributeObj 
            = new RentalPoolAgreementDocumentWSDLxxdc.APPSXXDC_PROCESS_SERX1794747X1X5();
        docAttributeObj.ATTRIBUTE1 = att1;
        docAttributeObj.ATTRIBUTE2 = att2;
        docAttributeObj.ATTRIBUTE3 = att3;
        docAttributeObj.ATTRIBUTE4 = att4;
        docAttributeObj.PARAM_ID = paramId;
        system.debug('docAttributeObj==='+docAttributeObj);
        listDocAttributes.add(docAttributeObj);
        system.debug('listDocAttributes==='+listDocAttributes);

        return listDocAttributes;
    }

    public class RPDocDetailsWrapper {
        public String PROC_STATUS {get;set;}
        public String PROC_MESSAGE {get;set;}
        public String ATTRIBUTE1 {get;set;}
        public String ATTRIBUTE2 {get;set;}
        public String ATTRIBUTE3 {get;set;}
        public String ATTRIBUTE4 {get;set;}
        public String PARAM_ID {get;set;}

        public RPDocDetailsWrapper(){

        }
    }
}