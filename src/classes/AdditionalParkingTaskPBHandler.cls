public with sharing class AdditionalParkingTaskPBHandler 
{

    @InvocableMethod
    public static void createTasksForParking(List<Task> lstSR)
    {   
        system.debug('createTasksForParking for method called AdditionalParkingTaskPBHandler');

        if(lstSR != null && lstSR.size() > 0)
        {
          Task objTask = lstSR[0];
          system.debug('objTask '+objTask );
          Case objCase = new Case();
          objCase = [ Select 
                      Id
                      ,OwnerId
                      ,Parking_Details_JSON__c 
                      ,Booking_unit__c
                      ,Booking_unit__r.No_of_parking__c 
                      ,Booking_unit__r.Parking_Bay_No__c 
                      ,Submit_for_Approval__c 
                      FROM
                      Case
                      Where Id =: objTask.WhatId
                    ];
          if(objCase != null && String.isNotBlank(objCase.OwnerId) && objTask.Subject == 'Verify Payment' && !objCase.Submit_for_Approval__c)
          {
            system.debug('objCase - Verify Payment');
            submitRecordForApproval(objCase);
          }
          else if(objTask.Subject == 'Archival of Documents')
          {
            system.debug('objCase - Archival of Documents');
            updateParkingDetails(objCase);
          }
        }
        else
        {
          system.debug('lstSR is empty');
        }
    }

    //submit record for approval
    private static void submitRecordForApproval(Case objCase)
    {
        try
        {
            User objUser = [Select Manager_Role__c,Id FROM User where Id =: objCase.OwnerId];
            if(objUser != null && String.isNotBlank(objUser.Manager_Role__c))
            {
                system.debug('objUser.Manager_Role__c '+objUser.Manager_Role__c);
                objCase.Approving_Authorities__c = objUser.Manager_Role__c;
                objCase.Submit_for_Approval__c = true;
                objCase.Status = 'Manager Approval';
                update objCase;

                Task objTask = new Task();
                objTask.ActivityDate = System.today() +1;
                objTask.Assigned_User__c = 'CRE Manager';
                objTask.CurrencyIsoCode = 'AED';
                objTask.OwnerId = objCase.OwnerId;
                objTask.Priority = 'High';
                objTask.Process_Name__c = 'Parking';
                objTask.Status = 'Not Started';
                objTask.Subject = 'Manager Approval - Pending';
                objTask.WhatId = objCase.Id;

                insert objTask;
            }
        }
        catch(Exception exp)
        {
            system.debug('exception occured '+exp.getMessage());
            system.debug('exception occured '+exp.getStackTraceString());
            errorLoggerNew('Error createTaskForPaymentVerification '+exp.getMessage()+' - '+exp.getStackTraceString(),objCase.Id,'');
        }
    }

    //update parking details on Booking Unit
    private static void updateParkingDetails(Case objCase)
    {   
        system.debug('updateParkingDetails method called');
        try
        {
            if(String.isNotBlank(objCase.Parking_Details_JSON__c))
            {
                String strParkingDetails = objCase.Parking_Details_JSON__c;
                List<AdditionalParkingDetailWrapper.Data> lstParkingInformation = 
                (List<AdditionalParkingDetailWrapper.Data>)JSON.deserialize(strParkingDetails,List<AdditionalParkingDetailWrapper.Data>.class);

                Booking_unit__c objBookingUnit = new Booking_unit__c(Id = objCase.Booking_unit__c);

                objBookingUnit = [
                                  SELECT 
                                  Id
                                  ,No_of_parking__c
                                  ,Parking_Bay_No__c 
                                  FROM 
                                  Booking_unit__c 
                                  WHERE 
                                  Id =: objBookingUnit.Id
                                 ];

                if(lstParkingInformation != null && lstParkingInformation.size() > 0)
                {   
                    
                    // make callout to IPMS for updating parking status there if SR is closed
                    AdditionalParkingUtility.updateParkingStatusInIPMS(objCase.Id,JSON.serialize(lstParkingInformation),'Closed');

                    String strSelectedParking = '';
                    String strSelectedParkingType = '';
                    Integer intTotalParkingSelected = 0;
                    for(AdditionalParkingDetailWrapper.Data objData : lstParkingInformation)
                    {
                        if(objData.blnIsSelected != NULL && objData.blnIsSelected)
                        {
                            //strSelectedParking += objData.PARKING_BAY_NUMBER;
                            strSelectedParking += objData.DETAIL_ID+' , ';
                            intTotalParkingSelected++;
                            if(!strSelectedParkingType.contains(objData.PARKING_TYPE))
                            {
                                strSelectedParkingType += objData.PARKING_TYPE+' , ';
                            }
                        }
                    }

                    system.debug('strSelectedParking '+strSelectedParking);
                    system.debug('intTotalParkingSelected '+intTotalParkingSelected);
                    strSelectedParking = strSelectedParking.trim().removeEnd(',');
                    system.debug('strSelectedParking updated '+strSelectedParking);

                    strSelectedParkingType = strSelectedParkingType.trim().removeEnd(',');

                    if(String.isNotBlank(strSelectedParking))
                    {   
                        if(String.isNotBlank(objBookingUnit.Parking_Bay_No__c))
                        {   
                            system.debug('parking bay is present');
                            objBookingUnit.Parking_Bay_No__c = objBookingUnit.Parking_Bay_No__c + ',' +strSelectedParking;
                        }
                        else
                        {
                            system.debug('parking bay is not present');
                            objBookingUnit.Parking_Bay_No__c = strSelectedParking;
                        }

                        if(objBookingUnit.No_of_parking__c != null )
                        {
                            system.debug('No_of_parking__c is present');
                            objBookingUnit.No_of_parking__c = objBookingUnit.No_of_parking__c + intTotalParkingSelected;
                        }
                        else
                        {   
                            system.debug('No_of_parking__c is not present');
                            objBookingUnit.No_of_parking__c = intTotalParkingSelected;
                        }

                        if(objBookingUnit.Type_of_Parking__c != null )
                        {
                            system.debug('Type_of_Parking__c is present');
                            objBookingUnit.Type_of_Parking__c = objBookingUnit.Type_of_Parking__c + strSelectedParkingType;
                        }
                        else
                        {   
                            system.debug('Type_of_Parking__c is not present');
                            objBookingUnit.Type_of_Parking__c = strSelectedParkingType;
                        }
                    }

                    update objBookingUnit;
                }
            }
        }
        catch(Exception exp)
        {   
            system.debug('Error updateParkingDetails '+exp.getMessage()+' - '+exp.getStackTraceString());
            errorLoggerNew('Error updateParkingDetails '+exp.getMessage()+' - '+exp.getStackTraceString(),objCase.Id,'');
        }
    }


    public static void errorLoggerNew(string strErrorMessage, string strCaseID,string strBookingUnitID)
    {
        Error_Log__c objError = new Error_Log__c();
        objError.Error_Details__c = strErrorMessage;
        objError.Process_Name__c = 'Additional Parking';
        if(String.isNotBlank(strCaseID) && strCaseID.startsWith('500')){
            objError.Case__c = strCaseID;
        }
        if(String.isNotBlank(strBookingUnitID)){
            objError.Booking_unit__c = strBookingUnitID;
        }
        insert objError;
    }
}