/****************************************************************************************************************
* Name                  : CC_CheckStepExists_Test                                                              *
* Created By            : NSI - Durga Prasad                                                                   *               
* Created Date          : 05/Jul/2017                                                                          *   
* ------------------------------------------------------------------------------------------------------------ */
@isTest
private class CC_CheckStepExists_Test {
    static testMethod void myUnitTest(){
        List<NSIBPM__SR_Template__c> SRTemplateList =  InitialiseTestData.createTestTemplateRecords(new List<NSIBPM__SR_Template__c>{new NSIBPM__SR_Template__c()});
        List<NSIBPM__Service_Request__c> SRList = InitialiseTestData.createTestServiceRequestRecords(new List<NSIBPM__Service_Request__c>{new NSIBPM__Service_Request__c(recordTypeId = InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Agent Registration'),
                                           NSIBPM__SR_Template__c = SRTemplateList[0].Id),new NSIBPM__Service_Request__c(recordTypeId = InitialiseTestData.getRecordTypeId('NSIBPM__Service_Request__c', 'Deal'), NSIBPM__SR_Template__c = SRTemplateList[0].Id)});
                                               
        NSIBPM__Service_Request__c serviceRequestObject = SRList[0] ; 
        serviceRequestObject.Last_Name__c = 'test User' ;
        update serviceRequestObject;
        
        list<NSIBPM__Step__c> createStepList = InitialiseTestData.createTestStepRecords(new List<NSIBPM__Step__c>{new NSIBPM__Step__c(NSIBPM__SR__c = SRList[0].id),new NSIBPM__Step__c(NSIBPM__SR__c = SRList[0].id)});
        
        CC_CheckStepExists objCC = new CC_CheckStepExists();
        objCC.EvaluateCustomCode(null,createStepList[0]);
    }
}