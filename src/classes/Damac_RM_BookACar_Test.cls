@isTest
public class Damac_RM_BookACar_Test {
    @testSetup static void setup() {
        Campaign__c camp = new Campaign__c();
        camp.End_Date__c = system.today().addmonths(10);
        camp.Marketing_End_Date__c = system.today().addmonths(10);
        camp.Marketing_Start_Date__c = system.today().addmonths(-10);
        camp.Start_Date__c =  system.today().addmonths(-10);
        insert camp;
        Inquiry__c newInquiry = new Inquiry__c();
        newInquiry = InitialiseTestData.getInquiryDetails(DAMAC_Constants.INQUIRY_RT,127);
        newInquiry.First_name__c = 'Test';
        newInquiry.Last_name__c = 'Lead';
        newInquiry.inquiry_source__c = 'Call Center';
        newInquiry.email__c = 'test@test.com';
        newInquiry.Campaign__c = camp.id;
        insert newInquiry ;
    }
    
    static testmethod void testData(){
    
        Inquiry__c inq = [SELECT ID FROM Inquiry__c LIMIT 1];
        
        Sales_Tours__c trip = new Sales_Tours__c ();
        trip.Check_In_Date__c = DateTime.Now().addDays(2);
        trip.No_Of_Cars__c = 2;
        
        
        apexpages.currentpage().getparameters().put ('id', inq.Id);
        Damac_RM_BookACar obj = new Damac_RM_BookACar ();
        obj.trip = trip;
        obj.save ();
    }
    
}