public without sharing class GenerateKeyReleaseFormExtension {
    
    @testVisible private Id bookingUnitId ;
    
    public GenerateKeyReleaseFormExtension(ApexPages.StandardController controller) {
        bookingUnitId = controller.getid();
    }

    public PageReference checkEligiblity() {
        if( String.isNotBlank( bookingUnitId ) ) {
            try {
                Booking_Unit__c objUnit = [ SELECT Id
                                                 , Name
                                                 , Okay_to_release_keys__c
                                                 , OK_to_release_Keys_LHO__c
                                              FROM Booking_Unit__c
                                             WHERE Id = :bookingUnitId ];
                if( objUnit != NULL && (objUnit.Okay_to_release_keys__c || objUnit.OK_to_release_Keys_LHO__c) ) {
                    PageReference newPage = Page.HandoverKeyReleaseForm ;
                    newPage.getParameters().put( 'id', bookingUnitId );
                    return newPage ;
                }
                else {
                    ApexPages.addmessage( new ApexPages.message( ApexPages.severity.INFO,'Unit is not ready for key release.\n Please close the respective Handover case.') );  
                }
            }
            catch( Exception e ) {
                ApexPages.addmessage( new ApexPages.message( ApexPages.severity.ERROR, e.getMessage() ) );
            }
        }
        else {
            ApexPages.addmessage( new ApexPages.message( ApexPages.severity.INFO,'Unit Id not found.') );
        }
        return NULL ;
    }
}