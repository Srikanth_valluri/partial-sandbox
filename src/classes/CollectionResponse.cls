//
// Generated by JSON2Apex http://json2apex.herokuapp.com/
//

public class CollectionResponse {

    public class UnitLines {
        public Integer registrationId {get;set;} 
        public String unitNumber {get;set;} 
        public Double reservationPrice {get;set;} 
        public Double amountPaid {get;set;} 
        public Double amountPaidDld {get;set;} 
        public Double invoicedRaised {get;set;} 
        public Double amountPaidOnInstallment {get;set;} 
        public Double balanceToPay {get;set;} 
        public Double soaBalance {get;set;} 
        public Double totalDueAmount {get;set;} 
        public Double totalOverDueAmount {get;set;} 
        public Double overDue180To365Days {get;set;} 
        public Double penaltiesCharged {get;set;} 
        public Double penaltiesWaived {get;set;} 
        public String lastPaidDate {get;set;} 
        public Double areaVariation {get;set;} 
        public Double areaVariationCharged {get;set;} 
        public Double areaVariationWaived {get;set;} 
        public Double liborAmount {get;set;} 
        public Integer orgId {get;set;} 
        public Integer unitId {get;set;} 
        public Double amountCredited {get;set;} 
        public Double overDueMoreThan720Days {get;set;} 
        public Double overDue90To180Days {get;set;} 
        public Double overDue365To720Days {get;set;} 
        public Double overDue0To90Days {get;set;} 
        public Double retentionAmount {get;set;} 
        public Double refundLiability {get;set;} 
        public Double accruedPenalties {get;set;} 

        public UnitLines(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'registrationId') {
                            registrationId = parser.getIntegerValue();
                        } else if (text == 'unitNumber') {
                            unitNumber = parser.getText();
                        } else if (text == 'reservationPrice') {
                            reservationPrice = parser.getDoubleValue();
                        } else if (text == 'amountPaid') {
                            amountPaid = parser.getDoubleValue();
                        } else if (text == 'amountPaidDld') {
                            amountPaidDld = parser.getDoubleValue();
                        } else if (text == 'invoicedRaised') {
                            invoicedRaised = parser.getDoubleValue();
                        } else if (text == 'amountPaidOnInstallment') {
                            amountPaidOnInstallment = parser.getDoubleValue();
                        } else if (text == 'balanceToPay') {
                            balanceToPay = parser.getDoubleValue();
                        } else if (text == 'soaBalance') {
                            soaBalance = parser.getDoubleValue();
                        } else if (text == 'totalDueAmount') {
                            totalDueAmount = parser.getDoubleValue();
                        } else if (text == 'totalOverDueAmount') {
                            totalOverDueAmount = parser.getDoubleValue();
                        } else if (text == 'overDue180To365Days') {
                            overDue180To365Days = parser.getDoubleValue();
                        } else if (text == 'penaltiesCharged') {
                            penaltiesCharged = parser.getDoubleValue();
                        } else if (text == 'penaltiesWaived') {
                            penaltiesWaived = parser.getDoubleValue();
                        } else if (text == 'lastPaidDate') {
                            lastPaidDate = parser.getText();
                        } else if (text == 'areaVariation') {
                            areaVariation = parser.getDoubleValue();
                        } else if (text == 'areaVariationCharged') {
                            areaVariationCharged = parser.getDoubleValue();
                        } else if (text == 'areaVariationWaived') {
                            areaVariationWaived = parser.getDoubleValue();
                        } else if (text == 'liborAmount') {
                            liborAmount = parser.getDoubleValue();
                        } else if (text == 'orgId') {
                            orgId = parser.getIntegerValue();
                        } else if (text == 'unitId') {
                            unitId = parser.getIntegerValue();
                        } else if (text == 'amountCredited') {
                            amountCredited = parser.getDoubleValue();
                        } else if (text == 'overDueMoreThan720Days') {
                            overDueMoreThan720Days = parser.getDoubleValue();
                        } else if (text == 'overDue90To180Days') {
                            overDue90To180Days = parser.getDoubleValue();
                        } else if (text == 'overDue365To720Days') {
                            overDue365To720Days = parser.getDoubleValue();
                        } else if (text == 'overDue0To90Days') {
                            overDue0To90Days = parser.getDoubleValue();
                        } else if (text == 'retentionAmount') {
                            retentionAmount = parser.getDoubleValue();
                        } else if (text == 'refundLiability') {
                            refundLiability = parser.getDoubleValue();
                        } else if (text == 'accruedPenalties') {
                            accruedPenalties = parser.getDoubleValue();
                        } else {
                            System.debug(LoggingLevel.WARN, 'UnitLines consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public Integer partyId {get;set;} 
    public String processStatus {get;set;} 
    public String processMessage {get;set;} 
    public Integer processingTimeInMs {get;set;} 
    public Double bouncedChequeValue {get;set;} 
    public String dataAsOf {get;set;} 
    public List<UnitLines> unitLines {get;set;} 

    public CollectionResponse(JSONParser parser) {
        while (parser.nextToken() != System.JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                    if (text == 'partyId') {
                        partyId = parser.getIntegerValue();
                    } else if (text == 'processStatus') {
                        processStatus = parser.getText();
                    } else if (text == 'processMessage') {
                        processMessage = parser.getText();
                    } else if (text == 'processingTimeInMs') {
                        processingTimeInMs = parser.getIntegerValue();
                    } else if (text == 'bouncedChequeValue') {
                        bouncedChequeValue = parser.getDoubleValue();
                    } else if (text == 'dataAsOf') {
                        dataAsOf = parser.getText();
                    } else if (text == 'unitLines') {
                        unitLines = arrayOfUnitLines(parser);
                    } else {
                        System.debug(LoggingLevel.WARN, 'CollectionResponse consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }
    }
    
    
    public static CollectionResponse parse(String json) {
        System.JSONParser parser = System.JSON.createParser(json);
        return new CollectionResponse(parser);
    }
    
    public static void consumeObject(System.JSONParser parser) {
        Integer depth = 0;
        do {
            System.JSONToken curr = parser.getCurrentToken();
            if (curr == System.JSONToken.START_OBJECT || 
                curr == System.JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == System.JSONToken.END_OBJECT ||
                curr == System.JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }
    

    private static List<UnitLines> arrayOfUnitLines(System.JSONParser p) {
        List<UnitLines> res = new List<UnitLines>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new UnitLines(p));
        }
        return res;
    }








}