public class HONotice_WebServiceAdapter extends GenericWebServiceAdapter implements WebServiceAdapter{

    public String url; 
    
    public override WebServiceAdapter call(){
        String regID_BUID = parameters.get('regID_BUID');
        system.debug('!!!!!!!!regID_BUID'+regID_BUID);
        String buildingName, urlHandoverNotice, codeforNotice, BUId, regID, approval ;
        
        if(String.isNotBlank(regID_BUID)) {
            BUId = regID_BUID.substringBefore('-');
            regID = regID_BUID.substringAfter('-');
        }
        system.debug('@@@@@@@@@@@BUId'+BUId);
        for (Booking_Unit__c objBU : [Select Id, Unit_Name__c, Approval_Status__c From Booking_Unit__c Where Id =: BUId]) {
             if (!String.isBlank(objBU.Unit_Name__c)) {
                 buildingName = objBU.Unit_Name__c.substringBefore('/');
                 approval = objBU.Approval_Status__c;
            }
        }
        
        Map<string,Handover_Notice_URL__c> mapCodes = Handover_Notice_URL__c.getAll();
        if (approval == 'HO' || approval == 'Approved') {
            if (!String.isBlank(buildingName) && mapCodes != null && mapCodes.containsKey(buildingName)
                &&  mapCodes.get(buildingName).NHO_Attribute__c != null) {
                codeforNotice = mapCodes.get(buildingName).NHO_Attribute__c;
            }
        } else if (approval == 'EHO' || approval == 'EHO_IF_PAY_MORE_RES') {
             if (!String.isBlank(buildingName) && mapCodes != null && mapCodes.containsKey(buildingName)
                 &&  mapCodes.get(buildingName).EHO_Attribute__c != null) {
                 codeforNotice = mapCodes.get(buildingName).EHO_Attribute__c;
            }        
        }
        
        if (!String.isBlank(codeforNotice)) {
            HO_NoticeGeneration.HO_NoticeResponse objHONotice =
                HO_NoticeGeneration.GetHONotice(codeforNotice , 'HO_NOTICE' , regID );

            if (objHONotice != null && objHONotice.URL != null) {
                this.url= objHONotice.URL;
            }
        }       
        
        return this;
    }
    
    public override String getResponse(){
        return this.url;
    }
    
}