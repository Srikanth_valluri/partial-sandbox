@isTest
public class UpdateHoEhoAndRelatedClOwnerHandlerTest{

    @testSetup 
    static void testData() {
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',OnOffCheck__c = true));
        settingLst2.add( new TriggerOnOffCustomSetting__c(Name= 'CreateHoCLOnUnitNoticeSent',OnOffCheck__c = true));
        insert settingLst2;
        List<Calling_List__c> lstCallingList = new List<Calling_List__c>();
        Account objAccount = new Account(Name = 'Miss. Madina Alieva');
        insert objAccount;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest( );
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c>  lstBookings = TestDataFactory_CRM.createBookingForAccount(objAccount.Id, objSR.Id, 1);
        insert lstBookings ;
        
        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        Inventory__c  objInv = TestDataFactory_CRM.createInventory(objProp.Id);
        objInv.Property_Country__c = 'UAE';
        objInv.Property_City__c = 'Dubai';
        objInv.Property_Status__c = 'Ready';
        objInv.Bedroom_Type__c = ' 1 BR';
        insert objInv;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        lstBookingUnits[0].Inventory__c = objInv.Id;
        lstBookingUnits[0].Area__c = 35;
        lstBookingUnits[0].Handover_Flag__c = 'Y';
        lstBookingUnits[0].Registration_ID__c = String.valueOf(System.currentTimeMillis());
        insert lstBookingUnits;

        Case objParentHandoverCase = new Case(AccountId = objAccount.Id, Priority = 'Medium',
                              RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover Parent').getRecordTypeId(), Booking_Unit__c =lstBookingUnits[0].Id);
        insert objParentHandoverCase;
        Case objEHOCase = new Case(AccountId = objAccount.Id, Priority = 'Medium',
                              RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId(), Booking_Unit__c =lstBookingUnits[0].Id);
        insert objEHOCase;
        Case objHOCase = new Case(AccountId = objAccount.Id, Priority = 'Medium',
                              RecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Handover').getRecordTypeId(), ParentId = objParentHandoverCase.Id, Booking_Unit__c = lstBookingUnits[0].Id);
        insert objHOCase;

        Calling_List__c objEHOCallingList = new Calling_List__c(RecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Early Handover Calling List').getRecordTypeId(), Case__c = objEHOCase.Id);
        lstCallingList.add(objEHOCallingList);

        Calling_List__c objHOCallingList = new Calling_List__c(RecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Handover Calling List').getRecordTypeId(), Case__c = objHOCase.Id);
        lstCallingList.add(objHOCallingList);
        
        insert lstCallingList;
    }

    @isTest
    public static void changeOwnerEHOTest(){
        Id recordTypeEHOId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();
        User creUser = createCREUsers('creUser7000@test.com', '7000');
        List<Case> lstCase = [SELECT Id, OwnerId FROM Case WHERE RecordTypeId =: recordTypeEHOId LIMIT 1];
        
        Test.startTest();
            lstCase[0].OwnerId = creUser.Id;
            update lstCase[0];
        Test.stopTest();
    }

    @isTest
    public static void changeOwnerHOTest(){
        Id recordTypeHOId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId();
        User creUser = createCREUsers('creUser7000@test.com', '7000');
        List<Case> lstCase = [SELECT Id, OwnerId FROM  Case WHERE RecordTypeId =: recordTypeHOId LIMIT 1];
        
        Test.startTest();
            lstCase[0].OwnerId = creUser.Id;
            update lstCase[0];
        Test.stopTest();
    }

    public static User createCREUsers(String creUserName, String extension) {
        User creUser;
        System.runAs(new User(Id = UserInfo.getUserID())) {
             creUser = new User(
                Username = creUserName,
                Extension = extension,
                Alias = 'TestCRE',
                Email = creUserName,
                Emailencodingkey = 'UTF-8',
                Lastname = 'Test CRE',
                Languagelocalekey = 'en_US',
                Localesidkey = 'en_US',
                Profileid = [Select Id From Profile Where Name='Contact Center - Manager'].Id,
                Country = 'United Arab Emirates',
                IsActive = true,
                Timezonesidkey='America/Los_Angeles'
            );
            insert creUser;
        }
        return creUser;
    }
}