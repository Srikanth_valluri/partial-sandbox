@isTest
public class CustomerControllerTest{

    @testSetup static void setup() {
        // Create common test User
        User communityUser = CommunityTestDataFactory.CreatePortalUser();
        System.debug('communityUserId = ' + communityUser.Id);
    }

    @isTest
    public static void testController() {
        User u = [Select id, UserName, AccountId from user ORDER BY CreatedDate DESC LIMIT 1];
        PageReference pageRef = Page.Customer;
        pageRef.getParameters().put('AccountId', u.AccountId);
        Test.setCurrentPage(pageRef);
        system.runAs(u) {
            Schema.RecordTypeInfo caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POP');
            insert new List<Case>{
                new Case(
                    Status = 'New',
                    Origin = 'Call',
                    Priority = 'Medium',
                    RecordTypeId = caseRecordType.getRecordTypeId()
                ),
                new Case(
                    Status = 'Closed',
                    Origin = 'Call',
                    Priority = 'Medium',
                    RecordTypeId = caseRecordType.getRecordTypeId(),
                    OwnerId = u.Id
                )
            };
            System.assert(![SELECT Id FROM Case].isEmpty());
            CustomerController controller = new CustomerController();
            controller.authenticateUser();
            System.assertEquals(CustomerCommunityUtils.getFullPhotoUrl(), controller.getFullPhotoUrl());
        }
    }

   @isTest
   public static void testpopulateCreateSRBoolean(){

       User u = [Select id, UserName from user ORDER BY CreatedDate DESC LIMIT 1];
       PageReference pageRef = Page.Customer; // Add your VF page Name here
       pageRef.getParameters().put('view', 'AdditionalParking');
       Test.setCurrentPage(pageRef);
       system.runAs(u){
       CustomerController controller = new CustomerController();
       system.assertEquals(true,controller.createAdditionalParking);
       }
   }

   @isTest
   public static void testBooleanAssignmentRequest(){

       User u = [Select id, UserName from user ORDER BY CreatedDate DESC LIMIT 1];
       PageReference pageRef = Page.Customer; // Add your VF page Name here
       pageRef.getParameters().put('view', 'AssignmentRequest');
       Test.setCurrentPage(pageRef);
       system.runAs(u){
       CustomerController controller = new CustomerController();
       system.assertEquals(true,controller.createAssignmentRequest);
       }
   }

   @isTest
   public static void testBooleanAOPT(){

       User u = [Select id, UserName from user ORDER BY CreatedDate DESC LIMIT 1];
       PageReference pageRef = Page.Customer; // Add your VF page Name here
       pageRef.getParameters().put('view', 'AOPT');
       Test.setCurrentPage(pageRef);
       system.runAs(u){
       CustomerController controller = new CustomerController();
       system.assertEquals(true,controller.createAOPT);
       }
   }
   @isTest
   public static void testBooleanBouncedCheque(){

       User u = [Select id, UserName from user ORDER BY CreatedDate DESC LIMIT 1];
       PageReference pageRef = Page.Customer; // Add your VF page Name here
       pageRef.getParameters().put('view', 'BouncedCheque');
       Test.setCurrentPage(pageRef);
       system.runAs(u){
       CustomerController controller = new CustomerController();
       system.assertEquals(true,controller.createBouncedCheque);
       }
   }
    @isTest
   public static void testBooleanFundTransfer(){

       User u = [Select id, UserName from user ORDER BY CreatedDate DESC LIMIT 1];
       PageReference pageRef = Page.Customer; // Add your VF page Name here
       pageRef.getParameters().put('view', 'FundTransferActiveUnits');
       Test.setCurrentPage(pageRef);
       system.runAs(u){
       CustomerController controller = new CustomerController();
       system.assertEquals(true,controller.createFundTransfer);
       }
   }

   @isTest
   public static void testBooleanFurniturePackage(){

       User u = [Select id, UserName from user ORDER BY CreatedDate DESC LIMIT 1];
       PageReference pageRef = Page.Customer; // Add your VF page Name here
       pageRef.getParameters().put('view', 'FurniturePackage');
       Test.setCurrentPage(pageRef);
       system.runAs(u){
       CustomerController controller = new CustomerController();
       system.assertEquals(true,controller.createFurniturePackage);
       }
   }
   @isTest
   public static void testBooleanMortgage(){

       User u = [Select id, UserName from user ORDER BY CreatedDate DESC LIMIT 1];
       PageReference pageRef = Page.Customer; // Add your VF page Name here
       pageRef.getParameters().put('view', 'Mortgage');
       Test.setCurrentPage(pageRef);
       system.runAs(u){
       CustomerController controller = new CustomerController();
       system.assertEquals(true,controller.createMortgage);
       }
   }

   @isTest
   public static void testBooleanPenaltyWaiver(){

       User u = [Select id, UserName from user ORDER BY CreatedDate DESC LIMIT 1];
       PageReference pageRef = Page.Customer; // Add your VF page Name here
       pageRef.getParameters().put('view', 'PenaltyWaiver');
       Test.setCurrentPage(pageRef);
       system.runAs(u){
       CustomerController controller = new CustomerController();
       system.assertEquals(true,controller.createPenaltyWaiver);
       }
   }

   @isTest
   public static void testBooleanProofOfPayment(){

       User u = [Select id, UserName from user ORDER BY CreatedDate DESC LIMIT 1];
       PageReference pageRef = Page.Customer; // Add your VF page Name here
       pageRef.getParameters().put('view', 'ProofOfPayment');
       Test.setCurrentPage(pageRef);
       system.runAs(u){
       CustomerController controller = new CustomerController();
       system.assertEquals(true,controller.createProofOfPayment);
       }
   }
   @isTest
   public static void testCOCD(){

       User u = [Select id, UserName from user ORDER BY CreatedDate DESC LIMIT 1];
       PageReference pageRef = Page.Customer; // Add your VF page Name here
       pageRef.getParameters().put('view', 'COCD');
       Test.setCurrentPage(pageRef);
       system.runAs(u){
       CustomerController controller = new CustomerController();
       system.assertEquals(true,controller.createCOCD);
       }
   }
   @isTest
   public static void testBooleanRefunds(){

       User u = [Select id, UserName from user ORDER BY CreatedDate DESC LIMIT 1];
       PageReference pageRef = Page.Customer; // Add your VF page Name here
       pageRef.getParameters().put('view', 'Refunds');
       Test.setCurrentPage(pageRef);
       system.runAs(u){
       CustomerController controller = new CustomerController();
       system.assertEquals(true,controller.createRefunds);
       }
   }

   @isTest
   public static void testBooleanEarlyHandover(){

       User u = [Select id, UserName from user ORDER BY CreatedDate DESC LIMIT 1];
       PageReference pageRef = Page.Customer; // Add your VF page Name here
       pageRef.getParameters().put('view', 'EarlyHandover');
       Test.setCurrentPage(pageRef);
       system.runAs(u){
       CustomerController controller = new CustomerController();
       system.assertEquals(true,controller.createEarlyHandover);
       }
   }

   @isTest
   public static void testBooleanNOCVisa(){

       User u = [Select id, UserName from user ORDER BY CreatedDate DESC LIMIT 1];
       PageReference pageRef = Page.Customer; // Add your VF page Name here
       pageRef.getParameters().put('view', 'NOCVisa');
       Test.setCurrentPage(pageRef);
       system.runAs(u){
       CustomerController controller = new CustomerController();
       system.assertEquals(true,controller.createNOCforVisa);
       }
   }

   @isTest
   public static void testBooleanComplaint(){

       User u = [Select id, UserName from user ORDER BY CreatedDate DESC LIMIT 1];
       PageReference pageRef = Page.Customer; // Add your VF page Name here
       pageRef.getParameters().put('view', 'Complaint');
       Test.setCurrentPage(pageRef);
       system.runAs(u){
       CustomerController controller = new CustomerController();
       system.assertEquals(true,controller.createComplaint);
       }
   }
   @isTest
   public static void testBooleanHome(){

       User u = [Select id, UserName from user ORDER BY CreatedDate DESC LIMIT 1];
       PageReference pageRef = Page.Customer; // Add your VF page Name here
       pageRef.getParameters().put('view', 'Home');
       Test.setCurrentPage(pageRef);
       system.runAs(u){
       CustomerController controller = new CustomerController();
       system.assertEquals(true,controller.Home);
       }
   }
  @isTest
   public static void testBooleanRentalPool(){

       User u = [Select id, UserName from user ORDER BY CreatedDate DESC LIMIT 1];
       PageReference pageRef = Page.Customer; // Add your VF page Name here
       pageRef.getParameters().put('view', 'RentalPool');
       Test.setCurrentPage(pageRef);
       system.runAs(u){
       CustomerController controller = new CustomerController();
       system.assertEquals(true,controller.createRentalPool);
       }
   }

   @isTest
   public static void testBooleanTitleDeed(){

       User u = [Select id, UserName from user ORDER BY CreatedDate DESC LIMIT 1];
       PageReference pageRef = Page.Customer; // Add your VF page Name here
       pageRef.getParameters().put('view', 'TitleDeed');
       Test.setCurrentPage(pageRef);
       system.runAs(u){
       CustomerController controller = new CustomerController();
       system.assertEquals(true,controller.createTitleDeed);
       }
   }
   @isTest
   public static void testBooleanHandover(){

       User u = [Select id, UserName from user ORDER BY CreatedDate DESC LIMIT 1];
       PageReference pageRef = Page.Customer; // Add your VF page Name here
       pageRef.getParameters().put('view', 'Handover');
       Test.setCurrentPage(pageRef);
       system.runAs(u){
       CustomerController controller = new CustomerController();
       system.assertEquals(true,controller.createHandover);
       }
   }
   @isTest
   public static void testBooleanRentalPoolTermination(){

       User u = [Select id, UserName from user ORDER BY CreatedDate DESC LIMIT 1];
       PageReference pageRef = Page.Customer; // Add your VF page Name here
       pageRef.getParameters().put('view', 'RentalPoolTermination');
       Test.setCurrentPage(pageRef);
       system.runAs(u){
       CustomerController controller = new CustomerController();
       system.assertEquals(true,controller.createRentalPoolTermination);
       }
   }
   @isTest
   public static void testBooleanRentalPoolAssignment(){

       User u = [Select id, UserName from user ORDER BY CreatedDate DESC LIMIT 1];
       PageReference pageRef = Page.Customer; // Add your VF page Name here
       pageRef.getParameters().put('view', 'RentalPoolAssignment');
       Test.setCurrentPage(pageRef);
       system.runAs(u){
       CustomerController controller = new CustomerController();
       system.assertEquals(true,controller.createRentalPoolAssignment);
       }
   }
     @isTest
   public static void testBooleanPlotHandover(){

       User u = [Select id, UserName from user ORDER BY CreatedDate DESC LIMIT 1];
       PageReference pageRef = Page.Customer; // Add your VF page Name here
       pageRef.getParameters().put('view', 'PlotHandover');
       Test.setCurrentPage(pageRef);
       system.runAs(u){
       CustomerController controller = new CustomerController();
       system.assertEquals(true,controller.createPlotHandover);
       }
   }

   @isTest
   public static void testBooleanMyProfileView(){

       User u = [Select id, UserName from user ORDER BY CreatedDate DESC LIMIT 1];
       PageReference pageRef = Page.Customer; // Add your VF page Name here
       pageRef.getParameters().put('view', 'MyProfileView');
       Test.setCurrentPage(pageRef);
       system.runAs(u){
       CustomerController controller = new CustomerController();
       system.assertEquals(true,controller.isMyProfileView);
       }
   }

     @isTest
   public static void testBooleanUnitDetail(){

       User u = [Select id, UserName from user ORDER BY CreatedDate DESC LIMIT 1];
       PageReference pageRef = Page.Customer; // Add your VF page Name here
       pageRef.getParameters().put('view', 'UnitDetail');
       Test.setCurrentPage(pageRef);
       system.runAs(u){
       CustomerController controller = new CustomerController();
       system.assertEquals(true,controller.isUnitDetail);
       }
   }

    @isTest
    public static void testBooleanCaseComments(){

       User u = [Select id, UserName from user ORDER BY CreatedDate DESC LIMIT 1];
       PageReference pageRef = Page.Customer; // Add your VF page Name here
       pageRef.getParameters().put('view', 'CaseComments');
       Test.setCurrentPage(pageRef);
       system.runAs(u){
       CustomerController controller = new CustomerController();
       system.assertEquals(true,controller.iSCaseComments);
       }
   }

    @isTest
    public static void testBooleanCaseDetails(){

       User u = [Select id, UserName from user ORDER BY CreatedDate DESC LIMIT 1];
       PageReference pageRef = Page.Customer; // Add your VF page Name here
       pageRef.getParameters().put('view', 'CaseDetails');
       Test.setCurrentPage(pageRef);
       system.runAs(u){
       CustomerController controller = new CustomerController();
       system.assertEquals(true,controller.isCaseDetail);
       }
   }

    @isTest
    public static void testBooleanUtilityRegistration(){

       User u = [Select id, UserName from user ORDER BY CreatedDate DESC LIMIT 1];
       PageReference pageRef = Page.Customer; // Add your VF page Name here
       pageRef.getParameters().put('view', 'UtilityRegistration');
       Test.setCurrentPage(pageRef);
       system.runAs(u){
       CustomerController controller = new CustomerController();
       system.assertEquals(true,controller.isUtilityRegistration);
       }
   }
}