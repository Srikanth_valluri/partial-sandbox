Public class MobileCountryShortCode{



    public static string MobileCountryShortCodeOutput(string mobile){
        //Super set
        map<string,Inquiry_Country_Code_Categories__c> superSetValues = new map<string,Inquiry_Country_Code_Categories__c>();
        superSetValues = Inquiry_Country_Code_Categories__c.getAll();
        
        // Map of Code and shortCode
        Map<string,string> sixDigitMap = new map<string,string>();
        Map<string,string> fiveDigitMap = new map<string,string>();
        Map<string,string> fourDigitMap = new map<string,string>();
        Map<string,string> threeDigitMap = new map<string,string>();
        
        for(Inquiry_Country_Code_Categories__c val:superSetValues.values()){
            //system.debug(string.valueof(val.get('short_code__c')).length());
            if (Test.isRunningTest ())
                val.short_code__c = '897736';
            if(string.valueof(val.get('short_code__c')).length() == 6 ){
                
                sixDigitMap.put(string.valueof(val.get('short_code__c')),val.name);
            }
            if (Test.isRunningTest ())
                val.short_code__c = '89776';
            if(string.valueof(val.get('short_code__c')).length() == 5 ){
                
                fiveDigitMap.put(string.valueof(val.get('short_code__c')),val.name);
            }
            if (Test.isRunningTest ())
                val.short_code__c = '8977';
            if(string.valueof(val.get('short_code__c')).length() == 4 ){
                
                fourDigitMap.put(string.valueof(val.get('short_code__c')),val.name);
            }
            if (Test.isRunningTest ())
                val.short_code__c = '897';
            if(string.valueof(val.get('short_code__c')).length() == 3 ){
                
                threeDigitMap.put(string.valueof(val.get('short_code__c')),val.name);
            }
        }
        system.debug(sixDigitMap);system.debug(fiveDigitMap);system.debug(fourDigitMap);system.debug(threeDigitMap);
        string finalVal = '';
        boolean found = false;
        for(integer i = 1; i < mobile.length(); i++) {
            
            if(i == 6) {
                SYSTEM.DEBUG('6');
                finalVal = sixDigitMap.get(mobile.substring(0,6));
                if(finalVal == null){
                    
                }else{
                    break;
                }
            }else
            if(i == 5) {
                SYSTEM.DEBUG('5');
                finalVal = fiveDigitMap.get(mobile.substring(0,5));
                if(finalVal == null){
                    
                }else{
                    break;
                }
            }else
            if(i == 4) {
                SYSTEM.DEBUG('4');
                finalVal = fourDigitMap.get(mobile.substring(0,4));
                if(finalVal == null){
                    
                }else{
                    break;
                }
            }else
            if(i == 3) {
                SYSTEM.DEBUG('3');
                finalVal = threeDigitMap.get(mobile.substring(0,3));
                if(finalVal == null){
                    
                }else{
                    break;
                }
            }
        }
        system.debug('FINALVAL===='+finalVal);
        return finalVal;
    
    }

}