@isTest
private class LoamsCommunityControllerTest {

    @testSetup
    static void setupTestData() {
        // Create common test User
        User communityUser = CommunityTestDataFactory.CreatePortalUser();
    }

    @isTest
    static void testController() {
        User u = [SELECT id, UserName, AccountId FROM user ORDER BY CreatedDate DESC LIMIT 1];
        Test.setCurrentPage(Page.Customer);
        Test.startTest();
            System.runAs(u) {
                LoamsCommunityController controller = new LoamsCommunityController();
                System.assertEquals(NULL, controller.authenticateUser());
                System.assertEquals(CustomerCommunityUtils.getFullPhotoUrl(), controller.getFullPhotoUrl());
                System.assert(LoamsCommunityController.getNotifications().isEmpty());
                System.assertEquals(NULL, LoamsCommunityController.markRead(NULL));
                System.assert(LoamsCommunityController.globalSearch('searchstring').isEmpty());
                System.assertEquals(NULL, LoamsCommunityController.globalSearch(NULL));
            }
        Test.stopTest();
    }

}