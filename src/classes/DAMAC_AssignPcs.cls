public class DAMAC_AssignPcs{
    
    public transient list<string> idsselected{get;set;}
    public transient list<string> idsInvSelected{get;set;}
    public transient String idString{get;set;}
    
    public transient list<string> propStatusToFilter{get;set;}
    
    public transient list<string> citiesToFilter{get;set;}
    
    public transient list<string> unitTypeToFilter{get;set;}
    
    public transient list<string> propertyNameFilter{get;set;}
    
    public transient list<string> acdFilter{get;set;}
    
    public transient list<string> bedRoomFilter{get;set;}
    public transient list<string> inventoryRecsId{get;set;}
    public list<Marketing_Documents__c> marketingNames {get;set;}
    
    public transient list<User> filteredinventory {get;set;}
    
    public transient set<string> marketingNamesFiltered{get;set;}
    
    public transient list<string> packageNamesFilter{get;set;}
    
    public transient list<string> viewTypeFilter{get;set;}
    
    public transient list<string> unitFeaturesFilter{get;set;}
    //public transient String untFtrsToFilterStr{get;set;}
    public String untFtrsToFilterStr{get;set;}
    
    public transient list<string> projFtrsToFilter{get;set;}
    
    public transient List<string> districtFtrsToFilter{get;set;}
    
    public transient integer minsft {get;set;}
    
    public transient integer maxsft {get;set;}
    
    public transient integer maxPriceSft {get;set;}
    
    public transient integer minPriceSft {get;set;}
    
    public transient integer minprice {get;set;}
    
    public transient integer maxprice {get;set;}
    
    public string strSelectedIds { get; set; }
    
    public Campaign__c campaign {get; set; }
    
    //public transient string floorPackagetype {get;set;}
    
    public transient map<string,inventory__c> headerMap{get;set;}
    
    @testvisible private list<Inventory_User__c> otherUserinventory;
    
    @testvisible private set<id> otherInventoryIds;
    
    @testvisible private string status='Released';
    
    public string invs{get;set;}
    
    public boolean ismobile{get;set;}
    
    public list<unit_assignment__c> myuaRecs {get;set;}
    
    public list<eoi_process__c> myeoiRecs {get;set;}
    public string selectedInventoryIdsForBooking {get;set;}
    public string selectedInventoryIdsForBooking1 {get;set;}
    public string selectedProductProjectname{get; set;}
    
    public String uaType { get; set; }
    
    // PDF
    public Map<string, set<String>> mpProperties{get; set;}
    
    public Map<string, List<Inventory__c>> mpAvailable{get; set;}
    
    public Map<string, Map<string, List<Inventory__c>>> mpFinal{get; set;}
    
    public Map<string, Location__c> mpBuildings {get; set;}
    
    public string widthPx{get; set;}
    
    public string ptWidthPx{get; set;}
    
    public Map<String, List<Payment_Plan__c>> mpPaymentPlans{get; set;}
    
    public Map<Id, List<Payment_Terms__c>> mpPaymentTerms{get; set;}
    
    public string planKeyStr{get; set;}
    
    public string pTermKeyStr{get; set;}
    
    public boolean render{get;set;}
    
    public string renderaspdf{get;set;}
    
    public integer offsetval {get;set;}
    public integer recNum {get;set;}
    public integer limits {get;set;}
    public integer limitVal {get;set;}
    public String searchText {get;set;}
    public String searchTextVal {get;set;}
    public String sortOrderVal {get;set;}
    public String sortColVal {get;set;}
    public Integer totalRecs {get;set;}
    public Integer startIndex {get;set;}
    public Integer endIndex {get;set;}
    public Id currentUser {get;set;}
    public string selInventories {get;set;}
    public Map<String,String> promoAndOfferMap {get;set;}
    public Boolean hasPromo {get;set;}
    public Boolean hasCampaign {get;set;}
    public Boolean hasScheme {get;set;}
    public Boolean hasOptions {get;set;}
    public Boolean hasUnit {get;set;}
    Set<String> projID = new Set<String>();
    Set<String> marktID = new Set<String>();
    List<String> untFtrsToFilterList = new List<String>();
    Set <ID> existingUserIds = new Set <ID> ();
    public Map<String, Map<String,String>> unitIdResponceMap {get;set;}
    public List<String> selectedIdsList {get;set;}
    public Map<String, Map<String,Boolean>> promoOfferCheckMap {get;set;}
    public Map<String,Boolean> promoOfferCheckInnerMap {get;set;}
    
    //Campaign section
    public List<Inventory_User__c> thisUserInventory;
    public Map<Id,String> invId_campaignIdmap {get;set;}
    public Map<ID,String> invId_campaignnamemap{get;set;}
    
    //Download Functionality
    public static String downloadpdf;
    
    //Currency conversion
    public Map<String,Map<String,Decimal>> fromToCurrencyMap;
    public String tocurrencyselected{get; set;}
    public String tocurrencyselectedtemp;
    //public list<inventory__c> filteredconvertedcurrencyinventory {get;set;}
    public Map<Id,Decimal> invid_pricerange{get; set;}
    public Map<Id,Decimal> invid_pricepersft{get; set;}
    public Map<Id,Decimal> invid_basePrice{get; set;}
    public Map<Id,Decimal> invid_vat{get; set;}
    public Map<Id,String> invid_currencyofsale{get; set;}
    //public List<SelectOption> currencyoptions{get;set;}
    
    // v1.1 Craig  - Share Project Details
    public Account searchAcc        {get; set;}
    public Inquiry__c searchInq     {get; set;}
    public String emailSubject     {get; set;}
    public String emaiBody          {get; set;}
    public Inventory__c inv { get; set; }
    
    /*  Start and End date values */
    public String startdateday {get;set;}
    public String startdatemonth{get;set;}
    public String startdateyear {get;set;}
    public String endDateday {get;set;}
    public String endDatemonth{get;set;}
    public String endDateyear {get;set;}
    
    public set<String> statusVal {get;set;} 
    public String currentRecordId {get;set;}
    public Search_User__c searchSection {get;set;}
    public String searchSectionAgencyName {get;set;}
    public String searchSectionTypeofAccount {get;set;}
    public String searchSectionCity {get;set;}
    public String searchSectionCountry {get;set;}
    public transient list<User> filteredUser {get;set;}
    public String pcName{get;set;}
    public String hosName{get;set;}
    public String dosName{get;set;}
    public User userCus {get;set;}
    public String hiddenVal{get;set;}
    public String hodName{get;set;}
    public List <Inventory__c> existingInventories { get; set; }
    public DAMAC_AssignPcs(Apexpages.standardController controller){
        inv = new Inventory__c ();
        searchSection = new Search_User__c();
        userCus = New User();
        statusVal = New Set<String> {'Released', 'Restricted_Apartments_In_Inventory', 'Apartments_In_Inventory'};
        currentRecordId  = ApexPages.CurrentPage().getparameters().get('id');
        existingUserIds = new Set <ID> ();
        
        existingInventories = new List <Inventory__c> ();
        existingInventories = [SELECT Name FROM Inventory__c WHERE Unit_Allocation__c =: currentRecordId AND Unit_Allocation__c != NULL LIMIT 10];
        
        for (Inventory_User__c inv : [SELECT User__c FROM Inventory_User__c WHERE Unit_Allocation__c =: currentRecordId]) {
            existingUserIds.add (inv.User__c);
        }
        hasPromo = false;
        hasCampaign = false;
        hasScheme = false;
        hasOptions = false;
        hasUnit = false;
        offsetval = 0;
        recNum = 0;
        limits = 500;
        emaiBody = 'Please find the Sales offer attachment';
        emailSubject ='Damac Sales Offer pdf';
        hiddenVal ='';
        currentUser = userinfo.getuserid();
        pageload();
    }
    
    public void isS1(){
        ismobile = false;
    }
    
    
    /****************************************************************************************
Method:         getRecCount
Description:    Get record count of avaliable inventory 
**************************************************************************************************/
    public List<SelectOption> teamNames {
        get{
            if(teamNames == null){
                teamNames = new List<SelectOption>();
                teamNames.add(new SelectOption('','--Select Team--'));
                for(Group gr : new List<Group>([SELECT ID,DeveloperName FROM Group WHERE DeveloperName LIKE '%DAMAC_TEAM%'])){
                    teamNames.add(new SelectOption(gr.id,gr.DeveloperName.subStringBefore('_DAMAC_TEAM')));
                }
            }
            return teamNames;
        }
        set;
        
    }
    public Integer getRecCount(String pageLoadParam){
    List <Account> accounts = new List <Account> ();
        if(pageLoadParam == 'pageLoadCall'){
            untFtrsToFilterStr = ' ';
            tocurrencyselectedtemp = ' ';
            offsetval = 0;
            limits = 500;
        }
        List<User> inventoryRecs = new List<User>();
        if(recNum != 0){
            startIndex = offsetval +1;
        }
        else{
            startIndex = offsetval;
        }
        
        if(limits < recNum){
            endIndex = offsetval + limits;
        }
        else{
            endIndex = offsetval + recNum;
        }
        
        String queryString =   'Select Title, Name,Email,Languages_Known__c,HOS_Name__c,HOD_Name__c,DOS_Name__c,UserName,Profile.Name,Contact.Account.Name,Contact.Account.Country__c,Contact.Account.City__c FROM User WHERE Profile.Name LIKE \'%Property Consultant%\' AND  IsActive = true AND ID NOT IN: existingUserIds  ';
        system.debug(pcName+'-'+hosName+'-'+dosName+'-'+userCus.New_Team__c+'-'+searchSection.Sales_Office__c);
        system.debug(hiddenVal+'-'+searchSectionAgencyName+'-'+searchSectionCity+'-'+searchSectionCountry+'-'+searchSectionTypeofAccount);
        if (pcName != '' && pcName != NULL) {
            
            queryString += 'AND Name LIKE \'%'+pcName+'%\'';
        }
        if (hosName != '' && hosName != NULL) {
            
            queryString += 'AND HOS_Name__c LIKE \'%'+hosName+'%\'';
        }
        if (dosName != '' && dosName != NULL) {
            
            queryString += 'AND DOS_Name__c LIKE \'%'+dosName+'%\' ';
        }
        if (hodName != '' && hodName != NULL) {
            
            queryString += 'AND HOD_Name__c LIKE \'%'+hodName+'%\' ';
        }
        
        if(userCus.New_Team__c != '' && userCus.New_Team__c != null){
            String userTeam = userCus.New_Team__c;
            queryString += ' AND  New_Team__c  =: userTeam  ';              
        }
        if( searchSection.Sales_Office__c != '' && searchSection.Sales_Office__c != null) {
            queryString +=  ' AND Sales_Office__c LIKE \'%'+searchSection.Sales_Office__c+'%\'';
        }
        Set <ID> userIds = new Set <ID> ();
        system.debug(hiddenVal +'hiddenVal =====');
        if(hiddenVal != '' && hiddenVal != null) {
            queryString = 'Select Title, Name,Email,Languages_Known__c,HOS_Name__c,HOD_Name__c,DOS_Name__c,UserName,Profile.Name,Contact.Account.Name,Contact.Account.Country__c ,Contact.Account.City__c FROM USER WHERE ID NOT IN: existingUserIds AND isActive = true ';
            String accountQuery = 'SELECT Id FROM Account WHERE id != null';
            if (searchSectionAgencyName != '' && searchSectionAgencyName != NULL)
                accountQuery += ' AND  Name LIKE \'%'+searchSectionAgencyName+'%\'';
            
            if (searchSectionCity != '' && searchSectionCity  != NULL)
                accountQuery += ' AND City__c=: searchSectionCity ';
            
            if (searchSectionCountry != '' && searchSectionCountry  != NULL)
                accountQuery += ' AND Country__c=: searchSectionCountry ';
            
            
            if (searchSectionTypeofAccount != '' && searchSectionTypeofAccount != NULL) {
                searchSectionTypeofAccount = searchSectionTypeofAccount.removeStart('[');
                searchSectionTypeofAccount = searchSectionTypeofAccount.removeEnd(']');
                if(searchSectionTypeofAccount != '' && searchSectionTypeofAccount != null) 
                    accountQuery += ' AND RecordType.Name =: searchSectionTypeofAccount ';
            }
            
            System.Debug (accountQuery);
            if(accountQuery.endsWith('WHERE')) {
                accountQuery = accountQuery.removeEnd('WHERE');
            }
            accountQuery += ' limit 10000';
            
            // if (accountQuery.trim () != 'SELECT Id FROM Account WHERE id != null')
            accounts = Database.query (accountQuery);
            
            if (accounts.size () > 0) {
                for (Contact con : [SELECT Salesforce_User__c FROM Contact WHERE AccountID IN : accounts AND Salesforce_User__c != NULL  LIMIT 10000]) {
                    if (con.Salesforce_User__c != NULL) 
                        userIds.add (con.Salesforce_User__c);
                }
            }
            if (userIds.size () > 0) {
                queryString += ' AND ID IN : userIds  LIMIT 10000 ';
            }else {
                queryString += ' LIMIT 10000 ';
            }
        }
        system.debug(queryString +'queryString 312');
        if ((accounts.size () > 0 && userIds.size () > 0 )|| hiddenVal == '') {
            inventoryRecs = database.query(queryString);
        } else {
            inventoryRecs = New List<User> ();
        }
        system.debug(inventoryRecs+'inventoryRecs====');
        inventoryRecsId = new List<String>();
        for(User invObj : inventoryRecs){
            inventoryRecsId.add(String.valueOf(invObj.id));
        }
        idString = string.join(inventoryRecsId,',');
        system.debug('>>>>idString>>>>>'+idString);
        return inventoryRecs.size();
    }
    /****************************************************************************************
Method:         filterData
Description:    filter avaliablity as per user applied filters
**************************************************************************************************/
    
    public PageReference filterData() {
        List <Account> accounts = new List <Account> ();
        offsetval = 0;
        recNum = 0;
        limits =10;
        searchTextVal = '';
        tocurrencyselectedtemp = ' ';
        
        isS1();
        
        
        
        String queryString =   'Select Title, Name,Email,Languages_Known__c,HOS_Name__c,HOD_Name__c,DOS_Name__c,UserName,Profile.Name FROM User WHERE Profile.Name LIKE \'%Property Consultant%\'  AND  IsActive = true AND ID NOT IN: existingUserIds  ';
        system.debug(pcName+'-'+hosName+'-'+dosName+'-'+userCus.New_Team__c+'-'+searchSection.Sales_Office__c);
        if (pcName != '' && pcName != NULL) {
            
            queryString += 'AND Name LIKE \'%'+pcName+'%\'';
        }
        if (hosName != '' && hosName != NULL) {
            
            queryString += 'AND HOS_Name__c LIKE \'%'+hosName+'%\'';
        }
        if (dosName != '' && dosName != NULL) {
            
            queryString += 'AND DOS_Name__c LIKE \'%'+dosName+'%\' ';
        }
        if (hodName != '' && hodName != NULL) {
            
            queryString += 'AND HOD_Name__c LIKE \'%'+hodName+'%\' ';
        }
        if(userCus.New_Team__c != '' && userCus.New_Team__c != null){
            String userTeam = userCus.New_Team__c;
            queryString += ' AND  New_Team__c  =: userTeam ';              
        }
        if( searchSection.Sales_Office__c != '' && searchSection.Sales_Office__c != null) {
            queryString +=  ' AND Sales_Office__c LIKE \'%'+searchSection.Sales_Office__c+'%\'';
        }
        Set <ID> userIds = new Set <ID> ();
        system.debug(hiddenVal +'hiddenVal =====');
        if(hiddenVal != '' && hiddenVal != null) {
            queryString = 'Select Title, Name,Email,Languages_Known__c,HOS_Name__c,HOD_Name__c,DOS_Name__c,UserName,Profile.Name,Contact.Account.Name,Contact.Account.Country__c,Contact.Account.City__c FROM USER WHERE ID NOT IN : existingUserIds AND isActive = true ';
            String accountQuery = 'SELECT Id FROM Account WHERE id != null';
            if (searchSectionAgencyName != '' && searchSectionAgencyName != NULL)
                accountQuery += ' AND  Name LIKE \'%'+ searchSectionAgencyName+'%\'';
            
            if (searchSectionCity != '' && searchSectionCity  != NULL)
                accountQuery += ' AND City__c =: searchSectionCity ';
            
            if (searchSectionCountry != '' && searchSectionCountry  != NULL)
                accountQuery += ' AND Country__c =: searchSectionCountry ';
            
            
            if (searchSectionTypeofAccount != '' && searchSectionTypeofAccount != NULL) {
                searchSectionTypeofAccount = searchSectionTypeofAccount.removeStart('[');
                searchSectionTypeofAccount = searchSectionTypeofAccount.removeEnd(']');
                if(searchSectionTypeofAccount != '' && searchSectionTypeofAccount != null) 
                    accountQuery += ' AND RecordType.Name =: searchSectionTypeofAccount ';
            }
            
            System.Debug (accountQuery);
            if(accountQuery.endsWith('WHERE')) {
                accountQuery = accountQuery.removeEnd('WHERE');
            }
            accountQuery += ' limit 10000';
            
            // if (accountQuery.trim () != 'SELECT Id FROM Account WHERE id != null')
            accounts = Database.query (accountQuery);
            
            if (accounts.size () > 0) {
                for (Contact con : [SELECT Salesforce_User__c FROM Contact WHERE AccountID IN : accounts AND Salesforce_User__c != NULL LIMIT 10000]) {
                    if (con.Salesforce_User__c != NULL)
                        userIds.add (con.Salesforce_User__c);
                }
            }
            if (userIds.size () > 0) {
                queryString += ' AND ID IN : userIds LIMIT :limits OFFSET: offsetval ';
            }
            else {
                queryString += ' LIMIT :limits OFFSET: offsetval';
            }
        }
        if ((accounts.size () > 0 && userIds.size () > 0 ) || hiddenVal == '') {
            filteredinventory = database.query(querystring);
        }else {
        filteredinventory = New List<User> ();
        }
        marketingNamesFiltered = new set<string>();
        headerMap = new map<string,inventory__c>();
        recNum = filteredinventory.size();
        totalRecs = getRecCount('noPageLoadCall');
        return null;
    }
    
    set<string> cities = new set<string>();
    set<string> propStatuses = new set<string>();
    set<string> unitTypes = new set<string>();
    set<string> unitss = new set<string>();
    set<string> propertyNames = new set<string>();
    set<string> acd = new set<string>();
    set<string> bedrooms= new set<string>();
    set<string> viewTypes = new set<string>();
    set<string> pkgNames = new set<string>();
    set<string> prjctFtrs = new set<string>();
    set<string> district =new set<String>();
    Map<String,id> productname_idmap = new Map<String,Id>();
    
    /****************************************************************************************
Method:         dataPagination
Description:    filter avaliablity as per user applied filters
**************************************************************************************************/
    public void dataPagination(){
         List <Account> accounts = new List <Account> ();
        String queryString =   'Select Title, Name,Email,Languages_Known__c,HOS_Name__c,HOD_Name__c,DOS_Name__c,UserName,Profile.Name FROM User WHERE Profile.Name LIKE \'%Property Consultant%\'  AND ID NOT IN: existingUserIds AND IsActive = true  ';
        system.debug(pcName+'-'+hosName+'-'+dosName+'-'+userCus.New_Team__c+'-'+searchSection.Sales_Office__c);
        if (pcName != '' && pcName != NULL) {
            
            queryString += 'AND Name LIKE \'%'+pcName+'%\'';
        }
        if (hosName != '' && hosName != NULL) {
            
            queryString += 'AND HOS_Name__c LIKE \'%'+hosName+'%\'';
        }
        if (dosName != '' && dosName != NULL) {
            
            queryString += 'AND DOS_Name__c LIKE \'%'+dosName+'%\' ';
        }
        if (hodName != '' && hodName != NULL) {
            
            queryString += 'AND HOD_Name__c LIKE \'%'+hodName+'%\' ';
        }
        if(userCus.New_Team__c != '' && userCus.New_Team__c != null){
            String userTeam = userCus.New_Team__c;
            queryString += ' AND  New_Team__c  =: userTeam  ';              
        }
        if( searchSection.Sales_Office__c != '' && searchSection.Sales_Office__c != null) {
            queryString +=  ' AND Sales_Office__c LIKE \'%'+searchSection.Sales_Office__c+'%\'';
        }
        system.debug(hiddenVal +'hiddenVal =====');
        if(hiddenVal != '' && hiddenVal != null) {
            queryString = 'Select Title, Name,Email,Languages_Known__c,HOS_Name__c,HOD_Name__c,DOS_Name__c,UserName,Profile.Name,Contact.Account.Name,Contact.Account.Country__c ,Contact.Account.City__c FROM USER WHERE ID NOT IN: existingUserIds AND isActive = true ';
            String accountQuery = 'SELECT Id FROM Account WHERE id != null';
            if (searchSectionAgencyName != '' && searchSectionAgencyName != NULL)
                accountQuery += ' AND  Name LIKE \'%'+ searchSectionAgencyName+'%\'';
            
            if (searchSectionCity != '' && searchSectionCity  != NULL)
                accountQuery += ' AND City__c =: searchSectionCity ';
            
            if (searchSectionCountry != '' && searchSectionCountry  != NULL)
                accountQuery += ' AND Country__c =: searchSectionCountry ';
            
            
            if (searchSectionTypeofAccount != '' && searchSectionTypeofAccount != NULL) {
                searchSectionTypeofAccount = searchSectionTypeofAccount.removeStart('[');
                searchSectionTypeofAccount = searchSectionTypeofAccount.removeEnd(']');
                if(searchSectionTypeofAccount != '' && searchSectionTypeofAccount != null) 
                    accountQuery += ' AND RecordType.Name =: searchSectionTypeofAccount ';
            }
            
            System.Debug (accountQuery);
            if(accountQuery.endsWith('WHERE')) {
                accountQuery = accountQuery.removeEnd('WHERE');
            }
            accountQuery += ' limit 10000';
           
            // if (accountQuery.trim () != 'SELECT Id FROM Account WHERE id != null')
            accounts = Database.query (accountQuery);
            Set <ID> userIds = new Set <ID> ();
            if (accounts.size () > 0) {
                for (Contact con : [SELECT Salesforce_User__c FROM Contact WHERE AccountID IN : accounts AND Salesforce_User__c != NULL  LIMIT 10000]) {
                    if (con.Salesforce_User__c != NULL)
                        userIds.add (con.Salesforce_User__c);
                }
            }
            if (userIds.size () > 0 || hiddenVal == '') {
                queryString += ' AND ID IN : userIds LIMIT :limits ';
            }
            else {
                queryString += ' LIMIT :limits ';
            }
        }
        
        String sortParam;
        
        // Sorting====
        system.debug(limits + '-'+offsetval+'----');
        if(sortColVal == 'none' || sortColVal == NULL){
            queryString += '  OFFSET: offsetval'; //AND Building_ID__c != NULL AND lastmodifieddate<LAST_N_DAYS:2
        }
        else
        {
            queryString = queryString.removeEnd(' LIMIT :limits ');
            if(sortColVal == 'Name'){
                sortParam = ' Name '+sortOrderVal+'';
            }
            else if(sortColVal == 'UserName'){
                sortParam = ' UserName ' +sortOrderVal+'';
            }
           
            else if(sortColVal == 'DosName'){
                sortParam = ' dos_Name__c '+sortOrderVal+'';
            }
            else if(sortColVal == 'HosName'){
                sortParam = ' Hos_Name__c '+sortOrderVal+'';
            }
            else if(sortColVal == 'HodName'){
                sortParam = ' hod_Name__c '+sortOrderVal+'';
            }
            else if(sortColVal == 'agencyName'){
                sortParam = ' Contact.Account.Name '+sortOrderVal+'';
            }
            else if(sortColVal == 'city'){
                sortParam = ' Contact.Account.City__c '+sortOrderVal+'';
            }
            else if(sortColVal == 'country'){
                sortParam = ' Contact.Account.Country__c '+sortOrderVal+'';
            }
            queryString += ' ORDER BY '+sortParam+' LIMIT :limits OFFSET: offsetval';
        }
        system.debug('queryString=='+queryString);
        if (accounts.size() > 0 || hiddenVal == '') {
            filteredinventory = database.query(querystring);
        } else {
            filteredinventory = New List<User> ();
        }
        recNum = filteredinventory.size();
        totalRecs = getRecCount('noPageLoadCall');
        //populateCampaignsection(filteredinventory);
        System.debug('@@@ data pagination called @@@');
    }
    
    
    
   
    public void pageload(){
        isS1();
        maxsft = 0;
        minsft = 0;
        maxprice = 0;
        minprice = 0;
        maxPriceSft = 0;
        minPriceSft = 0;
        campaign = new Campaign__c ();
        mpProperties = new Map<String,set<String>>();
        mpAvailable = new Map<String, list<Inventory__c>>();
        mpFinal = new Map<string, Map<string, List<Inventory__c>>>();
        mpBuildings = new Map<String,Location__c>();
        mpPaymentTerms = new Map<Id, List<Payment_Terms__c>>();
        mpPaymentPlans = new Map<String, List<Payment_Plan__c>>();
        planKeyStr = '';
        render = true;
        renderaspdf = '';
        otherUserinventory = new list<Inventory_User__c>();
        thisUserInventory = new list<Inventory_User__c>();
        
        
        idsselected = new list<string>();
        idsInvSelected = new List<String>();
        propStatusToFilter  = new list<string>();
        unitTypeToFilter = new list<string>();
        citiesToFilter  = new list<string>();
        propertyNameFilter  = new list<string>();
        acdFilter = new list<string>();
        filteredinventory = new list<User>();
        //filteredconvertedcurrencyinventory = new List<inventory__c>();
        marketingNames = new list<Marketing_Documents__c>();
        bedRoomFilter =  new list<string>();
        packageNamesFilter = new list<String>();
        viewTypeFilter = new list<String>();
        unitFeaturesFilter = new list<String>();
        projFtrsToFilter  = new list<String>();
        districtFtrsToFilter = new list<String>();
        myuaRecs = new list<unit_assignment__c>();
        myeoiRecs  = new list<eoi_process__c>();
        
        String  queryString =   'Select Id,Title, Name,Email,Languages_Known__c,HOS_Name__c,HOD_Name__c,DOS_Name__c,UserName,Profile.Name FROM User WHERE (Profile.Name LIKE \'%Property Consultant%\'  AND ID NOT IN: existingUserIds AND IsActive = true) LIMIT :limits OFFSET: offsetval';
        
        
        system.debug(queryString +'queryString ==============');
        filteredinventory =  database.query(queryString);
        recNum = filteredinventory.size();        
        totalRecs = getRecCount('pageLoadCall');
    }
  
    
    /***********************************************************************************************************************/      
    
    public void nextbtn(){
        offsetval += limits ;
        dataPagination();
    }
    
    public void prvbtn(){
        if(offsetval >= limits){
            offsetval -= limits ;
        }
        else{
            offsetval -= offsetval ;
        }
        dataPagination();
    }
    public void refreshTableData() {
        hiddenVal = Apexpages.currentPage().getParameters().get('assignAgencyVal');
        filterData();
    }
    public void changeRecCountFunMethod(){
        
        limits = integer.valueof((Apexpages.currentPage().getParameters().get('limitVal')));
        system.debug(limits+'limits=====');
        dataPagination();
    }
    
    public void changeSortOrder(){
        sortOrderVal = Apexpages.currentPage().getParameters().get('sortOrder');
        sortColVal = Apexpages.currentPage().getParameters().get('sortCol');
        dataPagination();
    }
    
    public void searchRecs(){
        offsetval = 0;
        searchTextVal = Apexpages.currentPage().getParameters().get('searchText');
        dataPagination();
    }
    
    public boolean getprv(){
        if(offsetval == 0){
            return true;
        }
        else{
            return false;
        }
    }
    
    public boolean getnxt(){
        if(recNum < limits){
            return true;
        }
        else{
            return false;
        }
    }
    
    
  
   
    
    /**
* v1.1 Craig  - Share Project Details
* Method to Search Account
*/
    @RemoteAction 
    public static List<Account> getAccounts(String pSearchKey) {
        String searchString = '%'
            + pSearchKey 
            + '%';
        List<Account> accountList = new List<Account>();
        accountList = [ SELECT Id, Name, Email__c
                       FROM Account 
                       WHERE OwnerId = :UserInfo.getUserId()
                       AND Name LIKE :searchString 
                      ]; 
        System.debug('accountList:::: '+accountList);
        return accountList;
    }
    
    
  
    
    @remoteAction
    public static String saveInventoriesMethod(String unitAllocationId,String selectedUserIds) {
        system.debug('unitAllocationId===='+unitAllocationId);
        try {
            System.Debug (selectedUserIds);
            if (selectedUserIds != NULL && unitAllocationId != '') {
                List<String> userIds = New List<String> ();
                selectedUserIds = selectedUserIds.removeStart(',');
                if(selectedUserIds.contains(',')) {
                    userIds = selectedUserIds.split(',');
                }
                else  {
                    userIds.add(selectedUserIds);
                }
                List<Inventory_User__c> invUserList = New List<Inventory_User__c> ();
                List<Inventory__c> invList = New List<Inventory__c> ();                
                for(Inventory__c inv : [SELECT Unit_Allocation__c,Unit_Allocation__r.Start_Date__c,Unit_Allocation__r.End_Date__c FROM Inventory__c WHERE Unit_Allocation__c =: unitAllocationId]) {
                    for(String eachUserId : userIds) {
                        Inventory_User__c invUser = New Inventory_User__c ();
                        invUser.Unit_Allocation__c = unitAllocationId;
                        invUser.Inventory__c = inv.id;
                        invUser.User__c = eachUserId;
                        invUser.unique_key__c = inv.Id+'###'+eachUserId;
                        invUser.Start_Date__c = inv.Unit_Allocation__r.Start_Date__c;
                        invUser.End_Date__c = inv.Unit_Allocation__r.End_Date__c ;
                        invUserList.add(invUser);
                    }
                }
                system.debug(invUserList);
                if(invUserList.size() > 0) {
                    system.debug(invUserList +'-----');
                    Upsert invUserList  unique_key__c;
                }
                
            }
            return 'success';
        }  catch (exception e) {
            System.Debug (e.getMessage ());
            return e.getMessage ();
            
        }
    }
    
    
}