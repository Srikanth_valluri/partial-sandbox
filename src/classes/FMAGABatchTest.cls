/*-------------------------------------------------------------------------------------------------
Description: Test class for FMAGABatch, FMAGAMeetingBatch & its schedulars(FMAGABatchScheduler,FMAGAMeetingBatchScheduler)
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 19-02-2019       | Lochana Rajput   | 1. Added functionality to test above mentioned classes
=============================================================================================================================
*/
@isTest
private class FMAGABatchTest
{
	private static testMethod void testFMAGABatch(){
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        Account objAcc = new Account();
		objAcc = TestDataFactory_CRM.createPersonAccount();
		insert objAcc;
		
		NSIBPM__Service_Request__c objServReq = TestDataFactory_CRM.createServiceRequest();
        insert objServReq ;

        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objServReq.Id,1);
        insert lstBooking ;

        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus ;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking ,3);
        for( Booking_Unit__c objUnit : lstBookingUnit  ) {
            objUnit.Resident__c = objAcc.Id ;
            objUnit.Handover_Flag__c = 'Y' ;
        }
        lstBookingUnit[2].Owner__c = objAcc.Id ;
        insert lstBookingUnit ;

		Location__c objLoc = new Location__c();
		objLoc = TestDataFactoryFM.createLocation();
		Id recTypeIdLoc=Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
		objLoc.RecordTypeId = recTypeIdLoc;
		objLoc.Property_Name__c = objProperty.id;
		insert objLoc;

		FM_User__c objFMUser = new FM_User__c(FM_User__c=UserInfo.getUserId(),
					FM_Role__c = 'Property Manager',
					Building__c = objLoc.Id);
		insert objFMUser;

		AGA_Schedule__c objAGA = new AGA_Schedule__c();
		objAGA.Property__c = objProperty.id;
		objAGA.Type_of_Meeting__c = 'AGA meeting';
		objAGA.Meeting_Date__c = system.today() - 2;
		insert objAGA;

		AGA_Schedule__c objAGANew = new AGA_Schedule__c();
		objAGANew.Property__c = objProperty.id;
		objAGANew.Type_of_Meeting__c = 'AGA meeting';
		objAGANew.Meeting_Date__c = system.today() - 3;
		insert objAGANew;
		       
        Id recTypeId=Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('AGA Process').getRecordTypeId();
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Booking_Unit__c=lstBookingUnit[0].id;
        fmCaseObj.Email__c='a@gmail.com';
        fmCaseObj.Mobile_no__c='12121212';
        fmCaseObj.RecordTypeId=recTypeId;
        fmCaseObj.Status__c='Submitted';
        fmCaseObj.Origin__c='Portal';
        fmCaseObj.Approval_Status__c = 'Pending';
        fmCaseObj.Submit_for_Approval__c = true;
        fmCaseObj.Account__c=objAcc.id;
        fmCaseObj.Current_Approver__c = 'FM Manager__';
        fmCaseObj.Tenant_Email__c = 'test@gmail.com';
		fmCaseObj.AGA_Schedule__c = objAGA.id;
        insert fmCaseObj;

		Id recTypeIdFM=Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Appointment').getRecordTypeId();
        FM_Case__c fmCaseObjNew=new FM_Case__c();
        fmCaseObjNew.Issue_Date__c=Date.today();
        fmCaseObjNew.Contact_person__c='test';
        fmCaseObjNew.Description__c='test';
        fmCaseObjNew.Contact_person_contractor__c='test';
        fmCaseObjNew.Booking_Unit__c=lstBookingUnit[0].id;
        fmCaseObjNew.Email__c='a@gmail.com';
        fmCaseObjNew.Mobile_no__c='12121212';
        fmCaseObjNew.RecordTypeId=recTypeIdFM;
        fmCaseObjNew.Status__c='Submitted';
        fmCaseObjNew.Origin__c='Portal';
        fmCaseObjNew.Approval_Status__c = 'Pending';
        fmCaseObjNew.Submit_for_Approval__c = true;
        fmCaseObjNew.Account__c=objAcc.id;
        fmCaseObjNew.Current_Approver__c = 'FM Manager__';
        fmCaseObjNew.Tenant_Email__c = 'test@gmail.com';
		fmCaseObjNew.AGA_Schedule__c = objAGANew.id;
        insert fmCaseObjNew;

        Test.startTest();        
        FMAGABatch objClass = new FMAGABatch();
        Database.Executebatch(objClass);
        Test.stopTest();
    }

	public static testMethod void testFMAGABatchschedule() {
		Test.StartTest();
		FMAGABatchScheduler sh1 = new FMAGABatchScheduler();
		FMAGABatchScheduler.cronExp = '0 0 8 * * ?';
		FMAGABatchScheduler.jobName = 'FMAGABatch';
		sh1.scheduleMyJob();		 
		//system.schedule('FMAGABatch', sch, sh1);
		Test.stopTest(); 
	}

	private static testMethod void testFMAGAMeetingBatch(){
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        Account objAcc = new Account();
		objAcc = TestDataFactory_CRM.createPersonAccount();
		insert objAcc;
		
		NSIBPM__Service_Request__c objServReq = TestDataFactory_CRM.createServiceRequest();
        insert objServReq ;

        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objServReq.Id,1);
        insert lstBooking ;

        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus ;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking ,3);
        for( Booking_Unit__c objUnit : lstBookingUnit  ) {
            objUnit.Resident__c = objAcc.Id ;
            objUnit.Handover_Flag__c = 'Y' ;
        }
        lstBookingUnit[2].Owner__c = objAcc.Id ;
        insert lstBookingUnit ;

		Location__c objLoc = new Location__c();
		objLoc = TestDataFactoryFM.createLocation();
		Id recTypeIdLoc=Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
		objLoc.RecordTypeId = recTypeIdLoc;
		objLoc.Property_Name__c = objProperty.id;
		insert objLoc;

		FM_User__c objFMUser = new FM_User__c(FM_User__c=UserInfo.getUserId(),
					FM_Role__c = 'Property Manager',
					Building__c = objLoc.Id);
		insert objFMUser;

		AGA_Schedule__c objAGA = new AGA_Schedule__c();
		objAGA.Property__c = objProperty.id;
		objAGA.Type_of_Meeting__c = 'AGA meeting';
		objAGA.Meeting_Date__c = system.today();
		insert objAGA;

		AGA_Schedule__c objAGANew = new AGA_Schedule__c();
		objAGANew.Property__c = objProperty.id;
		objAGANew.Type_of_Meeting__c = 'AGA meeting';
		objAGANew.Meeting_Date__c = system.today();
		insert objAGANew;
		       
        Id recTypeId=Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('AGA Process').getRecordTypeId();
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Booking_Unit__c=lstBookingUnit[0].id;
        fmCaseObj.Email__c='a@gmail.com';
        fmCaseObj.Mobile_no__c='12121212';
        fmCaseObj.RecordTypeId=recTypeId;
        fmCaseObj.Status__c='Submitted';
        fmCaseObj.Origin__c='Portal';
        fmCaseObj.Approval_Status__c = 'Pending';
        fmCaseObj.Submit_for_Approval__c = true;
        fmCaseObj.Account__c=objAcc.id;
        fmCaseObj.Current_Approver__c = 'FM Manager__';
        fmCaseObj.Tenant_Email__c = 'test@gmail.com';
		fmCaseObj.AGA_Schedule__c = objAGA.id;
        insert fmCaseObj;

		Id recTypeIdFM=Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Appointment').getRecordTypeId();
        FM_Case__c fmCaseObjNew=new FM_Case__c();
        fmCaseObjNew.Issue_Date__c=Date.today();
        fmCaseObjNew.Contact_person__c='test';
        fmCaseObjNew.Description__c='test';
        fmCaseObjNew.Contact_person_contractor__c='test';
        fmCaseObjNew.Booking_Unit__c=lstBookingUnit[0].id;
        fmCaseObjNew.Email__c='a@gmail.com';
        fmCaseObjNew.Mobile_no__c='12121212';
        fmCaseObjNew.RecordTypeId=recTypeIdFM;
        fmCaseObjNew.Status__c='Submitted';
        fmCaseObjNew.Origin__c='Portal';
        fmCaseObjNew.Approval_Status__c = 'Pending';
        fmCaseObjNew.Submit_for_Approval__c = true;
        fmCaseObjNew.Account__c=objAcc.id;
        fmCaseObjNew.Current_Approver__c = 'FM Manager__';
        fmCaseObjNew.Tenant_Email__c = 'test@gmail.com';
		fmCaseObjNew.AGA_Schedule__c = objAGANew.id;
        insert fmCaseObjNew;

        Test.startTest();        
        FMAGAMeetingBatch objClass = new FMAGAMeetingBatch();
        Database.Executebatch(objClass);
        Test.stopTest();
    }

	public static testMethod void testFMAGAMeetingBatchschedule() {
		Test.StartTest();
		FMAGAMeetingBatchScheduler sh1 = new FMAGAMeetingBatchScheduler();
		FMAGAMeetingBatchScheduler.cronExp = '0 0 8 * * ?';
		FMAGAMeetingBatchScheduler.jobName = 'FMAGAMeetingBatch';
		sh1.scheduleMyJob();		 
		//system.schedule('FMAGABatch', sch, sh1);
		Test.stopTest(); 
	}
}