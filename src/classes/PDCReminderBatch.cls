/**********************************************************************************************
Description: Batch to send PDC reminder email
----------------------------------------------------------------------------------------------
Version Date(DD-MM-YYYY)    Author              Version History
-----------------------------------------------------------------------------------------------
1.1     09-09-2020          Aishwarya Todkar    Used account email address
***********************************************************************************************/
public class PDCReminderBatch implements Database.Batchable<sObject>
                                        , Database.Stateful
                                        , Database.AllowsCallouts {
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([SELECT 
                                            Id
                                            , Name
                                            , Amount__c
                                            , Cheque_Date__c
                                            , Cheque_Number__c
                                            , Party_Id__c
                                            , Registration_ID__c
                                            , Booking_Unit__r.Unit_Name__c
                                            , Account__r.Person_Business_Email__c 
                                        FROM 
                                            Post_Dated_Cheque__c
                                        WHERE  
                                            Cheque_Date__c = Next_N_DAYS:3 ] ); 
        
    }
    public void execute(Database.BatchableContext bc, List<Post_Dated_Cheque__c> scope){
        for( Post_Dated_Cheque__c objPdc : scope ) {
            sendmail( objPdc );
        }
    }
    
    public void finish(Database.BatchableContext bc){
        
    }
    
    public void sendmail( Post_Dated_Cheque__c objPDC ){
        List<Attachment> lstAttach = new List<Attachment>();
        List<EmailMessage> lstEmails = new List<EmailMessage>();
        

        EmailTemplate emailTemplateObj = [SELECT Id
                                                ,Name
                                                , Subject
                                                , Body
                                                , HtmlValue
                                                , TemplateType
                                                , BrandTemplateId
                                            FROM 
                                                EmailTemplate 
                                            WHERE 
                                                Name = 'PDC Email Template' 
                                            LIMIT 1 ];
                                                
        // TODO Added to have attachment related to email template

        Attachment[] attachmentLst = [ SELECT Id,ContentType,Name, Body  From Attachment where parentId = : emailTemplateObj.Id];
        lstAttach.addAll(  attachmentLst );

        String toAddress = '', fromAddress,replyToAddress = '', contentType, contentValue,contentBody = '';
        String subject, strAccountId, strDPIId, bccAddress ='', strCCAddress;
        
        strAccountId = objPDC.Account__c;

        
        toAddress = objPDC.Account__r.Person_Business_Email__c;
        if( Test.isRunningTest() ) {
            toAddress = 'abc@g.com';
        }
        
        System.debug('toAddress = ' + toAddress);
        // need to verify 
        fromAddress = 'noreply@damacgroup.com';
        bccAddress = Label.Sf_Copy_Bcc_Mail_Id;
        contentType = 'text/html';
        strCCAddress = '';
        system.debug(' emailTemplateObj : '+ emailTemplateObj );
        if( String.isNotBlank( toAddress ) && emailTemplateObj != null ) {
            
            if(emailTemplateObj.body != NULL){
                contentBody =  emailTemplateObj.body;
            }
            if(emailTemplateObj.htmlValue != NULL){
                contentValue = emailTemplateObj.htmlValue;
            }
            if(string.isblank(contentValue)){
                contentValue='No HTML Body';
            }
            subject = emailTemplateObj.Subject != NULL ? emailTemplateObj.Subject : 'Reminder for cheque';
            
            contentValue = String.isNotBlank( objPDC.Cheque_Number__c ) ? contentValue.replace( '{!chqNo}', objPDC.Cheque_Number__c) : contentValue.replace( '{!chqNo}', '' );
            contentValue = String.isNotBlank( String.valueOf( objPDC.Amount__c ) ) ? contentValue.replace( '{!amount}', String.valueOf( objPDC.Amount__c ) ) : contentValue.replace( '{!amount}', '' );
            contentValue = objPDC.Booking_Unit__c != null && String.isNotBlank( objPDC.Booking_Unit__r.Unit_Name__c) ? contentValue.replace( '{!unitNo}', objPDC.Booking_Unit__r.Unit_Name__c ) : contentValue.replace( '{!unitNo}', '' );
            contentValue = String.isNotBlank( String.valueOf( objPDC.Cheque_Date__c ) ) ? contentValue.replace( '{!date}', String.valueOf(objPDC.Cheque_Date__c ) ) : contentValue.replace( '{!date}', '' );
               
            
            contentBody = String.isNotBlank( objPDC.Cheque_Number__c ) ? contentBody.replace( '{!chqNo}', objPDC.Cheque_Number__c ) : contentBody.replace( '{!chqNo}', '' );
            contentBody = String.isNotBlank( String.valueOf( objPDC.Amount__c ) ) ? contentBody.replace( '{!amount}', String.valueOf( objPDC.Amount__c ) ) : contentBody.replace( '{!amount}', '' );
            contentBody = objPDC.Booking_Unit__c != null && String.isNotBlank( objPDC.Booking_Unit__r.Unit_Name__c ) ? contentBody.replace( '{!unitNo}', objPDC.Booking_Unit__r.Unit_Name__c) : contentBody.replace( '{!unitNo}', '' );
            contentBody = String.isNotBlank( String.valueOf( objPDC.Cheque_Date__c ) ) ? contentBody.replace( '{!date}', String.valueOf( objPDC.Cheque_Date__c ) ) : contentBody.replace( '{!date}', '');
            
            // Callout to sendgrid to send an email
            SendGridEmailService.SendGridResponse objSendGridResponse =
                SendGridEmailService.sendEmailService(
                    toAddress
                    , ''
                    , strCCAddress
                    , ''
                    , bccAddress
                    , ''
                    , subject
                    , ''
                    , fromAddress
                    , ''
                    , replyToAddress
                    , ''
                    , contentType
                    , contentValue
                    , ''
                    , lstAttach
                );

                System.debug('objSendGridResponse == ' + objSendGridResponse);
                //Create Email Activity
                String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;
                if (responseStatus == 'Accepted') {
                }//End Email Activity Creation
                
        }//End Payment details null check if
    }//End emailTemplateObj IF
    
}