/**
 * Description: Class used to get penalty amount.
 */

public class GetPenaltyExtension {
    public Booking_Unit__c objBookingUnit {get; set;}
    public String strPenaltyValue {get; set;}

    public GetPenaltyExtension(ApexPages.StandardController controller) {
        objBookingUnit = (Booking_Unit__c)controller.getrecord();
    }

    /**
     * Method used to get penalty amount
     */
    public void generatePenalty() {

        // Fetch the Booking Unit details
        objBookingUnit = [SELECT Id, Registration_ID__c,Penalty_Value__c,Unit_Name__c FROM Booking_Unit__c WHERE Id =: objBookingUnit.Id];
        
        // Check if Registration Id is present or not.
        if (String.isNotBlank(objBookingUnit.Registration_ID__c)) {
            String strResponse;
            try {
                AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint objSoapEndpoint = new AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint();      
                objSoapEndpoint.timeout_x = 120000;
                strResponse = objSoapEndpoint.getPenaltyValue('2-' + String.valueOf(System.currentTimeMillis()), 'GET_PENALTY', 'SFDC', objBookingUnit.Registration_ID__c, 'Y');
                if (String.isNotBlank(strResponse)) {
                    objBookingUnit.Penalty_Value__c = Decimal.valueOf(strResponse);
                    strPenaltyValue = strResponse;
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,String.format(Label.Penalty_Accrued, new List<String>{objBookingUnit.Unit_Name__c, strResponse}));
                    ApexPages.addMessage(myMsg);
                    update objBookingUnit;
                }
            } catch(Exception objException) {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Some problem has been encountered.Please contact System Administrator for furthur details.');
                ApexPages.addMessage(myMsg);
            }
        } else {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Registration Id is not present for this Booking Unit.Please contact System Administrator for furthur details.');
                ApexPages.addMessage(myMsg);
        }
    }

    /**
     * Method used to redirect to Booking Unit detail page
     */    
    public PageReference redirectToDetailPage() {

        //Redirect to Booking Unit detail page
        PageReference  bookingUnitDetailPage = new PageReference ('/'+objBookingUnit.Id);
        return bookingUnitDetailPage;
    }
}