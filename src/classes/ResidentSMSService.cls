/**
 * @File Name          : ResidentSMSService.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 11/17/2019, 1:50:22 PM
 * @Modification Log   : 
 * Ver       Date            Author              Modification
 * 1.0    11/17/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
//******************************AKISHOR: This is send SMS to Visitors*****************************//
public without sharing class ResidentSMSService {   

    public NotifyResident.ResponseWrapper SendSMS2Resident( String AccountId
                                                          , String VM
                                                          , String Outstanding
                                                          , String VisitorType
                                                          , String BUId
                                                          , String Age
                                                          , String Designation
                                                          , String WatchList
                                                          , List<Booking_Unit__c> BUList
                                                          , String DocumentID
                                                          , String EntryType
                                                          , String Nationality
                                                          , String Gender
                                                          , String VisitorName
                                                          , String VisitorRecId
                                                          , String VisitorLocation,String VisitorCompany)
                                     {      
       System.debug('-->> 1: ');
       NotifyResident.ResponseWrapper  objWrpr = new NotifyResident.ResponseWrapper();
       String SMS ='';  String FinalMobile='00971556695784';     
       String SendSMS = 'N';String NoResident='N';      String Whitespot='N';
       if(VisitorCompany=='Whitespot' || VisitorCompany=='WHITESPOT'||VisitorCompany=='White Spot'|| VisitorCompany=='white spot'){
           Whitespot='Y';
           
       }
            System.debug('Whitespot ' + VisitorCompany+Whitespot);
       String COTP=String.valueOf(Math.random()).substring(8,12);                               
       String MsgContent=''; String OSLimit= Label.VMS_OS_Limit;  
       String ReturnMsg='';
       Id otpRecTypeId =  Schema.SObjectType.SMS_History__c.getRecordTypeInfosByName().get('OTP History').getRecordTypeId();
       //List<Booking_Unit__c> UnitName=[Select Unit_Name_c from Booking_Unit_c where Id:=BUId];

       
       List<Contractor_Information__c> lstContractor_Information = new List<Contractor_Information__c>();
        if(String.isNotBlank(DocumentID)&& Whitespot=='N'){
            System.debug('-->> 2: ');
            lstContractor_Information = [Select Id,Company_Name__c,Contractor_Mobile__c From Contractor_Information__c Where Emirates_ID__c != null AND FM_Case__c!=null
            AND Unit_Name__c=:BUList[0].Unit_Name__c AND Emirates_ID__c =: DocumentID LIMIT 1]; 
            //Company_Name__c 
        }
        
       System.debug('-->> 3: ');
      system.debug('EntryType'+EntryType+'BUId'+BUId+'lstContractor_Information'+lstContractor_Information ); 
      system.debug('Con'+lstContractor_Information);
            system.debug('Out'+Outstanding+ 'Od;imit'+OSLimit);
        //****************************For CheckOut entry***********************************//
       if(EntryType=='CheckOut' && VisitorRecId !=''){
           System.debug('-->> 4: ');
           Visitor_Details__c updChkout= new Visitor_Details__c();
           List<Visitor_Details__c> vistList= new List<Visitor_Details__c>();
           updChkout.Id=VisitorRecId;updChkout.Entry_Type__c='CheckOut';updChkout.Checkout_Time__c=System.Now();
           vistList.add(updChkout);
           update vistList;
                   objWrpr.errorMessage= 'CheckedOut';
                    objWrpr.status= 'OK';
                    objWrpr.statusCode='200';
       }
       
       else if(EntryType=='CheckIn'){//*****Check In started******************// && Whitespot=='N'
        System.debug('-->> 5: ');
               List<Account> AcntObj= new List<Account> ();
               if(String.isNotBlank(AccountId)){
                       System.debug('-->> 6: ');
                    System.debug('-->> entered: ' + AcntObj);       
                   AcntObj =  [SELECT Id
                                                  , Mobile_Phone_Encrypt__pc
                                                  , RecordType.Name
                                               FROM Account
                                              WHERE Id = :Id.valueOf(AccountId) 
                                            LIMIT 1];  
                                        
                                        
                }
                System.debug('-->> AcntObj: ' + AcntObj);                
                list<OTP__c> newotp = new List<OTP__c>();
                List<SMS_History__c> smsLst = new List<SMS_History__c>();
                if( String.isBlank( AccountId )){//check for units w/o residents
                    System.debug('-->> 7: ');
                    objWrpr.errorMessage= 'No Resident in the selected unit';
                    //NoResident='Y';//for creating visitor details even if unit is vacant
                    objWrpr.status= 'OK';
                    objWrpr.statusCode='200';
                } else{
                         System.debug('-->> 8: ');
                    if( String.isNotBlank( Outstanding ) && Decimal.ValueOf(Outstanding) > Decimal.ValueOf(OSLimit)){
                        
                        System.debug('-->> 9: ');
                        objWrpr.errorMessage= 'Not Allowed Due to pending Service Charges';
                        objWrpr.status= 'NOK';
                        objWrpr.statusCode='400';
                        objWrpr.OTP=COTP;if(VisitorType.contains('Contractor')){
                        SendSMS ='Y';}
                        else if (!VisitorType.contains('Contractor')){System.debug('-->> 10: '); SendSMS ='N';}
                         system.debug('objWrpr'+objWrpr);
                    }
                    else if( String.isBlank( Outstanding ) || Decimal.ValueOf(Outstanding) < Decimal.ValueOf(OSLimit)){ //SendSMS ='Y';
                         System.debug('-->> 11: ');
                        if( lstContractor_Information.size() > 0 && VisitorType.contains('Contractor') && lstContractor_Information.size() !=null) {                        
                            System.debug('-->> 12: ');
                            if(lstContractor_Information[0].Company_Name__c != VisitorCompany){
                                System.debug('-->> 13: ');
                            objWrpr.errorMessage= 'Company Not Matching'; objWrpr.status= 'NOK'; objWrpr.statusCode='400';}
                            else if(lstContractor_Information[0].Company_Name__c == VisitorCompany)
                            {System.debug('-->> 14: '); SendSMS ='Y';objWrpr.errorMessage= 'Contractor Details Found'; objWrpr.status= 'OK'; objWrpr.OTP=COTP;  objWrpr.statusCode='200';}                       
                                                   
                            //}else {
                              //  objWrpr.OTP='NA';
                            //}                      
                        } else if (VisitorType.contains('Contractor') && lstContractor_Information.size() <= 0 && lstContractor_Information.isEmpty()){
                            System.debug('-->> 15: ');
                            SendSMS ='N';
                            objWrpr.errorMessage= 'Contractor Details Not Found';
                            objWrpr.status= 'NOK';
                            objWrpr.OTP='NA';
                            objWrpr.statusCode='400';
                        }
                         else if (!VisitorType.contains('Contractor')){
                            System.debug('-->> 16: ');
                            SendSMS ='Y';
                            objWrpr.status= 'OK';
                            objWrpr.OTP='NA';
                            objWrpr.statusCode='200';objWrpr.errorMessage='Successfull';
                        }
                         system.debug('objWrpr1111'+objWrpr+ 'Od;imit'+OSLimit);
                  }//end of else of resident check
                }
        //*******Initaite SMS for visitor s with or without OTP*********//      
        
    //system.debug('Vistotro mob'+VM +VisitorType+AcntObj[0].Mobile_Phone_Encrypt__pc  );    
           
        if(VM !='' && VM !=null && VisitorType.contains('Contractor') && lstContractor_Information.size() > 0 && Whitespot=='N') //sending OTP here
        {
            System.debug('-->> 17: ');
            SMS = 'Dear Visitor, Please share below OTP at reception for access\n';
            SMS = SMS +'OTP :-'+ COTP;
            FinalMobile=VM;
            
        } 
        else if (AcntObj.size() > 0 && AcntObj != null && AcntObj[0].Mobile_Phone_Encrypt__pc !='' && AcntObj[0].Mobile_Phone_Encrypt__pc !=null 
              && !VisitorType.contains('Contractor') && SendSMS =='Y' && !BUList[0].Bedroom_Type__c.equalsIgnoreCase('office') 
              && !BUList[0].Bedroom_Type__c.equalsIgnoreCase('Retail')){
                  System.debug('-->> 18: ');
            SMS = 'Dear Resident, Your below visitor has been allowed through security.\n';
            SMS = SMS + 'Visitor Name: '+ VisitorName;
            SMS = SMS + '(' + VisitorType +')'+'\n';
            if(VisitorCompany !=''){
                System.debug('-->> 19: ');
            SMS = SMS + VisitorCompany;}
            FinalMobile=AcntObj[0].Mobile_Phone_Encrypt__pc;
        }//AKISHOR : 05Nov2019
         // system.debug('new SMS'+AcntObj+AcntObj[0].Mobile_Phone_Encrypt__pc+VisitorType+BUList[0].Bedroom_Type__c );
       if (AcntObj.size() > 0 && AcntObj != null && AcntObj[0].Mobile_Phone_Encrypt__pc !='' && AcntObj[0].Mobile_Phone_Encrypt__pc !=null 
              && (VisitorType.equalsIgnoreCase('MEP Contractor') || VisitorType.equalsIgnoreCase('Painting Contractor')) && SendSMS =='Y' &&
                !BUList[0].Bedroom_Type__c.equalsIgnoreCase('Retail') && !BUList[0].Bedroom_Type__c.equalsIgnoreCase('office')){
           System.debug('-->> 20: ');
            String SMSP = 'Dear Resident, We are glad to announce that APEX maintenance team in\n'; 
            SMSP=SMSP+ 'your facilities are at your service for FREE. Please contact our helpdesk to schedule your appointment 800APEX.\n';
            SMSP=SMSP+ 'Below visitor was allowed to your unit.\n';
            SMSP = SMSP + 'Visitor Name: '+ VisitorName;
            SMSP = SMSP + '(' + VisitorType +')'+'\n';
            if(VisitorCompany !=''){
                System.debug('-->> 21: ');
            SMSP = SMSP + 'Company: '+VisitorCompany;}
            System.debug('-->> 22: ');
            FinalMobile=AcntObj[0].Mobile_Phone_Encrypt__pc;
                    HttpRequest req = new HttpRequest();
                    HttpResponse res = new HttpResponse();
                    Http http = new Http();
                    String user = ''; String passwd = '';String strSID = '';                   
               
                    user = Label.FM_SMS_Service_Username; //'loams.';
                    passwd =  Label.FM_SMS_Service_Password; //'1@#$%qwert';
                    //strSID =  Label.FM_SMS_Service_SenderId; //'LOAMS';
                    strSID = SMSClass.getSenderName(user, FinalMobile, false);
                    req.setMethod('POST' ); // Method Type
                    req.setEndpoint('http://api.smscountry.com/SMSCwebservice_bulk.aspx'); //SMS WebService
                    //string msgCont = GenericUtility.encodeChar(MsgContent);                                        
                    req.setBody('user='+ user + '&passwd=' + passwd +'&message=' + SMSP + '&mobilenumber=' + FinalMobile + '&sid='+strSID+ '&MTYPE=LNG&DR=Y');                   
                    res = http.send(req);
        }//end MEP Contractor
        
      system.debug('SendSMS'+SendSMS + FinalMobile);      
        if(SendSMS=='Y' && FinalMobile!= null && FinalMobile.startsWith('00') && AcntObj.size() > 0 && AcntObj != null) {
                    System.debug('-->> 23: ');
                    //AcntObj[0].Mobile_Phone_Encrypt__pc!                         
                    HttpRequest req = new HttpRequest();
                    HttpResponse res = new HttpResponse();
                    Http http = new Http();
                    String user = ''; String passwd = '';String strSID = '';                    
               
                    user = Label.FM_SMS_Service_Username; //'loams.';
                    passwd =  Label.FM_SMS_Service_Password; //'1@#$%qwert';
                    strSID =  Label.FM_SMS_Service_SenderId; //'LOAMS';
                    req.setMethod('POST' ); // Method Type
                    req.setEndpoint('http://api.smscountry.com/SMSCwebservice_bulk.aspx'); //SMS WebService
                    //string msgCont = GenericUtility.encodeChar(MsgContent);
                                        
                    req.setBody('user='+ user + '&passwd=' + passwd +'&message=' + SMS + '&mobilenumber=' + FinalMobile + '&sid='+strSID+ '&MTYPE=LNG&DR=Y');                   
                    res = http.send(req);
                    
                    if( res.getBody() != null ) {
                        System.debug('-->> 24: ');
                        // Parse Response
                        SMS_History__c smsObj = new SMS_History__c();
                        smsObj.Phone_Number__c = VM;
                        smsObj.Customer__c = AcntObj[0].Id;                    
                        if(String.valueOf(res.getBody()).contains('OK:')){
                            System.debug('-->> 25: ');
                            system.debug('Message Send Sucessfully');
                            smsObj.Is_SMS_Sent__c = true;
                            smsObj.sms_Id__c = res.getBody().substringAfter('OK:');
                            //Create OTP for Contractor and link with Unit along + also check EID details
                            if(VisitorType.contains('Contractor') && lstContractor_Information.size() > 0  ){  
                                
                                System.debug('-->> 26: ');
                                OTP__c otp = new OTP__c();
                                otp.OTP_Number__c = COTP;
                                otp.Unit__c=BUId;
                                otp.Phone_Number__c=VM;
                                newotp.add(otp);
                            } 
                        }
                        else {
                            System.debug('-->> 27: ');
                            smsObj.Description__c = res.getBody();
                        }

                        if(!newotp.isEmpty()){
                            System.debug('-->> 28: ');
                            try{
                                System.debug('-->> 29: ');
                                upsert newotp Phone_Number__c;
                                smsObj.OTP__c = newotp[0].Id;
                                smsLst.add(smsObj);
                            }catch(exception ex){
                                System.debug('DML Exception*****: '+ ex);
                            }
                        }
                        System.debug('SMS List--'+ smsLst); 
                        if(smsLst.size() > 0 ) {
                            System.debug('-->> 30: ');
                            insert smsLst;
                        }
                      }
                System.debug('-->> 31: ');
                                        
                system.debug(' res.getBody() : ' + res.getBody());
       }
       System.debug('-->> 32: ');
            //Capture Visitor Details
            List<Visitor_Details__c> Vistor = new List<Visitor_Details__c>();
            String strEID = String.isNotBlank( DocumentID ) ? DocumentID : '212321';
            
            List<Visitor_Details__c> lstGetVisitor = [SELECT Id
                                                        FROM Visitor_Details__c
                                                       WHERE CreatedDate = TODAY
                                                         AND EID__c =: strEID
                                                         AND Checkout_Time__c = null AND UnitName1__c=:BUList[0].Unit_Name__c
                                                       LIMIT 1];
            if( lstGetVisitor != null  && lstGetVisitor.size() > 0 ) {
                objWrpr.RecordId=lstGetVisitor[0].Id;
                if( !VisitorType.equalsIgnoreCase('MEP Contractor') 
                 && !VisitorType.equalsIgnoreCase('Painting Contractor') 
                 && VisitorType.contains('Contractor') ) {
                    objWrpr.OTP =  String.isNotBlank( COTP ) ? COTP : '';  
                }
            } else {
                Visitor_Details__c vstor = new Visitor_Details__c();
                vstor.Age__c  = Age;
                vstor.UnitName__c=BUList[0].Id;
                vstor.SMS_Body__c=String.isNotBlank( SMS ) ? SMS : '';
                vstor.OTP__c = String.isNotBlank( COTP ) ? COTP : '';               
                vstor.Designation__c=Designation; 
                vstor.EID__c=DocumentID;
                vstor.Entry_Type__c=EntryType;
                vstor.Gender__c=Gender;
                vstor.Nationality__c=Nationality;
                vstor.Visitor_Name__c=VisitorName;
                vstor.Watch_List__c=WatchList;            
                vstor.Building__c=BUList[0].Inventory__r.Building_Location__c;//BUList[0].Inventory__r.Unit_Location__r.Id; //---locha
                if(BUList[0].Resident__c !=null){
                    System.debug('-->> 33: ');
                vstor.Resident__c=BUList[0].Resident__c;}
                vstor.CheckIn_Date__c=System.Now();
               
                vstor.Location__c = VisitorLocation;
                if(lstContractor_Information.size() > 0){
                    System.debug('-->> 34: ');
                vstor.Contractor_Mobile_WP__c = lstContractor_Information[0].Contractor_Mobile__c;}
                 system.debug(' Response' +objWrpr.errorMessage);
                vstor.Response__c = objWrpr.errorMessage;
                vstor.Visitor_Company__c = VisitorCompany;
                vstor.Visitor_Type__c = VisitorType;
                vstor.Visitor_Mobile__c=VM;
                Vistor.add(vstor);
                
                if(Vistor.size()> 0){System.debug('-->> 35: '); insert Vistor; objWrpr.RecordId=Vistor[0].Id; }
                if( Whitespot=='Y' ) {
                    objWrpr.RecordId = Vistor[0].Id;
                    objWrpr.status= 'OK';
                    objWrpr.OTP='0000';
                    objWrpr.statusCode='200';
                    objWrpr.errorMessage='Contractor Found';
                    
                }
            }
            //end vistor capturing      
       }
       System.debug('-->> 36: ');
      return objWrpr;
    }
    
}