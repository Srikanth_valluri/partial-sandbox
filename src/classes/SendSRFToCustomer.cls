/**
 * v1.1     14/10/2020  Ravi                Added the logic to close the SignNowStatus to closed
 * V1.2     28-10-2020  Aishwarya Todkar    added isEmpty check for callingList  
*/
public without sharing class SendSRFToCustomer {
    public String onsiteUrl = '';
    public Static String snagEmailTemplate = 'SNAG_Received_Template';
    public Static EmailTemplate SNAGReceivedTemplate;
    public Static List<OrgWideEmailAddress> owea;
    public Static List<OrgWideEmailAddress> owea_damac;
    public Static List<Messaging.SingleEmailMessage> messages =  new List<Messaging.SingleEmailMessage>();

    public static String signNowAccessToken;
    public static final String DOC_NAME = 'Snag Rectification Form';
    public static String unitName = '';
    public static Integer x;
    public static Integer y;
    public static List<EmailMessage> lstEmails = new List<EmailMessage>();
    public static String custName='';
    
    public static void fetchSRFDocfromOnsiteAPI(List<Booking_Unit__c> buList, String snagStat){

        if(buList!=null && !buList.isEmpty()){
            String onsiteUrl = Label.Snag_Onsite_Base_URL+'/'+buList[0].SnagValue__c+'/api/OnsiteApi/GetSRFPDFByRef?ExtRef='
                            + buList[0].Unit_Name__c+'&key='+Label.Snag_Onsite_URL_Key;
            List<Case> caseList = [SELECT Id from Case where Booking_Unit__c =:buList[0].Id AND RecordType.DeveloperName IN : SnagStatus.lstHandoverCaseRecTypes];
            unitName = buList[0].Unit_Name__c;
            custName = buList[0].Booking__r.Account__r.Name;            
            Attachment att;
            Boolean isReadyforHandover = false;
            system.debug('onsiteUrl>>>'+onsiteUrl);
            HttpRequest request = new HttpRequest();
            request.setEndpoint(onsiteUrl);
            request.setMethod('GET');
            request.setHeader('ACCEPT','application/json');
            request.setHeader('Content-Type', 'application/json');
            request.setTimeout(120000);
                            
            Http http = new Http();
            HTTPResponse response;
            response = http.send(request);

            system.debug('!!!!!!!!!!res'+response.getStatusCode());
            system.debug('!!!!!!!!!!res'+response.getBody());
            
            if(response.getStatusCode() == 200 && String.isNotBlank( response.getBody())) {
                Blob blobVal = response.getBodyAsBlob();
                
                if(snagStat == 'Ready for Handover' || snagStat=='Handed Over')
                {
                        System.debug('Inside SendSRF ready for handover');
                    att = new Attachment ( Name = 'Snag Rectification Form.pdf', Body = blobVal );
                    x = 250;
                    y = 1480;
                    isReadyforHandover = true;
                    SendSnagEmail(buList, att, 'SNAG_Completion_Template');
                    /*if(!caseList.isEmpty() && snagStat=='Handed Over'){//v1.1
                        //Update the status of Open SignNowStatus record under the Booking Unit to Closed if Status is Handed Over
                        updateOpenSingNowStatus(caseList[0]);
                    }*/
                }else if(snagStat == 'Open' 
                            || snagStat=='Submitted' 
                            || snagStat=='Revised'
                            || snagStat=='RESUBMIT'){
                    System.debug('Inside SendSRF revised open submitted');
                    att = new Attachment ( Name = 'Snag Rectification Form.pdf', Body = blobVal );
                    x = 250;
                    y = 1150; 
                    isReadyforHandover = false;
                    SendSnagEmail(buList, att, 'SNAG_Received_Template');
                    /*if(!caseList.isEmpty()){
                        //Update the status of Open SignNowStatus record under the Booking Unit to Closed
                        updateOpenSingNowStatus(caseList[0]);
                    }*/
                }
                
                List<String> listToAddresses = new List<String>();
                
                //Below logic to send the SRF document using Signnow
                List<Calling_List__c> callingList = new List<Calling_List__c>();
                callingList = [SELECT Id, Virtual_Viewing__c 
                                FROM Calling_List__c
                                WHERE RecordType.Name = 'Appointment Scheduling'
                                AND Virtual_Viewing__c = true
                                AND Booking_Unit__c = :buList[0].Id];

                System.debug('Virtual Viewing Flag:::' +callingList);                                             

                if( !callingList.isEmpty() && callingList[0].Virtual_Viewing__c == true){
                    SignNowWebService signowObj = new SignNowWebService();
                    SignNowWebService.JsonToApexWrapper objResponse = new SignNowWebService.JsonToApexWrapper();
                    Blob sentDocBody;

                    listToAddresses.add(buList[0].Booking__r.Account__r.Email__pc);
                    signNowAccessToken = signowObj.fetchAccessToken();
                    String docId = signowObj.uploadDocumentToSignNow(att, signNowAccessToken);
                    addSignaturePlaceholder(docId, x, y);
                    String strRequestBody = formBodyForInvite(listToAddresses, docId, isReadyforHandover);
                    objResponse = signowObj.sendSignatureInvite(docId, strRequestBody, signNowAccessToken);
                    //sentDocBody = signowObj.downloadSentDocument(docId, signNowAccessToken);
                    signowObj.subscribeEvent(docId, signNowAccessToken);
                    System.debug('objResponse.status-->' +objResponse.status);
                    if(!caseList.isEmpty() && snagStat!='Ready for Handover'){//v1.1
                        //Update the status of Open SignNowStatus record under the Booking Unit to Closed if Status is Handed Over
                        updateOpenSingNowStatus(caseList[0]);
                    }
                    
                    if(objResponse.status == 'success'){
                        System.debug('Inside success response');
                        try{
                            if(SnagStatus.bUUpdateflag == true) {
                             if(SnagStatus.bookingUnitList != Null && SnagStatus.bookingUnitList.size() > 0) {
                              update SnagStatus.bookingUnitList;
                              SnagStatus.bUUpdateflag = false;
                             }
                            }
                            cuda_signnow__SignNowStatus__c objStatus = new cuda_signnow__SignNowStatus__c();
                            objStatus.cuda_signnow__RelatedToCase__c = caseList[0].Id;
                            objStatus.cuda_signnow__Document_Id__c = docId;
                            objStatus.cuda_signnow__SN_Documet_Id__c = docId;
                            objStatus.cuda_signnow__Record_Id__c = caseList[0].Id;
                            objStatus.cuda_signnow__Record_Type__c = 'Case';
                            //objStatus.cuda_signnow__Sent_To__c = strInviteSentTo;
                            objStatus.cuda_signnow__Status__c = 'Pending';
                            objStatus.Name = DOC_NAME;
                            insert objStatus;
                            
                            Attachment attObj = new Attachment();
                            attObj.contentType = 'application/pdf';
                            attObj.name = DOC_NAME+'.pdf';
                            attObj.parentId = caseList[0].Id;
                            attObj.body = blobVal;
                            insert attObj;
                        }catch(Exception exp){
                            System.debug(exp.getMessage());    
                        }
                    }else {
                        //System.debug(exp.getMessage());    
                    }
                }
                if( lstEmails != null && lstEmails.size() > 0 ) {
                    insert lstEmails;
                }
            }
            //return att;
        }
        //return null;
    }
   
    public static String addSignaturePlaceholder(String docId, Integer x, Integer y){

        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeFieldName('fields');
        gen.writeStartArray();
        gen.writeStartObject();
        gen.writeNumberField('page_number', 0);
        gen.writeStringField('type', 'signature');
        //gen.writeStringField('name', '');
        gen.writeStringField('role', 'Account Name');
        gen.writeBooleanField('required', true);
        gen.writeNumberField('height', 60);
        gen.writeNumberField('width', 300);
        gen.writeNumberField('x', x);
        gen.writeNumberField('y', y);
        gen.writeEndObject();
        gen.writeEndArray();
        gen.writeEndObject();
        system.debug(gen.getAsString());

        String headerValue = 'Bearer ' + signNowAccessToken;
        String boundary = '------------------------700252463407842306668015';

        HttpRequest req = new HttpRequest( );
        req.setMethod('PUT');
        req.setBody(gen.getAsString());
        req.setHeader('Content-Type','json');
        req.setHeader( 'Authorization',headerValue );
        req.setEndpoint( 'https://api.signnow.com/document/' +docId );
        req.setTimeout(120000);
        System.debug('Req-->' +req);
        HTTPResponse response = new Http().send(req ); 
        //System.debug('---->Response'+response.getBody());
        JsonToApexWrapper objResponse = new JsonToApexWrapper();
        objResponse = (JsonToApexWrapper)System.JSON.deserialize(response.getBody(), JsonToApexWrapper.class);
        //System.debug( 'objResponse document id- -  ' + objResponse.id );
        return objResponse.id;
    }

    public static String formBodyForInvite(List<String> listToAddresses, String docId, Boolean isReadyForHandover){
        Integer intJb = 1;
        Integer intOrder = 1;
        String strRolejson = '{'+'"document_id": "'+docId+'",'+
                                        '"to": [';
        String strPbEmail = '';
        String strInviteSentTo = '';
        
        for(String toAddress : listToAddresses){
                strRolejson += '{"order":1, "role":"Account Name", "email":"'+toAddress+'","expiration_days":30}';
                //strInviteSentTo = '1)'+toAddress+'\n';
        }
        String EMAIL_SUBJECT = '';
        if(isReadyForHandover){
            EMAIL_SUBJECT = 'SNAGs Completed for Unit ' +unitName;
        }else{
            EMAIL_SUBJECT = 'SNAG Received for Unit ' +unitName;
        }
        String body = '';
        strRolejson += '],'+
                    '"from": "virtual.handover@damacproperties.com",'+
                    '"cc": ['+
                    '"virtual.handover@damacproperties.com"'+
                    '  ],'+
                    '"subject": "'+EMAIL_SUBJECT+'",'+
                    '"message": "'+body+'"'+
                    '}';

        return strRolejson;    
    }

    public static void SendSnagEmail(List<Booking_Unit__c> SngBUList, Attachment att, String strSnagEmailTemplate){//to send emails
        //List<Messaging.SingleEmailMessage> messages =   new List<Messaging.SingleEmailMessage>();
        List<OrgWideEmailAddress> owea;EmailTemplate SNAGCompleteTemplate;//String strSnagEmailTemplate = 'SNAG_Completion_Template';
        List<OrgWideEmailAddress> owea_damac;
        owea = new List<OrgWideEmailAddress>();
        owea_damac = new List<OrgWideEmailAddress>();
        SNAGCompleteTemplate = [SELECT ID, Subject, Body, HtmlValue FROM EmailTemplate 
                                           WHERE DeveloperName =: strSnagEmailTemplate limit 1];
        //Get Org wide email  address details
              /*     owea = [select Id from OrgWideEmailAddress 
                           where Address = 'atyourservice@damacproperties.com'];
                   
                   owea_damac = [select Id, Address from 
                                   OrgWideEmailAddress where DisplayName = 'DAMAC'];*/
        system.debug('SngBUList[0].Booking__r.Account__r.Email__pc  == ' + SngBUList[0].Booking__r.Account__r.Email__pc );
        if(SngBUList[0].Booking__r.Account__r.Email__pc != null) {
                    
            String toAddress = '', fromAddress,replyToAddress = '', contentType, contentValue,contentBody = '';
            String subject, strAccountId, strDPIId, bccAddress ='', strCCAddress ='';
                    // Callout to sendgrid to send an email
            strAccountId = SngBUList[0].Booking__r.Account__c;
            toAddress = SngBUList[0].Booking__r.Account__r.Email__pc;
            bccAddress = Label.Sf_Copy_Bcc_Mail_Id;
            fromAddress = Label.AT_Your_Service;

            subject =  SNAGCompleteTemplate.Subject.replace('{!Case.Unit_Name__c}', SngBUList[0].Unit_Name__c);
            contentType = 'text/html';
            contentValue = contentValue = SNAGCompleteTemplate.htmlValue;
            
            if(SNAGCompleteTemplate.body != NULL){
                contentBody =  SNAGCompleteTemplate.body;
                //Todo added to have cx name
                contentBody = contentBody.replace('{!Case.Account}', SngBUList[0].Booking__r.Account__r.Name);
                contentBody = contentBody.replace('{!Case.Unit_Name__c}', SngBUList[0].Unit_Name__c);
            }
            if(string.isblank(contentValue)){ contentValue='No HTML Body';    }
            //Todo added to have cx name 
            contentValue = contentValue.replace('{!Case.Account}', SngBUList[0].Booking__r.Account__r.Name);
            contentValue = contentValue.replace('{!Case.Unit_Name__c}', SngBUList[0].Unit_Name__c);

            List<Attachment> lstAttach = new List<Attachment>();
            lstAttach.add(att);

            SendGridEmailService.SendGridResponse objSendGridResponse =
                SendGridEmailService.sendEmailService(
                    toAddress
                    , ''
                    , strCCAddress
                    , ''
                    , bccAddress
                    , ''
                    , subject
                    , ''
                    , fromAddress
                    , ''
                    , replyToAddress
                    , ''
                    , contentType
                    , contentValue
                    , ''
                    , lstAttach
                );

            System.debug('SendGrid response:::' +objSendGridResponse.ResponseStatus);
            String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;
            if (responseStatus == 'Accepted') {
                EmailMessage mail = new EmailMessage();
                mail.Subject = subject;
                mail.MessageDate = System.Today();
                mail.Status = '3';//'Sent';
                mail.RelatedToId = strAccountId;
                mail.Account__c  = strAccountId;
                mail.Type__c = 'CaptureSnagStatus';
                mail.ToAddress = toAddress;
                mail.FromAddress = fromAddress;
                mail.TextBody = contentBody; //contentValue.replaceAll('\\<.*?\\>', '');
                mail.Sent_By_Sendgrid__c = true;
                mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                mail.CcAddress = strCCAddress;
                mail.BccAddress = bccAddress;
                lstEmails.add(mail);
                system.debug('Mail obj == ' + mail);
            }//End Email Activity Creation
            /**  Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                 String mailMessage;
                //message.setOrgWideEmailAddressId(owea.get(0).Id);
                mailMessage = SNAGCompleteTemplate.Body;
                mailMessage = mailMessage.replace('{!Case.Account}', SngBUList[0].Booking__r.Account__r.Name);
                mailMessage = mailMessage.replace('{!Case.Unit_Name__c}', SngBUList[0].Unit_Name__c);              
                message.subject = SNAGCompleteTemplate.Subject.replace('{!Case.Unit_Name__c}', SngBUList[0].Unit_Name__c);
                message.setPlainTextBody(mailMessage);
                message.setSaveAsActivity(true);
                message.setWhatId(SngBUList[0].Booking__r.Account__c);
                List<String> listToAddresses = new List<String>();
                listToAddresses.add(SngBUList[0].Booking__r.Account__r.Email__pc);
                //if(owea_damac != null) {
                //  listToAddresses.add(owea_damac[0].Address);
                // }
                message.toAddresses = listToAddresses;
                messages.add(message);
            */  
        }
        /**if (messages != null && messages.size() >0) {
             Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);//Sending emails
        } */
    }
    //v1.1
    public static string updateOpenSingNowStatus(Case objCase){
        cuda_signnow__SignNowStatus__c objStatus;// = new cuda_signnow__SignNowStatus__c();
        list<string> lstOpenStatus = new list<string>{
            'Pending'
        };
        for(cuda_signnow__SignNowStatus__c objSS : [select Id, cuda_signnow__Status__c, cuda_signnow__RelatedToCase__c
                                from cuda_signnow__SignNowStatus__c
                                where cuda_signnow__RelatedToCase__c =: objCase.Id AND cuda_signnow__Status__c IN : lstOpenStatus])
        {
            objStatus = objSS;
        }
        if(objStatus != null){
            objStatus.cuda_signnow__Status__c = 'Closed';
            update objStatus;
        }
        return null;
    }

    public class JsonToApexWrapper{
        public string expires_in;
        public string token_type;
        public string access_token;
        public string refresh_token;
        public string scope;
        public string last_login;
        public string id;
        public string status;
    }

    @InvocableMethod
    public static void invokeSnagInfo(List<Id> lstIds){
        System.debug('Inside invokeSnagInfo-->' +lstIds);
        sendSRFDocumentToCustomer(lstIds);        
    }

    /*********************************************************************************
     * Method Name : getSnagInfo
     * Description : Get the snag information from snagger server
     * Return Type : void
     * Parameter(s): List of BUs
    **********************************************************************************/
    @future(callout=true)
    public static void sendSRFDocumentToCustomer(List<Id> lstIds){
        List<Booking_Unit__c> lstBUs = new List<Booking_Unit__c>();
        owea = new List<OrgWideEmailAddress>();
        owea_damac = new List<OrgWideEmailAddress>();
        lstBUs = [select Id
                    , Booking__r.Account__c
                    , Booking__r.Account__r.Mobile_Phone_Encrypt__pc
                    , Booking__r.Account__r.Name
                    , Booking__r.Account__r.Email__pc
                    , Property_Name_Inventory__c
                    , SNAGs_Completed__c
                    , Unit_Name__c
                    , inventory__r.Building_Location__r.Is_New_Snagger__c  
                    , SnagValue__c
            from Booking_Unit__c 
            where Id In : lstIds AND Unit_Name__c != NULL];
        System.debug('lstBUs-->' +lstBUs);    
        //listCaseTobeUpdate = new List<Case>();
        System.debug('getSnagInfo lstBUs in SendSRF: ' + lstBUs);
        if(!lstBUs.isEmpty() && lstBUs.size() > 0) {
            //lstSnags = new list<SNAGs__c>();    
            
            String onsiteUrl = Label.Snag_Onsite_Base_URL+'/' + lstBUs[0].SnagValue__c 
                            +'/api/OnsiteApi/GetSRFPDFByRef?ExtRef='+lstBUs[0].Unit_Name__c
                            +'&key='+Label.Snag_Onsite_URL_Key;
            System.debug('onsiteURL-->' +onsiteUrl);
            List<Attachment> lstAttachments = new List<Attachment>();
            SNAGReceivedTemplate = [SELECT 
                                        ID, Subject, Body, HtmlValue 
                                    FROM 
                                        EmailTemplate 
                                    WHERE 
                                        DeveloperName =: snagEmailTemplate limit 1];
            //Get Org wide email  address details
            owea = [select 
                        Id 
                    from 
                        OrgWideEmailAddress 
                    where Address = 'atyourservice@damacproperties.com'];
            
            owea_damac = [select 
                            Id, Address
                        from 
                            OrgWideEmailAddress 
                        where DisplayName = 'DAMAC'];

                //Send Api Request
                if(String.isNotBlank(onsiteUrl)) {
                    Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                    String mailMessage;
                    message.setOrgWideEmailAddressId(owea.get(0).Id);
                    mailMessage = SNAGReceivedTemplate.Body;
                    mailMessage = mailMessage.replace('{!Booking_Unit__c.Unit_Name__c}', lstBUs[0].Unit_Name__c);              
                    message.subject = SNAGReceivedTemplate.Subject.replace('{!Booking_Unit__c.Unit_Name__c}', lstBUs[0].Unit_Name__c);
                    message.setPlainTextBody(mailMessage);
                    message.setSaveAsActivity(true);
                    message.setWhatId(lstBUs[0].Booking__r.Account__c);
                    List<String> listToAddresses = new List<String>();
                    listToAddresses.add(lstBUs[0].Booking__r.Account__r.Email__pc);
                    // if(owea_damac != null) {
                    //   listToAddresses.add(owea_damac[0].Address);
                    // }
                    //listToAddresses.add('kanujulka@gmail.com');
                    listToAddresses.add('rohit.jahagirdar@eternussolutions.com');
                    listToAddresses.add('rohitjahagirdar911@gmail.com');
                    message.toAddresses = listToAddresses;
                    

                        HttpRequest request = new HttpRequest();
                        request.setEndpoint(onsiteUrl);
                        request.setMethod('GET');
                        request.setHeader('ACCEPT','application/json');
                        request.setHeader('Content-Type', 'application/json');
                        request.setTimeout(120000);
                        
                        Http http = new Http();
                        HTTPResponse response;
                        response = http.send(request);
                    
                        system.debug('!!!!!!!!!!res'+response.getStatusCode());
                        system.debug('!!!!!!!!!!res'+response.getBody());
                        
                        if(response.getStatusCode() == 200 && String.isNotBlank( response.getBody())) {
                            Blob blobVal = response.getBodyAsBlob();//Blob.valueOf( res.getBody() ) ;
                            //system.debug('!!!!!!!!!!blobVal ::  '+blobVal);
                            List<Messaging.EmailFileAttachment> attachments = new List<Messaging.EmailFileAttachment>();
                            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                            efa.setFileName(lstBUs[0].Unit_Name__c + '.pdf');
                            efa.setBody(blobVal);
                            efa.setContentType('application/pdf');
                            attachments.add(efa);
                            message.setFileAttachments(attachments);
                        }//End Response if
                       
                    messages.add(message);    
                    System.debug('Messages-->' +messages);
                    
                }//End onsite EndPoint url if
                System.debug('Attachment === ' + lstAttachments);
                
                if (messages != null && messages.size() >0) {
                    System.debug('Inside messages not null');
                    Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);//Sending emails
                    System.debug('Mail Send Result-->' +results);
                }

        }// End lstBUs if 
    } // end of getSnagInfo


}