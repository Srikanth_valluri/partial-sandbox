/*-------------------------------------------------------------------------------------------------
Description: Controller for ViewSalesTours Page

    ============================================================================================================================
        Version | Date(DD-MM-YYYY) | Last Modified By | Comments
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 11-03-2018       | Lochana Rajput   | 1. Initial draft
    ----------------------------------------------------------------------------------------------------------------------------
        2.0     | 12-03-2018       | Lochana Rajput   | 1. Added pagination logic
    ----------------------------------------------------------------------------------------------------------------------------
        3.0     | 14-03-2018       | Monali Nagpure   | 1. Added search functionality
   =============================================================================================================================
*/

public without sharing class ViewSalesToursController {
    // public list<Sales_Tours__c>  lstSalesTours{get;set;}
    @testVisible
    Integer PAGE_SIZE = 10;
    public String searchText{get;set;}
    List<String> profNames;
    User userObj;
    //Constructor to initiaze
    public ViewSalesToursController() {
        profNames = new List<String>();
        // Get the list of Promoter Profile Names
        profNames = Label.PromoterCommunityProfile.split(',');
        userObj = new User();
        userObj = [ SELECT Id
                                  , Profile.Name
                                  , Name
                                  , ContactId
                               FROM User
                              WHERE Id = :UserInfo.getUserId()
        ];
        // lstSalesTours = new list<Sales_Tours__c>();
        /*lstSalesTours = [SELECT
                            Name,
                            Inquiry__c,
                            Inquiry__r.Name,
                            Tour_Outcome__c,
                            Check_In_Date__c,
                            Check_Out_Date__c
                        FROM Sales_Tours__c
                        WHERE Inquiry__r.CreatedBy.Profile.Name =: Label.PromoterCommunityProfile];*/
    }

    public Pagereference search() {
        /**************Added By Monali : 14/03/2018 : START **********************/
        System.debug('===========searchText : ' + searchText);
        List<Sales_Tours__c> searchSalesTourList =new List<Sales_Tours__c>();
        String finalSearchValue = '%' + searchText + '%';
        if(profNames != null
                    && !profNames.isEmpty()
                    && profNames.contains(userObj.Profile.Name)) {
        searchSalesTourList =[SELECT Name,
                                             Inquiry__c,
                                             Inquiry__r.Name,
                                             Tour_Outcome__c,
                                             Comments__c,
                                             Inquiry__r.Last_Name__c,
                                             Inquiry__r.First_Name__c,
                                             Inquiry__r.Inquiry_Status__c,
                                             Check_In_Date__c,
                                             Check_Out_Date__c,
                                             Inquiry__r.Owner.Name,
                                             Inquiry__r.Promoter_Name__c
                                        FROM Sales_Tours__c
                                       WHERE Inquiry__r.CreatedBy.Profile.Name = :userObj.Profile.Name
                                         AND ( Tour_Outcome__c LIKE :finalSearchValue
                                               OR Inquiry__r.First_Name__c LIKE :finalSearchValue
                                               OR Inquiry__r.Last_Name__c LIKE :finalSearchValue
                                               OR Inquiry__r.Name LIKE :finalSearchValue
                                             )];
        }
        System.debug('=====search======searchSalesTourList : ' + searchSalesTourList);
        setCon = new ApexPages.StandardSetController(searchSalesTourList);
        setCon.setPageSize(PAGE_SIZE);

        /**************Added By Monali : 14/03/2018 : END **********************/
        return null;
    }

    public ApexPages.StandardSetController setCon {
        get{
            if(setCon == null){
                // size = 10;
                // Get the list of Promoter Profile Names
                List<String> profileNames = Label.PromoterCommunityProfile.split(',');
                User currentUser = [ SELECT Id
                                          , Profile.Name
                                          , Name
                                          , ContactId
                                       FROM User
                                      WHERE Id = :UserInfo.getUserId()
                ];
                // Populate Promoter Name if User Profiel is mentioned in the Promoter Label
                if(profileNames != null
                    && !profileNames.isEmpty()
                    && profileNames.contains(currentUser.Profile.Name)) {
                    string queryString = 'SELECT Name,Inquiry__c,Inquiry__r.Name,Tour_Outcome__c,Comments__c,';
                    queryString+= 'Inquiry__r.Last_Name__c,Inquiry__r.First_Name__c, Inquiry__r.Inquiry_Status__c,';
                    queryString+= 'Check_In_Date__c,Check_Out_Date__c, Inquiry__r.Owner.Name,Inquiry__r.Promoter_Name__c FROM Sales_Tours__c ';
                    queryString+= ' WHERE Inquiry__r.CreatedBy.Profile.Name = \''+ currentUser.Profile.Name + '\'';
                    setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
                    setCon.setPageSize(PAGE_SIZE);
                    // noOfRecords = setCon.getResultSize();
                }
            }
            return setCon;
        }set;
    }

    public List<Sales_Tours__c> getlstSalesTours() {
        list<Sales_Tours__c> lstSalesTours = new list<Sales_Tours__c>();
        if(setCon != NULL) {
            System.debug('===='+setCon.getResultSize());
            for(Sales_Tours__c a : (List<Sales_Tours__c>)setCon.getRecords())
                lstSalesTours.add(a);
        }

        return lstSalesTours;
    }

}