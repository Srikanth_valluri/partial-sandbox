/*
    Class name      : Damac_RejectDealController
    Written By      : Srikanth V
    Test class Name : Damac_RejectDealControllerTest
    Functionality   : Apex class to get the SR rejection status,
                    comments from user and updating the step status,
                      internal, external statuses to Rejected, calling IPMS service
*/

public class Damac_RejectDealController {
    public NSIBPM__Service_Request__c srRecord { get; set; }
    public String message { get; set; }
    public Boolean lockFields { get; set; }
    public String srId { get; set; }

    public Damac_RejectDealController(ApexPages.StandardController stdController) {
        srId = '';
        message = '';
        srRecord = new NSIBPM__Service_Request__c();
        srId = apexpages.currentpage().getparameters().get('id');
        srRecord = [SELECT
                    NSIBPM__Internal_SR_Status__c, NSIBPM__External_SR_Status__c,
                    NSIBPM__Rejection_Reason__c, Reject_Status__c, Bulk_Deal__c, Bulk_Deal__r.Name 
                    FROM NSIBPM__Service_Request__c
                    WHERE ID = :srId];
        if (srRecord.NSIBPM__Rejection_Reason__c != NULL && srRecord.Reject_Status__c != NULL) {
            lockFields = false;
        }
    }
    // old step for the old step
    /*
    // to update sr record, closing the sr step
    public void save() {
        message = '';
        lockFields = false;
        try {
            if (srRecord.NSIBPM__Rejection_Reason__c != NULL && srRecord.Reject_Status__c != NULL) {
                try {
                    NSIBPM__SR_Status__c srStatus = new NSIBPM__SR_Status__c();
                    srStatus = [SELECT ID
                                FROM NSIBPM__SR_Status__c
                                WHERE NSIBPM__Code__c = 'REJECTED'
                                Order BY CreatedDate Desc
                                LIMIT 1];

                    srRecord.NSIBPM__Internal_SR_Status__c = srStatus.ID;
                    srRecord.NSIBPM__External_SR_Status__c = srStatus.ID;
                } catch(Exception e) { }
                update srRecord;

                ID StepID = apexpages.currentpage().getParameters().get('stepId');
                NSIBPM__Step__c step = [SELECT
                                        Name, NSIBPM__Summary__c, RecordTypeId, NSIBPM__Step_Status__c,
                                        RecordType.DeveloperName, OwnerId, Owner.Name,
                                        RecordType.Name, NSIBPM__Parent_Step__c,
                                        NSIBPM__SR__c, NSIBPM__SR__r.NSIBPM__SR_Template__c,
                                        NSIBPM__Status__c, NSIBPM__SR_Step__c,
                                        NSIBPM__Rejection_Reason__c, NSIBPM__Status__r.Name,
                                        NSIBPM__SR_Step__r.NSIBPM__Step_Template_Code__c
                                        FROM NSIBPM__Step__c
                                        WHERE Id != null
                                        AND Id = :StepID
                                        AND NSIBPM__SR__c != null
                                        AND NSIBPM__SR__r.NSIBPM__SR_Template__c != null
                                        AND IsDeleted = false
                                        LIMIT 1];
                try {
                    NSIBPM__Transition__c stepTransaction = new NSIBPM__Transition__c();
                    if (!Test.isRunningTest())
                    stepTransaction = [SELECT
                                    NSIBPM__From__c, NSIBPM__To__c
                                    FROM NSIBPM__Transition__c
                                    WHERE NSIBPM__From__c = :step.NSIBPM__Status__c
                                    AND NSIBPM__To_Status_text__c = 'Rejected'
                                    LIMIT 1];

                    step.NSIBPM__Status__c = stepTransaction.NSIBPM__To__c;
                    step.NSIBPM__Step_Notes__c = srRecord.NSIBPM__Rejection_Reason__c;
                    step.NSIBPM__Rejection_Reason__c = srRecord.Reject_Status__c;
                    if (!Test.isRunningTest())
                    update step;

                    // Calling IPMS service
                    CancelDealRejectionSteps obj = new CancelDealRejectionSteps();
                    obj.EvaluateCustomCode(srRecord, step);

                    message = Label.Deal_Status;
                    lockFields = true;
                } catch(Exception e) {
                    message = Label.Deal_Status;
                    lockFields = true;
                }
            } else {
                message = 'Required Fields are missing.';
            }
        } catch(Exception e) {
            message = e.getMessage() + ' ' + e.getLineNumber();
        }
    }
    */
    
    //CloudzLab Code for the new Step
    public void save() {
        message = '';
        lockFields = false;
        try {
            if(srRecord.Bulk_Deal__c != null){
                message = 'You cannot reject this deal as it is linked to a Bulk Deal '+srRecord.Bulk_Deal__r.Name;
            }else{
                if (srRecord.NSIBPM__Rejection_Reason__c != NULL && srRecord.Reject_Status__c != NULL) {
                    try {
                        ID StepID = apexpages.currentpage().getParameters().get('stepId');
                        New_Step__c step = [SELECT Step_No__c, Step_Status__c
                                            FROM New_Step__c
                                            WHERE Id = :StepID
                                            LIMIT 1];
                        
                        Step_Status_Transition__c stepTransition = new Step_Status_Transition__c();
                        if (!Test.isRunningTest())
                            stepTransition = [SELECT From_Step_Status__c, To_Step_Status__c, Is_Closed__c, SR_External_Status__c, SR_Internal_Status__c
                                              FROM Step_Status_Transition__c 
                                              WHERE To_Step_Status__c = 'Rejected' 
                                              AND From_Step_Status__c = :step.Step_Status__c
                                              AND Step_No__c = :step.Step_No__c
                                              Limit 1];
                        
                        srRecord.Internal_Status__c = stepTransition.SR_Internal_Status__c;
                        srRecord.External_Status__c = stepTransition.SR_External_Status__c;
                        update srRecord;
                        
                        step.Step_Status__c = stepTransition.To_Step_Status__c;
                        step.Comments__c = srRecord.NSIBPM__Rejection_Reason__c;
                        step.Rejection_Reason__c = srRecord.Reject_Status__c;
                        step.Is_Closed__c = stepTransition.Is_Closed__c;
                        if (!Test.isRunningTest())
                            update step;
                        
                        //Calling IPMS service
                        List<Id> stepIdList = new List<Id>();
                        stepIdList.add(StepID);
                        Invocable_CancelDealRejectionSteps.CancelDealRejectionSteps(stepIdList);
                        
                        message = Label.Deal_Status;
                        lockFields = true;
                    } catch(Exception e) {
                        message = Label.Deal_Status;
                        lockFields = true;
                    }
                } else {
                    message = 'Required Fields are missing.';
                }
            }
        } catch(Exception ex) {
            system.debug('Exception message: ' + ex.getMessage());
            system.debug('Exception line: ' + ex.getLineNumber());
            message = ex.getMessage() + ' ' + ex.getLineNumber();
        }
    }
    
}