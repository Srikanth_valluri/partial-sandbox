/**********************************************************************************************************************
Description: This class contains reusable SOQL queries & generic methods required in Damac Living App API classes
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By      | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   11-01-2021      | Anand Venkitakrishnan | Created initial draft
1.1     |   14-01-2021      | Shubham Suryawanshi   | Added Constants for: Reset Password with OTP module
***********************************************************************************************************************/

public class HDApp_Constants {

    public static final String successMsg = 'Successful';
    public static final String commonErrorMsg = 'This may be because of technical error that we\'re working to get fixed. Please try again later.';
    public static final String NO_JSON = 'No JSON body found in request';
    
    /***********************************************************************************************
    Description : Constants for: Work Permit
    Author : Anand Venkitakrishnan
    Used in : 
    ************************************************************************************************/    
    public static final String securityChequeDepositAmountMessage = 'Security Deposit Cheque for each Unit.\nCheque to be made in favor of Luxury facilities Management Co. LLC. - Without Date.';
    public static final String temporaryPowerConnectionAmountMessage = 'By Cheque for temporary power and water connection for each unit monthly.';
    public static final String garbageDisposalAmountMessage = 'By Cheque for garbage disposal for each unit (monthly and Excluding the constructional waste).';
    public static final String wpFMProcessName = 'Work Permit';

    /***********************************************************************************************
    Description : Constants used in Work Permit Create SR API 
    Author : Shubham Suryawanshi
    Used in : 
    ************************************************************************************************/    
    public static final String NO_ACTION = 'No action parameter value passed in request';
    public static final String INVALID_ACTION = 'Invalid action parameter passed. Expected values are : submit/draft/notify_contractor';
    public static final String NO_ACCOUNT = 'No account_id found in request';
    public static final String NO_BOOKING_UNIT = 'No booking_unit_id found in request';
    public static final String NO_ACCOUNT_REC_FOUND = 'No account record was found';
    public static final String NO_BU_REC_FOUND = 'No booking unit record was found';
    public static final String INVALID_PERSON_TO_COLLECT = 'Person To Collect Should be Contractor or Consultant';
    public static final String CONTRACTOR_NOTIFIED = 'An email with instructions has been sent to the given email id. Please note the SR no. and inform your contractor';
    public static final String NO_OTP_ENTERED = 'Please enter OTP and submit';
    public static final String INVALID_OTP = 'Inavlid OTP entered. Please enter correct OTP and submit';
    public static final String NO_CONTRACTOR_EMAIL = 'Please enter contractor/consultant Email Id to proceed.';
    
    
    /***********************************************************************************************
    Description : Constants for: Reset Password with OTP module
    Author : Shubham Suryawanshi
    Used in : HdApp_ResetPasswordOTPProcess_API.cls, HDApp_ResetPassword_API.cls
    ************************************************************************************************/    
    public static final String NO_USERNAME_PASSED = 'Please enter a valid username to continue';
    public static final String NO_USERNAME_ATTR = 'No \'username\' attribute param found in request';
    public static final String INVALID_USERNAME = 'Please enter a valid username to continue';
    public static final String RESET_PASSWORD_OTP_EMAIL_SUBJECT = 'OTP for reset password request';
    public static final String NO_OTP_PASSED = 'Please key in the OTP to continue';
    public static final String OTP_VERIFICATION_TIMEOUT = 'Your OTP has expired. Please try to enter the new OTP within 5 minutes.'; /*As per Bug : 5586 */
    public static final String OTP_VERIFICATION_SUCCESS = 'OTP successfully verified';
    public static final String WRONG_OTP = 'Incorrect OTP entered';
    public static final String RESET_PASSWORD_TIMEOUT = 'Your session has timed out. Please try again.';/*As per Bug : 5585 */
    public static final String NO_PASSWORD_FOUND = 'Please enter the new password';
    public static final String REST_PASSWORD_SUCCESS = 'Your password has been changed successfully. Please login with your new password.';
    public static final String INVALID_RESET_SEND_OTP_ENDPOINT = 'Invalid endpoint. Expected endpoint : /resetPasswordOTP/sendOTP';
    public static final String RESET_PASSWORD_PROCESS = 'Community User Reset Password';
    public static final String SUCCESSFULLY_OTP_SENT = 'We have sent an OTP to your registered Email ID {0} and mobile number {1}. Please eneter the OTP below to continue. ';
    public static final String INVALID_RESET_VERIFY_OTP_ENDPOINT = 'Invalid endpoint. Expected endpoint : /resetPasswordOTP/verifyotp';
    public static final String NO_GUID_ATTR = 'Missing parameter : guid';
    public static final String NO_GUID_PASSED = 'Missing parameter value : guid';
    public static final String NO_OTP_ATTR = 'Missing paramter : otp';
    public static final String INVALID_GUID = 'Either invalid guid passed OR is expired';
    public static final String INVALID_RESET_PWD_ENDPOINT = 'Invalid endpoint. Expected endpoint : /resetPassword/v.1.0';
    public static final String NO_PASSWORD_ATTR = 'Missing parameter : newPassword';
    public static final String REPEATED_PWD_MSG = 'The password provided has been used previously. Kindly enter a new password.';/** As per bug : 5584 */
    public static final String INAVALID_PWD_MSG = 'Your password must include letters and numbers';
    public static final String TOO_MANY_OTP_REQ_ATTEMPTS = 'Please try again after {0} minutes';
    public static final String RESET_PWD_EMAIL_BODY =  'Dear {accountName},  '  + 
    '\n\n'  + 
    'Please use the OTP {otp} to complete your change password request. Please note that this OTP is valid only for next 5 minutes.  '  + 
    '\n\n'  + 
    'Thanks'; 
    public static final String RESET_PWD_SMS_BODY =   '{otp} is the OTP to complete your change password request. Please note that this OTP is valid only for next 5 minutes.' ; 
    public static final String REQUEST_NEW_OTP = 'Please request for new OTP';
    /***********************************************************************************************
    ==============================                  END               ==============================
    ************************************************************************************************/    

    public static void startUpMethod() {
        System.debug('Inside startUpmethod');
    }
}