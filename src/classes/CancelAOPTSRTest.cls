/* * * * * * * * * * * * * *
*  Class Name:   CancelAOPTSRTest
*  Purpose:      Unit test class for CancelAOPTSR
*  Author:       Hardik Mehta - ESPL
*  Company:      ESPL
*  Created Date: 21-Nov-2017
*  Type:         Test Class
* * * * * * * * * * * * */
@isTest
public class CancelAOPTSRTest{
     public static final String strBookingUnitActiveStatus = 'Agreement executed by DAMAC';
  /* * * * * * * * * * * * *
  *  Method Name:  testCase
  *  Purpose:      This method is used to check the functionality when the Case is inserted 
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 21-Nov-2017
  * * * * * * * * * * * * */  
    public static testmethod void testCase(){
         List<Booking__c> bookingList = new List<Booking__c>();
         List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
         List<SR_Booking_Unit__c> srBookingUnitList = new List<SR_Booking_Unit__c>();
    
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        //Id aoptRecordTypeID = getRecordTypeIdForAOPT();
        Case objCase = new Case();
        objCase.Status = 'Submitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Case_Type__c = 'Proof of Payment';  
        insert objCase;
        
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        insert objInventory;

            //create Deal SR record
            NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
            insert objDealSR;

        //create Booking record for above created Deal and Account
            bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
            insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList)
        {
          objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
          objBookingUnit.Inventory__c = objInventory.Id;
          objBookingUnit.Registration_ID__c = '74712';
        }
        system.debug('>>>>>'+bookingUnitList);
        insert bookingUnitList;

        srBookingUnitList = TestDataFactory_CRM.createSRBookingUnis(objCase.Id , bookingUnitList);
        system.debug('+++++++++'+srBookingUnitList);
        insert srBookingUnitList;
        
        Task taskObj = new Task();
        taskObj.Status = 'In Progress';
        taskObj.Subject = 'Update AOPT Details in IPMS';
        insert taskObj;
        
        Payment_Plan__c ObjPaymentPlanOld = new Payment_Plan__c();
        ObjPaymentPlanOld.Status__c = 'InActive';
        ObjPaymentPlanOld.Booking_Unit__c = bookingUnitList[0].id;
        insert ObjPaymentPlanOld;

        Payment_Plan__c ObjPaymentPlan = new Payment_Plan__c();
        ObjPaymentPlan.Status__c = 'Active';
        ObjPaymentPlan.Booking_Unit__c = bookingUnitList[0].id;
        ObjPaymentPlan.Parent_Payment_Plan__c = ObjPaymentPlanOld.id;
        insert ObjPaymentPlan;
        
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        CancelAOPTSR  objCancel = new CancelAOPTSR(stdController);
        objCancel.caseId = objCase.Id;
        objCancel.cancelCase();
    
    }
  /* * * * * * * * * * * * *
  *  Method Name:  testCaseNotSubmitted
  *  Purpose:      This method is used to check the functionality when the Case is not submitted 
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 21-Nov-2017
  * * * * * * * * * * * * */    
        public static testmethod void testCaseNotSubmitted(){
         List<Booking__c> bookingList = new List<Booking__c>();
         List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
          List<SR_Booking_Unit__c> srBookingUnitList = new List<SR_Booking_Unit__c>();
    
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        //Id aoptRecordTypeID = getRecordTypeIdForAOPT();
        Case objCase = new Case();
        objCase.Status = '';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Case_Type__c = 'Proof of Payment';  
        insert objCase;        
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        CancelAOPTSR  objCancel = new CancelAOPTSR(stdController);
        objCancel.caseId = objCase.Id;
        objCancel.cancelCase();
    
    }
  /* * * * * * * * * * * * *
  *  Method Name:  testCaseNotInserted
  *  Purpose:      This method is used to check the functionality when the Case is not inserted
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 21-Nov-2017
  * * * * * * * * * * * * */       
            public static testmethod void testCaseNotInserted(){
             List<Booking__c> bookingList = new List<Booking__c>();
            List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
            List<SR_Booking_Unit__c> srBookingUnitList = new List<SR_Booking_Unit__c>();
    
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
       
        Case objCase = new Case();
        objCase.Status = '';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Case_Type__c = 'Proof of Payment';  
        insert objCase;
      
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        CancelAOPTSR  objCancel = new CancelAOPTSR(stdController);
        //objCancel.caseId = objCase.Id;
        objCancel.cancelCase();
    
    }
  /* * * * * * * * * * * * *
  *  Method Name:  testCaseNotInserted
  *  Purpose:      This method is used to check the functionality when the task is not inserted
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 21-Nov-2017
  * * * * * * * * * * * * */      
    public static testmethod void testNoTaskInserted(){
         List<Booking__c> bookingList = new List<Booking__c>();
    List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
    List<SR_Booking_Unit__c> srBookingUnitList = new List<SR_Booking_Unit__c>();
    
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        //Id aoptRecordTypeID = getRecordTypeIdForAOPT();
        Case objCase = new Case();
        objCase.Status = 'Submitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Case_Type__c = 'Proof of Payment';  
        insert objCase;
        
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        insert objInventory;

            //create Deal SR record
            NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
            insert objDealSR;

        //create Booking record for above created Deal and Account
            bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
            insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList)
        {
          objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
          objBookingUnit.Inventory__c = objInventory.Id;
          objBookingUnit.Registration_ID__c = '74712';
        }
        system.debug('>>>>>'+bookingUnitList);
        insert bookingUnitList;

        srBookingUnitList = TestDataFactory_CRM.createSRBookingUnis(objCase.Id , bookingUnitList);
        system.debug('+++++++++'+srBookingUnitList);
        insert srBookingUnitList;
        
        Payment_Plan__c ObjPaymentPlan = new Payment_Plan__c();
        ObjPaymentPlan.Status__c = 'Active';
        ObjPaymentPlan.Booking_Unit__c = bookingUnitList[0].id;
        insert ObjPaymentPlan;
        
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        CancelAOPTSR  objCancel = new CancelAOPTSR(stdController);
        objCancel.caseId = objCase.Id;
        objCancel.cancelCase();
    
    }
  /* * * * * * * * * * * * *
  *  Method Name:  testPaymentPlanInactive
  *  Purpose:      This method is used to check the functionality when the Payment plan is In active
  *  Author:       Hardik Mehta
  *  Company:      ESPL
  *  Created Date: 21-Nov-2017
  * * * * * * * * * * * * */     
        public static testmethod void testPaymentPlanInactive(){
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        List<SR_Booking_Unit__c> srBookingUnitList = new List<SR_Booking_Unit__c>();
    
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;
        //Id aoptRecordTypeID = getRecordTypeIdForAOPT();
        Case objCase = new Case();
        objCase.Status = 'Submitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Case_Type__c = 'Proof of Payment';  
        insert objCase;
        
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        insert objInventory;

            //create Deal SR record
            NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
            insert objDealSR;

        //create Booking record for above created Deal and Account
            bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
            insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList)
        {
          objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
          objBookingUnit.Inventory__c = objInventory.Id;
          objBookingUnit.Registration_ID__c = '74712';
        }
        system.debug('>>>>>'+bookingUnitList);
        insert bookingUnitList;

        srBookingUnitList = TestDataFactory_CRM.createSRBookingUnis(objCase.Id , bookingUnitList);
        system.debug('+++++++++'+srBookingUnitList);
        insert srBookingUnitList;
        
        Task taskObj = new Task();
        taskObj.Status = 'In Progress';
        taskObj.Subject = 'Update AOPT Details in IPMS';
        insert taskObj;
        
        Payment_Plan__c ObjPaymentPlan = new Payment_Plan__c();
        ObjPaymentPlan.Status__c = 'InActive';
        ObjPaymentPlan.Booking_Unit__c = bookingUnitList[0].id;
        insert ObjPaymentPlan;
        
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        CancelAOPTSR  objCancel = new CancelAOPTSR(stdController);
        objCancel.caseId = objCase.Id;
        objCancel.cancelCase();
    
    }
}