/************************************************************************************************
 * @Name              : UpdateUnitAssignmentsRankingBatch_Test
 * @Description       : Test Class for UpdateUnitAssignmentsRankingBatch
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         05/06/2020       Created
***********************************************************************************************/

@isTest
public class UpdateUnitAssignmentsRankingBatch_Test {

    static Database.QueryLocator queryLoc;
    static Database.BatchableContext BC; 
    static List<User_Assignment__c> userList;
    
    public static testMethod void testschedule() {
        Test.StartTest();
        UpdateUnitAssignmentsRankingScheduler unitAssign = new UpdateUnitAssignmentsRankingScheduler();
        String sch = '0 0 23 * * ?';
        system.schedule('Test status Check', sch, unitAssign );
        Test.stopTest();
    }
    
    
    
    
    @isTest
    static void testMethod1(){
        
         
        
        Id RMProfileId = [select Id from Profile where name = 'property consultant' LIMIT 1].Id;
        Id UserRoleId = [select Id from UserRole where name = 'Chairman' LIMIT 1].Id;
        Id UserId = [select Id from User where ProfileId =: RMProfileId LIMIT 1].Id;
     /*    User us = new User(    firstname = 'fName',
                                lastName = 'lName',
                                email = 'jithesh@qburst.com',
                                Username = 'superusername@test.com',
                                emailencodingkey='UTF-8',
                                Alias = 'jith',
                                TimeZoneSidKey = 'America/Los_Angeles',
                                LocaleSidKey = 'en_US',
                                LanguageLocaleKey = 'en_US',
                                ProfileId = RMProfileId,
                                UserRoleId = UserRoleId);
    insert us;*/
        
        
        User_Assignment__c userAssignment = new User_Assignment__c();
        userAssignment.Rank__c = 1;
        userAssignment.Shift_Day__c = Date.today();
        userAssignment.Shift_Time__c = Time.newInstance(10, 30, 00, 00);
        userAssignment.User_Assigned__c = UserId;        
        insert userAssignment;
        
        Inquiry_User_Assignment_Rules__c userRules = new Inquiry_User_Assignment_Rules__c();
        userRules.SetupOwnerId = UserId;
        userRules.User_Profile__c = 'property consultant';
        userRules.Net_Direct_Sales_Rank__c = userAssignment.Rank__c;
        insert userRules;
        
       
        
        userList = new List<User_Assignment__c>();
        userList.add(userAssignment);
        test.starttest();
        invokeBatch(userList);
        test.stoptest();  
        
    }
    
    @isTest
    static void testMethod2(){
        
         
        
        Id RMProfileId = [select Id from Profile where name = 'system administrator' LIMIT 1].Id;
        Id RMProfile2Id = [select Id from Profile where name = 'property consultant' LIMIT 1].Id;
        Id UserRoleId = [select Id from UserRole where name = 'Chairman' LIMIT 1].Id;
        Id UserId = [select Id from User where ProfileId =: RMProfileId LIMIT 1].Id;
        Id User2Id = [select Id from User where ProfileId =: RMProfile2Id LIMIT 1].Id;
     /*    User us = new User(    firstname = 'fName',
                                lastName = 'lName',
                                email = 'jithesh@qburst.com',
                                Username = 'superusername@test.com',
                                emailencodingkey='UTF-8',
                                Alias = 'jith',
                                TimeZoneSidKey = 'America/Los_Angeles',
                                LocaleSidKey = 'en_US',
                                LanguageLocaleKey = 'en_US',
                                ProfileId = RMProfileId,
                                UserRoleId = UserRoleId);
    insert us;*/
        
        
        User_Assignment__c userAssignment = new User_Assignment__c();
        userAssignment.Rank__c = 1;
        userAssignment.Shift_Day__c = Date.today();
        userAssignment.Shift_Time__c = Time.newInstance(10, 30, 00, 00);
        userAssignment.User_Assigned__c = UserId;        
        insert userAssignment;
        
        Inquiry_User_Assignment_Rules__c userRules = new Inquiry_User_Assignment_Rules__c();
        userRules.SetupOwnerId = User2Id;
        userRules.User_Profile__c = 'property consultant';
        userRules.Net_Direct_Sales_Rank__c = userAssignment.Rank__c;
        insert userRules;
    
       
        
        userList = new List<User_Assignment__c>();
        userList.add(userAssignment);
        test.starttest();
        invokeBatch(userList);
        test.stoptest();  
        
    }

    static void invokeBatch(List<User_Assignment__c> scope){
         UpdateUnitAssignmentsRankingBatch powerlineBatchcls = new UpdateUnitAssignmentsRankingBatch();
         queryLoc = powerlineBatchcls.start(BC);
         powerlineBatchcls.execute(BC,scope);
         powerlineBatchcls.finish(BC);
    }



}