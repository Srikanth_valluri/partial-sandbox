global class UpdateEmailOnPortalCxBatch implements Database.Batchable<sObject> {
    String query;
    public static Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
     
    global Database.QueryLocator start(Database.BatchableContext BC) {
    
        query = 'SELECT Id,PersonEmail,Party_ID__c,Email__pc,RecordTypeId,OwnerId FROM Account WHERE RecordTypeId =: personAccRTId AND Party_ID__c != \'\' AND Email__pc != null AND IsCustomerPortal = true ';
        
        //AND Id = \'0012500000hAWTQAA4\'
        
        system.debug( ' query : ' + query );
        
        return Database.getQueryLocator(query);
    }
    
    global void execute( Database.BatchableContext BC, List<Account> accLsLst ) {
        system.debug( ' accLsLst : ' + accLsLst.size() );

        List<Account> lstAcc = new List<Account>();
        Set<String> setPartyId = new Set<String>();
        Map<String,Account> mapPartyIdAcc  = new Map<String,Account>();     
        
        system.debug( ' accLsLst : ' + accLsLst );
        for( Account accObj : accLsLst ) {
            system.debug( ' accObj : ' + accObj );
            if( accObj.RecordTypeId == personAccRTId 
                && accObj.Party_ID__c != '' 
                && accObj.Email__pc != null
                && string.valueOf(accObj.OwnerId).startsWith('005') ) {
        

                setPartyId.add( accObj.Party_ID__c );
                mapPartyIdAcc.put(accObj.Party_ID__c ,accObj);
                
                if( accObj.PersonEmail != null) {
                    if( accObj.Email__pc != accObj.PersonEmail ) {
                        accObj.PersonEmail = accObj.Email__pc;
                    }
                }else{
                    accObj.PersonEmail = accObj.Email__pc;
                }
                
                lstAcc.add(accObj);
            }
        }
        
        system.debug( ' setPartyId : ' + setPartyId );
        system.debug( ' setPartyId.size() : ' + setPartyId.size() );
        system.debug( ' mapPartyIdAcc : ' + mapPartyIdAcc );
        system.debug( ' mapPartyIdAcc.size() : ' + mapPartyIdAcc.size() );
        
        
        
        if ( lstAcc != null && lstAcc.size() > 0) {
            update lstAcc;
        }

        
        

    }
     
    global void finish(Database.BatchableContext BC) {

    }
}