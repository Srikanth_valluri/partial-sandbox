@isTest
public class API_SubmitBooking_Test {

    @testSetup 
    static void createDataTestSetup() {
        Location__c loc ; 
        Inventory__c inventory ; 
        Address__c addressDetail ; 
        Property__c propertyDetail ; 
        
	    loc = InitialiseTestData.getLocationDetails('1234','Building');
	    insert loc ;
	    loc = InitialiseTestData.getLocationDetails('2345','Floor');
	    insert loc ;
	    loc = InitialiseTestData.getLocationDetails('3456','Unit');
	    insert loc ;
	    addressDetail = InitialiseTestData.getAddressDetails(9086);
	    //insert addressDetail ; 
	    propertyDetail = InitialiseTestData.getPropertyDetails(7650);
	    insert propertyDetail ; 
	    inventory = InitialiseTestData.getInventoryDetails('3456','1234','2345',9086,7650); 
	    insert inventory ; 
	    inventory.Status__c = 'Released';
	    update inventory ; 

    }
    
    @isTest 
    static void doSubmitBooking_Test() {
        RestRequest req = new RestRequest(); 
        
        req.requestURI = '/api_UnitPreBooking';  
        req.httpMethod = 'POST';
        
        API_UnitPreBooking.UnitPreBookingRequestDTO unitPreBookingRequestDTO = new API_UnitPreBooking.UnitPreBookingRequestDTO();
        
        unitPreBookingRequestDTO.selectedInventoryIds = new List<String>();
        unitPreBookingRequestDTO.eoiID = '';
        unitPreBookingRequestDTO.isUkBooking = false;
        
        for (Inventory__c inventory : [SELECT Id FROM Inventory__c]) {
            unitPreBookingRequestDTO.selectedInventoryIds.add(inventory.Id);
        }
        
        RestContext.request = req;
        RestContext.response = new RestResponse();
        
        API_UnitPreBooking.doPost(unitPreBookingRequestDTO);
        
        API_UnitPreBooking.UnitPreBookingResponseDTO responseDTO = (API_UnitPreBooking.UnitPreBookingResponseDTO)JSON.deserialize(RestContext.response.responseBody.toString(), API_UnitPreBooking.UnitPreBookingResponseDTO.class);
        
        Test.startTest();
        
        API_SubmitBooking.SubmitBookingRequestDTO submitBookingRequestDTO = new API_SubmitBooking.SubmitBookingRequestDTO();
        submitBookingRequestDTO.serviceRequestId = responseDTO.srID;
        
        submitBookingRequestDTO.unitsList = new List<Selected_Units__c>();
        
        for (API_UnitPreBooking.selectedUnits unit : responseDTO.units) {
            Selected_Units__c selectedUnit = new Selected_Units__c();
            selectedUnit.Id = unit.selectedUnitId;
            
            selectedUnit.SPA_Type__c = 'Standard SPA';
            //selectedUnit.Payment_Plan__c = selectedUnit.Payment_Plan__c;

            submitBookingRequestDTO.unitsList.add(selectedUnit);
        }
        
        submitBookingRequestDTO.modeOfPayment = 'Cash';
        submitBookingRequestDTO.tokenAmountAED = 30000;
        //submitBookingRequestDTO.selectedAgencyId = ;
        submitBookingRequestDTO.rmEmail = 'test@test.com';
        
        API_SubmitBooking.doPost(submitBookingRequestDTO);
        
        Test.stopTest();
    }
    
}