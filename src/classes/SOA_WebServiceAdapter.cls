global class SOA_WebServiceAdapter extends GenericWebServiceAdapter implements WebServiceAdapter{
    
    global String url; 
    
    public override WebServiceAdapter call(){
        String regID_BUID = parameters.get('regID_BUID');
        system.debug('!!!!!!!!regID_BUID'+regID_BUID);
        String BUId, regID;
        
        if(String.isNotBlank(regID_BUID)) {
            BUId = regID_BUID.substringBefore('-');
            regID = regID_BUID.substringAfter('-');
        }
        GenerateSOAController.soaResponse objSOA = GenerateSOAController.getSOADocument(regID);
        if(objSOA != NULL && String.isNotBlank(objSOA.url)) {
            this.url = objSOA.url;
        }
        return this;
    }
    
    public override String getResponse(){
        return this.url;
    }
    
    
}