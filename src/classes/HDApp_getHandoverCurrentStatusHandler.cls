/**********************************************************************************************************************
Description: This is the handler class for api class HDApp_getHandoverCurrentStatusDataAPI 
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   26-10-2020      | Shubham Suryawanshi | Changed the input params for method - getCallingList()
1.1     |   28-12-2020      | Shubham Suryawanshi | Added few additional params in query - returnBookingUnitDetails() method
***********************************************************************************************************************/
public without sharing class HDApp_getHandoverCurrentStatusHandler {
    public HDApp_getHandoverCurrentStatusHandler() {

    }
    public static List<Calling_List__c> getCallingList(String bookingUnitId, List<String> subProcessName){
        List<Calling_List__c> returnCallingList = new List<Calling_List__c>();
        returnCallingList = [SELECT Id
                                , Name
                                , Booking_Unit__c
                                , Appointment_Date__c
                                , Appointment_Rejection_Reason__c
                                , Appointment__c
                                , Appointment_Status__c
                                , Appointment__r.Slots__c
                                , Amount_Due__c //For -Inspection_By
                                , Sub_Purpose__c
                            FROM Calling_List__c
                            WHERE Booking_Unit__c =: bookingUnitId
                            AND Sub_Purpose__c IN: subProcessName
                            ORDER BY CreatedDate DESC
                            /*LIMIT 1*/];
        System.debug('returnCallingList'+returnCallingList);
        return returnCallingList;
    }
    /**********************************************************************************************************************
    Description : Method to create the SR Attachment records related to Account and Booking Unit
    Parameter(s )  : Booking_Unit__c, accountId, strDocumentName, UploadMultipleDocController.data, fileName
    Return Type : void
    **********************************************************************************************************************/
    public static List<Case> fetchCaseRecords(String bookingUnitId, String accountId){
        List<Case> caseList = new List<Case>();
        caseList = [SELECT Id,
                        RecordTypeId,
                        Status,
                        Booking_Unit__c,
                        AccountId,
                        Title_Deed_Uploaded__c
                    FROM Case WHERE Booking_Unit__c =: bookingUnitId
                    AND AccountId =: accountId
                    AND RecordType.Name = 'Title Deed'];
        System.debug('caseList'+caseList);
        return caseList;
    }
    public static locationWrapper returnLocationFromCallingList(String callinglistId){
        System.debug('callinglistId'+callinglistId);
        
        locationWrapper locationWrapperInstance = new locationWrapper();
        List<Calling_List__c> callingList = [SELECT Id
                                                , Name
                                                , Booking_Unit__c
                                                , Appointment_Status__c
                                                , Appointment_Date__c
                                                , Appointment_Rejection_Reason__c
                                                , Account__c
                                                , Appointment__c
                                                , Appointment__r.Slots__c
                                                , Appointment__r.Building__c
                                                , Appointment__r.Building__r.Property_Name__c
                                                , Appointment__r.Building__r.Property_Name__r.Latitude__c
                                                , Appointment__r.Building__r.Property_Name__r.Longitude__c
                                            FROM Calling_List__c
                                            WHERE Id =: callinglistId];
        System.debug('callingList'+callingList);
        for(Calling_List__c callingListObj: callingList){
            Appointment__c appointmentObj = new Appointment__c();
            appointmentObj.Id = callingListObj.Appointment__c != null ? callingListObj.Appointment__c : null;
            system.debug('appointmentObj.id'+appointmentObj.Id);
            appointmentObj = callingListObj.Appointment__r != null ? callingListObj.Appointment__r : null;
            system.debug('appointmentObj'+appointmentObj);

            Location__c locationObj = new Location__c();
            locationObj.Id = appointmentObj.Building__c != null ? appointmentObj.Building__c : null;
            system.debug('locationObj.id'+locationObj.Id);
            locationObj = appointmentObj.Building__r != null ? appointmentObj.Building__r : null;
            system.debug('locationObj'+locationObj);

            Property__c propertyObj = new Property__c();
            propertyObj.Id = appointmentObj.Building__r.Property_Name__c != null ? appointmentObj.Building__r.Property_Name__c : null;
            system.debug('propertyObj.id'+propertyObj.Id);
            propertyObj = appointmentObj.Building__r.Property_Name__r != null ? appointmentObj.Building__r.Property_Name__r : null;
            system.debug('propertyObj'+propertyObj);

            locationWrapperInstance.latitude = appointmentObj.Building__r.Property_Name__r.Latitude__c != null ? appointmentObj.Building__r.Property_Name__r.Latitude__c : null;
            System.debug('locationWrapperInstance.latitude'+locationWrapperInstance.latitude);
            locationWrapperInstance.longitude = appointmentObj.Building__r.Property_Name__r.Longitude__c != null ? appointmentObj.Building__r.Property_Name__r.Longitude__c : null;
            System.debug('locationWrapperInstance.longitude'+locationWrapperInstance.longitude);
        }
        return locationWrapperInstance;
    }
    public static Booking_Unit__c returnBookingUnitDetails(String bookingUnitId, String accountId){
        Booking_Unit__c bookingUnitInstance = [SELECT Id,
                                                    Name,
                                                    Unit_Name__c,
                                                    DEWA_Utility_Number__c,
                                                    SnagValue__c,
                                                    Total_snags__c,
                                                    Total_Snags_Open__c,
                                                    Total_Snags_Fixed__c,
                                                    Total_Snags_Closed__c,
                                                    Snags_Completed__c,
                                                    DP_PCC_Issued__c,
                                                    Total_Snags_Reported_by_Customer__c,
                                                    Title_Deed_Uploaded__c,
                                                    Okay_to_release_keys__c,
                                                    Is_Key_Release_Requested__c
                                                FROM Booking_Unit__c
                                                WHERE Id =:bookingUnitId
                                                AND Booking__r.Account__c =: accountId];
        System.debug('bookingUnitInstance'+bookingUnitInstance);
        return bookingUnitInstance;
    }
    public static List<SR_Attachments__c> returnDEWADocumentsFromAccountAndBookigUnit(String bookingUnitId, String accountId){
        List<SR_Attachments__c> documents = [SELECT Id, 
                                            Name,
                                            Account__c,
                                            Booking_Unit__c,
                                            Attachment_URL__c
                                        FROM SR_Attachments__c
                                        WHERE Account__c =: accountId
                                        AND Booking_Unit__c =: bookingUnitId
                                        AND Name Like '%DEWA-Certificate%'
                                        LIMIT 1];
        System.debug('documents'+documents);
        return documents;

    }

    /**********************************************************************************************************************
    Description : Method to get 12 hrs formatted time along with AM/PM for appointment slot booked
    Parameter(s )  : slot(String)
    Return Type : String
    Author : Shubham
    E.g : Input : 13:30 - 14:30 , Output : 1:30PM - 2:30PM
    **********************************************************************************************************************/
    public static String get12HrsFormattedTime(String slot) {
    
        System.debug('slot:: ' + slot);
        if(String.isBlank(slot)) {
            return null;
        }
        List<String> lstSlot = slot.split('-');
        String finaltime = '';

        for(String objStr : lstSlot) {
            
            //System.debug('objStr.left(2):: '+objStr.trim().left(2));
            if(objStr.trim().left(2).contains('12')) {
                objStr = String.valueOf( Decimal.valueOf(objStr.replace(':','.').trim()) + 12);// for handling only 12's case
            }
            Decimal strTime = Decimal.valueOf( objStr.replace(':','.').trim() );

            if(strTime >= 12) {
                finaltime += String.valueOf(strTime - 12).replace('.',':') + ' PM' + ' - ';
            }
            else {

                finaltime += String.valueOf(strTime).replace('.',':') + ' AM' + ' - ';
            }
        }
        System.debug('finaltime:: ' + finaltime.removeEnd('- '));
        return finaltime.removeEnd('- ').trim();
    }

    public class locationWrapper{
        public Decimal latitude;
        public Decimal longitude;
    }
}