/********************************************************************************
* Description - Test class developed for DisplaySourceAndTargetUnitDetailsCntrl *
*                                                                               *
* Version            Date            Author                    Description      *
* 1.0                13/01/2019      Arjun Khatri              Initial Draft    *
********************************************************************************/

@isTest
public class DisplaySourceAndTargetUnitDetailsTest {

    @isTest
    public static void test_positive() {
		
        //Create Customer record
        Account objAcc = new Account(Name = 'Test Account');
        insert objAcc;
        
        //Create Deal
        NSIBPM__Service_Request__c  objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR;
        
        //Create Bookings
        List<Booking__c> listBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id, 1);
        insert listBookings;
        
        //crearte booking unit
        List<Booking_Unit__c> listBUs =  TestDataFactory_CRM.createBookingUnits(listBookings, 1);
        insert listBUs;
        
        //Create Sales Service Request
        Sales_Service_Request__c objSSR = 
                new Sales_Service_Request__c(Account__c  = objAcc.id,
                                             Booking_Unit__c = listBUs[0].Id);
        insert objSSR;
        
        //Create fund Transfer Unit
        Fund_Transfer_Unit__c  objFundUnit = 
            new Fund_Transfer_Unit__c (Name = 'Test Fund Unit',
                                       Sales_Service_Request__c = objSSR.id);
        insert objFundUnit;
        
        //EHO Case record type Id
        Id caseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();
        
        //Create Case
        Case  objCase = TestDataFactory_CRM.createCase(objAcc.Id, caseRecTypeId);
        insert objCase;
        
        //Create Bookings
        List<SR_Booking_Unit__c> lstSRBookingUnits = TestDataFactory_CRM.createSRBookingUnis(objCase.Id, listBUs);
        lstSRBookingUnits[0].Sales_Service_Request__c = objSSR.Id;
        insert lstSRBookingUnits[0];
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objSSR);
        DisplaySourceAndTargetUnitDetailsCntrl objCtrl = new DisplaySourceAndTargetUnitDetailsCntrl(sc);
        PageReference pageRef = Page.DisplaySourceAndTargetUnitDetails;
        pageRef.getParameters().put('id', String.valueOf(objSSR.Id));
        Test.setCurrentPage(pageRef);
        
        Test.startTest();
        objCtrl.init();
        Test.stopTest();
        
    }
    
    @isTest
    public static void test_Error() {

        //Create Customer record
        Account objAcc = new Account(Name = 'Test Account');
        insert objAcc;
        
        //Create Deal
        NSIBPM__Service_Request__c  objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR;
        
        //create booking unit
        List<Booking__c> listBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objSR.Id, 1);
        insert listBookings;
        
        //create booking unit
        List<Booking_Unit__c> listBUs =  TestDataFactory_CRM.createBookingUnits(listBookings, 1);
        insert listBUs;
        
        //Create Sales Service Request
        Sales_Service_Request__c objSSR = 
                new Sales_Service_Request__c(Account__c  = objAcc.id,
                                             Booking_Unit__c = listBUs[0].Id);
        insert objSSR;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objSSR);
        DisplaySourceAndTargetUnitDetailsCntrl objCtrl = new DisplaySourceAndTargetUnitDetailsCntrl(sc);
        PageReference pageRef = Page.DisplaySourceAndTargetUnitDetails;
        Test.setCurrentPage(pageRef);
        
        Test.startTest();
        objCtrl.init();
        Test.stopTest();
        
    }
}