@isTest
/*
* Revision History: 
* Version   Author          Date        Description.
* 1.1       Swapnil Gholap  21/11/2017  Initial Draft
*/

global class RefundsExcessAmountMock implements WebServiceMock {
    global void doInvoke(
       Object stub,
       Object request,
       Map<String, Object> response,
       String endpoint,
       String soapAction,
       String requestName,
       String responseNS,
       String responseName,
       String responseType) {
                            
       Refunds.getExcessAmountResponse_element   respElement = new Refunds.getExcessAmountResponse_element  ();
       respElement.return_x = '{"Status":"S","PROC_STATUS":null,"PROC_MESSAGE":null,"Message":"Process Completed Returning 0 Response Message(s)...","PARAM_ID":"81965","Amount_COCA":"0","Excess_Amount":null,"Amount_Paid":"1650"}';
       response.put('response_x', respElement);
      
   }
   
   global class RefundsExcessAmountMock1 implements WebServiceMock {
    global void doInvoke(
       Object stub,
       Object request,
       Map<String, Object> response,
       String endpoint,
       String soapAction,
       String requestName,
       String responseNS,
       String responseName,
       String responseType) {
                            
       Refunds.getExcessAmountResponse_element   respElement = new Refunds.getExcessAmountResponse_element  ();
       respElement.return_x = '{"Status":"S","PROC_STATUS":null,"PROC_MESSAGE":null,"Message":"Process Completed Returning 0 Response Message(s)...","PARAM_ID":"81965","Amount_COCA":"0","Excess_Amount":"5000","Amount_Paid":"1650"}';
       response.put('response_x', respElement);
       
   }
    
}
    
}