@isTest
private class FM_EmailMessageTriggerHandlerTest{
    @testSetup static void setup(){
        Case objCase = new Case();
        objCase.Origin = 'Email';
        objCase.RecordtypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        objCase.Status = 'Open';
        insert objCase;
        
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Request_Type_DeveloperName__c='Proof_of_Payment';
        fmCaseObj.Nationality__c ='Pakistani';
        fmCaseObj.Mobile_no__c = '123456789';
        fmCaseObj.Outstanding_service_charges__c = '0';
        insert fmCaseObj;
        
        SR_Attachments__c objAttach = new SR_Attachments__c();
        objAttach.Attachment_URL__c = 'https://ptctest.damacgroup.com/DCOFFEE/show/header/dfe0003f9fa4a40f212839a04bc249ae';
        objAttach.FM_Case__c = fmCaseObj.Id;
        objAttach.Is_sent_in_email__c = true ;
        objAttach.isValid__c = true ;
        insert objAttach ;
        
        Location__c objBldg = new Location__c();
        objBldg.Name = 'VCH';
        objBldg.Location_ID__c = 'VCHAR1208';
        objBldg.Loams_Email__c = 'loamsbuilding@gmail.com';
        objBldg.RecordtypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
        insert objBldg;
        
        FM_User__c objUser = new FM_User__c();
        objUser.FM_Role__c = 'Property Director';
        objUser.FM_User__c = UserInfo.getUserId();
        objUser.Building__c = objBldg.Id;
        insert objUser;
    }
    
    @isTest static void testMethod1(){
        EmailMessage objEM = new EmailMessage();
        objEM.toAddress = 'loamsbuilding@gmail.com';
        objEM.fromAddress = 'user@test.com';
        objEM.Subject = 'Subject';
        objEM.ParentId = [Select Id from Case limit 1][0].Id;
        insert objEM;
        
        EmailMessage objEM1 = new EmailMessage();
        objEM1.toAddress = 'loamsbuilding@gmail.com';
        objEM1.fromAddress = 'user@test.com';
        objEM1.Subject = Label.FM_POP_Approval_Email_Subject ;
        objEM1.Related_FM_Case__c = [Select Id from FM_Case__c limit 1][0].Id;
        insert objEM1;
    }
}