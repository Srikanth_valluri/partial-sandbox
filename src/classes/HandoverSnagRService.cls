public with sharing class HandoverSnagRService {
    
    
    public List<SnagRResponseWrapper> getDefects(String bookingUnitName) {
        SnagRWSDL wsdlObj = new SnagRWSDL();
        SnagRWSDL.SnagrwebHttpSoap11Endpoint snagObj = new SnagRWSDL.SnagrwebHttpSoap11Endpoint();
        String responseStr = snagObj.defects(bookingUnitName);
        System.debug('==responseStr===' + responseStr);
        SnagRClass objSnagR = new SnagRClass();
        List<SnagRResponseWrapper> lstSnagR = new List<SnagRResponseWrapper>();
        lstSnagR = (List<SnagRResponseWrapper>)JSON.deserialize(responseStr, List<SnagRResponseWrapper>.class);
        System.debug('==lstSnagR.size===' + lstSnagR.size());
        System.debug('==lstSnagR===' + lstSnagR[0]);
        return lstSnagR;
    }

    public class SnagRClass {
        List<SnagRResponseWrapper> lstWrapper;
        public SnagRClass() {
            lstWrapper = new List<SnagRResponseWrapper>();
        }
    }

    public class SnagRResponseWrapper {
        public String ID;
        public String GUID;
        public String DrawingID;
        public String DrawingTitle;
        public String Status;
        public String CurrentStatus;
        public String UserDescription;
        public String LocationID;
        public String Location;
        public String LocationExtRef;
        public String LocationGUID;
        public String XCoord;
        public String YCoord;
        public String PriorityID;
        public String Priority;
        public String DueBy;
        public String DaysLeft;
        public String GroupID;
        public String GroupName;
        public String GroupInitials;
        public String DateClosed;
        public String BigPhoto;
        public String Lattitude;
        public String Longitude;
        public String CreatedByID;
        public String CreatedBy;
        public String CreatedDate;
        public String SDCategoryID;
        public String SDCategory;
        public String SDCode;
        public String SDType;
        public String SDID;
        public String SDDescription;
        public String DateSync;
        public String MPGUID;
        public String FCGUID;
        public String FutureDefectGUID;
        public String PreviousDefectGUID;
        public String ProjectTitle;
        public String ContractNo;
        public String Role;
        public String UGUserID;
        public String HistoryCount;
        public String LastChanged;
        public String PhotoExists;
    }
}