global without sharing class Invocable_DeRegisterAgents  {
    @InvocableMethod
    public static void deregisterAgents(List<Id> stepId) {
        for(New_Step__c step : [SELECT Id, Service_Request__c FROM New_Step__c WHERE Id IN :stepId]) {
            EvaluateCustomCode(step);
        }
    }


    public static String EvaluateCustomCode(New_Step__c step) {
        String result = 'Success';
        Account newAcc;
        List<Id> deactivateContactIdsList = new List<Id>();

        try {
            if(step.Service_Request__c != null){
                for(Amendment__c thisAmendment : getAllAmendments(step.Service_Request__c)) {
                    if(thisAmendment.Mark_For_Deletion__c){
                        deactivateContactIdsList.add(thisAmendment.Contact__c);
                    }
                }
                if(!deactivateContactIdsList.isEmpty()){
                    deactivateContacts(deactivateContactIdsList);
                    System.enqueueJob(new CC_DeRegisterAgents(deactivateContactIdsList));   
                }
            }else{
                result = 'Step is not associated to a service request.';    
            }
        }catch(Exception e) {
            result = 'Error :' + e.getMessage() + '';
        }
        return result;
    }

    public static List<Amendment__c> getAllAmendments(Id SrId) {
        String amendmentQuery = UtilityQueryManager.getAllFields(Amendment__c.getsObjectType().getDescribe());
        amendmentQuery += ' WHERE Service_Request__c =: SRId';
        return (Database.query(amendmentQuery));
    }

    public static void deactivateContacts(List<Id> contactIdsList){
        List<Contact> contactList =  new List<Contact>();
        for(Id thisId : contactIdsList){
           Contact newContact = new Contact();
           newContact.Id = thisId;
           newContact.Status__c = 'Cancelled';
           contactList.add(newContact);
        }
        if(!contactList.isEmpty()){
            update contactList;
        }
    }
}