/*
Developer Name: Pratiksha Narvekar
Created Date : 19-11-2017
*/
@isTest
private class CREPOPSendEmailTest {
    public static List<Task> listTask = new List<Task>();
    static testMethod void RejectmethodTest() {

        createCaseTask('Reject');
         CREPOPSendEmail.SendMail(listTask);

    }

    static testMethod void CompletedmethodTest() {

        createCaseTask('Completed');
        //Test.setMock(HttpCalloutMock.class, new MockHttpPOPSENDEmail());

        //updated on 8th July
        //New Mock Reponses
        String blobStr = 'Test Blob Response';
        FmHttpCalloutMock.Response bulkSoaInLangMockResponse = new FmHttpCalloutMock.Response(200, 'Success', '   {  '  +
         '       "responseId": "3072019-10305298",  '  +
         '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         '       "status": "S",  '  +
         '       "responseMessage": "Process Completed",  '  +
         '       "elapsedTimeMs": 8497,  '  +
         '       "responseLines": [  '  +
         '           {  '  +
         '               "documentName": "DPSOA",  '  +
         '               "language": "EN",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
         '           },  '  +
         '           {  '  +
         '               "documentName": "DPSOA_AR",  '  +
         '               "language": "AR",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
         '           }  '  +
         '       ],  '  +
         '       "complete": true  '  +
         '  }  ');

        FmHttpCalloutMock.Response getAllReceiptUrlResponse = new FmHttpCalloutMock.Response(200, 'Success', '   {  '  +
         '       "responseId": "3072019-10305298",  '  +
         '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         '       "status": "S",  '  +
         '       "responseMessage": "Process Completed",  '  +
         '       "elapsedTimeMs": 8497,  '  +
         '       "responseLines": [  '  +
         '           {  '  +
         '               "documentName": "DPSOA",  '  +
         '               "language": "EN",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
         '           },  '  +
         '           {  '  +
         '               "documentName": "DPSOA_AR",  '  +
         '               "language": "AR",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
         '           }  '  +
         '       ],  '  +
         '       "complete": true  '  +
         '  }  ');

        FmHttpCalloutMock.Response blobResponse = new FmHttpCalloutMock.Response(200, 'Success', blobStr);
        FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');

        String BULK_SOA_IN_LANG_str = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.BULK_SOA_LANG, new List<String> {'63062'});

        String DP_RECEIPT_ID_str = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.DP_RECEIPT_ID, new List<String> {'12344'});

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
          BULK_SOA_IN_LANG_str => bulkSoaInLangMockResponse,
          DP_RECEIPT_ID_str => getAllReceiptUrlResponse,
          'https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd' => blobResponse,
          'https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a' => blobResponse,
          'https://api.sendgrid.com/v3/mail/send' => sendGridResponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));

        CREPOPSendEmail.SendMail(listTask);

    }
    static testMethod void CancelledmethodTest() {

        createCaseTask('Cancelled');
        CREPOPSendEmail.SendMail(listTask);
        test.StartTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,TaskCreationWSDL.SRDataToIPMSMultipleResponse_element>();
        TaskCreationWSDL.SRDataToIPMSMultipleResponse_element response_x =  new TaskCreationWSDL.SRDataToIPMSMultipleResponse_element();
        response_x.return_x = '{"data":[{"PROC_STATUS":"S","PROC_MESSAGE":"SR Header Data Created for SR # :2-001234","PARAM_ID":"2-001234"},{"PROC_STATUS":"S","PROC_MESSAGE":"Created SR Task Data for SR # :2-001234 Task :Verify POA Documents","PARAM_ID":"2-001234"},{"PROC_STATUS":"E","PROC_MESSAGE":"Error: Param Id Not Passed...","PARAM_ID":"NULL"}],"message":"[WARNING] Check the PROC_MESSAGE attribute for Actual Error Message(s), Error Message Count = 1","status":"S"}';

        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);

        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
        test.StopTest();

    }
        static testMethod void CancelledmethodTesterror() {

        createCaseTask('Cancelled');
        CREPOPSendEmail.SendMail(listTask);
        test.StartTest();
        SOAPCalloutServiceMock.returnToMe = new Map<String,TaskCreationWSDL.SRDataToIPMSMultipleResponse_element>();
        TaskCreationWSDL.SRDataToIPMSMultipleResponse_element response_x =  new TaskCreationWSDL.SRDataToIPMSMultipleResponse_element();
        response_x.return_x = '{}';

        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);

        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
        test.StopTest();

    }
    static testMethod void RejectDuplicatemethodTest() {

        createCaseTask('Reject-Duplicate');
        CREPOPSendEmail.SendMail(listTask);

    }
    static testMethod void NotStartedmethodTest() {

        Account objAccount = [select id from Account where FirstName ='Pratiksha'];
        objAccount.PersonEmail = 'pratiksha.n@accely.com';
        upsert objAccount;


        Case objCase = [select id from case where Account.FirstName='Pratiksha'];

        List<SR_Booking_Unit__c> objSRB = [select id from SR_Booking_Unit__c where Case__c =: objCase.ID];

        list<case> listcaseobj = [Select Id, CaseNumber, RecordType.DeveloperName, RecordType.Name, POA_Name__c, POA_Expiry_Date__c,
                                  Purpose_of_POA__c, POA_Issued_By__c, Amount_Approved__c, Booking_Unit__c,
                                  Booking_Unit__r.Registration_Id__c, Account.Party_ID__c, Approved_Amount__c, CreatedDate,
                                  Actual_Approving_Percent__c, Status, Payment_Date__c, Payment_Mode__c,
                                  Payment_Allocation_Details__c, Total_Amount__c,
                                  (Select Id, Booking_Unit__c, Booking_Unit__r.Registration_Id__c
                                  From SR_Booking_Units__r Where Booking_Unit__c != null),
                                  (Select Id, New_Unit__c, New_Unit__r.Registration_Id__c, Allocated_Amount__c
                                  From Fund_Transfer_Units__r Where New_Unit__c != null)
                                  From Case
                                  Where Id =: objCase.ID];
        System.debug('....listcaseobj...'+listcaseobj);
        Task taskObj = new Task();
        taskObj.WhatId = objCase.ID;
        taskObj.ActivityDate = System.Today();
        taskObj.OwnerId = Userinfo.getUserID();
        taskObj.status = 'Not Started';
        taskObj.Priority = 'Normal';
        taskObj.Activity_Type__c = 'Calls';
        taskObj.Activity_Sub_Type__c = '';
        taskObj.Start_Date__c = System.Today();
        taskObj.End_Date__c = System.Today();
        taskObj.Description = 'Hello';
        taskObj.Cash_Receipt_Id__c='12344';
        taskObj.Pushed_to_IPMS__c = true;
        insert taskObj;
        listTask.add(taskObj);
        System.debug('...listTask...'+listTask);

        CREPOPSendEmail.SendMail(listTask);

    }

   static testMethod void NotStartedmethodTest1() {

        Account objAccount = [select id from Account where FirstName ='Pratiksha'];
        objAccount.PersonEmail = 'pratiksha.n@accely.com';
        upsert objAccount;

        POP_Finance_Users_Email__c objPopFin = new POP_Finance_Users_Email__c();
        objPopFin.Name = '1';
        objPopFin.Active__c = true;
        objPopFin.Email_Address__c = 'pratiksha.n@accely.com';
        insert objPopFin;

        Case objCase = [select id from case where Account.FirstName='Pratiksha'];

        List<SR_Booking_Unit__c> objSRB = [select id from SR_Booking_Unit__c where Case__c =: objCase.ID];

        list<case> listcaseobj = [Select Id, CaseNumber, RecordType.DeveloperName, RecordType.Name, POA_Name__c, POA_Expiry_Date__c,
                                  Purpose_of_POA__c, POA_Issued_By__c, Amount_Approved__c, Booking_Unit__c,
                                  Booking_Unit__r.Registration_Id__c, Account.Party_ID__c, Approved_Amount__c, CreatedDate,
                                  Actual_Approving_Percent__c, Status, Payment_Date__c, Payment_Mode__c,
                                  Payment_Allocation_Details__c, Total_Amount__c,
                                  (Select Id, Booking_Unit__c, Booking_Unit__r.Registration_Id__c
                                  From SR_Booking_Units__r Where Booking_Unit__c != null),
                                  (Select Id, New_Unit__c, New_Unit__r.Registration_Id__c, Allocated_Amount__c
                                  From Fund_Transfer_Units__r Where New_Unit__c != null)
                                  From Case
                                  Where Id =: objCase.ID];
        System.debug('....listcaseobj...'+listcaseobj);
        Task taskObj = new Task();
        taskObj.WhatId = objCase.ID;
        taskObj.ActivityDate = System.Today();
        taskObj.OwnerId = Userinfo.getUserID();
        taskObj.status = 'Not Started';
        taskObj.Priority = 'Normal';
        taskObj.Activity_Type__c = 'Calls';
        taskObj.Activity_Sub_Type__c = '';
        taskObj.Start_Date__c = System.Today();
        taskObj.End_Date__c = System.Today();
        taskObj.Description = 'Hello';
        taskObj.Cash_Receipt_Id__c='12344';
        taskObj.Pushed_to_IPMS__c = true;
        insert taskObj;
        listTask.add(taskObj);
        System.debug('...listTask...'+listTask);

        CREPOPSendEmail.SendMail(listTask);

    }



    public static void createCaseTask(String Status){

        Account objAccount = [select id from Account where FirstName ='Pratiksha'];
        objAccount.PersonEmail = 'pratiksha.n@accely.com';
        upsert objAccount;


        Case objCase = [select id from case where Account.FirstName='Pratiksha'];

        List<SR_Booking_Unit__c> objSRB = [select id from SR_Booking_Unit__c where Case__c =: objCase.ID];

        list<case> listcaseobj = [Select Id, CaseNumber, RecordType.DeveloperName, RecordType.Name, POA_Name__c, POA_Expiry_Date__c,
                                  Purpose_of_POA__c, POA_Issued_By__c, Amount_Approved__c, Booking_Unit__c,
                                  Booking_Unit__r.Registration_Id__c, Account.Party_ID__c, Approved_Amount__c, CreatedDate,
                                  Actual_Approving_Percent__c, Status, Payment_Date__c, Payment_Mode__c,
                                  Payment_Allocation_Details__c, Total_Amount__c,
                                  (Select Id, Booking_Unit__c, Booking_Unit__r.Registration_Id__c
                                  From SR_Booking_Units__r Where Booking_Unit__c != null),
                                  (Select Id, New_Unit__c, New_Unit__r.Registration_Id__c, Allocated_Amount__c
                                  From Fund_Transfer_Units__r Where New_Unit__c != null)
                                  From Case
                                  Where Id =: objCase.ID];
        System.debug('....listcaseobj...'+listcaseobj);
        Task taskObj = new Task();
        taskObj.WhatId = objCase.ID;
        taskObj.ActivityDate = System.Today();
        taskObj.OwnerId = Userinfo.getUserID();
        taskObj.status = Status;
        taskObj.Priority = 'Normal';
        taskObj.Activity_Type__c = 'Calls';
        taskObj.Activity_Sub_Type__c = '';
        taskObj.Start_Date__c = System.Today();
        taskObj.End_Date__c = System.Today();
        taskObj.Description = 'Hello';
        taskObj.Cash_Receipt_Id__c='12344';
        insert taskObj;
        listTask.add(taskObj);
        System.debug('...listTask...'+listTask);
    }

    @testSetup static void setup() {
     Account objAccount = new Account();
     NSIBPM__Service_Request__c objNSR = new NSIBPM__Service_Request__c();
     List<Booking__c> objBooking = new List<Booking__c>();
     List<Booking_Unit__c> objBU = new List<Booking_Unit__c>();
     List<Booking_Unit_Active_Status__c> objBUA =new List<Booking_Unit_Active_Status__c>();
     SR_Attachments__c objSA = new SR_Attachments__c();
     List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
     Case objCase = new Case();
     List<SR_Booking_Unit__c> listSBU = new List<SR_Booking_Unit__c>();

        objAccount = TestDataFactory_CRM.createPersonAccount();
        objAccount.FirstName='Pratiksha';
        objAccount.Party_ID__c = '63062';
        insert objAccount;
        Id POPcase = Schema.SObjectType.case.getRecordTypeInfosByName().get('POP').getRecordTypeId();
        objCase = TestDataFactory_CRM.createCase(objAccount.Id,POPcase);
        objCase.Payment_Date__c = Date.Today();
        objCase.Payment_Mode__c = 'Cash';
        objCase.Payment_Allocation_Details__c = 'test';
        objCase.Total_Amount__c = 2000;
        objCase.Payment_Currency__c = 'AED';
        insert objCase;

        objNSR = TestDataFactory_CRM.createServiceRequest();
        insert objNSR;
        objBooking = TestDataFactory_CRM.createBookingForAccount(objAccount.ID, objNSR.ID, 2);
        insert objBooking;

        lstActiveStatus = TestDataFactory_CRM.createActiveUnitCustomSetting( new list<String>{ 'Agreement executed by DAMAC' } );
        insert lstActiveStatus ;

        objBU = TestDataFactory_CRM.createBookingUnits(objBooking, 4);
        for( Booking_Unit__c objUnit : objBU ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC';
        }
        insert objBU;

        objBUA = TestDataFactory_CRM.createActiveFT_CS();
        insert objBUA;

        objSA = TestDataFactory_CRM.createCaseDocument(objCase.ID,'POP');
        insert objSA;

        /*listSBU = TestDataFactory_CRM.createSRBookingUnis(objCase.ID,objBU);
        insert listSBU;*/
        
        SR_Booking_Unit__c objSRBu = new SR_Booking_Unit__c();
        objSRBu.Booking_Unit__c = objBU[0].Id;
        objSRBu.Case__c = objCase.ID;
        insert objSRBu;

        System.debug('...listSBU...'+listSBU);

        //custom setting
        insert new IpmsRestServices__c(
                   SetupOwnerId = UserInfo.getOrganizationId(),
                   BaseUrl__c = 'http://83.111.194.181:8045/webservices/rest',
                   Username__c = 'oracle_user',
                   Password__c = 'crp1user',
                   Client_Id__c = '8MABLQM-KJ8I8-1XA58-WWCM1S1',
                   BearerToken__c = 'eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWyiwuVrJSKs5IBKK8bCUdpczEEiUrQ1MzAwtTMwsDIx2l1IoCqIC5oRFIoLQ4tSgvMTcVqM_C19HJJ9BX19vLwtNC1zDC0dRCNzzc2dcw2FCpFgBXRb-1XQAAAA.6Ym224Vwr9AniBeq6gL8OM9u4vGnUB_vbEUVjWojg14',
                   Timeout__c = 120000
                   );
    }

}