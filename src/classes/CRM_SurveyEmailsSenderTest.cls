/**
 * @File Name          : CRM_SurveyEmailsSenderTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 1/7/2020, 3:19:04 PM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    1/7/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest 
public with sharing class CRM_SurveyEmailsSenderTest {

    @testSetup static void setup() {
        // Create common test accounts
         Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Email__c ='test@test.com';
        objAcc.Email__pc ='test@test.com';
        
        objAcc.Mobile_Phone_Encrypt__pc = '0097405883798';
        insert objAcc ;
        
        Id recTypeCOD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeCOD );
        objCase.Type = 'Change of Contact Details';
        //objCase.IsPOA__c = false;
        //objCase.OCR_verified__c = true;
        //objCase.OQOOD_Fee_Applicable__c = false;
        //objCase.Approval_Status__c = 'Approved' ;
        objCase.Status = 'Working';
        
                                         
        insert objCase;
        /*
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        objSR.Agency__c = objAcc.Id ;
        insert objSR ;

        Id dealId = objSR.Deal_ID__c;
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id,1 );
        insert lstBookings ; 

        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings,1 );
        lstBookingUnits[0].Registration_ID__c = '1520';
        insert lstBookingUnits;

        List<SR_Booking_Unit__c> listSBU = new List<SR_Booking_Unit__c>();
        listSBU = TestDataFactory_CRM.createSRBookingUnis(objCase.ID,lstBookingUnits);
        insert listSBU;    
        */
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
        OnOffCheck__c = false);

        settingLst2.add(newSetting1);
        insert settingLst2;

        Id recordTypeIdWlc = [SELECT Id
            FROM RecordType
            WHERE SObjectType='Calling_List__c' 
            AND DeveloperName='Walk_In_Calling_List'
            AND IsActive = TRUE LIMIT 1].Id;

        Calling_List__c objCalling_List = new Calling_List__c(
            RecordTypeId = recordTypeIdWlc,
            Calling_List_Status__c = 'New',
            Account__c = objAcc.Id,
            //Booking_Unit__c = lstBookingUnits[0].Id,
            Type__c = 'Collections'
        );
        insert objCalling_List;
        System.debug('-->> objTask : ' + objCalling_List );
        // Creation of Email Message record
        EmailMessage newEmail = new EmailMessage();
        newEmail.FromAddress = 'test@test.com';
        newEmail.FromName = 'Test Sender';
        newEmail.ToAddress = 'test@test.com';
        newEmail.Subject = 'Test Subject';
        newEmail.TextBody = 'Test Body';
        newEmail.ParentId = objCase.Id;
        newEmail.Status = '0';
        newEmail.Incoming = true;
        newEmail.CCAddress= 'test@test.com';
        insert newEmail; 
    }


    @IsTest
    static void testEmailCaseSurvey(){

        Case  objCase  = [SELECT Id FROM Case LIMIT 1];
        //objCase.POP_Rejection_Reason__c = 'Insufficient details of proof of payment date of transfer requiried by the client';
        //objCase.Total_Amount__c = 500;
        //objCase.Payment_Mode__c = 'BANK TRANSFER';
        //update objCase;
        Account objAcc = [ SELECT Id from Account Limit 1];
        EmailMessage  emailMessage = [ SELECT Id, CCAddress, ParentId FROM EmailMessage LIMIT 1];

        List<Id> lstCaseId = new List<Id>();
        lstCaseId.add(objCase.Id);
        
        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
        

        Test.startTest();
        CRM_SurveyEmailsSender.sendSurveyEmails(lstCaseId);
        Test.stopTest();
        
    }

    @IsTest
    static void callingListWalkinSendingClosureEmail(){

        Calling_List__c objCalling_List  = [SELECT Id FROM Calling_List__c LIMIT 1];
        //objCase.POP_Rejection_Reason__c = 'Insufficient details of proof of payment date of transfer requiried by the client';
        //objCase.Total_Amount__c = 500;
        //objCase.Payment_Mode__c = 'BANK TRANSFER';
        //update objCase;
        Account objAcc = [ SELECT Id from Account Limit 1];
        EmailMessage  emailMessage = [ SELECT Id, CCAddress, ParentId FROM EmailMessage LIMIT 1];


        List<Id> lstCaseId = new List<Id>();
        lstCaseId.add(objCalling_List.Id);
        
        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
        

        Test.startTest();
        CRM_SurveyEmailsSender.sendSurveyEmails(lstCaseId);
        Test.stopTest();
        
    }
}