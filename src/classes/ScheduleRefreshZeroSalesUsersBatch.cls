/****************************************************************************************************
* Name          : ScheduleRefreshZeroSalesUsersBatch                                                *
* Description   : Scheduler to invoke the CreateActionPlanBatch class                               *
* Created Date  : 08/04/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                *
* 1.0         Craig Lobo    08/04/2018      Initial Draft.                                          *
****************************************************************************************************/
global class ScheduleRefreshZeroSalesUsersBatch implements Schedulable {
    global void execute(SchedulableContext SC) {
        RefreshZeroSalesUsersBatch zsUserBatch = new RefreshZeroSalesUsersBatch();
       database.executebatch(zsUserBatch);
   }
}