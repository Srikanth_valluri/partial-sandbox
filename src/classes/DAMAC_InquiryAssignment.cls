public class DAMAC_InquiryAssignment {

    public Map <Integer, List <ID>> queueWithInqMap { get; set; }
    public Map <ID, String> queueDetailsMap { get; set; }
    public List <Inquiry__c> topInquires { get; set; }
    public List <SelectOption> queueDropDowns { get; set; } 
    public List <Inquiry__c> userRelatedInquiries { get; set; }
    public Boolean showQueueDetails { get; set; }
    public Inquiry__c inq { get; set; }
    public ID queueId { get; set; }
    public Boolean enablePolling { get; set; }
    public ID apexJobID;
    public DAMAC_InquiryAssignment (){
        userRelatedInquiries = new List <Inquiry__c> ();
        queueDetailsMap = new Map <ID, String> ();
        queueWithInqMap = new Map <Integer, List <ID>> ();
        topInquires = new List <Inquiry__c> ();
        inq = new Inquiry__c ();
        queueDropDowns = new List <SelectOption> ();
        queueDropDowns.add (new SelectOption ('', '--None--'));
        queueId = NULL;
        apexJobID = NULL;
        enablePolling = false;
        showQueueDetails = true;
        searchByQueue ();
        getTopInquires ();
    }
    
    public void getTopInquires () {
        topInquires = [SELECT Name, Owner.name, Inquiry_source__c, RecordTypeName__c, 
                    Campaign__r.name, Inquiry_Status__c 
                    FROM Inquiry__c WHERE Inquiry_Assignment_Rules__c != NULL order By CreatedDate Desc limit 5 ];
    }
    public void getUserRelatedInquires () {
        showQueueDetails = false;
        System.Debug (inq.OwnerID);
        userRelatedInquiries = [SELECT Name, Owner.name, Inquiry_source__c, RecordTypeName__c, 
                    Campaign__r.name, Inquiry_Status__c 
                    FROM Inquiry__c WHERE Inquiry_Assignment_Rules__c != NULL 
                       AND OwnerID =: inq.OwnerID 
                       Limit 50
                       ];
    }
    
    public void searchByQueue () {
        System.Debug (queueID);
        queueDetailsMap = new Map <ID, String> ();
        queueWithInqMap = new Map <Integer, List <ID>> ();
        List <Inquiry_Assignment_Rules__c> rules = new List <Inquiry_Assignment_Rules__c> ();
        showQueueDetails = true;
        if (inq.OwnerID != NULL) {
            getUserRelatedInquires ();
        }
        else {
            if (queueId == NULL)
                rules = [ SELECT OwnerID, On_Create_Queue_Id__c, Owner.name FROM Inquiry_Assignment_Rules__c WHERE OwnerID != NULL];
            else {
                rules = [ SELECT OwnerID, On_Create_Queue_Id__c, Owner.name FROM Inquiry_Assignment_Rules__c WHERE OwnerID =: queueId];
            }
            Set <ID> tempQueueIds = new Set <ID> ();
            for (Inquiry_Assignment_Rules__c rule : rules) {                
                if (String.valueOF (rule.OwnerID).startsWith ('00G')) {
                    queueDetailsMap.put (rule.OwnerID, rule.Owner.Name);
                }
                if (rule.On_Create_Queue_Id__c != NULL) {
                    if (String.valueOF (rule.On_Create_Queue_Id__c).startsWith ('00G')) {
                        tempQueueIds.add (rule.On_Create_Queue_Id__c);
                    }
                }
                
            }
            for (Group g : [select Name FROM Group WHERE ID IN :tempQueueIds] ){
                queueDetailsMap.put (g.ID, g.Name);
            }
            for (ID key : queueDetailsMap.keySet ())
                queueDropDowns.add (new selectOption (key, queueDetailsMap.get (key)));
            
            
            List <Integer> sortedIndex = new List <Integer> ();
            Map <Integer, List <ID>> queueRelatedInqMap = new Map <Integer, List <ID>> ();
            for (AggregateResult res : [SELECT Count (ID) total, OwnerID FROM Inquiry__c 
                                        WHERE  OwnerID IN :queueDetailsMap.keySet ()
                                        AND Inquiry_Assignment_Rules__c != NULL
                                        GROUP BY OwnerID LIMIT 10000]) 
            {
                if (!queueRelatedInqMap.containsKey (Integer.valueOf (res.get ('total')))) {
                    queueRelatedInqMap.put (Integer.valueOf (res.get ('total')), new List <ID> {String.valueOf (res.get ('OwnerId'))});
                } else {
                    queueRelatedInqMap.get (Integer.valueOf (res.get ('total'))).add (String.valueOf (res.get ('OwnerId')));
                } 
                sortedIndex.add (Integer.valueOf (res.get ('total')));                      
            }
            System.Debug (queueRelatedInqMap);
            sortedIndex.sort ();
            
            for (Integer i:sortedIndex) {
                queueWithInqMap.put (i, queueRelatedInqMap.get (i));
            }
            System.Debug (queueWithInqMap);
        }
    }
    public void checkjobStatus () {
        System.Debug (apexJobID);
        AsyncApexJob job = new AsyncApexJob ();
        String jobStatus = '';
        if (apexJobID != NULL) {
            job = [Select Status From AsyncApexJob where ID =: apexJobID order by CreatedDate DESC limit 1];
            jobStatus = job.Status;
        }
        else {
            jobStatus = 'Completed';
        }
        enablePolling = true;
        System.Debug (job.status);
        if (jobStatus == 'Completed') {                
            enablePolling = false;        
        }
        if (Test.isRunningTest ()) {
            jobStatus = 'Failed';
        }
        if (jobStatus == 'Failed') {                
            enablePolling = false;        
        }
    }
    public void executeAssignment () {
        String queueID = apexpages.currentpage().getparameters().get('queueID');
        String skipLimits = apexpages.currentpage().getparameters().get('skipLimits');
        Boolean skipPowerLine = false;
        if (skipLimits.toLowerCase () == 'true') {
            skipPowerLine = true;
        }
        if (!Test.isRunningTest ())
            apexJobID = Database.executeBatch (new DAMAC_InquiryAssignmentBatch (queueID, skipPowerLine), 1);
        enablePolling = true;
        searchByQueue ();
    }
    public static string getAllfields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        if (objectType == null) {
            return fields;
        }
        for (string f :objectType.getDescribe ().fields.getMap ().keySet ())
            fields += f+ ', ';
        return fields.removeEnd(', ');         
    }
    public void executeReAssignmentLogic () {
        try {
            List <Inquiry__c> toInsert = new List <Inquiry__c> ();
            List <Inquiry__c> toInsertInquiries = new List <Inquiry__c> ();
            String ownerName = 'Inquiry_Wealth_Consultants_Overflow';
            String ownerName2 = 'Inquiry_Property_Consultants_Overflow';
            String ownerId = '';
            String ownerId2 = '';
            
            if (!Test.isRunningTest ()) {
                ownerId = [SELECT Id from Group where developerName =: ownerName AND Type = 'Queue' LIMIT 1].ID;
                ownerId2 = [SELECT Id from Group where developerName =: ownerName2 AND Type = 'Queue' LIMIT 1].ID;                
            }
            
            String queryCondition = 'SELECT '+getAllFields ('Inquiry__c')+' FROM Inquiry__c where OwnerId =: ownerId LIMIT 1';
            toInsert = Database.query(queryCondition);
            if (toinsert.size () > 0) {
                PreInquiryGetEligiblePCS getPcs = new PreInquiryGetEligiblePCS();
                toInsert = getPcs.getEligiblsPCS(toInsert, 'Create');        
                system.debug(toInsert);
                update toInsert;
            }
            
            toInsertInquiries = Database.Query ('SELECT '+getAllFields ('Inquiry__c')+' FROM Inquiry__c where OwnerId =: ownerId2 LIMIT 1');
            
            if (toInsertInquiries.size () > 0) {
                InquiryGetEligiblePCS getPcs = new InquiryGetEligiblePCS ();
                toInsertInquiries = getPcs.getEligiblsPCS(toInsertInquiries);        
                system.debug(toInsertInquiries);
                update toInsertInquiries;
            }
            
            
            
        }
        catch (Exception e) {}
    }
}