@isTest(seeAlldata = true)
public class UpdateContactDetailsController_Test {
    
    static testmethod void m1(){
       Buyer__c buy = [Select id from Buyer__c limit 1];
       Apexpages.currentPage().getParameters().put('id', buy.Id);
       UpdateContactDetailsController obj = new UpdateContactDetailsController();
       obj.email = 'test@test.com';
       obj.con.Phone_Country_Code__c = 'United Arab Emirates: 00971';
       obj.Phone = '124324254';
       obj.saveDetails();
    }

}