/**
 * Description: Class used to update the Owner of Parent Handover Case and related Calling List.
 */
public class UpdateHoEhoAndRelatedClOwnerHandler  {
    public static final map<String, Id> mapRecordTypeId ;
    static {
        mapRecordTypeId = new map<String, Id>();
        for(RecordType recTypeInst : [SELECT Id, DeveloperName 
                                        FROM RecordType
                                       WHERE SobjectType = 'Case']
        ) {
            mapRecordTypeId.put(recTypeInst.DeveloperName, recTypeInst.Id);
        }
    }

    /**
     * Method used to update the Owner of Parent Handover Case and related Calling List when the
     * Handover and Early Handover Case Owner changes.
     */
    public void updateOwnerOfCallingList(Map<Id, Case> newMapCases, Map<Id, Case> oldMapCases) {
        Id idEHRecordType = mapRecordTypeId.get('Early_Handover');
        Id idHandoverRecordType = mapRecordTypeId.get('Handover');
        List<Case> lstEarlyHandoverCases = new List<Case>();
        List<Case> lstHandoverCases = new List<Case>();
        Map<String, Id> mapCallingListRecordTypes = new Map<String, Id>();
        
        // Fetch the Calling List Record Types
        List<RecordType> lstCallinglistRecordTypes = [SELECT Id, DeveloperName from RecordType where SobjectType = 'Calling_List__c'];
        
        if (!lstCallinglistRecordTypes.isEmpty()) {
            for (RecordType objRecordType : lstCallinglistRecordTypes) {
                mapCallingListRecordTypes.put(objRecordType.DeveloperName, objRecordType.Id);
            }       
        }


        // Iterate over the Cases and find out HO and EHO type of Cases
        for (Id objCaseId : newMapCases.keySet()) {
            Case newCase = newMapCases.get(objCaseId);
            Case oldCase = oldMapCases.get(objCaseId);
            String strOwnerId = newCase.OwnerId;

            // Checking if the Owner of Case is changed and current Owner is User.
            if (newCase.RecordTypeId != null && newCase.RecordTypeId == idEHRecordType && newCase.OwnerId != oldCase.OwnerId && strOwnerId.startsWith('005')) {
                lstEarlyHandoverCases.add(newCase);
            } else if (newCase.RecordTypeId != null && newCase.RecordTypeId == idHandoverRecordType && newCase.OwnerId != oldCase.OwnerId && strOwnerId.startsWith('005')) {
                lstHandoverCases.add(newCase);
            }
        }

        // Update the Calling List related to EHO Cases
        if (!lstEarlyHandoverCases.isEmpty()) {
            List<Calling_List__c> lstCallingList = [SELECT Id, OwnerId, Case__c FROM Calling_List__c WHERE Case__c IN: lstEarlyHandoverCases AND RecordTypeId =: mapCallingListRecordTypes.get('Early_Handover_Calling_List') ];
            List<Calling_List__c> lstCallingListToUpdate = new List<Calling_List__c>();

            if (!lstCallingList.isEmpty()) {
                for (Calling_List__c objCallingList : lstCallingList) {
                    if (newMapCases.containsKey(objCallingList.Case__c) && newMapCases.get(objCallingList.Case__c).OwnerId != null) {
                        objCallingList.OwnerId = newMapCases.get(objCallingList.Case__c).OwnerId;
                        lstCallingListToUpdate.add(objCallingList);
                    }
                }
                if (!lstCallingListToUpdate.isEmpty()) {
                    update lstCallingListToUpdate;
                }
            }
        }

        // Update the Calling List related to HO Cases and Parent HO Case
        if (!lstHandoverCases.isEmpty()) {
            List<Calling_List__c> lstHOCallingListToUpdate = new List<Calling_List__c>();
            List<Calling_List__c> lstHOChildCallingListToUpdate = new List<Calling_List__c>();
            Map<Id, List<Calling_List__c>> mapParentCLToChildCL = new Map<Id, List<Calling_List__c>>();
            Set<String> setParentCL = new Set<String>();
            Map<Id, Id> mapParentHandoverToChildHandover = new Map<Id, Id>();
            List<Case> lstParentCasesToUpdate = new List<Case>();

            List<Calling_List__c> lstCallingListHandover = [SELECT Id, OwnerId, Case__c, Calling_List__c FROM Calling_List__c WHERE Case__c IN: lstHandoverCases AND RecordTypeId =: mapCallingListRecordTypes.get('Handover_Calling_List')];

            // Updating the parent CL
            if (!lstCallingListHandover.isEmpty()) {
                for (Calling_List__c objCallingList : lstCallingListHandover) {
                    if (newMapCases.containsKey(objCallingList.Case__c) && newMapCases.get(objCallingList.Case__c).OwnerId != null) {
                        objCallingList.OwnerId = newMapCases.get(objCallingList.Case__c).OwnerId;
                        lstHOCallingListToUpdate.add(objCallingList);
                    }
                }
                if (!lstHOCallingListToUpdate.isEmpty()) {
                    update lstHOCallingListToUpdate;
                }
            }

            // Updating the Parent Handover Case
            for (Case objCase : lstHandoverCases) {
                if (objCase.ParentId != null) {
                    mapParentHandoverToChildHandover.put(objCase.ParentId, objCase.Id);
                }
            }
            system.debug( '.mapParentHandoverToChildHandover : <<<<<<<<<< : ' + mapParentHandoverToChildHandover);
            if (!mapParentHandoverToChildHandover.isEmpty()) {
                //List<Case> lstParentCases = [SELECT OwnerId FROM Case WHERE Id IN: mapParentHandoverToChildHandover.keySet()];

                for (Id parentCaseId : mapParentHandoverToChildHandover.keySet()) {
                    if (newMapCases.containsKey(mapParentHandoverToChildHandover.get(parentCaseId)) && newMapCases.get(mapParentHandoverToChildHandover.get(parentCaseId)).OwnerId != null) {
                        Case objCase = new Case(Id = parentCaseId);
                        objCase.OwnerId = newMapCases.get(mapParentHandoverToChildHandover.get(objCase.Id)).OwnerId;
                        lstParentCasesToUpdate.add(objCase);            
                    }
                }
                system.debug( '.lstParentCasesToUpdate : <<<<<<<<<< : ' + lstParentCasesToUpdate);
                if (!lstParentCasesToUpdate.isEmpty()) {
                    update lstParentCasesToUpdate;
                }
            }
            
            /*
            // Updating the child CL
            if (!lstHOCallingListToUpdate.isEmpty()) {
                List<Calling_List__c> lstChildCallingListHandover = [SELECT Id, OwnerId, Case__c, Calling_List__r.Case__c FROM Calling_List__c WHERE Calling_List__c IN: lstHOCallingListToUpdate AND RecordTypeId =: mapCallingListRecordTypes.get('Appointment_Scheduling')];
                
                if (!lstChildCallingListHandover.isEmpty()) {
                    for (Calling_List__c objCL : lstChildCallingListHandover) {
                        if (mapParentCLToChildCL.containsKey(objCL.Calling_List__c)) {
                            List<Calling_List__c> lstTempCallingList = mapParentCLToChildCL.get(objCL.Calling_List__c);
                            lstTempCallingList.add(objCL);
                            mapParentCLToChildCL.put(objCL.Calling_List__c, lstTempCallingList);
                        } else {
                            mapParentCLToChildCL.put(objCL.Calling_List__c, new List<Calling_List__c> {objCL});
                        }
                    }
                }

                if (!mapParentCLToChildCL.isEmpty()) {
                    for (Id objParentCLId : mapParentCLToChildCL.keySet()) {
                        List<Calling_List__c> lstChildCallingList = mapParentCLToChildCL.get(objParentCLId);
                        if (!lstChildCallingList.isEmpty()) {
                            for (Calling_List__c objCallingList : lstChildCallingList) {
                                if (newMapCases.containsKey(objCallingList.Calling_List__r.Case__c) && newMapCases.get(objCallingList.Calling_List__r.Case__c).OwnerId != null) {
                                    objCallingList.OwnerId = newMapCases.get(objCallingList.Calling_List__r.Case__c).OwnerId;
                                    lstHOChildCallingListToUpdate.add(objCallingList);
                                }
                            }
                        }
                    }
                    if (!lstHOChildCallingListToUpdate.isEmpty()) {
                        update lstHOChildCallingListToUpdate;
                    }
                }
            }*/
        }
    }
}