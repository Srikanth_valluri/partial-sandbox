/**
 * @File Name          : RoboCallAPIs.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 8/29/2019, 3:15:27 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/29/2019, 3:15:27 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public with sharing class RoboCallAPIs {

    public static String Key = Label.Robocall_Key;
    public static String Authorization = Label.Robocall_Authorization;
    public static String createCampaignError;

    //public static PhoneBookResponse uploadPhoneBook(String phoneBookName, String phoneNumbers) {
    //    JSONGenerator gen = JSON.createGenerator(true);
	   // gen.writeStartObject();
	   // gen.writeStringField('phonebookname', phoneBookName);
	   // gen.writeStringField('numbers', phoneNumbers);
	   // gen.writeEndObject();
	   // String jsonS = gen.getAsString();

    //    HTTPResponse response = getResponse(Label.RC_Upload_Phonebook, jsonS);

    //    system.debug('!!!!response'+response.getBody());
    //    PhoneBookResponse objPB = new PhoneBookResponse();
    //    if (!string.isBlank(response.getBody())&& response.getStatusCode() == 200){
    //       objPB = PhoneBookResponse.parsePBResponse(response.getBody());
    //    }
    //    system.debug('!!!!objPB'+objPB);
    //    return objPB;
    //}

    public static AddCallResponse addCalls(string orderId, string additionalNumbers) {

        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('order_id', orderId);
        gen.writeStringField('additional_number', additionalNumbers);
        gen.writeEndObject();
        String jsonS = gen.getAsString();
        system.debug('!!!!jsonS'+jsonS);

        HTTPResponse response = getResponse(Label.RC_Add_Calls, jsonS);
        system.debug('!!!!response'+response.getBody());

        AddCallResponse objAC = new AddCallResponse();
        if (!string.isBlank(response.getBody())&& response.getStatusCode() == 200){
           objAC = AddCallResponse.parseACResponse(response.getBody());
        }
        system.debug('!!!!objAC'+objAC);

        return objAC;
    }

    public static CampaignResponse CreateCampaign(campaignWrapper objWrap) {
        String jsonS = JSON.serialize(objWrap);

        HTTPResponse response = getResponse(Label.RC_Campaign, jsonS);
        system.debug('!!!!response'+response.getBody());
        createCampaignError = response.getBody();
        system.debug('!!!!createCampaignError'+createCampaignError);
        CampaignResponse objCR = new CampaignResponse();
        if (!string.isBlank(response.getBody())&& response.getStatusCode() == 200){
           objCR = CampaignResponse.parseCampResponse(response.getBody());
        }
        system.debug('!!!!objCR'+objCR);
        return objCR;
    }

    public static HTTPResponse getResponse (String endpoint, String jsonBody) {

        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('POST');
        req.setHeader('Accept','application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('x-api-key', Key);
        req.setHeader('Authorization', Authorization);
        req.setbody(jsonBody);

        Http http = new Http();
        HTTPResponse response = http.send(req);

        return response;
    }

    public class PhoneBookResponse {
        public list<string> msg;
        public string id;
    }

    public class AddCallResponse {
        public string dnd_count;
        public string message;
        public list<Call_Ids> sivr_call_ids;
    }

    public class Call_Ids {
        public string call_id;
    }

    public class campaignWrapper {
        public string ivr_id;
        public string timezone;
        public string phonebook;
        public string priority;
        public string order_throttling;
        public string retry_duration;
        public string start_time;
        public string max_retry;
        public string call_scheduling;
        public string call_scheduling_start_time;
        public string call_scheduling_stop_time;
        public string k_number;
        public string is_transactional;
        public string additional_number;
    }

    public class CampaignResponse {
        public string order_id;
        public string result;
        public string status_code;
    }

    // public static PhoneBookResponse parsePBResponse (String json) {
    //     return (PhoneBookResponse) System.JSON.deserialize(json, PhoneBookResponse.class);
    // }

    public static AddCallResponse parseACResponse (String json) {
        return (AddCallResponse) System.JSON.deserialize(json, AddCallResponse.class);
    }

    public static CampaignResponse parseCampResponse (String json) {
        return (CampaignResponse) System.JSON.deserialize(json, CampaignResponse.class);
    }
}