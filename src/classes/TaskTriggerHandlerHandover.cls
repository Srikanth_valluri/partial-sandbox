/*******************************************************************************************
* Description - Controller of  GetSnagInformation to get snag information of Case          *
*                                                                                          *
* Version            Date            Author         Description                            *
* 1.0                                               Initial Draft                          *
* 1.1                29/01/2019      Arjun Khatri   Added null check on line no. 10        *
* 1.2                06/01/2020      Shashank Maind Added new method BypassIPMSforDTPCUnits
                                     to bypass IPMS callout for DTPC Tasks                 *
*******************************************************************************************/

public class TaskTriggerHandlerHandover {
    
    public static void checkValidDocuments(map<Id,Task> newMap, map<Id,Task> oldMap) {
        list<Task> listTask = new list<Task>();
        set<Id> setCaseId = new set<Id>();
        //map<Id, set<Id>> setCaseIdWithSetTaskId = new map<Id, set<Id>>();
        
        for (Task objTask : newMap.values()) {
            if (
                    String.isNotBlank(objTask.Subject) 
                    && objTask.Subject.equalsIgnoreCase('Upload All Handover Documents')
                    && String.isNotBlank(objTask.Assigned_User__c )
                    && objTask.Assigned_User__c == 'CDC'
                    && String.isNotBlank(objTask.Status)
                    && objTask.Status.equalsIgnoreCase('Completed')
                    && !oldMap.get(objTask.Id).Status.equalsIgnoreCase('Completed')
                    && String.valueOf(objTask.WhatId).startsWith('500') 
            ) {
                listTask.add(objTask);
                setCaseId.add(objTask.WhatId);
            }
        }
        
        if (!listTask.isEmpty()) {
            //list<Case> listCase = new list<Case>();
            set<Id> setAllCaseId = new set<Id>();
            
            /*listCase = [Select Id,
                               ParentId
                        From Case
                        Where Id IN :setCaseId
                        And RecordType.DeveloperName = 'HANDOVER'
                       ];*/
                       
            for (Case objCase : [Select Id,
                               ParentId
                        From Case
                        Where Id IN :setCaseId
                        And RecordType.DeveloperName = 'HANDOVER'
                       ]) {
                setAllCaseId.add(objCase.Id);
                if (objCase.ParentId != null) {
                    setAllCaseId.add(objCase.ParentId);
                }
            }
            
            if (!setAllCaseId.isEmpty()) {
                list<SR_Attachments__c> listDocument = new list<SR_Attachments__c>();
                
                listDocument = [Select Id
                                From SR_Attachments__c
                                Where Case__c IN :setAllCaseId
                                And (Attachment_URL__c = ''
                                    OR Attachment_URL__c = NULL)
                               ];
                               
                //for (SR_Attachments__c objDocument : listDocument) {
                if (!listDocument.isEmpty()) {
                    listTask[0].addError('Please Upload Documents on placeholders of Handover Case or Handover Parent Case.');
                }
                //}
            }
        }
    }
    
    public void afterUpdate(map<Id,Task> newMap, map<Id,Task> oldMap){
        Integer counterVal;
        set<Id> setTaskCaseId = new set<Id>();
        set<Id> setLocationId = new set<Id>();
        set<Id> setTaskForCallout1 = new set<Id>();
        set<Id> setTaskForCallout2 = new set<Id>();
        set<String> setTaskForUser = new set<String>();
        list<Task> lstTask = new list<Task>();
        map<String, String> mapAttachURL = new map<String, String>();
        system.debug('HO Task update called*****');
        
        if(Label.UsersOutsideSalesforce != null) {
            setTaskForUser.addAll(Label.UsersOutsideSalesforce.split(','));
            system.debug('setTaskForUser==='+setTaskForUser);
            counterVal = 1;
            for(Task taskInst: newMap.values()){
                if(taskInst.Process_Name__c != null
                && taskInst.Assigned_User__c != null
                && taskInst.WhatId != null
                && setTaskForUser.contains(taskInst.Assigned_User__c)
                && (String.valueOf(taskInst.WhatId).startsWith('a1J')
                || String.valueOf(taskInst.WhatId).startsWith('500'))
                && taskInst.Status != oldMap.get(taskInst.Id).Status){
                    if(counterVal <= 100) {
                        setTaskForCallout1.add(taskInst.Id);
                    }
                    else {
                        setTaskForCallout2.add(taskInst.Id);
                    }
                    setTaskCaseId.add(taskInst.WhatId);
                    counterVal++;
                }
            }
            system.debug('setTaskCaseId==='+setTaskCaseId);
            system.debug('setTaskForCallout1==='+setTaskForCallout1);
            system.debug('setTaskForCallout2==='+setTaskForCallout2);
            if(!setTaskForCallout1.isEmpty() && !setTaskCaseId.isEmpty()) {
                createTaskOnIPMS(setTaskForCallout1, setTaskCaseId);
            }
            if(!setTaskForCallout2.isEmpty() && !setTaskCaseId.isEmpty()) {
                createTaskOnIPMS(setTaskForCallout2, setTaskCaseId);
            }
        }
    }
    
    public void taskForExternalUser(list<Task> listTask) {
        Integer counterVal;
        set<Id> setTaskCaseId = new set<Id>();
        set<Id> setLocationId = new set<Id>();
        set<Id> setTaskForCallout1 = new set<Id>();
        set<Id> setTaskForCallout2 = new set<Id>();
        set<String> setTaskForUser = new set<String>();
        list<Task> lstTask = new list<Task>();
        map<String, String> mapAttachURL = new map<String, String>();
        system.debug('Inside handler HO*****');

        listTask = BypassIPMSforDTPCUnits(listTask);    //To filter out DTPC unit Task for bypassing IPMS callout for those;
        System.debug('listTask after filtering: '+listTask);
        
        if(Label.UsersOutsideSalesforce != null) {
            setTaskForUser.addAll(Label.UsersOutsideSalesforce.split(','));
            system.debug('setTaskForUser==='+setTaskForUser);
            counterVal = 1;
            for(Task taskInst: listTask){
                if(taskInst.Process_Name__c != null
                && taskInst.Assigned_User__c != null
                && taskInst.WhatId != null
                && setTaskForUser.contains(taskInst.Assigned_User__c)
                && (String.valueOf(taskInst.WhatId).startsWith('a1J')
                || String.valueOf(taskInst.WhatId).startsWith('500'))){
                    if(counterVal <= 100) {
                        setTaskForCallout1.add(taskInst.Id);
                    }
                    else {
                        setTaskForCallout2.add(taskInst.Id);
                    }
                    setTaskCaseId.add(taskInst.WhatId);
                    counterVal++;
                }
            }
            system.debug('setTaskCaseId==='+setTaskCaseId);
            system.debug('setTaskForCallout1==='+setTaskForCallout1);
            system.debug('setTaskForCallout2==='+setTaskForCallout2);
            if(!setTaskForCallout1.isEmpty() && !setTaskCaseId.isEmpty()) {
                createTaskOnIPMS(setTaskForCallout1, setTaskCaseId);
            }
            if(!setTaskForCallout2.isEmpty() && !setTaskCaseId.isEmpty()) {
                createTaskOnIPMS(setTaskForCallout2, setTaskCaseId);
            }
        }
    }

    public static List<Task> BypassIPMSforDTPCUnits(list<Task> listTask) {
        Set<Id> setCaseId = new Set<Id>();
        Set<Id> setTaskId = new Set<Id>();
        List<Task> lstNonDTPCTask = new List<Task>();
        List<Task> lstDTPCTaskToUpdate = new List<Task>();
        Map<id, String> mapCaseIdToUnitName = new Map<id, String>();

        for(Task objT : listTask) {
            if(objT.WhatId != null && String.valueOf(objT.WhatId).startsWith('500')) {
                setCaseId.add(objT.WhatId);
            }
            else{
                lstNonDTPCTask.add(objT);
            }
    
        }

        if(setCaseId.size() > 0) {

            List<Case> lstCase = [SELECT id
                                       , Booking_Unit__c
                                       , Booking_Unit__r.Unit_Name__c
                                  FROM Case 
                                  WHERE id IN : setCaseId];
            System.debug('lstCase: '+lstCase);

            for(Case objCase : lstCase) {
                if(objCase.Booking_unit__c == null) {
                    continue;
                }
                mapCaseIdToUnitName.put(objCase.id, objCase.Booking_Unit__r.Unit_Name__c);
            }
            System.debug('mapCaseIdToUnitName: '+mapCaseIdToUnitName);

            if(mapCaseIdToUnitName.size() > 0) {
                for(Task objTask : listTask) {
    
                    if(objTask.Subject == Label.HO_Early_Settlement_FM_Task && String.valueOf(mapCaseIdToUnitName.get(objTask.WhatId)).startsWith('DTPC')) {
                        System.debug('objTask UnitName: '+String.valueOf(mapCaseIdToUnitName.get(objTask.WhatId)).startsWith('DTPC'));
                       
                        setTaskId.add(objTask.Id);
                    }
                    else {
                        lstNonDTPCTask.add(objTask);
                    }
                }

            }
        }

        if(setTaskId.size() > 0) {
            List<Task> lstTsk = [SELECT id
                                      , Status
                                 FROM Task
                                 WHERE id IN : setTaskId];
            System.debug('lstTsk : '+lstTsk);

            for(Task tsk : lstTsk) {
                tsk.Status = 'Completed';
            }
            System.debug('lstTsk after status Update::' + lstTsk);
            //Perform Update on lstTsk
            Update lstTsk;
        }

        return lstNonDTPCTask;
        //return new List<Task>();
    
    }
    
    @future(callout=true)
    public static void createTaskOnIPMS(set<Id> setTaskForCallout, set<Id> setTaskLocationId) {
        system.debug('Task cfreation called********************'+setTaskLocationId);
        list<Error_Log__c> listErrorLog = new list<Error_Log__c>();
        
        map<Id, Location__c> mapLocationDetails = new map<Id, Location__c>([select Id, Building_Name__c,
                            Location_Code__c, Location_ID__c, Location_Type__c
                            from Location__c where Id IN: setTaskLocationId]);
        system.debug('!!!!!!!!!!!mapLocationDetails'+mapLocationDetails);
        
        map<Id, Case> mapCaseDetails = new map<Id, Case>([select Id, Status, Buyer__r.Party_ID__c,Excess_Amount__c,
                            Booking_Unit__r.Registration_ID__c, CaseNumber, Relationship_with_Seller__c,Additional_Details__c,
                            Case_Type__c, Buyer_Type__c, Booking_Unit__r.Unit_Details__c,
                            Booking_Unit__r.Rental_Pool__c, Purpose_of_POA__c, Purpose_of_POA_Seller__c,
                            Booking_Unit__r.Inventory__r.Property_City__c, Buyer__r.First_Name__c,
                            Booking_Unit__r.Inventory__r.Property_Status__c, Buyer__r.Last_Name__c,
                            Booking_Unit__r.Inventory__r.Property_Country__c, Buyer__r.Nationality__c,
                            Booking_Unit__r.Inventory__r.Property__r.DIFC__c, Buyer__r.Passport_Number__c,
                            Buyer__r.Passport_Expiry_Date__c, Buyer__r.IPMS_Buyer_ID__c, Seller__r.Party_ID__c,
                            Seller__r.FirstName, Seller__r.LastName, Seller__r.Nationality__pc,
                            AccountId, Account.Party_ID__c, Booking_Unit__c, Seller__c, NewPaymentTermJSON__c
                            from Case where Id IN: setTaskLocationId]);
        
        list<Task> lsTaskToUpdate = new list<Task>();
        for(Task objTask : [Select t.WhoId
                                 , t.WhatId
                                 , t.Type
                                 , t.Status
                                 , t.OwnerId
                                 , t.Id
                                 , t.Subject
                                 , t.CreatedDate
                                 , t.Description
                                 , t.Assigned_User__c
                                 , t.ActivityDate
                                 , t.Owner.Name
                                 , t.Task_Error_Details__c
                                 , t.Pushed_to_IPMS__c
                                 , t.Process_Name__c
                                 , t.Document_URL__c                                                               
                                 From Task t
                                 where t.Id IN : setTaskForCallout]){

            list<TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5> listObjBeans = 
            new list<TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5>();
            
            TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objHeaderBean = 
            new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
            String reqNo = '2-'+ string.valueOf(System.currentTimeMillis());
            if(String.valueOf(objTask.WhatId).startsWith('a1J')){
                objHeaderBean.PARAM_ID = '2-'+mapLocationDetails.get(objTask.WhatId).Location_ID__c;
            }else if(String.valueOf(objTask.WhatId).startsWith('500')){
                objHeaderBean.PARAM_ID = '2-'+mapCaseDetails.get(objTask.WhatId).CaseNumber;
            }
            objHeaderBean.ATTRIBUTE1 = 'HEADER';
            //objHeaderBean.ATTRIBUTE2 = objTask.Process_Name__c;
            objHeaderBean.ATTRIBUTE2 = 'HANDOVER';
            objHeaderBean.ATTRIBUTE3 = objTask.Status;
            system.debug('objTask.Status*****'+objTask.Status);
            objHeaderBean.ATTRIBUTE4 = objTask.Owner.Name;
            //objHeaderBean.ATTRIBUTE5 = mapCaseDetails.get(objTask.WhatId).Account.Party_ID__c;
            if(String.valueOf(objTask.WhatId).startsWith('a1J')){
                objHeaderBean.ATTRIBUTE5 = '0';       
            }else if(String.valueOf(objTask.WhatId).startsWith('500')){
                objHeaderBean.ATTRIBUTE5 = mapCaseDetails.get(objTask.WhatId).Account.Party_ID__c;
            }
            if(String.valueOf(objTask.WhatId).startsWith('a1J')){
                objHeaderBean.ATTRIBUTE6 = '';
            }else if(String.valueOf(objTask.WhatId).startsWith('500')){
                objHeaderBean.ATTRIBUTE6 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Registration_ID__c;
            }
            objHeaderBean.ATTRIBUTE7 = String.valueOf(objTask.CreatedDate.format('dd-MMM-yyyy').toUpperCase()); // format this as DD-MON- YYYY
            system.debug('date*****'+String.valueOf(objTask.CreatedDate.format('dd-MMM-yyyy').toUpperCase()));
            objHeaderBean.ATTRIBUTE8 = '';
            objHeaderBean.ATTRIBUTE9 = '';
            objHeaderBean.ATTRIBUTE10 = '';//set new Buyer Id here instead of objTask.WhatId;
            objHeaderBean.ATTRIBUTE11 = objTask.WhatId;
            objHeaderBean.ATTRIBUTE12 = '';
            objHeaderBean.ATTRIBUTE13 = '';
            objHeaderBean.ATTRIBUTE14 = '';
            objHeaderBean.ATTRIBUTE15 = '';
            objHeaderBean.ATTRIBUTE16 = '';
            objHeaderBean.ATTRIBUTE17 = '';
            objHeaderBean.ATTRIBUTE18 = '';
            objHeaderBean.ATTRIBUTE19 = '';
            objHeaderBean.ATTRIBUTE20 = '';
            objHeaderBean.ATTRIBUTE21 = '';
            objHeaderBean.ATTRIBUTE22 = '';
            objHeaderBean.ATTRIBUTE23 = '';
            objHeaderBean.ATTRIBUTE24 = '';
            objHeaderBean.ATTRIBUTE25 = '';
            objHeaderBean.ATTRIBUTE26 = '';
            objHeaderBean.ATTRIBUTE27 = '';
            objHeaderBean.ATTRIBUTE28 = '';
            objHeaderBean.ATTRIBUTE29 = '';
            objHeaderBean.ATTRIBUTE30 = '';
            objHeaderBean.ATTRIBUTE31 = '';
            objHeaderBean.ATTRIBUTE32 = '';
            objHeaderBean.ATTRIBUTE33 = '';
            objHeaderBean.ATTRIBUTE34 = '';
            if (objTask.Document_URL__c != null) {
              objHeaderBean.ATTRIBUTE35 = objTask.Document_URL__c;
            }
            objHeaderBean.ATTRIBUTE36 = '';
            objHeaderBean.ATTRIBUTE37 = '';
            objHeaderBean.ATTRIBUTE38 = '';
            objHeaderBean.ATTRIBUTE39 = '';
            //objHeaderBean.ATTRIBUTE40 = '';
            objHeaderBean.ATTRIBUTE41 = '';
            objHeaderBean.ATTRIBUTE42 = '';
            objHeaderBean.ATTRIBUTE43 = '';
            objHeaderBean.ATTRIBUTE44 = '';
            objHeaderBean.ATTRIBUTE45 = '';
            objHeaderBean.ATTRIBUTE46 = '';
            objHeaderBean.ATTRIBUTE47 = '';
            objHeaderBean.ATTRIBUTE48 = '';
            objHeaderBean.ATTRIBUTE49 = '';
            objHeaderBean.ATTRIBUTE50 = '';
            listObjBeans.add(objHeaderBean);
            
            // create TASK bean
            String taskOwner;
            if (objTask.Assigned_User__c == 'Legal') {
                taskOwner = 'LEGAL';
             } else if (objTask.Assigned_User__c == 'Finance') {
                 taskOwner = 'FINANCEEXECUTIVE';
             } else if (objTask.Assigned_User__c == 'AR') {
                 taskOwner = 'FINANCEEXECUTIVE';
             } else if (objTask.Assigned_User__c == 'Contracts') {
                 taskOwner = 'CONTRACTSHEAD';
             } else if (objTask.Assigned_User__c == 'Compliance') {
                 taskOwner = 'COMPLIANCEHEAD';
             } else if (objTask.Assigned_User__c == 'Compliance Manager') {
                 taskOwner = 'COMPLIANCEMANAGER';
             } else if (objTask.Assigned_User__c == 'CDC') {
                 taskOwner = 'CDCEXECUTIVE';
             } else if (objTask.Assigned_User__c == 'Sales Admin') {
                 taskOwner = 'SALESADMINMANAGER';
             } else if (objTask.Assigned_User__c == 'FM') {
                 taskOwner = 'FACILITIESAPPROVAL';
             }
            TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objTaskBean = new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
            if(String.valueOf(objTask.WhatId).startsWith('a1J')){
                objTaskBean.PARAM_ID = '2-'+mapLocationDetails.get(objTask.WhatId).Location_ID__c;
            }else if(String.valueOf(objTask.WhatId).startsWith('500')){
                objTaskBean.PARAM_ID = '2-'+mapCaseDetails.get(objTask.WhatId).CaseNumber;
            }
            objTaskBean.ATTRIBUTE1 = 'TASK';
            objTaskBean.ATTRIBUTE2 = objTask.Subject;
            system.debug('subject*****'+objTask.Subject);
            objTaskBean.ATTRIBUTE3 = objTask.Status;
            system.debug('subject*****'+objTask.Status);
            objTaskBean.ATTRIBUTE4 = taskOwner;
            if(String.valueOf(objTask.WhatId).startsWith('a1J')){
                objTaskBean.ATTRIBUTE5 = '0';       
            }else if(String.valueOf(objTask.WhatId).startsWith('500')){
                objTaskBean.ATTRIBUTE5 = mapCaseDetails.get(objTask.WhatId).Account.Party_ID__c;
            }
            if(String.valueOf(objTask.WhatId).startsWith('a1J')){
                objTaskBean.ATTRIBUTE6 = mapLocationDetails.get(objTask.WhatId).Location_ID__c;
            }else if(String.valueOf(objTask.WhatId).startsWith('500')){
                objTaskBean.ATTRIBUTE6 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Registration_ID__c;
            }
            objTaskBean.ATTRIBUTE7 = String.valueOf(objTask.CreatedDate.format('dd-MMM-yyyy').toUpperCase()); // format this as DD-MON- YYYY
            system.debug('created date*****'+String.valueOf(objTask.CreatedDate.format('dd-MMM-yyyy').toUpperCase()));
            objTaskBean.ATTRIBUTE8 = objTask.Id;
            system.debug('task id*****'+objTask.Id);
            Datetime dt = objTask.ActivityDate;
            objTaskBean.ATTRIBUTE9 = String.valueOf(dt.format('dd-MMM-yyyy').toUpperCase());
            system.debug('due date*****'+String.valueOf(dt.format('dd-MMM-yyyy').toUpperCase()));
            objTaskBean.ATTRIBUTE10 = '';
            objTaskBean.ATTRIBUTE11 = objTask.WhatId;
            objTaskBean.ATTRIBUTE12 = objTask.Subject;
            objTaskBean.ATTRIBUTE13 = objTask.Status;
            objTaskBean.ATTRIBUTE14 = objTask.Type;
            objTaskBean.ATTRIBUTE15 = objTask.Description;
            objTaskBean.ATTRIBUTE16 = objTask.Assigned_User__c;
            if(String.valueOf(objTask.WhatId).startsWith('a1J')){
                objTaskBean.ATTRIBUTE17 = mapLocationDetails.get(objTask.WhatId).Location_ID__c;
            }else{
                objTaskBean.ATTRIBUTE17 = '';
            }
            objTaskBean.ATTRIBUTE18 = '';
            objTaskBean.ATTRIBUTE19 = '';
            if (objTask.Subject == 'Auto transfer excess amount' ) {
                objTaskBean.ATTRIBUTE20 = String.valueOf(mapCaseDetails.get(objTask.WhatId).Excess_Amount__c);
                objTaskBean.ATTRIBUTE21 = mapCaseDetails.get(objTask.WhatId).Additional_Details__c;
            }else {
                objTaskBean.ATTRIBUTE20 = '';
                objTaskBean.ATTRIBUTE21 = '';
            }
            objTaskBean.ATTRIBUTE22 = '';
            objTaskBean.ATTRIBUTE23 = '';
            objTaskBean.ATTRIBUTE24 = '';
            objTaskBean.ATTRIBUTE25 = '';
            objTaskBean.ATTRIBUTE26 = '';
            objTaskBean.ATTRIBUTE27 = '';
            objTaskBean.ATTRIBUTE28 = '';
            objTaskBean.ATTRIBUTE29 = '';
            objTaskBean.ATTRIBUTE30 = '';
            objTaskBean.ATTRIBUTE31 = '';
            objTaskBean.ATTRIBUTE32 = '';
            objTaskBean.ATTRIBUTE33 = '';
            objTaskBean.ATTRIBUTE34 = '';
            objTaskBean.ATTRIBUTE35 = '';
            objTaskBean.ATTRIBUTE36 = '';
            objTaskBean.ATTRIBUTE37 = '';
            objTaskBean.ATTRIBUTE38 = '';
            objTaskBean.ATTRIBUTE39 = '';
            objTaskBean.ATTRIBUTE41 = '';
            objTaskBean.ATTRIBUTE42 = '';
            objTaskBean.ATTRIBUTE43 = '';
            objTaskBean.ATTRIBUTE44 = '';
            objTaskBean.ATTRIBUTE45 = '';
            objTaskBean.ATTRIBUTE46 = '';
            objTaskBean.ATTRIBUTE47 = '';
            objTaskBean.ATTRIBUTE48 = '';
            objTaskBean.ATTRIBUTE49 = '';
            objTaskBean.ATTRIBUTE50 = '';
            listObjBeans.add(objTaskBean);
            
            // create UNIT bean
            if(String.valueOf(objTask.WhatId).startsWith('500')){
                TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objUnitBean = 
                new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
                objUnitBean.PARAM_ID = '2-'+mapCaseDetails.get(objTask.WhatId).CaseNumber;
                objUnitBean.ATTRIBUTE1 = 'UNITS';
                //objUnitBean.ATTRIBUTE2 = objTask.Process_Name__c;
                objUnitBean.ATTRIBUTE2 = 'HANDOVER';
                objUnitBean.ATTRIBUTE3 = '';
                objUnitBean.ATTRIBUTE4 = '';
                objUnitBean.ATTRIBUTE5 = '';
                objUnitBean.ATTRIBUTE6 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Registration_ID__c;
                system.debug('reg*****'+'2-'+mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Registration_ID__c);
                objUnitBean.ATTRIBUTE7 = '';
                objUnitBean.ATTRIBUTE8 = '';
                objUnitBean.ATTRIBUTE9 = '';
                objUnitBean.ATTRIBUTE10 = '';
                objUnitBean.ATTRIBUTE11 = '';
                objUnitBean.ATTRIBUTE12 = '';
                objUnitBean.ATTRIBUTE13 = '';
                objUnitBean.ATTRIBUTE14 = '';
                objUnitBean.ATTRIBUTE15 = '';
                objUnitBean.ATTRIBUTE16 = '';
                objUnitBean.ATTRIBUTE17 = '';
                objUnitBean.ATTRIBUTE18 = '';
                objUnitBean.ATTRIBUTE19 = '';
                objUnitBean.ATTRIBUTE20 = '';
                objUnitBean.ATTRIBUTE21 = '';
                objUnitBean.ATTRIBUTE22 = '';
                objUnitBean.ATTRIBUTE23 = '';
                objUnitBean.ATTRIBUTE24 = '';
                objUnitBean.ATTRIBUTE25 = '';
                objUnitBean.ATTRIBUTE26 = '';
                objUnitBean.ATTRIBUTE27 = '';
                objUnitBean.ATTRIBUTE28 = '';
                objUnitBean.ATTRIBUTE29 = '';
                objUnitBean.ATTRIBUTE30 = '';
                objUnitBean.ATTRIBUTE31 = '';
                objUnitBean.ATTRIBUTE32 = '';
                objUnitBean.ATTRIBUTE33 = '';
                objUnitBean.ATTRIBUTE34 = '';
                objUnitBean.ATTRIBUTE35 = '';
                objUnitBean.ATTRIBUTE36 = '';
                objUnitBean.ATTRIBUTE37 = '';
                objUnitBean.ATTRIBUTE38 = '';
                objUnitBean.ATTRIBUTE39 = '';
                objUnitBean.ATTRIBUTE41 = '';
                objUnitBean.ATTRIBUTE42 = '';
                objUnitBean.ATTRIBUTE43 = '';
                objUnitBean.ATTRIBUTE44 = '';
                objUnitBean.ATTRIBUTE45 = '';
                objUnitBean.ATTRIBUTE46 = '';
                objUnitBean.ATTRIBUTE47 = '';
                objUnitBean.ATTRIBUTE48 = '';
                objUnitBean.ATTRIBUTE49 = '';
                objUnitBean.ATTRIBUTE50 = '';
                listObjBeans.add(objUnitBean);
            }
             
             //create term bean
            if (objTask.Subject == Label.HO_Early_Settlement_Finance_Task) {
                if (!String.isBlank(mapCaseDetails.get(objTask.WhatId).NewPaymentTermJSON__c)) {
                    list<MileStonePaymentDetailsWrapper.REG_TERM_PYMNT_TABLE> lstNewPaymentTerm =
                        (List<MileStonePaymentDetailsWrapper.REG_TERM_PYMNT_TABLE>)JSON.deserialize(mapCaseDetails.get(objTask.WhatId).NewPaymentTermJSON__c, list<MileStonePaymentDetailsWrapper.REG_TERM_PYMNT_TABLE>.class);
                    system.debug('!!!!!!!!!!!lstNewPaymentTerm' + lstNewPaymentTerm);
                    
                    if (lstNewPaymentTerm != null && lstNewPaymentTerm.size() > 0) {
                        for (MileStonePaymentDetailsWrapper.REG_TERM_PYMNT_TABLE objNewTerm : lstNewPaymentTerm) {
                            TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objPPBean = new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
                            objPPBean.PARAM_ID = '2-'+mapCaseDetails.get(objTask.WhatId).CaseNumber;
                            objPPBean.ATTRIBUTE1 = 'TERMS';
                            objPPBean.ATTRIBUTE2 = objNewTerm.INSTALLMENT;
                            objPPBean.ATTRIBUTE3 = objNewTerm.MILESTEON_PERCENT_VALUE;
                            if (!string.isBlank(objNewTerm.DUE_DATE)) {
                                system.debug('!!!!objNewTerm.DUE_DATE'+objNewTerm.DUE_DATE);
                                //Date objdate = date.parse(objNewTerm.DUE_DATE);
                                //system.debug('!!!!!!!objdate'+objdate);
                                //objPPBean.ATTRIBUTE4 = String.valueOf(DateTime.newInstance(objdate.year(),objdate.month(),objdate.day()).format('dd-MMM-yyyy').toUpperCase());
                                objPPBean.ATTRIBUTE4 = getConvertDateTime(objNewTerm.DUE_DATE);
                                system.debug('!!!!!!!objPPBean.ATTRIBUTE4'+objPPBean.ATTRIBUTE4);
                            }
                            objPPBean.ATTRIBUTE5 = objNewTerm.MILESTONE_EVENT;
                            objPPBean.ATTRIBUTE6 = objNewTerm.INVOICE_AMOUNT;
                            objPPBean.ATTRIBUTE7 = objNewTerm.DESCRIPTION;
                            system.debug('!!!!!!!!!!Registration_ID__c'+ mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Registration_ID__c);
                            objPPBean.ATTRIBUTE8 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Registration_ID__c;
                            listObjBeans.add(objPPBean);
                        }
                    }
                }
            }
            
            TaskCreationWSDL.TaskHttpSoap11Endpoint objClass = new TaskCreationWSDL.TaskHttpSoap11Endpoint();
            objClass.timeout_x = 120000;
            String response = objClass.SRDataToIPMSMultiple(reqNo, 'CREATE_SR', 'SFDC', listObjBeans);
            system.debug('resp*****'+response);
            if(String.isNotBlank(response)) {
                innerClass IC = (innerClass)JSON.deserialize(response, innerClass.class);
                system.debug('IC*****'+IC);
                if(IC.status.EqualsIgnoreCase('E')){
                    objTask.Pushed_to_IPMS__c = false;
                    objTask.Task_Error_Details__c = IC.status;
                    system.debug('objTask=='+objTask);
                    Error_Log__c objErr = createErrorLogRecord(objTask.WhatId);
                    objErr.Error_Details__c = IC.message;
                    listErrorLog.add(objErr);
                }else{
                    objTask.Pushed_to_IPMS__c = true;
                }
            }
            else {
                Error_Log__c objErr = createErrorLogRecord(objTask.WhatId);
                objErr.Error_Details__c = 'Error : No Response from IPMS for Task Creation';
                listErrorLog.add(objErr);
            }
            lsTaskToUpdate.add(objTask);
        } // end of for loop
        if(!lsTaskToUpdate.isEmpty()){
            update lsTaskToUpdate;
        }
        if(!listErrorLog.isEmpty()) {
            insertErrorLog(listErrorLog);
        }
    } // end of method    
    public class innerClass{
        public string message;
        public string status;        
        public innerClass(){            
        }
    }
    
    public static Error_Log__c createErrorLogRecord(Id locationId){
        Error_Log__c objErr = new Error_Log__c();
        //objErr.Account__c = accId;
        //objErr.Booking_Unit__c = bookingUnitId;
        //objErr.Case__c = caseId;
        objErr.Location__c = locationId;
        objErr.Process_Name__c = 'Handover';
        return objErr;
    }
    
    public static void insertErrorLog(list<Error_Log__c> listObjErr){
        try{
            insert listObjErr;
        }catch(Exception ex){
            //errorMessage = 'Error : '+ ex.getMessage();
            system.debug('Error Log ex*****'+ex);
        }
    }    

    public static string getConvertDateTime(string strDT) {
        Map<string,string> MapMonthList = new Map<string,string>();
        MapMonthList.put('01','JAN');
        MapMonthList.put('1','JAN');
        MapMonthList.put('02','FEB');
        MapMonthList.put('2','FEB');
        MapMonthList.put('03','MAR');
        MapMonthList.put('3','MAR');
        MapMonthList.put('04','APR');
        MapMonthList.put('4','APR');
        MapMonthList.put('05','MAY');
        MapMonthList.put('5','MAY');
        MapMonthList.put('06','JUN');
        MapMonthList.put('6','JUN');
        MapMonthList.put('07','JUL');
        MapMonthList.put('7','JUL');
        MapMonthList.put('08','AUG');
        MapMonthList.put('8','AUG');
        MapMonthList.put('09','SEP');
        MapMonthList.put('9','SEP');
        MapMonthList.put('10','OCT');
        MapMonthList.put('11','NOV');
        MapMonthList.put('12','DEC');
        //system.debug('strDT'+strDT);
        String[] strDTDivided = strDT.split('/');
        //system.debug('strDTDivided '+strDTDivided.get(1) );
        string month = String.ValueOf(MapMonthList.get(strDTDivided.get(1)));
        string day = strDTDivided.get(0).replace('/', '');
        string year = strDTDivided.get(2);
        
        //string stringDate = year + '-' + month + '-' + day;
        string stringDate = day + '-' + month + '-' + year;
        //system.debug('stringDate '+stringDate );
        return stringDate;
    }

    public void checkESignatureVerification(List<Task> listTask){
        System.debug('Inside checkESignatureVerification');
        Set<Id> caseIds = new Set<Id>();
        
        for(Task task : listTask){
            if(task.Subject == 'Get Key Release Form generated from the Director' && (task.Status == 'Completed' || task.Status == 'Closed')){
                caseIds.add(task.WhatId);
            }
        }
        System.debug('Case Ids--->' +caseIds);

        if(!caseIds.isEmpty()){
            List<Case> caseList = [SELECT Id,E_Signature__c from Case WHERE Id in:caseIds AND RecordTypeName__c = 'Handover' 
                                    and E_Signature__c != 'E-Signature Verified'] ;
            System.debug('Case List--->' +caseList);
            Map<Id,String> caseMap = new Map<Id,String>();
            for(Case caseObj : caseList){
                caseMap.put(caseObj.Id, caseObj.E_Signature__c);
            }
            System.debug('Case Map--->' +caseMap);
            for(Task t : listTask){
                if(caseMap.containsKey(t.WhatId) && (caseMap.get(t.WhatId) != null)){
                    System.debug('ESignature not verified');
                    t.addError('Please get E-Signature Verified before closing the task');
                }
            }
        }
    }
}