/**************************************************************************************************
* Name               : TravelDetailsUpdateUtilEmailBatch
* Description        : Batch class to send Travel Details Notification to Inquiry Owner
* Test Class         : TravelDetailsUpdateUtilEmailTest
* Created Date       : 27/02/2020
* Created By         : Qburst
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR        DATE           Comments
* 1.0         QBurst     27/02/2020        Created.
**************************************************************************************************/

public class TravelDetailsUpdateUtilEmailBatch implements Database.Batchable<sObject>{
    
    /*********************************************************************************************
    * @Description : Implementing the start method of batch interface, contains query.
    * @Params      : Database.BatchableContext
    * @Return      : Database.QueryLocator
    *********************************************************************************************/  
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([SELECT Id, OwnerId, Owner.Email, Name, To_Date__c, Status__c
                                         FROM Travel_Details__c
                                         WHERE To_Date__c < TODAY
                                         AND (Status__c = 'Procured' OR Status__c = 'Finance Approved')]); 
    }
    
    /*********************************************************************************************
    * @Description : Implementing the execute method of batch interface, contains the criteria.
    * @Params      : Database.BatchableContext, List<Travel_Details__c>
    * @Return      : void
    *********************************************************************************************/
    public void execute(Database.BatchableContext BC, List<Travel_Details__c> tdList){
        system.debug('tdList: ' + tdList);
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        EmailTemplate emailTemp = [SELECT Id FROM EmailTemplate WHERE DeveloperName ='Travel_Details_Notification' LIMIT 1];
        List<OrgWideEmailAddress> orgWideAddressDamac = [SELECT  Id, Address FROM OrgWideEmailAddress
                                                         WHERE DisplayName = :Label.DAMAC_no_replysf_damacgroup_com];
        Email_Notification_To_CC_List__c toCCList = Email_Notification_To_CC_List__c.getValues('Travel Details Update Utilization');
        for(Travel_Details__c travDetails : tdList){                     
            system.debug('Travel Details Owner Email: ' + travDetails.Owner.Email);
            List<String> toAddresses = new List<String>();
            List<String> ccAddresses = new List<String>();
            List<String> bccAddresses = new List<String>();
            Messaging.SingleEmailMessage mail = Messaging.renderStoredEmailTemplate(emailTemp.Id,
                                                                     UserInfo.getUserId(), travDetails.Id);
            mail.setOrgWideEmailAddressId(orgWideAddressDamac[0].Id);
            mail.setTreatTargetObjectAsRecipient(false);
            mail.setSaveAsActivity(false);
            toAddresses.add(travDetails.Owner.Email);
            if(toCCList != null){
                if(toCCList.To_Addresses__c != null){
                    for(String email: toCCList.To_Addresses__c.split(',')){
                        toAddresses.add(email.trim());
                    }
                }
                if(toCCList.CC_Addresses__c != null){
                    for(String email: toCCList.CC_Addresses__c.split(',')){
                        ccAddresses.add(email.trim());
                    }
                }
                if(toCCList.BCC_Addresses__c != null){
                    for(String email: toCCList.BCC_Addresses__c.split(',')){
                        bccAddresses.add(email.trim());
                    }
                }
            }
            mail.setToAddresses(toAddresses);
            mail.setCCAddresses(ccAddresses);
            mail.setBCCAddresses(bccAddresses);
            emails.add(mail);
        }
        system.debug('emails: ' + emails);
        if(!emails.isEmpty() && emails.size() > 0){
            if(!Test.isRunningTest()){
                Messaging.sendEmail(emails);
            }
        }        
    }
    
    /*********************************************************************************************
    * @Description : Implementing Finish method, to end an email after job completion.
    * @Params      : Database.BatchableContext
    * @Return      : void
    *********************************************************************************************/
    public void finish(Database.BatchableContext BC){
    /*
       AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems
                         FROM AsyncApexJob 
                         WHERE Id =: BC.getJobId()];
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {'vineet.kumar@nsigulf.com'};
       mail.setToAddresses(toAddresses);
       mail.setSubject('Lead Score Calculation ' + a.Status);
       mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems +
                             ' batches with '+ a.NumberOfErrors + ' failures.');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/
    }
}