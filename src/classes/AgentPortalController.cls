/**************************************************************************************************
* Name               : 
* Description        : An apex page controller for showing all active announcements                                             
* Created Date       : 31/07/2017                                                                         
* Created By         : Pratiksha Narvekar                                                                 
* Last Modified Date :  31.7.2017                                                                          
* Last Modified By   :  Lovel                                                                          
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR                    DATE                Comments                                                                   
* 1.0         Pratiksha                 31/07/2017     
* 1.1         Lovel                     31/07/2017          Added Filters for Widget
**************************************************************************************************/
 
public class AgentPortalController{
     // Changes by Lovel Starts -- 1.1
     public List<String> projectType{set;get;} // show type filter in VF
     public List<String> locationNames{set;get;} // show location filter in VF
     public List<String> bedrooms{set;get;}
     public AgentPortalController(){

     projectType = UtilityQueryManager.getAllProjectTypes();
     locationNames = UtilityQueryManager.getAllCities();
     bedrooms = UtilityQueryManager.getAllBedRooms();
          }
 
}