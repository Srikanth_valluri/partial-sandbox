/*
* Description - Test class developed for 'InlineBookingUnitExtension'
*
* Version            Date            Author            Description
* 1.0                14/12/17        Monali            Initial Draft
*/
@isTest
private class GenerateAgreementTest {
    static testMethod void testMethod1() {
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        Id recTypeEHO = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();
        
        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeEHO );
        objCase.Type = 'Early Handover';
        objCase.IsPOA__c = false;
        objCase.OCR_verified__c = true;
        objCase.OQOOD_Fee_Applicable__c = false;
        objCase.Approval_Status__c = 'Approved' ;
        objCase.Status = 'Working';
        insert objCase;

        Test.startTest();
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT());
        PageReference pageRef = Page.GenerateAgreement;
        pageRef.getParameters().put('id', String.valueOf(objCase.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        GenerateAgreement controller = new GenerateAgreement(sc);
        pageRef = controller.init();
        Test.stopTest();

    }

    static testMethod void testMethod2() {
        
        Id recTypeEHO = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();
        
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='12345');
        insert objAcc;

        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        Booking__c objBooking = new Booking__c(Account__c=objAcc.Id, Deal_SR__c=dealSR.Id);
        insert objBooking;

        Booking_Unit__c BUObj = new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='Test name1',
        Registration_ID__c = '92061', Registration_Status__c = 'Active Status', Unit_Selling_Price_AED__c = 100);
        insert BUObj;

        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeEHO );
        //objCase.Booking_Unit__c = BUObj.Id;
        insert objCase;

        GenerateAgreement.spaDetailsWrapper objWrap = new GenerateAgreement.spaDetailsWrapper();
        objWrap.PROC_STATUS ='';
        objWrap.PROC_MESSAGE ='';
        objWrap.ATTRIBUTE1 ='';
        objWrap.ATTRIBUTE2 ='';
        objWrap.ATTRIBUTE3 ='';
        objWrap.ATTRIBUTE4 ='';
        objWrap.PARAM_ID ='';

        Test.startTest();
        Test.setMock(WebServiceMock.class, new spaGenerationMock());
        PageReference pageRef = Page.GenerateAgreement;
        pageRef.getParameters().put('id', String.valueOf(objCase.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        GenerateAgreement controller = new GenerateAgreement(sc);
        pageRef = controller.init();
        Test.stopTest(); 
    }
    
    static testMethod void testMethod3() {
        Id recTypeEHO = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Early Handover').getRecordTypeId();
        
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person', party_ID__C='12345');
        insert objAcc;

        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        Booking__c objBooking = new Booking__c(Account__c=objAcc.Id, Deal_SR__c=dealSR.Id);
        insert objBooking;

        Booking_Unit__c BUObj = new Booking_Unit__c(Booking__c=objBooking.Id, Unit_Name__c='Test name1',
        Registration_ID__c = '92061', Registration_Status__c = 'Active Status', Unit_Selling_Price_AED__c = 100);
        insert BUObj;

        // insert Case
        Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recTypeEHO );
        insert objCase;
        
        test.startTest();
               
        PageReference pageRef = Page.GenerateAgreement;
        pageRef.getParameters().put('id', String.valueOf(objCase.Id)); 
        Test.setCurrentPage(pageRef); 
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockHandover());    
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        GenerateAgreement controller = new GenerateAgreement(sc);
         
        pageRef = controller.init();
         test.stopTest();
    }
}