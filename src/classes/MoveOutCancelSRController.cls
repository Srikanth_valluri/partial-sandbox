public with sharing class MoveOutCancelSRController {
	public MoveOutCancelSRController(ApexPages.StandardController stdController) { }

	public PageReference cancelSR() {
		String FMCaseId = ApexPages.currentPage().getParameters().get('id');
        //Recalling existing approval process and cancelling tasks
        List<FM_Case__c> lstCases = new List<FM_Case__c>();
        lstCases = cancelSRHelper(FMCaseId);
        update lstCases;
		return new PageReference('/'+FMCaseId);
    }
	public static List<FM_Case__c> cancelSRHelper(String FMCaseId){
        List<FM_Case__c> lstCases = new List<FM_Case__c>();
        try {
            //Recalling approval process
            lstCases = [SELECT Status__c, Id,
                                Request_Type_DeveloperName__c,
                                Booking_Unit__r.Property_City__c
                        FROM FM_Case__c WHERE Id=: FMCaseId];
            if(lstCases.size() > 0) {
                lstCases[0].Status__c = 'Cancelled';
                lstCases[0].Approval_Status__c='';
                lstCases[0].Submit_for_Approval__c = false;
                lstCases[0].Submitted__c = false;
            }
            List<ProcessInstanceWorkitem> piwi = [SELECT Id, ProcessInstanceId,
                                                        ProcessInstance.TargetObjectId
                                                    FROM ProcessInstanceWorkitem
                                                    WHERE ProcessInstance.TargetObjectId =: FMCaseId];
            System.debug('Target Object ID:' +piwi.size());
            if(piwi.size() > 0) {
                Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
                req.setComments('Recalling request');
                req.setAction('Removed');
                req.setWorkitemId(piwi.get(0).Id);
                req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
                Approval.ProcessResult result = Approval.process(req,false);
                System.debug('===result==' +result);
            }

            //Cancel tasks
            List<Task> lstTasks = new List<Task>();
            lstTasks = [SELECT Id, Status from Task WHERE WhatID =: FMCaseId];
            for(Task objTask : lstTasks) {
                objTask.Status = 'Cancelled';
            }
            update lstTasks;
        }
        catch(Exception excp) {
            System.debug('===excp==' +excp);
        }
        return lstCases;
    }
}