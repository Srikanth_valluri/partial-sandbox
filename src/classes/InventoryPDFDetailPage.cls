/**************************************************************************************************
* Name               : InventoryPDFDetailPage                                               
* Description        : An apex page controller for                                              
* Created Date       : NSI - Diana                                                                        
* Created By         : 24/Jan/2017                                                                 
* Last Modified Date :                                                                            
* Last Modified By   :                                                                            
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR                    DATE                                                                    
* 1.0         NSI - Diana          24/Jan/2017                                                               
**************************************************************************************************/
public class InventoryPDFDetailPage {
      /**************************************************************************************************
            Variables used in the class
    **************************************************************************************************/
    public string invid{set;get;}
    public Inventory__c inventory{set;get;}

    /**************************************************************************************************
    Method:         InventoryPDFDetailPage
    Description:    Constructor executing model of the class 
    **************************************************************************************************/
    public InventoryPDFDetailPage(ApexPages.StandardController controller) {
        
        invid = controller.getId();
        system.debug('***invid'+invid);
        for(Inventory__c thisInventory:[SELECT Selling_Price__c,List_Price_calc__c, Special_Price_calc__c,Property_Type__c,
                                        Building_Name__c,Marketing_Name__c,Unit__c,Property_Status__c,
                                        Unit_Type__c,IPMS_Bedrooms__c,Area_Sqft_2__c,Price_Per_Sqft__c,
                                        Anticipated_Completion_Date__c,Construction_Status__c,
                                        Land_Registration_Fee_A__c,Title_Deed_B__c,
                                        OQOOD_Fee_C__c,DSR_Fees_D__c,Total_Fees_to_DLD__c,
                                        Unit_Plan__c,Floor_Plan__c,Plot_Plan__c,Floor_Package_Name__c,Floor_Package_Type__c,
                                        Plot_Area__c,Bedroom_Type__c,Selling_Price_Excl_VAT__c
                                        from Inventory__c 
                                    WHERE Id = : invid]){
                inventory = thisInventory;                                 
       }
        
    }
}