@isTest
public class HDApp_WorkPermitSR_APITest {
	@TestSetup
    static void TestData() {
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(Agency_ID__c = '1234');
        insert sr;
        
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;
        
        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];
        
        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Customer Community Login User(Use this)%' Limit 1];
        User user1 = new User(Username = System.now().millisecond() + 'test12345@test.com',
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
        Database.insert(user1);
        
        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'test';
        locObj.Location_Type__c = 'Building';
        locObj.Location_ID__c = '12345';
        locObj.UAEProp__c = true;
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        invObj.Property_Country__c = 'UNITED ARAB EMIRATES';
        invObj.Marketing_Name_Doc__c = invObj.Id;
        invObj.Building_Location__c = locObj.Id;
        invObj.Property_Status__c = 'Ready'; 
        invObj.Unit_type__c = 'Villa';
        insert invObj;
        
        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1204',
                                      Registration_ID__c = '3901',
                                      Handover_Flag__c = 'N',
                                      Inventory__c = invObj.id,
                                      Dummy_Booking_Unit__c = true);
        insert bookingUnit;
        
        Id rtNocFitOutId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('NOC For FitOut').getRecordTypeId();
        FM_Case__c objWPFmCaseNocFitOut = new FM_Case__c();
        objWPFmCaseNocFitOut.Origin__c = 'Portal';
        objWPFmCaseNocFitOut.isHelloDamacAppCase__c = true;
        objWPFmCaseNocFitOut.Account__c = account.Id;
        objWPFmCaseNocFitOut.RecordtypeId = rtNocFitOutId;
        objWPFmCaseNocFitOut.Status__c = 'Closed';
        objWPFmCaseNocFitOut.Request_Type__c = 'NOC for Fit out/Alterations';
        objWPFmCaseNocFitOut.Type_of_NOC__c = 'NOC for Fit-out';
        objWPFmCaseNocFitOut.Booking_Unit__c = bookingUnit.Id;
        objWPFmCaseNocFitOut.Work_Permit_Type__c = 'Work Permit - Including Fit Out';
        objWPFmCaseNocFitOut.Person_To_Collect__c = 'Contractor';
        objWPFmCaseNocFitOut.Contact_person_contractor__c = 'Test Contractor';
        objWPFmCaseNocFitOut.Email_2__c = 'testctr@test.com';
        insert objWPFmCaseNocFitOut;
        
        Id rtWPId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Work Permit').getRecordTypeId();
        FM_Case__c objWPFmCaseContractor = new FM_Case__c();
        objWPFmCaseContractor.Origin__c = 'Portal';
        objWPFmCaseContractor.isHelloDamacAppCase__c = true;
        objWPFmCaseContractor.Account__c = account.Id;
        objWPFmCaseContractor.RecordtypeId = rtWPId;
        objWPFmCaseContractor.Status__c = 'Draft Request';
        objWPFmCaseContractor.Request_Type__c = 'Work Permit';
        objWPFmCaseContractor.Booking_Unit__c = bookingUnit.Id;
        objWPFmCaseContractor.Work_Permit_Type__c = 'Work Permit - Including Fit Out';
        objWPFmCaseContractor.Person_To_Collect__c = 'Contractor';
        objWPFmCaseContractor.Contact_person_contractor__c = 'Test Contractor';
        objWPFmCaseContractor.Email_2__c = 'testctr@test.com';
        insert objWPFmCaseContractor;
        
        OTP__c otpForSubmission = new OTP__c(FM_Case__c=objWPFmCaseContractor.Id,
                                             FM_Process_Name__c='Work Permit', 
                                             FM_SMS_OTP__c='1234',
                                             GUID__c='b09e81ef-cf42-13f0-2a3d-e5a0384e4cb5');
        insert otpForSubmission;
    }
    
    @isTest
    static void testGetLookupData() {
        Account account = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c bookingUnit = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        
        Test.startTest();
		
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/workPermitSR/getLookupData';
        req.addParameter('accountId',account.Id);
        req.addParameter('bookingUnitId',bookingUnit.Id);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WorkPermitSR_API.getLookupData();
        
        Test.stopTest();
    }
    
    @isTest
    static void testGetLookupDataWithNoParams() {
        Account account = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/workPermitSR/getLookupData';
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WorkPermitSR_API.getLookupData();
        
        req.addParameter('accountId',account.Id);
        
        HDApp_WorkPermitSR_API.getLookupData();
        
        Test.stopTest();
    }
    
    @isTest
    static void testCreateWorkPermitSRDraft() {
        Account account = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c bookingUnit = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        
        Test.startTest();
        
        List<FM_Case__c> listFMCase = [SELECT Id,Name FROM FM_Case__c WHERE Request_Type__c='NOC for Fit out/Alterations' LIMIT 1];
        
        String jsonString = 
            '   {																			'  +
            '		"action" : "draft",														'  +
            '       "sr_id": "",															'  +
            '       "account_id": "' + account.Id + '",										'  +
            '       "booking_unit_id": "' + bookingUnit.Id + '",							'  +
            '       "selected_fitout_sr_id": "' + listFMCase[0].Id + '",					'  +
            '		"type_of_work_permit" : "Work Permit - Including Fit Out",				'  +
            '		"purpose_of_request" : "Testing",										'  +
            '		"location" : "Test",													'  +
            '		"person_to_collect" : "Contractor",										'  +
            '		"permit_to_work_for" : "Sensitive Areas",								'  +
            '       "contractor_details" : {												'  +
            '       	"company_name" : "Test Company",									'  +
            '       	"contact_person_name" : "Test Contractor",							'  +
            '       	"contractor_email" : "testctr@test.com",							'  +
            '       	"country_code" : "United Arab Emirates: 00971",						'  +
            '       	"contractor_mobile_number" : "111111111",							'  +
            '       	"office_telephone" : "",											'  +
            '       	"contractor_type" : "",												'  +
            '       	"number_of_employees" : 1,											'  +
            '       	"employees_details" : [												'  +
            '       	{																	'  +
            '       		"emp_id" : "",													'  +
            '           	"employee_name" : "emp1",										'  +
            '           	"emirates_id" : "1234",											'  +
            '           	"company_name" : "Test Company",								'  +
            '           	"employee_country_code" : "United Arab Emirates: 00971",		'  +
            '           	"employee_mobile_number" : "2222222222"							'  +
            '       	}																	'  +
            '       	]																	'  +
        	'       }																		'  +
            '   }	';
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/workPermitSR/createSR';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonString);
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WorkPermitSR_API.createWorkPermitSR();
        
        Test.stopTest();
    }
    
    @isTest
    static void testCreateWorkPermitSRSubmit() {
        Account account = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c bookingUnit = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        
        Test.startTest();
        
        List<FM_Case__c> listNocFMCase = [SELECT Id,Name FROM FM_Case__c WHERE Request_Type__c='NOC for Fit out/Alterations' LIMIT 1];
        List<FM_Case__c> listWPFMCase = [SELECT Id,Name FROM FM_Case__c WHERE Request_Type__c='Work Permit' LIMIT 1];
        
        String jsonString = 
            '   {																			'  +
            '		"action" : "submit",													'  +
            '       "sr_id": "' + listWPFMCase[0].Id + '",									'  +
            '       "account_id": "' + account.Id + '",										'  +
            '       "booking_unit_id": "' + bookingUnit.Id + '",							'  +
            '       "selected_fitout_sr_id": "' + listNocFMCase[0].Id + '",					'  +
            '		"type_of_work_permit" : "Work Permit - Including Fit Out",				'  +
            '		"purpose_of_request" : "Testing",										'  +
            '		"location" : "Test",													'  +
            '		"person_to_collect" : "Contractor",										'  +
            '		"permit_to_work_for" : "Sensitive Areas",								'  +
            '		"guid" : "b09e81ef-cf42-13f0-2a3d-e5a0384e4cb5",						'  +
            '		"otp_entered" : "1234",													'  +
            '       "contractor_details" : {												'  +
            '       	"company_name" : "Test Company",									'  +
            '       	"contact_person_name" : "Test Contractor",							'  +
            '       	"contractor_email" : "testctr@test.com",							'  +
            '       	"country_code" : "United Arab Emirates: 00971",						'  +
            '       	"contractor_mobile_number" : "111111111",							'  +
            '       	"office_telephone" : "",											'  +
            '       	"contractor_type" : "",												'  +
            '       	"number_of_employees" : 1,											'  +
            '       	"employees_details" : [												'  +
            '       	{																	'  +
            '       		"emp_id" : "",													'  +
            '           	"employee_name" : "emp1",										'  +
            '           	"emirates_id" : "1234",											'  +
            '           	"company_name" : "Test Company",								'  +
            '           	"employee_country_code" : "United Arab Emirates: 00971",		'  +
            '           	"employee_mobile_number" : "2222222222"							'  +
            '       	}																	'  +
            '       	]																	'  +
        	'       }																		'  +
            '   }	';
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/workPermitSR/createSR';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonString);
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WorkPermitSR_API.createWorkPermitSR();
        
        Test.stopTest();
    }
    
    @isTest
    static void testCreateWorkPermitSRNotifyContractor() {
        Account account = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c bookingUnit = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        
        Test.startTest();
        
        List<FM_Case__c> listFMCase = [SELECT Id,Name FROM FM_Case__c WHERE Request_Type__c='NOC for Fit out/Alterations' LIMIT 1];
        
        String jsonString = 
            '   {																			'  +
            '		"action" : "notify_contractor",											'  +
            '       "sr_id": "",															'  +
            '       "account_id": "' + account.Id + '",										'  +
            '       "booking_unit_id": "' + bookingUnit.Id + '",							'  +
            '       "selected_fitout_sr_id": "' + listFMCase[0].Id + '",					'  +
            '		"type_of_work_permit" : "Work Permit - Including Fit Out",				'  +
            '		"purpose_of_request" : "Testing",										'  +
            '		"location" : "Test",													'  +
            '		"person_to_collect" : "Contractor",										'  +
            '		"permit_to_work_for" : "Sensitive Areas",								'  +
            '		"guid" : "",															'  +
            '		"otp_entered" : "1234",													'  +
            '       "contractor_details" : {												'  +
            '       	"company_name" : "Test Company",									'  +
            '       	"contact_person_name" : "Test Contractor",							'  +
            '       	"contractor_email" : "testctr@test.com",							'  +
            '       	"country_code" : "United Arab Emirates: 00971",						'  +
            '       	"contractor_mobile_number" : "111111111",							'  +
            '       	"office_telephone" : "",											'  +
            '       	"contractor_type" : "",												'  +
            '       	"number_of_employees" : 1,											'  +
            '       	"employees_details" : [												'  +
            '       	{																	'  +
            '       		"emp_id" : "",													'  +
            '           	"employee_name" : "emp1",										'  +
            '           	"emirates_id" : "1234",											'  +
            '           	"company_name" : "Test",										'  +
            '           	"employee_country_code" : "United Arab Emirates: 00971",		'  +
            '           	"employee_mobile_number" : "2222222222"							'  +
            '       	}																	'  +
            '       	]																	'  +
        	'       }																		'  +
            '   }	';
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/workPermitSR/createSR';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonString);
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WorkPermitSR_API.createWorkPermitSR();
        
        Test.stopTest();
    }
    
    @isTest
    static void testCreateWorkPermitSRWithInvalidData() {
        Account account = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        
        String jsonString = ''; 
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/workPermitSR/createSR';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonString);
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WorkPermitSR_API.createWorkPermitSR();
        
        jsonString = 
            '   {																			'  +
            '       "sr_id": ""															'  +
            '   }	';
        
        req.requestBody = Blob.valueOf(jsonString);
        
        HDApp_WorkPermitSR_API.createWorkPermitSR();
        
        jsonString = 
            '   {																			'  +
            '       "action": "abcd"														'  +
            '   }	';
        
        req.requestBody = Blob.valueOf(jsonString);
        
        HDApp_WorkPermitSR_API.createWorkPermitSR();
        
        jsonString = 
            '   {																			'  +
            '       "action": "draft"														'  +
            '   }	';
        
        req.requestBody = Blob.valueOf(jsonString);
        
        HDApp_WorkPermitSR_API.createWorkPermitSR();
        
        jsonString = 
            '   {																			'  +
            '       "action": "draft",														'  +
            '       "account_id": "' + account.Id + '"										'  +
            '   }	';
        
        req.requestBody = Blob.valueOf(jsonString);
        
        HDApp_WorkPermitSR_API.createWorkPermitSR();
        
        Test.stopTest();
    }
    
    @isTest
    static void testCreateWorkPermitSRWithInvalidOTP() {
        Account account = [SELECT Id FROM Account WHERE Party_ID__c = '63062'];
        Booking_Unit__c bookingUnit = [SELECT Id FROM Booking_Unit__c WHERE Registration_ID__c = '3901'];
        List<FM_Case__c> listWPFMCase = [SELECT Id,Name FROM FM_Case__c WHERE Request_Type__c='Work Permit' LIMIT 1];
        
        String jsonString = 
            '   {																			'  +
            '       "action": "submit",														'  +
            '       "sr_id": "' + listWPFMCase[0].Id + '"									'  +
            '   }	';
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/workPermitSR/createSR';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonString);
        RestContext.request = req;
        RestContext.response = res;
        
        HDApp_WorkPermitSR_API.createWorkPermitSR();
        
        jsonString = 
            '   {																			'  +
            '       "action": "submit",														'  +
            '       "sr_id": "' + listWPFMCase[0].Id + '",									'  +
            '       "guid": "abcdefgh"														'  +
            '   }	';
        
        req.requestBody = Blob.valueOf(jsonString);
        
        HDApp_WorkPermitSR_API.createWorkPermitSR();
        
        jsonString = 
            '   {																			'  +
            '       "action": "submit",														'  +
            '       "sr_id": "' + listWPFMCase[0].Id + '",									'  +
            '       "account_id": "' + account.Id + '",										'  +
            '       "booking_unit_id": "' + bookingUnit.Id + '",							'  +
            '       "guid": "abcdefgh",														'  +
            '		"otp_entered" : "1234"													'  +
            '   }	';
        
        req.requestBody = Blob.valueOf(jsonString);
        
        HDApp_WorkPermitSR_API.createWorkPermitSR();
        
        jsonString = 
            '   {																			'  +
            '       "action": "submit",														'  +
            '       "sr_id": "' + listWPFMCase[0].Id + '",									'  +
            '       "account_id": "' + account.Id + '",										'  +
            '       "booking_unit_id": "' + bookingUnit.Id + '",							'  +
            '       "guid": "b09e81ef-cf42-13f0-2a3d-e5a0384e4cb5",							'  +
            '		"otp_entered" : "5678"													'  +
            '   }	';
        
        req.requestBody = Blob.valueOf(jsonString);
        
        HDApp_WorkPermitSR_API.createWorkPermitSR();
        
        Test.stopTest();
    }
}