/***********************************************************************************
*   Description :  Test class developed for FMServiceChargesNotifierBatchScheduler.*
*----------------------------------------------------------------------------------*
*   Revision History:                                                              *
*   Version     Author          Date            Description                        *
*   1.0         Arjun Khatri    20/02/2019      Initial Draft                      *
************************************************************************************/
@isTest
public Class FMServiceChargesNotifierBatchSchedTest {
    
    @isTest
    public static void testSchedule() {
        FMServiceChargesNotifierBatchScheduler objSched = new FMServiceChargesNotifierBatchScheduler ();
        
        String sch = '0 0 23 * * ?'; 
        Test.StartTest();
        system.schedule('Test FM Outstanding Amount', sch, objSched );
        Test.stopTest();
    }
}