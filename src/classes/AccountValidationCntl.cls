public without sharing class AccountValidationCntl {
    public String phone{get; set;}
    public String email{get; set;}
    public String unitId{get; set;}
    public List<Booking_Unit__c> buList{get; set;}
    String unitName;
    
    public AccountValidationCntl() {
        phone = '';
        email = '';
        unitName = '';
        buList = new List<Booking_Unit__c>();
    }

    @RemoteAction
    public static List<Location__c> lookupBuilding(String name) {
        if (String.isBlank(name)) {
            return NULL;
        }
        name = '%' + String.escapeSingleQuotes(name) + '%';
        return [
            SELECT  Id
                    , Name
                    , Building_Name__c
                    , Location_Code__c
            FROM    Location__c
            WHERE   (Building_Name__c LIKE :name OR  Name LIKE :name)
                AND (NOT Building_Name__c  LIKE '%STORAGE%')
                AND (Location_Type__c = 'Building' OR Location_Type__c = NULL)
            LIMIT   200
        ];
    }

    @RemoteAction
    public static List<Booking_Unit__c> lookupUnit(String name, String locationId) {
        if (String.isBlank(name) || String.isBlank(locationId)) {
            return NULL;
        }
        name = '%' + String.escapeSingleQuotes(name) + '%';
        Set<String> setActiveStatuses = Booking_Unit_Active_Status__c.getAll().keySet();
        return [
            SELECT  Id
                    , Unit_Name__c
            FROM    Booking_Unit__c
            WHERE   Inventory__r.Building_Location__c = :locationId
                    AND Unit_Name__c LIKE :name
                    AND ((Registration_Status__c IN :setActiveStatuses
                            AND (Property_Status__c = 'Ready' OR Inventory__r.Property_Status__c = 'Ready')
                        ) OR Dummy_Booking_Unit__c = TRUE)
            LIMIT   200
        ];
    }

    
    public void checkDraftCase() {
        /*if(String.isBlank(unitId)) {
            buList.clear();
            //throw error
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please enter the Booking Unit'));
            //return NULL;
        }else if(String.isBlank(phone)&&String.isBlank(email)) {
            buList.clear();
            //throw error
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please enter Phone or Email'));
            //return NULL;
        }else */
        String phoneNumber = '';
         if(phone.contains('+')) {
            phoneNumber = phone.remove('+');
        }
        else {
            phoneNumber = phone;
        }
        phoneNumber = '%'+phoneNumber;
        String BUquery = 'SELECT Id, Registration_ID__c, Registration_Status__c, Unit_Name__c, Virtual_IBAN__c, Virtual_Bank_Account__c, Booking__r.Account__r.Person_Business_Email__c, Booking__r.Account__r.Mobile_Person_Business__c FROM Booking_Unit__c WHERE Unit_Name__c LIKE :unitId AND Unit_Active__c = \'Active\'';
        if(!String.isBlank(phone)&&String.isBlank(email)) {
            BUquery =  BUquery + ' AND Booking__r.Account__r.Mobile_Person_Business__c LIKE :phoneNumber ORDER BY CreatedDate DESC LIMIT 100';
        }else if(String.isBlank(phone)&&!String.isBlank(email)) {
            BUquery =  BUquery + ' AND Booking__r.Account__r.Person_Business_Email__c = :email ORDER BY CreatedDate DESC LIMIT 100';
        }else {
            BUquery =  BUquery + ' AND Booking__r.Account__r.Mobile_Person_Business__c LIKE :phoneNumber AND Booking__r.Account__r.Person_Business_Email__c = :email ORDER BY CreatedDate DESC LIMIT 100';
        }
        buList = Database.query(BUquery);
        system.debug('buList phone blank-->'+buList);
        
    }
}