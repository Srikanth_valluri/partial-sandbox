//Generated by wsdl2apex

public class Refunds {
    public class getExcessAmount_element {
        public String P_REQUEST_NUMBER;
        public String P_REQUEST_NAME;
        public String P_SOURCE_SYSTEM;
        public String RegistrationId;
        private String[] P_REQUEST_NUMBER_type_info = new String[]{'P_REQUEST_NUMBER','http://action.com',null,'0','1','true'};
        private String[] P_REQUEST_NAME_type_info = new String[]{'P_REQUEST_NAME','http://action.com',null,'0','1','true'};
        private String[] P_SOURCE_SYSTEM_type_info = new String[]{'P_SOURCE_SYSTEM','http://action.com',null,'0','1','true'};
        private String[] RegistrationId_type_info = new String[]{'RegistrationId','http://action.com',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://action.com','true','true'};
        private String[] field_order_type_info = new String[]{'P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM','RegistrationId'};
    }
    public class CustomerPaymentMade_element {
        public String P_REQUEST_NUMBER;
        public String P_REQUEST_NAME;
        public String P_SOURCE_SYSTEM;
        public String RegistrationId;
        private String[] P_REQUEST_NUMBER_type_info = new String[]{'P_REQUEST_NUMBER','http://action.com',null,'0','1','true'};
        private String[] P_REQUEST_NAME_type_info = new String[]{'P_REQUEST_NAME','http://action.com',null,'0','1','true'};
        private String[] P_SOURCE_SYSTEM_type_info = new String[]{'P_SOURCE_SYSTEM','http://action.com',null,'0','1','true'};
        private String[] RegistrationId_type_info = new String[]{'RegistrationId','http://action.com',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://action.com','true','true'};
        private String[] field_order_type_info = new String[]{'P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM','RegistrationId'};
    }
    public class getTokenRefunds_element {
        public String P_REQUEST_NUMBER;
        public String P_REQUEST_NAME;
        public String P_SOURCE_SYSTEM;
        public String RegistrationId;
        private String[] P_REQUEST_NUMBER_type_info = new String[]{'P_REQUEST_NUMBER','http://action.com',null,'0','1','true'};
        private String[] P_REQUEST_NAME_type_info = new String[]{'P_REQUEST_NAME','http://action.com',null,'0','1','true'};
        private String[] P_SOURCE_SYSTEM_type_info = new String[]{'P_SOURCE_SYSTEM','http://action.com',null,'0','1','true'};
        private String[] RegistrationId_type_info = new String[]{'RegistrationId','http://action.com',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://action.com','true','true'};
        private String[] field_order_type_info = new String[]{'P_REQUEST_NUMBER','P_REQUEST_NAME','P_SOURCE_SYSTEM','RegistrationId'};
    }
    public class CustomerPaymentMadeResponse_element {
        public String return_x;
        private String[] return_x_type_info = new String[]{'return','http://action.com',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://action.com','true','true'};
        private String[] field_order_type_info = new String[]{'return_x'};
    }
    public class getTokenRefundsResponse_element {
        public String return_x;
        private String[] return_x_type_info = new String[]{'return','http://action.com',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://action.com','true','true'};
        private String[] field_order_type_info = new String[]{'return_x'};
    }
    public class getExcessAmountResponse_element {
        public String return_x;
        private String[] return_x_type_info = new String[]{'return','http://action.com',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://action.com','true','true'};
        private String[] field_order_type_info = new String[]{'return_x'};
    }
    public class CustomerFundAndTokenHttpSoap11Endpoint {
        //public String endpoint_x = 'http://34.231.6.33:8080/CRM_SR_NEW/services/CustomerFundAndToken.CustomerFundAndTokenHttpSoap11Endpoint/';
        public String endpoint_x = Label.WSDLBaseUrl+':8080/CRM_SR_NEW/services/CustomerFundAndToken.CustomerFundAndTokenHttpSoap11Endpoint/';        
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://action.com', 'Refunds'};
        public String getTokenRefunds(String P_REQUEST_NUMBER,String P_REQUEST_NAME,String P_SOURCE_SYSTEM,String RegistrationId) {
            Refunds.getTokenRefunds_element request_x = new Refunds.getTokenRefunds_element();
            request_x.P_REQUEST_NUMBER = P_REQUEST_NUMBER;
            request_x.P_REQUEST_NAME = P_REQUEST_NAME;
            request_x.P_SOURCE_SYSTEM = P_SOURCE_SYSTEM;
            request_x.RegistrationId = RegistrationId;
            Refunds.getTokenRefundsResponse_element response_x;
            Map<String, Refunds.getTokenRefundsResponse_element> response_map_x = new Map<String, Refunds.getTokenRefundsResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'urn:getTokenRefunds',
              'http://action.com',
              'getTokenRefunds',
              'http://action.com',
              'getTokenRefundsResponse',
              'Refunds.getTokenRefundsResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.return_x;
        }
        public String getExcessAmount(String P_REQUEST_NUMBER,String P_REQUEST_NAME,String P_SOURCE_SYSTEM,String RegistrationId) {
            Refunds.getExcessAmount_element request_x = new Refunds.getExcessAmount_element();
            request_x.P_REQUEST_NUMBER = P_REQUEST_NUMBER;
            request_x.P_REQUEST_NAME = P_REQUEST_NAME;
            request_x.P_SOURCE_SYSTEM = P_SOURCE_SYSTEM;
            request_x.RegistrationId = RegistrationId;
            Refunds.getExcessAmountResponse_element response_x;
            Map<String, Refunds.getExcessAmountResponse_element> response_map_x = new Map<String, Refunds.getExcessAmountResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'urn:getExcessAmount',
              'http://action.com',
              'getExcessAmount',
              'http://action.com',
              'getExcessAmountResponse',
              'Refunds.getExcessAmountResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.return_x;
        }
        public String CustomerPaymentMade(String P_REQUEST_NUMBER,String P_REQUEST_NAME,String P_SOURCE_SYSTEM,String RegistrationId) {
            Refunds.CustomerPaymentMade_element request_x = new Refunds.CustomerPaymentMade_element();
            request_x.P_REQUEST_NUMBER = P_REQUEST_NUMBER;
            request_x.P_REQUEST_NAME = P_REQUEST_NAME;
            request_x.P_SOURCE_SYSTEM = P_SOURCE_SYSTEM;
            request_x.RegistrationId = RegistrationId;
            Refunds.CustomerPaymentMadeResponse_element response_x;
            Map<String, Refunds.CustomerPaymentMadeResponse_element> response_map_x = new Map<String, Refunds.CustomerPaymentMadeResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'urn:CustomerPaymentMade',
              'http://action.com',
              'CustomerPaymentMade',
              'http://action.com',
              'CustomerPaymentMadeResponse',
              'Refunds.CustomerPaymentMadeResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.return_x;
        }
    }
}