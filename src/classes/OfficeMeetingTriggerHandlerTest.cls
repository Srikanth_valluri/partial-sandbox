/*-------------------------------------------------------------------------------------------------
Description: Test class for private class OfficeMeetingTriggerHandler

    ============================================================================================================================
        Version | Date(DD-MM-YYYY) | Last Modified By | Comments
    ----------------------------------------------------------------------------------------------------------------------------
        1.0     | 13-05-2018       | Monali Nagpure   | 1. Test send email functionality
   =============================================================================================================================
*/
@isTest
public class OfficeMeetingTriggerHandlerTest {
    @testSetup static void setup() {
        //Creaing test data
        User PCuser = new User(
         ProfileId = [SELECT Id FROM Profile WHERE Name = 'Property Consultant'].Id,
         LastName = 'last',
         Email = 'puser000@amamama.com',
         Username = 'puser000@amamama.com' + System.currentTimeMillis(),
         CompanyName = 'TEST',
         Title = 'title',
         Alias = 'alias',
         TimeZoneSidKey = 'America/Los_Angeles',
         EmailEncodingKey = 'UTF-8',
         LanguageLocaleKey = 'en_US',
         LocaleSidKey = 'en_US'
        );
        insert PCuser;

        User receptionalistuser = new User(
         ProfileId = [SELECT Id FROM Profile WHERE Name = 'Receptionist'].Id,
         LastName = 'last',
         Email = 'puser000@amamama.com',
         Username = 'puser000@amamama.com' + System.currentTimeMillis(),
         CompanyName = 'TEST',
         Title = 'title',
         Alias = 'alias',
         TimeZoneSidKey = 'America/Los_Angeles',
         EmailEncodingKey = 'UTF-8',
         LanguageLocaleKey = 'en_US',
         LocaleSidKey = 'en_US',
         IsActive = true,
         Sales_Office__c = 'AKOYA'
        );
        insert receptionalistuser;


        Id devRecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        Inquiry__c inq = new Inquiry__c();
        inq.Mobile_Phone_Encrypt__c = '05789088';
        inq.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Preferred_Language__c = 'English';
        inq.Inquiry_Status__c = 'New' ;
        inq.First_Name__c = 'Test First name';
        inq.Last_Name__c = 'Test Last name';
        inq.Email__c = 'test@gmail.com';
        inq.Inquiry_Source__c = 'Customer Referral';
        inq.Mobile_Phone__c = '456789';
        inq.RecordTypeId = devRecordTypeId;
        inq.Assigned_PC__c = PCuser.Id;
        inq.Inquiry_Status__c = 'Meeting Scheduled';
        inq.Sales_Office__c = 'AKOYA';
        inq.Meeting_Due_Date__c = system.now();
        insert inq;

        Task tsk = new Task();
        tsk.WhatId=inq.Id;
        tsk.ActivityDate = System.today().addDays(2);
        tsk.Status = 'Not Started';
        tsk.Type = 'Meeting at Office';
        tsk.Activity_Type_3__c = 'Meeting at Office';
        tsk.Activity_Outcome__c = 'Show - Further Meeting Scheduled';
        tsk.Subject = 'Meeting With Customer';
        tsk.is_Owner_changed__c = true;
        insert tsk;    

    }
    @isTest static void testOfficeMeeting() {
        User receptionalistuser = [Select Id, 
                                          IsActive 
                                     From User 
                                    Where Profile.Name = 'Receptionist' 
                                      AND IsActive = true 
                                    Limit 1]; 
        system.debug('====Test class===receptionalistuser=======' +receptionalistuser); 
        
        List<String> inqSalesOffice = new List<String>();

        Schema.DescribeFieldResult fieldResult = Inquiry__c.Sales_Office__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for( Schema.PicklistEntry f : ple){
            inqSalesOffice.add(f.getValue());
        }
        system.debug('====Test class===inqSalesOffice=======' +inqSalesOffice); 
        
        
        List<QueueSObject> QueueList = [SELECT Queue.Id, 
                                               Queue.Name, 
                                               Queue.Type 
                                          FROM QueueSObject 
                                         WHERE Queue.Type ='Queue' 
                                           AND Queue.Name IN :inqSalesOffice];
        system.debug('====Test class===QueueList=======' +QueueList); 
        
        Inquiry__c inq = [SELECT Id, 
                                 Inquiry_Status__c,
                                 RecordType.Name,
                                 TSA_TL_of_Owner__c,
                                 First_Name__c,
                                 Last_Name__c,
                                 Email__c,
                                 Meeting_Due_Date__c,
                                 Sales_Office__c,
                                 Mobile_Phone__c
                            FROM Inquiry__c
                           WHERE RecordType.Name = 'Inquiry'
                             AND Inquiry_Status__c = 'Meeting Scheduled'
                           LIMIT 1
                             ];
        system.debug('====Test class===inq=======' +inq); 
        
        Office_Meeting__c  officeMeeting = new Office_Meeting__c();
        officeMeeting.Inquiry__c = inq.id;
        officeMeeting.Outcome__c = 'Show';
        officeMeeting.Status__c = 'Not Started';
        for(QueueSObject queue : QueueList){
            // system.debug('*****queueName*****' + queue);
            String queueName = queue.Queue.Name;
            if(queueName.equalsIgnoreCase(inq.Sales_Office__c)) {
                 officeMeeting.OwnerId = queue.Queue.Id;
            }
        }
        insert officeMeeting;
        
        Office_Meeting__Share  shareObj = new Office_Meeting__Share();
        shareObj.AccessLevel = 'Edit';
        shareObj.ParentId = officeMeeting.Id;
        shareObj.UserOrGroupId = receptionalistuser.Id;
        shareObj.RowCause = Schema.Office_Meeting__Share.RowCause.Manual;
		insert shareObj;        
        
        Office_Meeting__c ofcMeeting = new Office_Meeting__c();
        ofcMeeting.Id = officeMeeting.Id;
        officeMeeting.OwnerId = receptionalistuser.Id;
        
        update officeMeeting;
        
        officeMeeting.Status__c = 'Completed';
        ofcMeeting.Check_Out_Date__c = system.now();
        ofcMeeting.Check_In_Date__c = system.now();
        ofcMeeting.Outcome__c = 'Show';
        update officeMeeting;
    }
}