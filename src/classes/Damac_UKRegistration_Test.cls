/************************************************************************************************
 * @Name              : Damac_UKRegistration_Test
 * @Description       : Test Class for Damac_UKRegistration
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log
 * 1.0         QBurst         07/04/2020       Created
***********************************************************************************************/
@istest
public with sharing class Damac_UKRegistration_Test {
    public static testmethod void testMethod1(){
        Account acc = new Account ();
        acc.LastName = 'test';
        acc.Agency_Type__c = 'Corporate';
        acc.Vendor_ID__c = '767676';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        insert acc;

        Agent_Site__c agency = new Agent_Site__c();
        agency.Name = 'UAE';
        agency.Agency__c = acc.Id;
        insert agency;

        NSIBPM__SR_Template__c srTemplate = InitializeSRDataTest.createSRTemplate('Deal');
        insert srTemplate;

        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal', false, null);
        sr.Eligible_to_Sell_in_Dubai__c = true;
        sr.Agency_Type__c = 'Individual';
        sr.ID_Type__c = 'Passport';
        sr.Token_Amount_AED__c = 40000;
        sr.RecordTypeId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Change Agent').getRecordTypeId();
        sr.Booking_Wizard_Level__c = null;
        sr.Agency_Email_2__c = 'test2@gmail.com';
        sr.Alternate_Agency_Email_2__c = 'test1@gmail.com';
        sr.Country_of_Sale__c = 'UAE';
        sr.Mode_of_Payment__c = 'Online_Payment';
        sr.agency__c = acc.Id;
        sr.is_uk_deal__c = true;
        sr.Deal_Type__c = 'Recovery';
        insert sr;

        NSIBPM__Step_Template__c stpTemplate = InitializeSRDataTest.createStepTemplate('Deal', 'MANAGER_APPROVAL');
        insert stptemplate;

        List<string> statuses = new list<string>{'UNDER_MANAGER_REVIEW'};
        Map<string,NSIBPM__Status__c> stepStatuses = InitializeSRDataTest.createStepStatus(statuses);
        NSIBPM__Step__c stp = InitializeSRDataTest.createStep(sr.Id, stepStatuses.values()[0].Id, stptemplate.Id);

        Campaign__c camp = new Campaign__c();
        camp.End_Date__c = system.today().addmonths(10);
        camp.Marketing_End_Date__c = system.today().addmonths(10);
        camp.Marketing_Start_Date__c = system.today().addmonths(-10);
        camp.Start_Date__c = system.today().addmonths(-10);
        insert camp;

        Inquiry__c inq = new Inquiry__c ();
        inq.Activity_Counter__c = 101;
        inq.Inquiry_Status__c = 'Active';
        inq.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        inq.campaign__c = camp.Id;
        inq.ownerId = userinfo.getUserId();
        inq.Promoter_Name__c = 'Test';
        inq.Telesales_Executive__c = UserInfo.getUserId();
        insert inq;

        List<Booking__c> lstbk = new List<Booking__c>();
        lstbk.add(InitializeSRDataTest.createBooking(sr.Id));
        lstbk.add(InitializeSRDataTest.createBooking(sr.Id));
        lstbk[0].Inquiry_Account_Id__c = inq.Id;
        insert lstbk;

        Location__c loc = InitializeSRDataTest.createLocation('123', 'Building');
        loc.property_id__c = '123';
        insert loc;

        Property__c newProperty = new Property__c();
        newProperty.Property_ID__c = 1;
        newProperty.Property_Code__c = 'VIR';
        newProperty.Property_Name__c = 'VIRIDIS @ AKOYA OXYGEN';
        newProperty.District__c = 'AL YUFRAH 2';
        newProperty.AR_Transaction_Type__c  = 'INV VIR';
        newProperty.Penalty_Transaction_Type__c = 'DM-PENALTIES-VIR';
        newProperty.Brokerage_Distribution_Set__c = '11600';
        newProperty.Sales_Commission_Dist_Set__c = '11601';
        newProperty.Currency_Of_Sale__c = 'AED' ;
        newProperty.Signature_Col_Customer_Stmt__c = 'Front Line Investment Management Co. LLC';
        newProperty.EOI_Enabled__c = true;
        insert newProperty;

        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.Id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Unit_Location__c = loc.Id;
        lstInv[0].building_location__c = loc.Id;
        lstInv[0].property_id__c = newProperty.Id;
        insert lstInv;

        Id accountRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account a = new Account();
        a.LastName= 'Test Account';
        a.RecordTypeId = accountRTId;
        insert a;

        buyer__c buyer = new buyer__c();
        buyer.Buyer_Type__c = 'Individual';
        buyer.Address_Line_1__c = 'Ad1';
        buyer.Country__c = 'United Arab Emirates';
        buyer.City__c = 'Dubai' ;
        buyer.Account__c = acc.Id;
        buyer.inquiry__c = inq.Id;
        buyer.dob__c = system.today().addyears(-30);
        buyer.Email__c = 'test@test.com';
        buyer.First_Name__c = 'firstname';
        buyer.Last_Name__c = 'lastname';
        buyer.Nationality__c = 'Indian';
        buyer.Passport_Expiry__c = system.today().addyears(20);
        buyer.Passport_Number__c = 'J0565556';
        buyer.Phone__c = '569098767';
        buyer.Phone_Country_Code__c = 'United Arab Emirates: 00971';
        buyer.Place_of_Issue__c =  'India';
        buyer.Title__c = 'Mr';
        buyer.booking__c = lstbk[0].Id;
        buyer.Primary_Buyer__c = true;
        buyer.Unique_Key__c = lstbk[0].Id + '' + inq.Id;
        insert buyer;

        Currency_Rate__c rate = new Currency_Rate__c();
        rate.To_Currency__c = 'AED';
        rate.Conversion_Date__c = system.today();
        rate.conversion_rate__c = 1;
        rate.From_Currency__c = 'AED';
        insert rate;

        Inventory__c invent = new Inventory__c();
        invent.Inventory_ID__c = '1';
        invent.Building_ID__c = '1';
        invent.Floor_ID__c = '1';
        invent.Marketing_Name__c = 'Damac Heights';
        invent.Address_Id__c = '1' ;
        invent.EOI__C = NULL;
        invent.Tagged_to_EOI__c = false;
        invent.Is_Assigned__c = false;
        invent.Property_ID__c = newProperty.Id;
        invent.Property__c = newProperty.Id;
        invent.Status__c = 'Released';
        invent.special_price__c = 1000000;
        invent.CurrencyISOCode = 'AED';
        invent.Property_ID__c = '1234';
        invent.Floor_Package_ID__c = '';
        invent.building_location__c = loc.Id;
        invent.property_id__c = newProperty.Id;
        insert invent;

        Campaign__c c = new Campaign__c();
        c.Start_Date__c = system.today().addDays(-1);
        c.End_Date__c = system.today().addDays(1);
        c.Marketing_Start_Date__c = system.today().addDays(-1);
        c.Marketing_End_Date__c = system.today().addDays(1);
        c.RecordTypeID = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Roadshows').getRecordTypeId();   
        insert c; 

        Campaign_Inventory__c campInv = new Campaign_Inventory__c ();
        campInv.Inventory__c = invent.Id;
        campInv.Start_Date__c = Date.Today ();
        campInv.End_Date__c = Date.Today ();
        campInv.Campaign__c = c.Id;
        insert campInv;

        Payment_Plan__c pp = new Payment_Plan__c();
        pp.Effective_From__c = system.today();
        pp.Effective_To__c = system.today(); 
        pp.Building_Location__c =  loc.Id;  
        pp.term_id__c = '1234';  
        insert pp;

        Available_options__c opt = new Available_options__c();
        opt.type_of_option__c = 'Scheme';
        opt.Promotion_Option__c = 'test';
        opt.discount_type__c = 'psf';
        opt.Price_Value__c = 1;
        opt.Percent_Value__c = 5;
        insert opt;

        selected_units__c sel = new selected_units__c();
        sel.service_request__c = sr.Id;
        sel.Payment_Plan__c = pp.Id;
        sel.inventory__c = invent.Id;
        sel.Requested_Price_AED__c = 1000000;
        sel.Selected_Campaign__c = opt.Id;
        insert sel;

        Available_options__c opt1 = new Available_options__c();
        opt1.type_of_option__c = 'Option';
        opt1.Promotion_Option__c = 'test';
        opt1.discount_type__c = 'psf';
        opt1.Price_Value__c = 1;
        opt1.selected_units__c = sel.Id;
        insert opt1;

        Payment_Terms__c pt = new Payment_Terms__c();
        pt.Payment_Plan__c = pp.Id;
        pt.Percent_Value__c = '5';
        insert pt;

        DAMAC_Constants.skip_BookingUnitTrigger = true;
        List<Booking_Unit__c> buList = new List<Booking_Unit__c>();
        Booking_Unit__c bu1 = new Booking_Unit__c();
        bu1.Booking__c = lstbk[0].Id;
        bu1.Payment_Method__c = 'Cash';
        bu1.Primary_Buyer_s_Email__c = 'test@damac.com';
        bu1.Primary_Buyer_s_Name__c = 'testNSI';
        bu1.Primary_Buyer_s_Nationality__c = 'Russia';
        bu1.Inventory__c = invent.id;
        bu1.Registration_ID__c = '1234';
        bu1.Payment_Plan_Id__c = pp.Id;
        buList.add(bu1);

        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = lstbk[0].Id;
        bu.Payment_Method__c = 'Cash';
        bu.Primary_Buyer_s_Email__c = 'test@damac.com';
        bu.Primary_Buyer_s_Name__c = 'testNSI';
        bu.Primary_Buyer_s_Nationality__c = 'Russia';
        bu.Inventory__c = invent.Id;
        bu.Registration_ID__c = 'test123'; 
        bu.Payment_Plan_Id__c = pp.Id;
        buList.add(bu);
        insert buList;
        
        NSIBPM__SR_Status__c srStatus = new NSIBPM__SR_Status__c();
        srStatus.NSIBPM__Code__c = 'SUBMITTED';
        insert srStatus;

        Deal_Team__c dealTeam = new Deal_Team__c(Primary_Team__c = true, Associated_Deal__c = sr.Id, Associated_HOS__c = userInfo.getUserId());
        dealTeam.Associated_DOS__c = userInfo.getUserId();
        dealTeam.Associated_HOD__c = userInfo.getUserId();
        dealTeam.PC_Team__c = 'PC Team';
        insert dealTeam;
        
        List<Option__c> lstOptions = TestDataFactory_CRM.createOptions( buList );
        insert lstOptions;
        test.starttest();
        Damac_UKRegistration.processRequest(sr.Id, '3231231');
        Damac_UKRegistration.processJointBuyerFuture('Update', '432434', sr.Id, 'Buyer', 'SR', '3534534534');
        Date testDate = system.today();
        String dateStr = Damac_UKRegistration.getDatetext(testDate);
        for(Integer i = 0; i < 12; i++){
            testDate = testDate.addMonths(1);
            dateStr = Damac_UKRegistration.getDatetext(testDate);
        }
        test.stoptest();
    }
}