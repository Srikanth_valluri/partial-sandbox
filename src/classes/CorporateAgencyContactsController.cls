public with sharing class CorporateAgencyContactsController {

    public List<String> contactHeaderList   {get; set;}
    public List<String> contactFieldList    {get; set;}
    public List<Contact> contactList        {get; set;}

    public CorporateAgencyContactsController(ApexPages.StandardController controller){
        // Corporate_Agency_Contact_Fields
        String accountId = controller.getId();
        String loggedinUserId = UserInfo.getUserId();
        String loggedinUserRoleId = UserInfo.getUserRoleId();
        String loggedinUserProfileId = UserInfo.getProfileId();
        contactList = new List<Contact>();
        contactHeaderList = new List<String>();
        contactFieldList = new List<String>();
        String queryFields = '';

        Map<String,List<Contact>> contactOwner = new Map<String,List<Contact>>();
        Set<Id> agentsTeamProfileIdSet = new Set<Id>();
        for(Profile profileObj : [Select Id
                                    FROM Profile
                                   WHERE Name = :Label.Agent_Executive_Manager_Profile
                                      OR Name = :Label.Agent_Executive_Team_Profile
                                      OR Name = :Label.Agent_Admin_Team_Profile
                                      OR Name = :Label.Agent_Admin_Manager_Profile
                                      OR Name = :Label.System_Admin_Profile
        ]) {
            agentsTeamProfileIdSet.add(profileObj.Id);
        }

        Account acc = [SELECT Id, OwnerId FROM Account WHERE Id = :accountId];

        for (Schema.FieldSetMember contactField : SObjectType.Contact.FieldSets.CorporateAgencyContactFields.getFields()
        ) {
            if (contactField.getFieldPath() != 'OwnerId') {
                queryFields += contactField.getFieldPath() + ', ';
            }
            contactHeaderList.add(contactField.getLabel());
            contactFieldList.add(contactField.getFieldPath());
        }

        String contactQuery = ' SELECT '
                            + queryFields
                            + ' Id, OwnerId '
                            + ' FROM Contact '
                            + ' WHERE AccountId = :accountId ';

        if (loggedinUserId == acc.OwnerId
            || agentsTeamProfileIdSet.contains(loggedinUserProfileId)
        ) {
            contactList = Database.query(contactQuery);
/*

                contactList = [SELECT
                                    Id, Name, Email, Owner__c, Agent_Representative__c,
                                    Authorised_Signatory__c, Portal_Administrator__c,
                                    Shareholding__c
                                 FROM CONTACT
                                WHERE AccountId = :accountId];*/
        }
        if(contactList.isEmpty()) {
            if(String.isNotBlank(accountId)) {
                /*for(Contact con : [SELECT
                                        Id, Name, Email, Owner__c, Agent_Representative__c,OwnerId,
                                        Authorised_Signatory__c, Portal_Administrator__c,
                                        Shareholding__c
                                     FROM CONTACT
                                    WHERE AccountId = :accountId]) {*/
                for(Contact con :Database.query(contactQuery)) {
                    if(!contactOwner.containsKey(con.OwnerId)) {
                        contactOwner.put(con.OwnerId,new List<Contact>{});
                    }
                    contactOwner.get(con.OwnerId).add(con);
                }
            }
            getAllSubRoleIds(loggedinUserId,contactOwner);
        }
    }


    public void getAllSubRoleIds(String UserId,Map<String,List<Contact>> contactOwnerMap) {
        if (String.isNotBlank(UserId) && !contactOwnerMap.isEmpty()) {

            // get all of the roles underneath the passed roles
            for(User userObj : [ SELECT
                                    Id,Name,ManagerId
                                 FROM User
                                 WHERE Id = :UserId]) {
                if(contactOwnerMap.containsKey(userObj.Id)) {
                    contactList.addAll(contactOwnerMap.get(userObj.Id));
                }
            }
            getChildUsers(new Set<Id>{UserId},contactOwnerMap);
        }
    }

    public void getChildUsers(Set<Id> userIds,Map<String,List<Contact>> contactOwnerMap) {
        Set<Id> userIdSet = new Set<Id>();
        if(userIds.size() > 0) {
            for(User us : [SELECT
                                Id,Name,ManagerId
                           FROM User
                           WHERE ManagerId IN :userIds]) {
                if(contactOwnerMap.containsKey(us.Id)) {
                    contactList.addAll(contactOwnerMap.get(us.Id));
                }
                userIdSet.add(us.Id);
            }
            if(!userIdSet.isEmpty()) {
                getChildUsers(userIdSet,contactOwnerMap);
            }
        }
    }

}