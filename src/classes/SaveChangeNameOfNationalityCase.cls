@RestResource(urlMapping='/SaveChangeNameOfNationalityCase/*')
 Global class SaveChangeNameOfNationalityCase
 {
     @HtTPPost
    Global static String SaveChangeNameOfNationalityCase(NONCaseWrapper codCaseWrapper)
    {
        Case cocdSR=new Case();
        cocdSR.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(codCaseWrapper.RecordType).getRecordTypeId();
      
        cocdSR.AccountId =codCaseWrapper.AccountID;
        cocdSR.New_CR__c =codCaseWrapper.NewPassportNO;
        cocdSR.Passport_Issue_Date__c = codCaseWrapper.PassportIssuedDate;
        cocdSR.Passport_Issue_Place__c =codCaseWrapper.PassportIssuedPlace;
        cocdSR.Passport_Issue_Place_Arabic__c =codCaseWrapper.PassportIssuePlaceArabic;
        cocdSR.Changed_Nationality__c =codCaseWrapper.ChangedNationality;
        cocdSR.Changed_Nationality_Arabic__c =codCaseWrapper.ChangedNationalityArabic;
        cocdSR.Title__c = codCaseWrapper.Title;
        cocdSR.Title_Arabic__c =codCaseWrapper.TitleArabic;
        cocdSR.Customer_First_Name__c = codCaseWrapper.CustomerFirstName;
        cocdSR.Customer_First_Name_Arabic__c =codCaseWrapper.CustomerFirstNameArabic;
        cocdSR.Customer_Middle_Name__c =codCaseWrapper.CustomerMiddleName;
        cocdSR.Customer_Middle_Name_Arabic__c =codCaseWrapper.CustomerMiddleNameArabic; 
        cocdSR.Customer_Last_Name__c =codCaseWrapper.CustomerLastName; 
        cocdSR.Customer_Last_Name_Arabic__c =codCaseWrapper.CustomerLastNameArabic; 
        cocdSR.OQOOD_Fee_Applicable__c = false;
        cocdSR.Type = 'Name Nationality Change SR';
        cocdSR.SR_Type__c = 'Name Nationality Change SR';
         cocdSR.Status=codCaseWrapper.status;
        cocdSR.Origin=codCaseWrapper.origin;
        cocdSR.OwnerId=setCaseOwner(codCaseWrapper.AccountID);
            cocdSR.id=codCaseWrapper.salesforceId;
        upsert cocdSR;
        // PushNotificationToMobileApp.PushNotificationSend(cocdSR.CaseNumber,codCaseWrapper.fcm);
        return cocdSR.id;
    }
    
     Global class NONCaseWrapper{
        public String RecordType;
        public String AccountID;
        public String UserName;  
        public Date PassportIssuedDate;  
        public String PassportIssuedPlace;
        public String ChangedNationality;
        public String PassportIssuePlaceArabic;
        public String ChangedNationalityArabic;
        public String Title;
        public String TitleArabic;
        public String CustomerFirstName;
        public String CustomerFirstNameArabic;  
        public String CustomerMiddleName;
        public String CustomerMiddleNameArabic;
        public String CustomerLastName;
        public String CustomerLastNameArabic;
        public String NewPassportNO;
        public String status;
        public String origin;
        public String fcm;
         public String salesforceId;
        
       
    }
     
    @TestVisible 
    private static String setCaseOwner(String AccId) {
    String cocdSR;
        List<Account> lstAccount = [
            SELECT  Id, Primary_CRE__c, Secondary_CRE__c, Tertiary_CRE__c, Primary_Language__c
            FROM    Account
            WHERE   Id = :AccId
        ];
        if (lstAccount.isEmpty()) {
           
        }
        if (lstAccount[0].Primary_CRE__c != NULL) {
            cocdSR = lstAccount[0].Primary_CRE__c;
        } else if (lstAccount[0].Secondary_CRE__c != NULL) {
            cocdSR = lstAccount[0].Secondary_CRE__c;
        } else if (lstAccount[0].Tertiary_CRE__c != NULL) {
            cocdSR= lstAccount[0].Tertiary_CRE__c;
        } else {
            List<QueueSobject> lstQueue = [
                SELECT      Id, Queue.Name, QueueId FROM QueueSobject
                WHERE       SobjectType = 'Case'
                        AND Queue.Name IN ('Non Elite Arabs Queue', 'Non Elite Non Arabs Queue')
                ORDER BY    Queue.Name
                LIMIT   2
            ];
            if (lstQueue.size() != 2) {
               
            }
            if ('Arabic'.equalsIgnoreCase(lstAccount[0].Primary_Language__c)) {
                cocdSR = lstQueue[0].QueueId;
            } else {
                cocdSR= lstQueue[1].QueueId;
            }
        }
        return cocdSR;
    }
    
      
 }