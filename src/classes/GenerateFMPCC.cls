public without sharing class GenerateFMPCC {
    string strPageID;
     public GenerateFMPCC(ApexPages.standardController controller){
       
    }//End constructor
    
    public pagereference init(){
        strPageID = ApexPages.currentPage().getParameters().get('id');
        case objCase = [select Id, AccountId, Booking_Unit__c, CaseNumber, Recordtype.Name,
                        Booking_Unit__r.Registration_ID__c 
                        from case where id =:strPageID];
        FMPCCGeneration.FMPCCResponse objFMPCC;
        objFMPCC = FMPCCGeneration.GetFMPCC('PCC-DFM', objCase.Booking_Unit__r.Registration_ID__c, strPageID);
        String FMPCCurl = objFMPCC.url;       
        system.debug('!!!!!!!!!FMPCCurl'+FMPCCurl);
        list<SR_Attachments__c> lstCaseAttachment = new list<SR_Attachments__c>();
        list<Error_Log__c> lstErrorLog = new list<Error_Log__c>();
        Id attachmentId = fetchPCCDocument();
        if (FMPCCurl == null || FMPCCurl == '') {
            SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
            objCaseAttachment.Case__c = objCase.id;
            objCaseAttachment.Id = attachmentId;
            objCaseAttachment.Name = 'FM PCC ' + system.now();
            objCaseAttachment.Booking_Unit__c = objCase.Booking_Unit__c;
            lstCaseAttachment.add(objCaseAttachment);
                
            Error_Log__c objErr = new Error_Log__c();
            objErr.Account__c = objCase.AccountId;
            objErr.Booking_Unit__c = objCase.Booking_Unit__c;
            objErr.Case__c = objCase.id;
            objErr.Error_Details__c = objFMPCC.P_PROC_MESSAGE;
            objErr.Process_Name__c = objCase.Recordtype.Name;
            lstErrorLog.add(objErr);
        } else {
             SR_Attachments__c objCaseAttachment = new SR_Attachments__c();
             objCaseAttachment.Case__c = objCase.id;
             objCaseAttachment.Id = attachmentId;
             objCaseAttachment.Name = 'FM PCC ' + system.now();
             objCaseAttachment.Attachment_URL__c = FMPCCurl;
             objCaseAttachment.Booking_Unit__c = objCase.Booking_Unit__c;
             lstCaseAttachment.add(objCaseAttachment);
        }
           system.debug('lstCaseAttachment '+lstCaseAttachment );
            if (lstCaseAttachment != null && lstCaseAttachment.size()>0){
                upsert lstCaseAttachment;
            }
            if (lstErrorLog != null && lstErrorLog.size()>0){
                insert lstErrorLog;
            }
            
            pagereference newpg = new Pagereference(URL.getSalesforceBaseUrl().toExternalForm()+'/'+strPageID);
            newpg.setRedirect(false);
            return newpg;
    }//End init
    
    public Id fetchPCCDocument(){
        list<SR_Attachments__c> lstSR = [Select Id, Name, Case__c 
                                        from SR_Attachments__c 
                                        where Case__c =:strPageID
                                        and Name Like 'FM PCC%' limit 1];
        if(lstSR != null && !lstSR.isEmpty()){
            return lstSR[0].Id;
        }else{
            return null;
        }
    }
}