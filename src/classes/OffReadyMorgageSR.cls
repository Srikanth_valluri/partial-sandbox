public with sharing class OffReadyMorgageSR {
    
    @InvocableMethod
    public static void OffReadyPlan(List<Id> caseID){
        system.debug('OffReadyPlanMortgageSR ===== lstSR in Mortgage ' + caseID);
        //updateMortgageSR(caseID);
        if(caseID.size() > 0) {
            List<Case> parentCase = [ SELECT ID,
                                             Booking_Unit__c,
                                             Booking_Unit__r.Property_Status__c,
                                             CaseNumber,
                                             Owner.Email, Mortgage_Off_Ready_Plan__c,Mortgage_Status__c,
                                             Owner.FirstName
                                        FROM Case
                                       WHERE ID IN :caseID
                                    ];
            if( string.isNotBlank( parentCase[0].Booking_Unit__r.Property_Status__c ) ) {
                if((parentCase[0].Booking_Unit__r.Property_Status__c).equalsIgnoreCase('Off Plan') ) {
                    parentCase[0].Mortgage_Status__c = 'Mortgage Generate NOC';
                }else {
                     parentCase[0].Mortgage_Status__c = 'Mortgage Auto validation';
                }
                parentCase[0].Mortgage_Off_Ready_Plan__c = parentCase[0].Booking_Unit__r.Property_Status__c;
            }
            if(parentCase.size() > 0) {
                update parentCase;
                system.debug('======Successs : ' +parentCase );
            }
        }
        
        
        
        
    }

    /*@Future(callout=true)
    public static void updateMortgageSR(List<Id> caseIDList) {
        List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
        system.debug('OffReadyPlanMortgageSR ==updateMortgageSR===  ' +caseIDList);
        List<Case> parentCase = [ SELECT ID,
                                         Booking_Unit__c,
                                         CaseNumber,
                                         Owner.Email, 
                                         Owner.FirstName
                                    FROM Case
                                   WHERE ID IN :caseIDList
                                ];
        if(parentCase.size() > 0 && parentCase[0].Booking_Unit__c != null) {
            Booking_Unit__c bUnit = [SELECT ID, 
                                            Registration_ID__c 
                                       FROM Booking_Unit__c 
                                     WHERE ID = :parentCase[0].Booking_Unit__c
                                    ];
            system.debug('bUnit Registration_ID__c ==updateMortgageSR===  ' + bUnit.Registration_ID__c);
            
            try{
                UnitDetailsService.BookinUnitDetailsWrapper objWrap = new UnitDetailsService.BookinUnitDetailsWrapper();
                if(bUnit.Registration_ID__c != null ) {
                    objWrap = UnitDetailsService.getBookingUnitDetails(bUnit.Registration_ID__c);
                    if(objWrap != null){
                        parentCase[0].Mortgage_Off_Ready_Plan__c = objWrap.strReady_Offplan;
                        parentCase[0].Mortgage_Status__c = 'OFF-PLAN'.EqualsIgnoreCase(objWrap.strReady_Offplan) ? 'Mortgage Generate NOC' : 'Mortgage Auto validation';
                    } 
                }
                
                system.debug('======parentCase.size() : ' +parentCase.size() );
                if(parentCase.size() > 0) {
                    update parentCase;
                    system.debug('======Successs : ' +parentCase );
                }
            } catch(Exception e){
                system.debug('======Exception : ' +e  );
                Error_Log__c errorLogObj = new Error_Log__c(Error_Details__c=e.getMessage(),Case__c=caseIDList[0], Process_Name__c='Mortgage');
                lstErrorLog.add(errorLogObj);
            }
        } else {
            //send email to MA for selection of booking Unit
            if(  (!parentCase.isEmpty() && String.isNotBlank( parentCase[0].Owner.Email) ) || Test.isRunningTest() ){
              List<String> toAddresses = new List<String>();
                  //toAddresses.add('amit.joshi@eternussolutions.com');
                  //toAddresses.add('snehil.karn@eternussolutions.com');
                  toAddresses.add( parentCase[0].Owner.Email );
                  Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                  mail.setToAddresses(toAddresses);

                  mail.setSubject( 'Booking Unit Selection is pending for Case Number - ' +  parentCase[0].CaseNumber );
                  String mailBody = 'Hi '+parentCase[0].Owner.FirstName+',<br/>';
                  mailBody += '<p>Booking Unit Selection is pending for Case Number - '+ parentCase[0].CaseNumber+'. Please Booking Unit to proceed further.</p><br/><br/>';
                  mailBody += 'Thanks you,<br/>DAMAC Team.';

                  mail.setHtmlBody( mailBody );
                  system.debug('mail' + mail);
                  if(!Test.isRunningTest()){
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                  }
            }
        } 
        if(lstErrorLog.size() > 0) {
            insert lstErrorLog;
        }                       
    }*/

}