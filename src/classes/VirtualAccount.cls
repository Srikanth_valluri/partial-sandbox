/***************AKISHOR*04Sep2020*********For Sync'g Virtual Account Details****************
 * 
 * 14/10/2020 Ravi Removed the Active flag from Query
*/
@RestResource(urlMapping='/VirtualAccount/*')
global with sharing class VirtualAccount{
 @HttpPatch
    global static void VirtualAccount( String RegId, String Account,String IBAN ) 
    {
        ResponseWrapper objResponseWrapper = new ResponseWrapper();
        Set<Id> recTypeIdSet=new Set<Id>();
       
        System.debug('-->>input: '+ Account+IBAN);
        List<Booking_Unit__c> SngBUList= [Select Id,Registration_Id__c 
                                            from Booking_Unit__c 
                                            where Registration_Id__c=:RegId limit 1];//Unit_Active__c='Active' 
        
        if(SngBUList == NULL || SngBUList.isEmpty()){              
            objResponseWrapper.status = 'No Unit exists, please select correct unit.';
            objResponseWrapper.statusCode = '400';
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(objResponseWrapper));
        }
        String RegistrationId1;
        if(!SngBUList.isEmpty()){
            RegistrationId1=SngBUList[0].Registration_Id__c;
            
            if(RegistrationId1!='') {
                List<Booking_Unit__c> lstBu = new  List<Booking_Unit__c>();
                Booking_Unit__c bulist = new Booking_Unit__c();
                bulist.Id=SngBUList[0].Id;
                bulist.Virtual_Bank_Account__c= Account;
                bulist.Virtual_IBAN__c=IBAN;
                lstBu.add(bulist);            
                if(lstBu.size() > 0 ) {
                    try{
                        Database.update(lstBu); //add BU update DML
                        objResponseWrapper.status = 'Account Details Updated Successfully';
                        objResponseWrapper.statusCode = '200';
                        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(objResponseWrapper));
                        Restcontext.response.statusCode = 200;
                    } catch( Exception ex ) {
                        system.debug( ex.getMessage() );
                        objResponseWrapper.errorMessage = ex.getMessage();
                        objResponseWrapper.statusCode = '400';
                        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(objResponseWrapper));
                        Restcontext.response.statusCode = 400;
                        System.debug('============= response : ' + JSON.serialize(objResponseWrapper));
                        return;
                    }
                }
            }//end of regid If
        }
    }

    //for sending response
    public class ResponseWrapper {
        public String status;
        public String statusCode;
        public String errorMessage;

        public ResponseWrapper() {
            this.status = '';
            this.statusCode = '';
            this.errorMessage = '';
        }
    }
}