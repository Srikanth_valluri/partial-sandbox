@isTest
public class VAT_AddendumDocGenCaseTest {
    @isTest
    public static void testWithUnit() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest( );
        insert objSR ;
        
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1);
        insert lstBookings;
        
        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        Inventory__c objInv = TestDataFactory_CRM.createInventory(objProp.Id);
        objInv.Property_Country__c = 'UAE';
        objInv.Property_City__c = 'Dubai';
        objInv.Property_Status__c = 'Ready';
        objInv.Bedroom_Type__c = ' 1 BR';
        insert objInv;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        lstBookingUnits[0].Inventory__c = objInv.Id;
        lstBookingUnits[0].Area__c = 35;
        lstBookingUnits[0].Generate_and_Send_Addendum__c =true;
        lstBookingUnits[0].VAT_Email_Sent__c = false;
        lstBookingUnits[0].Permitted_Use_Type__c = 'Units With VAT';
        lstBookingUnits[0].VAT_Addendum_Template__c ='Test';
        insert lstBookingUnits;
        
        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c(name='Unit with HO VAT Addendum-Case'
                                                        ,Drawloop_Document_Package_Id__c ='test'
                                                        ,Delivery_Option_Id__c='test');
        Case objCase = new Case();
        objCase.Status = 'New';
        objCase.Origin = 'Web';
        objCase.Alternate_Email__c = 'test22@t.com';
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.RecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Handover VAT Addendum').RecordTypeId;
        insert objCase;
        Test.startTest();
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController( objCase );
        VAT_AddendumDocGenCase ctrl = new VAT_AddendumDocGenCase(stdCtrl);
        VAT_AddendumDocGenCase.generateVAT();
        Test.stopTest();
    }

    @isTest
    public static void testWithoutUnit() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest( );
        insert objSR ;
        
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1);
        insert lstBookings;
        
        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        Inventory__c objInv = TestDataFactory_CRM.createInventory(objProp.Id);
        objInv.Property_Country__c = 'UAE';
        objInv.Property_City__c = 'Dubai';
        objInv.Property_Status__c = 'Ready';
        objInv.Bedroom_Type__c = ' 1 BR';
        insert objInv;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        lstBookingUnits[0].Inventory__c = objInv.Id;
        lstBookingUnits[0].Area__c = 35;
        lstBookingUnits[0].Generate_and_Send_Addendum__c =true;
        lstBookingUnits[0].VAT_Email_Sent__c = false;
        lstBookingUnits[0].Permitted_Use_Type__c = 'Units Without VAT';
        lstBookingUnits[0].VAT_Addendum_Template__c ='Test';
        insert lstBookingUnits;
        
        insert new Riyadh_Rotana_Drawloop_Doc_Mapping__c(name='Unit w/o HO VAT Addendum-Case'
                                                        ,Drawloop_Document_Package_Id__c ='test'
                                                        ,Delivery_Option_Id__c='test');
        Case objCase = new Case();
        objCase.Status = 'New';
        objCase.Origin = 'Web';
        objCase.Alternate_Email__c = 'test22@t.com';
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.RecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Handover VAT Addendum').RecordTypeId;
        insert objCase;
        Test.startTest();
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController( objCase );
        VAT_AddendumDocGenCase ctrl = new VAT_AddendumDocGenCase(stdCtrl);
        VAT_AddendumDocGenCase.generateVAT();
        Test.stopTest();
    }

    @isTest
    public static void testUploadVAT() {

        Case objCase = new Case();
        objCase.Status = 'New';
        objCase.Origin = 'Web';
        objCase.Alternate_Email__c = 'test22@t.com';
        objCase.RecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Handover VAT Addendum').RecordTypeId;
        insert objCase;
        Test.startTest();
        
        Attachment att = new Attachment( name='VAT Addendum', parentId = objCase.Id, body = Blob.valueOf('test') );
        insert att;
        Test.stopTest();
    }
}