/************************************************************************************************
 * @Name              : DAMAC_MSTeamsIntegration_Test
 * @Description       : Test Class for DAMAC_MSTeamsIntegration 
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         24/03/2020       Created
***********************************************************************************************/

@isTest
public class DAMAC_MSTeamsIntegration_Test {

    @isTest
    static void testCreateMeeting(){
        API_Integration_Value__c apiValue = new API_Integration_Value__c();
        apiValue.Refresh_Token__c = 'refreshtoken'; 
        apiValue.Client_Id__c = 'testclientid';
        apiValue.Client_Secret__c = 'testclientsecret';
        apiValue.Endpoint_URL__c = 'testendpointurl';
        apiValue.Redirect_URI__c = 'testredirecturi'; 
        apiValue.Scope__c = 'testscope'; 
        apiValue.Event_Creation_URL__c = 'testeventcreationurl';
        apiValue.Tenant_Id__c = 'testtenantid'; 
        apiValue.Username__c = 'testusername';
        apiValue.Password__c = 'testpassword';
        apiValue.Meeting_EndPoint_URL__c = 'endpointurl';
        apiValue.name = 'Graph API';
        insert apiValue;
        
        String stTime = DateTime.now().addHours(2).formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
        String endTime = DateTime.now().addHours(3).formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
        
        Id inquiryRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        Inquiry__c inqObj = new Inquiry__c(Pre_InquiryId__c='123456',
                                            Mobile_Phone_Encrypt__c='456123',RecordTypeId = inquiryRT,
                                            Mobile_CountryCode__c='American Samoa: 001684',
                                            Mobile_Phone__c='1234',Email__c='mk@gmail.com',
                                            First_Name__c='Test',Last_Name__c='Last',
                                            CR_Number__c='0987',Organisation_Name__c = 'Oliver',
                                            Online_Meeting_Start_Date_Time__c = DateTime.now().addHours(2),
                                            Online_Meeting_End_Date_Time__c = DateTime.now().addHours(3)
                                            );
        inqObj.Assigned_PC__c = UserInfo.getuserId(); 
        inqObj.Meeting_Type__c = 'Direct Tour';
        inqObj.Tour_Date_Time__c = system.now();
        insert inqObj;
        
        Test.startTest();
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/microsoftgraphlogin?code=uhijhOAQ';
        request.params.put('code', 'testcode');

        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = res;
        DAMAC_MS_GRAPH_API.doGet();
        
        DAMAC_MSTeamsIntegration.fetchAccessToken();
        PageReference pageRef = Page.MS_GraphAPI_Token;
        Test.setCurrentPage(pageRef);
        DAMAC_MSTeamsIntegration obj = new DAMAC_MSTeamsIntegration();
        obj.getToken();
        
        DAMAC_TeamsMeeting_JSON json = DAMAC_MSTeamsIntegration.createMeeting('token', 'subject', stTime, endTime );
        DAMAC_MSTeamsIntegration.deleteEvent('testcontactid');
        DAMAC_MSTeamsIntegration.getEventDetails('testcontactid');
        
        DAMAC_MSTeamsIntegration.sendInvitation(inqObj.Id);
        Test.stopTest();
    }
}