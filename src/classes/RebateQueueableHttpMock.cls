@isTest
global class RebateQueueableHttpMock implements HttpCalloutMock{
    
    Integer responseNumber;

    global RebateQueueableHttpMock(Integer num) {
        responseNumber = num;
    }

    global HttpResponse respond(HttpRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type','application/json');
        String strResponse;

        if(responseNumber == 1) {
            strResponse = '{' +
                                '"OutputParameters": ' +
                                '{' +
                                    '"@xmlns": "http://xmlns.oracle.com/apps/ont/rest/Rebate/xxdc_open_receipt_proc/",'+
                                    '"@xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",'+
                                    
                                    '"X_ERRBUF": "Below Receipts found: ",'+
                                    '"X_ERRCODE": "S"'+
                                    '}'+
                               '}';
        }
        if(responseNumber == 2) {
            strResponse = '{' +
                                '"OutputParameters": ' +
                                '{' +
                                    '"@xmlns": "http://xmlns.oracle.com/apps/ont/rest/Rebate/xxdc_open_receipt_proc/",'+
                                    '"@xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",'+
                                    
                                    '"X_ERRBUF": "ERROR ",'+
                                    '"X_ERRCODE": "E"'+
                                    '}'+
                               '}';
        }
        if(responseNumber == 3) {
            strResponse ='';
        }
        res.setBody(strResponse );
        return res;
    }
}