Public class Damac_ZiwoCallHistory {

    public static String login () {
        Ziwo_settings__c settings = Ziwo_settings__c.getInstance (UserInfo.getUserId());
        
        HTTPRequest req = new HTTPRequest ();
        req.setEndpoint (settings.Endpoint__c+'auth/login');
        req.setMethod ('POST');
        req.setTimeOut (120000);
        req.setHeader ('Content-Type', 'application/x-www-form-urlencoded');
        req.setBody ('username='+settings.user_name__c+'&password='+settings.password__c);
        
        HTTP http = new HTTP ();
        HTTPResponse res = new HTTPResponse ();
        if (!Test.isRunningTest ())
            res = http.send (req);
        else
            res.setBody ('{"content": {"access_token": "data"}}');
        
        System.debug (res.getBody ());
        Damac_ZiwoLoginResp obj = Damac_ZiwoLoginResp.parse (res.getBody ());
        
        return obj.content.access_token;
    }
    @InvocableMethod
    public static void updateCallHistory (List<ID> ids) {
        set <String> callIds = new set <String> ();
        for (Waybeo_Logs__c log :[SELECT Ziwo_callID__c FROM Waybeo_Logs__c WHERE ID IN: ids AND Ziwo_callID__c != NULL]) {
            callIds.add (log.Ziwo_callID__c);
        }
        
        if(!System.isFuture() && !System.isBatch())
            captureCallHistory(callIds);
        else
            syncCaptureCallHistory(callIds);
    }


    @Future(Callout = TRUE)
    public Static void captureCallHistory (set<String> callIds) {
        synccaptureCallHistory(callIds);
    }
    
    public Static void syncCaptureCallHistory (set<String> callIds) {
    
       Ziwo_settings__c settings = Ziwo_settings__c.getInstance (UserInfo.getUserId());
        
        String accessToken = settings.Access_Token__c;
        Boolean updateToken = false;
        if (settings.Last_logged_In_Time__c != NULL) {
            DateTime lastLoggedInTime = settings.Last_logged_In_Time__c ;
            Long dt1Long = lastLoggedInTime.getTime();
            Long dt2Long = DateTime.now().getTime();
            Long milliseconds = dt2Long - dt1Long;
            Long seconds = milliseconds / 1000;
            Long minutes = seconds / 60;
            Long hours = minutes / 60;
            system.debug (hours+'==='+minutes+'=='+seconds+'==='+milliseconds);
            if (hours >= 24 || accessToken == NULL) {
                accessToken = login();
                updateToken = true;
            }
        } else {
            accessToken = login();
            updateToken = true;
        }
        
        List <Waybeo_Logs__c> callHistories = new List <Waybeo_Logs__c> ();
        
        for (String callId : callIds) {
            HTTPRequest req = new HTTPRequest ();
            req.setMethod ('GET');
            req.setEndpoint (settings.Endpoint__c+'callHistory/'+callId+'?access_token='+accessToken);
            req.setTimeOut (120000);
            HTTP http = new HTTP ();
            HTTPResponse res = new HTTPResponse ();
            if (!test.isRunningTest ())
                res = http.send (req);
            else {
                res.setStatusCode (200);
                res.setBody ('{"result":true,'
                                +'"content":{"id":64,'
                                +'"callID":"945f9b2b-5db4-4204-9445-65c7bd1075d2",'
                                +'"startedAt":"2020-02-03T13:39:22.000Z",'
                                +'"endedAt":"2020-02-03T13:39:43.000Z",'
                                +'"answeredAt":"2020-02-03T13:39:43.000Z",'
                                +'"queueEnteredAt":null,"agentRingStartedAt":null,'
                                +'"queueAnsweredAt":null,"agentWaitTime":0,"audioQuality":100,'
                                +'"callerIDName":"042480532","callerIDNumber":"042480532",'
                                +'"channelName":"verto.rtc/agent-1111@127.0.0.1",'
                                +'"didCalled":"00919652300133","direction":"outbound",'
                                +'"disposition":"ANSWER","duration":21,"talkTime":0,'
                                +'"ringTime":21,"gateway":"default-external-gateway",'
                                +'"hangupBy":"originatee","hangupCause":null,"holdTime":0,'
                                +'"ivrFile":"","lostInIVR":false,"nonWorkingHours":false,'
                                +'"queueTalkTime":0,"queueWaitTime":0,'
                                +'"recordingFile":"945f9b2b-5db4-4204-9445-65c7bd1075d2.mp3",'
                                +'"voicemail":false,"flags":0,"offeredToAgentId":8,'
                                +'"positionId":6,"agentId":8,"otherSideUserId":null,'
                                +'"numberId":null,"queueId":null,"nonFCRRepeats":6,'
                                +'"ivrTime":null,"result":null,"surveyRating":null,'
                                +'"surveyRecording":null,"createdAt":"2020-02-03T13:39:43.547Z",'
                                +'"updatedAt":"2020-02-03T13:39:43.547Z","extendedInfo":{},'
                                +'"startDateTime":"2020-02-03T13:39:22.000Z","endDateTime":"2020-02-03T13:39:43.000Z",'
                                +'"answeredDateTime":"2020-02-03T13:39:43.000Z","queueEnterDateTime":null,'
                                +'"agentRingStartDateTime":null,"queueAnsweredDateTime":null},"info":{}}');
            } 
                
            System.debug (res.getBody ());
            
            if (res.getStatusCode () == 200) {
                Damac_ZiwoCallHistory_RESP obj = Damac_ZiwoCallHistory_RESP.parse (res.getBody ());
                
                Waybeo_Logs__c log = new Waybeo_Logs__c ();
                
                log.Ziwo_callID__c = callId;
                log.Ziwo_agentRingStartDateTime__c = ''+obj.content.agentRingStartDateTime;
                log.Ziwo_agentRingStartedAt__c = ''+obj.content.agentRingStartedAt;
                log.Ziwo_agentWaitTime__c = ''+obj.content.agentWaitTime;
                log.Ziwo_answeredAt__c = obj.content.answeredAt;
                log.Ziwo_answeredDateTime__c = obj.content.answeredDateTime;
                log.Ziwo_audioQuality__c = ''+obj.content.audioQuality;
                log.Ziwo_callerIDName__c = obj.content.callerIDName;
                log.Ziwo_callerIDNumber__c = obj.content.callerIDNumber;
                log.Ziwo_channelName__c = obj.content.channelName;
                log.Ziwo_createdAt__c = obj.content.createdAt;
                log.Ziwo_didCalled__c = obj.content.didCalled;
                log.Ziwo_direction__c = obj.content.direction;
                log.Ziwo_disposition__c = obj.content.disposition;
                log.Ziwo_duration__c = ''+obj.content.duration;
                log.Ziwo_endDateTime__c = obj.content.endDateTime;
                log.Ziwo_endedAt__c = obj.content.endedAt;
                log.Ziwo_extendedInfo__c = ''+obj.content.extendedInfo;
                log.Ziwo_flags__c = ''+obj.content.flags;
                log.Ziwo_gateway__c = obj.content.gateway;
                log.Ziwo_hangupBy__c = obj.content.hangupBy;
                log.Ziwo_hangupCause__c = ''+obj.content.hangupCause;
                log.Ziwo_holdTime__c = ''+obj.content.holdTime;
                log.ziwo_id__c = ''+obj.content.id;
                log.Ziwo_ivrFile__c = obj.content.ivrFile;
                log.Ziwo_ivrTime__c = ''+obj.content.ivrTime;
                log.Ziwo_lostInIVR__c = ''+obj.content.lostInIVR;
                log.Ziwo_nonFCRRepeats__c = ''+obj.content.nonFCRRepeats;
                log.Ziwo_nonWorkingHours__c = ''+obj.content.nonWorkingHours;
                log.Ziwo_numberId__c = ''+obj.content.numberId;
                log.Ziwo_offeredToAgentId__c = ''+obj.content.offeredToAgentId;
                log.Ziwo_otherSideUserId__c = ''+obj.content.otherSideUserId;
                log.Ziwo_positionId__c = ''+obj.content.positionId;
                log.Ziwo_queueAnsweredAt__c = ''+obj.content.queueAnsweredAt;
                log.Ziwo_queueAnsweredDateTime__c = ''+obj.content.queueAnsweredDateTime;
                log.Ziwo_queueEnterDateTime__c = ''+obj.content.queueEnterDateTime;
                log.Ziwo_queueEnteredAt__c = ''+obj.content.queueEnteredAt;
                log.Ziwo_queueId__c = ''+obj.content.queueId;
                log.Ziwo_queueTalkTime__c = ''+(obj.content.queueTalkTime == null ? 0:obj.content.queueTalkTime);
                log.Ziwo_queueWaitTime__c = ''+obj.content.queueWaitTime;
                log.Ziwo_recordingFile__c = ''+obj.content.recordingFile;
                log.Ziwo_result__c = ''+obj.content.result;
                log.Ziwo_ringTime__c = ''+obj.content.ringTime;
                log.Ziwo_startDateTime__c = ''+obj.content.startDateTime;
                log.Ziwo_startedAt__c = ''+obj.content.startedAt;
                log.Ziwo_surveyRating__c = ''+obj.content.surveyRating;
                log.Ziwo_surveyRecording__c = ''+obj.content.surveyRecording;
                log.Ziwo_talkTime__c = ''+(obj.content.talkTime == null? 0: obj.content.talkTime);
                log.Ziwo_updatedAt__c = ''+obj.content.updatedAt;
                log.Ziwo_voicemail__c = ''+obj.content.voicemail;
                callHistories.add (log);
            }
        }
        Schema.SObjectField f = Waybeo_Logs__c.Fields.Ziwo_callID__c;
        Database.UpsertResult[] results = Database.upsert (callHistories, f, false) ;
        System.debug (results);
        
        if (updateToken) {
            settings = [SELECT Access_token__c, Last_logged_in_Time__c FROM Ziwo_settings__c];
            settings.Access_Token__c = accessToken;
            settings.Last_logged_In_Time__c = DateTime.Now ();
            update settings;
        }
        
        
    }
}