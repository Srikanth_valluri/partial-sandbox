/*
Controller to capture the Count of page View as an Inline VF page on the Record Detail Pages
*/

global class DAMAC_PAGE_VIEWS{
    
    public static decimal latitude{get; set;}
    public static decimal longitude{get; set;}
    public static Id recordId{get; set;}
    public DAMAC_PAGE_VIEWS(){
    
    }
    public DAMAC_PAGE_VIEWS(ApexPages.StandardController controller) { // Required with StandardController
    
    }
    @RemoteAction
    global static void updateCount(decimal lat, decimal lng, string recId) {
        latitude = lat;
        longitude = lng;
        recordId = recId;
        updateCount();
    }
    public static void updateCount() {
        System.debug('>>>>>>>>>>>>>>'+latitude);
        System.debug('>>>>>>>>>>>>>>'+longitude);
       
        /*
        Url thisUrl =   Url.getCurrentRequestUrl();
        String thisPath = thisUrl.getPath(), 
        thisFile = thisUrl.getFile(), 
        thisFullPath = thisPath + '/' + thisFile;
        system.debug(ApexPages.currentPage().getParameters().get('id'));
        system.debug(URL.getSalesforceBaseUrl().toExternalForm());
        system.debug('thisUrl ='+thisUrl);
        system.debug('thisPath ='+thisPath );
        system.debug('thisFile '+thisFile );
        
        system.debug('-->'+system.URL.getSalesforceBaseURL().getHost());
        
        Map<String, String> headerlist = ApexPages.currentPage().getHeaders();
        system.debug(headerlist.size());
        system.debug(headerlist);
        string ipAddress;
        // True-Client-IP has the value when the request is coming via the caching integration.
        ipAddress = ApexPages.currentPage().getHeaders().get('True-Client-IP');
        
        // X-Salesforce-SIP has the value when no caching integration or via secure URL.
        if (ipAddress == '' || ipAddress == null) {
            ipAddress = ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
        } 
        
        // get IP address when no caching (sandbox, dev, secure urls)        
        if (ipAddress == '' || ipAddress == null) {
            ipAddress = ApexPages.currentPage().getHeaders().get('X-Forwarded-For');
        }
        system.debug('-->IP=='+ipAddress);
        */
        String ip;
        string sessionInfo='';
        if(!Test.isRunningTest()){
            Map<String,String> currentSessionAttributes = Auth.SessionManagement.getCurrentSession();
            ip = currentSessionAttributes.get('SourceIp');
            system.debug('-->'+ip);
            system.debug('>>>>>>>>>>'+currentSessionAttributes);
            for(string s:currentSessionAttributes.keyset()){
                sessionInfo = sessionInfo+'\n'+s+'-'+currentSessionAttributes.get(s);
                
            }
            system.debug(sessionInfo);
        }
        
        recordId = string.valueof(recordId).substring(0,15);
        
        string objectName = '';
        objectName = string.valueof(recordId.getsobjecttype());
        System.debug('OBJECT NAME=='+objectName);
        
        list<Page_View__c> pv = new list<Page_View__c>();
        string key = userinfo.getuserid()+'-'+recordId+'-'+system.today().day()+'/'+system.today().Month()+'/'+system.today().year();
        
        pv = [select id,View_Count__c,Unique_Key__c from Page_View__c where Unique_Key__c=:key limit 1];
        if(pv.size() == 0 ){
            Page_View__c p = new Page_View__c();
            p.Unique_Key__c = userinfo.getuserid()+'-'+recordId+'-'+system.today().day()+'/'+system.today().Month()+'/'+system.today().year();
            p.User__c = userinfo.getuserid();
            p.Last_Open_Date_time__c = system.now();
            p.Page_URL__c = string.valueof(system.URL.getOrgDomainUrl().gethost())+'/'+recordId;
            p.View_Count__c = 1;    
            p.Log_Date__c = system.today();  
            p.IP_Address__c = ip ;
            p.Session_Info__c = sessionInfo;
            P.Object_Name__c = objectName;
            p.Record_ID__c = (ID)recordId;
            p.Location__latitude__s = latitude;
            p.Location__longitude__s = longitude;
            String sObjectType = ((ID)recordId).getSobjectType().getDescribe().getName();
            
            try {
                if (p.record_id__c != NULL) {
                    Sobject acc = Database.query('SELECT Name, RecordTypeId FROM '+sObjectType+' WHERE ID =: recordId');
                    String recordTypeId = ''+acc.get('RecordTypeId');
                    p.record_type_id__c = recordTypeId;
                    p.Record_Name__c = ''+acc.get('Name');
                    p.record_type_name__c = Schema.getGlobalDescribe().get(sObjectType).getDescribe().getRecordTypeInfosById().get(recordTypeId).getName();
                }
            } catch (Exception e) {
                Sobject acc = Database.query('SELECT Name FROM '+sObjectType+' WHERE ID =: recordId');
                p.Record_Name__c = ''+acc.get('Name');
                System.debug (e.getMessage()+e.getLineNumber());
            }
            System.debug('>>>>>>>>>>>'+p.Location__c);            
            pv.add(p);  
        }else{
            pv[0].Last_Open_Date_time__c = system.now();
            pv[0].View_Count__c = pv[0].View_Count__c+1;  
            pv[0].IP_Address__c = ip ;
            pv[0].Session_Info__c = sessionInfo;
            pv[0].Location__latitude__s = latitude;
            pv[0].Location__longitude__s = longitude;
        }
        
        upsert pv Unique_Key__c;
        
        Geo_Location__c loc = new Geo_Location__c();
        loc.Page_View__c = pv[0].Id;
        loc.Location__longitude__s = longitude;
        loc.Location__latitude__s = latitude;
        insert loc;
        //return null;
        //return new pagereference('/'+recordId+'?nooverride=1');
    }
}