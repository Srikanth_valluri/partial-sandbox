/**************************************************************************************************
* Name               : AssignedAgentTriggerHandlerTest                                            *
* Description        : This is a test class for AssignedAgentTriggerHandler.                      *
* Created Date       : 10/07/2017                                                                 *
* Created By         : NSI                                                                        *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR            DATE		COMMENTS                                              *
* 1.0         NSI - Vineet      10/07/2017  Initial draft.                                        *
**************************************************************************************************/
@isTest
private class AssignedAgentTriggerHandlerTest {
	
	private static List<Campaign__c> campaignRecordList = new List<Campaign__c>(); 
	private static List<Assigned_Agent__c> assignedAgentsList = new List<Assigned_Agent__c>();
    
	private static void setupData(){

		campaignRecordList = TestDataFactory.createCampaignRecords(new List<Campaign__c>{new Campaign__c(),new Campaign__c()});		
		List<Inventory__c> invList=TestDataFactory.CreateInventoryRecords(new List<Inventory__c>{new Inventory__c()});
       
        Campaign_Inventory__c cmpInv=new Campaign_Inventory__c();
        cmpInv.Campaign__c=campaignRecordList[1].id;
        cmpInv.Inventory__c=invList[0].id;
        insert cmpInv;
        
        assignedAgentsList = TestDataFactory.createAssignedAgentRecords(new List<Assigned_Agent__c>{new Assigned_Agent__c(Unique_ID__c='234jhg',User__c=userInfo.getUserId(),Campaign__c=campaignRecordList[0].id),new Assigned_Agent__c(Unique_ID__c='35gfr',User__c=userInfo.getUserId(),Campaign__c=campaignRecordList[1].id)});		
        
    }
	
    static testMethod void myUnitTest() {
    	setupData();
        update assignedAgentsList;
        delete assignedAgentsList;
    }
}// End of class.