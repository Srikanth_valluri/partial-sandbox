@isTest
private class CreatePOPSRControllerTest {
    static testMethod void testNewCase() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        insert objAcc;
        System.assert(objAcc != null);
    
        Case objCase = new Case();
        objCase.Origin = 'Email';
        objCase.Subject = 'Test email';
        objCase.AccountId = objAcc.id;
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        insert objCase;

        Case objCase1 = new Case();
        objCase1.Origin = 'Email';
        objCase1.Subject = 'Test email';
        objCase1.AccountId = objAcc.id;
        objCase1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RentalPool Email').getRecordTypeId();
        insert objCase1;
        
        Case objCase2 = new Case();
        objCase2.Origin = 'Email';
        objCase2.Subject = 'Test email';
        objCase2.AccountId = objAcc.id;
        objCase2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Collection Email').getRecordTypeId();
        insert objCase2;        

        EmailMessage[] newEmail = new EmailMessage[0];
        EmailMessage objEmailMessage = new EmailMessage(
            FromAddress = 'test@abc.org', 
            Incoming = True, ToAddress= 'hello@670ocglw7xhomi4oyr5yw2zvf.8kp7yeag.8.case.salesforce.com', 
            Subject = 'Test email', 
            TextBody = '23456 ', 
            ParentId = objCase.Id); 
        insert objEmailMessage;
        
        Attachment attobj=new Attachment();
        attobj.Name='test';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attobj.body=bodyBlob;
        attobj.ContentType = 'application/pdf';
        attobj.parentId=objEmailMessage.Id;
        insert attobj;
        System.debug('attobj' + attobj);
        
        PageReference pageRef = Page.CreatePOPSR; 
        pageRef.getParameters().put('id', String.valueOf(objCase.Id));
        
        ApexPages.StandardController controller = new ApexPages.standardController(objCase);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT());
            Test.setMock(WebServiceMock.class, new RefundsMock.RefundsMock2()); 
            Test.setMock(WebServiceMock.class, new RefundsExcessAmountMock.RefundsExcessAmountMock1());
             Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
             Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
             Test.setMock(WebServiceMock.class, new RefundsApprovingAuthoritiesMock());
             Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(3));
             
        CreatePOPSRController obj = new CreatePOPSRController(controller);
        obj.caseId = objCase.Id;
        obj.init();
    
    }
    
    static testMethod void testNewCase1() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        insert objAcc;
        System.assert(objAcc != null);
    
        Case objCase = new Case();
        objCase.Origin = 'Email';
        objCase.Subject = 'Test email';
        objCase.AccountId = objAcc.id;
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        objCase.status = 'Closed';
        insert objCase;

        Case objCase1 = new Case();
        objCase1.Origin = 'Email';
        objCase1.Subject = 'Test email';
        objCase1.AccountId = objAcc.id;
        objCase1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RentalPool Email').getRecordTypeId();
        objCase.status = 'Closed';
        insert objCase1;
        
        Case objCase2 = new Case();
        objCase2.Origin = 'Email';
        objCase2.Subject = 'Test email';
        objCase2.AccountId = objAcc.id;
        objCase2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Collection Email').getRecordTypeId();
        objCase.status = 'Closed';
        insert objCase2;        
        
        PageReference pageRef = Page.CreatePOPSR; 
        pageRef.getParameters().put('id', String.valueOf(objCase.Id));
    
        ApexPages.StandardController controller = new ApexPages.standardController(objCase);
Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT());
            Test.setMock(WebServiceMock.class, new RefundsMock.RefundsMock2()); 
            Test.setMock(WebServiceMock.class, new RefundsExcessAmountMock.RefundsExcessAmountMock1());
             Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
             Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
             Test.setMock(WebServiceMock.class, new RefundsApprovingAuthoritiesMock());
             Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(3));
        CreatePOPSRController obj = new CreatePOPSRController(controller);
        obj.caseId = objCase.Id;
        obj.init();
    }   
}