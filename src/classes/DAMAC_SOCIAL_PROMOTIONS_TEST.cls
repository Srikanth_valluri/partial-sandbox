@istest
private class DAMAC_SOCIAL_PROMOTIONS_TEST {
    @testSetup static void setup() 
    {
        Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account portalAccount1 = new Account(
            LastName = 'TestAccount',
            recordTypeId = personAccRtId
        );
        Database.insert(portalAccount1);
    }
    @isTest static void testPromotionsPOST ()
    {
        Account agency = [SELECT Sendgrid_Id__c FROM Account WHERE Name != NULL LIMIT 1];
        
        Test.startTest ();
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/socialpromos';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf ('{"id": "'+agency.Sendgrid_Id__c+'", "Twitter" : {"twitter_id" : "13456"}}');
            RestContext.request = req;
            RestContext.response= res;
            
            DAMAC_SOCIAL_PROMOTIONS.doPost();
            
        Test.stopTest ();
        
        

    }
    
    @isTest static void testPromotionsGET ()
    {
        Account agency = [SELECT Sendgrid_Id__c FROM Account WHERE Name != NULL LIMIT 1];
        
        Test.startTest ();
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/socialpromos?cid='+agency.Sendgrid_Id__c;
            req.httpMethod = 'GET';
            RestContext.request = req;
            RestContext.response= res;
            
            DAMAC_SOCIAL_PROMOTIONS.doGet();
            
        Test.stopTest ();
        
        

    }
}