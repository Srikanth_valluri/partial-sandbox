public class AirwayBillTrackListResponse {
    public List<AirwayBillTrackList> AirwayBillTrackList{get;set;}
    public Integer Code{get;set;}
    public String Description{get;set;}
    
    
    
    public class AirwayBillTrackList {
        public String AirWayBillNo{get;set;}
        public String Destination{get;set;}
        public String ForwardingNumber{get;set;}
        public String Origin{get;set;}
        public String ShipmentProgress{get;set;}
        public String ShipperReference{get;set;}
        public List<TrackingLogDetails> TrackingLogDetails{get;set;}
        public String Weight;
        
        
        
        public AirwayBillTrackList(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'AirWayBillNo') {
                            AirWayBillNo = String.valueOf(parser.getText());
                        } else if (text == 'Destination') {
                            Destination = parser.getText();
                        } else if (text == 'ForwardingNumber') {
                            ForwardingNumber = parser.getText();
                        } else if (text == 'Origin') {
                            Origin = parser.getText();
                        } else if (text == 'ShipmentProgress') {
                            ShipmentProgress = parser.getText();
                        } else if (text == 'ShipperReference') {
                            ShipperReference = parser.getText();
                        } else if (text == 'TrackingLogDetails') {
                            TrackingLogDetails = arrayOfABT(parser); 
                        } else if (text == 'Weight') {
                            Weight = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'CollectionResponse consuming unrecognized property: '+text);
                            //consumeObject(parser);
                        }
                    }
                }
            }
        
        }
        private List<TrackingLogDetails> arrayOfABT(System.JSONParser p) {
            List<TrackingLogDetails> res = new List<TrackingLogDetails>();
            if (p.getCurrentToken() == null) p.nextToken();
            while (p.nextToken() != System.JSONToken.END_ARRAY) {
                res.add(new TrackingLogDetails(p));
            }
            return res;
        }
        
    }
    public class TrackingLogDetails {
        public String ActivityDate{get;set;}
        public String ActivityTime{get;set;}
        public String DeliveredTo{get;set;}
        public String Location{get;set;}
        public String Remarks{get;set;}
        public String Status{get;set;}
        
        public TrackingLogDetails(JSONParser parser ) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'ActivityDate') {
                            ActivityDate = String.valueOf(parser.getText());
                        } else if (text == 'ActivityTime') {
                            ActivityTime = parser.getText();
                        } else if (text == 'DeliveredTo') {
                            DeliveredTo = parser.getText();
                        } else if (text == 'Location') {
                            Location = parser.getText();
                        } else if (text == 'Remarks') {
                            Remarks = parser.getText();
                        } else if (text == 'Status') {
                            Status = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'CollectionResponse consuming unrecognized property: '+text);
                            //consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public AirwayBillTrackListResponse(JSONParser parser) {
        while (parser.nextToken() != System.JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                    if (text == 'Code') {
                        Code = parser.getIntegerValue();
                    } else if (text == 'Description') {
                        Description = parser.getText();
                    } else if (text == 'AirwayBillTrackList') {
                        AirwayBillTrackList = arrayOfABT(parser); 
                    } 
                }
            }
        }
    
    }
    
    private List<AirwayBillTrackList> arrayOfABT(System.JSONParser p) {
        List<AirwayBillTrackList> res = new List<AirwayBillTrackList>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new AirwayBillTrackList(p));
        }
        return res;
    }
    
    public static AirwayBillTrackListResponse parse(String json) {
        System.JSONParser parser = System.JSON.createParser(json);
        return new AirwayBillTrackListResponse(parser);
    }
    

}