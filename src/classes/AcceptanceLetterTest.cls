@isTest
public class AcceptanceLetterTest 
{
public static final String strBookingUnitActiveStatus = 'Agreement executed by DAMAC';
    
    static testmethod void testWithAttachments()
    {
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Address_Line_1__c = 'XYZ';
        acc.Country__c = 'Canada';
        acc.City__c = 'Barcelona';
        acc.State__c = 'lmn';
        acc.Zip_Postal_Code__c = '111';
        insert acc;
        
        Case objCase = new Case();
        objCase.Account = acc;
        objCase.Status = 'Submitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Type = 'Offer & Acceptance Letter';  
        objCase.Account_Email__c = 'test@gmail.com';
        insert objCase;
        
        SR_Attachments__c attachObj =new SR_Attachments__c();
        attachObj = TestDataFactory_CRM.createCaseDocument(objCase.Id, 'Offer & Acceptance Letter');
        insert attachObj;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        GenerateOfferAcceptanceLetterController objGenerate = new GenerateOfferAcceptanceLetterController(stdController);
        objGenerate.caseId = objCase.Id;
        objGenerate.generateOfferAcceptanceLetter();
    }
    
     static testmethod void testWhenStatusNotSubmitted()
     {
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Address_Line_1__c = 'XYZ';
        acc.Country__c = 'Canada';
        acc.City__c = 'Barcelona';
        acc.State__c = 'lmn';
        acc.Zip_Postal_Code__c = '111';

        insert acc;
        
        Case objCase = new Case();
        objCase.Account = acc;
        objCase.Status = 'Submitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Type = 'Offer & Acceptance Letter'; 
        objCase.Account_Email__c = 'test@gmail.com'; 
        insert objCase;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        GenerateOfferAcceptanceLetterController objGenerate = new GenerateOfferAcceptanceLetterController(stdController);
        objGenerate.caseId = objCase.Id;
        objGenerate.generateOfferAcceptanceLetter();
    }
    
     static testmethod void testWhenNoApprovalStatus()
     {
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Address_Line_1__c = 'XYZ';
        acc.Country__c = 'Canada';
        acc.City__c = 'Barcelona';
        acc.State__c = 'lmn';
        acc.Zip_Postal_Code__c = '111';
        insert acc;
        
        Case objCase = new Case();
        objCase.Account = acc;
        objCase.Status = 'Submitted';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.Approval_Status__c = '';
        objCase.Type = 'Offer & Acceptance Letter'; 
        objCase.Account_Email__c = 'test@gmail.com'; 
        insert objCase;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
        GenerateOfferAcceptanceLetterController objGenerate = new GenerateOfferAcceptanceLetterController(stdController);
        objGenerate.caseId = objCase.Id;
        objGenerate.generateOfferAcceptanceLetter();
    }
    
    static testmethod void testJSON()
    {
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        List<SR_Booking_Unit__c> srBookingUnitList = new List<SR_Booking_Unit__c>();
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Address_Line_1__c = 'XYZ';
        acc.Country__c = 'Canada';
        acc.City__c = 'Barcelona';
        acc.State__c = 'lmn';
        acc.Zip_Postal_Code__c = '111';
        insert acc;
        
        Case objCase = new Case();
        objCase.Account = acc;
        objCase.Status = 'Submitted';
        objCase.Approval_Status__c = 'Approved';
        objCase.Is_New_Payment_Terms_Applied__c = True;
        objCase.NewPaymentTermJSON__c = '[{"strTermID":"145486","strStatus":null,"strRegID":null,"strPaymentDate":null,"strPaidAmount":"98822.84","strLineID":"808896","strInvoiceAmount":"98860.8","strDueAmount":"37.96","percentValue":"24","paymentDate":"06-DEC-2015","name":"Date","mileStoneEventArabic":"فورا","mileStoneEvent":"Immediate","isReceiptPresent":true,"installment":"DP","description":"DEPOSIT","blnNewTerm":null},{"strTermID":"145487","strStatus":null,"strRegID":null,"strPaymentDate":null,"strPaidAmount":null,"strLineID":"808897","strInvoiceAmount":null,"strDueAmount":null,"percentValue":"0","paymentDate":"","name":"Date","mileStoneEventArabic":null,"mileStoneEvent":null,"isReceiptPresent":true,"installment":"I001","description":"1ST INSTALMENT","blnNewTerm":null},{"strTermID":"145488","strStatus":null,"strRegID":null,"strPaymentDate":null,"strPaidAmount":"0","strLineID":"808898","strInvoiceAmount":"49430.4","strDueAmount":"49430.4","percentValue":"10","paymentDate":"03-JUN-2016","name":"Date","mileStoneEventArabic":"عند أو قبل","mileStoneEvent":"On or Before","isReceiptPresent":false,"installment":"I002","description":"2ND INSTALMENT","blnNewTerm":null},{"strTermID":"145489","strStatus":null,"strRegID":null,"strPaymentDate":null,"strPaidAmount":"0","strLineID":"808899","strInvoiceAmount":"49430.4","strDueAmount":"49430.4","percentValue":"10","paymentDate":"02-AUG-2016","name":"Date","mileStoneEventArabic":"عند أو قبل","mileStoneEvent":"On or Before","isReceiptPresent":false,"installment":"I003","description":"3RD INSTALMENT","blnNewTerm":null},{"strTermID":"145490","strStatus":null,"strRegID":null,"strPaymentDate":null,"strPaidAmount":"0","strLineID":"808900","strInvoiceAmount":"49430.4","strDueAmount":"49430.4","percentValue":"10","paymentDate":"30-NOV-2016","name":"Date","mileStoneEventArabic":"عند أو قبل","mileStoneEvent":"On or Before","isReceiptPresent":false,"installment":"I004","description":"4TH INSTALMENT","blnNewTerm":null},{"strTermID":"145491","strStatus":null,"strRegID":null,"strPaymentDate":null,"strPaidAmount":null,"strLineID":"808901","strInvoiceAmount":null,"strDueAmount":null,"percentValue":"50","paymentDate":"30/11/2017","name":"Date","mileStoneEventArabic":"عند أو قبل","mileStoneEvent":"On or Before","isReceiptPresent":false,"installment":"I005","description":"5TH INSTALMENT","blnNewTerm":null}]';
        objCase.Type = 'Question';  
        objCase.Account_Email__c = 'test@gmail.com';
        insert objCase;
        
        Property__c objProperty = new Property__c();
        objProperty.Name = 'Test Project';
        objProperty.Property_Name__c = 'Test Property';
        objProperty.Property_ID__c = 3431;
        objProperty.CurrencyIsoCode = 'AED';
        insert objProperty;

        //create data for Inventory
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        objInventory.Unit__c = '1345';
        insert objInventory;

            //create Deal SR record
            NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
            insert objDealSR;

        //create Booking record for above created Deal and Account
            bookingList = TestDataFactory_CRM.createBookingForAccount(acc.Id,objDealSR.Id,1);
            insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList)
        {
          objBookingUnit.Registration_Status__c = strBookingUnitActiveStatus;
          objBookingUnit.Inventory__c = objInventory.Id;
          objBookingUnit.Registration_ID__c = '74712';
        }
        insert bookingUnitList;

        srBookingUnitList = TestDataFactory_CRM.createSRBookingUnis(objCase.Id , bookingUnitList);
        insert srBookingUnitList;
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorAOPT());
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1) );

            ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
            GenerateOfferAcceptanceLetterController objGenerate = new GenerateOfferAcceptanceLetterController(stdController);
            objGenerate.caseId = objCase.Id;
            
            objGenerate.generateOfferAcceptanceLetter();
        Test.stopTest();
    }

    static testmethod void insertRecords_test_scenario1()
    {   
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Address_Line_1__c = 'XYZ';
        acc.Country__c = 'Canada';
        acc.City__c = 'Barcelona';
        acc.State__c = 'lmn';
        acc.Zip_Postal_Code__c = '111';
        insert acc;

        Case objCase = new Case();
        objCase.Account = acc;
        objCase.Status = 'Submitted';
        objCase.Approval_Status__c = 'Approved';
        objCase.Type = 'Question';  
        objCase.Account_Email__c = 'test@gmail.com';
        insert objCase;

        Test.startTest();
            
            ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
            GenerateOfferAcceptanceLetterController objGenerate = new GenerateOfferAcceptanceLetterController(stdController);
            objGenerate.caseId = objCase.Id;
            objGenerate.objCase = objCase;
            
            //objGenerate.insertRecords();
        Test.stopTest();
    }

    static testmethod void insertRecords_test_scenario2()
    {   
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Address_Line_1__c = 'XYZ';
        acc.Country__c = 'Canada';
        acc.City__c = 'Barcelona';
        acc.State__c = 'lmn';
        acc.Zip_Postal_Code__c = '111';
        insert acc;

        Case objCase = new Case();
        objCase.AccountId = acc.Id;
        objCase.Status = 'Submitted';
        objCase.Approval_Status__c = 'Approved';
        objCase.Type = 'Question';  
        objCase.Account_Email__c = 'test@gmail.com';
        insert objCase;

        SR_Attachments__c objSRAttachment = new SR_Attachments__c();
        objSRAttachment.Case__c= objCase.Id;
        objSRAttachment.Type__c='Offer & Acceptance Letter';
        insert objSRAttachment;
        
        Test.startTest();
            
            ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
            GenerateOfferAcceptanceLetterController objGenerate = new GenerateOfferAcceptanceLetterController(stdController);
            objGenerate.caseId = objCase.Id;
            objGenerate.objCase = objCase;
            
            //objGenerate.insertRecords();
        Test.stopTest();
    }

    static testmethod void sendEmailTest1()
    {   
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Address_Line_1__c = 'XYZ';
        acc.Country__c = 'Canada';
        acc.City__c = 'Barcelona';
        acc.State__c = 'lmn';
        
        acc.Zip_Postal_Code__c = '111';
        insert acc;

        Case objCase = new Case();
        objCase.Account = acc;
        objCase.Status = 'Submitted';
        objCase.Approval_Status__c = 'Approved';
        objCase.Type = 'Question';  
        objCase.Account_Email__c = 'test@gmail.com';
        objCase.Offer_Acceptance_Letter_Generated__c = true;
        insert objCase;
        
        SR_Attachments__c objSRAttachment = new SR_Attachments__c();
        objSRAttachment.Case__c= objCase.Id;
        objSRAttachment.Type__c='Offer & Acceptance Letter';
        insert objSRAttachment;

        Test.startTest();
            
            ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
            GenerateOfferAcceptanceLetterController objGenerate = new GenerateOfferAcceptanceLetterController(stdController);
            objGenerate.caseId = objCase.Id;
            objGenerate.objCase = objCase;
            Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
            objGenerate.sendEmailFunction();
        Test.stopTest();
    }
    
    static testmethod void sendEmailTest2()
    {   
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Address_Line_1__c = 'XYZ';
        acc.Country__c = 'Canada';
        acc.City__c = 'Barcelona';
        acc.State__c = 'lmn';
        acc.Zip_Postal_Code__c = '111';
        insert acc;

        Case objCase = new Case();
        objCase.Account = acc;
        objCase.Status = 'Submitted';
        objCase.Approval_Status__c = 'Approved';
        objCase.Type = 'Question';  
        
        objCase.Offer_Acceptance_Letter_Generated__c = true;
        insert objCase;
        
        SR_Attachments__c objSRAttachment = new SR_Attachments__c();
        objSRAttachment.Case__c= objCase.Id;
        objSRAttachment.Type__c='Offer & Acceptance Letter';
        insert objSRAttachment;

        Test.startTest();
            
            ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
            GenerateOfferAcceptanceLetterController objGenerate = new GenerateOfferAcceptanceLetterController(stdController);
            objGenerate.caseId = objCase.Id;
            objGenerate.objCase = objCase;
            Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
            objGenerate.sendEmailFunction();
        Test.stopTest();
    }   
    static testmethod void sendEmailTest3()
    {   
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Address_Line_1__c = 'XYZ';
        acc.Country__c = 'Canada';
        acc.City__c = 'Barcelona';
        acc.State__c = 'lmn';
        acc.Zip_Postal_Code__c = '111';
        insert acc;

        Case objCase = new Case();
        objCase.Account = acc;
        objCase.Status = 'Submitted';
        objCase.Approval_Status__c = 'Approved';
        objCase.Type = 'Question';  
        objCase.Account_Email__c = 'test@gmail.com';
        insert objCase;
        
        SR_Attachments__c objSRAttachment = new SR_Attachments__c();
        objSRAttachment.Case__c= objCase.Id;
        objSRAttachment.Type__c='Offer & Acceptance Letter';
        insert objSRAttachment;

        Test.startTest();
            
            ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
            GenerateOfferAcceptanceLetterController objGenerate = new GenerateOfferAcceptanceLetterController(stdController);
            objGenerate.caseId = objCase.Id;
            objGenerate.objCase = objCase;
            Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
            objGenerate.sendEmailFunction();
        Test.stopTest();
    }

    static testmethod void testEarlySettelmentJordan()
    {   
        Account objAcc = TestDataFactory_CRM.createBusinessAccount();
        objAcc.Mobile__c ='1234567890';
        insert objAcc;
                
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;

        Id  bildingLocRecTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
        Location__c loc = new Location__c();
        loc.Location_ID__c = '22656';
        loc.Name = 'JNU';
        loc.recordTypeId = bildingLocRecTypeId;
        loc.Location_Name__c = 'test';
        loc.Address__c = 'test';
        loc.Contact__c = 'test';
        loc.Timings__c = 'test';
        loc.Map_URL__c = 'https://test';
        insert loc;
        
        Property__c objProperty = TestDataFactory_CRM.createProperty();
        insert objProperty;

        Inventory__c objInventory = TestDataFactory_CRM.createInventory(objProperty.Id);
        objInventory.Building_Location__c = loc.id;
        objInventory.property_Name__c = 'akoya oxygen';
        insert objInventory;

        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        //objBookingUnit.Mortgage__c = true;
        objBookingUnit.Inventory__c = objInventory.Id;
        objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_DateTime__c = System.now().addDays( 185 );
        objBookingUnit.Property_Country__c = 'Jordan';
        insert objBookingUnit;

        Case objCase = new Case();
        objCase.Account = objAcc;
        objCase.Status = 'Submitted';
        objCase.Approval_Status__c = 'Approved';
        objCase.Type = 'Question';  
        objCase.Account_Email__c = 'test@gmail.com';
        objCase.Early_Settlement_Case__c = true;
        insert objCase;

        SR_Booking_Unit__c srBu = new SR_Booking_Unit__c();
        srBu.Case__c = objCase.id;
        srBu.Booking_Unit__c = objBookingUnit.Id;
        insert srBu;

        SR_Attachments__c objSRAttachment = new SR_Attachments__c();
        objSRAttachment.Case__c= objCase.Id;
        objSRAttachment.Type__c='Offer & Acceptance Letter';
        insert objSRAttachment;

        Test.startTest();
            
            ApexPages.StandardController stdController = new ApexPages.StandardController(objCase);
            GenerateOfferAcceptanceLetterController objGenerate = new GenerateOfferAcceptanceLetterController(stdController);
            objGenerate.caseId = objCase.Id;
            objGenerate.objCase = objCase;
            Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
            objGenerate.sendEmailFunction();
        Test.stopTest();
    }
    
}