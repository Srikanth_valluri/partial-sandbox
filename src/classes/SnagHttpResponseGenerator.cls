@isTest

global class SnagHttpResponseGenerator implements HttpCalloutMock {

    public integer intResponseNumber ;

    public SnagHttpResponseGenerator () {

    }

    public SnagHttpResponseGenerator ( integer intNum ) {
        intResponseNumber = intNum ;
    }
    
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        System.debug('mock req endPoint : ' + req.getEndPoint());
        if(req.getEndPoint().contains('GetInspectionIssuesByRef')) {
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            String strResponse = '[{"Id":"testSCID","SDCode":"test SD code", "SDType":"test SD code",'
                                +' "Priority":"High", "Status":"Closed", "CurrentStatus":"Open",'
                                +'"SDDescription":"test descr", "CreatedDate": "019-01-14T13:53:59.813",'
                                +' "DueBy":"2019-01-21T00:00:00"}]';
            res.setBody(strResponse);    
            if(intResponseNumber == 0) {
                res.setHeader('Content-Type', 'application/json');
                res.setStatusCode(400);
                res.setBody('');
            }
        }
        else if(req.getEndPoint().contains('GetInspectionData')) {
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            res.setBody('[{"Data":"1", "Question":"Is Unit Ready for Handover?"}]');
            if(intResponseNumber == 0) {
                res.setHeader('Content-Type', 'application/json');
                res.setStatusCode(400);
                res.setBody('');
            }
        }
        else if (req.getEndpoint().contains('locations')) {            
            res.setHeader('Content-Type', 'application/json');
            res.setBody('[{"LocID":4756,"DrwgID":448,"Xcoord":111,"Ycoord":516,"Location":"Entrance","Datestamp":"2016-05-31T21:27:00Z","Lat":0.0,"Lng":0.0,"LocGUID":"7121008a-38e2-40b0-bf81-6ece51dcb2a2","DateSync":"2018-04-08T08:42:44.38Z","ExtRef":"SS/SD44/704","Closed":false,"RFDI":false,"LocationTypeID":0}]');
            res.setStatusCode(200);
            system.debug('!!!!!!intResponseNumber'+intResponseNumber);
            if(intResponseNumber == 0) {
                res.setHeader('Content-Type', 'application/json');
                res.setBody('');                
                res.setStatusCode(200);
                system.debug('!!!!!!!!!!!response'+res);
            } 
        }
        else if (req.getEndpoint().contains('inspections') && !req.getEndpoint().contains('inspections/{id}')) {            
            res.setHeader('Content-Type', 'application/json');
            res.setBody('[{"PlotID":47}]');
            res.setStatusCode(200); 
            
            if(intResponseNumber == 0) {
                res.setHeader('Content-Type', 'application/json');
                res.setBody('');                
                res.setStatusCode(200);                
            }           
        }
        else if (req.getEndPoint().contains('schedchecks')) {
            res.setHeader('Content-Type', 'application/json');
            res.setBody('[{"SCID":47,"PlotID":47,"Code":"SVRQ001","Description":"SVRQ","srt":"001","Qty":0.00,"Result":0,"Comment":"","UserID":0,"DateStamp":"2016-12-20T07:46:00Z","DueBy":null,"PPCGuid":"3AD6760D-411B-4CCF-9B5F-BBF0514F2559","DateSync":"2016-12-21T14:47:00Z","CantSkip":false,"Cost":0.00,"SCGuid":"FC7B2EE5-2FE8-47D6-B532-2686CCE05000","FormCode":"SVRQ","Percent":0,"Categ":"Form"}]');
            res.setStatusCode(200);  
            if(intResponseNumber == 0) {
                res.setHeader('Content-Type', 'application/json');
                res.setBody('');                
                res.setStatusCode(200);                
            } 
        }
        else if (req.getEndPoint().contains('FormPlotsLinkedSnags')) {
            res.setHeader('Content-Type', 'application/json');
            res.setBody('[89983,89984,89985,89986,89987,89988,89989,89990,89991,89992,89993,89994,89996,89997,89998,89999,90000,90001,90002,90003,90004,90005,90006,90008,90009,90010,90011,90012,90013,90014,90015,90016,90017,90018,90019,90020,90021,90022,90023,90024,90025,90026,90027,90028,90029,90030,90031,90032,90033,90034,90035,90036,90037,90038,90039,90040,90041,90042,90043,90044,90045,90046,90047,90048,90049,90050,90051,90052,90053,90054,90055,90056,90057,90058,90059,90060,90061,90062,90063]');
            res.setStatusCode(200); 
            if(intResponseNumber == 0) {
                res.setHeader('Content-Type', 'application/json');
                res.setBody('');                
                res.setStatusCode(200);                
            }
        }
        else if (req.getEndPoint().contains('defects')) {
            res.setHeader('Content-Type', 'application/json');
            res.setBody('[{"ID":90063,"GUID":"F2D0C0D7-C468-40A7-A3CE-90385A4D6ADB","DrawingID":448,"DrawingTitle":"SSSD44704","Status":"Open","CurrentStatus":"MCClosed","UserDescription":"Handed over villa hence closed","LocationID":4753,"Location":"Maids Room","LocationExtRef":"","LocationGUID":"","XCoord":246,"YCoord":538,"PriorityID":3,"Priority":"High","DueBy":"2016-12-16T08:42:00Z","DaysLeft":473,"GroupID":2,"GroupName":"Project Mgmt Team Client Inspections","GroupInitials":"PMT","DateClosed":"2018-04-03T09:50:23.817Z","BigPhoto":null,"Lattitude":0.0,"Longitude":0.0,"CreatedByID":21,"CreatedBy":"CREP Mahmoud Bseiso","CreatedDate":"2016-12-20T08:42:00Z","SDCategoryID":4,"SDCategory":"Joinery","SDCode":"STD015","SDType":"Doors","SDID":99,"SDDescription":"Door Shutter  - Clear width of shutter","DateSync":"2018-04-03T08:50:23.817Z","MPGUID":"10E4B99F-FD9C-47B1-A342-96B26608F686","FCGUID":"","FutureDefectGUID":"","PreviousDefectGUID":"","ProjectTitle":"AKOYA Villas - Silverspring","ContractNo":"GHANTOOT","Role":1,"UGUserID":85,"HistoryCount":4,"LastChanged":"2018-04-03T08:50:23.933Z","PhotoExists":true},{"ID":90062,"GUID":"42EA7914-EE69-434B-8910-017F1E8EFEF3","DrawingID":448,"DrawingTitle":"SSSD44704","Status":"Open","CurrentStatus":"MCClosed","UserDescription":"Handed over villa hence closed","LocationID":4753,"Location":"Maids Room","LocationExtRef":"","LocationGUID":"","XCoord":246,"YCoord":538,"PriorityID":3,"Priority":"High","DueBy":"2016-12-16T08:42:00Z","DaysLeft":473,"GroupID":2,"GroupName":"Project Mgmt Team Client Inspections","GroupInitials":"PMT","DateClosed":"2018-04-03T09:50:23.807Z","BigPhoto":null,"Lattitude":0.0,"Longitude":0.0,"CreatedByID":21,"CreatedBy":"CREP Mahmoud Bseiso","CreatedDate":"2016-12-20T08:41:00Z","SDCategoryID":3,"SDCategory":"Civil","SDCode":"STD012","SDType":"Walls","SDID":71,"SDDescription":"Finish & plumpness of plastering and paint.","DateSync":"2018-04-03T08:50:23.807Z","MPGUID":"10E4B99F-FD9C-47B1-A342-96B26608F686","FCGUID":"","FutureDefectGUID":"","PreviousDefectGUID":"","ProjectTitle":"AKOYA Villas - Silverspring","ContractNo":"GHANTOOT","Role":1,"UGUserID":85,"HistoryCount":4,"LastChanged":"2018-04-03T08:50:23.967Z","PhotoExists":true}]');
            res.setStatusCode(200); 
            if(intResponseNumber == 0) {
                res.setHeader('Content-Type', 'application/json');
                res.setBody('');                
                res.setStatusCode(200);                
            }
        }
        else if (req.getEndPoint().contains('inspections/{id}')) {
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"ProjectTitle":"DAMAC Heights","ContractNo":"","DrawingTitle":"44 1","DrwgID":335,"InspectionID":289,"InspectionName":"Service Request Form-SRF","InspectionExtRef":"DRZ/44/4401","InspectionInits":"SVRQ","Location":"Entrance","LocID":3132,"LocationExtRef":"DRZ/44/4401","CreatedDate":"2018-05-16T06:36:00","CreatedBy":"CREP Mahmoud Bseiso","CreatedByID":9,"Status":"Closed-P","ClosedDate":"2018-07-05T06:32:00","ClosedBy":"CREP amr.mohamed","ClosedByID":59,"UpdateDate":"2018-07-05T07:58:00","InspectionForms":[{"FormName":"Service Request Form","FormShortName":"SVRQ","FormID":289,"FormCode":"SVRQ","IssuedDate":"0001-01-01T00:00:00","ValudUntil":"0001-01-01T00:00:00","IssuedTo":"","IssuedBy":"","AcceptedBy":"","Closed":true,"Passed":true,"FormChecks":[{"CheckCode":"SVRQ0001","Categ":"","Question":"Is Unit Ready for Handover?","srt":"0001","Result":1,"ResultText":"Yes","ResultImg":null,"FIAID":67,"Username":"CREP Mahmoud Bseiso","UserID":9,"DateStamp":"2018-05-16T07:52:00","AnswerType":"ShortDate","Mandatory":false,"PageNo":1,"FCID":5174,"ListCode":"","LinkCheckCode":"","FCGUID":"282B7BD7-CA8E-43FB-B750-D14B6B9257DC","AutoSnagged":false}]}]}');
            res.setStatusCode(200); 
            if(intResponseNumber == 0) {
                res.setHeader('Content-Type', 'application/json');
                res.setBody('');                
                res.setStatusCode(200);                
            }

        }
        else if (req.getEndPoint().contains('SMSCwebservice_bulk.aspx')){            
            res.setHeader('Content-Type', 'application/json');
            res.setBody('SMS message(s) sent');
            res.setStatusCode(200);
            return res;
        }
        return res;
    }
}