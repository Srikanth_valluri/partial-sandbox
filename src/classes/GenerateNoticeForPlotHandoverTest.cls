/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GenerateNoticeForPlotHandoverTest {

    static testMethod void TestPositiveCase1() {
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
        Case objCase = new Case();
        objCase.Status = 'Submitted';
        objCase.Origin = 'Web';
        objCase.RecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Plot NOC').RecordTypeId;
        insert objCase;
        
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        PageReference pageRef = Page.GeneratePlotHandoverNotice;
        pageRef.getParameters().put('id', String.valueOf(objCase.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        GenerateNoticeForPlotHandover controller = new GenerateNoticeForPlotHandover(sc); 
        controller.init();
        controller.returnToCase();
        //controller.continueNotice();
        list<SelectOption> lstNotice = controller.getNotices();
        controller.selectedNotice = 'Approval of Contractor/Consultants';
        controller.generateNotice();
        Test.StopTest();
 
    }
    
    static testMethod void TestPositiveCase2() {
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
        Case objCase = new Case();
        objCase.Status = 'Submitted';
        objCase.Plot_Handover_Status__c = 'Handover Documents Signed';
        objCase.Origin = 'Web';
        objCase.RecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Plot NOC').RecordTypeId;
        insert objCase;
        
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        PageReference pageRef = Page.GeneratePlotHandoverNotice;
        pageRef.getParameters().put('id', String.valueOf(objCase.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        GenerateNoticeForPlotHandover controller = new GenerateNoticeForPlotHandover(sc); 
        controller.init();
        controller.returnToCase();
        //controller.continueNotice();
        list<SelectOption> lstNotice = controller.getNotices();
        controller.selectedNotice = 'Obtain Telecommunication Services';
        controller.generateNotice();
        controller.selectedNotice = 'Obtain Water and Electricity Services';
        controller.generateNotice();
        Test.StopTest(); 
    }
    
    static testMethod void TestPositiveCase3() {
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
        Case objCase = new Case();
        objCase.Status = 'Submitted';
        objCase.Plot_Handover_Status__c = 'Building Designs Approved by Design Team';
        objCase.Origin = 'Web';
        objCase.RecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Plot NOC').RecordTypeId;
        insert objCase;
        
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        PageReference pageRef = Page.GeneratePlotHandoverNotice;
        pageRef.getParameters().put('id', String.valueOf(objCase.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        GenerateNoticeForPlotHandover controller = new GenerateNoticeForPlotHandover(sc); 
        controller.init();
        controller.returnToCase();
        //controller.continueNotice();
        list<SelectOption> lstNotice = controller.getNotices();
        controller.selectedNotice = 'Building Design Plan';
        controller.generateNotice();
        controller.selectedNotice = 'Obtain Building Permit';
        controller.generateNotice();
        Test.StopTest(); 
    }
    
    static testMethod void TestPositiveCase4() {
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        
        insert TestDataFactory_CRM.createPersonAccount();
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id,IsPersonAccount from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 1);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 1);
        lstBookingUnits[0].Registration_ID__c = '74712';
        lstBookingUnits[0].Unit_Name__c = 'BUTest';
        lstBookingUnits[0].Unit_type__c = 'PLOT';
        lstBookingUnits[0].Handover_Flag__c  = 'N';     
        insert lstBookingUnits;
        
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
        Case objCase = new Case();
        objCase.Status = 'Submitted';
        objCase.Plot_Handover_Status__c = 'Building Designs Approved by Design Team';
        objCase.Origin = 'Web';
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.RecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Plot NOC').RecordTypeId;
        insert objCase;
        
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());        
        PageReference pageRef = Page.GeneratePlotHandoverNotice;
        pageRef.getParameters().put('id', String.valueOf(objCase.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        GenerateNoticeForPlotHandover controller = new GenerateNoticeForPlotHandover(sc); 
        controller.init();
        controller.returnToCase();
        //controller.continueNotice();
        list<SelectOption> lstNotice = controller.getNotices();
        controller.selectedNotice = 'Obtain Building Permit';
        controller.generateNotice();
        /*controller.selectedNotice = 'Obtain Demarcation Certificate';
		controller.generateNotice();*/
        Test.StopTest(); 
    }
    
    static testMethod void TestPositiveCase5() {
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;        
        
        insert TestDataFactory_CRM.createPersonAccount();
        List<Booking_Unit_Active_Status__c> lstActiveStatus = new List<Booking_Unit_Active_Status__c>();
        lstActiveStatus = TestDataFactory_CRM.createActiveFT_CS();
        insert lstActiveStatus;
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();
        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        Account objAcc = [SELECT Id,IsPersonAccount from Account LIMIT 1];
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 1);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 1);
        lstBookingUnits[0].Registration_ID__c = '74712';
        lstBookingUnits[0].Unit_Name__c = 'BUTest';
        lstBookingUnits[0].Unit_type__c = 'PLOT';
        lstBookingUnits[0].Handover_Flag__c  = 'Y';     
        insert lstBookingUnits;
        
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
        Case objCase = new Case();
        objCase.Status = 'Submitted';
        objCase.Plot_Handover_Status__c = 'Handover Documents Signed';
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        objCase.Origin = 'Web';
        objCase.RecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Plot NOC').RecordTypeId;
        insert objCase;
        
        SR_Attachments__c pccDoc = TestDataFactory_CRM.createCaseDocument(objCase.Id, 'Building Design Plan');
        insert pccDoc;
        
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        PageReference pageRef = Page.GeneratePlotHandoverNotice;
        pageRef.getParameters().put('id', String.valueOf(objCase.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        GenerateNoticeForPlotHandover controller = new GenerateNoticeForPlotHandover(sc); 
        controller.init();
        controller.returnToCase();
        //controller.continueNotice();
        list<SelectOption> lstNotice = controller.getNotices();
        controller.selectedNotice = 'Commencement of Construction';
        controller.generateNotice();
        Test.StopTest(); 
    } 
    
    static testMethod void TestPositiveCase6() {
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
        Case objCase = new Case();
        objCase.Status = 'Submitted';
        objCase.Plot_Handover_Status__c = 'Building Design Template Received';
        objCase.Origin = 'Web';
        objCase.RecordTypeId = Schema.SobjectType.Case.RecordTypeInfosByName.get('Plot NOC').RecordTypeId;
        insert objCase;
        
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        PageReference pageRef = Page.GeneratePlotHandoverNotice;
        pageRef.getParameters().put('id', String.valueOf(objCase.Id)); 
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        GenerateNoticeForPlotHandover controller = new GenerateNoticeForPlotHandover(sc); 
        controller.init();
        controller.returnToCase();
        //controller.continueNotice();
        list<SelectOption> lstNotice = controller.getNotices();
        controller.selectedNotice = 'Obtain Demarcation Certificate';
        controller.generateNotice();
        controller.selectedNotice = 'Obtain Building Permit';
        controller.generateNotice();
        Test.StopTest(); 
    }   
}