/**
 * Class to create or move to the saved existing SR record
 * Called for Convert to Agent button on Inquiry Object
 */
public with sharing class InquiryConvertToAgentController {

    public Boolean show             {get; set;} // Display error message
    public String myMsg             {get; set;} // Contains error message
    public Id inquiryId;                        // Inquiry Id to fetch complete record
    public Inquiry__c inquiryObj;               // Get Inquiry record to check Inquiry Status
    public NSIBPM__Service_Request__c srRecord; // Pass SR record if exist to the registration process

    /**
     * Method to get the Service record (if exist) related to the Inquiry
     */
    public InquiryConvertToAgentController(ApexPages.StandardController controller) {
        show = false;
        myMsg = '';
        inquiryId = controller.getId();
        System.debug('inquiryId>>> ' + inquiryId);
        if (String.isNotBlank(inquiryId)) {
            List<NSIBPM__Service_Request__c> srRecordsList = new List<NSIBPM__Service_Request__c>();
            srRecordsList = [  SELECT Id
                                    , Name
                                 FROM NSIBPM__Service_Request__c
                                WHERE Inquiry__c = :inquiryId
                                LIMIT 1
            ];

            System.debug('srRecordsList>>> ' + srRecordsList);
            if (!srRecordsList.isEmpty()) {
                //srRecord = srRecordsList[0];
                myMsg = 'ERROR: Service Request exist for the Inquiry ';
                show = true;
            } else {
                srRecord = null;
            }
        }
    }

    /**
     * Method to get the Inquiry Status and pass the InquiryId and
     * the SR record Id to the SRNavigationOverride VF page
     */
    public PageReference serviceRequestPage() {
        if (show) {
            return null;
        }
        inquiryObj = [ SELECT Id
                            , Name
                            , Inquiry_Status__c
                         FROM Inquiry__c
                        WHERE Id = :inquiryId
                        LIMIT 1
        ];
        System.debug('inquiryObj>>> ' + inquiryObj);

        if (inquiryObj != null) {
            if (inquiryObj.Inquiry_Status__c.equalsIgnoreCase('Potential Agent')) {
                PageReference srNavigationPage = new PageReference('/apex/SRNavigationOverride');
                if (srRecord != null) {
                    System.debug('inquiryObj>>> ' + inquiryObj);
                    srNavigationPage.getParameters().put('id', srRecord.Id);
                    srNavigationPage.getParameters().put('retURL', '/' + srRecord.Id);
                } else {
                    System.debug('inquiryObj>>> ' + inquiryObj);
                    srNavigationPage.getParameters().put('inquiryid', inquiryId);
                }
                srNavigationPage.setRedirect(true);
                return srNavigationPage;
            }
        }
        myMsg = ' Inquiry cannot be converted to Agent, if Inquiry Status is not \'Potential Agent\' ';
        show = true;
        return null;
    }

}