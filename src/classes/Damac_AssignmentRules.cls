public class Damac_AssignmentRules {
    public String selectedCriteria { get; set; }
    public List <Inquiry_Assignment_Rules__c> rules { get; set; }
    public Map <ID, User> userDetails { get; set; }
    public Map <ID, List <ID>> groupMembers { get; set; }
    public Inquiry__c inq { get; set; }
    
    public List <SelectOption> getRuleOptions () {
        List <SelectOption> options = new List <SelectOption> ();
        options.add (new selectOption ('', '--None--'));
        options.add (new selectOption ('Create', 'Create'));
        options.add (new selectOption ('Update', 'Update'));
        options.add (new selectOption ('Reassign', 'Reassign'));
        return options;
    }
    public Damac_AssignmentRules () {
        groupMembers = new Map <ID, List <ID>> ();
    	userDetails = new Map <Id, User> ();
        selectedCriteria = '';
        inq = new Inquiry__c ();
        rules = new List <Inquiry_Assignment_Rules__c> ();
    }
    public void deleteGroupMember () {
        String memberId = apexpages.currentpage().getparameters().get('memberId');
        String ownerId = apexpages.currentpage().getparameters().get('ownerId');
        
        delete [SELECT groupId, UserOrGroupId FROM GroupMember WHERE groupId =: ownerId and UserOrGroupId =: memberId];
        displayRules();
    }
    public void addGroupMember () {
        
        String groupId = apexpages.currentpage().getparameters().get('groupId');
        String userId = apexpages.currentpage().getparameters().get('userId');
        System.Debug(groupId+'==='+userId);
        insert (new GroupMember (UserOrGroupId = userId, groupId = groupId));
        displayRules();
    }
    public void displayRules () {
        rules = new List <Inquiry_Assignment_Rules__c> ();
        groupMembers = new Map <ID, List <ID>> ();
        Set <ID> userIds = new Set <ID> ();
        if (selectedCriteria != NULL && selectedCriteria != '') {
            rules = [SELECT Name, Priority__c, OwnerId, Owner.Name FROM Inquiry_Assignment_Rules__c 
                     WHERE Execute_on__c =: selectedCriteria 
                     AND Active__c = TRUE 
                     Order By Priority__c ASC];
            Map <ID, ID> ownerIds = new Map <ID, ID> ();
            for (Inquiry_Assignment_Rules__c rule :rules) {
                if (!String.valueOf (rule.ownerId).startsWith ('005')) {
                    groupMembers.put (rule.OwnerId, new List <ID> {rule.OwnerId});
                    ownerIds.put (rule.Id, rule.ownerId);
                } else {
                    groupMembers.put (rule.OwnerId, new List <ID> {rule.OwnerId});
                    userIds.add (rule.OwnerId);
                }
            }
            
            for (GroupMember member :[SELECT groupId, UserOrGroupId FROM GroupMember WHERE groupId IN: ownerIds.values()]) {
                userIds.add (member.userOrGroupId);
                if (groupMembers.containsKey (member.groupId))
                    groupMembers.get (member.groupId).add (member.UserOrGroupId);
                else
                    groupMembers.put (member.groupId, new List <ID> {member.UserOrGroupId});
            }
            userDetails = new Map <ID, User> ([SELECT Name FROM User WHERE ID IN :userIds ]);
            
        }
    }
}