@isTest
private class SendGridWithAttachmentsTest{

    @isTest static void testSend() {
        set<Id> lstAttach = new set<Id>();
        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Nationality__c = 'UAE';
        objAcc.Country__c = 'United Arab Emirates';
        insert objAcc ;
        
        Attachment objAttach = new Attachment();
        objAttach.Name = 'Test File';
        objAttach.Body = Blob.valueOf('Unit Test Attachment Body');
        objAttach.ParentId = objAcc.Id;
        insert objAttach;
        
        lstAttach.add(objAttach.Id);
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new SendGridResponseMock());
        HTTPResponse resp = SendGridWithAttachments.sendRequestToSendGrid('abc@xyz.com', 'new@old.com', 'With Attachment', 
                        'new', 'Example text', 'Example text', lstAttach, '');
        test.stopTest();
    
    }
}