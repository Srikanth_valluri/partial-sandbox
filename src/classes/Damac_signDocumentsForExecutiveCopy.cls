public class Damac_signDocumentsForExecutiveCopy implements Queueable, Database.allowsCallouts {

    Set <Id> srId = new Set<ID> ();
    public Damac_signDocumentsForExecutiveCopy (Set <ID> srRecIds) {
        srId = srRecIds;
    }
    public void execute(QueueableContext context) {
        Signnow_credentials__c credentials = Signnow_credentials__c.getInstance(UserInfo.getUserId());
        String accessToken = Damac_SignNowAPI.authenticate(credentials);
        
        for (ESign_Details__c uDocs :[SELECT Unit_Documents__r.Document_Name__c, Unit_Documents__r.Service_Request__c, Unit_Documents__r.CV_Id__c 
                                        FROM ESign_Details__c 
                                        WHERE Unit_Documents__r.Service_Request__c IN: srId AND Status__c = 'Signed'
                                        AND Signer_Type__c = 'Signer 2'
                                        AND Unit_Documents__r.CV_Id__c != null])
        {
            
            agentReviewDocuments (accessToken, uDocs.Id, uDocs.Unit_Documents__r.CV_Id__c, 
                                  uDocs.Unit_Documents__r.Document_Name__c, uDocs.Unit_Documents__r.Service_Request__c);
            
        }
        
        
    }
    @Future (Callout = true)
    public static void agentReviewDocuments (String accessToken, ID eSignID, ID contentVersionId, String documentName, Id srId) {
        Map <ID, ESign_Details__c> contentVersionIds = new Map <ID, ESign_Details__c> ();
        Signnow_Credentials__c credentials = Signnow_Credentials__c.getInstance ();
        for (ESign_Details__c uDocs :[SELECT Unit_Documents__r.Document_Name__c, 
                                      Unit_Documents__r.Service_Request__c, 
                                      Unit_Documents__r.Service_Request__r.Name,
                                      Unit_Documents__r.CV_Id__c 
                                        FROM ESign_Details__c 
                                        WHERE ID =: eSignID])
        {
            contentVersionIds.put (uDocs.Unit_Documents__r.CV_Id__c, uDocs);
        }
        String docuName = documentName.split (' ')[0]+'%';
        String docCountryname = documentName.split (' ')[0];
        String signer3Email = credentials.Third_Signer_Agent_Registration__c;
        for (Country_based_signing_Authorities__c authorities : [SELECT Signer_1__c, Signer_2__c FROM Country_based_signing_Authorities__c
                                                                    WHERE Name =: docCountryname ])
        {
            signer3Email = authorities.Signer_2__c;
        }
        System.debug (signer3Email);
        
        List<Agent_Sign_Positions__mdt> agentSignPositions = null;
        try{
            Unit_Documents__c unitDoc = [SELECT Document_Name__c, Service_Request__c FROM Unit_Documents__c
                                         WHERE Service_Request__c =: srId 
                                         AND Status__c = 'Generated' AND Document_Name__c LIKE :docuName]; 
            
            
            agentSignPositions  = [SELECT MasterLabel, Document_Name__c, Page_Number__c,Signer__c,
                                                                      X_Position__c, Y_Position__c, Width__c, Height__c,Required__c
                                                               FROM Agent_Sign_Positions__mdt
                                                               WHERE Document_Name__c =: unitDoc.Document_Name__c
                                                               AND Signer__c = '3' ORDER BY Page_Number__c];
       } catch(Exception e) {}
                                                           
        Map <ID, Map <String, String>> srUnitDocs = new Map <ID, Map <String, String>> ();
        
        for (ContentVersion version : [SELECT VersionData, Title FROM ContentVersion WHERE Id =: contentVersionId])
        {
            String documentId = Damac_SignNowAPI.uploadDocument(credentials, version, accessToken, null, agentSignPositions);
                
            List <String> documentIds = new List <String> ();
            documentIds.add (documentId);
            
            Map <String, HttpResponse> res = new Map <String, HttpResponse> ();
            
            res = Damac_SignNowAPI.sendInvitieToSign (credentials, documentIds, accessToken, signer3Email, '', '', 
                                                      contentVersionIds.get (version.id).Unit_Documents__r.Service_request__r.Name
                                                      +'-'+version.title);

            System.debug (res.get (documentId).getBody());
            
            if (res.get(documentId).getStatusCode () == 200) {
                String docName = contentVersionIds.get (version.id).Unit_Documents__r.Document_Name__c.split (' ')[0];
                Map <String, String> signNowDocumentIds = new Map <String, String> ();
                if (srUnitDocs.containsKey (contentVersionIds.get (Version.ID).Unit_Documents__r.Service_Request__c))
                    signNowDocumentIds = srUnitDocs.get (contentVersionIds.get (Version.ID).Unit_Documents__r.Service_Request__c);
                    
                signNowDocumentIds.put (docName, documentId);
                
                srUnitDocs.put (contentVersionIds.get (Version.ID).Unit_Documents__r.Service_Request__c, signNowDocumentIds);
            
            }
        }
        List <ESign_Details__c> eSignRecords = new List <ESign_Details__c> ();
        for (Unit_Documents__c doc :[SELECT Document_Name__c, Service_Request__c FROM Unit_Documents__c WHERE Service_Request__c IN : srUnitDocs.keySet () 
                                    AND Status__c = 'Pending Upload' AND Service_Request__r.NSIBPM__Internal_SR_Status__r.Name = 'Approved']) {
            if (srUnitDocs.containsKey(doc.Service_Request__c)) {
                
                Map <String, String> documentNames = srUnitDocs.get (doc.Service_Request__c);
                for (String key : documentNames.keySet()) {
                    if (doc.Document_Name__c.startsWith (key)) {
                        ESign_Details__c sDoc = new ESign_Details__c ();
                        sDoc.Document_Id__c = documentNames.get (key);
                        sDoc.Unit_Documents__c = doc.Id;
                        sDoc.Status__c = 'Email Sent';
                        sDoc.To_email__c = signer3Email;
                        eSignRecords.add (sDoc);
                    }
                }                      
            }
        }
        if (eSignRecords.size () > 0) {
            Database.insert(eSignRecords, false);
        
        }
    }
}