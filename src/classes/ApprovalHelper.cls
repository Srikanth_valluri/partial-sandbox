/*-------------------------------------------------------------------------------------------------
Description: ApprovalHelper

============================================================================================================================
    Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
    1.0     | 31-05-2018       | Ashish Agarwal   | 1.Added a new logic for submitting a case for approval.
=============================================================================================================================
*/

public without sharing class ApprovalHelper {

    public static list<Id> extractEligibleCaseIds( map<Id,FM_Case__c> mapNewFMCases, map<Id,FM_Case__c> mapOldFMCases ) {
        list<Id> lstEligibleCasesId = new list<Id>();
        id FMCaseAccessCardId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().
                                    get('Request For Access Card').getRecordTypeId();
        System.debug('FMCaseAccessCardId = ' + FMCaseAccessCardId);
        for( FM_Case__c objFMCase : mapNewFMCases.values() ) {
            system.debug('== Is locked=='+ objFMCase.Id + ' == '+Approval.isLocked( objFMCase.Id ));
            //System.debug('objFMCase = ' + JSON.serialize(objFMCase));
            //System.debug('mapOldFMCases.get( objFMCase.Id ) = ' + JSON.serialize(mapOldFMCases.get( objFMCase.Id )));
            System.debug('mapOldFMCases.get( objFMCase.Id ).Approval_Status__c = ' + mapOldFMCases.get( objFMCase.Id ).Approval_Status__c);
            System.debug('objFMCase.Approval_Status__c = ' + objFMCase.Approval_Status__c);

            System.debug('mapOldFMCases.get( objFMCase.Id ).Approving_Authorities__c = ' + mapOldFMCases.get( objFMCase.Id ).Approving_Authorities__c);
            System.debug('objFMCase.Approving_Authorities__c = ' + objFMCase.Approving_Authorities__c);

            System.debug('mapOldFMCases.get( objFMCase.Id ).Submit_for_Approval__c = ' + mapOldFMCases.get( objFMCase.Id ).Submit_for_Approval__c);
            System.debug('objFMCase.Submit_for_Approval__c = ' + objFMCase.Submit_for_Approval__c);

            System.debug('mapOldFMCases.get( objFMCase.Id ).Unit_Name__c = ' + mapOldFMCases.get( objFMCase.Id ).Unit_Name__c);
            System.debug('objFMCase.Unit_Name__c = ' + objFMCase.Unit_Name__c);
            // if(!Approval.isLocked( objFMCase.Id )) {
                System.debug('in app'+Approval.isLocked( objFMCase.Id ));
                System.debug('mapNewFMCases==== '+mapNewFMCases);
                System.debug('mapOldFMCases==== '+mapOldFMCases);
                
                if( String.isNotBlank( objFMCase.Approving_Authorities__c ) &&
                    String.isNotBlank( objFMCase.Approval_Status__c ) &&
                    objFMCase.Approval_Status__c.equalsIgnoreCase( 'Pending' ) &&
                    objFMCase.Submit_for_Approval__c == true &&
                    String.isNotBlank( objFMCase.Unit_Name__c ) &&
                    ( objFMCase.Approving_Authorities__c != mapOldFMCases.get( objFMCase.Id ).Approving_Authorities__c ||
                    objFMCase.Submit_for_Approval__c != mapOldFMCases.get( objFMCase.Id ).Submit_for_Approval__c ) ) {
                    lstEligibleCasesId.add( objFMCase.Id );
                } else if(mapOldFMCases.get( objFMCase.Id ).name ==NULL && String.isNotBlank( objFMCase.Approving_Authorities__c ) &&
                    String.isNotBlank( objFMCase.Approval_Status__c ) &&
                    objFMCase.Approval_Status__c.equalsIgnoreCase( 'Pending' ) &&
                    objFMCase.Submit_for_Approval__c == true &&
                    objFMCase.RecordTypeId == FMCaseAccessCardId &&
                    String.isNotBlank( objFMCase.Unit_Name__c )){
                    System.debug('ala else madhe ');    
                    lstEligibleCasesId.add( objFMCase.Id );
                }
            // }
        }
        return lstEligibleCasesId ;
    }

    public static list<Id> extractEligibleCaseToRejectIds(map<Id,FM_Case__c> mapNewFMCases, map<Id,FM_Case__c> mapOldFMCases){
        list<Id> lstEligibleCasesIdToReject = new list<Id>();
        for( FM_Case__c objFMCase : mapNewFMCases.values() ) {
            system.debug('== Is locked=='+ objFMCase.Id + ' == '+Approval.isLocked( objFMCase.Id ));
            system.debug('objFMCase.Approval_Status__c '+objFMCase.Approval_Status__c);
            system.debug('Submit_for_Approval__c============'+objFMCase.Submit_for_Approval__c );
            system.debug('objFMCase.Unit_Name__c'+objFMCase.Unit_Name__c);
            system.debug('mapOldFMCases.get( objFMCase.Id ).Current_Approver__c '+mapOldFMCases.get( objFMCase.Id ).Current_Approver__c);
            system.debug('objFMCase.Current_Approver__c '+objFMCase.Current_Approver__c);
            if( String.isNotBlank( objFMCase.Approval_Status__c ) &&
                objFMCase.Submit_for_Approval__c == true &&
                String.isNotBlank( objFMCase.Unit_Name__c ) &&
                String.isNotBlank( mapOldFMCases.get( objFMCase.Id ).Current_Approver__c ) &&
                 objFMCase.Current_Approver__c != mapOldFMCases.get( objFMCase.Id ).Current_Approver__c &&
                 mapOldFMCases.get( objFMCase.Id ).Current_Approver__c.contains('__') &&
                 objFMCase.Request_Type_DeveloperName__c.equalsIgnoreCase( 'Tenant_Registration' )) {
                     system.debug('ala aat');
                lstEligibleCasesIdToReject.add( objFMCase.Id );
            }
        }
        System.debug('lstEligibleCasesIdToReject============='+lstEligibleCasesIdToReject);
        return lstEligibleCasesIdToReject;
    }


    public static list<Id> extractApprovedCaseIds( map<Id,FM_Case__c> mapNewFMCases, map<Id,FM_Case__c> mapOldFMCases ) {
        list<Id> lstApprovedCasesId = new list<Id>();
        for( FM_Case__c objFMCase : mapNewFMCases.values() ) {
            if( String.isBlank( objFMCase.Approving_Authorities__c ) &&
                String.isNotBlank( mapOldFMCases.get( objFMCase.Id ).Approving_Authorities__c ) &&
                String.isNotBlank( objFMCase.Approval_Status__c ) &&
                objFMCase.Approval_Status__c.equalsIgnoreCase( 'Pending' ) &&
                objFMCase.Submit_for_Approval__c == true ) {
                lstApprovedCasesId.add( objFMCase.Id );
            }
        }
        return lstApprovedCasesId ;
    }

    public static set<String> extractLocationNames( list<FM_Case__c> lstEligibleCases ) {
        set<String> setLocationNames = new set<String>();
        for( FM_Case__c objFMCase : lstEligibleCases ) {
            if( String.isNotBlank( objFMCase.Unit_Name__c ) ) {
                setLocationNames.add( objFMCase.Unit_Name__c.subString( 0, objFMCase.Unit_Name__c.indexOf( '/' ) ) );
            }
        }
        return setLocationNames ;
    }

    public static map<String, map<String, Id>> getLocationRolesMap( list<FM_User__c> lstApprovingUsers ) {
        map<String, map<String, Id>> mapLocationRoleUser = new map<String, map<String, Id>>();
        if( lstApprovingUsers != NULL && !lstApprovingUsers.isEmpty() ) {
            for( FM_User__c objUser : lstApprovingUsers ) {
                if( mapLocationRoleUser.containsKey( objUser.Building__r.Name ) ) {
                    mapLocationRoleUser.get( objUser.Building__r.Name ).put( objUser.FM_Role__c, objUser.FM_User__c );
                }
                else {
                    mapLocationRoleUser.put( objUser.Building__r.Name, new map<String, Id>{ objUser.FM_Role__c => objUser.FM_User__c } ) ;
                }
            }
        }
        return mapLocationRoleUser ;
    }

    public static Approval.ProcessSubmitRequest createApprovalRequest( String strComments, Id objectId, Id approverId, String strProcessName ) {
        Approval.ProcessSubmitRequest objApprovalSubmitRequest = new Approval.ProcessSubmitRequest();
        objApprovalSubmitRequest.setComments( strComments );
        objApprovalSubmitRequest.setObjectId( objectId );
        objApprovalSubmitRequest.setProcessDefinitionNameOrId( strProcessName );
        objApprovalSubmitRequest.setSkipEntryCriteria( true );
        objApprovalSubmitRequest.setNextApproverIds( new Id[] { approverId } );
        return objApprovalSubmitRequest ;
    }

}