Global class DAMAC_sendSMSController {

    public List <Campaign_SMS__c> campaignSMSHistory { get; set; } 
    public ID selectedCampaignSMSID { get; set; }
    public Campaign__c campaign { get; set; }
    public Inquiry__c inquiry { get; set; }
    public campaign_SMS__c newSMSRequest { get; set; }
    public boolean showMsgDiv { get; set; }
    
    public String campaigns { get; set; }
    public String campaignCountry { get; set; }
    public String campaignCity { get; set; }
    public String campaignType { get; set; }
    public String campaignCategory { get; set; }
    
    public String inquirySource { get; set; }
    public String inquiryStatus { get; set; }
    public String inquiryClass { get; set; }
    public String inquiryRecordType { get; set; }
    
    public String phoneCodes { get; set; }
    public String nationalities { get; set; }
    public Integer inquiriesCount { get; set; }
    public String errorMessage { get; set; }
    
    List <String> campaignIdsList;
    List <String> countryList;
    List <String> cityList;
    List <String> typeList;
    List <String> categoriesList;
    List <String> sourceList;
    List <String> statusList;
    List <String> classList;
    List <String> recordTypeList;
    List <String> phoneCodesList;
    List <String> nationalitiesList;
    
    global DAMAC_sendSMSController () {
    campaignIdsList = new List <String> ();
    countryList = new List <String> ();
    cityList = new List <String> ();
    typeList = new List <String> ();
    categoriesList = new List <String> ();
    sourceList = new List <String> ();
    statusList = new List <String> ();
    classList = new List <String> ();
    recordTypeList = new List <String> ();
    phoneCodesList = new List <String> ();
    nationalitiesList = new List <String> ();
    
        campaignSMSHistory = new List <Campaign_SMS__c> ();
        campaigns = '';
        campaignCountry = '';
        campaignCity = '';
        campaignType = '';
        campaignCategory = '';
        
        inquirySource = '';
        inquiryStatus = '';
        inquiryClass = '';
        inquiryRecordType = '';
        
        phoneCodes = '';
        nationalities = '';
        inquiriesCount = 0;
        selectedCampaignSMSID = null;
        showMsgDiv = false;
        newSMSRequest = new campaign_SMS__c ();
        campaign = new Campaign__c ();
        inquiry = new Inquiry__c ();
        errorMessage = '';
        getSMSHistory ();
    }
    
    global static List <String> getPickListOptions (String objectAPI, String fieldAPI) {
        List <String> returnList = new List <String> ();
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get (objectAPI).getDescribe ().fields.getMap ();
        
        Schema.DisplayType fielddataType = fieldMap.get(fieldAPI).getDescribe().getType();
        List <Schema.Picklistentry> fld =fieldmap.get (fieldAPI).getDescribe ().getpicklistValues();
        for (integer i=0; i < fld.Size(); i++){
            returnList.add (fld[i].getvalue());
        }
        return returnList;
         
    }
    
    // Action to send the related records to page based on the search term for Campaigns, Phone Codes, Nationality
    @remoteAction 
    global static Map <String, String> getValues (String objectName, String fieldAPI, String query, String searchTerm) {
        Map <String, String> resultsMap = new Map <String, String> ();
        
        if (query != '' && query != NULL && objectName == 'Campaign__c') {
            for (SObject obj : Database.Query (query)) {
                if ((String.valueOf (obj.get ('Name'))).containsIgnoreCase (searchTerm)) {
                    resultsMap.put (obj.Id, (String) obj.get ('Name'));
                }
                if (obj.get ('Campaign_Name__c') != NULL) {
                    if ((String.valueOf (obj.get ('Campaign_Name__c'))).containsIgnoreCase (searchTerm)) {
                        resultsMap.put (obj.Id, (String) obj.get ('Campaign_Name__c'));
                    }
                }

            }
        }
        
        if (query != '' && query != NULL && objectName == 'Inquiry__c' && fieldAPI == 'RecordTypeId') {
            for (RecordType obj : Database.Query (query)) {
                resultsMap.put (obj.Id, obj.Name);
            }
        }
        
        if (query != '' && query != NULL && objectName == 'Inquiry__c' && fieldAPI == 'Class__c') {
            for (AggregateResult obj : Database.Query (query)) {
                 
                if (obj.get ('Class__c') != NULL) {
                    if ((String.valueOf (obj.get ('Class__c'))).containsIgnoreCase (searchTerm)) {
                        resultsMap.put ((String) obj.get ('Class__c'), (String) obj.get ('Class__c'));
                    }
                }

            }
        }
        
        List <String> values = new List <String> ();
        if (objectName != '' && objectName != NULL && fieldAPI != '' && fieldAPI != '')
            values = getPickListOptions (objectName, fieldAPI);
        
        if (values.size () > 0) {
            for (String key :values) {
                if (key.startsWithIgnoreCase (searchTerm)) {
                    resultsMap.put (key, key);
                }
            }
        }
        return resultsMap;
        
    }
    // To get the 20 recent SMS history records that are created previously
    global void getSMSHistory () {
        campaignSMSHistory = new list <Campaign_SMS__c> ();
        ID userId = UserInfo.getUserID ();
        campaignSMSHistory =  [ 
                                SELECT SMS_Message__c,
                                    createddate,
                                    SMS_Response__c,
                                    SMS_Queued__c,
                                    Total_Mobile_Numbers__c,
                                    SMS_Process_Complete__c ,Approval_Status__c,
                                    Total_Inquiries_in_the_Date_Range__c,
                                    Error_From_Provider__c,
                                    CreatedBy.Name  
                                    FROM Campaign_SMS__c 
                                    WHERE Marketing_Campaign__c = null
                                    AND OwnerId =: userId 
                                    ORDER BY CREATEDDATE DESC LIMIT 20 ];
                                    
    }
    // Method to submit the SMS history record for Approval, also creating a CSV File for the SMS history record with MObile Numbers
    public void submitForApproval (){
        Savepoint sp = Database.setSavepoint();
        try{
            System.Debug (newSMSRequest.sms_Message__c);
            if (newSMSRequest.sms_Message__c != null
                && newSMSRequest.sms_Message__c != ''
                && inquiriesCount > 0 
                && newSMSRequest.Scheduled_Date_Time__c != null 
                && newSMSRequest.Scheduled_Date_Time__c > system.now()) {
                
                
                newSMSRequest.Total_Inquiries_in_the_Date_Range__c =  inquiriesCount ;
                System.Debug (newSMSRequest);
                insert newSMSRequest;
                
                Database.executeBatch (new DAMAC_sendSMSBatchHelper (campaignIdsList, countryList, cityList, typeList, 
                                                                categoriesList, 
                                                                phoneCodesList, 
                                                                nationalitiesList, sourceList, classList, statusList, recordTypeList,
                                                                newSMSRequest.Inquiry_Start_Date__c, 
                                                                newSMSRequest.Inquiry_End_Date__c, newSMSRequest.ID), 200);
            
                
                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                req.setObjectId (newSMSRequest.id); 
                Approval.ProcessResult result = Approval.process(req); 
                newSMSRequest = new campaign_SMS__c(); 
                getSMSHistory();
                errorMessage = 'SMS Request Submitted Successfully';
                
            } else {
                errorMessage = 'Message cannot be blank or no inquiries in the date range or Scheduled date is blank or scheduled date should cannot be in Past';
                
            }
        }catch(exception e){
            ApexPages.addMessages (e);
            Database.rollback(sp);
        }
    }
    
    // Method to schedule the SMS job.
    public void callSMSJob () {
        errorMessage = '';
        List <Attachment> att = new List <Attachment> ();
        att = [ SELECT Id FROM Attachment WHERE ParentID =: selectedCampaignSMSID ];
        if (att.size () > 0) { 
            System.enqueueJob (new DAMAC_AsyncSendSMS (selectedCampaignSMSID, ''));
            Campaign_sms__c campSMS = new Campaign_sms__c ();
            campSMS.id = selectedCampaignSMSID;
            campSMS.SMS_Queued__c = true;
            update campSMS;
            getSMSHistory ();
            campSMS = new campaign_sms__c ();
            errorMessage = 'Message is Queued Successfully';
            
        }
        else {
            errorMessage = 'Message cannot be blank or no inquiries in the date range';
            
        }
    }
    public List <String> splitByComma (String value) {
        List <String> returnList = new List <String> ();
        if (value != '' && value != null) {
            if (value.contains (','))
               returnList = value.split (',');
           else
               returnList.add (value);
        }
        return returnList;
    
    }
    public void RecordCount (String soqlQuery) {        
        inquiriesCount = 0; 
        String UserId = UserInfo.getUserId();
        Send_SMS__c settings = Send_SMS__c.getInstance (UserInfo.getUserID ());
        String SerViceURL = settings.Salesforce_Base_URL__c;
        
        partnerSoapSforceCom.Soap partner = new partnerSoapSforceCom.Soap();
        partnerSoapSforceCom.SessionHeader_element header=new partnerSoapSforceCom.SessionHeader_element();
        header.sessionId = userInfo.getSessionID ();
        
        partnerSoapSforceCom.LoginResult result = new partnerSoapSforceCom.LoginResult ();
        partner.SessionHeader = header;
        partner.endpoint_x = SerViceURL+'/services/Soap/u/33.0';
        partner.timeout_x = 120000;
        
        try{
            partnerSoapSforceCom.QueryResult qr = partner.Query (soqlQuery);
            inquiriesCount = qr.size;
            system.debug (inquiriesCount);
            showMsgDiv = true;
            if (inquiriesCount > settings.Record_Count__c) {
                errorMessage = settings.Record_Limit_message__c;
                showMsgDiv = false;
            } 
            
        } catch(Exception e ) {
            errorMessage = e.getMessage ();
        }
        if (Test.isRunningTest ())
            inquiriesCount = 1;
    }
    
    // Method to fetch the Inquiry records count based on the criteria
    public void getFilterRecords () {
        
        showMsgDiv = false;
        errorMessage = '';
        inquiriesCount = 0;
        try {
            
            campaignIdsList = splitByComma(campaigns);
            countryList = splitByComma(campaignCountry);
            cityList = splitByComma (campaignCity);
            typeList = splitByComma (campaignType);
            categoriesList = splitByComma (campaignCategory);
            sourceList = splitByComma (inquirySource);
            statusList = splitByComma (inquiryStatus);
            classList = splitByComma (inquiryClass);
            recordTypeList = splitByComma (inquiryRecordType);
            phoneCodesList = splitByComma(phoneCodes);
            nationalitiesList = splitByComma (nationalities);
            
            
            String query = 'SELECT id FROM Inquiry__c '
                            +' WHERE Mobile_Phone_Encrypt__c != null ';
                            /*
                            +' AND Validity__c = \'Valid\''
                            +' AND Reachable__c = \'Reachable\''
                            +' AND Phone_Number_Type__c = \'Mobile\''
                            */
            query += ' AND (RecordType.name != \'CIL\' AND RecordType.name != \'Agent team\') ';
            query += ' AND Duplicate__c = False AND Inquiry_Owner__c != \'Queue\' AND';
            
            if (campaignIdsList.size() > 0) {
                query += '( ';
                for (String key : campaignIdsList)
                    query += '  Campaign__c = \''+key+'\' OR ';
                    
                
                query = query.removeEND ('OR ');
                query += ' ) AND';
            }
            if (countryList.size () > 0) {
                query += '( ';
                for (String key : countryList )
                    query += '  Campaign__r.Country__c = \''+key+'\' OR ';
                    
                query = query.removeEND ('OR ');
                query += ' ) AND';
            }
            
            if (cityList.size () > 0) {
                query += '( ';
                for (String key : cityList )
                    query += '  Campaign__r.City__c = \''+key+'\' OR ';
                    
                query = query.removeEND ('OR ');
                query += ' ) AND';
            }
            if (typeList.size () > 0) {
                query += '( ';
                for (String key : typeList)
                    query += '  Campaign__r.Campaign_Type_New__c = \''+key+'\' OR ';
                    
                query = query.removeEND ('OR ');
                query += ' ) AND';
            }  
            
            if (categoriesList.size () > 0) {
                query += '( ';
                for (String key : categoriesList)
                    query += '  Campaign__r.Campaign_Category_New__c = \''+key+'\' OR ';
                    
                query = query.removeEND ('OR ');
                query += ' ) AND';
            }    
                
            if (phoneCodesList.size () > 0) {
                query += '( ';
                for (String key : phoneCodesList)
                    query += '  Mobile_CountryCode__c = \''+key+'\' OR ';
                    
                query = query.removeEND ('OR ');
                query += ' ) AND';
            }
            
            if (nationalitiesList.size () > 0) {
                query += '( ';
                for (String key : nationalitiesList)
                    query += '  Nationality__c = \''+key+'\' OR ';
                    
                query = query.removeEND ('OR ');
                query += ' ) AND';
            }
            
            if (sourceList.size () > 0) {
                query += '( ';
                for (String key : sourceList)
                    query += '  Inquiry_Source__c = \''+key+'\' OR ';
                    
                query = query.removeEND ('OR ');
                query += ' ) AND';
            }
            
            if (classList.size () > 0) {
                query += '( ';
                for (String key : classList)
                    query += '  Class__c = \''+key+'\' OR ';
                    
                query = query.removeEND ('OR ');
                query += ' ) AND';
            }
            
            if (statusList.size () > 0) {
                query += '( ';
                for (String key : statusList)
                    query += '  Inquiry_Status__c = \''+key+'\' OR ';
                    
                query = query.removeEND ('OR ');
                query += ' ) AND';
            }
            
            if (recordTypeList.size () > 0) {
                query += '( ';
                for (String key : recordTypeList) {
                    query += '  recordTypeId = \''+key+'\' OR ';
                }
                query = query.removeEND ('OR ');
                query += ' ) AND';
            }
            
            if (newSMSRequest.Inquiry_Start_Date__c != null) {  
                Datetime dt = newSMSRequest.Inquiry_Start_Date__c;
                String str = dt.format('yyyy-MM-dd\'T\'kk:mm:ss\'z\''); 
                
                if (dt.hour() == 0) {
                    str = str.replace('T24', 'T00');
                }
                
                str = str.removeEnd ('z')+'+0400';          
                query += ' CreatedDate >='+str+' AND';
            }
            if (newSMSRequest.Inquiry_End_Date__c != null) {   
                Datetime dt = newSMSRequest.Inquiry_End_Date__c;
                if (dt.second() == 0)
                    dt = dt.addSeconds(60);
                String str = dt.format('yyyy-MM-dd\'T\'kk:mm:ss\'z\'');  
                
                if (dt.hour() == 0) {
                    str = str.replace('T24', 'T00');
                }
                
                str = str.removeEnd ('z')+'+0400';          
                query += ' CreatedDate <= '+str;
            }
                        
            query = query.removeEnd('AND');
            System.Debug (query);
            recordCount (query);
            
            
        }
        catch (Exception e) {
            errorMessage = e.getMessage ();
            
        }       
    }     
}