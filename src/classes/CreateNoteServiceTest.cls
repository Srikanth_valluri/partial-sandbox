@isTest
private class CreateNoteServiceTest
{
  @isTest static void testCreateNoteBasedOnCECommentsCollections() {

        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;

        list<Account> accountobj = TestDataFactory_CRM.createCustomers(1);
        insert accountobj;

        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( accountobj[0].Id, dealSR.Id, 1 );
        insert lstBookings;

        List<Booking_Unit__c> bookingUnitsObj = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        insert bookingUnitsObj;


        //String recordTypeDevName = ;
        List<Calling_List__c> listCallingList = TestDataFactory_CRM.createCallingList( 'Collections_Calling_List', 1 , bookingUnitsObj[0] );
        listCallingList[0].IsHideFromUI__c = false;
        listCallingList[0].Customer_Flag__c = true;
        insert listCallingList;
        system.debug(listCallingList);
        //RecoveryAndAddressFlagUpdateService obj = new RecoveryAndAddressFlagUpdateService();
        
        Test.startTest();
            listCallingList[0].CE_Comments__c = 'test comments';
            update listCallingList;
        Test.stopTest();
            
      }
     @isTest static void testCreateNoteBasedOnCECommentsMiscellaneous() {
     
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;  
     
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;

        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( accountobj[0].Id, dealSR.Id, 1 );
        insert lstBookings;

        List<Booking_Unit__c> bookingUnitsObj = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        insert bookingUnitsObj;

        List<Calling_List__c> listCallingList = TestDataFactory_CRM.createCallingList( 'Miscellaneous_List', 1 , bookingUnitsObj[0] );
        listCallingList[0].IsHideFromUI__c = false;
        listCallingList[0].Customer_Flag__c = true;
        insert listCallingList;
        system.debug(listCallingList);
        
        Test.startTest();
            listCallingList[0].CE_Comments__c = 'test comments';
            update listCallingList;
        Test.stopTest();
            
      }
      
    
  
    @isTest static void testCreateNoteBasedOnCECommentsDroopedCall() {
    
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2; 
    
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;

        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( accountobj[0].Id, dealSR.Id, 1 );
        insert lstBookings;

        List<Booking_Unit__c> bookingUnitsObj = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        insert bookingUnitsObj;

        List<Calling_List__c> listCallingList = TestDataFactory_CRM.createCallingList( 'Dropped_Calls_Call_Back', 1 , bookingUnitsObj[0] );
        listCallingList[0].IsHideFromUI__c = false;
        listCallingList[0].Customer_Flag__c = true;
        insert listCallingList;
        system.debug(listCallingList);
        
        Test.startTest();
            listCallingList[0].CE_Comments__c = 'test comments';
            update listCallingList;
        Test.stopTest();
  }  

    @isTest static void testCreateNoteBasedOnCECommentsRecovery() {
    
        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2; 
    
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;

        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( accountobj[0].Id, dealSR.Id, 1 );
        insert lstBookings;

        List<Booking_Unit__c> bookingUnitsObj = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        insert bookingUnitsObj;

        List<Calling_List__c> listCallingList = TestDataFactory_CRM.createCallingList( 'Recovery_Calling_List', 1 , bookingUnitsObj[0] );
        listCallingList[0].IsHideFromUI__c = false;
        listCallingList[0].Customer_Flag__c = true;
        insert listCallingList;
        system.debug(listCallingList);
        
        Test.startTest();
            listCallingList[0].CE_Comments__c = 'test comments';
            update listCallingList;
        Test.stopTest();
  }  
  
  @isTest static void testCreateNoteBasedOnCECommentsWalkin() {

        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        list<Account> accountobj =TestDataFactory_CRM.createCustomers(1);
        insert accountobj;

        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( accountobj[0].Id, dealSR.Id, 1 );
        insert lstBookings;

        List<Booking_Unit__c> bookingUnitsObj = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        insert bookingUnitsObj;

        List<Calling_List__c> listCallingList = TestDataFactory_CRM.createCallingList( 'Walk_In_Calling_List', 1 , bookingUnitsObj[0] );
        listCallingList[0].IsHideFromUI__c = false;
        listCallingList[0].Customer_Flag__c = true;
        insert listCallingList;
        system.debug(listCallingList);
        
        Test.startTest();
            listCallingList[0].CRE_Remarks__c = 'test comments';
            update listCallingList;
        Test.stopTest();
            
  }
}