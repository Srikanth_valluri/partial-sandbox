/************************************************************************************
* Description - Test class developed for 'actionCom'
*
* Version            Date            Author            Description
* 1.0                18/11/17        Naresh (Accely)           Initial Draft
*************************************************************************************/

@isTest(SeeAllData=false)
private class COCDFinalUpdateTest {

	 public static Account AccData;
     public static Case CaseData;
  
    @testsetup //Initial Test Data
    public static void TestData(){
  
    Id RecordTypeIdCase=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Change of Details').getRecordTypeId();
    AccData=TestDataFactory_CRM.createPersonAccount();
    Insert AccData;
    System.assert(AccData!=null);
    
    CaseData=TestDataFactory_CRM.createCase(AccData.Id,RecordTypeIdCase);
    Insert CaseData;
    
    System.assert(CaseData!=null);
    }
    

    // Test Method: UpdateCustomerInIpms & updateHandler()
	public static testmethod void Test_UpdateCustomerInIpms(){
		
		Set<string> caseids = new Set<string>();
		List<Case> lstsr = new List<Case>();
	    Case createCase = getCase();
	    
	    System.assertEquals(createCase.RecordType.Name,'Change of Details');
	   	System.assert(createCase != null);
	   	
		caseids.add(createCase.Id);  
		lstsr.add(createCase);
		
	    test.StartTest();
	    
	    SOAPCalloutServiceMock.returnToMe  = new Map<String,actionCom.UpdateCOCDResponse_element >();
	    actionCom.UpdateCOCDResponse_element  obj =  new actionCom.UpdateCOCDResponse_element ();
	    obj.return_x = 'S';
	    SOAPCalloutServiceMock.returnToMe.put('response_x',obj);
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
        
		COCDFinalUpdate.UpdateCustomerInIpms(caseids);
		COCDFinalUpdate.updateHandler(lstsr);
		test.StopTest(); 
	}
	
	
	// Test Method: UpdateNameNationalityInIpms
	public static testmethod void Test_UpdateNameNationalityInIpms(){
	
		Set<string> caseids = new Set<string>();
		List<Case> lstsr = new List<Case>();
	    Case createCase = getCase();
	    
	    createCase.Type = 'Name Nationality Change SR';
	    createCase.Passport_Issue_Date__c = Date.newInstance(2005, 12, 9);
	    update createCase;  
	    
	    System.assert(createCase != null);
	    System.assertEquals('Name Nationality Change SR' , createCase.Type);
	    
	    
	    lstsr.add(createCase);
		caseids.add(createCase.Id);  
	    
	    test.StartTest();
	    SOAPCalloutServiceMock.returnToMe  = new Map<String,NameAndNationalityService.NameAndNationalityProcessResponse_element>();
	    NameAndNationalityService.NameAndNationalityProcessResponse_element  obj =  new NameAndNationalityService.NameAndNationalityProcessResponse_element();
	    obj.return_x = 'S';
	    SOAPCalloutServiceMock.returnToMe.put('response_x',obj);
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
	   	COCDFinalUpdate.UpdateNameNationalityInIpms(caseids);
		COCDFinalUpdate.updateHandler(lstsr);
		test.StopTest(); 
	}
	
	
		// Test Method: LogError
	public static testmethod void Test_LogError(){
	    test.StartTest();
	    Case createCase = getCase();
		string errormsg = 'successfully';
		string caseid = createCase.Id;
		string processname = 'COD';
		COCDFinalUpdate.LogError(errormsg, caseid, processname);
		test.StopTest();
	}
	
	
	// Inner Class
	public static testmethod void Test_RuleEngineResponse(){
		COCDFinalUpdate.RuleEngineResponse objwrap = new COCDFinalUpdate.RuleEngineResponse();
		objwrap.regId ='Test';
        objwrap.adminFee ='Test';
        objwrap.UniqueTransactionNumber ='Test';
        objwrap.description ='Test';
        objwrap.allowed ='Test';
        objwrap.message='Test';
	
	}

	// Test Method: createDebitCreditMemoInIPMS
	public static testmethod void Test_createDebitCreditMemoInIPMS(){
	   
	   	string regid = '2345';
		string adminfee = 'Test';
		string description = 'Test';
		
	    test.StartTest();
		SOAPCalloutServiceMock.returnToMe  = new Map<String,actionCom.CreateDebitCreditMemoResponse_element>();
	    actionCom.CreateDebitCreditMemoResponse_element  obj =  new actionCom.CreateDebitCreditMemoResponse_element();
	    obj.return_x = 'S';  
	    SOAPCalloutServiceMock.returnToMe.put('response_x',obj);
	    Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
		COCDFinalUpdate.createDebitCreditMemoInIPMS(regid, adminfee, description);
		test.StopTest();
	}
     
   
      	// Test Method: callRuleEngine
	public static testmethod void Test_callRuleEngine(){
    
    	string partyid = '2548';
		string sourceofrequest = 'SFDC';
		string processname = 'COD';
		string subprocesname = 'COD';
		string customertype = 'Indivisual';
		string customernationality = 'Indian';
		
		test.StartTest();
	    SOAPCalloutServiceMock.returnToMe  = new Map<String,actionCom.COCDFinancialsResponse_element>();
	    actionCom.COCDFinancialsResponse_element  obj =  new actionCom.COCDFinancialsResponse_element();
	    obj.return_x = 'S';
	    SOAPCalloutServiceMock.returnToMe.put('response_x',obj);
      
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
	  	COCDFinalUpdate.callRuleEngine(partyid, sourceofrequest, processname, subprocesname, customertype, customernationality);
		test.StopTest();
	}
    
    // Test Method: getUndertakingChangeNameNationality
	public static testmethod void Test_getUndertakingChangeNameNationality(){
		test.StartTest();
		ChangeOfDetailController.lodauWrapper objwrap = new ChangeOfDetailController.lodauWrapper();
	    objwrap.RegId ='Test';
        objwrap.FirstName ='Test';
        objwrap.MiddleName ='Test';
        objwrap.LastName  ='Test';
        objwrap.Nationality   ='Test';
        objwrap.PassportNumber ='Test';
        objwrap.AddressLine1  ='Test';
        objwrap.AddressLine2  ='Test';
        objwrap.AddressLine3  ='Test';
        objwrap.AddressLine4 ='Test';
        objwrap.City ='Test';
        objwrap.Country ='Test';
        objwrap.HomePhone ='Test';
        objwrap.Fax  ='Test';
        objwrap.Mobile ='Test';
        objwrap.Email ='Test';
        objwrap.MasterDeveloper ='Test';
        objwrap.UnitName  ='Test';
        objwrap.PropertyName ='Test';
        objwrap.PropertyAddress ='Test';
        objwrap.PropertyCity ='Test';
        objwrap.SPADate  ='Test';
        objwrap.ChangedFirstName ='Test2';
        objwrap.ChangedMiddleName ='Test3';
        objwrap.ChangedLastName ='Test Test';
        objwrap.ArrayOfUnitName ='Test';
        objwrap.ArrayOfPropertyName ='Test';
        objwrap.ArrayOfPropertyAddress ='Test';   
        objwrap.ArrayOfNationality ='India';
        objwrap.OriginalNationality ='Test';
	  	
		SOAPCalloutServiceMock.returnToMe =  new Map<String,documentGeneration.DocGenerationResponse_element>();
		documentGeneration.DocGenerationResponse_element response_x = new documentGeneration.DocGenerationResponse_element();
		response_x.return_x = 'S';
		SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
		Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());

		COCDFinalUpdate.getUndertakingChangeNameNationality(objwrap);
		COCDFinalUpdate.getUndertakingChangeNationality(objwrap);
		test.StopTest();
	}
     

  // To get Case Records    
  public static Case getCase(){
    return [SELECT id,Customer_First_Name__c,Type,AccountId,RecordType.Name,Customer_First_Name_Arabic__c,Country__c,Country_Arabic__c,
    Address__c,Address_Arabic_1__c,Address_2__c,Address_Arabic_2__c,Address_3__c,Address_Arabic_3__c,Address_4__c,Address_Arabic_4__c,City__c,
    City_Arabic__c,State__c,State_Arabic__c,Postal_Code_Arabic__c,Contact_Email__c,Mobile_Country_Code__c,Contact_Mobile__c,
    Home_Phone_Country_Code__c,Home_Phone__c,Work_Phone_Country_Code__c,Work_Phone__c,Customer_Middle_Name__c,Customer_Middle_Name_Arabic__c,
    Customer_Last_Name__c,Customer_Last_Name_Arabic__c,CRF_File_URL__c,Passport_Issue_Date__c,Postal_Code__c,New_CR__c,Passport_Issue_Place__c,
    Passport_Issue_Place_Arabic__c,Changed_Nationality__c,POA_File_URL__c,Changed_Nationality_Arabic__c,
    Title__c,Title_Arabic__c FROM Case WHERE Credit_Note_Amount__c=5000];
  }
}