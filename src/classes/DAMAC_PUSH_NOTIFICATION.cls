/*
 * Class Name : Damac_FireBaseNotification
 * Description : To send the push notifications to the mobile using Fire base for damac agents App
 * */
public class DAMAC_PUSH_NOTIFICATION {

    // Method to send the notifications by taking the anouncement record ids as a parameter
    @invocableMethod
    public static void sendNotification(List <ID> recordIds) {
        sendNotificationaAsync(recordIds);
    }
    
    @Future(Callout = TRUE)
    Public static void sendNotificationaAsync(list<id> recordIds){
        GoogleFirebase_credentials__c credentials = GoogleFirebase_credentials__c.getInstance(userInfo.getUserID());        
        for (Announcement__c announcement : [SELECT Description__c,title__c,Push_Notification_to__c,(select id,URL__c from Announcement_sections__r where EDM_Type__c='English' and Type__c='Image' LIMIT 1) FROM Announcement__c WHERE ID IN: recordIds]) {
            string notificationImage='';
            if(announcement.Announcement_sections__r.size() > 0){
                notificationImage = announcement.Announcement_sections__r[0].URL__c;
            }
            if(announcement.Push_Notification_to__c != null){
                HTTPRequest req = new HTTPRequest ();
                req.setEndpoint (credentials.Endpoint__c);
                req.setMethod ('POST');            
                req.setHeader ('Authorization', 'key='+credentials.Server_Key__c);
                req.setHeader ('Content-Type', 'application/JSON');
                req.setBody(prepareReqBody(announcement,notificationImage));
                
                HTTP http = new HTTP ();
                HTTPResponse res = new HTTPResponse ();
                if(!test.isrunningtest()){
                    res = http.send (req);
                    System.Debug (res.getStatusCode ()+' ::::: Response Body :::: '+res.getBody ());
                    if(res.getStatusCode () == 200){
                        announcement.Notification_Resonse__c = res.getBody ();
                    }else{
                        announcement.Notification_Resonse__c = res.getStatusCode ()+'-'+res.getBody ().substring(0, 200);
                    }
                    update announcement;
                }
            }
        }                 
        
        for (Push_Notifications__c pushNotification : [SELECT Message__c, Title__c, Notification_Type__c,Notification_Image__c,
                                                              user__c,user__r.Device_Token_ID__c,Firebase__c
                                                         FROM Push_Notifications__c
                                                         WHERE ID IN :recordIds]) {
        
            
            if(pushNotification.user__r.Device_Token_ID__c != null){
                HTTPRequest req = new HTTPRequest ();
                req.setEndpoint (credentials.Endpoint__c);
                req.setMethod ('POST');            
                req.setHeader ('Authorization', 'key='+credentials.Server_Key__c);
                req.setHeader ('Content-Type', 'application/JSON');
                req.setBody(prepareNotificationReqBody (pushNotification));
                
                HTTP http = new HTTP ();
                HTTPResponse res = new HTTPResponse ();
                if(!test.isrunningtest()){
                    res = http.send (req);
                    System.Debug (res.getStatusCode ()+' ::::: Response Body :::: '+res.getBody ());                   
                    update pushNotification;
                }
            }
        }                 
    
    }
    //to handle push notification with Announcement Object
    public static String prepareReqBody (Announcement__c record,string notifyimage){
        String reqBody = '';
        DAMAC_PUSH_NOTIFICATION obj = new DAMAC_PUSH_NOTIFICATION();
        obj.to = '/topics/all';
        DAMAC_PUSH_NOTIFICATION.cls_notification notification = new DAMAC_PUSH_NOTIFICATION.cls_notification();
        notification.title = 'DAMAC-'+record.title__c;
        notification.body = record.Description__c;
        if(notifyimage !='' && notifyimage !=null){
            notification.image = notifyimage;
        }
        obj.notification = notification;
        reqBody = JSON.serialize(obj);
        System.debug ('::: Request Body :::'+reqBody);
        return reqBody;       
    }
    //to handle push notification with Push Notifications Object 
    public static String prepareNotificationReqBody (Push_Notifications__c record){
        String reqBody = '';        
        DAMAC_PUSH_NOTIFICATION obj = new DAMAC_PUSH_NOTIFICATION();         
        obj.to = record.User__r.Device_Token_ID__c;
        DAMAC_PUSH_NOTIFICATION.cls_notification notification = new DAMAC_PUSH_NOTIFICATION.cls_notification();
        notification.title = record.title__c;//'Notification from Damac Agent App';
        notification.body = record.message__c;//'Notification Body';           
        obj.notification = notification;
        
        DAMAC_PUSH_NOTIFICATION.cls_data data = new DAMAC_PUSH_NOTIFICATION.cls_data();
        data.type= record.Notification_Type__c.toUpperCase();
        data.userId= record.user__c;
        data.title = record.title__c;
        data.body = record.message__c;
        data.image = record.Notification_Image__c;
        obj.data = data;
        
        reqBody = JSON.serialize(obj);
        System.debug ('::: Request Body :::'+reqBody);
        return reqBody;       
    }
    public String to;   
    public cls_notification notification;
    public cls_data data;
    public class cls_notification {
        public String title;
        public String body;
        public string image;        
    }
    public class cls_data {
        public string type;
        public string userId;
        public String title;
        public String body;
        public string image;
    }
}