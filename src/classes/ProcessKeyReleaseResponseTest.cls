@isTest
public class ProcessKeyReleaseResponseTest {
    public ProcessKeyReleaseResponseTest() {
    }

    static testMethod void testAttachments() {
        TestUtility utility = new TestUtility();
        List<Case> caseList = new List<Case>();
        list<id> buId = new list<id>();

        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
          objBookingUnit.Okay_to_release_keys__c = true;
          objBookingUnit.Keys_Released__c = true;
        }
        insert bookingUnitList;
        
        for (Booking_Unit__c objBu : bookingUnitList) {
            buId.add(objBu.id);
        }

       Case obj=new Case();

       obj.Status='New';
       obj.Origin='Call';
       obj.Priority='Medium';
       obj.Booking_Unit__c = buId[0];
       obj.RecordTypeId = Schema.getGlobalDescribe().get('Case').getDescribe().getRecordTypeInfosByName().get('Handover').getRecordTypeId();
       insert obj;      
        
        List<Attachment> attachList = new List<Attachment>();
        for(Integer i=0;i<1;i++) {
            Attachment attobj=new Attachment();

            attobj.Name='Key Release Authorization Form.pdf';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            attobj.body=bodyBlob;
            attobj.ContentType = 'application/pdf';

            attobj.parentId=obj.Id;
            attachList.add(attobj);
        }//end for
        insert attachList;
        
        System.debug('attachList' + attachList);
        System.assertEquals(1, attachList.size());
        
        Test.startTest();
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1));
        Test.stopTest();
        
        ProcessKeyReleaseResponse controller = new ProcessKeyReleaseResponse();
        ProcessKeyReleaseResponse.ProcessResponse(attachList);
    }
}