@isTest
Public Class RebateOnAdvanceServiceTest {

    @isTest
    Public Static void test1() {
        //Credentials
        insert new List<Credentials_Details__c> { new Credentials_Details__c( Name = 'Rebate Get Receipt'
                                                                            , User_Name__c = 'test'
                                                                            , Password__c = 'test123'
                                                                            , Endpoint__c = 'bc1@z.com' ) 

                                                , new Credentials_Details__c( Name = 'Rebate Create Advance CM'
                                                                            , User_Name__c = 'test'
                                                                            , Password__c = 'test123'
                                                                            , Endpoint__c = 'bc1@z.com' )
                                                };

        // create Account
        Account objAcc = new Account(Name = 'test');
        insert objAcc;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        insert objBookingUnit;
        
        Id caseRecTypeId =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rebate On Advance').getRecordTypeId();
        Case objCase = new Case(Status = 'In Progress'
                                , AccountId = objAcc.Id
                                , recordTypeid = caseRecTypeId
                                , Booking_unit__c = objBookingUnit.Id);
        insert objCase;                     
        
        Task objTask = new Task(Subject = 'Check advance paid and apply 7% rebate as per policy'
                                , whatId = objCase.Id
                                , Assigned_User__c =    'Finance'
                                , Status = 'In Progress'
                                , Process_name__c = 'Overdue Rebate/Discount'
                                , CurrencyIsoCode = 'AED'
                                , ActivityDate = system.today().addDays(3)
                                , Priority = 'Normal');
        insert objTask;

        PageReference pageRef = Page.RebateServicePage;
        pageRef.getParameters().put('id', String.valueOf(objCase.Id));
        Test.setCurrentPage(pageRef);

        Test.startTest();
            Test.setMock(HttpCalloutMock.Class, new RebateServiceHttpMock(1));
            ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
            RebateOnAdvanceService controller = new RebateOnAdvanceService(sc);
        Test.stopTest();
    }
    
    @isTest
    Public Static void test2() {
        //Credentials
        insert new List<Credentials_Details__c> { new Credentials_Details__c( Name = 'Rebate Get Receipt'
                                                                            , User_Name__c = 'test'
                                                                            , Password__c = 'test123'
                                                                            , Endpoint__c = 'bc1@z.com' ) 

                                                , new Credentials_Details__c( Name = 'Rebate Create Advance CM'
                                                                            , User_Name__c = 'test'
                                                                            , Password__c = 'test123'
                                                                            , Endpoint__c = 'bc1@z.com' )
                                                };

        // create Account
        Account objAcc = new Account(Name = 'test');
        insert objAcc;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        insert objBookingUnit;
        
        Id caseRecTypeId =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rebate On Advance').getRecordTypeId();
        Case objCase = new Case(Status = 'In Progress'
                                , AccountId = objAcc.Id
                                , recordTypeid = caseRecTypeId
                                , Booking_unit__c = objBookingUnit.Id);
        insert objCase;                     
        
        Task objTask = new Task(Subject = 'Check advance paid and apply 7% rebate as per policy'
                                , whatId = objCase.Id
                                , Assigned_User__c =    'Finance'
                                , Status = 'In Progress'
                                , Process_name__c = 'Overdue Rebate/Discount'
                                , CurrencyIsoCode = 'AED'
                                , ActivityDate = system.today().addDays(3)
                                , Priority = 'Normal');
        insert objTask;
        PageReference pageRef = Page.RebateServicePage;
        pageRef.getParameters().put('id', String.valueOf(objCase.Id));
        Test.setCurrentPage(pageRef);

        Test.startTest();
            Test.setMock(HttpCalloutMock.Class, new RebateServiceHttpMock(2));
            ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
            RebateOnAdvanceService controller = new RebateOnAdvanceService(sc);
        Test.stopTest();
    }
    

    @isTest
    Public Static void test3() {
        //Credentials
        insert new List<Credentials_Details__c> { new Credentials_Details__c( Name = 'Rebate Get Receipt'
                                                                            , User_Name__c = 'test'
                                                                            , Password__c = 'test123'
                                                                            , Endpoint__c = 'bc1@z.com' ) 

                                                , new Credentials_Details__c( Name = 'Rebate Create Advance CM'
                                                                            , User_Name__c = 'test'
                                                                            , Password__c = 'test123'
                                                                            , Endpoint__c = 'bc1@z.com' )
                                                };

        // create Account
        Account objAcc = new Account(Name = 'test');
        insert objAcc;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        insert objBookingUnit;
        
        Id caseRecTypeId =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rebate On Advance').getRecordTypeId();
        Case objCase = new Case(Status = 'In Progress'
                                , AccountId = objAcc.Id
                                , recordTypeid = caseRecTypeId
                                , Booking_unit__c = objBookingUnit.Id);
        insert objCase;                     
        
        Task objTask = new Task(Subject = 'Check advance paid and apply 7% rebate as per policy'
                                , whatId = objCase.Id
                                , Assigned_User__c =    'Finance'
                                , Status = 'In Progress'
                                , Process_name__c = 'Overdue Rebate/Discount'
                                , CurrencyIsoCode = 'AED'
                                , ActivityDate = system.today().addDays(3)
                                , Priority = 'Normal');
        insert objTask;
        PageReference pageRef = Page.RebateServicePage;
        pageRef.getParameters().put('id', String.valueOf(objCase.Id));
        Test.setCurrentPage(pageRef);

        Test.startTest();
            Test.setMock(HttpCalloutMock.Class, new RebateServiceHttpMock(3));
            ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
            RebateOnAdvanceService controller = new RebateOnAdvanceService(sc);
        Test.stopTest();
    }

    @isTest
    Public Static void testNullTask() {
        //Credentials
        insert new List<Credentials_Details__c> { new Credentials_Details__c( Name = 'Rebate Get Receipt'
                                                                            , User_Name__c = 'test'
                                                                            , Password__c = 'test123'
                                                                            , Endpoint__c = 'bc1@z.com' ) 

                                                , new Credentials_Details__c( Name = 'Rebate Create Advance CM'
                                                                            , User_Name__c = 'test'
                                                                            , Password__c = 'test123'
                                                                            , Endpoint__c = 'bc1@z.com' )
                                                };

        // create Account
        Account objAcc = new Account(Name = 'test');
        insert objAcc;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        insert objBookingUnit;
        
        Id caseRecTypeId =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rebate On Advance').getRecordTypeId();
        Case objCase = new Case(Status = 'In Progress'
                                , AccountId = objAcc.Id
                                , recordTypeid = caseRecTypeId
                                , Booking_unit__c = objBookingUnit.Id);
        insert objCase;                     
        
        Task objTask = new Task(Subject = 'Check advance paid and apply 7% rebate as per policy'
                                , whatId = objCase.Id
                                , Assigned_User__c =    'Finance'
                                , Status = 'In Progress'
                                , Process_name__c = 'Overdue Rebate/Discount'
                                , CurrencyIsoCode = 'AED'
                                , ActivityDate = system.today().addDays(3)
                                , Priority = 'Normal');
        //insert objTask;

        PageReference pageRef = Page.RebateServicePage;
        pageRef.getParameters().put('id', String.valueOf(objCase.Id));
        Test.setCurrentPage(pageRef);

        Test.startTest();
            Test.setMock(HttpCalloutMock.Class, new RebateServiceHttpMock(1));
            ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
            RebateOnAdvanceService controller = new RebateOnAdvanceService(sc);
        Test.stopTest();
    }

    @isTest
    Public Static void testClosedlTask() {
        //Credentials
        insert new List<Credentials_Details__c> { new Credentials_Details__c( Name = 'Rebate Get Receipt'
                                                                            , User_Name__c = 'test'
                                                                            , Password__c = 'test123'
                                                                            , Endpoint__c = 'bc1@z.com' ) 

                                                , new Credentials_Details__c( Name = 'Rebate Create Advance CM'
                                                                            , User_Name__c = 'test'
                                                                            , Password__c = 'test123'
                                                                            , Endpoint__c = 'bc1@z.com' )
                                                };

        // create Account
        Account objAcc = new Account(Name = 'test');
        insert objAcc;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        insert objBookingUnit;
        
        Id caseRecTypeId =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rebate On Advance').getRecordTypeId();
        Case objCase = new Case(Status = 'In Progress'
                                , AccountId = objAcc.Id
                                , recordTypeid = caseRecTypeId
                                , Booking_unit__c = objBookingUnit.Id);
        insert objCase;                     
        
        Task objTask = new Task(Subject = 'Check advance paid and apply 7% rebate as per policy'
                                , whatId = objCase.Id
                                , Assigned_User__c =    'Finance'
                                , Status = 'Completed'
                                , Process_name__c = 'Overdue Rebate/Discount'
                                , CurrencyIsoCode = 'AED'
                                , ActivityDate = system.today().addDays(3)
                                , Update_ipms__c = true
                                , Priority = 'Normal');
        insert objTask;

        PageReference pageRef = Page.RebateServicePage;
        pageRef.getParameters().put('id', String.valueOf(objCase.Id));
        Test.setCurrentPage(pageRef);

        Test.startTest();
            Test.setMock(HttpCalloutMock.Class, new RebateServiceHttpMock(1));
            ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
            RebateOnAdvanceService controller = new RebateOnAdvanceService(sc);
        Test.stopTest();
    }

    @isTest
    Public Static void test_createcreateAdvanceCM1() {
        
        //Credentials
        insert new List<Credentials_Details__c> { new Credentials_Details__c( Name = 'Rebate Get Receipt'
                                                                            , User_Name__c = 'test'
                                                                            , Password__c = 'test123'
                                                                            , Endpoint__c = 'bc1@z.com' ) 

                                                , new Credentials_Details__c( Name = 'Rebate Create Advance CM'
                                                                            , User_Name__c = 'test'
                                                                            , Password__c = 'test123'
                                                                            , Endpoint__c = 'bc1@z.com' )
                                                };

        // create Account
        Account objAcc = new Account(Name = 'test');
        insert objAcc;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        insert objBookingUnit;
        
        Id caseRecTypeId =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rebate On Advance').getRecordTypeId();
        Case objCase = new Case(Status = 'In Progress'
                                , AccountId = objAcc.Id
                                , recordTypeid = caseRecTypeId
                                , Booking_unit__c = objBookingUnit.Id);
        insert objCase;

        Test.startTest();
        Test.setMock( HttpCalloutMock.class, new RebateQueueableHttpMock(1)); 
            RebateOnAdvanceService controller = new RebateOnAdvanceService();
            controller.registrationId = '24234';
            controller.caseId = objCase.Id;

            RebateOnAdvanceService.ReceiptIdWrapper wrapObj = new RebateOnAdvanceService.ReceiptIdWrapper();
            wrapObj.receiptId = 25545;
            wrapObj.isSelected = true;

            controller.lstReceiptIdwapper = new List<RebateOnAdvanceService.ReceiptIdWrapper> {wrapObj};
            controller.doValidation();
        Test.stopTest();
    }

    @isTest
    Public Static void test_createcreateAdvanceCM2() {
        
        //Credentials
        insert new List<Credentials_Details__c> { new Credentials_Details__c( Name = 'Rebate Get Receipt'
                                                                            , User_Name__c = 'test'
                                                                            , Password__c = 'test123'
                                                                            , Endpoint__c = 'bc1@z.com' ) 

                                                , new Credentials_Details__c( Name = 'Rebate Create Advance CM'
                                                                            , User_Name__c = 'test'
                                                                            , Password__c = 'test123'
                                                                            , Endpoint__c = 'bc1@z.com' )
                                                };

        // create Account
        Account objAcc = new Account(Name = 'test');
        insert objAcc;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        insert objBookingUnit;
        
        Id caseRecTypeId =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rebate On Advance').getRecordTypeId();
        Case objCase = new Case(Status = 'In Progress'
                                , AccountId = objAcc.Id
                                , recordTypeid = caseRecTypeId
                                , Booking_unit__c = objBookingUnit.Id);
        insert objCase;

        Test.startTest();
        Test.setMock( HttpCalloutMock.class, new RebateQueueableHttpMock(2)); 
            RebateOnAdvanceService controller = new RebateOnAdvanceService();
            controller.registrationId = '24234';
            controller.caseId = objCase.Id;

            RebateOnAdvanceService.ReceiptIdWrapper wrapObj = new RebateOnAdvanceService.ReceiptIdWrapper();
            wrapObj.receiptId = 25545;
            wrapObj.isSelected = true;

            controller.lstReceiptIdwapper = new List<RebateOnAdvanceService.ReceiptIdWrapper> {wrapObj};
            controller.doValidation();
        Test.stopTest();
    }

    @isTest
    Public Static void test_createcreateAdvanceCM3() {
        
        //Credentials
        insert new List<Credentials_Details__c> { new Credentials_Details__c( Name = 'Rebate Get Receipt'
                                                                            , User_Name__c = 'test'
                                                                            , Password__c = 'test123'
                                                                            , Endpoint__c = 'bc1@z.com' ) 

                                                , new Credentials_Details__c( Name = 'Rebate Create Advance CM'
                                                                            , User_Name__c = 'test'
                                                                            , Password__c = 'test123'
                                                                            , Endpoint__c = 'bc1@z.com' )
                                                };

        // create Account
        Account objAcc = new Account(Name = 'test');
        insert objAcc;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        insert objBookingUnit;
        
        Id caseRecTypeId =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rebate On Advance').getRecordTypeId();
        Case objCase = new Case(Status = 'In Progress'
                                , AccountId = objAcc.Id
                                , recordTypeid = caseRecTypeId
                                , Booking_unit__c = objBookingUnit.Id);
        insert objCase;

        Test.startTest();
        Test.setMock( HttpCalloutMock.class, new RebateQueueableHttpMock(3)); 
            RebateOnAdvanceService controller = new RebateOnAdvanceService();
            controller.registrationId = '24234';
            controller.caseId = objCase.Id;

            RebateOnAdvanceService.ReceiptIdWrapper wrapObj = new RebateOnAdvanceService.ReceiptIdWrapper();
            wrapObj.receiptId = 25545;
            wrapObj.isSelected = true;

            controller.lstReceiptIdwapper = new List<RebateOnAdvanceService.ReceiptIdWrapper> {wrapObj};
            controller.doValidation();
        Test.stopTest();
    }

    @isTest
    Public Static void test_doValidation1() {

        Id caseRecTypeId =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rebate On Advance').getRecordTypeId();
        Case objCase = new Case(Status = 'In Progress'
                                , recordTypeid = caseRecTypeId);

        Test.startTest();
        Test.setMock( HttpCalloutMock.class, new RebateQueueableHttpMock(1)); 
            RebateOnAdvanceService controller = new RebateOnAdvanceService();
            controller.registrationId = '24234';
            controller.caseId = objCase.Id;

            RebateOnAdvanceService.ReceiptIdWrapper wrapObj = new RebateOnAdvanceService.ReceiptIdWrapper();
            wrapObj.receiptId = 25545;
            wrapObj.isSelected = true;

            //controller.lstReceiptIdwapper = new List<RebateOnAdvanceService.ReceiptIdWrapper> {wrapObj};
            controller.doValidation();
        Test.stopTest();
    }

    @isTest
    Public Static void test_doValidation2() {
        Id caseRecTypeId =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rebate On Advance').getRecordTypeId();
        Case objCase = new Case(Status = 'In Progress'
                                , recordTypeid = caseRecTypeId);

        Test.startTest();
        Test.setMock( HttpCalloutMock.class, new RebateQueueableHttpMock(1)); 
            RebateOnAdvanceService controller = new RebateOnAdvanceService();
            //controller.registrationId = '24234';
            controller.caseId = objCase.Id;

            RebateOnAdvanceService.ReceiptIdWrapper wrapObj = new RebateOnAdvanceService.ReceiptIdWrapper();
            wrapObj.receiptId = 25545;
            wrapObj.isSelected = true;

            controller.lstReceiptIdwapper = new List<RebateOnAdvanceService.ReceiptIdWrapper> {wrapObj};
            controller.doValidation();
        Test.stopTest();
    }

    @isTest
    Public Static void test_doValidation3() {

        Test.startTest();
        Test.setMock( HttpCalloutMock.class, new RebateQueueableHttpMock(1)); 
            RebateOnAdvanceService controller = new RebateOnAdvanceService();
            controller.registrationId = '24234';
            //controller.caseId = objCase.Id;

            RebateOnAdvanceService.ReceiptIdWrapper wrapObj = new RebateOnAdvanceService.ReceiptIdWrapper();
            wrapObj.receiptId = 25545;
            wrapObj.isSelected = true;

            controller.lstReceiptIdwapper = new List<RebateOnAdvanceService.ReceiptIdWrapper> {wrapObj};
            controller.doValidation();
        Test.stopTest();
    }
}