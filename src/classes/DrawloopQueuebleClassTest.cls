/*-------------------------------------------------------------------------------------------------
Description: Test class for DrawloopQueuebleClass
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 14-04-2019       | Lochana Karle    | 1. Added functionality to test DrawloopQueuebleClass
=============================================================================================================================
*/
@isTest
private class DrawloopQueuebleClassTest{	
	private static testMethod void testOne(){
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
		 
        // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;

        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;        
        
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            objUnit.Registration_status__c = 'Agreement executed by DAMAC';
            objUnit.Registration_ID__c = '1234'; 
			objUnit.FM_Service_Notice__c = true;
			objUnit.Service_Notice_Sent_Date__c  = null;
        }
        insert lstBookingUnits;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        System.enqueueJob(new DrawloopQueuebleClass(lstBookingUnits));
        Test.stopTest();
    }
}