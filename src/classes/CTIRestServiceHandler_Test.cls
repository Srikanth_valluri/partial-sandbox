@isTest
public class CTIRestServiceHandler_Test
{
    static testmethod void method01 () {
        User u = [SELECT ID, Extension,Profile.Name FROM User where ID =: USERInfo.getUserID ()];
        
        if (u.Extension == NULL) {
            u.Extension = '0715';
            u.Languages_Known__c = 'Hindi';
            update u;
        }
        Set<String> usrEtnsnSet = New Set<String> ();
        
        Map <String, ID>  userExtensions = new Map <String, ID> ();
        for (User usr:[SELECT Extension, Languages_Known__c FROM User WHERE Extension =: u.Extension 
                    AND ISACTIVE = TRUE 
                    AND Is_Blacklisted__c = False 
                    AND isUserOnLeave__c = False]) {
            userExtensions.put (usr.Extension, usr.ID);
            usrEtnsnSet.add(usr.Extension);
            }
        
        Inquiry_User_Assignment_Rules__c rule = New Inquiry_User_Assignment_Rules__c ();
            rule.Net_Direct_Sales_Rank__c = 1;
            rule.setupOwnerId = userExtensions.values()[0];
        insert rule;
         String sInqStatus = System.Label.CTI_Inq_Status;
        List <String> ctiInqStatus = sInqStatus.split (',');
       
        
        Campaign__c camp = new Campaign__c ();
            camp.Start_Date__c = system.today().adddays(-3);
            camp.End_Date__c =  system.today().adddays(3);
            camp.Marketing_Start_Date__c =  system.today().adddays(-3);
            camp.Marketing_End_Date__c =  system.today().adddays(3);
            camp.Language__c = 'English';
        insert camp;
        
        Id inquiryRecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(DAMAC_Constants.INQUIRY_RT).getRecordTypeId();
        Inquiry__c inq = New Inquiry__c ();
            inq.First_Name__c = 'testInq';
            inq.Preferred_Language__c = 'English';
            inq.Last_Name__c ='inqLast';
            inq.Inquiry_Source__c = 'Prospecting';
            inq.Primary_Contacts__c = 'Mobile Phone';
            inq.Mobile_CountryCode__c = 'India: 0091';
            inq.Mobile_Phone__c = '1236547890';
            inq.Mobile_Phone_Encrypt__c = '1223467886';
            inq.Email__c = 'test@gmail.com';
            inq.Duplicate__c = false;
            inq.Inquiry_Status__c = ctiInqStatus[0];
            inq.recordTypeId = inquiryRecordTypeId;
            inq.Mobile_Phone_Encrypt_5__c = '8977365305';
            inq.Mobile_Phone_5__c = '8977365305';
            inq.Mobile_Country_Code_5__c = 'India: 0091';
            inq.Campaign__c =camp.id;
            inq.OwnerID = u.id;
        insert inq;
        
        List <String> lstPcExt = new List <String> ();
        lstPcExt.add (u.Extension);
         
        Virtual_Number__c v1 = new Virtual_Number__c ();
        v1.Name = '8977365305';
        v1.End_Date__c = system.today().adddays(3);
        v1.Start_Date__c = system.today().adddays(-3);
        v1.Active__c = TRUE;
        insert v1;
        
        JO_Campaign_Virtual_Number__c vNumber = new JO_Campaign_Virtual_Number__c ();
        vNumber.Related_Campaign__c = camp.ID;
        vNumber.Related_Virtual_Number__c = v1.ID;
        insert vNumber;
        
        Assigned_PC__c assign = new Assigned_PC__c ();
            assign.User__c = u.ID;
            assign.Start_Date__c = system.today().adddays(-3);
            assign.End_Date__c = system.today().adddays(3);
            assign.Campaign__c = camp.Id;
        insert assign;
        
        CTIRestServiceHandler.getEligibleExtensions ('8977365305', lstPcExt);
        CTIRestServiceHandler.checkforExistingOwnerExtension ('8977365305', usrEtnsnSet);
        CTIRestServiceHandler.checkCampaignExtension(usrEtnsnSet);
    
    }
     static testmethod void method02 () {
        User u = [SELECT ID, Extension,Profile.Name FROM User where ID =: USERInfo.getUserID ()
                                                               ];
      
        if (u.Extension == NULL) {
            u.Extension = '0715';
            update u;
        }
       
        List <String> lstPcExt = new List <String> ();
        lstPcExt.add (u.Extension);
        
        Campaign__c camp = new Campaign__c ();
            camp.Start_Date__c = system.today().adddays(-3);
            camp.End_Date__c =  system.today().adddays(3);
            camp.Marketing_Start_Date__c =  system.today().adddays(-3);
            camp.Marketing_End_Date__c =  system.today().adddays(3);
        insert camp;
        
        Virtual_Number__c v1 = new Virtual_Number__c ();
            v1.Name = '8977365305';
            v1.End_Date__c = system.today().adddays(3);
            v1.Start_Date__c = system.today().adddays(-3);
            v1.Active__c = TRUE;
        insert v1;
        
        JO_Campaign_Virtual_Number__c vNumber = new JO_Campaign_Virtual_Number__c ();
            vNumber.Related_Campaign__c = camp.ID;
            vNumber.Related_Virtual_Number__c = v1.ID;
        insert vNumber;
        
        Assigned_PC__c assign = new Assigned_PC__c ();
            assign.User__c = u.ID;
            assign.Start_Date__c = system.today().adddays(-3);
            assign.End_Date__c = system.today().adddays(3);
            assign.Campaign__c = camp.Id;
        insert assign;
        
        CTIRestServiceHandler.getEligibleExtensions ('8977365305', lstPcExt);
    
    }
}