/*********************************************************************************************************
* Name               : BulkUpdateController
* Test Class         : 
* Description        : Controller class for BulkUpdate VF Page
* Created Date       : 17/11/2020
* -----------------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst        17/11/2020      Initial Draft.
**********************************************************************************************************/
global With Sharing Class BulkUpdateController{
    Public Blob contentFile {get;set;}
    public List<String> fileLines; 
    public String loadMsg {get; set;}
    public List<String> csvFileLines{get;set;}
    Public String asString{get;set;}
    public string invListJSON {get; set;}
    public string invNamePriceMapJson {get; set;}
    public string invNameViewTypeMapJson {get; set;}
    public string invNameMarketingNameMapJson {get; set;}
    public string invNameBedroomTypeMapJson {get; set;}
    public string invNameStoreroomMapJson {get; set;}
    public List<String> invNameList; 
    Public Map<String, String> invNamePriceMap;
    Public Map<String, String> invNameViewTypeMap;
    Public Map<String, String> invNameMarketingNameMap;
    Public Map<String, String> invNameBedroomTypeMap;
    Public Map<String, String> invNameStoreroomMap;
    Public List<Inventory__c> invToUpdate {get; set;}

   
    /*********************************************************************************************
    * @Description : Controller class.
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public BulkUpdateController() {
        csvFileLines = new List<String>();
        fileLines = new List<String>();
        loadMsg = '';
        invNameList = new List<String>();
        invNamePriceMap = new Map<String, String>();
        invNameViewTypeMap = new Map<String, String>();
        invNameMarketingNameMap = new Map<String, String>();
        invNameBedroomTypeMap = new Map<String, String>();
        invNameStoreroomMap = new Map<String, String>();
        invToUpdate = new List<Inventory__c>();
        invListJSON = JSON.serialize(invToUpdate);
        invNamePriceMapJson = JSON.serialize(invNamePriceMap);
        invNameViewTypeMapJson = JSON.serialize(invNameViewTypeMap);
        invNameMarketingNameMapJson = JSON.serialize(invNameMarketingNameMap);
        invNameBedroomTypeMapJson = JSON.serialize(invNameBedroomTypeMap);
        invNameStoreroomMapJson = JSON.serialize(invNameStoreroomMap);
    }
    
    public void updateInventory(){
        loadMsg = '';
        Map<String, Id> markDocIdMap = new Map<String, Id>();
        system.debug('invToUpdate.size(): ' + invToUpdate.size());
        system.debug('invToUpdate: ' + invToUpdate);
        if(invToUpdate.size() > 0){
            system.debug('invNameMarketingNameMap.values(): ' + invNameMarketingNameMap.values());
            for(Marketing_Documents__c doc: [SELECT Id, Marketing_Name__c 
                                             FROM Marketing_Documents__c 
                                             WHERE Marketing_Name__c IN: invNameMarketingNameMap.values()]){
                markDocIdMap.put(doc.Marketing_Name__c, doc.Id);
            }
            system.debug('markDocIdMap: ' + markDocIdMap);
            List<Inventory_Log__c> invLogList = new List<Inventory_Log__c>();
            for(Inventory__c inv: invToUpdate){
                
                if(invNamePriceMap.containsKey(inv.Unit_Name__c)) {
                    Decimal newPrice = Decimal.valueOf(invNamePriceMap.get(inv.Unit_Name__c).trim());
                    if(inv.Special_Price__c != newPrice){
                        invLogList.add(InventoryProjectUtility.createInventoryLog(inv.Id, 'Special_Price__c', 
                                        String.valueOf(inv.Special_Price__c), String.valueOf(newPrice), 
                                        'Bulk Update - csv upload', ''));
                        inv.Special_Price__c = newPrice;
                    }
                }
                if(invNameViewTypeMap.containsKey(inv.Unit_Name__c)) {
                    String newViewType = invNameViewTypeMap.get(inv.Unit_Name__c).trim();
                    if(inv.View_Type__c != newViewType){
                        invLogList.add(InventoryProjectUtility.createInventoryLog(inv.Id, 'View_Type__c', 
                                        inv.View_Type__c, newViewType, 'Bulk Update - csv upload', ''));
                        inv.View_Type__c = newViewType;
                    }
                }
                if(invNameMarketingNameMap.containsKey(inv.Unit_Name__c)) {
                    String newMarkName = invNameMarketingNameMap.get(inv.Unit_Name__c).trim();
                    if(inv.Marketing_Name__c != newMarkName){
                        invLogList.add(InventoryProjectUtility.createInventoryLog(inv.Id, 'Marketing_Name__c', 
                                        inv.Marketing_Name__c, newMarkName, 'Bulk Update - csv upload', ''));
                        inv.Marketing_Name__c = invNameMarketingNameMap.get(inv.Unit_Name__c).trim();
                        if(markDocIdMap.containsKey(inv.Marketing_Name__c)){
                            inv.Marketing_Name_Doc__c = markDocIdMap.get(inv.Marketing_Name__c);
                        } else{
                            inv.Marketing_Name_Doc__c = null;
                        }
                    }
                }
                if(invNameBedroomTypeMap.containsKey(inv.Unit_Name__c)) {
                    String newBedroomType = invNameBedroomTypeMap.get(inv.Unit_Name__c).trim();
                    if(inv.Bedroom_Type__c != newBedroomType){
                        invLogList.add(InventoryProjectUtility.createInventoryLog(inv.Id, 'Bedroom_Type__c', 
                                        inv.Bedroom_Type__c, newBedroomType, 'Bulk Update - csv upload', ''));
                        inv.Bedroom_Type__c = invNameBedroomTypeMap.get(inv.Unit_Name__c).trim();
                    }
                }
                if(invNameStoreroomMap.containsKey(inv.Unit_Name__c)) {
                    String newstoreroom = invNameStoreroomMap.get(inv.Unit_Name__c).trim();
                    if(inv.Store_Room__c != newstoreroom ){
                        invLogList.add(InventoryProjectUtility.createInventoryLog(inv.Id, 'Store_Room__c', 
                                        inv.Store_Room__c, newstoreroom, 'Bulk Update - csv upload', ''));
                        inv.Store_Room__c = invNameStoreroomMap.get(inv.Unit_Name__c).trim();
                    }
                }
            }
            try{
                
                if(invLogList.size() > 0){
                    update invToUpdate;
                    insert invLogList;
                }
                invToUpdate = new List<Inventory__c>();
                for(Inventory__c inv: [SELECT Id, Name, Unit_Name__c, Bedroom_Type__c,
                                        Special_Price__c, Building_Location__r.Building_Name__c,
                                        Building_Location__c, Space_Type_Lookup_Code__c, Store_Room__c,
                                        View_Type__c, Type_Mktg__c, Unit_Area_sft__c, status__c,
                                        Property_Name_2__c, Marketing_Name__c, Total_Area_JOPD_sft__c
                                       FROM Inventory__c
                                       WHERE Unit_Name__c IN: invNameList]){
                    invToUpdate.add(inv);
                }
                system.debug('invToUpdate: ' + invToUpdate);
                if(invToUpdate.size() > 0){
                    loadMsg = 'Inventories Updated Successfully';
                    invListJSON = JSON.serialize(invToUpdate);
                }
            } catch (exception e){
                loadMsg = 'An Error Occured while Updating Inventories, please contact the administrator.';
                System.debug('Exception: ' + e.getMessage() + ': ' +  e.getLineNumber());
            }
        }
    }
    
    public void importDataFromCSVController(){
        loadMsg = '';
        String fileString =  system.ApexPages.currentPage().getParameters().get('fileBlob');
        contentFile = EncodingUtil.base64Decode(fileString);
        system.debug('contentFile:' + contentFile); 
        Integer priceIndex, viewTypeIndex, markNameIndex, bedroomTypeIndex, storeroomIndex;
        invNamePriceMap = new Map<String, String>();
        invNameViewTypeMap = new Map<String, String>();
        invNameMarketingNameMap = new Map<String, String>();
        invNameBedroomTypeMap = new Map<String, String>();
        invNameStoreroomMap = new Map<String, String>();
        try{
            if(contentFile != null){
                asString = blobToString(contentFile,'ISO-8859-1');
                fileLines= AsString.split('\n');
                system.debug('fileLines Size: ' + fileLines);
                if(fileLines[0] != null && fileLines[0] != ''){
                    Integer j=0;
                    String unitHeader = fileLines[0].split(',')[0];
                    for(String header: fileLines[0].split(',')){
                        if(priceIndex == null && header.toLowerCase().trim() == 'price'){
                            priceIndex = j;
                        } else if(viewTypeIndex == null && header.toLowerCase().trim() == 'view type'){
                            viewTypeIndex = j;
                        } else if(markNameIndex == null && header.toLowerCase().trim() == 'marketing name'){
                            markNameIndex = j;
                        } else if(bedroomTypeIndex == null && header.toLowerCase().trim() == 'bedroom type'){
                            bedroomTypeIndex = j;
                        } else if(storeroomIndex == null && header.toLowerCase().trim() == 'store room'){
                            storeroomIndex = j;
                        }
                        
                        j++;
                    }
                    system.debug('unitHeader: ' + unitHeader);
                    if(unitHeader == null || unitHeader == '' || unitHeader.toLowercase().trim() != 'unit'){
                        loadMsg = 'Please Upload a valid File';
                        system.debug('Invalid File - Invalid Headers');
                        
                    } else{
                        system.debug('Valid File');
                        for(Integer i=1; i<fileLines.size(); i++){
                            List<String> csvRecordData = new List<String>();
                            csvRecordData = fileLines[i].split(',');
                            System.debug('csvRecordData | ' + csvRecordData);
                                if(csvRecordData[0] != null && csvRecordData[0] != '' ){
                                invNameList.add(csvRecordData[0]);
                                if(csvRecordData.size() > priceIndex && priceIndex != null 
                                        && csvRecordData[priceIndex] != null && csvRecordData[priceIndex].trim() != '') {
                                    invNamePriceMap.put(csvRecordData[0], csvRecordData[priceIndex].trim());
                                }
                                if(csvRecordData.size() >  viewTypeIndex && viewTypeIndex != null 
                                        && csvRecordData[viewTypeIndex] != null && csvRecordData[viewTypeIndex].trim() != '') {
                                    invNameViewTypeMap.put(csvRecordData[0], csvRecordData[viewTypeIndex].trim());
                                }
                                if(csvRecordData.size() > markNameIndex && markNameIndex != null 
                                        && csvRecordData[markNameIndex] != null && csvRecordData[markNameIndex].trim() != '') {
                                    invNameMarketingNameMap.put(csvRecordData[0], csvRecordData[markNameIndex].trim());
                                }
                                if(csvRecordData.size() > bedroomTypeIndex && bedroomTypeIndex != null 
                                        && csvRecordData[bedroomTypeIndex] != null && csvRecordData[bedroomTypeIndex].trim() != '') {
                                    invNameBedroomTypeMap.put(csvRecordData[0], csvRecordData[bedroomTypeIndex].trim());
                                }
                                if(csvRecordData.size() > storeroomIndex && storeroomIndex != null 
                                        && csvRecordData[storeroomIndex] != null && csvRecordData[storeroomIndex].trim() != '') {
                                    invNameStoreroomMap.put(csvRecordData[0], csvRecordData[storeroomIndex].trim());
                                }
                                
                            }
                        }
                        system.debug('invNameList: ' + invNameList);
                        if(invNameList.size() > 0){
                            invNamePriceMapJson = JSON.serialize(invNamePriceMap);
                            invNameViewTypeMapJson = JSON.serialize(invNameViewTypeMap);
                            invNameMarketingNameMapJson = JSON.serialize(invNameMarketingNameMap);
                            invNameBedroomTypeMapJson = JSON.serialize(invNameBedroomTypeMap);
                            invNameStoreroomMapJson = JSON.serialize(invNameStoreroomMap);
                            invToUpdate = new List<Inventory__c>();
                            for(Inventory__c inv: [SELECT Id, Name, Unit_Name__c, Marketing_Name__c,
                                                    Special_Price__c, Building_Location__r.Building_Name__c,
                                                    Building_Location__c, Space_Type_Lookup_Code__c,
                                                    Bedroom_Type__c, View_Type__c, Store_Room__c,
                                                    Type_Mktg__c, Unit_Area_sft__c, status__c
                                                   FROM Inventory__c
                                                   WHERE Unit_Name__c IN: invNameList]){
                                invToUpdate.add(inv);
                            }
                            system.debug('invToUpdate: ' + invToUpdate);
                            system.debug('invToUpdate.size(): ' + invToUpdate.size());
                            if(invToUpdate.size() > 0){
                                loadMsg = 'File Uploaded Successfully';
                                invListJSON = JSON.serialize(invToUpdate);
                                
                            }
                        }
                    }
                }  else{
                     loadMsg = 'Please Upload a valid File';
                 }
             } else{
                 loadMsg = 'Please Upload a valid File';
             }
          } catch (Exception e) {
            loadMsg = 'An Error Occured while Uploading the File, please contact the administrator.';
            System.debug('Excetpion: ' + e.getMessage() + ': ' +  e.getLineNumber());
        } 
        system.debug('loadMsg: ' + loadMsg);
        
    }

    
    public static String blobToString(Blob input, String inCharset){
        
        String hex = EncodingUtil.convertToHex(input);
        System.assertEquals(0, hex.length() & 1);
        final Integer bytesCount = hex.length() >> 1;
        String[] bytes = new String[bytesCount];
        for(Integer i = 0; i < bytesCount; ++i)
            bytes[i] =  hex.mid(i << 1, 2);
        return EncodingUtil.urlDecode('%' + String.join(bytes, '%'), inCharset);
    } 
}