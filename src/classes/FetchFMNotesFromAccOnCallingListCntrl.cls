/***********************************************************************
* Class to fetch FM Notes from Account and calling list
**********************************************************************/
public without sharing class FetchFMNotesFromAccOnCallingListCntrl {
    
    public String callingListId                     {get;set;}
    public Id callingListRecordTypeId               {get;set;}
    public List<Calling_List__c> callingList        {get;set;}
    public List<Id>    accList                      {get;set;}
    public List<FM_Notes__c>  fmNotesList           {get;set;}
    
    public FetchFMNotesFromAccOnCallingListCntrl(ApexPages.StandardController controller) {
        fetchFMNotes();
    }
    
    /*This Method is used to fetch all the FM POP cases*/
    public void fetchFMNotes(){
        
        callingList = new List<Calling_List__c>();
        accList = new List<Id>();
        fmNotesList = new List<FM_Notes__c>();
        
        callingListRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('FM Collections').RecordTypeId;
        callingListId = ApexPages.currentPage().getParameters().get('id');
        callingListId = String.valueOf(callingListId).substring(0, 15);
        
        callingList = [ SELECT Id,
                            Name,
                            Account__r.Name,
                            Account__c,
                            RecordTypeId,
                            Case__c,
                            RecordType.Name
                       FROM Calling_List__c
                      WHERE Id = :callingListId
                        AND RecordTypeId = :callingListRecordTypeId ];

        /*for( Calling_List__c callingInst : callingList ) {
            if( callingInst.Account__c != null ) {
                accList.add(callingInst.Account__c);
            }
        }
        
        System.debug('accList::::'+accList);*/
        
        if( callingList != null && callingList.isEmpty() == false ) {
            fmNotesList = [SELECT Id,
                           Name,Account__c,Account__r.Name,
                           FMCallingList__c ,FMOutcome__c,
                           FM_Remarks__c,
                           FMCallingList__r.Name,
                           PTP_Date__c ,CreatedDate ,ownerId,owner.Name,
                           Result__c ,
                           Type__c 
                      FROM FM_Notes__c
                     WHERE (Account__c =: callingList[0].Account__c
                       OR FMCallingList__c =: callingListId)
                  ORDER BY CreatedDate desc limit 950 ];
                      
            System.debug('fmNotesList::in pop::'+fmNotesList);
        
        }//if
   
   }//fetchFMNotes
}