@isTest
private class UtillityForUploadTest {

    @isTest static void test_DocumrntUploadSuccess() {
        Credentials_Details__c settings = new Credentials_Details__c();
            settings.Name = 'office365RestServices';
            settings.User_Name__c = 'Some Value';
            settings.Password__c = 'Some Value';
            settings.Resource__c = 'Some Value';
            settings.grant_type__c = 'Some Value';
            insert settings;
        //Set mock response
        Test.setMock(HttpCalloutMock.class, new Office365RestServiceMock.Office365RestServiceMockPositive2());
        System.Test.startTest();
            UtillityForUpload.getOfficeURL ('String Url' ,'String fileName');
        System.Test.stopTest();

    }
}