/****************************************************************************************************
* Name               : PaymentPlanBuildingController
* Description        : Controller class for Payment Plan VF Page
* Created Date       : 16/10/2020
* Created By         : QBurst
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0         QBurst         16/10/2020      Initial Draft
****************************************************************************************************/

    global with sharing class PaymentPlanBuildingController {
    public String buildingId {get; set;}
    public Map<String, Id> planNamesMap {get; set;}
    public List<String> planNamesList {get; set;}
    public Integer floorCount {get; set;}
    public Integer unitCount {get; set;}
    public Location__c building {get; set;}
    public static string payplanJSON {get; set;}
    public Map<String, Date> payPlanEndDateMap { get; set;}
    public String payPlanEndDateMapJSON {get; set;}
    public String propertyName;
    /*********************************************************************************************
    * @Description : Constructor method
    * @Params      : void
    * @Return      : void
    *********************************************************************************************/
    public PaymentPlanBuildingController() {
        buildingId = ApexPages.currentPage().getParameters().get('Id');
        floorCount = 0;
        unitCount = 0; 
        payplanJSON = 'default';
        planNamesMap = new  Map<String, Id>();
        planNamesList = new List<String>();
        propertyName = '';
        payPlanEndDateMap = new Map<String, Date>();
        payPlanEndDateMapJSON = JSON.serialize(payPlanEndDateMap);

        if(buildingId != null && buildingId != ''){
            building = [SELECT Id, Name, Building_Name__c, Location_ID__c,
                                Property_Name__r.Property_Name__c, Property_Name__c
                               FROM Location__c 
                               WHERE Location_Type__c = 'Building'
                               AND Id =: buildingId];
            if(building.Property_Name__c != null && building.Property_Name__r.Property_Name__c != ''){
                propertyName = building.Property_Name__r.Property_Name__c;
            }                     
            for(Location__c loc: [SELECT Id, Name, Building_Name__c , Building_Number__c
                               FROM Location__c 
                               WHERE Location_Type__c = 'Floor'
                               AND Building_Number__c =: buildingId]){
                floorCount++;
            }
            for(Inventory__c inv: [SELECT Id FROM Inventory__c 
                                WHERE Building_Location__c =: buildingId]){
                unitCount++;
            }

            for(Payment_Plan__c payPlan: [SELECT id, Name, Building_ID__c, Booking_Unit__c, Payment_Term_Description__c,
                                        Active__c, Effective_To_calculated__c, Effective_from__c,Effective_To__c
                                 FROM Payment_Plan__c
                                 WHERE Effective_To_calculated__c >= TODAY
                                 AND Active__c = TRUE
                                 ORDER BY CreatedDate DESC
                                 LIMIT 1000]){
                String planName = payPlan.Name;
                if(payPlan.Payment_Term_Description__c != null  && payPlan.Payment_Term_Description__c != ''){
                    planName += ' - ' + payPlan.Payment_Term_Description__c;
                }
                planNamesList.add(planName);
                planNamesMap.put(planName, payPlan.Id);
            }
            fetchPayPlans();
        }
    }

    public void fetchPayPlans(){
        List<Payment_Plan__c> payPlanList = new List<Payment_Plan__c>();
        List<Id> payPlanIds = new List<Id>();
        for(Payment_Plan_Association__c planAssoc : [SELECT Id, Name, Payment_Plan__c, 
                                                        Location__c, Effective_From__c,
                                                        Effective_To__c, Property__c,
                                                        Property__r.Property_Name__c
                                                     FROM Payment_Plan_Association__c
                                                     WHERE Location__c =: buildingId]){
            payPlanIds.add(planAssoc.Payment_Plan__c);
            if(!payPlanEndDateMap.containsKey(planAssoc.Payment_Plan__c)){
                payPlanEndDateMap.put(String.valueOf(planAssoc.Payment_Plan__c), planAssoc.Effective_To__c);
            }
        }
        payPlanEndDateMapJSON = JSON.serialize(payPlanEndDateMap);
        system.debug('payPlanIds: ' + payPlanIds);
        for(Payment_Plan__c payPlan: [SELECT Id, Building_Location__c, Name, 
                                            Payment_Term_Description__c, Effective_To_calculated__c, 
                                            LastModifiedDate, Effective_To__c,
                                            (SELECT Id, Name,Installment__c,Description__c,Milestone_Event__c, CreatedDate,
                                                Milestone_Event_Arabic__c,Percent_Value__c FROM Payment_Terms__r) 
                                       FROM Payment_Plan__c WHERE Id IN: payPlanIds]){
                payPlanList.add(payPlan);
                system.debug('payPlanList'+payPlan);
            }
            system.debug('payPlanList'+payPlanList);
            payplanJSON = JSON.serialize(payPlanList);
    }

    public void associatePayPlan(){
        String payPlanId = ApexPages.currentPage().getParameters().get('payPlanId');
        String endDate = ApexPages.currentPage().getParameters().get('endDate');
        system.debug('payPlanId'+payPlanId);
        system.debug('endDate'+endDate);
        /*
        Payment_Plan__c explan = [select Id, Building_ID__c, Name, Building_Location__c,
                                  Effective_From__c,Effective_To__c ,
                                  (SELECT Id, Description__c, Milestone_Event__c, Line_ID__c,
                                                 Milestone_Event_Arabic__c, Percent_Value__c, Payment_Date__c,
                                                 Installment__c, Modified_Percent_Value__c, Seq_No__c, Event_Days__c
                                                 FROM Payment_Terms__r ORDER  BY Installment__c),
                                  Effective_To_calculated__c, TERM_ID__c from Payment_Plan__c Where id=: payPlanId];
        system.debug('explan: ' + explan.Name);
        Payment_Plan__c newPlan = new Payment_Plan__c();
        newplan = explan.clone();
        newplan.Building_ID__c = building.Location_ID__c;
        newPlan.Building_Location__c = buildingId;
        if(endDate != null && endDate != ''){
            newplan.Effective_To__c =  Date.parse(endDate);
        }
        newplan.Parent_Payment_Plan__c = explan.Id;
        insert newplan;
        List<Payment_Terms__c> payTermsToInsert = new List<Payment_Terms__c>();
        for(Payment_Terms__c term: explan.Payment_Terms__r){
            Payment_Terms__c newTerm = term.clone();
            newTerm.Payment_Plan__c = newplan.Id;
            payTermsToInsert.add(newTerm);
        }
        insert payTermsToInsert;
        */
        Payment_Plan_Association__c planAssoc = new Payment_Plan_Association__c();
        planAssoc.Payment_Plan__c = payPlanId;
        planAssoc.Location__c = buildingId;
        planAssoc.Effective_From__c = system.today();
        if(endDate != null && endDate != ''){
            planAssoc.Effective_To__c =  Date.parse(endDate);
        }
        insert planAssoc;
        fetchPayPlans();
    }
}