@isTest
public class AvailableUnitsControllerTest {
    private static List<User> userList = new List<User>();
   
   
    public static testmethod void testMethod1() {
        AvailableUnitsController auc  = new AvailableUnitsController ();
        auc.init ();
        auc.getCampaignList ();
        auc.getFloorList ();
        auc.getPackageList ();
        auc.refreshFloorList ();
        auc.refreshPackageList ();
        auc.getQueryString (0);
        auc.getPriceRelatedProperties ();
        auc.getNextRecords ();
        auc.getRelatedProperties ();
        auc.countTotalPages ();
        auc.setPreviousNextFlags ();
        auc.next ();
        auc.previous ();
        AvailableUnitsController.getInventoryDetails ('test');
        AvailableUnitsController.getAgencyDetails ('test');
        AvailableUnitsController.getCorporateAgents ('tst');
        AvailableUnitsController.validateSelectedUnits ('', '', '');
        AvailableUnitsController.getInventoryUsersList ('');
        auc.selectInventory ();
        auc.deleteInventory ();
        auc.getCampaignDetails (NULL);
        auc.getCampaignRelatedInventory ('');
        auc.getGeneralInventories ();
        auc.getMyInventories ();
        auc.getMyEOIs ();
        auc.getAssociatedProperties ('');
        
        auc.getProjectList();
        auc.getBuildingList();
        auc.getBedroomTypeList();
        //auc.getPriceList();
        auc.refreshthePropertyFilters();
        auc.refreshtheBuildingFilters();
        auc.refreshtheBedroomFilters();
        auc.getSpaMethods();
        auc.getProperties();
        auc.getRelatedInventories();
        auc.showNextRecords();
        auc.getAgencyRelatedInventories();
        
        //auc.selectInventory();
        auc.getAssociatedCampaigns();
        //auc.getTeamRelatedInventories();
        auc.getGeneralInventories();
        AvailableUnitsController.getCampaignRelatedInventories();
        AvailableUnitsController.getTeamUsers();
        auc.sortToggle();
        auc.getSortedList();
        AvailableUnitsController.getRelatedCampaign();
    }
    
}