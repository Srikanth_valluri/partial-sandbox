/************************************************************************************
Version History                                                                     *
------------------------------------------------------------------------------------*
Version    Date        Author            Description                                *
1.0                      Dipika Rajput                     Initial Draft                              *
*************************************************************************************/
@isTest
private class GetPDCDetailsTest{
    @isTest
    private static void getPDCDetailsPositiveTest() {
        insert new Credentials_Details__c (
            SetupOwnerId = UserInfo.getOrganizationId(),
            Name = 'PDC Details',
            Endpoint__c= 'https://dxbhoebtstap.damacholding.home:4488/webservices/rest/XXDC_PROCESS_SERVICE_WS/retrieve/',
            User_Name__c= 'oracle_user',
            Password__c = 'crp1user' 
        );
        
     /*// Create common test accounts
            Account objAcc = TestDataFactory_CRM.createPersonAccount();
            objAcc.Email__c ='test@test.com';
            objAcc.Mobile_Phone_Encrypt__pc = '0097405883798';
            insert objAcc ;
    
    //Insert Service Request
         NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
         objSR.Agency__c = objAcc.Id ;
         insert objSR ;
 
         Id dealId = objSR.Deal_ID__c;
         //Insert Bookings
         List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id,1 );
         insert lstBookings ; 
 
         //Insert Booking Units
         List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings,1 );
         lstBookingUnits[0].Registration_ID__c = '1520';
         insert lstBookingUnits;*/
         
           
         FmIpmsRestServices.DueInvoicesResponse invoicesResponse = new FmIpmsRestServices.DueInvoicesResponse();
        invoicesResponse.OutputParameters = new FmIpmsRestServices.OutputParameters();
        invoicesResponse.OutputParameters.X_RETURN_STATUS = 'S';
        invoicesResponse.OutputParameters.X_RETURN_MESSAGE = 'Total Records Fetched...1';
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE = new FmIpmsRestServices.ResponseMessage();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM = new List<Map<String, String>>();
        invoicesResponse.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM.add(
            new Map<String, String> {
                'ATTRIBUTE1' => '1520',
                'ATTRIBUTE2' => 'unitName',
                'ATTRIBUTE3' => '20-june-2020',
                'ATTRIBUTE4' => '333',
                'ATTRIBUTE5' => 'orgId',
                'ATTRIBUTE6' => 'partyId',
                'ATTRIBUTE7' => 'partyName'
               
            }
        );
         
         Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(200, 'S', JSON.serialize(invoicesResponse)));

        Test.startTest();
        GetPDCDetails.fetchPdcDetails();
           
        Test.stopTest();
}
}