@isTest
public class Damac_CreateInquiryController_Test {
    public static testMethod void method1 () {
        Whatsapp_Lead_Info__c settings = new Whatsapp_Lead_Info__c ();
        settings.endpoint__c = 'tset.com';
        settings.Authorization__c = 'test';
        insert settings;
        
        Nexmo_Whats_App_Request__c watsApp = New Nexmo_Whats_App_Request__c ();
            watsApp.Unique_Key__c = '91234567890098765678';
        insert watsApp;
        watsApp.record_id__c = watsApp.id;
        update watsApp ;
        
        Nexmo_Whats_App_Message__c msg = new Nexmo_Whats_App_Message__c ();
        msg.Nexmo_Message_Body__c = 'Ref:afasdfs! test message';
        msg.Nexmo_Whats_App_Request__c = watsApp.Id;
        insert msg;
        
        Campaign__c camp = new Campaign__c();
            camp.End_Date__c = system.today().addmonths(10);
            camp.Marketing_End_Date__c = system.today().addmonths(10);
            camp.Marketing_Start_Date__c = system.today().addmonths(-10);
            camp.Start_Date__c =  system.today().addmonths(-10);
        insert camp;
        Inquiry__c inq = new Inquiry__c ();
            inq.Activity_Counter__c =101;
            inq.Inquiry_Status__c='Active';
            inq.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
            inq.campaign__c = camp.id;
            inq.ownerId = userinfo.getUserId();
        insert inq;
        apexpages.currentpage().getparameters().put ('waRecordId', watsApp.Id);
        
        Damac_CreateInquiryController obj = New Damac_CreateInquiryController (new ApexPages.StandardController(inq));
            obj.waRecordId = watsApp.id;
            obj.save();
            Damac_CreateInquiryController.getAllFields('Account');
            List<Campaign__c> result = Damac_CreateInquiryController.getCampaigns ('test');
            obj.clearMessages();
    }
}