@isTest
private class FmCaseNotificationServiceTest {

    @isTest
    static void testService() {
        insert new FMNotificationServices__c(
            Name = 'FM Case Status Update Notifications',
            NotificationServiceClass__c = 'FmCaseNotificationService'
        );

        User portalUser = CommunityTestDataFactory.createPortalUser();
        Id portalAccountId = [SELECT Id, Contact.AccountId FROM User WHERE Id = :portalUser.Id][0].Contact.AccountId;

        System.debug('portalAccountId = ' + portalAccountId);

        Location__c location = new Location__c(
            Name = 'Test Location',
            Location_ID__c = 'LOC',
            GeoLocation__latitude__s = 0,
            GeoLocation__longitude__s =0
        );
        insert location;

        Property__c property = TestDataFactory_CRM.createProperty();
        insert property;

        Inventory__c inventory = TestDataFactory_CRM.createInventory(property.Id);
        inventory.Building_Location__c = location.Id;
        insert inventory;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount(portalAccountId, objDealSR.Id, 1);
        insert lstBooking;

        List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBooking, 1);
        for (Booking_Unit__c unit : lstBookingUnit) {
            unit.Unit_name__c = 'Unit Name';
            unit.Handover_Flag__c = 'Y';
            unit.Early_Handover__c = true;
            unit.Inventory__c = inventory.Id;
        }
        insert lstBookingUnit;

        insert TestDataFactory_CRM.createActiveFT_CS();

        CustomerCommunityUtils.customerAccountId = portalAccountId;

        FM_Case__c appointment = new FM_Case__c(
            Origin__c = 'Portal',
            Request_Type__c = 'Appointment',
            Account__c = CustomerCommunityUtils.customerAccountId,
            Submitted__c = true,
            Status__c = 'Submitted',
            Appointment_Status__c = 'Submitted',
            RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName()
                                        .get('Appointment').getRecordTypeId(),
            Notifications_Read__c = false
        );
        insert appointment;

        Test.startTest();
            FmCaseNotificationService fmCaseNotifService = new FmCaseNotificationService();

            System.assertEquals(NULL, fmCaseNotifService.getNotifications());

            NotificationService.Notification notificationRequest = new NotificationService.Notification();
            notificationRequest.accountId = portalAccountId;
            List<NotificationService.Notification> lstNotification = fmCaseNotifService.getNotifications(
                notificationRequest
            );

            lstNotification = FmCaseNotificationService.markRead(lstNotification);

            NotificationService.Notification readRequest = new NotificationService.Notification();
            readRequest.className = FmCaseNotificationService.class.getName();
            readRequest.recordId = appointment.Id;
            System.assertNotEquals(NULL, fmCaseNotifService.markUnread(readRequest));
        Test.stopTest();
    }

}