public without sharing class FetchDocumentURL {
    public static string fetchDocURL(String docDeveloperName){
        String strURL = null;
        list<Document> lstDoc = [Select d.Name
                                          , d.Id
                                          , d.DeveloperName 
                                 From Document d 
                                 where DeveloperName =: docDeveloperName limit 1];
        if(lstDoc != null && !lstDoc.isEmpty()){
            strURL = '/servlet/servlet.FileDownload?file='+lstDoc[0].Id;
        }
        return strURL;
    }
}