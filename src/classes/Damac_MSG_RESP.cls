public class Damac_MSG_RESP{
    public cls_message message;
    public cls_conversation conversation;
    public class cls_message {
        public String text; //test
        public String mediaUrl; //https://damacholding--FullCopy--c.cs102.content.force.com/sfc/dist/version/download/?oid=00D1j0000008anH&ids=0681n000007cm18&d=%2Fa%2F1j0000008OcF%2Fx_xbXkt0xRjwR_jfv5ml_3LXGZ0wtOHJ3ea6Ygk68JU&asPdf=false
        public String type; //file
        public String role; //appMaker
        public Double received; //1544442050.522
        public String authorId; //00uhirbfhgzdnYDms0h7
        public String avatarUrl;    //https://www.gravatar.com/avatar/00000000000000000000000000000000.png?s=200&d=mm
        public String mediaType;    //image/jpeg
        public Integer mediaSize;   //24950
        public String res_id;  //5c0e50c3b5e4fd00222f5886
        public cls_source source;
    }
    public class cls_source {
        public String type; //api
    }
    public class cls_conversation {
        public String res_id;  //c8ccbd7faa685086f1c6865a
        public Double appMakerLastRead; //1544442050.522
        public Integer unreadCount; //5
    }
    public static Damac_MSG_RESP parse(String json){
        return (Damac_MSG_RESP) System.JSON.deserialize(json, Damac_MSG_RESP.class);
    }
}