/********************************************************************************
* Description - test class for class SmsHistoryService                          *
*                                                                               *
* Version            Date            Author                    Description      *
* 1.0                30/01/2019      Arjun Khatri              Initial Draft    *
********************************************************************************/
@isTest
public Class SmsHistoryServiceTest {
    
    @isTest
    public static void testPutSuccess() {
        SMS_History__c objSms = createSmsHistory();
        insert objSms;
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/SmsHistoryService?msgid=0&deptID=0&appID=0&jobno=123&mobilenumber=91913XXXXXX&status=3&DoneTime=06%2F11%2F2019+20%3A56%3A06&messagepart=SMSCountent&sender_name=smscou&method=sendSmsReport HTTP/1.1';  
        req.httpMethod = 'Get';
        RestContext.request = req;
        RestContext.response = res;
        RestContext.request.params.put('jobno', '123');
        RestContext.request.params.put('status', '0');
        
        Test.startTest();
        String smsRes = SmsHistoryService.updateSmsHistories();
        System.debug('test sms res = ' + smsRes);
        System.debug('test sms = ' + [Select id, Delivery_Status__c from SMS_History__c]);
        
    }
    
    static SMS_History__c createSmsHistory() {
        return new SMS_History__c(Name = 'test Name', SMS_Id__c = '123');
    }
}