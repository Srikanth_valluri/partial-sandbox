@isTest
public class TransactionClassTest {
    @isTest
    static void testCreateTransaction(){
    
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Registration_ID__c = '3901';
        objBU.Unit_Name__c = 'test';
        objBU.Booking__c = objBooking.Id;
        insert objBU;
    
        Case objCase = new Case();
        objCase.Mortgage_Amount__c = 500.0;
        objCase.Booking_Unit__c = objBU.Id;
        insert objCase;
    
        Credentials_Details__c objCredDetail = new Credentials_Details__c();
        objCredDetail.Name = 'Mortgage Service';
        objCredDetail.Endpoint__c = 'http://151.253.15.117:8050/webservices/rest/XXDC_PROCESS_SERVICE_WS/process/';
        objCredDetail.User_Name__c = 'oracle_user';
        objCredDetail.Password__c = 'crp1user';
        insert objCredDetail;
        
        Test.startTest();
            TransactionClass.createTransaction('test', 12.0);
            TransactionClass.sendDetailsFromPB(new List<Id>{objCase.Id});
        Test.stopTest();
    }
}