/************************************************************************************************
 * @Name              : RestContextHandler
 * @Test Class Name   : DAMAC_Approval_Factory_Test
 * @Description       : Apex Class to handle Rest Context, Params
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         Charan Vuyyuru 14/07/2020       Created
 ***********************************************************************************************/

global class RestContextHandler {
    
    public class RestException extends Exception {}
    public WebServiceResponse response {get;set;}
    public RestRequest restRequest {get;set;}
    public RestResponse restResponse {get;set;}
    public Boolean shouldLogRequest {get;set;}
    
    public RestContextHandler(Boolean shouldLogRequest) {
        if (RestContext.request == null || RestContext.response == null) 
            throw new RestException('RestContext request and RestContext response should not be null');
        this.shouldLogRequest = shouldLogRequest;
        this.response = new WebServiceResponse();

        this.restRequest = RestContext.request; 
        this.restResponse = RestContext.response;
        this.setResponseHeaders();
    }
    
    public void finalize() {
       this.finalize(null);
    }
    
    public void finalize(Exception exc) {        
        if (this.shouldLogRequest) {
            string resp = (response.data<>null && string.valueOf(response.data).length()>131072) ? String.valueOf(response.data).substring(0,130000) : String.valueOf(response.data);
        }
        this.setResponseBody(Blob.valueOf(response.serialize()));
    }
    /*
    public Map<String, String> getRequestBodyMap() {        
        try {
            if (String.IsBlank(this.restRequest.requestbody.toString()))
                return null;

            Map<String,String> mpFldApiFldValues = new Map<String, String>();
            Map<String,object> mpUntypedObject = (Map<String,Object>)JSON.deserializeUntyped(this.restRequest.requestbody.tostring());
            System.debug('>>>>>>mpUntypedObject>>>>>>>>>>'+mpUntypedObject);

            for( String key : mpUntypedObject.keyset()) {
                System.debug('>>>>>>key>>>>>>>>>>'+key);
            
                mpFldApiFldValues.put(key,(string)mpUntypedObject.get(key));
            }            
            return mpFldApiFldValues;
        }
        catch(exception ex) {            
            system.debug('Request Body Format not supported '+ex.getMessage());
            throw ex;
        }
    }
    */
    public Map<String, String> getRequestParams() {
        
        try {
            if (String.IsBlank(this.restRequest.requestbody.toString()))
                return null;

            Map<String,String> mpFldApiFldValues = new Map<String, String>();
            Map<String,object> mpUntypedObject = (Map<String,Object>)JSON.deserializeUntyped(this.restRequest.requestbody.tostring());
            System.debug('>>>>>>mpUntypedObject>>>>>>>>>>'+mpUntypedObject);

            for( String key : mpUntypedObject.keyset()) {
                System.debug('>>>>>>key>>>>>>>>>>'+key);
            
                mpFldApiFldValues.put(key,JSON.serializePretty(mpUntypedObject.get(key)));
            }            
            return mpFldApiFldValues;
        }
        catch(exception ex) {            
            system.debug('Request Body Format not supported '+ex.getMessage());
            throw ex;
        }
    }
    /*
    public Map<String,object> getRequestBodyMapObject() {        
        try {
            if (String.IsBlank(this.restRequest.requestbody.toString()))
                return null;

            Map<String,object> mpUntypedObject = (Map<String,Object>)JSON.deserializeUntyped(this.restRequest.requestbody.tostring());
            return mpUntypedObject;
        }
        catch(exception ex) {
            system.debug('Request Body Format not supported '+ex.getMessage());
            throw ex;
        }
    }
    */
    public void setResponseBody(Blob body) {        
        this.restResponse.responseBody = body;
    }
    
    public void addResponseHeader(String key, String value) {        
        this.restResponse.addHeader(key, value);
    }
    /*
    public void setResponseStatusCode(Integer statusCode) {    
        this.restResponse.statusCode = statusCode;
    }   
    */
    public void setResponseHeaders() {        
        this.addResponseHeader('Content-Type', 'application/json;charset=UTF-8');
    }
}