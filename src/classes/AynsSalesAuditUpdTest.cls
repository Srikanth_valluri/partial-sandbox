/*****************************************************************************************
This is the test class for below classes
AsyncSalesAuditWS
Created: 09/07/2017
Created By - Alok DAMAC
*******************************************************************************************/
@isTest 
private class AynsSalesAuditUpdTest{

    private static NSIBPM__Service_Request__c testSR;
    private static Booking_Unit__c bookingUnit;
    private static Date d = Date.newinstance(25,12,2021);
    private static Receipt__c rec;
    private static String body = '';
    private static Payment_Terms__c pymTerms;
    private static NSIBPM__SR_Doc__c srdoc;
    private static List<Id> recids= new List<Id>();
    private static List<Id> recordids = new List<Id>();
    private static List<Id> sBUids = new List<Id>();
    private static List<Id> sRids = new List<Id>();


private static void init(){
        Location__c loc=new Location__c();
        loc.Location_ID__c='834';
        insert loc;
        Inventory__c inv = new Inventory__c();
        inv.Unit_Location__c=loc.id;
        insert inv;
        
        testSR= new NSIBPM__Service_Request__c();
        testSR.Delivery_mode__c='Email';
        testSR.Deal_ID__c='9298882';
        insert testSR;
        
        sRids.add(testSR.id);
        srdoc = new NSIBPM__SR_Doc__c();
        srdoc.NSIBPM__Service_Request__c=testSR.id;
        insert srdoc;
        
        Booking__c bk= new Booking__c();
        bk.Deal_SR__c=testSR.id;
        bk.Booking_channel__c='Office';
        insert bk;
        
        Buyer__c primBuyer= new Buyer__c();
        primBuyer.Primary_Buyer__c=true;
        primBuyer.Buyer_Type__c='Individual';
        primBuyer.Booking__c=bk.id;
        primBuyer.Phone_Country_Code__c='India: 0091';
        primBuyer.Passport_Expiry_Date__c='2/12/2019';
        primBuyer.City__c='Dubai';
        primBuyer.Country__c='United Arab Emirates';
        primBuyer.Address_Line_1__c='AddLine1';
        primBuyer.Address_Changed__c=true;
        primBuyer.Date_of_Birth__c='5/1/1988';
        primBuyer.Email__c='test@salesaudit.com';
        primBuyer.First_Name__c='SABuyer';
        primBuyer.Last_Name__c='TestBuyer';
        primBuyer.Nationality__c='Indian'; 
        primBuyer.Passport_Number__c='PASSP32828'; 
        primBuyer.Phone__c='67677754';
        primBuyer.Place_of_Issue__c='Mumbai'; 
        primBuyer.Title__c='Mr.';
        //insert primBuyer;
        
        bookingUnit= new Booking_Unit__c ();
        bookingUnit.Booking__c=bk.id;
        bookingUnit.Inventory__c=inv.id;
        insert bookingUnit;
        
        Payment_Plan__c ppp = new Payment_Plan__c();
        ppp.TERM_ID__c='4546';
        insert ppp;
        
        Payment_Plan__c pp = new Payment_Plan__c();
        pp.Parent_Payment_Plan__c=ppp.id;
        pp.Booking_Unit__c=bookingUnit.id;
        insert pp;
        
        pymTerms = new Payment_Terms__c();
        pymTerms.Payment_Plan__c=pp.id;
        insert pymTerms;
        
        recordids.add(bk.id);
        sBUids.add(bookingUnit.id);
        
        rec = new Receipt__c();
        rec.Amount__c=100;
        rec.Booking_Unit__c=bookingUnit.id;
        insert rec;
        
        recids.add(rec.id);
        
        NSIBPM__Document_Master__c docMast = new NSIBPM__Document_Master__c();
        docMast.NSIBPM__Code__c='SOA';
        insert docMast;
        
        NSIBPM__Document_Master__c docMast1 = new NSIBPM__Document_Master__c();
        docMast1.NSIBPM__Code__c='SPA';
        insert docMast1;
        
        NSIBPM__Document_Master__c docMast2 = new NSIBPM__Document_Master__c();
        docMast2.NSIBPM__Code__c='DP-INVOICE';
        insert docMast2;
        
        NSIBPM__SR_Status__c st= new NSIBPM__SR_Status__c();
        st.NSIBPM__Code__c='SUBMITTED';
        insert st;
        
        NSIBPM__SR_Status__c st1= new NSIBPM__SR_Status__c();
        st1.NSIBPM__Code__c='AGREEMENT_GENERATED';
        insert st1; 
    }

@isTest static void testSA(){
 init();
 Test.startTest();
 AsyncSalesAuditUpd.getIPMSsetting('IPMS_webservice');
 AsyncSalesAuditUpd.sendSalesAuditUpdate(recordids,'UPDATE_REGISTRATION','Y');
 AsyncSalesAuditUpd.sendRegnUpdate(recordids,'UPDATE_REGISTRATION','Y');
 body= SetResponse(bookingUnit.id);
 AsyncSalesAuditUpd.parseRegnUpdateResponse(body);
 Test.stopTest();
}
private static string SetResponse(String idval){
        string body = '';
        body+='<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">';
        body+='<env:Header/>';
        body+='<env:Body>';
          body+='<OutputParameters xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://xmlns.oracle.com/apps/ont/soaprovider/plsql/xxdc_process_service_ws/process/">';
             body+='<X_RESPONSE_MESSAGE>';
                body+='<X_RESPONSE_MESSAGE_ITEM>';
                   body+='<PARAM_ID>'+idval+'</PARAM_ID>';
                   body+='<PROC_STATUS>S</PROC_STATUS>';
                   body+='<PROC_MESSAGE>Sales Audit Flag Updated</PROC_MESSAGE>';
                   body+='<ATTRIBUTE1>9383282</ATTRIBUTE1>';
                   body+='<ATTRIBUTE2></ATTRIBUTE2>';
                   body+='<ATTRIBUTE17>Y</ATTRIBUTE17>';
                body+='</X_RESPONSE_MESSAGE_ITEM>';
             body+='</X_RESPONSE_MESSAGE>';
             body+='<X_RETURN_STATUS>S</X_RETURN_STATUS>';
             body+='<X_RETURN_MESSAGE>Process Completed successfully...</X_RETURN_MESSAGE>';
          body+='</OutputParameters>';
        body+='</env:Body>';
        body+='</env:Envelope>';
        body=body.trim();
        body= body.replaceAll('null', '');
        body=body.trim();
        
        return body;
    }
}