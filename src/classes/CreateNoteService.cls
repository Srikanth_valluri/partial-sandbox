public without sharing class CreateNoteService {
    
    public static void createNoteBasedOnCEComments( List<Calling_List__c> lstCallingList ,Map<Id, Calling_List__c> mapOldCallList ) {
        
        Id collectioncallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Collections Calling List').RecordTypeId;
        Id MiscellaneousRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Miscellaneous List').RecordTypeId;
        Id CallBackRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Call Back Request').RecordTypeId;
        Id DroppedCallsRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Dropped Calls - Call Back').RecordTypeId;
        Id walkInCallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Walk In Calling List').RecordTypeId;
        Id recoveryRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Recovery Calling List').RecordTypeId;
                
        
        system.debug('in  createNoteBasedOnCEComments : '+lstCallingList);

        List<ContentNote> lstContentNote = new List<ContentNote>();
        List<ContentDocumentLink> lstContentDocumentLink = new List<ContentDocumentLink>();
        for( Calling_List__c objCallingList : lstCallingList ) {
            if( 
            //objCallingList.Customer_Flag__c == true && 
             objCallingList.IsHideFromUI__c == false
             && objCallingList.CE_Comments__c != ''
             && objCallingList.CE_Comments__c != Null
             && ( mapOldCallList.get(objCallingList.Id).CE_Comments__c != objCallingList.CE_Comments__c )
                && ( objCallingList.RecordTypeId == collectioncallingRecordTypeId ||
                    //objCallingList.RecordTypeId == MiscellaneousRecordTypeId ||
                    //objCallingList.RecordTypeId == CallBackRecordTypeId  ||
                    //objCallingList.RecordTypeId == DroppedCallsRecordTypeId )
                    objCallingList.RecordTypeId == recoveryRecordTypeId ) 
            ){
                    ContentNote objContentNote = new ContentNote();
                    objContentNote.Title = 'CE-Comments Notes';
                    //objContentNote.Content = Blob.valueOf(objCallingList.CE_Comments__c);
                    objContentNote.Content = Blob.valueOf(objCallingList.CE_Comments__c.escapeHtml4().replace('\r\n', '<br>').replace('\n', '<br>').replace('\r', '<br>'));
                    
                    lstContentNote.add(objContentNote);
            } else if( objCallingList.IsHideFromUI__c == false 
                     && objCallingList.CRE_Remarks__c != ''
                     && ( mapOldCallList.get(objCallingList.Id).CRE_Remarks__c   != objCallingList.CRE_Remarks__c )
                     && objCallingList.RecordTypeId == walkInCallingRecordTypeId ) {
                    ContentNote objContentNote = new ContentNote();
                    objContentNote.Title = 'CRE-Remarks Notes';
                    objContentNote.Content = Blob.valueOf(objCallingList.CRE_Remarks__c.escapeHtml4().replace('\r\n', '<br>').replace('\n', '<br>').replace('\r', '<br>'));
                    
                    lstContentNote.add(objContentNote);
              }
        }
        
        try{
            if( !lstContentNote.isEmpty() && lstContentNote != null ) {
                insert lstContentNote;
            }
        } catch( Exception e ) {
            System.debug('there is something problem:while insertting Note :'+e.getMessage());
        }        
        
        if( !lstContentNote.isEmpty() && lstContentNote != null ) { 
            for( Integer i = 0; i < lstContentNote.size();i++ ) {
    
                ContentDocumentLink objContentDocumentLink = new ContentDocumentLink();
                objContentDocumentLink.ContentDocumentId = lstContentNote[i].Id;
                objContentDocumentLink.LinkedEntityId = lstCallingList[i].Id;
                objContentDocumentLink.Visibility ='AllUsers';
                objContentDocumentLink.ShareType ='V';
                lstContentDocumentLink.add(objContentDocumentLink);
                
            }
        }
        
        if( !lstContentDocumentLink.isEmpty() && lstContentDocumentLink != null) {
            insert lstContentDocumentLink;
        }

    }
}