@isTest
public with sharing class EscalateCLExtentionTest {
    @IsTest
    static void positiveTestForDirector(){
        Profile p2 = [SELECT Id FROM Profile WHERE Name='System Administrator' Limit 1];
        //ID adminRoleId = [ Select id from userRole where name = 'Director'].id;
        UserRole userRoleObj = new UserRole(Name = 'Director', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        User u2 = new User(Alias = 'standt2', Email='stand2@testorg.com', userRoleId=userRoleObj.Id, isActive = true,
                           EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p2.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='adm@testorg123.com');
        insert u2;
        User u1 = new User(Alias = 'standt1', Email='stan1@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', isActive = true,
                           LocaleSidKey='en_US', ProfileId = p2.Id, ManagerID = u2.id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='sta0@damactest1234.com');
        insert u1;
        User u3 = new User(Alias = 'standt1', Email='stan1@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', isActive = true,
                           LocaleSidKey='en_US', ProfileId = p2.Id, ManagerID = u2.id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='sta1@damactest1234.com');
        insert u3;
        System.runAs(u2) {
            List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                        OnOffCheck__c = true);
            
            settingLst2.add(newSetting1);
            insert settingLst2;
            
            NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
            insert objDealSR ;
            
            Account objAcc = TestDataFactory_CRM.createPersonAccount();
            insert objAcc ;
            
            List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objDealSR.Id, 1 );
            insert lstBooking ;
            
            List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking, 1 );
            insert lstBookingUnit ;
            
            List<Calling_List__c> lstCalling = TestDataFactory_CRM.createCallingList( 'Appointment_Scheduling', 1 , lstBookingUnit[0] );
            lstCalling[0].OwnerId =u1.Id;
            insert lstCalling ;
            
            
            PageReference pgRef = Page.EscalateCL;
            Test.setCurrentPage(pgRef);
            pgRef.getParameters().put('id', lstCalling[0].Id);
            ApexPages.StandardController sc = new ApexPages.standardController(lstCalling[0]);
            EscalateCLExtention  extn = new EscalateCLExtention(sc);
            extn.escalate();
            
        }
    }
    @IsTest
    static void positiveTestForSVP(){
        Profile p2 = [SELECT Id FROM Profile WHERE Name='System Administrator' Limit 1];
        //ID adminRoleId = [ Select id from userRole where name = 'Director'].id;
        UserRole userRoleObj = new UserRole(Name = 'SVP', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        User u2 = new User(Alias = 'standt2', Email='stand2@testorg.com', userRoleId=userRoleObj.Id, isActive = true,
                           EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p2.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='adm@testorg123.com');
        insert u2;
        User u1 = new User(Alias = 'standt1', Email='stan1@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', isActive = true,
                           LocaleSidKey='en_US', ProfileId = p2.Id, ManagerID = u2.id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='sta0@damactest1234.com');
        insert u1;
        System.runAs(u1) {
            List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                        OnOffCheck__c = true);
            
            settingLst2.add(newSetting1);
            insert settingLst2;
            
            NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
            insert objDealSR ;
            
            Account objAcc = TestDataFactory_CRM.createPersonAccount();
            insert objAcc ;
            
            List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objDealSR.Id, 1 );
            insert lstBooking ;
            
            List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking, 1 );
            insert lstBookingUnit ;
            
            List<Calling_List__c> lstCalling = TestDataFactory_CRM.createCallingList( 'Appointment_Scheduling', 1 , lstBookingUnit[0] );
            insert lstCalling ;
            
            
            PageReference pgRef = Page.EscalateCL;
            Test.setCurrentPage(pgRef);
            pgRef.getParameters().put('id', lstCalling[0].Id);
            ApexPages.StandardController sc = new ApexPages.standardController(lstCalling[0]);
            EscalateCLExtention  extn = new EscalateCLExtention(sc);
            extn.escalate();
            
        }
    }
    @IsTest
    static void positiveTestForManager(){
        Profile p2 = [SELECT Id FROM Profile WHERE Name='System Administrator' Limit 1];
        //ID adminRoleId = [ Select id from userRole where name = 'Director'].id;
        UserRole userRoleObj = new UserRole(Name = 'Manager', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        User u2 = new User(Alias = 'standt2', Email='stand2@testorg.com', userRoleId=userRoleObj.Id, isActive = true,
                           EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p2.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='adm@testorg123.com');
        insert u2;
        User u1 = new User(Alias = 'standt1', Email='stan1@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', isActive = true,
                           LocaleSidKey='en_US', ProfileId = p2.Id, ManagerID = u2.id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='sta0@damactest1234.com');
        insert u1;
        System.runAs(u1) {
            List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                        OnOffCheck__c = true);
            
            settingLst2.add(newSetting1);
            insert settingLst2;
            
            NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
            insert objDealSR ;
            
            Account objAcc = TestDataFactory_CRM.createPersonAccount();
            insert objAcc ;
            
            List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objDealSR.Id, 1 );
            insert lstBooking ;
            
            List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking, 1 );
            insert lstBookingUnit ;
            
            List<Calling_List__c> lstCalling = TestDataFactory_CRM.createCallingList( 'Appointment_Scheduling', 1 , lstBookingUnit[0] );
            insert lstCalling ;
            
            
            PageReference pgRef = Page.EscalateCL;
            Test.setCurrentPage(pgRef);
            pgRef.getParameters().put('id', lstCalling[0].Id);
            ApexPages.StandardController sc = new ApexPages.standardController(lstCalling[0]);
            EscalateCLExtention  extn = new EscalateCLExtention(sc);
            extn.escalate();
            
        }
    }
    @IsTest
    static void negativeTestCaseNoManager(){
        User pcUser = InitialiseTestData.getPropertyConsultantUsers('userABC@test.com');
        insert pcUser;
        System.runAs(pcUser) {
            List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                                        OnOffCheck__c = true);
            
            settingLst2.add(newSetting1);
            insert settingLst2;
            
            NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
            insert objDealSR ;
            
            Account objAcc = TestDataFactory_CRM.createPersonAccount();
            insert objAcc ;
            
            List<Booking__c> lstBooking = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objDealSR.Id, 1 );
            insert lstBooking ;
            
            List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits( lstBooking, 1 );
            insert lstBookingUnit ;
            
            List<Calling_List__c> lstCalling = TestDataFactory_CRM.createCallingList( 'Appointment_Scheduling', 1 , lstBookingUnit[0] );
            insert lstCalling ;
            
            
            PageReference pgRef = Page.EscalateCL;
            Test.setCurrentPage(pgRef);
            pgRef.getParameters().put('id', lstCalling[0].Id);
            ApexPages.StandardController sc = new ApexPages.standardController(lstCalling[0]);
            EscalateCLExtention  extn = new EscalateCLExtention(sc);
            extn.escalate();
            
        }
    }
}