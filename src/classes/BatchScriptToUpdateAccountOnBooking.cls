public class BatchScriptToUpdateAccountOnBooking implements Database.Batchable<sObject>{

    String query;

    public Database.QueryLocator start(Database.BatchableContext BC){
    
        query= 'Select Id,Account__c ,(Select Party_ID__c From Buyers__r Where Primary_Buyer__c = true) from Booking__c Where Account__c = null';
        system.debug(' query : ' + query);
        return Database.getQueryLocator(query);
    }
    public void execute(Database.BatchableContext BC, List<Booking__c> lstbooking){
        System.debug('lstbooking size:::'+lstbooking);
        
        set<String> setPartyId = new set<String>();
        
        for(Booking__c objBooking : lstbooking){
            system.debug('objBooking.Buyers__r : '+objBooking.Buyers__r);
            for( Buyer__c objB : objBooking.Buyers__r) {
                if( string.isNotBlank( objB.Party_ID__c ) ) {
                    setPartyId.add(objB.Party_ID__c);
                }
            }
        }
        system.debug(' setPartyId : ' + setPartyId);
        
        map<string,Account> mapPartyIdAccount = new map<string,Account>();
        for(Account objAcc : [SELECT ID,Party_ID__c FROM Account WHERE Party_ID__c IN: setPartyId]) {
            mapPartyIdAccount.put(objAcc.Party_ID__c,objAcc);
        }
        system.debug(' mapPartyIdAccount : ' + mapPartyIdAccount);

        List<Booking__c> lstUpdate = new List<Booking__c>();
        for(Booking__c objBooking : lstbooking){
            for( Buyer__c objB : objBooking.Buyers__r ) {
                if(mapPartyIdAccount.containskey(objB.Party_ID__c)){
                    objBooking.Account__c = mapPartyIdAccount.get(objB.Party_ID__c).Id;
                }
            }
            lstUpdate.add(objBooking);
        }
        System.debug('lstUpdate::'+lstUpdate.size());
        
        if( !lstUpdate.isEmpty() ) {
            update lstUpdate;
        }
    }
    public void finish(Database.BatchableContext BC){
    }
}