/**************************************************************************************************
* Name               : Invocable_UpdateRegnStatus_Test
* Description        : Test Class for Invocable_UpdateRegnStatus Class
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR          DATE            Comments
* 1.0                         16/06/2019      Created
**************************************************************************************************/
@isTest(SeeAllData=true)
public class Invocable_UpdateRegnStatus_Test {
    static testMethod void test_EvaluateCustomCode_UseCase1(){
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        NSIBPM__Service_Request__c  sSR1 = new NSIBPM__Service_Request__c ();
        insert sSR1;
        NSIBPM__Service_Request__c  sSR2 = new NSIBPM__Service_Request__c ();
        sSR2.NSIBPM__Parent_SR__c = sSR1.Id;
        insert sSR2;
        Booking__c booking = new Booking__c(Account__c = acc.Id, Deal_SR__c = sSR2.Id);
        insert booking;
        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id, Unit_Name__c = 'Test name 1',
                                                          Registration_ID__c = '18970987987600', Registration_Status__c = 'Active Status',
                                                          Unit_Selling_Price_AED__c = 100);
        insert bookingUnit;
        New_Step__c sStep = new New_Step__c();
        sStep.Service_Request__c = sSR2.Id;
        List<Id> stepIds = new List<Id>();
        stepIds.add(sStep.Id);

        Invocable_UpdateRegnStatus.updateBuyer(stepIds);
        Invocable_UpdateRegnStatus.evaluateCustomCode(sStep);
    }
}