@isTest
public class DAMAC_InquiryButtonsLightning_Test {

    @isTest
    static void testAllMethods(){
        Id inquiryRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        Inquiry__c inqObj = new Inquiry__c(Pre_InquiryId__c='123456',
                                            Mobile_Phone_Encrypt__c='456123',RecordTypeId = inquiryRT,
                                            Mobile_CountryCode__c='American Samoa: 001684',
                                            Mobile_Phone__c='1234432111',Email__c='mk@gmail.com',
                                            First_Name__c='Test',Last_Name__c='Last',
                                            Sales_Office_Location_Url__c ='test',
                                            Meeting_Due_Date__c = DateTime.now().addHours(2),
                                            CR_Number__c='0987',Organisation_Name__c = 'Oliver',
                                            Is_Meeting_scheduled__c=true, Schedule_Conditions__c = true,
                                            Online_Meeting_Start_Date_Time__c = DateTime.now().addHours(2),
                                            Online_Meeting_End_Date_Time__c = DateTime.now().addHours(3)
                                            );
        inqObj.Assigned_PC__c = UserInfo.getuserId(); 
        inqObj.Meeting_Type__c = 'Direct Tour';
        inqObj.Tour_Date_Time__c = system.now();
        insert inqObj;
        Test.startTest();
            DAMAC_ConfirmMeetingLightning.updateInquiryWithMeetingDetails(inqObj.Id);
            inqObj.Meeting_Due_Date__c = null;
            update inqObj;
            DAMAC_ConfirmMeetingLightning.updateInquiryWithMeetingDetails(inqObj.Id);
            inqObj.Meeting_Due_Date__c = DateTime.now().addHours(2);
            inqObj.Online_Meeting_End_Date_Time__c = DateTime.now().addDays(4);
            update inqObj;
            DAMAC_ConfirmMeetingLightning.updateInquiryWithMeetingDetails(inqObj.Id);
            DAMAC_CancelMeetingLightning.updateInquiryWithCancellationDetails(inqObj.Id);
            Damac_TransferActivityLightning.transferActivities(inqObj.Id);
            String mobilePhoneEncrypt = UtilityHelperCls.encryptMobile(inqObj.Mobile_Phone__c);
            String encryptNum = Damac_ClickToDialLightning.encryptMobilePhoneAura(mobilePhoneEncrypt);
            Damac_ClickToDialLightning.fetchInquiryDetails(inqObj.Id);
            Damac_ClickToDialLightning.updateInquiryWithMobileEncrypted(encryptNum, inqObj.Id);
            Damac_ClickToDialLightning.fetchUserExtension();
            Damac_ClickToDialLightning.makeCallWaybeo('ext', 'customer');
            
        Test.stopTest();
    }
}