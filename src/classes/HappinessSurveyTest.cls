@isTest
public with sharing class HappinessSurveyTest {
    @IsTest
    static void methodName(){

         Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Mobile_Phone_Encrypt__pc = '123123123';
        objAcc.Mobile_Phone_Encrypt_2__pc = '123123123';
        objAcc.Mobile_Phone_Encrypt_3__pc = '123123123';
        objAcc.Mobile_Phone_Encrypt_4__pc = '123123123';
        objAcc.Mobile_Phone_Encrypt_5__pc = '123123123';
        objAcc.Telephone__c = '123123123';
        insert objAcc ;

         NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        objBookingUnit.Registration_Status_Code__c = 'ABS';
        objBookingUnit.MollakId__c = '1005';
        insert objBookingUnit;

        List<Buyer__c> lstBuyers = TestDataFactory_CRM.createBuyer(objBooking.Id, 2, objAcc.Id);
        lstBuyers[0].Primary_Buyer__c = false;
        insert lstBuyers;

        Test.setMock(HttpCalloutMock.class, new RESTCalloutServiceMockCallingList(1));
        Test.startTest();
        HappinessSurvey.ResponseWrapper objWrap =  HappinessSurvey.HappinessSurvey('JNU/ABC/234','DAMAC TOWERS BY PARAMOUNT','Visitor','Owner','Gym','Men’s Gym','How was ur experience with gym',
        'not happy','James Bond','QR Code','00971505124682','instruments not working','gym is not clean');
        
        Test.stopTest();
        
    }
    

}