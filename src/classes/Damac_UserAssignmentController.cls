/***************************************************************************************************************

 * @Name              : Damac_UserAssignmentController
 * @Test Class Name   : Damac_UserAssignmentControllerTest
 * @Description       : To Display/Create/Update/Delete UserAssignment Records for Property Consultant Users
 
****************************************************************************************************************/
global class Damac_UserAssignmentController{
    public User_Assignment__c userAssignmet { get; set; }
    public String userNameSelected { get; set; }
    public String userId {get; set; }
    public String popupUserName { get; set; }
    public User_Assignment__c userAssignToEdit { get; set; }
    public List<User_Assignment__c> usersAssignedToDisplay { get; set; }
    public String successMessage { get; set; }
    public String errorMessage { get; set; }
    public Damac_UserAssignmentController(){
        usersAssignedToDisplay = new List<User_Assignment__c>();
        userAssignmet = new User_Assignment__c ();
        userAssignToEdit = new User_Assignment__c();
        successMessage = '';
        errorMessage = '';
        popupUserName = '';
    }
    //To display UserAssignment Records based on filter condition applied on Page
    public void retriveUserAssignedRecords(){
       usersAssignedToDisplay = new List<User_Assignment__c>();
        String profileName = '%Property Consultant%';
       Date currentDate = userAssignmet.Shift_Day__c;
       String query = 'SELECT Campaign_SalesOffice__c,Name,Rank__c,Shift_Day__c,Shift_Time__c,User_Assigned__c,User_Assigned__r.Name  FROM User_Assignment__c WHERE ';
       if(currentDate == null){
          query += 'Shift_Day__c >= LAST_N_DAYS:7 AND User_Assigned__c != null AND User_Assigned__r.Profile.Name LIKE : profileName '; 
       }else{
           query += 'Shift_Day__c =:currentDate AND User_Assigned__c != null AND User_Assigned__r.Profile.Name LIKE : profileName ';
       }
       Time currentTime = userAssignmet.Shift_Time__c;
       if(userAssignmet.Shift_Time__c != null){
           query += ' AND Shift_Time__c =: currentTime ';
       }
       String userId = userAssignmet.User_Assigned__c;
       if(userId != ''){
           query += ' AND User_Assigned__c  =: userId ';  
       }
       usersAssignedToDisplay = database.query(query);     
    }
    //To delete selected UserAssignment Record
    public void deleteUserAssign(){
        try{
            successMessage ='';
            errorMessage = '';
            String userIdToDelete = apexpages.currentpage().getparameters().get('userToDelete');
            User_Assignment__c userAssignToDelete = new User_Assignment__c ();
            userAssignToDelete.Id = userIdToDelete ;
            delete userAssignToDelete ;
            retriveUserAssignedRecords();
            successMessage = 'Record deleted Successfully';
        }catch(Exception e){
            errorMessage = e.getMessage();
        }
    }
    //To edit selected UserAssignment Record
    public void editUserAssign(){
        
        String userIdToEdit = apexpages.currentpage().getparameters().get('userToEdit');
        userAssignToEdit = new User_Assignment__c();
        userAssignToEdit = [SELECT Campaign_SalesOffice__c,Name,Rank__c,Shift_Day__c,Shift_Time__c,User_Assigned__c,User_Assigned__r.Name  FROM User_Assignment__c WHERE Id = : userIdToEdit ];
        popupUserName = userAssignToEdit.User_Assigned__r.Name;
        userId = userAssignToEdit.User_Assigned__c;
    }
    //To insert new UserAssignment Record
    public void newUserAssign(){
        userAssignToEdit = new User_Assignment__c();
        popupUserName = '';
        userId = '';
    }
    //This method is to update/insert UserAssignment Records
    public void saveUserAssign(){
        successMessage ='';
        errorMessage = '';
        try{
        String userIdToEdit = apexpages.currentpage().getparameters().get('userAssignId');
        if(userId != ''){
            userAssignToEdit.User_Assigned__c = userId;
        }else{
            userAssignToEdit.User_Assigned__c = null;
        }
        if(userIdToEdit == ''){
            insert userAssignToEdit;
        }else{
            update userAssignToEdit;
        }
        retriveUserAssignedRecords();
        successMessage = 'Record Saved Successfully';
        }catch(Exception e){
           errorMessage = e.getMessage(); 
        }
    }
    //For retriving user records to display in dropdown
    @RemoteAction 
    global static List<User> getUserDetails(String searchKey){
        String key = '%' + searchKey + '%';
        String profileName = '%Property Consultant%';
        List<User> displayUsersList = new List<User>();
        displayUsersList = [SELECT Id, Name FROM User WHERE Name LIKE : key AND Profile.Name LIKE : profileName LIMIT 10];  
        return displayUsersList ;
    }
}