/**********************************************************************************************
Description: class to get collection promotionn
______________________________________________________________________________________________
Version Date(DD-MM-YYYY)    Author              Description
-----------------------------------------------------------------------------------------------
1.0     16-03-2020          Aishwarya Todkar    Initial Draft

1.1     14-03-2020          Aishwarya Todkar    Introduced 'In' and 'Not In' Operators
**********************************************************************************************/
public Class CollectionPromotionConfig {

/**********************************************************************************************
Description : Method to filtere rules from Rule Engine based on Priority, Process Name
              and Object Name
Parameters  : Process Name, Record Id
Return Type : String ( rule )
**********************************************************************************************/   
    public static String filterSfRuleEngine( String processName, Id recordId) {
        
        Map<String, List<SF_Rule_Engines__mdt> > mapOfRuleNoToRule = new Map<String, List<SF_Rule_Engines__mdt> >();
        Set<String> apiNameSet = new Set<String>();
        String objectName = recordId.getSObjectType().getDescribe().getName();

        System.debug('processName==' + processName);
        System.debug('recordId==' + recordId);
        System.debug('objectName==' + objectName);

        if( String.isNotBlank( processName ) && recordId != null ) {
            List<SF_Rule_Engines__mdt> listSfRules = new List<SF_Rule_Engines__mdt>();
            listSfRules = [SELECT
                                Field_API_Name__c
                                , Field_Value__c
                                , Operator__c
                                , Process_Name__c
                                , Rule_Number__c
                                , Object_API_Name__c
                                , Priority__c
                            FROM
                                SF_Rule_Engines__mdt
                            WHERE
                                /*Process_Name__c != null
                            AND*/
                                Process_Name__c =: processName
                            AND
                                Object_API_Name__c =: objectName
                            AND
                                Field_API_Name__c != null
                            AND
                                Field_Value__c != null
                            AND
                                Operator__c != null
                            AND
                                Active__c = true
                            ORDER BY
                                Priority__c ASC
                            ];
            if( listSfRules.size() > 0 ) {
                for( SF_Rule_Engines__mdt ruleObj : listSfRules ) {
                    
                    apiNameSet.add( ruleObj.Field_API_Name__c );                                        

                    if( mapOfRuleNoToRule.containsKey( ruleObj.Rule_Number__c ) ) {
                        mapOfRuleNoToRule.get( ruleObj.Rule_Number__c ).add( ruleObj );
                    }
                    else {
                        mapOfRuleNoToRule.put( ruleObj.Rule_Number__c, new List<SF_Rule_Engines__mdt>{ ruleObj } );
                    }
                }//End For

                sObject obj;
        
                if( apiNameSet != null && apiNameSet.size() > 0 )
                    obj = getObjectDetails( apiNameSet, recordId, objectName );
                
                system.debug('mapOfRuleNoToRule--'+mapOfRuleNoToRule);
                return calculateAuthorities( mapOfRuleNoToRule, obj, objectName);
            }
        }//End if
        return null;
    }//End Methof

/**********************************************************************************************
Method Description  : Method to query dynamically for requested record
Parameters          : Process Name, Record Id
Return Type         : Set<String> (Set Of API names), Record Id, Object Name
**********************************************************************************************/
    public static sObject getObjectDetails( Set<String> apiNameSet, Id recordId, String objectName) {
        if( !apiNameSet.isEmpty() && recordId != null && String.isNotBlank( objectName ) ) {
            String query = 'Select id,';
            for( String apiName : apiNameSet ) {
                query += apiName + ',';
            }

            query = query.removeEnd(',');
            query += ' from ' + objectName + ' where id = \'' + recordId + '\'';
            System.debug('Query == ' + query );
            List<sObject> lstsObject = Database.query( query );
            System.debug('lstsObject==' + lstsObject );

            if( lstsObject != null && lstsObject.size() > 0 )
                return lstsObject[0];
        }
        
        return null;
    }

/**********************************************************************************************
Method Description  : Method to check conditions present Rule engine 
Parameters          : Process Name, Record Id
Return Type         : Map of SF Rule Engine, sObject instance, Object Name
**********************************************************************************************/
    public static String calculateAuthorities( Map<String, List<SF_Rule_Engines__mdt> > mapOfRuleNoToRule
                                                , sObject obj
                                                , String objectName) {
        Boolean ruleResponse = false;
        String approvingAuthority = '';
        
        if( mapOfRuleNoToRule != null && mapOfRuleNoToRule.size() > 0 && obj != null && String.isNotBlank( objectName ) ) {

            for( String ruleNumber :  mapOfRuleNoToRule.keySet() ) {
                
                System.debug('ruleNumber = ' + ruleNumber);
                
                String processName = '';

                if( mapOfRuleNoToRule.get( ruleNumber ) != null && mapOfRuleNoToRule.get( ruleNumber ).size() > 0 ) {

                    for( SF_Rule_Engines__mdt  ruleObj : mapOfRuleNoToRule.get( ruleNumber )) {
                        
                        processName = ruleObj.Process_Name__c;
                        System.debug( 'processName==> ' + processName );
                        System.debug('Expression=> ' + ruleObj.Field_API_Name__c + ' ' + ruleObj.Operator__c + ' ' + ruleObj.Field_Value__c );
                        
                        if( ruleObj.Operator__c == '==' ) {

                            if( ruleObj.Field_Value__c.isNumeric() ) {
                                Decimal fieldValue = getDecimalValue( ruleObj, obj );
                                System.debug( 'fieldValue - ' + fieldValue);
                                if( fieldValue != null && fieldValue == Decimal.valueOf( ruleObj.Field_Value__c ) ) {
                                    ruleResponse = true; 
                                }
                                else{  ruleResponse = false; break; } 
                            }
                            else {
                                String fieldValue = getStringValue( ruleObj, obj );
                                System.debug( 'fieldValue - ' + fieldValue);
                                if( String.isNotBlank( fieldValue ) && String.valueOf( ruleObj.Field_Value__c ).equalsIgnoreCase( fieldValue )) {
                                    ruleResponse = true;
                                }
                                else { ruleResponse = false; break; }
                            }
                        }//End ==
                        else if( ruleObj.Operator__c == '!=' ) {
                            
                            if( ruleObj.Field_Value__c.isNumeric() ) {
                                Decimal fieldValue = getDecimalValue( ruleObj, obj );
                                System.debug( 'fieldValue - ' + fieldValue);
                                if( fieldValue != null && fieldValue != Decimal.valueOf( ruleObj.Field_Value__c ) ) {
                                    ruleResponse = true;
                                }
                                 else { ruleResponse = false; break; }
                            }
                            else {
                                String fieldValue = getStringValue( ruleObj, obj );
                                System.debug( 'fieldValue - ' + fieldValue);
                                if( String.isNotBlank( fieldValue ) && !String.valueOf( ruleObj.Field_Value__c ).equalsIgnoreCase( fieldValue ) ) {
                                    ruleResponse = true;
                                }
                                else { ruleResponse = false; break; }
                            }
                        }//End !=
                        else if( ruleObj.Operator__c == '<') {

                            Decimal fieldValue = getDecimalValue( ruleObj, obj );
                            System.debug( 'fieldValue - ' + fieldValue);
                            if( fieldValue != null && fieldValue < Decimal.valueOf( ruleObj.Field_Value__c ) ) {
                                ruleResponse = true; 
                            }
                            else { 
                                ruleResponse = false; 
                                break;
                            }
                        }//End <
                        else if( ruleObj.Operator__c == '>') {
                            
                            Decimal fieldValue = getDecimalValue( ruleObj, obj );
                            System.debug( 'fieldValue - ' + fieldValue);
                            if( fieldValue != null && fieldValue > Decimal.valueOf( ruleObj.Field_Value__c) ) {
                                ruleResponse = true; 
                            }
                             else { ruleResponse = false; break; }
                        }//End >
                        else if( ruleObj.Operator__c == '<=') {

                            Decimal fieldValue = getDecimalValue( ruleObj, obj );
                            System.debug( 'fieldValue - ' + fieldValue);                            
                            if( fieldValue != null && fieldValue <= Decimal.valueOf( ruleObj.Field_Value__c ) ) {
                                ruleResponse = true; 
                            }
                             else { ruleResponse = false; break; }
                        }//End <=
                        else if( ruleObj.Operator__c == '>=') {

                            Decimal fieldValue = getDecimalValue( ruleObj, obj );
                            System.debug( 'fieldValue - ' + fieldValue); 
                            if( fieldValue != null && fieldValue >= Decimal.valueOf( ruleObj.Field_Value__c ) ) {
                                ruleResponse = true;
                            }
                             else { ruleResponse = false; break; }
                        }//End >=
                        else if( ruleObj.Operator__c == 'Contains') {

                            String fieldValue = getStringValue( ruleObj, obj );
                            System.debug( 'fieldValue - ' + fieldValue); 
                            if( String.isNotBlank( fieldValue ) && fieldValue.toLowerCase().contains( ruleObj.Field_Value__c.toLowerCase() ) ) {
                                ruleResponse = true; 
                            }
                             else { ruleResponse = false; break; }
                        }//End Contains
                        else if( ruleObj.Operator__c == 'Does Not Contains') {
                            String fieldValue = getStringValue( ruleObj, obj );
                            System.debug( 'fieldValue - ' + fieldValue); 
                            if( String.isNotBlank( fieldValue ) && !fieldValue.toLowerCase().contains( String.valueOf( ruleObj.Field_Value__c.toLowerCase() ) ) ) {
                                ruleResponse = true;
                            }
                             else { ruleResponse = false; break; }
                        }//End Does Not Contains
                        else if( ruleObj.Operator__c == 'Starts With') {
                            
                            String fieldValue = getStringValue( ruleObj, obj );
                            System.debug( 'fieldValue - ' + fieldValue); 
                            if( String.isNotBlank( fieldValue ) && fieldValue.startsWithIgnoreCase( ruleObj.Field_Value__c ) ) {
                                ruleResponse = true;
                            }
                             else { ruleResponse = false; break; }
                        }//End Starts With
                         else if( ruleObj.Operator__c == 'In') {
                            
                            String fieldValue = getStringValue( ruleObj, obj );
                            System.debug('fieldValue: '+fieldValue);
                            if( String.isNotBlank( fieldValue ) ) {
                                fieldValue = fieldValue.toLowerCase();
                                Set<String> setOfValues = new Set<String>();
                                if( ruleObj.Field_Value__c.contains( ',' ) ) {
                                    setOfValues.addAll( ruleObj.Field_Value__c.toLowerCase().split( ',' ) );
                                }
                                else {
                                    setOfValues.add( ruleObj.Field_Value__c );
                                }
                                if( setOfValues.contains( fieldValue ) ) {
                                    ruleResponse = true; 
                                }
                                else { ruleResponse = false; break; }
                            }
                            else { ruleResponse = false; break; }
                        }//End In
                        else if( ruleObj.Operator__c == 'Not In') {
                            
                            String fieldValue = getStringValue( ruleObj, obj );
                            
                            
                            System.debug('fieldValue: '+fieldValue);
                            if( String.isNotBlank( fieldValue ) ) {
                                fieldValue = fieldValue.toLowerCase();
                                Set<String> setOfValues = new Set<String>();
                                if( ruleObj.Field_Value__c.contains( ',' ) ) {
                                    setOfValues.addAll( ruleObj.Field_Value__c.toLowerCase().split( ',' ) );
                                }
                                else {
                                    setOfValues.add( ruleObj.Field_Value__c );
                                }

                                if( !setOfValues.contains( fieldValue ) ) {
                                    ruleResponse = true; 
                                }
                                else { ruleResponse = false; break; }
                            }
                            else { ruleResponse = false; break; }
                        }//End Not In
                        System.debug('------------------------------------------------------');
                    }//End For Loop

                    System.debug( 'ruleResponse = ' + ruleResponse);
                    System.debug('********************************************************');

                    if( ruleResponse ) {
                        return objectName + ':' + ruleNumber + '-' + processName ;
                    }
                }//End if
            }//End for loop of map
            return null;
        }//End map if
        return null;
    }//end Method

/**********************************************************************************************
Method Description  : Method to return decimal value from record
Parameters          : SF Rule Engine instance, sObject Instance
Return Type         : Map of SF Rule Engine, sObject instance, Object Name
**********************************************************************************************/
    public static Decimal getDecimalValue( SF_Rule_Engines__mdt ruleObj, sObject obj ) {
        
        if( ruleObj.Field_API_Name__c.contains('.') ) {
            String strAfter = ruleObj.Field_API_Name__c.subStringAfter('.');
            String relatedObject1 = ruleObj.Field_API_Name__c.subStringBefore('.');
            if( strAfter.contains('.') ) {
                
                String relatedObject2 = strAfter.subStringBefore('.');
                strAfter = strAfter.subStringAfter('.');
                if( strAfter.contains('.') ) {

                    // 3 level parenting
                    String relatedObject3 = strAfter.subStringBefore('.');
                    String fieldOfRelatedObj = strAfter.subStringAfter('.');
                    System.debug('Parenting 3---');
                    System.debug('fieldOfRelatedObj-'+fieldOfRelatedObj);
                    System.debug('related expression--' + relatedObject1 + '.' + relatedObject2 + '.' + relatedObject3 + '.' + fieldOfRelatedObj );
                    if(obj.getSobject( relatedObject1 ).getSobject( relatedObject2 ).getSobject( relatedObject3 ).get( fieldOfRelatedObj ) != null) {
                        return Decimal.valueOf ( String.valueOf( obj.getSobject( relatedObject1 ).getSobject( relatedObject2 ).getSobject( relatedObject3 ).get( fieldOfRelatedObj ) ) );   
                    } else {
                        return null;
                    }
                    //return ( Decimal )obj.getSobject( relatedObject1 ).getSobject( relatedObject2 ).getSobject( relatedObject3 ).get( fieldOfRelatedObj );
                }
                else {

                    // 2 level parenting
                    String fieldOfRelatedObj = strAfter;
                    System.debug('Parenting 2---');
                    System.debug('fieldOfRelatedObj-'+fieldOfRelatedObj);
                    System.debug('related expression--' + relatedObject1 + '.' + relatedObject2 + '.' + fieldOfRelatedObj );
                    if(obj.getSobject( relatedObject1 ).getSobject( relatedObject2 ).get( fieldOfRelatedObj ) != null) {
                        return Decimal.valueOf ( String.valueOf( obj.getSobject( relatedObject1 ).getSobject( relatedObject2 ).get( fieldOfRelatedObj ) ) );
                    } else {
                        return null;
                    }
                    //return ( Decimal )obj.getSobject( relatedObject1 ).getSobject( relatedObject2 ).get( fieldOfRelatedObj );
                }
            }
            else {

                // 1 level parenting
                System.debug('Parenting 1---');
                System.debug('fieldOfRelatedObj-' + strAfter);
                System.debug('related expression--' + relatedObject1 + '.' + strAfter );
                if(obj.getSobject( relatedObject1 ).get( strAfter ) != null) {
                    return Decimal.valueOf (  String.valueOf( obj.getSobject( relatedObject1 ).get( strAfter ) ) );
                } else {
                    return null;
                }
                //return ( Decimal )obj.getSobject( relatedObject1 ).get( strAfter );
            }
        }
        else {
            //return ( Decimal )obj.get( ruleObj.Field_API_Name__c );
            if(obj.get( ruleObj.Field_API_Name__c ) != null) {
                return Decimal.valueOf ( String.valueOf( obj.get( ruleObj.Field_API_Name__c ) ) );
            } else {
                return null;
            }
        }
    }

/**********************************************************************************************
Method Description  : Method to return String value from record
Parameters          : SF Rule Engine instance, sObject Instance
Return Type         : Map of SF Rule Engine, sObject instance, Object Name
**********************************************************************************************/
    public static String getStringValue( SF_Rule_Engines__mdt ruleObj, sObject obj ) {
        
        if( ruleObj.Field_API_Name__c.contains('.') ) {
            String strAfter = ruleObj.Field_API_Name__c.subStringAfter('.');
            String relatedObject1 = ruleObj.Field_API_Name__c.subStringBefore('.');
            if( strAfter.contains('.') ) {
                
                String relatedObject2 = strAfter.subStringBefore('.');
                strAfter = strAfter.subStringAfter('.');

                if( strAfter.contains('.') ) {

                    // 3 level parenting
                    String relatedObject3 = strAfter.subStringBefore('.');
                    String fieldOfRelatedObj = strAfter.subStringAfter('.');
                    System.debug('Parenting 3---');
                    System.debug('fieldOfRelatedObj-'+fieldOfRelatedObj);
                    System.debug('related expression--' + relatedObject1 + '.' + relatedObject2 + '.' + relatedObject3 + '.' + fieldOfRelatedObj );
                    if(obj.getSobject( relatedObject1 ).getSobject( relatedObject2 ).getSobject( relatedObject3 ).get( fieldOfRelatedObj ) != null) {
                        return String.valueOf( obj.getSobject( relatedObject1 ).getSobject( relatedObject2 ).getSobject( relatedObject3 ).get( fieldOfRelatedObj ) );
                    } else {
                        return null;
                    }
                }
                else {

                    // 2 level parenting
                    String fieldOfRelatedObj = strAfter;
                    System.debug('Parenting 2---');
                    System.debug('fieldOfRelatedObj-'+fieldOfRelatedObj);
                    System.debug('related expression--' + relatedObject1 + '.' + relatedObject2 + '.' + fieldOfRelatedObj );
                    if(obj.getSobject( relatedObject1 ).getSobject( relatedObject2 ).get( fieldOfRelatedObj ) != null) {
                        return String.valueOf( obj.getSobject( relatedObject1 ).getSobject( relatedObject2 ).get( fieldOfRelatedObj ) );
                    } else {
                        return null;
                    }
                }
            }
            else {

                // 1 level parenting
                System.debug('Parenting 1---');
                System.debug('fieldOfRelatedObj-' + strAfter);
                System.debug('related expression--' + relatedObject1 + '.' + strAfter );
                if(obj.getSobject( relatedObject1 ).get( strAfter ) != null) {
                    return String.valueOf( obj.getSobject( relatedObject1 ).get( strAfter ) );
                } else {
                    return null;
                }
            }
        }
        else {
            if(obj.get( ruleObj.Field_API_Name__c ) != null) {
                return String.valueOf( obj.get( ruleObj.Field_API_Name__c ) );
            } else {
                return null;
            }
        }
    }

/**********************************************************************************************
Method Description  : Method to get Promotion Category once the conditions satisfies in SF rule 
                        against single process and object 
Parameters          : SF Rule Number, Process name, Object Name
Return Type         : List of collection promotions
**********************************************************************************************/
    public static List<Collection_Promotion_Config__mdt> getPromotionCategory( String ruleNumber
                                                                            , String processName
                                                                            , String objectName ) {
        
        System.debug( ' Rule Key in CS = ' + ruleNumber);
        System.debug( ' processName = ' + processName);
        System.debug( ' objectName = ' + objectName);

        List<Collection_Promotion_Config__mdt> listPromConfig = new List<Collection_Promotion_Config__mdt>();
        if( String.isNotBlank( ruleNumber ) 
        && String.isNotBlank( processName )  
        && String.isNotBlank( objectName ) ) {

            listPromConfig = [SELECT
                                    Discount__c
                                    , Discount_Category__c
                                    , Discount_Type__c
                                    , Offer_Target__c
                                    , Process_Name__c
                                    , Rule_Number__c
                                    , Object_API_Name__c
                                    , Offer_From__c
                                FROM
                                    Collection_Promotion_Config__mdt
                                WHERE
                                    Rule_Number__c =: ruleNumber
                                AND
                                    Process_Name__c =: processName
                                AND
                                    Object_API_Name__c =: objectName
                                ORDER BY
                                    Rule_Number__c ];
            
            System.debug( 'listPromConfig == ' + listPromConfig);

            return listPromConfig;
        }
        return null;
    }
}