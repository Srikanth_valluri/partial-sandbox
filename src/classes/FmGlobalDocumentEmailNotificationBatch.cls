global without sharing class FmGlobalDocumentEmailNotificationBatch
        implements Database.Batchable<String>, Database.AllowsCallouts {

    FM_Document_Upload_Batch__c docUploadBatch;
    Set<String> recipientEmails;
    Set<String> recipientSms;
    Map<String, String> mapEmailToAccountId = new Map<String, String>();
    String EXCEPT_DTPC_UNITS = 'Everyone except DTPC';

    global FmGlobalDocumentEmailNotificationBatch(Id docUploadBatchId) {
        // System.debug('docUploadBatchId : '+docUploadBatchId);
        List<FM_Document_Upload_Batch__c> lstDocUploadBatch = [
            SELECT  Id
                    , Name
                    , FM_Recipient__c
                    , Notification_Email_Message__c
                    , Notification_Email_Subject__c
                    , Notification_SMS_Message__c
            FROM    FM_Document_Upload_Batch__c
            WHERE   Id = :docUploadBatchId
        ];
        if (lstDocUploadBatch.isEmpty()) {
            return;
        }
        docUploadBatch = lstDocUploadBatch[0];

        recipientEmails = new Set<String>();
        recipientSms = new Set<String>();

        Set<String> setActiveStatuses = Booking_Unit_Active_Status__c.getAll().keySet();
        Set<String> invalidHandoverStatus = new Set<String> {NULL, ''};
        invalidHandoverStatus.addAll(Label.FM_Booking_Unit_Handover_Status.split(','));

        Set<String> ownerRecipientValues = new Set<String> {'Everyone', 'Owners', 'Owners and Tenants'};
        Set<String> tenantRecipientValues = new Set<String> {'Everyone', 'Tenants', 'Owners and Tenants'};

        Set<Id> allPropertyIds = new Set<Id>();


        if (String.isBlank(docUploadBatch.Notification_Email_Message__c)
            && String.isBlank(docUploadBatch.Notification_SMS_Message__c)) {
            return;
        }

        for (Booking_Unit__c unit : [
            SELECT  Id
                    , Booking__c
                    , Booking__r.Id
                    , Booking__r.Account__c
                    , Booking__r.Account__r.Id
                    , Booking__r.Account__r.Email__c
                    , Booking__r.Account__r.Email__pc
                    , Booking__r.Account__r.IsPersonAccount
                    , Booking__r.Account__r.Mobile__c
                    , Booking__r.Account__r.Mobile_Phone_Encrypt__pc
                    , Tenant__c
                    , Tenant__r.Id
                    , Tenant__r.Email__c
                    , Tenant__r.Email__pc
                    , Tenant__r.IsPersonAccount
                    , Tenant__r.Mobile__c
                    , Tenant__r.Mobile_Phone_Encrypt__pc
                    , Inventory__r.Building_Location__r.Property_Name__c
            FROM    Booking_Unit__c
            WHERE   Handover_Status__c NOT IN :invalidHandoverStatus
                AND Registration_Status__c IN :setActiveStatuses
                AND (Property_City__c = 'Dubai' OR Property_City__c ='DUBAI')
        ]) {
            String email, accountId, mobile;
            if (ownerRecipientValues.contains(docUploadBatch.FM_Recipient__c)) {
                if (String.isNotBlank(docUploadBatch.Notification_Email_Message__c)) {
                    email = unit.Booking__r.Account__r.IsPersonAccount
                        ? unit.Booking__r.Account__r.Email__pc
                        : unit.Booking__r.Account__r.Email__c;
                    accountId = unit.Booking__r.Account__c;
                    if (String.isNotBlank(email)) {
                        recipientEmails.add(email);
                        mapEmailToAccountId.put(email, accountId);
                    }
                }
                if (String.isNotBlank(docUploadBatch.Notification_SMS_Message__c)) {
                    mobile = unit.Booking__r.Account__r.IsPersonAccount
                        ? unit.Booking__r.Account__r.Mobile_Phone_Encrypt__pc
                        : unit.Booking__r.Account__r.Mobile__c;
                    if (String.isNotBlank(mobile)) {
                        recipientSms.add(mobile);
                    }
                }
            }

            if (tenantRecipientValues.contains(docUploadBatch.FM_Recipient__c)) {
                if (String.isNotBlank(docUploadBatch.Notification_Email_Message__c)) {
                    email = unit.Tenant__r.IsPersonAccount
                        ? unit.Tenant__r.Email__pc
                        : unit.Tenant__r.Email__c;
                    accountId = unit.Tenant__c;
                    if (String.isNotBlank(email)) {
                        recipientEmails.add(email);
                        mapEmailToAccountId.put(email, accountId);
                    }
                }
                if (String.isNotBlank(docUploadBatch.Notification_SMS_Message__c)) {
                    mobile = unit.Tenant__r.IsPersonAccount
                        ? unit.Tenant__r.Mobile_Phone_Encrypt__pc
                        : unit.Tenant__r.Mobile__c;
                    if (String.isNotBlank(mobile)) {
                        recipientSms.add(mobile);
                    }
                }
            }
            allPropertyIds.add(unit.Inventory__r.Building_Location__r.Property_Name__c);
        }

        if (new Set<String>{'Everyone', 'Board Members'}.contains(docUploadBatch.FM_Recipient__c)) {
            for (Board_Member__c boardMember : [
                SELECT  Id
                        , Property__c
                        , Board_Member__c
                        , Board_Member__r.Name
                        , Board_Member__r.Email__c
                        , Board_Member__r.Email__pc
                        , Board_Member__r.IsPersonAccount
                        , Board_Member__r.Mobile_Phone_Encrypt__pc
                        , Board_Member__r.Mobile__c
                        , Email__c
                FROM    Board_Member__c
                WHERE   Property__c IN :allPropertyIds
            ]) {
                if (String.isNotBlank(docUploadBatch.Notification_Email_Message__c)) {
                    String email = boardMember.Board_Member__c == NULL
                        ? boardMember.Email__c
                        : (boardMember.Board_Member__r.IsPersonAccount
                           ? boardMember.Board_Member__r.Email__pc
                           : boardMember.Board_Member__r.Email__c
                          );
                      if (String.isNotBlank(email)) {
                        //System.debug('board member email = ' + email);
                        recipientEmails.add(email);
                        mapEmailToAccountId.put(email, boardMember.Board_Member__c);
                    }
                }
                if (String.isNotBlank(docUploadBatch.Notification_SMS_Message__c)) {
                    String mobile = boardMember.Board_Member__r.IsPersonAccount
                        ? boardMember.Board_Member__r.Mobile_Phone_Encrypt__pc
                        : boardMember.Board_Member__r.Mobile__c;
                    if (String.isNotBlank(mobile)) {
                        recipientSms.add(mobile);
                    }
                }
            }
        }

        for (Inventory__c inventory : [
            SELECT  Id
                    , Tenant__c
                    , Tenant__r.Id
                    , Tenant__r.Email__c
                    , Tenant__r.Email__pc
                    , Tenant__r.IsPersonAccount
                    , Status__c
                    , Tenant__r.Mobile_Phone_Encrypt__pc
                    , Tenant__r.Mobile__c
            FROM    Inventory__c
            WHERE   Status__c = 'Restricted'
                AND Property_Country__c = 'United Arab Emirates'
        ]) {
            String email, mobile;
            if (String.isNotBlank(docUploadBatch.Notification_Email_Message__c)) {
                if (tenantRecipientValues.contains(docUploadBatch.FM_Recipient__c)) {
                    email = inventory.Tenant__r.IsPersonAccount
                        ? inventory.Tenant__r.Email__pc
                        : inventory.Tenant__r.Email__c;
                }
                //System.debug('Inventory Tenant Email = ' + email);
                if (String.isNotBlank(email)) {
                    recipientEmails.add(email);
                    mapEmailToAccountId.put(email, inventory.Tenant__c);
                }
            }
            if (String.isNotBlank(docUploadBatch.Notification_SMS_Message__c)) {
                mobile = inventory.Tenant__r.IsPersonAccount
                    ? inventory.Tenant__r.Mobile_Phone_Encrypt__pc
                    : inventory.Tenant__r.Mobile__c;
                if (String.isNotBlank(mobile)) {
                    recipientSms.add(mobile);
                }
            }
        }

        System.debug('recipientEmails = ' + recipientEmails);
        System.debug('recipientEmails.size = ' + recipientEmails.size());
        System.debug('recipientSms : '+recipientSms);
        System.debug('recipientSms.size = ' + recipientSms.size());

        List<SMS_History__c> lstSmsHistory = new List<SMS_History__c>();
        for (String strMobile : recipientSms) {
            SMS_History__c objSmsHistory = new SMS_History__c();
            objSmsHistory.Message__c = docUploadBatch.Notification_SMS_Message__c;
            objSmsHistory.Phone_Number__c = strMobile;
            lstSmsHistory.add(objSmsHistory);
        }
        // System.debug('SMS History : ' + lstSmsHistory);
        // insert lstSmsHistory;
        Database.SaveResult[] smsSrList = Database.insert(lstSmsHistory, false);

        Set<String> setSmsHistoryId = new Set<String>();
        List<Error_Log__c> lstErrorLogs = new List<Error_Log__c>();
        for (Database.SaveResult sr : smsSrList) {
            if (sr.isSuccess()) {
                setSmsHistoryId.add(sr.getId());
            }else {
                for (Database.Error error : sr.getErrors()) {
                    Error_Log__c objErrorLog = new Error_Log__c();
                    objErrorLog.Error_Details__c = error.getMessage();
                    lstErrorLogs.add(objErrorLog);
                }
            }
        }
        insert lstErrorLogs;

        System.debug('setSmsHistoryId :'+setSmsHistoryId);
        System.enqueueJob(new SendSMSQueueable(setSmsHistoryId));
    }

    //Constructor only for 'Everyone except DTPC' doc level records. Created on 29 Dec'19
    global FmGlobalDocumentEmailNotificationBatch(Id docUploadBatchId, String docLevel) {
      if(docLevel != EXCEPT_DTPC_UNITS ) {
          return;
      }

      List<FM_Document_Upload_Batch__c> lstDocUploadBatch = [
          SELECT  Id
                  , Name
                  , FM_Recipient__c
                  , Notification_Email_Message__c
                  , Notification_Email_Subject__c
                  , Notification_SMS_Message__c
          FROM    FM_Document_Upload_Batch__c
          WHERE   Id = :docUploadBatchId
      ];
      if (lstDocUploadBatch.isEmpty()) {
          return;
      }
      docUploadBatch = lstDocUploadBatch[0];

      recipientEmails = new Set<String>();
      recipientSms = new Set<String>();

      Set<String> setActiveStatuses = Booking_Unit_Active_Status__c.getAll().keySet();
      Set<String> invalidHandoverStatus = new Set<String> {NULL, ''};
      invalidHandoverStatus.addAll(Label.FM_Booking_Unit_Handover_Status.split(','));

      Set<String> ownerRecipientValues = new Set<String> {'Everyone', 'Owners', 'Owners and Tenants'};
      Set<String> tenantRecipientValues = new Set<String> {'Everyone', 'Tenants', 'Owners and Tenants'};

      Set<Id> allPropertyIds = new Set<Id>();


      if (String.isBlank(docUploadBatch.Notification_Email_Message__c)
          && String.isBlank(docUploadBatch.Notification_SMS_Message__c)) {
          return;
      }

      for (Booking_Unit__c unit : [
          SELECT  Id
                  , Booking__c
                  , Booking__r.Id
                  , Booking__r.Account__c
                  , Booking__r.Account__r.Id
                  , Booking__r.Account__r.Email__c
                  , Booking__r.Account__r.Email__pc
                  , Booking__r.Account__r.IsPersonAccount
                  , Booking__r.Account__r.Mobile__c
                  , Booking__r.Account__r.Mobile_Phone_Encrypt__pc
                  , Tenant__c
                  , Tenant__r.Id
                  , Tenant__r.Email__c
                  , Tenant__r.Email__pc
                  , Tenant__r.IsPersonAccount
                  , Tenant__r.Mobile__c
                  , Tenant__r.Mobile_Phone_Encrypt__pc
                  , Inventory__r.Building_Location__r.Property_Name__c
          FROM    Booking_Unit__c
          WHERE   Handover_Status__c NOT IN :invalidHandoverStatus
                   AND Registration_Status__c IN :setActiveStatuses
          AND (Property_City__c = 'Dubai' OR Property_City__c ='DUBAI')
                   AND Inventory__r.Building_Location__r.Name != 'DTPC'
      ]) {
          String email, accountId, mobile;
          if (ownerRecipientValues.contains(docUploadBatch.FM_Recipient__c)) {
              if (String.isNotBlank(docUploadBatch.Notification_Email_Message__c)) {
                  email = unit.Booking__r.Account__r.IsPersonAccount
                      ? unit.Booking__r.Account__r.Email__pc
                      : unit.Booking__r.Account__r.Email__c;
                  accountId = unit.Booking__r.Account__c;
                  if (String.isNotBlank(email)) {
                      recipientEmails.add(email);
                      mapEmailToAccountId.put(email, accountId);
                  }
              }
              if (String.isNotBlank(docUploadBatch.Notification_SMS_Message__c)) {
                  mobile = unit.Booking__r.Account__r.IsPersonAccount
                      ? unit.Booking__r.Account__r.Mobile_Phone_Encrypt__pc
                      : unit.Booking__r.Account__r.Mobile__c;
                  if (String.isNotBlank(mobile)) {
                      recipientSms.add(mobile);
                  }
              }
          }

          if (tenantRecipientValues.contains(docUploadBatch.FM_Recipient__c)) {
              if (String.isNotBlank(docUploadBatch.Notification_Email_Message__c)) {
                  email = unit.Tenant__r.IsPersonAccount
                      ? unit.Tenant__r.Email__pc
                      : unit.Tenant__r.Email__c;
                  accountId = unit.Tenant__c;
                  if (String.isNotBlank(email)) {
                      recipientEmails.add(email);
                      mapEmailToAccountId.put(email, accountId);
                  }
              }
              if (String.isNotBlank(docUploadBatch.Notification_SMS_Message__c)) {
                  mobile = unit.Tenant__r.IsPersonAccount
                      ? unit.Tenant__r.Mobile_Phone_Encrypt__pc
                      : unit.Tenant__r.Mobile__c;
                  if (String.isNotBlank(mobile)) {
                      recipientSms.add(mobile);
                  }
              }
          }
          allPropertyIds.add(unit.Inventory__r.Building_Location__r.Property_Name__c);
      }

      if (new Set<String>{'Everyone', 'Board Members'}.contains(docUploadBatch.FM_Recipient__c)) {
          for (Board_Member__c boardMember : [
              SELECT  Id
                      , Property__c
                      , Board_Member__c
                      , Board_Member__r.Name
                      , Board_Member__r.Email__c
                      , Board_Member__r.Email__pc
                      , Board_Member__r.IsPersonAccount
                      , Board_Member__r.Mobile_Phone_Encrypt__pc
                      , Board_Member__r.Mobile__c
                      , Email__c
              FROM    Board_Member__c
              WHERE   Property__c IN :allPropertyIds
          ]) {
              if (String.isNotBlank(docUploadBatch.Notification_Email_Message__c)) {
                  String email = boardMember.Board_Member__c == NULL
                      ? boardMember.Email__c
                      : (boardMember.Board_Member__r.IsPersonAccount
                         ? boardMember.Board_Member__r.Email__pc
                         : boardMember.Board_Member__r.Email__c
                        );
                    if (String.isNotBlank(email)) {
                      //System.debug('board member email = ' + email);
                      recipientEmails.add(email);
                      mapEmailToAccountId.put(email, boardMember.Board_Member__c);
                  }
              }
              if (String.isNotBlank(docUploadBatch.Notification_SMS_Message__c)) {
                  String mobile = boardMember.Board_Member__r.IsPersonAccount
                      ? boardMember.Board_Member__r.Mobile_Phone_Encrypt__pc
                      : boardMember.Board_Member__r.Mobile__c;
                  if (String.isNotBlank(mobile)) {
                      recipientSms.add(mobile);
                  }
              }
          }
      }

      for (Inventory__c inventory : [
          SELECT  Id
                  , Tenant__c
                  , Tenant__r.Id
                  , Tenant__r.Email__c
                  , Tenant__r.Email__pc
                  , Tenant__r.IsPersonAccount
                  , Status__c
                  , Tenant__r.Mobile_Phone_Encrypt__pc
                  , Tenant__r.Mobile__c
          FROM    Inventory__c
          WHERE   Status__c = 'Restricted'
              AND Property_Country__c = 'United Arab Emirates'
      ]) {
          String email, mobile;
          if (String.isNotBlank(docUploadBatch.Notification_Email_Message__c)) {
              if (tenantRecipientValues.contains(docUploadBatch.FM_Recipient__c)) {
                  email = inventory.Tenant__r.IsPersonAccount
                      ? inventory.Tenant__r.Email__pc
                      : inventory.Tenant__r.Email__c;
              }
              //System.debug('Inventory Tenant Email = ' + email);
              if (String.isNotBlank(email)) {
                  recipientEmails.add(email);
                  mapEmailToAccountId.put(email, inventory.Tenant__c);
              }
          }
          if (String.isNotBlank(docUploadBatch.Notification_SMS_Message__c)) {
              mobile = inventory.Tenant__r.IsPersonAccount
                  ? inventory.Tenant__r.Mobile_Phone_Encrypt__pc
                  : inventory.Tenant__r.Mobile__c;
              if (String.isNotBlank(mobile)) {
                  recipientSms.add(mobile);
              }
          }
      }

      System.debug('recipientEmails = ' + recipientEmails);
      System.debug('recipientEmails.size = ' + recipientEmails.size());
      System.debug('recipientSms : '+recipientSms);
      System.debug('recipientSms.size = ' + recipientSms.size());

      List<SMS_History__c> lstSmsHistory = new List<SMS_History__c>();
      for (String strMobile : recipientSms) {
          SMS_History__c objSmsHistory = new SMS_History__c();
          objSmsHistory.Message__c = docUploadBatch.Notification_SMS_Message__c;
          objSmsHistory.Phone_Number__c = strMobile;
          lstSmsHistory.add(objSmsHistory);
      }
      // System.debug('SMS History : ' + lstSmsHistory);
      // insert lstSmsHistory;
      Database.SaveResult[] smsSrList = Database.insert(lstSmsHistory, false);

      Set<String> setSmsHistoryId = new Set<String>();
      List<Error_Log__c> lstErrorLogs = new List<Error_Log__c>();
      for (Database.SaveResult sr : smsSrList) {
          if (sr.isSuccess()) {
              setSmsHistoryId.add(sr.getId());
          }else {
              for (Database.Error error : sr.getErrors()) {
                  Error_Log__c objErrorLog = new Error_Log__c();
                  objErrorLog.Error_Details__c = error.getMessage();
                  lstErrorLogs.add(objErrorLog);
              }
          }
      }
      insert lstErrorLogs;

      System.debug('setSmsHistoryId :'+setSmsHistoryId);
      System.enqueueJob(new SendSMSQueueable(setSmsHistoryId));
  } //Constructor END

    global Iterable<String> start(Database.BatchableContext BC) {
        // System.debug('recipientEmails : '+recipientEmails);
        return new List<String>(recipientEmails);
    }

    global void execute(Database.BatchableContext BC, List<String> scope) {

           // System.Debug('scope = ' + scope);

           String htmlBody = '';

           htmlBody += '<head>' +
               '<style type="text/css">' +
               'table, thead, tbody, tr, th, td {' +
               'border:1px solid;' +
               '}' +
               'table {' +
               'border-collapse: collapse;' +
               'font-family: "Trebuchet MS", Helvetica, sans-serif;' +
               '}' +
               'thead {' +
               'background: #D3D3D3;' +
               'background: #36304a;' +
               '}' +
               'th {' +
               'color: #fff;' +
               'font-weight: normal;' +
               '}' +
               'tbody tr:nth-child(even) {' +
               'background-color: #f5f5f5;' +
               '}' +
               'tbody tr {' +
               'color: #555;' +
               '}' +
               '</style>' +
               '</head>';

           htmlBody += String.isNotBlank(docUploadBatch.Notification_Email_Message__c)
               ? (docUploadBatch.Notification_Email_Message__c) : '';

           htmlBody += '<br/><br/><br/>';

           htmlBody += '<table>' +
               '<thead>' +
               '<tr>' +
               '<th>Sr. No</th>' +
               '<th>Document Name</th>' +
               '<th>Document Type</th>' +
               '<th>Document URL</th>' +
               '</tr>' +
               '</thead>';
           htmlBody += '<tbody>';

           Integer docNumber = 1;
           for (SR_Attachments__c doc : [
               SELECT  Id
               , Name
               , Document_Type__c
               , Attachment_URL__c
               FROM    SR_Attachments__c
               WHERE   FM_Document_Upload_Batch__c = :docUploadBatch.Id
           ]) {
               htmlBody +=
                   '<tr>' +
                   '<td>' + (docNumber++) + '</td>' +
                   '<td>' + doc.Name + '</td>' +
                   '<td>' + doc.Document_Type__c + '</td>' +
                   '<td><a href="' + doc.Attachment_URL__c + '">' + doc.Attachment_URL__c + '</a></td>' +
                   '</tr>';
           }

           htmlBody += '</tbody>' +
               '</table>';

           // System.debug('htmlBody = ' + htmlBody);

           List<EmailMessage> lstEmail = new List<EmailMessage>();
           List<Error_Log__c> lstErrors = new List<Error_Log__c>();

           List<OrgWideEmailAddress> orgWideAddressHelloDamac = [
               SELECT  Id
               , Address
               , DisplayName
               FROM    OrgWideEmailAddress
               WHERE   Id = :Label.CommunityPortalOrgWideAddressId
           ];

           Boolean isSandbox = [SELECT IsSandbox FROM Organization].IsSandbox;

           try {
               for(String email : scope) {
                   // System.debug('email = ' + email);
                   if (isSandbox
                       && !email.containsIgnoreCase('davidvarghese1989@gmail.com')
                       && !email.containsIgnoreCase('@damacgroup.com')
                       && !email.containsIgnoreCase('@eternussolutions.com')
                       && !email.containsIgnoreCase('@mailinator.com')) {
                           continue;
                       }
                   SendGridEmailService.SendGridResponse response = SendGridEmailService.sendEmailService(
                       email, '', '', '', '', '', docUploadBatch.Notification_Email_Subject__c, '',
                       orgWideAddressHelloDamac[0].Address, orgWideAddressHelloDamac[0].DisplayName, '',
                       orgWideAddressHelloDamac[0].DisplayName, 'text/html', htmlBody, '', new List<Attachment>()
                   );
                   // System.Debug('response = ' + response);
                   SYstem.debug('mapEmailToAccountId :' + mapEmailToAccountId);
                   System.debug('email :' + email);

                   String accountId = mapEmailToAccountId.get(email);

                   String sendGridResponseStatus = Test.isRunningTest()
                                                   ? 'Accepted'
                                                   : response.ResponseStatus;

                   if ('Accepted'.equalsIgnoreCase(sendGridResponseStatus)) {
                       System.debug('SendGrid status is accepted for :'+email);
                       EmailMessage mail = new EmailMessage();
                       mail.Subject = docUploadBatch.Notification_Email_Subject__c;
                       mail.MessageDate = System.Today();
                       mail.Status = '3';
                       //mail.RelatedToId = docUploadBatch.Id;//Put FM case id
                       mail.RelatedToId = String.isBlank(accountId) ? NULL : accountId;
                       mail.ToAddress = email;
                       mail.FromAddress = orgWideAddressHelloDamac[0].Address;
                       //mail.TextBody = htmlBody;
                       mail.HtmlBody = htmlBody;
                       mail.CcAddress = '';
                       mail.BccAddress = '';
                       mail.Sent_By_Sendgrid__c = true;
                       mail.Sendgrid_Status__c = response.ResponseStatus;
                       mail.SentGrid_MessageId__c = response.messageId;
                       mail.Booking_Unit__c = NULL;
                       mail.Account__c = String.isBlank(accountId) ? NULL : accountId;
                       lstEmail.add(mail);
                   } else {
                       Error_Log__c objError = new Error_Log__c();
                       objError.Account__c = String.isBlank(accountId) ? NULL : accountId;
                       objError.Error_Details__c = 'Request Failed from SendGrid for ' + docUploadBatch.Name;
                       objError.Process_Name__c = 'Generic Email';
                       lstErrors.add(objError);
                   }
               }//for

               insert lstEmail;
               insert lstErrors;
           } catch(System.Exception excp) {
               // System.debug('excp = ' + excp.getMessage());
               // System.debug('excp = ' + excp);

               Error_Log__c objError = new Error_Log__c();
               objError.Account__c = NULL;
               objError.Error_Details__c = 'Request Failed from SendGrid for ' + docUploadBatch.Id;
               objError.Process_Name__c = 'Generic Email';
               lstErrors.add(objError);
           }
    }

    global void finish(Database.BatchableContext BC) {
        if (Test.isRunningTest()) {
            Integer i = 0;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
        }
    }

}