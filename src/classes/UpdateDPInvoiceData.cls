/**
 * @File Name          : UpdateDPInvoiceData.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 7/9/2019, 7:39:37 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    7/9/2019, 6:54:55 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public without sharing class UpdateDPInvoiceData {
    
    // This function is called from DP Invoice URLs process Builder to update invoice url field
    @InvocableMethod
    public static void updateInvoiceURLs(List<Id> dpInvoiceIdList){
       
        List<DP_Invoices__c> fetchdpInvoiceIdList = [
            SELECT Id,
                Invoice_URLs__c,
                TAX_Invoice__c,
                Other_Language_TAX_Invoice__c,
               // COCD_Letter__c,
                SOA__c,
                Other_Language_SOA__c,
                Cover_Letter__c,
                Registration_Id__c
            FROM DP_Invoices__c
            WHERE Id IN :dpInvoiceIdList
        ];
        for(DP_Invoices__c dpInvoiceRecord : fetchdpInvoiceIdList){
              String docURL = '<ul>';
            
            if(dpInvoiceRecord.TAX_Invoice__c != NULL){
                docURL += '<li>' + '<a href="'+ dpInvoiceRecord.TAX_Invoice__c + '" target="_blank">'+'Tax Invoice' + '</a>' + '</li>';
            }
            if(dpInvoiceRecord.Other_Language_TAX_Invoice__c != NULL){
                docURL += '<li>' + '<a href="'+ dpInvoiceRecord.Other_Language_TAX_Invoice__c + '" target="_blank">'+'Other Language Tax Invoice' + '</a>' + '</li>';
            }
            if(dpInvoiceRecord.SOA__c != NULL){
                docURL += '<li>' + '<a href="'+ dpInvoiceRecord.SOA__c + '" target="_blank">'+'SOA' + '</a>' + '</li>';
            }
            if(dpInvoiceRecord.Other_Language_SOA__c != NULL){
                docURL += '<li>' + '<a href="'+ dpInvoiceRecord.Other_Language_SOA__c + '" target="_blank">'+'Other Language SOA' + '</a>' + '</li>';
            }
            if(dpInvoiceRecord.Cover_Letter__c != NULL){
                docURL += '<li>' + '<a href="'+ dpInvoiceRecord.Cover_Letter__c + '" target="_blank">'+'Cover Letter' + '</a>' + '</li>';
            }
            /*if(dpInvoiceRecord.COCD_Letter__c != NULL){
                docURL += '<li>' + '<a href="'+ dpInvoiceRecord.COCD_Letter__c + '" target="_blank">'+'COCD Letter' + '</a>' + '</li>';
            }*/
            docURL += '</ul>';
            if(String.isNotBlank(docURL)){
                dpInvoiceRecord.Invoice_URLs__c = docURL;
                
            }
            System.debug('dpInvoiceRecord:::::::::::'+dpInvoiceRecord);
           
        }
        if(fetchdpInvoiceIdList != NULL && !fetchdpInvoiceIdList.isEmpty()){
            update fetchdpInvoiceIdList;
        }
        
    }
}