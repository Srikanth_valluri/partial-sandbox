@isTest
public class TriggerFactoryClsTest {
    
    static testMethod void test_method_1(){
        TriggerFactoryCls.BYPASS_ON_TEST = true;
        TriggerFactoryCls.createHandler(NSIBPM__Service_Request__c.sObjectType);
        TriggerFactoryCls.createHandler(Location__c.sObjectType);
        TriggerFactoryCls.createHandler(Inventory__c.sObjectType);
        TriggerFactoryCls.createHandler(Inquiry__c.sObjectType);
        TriggerFactoryCls.createHandler(Account.sObjectType);
        TriggerFactoryCls.createHandler(Event.sObjectType);
        TriggerFactoryCls.createHandler(Task.sObjectType);
        TriggerFactoryCls.createHandler(JO_Campaign_Virtual_Number__c.sObjectType);
        TriggerFactoryCls.createHandler(User.sObjectType);
        TriggerFactoryCls.createHandler(Assigned_PC__c.sObjectType);
        TriggerFactoryCls.createHandler(Agency_PC__c.sObjectType);
        TriggerFactoryCls.createHandler(Inventory_Release__c.sObjectType);
        TriggerFactoryCls.createHandler(Address__c.sObjectType);
        TriggerFactoryCls.createHandler(Inventory_User__c.sObjectType);
        TriggerFactoryCls.createHandler(Assigned_Agent__c.sObjectType);
        TriggerFactoryCls.createHandler(Attachment.sObjectType);
        TriggerFactoryCls.createHandler(Contact.sObjectType);
        TriggerFactoryCls.createHandler(Team_Building__c.sObjectType);
        TriggerFactoryCls.createHandler(Payment_Terms__c.sObjectType);
        TriggerFactoryCls.createHandler(Campaign_Inventory__c.sObjectType);
        TriggerFactoryCls.createHandler(Agent_Commission__c.sObjectType);
        TriggerFactoryCls.createHandler(Buyer__c.sObjectType);
        TriggerFactoryCls.createHandler(deal_team__c.sObjectType);
        TriggerFactoryCls.createHandler(NSIBPM__SR_Doc__c.sObjectType);
        TriggerFactoryCls.createHandler(Proof_of_Payment__c.sObjectType);
        TriggerFactoryCls.createHandler(Target__c.sObjectType);
        TriggerFactoryCls.createHandler(Campaign__c.sObjectType);
        TriggerFactoryCls.createHandler(NSIBPM__Step__c.sObjectType);
        TriggerFactoryCls.createHandler(Unit_Assignment__c.sObjectType);
        TriggerFactoryCls.createHandler(Amendment__c.sObjectType);
        TriggerFactoryCls.createHandler(User_Action_Plan__c.sObjectType);
        TriggerFactoryCls.createHandler(Travel_Details__c.sObjectType);
        TriggerFactoryCls.createHandler(ContentVersion.sObjectType);
        TriggerFactoryCls.createHandler(New_Step__c.sObjectType);
        
        boolean boolVal = TriggerFactoryCls.getBYPASS_UPDATE_TRIGGER();
        TriggerFactoryCls.resetBYPASS_UPDATE_TRIGGER();
        TriggerFactoryCls.setBYPASS_UPDATE_TRIGGER();
    }
    
    static testMethod void test_method_2(){
        List<User> createdUsers = TestDataFactory.createTestUserRecords(new List<User>{new User(UserRoleId='00E9E000000MTyT', ManagerId = UserInfo.getUserId(), IPMS_Employee_ID__c = '1000'),
            new User(UserRoleId='00E9E000000MTyO', ManagerId = UserInfo.getUserId(), IPMS_Employee_ID__c = '1001'), 
            new User(UserRoleId='00E9E000000MTxz', ManagerId = UserInfo.getUserId(), IPMS_Employee_ID__c = '1002'), 
            new User(UserRoleId='00E9E000000MTxk', ManagerId = UserInfo.getUserId(), IPMS_Employee_ID__c = '1003'), 
            new User(UserRoleId='00E9E000000MTxf', ManagerId = UserInfo.getUserId(), IPMS_Employee_ID__c = '1004')});
        
        createdUsers[0].ManagerId = createdUsers[2].Id;
        createdUsers[1].ManagerId = createdUsers[2].Id;
        createdUsers[2].ManagerId = createdUsers[3].Id;
        createdUsers[3].ManagerId = createdUsers[4].Id;
        createdUsers[4].ManagerId = UserInfo.getUserId();
        
        update createdUsers;
        Test.startTest();
        {
            system.runAs(new User(Id = userInfo.getUserId())){
                List<Target__c> createdTargets = TestDataFactory.createTestTargetRecords(new List<Target__c>{
                    new Target__c(User__c=createdUsers[0].Id, Month__c='May'), 
                        new Target__c(User__c = createdUsers[0].Id, Month__c='June'), 
                        new Target__c(User__c = createdUsers[0].Id, Month__c = 'July'), 
                        new Target__c(User__c = createdUsers[1].Id, Month__c='June'),
                        new Target__c(User__c = createdUsers[1].Id, Month__c='May'), 
                        new Target__c(User__c = createdUsers[1].Id, Month__c = 'July'), 
                        new Target__c(User__c = createdUsers[2].Id, Month__c='June'),
                        new Target__c(User__c = createdUsers[2].Id, Month__c='May'), 
                        new Target__c(User__c = createdUsers[2].Id, Month__c = 'July'),
                        new Target__c(User__c = createdUsers[3].Id, Month__c='June'),
                        new Target__c(User__c = createdUsers[3].Id, Month__c='May'), 
                        new Target__c(User__c = createdUsers[3].Id, Month__c = 'July')
                        });
            }
        }
        Test.stopTest();
    }
}