/**
 * Craig - Added Status condition in Case query (NOT - New, Draft Request, Cancelled, In progress) - by Kanu (04-10-2019)
 */

public class CaseListController {

    public transient List<Case> lstCase                 {get; set;}
    public transient String strBaseURL                  {get; set;}
    public transient List<CaseWrapper> lstCseWrapper    {get; set;}
    public transient Boolean isPortalUser               {get; set;}
    public transient String customerAccountId           {get; set;}

    public CaseListController() {
        lstCase = new List<Case>();
        lstCseWrapper =  new List<CaseWrapper>();
        strBaseURL = System.URL.getSalesforceBaseUrl().toExternalForm();
        isPortalUser = CustomerCommunityUtils.isPortalUser(UserInfo.getUserId());
        fetchCases();
    }

    public void fetchCases() {
        Id casePenaltyWaiverRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Penalty Waiver').getRecordTypeId();
        Id caseEmailRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        Id caseNonSRCaseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Non SR case').getRecordTypeId();
        System.debug('customerAccountId ---' + CustomerCommunityUtils.customerAccountId);
        lstCase = [ SELECT      Id,
                                CaseNumber,
                                CreatedDate,
                                Type,
                                SR_type__c,
                                RecordType.Name,
                                Description,
                                Contact.Name,
                                Account.Name,
                                Booking_Unit__r.Unit_Name__c,
                                Account.AnnualRevenue,
                                AccountId,
                                ContactId,
                                Origin,
                                Priority,
                                Status,
                                (
                                SELECT  Id,
                                        Booking_Unit__r.Unit_Name__c
                                FROM    SR_Booking_Units__r
                                )
                    FROM        Case
                    WHERE       ((AccountId = :CustomerCommunityUtils.customerAccountId
                                    AND RecordTypeId NOT IN (:casePenaltyWaiverRTId, :caseEmailRTId, :caseNonSRCaseRTId)
                                    )
                   					OR CreatedById = :UserInfo.getUserId()
                                )
                   				AND Status NOT IN ('New', 'Draft Request', 'Cancelled', 'In progress')
                    ORDER BY    LastModifiedDate desc Limit 6];
                      System.debug('lstCase ---'+lstCase );
                      System.debug('lstCase ---'+lstCase );
        List<CRE_Allocation_Customer_Type_Setting__mdt> lstCREMetadata = [SELECT DeveloperName,
                                                                                 Maximum_Value__c,
                                                                                 Minimum_Value__c,
                                                                                 Is_Max__c
                                                                            FROM CRE_Allocation_Customer_Type_Setting__mdt];
        for (Case objCase : lstCase) {
            System.debug('objCase---'+objCase);
            CaseWrapper objCseWrapper = new CaseWrapper();
            objCseWrapper.objCase = objCase;
            String srType = String.isBlank(objCase.SR_Type__c)
                            ? (String.isBlank(objCase.Type) ? '' : objCase.Type)
                            : objCase.SR_Type__c;
            if ('proof of payment sr'.equalsIgnoreCase(srType)) {
                for (SR_Booking_Unit__c srBookingUnit : objCase.SR_Booking_Units__r) {
                    objCseWrapper.unitName += String.isBlank(srBookingUnit.Booking_Unit__r.Unit_Name__c)
                                                ? '' : srBookingUnit.Booking_Unit__r.Unit_Name__c + ', ';
                }
                objCseWrapper.unitName = objCseWrapper.unitName.removeEnd(', ');
            } else {
                objCseWrapper.unitName = objCase.Booking_Unit__r.Unit_Name__c;
            }

            for (CRE_Allocation_Customer_Type_Setting__mdt objMdt : lstCREMetadata) {
                if (objCase.AccountId != null
                && objCase.Account.AnnualRevenue != null){
                    if (objCase.Account.AnnualRevenue >= objMdt.Minimum_Value__c
                    && objCase.Account.AnnualRevenue <= objMdt.Maximum_Value__c){
                        objCseWrapper.strtype = objMdt.DeveloperName;
                    }
                    if (objCase.Account.AnnualRevenue >= objMdt.Minimum_Value__c
                    && objMdt.Is_Max__c == true){
                        objCseWrapper.strtype = objMdt.DeveloperName;
                        System.debug('objCase.Account.AnnualRevenue'+objCase.Account.AnnualRevenue);
                    }
                }
            }
            lstCseWrapper.add(objCseWrapper);
            System.debug('lstCseWrapper'+lstCseWrapper);
        }
    }

    @RemoteAction
    public static CaseComment createCaseComment(String caseId, String commentMessage){
        CaseComment caseComment = new CaseComment();
        caseComment.ParentId = caseId;
        caseComment.CommentBody = commentMessage;
        insert caseComment;
        return caseComment;
    }

    public class CaseWrapper{
        public Case objCase             {get; set;}
        public String strtype           {get; set;}
        public String unitName          {get; set;}
        public String CaseComments      {get; set;}
        public CaseWrapper(){
            objCase = new Case();
            strtype ='';
            unitName = '';
            CaseComments = '';
        }
    }

    public class caseCommentWrapper{
    public boolean showCases            {get; set;}
    public boolean showComments         {get; set;}
    public CaseWrapper objCaseWrapper   {get; set;}
        public caseCommentWrapper(){
            showCases = false;
            showComments = false;
            objCaseWrapper = new CaseWrapper();
        }
    }
    @RemoteAction
    public static caseCommentWrapper switchCaseView(string caseId,String strView){
        caseCommentWrapper objCaseCommentWrapper = new caseCommentWrapper();
        if(strView =='back'){
            objCaseCommentWrapper.showCases = true;
            objCaseCommentWrapper.showComments = false;
        }
        else{
          objCaseCommentWrapper.showCases = false;
            objCaseCommentWrapper.showComments = true;
        }
        list<Case> lstCase = [SELECT  Id,
                               CaseNumber,
                               CreatedDate,
                               Type,
                               Description,
                               Contact.Name,
                               Account.Name,
                               Booking_Unit__r.Name,
                               Account.AnnualRevenue,
                               AccountId,
                               ContactId,
                               Origin,
                               Priority,
                               Status,
                                     (SELECT ParentId, CommentBody, CreatedBy.Name, CreatedDate FROM CaseComments)
                                FROM case WHERE id =:caseId];
        if(!lstCase.isEmpty()){
            objCaseCommentWrapper.objCaseWrapper.objCase = lstCase[0];
        }
        return objCaseCommentWrapper;
    }

    @RemoteAction
    public static Boolean deleteCase(String caseId) {
        Database.delete(caseId);
        return true;
    }
}