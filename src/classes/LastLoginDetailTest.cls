@isTest
private class LastLoginDetailTest{
    private static testMethod void unitTest(){
        Account objA = TestDataFactory_CRM.createPersonAccount();
        objA.LastName = 'Last';
        insert objA;
        
        /*
        Contact objC = new Contact();
        objC.LastName = 'Last';
        objC.AccountId = objA.Id;
        insert objC;
        */
        
        list<Account> lstA = [Select Id, PersonContactId from Account where Id =: objA.Id];
        list<Profile> lstP =[Select Id from Profile where Name Like 'Customer Community%' limit 1];
        User objU = TestDataFactory_CRM.createUser(objA.LastName, lstP[0].Id, null);
        objU.ContactId = lstA[0].PersonContactId;
        insert objU;
        
        system.runAs(objU) {
            ApexPages.StandardController sc = new ApexPages.StandardController(objA);
            LastLoginDetail objClass = new LastLoginDetail(sc);
        }
    }
}