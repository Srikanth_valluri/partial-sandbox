public without sharing class FmcVacationNoticeController extends VacationNoticeController {

    public List<SelectOption> lstUnit {get; set;}

    public FmcVacationNoticeController() {
        super(false);
        if (FmcUtils.isCurrentView('VacationNotice')) {
            isEdittable = true;
            strFMCaseId = ApexPages.currentPage().getParameters().get('id');
            isMandatory = false;
            strSRType = FmcUtils.isOwner() ? 'Vacation_Notice' : 'Vacation_Notice_T';
            strAccountId = CustomerCommunityUtils.customerAccountId;
            if (String.isBlank(strFMCaseId)) {
               strSelectedUnit = ApexPages.currentPage().getParameters().get('unitId');
               if (String.isBlank(strSelectedUnit)) {
                   lstUnit = getUnitOptions();
               } else {
                   objUnit = FM_Utility.getUnitDetails(strSelectedUnit);
                   lstUnit = new List<SelectOption>{
                       new SelectOption(objUnit.Id, objUnit.Unit_Name__c, true)
                   };
               }
            } else {
                List<FM_Case__c> lstCase = [
                    SELECT  Id
                            , Booking_Unit__c
                            , Booking_Unit__r.Unit_Name__c
                    FROM    FM_Case__c
                    WHERE   Account__c = :strAccountId
                            AND Id = :strFMCaseId
                    LIMIT 1
                ];
                if (!lstCase.isEmpty()) {
                    strSelectedUnit = lstCase[0].Booking_Unit__c;
                    if (String.isBlank(strSelectedUnit)) {
                        lstUnit = getUnitOptions();
                    } else {
                        objUnit = FM_Utility.getUnitDetails(strSelectedUnit);
                        lstUnit = new List<SelectOption>{
                            new SelectOption(objUnit.Id, objUnit.Unit_Name__c, true)
                        };
                    }
                } else {
                    strSelectedUnit = ApexPages.currentPage().getParameters().get('unitId');
                    if (String.isBlank(strSelectedUnit)) {
                        lstUnit = getUnitOptions();
                    } else {
                        objUnit = FM_Utility.getUnitDetails(strSelectedUnit);
                        lstUnit = new List<SelectOption>{
                            new SelectOption(objUnit.Id, objUnit.Unit_Name__c, true)
                        };
                    }
                }
            }
            System.debug('strFMCaseId = ' + strFMCaseId);
            System.debug('strAccountId = ' + strAccountId);
            System.debug('strSelectedUnit = ' + strSelectedUnit);
            if (String.isBlank(strSelectedUnit)) {
                strSelectedUnit = '';
            }
            if(String.isNotBlank(strSelectedUnit)
               || String.isNotBlank(strFMCaseId)){
                init();
            }
        }
    }

    public void selectUnit() {
        System.debug('strSelectedUnit = ' + strSelectedUnit);
        if( String.isNotBlank( strSelectedUnit ) ) {
            init();
        }
    }

    protected override void setCaseOrigin() {
        objFMCase.Origin__c='Portal';
    }

    public void saveAsDraft() {
        createVacationNoticeCase();
    }

    public PageReference submitSr() {
        PageReference nextPage = submitVacationNotice();
        if (nextPage != NULL) {
            String unitId = nextPage.getUrl().substringAfterLast('/');
            nextPage = new PageReference(ApexPages.currentPage().getUrl());
            nextPage.getParameters().clear();
            nextPage.getParameters().put('view', 'CaseDetails');
            nextPage.getParameters().put('id', unitId);
            nextPage.setRedirect(true);
        }
        System.debug('nextPage = ' + nextPage);
        return nextPage;
    }

    private static List<SelectOption> getUnitOptions() {
        List<SelectOption> lstOptions = new List<SelectOption>{new SelectOption('', 'Select', true)};
        for (Booking_Unit__c unit : FmcUtils.queryOwnedUnitsForAccount(
            CustomerCommunityUtils.customerAccountId, 'Id, Name, Unit_Name__c'
        )) {
            lstOptions.add(new SelectOption(unit.Id, unit.Unit_Name__c));
        }
        return lstOptions;
    }

}