@istest
public class IPMS_Requests_Test{



    static testmethod void testMethods(){
    
        Test.startTest();
        string body = '';
        Date d = Date.newinstance(25,12,2021);
        
        Location__c loc=new Location__c();
        loc.Location_ID__c='123';
        insert loc;
        Inventory__c inv = new Inventory__c();
        inv.Unit_Location__c=loc.id;
        insert inv;
        
        NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
        SR.Delivery_mode__c='Email';
        SR.Deal_ID__c='1001';
        insert SR;
        
        List<id> SRids = new List<id>();
        SRids.add(SR.id);
        NSIBPM__SR_Doc__c srdoc= new NSIBPM__SR_Doc__c();
        srdoc.NSIBPM__Service_Request__c=SR.id;
        insert srdoc;
        
        Booking__c bk= new Booking__c();
        bk.Deal_SR__c=SR.id;
        bk.Booking_channel__c='Office';
        insert bk;
        
        Account acc=new Account(Name='Testing Corp Buyer');
        insert acc;
        
        Buyer__c PB= new Buyer__c();
        PB.Primary_Buyer__c=true;
        PB.Account__c=acc.Id;
        PB.Buyer_Type__c='Individual';
        PB.Booking__c=bk.id;
        PB.Phone_Country_Code__c='India: 0091';
        PB.Passport_Expiry_Date__c='25/03/2017';
        PB.CR_Registration_Expiry_Date__c='25/12/2017';
        PB.City__c='Dubai';
        PB.Country__c='United Arab Emirates';
        PB.Address_Line_1__c='street1';
        PB.Address_Changed__c=true;
        PB.Date_of_Birth__c='25/12/1990';
        PB.Email__c='test@test.com';
        PB.First_Name__c='Buyer';
        PB.Last_Name__c='test';
        PB.Nationality__c='Indian'; 
        PB.Passport_Number__c='PP123'; 
        PB.Phone__c='53532255';
        PB.Place_of_Issue__c='Delhi'; 
        PB.Title__c='Mr.';
        PB.party_id__c = '12344';
        pb.Title_Arabic__c = 'Mr';
        pb.First_Name_Arabic__c = 'tyui';
        pb.Last_Name_Arabic__c = 'tyyt';
        pb.Nationality_Arabic__c = 'ABC';
        pb.Passport_Number__c = '456879';
        pb.CR_Number__c = '123';
        pb.Place_of_Issue_Arabic__c = 'ad';
        pb.Address_Line_1_Arabic__c = 'abc';
        pb.Address_Line_2_Arabic__c = 'abc';
        pb.Address_Line_3_Arabic__c = 'abc';
        pb.Address_Line_4_Arabic__c = 'abc';
        pb.City_Arabic__c= 'abc';
        pb.Country_Arabic__c= 'abc';
        insert PB;
        
        Buyer__c JB= new Buyer__c();
        JB.Primary_Buyer__c=false;
        JB.Buyer_type__c='Individual';
        JB.Booking__c=bk.id;
        JB.Passport_Expiry_Date__c='25/11/2017';
        JB.CR_Registration_Expiry_Date__c='25/06/2017';
        jb.status__c='New';
        jb.Date_of_Birth__c='25/12/1990';
        jb.City__c='Dubai';
        jb.Country__c='United Arab Emirates';
        jb.Address_Line_1__c='street1';
        jb.Email__c='test@test.com';
        jb.First_Name__c='Buyer';
        jb.Last_Name__c='test';
        jb.Nationality__c='Indian'; 
        jb.Passport_Number__c='PP123'; 
        jb.Phone__c='53532255';
        jb.Phone_Country_Code__c='India: 0091';
        jb.Place_of_Issue__c='Delhi'; 
        jb.Title__c='Mr.';
        jb.Title_Arabic__c = 'Mr';
        jb.First_Name_Arabic__c = 'rtyui';
        jb.Last_Name_Arabic__c = 'tyyt';
        jb.Nationality_Arabic__c = 'ABC';
        jb.Passport_Number__c = '456879';
        jb.CR_Number__c = '123';
        jb.Place_of_Issue_Arabic__c = 'ad';
        jb.Address_Line_1_Arabic__c = 'abc';
        jb.Address_Line_2_Arabic__c = 'abc';
        jb.Address_Line_3_Arabic__c = 'abc';
        jb.Address_Line_4_Arabic__c = 'abc';
        jb.City_Arabic__c= 'abc';
        jb.Country_Arabic__c= 'abc';
        jb.Account__c=acc.Id;
        insert JB;
        
        Booking_Unit__c BU= new Booking_Unit__c ();
        BU.Booking__c=bk.id;
        BU.Inventory__c=inv.id;
        insert BU;
        
        Payment_Plan__c ppp = new Payment_Plan__c();
        ppp.TERM_ID__c='100';
        insert ppp;
        
        Payment_Plan__c pp = new Payment_Plan__c();
        pp.Parent_Payment_Plan__c=ppp.id;
        pp.Booking_Unit__c=BU.id;
        insert pp;
        
        Payment_Terms__c PT = new Payment_Terms__c();
        PT.Payment_Plan__c=pp.id;
        insert PT;
        
        List<id>recordids= new List<id>();
        recordids.add(bk.id);
        
        List<id>BUids= new List<id>();
        BUids.add(BU.id);
        
        Receipt__c rec= new Receipt__c();
        rec.Amount__c=100;
        rec.Booking_Unit__c=BU.id;
        insert rec;
        
        List<id>recids= new List<id>();
        recids.add(rec.id);
        
         List<id>bookingrecordids= new List<id>();
         bookingrecordids.add(bk.id);
        
        string dealrecordtypeid = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
    
        NSIBPM__Service_Request__c SR1 =   new NSIBPM__Service_Request__c();
        SR1.Delivery_mode__c='Email';
        
        SR1.Deal_ID__c='1001';
        SR1.recordtypeid = dealrecordtypeid;
        insert SR1;
        
        Booking__c bk1= new Booking__c();
        bk1.Deal_SR__c=SR1.id;
        bk1.Booking_channel__c='Office';
        insert bk1;
        
        Booking_Unit__c BU1= new Booking_Unit__c ();
        BU1.Booking__c=bk1.id;
        BU1.Inventory__c=inv.id; 
        BU1.Registration_ID__c = '56789';   
        insert BU1;
        
        proof_of_payment__c pop = new proof_of_payment__c();
        pop.deal_sr__c = sr1.id;
        pop.booking_unit__c = bu1.id;
        pop.payment_amount__c = 1432332;
        pop.payment_mode__c = 'Cash';
        pop.payment_remarks__c = 'AED';
        pop.currency__c = 'AED';
        pop.payment_date__c = system.today();
        insert pop;
        
        List<id>poprecids= new List<id>();
        poprecids.add(pop.id);
        
        attachment a = new attachment();
        a.parentid = pop.id;
        a.name = 'Test';
        a.body = blob.valueof('javsdsajdgajg');
        a.ContentType='image/png';
        DAMAC_Constants.Skip_AttachmentTrigger_SMS = true;
        insert a;
        
        update a;
        
        
        Announcement_Request__c ar = new Announcement_Request__c();
        insert ar;
        
        attachment a1 = new attachment();
        a1.parentid = ar.id;
        a1.name = 'Test';
        a1.body = blob.valueof('javsdsajdgajg');
        a1.ContentType='image/png';
        DAMAC_Constants.Skip_AttachmentTrigger_SMS = true;
        insert a1;
        
        
        
        
        list<id> attachIds = new list<id>();
        attachIds.add(a.id);
        
        NSIBPM__Document_Master__c DM = new NSIBPM__Document_Master__c();
        DM.NSIBPM__Code__c='SOA';
        insert DM;
        
        NSIBPM__Document_Master__c DM1 = new NSIBPM__Document_Master__c();
        DM1.NSIBPM__Code__c='SPA';
        insert DM1;
        
        NSIBPM__Document_Master__c DM2 = new NSIBPM__Document_Master__c();
        DM2.NSIBPM__Code__c='DP-INVOICE';
        insert DM2;
        
        NSIBPM__SR_Status__c st= new NSIBPM__SR_Status__c();
        st.NSIBPM__Code__c='SUBMITTED';
        insert st;
        
        NSIBPM__SR_Status__c st1= new NSIBPM__SR_Status__c();
        st1.NSIBPM__Code__c='AGREEMENT_GENERATED';
        insert st1;
        
        
        
        IPMS_Requests.prepareRequestBody(poprecids);
        IPMS_Requests.sendPoPSR(poprecids);
        body= SetResponse(pop.id);
        IPMS_Requests.parsePOPSRResponse(body);
        
        IPMS_Requests.preparePOPSRAttachDoc(attachids);
        IPMS_Requests.sendPoPAttachDoc(attachids);
        body= SetResponse(a.id);
        IPMS_Requests.parsepopSRAttachDocResponse(body);
        
        
        //
        
        IPMS_Requests.preparePartyUpdateArabic(bookingrecordids);
        IPMS_Requests.sendPoPAttachDoc(bookingrecordids);
        body= SetResponse(bk.id);
        IPMS_Requests.parsepopSRAttachDocResponse(body);
        
        test.stoptest();
    
    
    
    }
    
    static testmethod void testReSubmitReqs(){
         string body = '';
        Date d = Date.newinstance(25,12,2021);
        
        Location__c loc=new Location__c();
        loc.Location_ID__c='123';
        insert loc;
        Inventory__c inv = new Inventory__c();
        inv.Unit_Location__c=loc.id;
        insert inv;
        
        NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
        SR.Delivery_mode__c='Email';
        SR.Deal_ID__c='1001';
        insert SR;
        
        List<id> SRids = new List<id>();
        SRids.add(SR.id);
        NSIBPM__SR_Doc__c srdoc= new NSIBPM__SR_Doc__c();
        srdoc.NSIBPM__Service_Request__c=SR.id;
        insert srdoc;
        
        Booking__c bk= new Booking__c();
        bk.Deal_SR__c=SR.id;
        bk.Booking_channel__c='Office';
        insert bk;
        
        Account acc=new Account(Name='Testing Corp Buyer');
        insert acc;
               
        Buyer__c PB= new Buyer__c();
        PB.Primary_Buyer__c=true;
        PB.Account__c=acc.Id;
        PB.Buyer_Type__c='Individual';
        PB.Booking__c=bk.id;
        PB.Phone_Country_Code__c='India: 0091';
        PB.Passport_Expiry_Date__c='25/03/2017';
        PB.CR_Registration_Expiry_Date__c='25/12/2017';
        PB.City__c='Dubai';
        PB.Country__c='United Arab Emirates';
        PB.Address_Line_1__c='street1';
        PB.Address_Changed__c=true;
        PB.Date_of_Birth__c='25/12/1990';
        PB.Email__c='test@test.com';
        PB.First_Name__c='Buyer';
        PB.Last_Name__c='test';
        PB.Nationality__c='Indian'; 
        PB.Passport_Number__c='PP123'; 
        PB.Phone__c='53532255';
        PB.Place_of_Issue__c='Delhi'; 
        PB.Title__c='Mr.';
        PB.party_id__c = '12344';
        pb.Title_Arabic__c = 'Mr';
        pb.First_Name_Arabic__c = 'tyui';
        pb.Last_Name_Arabic__c = 'tyyt';
        pb.Nationality_Arabic__c = 'ABC';
        pb.Passport_Number__c = '456879';
        pb.CR_Number__c = '123';
        pb.Place_of_Issue_Arabic__c = 'ad';
        pb.Address_Line_1_Arabic__c = 'abc';
        pb.Address_Line_2_Arabic__c = 'abc';
        pb.Address_Line_3_Arabic__c = 'abc';
        pb.Address_Line_4_Arabic__c = 'abc';
        pb.City_Arabic__c= 'abc';
        pb.Country_Arabic__c= 'abc';
        insert PB;
        
        Buyer__c JB= new Buyer__c();
        JB.Primary_Buyer__c=false;
        JB.Buyer_type__c='Individual';
        JB.Booking__c=bk.id;
        JB.Passport_Expiry_Date__c='25/11/2017';
        JB.CR_Registration_Expiry_Date__c='25/06/2017';
        jb.status__c='New';
        jb.Date_of_Birth__c='25/12/1990';
        jb.City__c='Dubai';
        jb.Country__c='United Arab Emirates';
        jb.Address_Line_1__c='street1';
        jb.Email__c='test@test.com';
        jb.First_Name__c='Buyer';
        jb.Last_Name__c='test';
        jb.Nationality__c='Indian'; 
        jb.Passport_Number__c='PP123'; 
        jb.Phone__c='53532255';
        jb.Phone_Country_Code__c='India: 0091';
        jb.Place_of_Issue__c='Delhi'; 
        jb.Title__c='Mr.';
        jb.Title_Arabic__c = 'Mr';
        jb.First_Name_Arabic__c = 'rtyui';
        jb.Last_Name_Arabic__c = 'tyyt';
        jb.Nationality_Arabic__c = 'ABC';
        jb.Passport_Number__c = '456879';
        jb.CR_Number__c = '123';
        jb.Place_of_Issue_Arabic__c = 'ad';
        jb.Address_Line_1_Arabic__c = 'abc';
        jb.Address_Line_2_Arabic__c = 'abc';
        jb.Address_Line_3_Arabic__c = 'abc';
        jb.Address_Line_4_Arabic__c = 'abc';
        jb.City_Arabic__c= 'abc';
        jb.Country_Arabic__c= 'abc';
        jb.Account__c=acc.Id;
        insert JB;
        
        Booking_Unit__c BU= new Booking_Unit__c ();
        BU.Booking__c=bk.id;
        BU.Inventory__c=inv.id;
        insert BU;
        
        Payment_Plan__c ppp = new Payment_Plan__c();
        ppp.TERM_ID__c='100';
        insert ppp;
        
        Payment_Plan__c pp = new Payment_Plan__c();
        pp.Parent_Payment_Plan__c=ppp.id;
        pp.Booking_Unit__c=BU.id;
        insert pp;
        
        Payment_Terms__c PT = new Payment_Terms__c();
        PT.Payment_Plan__c=pp.id;
        insert PT;
        
        List<id>recordids= new List<id>();
        recordids.add(bk.id);
        
        List<id>BUids= new List<id>();
        BUids.add(BU.id);
        
        Receipt__c rec= new Receipt__c();
        rec.Amount__c=100;
        rec.Booking_Unit__c=BU.id;
        insert rec;
        
        List<id>recids= new List<id>();
        recids.add(rec.id);
        
         List<id>bookingrecordids= new List<id>();
         bookingrecordids.add(bk.id);
        
        string dealrecordtypeid = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
    
        NSIBPM__Service_Request__c SR1 =   new NSIBPM__Service_Request__c();
        SR1.Delivery_mode__c='Email';
        
        SR1.Deal_ID__c='1001';
        SR1.recordtypeid = dealrecordtypeid;
        insert SR1;
        
        Booking__c bk1= new Booking__c();
        bk1.Deal_SR__c=SR1.id;
        bk1.Booking_channel__c='Office';
        insert bk1;
        
        Booking_Unit__c BU1= new Booking_Unit__c ();
        BU1.Booking__c=bk1.id;
        BU1.Inventory__c=inv.id; 
        BU1.Registration_ID__c = '56789';   
        insert BU1;
        
        proof_of_payment__c pop = new proof_of_payment__c();
        pop.deal_sr__c = sr1.id;
        pop.booking_unit__c = bu1.id;
        pop.payment_amount__c = 1432332;
        pop.payment_mode__c = 'Cash';
        pop.payment_remarks__c = 'AED';
        pop.currency__c = 'AED';
        pop.payment_date__c = system.today();
        insert pop;
        test.startTest();
             ReSubmit_IPMS_Requests.Retry_POP_SR(pop.id);
        test.stopTest();
        
    }
    
     private static string SetResponse(String idval){
        string body = '';
        body+='<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">';
        body+='<env:Header/>';
        body+='<env:Body>';
          body+='<OutputParameters xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://xmlns.oracle.com/apps/ont/soaprovider/plsql/xxdc_process_service_ws/process/">';
             body+='<X_RESPONSE_MESSAGE>';
                body+='<X_RESPONSE_MESSAGE_ITEM>';
                   body+='<PARAM_ID>'+idval+'</PARAM_ID>';
                   body+='<PROC_STATUS>S</PROC_STATUS>';
                   body+='<PROC_MESSAGE>Process Completed, SR# :1-1986612946 Created..</PROC_MESSAGE>';
                   body+='<ATTRIBUTE1>810117</ATTRIBUTE1>';
                   body+='<ATTRIBUTE2>1274778</ATTRIBUTE2>';
                   body+='<ATTRIBUTE3>123</ATTRIBUTE3>';
                   body+='<ATTRIBUTE4 xsi:nil="true"/>';
                body+='</X_RESPONSE_MESSAGE_ITEM>';
             body+='</X_RESPONSE_MESSAGE>';
             body+='<X_RETURN_STATUS>S</X_RETURN_STATUS>';
             body+='<X_RETURN_MESSAGE>Process Completed successfully...</X_RETURN_MESSAGE>';
          body+='</OutputParameters>';
        body+='</env:Body>';
        body+='</env:Envelope>';
        body=body.trim();
        body= body.replaceAll('null', '');
        body=body.trim();
        
        return body;
    }
    private static string SetDocResponse(String idval,String stype){
        string body = '';
        body+='<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">';
        body+='<env:Header/>';
        body+='<env:Body>';
          body+='<OutputParameters xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://xmlns.oracle.com/apps/ont/soaprovider/plsql/xxdc_process_service_ws/process/">';
             body+='<X_RESPONSE_MESSAGE>';
                body+='<X_RESPONSE_MESSAGE_ITEM>';
                   body+='<PARAM_ID>'+idval+'</PARAM_ID>';
                   body+='<PROC_STATUS>S</PROC_STATUS>';
                   body+='<PROC_MESSAGE>Bank :SFDC Test Bank One Created for Vendor with Id:810117</PROC_MESSAGE>';
                   body+='<ATTRIBUTE1>810117</ATTRIBUTE1>';
                   body+='<ATTRIBUTE2>1274778</ATTRIBUTE2>';
                   body+='<ATTRIBUTE3>'+stype+'</ATTRIBUTE3>';
                   body+='<ATTRIBUTE4 xsi:nil="true"/>';
                body+='</X_RESPONSE_MESSAGE_ITEM>';
             body+='</X_RESPONSE_MESSAGE>';
             body+='<X_RETURN_STATUS>S</X_RETURN_STATUS>';
             body+='<X_RETURN_MESSAGE>Process Completed successfully...</X_RETURN_MESSAGE>';
          body+='</OutputParameters>';
        body+='</env:Body>';
        body+='</env:Envelope>';
        body=body.trim();
        body= body.replaceAll('null', '');
        body=body.trim();
        
        return body;
    }

}