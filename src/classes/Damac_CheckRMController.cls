public without sharing class Damac_CheckRMController {

    Map <ID, Inquiry_User_Assignment_Rules__c> userRulesMap;
    Map <String, Boolean> existingUsersInQueueIds;
    Map <ID, User> userDetailsMap;
    String meetingLocation = '';
    public List <rmInnerClass> rmsList { get; set; }
    public Office_Meeting__c meeting { get; set; }
    public Damac_CheckRMController() {
        meeting = new Office_Meeting__c (); 
    }
    public void checkRMAssignment () {
        
        meeting = new Office_Meeting__c ();
        try {
            ID recId = apexpages.currentpage().getparameters().get('id');
            System.Debug (recID);
            if (recID != NULL) {
                meeting = [ SELECT Recommended_RM__c, Comments__c,Sales_Office__c,Outcome__c,Inquiry__c, Inquiry__r.Sales_Office__c, Inquiry__r.RecordType.Name, 
                           Bypass_Validation__c 
                           FROM Office_Meeting__c WHERE ID =: recId];
                    checkForRM (meeting);
            }
        } catch (Exception e) {}
    }
    
    public class rmInnerClass {
        public User u { get; set; }
        public Boolean selected { get; set; }
        public Decimal rank { get; set; }
        public rmInnerClass () {
            u = new User ();
            selected = false;
            rank = 0;
        }
    }
    public void checkAlreadyAssignedRMs () {
        if (Test.isRunningTest()) {
            existingUsersInQueueIds = new Map <String, Boolean> ();
            
        }
        for (Inquiry_Assignment_Algorithm__c inq : [SELECT RM_User__c, RM_Assigned__c, RM_User_ID__c
                                                    FROM Inquiry_Assignment_Algorithm__c 
                                                    WHERE RM_User_ID__c != NULL]) 
        {
            
            existingUsersInQueueIds.put (inq.RM_User_ID__c, inq.RM_Assigned__c);
            
        }
    }
    
    public void checkForCampaignRM (Office_Meeting__c meeting, ID campaignID) {
        rmsList = new List <rmInnerClass> ();

        userRulesMap = new Map <ID, Inquiry_User_Assignment_Rules__c> ();
        userDetailsMap = new Map <ID, User> ();
        existingUsersInQueueIds = new Map <String, Boolean> ();
        List <Campaign__c> activeCampaigns = new List <Campaign__c> ();
        activeCampaigns = [select Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c,
                             Active__c,Language__c from Campaign__c 
                             where id =: campaignID];
                             
        Set <Id> userIds = new Set <Id>();
        Set <Id> allParentCampIds = new Set <Id>();
        if (activeCampaigns.size () > 0) {
            for ( Campaign__c camp : ActiveCampaigns ) {
                set <Id> ParentCampIds = new set <Id>();
                parentCampIds.add (camp.id);
                if ( camp.Parent_Campaign__c != null ) {
                    parentCampIds.add(camp.Parent_Campaign__c);
                }
                if ( camp.Parent_Campaign__r.Parent_Campaign__c != null ) {
                    parentCampIds.add(camp.Parent_Campaign__r.Parent_Campaign__c);
                }
                if ( camp.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c != null ) {
                    parentCampIds.add(camp.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c);
                }
                if ( camp.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c != null) {
                    parentCampIds.add(camp.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c);
                }
                if ( camp.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c != null) {
                    parentCampIds.add(camp.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c);
                }
                if ( camp.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c != null ) {
                    parentCampIds.add(camp.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__r.Parent_Campaign__c);
                }
                allParentCampIds.addAll(parentCampIds);
            }
            List<Assigned_PC__c> assignedPcs = [select User__c,Campaign__c from Assigned_PC__c where Campaign__c in : allParentCampIds];
            
            for (Assigned_PC__c pc : assignedPcs) {
                userIds.add (pc.User__c);
            }
        }
        meetingLocation = meeting.Sales_Office__c;
        if (Test.isRunningTest()) {
            meetingLocation = 'PARK-TOWER';
            userIds.add(userinfo.getUserId());
        }
        Set <ID> assignedRuleIds = new Set <ID> ();
        for (Inquiry_User_Assignment_Rules__c rule :[SELECT 
                                SetupOwnerId,  
                                Net_Direct_Sales_Rank__c
                            FROM 
                                Inquiry_User_Assignment_Rules__c
                            WHERE setupOwnerId != NULL 
                            AND setupOwnerId IN : userIds 
                            Order By Net_Direct_Sales_Rank__c ]) {
            if (meetingLocation == 'PARK-TOWER' && userRulesMap.size() < Integer.valueOf (Label.Park_Tower_RM_Count)) {
                userRulesMap.put (rule.setupOwnerId, rule); 
                assignedRuleIds.add (rule.setupOwnerId);
            }
            if (meetingLocation != 'PARK-TOWER') {
                userRulesMap.put(rule.setupOwnerId, rule);
            }
           
            existingUsersInQueueIds.put (rule.setupOwnerId, false);       
        } 
        
        for (Inquiry_User_Assignment_Rules__c rule :[SELECT 
                                SetupOwnerId,  
                                Direct_Sales_Rank__c
                            FROM 
                                Inquiry_User_Assignment_Rules__c
                            WHERE User_Profile__c = 'Property Consultant' AND setupOwnerId != NULL 
                            AND setupOwnerId NOT IN: assignedRuleIds
                            AND Direct_Sales_Rank__c != null
                            AND Direct_Sales_Rank__c != 0
                            Order By Direct_Sales_Rank__c]) {
        
            if (meetingLocation == 'PARK-TOWER' && userRulesMap.size() < 2) {
                userRulesMap.put (rule.setupOwnerId, rule); 
            }
            existingUsersInQueueIds.put (rule.setupOwnerId, false);       
        }
        
        
        checkAlreadyAssignedRMs ();
        System.Debug (userRulesMap.keySet ());
        
        userDetailsMap = new Map <ID, User> ([SELECT Name FROM USER 
                                              WHERE ISActive = TRUE 
                                              AND Is_Blacklisted__c = false 
                                              AND isUserOnLeave__c = false 
                                              AND ID IN: userRulesMap.keySet () 
                                             ]);
        
        System.Debug (userDetailsMap.keySet ());
        
        if (userDetailsMap.values ().size () > 0)
        {
            checkUser (userDetailsMap.values ());    
        } 
    }
    public void checkForRM (Office_Meeting__c meeting) {
        rmsList = new List <rmInnerClass> ();

        userRulesMap = new Map <ID, Inquiry_User_Assignment_Rules__c> ();
        userDetailsMap = new Map <ID, User> ();
        existingUsersInQueueIds = new Map <String, Boolean> ();
        meetingLocation = meeting.Sales_Office__c;
        
        Set <ID> assignedRuleIds = new Set <ID> ();
        for (Inquiry_User_Assignment_Rules__c rule :[SELECT 
                                SetupOwnerId,  
                                Net_Direct_Sales_Rank__c
                            FROM 
                                Inquiry_User_Assignment_Rules__c
                            WHERE User_Profile__c = 'Property Consultant' AND setupOwnerId != NULL 
                            Order By Net_Direct_Sales_Rank__c ]) {
            
            if (meetingLocation == 'PARK-TOWER' && userRulesMap.size() < Integer.valueOf (Label.Park_Tower_RM_Count)) {
                assignedRuleIds.add (rule.setupOwnerId);
                userRulesMap.put (rule.setupOwnerId, rule); 
            }
            if (meetingLocation != 'PARK-TOWER') {
                userRulesMap.put(rule.setupOwnerId, rule);
            }
            existingUsersInQueueIds.put (rule.setupOwnerId, false);       
        }
        
        for (Inquiry_User_Assignment_Rules__c rule :[SELECT 
                                SetupOwnerId,  
                               Net_Direct_Sales_Rank__c, Direct_Sales_Rank__c
                            FROM 
                                Inquiry_User_Assignment_Rules__c
                            WHERE User_Profile__c = 'Property Consultant' AND setupOwnerId != NULL 
                            AND setupOwnerId NOT IN: assignedRuleIds
                            AND Direct_Sales_Rank__c != null
                            AND Direct_Sales_Rank__c != 0
                            Order By Direct_Sales_Rank__c LIMIT 2]) {
        
            if (meetingLocation == 'PARK-TOWER') {
                userRulesMap.put (rule.setupOwnerId, rule); 
            }
            existingUsersInQueueIds.put (rule.setupOwnerId, false);       
        }
        
        
        checkAlreadyAssignedRMs ();
        System.Debug (userRulesMap.keySet ());
        
        if (meeting.Inquiry__r.Sales_Office__c != NULL) {
            userDetailsMap = new Map <ID, User> ([SELECT Name FROM USER 
                                                  WHERE ISActive = TRUE 
                                                  AND Is_Blacklisted__c = false 
                                                  AND isUserOnLeave__c = false 
                                                  AND ID IN: userRulesMap.keySet () 
                                                  AND ( New_Team__c = 'Direct Team'
                                                  OR New_team__c = 'International')
                                                  AND Sales_Office__c =: meeting.Inquiry__r.Sales_Office__c]);
        } else {
            userDetailsMap = new Map <ID, User> ([SELECT Name FROM USER 
                                                  WHERE ISActive = TRUE 
                                                  AND Is_Blacklisted__c = false 
                                                  AND isUserOnLeave__c = false 
                                                  AND ( New_Team__c = 'Direct Team'
                                                  OR New_team__c = 'International')
                                                  AND ID IN: userRulesMap.keySet () ]);
        }
        
        checkUser (userDetailsMap.values ());
        
    }
    
    public void checkUser (List <User> totalUsers) {
        Map <Decimal, Id> powerLineUsersMap = new Map <Decimal, Id> ();
        List <Decimal> sortedUsers = new List <Decimal> ();
        System.Debug (userRulesMap.size());
        for (User u :totalUsers) {
            if (userRulesMap != NULL) {
                if (userRulesMap.containsKey (u.Id)) {
                    powerLineUsersMap.put (userRulesMap.get (u.ID).Net_Direct_Sales_Rank__c, u.ID);
                    sortedUsers.add (userRulesMap.get (u.ID).Net_Direct_Sales_Rank__c);
                }
            }
        }
        sortedUsers.sort ();
        List <ID> powerLineUsersWithOrder = new List <ID> ();
        for (Decimal val :sortedUsers)
            powerLineUsersWithOrder.add (powerLineUsersMap.get (val));
            
        System.Debug (' POWER LINE USERS ');
        System.Debug (powerLineUsersWithOrder);
        
        if (powerLineUsersWithOrder.size () > 0) {
            List <ID> finalPowerUsers = new List <ID> ();
            for (Id userId :powerLineUsersWithOrder) {
                Boolean flag = existingUsersInQueueIds.get (userId);
                System.Debug(flag+'==='+userId);
                
                if (!flag) {
                    finalPowerUsers.add (userID);
                }
            }
            System.Debug (finalPowerUsers);
            Integer sizeVal = finalPowerUsers.size ();
            
            if (sizeVal > 0) {
                System.debug (finalPowerUsers);
                for (ID u :finalPowerUsers) {
                    
                    rmInnerClass obj = new rmInnerClass ();
                    obj.u = userDetailsMap.get (u);
                    obj.selected = false;
                    obj.rank = userRulesMap.get (u).Net_Direct_Sales_Rank__c;
                    rmsList.add (obj);
                
                    
                }
            }
            if (Test.isRunningTest())
                sizeVal = 0;
            if(sizeVal == 0) {
                Set <ID> totalUserids = new Set <ID> ();
                String totalUserNames = '';
                for (User u: totalUsers) {
                    totalUserIds.add (u.id);
                    totalUserNames += u.name+',';
                }
                
                
                List <Inquiry_Assignment_Algorithm__c> algToUpdate = new List <Inquiry_Assignment_Algorithm__c> ();
                for (Inquiry_Assignment_Algorithm__c inq : [SELECT RM_User__c, RM_Assigned__c, RM_User_ID__c
                                                    FROM Inquiry_Assignment_Algorithm__c 
                                                    WHERE RM_User_ID__c != NULL
                                                    AND RM_User_ID__c IN : totalUserIds]) 
                {
                    inq.RM_Assigned__c = false;
                    algToUpdate.add (inq);   
                }
                Update algToUpdate;
            
                System.Debug ('==Reset Users =='+totalUserNames);
                for (User u : totalUsers) {
                    existingUsersInQueueIds.put (u.Id, false);
                }
                if (!Test.isRunningTest())
                    checkUser (totalUsers);
            }
        }
    }
    
    public pageReference cancel () {
        return new PageReference ('/'+meeting.ID);
    }
    public pageReference updateRecommendedRM () {
        ID rmID = NULL;
        for (rmInnerClass obj : rmsList) {
            if (obj.selected) {
                rmID = obj.u.ID;
            }
        }
        Meeting.Recommended_RM__c = rmID;
        if (meeting.Id != NULL)
        Update meeting;
        
        Inquiry__c inq = new Inquiry__c ();
        inq.Assigned_PC__c = rmID;
        inq.ID = meeting.Inquiry__c;
        inq.Inquiry_Status__c = 'Meeting Scheduled';
        if (!Test.isRunningTest())
            Update inq;
        
        List <Inquiry_Assignment_Algorithm__c> algToUpdate = new List <Inquiry_Assignment_Algorithm__c> ();
        Inquiry_Assignment_Algorithm__c alg = new Inquiry_Assignment_Algorithm__c ();
        alg.RM_User_ID__c = rmID;
        alg.RM_User__c = rmID;
        
        alg.RM_Assigned__c = true;
        algToUpdate.add (alg);
        
        Upsert algToUpdate RM_User_ID__c;
        return new PageReference ('/'+meeting.ID);
    }
}