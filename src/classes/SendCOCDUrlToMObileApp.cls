@RestResource(urlMapping='/SendCOCDUrlToMObileApp/*')
 Global class SendCOCDUrlToMObileApp
 {
     @HtTPPost
    Global static String SendCOCDUrlToMObileApps(BuyersInfoWrapper buyersInfoWrapper)
    {
        String cocdFormURL;
        Account accObj=[select id,name,IsPersonAccount,Passport_Expiry_Date__pc ,CR_Registration_Expiry_Date__c,Party_ID__c,Address_Line_1__pc,
        Address_Line_1__c,Address_Line_2__pc,Address_Line_2__c,Address_Line_3__pc,Address_Line_3__c,Address_Line_4__pc,Address_Line_4__c,City__pc,City__c,
        State__c,BillingPostalCode,Country__pc,Country__c,Mobile_Country_Code__pc,Mobile_Country_Code__c,Mobile_Phone_Encrypt__pc,
        Home_Phone_Country_Code__c,Home_Phone_Encrypt__c,Email__pc,Email__c,Passport_Number__pc,CR_Number__c from Account where id=:buyersInfoWrapper.AccountID];
        Boolean personAccount = accObj.IsPersonAccount;
        //Adding date format for Passport Issue Date (Expiry Date) change 115/12/2017: AJ
        Date d = personAccount ? accObj.Passport_Expiry_Date__pc : accObj.CR_Registration_Expiry_Date__c;

        String P_REQUEST_NUMBER = String.valueOf( datetime.now().getTime() );
        String P_REQUEST_NAME = 'COCD_LETTER';
        String P_SOURCE_SYSTEM = 'SFDC';
        String IPMSPartyID = accObj.Party_ID__c;
        String Address1 = buyersInfoWrapper.address1;
        String Address2 = buyersInfoWrapper.address2;
        String Address3 = buyersInfoWrapper.address3;
        String Address4 = buyersInfoWrapper.address4;
        String city = buyersInfoWrapper.city;
        String state =buyersInfoWrapper.state;
        String postalCode = buyersInfoWrapper.postalCode;
        String country =buyersInfoWrapper.country;
        String MobileCountryCode = buyersInfoWrapper.mobileCountryCode;
        String MobileAreaCode = '';
        //String MobileNumber = personAccount ? accObj.Mobile_Phone_Encrypt__pc : accObj.Mobile__c;
         String MobileNumber =buyersInfoWrapper.phone;
        String PhoneCountryCode = String.isNotBlank(  accObj.Home_Phone_Country_Code__c)
                                    ? accObj.Home_Phone_Country_Code__c.split(':')[1].removeStart(' 00')
                                    : '';
        String PhoneAreaCode = '';
        String PhoneNumber = accObj.Home_Phone_Encrypt__c;
        String FaxCountryCode = '';
        String FaxAreaCode = '';
        String FaxNumber = '';
        String EmailAddress =buyersInfoWrapper.email;
        //String PassportIssueDate = personAccount ? String.valueOf(accObj.Passport_Expiry_Date__pc) : String.valueOf(accObj.CR_Registration_Expiry_Date__c);
        //Date Format Change 16/12/2017 AJ
        String PassportIssueDate = ( d != null ) ? DateTime.newInstance(d.year(),d.month(),d.day()).format('dd-MMM-YYYY') : '';

        String PassportNumnber = personAccount ? accObj.Passport_Number__pc : accObj.CR_Number__c;

       /* if(String.isNotBlank(PassportIssueDate))
        {
            PassportIssueDate = AOPTUtility.formatDate(PassportIssueDate);
        }
        else
        {
            PassportIssueDate = '';
        }
        */
      if(!test.isRunningTest())
      {
        actionCom.COCDHttpSoap11Endpoint cocdFormCalloutObj = new actionCom.COCDHttpSoap11Endpoint();
        cocdFormCalloutObj.timeout_x = 120000;
        cocdFormURL = cocdFormCalloutObj.generateCOCD(P_REQUEST_NUMBER, P_REQUEST_NAME, P_SOURCE_SYSTEM, IPMSPartyID, Address1, Address2, Address3,
            Address4, city, state, postalCode, country, MobileCountryCode, MobileAreaCode, MobileNumber, PhoneCountryCode, PhoneAreaCode, PhoneNumber,
            FaxCountryCode, FaxAreaCode, FaxNumber, EmailAddress, PassportIssueDate, PassportNumnber);

        System.debug('===cocdFormURL=== '+cocdFormURL);
        cocdFormURL = ( cocdFormURL.indexOf('~') != -1 && !cocdFormURL.split('~')[0].equalsIgnoreCase('s') ) ? 'No URL found from IPMS' : cocdFormURL;

        System.debug('=1111==cocdFormURL=== ' + cocdFormUrl);
      }
        return cocdFormURL ;
    }
    
    global class BuyersInfoWrapper
    {
        public String AccountID;
        public String country;
        public String city;
        public String phone;
        public String state;
        public String postalCode;
        public String email;
        public String mobileCountryCode;
        public String address1;
         public String address2;
        public String address3;
        public String address4;
    }
 }