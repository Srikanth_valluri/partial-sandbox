/**
 * @File Name          : BookingUnitEmailUnitSoaPdfController.cls
 * @Description        :
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              :
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 9/2/2019, 3:52:59 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    9/2/2019, 3:49:21 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public class BookingUnitEmailUnitSoaPdfController {

    public Id recordId {get; set;}
    public String responseUrl {get; set;}
    public String accessToken {get; set;}
    public String pdfFile {get; set;}
    public Blob pdfBlob {get; set;}
    public String emailId {get; set;}
    public String registrationId;
    public String accountId;
    public String str;
    public String strUnitName;


    public BookingUnitEmailUnitSoaPdfController(ApexPages.StandardController controller) {
        recordId = controller.getId();
        System.debug('Booking Unit Id in consturctor: '+recordId);
    }

    public PageReference sendUnitSoaPdfEmailCallingList() {

        Calling_List__c callLst = [SELECT id,
                                          Party_ID__c,
                                          Registration_ID__c,
                                          Account__r.Party_ID__c,
                                          Booking_Unit__r.Registration_ID__c,
                                          Account__c,
                                          Account__r.Name,
                                          Account__r.RecordType.Name,
                                          Account__r.Email__c,
                                          Account__r.Email__pc,
                                          Booking_Unit__r.Unit_Name__c,
                                          Booking_Unit_Name__c
                                    FROM Calling_List__c
                                    WHERE Id =: recordId];

        registrationId = callLst.Registration_ID__c;
        if(registrationId == '' || registrationId == Null) {
            registrationId = callLst.Booking_Unit__r.Registration_ID__c;
        }
        accountId = callLst.Account__c;
        System.debug('registrationId is: '+registrationId);
        System.debug('Account ID is: '+accountId);

        //Account account = [SELECT id, Name, RecordType.Name, Email__c, Email__pc
        //                   FROM Account
        //                   WHERE Id =: accountId];

        //Fetch Email field value as per the Type of Account i.e. Business/Person Account
        if(callLst.Account__r.RecordType.Name == 'Business Account') {
            System.debug('Its Business Account');
             emailId = callLst.Account__r.Email__c;
        }
        else if(callLst.Account__r.RecordType.Name == 'Person Account') {
            System.debug('Its Person Account');
             emailId = callLst.Account__r.Email__pc;
        }

        System.debug('Email Id of Account: '+emailId);
        str = callLst.Account__r.Name;
        strUnitName = callLst.Booking_Unit_Name__c;
        PageReference pg = sendUnitSoaPdfEmail();
        return null;

    }

    public PageReference sendUnitSoaPdfEmailBookingUnit() {
        Booking_unit__c bookingUnit = [SELECT Id, Booking__r.Account__c, Booking__r.Account__r.Name , Booking__r.Account__r.RecordType.Name, Booking__r.Account__r.Email__c, Booking__r.Account__r.Email__pc, Unit_Name__c , Registration_ID__c
                                   FROM Booking_Unit__c
                                   WHERE Id =: recordId];
        System.debug('Account is: '+bookingUnit.Booking__r.Account__c);

        registrationId = bookingUnit.Registration_ID__c;
        accountId = bookingUnit.Booking__r.Account__c;

        System.debug('registrationId is: '+registrationId);
        System.debug('Account ID is: '+accountId);

        //Account account = [SELECT id, Name, RecordType.Name, Email__c, Email__pc
        //                   FROM Account
        //                   WHERE Id =: accountId];

        //Fetch Email field value as per the Type of Account i.e. Business/Person Account
        if(bookingUnit.Booking__r.Account__r.RecordType.Name == 'Business Account') {
            System.debug('Its Business Account');
            emailId = bookingUnit.Booking__r.Account__r.Email__c;
        }
        else if(bookingUnit.Booking__r.Account__r.RecordType.Name == 'Person Account') {
            System.debug('Its Person Account');
            emailId = bookingUnit.Booking__r.Account__r.Email__pc;
        }

        System.debug('Email Id of Account: '+emailId);
        str = bookingUnit.Booking__r.Account__r.Name;
        strUnitName = bookingUnit.Unit_Name__c;
        PageReference pg = sendUnitSoaPdfEmail();
        return null;

    }

    public PageReference sendUnitSoaPdfEmail() {
        FmIpmsRestCoffeeServices.updateCustomSetting = false;
        Booking_Unit__c bookingUnit;
        /*if(registrationId == NULL || String.isBlank(registrationId)){
            bookingUnit = [SELECT Id, Booking__r.Account__c, Booking__r.Account__r.Name , Booking__r.Account__r.RecordType.Name, Booking__r.Account__r.Email__c, Booking__r.Account__r.Email__pc, Unit_Name__c , Registration_ID__c
                                       FROM Booking_Unit__c
                                       WHERE Id =: recordId];
            System.debug('Account is: '+bookingUnit.Booking__r.Account__c);

            registrationId = bookingUnit.Registration_ID__c;
            accountId = bookingUnit.Booking__r.Account__c;

            System.debug('registrationId is: '+registrationId);
            System.debug('Account ID is: '+accountId);

            //Account account = [SELECT id, Name, RecordType.Name, Email__c, Email__pc
            //                   FROM Account
            //                   WHERE Id =: accountId];

            //Fetch Email field value as per the Type of Account i.e. Business/Person Account
            if(bookingUnit.Booking__r.Account__r.RecordType.Name == 'Business Account') {
                System.debug('Its Business Account');
                emailId = bookingUnit.Booking__r.Account__r.Email__c;
            }
            else if(bookingUnit.Booking__r.Account__r.RecordType.Name == 'Person Account') {
                System.debug('Its Person Account');
                emailId = bookingUnit.Booking__r.Account__r.Email__pc;
            }

            System.debug('Email Id of Account: '+emailId);
        }*/


        responseUrl = FmIpmsRestCoffeeServices.getUnitSoa(registrationId);
        System.debug('responseUrl is: '+responseUrl);

        if(String.isBlank(responseUrl) || responseUrl == null || responseUrl == 'Null') {
            System.debug('In ApexMessage Method');
            ApexPages.Message msg = new ApexPages.Message(ApexPages.SEVERITY.WARNING, 'There was an error processing your request. Please try again...');
            ApexPages.addMessage(msg);
            return null;
        }

        //Http callout

        accessToken = FmIpmsRestCoffeeServices.accessToken;
        System.debug('AccessToken is' + accessToken);

        HttpRequest req = new HttpRequest();
        req.setEndpoint(responseUrl);
        req.setHeader('Accept', 'application/json');
        req.setMethod('GET');
        req.setHeader('Authorization','Bearer' + accessToken);
        req.setTimeout(120000);
        HttpResponse response = new Http().send(req);
        System.debug('Status Code = ' + response.getStatusCode());
        System.debug('Pdf Body = ' + response.getBodyAsBlob());

        ApexPages.Message pageMessage;

        if(response.getStatusCode() == 200) {
            pdfBlob =  response.getBodyAsBlob();
            pdfFile = EncodingUtil.base64Encode(pdfBlob);

            //SendGrid
            //Convert name to Capitalized Case
            /*if(str == NULL || String.isBlank(str)){
                str = bookingUnit.Booking__r.Account__r.Name;
            }*/
            String name = str.toLowerCase();
            List<String> names = name.split(' ');
            for (Integer i = 0; i < names.size(); i++)
                names[i] = names[i].capitalize();
            name = String.join(names, ' ');
            System.debug(name);

            /*if(strUnitName == NULL || String.isBlank(strUnitName)){
                strUnitName = bookingUnit.Unit_Name__c;
            }*/

            String emailSubject = 'Statement of Account for unit ' + strUnitName;
            String emailBody = 'Dear ' + name + ',';
            emailBody += '<br/><br/> As requested, please find attached the Statement of Account for unit ' + strUnitName;
            emailBody += '<br/><br/><br/> Regards,';
            emailBody += '<br/><br/>' + UserInfo.getName();
            emailBody += '<br/> Officer-Client Relations';
            emailBody += '<br/> DAMAC PROPERTIES Co. LLC.';
            emailBody += '<br/> P.O. Box: 2195, Dubai, United Arab Emirates';
            emailBody += '<br/> Telephone: +971 4 237 5000';
            emailBody += '<br/> Fax: +971 4 373 1373';
            emailBody += '<br/> E-mail: <a href="mailto:atyourservice@damacproperties.com"> atyourservice@damacproperties.com</a>';
            emailBody += '<br/> <a href="http://www.damacproperties.com/">http://www.damacproperties.com/</a>';

            Attachment attach = new Attachment();
            attach.contentType = 'application/pdf';
            attach.name = 'Unit SOA '+registrationId+'.pdf';
            attach.body = pdfBlob;

            List<Attachment> attachlst = new List<Attachment>();
            attachlst.add(attach);

            List<OrgWideEmailAddress> orgWideAddressHelloDamac = [
                SELECT  Id
                        , Address
                        , DisplayName
                FROM    OrgWideEmailAddress
                WHERE   DisplayName = :Label.DAMAC_no_replysf_damacgroup_com
            ];
            System.debug('@@@Email Address: ' +  orgWideAddressHelloDamac[0].Address);

            List<OrgWideEmailAddress> bccMailAddress = [
                SELECT Id,
                       Address,
                       DisplayName
                FROM   OrgWideEmailAddress
                WHERE  DisplayName = :Label.Sf_Copy_Bcc_Mail_Id
            ];
            System.debug('@@@Bcc mail Id: ' + bccMailAddress[0].Address);

            Boolean isSandbox = [SELECT IsSandbox FROM Organization].IsSandbox;
            System.debug('IsSandbox is: '+isSandbox);

            if ((isSandbox
                && (emailId.containsIgnoreCase('davidvarghese1989@gmail.com')
                    || emailId.containsIgnoreCase('@damacgroup.com')
                    || emailId.containsIgnoreCase('@eternussolutions.com')
                    || emailId.containsIgnoreCase('@mailinator.com')
                )
            ) || !isSandbox) {
                SendGridEmailService.SendGridResponse sendGridResponse = SendGridEmailService.sendEmailService(
                    emailId, '', '', '', bccMailAddress[0].Address, '', emailSubject, '', orgWideAddressHelloDamac[0].Address, '', '', '',
                    'text/html', emailBody, '', attachlst
                );

                if(sendGridResponse.ResponseStatus == 'Accepted') {
                    EmailMessage mail = new EmailMessage();
                    mail.Subject = emailSubject;
                    mail.MessageDate = System.Today();
                    mail.Status = '3';
                    mail.RelatedToId = recordId;//Here it is BookingUnit id
                    mail.ToAddress = emailId;
                    mail.FromAddress = orgWideAddressHelloDamac[0].Address;
                    mail.TextBody = emailBody;
                    mail.CcAddress = '';
                    mail.BccAddress = bccMailAddress[0].Address;
                    mail.Sent_By_Sendgrid__c = true;
                    mail.SentGrid_MessageId__c = sendGridResponse.messageId;
                    mail.Account__c = accountId;
                    mail.SendGrid_Status__c = sendGridResponse.ResponseStatus;
                    insert mail;
                    pageMessage = new ApexPages.Message(
                        ApexPages.SEVERITY.CONFIRM, 'An email has been successfully sent to ' + emailId
                    );
                }
                else {
                    Error_Log__c objError = new Error_Log__c();
                    objError.Account__c = accountId;
                    objError.Error_Details__c = 'Unit SOA Email from SendGrid for registation Id ' + registrationId;
                    objError.Process_Name__c = 'Generic Email';
                    insert objError;
                    pageMessage = new ApexPages.Message(
                        ApexPages.SEVERITY.ERROR, 'There was a problem while sending an email to ' + emailId
                    );
                }

            }

             else {
                pageMessage = new ApexPages.Message(
                    ApexPages.SEVERITY.ERROR, 'There was an error while generating Unit SOA for registration Id ' + registrationId
                );
            }
        }

        ApexPages.addMessage(pageMessage);

        FmIpmsRestCoffeeServices.updateCustomSetting = true;
        FmIpmsRestCoffeeServices.updateBearerToken();//calling updateMethod of Custom Setting after all callouts execution
        return null;

    }

}