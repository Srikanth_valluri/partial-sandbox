/** 
 * Description: Test class for GenerateNonSRCaseExtension
 */

@isTest
private class GenerateNonSRCaseExtensionTest {

    /** 
     * Description: Test method to check if the Non SR type of case is generated or not.
     */
    @isTest
    public static void testNonSRCaseCreation() {
        
        // Creation of Case
        Case objCase = new Case();
        objCase.Status = 'NotSubmitted';
        objCase.recordTypeId = getRecordTypeIdForNonSRCase();
        insert objCase;

        // Creation of Task
        Task objTask = new Task();
        objTask.subject = 'Upload All Handover Documents';
        objTask.ActivityDate = System.Today();
        objTask.WhatId = objCase.Id;
        objTask.status = 'In Progress';
        objTask.description = 'Test comments';
        insert objTask;
        
        PageReference redirectionUrl;
        Test.startTest();
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(objTask);
            GenerateNonSRCaseExtension objController = new GenerateNonSRCaseExtension(standardControllerInstance);
            objController.objTask = objTask;
            Test.setCurrentPageReference(new PageReference('Page.GenerateNonSRCasePage'));
            System.currentPageReference().getParameters().put('Id', objTask.Id);
            redirectionUrl = objController.createNonSRCase();
        Test.stopTest();
        
        // Check if the task status is completed and the task is related to new case or not
        System.assertEquals('/'+objTask.Id, redirectionUrl.getURL());
        System.assert(objTask.status == 'Completed');
        System.assert(objTask.whatId != objCase.Id);
    }
    
    /**
     * Method to get the "Non-SR" record type
     */
    private static Id getRecordTypeIdForNonSRCase() {
        Map <String,Schema.RecordTypeInfo> caseRecordTypes = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id nonSRRecordTypeID = caseRecordTypes.get('Non SR case').getRecordTypeId();
        return nonSRRecordTypeID ;
    }
}