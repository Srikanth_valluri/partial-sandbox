@isTest
private class HDApp_ResetPassword_APITest {

    @TestSetup
    static void TestData() {
        
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com',
                                       Salutation = 'MR',
                                       Nationality__c = 'Indian',
                                       Nationality__pc = 'Indian',
                                       Passport_Number__pc = '1234',
                                       Passport_Number__c = '1234',
                                       Passport_Expiry_Date__c = Date.today(),
                                       Passport_Expiry_Date__pc = Date.today(),
                                       PersonEmail = 'test@test.com',
                                       Email__c = 'test@test.com',
                                       Mobile_Phone_Encrypt__pc = '0091123456789',
                                       Mobile__c = '0091123456789',
                                       PersonMailingStreet = 'test',
                                       PersonMailingPostalCode = '12345',
                                       PersonMailingCity = 'test',
                                       PersonMailingState = 'test',
                                       PersonMailingCountry = 'test',
                                       Address_Line_1__pc = 'test',
                                       Address_Line_2__pc = 'test',
                                       Address_Line_3__pc = 'test',
                                       Address_Line_4__pc = 'test',
                                       Address_Line_1__c = 'test',
                                       Address_Line_2__c = 'test',
                                       Address_Line_3__c = 'test',
                                       Address_Line_4__c = 'test',
                                       City__pc = 'test',
                                       City__c = 'Batna',
                                       Country__pc = 'India',
                                       Country__c = 'India',
                                       Mobile_Phone_Encrypt_2__pc = '0091123456789',
                                       Mobile_Phone_Encrypt_3__pc = '0091123456789',
                                       Mobile_Phone_Encrypt_4__pc = '0091123456789',
                                       Mobile_Phone_Encrypt_5__pc = '0091123456789',
                                       Email_2__pc = 'test@test.com',
                                       Email_3__pc = 'test@test.com',
                                       Email_4__pc = 'test@test.com',
                                       Email_5__pc = 'test@test.com',
                                       Status__c = 'Active',
                                       Email_1__c = 'test@test.com',
                                       Email_2__c = 'test@test.com',
                                       Email_3__c = 'test@test.com',
                                       Primary_Language__c = 'English',
                                       Date_of_Birth__pc = Date.today().addDays(-10),
                                       Date_Of_Birth__c = Date.today().addDays(-10),
                                       Mobile_Country_Code__pc = 'India: 0091',
                                       Mobile_Country_Code__c = 'India: 0091',
                                       Mobile_Country_Code_2__c = 'India: 0091',
                                       Mobile_Country_Code_3__c = 'India: 0091',
                                       Mobile_Country_Code_4__c = 'India: 0091',
                                       Mobile_Country_Code_5__c = 'India: 0091',
                                       Mobile_Phone_2__c = '0091123456789',
                                       Mobile_Phone_3__c = '0091123456789',
                                       Mobile_Phone_4__c = '0091123456789',
                                       Mobile_Phone_5__c = '0091123456789',
                                       Mobile_Country_Code_2__pc = 'India: 0091',
                                       Mobile_Country_Code_3__pc = 'India: 0091',
                                       Mobile_Country_Code_4__pc = 'India: 0091',
                                       Mobile_Country_Code_5__pc = 'India: 0091',
                                       CR_Number__c = '123456',
                                       CR_Registration_Expiry_Date__c = Date.Today().addDays(10),
                                       Mobile_Phone_Encrypt__c = '12356987' );
        insert account;
        
        Contact con = [SELECT id FROM Contact WHERE AccountId=: account.Id];

        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Customer Community Login User(Use this)%' Limit 1];
        User user1 = new User(Username = 'test1234567@test.com',
                              //ContactId = contact1.Id,
                              ContactId = con.Id,
                              ProfileId = portalProfile.Id,
                              Alias = 'test123',
                              Email = 'test12345@test.com',
                              EmailEncodingKey = 'UTF-8',
                              LastName = 'Test Account',
                              CommunityNickname = 'test12345',
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US'
                              );
        Database.insert(user1);
        
    }

    @isTest
    static void Test_resetpassword() {

        Account acc = [SELECT id FROM Account WHERE Party_ID__c = '63062'];
        User usr = [SELECT id, username FROM user WHERE username = 'test1234567@test.com'];

        String guid ='abcd-1234-efgh-5678';
        OTP__c objOTP = new OTP__c();
        objOTP.Account__c = acc.id;
        objOTP.FM_Email_OTP__c = '1234';
        objOTP.FM_Process_Name__c = HDApp_Constants.RESET_PASSWORD_PROCESS;
        objOTP.FM_SMS_OTP__c = '1234';
        objOTP.OTP_Number__c = '1234';
        objOTP.GUID__c = guid;
        objOTP.isExpired__c = false;
        objOTP.isVerified__c =  true;
        insert objOTP;

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/resetPassword/v.1.0';  
        req.addParameter('username',usr.username);
        req.addParameter('guid',guid);
        req.addParameter('newPassword','test@1234');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        /************************ SUCCESS ****************************/
        HDApp_ResetPassword_API.resetpassword();

        /*********************** ENDPOINT CHECK **********************/
        req = new RestRequest();
        res = new RestResponse();
        req.requestURI = '/resetPassword/';  
        req.addParameter('username',usr.username);
        req.addParameter('guid',guid);
        req.addParameter('newPassword','test@1234');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        HDApp_ResetPassword_API.resetpassword();
        
        /************************ NO_PARAM ****************************/
        req = new RestRequest();
        res = new RestResponse();
        req.requestURI = '/resetPassword/v.1.0';  
        //req.addParameter('username',usr.username);
        req.addParameter('guid',guid);
        req.addParameter('newPassword','test@1234');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        HDApp_ResetPassword_API.resetpassword();

        req = new RestRequest();
        res = new RestResponse();
        req.requestURI = '/resetPassword/v.1.0';  
        req.addParameter('username',usr.username);
        //req.addParameter('guid',guid);
        req.addParameter('newPassword','test@1234');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        HDApp_ResetPassword_API.resetpassword();

        req = new RestRequest();
        res = new RestResponse();
        req.requestURI = '/resetPassword/v.1.0';  
        req.addParameter('username',usr.username);
        req.addParameter('guid',guid);
        //req.addParameter('newPassword','test@1234');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        HDApp_ResetPassword_API.resetpassword();

        /************************ NO_PARAM Value **********************/
        req = new RestRequest();
        res = new RestResponse();
        req.requestURI = '/resetPassword/v.1.0';  
        req.addParameter('username','');
        req.addParameter('guid',guid);
        req.addParameter('newPassword','test@1234');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        HDApp_ResetPassword_API.resetpassword();

        req = new RestRequest();
        res = new RestResponse();
        req.requestURI = '/resetPassword/v.1.0';  
        req.addParameter('username',usr.username);
        req.addParameter('guid','');
        req.addParameter('newPassword','test@1234');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        HDApp_ResetPassword_API.resetpassword();

        req = new RestRequest();
        res = new RestResponse();
        req.requestURI = '/resetPassword/v.1.0';  
        req.addParameter('username',usr.username);
        req.addParameter('guid',guid);
        req.addParameter('newPassword','');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        HDApp_ResetPassword_API.resetpassword();

        req = new RestRequest();
        res = new RestResponse();
        req.requestURI = '/resetPassword/v.1.0';  
        req.addParameter('username',usr.username);
        req.addParameter('guid','32132');
        req.addParameter('newPassword','test@123');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        HDApp_ResetPassword_API.resetpassword();
        /************************ END**********************/
        Test.stopTest();

    }
}