@isTest
public class Damac_SendWeChat_Test {
    static testMethod void Damac_SendWeChat () {
        ID recordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();      
        Inquiry__c inq = new Inquiry__c ();
        inq.Mobile_phone__c = '7894561230';
        inq.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Mobile_Phone_Encrypt__c = 'WERTYUIYRTYUIYRTYU';
        inq.Class__c = 'test';
        inq.First_Name__c = 'test';
        inq.Inquiry_Status__c = 'Active';
        inq.Inquiry_Source__c ='Agent Referral';  
        inq.RecordtypeId = recordTypeId;
        inq.Class__c = 'test';
        inq.Email__c = 'test@gmail.com';
        inq.Phone_CountryCode__c = 'United Arab Emirates: 00971';
        inq.Nationality__c = 'Australian';
        inq.weChat_Id__c = 'WERTYUIYRTYUIYRTYU';
        insert inq;        
        
        Wechat_Message__c msg = new Wechat_Message__c ();
        msg.Last_Message_Sent__c = DateTime.Now ();
        msg.Outbound_Success__c = false;
        msg.Record_Id__c = inq.Id;

        msg.Wechat_ID__c = 'WERTYUIYRTYUIYRTYU';
        msg.Unique_Id__c = 'WERTYUIYRTYUIYRTYU'+userInfo.getUserId ();
        upsert msg Unique_Id__c ;
        
        Wechat_Deliverability__c del = new Wechat_Deliverability__c ();
        del.Wechat_Message__c = msg.id;
        del.Message_Source_Type__c = 'Inbound';
        insert del;
        
        Wechat_Deliverability__c del2 = new Wechat_Deliverability__c ();
        del2.Wechat_Message__c = msg.id;
        del2.Message_Source_Type__c = 'Outbound';
        insert del2;
        
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersionInsert;
        
        Apexpages.currentpage().getparameters().put ('id', inq.id);
        Damac_SendWeChat obj = new Damac_SendWeChat ();
        Damac_SendWeChat.getContentDocURL (contentVersionInsert.ID);
        obj.init ();
        obj.msgText = 'test';
        obj.msgFileText = 'test';
        obj.msgFileLink = 'test.com';
        obj.fileType = 'jpg';
        
        obj.sendMessage ();

    }
}