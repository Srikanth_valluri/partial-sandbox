/***************************************************************************************************
 * @Description : Batch class to generate FM Pre-Legal Notice on booking unit                      *
 *                                                                                                 *
***************************************************************************************************/
global class UpdateFmPreLegalNoticeOnBu implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful{
  
    private String query;
    String buId;
    String strDDPTemplateId;
    String strTemplateDeliveryId;
    public String SERVER_URL;
    public String SESSION_ID;

    public Database.Querylocator start( Database.BatchableContext bc ) {
        login();
        query = 'SELECT Id, Send_FM_Pre_Legal_Notice__c, ' +
                        ' Booking__r.Account__r.Mobile_Phone_Encrypt__pc,Building_Name__c,Unit_Name__c, ' +
                        ' Booking__r.Account__r.Name FROM Booking_Unit__c '+
                        ' WHERE Send_FM_Pre_Legal_Notice__c = true ';
                        /*' WHERE Unit_Active__c = \'Active\' AND Registration_ID__c != Null' +
                        ' AND Booking__r.Account__c != NULL AND Unit_Name__c != NULL AND Building_Name__c != NULL' +
                        ' AND Send_FM_Pre_Legal_Notice__c = true ';*///AND Id = \'a0x0Y000001hiZW\'';
                       
        system.debug( 'query  : '+ query );
        return Database.getQueryLocator(query);    
    }
    

  
  global void execute(Database.BatchableContext info, List<Booking_Unit__c> lstBu ){
   
    /*for( Booking_Unit__c objBu : lstBu ) {
        if( objBu.Send_FM_Pre_Legal_Notice__c = true ) {
            if (!Test.isRunningTest()) {
                Database.executeBatch(
                    new GenerateBUDrawloopDocumentBatch(
                        String.valueOf(objBu.Id)
                        , 'a0V25000002xZL8'
                        , 'a0T25000004nSjk' )
                );
            }
        }
    }*/
    
       system.debug('SESSION_ID*********'+SESSION_ID);
        
        if(SESSION_ID != null && SESSION_ID != ''){
            system.debug('SESSION ID received*********');
            Loop.loopMessage lm = new Loop.loopMessage();
            lm.sessionId = SESSION_ID;
            system.debug('== List Size ==' +lstBU.size() );
            for( Booking_Unit__c objBU : lstBU ){
                system.debug('== In for objBU : '+objBU);
                Map<string, string> variables = new map<String,String>(); // MAIN RECORD ID – SAME OBJECT AS THE DDP RECORD TYPE SPECIFIES’ // DDP ID
                variables.put('deploy', Label.FM_Pre_Legal_DDP_Delivery_Id );
                lm.requests.add(new Loop.loopMessage.loopMessageRequest(
                                objBU.Id, 
                                Label.FM_Pre_Legal_DDP_Template_Id,
                                variables)); 
            } // end of for loop
            String response = lm.sendAllRequests();
            system.debug('== response ==' +response );
        }
    
  }
  
    public void finish( Database.BatchableContext bc ) {
        
    }
 
    //Method used for login purpose to get the session id
    public void login(){
        Admin_Login_for_Drawloop__c mc = Admin_Login_for_Drawloop__c.getOrgDefaults();
        Http h = new Http(); 
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://' + mc.Domain__c + '.salesforce.com/services/Soap/u/28.0');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        request.setHeader('SOAPAction', '""');
        request.setBody('<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Header/><Body><login xmlns="urn:partner.soap.sforce.com"><username>' + mc.Username__c + '</username><password>' + mc.Password__c + '</password></login></Body></Envelope>');
        
        HttpResponse response = h.send(request);
        system.debug('----Response Body-----'+response.getBody());
        
        Dom.XmlNode resultElmt = response.getBodyDocument().getRootElement()
        .getChildElement('Body','http://schemas.xmlsoap.org/soap/envelope/')
        .getChildElement('loginResponse','urn:partner.soap.sforce.com')
        .getChildElement('result','urn:partner.soap.sforce.com');
        
        SERVER_URL = resultElmt.getChildElement('serverUrl','urn:partner.soap.sforce.com').getText().split('/services')[0];
        SESSION_ID = resultElmt.getChildElement('sessionId','urn:partner.soap.sforce.com').getText();
        
        system.debug('--SERVER_URL---'+SERVER_URL);
        system.debug('--SESSION_ID---'+SESSION_ID);
        if(Test.isRunningTest()){
            SESSION_ID = 'none';
        }
    }
 
}