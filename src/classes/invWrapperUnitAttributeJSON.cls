public class invWrapperUnitAttributeJSON {

    public String Unit;
    public String Area;
    public String Status;
    public String Price;
    public String Bedroom;
    public String space_type;
    public String ACD;
    public String completed_per;
    public String BCC_date;
    public String Furnished;
    public String plot_no;
    public String municipality_no;
    public String Parkings;
    public String con_status;
    public String view_type;
    public String store_room;
    public String DEWA_no;
    public String accessible_unit;
    public String shell_core;

    
    public static List<invWrapperUnitAttributeJSON> parse(String json) {
        return (List<invWrapperUnitAttributeJSON>) System.JSON.deserialize(json, List<invWrapperUnitAttributeJSON>.class);
    }
}