@isTest
/*
* Revision History: 
* Version   Author          Date        Description.
* 1.1       Swapnil Gholap  21/11/2017  Initial Draft
*/
public class RoleUtilityTest {
    static testMethod void Test1(){        
        RoleUtility.getSalesforceRole('Manager');
        RoleUtility.getSalesforceRole('Director');
        RoleUtility.getSalesforceRole('HOD');
        RoleUtility.getSalesforceRole('Committee');
        RoleUtility.getSalesforceRole('SVP Operations');
        RoleUtility.getSalesforceRole('Sales Admin AVP');
    }
}