/*************************************************************************************************************************
* Name               : SendEmailToPromoterManagerBatch                                                                   *
* Description        : Batch to send reports to Promoter managers emails every 3 hours.           ..................     *
* Created Date       : 24/07/2018                                                                                        *
* Created By         : MOHIT                                                                                             *
* --------------------------------------------------------------------------------------------------------------------   */
global class SendEmailToPromoterManagerBatch  implements Database.Batchable<sObject>, Database.Stateful{
    global String query;
    global Map<Id,String> userAndItsManagerMap = new  Map<Id,String>();
    global Map<Id,String> managerAndItsInfoMap = new  Map<Id,String>();
    global String endTimeFormat;
    global String startTimeFormat;
    global String profileName = 'Promoter Community Profile';
    global Map<Id,User> managerIdUserInfo = new Map<Id,User>();
    global DateTime startDate;
    global DateTime endDate;
    global Map<Id,Integer> proInqCountWithCreatedByMap = new Map<Id,Integer>();
    global Map<Id,String> proInqManagerInfoMap = new Map<Id,String>();
    global Map<Id,Promoter_Inquiry__c> proInqCreatedUserInfoMap = new Map<Id,Promoter_Inquiry__c>();


   // global Map<Id,User> inqCreatedByIdWithManagerIdMap = new Map<Id,User>();
    global Map<Id, Map<Id,Integer>> managerIdWithInqCountmap = new Map<Id, Map<Id,Integer>>();
    global SendEmailToPromoterManagerBatch(DateTime endSchedulerDate){
        endDate = endSchedulerDate;
        endTimeFormat = String.valueOfGmt(endDate);
        List<String> splitEndDate = endTimeFormat.split(' ');
        endTimeFormat = splitEndDate[0]+ 'T'+splitEndDate[1]+'Z';

        startDate = endDate.addHours(-3);
        startTimeFormat  = String.valueOfGmt(startDate);
        List<String> startTimeFormatList = startTimeFormat.split(' ');
        startTimeFormat = startTimeFormatList[0] + 'T'+startTimeFormatList[1]+'Z';
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query ;
        if(test.isrunningtest()){
         query = ' Select id,Manager.name,Managerid,Manager.email,name from User where manager.email != null and isactive = true and  profile.Name =\''+profileName +'\' limit 10 ';
        }else{
         query = ' Select id,Manager.name,Managerid,Manager.email,name from User where manager.email != null and isactive = true and profile.Name =\''+profileName +'\' ';   
        }
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<User> userList) {
        Set<Id> userIdsSet = new Set<id>();
        for(User userObj : userList){
            userIdsSet.add(userObj.Id);
            userAndItsManagerMap.put(userObj.Id,userObj.managerId+','+userObj.Name);
            managerAndItsInfoMap.put(userObj.managerId,userObj.manager.Name+','+userObj.manager.Email);
            system.debug('>>>userAndItsManagerMap>>>'+userAndItsManagerMap);
        }
        system.debug('>>>userAndItsManagerMap>>>'+userAndItsManagerMap);
        system.debug('>>userIdsSet>>>'+userIdsSet);
        String promoterInqQuery = ' Select id , Name ,Owner.id, createdby.name,Created_By_Manager__r.Name,Created_By_Manager__r.email from Promoter_Inquiry__c where  CreatedDate >=' 
                                    + startTimeFormat + ' AND CreatedDate  <= ' +endTimeFormat+ ' AND Created_By_Manager__c != Null AND createdBy.profile.Name = \'' +profileName+ '\' ';
        List<Promoter_Inquiry__c> proInqList = Database.query(promoterInqQuery );
        if( proInqList != null && proInqList.size() > 0 && !proInqList.isEmpty()){
            for(Promoter_Inquiry__c proInqObj :  proInqList){
            //userList.remove(proInqObj.createdbyId);
            //Integer result = userList.indexOf(proInqObj.createdbyId);
            //system.debug('>>>>result>>>'+result);
           // userToRemove.add(proInqObj.createdbyId);
           // userList.remove(result);
            system.debug('>>proInqObj.createdbyid>>>'+proInqObj.createdbyid);
                if(userIdsSet.contains(proInqObj.createdbyid)){
                    system.debug('>>proInqObj true true>>>');
                }
                
                if(!proInqCreatedUserInfoMap.containsKey(proInqObj.createdbyid)) {
                        proInqCreatedUserInfoMap.put(proInqObj.createdbyid,proInqObj);
                }
                if(!proInqManagerInfoMap.containsKey(proInqObj.Created_By_Manager__c)) {
                        proInqManagerInfoMap.put(proInqObj.Created_By_Manager__c,proInqObj.Created_By_Manager__r.Name+ ','+proInqObj.Created_By_Manager__r.email);
                }
                if(proInqCountWithCreatedByMap.containsKey(proInqObj.createdbyid )){
                    Integer count = proInqCountWithCreatedByMap.get(proInqObj.createdbyid );
                    count++;
                    proInqCountWithCreatedByMap.put(proInqObj.createdbyid ,count);
                    
                } else{
                    proInqCountWithCreatedByMap.put(proInqObj.createdbyid ,1);
                }
            }
        }
       
        for (Id userId : userIdsSet){
            if(!proInqCountWithCreatedByMap.containsKey(userId )){
                proInqCountWithCreatedByMap.put(userId,0);   
            } 
        }
        //userList.removeAll(userToRemove); 
        for(Id inqCreatedById : proInqCountWithCreatedByMap.keySet()){
            system.debug('>>>inqCreatedById>>>>'+inqCreatedById);
            system.debug('>>>userAndItsManagerMap.get(inqCreatedById )>>>>'+userAndItsManagerMap.get(inqCreatedById ).split(',')[0]);
            Id managerId = userAndItsManagerMap.get(inqCreatedById ).split(',')[0];
            Integer counter = proInqCountWithCreatedByMap.get(inqCreatedById);
            if(managerIdWithInqCountmap.containsKey(managerId)){
                managerIdWithInqCountmap.get(managerId).put(inqCreatedById,counter);
            }else{
                managerIdWithInqCountmap.put(managerId,new Map<Id,Integer> {inqCreatedById => counter });
            }
        }                                         
     } 
     global void finish(Database.BatchableContext BC) {
        List<Messaging.SingleEmailMessage> mails=new List<Messaging.SingleEmailMessage>();
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'no-replysf@damacgroup.com'];
        //for(ID id : proInqManagerInfoMap.keySet())
        for(ID id : managerAndItsInfoMap.keySet()){
            String mailBody='Hi '+managerAndItsInfoMap.get(id).split(',')[0]+',<br/>'+'Following are the details for the Promoter Inquiries created:<br/><br/>'+'<html><body>'+
                            '<table style="border:1px solid black; text-align:left; border-collapse:collapse;width:50%;">'+
                            '<tr><th style="border:1px solid black; text-align:center; border-collapse:collapse;">Sr.No</th>'+
                            '<th style="border:1px solid black;border-collapse:collapse; text-align:left;">Promoter</th><th style="border:1px solid black;border-collapse:collapse; text-align:center;">Count</th></tr>';
            Integer serialNumber=1;
            String internalBody='';
            for (Id idCount : proInqCountWithCreatedByMap.keyset()){
                if(managerIdWithInqCountmap.get(id).containsKey(idCount)){
                    internalBody=internalBody+'<tr>'+'<td  style="border:1px solid black;border-collapse:collapse;text-align:center;">'+serialNumber+'</td>'+'<td style="border:1px solid black;border-collapse:collapse;">'+userAndItsManagerMap.get(idCount).split(',')[1]+'</td>'
                                        +'<td style="border:1px solid black;text-align:center;border-collapse:collapse;">'+proInqCountWithCreatedByMap.get(idCount)+'</td>'+'</tr>';
                    serialNumber++;
                }                
            }
            mailBody=mailBody+internalBody+'</table></body></html>';
            List<String> toAddress=new List<String>{managerAndItsInfoMap.get(id).split(',')[1]};
            Messaging.SingleEmailMessage emailContent=SendEmailUtility.createEmail(mailBody,toAddress,'Promoter Inquiry Details',owea);
            mails.add(emailContent);
        }
        Messaging.sendEmail(mails);
    }
}