@isTest
private class PayfortPurchasePageControllerTest {

	@testSetup
	private static void testData() {

		//custom setting
		insert new PayfortGatewayCreds__c( Merchant_Identifier__c= '17e98f0b'
										 , Request_Phrase__c= '$2y$10$QzSutSlCY'
										 , ResponsePhrase__c = '$2y$10$V5IdbyFmy'
										 , AccessCode__c='wT7xGi6a4fwntXQyq0Gs'
										 , PurchaseEndpoint__c='https://sbpaymentservices.payfort.com/FortAPI/paymentApi'
										 , CurrencyInPay__c='AED'
										 , SiteBaseURL__c='https://partial-servicecloudtrial-155c0807bf-1580afc5db1.cs80.force.com/Customer'
										 , TokenizationEndpoint__c='https://sbcheckout.PayFort.com/FortAPI/paymentPage'
										 , Language__c='en' );

		NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'Test Unit Name',
                                      Registration_ID__c = '3901');
        insert bookingUnit;

        //Insert Receipt;
        Receipt__c receipt = new Receipt__c(Booking_Unit__c = bookingUnit.Id, Customer__c=account.id);
        insert receipt;
	}

	@isTest 
	private static void testPurchaseWithRememberCard() {

		Test.startTest();

		Booking_Unit__c bookingUnit = [SELECT id, Unit_Name__c 
									   FROM Booking_Unit__c 
									   WHERE Unit_Name__c = 'Test Unit Name'];
        
        Account account = [SELECT id, Email__pc, Email__c, isPersonAccount 
        				   FROM Account 
        				   WHERE Party_ID__c = '63062'];

        CustomerCommunityUtils.customerAccountId = account.Id;

		PageReference pager = page.PayfortPurchasePage;
		Test.setCurrentPage(pager);

		pager.getParameters().put('command', 'PURCHASE');
		pager.getParameters().put('response_message', 'Success');
		pager.getParameters().put('remember_me', 'Yes');
		pager.getParameters().put('token_name', 'abcd1234');
		pager.getParameters().put('status', '14');
		pager.getParameters().put('card_number', '418887******0002');
		
		PayfortPurchasePageController objCls = new PayfortPurchasePageController();

		Test.stopTest();
	}

	@isTest 
	private static void testPurchaseWithoutRememberCard() {
		Test.startTest();

		Booking_Unit__c bookingUnit = [SELECT id, Unit_Name__c 
									   FROM Booking_Unit__c 
									   WHERE Unit_Name__c = 'Test Unit Name'];

	    Account account = [SELECT id, Email__pc, Email__c, isPersonAccount 
	    				   FROM Account 
	    				   WHERE Party_ID__c = '63062'];

	    CustomerCommunityUtils.customerAccountId = account.Id;

		PageReference pager = page.PayfortPurchasePage;
		Test.setCurrentPage(pager);

		pager.getParameters().put('command', 'PURCHASE');
		pager.getParameters().put('response_message', 'Success');
		pager.getParameters().put('remember_me', 'No');
		//pager.getParameters().put('token_name', 'abcd1234');
		pager.getParameters().put('status', '14');
		pager.getParameters().put('card_number', '418887******0002');
		
		PayfortPurchasePageController objCls = new PayfortPurchasePageController();

		Test.stopTest();
	}

	@isTest 
	private static void testTokenizationWitInstallmentCVV() {

		Test.startTest();

		Account account = [SELECT id, Email__pc, Email__c, isPersonAccount 
						   FROM Account 
						   WHERE Party_ID__c = '63062'];

	    CustomerCommunityUtils.customerAccountId = account.Id;

		PageReference pager = page.PayfortPurchasePage;
		Test.setCurrentPage(pager);

		pager.getParameters().put('service_command', 'TOKENIZATION');
		pager.getParameters().put('token_name', 'abcd1234');
		pager.getParameters().put('merchant_reference', 'test');
		pager.getParameters().put('card_security_code', '123');
		pager.getParameters().put('issuer_code', 'zaQnN1');
		pager.getParameters().put('plan_code', 'M9mv5Z');
		pager.getParameters().put('remember_me', 'YES');

		pager.getHeaders().put('True-Client-IP','125.0.0.1');
		pager.getHeaders().put('X-Salesforce-SIP','125.0.0.1');
		pager.getHeaders().put('X-Forwarded-For','125.0.0.1');

		String jsonResponse =  ' {"response_code":"20064","card_holder_name":"Test","signature":"3bd5cc500603ce507700a033c7202bb972306f769c73fdd44633e6afe302bc24","merchant_identifier":"17e98f0b","access_code":"wT7xGi6a4fwntXQyq0Gs","customer_ip":"114.143.177.210","plan_code":"V1rqnl","number_of_installments":"24","language":"en","eci":"ECOMMERCE","issuer_code":"zaQnN1","installments":"HOSTED","merchant_reference":"RECP-0853","currency":"AED","remember_me":"YES","amount":"100100","card_number":"418887******0002","payment_option":"VISA","expiry_date":"2105","fort_id":"158366670700023210","command":"PURCHASE","3ds_url":"https://sbsimulator.payfort.com/secure3dsSimulator?FORTSESSIONID=9k5rgubedcfdcc4l91aba386mr&paymentId=5740559166725965820&DOID=C70BAA4D83511CCADAEC071226C925FB&o=pt&action=retry","response_message":"3-D Secure check requested","customer_email":"shubham.suryawanshi@eternussolutions.com","status":"20"}'; 

		FmHttpCalloutMock.Response getPurchaseResponse = new FmHttpCalloutMock.Response(200, 'Success', jsonResponse);

		Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
            'https://sbpaymentservices.payfort.com/FortAPI/paymentApi' => getPurchaseResponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
		

		PayfortPurchasePageController objCls = new PayfortPurchasePageController();

		Test.stopTest();
	}

	@isTest 
	private static void testTokenizationWitInstallment() {
		Test.startTest();

		Account account = [SELECT id, Email__pc, Email__c, isPersonAccount 
						   FROM Account 
						   WHERE Party_ID__c = '63062'];

		CustomerCommunityUtils.customerAccountId = account.Id;

		PageReference pager = page.PayfortPurchasePage;
		Test.setCurrentPage(pager);

		pager.getParameters().put('service_command', 'TOKENIZATION');
		pager.getParameters().put('token_name', 'abcd1234');
		pager.getParameters().put('merchant_reference', 'test');
		//pager.getParameters().put('card_security_code', '123');
		pager.getParameters().put('issuer_code', 'zaQnN1');
		pager.getParameters().put('plan_code', 'M9mv5Z');
		pager.getParameters().put('remember_me', 'YES');

		pager.getHeaders().put('True-Client-IP','125.0.0.1');
		pager.getHeaders().put('X-Salesforce-SIP','125.0.0.1');
		pager.getHeaders().put('X-Forwarded-For','125.0.0.1');

		String jsonResponse =  ' {"response_code":"20064","card_holder_name":"Test","signature":"3bd5cc500603ce507700a033c7202bb972306f769c73fdd44633e6afe302bc24","merchant_identifier":"17e98f0b","access_code":"wT7xGi6a4fwntXQyq0Gs","customer_ip":"114.143.177.210","plan_code":"V1rqnl","number_of_installments":"24","language":"en","eci":"ECOMMERCE","issuer_code":"zaQnN1","installments":"HOSTED","merchant_reference":"RECP-0853","currency":"AED","remember_me":"YES","amount":"100100","card_number":"418887******0002","payment_option":"VISA","expiry_date":"2105","fort_id":"158366670700023210","command":"PURCHASE","3ds_url":"https://sbsimulator.payfort.com/secure3dsSimulator?FORTSESSIONID=9k5rgubedcfdcc4l91aba386mr&paymentId=5740559166725965820&DOID=C70BAA4D83511CCADAEC071226C925FB&o=pt&action=retry","response_message":"3-D Secure check requested","customer_email":"shubham.suryawanshi@eternussolutions.com","status":"20"}'; 

		FmHttpCalloutMock.Response getPurchaseResponse = new FmHttpCalloutMock.Response(200, 'Success', jsonResponse);

		Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
            'https://sbpaymentservices.payfort.com/FortAPI/paymentApi' => getPurchaseResponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
		

		PayfortPurchasePageController objCls = new PayfortPurchasePageController();
		objCls.selectedAccountId = CustomerCommunityUtils.customerAccountId;

		Test.stopTest();
	}

	@isTest 
	private static void testRedirectTo3dsURL() {

		Test.startTest();

		Account account = [SELECT id FROM Account WHERE Party_ID__c = '63062'];

		Receipt__c recp = [SELECT id, Name, Booking_Unit__c, Customer__c 
					       FROM Receipt__c
					       WHERE Customer__c =: account.id ];

		CustomerCommunityUtils.customerAccountId = account.Id;

		PageReference pager = page.PayfortPurchasePage;
		Test.setCurrentPage(pager);

		pager.getParameters().put('merchant_reference', 'test');
		pager.getParameters().put('status', '14');
		pager.getParameters().put('response_message', 'Failed');
		pager.getParameters().put('installments', 'HOSTED');
		pager.getParameters().put('number_of_installments', '12');
		pager.getParameters().put('plan_code', 'M9mv5Z');
		pager.getParameters().put('remember_me', 'YES');


		PayfortPurchasePageController objCls = new PayfortPurchasePageController();
		objCls.redirectTo3dsURL();
		
		objCls.typeOfUpdate = 'PURCHASE_WITH_TOKEN';
		objCls.selectedAccountId = account.Id;
		objCls.token_name = 'abcd1234';
		objCls.receiptName = recp.Name;
		objCls.fortId = '1234';
		objCls.responseMessage = 'test';
		objCls.statusCode = '00';
		objCls.cardNumber = '418887******0002';
		objCls.redirectTo3dsURL();

		objCls.typeOfUpdate = 'PURCHASE_WITHOUT_TOKEN';
		objCls.redirectTo3dsURL();



		Test.stopTest();
	}
}