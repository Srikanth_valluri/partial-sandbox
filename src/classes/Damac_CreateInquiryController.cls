global class Damac_CreateInquiryController {
    public Inquiry__c inq { get; set; }
    public String message { get; set; }
    public String msgType { get; set; }
    public String waRecordId { get; set; }
    public String campName { get; set; }
    public Nexmo_Whats_App_Request__c req;
    public Damac_CreateInquiryController (ApexPages.StandardController stdController) {
        inq = new Inquiry__c ();
        inq = (Inquiry__c) stdController.getRecord();
        System.Debug (stdController.view().getUrl());
        waRecordId = '';
        campName = '';
        waRecordId = apexpages.currentpage().getparameters().get('waRecordId');
        req = new Nexmo_Whats_App_Request__c ();
        req = [SELECT inquiry__c, Record_id__c, Source_Reference__c, Unique_Key__c, OwnerId FROM Nexmo_Whats_App_Request__c WHERE Id =: waRecordId ];
        
        
        
        if (req.Source_Reference__c != NULL) {
            inq.Campaign__c = req.Source_Reference__c;
            campname = [SELECT Name, Campaign_Name__c FROM Campaign__c WHERE Id =: req.Source_Reference__c ].campaign_Name__c;
        } else {
            
            try {
                if (!Test.isRunningTest ())
                    inq.Campaign__c = Label.Default_Campaign;
                campname = [SELECT Name, Campaign_Name__c FROM Campaign__c WHERE Id =: Label.Default_Campaign].Campaign_Name__c;
            }
            catch (Exception e) {}
        }
        
        if (req.Record_id__c == req.Id) {
            Nexmo_Whats_App_Message__c message = new Nexmo_Whats_App_Message__c();
            message = [SELECT Nexmo_Message_Body__c FROM Nexmo_Whats_App_Message__c 
                            WHERE Nexmo_Whats_App_Request__c =: req.Id 
                            Order BY CreatedDate ASC LIMIT 1];
            
            if (message.Nexmo_Message_Body__c != null) {
                String refId = message.Nexmo_Message_Body__c;
                
                if (refId.contains ('Ref:')) {
                    refId = refId.subStringBetween('Ref:', '!');
                    inq = Damac_FetchWA_InquiryDetails.doCallout (refId, inq);
                    if (inq.Campaign__c != null) 
                        campname = [SELECT Name, Campaign_Name__c FROM Campaign__c WHERE Id =: inq.Campaign__c ].campaign_Name__c;
                }
                
            }
        }
        
                       
    }
    
    public void save () {
        System.Debug (inq.Campaign__c+'====');
        System.Debug (' ::: waRecordId :::'+waRecordId );
        String campaignId = inq.Campaign__c;
        req = [SELECT Source_Reference__c, Unique_Key__c, OwnerId FROM Nexmo_Whats_App_Request__c WHERE Id =: waRecordId ];
        if (req.Source_Reference__c != NULL) {
            inq.Campaign__c = req.Source_Reference__c;
            campaignId = req.Source_Reference__c;
        }
        
        try {
            String uniqueKey = req.Unique_Key__c;
            String userId = '';
            if (req.Unique_Key__c.length () >= 18) {
                userId = req.Unique_Key__c.right(18);
                if (userId.startsWith ('005')) {
                    uniqueKey = uniqueKey.replace (userid, '');
                }
            }
            System.Debug ('==== Mobile =='+uniqueKey );
            String mobileCountryCode = MobileCountryShortCode.MobileCountryShortCodeOutput('00'+uniqueKey);
            uniqueKey = '00'+uniqueKey;
            inq.Mobile_CountryCode__c = mobileCountryCode;
            if(test.isRunningTest()) {
                mobileCountryCode = 'India:0091';
            }
            String mobileCode = mobileCountryCode.split (':')[1];
            mobileCode = mobileCode.trim ();
            
            if (uniqueKey.startsWith (mobileCode)) {
                uniqueKey = uniqueKey.subString (mobileCode.length (), uniqueKey.length ());
            }
            inq.Mobile_Phone_Encrypt__c = uniqueKey ;
            // Campaign Mapping
            
            if (campaignId != NULL && campaignId != '') {
                Id roadshowRt = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Roadshows').getRecordTypeId();
                Id digitalRt = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Digital').getRecordTypeId();
                Id standaloneRt = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Standalone').getRecordTypeId();
                Id conventionalRt = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Conventional').getRecordTypeId();
                Id salesEventsRt = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Sales Events').getRecordTypeId();
                Map<Id, Campaign__c> campaignMap = new Map<Id, Campaign__c> ();
                for (Campaign__c thisCampaign :[SELECT Id, Name, RecordTypeId, Campaign_Type_New__c FROM Campaign__c WHERE Id =:inq.Campaign__c]) {
                    campaignMap.put(thisCampaign.Id, thisCampaign);
                }
                if (campaignMap.get(inq.Campaign__c).RecordTypeId == roadshowRt) {
                    inq.Inquiry_Source__c = 'Roadshow/Exhibition';
                } else if (campaignMap.get(inq.Campaign__c).RecordTypeId == digitalRt) {
                    if (campaignMap.get(inq.Campaign__c).Campaign_Type_New__c == 'Social') {
                        inq.Inquiry_Source__c = 'Social';
                    } else if (campaignMap.get(inq.Campaign__c).Campaign_Type_New__c == 'Referral') {
                        inq.Inquiry_Source__c = 'Other Referrals';
                    } else {
                        inq.Inquiry_Source__c = 'Digital';
                    }
                } else if (campaignMap.get(inq.Campaign__c).RecordTypeId == standaloneRt) {
                    inq.Inquiry_Source__c = 'Direct';
                } else if (campaignMap.get(inq.Campaign__c).RecordTypeId == conventionalRt) {
                    if (campaignMap.get(inq.Campaign__c).Campaign_Type_New__c == 'Stands') {
                        inq.Inquiry_Source__c = 'Stands';
                    } else if (campaignMap.get(inq.Campaign__c).Campaign_Type_New__c == 'Referral') {
                        inq.Inquiry_Source__c = 'Other Referrals';
                    } else {
                        inq.Inquiry_Source__c = 'Conventional';
                    }
                } else if (campaignMap.get(inq.Campaign__c).RecordTypeId == salesEventsRt) {
                    inq.Inquiry_Source__c = 'Events';
                }
            } else {
                inq.Inquiry_Source__c = 'DAMAC Website';
                inq.Campaign__c = Label.Default_Campaign;
            }
            inq.whats_app__c = true;
            if (inq.Id != NULL)
                update inq;
            else {
                inq.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
                insert inq;
            }
            System.Debug (userId);
            

            if (!userId.startsWith ('005')) {
                id InqId = inq.Id;
                String query = 'SELECT '+getAllFields ('Inquiry__c')+' FROM Inquiry__c where id =: InqId  LIMIT 1';
                System.Debug (query);
                inq = Database.query (query);
                req.Unique_Key__c = uniqueKey+inq.OwnerId;
                req.Record_Id__c = inq.Id;
                req.Record_Name__c = inq.Name;
                req.OwnerId = inq.OwnerId;
                update req;
            }
            
            message = 'Inquiry created successfully.';                
            msgType = 'success';
        } catch (Exception e) {
            message = e.getMessage();
            msgType = 'error';
        }
        
    }
    public void clearMessages () {
        message = '';
        msgType = '';
    }
    
     /************************************************************************************************
    * @Description : Utility Method to get all fields based on object name for query                *
    * @Params      : Object API                                                                     *
    * @Return      : Object related Field APIs as Comma seperated                                   *
    ************************************************************************************************/
    public static string getAllFields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        
        if (objectType == null)
            return fields;
        
        Map <String, Schema.SObjectField> fieldMap = objectType.getDescribe ().fields.getMap ();
        
        for (Schema.SObjectField sfield : fieldMap.Values()) {
            
            fields += sfield.getDescribe().getName ()+ ', ';
            
        }
        return fields.removeEnd(', '); 
    }
    @RemoteAction
    global static List<Campaign__c> getCampaigns (String searchString) {
        List<Campaign__c> campNames = New List<Campaign__c> ();
            string searchquery =  'SELECT Id,Campaign_Name__c FROM Campaign__c WHERE Campaign_Name__c LIKE \'%'+searchstring+'%\'LIMIT 10';
            campNames = database.query(searchquery);
        return campNames;
    }    

}