public without sharing class ProofOfPaymentController_CC extends ProofofPaymentController {

    public String selectedUnits             {get;set;}
    public String ChequeFromAccount         {get;set;}
    public String CashDepositSlipDetails    {get;set;}
    public String sourceOfFunds             {get;set;}

    public ProofofPaymentController_CC() { 
        super(false);
        if (CustomerCommunityUtils.isCurrentView('proofofpayment')) {
            super();
            serviceRequestId = '';
            selectedUnits = '';
        }
    }

    public override void init(){
        strHelpPageId = FetchDocumentURL.fetchDocURL('POP_Help');
        try{
            lstSRattachments = new List<SR_Attachments__c>();
            lstCase = new List<Case>();
            AccountList = [SELECT Id
                           ,Name
                           ,Party_ID__c
                           FROM Account
                           WHERE id =: strAccID Limit 1];

            lstBookingUnitWrapper = new list<BookingUnitWrapper>();
            for(Booking_Unit__c varB : [SELECT Id
                    ,Name
                    ,Unit_Name__c
                    ,Registration_ID__c
                    ,Selling_Price__c
                    ,Requested_Price__c

                    ,Inventory__r.IPMS_Bedrooms__c
                    ,Inventory__r.Selling_Price__c
                    ,Inventory__r.Price_Per_Sqft__c
                    ,Inventory__r.Property_Status__c
                    ,Inventory__r.Anticipated_Completion_Date__c
                    ,Inventory__r.Property__r.Name
                    ,Inventory__r.Property__c
                    ,Unit_Details__c
                    ,Booking__r.Deal_SR__r.Name
                    FROM Booking_Unit__c
                    WHERE Booking__r.Account__c =: strAccID
                    AND Unit_Name__c != null
                   ]){
                    lstBookingUnitWrapper.add(new BookingUnitWrapper(varB));
            }

            if(!String.isBlank(strCaseID)){
                System.debug('inside case details method..');
                lstCase = [SELECT  ID
                           ,CaseNumber
                           ,RecordType.name
                           ,RecordType.DeveloperName
                           ,POA_Name__c
                           ,Status
                           ,SR_Type__c
                           ,Payment_Date__c
                           ,Payment_Mode__c
                           ,Payment_Allocation_Details__c
                           ,Total_Amount__c
                           ,Payment_Currency__c
                           ,Total_Token_Amount__c
                           ,Excess_Amount__c
                           ,Sender_Name__c, Cheque_Bank_Name__c
                           ,Swift_Code__c
                           ,Cash_Deposit_Slip_Details__c
                           ,Cheque_from_Account__c
                           ,Source_of_Funds__c
                           ,(SELECT id
                             ,Booking_Unit__c
                             FROM SR_Booking_Units__r)
                           ,(SELECT ID
                             ,Name
                             ,View__c
                             ,Attachment__c
                             ,type__c
                             ,Case__r.CaseNumber
                             ,isValid__c
                             FROM SR_Attachments__r)
                           FROM Case
                           WHERE id =: strCaseID ];

                if(!lstCase.isEmpty()){
                    objCase = lstCase[0];
                    lstSRattachments = lstCase[0].SR_Attachments__r;
                    for(Case detailCase : lstCase){
                        paymentdaterender = true;
                        PaymentDate = detailCase.Payment_Date__c;
                        PaymentMode = detailCase.Payment_Mode__c;
                        POPRemark = detailCase.Payment_Allocation_Details__c;
                        Totalamount = detailCase.Total_Amount__c;
                        PaymentCurrency = detailCase.Payment_Currency__c;
                        ChequeFromAccount = detailCase.Cheque_from_Account__c;
                        CashDepositSlipDetails = detailCase.Cash_Deposit_Slip_Details__c;
                        sourceOfFunds = detailCase.Source_of_Funds__c;
                    }
                }
                System.debug('--lstCase --'+lstCase .Size()+'--'+lstCase );
                //bookingchecked = true;
                lstSPDCattachments = new List<SR_Attachments__c>();
                lstSPDCattachments = [SELECT  ID
                                      ,name
                                      ,IsValid__c,Type__c
                                      ,Attachment__c
                                      FROM SR_Attachments__c
                                      WHERE Case__c =: strCaseID
                                      AND (Type__c = 'POP')
                                     ];
                if(lstSPDCattachments.Size()>0){
                    blnShowPOPdoc = true;
                }

                /*list<SR_Booking_Unit__c> listSR_Booking_Unit = new list<SR_Booking_Unit__c>();
                listSR_Booking_Unit =  [SELECT Id
                                        ,Case__c
                                        ,Booking_Unit__c
                                        ,Case__r.SR_Type__c
                                        ,Case__r.RecordType.DeveloperName
                                        FROM SR_Booking_Unit__c
                                        WHERE Case__r.Status != 'Closed'
                                        AND Case__c  = : strCaseID
                                       ];

                if(!listSR_Booking_Unit.isEmpty()){
                    for(SR_Booking_Unit__c objSRBU : listSR_Booking_Unit){
                        selectedBooking.add(objSRBU);
                    }

                }*/

                if(lstCase != null && !lstCase.isEmpty()) {
                    for(BookingUnitWrapper objBUW: lstBookingUnitWrapper) {
                        for(SR_Booking_Unit__c objSRBU : lstCase[0].SR_Booking_Units__r) {
                            if(objBUW.objBookingUnit.Id == objSRBU.Booking_Unit__c) {
                                objBUW.selected = true;
                            }
                        }
                    }
                }


            }
        }
        catch(Exception e){
            System.debug('...init method exception....'+e+'...line number...'+e.getLineNumber());
        }

    }

    public override list<SelectOption> getPaymentModeOptions()
    {
        list<SelectOption> options =  new list<SelectOption>();
        options.add(new selectOption('','--- None ---'));
        Schema.DescribeFieldResult fieldResult = Case.Payment_Mode__c.getDescribe();
        list<Schema.picklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.picklistEntry f:ple)
        {
            /*if(f.getValue() == 'Bank Transfer')
                options.add(new selectOption(f.getLabel(),f.getValue() + ' - Bank Confirmation showing Swift/IBAN code'));
            else if(f.getValue() == 'Cash' || f.getValue() == 'Cheque')
                options.add(new selectOption(f.getLabel(),f.getValue() + ' - Deposit Slips'));
            else if(f.getValue() == 'Credit Card')*/
                options.add(new selectOption(f.getLabel(),f.getValue()));
        }
        return Options;
    }

    public override PageReference SelectedBookingUnits(){
        listUnitDetails = new list<UnitDetailsWrapper>();
        CapturedBookingUnit = new list<Booking_Unit__c>();

        renderUnitDetails = false;
        try{
            set<Booking_Unit__c> setBU = new set<Booking_Unit__c>();
            for(BookingUnitWrapper BUnit: lstBookingUnitWrapper) {
                if(BUnit.selected == true) {
                    setBU.add(BUnit.objBookingUnit);
                }
            }

            CapturedBookingUnit.addAll(setBU);

            if(!CapturedBookingUnit.isEmpty()){
                map<String, Booking_Unit__c> mapBU = new map<String, Booking_Unit__c>();
                for(Booking_Unit__c objBU : CapturedBookingUnit){
                    mapBU.put(objBU.Name, objBU);
                }
                WrapperBookingUnit objWrb = new WrapperBookingUnit();
                //listUnitDetails.clear();
                for(Booking_Unit__c objB : mapBU.values()){
                    try{
                        renderUnitDetails = true;

                        UnitDetailsWrapper objUnitDetailsWrapper = new UnitDetailsWrapper();
                        objWrb.objIPMSDetailsWrapper = UnitDetailsService.getBookingUnitDetails(objB.Registration_ID__c );
                        objUnitDetailsWrapper.bookingunit = objB.Unit_Name__c;
                        objUnitDetailsWrapper.strPrice = objB.Selling_Price__c;
                        objUnitDetailsWrapper.CurrencyCode = objB.CurrencyIsoCode;
                        objUnitDetailsWrapper.RequestedPrice = objB.Requested_Price__c;
                        objUnitDetailsWrapper.strAmountPaid = objWrb.objIPMSDetailsWrapper.strAmountPaid;
                        objUnitDetailsWrapper.strTotalOutstanding = objWrb.objIPMSDetailsWrapper.strTotalOutstanding;
                        objUnitDetailsWrapper.strAmountOverdue = objWrb.objIPMSDetailsWrapper.strAmountOverdue;
                        listUnitDetails.add(objUnitDetailsWrapper);
                    }
                    catch(exception ex){
                        ApexPages.addMessage(new ApexPages.message(
                            ApexPages.severity.ERROR,ex.getMessage()));
                        errorLogger(ex.getMessage(),'',CapturedBookingUnit);
                    }
                }

                System.debug('...listUnitDetails....'+listUnitDetails);
            }
            System.debug('...CapturedBookingUnit...'+CapturedBookingUnit);

            if(String.isBlank(strCaseID)){
                Id POPcase = Schema.SObjectType.case.getRecordTypeInfosByName().get('POP').getRecordTypeId();
                case objcase = new case();
                objcase.AccountID = strAccID;
                objcase.RecordtypeID = POPcase;
                objcase.SR_Type__c = 'Proof of Payment SR';
                objcase.Type = 'Proof of Payment SR';
                insert objcase;
                strCaseID = objcase.id;
                objOldCase = [Select id,CaseNumber from Case where id =: strCaseID];
            }
        }
        catch(Exception e){
            System.debug('....selected booking unit error...'+e.getLineNumber()+e);
        }
        return null;
    }

    public List<SelectOption> getBookingUnitOptions() {
        List<SelectOption> lstOption = new List<SelectOption>();
        if(lstBookingUnitWrapper == null && !String.isBlank(strAccID)) {
            lstBookingUnitWrapper = new list<BookingUnitWrapper>();
            for(Booking_Unit__c bookingUnit : [ SELECT  Id
                                                       ,Name
                                                       ,Unit_Name__c
                                                       ,Registration_ID__c
                                                       ,Selling_Price__c
                                                       ,CurrencyISOCode
                                                       ,Inventory__r.Selling_Price__c
                                                       ,Inventory__r.Price_Per_Sqft__c
                                                       ,Inventory__r.Property_Status__c
                                                       ,Inventory__r.Anticipated_Completion_Date__c
                                                       ,Inventory__r.Property__r.Name
                                                       ,Inventory__r.Property__c
                                                       ,Unit_Details__c
                                                       ,Booking__r.Deal_SR__r.Name
                                               FROM     Booking_Unit__c
                                               WHERE    Booking__r.Account__c =: strAccID
                                               AND      Unit_Name__c !='']) {
                lstBookingUnitWrapper.add(new BookingUnitWrapper(bookingUnit));
                lstOption.add(new SelectOption(bookingUnit.Unit_Name__c, bookingUnit.Unit_Name__c));
            }
        } else {
            for (BookingUnitWrapper unitWrapper : lstBookingUnitWrapper) {
                lstOption.add(new SelectOption(
                    unitWrapper.objBookingUnit.Unit_Name__c, unitWrapper.objBookingUnit.Unit_Name__c
                ));
            }
        }
        return lstOption;
    }

    public override PageReference createCaseMethod() {
        try{
            OtherDocAttachmentName = '';
            OtherDocAttachmentBody = '';
            PageReference pgr = new PageReference('/' + strCaseID);
            if(String.isNotBlank(fileBody) && String.isNotBlank(fileName)){
                if(String.isNotBlank(strCaseID)){
                    UploadMultipleDocController.data MultipleDocData = new UploadMultipleDocController.data();
                    List<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocRequest = new  List<UploadMultipleDocController.MultipleDocRequest>();
                    system.debug('fileName : '+fileName);
                    system.debug('fileBody : '+fileBody);
                    system.debug('objOldCase : '+objOldCase);
                    system.debug('objOldCase.CaseNumber : '+objOldCase.CaseNumber);
                    system.debug('strCaseID : '+strCaseID);
                    if(!String.isBlank(strCaseID) && objOldCase == NULL){
                        objOldCase = [Select id,CaseNumber from Case where id =: strCaseID];
                    }
                    system.debug('Another objOldCase : '+objOldCase);
                    system.debug('Another objOldCase.CaseNumber : '+objOldCase.CaseNumber);
                    lstMultipleDocRequest = docRepositoryUpload(fileName, fileBody, 'POP',objOldCase.CaseNumber);

                    //System.debug('--lstMultipleDocRequest--'+lstMultipleDocRequest.Size()+'--'+lstMultipleDocRequest);
                    if(lstMultipleDocRequest.Size()>0){
                        MultipleDocData = UploadMultipleDocController.getMultipleDocUrl(lstMultipleDocRequest);
                        System.debug('--MultipleDocData--'+MultipleDocData);
                        if(MultipleDocData != null && MultipleDocData.status == 'Exception'){
                            ApexPages.addmessage(new ApexPages.message(
                            ApexPages.severity.Error,MultipleDocData.message));
                            return null;
                        }
                        if(MultipleDocData != null && (MultipleDocData.Data == null || MultipleDocData.Data.Size() == 0)){
                            ApexPages.addmessage(new ApexPages.message(
                            ApexPages.severity.Error,MultipleDocData.message));
                            return null;
                        }
                    }


                    SR_Attachments__c objSRatt = new SR_Attachments__c ();
                    objSRatt.case__c = strCaseID;
                    objSRatt.Type__C = 'POP';
                    objSRatt.Name = extractName(fileName);

                    if(MultipleDocData != null && MultipleDocData.Data.Size()>0){
                        UploadMultipleDocController.MultipleDocResponse objData = MultipleDocData.Data[0];
                        if(objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url)){
                            objSRatt.Attachment_URL__c = objData.url;
                        }
                        else{
                           ApexPages.addmessage(new ApexPages.message(
                           ApexPages.severity.Error,'An exception has occurred while processing the request, document-'+objData.PARAM_ID+', PROC_STATUS-'+objData.PROC_STATUS+' URL-'+objData.url));
                           return null;
                        }
                    }
                    else{
                         ApexPages.addmessage(new ApexPages.message(
                         ApexPages.severity.Error,'An exception has occurred while processing the request of document upload, got null value, please contact the support team.'));
                         return null;
                    }
                    lstMultipleDocRequest = null;
                    MultipleDocData = null;
                    fileBody = '';
                    fileName = '';
                    insert objSRatt;

                    for(BookingUnitWrapper BUnit: lstBookingUnitWrapper) {
                        if(BUnit.selected == true) {
                            selectedBookingUnit.add(BUnit.objBookingUnit);

                        }
                    }

                    List<SR_Booking_Unit__c> lstSRbooking = new List<SR_Booking_Unit__c>();
                    for(Booking_Unit__c objBooking : selectedBookingUnit){
                        SR_Booking_Unit__c objSRBooking = new SR_Booking_Unit__c();
                        objSRBooking.Booking_Unit__c =  objBooking.ID;
                        objSRBooking.Case__c = strCaseID;
                        lstSRbooking.add(objSRBooking);
                    }
                    if (lstSRbooking.size() > 0) {
                        insert lstSRbooking;
                    }

                    System.debug('Update Case Details');
                    case objcase = new case();
                    objcase.id = strCaseID;
                    objcase.Total_Amount__c =  Totalamount;
                    objcase.Payment_Allocation_Details__c = POPRemark;
                    objcase.Payment_Mode__c = PaymentMode;
                    objcase.Payment_Date__c =PaymentDate;
                    objcase.Payment_Currency__c = PaymentCurrency;
                    objcase.Sender_Name__c = strSenderName;
                    objcase.Cheque_Bank_Name__c = strBankName;
                    objcase.Swift_Code__c = strSwiftCode;
                    objcase.Status = 'Submitted';
                    objcase.Origin = 'Portal';
                    objcase.Cheque_from_Account__c = ChequeFromAccount;
                    objcase.Cash_Deposit_Slip_Details__c = CashDepositSlipDetails;
                    objcase.Source_of_Funds__c = sourceOfFunds;
                    objcase = setCaseOwner(objcase);
                    update objCase;

                    Task objTask = new Task();
                    objTask.WhatId = strCaseID;
                    objTask.Subject = 'Verify Proof of Payment Details in IPMS';
                    objTask.ActivityDate = system.today().addDays(1);
                    objTask.Assigned_User__c = 'Finance';
                   /* objTask = TaskUtility.getTask( (SObject)objCase, 'Verify Case Details', 'CRE',
                                   'POP', system.today().addDays(1) );
                    objTask.OwnerId = Label.DefaultCaseOwnerId;*/
                    objTask.Document_URL__c = objSRatt.Attachment_URL__c;
                    objTask.Process_Name__c ='POP';
                    objTask.Priority = 'High';
                    objTask.Status = 'Not Started';
                    system.debug('--objTask--Proof Of Payment'+objTask);
                    insert objTask;
                    system.debug('>>>>>>>objTask'+objTask);

                    objSRatt = null;
                    System.debug('---objTask---'+objTask);

                    objAccNew.ID = strAccID;
                    objAccNew.POP_Status__c ='POP SR has been created' ;
                    update objAccNew;

                    return pgr;
                }
            }
            else{
               ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select POP doc before submitting SR'));
            }

                return null;
        } catch(Exception ex){
            System.debug('...Exception is....'+ex+'....Line Number'+ex.getLineNumber());
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getMessage()));
            return null;
        }
    }

    public static Case setCaseOwner(Case srPop) {
        List<QueueSobject> lstQueue = [
            SELECT  Id, Queue.Name, QueueId FROM QueueSobject
            WHERE   SobjectType = 'Case' AND Queue.Name = 'Collection Queue'
            LIMIT   1
        ];
        if (!lstQueue.isEmpty()) {
            srPop.OwnerId = lstQueue[0].QueueId;
        }
        return srPop;
    }

    public void submitSr() {
        PageReference nextPage = createCaseMethod();
        System.debug('>>>>>>>>>>>>>>NextPage'+nextPage);
        if (nextPage != NULL) {
            serviceRequestId = nextPage.getUrl().substringAfter('/');
            system.debug('>>>>>>>>>>>>>>serviceRequestId'+serviceRequestId);
        }
    }

    public PageReference reDirectToPdf(){
        PortalHelpSettings__c portalSettLst = [
                                            SELECT
                                                   Document_API_Name__c
                                            FROM
                                                   PortalHelpSettings__c
                                            WHERE
                                                   Name= 'Proof of Payments'
                                             ];
        Document doc = [
                            SELECT
                                Id,
                                Name,
                                body ,
                                Description
                           FROM Document
                           WHERE Name = :portalSettLst.Document_API_Name__c
                           LIMIT 1
                       ];

        PageReference pageRef = new PageReference('/servlet/servlet.FileDownload?file='+doc.Id);

        pageRef.setRedirect(true);
        return pageRef;
    }

    public list<SelectOption> getChequeAccountOptions()
	{
	    list<SelectOption> options =  new list<SelectOption>();
	    options.add(new selectOption('','--- None ---'));
	    Schema.DescribeFieldResult fieldResult = Case.Cheque_from_Account__c.getDescribe();
	    list<Schema.picklistEntry> ple = fieldResult.getPicklistValues();
	    for(Schema.picklistEntry f:ple)
	    {
	            options.add(new selectOption(f.getLabel(),f.getValue()));
	    }
	    return Options;
	}
	
	public list<SelectOption> getCashDepositSlipOptions()
	{
	    list<SelectOption> options =  new list<SelectOption>();
	    options.add(new selectOption('','--- None ---'));
	    Schema.DescribeFieldResult fieldResult = Case.Cash_Deposit_Slip_Details__c.getDescribe();
	    list<Schema.picklistEntry> ple = fieldResult.getPicklistValues();
	    for(Schema.picklistEntry f:ple)
	    {
	            options.add(new selectOption(f.getLabel(),f.getValue()));
	    }
	    return Options;
	}
}