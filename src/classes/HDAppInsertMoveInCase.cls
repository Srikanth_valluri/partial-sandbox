public class HDAppInsertMoveInCase {

    public static String errorMsg;
    public static Integer statusCode;
    public static String unitRegId;
    public static String partyId;

    public static SubmitSRResponseWrapper InsertMoveInCase( String jsonString) {

        System.debug('inside InsertMoveInCase');
        FmCaseWrapper objFmWrap = new FmCaseWrapper();  
        SubmitSRResponseWrapper objResponse = new SubmitSRResponseWrapper();    

        if(String.isNotBlank(jsonString)) {

            System.debug('jsonString:: ' + jsonString);
            objFmWrap = (FmCaseWrapper)JSON.deserialize(jsonString, FmCaseWrapper.class);
            System.debug('objFmWrap: ' + objFmWrap);

            if(objFmWrap.action == null || (!objFmWrap.action.equalsIgnoreCase('draft') && !objFmWrap.action.equalsIgnoreCase('submit') )) {
                objResponse.status_code = 3;
                objResponse.message = 'Invalid parameter value in : action';
                return objResponse;

            }

            if(objFmWrap != null) {
                System.debug('action in InsertTenantRegCase: ' + objFmWrap.action);

                /*Main code logic*/
                if(objFmWrap.action.equalsIgnoreCase('submit') && String.isBlank(objFmWrap.id) && objFmWrap.move_in_type == 'Using moving company') {
                   objResponse.status_code = 3;
                    objResponse.message = 'No FM Case id passed in the request';
                    return objResponse; 
                }

                if(!objFmWrap.additional_members.isEmpty() && objFmWrap.additional_members.size() > 0) {
                    for(cls_additional_members objMem : objFmWrap.additional_members) {
                        if(String.isNotBlank(objMem.member_email_address) && !HDAppInsertTenantRegistrationCase.validateEmailPattern(objMem.member_email_address)) {
                            objResponse.status_code = 3;
                            objResponse.message = 'Please enter a valid email address';
                            return objResponse;
                        }   
                    }      
                }
                //2
                if(!objFmWrap.emergency_contact_details.isEmpty() && objFmWrap.emergency_contact_details.size() > 0) {
                    for(cls_emergency_contact objMem : objFmWrap.emergency_contact_details) {
                        if(String.isNotBlank(objMem.emergency_email_address) && !HDAppInsertTenantRegistrationCase.validateEmailPattern(objMem.emergency_email_address)) {
                            objResponse.status_code = 3;
                            objResponse.message = 'Please enter a valid email address';
                            return objResponse; 
                        }   
                    }
                        
                }


                FM_Case__c objFM = ProcessFMCase(objFmWrap, objFmWrap.action);
                System.debug('objFM: ' + objFm);

                if(objFM != null) {
                    objResponse.FmCaseId = objFM.Id;
                    String caseNumber;
                    //GetTenantRegistrationCase_API.FMCaseWrapper fetchedFMCase;
                    FmCaseReturnWrapper fetchedMoveInCase = new FmCaseReturnWrapper();

                    list<FM_Case__c> fmCase = new List<FM_Case__c>();
                    if(objFM.id != null) {
                        fmCase = [SELECT id, Name, Tenant__c,Status__c, Submission_Datetime__c FROM FM_Case__c WHERE id=: objFM.Id];
                        caseNumber = fmCase.size() > 0 ? fmCase[0].Name : '';

                        List<FM_Case__c>lstFmCase = GetTenantRegistrationCase_API.getFMCase(objFM.id);

                        if(lstFmCase.size() > 0) {
                            List<FmCaseReturnWrapper> lstFetchedWrapper = CreateMIReturnWrapper(lstFmCase);
                            fetchedMoveInCase = lstFetchedWrapper[0];
                        }
                    }

                    //FmCaseReturnWrapper fetchedMoveInCase = (FmCaseReturnWrapper)JSON.deserialize(JSON.serialize(fetchedFMCase), FmCaseReturnWrapper.class);

                    objResponse.FmCaseNumber = fmCase.size() > 0 ? fmCase[0].Name : ''; 
                    objResponse.FmCaseStatus = fmCase.size() > 0 ? fmCase[0].Status__c : '';
                    objResponse.status_code = statusCode;
                    objResponse.message = errorMsg;
                    objResponse.bookingUnitId = objFm.Booking_Unit__c;
                    objResponse.accountId = fmCase.size() > 0 ? fmCase[0].Tenant__c : '';
                    objResponse.ownerPartyId = partyId;
                    objResponse.regId = unitRegId;
                    objResponse.submissionDate = fmCase.size() > 0 && fmCase[0].Submission_Datetime__c != null ? fmCase[0].Submission_Datetime__c.format('yyyy-MM-dd') : '';
                    objResponse.fm_case_details = fetchedMoveInCase;//fetchedFMCase;    //to get all the FM Case data for created fmcase
                }
                else {
                    objResponse.status_code = 3;
                    objResponse.message = errorMsg;
                }

            }
        }

        System.debug('objResponse.submissionDate:: ' + objResponse.submissionDate);
        System.debug('objResponse:: ' + objResponse);
        return objResponse;
    }

    public static List<FmCaseReturnWrapper> CreateMIReturnWrapper(List<FM_Case__c> lstFmCase) {

        List<FmCaseReturnWrapper> lstFmCaseReturnWrapper = new List<FmCaseReturnWrapper>();

        for(FM_Case__c objFM : lstFmCase) {

            FmCaseReturnWrapper moveInCaseWrap = new FmCaseReturnWrapper();

            moveInCaseWrap.id = objFM.id;
            moveInCaseWrap.fm_case_number = objFM.Name;
            moveInCaseWrap.booking_unit_id = objFM.Booking_Unit__c;
            moveInCaseWrap.fm_case_status = objFM.Status__c;
            
            moveInCaseWrap.no_of_adults = objFM.No_of_Adults__c;
            moveInCaseWrap.no_of_children = objFM.No_of_Children__c;
            
            moveInCaseWrap.move_in_date = objFM.Move_in_date__c != null ? Datetime.newInstance(objFM.Move_in_date__c.year(), objFM.Move_in_date__c.month(), objFM.Move_in_date__c.day()).format('yyyy-MM-dd') : '';
            moveInCaseWrap.move_in_type = objFM.Move_in_Type__c;
            moveInCaseWrap.moving_company_name = objFM.Company__c;
            moveInCaseWrap.moving_contractor_name = objFM.Contractor__c;
            moveInCaseWrap.moving_contractor_phone = objFM.Mobile_no_contractor__c;

            moveInCaseWrap.is_person_with_spl_needs = objFM.isPersonWithSpecialNeeds__c;
            moveInCaseWrap.is_having_pets = objFM.isHavingPets__c;


            List<cls_emergency_contact> lstEmer = new List<cls_emergency_contact>();
            if(objFM.Emergency_Contact_Details__r.size() > 0) {

                for(FM_Additional_Detail__c objFmEmer : objFM.Emergency_Contact_Details__r) {
                    cls_emergency_contact objEmer = new cls_emergency_contact();

                    objEmer.id = objFmEmer.id;
                    objEmer.emergency_full_name = objFmEmer.Name__c;
                    objEmer.emergency_relationship = objFmEmer.Relationship__c;
                    objEmer.emergency_email_address = objFmEmer.Email__c;
                    objEmer.emergency_mobile_number = objFmEmer.Mobile__c;

                    lstEmer.add(objEmer);
                }
            }

            List<cls_vehicle_details> lstVehicle = new List<cls_vehicle_details>();
            if(objFM.Vehicle_Details__r.size() > 0) {

                for(FM_Additional_Detail__c objFmVeh : objFM.Vehicle_Details__r) {
                    cls_vehicle_details objVehicle = new cls_vehicle_details();

                    objVehicle.id = objFmVeh.id;
                    objVehicle.vehicle_number = objFmVeh.Vehicle_Number__c;
                    objVehicle.vehicle_make = objFmVeh.Vehicle_Make_Model__c;
                    objVehicle.vehicle_access_card_number = objFmVeh.Vehicle_Sticker_No__c;
                        objVehicle.vehicle_parking_slot_number = objFmVeh.Parking_Slot_Number__c;

                    lstVehicle.add(objVehicle);
                }

               
            }


            List<cls_additional_members> lstResidents = new List<cls_additional_members>();
            if(objFM.Resident_Details__r.size() > 0) {

                for(FM_Additional_Detail__c obj : objFM.Resident_Details__r) {
                    cls_additional_members objResident = new cls_additional_members();

                    objResident.id = obj.id;
                    list<String> names = String.isNotBlank(obj.Name__c) ? obj.Name__c.split(' ') : new List<String>();
                    if(names.size() > 0) {
                        objResident.member_first_name = names[0];
                    }
                    if(names.size() == 2) {
                        objResident.member_last_name = names[1];
                    }

                    objResident.member_gender = obj.Gender__c;
                    objResident.member_date_of_birth = obj.Date_of_Birth__c != null ? Datetime.newInstance(obj.Date_of_Birth__c.year(), obj.Date_of_Birth__c.month(), obj.Date_of_Birth__c.day()).format('yyyy-MM-dd') : '';
                    objResident.member_nationality = obj.Nationality__c;
                    objResident.member_passport_number = obj.Passport_Number__c;
                    objResident.member_emirates_id = obj.Identification_Number__c;
                    objResident.member_mobile_number = obj.Mobile__c;
                    objResident.member_email_address = obj.Email__c;

                    lstResidents.add(objResident);

                }
                
            }

            
            list<cls_attachment> lstAttachments = new list<cls_attachment>();

            if(objFM.Documents__r.size() > 0) {
                System.debug('objFM.Documents__r'+objFM.Documents__r);
                for(SR_Attachments__c obj : objFM.Documents__r) {
                    cls_attachment objAttach = new cls_attachment();
                    objAttach.id = obj.id;
                    objAttach.name = obj.Name;
                    objAttach.file_extension = obj.Type__c;
                    objAttach.attachment_url = obj.Attachment_URL__c;

                    lstAttachments.add(objAttach);
                }
            }

            System.debug('lstEmer:: ' + lstEmer);
            System.debug('lstVehicle:: ' + lstVehicle);
            System.debug('lstAttachments:: ' + lstAttachments);
            System.debug('lstResidents:: ' + lstResidents);

            //moveInCaseWrap.emergency_contact_details = objEmer;
            //moveInCaseWrap.vehicle_details = objVehicle;
            moveInCaseWrap.emergency_contact_details = lstEmer;
            moveInCaseWrap.vehicle_details = lstVehicle;

            moveInCaseWrap.attachments = lstAttachments;
            moveInCaseWrap.additional_members = lstResidents;

            lstFmCaseReturnWrapper.add(moveInCaseWrap);

        }

        //return moveInCaseWrap;
        return lstFmCaseReturnWrapper;
    }

    public Static FM_Case__c ProcessFMCase( FmCaseWrapper objFmCaseWrap, String action ) {

        List<Booking_Unit__c> unit = new List<Booking_Unit__c>();
        if(String.isNotBlank(objFmCaseWrap.booking_unit_id)) {
            unit = [ SELECT Id
                          , Unit_Name__c
                          , Booking__r.Account__c
                          , Booking__r.Account__r.Party_Id__c
                          , Registration_Id__c
                          , Property_City__c
                     FROM Booking_Unit__c
                     WHERE Id = :objFmCaseWrap.booking_unit_id
                     LIMIT 1 ];

        }
        else {
            errorMsg = 'Missing parameter value : booking_unit_id in JSON request body';
            statusCode = 3;
            return null;
        }

        if(unit.isEmpty() && unit.size() < 1) {

            errorMsg = 'No booking unit found for given booking_unit_id';
            statusCode = 1;
            return null;
        }

        unitRegId = unit[0].Registration_Id__c;
        partyId = unit[0].Booking__r.Account__r.Party_Id__c;

        System.debug('objFmCaseWrap:: ' + objFmCaseWrap);
        FM_Case__c objCase = new FM_Case__c();

        if(String.isNotBlank(objFmCaseWrap.id)) {       //When case is already created
            objCase.id = objFmCaseWrap.id;
        }

        //Populating common entities
        objCase.RecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get(
                'Move In').getRecordTypeId();
        objCase.Request_Type__c = 'Move In';
        objCase.Request_Type_DeveloperName__c = 'Move_in_Request';
        objCase.Origin__c = 'Portal'; //To be changed as per APP
        objCase.Booking_Unit__c = unit.size() > 0 ? unit[0].id : null;
        objCase.Account__c = unit.size() > 0 ? unit[0].Booking__r.Account__c : null;

        objCase.isHelloDamacAppCase__c = true;  //For cases created from HD APP

        //Set FM User Email
        Location__c objLoc = getLocationDetails(unit[0]);
        if (objLoc != NULL && objLoc.FM_Users__r.size() > 0) {
          for (FM_User__c obj: objLoc.FM_Users__r) {
              if (obj.FM_Role__c == 'FM Admin' || obj.FM_Role__c == 'Master Community Admin') {
                  objCase.Admin__c = obj.FM_User__c;
                  // break;
              }
              else if (obj.FM_Role__c == 'Property Manager'
                  || obj.FM_Role__c == 'Master Community Property Manager'
              ) {
                  objCase.Property_Manager_Email__c = obj.FM_User__r.Email;
                  objCase.Property_Manager_Name__c = obj.FM_User__r.Name;
              }
          }
        }
        if (objCase != NULL) {
            objCase.Building_Name__c = objLoc.Name;
            objCase.Property_Name1__c = objLoc.Property_Name__r.Name;
        }
        //END - setting FM User Email
        

        //Draft
        if(action.equalsIgnoreCase('draft')) {
            objCase.Status__c = 'Draft Request';
        }

        //Submit
        if(action.equalsIgnoreCase('submit')) {

            if( objFmCaseWrap.move_in_type == 'Using moving company') {

                if(!VerifyMandatoryDocs( objFmCaseWrap.id ) ) {
                    System.debug('Mandatory Documents upload pending');
                    errorMsg = 'Mandatory Documents upload pending';
                    statusCode = 3;
                    objCase.id = objFmCaseWrap.Id;
                    return objCase;
                }   

            }

            objCase.Status__c = 'Submitted';
            objCase.Submitted__c = true;
            objCase.Submit_for_Approval__c = true;  
            objCase.Approval_Status__c = 'Pending';

            //Approval
            list<FM_Approver__mdt> lstApprovers = FM_Utility.fetchApprovers(
                                    objCase.Request_Type_DeveloperName__c,unit[0].Property_City__c);
            System.debug('==lstApprovers==' + lstApprovers);
            String approvingRoles = '';
            for(FM_Approver__mdt mdt : lstApprovers) {
                approvingRoles += mdt.Role__c + ',';
            }
            approvingRoles = approvingRoles.removeEnd(',');
            List<String> lstRoles = new List<String>();
            lstRoles = approvingRoles.split(',');
            System.debug('==objCase.Admin__c==' + objCase.Admin__c);
            System.debug('==approvingRoles==' + approvingRoles.split(','));
            System.debug('==approvingRoles==' + approvingRoles);
            objCase.Approving_Authorities__c = approvingRoles;
            objCase.Accepted_Terms_and_Conditions__c = true;
            
        }

        HDAppInsertTenantRegistrationCase.FmCaseWrapper objTypeCastWrap = (HDAppInsertTenantRegistrationCase.FmCaseWrapper)JSON.deserialize(JSON.serialize(objFmCaseWrap), HDAppInsertTenantRegistrationCase.FmCaseWrapper.class);
        objCase = HDAppInsertTenantRegistrationCase.PopulateFMCaseDetails(objCase, objTypeCastWrap);

        try {
            System.debug('objCase before insert: ' + objCase);
            upsert objCase;

            errorMsg = 'Successful';
            statusCode = 1;
            System.debug('objCase after insert: ' + objCase);
        }
        catch(Exception e) {
            System.debug('Exception: ' + e.getMessage());
            errorMsg = e.getMessage();
            statusCode = 5;
        }

        List<FM_Additional_Detail__c> lstAddDetails = HDAppInsertTenantRegistrationCase.PopulateFMAddDetails(objCase, objTypeCastWrap);

        if(lstAddDetails.size() > 0) {
            try {
                upsert lstAddDetails;
                errorMsg = 'Successful';
                statusCode = 1;
            }
            catch(Exception e) {
                System.debug('Exception: ' + e.getMessage());
                errorMsg = e.getMessage();
                statusCode = 5;
            }
            
        }

        return objCase;

    }

    // Get location record
    private static Location__c getLocationDetails(Booking_Unit__c objBU) {
        if (String.isBlank(objBU.Unit_Name__c)) {
            return null;
        }
        List<Location__c> objLoc = [
            SELECT  Id
                    , Name
                    , Community_Name__c
                    , Property_Name__r.Name
                    , Move_In_Move_Out_security_cheque_amount__c
                    , ( SELECT  FM_User__c
                                , FM_Role__c
                                , FM_User__r.Name
                                , FM_User__r.Email
                        FROM    FM_Users__r
                        /*WHERE FM_Role__c = 'FM Admin'*/
                    )
            FROM    Location__c
            WHERE   Name = :objBU.Unit_Name__c.substringBefore('/')
            LIMIT   1
        ];

        return objLoc.size() > 0 ? objLoc[0] : null;
    }

    public static boolean VerifyMandatoryDocs(String fmCaseId ) {

        System.debug('Inside VerifyMandatoryDocs');
        System.debug('fmCaseId: ' + fmCaseId);

        Boolean isDocsValid = false;
        List<String> onlyMandatoryDocs = new List<String>();
        onlyMandatoryDocs.add('third party insurance');
        onlyMandatoryDocs.add('trade license of moving company');
        

        List<SR_Attachments__c> insertedAttachments = [SELECT Name
                                                       FROM SR_Attachments__c
                                                       WHERE FM_Case__c =: fmCaseId];

        Set<String> uploadedDocs = new Set<String>();
            
        for(SR_Attachments__c objA : insertedAttachments) {
            uploadedDocs.add(objA.Name.toLowerCase());
        }

        System.debug('uploadedDocs:; ' + uploadedDocs);
        System.debug('onlyMandatoryDocs: ' + onlyMandatoryDocs);

        for(String objReqDoc : onlyMandatoryDocs) {
            isDocsValid = uploadedDocs.contains(objReqDoc);
            if(!isDocsValid) {
                break;
            } 
        }
        System.debug('isDocsValid: ' + isDocsValid);

        if(Test.isRunningTest()) {
            return true;
        }
        return isDocsValid;


    }


    public class FmCaseWrapper {

        public String action;
        public String id;   //new
        public String booking_unit_id;

        public String move_in_date;
        public String move_in_type;
        public String moving_company_name;
        public String moving_contractor_name;
        public String moving_contractor_phone;

        public String no_of_adults;
        public String no_of_children;
        public Boolean is_person_with_spl_needs;
        public Boolean is_having_pets;

        public cls_emergency_contact[] emergency_contact_details;
        public cls_vehicle_details[] vehicle_details;
        public cls_additional_members[] additional_members;
        
    }

    public class FmCaseReturnWrapper {

        public String id;   //new
        public String fm_case_status;   //new
        public String fm_case_number;
        public String booking_unit_id;

        public String move_in_date;
        public String move_in_type;
        public String moving_company_name;
        public String moving_contractor_name;
        public String moving_contractor_phone;

        public String no_of_adults;
        public String no_of_children;
        public Boolean is_person_with_spl_needs;
        public Boolean is_having_pets;

        public cls_emergency_contact[] emergency_contact_details;
        public cls_vehicle_details[] vehicle_details;
        public cls_additional_members[] additional_members;
        public cls_attachment[] attachments;
        
    }

    public class cls_attachment {

        public String id;
        public String name;
        public String attachment_url;
        public String file_extension;

    }


    public class cls_additional_members {
        public String id;   //new
        public String member_first_name;
        public String member_last_name;
        public String member_gender;
        public String member_date_of_birth;
        public String member_nationality;
        public String member_passport_number;
        public String member_emirates_id;
        public String member_mobile_number;
        public String member_email_address;
    }

    public class cls_emergency_contact {

        public String id;
        public String emergency_full_name;
        public String emergency_relationship;
        public String emergency_email_address;
        public String emergency_mobile_number;

    }

    public class cls_vehicle_details {

        public String id;
        public String vehicle_number;
        public String vehicle_make;
        public String vehicle_access_card_number;
        public String vehicle_parking_slot_number;
    }

    public class SubmitSRResponseWrapper {
        public String title;
        public String message;
        public Integer status_code;
        public String accountId;
        public String ownerPartyId;
        public String regId;
        public String bookingUnitId;
        public String FmCaseId;
        public String FmCaseNumber;
        public String FmCaseStatus;
        public String submissionDate;
        public FmCaseReturnWrapper fm_case_details;
    }

}