@isTest
public class DAMAC_InventoryVillaService_TEST{

    @testSetup static void setup() {
        Location__c location = new Location__c();
        location.Name = 'Test Location1';
        location.Location_ID__c = 'Test1Id';
        insert location;
        
        Inventory__c inventory = new Inventory__c();
        inventory.Building_Location__c = location.Id;
        inventory.Status__c='Released';
        insert inventory;
        
        Campaign__c campaign = new Campaign__c();
        campaign.Agent_quality__c = 'test campaign 1';
        campaign.Marketing_Start_Date__c = Date.today()-5;
        campaign.Start_Date__c = Date.today()-4;
        campaign.End_Date__c = Date.today()-1;
        campaign.Marketing_End_Date__c = Date.today();
        try {
            campaign.recordTypeId = [SELECT ID FROM RecordType WHERE developerName = 'Standalone' AND SobjectType = 'Campaign__c'].Id;
        } catch (Exception e) {}
        campaign.Campaign_Type_New__c = 'Villa Configuration';
        insert campaign;
        
        Campaign_Inventory__c campaignInv = new Campaign_Inventory__c();
        campaignInv.Inventory__c = inventory.Id;
        campaignInv.Campaign__c = campaign.Id;
        insert campaignInv;
        
        Inventory_user__c inventoryUser = new Inventory_user__c();
        inventoryUser.Inventory__c = inventory.Id;
        insert inventoryUser;
        
        Payment_plan__c paymentPlan = new Payment_plan__c();
        paymentPlan.Building_Location__c = inventory.Building_Location__c;
        insert paymentPlan;
        
        Payment_Terms__c paymentTerm = new Payment_Terms__c();
        paymentTerm.Payment_Plan__c = paymentPlan.Id;
        insert paymentTerm;
    }
    static testMethod void inventoryService(){
        
        Inventory__c inventory = [SELECT ID FROM Inventory__c LIMIT 1];
        
        Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/PortalInventoryService/plans';  //Request URL
        req.httpMethod = 'GET';
        req.addParameter('invId', inventory.Id);
        RestContext.request = req;
        RestContext.response= res;
        
        DAMAC_InventoryVillaService.getInventory();
        Test.stopTest();
    }
    static testMethod void inventoryService1(){
        Inventory__c inventory = [SELECT ID FROM Inventory__c LIMIT 1];
        
        
        Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/PortalInventoryService/searchInventory';  //Request URL
        req.httpMethod = 'GET';
        req.addParameter('invId', inventory.Id);
        RestContext.request = req;
        RestContext.response= res;
        
        DAMAC_InventoryVillaService.getInventory();
        Test.stopTest();
    }
    
    static testMethod void inventoryService2 (){
        Inventory__c inventory = [SELECT ID FROM Inventory__c LIMIT 1];
        
        
        Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/PortalInventoryService/searchRM';  //Request URL
        req.addParameter('email', 'test@test.com');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response= res;
        
        DAMAC_InventoryVillaService.getInventory();
        Test.stopTest();
    }
    
    static testMethod void inventoryService3 (){
        Inventory__c inventory = [SELECT ID FROM Inventory__c LIMIT 1];
        
        
        Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/PortalInventoryService/searchAgent';  //Request URL
        req.addParameter('email', 'test@test.com');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response= res;
        
        DAMAC_InventoryVillaService.getInventory();
        Test.stopTest();
    }
}