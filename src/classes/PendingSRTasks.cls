public class PendingSRTasks{//AKISHOR: for displaying SR pending tasks
public List<Task> tasks{get;set;}//=new List<Task>();
    public void tasks()
    {
        String userId = UserInfo.getUserId();   

        system.debug([Select Id,Status,WhatId ,CreatedDate,Process_Name__c,Subject,Assigned_User__c,OwnerId from Task where WhatId !='' AND process_Name__c !='' AND Status != 'Reject' AND Status != 'Aborted' AND Status != 'Cancelled' AND OwnerId =: userId]);
        tasks = [Select Id,Status,What.Name,CreatedDate,Process_Name__c,Subject,Assigned_User__c,OwnerId from Task where WhatId !='' AND process_Name__c !='' AND Status != 'Reject' AND Status != 'Aborted' AND Status != 'Cancelled' AND Status != 'Closed' AND Status != 'Reject - Needs Correction' AND Status != 'Completed' AND Status != 'Cancel' AND OwnerId =: userId];
        //return tasks;
        //(Select Id from User where Id=:UserInfo.getUserId()) 
    }
}