@isTest
public with sharing class AP_KnowledgeArticleControllerTest {
    @isTest
    public static void testFetchSubCatAndArticlesOnLoad() {

        //Create AP_Knowledge__kav
        AP_Knowledge__kav article = new AP_Knowledge__kav(Summary='xyz',Urlname='xyz');
        article.Title = 'Test Title';
        article.Description__c = 'Test Description';
        article.Language = 'en_US';
        insert article;
        
        AP_Knowledge__kav newArticle = [SELECT
                                            Id, Title, KnowledgeArticleId
                                        From
                                            AP_Knowledge__kav
                                        WHERE
                                            Id =: article.Id
                                        ][0];

        //Publishing Article
        if(newArticle.KnowledgeArticleId != null ) {
            KbManagement.PublishingService.publishArticle(newArticle.KnowledgeArticleId, true);
        }
        DescribeDataCategoryGroupResult[] results = Schema.describeDataCategoryGroups(
            new String[] { 'KnowledgeArticleVersion'}
        );

        //Associating data category to knowledge article
        AP_Knowledge__DataCategorySelection dataCat = new AP_Knowledge__DataCategorySelection ();
        System.debug('article.Id - ' + article.Id);
        dataCat.DataCategoryGroupName = results[0].getName();
        dataCat.DataCategoryName = 'FAQ';
        dataCat.ParentId = newArticle.Id;
        insert dataCat;

        Test.startTest();
        PageReference pageRef = Page.AP_KnowledgeArticle;
        Test.setCurrentPage(pageRef);
        AP_KnowledgeArticleController controller = new AP_KnowledgeArticleController();
        //controller.parentCategory = 'AP_FAQs';
        Test.stopTest();
        //system.assert(controller.subCatAndKnowArtMap.containsKey('FAQ'));
    }

    @isTest
    public static void testGetDataCategoriesStructure() {
        Test.startTest();
        PageReference pageRef = Page.AP_KnowledgeArticle;
        Test.setCurrentPage(pageRef);
        AP_KnowledgeArticleController controller = new AP_KnowledgeArticleController();
        controller.getDataCategoriesStructure();
        Test.stopTest();

        DescribeDataCategoryGroupResult[] results = Schema.describeDataCategoryGroups(
            new String[] { 'KnowledgeArticleVersion'}
        );

        //system.assertEquals(results.size(), controller.parentCatList.size());
    }

}