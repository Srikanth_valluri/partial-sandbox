@isTest
private class Damac_TripsAllocationController_Test {

    static testMethod void testTripController() {
        
            
            inquiry__c i = new inquiry__c();
            i.First_Name__c = 'FirstName';
            i.last_Name__c = 'FirstName';
            i.ownerid=userinfo.getuserid();
            insert i;
            
            Trip__c trip = new Trip__c ();
            trip.Trip_Requester_Comments__c = 'Testing';
            trip.start_time__c = DateTime.Now().addHours(10);
            trip.end_time__c = DateTime.Now().addHours(15);
            trip.Status__c = 'Test';
            trip.inquiry__c=i.id;
            insert trip;
            
            
            Cars__c car = new Cars__c ();
            insert car;
            
            Driver__c driver = new Driver__c ();
            driver.User__c = [SELECT ID FROM User WHERE ID =: UserInfo.getUserId() LIMIT 1].ID;
            driver.Coordinator__c = driver.User__c;
            driver.Assigned_Car__c = car.id;
            insert driver;
            
            Damac_TripsAllocationController obj = new Damac_TripsAllocationController();
            try{
                Damac_TripsAllocationController.assignDriverToTrip (driver.Id, trip.Id);
            }catch(exception e){}
            
            apexpages.currentpage().getparameters().put('tripId', trip.Id);
            obj.rescheduleTrip.Start_Time__c = DateTime.Now().addDays(2);            
            obj.reschduleTripTime();
            obj.cancelTrip ();
            obj.getAllTripsOptions ();
            obj.reassignTrip ();
            trip.Driver__c = driver.id;
            
    }
    
    static testMethod void testTripShareController() {
        
            inquiry__c i = new inquiry__c();
            i.First_Name__c = 'FirstName';
            i.last_Name__c = 'FirstName';
            i.ownerid=userinfo.getuserid();
            insert i;
            
            Trip__c trip = new Trip__c ();
            trip.Trip_Requester_Comments__c = 'Testing';
            trip.start_time__c = DateTime.Now();
            trip.end_time__c = DateTime.Now();
            trip.Status__c = 'Test';
            trip.inquiry__c = i.id;
            insert trip;
            
            
            Cars__c car = new Cars__c ();
            insert car;
            
            Driver__c driver = new Driver__c ();
            driver.User__c = [SELECT ID FROM User WHERE ID =: UserInfo.getUserId() LIMIT 1].ID;
            driver.Coordinator__c = driver.User__c;
            driver.Assigned_Car__c = car.id;
            insert driver;
            
            
    }
}